/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CCI0031_RD_PDH_MDLPRM_H_
#define _AF6_REG_AF6CCI0031_RD_PDH_MDLPRM_H_

/*--------------------------- Define -----------------------------------------*/

#define cAf6RegMdlTxOc24SliceOffset 0x800

/*--------------------------------------
 Register Full Name: CONFIG MDL Tx
 Address      : 0x0460 - 0x477
--------------------------------------*/
#define cAf6RegCfgMdlTxInsCtrl(bufferId, de3Id) (0x0420 + (bufferId * 0x20) + de3Id)
#define cAf6RegCfgMdlTxInsCtrlBase              (0x0420)
/*--------------------------------------
BitField Name: cfg_entx
BitField Type: R/W
BitField Desc: SET/CLEAR to ALRM STATUS MESSAGE in BUFFER 1
--------------------------------------*/
#define cAf6RegCfgMdlTxInsEnMask     cBit0
#define cAf6RegCfgMdlTxInsEnShift    0

/*------------------------------------------------------------------------------
Reg Name   : CONFIG ENABLE MONITOR FOR DEBUG BOARD
Reg Addr   : 0x14001
Reg Formula:
    Where  :
Reg Desc   :
config enable monitor data before stuff

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_debug_Base                                                                    0x14001
#define cAf6Reg_upen_cfg_debug                                                                         0x14001
#define cAf6Reg_upen_cfg_debug_WidthVal                                                                     32
#define cAf6Reg_upen_cfg_debug_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: out_cfg_bar
BitField Type: R/W
BitField Desc: reserve
BitField Bits: [31:19]
--------------------------------------*/
#define cAf6_upen_cfg_debug_out_cfg_bar_Bit_Start                                                           19
#define cAf6_upen_cfg_debug_out_cfg_bar_Bit_End                                                             31
#define cAf6_upen_cfg_debug_out_cfg_bar_Mask                                                         cBit31_19
#define cAf6_upen_cfg_debug_out_cfg_bar_Shift                                                               19
#define cAf6_upen_cfg_debug_out_cfg_bar_MaxVal                                                          0x1fff
#define cAf6_upen_cfg_debug_out_cfg_bar_MinVal                                                             0x0
#define cAf6_upen_cfg_debug_out_cfg_bar_RstVal                                                             0x0

/*--------------------------------------
BitField Name: out_cfg_selinfo1
BitField Type: R/W
BitField Desc: select just 8 bit INFO from PRM FORCE but except 2 bit NmNi, (1)
is enable, (0) is disable
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_upen_cfg_debug_out_cfg_selinfo1_Bit_Start                                                      18
#define cAf6_upen_cfg_debug_out_cfg_selinfo1_Bit_End                                                        18
#define cAf6_upen_cfg_debug_out_cfg_selinfo1_Mask                                                       cBit18
#define cAf6_upen_cfg_debug_out_cfg_selinfo1_Shift                                                          18
#define cAf6_upen_cfg_debug_out_cfg_selinfo1_MaxVal                                                        0x1
#define cAf6_upen_cfg_debug_out_cfg_selinfo1_MinVal                                                        0x0
#define cAf6_upen_cfg_debug_out_cfg_selinfo1_RstVal                                                        0x0

/*--------------------------------------
BitField Name: cfg_loopaback_mdl
BitField Type: R/W
BitField Desc: loop back MDL DS3, (1) is enable, (0) is disable
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_upen_cfg_debug_cfg_loopaback_mdl_Bit_Start                                                     17
#define cAf6_upen_cfg_debug_cfg_loopaback_mdl_Bit_End                                                       17
#define cAf6_upen_cfg_debug_cfg_loopaback_mdl_Mask                                                      cBit17
#define cAf6_upen_cfg_debug_cfg_loopaback_mdl_Shift                                                         17
#define cAf6_upen_cfg_debug_cfg_loopaback_mdl_MaxVal                                                       0x1
#define cAf6_upen_cfg_debug_cfg_loopaback_mdl_MinVal                                                       0x0
#define cAf6_upen_cfg_debug_cfg_loopaback_mdl_RstVal                                                       0x0

/*--------------------------------------
BitField Name: cfg_loopaback_prm
BitField Type: R/W
BitField Desc: loop back PRM DS1, (1) is enable, (0) is disable
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_upen_cfg_debug_cfg_loopaback_prm_Bit_Start                                                     16
#define cAf6_upen_cfg_debug_cfg_loopaback_prm_Bit_End                                                       16
#define cAf6_upen_cfg_debug_cfg_loopaback_prm_Mask                                                      cBit16
#define cAf6_upen_cfg_debug_cfg_loopaback_prm_Shift                                                         16
#define cAf6_upen_cfg_debug_cfg_loopaback_prm_MaxVal                                                       0x1
#define cAf6_upen_cfg_debug_cfg_loopaback_prm_MinVal                                                       0x0
#define cAf6_upen_cfg_debug_cfg_loopaback_prm_RstVal                                                       0x0

/*--------------------------------------
BitField Name: out_info_force
BitField Type: R/W
BitField Desc: 10 bits info force [15:13] : 3'd1 : to set bit G1 3'd2 :  to set
bit G2 3'd3 :  to set bit G3 3'd4 : to set bit G4 3'd5 : to set bit G5 3'd6 :
to set bit G6 [12] : to set bit SE [11] : to set bit FE [10] : to set bit LV [9]
: to set bit SL [8] : to set bit LB
BitField Bits: [15:08]
--------------------------------------*/
#define cAf6_upen_cfg_debug_out_info_force_Bit_Start                                                         8
#define cAf6_upen_cfg_debug_out_info_force_Bit_End                                                          15
#define cAf6_upen_cfg_debug_out_info_force_Mask                                                       cBit15_8
#define cAf6_upen_cfg_debug_out_info_force_Shift                                                             8
#define cAf6_upen_cfg_debug_out_info_force_MaxVal                                                         0xff
#define cAf6_upen_cfg_debug_out_info_force_MinVal                                                          0x0
#define cAf6_upen_cfg_debug_out_info_force_RstVal                                                          0x0

/*--------------------------------------
BitField Name: out_cfg_dis
BitField Type: R/W
BitField Desc: disable monitor, reset address buffer monitor, (1) is disable,
(0) is enable
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_cfg_debug_out_cfg_dis_Bit_Start                                                            0
#define cAf6_upen_cfg_debug_out_cfg_dis_Bit_End                                                              0
#define cAf6_upen_cfg_debug_out_cfg_dis_Mask                                                             cBit0
#define cAf6_upen_cfg_debug_out_cfg_dis_Shift                                                                0
#define cAf6_upen_cfg_debug_out_cfg_dis_MaxVal                                                             0x1
#define cAf6_upen_cfg_debug_out_cfg_dis_MinVal                                                             0x0
#define cAf6_upen_cfg_debug_out_cfg_dis_RstVal                                                             0x1


/*------------------------------------------------------------------------------
Reg Name   : CONFIG ID TO MONITOR FOR DEBUG BOARD
Reg Addr   : 0x14002
Reg Formula: 
    Where  : 
Reg Desc   : 
config ID to monitor tx message

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfgid_mon_Base                                                                    0x14002
#define cAf6Reg_upen_cfgid_mon                                                                         0x14002
#define cAf6Reg_upen_cfgid_mon_WidthVal                                                                     32
#define cAf6Reg_upen_cfgid_mon_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: out_cfgid_mon
BitField Type: R/W
BitField Desc: ID which is configured to monitor
BitField Bits: [09:00]
--------------------------------------*/
#define cAf6_upen_cfgid_mon_out_cfgid_mon_Bit_Start                                                          0
#define cAf6_upen_cfgid_mon_out_cfgid_mon_Bit_End                                                            9
#define cAf6_upen_cfgid_mon_out_cfgid_mon_Mask                                                         cBit9_0
#define cAf6_upen_cfgid_mon_out_cfgid_mon_Shift                                                              0
#define cAf6_upen_cfgid_mon_out_cfgid_mon_MaxVal                                                         0x3ff
#define cAf6_upen_cfgid_mon_out_cfgid_mon_MinVal                                                           0x0
#define cAf6_upen_cfgid_mon_out_cfgid_mon_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG PRM BIT C/R & STANDARD Tx
Reg Addr   : 0x1000 - 0x153F
Reg Formula: 0x1000+$STSID[4:3]*7 + $VTGID*64 + $SLICEID*32 + $STSID[2:0]*4 + $VTID
    Where  :
           + $STSID (0-23) : STS ID
           + $VTGID (0-6) : VTG ID
           + $SLICEID (0-1) : SLICE ID
           + $VTID (0-3) : VT ID
Reg Desc   :
Config PRM bit C/R & Standard

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_prm_txcfgcr_Base                                                                   0x1000
#define cAf6Reg_upen_prm_txcfgcr(STSID, VTGID, SLICEID, VTID)         (0x1000+(STSID)[4:3]*7+(VTGID)*64+(SLICEID)*32+(STSID)[2:0]*4+(VTID))
#define cAf6Reg_upen_prm_txcfgcr_WidthVal                                                                   32
#define cAf6Reg_upen_prm_txcfgcr_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: cfg_prm_lb
BitField Type: R/W
BitField Desc: config bit LB
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_prm_txcfgcr_cfg_prm_lb_Bit_Start                                                           4
#define cAf6_upen_prm_txcfgcr_cfg_prm_lb_Bit_End                                                             4
#define cAf6_upen_prm_txcfgcr_cfg_prm_lb_Mask                                                            cBit4
#define cAf6_upen_prm_txcfgcr_cfg_prm_lb_Shift                                                               4
#define cAf6_upen_prm_txcfgcr_cfg_prm_lb_MaxVal                                                            0x1
#define cAf6_upen_prm_txcfgcr_cfg_prm_lb_MinVal                                                            0x0
#define cAf6_upen_prm_txcfgcr_cfg_prm_lb_RstVal                                                            0x0

/*--------------------------------------
BitField Name: cfg_prmen_tx
BitField Type: R/W
BitField Desc: config enable Tx, (0) is disable (1) is enable
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_prm_txcfgcr_cfg_prmen_tx_Bit_Start                                                         3
#define cAf6_upen_prm_txcfgcr_cfg_prmen_tx_Bit_End                                                           3
#define cAf6_upen_prm_txcfgcr_cfg_prmen_tx_Mask                                                          cBit3
#define cAf6_upen_prm_txcfgcr_cfg_prmen_tx_Shift                                                             3
#define cAf6_upen_prm_txcfgcr_cfg_prmen_tx_MaxVal                                                          0x1
#define cAf6_upen_prm_txcfgcr_cfg_prmen_tx_MinVal                                                          0x0
#define cAf6_upen_prm_txcfgcr_cfg_prmen_tx_RstVal                                                          0x0

/*--------------------------------------
BitField Name: cfg_prm_txenstuff
BitField Type: R/W
BitField Desc: config enable stuff message, (1) is disable, (0) is enbale
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_prm_txcfgcr_cfg_prm_txenstuff_Bit_Start                                                    2
#define cAf6_upen_prm_txcfgcr_cfg_prm_txenstuff_Bit_End                                                      2
#define cAf6_upen_prm_txcfgcr_cfg_prm_txenstuff_Mask                                                     cBit2
#define cAf6_upen_prm_txcfgcr_cfg_prm_txenstuff_Shift                                                        2
#define cAf6_upen_prm_txcfgcr_cfg_prm_txenstuff_MaxVal                                                     0x1
#define cAf6_upen_prm_txcfgcr_cfg_prm_txenstuff_MinVal                                                     0x0
#define cAf6_upen_prm_txcfgcr_cfg_prm_txenstuff_RstVal                                                     0x0
/* Hardware change 2016-June-10 */
#define cAf6_upen_prm_txcfgcr_cfg_prm_txfcs_mode_Mask                                                     cBit2
#define cAf6_upen_prm_txcfgcr_cfg_prm_txfcs_mode_Shift                                                        2

/*--------------------------------------
BitField Name: cfg_prmstd_tx
BitField Type: R/W
BitField Desc: config standard Tx, (0) is ANSI, (1) is AT&T
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_prm_txcfgcr_cfg_prmstd_tx_Bit_Start                                                        1
#define cAf6_upen_prm_txcfgcr_cfg_prmstd_tx_Bit_End                                                          1
#define cAf6_upen_prm_txcfgcr_cfg_prmstd_tx_Mask                                                         cBit1
#define cAf6_upen_prm_txcfgcr_cfg_prmstd_tx_Shift                                                            1
#define cAf6_upen_prm_txcfgcr_cfg_prmstd_tx_MaxVal                                                         0x1
#define cAf6_upen_prm_txcfgcr_cfg_prmstd_tx_MinVal                                                         0x0
#define cAf6_upen_prm_txcfgcr_cfg_prmstd_tx_RstVal                                                         0x0

/*--------------------------------------
BitField Name: cfg_prm_cr
BitField Type: R/W
BitField Desc: config bit command/respond PRM message
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_prm_txcfgcr_cfg_prm_cr_Bit_Start                                                           0
#define cAf6_upen_prm_txcfgcr_cfg_prm_cr_Bit_End                                                             0
#define cAf6_upen_prm_txcfgcr_cfg_prm_cr_Mask                                                            cBit0
#define cAf6_upen_prm_txcfgcr_cfg_prm_cr_Shift                                                               0
#define cAf6_upen_prm_txcfgcr_cfg_prm_cr_MaxVal                                                            0x1
#define cAf6_upen_prm_txcfgcr_cfg_prm_cr_MinVal                                                            0x0
#define cAf6_upen_prm_txcfgcr_cfg_prm_cr_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG PRM BIT L/B Tx
Reg Addr   : 0x1800 - 0x1D3F
Reg Formula: 0x1800+$STSID[4:3]*7 + $VTGID*64 + $SLICEID*32 + $STSID[2:0]*4 + $VTID
    Where  :
           + $STSID (0-23) : STS ID
           + $VTGID (0-6) : VTG ID
           + $SLICEID (0-1) : SLICE ID
           + $VTID (0-3) : VT ID
Reg Desc   :
CONFIG PRM BIT L/B Tx

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_prm_txcfglb_Base                                                                   0x1800
#define cAf6Reg_upen_prm_txcfglb(STSID, VTGID, SLICEID, VTID)         (0x1800+(STSID)[4:3]*7+(VTGID)*64+(SLICEID)*32+(STSID)[2:0]*4+(VTID))
#define cAf6Reg_upen_prm_txcfglb_WidthVal                                                                   32
#define cAf6Reg_upen_prm_txcfglb_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: cfg_prm_enlb
BitField Type: R/W
BitField Desc: config enable CPU config LB bit, (0) is disable, (1) is enable
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_prm_txcfglb_cfg_prm_enlb_Bit_Start                                                         1
#define cAf6_upen_prm_txcfglb_cfg_prm_enlb_Bit_End                                                           1
#define cAf6_upen_prm_txcfglb_cfg_prm_enlb_Mask                                                          cBit1
#define cAf6_upen_prm_txcfglb_cfg_prm_enlb_Shift                                                             1
#define cAf6_upen_prm_txcfglb_cfg_prm_enlb_MaxVal                                                          0x1
#define cAf6_upen_prm_txcfglb_cfg_prm_enlb_MinVal                                                          0x0
#define cAf6_upen_prm_txcfglb_cfg_prm_enlb_RstVal                                                          0x0

/*--------------------------------------
BitField Name: cfg_prm_lb
BitField Type: R/W
BitField Desc: config bit Loopback PRM message
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_prm_txcfglb_cfg_prm_lb_Bit_Start                                                           0
#define cAf6_upen_prm_txcfglb_cfg_prm_lb_Bit_End                                                             0
#define cAf6_upen_prm_txcfglb_cfg_prm_lb_Mask                                                            cBit0
#define cAf6_upen_prm_txcfglb_cfg_prm_lb_Shift                                                               0
#define cAf6_upen_prm_txcfglb_cfg_prm_lb_MaxVal                                                            0x1
#define cAf6_upen_prm_txcfglb_cfg_prm_lb_MinVal                                                            0x0
#define cAf6_upen_prm_txcfglb_cfg_prm_lb_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : Status process PRM status stuff
Reg Addr   : 0x2000 - 0x253F
Reg Formula: 0x2000+$STSID[4:3]*7 + $VTGID*64 + $SLICEID*32 + $STSID[2:0]*4 + $VTID
    Where  : 
           + $STSID (0-23) : STS ID
           + $VTGID (0-6) : VTG ID
           + $SLICEID (0-1) : SLICE ID
           + $VTID (0-3) : VT ID
Reg Desc   : 
Status process PRM data

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_prm_txsta_Base                                                                     0x2000
#define cAf6Reg_upen_prm_txsta(STSID, VTGID, SLICEID, VTID)           (0x2000+(STSID)[4:3]*7+(VTGID)*64+(SLICEID)*32+(STSID)[2:0]*4+(VTID))
#define cAf6Reg_upen_prm_txsta_WidthVal                                                                     32
#define cAf6Reg_upen_prm_txsta_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: sta_arm_mess
BitField Type: R/W
BitField Desc: use for detect new message
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_prm_txsta_sta_arm_mess_Bit_Start                                                           4
#define cAf6_upen_prm_txsta_sta_arm_mess_Bit_End                                                             4
#define cAf6_upen_prm_txsta_sta_arm_mess_Mask                                                            cBit4
#define cAf6_upen_prm_txsta_sta_arm_mess_Shift                                                               4
#define cAf6_upen_prm_txsta_sta_arm_mess_MaxVal                                                            0x1
#define cAf6_upen_prm_txsta_sta_arm_mess_MinVal                                                            0x0
#define cAf6_upen_prm_txsta_sta_arm_mess_RstVal                                                            0x0

/*--------------------------------------
BitField Name: cnt_prm_lenmess
BitField Type: R/W
BitField Desc: count number of byte in message
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_upen_prm_txsta_cnt_prm_lenmess_Bit_Start                                                        0
#define cAf6_upen_prm_txsta_cnt_prm_lenmess_Bit_End                                                          3
#define cAf6_upen_prm_txsta_cnt_prm_lenmess_Mask                                                       cBit3_0
#define cAf6_upen_prm_txsta_cnt_prm_lenmess_Shift                                                            0
#define cAf6_upen_prm_txsta_cnt_prm_lenmess_MaxVal                                                         0xf
#define cAf6_upen_prm_txsta_cnt_prm_lenmess_MinVal                                                         0x0
#define cAf6_upen_prm_txsta_cnt_prm_lenmess_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : Status process MDL STUFF
Reg Addr   : 0x2800 - 0x282F
Reg Formula: 0x2800+$MDLID
    Where  :
           + $MDLID (0-47) : DE3 ID
Reg Desc   :
Status process MDL data

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mdl_txsta_stuff_Base                                                               0x2800
#define cAf6Reg_upen_mdl_txsta_stuff(MDLID)                                                   (0x2800+(MDLID))
#define cAf6Reg_upen_mdl_txsta_stuff_WidthVal                                                               64
#define cAf6Reg_upen_mdl_txsta_stuff_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: sta1_de3_enstuff
BitField Type: R/W
BitField Desc: status 1 for enstuff byte
BitField Bits: [35:20]
--------------------------------------*/
#define cAf6_upen_mdl_txsta_stuff_sta1_de3_enstuff_Bit_Start                                                20
#define cAf6_upen_mdl_txsta_stuff_sta1_de3_enstuff_Bit_End                                                  35
#define cAf6_upen_mdl_txsta_stuff_sta1_de3_enstuff_Mask_01                                           cBit31_20
#define cAf6_upen_mdl_txsta_stuff_sta1_de3_enstuff_Shift_01                                                 20
#define cAf6_upen_mdl_txsta_stuff_sta1_de3_enstuff_Mask_02                                             cBit3_0
#define cAf6_upen_mdl_txsta_stuff_sta1_de3_enstuff_Shift_02                                                  0

/*--------------------------------------
BitField Name: sta0_de3_enstuff
BitField Type: R/W
BitField Desc: status 0 for enstuff byte
BitField Bits: [19:00]
--------------------------------------*/
#define cAf6_upen_mdl_txsta_stuff_sta0_de3_enstuff_Bit_Start                                                 0
#define cAf6_upen_mdl_txsta_stuff_sta0_de3_enstuff_Bit_End                                                  19
#define cAf6_upen_mdl_txsta_stuff_sta0_de3_enstuff_Mask                                               cBit19_0
#define cAf6_upen_mdl_txsta_stuff_sta0_de3_enstuff_Shift                                                     0
#define cAf6_upen_mdl_txsta_stuff_sta0_de3_enstuff_MaxVal                                              0xfffff
#define cAf6_upen_mdl_txsta_stuff_sta0_de3_enstuff_MinVal                                                  0x0
#define cAf6_upen_mdl_txsta_stuff_sta0_de3_enstuff_RstVal                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG CONTROL STUFF 0
Reg Addr   : 0x3000
Reg Formula:
    Where  : 
Reg Desc   : 
config control Stuff global

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_ctrl0_Base                                                                     0x3000
#define cAf6Reg_upen_cfg_ctrl0                                                                          0x3000
#define cAf6Reg_upen_cfg_ctrl0_WidthVal                                                                     32
#define cAf6Reg_upen_cfg_ctrl0_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: out_txcfg_ctrl0
BitField Type: R/W
BitField Desc: reserve
BitField Bits: [31:08]
--------------------------------------*/
#define cAf6_upen_cfg_ctrl0_out_txcfg_ctrl0_Bit_Start                                                        8
#define cAf6_upen_cfg_ctrl0_out_txcfg_ctrl0_Bit_End                                                         31
#define cAf6_upen_cfg_ctrl0_out_txcfg_ctrl0_Mask                                                      cBit31_8
#define cAf6_upen_cfg_ctrl0_out_txcfg_ctrl0_Shift                                                            8
#define cAf6_upen_cfg_ctrl0_out_txcfg_ctrl0_MaxVal                                                    0xffffff
#define cAf6_upen_cfg_ctrl0_out_txcfg_ctrl0_MinVal                                                         0x0
#define cAf6_upen_cfg_ctrl0_out_txcfg_ctrl0_RstVal                                                         0x0

/*--------------------------------------
BitField Name: reserve1
BitField Type: R/W
BitField Desc: reserve1
BitField Bits: [06:02]
--------------------------------------*/
#define cAf6_upen_cfg_ctrl0_reserve1_Bit_Start                                                               2
#define cAf6_upen_cfg_ctrl0_reserve1_Bit_End                                                                 6
#define cAf6_upen_cfg_ctrl0_reserve1_Mask                                                              cBit6_2
#define cAf6_upen_cfg_ctrl0_reserve1_Shift                                                                   2
#define cAf6_upen_cfg_ctrl0_reserve1_MaxVal                                                               0x1f
#define cAf6_upen_cfg_ctrl0_reserve1_MinVal                                                                0x0
#define cAf6_upen_cfg_ctrl0_reserve1_RstVal                                                                0x0

/*--------------------------------------
BitField Name: fcsinscfg
BitField Type: R/W
BitField Desc: (1) enable insert FCS, (0) disable
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_cfg_ctrl0_fcsinscfg_Bit_Start                                                              1
#define cAf6_upen_cfg_ctrl0_fcsinscfg_Bit_End                                                                1
#define cAf6_upen_cfg_ctrl0_fcsinscfg_Mask                                                               cBit1
#define cAf6_upen_cfg_ctrl0_fcsinscfg_Shift                                                                  1
#define cAf6_upen_cfg_ctrl0_fcsinscfg_MaxVal                                                               0x1
#define cAf6_upen_cfg_ctrl0_fcsinscfg_MinVal                                                               0x0
#define cAf6_upen_cfg_ctrl0_fcsinscfg_RstVal                                                               0x1


/*------------------------------------------------------------------------------
Reg Name   : COUNTER BYTE MESSAGE TX PRM
Reg Addr   : 0x4000 - 0x4D3F
Reg Formula: 0x4000+$STSID[4:3]*7 + $VTGID*64 + $SLICEID*32 + $STSID[2:0]*4 + $VTID + $UPRO * 2048 
    Where  : 
           + $STSID (0-23) : STS ID
           + $VTGID (0-6) : VTG ID
           + $SLICEID (0-1) : SLICE ID
           + $VTID (0-3) : VT ID
           + $UPRO (0-1) : upen read only
Reg Desc   :
counter byte message PRM

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txprm_cnt_byte_Base                                                                0x4000
#define cAf6Reg_upen_txprm_cnt_byte(STSID, VTGID, SLICEID, VTID, UPRO)(0x4000+(STSID)[4:3]*7+(VTGID)*64+(SLICEID)*32+(STSID)[2:0]*4+(VTID)+(UPRO)*2048)
#define cAf6Reg_upen_txprm_cnt_byte_WidthVal                                                                32
#define cAf6Reg_upen_txprm_cnt_byte_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: cnt_byte_prm
BitField Type: R/W
BitField Desc: value counter byte
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txprm_cnt_byte_cnt_byte_prm_Bit_Start                                                      0
#define cAf6_upen_txprm_cnt_byte_cnt_byte_prm_Bit_End                                                       31
#define cAf6_upen_txprm_cnt_byte_cnt_byte_prm_Mask                                                    cBit31_0
#define cAf6_upen_txprm_cnt_byte_cnt_byte_prm_Shift                                                          0
#define cAf6_upen_txprm_cnt_byte_cnt_byte_prm_MaxVal                                                0xffffffff
#define cAf6_upen_txprm_cnt_byte_cnt_byte_prm_MinVal                                                       0x0
#define cAf6_upen_txprm_cnt_byte_cnt_byte_prm_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER PACKET MESSAGE TX PRM
Reg Addr   : 0x5000 - 0x5D3F
Reg Formula: 0x5000+$STSID[4:3]*7 + $VTGID*64 + $SLICEID*32 + $STSID[2:0]*4 + $VTID + $UPRO * 2048 
    Where  : 
           + $STSID (0-23) : STS ID
           + $VTGID (0-6) : VTG ID
           + $SLICEID (0-1) : SLICE ID
           + $VTID (0-3) : VT ID
           + $UPRO (0-1) : upen read only
Reg Desc   : 
counter packet message PRM

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txprm_cnt_pkt_Base                                                                 0x5000
#define cAf6Reg_upen_txprm_cnt_pkt(STSID, VTGID, SLICEID, VTID, UPRO) (0x5000+(STSID)[4:3]*7+(VTGID)*64+(SLICEID)*32+(STSID)[2:0]*4+(VTID)+(UPRO)*2048)
#define cAf6Reg_upen_txprm_cnt_pkt_WidthVal                                                                 32
#define cAf6Reg_upen_txprm_cnt_pkt_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: cnt_pkt_prm
BitField Type: R/W
BitField Desc: value counter packet
BitField Bits: [14:00]
--------------------------------------*/
#define cAf6_upen_txprm_cnt_pkt_cnt_pkt_prm_Bit_Start                                                        0
#define cAf6_upen_txprm_cnt_pkt_cnt_pkt_prm_Bit_End                                                         14
#define cAf6_upen_txprm_cnt_pkt_cnt_pkt_prm_Mask                                                      cBit14_0
#define cAf6_upen_txprm_cnt_pkt_cnt_pkt_prm_Shift                                                            0
#define cAf6_upen_txprm_cnt_pkt_cnt_pkt_prm_MaxVal                                                      0x7fff
#define cAf6_upen_txprm_cnt_pkt_cnt_pkt_prm_MinVal                                                         0x0
#define cAf6_upen_txprm_cnt_pkt_cnt_pkt_prm_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER VALID INFO TX PRM
Reg Addr   : 0x6000 - 0x6D3F
Reg Formula: 0x6000+$STSID[4:3]*7 + $VTGID*64 + $SLICEID*32 + $STSID[2:0]*4 + $VTID + $UPRO * 2048 
    Where  : 
           + $STSID (0-23) : STS ID
           + $VTGID (0-6) : VTG ID
           + $SLICEID (0-1) : SLICE ID
           + $VTID (0-3) : VT ID
           + $UPRO (0-1) : upen read only
Reg Desc   : 
counter valid info PRM

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txprm_cnt_info_Base                                                                0x6000
#define cAf6Reg_upen_txprm_cnt_info(STSID, VTGID, SLICEID, VTID, UPRO)(0x6000+(STSID)[4:3]*7+(VTGID)*64+(SLICEID)*32+(STSID)[2:0]*4+(VTID)+(UPRO)*2048)
#define cAf6Reg_upen_txprm_cnt_info_WidthVal                                                                32
#define cAf6Reg_upen_txprm_cnt_info_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: cnt_vld_prm_info
BitField Type: R/W
BitField Desc: value counter valid info
BitField Bits: [14:00]
--------------------------------------*/
#define cAf6_upen_txprm_cnt_info_cnt_vld_prm_info_Bit_Start                                                  0
#define cAf6_upen_txprm_cnt_info_cnt_vld_prm_info_Bit_End                                                   14
#define cAf6_upen_txprm_cnt_info_cnt_vld_prm_info_Mask                                                cBit14_0
#define cAf6_upen_txprm_cnt_info_cnt_vld_prm_info_Shift                                                      0
#define cAf6_upen_txprm_cnt_info_cnt_vld_prm_info_MaxVal                                                0x7fff
#define cAf6_upen_txprm_cnt_info_cnt_vld_prm_info_MinVal                                                   0x0
#define cAf6_upen_txprm_cnt_info_cnt_vld_prm_info_RstVal                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : Buff Message MDL IDLE1
Reg Addr   : 0x0000 - 0x012F
Reg Formula: 0x0000+ $DWORDID*16 + $DE3ID1
    Where  :
           + $DWORDID (0-18) : DWORD ID
           + $DE3ID1(0-15)  : DE3 ID1
Reg Desc   :
config message MDL IDLE Channel ID 0-15

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mdl_idle1_Base                                                                     0x0000
#define cAf6Reg_upen_mdl_idle1(DWORDID, DE3ID1)                                 (0x0000+(DWORDID)*16+(DE3ID1))
#define cAf6Reg_upen_mdl_idle1_WidthVal                                                                     32
#define cAf6Reg_upen_mdl_idle1_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: idle_byte13
BitField Type: R/W
BitField Desc: MS BYTE
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_upen_mdl_idle1_idle_byte13_Bit_Start                                                           24
#define cAf6_upen_mdl_idle1_idle_byte13_Bit_End                                                             31
#define cAf6_upen_mdl_idle1_idle_byte13_Mask                                                         cBit31_24
#define cAf6_upen_mdl_idle1_idle_byte13_Shift                                                               24
#define cAf6_upen_mdl_idle1_idle_byte13_MaxVal                                                            0xff
#define cAf6_upen_mdl_idle1_idle_byte13_MinVal                                                             0x0
#define cAf6_upen_mdl_idle1_idle_byte13_RstVal                                                             0x0

/*--------------------------------------
BitField Name: ilde_byte12
BitField Type: R/W
BitField Desc: BYTE 2
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_upen_mdl_idle1_ilde_byte12_Bit_Start                                                           16
#define cAf6_upen_mdl_idle1_ilde_byte12_Bit_End                                                             23
#define cAf6_upen_mdl_idle1_ilde_byte12_Mask                                                         cBit23_16
#define cAf6_upen_mdl_idle1_ilde_byte12_Shift                                                               16
#define cAf6_upen_mdl_idle1_ilde_byte12_MaxVal                                                            0xff
#define cAf6_upen_mdl_idle1_ilde_byte12_MinVal                                                             0x0
#define cAf6_upen_mdl_idle1_ilde_byte12_RstVal                                                             0x0

/*--------------------------------------
BitField Name: idle_byte11
BitField Type: R/W
BitField Desc: BYTE 1
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_upen_mdl_idle1_idle_byte11_Bit_Start                                                            8
#define cAf6_upen_mdl_idle1_idle_byte11_Bit_End                                                             15
#define cAf6_upen_mdl_idle1_idle_byte11_Mask                                                          cBit15_8
#define cAf6_upen_mdl_idle1_idle_byte11_Shift                                                                8
#define cAf6_upen_mdl_idle1_idle_byte11_MaxVal                                                            0xff
#define cAf6_upen_mdl_idle1_idle_byte11_MinVal                                                             0x0
#define cAf6_upen_mdl_idle1_idle_byte11_RstVal                                                             0x0

/*--------------------------------------
BitField Name: idle_byte10
BitField Type: R/W
BitField Desc: LS BYTE
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_upen_mdl_idle1_idle_byte10_Bit_Start                                                            0
#define cAf6_upen_mdl_idle1_idle_byte10_Bit_End                                                              7
#define cAf6_upen_mdl_idle1_idle_byte10_Mask                                                           cBit7_0
#define cAf6_upen_mdl_idle1_idle_byte10_Shift                                                                0
#define cAf6_upen_mdl_idle1_idle_byte10_MaxVal                                                            0xff
#define cAf6_upen_mdl_idle1_idle_byte10_MinVal                                                             0x0
#define cAf6_upen_mdl_idle1_idle_byte10_RstVal                                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : Buff Message MDL IDLE2
Reg Addr   : 0x0130 - 0x01C7
Reg Formula: 0x0130+ $DWORDID*8 + $DE3ID2
    Where  : 
           + $DWORDID (0-18) : DWORD ID
           + $DE3ID2 (0-7) : DE3 ID2
Reg Desc   : 
config message MDL IDLE Channel ID 16-23

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mdl_idle2_Base                                                                     0x0130
#define cAf6Reg_upen_mdl_idle2(DWORDID, DE3ID2)                                  (0x0130+(DWORDID)*8+(DE3ID2))
#define cAf6Reg_upen_mdl_idle2_WidthVal                                                                     32
#define cAf6Reg_upen_mdl_idle2_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: idle_byte23
BitField Type: R/W
BitField Desc: MS BYTE
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_upen_mdl_idle2_idle_byte23_Bit_Start                                                           24
#define cAf6_upen_mdl_idle2_idle_byte23_Bit_End                                                             31
#define cAf6_upen_mdl_idle2_idle_byte23_Mask                                                         cBit31_24
#define cAf6_upen_mdl_idle2_idle_byte23_Shift                                                               24
#define cAf6_upen_mdl_idle2_idle_byte23_MaxVal                                                            0xff
#define cAf6_upen_mdl_idle2_idle_byte23_MinVal                                                             0x0
#define cAf6_upen_mdl_idle2_idle_byte23_RstVal                                                             0x0

/*--------------------------------------
BitField Name: ilde_byte22
BitField Type: R/W
BitField Desc: BYTE 2
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_upen_mdl_idle2_ilde_byte22_Bit_Start                                                           16
#define cAf6_upen_mdl_idle2_ilde_byte22_Bit_End                                                             23
#define cAf6_upen_mdl_idle2_ilde_byte22_Mask                                                         cBit23_16
#define cAf6_upen_mdl_idle2_ilde_byte22_Shift                                                               16
#define cAf6_upen_mdl_idle2_ilde_byte22_MaxVal                                                            0xff
#define cAf6_upen_mdl_idle2_ilde_byte22_MinVal                                                             0x0
#define cAf6_upen_mdl_idle2_ilde_byte22_RstVal                                                             0x0

/*--------------------------------------
BitField Name: idle_byte21
BitField Type: R/W
BitField Desc: BYTE 1
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_upen_mdl_idle2_idle_byte21_Bit_Start                                                            8
#define cAf6_upen_mdl_idle2_idle_byte21_Bit_End                                                             15
#define cAf6_upen_mdl_idle2_idle_byte21_Mask                                                          cBit15_8
#define cAf6_upen_mdl_idle2_idle_byte21_Shift                                                                8
#define cAf6_upen_mdl_idle2_idle_byte21_MaxVal                                                            0xff
#define cAf6_upen_mdl_idle2_idle_byte21_MinVal                                                             0x0
#define cAf6_upen_mdl_idle2_idle_byte21_RstVal                                                             0x0

/*--------------------------------------
BitField Name: idle_byte20
BitField Type: R/W
BitField Desc: LS BYTE
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_upen_mdl_idle2_idle_byte20_Bit_Start                                                            0
#define cAf6_upen_mdl_idle2_idle_byte20_Bit_End                                                              7
#define cAf6_upen_mdl_idle2_idle_byte20_Mask                                                           cBit7_0
#define cAf6_upen_mdl_idle2_idle_byte20_Shift                                                                0
#define cAf6_upen_mdl_idle2_idle_byte20_MaxVal                                                            0xff
#define cAf6_upen_mdl_idle2_idle_byte20_MinVal                                                             0x0
#define cAf6_upen_mdl_idle2_idle_byte20_RstVal                                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : Config Buff Message MDL TESTPATH1
Reg Addr   : 0x0200 - 0x032F
Reg Formula: 0x0200+$DE3ID1 + $DWORDID*16
    Where  :
           + $DWORDID (0-18) : DWORD ID
           + $DE3ID1(0-15)  : DS3 E3 ID
Reg Desc   :
config message MDL TESTPATH Channel ID 0-15

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mdl_tp1_Base                                                                       0x0200
#define cAf6Reg_upen_mdl_tp1(DWORDID, DE3ID1)                                   (0x0200+(DE3ID1)+(DWORDID)*16)
#define cAf6Reg_upen_mdl_tp1_WidthVal                                                                       32
#define cAf6Reg_upen_mdl_tp1_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: tp_byte13
BitField Type: R/W
BitField Desc: MS BYTE
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_upen_mdl_tp1_tp_byte13_Bit_Start                                                               24
#define cAf6_upen_mdl_tp1_tp_byte13_Bit_End                                                                 31
#define cAf6_upen_mdl_tp1_tp_byte13_Mask                                                             cBit31_24
#define cAf6_upen_mdl_tp1_tp_byte13_Shift                                                                   24
#define cAf6_upen_mdl_tp1_tp_byte13_MaxVal                                                                0xff
#define cAf6_upen_mdl_tp1_tp_byte13_MinVal                                                                 0x0
#define cAf6_upen_mdl_tp1_tp_byte13_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: tp_byte12
BitField Type: R/W
BitField Desc: BYTE 2
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_upen_mdl_tp1_tp_byte12_Bit_Start                                                               16
#define cAf6_upen_mdl_tp1_tp_byte12_Bit_End                                                                 23
#define cAf6_upen_mdl_tp1_tp_byte12_Mask                                                             cBit23_16
#define cAf6_upen_mdl_tp1_tp_byte12_Shift                                                                   16
#define cAf6_upen_mdl_tp1_tp_byte12_MaxVal                                                                0xff
#define cAf6_upen_mdl_tp1_tp_byte12_MinVal                                                                 0x0
#define cAf6_upen_mdl_tp1_tp_byte12_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: tp_byte11
BitField Type: R/W
BitField Desc: BYTE 1
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_upen_mdl_tp1_tp_byte11_Bit_Start                                                                8
#define cAf6_upen_mdl_tp1_tp_byte11_Bit_End                                                                 15
#define cAf6_upen_mdl_tp1_tp_byte11_Mask                                                              cBit15_8
#define cAf6_upen_mdl_tp1_tp_byte11_Shift                                                                    8
#define cAf6_upen_mdl_tp1_tp_byte11_MaxVal                                                                0xff
#define cAf6_upen_mdl_tp1_tp_byte11_MinVal                                                                 0x0
#define cAf6_upen_mdl_tp1_tp_byte11_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: tp_byte10
BitField Type: R/W
BitField Desc: LS BYTE
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_upen_mdl_tp1_tp_byte10_Bit_Start                                                                0
#define cAf6_upen_mdl_tp1_tp_byte10_Bit_End                                                                  7
#define cAf6_upen_mdl_tp1_tp_byte10_Mask                                                               cBit7_0
#define cAf6_upen_mdl_tp1_tp_byte10_Shift                                                                    0
#define cAf6_upen_mdl_tp1_tp_byte10_MaxVal                                                                0xff
#define cAf6_upen_mdl_tp1_tp_byte10_MinVal                                                                 0x0
#define cAf6_upen_mdl_tp1_tp_byte10_RstVal                                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : Config Buff Message MDL TESTPATH2
Reg Addr   : 0x0330 - 0x03C7
Reg Formula: 0x0330+ $DWORDID*8+ $DE3ID2
    Where  : 
           + $DWORDID (0-18) : DWORD ID
           + $DE3ID2 (0-7) : DE3 ID2
Reg Desc   : 
config message MDL TESTPATH Channel ID 16-23

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mdl_tp2_Base                                                                       0x0330
#define cAf6Reg_upen_mdl_tp2(DWORDID, DE3ID2)                                    (0x0330+(DWORDID)*8+(DE3ID2))
#define cAf6Reg_upen_mdl_tp2_WidthVal                                                                       32
#define cAf6Reg_upen_mdl_tp2_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: tp_byte23
BitField Type: R/W
BitField Desc: MS BYTE
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_upen_mdl_tp2_tp_byte23_Bit_Start                                                               24
#define cAf6_upen_mdl_tp2_tp_byte23_Bit_End                                                                 31
#define cAf6_upen_mdl_tp2_tp_byte23_Mask                                                             cBit31_24
#define cAf6_upen_mdl_tp2_tp_byte23_Shift                                                                   24
#define cAf6_upen_mdl_tp2_tp_byte23_MaxVal                                                                0xff
#define cAf6_upen_mdl_tp2_tp_byte23_MinVal                                                                 0x0
#define cAf6_upen_mdl_tp2_tp_byte23_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: tp_byte22
BitField Type: R/W
BitField Desc: BYTE 2
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_upen_mdl_tp2_tp_byte22_Bit_Start                                                               16
#define cAf6_upen_mdl_tp2_tp_byte22_Bit_End                                                                 23
#define cAf6_upen_mdl_tp2_tp_byte22_Mask                                                             cBit23_16
#define cAf6_upen_mdl_tp2_tp_byte22_Shift                                                                   16
#define cAf6_upen_mdl_tp2_tp_byte22_MaxVal                                                                0xff
#define cAf6_upen_mdl_tp2_tp_byte22_MinVal                                                                 0x0
#define cAf6_upen_mdl_tp2_tp_byte22_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: tp_byte21
BitField Type: R/W
BitField Desc: BYTE 1
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_upen_mdl_tp2_tp_byte21_Bit_Start                                                                8
#define cAf6_upen_mdl_tp2_tp_byte21_Bit_End                                                                 15
#define cAf6_upen_mdl_tp2_tp_byte21_Mask                                                              cBit15_8
#define cAf6_upen_mdl_tp2_tp_byte21_Shift                                                                    8
#define cAf6_upen_mdl_tp2_tp_byte21_MaxVal                                                                0xff
#define cAf6_upen_mdl_tp2_tp_byte21_MinVal                                                                 0x0
#define cAf6_upen_mdl_tp2_tp_byte21_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: tp_byte20
BitField Type: R/W
BitField Desc: LS BYTE
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_upen_mdl_tp2_tp_byte20_Bit_Start                                                                0
#define cAf6_upen_mdl_tp2_tp_byte20_Bit_End                                                                  7
#define cAf6_upen_mdl_tp2_tp_byte20_Mask                                                               cBit7_0
#define cAf6_upen_mdl_tp2_tp_byte20_Shift                                                                    0
#define cAf6_upen_mdl_tp2_tp_byte20_MaxVal                                                                0xff
#define cAf6_upen_mdl_tp2_tp_byte20_MinVal                                                                 0x0
#define cAf6_upen_mdl_tp2_tp_byte20_RstVal                                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : Status process MDL data
Reg Addr   : 0x0400 - 0x0417
Reg Formula: 0x0400+$DE3ID
    Where  : 
           + $DE3ID (0-23) : DE3 ID
Reg Desc   : 
Status process MDL data

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mdl_txsta_Base                                                                     0x0400
#define cAf6Reg_upen_mdl_txsta(DE3ID)                                                         (0x0400+(DE3ID))
#define cAf6Reg_upen_mdl_txsta_WidthVal                                                                     32
#define cAf6Reg_upen_mdl_txsta_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: vld_page
BitField Type: R/W
BitField Desc: valid page
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_upen_mdl_txsta_vld_page_Bit_Start                                                              10
#define cAf6_upen_mdl_txsta_vld_page_Bit_End                                                                10
#define cAf6_upen_mdl_txsta_vld_page_Mask                                                               cBit10
#define cAf6_upen_mdl_txsta_vld_page_Shift                                                                  10
#define cAf6_upen_mdl_txsta_vld_page_MaxVal                                                                0x1
#define cAf6_upen_mdl_txsta_vld_page_MinVal                                                                0x0
#define cAf6_upen_mdl_txsta_vld_page_RstVal                                                                0x0

/*--------------------------------------
BitField Name: sta_cnt_mess1s
BitField Type: R/W
BitField Desc: use for count MDL message to sent in 1s
BitField Bits: [09:08]
--------------------------------------*/
#define cAf6_upen_mdl_txsta_sta_cnt_mess1s_Bit_Start                                                         8
#define cAf6_upen_mdl_txsta_sta_cnt_mess1s_Bit_End                                                           9
#define cAf6_upen_mdl_txsta_sta_cnt_mess1s_Mask                                                        cBit9_8
#define cAf6_upen_mdl_txsta_sta_cnt_mess1s_Shift                                                             8
#define cAf6_upen_mdl_txsta_sta_cnt_mess1s_MaxVal                                                          0x3
#define cAf6_upen_mdl_txsta_sta_cnt_mess1s_MinVal                                                          0x0
#define cAf6_upen_mdl_txsta_sta_cnt_mess1s_RstVal                                                          0x0

/*--------------------------------------
BitField Name: sta_alrm_1s
BitField Type: R/W
BitField Desc: use for detecting new sending message
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_upen_mdl_txsta_sta_alrm_1s_Bit_Start                                                            7
#define cAf6_upen_mdl_txsta_sta_alrm_1s_Bit_End                                                              7
#define cAf6_upen_mdl_txsta_sta_alrm_1s_Mask                                                             cBit7
#define cAf6_upen_mdl_txsta_sta_alrm_1s_Shift                                                                7
#define cAf6_upen_mdl_txsta_sta_alrm_1s_MaxVal                                                             0x1
#define cAf6_upen_mdl_txsta_sta_alrm_1s_MinVal                                                             0x0
#define cAf6_upen_mdl_txsta_sta_alrm_1s_RstVal                                                             0x0

/*--------------------------------------
BitField Name: cnt_mdl_lenmess
BitField Type: R/W
BitField Desc: count number of byte in message
BitField Bits: [6:0]
--------------------------------------*/
#define cAf6_upen_mdl_txsta_cnt_mdl_lenmess_Bit_Start                                                        0
#define cAf6_upen_mdl_txsta_cnt_mdl_lenmess_Bit_End                                                          6
#define cAf6_upen_mdl_txsta_cnt_mdl_lenmess_Mask                                                       cBit6_0
#define cAf6_upen_mdl_txsta_cnt_mdl_lenmess_Shift                                                            0
#define cAf6_upen_mdl_txsta_cnt_mdl_lenmess_MaxVal                                                        0x7f
#define cAf6_upen_mdl_txsta_cnt_mdl_lenmess_MinVal                                                         0x0
#define cAf6_upen_mdl_txsta_cnt_mdl_lenmess_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : SET TO HAVE PACKET MDL BUFFER 1
Reg Addr   : 0x0420 - 0x437
Reg Formula: 0x0420+$DE3ID
    Where  :
           + $DE3ID (0-23) : DE3 ID
Reg Desc   :
SET/CLEAR to ALRM STATUS MESSAGE in BUFFER 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_sta_idle_alren_Base                                                                0x0420
#define cAf6Reg_upen_sta_idle_alren(DE3ID)                                                    (0x0420+(DE3ID))
#define cAf6Reg_upen_sta_idle_alren_WidthVal                                                                32
#define cAf6Reg_upen_sta_idle_alren_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: idle_cfgen
BitField Type: R/W
BitField Desc: (0) : engine clear for indication to have sent, (1) CPU set for
indication to have new message which must send for buffer 0
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_sta_idle_alren_idle_cfgen_Bit_Start                                                        0
#define cAf6_upen_sta_idle_alren_idle_cfgen_Bit_End                                                          0
#define cAf6_upen_sta_idle_alren_idle_cfgen_Mask                                                         cBit0
#define cAf6_upen_sta_idle_alren_idle_cfgen_Shift                                                            0
#define cAf6_upen_sta_idle_alren_idle_cfgen_MaxVal                                                         0x1
#define cAf6_upen_sta_idle_alren_idle_cfgen_MinVal                                                         0x0
#define cAf6_upen_sta_idle_alren_idle_cfgen_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : SET TO HAVE PACKET MDL BUFFER 2
Reg Addr   : 0x0440 - 0x457
Reg Formula: 0x0440+$DE3ID
    Where  :
           + $DE3ID (0-23) : DE3 ID
Reg Desc   :
SET/CLEAR to ALRM STATUS MESSAGE in BUFFER 2

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_sta_tp_alren_Base                                                                  0x0440
#define cAf6Reg_upen_sta_tp_alren(DE3ID)                                                      (0x0440+(DE3ID))
#define cAf6Reg_upen_sta_tp_alren_WidthVal                                                                  32
#define cAf6Reg_upen_sta_tp_alren_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: tp_cfgen
BitField Type: R/W
BitField Desc: (0) : engine clear for indication to have sent, (1) CPU set for
indication to have new message which must send for buffer 1
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_sta_tp_alren_tp_cfgen_Bit_Start                                                            0
#define cAf6_upen_sta_tp_alren_tp_cfgen_Bit_End                                                              0
#define cAf6_upen_sta_tp_alren_tp_cfgen_Mask                                                             cBit0
#define cAf6_upen_sta_tp_alren_tp_cfgen_Shift                                                                0
#define cAf6_upen_sta_tp_alren_tp_cfgen_MaxVal                                                             0x1
#define cAf6_upen_sta_tp_alren_tp_cfgen_MinVal                                                             0x0
#define cAf6_upen_sta_tp_alren_tp_cfgen_RstVal                                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG MDL Tx
Reg Addr   : 0x0460 - 0x477
Reg Formula: 0x0460+$DE3ID
    Where  : 
           + $DE3ID (0-23) : DE3 ID
Reg Desc   : 
Config Tx MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_mdl_Base                                                                       0x0460
#define cAf6Reg_upen_cfg_mdl(DE3ID)                                                           (0x0460+(DE3ID))
#define cAf6Reg_upen_cfg_mdl_WidthVal                                                                       32
#define cAf6Reg_upen_cfg_mdl_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: cfg_seq_tx
BitField Type: R/W
BitField Desc: config enable Tx continous, (0) is disable, (1) is enable
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_cfg_mdl_cfg_seq_tx_Bit_Start                                                               4
#define cAf6_upen_cfg_mdl_cfg_seq_tx_Bit_End                                                                 4
#define cAf6_upen_cfg_mdl_cfg_seq_tx_Mask                                                                cBit4
#define cAf6_upen_cfg_mdl_cfg_seq_tx_Shift                                                                   4
#define cAf6_upen_cfg_mdl_cfg_seq_tx_MaxVal                                                                0x1
#define cAf6_upen_cfg_mdl_cfg_seq_tx_MinVal                                                                0x0
#define cAf6_upen_cfg_mdl_cfg_seq_tx_RstVal                                                                0x0

/*--------------------------------------
BitField Name: cfg_entx
BitField Type: R/W
BitField Desc: config enable Tx, (0) is disable, (1) is enable
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_cfg_mdl_cfg_entx_Bit_Start                                                                 3
#define cAf6_upen_cfg_mdl_cfg_entx_Bit_End                                                                   3
#define cAf6_upen_cfg_mdl_cfg_entx_Mask                                                                  cBit3
#define cAf6_upen_cfg_mdl_cfg_entx_Shift                                                                     3
#define cAf6_upen_cfg_mdl_cfg_entx_MaxVal                                                                  0x1
#define cAf6_upen_cfg_mdl_cfg_entx_MinVal                                                                  0x0
#define cAf6_upen_cfg_mdl_cfg_entx_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: cfg_fcs_tx
BitField Type: R/W
BitField Desc: config mode transmit FCS, (1) is T.403-MSB, (0) is T.107-LSB
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_cfg_mdl_cfg_fcs_tx_Bit_Start                                                               2
#define cAf6_upen_cfg_mdl_cfg_fcs_tx_Bit_End                                                                 2
#define cAf6_upen_cfg_mdl_cfg_fcs_tx_Mask                                                                cBit2
#define cAf6_upen_cfg_mdl_cfg_fcs_tx_Shift                                                                   2
#define cAf6_upen_cfg_mdl_cfg_fcs_tx_MaxVal                                                                0x1
#define cAf6_upen_cfg_mdl_cfg_fcs_tx_MinVal                                                                0x0
#define cAf6_upen_cfg_mdl_cfg_fcs_tx_RstVal                                                                0x0

/*--------------------------------------
BitField Name: cfg_mdlstd_tx
BitField Type: R/W
BitField Desc: config standard Tx, (0) is ANSI, (1) is AT&T
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_cfg_mdl_cfg_mdlstd_tx_Bit_Start                                                            1
#define cAf6_upen_cfg_mdl_cfg_mdlstd_tx_Bit_End                                                              1
#define cAf6_upen_cfg_mdl_cfg_mdlstd_tx_Mask                                                             cBit1
#define cAf6_upen_cfg_mdl_cfg_mdlstd_tx_Shift                                                                1
#define cAf6_upen_cfg_mdl_cfg_mdlstd_tx_MaxVal                                                             0x1
#define cAf6_upen_cfg_mdl_cfg_mdlstd_tx_MinVal                                                             0x0
#define cAf6_upen_cfg_mdl_cfg_mdlstd_tx_RstVal                                                             0x0

/*--------------------------------------
BitField Name: cfg_mdl_cr
BitField Type: R/W
BitField Desc: config bit command/respond MDL message, bit 0 is channel 0 of DS3
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_cfg_mdl_cfg_mdl_cr_Bit_Start                                                               0
#define cAf6_upen_cfg_mdl_cfg_mdl_cr_Bit_End                                                                 0
#define cAf6_upen_cfg_mdl_cfg_mdl_cr_Mask                                                                cBit0
#define cAf6_upen_cfg_mdl_cfg_mdl_cr_Shift                                                                   0
#define cAf6_upen_cfg_mdl_cfg_mdl_cr_MaxVal                                                                0x1
#define cAf6_upen_cfg_mdl_cfg_mdl_cr_MinVal                                                                0x0
#define cAf6_upen_cfg_mdl_cfg_mdl_cr_RstVal                                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER BYTE MESSAGE TX MDL IDLE R2C 1
Reg Addr   : 0x0700 - 0x0717
Reg Formula: 0x0700+ $DE3ID
    Where  : 
           + $DE3ID (0-23) : DE3 ID
Reg Desc   : 
counter byte read to clear IDLE message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txmdl_cntr2c_byteidle1_Base                                                        0x0700
#define cAf6Reg_upen_txmdl_cntr2c_byteidle1(DE3ID)                                            (0x0700+(DE3ID))
#define cAf6Reg_upen_txmdl_cntr2c_byteidle1_WidthVal                                                        32
#define cAf6Reg_upen_txmdl_cntr2c_byteidle1_WriteMask                                                      0x0

/*--------------------------------------
BitField Name: cntr2c_byte_idle_mdl
BitField Type: R/W
BitField Desc: value counter byte
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txmdl_cntr2c_byteidle1_cntr2c_byte_idle_mdl_Bit_Start                                       0
#define cAf6_upen_txmdl_cntr2c_byteidle1_cntr2c_byte_idle_mdl_Bit_End                                       31
#define cAf6_upen_txmdl_cntr2c_byteidle1_cntr2c_byte_idle_mdl_Mask                                    cBit31_0
#define cAf6_upen_txmdl_cntr2c_byteidle1_cntr2c_byte_idle_mdl_Shift                                          0
#define cAf6_upen_txmdl_cntr2c_byteidle1_cntr2c_byte_idle_mdl_MaxVal                                0xffffffff
#define cAf6_upen_txmdl_cntr2c_byteidle1_cntr2c_byte_idle_mdl_MinVal                                       0x0
#define cAf6_upen_txmdl_cntr2c_byteidle1_cntr2c_byte_idle_mdl_RstVal                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER BYTE MESSAGE TX MDL IDLE RO1
Reg Addr   : 0x0780 - 0x0797
Reg Formula: 0x0780+ $DE3ID
    Where  : 
           + $DE3ID (0-23) : DE3 ID
Reg Desc   : 
counter byte read only IDLE message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txmdl_cntro_byteidle1_Base                                                         0x0780
#define cAf6Reg_upen_txmdl_cntro_byteidle1(DE3ID)                                             (0x0780+(DE3ID))
#define cAf6Reg_upen_txmdl_cntro_byteidle1_WidthVal                                                         32
#define cAf6Reg_upen_txmdl_cntro_byteidle1_WriteMask                                                       0x0

/*--------------------------------------
BitField Name: cntro_byte_idle_mdl
BitField Type: R/W
BitField Desc: value counter byte
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txmdl_cntro_byteidle1_cntro_byte_idle_mdl_Bit_Start                                        0
#define cAf6_upen_txmdl_cntro_byteidle1_cntro_byte_idle_mdl_Bit_End                                         31
#define cAf6_upen_txmdl_cntro_byteidle1_cntro_byte_idle_mdl_Mask                                      cBit31_0
#define cAf6_upen_txmdl_cntro_byteidle1_cntro_byte_idle_mdl_Shift                                            0
#define cAf6_upen_txmdl_cntro_byteidle1_cntro_byte_idle_mdl_MaxVal                                  0xffffffff
#define cAf6_upen_txmdl_cntro_byteidle1_cntro_byte_idle_mdl_MinVal                                         0x0
#define cAf6_upen_txmdl_cntro_byteidle1_cntro_byte_idle_mdl_RstVal                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER BYTE MESSAGE TX MDL PATH R2C
Reg Addr   : 0x0720 - 0x0737
Reg Formula: 0x0720+ $DE3ID
    Where  : 
           + $DE3ID (0-23) : DE3 ID
Reg Desc   : 
counter byte read to clear PATH message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txmdl_cntr2c_bytepath_Base                                                         0x0720
#define cAf6Reg_upen_txmdl_cntr2c_bytepath(DE3ID)                                             (0x0720+(DE3ID))
#define cAf6Reg_upen_txmdl_cntr2c_bytepath_WidthVal                                                         32
#define cAf6Reg_upen_txmdl_cntr2c_bytepath_WriteMask                                                       0x0

/*--------------------------------------
BitField Name: cntr2c_byte_path_mdl
BitField Type: R/W
BitField Desc: value counter byte
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txmdl_cntr2c_bytepath_cntr2c_byte_path_mdl_Bit_Start                                       0
#define cAf6_upen_txmdl_cntr2c_bytepath_cntr2c_byte_path_mdl_Bit_End                                        31
#define cAf6_upen_txmdl_cntr2c_bytepath_cntr2c_byte_path_mdl_Mask                                     cBit31_0
#define cAf6_upen_txmdl_cntr2c_bytepath_cntr2c_byte_path_mdl_Shift                                           0
#define cAf6_upen_txmdl_cntr2c_bytepath_cntr2c_byte_path_mdl_MaxVal                                 0xffffffff
#define cAf6_upen_txmdl_cntr2c_bytepath_cntr2c_byte_path_mdl_MinVal                                        0x0
#define cAf6_upen_txmdl_cntr2c_bytepath_cntr2c_byte_path_mdl_RstVal                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER BYTE MESSAGE TX MDL PATH RO
Reg Addr   : 0x07A0 - 0x07B7
Reg Formula: 0x07A0+ $DE3ID
    Where  : 
           + $DE3ID (0-23) : DE3 ID
Reg Desc   : 
counter byte read only PATH message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txmdl_cntro_bytepath_Base                                                          0x07A0
#define cAf6Reg_upen_txmdl_cntro_bytepath(DE3ID)                                              (0x07A0+(DE3ID))
#define cAf6Reg_upen_txmdl_cntro_bytepath_WidthVal                                                          32
#define cAf6Reg_upen_txmdl_cntro_bytepath_WriteMask                                                        0x0

/*--------------------------------------
BitField Name: cntro_byte_path_mdl
BitField Type: R/W
BitField Desc: value counter byte
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txmdl_cntro_bytepath_cntro_byte_path_mdl_Bit_Start                                         0
#define cAf6_upen_txmdl_cntro_bytepath_cntro_byte_path_mdl_Bit_End                                          31
#define cAf6_upen_txmdl_cntro_bytepath_cntro_byte_path_mdl_Mask                                       cBit31_0
#define cAf6_upen_txmdl_cntro_bytepath_cntro_byte_path_mdl_Shift                                             0
#define cAf6_upen_txmdl_cntro_bytepath_cntro_byte_path_mdl_MaxVal                                   0xffffffff
#define cAf6_upen_txmdl_cntro_bytepath_cntro_byte_path_mdl_MinVal                                          0x0
#define cAf6_upen_txmdl_cntro_bytepath_cntro_byte_path_mdl_RstVal                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER BYTE MESSAGE TX MDL TEST R2C
Reg Addr   : 0x0740 - 0x0757
Reg Formula: 0x0740+ $DE3ID
    Where  : 
           + $DE3ID (0-23) : DE3 ID
Reg Desc   : 
counter byte read to clear TEST message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txmdl_cntr2c_bytetest_Base                                                         0x0740
#define cAf6Reg_upen_txmdl_cntr2c_bytetest(DE3ID)                                             (0x0740+(DE3ID))
#define cAf6Reg_upen_txmdl_cntr2c_bytetest_WidthVal                                                         32
#define cAf6Reg_upen_txmdl_cntr2c_bytetest_WriteMask                                                       0x0

/*--------------------------------------
BitField Name: cntr2c_byte_test_mdl
BitField Type: R/W
BitField Desc: value counter byte
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txmdl_cntr2c_bytetest_cntr2c_byte_test_mdl_Bit_Start                                       0
#define cAf6_upen_txmdl_cntr2c_bytetest_cntr2c_byte_test_mdl_Bit_End                                        31
#define cAf6_upen_txmdl_cntr2c_bytetest_cntr2c_byte_test_mdl_Mask                                     cBit31_0
#define cAf6_upen_txmdl_cntr2c_bytetest_cntr2c_byte_test_mdl_Shift                                           0
#define cAf6_upen_txmdl_cntr2c_bytetest_cntr2c_byte_test_mdl_MaxVal                                 0xffffffff
#define cAf6_upen_txmdl_cntr2c_bytetest_cntr2c_byte_test_mdl_MinVal                                        0x0
#define cAf6_upen_txmdl_cntr2c_bytetest_cntr2c_byte_test_mdl_RstVal                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER BYTE MESSAGE TX MDL TEST RO
Reg Addr   : 0x07C0 - 0x07D7
Reg Formula: 0x07C0+ $DE3ID
    Where  : 
           + $DE3ID (0-23) : DE3 ID
Reg Desc   : 
counter byte read only TEST message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txmdl_cntro_bytetest_Base                                                          0x07C0
#define cAf6Reg_upen_txmdl_cntro_bytetest(DE3ID)                                              (0x07C0+(DE3ID))
#define cAf6Reg_upen_txmdl_cntro_bytetest_WidthVal                                                          32
#define cAf6Reg_upen_txmdl_cntro_bytetest_WriteMask                                                        0x0

/*--------------------------------------
BitField Name: cntro_byte_test_mdl
BitField Type: R/W
BitField Desc: value counter byte
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txmdl_cntro_bytetest_cntro_byte_test_mdl_Bit_Start                                         0
#define cAf6_upen_txmdl_cntro_bytetest_cntro_byte_test_mdl_Bit_End                                          31
#define cAf6_upen_txmdl_cntro_bytetest_cntro_byte_test_mdl_Mask                                       cBit31_0
#define cAf6_upen_txmdl_cntro_bytetest_cntro_byte_test_mdl_Shift                                             0
#define cAf6_upen_txmdl_cntro_bytetest_cntro_byte_test_mdl_MaxVal                                   0xffffffff
#define cAf6_upen_txmdl_cntro_bytetest_cntro_byte_test_mdl_MinVal                                          0x0
#define cAf6_upen_txmdl_cntro_bytetest_cntro_byte_test_mdl_RstVal                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER VALID MESSAGE TX MDL IDLE R2C
Reg Addr   : 0x0600 - 0x0617
Reg Formula: 0x0600+ $DE3ID
    Where  : 
           + $DE3ID (0-23) : DE3 ID
Reg Desc   : 
counter valid read to clear IDLE message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txmdl_cntr2c_valididle_Base                                                        0x0600
#define cAf6Reg_upen_txmdl_cntr2c_valididle(DE3ID)                                            (0x0600+(DE3ID))
#define cAf6Reg_upen_txmdl_cntr2c_valididle_WidthVal                                                        32
#define cAf6Reg_upen_txmdl_cntr2c_valididle_WriteMask                                                      0x0

/*--------------------------------------
BitField Name: cntr2c_valid_idle_mdl
BitField Type: R/W
BitField Desc: value counter valid
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txmdl_cntr2c_valididle_cntr2c_valid_idle_mdl_Bit_Start                                       0
#define cAf6_upen_txmdl_cntr2c_valididle_cntr2c_valid_idle_mdl_Bit_End                                      31
#define cAf6_upen_txmdl_cntr2c_valididle_cntr2c_valid_idle_mdl_Mask                                   cBit31_0
#define cAf6_upen_txmdl_cntr2c_valididle_cntr2c_valid_idle_mdl_Shift                                         0
#define cAf6_upen_txmdl_cntr2c_valididle_cntr2c_valid_idle_mdl_MaxVal                               0xffffffff
#define cAf6_upen_txmdl_cntr2c_valididle_cntr2c_valid_idle_mdl_MinVal                                      0x0
#define cAf6_upen_txmdl_cntr2c_valididle_cntr2c_valid_idle_mdl_RstVal                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER VALID MESSAGE TX MDL IDLE RO
Reg Addr   : 0x0680 - 0x0697
Reg Formula: 0x0680+ $DE3ID
    Where  : 
           + $DE3ID (0-23) : DE3 ID
Reg Desc   : 
counter valid read only IDLE message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txmdl_cntro_valididle_Base                                                         0x0680
#define cAf6Reg_upen_txmdl_cntro_valididle(DE3ID)                                             (0x0680+(DE3ID))
#define cAf6Reg_upen_txmdl_cntro_valididle_WidthVal                                                         32
#define cAf6Reg_upen_txmdl_cntro_valididle_WriteMask                                                       0x0

/*--------------------------------------
BitField Name: cntro_valid_idle_mdl
BitField Type: R/W
BitField Desc: value counter valid
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txmdl_cntro_valididle_cntro_valid_idle_mdl_Bit_Start                                       0
#define cAf6_upen_txmdl_cntro_valididle_cntro_valid_idle_mdl_Bit_End                                        31
#define cAf6_upen_txmdl_cntro_valididle_cntro_valid_idle_mdl_Mask                                     cBit31_0
#define cAf6_upen_txmdl_cntro_valididle_cntro_valid_idle_mdl_Shift                                           0
#define cAf6_upen_txmdl_cntro_valididle_cntro_valid_idle_mdl_MaxVal                                 0xffffffff
#define cAf6_upen_txmdl_cntro_valididle_cntro_valid_idle_mdl_MinVal                                        0x0
#define cAf6_upen_txmdl_cntro_valididle_cntro_valid_idle_mdl_RstVal                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER VALID MESSAGE TX MDL PATH R2C
Reg Addr   : 0x0620 - 0x0637
Reg Formula: 0x0620+ $DE3ID
    Where  : 
           + $DE3ID (0-23) : DE3 ID
Reg Desc   : 
counter valid read to clear PATH message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txmdl_cntr2c_validpath_Base                                                        0x0620
#define cAf6Reg_upen_txmdl_cntr2c_validpath(DE3ID)                                            (0x0620+(DE3ID))
#define cAf6Reg_upen_txmdl_cntr2c_validpath_WidthVal                                                        32
#define cAf6Reg_upen_txmdl_cntr2c_validpath_WriteMask                                                      0x0

/*--------------------------------------
BitField Name: cntr2c_valid_path_mdl
BitField Type: R/W
BitField Desc: value counter valid
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txmdl_cntr2c_validpath_cntr2c_valid_path_mdl_Bit_Start                                       0
#define cAf6_upen_txmdl_cntr2c_validpath_cntr2c_valid_path_mdl_Bit_End                                      31
#define cAf6_upen_txmdl_cntr2c_validpath_cntr2c_valid_path_mdl_Mask                                   cBit31_0
#define cAf6_upen_txmdl_cntr2c_validpath_cntr2c_valid_path_mdl_Shift                                         0
#define cAf6_upen_txmdl_cntr2c_validpath_cntr2c_valid_path_mdl_MaxVal                               0xffffffff
#define cAf6_upen_txmdl_cntr2c_validpath_cntr2c_valid_path_mdl_MinVal                                      0x0
#define cAf6_upen_txmdl_cntr2c_validpath_cntr2c_valid_path_mdl_RstVal                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER VALID MESSAGE TX MDL PATH RO
Reg Addr   : 0x06A0 - 0x06B7
Reg Formula: 0x06A0+ $DE3ID
    Where  : 
           + $DE3ID (0-23) : DE3 ID
Reg Desc   : 
counter valid read only PATH message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txmdl_cntro_validpath_Base                                                         0x06A0
#define cAf6Reg_upen_txmdl_cntro_validpath(DE3ID)                                             (0x06A0+(DE3ID))
#define cAf6Reg_upen_txmdl_cntro_validpath_WidthVal                                                         32
#define cAf6Reg_upen_txmdl_cntro_validpath_WriteMask                                                       0x0

/*--------------------------------------
BitField Name: cntro_valid_path_mdl
BitField Type: R/W
BitField Desc: value counter valid
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txmdl_cntro_validpath_cntro_valid_path_mdl_Bit_Start                                       0
#define cAf6_upen_txmdl_cntro_validpath_cntro_valid_path_mdl_Bit_End                                        31
#define cAf6_upen_txmdl_cntro_validpath_cntro_valid_path_mdl_Mask                                     cBit31_0
#define cAf6_upen_txmdl_cntro_validpath_cntro_valid_path_mdl_Shift                                           0
#define cAf6_upen_txmdl_cntro_validpath_cntro_valid_path_mdl_MaxVal                                 0xffffffff
#define cAf6_upen_txmdl_cntro_validpath_cntro_valid_path_mdl_MinVal                                        0x0
#define cAf6_upen_txmdl_cntro_validpath_cntro_valid_path_mdl_RstVal                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER VALID MESSAGE TX MDL TEST R2C
Reg Addr   : 0x0640 - 0x0657
Reg Formula: 0x0640+ $DE3ID
    Where  : 
           + $DE3ID (0-23) : DE3 ID
Reg Desc   : 
counter valid read to clear TEST message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txmdl_cntr2c_validtest_Base                                                        0x0640
#define cAf6Reg_upen_txmdl_cntr2c_validtest(DE3ID)                                            (0x0640+(DE3ID))
#define cAf6Reg_upen_txmdl_cntr2c_validtest_WidthVal                                                        32
#define cAf6Reg_upen_txmdl_cntr2c_validtest_WriteMask                                                      0x0

/*--------------------------------------
BitField Name: cntr2c_valid_test_mdl
BitField Type: R/W
BitField Desc: value counter valid
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txmdl_cntr2c_validtest_cntr2c_valid_test_mdl_Bit_Start                                       0
#define cAf6_upen_txmdl_cntr2c_validtest_cntr2c_valid_test_mdl_Bit_End                                      31
#define cAf6_upen_txmdl_cntr2c_validtest_cntr2c_valid_test_mdl_Mask                                   cBit31_0
#define cAf6_upen_txmdl_cntr2c_validtest_cntr2c_valid_test_mdl_Shift                                         0
#define cAf6_upen_txmdl_cntr2c_validtest_cntr2c_valid_test_mdl_MaxVal                               0xffffffff
#define cAf6_upen_txmdl_cntr2c_validtest_cntr2c_valid_test_mdl_MinVal                                      0x0
#define cAf6_upen_txmdl_cntr2c_validtest_cntr2c_valid_test_mdl_RstVal                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER VALID MESSAGE TX MDL TEST RO
Reg Addr   : 0x06C0 - 0x06D7
Reg Formula: 0x06C0+ $DE3ID
    Where  : 
           + $DE3ID (0-23) : DE3 ID
Reg Desc   : 
counter valid read only TEST message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txmdl_cntro_validtest_Base                                                         0x06C0
#define cAf6Reg_upen_txmdl_cntro_validtest(DE3ID)                                             (0x06C0+(DE3ID))
#define cAf6Reg_upen_txmdl_cntro_validtest_WidthVal                                                         32
#define cAf6Reg_upen_txmdl_cntro_validtest_WriteMask                                                       0x0

/*--------------------------------------
BitField Name: cntro_valid_test_mdl
BitField Type: R/W
BitField Desc: value counter valid
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txmdl_cntro_validtest_cntro_valid_test_mdl_Bit_Start                                       0
#define cAf6_upen_txmdl_cntro_validtest_cntro_valid_test_mdl_Bit_End                                        31
#define cAf6_upen_txmdl_cntro_validtest_cntro_valid_test_mdl_Mask                                     cBit31_0
#define cAf6_upen_txmdl_cntro_validtest_cntro_valid_test_mdl_Shift                                           0
#define cAf6_upen_txmdl_cntro_validtest_cntro_valid_test_mdl_MaxVal                                 0xffffffff
#define cAf6_upen_txmdl_cntro_validtest_cntro_valid_test_mdl_MinVal                                        0x0
#define cAf6_upen_txmdl_cntro_validtest_cntro_valid_test_mdl_RstVal                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : Buff Message MDL IDLE1_2
Reg Addr   : 0x0800 - 0x092F
Reg Formula: 0x0800+ $DWORDID*16 + $DE3ID1
    Where  :
           + $DWORDID (0-18) : DWORD ID
           + $DE3ID1(0-15)  : DE3 ID1
Reg Desc   :
config message MDL IDLE Channel ID 0-15

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mdl_idle1_2_Base                                                                   0x0800
#define cAf6Reg_upen_mdl_idle1_2(DWORDID, DE3ID1)                               (0x0800+(DWORDID)*16+(DE3ID1))
#define cAf6Reg_upen_mdl_idle1_2_WidthVal                                                                   32
#define cAf6Reg_upen_mdl_idle1_2_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: idle_byte13_2
BitField Type: R/W
BitField Desc: MS BYTE
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_upen_mdl_idle1_2_idle_byte13_2_Bit_Start                                                       24
#define cAf6_upen_mdl_idle1_2_idle_byte13_2_Bit_End                                                         31
#define cAf6_upen_mdl_idle1_2_idle_byte13_2_Mask                                                     cBit31_24
#define cAf6_upen_mdl_idle1_2_idle_byte13_2_Shift                                                           24
#define cAf6_upen_mdl_idle1_2_idle_byte13_2_MaxVal                                                        0xff
#define cAf6_upen_mdl_idle1_2_idle_byte13_2_MinVal                                                         0x0
#define cAf6_upen_mdl_idle1_2_idle_byte13_2_RstVal                                                         0x0

/*--------------------------------------
BitField Name: ilde_byte12_2
BitField Type: R/W
BitField Desc: BYTE 2
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_upen_mdl_idle1_2_ilde_byte12_2_Bit_Start                                                       16
#define cAf6_upen_mdl_idle1_2_ilde_byte12_2_Bit_End                                                         23
#define cAf6_upen_mdl_idle1_2_ilde_byte12_2_Mask                                                     cBit23_16
#define cAf6_upen_mdl_idle1_2_ilde_byte12_2_Shift                                                           16
#define cAf6_upen_mdl_idle1_2_ilde_byte12_2_MaxVal                                                        0xff
#define cAf6_upen_mdl_idle1_2_ilde_byte12_2_MinVal                                                         0x0
#define cAf6_upen_mdl_idle1_2_ilde_byte12_2_RstVal                                                         0x0

/*--------------------------------------
BitField Name: idle_byte11_2
BitField Type: R/W
BitField Desc: BYTE 1
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_upen_mdl_idle1_2_idle_byte11_2_Bit_Start                                                        8
#define cAf6_upen_mdl_idle1_2_idle_byte11_2_Bit_End                                                         15
#define cAf6_upen_mdl_idle1_2_idle_byte11_2_Mask                                                      cBit15_8
#define cAf6_upen_mdl_idle1_2_idle_byte11_2_Shift                                                            8
#define cAf6_upen_mdl_idle1_2_idle_byte11_2_MaxVal                                                        0xff
#define cAf6_upen_mdl_idle1_2_idle_byte11_2_MinVal                                                         0x0
#define cAf6_upen_mdl_idle1_2_idle_byte11_2_RstVal                                                         0x0

/*--------------------------------------
BitField Name: idle_byte10_2
BitField Type: R/W
BitField Desc: LS BYTE
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_upen_mdl_idle1_2_idle_byte10_2_Bit_Start                                                        0
#define cAf6_upen_mdl_idle1_2_idle_byte10_2_Bit_End                                                          7
#define cAf6_upen_mdl_idle1_2_idle_byte10_2_Mask                                                       cBit7_0
#define cAf6_upen_mdl_idle1_2_idle_byte10_2_Shift                                                            0
#define cAf6_upen_mdl_idle1_2_idle_byte10_2_MaxVal                                                        0xff
#define cAf6_upen_mdl_idle1_2_idle_byte10_2_MinVal                                                         0x0
#define cAf6_upen_mdl_idle1_2_idle_byte10_2_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : Buff Message MDL IDLE2_2
Reg Addr   : 0x0930 - 0x09C7
Reg Formula: 0x0930+ $DWORDID*8 + $DE3ID2
    Where  :
           + $DWORDID (0-18) : DWORD ID
           + $DE3ID2 (0-7) : DE3 ID2
Reg Desc   :
config message MDL IDLE Channel ID 16-23

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mdl_idle2_2_Base                                                                   0x0930
#define cAf6Reg_upen_mdl_idle2_2(DWORDID, DE3ID2)                                (0x0930+(DWORDID)*8+(DE3ID2))
#define cAf6Reg_upen_mdl_idle2_2_WidthVal                                                                   32
#define cAf6Reg_upen_mdl_idle2_2_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: idle_byte23_2
BitField Type: R/W
BitField Desc: MS BYTE
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_upen_mdl_idle2_2_idle_byte23_2_Bit_Start                                                       24
#define cAf6_upen_mdl_idle2_2_idle_byte23_2_Bit_End                                                         31
#define cAf6_upen_mdl_idle2_2_idle_byte23_2_Mask                                                     cBit31_24
#define cAf6_upen_mdl_idle2_2_idle_byte23_2_Shift                                                           24
#define cAf6_upen_mdl_idle2_2_idle_byte23_2_MaxVal                                                        0xff
#define cAf6_upen_mdl_idle2_2_idle_byte23_2_MinVal                                                         0x0
#define cAf6_upen_mdl_idle2_2_idle_byte23_2_RstVal                                                         0x0

/*--------------------------------------
BitField Name: ilde_byte22_2
BitField Type: R/W
BitField Desc: BYTE 2
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_upen_mdl_idle2_2_ilde_byte22_2_Bit_Start                                                       16
#define cAf6_upen_mdl_idle2_2_ilde_byte22_2_Bit_End                                                         23
#define cAf6_upen_mdl_idle2_2_ilde_byte22_2_Mask                                                     cBit23_16
#define cAf6_upen_mdl_idle2_2_ilde_byte22_2_Shift                                                           16
#define cAf6_upen_mdl_idle2_2_ilde_byte22_2_MaxVal                                                        0xff
#define cAf6_upen_mdl_idle2_2_ilde_byte22_2_MinVal                                                         0x0
#define cAf6_upen_mdl_idle2_2_ilde_byte22_2_RstVal                                                         0x0

/*--------------------------------------
BitField Name: idle_byte21_2
BitField Type: R/W
BitField Desc: BYTE 1
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_upen_mdl_idle2_2_idle_byte21_2_Bit_Start                                                        8
#define cAf6_upen_mdl_idle2_2_idle_byte21_2_Bit_End                                                         15
#define cAf6_upen_mdl_idle2_2_idle_byte21_2_Mask                                                      cBit15_8
#define cAf6_upen_mdl_idle2_2_idle_byte21_2_Shift                                                            8
#define cAf6_upen_mdl_idle2_2_idle_byte21_2_MaxVal                                                        0xff
#define cAf6_upen_mdl_idle2_2_idle_byte21_2_MinVal                                                         0x0
#define cAf6_upen_mdl_idle2_2_idle_byte21_2_RstVal                                                         0x0

/*--------------------------------------
BitField Name: idle_byte20_2
BitField Type: R/W
BitField Desc: LS BYTE
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_upen_mdl_idle2_2_idle_byte20_2_Bit_Start                                                        0
#define cAf6_upen_mdl_idle2_2_idle_byte20_2_Bit_End                                                          7
#define cAf6_upen_mdl_idle2_2_idle_byte20_2_Mask                                                       cBit7_0
#define cAf6_upen_mdl_idle2_2_idle_byte20_2_Shift                                                            0
#define cAf6_upen_mdl_idle2_2_idle_byte20_2_MaxVal                                                        0xff
#define cAf6_upen_mdl_idle2_2_idle_byte20_2_MinVal                                                         0x0
#define cAf6_upen_mdl_idle2_2_idle_byte20_2_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : Config Buff Message MDL TESTPATH1_2
Reg Addr   : 0x0A00 - 0x0B2F
Reg Formula: 0x0A00+$DE3ID1 + $DWORDID*16
    Where  :
           + $DWORDID (0-18) : DWORD ID
           + $DE3ID1(0-15)  : DS3 E3 ID
Reg Desc   :
config message MDL TESTPATH Channel ID 0-15

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mdl_tp1_2_Base                                                                     0x0A00
#define cAf6Reg_upen_mdl_tp1_2(DWORDID, DE3ID1)                                 (0x0A00+(DE3ID1)+(DWORDID)*16)
#define cAf6Reg_upen_mdl_tp1_2_WidthVal                                                                     32
#define cAf6Reg_upen_mdl_tp1_2_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: tp_byte13_2
BitField Type: R/W
BitField Desc: MS BYTE
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_upen_mdl_tp1_2_tp_byte13_2_Bit_Start                                                           24
#define cAf6_upen_mdl_tp1_2_tp_byte13_2_Bit_End                                                             31
#define cAf6_upen_mdl_tp1_2_tp_byte13_2_Mask                                                         cBit31_24
#define cAf6_upen_mdl_tp1_2_tp_byte13_2_Shift                                                               24
#define cAf6_upen_mdl_tp1_2_tp_byte13_2_MaxVal                                                            0xff
#define cAf6_upen_mdl_tp1_2_tp_byte13_2_MinVal                                                             0x0
#define cAf6_upen_mdl_tp1_2_tp_byte13_2_RstVal                                                             0x0

/*--------------------------------------
BitField Name: tp_byte12_2
BitField Type: R/W
BitField Desc: BYTE 2
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_upen_mdl_tp1_2_tp_byte12_2_Bit_Start                                                           16
#define cAf6_upen_mdl_tp1_2_tp_byte12_2_Bit_End                                                             23
#define cAf6_upen_mdl_tp1_2_tp_byte12_2_Mask                                                         cBit23_16
#define cAf6_upen_mdl_tp1_2_tp_byte12_2_Shift                                                               16
#define cAf6_upen_mdl_tp1_2_tp_byte12_2_MaxVal                                                            0xff
#define cAf6_upen_mdl_tp1_2_tp_byte12_2_MinVal                                                             0x0
#define cAf6_upen_mdl_tp1_2_tp_byte12_2_RstVal                                                             0x0

/*--------------------------------------
BitField Name: tp_byte11_2
BitField Type: R/W
BitField Desc: BYTE 1
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_upen_mdl_tp1_2_tp_byte11_2_Bit_Start                                                            8
#define cAf6_upen_mdl_tp1_2_tp_byte11_2_Bit_End                                                             15
#define cAf6_upen_mdl_tp1_2_tp_byte11_2_Mask                                                          cBit15_8
#define cAf6_upen_mdl_tp1_2_tp_byte11_2_Shift                                                                8
#define cAf6_upen_mdl_tp1_2_tp_byte11_2_MaxVal                                                            0xff
#define cAf6_upen_mdl_tp1_2_tp_byte11_2_MinVal                                                             0x0
#define cAf6_upen_mdl_tp1_2_tp_byte11_2_RstVal                                                             0x0

/*--------------------------------------
BitField Name: tp_byte10_2
BitField Type: R/W
BitField Desc: LS BYTE
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_upen_mdl_tp1_2_tp_byte10_2_Bit_Start                                                            0
#define cAf6_upen_mdl_tp1_2_tp_byte10_2_Bit_End                                                              7
#define cAf6_upen_mdl_tp1_2_tp_byte10_2_Mask                                                           cBit7_0
#define cAf6_upen_mdl_tp1_2_tp_byte10_2_Shift                                                                0
#define cAf6_upen_mdl_tp1_2_tp_byte10_2_MaxVal                                                            0xff
#define cAf6_upen_mdl_tp1_2_tp_byte10_2_MinVal                                                             0x0
#define cAf6_upen_mdl_tp1_2_tp_byte10_2_RstVal                                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : Config Buff Message MDL TESTPATH2_2
Reg Addr   : 0x0B30 - 0x0BC7
Reg Formula: 0x0B30+ $DWORDID*8+ $DE3ID2
    Where  :
           + $DWORDID (0-18) : DWORD ID
           + $DE3ID2 (0-7) : DE3 ID2
Reg Desc   :
config message MDL TESTPATH Channel ID 16-23

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mdl_tp2_2_Base                                                                     0x0B30
#define cAf6Reg_upen_mdl_tp2_2(DWORDID, DE3ID2)                                  (0x0B30+(DWORDID)*8+(DE3ID2))
#define cAf6Reg_upen_mdl_tp2_2_WidthVal                                                                     32
#define cAf6Reg_upen_mdl_tp2_2_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: tp_byte23_2
BitField Type: R/W
BitField Desc: MS BYTE
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_upen_mdl_tp2_2_tp_byte23_2_Bit_Start                                                           24
#define cAf6_upen_mdl_tp2_2_tp_byte23_2_Bit_End                                                             31
#define cAf6_upen_mdl_tp2_2_tp_byte23_2_Mask                                                         cBit31_24
#define cAf6_upen_mdl_tp2_2_tp_byte23_2_Shift                                                               24
#define cAf6_upen_mdl_tp2_2_tp_byte23_2_MaxVal                                                            0xff
#define cAf6_upen_mdl_tp2_2_tp_byte23_2_MinVal                                                             0x0
#define cAf6_upen_mdl_tp2_2_tp_byte23_2_RstVal                                                             0x0

/*--------------------------------------
BitField Name: tp_byte22_2
BitField Type: R/W
BitField Desc: BYTE 2
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_upen_mdl_tp2_2_tp_byte22_2_Bit_Start                                                           16
#define cAf6_upen_mdl_tp2_2_tp_byte22_2_Bit_End                                                             23
#define cAf6_upen_mdl_tp2_2_tp_byte22_2_Mask                                                         cBit23_16
#define cAf6_upen_mdl_tp2_2_tp_byte22_2_Shift                                                               16
#define cAf6_upen_mdl_tp2_2_tp_byte22_2_MaxVal                                                            0xff
#define cAf6_upen_mdl_tp2_2_tp_byte22_2_MinVal                                                             0x0
#define cAf6_upen_mdl_tp2_2_tp_byte22_2_RstVal                                                             0x0

/*--------------------------------------
BitField Name: tp_byte21_2
BitField Type: R/W
BitField Desc: BYTE 1
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_upen_mdl_tp2_2_tp_byte21_2_Bit_Start                                                            8
#define cAf6_upen_mdl_tp2_2_tp_byte21_2_Bit_End                                                             15
#define cAf6_upen_mdl_tp2_2_tp_byte21_2_Mask                                                          cBit15_8
#define cAf6_upen_mdl_tp2_2_tp_byte21_2_Shift                                                                8
#define cAf6_upen_mdl_tp2_2_tp_byte21_2_MaxVal                                                            0xff
#define cAf6_upen_mdl_tp2_2_tp_byte21_2_MinVal                                                             0x0
#define cAf6_upen_mdl_tp2_2_tp_byte21_2_RstVal                                                             0x0

/*--------------------------------------
BitField Name: tp_byte20_2
BitField Type: R/W
BitField Desc: LS BYTE
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_upen_mdl_tp2_2_tp_byte20_2_Bit_Start                                                            0
#define cAf6_upen_mdl_tp2_2_tp_byte20_2_Bit_End                                                              7
#define cAf6_upen_mdl_tp2_2_tp_byte20_2_Mask                                                           cBit7_0
#define cAf6_upen_mdl_tp2_2_tp_byte20_2_Shift                                                                0
#define cAf6_upen_mdl_tp2_2_tp_byte20_2_MaxVal                                                            0xff
#define cAf6_upen_mdl_tp2_2_tp_byte20_2_MinVal                                                             0x0
#define cAf6_upen_mdl_tp2_2_tp_byte20_2_RstVal                                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : Status process MDL data_2
Reg Addr   : 0x0C00 - 0x0C17
Reg Formula: 0x0C00+$DE3ID
    Where  : 
           + $DE3ID (0-23) : DE3 ID
Reg Desc   : 
Status process MDL data

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mdl_txsta_2_Base                                                                   0x0C00
#define cAf6Reg_upen_mdl_txsta_2(DE3ID)                                                       (0x0C00+(DE3ID))
#define cAf6Reg_upen_mdl_txsta_2_WidthVal                                                                   32
#define cAf6Reg_upen_mdl_txsta_2_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: vld_page_2
BitField Type: R/W
BitField Desc: valid page
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_upen_mdl_txsta_2_vld_page_2_Bit_Start                                                          10
#define cAf6_upen_mdl_txsta_2_vld_page_2_Bit_End                                                            10
#define cAf6_upen_mdl_txsta_2_vld_page_2_Mask                                                           cBit10
#define cAf6_upen_mdl_txsta_2_vld_page_2_Shift                                                              10
#define cAf6_upen_mdl_txsta_2_vld_page_2_MaxVal                                                            0x1
#define cAf6_upen_mdl_txsta_2_vld_page_2_MinVal                                                            0x0
#define cAf6_upen_mdl_txsta_2_vld_page_2_RstVal                                                            0x0

/*--------------------------------------
BitField Name: sta_cnt_mess1s_2
BitField Type: R/W
BitField Desc: use for count MDL message to sent in 1s
BitField Bits: [09:08]
--------------------------------------*/
#define cAf6_upen_mdl_txsta_2_sta_cnt_mess1s_2_Bit_Start                                                     8
#define cAf6_upen_mdl_txsta_2_sta_cnt_mess1s_2_Bit_End                                                       9
#define cAf6_upen_mdl_txsta_2_sta_cnt_mess1s_2_Mask                                                    cBit9_8
#define cAf6_upen_mdl_txsta_2_sta_cnt_mess1s_2_Shift                                                         8
#define cAf6_upen_mdl_txsta_2_sta_cnt_mess1s_2_MaxVal                                                      0x3
#define cAf6_upen_mdl_txsta_2_sta_cnt_mess1s_2_MinVal                                                      0x0
#define cAf6_upen_mdl_txsta_2_sta_cnt_mess1s_2_RstVal                                                      0x0

/*--------------------------------------
BitField Name: sta_alrm_1s_2
BitField Type: R/W
BitField Desc: use for detecting new sending message
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_upen_mdl_txsta_2_sta_alrm_1s_2_Bit_Start                                                        7
#define cAf6_upen_mdl_txsta_2_sta_alrm_1s_2_Bit_End                                                          7
#define cAf6_upen_mdl_txsta_2_sta_alrm_1s_2_Mask                                                         cBit7
#define cAf6_upen_mdl_txsta_2_sta_alrm_1s_2_Shift                                                            7
#define cAf6_upen_mdl_txsta_2_sta_alrm_1s_2_MaxVal                                                         0x1
#define cAf6_upen_mdl_txsta_2_sta_alrm_1s_2_MinVal                                                         0x0
#define cAf6_upen_mdl_txsta_2_sta_alrm_1s_2_RstVal                                                         0x0

/*--------------------------------------
BitField Name: cnt_mdl_lenmess_2
BitField Type: R/W
BitField Desc: count number of byte in message
BitField Bits: [6:0]
--------------------------------------*/
#define cAf6_upen_mdl_txsta_2_cnt_mdl_lenmess_2_Bit_Start                                                    0
#define cAf6_upen_mdl_txsta_2_cnt_mdl_lenmess_2_Bit_End                                                      6
#define cAf6_upen_mdl_txsta_2_cnt_mdl_lenmess_2_Mask                                                   cBit6_0
#define cAf6_upen_mdl_txsta_2_cnt_mdl_lenmess_2_Shift                                                        0
#define cAf6_upen_mdl_txsta_2_cnt_mdl_lenmess_2_MaxVal                                                    0x7f
#define cAf6_upen_mdl_txsta_2_cnt_mdl_lenmess_2_MinVal                                                     0x0
#define cAf6_upen_mdl_txsta_2_cnt_mdl_lenmess_2_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : SET TO HAVE PACKET MDL BUFFER 1_2
Reg Addr   : 0x0C20 - 0xC37
Reg Formula: 0x0C20+$DE3ID
    Where  : 
           + $DE3ID (0-23) : DE3 ID
Reg Desc   : 
SET/CLEAR to ALRM STATUS MESSAGE in BUFFER 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_sta_idle_alren_2_Base                                                              0x0C20
#define cAf6Reg_upen_sta_idle_alren_2(DE3ID)                                                  (0x0C20+(DE3ID))
#define cAf6Reg_upen_sta_idle_alren_2_WidthVal                                                              32
#define cAf6Reg_upen_sta_idle_alren_2_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: idle_cfgen_2
BitField Type: R/W
BitField Desc: (0) : engine clear for indication to have sent, (1) CPU set for
indication to have new message which must send for buffer 0
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_sta_idle_alren_2_idle_cfgen_2_Bit_Start                                                    0
#define cAf6_upen_sta_idle_alren_2_idle_cfgen_2_Bit_End                                                      0
#define cAf6_upen_sta_idle_alren_2_idle_cfgen_2_Mask                                                     cBit0
#define cAf6_upen_sta_idle_alren_2_idle_cfgen_2_Shift                                                        0
#define cAf6_upen_sta_idle_alren_2_idle_cfgen_2_MaxVal                                                     0x1
#define cAf6_upen_sta_idle_alren_2_idle_cfgen_2_MinVal                                                     0x0
#define cAf6_upen_sta_idle_alren_2_idle_cfgen_2_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : SET TO HAVE PACKET MDL BUFFER 2_2
Reg Addr   : 0x0C40 - 0xC57
Reg Formula: 0x0C40+$DE3ID
    Where  : 
           + $DE3ID (0-23) : DE3 ID
Reg Desc   : 
SET/CLEAR to ALRM STATUS MESSAGE in BUFFER 2

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_sta_tp_alren_2_Base                                                                0x0C40
#define cAf6Reg_upen_sta_tp_alren_2(DE3ID)                                                    (0x0C40+(DE3ID))
#define cAf6Reg_upen_sta_tp_alren_2_WidthVal                                                                32
#define cAf6Reg_upen_sta_tp_alren_2_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: tp_cfgen_2
BitField Type: R/W
BitField Desc: (0) : engine clear for indication to have sent, (1) CPU set for
indication to have new message which must send for buffer 1
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_sta_tp_alren_2_tp_cfgen_2_Bit_Start                                                        0
#define cAf6_upen_sta_tp_alren_2_tp_cfgen_2_Bit_End                                                          0
#define cAf6_upen_sta_tp_alren_2_tp_cfgen_2_Mask                                                         cBit0
#define cAf6_upen_sta_tp_alren_2_tp_cfgen_2_Shift                                                            0
#define cAf6_upen_sta_tp_alren_2_tp_cfgen_2_MaxVal                                                         0x1
#define cAf6_upen_sta_tp_alren_2_tp_cfgen_2_MinVal                                                         0x0
#define cAf6_upen_sta_tp_alren_2_tp_cfgen_2_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG MDL Tx_2
Reg Addr   : 0x0C60 - 0xC77
Reg Formula: 0x0C60+$DE3ID
    Where  : 
           + $DE3ID (0-23) : DE3 ID
Reg Desc   : 
Config Tx MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_mdl_2_Base                                                                     0x0C60
#define cAf6Reg_upen_cfg_mdl_2(DE3ID)                                                         (0x0C60+(DE3ID))
#define cAf6Reg_upen_cfg_mdl_2_WidthVal                                                                     32
#define cAf6Reg_upen_cfg_mdl_2_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: cfg_seq_txen
BitField Type: R/W
BitField Desc: config enable Tx continous, (0) is disable, (1) is enable
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_cfg_mdl_2_cfg_seq_txen_Bit_Start                                                           4
#define cAf6_upen_cfg_mdl_2_cfg_seq_txen_Bit_End                                                             4
#define cAf6_upen_cfg_mdl_2_cfg_seq_txen_Mask                                                            cBit4
#define cAf6_upen_cfg_mdl_2_cfg_seq_txen_Shift                                                               4
#define cAf6_upen_cfg_mdl_2_cfg_seq_txen_MaxVal                                                            0x1
#define cAf6_upen_cfg_mdl_2_cfg_seq_txen_MinVal                                                            0x0
#define cAf6_upen_cfg_mdl_2_cfg_seq_txen_RstVal                                                            0x0

/*--------------------------------------
BitField Name: cfg_entx_2
BitField Type: R/W
BitField Desc: config enable Tx, (0) is disable, (1) is enable
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_cfg_mdl_2_cfg_entx_2_Bit_Start                                                             3
#define cAf6_upen_cfg_mdl_2_cfg_entx_2_Bit_End                                                               3
#define cAf6_upen_cfg_mdl_2_cfg_entx_2_Mask                                                              cBit3
#define cAf6_upen_cfg_mdl_2_cfg_entx_2_Shift                                                                 3
#define cAf6_upen_cfg_mdl_2_cfg_entx_2_MaxVal                                                              0x1
#define cAf6_upen_cfg_mdl_2_cfg_entx_2_MinVal                                                              0x0
#define cAf6_upen_cfg_mdl_2_cfg_entx_2_RstVal                                                              0x0

/*--------------------------------------
BitField Name: cfg_fcs_tx
BitField Type: R/W
BitField Desc: config mode transmit FCS, (1) is T.403-MSB, (0) is T.107-LSB
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_cfg_mdl_2_cfg_fcs_tx_Bit_Start                                                             2
#define cAf6_upen_cfg_mdl_2_cfg_fcs_tx_Bit_End                                                               2
#define cAf6_upen_cfg_mdl_2_cfg_fcs_tx_Mask                                                              cBit2
#define cAf6_upen_cfg_mdl_2_cfg_fcs_tx_Shift                                                                 2
#define cAf6_upen_cfg_mdl_2_cfg_fcs_tx_MaxVal                                                              0x1
#define cAf6_upen_cfg_mdl_2_cfg_fcs_tx_MinVal                                                              0x0
#define cAf6_upen_cfg_mdl_2_cfg_fcs_tx_RstVal                                                              0x0

/*--------------------------------------
BitField Name: cfg_mdlstd_tx_2
BitField Type: R/W
BitField Desc: config standard Tx, (0) is ANSI, (1) is AT&T
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_cfg_mdl_2_cfg_mdlstd_tx_2_Bit_Start                                                        1
#define cAf6_upen_cfg_mdl_2_cfg_mdlstd_tx_2_Bit_End                                                          1
#define cAf6_upen_cfg_mdl_2_cfg_mdlstd_tx_2_Mask                                                         cBit1
#define cAf6_upen_cfg_mdl_2_cfg_mdlstd_tx_2_Shift                                                            1
#define cAf6_upen_cfg_mdl_2_cfg_mdlstd_tx_2_MaxVal                                                         0x1
#define cAf6_upen_cfg_mdl_2_cfg_mdlstd_tx_2_MinVal                                                         0x0
#define cAf6_upen_cfg_mdl_2_cfg_mdlstd_tx_2_RstVal                                                         0x0

/*--------------------------------------
BitField Name: cfg_mdl_cr_2
BitField Type: R/W
BitField Desc: config bit command/respond MDL message, bit 0 is channel 0 of DS3
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_cfg_mdl_2_cfg_mdl_cr_2_Bit_Start                                                           0
#define cAf6_upen_cfg_mdl_2_cfg_mdl_cr_2_Bit_End                                                             0
#define cAf6_upen_cfg_mdl_2_cfg_mdl_cr_2_Mask                                                            cBit0
#define cAf6_upen_cfg_mdl_2_cfg_mdl_cr_2_Shift                                                               0
#define cAf6_upen_cfg_mdl_2_cfg_mdl_cr_2_MaxVal                                                            0x1
#define cAf6_upen_cfg_mdl_2_cfg_mdl_cr_2_MinVal                                                            0x0
#define cAf6_upen_cfg_mdl_2_cfg_mdl_cr_2_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER BYTE MESSAGE TX MDL IDLE R2C2
Reg Addr   : 0x0F00 - 0x0F17
Reg Formula: 0x0F00+ $DE3ID
    Where  : 
           + $DE3ID (0-23) : DE3 ID
Reg Desc   : 
counter byte read to clear IDLE message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txmdl_cntr2c_byteidle2_Base                                                        0x0F00
#define cAf6Reg_upen_txmdl_cntr2c_byteidle2(DE3ID)                                            (0x0F00+(DE3ID))
#define cAf6Reg_upen_txmdl_cntr2c_byteidle2_WidthVal                                                        32
#define cAf6Reg_upen_txmdl_cntr2c_byteidle2_WriteMask                                                      0x0

/*--------------------------------------
BitField Name: cntr2c_byte_idle_mdl
BitField Type: R/W
BitField Desc: value counter byte
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txmdl_cntr2c_byteidle2_cntr2c_byte_idle_mdl_Bit_Start                                       0
#define cAf6_upen_txmdl_cntr2c_byteidle2_cntr2c_byte_idle_mdl_Bit_End                                       31
#define cAf6_upen_txmdl_cntr2c_byteidle2_cntr2c_byte_idle_mdl_Mask                                    cBit31_0
#define cAf6_upen_txmdl_cntr2c_byteidle2_cntr2c_byte_idle_mdl_Shift                                          0
#define cAf6_upen_txmdl_cntr2c_byteidle2_cntr2c_byte_idle_mdl_MaxVal                                0xffffffff
#define cAf6_upen_txmdl_cntr2c_byteidle2_cntr2c_byte_idle_mdl_MinVal                                       0x0
#define cAf6_upen_txmdl_cntr2c_byteidle2_cntr2c_byte_idle_mdl_RstVal                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER BYTE MESSAGE TX MDL IDLE RO2
Reg Addr   : 0x0F80 - 0x0F97
Reg Formula: 0x0F80+ $DE3ID
    Where  : 
           + $DE3ID (0-23) : DE3 ID
Reg Desc   : 
counter byte read only IDLE message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txmdl_cntro_byteidle2_Base                                                         0x0F80
#define cAf6Reg_upen_txmdl_cntro_byteidle2(DE3ID)                                             (0x0F80+(DE3ID))
#define cAf6Reg_upen_txmdl_cntro_byteidle2_WidthVal                                                         32
#define cAf6Reg_upen_txmdl_cntro_byteidle2_WriteMask                                                       0x0

/*--------------------------------------
BitField Name: cntro_byte_idle_mdl
BitField Type: R/W
BitField Desc: value counter byte
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txmdl_cntro_byteidle2_cntro_byte_idle_mdl_Bit_Start                                        0
#define cAf6_upen_txmdl_cntro_byteidle2_cntro_byte_idle_mdl_Bit_End                                         31
#define cAf6_upen_txmdl_cntro_byteidle2_cntro_byte_idle_mdl_Mask                                      cBit31_0
#define cAf6_upen_txmdl_cntro_byteidle2_cntro_byte_idle_mdl_Shift                                            0
#define cAf6_upen_txmdl_cntro_byteidle2_cntro_byte_idle_mdl_MaxVal                                  0xffffffff
#define cAf6_upen_txmdl_cntro_byteidle2_cntro_byte_idle_mdl_MinVal                                         0x0
#define cAf6_upen_txmdl_cntro_byteidle2_cntro_byte_idle_mdl_RstVal                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER BYTE MESSAGE TX MDL PATH R2C2
Reg Addr   : 0x0F20 - 0x0F37
Reg Formula: 0x0F20+ $DE3ID
    Where  : 
           + $DE3ID (0-23) : DE3 ID
Reg Desc   : 
counter byte read to clear PATH message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txmdl_cntr2c_bytepath2_Base                                                        0x0F20
#define cAf6Reg_upen_txmdl_cntr2c_bytepath2(DE3ID)                                            (0x0F20+(DE3ID))
#define cAf6Reg_upen_txmdl_cntr2c_bytepath2_WidthVal                                                        32
#define cAf6Reg_upen_txmdl_cntr2c_bytepath2_WriteMask                                                      0x0

/*--------------------------------------
BitField Name: cntr2c_byte_path_mdl
BitField Type: R/W
BitField Desc: value counter byte
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txmdl_cntr2c_bytepath2_cntr2c_byte_path_mdl_Bit_Start                                       0
#define cAf6_upen_txmdl_cntr2c_bytepath2_cntr2c_byte_path_mdl_Bit_End                                       31
#define cAf6_upen_txmdl_cntr2c_bytepath2_cntr2c_byte_path_mdl_Mask                                    cBit31_0
#define cAf6_upen_txmdl_cntr2c_bytepath2_cntr2c_byte_path_mdl_Shift                                          0
#define cAf6_upen_txmdl_cntr2c_bytepath2_cntr2c_byte_path_mdl_MaxVal                                0xffffffff
#define cAf6_upen_txmdl_cntr2c_bytepath2_cntr2c_byte_path_mdl_MinVal                                       0x0
#define cAf6_upen_txmdl_cntr2c_bytepath2_cntr2c_byte_path_mdl_RstVal                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER BYTE MESSAGE TX MDL PATH RO2
Reg Addr   : 0x0FA0 - 0x0FB7
Reg Formula: 0x0FA0+ $DE3ID
    Where  : 
           + $DE3ID (0-23) : DE3 ID
Reg Desc   : 
counter byte read only PATH message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txmdl_cntro_bytepath2_Base                                                         0x0FA0
#define cAf6Reg_upen_txmdl_cntro_bytepath2(DE3ID)                                             (0x0FA0+(DE3ID))
#define cAf6Reg_upen_txmdl_cntro_bytepath2_WidthVal                                                         32
#define cAf6Reg_upen_txmdl_cntro_bytepath2_WriteMask                                                       0x0

/*--------------------------------------
BitField Name: cntro_byte_path_mdl
BitField Type: R/W
BitField Desc: value counter byte
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txmdl_cntro_bytepath2_cntro_byte_path_mdl_Bit_Start                                        0
#define cAf6_upen_txmdl_cntro_bytepath2_cntro_byte_path_mdl_Bit_End                                         31
#define cAf6_upen_txmdl_cntro_bytepath2_cntro_byte_path_mdl_Mask                                      cBit31_0
#define cAf6_upen_txmdl_cntro_bytepath2_cntro_byte_path_mdl_Shift                                            0
#define cAf6_upen_txmdl_cntro_bytepath2_cntro_byte_path_mdl_MaxVal                                  0xffffffff
#define cAf6_upen_txmdl_cntro_bytepath2_cntro_byte_path_mdl_MinVal                                         0x0
#define cAf6_upen_txmdl_cntro_bytepath2_cntro_byte_path_mdl_RstVal                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER BYTE MESSAGE TX MDL TEST R2C2
Reg Addr   : 0x0F40 - 0x0F57
Reg Formula: 0x0F40+ $DE3ID
    Where  : 
           + $DE3ID (0-23) : DE3 ID
Reg Desc   : 
counter byte read to clear TEST message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txmdl_cntr2c_bytetest2_Base                                                        0x0F40
#define cAf6Reg_upen_txmdl_cntr2c_bytetest2(DE3ID)                                            (0x0F40+(DE3ID))
#define cAf6Reg_upen_txmdl_cntr2c_bytetest2_WidthVal                                                        32
#define cAf6Reg_upen_txmdl_cntr2c_bytetest2_WriteMask                                                      0x0

/*--------------------------------------
BitField Name: cntr2c_byte_test_mdl
BitField Type: R/W
BitField Desc: value counter byte
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txmdl_cntr2c_bytetest2_cntr2c_byte_test_mdl_Bit_Start                                       0
#define cAf6_upen_txmdl_cntr2c_bytetest2_cntr2c_byte_test_mdl_Bit_End                                       31
#define cAf6_upen_txmdl_cntr2c_bytetest2_cntr2c_byte_test_mdl_Mask                                    cBit31_0
#define cAf6_upen_txmdl_cntr2c_bytetest2_cntr2c_byte_test_mdl_Shift                                          0
#define cAf6_upen_txmdl_cntr2c_bytetest2_cntr2c_byte_test_mdl_MaxVal                                0xffffffff
#define cAf6_upen_txmdl_cntr2c_bytetest2_cntr2c_byte_test_mdl_MinVal                                       0x0
#define cAf6_upen_txmdl_cntr2c_bytetest2_cntr2c_byte_test_mdl_RstVal                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER BYTE MESSAGE TX MDL TEST RO2
Reg Addr   : 0x0FC0 - 0x0FD7
Reg Formula: 0x0FC0+ $DE3ID
    Where  : 
           + $DE3ID (0-23) : DE3 ID
Reg Desc   : 
counter byte read only TEST message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txmdl_cntro_bytetest2_Base                                                         0x0FC0
#define cAf6Reg_upen_txmdl_cntro_bytetest2(DE3ID)                                             (0x0FC0+(DE3ID))
#define cAf6Reg_upen_txmdl_cntro_bytetest2_WidthVal                                                         32
#define cAf6Reg_upen_txmdl_cntro_bytetest2_WriteMask                                                       0x0

/*--------------------------------------
BitField Name: cntro_byte_test_mdl
BitField Type: R/W
BitField Desc: value counter byte
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txmdl_cntro_bytetest2_cntro_byte_test_mdl_Bit_Start                                        0
#define cAf6_upen_txmdl_cntro_bytetest2_cntro_byte_test_mdl_Bit_End                                         31
#define cAf6_upen_txmdl_cntro_bytetest2_cntro_byte_test_mdl_Mask                                      cBit31_0
#define cAf6_upen_txmdl_cntro_bytetest2_cntro_byte_test_mdl_Shift                                            0
#define cAf6_upen_txmdl_cntro_bytetest2_cntro_byte_test_mdl_MaxVal                                  0xffffffff
#define cAf6_upen_txmdl_cntro_bytetest2_cntro_byte_test_mdl_MinVal                                         0x0
#define cAf6_upen_txmdl_cntro_bytetest2_cntro_byte_test_mdl_RstVal                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER VALID MESSAGE TX MDL IDLE R2C2
Reg Addr   : 0x0E00 - 0x0E17
Reg Formula: 0x0E00+ $DE3ID
    Where  : 
           + $DE3ID (0-23) : DE3 ID
Reg Desc   : 
counter valid read to clear IDLE message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txmdl_cntr2c_valididle2_Base                                                       0x0E00
#define cAf6Reg_upen_txmdl_cntr2c_valididle2(DE3ID)                                           (0x0E00+(DE3ID))
#define cAf6Reg_upen_txmdl_cntr2c_valididle2_WidthVal                                                       32
#define cAf6Reg_upen_txmdl_cntr2c_valididle2_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: cntr2c_valid_idle_mdl
BitField Type: R/W
BitField Desc: value counter valid
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txmdl_cntr2c_valididle2_cntr2c_valid_idle_mdl_Bit_Start                                       0
#define cAf6_upen_txmdl_cntr2c_valididle2_cntr2c_valid_idle_mdl_Bit_End                                      31
#define cAf6_upen_txmdl_cntr2c_valididle2_cntr2c_valid_idle_mdl_Mask                                  cBit31_0
#define cAf6_upen_txmdl_cntr2c_valididle2_cntr2c_valid_idle_mdl_Shift                                        0
#define cAf6_upen_txmdl_cntr2c_valididle2_cntr2c_valid_idle_mdl_MaxVal                              0xffffffff
#define cAf6_upen_txmdl_cntr2c_valididle2_cntr2c_valid_idle_mdl_MinVal                                     0x0
#define cAf6_upen_txmdl_cntr2c_valididle2_cntr2c_valid_idle_mdl_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER VALID MESSAGE TX MDL IDLE RO2
Reg Addr   : 0x0E80 - 0x0E97
Reg Formula: 0x0E80+ $DE3ID
    Where  : 
           + $DE3ID (0-23) : DE3 ID
Reg Desc   : 
counter valid read only IDLE message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txmdl_cntro_valididle2_Base                                                        0x0E80
#define cAf6Reg_upen_txmdl_cntro_valididle2(DE3ID)                                            (0x0E80+(DE3ID))
#define cAf6Reg_upen_txmdl_cntro_valididle2_WidthVal                                                        32
#define cAf6Reg_upen_txmdl_cntro_valididle2_WriteMask                                                      0x0

/*--------------------------------------
BitField Name: cntro_valid_idle_mdl
BitField Type: R/W
BitField Desc: value counter valid
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txmdl_cntro_valididle2_cntro_valid_idle_mdl_Bit_Start                                       0
#define cAf6_upen_txmdl_cntro_valididle2_cntro_valid_idle_mdl_Bit_End                                       31
#define cAf6_upen_txmdl_cntro_valididle2_cntro_valid_idle_mdl_Mask                                    cBit31_0
#define cAf6_upen_txmdl_cntro_valididle2_cntro_valid_idle_mdl_Shift                                          0
#define cAf6_upen_txmdl_cntro_valididle2_cntro_valid_idle_mdl_MaxVal                                0xffffffff
#define cAf6_upen_txmdl_cntro_valididle2_cntro_valid_idle_mdl_MinVal                                       0x0
#define cAf6_upen_txmdl_cntro_valididle2_cntro_valid_idle_mdl_RstVal                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER VALID MESSAGE TX MDL PATH R2C2
Reg Addr   : 0x0E20 - 0x0E37
Reg Formula: 0x0E20+ $DE3ID
    Where  : 
           + $DE3ID (0-23) : DE3 ID
Reg Desc   : 
counter valid read to clear PATH message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txmdl_cntr2c_validpath2_Base                                                       0x0E20
#define cAf6Reg_upen_txmdl_cntr2c_validpath2(DE3ID)                                           (0x0E20+(DE3ID))
#define cAf6Reg_upen_txmdl_cntr2c_validpath2_WidthVal                                                       32
#define cAf6Reg_upen_txmdl_cntr2c_validpath2_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: cntr2c_valid_path_mdl
BitField Type: R/W
BitField Desc: value counter valid
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txmdl_cntr2c_validpath2_cntr2c_valid_path_mdl_Bit_Start                                       0
#define cAf6_upen_txmdl_cntr2c_validpath2_cntr2c_valid_path_mdl_Bit_End                                      31
#define cAf6_upen_txmdl_cntr2c_validpath2_cntr2c_valid_path_mdl_Mask                                  cBit31_0
#define cAf6_upen_txmdl_cntr2c_validpath2_cntr2c_valid_path_mdl_Shift                                        0
#define cAf6_upen_txmdl_cntr2c_validpath2_cntr2c_valid_path_mdl_MaxVal                              0xffffffff
#define cAf6_upen_txmdl_cntr2c_validpath2_cntr2c_valid_path_mdl_MinVal                                     0x0
#define cAf6_upen_txmdl_cntr2c_validpath2_cntr2c_valid_path_mdl_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER VALID MESSAGE TX MDL PATH RO2
Reg Addr   : 0x0EA0 - 0x0EB7
Reg Formula: 0x0EA0+ $DE3ID
    Where  : 
           + $DE3ID (0-23) : DE3 ID
Reg Desc   : 
counter valid read only PATH message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txmdl_cntro_validpath2_Base                                                        0x0EA0
#define cAf6Reg_upen_txmdl_cntro_validpath2(DE3ID)                                            (0x0EA0+(DE3ID))
#define cAf6Reg_upen_txmdl_cntro_validpath2_WidthVal                                                        32
#define cAf6Reg_upen_txmdl_cntro_validpath2_WriteMask                                                      0x0

/*--------------------------------------
BitField Name: cntro_valid_path_mdl
BitField Type: R/W
BitField Desc: value counter valid
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txmdl_cntro_validpath2_cntro_valid_path_mdl_Bit_Start                                       0
#define cAf6_upen_txmdl_cntro_validpath2_cntro_valid_path_mdl_Bit_End                                       31
#define cAf6_upen_txmdl_cntro_validpath2_cntro_valid_path_mdl_Mask                                    cBit31_0
#define cAf6_upen_txmdl_cntro_validpath2_cntro_valid_path_mdl_Shift                                          0
#define cAf6_upen_txmdl_cntro_validpath2_cntro_valid_path_mdl_MaxVal                                0xffffffff
#define cAf6_upen_txmdl_cntro_validpath2_cntro_valid_path_mdl_MinVal                                       0x0
#define cAf6_upen_txmdl_cntro_validpath2_cntro_valid_path_mdl_RstVal                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER VALID MESSAGE TX MDL TEST R2C2
Reg Addr   : 0x0E40 - 0x0E57
Reg Formula: 0x0E40+ $DE3ID
    Where  : 
           + $DE3ID (0-23) : DE3 ID
Reg Desc   : 
counter valid read to clear TEST message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txmdl_cntr2c_validtest2_Base                                                       0x0E40
#define cAf6Reg_upen_txmdl_cntr2c_validtest2(DE3ID)                                           (0x0E40+(DE3ID))
#define cAf6Reg_upen_txmdl_cntr2c_validtest2_WidthVal                                                       32
#define cAf6Reg_upen_txmdl_cntr2c_validtest2_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: cntr2c_valid_test_mdl
BitField Type: R/W
BitField Desc: value counter valid
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txmdl_cntr2c_validtest2_cntr2c_valid_test_mdl_Bit_Start                                       0
#define cAf6_upen_txmdl_cntr2c_validtest2_cntr2c_valid_test_mdl_Bit_End                                      31
#define cAf6_upen_txmdl_cntr2c_validtest2_cntr2c_valid_test_mdl_Mask                                  cBit31_0
#define cAf6_upen_txmdl_cntr2c_validtest2_cntr2c_valid_test_mdl_Shift                                        0
#define cAf6_upen_txmdl_cntr2c_validtest2_cntr2c_valid_test_mdl_MaxVal                              0xffffffff
#define cAf6_upen_txmdl_cntr2c_validtest2_cntr2c_valid_test_mdl_MinVal                                     0x0
#define cAf6_upen_txmdl_cntr2c_validtest2_cntr2c_valid_test_mdl_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER VALID MESSAGE TX MDL TEST RO2
Reg Addr   : 0x0EC0 - 0x0ED7
Reg Formula: 0x0EC0+ $DE3ID
    Where  : 
           + $DE3ID (0-23) : DE3 ID
Reg Desc   : 
counter valid read only TEST message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txmdl_cntro_validtest2_Base                                                        0x0EC0
#define cAf6Reg_upen_txmdl_cntro_validtest2(DE3ID)                                            (0x0EC0+(DE3ID))
#define cAf6Reg_upen_txmdl_cntro_validtest2_WidthVal                                                        32
#define cAf6Reg_upen_txmdl_cntro_validtest2_WriteMask                                                      0x0

/*--------------------------------------
BitField Name: cntro_valid_test_mdl
BitField Type: R/W
BitField Desc: value counter valid
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txmdl_cntro_validtest2_cntro_valid_test_mdl_Bit_Start                                       0
#define cAf6_upen_txmdl_cntro_validtest2_cntro_valid_test_mdl_Bit_End                                       31
#define cAf6_upen_txmdl_cntro_validtest2_cntro_valid_test_mdl_Mask                                    cBit31_0
#define cAf6_upen_txmdl_cntro_validtest2_cntro_valid_test_mdl_Shift                                          0
#define cAf6_upen_txmdl_cntro_validtest2_cntro_valid_test_mdl_MaxVal                                0xffffffff
#define cAf6_upen_txmdl_cntro_validtest2_cntro_valid_test_mdl_MinVal                                       0x0
#define cAf6_upen_txmdl_cntro_validtest2_cntro_valid_test_mdl_RstVal                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : Config Buff Message PRM STANDARD
Reg Addr   : 0x08000 - 0x0853F
Reg Formula: 0x08000+$STSID[4:3]*7 + $VTGID*64 + $SLICEID*32 + $STSID[2:0]*4 + $VTID
    Where  : 
           + $STSID (0-23) : STS ID
           + $VTGID (0-6) : VTG ID
           + $SLICEID (0-1) : SLICE ID
           + $VTID (0-3) : VT ID
Reg Desc   : 
config standard message PRM

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxprm_cfgstd_Base                                                                 0x08000
#define cAf6Reg_upen_rxprm_cfgstd(STSID, VTGID, SLICEID, VTID)        (0x08000+(STSID)[4:3]*7+(VTGID)*64+(SLICEID)*32+(STSID)[2:0]*4+(VTID))
#define cAf6Reg_upen_rxprm_cfgstd_WidthVal                                                                  32
#define cAf6Reg_upen_rxprm_cfgstd_WriteMask                                                                0x0

/*---------------------------------------
 * BitField Name: cfg_prm_rxen
 */
#define cAf6_upen_rxprm_cfgstd_cfg_prm_rxen_Mask  														cBit3
#define cAf6_upen_rxprm_cfgstd_cfg_prm_rxen_Shift 														3

/*--------------------------------------
BitField Name: cfg_prm_cr
BitField Type: R/W
BitField Desc: config C/R bit expected
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_rxprm_cfgstd_cfg_prm_cr_Bit_Start                                                          2
#define cAf6_upen_rxprm_cfgstd_cfg_prm_cr_Bit_End                                                            2
#define cAf6_upen_rxprm_cfgstd_cfg_prm_cr_Mask                                                           cBit2
#define cAf6_upen_rxprm_cfgstd_cfg_prm_cr_Shift                                                              2
#define cAf6_upen_rxprm_cfgstd_cfg_prm_cr_MaxVal                                                           0x1
#define cAf6_upen_rxprm_cfgstd_cfg_prm_cr_MinVal                                                           0x0
#define cAf6_upen_rxprm_cfgstd_cfg_prm_cr_RstVal                                                           0x0

/*--------------------------------------
BitField Name: cfg_prm_rxenstuff
BitField Type: R/W
BitField Desc: enable destuff   (0) is disable, (1) is enable
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_rxprm_cfgstd_cfg_prm_rxenstuff_Bit_Start                                                   1
#define cAf6_upen_rxprm_cfgstd_cfg_prm_rxenstuff_Bit_End                                                     1
#define cAf6_upen_rxprm_cfgstd_cfg_prm_rxenstuff_Mask                                                    cBit1
#define cAf6_upen_rxprm_cfgstd_cfg_prm_rxenstuff_Shift                                                       1
#define cAf6_upen_rxprm_cfgstd_cfg_prm_rxenstuff_MaxVal                                                    0x1
#define cAf6_upen_rxprm_cfgstd_cfg_prm_rxenstuff_MinVal                                                    0x0
#define cAf6_upen_rxprm_cfgstd_cfg_prm_rxenstuff_RstVal                                                    0x0
/* Hardware change 2016-June-10 */
#define cAf6_upen_rxprm_cfgstd_cfg_prm_rxfcs_mode_Mask                                                    cBit1
#define cAf6_upen_rxprm_cfgstd_cfg_prm_rxfcs_mode_Shift                                                       1

/*--------------------------------------
BitField Name: cfg_std_prm
BitField Type: R/W
BitField Desc: config standard   (0) is ANSI, (1) is AT&T
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_rxprm_cfgstd_cfg_std_prm_Bit_Start                                                         0
#define cAf6_upen_rxprm_cfgstd_cfg_std_prm_Bit_End                                                           0
#define cAf6_upen_rxprm_cfgstd_cfg_std_prm_Mask                                                          cBit0
#define cAf6_upen_rxprm_cfgstd_cfg_std_prm_Shift                                                             0
#define cAf6_upen_rxprm_cfgstd_cfg_std_prm_MaxVal                                                          0x1
#define cAf6_upen_rxprm_cfgstd_cfg_std_prm_MinVal                                                          0x0
#define cAf6_upen_rxprm_cfgstd_cfg_std_prm_RstVal                                                          0x0

/*------------------------------------------------------------------------------
Reg Name   : Buff Message MDL with configuration type1
Reg Addr   : 0x9800 - 0x09A7F
Reg Formula: 0x9800 + $RXDWORDID*32 + $MDLID1
    Where  : 
           + $RXDWORDID (0-19) : RX Double WORD ID
           + $MDLID1(0-31)  : DS3 E3 MDL ID
Reg Desc   : 
buffer message MDL which is configured type

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmdl_typebuff1_Base                                                               0x9800
#define cAf6Reg_upen_rxmdl_typebuff1(RXDWORDID, MDLID1)                       (0x9800+(RXDWORDID)*32+(MDLID1))
#define cAf6Reg_upen_rxmdl_typebuff1_WidthVal                                                               32
#define cAf6Reg_upen_rxmdl_typebuff1_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: mdl_tbyte3
BitField Type: R/W
BitField Desc: MS BYTE
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_upen_rxmdl_typebuff1_mdl_tbyte3_Bit_Start                                                      24
#define cAf6_upen_rxmdl_typebuff1_mdl_tbyte3_Bit_End                                                        31
#define cAf6_upen_rxmdl_typebuff1_mdl_tbyte3_Mask                                                    cBit31_24
#define cAf6_upen_rxmdl_typebuff1_mdl_tbyte3_Shift                                                          24
#define cAf6_upen_rxmdl_typebuff1_mdl_tbyte3_MaxVal                                                       0xff
#define cAf6_upen_rxmdl_typebuff1_mdl_tbyte3_MinVal                                                        0x0
#define cAf6_upen_rxmdl_typebuff1_mdl_tbyte3_RstVal                                                        0x0

/*--------------------------------------
BitField Name: mdl_tbyte2
BitField Type: R/W
BitField Desc: BYTE 2
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_upen_rxmdl_typebuff1_mdl_tbyte2_Bit_Start                                                      16
#define cAf6_upen_rxmdl_typebuff1_mdl_tbyte2_Bit_End                                                        23
#define cAf6_upen_rxmdl_typebuff1_mdl_tbyte2_Mask                                                    cBit23_16
#define cAf6_upen_rxmdl_typebuff1_mdl_tbyte2_Shift                                                          16
#define cAf6_upen_rxmdl_typebuff1_mdl_tbyte2_MaxVal                                                       0xff
#define cAf6_upen_rxmdl_typebuff1_mdl_tbyte2_MinVal                                                        0x0
#define cAf6_upen_rxmdl_typebuff1_mdl_tbyte2_RstVal                                                        0x0

/*--------------------------------------
BitField Name: mdl_tbyte1
BitField Type: R/W
BitField Desc: BYTE 1
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_upen_rxmdl_typebuff1_mdl_tbyte1_Bit_Start                                                       8
#define cAf6_upen_rxmdl_typebuff1_mdl_tbyte1_Bit_End                                                        15
#define cAf6_upen_rxmdl_typebuff1_mdl_tbyte1_Mask                                                     cBit15_8
#define cAf6_upen_rxmdl_typebuff1_mdl_tbyte1_Shift                                                           8
#define cAf6_upen_rxmdl_typebuff1_mdl_tbyte1_MaxVal                                                       0xff
#define cAf6_upen_rxmdl_typebuff1_mdl_tbyte1_MinVal                                                        0x0
#define cAf6_upen_rxmdl_typebuff1_mdl_tbyte1_RstVal                                                        0x0

/*--------------------------------------
BitField Name: mdl_tbyte0
BitField Type: R/W
BitField Desc: LS BYTE
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_upen_rxmdl_typebuff1_mdl_tbyte0_Bit_Start                                                       0
#define cAf6_upen_rxmdl_typebuff1_mdl_tbyte0_Bit_End                                                         7
#define cAf6_upen_rxmdl_typebuff1_mdl_tbyte0_Mask                                                      cBit7_0
#define cAf6_upen_rxmdl_typebuff1_mdl_tbyte0_Shift                                                           0
#define cAf6_upen_rxmdl_typebuff1_mdl_tbyte0_MaxVal                                                       0xff
#define cAf6_upen_rxmdl_typebuff1_mdl_tbyte0_MinVal                                                        0x0
#define cAf6_upen_rxmdl_typebuff1_mdl_tbyte0_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : Buff Message MDL with configuration type2
Reg Addr   : 0x9A80 - 0x09BBF
Reg Formula: 0x9A80 + $RXDWORDID*16 + $MDLID2
    Where  :
           + $RXDWORDID (0-19) : RX Double WORD ID
           + $MDLID2(0-15)  : DS3 E3 MDL ID
Reg Desc   :
buffer message MDL which is configured type

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmdl_typebuff2_Base                                                               0x9A80
#define cAf6Reg_upen_rxmdl_typebuff2(RXDWORDID, MDLID2)                       (0x9A80+(RXDWORDID)*16+(MDLID2))
#define cAf6Reg_upen_rxmdl_typebuff2_WidthVal                                                               32
#define cAf6Reg_upen_rxmdl_typebuff2_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: mdl_tbyte3
BitField Type: R/W
BitField Desc: MS BYTE
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_upen_rxmdl_typebuff2_mdl_tbyte3_Bit_Start                                                      24
#define cAf6_upen_rxmdl_typebuff2_mdl_tbyte3_Bit_End                                                        31
#define cAf6_upen_rxmdl_typebuff2_mdl_tbyte3_Mask                                                    cBit31_24
#define cAf6_upen_rxmdl_typebuff2_mdl_tbyte3_Shift                                                          24
#define cAf6_upen_rxmdl_typebuff2_mdl_tbyte3_MaxVal                                                       0xff
#define cAf6_upen_rxmdl_typebuff2_mdl_tbyte3_MinVal                                                        0x0
#define cAf6_upen_rxmdl_typebuff2_mdl_tbyte3_RstVal                                                        0x0

/*--------------------------------------
BitField Name: mdl_tbyte2
BitField Type: R/W
BitField Desc: BYTE 2
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_upen_rxmdl_typebuff2_mdl_tbyte2_Bit_Start                                                      16
#define cAf6_upen_rxmdl_typebuff2_mdl_tbyte2_Bit_End                                                        23
#define cAf6_upen_rxmdl_typebuff2_mdl_tbyte2_Mask                                                    cBit23_16
#define cAf6_upen_rxmdl_typebuff2_mdl_tbyte2_Shift                                                          16
#define cAf6_upen_rxmdl_typebuff2_mdl_tbyte2_MaxVal                                                       0xff
#define cAf6_upen_rxmdl_typebuff2_mdl_tbyte2_MinVal                                                        0x0
#define cAf6_upen_rxmdl_typebuff2_mdl_tbyte2_RstVal                                                        0x0

/*--------------------------------------
BitField Name: mdl_tbyte1
BitField Type: R/W
BitField Desc: BYTE 1
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_upen_rxmdl_typebuff2_mdl_tbyte1_Bit_Start                                                       8
#define cAf6_upen_rxmdl_typebuff2_mdl_tbyte1_Bit_End                                                        15
#define cAf6_upen_rxmdl_typebuff2_mdl_tbyte1_Mask                                                     cBit15_8
#define cAf6_upen_rxmdl_typebuff2_mdl_tbyte1_Shift                                                           8
#define cAf6_upen_rxmdl_typebuff2_mdl_tbyte1_MaxVal                                                       0xff
#define cAf6_upen_rxmdl_typebuff2_mdl_tbyte1_MinVal                                                        0x0
#define cAf6_upen_rxmdl_typebuff2_mdl_tbyte1_RstVal                                                        0x0

/*--------------------------------------
BitField Name: mdl_tbyte0
BitField Type: R/W
BitField Desc: LS BYTE
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_upen_rxmdl_typebuff2_mdl_tbyte0_Bit_Start                                                       0
#define cAf6_upen_rxmdl_typebuff2_mdl_tbyte0_Bit_End                                                         7
#define cAf6_upen_rxmdl_typebuff2_mdl_tbyte0_Mask                                                      cBit7_0
#define cAf6_upen_rxmdl_typebuff2_mdl_tbyte0_Shift                                                           0
#define cAf6_upen_rxmdl_typebuff2_mdl_tbyte0_MaxVal                                                       0xff
#define cAf6_upen_rxmdl_typebuff2_mdl_tbyte0_MinVal                                                        0x0
#define cAf6_upen_rxmdl_typebuff2_mdl_tbyte0_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : Buff Message MDL without configuration type1
Reg Addr   : 0x09C00 - 0x09E7F
Reg Formula: 0x09C00 + $MDLID1 + $RXDWORDID*32
    Where  :
           + $RXDWORDID (0-19) : RX Double WORD ID
           + $MDLID1(0-31)  : DS3 E3 MDL ID
Reg Desc   :
buffer message MDL which is not configured type

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmdl_ntypebuff1_Base                                                             0x09C00
#define cAf6Reg_upen_rxmdl_ntypebuff1(RXDWORDID, MDLID1)                     (0x09C00+(MDLID1)+(RXDWORDID)*32)
#define cAf6Reg_upen_rxmdl_ntypebuff1_WidthVal                                                              32
#define cAf6Reg_upen_rxmdl_ntypebuff1_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: mdl_ntbyte3
BitField Type: R/W
BitField Desc: MS BYTE
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_upen_rxmdl_ntypebuff1_mdl_ntbyte3_Bit_Start                                                    24
#define cAf6_upen_rxmdl_ntypebuff1_mdl_ntbyte3_Bit_End                                                      31
#define cAf6_upen_rxmdl_ntypebuff1_mdl_ntbyte3_Mask                                                  cBit31_24
#define cAf6_upen_rxmdl_ntypebuff1_mdl_ntbyte3_Shift                                                        24
#define cAf6_upen_rxmdl_ntypebuff1_mdl_ntbyte3_MaxVal                                                     0xff
#define cAf6_upen_rxmdl_ntypebuff1_mdl_ntbyte3_MinVal                                                      0x0
#define cAf6_upen_rxmdl_ntypebuff1_mdl_ntbyte3_RstVal                                                      0x0

/*--------------------------------------
BitField Name: mdl_ntbyte2
BitField Type: R/W
BitField Desc: BYTE 2
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_upen_rxmdl_ntypebuff1_mdl_ntbyte2_Bit_Start                                                    16
#define cAf6_upen_rxmdl_ntypebuff1_mdl_ntbyte2_Bit_End                                                      23
#define cAf6_upen_rxmdl_ntypebuff1_mdl_ntbyte2_Mask                                                  cBit23_16
#define cAf6_upen_rxmdl_ntypebuff1_mdl_ntbyte2_Shift                                                        16
#define cAf6_upen_rxmdl_ntypebuff1_mdl_ntbyte2_MaxVal                                                     0xff
#define cAf6_upen_rxmdl_ntypebuff1_mdl_ntbyte2_MinVal                                                      0x0
#define cAf6_upen_rxmdl_ntypebuff1_mdl_ntbyte2_RstVal                                                      0x0

/*--------------------------------------
BitField Name: mdl_ntbyte1
BitField Type: R/W
BitField Desc: BYTE 1
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_upen_rxmdl_ntypebuff1_mdl_ntbyte1_Bit_Start                                                     8
#define cAf6_upen_rxmdl_ntypebuff1_mdl_ntbyte1_Bit_End                                                      15
#define cAf6_upen_rxmdl_ntypebuff1_mdl_ntbyte1_Mask                                                   cBit15_8
#define cAf6_upen_rxmdl_ntypebuff1_mdl_ntbyte1_Shift                                                         8
#define cAf6_upen_rxmdl_ntypebuff1_mdl_ntbyte1_MaxVal                                                     0xff
#define cAf6_upen_rxmdl_ntypebuff1_mdl_ntbyte1_MinVal                                                      0x0
#define cAf6_upen_rxmdl_ntypebuff1_mdl_ntbyte1_RstVal                                                      0x0

/*--------------------------------------
BitField Name: mdl_ntbyte0
BitField Type: R/W
BitField Desc: LS BYTE
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_upen_rxmdl_ntypebuff1_mdl_ntbyte0_Bit_Start                                                     0
#define cAf6_upen_rxmdl_ntypebuff1_mdl_ntbyte0_Bit_End                                                       7
#define cAf6_upen_rxmdl_ntypebuff1_mdl_ntbyte0_Mask                                                    cBit7_0
#define cAf6_upen_rxmdl_ntypebuff1_mdl_ntbyte0_Shift                                                         0
#define cAf6_upen_rxmdl_ntypebuff1_mdl_ntbyte0_MaxVal                                                     0xff
#define cAf6_upen_rxmdl_ntypebuff1_mdl_ntbyte0_MinVal                                                      0x0
#define cAf6_upen_rxmdl_ntypebuff1_mdl_ntbyte0_RstVal                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : Buff Message MDL without configuration type2
Reg Addr   : 0x09E80 - 0x09FBF
Reg Formula: 0x09E80 + $MDLID2 + $RXDWORDID*16
    Where  : 
           + $RXDWORDID (0-19) : RX Double WORD ID
           + $MDLID2(0-15)  : DS3 E3 MDL ID
Reg Desc   : 
buffer message MDL which is not configured type

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmdl_ntypebuff2_Base                                                             0x09E80
#define cAf6Reg_upen_rxmdl_ntypebuff2(RXDWORDID, MDLID2)                     (0x09E80+(MDLID2)+(RXDWORDID)*16)
#define cAf6Reg_upen_rxmdl_ntypebuff2_WidthVal                                                              32
#define cAf6Reg_upen_rxmdl_ntypebuff2_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: mdl_ntbyte3
BitField Type: R/W
BitField Desc: MS BYTE
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_upen_rxmdl_ntypebuff2_mdl_ntbyte3_Bit_Start                                                    24
#define cAf6_upen_rxmdl_ntypebuff2_mdl_ntbyte3_Bit_End                                                      31
#define cAf6_upen_rxmdl_ntypebuff2_mdl_ntbyte3_Mask                                                  cBit31_24
#define cAf6_upen_rxmdl_ntypebuff2_mdl_ntbyte3_Shift                                                        24
#define cAf6_upen_rxmdl_ntypebuff2_mdl_ntbyte3_MaxVal                                                     0xff
#define cAf6_upen_rxmdl_ntypebuff2_mdl_ntbyte3_MinVal                                                      0x0
#define cAf6_upen_rxmdl_ntypebuff2_mdl_ntbyte3_RstVal                                                      0x0

/*--------------------------------------
BitField Name: mdl_ntbyte2
BitField Type: R/W
BitField Desc: BYTE 2
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_upen_rxmdl_ntypebuff2_mdl_ntbyte2_Bit_Start                                                    16
#define cAf6_upen_rxmdl_ntypebuff2_mdl_ntbyte2_Bit_End                                                      23
#define cAf6_upen_rxmdl_ntypebuff2_mdl_ntbyte2_Mask                                                  cBit23_16
#define cAf6_upen_rxmdl_ntypebuff2_mdl_ntbyte2_Shift                                                        16
#define cAf6_upen_rxmdl_ntypebuff2_mdl_ntbyte2_MaxVal                                                     0xff
#define cAf6_upen_rxmdl_ntypebuff2_mdl_ntbyte2_MinVal                                                      0x0
#define cAf6_upen_rxmdl_ntypebuff2_mdl_ntbyte2_RstVal                                                      0x0

/*--------------------------------------
BitField Name: mdl_ntbyte1
BitField Type: R/W
BitField Desc: BYTE 1
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_upen_rxmdl_ntypebuff2_mdl_ntbyte1_Bit_Start                                                     8
#define cAf6_upen_rxmdl_ntypebuff2_mdl_ntbyte1_Bit_End                                                      15
#define cAf6_upen_rxmdl_ntypebuff2_mdl_ntbyte1_Mask                                                   cBit15_8
#define cAf6_upen_rxmdl_ntypebuff2_mdl_ntbyte1_Shift                                                         8
#define cAf6_upen_rxmdl_ntypebuff2_mdl_ntbyte1_MaxVal                                                     0xff
#define cAf6_upen_rxmdl_ntypebuff2_mdl_ntbyte1_MinVal                                                      0x0
#define cAf6_upen_rxmdl_ntypebuff2_mdl_ntbyte1_RstVal                                                      0x0

/*--------------------------------------
BitField Name: mdl_ntbyte0
BitField Type: R/W
BitField Desc: LS BYTE
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_upen_rxmdl_ntypebuff2_mdl_ntbyte0_Bit_Start                                                     0
#define cAf6_upen_rxmdl_ntypebuff2_mdl_ntbyte0_Bit_End                                                       7
#define cAf6_upen_rxmdl_ntypebuff2_mdl_ntbyte0_Mask                                                    cBit7_0
#define cAf6_upen_rxmdl_ntypebuff2_mdl_ntbyte0_Shift                                                         0
#define cAf6_upen_rxmdl_ntypebuff2_mdl_ntbyte0_MaxVal                                                     0xff
#define cAf6_upen_rxmdl_ntypebuff2_mdl_ntbyte0_MinVal                                                      0x0
#define cAf6_upen_rxmdl_ntypebuff2_mdl_ntbyte0_RstVal                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : Config Buff Message MDL TYPE
Reg Addr   : 0x0A000 - 0x0A02F
Reg Formula: 0x0A000+$MDLID
    Where  : 
           + $MDLID(0-47)  : DS3 E3 MDL ID
Reg Desc   : 
config type message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmdl_cfgtype_Base                                                                0x0A000
#define cAf6Reg_upen_rxmdl_cfgtype(MDLID)                                                    (0x0A000+(MDLID))
#define cAf6Reg_upen_rxmdl_cfgtype_WidthVal                                                                 32
#define cAf6Reg_upen_rxmdl_cfgtype_WriteMask                                                               0x0

/* OLD FPGA */
#define cAf6_upen_rxmdl_cfgtype_cfg_type_mdl_Mask                                                      cBit7_0
#define cAf6_upen_rxmdl_cfgtype_cfg_type_mdl_Shift                                                           0
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_std_Mask                                                         cBit8
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_std_Shift                                                            8

/*--------------------------------------
BitField Name: cfg_fcs_rx
BitField Type: R/W
BitField Desc: config mode receive FCS, (1) is T.403-MSB, (0) is T.107-LSB
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_rxmdl_cfgtype_cfg_fcs_rx_Bit_Start                                                         5
#define cAf6_upen_rxmdl_cfgtype_cfg_fcs_rx_Bit_End                                                           5
#define cAf6_upen_rxmdl_cfgtype_cfg_fcs_rx_Mask                                                          cBit5
#define cAf6_upen_rxmdl_cfgtype_cfg_fcs_rx_Shift                                                             5
#define cAf6_upen_rxmdl_cfgtype_cfg_fcs_rx_MaxVal                                                          0x1
#define cAf6_upen_rxmdl_cfgtype_cfg_fcs_rx_MinVal                                                          0x0
#define cAf6_upen_rxmdl_cfgtype_cfg_fcs_rx_RstVal                                                          0x0

/*--------------------------------------
BitField Name: cfg_mdl_cr
BitField Type: R/W
BitField Desc: config C/R bit expected
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_cr_Bit_Start                                                         4
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_cr_Bit_End                                                           4
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_cr_Mask                                                          cBit4
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_cr_Shift                                                             4
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_cr_MaxVal                                                          0x1
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_cr_MinVal                                                          0x0
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_cr_RstVal    

/*--------------------------------------
BitField Name: cfg_mdl_std
BitField Type: R/W
BitField Desc: (0) is ANSI, (1) is AT&T
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_std_Bit_Start                                                        3
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_std_Bit_End                                                          3
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_std_new_Mask                                                         cBit3
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_std_new_Shift                                                            3
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_std_MaxVal                                                         0x1
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_std_MinVal                                                         0x0
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_std_RstVal                                                         0x0

/*--------------------------------------
BitField Name: cfg_mdl_mask_test
BitField Type: R/W
BitField Desc: config enable mask to moitor test massage (1): enable, (0):
disable
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_mask_test_Bit_Start                                                  2
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_mask_test_Bit_End                                                    2
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_mask_test_Mask                                                   cBit2
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_mask_test_Shift                                                      2
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_mask_test_MaxVal                                                   0x1
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_mask_test_MinVal                                                   0x0
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_mask_test_RstVal                                                   0x0

/*--------------------------------------
BitField Name: cfg_mdl_mask_path
BitField Type: R/W
BitField Desc: config enable mask to moitor path massage (1): enable, (0):
disable
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_mask_path_Bit_Start                                                  1
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_mask_path_Bit_End                                                    1
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_mask_path_Mask                                                   cBit1
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_mask_path_Shift                                                      1
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_mask_path_MaxVal                                                   0x1
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_mask_path_MinVal                                                   0x0
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_mask_path_RstVal                                                   0x0

/*--------------------------------------
BitField Name: cfg_mdl_mask_idle
BitField Type: R/W
BitField Desc: config enable mask to moitor idle massage (1): enable, (0):
disable
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_mask_idle_Bit_Start                                                  0
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_mask_idle_Bit_End                                                    0
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_mask_idle_Mask                                                   cBit0
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_mask_idle_Shift                                                      0
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_mask_idle_MaxVal                                                   0x1
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_mask_idle_MinVal                                                   0x0
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_mask_idle_RstVal                                                   0x0

/*------------------------------------------------------------------------------
Reg Name   : CONFIG CONTROL RX DESTUFF 0
Reg Addr   : 0xA041
Reg Formula: 
    Where  : 
Reg Desc   : 
config control DeStuff global

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_destuff_ctrl0_Base                                                                 0xA041
#define cAf6Reg_upen_destuff_ctrl0                                                                      0xA041
#define cAf6Reg_upen_destuff_ctrl0_WidthVal                                                                 32
#define cAf6Reg_upen_destuff_ctrl0_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: cfg_crmon
BitField Type: R/W
BitField Desc: (0) : disable, (1) : enable
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_destuff_ctrl0_cfg_crmon_Bit_Start                                                          5
#define cAf6_upen_destuff_ctrl0_cfg_crmon_Bit_End                                                            5
#define cAf6_upen_destuff_ctrl0_cfg_crmon_Mask                                                           cBit5
#define cAf6_upen_destuff_ctrl0_cfg_crmon_Shift                                                              5
#define cAf6_upen_destuff_ctrl0_cfg_crmon_MaxVal                                                           0x1
#define cAf6_upen_destuff_ctrl0_cfg_crmon_MinVal                                                           0x0
#define cAf6_upen_destuff_ctrl0_cfg_crmon_RstVal                                                           0x0

/*--------------------------------------
BitField Name: reserve1
BitField Type: R/W
BitField Desc: reserve
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_destuff_ctrl0_reserve1_Bit_Start                                                           4
#define cAf6_upen_destuff_ctrl0_reserve1_Bit_End                                                             4
#define cAf6_upen_destuff_ctrl0_reserve1_Mask                                                            cBit4
#define cAf6_upen_destuff_ctrl0_reserve1_Shift                                                               4
#define cAf6_upen_destuff_ctrl0_reserve1_MaxVal                                                            0x1
#define cAf6_upen_destuff_ctrl0_reserve1_MinVal                                                            0x0
#define cAf6_upen_destuff_ctrl0_reserve1_RstVal                                                            0x0

/*--------------------------------------
BitField Name: fcsmon
BitField Type: R/W
BitField Desc: (0) : disable monitor, (1) : enable
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_destuff_ctrl0_fcsmon_Bit_Start                                                             3
#define cAf6_upen_destuff_ctrl0_fcsmon_Bit_End                                                               3
#define cAf6_upen_destuff_ctrl0_fcsmon_Mask                                                              cBit3
#define cAf6_upen_destuff_ctrl0_fcsmon_Shift                                                                 3
#define cAf6_upen_destuff_ctrl0_fcsmon_MaxVal                                                              0x1
#define cAf6_upen_destuff_ctrl0_fcsmon_MinVal                                                              0x0
#define cAf6_upen_destuff_ctrl0_fcsmon_RstVal                                                              0x1

/*--------------------------------------
BitField Name: headermon_en
BitField Type: R/W
BitField Desc: (1) : enable monitor, (0) disable
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_destuff_ctrl0_headermon_en_Bit_Start                                                       0
#define cAf6_upen_destuff_ctrl0_headermon_en_Bit_End                                                         0
#define cAf6_upen_destuff_ctrl0_headermon_en_Mask                                                        cBit0
#define cAf6_upen_destuff_ctrl0_headermon_en_Shift                                                           0
#define cAf6_upen_destuff_ctrl0_headermon_en_MaxVal                                                        0x1
#define cAf6_upen_destuff_ctrl0_headermon_en_MinVal                                                        0x0
#define cAf6_upen_destuff_ctrl0_headermon_en_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : STICKY TO RECEIVE PACKET MDL BUFF CONFIG1
Reg Addr   : 0x0A045
Reg Formula: 
    Where  : 
Reg Desc   : 
sticky row for buffer of type message is configured 0-31

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mdl_stk_cfg1_Base                                                                 0x0A045
#define cAf6Reg_upen_mdl_stk_cfg1                                                                      0x0A045
#define cAf6Reg_upen_mdl_stk_cfg1_WidthVal                                                                  32
#define cAf6Reg_upen_mdl_stk_cfg1_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: stk_cfg_alr1
BitField Type: R/W
BitField Desc: sticky for buffer of type message is configured 0-31
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_mdl_stk_cfg1_stk_cfg_alr1_Bit_Start                                                        0
#define cAf6_upen_mdl_stk_cfg1_stk_cfg_alr1_Bit_End                                                         31
#define cAf6_upen_mdl_stk_cfg1_stk_cfg_alr1_Mask                                                      cBit31_0
#define cAf6_upen_mdl_stk_cfg1_stk_cfg_alr1_Shift                                                            0
#define cAf6_upen_mdl_stk_cfg1_stk_cfg_alr1_MaxVal                                                  0xffffffff
#define cAf6_upen_mdl_stk_cfg1_stk_cfg_alr1_MinVal                                                         0x0
#define cAf6_upen_mdl_stk_cfg1_stk_cfg_alr1_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : STICKY TO RECEIVE PACKET MDL BUFF CONFIG 2
Reg Addr   : 0x0A046
Reg Formula:
    Where  :
Reg Desc   :


------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mdl_stk_cfg2_Base                                                                 0x0A046
#define cAf6Reg_upen_mdl_stk_cfg2                                                                      0x0A046
#define cAf6Reg_upen_mdl_stk_cfg2_WidthVal                                                                  32
#define cAf6Reg_upen_mdl_stk_cfg2_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: stk_cfg_alr2
BitField Type: R/W
BitField Desc: sticky for buffer of type message is configured 32-47
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_mdl_stk_cfg2_stk_cfg_alr2_Bit_Start                                                        0
#define cAf6_upen_mdl_stk_cfg2_stk_cfg_alr2_Bit_End                                                         15
#define cAf6_upen_mdl_stk_cfg2_stk_cfg_alr2_Mask                                                      cBit15_0
#define cAf6_upen_mdl_stk_cfg2_stk_cfg_alr2_Shift                                                            0
#define cAf6_upen_mdl_stk_cfg2_stk_cfg_alr2_MaxVal                                                      0xffff
#define cAf6_upen_mdl_stk_cfg2_stk_cfg_alr2_MinVal                                                         0x0
#define cAf6_upen_mdl_stk_cfg2_stk_cfg_alr2_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER GOOD MESSAGE RX PRM
Reg Addr   : 0xC000 - 0xCD3F
Reg Formula: 0xC000+$STSID[4:3]*7 + $VTGID*64 + $SLICEID*32 + $STSID[2:0]*4 + $VTID + $UPRO * 2048
    Where  :
           + $STSID (0-23) : STS ID
           + $VTGID (0-6) : VTG ID
           + $SLICEID (0-1) : SLICE ID
           + $VTID (0-3) : VT ID
           + $UPRO (0-1) : upen read only
Reg Desc   :
counter good message PRM

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxprm_gmess_Base                                                                   0xC000
#define cAf6Reg_upen_rxprm_gmess(STSID, VTGID, SLICEID, VTID, UPRO)   (0xC000+(STSID)[4:3]*7+(VTGID)*64+(SLICEID)*32+(STSID)[2:0]*4+(VTID)+(UPRO)*2048)
#define cAf6Reg_upen_rxprm_gmess_WidthVal                                                                   32
#define cAf6Reg_upen_rxprm_gmess_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: cnt_gmess_prm
BitField Type: R/W
BitField Desc: value counter packet
BitField Bits: [14:00]
--------------------------------------*/
#define cAf6_upen_rxprm_gmess_cnt_gmess_prm_Bit_Start                                                        0
#define cAf6_upen_rxprm_gmess_cnt_gmess_prm_Bit_End                                                         14
#define cAf6_upen_rxprm_gmess_cnt_gmess_prm_Mask                                                      cBit14_0
#define cAf6_upen_rxprm_gmess_cnt_gmess_prm_Shift                                                            0
#define cAf6_upen_rxprm_gmess_cnt_gmess_prm_MaxVal                                                      0x7fff
#define cAf6_upen_rxprm_gmess_cnt_gmess_prm_MinVal                                                         0x0
#define cAf6_upen_rxprm_gmess_cnt_gmess_prm_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER DROP MESSAGE RX PRM
Reg Addr   : 0xD000 - 0xDD3F
Reg Formula: 0xD000+$STSID[4:3]*7 + $VTGID*64 + $SLICEID*32 + $STSID[2:0]*4 + $VTID + $UPRO * 2048
    Where  :
           + $STSID (0-23) : STS ID
           + $VTGID (0-6) : VTG ID
           + $SLICEID (0-1) : SLICE ID
           + $VTID (0-3) : VT ID
           + $UPRO (0-1) : upen read only
Reg Desc   :
counter drop message PRM

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxprm_drmess_Base                                                                  0xD000
#define cAf6Reg_upen_rxprm_drmess(STSID, VTGID, SLICEID, VTID, UPRO)  (0xD000+(STSID)[4:3]*7+(VTGID)*64+(SLICEID)*32+(STSID)[2:0]*4+(VTID)+(UPRO)*2048)
#define cAf6Reg_upen_rxprm_drmess_WidthVal                                                                  32
#define cAf6Reg_upen_rxprm_drmess_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: cnt_drmess_prm
BitField Type: R/W
BitField Desc: value counter packet
BitField Bits: [14:00]
--------------------------------------*/
#define cAf6_upen_rxprm_drmess_cnt_drmess_prm_Bit_Start                                                      0
#define cAf6_upen_rxprm_drmess_cnt_drmess_prm_Bit_End                                                       14
#define cAf6_upen_rxprm_drmess_cnt_drmess_prm_Mask                                                    cBit14_0
#define cAf6_upen_rxprm_drmess_cnt_drmess_prm_Shift                                                          0
#define cAf6_upen_rxprm_drmess_cnt_drmess_prm_MaxVal                                                    0x7fff
#define cAf6_upen_rxprm_drmess_cnt_drmess_prm_MinVal                                                       0x0
#define cAf6_upen_rxprm_drmess_cnt_drmess_prm_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER MISS MESSAGE RX PRM
Reg Addr   : 0xE000 - 0xED3F
Reg Formula: 0xE000+$STSID[4:3]*7 + $VTGID*64 + $SLICEID*32 + $STSID[2:0]*4 + $VTID + $UPRO * 2048
    Where  :
           + $STSID (0-23) : STS ID
           + $VTGID (0-6) : VTG ID
           + $SLICEID (0-1) : SLICE ID
           + $VTID (0-3) : VT ID
           + $UPRO (0-1) : upen read only
Reg Desc   :
counter miss message PRM

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxprm_mmess_Base                                                                   0xE000
#define cAf6Reg_upen_rxprm_mmess(STSID, VTGID, SLICEID, VTID, UPRO)   (0xE000+(STSID)[4:3]*7+(VTGID)*64+(SLICEID)*32+(STSID)[2:0]*4+(VTID)+(UPRO)*2048)
#define cAf6Reg_upen_rxprm_mmess_WidthVal                                                                   32
#define cAf6Reg_upen_rxprm_mmess_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: cnt_mmess_prm
BitField Type: R/W
BitField Desc: value counter packet
BitField Bits: [14:00]
--------------------------------------*/
#define cAf6_upen_rxprm_mmess_cnt_mmess_prm_Bit_Start                                                        0
#define cAf6_upen_rxprm_mmess_cnt_mmess_prm_Bit_End                                                         14
#define cAf6_upen_rxprm_mmess_cnt_mmess_prm_Mask                                                      cBit14_0
#define cAf6_upen_rxprm_mmess_cnt_mmess_prm_Shift                                                            0
#define cAf6_upen_rxprm_mmess_cnt_mmess_prm_MaxVal                                                      0x7fff
#define cAf6_upen_rxprm_mmess_cnt_mmess_prm_MinVal                                                         0x0
#define cAf6_upen_rxprm_mmess_cnt_mmess_prm_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : STICKY TO RECEIVE FCS MESSAGE MDL 1
Reg Addr   : 0x0A049
Reg Formula: 
    Where  : 
Reg Desc   : 
STICKY TO RECEIVE FCS PACKET MDL 0-31

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_fcs_stk1_Base                                                                     0x0A049
#define cAf6Reg_upen_fcs_stk1                                                                          0x0A049
#define cAf6Reg_upen_fcs_stk1_WidthVal                                                                      32
#define cAf6Reg_upen_fcs_stk1_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: stk_mdl_fcs1
BitField Type: R/W
BitField Desc: sticky FCS message channel 0-31
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_fcs_stk1_stk_mdl_fcs1_Bit_Start                                                            0
#define cAf6_upen_fcs_stk1_stk_mdl_fcs1_Bit_End                                                             31
#define cAf6_upen_fcs_stk1_stk_mdl_fcs1_Mask                                                          cBit31_0
#define cAf6_upen_fcs_stk1_stk_mdl_fcs1_Shift                                                                0
#define cAf6_upen_fcs_stk1_stk_mdl_fcs1_MaxVal                                                      0xffffffff
#define cAf6_upen_fcs_stk1_stk_mdl_fcs1_MinVal                                                             0x0
#define cAf6_upen_fcs_stk1_stk_mdl_fcs1_RstVal                                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : STICKY TO RECEIVE FCS MESSAGE MDL 2
Reg Addr   : 0x0A04A
Reg Formula: 
    Where  : 
Reg Desc   : 
STICKY TO RECEIVE FCS PACKET MDL 32-47

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_fcs_stk2_Base                                                                     0x0A04A
#define cAf6Reg_upen_fcs_stk2                                                                          0x0A04A
#define cAf6Reg_upen_fcs_stk2_WidthVal                                                                      32
#define cAf6Reg_upen_fcs_stk2_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: stk_mdl_fcs2
BitField Type: R/W
BitField Desc: sticky FCS message channel 32-47
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_fcs_stk2_stk_mdl_fcs2_Bit_Start                                                            0
#define cAf6_upen_fcs_stk2_stk_mdl_fcs2_Bit_End                                                             15
#define cAf6_upen_fcs_stk2_stk_mdl_fcs2_Mask                                                          cBit15_0
#define cAf6_upen_fcs_stk2_stk_mdl_fcs2_Shift                                                                0
#define cAf6_upen_fcs_stk2_stk_mdl_fcs2_MaxVal                                                          0xffff
#define cAf6_upen_fcs_stk2_stk_mdl_fcs2_MinVal                                                             0x0
#define cAf6_upen_fcs_stk2_stk_mdl_fcs2_RstVal                                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER BYTE MESSAGE RX PRM
Reg Addr   : 0xF000 - 0xFD3F
Reg Formula: 0xF000+$STSID[4:3]*7 + $VTGID*64 + $SLICEID*32 + $STSID[2:0]*4 + $VTID + $UPRO * 2048
    Where  :
           + $STSID (0-23) : STS ID
           + $VTGID (0-6) : VTG ID
           + $SLICEID (0-1) : SLICE ID
           + $VTID (0-3) : VT ID
           + $UPRO (0-1) : upen read only
Reg Desc   :
counter byte message PRM

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxprm_cnt_byte_Base                                                                0xF000
#define cAf6Reg_upen_rxprm_cnt_byte(STSID, VTGID, SLICEID, VTID, UPRO)(0xF000+(STSID)[4:3]*7+(VTGID)*64+(SLICEID)*32+(STSID)[2:0]*4+(VTID)+(UPRO)*2048)
#define cAf6Reg_upen_rxprm_cnt_byte_WidthVal                                                                32
#define cAf6Reg_upen_rxprm_cnt_byte_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: cnt_byte_rxprm
BitField Type: R/W
BitField Desc: value counter byte
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxprm_cnt_byte_cnt_byte_rxprm_Bit_Start                                                    0
#define cAf6_upen_rxprm_cnt_byte_cnt_byte_rxprm_Bit_End                                                     31
#define cAf6_upen_rxprm_cnt_byte_cnt_byte_rxprm_Mask                                                  cBit31_0
#define cAf6_upen_rxprm_cnt_byte_cnt_byte_rxprm_Shift                                                        0
#define cAf6_upen_rxprm_cnt_byte_cnt_byte_rxprm_MaxVal                                              0xffffffff
#define cAf6_upen_rxprm_cnt_byte_cnt_byte_rxprm_MinVal                                                     0x0
#define cAf6_upen_rxprm_cnt_byte_cnt_byte_rxprm_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER BYTE MESSAGE RX MDL IDLE R2C
Reg Addr   : 0xB400 - 0xB42F
Reg Formula: 0xB400+ $MDLID
    Where  : 
           + $MDLID (0-47) : MDL ID
Reg Desc   : 
counter byte read to clear IDLE message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmdl_cntr2c_byteidle_Base                                                         0xB400
#define cAf6Reg_upen_rxmdl_cntr2c_byteidle(MDLID)                                             (0xB400+(MDLID))
#define cAf6Reg_upen_rxmdl_cntr2c_byteidle_WidthVal                                                         32
#define cAf6Reg_upen_rxmdl_cntr2c_byteidle_WriteMask                                                       0x0

/*--------------------------------------
BitField Name: cntr2c_byte_idle_mdl
BitField Type: R/W
BitField Desc: value counter byte
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxmdl_cntr2c_byteidle_cntr2c_byte_idle_mdl_Bit_Start                                       0
#define cAf6_upen_rxmdl_cntr2c_byteidle_cntr2c_byte_idle_mdl_Bit_End                                        31
#define cAf6_upen_rxmdl_cntr2c_byteidle_cntr2c_byte_idle_mdl_Mask                                     cBit31_0
#define cAf6_upen_rxmdl_cntr2c_byteidle_cntr2c_byte_idle_mdl_Shift                                           0
#define cAf6_upen_rxmdl_cntr2c_byteidle_cntr2c_byte_idle_mdl_MaxVal                                 0xffffffff
#define cAf6_upen_rxmdl_cntr2c_byteidle_cntr2c_byte_idle_mdl_MinVal                                        0x0
#define cAf6_upen_rxmdl_cntr2c_byteidle_cntr2c_byte_idle_mdl_RstVal                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER BYTE MESSAGE RX MDL IDLE RO
Reg Addr   : 0xB500 - 0xB52F
Reg Formula: 0xB500+ $MDLID
    Where  : 
           + $MDLID (0-47) : MDL ID
Reg Desc   : 
counter byte read only IDLE message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmdl_cntro_byteidle_Base                                                          0xB500
#define cAf6Reg_upen_rxmdl_cntro_byteidle(MDLID)                                              (0xB500+(MDLID))
#define cAf6Reg_upen_rxmdl_cntro_byteidle_WidthVal                                                          32
#define cAf6Reg_upen_rxmdl_cntro_byteidle_WriteMask                                                        0x0

/*--------------------------------------
BitField Name: cntro_byte_idle_mdl
BitField Type: R/W
BitField Desc: value counter byte
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxmdl_cntro_byteidle_cntro_byte_idle_mdl_Bit_Start                                         0
#define cAf6_upen_rxmdl_cntro_byteidle_cntro_byte_idle_mdl_Bit_End                                          31
#define cAf6_upen_rxmdl_cntro_byteidle_cntro_byte_idle_mdl_Mask                                       cBit31_0
#define cAf6_upen_rxmdl_cntro_byteidle_cntro_byte_idle_mdl_Shift                                             0
#define cAf6_upen_rxmdl_cntro_byteidle_cntro_byte_idle_mdl_MaxVal                                   0xffffffff
#define cAf6_upen_rxmdl_cntro_byteidle_cntro_byte_idle_mdl_MinVal                                          0x0
#define cAf6_upen_rxmdl_cntro_byteidle_cntro_byte_idle_mdl_RstVal                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER BYTE MESSAGE RX MDL PATH R2C
Reg Addr   : 0xB440 - 0xB46F
Reg Formula: 0xB440+ $MDLID
    Where  : 
           + $MDLID (0-47) : MDL ID
Reg Desc   : 
counter byte read to clear PATH message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmdl_cntr2c_bytepath_Base                                                         0xB440
#define cAf6Reg_upen_rxmdl_cntr2c_bytepath(MDLID)                                             (0xB440+(MDLID))
#define cAf6Reg_upen_rxmdl_cntr2c_bytepath_WidthVal                                                         32
#define cAf6Reg_upen_rxmdl_cntr2c_bytepath_WriteMask                                                       0x0

/*--------------------------------------
BitField Name: cntr2c_byte_path_mdl
BitField Type: R/W
BitField Desc: value counter byte
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxmdl_cntr2c_bytepath_cntr2c_byte_path_mdl_Bit_Start                                       0
#define cAf6_upen_rxmdl_cntr2c_bytepath_cntr2c_byte_path_mdl_Bit_End                                        31
#define cAf6_upen_rxmdl_cntr2c_bytepath_cntr2c_byte_path_mdl_Mask                                     cBit31_0
#define cAf6_upen_rxmdl_cntr2c_bytepath_cntr2c_byte_path_mdl_Shift                                           0
#define cAf6_upen_rxmdl_cntr2c_bytepath_cntr2c_byte_path_mdl_MaxVal                                 0xffffffff
#define cAf6_upen_rxmdl_cntr2c_bytepath_cntr2c_byte_path_mdl_MinVal                                        0x0
#define cAf6_upen_rxmdl_cntr2c_bytepath_cntr2c_byte_path_mdl_RstVal                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER BYTE MESSAGE RX MDL PATH RO
Reg Addr   : 0xB540 - 0xB56F
Reg Formula: 0xB540+ $MDLID
    Where  : 
           + $MDLID (0-47) : MDL ID
Reg Desc   : 
counter byte read only PATH message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmdl_cntro_bytepath_Base                                                          0xB540
#define cAf6Reg_upen_rxmdl_cntro_bytepath(MDLID)                                              (0xB540+(MDLID))
#define cAf6Reg_upen_rxmdl_cntro_bytepath_WidthVal                                                          32
#define cAf6Reg_upen_rxmdl_cntro_bytepath_WriteMask                                                        0x0

/*--------------------------------------
BitField Name: cntro_byte_path_mdl
BitField Type: R/W
BitField Desc: value counter byte
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxmdl_cntro_bytepath_cntro_byte_path_mdl_Bit_Start                                         0
#define cAf6_upen_rxmdl_cntro_bytepath_cntro_byte_path_mdl_Bit_End                                          31
#define cAf6_upen_rxmdl_cntro_bytepath_cntro_byte_path_mdl_Mask                                       cBit31_0
#define cAf6_upen_rxmdl_cntro_bytepath_cntro_byte_path_mdl_Shift                                             0
#define cAf6_upen_rxmdl_cntro_bytepath_cntro_byte_path_mdl_MaxVal                                   0xffffffff
#define cAf6_upen_rxmdl_cntro_bytepath_cntro_byte_path_mdl_MinVal                                          0x0
#define cAf6_upen_rxmdl_cntro_bytepath_cntro_byte_path_mdl_RstVal                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER BYTE MESSAGE RX MDL TEST R2C
Reg Addr   : 0xB480 - 0xB4AF
Reg Formula: 0xB480+ $MDLID
    Where  : 
           + $MDLID (0-47) : MDL ID
Reg Desc   : 
counter byte read to clear TEST message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmdl_cntr2c_bytetest_Base                                                         0xB480
#define cAf6Reg_upen_rxmdl_cntr2c_bytetest(MDLID)                                             (0xB480+(MDLID))
#define cAf6Reg_upen_rxmdl_cntr2c_bytetest_WidthVal                                                         32
#define cAf6Reg_upen_rxmdl_cntr2c_bytetest_WriteMask                                                       0x0

/*--------------------------------------
BitField Name: cntr2c_byte_test_mdl
BitField Type: R/W
BitField Desc: value counter byte
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxmdl_cntr2c_bytetest_cntr2c_byte_test_mdl_Bit_Start                                       0
#define cAf6_upen_rxmdl_cntr2c_bytetest_cntr2c_byte_test_mdl_Bit_End                                        31
#define cAf6_upen_rxmdl_cntr2c_bytetest_cntr2c_byte_test_mdl_Mask                                     cBit31_0
#define cAf6_upen_rxmdl_cntr2c_bytetest_cntr2c_byte_test_mdl_Shift                                           0
#define cAf6_upen_rxmdl_cntr2c_bytetest_cntr2c_byte_test_mdl_MaxVal                                 0xffffffff
#define cAf6_upen_rxmdl_cntr2c_bytetest_cntr2c_byte_test_mdl_MinVal                                        0x0
#define cAf6_upen_rxmdl_cntr2c_bytetest_cntr2c_byte_test_mdl_RstVal                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER BYTE MESSAGE RX MDL TEST RO
Reg Addr   : 0xB580 - 0xB5AF
Reg Formula: 0xB580+ $MDLID
    Where  : 
           + $MDLID (0-47) : MDL ID
Reg Desc   : 
counter byte read only TEST message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmdl_cntro_bytetest_Base                                                          0xB580
#define cAf6Reg_upen_rxmdl_cntro_bytetest(MDLID)                                              (0xB580+(MDLID))
#define cAf6Reg_upen_rxmdl_cntro_bytetest_WidthVal                                                          32
#define cAf6Reg_upen_rxmdl_cntro_bytetest_WriteMask                                                        0x0

/*--------------------------------------
BitField Name: cntro_byte_test_mdl
BitField Type: R/W
BitField Desc: value counter byte
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxmdl_cntro_bytetest_cntro_byte_test_mdl_Bit_Start                                         0
#define cAf6_upen_rxmdl_cntro_bytetest_cntro_byte_test_mdl_Bit_End                                          31
#define cAf6_upen_rxmdl_cntro_bytetest_cntro_byte_test_mdl_Mask                                       cBit31_0
#define cAf6_upen_rxmdl_cntro_bytetest_cntro_byte_test_mdl_Shift                                             0
#define cAf6_upen_rxmdl_cntro_bytetest_cntro_byte_test_mdl_MaxVal                                   0xffffffff
#define cAf6_upen_rxmdl_cntro_bytetest_cntro_byte_test_mdl_MinVal                                          0x0
#define cAf6_upen_rxmdl_cntro_bytetest_cntro_byte_test_mdl_RstVal                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER GOOD MESSAGE RX MDL IDLE R2C
Reg Addr   : 0xB000 - 0xB02F
Reg Formula: 0xB000+ $MDLID
    Where  : 
           + $MDLID (0-47) : MDL ID
Reg Desc   : 
counter good read to clear IDLE message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmdl_cntr2c_goodidle_Base                                                         0xB000
#define cAf6Reg_upen_rxmdl_cntr2c_goodidle(MDLID)                                             (0xB000+(MDLID))
#define cAf6Reg_upen_rxmdl_cntr2c_goodidle_WidthVal                                                         32
#define cAf6Reg_upen_rxmdl_cntr2c_goodidle_WriteMask                                                       0x0

/*--------------------------------------
BitField Name: cntr2c_good_idle_mdl
BitField Type: R/W
BitField Desc: value counter good
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxmdl_cntr2c_goodidle_cntr2c_good_idle_mdl_Bit_Start                                       0
#define cAf6_upen_rxmdl_cntr2c_goodidle_cntr2c_good_idle_mdl_Bit_End                                        31
#define cAf6_upen_rxmdl_cntr2c_goodidle_cntr2c_good_idle_mdl_Mask                                     cBit31_0
#define cAf6_upen_rxmdl_cntr2c_goodidle_cntr2c_good_idle_mdl_Shift                                           0
#define cAf6_upen_rxmdl_cntr2c_goodidle_cntr2c_good_idle_mdl_MaxVal                                 0xffffffff
#define cAf6_upen_rxmdl_cntr2c_goodidle_cntr2c_good_idle_mdl_MinVal                                        0x0
#define cAf6_upen_rxmdl_cntr2c_goodidle_cntr2c_good_idle_mdl_RstVal                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER GOOD MESSAGE RX MDL IDLE RO
Reg Addr   : 0xB200 - 0xB22F
Reg Formula: 0xB200+ $MDLID
    Where  : 
           + $MDLID (0-47) : MDL ID
Reg Desc   : 
counter good read only IDLE message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmdl_cntro_goodidle_Base                                                          0xB200
#define cAf6Reg_upen_rxmdl_cntro_goodidle(MDLID)                                              (0xB200+(MDLID))
#define cAf6Reg_upen_rxmdl_cntro_goodidle_WidthVal                                                          32
#define cAf6Reg_upen_rxmdl_cntro_goodidle_WriteMask                                                        0x0

/*--------------------------------------
BitField Name: cntro_good_idle_mdl
BitField Type: R/W
BitField Desc: value counter good
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxmdl_cntro_goodidle_cntro_good_idle_mdl_Bit_Start                                         0
#define cAf6_upen_rxmdl_cntro_goodidle_cntro_good_idle_mdl_Bit_End                                          31
#define cAf6_upen_rxmdl_cntro_goodidle_cntro_good_idle_mdl_Mask                                       cBit31_0
#define cAf6_upen_rxmdl_cntro_goodidle_cntro_good_idle_mdl_Shift                                             0
#define cAf6_upen_rxmdl_cntro_goodidle_cntro_good_idle_mdl_MaxVal                                   0xffffffff
#define cAf6_upen_rxmdl_cntro_goodidle_cntro_good_idle_mdl_MinVal                                          0x0
#define cAf6_upen_rxmdl_cntro_goodidle_cntro_good_idle_mdl_RstVal                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER GOOD MESSAGE RX MDL PATH R2C
Reg Addr   : 0xB040 - 0xB06F
Reg Formula: 0xB040+ $MDLID
    Where  : 
           + $MDLID (0-47) : MDL ID
Reg Desc   : 
counter good read to clear PATH message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmdl_cntr2c_goodpath_Base                                                         0xB040
#define cAf6Reg_upen_rxmdl_cntr2c_goodpath(MDLID)                                             (0xB040+(MDLID))
#define cAf6Reg_upen_rxmdl_cntr2c_goodpath_WidthVal                                                         32
#define cAf6Reg_upen_rxmdl_cntr2c_goodpath_WriteMask                                                       0x0

/*--------------------------------------
BitField Name: cntr2c_good_path_mdl
BitField Type: R/W
BitField Desc: value counter good
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxmdl_cntr2c_goodpath_cntr2c_good_path_mdl_Bit_Start                                       0
#define cAf6_upen_rxmdl_cntr2c_goodpath_cntr2c_good_path_mdl_Bit_End                                        31
#define cAf6_upen_rxmdl_cntr2c_goodpath_cntr2c_good_path_mdl_Mask                                     cBit31_0
#define cAf6_upen_rxmdl_cntr2c_goodpath_cntr2c_good_path_mdl_Shift                                           0
#define cAf6_upen_rxmdl_cntr2c_goodpath_cntr2c_good_path_mdl_MaxVal                                 0xffffffff
#define cAf6_upen_rxmdl_cntr2c_goodpath_cntr2c_good_path_mdl_MinVal                                        0x0
#define cAf6_upen_rxmdl_cntr2c_goodpath_cntr2c_good_path_mdl_RstVal                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER GOOD MESSAGE RX MDL PATH RO
Reg Addr   : 0xB240 - 0xB26F
Reg Formula: 0xB240+ $MDLID
    Where  : 
           + $MDLID (0-47) : MDL ID
Reg Desc   : 
counter good read only PATH message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmdl_cntro_goodpath_Base                                                          0xB240
#define cAf6Reg_upen_rxmdl_cntro_goodpath(MDLID)                                              (0xB240+(MDLID))
#define cAf6Reg_upen_rxmdl_cntro_goodpath_WidthVal                                                          32
#define cAf6Reg_upen_rxmdl_cntro_goodpath_WriteMask                                                        0x0

/*--------------------------------------
BitField Name: cntro_good_path_mdl
BitField Type: R/W
BitField Desc: value counter good
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxmdl_cntro_goodpath_cntro_good_path_mdl_Bit_Start                                         0
#define cAf6_upen_rxmdl_cntro_goodpath_cntro_good_path_mdl_Bit_End                                          31
#define cAf6_upen_rxmdl_cntro_goodpath_cntro_good_path_mdl_Mask                                       cBit31_0
#define cAf6_upen_rxmdl_cntro_goodpath_cntro_good_path_mdl_Shift                                             0
#define cAf6_upen_rxmdl_cntro_goodpath_cntro_good_path_mdl_MaxVal                                   0xffffffff
#define cAf6_upen_rxmdl_cntro_goodpath_cntro_good_path_mdl_MinVal                                          0x0
#define cAf6_upen_rxmdl_cntro_goodpath_cntro_good_path_mdl_RstVal                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER GOOD MESSAGE RX MDL TEST R2C
Reg Addr   : 0xB080 - 0xB0AF
Reg Formula: 0xB080+ $MDLID
    Where  : 
           + $MDLID (0-47) : MDL ID
Reg Desc   : 
counter good read to clear TEST message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmdl_cntr2c_goodtest_Base                                                         0xB080
#define cAf6Reg_upen_rxmdl_cntr2c_goodtest(MDLID)                                             (0xB080+(MDLID))
#define cAf6Reg_upen_rxmdl_cntr2c_goodtest_WidthVal                                                         32
#define cAf6Reg_upen_rxmdl_cntr2c_goodtest_WriteMask                                                       0x0

/*--------------------------------------
BitField Name: cntr2c_good_test_mdl
BitField Type: R/W
BitField Desc: value counter good
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxmdl_cntr2c_goodtest_cntr2c_good_test_mdl_Bit_Start                                       0
#define cAf6_upen_rxmdl_cntr2c_goodtest_cntr2c_good_test_mdl_Bit_End                                        31
#define cAf6_upen_rxmdl_cntr2c_goodtest_cntr2c_good_test_mdl_Mask                                     cBit31_0
#define cAf6_upen_rxmdl_cntr2c_goodtest_cntr2c_good_test_mdl_Shift                                           0
#define cAf6_upen_rxmdl_cntr2c_goodtest_cntr2c_good_test_mdl_MaxVal                                 0xffffffff
#define cAf6_upen_rxmdl_cntr2c_goodtest_cntr2c_good_test_mdl_MinVal                                        0x0
#define cAf6_upen_rxmdl_cntr2c_goodtest_cntr2c_good_test_mdl_RstVal                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER GOOD MESSAGE RX MDL TEST RO
Reg Addr   : 0xB280 - 0xB2AF
Reg Formula: 0xB280+ $MDLID
    Where  : 
           + $MDLID (0-47) : MDL ID
Reg Desc   : 
counter good read only TEST message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmdl_cntro_goodtest_Base                                                          0xB280
#define cAf6Reg_upen_rxmdl_cntro_goodtest(MDLID)                                              (0xB280+(MDLID))
#define cAf6Reg_upen_rxmdl_cntro_goodtest_WidthVal                                                          32
#define cAf6Reg_upen_rxmdl_cntro_goodtest_WriteMask                                                        0x0

/*--------------------------------------
BitField Name: cntro_good_test_mdl
BitField Type: R/W
BitField Desc: value counter good
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxmdl_cntro_goodtest_cntro_good_test_mdl_Bit_Start                                         0
#define cAf6_upen_rxmdl_cntro_goodtest_cntro_good_test_mdl_Bit_End                                          31
#define cAf6_upen_rxmdl_cntro_goodtest_cntro_good_test_mdl_Mask                                       cBit31_0
#define cAf6_upen_rxmdl_cntro_goodtest_cntro_good_test_mdl_Shift                                             0
#define cAf6_upen_rxmdl_cntro_goodtest_cntro_good_test_mdl_MaxVal                                   0xffffffff
#define cAf6_upen_rxmdl_cntro_goodtest_cntro_good_test_mdl_MinVal                                          0x0
#define cAf6_upen_rxmdl_cntro_goodtest_cntro_good_test_mdl_RstVal                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER DROP MESSAGE RX MDL IDLE R2C
Reg Addr   : 0xB0C0 - 0xB0EF
Reg Formula: 0xB0C0+ $MDLID
    Where  : 
           + $MDLID (0-47) : MDL ID
Reg Desc   : 
counter drop read to clear IDLE message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmdl_cntr2c_dropidle_Base                                                         0xB0C0
#define cAf6Reg_upen_rxmdl_cntr2c_dropidle(MDLID)                                             (0xB0C0+(MDLID))
#define cAf6Reg_upen_rxmdl_cntr2c_dropidle_WidthVal                                                         32
#define cAf6Reg_upen_rxmdl_cntr2c_dropidle_WriteMask                                                       0x0

/*--------------------------------------
BitField Name: cntr2c_drop_idle_mdl
BitField Type: R/W
BitField Desc: value counter drop
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxmdl_cntr2c_dropidle_cntr2c_drop_idle_mdl_Bit_Start                                       0
#define cAf6_upen_rxmdl_cntr2c_dropidle_cntr2c_drop_idle_mdl_Bit_End                                        31
#define cAf6_upen_rxmdl_cntr2c_dropidle_cntr2c_drop_idle_mdl_Mask                                     cBit31_0
#define cAf6_upen_rxmdl_cntr2c_dropidle_cntr2c_drop_idle_mdl_Shift                                           0
#define cAf6_upen_rxmdl_cntr2c_dropidle_cntr2c_drop_idle_mdl_MaxVal                                 0xffffffff
#define cAf6_upen_rxmdl_cntr2c_dropidle_cntr2c_drop_idle_mdl_MinVal                                        0x0
#define cAf6_upen_rxmdl_cntr2c_dropidle_cntr2c_drop_idle_mdl_RstVal                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER DROP MESSAGE RX MDL IDLE RO
Reg Addr   : 0xB2C0 - 0xB2EF
Reg Formula: 0xB2C0+ $MDLID
    Where  : 
           + $MDLID (0-47) : MDL ID
Reg Desc   : 
counter drop read only IDLE message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmdl_cntro_dropidle_Base                                                          0xB2C0
#define cAf6Reg_upen_rxmdl_cntro_dropidle(MDLID)                                              (0xB2C0+(MDLID))
#define cAf6Reg_upen_rxmdl_cntro_dropidle_WidthVal                                                          32
#define cAf6Reg_upen_rxmdl_cntro_dropidle_WriteMask                                                        0x0

/*--------------------------------------
BitField Name: cntro_drop_idle_mdl
BitField Type: R/W
BitField Desc: value counter drop
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxmdl_cntro_dropidle_cntro_drop_idle_mdl_Bit_Start                                         0
#define cAf6_upen_rxmdl_cntro_dropidle_cntro_drop_idle_mdl_Bit_End                                          31
#define cAf6_upen_rxmdl_cntro_dropidle_cntro_drop_idle_mdl_Mask                                       cBit31_0
#define cAf6_upen_rxmdl_cntro_dropidle_cntro_drop_idle_mdl_Shift                                             0
#define cAf6_upen_rxmdl_cntro_dropidle_cntro_drop_idle_mdl_MaxVal                                   0xffffffff
#define cAf6_upen_rxmdl_cntro_dropidle_cntro_drop_idle_mdl_MinVal                                          0x0
#define cAf6_upen_rxmdl_cntro_dropidle_cntro_drop_idle_mdl_RstVal                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER DROP MESSAGE RX MDL PATH R2C
Reg Addr   : 0xB100 - 0xB12F
Reg Formula: 0xB100+ $MDLID
    Where  : 
           + $MDLID (0-47) : MDL ID
Reg Desc   : 
counter drop read to clear PATH message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmdl_cntr2c_droppath_Base                                                         0xB100
#define cAf6Reg_upen_rxmdl_cntr2c_droppath(MDLID)                                             (0xB100+(MDLID))
#define cAf6Reg_upen_rxmdl_cntr2c_droppath_WidthVal                                                         32
#define cAf6Reg_upen_rxmdl_cntr2c_droppath_WriteMask                                                       0x0

/*--------------------------------------
BitField Name: cntr2c_drop_path_mdl
BitField Type: R/W
BitField Desc: value counter drop
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxmdl_cntr2c_droppath_cntr2c_drop_path_mdl_Bit_Start                                       0
#define cAf6_upen_rxmdl_cntr2c_droppath_cntr2c_drop_path_mdl_Bit_End                                        31
#define cAf6_upen_rxmdl_cntr2c_droppath_cntr2c_drop_path_mdl_Mask                                     cBit31_0
#define cAf6_upen_rxmdl_cntr2c_droppath_cntr2c_drop_path_mdl_Shift                                           0
#define cAf6_upen_rxmdl_cntr2c_droppath_cntr2c_drop_path_mdl_MaxVal                                 0xffffffff
#define cAf6_upen_rxmdl_cntr2c_droppath_cntr2c_drop_path_mdl_MinVal                                        0x0
#define cAf6_upen_rxmdl_cntr2c_droppath_cntr2c_drop_path_mdl_RstVal                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER DROP MESSAGE RX MDL PATH RO
Reg Addr   : 0xB300 - 0xB32F
Reg Formula: 0xB300+ $MDLID
    Where  : 
           + $MDLID (0-47) : MDL ID
Reg Desc   : 
counter drop read only PATH message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmdl_cntro_droppath_Base                                                          0xB300
#define cAf6Reg_upen_rxmdl_cntro_droppath(MDLID)                                              (0xB300+(MDLID))
#define cAf6Reg_upen_rxmdl_cntro_droppath_WidthVal                                                          32
#define cAf6Reg_upen_rxmdl_cntro_droppath_WriteMask                                                        0x0

/*--------------------------------------
BitField Name: cntro_drop_path_mdl
BitField Type: R/W
BitField Desc: value counter drop
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxmdl_cntro_droppath_cntro_drop_path_mdl_Bit_Start                                         0
#define cAf6_upen_rxmdl_cntro_droppath_cntro_drop_path_mdl_Bit_End                                          31
#define cAf6_upen_rxmdl_cntro_droppath_cntro_drop_path_mdl_Mask                                       cBit31_0
#define cAf6_upen_rxmdl_cntro_droppath_cntro_drop_path_mdl_Shift                                             0
#define cAf6_upen_rxmdl_cntro_droppath_cntro_drop_path_mdl_MaxVal                                   0xffffffff
#define cAf6_upen_rxmdl_cntro_droppath_cntro_drop_path_mdl_MinVal                                          0x0
#define cAf6_upen_rxmdl_cntro_droppath_cntro_drop_path_mdl_RstVal                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER DROP MESSAGE RX MDL TEST R2C
Reg Addr   : 0xB140 - 0xB16F
Reg Formula: 0xB140+ $MDLID
    Where  : 
           + $MDLID (0-47) : MDL ID
Reg Desc   : 
counter drop read to clear TEST message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmdl_cntr2c_droptest_Base                                                         0xB140
#define cAf6Reg_upen_rxmdl_cntr2c_droptest(MDLID)                                             (0xB140+(MDLID))
#define cAf6Reg_upen_rxmdl_cntr2c_droptest_WidthVal                                                         32
#define cAf6Reg_upen_rxmdl_cntr2c_droptest_WriteMask                                                       0x0

/*--------------------------------------
BitField Name: cntr2c_drop_test_mdl
BitField Type: R/W
BitField Desc: value counter drop
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxmdl_cntr2c_droptest_cntr2c_drop_test_mdl_Bit_Start                                       0
#define cAf6_upen_rxmdl_cntr2c_droptest_cntr2c_drop_test_mdl_Bit_End                                        31
#define cAf6_upen_rxmdl_cntr2c_droptest_cntr2c_drop_test_mdl_Mask                                     cBit31_0
#define cAf6_upen_rxmdl_cntr2c_droptest_cntr2c_drop_test_mdl_Shift                                           0
#define cAf6_upen_rxmdl_cntr2c_droptest_cntr2c_drop_test_mdl_MaxVal                                 0xffffffff
#define cAf6_upen_rxmdl_cntr2c_droptest_cntr2c_drop_test_mdl_MinVal                                        0x0
#define cAf6_upen_rxmdl_cntr2c_droptest_cntr2c_drop_test_mdl_RstVal                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER DROP MESSAGE RX MDL TEST RO
Reg Addr   : 0xB340 - 0xB36F
Reg Formula: 0xB340+ $MDLID
    Where  : 
           + $MDLID (0-47) : MDL ID
Reg Desc   : 
counter drop read only TEST message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmdl_cntro_droptest_Base                                                          0xB340
#define cAf6Reg_upen_rxmdl_cntro_droptest(MDLID)                                              (0xB340+(MDLID))
#define cAf6Reg_upen_rxmdl_cntro_droptest_WidthVal                                                          32
#define cAf6Reg_upen_rxmdl_cntro_droptest_WriteMask                                                        0x0

/*--------------------------------------
BitField Name: cntro_drop_test_mdl
BitField Type: R/W
BitField Desc: value counter drop
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxmdl_cntro_droptest_cntro_drop_test_mdl_Bit_Start                                         0
#define cAf6_upen_rxmdl_cntro_droptest_cntro_drop_test_mdl_Bit_End                                          31
#define cAf6_upen_rxmdl_cntro_droptest_cntro_drop_test_mdl_Mask                                       cBit31_0
#define cAf6_upen_rxmdl_cntro_droptest_cntro_drop_test_mdl_Shift                                             0
#define cAf6_upen_rxmdl_cntro_droptest_cntro_drop_test_mdl_MaxVal                                   0xffffffff
#define cAf6_upen_rxmdl_cntro_droptest_cntro_drop_test_mdl_MinVal                                          0x0
#define cAf6_upen_rxmdl_cntro_droptest_cntro_drop_test_mdl_RstVal                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : PRM LB per Channel Interrupt Enable Control
Reg Addr   : 0x1A000-0x1A3FF
Reg Formula: 0x1A000+ $STSID*32 + $VTGID*4 + $VTID
    Where  :
           + $STSID (0-23) : STS ID
           + $VTGID (0-6) : VTG ID
           + $VTID (0-3) : VT ID
Reg Desc   :
This is the per Channel interrupt enable

------------------------------------------------------------------------------*/
#define cAf6Reg_prm_cfg_lben_int_Base                                                                  0x1A000
#define cAf6Reg_prm_cfg_lben_int(STSID, VTGID, VTID)                     (0x1A000+(STSID)*32+(VTGID)*4+(VTID))
#define cAf6Reg_prm_cfg_lben_int_WidthVal                                                                   32
#define cAf6Reg_prm_cfg_lben_int_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: prm_cfglben_int1
BitField Type: RW
BitField Desc: Set 1 to enable change event to generate an interrupt slice 1
BitField Bits: [1]
--------------------------------------*/
#define cAf6_prm_cfg_lben_int_prm_cfglben_int1_Bit_Start                                                     1
#define cAf6_prm_cfg_lben_int_prm_cfglben_int1_Bit_End                                                       1
#define cAf6_prm_cfg_lben_int_prm_cfglben_int1_Mask                                                      cBit1
#define cAf6_prm_cfg_lben_int_prm_cfglben_int1_Shift                                                         1
#define cAf6_prm_cfg_lben_int_prm_cfglben_int1_MaxVal                                                      0x1
#define cAf6_prm_cfg_lben_int_prm_cfglben_int1_MinVal                                                      0x0
#define cAf6_prm_cfg_lben_int_prm_cfglben_int1_RstVal                                                      0x0

/*--------------------------------------
BitField Name: prm_cfglben_int0
BitField Type: RW
BitField Desc: Set 1 to enable change event to generate an interrupt slice 0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_prm_cfg_lben_int_prm_cfglben_int0_Bit_Start                                                     0
#define cAf6_prm_cfg_lben_int_prm_cfglben_int0_Bit_End                                                       0
#define cAf6_prm_cfg_lben_int_prm_cfglben_int0_Mask                                                      cBit0
#define cAf6_prm_cfg_lben_int_prm_cfglben_int0_Shift                                                         0
#define cAf6_prm_cfg_lben_int_prm_cfglben_int0_MaxVal                                                      0x1
#define cAf6_prm_cfg_lben_int_prm_cfglben_int0_MinVal                                                      0x0
#define cAf6_prm_cfg_lben_int_prm_cfglben_int0_RstVal                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : PRM LB Interrupt per Channel Interrupt Status
Reg Addr   : 0x1A400-0x1A7FF
Reg Formula: 0x1A400+ $STSID*32 + $VTGID*4 + $VTID
    Where  : 
           + $STSID (0-23) : STS ID
           + $VTGID (0-6) : VTG ID
           + $VTID (0-3) : VT ID
Reg Desc   :
This is the per Channel interrupt status.

------------------------------------------------------------------------------*/
#define cAf6Reg_prm_lb_int_sta_Base                                                                    0x1A400
#define cAf6Reg_prm_lb_int_sta_WidthVal                                                                     32
#define cAf6Reg_prm_lb_int_sta_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: prm_lbint_sta1
BitField Type: RW
BitField Desc: Set 1 if there is a change event slice 1
BitField Bits: [1]
--------------------------------------*/
#define cAf6_prm_lb_int_sta_prm_lbint_sta1_Bit_Start                                                         1
#define cAf6_prm_lb_int_sta_prm_lbint_sta1_Bit_End                                                           1
#define cAf6_prm_lb_int_sta_prm_lbint_sta1_Mask                                                          cBit1
#define cAf6_prm_lb_int_sta_prm_lbint_sta1_Shift                                                             1
#define cAf6_prm_lb_int_sta_prm_lbint_sta1_MaxVal                                                          0x1
#define cAf6_prm_lb_int_sta_prm_lbint_sta1_MinVal                                                          0x0
#define cAf6_prm_lb_int_sta_prm_lbint_sta1_RstVal                                                          0x0

/*--------------------------------------
BitField Name: prm_lbint_sta0
BitField Type: RW
BitField Desc: Set 1 if there is a change event slice 0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_prm_lb_int_sta_prm_lbint_sta0_Bit_Start                                                         0
#define cAf6_prm_lb_int_sta_prm_lbint_sta0_Bit_End                                                           0
#define cAf6_prm_lb_int_sta_prm_lbint_sta0_Mask                                                          cBit0
#define cAf6_prm_lb_int_sta_prm_lbint_sta0_Shift                                                             0
#define cAf6_prm_lb_int_sta_prm_lbint_sta0_MaxVal                                                          0x1
#define cAf6_prm_lb_int_sta_prm_lbint_sta0_MinVal                                                          0x0
#define cAf6_prm_lb_int_sta_prm_lbint_sta0_RstVal                                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : PRM LB Interrupt per Channel Current Status
Reg Addr   : 0x1A800-0x1ABFF
Reg Formula: 0x1A800+ $STSID*32 + $VTGID*4 + $VTID
    Where  : 
           + $STSID (0-23) : STS ID
           + $VTGID (0-6) : VTG ID
           + $VTID (0-3) : VT ID
Reg Desc   :
This is the per Channel Current status.

------------------------------------------------------------------------------*/
#define cAf6Reg_prm_lb_int_crrsta_Base                                                                 0x1A800
#define cAf6Reg_prm_lb_int_crrsta_WidthVal                                                                  32
#define cAf6Reg_prm_lb_int_crrsta_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: prm_lbint_crrsta1
BitField Type: RW
BitField Desc: Current status of event slice 1
BitField Bits: [1]
--------------------------------------*/
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta1_Bit_Start                                                   1
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta1_Bit_End                                                     1
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta1_Mask                                                    cBit1
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta1_Shift                                                       1
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta1_MaxVal                                                    0x1
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta1_MinVal                                                    0x0
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta1_RstVal                                                    0x0

/*--------------------------------------
BitField Name: prm_lbint_crrsta0
BitField Type: RW
BitField Desc: Current status of event slice 0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta0_Bit_Start                                                   0
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta0_Bit_End                                                     0
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta0_Mask                                                    cBit0
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta0_Shift                                                       0
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta0_MaxVal                                                    0x1
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta0_MinVal                                                    0x0
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta0_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : PRM LB Interrupt per Channel Interrupt OR Status
Reg Addr   : 0x1AC00-0x1AC20
Reg Formula: 0x1AC00 +  $GID
    Where  : 
           + $GID (0-31) : group ID
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_prm_lb_intsta_Base                                                                     0x1AC00
#define cAf6Reg_prm_lb_intsta(GID)                                                             (0x1AC00+(GID))
#define cAf6Reg_prm_lb_intsta_WidthVal                                                                      32
#define cAf6Reg_prm_lb_intsta_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: prm_lbintsta
BitField Type: RW
BitField Desc: Set to 1 if any interrupt status bit of corresponding channel is
set and its interrupt is enabled.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_prm_lb_intsta_prm_lbintsta_Bit_Start                                                            0
#define cAf6_prm_lb_intsta_prm_lbintsta_Bit_End                                                             31
#define cAf6_prm_lb_intsta_prm_lbintsta_Mask                                                          cBit31_0
#define cAf6_prm_lb_intsta_prm_lbintsta_Shift                                                                0
#define cAf6_prm_lb_intsta_prm_lbintsta_MaxVal                                                      0xffffffff
#define cAf6_prm_lb_intsta_prm_lbintsta_MinVal                                                             0x0
#define cAf6_prm_lb_intsta_prm_lbintsta_RstVal                                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : PRM LB Interrupt OR Status
Reg Addr   : 0x1AFFF
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 2 bits. Each bit is used to store Interrupt OR status.

------------------------------------------------------------------------------*/
#define cAf6Reg_prm_lb_sta_int_Base                                                                    0x1AFFF
#define cAf6Reg_prm_lb_sta_int                                                                         0x1AFFF
#define cAf6Reg_prm_lb_sta_int_WidthVal                                                                     32
#define cAf6Reg_prm_lb_sta_int_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: prm_lbsta_int
BitField Type: RW
BitField Desc: Set to 1 if any interrupt status bit is set and its interrupt is
enabled
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_prm_lb_sta_int_prm_lbsta_int_Bit_Start                                                          0
#define cAf6_prm_lb_sta_int_prm_lbsta_int_Bit_End                                                           31
#define cAf6_prm_lb_sta_int_prm_lbsta_int_Mask                                                        cBit31_0
#define cAf6_prm_lb_sta_int_prm_lbsta_int_Shift                                                              0
#define cAf6_prm_lb_sta_int_prm_lbsta_int_MaxVal                                                    0xffffffff
#define cAf6_prm_lb_sta_int_prm_lbsta_int_MinVal                                                           0x0
#define cAf6_prm_lb_sta_int_prm_lbsta_int_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : PRM LB Interrupt Enable Control
Reg Addr   : 0x1AFFE
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 2 interrupt enable bits .

------------------------------------------------------------------------------*/
#define cAf6Reg_prm_lb_en_int_Base                                                                     0x1AFFE
#define cAf6Reg_prm_lb_en_int                                                                          0x1AFFE
#define cAf6Reg_prm_lb_en_int_WidthVal                                                                      32
#define cAf6Reg_prm_lb_en_int_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: prm_lben_int
BitField Type: RW
BitField Desc: Set to 1 to enable to generate interrupt.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_prm_lb_en_int_prm_lben_int_Bit_Start                                                            0
#define cAf6_prm_lb_en_int_prm_lben_int_Bit_End                                                             31
#define cAf6_prm_lb_en_int_prm_lben_int_Mask                                                          cBit31_0
#define cAf6_prm_lb_en_int_prm_lben_int_Shift                                                                0
#define cAf6_prm_lb_en_int_prm_lben_int_MaxVal                                                      0xffffffff
#define cAf6_prm_lb_en_int_prm_lben_int_MinVal                                                             0x0
#define cAf6_prm_lb_en_int_prm_lben_int_RstVal                                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : MDL per Channel Interrupt Enable Control
Reg Addr   : 0xA100-0xA13F
Reg Formula: 0xA100 +  $MDLDE3ID + $LINEID * 32
    Where  : 
           + $MDLDE3ID (0-23) : DE3 ID
           + $LINEID(0-1) : LINE ID
Reg Desc   : 
This is the per Channel interrupt enable

------------------------------------------------------------------------------*/
#define cAf6Reg_mdl_cfgen_int_Base                                                                      0xA100
#define cAf6Reg_mdl_cfgen_int(MDLDE3ID, LINEID)                                (0xA100+(MDLDE3ID)+(LINEID)*32)
#define cAf6Reg_mdl_cfgen_int_WidthVal                                                                      32
#define cAf6Reg_mdl_cfgen_int_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: mdl_cfgen_int
BitField Type: RW
BitField Desc: Set 1 to enable change event to generate an interrupt.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_mdl_cfgen_int_mdl_cfgen_int_Bit_Start                                                           0
#define cAf6_mdl_cfgen_int_mdl_cfgen_int_Bit_End                                                             0
#define cAf6_mdl_cfgen_int_mdl_cfgen_int_Mask                                                            cBit0
#define cAf6_mdl_cfgen_int_mdl_cfgen_int_Shift                                                               0
#define cAf6_mdl_cfgen_int_mdl_cfgen_int_MaxVal                                                            0x1
#define cAf6_mdl_cfgen_int_mdl_cfgen_int_MinVal                                                            0x0
#define cAf6_mdl_cfgen_int_mdl_cfgen_int_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : MDL Interrupt per Channel Interrupt Status
Reg Addr   : 0xA140-0xA17F
Reg Formula: 0xA140 +  $MDLDE3ID + $LINEID * 32
    Where  : 
           + $MDLDE3ID (0-23) : DE3 ID
           + $LINEID(0-1) : LINE ID
Reg Desc   : 
This is the per Channel interrupt status.

------------------------------------------------------------------------------*/
#define cAf6Reg_mdl_int_sta_Base                                                                        0xA140
#define cAf6Reg_mdl_int_sta(MDLDE3ID, LINEID)                                  (0xA140+(MDLDE3ID)+(LINEID)*32)
#define cAf6Reg_mdl_int_sta_WidthVal                                                                        32
#define cAf6Reg_mdl_int_sta_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: mdlint_sta
BitField Type: RW
BitField Desc: Set 1 if there is a change event.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_mdl_int_sta_mdlint_sta_Bit_Start                                                                0
#define cAf6_mdl_int_sta_mdlint_sta_Bit_End                                                                  0
#define cAf6_mdl_int_sta_mdlint_sta_Mask                                                                 cBit0
#define cAf6_mdl_int_sta_mdlint_sta_Shift                                                                    0
#define cAf6_mdl_int_sta_mdlint_sta_MaxVal                                                                 0x1
#define cAf6_mdl_int_sta_mdlint_sta_MinVal                                                                 0x0
#define cAf6_mdl_int_sta_mdlint_sta_RstVal                                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : MDL Interrupt per Channel Current Status
Reg Addr   : 0xA180-0xA1AF
Reg Formula: 0xA180 +  $MDLDE3ID + $LINEID * 32
    Where  : 
           + $MDLDE3ID (0-23) : DE3 ID
           + $LINEID(0-1) : LINE ID
Reg Desc   : 
This is the per Channel Current status.

------------------------------------------------------------------------------*/
#define cAf6Reg_mdl_int_crrsta_Base                                                                     0xA180
#define cAf6Reg_mdl_int_crrsta(MDLDE3ID, LINEID)                               (0xA180+(MDLDE3ID)+(LINEID)*32)
#define cAf6Reg_mdl_int_crrsta_WidthVal                                                                     32
#define cAf6Reg_mdl_int_crrsta_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: mdlint_crrsta
BitField Type: RW
BitField Desc: Current status of event.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_mdl_int_crrsta_mdlint_crrsta_Bit_Start                                                          0
#define cAf6_mdl_int_crrsta_mdlint_crrsta_Bit_End                                                            0
#define cAf6_mdl_int_crrsta_mdlint_crrsta_Mask                                                           cBit0
#define cAf6_mdl_int_crrsta_mdlint_crrsta_Shift                                                              0
#define cAf6_mdl_int_crrsta_mdlint_crrsta_MaxVal                                                           0x1
#define cAf6_mdl_int_crrsta_mdlint_crrsta_MinVal                                                           0x0
#define cAf6_mdl_int_crrsta_mdlint_crrsta_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : MDL Interrupt per Channel Interrupt OR Status
Reg Addr   : 0xA1C0-0xA1C1
Reg Formula: 0xA1C0 +  $GID
    Where  : 
           + $GID (0-1) : group ID
Reg Desc   : 
The register consists of 32 bits for 32 VT/TUs of the related STS/VC in the Rx DS1/E1/J1 Framer. Each bit is used to store Interrupt OR tus of the related DS1/E1/J1.

------------------------------------------------------------------------------*/
#define cAf6Reg_mdl_intsta_Base                                                                         0xA1C0
#define cAf6Reg_mdl_intsta(GID)                                                                 (0xA1C0+(GID))
#define cAf6Reg_mdl_intsta_WidthVal                                                                         32
#define cAf6Reg_mdl_intsta_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: mdlintsta
BitField Type: RW
BitField Desc: Set to 1 if any interrupt status bit of corresponding channel is
set and its interrupt is enabled.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_mdl_intsta_mdlintsta_Bit_Start                                                                  0
#define cAf6_mdl_intsta_mdlintsta_Bit_End                                                                   31
#define cAf6_mdl_intsta_mdlintsta_Mask                                                                cBit31_0
#define cAf6_mdl_intsta_mdlintsta_Shift                                                                      0
#define cAf6_mdl_intsta_mdlintsta_MaxVal                                                            0xffffffff
#define cAf6_mdl_intsta_mdlintsta_MinVal                                                                   0x0
#define cAf6_mdl_intsta_mdlintsta_RstVal                                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : MDL Interrupt OR Status
Reg Addr   : 0xA1FF
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 2 bits. Each bit is used to store Interrupt OR status.

------------------------------------------------------------------------------*/
#define cAf6Reg_mdl_sta_int_Base                                                                        0xA1FF
#define cAf6Reg_mdl_sta_int                                                                             0xA1FF
#define cAf6Reg_mdl_sta_int_WidthVal                                                                        32
#define cAf6Reg_mdl_sta_int_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: mdlsta_int
BitField Type: RW
BitField Desc: Set to 1 if any interrupt status bit is set and its interrupt is
enabled
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_mdl_sta_int_mdlsta_int_Bit_Start                                                                0
#define cAf6_mdl_sta_int_mdlsta_int_Bit_End                                                                  1
#define cAf6_mdl_sta_int_mdlsta_int_Mask                                                               cBit1_0
#define cAf6_mdl_sta_int_mdlsta_int_Shift                                                                    0
#define cAf6_mdl_sta_int_mdlsta_int_MaxVal                                                                 0x3
#define cAf6_mdl_sta_int_mdlsta_int_MinVal                                                                 0x0
#define cAf6_mdl_sta_int_mdlsta_int_RstVal                                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : MDL Interrupt Enable Control
Reg Addr   : 0xA1FE
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 2 interrupt enable bits .

------------------------------------------------------------------------------*/
#define cAf6Reg_mdl_en_int_Base                                                                         0xA1FE
#define cAf6Reg_mdl_en_int                                                                              0xA1FE
#define cAf6Reg_mdl_en_int_WidthVal                                                                         32
#define cAf6Reg_mdl_en_int_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: mdlen_int
BitField Type: RW
BitField Desc: Set to 1 to enable to generate interrupt.
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_mdl_en_int_mdlen_int_Bit_Start                                                                  0
#define cAf6_mdl_en_int_mdlen_int_Bit_End                                                                    1
#define cAf6_mdl_en_int_mdlen_int_Mask                                                                 cBit1_0
#define cAf6_mdl_en_int_mdlen_int_Shift                                                                      0
#define cAf6_mdl_en_int_mdlen_int_MaxVal                                                                   0x3
#define cAf6_mdl_en_int_mdlen_int_MinVal                                                                   0x0
#define cAf6_mdl_en_int_mdlen_int_RstVal                                                                   0x0

#endif /* _AF6_REG_AF6CCI0031_RD_PDH_MDLPRM_H_ */

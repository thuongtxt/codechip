/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60210031PdhDe1Common.h
 *
 * Created Date: Aug, 2015
 *
 * Description : DE1 common functions
 *
 * Notes       : This will share between M13/VC-De1 function.
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031PDHDE1COMMON_H_
#define _THA60210031PDHDE1COMMON_H_

/*--------------------------- Include files ----------------------------------*/
#include "AtDevice.h"
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../../default/pdh/ThaModulePdhForStmReg.h"
#include "Tha60210031ModulePdh.h"
#include "Tha60210031ModulePdhReg.h"
#include "Tha60210031PdhVcDe1Internal.h"
#include "Tha60210031PdhDe2De1Internal.h"
#include "Tha60210031PdhDe1.h"


/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods    m_AtChannelOverride;
static tThaPdhDe1Methods    m_ThaPdhDe1Override;
static tAtPdhChannelMethods m_AtPdhChannelOverride;

/* Save super implementation */
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;
static const tAtPdhChannelMethods *m_AtPdhChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 De1ErrCounterOffset(ThaPdhDe1 de1, eBool r2cEn)
    {
    uint32 offset = mMethodsGet(de1)->Ds1E1J1DefaultOffset(de1);
    return r2cEn ? offset : (1024 + offset);
    }

static eAtRet InterruptEnable(AtChannel self)
    {
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
    return Tha6021PdhDe1SupportedInterruptMasks(self);
    }

static eAtRet InterruptDisable(AtChannel self)
    {
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static eBool InterruptIsEnabled(AtChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet Debug(AtChannel self)
    {
    m_AtChannelMethods->Debug(self);
    AtPrintc(cSevNormal, "* AddressOffset: 0x%08x\r\n", ThaPdhDe1DefaultOffset((ThaPdhDe1)self));
    return cAtOk;
    }

static eAtRet DisableLiuOverSonet(AtPdhChannel self)
    {
    ThaStmModulePdh modulePdh = (ThaStmModulePdh)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cAtModulePdh);
    AtSdhChannel vc1x = (AtSdhChannel)AtPdhChannelVcInternalGet(self);

    if (!ThaStmModulePdhLiuDe3OverVc3IsSupported((ThaStmModulePdh)modulePdh))
        return cAtOk;

    /* Only handle for de1 over Vc1x */
    if (vc1x == NULL)
        return cAtOk;

    return ThaPdhStsVtDe1LiuMapVc1xEnable(modulePdh, vc1x, cAtFalse);
    }

static eAtRet FrameTypeSet(AtPdhChannel self, uint16 frameType)
    {
    eAtRet ret;

    /* Let super deal with working logic */
    ret = m_AtPdhChannelMethods->FrameTypeSet(self, frameType);
    if (ret != cAtOk)
        return ret;

    return DisableLiuOverSonet(self);
    }

static void ListenerNotify(AtChannel self, tAtChannelEventListener *listener, void *userData)
    {
    uint32 currentAlarm  = AtChannelDefectGet(self);

    if ((currentAlarm & cAtPdhDe1AlarmAis) == 0)
        {
        if (userData && listener->AlarmChangeStateWithUserData)
            {
            listener->AlarmChangeStateWithUserData(self, cAtPdhDe1AlarmAis, 0, userData);
            return;
            }

        if (listener->AlarmChangeState)
            listener->AlarmChangeState(self, cAtPdhDe1AlarmAis, 0);
        }
    }

static void ListenerDidAdd(AtChannel self, tAtChannelEventListener *listener, void *userData)
    {
    if (AtModulePdhNeedClearanceNotifyLogic((AtModulePdh)AtChannelModuleGet(self)))
        ListenerNotify(self, listener, userData);
    }

static void OverrideThaPdhDe1(AtPdhDe1 self)
    {
    ThaPdhDe1 de1 = (ThaPdhDe1)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhDe1Override, mMethodsGet(de1), sizeof(m_ThaPdhDe1Override));

        mMethodOverride(m_ThaPdhDe1Override, De1ErrCounterOffset);
        }

    mMethodsSet(de1, &m_ThaPdhDe1Override);
    }

static void OverrideAtChannel(AtPdhDe1 self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, InterruptIsEnabled);
        mMethodOverride(m_AtChannelOverride, InterruptEnable);
        mMethodOverride(m_AtChannelOverride, InterruptDisable);
        mMethodOverride(m_AtChannelOverride, Debug);
        mMethodOverride(m_AtChannelOverride, ListenerDidAdd);
        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtPdhChannel(AtPdhDe1 self)
    {
    AtPdhChannel channel = (AtPdhChannel)self;
    
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPdhChannelOverride, m_AtPdhChannelMethods, sizeof(tAtPdhChannelMethods));

        mMethodOverride(m_AtPdhChannelOverride, FrameTypeSet);
        }

    mMethodsSet(channel, &m_AtPdhChannelOverride);
    }

static void Override(AtPdhDe1 self)
    {
    OverrideAtChannel(self);
    OverrideThaPdhDe1(self);
    OverrideAtPdhChannel(self);
    }

#endif /* _THA60210031PDHDE1COMMON_H_ */

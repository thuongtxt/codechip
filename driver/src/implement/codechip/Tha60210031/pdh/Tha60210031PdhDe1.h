/*-----------------------------------------------------------------------------
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies. 
 *
 * Module      : PDH
 *
 * File        : Tha60210031PdhDe1.h
 *
 * Created Date: Aug 5, 2015
 *
 * Description : PDH DS1/E1 header.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031PDHDE1_H_
#define _THA60210031PDHDE1_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pdh/ThaPdhDe2De1.h"

#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031PdhDe2De1 * Tha60210031PdhDe2De1;
typedef struct tTha60210031PdhVcDe1 * Tha60210031PdhVcDe1;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/
AtPdhDe1 Tha60210031PdhVcDe1ObjectInit(AtPdhDe1 self, AtSdhVc vc1x, AtModulePdh module);
AtPdhDe1 Tha60210031PdhDe2De1ObjectInit(AtPdhDe1 self, uint32 channelId, AtModulePdh module);

AtPdhDe1 Tha60210031PdhDe2De1New(uint32 channelId, AtModulePdh module);
AtPdhDe1 Tha60210031PdhVcDe1New(AtSdhVc vc1x, AtModulePdh module);

uint8 Tha60210031Tu1xOrDe1IdGet(ThaPdhDe1 self, uint8 *tugOrDe2);
uint32 Tha6021PdhDe1SupportedInterruptMasks(AtChannel self);

#ifdef __cplusplus
}
#endif

#endif /* _THA60210031PDHDE1_H_ */

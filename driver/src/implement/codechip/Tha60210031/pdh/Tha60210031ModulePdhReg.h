/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive
 * Technologies. The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      :
 *
 * File        :
 *
 * Created Date:
 *
 * Description : This file contain all constance definitions of  block.
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CCI0031_RD_PDH_H_
#define _AF6_REG_AF6CCI0031_RD_PDH_H_

/*--------------------------- Define -----------------------------------------*/
/*------------------------------------------------------------------------------
Reg Name   : PDH Global Registers
Reg Addr   : 0x00000000 - 0x00000000
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the global configuration register for the PDH

------------------------------------------------------------------------------*/
#define cAf6Reg_glb_Base                                                                            0x00000000
#define cAf6Reg_glb                                                                                 0x00000000
#define cAf6Reg_glb_WidthVal                                                                                32
#define cAf6Reg_glb_WriteMask                                                                              0x0

/*--------------------------------------
BitField Name: PDHE13IdConv
BitField Type: RW
BitField Desc: Set 1 to enable ID conversion of the E1 in an E3 as following ID
in an E3 (bit 4:0) ID at Data Map (bit 4:0) 5'd0                  5'd0 5'd1
5'd1 5'd2                  5'd2 5'd3                  5'd4 5'd4
5'd5 5'd5                  5'd6 5'd6                  5'd8 5'd7
5'd9 5'd8                  5'd10 5'd9                  5'd12 5'd10
5'd13 5'd11                 5'd14 5'd12                 5'd16 5'd13
5'd17 5'd14                 5'd18 5'd15                 5'd20
BitField Bits: [9]
--------------------------------------*/
#define cAf6_glb_PDHE13IdConv_Bit_Start                                                                      9
#define cAf6_glb_PDHE13IdConv_Bit_End                                                                        9
#define cAf6_glb_PDHE13IdConv_Mask                                                                       cBit9
#define cAf6_glb_PDHE13IdConv_Shift                                                                          9
#define cAf6_glb_PDHE13IdConv_MaxVal                                                                       0x1
#define cAf6_glb_PDHE13IdConv_MinVal                                                                       0x0
#define cAf6_glb_PDHE13IdConv_RstVal                                                                       0x0

/*--------------------------------------
BitField Name: PDHFullSonetSDH
BitField Type: RW
BitField Desc: Set 1 to enable full SONET/SDH mode (no traffic on PDH interface)
BitField Bits: [8]
--------------------------------------*/
#define cAf6_glb_PDHFullSonetSDH_Bit_Start                                                                   8
#define cAf6_glb_PDHFullSonetSDH_Bit_End                                                                     8
#define cAf6_glb_PDHFullSonetSDH_Mask                                                                    cBit8
#define cAf6_glb_PDHFullSonetSDH_Shift                                                                       8
#define cAf6_glb_PDHFullSonetSDH_MaxVal                                                                    0x1
#define cAf6_glb_PDHFullSonetSDH_MinVal                                                                    0x0
#define cAf6_glb_PDHFullSonetSDH_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: PDHFullLineLoop
BitField Type: RW
BitField Desc: Set 1 to enable full Line Loop Back at Data map side
BitField Bits: [7]
--------------------------------------*/
#define cAf6_glb_PDHFullLineLoop_Bit_Start                                                                   7
#define cAf6_glb_PDHFullLineLoop_Bit_End                                                                     7
#define cAf6_glb_PDHFullLineLoop_Mask                                                                    cBit7
#define cAf6_glb_PDHFullLineLoop_Shift                                                                       7
#define cAf6_glb_PDHFullLineLoop_MaxVal                                                                    0x1
#define cAf6_glb_PDHFullLineLoop_MinVal                                                                    0x0
#define cAf6_glb_PDHFullLineLoop_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: PDHFullPayLoop
BitField Type: RW
BitField Desc: Set 1 to enable full Payload Loop back at Data map side
BitField Bits: [6]
--------------------------------------*/
#define cAf6_glb_PDHFullPayLoop_Bit_Start                                                                    6
#define cAf6_glb_PDHFullPayLoop_Bit_End                                                                      6
#define cAf6_glb_PDHFullPayLoop_Mask                                                                     cBit6
#define cAf6_glb_PDHFullPayLoop_Shift                                                                        6
#define cAf6_glb_PDHFullPayLoop_MaxVal                                                                     0x1
#define cAf6_glb_PDHFullPayLoop_MinVal                                                                     0x0
#define cAf6_glb_PDHFullPayLoop_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: PDHFullPDHBus
BitField Type: RW
BitField Desc: Set 1 to enable Full traffic on PDH Bus
BitField Bits: [5]
--------------------------------------*/
#define cAf6_glb_PDHFullPDHBus_Bit_Start                                                                     5
#define cAf6_glb_PDHFullPDHBus_Bit_End                                                                       5
#define cAf6_glb_PDHFullPDHBus_Mask                                                                      cBit5
#define cAf6_glb_PDHFullPDHBus_Shift                                                                         5
#define cAf6_glb_PDHFullPDHBus_MaxVal                                                                      0x1
#define cAf6_glb_PDHFullPDHBus_MinVal                                                                      0x0
#define cAf6_glb_PDHFullPDHBus_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: PDHParallelPDHMd
BitField Type: RW
BitField Desc: Set 1 to enable Parallel PDH Bus mode
BitField Bits: [4]
--------------------------------------*/
#define cAf6_glb_PDHParallelPDHMd_Bit_Start                                                                  4
#define cAf6_glb_PDHParallelPDHMd_Bit_End                                                                    4
#define cAf6_glb_PDHParallelPDHMd_Mask                                                                   cBit4
#define cAf6_glb_PDHParallelPDHMd_Shift                                                                      4
#define cAf6_glb_PDHParallelPDHMd_MaxVal                                                                   0x1
#define cAf6_glb_PDHParallelPDHMd_MinVal                                                                   0x0
#define cAf6_glb_PDHParallelPDHMd_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: PDHSaturation
BitField Type: RW
BitField Desc: Set 1 to enable Saturation mode of all PDH counters. Set 0 for
Roll-Over mode
BitField Bits: [3]
--------------------------------------*/
#define cAf6_glb_PDHSaturation_Bit_Start                                                                     3
#define cAf6_glb_PDHSaturation_Bit_End                                                                       3
#define cAf6_glb_PDHSaturation_Mask                                                                      cBit3
#define cAf6_glb_PDHSaturation_Shift                                                                         3
#define cAf6_glb_PDHSaturation_MaxVal                                                                      0x1
#define cAf6_glb_PDHSaturation_MinVal                                                                      0x0
#define cAf6_glb_PDHSaturation_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: PDHReservedBit
BitField Type: RW
BitField Desc: Transmit Reserved Bit
BitField Bits: [2]
--------------------------------------*/
#define cAf6_glb_PDHReservedBit_Bit_Start                                                                    2
#define cAf6_glb_PDHReservedBit_Bit_End                                                                      2
#define cAf6_glb_PDHReservedBit_Mask                                                                     cBit2
#define cAf6_glb_PDHReservedBit_Shift                                                                        2
#define cAf6_glb_PDHReservedBit_MaxVal                                                                     0x1
#define cAf6_glb_PDHReservedBit_MinVal                                                                     0x0
#define cAf6_glb_PDHReservedBit_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: PDHStufftBit
BitField Type: RW
BitField Desc: Transmit Stuffing Bit
BitField Bits: [1]
--------------------------------------*/
#define cAf6_glb_PDHStufftBit_Bit_Start                                                                      1
#define cAf6_glb_PDHStufftBit_Bit_End                                                                        1
#define cAf6_glb_PDHStufftBit_Mask                                                                       cBit1
#define cAf6_glb_PDHStufftBit_Shift                                                                          1
#define cAf6_glb_PDHStufftBit_MaxVal                                                                       0x1
#define cAf6_glb_PDHStufftBit_MinVal                                                                       0x0
#define cAf6_glb_PDHStufftBit_RstVal                                                                       0x0

/*--------------------------------------
BitField Name: PDHNoIDConv
BitField Type: RW
BitField Desc: Set 1 to enable no ID convert at PDH Block
BitField Bits: [0]
--------------------------------------*/
#define cAf6_glb_PDHNoIDConv_Bit_Start                                                                       0
#define cAf6_glb_PDHNoIDConv_Bit_End                                                                         0
#define cAf6_glb_PDHNoIDConv_Mask                                                                        cBit0
#define cAf6_glb_PDHNoIDConv_Shift                                                                           0
#define cAf6_glb_PDHNoIDConv_MaxVal                                                                        0x1
#define cAf6_glb_PDHNoIDConv_MinVal                                                                        0x0
#define cAf6_glb_PDHNoIDConv_RstVal                                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : STS/VT Demap Control
Reg Addr   : 0x00024000 - 0x000243FF
Reg Formula: 0x00024000 +  32*stsid + 4*vtgid + vtid
    Where  : 
           + $stsid(0-23):
           + $vtgid(0-6):
           + $vtid(0-3)
Reg Desc   : 
The STS/VT Demap Control is use to configure for per channel STS/VT demap operation.

------------------------------------------------------------------------------*/
#define cAf6Reg_stsvt_demap_ctrl_Base                                                               0x00024000
#define cAf6Reg_stsvt_demap_ctrl(stsid, vtgid, vtid)                  (0x00024000+32*(stsid)+4*(vtgid)+(vtid))
#define cAf6Reg_stsvt_demap_ctrl_WidthVal                                                                   32
#define cAf6Reg_stsvt_demap_ctrl_WriteMask                                                                 0x0

/*--------------------------------------
Field: [9] %%  Pdhoversdhmd %% Set 1 to PDH LIU over OCN %% RW %% 0x0 %% 0x0
--------------------------------------*/
#define cAf6_stsvt_demap_ctrl_Pdhoversdhmd_Mask                                                         cBit9
#define cAf6_stsvt_demap_ctrl_Pdhoversdhmd_Shift                                                            9

/*--------------------------------------
BitField Name: StsVtDemapFrcAis
BitField Type: RW
BitField Desc: This bit is set to 1 to force the AIS to downstream data. 1:
force AIS. 0: not force AIS.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_stsvt_demap_ctrl_StsVtDemapFrcAis_Bit_Start                                                     8
#define cAf6_stsvt_demap_ctrl_StsVtDemapFrcAis_Bit_End                                                       8
#define cAf6_stsvt_demap_ctrl_StsVtDemapFrcAis_Mask                                                      cBit8
#define cAf6_stsvt_demap_ctrl_StsVtDemapFrcAis_Shift                                                         8
#define cAf6_stsvt_demap_ctrl_StsVtDemapFrcAis_MaxVal                                                      0x1
#define cAf6_stsvt_demap_ctrl_StsVtDemapFrcAis_MinVal                                                      0x0
#define cAf6_stsvt_demap_ctrl_StsVtDemapFrcAis_RstVal                                                      0x0

/*--------------------------------------
BitField Name: StsVtDemapAutoAis
BitField Type: RW
BitField Desc: This bit is set to 1 to automatically generate AIS to downstream
data whenever the high-order STS/VT is in AIS. 1: auto AIS generation. 0: not
auto AIS generation
BitField Bits: [7]
--------------------------------------*/
#define cAf6_stsvt_demap_ctrl_StsVtDemapAutoAis_Bit_Start                                                    7
#define cAf6_stsvt_demap_ctrl_StsVtDemapAutoAis_Bit_End                                                      7
#define cAf6_stsvt_demap_ctrl_StsVtDemapAutoAis_Mask                                                     cBit7
#define cAf6_stsvt_demap_ctrl_StsVtDemapAutoAis_Shift                                                        7
#define cAf6_stsvt_demap_ctrl_StsVtDemapAutoAis_MaxVal                                                     0x1
#define cAf6_stsvt_demap_ctrl_StsVtDemapAutoAis_MinVal                                                     0x0
#define cAf6_stsvt_demap_ctrl_StsVtDemapAutoAis_RstVal                                                     0x0

/*--------------------------------------
BitField Name: StsVtDemapCepMode
BitField Type: RW
BitField Desc: STS/VC CEP fractional mode
BitField Bits: [6]
--------------------------------------*/
#define cAf6_stsvt_demap_ctrl_StsVtDemapCepMode_Bit_Start                                                    6
#define cAf6_stsvt_demap_ctrl_StsVtDemapCepMode_Bit_End                                                      6
#define cAf6_stsvt_demap_ctrl_StsVtDemapCepMode_Mask                                                     cBit6
#define cAf6_stsvt_demap_ctrl_StsVtDemapCepMode_Shift                                                        6
#define cAf6_stsvt_demap_ctrl_StsVtDemapCepMode_MaxVal                                                     0x1
#define cAf6_stsvt_demap_ctrl_StsVtDemapCepMode_MinVal                                                     0x0
#define cAf6_stsvt_demap_ctrl_StsVtDemapCepMode_RstVal                                                     0x0

/*--------------------------------------
BitField Name: StsVtDemapFrmtype
BitField Type: RW
BitField Desc: STS/VT Frmtype configure
BitField Bits: [5:4]
--------------------------------------*/
#define cAf6_stsvt_demap_ctrl_StsVtDemapFrmtype_Bit_Start                                                    4
#define cAf6_stsvt_demap_ctrl_StsVtDemapFrmtype_Bit_End                                                      5
#define cAf6_stsvt_demap_ctrl_StsVtDemapFrmtype_Mask                                                   cBit5_4
#define cAf6_stsvt_demap_ctrl_StsVtDemapFrmtype_Shift                                                        4
#define cAf6_stsvt_demap_ctrl_StsVtDemapFrmtype_MaxVal                                                     0x3
#define cAf6_stsvt_demap_ctrl_StsVtDemapFrmtype_MinVal                                                     0x0
#define cAf6_stsvt_demap_ctrl_StsVtDemapFrmtype_RstVal                                                     0x0

/*--------------------------------------
BitField Name: StsVtDemapSigtype
BitField Type: RW
BitField Desc: STS/VT Sigtype configure Sigtype[3:0] Frmtype[1:0] CepMode
Operation Mode 0 x x VT/TU to DS1 Demap 1 x x VT/TU to E1 Demap 2 x 0 STS1/VC3
to DS3 Demap 2 x 1 TUG3/TU3 to DS3 Demap 3 x 0 STS1/VC3 to E3 Demap 3 x 1
TUG3/TU3 to E3 Demap 4 0 x Packet over VT1.5/TU11 4 1 x Reserved 4 2 x Reserved
4 3 x VT1.5/TU11 Basic CEP 5 0 x Packet over VT2/TU12 5 1 x Reserved 5 2 x
Reserved 5 3 x VT2/TU12 Basic CEP 6 x x Reserved 7 x x Reserved 8 0 x Packet
over STS1/VC3 8 1 0 STS1/VC3 Fractional CEP carrying VT2/TU12 8 1 1 STS1/VC3
Fractional CEP carrying VT15/TU11 8 2 0 STS1/VC3 Fractional CEP carrying E3 8 2
1 STS1/VC3 Fractional CEP carrying DS3 8 3 x STS1/VC3 Basic CEP 9 0 x Packet
over STS3c/VC4 9 1 0 STS3c/VC4 Fractional CEP carrying VT2/TU12 9 1 1 STS3c/VC4
Fractional CEP carrying VT15/TU11 9 2 0 STS3c/VC4 Fractional CEP carrying E3 9 2
1 STS3c/VC4 Fractional CEP carrying DS3 9 3 x STS3c/VC4 Basic CEP 10 3 x
STS12c/VC4_4c Basic CEP 11 x x Reserved 12 x x Packet over TU3 13 x x Reserved
14 x x Reserved 15 x x Disable STS/VC/VT/TU/DS1/E1/DS3/E3
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_stsvt_demap_ctrl_StsVtDemapSigtype_Bit_Start                                                    0
#define cAf6_stsvt_demap_ctrl_StsVtDemapSigtype_Bit_End                                                      3
#define cAf6_stsvt_demap_ctrl_StsVtDemapSigtype_Mask                                                   cBit3_0
#define cAf6_stsvt_demap_ctrl_StsVtDemapSigtype_Shift                                                        0
#define cAf6_stsvt_demap_ctrl_StsVtDemapSigtype_MaxVal                                                     0xf
#define cAf6_stsvt_demap_ctrl_StsVtDemapSigtype_MinVal                                                     0x0
#define cAf6_stsvt_demap_ctrl_StsVtDemapSigtype_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : STS/VT Demap HW Status
Reg Addr   : 0x00024400 - 0x000247FF
Reg Formula: 
    Where  : 
Reg Desc   : 
for HW debug only

------------------------------------------------------------------------------*/
#define cAf6Reg_stsvt_demap_hw_stat_Base                                                            0x00024400
#define cAf6Reg_stsvt_demap_hw_stat                                                                 0x00024400
#define cAf6Reg_stsvt_demap_hw_stat_WidthVal                                                                32
#define cAf6Reg_stsvt_demap_hw_stat_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: STSVTDemapStatus
BitField Type: RW
BitField Desc: for HW debug only
BitField Bits: [26:0]
--------------------------------------*/
#define cAf6_stsvt_demap_hw_stat_STSVTDemapStatus_Bit_Start                                                  0
#define cAf6_stsvt_demap_hw_stat_STSVTDemapStatus_Bit_End                                                   26
#define cAf6_stsvt_demap_hw_stat_STSVTDemapStatus_Mask                                                cBit26_0
#define cAf6_stsvt_demap_hw_stat_STSVTDemapStatus_Shift                                                      0
#define cAf6_stsvt_demap_hw_stat_STSVTDemapStatus_MaxVal                                             0x7ffffff
#define cAf6_stsvt_demap_hw_stat_STSVTDemapStatus_MinVal                                                   0x0
#define cAf6_stsvt_demap_hw_stat_STSVTDemapStatus_RstVal                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : STS/VT Demap Output Debug PRBS
Reg Addr   : 0x00020110-0x00020117
Reg Formula: 
    Where  : 
Reg Desc   : 
for HW debug only

------------------------------------------------------------------------------*/
#define cAf6Reg_stsvt_demap_debug_prbs_Base                                                         0x00020110
#define cAf6Reg_stsvt_demap_debug_prbs                                                              0x00020110
#define cAf6Reg_stsvt_demap_debug_prbs_WidthVal                                                             32
#define cAf6Reg_stsvt_demap_debug_prbs_WriteMask                                                           0x0

/*--------------------------------------
BitField Name: OutputDebugPRBS
BitField Type: RW
BitField Desc: for HW debug only
BitField Bits: [9:0]
--------------------------------------*/
#define cAf6_stsvt_demap_debug_prbs_OutputDebugPRBS_Bit_Start                                                0
#define cAf6_stsvt_demap_debug_prbs_OutputDebugPRBS_Bit_End                                                  9
#define cAf6_stsvt_demap_debug_prbs_OutputDebugPRBS_Mask                                               cBit9_0
#define cAf6_stsvt_demap_debug_prbs_OutputDebugPRBS_Shift                                                    0
#define cAf6_stsvt_demap_debug_prbs_OutputDebugPRBS_MaxVal                                               0x3ff
#define cAf6_stsvt_demap_debug_prbs_OutputDebugPRBS_MinVal                                                 0x0
#define cAf6_stsvt_demap_debug_prbs_OutputDebugPRBS_RstVal                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : STS/VT Demap Output Debug Test ID Configuration
Reg Addr   : 0x00020000-0x00020000
Reg Formula: 
    Where  : 
Reg Desc   : 
for HW debug only

------------------------------------------------------------------------------*/
#define cAf6Reg_stsvt_demap_dbg_testid_cfg_Base                                                     0x00020000
#define cAf6Reg_stsvt_demap_dbg_testid_cfg                                                          0x00020000
#define cAf6Reg_stsvt_demap_dbg_testid_cfg_WidthVal                                                         32
#define cAf6Reg_stsvt_demap_dbg_testid_cfg_WriteMask                                                       0x0

/*--------------------------------------
BitField Name: TestIDCfg
BitField Type: RW
BitField Desc: for HW debug only
BitField Bits: [9:0]
--------------------------------------*/
#define cAf6_stsvt_demap_dbg_testid_cfg_TestIDCfg_Bit_Start                                                  0
#define cAf6_stsvt_demap_dbg_testid_cfg_TestIDCfg_Bit_End                                                    9
#define cAf6_stsvt_demap_dbg_testid_cfg_TestIDCfg_Mask                                                 cBit9_0
#define cAf6_stsvt_demap_dbg_testid_cfg_TestIDCfg_Shift                                                      0
#define cAf6_stsvt_demap_dbg_testid_cfg_TestIDCfg_MaxVal                                                 0x3ff
#define cAf6_stsvt_demap_dbg_testid_cfg_TestIDCfg_MinVal                                                   0x0
#define cAf6_stsvt_demap_dbg_testid_cfg_TestIDCfg_RstVal                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : STS/VT Demap Output Debug Test Report Sticky
Reg Addr   : 0x00020001- 0x00020001
Reg Formula: 
    Where  : 
Reg Desc   : 
for HW debug only

------------------------------------------------------------------------------*/
#define cAf6Reg_stsvt_demap_dbg_testrep_sticky_Base                                                 0x00020001
#define cAf6Reg_stsvt_demap_dbg_testrep_sticky                                                      0x00020001
#define cAf6Reg_stsvt_demap_dbg_testrep_sticky_WidthVal                                                     32
#define cAf6Reg_stsvt_demap_dbg_testrep_sticky_WriteMask                                                   0x0

/*--------------------------------------
BitField Name: TestRptStk
BitField Type: RW
BitField Desc: for HW debug only
BitField Bits: [0]
--------------------------------------*/
#define cAf6_stsvt_demap_dbg_testrep_sticky_TestRptStk_Bit_Start                                             0
#define cAf6_stsvt_demap_dbg_testrep_sticky_TestRptStk_Bit_End                                               0
#define cAf6_stsvt_demap_dbg_testrep_sticky_TestRptStk_Mask                                              cBit0
#define cAf6_stsvt_demap_dbg_testrep_sticky_TestRptStk_Shift                                                 0
#define cAf6_stsvt_demap_dbg_testrep_sticky_TestRptStk_MaxVal                                              0x1
#define cAf6_stsvt_demap_dbg_testrep_sticky_TestRptStk_MinVal                                              0x0
#define cAf6_stsvt_demap_dbg_testrep_sticky_TestRptStk_RstVal                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : STS/VT Demap Output Debug Test Pattern Configuration
Reg Addr   : 0x00020002-0x00020002
Reg Formula: 
    Where  : 
Reg Desc   : 
for HW debug only

------------------------------------------------------------------------------*/
#define cAf6Reg_stsvt_demap_dbg_testpat_cfg_Base                                                    0x00020002
#define cAf6Reg_stsvt_demap_dbg_testpat_cfg                                                         0x00020002
#define cAf6Reg_stsvt_demap_dbg_testpat_cfg_WidthVal                                                        32
#define cAf6Reg_stsvt_demap_dbg_testpat_cfg_WriteMask                                                      0x0

/*--------------------------------------
BitField Name: TestPatternCfg
BitField Type: RW
BitField Desc: for HW debug only
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_stsvt_demap_dbg_testpat_cfg_TestPatternCfg_Bit_Start                                            0
#define cAf6_stsvt_demap_dbg_testpat_cfg_TestPatternCfg_Bit_End                                             31
#define cAf6_stsvt_demap_dbg_testpat_cfg_TestPatternCfg_Mask                                          cBit31_0
#define cAf6_stsvt_demap_dbg_testpat_cfg_TestPatternCfg_Shift                                                0
#define cAf6_stsvt_demap_dbg_testpat_cfg_TestPatternCfg_MaxVal                                      0xffffffff
#define cAf6_stsvt_demap_dbg_testpat_cfg_TestPatternCfg_MinVal                                             0x0
#define cAf6_stsvt_demap_dbg_testpat_cfg_TestPatternCfg_RstVal                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : STS/VT Demap Concatenation Control
Reg Addr   : 0x00025000 - 0x0002501F
Reg Formula: 0x00025000 +  stsid
    Where  : 
           + $stsid(0-23):
Reg Desc   : 
The STS/VT Demap Concatenation Control is use to configure for per STS1/VC3 Concatenation operation.

------------------------------------------------------------------------------*/
#define cAf6Reg_stsvt_demap_conc_ctrl_Base                                                          0x00025000
#define cAf6Reg_stsvt_demap_conc_ctrl(stsid)                                              (0x00025000+(stsid))
#define cAf6Reg_stsvt_demap_conc_ctrl_WidthVal                                                              32
#define cAf6Reg_stsvt_demap_conc_ctrl_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: StsVtDemapCcatEn
BitField Type: RW
BitField Desc: This bit is set to 1 to configure the STS to Concatenation mode
BitField Bits: [5]
--------------------------------------*/
#define cAf6_stsvt_demap_conc_ctrl_StsVtDemapCcatEn_Bit_Start                                                5
#define cAf6_stsvt_demap_conc_ctrl_StsVtDemapCcatEn_Bit_End                                                  5
#define cAf6_stsvt_demap_conc_ctrl_StsVtDemapCcatEn_Mask                                                 cBit5
#define cAf6_stsvt_demap_conc_ctrl_StsVtDemapCcatEn_Shift                                                    5
#define cAf6_stsvt_demap_conc_ctrl_StsVtDemapCcatEn_MaxVal                                                 0x1
#define cAf6_stsvt_demap_conc_ctrl_StsVtDemapCcatEn_MinVal                                                 0x0
#define cAf6_stsvt_demap_conc_ctrl_StsVtDemapCcatEn_RstVal                                                 0x0

/*--------------------------------------
BitField Name: StsVTDemapCcatMstID
BitField Type: RW
BitField Desc: Master ID of the concatenated STS1/VC3. Write the same STS1/VC3
ID to indicate that STS1/VC3 is the master STS1/VC3 of a concatenated group
BitField Bits: [4:0]
--------------------------------------*/
#define cAf6_stsvt_demap_conc_ctrl_StsVTDemapCcatMstID_Bit_Start                                             0
#define cAf6_stsvt_demap_conc_ctrl_StsVTDemapCcatMstID_Bit_End                                               4
#define cAf6_stsvt_demap_conc_ctrl_StsVTDemapCcatMstID_Mask                                            cBit4_0
#define cAf6_stsvt_demap_conc_ctrl_StsVTDemapCcatMstID_Shift                                                 0
#define cAf6_stsvt_demap_conc_ctrl_StsVTDemapCcatMstID_MaxVal                                             0x1f
#define cAf6_stsvt_demap_conc_ctrl_StsVTDemapCcatMstID_MinVal                                              0x0
#define cAf6_stsvt_demap_conc_ctrl_StsVTDemapCcatMstID_RstVal                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : STS/VT Demap CEP STS HW Status
Reg Addr   : 0x00025080-0x0002509F
Reg Formula: 
    Where  : 
Reg Desc   : 
for HW debug only

------------------------------------------------------------------------------*/
#define cAf6Reg_stsvt_demap_cep_sts_hw_stat_Base                                                    0x00025080
#define cAf6Reg_stsvt_demap_cep_sts_hw_stat                                                         0x00025080
#define cAf6Reg_stsvt_demap_cep_sts_hw_stat_WidthVal                                                       128
#define cAf6Reg_stsvt_demap_cep_sts_hw_stat_WriteMask                                                      0x0

/*--------------------------------------
BitField Name: EBMVtg
BitField Type: RW
BitField Desc:
BitField Bits: [118:105]
--------------------------------------*/
#define cAf6_stsvt_demap_cep_sts_hw_stat_EBMVtg_Bit_Start                                                  105
#define cAf6_stsvt_demap_cep_sts_hw_stat_EBMVtg_Bit_End                                                    118
#define cAf6_stsvt_demap_cep_sts_hw_stat_EBMVtg_Mask                                                  cBit22_9
#define cAf6_stsvt_demap_cep_sts_hw_stat_EBMVtg_Shift                                                        9
#define cAf6_stsvt_demap_cep_sts_hw_stat_EBMVtg_MaxVal                                                     0x0
#define cAf6_stsvt_demap_cep_sts_hw_stat_EBMVtg_MinVal                                                     0x0
#define cAf6_stsvt_demap_cep_sts_hw_stat_EBMVtg_RstVal                                                     0x0

/*--------------------------------------
BitField Name: EBMBit
BitField Type: RW
BitField Desc:
BitField Bits: [104:77]
--------------------------------------*/
#define cAf6_stsvt_demap_cep_sts_hw_stat_EBMBit_Bit_Start                                                   77
#define cAf6_stsvt_demap_cep_sts_hw_stat_EBMBit_Bit_End                                                    104
#define cAf6_stsvt_demap_cep_sts_hw_stat_EBMBit_Mask_01                                              cBit31_13
#define cAf6_stsvt_demap_cep_sts_hw_stat_EBMBit_Shift_01                                                    13
#define cAf6_stsvt_demap_cep_sts_hw_stat_EBMBit_Mask_02                                                cBit8_0
#define cAf6_stsvt_demap_cep_sts_hw_stat_EBMBit_Shift_02                                                     0

/*--------------------------------------
BitField Name: STSPos
BitField Type: RW
BitField Desc:
BitField Bits: [76:49]
--------------------------------------*/
#define cAf6_stsvt_demap_cep_sts_hw_stat_STSPos_Bit_Start                                                   49
#define cAf6_stsvt_demap_cep_sts_hw_stat_STSPos_Bit_End                                                     76
#define cAf6_stsvt_demap_cep_sts_hw_stat_STSPos_Mask_01                                              cBit31_17
#define cAf6_stsvt_demap_cep_sts_hw_stat_STSPos_Shift_01                                                    17
#define cAf6_stsvt_demap_cep_sts_hw_stat_STSPos_Mask_02                                               cBit12_0
#define cAf6_stsvt_demap_cep_sts_hw_stat_STSPos_Shift_02                                                     0

/*--------------------------------------
BitField Name: STSNeg
BitField Type: RW
BitField Desc:
BitField Bits: [48:21]
--------------------------------------*/
#define cAf6_stsvt_demap_cep_sts_hw_stat_STSNeg_Bit_Start                                                   21
#define cAf6_stsvt_demap_cep_sts_hw_stat_STSNeg_Bit_End                                                     48
#define cAf6_stsvt_demap_cep_sts_hw_stat_STSNeg_Mask_01                                              cBit31_21
#define cAf6_stsvt_demap_cep_sts_hw_stat_STSNeg_Shift_01                                                    21
#define cAf6_stsvt_demap_cep_sts_hw_stat_STSNeg_Mask_02                                               cBit16_0
#define cAf6_stsvt_demap_cep_sts_hw_stat_STSNeg_Shift_02                                                     0

/*--------------------------------------
BitField Name: B3Calc
BitField Type: RW
BitField Desc:
BitField Bits: [20:13]
--------------------------------------*/
#define cAf6_stsvt_demap_cep_sts_hw_stat_B3Calc_Bit_Start                                                   13
#define cAf6_stsvt_demap_cep_sts_hw_stat_B3Calc_Bit_End                                                     20
#define cAf6_stsvt_demap_cep_sts_hw_stat_B3Calc_Mask                                                 cBit20_13
#define cAf6_stsvt_demap_cep_sts_hw_stat_B3Calc_Shift                                                       13
#define cAf6_stsvt_demap_cep_sts_hw_stat_B3Calc_MaxVal                                                    0xff
#define cAf6_stsvt_demap_cep_sts_hw_stat_B3Calc_MinVal                                                     0x0
#define cAf6_stsvt_demap_cep_sts_hw_stat_B3Calc_RstVal                                                     0x0

/*--------------------------------------
BitField Name: B3Lat
BitField Type: RW
BitField Desc:
BitField Bits: [12:5]
--------------------------------------*/
#define cAf6_stsvt_demap_cep_sts_hw_stat_B3Lat_Bit_Start                                                     5
#define cAf6_stsvt_demap_cep_sts_hw_stat_B3Lat_Bit_End                                                      12
#define cAf6_stsvt_demap_cep_sts_hw_stat_B3Lat_Mask                                                   cBit12_5
#define cAf6_stsvt_demap_cep_sts_hw_stat_B3Lat_Shift                                                         5
#define cAf6_stsvt_demap_cep_sts_hw_stat_B3Lat_MaxVal                                                     0xff
#define cAf6_stsvt_demap_cep_sts_hw_stat_B3Lat_MinVal                                                      0x0
#define cAf6_stsvt_demap_cep_sts_hw_stat_B3Lat_RstVal                                                      0x0

/*--------------------------------------
BitField Name: VtgID
BitField Type: RW
BitField Desc:
BitField Bits: [4:2]
--------------------------------------*/
#define cAf6_stsvt_demap_cep_sts_hw_stat_VtgID_Bit_Start                                                     2
#define cAf6_stsvt_demap_cep_sts_hw_stat_VtgID_Bit_End                                                       4
#define cAf6_stsvt_demap_cep_sts_hw_stat_VtgID_Mask                                                    cBit4_2
#define cAf6_stsvt_demap_cep_sts_hw_stat_VtgID_Shift                                                         2
#define cAf6_stsvt_demap_cep_sts_hw_stat_VtgID_MaxVal                                                      0x7
#define cAf6_stsvt_demap_cep_sts_hw_stat_VtgID_MinVal                                                      0x0
#define cAf6_stsvt_demap_cep_sts_hw_stat_VtgID_RstVal                                                      0x0

/*--------------------------------------
BitField Name: VtID
BitField Type: RW
BitField Desc:
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_stsvt_demap_cep_sts_hw_stat_VtID_Bit_Start                                                      0
#define cAf6_stsvt_demap_cep_sts_hw_stat_VtID_Bit_End                                                        1
#define cAf6_stsvt_demap_cep_sts_hw_stat_VtID_Mask                                                     cBit1_0
#define cAf6_stsvt_demap_cep_sts_hw_stat_VtID_Shift                                                          0
#define cAf6_stsvt_demap_cep_sts_hw_stat_VtID_MaxVal                                                       0x3
#define cAf6_stsvt_demap_cep_sts_hw_stat_VtID_MinVal                                                       0x0
#define cAf6_stsvt_demap_cep_sts_hw_stat_VtID_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : STS/VT Demap CEP VC4 fractional VC3 Control
Reg Addr   : 0x000251FF
Reg Formula: 
    Where  : 
Reg Desc   : 
The STS/VT Demap CEP VC4 fractional VC3 Control is use to configure for per STS1/VC3 Concatenation operation.

------------------------------------------------------------------------------*/
#define cAf6Reg_stsvt_demap_cep_vc4_frac_vc3_ctrl_Base                                              0x000251FF
#define cAf6Reg_stsvt_demap_cep_vc4_frac_vc3_ctrl                                                   0x000251FF
#define cAf6Reg_stsvt_demap_cep_vc4_frac_vc3_ctrl_WidthVal                                                  32
#define cAf6Reg_stsvt_demap_cep_vc4_frac_vc3_ctrl_WriteMask                                                0x0

/*--------------------------------------
BitField Name: VC4FracVC3
BitField Type: RW
BitField Desc: Each bit for each STS. Set 1 is VC4 fractional VC3 mode. Set 0
for other mode
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_stsvt_demap_cep_vc4_frac_vc3_ctrl_VC4FracVC3_Bit_Start                                          0
#define cAf6_stsvt_demap_cep_vc4_frac_vc3_ctrl_VC4FracVC3_Bit_End                                           23
#define cAf6_stsvt_demap_cep_vc4_frac_vc3_ctrl_VC4FracVC3_Mask                                        cBit23_0
#define cAf6_stsvt_demap_cep_vc4_frac_vc3_ctrl_VC4FracVC3_Shift                                              0
#define cAf6_stsvt_demap_cep_vc4_frac_vc3_ctrl_VC4FracVC3_MaxVal                                      0xffffff
#define cAf6_stsvt_demap_cep_vc4_frac_vc3_ctrl_VC4FracVC3_MinVal                                           0x0
#define cAf6_stsvt_demap_cep_vc4_frac_vc3_ctrl_VC4FracVC3_RstVal                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer Mux Control
Reg Addr   : 0x00030000 - 0x0003001F
Reg Formula: 0x00030000 +  stsid
    Where  : 
           + $stsid(0-23):
Reg Desc   : 
These registers are used to configure for the source data of Ds1/E1 which may be get from Serial PDH interface or from SONET/SDH.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rxfrm_mux_ctrl_Base                                                            0x00030000
#define cAf6Reg_dej1_rxfrm_mux_ctrl(stsid)                                                (0x00030000+(stsid))
#define cAf6Reg_dej1_rxfrm_mux_ctrl_WidthVal                                                                32
#define cAf6Reg_dej1_rxfrm_mux_ctrl_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: PDHRxMuxSel
BitField Type: RW
BitField Desc: 28 selector bit for 28 DS1/E1/J1 in a STS/VC 1: source selected
from PDH Interface 0 (default): source selected from SONET/SDH Interface
BitField Bits: [27:0]
--------------------------------------*/
#define cAf6_dej1_rxfrm_mux_ctrl_PDHRxMuxSel_Bit_Start                                                       0
#define cAf6_dej1_rxfrm_mux_ctrl_PDHRxMuxSel_Bit_End                                                        27
#define cAf6_dej1_rxfrm_mux_ctrl_PDHRxMuxSel_Mask                                                     cBit27_0
#define cAf6_dej1_rxfrm_mux_ctrl_PDHRxMuxSel_Shift                                                           0
#define cAf6_dej1_rxfrm_mux_ctrl_PDHRxMuxSel_MaxVal                                                  0xfffffff
#define cAf6_dej1_rxfrm_mux_ctrl_PDHRxMuxSel_MinVal                                                        0x0
#define cAf6_dej1_rxfrm_mux_ctrl_PDHRxMuxSel_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : PDH Rx Per STS/VC Payload Control
Reg Addr   : 0x00030020 - 0x0003003F
Reg Formula: 0x00030020 +  stsid
    Where  : 
           + $stsid(0-23):
Reg Desc   : 
The STS configuration registers are used to configure kinds of STS-1s and the kinds of VT/TUs in each STS-1.

------------------------------------------------------------------------------*/
#define cAf6Reg_rx_stsvc_payload_ctrl_Base                                                          0x00030020
#define cAf6Reg_rx_stsvc_payload_ctrl(stsid)                                              (0x00030020+(stsid))
#define cAf6Reg_rx_stsvc_payload_ctrl_WidthVal                                                              32
#define cAf6Reg_rx_stsvc_payload_ctrl_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: PDHVtRxMapTug26Type
BitField Type: RW
BitField Desc: Define types of VT/TUs in TUG-2-6. 0: TU11/VT1.5 type 1: TU12/VT2
type 2: reserved 3: VC/STS/DS3/E3
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug26Type_Bit_Start                                            12
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug26Type_Bit_End                                              13
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug26Type_Mask                                          cBit13_12
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug26Type_Shift                                                12
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug26Type_MaxVal                                              0x3
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug26Type_MinVal                                              0x0
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug26Type_RstVal                                              0x0

/*--------------------------------------
BitField Name: PDHVtRxMapTug25Type
BitField Type: RW
BitField Desc: Define types of VT/TUs in TUG-2-5
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug25Type_Bit_Start                                            10
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug25Type_Bit_End                                              11
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug25Type_Mask                                          cBit11_10
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug25Type_Shift                                                10
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug25Type_MaxVal                                              0x3
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug25Type_MinVal                                              0x0
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug25Type_RstVal                                              0x0

/*--------------------------------------
BitField Name: PDHVtRxMapTug24Type
BitField Type: RW
BitField Desc: Define types of VT/TUs in TUG-2-4
BitField Bits: [9:8]
--------------------------------------*/
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug24Type_Bit_Start                                             8
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug24Type_Bit_End                                               9
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug24Type_Mask                                            cBit9_8
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug24Type_Shift                                                 8
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug24Type_MaxVal                                              0x3
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug24Type_MinVal                                              0x0
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug24Type_RstVal                                              0x0

/*--------------------------------------
BitField Name: PDHVtRxMapTug23Type
BitField Type: RW
BitField Desc: Define types of VT/TUs in TUG-2-3
BitField Bits: [7:6]
--------------------------------------*/
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug23Type_Bit_Start                                             6
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug23Type_Bit_End                                               7
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug23Type_Mask                                            cBit7_6
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug23Type_Shift                                                 6
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug23Type_MaxVal                                              0x3
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug23Type_MinVal                                              0x0
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug23Type_RstVal                                              0x0

/*--------------------------------------
BitField Name: PDHVtRxMapTug22Type
BitField Type: RW
BitField Desc: Define types of VT/TUs in TUG-2-2
BitField Bits: [5:4]
--------------------------------------*/
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug22Type_Bit_Start                                             4
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug22Type_Bit_End                                               5
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug22Type_Mask                                            cBit5_4
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug22Type_Shift                                                 4
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug22Type_MaxVal                                              0x3
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug22Type_MinVal                                              0x0
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug22Type_RstVal                                              0x0

/*--------------------------------------
BitField Name: PDHVtRxMapTug21Type
BitField Type: RW
BitField Desc: Define types of VT/TUs in TUG-2-1
BitField Bits: [3:2]
--------------------------------------*/
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug21Type_Bit_Start                                             2
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug21Type_Bit_End                                               3
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug21Type_Mask                                            cBit3_2
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug21Type_Shift                                                 2
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug21Type_MaxVal                                              0x3
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug21Type_MinVal                                              0x0
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug21Type_RstVal                                              0x0

/*--------------------------------------
BitField Name: PDHVtRxMapTug20Type
BitField Type: RW
BitField Desc: Define types of VT/TUs in TUG-2-0
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug20Type_Bit_Start                                             0
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug20Type_Bit_End                                               1
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug20Type_Mask                                            cBit1_0
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug20Type_Shift                                                 0
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug20Type_MaxVal                                              0x3
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug20Type_MinVal                                              0x0
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug20Type_RstVal                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : RxM23E23 Control
Reg Addr   : 0x00040000 - 0x0004001F
Reg Formula: 0x00040000 +  de3id
    Where  : 
           + $de3id(0-23):
Reg Desc   : 
The RxM23E23 Control is use to configure for per channel Rx DS3/E3 operation.

------------------------------------------------------------------------------*/
#define cAf6Reg_rxm23e23_ctrl_Base                                                                  0x00040000
#define cAf6Reg_rxm23e23_ctrl(de3id)                                                      (0x00040000+(de3id))
#define cAf6Reg_rxm23e23_ctrl_WidthVal                                                                      32
#define cAf6Reg_rxm23e23_ctrl_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: RxDS3E3ChEnb
BitField Type: RW
BitField Desc:
BitField Bits: [17]
--------------------------------------*/
#define cAf6_rxm23e23_ctrl_RxDS3E3ChEnb_Bit_Start                                                           17
#define cAf6_rxm23e23_ctrl_RxDS3E3ChEnb_Bit_End                                                             17
#define cAf6_rxm23e23_ctrl_RxDS3E3ChEnb_Mask                                                            cBit17
#define cAf6_rxm23e23_ctrl_RxDS3E3ChEnb_Shift                                                               17
#define cAf6_rxm23e23_ctrl_RxDS3E3ChEnb_MaxVal                                                             0x1
#define cAf6_rxm23e23_ctrl_RxDS3E3ChEnb_MinVal                                                             0x0
#define cAf6_rxm23e23_ctrl_RxDS3E3ChEnb_RstVal                                                             0x0

/*--------------------------------------
BitField Name: RxDS3E3LosThres
BitField Type: RW
BitField Desc:
BitField Bits: [16:14]
--------------------------------------*/
#define cAf6_rxm23e23_ctrl_RxDS3E3LosThres_Bit_Start                                                        14
#define cAf6_rxm23e23_ctrl_RxDS3E3LosThres_Bit_End                                                          16
#define cAf6_rxm23e23_ctrl_RxDS3E3LosThres_Mask                                                      cBit16_14
#define cAf6_rxm23e23_ctrl_RxDS3E3LosThres_Shift                                                            14
#define cAf6_rxm23e23_ctrl_RxDS3E3LosThres_MaxVal                                                          0x7
#define cAf6_rxm23e23_ctrl_RxDS3E3LosThres_MinVal                                                          0x0
#define cAf6_rxm23e23_ctrl_RxDS3E3LosThres_RstVal                                                          0x0

/*--------------------------------------
BitField Name: RxDS3E3AisThres
BitField Type: RW
BitField Desc:
BitField Bits: [13:11]
--------------------------------------*/
#define cAf6_rxm23e23_ctrl_RxDS3E3AisThres_Bit_Start                                                        11
#define cAf6_rxm23e23_ctrl_RxDS3E3AisThres_Bit_End                                                          13
#define cAf6_rxm23e23_ctrl_RxDS3E3AisThres_Mask                                                      cBit13_11
#define cAf6_rxm23e23_ctrl_RxDS3E3AisThres_Shift                                                            11
#define cAf6_rxm23e23_ctrl_RxDS3E3AisThres_MaxVal                                                          0x7
#define cAf6_rxm23e23_ctrl_RxDS3E3AisThres_MinVal                                                          0x0
#define cAf6_rxm23e23_ctrl_RxDS3E3AisThres_RstVal                                                          0x0

/*--------------------------------------
BitField Name: RxDS3E3LofThres
BitField Type: RW
BitField Desc:
BitField Bits: [10:8]
--------------------------------------*/
#define cAf6_rxm23e23_ctrl_RxDS3E3LofThres_Bit_Start                                                         8
#define cAf6_rxm23e23_ctrl_RxDS3E3LofThres_Bit_End                                                          10
#define cAf6_rxm23e23_ctrl_RxDS3E3LofThres_Mask                                                       cBit10_8
#define cAf6_rxm23e23_ctrl_RxDS3E3LofThres_Shift                                                             8
#define cAf6_rxm23e23_ctrl_RxDS3E3LofThres_MaxVal                                                          0x7
#define cAf6_rxm23e23_ctrl_RxDS3E3LofThres_MinVal                                                          0x0
#define cAf6_rxm23e23_ctrl_RxDS3E3LofThres_RstVal                                                          0x0

/*--------------------------------------
BitField Name: RxDS3E3PayAllOne
BitField Type: RW
BitField Desc: This bit is set to 1 to force the DS3/E3 framer payload AIS
downstream
BitField Bits: [7]
--------------------------------------*/
#define cAf6_rxm23e23_ctrl_RxDS3E3PayAllOne_Bit_Start                                                        7
#define cAf6_rxm23e23_ctrl_RxDS3E3PayAllOne_Bit_End                                                          7
#define cAf6_rxm23e23_ctrl_RxDS3E3PayAllOne_Mask                                                         cBit7
#define cAf6_rxm23e23_ctrl_RxDS3E3PayAllOne_Shift                                                            7
#define cAf6_rxm23e23_ctrl_RxDS3E3PayAllOne_MaxVal                                                         0x1
#define cAf6_rxm23e23_ctrl_RxDS3E3PayAllOne_MinVal                                                         0x0
#define cAf6_rxm23e23_ctrl_RxDS3E3PayAllOne_RstVal                                                         0x0

/*--------------------------------------
BitField Name: RxDS3E3LineAllOne
BitField Type: RW
BitField Desc: This bit is set to 1 to force the DS3/E3 framer line AIS
downstream
BitField Bits: [6]
--------------------------------------*/
#define cAf6_rxm23e23_ctrl_RxDS3E3LineAllOne_Bit_Start                                                       6
#define cAf6_rxm23e23_ctrl_RxDS3E3LineAllOne_Bit_End                                                         6
#define cAf6_rxm23e23_ctrl_RxDS3E3LineAllOne_Mask                                                        cBit6
#define cAf6_rxm23e23_ctrl_RxDS3E3LineAllOne_Shift                                                           6
#define cAf6_rxm23e23_ctrl_RxDS3E3LineAllOne_MaxVal                                                        0x1
#define cAf6_rxm23e23_ctrl_RxDS3E3LineAllOne_MinVal                                                        0x0
#define cAf6_rxm23e23_ctrl_RxDS3E3LineAllOne_RstVal                                                        0x0

/*--------------------------------------
BitField Name: RxDS3E3LineLoop
BitField Type: RW
BitField Desc:
BitField Bits: [5]
--------------------------------------*/
#define cAf6_rxm23e23_ctrl_RxDS3E3LineLoop_Bit_Start                                                         5
#define cAf6_rxm23e23_ctrl_RxDS3E3LineLoop_Bit_End                                                           5
#define cAf6_rxm23e23_ctrl_RxDS3E3LineLoop_Mask                                                          cBit5
#define cAf6_rxm23e23_ctrl_RxDS3E3LineLoop_Shift                                                             5
#define cAf6_rxm23e23_ctrl_RxDS3E3LineLoop_MaxVal                                                          0x1
#define cAf6_rxm23e23_ctrl_RxDS3E3LineLoop_MinVal                                                          0x0
#define cAf6_rxm23e23_ctrl_RxDS3E3LineLoop_RstVal                                                          0x0

/*--------------------------------------
BitField Name: RxDS3E3FrcLof
BitField Type: RW
BitField Desc: This bit is set to 1 to force the Rx DS3/E3 framer into LOF
status. 1: force LOF. 0: not force LOF.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_rxm23e23_ctrl_RxDS3E3FrcLof_Bit_Start                                                           4
#define cAf6_rxm23e23_ctrl_RxDS3E3FrcLof_Bit_End                                                             4
#define cAf6_rxm23e23_ctrl_RxDS3E3FrcLof_Mask                                                            cBit4
#define cAf6_rxm23e23_ctrl_RxDS3E3FrcLof_Shift                                                               4
#define cAf6_rxm23e23_ctrl_RxDS3E3FrcLof_MaxVal                                                            0x1
#define cAf6_rxm23e23_ctrl_RxDS3E3FrcLof_MinVal                                                            0x0
#define cAf6_rxm23e23_ctrl_RxDS3E3FrcLof_RstVal                                                            0x0

/*--------------------------------------
BitField Name: RxDS3E3Md
BitField Type: RW
BitField Desc: Rx DS3/E3 framing mode 0000: DS3 Unframed 0001: DS3 C-bit parity
carrying 28 DS1s or 21 E1s 0010: DS3 M23 carrying 28 DS1s or 21 E1s 0011: DS3
C-bit map 0100: E3 Unframed 0101: E3 G832 0110: E3 G.751 carrying 16 E1s 0111:
E3 G.751 Map 1000: Bypass RxM13E13 (DS1/E1 mode)
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_rxm23e23_ctrl_RxDS3E3Md_Bit_Start                                                               0
#define cAf6_rxm23e23_ctrl_RxDS3E3Md_Bit_End                                                                 3
#define cAf6_rxm23e23_ctrl_RxDS3E3Md_Mask                                                              cBit3_0
#define cAf6_rxm23e23_ctrl_RxDS3E3Md_Shift                                                                   0
#define cAf6_rxm23e23_ctrl_RxDS3E3Md_MaxVal                                                                0xf
#define cAf6_rxm23e23_ctrl_RxDS3E3Md_MinVal                                                                0x0
#define cAf6_rxm23e23_ctrl_RxDS3E3Md_RstVal                                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : RxM23E23 HW Status
Reg Addr   : 0x00040080-0x0004009F
Reg Formula: 0x00040080 +  de3id
    Where  : 
           + $de3id(0-23):
Reg Desc   : 
for HW debug only

------------------------------------------------------------------------------*/
#define cAf6Reg_rxm23e23_hw_stat_Base                                                               0x00040080
#define cAf6Reg_rxm23e23_hw_stat(de3id)                                                   (0x00040080+(de3id))
#define cAf6Reg_rxm23e23_hw_stat_WidthVal                                                                   32
#define cAf6Reg_rxm23e23_hw_stat_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: RxM23E23Status
BitField Type: RW
BitField Desc: for HW debug only
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_rxm23e23_hw_stat_RxM23E23Status_Bit_Start                                                       0
#define cAf6_rxm23e23_hw_stat_RxM23E23Status_Bit_End                                                         1
#define cAf6_rxm23e23_hw_stat_RxM23E23Status_Mask                                                      cBit1_0
#define cAf6_rxm23e23_hw_stat_RxM23E23Status_Shift                                                           0
#define cAf6_rxm23e23_hw_stat_RxM23E23Status_MaxVal                                                        0x3
#define cAf6_rxm23e23_hw_stat_RxM23E23Status_MinVal                                                        0x0
#define cAf6_rxm23e23_hw_stat_RxM23E23Status_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : RxDS3E3 OH Pro Control
Reg Addr   : 0x040420-0x04043F
Reg Formula: 0x040420 +  de3id
    Where  : 
           + $de3id(0-23):
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_rx_de3_oh_ctrl_Base                                                                   0x040420
#define cAf6Reg_rx_de3_oh_ctrl(de3id)                                                       (0x040420+(de3id))
#define cAf6Reg_rx_de3_oh_ctrl_WidthVal                                                                     32
#define cAf6Reg_rx_de3_oh_ctrl_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: DLEn
BitField Type: RW
BitField Desc: DLEn 1: Data Link send to HDLC 0: Data Link not send to HDLC
BitField Bits: [5]
--------------------------------------*/
#define cAf6_rx_de3_oh_ctrl_DLEn_Bit_Start                                                                   5
#define cAf6_rx_de3_oh_ctrl_DLEn_Bit_End                                                                     5
#define cAf6_rx_de3_oh_ctrl_DLEn_Mask                                                                    cBit5
#define cAf6_rx_de3_oh_ctrl_DLEn_Shift                                                                       5
#define cAf6_rx_de3_oh_ctrl_DLEn_MaxVal                                                                    0x1
#define cAf6_rx_de3_oh_ctrl_DLEn_MinVal                                                                    0x0
#define cAf6_rx_de3_oh_ctrl_DLEn_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: Ds3E3DLSL
BitField Type: RW
BitField Desc: 00: DL 01: UDL 10: FEAC 11: NA
BitField Bits: [4:3]
--------------------------------------*/
#define cAf6_rx_de3_oh_ctrl_Ds3E3DLSL_Bit_Start                                                              3
#define cAf6_rx_de3_oh_ctrl_Ds3E3DLSL_Bit_End                                                                4
#define cAf6_rx_de3_oh_ctrl_Ds3E3DLSL_Mask                                                             cBit4_3
#define cAf6_rx_de3_oh_ctrl_Ds3E3DLSL_Shift                                                                  3
#define cAf6_rx_de3_oh_ctrl_Ds3E3DLSL_MaxVal                                                               0x3
#define cAf6_rx_de3_oh_ctrl_Ds3E3DLSL_MinVal                                                               0x0
#define cAf6_rx_de3_oh_ctrl_Ds3E3DLSL_RstVal                                                               0x0

/*--------------------------------------
BitField Name: Ds3FEACEn
BitField Type: RW
BitField Desc: Enable detection FEAC state change
BitField Bits: [2]
--------------------------------------*/
#define cAf6_rx_de3_oh_ctrl_Ds3FEACEn_Bit_Start                                                              2
#define cAf6_rx_de3_oh_ctrl_Ds3FEACEn_Bit_End                                                                2
#define cAf6_rx_de3_oh_ctrl_Ds3FEACEn_Mask                                                               cBit2
#define cAf6_rx_de3_oh_ctrl_Ds3FEACEn_Shift                                                                  2
#define cAf6_rx_de3_oh_ctrl_Ds3FEACEn_MaxVal                                                               0x1
#define cAf6_rx_de3_oh_ctrl_Ds3FEACEn_MinVal                                                               0x0
#define cAf6_rx_de3_oh_ctrl_Ds3FEACEn_RstVal                                                               0x0

/*--------------------------------------
BitField Name: FBEFbitCntEn
BitField Type: RW
BitField Desc: Enable FBE counts F-Bit
BitField Bits: [1]
--------------------------------------*/
#define cAf6_rx_de3_oh_ctrl_FBEFbitCntEn_Bit_Start                                                           1
#define cAf6_rx_de3_oh_ctrl_FBEFbitCntEn_Bit_End                                                             1
#define cAf6_rx_de3_oh_ctrl_FBEFbitCntEn_Mask                                                            cBit1
#define cAf6_rx_de3_oh_ctrl_FBEFbitCntEn_Shift                                                               1
#define cAf6_rx_de3_oh_ctrl_FBEFbitCntEn_MaxVal                                                            0x1
#define cAf6_rx_de3_oh_ctrl_FBEFbitCntEn_MinVal                                                            0x0
#define cAf6_rx_de3_oh_ctrl_FBEFbitCntEn_RstVal                                                            0x0

/*--------------------------------------
BitField Name: FBEMbitCntEn
BitField Type: RW
BitField Desc: Enable FBE counts M-Bit
BitField Bits: [0]
--------------------------------------*/
#define cAf6_rx_de3_oh_ctrl_FBEMbitCntEn_Bit_Start                                                           0
#define cAf6_rx_de3_oh_ctrl_FBEMbitCntEn_Bit_End                                                             0
#define cAf6_rx_de3_oh_ctrl_FBEMbitCntEn_Mask                                                            cBit0
#define cAf6_rx_de3_oh_ctrl_FBEMbitCntEn_Shift                                                               0
#define cAf6_rx_de3_oh_ctrl_FBEMbitCntEn_MaxVal                                                            0x1
#define cAf6_rx_de3_oh_ctrl_FBEMbitCntEn_MinVal                                                            0x0
#define cAf6_rx_de3_oh_ctrl_FBEMbitCntEn_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : RxM23E23 Framer REI Threshold Control
Reg Addr   : 0x00040403
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the REI threshold configuration register for the PDH DS3/E3 Rx framer

------------------------------------------------------------------------------*/
#define cAf6Reg_rxm23e23_rei_thrsh_cfg_Base                                                         0x00040403
#define cAf6Reg_rxm23e23_rei_thrsh_cfg                                                              0x00040403
#define cAf6Reg_rxm23e23_rei_thrsh_cfg_WidthVal                                                             32
#define cAf6Reg_rxm23e23_rei_thrsh_cfg_WriteMask                                                           0x0

/*--------------------------------------
BitField Name: RxM23E23ReiThres
BitField Type: RW
BitField Desc: Threshold to generate interrupt if the REI counter exceed this
threshold
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_rxm23e23_rei_thrsh_cfg_RxM23E23ReiThres_Bit_Start                                               0
#define cAf6_rxm23e23_rei_thrsh_cfg_RxM23E23ReiThres_Bit_End                                                15
#define cAf6_rxm23e23_rei_thrsh_cfg_RxM23E23ReiThres_Mask                                             cBit15_0
#define cAf6_rxm23e23_rei_thrsh_cfg_RxM23E23ReiThres_Shift                                                   0
#define cAf6_rxm23e23_rei_thrsh_cfg_RxM23E23ReiThres_MaxVal                                             0xffff
#define cAf6_rxm23e23_rei_thrsh_cfg_RxM23E23ReiThres_MinVal                                                0x0
#define cAf6_rxm23e23_rei_thrsh_cfg_RxM23E23ReiThres_RstVal                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : RxM23E23 Framer FBE Threshold Control
Reg Addr   : 0x00040404
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the REI threshold configuration register for the PDH DS3/E3 Rx framer

------------------------------------------------------------------------------*/
#define cAf6Reg_rxm23e23_fbe_thrsh_cfg_Base                                                         0x00040404
#define cAf6Reg_rxm23e23_fbe_thrsh_cfg                                                              0x00040404
#define cAf6Reg_rxm23e23_fbe_thrsh_cfg_WidthVal                                                             32
#define cAf6Reg_rxm23e23_fbe_thrsh_cfg_WriteMask                                                           0x0

/*--------------------------------------
BitField Name: RxM23E23FbeThres
BitField Type: RW
BitField Desc: Threshold to generate interrupt if the FBE counter exceed this
threshold
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_rxm23e23_fbe_thrsh_cfg_RxM23E23FbeThres_Bit_Start                                               0
#define cAf6_rxm23e23_fbe_thrsh_cfg_RxM23E23FbeThres_Bit_End                                                15
#define cAf6_rxm23e23_fbe_thrsh_cfg_RxM23E23FbeThres_Mask                                             cBit15_0
#define cAf6_rxm23e23_fbe_thrsh_cfg_RxM23E23FbeThres_Shift                                                   0
#define cAf6_rxm23e23_fbe_thrsh_cfg_RxM23E23FbeThres_MaxVal                                             0xffff
#define cAf6_rxm23e23_fbe_thrsh_cfg_RxM23E23FbeThres_MinVal                                                0x0
#define cAf6_rxm23e23_fbe_thrsh_cfg_RxM23E23FbeThres_RstVal                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : RxM23E23 Framer Parity Threshold Control
Reg Addr   : 0x00040405
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the REI threshold configuration register for the PDH DS3/E3 Rx framer

------------------------------------------------------------------------------*/
#define cAf6Reg_rxm23e23_parity_thrsh_cfg_Base                                                      0x00040405
#define cAf6Reg_rxm23e23_parity_thrsh_cfg                                                           0x00040405
#define cAf6Reg_rxm23e23_parity_thrsh_cfg_WidthVal                                                          32
#define cAf6Reg_rxm23e23_parity_thrsh_cfg_WriteMask                                                        0x0

/*--------------------------------------
BitField Name: RxM23E23ParThres
BitField Type: RW
BitField Desc: Threshold to generate interrupt if the PAR counter exceed this
threshold
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_rxm23e23_parity_thrsh_cfg_RxM23E23ParThres_Bit_Start                                            0
#define cAf6_rxm23e23_parity_thrsh_cfg_RxM23E23ParThres_Bit_End                                             15
#define cAf6_rxm23e23_parity_thrsh_cfg_RxM23E23ParThres_Mask                                          cBit15_0
#define cAf6_rxm23e23_parity_thrsh_cfg_RxM23E23ParThres_Shift                                                0
#define cAf6_rxm23e23_parity_thrsh_cfg_RxM23E23ParThres_MaxVal                                          0xffff
#define cAf6_rxm23e23_parity_thrsh_cfg_RxM23E23ParThres_MinVal                                             0x0
#define cAf6_rxm23e23_parity_thrsh_cfg_RxM23E23ParThres_RstVal                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : RxM23E23 Framer Cbit Parity Threshold Control
Reg Addr   : 0x00040406
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the REI threshold configuration register for the PDH DS3/E3 Rx framer

------------------------------------------------------------------------------*/
#define cAf6Reg_rxm23e23_cbit_parity_thrsh_cfg_Base                                                 0x00040406
#define cAf6Reg_rxm23e23_cbit_parity_thrsh_cfg                                                      0x00040406
#define cAf6Reg_rxm23e23_cbit_parity_thrsh_cfg_WidthVal                                                     32
#define cAf6Reg_rxm23e23_cbit_parity_thrsh_cfg_WriteMask                                                   0x0

/*--------------------------------------
BitField Name: RxM23E23CParThres
BitField Type: RW
BitField Desc: Threshold to generate interrupt if the Cbit Parity counter exceed
this threshold
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_rxm23e23_cbit_parity_thrsh_cfg_RxM23E23CParThres_Bit_Start                                       0
#define cAf6_rxm23e23_cbit_parity_thrsh_cfg_RxM23E23CParThres_Bit_End                                       15
#define cAf6_rxm23e23_cbit_parity_thrsh_cfg_RxM23E23CParThres_Mask                                    cBit15_0
#define cAf6_rxm23e23_cbit_parity_thrsh_cfg_RxM23E23CParThres_Shift                                          0
#define cAf6_rxm23e23_cbit_parity_thrsh_cfg_RxM23E23CParThres_MaxVal                                    0xffff
#define cAf6_rxm23e23_cbit_parity_thrsh_cfg_RxM23E23CParThres_MinVal                                       0x0
#define cAf6_rxm23e23_cbit_parity_thrsh_cfg_RxM23E23CParThres_RstVal                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : RxM23E23 Framer REI Counter
Reg Addr   : 0x00040440 - 0x0004047F
Reg Formula: 0x00040440 +  32*Rd2Clr + de3id
    Where  : 
           + $Rd2Clr(0-1): Rd2Clr = 0: Read to clear. Otherwise, Read Only
           + $de3id(0-23):)
Reg Desc   : 
This is the per channel REI counter for DS3/E3 receive framer

------------------------------------------------------------------------------*/
#define cAf6Reg_rxm23e23_rei_cnt_Base                                                               0x00040440
#define cAf6Reg_rxm23e23_rei_cnt(Rd2Clr, de3id)                               (0x00040440+32*(Rd2Clr)+(de3id))
#define cAf6Reg_rxm23e23_rei_cnt_WidthVal                                                                   32
#define cAf6Reg_rxm23e23_rei_cnt_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: RxM23E23ReiCnt
BitField Type: RW
BitField Desc: DS3/E3 REI error accumulator
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_rxm23e23_rei_cnt_RxM23E23ReiCnt_Bit_Start                                                       0
#define cAf6_rxm23e23_rei_cnt_RxM23E23ReiCnt_Bit_End                                                        15
#define cAf6_rxm23e23_rei_cnt_RxM23E23ReiCnt_Mask                                                     cBit15_0
#define cAf6_rxm23e23_rei_cnt_RxM23E23ReiCnt_Shift                                                           0
#define cAf6_rxm23e23_rei_cnt_RxM23E23ReiCnt_MaxVal                                                     0xffff
#define cAf6_rxm23e23_rei_cnt_RxM23E23ReiCnt_MinVal                                                        0x0
#define cAf6_rxm23e23_rei_cnt_RxM23E23ReiCnt_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : RxM23E23 Framer FBE Counter
Reg Addr   : 0x00040480 - 0x000404BF
Reg Formula: 0x00040480 +  32*Rd2Clr + de3id
    Where  : 
           + $Rd2Clr(0-1): Rd2Clr = 0: Read to clear. Otherwise, Read Only.
           + $de3id(0-23):
Reg Desc   : 
This is the per channel FBE counter for DS3/E3 receive framer

------------------------------------------------------------------------------*/
#define cAf6Reg_rxm23e23_fbe_cnt_Base                                                               0x00040480
#define cAf6Reg_rxm23e23_fbe_cnt(Rd2Clr, de3id)                               (0x00040480+32*(Rd2Clr)+(de3id))
#define cAf6Reg_rxm23e23_fbe_cnt_WidthVal                                                                   32
#define cAf6Reg_rxm23e23_fbe_cnt_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: count
BitField Type: RW
BitField Desc: frame FBE counter
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_rxm23e23_fbe_cnt_count_Bit_Start                                                                0
#define cAf6_rxm23e23_fbe_cnt_count_Bit_End                                                                 15
#define cAf6_rxm23e23_fbe_cnt_count_Mask                                                              cBit15_0
#define cAf6_rxm23e23_fbe_cnt_count_Shift                                                                    0
#define cAf6_rxm23e23_fbe_cnt_count_MaxVal                                                              0xffff
#define cAf6_rxm23e23_fbe_cnt_count_MinVal                                                                 0x0
#define cAf6_rxm23e23_fbe_cnt_count_RstVal                                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : RxM23E23 Framer Parity Counter
Reg Addr   : 0x000404C0 - 0x000404FF
Reg Formula: 0x000404C0 +  32*Rd2Clr + de3id
    Where  : 
           + $Rd2Clr(0-1): Rd2Clr = 0: Read to clear. Otherwise, Read Only.
           + $de3id(0-23):
Reg Desc   : 
This is the per channel Parity counter for DS3/E3 receive framer

------------------------------------------------------------------------------*/
#define cAf6Reg_rxm23e23_parity_cnt_Base                                                            0x000404C0
#define cAf6Reg_rxm23e23_parity_cnt(Rd2Clr, de3id)                            (0x000404C0+32*(Rd2Clr)+(de3id))
#define cAf6Reg_rxm23e23_parity_cnt_WidthVal                                                                32
#define cAf6Reg_rxm23e23_parity_cnt_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: RxM23E23ParCnt
BitField Type: RW
BitField Desc: DS3/E3 Parity error accumulator
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_rxm23e23_parity_cnt_RxM23E23ParCnt_Bit_Start                                                    0
#define cAf6_rxm23e23_parity_cnt_RxM23E23ParCnt_Bit_End                                                     15
#define cAf6_rxm23e23_parity_cnt_RxM23E23ParCnt_Mask                                                  cBit15_0
#define cAf6_rxm23e23_parity_cnt_RxM23E23ParCnt_Shift                                                        0
#define cAf6_rxm23e23_parity_cnt_RxM23E23ParCnt_MaxVal                                                  0xffff
#define cAf6_rxm23e23_parity_cnt_RxM23E23ParCnt_MinVal                                                     0x0
#define cAf6_rxm23e23_parity_cnt_RxM23E23ParCnt_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : RxM23E23 Framer Cbit Parity Counter
Reg Addr   : 0x00040500 - 0x0004053F
Reg Formula: 0x00040500 +  32*Rd2Clr + de3id
    Where  : 
           + $Rd2Clr(0-1): Rd2Clr = 0: Read to clear. Otherwise, Read Only.
           + $de3id(0-23):
Reg Desc   : 
This is the per channel Cbit Parity counter for DS3/E3 receive framer

------------------------------------------------------------------------------*/
#define cAf6Reg_rxm23e23_cbit_parity_cnt_Base                                                       0x00040500
#define cAf6Reg_rxm23e23_cbit_parity_cnt(Rd2Clr, de3id)                       (0x00040500+32*(Rd2Clr)+(de3id))
#define cAf6Reg_rxm23e23_cbit_parity_cnt_WidthVal                                                           32
#define cAf6Reg_rxm23e23_cbit_parity_cnt_WriteMask                                                         0x0

/*--------------------------------------
BitField Name: RxM23E23CParCnt
BitField Type: RW
BitField Desc: DS3/E3 Cbit Parity error accumulator
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_rxm23e23_cbit_parity_cnt_RxM23E23CParCnt_Bit_Start                                              0
#define cAf6_rxm23e23_cbit_parity_cnt_RxM23E23CParCnt_Bit_End                                               15
#define cAf6_rxm23e23_cbit_parity_cnt_RxM23E23CParCnt_Mask                                            cBit15_0
#define cAf6_rxm23e23_cbit_parity_cnt_RxM23E23CParCnt_Shift                                                  0
#define cAf6_rxm23e23_cbit_parity_cnt_RxM23E23CParCnt_MaxVal                                            0xffff
#define cAf6_rxm23e23_cbit_parity_cnt_RxM23E23CParCnt_MinVal                                               0x0
#define cAf6_rxm23e23_cbit_parity_cnt_RxM23E23CParCnt_RstVal                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : RxM23E23 Framer Line Code Violation Counter
Reg Addr   : 0x00040600 - 0x0004063F
Reg Formula: 0x00040600 +  32*Rd2Clr + de3id
    Where  : 
           + $Rd2Clr(0-1): Rd2Clr = 0: Read to clear. Otherwise, Read Only.
           + $de3id(0-23):
Reg Desc   : 
This is the per channel Cbit Parity counter for DS3/E3 receive framer

------------------------------------------------------------------------------*/
#define cAf6Reg_rxm23e23_lcv_cnt_Base                                                               0x00040600
#define cAf6Reg_rxm23e23_lcv_cnt(Rd2Clr, de3id)                               (0x00040600+32*(Rd2Clr)+(de3id))
#define cAf6Reg_rxm23e23_lcv_cnt_WidthVal                                                                   32
#define cAf6Reg_rxm23e23_lcv_cnt_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: RxM23E23LCVCnt
BitField Type: RW
BitField Desc: DS3/E3 Line code violation accumulator
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_rxm23e23_lcv_cnt_RxM23E23LCVCnt_Bit_Start                                                       0
#define cAf6_rxm23e23_lcv_cnt_RxM23E23LCVCnt_Bit_End                                                        15
#define cAf6_rxm23e23_lcv_cnt_RxM23E23LCVCnt_Mask                                                     cBit15_0
#define cAf6_rxm23e23_lcv_cnt_RxM23E23LCVCnt_Shift                                                           0
#define cAf6_rxm23e23_lcv_cnt_RxM23E23LCVCnt_MaxVal                                                     0xffff
#define cAf6_rxm23e23_lcv_cnt_RxM23E23LCVCnt_MinVal                                                        0x0
#define cAf6_rxm23e23_lcv_cnt_RxM23E23LCVCnt_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : RxDS3E3 FEAC Buffer
Reg Addr   : 0x040560-0x04057F
Reg Formula: 0x040560 +  de3id
    Where  : 
           + $de3id(0-23):
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_rx_de3_feac_buffer_Base                                                               0x040560
#define cAf6Reg_rx_de3_feac_buffer(de3id)                                                   (0x040560+(de3id))
#define cAf6Reg_rx_de3_feac_buffer_WidthVal                                                                 32
#define cAf6Reg_rx_de3_feac_buffer_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: FEAC State
BitField Type: RW
BitField Desc: 00: idle codeword 01: active codeword 10: de-active codeword 11:
alarm/status codeword
BitField Bits: [7:6]
--------------------------------------*/
#define cAf6_rx_de3_feac_buffer_FEAC_State_Bit_Start                                                         6
#define cAf6_rx_de3_feac_buffer_FEAC_State_Bit_End                                                           7
#define cAf6_rx_de3_feac_buffer_FEAC_State_Mask                                                        cBit7_6
#define cAf6_rx_de3_feac_buffer_FEAC_State_Shift                                                             6
#define cAf6_rx_de3_feac_buffer_FEAC_State_MaxVal                                                          0x3
#define cAf6_rx_de3_feac_buffer_FEAC_State_MinVal                                                          0x0
#define cAf6_rx_de3_feac_buffer_FEAC_State_RstVal                                                          0x0

/*--------------------------------------
BitField Name: FEAC Codeword
BitField Type: RW
BitField Desc: 6 x-bits cw in format 111111110xxxxxx0
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_rx_de3_feac_buffer_FEAC_Codeword_Bit_Start                                                      0
#define cAf6_rx_de3_feac_buffer_FEAC_Codeword_Bit_End                                                        5
#define cAf6_rx_de3_feac_buffer_FEAC_Codeword_Mask                                                     cBit5_0
#define cAf6_rx_de3_feac_buffer_FEAC_Codeword_Shift                                                          0
#define cAf6_rx_de3_feac_buffer_FEAC_Codeword_MaxVal                                                      0x3f
#define cAf6_rx_de3_feac_buffer_FEAC_Codeword_MinVal                                                       0x0
#define cAf6_rx_de3_feac_buffer_FEAC_Codeword_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : RxM23E23 Framer Channel Interrupt Enable Control
Reg Addr   : 0x000405FE
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the Channel interrupt enable of DS3/E3 Rx Framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_rxm23e23_chn_intr_cfg_Base                                                          0x000405FE
#define cAf6Reg_rxm23e23_chn_intr_cfg                                                               0x000405FE
#define cAf6Reg_rxm23e23_chn_intr_cfg_WidthVal                                                              32
#define cAf6Reg_rxm23e23_chn_intr_cfg_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: RxM23E23IntrEn
BitField Type: RW
BitField Desc: Each bit correspond with each channel DS3/E3 0: Disable 1: Enable
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_rxm23e23_chn_intr_cfg_RxM23E23IntrEn_Bit_Start                                                  0
#define cAf6_rxm23e23_chn_intr_cfg_RxM23E23IntrEn_Bit_End                                                   23
#define cAf6_rxm23e23_chn_intr_cfg_RxM23E23IntrEn_Mask                                                cBit23_0
#define cAf6_rxm23e23_chn_intr_cfg_RxM23E23IntrEn_Shift                                                      0
#define cAf6_rxm23e23_chn_intr_cfg_RxM23E23IntrEn_MaxVal                                              0xffffff
#define cAf6_rxm23e23_chn_intr_cfg_RxM23E23IntrEn_MinVal                                                   0x0
#define cAf6_rxm23e23_chn_intr_cfg_RxM23E23IntrEn_RstVal                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : RxM23E23 Framer Channel Interrupt Status
Reg Addr   : 0x000405FF
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the Channel interrupt tus of DS3/E3 Rx Framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_rxm23e23_chn_intr_stat_Base                                                         0x000405FF
#define cAf6Reg_rxm23e23_chn_intr_stat                                                              0x000405FF
#define cAf6Reg_rxm23e23_chn_intr_stat_WidthVal                                                             32
#define cAf6Reg_rxm23e23_chn_intr_stat_WriteMask                                                           0x0

/*--------------------------------------
BitField Name: RxM23E23IntrSta
BitField Type: RW
BitField Desc: Each bit correspond with each channel DS3/E3 0: No Interrupt 1:
Have Interrupt
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_rxm23e23_chn_intr_stat_RxM23E23IntrSta_Bit_Start                                                0
#define cAf6_rxm23e23_chn_intr_stat_RxM23E23IntrSta_Bit_End                                                 23
#define cAf6_rxm23e23_chn_intr_stat_RxM23E23IntrSta_Mask                                              cBit23_0
#define cAf6_rxm23e23_chn_intr_stat_RxM23E23IntrSta_Shift                                                    0
#define cAf6_rxm23e23_chn_intr_stat_RxM23E23IntrSta_MaxVal                                            0xffffff
#define cAf6_rxm23e23_chn_intr_stat_RxM23E23IntrSta_MinVal                                                 0x0
#define cAf6_rxm23e23_chn_intr_stat_RxM23E23IntrSta_RstVal                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : RxM23E23 Framer per Channel Interrupt Enable Control
Reg Addr   : 0x00040580-0x0004059F
Reg Formula: 0x00040580 +  de3id
    Where  : 
           + $de3id(0-23):
Reg Desc   : 
This is the per Channel interrupt enable of DS1/E1/J1 Rx Framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_rxm23e23_per_chn_intr_cfg_Base                                                      0x00040580
#define cAf6Reg_rxm23e23_per_chn_intr_cfg(de3id)                                          (0x00040580+(de3id))
#define cAf6Reg_rxm23e23_per_chn_intr_cfg_WidthVal                                                          32
#define cAf6Reg_rxm23e23_per_chn_intr_cfg_WriteMask                                                        0x0

#define cAf6_rxm23e23_per_chn_intr_cfg_DE3SSMChg_En_Mask                                                cBit16
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3BerTca_En_Mask                                                cBit15
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3BerSd_En_Mask                                                 cBit14
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3BerSf_En_Mask                                                 cBit13

/*--------------------------------------
BitField Name: DS3FEAC_E3SSM_En
BitField Type: RW
BitField Desc: Set 1 to enable change DS3 FEAC or E3G832 SSM event to generate
an interrupt.
BitField Bits: [7]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_cfg_DS3FEAC_E3SSM_En_Bit_Start                                            7
#define cAf6_rxm23e23_per_chn_intr_cfg_DS3FEAC_E3SSM_En_Bit_End                                              7
#define cAf6_rxm23e23_per_chn_intr_cfg_DS3FEAC_E3SSM_En_Mask                                             cBit7
#define cAf6_rxm23e23_per_chn_intr_cfg_DS3FEAC_E3SSM_En_Shift                                                7
#define cAf6_rxm23e23_per_chn_intr_cfg_DS3FEAC_E3SSM_En_MaxVal                                             0x1
#define cAf6_rxm23e23_per_chn_intr_cfg_DS3FEAC_E3SSM_En_MinVal                                             0x0
#define cAf6_rxm23e23_per_chn_intr_cfg_DS3FEAC_E3SSM_En_RstVal                                             0x0

/*--------------------------------------
BitField Name: DS3AIC_E3TM_En
BitField Type: RW
BitField Desc: Set 1 to enable change DS3 AIC or E3G832 TM event to generate an
interrupt.
BitField Bits: [6]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_cfg_DS3AIC_E3TM_En_Bit_Start                                              6
#define cAf6_rxm23e23_per_chn_intr_cfg_DS3AIC_E3TM_En_Bit_End                                                6
#define cAf6_rxm23e23_per_chn_intr_cfg_DS3AIC_E3TM_En_Mask                                               cBit6
#define cAf6_rxm23e23_per_chn_intr_cfg_DS3AIC_E3TM_En_Shift                                                  6
#define cAf6_rxm23e23_per_chn_intr_cfg_DS3AIC_E3TM_En_MaxVal                                               0x1
#define cAf6_rxm23e23_per_chn_intr_cfg_DS3AIC_E3TM_En_MinVal                                               0x0
#define cAf6_rxm23e23_per_chn_intr_cfg_DS3AIC_E3TM_En_RstVal                                               0x0

/*--------------------------------------
BitField Name: DS3IDLESig_E3PT_En
BitField Type: RW
BitField Desc: Set 1 to enable change DS3 IDLE signal or E3G832 PayloadType
event to generate an interrupt.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_cfg_DS3IDLESig_E3PT_En_Bit_Start                                          5
#define cAf6_rxm23e23_per_chn_intr_cfg_DS3IDLESig_E3PT_En_Bit_End                                            5
#define cAf6_rxm23e23_per_chn_intr_cfg_DS3IDLESig_E3PT_En_Mask                                           cBit5
#define cAf6_rxm23e23_per_chn_intr_cfg_DS3IDLESig_E3PT_En_Shift                                              5
#define cAf6_rxm23e23_per_chn_intr_cfg_DS3IDLESig_E3PT_En_MaxVal                                           0x1
#define cAf6_rxm23e23_per_chn_intr_cfg_DS3IDLESig_E3PT_En_MinVal                                           0x0
#define cAf6_rxm23e23_per_chn_intr_cfg_DS3IDLESig_E3PT_En_RstVal                                           0x0

/*--------------------------------------
BitField Name: DE3AisAllOneIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change AIS All One event to generate an
interrupt.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3AisAllOneIntrEn_Bit_Start                                          4
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3AisAllOneIntrEn_Bit_End                                            4
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3AisAllOneIntrEn_Mask                                           cBit4
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3AisAllOneIntrEn_Shift                                              4
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3AisAllOneIntrEn_MaxVal                                           0x1
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3AisAllOneIntrEn_MinVal                                           0x0
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3AisAllOneIntrEn_RstVal                                           0x0

/*--------------------------------------
BitField Name: DE3LosAllZeroIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change LOS All Zero event to generate an
interrupt.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3LosAllZeroIntrEn_Bit_Start                                         3
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3LosAllZeroIntrEn_Bit_End                                           3
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3LosAllZeroIntrEn_Mask                                          cBit3
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3LosAllZeroIntrEn_Shift                                             3
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3LosAllZeroIntrEn_MaxVal                                          0x1
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3LosAllZeroIntrEn_MinVal                                          0x0
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3LosAllZeroIntrEn_RstVal                                          0x0

/*--------------------------------------
BitField Name: DS3AISSigIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change AIS Signal event to generate an interrupt.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_cfg_DS3AISSigIntrEn_Bit_Start                                             2
#define cAf6_rxm23e23_per_chn_intr_cfg_DS3AISSigIntrEn_Bit_End                                               2
#define cAf6_rxm23e23_per_chn_intr_cfg_DS3AISSigIntrEn_Mask                                              cBit2
#define cAf6_rxm23e23_per_chn_intr_cfg_DS3AISSigIntrEn_Shift                                                 2
#define cAf6_rxm23e23_per_chn_intr_cfg_DS3AISSigIntrEn_MaxVal                                              0x1
#define cAf6_rxm23e23_per_chn_intr_cfg_DS3AISSigIntrEn_MinVal                                              0x0
#define cAf6_rxm23e23_per_chn_intr_cfg_DS3AISSigIntrEn_RstVal                                              0x0

/*--------------------------------------
BitField Name: DE3RDIIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change RDI event to generate an interrupt.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3RDIIntrEn_Bit_Start                                                1
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3RDIIntrEn_Bit_End                                                  1
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3RDIIntrEn_Mask                                                 cBit1
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3RDIIntrEn_Shift                                                    1
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3RDIIntrEn_MaxVal                                                 0x1
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3RDIIntrEn_MinVal                                                 0x0
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3RDIIntrEn_RstVal                                                 0x0

/*--------------------------------------
BitField Name: DE3LofIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change LOF event to generate an interrupt.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3LofIntrEn_Bit_Start                                                0
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3LofIntrEn_Bit_End                                                  0
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3LofIntrEn_Mask                                                 cBit0
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3LofIntrEn_Shift                                                    0
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3LofIntrEn_MaxVal                                                 0x1
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3LofIntrEn_MinVal                                                 0x0
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3LofIntrEn_RstVal                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : RxM23E23 Framer per Channel Interrupt Change Status
Reg Addr   : 0x000405A0-0x000405BF
Reg Formula: 0x000405A0 +  de3id
    Where  : 
           + $de3id(0-23):
Reg Desc   : 
This is the per Channel interrupt tus of DS3/E3 Rx Framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_rxm23e23_per_chn_intr_stat_Base                                                     0x000405A0
#define cAf6Reg_rxm23e23_per_chn_intr_stat(de3id)                                         (0x000405A0+(de3id))
#define cAf6Reg_rxm23e23_per_chn_intr_stat_WidthVal                                                         32
#define cAf6Reg_rxm23e23_per_chn_intr_stat_WriteMask                                                       0x0

/*--------------------------------------
BitField Name: DS3FEAC_E3SSM
BitField Type: RW
BitField Desc: Set 1 if there is a change DS3 FEAC or E3G832 SSM event to
generate an interrupt.
BitField Bits: [7]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_stat_DS3FEAC_E3SSM_Bit_Start                                              7
#define cAf6_rxm23e23_per_chn_intr_stat_DS3FEAC_E3SSM_Bit_End                                                7
#define cAf6_rxm23e23_per_chn_intr_stat_DS3FEAC_E3SSM_Mask                                               cBit7
#define cAf6_rxm23e23_per_chn_intr_stat_DS3FEAC_E3SSM_Shift                                                  7
#define cAf6_rxm23e23_per_chn_intr_stat_DS3FEAC_E3SSM_MaxVal                                               0x1
#define cAf6_rxm23e23_per_chn_intr_stat_DS3FEAC_E3SSM_MinVal                                               0x0
#define cAf6_rxm23e23_per_chn_intr_stat_DS3FEAC_E3SSM_RstVal                                               0x0

/*--------------------------------------
BitField Name: DS3AIC_E3TM
BitField Type: RW
BitField Desc: Set 1 if there is a change DS3 AIC or E3G832 TM event to generate
an interrupt.
BitField Bits: [6]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_stat_DS3AIC_E3TM_Bit_Start                                                6
#define cAf6_rxm23e23_per_chn_intr_stat_DS3AIC_E3TM_Bit_End                                                  6
#define cAf6_rxm23e23_per_chn_intr_stat_DS3AIC_E3TM_Mask                                                 cBit6
#define cAf6_rxm23e23_per_chn_intr_stat_DS3AIC_E3TM_Shift                                                    6
#define cAf6_rxm23e23_per_chn_intr_stat_DS3AIC_E3TM_MaxVal                                                 0x1
#define cAf6_rxm23e23_per_chn_intr_stat_DS3AIC_E3TM_MinVal                                                 0x0
#define cAf6_rxm23e23_per_chn_intr_stat_DS3AIC_E3TM_RstVal                                                 0x0

/*--------------------------------------
BitField Name: DS3IDLESig_E3PT
BitField Type: RW
BitField Desc: Set 1 if there is a change DS3 IDLE signal or E3G832 PayloadType
event to generate an interrupt.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_stat_DS3IDLESig_E3PT_Bit_Start                                            5
#define cAf6_rxm23e23_per_chn_intr_stat_DS3IDLESig_E3PT_Bit_End                                              5
#define cAf6_rxm23e23_per_chn_intr_stat_DS3IDLESig_E3PT_Mask                                             cBit5
#define cAf6_rxm23e23_per_chn_intr_stat_DS3IDLESig_E3PT_Shift                                                5
#define cAf6_rxm23e23_per_chn_intr_stat_DS3IDLESig_E3PT_MaxVal                                             0x1
#define cAf6_rxm23e23_per_chn_intr_stat_DS3IDLESig_E3PT_MinVal                                             0x0
#define cAf6_rxm23e23_per_chn_intr_stat_DS3IDLESig_E3PT_RstVal                                             0x0

/*--------------------------------------
BitField Name: DE3AisAllOneIntr
BitField Type: RW
BitField Desc: Set 1 if there is a change AIS All One event to generate an
interrupt.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_stat_DE3AisAllOneIntr_Bit_Start                                           4
#define cAf6_rxm23e23_per_chn_intr_stat_DE3AisAllOneIntr_Bit_End                                             4
#define cAf6_rxm23e23_per_chn_intr_stat_DE3AisAllOneIntr_Mask                                            cBit4
#define cAf6_rxm23e23_per_chn_intr_stat_DE3AisAllOneIntr_Shift                                               4
#define cAf6_rxm23e23_per_chn_intr_stat_DE3AisAllOneIntr_MaxVal                                            0x1
#define cAf6_rxm23e23_per_chn_intr_stat_DE3AisAllOneIntr_MinVal                                            0x0
#define cAf6_rxm23e23_per_chn_intr_stat_DE3AisAllOneIntr_RstVal                                            0x0

/*--------------------------------------
BitField Name: DE3LosAllZeroIntr
BitField Type: RW
BitField Desc: Set 1 if there is a change LOS All Zero event to generate an
interrupt.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_stat_DE3LosAllZeroIntr_Bit_Start                                          3
#define cAf6_rxm23e23_per_chn_intr_stat_DE3LosAllZeroIntr_Bit_End                                            3
#define cAf6_rxm23e23_per_chn_intr_stat_DE3LosAllZeroIntr_Mask                                           cBit3
#define cAf6_rxm23e23_per_chn_intr_stat_DE3LosAllZeroIntr_Shift                                              3
#define cAf6_rxm23e23_per_chn_intr_stat_DE3LosAllZeroIntr_MaxVal                                           0x1
#define cAf6_rxm23e23_per_chn_intr_stat_DE3LosAllZeroIntr_MinVal                                           0x0
#define cAf6_rxm23e23_per_chn_intr_stat_DE3LosAllZeroIntr_RstVal                                           0x0

/*--------------------------------------
BitField Name: DS3AISSigIntr
BitField Type: RW
BitField Desc: Set 1 if there is a change AIS Signal event to generate an
interrupt.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_stat_DS3AISSigIntr_Bit_Start                                              2
#define cAf6_rxm23e23_per_chn_intr_stat_DS3AISSigIntr_Bit_End                                                2
#define cAf6_rxm23e23_per_chn_intr_stat_DS3AISSigIntr_Mask                                               cBit2
#define cAf6_rxm23e23_per_chn_intr_stat_DS3AISSigIntr_Shift                                                  2
#define cAf6_rxm23e23_per_chn_intr_stat_DS3AISSigIntr_MaxVal                                               0x1
#define cAf6_rxm23e23_per_chn_intr_stat_DS3AISSigIntr_MinVal                                               0x0
#define cAf6_rxm23e23_per_chn_intr_stat_DS3AISSigIntr_RstVal                                               0x0

/*--------------------------------------
BitField Name: DE3RDIIntr
BitField Type: RW
BitField Desc: Set 1 if there is a  change RDI event to generate an interrupt.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_stat_DE3RDIIntr_Bit_Start                                                 1
#define cAf6_rxm23e23_per_chn_intr_stat_DE3RDIIntr_Bit_End                                                   1
#define cAf6_rxm23e23_per_chn_intr_stat_DE3RDIIntr_Mask                                                  cBit1
#define cAf6_rxm23e23_per_chn_intr_stat_DE3RDIIntr_Shift                                                     1
#define cAf6_rxm23e23_per_chn_intr_stat_DE3RDIIntr_MaxVal                                                  0x1
#define cAf6_rxm23e23_per_chn_intr_stat_DE3RDIIntr_MinVal                                                  0x0
#define cAf6_rxm23e23_per_chn_intr_stat_DE3RDIIntr_RstVal                                                  0x0

/*--------------------------------------
BitField Name: DE3LofIntr
BitField Type: RW
BitField Desc: Set 1 if there is a  change LOF event to generate an interrupt.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_stat_DE3LofIntr_Bit_Start                                                 0
#define cAf6_rxm23e23_per_chn_intr_stat_DE3LofIntr_Bit_End                                                   0
#define cAf6_rxm23e23_per_chn_intr_stat_DE3LofIntr_Mask                                                  cBit0
#define cAf6_rxm23e23_per_chn_intr_stat_DE3LofIntr_Shift                                                     0
#define cAf6_rxm23e23_per_chn_intr_stat_DE3LofIntr_MaxVal                                                  0x1
#define cAf6_rxm23e23_per_chn_intr_stat_DE3LofIntr_MinVal                                                  0x0
#define cAf6_rxm23e23_per_chn_intr_stat_DE3LofIntr_RstVal                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : RxM23E23 Framer per Channel Current Status
Reg Addr   : 0x000405C0-0x000405DF
Reg Formula: 0x000405C0 +  de3id
    Where  : 
           + $de3id(0-23):
Reg Desc   : 
This is the per Channel Current tus of DS3/E3 Rx Framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_rxm23e23_per_chn_curr_stat_Base                                                     0x000405C0
#define cAf6Reg_rxm23e23_per_chn_curr_stat(de3id)                                         (0x000405C0+(de3id))
#define cAf6Reg_rxm23e23_per_chn_curr_stat_WidthVal                                                         32
#define cAf6Reg_rxm23e23_per_chn_curr_stat_WriteMask                                                       0x0

/*--------------------------------------
BitField Name: DS3FEAC_E3SSM_CurrSta
BitField Type: RW
BitField Desc: Current status of DS3 FEAC or E3G832 SSM event to generate an
interrupt.
BitField Bits: [7]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_curr_stat_DS3FEAC_E3SSM_CurrSta_Bit_Start                                       7
#define cAf6_rxm23e23_per_chn_curr_stat_DS3FEAC_E3SSM_CurrSta_Bit_End                                        7
#define cAf6_rxm23e23_per_chn_curr_stat_DS3FEAC_E3SSM_CurrSta_Mask                                       cBit7
#define cAf6_rxm23e23_per_chn_curr_stat_DS3FEAC_E3SSM_CurrSta_Shift                                          7
#define cAf6_rxm23e23_per_chn_curr_stat_DS3FEAC_E3SSM_CurrSta_MaxVal                                       0x1
#define cAf6_rxm23e23_per_chn_curr_stat_DS3FEAC_E3SSM_CurrSta_MinVal                                       0x0
#define cAf6_rxm23e23_per_chn_curr_stat_DS3FEAC_E3SSM_CurrSta_RstVal                                       0x0

/*--------------------------------------
BitField Name: DS3AIC_E3TM_CurrSta
BitField Type: RW
BitField Desc: Current status of DS3 AIC or E3G832 TM event to generate an
interrupt.
BitField Bits: [6]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_curr_stat_DS3AIC_E3TM_CurrSta_Bit_Start                                        6
#define cAf6_rxm23e23_per_chn_curr_stat_DS3AIC_E3TM_CurrSta_Bit_End                                          6
#define cAf6_rxm23e23_per_chn_curr_stat_DS3AIC_E3TM_CurrSta_Mask                                         cBit6
#define cAf6_rxm23e23_per_chn_curr_stat_DS3AIC_E3TM_CurrSta_Shift                                            6
#define cAf6_rxm23e23_per_chn_curr_stat_DS3AIC_E3TM_CurrSta_MaxVal                                         0x1
#define cAf6_rxm23e23_per_chn_curr_stat_DS3AIC_E3TM_CurrSta_MinVal                                         0x0
#define cAf6_rxm23e23_per_chn_curr_stat_DS3AIC_E3TM_CurrSta_RstVal                                         0x0

/*--------------------------------------
BitField Name: DS3IDLESig_E3PT_CurrSta
BitField Type: RW
BitField Desc: Current status of DS3 IDLE signal or E3G832 PayloadType event to
generate an interrupt.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_curr_stat_DS3IDLESig_E3PT_CurrSta_Bit_Start                                       5
#define cAf6_rxm23e23_per_chn_curr_stat_DS3IDLESig_E3PT_CurrSta_Bit_End                                       5
#define cAf6_rxm23e23_per_chn_curr_stat_DS3IDLESig_E3PT_CurrSta_Mask                                     cBit5
#define cAf6_rxm23e23_per_chn_curr_stat_DS3IDLESig_E3PT_CurrSta_Shift                                        5
#define cAf6_rxm23e23_per_chn_curr_stat_DS3IDLESig_E3PT_CurrSta_MaxVal                                     0x1
#define cAf6_rxm23e23_per_chn_curr_stat_DS3IDLESig_E3PT_CurrSta_MinVal                                     0x0
#define cAf6_rxm23e23_per_chn_curr_stat_DS3IDLESig_E3PT_CurrSta_RstVal                                     0x0

/*--------------------------------------
BitField Name: DE3AisAllOneCurrSta
BitField Type: RW
BitField Desc: Current status of AIS All One event to generate an interrupt.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_curr_stat_DE3AisAllOneCurrSta_Bit_Start                                        4
#define cAf6_rxm23e23_per_chn_curr_stat_DE3AisAllOneCurrSta_Bit_End                                          4
#define cAf6_rxm23e23_per_chn_curr_stat_DE3AisAllOneCurrSta_Mask                                         cBit4
#define cAf6_rxm23e23_per_chn_curr_stat_DE3AisAllOneCurrSta_Shift                                            4
#define cAf6_rxm23e23_per_chn_curr_stat_DE3AisAllOneCurrSta_MaxVal                                         0x1
#define cAf6_rxm23e23_per_chn_curr_stat_DE3AisAllOneCurrSta_MinVal                                         0x0
#define cAf6_rxm23e23_per_chn_curr_stat_DE3AisAllOneCurrSta_RstVal                                         0x0

/*--------------------------------------
BitField Name: DE3LosAllZeroCurrSta
BitField Type: RW
BitField Desc: Current status of LOS All Zero event to generate an interrupt.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_curr_stat_DE3LosAllZeroCurrSta_Bit_Start                                       3
#define cAf6_rxm23e23_per_chn_curr_stat_DE3LosAllZeroCurrSta_Bit_End                                         3
#define cAf6_rxm23e23_per_chn_curr_stat_DE3LosAllZeroCurrSta_Mask                                        cBit3
#define cAf6_rxm23e23_per_chn_curr_stat_DE3LosAllZeroCurrSta_Shift                                           3
#define cAf6_rxm23e23_per_chn_curr_stat_DE3LosAllZeroCurrSta_MaxVal                                        0x1
#define cAf6_rxm23e23_per_chn_curr_stat_DE3LosAllZeroCurrSta_MinVal                                        0x0
#define cAf6_rxm23e23_per_chn_curr_stat_DE3LosAllZeroCurrSta_RstVal                                        0x0

/*--------------------------------------
BitField Name: DS3AISSigCurrSta
BitField Type: RW
BitField Desc: Current status of AIS Signal event to generate an interrupt.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_curr_stat_DS3AISSigCurrSta_Bit_Start                                           2
#define cAf6_rxm23e23_per_chn_curr_stat_DS3AISSigCurrSta_Bit_End                                             2
#define cAf6_rxm23e23_per_chn_curr_stat_DS3AISSigCurrSta_Mask                                            cBit2
#define cAf6_rxm23e23_per_chn_curr_stat_DS3AISSigCurrSta_Shift                                               2
#define cAf6_rxm23e23_per_chn_curr_stat_DS3AISSigCurrSta_MaxVal                                            0x1
#define cAf6_rxm23e23_per_chn_curr_stat_DS3AISSigCurrSta_MinVal                                            0x0
#define cAf6_rxm23e23_per_chn_curr_stat_DS3AISSigCurrSta_RstVal                                            0x0

/*--------------------------------------
BitField Name: DE3RDICurrSta
BitField Type: RW
BitField Desc: Current status of RDI event to generate an interrupt.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_curr_stat_DE3RDICurrSta_Bit_Start                                              1
#define cAf6_rxm23e23_per_chn_curr_stat_DE3RDICurrSta_Bit_End                                                1
#define cAf6_rxm23e23_per_chn_curr_stat_DE3RDICurrSta_Mask                                               cBit1
#define cAf6_rxm23e23_per_chn_curr_stat_DE3RDICurrSta_Shift                                                  1
#define cAf6_rxm23e23_per_chn_curr_stat_DE3RDICurrSta_MaxVal                                               0x1
#define cAf6_rxm23e23_per_chn_curr_stat_DE3RDICurrSta_MinVal                                               0x0
#define cAf6_rxm23e23_per_chn_curr_stat_DE3RDICurrSta_RstVal                                               0x0

/*--------------------------------------
BitField Name: DE3LofCurrSta
BitField Type: RW
BitField Desc: Current status of LOF event to generate an interrupt.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_curr_stat_DE3LofCurrSta_Bit_Start                                              0
#define cAf6_rxm23e23_per_chn_curr_stat_DE3LofCurrSta_Bit_End                                                0
#define cAf6_rxm23e23_per_chn_curr_stat_DE3LofCurrSta_Mask                                               cBit0
#define cAf6_rxm23e23_per_chn_curr_stat_DE3LofCurrSta_Shift                                                  0
#define cAf6_rxm23e23_per_chn_curr_stat_DE3LofCurrSta_MaxVal                                               0x1
#define cAf6_rxm23e23_per_chn_curr_stat_DE3LofCurrSta_MinVal                                               0x0
#define cAf6_rxm23e23_per_chn_curr_stat_DE3LofCurrSta_RstVal                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : RxDS3E3 OH Pro DLK Swap Bit Enable
Reg Addr   : 0x04040B
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_rxde3_oh_dlk_swap_bit_cfg_Base                                                        0x04040B
#define cAf6Reg_rxde3_oh_dlk_swap_bit_cfg                                                             0x04040B
#define cAf6Reg_rxde3_oh_dlk_swap_bit_cfg_WidthVal                                                          32
#define cAf6Reg_rxde3_oh_dlk_swap_bit_cfg_WriteMask                                                        0x0

/*--------------------------------------
BitField Name: DlkSwapBitDs3En
BitField Type: RW
BitField Desc:
BitField Bits: [2]
--------------------------------------*/
#define cAf6_rxde3_oh_dlk_swap_bit_cfg_DlkSwapBitDs3En_Bit_Start                                             2
#define cAf6_rxde3_oh_dlk_swap_bit_cfg_DlkSwapBitDs3En_Bit_End                                               2
#define cAf6_rxde3_oh_dlk_swap_bit_cfg_DlkSwapBitDs3En_Mask                                              cBit2
#define cAf6_rxde3_oh_dlk_swap_bit_cfg_DlkSwapBitDs3En_Shift                                                 2
#define cAf6_rxde3_oh_dlk_swap_bit_cfg_DlkSwapBitDs3En_MaxVal                                              0x1
#define cAf6_rxde3_oh_dlk_swap_bit_cfg_DlkSwapBitDs3En_MinVal                                              0x0
#define cAf6_rxde3_oh_dlk_swap_bit_cfg_DlkSwapBitDs3En_RstVal                                              0x0

/*--------------------------------------
BitField Name: DlkSwapBitE3g751En
BitField Type: RW
BitField Desc:
BitField Bits: [1]
--------------------------------------*/
#define cAf6_rxde3_oh_dlk_swap_bit_cfg_DlkSwapBitE3g751En_Bit_Start                                          1
#define cAf6_rxde3_oh_dlk_swap_bit_cfg_DlkSwapBitE3g751En_Bit_End                                            1
#define cAf6_rxde3_oh_dlk_swap_bit_cfg_DlkSwapBitE3g751En_Mask                                           cBit1
#define cAf6_rxde3_oh_dlk_swap_bit_cfg_DlkSwapBitE3g751En_Shift                                              1
#define cAf6_rxde3_oh_dlk_swap_bit_cfg_DlkSwapBitE3g751En_MaxVal                                           0x1
#define cAf6_rxde3_oh_dlk_swap_bit_cfg_DlkSwapBitE3g751En_MinVal                                           0x0
#define cAf6_rxde3_oh_dlk_swap_bit_cfg_DlkSwapBitE3g751En_RstVal                                           0x0

/*--------------------------------------
BitField Name: DlkSwapBitE3g823En
BitField Type: RW
BitField Desc:
BitField Bits: [0]
--------------------------------------*/
#define cAf6_rxde3_oh_dlk_swap_bit_cfg_DlkSwapBitE3g823En_Bit_Start                                          0
#define cAf6_rxde3_oh_dlk_swap_bit_cfg_DlkSwapBitE3g823En_Bit_End                                            0
#define cAf6_rxde3_oh_dlk_swap_bit_cfg_DlkSwapBitE3g823En_Mask                                           cBit0
#define cAf6_rxde3_oh_dlk_swap_bit_cfg_DlkSwapBitE3g823En_Shift                                              0
#define cAf6_rxde3_oh_dlk_swap_bit_cfg_DlkSwapBitE3g823En_MaxVal                                           0x1
#define cAf6_rxde3_oh_dlk_swap_bit_cfg_DlkSwapBitE3g823En_MinVal                                           0x0
#define cAf6_rxde3_oh_dlk_swap_bit_cfg_DlkSwapBitE3g823En_RstVal                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : DS3/E3 Test Pattern Monitoring Global Control
Reg Addr   : 0x040700
Reg Formula: 0x040700 + id
    Where  : 
           + $id(0-3):
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_de3_testpat_mon_glb_cfg_Base                                                          0x040700
#define cAf6Reg_de3_testpat_mon_glb_cfg(id)                                                    (0x040700+(id))
#define cAf6Reg_de3_testpat_mon_glb_cfg_WidthVal                                                            32
#define cAf6Reg_de3_testpat_mon_glb_cfg_WriteMask                                                          0x0

/*--------------------------------------
BitField Name: RxDe3PtmEn
BitField Type: RW
BitField Desc: Bit Enable Test Pattern Monitoring
BitField Bits: [22]
--------------------------------------*/
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmEn_Bit_Start                                                   22
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmEn_Bit_End                                                     22
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmEn_Mask                                                    cBit22
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmEn_Shift                                                       22
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmEn_MaxVal                                                     0x1
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmEn_MinVal                                                     0x0
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmEn_RstVal                                                     0x0

/*--------------------------------------
BitField Name: RxDe3PtmID
BitField Type: RW
BitField Desc: To configure which DS3/E3 line is selected  to monitor the Test
Pattern
BitField Bits: [21:18]
--------------------------------------*/
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmID_Bit_Start                                                   18
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmID_Bit_End                                                     21
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmID_Mask                                                 cBit21_18
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmID_Shift                                                       18
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmID_MaxVal                                                     0xf
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmID_MinVal                                                     0x0
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmID_RstVal                                                     0x0

/*--------------------------------------
BitField Name: RxDe3PtmIntEn
BitField Type: RW
BitField Desc: Interrupt enable to output upint
BitField Bits: [17]
--------------------------------------*/
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmIntEn_Bit_Start                                                17
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmIntEn_Bit_End                                                  17
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmIntEn_Mask                                                 cBit17
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmIntEn_Shift                                                    17
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmIntEn_MaxVal                                                  0x1
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmIntEn_MinVal                                                  0x0
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmIntEn_RstVal                                                  0x0

/*--------------------------------------
BitField Name: RxDe3PtmSatura
BitField Type: RW
BitField Desc: Counter BERT mode 1: PTM Counter Saturation Mode 0: PTM Counter
Roll-Over Mode
BitField Bits: [16]
--------------------------------------*/
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmSatura_Bit_Start                                               16
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmSatura_Bit_End                                                 16
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmSatura_Mask                                                cBit16
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmSatura_Shift                                                   16
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmSatura_MaxVal                                                 0x1
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmSatura_MinVal                                                 0x0
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmSatura_RstVal                                                 0x0

/*--------------------------------------
BitField Name: RxDe3PtmStkCfg
BitField Type: RW
BitField Desc: BERT error sticky
BitField Bits: [15]
--------------------------------------*/
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmStkCfg_Bit_Start                                               15
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmStkCfg_Bit_End                                                 15
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmStkCfg_Mask                                                cBit15
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmStkCfg_Shift                                                   15
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmStkCfg_MaxVal                                                 0x1
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmStkCfg_MinVal                                                 0x0
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmStkCfg_RstVal                                                 0x0

/*--------------------------------------
BitField Name: RxDe3PtmLopThr
BitField Type: RW
BitField Desc: Threshold for Loss Of Pattern detection. Status will go LOP if
the number of             error patterns (8 bits) in 16 consecutive patterns is
more than or equal the RxDe3PtmLopThr
BitField Bits: [14:11]
--------------------------------------*/
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmLopThr_Bit_Start                                               11
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmLopThr_Bit_End                                                 14
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmLopThr_Mask                                             cBit14_11
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmLopThr_Shift                                                   11
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmLopThr_MaxVal                                                 0xf
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmLopThr_MinVal                                                 0x0
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmLopThr_RstVal                                                 0x0

/*--------------------------------------
BitField Name: RxDe3PtmSynThr
BitField Type: RW
BitField Desc: Threshold for Sync detection. Status will go SYN if the number of
corrected              consecutive patterns (8 bits) is more than or equal the
RxDe3PtmSynThr.
BitField Bits: [10:7]
--------------------------------------*/
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmSynThr_Bit_Start                                                7
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmSynThr_Bit_End                                                 10
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmSynThr_Mask                                              cBit10_7
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmSynThr_Shift                                                    7
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmSynThr_MaxVal                                                 0xf
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmSynThr_MinVal                                                 0x0
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmSynThr_RstVal                                                 0x0

/*--------------------------------------
BitField Name: RxDe3PtmSwap
BitField Type: RW
BitField Desc: PTM swap Bit. If it is set to 1, pattern is transmitted LSB
first, then MSB.             Normal is set to 0.
BitField Bits: [6]
--------------------------------------*/
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmSwap_Bit_Start                                                  6
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmSwap_Bit_End                                                    6
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmSwap_Mask                                                   cBit6
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmSwap_Shift                                                      6
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmSwap_MaxVal                                                   0x1
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmSwap_MinVal                                                   0x0
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmSwap_RstVal                                                   0x0

/*--------------------------------------
BitField Name: RxDe3PtmInv
BitField Type: RW
BitField Desc: PTM inversion Bit. Normal is set to 0.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmInv_Bit_Start                                                   5
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmInv_Bit_End                                                     5
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmInv_Mask                                                    cBit5
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmInv_Shift                                                       5
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmInv_MaxVal                                                    0x1
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmInv_MinVal                                                    0x0
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmInv_RstVal                                                    0x0

/*--------------------------------------
BitField Name: RxDe3PtmReSync
BitField Type: RW
BitField Desc: Bit force Ptm engine n from a certain te to loss sync state.
Engine will                 re-search pattern test. Note: if this bit is hold
the value 1, engine is always in loss sync state.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmReSync_Bit_Start                                                4
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmReSync_Bit_End                                                  4
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmReSync_Mask                                                 cBit4
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmReSync_Shift                                                    4
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmReSync_MaxVal                                                 0x1
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmReSync_MinVal                                                 0x0
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmReSync_RstVal                                                 0x0

/*--------------------------------------
BitField Name: RxDe3PtmMod
BitField Type: RW
BitField Desc: Monitor DS3/E3 BERT mode 0000: Prbs9 0001: Prbs11 0010: Prbs15
0011: Prbs20 0100: Prbs20 0101: Qrss20 0110: Prbs23 0111: Prbs31 1000: Sequence
1001: AllOne 1010: AllZero 1011: AA55
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmMod_Bit_Start                                                   0
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmMod_Bit_End                                                     3
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmMod_Mask                                                  cBit3_0
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmMod_Shift                                                       0
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmMod_MaxVal                                                    0xf
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmMod_MinVal                                                    0x0
#define cAf6_de3_testpat_mon_glb_cfg_RxDe3PtmMod_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : DS3/E3 Test Pattern Monitoring Sticky Enable
Reg Addr   : 0x040701
Reg Formula: 0x040701 + 16*id
    Where  : 
           + $id(0-3):
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_de3_testpat_mon_sticky_en_Base                                                        0x040701
#define cAf6Reg_de3_testpat_mon_sticky_en(id)                                               (0x040701+16*(id))
#define cAf6Reg_de3_testpat_mon_sticky_en_WidthVal                                                          32
#define cAf6Reg_de3_testpat_mon_sticky_en_WriteMask                                                        0x0

/*--------------------------------------
BitField Name: RxDe3PtmStkEn
BitField Type: RW
BitField Desc: Enable PTM Sticky
BitField Bits: [0]
--------------------------------------*/
#define cAf6_de3_testpat_mon_sticky_en_RxDe3PtmStkEn_Bit_Start                                               0
#define cAf6_de3_testpat_mon_sticky_en_RxDe3PtmStkEn_Bit_End                                                 0
#define cAf6_de3_testpat_mon_sticky_en_RxDe3PtmStkEn_Mask                                                cBit0
#define cAf6_de3_testpat_mon_sticky_en_RxDe3PtmStkEn_Shift                                                   0
#define cAf6_de3_testpat_mon_sticky_en_RxDe3PtmStkEn_MaxVal                                                0x1
#define cAf6_de3_testpat_mon_sticky_en_RxDe3PtmStkEn_MinVal                                                0x0
#define cAf6_de3_testpat_mon_sticky_en_RxDe3PtmStkEn_RstVal                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : DS3/E3 Test Pattern Monitoring Error Counter
Reg Addr   : 0x040702
Reg Formula: 0x040702 +  16*id +  R2Clr
    Where  : 
           + $id(0-3):
           + $R2Clr(0-1): 0: R2CLR, 1: RO
Reg Desc   : 
These registers are used to be error counter report of PTM while the monitor is in frame

------------------------------------------------------------------------------*/
#define cAf6Reg_de3_testpat_mon_err_cnt_Base                                                          0x040702
#define cAf6Reg_de3_testpat_mon_err_cnt(id, R2Clr)                                  (0x040702+16*(id)+(R2Clr))
#define cAf6Reg_de3_testpat_mon_err_cnt_WidthVal                                                            32
#define cAf6Reg_de3_testpat_mon_err_cnt_WriteMask                                                          0x0

/*--------------------------------------
BitField Name: RxDE3PtmErrcnt
BitField Type: RW
BitField Desc: Error counter for BERT monitoring.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_de3_testpat_mon_err_cnt_RxDE3PtmErrcnt_Bit_Start                                                0
#define cAf6_de3_testpat_mon_err_cnt_RxDE3PtmErrcnt_Bit_End                                                 31
#define cAf6_de3_testpat_mon_err_cnt_RxDE3PtmErrcnt_Mask                                              cBit31_0
#define cAf6_de3_testpat_mon_err_cnt_RxDE3PtmErrcnt_Shift                                                    0
#define cAf6_de3_testpat_mon_err_cnt_RxDE3PtmErrcnt_MaxVal                                          0xffffffff
#define cAf6_de3_testpat_mon_err_cnt_RxDE3PtmErrcnt_MinVal                                                 0x0
#define cAf6_de3_testpat_mon_err_cnt_RxDE3PtmErrcnt_RstVal                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : DS3/E3 Test Pattern Monitoring Good Bit Counter
Reg Addr   : 0x040704
Reg Formula: 0x040704 +  16*id +  R2Clr
    Where  : 
           + $id(0-3):
           + $R2Clr(0-1): 0: R2CLR, 1: RO
Reg Desc   : 
These registers are used to be good bit counter report of PTM while the monitor is in frame

------------------------------------------------------------------------------*/
#define cAf6Reg_de3_testpat_mon_goodbit_cnt_Base                                                      0x040704
#define cAf6Reg_de3_testpat_mon_goodbit_cnt(id, R2Clr)                              (0x040704+16*(id)+(R2Clr))
#define cAf6Reg_de3_testpat_mon_goodbit_cnt_WidthVal                                                        32
#define cAf6Reg_de3_testpat_mon_goodbit_cnt_WriteMask                                                      0x0

/*--------------------------------------
BitField Name: RxDE3PtmGoodCnt
BitField Type: RW
BitField Desc: Good bit counter for BERT monitoring.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_de3_testpat_mon_goodbit_cnt_RxDE3PtmGoodCnt_Bit_Start                                           0
#define cAf6_de3_testpat_mon_goodbit_cnt_RxDE3PtmGoodCnt_Bit_End                                            31
#define cAf6_de3_testpat_mon_goodbit_cnt_RxDE3PtmGoodCnt_Mask                                         cBit31_0
#define cAf6_de3_testpat_mon_goodbit_cnt_RxDE3PtmGoodCnt_Shift                                               0
#define cAf6_de3_testpat_mon_goodbit_cnt_RxDE3PtmGoodCnt_MaxVal                                     0xffffffff
#define cAf6_de3_testpat_mon_goodbit_cnt_RxDE3PtmGoodCnt_MinVal                                            0x0
#define cAf6_de3_testpat_mon_goodbit_cnt_RxDE3PtmGoodCnt_RstVal                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : DS3/E3 Test Pattern Monitoring Lost Bit Counter
Reg Addr   : 0x040706
Reg Formula: 0x040706 +  16*id +  R2Clr
    Where  : 
           + $id(0-3):
           + $R2Clr(0-1): 0: R2CLR, 1: RO
Reg Desc   : 
These registers are used to be bit counter report of PTM while the monitor is in loss of frame

------------------------------------------------------------------------------*/
#define cAf6Reg_de3_testpat_mon_lostbit_cnt_Base                                                      0x040706
#define cAf6Reg_de3_testpat_mon_lostbit_cnt(id, R2Clr)                              (0x040706+16*(id)+(R2Clr))
#define cAf6Reg_de3_testpat_mon_lostbit_cnt_WidthVal                                                        32
#define cAf6Reg_de3_testpat_mon_lostbit_cnt_WriteMask                                                      0x0

/*--------------------------------------
BitField Name: RxDE3PtmLostCnt
BitField Type: RW
BitField Desc: Counter for BERT monitoring.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_de3_testpat_mon_lostbit_cnt_RxDE3PtmLostCnt_Bit_Start                                           0
#define cAf6_de3_testpat_mon_lostbit_cnt_RxDE3PtmLostCnt_Bit_End                                            31
#define cAf6_de3_testpat_mon_lostbit_cnt_RxDE3PtmLostCnt_Mask                                         cBit31_0
#define cAf6_de3_testpat_mon_lostbit_cnt_RxDE3PtmLostCnt_Shift                                               0
#define cAf6_de3_testpat_mon_lostbit_cnt_RxDE3PtmLostCnt_MaxVal                                     0xffffffff
#define cAf6_de3_testpat_mon_lostbit_cnt_RxDE3PtmLostCnt_MinVal                                            0x0
#define cAf6_de3_testpat_mon_lostbit_cnt_RxDE3PtmLostCnt_RstVal                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : DS3/E3 Test Pattern Monitoring Counter Loading
Reg Addr   : 0x040708
Reg Formula: 0x040708 + 16*id
    Where  : 
           + $id(0-3):
Reg Desc   : 
This register is used to configure the enable to load all the test pattern monitor to the loading registers at the same time.

------------------------------------------------------------------------------*/
#define cAf6Reg_de3_testpat_mon_cnt_cfg_Base                                                          0x040708
#define cAf6Reg_de3_testpat_mon_cnt_cfg(id)                                                 (0x040708+16*(id))
#define cAf6Reg_de3_testpat_mon_cnt_cfg_WidthVal                                                            32
#define cAf6Reg_de3_testpat_mon_cnt_cfg_WriteMask                                                          0x0

/*--------------------------------------
BitField Name: RxDE3PtmCntLoadEn
BitField Type: RW
BitField Desc: DS3/E3 Pattern Monitoring Counter Load Enable.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_de3_testpat_mon_cnt_cfg_RxDE3PtmCntLoadEn_Bit_Start                                             0
#define cAf6_de3_testpat_mon_cnt_cfg_RxDE3PtmCntLoadEn_Bit_End                                               0
#define cAf6_de3_testpat_mon_cnt_cfg_RxDE3PtmCntLoadEn_Mask                                              cBit0
#define cAf6_de3_testpat_mon_cnt_cfg_RxDE3PtmCntLoadEn_Shift                                                 0
#define cAf6_de3_testpat_mon_cnt_cfg_RxDE3PtmCntLoadEn_MaxVal                                              0x1
#define cAf6_de3_testpat_mon_cnt_cfg_RxDE3PtmCntLoadEn_MinVal                                              0x0
#define cAf6_de3_testpat_mon_cnt_cfg_RxDE3PtmCntLoadEn_RstVal                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : DS3/E3 Test Pattern Monitoring Error-Bit Counter Loading
Reg Addr   : 0x040709
Reg Formula: 0x040709 + 16*id
    Where  : 
           + $id(0-3):
Reg Desc   : 
This register stores the value of Error-Bit counter after counter loading action

------------------------------------------------------------------------------*/
#define cAf6Reg_de3_testpat_mon_errbit_cnt_Base                                                       0x040709
#define cAf6Reg_de3_testpat_mon_errbit_cnt(id)                                              (0x040709+16*(id))
#define cAf6Reg_de3_testpat_mon_errbit_cnt_WidthVal                                                         32
#define cAf6Reg_de3_testpat_mon_errbit_cnt_WriteMask                                                       0x0

/*--------------------------------------
BitField Name: RxDE3PtmLoadErrCnt
BitField Type: RW
BitField Desc: Load Error counter.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_de3_testpat_mon_errbit_cnt_RxDE3PtmLoadErrCnt_Bit_Start                                         0
#define cAf6_de3_testpat_mon_errbit_cnt_RxDE3PtmLoadErrCnt_Bit_End                                          31
#define cAf6_de3_testpat_mon_errbit_cnt_RxDE3PtmLoadErrCnt_Mask                                       cBit31_0
#define cAf6_de3_testpat_mon_errbit_cnt_RxDE3PtmLoadErrCnt_Shift                                             0
#define cAf6_de3_testpat_mon_errbit_cnt_RxDE3PtmLoadErrCnt_MaxVal                                   0xffffffff
#define cAf6_de3_testpat_mon_errbit_cnt_RxDE3PtmLoadErrCnt_MinVal                                          0x0
#define cAf6_de3_testpat_mon_errbit_cnt_RxDE3PtmLoadErrCnt_RstVal                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : DS3/E3 Test Pattern Monitoring Good-Bit Counter Loading
Reg Addr   : 0x04070A
Reg Formula: 0x04070A +  16*id
    Where  : 
           + $id(0-3):
Reg Desc   : 
This register stores the value of Good-Bit counter after counter loading action

------------------------------------------------------------------------------*/
#define cAf6Reg_de3_testpat_mon_goodbit_cnt_loading_Base                                              0x04070A
#define cAf6Reg_de3_testpat_mon_goodbit_cnt_loading(id)                                     (0x04070A+16*(id))
#define cAf6Reg_de3_testpat_mon_goodbit_cnt_loading_WidthVal                                                32
#define cAf6Reg_de3_testpat_mon_goodbit_cnt_loading_WriteMask                                              0x0

/*--------------------------------------
BitField Name: RxDE3PtmGdCntLoad
BitField Type: RW
BitField Desc: Load Good counter.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_de3_testpat_mon_goodbit_cnt_loading_RxDE3PtmGdCntLoad_Bit_Start                                       0
#define cAf6_de3_testpat_mon_goodbit_cnt_loading_RxDE3PtmGdCntLoad_Bit_End                                      31
#define cAf6_de3_testpat_mon_goodbit_cnt_loading_RxDE3PtmGdCntLoad_Mask                                cBit31_0
#define cAf6_de3_testpat_mon_goodbit_cnt_loading_RxDE3PtmGdCntLoad_Shift                                       0
#define cAf6_de3_testpat_mon_goodbit_cnt_loading_RxDE3PtmGdCntLoad_MaxVal                              0xffffffff
#define cAf6_de3_testpat_mon_goodbit_cnt_loading_RxDE3PtmGdCntLoad_MinVal                                     0x0
#define cAf6_de3_testpat_mon_goodbit_cnt_loading_RxDE3PtmGdCntLoad_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : DS3/E3 Test Pattern Monitoring Lost-Bit Counter Loading
Reg Addr   : 0x04070B
Reg Formula: 0x04070B +  16*id
    Where  : 
           + $id(0-3):
Reg Desc   : 
This register stores the value of Lost-Bit counter after counter loading action

------------------------------------------------------------------------------*/
#define cAf6Reg_de3_testpat_mon_lostbit_cnt_loading_Base                                              0x04070B
#define cAf6Reg_de3_testpat_mon_lostbit_cnt_loading(id)                                     (0x04070B+16*(id))
#define cAf6Reg_de3_testpat_mon_lostbit_cnt_loading_WidthVal                                                32
#define cAf6Reg_de3_testpat_mon_lostbit_cnt_loading_WriteMask                                              0x0

/*--------------------------------------
BitField Name: RxDE3PtmLossCntLoad
BitField Type: RW
BitField Desc: Load Loss counter.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_de3_testpat_mon_lostbit_cnt_loading_RxDE3PtmLossCntLoad_Bit_Start                                       0
#define cAf6_de3_testpat_mon_lostbit_cnt_loading_RxDE3PtmLossCntLoad_Bit_End                                      31
#define cAf6_de3_testpat_mon_lostbit_cnt_loading_RxDE3PtmLossCntLoad_Mask                                cBit31_0
#define cAf6_de3_testpat_mon_lostbit_cnt_loading_RxDE3PtmLossCntLoad_Shift                                       0
#define cAf6_de3_testpat_mon_lostbit_cnt_loading_RxDE3PtmLossCntLoad_MaxVal                              0xffffffff
#define cAf6_de3_testpat_mon_lostbit_cnt_loading_RxDE3PtmLossCntLoad_MinVal                                     0x0
#define cAf6_de3_testpat_mon_lostbit_cnt_loading_RxDE3PtmLossCntLoad_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : RxM12E12 Control
Reg Addr   : 0x00041000 - 0x000410FF
Reg Formula: 0x00041000 +  8*de3id + de2id
    Where  : 
           + $de3id(0-23)
           + $de2id(0-6):
Reg Desc   : 
The RxM12E12 Control is use to configure for per channel Rx DS2/E2 operation.

------------------------------------------------------------------------------*/
#define cAf6Reg_rxm12e12_ctrl_Base                                                                  0x00041000
#define cAf6Reg_rxm12e12_ctrl(de3id, de2id)                                     (0x00041000+8*(de3id)+(de2id))
#define cAf6Reg_rxm12e12_ctrl_WidthVal                                                                      32
#define cAf6Reg_rxm12e12_ctrl_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: RxDS2E2FrcLof
BitField Type: RW
BitField Desc: This bit is set to 1 to force the Rx DS3/E3 framer into LOF tus.
1: force LOF. 0: not force LOF.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_rxm12e12_ctrl_RxDS2E2FrcLof_Bit_Start                                                           2
#define cAf6_rxm12e12_ctrl_RxDS2E2FrcLof_Bit_End                                                             2
#define cAf6_rxm12e12_ctrl_RxDS2E2FrcLof_Mask                                                            cBit2
#define cAf6_rxm12e12_ctrl_RxDS2E2FrcLof_Shift                                                               2
#define cAf6_rxm12e12_ctrl_RxDS2E2FrcLof_MaxVal                                                            0x1
#define cAf6_rxm12e12_ctrl_RxDS2E2FrcLof_MinVal                                                            0x0
#define cAf6_rxm12e12_ctrl_RxDS2E2FrcLof_RstVal                                                            0x0

/*--------------------------------------
BitField Name: RxDS2E2Md
BitField Type: RW
BitField Desc: Rx DS2/E2 framing mode 00: DS2 T1.107 carrying 4 DS1s 01: DS2
G.747 carrying 3 E1s 10: E2 G.742 carrying 4 E1s 11: Reserved
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_rxm12e12_ctrl_RxDS2E2Md_Bit_Start                                                               0
#define cAf6_rxm12e12_ctrl_RxDS2E2Md_Bit_End                                                                 1
#define cAf6_rxm12e12_ctrl_RxDS2E2Md_Mask                                                              cBit1_0
#define cAf6_rxm12e12_ctrl_RxDS2E2Md_Shift                                                                   0
#define cAf6_rxm12e12_ctrl_RxDS2E2Md_MaxVal                                                                0x3
#define cAf6_rxm12e12_ctrl_RxDS2E2Md_MinVal                                                                0x0
#define cAf6_rxm12e12_ctrl_RxDS2E2Md_RstVal                                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : PDH RxM12E12 Per STS/VC Payload Control
Reg Addr   : 0x00041300 - 0x0004131F
Reg Formula: 0x00041300 +  stsid
    Where  : 
           + $stsid(0-23):
Reg Desc   : 
The STS configuration registers are used to configure kinds of STS-1s and the kinds of VT/TUs in each STS-1.

------------------------------------------------------------------------------*/
#define cAf6Reg_rxm12e12_stsvc_payload_ctrl_Base                                                    0x00041300
#define cAf6Reg_rxm12e12_stsvc_payload_ctrl(stsid)                                        (0x00041300+(stsid))
#define cAf6Reg_rxm12e12_stsvc_payload_ctrl_WidthVal                                                        32
#define cAf6Reg_rxm12e12_stsvc_payload_ctrl_WriteMask                                                      0x0

/*--------------------------------------
BitField Name: PDHVtRxMapTug26Type
BitField Type: RW
BitField Desc: Define types of VT/TUs in TUG-2-6. 0: TU11/VT1.5 type 1: TU12/VT2
type 2: reserved 3: reserved
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug26Type_Bit_Start                                      12
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug26Type_Bit_End                                        13
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug26Type_Mask                                    cBit13_12
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug26Type_Shift                                          12
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug26Type_MaxVal                                        0x3
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug26Type_MinVal                                        0x0
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug26Type_RstVal                                        0x0

/*--------------------------------------
BitField Name: PDHVtRxMapTug25Type
BitField Type: RW
BitField Desc: Define types of VT/TUs in TUG-2-5
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug25Type_Bit_Start                                      10
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug25Type_Bit_End                                        11
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug25Type_Mask                                    cBit11_10
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug25Type_Shift                                          10
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug25Type_MaxVal                                        0x3
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug25Type_MinVal                                        0x0
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug25Type_RstVal                                        0x0

/*--------------------------------------
BitField Name: PDHVtRxMapTug24Type
BitField Type: RW
BitField Desc: Define types of VT/TUs in TUG-2-4
BitField Bits: [9:8]
--------------------------------------*/
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug24Type_Bit_Start                                       8
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug24Type_Bit_End                                         9
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug24Type_Mask                                      cBit9_8
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug24Type_Shift                                           8
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug24Type_MaxVal                                        0x3
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug24Type_MinVal                                        0x0
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug24Type_RstVal                                        0x0

/*--------------------------------------
BitField Name: PDHVtRxMapTug23Type
BitField Type: RW
BitField Desc: Define types of VT/TUs in TUG-2-3
BitField Bits: [7:6]
--------------------------------------*/
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug23Type_Bit_Start                                       6
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug23Type_Bit_End                                         7
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug23Type_Mask                                      cBit7_6
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug23Type_Shift                                           6
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug23Type_MaxVal                                        0x3
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug23Type_MinVal                                        0x0
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug23Type_RstVal                                        0x0

/*--------------------------------------
BitField Name: PDHVtRxMapTug22Type
BitField Type: RW
BitField Desc: Define types of VT/TUs in TUG-2-2
BitField Bits: [5:4]
--------------------------------------*/
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug22Type_Bit_Start                                       4
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug22Type_Bit_End                                         5
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug22Type_Mask                                      cBit5_4
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug22Type_Shift                                           4
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug22Type_MaxVal                                        0x3
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug22Type_MinVal                                        0x0
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug22Type_RstVal                                        0x0

/*--------------------------------------
BitField Name: PDHVtRxMapTug21Type
BitField Type: RW
BitField Desc: Define types of VT/TUs in TUG-2-1
BitField Bits: [3:2]
--------------------------------------*/
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug21Type_Bit_Start                                       2
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug21Type_Bit_End                                         3
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug21Type_Mask                                      cBit3_2
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug21Type_Shift                                           2
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug21Type_MaxVal                                        0x3
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug21Type_MinVal                                        0x0
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug21Type_RstVal                                        0x0

/*--------------------------------------
BitField Name: PDHVtRxMapTug20Type
BitField Type: RW
BitField Desc: Define types of VT/TUs in TUG-2-0
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug20Type_Bit_Start                                       0
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug20Type_Bit_End                                         1
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug20Type_Mask                                      cBit1_0
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug20Type_Shift                                           0
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug20Type_MaxVal                                        0x3
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug20Type_MinVal                                        0x0
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug20Type_RstVal                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : RxM12E12 HW Status
Reg Addr   : 0x00041200 - 0x000412FE
Reg Formula: 0x00041200 +  8*de3id + de2id
    Where  : 
           + $de3id(0-23)
           + $de2id(0-6):
Reg Desc   : 
for HW debug only

------------------------------------------------------------------------------*/
#define cAf6Reg_rxm12e12_hw_stat_Base                                                               0x00041200
#define cAf6Reg_rxm12e12_hw_stat(de3id, de2id)                                  (0x00041200+8*(de3id)+(de2id))
#define cAf6Reg_rxm12e12_hw_stat_WidthVal                                                                   32
#define cAf6Reg_rxm12e12_hw_stat_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: RxM12E12Status
BitField Type: RW
BitField Desc: for HW debug only
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_rxm12e12_hw_stat_RxM12E12Status_Bit_Start                                                       0
#define cAf6_rxm12e12_hw_stat_RxM12E12Status_Bit_End                                                         1
#define cAf6_rxm12e12_hw_stat_RxM12E12Status_Mask                                                      cBit1_0
#define cAf6_rxm12e12_hw_stat_RxM12E12Status_Shift                                                           0
#define cAf6_rxm12e12_hw_stat_RxM12E12Status_MaxVal                                                        0x3
#define cAf6_rxm12e12_hw_stat_RxM12E12Status_MinVal                                                        0x0
#define cAf6_rxm12e12_hw_stat_RxM12E12Status_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : RxM12E12 Framer Channel Interrupt Enable Control
Reg Addr   : 0x000413FE
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the Channel interrupt enable of DS2/E2 Rx Framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_RxM12E12_chn_intr_cfg_Base                                                          0x000413FE
#define cAf6Reg_RxM12E12_chn_intr_cfg                                                               0x000413FE
#define cAf6Reg_RxM12E12_chn_intr_cfg_WidthVal                                                              32
#define cAf6Reg_RxM12E12_chn_intr_cfg_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: RxM12E12IntrEn
BitField Type: RW
BitField Desc: Each bit correspond with each channel DS2/E2 in DS3/E3 0: Disable
1: Enable
BitField Bits: [6:0]
--------------------------------------*/
#define cAf6_RxM12E12_chn_intr_cfg_RxM12E12IntrEn_Bit_Start                                                  0
#define cAf6_RxM12E12_chn_intr_cfg_RxM12E12IntrEn_Bit_End                                                    6
#define cAf6_RxM12E12_chn_intr_cfg_RxM12E12IntrEn_Mask                                                 cBit6_0
#define cAf6_RxM12E12_chn_intr_cfg_RxM12E12IntrEn_Shift                                                      0
#define cAf6_RxM12E12_chn_intr_cfg_RxM12E12IntrEn_MaxVal                                                  0x7f
#define cAf6_RxM12E12_chn_intr_cfg_RxM12E12IntrEn_MinVal                                                   0x0
#define cAf6_RxM12E12_chn_intr_cfg_RxM12E12IntrEn_RstVal                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : RxM12E12 Framer Channel Interrupt Status
Reg Addr   : 0x000413FF
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the Channel interrupt tus of DS2/E2 Rx Framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_RxM12E12_chn_intr_stat_Base                                                         0x000413FF
#define cAf6Reg_RxM12E12_chn_intr_stat                                                              0x000413FF
#define cAf6Reg_RxM12E12_chn_intr_stat_WidthVal                                                             32
#define cAf6Reg_RxM12E12_chn_intr_stat_WriteMask                                                           0x0

/*--------------------------------------
BitField Name: RxM12E12IntrSta
BitField Type: RW
BitField Desc: Each bit correspond with each channel DS2/E2 in DS3/E3 0: No
Interrupt 1: Have Interrupt
BitField Bits: [6:0]
--------------------------------------*/
#define cAf6_RxM12E12_chn_intr_stat_RxM12E12IntrSta_Bit_Start                                                0
#define cAf6_RxM12E12_chn_intr_stat_RxM12E12IntrSta_Bit_End                                                  6
#define cAf6_RxM12E12_chn_intr_stat_RxM12E12IntrSta_Mask                                               cBit6_0
#define cAf6_RxM12E12_chn_intr_stat_RxM12E12IntrSta_Shift                                                    0
#define cAf6_RxM12E12_chn_intr_stat_RxM12E12IntrSta_MaxVal                                                0x7f
#define cAf6_RxM12E12_chn_intr_stat_RxM12E12IntrSta_MinVal                                                 0x0
#define cAf6_RxM12E12_chn_intr_stat_RxM12E12IntrSta_RstVal                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : RxM12E12 Framer per Channel Interrupt Enable Control
Reg Addr   : 0x00041380-0x0004139F
Reg Formula: 0x00041380 +  de3id
    Where  : 
           + $de3id(0-23):
Reg Desc   : 
This is the per Channel interrupt enable of DS2/E2 Rx Framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_RxM12E12_per_chn_intr_cfg_Base                                                      0x00041380
#define cAf6Reg_RxM12E12_per_chn_intr_cfg(de3id)                                          (0x00041380+(de3id))
#define cAf6Reg_RxM12E12_per_chn_intr_cfg_WidthVal                                                          32
#define cAf6Reg_RxM12E12_per_chn_intr_cfg_WriteMask                                                        0x0

/*--------------------------------------
BitField Name: DE2LOFIntrEn
BitField Type: RW
BitField Desc: Each bit correspond with each channel DS2/E2 in DS3/E3 0: No
Interrupt 1: Have Interrupt
BitField Bits: [6:0]
--------------------------------------*/
#define cAf6_RxM12E12_per_chn_intr_cfg_DE2LOFIntrEn_Bit_Start                                                0
#define cAf6_RxM12E12_per_chn_intr_cfg_DE2LOFIntrEn_Bit_End                                                  6
#define cAf6_RxM12E12_per_chn_intr_cfg_DE2LOFIntrEn_Mask                                               cBit6_0
#define cAf6_RxM12E12_per_chn_intr_cfg_DE2LOFIntrEn_Shift                                                    0
#define cAf6_RxM12E12_per_chn_intr_cfg_DE2LOFIntrEn_MaxVal                                                0x7f
#define cAf6_RxM12E12_per_chn_intr_cfg_DE2LOFIntrEn_MinVal                                                 0x0
#define cAf6_RxM12E12_per_chn_intr_cfg_DE2LOFIntrEn_RstVal                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : RxM12E12 Framer per Channel Interrupt Change Status
Reg Addr   : 0x000413A0-0x000413BF
Reg Formula: 0x000413A0 +  de3id
    Where  : 
           + $de3id(0-23):
Reg Desc   : 
This is the per Channel interrupt status of DS2/E2 Rx Framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_RxM12E12_per_chn_intr_stat_Base                                                     0x000413A0
#define cAf6Reg_RxM12E12_per_chn_intr_stat(de3id)                                         (0x000413A0+(de3id))
#define cAf6Reg_RxM12E12_per_chn_intr_stat_WidthVal                                                         32
#define cAf6Reg_RxM12E12_per_chn_intr_stat_WriteMask                                                       0x0

/*--------------------------------------
BitField Name: DE2LOFIntr
BitField Type: RW
BitField Desc: Each bit correspond with each channel DS2/E2 in DS3/E3 0: No
Interrupt 1: Have Interrupt
BitField Bits: [6:0]
--------------------------------------*/
#define cAf6_RxM12E12_per_chn_intr_stat_DE2LOFIntr_Bit_Start                                                 0
#define cAf6_RxM12E12_per_chn_intr_stat_DE2LOFIntr_Bit_End                                                   6
#define cAf6_RxM12E12_per_chn_intr_stat_DE2LOFIntr_Mask                                                cBit6_0
#define cAf6_RxM12E12_per_chn_intr_stat_DE2LOFIntr_Shift                                                     0
#define cAf6_RxM12E12_per_chn_intr_stat_DE2LOFIntr_MaxVal                                                 0x7f
#define cAf6_RxM12E12_per_chn_intr_stat_DE2LOFIntr_MinVal                                                  0x0
#define cAf6_RxM12E12_per_chn_intr_stat_DE2LOFIntr_RstVal                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : RxM12E12 Framer per Channel Current Status
Reg Addr   : 0x000413C0-0x000413DF
Reg Formula: 0x000413C0 +  de3id
    Where  : 
           + $de3id(0-23):
Reg Desc   : 
This is the per Channel Current status of DS2/E2 Rx Framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_RxM12E12_per_chn_curr_stat_Base                                                     0x000413C0
#define cAf6Reg_RxM12E12_per_chn_curr_stat(de3id)                                         (0x000413C0+(de3id))
#define cAf6Reg_RxM12E12_per_chn_curr_stat_WidthVal                                                         32
#define cAf6Reg_RxM12E12_per_chn_curr_stat_WriteMask                                                       0x0

/*--------------------------------------
BitField Name: DE2LOFCurrSta
BitField Type: RW
BitField Desc: Each bit correspond with each channel DS2/E2 in DS3/E3 0: No
Interrupt 1: Have Interrupt
BitField Bits: [6:0]
--------------------------------------*/
#define cAf6_RxM12E12_per_chn_curr_stat_DE2LOFCurrSta_Bit_Start                                              0
#define cAf6_RxM12E12_per_chn_curr_stat_DE2LOFCurrSta_Bit_End                                                6
#define cAf6_RxM12E12_per_chn_curr_stat_DE2LOFCurrSta_Mask                                             cBit6_0
#define cAf6_RxM12E12_per_chn_curr_stat_DE2LOFCurrSta_Shift                                                  0
#define cAf6_RxM12E12_per_chn_curr_stat_DE2LOFCurrSta_MaxVal                                              0x7f
#define cAf6_RxM12E12_per_chn_curr_stat_DE2LOFCurrSta_MinVal                                               0x0
#define cAf6_RxM12E12_per_chn_curr_stat_DE2LOFCurrSta_RstVal                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer FBE Threshold Control
Reg Addr   : 0x00050003 - 0x00050003
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the FBE threshold configuration register for the PDH DS1/E1/J1 Rx framer

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_fbe_thrsh_cfg_Base                                                             0x00050003
#define cAf6Reg_dej1_fbe_thrsh_cfg                                                                  0x00050003
#define cAf6Reg_dej1_fbe_thrsh_cfg_WidthVal                                                                 32
#define cAf6Reg_dej1_fbe_thrsh_cfg_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: RxDE1FbeThres
BitField Type: RW
BitField Desc: Threshold to generate interrupt if the FBE counter exceed this
threshold
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_dej1_fbe_thrsh_cfg_RxDE1FbeThres_Bit_Start                                                      0
#define cAf6_dej1_fbe_thrsh_cfg_RxDE1FbeThres_Bit_End                                                        7
#define cAf6_dej1_fbe_thrsh_cfg_RxDE1FbeThres_Mask                                                     cBit7_0
#define cAf6_dej1_fbe_thrsh_cfg_RxDE1FbeThres_Shift                                                          0
#define cAf6_dej1_fbe_thrsh_cfg_RxDE1FbeThres_MaxVal                                                      0xff
#define cAf6_dej1_fbe_thrsh_cfg_RxDE1FbeThres_MinVal                                                       0x0
#define cAf6_dej1_fbe_thrsh_cfg_RxDE1FbeThres_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer CRC Threshold Control
Reg Addr   : 0x00050004 - 0x00050004
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the CRC threshold configuration register for the PDH DS1/E1/J1 Rx framer

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rx_crc_thrsh_cfg_Base                                                          0x00050004
#define cAf6Reg_dej1_rx_crc_thrsh_cfg                                                               0x00050004
#define cAf6Reg_dej1_rx_crc_thrsh_cfg_WidthVal                                                              32
#define cAf6Reg_dej1_rx_crc_thrsh_cfg_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: RxDE1CrcThres
BitField Type: RW
BitField Desc: Threshold to generate interrupt if the CRC counter exceed this
threshold
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_dej1_rx_crc_thrsh_cfg_RxDE1CrcThres_Bit_Start                                                   0
#define cAf6_dej1_rx_crc_thrsh_cfg_RxDE1CrcThres_Bit_End                                                     7
#define cAf6_dej1_rx_crc_thrsh_cfg_RxDE1CrcThres_Mask                                                  cBit7_0
#define cAf6_dej1_rx_crc_thrsh_cfg_RxDE1CrcThres_Shift                                                       0
#define cAf6_dej1_rx_crc_thrsh_cfg_RxDE1CrcThres_MaxVal                                                   0xff
#define cAf6_dej1_rx_crc_thrsh_cfg_RxDE1CrcThres_MinVal                                                    0x0
#define cAf6_dej1_rx_crc_thrsh_cfg_RxDE1CrcThres_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer REI Threshold Control
Reg Addr   : 0x00050005 - 0x00050005
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the REI threshold configuration register for the PDH DS1/E1/J1 Rx framer

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rei_thrsh_cfg_Base                                                             0x00050005
#define cAf6Reg_dej1_rei_thrsh_cfg                                                                  0x00050005
#define cAf6Reg_dej1_rei_thrsh_cfg_WidthVal                                                                 32
#define cAf6Reg_dej1_rei_thrsh_cfg_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: RxDE1ReiThres
BitField Type: RW
BitField Desc: Threshold to generate interrupt if the FBE counter exceed this
threshold
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_dej1_rei_thrsh_cfg_RxDE1ReiThres_Bit_Start                                                      0
#define cAf6_dej1_rei_thrsh_cfg_RxDE1ReiThres_Bit_End                                                        7
#define cAf6_dej1_rei_thrsh_cfg_RxDE1ReiThres_Mask                                                     cBit7_0
#define cAf6_dej1_rei_thrsh_cfg_RxDE1ReiThres_Shift                                                          0
#define cAf6_dej1_rei_thrsh_cfg_RxDE1ReiThres_MaxVal                                                      0xff
#define cAf6_dej1_rei_thrsh_cfg_RxDE1ReiThres_MinVal                                                       0x0
#define cAf6_dej1_rei_thrsh_cfg_RxDE1ReiThres_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer Control
Reg Addr   : 0x00054000 - 0x000543FF
Reg Formula: 0x00054000 +  32*de3id + 4*de2id + de1id
    Where  : 
           + $de3id(0-23)
           + $de2id(0-6):
           + $de1id(0-3):
Reg Desc   : 
DS1/E1/J1 Rx framer control is used to configure for Frame mode (DS1, E1, J1) at the DS1/E1 framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rx_framer_ctrl_Base                                                            0x00054000
#define cAf6Reg_dej1_rx_framer_ctrl(de3id, de2id, de1id)              (0x00054000+32*(de3id)+4*(de2id)+(de1id))
#define cAf6Reg_dej1_rx_framer_ctrl_WidthVal                                                                32
#define cAf6Reg_dej1_rx_framer_ctrl_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: RxDE1FrcAISLineDwn
BitField Type: RW
BitField Desc: Set 1 to force AIS Line DownStream
BitField Bits: [29]
--------------------------------------*/
#define cAf6_dej1_rx_framer_ctrl_RxDE1FrcAISLineDwn_Bit_Start                                               29
#define cAf6_dej1_rx_framer_ctrl_RxDE1FrcAISLineDwn_Bit_End                                                 29
#define cAf6_dej1_rx_framer_ctrl_RxDE1FrcAISLineDwn_Mask                                                cBit29
#define cAf6_dej1_rx_framer_ctrl_RxDE1FrcAISLineDwn_Shift                                                   29
#define cAf6_dej1_rx_framer_ctrl_RxDE1FrcAISLineDwn_MaxVal                                                 0x1
#define cAf6_dej1_rx_framer_ctrl_RxDE1FrcAISLineDwn_MinVal                                                 0x0
#define cAf6_dej1_rx_framer_ctrl_RxDE1FrcAISLineDwn_RstVal                                                 0x0

/*--------------------------------------
BitField Name: RxDE1FrcAISPldDwn
BitField Type: RW
BitField Desc: Set 1 to force AIS payload DownStream
BitField Bits: [28]
--------------------------------------*/
#define cAf6_dej1_rx_framer_ctrl_RxDE1FrcAISPldDwn_Bit_Start                                                28
#define cAf6_dej1_rx_framer_ctrl_RxDE1FrcAISPldDwn_Bit_End                                                  28
#define cAf6_dej1_rx_framer_ctrl_RxDE1FrcAISPldDwn_Mask                                                 cBit28
#define cAf6_dej1_rx_framer_ctrl_RxDE1FrcAISPldDwn_Shift                                                    28
#define cAf6_dej1_rx_framer_ctrl_RxDE1FrcAISPldDwn_MaxVal                                                  0x1
#define cAf6_dej1_rx_framer_ctrl_RxDE1FrcAISPldDwn_MinVal                                                  0x0
#define cAf6_dej1_rx_framer_ctrl_RxDE1FrcAISPldDwn_RstVal                                                  0x0

/*--------------------------------------
BitField Name: RxDE1FrcLos
BitField Type: RW
BitField Desc: Set 1 to force LOS
BitField Bits: [27]
--------------------------------------*/
#define cAf6_dej1_rx_framer_ctrl_RxDE1FrcLos_Bit_Start                                                      27
#define cAf6_dej1_rx_framer_ctrl_RxDE1FrcLos_Bit_End                                                        27
#define cAf6_dej1_rx_framer_ctrl_RxDE1FrcLos_Mask                                                       cBit27
#define cAf6_dej1_rx_framer_ctrl_RxDE1FrcLos_Shift                                                          27
#define cAf6_dej1_rx_framer_ctrl_RxDE1FrcLos_MaxVal                                                        0x1
#define cAf6_dej1_rx_framer_ctrl_RxDE1FrcLos_MinVal                                                        0x0
#define cAf6_dej1_rx_framer_ctrl_RxDE1FrcLos_RstVal                                                        0x0

/*--------------------------------------
BitField Name: RxDE1MFASChken
BitField Type: RW
BitField Desc: Set 1 to enable check MFAS to go INFRAME in case of E1CRC mode.
Default is 0
BitField Bits: [26]
--------------------------------------*/
#define cAf6_dej1_rx_framer_ctrl_RxDE1MFASChken_Bit_Start                                                   26
#define cAf6_dej1_rx_framer_ctrl_RxDE1MFASChken_Bit_End                                                     26
#define cAf6_dej1_rx_framer_ctrl_RxDE1MFASChken_Mask                                                    cBit26
#define cAf6_dej1_rx_framer_ctrl_RxDE1MFASChken_Shift                                                       26
#define cAf6_dej1_rx_framer_ctrl_RxDE1MFASChken_MaxVal                                                     0x1
#define cAf6_dej1_rx_framer_ctrl_RxDE1MFASChken_MinVal                                                     0x0
#define cAf6_dej1_rx_framer_ctrl_RxDE1MFASChken_RstVal                                                     0x0

/*--------------------------------------
BitField Name: RxDE1LosCntThres
BitField Type: RW
BitField Desc: Threshold of LOS monitoring block. This function always monitors
the number of bit 1 of the incoming signal in each of two consecutive frame
periods (256*2 bits in E1, 193*3 bits in DS1). If it is less than the desire
number, LOS will be set then.
BitField Bits: [25:23]
--------------------------------------*/
#define cAf6_dej1_rx_framer_ctrl_RxDE1LosCntThres_Bit_Start                                                 23
#define cAf6_dej1_rx_framer_ctrl_RxDE1LosCntThres_Bit_End                                                   25
#define cAf6_dej1_rx_framer_ctrl_RxDE1LosCntThres_Mask                                               cBit25_23
#define cAf6_dej1_rx_framer_ctrl_RxDE1LosCntThres_Shift                                                     23
#define cAf6_dej1_rx_framer_ctrl_RxDE1LosCntThres_MaxVal                                                   0x7
#define cAf6_dej1_rx_framer_ctrl_RxDE1LosCntThres_MinVal                                                   0x0
#define cAf6_dej1_rx_framer_ctrl_RxDE1LosCntThres_RstVal                                                   0x0

/*--------------------------------------
BitField Name: RxDE1MonOnly
BitField Type: RW
BitField Desc: Set 1 to enable the Monitor Only mode at the RXDS1/E1 framer
BitField Bits: [22]
--------------------------------------*/
#define cAf6_dej1_rx_framer_ctrl_RxDE1MonOnly_Bit_Start                                                     22
#define cAf6_dej1_rx_framer_ctrl_RxDE1MonOnly_Bit_End                                                       22
#define cAf6_dej1_rx_framer_ctrl_RxDE1MonOnly_Mask                                                      cBit22
#define cAf6_dej1_rx_framer_ctrl_RxDE1MonOnly_Shift                                                         22
#define cAf6_dej1_rx_framer_ctrl_RxDE1MonOnly_MaxVal                                                       0x1
#define cAf6_dej1_rx_framer_ctrl_RxDE1MonOnly_MinVal                                                       0x0
#define cAf6_dej1_rx_framer_ctrl_RxDE1MonOnly_RstVal                                                       0x0

/*--------------------------------------
BitField Name: RxDE1LocalLineLoop
BitField Type: RW
BitField Desc: Set 1 to enable Local Line loop back
BitField Bits: [21]
--------------------------------------*/
#define cAf6_dej1_rx_framer_ctrl_RxDE1LocalLineLoop_Bit_Start                                               21
#define cAf6_dej1_rx_framer_ctrl_RxDE1LocalLineLoop_Bit_End                                                 21
#define cAf6_dej1_rx_framer_ctrl_RxDE1LocalLineLoop_Mask                                                cBit21
#define cAf6_dej1_rx_framer_ctrl_RxDE1LocalLineLoop_Shift                                                   21
#define cAf6_dej1_rx_framer_ctrl_RxDE1LocalLineLoop_MaxVal                                                 0x1
#define cAf6_dej1_rx_framer_ctrl_RxDE1LocalLineLoop_MinVal                                                 0x0
#define cAf6_dej1_rx_framer_ctrl_RxDE1LocalLineLoop_RstVal                                                 0x0

/*--------------------------------------
BitField Name: RxDe1LocalPayLoop
BitField Type: RW
BitField Desc: Set 1 to enable Local Payload loop back. Sharing for VT Loopback
in case of CEP path
BitField Bits: [20]
--------------------------------------*/
#define cAf6_dej1_rx_framer_ctrl_RxDe1LocalPayLoop_Bit_Start                                                20
#define cAf6_dej1_rx_framer_ctrl_RxDe1LocalPayLoop_Bit_End                                                  20
#define cAf6_dej1_rx_framer_ctrl_RxDe1LocalPayLoop_Mask                                                 cBit20
#define cAf6_dej1_rx_framer_ctrl_RxDe1LocalPayLoop_Shift                                                    20
#define cAf6_dej1_rx_framer_ctrl_RxDe1LocalPayLoop_MaxVal                                                  0x1
#define cAf6_dej1_rx_framer_ctrl_RxDe1LocalPayLoop_MinVal                                                  0x0
#define cAf6_dej1_rx_framer_ctrl_RxDe1LocalPayLoop_RstVal                                                  0x0

/*--------------------------------------
BitField Name: RxDE1E1SaCfg
BitField Type: RW
BitField Desc: E1 Sa used for FDL configuration 0x0: No Sa bit used 0x1: Sa4 is
used for FDL 0x2: Sa4-Sa5 is used for FDL 0x3: Sa4-Sa6 is used for FDL 0x4:
Sa4-Sa7 is used for FDL 0x5: Sa4-Sa8 is used for FDL
BitField Bits: [19:17]
--------------------------------------*/
#define cAf6_dej1_rx_framer_ctrl_RxDE1E1SaCfg_Bit_Start                                                     17
#define cAf6_dej1_rx_framer_ctrl_RxDE1E1SaCfg_Bit_End                                                       19
#define cAf6_dej1_rx_framer_ctrl_RxDE1E1SaCfg_Mask                                                   cBit19_17
#define cAf6_dej1_rx_framer_ctrl_RxDE1E1SaCfg_Shift                                                         17
#define cAf6_dej1_rx_framer_ctrl_RxDE1E1SaCfg_MaxVal                                                       0x7
#define cAf6_dej1_rx_framer_ctrl_RxDE1E1SaCfg_MinVal                                                       0x0
#define cAf6_dej1_rx_framer_ctrl_RxDE1E1SaCfg_RstVal                                                       0x0

/*--------------------------------------
BitField Name: RxDE1LofThres
BitField Type: RW
BitField Desc: Threshold for FAS error to declare LOF in DS1/E1/J1 mode
BitField Bits: [16:13]
--------------------------------------*/
#define cAf6_dej1_rx_framer_ctrl_RxDE1LofThres_Bit_Start                                                    13
#define cAf6_dej1_rx_framer_ctrl_RxDE1LofThres_Bit_End                                                      16
#define cAf6_dej1_rx_framer_ctrl_RxDE1LofThres_Mask                                                  cBit16_13
#define cAf6_dej1_rx_framer_ctrl_RxDE1LofThres_Shift                                                        13
#define cAf6_dej1_rx_framer_ctrl_RxDE1LofThres_MaxVal                                                      0xf
#define cAf6_dej1_rx_framer_ctrl_RxDE1LofThres_MinVal                                                      0x0
#define cAf6_dej1_rx_framer_ctrl_RxDE1LofThres_RstVal                                                      0x0

/*--------------------------------------
BitField Name: RxDE1AisCntThres
BitField Type: RW
BitField Desc: Threshold of AIS monitoring block. This function always monitors
the number of bit 0 of the incoming signal in each of two consecutive frame
periods (256*2 bits in E1, 193*3 bits in DS1). If it is less than the desire
number, AIS will be set then.
BitField Bits: [12:10]
--------------------------------------*/
#define cAf6_dej1_rx_framer_ctrl_RxDE1AisCntThres_Bit_Start                                                 10
#define cAf6_dej1_rx_framer_ctrl_RxDE1AisCntThres_Bit_End                                                   12
#define cAf6_dej1_rx_framer_ctrl_RxDE1AisCntThres_Mask                                               cBit12_10
#define cAf6_dej1_rx_framer_ctrl_RxDE1AisCntThres_Shift                                                     10
#define cAf6_dej1_rx_framer_ctrl_RxDE1AisCntThres_MaxVal                                                   0x7
#define cAf6_dej1_rx_framer_ctrl_RxDE1AisCntThres_MinVal                                                   0x0
#define cAf6_dej1_rx_framer_ctrl_RxDE1AisCntThres_RstVal                                                   0x0

/*--------------------------------------
BitField Name: RxDE1RaiCntThres
BitField Type: RW
BitField Desc: Threshold of RAI monitoring block
BitField Bits: [9:7]
--------------------------------------*/
#define cAf6_dej1_rx_framer_ctrl_RxDE1RaiCntThres_Bit_Start                                                  7
#define cAf6_dej1_rx_framer_ctrl_RxDE1RaiCntThres_Bit_End                                                    9
#define cAf6_dej1_rx_framer_ctrl_RxDE1RaiCntThres_Mask                                                 cBit9_7
#define cAf6_dej1_rx_framer_ctrl_RxDE1RaiCntThres_Shift                                                      7
#define cAf6_dej1_rx_framer_ctrl_RxDE1RaiCntThres_MaxVal                                                   0x7
#define cAf6_dej1_rx_framer_ctrl_RxDE1RaiCntThres_MinVal                                                   0x0
#define cAf6_dej1_rx_framer_ctrl_RxDE1RaiCntThres_RstVal                                                   0x0

/*--------------------------------------
BitField Name: RxDE1AisMaskEn
BitField Type: RW
BitField Desc: Set 1 to enable output data of the DS1/E1/J1 framer to be set to
all one (1) during LOF
BitField Bits: [6]
--------------------------------------*/
#define cAf6_dej1_rx_framer_ctrl_RxDE1AisMaskEn_Bit_Start                                                    6
#define cAf6_dej1_rx_framer_ctrl_RxDE1AisMaskEn_Bit_End                                                      6
#define cAf6_dej1_rx_framer_ctrl_RxDE1AisMaskEn_Mask                                                     cBit6
#define cAf6_dej1_rx_framer_ctrl_RxDE1AisMaskEn_Shift                                                        6
#define cAf6_dej1_rx_framer_ctrl_RxDE1AisMaskEn_MaxVal                                                     0x1
#define cAf6_dej1_rx_framer_ctrl_RxDE1AisMaskEn_MinVal                                                     0x0
#define cAf6_dej1_rx_framer_ctrl_RxDE1AisMaskEn_RstVal                                                     0x0

/*--------------------------------------
BitField Name: RxDE1En
BitField Type: RW
BitField Desc: Set 1 to enable the DS1/E1/J1 framer.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_dej1_rx_framer_ctrl_RxDE1En_Bit_Start                                                           5
#define cAf6_dej1_rx_framer_ctrl_RxDE1En_Bit_End                                                             5
#define cAf6_dej1_rx_framer_ctrl_RxDE1En_Mask                                                            cBit5
#define cAf6_dej1_rx_framer_ctrl_RxDE1En_Shift                                                               5
#define cAf6_dej1_rx_framer_ctrl_RxDE1En_MaxVal                                                            0x1
#define cAf6_dej1_rx_framer_ctrl_RxDE1En_MinVal                                                            0x0
#define cAf6_dej1_rx_framer_ctrl_RxDE1En_RstVal                                                            0x0

/*--------------------------------------
BitField Name: RxDE1FrcLof
BitField Type: RW
BitField Desc: Set 1 to force Re-frame
BitField Bits: [4]
--------------------------------------*/
#define cAf6_dej1_rx_framer_ctrl_RxDE1FrcLof_Bit_Start                                                       4
#define cAf6_dej1_rx_framer_ctrl_RxDE1FrcLof_Bit_End                                                         4
#define cAf6_dej1_rx_framer_ctrl_RxDE1FrcLof_Mask                                                        cBit4
#define cAf6_dej1_rx_framer_ctrl_RxDE1FrcLof_Shift                                                           4
#define cAf6_dej1_rx_framer_ctrl_RxDE1FrcLof_MaxVal                                                        0x1
#define cAf6_dej1_rx_framer_ctrl_RxDE1FrcLof_MinVal                                                        0x0
#define cAf6_dej1_rx_framer_ctrl_RxDE1FrcLof_RstVal                                                        0x0

/*--------------------------------------
BitField Name: RxDs1Md
BitField Type: RW
BitField Desc: Receive DS1/J1 framing mode 0000: DS1/J1 Unframe 0001: DS1 SF
(D4) 0010: DS1 ESF 0011: DS1 DDS 0100: DS1 SLC 0101: J1 SF 0110: J1 ESF 1000: E1
Unframe 1001: E1 Basic Frame 1010: E1 CRC4 Frame
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_dej1_rx_framer_ctrl_RxDs1Md_Bit_Start                                                           0
#define cAf6_dej1_rx_framer_ctrl_RxDs1Md_Bit_End                                                             3
#define cAf6_dej1_rx_framer_ctrl_RxDs1Md_Mask                                                          cBit3_0
#define cAf6_dej1_rx_framer_ctrl_RxDs1Md_Shift                                                               0
#define cAf6_dej1_rx_framer_ctrl_RxDs1Md_MaxVal                                                            0xf
#define cAf6_dej1_rx_framer_ctrl_RxDs1Md_MinVal                                                            0x0
#define cAf6_dej1_rx_framer_ctrl_RxDs1Md_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer HW Status
Reg Addr   : 0x00054400-0x000547FF
Reg Formula: 0x00054400 +  32*de3id + 4*de2id + de1id
    Where  : 
           + $de3id(0-23)
           + $de2id(0-6):
           + $de1id(0-3):
Reg Desc   : 
These registers are used for Hardware tus only

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rx_framer_hw_stat_Base                                                         0x00054400
#define cAf6Reg_dej1_rx_framer_hw_stat(de3id, de2id, de1id)           (0x00054400+32*(de3id)+4*(de2id)+(de1id))
#define cAf6Reg_dej1_rx_framer_hw_stat_WidthVal                                                             32
#define cAf6Reg_dej1_rx_framer_hw_stat_WriteMask                                                           0x0

/*--------------------------------------
BitField Name: RxDE1Sta
BitField Type: RW
BitField Desc: RxDE1Sta 000: Search 001: Shift 010: Wait 011: Verify 100: Fs
search 101: Inframe
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_dej1_rx_framer_hw_stat_RxDE1Sta_Bit_Start                                                       0
#define cAf6_dej1_rx_framer_hw_stat_RxDE1Sta_Bit_End                                                         2
#define cAf6_dej1_rx_framer_hw_stat_RxDE1Sta_Mask                                                      cBit2_0
#define cAf6_dej1_rx_framer_hw_stat_RxDE1Sta_Shift                                                           0
#define cAf6_dej1_rx_framer_hw_stat_RxDE1Sta_MaxVal                                                        0x7
#define cAf6_dej1_rx_framer_hw_stat_RxDE1Sta_MinVal                                                        0x0
#define cAf6_dej1_rx_framer_hw_stat_RxDE1Sta_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer CRC Error Counter
Reg Addr   : 0x00056800 - 0x00056FFF
Reg Formula: 0x00056800 +  1024*Rd2Clr + 32*de3id + 4*de2id + de1id
    Where  : 
           + $Rd2Clr(0-1): Rd2Clr = 0: Read to clear. Otherwise, Read Only
           + $de3id(0-23):
           + $de2id(0-6):
           + $de1id(0-3):
Reg Desc   : 
This is the per channel CRC error counter for DS1/E1/J1 receive framer

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rx_framer_crcerr_cnt_Base                                                      0x00056800
#define cAf6Reg_dej1_rx_framer_crcerr_cnt(Rd2Clr, de3id, de2id, de1id)(0x00056800+1024*(Rd2Clr)+32*(de3id)+4*(de2id)+(de1id))
#define cAf6Reg_dej1_rx_framer_crcerr_cnt_WidthVal                                                          32
#define cAf6Reg_dej1_rx_framer_crcerr_cnt_WriteMask                                                        0x0

/*--------------------------------------
BitField Name: DE1CrcErrCnt
BitField Type: RW
BitField Desc: DS1/E1/J1 CRC Error Accumulator
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_dej1_rx_framer_crcerr_cnt_DE1CrcErrCnt_Bit_Start                                                0
#define cAf6_dej1_rx_framer_crcerr_cnt_DE1CrcErrCnt_Bit_End                                                  7
#define cAf6_dej1_rx_framer_crcerr_cnt_DE1CrcErrCnt_Mask                                               cBit7_0
#define cAf6_dej1_rx_framer_crcerr_cnt_DE1CrcErrCnt_Shift                                                    0
#define cAf6_dej1_rx_framer_crcerr_cnt_DE1CrcErrCnt_MaxVal                                                0xff
#define cAf6_dej1_rx_framer_crcerr_cnt_DE1CrcErrCnt_MinVal                                                 0x0
#define cAf6_dej1_rx_framer_crcerr_cnt_DE1CrcErrCnt_RstVal                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer REI Counter
Reg Addr   : 0x00057000 - 0x000577FF
Reg Formula: 0x00057000 +  1024*Rd2Clr + 32*de3id + 4*de2id + de1id
    Where  : 
           + $Rd2Clr(0-1): Rd2Clr = 0: Read to clear. Otherwise, Read Only
           + $de3id(0-23):
           + $de2id(0-6):
           + $de1id(0-3):
Reg Desc   : 
This is the per channel REI counter for DS1/E1/J1 receive framer

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rx_framer_rei_cnt_Base                                                         0x00057000
#define cAf6Reg_dej1_rx_framer_rei_cnt(Rd2Clr, de3id, de2id, de1id)   (0x00057000+1024*(Rd2Clr)+32*(de3id)+4*(de2id)+(de1id))
#define cAf6Reg_dej1_rx_framer_rei_cnt_WidthVal                                                             32
#define cAf6Reg_dej1_rx_framer_rei_cnt_WriteMask                                                           0x0

/*--------------------------------------
BitField Name: DE1ReiCnt
BitField Type: RW
BitField Desc: DS1/E1/J1 REI error accumulator
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_dej1_rx_framer_rei_cnt_DE1ReiCnt_Bit_Start                                                      0
#define cAf6_dej1_rx_framer_rei_cnt_DE1ReiCnt_Bit_End                                                        7
#define cAf6_dej1_rx_framer_rei_cnt_DE1ReiCnt_Mask                                                     cBit7_0
#define cAf6_dej1_rx_framer_rei_cnt_DE1ReiCnt_Shift                                                          0
#define cAf6_dej1_rx_framer_rei_cnt_DE1ReiCnt_MaxVal                                                      0xff
#define cAf6_dej1_rx_framer_rei_cnt_DE1ReiCnt_MinVal                                                       0x0
#define cAf6_dej1_rx_framer_rei_cnt_DE1ReiCnt_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer FBE Counter
Reg Addr   : 0x00056000-0x000567FF
Reg Formula: 0x00056000 +  1024*Rd2Clr + 32*de3id + 4*de2id + de1id
    Where  : 
           + $Rd2Clr(0-1): Rd2Clr = 0: Read to clear. Otherwise, Read Only
           + $de3id(0-23):
           + $de2id(0-6):
           + $de1id(0-3):
Reg Desc   : 
This is the per channel REI counter for DS1/E1/J1 receive framer

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rx_framer_fbe_cnt_Base                                                         0x00056000
#define cAf6Reg_dej1_rx_framer_fbe_cnt(Rd2Clr, de3id, de2id, de1id)   (0x00056000+1024*(Rd2Clr)+32*(de3id)+4*(de2id)+(de1id))
#define cAf6Reg_dej1_rx_framer_fbe_cnt_WidthVal                                                             32
#define cAf6Reg_dej1_rx_framer_fbe_cnt_WriteMask                                                           0x0

/*--------------------------------------
BitField Name: DE1FbeCnt
BitField Type: RW
BitField Desc: DS1/E1/J1 F-bit error accumulator
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_dej1_rx_framer_fbe_cnt_DE1FbeCnt_Bit_Start                                                      0
#define cAf6_dej1_rx_framer_fbe_cnt_DE1FbeCnt_Bit_End                                                        7
#define cAf6_dej1_rx_framer_fbe_cnt_DE1FbeCnt_Mask                                                     cBit7_0
#define cAf6_dej1_rx_framer_fbe_cnt_DE1FbeCnt_Shift                                                          0
#define cAf6_dej1_rx_framer_fbe_cnt_DE1FbeCnt_MaxVal                                                      0xff
#define cAf6_dej1_rx_framer_fbe_cnt_DE1FbeCnt_MinVal                                                       0x0
#define cAf6_dej1_rx_framer_fbe_cnt_DE1FbeCnt_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer per Channel Interrupt Enable Control
Reg Addr   : 0x00055000-0x000553FF
Reg Formula: 0x00055000 +  StsID*32 + Tug2ID*4 + VtnID
    Where  : 
           + $StsID(0-23): STS-1/VC-3 ID
           + $Tug2ID(0-6): TUG-2/VTG ID
           + $VtnID(0-3): VT/TU number ID in the TUG-2/VTG
Reg Desc   : 
This is the per Channel interrupt enable of DS1/E1/J1 Rx Framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rx_framer_per_chn_intr_en_ctrl_Base                                            0x00055000
#define cAf6Reg_dej1_rx_framer_per_chn_intr_en_ctrl(StsID, Tug2ID, VtnID)(0x00055000+(StsID)*32+(Tug2ID)*4+(VtnID))
#define cAf6Reg_dej1_rx_framer_per_chn_intr_en_ctrl_WidthVal                                                32
#define cAf6Reg_dej1_rx_framer_per_chn_intr_en_ctrl_WriteMask                                              0x0

/*--------------------------------------
BitField Name: DE1BerSdIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable the BER-SD interrupt.
BitField Bits: [13]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1BerSdIntrEn_Bit_Start                                      13
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1BerSdIntrEn_Bit_End                                      13
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1BerSdIntrEn_Mask                                    cBit13
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1BerSdIntrEn_Shift                                       13
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1BerSdIntrEn_MaxVal                                     0x1
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1BerSdIntrEn_MinVal                                     0x0
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1BerSdIntrEn_RstVal                                     0x0

/*--------------------------------------
BitField Name: DE1BerSfIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable the BER-SF interrupt.
BitField Bits: [12]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1BerSfIntrEn_Bit_Start                                      12
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1BerSfIntrEn_Bit_End                                      12
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1BerSfIntrEn_Mask                                    cBit12
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1BerSfIntrEn_Shift                                       12
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1BerSfIntrEn_MaxVal                                     0x1
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1BerSfIntrEn_MinVal                                     0x0
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1BerSfIntrEn_RstVal                                     0x0

/*--------------------------------------
BitField Name: DE1oblcIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable the new Outband LoopCode detected to generate an
interrupt.
BitField Bits: [11]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1oblcIntrEn_Bit_Start                                      11
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1oblcIntrEn_Bit_End                                      11
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1oblcIntrEn_Mask                                     cBit11
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1oblcIntrEn_Shift                                        11
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1oblcIntrEn_MaxVal                                      0x1
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1oblcIntrEn_MinVal                                      0x0
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1oblcIntrEn_RstVal                                      0x0

/*--------------------------------------
BitField Name: DE1iblcIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable the new Inband LoopCode detected to generate an
interrupt.
BitField Bits: [10]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1iblcIntrEn_Bit_Start                                      10
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1iblcIntrEn_Bit_End                                      10
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1iblcIntrEn_Mask                                     cBit10
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1iblcIntrEn_Shift                                        10
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1iblcIntrEn_MaxVal                                      0x1
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1iblcIntrEn_MinVal                                      0x0
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1iblcIntrEn_RstVal                                      0x0

/*--------------------------------------
BitField Name: DE1SigRAIIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable Signalling RAI event to generate an interrupt.
BitField Bits: [9]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1SigRAIIntrEn_Bit_Start                                       9
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1SigRAIIntrEn_Bit_End                                       9
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1SigRAIIntrEn_Mask                                    cBit9
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1SigRAIIntrEn_Shift                                       9
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1SigRAIIntrEn_MaxVal                                     0x1
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1SigRAIIntrEn_MinVal                                     0x0
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1SigRAIIntrEn_RstVal                                     0x0

/*--------------------------------------
BitField Name: DE1SigLOFIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable Signalling LOF event to generate an interrupt.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1SigLOFIntrEn_Bit_Start                                       8
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1SigLOFIntrEn_Bit_End                                       8
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1SigLOFIntrEn_Mask                                    cBit8
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1SigLOFIntrEn_Shift                                       8
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1SigLOFIntrEn_MaxVal                                     0x1
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1SigLOFIntrEn_MinVal                                     0x0
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1SigLOFIntrEn_RstVal                                     0x0

/*--------------------------------------
BitField Name: DE1FbeIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change FBE te event to generate an interrupt.
BitField Bits: [7]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1FbeIntrEn_Bit_Start                                       7
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1FbeIntrEn_Bit_End                                        7
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1FbeIntrEn_Mask                                       cBit7
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1FbeIntrEn_Shift                                          7
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1FbeIntrEn_MaxVal                                       0x1
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1FbeIntrEn_MinVal                                       0x0
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1FbeIntrEn_RstVal                                       0x0

/*--------------------------------------
BitField Name: DE1CrcIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change CRC te event to generate an interrupt.
BitField Bits: [6]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1CrcIntrEn_Bit_Start                                       6
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1CrcIntrEn_Bit_End                                        6
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1CrcIntrEn_Mask                                       cBit6
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1CrcIntrEn_Shift                                          6
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1CrcIntrEn_MaxVal                                       0x1
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1CrcIntrEn_MinVal                                       0x0
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1CrcIntrEn_RstVal                                       0x0

/*--------------------------------------
BitField Name: DE1ReiIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change REI te event to generate an interrupt.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1ReiIntrEn_Bit_Start                                       5
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1ReiIntrEn_Bit_End                                        5
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1ReiIntrEn_Mask                                       cBit5
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1ReiIntrEn_Shift                                          5
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1ReiIntrEn_MaxVal                                       0x1
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1ReiIntrEn_MinVal                                       0x0
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1ReiIntrEn_RstVal                                       0x0

/*--------------------------------------
BitField Name: DE1LomfIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change LOMF te event to generate an interrupt.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LomfIntrEn_Bit_Start                                       4
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LomfIntrEn_Bit_End                                       4
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LomfIntrEn_Mask                                      cBit4
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LomfIntrEn_Shift                                         4
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LomfIntrEn_MaxVal                                      0x1
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LomfIntrEn_MinVal                                      0x0
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LomfIntrEn_RstVal                                      0x0

/*--------------------------------------
BitField Name: DE1LosIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change LOS te event to generate an interrupt.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LosIntrEn_Bit_Start                                       3
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LosIntrEn_Bit_End                                        3
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LosIntrEn_Mask                                       cBit3
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LosIntrEn_Shift                                          3
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LosIntrEn_MaxVal                                       0x1
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LosIntrEn_MinVal                                       0x0
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LosIntrEn_RstVal                                       0x0

/*--------------------------------------
BitField Name: DE1AisIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change AIS te event to generate an interrupt.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1AisIntrEn_Bit_Start                                       2
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1AisIntrEn_Bit_End                                        2
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1AisIntrEn_Mask                                       cBit2
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1AisIntrEn_Shift                                          2
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1AisIntrEn_MaxVal                                       0x1
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1AisIntrEn_MinVal                                       0x0
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1AisIntrEn_RstVal                                       0x0

/*--------------------------------------
BitField Name: DE1RaiIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change RAI te event to generate an interrupt.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1RaiIntrEn_Bit_Start                                       1
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1RaiIntrEn_Bit_End                                        1
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1RaiIntrEn_Mask                                       cBit1
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1RaiIntrEn_Shift                                          1
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1RaiIntrEn_MaxVal                                       0x1
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1RaiIntrEn_MinVal                                       0x0
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1RaiIntrEn_RstVal                                       0x0

/*--------------------------------------
BitField Name: DE1LofIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change LOF te event to generate an interrupt.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LofIntrEn_Bit_Start                                       0
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LofIntrEn_Bit_End                                        0
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LofIntrEn_Mask                                       cBit0
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LofIntrEn_Shift                                          0
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LofIntrEn_MaxVal                                       0x1
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LofIntrEn_MinVal                                       0x0
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LofIntrEn_RstVal                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer per Channel Interrupt Status
Reg Addr   : 0x00055400-0x000557FF
Reg Formula: 0x00055400 +  StsID*32 + Tug2ID*4 + VtnID
    Where  : 
           + $StsID(0-23): STS-1/VC-3 ID
           + $Tug2ID(0-6): TUG-2/VTG ID
           + $VtnID(0-3): VT/TU number ID in the TUG-2/VTG
Reg Desc   : 
This is the per Channel interrupt tus of DS1/E1/J1 Rx Framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rx_framer_per_chn_intr_stat_Base                                               0x00055400
#define cAf6Reg_dej1_rx_framer_per_chn_intr_stat(StsID, Tug2ID, VtnID)(0x00055400+(StsID)*32+(Tug2ID)*4+(VtnID))
#define cAf6Reg_dej1_rx_framer_per_chn_intr_stat_WidthVal                                                   32
#define cAf6Reg_dej1_rx_framer_per_chn_intr_stat_WriteMask                                                 0x0

#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1BerTcaIntr_Mask                                        cBit14
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1BerTcaIntr_Shift                                           14

/*--------------------------------------
BitField Name: DE1BerSdIntr
BitField Type: RW
BitField Desc: Set 1 if there is a change of BER-SD.
BitField Bits: [13]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1BerSdIntr_Bit_Start                                        13
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1BerSdIntr_Bit_End                                          13
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1BerSdIntr_Mask                                         cBit13
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1BerSdIntr_Shift                                            13
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1BerSdIntr_MaxVal                                          0x1
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1BerSdIntr_MinVal                                          0x0
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1BerSdIntr_RstVal                                          0x0

/*--------------------------------------
BitField Name: DE1BerSfIntr
BitField Type: RW
BitField Desc: Set 1 if there is a change of BER-SF.
BitField Bits: [12]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1BerSfIntr_Bit_Start                                        12
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1BerSfIntr_Bit_End                                          12
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1BerSfIntr_Mask                                         cBit12
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1BerSfIntr_Shift                                            12
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1BerSfIntr_MaxVal                                          0x1
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1BerSfIntr_MinVal                                          0x0
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1BerSfIntr_RstVal                                          0x0

/*--------------------------------------
BitField Name: DE1oblcIntr
BitField Type: RW
BitField Desc: Set 1 if there is a change the new Outband LoopCode detected to
generate an interrupt.
BitField Bits: [11]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1oblcIntr_Bit_Start                                         11
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1oblcIntr_Bit_End                                           11
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1oblcIntr_Mask                                          cBit11
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1oblcIntr_Shift                                             11
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1oblcIntr_MaxVal                                           0x1
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1oblcIntr_MinVal                                           0x0
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1oblcIntr_RstVal                                           0x0

/*--------------------------------------
BitField Name: DE1iblcIntr
BitField Type: RW
BitField Desc: Set 1 if there is a change the new Inband LoopCode detected to
generate an interrupt.
BitField Bits: [10]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1iblcIntr_Bit_Start                                         10
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1iblcIntr_Bit_End                                           10
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1iblcIntr_Mask                                          cBit10
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1iblcIntr_Shift                                             10
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1iblcIntr_MaxVal                                           0x1
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1iblcIntr_MinVal                                           0x0
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1iblcIntr_RstVal                                           0x0

/*--------------------------------------
BitField Name: DE1SigRAIIntr
BitField Type: RW
BitField Desc: Set 1 if there is a change Signalling RAI event to generate an
interrupt.
BitField Bits: [9]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1SigRAIIntr_Bit_Start                                        9
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1SigRAIIntr_Bit_End                                          9
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1SigRAIIntr_Mask                                         cBit9
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1SigRAIIntr_Shift                                            9
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1SigRAIIntr_MaxVal                                         0x1
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1SigRAIIntr_MinVal                                         0x0
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1SigRAIIntr_RstVal                                         0x0

/*--------------------------------------
BitField Name: DE1SigLOFIntr
BitField Type: RW
BitField Desc: Set 1 if there is a change Signalling LOF event to generate an
interrupt.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1SigLOFIntr_Bit_Start                                        8
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1SigLOFIntr_Bit_End                                          8
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1SigLOFIntr_Mask                                         cBit8
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1SigLOFIntr_Shift                                            8
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1SigLOFIntr_MaxVal                                         0x1
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1SigLOFIntr_MinVal                                         0x0
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1SigLOFIntr_RstVal                                         0x0

/*--------------------------------------
BitField Name: DE1FbeIntr
BitField Type: RW
BitField Desc: Set 1 if there is a change in FBE exceed threshold te event.
BitField Bits: [7]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1FbeIntr_Bit_Start                                           7
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1FbeIntr_Bit_End                                             7
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1FbeIntr_Mask                                            cBit7
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1FbeIntr_Shift                                               7
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1FbeIntr_MaxVal                                            0x1
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1FbeIntr_MinVal                                            0x0
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1FbeIntr_RstVal                                            0x0

/*--------------------------------------
BitField Name: DE1CrcIntr
BitField Type: RW
BitField Desc: Set 1 if there is a change in CRC exceed threshold te event.
BitField Bits: [6]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1CrcIntr_Bit_Start                                           6
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1CrcIntr_Bit_End                                             6
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1CrcIntr_Mask                                            cBit6
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1CrcIntr_Shift                                               6
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1CrcIntr_MaxVal                                            0x1
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1CrcIntr_MinVal                                            0x0
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1CrcIntr_RstVal                                            0x0

/*--------------------------------------
BitField Name: DE1ReiIntr
BitField Type: RW
BitField Desc: Set 1 if there is a change in REI exceed threshold te event.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1ReiIntr_Bit_Start                                           5
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1ReiIntr_Bit_End                                             5
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1ReiIntr_Mask                                            cBit5
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1ReiIntr_Shift                                               5
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1ReiIntr_MaxVal                                            0x1
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1ReiIntr_MinVal                                            0x0
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1ReiIntr_RstVal                                            0x0

/*--------------------------------------
BitField Name: DE1LomfIntr
BitField Type: RW
BitField Desc: Set 1 if there is a change in LOMF te event.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LomfIntr_Bit_Start                                          4
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LomfIntr_Bit_End                                            4
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LomfIntr_Mask                                           cBit4
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LomfIntr_Shift                                              4
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LomfIntr_MaxVal                                           0x1
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LomfIntr_MinVal                                           0x0
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LomfIntr_RstVal                                           0x0

/*--------------------------------------
BitField Name: DE1LosIntr
BitField Type: RW
BitField Desc: Set 1 if there is a change in LOS te event.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LosIntr_Bit_Start                                           3
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LosIntr_Bit_End                                             3
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LosIntr_Mask                                            cBit3
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LosIntr_Shift                                               3
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LosIntr_MaxVal                                            0x1
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LosIntr_MinVal                                            0x0
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LosIntr_RstVal                                            0x0

/*--------------------------------------
BitField Name: DE1AisIntr
BitField Type: RW
BitField Desc: Set 1 if there is a change in AIS te event.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1AisIntr_Bit_Start                                           2
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1AisIntr_Bit_End                                             2
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1AisIntr_Mask                                            cBit2
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1AisIntr_Shift                                               2
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1AisIntr_MaxVal                                            0x1
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1AisIntr_MinVal                                            0x0
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1AisIntr_RstVal                                            0x0

/*--------------------------------------
BitField Name: DE1RaiIntr
BitField Type: RW
BitField Desc: Set 1 if there is a change in RAI te event.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1RaiIntr_Bit_Start                                           1
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1RaiIntr_Bit_End                                             1
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1RaiIntr_Mask                                            cBit1
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1RaiIntr_Shift                                               1
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1RaiIntr_MaxVal                                            0x1
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1RaiIntr_MinVal                                            0x0
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1RaiIntr_RstVal                                            0x0

/*--------------------------------------
BitField Name: DE1LofIntr
BitField Type: RW
BitField Desc: Set 1 if there is a change in LOF te event.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LofIntr_Bit_Start                                           0
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LofIntr_Bit_End                                             0
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LofIntr_Mask                                            cBit0
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LofIntr_Shift                                               0
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LofIntr_MaxVal                                            0x1
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LofIntr_MinVal                                            0x0
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LofIntr_RstVal                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer per Channel Current Status
Reg Addr   : 0x00055800-0x00055BFF
Reg Formula: 0x00055800 +  StsID*32 + Tug2ID*4 + VtnID
    Where  : 
           + $StsID(0-23): STS-1/VC-3 ID
           + $Tug2ID(0-6): TUG-2/VTG ID
           + $VtnID(0-3): VT/TU number ID in the TUG-2/VTG
Reg Desc   : 
This is the per Channel Current tus of DS1/E1/J1 Rx Framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rx_framer_per_chn_curr_stat_Base                                               0x00055800
#define cAf6Reg_dej1_rx_framer_per_chn_curr_stat(StsID, Tug2ID, VtnID)(0x00055800+(StsID)*32+(Tug2ID)*4+(VtnID))
#define cAf6Reg_dej1_rx_framer_per_chn_curr_stat_WidthVal                                                   32
#define cAf6Reg_dej1_rx_framer_per_chn_curr_stat_WriteMask                                                 0x0

/*--------------------------------------
BitField Name: DE1BerSdIntr
BitField Type: RW
BitField Desc: Set 1 if there is a change of BER-SD.
BitField Bits: [13]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1BerSdIntr_Bit_Start                                        13
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1BerSdIntr_Bit_End                                          13
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1BerSdIntr_Mask                                         cBit13
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1BerSdIntr_Shift                                            13
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1BerSdIntr_MaxVal                                          0x1
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1BerSdIntr_MinVal                                          0x0
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1BerSdIntr_RstVal                                          0x0

/*--------------------------------------
BitField Name: DE1BerSfIntr
BitField Type: RW
BitField Desc: Set 1 if there is a change of BER-SF.
BitField Bits: [12]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1BerSfIntr_Bit_Start                                        12
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1BerSfIntr_Bit_End                                          12
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1BerSfIntr_Mask                                         cBit12
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1BerSfIntr_Shift                                            12
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1BerSfIntr_MaxVal                                          0x1
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1BerSfIntr_MinVal                                          0x0
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1BerSfIntr_RstVal                                          0x0

/*--------------------------------------
BitField Name: DE1oblcIntrSta
BitField Type: RW
BitField Desc: Current status the new Outband LoopCode detected to generate an
interrupt.
BitField Bits: [11]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1oblcIntrSta_Bit_Start                                      11
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1oblcIntrSta_Bit_End                                        11
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1oblcIntrSta_Mask                                       cBit11
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1oblcIntrSta_Shift                                          11
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1oblcIntrSta_MaxVal                                        0x1
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1oblcIntrSta_MinVal                                        0x0
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1oblcIntrSta_RstVal                                        0x0

/*--------------------------------------
BitField Name: DE1iblcIntrSta
BitField Type: RW
BitField Desc: Current status the new Inband LoopCode detected to generate an
interrupt.
BitField Bits: [10]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1iblcIntrSta_Bit_Start                                      10
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1iblcIntrSta_Bit_End                                        10
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1iblcIntrSta_Mask                                       cBit10
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1iblcIntrSta_Shift                                          10
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1iblcIntrSta_MaxVal                                        0x1
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1iblcIntrSta_MinVal                                        0x0
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1iblcIntrSta_RstVal                                        0x0

/*--------------------------------------
BitField Name: DE1SigRAIIntrSta
BitField Type: RW
BitField Desc: Current status Signalling RAI event to generate an interrupt.
BitField Bits: [9]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1SigRAIIntrSta_Bit_Start                                       9
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1SigRAIIntrSta_Bit_End                                       9
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1SigRAIIntrSta_Mask                                      cBit9
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1SigRAIIntrSta_Shift                                         9
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1SigRAIIntrSta_MaxVal                                      0x1
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1SigRAIIntrSta_MinVal                                      0x0
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1SigRAIIntrSta_RstVal                                      0x0

/*--------------------------------------
BitField Name: DE1SigLOFIntrSta
BitField Type: RW
BitField Desc: Current status Signalling LOF event to generate an interrupt.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1SigLOFIntrSta_Bit_Start                                       8
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1SigLOFIntrSta_Bit_End                                       8
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1SigLOFIntrSta_Mask                                      cBit8
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1SigLOFIntrSta_Shift                                         8
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1SigLOFIntrSta_MaxVal                                      0x1
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1SigLOFIntrSta_MinVal                                      0x0
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1SigLOFIntrSta_RstVal                                      0x0

/*--------------------------------------
BitField Name: DE1FbeCurrSta
BitField Type: RW
BitField Desc: Current tus of FBE exceed threshold event.
BitField Bits: [7]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1FbeCurrSta_Bit_Start                                        7
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1FbeCurrSta_Bit_End                                          7
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1FbeCurrSta_Mask                                         cBit7
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1FbeCurrSta_Shift                                            7
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1FbeCurrSta_MaxVal                                         0x1
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1FbeCurrSta_MinVal                                         0x0
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1FbeCurrSta_RstVal                                         0x0

/*--------------------------------------
BitField Name: DE1CrcCurrSta
BitField Type: RW
BitField Desc: Current tus of CRC exceed threshold event.
BitField Bits: [6]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1CrcCurrSta_Bit_Start                                        6
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1CrcCurrSta_Bit_End                                          6
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1CrcCurrSta_Mask                                         cBit6
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1CrcCurrSta_Shift                                            6
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1CrcCurrSta_MaxVal                                         0x1
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1CrcCurrSta_MinVal                                         0x0
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1CrcCurrSta_RstVal                                         0x0

/*--------------------------------------
BitField Name: DE1ReiCurrSta
BitField Type: RW
BitField Desc: Current tus of REI exceed threshold event.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1ReiCurrSta_Bit_Start                                        5
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1ReiCurrSta_Bit_End                                          5
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1ReiCurrSta_Mask                                         cBit5
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1ReiCurrSta_Shift                                            5
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1ReiCurrSta_MaxVal                                         0x1
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1ReiCurrSta_MinVal                                         0x0
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1ReiCurrSta_RstVal                                         0x0

/*--------------------------------------
BitField Name: DE1LomfCurrSta
BitField Type: RW
BitField Desc: Current tus of LOMF event.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LomfCurrSta_Bit_Start                                       4
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LomfCurrSta_Bit_End                                         4
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LomfCurrSta_Mask                                        cBit4
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LomfCurrSta_Shift                                           4
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LomfCurrSta_MaxVal                                        0x1
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LomfCurrSta_MinVal                                        0x0
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LomfCurrSta_RstVal                                        0x0

/*--------------------------------------
BitField Name: DE1LosCurrSta
BitField Type: RW
BitField Desc: Current tus of LOS event.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LosCurrSta_Bit_Start                                        3
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LosCurrSta_Bit_End                                          3
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LosCurrSta_Mask                                         cBit3
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LosCurrSta_Shift                                            3
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LosCurrSta_MaxVal                                         0x1
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LosCurrSta_MinVal                                         0x0
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LosCurrSta_RstVal                                         0x0

/*--------------------------------------
BitField Name: DE1AisCurrSta
BitField Type: RW
BitField Desc: Current tus of AIS event.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1AisCurrSta_Bit_Start                                        2
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1AisCurrSta_Bit_End                                          2
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1AisCurrSta_Mask                                         cBit2
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1AisCurrSta_Shift                                            2
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1AisCurrSta_MaxVal                                         0x1
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1AisCurrSta_MinVal                                         0x0
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1AisCurrSta_RstVal                                         0x0

/*--------------------------------------
BitField Name: DE1RaiCurrSta
BitField Type: RW
BitField Desc: Current tus of RAI event.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1RaiCurrSta_Bit_Start                                        1
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1RaiCurrSta_Bit_End                                          1
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1RaiCurrSta_Mask                                         cBit1
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1RaiCurrSta_Shift                                            1
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1RaiCurrSta_MaxVal                                         0x1
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1RaiCurrSta_MinVal                                         0x0
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1RaiCurrSta_RstVal                                         0x0

/*--------------------------------------
BitField Name: DE1LofCurrSta
BitField Type: RW
BitField Desc: Current tus of LOF event.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LofCurrSta_Bit_Start                                        0
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LofCurrSta_Bit_End                                          0
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LofCurrSta_Mask                                         cBit0
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LofCurrSta_Shift                                            0
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LofCurrSta_MaxVal                                         0x1
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LofCurrSta_MinVal                                         0x0
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LofCurrSta_RstVal                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer per Channel Interrupt OR Status
Reg Addr   : 0x00055C00-0x00055C1F
Reg Formula: 0x00055C00 +  StsID
    Where  : 
           + $StsID(0-23): STS-1/VC-3 ID
Reg Desc   : 
The register consists of 28 bits for 28 VT/TUs of the related STS/VC in the Rx DS1/E1/J1 Framer. Each bit is used to store Interrupt OR tus of the related DS1/E1/J1.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rx_framer_per_chn_intr_or_stat_Base                                            0x00055C00
#define cAf6Reg_dej1_rx_framer_per_chn_intr_or_stat(StsID)                                (0x00055C00+(StsID))
#define cAf6Reg_dej1_rx_framer_per_chn_intr_or_stat_WidthVal                                                32
#define cAf6Reg_dej1_rx_framer_per_chn_intr_or_stat_WriteMask                                              0x0

/*--------------------------------------
BitField Name: RxDE1VtIntrOrSta
BitField Type: RW
BitField Desc: Set to 1 if any interrupt tus bit of corresponding DS1/E1/J1 is
set and its interrupt is enabled.
BitField Bits: [27:0]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_or_stat_RxDE1VtIntrOrSta_Bit_Start                                       0
#define cAf6_dej1_rx_framer_per_chn_intr_or_stat_RxDE1VtIntrOrSta_Bit_End                                      27
#define cAf6_dej1_rx_framer_per_chn_intr_or_stat_RxDE1VtIntrOrSta_Mask                                cBit27_0
#define cAf6_dej1_rx_framer_per_chn_intr_or_stat_RxDE1VtIntrOrSta_Shift                                       0
#define cAf6_dej1_rx_framer_per_chn_intr_or_stat_RxDE1VtIntrOrSta_MaxVal                               0xfffffff
#define cAf6_dej1_rx_framer_per_chn_intr_or_stat_RxDE1VtIntrOrSta_MinVal                                     0x0
#define cAf6_dej1_rx_framer_per_chn_intr_or_stat_RxDE1VtIntrOrSta_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer per STS/VC Interrupt OR Status
Reg Addr   : 0x00055FFF
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 12 bits for 12 STS/VCs of the Rx DS1/E1/J1 Framer. Each bit is used to store Interrupt OR tus of the related STS/VC.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rx_framer_per_stsvc_intr_or_stat_Base                                          0x00055FFF
#define cAf6Reg_dej1_rx_framer_per_stsvc_intr_or_stat                                               0x00055FFF
#define cAf6Reg_dej1_rx_framer_per_stsvc_intr_or_stat_WidthVal                                              32
#define cAf6Reg_dej1_rx_framer_per_stsvc_intr_or_stat_WriteMask                                            0x0

/*--------------------------------------
BitField Name: RxDE1StsIntrOrSta
BitField Type: RW
BitField Desc: Set to 1 if any interrupt tus bit of corresponding STS/VC is set
and its interrupt is enabled
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_stsvc_intr_or_stat_RxDE1StsIntrOrSta_Bit_Start                                       0
#define cAf6_dej1_rx_framer_per_stsvc_intr_or_stat_RxDE1StsIntrOrSta_Bit_End                                      23
#define cAf6_dej1_rx_framer_per_stsvc_intr_or_stat_RxDE1StsIntrOrSta_Mask                                cBit23_0
#define cAf6_dej1_rx_framer_per_stsvc_intr_or_stat_RxDE1StsIntrOrSta_Shift                                       0
#define cAf6_dej1_rx_framer_per_stsvc_intr_or_stat_RxDE1StsIntrOrSta_MaxVal                                0xffffff
#define cAf6_dej1_rx_framer_per_stsvc_intr_or_stat_RxDE1StsIntrOrSta_MinVal                                     0x0
#define cAf6_dej1_rx_framer_per_stsvc_intr_or_stat_RxDE1StsIntrOrSta_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer per STS/VC Interrupt Enable Control
Reg Addr   : 0x00055FFE
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 12 interrupt enable bits for 12 STS/VCs in the Rx DS1/E1/J1 Framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rx_framer_per_stsvc_intr_en_ctrl_Base                                          0x00055FFE
#define cAf6Reg_dej1_rx_framer_per_stsvc_intr_en_ctrl                                               0x00055FFE
#define cAf6Reg_dej1_rx_framer_per_stsvc_intr_en_ctrl_WidthVal                                              32
#define cAf6Reg_dej1_rx_framer_per_stsvc_intr_en_ctrl_WriteMask                                            0x0

/*--------------------------------------
BitField Name: RxDE1StsIntrEn
BitField Type: RW
BitField Desc: Set to 1 to enable the related STS/VC to generate interrupt.
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_stsvc_intr_en_ctrl_RxDE1StsIntrEn_Bit_Start                                       0
#define cAf6_dej1_rx_framer_per_stsvc_intr_en_ctrl_RxDE1StsIntrEn_Bit_End                                      23
#define cAf6_dej1_rx_framer_per_stsvc_intr_en_ctrl_RxDE1StsIntrEn_Mask                                cBit23_0
#define cAf6_dej1_rx_framer_per_stsvc_intr_en_ctrl_RxDE1StsIntrEn_Shift                                       0
#define cAf6_dej1_rx_framer_per_stsvc_intr_en_ctrl_RxDE1StsIntrEn_MaxVal                                0xffffff
#define cAf6_dej1_rx_framer_per_stsvc_intr_en_ctrl_RxDE1StsIntrEn_MinVal                                     0x0
#define cAf6_dej1_rx_framer_per_stsvc_intr_en_ctrl_RxDE1StsIntrEn_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer DLK Control
Reg Addr   : 0x000D8000 -  0x000D82F7
Reg Formula: 
    Where  : 
Reg Desc   : 
The register is used to configuration for  Rx DS1/E1/J1 Framer DLK.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rx_framer_dlk_ctrl_Base                                                        0x000D8000
#define cAf6Reg_dej1_rx_framer_dlk_ctrl                                                             0x000D8000
#define cAf6Reg_dej1_rx_framer_dlk_ctrl_WidthVal                                                            32
#define cAf6Reg_dej1_rx_framer_dlk_ctrl_WriteMask                                                          0x0

/*--------------------------------------
BitField Name: RxDE1DlkStkThres
BitField Type: RW
BitField Desc:
BitField Bits: [16:11]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkStkThres_Bit_Start                                             11
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkStkThres_Bit_End                                               16
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkStkThres_Mask                                           cBit16_11
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkStkThres_Shift                                                 11
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkStkThres_MaxVal                                              0x3f
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkStkThres_MinVal                                               0x0
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkStkThres_RstVal                                               0x0

/*--------------------------------------
BitField Name: RxDE1DlkBomThres
BitField Type: RW
BitField Desc: Threshold to generate interrupt if the BOM counter exceed this
threshold
BitField Bits: [10:6]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkBomThres_Bit_Start                                              6
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkBomThres_Bit_End                                               10
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkBomThres_Mask                                            cBit10_6
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkBomThres_Shift                                                  6
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkBomThres_MaxVal                                              0x1f
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkBomThres_MinVal                                               0x0
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkBomThres_RstVal                                               0x0

/*--------------------------------------
BitField Name: RxDE1DlkFcsInvEn
BitField Type: RW
BitField Desc: Set 1 to enable FCS invert.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkFcsInvEn_Bit_Start                                              5
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkFcsInvEn_Bit_End                                                5
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkFcsInvEn_Mask                                               cBit5
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkFcsInvEn_Shift                                                  5
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkFcsInvEn_MaxVal                                               0x1
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkFcsInvEn_MinVal                                               0x0
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkFcsInvEn_RstVal                                               0x0

/*--------------------------------------
BitField Name: RxDE1DlkPayInvEn
BitField Type: RW
BitField Desc: Set 1 to enable payload invert.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkPayInvEn_Bit_Start                                              4
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkPayInvEn_Bit_End                                                4
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkPayInvEn_Mask                                               cBit4
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkPayInvEn_Shift                                                  4
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkPayInvEn_MaxVal                                               0x1
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkPayInvEn_MinVal                                               0x0
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkPayInvEn_RstVal                                               0x0

/*--------------------------------------
BitField Name: RxDE1DlkFcsEn
BitField Type: RW
BitField Desc: reserved
BitField Bits: [3]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkFcsEn_Bit_Start                                                 3
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkFcsEn_Bit_End                                                   3
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkFcsEn_Mask                                                  cBit3
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkFcsEn_Shift                                                     3
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkFcsEn_MaxVal                                                  0x1
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkFcsEn_MinVal                                                  0x0
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkFcsEn_RstVal                                                  0x0

/*--------------------------------------
BitField Name: RxDE1DlkByteStuff
BitField Type: RW
BitField Desc: reserved
BitField Bits: [2]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkByteStuff_Bit_Start                                             2
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkByteStuff_Bit_End                                               2
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkByteStuff_Mask                                              cBit2
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkByteStuff_Shift                                                 2
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkByteStuff_MaxVal                                              0x1
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkByteStuff_MinVal                                              0x0
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkByteStuff_RstVal                                              0x0

/*--------------------------------------
BitField Name: RxDE1DlkMsbLsb
BitField Type: RW
BitField Desc: set 1 to enable MSB and LSB convert
BitField Bits: [1]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkMsbLsb_Bit_Start                                                1
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkMsbLsb_Bit_End                                                  1
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkMsbLsb_Mask                                                 cBit1
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkMsbLsb_Shift                                                    1
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkMsbLsb_MaxVal                                                 0x1
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkMsbLsb_MinVal                                                 0x0
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkMsbLsb_RstVal                                                 0x0

/*--------------------------------------
BitField Name: RxDE1DlkEn
BitField Type: RW
BitField Desc: Set 1 to enable monitor DS1/E1/J1 Rx DLK
BitField Bits: [0]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkEn_Bit_Start                                                    0
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkEn_Bit_End                                                      0
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkEn_Mask                                                     cBit0
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkEn_Shift                                                        0
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkEn_MaxVal                                                     0x1
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkEn_MinVal                                                     0x0
#define cAf6_dej1_rx_framer_dlk_ctrl_RxDE1DlkEn_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer DLK Status
Reg Addr   : 0x000D9000 -  0x000D92F7
Reg Formula: 
    Where  : 
Reg Desc   : 
The register is used to configuration for  Rx DS1/E1/J1 Framer DLK. For HW use only

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rx_framer_dlk_stat_Base                                                        0x000D9000
#define cAf6Reg_dej1_rx_framer_dlk_stat                                                             0x000D9000
#define cAf6Reg_dej1_rx_framer_dlk_stat_WidthVal                                                            32
#define cAf6Reg_dej1_rx_framer_dlk_stat_WriteMask                                                          0x0

/*--------------------------------------
BitField Name: RxDE1DlkNewBomCnt
BitField Type: RW
BitField Desc:
BitField Bits: [17]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewBomCnt_Bit_Start                                            17
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewBomCnt_Bit_End                                              17
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewBomCnt_Mask                                             cBit17
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewBomCnt_Shift                                                17
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewBomCnt_MaxVal                                              0x1
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewBomCnt_MinVal                                              0x0
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewBomCnt_RstVal                                              0x0

/*--------------------------------------
BitField Name: RxDE1DlkNewCacheByteCnt
BitField Type: RW
BitField Desc:
BitField Bits: [16]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewCacheByteCnt_Bit_Start                                      16
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewCacheByteCnt_Bit_End                                        16
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewCacheByteCnt_Mask                                       cBit16
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewCacheByteCnt_Shift                                          16
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewCacheByteCnt_MaxVal                                        0x1
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewCacheByteCnt_MinVal                                        0x0
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewCacheByteCnt_RstVal                                        0x0

/*--------------------------------------
BitField Name: RxDE1DlkNewDataStoreEn
BitField Type: RW
BitField Desc:
BitField Bits: [15]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewDataStoreEn_Bit_Start                                       15
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewDataStoreEn_Bit_End                                         15
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewDataStoreEn_Mask                                        cBit15
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewDataStoreEn_Shift                                           15
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewDataStoreEn_MaxVal                                         0x1
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewDataStoreEn_MinVal                                         0x0
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewDataStoreEn_RstVal                                         0x0

/*--------------------------------------
BitField Name: RxDE1DlkNewDataStore
BitField Type: RW
BitField Desc:
BitField Bits: [14]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewDataStore_Bit_Start                                         14
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewDataStore_Bit_End                                           14
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewDataStore_Mask                                          cBit14
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewDataStore_Shift                                             14
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewDataStore_MaxVal                                           0x1
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewDataStore_MinVal                                           0x0
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewDataStore_RstVal                                           0x0

/*--------------------------------------
BitField Name: RxDE1DlkNewCacheFill
BitField Type: RW
BitField Desc:
BitField Bits: [13]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewCacheFill_Bit_Start                                         13
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewCacheFill_Bit_End                                           13
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewCacheFill_Mask                                          cBit13
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewCacheFill_Shift                                             13
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewCacheFill_MaxVal                                           0x1
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewCacheFill_MinVal                                           0x0
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewCacheFill_RstVal                                           0x0

/*--------------------------------------
BitField Name: RxDE1DlkNewMsgCnt
BitField Type: RW
BitField Desc:
BitField Bits: [12]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewMsgCnt_Bit_Start                                            12
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewMsgCnt_Bit_End                                              12
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewMsgCnt_Mask                                             cBit12
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewMsgCnt_Shift                                                12
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewMsgCnt_MaxVal                                              0x1
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewMsgCnt_MinVal                                              0x0
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewMsgCnt_RstVal                                              0x0

/*--------------------------------------
BitField Name: RxDE1DlkNewRdAdd
BitField Type: RW
BitField Desc:
BitField Bits: [11]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewRdAdd_Bit_Start                                             11
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewRdAdd_Bit_End                                               11
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewRdAdd_Mask                                              cBit11
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewRdAdd_Shift                                                 11
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewRdAdd_MaxVal                                               0x1
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewRdAdd_MinVal                                               0x0
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewRdAdd_RstVal                                               0x0

/*--------------------------------------
BitField Name: RxDE1DlkNewWrAdd
BitField Type: RW
BitField Desc:
BitField Bits: [10]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewWrAdd_Bit_Start                                             10
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewWrAdd_Bit_End                                               10
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewWrAdd_Mask                                              cBit10
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewWrAdd_Shift                                                 10
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewWrAdd_MaxVal                                               0x1
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewWrAdd_MinVal                                               0x0
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewWrAdd_RstVal                                               0x0

/*--------------------------------------
BitField Name: RxDE1DlkNewFcs
BitField Type: RW
BitField Desc:
BitField Bits: [9]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewFcs_Bit_Start                                                9
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewFcs_Bit_End                                                  9
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewFcs_Mask                                                 cBit9
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewFcs_Shift                                                    9
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewFcs_MaxVal                                                 0x1
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewFcs_MinVal                                                 0x0
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewFcs_RstVal                                                 0x0

/*--------------------------------------
BitField Name: RxDE1DlkNewMsgShift
BitField Type: RW
BitField Desc:
BitField Bits: [8]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewMsgShift_Bit_Start                                           8
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewMsgShift_Bit_End                                             8
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewMsgShift_Mask                                            cBit8
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewMsgShift_Shift                                               8
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewMsgShift_MaxVal                                            0x1
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewMsgShift_MinVal                                            0x0
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewMsgShift_RstVal                                            0x0

/*--------------------------------------
BitField Name: RxDE1DlkNewBitCnt
BitField Type: RW
BitField Desc:
BitField Bits: [7]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewBitCnt_Bit_Start                                             7
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewBitCnt_Bit_End                                               7
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewBitCnt_Mask                                              cBit7
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewBitCnt_Shift                                                 7
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewBitCnt_MaxVal                                              0x1
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewBitCnt_MinVal                                              0x0
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewBitCnt_RstVal                                              0x0

/*--------------------------------------
BitField Name: RxDE1DlkNewStuffCnt
BitField Type: RW
BitField Desc:
BitField Bits: [6]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewStuffCnt_Bit_Start                                           6
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewStuffCnt_Bit_End                                             6
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewStuffCnt_Mask                                            cBit6
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewStuffCnt_Shift                                               6
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewStuffCnt_MaxVal                                            0x1
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewStuffCnt_MinVal                                            0x0
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewStuffCnt_RstVal                                            0x0

/*--------------------------------------
BitField Name: RxDE1DlkNewFlagOff
BitField Type: RW
BitField Desc:
BitField Bits: [5]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewFlagOff_Bit_Start                                            5
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewFlagOff_Bit_End                                              5
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewFlagOff_Mask                                             cBit5
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewFlagOff_Shift                                                5
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewFlagOff_MaxVal                                             0x1
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewFlagOff_MinVal                                             0x0
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewFlagOff_RstVal                                             0x0

/*--------------------------------------
BitField Name: RxDE1DlkNewByte3Store
BitField Type: RW
BitField Desc:
BitField Bits: [4]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewByte3Store_Bit_Start                                         4
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewByte3Store_Bit_End                                           4
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewByte3Store_Mask                                          cBit4
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewByte3Store_Shift                                             4
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewByte3Store_MaxVal                                          0x1
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewByte3Store_MinVal                                          0x0
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewByte3Store_RstVal                                          0x0

/*--------------------------------------
BitField Name: RxDE1DlkNewByte2Store
BitField Type: RW
BitField Desc:
BitField Bits: [3]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewByte2Store_Bit_Start                                         3
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewByte2Store_Bit_End                                           3
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewByte2Store_Mask                                          cBit3
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewByte2Store_Shift                                             3
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewByte2Store_MaxVal                                          0x1
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewByte2Store_MinVal                                          0x0
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewByte2Store_RstVal                                          0x0

/*--------------------------------------
BitField Name: RxDE1DlkNewByte1Store
BitField Type: RW
BitField Desc:
BitField Bits: [2]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewByte1Store_Bit_Start                                         2
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewByte1Store_Bit_End                                           2
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewByte1Store_Mask                                          cBit2
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewByte1Store_Shift                                             2
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewByte1Store_MaxVal                                          0x1
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewByte1Store_MinVal                                          0x0
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewByte1Store_RstVal                                          0x0

/*--------------------------------------
BitField Name: RxDE1DlkNewByteShf
BitField Type: RW
BitField Desc:
BitField Bits: [1]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewByteShf_Bit_Start                                            1
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewByteShf_Bit_End                                              1
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewByteShf_Mask                                             cBit1
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewByteShf_Shift                                                1
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewByteShf_MaxVal                                             0x1
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewByteShf_MinVal                                             0x0
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewByteShf_RstVal                                             0x0

/*--------------------------------------
BitField Name: RxDE1DlkNewState
BitField Type: RW
BitField Desc:
BitField Bits: [0]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewState_Bit_Start                                              0
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewState_Bit_End                                                0
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewState_Mask                                               cBit0
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewState_Shift                                                  0
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewState_MaxVal                                               0x1
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewState_MinVal                                               0x0
#define cAf6_dej1_rx_framer_dlk_stat_RxDE1DlkNewState_RstVal                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer DLK Flush Message
Reg Addr   : 0x000D0011
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to flush all messages of a channel.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rx_framer_dlk_flush_msg_ctrl_Base                                              0x000D0011
#define cAf6Reg_dej1_rx_framer_dlk_flush_msg_ctrl                                                   0x000D0011
#define cAf6Reg_dej1_rx_framer_dlk_flush_msg_ctrl_WidthVal                                                  32
#define cAf6Reg_dej1_rx_framer_dlk_flush_msg_ctrl_WriteMask                                                0x0

/*--------------------------------------
BitField Name: RxDE1DlkFlushEn
BitField Type: RW
BitField Desc: Set 1 to enable flush buffer.
BitField Bits: [10]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dlk_flush_msg_ctrl_RxDE1DlkFlushEn_Bit_Start                                      10
#define cAf6_dej1_rx_framer_dlk_flush_msg_ctrl_RxDE1DlkFlushEn_Bit_End                                      10
#define cAf6_dej1_rx_framer_dlk_flush_msg_ctrl_RxDE1DlkFlushEn_Mask                                     cBit10
#define cAf6_dej1_rx_framer_dlk_flush_msg_ctrl_RxDE1DlkFlushEn_Shift                                        10
#define cAf6_dej1_rx_framer_dlk_flush_msg_ctrl_RxDE1DlkFlushEn_MaxVal                                      0x1
#define cAf6_dej1_rx_framer_dlk_flush_msg_ctrl_RxDE1DlkFlushEn_MinVal                                      0x0
#define cAf6_dej1_rx_framer_dlk_flush_msg_ctrl_RxDE1DlkFlushEn_RstVal                                      0x0

/*--------------------------------------
BitField Name: RxDE1DlkChid
BitField Type: RW
BitField Desc: Channel DLK which is flushed.
BitField Bits: [9:0]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dlk_flush_msg_ctrl_RxDE1DlkChid_Bit_Start                                        0
#define cAf6_dej1_rx_framer_dlk_flush_msg_ctrl_RxDE1DlkChid_Bit_End                                          9
#define cAf6_dej1_rx_framer_dlk_flush_msg_ctrl_RxDE1DlkChid_Mask                                       cBit9_0
#define cAf6_dej1_rx_framer_dlk_flush_msg_ctrl_RxDE1DlkChid_Shift                                            0
#define cAf6_dej1_rx_framer_dlk_flush_msg_ctrl_RxDE1DlkChid_MaxVal                                       0x3ff
#define cAf6_dej1_rx_framer_dlk_flush_msg_ctrl_RxDE1DlkChid_MinVal                                         0x0
#define cAf6_dej1_rx_framer_dlk_flush_msg_ctrl_RxDE1DlkChid_RstVal                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer DLK Access DDR Control
Reg Addr   : 0x000D0010
Reg Formula: 
    Where  : 
Reg Desc   : 
.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_framer_clk_acc_ddr_ctrl_Base                                                   0x000D0010
#define cAf6Reg_dej1_framer_clk_acc_ddr_ctrl                                                        0x000D0010
#define cAf6Reg_dej1_framer_clk_acc_ddr_ctrl_WidthVal                                                       32
#define cAf6Reg_dej1_framer_clk_acc_ddr_ctrl_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: RxDE1DlkDDRReady
BitField Type: RW
BitField Desc:
BitField Bits: [16]
--------------------------------------*/
#define cAf6_dej1_framer_clk_acc_ddr_ctrl_RxDE1DlkDDRReady_Bit_Start                                        16
#define cAf6_dej1_framer_clk_acc_ddr_ctrl_RxDE1DlkDDRReady_Bit_End                                          16
#define cAf6_dej1_framer_clk_acc_ddr_ctrl_RxDE1DlkDDRReady_Mask                                         cBit16
#define cAf6_dej1_framer_clk_acc_ddr_ctrl_RxDE1DlkDDRReady_Shift                                            16
#define cAf6_dej1_framer_clk_acc_ddr_ctrl_RxDE1DlkDDRReady_MaxVal                                          0x1
#define cAf6_dej1_framer_clk_acc_ddr_ctrl_RxDE1DlkDDRReady_MinVal                                          0x0
#define cAf6_dej1_framer_clk_acc_ddr_ctrl_RxDE1DlkDDRReady_RstVal                                          0x0

/*--------------------------------------
BitField Name: RxDE1DlkChid
BitField Type: RW
BitField Desc: Channel DLK which is read.
BitField Bits: [9:0]
--------------------------------------*/
#define cAf6_dej1_framer_clk_acc_ddr_ctrl_RxDE1DlkChid_Bit_Start                                             0
#define cAf6_dej1_framer_clk_acc_ddr_ctrl_RxDE1DlkChid_Bit_End                                               9
#define cAf6_dej1_framer_clk_acc_ddr_ctrl_RxDE1DlkChid_Mask                                            cBit9_0
#define cAf6_dej1_framer_clk_acc_ddr_ctrl_RxDE1DlkChid_Shift                                                 0
#define cAf6_dej1_framer_clk_acc_ddr_ctrl_RxDE1DlkChid_MaxVal                                            0x3ff
#define cAf6_dej1_framer_clk_acc_ddr_ctrl_RxDE1DlkChid_MinVal                                              0x0
#define cAf6_dej1_framer_clk_acc_ddr_ctrl_RxDE1DlkChid_RstVal                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer DLK Message0
Reg Addr   : 0x000D0016
Reg Formula: 
    Where  : 
Reg Desc   : 
.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rx_framer_clk_msg0_Base                                                        0x000D0016
#define cAf6Reg_dej1_rx_framer_clk_msg0                                                             0x000D0016
#define cAf6Reg_dej1_rx_framer_clk_msg0_WidthVal                                                            32
#define cAf6Reg_dej1_rx_framer_clk_msg0_WriteMask                                                          0x0

/*--------------------------------------
BitField Name: RxDE1DlkCache1
BitField Type: RW
BitField Desc:
BitField Bits: [26:24]
--------------------------------------*/
#define cAf6_dej1_rx_framer_clk_msg0_RxDE1DlkCache1_Bit_Start                                               24
#define cAf6_dej1_rx_framer_clk_msg0_RxDE1DlkCache1_Bit_End                                                 26
#define cAf6_dej1_rx_framer_clk_msg0_RxDE1DlkCache1_Mask                                             cBit26_24
#define cAf6_dej1_rx_framer_clk_msg0_RxDE1DlkCache1_Shift                                                   24
#define cAf6_dej1_rx_framer_clk_msg0_RxDE1DlkCache1_MaxVal                                                 0x7
#define cAf6_dej1_rx_framer_clk_msg0_RxDE1DlkCache1_MinVal                                                 0x0
#define cAf6_dej1_rx_framer_clk_msg0_RxDE1DlkCache1_RstVal                                                 0x0

/*--------------------------------------
BitField Name: RxDE1DlkByte1
BitField Type: RW
BitField Desc:
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_dej1_rx_framer_clk_msg0_RxDE1DlkByte1_Bit_Start                                                16
#define cAf6_dej1_rx_framer_clk_msg0_RxDE1DlkByte1_Bit_End                                                  23
#define cAf6_dej1_rx_framer_clk_msg0_RxDE1DlkByte1_Mask                                              cBit23_16
#define cAf6_dej1_rx_framer_clk_msg0_RxDE1DlkByte1_Shift                                                    16
#define cAf6_dej1_rx_framer_clk_msg0_RxDE1DlkByte1_MaxVal                                                 0xff
#define cAf6_dej1_rx_framer_clk_msg0_RxDE1DlkByte1_MinVal                                                  0x0
#define cAf6_dej1_rx_framer_clk_msg0_RxDE1DlkByte1_RstVal                                                  0x0

/*--------------------------------------
BitField Name: RxDE1DlkCache0
BitField Type: RW
BitField Desc:
BitField Bits: [10:8]
--------------------------------------*/
#define cAf6_dej1_rx_framer_clk_msg0_RxDE1DlkCache0_Bit_Start                                                8
#define cAf6_dej1_rx_framer_clk_msg0_RxDE1DlkCache0_Bit_End                                                 10
#define cAf6_dej1_rx_framer_clk_msg0_RxDE1DlkCache0_Mask                                              cBit10_8
#define cAf6_dej1_rx_framer_clk_msg0_RxDE1DlkCache0_Shift                                                    8
#define cAf6_dej1_rx_framer_clk_msg0_RxDE1DlkCache0_MaxVal                                                 0x7
#define cAf6_dej1_rx_framer_clk_msg0_RxDE1DlkCache0_MinVal                                                 0x0
#define cAf6_dej1_rx_framer_clk_msg0_RxDE1DlkCache0_RstVal                                                 0x0

/*--------------------------------------
BitField Name: RxDE1DlkByte0
BitField Type: RW
BitField Desc:
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_dej1_rx_framer_clk_msg0_RxDE1DlkByte0_Bit_Start                                                 0
#define cAf6_dej1_rx_framer_clk_msg0_RxDE1DlkByte0_Bit_End                                                   7
#define cAf6_dej1_rx_framer_clk_msg0_RxDE1DlkByte0_Mask                                                cBit7_0
#define cAf6_dej1_rx_framer_clk_msg0_RxDE1DlkByte0_Shift                                                     0
#define cAf6_dej1_rx_framer_clk_msg0_RxDE1DlkByte0_MaxVal                                                 0xff
#define cAf6_dej1_rx_framer_clk_msg0_RxDE1DlkByte0_MinVal                                                  0x0
#define cAf6_dej1_rx_framer_clk_msg0_RxDE1DlkByte0_RstVal                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer DLK Message1
Reg Addr   : 0x000D0017
Reg Formula: 
    Where  : 
Reg Desc   : 
.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rx_framer_msg1_Base                                                            0x000D0017
#define cAf6Reg_dej1_rx_framer_msg1                                                                 0x000D0017
#define cAf6Reg_dej1_rx_framer_msg1_WidthVal                                                                32
#define cAf6Reg_dej1_rx_framer_msg1_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: RxDE1DlkCache3
BitField Type: RW
BitField Desc:
BitField Bits: [26:24]
--------------------------------------*/
#define cAf6_dej1_rx_framer_msg1_RxDE1DlkCache3_Bit_Start                                                   24
#define cAf6_dej1_rx_framer_msg1_RxDE1DlkCache3_Bit_End                                                     26
#define cAf6_dej1_rx_framer_msg1_RxDE1DlkCache3_Mask                                                 cBit26_24
#define cAf6_dej1_rx_framer_msg1_RxDE1DlkCache3_Shift                                                       24
#define cAf6_dej1_rx_framer_msg1_RxDE1DlkCache3_MaxVal                                                     0x7
#define cAf6_dej1_rx_framer_msg1_RxDE1DlkCache3_MinVal                                                     0x0
#define cAf6_dej1_rx_framer_msg1_RxDE1DlkCache3_RstVal                                                     0x0

/*--------------------------------------
BitField Name: RxDE1DlkByte3
BitField Type: RW
BitField Desc:
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_dej1_rx_framer_msg1_RxDE1DlkByte3_Bit_Start                                                    16
#define cAf6_dej1_rx_framer_msg1_RxDE1DlkByte3_Bit_End                                                      23
#define cAf6_dej1_rx_framer_msg1_RxDE1DlkByte3_Mask                                                  cBit23_16
#define cAf6_dej1_rx_framer_msg1_RxDE1DlkByte3_Shift                                                        16
#define cAf6_dej1_rx_framer_msg1_RxDE1DlkByte3_MaxVal                                                     0xff
#define cAf6_dej1_rx_framer_msg1_RxDE1DlkByte3_MinVal                                                      0x0
#define cAf6_dej1_rx_framer_msg1_RxDE1DlkByte3_RstVal                                                      0x0

/*--------------------------------------
BitField Name: RxDE1DlkCache2
BitField Type: RW
BitField Desc:
BitField Bits: [10:8]
--------------------------------------*/
#define cAf6_dej1_rx_framer_msg1_RxDE1DlkCache2_Bit_Start                                                    8
#define cAf6_dej1_rx_framer_msg1_RxDE1DlkCache2_Bit_End                                                     10
#define cAf6_dej1_rx_framer_msg1_RxDE1DlkCache2_Mask                                                  cBit10_8
#define cAf6_dej1_rx_framer_msg1_RxDE1DlkCache2_Shift                                                        8
#define cAf6_dej1_rx_framer_msg1_RxDE1DlkCache2_MaxVal                                                     0x7
#define cAf6_dej1_rx_framer_msg1_RxDE1DlkCache2_MinVal                                                     0x0
#define cAf6_dej1_rx_framer_msg1_RxDE1DlkCache2_RstVal                                                     0x0

/*--------------------------------------
BitField Name: RxDE1DlkByte2
BitField Type: RW
BitField Desc:
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_dej1_rx_framer_msg1_RxDE1DlkByte2_Bit_Start                                                     0
#define cAf6_dej1_rx_framer_msg1_RxDE1DlkByte2_Bit_End                                                       7
#define cAf6_dej1_rx_framer_msg1_RxDE1DlkByte2_Mask                                                    cBit7_0
#define cAf6_dej1_rx_framer_msg1_RxDE1DlkByte2_Shift                                                         0
#define cAf6_dej1_rx_framer_msg1_RxDE1DlkByte2_MaxVal                                                     0xff
#define cAf6_dej1_rx_framer_msg1_RxDE1DlkByte2_MinVal                                                      0x0
#define cAf6_dej1_rx_framer_msg1_RxDE1DlkByte2_RstVal                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer DLK Message2
Reg Addr   : 0x000D0018
Reg Formula: 
    Where  : 
Reg Desc   : 
.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rx_framer_dkl_msg2_Base                                                        0x000D0018
#define cAf6Reg_dej1_rx_framer_dkl_msg2                                                             0x000D0018
#define cAf6Reg_dej1_rx_framer_dkl_msg2_WidthVal                                                            32
#define cAf6Reg_dej1_rx_framer_dkl_msg2_WriteMask                                                          0x0

/*--------------------------------------
BitField Name: RxDE1DlkCache5
BitField Type: RW
BitField Desc:
BitField Bits: [26:24]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dkl_msg2_RxDE1DlkCache5_Bit_Start                                               24
#define cAf6_dej1_rx_framer_dkl_msg2_RxDE1DlkCache5_Bit_End                                                 26
#define cAf6_dej1_rx_framer_dkl_msg2_RxDE1DlkCache5_Mask                                             cBit26_24
#define cAf6_dej1_rx_framer_dkl_msg2_RxDE1DlkCache5_Shift                                                   24
#define cAf6_dej1_rx_framer_dkl_msg2_RxDE1DlkCache5_MaxVal                                                 0x7
#define cAf6_dej1_rx_framer_dkl_msg2_RxDE1DlkCache5_MinVal                                                 0x0
#define cAf6_dej1_rx_framer_dkl_msg2_RxDE1DlkCache5_RstVal                                                 0x0

/*--------------------------------------
BitField Name: RxDE1DlkByte5
BitField Type: RW
BitField Desc:
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dkl_msg2_RxDE1DlkByte5_Bit_Start                                                16
#define cAf6_dej1_rx_framer_dkl_msg2_RxDE1DlkByte5_Bit_End                                                  23
#define cAf6_dej1_rx_framer_dkl_msg2_RxDE1DlkByte5_Mask                                              cBit23_16
#define cAf6_dej1_rx_framer_dkl_msg2_RxDE1DlkByte5_Shift                                                    16
#define cAf6_dej1_rx_framer_dkl_msg2_RxDE1DlkByte5_MaxVal                                                 0xff
#define cAf6_dej1_rx_framer_dkl_msg2_RxDE1DlkByte5_MinVal                                                  0x0
#define cAf6_dej1_rx_framer_dkl_msg2_RxDE1DlkByte5_RstVal                                                  0x0

/*--------------------------------------
BitField Name: RxDE1DlkCache4
BitField Type: RW
BitField Desc:
BitField Bits: [10:8]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dkl_msg2_RxDE1DlkCache4_Bit_Start                                                8
#define cAf6_dej1_rx_framer_dkl_msg2_RxDE1DlkCache4_Bit_End                                                 10
#define cAf6_dej1_rx_framer_dkl_msg2_RxDE1DlkCache4_Mask                                              cBit10_8
#define cAf6_dej1_rx_framer_dkl_msg2_RxDE1DlkCache4_Shift                                                    8
#define cAf6_dej1_rx_framer_dkl_msg2_RxDE1DlkCache4_MaxVal                                                 0x7
#define cAf6_dej1_rx_framer_dkl_msg2_RxDE1DlkCache4_MinVal                                                 0x0
#define cAf6_dej1_rx_framer_dkl_msg2_RxDE1DlkCache4_RstVal                                                 0x0

/*--------------------------------------
BitField Name: RxDE1DlkByte4
BitField Type: RW
BitField Desc:
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dkl_msg2_RxDE1DlkByte4_Bit_Start                                                 0
#define cAf6_dej1_rx_framer_dkl_msg2_RxDE1DlkByte4_Bit_End                                                   7
#define cAf6_dej1_rx_framer_dkl_msg2_RxDE1DlkByte4_Mask                                                cBit7_0
#define cAf6_dej1_rx_framer_dkl_msg2_RxDE1DlkByte4_Shift                                                     0
#define cAf6_dej1_rx_framer_dkl_msg2_RxDE1DlkByte4_MaxVal                                                 0xff
#define cAf6_dej1_rx_framer_dkl_msg2_RxDE1DlkByte4_MinVal                                                  0x0
#define cAf6_dej1_rx_framer_dkl_msg2_RxDE1DlkByte4_RstVal                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer DLK Message3
Reg Addr   : 0x000D0019
Reg Formula: 
    Where  : 
Reg Desc   : 
.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rx_framer_dlk_msg3_Base                                                        0x000D0019
#define cAf6Reg_dej1_rx_framer_dlk_msg3                                                             0x000D0019
#define cAf6Reg_dej1_rx_framer_dlk_msg3_WidthVal                                                            32
#define cAf6Reg_dej1_rx_framer_dlk_msg3_WriteMask                                                          0x0

/*--------------------------------------
BitField Name: RxDE1DlkCache7
BitField Type: RW
BitField Desc:
BitField Bits: [26:24]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dlk_msg3_RxDE1DlkCache7_Bit_Start                                               24
#define cAf6_dej1_rx_framer_dlk_msg3_RxDE1DlkCache7_Bit_End                                                 26
#define cAf6_dej1_rx_framer_dlk_msg3_RxDE1DlkCache7_Mask                                             cBit26_24
#define cAf6_dej1_rx_framer_dlk_msg3_RxDE1DlkCache7_Shift                                                   24
#define cAf6_dej1_rx_framer_dlk_msg3_RxDE1DlkCache7_MaxVal                                                 0x7
#define cAf6_dej1_rx_framer_dlk_msg3_RxDE1DlkCache7_MinVal                                                 0x0
#define cAf6_dej1_rx_framer_dlk_msg3_RxDE1DlkCache7_RstVal                                                 0x0

/*--------------------------------------
BitField Name: RxDE1DlkByte7
BitField Type: RW
BitField Desc:
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dlk_msg3_RxDE1DlkByte7_Bit_Start                                                16
#define cAf6_dej1_rx_framer_dlk_msg3_RxDE1DlkByte7_Bit_End                                                  23
#define cAf6_dej1_rx_framer_dlk_msg3_RxDE1DlkByte7_Mask                                              cBit23_16
#define cAf6_dej1_rx_framer_dlk_msg3_RxDE1DlkByte7_Shift                                                    16
#define cAf6_dej1_rx_framer_dlk_msg3_RxDE1DlkByte7_MaxVal                                                 0xff
#define cAf6_dej1_rx_framer_dlk_msg3_RxDE1DlkByte7_MinVal                                                  0x0
#define cAf6_dej1_rx_framer_dlk_msg3_RxDE1DlkByte7_RstVal                                                  0x0

/*--------------------------------------
BitField Name: RxDE1DlkCache6
BitField Type: RW
BitField Desc:
BitField Bits: [10:8]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dlk_msg3_RxDE1DlkCache6_Bit_Start                                                8
#define cAf6_dej1_rx_framer_dlk_msg3_RxDE1DlkCache6_Bit_End                                                 10
#define cAf6_dej1_rx_framer_dlk_msg3_RxDE1DlkCache6_Mask                                              cBit10_8
#define cAf6_dej1_rx_framer_dlk_msg3_RxDE1DlkCache6_Shift                                                    8
#define cAf6_dej1_rx_framer_dlk_msg3_RxDE1DlkCache6_MaxVal                                                 0x7
#define cAf6_dej1_rx_framer_dlk_msg3_RxDE1DlkCache6_MinVal                                                 0x0
#define cAf6_dej1_rx_framer_dlk_msg3_RxDE1DlkCache6_RstVal                                                 0x0

/*--------------------------------------
BitField Name: RxDE1DlkByte6
BitField Type: RW
BitField Desc:
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dlk_msg3_RxDE1DlkByte6_Bit_Start                                                 0
#define cAf6_dej1_rx_framer_dlk_msg3_RxDE1DlkByte6_Bit_End                                                   7
#define cAf6_dej1_rx_framer_dlk_msg3_RxDE1DlkByte6_Mask                                                cBit7_0
#define cAf6_dej1_rx_framer_dlk_msg3_RxDE1DlkByte6_Shift                                                     0
#define cAf6_dej1_rx_framer_dlk_msg3_RxDE1DlkByte6_MaxVal                                                 0xff
#define cAf6_dej1_rx_framer_dlk_msg3_RxDE1DlkByte6_MinVal                                                  0x0
#define cAf6_dej1_rx_framer_dlk_msg3_RxDE1DlkByte6_RstVal                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer DLK Interrupt Enable
Reg Addr   : 0x000DE000
Reg Formula: 
    Where  : 
Reg Desc   : 
.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rx_framer_dlk_intr_en_ctrl_Base                                                0x000DE000
#define cAf6Reg_dej1_rx_framer_dlk_intr_en_ctrl                                                     0x000DE000
#define cAf6Reg_dej1_rx_framer_dlk_intr_en_ctrl_WidthVal                                                    32
#define cAf6Reg_dej1_rx_framer_dlk_intr_en_ctrl_WriteMask                                                  0x0

/*--------------------------------------
BitField Name: RxDE1DlkBOMEn
BitField Type: RW
BitField Desc: Set 1 if there is a the new BOM detected
BitField Bits: [4]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dlk_intr_en_ctrl_RxDE1DlkBOMEn_Bit_Start                                         4
#define cAf6_dej1_rx_framer_dlk_intr_en_ctrl_RxDE1DlkBOMEn_Bit_End                                           4
#define cAf6_dej1_rx_framer_dlk_intr_en_ctrl_RxDE1DlkBOMEn_Mask                                          cBit4
#define cAf6_dej1_rx_framer_dlk_intr_en_ctrl_RxDE1DlkBOMEn_Shift                                             4
#define cAf6_dej1_rx_framer_dlk_intr_en_ctrl_RxDE1DlkBOMEn_MaxVal                                          0x1
#define cAf6_dej1_rx_framer_dlk_intr_en_ctrl_RxDE1DlkBOMEn_MinVal                                          0x0
#define cAf6_dej1_rx_framer_dlk_intr_en_ctrl_RxDE1DlkBOMEn_RstVal                                          0x0

/*--------------------------------------
BitField Name: RxDE1DlkCacheErrEn
BitField Type: RW
BitField Desc:
BitField Bits: [3]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dlk_intr_en_ctrl_RxDE1DlkCacheErrEn_Bit_Start                                       3
#define cAf6_dej1_rx_framer_dlk_intr_en_ctrl_RxDE1DlkCacheErrEn_Bit_End                                       3
#define cAf6_dej1_rx_framer_dlk_intr_en_ctrl_RxDE1DlkCacheErrEn_Mask                                     cBit3
#define cAf6_dej1_rx_framer_dlk_intr_en_ctrl_RxDE1DlkCacheErrEn_Shift                                        3
#define cAf6_dej1_rx_framer_dlk_intr_en_ctrl_RxDE1DlkCacheErrEn_MaxVal                                     0x1
#define cAf6_dej1_rx_framer_dlk_intr_en_ctrl_RxDE1DlkCacheErrEn_MinVal                                     0x0
#define cAf6_dej1_rx_framer_dlk_intr_en_ctrl_RxDE1DlkCacheErrEn_RstVal                                     0x0

/*--------------------------------------
BitField Name: RxDE1DlkAbortEn
BitField Type: RW
BitField Desc:
BitField Bits: [2]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dlk_intr_en_ctrl_RxDE1DlkAbortEn_Bit_Start                                       2
#define cAf6_dej1_rx_framer_dlk_intr_en_ctrl_RxDE1DlkAbortEn_Bit_End                                         2
#define cAf6_dej1_rx_framer_dlk_intr_en_ctrl_RxDE1DlkAbortEn_Mask                                        cBit2
#define cAf6_dej1_rx_framer_dlk_intr_en_ctrl_RxDE1DlkAbortEn_Shift                                           2
#define cAf6_dej1_rx_framer_dlk_intr_en_ctrl_RxDE1DlkAbortEn_MaxVal                                        0x1
#define cAf6_dej1_rx_framer_dlk_intr_en_ctrl_RxDE1DlkAbortEn_MinVal                                        0x0
#define cAf6_dej1_rx_framer_dlk_intr_en_ctrl_RxDE1DlkAbortEn_RstVal                                        0x0

/*--------------------------------------
BitField Name: RxDE1DlkOverThresEn
BitField Type: RW
BitField Desc:
BitField Bits: [1]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dlk_intr_en_ctrl_RxDE1DlkOverThresEn_Bit_Start                                       1
#define cAf6_dej1_rx_framer_dlk_intr_en_ctrl_RxDE1DlkOverThresEn_Bit_End                                       1
#define cAf6_dej1_rx_framer_dlk_intr_en_ctrl_RxDE1DlkOverThresEn_Mask                                    cBit1
#define cAf6_dej1_rx_framer_dlk_intr_en_ctrl_RxDE1DlkOverThresEn_Shift                                       1
#define cAf6_dej1_rx_framer_dlk_intr_en_ctrl_RxDE1DlkOverThresEn_MaxVal                                     0x1
#define cAf6_dej1_rx_framer_dlk_intr_en_ctrl_RxDE1DlkOverThresEn_MinVal                                     0x0
#define cAf6_dej1_rx_framer_dlk_intr_en_ctrl_RxDE1DlkOverThresEn_RstVal                                     0x0

/*--------------------------------------
BitField Name: RxDE1DlkNewMsgEn
BitField Type: RW
BitField Desc:
BitField Bits: [0]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dlk_intr_en_ctrl_RxDE1DlkNewMsgEn_Bit_Start                                       0
#define cAf6_dej1_rx_framer_dlk_intr_en_ctrl_RxDE1DlkNewMsgEn_Bit_End                                        0
#define cAf6_dej1_rx_framer_dlk_intr_en_ctrl_RxDE1DlkNewMsgEn_Mask                                       cBit0
#define cAf6_dej1_rx_framer_dlk_intr_en_ctrl_RxDE1DlkNewMsgEn_Shift                                          0
#define cAf6_dej1_rx_framer_dlk_intr_en_ctrl_RxDE1DlkNewMsgEn_MaxVal                                       0x1
#define cAf6_dej1_rx_framer_dlk_intr_en_ctrl_RxDE1DlkNewMsgEn_MinVal                                       0x0
#define cAf6_dej1_rx_framer_dlk_intr_en_ctrl_RxDE1DlkNewMsgEn_RstVal                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer DLK Interrupt Sticky
Reg Addr   : 0x000DE400
Reg Formula: 
    Where  : 
Reg Desc   : 
.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rx_framer_dlk_intr_sticky_Base                                                 0x000DE400
#define cAf6Reg_dej1_rx_framer_dlk_intr_sticky                                                      0x000DE400
#define cAf6Reg_dej1_rx_framer_dlk_intr_sticky_WidthVal                                                     32
#define cAf6Reg_dej1_rx_framer_dlk_intr_sticky_WriteMask                                                   0x0

/*--------------------------------------
BitField Name: RxDE1DlkBOMIntr
BitField Type: RW
BitField Desc: Set 1 if there is a the new BOM detected
BitField Bits: [4]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dlk_intr_sticky_RxDE1DlkBOMIntr_Bit_Start                                        4
#define cAf6_dej1_rx_framer_dlk_intr_sticky_RxDE1DlkBOMIntr_Bit_End                                          4
#define cAf6_dej1_rx_framer_dlk_intr_sticky_RxDE1DlkBOMIntr_Mask                                         cBit4
#define cAf6_dej1_rx_framer_dlk_intr_sticky_RxDE1DlkBOMIntr_Shift                                            4
#define cAf6_dej1_rx_framer_dlk_intr_sticky_RxDE1DlkBOMIntr_MaxVal                                         0x1
#define cAf6_dej1_rx_framer_dlk_intr_sticky_RxDE1DlkBOMIntr_MinVal                                         0x0
#define cAf6_dej1_rx_framer_dlk_intr_sticky_RxDE1DlkBOMIntr_RstVal                                         0x0

/*--------------------------------------
BitField Name: RxDE1DlkCacheErrIntr
BitField Type: RW
BitField Desc: Set 1 if there is a the new Cache Error detected
BitField Bits: [3]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dlk_intr_sticky_RxDE1DlkCacheErrIntr_Bit_Start                                       3
#define cAf6_dej1_rx_framer_dlk_intr_sticky_RxDE1DlkCacheErrIntr_Bit_End                                       3
#define cAf6_dej1_rx_framer_dlk_intr_sticky_RxDE1DlkCacheErrIntr_Mask                                    cBit3
#define cAf6_dej1_rx_framer_dlk_intr_sticky_RxDE1DlkCacheErrIntr_Shift                                       3
#define cAf6_dej1_rx_framer_dlk_intr_sticky_RxDE1DlkCacheErrIntr_MaxVal                                     0x1
#define cAf6_dej1_rx_framer_dlk_intr_sticky_RxDE1DlkCacheErrIntr_MinVal                                     0x0
#define cAf6_dej1_rx_framer_dlk_intr_sticky_RxDE1DlkCacheErrIntr_RstVal                                     0x0

/*--------------------------------------
BitField Name: RxDE1DlkAbortIntr
BitField Type: RW
BitField Desc: Set 1 if there is a the new Abort detected
BitField Bits: [2]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dlk_intr_sticky_RxDE1DlkAbortIntr_Bit_Start                                       2
#define cAf6_dej1_rx_framer_dlk_intr_sticky_RxDE1DlkAbortIntr_Bit_End                                        2
#define cAf6_dej1_rx_framer_dlk_intr_sticky_RxDE1DlkAbortIntr_Mask                                       cBit2
#define cAf6_dej1_rx_framer_dlk_intr_sticky_RxDE1DlkAbortIntr_Shift                                          2
#define cAf6_dej1_rx_framer_dlk_intr_sticky_RxDE1DlkAbortIntr_MaxVal                                       0x1
#define cAf6_dej1_rx_framer_dlk_intr_sticky_RxDE1DlkAbortIntr_MinVal                                       0x0
#define cAf6_dej1_rx_framer_dlk_intr_sticky_RxDE1DlkAbortIntr_RstVal                                       0x0

/*--------------------------------------
BitField Name: RxDE1DlkOverThresIntr
BitField Type: RW
BitField Desc: Set 1 if there is a the new Over Threshold detected
BitField Bits: [1]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dlk_intr_sticky_RxDE1DlkOverThresIntr_Bit_Start                                       1
#define cAf6_dej1_rx_framer_dlk_intr_sticky_RxDE1DlkOverThresIntr_Bit_End                                       1
#define cAf6_dej1_rx_framer_dlk_intr_sticky_RxDE1DlkOverThresIntr_Mask                                   cBit1
#define cAf6_dej1_rx_framer_dlk_intr_sticky_RxDE1DlkOverThresIntr_Shift                                       1
#define cAf6_dej1_rx_framer_dlk_intr_sticky_RxDE1DlkOverThresIntr_MaxVal                                     0x1
#define cAf6_dej1_rx_framer_dlk_intr_sticky_RxDE1DlkOverThresIntr_MinVal                                     0x0
#define cAf6_dej1_rx_framer_dlk_intr_sticky_RxDE1DlkOverThresIntr_RstVal                                     0x0

/*--------------------------------------
BitField Name: RxDE1DlkNewMsgIntr
BitField Type: RW
BitField Desc: Set 1 if there is a the new message detected
BitField Bits: [0]
--------------------------------------*/
#define cAf6_dej1_rx_framer_dlk_intr_sticky_RxDE1DlkNewMsgIntr_Bit_Start                                       0
#define cAf6_dej1_rx_framer_dlk_intr_sticky_RxDE1DlkNewMsgIntr_Bit_End                                       0
#define cAf6_dej1_rx_framer_dlk_intr_sticky_RxDE1DlkNewMsgIntr_Mask                                      cBit0
#define cAf6_dej1_rx_framer_dlk_intr_sticky_RxDE1DlkNewMsgIntr_Shift                                         0
#define cAf6_dej1_rx_framer_dlk_intr_sticky_RxDE1DlkNewMsgIntr_MaxVal                                      0x1
#define cAf6_dej1_rx_framer_dlk_intr_sticky_RxDE1DlkNewMsgIntr_MinVal                                      0x0
#define cAf6_dej1_rx_framer_dlk_intr_sticky_RxDE1DlkNewMsgIntr_RstVal                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Test Pattern Monitoring Global Control
Reg Addr   : 0x054900 - 0x054900
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to global configuration for PTM block.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_testpat_mon_glb_ctrl_Base                                                        0x054900
#define cAf6Reg_dej1_testpat_mon_glb_ctrl                                                             0x054900
#define cAf6Reg_dej1_testpat_mon_glb_ctrl_WidthVal                                                          32
#define cAf6Reg_dej1_testpat_mon_glb_ctrl_WriteMask                                                        0x0

/*--------------------------------------
BitField Name: RxDE3PtmLosThr
BitField Type: RW
BitField Desc: Threshold for Loss Of Sync detection. Status will go LOS if the
number of error patterns (8 bits) in 16 consecutive patterns is more than or
equal the RxDE3PtmLosThr.
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_dej1_testpat_mon_glb_ctrl_RxDE3PtmLosThr_Bit_Start                                              8
#define cAf6_dej1_testpat_mon_glb_ctrl_RxDE3PtmLosThr_Bit_End                                               11
#define cAf6_dej1_testpat_mon_glb_ctrl_RxDE3PtmLosThr_Mask                                            cBit11_8
#define cAf6_dej1_testpat_mon_glb_ctrl_RxDE3PtmLosThr_Shift                                                  8
#define cAf6_dej1_testpat_mon_glb_ctrl_RxDE3PtmLosThr_MaxVal                                               0xf
#define cAf6_dej1_testpat_mon_glb_ctrl_RxDE3PtmLosThr_MinVal                                               0x0
#define cAf6_dej1_testpat_mon_glb_ctrl_RxDE3PtmLosThr_RstVal                                               0x0

/*--------------------------------------
BitField Name: RxDE3PtmSynThr
BitField Type: RW
BitField Desc: Threshold for Sync detection. Status will go SYN if the number of
corrected consecutive patterns (8 bits) is more than or equal the
RxDE3PtmLosThr.
BitField Bits: [7:4]
--------------------------------------*/
#define cAf6_dej1_testpat_mon_glb_ctrl_RxDE3PtmSynThr_Bit_Start                                              4
#define cAf6_dej1_testpat_mon_glb_ctrl_RxDE3PtmSynThr_Bit_End                                                7
#define cAf6_dej1_testpat_mon_glb_ctrl_RxDE3PtmSynThr_Mask                                             cBit7_4
#define cAf6_dej1_testpat_mon_glb_ctrl_RxDE3PtmSynThr_Shift                                                  4
#define cAf6_dej1_testpat_mon_glb_ctrl_RxDE3PtmSynThr_MaxVal                                               0xf
#define cAf6_dej1_testpat_mon_glb_ctrl_RxDE3PtmSynThr_MinVal                                               0x0
#define cAf6_dej1_testpat_mon_glb_ctrl_RxDE3PtmSynThr_RstVal                                               0x0

/*--------------------------------------
BitField Name: RxDE1PtmSwap
BitField Type: RW
BitField Desc: PTM swap Bit. If it is set to 1, pattern is shifted LSB first,
then MSB. Normal is set to 0.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_dej1_testpat_mon_glb_ctrl_RxDE1PtmSwap_Bit_Start                                                1
#define cAf6_dej1_testpat_mon_glb_ctrl_RxDE1PtmSwap_Bit_End                                                  1
#define cAf6_dej1_testpat_mon_glb_ctrl_RxDE1PtmSwap_Mask                                                 cBit1
#define cAf6_dej1_testpat_mon_glb_ctrl_RxDE1PtmSwap_Shift                                                    1
#define cAf6_dej1_testpat_mon_glb_ctrl_RxDE1PtmSwap_MaxVal                                                 0x1
#define cAf6_dej1_testpat_mon_glb_ctrl_RxDE1PtmSwap_MinVal                                                 0x0
#define cAf6_dej1_testpat_mon_glb_ctrl_RxDE1PtmSwap_RstVal                                                 0x0

/*--------------------------------------
BitField Name: RxDE1PtmInv
BitField Type: RW
BitField Desc: PTM inversion Bit.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_dej1_testpat_mon_glb_ctrl_RxDE1PtmInv_Bit_Start                                                 0
#define cAf6_dej1_testpat_mon_glb_ctrl_RxDE1PtmInv_Bit_End                                                   0
#define cAf6_dej1_testpat_mon_glb_ctrl_RxDE1PtmInv_Mask                                                  cBit0
#define cAf6_dej1_testpat_mon_glb_ctrl_RxDE1PtmInv_Shift                                                     0
#define cAf6_dej1_testpat_mon_glb_ctrl_RxDE1PtmInv_MaxVal                                                  0x1
#define cAf6_dej1_testpat_mon_glb_ctrl_RxDE1PtmInv_MinVal                                                  0x0
#define cAf6_dej1_testpat_mon_glb_ctrl_RxDE1PtmInv_RstVal                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Test Pattern Monitoring Sticky Enable
Reg Addr   : 0x054901 - 0x054901
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to configure the test pattern sticky mode

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_testpat_mon_sticky_en_Base                                                       0x054901
#define cAf6Reg_dej1_testpat_mon_sticky_en                                                            0x054901
#define cAf6Reg_dej1_testpat_mon_sticky_en_WidthVal                                                         32
#define cAf6Reg_dej1_testpat_mon_sticky_en_WriteMask                                                       0x0

/*--------------------------------------
BitField Name: RxDE1PtmStkEn
BitField Type: RW
BitField Desc: Test Pattern Error Sticky Control
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_dej1_testpat_mon_sticky_en_RxDE1PtmStkEn_Bit_Start                                              0
#define cAf6_dej1_testpat_mon_sticky_en_RxDE1PtmStkEn_Bit_End                                               15
#define cAf6_dej1_testpat_mon_sticky_en_RxDE1PtmStkEn_Mask                                            cBit15_0
#define cAf6_dej1_testpat_mon_sticky_en_RxDE1PtmStkEn_Shift                                                  0
#define cAf6_dej1_testpat_mon_sticky_en_RxDE1PtmStkEn_MaxVal                                            0xffff
#define cAf6_dej1_testpat_mon_sticky_en_RxDE1PtmStkEn_MinVal                                               0x0
#define cAf6_dej1_testpat_mon_sticky_en_RxDE1PtmStkEn_RstVal                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Test Pattern Monitoring Sticky
Reg Addr   : 0x054902 - 0x054902
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to be tus sticky report of 4 PTM engines.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_testpat_mon_sticky_Base                                                          0x054902
#define cAf6Reg_dej1_testpat_mon_sticky                                                               0x054902
#define cAf6Reg_dej1_testpat_mon_sticky_WidthVal                                                            32
#define cAf6Reg_dej1_testpat_mon_sticky_WriteMask                                                          0x0

/*--------------------------------------
BitField Name: RxDE1PtmStk
BitField Type: RW
BitField Desc: BERT Error Sticky
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_dej1_testpat_mon_sticky_RxDE1PtmStk_Bit_Start                                                   0
#define cAf6_dej1_testpat_mon_sticky_RxDE1PtmStk_Bit_End                                                    15
#define cAf6_dej1_testpat_mon_sticky_RxDE1PtmStk_Mask                                                 cBit15_0
#define cAf6_dej1_testpat_mon_sticky_RxDE1PtmStk_Shift                                                       0
#define cAf6_dej1_testpat_mon_sticky_RxDE1PtmStk_MaxVal                                                 0xffff
#define cAf6_dej1_testpat_mon_sticky_RxDE1PtmStk_MinVal                                                    0x0
#define cAf6_dej1_testpat_mon_sticky_RxDE1PtmStk_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Test Pattern Monitoring Mode
Reg Addr   : 0x054910 - 0x05491F
Reg Formula: 0x054910 +  index
    Where  : 
           + $index(0-15):
Reg Desc   : 
This register is used to data mode for PTM block.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_testpat_mon_mode_Base                                                            0x054910
#define cAf6Reg_dej1_testpat_mon_mode(index)                                                (0x054910+(index))
#define cAf6Reg_dej1_testpat_mon_mode_WidthVal                                                              32
#define cAf6Reg_dej1_testpat_mon_mode_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: RxDE1PtmPatBit
BitField Type: RW
BitField Desc: The number of bit pattern is used.
BitField Bits: [9:5]
--------------------------------------*/
#define cAf6_dej1_testpat_mon_mode_RxDE1PtmPatBit_Bit_Start                                                  5
#define cAf6_dej1_testpat_mon_mode_RxDE1PtmPatBit_Bit_End                                                    9
#define cAf6_dej1_testpat_mon_mode_RxDE1PtmPatBit_Mask                                                 cBit9_5
#define cAf6_dej1_testpat_mon_mode_RxDE1PtmPatBit_Shift                                                      5
#define cAf6_dej1_testpat_mon_mode_RxDE1PtmPatBit_MaxVal                                                  0x1f
#define cAf6_dej1_testpat_mon_mode_RxDE1PtmPatBit_MinVal                                                   0x0
#define cAf6_dej1_testpat_mon_mode_RxDE1PtmPatBit_RstVal                                                   0x0

/*--------------------------------------
BitField Name: RxDE1PtmMode
BitField Type: RW
BitField Desc: Receive DS1/E1/J1 BERT mode 00000: Prbs9 00001: Prbs11 00010:
Prbs15 00011: Prbs20 (X19 + X2 + 1) 00100: Prbs20 (X19 + X16 + 1) 00101: Qrss20
(X19 + X16 + 1) 00110: Prbs23 00111: DDS1 01000: DDS2 01001: DDS3 01010: DDS4
01011: DDS5 01100: Daly55 01101: Fix 3 in 24 01110: Fix 1 in 8 01111: Octet55
10000: Fix pattern n-bit 10001: Sequence 10010: Prbs7
BitField Bits: [4:0]
--------------------------------------*/
#define cAf6_dej1_testpat_mon_mode_RxDE1PtmMode_Bit_Start                                                    0
#define cAf6_dej1_testpat_mon_mode_RxDE1PtmMode_Bit_End                                                      4
#define cAf6_dej1_testpat_mon_mode_RxDE1PtmMode_Mask                                                   cBit4_0
#define cAf6_dej1_testpat_mon_mode_RxDE1PtmMode_Shift                                                        0
#define cAf6_dej1_testpat_mon_mode_RxDE1PtmMode_MaxVal                                                    0x1f
#define cAf6_dej1_testpat_mon_mode_RxDE1PtmMode_MinVal                                                     0x0
#define cAf6_dej1_testpat_mon_mode_RxDE1PtmMode_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Test Pattern Monitoring Fixed Pattern
Reg Addr   : 0x054920 - 0x05492F
Reg Formula: 0x054920 +  index
    Where  : 
           + $index(0-15):
Reg Desc   : 
This register is used to be expected data in fix pattern mode for engine #0 - 15

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_testpat_mon_fixed_pattern_cfg_Base                                               0x054920
#define cAf6Reg_dej1_testpat_mon_fixed_pattern_cfg(index)                                   (0x054920+(index))
#define cAf6Reg_dej1_testpat_mon_fixed_pattern_cfg_WidthVal                                                 32
#define cAf6Reg_dej1_testpat_mon_fixed_pattern_cfg_WriteMask                                               0x0

/*--------------------------------------
BitField Name: RxDE1FixPat
BitField Type: RW
BitField Desc: Configurable fixed pattern for BERT monitoring.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_dej1_testpat_mon_fixed_pattern_cfg_RxDE1FixPat_Bit_Start                                        0
#define cAf6_dej1_testpat_mon_fixed_pattern_cfg_RxDE1FixPat_Bit_End                                         31
#define cAf6_dej1_testpat_mon_fixed_pattern_cfg_RxDE1FixPat_Mask                                      cBit31_0
#define cAf6_dej1_testpat_mon_fixed_pattern_cfg_RxDE1FixPat_Shift                                            0
#define cAf6_dej1_testpat_mon_fixed_pattern_cfg_RxDE1FixPat_MaxVal                                  0xffffffff
#define cAf6_dej1_testpat_mon_fixed_pattern_cfg_RxDE1FixPat_MinVal                                         0x0
#define cAf6_dej1_testpat_mon_fixed_pattern_cfg_RxDE1FixPat_RstVal                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Test Pattern Monitoring Selected Channel
Reg Addr   : 0x054800 - 0x05480F
Reg Formula: 0x054800 +  index
    Where  : 
           + $index(0-15):
Reg Desc   : 
This register is used to control configuration for PTM engine #0 - 15

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_testpat_mon_sel_chn_cfg_Base                                                     0x054800
#define cAf6Reg_dej1_testpat_mon_sel_chn_cfg(index)                                         (0x054800+(index))
#define cAf6Reg_dej1_testpat_mon_sel_chn_cfg_WidthVal                                                       32
#define cAf6Reg_dej1_testpat_mon_sel_chn_cfg_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: RxDE1PtgEn
BitField Type: RW
BitField Desc: PTG Bit Enable.
BitField Bits: [9]
--------------------------------------*/
#define cAf6_dej1_testpat_mon_sel_chn_cfg_RxDE1PtgEn_Bit_Start                                               9
#define cAf6_dej1_testpat_mon_sel_chn_cfg_RxDE1PtgEn_Bit_End                                                 9
#define cAf6_dej1_testpat_mon_sel_chn_cfg_RxDE1PtgEn_Mask                                                cBit9
#define cAf6_dej1_testpat_mon_sel_chn_cfg_RxDE1PtgEn_Shift                                                   9
#define cAf6_dej1_testpat_mon_sel_chn_cfg_RxDE1PtgEn_MaxVal                                                0x1
#define cAf6_dej1_testpat_mon_sel_chn_cfg_RxDE1PtgEn_MinVal                                                0x0
#define cAf6_dej1_testpat_mon_sel_chn_cfg_RxDE1PtgEn_RstVal                                                0x0

/*--------------------------------------
BitField Name: RxDE1PtmID
BitField Type: RW
BitField Desc: Configure which channel in 336 DS1s/E1s/J1s is monitored.
BitField Bits: [8:0]
--------------------------------------*/
#define cAf6_dej1_testpat_mon_sel_chn_cfg_RxDE1PtmID_Bit_Start                                               0
#define cAf6_dej1_testpat_mon_sel_chn_cfg_RxDE1PtmID_Bit_End                                                 8
#define cAf6_dej1_testpat_mon_sel_chn_cfg_RxDE1PtmID_Mask                                              cBit8_0
#define cAf6_dej1_testpat_mon_sel_chn_cfg_RxDE1PtmID_Shift                                                   0
#define cAf6_dej1_testpat_mon_sel_chn_cfg_RxDE1PtmID_MaxVal                                              0x1ff
#define cAf6_dej1_testpat_mon_sel_chn_cfg_RxDE1PtmID_MinVal                                                0x0
#define cAf6_dej1_testpat_mon_sel_chn_cfg_RxDE1PtmID_RstVal                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Test Pattern Monitoring nxDS0 Control
Reg Addr   : 0x054810 - 0x05481F
Reg Formula: 0x054810 +  index
    Where  : 
           + $index(0-15):
Reg Desc   : 
These registers are used to control the mode of nxDS0 in a DS1/E1/J1

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_testpat_mon_nxds0_ctrl_Base                                                      0x054810
#define cAf6Reg_dej1_testpat_mon_nxds0_ctrl(index)                                          (0x054810+(index))
#define cAf6Reg_dej1_testpat_mon_nxds0_ctrl_WidthVal                                                        32
#define cAf6Reg_dej1_testpat_mon_nxds0_ctrl_WriteMask                                                      0x0

/*--------------------------------------
BitField Name: RxDE1PtmFbitOVR
BitField Type: RW
BitField Desc: F- bit overwrite
BitField Bits: [2]
--------------------------------------*/
#define cAf6_dej1_testpat_mon_nxds0_ctrl_RxDE1PtmFbitOVR_Bit_Start                                           2
#define cAf6_dej1_testpat_mon_nxds0_ctrl_RxDE1PtmFbitOVR_Bit_End                                             2
#define cAf6_dej1_testpat_mon_nxds0_ctrl_RxDE1PtmFbitOVR_Mask                                            cBit2
#define cAf6_dej1_testpat_mon_nxds0_ctrl_RxDE1PtmFbitOVR_Shift                                               2
#define cAf6_dej1_testpat_mon_nxds0_ctrl_RxDE1PtmFbitOVR_MaxVal                                            0x1
#define cAf6_dej1_testpat_mon_nxds0_ctrl_RxDE1PtmFbitOVR_MinVal                                            0x0
#define cAf6_dej1_testpat_mon_nxds0_ctrl_RxDE1PtmFbitOVR_RstVal                                            0x0

/*--------------------------------------
BitField Name: RxDE1PtmDS0_6b
BitField Type: RW
BitField Desc: Receive Test Pattern from the 2nd bit to the 8th bit
BitField Bits: [1]
--------------------------------------*/
#define cAf6_dej1_testpat_mon_nxds0_ctrl_RxDE1PtmDS0_6b_Bit_Start                                            1
#define cAf6_dej1_testpat_mon_nxds0_ctrl_RxDE1PtmDS0_6b_Bit_End                                              1
#define cAf6_dej1_testpat_mon_nxds0_ctrl_RxDE1PtmDS0_6b_Mask                                             cBit1
#define cAf6_dej1_testpat_mon_nxds0_ctrl_RxDE1PtmDS0_6b_Shift                                                1
#define cAf6_dej1_testpat_mon_nxds0_ctrl_RxDE1PtmDS0_6b_MaxVal                                             0x1
#define cAf6_dej1_testpat_mon_nxds0_ctrl_RxDE1PtmDS0_6b_MinVal                                             0x0
#define cAf6_dej1_testpat_mon_nxds0_ctrl_RxDE1PtmDS0_6b_RstVal                                             0x0

/*--------------------------------------
BitField Name: RxDE1PtmDS0_7b
BitField Type: RW
BitField Desc: Receive Test Pattern from the 2nd bit to the 7h bit. If DS0_7b
and DS0_6b are not active, engine will receive test pattern from the 1st bit to
the 8th bit
BitField Bits: [0]
--------------------------------------*/
#define cAf6_dej1_testpat_mon_nxds0_ctrl_RxDE1PtmDS0_7b_Bit_Start                                            0
#define cAf6_dej1_testpat_mon_nxds0_ctrl_RxDE1PtmDS0_7b_Bit_End                                              0
#define cAf6_dej1_testpat_mon_nxds0_ctrl_RxDE1PtmDS0_7b_Mask                                             cBit0
#define cAf6_dej1_testpat_mon_nxds0_ctrl_RxDE1PtmDS0_7b_Shift                                                0
#define cAf6_dej1_testpat_mon_nxds0_ctrl_RxDE1PtmDS0_7b_MaxVal                                             0x1
#define cAf6_dej1_testpat_mon_nxds0_ctrl_RxDE1PtmDS0_7b_MinVal                                             0x0
#define cAf6_dej1_testpat_mon_nxds0_ctrl_RxDE1PtmDS0_7b_RstVal                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Test Pattern Monitoring nxDS0 Concatenation Control
Reg Addr   : 0x054820 - 0x05482F
Reg Formula: 0x054820 +  index
    Where  : 
           + $index(0-15):
Reg Desc   : 
This register is used to be active DSO for PTG engine: 0-3. A 32 bits register is dedicated for 32 timeslot in case of frame E1, and 24 bits (bit 24 - 1) is given for 24 timeslot in case of frame DS1/J1. The 32-bits are set 1 to indicate full channel extraction.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_testpat_mon_nxds0_conc_ctrl_Base                                                 0x054820
#define cAf6Reg_dej1_testpat_mon_nxds0_conc_ctrl(index)                                     (0x054820+(index))
#define cAf6Reg_dej1_testpat_mon_nxds0_conc_ctrl_WidthVal                                                   32
#define cAf6Reg_dej1_testpat_mon_nxds0_conc_ctrl_WriteMask                                                 0x0

/*--------------------------------------
BitField Name: RxDE1PtgDS0ConCtrl
BitField Type: RW
BitField Desc: Per time slot receive DS1/E1/J1 Concatenation enable.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_dej1_testpat_mon_nxds0_conc_ctrl_RxDE1PtgDS0ConCtrl_Bit_Start                                       0
#define cAf6_dej1_testpat_mon_nxds0_conc_ctrl_RxDE1PtgDS0ConCtrl_Bit_End                                      31
#define cAf6_dej1_testpat_mon_nxds0_conc_ctrl_RxDE1PtgDS0ConCtrl_Mask                                 cBit31_0
#define cAf6_dej1_testpat_mon_nxds0_conc_ctrl_RxDE1PtgDS0ConCtrl_Shift                                       0
#define cAf6_dej1_testpat_mon_nxds0_conc_ctrl_RxDE1PtgDS0ConCtrl_MaxVal                              0xffffffff
#define cAf6_dej1_testpat_mon_nxds0_conc_ctrl_RxDE1PtgDS0ConCtrl_MinVal                                     0x0
#define cAf6_dej1_testpat_mon_nxds0_conc_ctrl_RxDE1PtgDS0ConCtrl_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Test Pattern Monitoring Error Counter
Reg Addr   : 0x054960 - 0x05497F
Reg Formula: 0x054960 +  index
    Where  : 
           + $index(0-15):
Reg Desc   : 
This is the error counter report of PTM engine #0 - 15 during monitor period.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_testpat_mon_err_cnt_Base                                                         0x054960
#define cAf6Reg_dej1_testpat_mon_err_cnt(index)                                             (0x054960+(index))
#define cAf6Reg_dej1_testpat_mon_err_cnt_WidthVal                                                           32
#define cAf6Reg_dej1_testpat_mon_err_cnt_WriteMask                                                         0x0

/*--------------------------------------
BitField Name: RxDE1PtmErrCnt
BitField Type: RW
BitField Desc: Error counter for BERT monitoring.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_dej1_testpat_mon_err_cnt_RxDE1PtmErrCnt_Bit_Start                                               0
#define cAf6_dej1_testpat_mon_err_cnt_RxDE1PtmErrCnt_Bit_End                                                15
#define cAf6_dej1_testpat_mon_err_cnt_RxDE1PtmErrCnt_Mask                                             cBit15_0
#define cAf6_dej1_testpat_mon_err_cnt_RxDE1PtmErrCnt_Shift                                                   0
#define cAf6_dej1_testpat_mon_err_cnt_RxDE1PtmErrCnt_MaxVal                                             0xffff
#define cAf6_dej1_testpat_mon_err_cnt_RxDE1PtmErrCnt_MinVal                                                0x0
#define cAf6_dej1_testpat_mon_err_cnt_RxDE1PtmErrCnt_RstVal                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Test Pattern Monitoring Good Bit Counter
Reg Addr   : 0x054980 - 0x05499F
Reg Formula: 0x054980 +  index
    Where  : 
           + $index(0-15):
Reg Desc   : 
This is the good counter report of PTM engine #0 - 15 during monitor period.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_testpat_mon_goodbit_cnt_Base                                                     0x054980
#define cAf6Reg_dej1_testpat_mon_goodbit_cnt(index)                                         (0x054980+(index))
#define cAf6Reg_dej1_testpat_mon_goodbit_cnt_WidthVal                                                       32
#define cAf6Reg_dej1_testpat_mon_goodbit_cnt_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: RxDE1PtmGoodCnt
BitField Type: RW
BitField Desc: Good counter for BERT monitoring.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_dej1_testpat_mon_goodbit_cnt_RxDE1PtmGoodCnt_Bit_Start                                          0
#define cAf6_dej1_testpat_mon_goodbit_cnt_RxDE1PtmGoodCnt_Bit_End                                           15
#define cAf6_dej1_testpat_mon_goodbit_cnt_RxDE1PtmGoodCnt_Mask                                        cBit15_0
#define cAf6_dej1_testpat_mon_goodbit_cnt_RxDE1PtmGoodCnt_Shift                                              0
#define cAf6_dej1_testpat_mon_goodbit_cnt_RxDE1PtmGoodCnt_MaxVal                                        0xffff
#define cAf6_dej1_testpat_mon_goodbit_cnt_RxDE1PtmGoodCnt_MinVal                                           0x0
#define cAf6_dej1_testpat_mon_goodbit_cnt_RxDE1PtmGoodCnt_RstVal                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Test Pattern Monitoring Loss Bit Counter
Reg Addr   : 0x0549A0 - 0x0549BF
Reg Formula: 0x0549A0 +  index
    Where  : 
           + $index(0-23):
Reg Desc   : 
This is the loss counter report of PTM engine #0 - 16 during monitor period.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_testpat_mon_lossbit_cnt_Base                                                     0x0549A0
#define cAf6Reg_dej1_testpat_mon_lossbit_cnt(index)                                         (0x0549A0+(index))
#define cAf6Reg_dej1_testpat_mon_lossbit_cnt_WidthVal                                                       32
#define cAf6Reg_dej1_testpat_mon_lossbit_cnt_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: RxDE1PtmLossCnt
BitField Type: RW
BitField Desc: Loss counter for BERT monitoring.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_dej1_testpat_mon_lossbit_cnt_RxDE1PtmLossCnt_Bit_Start                                          0
#define cAf6_dej1_testpat_mon_lossbit_cnt_RxDE1PtmLossCnt_Bit_End                                           15
#define cAf6_dej1_testpat_mon_lossbit_cnt_RxDE1PtmLossCnt_Mask                                        cBit15_0
#define cAf6_dej1_testpat_mon_lossbit_cnt_RxDE1PtmLossCnt_Shift                                              0
#define cAf6_dej1_testpat_mon_lossbit_cnt_RxDE1PtmLossCnt_MaxVal                                        0xffff
#define cAf6_dej1_testpat_mon_lossbit_cnt_RxDE1PtmLossCnt_MinVal                                           0x0
#define cAf6_dej1_testpat_mon_lossbit_cnt_RxDE1PtmLossCnt_RstVal                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Test Pattern Monitoring Counter Load ID
Reg Addr   : 0x054903 - 0x054903
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to load the error/good/loss counter report of PTM engine: 0-15 which the channel ID is value of register.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_testpat_mon_cntid_cfg_Base                                                       0x054903
#define cAf6Reg_dej1_testpat_mon_cntid_cfg                                                            0x054903
#define cAf6Reg_dej1_testpat_mon_cntid_cfg_WidthVal                                                         32
#define cAf6Reg_dej1_testpat_mon_cntid_cfg_WriteMask                                                       0x0

/*--------------------------------------
BitField Name: RxDE1PtmLoadID
BitField Type: RW
BitField Desc: Load ID counter.
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_dej1_testpat_mon_cntid_cfg_RxDE1PtmLoadID_Bit_Start                                             0
#define cAf6_dej1_testpat_mon_cntid_cfg_RxDE1PtmLoadID_Bit_End                                               3
#define cAf6_dej1_testpat_mon_cntid_cfg_RxDE1PtmLoadID_Mask                                            cBit3_0
#define cAf6_dej1_testpat_mon_cntid_cfg_RxDE1PtmLoadID_Shift                                                 0
#define cAf6_dej1_testpat_mon_cntid_cfg_RxDE1PtmLoadID_MaxVal                                              0xf
#define cAf6_dej1_testpat_mon_cntid_cfg_RxDE1PtmLoadID_MinVal                                              0x0
#define cAf6_dej1_testpat_mon_cntid_cfg_RxDE1PtmLoadID_RstVal                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Test Pattern Monitoring Error-Bit Counter Loading
Reg Addr   : 0x054904 - 0x649B04
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the loaded error counter report of PTM.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_testpat_mon_errbit_cnt_Base                                                      0x054904
#define cAf6Reg_dej1_testpat_mon_errbit_cnt                                                           0x054904
#define cAf6Reg_dej1_testpat_mon_errbit_cnt_WidthVal                                                        32
#define cAf6Reg_dej1_testpat_mon_errbit_cnt_WriteMask                                                      0x0

/*--------------------------------------
BitField Name: RxDE1PtmLoadErrCnt
BitField Type: RW
BitField Desc: Load Error counter.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_dej1_testpat_mon_errbit_cnt_RxDE1PtmLoadErrCnt_Bit_Start                                        0
#define cAf6_dej1_testpat_mon_errbit_cnt_RxDE1PtmLoadErrCnt_Bit_End                                         15
#define cAf6_dej1_testpat_mon_errbit_cnt_RxDE1PtmLoadErrCnt_Mask                                      cBit15_0
#define cAf6_dej1_testpat_mon_errbit_cnt_RxDE1PtmLoadErrCnt_Shift                                            0
#define cAf6_dej1_testpat_mon_errbit_cnt_RxDE1PtmLoadErrCnt_MaxVal                                      0xffff
#define cAf6_dej1_testpat_mon_errbit_cnt_RxDE1PtmLoadErrCnt_MinVal                                         0x0
#define cAf6_dej1_testpat_mon_errbit_cnt_RxDE1PtmLoadErrCnt_RstVal                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Test Pattern Monitoring Good-Bit Counter Loading
Reg Addr   : 0x054905 - 0x649B05
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the loaded good counter report of PTM.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_testpat_mon_goodbit_cnt_loading_Base                                             0x054905
#define cAf6Reg_dej1_testpat_mon_goodbit_cnt_loading                                                  0x054905
#define cAf6Reg_dej1_testpat_mon_goodbit_cnt_loading_WidthVal                                               32
#define cAf6Reg_dej1_testpat_mon_goodbit_cnt_loading_WriteMask                                             0x0

/*--------------------------------------
BitField Name: RxDE1PtmLoadGoodCnt
BitField Type: RW
BitField Desc: Load Good counter.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_dej1_testpat_mon_goodbit_cnt_loading_RxDE1PtmLoadGoodCnt_Bit_Start                                       0
#define cAf6_dej1_testpat_mon_goodbit_cnt_loading_RxDE1PtmLoadGoodCnt_Bit_End                                      15
#define cAf6_dej1_testpat_mon_goodbit_cnt_loading_RxDE1PtmLoadGoodCnt_Mask                                cBit15_0
#define cAf6_dej1_testpat_mon_goodbit_cnt_loading_RxDE1PtmLoadGoodCnt_Shift                                       0
#define cAf6_dej1_testpat_mon_goodbit_cnt_loading_RxDE1PtmLoadGoodCnt_MaxVal                                  0xffff
#define cAf6_dej1_testpat_mon_goodbit_cnt_loading_RxDE1PtmLoadGoodCnt_MinVal                                     0x0
#define cAf6_dej1_testpat_mon_goodbit_cnt_loading_RxDE1PtmLoadGoodCnt_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Test Pattern Monitoring Lost-Bit Counter Loading
Reg Addr   : 0x054906 - 0x649B06
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the loaded loss counter report of PTM.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_testpat_mon_lostbit_cnt_Base                                                     0x054906
#define cAf6Reg_dej1_testpat_mon_lostbit_cnt                                                          0x054906
#define cAf6Reg_dej1_testpat_mon_lostbit_cnt_WidthVal                                                       32
#define cAf6Reg_dej1_testpat_mon_lostbit_cnt_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: RxDE1PtmLoadLossCnt
BitField Type: RW
BitField Desc: Load Loss counter.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_dej1_testpat_mon_lostbit_cnt_RxDE1PtmLoadLossCnt_Bit_Start                                       0
#define cAf6_dej1_testpat_mon_lostbit_cnt_RxDE1PtmLoadLossCnt_Bit_End                                       15
#define cAf6_dej1_testpat_mon_lostbit_cnt_RxDE1PtmLoadLossCnt_Mask                                    cBit15_0
#define cAf6_dej1_testpat_mon_lostbit_cnt_RxDE1PtmLoadLossCnt_Shift                                          0
#define cAf6_dej1_testpat_mon_lostbit_cnt_RxDE1PtmLoadLossCnt_MaxVal                                    0xffff
#define cAf6_dej1_testpat_mon_lostbit_cnt_RxDE1PtmLoadLossCnt_MinVal                                       0x0
#define cAf6_dej1_testpat_mon_lostbit_cnt_RxDE1PtmLoadLossCnt_RstVal                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Test Pattern Monitoring Timing Status
Reg Addr   : 0x054830 - 0x05483F
Reg Formula: 0x054830 +  index
    Where  : 
           + $index(0-15):
Reg Desc   : 
These registers are used for Hardware tus only

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_testpat_mon_timing_stat_Base                                                     0x054830
#define cAf6Reg_dej1_testpat_mon_timing_stat(index)                                         (0x054830+(index))
#define cAf6Reg_dej1_testpat_mon_timing_stat_WidthVal                                                       32
#define cAf6Reg_dej1_testpat_mon_timing_stat_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: Status
BitField Type: RW
BitField Desc: This field is used for Hw tus only.
BitField Bits: [9:0]
--------------------------------------*/
#define cAf6_dej1_testpat_mon_timing_stat_Status_Bit_Start                                                   0
#define cAf6_dej1_testpat_mon_timing_stat_Status_Bit_End                                                     9
#define cAf6_dej1_testpat_mon_timing_stat_Status_Mask                                                  cBit9_0
#define cAf6_dej1_testpat_mon_timing_stat_Status_Shift                                                       0
#define cAf6_dej1_testpat_mon_timing_stat_Status_MaxVal                                                  0x3ff
#define cAf6_dej1_testpat_mon_timing_stat_Status_MinVal                                                    0x0
#define cAf6_dej1_testpat_mon_timing_stat_Status_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Test Pattern Monitoring Status
Reg Addr   : 0x054940 - 0x05495F
Reg Formula: 0x054940 +  index
    Where  : 
           + $index(0-15):
Reg Desc   : 
These registers are used for Hardware tus only

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_testpat_mon_stat_Base                                                            0x054940
#define cAf6Reg_dej1_testpat_mon_stat(index)                                                (0x054940+(index))
#define cAf6Reg_dej1_testpat_mon_stat_WidthVal                                                              64
#define cAf6Reg_dej1_testpat_mon_stat_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: Status
BitField Type: RW
BitField Desc: This field is used for Hw tus only.
BitField Bits: [36:0]
--------------------------------------*/
#define cAf6_dej1_testpat_mon_stat_Status_Bit_Start                                                          0
#define cAf6_dej1_testpat_mon_stat_Status_Bit_End                                                           36
#define cAf6_dej1_testpat_mon_stat_Status_Mask_01                                                     cBit31_0
#define cAf6_dej1_testpat_mon_stat_Status_Shift_01                                                           0
#define cAf6_dej1_testpat_mon_stat_Status_Mask_02                                                      cBit4_0
#define cAf6_dej1_testpat_mon_stat_Status_Shift_02                                                           0


/*------------------------------------------------------------------------------
Reg Name   : SPE/VT Map Fifo Length Water Mark
Reg Addr   : 0x00070002 - 0x00070002
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used for Hardware tus only.

------------------------------------------------------------------------------*/
#define cAf6Reg_spevt_map_fflen_watermask_Base                                                      0x00070002
#define cAf6Reg_spevt_map_fflen_watermask                                                           0x00070002
#define cAf6Reg_spevt_map_fflen_watermask_WidthVal                                                          32
#define cAf6Reg_spevt_map_fflen_watermask_WriteMask                                                        0x0

/*--------------------------------------
BitField Name: STSVTFifoLenMark
BitField Type: RW
BitField Desc: For Hardware use only
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_spevt_map_fflen_watermask_STSVTFifoLenMark_Bit_Start                                            0
#define cAf6_spevt_map_fflen_watermask_STSVTFifoLenMark_Bit_End                                             23
#define cAf6_spevt_map_fflen_watermask_STSVTFifoLenMark_Mask                                          cBit23_0
#define cAf6_spevt_map_fflen_watermask_STSVTFifoLenMark_Shift                                                0
#define cAf6_spevt_map_fflen_watermask_STSVTFifoLenMark_MaxVal                                        0xffffff
#define cAf6_spevt_map_fflen_watermask_STSVTFifoLenMark_MinVal                                             0x0
#define cAf6_spevt_map_fflen_watermask_STSVTFifoLenMark_RstVal                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : STS/VT Map Control
Reg Addr   : 0x00076000 - 0x000763FF
Reg Formula: 0x00076000 +  32*stsid + 4*vtgid + vtid
    Where  : 
           + $stsid(0-23):
           + $vtgid(0-6):
           + $vtid(0-3):
Reg Desc   : 
The STS/VT Map Control is use to configure for per channel STS/VT Map operation.

------------------------------------------------------------------------------*/
#define cAf6Reg_stsvt_map_ctrl_Base                                                                 0x00076000
#define cAf6Reg_stsvt_map_ctrl(stsid, vtgid, vtid)                    (0x00076000+32*(stsid)+4*(vtgid)+(vtid))
#define cAf6Reg_stsvt_map_ctrl_WidthVal                                                                     32
#define cAf6Reg_stsvt_map_ctrl_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: Pdhoversdhmd
BitField Type: RW
BitField Desc: Set 1 to PDH LIU over OCN
BitField Bits: [20]
--------------------------------------*/
#define cAf6_stsvt_map_ctrl_Pdhoversdhmd_Bit_Start                                                          20
#define cAf6_stsvt_map_ctrl_Pdhoversdhmd_Bit_End                                                            20
#define cAf6_stsvt_map_ctrl_Pdhoversdhmd_Mask                                                           cBit20
#define cAf6_stsvt_map_ctrl_Pdhoversdhmd_Shift                                                              20
#define cAf6_stsvt_map_ctrl_Pdhoversdhmd_MaxVal                                                            0x1
#define cAf6_stsvt_map_ctrl_Pdhoversdhmd_MinVal                                                            0x0
#define cAf6_stsvt_map_ctrl_Pdhoversdhmd_RstVal                                                            0x0

/*--------------------------------------
BitField Name: StsVtMapAismask
BitField Type: RW
BitField Desc: Set 1 to mask AIS from PDH to OCN
BitField Bits: [19]
--------------------------------------*/
#define cAf6_stsvt_map_ctrl_StsVtMapAismask_Bit_Start                                                       19
#define cAf6_stsvt_map_ctrl_StsVtMapAismask_Bit_End                                                         19
#define cAf6_stsvt_map_ctrl_StsVtMapAismask_Mask                                                        cBit19
#define cAf6_stsvt_map_ctrl_StsVtMapAismask_Shift                                                           19
#define cAf6_stsvt_map_ctrl_StsVtMapAismask_MaxVal                                                         0x1
#define cAf6_stsvt_map_ctrl_StsVtMapAismask_MinVal                                                         0x0
#define cAf6_stsvt_map_ctrl_StsVtMapAismask_RstVal                                                         0x0

/*--------------------------------------
BitField Name: StsVtMapJitIDCfg
BitField Type: RW
BitField Desc: STS/VT Mapping Jitter ID configure, default value is 0
BitField Bits: [18:15]
--------------------------------------*/
#define cAf6_stsvt_map_ctrl_StsVtMapJitIDCfg_Bit_Start                                                      15
#define cAf6_stsvt_map_ctrl_StsVtMapJitIDCfg_Bit_End                                                        18
#define cAf6_stsvt_map_ctrl_StsVtMapJitIDCfg_Mask                                                    cBit18_15
#define cAf6_stsvt_map_ctrl_StsVtMapJitIDCfg_Shift                                                          15
#define cAf6_stsvt_map_ctrl_StsVtMapJitIDCfg_MaxVal                                                        0xf
#define cAf6_stsvt_map_ctrl_StsVtMapJitIDCfg_MinVal                                                        0x0
#define cAf6_stsvt_map_ctrl_StsVtMapJitIDCfg_RstVal                                                        0x0

/*--------------------------------------
BitField Name: StsVtMapStuffThres
BitField Type: RW
BitField Desc: STS/VT Mapping Stuff Threshold Configure, default value is 12
BitField Bits: [14:10]
--------------------------------------*/
#define cAf6_stsvt_map_ctrl_StsVtMapStuffThres_Bit_Start                                                    10
#define cAf6_stsvt_map_ctrl_StsVtMapStuffThres_Bit_End                                                      14
#define cAf6_stsvt_map_ctrl_StsVtMapStuffThres_Mask                                                  cBit14_10
#define cAf6_stsvt_map_ctrl_StsVtMapStuffThres_Shift                                                        10
#define cAf6_stsvt_map_ctrl_StsVtMapStuffThres_MaxVal                                                     0x1f
#define cAf6_stsvt_map_ctrl_StsVtMapStuffThres_MinVal                                                      0x0
#define cAf6_stsvt_map_ctrl_StsVtMapStuffThres_RstVal                                                      0x0

/*--------------------------------------
BitField Name: StsVtMapJitterCfg
BitField Type: RW
BitField Desc: STS/VT Mapping Jitter configure - Vtmap: StsVtMapJitterCfg: Bit
FiFo mode - 1'b1: Bit FiFo stuff mode: Only in case of ACR/DCR timing mode or
Slaver mode from  ACR/DCR master timing ID - 1'b0: Byte FiFO stuff mode: default
value StsVtMapJitterCfg: Master Enable. Only valid in Bit FiFo stuff_mode
StsVtMapJitterCfg: AIS insert enable StsVtMapJitterCfg: Byte FiFo mode In case
of Byte FiFo Stuff Mode: + 1'b1: Byte FiFo mode Enable: default value + 1'b0:
Byte LoopTime FiFo mode Enable: only in case of Loop timing mode - Spe Map:
0001: old_stuff_mode 0010: holdover_en 0100: aisins_en 1000: fine_stuff_mode
BitField Bits: [9:6]
--------------------------------------*/
#define cAf6_stsvt_map_ctrl_StsVtMapJitterCfg_Bit_Start                                                      6
#define cAf6_stsvt_map_ctrl_StsVtMapJitterCfg_Bit_End                                                        9
#define cAf6_stsvt_map_ctrl_StsVtMapJitterCfg_Mask                                                     cBit9_6
#define cAf6_stsvt_map_ctrl_StsVtMapJitterCfg_Shift                                                          6
#define cAf6_stsvt_map_ctrl_StsVtMapJitterCfg_MaxVal                                                       0xf
#define cAf6_stsvt_map_ctrl_StsVtMapJitterCfg_MinVal                                                       0x0
#define cAf6_stsvt_map_ctrl_StsVtMapJitterCfg_RstVal                                                       0x0

/*--------------------------------------
BitField Name: StsVtMapCenterFifo
BitField Type: RW
BitField Desc: This bit is set to 1 to force center the mapping FIFO.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_stsvt_map_ctrl_StsVtMapCenterFifo_Bit_Start                                                     5
#define cAf6_stsvt_map_ctrl_StsVtMapCenterFifo_Bit_End                                                       5
#define cAf6_stsvt_map_ctrl_StsVtMapCenterFifo_Mask                                                      cBit5
#define cAf6_stsvt_map_ctrl_StsVtMapCenterFifo_Shift                                                         5
#define cAf6_stsvt_map_ctrl_StsVtMapCenterFifo_MaxVal                                                      0x1
#define cAf6_stsvt_map_ctrl_StsVtMapCenterFifo_MinVal                                                      0x0
#define cAf6_stsvt_map_ctrl_StsVtMapCenterFifo_RstVal                                                      0x0

/*--------------------------------------
BitField Name: StsVtMapBypass
BitField Type: RW
BitField Desc: This bit is set to 1 to bypass the STS/VT map
BitField Bits: [4]
--------------------------------------*/
#define cAf6_stsvt_map_ctrl_StsVtMapBypass_Bit_Start                                                         4
#define cAf6_stsvt_map_ctrl_StsVtMapBypass_Bit_End                                                           4
#define cAf6_stsvt_map_ctrl_StsVtMapBypass_Mask                                                          cBit4
#define cAf6_stsvt_map_ctrl_StsVtMapBypass_Shift                                                             4
#define cAf6_stsvt_map_ctrl_StsVtMapBypass_MaxVal                                                          0x1
#define cAf6_stsvt_map_ctrl_StsVtMapBypass_MinVal                                                          0x0
#define cAf6_stsvt_map_ctrl_StsVtMapBypass_RstVal                                                          0x0

/*--------------------------------------
BitField Name: StsVtMapMd
BitField Type: RW
BitField Desc: STS/VT Mapping mode StsVtMapMd   StsVtMapBypass  Operation Mode 0
0     E1 to VT2 Map 0           1     E1 to Bus 1               0     DS1 to
VT1.5 Map 1                1     DS1 to Bus 2              0     E3 to VC3 Map 2
1     E3 to TU3 Map 3           0     DS3 to VC3 Map 3                  1
DS3 to TU3 Map 4                  x     VT2/TU12 Basic CEP 5              x
Packet over STS1/VC3 6            0     STS1/VC3 Fractional CEP carrying
VT2/TU12 6               1     STS1/VC3 Fractional CEP carrying VT15/TU11 7
0     STS1/VC3 Fractional CEP carrying E3 7             1     STS1/VC3
Fractional CEP carrying DS3 8            x     STS1/VC3 Basic CEP 9
x     Packet over STS3c/VC4 10                0       STS3c/VC4 Fractional CEP
carrying VT2/TU12 10           1       STS3c/VC4 Fractional CEP carrying
VT15/TU11 11          0       STS3c/VC4 Fractional CEP carrying E3 11         1
STS3c/VC4 Fractional CEP carrying DS3 12                x
STS3c/VC4/STS12c/VC4_4c Basic CEP 13            x       Packet over TU3 14
0       E3 to Bus 14            1       DS3 to Bus 15           x       Disable
STS/VC/VT/TU/DS1/E1/DS3/E3
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_stsvt_map_ctrl_StsVtMapMd_Bit_Start                                                             0
#define cAf6_stsvt_map_ctrl_StsVtMapMd_Bit_End                                                               3
#define cAf6_stsvt_map_ctrl_StsVtMapMd_Mask                                                            cBit3_0
#define cAf6_stsvt_map_ctrl_StsVtMapMd_Shift                                                                 0
#define cAf6_stsvt_map_ctrl_StsVtMapMd_MaxVal                                                              0xf
#define cAf6_stsvt_map_ctrl_StsVtMapMd_MinVal                                                              0x0
#define cAf6_stsvt_map_ctrl_StsVtMapMd_RstVal                                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : VT Async Map Control
Reg Addr   : 0x00076800 - 0x00076BFF
Reg Formula: 0x00076800 +  32*stsid + 4*vtgid + vtid
    Where  : 
           + $stsid(0-23)
           + $vtgid(0-6):
           + $vtid(0-3):
Reg Desc   : 
The VT Async Map Control is use to configure for per channel VT Async Map operation.

------------------------------------------------------------------------------*/
#define cAf6Reg_vt_async_map_ctrl_Base                                                              0x00076800
#define cAf6Reg_vt_async_map_ctrl(stsid, vtgid, vtid)                 (0x00076800+32*(stsid)+4*(vtgid)+(vtid))
#define cAf6Reg_vt_async_map_ctrl_WidthVal                                                                  32
#define cAf6Reg_vt_async_map_ctrl_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: BitFiFoID
BitField Type: RW
BitField Desc: Bit FiFo ID, related to ACR/DCR ID engine. Valid from 0 to 255
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_vt_async_map_ctrl_BitFiFoID_Bit_Start                                                          24
#define cAf6_vt_async_map_ctrl_BitFiFoID_Bit_End                                                            31
#define cAf6_vt_async_map_ctrl_BitFiFoID_Mask                                                        cBit31_24
#define cAf6_vt_async_map_ctrl_BitFiFoID_Shift                                                              24
#define cAf6_vt_async_map_ctrl_BitFiFoID_MaxVal                                                           0xff
#define cAf6_vt_async_map_ctrl_BitFiFoID_MinVal                                                            0x0
#define cAf6_vt_async_map_ctrl_BitFiFoID_RstVal                                                            0x0

/*--------------------------------------
BitField Name: VTStuffThr
BitField Type: RW
BitField Desc: The number of VT Multiframe (500us) threshold for VT async
stuffing. Using for BitFiFo mode and ByteFiFo mode. Default value is 0
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_vt_async_map_ctrl_VTStuffThr_Bit_Start                                                          0
#define cAf6_vt_async_map_ctrl_VTStuffThr_Bit_End                                                           23
#define cAf6_vt_async_map_ctrl_VTStuffThr_Mask                                                        cBit23_0
#define cAf6_vt_async_map_ctrl_VTStuffThr_Shift                                                              0
#define cAf6_vt_async_map_ctrl_VTStuffThr_MaxVal                                                      0xffffff
#define cAf6_vt_async_map_ctrl_VTStuffThr_MinVal                                                           0x0
#define cAf6_vt_async_map_ctrl_VTStuffThr_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : STS/VT Map FIFO Center Sticky
Reg Addr   : 0x00070020 - 0x0007003B
Reg Formula: 0x00070020 +  index
    Where  : 
           + $index(0-23):
Reg Desc   : 
These registers are used for Hardware tus only

------------------------------------------------------------------------------*/
#define cAf6Reg_stsvt_map_ff_center_sticky_Base                                                     0x00070020
#define cAf6Reg_stsvt_map_ff_center_sticky(index)                                         (0x00070020+(index))
#define cAf6Reg_stsvt_map_ff_center_sticky_WidthVal                                                         32
#define cAf6Reg_stsvt_map_ff_center_sticky_WriteMask                                                       0x0

/*--------------------------------------
BitField Name: STSVTFifoCenStk
BitField Type: RW
BitField Desc: This field is used for Hw tus only.
BitField Bits: [27:0]
--------------------------------------*/
#define cAf6_stsvt_map_ff_center_sticky_STSVTFifoCenStk_Bit_Start                                            0
#define cAf6_stsvt_map_ff_center_sticky_STSVTFifoCenStk_Bit_End                                             27
#define cAf6_stsvt_map_ff_center_sticky_STSVTFifoCenStk_Mask                                          cBit27_0
#define cAf6_stsvt_map_ff_center_sticky_STSVTFifoCenStk_Shift                                                0
#define cAf6_stsvt_map_ff_center_sticky_STSVTFifoCenStk_MaxVal                                       0xfffffff
#define cAf6_stsvt_map_ff_center_sticky_STSVTFifoCenStk_MinVal                                             0x0
#define cAf6_stsvt_map_ff_center_sticky_STSVTFifoCenStk_RstVal                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : STS/VT Map HW Status
Reg Addr   : 0x00076400-0x000767FF
Reg Formula: 0x00076400 +  32*stsid + 4*vtgid + vtid
    Where  : 
           + $stsid(0-23):
           + $vtgid(0-6):
           + $vtid(0-3):
Reg Desc   : 
for HW debug only

------------------------------------------------------------------------------*/
#define cAf6Reg_stsvt_map_hw_stat_Base                                                              0x00076400
#define cAf6Reg_stsvt_map_hw_stat(stsid, vtgid, vtid)                 (0x00076400+32*(stsid)+4*(vtgid)+(vtid))
#define cAf6Reg_stsvt_map_hw_stat_WidthVal                                                                  64
#define cAf6Reg_stsvt_map_hw_stat_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: STSVTStatus
BitField Type: RW
BitField Desc: for HW debug only
BitField Bits: [35:0]
--------------------------------------*/
#define cAf6_stsvt_map_hw_stat_STSVTStatus_Bit_Start                                                         0
#define cAf6_stsvt_map_hw_stat_STSVTStatus_Bit_End                                                          35
#define cAf6_stsvt_map_hw_stat_STSVTStatus_Mask_01                                                    cBit31_0
#define cAf6_stsvt_map_hw_stat_STSVTStatus_Shift_01                                                          0
#define cAf6_stsvt_map_hw_stat_STSVTStatus_Mask_02                                                     cBit3_0
#define cAf6_stsvt_map_hw_stat_STSVTStatus_Shift_02                                                          0


/*------------------------------------------------------------------------------
Reg Name   : TS/VT Map Jitter Configuration
Reg Addr   : 0x00070080
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the configuration for STS/VT Mapping.

------------------------------------------------------------------------------*/
#define cAf6Reg_ts_vt_map_jitter_cfg_Base                                                           0x00070080
#define cAf6Reg_ts_vt_map_jitter_cfg                                                                0x00070080
#define cAf6Reg_ts_vt_map_jitter_cfg_WidthVal                                                               96
#define cAf6Reg_ts_vt_map_jitter_cfg_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: SWThreshold1
BitField Type: RW
BitField Desc:
BitField Bits: [69:63]
--------------------------------------*/
#define cAf6_ts_vt_map_jitter_cfg_SWThreshold1_Bit_Start                                                    63
#define cAf6_ts_vt_map_jitter_cfg_SWThreshold1_Bit_End                                                      69
#define cAf6_ts_vt_map_jitter_cfg_SWThreshold1_Mask_01                                                  cBit31
#define cAf6_ts_vt_map_jitter_cfg_SWThreshold1_Shift_01                                                     31
#define cAf6_ts_vt_map_jitter_cfg_SWThreshold1_Mask_02                                                 cBit5_0
#define cAf6_ts_vt_map_jitter_cfg_SWThreshold1_Shift_02                                                      0

/*--------------------------------------
BitField Name: SWThreshold2
BitField Type: RW
BitField Desc:
BitField Bits: [62:56]
--------------------------------------*/
#define cAf6_ts_vt_map_jitter_cfg_SWThreshold2_Bit_Start                                                    56
#define cAf6_ts_vt_map_jitter_cfg_SWThreshold2_Bit_End                                                      62
#define cAf6_ts_vt_map_jitter_cfg_SWThreshold2_Mask                                                  cBit30_24
#define cAf6_ts_vt_map_jitter_cfg_SWThreshold2_Shift                                                        24
#define cAf6_ts_vt_map_jitter_cfg_SWThreshold2_MaxVal                                                      0x0
#define cAf6_ts_vt_map_jitter_cfg_SWThreshold2_MinVal                                                      0x0
#define cAf6_ts_vt_map_jitter_cfg_SWThreshold2_RstVal                                                      0x0

/*--------------------------------------
BitField Name: SWThreshold3
BitField Type: RW
BitField Desc:
BitField Bits: [55:49]
--------------------------------------*/
#define cAf6_ts_vt_map_jitter_cfg_SWThreshold3_Bit_Start                                                    49
#define cAf6_ts_vt_map_jitter_cfg_SWThreshold3_Bit_End                                                      55
#define cAf6_ts_vt_map_jitter_cfg_SWThreshold3_Mask                                                  cBit23_17
#define cAf6_ts_vt_map_jitter_cfg_SWThreshold3_Shift                                                        17
#define cAf6_ts_vt_map_jitter_cfg_SWThreshold3_MaxVal                                                      0x0
#define cAf6_ts_vt_map_jitter_cfg_SWThreshold3_MinVal                                                      0x0
#define cAf6_ts_vt_map_jitter_cfg_SWThreshold3_RstVal                                                      0x0

/*--------------------------------------
BitField Name: SWThreshold4
BitField Type: RW
BitField Desc:
BitField Bits: [48:42]
--------------------------------------*/
#define cAf6_ts_vt_map_jitter_cfg_SWThreshold4_Bit_Start                                                    42
#define cAf6_ts_vt_map_jitter_cfg_SWThreshold4_Bit_End                                                      48
#define cAf6_ts_vt_map_jitter_cfg_SWThreshold4_Mask                                                  cBit16_10
#define cAf6_ts_vt_map_jitter_cfg_SWThreshold4_Shift                                                        10
#define cAf6_ts_vt_map_jitter_cfg_SWThreshold4_MaxVal                                                      0x0
#define cAf6_ts_vt_map_jitter_cfg_SWThreshold4_MinVal                                                      0x0
#define cAf6_ts_vt_map_jitter_cfg_SWThreshold4_RstVal                                                      0x0

/*--------------------------------------
BitField Name: SWThreshold5
BitField Type: RW
BitField Desc:
BitField Bits: [41:35]
--------------------------------------*/
#define cAf6_ts_vt_map_jitter_cfg_SWThreshold5_Bit_Start                                                    35
#define cAf6_ts_vt_map_jitter_cfg_SWThreshold5_Bit_End                                                      41
#define cAf6_ts_vt_map_jitter_cfg_SWThreshold5_Mask                                                    cBit9_3
#define cAf6_ts_vt_map_jitter_cfg_SWThreshold5_Shift                                                         3
#define cAf6_ts_vt_map_jitter_cfg_SWThreshold5_MaxVal                                                      0x0
#define cAf6_ts_vt_map_jitter_cfg_SWThreshold5_MinVal                                                      0x0
#define cAf6_ts_vt_map_jitter_cfg_SWThreshold5_RstVal                                                      0x0

/*--------------------------------------
BitField Name: SWCntMax1
BitField Type: RW
BitField Desc:
BitField Bits: [34:28]
--------------------------------------*/
#define cAf6_ts_vt_map_jitter_cfg_SWCntMax1_Bit_Start                                                       28
#define cAf6_ts_vt_map_jitter_cfg_SWCntMax1_Bit_End                                                         34
#define cAf6_ts_vt_map_jitter_cfg_SWCntMax1_Mask_01                                                  cBit31_28
#define cAf6_ts_vt_map_jitter_cfg_SWCntMax1_Shift_01                                                        28
#define cAf6_ts_vt_map_jitter_cfg_SWCntMax1_Mask_02                                                    cBit2_0
#define cAf6_ts_vt_map_jitter_cfg_SWCntMax1_Shift_02                                                         0

/*--------------------------------------
BitField Name: SWCntMax2
BitField Type: RW
BitField Desc:
BitField Bits: [27:21]
--------------------------------------*/
#define cAf6_ts_vt_map_jitter_cfg_SWCntMax2_Bit_Start                                                       21
#define cAf6_ts_vt_map_jitter_cfg_SWCntMax2_Bit_End                                                         27
#define cAf6_ts_vt_map_jitter_cfg_SWCntMax2_Mask                                                     cBit27_21
#define cAf6_ts_vt_map_jitter_cfg_SWCntMax2_Shift                                                           21
#define cAf6_ts_vt_map_jitter_cfg_SWCntMax2_MaxVal                                                        0x7f
#define cAf6_ts_vt_map_jitter_cfg_SWCntMax2_MinVal                                                         0x0
#define cAf6_ts_vt_map_jitter_cfg_SWCntMax2_RstVal                                                         0x0

/*--------------------------------------
BitField Name: SWCntMax3
BitField Type: RW
BitField Desc:
BitField Bits: [20:14]
--------------------------------------*/
#define cAf6_ts_vt_map_jitter_cfg_SWCntMax3_Bit_Start                                                       14
#define cAf6_ts_vt_map_jitter_cfg_SWCntMax3_Bit_End                                                         20
#define cAf6_ts_vt_map_jitter_cfg_SWCntMax3_Mask                                                     cBit20_14
#define cAf6_ts_vt_map_jitter_cfg_SWCntMax3_Shift                                                           14
#define cAf6_ts_vt_map_jitter_cfg_SWCntMax3_MaxVal                                                        0x7f
#define cAf6_ts_vt_map_jitter_cfg_SWCntMax3_MinVal                                                         0x0
#define cAf6_ts_vt_map_jitter_cfg_SWCntMax3_RstVal                                                         0x0

/*--------------------------------------
BitField Name: SWCntMax4
BitField Type: RW
BitField Desc:
BitField Bits: [13:7]
--------------------------------------*/
#define cAf6_ts_vt_map_jitter_cfg_SWCntMax4_Bit_Start                                                        7
#define cAf6_ts_vt_map_jitter_cfg_SWCntMax4_Bit_End                                                         13
#define cAf6_ts_vt_map_jitter_cfg_SWCntMax4_Mask                                                      cBit13_7
#define cAf6_ts_vt_map_jitter_cfg_SWCntMax4_Shift                                                            7
#define cAf6_ts_vt_map_jitter_cfg_SWCntMax4_MaxVal                                                        0x7f
#define cAf6_ts_vt_map_jitter_cfg_SWCntMax4_MinVal                                                         0x0
#define cAf6_ts_vt_map_jitter_cfg_SWCntMax4_RstVal                                                         0x0

/*--------------------------------------
BitField Name: SWCntMax5
BitField Type: RW
BitField Desc:
BitField Bits: [6:0]
--------------------------------------*/
#define cAf6_ts_vt_map_jitter_cfg_SWCntMax5_Bit_Start                                                        0
#define cAf6_ts_vt_map_jitter_cfg_SWCntMax5_Bit_End                                                          6
#define cAf6_ts_vt_map_jitter_cfg_SWCntMax5_Mask                                                       cBit6_0
#define cAf6_ts_vt_map_jitter_cfg_SWCntMax5_Shift                                                            0
#define cAf6_ts_vt_map_jitter_cfg_SWCntMax5_MaxVal                                                        0x7f
#define cAf6_ts_vt_map_jitter_cfg_SWCntMax5_MinVal                                                         0x0
#define cAf6_ts_vt_map_jitter_cfg_SWCntMax5_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Tx Framer Control
Reg Addr   : 0x00094000 - 0x000943FF
Reg Formula: 0x00094000 +  32*de3id + 4*de2id + de1id
    Where  : 
           + $de3id(0-23):
           + $de2id(0-6):
           + $de1id(0-3):
Reg Desc   : 
DS1/E1/J1 Rx framer control is used to configure for Frame mode (DS1, E1, J1) at the DS1/E1 framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_tx_framer_ctrl_Base                                                            0x00094000
#define cAf6Reg_dej1_tx_framer_ctrl(de3id, de2id, de1id)              (0x00094000+32*(de3id)+4*(de2id)+(de1id))
#define cAf6Reg_dej1_tx_framer_ctrl_WidthVal                                                                32
#define cAf6Reg_dej1_tx_framer_ctrl_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: TxDE1TxSaBit
BitField Type: RW
BitField Desc: Transmitted unused Sa bit
BitField Bits: [22]
--------------------------------------*/
#define cAf6_dej1_tx_framer_ctrl_TxDE1TxSaBit_Bit_Start                                                     22
#define cAf6_dej1_tx_framer_ctrl_TxDE1TxSaBit_Bit_End                                                       22
#define cAf6_dej1_tx_framer_ctrl_TxDE1TxSaBit_Mask                                                      cBit22
#define cAf6_dej1_tx_framer_ctrl_TxDE1TxSaBit_Shift                                                         22
#define cAf6_dej1_tx_framer_ctrl_TxDE1TxSaBit_MaxVal                                                       0x1
#define cAf6_dej1_tx_framer_ctrl_TxDE1TxSaBit_MinVal                                                       0x0
#define cAf6_dej1_tx_framer_ctrl_TxDE1TxSaBit_RstVal                                                       0x0

/*--------------------------------------
BitField Name: TxDE1FbitBypass
BitField Type: RW
BitField Desc: Set 1 to bypass Fbit insertion in Tx DS1/E1 Framer
BitField Bits: [21]
--------------------------------------*/
#define cAf6_dej1_tx_framer_ctrl_TxDE1FbitBypass_Bit_Start                                                  21
#define cAf6_dej1_tx_framer_ctrl_TxDE1FbitBypass_Bit_End                                                    21
#define cAf6_dej1_tx_framer_ctrl_TxDE1FbitBypass_Mask                                                   cBit21
#define cAf6_dej1_tx_framer_ctrl_TxDE1FbitBypass_Shift                                                      21
#define cAf6_dej1_tx_framer_ctrl_TxDE1FbitBypass_MaxVal                                                    0x1
#define cAf6_dej1_tx_framer_ctrl_TxDE1FbitBypass_MinVal                                                    0x0
#define cAf6_dej1_tx_framer_ctrl_TxDE1FbitBypass_RstVal                                                    0x0

/*--------------------------------------
BitField Name: TxDE1LineAisIns
BitField Type: RW
BitField Desc: Set 1 to enable Line AIS Insert
BitField Bits: [20]
--------------------------------------*/
#define cAf6_dej1_tx_framer_ctrl_TxDE1LineAisIns_Bit_Start                                                  20
#define cAf6_dej1_tx_framer_ctrl_TxDE1LineAisIns_Bit_End                                                    20
#define cAf6_dej1_tx_framer_ctrl_TxDE1LineAisIns_Mask                                                   cBit20
#define cAf6_dej1_tx_framer_ctrl_TxDE1LineAisIns_Shift                                                      20
#define cAf6_dej1_tx_framer_ctrl_TxDE1LineAisIns_MaxVal                                                    0x1
#define cAf6_dej1_tx_framer_ctrl_TxDE1LineAisIns_MinVal                                                    0x0
#define cAf6_dej1_tx_framer_ctrl_TxDE1LineAisIns_RstVal                                                    0x0

/*--------------------------------------
BitField Name: TxDE1PayAisIns
BitField Type: RW
BitField Desc: Set 1 to enable Payload AIS Insert
BitField Bits: [19]
--------------------------------------*/
#define cAf6_dej1_tx_framer_ctrl_TxDE1PayAisIns_Bit_Start                                                   19
#define cAf6_dej1_tx_framer_ctrl_TxDE1PayAisIns_Bit_End                                                     19
#define cAf6_dej1_tx_framer_ctrl_TxDE1PayAisIns_Mask                                                    cBit19
#define cAf6_dej1_tx_framer_ctrl_TxDE1PayAisIns_Shift                                                       19
#define cAf6_dej1_tx_framer_ctrl_TxDE1PayAisIns_MaxVal                                                     0x1
#define cAf6_dej1_tx_framer_ctrl_TxDE1PayAisIns_MinVal                                                     0x0
#define cAf6_dej1_tx_framer_ctrl_TxDE1PayAisIns_RstVal                                                     0x0

/*--------------------------------------
BitField Name: TxDE1RmtLineloop
BitField Type: RW
BitField Desc: Set 1 to enable remote Line Loop back
BitField Bits: [18]
--------------------------------------*/
#define cAf6_dej1_tx_framer_ctrl_TxDE1RmtLineloop_Bit_Start                                                 18
#define cAf6_dej1_tx_framer_ctrl_TxDE1RmtLineloop_Bit_End                                                   18
#define cAf6_dej1_tx_framer_ctrl_TxDE1RmtLineloop_Mask                                                  cBit18
#define cAf6_dej1_tx_framer_ctrl_TxDE1RmtLineloop_Shift                                                     18
#define cAf6_dej1_tx_framer_ctrl_TxDE1RmtLineloop_MaxVal                                                   0x1
#define cAf6_dej1_tx_framer_ctrl_TxDE1RmtLineloop_MinVal                                                   0x0
#define cAf6_dej1_tx_framer_ctrl_TxDE1RmtLineloop_RstVal                                                   0x0

/*--------------------------------------
BitField Name: TxDE1RmtPayloop
BitField Type: RW
BitField Desc: Set 1 to enable remote Payload Loop back. Sharing for VT Loopback
in case of CEP path
BitField Bits: [17]
--------------------------------------*/
#define cAf6_dej1_tx_framer_ctrl_TxDE1RmtPayloop_Bit_Start                                                  17
#define cAf6_dej1_tx_framer_ctrl_TxDE1RmtPayloop_Bit_End                                                    17
#define cAf6_dej1_tx_framer_ctrl_TxDE1RmtPayloop_Mask                                                   cBit17
#define cAf6_dej1_tx_framer_ctrl_TxDE1RmtPayloop_Shift                                                      17
#define cAf6_dej1_tx_framer_ctrl_TxDE1RmtPayloop_MaxVal                                                    0x1
#define cAf6_dej1_tx_framer_ctrl_TxDE1RmtPayloop_MinVal                                                    0x0
#define cAf6_dej1_tx_framer_ctrl_TxDE1RmtPayloop_RstVal                                                    0x0

/*--------------------------------------
BitField Name: TxDE1AutoAis
BitField Type: RW
BitField Desc: Set 1 to enable AIS indication from data map block to
automatically transmit all 1s at DS1/E1/J1 Tx framer
BitField Bits: [16]
--------------------------------------*/
#define cAf6_dej1_tx_framer_ctrl_TxDE1AutoAis_Bit_Start                                                     16
#define cAf6_dej1_tx_framer_ctrl_TxDE1AutoAis_Bit_End                                                       16
#define cAf6_dej1_tx_framer_ctrl_TxDE1AutoAis_Mask                                                      cBit16
#define cAf6_dej1_tx_framer_ctrl_TxDE1AutoAis_Shift                                                         16
#define cAf6_dej1_tx_framer_ctrl_TxDE1AutoAis_MaxVal                                                       0x1
#define cAf6_dej1_tx_framer_ctrl_TxDE1AutoAis_MinVal                                                       0x0
#define cAf6_dej1_tx_framer_ctrl_TxDE1AutoAis_RstVal                                                       0x0

/*--------------------------------------
BitField Name: TxDE1DlkBypass
BitField Type: RW
BitField Desc: Set 1 to bypass FDL insertion
BitField Bits: [15]
--------------------------------------*/
#define cAf6_dej1_tx_framer_ctrl_TxDE1DlkBypass_Bit_Start                                                   15
#define cAf6_dej1_tx_framer_ctrl_TxDE1DlkBypass_Bit_End                                                     15
#define cAf6_dej1_tx_framer_ctrl_TxDE1DlkBypass_Mask                                                    cBit15
#define cAf6_dej1_tx_framer_ctrl_TxDE1DlkBypass_Shift                                                       15
#define cAf6_dej1_tx_framer_ctrl_TxDE1DlkBypass_MaxVal                                                     0x1
#define cAf6_dej1_tx_framer_ctrl_TxDE1DlkBypass_MinVal                                                     0x0
#define cAf6_dej1_tx_framer_ctrl_TxDE1DlkBypass_RstVal                                                     0x0

/*--------------------------------------
BitField Name: TxDE1DlkSwap
BitField Type: RW
BitField Desc: Set 1 ti Swap FDL transmit bit
BitField Bits: [14]
--------------------------------------*/
#define cAf6_dej1_tx_framer_ctrl_TxDE1DlkSwap_Bit_Start                                                     14
#define cAf6_dej1_tx_framer_ctrl_TxDE1DlkSwap_Bit_End                                                       14
#define cAf6_dej1_tx_framer_ctrl_TxDE1DlkSwap_Mask                                                      cBit14
#define cAf6_dej1_tx_framer_ctrl_TxDE1DlkSwap_Shift                                                         14
#define cAf6_dej1_tx_framer_ctrl_TxDE1DlkSwap_MaxVal                                                       0x1
#define cAf6_dej1_tx_framer_ctrl_TxDE1DlkSwap_MinVal                                                       0x0
#define cAf6_dej1_tx_framer_ctrl_TxDE1DlkSwap_RstVal                                                       0x0

/*--------------------------------------
BitField Name: TxDE1DLCfg
BitField Type: RW
BitField Desc: Configured for transmit 5 datalink bits (Sa4 ? Sa8): 1: enable,
otherwise disable.
BitField Bits: [13:9]
--------------------------------------*/
#define cAf6_dej1_tx_framer_ctrl_TxDE1DLCfg_Bit_Start                                                        9
#define cAf6_dej1_tx_framer_ctrl_TxDE1DLCfg_Bit_End                                                         13
#define cAf6_dej1_tx_framer_ctrl_TxDE1DLCfg_Mask                                                      cBit13_9
#define cAf6_dej1_tx_framer_ctrl_TxDE1DLCfg_Shift                                                            9
#define cAf6_dej1_tx_framer_ctrl_TxDE1DLCfg_MaxVal                                                        0x1f
#define cAf6_dej1_tx_framer_ctrl_TxDE1DLCfg_MinVal                                                         0x0
#define cAf6_dej1_tx_framer_ctrl_TxDE1DLCfg_RstVal                                                         0x0

/*--------------------------------------
BitField Name: TxDE1AutoYel
BitField Type: RW
BitField Desc: Auto Yellow generation enable 1: Yellow alarm detected from Rx
Framer will be automatically transmitted in Tx Framer 0: No automatically Yellow
alarm transmitted
BitField Bits: [8]
--------------------------------------*/
#define cAf6_dej1_tx_framer_ctrl_TxDE1AutoYel_Bit_Start                                                      8
#define cAf6_dej1_tx_framer_ctrl_TxDE1AutoYel_Bit_End                                                        8
#define cAf6_dej1_tx_framer_ctrl_TxDE1AutoYel_Mask                                                       cBit8
#define cAf6_dej1_tx_framer_ctrl_TxDE1AutoYel_Shift                                                          8
#define cAf6_dej1_tx_framer_ctrl_TxDE1AutoYel_MaxVal                                                       0x1
#define cAf6_dej1_tx_framer_ctrl_TxDE1AutoYel_MinVal                                                       0x0
#define cAf6_dej1_tx_framer_ctrl_TxDE1AutoYel_RstVal                                                       0x0

/*--------------------------------------
BitField Name: TxDE1FrcYel
BitField Type: RW
BitField Desc: SW force to Tx Yellow alarm
BitField Bits: [7]
--------------------------------------*/
#define cAf6_dej1_tx_framer_ctrl_TxDE1FrcYel_Bit_Start                                                       7
#define cAf6_dej1_tx_framer_ctrl_TxDE1FrcYel_Bit_End                                                         7
#define cAf6_dej1_tx_framer_ctrl_TxDE1FrcYel_Mask                                                        cBit7
#define cAf6_dej1_tx_framer_ctrl_TxDE1FrcYel_Shift                                                           7
#define cAf6_dej1_tx_framer_ctrl_TxDE1FrcYel_MaxVal                                                        0x1
#define cAf6_dej1_tx_framer_ctrl_TxDE1FrcYel_MinVal                                                        0x0
#define cAf6_dej1_tx_framer_ctrl_TxDE1FrcYel_RstVal                                                        0x0

/*--------------------------------------
BitField Name: TxDE1AutoCrcErr
BitField Type: RW
BitField Desc: Auto CRC error enable 1: CRC Error detected from Rx Framer will
be automatically transmitted in REI bit of Tx Framer 0: No automatically CRC
Error alarm transmitted
BitField Bits: [6]
--------------------------------------*/
#define cAf6_dej1_tx_framer_ctrl_TxDE1AutoCrcErr_Bit_Start                                                   6
#define cAf6_dej1_tx_framer_ctrl_TxDE1AutoCrcErr_Bit_End                                                     6
#define cAf6_dej1_tx_framer_ctrl_TxDE1AutoCrcErr_Mask                                                    cBit6
#define cAf6_dej1_tx_framer_ctrl_TxDE1AutoCrcErr_Shift                                                       6
#define cAf6_dej1_tx_framer_ctrl_TxDE1AutoCrcErr_MaxVal                                                    0x1
#define cAf6_dej1_tx_framer_ctrl_TxDE1AutoCrcErr_MinVal                                                    0x0
#define cAf6_dej1_tx_framer_ctrl_TxDE1AutoCrcErr_RstVal                                                    0x0

/*--------------------------------------
BitField Name: TxDE1FrcCrcErr
BitField Type: RW
BitField Desc: SW force to Tx CRC Error alarm (REI bit)
BitField Bits: [5]
--------------------------------------*/
#define cAf6_dej1_tx_framer_ctrl_TxDE1FrcCrcErr_Bit_Start                                                    5
#define cAf6_dej1_tx_framer_ctrl_TxDE1FrcCrcErr_Bit_End                                                      5
#define cAf6_dej1_tx_framer_ctrl_TxDE1FrcCrcErr_Mask                                                     cBit5
#define cAf6_dej1_tx_framer_ctrl_TxDE1FrcCrcErr_Shift                                                        5
#define cAf6_dej1_tx_framer_ctrl_TxDE1FrcCrcErr_MaxVal                                                     0x1
#define cAf6_dej1_tx_framer_ctrl_TxDE1FrcCrcErr_MinVal                                                     0x0
#define cAf6_dej1_tx_framer_ctrl_TxDE1FrcCrcErr_RstVal                                                     0x0

/*--------------------------------------
BitField Name: TxDE1En
BitField Type: RW
BitField Desc: Set 1 to enable the DS1/E1/J1 framer.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_dej1_tx_framer_ctrl_TxDE1En_Bit_Start                                                           4
#define cAf6_dej1_tx_framer_ctrl_TxDE1En_Bit_End                                                             4
#define cAf6_dej1_tx_framer_ctrl_TxDE1En_Mask                                                            cBit4
#define cAf6_dej1_tx_framer_ctrl_TxDE1En_Shift                                                               4
#define cAf6_dej1_tx_framer_ctrl_TxDE1En_MaxVal                                                            0x1
#define cAf6_dej1_tx_framer_ctrl_TxDE1En_MinVal                                                            0x0
#define cAf6_dej1_tx_framer_ctrl_TxDE1En_RstVal                                                            0x0

/*--------------------------------------
BitField Name: RxDE1Md
BitField Type: RW
BitField Desc: Receive DS1/J1 framing mode 0000: DS1/J1 Unframe 0001: DS1 SF
(D4) 0010: DS1 ESF 0011: DS1 DDS 0100: DS1 SLC 0101: J1 SF 0110: J1 ESF 1000: E1
Unframe 1001: E1 Basic Frame 1010: E1 CRC4 Frame
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_dej1_tx_framer_ctrl_RxDE1Md_Bit_Start                                                           0
#define cAf6_dej1_tx_framer_ctrl_RxDE1Md_Bit_End                                                             3
#define cAf6_dej1_tx_framer_ctrl_RxDE1Md_Mask                                                          cBit3_0
#define cAf6_dej1_tx_framer_ctrl_RxDE1Md_Shift                                                               0
#define cAf6_dej1_tx_framer_ctrl_RxDE1Md_MaxVal                                                            0xf
#define cAf6_dej1_tx_framer_ctrl_RxDE1Md_MinVal                                                            0x0
#define cAf6_dej1_tx_framer_ctrl_RxDE1Md_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Tx Framer Status
Reg Addr   : 0x00094400 - 0x000947FF
Reg Formula: 0x00094400 +  32*de3id + 4*de2id + de1id
    Where  : 
           + $de3id(0-23):
           + $de2id(0-6):
           + $de1id(0-3):
Reg Desc   : 
These registers are used for Hardware tus only

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_tx_framer_stat_Base                                                            0x00094400
#define cAf6Reg_dej1_tx_framer_stat(de3id, de2id, de1id)              (0x00094400+32*(de3id)+4*(de2id)+(de1id))
#define cAf6Reg_dej1_tx_framer_stat_WidthVal                                                                32
#define cAf6Reg_dej1_tx_framer_stat_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: RxDE1Sta
BitField Type: RW
BitField Desc: 000: Search 001: Shift 010: Wait 011: Verify 100: Fs search 101:
Inframe
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_dej1_tx_framer_stat_RxDE1Sta_Bit_Start                                                          0
#define cAf6_dej1_tx_framer_stat_RxDE1Sta_Bit_End                                                            2
#define cAf6_dej1_tx_framer_stat_RxDE1Sta_Mask                                                         cBit2_0
#define cAf6_dej1_tx_framer_stat_RxDE1Sta_Shift                                                              0
#define cAf6_dej1_tx_framer_stat_RxDE1Sta_MaxVal                                                           0x7
#define cAf6_dej1_tx_framer_stat_RxDE1Sta_MinVal                                                           0x0
#define cAf6_dej1_tx_framer_stat_RxDE1Sta_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Tx Framer Signaling Insertion Control
Reg Addr   : 0x00094800 - 0x00094BFF
Reg Formula: 0x00094800 +  32*de3id + 4*de2id + de1id
    Where  : 
           + $de3id(0-23):
           + $de2id(0-6):
           + $de1id(0-3):
Reg Desc   : 
DS1/E1/J1 Rx framer signaling insertion control is used to configure for signaling operation modes at the DS1/E1 framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_tx_framer_sign_insertion_ctrl_Base                                             0x00094800
#define cAf6Reg_dej1_tx_framer_sign_insertion_ctrl(de3id, de2id, de1id)(0x00094800+32*(de3id)+4*(de2id)+(de1id))
#define cAf6Reg_dej1_tx_framer_sign_insertion_ctrl_WidthVal                                                 32
#define cAf6Reg_dej1_tx_framer_sign_insertion_ctrl_WriteMask                                               0x0

/*--------------------------------------
BitField Name: TxDE1SigMfrmEn
BitField Type: RW
BitField Desc: Set 1 to enable the Tx DS1/E1/J1 framer to sync the signaling
multiframe to the incoming data flow. No applicable for DS1
BitField Bits: [30]
--------------------------------------*/
#define cAf6_dej1_tx_framer_sign_insertion_ctrl_TxDE1SigMfrmEn_Bit_Start                                      30
#define cAf6_dej1_tx_framer_sign_insertion_ctrl_TxDE1SigMfrmEn_Bit_End                                      30
#define cAf6_dej1_tx_framer_sign_insertion_ctrl_TxDE1SigMfrmEn_Mask                                     cBit30
#define cAf6_dej1_tx_framer_sign_insertion_ctrl_TxDE1SigMfrmEn_Shift                                        30
#define cAf6_dej1_tx_framer_sign_insertion_ctrl_TxDE1SigMfrmEn_MaxVal                                      0x1
#define cAf6_dej1_tx_framer_sign_insertion_ctrl_TxDE1SigMfrmEn_MinVal                                      0x0
#define cAf6_dej1_tx_framer_sign_insertion_ctrl_TxDE1SigMfrmEn_RstVal                                      0x0

/*--------------------------------------
BitField Name: TxDE1SigBypass
BitField Type: RW
BitField Desc: 30 signaling bypass bit for 32 DS0 in an E1 frame. In DS1 mode,
only 24 LSB bits is used. Set 1 to bypass Signaling insertion in Tx DS1/E1
Framer
BitField Bits: [29:0]
--------------------------------------*/
#define cAf6_dej1_tx_framer_sign_insertion_ctrl_TxDE1SigBypass_Bit_Start                                       0
#define cAf6_dej1_tx_framer_sign_insertion_ctrl_TxDE1SigBypass_Bit_End                                      29
#define cAf6_dej1_tx_framer_sign_insertion_ctrl_TxDE1SigBypass_Mask                                   cBit29_0
#define cAf6_dej1_tx_framer_sign_insertion_ctrl_TxDE1SigBypass_Shift                                         0
#define cAf6_dej1_tx_framer_sign_insertion_ctrl_TxDE1SigBypass_MaxVal                               0x3fffffff
#define cAf6_dej1_tx_framer_sign_insertion_ctrl_TxDE1SigBypass_MinVal                                      0x0
#define cAf6_dej1_tx_framer_sign_insertion_ctrl_TxDE1SigBypass_RstVal                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Tx Framer Signaling ID Conversion Control
Reg Addr   : 0x000C2000 - 0x000C23FF
Reg Formula: 0x000C2000 +  pwid
    Where  : 
           + $pwid(0-1023):
Reg Desc   : 
DS1/E1/J1 Rx framer signaling ID conversion control is used to convert the PW ID to DS1/E1/J1 Line ID at the DS1/E1 framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_tx_framer_sign_id_conv_ctrl_Base                                               0x000C2000
#define cAf6Reg_dej1_tx_framer_sign_id_conv_ctrl(pwid)                                     (0x000C2000+(pwid))
#define cAf6Reg_dej1_tx_framer_sign_id_conv_ctrl_WidthVal                                                   64
#define cAf6Reg_dej1_tx_framer_sign_id_conv_ctrl_WriteMask                                                 0x0

/*--------------------------------------
BitField Name: TxDE1SigDS1
BitField Type: RW
BitField Desc: Set 1 if the PW carry DS0 for the DS1/J1, set 0 for E1
BitField Bits: [41]
--------------------------------------*/
#define cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigDS1_Bit_Start                                         41
#define cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigDS1_Bit_End                                           41
#define cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigDS1_Mask                                           cBit9
#define cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigDS1_Shift                                              9
#define cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigDS1_MaxVal                                           0x0
#define cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigDS1_MinVal                                           0x0
#define cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigDS1_RstVal                                           0x0

/*--------------------------------------
BitField Name: TxDE1SigLineID
BitField Type: RW
BitField Desc: Output DS1/E1/J1 Line ID of the conversion.
BitField Bits: [40:32]
--------------------------------------*/
#define cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigLineID_Bit_Start                                      32
#define cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigLineID_Bit_End                                        40
#define cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigLineID_Mask                                      cBit8_0
#define cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigLineID_Shift                                           0
#define cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigLineID_MaxVal                                        0x0
#define cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigLineID_MinVal                                        0x0
#define cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigLineID_RstVal                                        0x0

/*--------------------------------------
BitField Name: TxDE1SigEnb
BitField Type: RW
BitField Desc: 32 signaling enable bit for 32 DS0 in an E1 frame. In DS1 mode,
only 24 LSB bits is used.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigEnb_Bit_Start                                          0
#define cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigEnb_Bit_End                                           31
#define cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigEnb_Mask                                        cBit31_0
#define cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigEnb_Shift                                              0
#define cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigEnb_MaxVal                                    0xffffffff
#define cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigEnb_MinVal                                           0x0
#define cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigEnb_RstVal                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Tx Framer Signaling CPU Insert Enable Control
Reg Addr   : 0x000CFF01
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the CPU signaling insert global enable bit

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_tx_framer_sign_cpuins_en_ctrl_Base                                             0x000CFF01
#define cAf6Reg_dej1_tx_framer_sign_cpuins_en_ctrl                                                  0x000CFF01
#define cAf6Reg_dej1_tx_framer_sign_cpuins_en_ctrl_WidthVal                                                 32
#define cAf6Reg_dej1_tx_framer_sign_cpuins_en_ctrl_WriteMask                                               0x0

/*--------------------------------------
BitField Name: TxDE1SigCPUMd
BitField Type: RW
BitField Desc: Set 1 to enable CPU mode (all signaling is from CPU)
BitField Bits: [0]
--------------------------------------*/
#define cAf6_dej1_tx_framer_sign_cpuins_en_ctrl_TxDE1SigCPUMd_Bit_Start                                       0
#define cAf6_dej1_tx_framer_sign_cpuins_en_ctrl_TxDE1SigCPUMd_Bit_End                                        0
#define cAf6_dej1_tx_framer_sign_cpuins_en_ctrl_TxDE1SigCPUMd_Mask                                       cBit0
#define cAf6_dej1_tx_framer_sign_cpuins_en_ctrl_TxDE1SigCPUMd_Shift                                          0
#define cAf6_dej1_tx_framer_sign_cpuins_en_ctrl_TxDE1SigCPUMd_MaxVal                                       0x1
#define cAf6_dej1_tx_framer_sign_cpuins_en_ctrl_TxDE1SigCPUMd_MinVal                                       0x0
#define cAf6_dej1_tx_framer_sign_cpuins_en_ctrl_TxDE1SigCPUMd_RstVal                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Tx Framer Signaling CPU DS1/E1 signaling Write Control
Reg Addr   : 0x000CFF02
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the CPU signaling write control

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_tx_framer_sign_cpu_de1_wr_ctrl_Base                                            0x000CFF02
#define cAf6Reg_dej1_tx_framer_sign_cpu_de1_wr_ctrl                                                 0x000CFF02
#define cAf6Reg_dej1_tx_framer_sign_cpu_de1_wr_ctrl_WidthVal                                                32
#define cAf6Reg_dej1_tx_framer_sign_cpu_de1_wr_ctrl_WriteMask                                              0x0

/*--------------------------------------
BitField Name: TxDE1SigCPUWrCtrl
BitField Type: RW
BitField Desc: Set 1 if signaling value written to the Signaling buffer in next
CPU cylce is for DS1/J1, set 0 for E1
BitField Bits: [0]
--------------------------------------*/
#define cAf6_dej1_tx_framer_sign_cpu_de1_wr_ctrl_TxDE1SigCPUWrCtrl_Bit_Start                                       0
#define cAf6_dej1_tx_framer_sign_cpu_de1_wr_ctrl_TxDE1SigCPUWrCtrl_Bit_End                                       0
#define cAf6_dej1_tx_framer_sign_cpu_de1_wr_ctrl_TxDE1SigCPUWrCtrl_Mask                                   cBit0
#define cAf6_dej1_tx_framer_sign_cpu_de1_wr_ctrl_TxDE1SigCPUWrCtrl_Shift                                       0
#define cAf6_dej1_tx_framer_sign_cpu_de1_wr_ctrl_TxDE1SigCPUWrCtrl_MaxVal                                     0x1
#define cAf6_dej1_tx_framer_sign_cpu_de1_wr_ctrl_TxDE1SigCPUWrCtrl_MinVal                                     0x0
#define cAf6_dej1_tx_framer_sign_cpu_de1_wr_ctrl_TxDE1SigCPUWrCtrl_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Tx Framer Signaling Buffer
Reg Addr   : 0x000C8000 - 0x000CFF00
Reg Formula: 0x000C8000 +  1024*de3id + 128*de2id + 32*de1id + tscnt
    Where  : 
           + $de3id(0-23):
           + $de2id(0-6):
           + $de1id(0-3):
           + $tscnt(0-23): (ds1) or 0 - 31 (e1)
Reg Desc   : 
DS1/E1/J1 Rx framer signaling ABCD bit store for each DS0 at the DS1/E1 framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_tx_framer_sign_buffer_Base                                                     0x000C8000
#define cAf6Reg_dej1_tx_framer_sign_buffer(de3id, de2id, de1id, tscnt)(0x000C8000+1024*(de3id)+128*(de2id)+32*(de1id)+(tscnt))
#define cAf6Reg_dej1_tx_framer_sign_buffer_WidthVal                                                         32
#define cAf6Reg_dej1_tx_framer_sign_buffer_WriteMask                                                       0x0

/*--------------------------------------
BitField Name: TxDE1SigABCD
BitField Type: RW
BitField Desc: 4-bit ABCD signaling for each DS0 (Note: for DS1/J1, must write
the 0x000C8002 value 1 before writing this ABCD, for E1  must write the
0x000C8002 value 0 before writing this ABCD)
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_dej1_tx_framer_sign_buffer_TxDE1SigABCD_Bit_Start                                               0
#define cAf6_dej1_tx_framer_sign_buffer_TxDE1SigABCD_Bit_End                                                 3
#define cAf6_dej1_tx_framer_sign_buffer_TxDE1SigABCD_Mask                                              cBit3_0
#define cAf6_dej1_tx_framer_sign_buffer_TxDE1SigABCD_Shift                                                   0
#define cAf6_dej1_tx_framer_sign_buffer_TxDE1SigABCD_MaxVal                                                0xf
#define cAf6_dej1_tx_framer_sign_buffer_TxDE1SigABCD_MinVal                                                0x0
#define cAf6_dej1_tx_framer_sign_buffer_TxDE1SigABCD_RstVal                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Test Pattern Generation Control
Reg Addr   : 0x0950FF - 0x0950FF
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to global configuration for PTG engines

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_testpat_gen_ctrl_Base                                                            0x0950FF
#define cAf6Reg_dej1_testpat_gen_ctrl                                                                 0x0950FF
#define cAf6Reg_dej1_testpat_gen_ctrl_WidthVal                                                              32
#define cAf6Reg_dej1_testpat_gen_ctrl_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: TxDE1PtgSwap
BitField Type: RW
BitField Desc:
BitField Bits: [1]
--------------------------------------*/
#define cAf6_dej1_testpat_gen_ctrl_TxDE1PtgSwap_Bit_Start                                                    1
#define cAf6_dej1_testpat_gen_ctrl_TxDE1PtgSwap_Bit_End                                                      1
#define cAf6_dej1_testpat_gen_ctrl_TxDE1PtgSwap_Mask                                                     cBit1
#define cAf6_dej1_testpat_gen_ctrl_TxDE1PtgSwap_Shift                                                        1
#define cAf6_dej1_testpat_gen_ctrl_TxDE1PtgSwap_MaxVal                                                     0x1
#define cAf6_dej1_testpat_gen_ctrl_TxDE1PtgSwap_MinVal                                                     0x0
#define cAf6_dej1_testpat_gen_ctrl_TxDE1PtgSwap_RstVal                                                     0x0

/*--------------------------------------
BitField Name: TxDE1PtgInv
BitField Type: RW
BitField Desc: PTG inversion Bit.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_dej1_testpat_gen_ctrl_TxDE1PtgInv_Bit_Start                                                     0
#define cAf6_dej1_testpat_gen_ctrl_TxDE1PtgInv_Bit_End                                                       0
#define cAf6_dej1_testpat_gen_ctrl_TxDE1PtgInv_Mask                                                      cBit0
#define cAf6_dej1_testpat_gen_ctrl_TxDE1PtgInv_Shift                                                         0
#define cAf6_dej1_testpat_gen_ctrl_TxDE1PtgInv_MaxVal                                                      0x1
#define cAf6_dej1_testpat_gen_ctrl_TxDE1PtgInv_MinVal                                                      0x0
#define cAf6_dej1_testpat_gen_ctrl_TxDE1PtgInv_RstVal                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Test Pattern Generation Single Bit Error Insertion
Reg Addr   : 0x0950FE - 0x0950FE
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to insert a single erroneous bit to DS1/E1/J1 Test Pattern generator.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_testpat_gen_single_biterr_ins_Base                                               0x0950FE
#define cAf6Reg_dej1_testpat_gen_single_biterr_ins                                                    0x0950FE
#define cAf6Reg_dej1_testpat_gen_single_biterr_ins_WidthVal                                                 32
#define cAf6Reg_dej1_testpat_gen_single_biterr_ins_WriteMask                                               0x0

/*--------------------------------------
BitField Name: TxDE1PtmErrIDIns
BitField Type: RW
BitField Desc:
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_dej1_testpat_gen_single_biterr_ins_TxDE1PtmErrIDIns_Bit_Start                                       0
#define cAf6_dej1_testpat_gen_single_biterr_ins_TxDE1PtmErrIDIns_Bit_End                                       3
#define cAf6_dej1_testpat_gen_single_biterr_ins_TxDE1PtmErrIDIns_Mask                                  cBit3_0
#define cAf6_dej1_testpat_gen_single_biterr_ins_TxDE1PtmErrIDIns_Shift                                       0
#define cAf6_dej1_testpat_gen_single_biterr_ins_TxDE1PtmErrIDIns_MaxVal                                     0xf
#define cAf6_dej1_testpat_gen_single_biterr_ins_TxDE1PtmErrIDIns_MinVal                                     0x0
#define cAf6_dej1_testpat_gen_single_biterr_ins_TxDE1PtmErrIDIns_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Test Pattern Generation Mode
Reg Addr   : 0x095080 - 0x09508F
Reg Formula: 0x095080 +  index
    Where  : 
           + $index(0-15):
Reg Desc   : 
This register is used to data mode for PTG block.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_testpat_gen_mode_Base                                                            0x095080
#define cAf6Reg_dej1_testpat_gen_mode(index)                                                (0x095080+(index))
#define cAf6Reg_dej1_testpat_gen_mode_WidthVal                                                              32
#define cAf6Reg_dej1_testpat_gen_mode_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: TxDE1PtmPatBit
BitField Type: RW
BitField Desc: The number of bit pattern is used.
BitField Bits: [9:5]
--------------------------------------*/
#define cAf6_dej1_testpat_gen_mode_TxDE1PtmPatBit_Bit_Start                                                  5
#define cAf6_dej1_testpat_gen_mode_TxDE1PtmPatBit_Bit_End                                                    9
#define cAf6_dej1_testpat_gen_mode_TxDE1PtmPatBit_Mask                                                 cBit9_5
#define cAf6_dej1_testpat_gen_mode_TxDE1PtmPatBit_Shift                                                      5
#define cAf6_dej1_testpat_gen_mode_TxDE1PtmPatBit_MaxVal                                                  0x1f
#define cAf6_dej1_testpat_gen_mode_TxDE1PtmPatBit_MinVal                                                   0x0
#define cAf6_dej1_testpat_gen_mode_TxDE1PtmPatBit_RstVal                                                   0x0

/*--------------------------------------
BitField Name: TxDE1PtgMode
BitField Type: RW
BitField Desc: 00000: Prbs9 00001: Prbs11 00010: Prbs15 00011: Prbs20 (X19 + X2
+ 1) 00100: Prbs20 (X19 + X16 + 1) 00101: Qrss20 (X19 + X16 + 1) 00110: Prbs23
00111: DDS1 01000: DDS2 01001: DDS3 01010: DDS4 01011: DDS5 01100: Daly55 01101:
Fix 3 in 24 01110: Fix 1 in 8 01111: Octet55 10000: Fix pattern n-bit 10001:
Sequence 10010: Prbs7
BitField Bits: [4:0]
--------------------------------------*/
#define cAf6_dej1_testpat_gen_mode_TxDE1PtgMode_Bit_Start                                                    0
#define cAf6_dej1_testpat_gen_mode_TxDE1PtgMode_Bit_End                                                      4
#define cAf6_dej1_testpat_gen_mode_TxDE1PtgMode_Mask                                                   cBit4_0
#define cAf6_dej1_testpat_gen_mode_TxDE1PtgMode_Shift                                                        0
#define cAf6_dej1_testpat_gen_mode_TxDE1PtgMode_MaxVal                                                    0x1f
#define cAf6_dej1_testpat_gen_mode_TxDE1PtgMode_MinVal                                                     0x0
#define cAf6_dej1_testpat_gen_mode_TxDE1PtgMode_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Test Pattern Generation Selected Channel
Reg Addr   : 0x095000 - 0x09500F
Reg Formula: 0x095000 +  index
    Where  : 
           + $index(0-15):
Reg Desc   : 
This register is used to global configuration for PTG engine #0 - 15

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_testpat_gen_sel_chn_Base                                                         0x095000
#define cAf6Reg_dej1_testpat_gen_sel_chn(index)                                             (0x095000+(index))
#define cAf6Reg_dej1_testpat_gen_sel_chn_WidthVal                                                           32
#define cAf6Reg_dej1_testpat_gen_sel_chn_WriteMask                                                         0x0

/*--------------------------------------
BitField Name: TxDE1PtgEn
BitField Type: RW
BitField Desc: PTG Bit Enable.
BitField Bits: [9]
--------------------------------------*/
#define cAf6_dej1_testpat_gen_sel_chn_TxDE1PtgEn_Bit_Start                                                   9
#define cAf6_dej1_testpat_gen_sel_chn_TxDE1PtgEn_Bit_End                                                     9
#define cAf6_dej1_testpat_gen_sel_chn_TxDE1PtgEn_Mask                                                    cBit9
#define cAf6_dej1_testpat_gen_sel_chn_TxDE1PtgEn_Shift                                                       9
#define cAf6_dej1_testpat_gen_sel_chn_TxDE1PtgEn_MaxVal                                                    0x1
#define cAf6_dej1_testpat_gen_sel_chn_TxDE1PtgEn_MinVal                                                    0x0
#define cAf6_dej1_testpat_gen_sel_chn_TxDE1PtgEn_RstVal                                                    0x0

/*--------------------------------------
BitField Name: TxDE1PtgID
BitField Type: RW
BitField Desc: Configure which channel in 336 DS1s/E1s/J1s is inserted a test
pattern.
BitField Bits: [8:0]
--------------------------------------*/
#define cAf6_dej1_testpat_gen_sel_chn_TxDE1PtgID_Bit_Start                                                   0
#define cAf6_dej1_testpat_gen_sel_chn_TxDE1PtgID_Bit_End                                                     8
#define cAf6_dej1_testpat_gen_sel_chn_TxDE1PtgID_Mask                                                  cBit8_0
#define cAf6_dej1_testpat_gen_sel_chn_TxDE1PtgID_Shift                                                       0
#define cAf6_dej1_testpat_gen_sel_chn_TxDE1PtgID_MaxVal                                                  0x1ff
#define cAf6_dej1_testpat_gen_sel_chn_TxDE1PtgID_MinVal                                                    0x0
#define cAf6_dej1_testpat_gen_sel_chn_TxDE1PtgID_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Test Pattern Generation nxDS0 Control
Reg Addr   : 0x095010 - 0x09501F
Reg Formula: 0x095010 +  index
    Where  : 
           + $index(0-15):
Reg Desc   : 
This register is used to configure the mode of nxDS0 in DS1/E1/J1 test pattern generator.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_testpat_gen_nxds0_ctrl_Base                                                      0x095010
#define cAf6Reg_dej1_testpat_gen_nxds0_ctrl(index)                                          (0x095010+(index))
#define cAf6Reg_dej1_testpat_gen_nxds0_ctrl_WidthVal                                                        32
#define cAf6Reg_dej1_testpat_gen_nxds0_ctrl_WriteMask                                                      0x0

/*--------------------------------------
BitField Name: TxDE1PtgDS0_6b
BitField Type: RW
BitField Desc: Transmit Test Pattern from the 2nd bit to the 8th bit
BitField Bits: [1]
--------------------------------------*/
#define cAf6_dej1_testpat_gen_nxds0_ctrl_TxDE1PtgDS0_6b_Bit_Start                                            1
#define cAf6_dej1_testpat_gen_nxds0_ctrl_TxDE1PtgDS0_6b_Bit_End                                              1
#define cAf6_dej1_testpat_gen_nxds0_ctrl_TxDE1PtgDS0_6b_Mask                                             cBit1
#define cAf6_dej1_testpat_gen_nxds0_ctrl_TxDE1PtgDS0_6b_Shift                                                1
#define cAf6_dej1_testpat_gen_nxds0_ctrl_TxDE1PtgDS0_6b_MaxVal                                             0x1
#define cAf6_dej1_testpat_gen_nxds0_ctrl_TxDE1PtgDS0_6b_MinVal                                             0x0
#define cAf6_dej1_testpat_gen_nxds0_ctrl_TxDE1PtgDS0_6b_RstVal                                             0x0

/*--------------------------------------
BitField Name: TxDE1PtgDS0_7b
BitField Type: RW
BitField Desc: Transmit Test Pattern from the 2nd bit to the 7h bit. If DS0_7b
and DS0_6b are not active, engine will transmit Test Pattern from the 1st bit to
the 8th bit
BitField Bits: [0]
--------------------------------------*/
#define cAf6_dej1_testpat_gen_nxds0_ctrl_TxDE1PtgDS0_7b_Bit_Start                                            0
#define cAf6_dej1_testpat_gen_nxds0_ctrl_TxDE1PtgDS0_7b_Bit_End                                              0
#define cAf6_dej1_testpat_gen_nxds0_ctrl_TxDE1PtgDS0_7b_Mask                                             cBit0
#define cAf6_dej1_testpat_gen_nxds0_ctrl_TxDE1PtgDS0_7b_Shift                                                0
#define cAf6_dej1_testpat_gen_nxds0_ctrl_TxDE1PtgDS0_7b_MaxVal                                             0x1
#define cAf6_dej1_testpat_gen_nxds0_ctrl_TxDE1PtgDS0_7b_MinVal                                             0x0
#define cAf6_dej1_testpat_gen_nxds0_ctrl_TxDE1PtgDS0_7b_RstVal                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Test Pattern Generation nxDS0 Concatenation Control
Reg Addr   : 0x095020 - 0x09502F
Reg Formula: 0x095020 +  index
    Where  : 
           + $index(0-15):
Reg Desc   : 
This register is used to be active bit at level DS0 or PTG engine 0. A 32 bits register is dedicated for 32 timeslot in case of frame E1, and 24 bits (bit 24 - 1) is given for 24 timeslot in case of frame DS1. The 32-bits are set 1 to indicate full channel insertion.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_testpat_gen_nxds0_conc_ctrl_Base                                                 0x095020
#define cAf6Reg_dej1_testpat_gen_nxds0_conc_ctrl(index)                                     (0x095020+(index))
#define cAf6Reg_dej1_testpat_gen_nxds0_conc_ctrl_WidthVal                                                   32
#define cAf6Reg_dej1_testpat_gen_nxds0_conc_ctrl_WriteMask                                                 0x0

/*--------------------------------------
BitField Name: RxDE1FixPat
BitField Type: RW
BitField Desc: Configurable fixed pattern for BERT monitoring
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_dej1_testpat_gen_nxds0_conc_ctrl_RxDE1FixPat_Bit_Start                                          0
#define cAf6_dej1_testpat_gen_nxds0_conc_ctrl_RxDE1FixPat_Bit_End                                           31
#define cAf6_dej1_testpat_gen_nxds0_conc_ctrl_RxDE1FixPat_Mask                                        cBit31_0
#define cAf6_dej1_testpat_gen_nxds0_conc_ctrl_RxDE1FixPat_Shift                                              0
#define cAf6_dej1_testpat_gen_nxds0_conc_ctrl_RxDE1FixPat_MaxVal                                    0xffffffff
#define cAf6_dej1_testpat_gen_nxds0_conc_ctrl_RxDE1FixPat_MinVal                                           0x0
#define cAf6_dej1_testpat_gen_nxds0_conc_ctrl_RxDE1FixPat_RstVal                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Test Pattern Generation Fixed Pattern Control
Reg Addr   : 0x095090 - 0x09509F
Reg Formula: 0x095090 +  index
    Where  : 
           + $index(0-15):
Reg Desc   : 
This register is used to be transmitted data in fix pattern mode for engine #0 - 15

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_testpat_gen_fixedpat_ctrl_Base                                                   0x095090
#define cAf6Reg_dej1_testpat_gen_fixedpat_ctrl(index)                                       (0x095090+(index))
#define cAf6Reg_dej1_testpat_gen_fixedpat_ctrl_WidthVal                                                     32
#define cAf6Reg_dej1_testpat_gen_fixedpat_ctrl_WriteMask                                                   0x0

/*--------------------------------------
BitField Name: RxDE1FixPat
BitField Type: RW
BitField Desc: Configurable fixed pattern for BERT monitoring.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_dej1_testpat_gen_fixedpat_ctrl_RxDE1FixPat_Bit_Start                                            0
#define cAf6_dej1_testpat_gen_fixedpat_ctrl_RxDE1FixPat_Bit_End                                             31
#define cAf6_dej1_testpat_gen_fixedpat_ctrl_RxDE1FixPat_Mask                                          cBit31_0
#define cAf6_dej1_testpat_gen_fixedpat_ctrl_RxDE1FixPat_Shift                                                0
#define cAf6_dej1_testpat_gen_fixedpat_ctrl_RxDE1FixPat_MaxVal                                      0xffffffff
#define cAf6_dej1_testpat_gen_fixedpat_ctrl_RxDE1FixPat_MinVal                                             0x0
#define cAf6_dej1_testpat_gen_fixedpat_ctrl_RxDE1FixPat_RstVal                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Test Pattern Generation Error Rate Insertion
Reg Addr   : 0x0950A0 - 0x0950AF
Reg Formula: 0x0950A0 +  index
    Where  : 
           + $index(0-15):
Reg Desc   : 
This register is used to control register for error insertion

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_testpat_gen_errrate_inst_Base                                                    0x0950A0
#define cAf6Reg_dej1_testpat_gen_errrate_inst(index)                                        (0x0950A0+(index))
#define cAf6Reg_dej1_testpat_gen_errrate_inst_WidthVal                                                      32
#define cAf6Reg_dej1_testpat_gen_errrate_inst_WriteMask                                                    0x0

/*--------------------------------------
BitField Name: TxDE1BerErr
BitField Type: RW
BitField Desc: BER enable insertion for engine n. Single bit error can occur
during BER insertion.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_dej1_testpat_gen_errrate_inst_TxDE1BerErr_Bit_Start                                             3
#define cAf6_dej1_testpat_gen_errrate_inst_TxDE1BerErr_Bit_End                                               3
#define cAf6_dej1_testpat_gen_errrate_inst_TxDE1BerErr_Mask                                              cBit3
#define cAf6_dej1_testpat_gen_errrate_inst_TxDE1BerErr_Shift                                                 3
#define cAf6_dej1_testpat_gen_errrate_inst_TxDE1BerErr_MaxVal                                              0x1
#define cAf6_dej1_testpat_gen_errrate_inst_TxDE1BerErr_MinVal                                              0x0
#define cAf6_dej1_testpat_gen_errrate_inst_TxDE1BerErr_RstVal                                              0x0

/*--------------------------------------
BitField Name: TxDE1BerMd
BitField Type: RW
BitField Desc: Bit Error Rate inserted to Pattern Generator 000: BER 10-2 001:
BER 10-3 010: BER 10-4 011: BER 10-5 100: BER 10-6 101: BER 10-7 11x: BER 10-7
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_dej1_testpat_gen_errrate_inst_TxDE1BerMd_Bit_Start                                              0
#define cAf6_dej1_testpat_gen_errrate_inst_TxDE1BerMd_Bit_End                                                2
#define cAf6_dej1_testpat_gen_errrate_inst_TxDE1BerMd_Mask                                             cBit2_0
#define cAf6_dej1_testpat_gen_errrate_inst_TxDE1BerMd_Shift                                                  0
#define cAf6_dej1_testpat_gen_errrate_inst_TxDE1BerMd_MaxVal                                               0x7
#define cAf6_dej1_testpat_gen_errrate_inst_TxDE1BerMd_MinVal                                               0x0
#define cAf6_dej1_testpat_gen_errrate_inst_TxDE1BerMd_RstVal                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Test Pattern Generation Timing Status
Reg Addr   : 0x615030 - 0x65503F
Reg Formula: 0x615030 +  index
    Where  : 
           + $index(0-15):
Reg Desc   : 
These registers are used for Hardware tus only

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_testpat_gen_timing_stat_Base                                                     0x615030
#define cAf6Reg_dej1_testpat_gen_timing_stat(index)                                         (0x615030+(index))
#define cAf6Reg_dej1_testpat_gen_timing_stat_WidthVal                                                       32
#define cAf6Reg_dej1_testpat_gen_timing_stat_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: tus
BitField Type: RW
BitField Desc: status
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_dej1_testpat_gen_timing_stat_tus_Bit_Start                                                      0
#define cAf6_dej1_testpat_gen_timing_stat_tus_Bit_End                                                        3
#define cAf6_dej1_testpat_gen_timing_stat_tus_Mask                                                     cBit3_0
#define cAf6_dej1_testpat_gen_timing_stat_tus_Shift                                                          0
#define cAf6_dej1_testpat_gen_timing_stat_tus_MaxVal                                                       0xf
#define cAf6_dej1_testpat_gen_timing_stat_tus_MinVal                                                       0x0
#define cAf6_dej1_testpat_gen_timing_stat_tus_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Test Pattern Generation Status
Reg Addr   : 0x0950C0 - 0x0950EF
Reg Formula: 0x0950C0 + 16*high + index
    Where  : 
           + $high(0-1):
           + $index(0-15):
Reg Desc   : 
These registers are used for Hardware tus only

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_testpat_gen_stat_Base                                                            0x0950C0
#define cAf6Reg_dej1_testpat_gen_stat(high, index)                                (0x0950C0+16*(high)+(index))
#define cAf6Reg_dej1_testpat_gen_stat_WidthVal                                                              32
#define cAf6Reg_dej1_testpat_gen_stat_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: tus
BitField Type: RW
BitField Desc: status
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_dej1_testpat_gen_stat_tus_Bit_Start                                                             0
#define cAf6_dej1_testpat_gen_stat_tus_Bit_End                                                               3
#define cAf6_dej1_testpat_gen_stat_tus_Mask                                                            cBit3_0
#define cAf6_dej1_testpat_gen_stat_tus_Shift                                                                 0
#define cAf6_dej1_testpat_gen_stat_tus_MaxVal                                                              0xf
#define cAf6_dej1_testpat_gen_stat_tus_MinVal                                                              0x0
#define cAf6_dej1_testpat_gen_stat_tus_RstVal                                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Payload Error Insert Enable Configuration
Reg Addr   : 0x00090002
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_payload_errins_en_cfg_Base                                                     0x00090002
#define cAf6Reg_dej1_payload_errins_en_cfg                                                          0x00090002
#define cAf6Reg_dej1_payload_errins_en_cfg_WidthVal                                                         32
#define cAf6Reg_dej1_payload_errins_en_cfg_WriteMask                                                       0x0

/*--------------------------------------
BitField Name: TxDE1PayErrInsID
BitField Type: RW
BitField Desc:
BitField Bits: [8:0]
--------------------------------------*/
#define cAf6_dej1_payload_errins_en_cfg_TxDE1PayErrInsID_Bit_Start                                           0
#define cAf6_dej1_payload_errins_en_cfg_TxDE1PayErrInsID_Bit_End                                             8
#define cAf6_dej1_payload_errins_en_cfg_TxDE1PayErrInsID_Mask                                          cBit8_0
#define cAf6_dej1_payload_errins_en_cfg_TxDE1PayErrInsID_Shift                                               0
#define cAf6_dej1_payload_errins_en_cfg_TxDE1PayErrInsID_MaxVal                                          0x1ff
#define cAf6_dej1_payload_errins_en_cfg_TxDE1PayErrInsID_MinVal                                            0x0
#define cAf6_dej1_payload_errins_en_cfg_TxDE1PayErrInsID_RstVal                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Tx Framer DLK Indirect Access Control
Reg Addr   : 0x00090008
Reg Formula: 
    Where  : 
Reg Desc   : 
The register holds read/write/address command to access registers that are required to read or write indirectly. %%
When writing a value into indirect registers, data needs to be written into the DLK Indirect Access Write Data Control register, address and command are then written into this register with 1 on bit DLKRdWrPend. Until hardware completes the write cycle, bit DLKRdWrPend is clear to 0.  %%
When reading indirect registers, address and command are written into this register with 1 on bit DLKRdWrPend. Until hardware completes the read cycle, bit DLKRdWrPend is clear to 0. Read data is ready on the DLK Indirect Access Read Data Status register.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_tx_framer_dlk_indir_acc_ctrl_Base                                              0x00090008
#define cAf6Reg_dej1_tx_framer_dlk_indir_acc_ctrl                                                   0x00090008
#define cAf6Reg_dej1_tx_framer_dlk_indir_acc_ctrl_WidthVal                                                  32
#define cAf6Reg_dej1_tx_framer_dlk_indir_acc_ctrl_WriteMask                                                0x0

/*--------------------------------------
BitField Name: DLKRdWrPend
BitField Type: RW
BitField Desc: Read/Write Pending 1: Read/Write has been pending 0: Read/Write
is done
BitField Bits: [15]
--------------------------------------*/
#define cAf6_dej1_tx_framer_dlk_indir_acc_ctrl_DLKRdWrPend_Bit_Start                                        15
#define cAf6_dej1_tx_framer_dlk_indir_acc_ctrl_DLKRdWrPend_Bit_End                                          15
#define cAf6_dej1_tx_framer_dlk_indir_acc_ctrl_DLKRdWrPend_Mask                                         cBit15
#define cAf6_dej1_tx_framer_dlk_indir_acc_ctrl_DLKRdWrPend_Shift                                            15
#define cAf6_dej1_tx_framer_dlk_indir_acc_ctrl_DLKRdWrPend_MaxVal                                          0x1
#define cAf6_dej1_tx_framer_dlk_indir_acc_ctrl_DLKRdWrPend_MinVal                                          0x0
#define cAf6_dej1_tx_framer_dlk_indir_acc_ctrl_DLKRdWrPend_RstVal                                          0x0

/*--------------------------------------
BitField Name: DLKWnR
BitField Type: RW
BitField Desc: Write not Read. This bit is used to control the hardware to read
or write an indirect register. 1: Read an indirect register 0: Write to an
indirect register
BitField Bits: [12]
--------------------------------------*/
#define cAf6_dej1_tx_framer_dlk_indir_acc_ctrl_DLKWnR_Bit_Start                                             12
#define cAf6_dej1_tx_framer_dlk_indir_acc_ctrl_DLKWnR_Bit_End                                               12
#define cAf6_dej1_tx_framer_dlk_indir_acc_ctrl_DLKWnR_Mask                                              cBit12
#define cAf6_dej1_tx_framer_dlk_indir_acc_ctrl_DLKWnR_Shift                                                 12
#define cAf6_dej1_tx_framer_dlk_indir_acc_ctrl_DLKWnR_MaxVal                                               0x1
#define cAf6_dej1_tx_framer_dlk_indir_acc_ctrl_DLKWnR_MinVal                                               0x0
#define cAf6_dej1_tx_framer_dlk_indir_acc_ctrl_DLKWnR_RstVal                                               0x0

/*--------------------------------------
BitField Name: DLKRegAdr
BitField Type: RW
BitField Desc: Register Address
BitField Bits: [8:0]
--------------------------------------*/
#define cAf6_dej1_tx_framer_dlk_indir_acc_ctrl_DLKRegAdr_Bit_Start                                           0
#define cAf6_dej1_tx_framer_dlk_indir_acc_ctrl_DLKRegAdr_Bit_End                                             8
#define cAf6_dej1_tx_framer_dlk_indir_acc_ctrl_DLKRegAdr_Mask                                          cBit8_0
#define cAf6_dej1_tx_framer_dlk_indir_acc_ctrl_DLKRegAdr_Shift                                               0
#define cAf6_dej1_tx_framer_dlk_indir_acc_ctrl_DLKRegAdr_MaxVal                                          0x1ff
#define cAf6_dej1_tx_framer_dlk_indir_acc_ctrl_DLKRegAdr_MinVal                                            0x0
#define cAf6_dej1_tx_framer_dlk_indir_acc_ctrl_DLKRegAdr_RstVal                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Tx Framer DLK Indirect Access Write Data Control
Reg Addr   : 0x00090009
Reg Formula: 
    Where  : 
Reg Desc   : 
The registers hold data value in order to write this value to indirect registers. This register is used in conjunction with the DLK Indirect Access Control register.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_tx_framer_dlk_indir_acc_wr_data_ctrl_Base                                      0x00090009
#define cAf6Reg_dej1_tx_framer_dlk_indir_acc_wr_data_ctrl                                           0x00090009
#define cAf6Reg_dej1_tx_framer_dlk_indir_acc_wr_data_ctrl_WidthVal                                          32
#define cAf6Reg_dej1_tx_framer_dlk_indir_acc_wr_data_ctrl_WriteMask                                        0x0

/*--------------------------------------
BitField Name: WrHoldDat
BitField Type: RW
BitField Desc: Write Access Holding Data
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_dej1_tx_framer_dlk_indir_acc_wr_data_ctrl_WrHoldDat_Bit_Start                                       0
#define cAf6_dej1_tx_framer_dlk_indir_acc_wr_data_ctrl_WrHoldDat_Bit_End                                      15
#define cAf6_dej1_tx_framer_dlk_indir_acc_wr_data_ctrl_WrHoldDat_Mask                                 cBit15_0
#define cAf6_dej1_tx_framer_dlk_indir_acc_wr_data_ctrl_WrHoldDat_Shift                                       0
#define cAf6_dej1_tx_framer_dlk_indir_acc_wr_data_ctrl_WrHoldDat_MaxVal                                  0xffff
#define cAf6_dej1_tx_framer_dlk_indir_acc_wr_data_ctrl_WrHoldDat_MinVal                                     0x0
#define cAf6_dej1_tx_framer_dlk_indir_acc_wr_data_ctrl_WrHoldDat_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Tx Framer DLK Indirect Access Read Data Status
Reg Addr   : 0x0009000A
Reg Formula: 
    Where  : 
Reg Desc   : 
The registers hold data value after reading indirect registers. This register is used in conjunction with the DLK Indirect Access Control register.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_tx_framer_dlk_indir_acc_rd_data_stat_Base                                      0x0009000A
#define cAf6Reg_dej1_tx_framer_dlk_indir_acc_rd_data_stat                                           0x0009000A
#define cAf6Reg_dej1_tx_framer_dlk_indir_acc_rd_data_stat_WidthVal                                          32
#define cAf6Reg_dej1_tx_framer_dlk_indir_acc_rd_data_stat_WriteMask                                        0x0

/*--------------------------------------
BitField Name: RdHoldDat
BitField Type: RW
BitField Desc: Read Access Holding Data
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_dej1_tx_framer_dlk_indir_acc_rd_data_stat_RdHoldDat_Bit_Start                                       0
#define cAf6_dej1_tx_framer_dlk_indir_acc_rd_data_stat_RdHoldDat_Bit_End                                      15
#define cAf6_dej1_tx_framer_dlk_indir_acc_rd_data_stat_RdHoldDat_Mask                                 cBit15_0
#define cAf6_dej1_tx_framer_dlk_indir_acc_rd_data_stat_RdHoldDat_Shift                                       0
#define cAf6_dej1_tx_framer_dlk_indir_acc_rd_data_stat_RdHoldDat_MaxVal                                  0xffff
#define cAf6_dej1_tx_framer_dlk_indir_acc_rd_data_stat_RdHoldDat_MinVal                                     0x0
#define cAf6_dej1_tx_framer_dlk_indir_acc_rd_data_stat_RdHoldDat_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Tx Framer DLK Insertion Control
Reg Addr   : 0x096020-0x09602F
Reg Formula: 
    Where  : 
Reg Desc   : 
These registers are used to control DLK insertion engines.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_tx_framer_dlk_ins_ctrl_Base                                                      0x096020
#define cAf6Reg_dej1_tx_framer_dlk_ins_ctrl                                                           0x096020
#define cAf6Reg_dej1_tx_framer_dlk_ins_ctrl_WidthVal                                                        32
#define cAf6Reg_dej1_tx_framer_dlk_ins_ctrl_WriteMask                                                      0x0

/*--------------------------------------
BitField Name: DlkMsgLength
BitField Type: RW
BitField Desc: length of message to transmit in byte. The maximum length is 128
in byte.
BitField Bits: [15:9]
--------------------------------------*/
#define cAf6_dej1_tx_framer_dlk_ins_ctrl_DlkMsgLength_Bit_Start                                              9
#define cAf6_dej1_tx_framer_dlk_ins_ctrl_DlkMsgLength_Bit_End                                               15
#define cAf6_dej1_tx_framer_dlk_ins_ctrl_DlkMsgLength_Mask                                            cBit15_9
#define cAf6_dej1_tx_framer_dlk_ins_ctrl_DlkMsgLength_Shift                                                  9
#define cAf6_dej1_tx_framer_dlk_ins_ctrl_DlkMsgLength_MaxVal                                              0x7f
#define cAf6_dej1_tx_framer_dlk_ins_ctrl_DlkMsgLength_MinVal                                               0x0
#define cAf6_dej1_tx_framer_dlk_ins_ctrl_DlkMsgLength_RstVal                                               0x0

/*--------------------------------------
BitField Name: DlkDE1ID
BitField Type: RW
BitField Desc: To configure which DS1/E1 ID is selected to insert DLK. At a
time, user can choose 16 DS1/E1s to carry DLK message.
BitField Bits: [8:0]
--------------------------------------*/
#define cAf6_dej1_tx_framer_dlk_ins_ctrl_DlkDE1ID_Bit_Start                                                  0
#define cAf6_dej1_tx_framer_dlk_ins_ctrl_DlkDE1ID_Bit_End                                                    8
#define cAf6_dej1_tx_framer_dlk_ins_ctrl_DlkDE1ID_Mask                                                 cBit8_0
#define cAf6_dej1_tx_framer_dlk_ins_ctrl_DlkDE1ID_Shift                                                      0
#define cAf6_dej1_tx_framer_dlk_ins_ctrl_DlkDE1ID_MaxVal                                                 0x1ff
#define cAf6_dej1_tx_framer_dlk_ins_ctrl_DlkDE1ID_MinVal                                                   0x0
#define cAf6_dej1_tx_framer_dlk_ins_ctrl_DlkDE1ID_RstVal                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Tx Framer DLK Insertion Enable
Reg Addr   : 0x00096010
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to configure enable or disable DLK insertion engines. This configuration is on per engine.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_tx_framer_dlk_ins_en_Base                                                      0x00096010
#define cAf6Reg_dej1_tx_framer_dlk_ins_en                                                           0x00096010
#define cAf6Reg_dej1_tx_framer_dlk_ins_en_WidthVal                                                          32
#define cAf6Reg_dej1_tx_framer_dlk_ins_en_WriteMask                                                        0x0

/*--------------------------------------
BitField Name: DlkInsEn
BitField Type: RW
BitField Desc: 1: engine is ready to insert DLK 0: engine is disable to insert
DLK.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_dej1_tx_framer_dlk_ins_en_DlkInsEn_Bit_Start                                                    0
#define cAf6_dej1_tx_framer_dlk_ins_en_DlkInsEn_Bit_End                                                     15
#define cAf6_dej1_tx_framer_dlk_ins_en_DlkInsEn_Mask                                                  cBit15_0
#define cAf6_dej1_tx_framer_dlk_ins_en_DlkInsEn_Shift                                                        0
#define cAf6_dej1_tx_framer_dlk_ins_en_DlkInsEn_MaxVal                                                  0xffff
#define cAf6_dej1_tx_framer_dlk_ins_en_DlkInsEn_MinVal                                                     0x0
#define cAf6_dej1_tx_framer_dlk_ins_en_DlkInsEn_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Tx Framer DLK Insertion New Message Start Enable
Reg Addr   : 0x00096011
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to configure DLK insertion engines to transmit a new message. By setting a bit in this register to 1, DLK engine (appropriate to that bit) will get message in the message memory to insert into the configured DS1/E1 frame. When transmitting finishes, engine automatically clears this bit to inform to CPU that the DLK message has been already transmitted.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_tx_framer_dlk_ins_newmsg_start_en_Base                                         0x00096011
#define cAf6Reg_dej1_tx_framer_dlk_ins_newmsg_start_en                                              0x00096011
#define cAf6Reg_dej1_tx_framer_dlk_ins_newmsg_start_en_WidthVal                                             32
#define cAf6Reg_dej1_tx_framer_dlk_ins_newmsg_start_en_WriteMask                                           0x0

/*--------------------------------------
BitField Name: DlkNewMsgStartEn
BitField Type: RW
BitField Desc: DlkNewMsgStartEn
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_dej1_tx_framer_dlk_ins_newmsg_start_en_DlkNewMsgStartEn_Bit_Start                                       0
#define cAf6_dej1_tx_framer_dlk_ins_newmsg_start_en_DlkNewMsgStartEn_Bit_End                                      15
#define cAf6_dej1_tx_framer_dlk_ins_newmsg_start_en_DlkNewMsgStartEn_Mask                                cBit15_0
#define cAf6_dej1_tx_framer_dlk_ins_newmsg_start_en_DlkNewMsgStartEn_Shift                                       0
#define cAf6_dej1_tx_framer_dlk_ins_newmsg_start_en_DlkNewMsgStartEn_MaxVal                                  0xffff
#define cAf6_dej1_tx_framer_dlk_ins_newmsg_start_en_DlkNewMsgStartEn_MinVal                                     0x0
#define cAf6_dej1_tx_framer_dlk_ins_newmsg_start_en_DlkNewMsgStartEn_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Tx Framer DLK Insertion Engine Status
Reg Addr   : 0x00096012
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to inform to CPU the tus of DLK insertion engine. Before using an engine to transmit DLK for a DS1/E1 channel, CPU must read this register to get the status of that engin

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_tx_framer_dlk_ins_eng_stat_Base                                                0x00096012
#define cAf6Reg_dej1_tx_framer_dlk_ins_eng_stat                                                     0x00096012
#define cAf6Reg_dej1_tx_framer_dlk_ins_eng_stat_WidthVal                                                    32
#define cAf6Reg_dej1_tx_framer_dlk_ins_eng_stat_WriteMask                                                  0x0

/*--------------------------------------
BitField Name: DlkEngSta
BitField Type: RW
BitField Desc: 16-bit corresponds to 16 engines 1: Engine is busy. 0: Engine is
free
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_dej1_tx_framer_dlk_ins_eng_stat_DlkEngSta_Bit_Start                                             0
#define cAf6_dej1_tx_framer_dlk_ins_eng_stat_DlkEngSta_Bit_End                                              15
#define cAf6_dej1_tx_framer_dlk_ins_eng_stat_DlkEngSta_Mask                                           cBit15_0
#define cAf6_dej1_tx_framer_dlk_ins_eng_stat_DlkEngSta_Shift                                                 0
#define cAf6_dej1_tx_framer_dlk_ins_eng_stat_DlkEngSta_MaxVal                                           0xffff
#define cAf6_dej1_tx_framer_dlk_ins_eng_stat_DlkEngSta_MinVal                                              0x0
#define cAf6_dej1_tx_framer_dlk_ins_eng_stat_DlkEngSta_RstVal                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Tx Framer DLK Insertion Message Memory
Reg Addr   : 0x096800-0x096FFF
Reg Formula: 0x096800 +  msgid*128 + byteid
    Where  : 
           + $msgid(0-15):
           + $byteid(0-127):
Reg Desc   : 
This memory contains data messages; each DLK insertion engine has a 128-byte message.e whether or not it is being usued by another DS1/E1 channel.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_tx_framer_dlk_ins_msg_mem_Base                                                   0x096800
#define cAf6Reg_dej1_tx_framer_dlk_ins_msg_mem(msgid, byteid)                  (0x096800+(msgid)*128+(byteid))
#define cAf6Reg_dej1_tx_framer_dlk_ins_msg_mem_WidthVal                                                     32
#define cAf6Reg_dej1_tx_framer_dlk_ins_msg_mem_WriteMask                                                   0x0

/*--------------------------------------
BitField Name: DlkInsMsg
BitField Type: RW
BitField Desc: DlkInsMsg
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_dej1_tx_framer_dlk_ins_msg_mem_DlkInsMsg_Bit_Start                                              0
#define cAf6_dej1_tx_framer_dlk_ins_msg_mem_DlkInsMsg_Bit_End                                                7
#define cAf6_dej1_tx_framer_dlk_ins_msg_mem_DlkInsMsg_Mask                                             cBit7_0
#define cAf6_dej1_tx_framer_dlk_ins_msg_mem_DlkInsMsg_Shift                                                  0
#define cAf6_dej1_tx_framer_dlk_ins_msg_mem_DlkInsMsg_MaxVal                                              0xff
#define cAf6_dej1_tx_framer_dlk_ins_msg_mem_DlkInsMsg_MinVal                                               0x0
#define cAf6_dej1_tx_framer_dlk_ins_msg_mem_DlkInsMsg_RstVal                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Tx Framer DLK Insertion BOM Control
Reg Addr   : 0x097000-0x0973FF
Reg Formula: 0x097000 +  de3id*32 + de2id*4 + de1id
    Where  : 
           + $de3id(0-23):
           + $de2id(0-6):
           + $de1id(0-3):
Reg Desc   : 
This memory is used to control transmitting Bit-Oriented Message (BOM). This memory is accessed indirectly via indirect Access registers

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_tx_framer_dlk_ins_bom_ctrl_Base                                                  0x097000
#define cAf6Reg_dej1_tx_framer_dlk_ins_bom_ctrl(de3id, de2id, de1id)   (0x097000+(de3id)*32+(de2id)*4+(de1id))
#define cAf6Reg_dej1_tx_framer_dlk_ins_bom_ctrl_WidthVal                                                    32
#define cAf6Reg_dej1_tx_framer_dlk_ins_bom_ctrl_WriteMask                                                  0x0

/*--------------------------------------
BitField Name: DlkBOMEn
BitField Type: RW
BitField Desc: enable transmitting BOM. By setting this field to 1, DLK engine
will rt to insert 6-bit into the BOM pattern 111111110xxxxxx0 with the MSB is
transmitted first. When transmitting finishes, engine automatically clears this
bit to inform to CPU that the BOM message has been already transmitted.
BitField Bits: [10]
--------------------------------------*/
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMEn_Bit_Start                                             10
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMEn_Bit_End                                               10
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMEn_Mask                                              cBit10
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMEn_Shift                                                 10
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMEn_MaxVal                                               0x1
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMEn_MinVal                                               0x0
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMEn_RstVal                                               0x0

/*--------------------------------------
BitField Name: DlkBOMRptTime
BitField Type: RW
BitField Desc: indicate the number of time that the BOM is transmitted
repeatedly.
BitField Bits: [9:6]
--------------------------------------*/
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMRptTime_Bit_Start                                         6
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMRptTime_Bit_End                                           9
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMRptTime_Mask                                        cBit9_6
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMRptTime_Shift                                             6
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMRptTime_MaxVal                                          0xf
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMRptTime_MinVal                                          0x0
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMRptTime_RstVal                                          0x0

/*--------------------------------------
BitField Name: DlkBOMMsg
BitField Type: RW
BitField Desc: 6-bit BOM message in pattern BOM 111111110xxxxxx0 in which the
MSB is transmitted first.
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMMsg_Bit_Start                                             0
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMMsg_Bit_End                                               5
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMMsg_Mask                                            cBit5_0
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMMsg_Shift                                                 0
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMMsg_MaxVal                                             0x3f
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMMsg_MinVal                                              0x0
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMMsg_RstVal                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : TxM23E23 Control
Reg Addr   : 0x00080000 - 0x0008001F
Reg Formula: 0x00080000 +  de3id
    Where  : 
           + $de3id(0-23)
Reg Desc   : 
The TxM23E23 Control is use to configure for per channel Tx DS3/E3 operation.

------------------------------------------------------------------------------*/
#define cAf6Reg_txm23e23_ctrl_Base                                                                  0x00080000
#define cAf6Reg_txm23e23_ctrl(de3id)                                                      (0x00080000+(de3id))
#define cAf6Reg_txm23e23_ctrl_WidthVal                                                                     128
#define cAf6Reg_txm23e23_ctrl_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: TxDS3E3DLKIdleByte
BitField Type: RW
BitField Desc:
BitField Bits: [36:29]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3DLKIdleByte_Bit_Start                                                     29
#define cAf6_txm23e23_ctrl_TxDS3E3DLKIdleByte_Bit_End                                                       36
#define cAf6_txm23e23_ctrl_TxDS3E3DLKIdleByte_Mask_01                                                cBit31_29
#define cAf6_txm23e23_ctrl_TxDS3E3DLKIdleByte_Shift_01                                                      29
#define cAf6_txm23e23_ctrl_TxDS3E3DLKIdleByte_Mask_02                                                  cBit4_0
#define cAf6_txm23e23_ctrl_TxDS3E3DLKIdleByte_Shift_02                                                       0

/*--------------------------------------
BitField Name: TxDS3E3ChEnb
BitField Type: RW
BitField Desc:
BitField Bits: [28]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3ChEnb_Bit_Start                                                           28
#define cAf6_txm23e23_ctrl_TxDS3E3ChEnb_Bit_End                                                             28
#define cAf6_txm23e23_ctrl_TxDS3E3ChEnb_Mask                                                            cBit28
#define cAf6_txm23e23_ctrl_TxDS3E3ChEnb_Shift                                                               28
#define cAf6_txm23e23_ctrl_TxDS3E3ChEnb_MaxVal                                                             0x1
#define cAf6_txm23e23_ctrl_TxDS3E3ChEnb_MinVal                                                             0x0
#define cAf6_txm23e23_ctrl_TxDS3E3ChEnb_RstVal                                                             0x0

/*--------------------------------------
BitField Name: TxDS3E3DLKMode
BitField Type: RW
BitField Desc: 0:DS3DL/E3G751NA/E3G832GC; 1:DS3UDL/E3G832NR; 2: DS3FEAC; 3:DS3NA
BitField Bits: [27:26]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3DLKMode_Bit_Start                                                         26
#define cAf6_txm23e23_ctrl_TxDS3E3DLKMode_Bit_End                                                           27
#define cAf6_txm23e23_ctrl_TxDS3E3DLKMode_Mask                                                       cBit27_26
#define cAf6_txm23e23_ctrl_TxDS3E3DLKMode_Shift                                                             26
#define cAf6_txm23e23_ctrl_TxDS3E3DLKMode_MaxVal                                                           0x3
#define cAf6_txm23e23_ctrl_TxDS3E3DLKMode_MinVal                                                           0x0
#define cAf6_txm23e23_ctrl_TxDS3E3DLKMode_RstVal                                                           0x0

/*--------------------------------------
BitField Name: TxDS3E3DLKEnb
BitField Type: RW
BitField Desc:
BitField Bits: [25]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3DLKEnb_Bit_Start                                                          25
#define cAf6_txm23e23_ctrl_TxDS3E3DLKEnb_Bit_End                                                            25
#define cAf6_txm23e23_ctrl_TxDS3E3DLKEnb_Mask                                                           cBit25
#define cAf6_txm23e23_ctrl_TxDS3E3DLKEnb_Shift                                                              25
#define cAf6_txm23e23_ctrl_TxDS3E3DLKEnb_MaxVal                                                            0x1
#define cAf6_txm23e23_ctrl_TxDS3E3DLKEnb_MinVal                                                            0x0
#define cAf6_txm23e23_ctrl_TxDS3E3DLKEnb_RstVal                                                            0x0

/*--------------------------------------
BitField Name: TxDS3E3FeacThres
BitField Type: RW
BitField Desc:
BitField Bits: [24:21]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3FeacThres_Bit_Start                                                       21
#define cAf6_txm23e23_ctrl_TxDS3E3FeacThres_Bit_End                                                         24
#define cAf6_txm23e23_ctrl_TxDS3E3FeacThres_Mask                                                     cBit24_21
#define cAf6_txm23e23_ctrl_TxDS3E3FeacThres_Shift                                                           21
#define cAf6_txm23e23_ctrl_TxDS3E3FeacThres_MaxVal                                                         0xf
#define cAf6_txm23e23_ctrl_TxDS3E3FeacThres_MinVal                                                         0x0
#define cAf6_txm23e23_ctrl_TxDS3E3FeacThres_RstVal                                                         0x0

/*--------------------------------------
BitField Name: TxDS3E3LineAllOne
BitField Type: RW
BitField Desc:
BitField Bits: [20]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3LineAllOne_Bit_Start                                                      20
#define cAf6_txm23e23_ctrl_TxDS3E3LineAllOne_Bit_End                                                        20
#define cAf6_txm23e23_ctrl_TxDS3E3LineAllOne_Mask                                                       cBit20
#define cAf6_txm23e23_ctrl_TxDS3E3LineAllOne_Shift                                                          20
#define cAf6_txm23e23_ctrl_TxDS3E3LineAllOne_MaxVal                                                        0x1
#define cAf6_txm23e23_ctrl_TxDS3E3LineAllOne_MinVal                                                        0x0
#define cAf6_txm23e23_ctrl_TxDS3E3LineAllOne_RstVal                                                        0x0

/*--------------------------------------
BitField Name: TxDS3E3PayLoop
BitField Type: RW
BitField Desc:
BitField Bits: [19]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3PayLoop_Bit_Start                                                         19
#define cAf6_txm23e23_ctrl_TxDS3E3PayLoop_Bit_End                                                           19
#define cAf6_txm23e23_ctrl_TxDS3E3PayLoop_Mask                                                          cBit19
#define cAf6_txm23e23_ctrl_TxDS3E3PayLoop_Shift                                                             19
#define cAf6_txm23e23_ctrl_TxDS3E3PayLoop_MaxVal                                                           0x1
#define cAf6_txm23e23_ctrl_TxDS3E3PayLoop_MinVal                                                           0x0
#define cAf6_txm23e23_ctrl_TxDS3E3PayLoop_RstVal                                                           0x0

/*--------------------------------------
BitField Name: TxDS3E3LineLoop
BitField Type: RW
BitField Desc:
BitField Bits: [18]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3LineLoop_Bit_Start                                                        18
#define cAf6_txm23e23_ctrl_TxDS3E3LineLoop_Bit_End                                                          18
#define cAf6_txm23e23_ctrl_TxDS3E3LineLoop_Mask                                                         cBit18
#define cAf6_txm23e23_ctrl_TxDS3E3LineLoop_Shift                                                            18
#define cAf6_txm23e23_ctrl_TxDS3E3LineLoop_MaxVal                                                          0x1
#define cAf6_txm23e23_ctrl_TxDS3E3LineLoop_MinVal                                                          0x0
#define cAf6_txm23e23_ctrl_TxDS3E3LineLoop_RstVal                                                          0x0

/*--------------------------------------
BitField Name: TxDS3E3FrcYel
BitField Type: RW
BitField Desc:
BitField Bits: [17]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3FrcYel_Bit_Start                                                          17
#define cAf6_txm23e23_ctrl_TxDS3E3FrcYel_Bit_End                                                            17
#define cAf6_txm23e23_ctrl_TxDS3E3FrcYel_Mask                                                           cBit17
#define cAf6_txm23e23_ctrl_TxDS3E3FrcYel_Shift                                                              17
#define cAf6_txm23e23_ctrl_TxDS3E3FrcYel_MaxVal                                                            0x1
#define cAf6_txm23e23_ctrl_TxDS3E3FrcYel_MinVal                                                            0x0
#define cAf6_txm23e23_ctrl_TxDS3E3FrcYel_RstVal                                                            0x0

/*--------------------------------------
BitField Name: TxDS3E3AutoYel
BitField Type: RW
BitField Desc:
BitField Bits: [16]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3AutoYel_Bit_Start                                                         16
#define cAf6_txm23e23_ctrl_TxDS3E3AutoYel_Bit_End                                                           16
#define cAf6_txm23e23_ctrl_TxDS3E3AutoYel_Mask                                                          cBit16
#define cAf6_txm23e23_ctrl_TxDS3E3AutoYel_Shift                                                             16
#define cAf6_txm23e23_ctrl_TxDS3E3AutoYel_MaxVal                                                           0x1
#define cAf6_txm23e23_ctrl_TxDS3E3AutoYel_MinVal                                                           0x0
#define cAf6_txm23e23_ctrl_TxDS3E3AutoYel_RstVal                                                           0x0

/*--------------------------------------
BitField Name: TxDS3E3LoopMd
BitField Type: RW
BitField Desc: DS3 C-bit mode: [15:14]: FE Control: 00: idle code; 01: activate
cw; 10: de-activate cw; 11: alrm/status E3G832 mode: [15:14]: DL mode Normal:
Loop Mode configuration
BitField Bits: [15:14]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3LoopMd_Bit_Start                                                          14
#define cAf6_txm23e23_ctrl_TxDS3E3LoopMd_Bit_End                                                            15
#define cAf6_txm23e23_ctrl_TxDS3E3LoopMd_Mask                                                        cBit15_14
#define cAf6_txm23e23_ctrl_TxDS3E3LoopMd_Shift                                                              14
#define cAf6_txm23e23_ctrl_TxDS3E3LoopMd_MaxVal                                                            0x3
#define cAf6_txm23e23_ctrl_TxDS3E3LoopMd_MinVal                                                            0x0
#define cAf6_txm23e23_ctrl_TxDS3E3LoopMd_RstVal                                                            0x0

/*--------------------------------------
BitField Name: TxDS3E3LoopEn
BitField Type: RW
BitField Desc: DS3 C-bit mode: [12:7]: FEAC Control Word E3G832 mode: [13:11]:
Payload type [10:7]:  SSM Config Normal: [13:7]: Loop en configuration per
channel
BitField Bits: [13:7]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3LoopEn_Bit_Start                                                           7
#define cAf6_txm23e23_ctrl_TxDS3E3LoopEn_Bit_End                                                            13
#define cAf6_txm23e23_ctrl_TxDS3E3LoopEn_Mask                                                         cBit13_7
#define cAf6_txm23e23_ctrl_TxDS3E3LoopEn_Shift                                                               7
#define cAf6_txm23e23_ctrl_TxDS3E3LoopEn_MaxVal                                                           0x7f
#define cAf6_txm23e23_ctrl_TxDS3E3LoopEn_MinVal                                                            0x0
#define cAf6_txm23e23_ctrl_TxDS3E3LoopEn_RstVal                                                            0x0

/*--------------------------------------
BitField Name: TxDS3E3Ohbypass
BitField Type: RW
BitField Desc:
BitField Bits: [6]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3Ohbypass_Bit_Start                                                         6
#define cAf6_txm23e23_ctrl_TxDS3E3Ohbypass_Bit_End                                                           6
#define cAf6_txm23e23_ctrl_TxDS3E3Ohbypass_Mask                                                          cBit6
#define cAf6_txm23e23_ctrl_TxDS3E3Ohbypass_Shift                                                             6
#define cAf6_txm23e23_ctrl_TxDS3E3Ohbypass_MaxVal                                                          0x1
#define cAf6_txm23e23_ctrl_TxDS3E3Ohbypass_MinVal                                                          0x0
#define cAf6_txm23e23_ctrl_TxDS3E3Ohbypass_RstVal                                                          0x0

/*--------------------------------------
BitField Name: TxDS3E3IdleSet
BitField Type: RW
BitField Desc:
BitField Bits: [5]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3IdleSet_Bit_Start                                                          5
#define cAf6_txm23e23_ctrl_TxDS3E3IdleSet_Bit_End                                                            5
#define cAf6_txm23e23_ctrl_TxDS3E3IdleSet_Mask                                                           cBit5
#define cAf6_txm23e23_ctrl_TxDS3E3IdleSet_Shift                                                              5
#define cAf6_txm23e23_ctrl_TxDS3E3IdleSet_MaxVal                                                           0x1
#define cAf6_txm23e23_ctrl_TxDS3E3IdleSet_MinVal                                                           0x0
#define cAf6_txm23e23_ctrl_TxDS3E3IdleSet_RstVal                                                           0x0

/*--------------------------------------
BitField Name: TxDS3E3AisSet
BitField Type: RW
BitField Desc:
BitField Bits: [4]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3AisSet_Bit_Start                                                           4
#define cAf6_txm23e23_ctrl_TxDS3E3AisSet_Bit_End                                                             4
#define cAf6_txm23e23_ctrl_TxDS3E3AisSet_Mask                                                            cBit4
#define cAf6_txm23e23_ctrl_TxDS3E3AisSet_Shift                                                               4
#define cAf6_txm23e23_ctrl_TxDS3E3AisSet_MaxVal                                                            0x1
#define cAf6_txm23e23_ctrl_TxDS3E3AisSet_MinVal                                                            0x0
#define cAf6_txm23e23_ctrl_TxDS3E3AisSet_RstVal                                                            0x0

/*--------------------------------------
BitField Name: TxDS3E3Md
BitField Type: RW
BitField Desc: Tx DS3/E3 framing mode 0000: DS3 Unframed 0001: DS3 C-bit parity
carrying 28 DS1s or 21 E1s 0010: DS3 M23 carrying 28 DS1s or 21 E1s 0011: DS3
C-bit map 0100: E3 Unframed 0101: E3 G832 0110: E3 G.751 carrying 16 E1s 0111:
E3 G751 Map 1000: Bypass RxM13E13 (DS1/E1 mode)
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3Md_Bit_Start                                                               0
#define cAf6_txm23e23_ctrl_TxDS3E3Md_Bit_End                                                                 3
#define cAf6_txm23e23_ctrl_TxDS3E3Md_Mask                                                              cBit3_0
#define cAf6_txm23e23_ctrl_TxDS3E3Md_Shift                                                                   0
#define cAf6_txm23e23_ctrl_TxDS3E3Md_MaxVal                                                                0xf
#define cAf6_txm23e23_ctrl_TxDS3E3Md_MinVal                                                                0x0
#define cAf6_txm23e23_ctrl_TxDS3E3Md_RstVal                                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : TxM23E23 HW Status
Reg Addr   : 0x00080080-0x0008009F
Reg Formula: 0x00080080 +  de3id
    Where  : 
           + $de3id(0-23):
Reg Desc   : 
for HW debug only

------------------------------------------------------------------------------*/
#define cAf6Reg_txm23e23_hw_stat_Base                                                               0x00080080
#define cAf6Reg_txm23e23_hw_stat(de3id)                                                   (0x00080080+(de3id))
#define cAf6Reg_txm23e23_hw_stat_WidthVal                                                                   64
#define cAf6Reg_txm23e23_hw_stat_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: TxM23E23Status
BitField Type: RW
BitField Desc: for HW debug only
BitField Bits: [42:0]
--------------------------------------*/
#define cAf6_txm23e23_hw_stat_TxM23E23Status_Bit_Start                                                       0
#define cAf6_txm23e23_hw_stat_TxM23E23Status_Bit_End                                                        42
#define cAf6_txm23e23_hw_stat_TxM23E23Status_Mask_01                                                  cBit31_0
#define cAf6_txm23e23_hw_stat_TxM23E23Status_Shift_01                                                        0
#define cAf6_txm23e23_hw_stat_TxM23E23Status_Mask_02                                                  cBit10_0
#define cAf6_txm23e23_hw_stat_TxM23E23Status_Shift_02                                                        0


/*------------------------------------------------------------------------------
Reg Name   : TxM23E23 E3g832 Trace Byte
Reg Addr   : 0x00080200-0x0803FF
Reg Formula: 0x00080200 +  de3id*16 + bytecnt
    Where  : 
           + $de3id(0-23:
           + $bytecnt(0-15:
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_txm23e23_e3g832_tracebyte_Base                                                      0x00080200
#define cAf6Reg_txm23e23_e3g832_tracebyte(de3id, bytecnt)                    (0x00080200+(de3id)*16+(bytecnt))
#define cAf6Reg_txm23e23_e3g832_tracebyte_WidthVal                                                          32
#define cAf6Reg_txm23e23_e3g832_tracebyte_WriteMask                                                        0x0

/*--------------------------------------
BitField Name: TxM23E23_E3g832_Trace_Byte
BitField Type: RW
BitField Desc: TxM23E23 E3g832 Trace Byte
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_txm23e23_e3g832_tracebyte_TxM23E23_E3g832_Trace_Byte_Bit_Start                                       0
#define cAf6_txm23e23_e3g832_tracebyte_TxM23E23_E3g832_Trace_Byte_Bit_End                                       7
#define cAf6_txm23e23_e3g832_tracebyte_TxM23E23_E3g832_Trace_Byte_Mask                                 cBit7_0
#define cAf6_txm23e23_e3g832_tracebyte_TxM23E23_E3g832_Trace_Byte_Shift                                       0
#define cAf6_txm23e23_e3g832_tracebyte_TxM23E23_E3g832_Trace_Byte_MaxVal                                    0xff
#define cAf6_txm23e23_e3g832_tracebyte_TxM23E23_E3g832_Trace_Byte_MinVal                                     0x0
#define cAf6_txm23e23_e3g832_tracebyte_TxM23E23_E3g832_Trace_Byte_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : DS3/E3 Test Pattern Generation Global Control
Reg Addr   : 0x0800D0-7800D3
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_de3_testpat_gen_glb_ctrl_Base                                                         0x0800D0
#define cAf6Reg_de3_testpat_gen_glb_ctrl                                                              0x0800D0
#define cAf6Reg_de3_testpat_gen_glb_ctrl_WidthVal                                                           32
#define cAf6Reg_de3_testpat_gen_glb_ctrl_WriteMask                                                         0x0

/*--------------------------------------
BitField Name: TxDe3PtgBerEn
BitField Type: RW
BitField Desc: Enable transmit DS3/E3 BER insert
BitField Bits: [15]
--------------------------------------*/
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgBerEn_Bit_Start                                               15
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgBerEn_Bit_End                                                 15
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgBerEn_Mask                                                cBit15
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgBerEn_Shift                                                   15
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgBerEn_MaxVal                                                 0x1
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgBerEn_MinVal                                                 0x0
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgBerEn_RstVal                                                 0x0

/*--------------------------------------
BitField Name: TxDe3PtgBerMod
BitField Type: RW
BitField Desc: BER mode configuration 000: BER 10-2 001: BER 10-3 010: BER 10-4
011: BER 10-5 100: BER 10-6 1x1: BER 10-7
BitField Bits: [14:12]
--------------------------------------*/
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgBerMod_Bit_Start                                              12
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgBerMod_Bit_End                                                14
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgBerMod_Mask                                            cBit14_12
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgBerMod_Shift                                                  12
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgBerMod_MaxVal                                                0x7
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgBerMod_MinVal                                                0x0
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgBerMod_RstVal                                                0x0

/*--------------------------------------
BitField Name: TxDe3PtgSwap
BitField Type: RW
BitField Desc: PTG swap Bit. If it is set to 1, pattern is transmitted LSB
first, then MSB.             Normal is set to 0.
BitField Bits: [11]
--------------------------------------*/
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgSwap_Bit_Start                                                11
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgSwap_Bit_End                                                  11
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgSwap_Mask                                                 cBit11
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgSwap_Shift                                                    11
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgSwap_MaxVal                                                  0x1
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgSwap_MinVal                                                  0x0
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgSwap_RstVal                                                  0x0

/*--------------------------------------
BitField Name: TxDe3PtgInv
BitField Type: RW
BitField Desc: PTG inversion Bit. Normal is set to 0.
BitField Bits: [10]
--------------------------------------*/
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgInv_Bit_Start                                                 10
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgInv_Bit_End                                                   10
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgInv_Mask                                                  cBit10
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgInv_Shift                                                     10
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgInv_MaxVal                                                   0x1
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgInv_MinVal                                                   0x0
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgInv_RstVal                                                   0x0

/*--------------------------------------
BitField Name: TxDe3PtgBitErr
BitField Type: RW
BitField Desc: Enable for single bit error insertion.
BitField Bits: [9]
--------------------------------------*/
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgBitErr_Bit_Start                                               9
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgBitErr_Bit_End                                                 9
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgBitErr_Mask                                                cBit9
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgBitErr_Shift                                                   9
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgBitErr_MaxVal                                                0x1
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgBitErr_MinVal                                                0x0
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgBitErr_RstVal                                                0x0

/*--------------------------------------
BitField Name: TxDe3PtgMod
BitField Type: RW
BitField Desc: Generation DS3/E3 BERT mode 0000: Prbs9 0001: Prbs11 0010: Prbs15
0011: Prbs20 0100: Prbs20 0101: Qrss20 0110: Prbs23 0111: Prbs31 1000: Sequence
1001: AllOne 1010: AllZero 1011: AA55
BitField Bits: [8:5]
--------------------------------------*/
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgMod_Bit_Start                                                  5
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgMod_Bit_End                                                    8
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgMod_Mask                                                 cBit8_5
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgMod_Shift                                                      5
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgMod_MaxVal                                                   0xf
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgMod_MinVal                                                   0x0
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgMod_RstVal                                                   0x0

/*--------------------------------------
BitField Name: TxDe3PtgEn
BitField Type: RW
BitField Desc: Bit Enable Test Pattern Generation
BitField Bits: [4]
--------------------------------------*/
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgEn_Bit_Start                                                   4
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgEn_Bit_End                                                     4
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgEn_Mask                                                    cBit4
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgEn_Shift                                                       4
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgEn_MaxVal                                                    0x1
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgEn_MinVal                                                    0x0
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgEn_RstVal                                                    0x0

/*--------------------------------------
BitField Name: TxDe3PtgID
BitField Type: RW
BitField Desc: To configure which DS3/E3 line is selected  to test the Pattern
GenerationID Configuration
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgID_Bit_Start                                                   0
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgID_Bit_End                                                     3
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgID_Mask                                                  cBit3_0
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgID_Shift                                                       0
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgID_MaxVal                                                    0xf
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgID_MinVal                                                    0x0
#define cAf6_de3_testpat_gen_glb_ctrl_TxDe3PtgID_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : TxM12E12 Control
Reg Addr   : 0x00081000 - 0x000810FF
Reg Formula: 0x00081000 +  8*de3id + de2id
    Where  : 
           + $de3id(0-23):
           + $de2id(0-6):
Reg Desc   : 
The TxM12E12 Control is use to configure for per channel Tx DS2/E2 operation.

------------------------------------------------------------------------------*/
#define cAf6Reg_txm12e12_ctrl_Base                                                                  0x00081000
#define cAf6Reg_txm12e12_ctrl(de3id, de2id)                                     (0x00081000+8*(de3id)+(de2id))
#define cAf6Reg_txm12e12_ctrl_WidthVal                                                                      32
#define cAf6Reg_txm12e12_ctrl_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: TxDS2E2Md
BitField Type: RW
BitField Desc: Tx DS2/E2 framing mode 00: DS2 T1.107 carrying 4 DS1s 01: DS2
G.747 carrying 3 E1s 10: E2 G.742 carrying 4 E1s 11: Reserved
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_txm12e12_ctrl_TxDS2E2Md_Bit_Start                                                               0
#define cAf6_txm12e12_ctrl_TxDS2E2Md_Bit_End                                                                 1
#define cAf6_txm12e12_ctrl_TxDS2E2Md_Mask                                                              cBit1_0
#define cAf6_txm12e12_ctrl_TxDS2E2Md_Shift                                                                   0
#define cAf6_txm12e12_ctrl_TxDS2E2Md_MaxVal                                                                0x3
#define cAf6_txm12e12_ctrl_TxDS2E2Md_MinVal                                                                0x0
#define cAf6_txm12e12_ctrl_TxDS2E2Md_RstVal                                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : TxM12E12 HW Status
Reg Addr   : 0x00081200 - 0x000812FF
Reg Formula: 0x00081200 +  8*de3id + de2id
    Where  : 
           + $de3id(0-23):
           + $de2id(0-6):
Reg Desc   : 
for HW debug only

------------------------------------------------------------------------------*/
#define cAf6Reg_txm12e12_hw_stat_Base                                                               0x00081200
#define cAf6Reg_txm12e12_hw_stat(de3id, de2id)                                  (0x00081200+8*(de3id)+(de2id))
#define cAf6Reg_txm12e12_hw_stat_WidthVal                                                                   96
#define cAf6Reg_txm12e12_hw_stat_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: TxM12E12Status
BitField Type: RW
BitField Desc: for HW debug only
BitField Bits: [80:0]
--------------------------------------*/
#define cAf6_txm12e12_hw_stat_TxM12E12Status_Bit_Start                                                       0
#define cAf6_txm12e12_hw_stat_TxM12E12Status_Bit_End                                                        80
#define cAf6_txm12e12_hw_stat_TxM12E12Status_Mask_01                                                  cBit31_0
#define cAf6_txm12e12_hw_stat_TxM12E12Status_Shift_01                                                        0
#define cAf6_txm12e12_hw_stat_TxM12E12Status_Mask_02                                                  cBit31_0
#define cAf6_txm12e12_hw_stat_TxM12E12Status_Shift_02                                                        0
#define cAf6_txm12e12_hw_stat_TxM12E12Status_Mask_03                                                  cBit16_0
#define cAf6_txm12e12_hw_stat_TxM12E12Status_Shift_03                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Tx M12E12 Jitter Configuration
Reg Addr   : 0x00081380
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the configuration for Tx M13/E13

------------------------------------------------------------------------------*/
#define cAf6Reg_txm12e12_jitter_cfg_Base                                                            0x00081380
#define cAf6Reg_txm12e12_jitter_cfg                                                                 0x00081380
#define cAf6Reg_txm12e12_jitter_cfg_WidthVal                                                                96
#define cAf6Reg_txm12e12_jitter_cfg_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: SWThreshold1
BitField Type: RW
BitField Desc:
BitField Bits: [69:63]
--------------------------------------*/
#define cAf6_txm12e12_jitter_cfg_SWThreshold1_Bit_Start                                                     63
#define cAf6_txm12e12_jitter_cfg_SWThreshold1_Bit_End                                                       69
#define cAf6_txm12e12_jitter_cfg_SWThreshold1_Mask_01                                                   cBit31
#define cAf6_txm12e12_jitter_cfg_SWThreshold1_Shift_01                                                      31
#define cAf6_txm12e12_jitter_cfg_SWThreshold1_Mask_02                                                  cBit5_0
#define cAf6_txm12e12_jitter_cfg_SWThreshold1_Shift_02                                                       0

/*--------------------------------------
BitField Name: SWThreshold2
BitField Type: RW
BitField Desc:
BitField Bits: [62:56]
--------------------------------------*/
#define cAf6_txm12e12_jitter_cfg_SWThreshold2_Bit_Start                                                     56
#define cAf6_txm12e12_jitter_cfg_SWThreshold2_Bit_End                                                       62
#define cAf6_txm12e12_jitter_cfg_SWThreshold2_Mask                                                   cBit30_24
#define cAf6_txm12e12_jitter_cfg_SWThreshold2_Shift                                                         24
#define cAf6_txm12e12_jitter_cfg_SWThreshold2_MaxVal                                                       0x0
#define cAf6_txm12e12_jitter_cfg_SWThreshold2_MinVal                                                       0x0
#define cAf6_txm12e12_jitter_cfg_SWThreshold2_RstVal                                                       0x0

/*--------------------------------------
BitField Name: SWThreshold3
BitField Type: RW
BitField Desc:
BitField Bits: [55:49]
--------------------------------------*/
#define cAf6_txm12e12_jitter_cfg_SWThreshold3_Bit_Start                                                     49
#define cAf6_txm12e12_jitter_cfg_SWThreshold3_Bit_End                                                       55
#define cAf6_txm12e12_jitter_cfg_SWThreshold3_Mask                                                   cBit23_17
#define cAf6_txm12e12_jitter_cfg_SWThreshold3_Shift                                                         17
#define cAf6_txm12e12_jitter_cfg_SWThreshold3_MaxVal                                                       0x0
#define cAf6_txm12e12_jitter_cfg_SWThreshold3_MinVal                                                       0x0
#define cAf6_txm12e12_jitter_cfg_SWThreshold3_RstVal                                                       0x0

/*--------------------------------------
BitField Name: SWThreshold4
BitField Type: RW
BitField Desc:
BitField Bits: [48:42]
--------------------------------------*/
#define cAf6_txm12e12_jitter_cfg_SWThreshold4_Bit_Start                                                     42
#define cAf6_txm12e12_jitter_cfg_SWThreshold4_Bit_End                                                       48
#define cAf6_txm12e12_jitter_cfg_SWThreshold4_Mask                                                   cBit16_10
#define cAf6_txm12e12_jitter_cfg_SWThreshold4_Shift                                                         10
#define cAf6_txm12e12_jitter_cfg_SWThreshold4_MaxVal                                                       0x0
#define cAf6_txm12e12_jitter_cfg_SWThreshold4_MinVal                                                       0x0
#define cAf6_txm12e12_jitter_cfg_SWThreshold4_RstVal                                                       0x0

/*--------------------------------------
BitField Name: SWThreshold5
BitField Type: RW
BitField Desc:
BitField Bits: [41:35]
--------------------------------------*/
#define cAf6_txm12e12_jitter_cfg_SWThreshold5_Bit_Start                                                     35
#define cAf6_txm12e12_jitter_cfg_SWThreshold5_Bit_End                                                       41
#define cAf6_txm12e12_jitter_cfg_SWThreshold5_Mask                                                     cBit9_3
#define cAf6_txm12e12_jitter_cfg_SWThreshold5_Shift                                                          3
#define cAf6_txm12e12_jitter_cfg_SWThreshold5_MaxVal                                                       0x0
#define cAf6_txm12e12_jitter_cfg_SWThreshold5_MinVal                                                       0x0
#define cAf6_txm12e12_jitter_cfg_SWThreshold5_RstVal                                                       0x0

/*--------------------------------------
BitField Name: SWCntMax1
BitField Type: RW
BitField Desc:
BitField Bits: [34:28]
--------------------------------------*/
#define cAf6_txm12e12_jitter_cfg_SWCntMax1_Bit_Start                                                        28
#define cAf6_txm12e12_jitter_cfg_SWCntMax1_Bit_End                                                          34
#define cAf6_txm12e12_jitter_cfg_SWCntMax1_Mask_01                                                   cBit31_28
#define cAf6_txm12e12_jitter_cfg_SWCntMax1_Shift_01                                                         28
#define cAf6_txm12e12_jitter_cfg_SWCntMax1_Mask_02                                                     cBit2_0
#define cAf6_txm12e12_jitter_cfg_SWCntMax1_Shift_02                                                          0

/*--------------------------------------
BitField Name: SWCntMax2
BitField Type: RW
BitField Desc:
BitField Bits: [27:21]
--------------------------------------*/
#define cAf6_txm12e12_jitter_cfg_SWCntMax2_Bit_Start                                                        21
#define cAf6_txm12e12_jitter_cfg_SWCntMax2_Bit_End                                                          27
#define cAf6_txm12e12_jitter_cfg_SWCntMax2_Mask                                                      cBit27_21
#define cAf6_txm12e12_jitter_cfg_SWCntMax2_Shift                                                            21
#define cAf6_txm12e12_jitter_cfg_SWCntMax2_MaxVal                                                         0x7f
#define cAf6_txm12e12_jitter_cfg_SWCntMax2_MinVal                                                          0x0
#define cAf6_txm12e12_jitter_cfg_SWCntMax2_RstVal                                                          0x0

/*--------------------------------------
BitField Name: SWCntMax3
BitField Type: RW
BitField Desc:
BitField Bits: [20:14]
--------------------------------------*/
#define cAf6_txm12e12_jitter_cfg_SWCntMax3_Bit_Start                                                        14
#define cAf6_txm12e12_jitter_cfg_SWCntMax3_Bit_End                                                          20
#define cAf6_txm12e12_jitter_cfg_SWCntMax3_Mask                                                      cBit20_14
#define cAf6_txm12e12_jitter_cfg_SWCntMax3_Shift                                                            14
#define cAf6_txm12e12_jitter_cfg_SWCntMax3_MaxVal                                                         0x7f
#define cAf6_txm12e12_jitter_cfg_SWCntMax3_MinVal                                                          0x0
#define cAf6_txm12e12_jitter_cfg_SWCntMax3_RstVal                                                          0x0

/*--------------------------------------
BitField Name: SWCntMax4
BitField Type: RW
BitField Desc:
BitField Bits: [13:7]
--------------------------------------*/
#define cAf6_txm12e12_jitter_cfg_SWCntMax4_Bit_Start                                                         7
#define cAf6_txm12e12_jitter_cfg_SWCntMax4_Bit_End                                                          13
#define cAf6_txm12e12_jitter_cfg_SWCntMax4_Mask                                                       cBit13_7
#define cAf6_txm12e12_jitter_cfg_SWCntMax4_Shift                                                             7
#define cAf6_txm12e12_jitter_cfg_SWCntMax4_MaxVal                                                         0x7f
#define cAf6_txm12e12_jitter_cfg_SWCntMax4_MinVal                                                          0x0
#define cAf6_txm12e12_jitter_cfg_SWCntMax4_RstVal                                                          0x0

/*--------------------------------------
BitField Name: SWCntMax5
BitField Type: RW
BitField Desc:
BitField Bits: [6:0]
--------------------------------------*/
#define cAf6_txm12e12_jitter_cfg_SWCntMax5_Bit_Start                                                         0
#define cAf6_txm12e12_jitter_cfg_SWCntMax5_Bit_End                                                           6
#define cAf6_txm12e12_jitter_cfg_SWCntMax5_Mask                                                        cBit6_0
#define cAf6_txm12e12_jitter_cfg_SWCntMax5_Shift                                                             0
#define cAf6_txm12e12_jitter_cfg_SWCntMax5_MaxVal                                                         0x7f
#define cAf6_txm12e12_jitter_cfg_SWCntMax5_MinVal                                                          0x0
#define cAf6_txm12e12_jitter_cfg_SWCntMax5_RstVal                                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1 Rx Framer Inband LoopCode Error Threshold Configuration
Reg Addr   : 0xE_0002
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is the threshold configuration of error inband loopcode patterns.

------------------------------------------------------------------------------*/
#define cAf6Reg_ds1_rxfrm_inband_lpc_err_thres_cfg_Base                                                0xE0002
#define cAf6Reg_ds1_rxfrm_inband_lpc_err_thres_cfg                                                     0xE0002
#define cAf6Reg_ds1_rxfrm_inband_lpc_err_thres_cfg_WidthVal                                                 32
#define cAf6Reg_ds1_rxfrm_inband_lpc_err_thres_cfg_WriteMask                                               0x0

/*--------------------------------------
BitField Name: err_thres
BitField Type: RW
BitField Desc: if cnt err more than limit error,search failed
BitField Bits: [6:0]
--------------------------------------*/
#define cAf6_ds1_rxfrm_inband_lpc_err_thres_cfg_err_thres_Bit_Start                                          0
#define cAf6_ds1_rxfrm_inband_lpc_err_thres_cfg_err_thres_Bit_End                                            6
#define cAf6_ds1_rxfrm_inband_lpc_err_thres_cfg_err_thres_Mask                                         cBit6_0
#define cAf6_ds1_rxfrm_inband_lpc_err_thres_cfg_err_thres_Shift                                              0
#define cAf6_ds1_rxfrm_inband_lpc_err_thres_cfg_err_thres_MaxVal                                          0x7f
#define cAf6_ds1_rxfrm_inband_lpc_err_thres_cfg_err_thres_MinVal                                           0x0
#define cAf6_ds1_rxfrm_inband_lpc_err_thres_cfg_err_thres_RstVal                                           0xF


/*------------------------------------------------------------------------------
Reg Name   : DS1 Rx Framer Inband LoopCode Debound Configuration
Reg Addr   : 0xE_0003
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is the threshold configuration of stable inband loopcode patterns.

------------------------------------------------------------------------------*/
#define cAf6Reg_ds1_rxfrm_inband_lpc_debound_thres_cfg_Base                                            0xE0003
#define cAf6Reg_ds1_rxfrm_inband_lpc_debound_thres_cfg                                                 0xE0003
#define cAf6Reg_ds1_rxfrm_inband_lpc_debound_thres_cfg_WidthVal                                             32
#define cAf6Reg_ds1_rxfrm_inband_lpc_debound_thres_cfg_WriteMask                                           0x0

/*--------------------------------------
BitField Name: stb_thres
BitField Type: RW
BitField Desc: if cnt match more than limit mat, search ok
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_ds1_rxfrm_inband_lpc_debound_thres_cfg_stb_thres_Bit_Start                                       0
#define cAf6_ds1_rxfrm_inband_lpc_debound_thres_cfg_stb_thres_Bit_End                                       15
#define cAf6_ds1_rxfrm_inband_lpc_debound_thres_cfg_stb_thres_Mask                                    cBit15_0
#define cAf6_ds1_rxfrm_inband_lpc_debound_thres_cfg_stb_thres_Shift                                          0
#define cAf6_ds1_rxfrm_inband_lpc_debound_thres_cfg_stb_thres_MaxVal                                    0xffff
#define cAf6_ds1_rxfrm_inband_lpc_debound_thres_cfg_stb_thres_MinVal                                       0x0
#define cAf6_ds1_rxfrm_inband_lpc_debound_thres_cfg_stb_thres_RstVal                                     0xFFE


/*------------------------------------------------------------------------------
Reg Name   : DS1 Rx Framer FDL LoopCode Debound Configuration
Reg Addr   : 0xE_0004
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is the threshold configuration of stable FDL loopcode patterns.

------------------------------------------------------------------------------*/
#define cAf6Reg_ds1_rxfrm_fdl_lpc_debound_thres_cfg_Base                                               0xE0004
#define cAf6Reg_ds1_rxfrm_fdl_lpc_debound_thres_cfg                                                    0xE0004
#define cAf6Reg_ds1_rxfrm_fdl_lpc_debound_thres_cfg_WidthVal                                                32
#define cAf6Reg_ds1_rxfrm_fdl_lpc_debound_thres_cfg_WriteMask                                              0x0

/*--------------------------------------
BitField Name: stb_thres
BitField Type: RW
BitField Desc: if message codes repeat more than thresh, search ok,
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_ds1_rxfrm_fdl_lpc_debound_thres_cfg_stb_thres_Bit_Start                                         0
#define cAf6_ds1_rxfrm_fdl_lpc_debound_thres_cfg_stb_thres_Bit_End                                           7
#define cAf6_ds1_rxfrm_fdl_lpc_debound_thres_cfg_stb_thres_Mask                                        cBit7_0
#define cAf6_ds1_rxfrm_fdl_lpc_debound_thres_cfg_stb_thres_Shift                                             0
#define cAf6_ds1_rxfrm_fdl_lpc_debound_thres_cfg_stb_thres_MaxVal                                         0xff
#define cAf6_ds1_rxfrm_fdl_lpc_debound_thres_cfg_stb_thres_MinVal                                          0x0
#define cAf6_ds1_rxfrm_fdl_lpc_debound_thres_cfg_stb_thres_RstVal                                          0x5


/*------------------------------------------------------------------------------
Reg Name   : DS1 Rx Framer FDL LoopCode Error Threshold Configuration
Reg Addr   : 0xE_0005
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is the threshold configuration of error FDL loopcode patterns.

------------------------------------------------------------------------------*/
#define cAf6Reg_ds1_rxfrm_fdl_lpc_err_thres_cfg_Base                                                   0xE0005
#define cAf6Reg_ds1_rxfrm_fdl_lpc_err_thres_cfg                                                        0xE0005
#define cAf6Reg_ds1_rxfrm_fdl_lpc_err_thres_cfg_WidthVal                                                    32
#define cAf6Reg_ds1_rxfrm_fdl_lpc_err_thres_cfg_WriteMask                                                  0x0

/*--------------------------------------
BitField Name: err_thres
BitField Type: RW
BitField Desc: if pattern error more than thresh , change to check another
pattern
BitField Bits: [6:0]
--------------------------------------*/
#define cAf6_ds1_rxfrm_fdl_lpc_err_thres_cfg_err_thres_Bit_Start                                             0
#define cAf6_ds1_rxfrm_fdl_lpc_err_thres_cfg_err_thres_Bit_End                                               6
#define cAf6_ds1_rxfrm_fdl_lpc_err_thres_cfg_err_thres_Mask                                            cBit6_0
#define cAf6_ds1_rxfrm_fdl_lpc_err_thres_cfg_err_thres_Shift                                                 0
#define cAf6_ds1_rxfrm_fdl_lpc_err_thres_cfg_err_thres_MaxVal                                             0x7f
#define cAf6_ds1_rxfrm_fdl_lpc_err_thres_cfg_err_thres_MinVal                                              0x0
#define cAf6_ds1_rxfrm_fdl_lpc_err_thres_cfg_err_thres_RstVal                                              0xF


/*------------------------------------------------------------------------------
Reg Name   : Number of pattern
Reg Addr   : 0xE_20FE
Reg Formula: 
    Where  : 
Reg Desc   : 
For HW debug

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_len_Base                                                                          0xE20FE
#define cAf6Reg_upen_len                                                                               0xE20FE
#define cAf6Reg_upen_len_WidthVal                                                                           32
#define cAf6Reg_upen_len_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: info_len_l
BitField Type: RO
BitField Desc: number patt detected
BitField Bits: [10:00]
--------------------------------------*/
#define cAf6_upen_len_info_len_l_Bit_Start                                                                   0
#define cAf6_upen_len_info_len_l_Bit_End                                                                    10
#define cAf6_upen_len_info_len_l_Mask                                                                 cBit10_0
#define cAf6_upen_len_info_len_l_Shift                                                                       0
#define cAf6_upen_len_info_len_l_MaxVal                                                                  0x7ff
#define cAf6_upen_len_info_len_l_MinVal                                                                    0x0
#define cAf6_upen_len_info_len_l_RstVal                                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1 Rx Framer Inband Loopcode Status
Reg Addr   : 0xE_2100 - 0xE_24FF
Reg Formula: 0xE_2100 + ds1
    Where  : 
           + $ds1 (0-1023):
Reg Desc   : 
These registers are used to report the current inband loopcode status per channel.

------------------------------------------------------------------------------*/
#define cAf6Reg_ds1_rxfrm_inband_lpc_sta_Base                                                          0xE2100
#define cAf6Reg_ds1_rxfrm_inband_lpc_sta(ds1)                                                  (0xE2100+(ds1))
#define cAf6Reg_ds1_rxfrm_inband_lpc_sta_WidthVal                                                           32
#define cAf6Reg_ds1_rxfrm_inband_lpc_sta_WriteMask                                                         0x0

/*--------------------------------------
BitField Name: iblpc_sta
BitField Type: RO
BitField Desc: bit [2:0] == 0x0  CSU UP bit [2:0] == 0x1  CSU DOWN bit [2:0] ==
0x2  FAC1 UP bit [2:0] == 0x3  FAC1 DOWN bit [2:0] == 0x4  FAC2 UP bit [2:0] ==
0x5  FAC2 DOWN bit [2:0] == 0x7  Change from LoopCode to normal
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_ds1_rxfrm_inband_lpc_sta_iblpc_sta_Bit_Start                                                    0
#define cAf6_ds1_rxfrm_inband_lpc_sta_iblpc_sta_Bit_End                                                      2
#define cAf6_ds1_rxfrm_inband_lpc_sta_iblpc_sta_Mask                                                   cBit2_0
#define cAf6_ds1_rxfrm_inband_lpc_sta_iblpc_sta_Shift                                                        0
#define cAf6_ds1_rxfrm_inband_lpc_sta_iblpc_sta_MaxVal                                                     0x7
#define cAf6_ds1_rxfrm_inband_lpc_sta_iblpc_sta_MinVal                                                     0x0
#define cAf6_ds1_rxfrm_inband_lpc_sta_iblpc_sta_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Number of message
Reg Addr   : 0xE_08FE
Reg Formula: 
    Where  : 
Reg Desc   : 
For HW debug

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_len_mess_Base                                                                     0xE08FE
#define cAf6Reg_upen_len_mess                                                                          0xE08FE
#define cAf6Reg_upen_len_mess_WidthVal                                                                      32
#define cAf6Reg_upen_len_mess_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: info_len_l
BitField Type: RO
BitField Desc: number patt detected
BitField Bits: [10:00]
--------------------------------------*/
#define cAf6_upen_len_mess_info_len_l_Bit_Start                                                              0
#define cAf6_upen_len_mess_info_len_l_Bit_End                                                               10
#define cAf6_upen_len_mess_info_len_l_Mask                                                            cBit10_0
#define cAf6_upen_len_mess_info_len_l_Shift                                                                  0
#define cAf6_upen_len_mess_info_len_l_MaxVal                                                             0x7ff
#define cAf6_upen_len_mess_info_len_l_MinVal                                                               0x0
#define cAf6_upen_len_mess_info_len_l_RstVal                                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1 Rx Framer Outband Loopcode Status
Reg Addr   : 0xE_1800 - 0xE_1BFF
Reg Formula: 0xE_1800 + $ds1
    Where  : 
           + $ds1 (0-1023):
Reg Desc   : 
These registers are used to report the current captured FDL loopcode status per channel.

------------------------------------------------------------------------------*/
#define cAf6Reg_ds1_rxfrm_fdl_lpc_sta_Base                                                             0xE1800
#define cAf6Reg_ds1_rxfrm_fdl_lpc_sta(ds1)                                                     (0xE1800+(ds1))
#define cAf6Reg_ds1_rxfrm_fdl_lpc_sta_WidthVal                                                              32
#define cAf6Reg_ds1_rxfrm_fdl_lpc_sta_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: bom_pat
BitField Type: RO
BitField Desc: captured 8-bit BOM pattern 0xxxxxx0
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_ds1_rxfrm_fdl_lpc_sta_bom_pat_Bit_Start                                                         8
#define cAf6_ds1_rxfrm_fdl_lpc_sta_bom_pat_Bit_End                                                          15
#define cAf6_ds1_rxfrm_fdl_lpc_sta_bom_pat_Mask                                                       cBit15_8
#define cAf6_ds1_rxfrm_fdl_lpc_sta_bom_pat_Shift                                                             8
#define cAf6_ds1_rxfrm_fdl_lpc_sta_bom_pat_MaxVal                                                         0xff
#define cAf6_ds1_rxfrm_fdl_lpc_sta_bom_pat_MinVal                                                          0x0
#define cAf6_ds1_rxfrm_fdl_lpc_sta_bom_pat_RstVal                                                          0x0

/*--------------------------------------
BitField Name: bom_ind
BitField Type: RO
BitField Desc: BOM message indication if it is 1
BitField Bits: [4]
--------------------------------------*/
#define cAf6_ds1_rxfrm_fdl_lpc_sta_bom_ind_Bit_Start                                                         4
#define cAf6_ds1_rxfrm_fdl_lpc_sta_bom_ind_Bit_End                                                           4
#define cAf6_ds1_rxfrm_fdl_lpc_sta_bom_ind_Mask                                                          cBit4
#define cAf6_ds1_rxfrm_fdl_lpc_sta_bom_ind_Shift                                                             4
#define cAf6_ds1_rxfrm_fdl_lpc_sta_bom_ind_MaxVal                                                          0x1
#define cAf6_ds1_rxfrm_fdl_lpc_sta_bom_ind_MinVal                                                          0x0
#define cAf6_ds1_rxfrm_fdl_lpc_sta_bom_ind_RstVal                                                          0x0

/*--------------------------------------
BitField Name: loopcode
BitField Type: RO
BitField Desc: Captured loopcode pattern 0000: 11111111_01111110 0001: line loop
up 0010: line loop down 0011: payload loop up 0100: payload loop down 0101:
smartjack activation 0110: smartjact deactivation 0111: idle
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_ds1_rxfrm_fdl_lpc_sta_loopcode_Bit_Start                                                        0
#define cAf6_ds1_rxfrm_fdl_lpc_sta_loopcode_Bit_End                                                          3
#define cAf6_ds1_rxfrm_fdl_lpc_sta_loopcode_Mask                                                       cBit3_0
#define cAf6_ds1_rxfrm_fdl_lpc_sta_loopcode_Shift                                                            0
#define cAf6_ds1_rxfrm_fdl_lpc_sta_loopcode_MaxVal                                                         0xf
#define cAf6_ds1_rxfrm_fdl_lpc_sta_loopcode_MinVal                                                         0x0
#define cAf6_ds1_rxfrm_fdl_lpc_sta_loopcode_RstVal                                                         0x0

#endif /* _AF6_REG_AF6CCI0031_RD_PDH_H_ */

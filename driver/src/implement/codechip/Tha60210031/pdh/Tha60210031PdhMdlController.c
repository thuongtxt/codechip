/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60210031PdhMdlController.c
 *
 * Created Date: Aug 11, 2015
 *
 * Description : MDL Controller class
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/pdh/ThaPdhDe3.h"
#include "../../../default/man/ThaDevice.h"
#include "Tha60210031PdhDe3.h"
#include "Tha60210031PdhMdlController.h"
#include "Tha60210031PdhMdlControllerReg.h"
#include "Tha60210031ModulePdh.h"
#include "Tha60210031PdhMdlControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cNumberOfHwBuffer 2
#define cNumMdlDwords     20

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210031PdhMdlController*)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
/* Implementation of this class*/
static char m_methodsInit = 0;

/* Override */
static tAtObjectMethods            m_AtObjectOverride;
static tAtPdhMdlControllerMethods  m_AtPdhMdlControllerOverride;
static tThaPdhMdlControllerMethods m_ThaPdhMdlControllerOverride;

/* Save super implementation */
static const tAtObjectMethods           *m_AtObjectMethods           = NULL;
static const tAtPdhMdlControllerMethods *m_AtPdhMdlControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static Tha60210031ModulePdh PdhModule(AtPdhDe3 self)
    {
    return (Tha60210031ModulePdh)AtChannelModuleGet((AtChannel)self);
    }

static uint32 MdlBaseAddress(AtPdhDe3 self)
    {
    return (uint32)ThaModulePdhMdlBaseAddress((ThaModulePdh)AtChannelModuleGet((AtChannel)self));
    }

static uint32 MdlOffset(AtPdhMdlController self)
    {
    return mMethodsGet((ThaPdhMdlController)self)->InterruptOffset((ThaPdhMdlController)self);
    }

static uint32 HwTypeValueGet(uint32 type)
    {
    switch (type)
        {
        case cAtPdhMdlControllerTypePathMessage: return cMdlMsgTypePath;
        case cAtPdhMdlControllerTypeIdleSignal:  return cMdlMsgTypeIdleTest;
        case cAtPdhMdlControllerTypeTestSignal:  return cMdlMsgTestSignal;
        default:                                 return 0;
        }
    }

static void TxEnable(AtPdhDe3 de3, eBool enable)
    {
    uint32 regAddr = mMethodsGet(PdhModule(de3))->MdlTxCtrlReg(PdhModule(de3), de3);
    uint32 regVal = mChannelHwRead(de3, regAddr, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_upen_cfg_mdl_cfg_entx_, enable ? 1 : 0);
    mChannelHwWrite(de3, regAddr, regVal, cAtModulePdh);
    }

static eBool TxIsEnable(AtPdhDe3 de3)
    {
    uint32 regAddr = mMethodsGet(PdhModule(de3))->MdlTxCtrlReg(PdhModule(de3), de3);
    uint32 regVal = mChannelHwRead(de3, regAddr, cAtModulePdh);
    return (regVal & cAf6_upen_cfg_mdl_cfg_entx_Mask) ? cAtTrue : cAtFalse;
    }

static void RxTypeCfg(AtPdhDe3 de3, eAtPdhMdlControllerType type)
    {
    uint32 regAddr = mMethodsGet(PdhModule(de3))->MdlRxCfgReg(PdhModule(de3), de3);
    uint32 regVal = mChannelHwRead(de3, regAddr, cAtModulePdh);

    mRegFieldSet(regVal, cAf6_upen_rxmdl_cfgtype_cfg_type_mdl_, HwTypeValueGet(type));
    mChannelHwWrite(de3, regAddr, regVal, cAtModulePdh);
    }

static eBool RxFilterMessagePerTypeIsSupported(AtPdhMdlController self)
    {
    return Tha60210031De3MdlRxFilterMessagePerTypeIsSupported(AtPdhMdlControllerDe3Get(self));
    }

static uint32 RxEnableBitShiftGet(AtPdhMdlController self)
    {
    uint32 type = AtPdhMdlControllerTypeGet(self);
    switch (type)
        {
        case cAtPdhMdlControllerTypePathMessage:
            return 1;
        case cAtPdhMdlControllerTypeIdleSignal:
            return 0;
        case cAtPdhMdlControllerTypeTestSignal:
            return 2;
        default:
            return 0;
        }
    }

static uint32 RxEnableBitMaskGet(AtPdhMdlController self)
    {
    return (cBit0 << RxEnableBitShiftGet(self));
    }

static void RxEnable(AtPdhMdlController self, AtPdhDe3 de3, eBool enable)
    {
    uint32 regAddr = mMethodsGet(PdhModule(de3))->MdlRxCfgReg(PdhModule(de3), de3);
    uint32 regVal = mChannelHwRead(de3, regAddr, cAtModulePdh);
    mFieldIns(&regVal, RxEnableBitMaskGet(self), RxEnableBitShiftGet(self), enable ? 1: 0);
    mChannelHwWrite(de3, regAddr, regVal, cAtModulePdh);
    }

static void StandardSet(AtPdhMdlController self, AtPdhDe3 de3)
    {
    ThaPdhMdlControllerHwStandardHeaderSet(self, AtPdhDe3MdlStandardGet(de3));
    }

static eAtRet Enable(AtPdhMdlController self, eBool enable)
    {
    AtPdhDe3 de3;
    eBool isEnabled = AtPdhMdlControllerIsEnabled(self);
    if (enable == isEnabled)
        return cAtOk;

    /* Apply Hardware for current enable */
    de3 = AtPdhMdlControllerDe3Get(self);
    if(!TxIsEnable(de3))
        {
        TxEnable(de3, enable);
        if (!RxFilterMessagePerTypeIsSupported(self))
            RxTypeCfg(de3, AtPdhMdlControllerTypeGet(self));
        else
            Tha60210031PdhMdlBufferReceivedStickyClear(de3);
        }
        
    if (RxFilterMessagePerTypeIsSupported(self))
        RxEnable(self, de3, enable);

    /* Update STD */
    StandardSet(self, de3);

    /* Supper update database */
    return  m_AtPdhMdlControllerMethods->Enable(self, enable);
    }

static void TxCRSet(AtPdhDe3 de3, uint8 value)
    {
    uint32 regAddr = mMethodsGet(PdhModule(de3))->MdlTxCtrlReg(PdhModule(de3), de3);
    uint32 regVal = mChannelHwRead(de3, regAddr, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_upen_cfg_mdl_cfg_mdl_cr_, value);
    mChannelHwWrite(de3, regAddr, regVal, cAtModulePdh);
    }

static uint32 DwordDataGet(uint8 *messageBuffer, uint32 dword_i)
    {
    uint32 regVal;

    /* Will start from 3 */
    regVal  =   (uint32)messageBuffer[dword_i*4 + 3];
    regVal |= (((uint32)messageBuffer[dword_i*4 + 4]) << 8)  & cBit15_8;
    regVal |= (((uint32)messageBuffer[dword_i*4 + 5]) << 16) & cBit23_16;
    regVal |= (((uint32)messageBuffer[dword_i*4 + 6]) << 24) & cBit31_24;
    return regVal;
    }

static void HwBufferWrite(AtPdhDe3 de3, uint8 *messageBuffer)
    {
    uint32 dword_i, regVal, regAddr;
    Tha60210031ModulePdh pdhModule = PdhModule(de3);
    uint32 dwordOffset, addressStart;
    uint8 sliceId, hwIdInSlice;

    ThaPdhChannelHwIdGet((AtPdhChannel)de3, cAtModulePdh, &sliceId, &hwIdInSlice);

    dwordOffset = mMethodsGet(pdhModule)->TxBufferDwordOffset(pdhModule, sliceId, hwIdInSlice);
    addressStart = mMethodsGet(pdhModule)->TxBufferBaseGet(pdhModule, de3);

    for (dword_i = 0; dword_i < 19; dword_i ++)
        {
        regVal = DwordDataGet(messageBuffer, dword_i);
        regAddr = addressStart + (dwordOffset * dword_i);
        mChannelHwWrite(de3, regAddr, regVal, cAtModulePdh);
        }
    }

static uint32 MdlTxInsCtrl(AtPdhDe3 self)
    {
    uint8 hwIdInSlice, sliceId;
    uint32 sliceOffset;
    Tha60210031ModulePdh pdhModule = (Tha60210031ModulePdh)AtChannelModuleGet((AtChannel)self);

    ThaPdhChannelHwIdGet((AtPdhChannel)self, cAtModulePdh, &sliceId, &hwIdInSlice);
    sliceOffset = Tha60210031ModulePdhMdlSliceOffset(pdhModule, sliceId);
    return Tha60210031ModulePdhMdlTxInsCtrlBase(pdhModule) + hwIdInSlice + sliceOffset + MdlBaseAddress(self);
    }

static eBool IsAvailable(AtPdhDe3 self)
    {
    uint32 regVal = mChannelHwRead(self, MdlTxInsCtrl(self), cAtModulePdh);
    return (regVal & cAf6RegCfgMdlTxInsEnMask) ? cAtFalse : cAtTrue;
    }

static eBool RequestSend(AtPdhDe3 self)
    {
    const uint32 cReqestSendTimeoutInMs = 50;
    tAtOsalCurTime curTime, startTime;
    uint32 elapse = 0;
    eBool requestDone = cAtFalse;
    uint32 regAddr = MdlTxInsCtrl(self);
    AtOsalCurTimeGet(&startTime);
    while ((elapse < cReqestSendTimeoutInMs) && (!requestDone))
        {
        uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePdh);
        mFieldIns(&regVal, cAf6RegCfgMdlTxInsEnMask, cAf6RegCfgMdlTxInsEnShift, 1);
        mChannelHwWrite(self, regAddr, regVal, cAtModulePdh);
        regVal = mChannelHwRead(self, regAddr, cAtModulePdh);
        requestDone = ((regVal  & cAf6RegCfgMdlTxInsEnMask) != 0) ? cAtTrue : cAtFalse;

        /* Calculate elapse time and retry */
        if (!requestDone)
            {
            AtOsalCurTimeGet(&curTime);
            elapse = mTimeIntervalInMsGet(startTime, curTime);
            }
        }
        
    /* Return */
    return requestDone;
    }

static eAtRet HwSendNoLock(ThaPdhMdlController self, uint8 *messageBuffer, uint32 bufferLength)
    {
    AtPdhDe3 de3 = AtPdhMdlControllerDe3Get((AtPdhMdlController)self);
    AtUnused(bufferLength);

    if (IsAvailable(de3) == cAtFalse)
        return cAtErrorRsrcNoAvail;

    /* Set C/R bit for each Tx message */
    TxCRSet(de3, AtPdhMdlControllerTxCRBitGet((AtPdhMdlController)self));
    HwBufferWrite(de3, messageBuffer);

    return RequestSend(de3) ? cAtOk : cAtErrorResourceBusy;
    }

static eAtRet HwSend(ThaPdhMdlController self, uint8 *messageBuffer, uint32 bufferLength)
    {
    eAtRet ret;
    AtPdhDe3 de3 = AtPdhMdlControllerDe3Get((AtPdhMdlController)self);
    AtModulePdh module = (AtModulePdh)PdhModule(de3);

    Tha60210031ModulePdhMdlLock(module);
    ret = HwSendNoLock(self, messageBuffer, bufferLength);
    Tha60210031ModulePdhMdlUnLock(module);

    return ret;
    }

static uint32 BufferFromDwordGet(uint8 *messageBuffer, uint32 bufferLength, uint32 currentIdx, uint32 data)
    {
    uint32 nextIndex = currentIdx;
    uint32 idx;
    for (idx = 0; idx < 4; idx ++)
        {
        uint32 shift = (8*idx);
        uint32 mask =  (cBit7_0 << shift);
        if (nextIndex >= bufferLength)
            break;

        messageBuffer[nextIndex] = (uint8)((data & mask) >> shift);
        nextIndex++;
        }
    return nextIndex;
    }

static uint32 RxBufferOffset(uint32 bufferId)
    {
    return (bufferId == 0) ? 0x0400 : 0x0;
    }

static uint32 RxBufferBaseAddress(AtPdhDe3 self, uint32 lineId)
    {
    uint32 baseAddress = mMethodsGet(PdhModule(self))->MdlRxBufBaseAddress(PdhModule(self)) + lineId;
    if (lineId > 31)
        baseAddress = (baseAddress - 32) + 0x0280;
    return baseAddress;
    }

static uint32 RxBufferDwordOffGet(uint32 lineId)
    {
    if (lineId > 31)
        return 16;
    return 32;
    }

static eAtRet HwBufferRead(AtPdhDe3 self, uint8 *messageBuffer, uint32 bufferLength, uint8 bufferId)
    {
    uint32 dword_i;
    uint32 mdlId  = Tha60210031RxMdlIdFromChannelGet(PdhModule(self), self);
    uint32 addressStart = RxBufferOffset(bufferId) + RxBufferBaseAddress(self, mdlId)+ MdlBaseAddress(self);
    uint32 currentIdx = 0;
    for (dword_i = 0; dword_i < cNumMdlDwords; dword_i ++)
        {
        uint32 regAddr = addressStart + (RxBufferDwordOffGet(mdlId) * dword_i);
        uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePdh);
        currentIdx = BufferFromDwordGet(messageBuffer, bufferLength, currentIdx, regVal);
        }
    return cAtOk;
    }

static eAtRet HwBufferReadNew(AtPdhDe3 self, uint8 *messageBuffer, uint32 bufferLength, uint8 bufferId)
    {
    uint32 dword_i;
    uint32 addressStart;
    uint8 sliceId,hwIdInSlice;
    uint32 baseAddress = mMethodsGet(PdhModule(self))->MdlRxBufBaseAddress(PdhModule(self));
    uint32 wordOffSet = mMethodsGet(PdhModule(self))->MdlRxBufWordOffset(PdhModule(self));
    uint32 currentIdx = 0;
    uint32 offset = 0;
    ThaPdhChannelHwIdGet((AtPdhChannel)self, cAtModulePdh, &sliceId, &hwIdInSlice);
    addressStart = baseAddress + (hwIdInSlice * wordOffSet) + (sliceId* 32UL)+ MdlBaseAddress(self);
    AtUnused(bufferId);
    for (dword_i = 0; dword_i < cNumMdlDwords; dword_i ++)
        {
        uint32 regAddr = addressStart + dword_i;
        uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePdh);
        currentIdx = BufferFromDwordGet(messageBuffer, (bufferLength - offset), currentIdx, regVal);
        if ((messageBuffer[1] == 0x08) && (dword_i == 0))
            offset = 1;
        }

    if(offset == 1)
        messageBuffer[bufferLength - 1] = 0;

    return cAtOk;
    }

static uint32 SwTypeGet(uint8 hwType)
    {
    switch (hwType)
        {
        case cMdlMsgTypePath:     return cAtPdhMdlControllerTypePathMessage;
        case cMdlMsgTypeIdleTest: return cAtPdhMdlControllerTypeIdleSignal;
        case cMdlMsgTestSignal:   return cAtPdhMdlControllerTypeTestSignal;
        default:                  return cAtPdhMdlControllerTypeUnknown;
        }
    }

static uint32 RxTypeClasifyGet(uint8 *data)
    {
    eBool validAddress = cAtFalse;

    if ((data[0] == cMdlAddress1CVal) || (data[0] == cMdlAddress1RVal))
        validAddress = cAtTrue;

    /* Check for AT&T format */
    if (data[1] == cMdlAddress2AttVal)
        {
        if (validAddress)
            return SwTypeGet(data[2]);

        return cAtPdhMdlControllerTypeUnknown;
        }

    /* Check for ANSI format */
    if (data[1] == cMdlAddress2AnsiVal)
        {
        if (validAddress && (data[2] == cMdlControlVal))
            return SwTypeGet(data[3]);

        return cAtPdhMdlControllerTypeUnknown;
        }

    return cAtPdhMdlControllerTypeUnknown;
    }

static uint32 RxTypeGet(AtPdhDe3 self, uint8 bufferId)
    {
    uint8  data[4];
    uint32 mdlId = Tha60210031RxMdlIdFromChannelGet(PdhModule(self), self);
    uint32 addressStart = RxBufferOffset(bufferId) + RxBufferBaseAddress(self, mdlId) + MdlBaseAddress(self);
    uint32 regVal = mChannelHwRead(self, addressStart, cAtModulePdh);
    BufferFromDwordGet(data, 4, 0, regVal);
    return RxTypeClasifyGet(data);
    }

static uint32 RxTypeFilterGet(AtPdhDe3 self)
    {
    uint32 regAddr, regVal;
    uint8  data[4];
    uint8 sliceId,hwIdInSlice;
    uint32 baseAddress = mMethodsGet(PdhModule(self))->MdlRxBufBaseAddress(PdhModule(self));
    uint32 wordOffSet = mMethodsGet(PdhModule(self))->MdlRxBufWordOffset(PdhModule(self));
    ThaPdhChannelHwIdGet((AtPdhChannel)self, cAtModulePdh, &sliceId, &hwIdInSlice);
    regAddr = baseAddress + (hwIdInSlice * wordOffSet) + (sliceId * 32UL) + MdlBaseAddress(self);
    regVal = mChannelHwRead(self, regAddr, cAtModulePdh);
    BufferFromDwordGet(data, 4, 0, regVal);
    return RxTypeClasifyGet(data);
    }

static uint32 RegisterSticky(AtPdhDe3 self, uint8 bufferId, uint32 *mask)
    {
    uint32 baseAddress = mMethodsGet(PdhModule(self))->MdlRxStickyBaseAddress(PdhModule(self), bufferId);
    uint32 offset = mMethodsGet(PdhModule(self))->MdlRxStickyOffset(PdhModule(self), self);
    *mask = mMethodsGet(PdhModule(self))->MdlRxStickyMask(PdhModule(self), self);
    return baseAddress + offset + MdlBaseAddress(self);
    }

static void BufferReceivedStickyClear(AtPdhDe3 self, uint8 bufferId)
    {
    uint32 mask;
    uint32 regAddr = RegisterSticky(self, bufferId, &mask);
    mChannelHwWrite(self, regAddr, mask, cAtModulePdh);
    }

static eBool BufferReceived(AtPdhDe3 self, uint8 bufferId)
    {
    uint32 mask;
    uint32 regAddr = RegisterSticky(self, bufferId, &mask);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePdh);

    if (AtDeviceIsSimulated(AtChannelDeviceGet((AtChannel)self)))
        return cAtTrue;

    return (regVal & mask) ? cAtTrue :cAtFalse;
    }

static eBool HwMessageReceived(ThaPdhMdlController self)
    {
    AtPdhDe3 de3 = AtPdhMdlControllerDe3Get((AtPdhMdlController)self);
    uint8 buffer_i;

    for (buffer_i = 0; buffer_i < cNumberOfHwBuffer; buffer_i++)
        {
        uint32 rxType;

        if (RxFilterMessagePerTypeIsSupported((AtPdhMdlController)self) && (buffer_i == 0))
            continue;

        if (!BufferReceived(de3, buffer_i))
            continue;

        if (RxFilterMessagePerTypeIsSupported((AtPdhMdlController)self))
            rxType = RxTypeFilterGet(de3);
        else
            rxType = RxTypeGet(de3, buffer_i);

        if (rxType != AtPdhMdlControllerTypeGet((AtPdhMdlController)self))
            continue;

        /* Cached this buffer */
        mThis(self)->receivedBuffer = buffer_i;
        return cAtTrue;
        }

    return cAtFalse;
    }

static uint32 HwReceived(ThaPdhMdlController self, uint8 *messageBuffer, uint32 bufferLength)
    {
    uint32 rxLength;
    AtPdhDe3 de3 = AtPdhMdlControllerDe3Get((AtPdhMdlController)self);

    if (RxFilterMessagePerTypeIsSupported((AtPdhMdlController)self))
        rxLength = HwBufferReadNew(AtPdhMdlControllerDe3Get((AtPdhMdlController)self), messageBuffer, bufferLength, mThis(self)->receivedBuffer);
    else
        rxLength = HwBufferRead(AtPdhMdlControllerDe3Get((AtPdhMdlController)self), messageBuffer, bufferLength, mThis(self)->receivedBuffer);

    /* Clear Sticky */
    BufferReceivedStickyClear(de3, mThis(self)->receivedBuffer);
    return rxLength;
    }

static void Debug(AtPdhMdlController self)
    {
    AtPdhDe3 de3 = AtPdhMdlControllerDe3Get((AtPdhMdlController)self);
    AtPrintc(cSevNeutral, "*==============================================================================================*\r\n");
    AtPrintc(cSevInfo, "MdlOffset                      :: 0x%x \r\n",MdlOffset(self));
    AtPrintc(cSevInfo, "txCtrlReg                      :: 0x%x \r\n",mMethodsGet(PdhModule(de3))->MdlTxCtrlReg(PdhModule(de3), de3));
    AtPrintc(cSevInfo, "rxCfgReg                       :: 0x%x \r\n",mMethodsGet(PdhModule(de3))->MdlRxCfgReg(PdhModule(de3), de3));
    AtPrintc(cSevInfo, "rxBufBaseAddress               :: 0x%x \r\n",mMethodsGet(PdhModule(de3))->MdlRxBufBaseAddress(PdhModule(de3)));
    AtPrintc(cSevInfo, "rxBufWordOffset                :: 0x%x \r\n",mMethodsGet(PdhModule(de3))->MdlRxBufWordOffset(PdhModule(de3)));
    AtPrintc(cSevInfo, "rxStickyBaseAddr               :: 0x%x \r\n",mMethodsGet(PdhModule(de3))->MdlRxStickyBaseAddress(PdhModule(de3), 1));
    AtPrintc(cSevInfo, "MdlRxStickyOffset              :: 0x%x \r\n",mMethodsGet(PdhModule(de3))->MdlRxStickyOffset(PdhModule(de3), de3));
    AtPrintc(cSevInfo, "MdlRxStickyMask                :: 0x%x \r\n",mMethodsGet(PdhModule(de3))->MdlRxStickyMask(PdhModule(de3), de3));
    AtPrintc(cSevInfo, "RxMdlIdFromChannel             :: %u \r\n",mMethodsGet(PdhModule(de3))->RxMdlIdFromChannelGet(PdhModule(de3), de3));
    m_AtPdhMdlControllerMethods->Debug(self);
    }

static uint32 TxMsgCountersGet(AtPdhMdlController self, eBool r2c)
    {
    AtPdhDe3 de3 = AtPdhMdlControllerDe3Get(self);
    uint32 regAddr = mMethodsGet(PdhModule(de3))->MdlTxCounterOffset(PdhModule(de3), de3)
                    + MdlBaseAddress(de3)
                    + mMethodsGet(PdhModule(de3))->MdlTxPktOffset(PdhModule(de3),
                                                                  AtPdhMdlControllerTypeGet(self),
                                                                  r2c);
    return mChannelHwRead(AtPdhMdlControllerDe3Get(self), regAddr, cAtModulePdh);
    }

static uint32 TxByteCountersGet(AtPdhMdlController self, eBool r2c)
    {
    AtPdhDe3 de3 = AtPdhMdlControllerDe3Get(self);
    uint32 regAddr = mMethodsGet(PdhModule(de3))->MdlTxCounterOffset(PdhModule(de3), de3)
                    + MdlBaseAddress(de3)
                    + mMethodsGet(PdhModule(de3))->MdlTxByteOffset(PdhModule(de3),
                                                                  AtPdhMdlControllerTypeGet(self),
                                                                  r2c);
    return mChannelHwRead(AtPdhMdlControllerDe3Get(self), regAddr, cAtModulePdh);
    }

static uint32 RxMsgCountersGet(AtPdhMdlController self, eBool r2c)
    {
    AtPdhDe3 de3 = AtPdhMdlControllerDe3Get(self);
    uint32 regAddr = mMethodsGet(PdhModule(de3))->MdlRxCounterOffset(PdhModule(de3), de3)
                    + MdlBaseAddress(de3)
                    + mMethodsGet(PdhModule(de3))->MdlRxPktOffset(PdhModule(de3),
                                                                  AtPdhMdlControllerTypeGet(self),
                                                                  r2c);
    return mChannelHwRead(AtPdhMdlControllerDe3Get(self), regAddr, cAtModulePdh);
    }

static uint32 RxByteCountersGet(AtPdhMdlController self, eBool r2c)
    {
    AtPdhDe3 de3 = AtPdhMdlControllerDe3Get(self);
    uint32 regAddr = mMethodsGet(PdhModule(de3))->MdlRxCounterOffset(PdhModule(de3), de3)
                    + MdlBaseAddress(de3)
                    + mMethodsGet(PdhModule(de3))->MdlRxByteOffset(PdhModule(de3),
                                                                  AtPdhMdlControllerTypeGet(self),
                                                                  r2c);
    return mChannelHwRead(AtPdhMdlControllerDe3Get(self), regAddr, cAtModulePdh);
    }

static uint32 RxDropMsgCountersGet(AtPdhMdlController self, eBool r2c)
    {
    AtPdhDe3 de3 = AtPdhMdlControllerDe3Get(self);
    uint32 regAddr = mMethodsGet(PdhModule(de3))->MdlRxCounterOffset(PdhModule(de3), de3)
                    + MdlBaseAddress(de3)
                    + mMethodsGet(PdhModule(de3))->MdlRxDropOffset(PdhModule(de3),
                                                                  AtPdhMdlControllerTypeGet(self),
                                                                  r2c);
    return mChannelHwRead(AtPdhMdlControllerDe3Get(self), regAddr, cAtModulePdh);
    }

static uint32 CountersRead(AtPdhMdlController self, uint32 counterType, eBool r2c)
    {
    switch (counterType)
        {
        case cAtPdhMdlControllerCounterTypeTxPkts:     return TxMsgCountersGet(self, r2c);
        case cAtPdhMdlControllerCounterTypeTxBytes:    return TxByteCountersGet(self, r2c);
        case cAtPdhMdlControllerCounterTypeRxPkts:     return RxMsgCountersGet(self, r2c);
        case cAtPdhMdlControllerCounterTypeRxBytes:    return RxByteCountersGet(self, r2c);
        case cAtPdhMdlControllerCounterTypeRxDropPkts: return RxDropMsgCountersGet(self, r2c);
        default :return 0;
        }
    }

static uint32 CountersGet(AtPdhMdlController self, uint32 counterType)
    {
    return CountersRead(self, counterType, cAtFalse);
    }

static uint32 CountersClearGet(AtPdhMdlController self, uint32 counterType)
    {
    return CountersRead(self, counterType, cAtTrue);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210031PdhMdlController *object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(receivedBuffer);
    }

static eAtRet MdlHwBufferFlushZero(AtPdhDe3 de3)
    {
    uint32 dword_i;
    uint32 addressStart;
    uint8 sliceId, hwIdInSlice;
    Tha60210031ModulePdh pdhModule = PdhModule(de3);
    uint32 baseAddress = mMethodsGet(pdhModule)->MdlRxBufBaseAddress(pdhModule);
    uint32 wordOffSet = mMethodsGet(pdhModule)->MdlRxBufWordOffset(pdhModule);

    ThaPdhChannelHwIdGet((AtPdhChannel)de3, cAtModulePdh, &sliceId, &hwIdInSlice);
    addressStart = baseAddress + (hwIdInSlice * wordOffSet) + (sliceId* 32UL)+ MdlBaseAddress(de3);
    for (dword_i = 0; dword_i < cNumMdlDwords; dword_i ++)
        mChannelHwWrite(de3, addressStart + dword_i, 0x0, cAtModulePdh);

    return cAtOk;
    }

static void OverrideAtObject(ThaPdhMdlController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideThaPdhMdlController(ThaPdhMdlController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhMdlControllerOverride, mMethodsGet(self), sizeof(m_ThaPdhMdlControllerOverride));

        mMethodOverride(m_ThaPdhMdlControllerOverride, HwSend);
        mMethodOverride(m_ThaPdhMdlControllerOverride, HwReceived);
        mMethodOverride(m_ThaPdhMdlControllerOverride, HwMessageReceived);
        }

    mMethodsSet(self, &m_ThaPdhMdlControllerOverride);
    }

static void OverrideAtPdhMdlController(ThaPdhMdlController self)
    {
    AtPdhMdlController controller = (AtPdhMdlController) self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPdhMdlControllerMethods = mMethodsGet(controller);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPdhMdlControllerOverride, m_AtPdhMdlControllerMethods, sizeof(m_AtPdhMdlControllerOverride));

        mMethodOverride(m_AtPdhMdlControllerOverride, Enable);
        mMethodOverride(m_AtPdhMdlControllerOverride, Debug);
        mMethodOverride(m_AtPdhMdlControllerOverride, CountersGet);
        mMethodOverride(m_AtPdhMdlControllerOverride, CountersClearGet);
        }

    mMethodsSet(controller, &m_AtPdhMdlControllerOverride);
    }

static void Override(ThaPdhMdlController self)
    {
    OverrideAtObject(self);
    OverrideThaPdhMdlController(self);
    OverrideAtPdhMdlController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031PdhMdlController);
    }

AtPdhMdlController Tha60210031PdhMdlControllerObjectInit(AtPdhMdlController self, AtPdhDe3 de3, uint32 mdlType)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhMdlControllerObjectInit(self, de3, mdlType) == NULL)
        return NULL;

    /* Setup methods */
    Override((ThaPdhMdlController)self);
    m_methodsInit = 1;

    return self;
    }

AtPdhMdlController Tha60210031PdhMdlControllerNew(AtPdhDe3 self, uint32 mdlType)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhMdlController newMdlController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newMdlController == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031PdhMdlControllerObjectInit(newMdlController, self, mdlType);
    }

void Tha60210031PdhMdlBufferReceivedStickyClear(AtPdhDe3 self)
    {
    BufferReceivedStickyClear(self, 0);
    BufferReceivedStickyClear(self, 1);
    }

eAtRet Tha60210031PdhDe3MdlHwBufferFlush(AtPdhDe3 de3)
    {
    if (de3)
        return MdlHwBufferFlushZero(de3);
    return cAtErrorNotExist;
    }

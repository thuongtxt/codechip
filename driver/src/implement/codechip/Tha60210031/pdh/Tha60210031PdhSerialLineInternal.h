/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60210031PdhSerialLineInternal.h
 * 
 * Created Date: Nov 11, 2016
 *
 * Description : SerialLine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031PDHSERIALLINEINTERNAL_H_
#define _THA60210031PDHSERIALLINEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/pdh/ThaPdhDe3SerialLine.h"
#include "Tha60210031PdhSerialLine.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031PdhSerialLine * Tha60210031PdhSerialLine;

typedef struct tTha60210031PdhSerialLine
    {
    tThaPdhDe3SerialLine super;
    }tTha60210031PdhSerialLine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhSerialLine Tha60210031PdhSerialLineObjectInit(AtPdhSerialLine self, uint32 channelId, AtModulePdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031PDHSERIALLINEINTERNAL_H_ */

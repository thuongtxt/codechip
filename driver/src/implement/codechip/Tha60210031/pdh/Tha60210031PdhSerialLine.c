/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH Serial Line
 *
 * File        : Tha60210031PdhSerialLine.c
 *
 * Created Date: Mar 4, 2015
 *
 * Description : Serial Line
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/
#include "AtModulePdh.h"
#include "AtSdhLine.h"
#include "../../../../generic/sdh/AtSdhVcInternal.h"
#include "../../../../generic/ber/AtModuleBerInternal.h"
#include "../../../default/pdh/ThaModulePdhForStmReg.h"
#include "../../../default/pdh/ThaModulePdh.h"
#include "../../../default/pdh/ThaPdhDe3.h"
#include "../../../default/man/ThaDeviceInternal.h"
#include "../sdh/Tha60210031SdhVcInternal.h"
#include "Tha60210031PdhSerialLineInternal.h"
#include "Tha60210031ModulePdh.h"
#include "Tha60210031ModulePdhReg.h"
#include "Tha60210031PdhSerialLineInternal.h"

/*--------------------------- Include files ----------------------------------*/

/*--------------------------- Define -----------------------------------------*/
/* Internal Registers*/
#define cLiuLineEc1Mode     0x10b
#define cNumBitsInDword     32

#define cThaRegLIU_Tx_EXT_Mode      0x00010E
#define cThaRegLIU_Tx_Ch_Enb        0x00010F

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210031PdhSerialLine)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPdhSerialLineMethods     m_AtPdhSerialLineOverride;
static tThaPdhDe3SerialLineMethods m_ThaPdhDe3SerialLineOverride;
static tAtChannelMethods           m_AtChannelOverride;

/* Save super implementation */
static const tAtChannelMethods           *m_AtChannelMethods       = NULL;
static const tAtPdhSerialLineMethods     *m_AtPdhSerialLineMethods = NULL;
static const tThaPdhDe3SerialLineMethods *m_ThaPdhDe3SerialLineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet HwIdFactorGet(ThaPdhDe3SerialLine self, uint32 phyModule, uint32 *slice, uint32 *hwIdInSlice)
    {
    uint32 channelId = AtChannelIdGet((AtChannel)self);
    AtUnused(phyModule);
    *slice = channelId / 24;
    *hwIdInSlice = channelId % 24;
    return cAtOk;
    }

static AtModulePdh PdhModule(AtPdhSerialLine self)
    {
    return (AtModulePdh)AtChannelModuleGet((AtChannel)self);
    }

static AtPdhDe3 De3Get(AtPdhSerialLine self)
    {
    return ThaPdhDe3SerialLineDe3ObjectGet((ThaPdhDe3SerialLine)self);
    }

static eAtRet Ec1Enable(AtPdhSerialLine self, eBool ec1)
    {
    if (Tha60210031ModulePdhEc1DiagLiuModeShouldEnable((Tha60210031ModulePdh)PdhModule(self)))
        ThaModulePdhDiagLiuEc1ModeEnable((AtChannel)self, ec1);

    /* Invert RX clock */
    if (Tha60210031ModulePdhEc1LiuRxClockInvertionShouldEnable((Tha60210031ModulePdh)PdhModule(self)))
        ThaModulePdhDiagLiuRxClockInvertEnable((AtChannel)self, cAtTrue);

    return cAtOk;
    }

static eAtRet StsMapDe3Update(AtPdhSerialLine self, eAtPdhDe3SerialLineMode mode)
    {
    eAtRet ret = cAtOk;
    AtSdhChannel sdhVc = (AtSdhChannel)AtPdhChannelVcInternalGet((AtPdhChannel)De3Get(self));
    
    if (!ThaPdhStsDe3LiuMapVc3IsEnabled((ThaStmModulePdh)PdhModule(self), sdhVc))
        return cAtOk;

    if (mode == cAtPdhDe3SerialLineModeDs3)
        {
        ret|= ThaPdhStsMapMdSet(PdhModule(self), sdhVc, cThaStsMapDs3Vc3Md);
        ret|= ThaPdhStsVtDeMapCtrlSet(PdhModule(self), sdhVc, cThaStsVtVc3Ds3DmapMd);
        ret|= Tha60210031PdhStsDe3LiuOverVc3CepSet(PdhModule(self), sdhVc);
        }

    if (mode == cAtPdhDe3SerialLineModeE3)
        {
        ret|= ThaPdhStsMapMdSet(PdhModule(self), sdhVc, cThaStsMapE3Vc3Md);
        ret|= ThaPdhStsVtDeMapCtrlSet(PdhModule(self), sdhVc, cThaStsVtVc3E3DmapMd);
        ret|= Tha60210031PdhStsDe3LiuOverVc3CepSet(PdhModule(self), sdhVc);
        }

    return ret;
    }

static eBool ServiceIsRunning(AtChannel self)
    {
    uint32 currentMode = AtPdhSerialLineModeGet((AtPdhSerialLine)self);
    AtModulePdh pdhModule = (AtModulePdh)AtChannelModuleGet(self);
    AtPdhDe3 de3 = De3Get((AtPdhSerialLine)self);
    AtSdhVc vc3FromDe3  = AtPdhChannelVcInternalGet((AtPdhChannel)de3);

    if (currentMode == cAtPdhDe3SerialLineModeEc1)
        return m_AtChannelMethods->ServiceIsRunning(self);

    if (ThaPdhStsDe3LiuMapVc3IsEnabled((ThaStmModulePdh)pdhModule, (AtSdhChannel)vc3FromDe3))
        return AtChannelServiceIsRunning((AtChannel)vc3FromDe3);

    return m_AtChannelMethods->ServiceIsRunning(self);
    }

static eAtRet De3LiuModeUpdate(AtPdhSerialLine self, eAtPdhDe3SerialLineMode mode)
    {
    eAtRet ret = cAtOk;
    Tha60210031ModulePdh modulePdh = (Tha60210031ModulePdh)AtChannelModuleGet((AtChannel)self);
    AtChannel de3 = AtPdhSerialLineChannelGet(self);

    if (Tha60210031ModulePdhEc1DiagLiuModeShouldEnable(modulePdh))
        ret |= ThaModulePdhDiagLiuLineModeSet(de3, (mode == cAtPdhDe3SerialLineModeDs3) ? cAtTrue : cAtFalse);

    if (Tha60210031ModulePdhEc1LiuRxClockInvertionShouldEnable(modulePdh))
        ret |= ThaModulePdhDiagLiuRxClockInvertEnable(de3, cAtTrue);

    return ret;
    }

static eAtRet ModeSet(AtPdhSerialLine self, uint32 mode)
    {
    eAtRet ret = cAtOk;
    uint32 currentMode = AtPdhSerialLineModeGet(self);
    eBool ec1Mode = (mode == cAtPdhDe3SerialLineModeEc1) ? cAtTrue : cAtFalse;

    if ((currentMode != mode) && AtChannelServiceIsRunning((AtChannel)self))
        return cAtErrorChannelBusy;

    /* Need to call this first since Core may be not available at diagnostic phase */
    Ec1Enable(self, ec1Mode);
    if (currentMode == mode)
        return ret;

    ret = m_AtPdhSerialLineMethods->ModeSet(self, mode);
    if (ret != cAtOk)
        return ret;

    /* process for the case switching between DS3 LIU and E3 LIU over CEP */
    if (mode != cAtPdhDe3SerialLineModeEc1)
        {
        ret |= StsMapDe3Update(self, mode);
        ret |= De3LiuModeUpdate(self, mode);
        }

    if (ret != cAtOk)
        {
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical,
                                AtSourceLocation, "ERROR: when control DS3/E3 mode over VC#3 \r\n");
        return ret;
        }

    return ret;
    }

static eAtRet HwLiuModeSet(ThaPdhDe3SerialLine self, AtChannel channel, eBool isEc1Mode)
    {
    uint32 regAddr;
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 lineId = AtChannelIdGet((AtChannel)self);
    uint8 ec1Mode = (isEc1Mode) ? 1 : 0;
    AtUnused(channel);

    regAddr = cLiuLineEc1Mode;
    mChannelHwLongRead(self, regAddr, longRegVal, cThaLongRegMaxSize, cAtModulePdh);
    if (lineId < cNumBitsInDword)
        mFieldIns(&longRegVal[0], cBit0 << lineId, lineId, ec1Mode);
    else
        mFieldIns(&longRegVal[1], cBit0 << (lineId - cNumBitsInDword), lineId - cNumBitsInDword, ec1Mode);
    mChannelHwLongWrite(self, regAddr, longRegVal, cThaLongRegMaxSize, cAtModulePdh);

    return cAtOk;
    }

static eAtRet MapEc1Line(ThaPdhDe3SerialLine self, AtSdhLine line)
    {
    return HwLiuModeSet(self, (AtChannel)line, cAtTrue);
    }

static eAtRet MapDe3Line(ThaPdhDe3SerialLine self, AtPdhDe3 de3)
    {
    return HwLiuModeSet(self, (AtChannel)de3, cAtFalse);
    }

static uint32 ChannelIdToMask(uint32 channelId)
    {
    return cBit0 << (channelId % 32);
    }

static uint32 ChannelIdToShift(uint32 channelId)
    {
    return (channelId % 32);
    }

static eAtRet HelperHwEnable(AtChannel self, uint32 address, eBool enable)
    {
    uint32 longRegVal[cThaNumDwordsInLongReg];
    uint32 dwordIndex, channelMask, channelShift;
    uint32 channelId = AtChannelIdGet(self);

    dwordIndex = channelId / 32U;
    channelMask = ChannelIdToMask(channelId);
    channelShift = ChannelIdToShift(channelId);

    mChannelHwLongRead(self, address, longRegVal, cThaNumDwordsInLongReg, cAtModulePdh);
    mRegFieldSet(longRegVal[dwordIndex], channel, enable ? 1 : 0);
    mChannelHwLongWrite(self, address, longRegVal, cThaNumDwordsInLongReg, cAtModulePdh);

    return cAtOk;
    }

static eBool HelperHwIsEnabled(AtChannel self, uint32 address)
    {
    uint32 longRegVal[cThaNumDwordsInLongReg];
    uint32 dwordIndex, channelMask, channelShift;
    uint32 channelId = AtChannelIdGet(self);

    dwordIndex = channelId / 32U;
    channelMask = ChannelIdToMask(channelId);
    channelShift = ChannelIdToShift(channelId);

    mChannelHwLongRead(self, address, longRegVal, cThaNumDwordsInLongReg, cAtModulePdh);
    return mRegField(longRegVal[dwordIndex], channel) ? cAtTrue : cAtFalse;
    }

static eAtRet TxEnable(AtChannel self, eBool enable)
    {
    return HelperHwEnable(self, cThaRegLIU_Tx_Ch_Enb, enable);
    }

static eBool TxIsEnabled(AtChannel self)
    {
    return HelperHwIsEnabled(self, cThaRegLIU_Tx_Ch_Enb);
    }

static eBool TxCanEnable(AtChannel self, eBool enable)
    {
    AtUnused(enable);
    return ThaModulePdhDe3TxLiuEnableIsSupported((ThaModulePdh)AtChannelModuleGet(self));
    }

static eBool TimingModeIsSupported(AtChannel self, eAtTimingMode timingMode)
    {
    AtUnused(timingMode);
    if (!ThaModulePdhDe3TxLiuEnableIsSupported((ThaModulePdh)AtChannelModuleGet(self)))
        return cAtFalse;

    return cAtTrue;
    }

static eAtRet TimingSet(AtChannel self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    AtUnused(timingSource);

    /* Just pass if not supported. */
    if (!AtChannelTimingModeIsSupported(self, timingMode))
        return cAtOk;

    if (timingMode == cAtTimingModeExt1Ref)
        return HelperHwEnable(self, cThaRegLIU_Tx_EXT_Mode, cAtTrue);

    return HelperHwEnable(self, cThaRegLIU_Tx_EXT_Mode, cAtFalse);
    }

static void OverrideAtPdhSerialLine(AtPdhSerialLine self)
    {
    AtPdhSerialLine serialLine = (AtPdhSerialLine)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPdhSerialLineMethods = mMethodsGet(serialLine);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPdhSerialLineOverride, m_AtPdhSerialLineMethods, sizeof(tAtPdhSerialLineMethods));

        mMethodOverride(m_AtPdhSerialLineOverride, ModeSet);
        }

    mMethodsSet(serialLine, &m_AtPdhSerialLineOverride);
    }

static void OverrideThaPdhDe3SerialLine(AtPdhSerialLine self)
    {
    ThaPdhDe3SerialLine serialLine = (ThaPdhDe3SerialLine)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPdhDe3SerialLineMethods = mMethodsGet(serialLine);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhDe3SerialLineOverride, m_ThaPdhDe3SerialLineMethods, sizeof(tThaPdhDe3SerialLineMethods));

        mMethodOverride(m_ThaPdhDe3SerialLineOverride, HwIdFactorGet);
        mMethodOverride(m_ThaPdhDe3SerialLineOverride, MapEc1Line);
        mMethodOverride(m_ThaPdhDe3SerialLineOverride, MapDe3Line);
        }

    mMethodsSet(serialLine, &m_ThaPdhDe3SerialLineOverride);
    }

static eAtRet Init(AtChannel self)
    {
    AtPdhSerialLineModeSet((AtPdhSerialLine)self, cAtPdhDe3SerialLineModeDs3);
    return cAtOk;
    }

static void OverrideAtChannel(AtPdhSerialLine self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, ServiceIsRunning);
        mMethodOverride(m_AtChannelOverride, TxEnable);
        mMethodOverride(m_AtChannelOverride, TxIsEnabled);
        mMethodOverride(m_AtChannelOverride, TxCanEnable);
        mMethodOverride(m_AtChannelOverride, TimingSet);
        mMethodOverride(m_AtChannelOverride, TimingModeIsSupported);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtPdhSerialLine self)
    {
    OverrideAtPdhSerialLine(self);
    OverrideThaPdhDe3SerialLine(self);
    OverrideAtChannel(self);
    }

AtPdhSerialLine Tha60210031PdhSerialLineObjectInit(AtPdhSerialLine self, uint32 channelId, AtModulePdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tTha60210031PdhSerialLine));

    /* Supper constructor */
    if (ThaPdhDe3SerialLineObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPdhSerialLine Tha60210031PdhSerialLineNew(uint32 channelId, AtModulePdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhSerialLine newSerialLine = mMethodsGet(osal)->MemAlloc(osal, sizeof(tTha60210031PdhSerialLine));
    if (newSerialLine == NULL)
        return NULL;

    /* construct it */
    return Tha60210031PdhSerialLineObjectInit(newSerialLine, channelId, module);
    }

eAtRet Tha60210031PdhSerLineStsMapDe3Update(AtPdhSerialLine self, eAtPdhDe3SerialLineMode mode)
    {
    return StsMapDe3Update(self, mode);
    }

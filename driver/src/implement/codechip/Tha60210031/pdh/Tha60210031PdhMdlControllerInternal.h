/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60210031PdhMdlControllerInternal.h
 * 
 * Created Date: Oct 28, 2017
 *
 * Description : MDL controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031PDHMDLCONTROLLERINTERNAL_H_
#define _THA60210031PDHMDLCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/pdh/ThaPdhMdlControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031PdhMdlController
    {
    tThaPdhMdlController super;
    uint8 receivedBuffer;
    }tTha60210031PdhMdlController;

/*--------------------------- Forward declarations ---------------------------*/
AtPdhMdlController Tha60210031PdhMdlControllerObjectInit(AtPdhMdlController self, AtPdhDe3 de3, uint32 mdlType);

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031PDHMDLCONTROLLERINTERNAL_H_ */


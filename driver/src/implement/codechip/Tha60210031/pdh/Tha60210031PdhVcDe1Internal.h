/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60210031PdhVcDe1Internal.h
 * 
 * Created Date: Feb 6, 2017
 *
 * Description : VC's DS1/E1 class representation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031PDHVCDE1INTERNAL_H_
#define _THA60210031PDHVCDE1INTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "Tha60210031PdhDe1.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031PdhVcDe1
    {
    tThaPdhVcDe1 super;
    }tTha60210031PdhVcDe1;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhDe1 Tha60210031PdhVcDe1ObjectInit(AtPdhDe1 self, AtSdhVc vc1x, AtModulePdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031PDHVCDE1INTERNAL_H_ */


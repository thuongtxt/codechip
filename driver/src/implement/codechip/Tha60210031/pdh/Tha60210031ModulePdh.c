/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60210031ModulePdh.c
 *
 * Created Date: Jun 23, 2014
 *
 * Description : PDH module
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/pdh/AtPdhDe3Internal.h"
#include "../../../../generic/pdh/AtPdhPrmControllerInternal.h"
#include "../../../../generic/pdh/AtPdhMdlControllerInternal.h"
#include "../../../default/ocn/ThaModuleOcn.h"
#include "../../../default/man/ThaDevice.h"
#include "../../../default/pdh/ThaModulePdhReg.h"
#include "../../../default/pdh/ThaModulePdhForStmReg.h"
#include "../../../default/pdh/ThaPdhMdlController.h"
#include "../../../default/pdh/ThaPdhPrmController.h"
#include "../../Tha60210011/common/Tha602100xxCommon.h"
#include "../man/Tha60210031DeviceReg.h"
#include "../sdh/Tha60210031SdhVcInternal.h"
#include "Tha60210031PdhSerialLine.h"
#include "Tha60210031ModulePdh.h"

#include "../../../default/pdh/diag/ThaPdhErrorGenerator.h"
#include "Tha60210031ModulePdhReg.h"
#include "Tha60210031PdhDe1.h"
#include "Tha60210031PdhMdlController.h"
#include "Tha60210031PdhMdlControllerReg.h"
#include "Tha60210031PdhDe3.h"

/*--------------------------- Define -----------------------------------------*/
#define cLiuPortMonitorRegAddr               (0xF0004a)
#define cLiuRxClockFreqRegAddr               (0xF00068)
#define cLiuTxClockFreqRegAddr               (0xF00069)

#define cAf6_pdh_slice_intr_Mask(sl)         (1U << (sl))
#define cAf6_pdh_de3_intr_Mask(_de3)         (1U << (_de3))
#define cAf6_pdh_de1_intr_Mask(_de1)         (1U << (_de1))

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210031ModulePdh*)self)
#define mMakeRegBase(absoluteAddress) (absoluteAddress - cThaModulePdhOriginalBaseAddress)

/*--------------------------- Local typedefs ---------------------------------*/
typedef AtPdhMdlController (*MdlControllerFunction)(AtPdhDe3 self);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210031ModulePdhMethods m_methods;

/* Override */
static tAtObjectMethods             m_AtObjectOverride;
static tAtModuleMethods             m_AtModuleOverride;
static tAtModulePdhMethods          m_AtModulePdhOverride;
static tThaModulePdhMethods         m_ThaModulePdhOverride;
static tThaStmModulePdhMethods      m_ThaStmModulePdhOverride;
static tTha60150011ModulePdhMethods m_Tha60150011ModulePdhOverride;

/* Save super implementation */
static const tAtObjectMethods     *m_AtObjectMethods     = NULL;
static const tAtModuleMethods     *m_AtModuleMethods     = NULL;
static const tAtModulePdhMethods  *m_AtModulePdhMethods  = NULL;
static const tThaModulePdhMethods *m_ThaModulePdhMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 SliceBase(ThaModulePdh self, uint32 sliceId)
    {
    return mMethodsGet(self)->BaseAddress(self) + (sliceId * 0x100000UL);
    }

static eBool CasSupported(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool MdlIsSupported(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool InbandLoopcodeIsSupported(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 StartVersionSupportBom(ThaModulePdh self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 SerialIdFromHwIdGet(Tha60210031ModulePdh self, uint8 sliceId, uint8 de3InSlice)
    {
    AtUnused(self);
    return (uint32)(sliceId * 24 + de3InSlice);
    }

static AtPdhDe3 De3ChannelFromHwIdGet(ThaModulePdh self, eAtModule phyModule, uint8 sliceId, uint8 de3Id)
    {
    uint32 serialId = Tha6021ModulePdhSerialIdFromHwIdGet(self, sliceId, de3Id);
    AtUnused(phyModule);
    return Tha6021ModulePdhDe3FromSerialIdGet(self, serialId);
    }

static AtPdhDe3 De3FromSerialIdGet(ThaModulePdh self, uint32 serialId)
    {
    AtPdhSerialLine serialLine = AtModulePdhDe3SerialLineGet((AtModulePdh)self, serialId);

    if (AtPdhSerialLineModeGet(serialLine) == cAtPdhDe3SerialLineModeEc1)
        {
        AtSdhLine ec1 = ThaPdhDe3SerialLineEc1ObjectGet((ThaPdhDe3SerialLine)serialLine);
        AtSdhChannel au3 = AtSdhChannelSubChannelGet((AtSdhChannel)ec1, 0);
        AtSdhChannel vc3 = AtSdhChannelSubChannelGet((AtSdhChannel)au3, 0);

        if (AtSdhChannelMapTypeGet(vc3) == cAtSdhVcMapTypeVc3MapDe3)
            return (AtPdhDe3)AtSdhChannelMapChannelGet(vc3);

        return NULL;
        }

    return AtModulePdhDe3Get((AtModulePdh)self, serialId);
    }

static AtPdhDe1 De1ChannelFromHwIdGet(ThaModulePdh self, eAtModule phyModule, uint8 sliceId, uint8 de3Id, uint8 de2Id, uint8 de1Id)
    {
    uint32 serialId = Tha6021ModulePdhSerialIdFromHwIdGet(self, sliceId, de3Id);
    AtUnused(phyModule);
    return Tha6021ModulePdhDe1FromSerialIdGet(self, serialId, de2Id, de1Id);
    }

static AtPdhDe1 De1FromSerialIdGet(ThaModulePdh self, uint32 serialId, uint8 de2Id, uint8 de1Id)
    {
    AtPdhSerialLine serialLine = AtModulePdhDe3SerialLineGet((AtModulePdh)self, serialId);
    AtPdhDe3 de3 = NULL;

    if (AtPdhSerialLineModeGet(serialLine) == cAtPdhDe3SerialLineModeEc1)
        {
        AtSdhLine ec1 = ThaPdhDe3SerialLineEc1ObjectGet((ThaPdhDe3SerialLine)serialLine);
        AtSdhChannel au3 = AtSdhChannelSubChannelGet((AtSdhChannel)ec1, 0);
        AtSdhChannel vc3;
        uint8 mapType;

        vc3 = AtSdhChannelSubChannelGet((AtSdhChannel)au3, 0);
        mapType = AtSdhChannelMapTypeGet(vc3);

        if (mapType == cAtSdhVcMapTypeVc3MapDe3)
            de3 = (AtPdhDe3)AtSdhChannelMapChannelGet(vc3);
        else
            {
            if (mapType == cAtSdhVcMapTypeVc3Map7xTug2s)
                {
                AtSdhChannel tu1x = AtSdhChannelSubChannelGet(AtSdhChannelSubChannelGet(vc3, de2Id), de1Id);
                AtSdhChannel vc1x = AtSdhChannelSubChannelGet(tu1x, 0);

                if (AtSdhChannelMapTypeGet(vc1x) == cAtSdhVcMapTypeVc1xMapDe1)
                    return (AtPdhDe1)AtSdhChannelMapChannelGet(vc1x);
                }
            return NULL;
            }
        }
    else
        de3 = AtModulePdhDe3Get((AtModulePdh)self, serialId);

    if (de3)
        {
        ThaPdhDe3De1Hw2SwIdGet((ThaPdhDe3)de3, de2Id, de1Id, &de2Id, &de1Id);
        return AtPdhDe3De1Get(de3, de2Id, de1Id);
        }

    return NULL;
    }

static eBool HasDe3Interrupt(ThaModulePdh self, uint32 glbIntr, AtIpCore ipCore)
    {
    AtUnused(self);
    AtUnused(ipCore);
    return (glbIntr & (cAf6_inten_status_DE3Grp1IntEnable_Mask | cAf6_inten_status_DE3Grp0IntEnable_Mask)) ? cAtTrue : cAtFalse;
    }

static eBool HasDe1Interrupt(ThaModulePdh self, uint32 glbIntr, AtIpCore ipCore)
    {
    AtUnused(self);
    AtUnused(ipCore);
    return (glbIntr & (cAf6_inten_status_DE1Grp1IntEnable_Mask | cAf6_inten_status_DE1Grp0IntEnable_Mask)) ? cAtTrue : cAtFalse;
    }

static uint32 De3InterruptStatusGet(ThaModulePdh self, uint32 glbIntr, AtHal hal)
    {
    uint32 grp0Intr = (glbIntr & cAf6_inten_status_DE3Grp0IntEnable_Mask) >> cAf6_inten_status_DE3Grp0IntEnable_Shift;
    uint32 grp1Intr = (glbIntr & cAf6_inten_status_DE3Grp1IntEnable_Mask) >> cAf6_inten_status_DE3Grp1IntEnable_Shift;
    AtUnused(self);
    AtUnused(hal);
    return grp0Intr | (grp1Intr << 1);
    }

static uint32 De1InterruptStatusGet(ThaModulePdh self, uint32 glbIntr, AtHal hal)
    {
    uint32 grp0Intr = (glbIntr & cAf6_inten_status_DE1Grp0IntEnable_Mask) >> cAf6_inten_status_DE1Grp0IntEnable_Shift;
    uint32 grp1Intr = (glbIntr & cAf6_inten_status_DE1Grp1IntEnable_Mask) >> cAf6_inten_status_DE1Grp1IntEnable_Shift;
    AtUnused(self);
    AtUnused(hal);
    return grp0Intr | (grp1Intr << 1);
    }

static eAtRet De1InterruptEnable(ThaModulePdh self, eBool enable)
    {
    uint32 sliceId, numSlices = ThaModulePdhNumSlices(self);
    uint32 regVal = (enable) ? cBit23_0 : 0x0;

    for (sliceId = 0; sliceId < numSlices; sliceId++)
        {
        uint32 sliceBase = (uint32)ThaModulePdhSliceBase(self, sliceId);
        mModuleHwWrite(self, sliceBase + cAf6Reg_dej1_rx_framer_per_stsvc_intr_en_ctrl, regVal);
        }

    return cAtOk;
    }

static void De3InterruptProcess(ThaModulePdh self, uint32 de3Intr, AtHal hal)
    {
    uint8 slice24, numStsPerSlice;
    const uint8 numSlices = mMethodsGet(self)->NumSlices(self);
    uint32 regBase = ThaModulePdhRxm23e23ChnIntrCfgBase(self);

    numStsPerSlice = ThaModulePdhNumStsInSlice(self);
    for (slice24 = 0; slice24 < numSlices; slice24++)
        {
        if (de3Intr & cAf6_pdh_slice_intr_Mask(slice24))
            {
            const uint32 baseAddress = mMethodsGet(self)->SliceBase(self, slice24);
            const uint32 sliceOffset = (uint32)mMethodsGet(self)->SliceOffset(self, (uint32)slice24);
            uint32 intrStatus = AtHalRead(hal, (baseAddress + cAf6Reg_rxm23e23_chn_intr_stat)); /* Slice interrupt status */
            uint32 intrMask = AtHalRead(hal, (baseAddress + regBase)); /* Slice interrupt mask */
            uint8 de3;

            intrStatus &= intrMask;

            for (de3 = 0; de3 < numStsPerSlice; de3++)
                {
                if (intrStatus & cAf6_pdh_de3_intr_Mask(de3))
                    {
                    AtPdhDe3 de3Channel = ThaModulePdhDe3ChannelFromHwIdGet(self, cAtModulePdh, slice24, de3);
                    uint32 offset = sliceOffset + de3;

                    /* Execute channel interrupt. */
                    AtChannelInterruptProcess((AtChannel)de3Channel, offset);
                    }
                }
            }
        }
    }

static void De1InterruptProcess(ThaModulePdh self, uint32 de1Intr, AtHal hal)
    {
    uint8 slice24, numStsPerSlice;
    const uint8 numSlices = mMethodsGet(self)->NumSlices(self);

    numStsPerSlice = ThaModulePdhNumStsInSlice(self);
    for (slice24 = 0; slice24 < numSlices; slice24++)
        {
        if (de1Intr & cAf6_pdh_slice_intr_Mask(slice24))
            {
            const uint32 baseAddress = mMethodsGet(self)->SliceBase(self, (uint32)slice24);
            const uint32 sliceOffet = (uint32)mMethodsGet(self)->SliceOffset(self, slice24);
            uint32 intrStatus = AtHalRead(hal, (baseAddress + cAf6Reg_dej1_rx_framer_per_stsvc_intr_or_stat)); /* Slice interrupt status */
            uint32 intrMask = AtHalRead(hal, (baseAddress + cAf6Reg_dej1_rx_framer_per_stsvc_intr_en_ctrl)); /* Slice interrupt mask */
            uint8 de3;

            intrStatus &= intrMask;

            for (de3 = 0; de3 < numStsPerSlice; de3++)
                {
                if (intrStatus & cAf6_pdh_de3_intr_Mask(de3))
                    {
                    /* Get DE1 channel OR interrupt. */
                    uint32 intrDe1Status = AtHalRead(hal, (baseAddress + (uint32)cAf6Reg_dej1_rx_framer_per_chn_intr_or_stat(de3)));
                    uint8 de28 = 0;
                    uint8 de2, de1;

                    for (de2 = 0; de2 < 7; de2++)
                        {
                        for (de1 = 0; de1 < 4; de1++)
                            {
                            /* Check for DS1/E1 channel OR interrupt. */
                            if (intrDe1Status & cAf6_pdh_de1_intr_Mask(de28))
                                {
                                AtPdhDe1 de1Channel = ThaModulePdhDe1ChannelFromHwIdGet(self, cAtModulePdh, slice24, de3, de2, de1);
                                uint32 de1Offset = sliceOffet + (uint32)(de3 << 5) + (uint32)(de2 << 2) + de1;

                                AtChannelInterruptProcess((AtChannel)de1Channel, de1Offset);
                                }
                            de28++;
                            }
                        }
                    }
                }
            }
        }
    }

static void InterruptProcess(AtModule self, uint32 glbIntr, AtIpCore ipCore)
    {
    ThaModulePdhInterruptProcess((ThaModulePdh)self, glbIntr, ipCore);
    }

static eBool HasM13(ThaModulePdh self, uint8 partId)
    {
    AtUnused(self);
    AtUnused(partId);
    return cAtTrue;
    }

static uint32 NumberOfDe3sGet(AtModulePdh self)
    {
    AtUnused(self);
    return 48;
    }

static AtPdhDe3 De3Create(AtModulePdh self, uint32 de3Id)
    {
    return Tha60210031PdhDe3New(de3Id, self);
    }

static AtPdhDe1 De2De1Create(AtModulePdh self, AtPdhDe2 de2, uint32 de1Id)
    {
    AtUnused(de2);
    return Tha60210031PdhDe2De1New(de1Id, self);
    }

static AtPdhDe3 VcDe3Create(AtModulePdh self, AtSdhChannel sdhVc)
    {
    AtUnused(self);
    AtUnused(sdhVc);
    /* No actual VC DS3/E3 here. Just let VC3 to get DS3/D3 itself */
    return NULL;
    }

static AtPdhDe1 VcDe1Create(AtModulePdh self, AtSdhChannel sdhVc)
    {
    return Tha60210031PdhVcDe1New((AtSdhVc)sdhVc, self);
    }

static uint32 NumberOfDe3SerialLinesGet(AtModulePdh self)
    {
    AtUnused(self);
    return 48;
    }

static AtPdhSerialLine De3SerialCreate(AtModulePdh self, uint32 serialLineId)
    {
    return Tha60210031PdhSerialLineNew(serialLineId, self);
    }

static eAtRet Ds3LiuModeDefault(AtModulePdh module)
    {
    uint32 i;
    eAtRet ret = cAtOk;

    for (i = 0; i < AtModulePdhNumberOfDe3SerialLinesGet(module); i++)
        {
        AtPdhSerialLine serialLine = AtModulePdhDe3SerialLineGet(module, i);
        ret |= AtPdhSerialLineModeSet(serialLine, cAtPdhDe3SerialLineModeDs3);
        }

    return ret;
    }

static eAtRet LiuFrequencyMonitorSet(AtModulePdh self, AtPdhChannel channel)
    {
    uint32 de3Id;
    if (!Tha60210031ModulePdhHasLiu((ThaModulePdh)self))
        return cAtErrorModeNotSupport;

    de3Id = AtChannelIdGet((AtChannel)channel);
    mModuleHwWrite(self, cLiuPortMonitorRegAddr, de3Id);
    return cAtOk;
    }

static eAtRet LiuFrequencyMonitorGet(AtModulePdh self, tAtModulePdhLiuMonitor *clockMonitor)
    {
    uint32 de3Id;
    if (!Tha60210031ModulePdhHasLiu((ThaModulePdh)self))
        return cAtErrorModeNotSupport;

    de3Id = mModuleHwRead(self, cLiuPortMonitorRegAddr);
    clockMonitor->rxClockRate = mModuleHwRead(self, cLiuRxClockFreqRegAddr);
    clockMonitor->txClockRate = mModuleHwRead(self, cLiuTxClockFreqRegAddr);
    clockMonitor->channel = (AtPdhChannel)AtModulePdhDe3Get(self, de3Id);
    return cAtOk;
    }

static void E13IdConvEnable(ThaModulePdh self)
    {
    uint32 regAddr, regVal;
    uint32 numSlice = ThaModulePdhNumSlices(self);

    regAddr = 0x700000UL;
    regVal  = mModuleHwRead(self, regAddr);
    mFieldIns(&regVal, cBit9, 9, 1);
    mModuleHwWrite(self, regAddr, regVal);

    if (numSlice > 1)
        {
        regAddr = 0x800000UL;
        regVal  = mModuleHwRead(self, regAddr);
        mFieldIns(&regVal, cBit9, 9, 1);
        mModuleHwWrite(self, regAddr, regVal);
        }
    }

static uint32 Rxdestuff_ctrl0_Base(ThaModulePdh self)
    {
    return mMethodsGet(mThis(self))->UpenDestuffCtrl0Base(mThis(self)) + ThaModulePdhPrmBaseAddress((ThaModulePdh)self);
    }

static eAtRet PrmDefaultSet(ThaModulePdh self)
    {
    uint32 regAddr, regVal;

    if (!ThaModulePdhPrmIsSupported(self))
        return cAtOk;

    regAddr = Rxdestuff_ctrl0_Base(self);
    regVal  = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_upen_destuff_ctrl0_fcsmon_, 1);    /* Enable FCS monitor */
    mRegFieldSet(regVal, cAf6_upen_destuff_ctrl0_headermon_en_, 1);
    mRegFieldSet(regVal, cAf6_upen_destuff_ctrl0_cfg_crmon_, 0); /* Disable CR compare */
    mModuleHwWrite(self, regAddr, regVal);

    regAddr = mMethodsGet(mThis(self))->UpenCfgCtrl0Base(mThis(self)) + ThaModulePdhPrmBaseAddress(self);
    regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_upen_cfg_ctrl0_fcsinscfg_, 1); /* Enable FCS insert */
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static void ErrorGeneratorDelete(ThaModulePdh self)
    {
    AtObjectDelete((AtObject)mThis(self)->de1RxErrorGenerator);
    mThis(self)->de1RxErrorGenerator = NULL;
    AtObjectDelete((AtObject)mThis(self)->de1TxErrorGenerator);
    mThis(self)->de1TxErrorGenerator = NULL;
    AtObjectDelete((AtObject)mThis(self)->de3RxErrorGenerator);
    mThis(self)->de3RxErrorGenerator = NULL;
    AtObjectDelete((AtObject)mThis(self)->de3TxErrorGenerator);
    mThis(self)->de3TxErrorGenerator = NULL;
    }

static void ErrorGeneratorDefaultSet(ThaModulePdh self)
    {
    ThaPdhDe1TxErrorGeneratorHwDefaultSet(self);
    ThaPdhDe1RxErrorGeneratorHwDefaultSet(self);
    ThaPdhDe3TxErrorGeneratorHwDefaultSet(self);
    ThaPdhDe3RxErrorGeneratorHwDefaultSet(self);
    ErrorGeneratorDelete(self);
    }

static ThaVersionReader VersionReader(ThaModulePdh self)
    {
    return ThaDeviceVersionReader(AtModuleDeviceGet((AtModule)self));
    }

static eBool DefaultRxLiuPhaseToTollerateLowTemperatureIsSupported(ThaModulePdh self)
    {
    uint32 hwVersion;

    if (!mMethodsGet(mThis(self))->HasLiu(mThis(self)))
        return cAtFalse;

    hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader(self));
    if (hwVersion < ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x6, 0x4644))
        return cAtFalse;

    return cAtTrue;
    }

static void DefaultRxLiuPhaseToTollerateLowTemperature(ThaModulePdh self)
    {
    uint32 address = cAf6Reg_mac_add_47_32_ctrl_Base;
    uint32 value = mModuleHwRead(self, address);
    static const uint32 cPhase = 3;
    mRegFieldSet(value, cAf6_mac_add_47_32_ctrl_Rx_Liu_Phase_, cPhase);
    mModuleHwWrite(self, address, value);
    }

static eBool ShouldWriteDefaultPhaseForLowTemperature(ThaModulePdh self)
    {
    /* As hardware is not expecting this and it should be controlled by hardware.
     * But we still keep this logic here to quickly handle when it is necessary */
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet DefaultSet(ThaModulePdh self)
    {
    E13IdConvEnable(self);
    PrmDefaultSet(self);

    if (ThaDeviceErrorGeneratorIsSupported((ThaDevice)AtModuleDeviceGet((AtModule)self)))
        ErrorGeneratorDefaultSet(self);

    if (DefaultRxLiuPhaseToTollerateLowTemperatureIsSupported(self) && ShouldWriteDefaultPhaseForLowTemperature(self))
        DefaultRxLiuPhaseToTollerateLowTemperature(self);

    return Ds3LiuModeDefault((AtModulePdh)self);
    }

static uint32 De3CounterOffset(ThaModulePdh self, eBool r2c)
    {
    AtUnused(self);
    return (r2c) ? 0 : 32UL;
    }

static AtPdhMdlController MdlControllerCreate(ThaModulePdh self, AtPdhDe3 de3, uint32 mdlType)
    {
    AtUnused(self);
    return Tha60210031PdhMdlControllerNew(de3, mdlType);
    }

static eBool DetectDe3LosByAllZeroPattern(ThaModulePdh self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static eBool DetectDe1LosByAllZeroPattern(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 MdlTxCtrlReg(Tha60210031ModulePdh self, AtPdhDe3 de3)
    {
    uint8 hwIdInSlice, sliceId;
    uint32 regBase = mMethodsGet(self)->UpenCfgMdlBase(self);
    uint32 sliceOffset;

    ThaPdhChannelHwIdGet((AtPdhChannel)de3, cAtModulePdh, &sliceId, &hwIdInSlice);
    sliceOffset = mMethodsGet(self)->MdlSliceOffset(self, sliceId);

    return regBase + hwIdInSlice + sliceOffset + ThaModulePdhMdlBaseAddress((ThaModulePdh)self);
    }

static uint32 MdlRxCfgReg(Tha60210031ModulePdh self, AtPdhDe3 de3)
    {
    uint32 offset = mMethodsGet(self)->UpenRxmdlCfgtypeOffset(self, de3);
    return mMethodsGet(self)->UpenRxmdlCfgtypeBase(self) + offset +  ThaModulePdhMdlBaseAddress((ThaModulePdh)self);
    }

static uint32 MdlRxBufBaseAddress(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return 0x9800UL;
    }

static uint32 MdlRxBufWordOffset(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return 64UL;
    }

static uint32 MdlRxStickyBaseAddress(Tha60210031ModulePdh self, uint32 bufferId)
    {
    AtUnused(self);
    return 0x0A043UL + (bufferId* 2UL);
    }

static uint32 MdlRxStickyOffset(Tha60210031ModulePdh self, AtPdhDe3 de3)
    {
    return Tha60210031RxMdlIdFromChannelGet(self, de3)/32;
    }

static uint32 MdlRxStickyMask(Tha60210031ModulePdh self, AtPdhDe3 de3)
    {
    return (cBit0 << (Tha60210031RxMdlIdFromChannelGet(self, de3)%32));
    }

static uint32 RxMdlIdFromChannelGet(Tha60210031ModulePdh self, AtPdhDe3 de3)
    {
    uint8 sliceId, hwIdInSlice;
    uint32 numbSlice = ThaInterruptManagerNumSlices(ThaModulePdhMdlInterruptManager((ThaModulePdh)self));
    ThaPdhChannelHwIdGet((AtPdhChannel)de3, cAtModulePdh, &sliceId, &hwIdInSlice);
    return (uint32)(hwIdInSlice * numbSlice) + (uint32)sliceId;
    }

static uint32 MdlTxCounterOffset(Tha60210031ModulePdh self, AtPdhDe3 de3)
    {
    uint8 sliceId, hwIdInSlice;
    AtUnused(self);
    ThaPdhChannelHwIdGet((AtPdhChannel)de3, cAtModulePdh, &sliceId, &hwIdInSlice);
    return (uint32) hwIdInSlice + mMethodsGet(self)->MdlSliceOffset(self, sliceId);
    }

static uint32 MdlTxPktOffset(Tha60210031ModulePdh self, uint32 type, eBool r2c)
    {
    AtUnused(self);
    switch (type)
        {
        case cAtPdhMdlControllerTypePathMessage: return (r2c) ? 0x620 : 0x6A0;
        case cAtPdhMdlControllerTypeIdleSignal:  return (r2c) ? 0x600 : 0x680;
        case cAtPdhMdlControllerTypeTestSignal:  return (r2c) ? 0x640 : 0x6C0;
        default:                                 return 0;
        }
    }

static uint32 MdlTxByteOffset(Tha60210031ModulePdh self, uint32 type, eBool r2c)
    {
    return mMethodsGet(self)->MdlTxPktOffset(self, type, r2c) + 0x100UL;
    }

static uint32 MdlRxCounterOffset(Tha60210031ModulePdh self, AtPdhDe3 de3)
    {
    return Tha60210031RxMdlIdFromChannelGet(self, de3);
    }

static uint32 MdlRxPktOffset(Tha60210031ModulePdh self, uint32 type, eBool r2c)
    {
    AtUnused(self);
    switch (type)
        {
        case cAtPdhMdlControllerTypePathMessage: return (r2c) ? 0xB040 : 0xB240;
        case cAtPdhMdlControllerTypeIdleSignal:  return (r2c) ? 0xB000 : 0xB200;
        case cAtPdhMdlControllerTypeTestSignal:  return (r2c) ? 0xB080 : 0xB280;
        default:                                 return 0;
        }
    }

static uint32 MdlRxByteOffset(Tha60210031ModulePdh self, uint32 type, eBool r2c)
    {
    AtUnused(self);
    switch (type)
        {
        case cAtPdhMdlControllerTypePathMessage: return (r2c) ? 0xB440 : 0xB540;
        case cAtPdhMdlControllerTypeIdleSignal:  return (r2c) ? 0xB400 : 0xB500;
        case cAtPdhMdlControllerTypeTestSignal:  return (r2c) ? 0xB480 : 0xB580;
        default:                                 return 0;
        }
    }

static uint32 MdlRxDropOffset(Tha60210031ModulePdh self, uint32 type, eBool r2c)
    {
    AtUnused(self);
    switch (type)
        {
        case cAtPdhMdlControllerTypePathMessage: return (r2c) ? 0xB100 : 0xB300;
        case cAtPdhMdlControllerTypeIdleSignal:  return (r2c) ? 0xB0C0 : 0xB2C0;
        case cAtPdhMdlControllerTypeTestSignal:  return (r2c) ? 0xB140 : 0xB340;
        default:                                 return 0;
        }
    }

static uint32 StartVersionSupportDe3Disabling(ThaModulePdh self)
    {
    return ThaVersionReaderPwVersionBuild(VersionReader(self), 0x15, 0x09, 0x23, 0x31);
    }

static eBool De3TxLiuEnableIsSupported(ThaModulePdh self)
    {
    uint32 hwVersion;

    if (!mMethodsGet(mThis(self))->HasLiu(mThis(self)))
        return cAtFalse;

    hwVersion = ThaVersionReaderHardwareVersion(VersionReader(self));
    if (hwVersion < ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x03, 0x06, 0))
        return cAtFalse;

    hwVersion = ThaVersionReaderPwVersionBuild(VersionReader(self), 0x15, 0x11, 0x4, 0x36);
    return (AtDeviceVersionNumber(AtModuleDeviceGet((AtModule)self)) >= hwVersion) ? cAtTrue : cAtFalse;
    }

static uint32 StartVersionSupportAisDownStream(ThaModulePdh self)
    {
    return ThaVersionReaderPwVersionBuild(VersionReader(self), 0x15, 0x10, 0x14, 0x00);
    }

static eBool HasLiu(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 StartVersionSupportPrm(ThaModulePdh self)
    {
    uint32 hwVersion = ThaVersionReaderHardwareVersion(VersionReader(self));

    if (hwVersion < ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x03, 0x06, 0))
        return cInvalidUint32;

    return ThaVersionReaderPwVersionBuild(VersionReader(self), 0x15, 0x11, 0x4, 0x36);
    }

static eAtModulePdhRet HwPrmCrCompareEnable(AtModulePdh self, eBool enable)
    {
    uint32 regAddress, regValue;

    if (!ThaModulePdhPrmIsSupported((ThaModulePdh)self))
        return cAtErrorModeNotSupport;

    regAddress = Rxdestuff_ctrl0_Base((ThaModulePdh)self);
    regValue = mModuleHwRead(self, regAddress);
    mRegFieldSet(regValue, cAf6_upen_destuff_ctrl0_cfg_crmon_, mBoolToBin(enable));
    mModuleHwWrite(self, regAddress, regValue);

    return cAtOk;
    }

static eAtModulePdhRet PrmCrCompareEnable(AtModulePdh self, eBool enable)
    {
    return Tha60210031ModulePdhPrmCrCompareEnable(self, enable);
    }

static eBool HwPrmCrCompareIsEnabled(AtModulePdh self)
    {
    uint32 regAddress = Rxdestuff_ctrl0_Base((ThaModulePdh)self);
    uint32 regValue = mModuleHwRead(self, regAddress);
    return mRegField(regValue, cAf6_upen_destuff_ctrl0_cfg_crmon_) ? cAtTrue : cAtFalse;
    }

static eBool PrmCrCompareIsEnabled(AtModulePdh self)
    {
    return Tha60210031ModulePdhPrmCrCompareIsEnabled(self);
    }

static uint32 StartVersionSupportDe3Ssm(ThaModulePdh self)
    {
    return ThaVersionReaderPwVersionBuild(VersionReader(self), 0x15, 0x11, 0x19, 0x38);
    }

static uint32 StartVersionSupportDe1Ssm(ThaModulePdh self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x6, 0x4644);
    }

static uint32 StartVersionSupportDe3FramedSatop(ThaModulePdh self)
    {
    AtUnused(self);
    return ThaVersionReaderPwVersionBuild(VersionReader(self), 0x15, 0x12, 0x05, 0x39);
    }

static eBool De3FramedSatopIsSupported(ThaModulePdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    uint32 startSupportedVersion = mMethodsGet(self)->StartVersionSupportDe3FramedSatop(self);
    if (AtDeviceIsSimulated(device))
        return cAtTrue;
    return (AtDeviceVersionNumber(device) >= startSupportedVersion) ? cAtTrue : cAtFalse;
    }

static eBool De3AisSelectablePatternIsSupported(ThaModulePdh self)
    {
    uint32 startSupportedVersion = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x6, 0x4629);
    uint32 version = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader(self));
    return (version >= startSupportedVersion) ? cAtTrue : cAtFalse;
    }

static eBool RxHw3BitFeacSignalIsSupported(ThaModulePdh self)
    {
    uint32 startSupportedVersion = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x6, 0x4644);
    uint32 version = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader(self));
    return (version >= startSupportedVersion) ? cAtTrue : cAtFalse;
    }

static AtPdhPrmController PrmControllerObjectCreate(ThaModulePdh self, AtPdhDe1 de1)
    {
    AtUnused(self);
    return Tha60210031PdhPrmControllerNew(de1);
    }

static ThaPdhDebugger DebuggerObjectCreate(ThaModulePdh self)
    {
    return Tha60210031PdhDebuggerNew(self);
    }

static uint32 StartVersionSupportLiuDe3OverVc3(AtDevice device)
    {
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x15, 0x12, 0x02, 0x39);
    }

static uint32 StartBuiltNumberSupportLiuDe3OverVc3(AtDevice device)
    {
    AtUnused(device);
    return ThaVersionReaderHardwareBuiltNumberBuild(0x0, 0x0, 0x4);
    }

static eBool LiuDe3OverVc3IsSupported(ThaStmModulePdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    
    if (AtDeviceAllFeaturesAvailableInSimulation(device))
        return cAtTrue;

    if (AtDeviceVersionNumber(device) > StartVersionSupportLiuDe3OverVc3(device))
        return cAtTrue;

    if (AtDeviceVersionNumber(device) == StartVersionSupportLiuDe3OverVc3(device))
        return (AtDeviceBuiltNumber(device) >= StartBuiltNumberSupportLiuDe3OverVc3(device)) ? cAtTrue : cAtFalse;

    return cAtFalse;
    }

static uint32 StartVersionSupportMdlFilterMessageByType(ThaModulePdh self)
    {
    return ThaVersionReaderPwVersionBuild(VersionReader(self), 0x15, 0x11, 0x16, 0x00);
    }

static uint32 StartHardwareVersionSupportsDataLinkFcsLsbBitOrder(ThaModulePdh self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x6, 0x4609);
    }

static uint32 StartDateSupportForcedAisCorrection(ThaModulePdh self)
    {
    return ThaVersionReaderPwVersionBuild(VersionReader(self), 0x15, 0x12, 0x02, 0x36);
    }

static uint32 StartBuiltNumberSupportForcedAisCorrection(ThaModulePdh self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareBuiltNumberBuild(0x3, 0x6, 0x19);
    }

static eBool LiuForcedAisIsSupported(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool ForcedAisCorrectionIsSupported(ThaModulePdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);

    if (AtDeviceVersionNumber(device) > StartDateSupportForcedAisCorrection(self))
        return cAtTrue;

    if (AtDeviceVersionNumber(device) == StartDateSupportForcedAisCorrection(self))
        return (AtDeviceBuiltNumber(device) >= StartBuiltNumberSupportForcedAisCorrection(self)) ? cAtTrue : cAtFalse;

    return cAtFalse;
    }

static uint32 StartVersionSupportAlarmForwardingStatus(ThaModulePdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x16, 0x01, 0x27, 0x44);
    }

static AtErrorGenerator De1TxErrorGeneratorGet(ThaModulePdh self)
    {
    if (!ThaDeviceErrorGeneratorIsSupported((ThaDevice)AtModuleDeviceGet((AtModule)self)))
        return NULL;

    if (mThis(self)->de1TxErrorGenerator == NULL)
        mThis(self)->de1TxErrorGenerator = ThaPdhDe1TxErrorGeneratorNew(NULL);

    return mThis(self)->de1TxErrorGenerator;
    }

static AtErrorGenerator De1RxErrorGeneratorGet(ThaModulePdh self)
    {
    if (!ThaDeviceErrorGeneratorIsSupported((ThaDevice)AtModuleDeviceGet((AtModule)self)))
        return NULL;

    if (mThis(self)->de1RxErrorGenerator == NULL)
        mThis(self)->de1RxErrorGenerator = ThaPdhDe1RxErrorGeneratorNew(NULL);

    return mThis(self)->de1RxErrorGenerator;
    }

static AtErrorGenerator De3TxErrorGeneratorGet(ThaModulePdh self)
    {
    if (!ThaDeviceErrorGeneratorIsSupported((ThaDevice)AtModuleDeviceGet((AtModule)self)))
        return NULL;

    if (mThis(self)->de3TxErrorGenerator == NULL)
        mThis(self)->de3TxErrorGenerator = ThaPdhDe3TxErrorGeneratorNew(NULL);

    return mThis(self)->de3TxErrorGenerator;
    }

static AtErrorGenerator De3RxErrorGeneratorGet(ThaModulePdh self)
    {
    if (!ThaDeviceErrorGeneratorIsSupported((ThaDevice)AtModuleDeviceGet((AtModule)self)))
        return NULL;

    if (mThis(self)->de3RxErrorGenerator == NULL)
        mThis(self)->de3RxErrorGenerator = ThaPdhDe3RxDiagErrorGeneratorNew(NULL);

    return mThis(self)->de3RxErrorGenerator;
    }

static uint32 ErrorGeneratorBerDivCoefficient(ThaModulePdh self)
    {
    AtUnused(self);
    return 8;
    }

static void Delete(AtObject self)
    {
    ErrorGeneratorDelete((ThaModulePdh)self);
    AtOsalMutexDestroy(mThis(self)->mdlMutex);
    mThis(self)->mdlMutex = NULL;

    m_AtObjectMethods->Delete(self);
    }

static uint32 StartVersionFixDs3AisInterrupt(ThaModulePdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x16, 0x03, 0x22, 0x45);
    }

static eBool DatalinkFcsBitOrderIsSupported(ThaModulePdh self)
    {
    ThaVersionReader versionReader = VersionReader(self);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    uint32 startHwVersionSupports = mMethodsGet(mThis(self))->StartHardwareVersionSupportsDataLinkFcsLsbBitOrder(self);

    if (hwVersion >= startHwVersionSupports)
        return cAtTrue;

    return cAtFalse;
    }

static uint32 PrmStsAndTug2Offset(ThaModulePdh self)
    {
    AtUnused(self);
    return 64UL;
    }

static uint32 De1PrmOffset(ThaModulePdh self, ThaPdhDe1 de1)
    {
    uint8 hwId, sliceId, tugOrDe2Id, tu1xOrDe1Id;
    uint32 hwStsLineOffset, hwStsLowOffset, offset;
    uint32 baseAddress;
    uint8 hwStsMsb, hwStsLsb;

    AtUnused(self);
    /*((STSID[4:3]*7 + VTGID)*64) + (sliceID*32 + STSID[2:0]*4 + VTID)*/
    if (ThaPdhChannelHwIdGet((AtPdhChannel)de1, cAtModulePdh, &sliceId, &hwId) != cAtOk)
        return cBit31_0;

    hwStsMsb = (uint8)((hwId & cBit4_3) >> 3);
    hwStsLsb = (uint8)(hwId & cBit2_0);
    hwStsLineOffset = (uint8)(hwStsLsb * 4);
    hwStsLowOffset = (uint8)(hwStsMsb * 7);
    tu1xOrDe1Id = Tha60210031Tu1xOrDe1IdGet(de1, &tugOrDe2Id);
    offset = (uint32)((hwStsLowOffset + tugOrDe2Id) * ThaModulePdhPrmStsAndTug2Offset(self)) + ((uint32)(sliceId * 32) + hwStsLineOffset + tu1xOrDe1Id);
    baseAddress = ThaModulePdhPrmBaseAddress((ThaModulePdh)AtChannelModuleGet((AtChannel)de1));
    return (offset + baseAddress);
    }

static eBool JitterAttenuatorIsSupported(Tha60150011ModulePdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool ShouldDetectAisAllOnesWithUnframedMode(ThaModulePdh self)
    {
    return Tha602100xxModulePdhShouldDetectAisAllOnesWithUnframedMode(self);
    }

static uint8 NumStsInSlice(ThaModulePdh self)
    {
    AtUnused(self);
    return 24;
    }

static eBool Ec1DiagLiuModeShouldEnable(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool Ec1LiuRxClockInvertionShouldEnable(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool PrmFcsBitOrderIsSupported(ThaModulePdh self)
    {
    return ThaModulePdhPrmIsSupported((ThaModulePdh)self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60210031ModulePdh object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObject(de3TxErrorGenerator);
    mEncodeObject(de3RxErrorGenerator);
    mEncodeObject(de1TxErrorGenerator);
    mEncodeObject(de1RxErrorGenerator);
    mEncodeNone(mdlMutex);
    }

static uint32 TxBufferBaseGet(Tha60210031ModulePdh self, AtPdhDe3 de3)
    {
    uint8 hwIdInSlice, sliceId;
    uint32 addressBase;
    uint32 offset;

    ThaPdhChannelHwIdGet((AtPdhChannel)de3, cAtModulePdh, &sliceId, &hwIdInSlice);
    addressBase = mMethodsGet(self)->UpenMdlBuffer1Base(self, hwIdInSlice);
    offset = mMethodsGet(self)->UpenMdlBuffer1Offset(self, sliceId, hwIdInSlice);

    return offset + addressBase + ThaModulePdhMdlBaseAddress((ThaModulePdh)self);
    }

static uint32 TxBufferDwordOffset(Tha60210031ModulePdh self, uint8 sliceId, uint8 hwIdInSlice)
    {
    AtUnused(self);
    AtUnused(sliceId);
    if (hwIdInSlice > 15)
        return 8;
    return 16;
    }

static eBool De1LiuShouldReportLofWhenLos(ThaModulePdh self)
    {
    return Tha602100xxModulePdhDe1LiuShouldReportLofWhenLos(self);
    }

static eBool De3LiuShouldReportLofWhenLos(ThaModulePdh self)
    {
    return Tha602100xxModulePdhDe3LiuShouldReportLofWhenLos(self);
    }

static uint32 RxDe3OhThrshCfgBase(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return mMakeRegBase(cAf6Reg_rxde3_oh_thrsh_cfg_Base);
    }

static uint32 Rxm23e23ChnIntrCfgBase(ThaModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_rxm23e23_chn_intr_cfg;
    }

static eBool CanHotChangeFrameType(ThaModulePdh self)
    {
    AtUnused(self);

    /* Frame type can be hot changed to reduce register
     * accesses during add/remove Pseudowire */
    return cAtTrue;
    }

static uint32 StartVersionSupportDe1LomfConsequentialAction(ThaModulePdh self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x04, 0x06, 0x4647);
    }

static uint32 MdlSliceOffset(Tha60210031ModulePdh self, uint8 sliceId)
    {
    AtUnused(self);
    return (uint32)sliceId * cAf6RegMdlTxOc24SliceOffset;
    }

static uint32 MdlTxInsCtrlBase(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return cAf6RegCfgMdlTxInsCtrlBase;
    }

static uint32 UpenCfgMdlBase(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_upen_cfg_mdl_Base;
    }

static uint32 UpenRxmdlCfgtypeBase(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_upen_rxmdl_cfgtype_Base;
    }

static uint32 UpenDestuffCtrl0Base(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_upen_destuff_ctrl0_Base;
    }

static uint32 MdlControllerSliceOffset(Tha60210031ModulePdh self, uint8 sliceId)
    {
    AtUnused(self);
    return sliceId * 32UL;
    }

static uint32 UpenRxmdlCfgtypeOffset(Tha60210031ModulePdh self, AtPdhDe3 de3)
    {
    return Tha60210031RxMdlIdFromChannelGet(self, de3);
    }

static uint32 UpenCfgCtrl0Base(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_upen_cfg_ctrl0_Base;
    }

static uint32 UpenMdlBuffer1Base(Tha60210031ModulePdh self, uint8 hwStsId)
    {
    AtUnused(self);
    return (hwStsId > 15) ? 0x0130 : 0;
    }

static uint32 UpenMdlBuffer1Offset(Tha60210031ModulePdh self, uint8 sliceId, uint8 hwIdInSlice)
    {
    return (hwIdInSlice % 16) + mMethodsGet(self)->MdlSliceOffset(self, sliceId);
    }

static uint32 UpenPrmTxcfgcrBase(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_upen_prm_txcfgcr_Base;
    }

static uint32 UpenPrmTxcfglbBase(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_upen_prm_txcfglb_Base;
    }

static uint32 LBOffset(Tha60210031ModulePdh self, ThaPdhDe1 de1)
    {
    uint8 hwSliceId, hwStsId;
    uint8 tugOrDe2Id, tu1xOrDe1Id;
    uint32 offset, baseAddress;

    AtUnused(self);

    /* $SLICEID*1024 + $STSID*32 + $VTGID*4 + $VTID */
    if (ThaPdhChannelHwIdGet((AtPdhChannel)de1, cAtModulePdh, &hwSliceId, &hwStsId) != cAtOk)
        return cBit31_0;

    tu1xOrDe1Id = Tha60210031Tu1xOrDe1IdGet(de1, &tugOrDe2Id);
    offset = (uint32)(hwSliceId << 10) + (uint32)(hwStsId << 5) + (uint32)(tugOrDe2Id << 2) + tu1xOrDe1Id;
    baseAddress = ThaModulePdhPrmBaseAddress((ThaModulePdh)AtChannelModuleGet((AtChannel)de1));
    return (offset + baseAddress);
    }

static uint32 UpenRxprmCfgstdBase(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_upen_rxprm_cfgstd_Base;
    }

static uint32 NumInterruptManagers(AtModule self)
    {
    AtUnused(self);
    return 2;
    }

static uint32 DefaultInbandLoopcodeThreshold(ThaModulePdh self)
    {
    static uint32 cPdhSlice24CoreClockInHz = 155520000; /* Hz or clocks per second */
    uint32 numClocksIn5s = (3 * cPdhSlice24CoreClockInHz);
    AtUnused(self);
    return numClocksIn5s;
    }

static AtPdhChannel VcDe3ObjectGet(ThaModulePdh self, AtSdhChannel vc3)
    {
    uint32 lineId = AtSdhChannelLineGet(vc3);
    return (AtPdhChannel)AtModulePdhDe3Get((AtModulePdh)self, lineId);
    }

static void MethodsInit(AtModulePdh self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, HasLiu);
        mMethodOverride(m_methods, MdlTxCtrlReg);
        mMethodOverride(m_methods, MdlRxCfgReg);
        mMethodOverride(m_methods, MdlRxBufBaseAddress);
        mMethodOverride(m_methods, MdlRxBufWordOffset);
        mMethodOverride(m_methods, MdlRxStickyBaseAddress);
        mMethodOverride(m_methods, MdlRxStickyOffset);
        mMethodOverride(m_methods, MdlRxStickyMask);
        mMethodOverride(m_methods, RxMdlIdFromChannelGet);
        mMethodOverride(m_methods, MdlTxCounterOffset);
        mMethodOverride(m_methods, MdlTxPktOffset);
        mMethodOverride(m_methods, MdlTxByteOffset);
        mMethodOverride(m_methods, MdlRxCounterOffset);
        mMethodOverride(m_methods, MdlRxPktOffset);
        mMethodOverride(m_methods, MdlRxByteOffset);
        mMethodOverride(m_methods, MdlRxDropOffset);
        mMethodOverride(m_methods, StartVersionSupportMdlFilterMessageByType);
        mMethodOverride(m_methods, StartHardwareVersionSupportsDataLinkFcsLsbBitOrder);
        mMethodOverride(m_methods, DatalinkFcsBitOrderIsSupported);
        mMethodOverride(m_methods, Ec1DiagLiuModeShouldEnable);
        mMethodOverride(m_methods, Ec1LiuRxClockInvertionShouldEnable);
        mMethodOverride(m_methods, TxBufferBaseGet);
        mMethodOverride(m_methods, TxBufferDwordOffset);
        mMethodOverride(m_methods, RxDe3OhThrshCfgBase);
        mMethodOverride(m_methods, SerialIdFromHwIdGet);
        mMethodOverride(m_methods, UpenMdlBuffer1Base);
        mMethodOverride(m_methods, UpenMdlBuffer1Offset);
        mMethodOverride(m_methods, MdlSliceOffset);
        mMethodOverride(m_methods, MdlTxInsCtrlBase);
        mMethodOverride(m_methods, UpenCfgMdlBase);
        mMethodOverride(m_methods, UpenRxmdlCfgtypeBase);
        mMethodOverride(m_methods, UpenDestuffCtrl0Base);
        mMethodOverride(m_methods, UpenCfgCtrl0Base);
        mMethodOverride(m_methods, MdlControllerSliceOffset);
        mMethodOverride(m_methods, UpenRxmdlCfgtypeOffset);
        mMethodOverride(m_methods, UpenPrmTxcfgcrBase);
        mMethodOverride(m_methods, UpenPrmTxcfglbBase);
        mMethodOverride(m_methods, UpenRxprmCfgstdBase);
        mMethodOverride(m_methods, LBOffset);
        }

    mMethodsSet(mThis(self), &m_methods);
    }

static void OverrideAtObject(AtModulePdh self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModulePdh self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, InterruptProcess);
        mMethodOverride(m_AtModuleOverride, NumInterruptManagers);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtModulePdh(AtModulePdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModulePdhMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePdhOverride, m_AtModulePdhMethods, sizeof(m_AtModulePdhOverride));

        mMethodOverride(m_AtModulePdhOverride, NumberOfDe3sGet);
        mMethodOverride(m_AtModulePdhOverride, De3Create);
        mMethodOverride(m_AtModulePdhOverride, NumberOfDe3SerialLinesGet);
        mMethodOverride(m_AtModulePdhOverride, De3SerialCreate);
        mMethodOverride(m_AtModulePdhOverride, VcDe3Create);
        mMethodOverride(m_AtModulePdhOverride, De2De1Create);
        mMethodOverride(m_AtModulePdhOverride, VcDe1Create);
        mMethodOverride(m_AtModulePdhOverride, PrmCrCompareEnable);
        mMethodOverride(m_AtModulePdhOverride, PrmCrCompareIsEnabled);
        mMethodOverride(m_AtModulePdhOverride, LiuFrequencyMonitorSet);
        mMethodOverride(m_AtModulePdhOverride, LiuFrequencyMonitorGet);
        }

    mMethodsSet(self, &m_AtModulePdhOverride);
    }

static void OverrideThaModulePdh(AtModulePdh self)
    {
    ThaModulePdh module = (ThaModulePdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePdhMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdhOverride, m_ThaModulePdhMethods, sizeof(m_ThaModulePdhOverride));

        mMethodOverride(m_ThaModulePdhOverride, SliceBase);
        mMethodOverride(m_ThaModulePdhOverride, HasM13);
        mMethodOverride(m_ThaModulePdhOverride, InbandLoopcodeIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, CasSupported);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportBom);
        mMethodOverride(m_ThaModulePdhOverride, DefaultSet);
        mMethodOverride(m_ThaModulePdhOverride, De3CounterOffset);
        mMethodOverride(m_ThaModulePdhOverride, MdlControllerCreate);
        mMethodOverride(m_ThaModulePdhOverride, De3ChannelFromHwIdGet);
        mMethodOverride(m_ThaModulePdhOverride, De1ChannelFromHwIdGet);
        mMethodOverride(m_ThaModulePdhOverride, HasDe3Interrupt);
        mMethodOverride(m_ThaModulePdhOverride, HasDe1Interrupt);
        mMethodOverride(m_ThaModulePdhOverride, De3InterruptStatusGet);
        mMethodOverride(m_ThaModulePdhOverride, De1InterruptStatusGet);
        mMethodOverride(m_ThaModulePdhOverride, De1InterruptEnable);
        mMethodOverride(m_ThaModulePdhOverride, De3InterruptProcess);
        mMethodOverride(m_ThaModulePdhOverride, De1InterruptProcess);
        mMethodOverride(m_ThaModulePdhOverride, MdlIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, DetectDe1LosByAllZeroPattern);
        mMethodOverride(m_ThaModulePdhOverride, DetectDe3LosByAllZeroPattern);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportDe3Disabling);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportPrm);
        mMethodOverride(m_ThaModulePdhOverride, PrmFcsBitOrderIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, De3TxLiuEnableIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportAisDownStream);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportDe3Ssm);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportDe1Ssm);
        mMethodOverride(m_ThaModulePdhOverride, PrmControllerObjectCreate);
        mMethodOverride(m_ThaModulePdhOverride, DebuggerObjectCreate);
        mMethodOverride(m_ThaModulePdhOverride, LiuForcedAisIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, ForcedAisCorrectionIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportDe3FramedSatop);
        mMethodOverride(m_ThaModulePdhOverride, De3FramedSatopIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, De3AisSelectablePatternIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportAlarmForwardingStatus);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionFixDs3AisInterrupt);
        mMethodOverride(m_ThaModulePdhOverride, De1TxErrorGeneratorGet);
        mMethodOverride(m_ThaModulePdhOverride, De1RxErrorGeneratorGet);
        mMethodOverride(m_ThaModulePdhOverride, De3TxErrorGeneratorGet);
        mMethodOverride(m_ThaModulePdhOverride, De3RxErrorGeneratorGet);
        mMethodOverride(m_ThaModulePdhOverride, ErrorGeneratorBerDivCoefficient);
        mMethodOverride(m_ThaModulePdhOverride, De1PrmOffset);
        mMethodOverride(m_ThaModulePdhOverride, PrmStsAndTug2Offset);
        mMethodOverride(m_ThaModulePdhOverride, ShouldDetectAisAllOnesWithUnframedMode);
        mMethodOverride(m_ThaModulePdhOverride, De1LiuShouldReportLofWhenLos);
        mMethodOverride(m_ThaModulePdhOverride, De3LiuShouldReportLofWhenLos);
        mMethodOverride(m_ThaModulePdhOverride, Rxm23e23ChnIntrCfgBase);
        mMethodOverride(m_ThaModulePdhOverride, NumStsInSlice);
        mMethodOverride(m_ThaModulePdhOverride, RxHw3BitFeacSignalIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, CanHotChangeFrameType);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportDe1LomfConsequentialAction);
        mMethodOverride(m_ThaModulePdhOverride, DefaultInbandLoopcodeThreshold);
        mMethodOverride(m_ThaModulePdhOverride, VcDe3ObjectGet);
        }

    mMethodsSet(module, &m_ThaModulePdhOverride);
    }

static void OverrideThaStmModulePdh(AtModulePdh self)
    {
    ThaStmModulePdh module = (ThaStmModulePdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaStmModulePdhOverride, mMethodsGet(module), sizeof(m_ThaStmModulePdhOverride));

        mMethodOverride(m_ThaStmModulePdhOverride, LiuDe3OverVc3IsSupported);
        }

    mMethodsSet(module, &m_ThaStmModulePdhOverride);
    }

static void OverrideTha60150011ModulePdh(AtModulePdh self)
    {
    Tha60150011ModulePdh module = (Tha60150011ModulePdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60150011ModulePdhOverride, mMethodsGet(self), sizeof(m_Tha60150011ModulePdhOverride));

        mMethodOverride(m_Tha60150011ModulePdhOverride, JitterAttenuatorIsSupported);
        }

    mMethodsSet(module, &m_Tha60150011ModulePdhOverride);
    }

static void Override(AtModulePdh self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    OverrideAtModulePdh(self);
    OverrideThaModulePdh(self);
    OverrideThaStmModulePdh(self);
    OverrideTha60150011ModulePdh(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031ModulePdh);
    }

AtModulePdh Tha60210031ModulePdhObjectInit(AtModulePdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60150011ModulePdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->mdlMutex = mMethodsGet(osal)->MutexCreate(osal);

    return self;
    }

AtModulePdh Tha60210031ModulePdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031ModulePdhObjectInit(newModule, device);
    }

eBool Tha60210031ModulePdhHasLiu(ThaModulePdh self)
    {
    if (self)
        return mMethodsGet(mThis(self))->HasLiu(mThis(self));
    return cAtFalse;
    }

uint32 Tha60210031RxMdlIdFromChannelGet(Tha60210031ModulePdh self, AtPdhDe3 de3)
    {
    if (self)
        return mMethodsGet(mThis(self))->RxMdlIdFromChannelGet(self, de3);
    return cBit31_0;
    }

eAtRet Tha60210031PdhStsDe3LiuOverVc3CepSet(AtModulePdh self, AtSdhChannel sdhVc)
    {
    uint32 regVal, address, offset;
    uint8 stsId;
    ThaStmModulePdh pdhModule = (ThaStmModulePdh) self;

    if (pdhModule == NULL)
        return cAtErrorNullPointer;

    if (!ThaStmModulePdhLiuDe3OverVc3IsSupported((ThaStmModulePdh)self))
        return cAtErrorModeNotSupport;

    stsId = AtSdhChannelSts1Get(sdhVc);
    offset = mMethodsGet(pdhModule)->DefaultOffset(pdhModule, sdhVc, stsId, cThaPdhNotCare, cThaPdhNotCare);
    address = cThaRegStsVtMapCtrl + offset;
    regVal = mModuleHwRead(pdhModule, address);
    mRegFieldSet(regVal, cThaStsVtMapJitCfg, ThaModulePdhDefaultDe3JitterStuffingMode((ThaModulePdh)self));
    mRegFieldSet(regVal, cThaStsVtMapAismask, 1); /* Disable AIS*/
    mModuleHwWrite(pdhModule, address, regVal);

    return cAtOk;
    }

uint32 Tha60210031ModulePdhStartVersionSupportMdlFilterMessageByType(ThaModulePdh self)
    {
    if (self)
        return mMethodsGet(mThis(self))->StartVersionSupportMdlFilterMessageByType(self);

    return cBit31_0;
    }

void Tha60210031ModulePdhMdlLock(AtModulePdh self)
    {
    if (self)
        AtOsalMutexLock(mThis(self)->mdlMutex);
    }

void Tha60210031ModulePdhMdlUnLock(AtModulePdh self)
    {
    if (self)
        AtOsalMutexUnLock(mThis(self)->mdlMutex);
    }

eBool Tha60210031ModulePdhDatalinkFcsBitOrderIsSupported(ThaModulePdh self)
    {
    if (self)
        return mMethodsGet(mThis(self))->DatalinkFcsBitOrderIsSupported(self);
    return cAtFalse;
    }

eAtModulePdhRet Tha60210031ModulePdhPrmCrCompareEnable(AtModulePdh self, eBool enable)
    {
    if (self)
        return HwPrmCrCompareEnable(self, enable);
    return cAtErrorNullPointer;
    }

eBool Tha60210031ModulePdhPrmCrCompareIsEnabled(AtModulePdh self)
    {
    if (self)
        return HwPrmCrCompareIsEnabled(self);
    return cAtFalse;
    }

eAtModulePdhRet Tha60210031ModulePdhPrmDefaultSet(ThaModulePdh self)
    {
    if (self)
        return PrmDefaultSet(self);
    return cAtErrorNullPointer;
    }

eBool Tha60210031ModulePdhEc1DiagLiuModeShouldEnable(Tha60210031ModulePdh self)
    {
    if (self)
        return mMethodsGet(self)->Ec1DiagLiuModeShouldEnable(self);
    return cAtFalse;
    }

eBool Tha60210031ModulePdhEc1LiuRxClockInvertionShouldEnable(Tha60210031ModulePdh self)
    {
    if (self)
        return mMethodsGet(self)->Ec1LiuRxClockInvertionShouldEnable(self);
    return cAtFalse;
    }

uint32 Tha60210031ModulePdhRxDe3OhThrshCfgAddress(Tha60210031ModulePdh self)
    {
    if (self)
        return mMethodsGet(mThis(self))->RxDe3OhThrshCfgBase(self) + cThaModulePdhOriginalBaseAddress;
    return cInvalidUint32;
    }

AtPdhDe3 Tha6021ModulePdhDe3FromSerialIdGet(ThaModulePdh self, uint32 serialId)
    {
    if (self)
        return De3FromSerialIdGet(self, serialId);
    return NULL;
    }

AtPdhDe1 Tha6021ModulePdhDe1FromSerialIdGet(ThaModulePdh self, uint32 serialId, uint8 de2Id, uint8 de1Id)
    {
    if (self)
        return De1FromSerialIdGet(self, serialId, de2Id, de1Id);
    return NULL;
    }

uint32 Tha6021ModulePdhSerialIdFromHwIdGet(ThaModulePdh self, uint8 sliceId, uint8 de3InSlice)
    {
    if (self)
        return mMethodsGet(mThis(self))->SerialIdFromHwIdGet(mThis(self), sliceId, de3InSlice);
    return cInvalidUint32;
    }

uint32 Tha60210031ModulePdhMdlSliceOffset(Tha60210031ModulePdh self, uint8 sliceId)
    {
    if (self)
        return mMethodsGet(self)->MdlSliceOffset(self, sliceId);
    return cInvalidUint32;
    }

uint32 Tha60210031ModulePdhMdlTxInsCtrlBase(Tha60210031ModulePdh self)
    {
    if (self)
        return mMethodsGet(self)->MdlTxInsCtrlBase(self);
    return cInvalidUint32;
    }

uint32 Tha60210031ModulePdhMdlControllerSliceOffset(Tha60210031ModulePdh self, uint8 sliceId)
    {
    if (self)
        return mMethodsGet(self)->MdlControllerSliceOffset(self, sliceId);
    return cInvalidUint32;
    }

uint32 Tha60210031ModulePdhUpenPrmTxcfgcrBase(Tha60210031ModulePdh self)
    {
    if (self)
        return mMethodsGet(self)->UpenPrmTxcfgcrBase(self);
    return cInvalidUint32;
    }

uint32 Tha60210031ModulePdhUpenPrmTxcfglbBase(Tha60210031ModulePdh self)
    {
    if (self)
        return mMethodsGet(self)->UpenPrmTxcfglbBase(self);
    return cInvalidUint32;
    }

uint32 Tha60210031ModulePdhLBOffset(Tha60210031ModulePdh self, ThaPdhDe1 de1)
    {
    if (self)
        return mMethodsGet(self)->LBOffset(self, de1);
    return cInvalidUint32;
    }

uint32 Tha60210031ModulePdhUpenRxprmCfgstdBase(Tha60210031ModulePdh self)
    {
    if (self)
        return mMethodsGet(self)->UpenRxprmCfgstdBase(self);
    return cInvalidUint32;
    }

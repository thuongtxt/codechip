/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60210031PdhVcDe1.c
 *
 * Created Date: June 16, 2015
 *
 * Description : DE1 inside a DE2
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210031PdhDe1.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210031PdhVcDe1)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
#include "Tha60210031PdhDe1Common.h"

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031PdhVcDe1);
    }

AtPdhDe1 Tha60210031PdhVcDe1ObjectInit(AtPdhDe1 self, AtSdhVc vc1x, AtModulePdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Supper constructor */
    if (ThaPdhVcDe1ObjectInit(self, vc1x, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    return self;
    }

AtPdhDe1 Tha60210031PdhVcDe1New(AtSdhVc vc1x, AtModulePdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhDe1 newDe1 = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDe1 == NULL)
        return NULL;

    /* construct it */
    return Tha60210031PdhVcDe1ObjectInit(newDe1, vc1x, module);
    }

uint32 Tha6021PdhDe1SupportedInterruptMasks(AtChannel self)
    {
    AtUnused(self);
    return (cAtPdhDe1AlarmLos |
            cAtPdhDe1AlarmLof |
            cAtPdhDe1AlarmAis |
            cAtPdhDe1AlarmRai |
            cAtPdhDe1AlarmLomf|
            cAtPdhDe1AlarmSigLof   |
            cAtPdhDe1AlarmSigRai   |
            cAtPdhDe1AlarmEbit     |
            cAtPdhDs1AlarmInbandLoopCodeChange|
            cAtPdhE1AlarmSsmChange |
            cAtPdhDs1AlarmBomChange|
            cAtPdhDe1AlarmSdBer    |
            cAtPdhDe1AlarmSfBer    |
            cAtPdhDe1AlarmBerTca   |
            cAtPdhDs1AlarmPrmLBBitChange |
            cAtPdhDe1AlarmClockStateChange);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60210031PdhPrmController.c
 *
 * Created Date: Oct 29, 2015
 *
 * Description : PRM controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/man/AtModuleInternal.h"
#include "../../../default/man/versionreader/ThaVersionReader.h"
#include "../../../default/pdh/ThaPdhPrmControllerInternal.h"
#include "../../../default/pdh/ThaModulePdhReg.h"
#include "../../Tha60210021/pdh/Tha60210021ModulePdhPrm.h"
#include "Tha60210031ModulePdh.h"
#include "Tha60210031PdhMdlControllerReg.h"
#include "Tha60210031PdhDe1.h"
#include "Tha60210031PdhPrmControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtPdhPrmControllerMethods  m_AtPdhPrmControllerOverride;
static tThaPdhPrmControllerMethods m_ThaPdhPrmControllerOverride;

/* Save super implementation */
static const tAtPdhPrmControllerMethods *m_AtPdhPrmControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaPdhDe1 De1Get(AtPdhPrmController self)
    {
    return (ThaPdhDe1)AtPdhPrmControllerDe1Get(self);
    }

static uint32 PrmOffset(AtPdhPrmController self)
    {
    ThaPdhDe1 de1 = De1Get(self);
    ThaModulePdh modulePdh = (ThaModulePdh) AtChannelModuleGet((AtChannel) de1);

    return ThaModulePdhDe1PrmOffset(modulePdh, de1);
    }

static eBool IsEnabled(AtPdhPrmController self)
    {
    ThaPdhDe1 de1 = De1Get(self);
    uint32 regAddress = cAf6Reg_upen_prm_txcfgcr_Base + PrmOffset(self);
    uint32 regValue = mChannelHwRead(de1, regAddress, cAtModulePdh);

    return mRegField(regValue, cAf6_upen_prm_txcfgcr_cfg_prmen_tx_) ? cAtTrue : cAtFalse;
    }

static eAtPdhDe1PrmStandard TxStandardGet(AtPdhPrmController self)
    {
    ThaPdhDe1 de1 = De1Get(self);
    uint32 regAddr = cAf6Reg_upen_prm_txcfgcr_Base + PrmOffset(self);
    uint32 regVal = mChannelHwRead(de1, regAddr, cAtModulePdh);

    return mRegField(regVal, cAf6_upen_prm_txcfgcr_cfg_prmstd_tx_) ? cAtPdhDe1PrmStandardAtt : cAtPdhDe1PrmStandardAnsi;
    }

static eAtPdhDe1PrmStandard RxStandardGet(AtPdhPrmController self)
    {
    ThaPdhDe1 de1 = De1Get(self);
    uint32 regAddr = cAf6Reg_upen_rxprm_cfgstd_Base + PrmOffset(self);
    uint32 regVal = mChannelHwRead(de1, regAddr, cAtModulePdh);

    return mRegField(regVal, cAf6_upen_rxprm_cfgstd_cfg_std_prm_) ? cAtPdhDe1PrmStandardAtt : cAtPdhDe1PrmStandardAnsi;
    }

static uint32 TxMessageRegister(ThaPdhPrmController self)
    {
    AtUnused(self);
    return cAf6Reg_upen_txprm_cnt_pkt_Base;
    }

static uint32 TxByteRegister(ThaPdhPrmController self)
    {
    AtUnused(self);
    return cAf6Reg_upen_txprm_cnt_byte_Base;
    }

static uint32 RxGoodMessageRegister(ThaPdhPrmController self)
    {
    AtUnused(self);
    return cAf6Reg_upen_rxprm_gmess_Base;
    }

static uint32 RxByteRegister(ThaPdhPrmController self)
    {
    AtUnused(self);
    return cAf6Reg_upen_rxprm_cnt_byte_Base;
    }

static uint32 RxDiscardRegister(ThaPdhPrmController self)
    {
    AtUnused(self);
    return cAf6Reg_upen_rxprm_drmess_Base;
    }

static uint32 RxMissRegister(ThaPdhPrmController self)
    {
    AtUnused(self);
    return cAf6Reg_upen_rxprm_mmess_Base;
    }

static uint8 TxCRBitGet(AtPdhPrmController self)
    {
    ThaPdhDe1 de1 = (ThaPdhDe1)De1Get(self);
    uint32 regAddress = cAf6Reg_upen_prm_txcfgcr_Base + PrmOffset(self);
    uint32 regValue = mChannelHwRead(de1, regAddress, cAtModulePdh);

    return (uint8)mRegField(regValue, cAf6_upen_prm_txcfgcr_cfg_prm_cr_);
    }

static eAtModulePdhRet HwTxStandardSet(ThaPdhPrmController self, uint8 standard)
    {
    ThaPdhDe1 de1 = De1Get((AtPdhPrmController)self);
    uint32 regAddr, regVal;

    regAddr = cAf6Reg_upen_prm_txcfgcr_Base + PrmOffset((AtPdhPrmController)self);
    regVal = mChannelHwRead(de1, regAddr, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_upen_prm_txcfgcr_cfg_prmstd_tx_, standard);
    mChannelHwWrite(de1, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

static eAtModulePdhRet HwRxStandardSet(ThaPdhPrmController self, uint8 standard)
    {
    ThaPdhDe1 de1 = De1Get((AtPdhPrmController)self);
    uint32 regAddr, regVal;

    regAddr = cAf6Reg_upen_rxprm_cfgstd_Base + PrmOffset((AtPdhPrmController)self);
    regVal = mChannelHwRead(de1, regAddr, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_upen_rxprm_cfgstd_cfg_std_prm_, standard);
    mChannelHwWrite(de1, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

static eAtModulePdhRet HwTxCRBitSet(ThaPdhPrmController self, uint8 value)
    {
    ThaPdhDe1 de1 = De1Get((AtPdhPrmController)self);
    uint32 regAddress, regValue;

    regAddress = cAf6Reg_upen_prm_txcfgcr_Base + PrmOffset((AtPdhPrmController)self);
    regValue = mChannelHwRead(de1, regAddress, cAtModulePdh);
    mRegFieldSet(regValue, cAf6_upen_prm_txcfgcr_cfg_prm_cr_, value);
    mChannelHwWrite(de1, regAddress, regValue, cAtModulePdh);

    return cAtOk;
    }

static eAtModulePdhRet RxHwEnable(ThaPdhPrmController self, eBool enable)
    {
    ThaPdhDe1 de1 = De1Get((AtPdhPrmController)self);
    uint32 regAddress, regValue;

    regAddress = cAf6Reg_upen_rxprm_cfgstd_Base + PrmOffset((AtPdhPrmController)self);
    regValue = mChannelHwRead(de1, regAddress, cAtModulePdh);
    mRegFieldSet(regValue, cAf6_upen_rxprm_cfgstd_cfg_prm_rxen_, (enable ? 1 : 0));
    mChannelHwWrite(de1, regAddress, regValue, cAtModulePdh);
    return cAtOk;
    }

static uint32 StartVersionHasRxEnabledCfgRegister(ThaPdhPrmController self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x6, 0x4647);
    }

static eAtModulePdhRet TxHwEnable(ThaPdhPrmController self, eBool enable)
    {
    ThaPdhDe1 de1 = De1Get((AtPdhPrmController)self);
    uint32 regAddress, regValue;

    regAddress = cAf6Reg_upen_prm_txcfgcr_Base + PrmOffset((AtPdhPrmController)self);
    regValue = mChannelHwRead(de1, regAddress, cAtModulePdh);
    mRegFieldSet(regValue, cAf6_upen_prm_txcfgcr_cfg_prmen_tx_, mBoolToBin(enable));
    mChannelHwWrite(de1, regAddress, regValue, cAtModulePdh);

    return cAtOk;
    }

static eAtModulePdhRet HwEnable(ThaPdhPrmController self, eBool enable)
    {
    eAtRet ret = TxHwEnable(self, enable);

    if (ThaPdhPrmControllerHasRxEnabledCfgRegister(self))
    	ret |= RxHwEnable(self, enable);

    return ret;
    }

static uint32 CounterOffset(ThaPdhPrmController self, eBool read2Clear)
    {
    uint8 hwRead2clear = read2Clear ? 0x0 : 0x1;
    return (PrmOffset((AtPdhPrmController)self) + (uint32)(hwRead2clear * 2048));
    }

static eAtModulePdhRet HwTxLBBitSet(ThaPdhPrmController self, uint8 value)
    {
    ThaPdhDe1 de1 = De1Get((AtPdhPrmController)self);
    uint32 regAddress, regValue;

    regAddress = cAf6Reg_upen_prm_txcfglb_Base + Tha602PrmControllerLBOffset((AtPdhPrmController)self);
    regValue = mChannelHwRead(de1, regAddress, cAtModulePdh);
    mRegFieldSet(regValue, cAf6_upen_prm_txcfglb_cfg_prm_lb_, value);
    mChannelHwWrite(de1, regAddress, regValue, cAtModulePdh);

    return cAtOk;
    }

static eAtModulePdhRet TxLBBitEnable(AtPdhPrmController self, eBool enable)
    {
    ThaPdhDe1 de1 = De1Get(self);
    uint32 regAddress, regValue;

    regAddress = cAf6Reg_upen_prm_txcfglb_Base + Tha602PrmControllerLBOffset(self);
    regValue = mChannelHwRead(de1, regAddress, cAtModulePdh);
    mRegFieldSet(regValue, cAf6_upen_prm_txcfglb_cfg_prm_enlb_, mBoolToBin(enable));
    mChannelHwWrite(de1, regAddress, regValue, cAtModulePdh);

    return cAtOk;
    }

static eBool TxLBBitIsEnabled(AtPdhPrmController self)
    {
    ThaPdhDe1 de1 = De1Get(self);
    uint32 regAddress, regValue;

    regAddress = cAf6Reg_upen_prm_txcfglb_Base + Tha602PrmControllerLBOffset(self);
    regValue = mChannelHwRead(de1, regAddress, cAtModulePdh);
    return mRegField(regValue, cAf6_upen_prm_txcfglb_cfg_prm_enlb_) ? cAtTrue : cAtFalse;
    }

static uint8 TxLBBitGet(AtPdhPrmController self)
    {
    ThaPdhDe1 de1 = (ThaPdhDe1)De1Get(self);
    uint32 regAddress = cAf6Reg_upen_prm_txcfglb_Base + Tha602PrmControllerLBOffset(self);
    uint32 regValue = mChannelHwRead(de1, regAddress, cAtModulePdh);

    return (uint8)mRegField(regValue, cAf6_upen_prm_txcfglb_cfg_prm_lb_);
    }

static eAtModulePdhRet HwCRBitExpectSet(ThaPdhPrmController self, uint8 value)
    {
    ThaPdhDe1 de1 = De1Get((AtPdhPrmController)self);
    uint32 regAddress, regValue;

    regAddress = cAf6Reg_upen_rxprm_cfgstd_Base + PrmOffset((AtPdhPrmController)self);
    regValue = mChannelHwRead(de1, regAddress, cAtModulePdh);
    mRegFieldSet(regValue, cAf6_upen_rxprm_cfgstd_cfg_prm_cr_, value);
    mChannelHwWrite(de1, regAddress, regValue, cAtModulePdh);

    return cAtOk;
    }

static uint8 CRBitExpectGet(AtPdhPrmController self)
    {
    ThaPdhDe1 de1 = (ThaPdhDe1)De1Get(self);
    uint32 regAddress = cAf6Reg_upen_rxprm_cfgstd_Base + PrmOffset(self);
    uint32 regValue = mChannelHwRead(de1, regAddress, cAtModulePdh);

    return (uint8)mRegField(regValue, cAf6_upen_rxprm_cfgstd_cfg_prm_cr_);
    }

static eAtModulePdhRet HwFcsBitOrderSet(AtPdhPrmController self, eAtBitOrder order)
    {
    ThaPdhDe1 de1 = De1Get(self);
    uint32 regAddr, regVal;

    /* Check if invalid input */
    if ((order != cAtBitOrderMsb) && (order != cAtBitOrderLsb))
        return cAtErrorInvlParm;

    regAddr = cAf6Reg_upen_prm_txcfgcr_Base + PrmOffset(self);
    regVal = mChannelHwRead(de1, regAddr, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_upen_prm_txcfgcr_cfg_prm_txfcs_mode_, (order == cAtBitOrderLsb) ? 0 : 1);
    mChannelHwWrite(de1, regAddr, regVal, cAtModulePdh);

    regAddr = cAf6Reg_upen_rxprm_cfgstd_Base + PrmOffset(self);
    regVal = mChannelHwRead(de1, regAddr, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_upen_rxprm_cfgstd_cfg_prm_rxfcs_mode_, (order == cAtBitOrderLsb) ? 0 : 1);
    mChannelHwWrite(de1, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

static eAtBitOrder HwFcsBitOrderGet(AtPdhPrmController self)
    {
    uint32 regAddr = cAf6Reg_upen_prm_txcfgcr_Base + PrmOffset(self);
    uint32 regVal = mChannelHwRead(De1Get(self), regAddr, cAtModulePdh);

    return mRegField(regVal, cAf6_upen_prm_txcfgcr_cfg_prm_txfcs_mode_) ? cAtBitOrderMsb : cAtBitOrderLsb;
    }

static uint32 InterruptOffset(ThaPdhPrmController self, uint8 *sliceId)
    {
    ThaPdhDe1 de1 = De1Get((AtPdhPrmController)self);
    return Tha602PrmControllerInterruptOffset(de1, sliceId);
    }

static void OverrideAtPdhPrmController(AtPdhPrmController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPdhPrmControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPdhPrmControllerOverride, m_AtPdhPrmControllerMethods, sizeof(m_AtPdhPrmControllerOverride));

        mMethodOverride(m_AtPdhPrmControllerOverride, IsEnabled);
        mMethodOverride(m_AtPdhPrmControllerOverride, TxStandardGet);
        mMethodOverride(m_AtPdhPrmControllerOverride, RxStandardGet);
        mMethodOverride(m_AtPdhPrmControllerOverride, TxCRBitGet);
        mMethodOverride(m_AtPdhPrmControllerOverride, TxLBBitGet);
        mMethodOverride(m_AtPdhPrmControllerOverride, CRBitExpectGet);
        mMethodOverride(m_AtPdhPrmControllerOverride, TxLBBitEnable);
        mMethodOverride(m_AtPdhPrmControllerOverride, TxLBBitIsEnabled);
        }

    mMethodsSet(self, &m_AtPdhPrmControllerOverride);
    }

static void OverrideThaPdhPrmController(AtPdhPrmController self)
    {
    ThaPdhPrmController controller = (ThaPdhPrmController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhPrmControllerOverride, mMethodsGet(controller), sizeof(m_ThaPdhPrmControllerOverride));

        mMethodOverride(m_ThaPdhPrmControllerOverride, TxMessageRegister);
        mMethodOverride(m_ThaPdhPrmControllerOverride, TxByteRegister);
        mMethodOverride(m_ThaPdhPrmControllerOverride, RxGoodMessageRegister);
        mMethodOverride(m_ThaPdhPrmControllerOverride, RxByteRegister);
        mMethodOverride(m_ThaPdhPrmControllerOverride, RxDiscardRegister);
        mMethodOverride(m_ThaPdhPrmControllerOverride, RxMissRegister);
        mMethodOverride(m_ThaPdhPrmControllerOverride, CounterOffset);
        mMethodOverride(m_ThaPdhPrmControllerOverride, HwTxStandardSet);
        mMethodOverride(m_ThaPdhPrmControllerOverride, HwRxStandardSet);
        mMethodOverride(m_ThaPdhPrmControllerOverride, HwTxCRBitSet);
        mMethodOverride(m_ThaPdhPrmControllerOverride, HwEnable);
        mMethodOverride(m_ThaPdhPrmControllerOverride, HwTxLBBitSet);
        mMethodOverride(m_ThaPdhPrmControllerOverride, HwCRBitExpectSet);
        mMethodOverride(m_ThaPdhPrmControllerOverride, HwFcsBitOrderSet);
        mMethodOverride(m_ThaPdhPrmControllerOverride, HwFcsBitOrderGet);
        mMethodOverride(m_ThaPdhPrmControllerOverride, StartVersionHasRxEnabledCfgRegister);
        mMethodOverride(m_ThaPdhPrmControllerOverride, InterruptOffset);
        }

    mMethodsSet(controller, &m_ThaPdhPrmControllerOverride);
    }

static void Override(AtPdhPrmController self)
    {
    OverrideAtPdhPrmController(self);
    OverrideThaPdhPrmController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031PdhPrmController);
    }

AtPdhPrmController Tha60210031PdhPrmControllerObjectInit(AtPdhPrmController self, AtPdhDe1 de1)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhPrmControllerObjectInit(self, de1) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPdhPrmController Tha60210031PdhPrmControllerNew(AtPdhDe1 de1)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhPrmController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031PdhPrmControllerObjectInit(newController, de1);
    }

uint32 Tha602PrmControllerInterruptOffset(ThaPdhDe1 self, uint8 *sliceId)
    {
    uint8 hwStsId, hwSliceId, tugOrDe2Id, tu1xOrDe1Id;
    uint32 offset;

    if (ThaPdhChannelHwIdGet((AtPdhChannel)self, cAtModulePdh, &hwSliceId, &hwStsId) != cAtOk)
        return cBit31_0;

    if (sliceId)
        *sliceId = hwSliceId;
    tu1xOrDe1Id = Tha60210031Tu1xOrDe1IdGet(self, &tugOrDe2Id);
    offset = (uint32)((hwStsId << 5) + (tugOrDe2Id << 2) + tu1xOrDe1Id);
    return (offset);
    }

uint32 Tha602PrmControllerLBOffset(AtPdhPrmController self)
    {
    ThaPdhDe1 de1 = De1Get(self);
    Tha60210031ModulePdh pdhModule = (Tha60210031ModulePdh)AtChannelModuleGet((AtChannel)de1);
    return Tha60210031ModulePdhLBOffset(pdhModule, de1);
    }

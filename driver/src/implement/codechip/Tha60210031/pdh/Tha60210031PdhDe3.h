/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Tha60210031
 * 
 * File        : Tha60210031PdhDe3.h
 * 
 * Created Date: Nov 5, 2015
 *
 * Description : header file
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031PDHDE3_H_
#define _THA60210031PDHDE3_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/pdh/ThaPdhDe3.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
typedef struct tTha60210031PdhDe3 * Tha60210031PdhDe3;

eBool Tha60210031PdhDe3IsDe3LiuMode(ThaPdhDe3 self);
eBool Tha60210031De3MdlRxFilterMessagePerTypeIsSupported(AtPdhDe3 self);
eAtRet Tha60210031PdhDe3MdlHwBufferFlush(AtPdhDe3 self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031PDHDE3_H_ */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAP
 *
 * File        : Tha61210011ModuleDemap.c
 *
 * Created Date: May 28, 2015
 *
 * Description : DEMAP module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/map/Tha60210011ModuleMapInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha61210011ModuleDemap
    {
    tTha60210011ModuleDemap super;
    }tTha61210011ModuleDemap;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleAbstractMapMethods m_ThaModuleAbstractMapOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 NumHoSlices(ThaModuleAbstractMap self)
    {
    AtUnused(self);
    return 1;
    }

static void OverrideThaModuleAbstractMap(AtModule self)
    {
    ThaModuleAbstractMap mapModule = (ThaModuleAbstractMap)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleAbstractMapOverride, mMethodsGet(mapModule), sizeof(m_ThaModuleAbstractMapOverride));

        mMethodOverride(m_ThaModuleAbstractMapOverride, NumHoSlices);
        }

    mMethodsSet(mapModule, &m_ThaModuleAbstractMapOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleAbstractMap(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha61210011ModuleDemap);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011ModuleDemapObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha61210011ModuleDemapNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha61210011SdhLineSerdesController.c
 *
 * Created Date: Jul 23, 2015
 *
 * Description : SDH SERDES controller.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/physical/Tha6021Physical.h"
#include "../../Tha60150011/physical/Tha60150011SdhLineSerdesController.h"
#include "Tha61210011Physical.h"

/*--------------------------- Define -----------------------------------------*/
#define cSerdesControl       0xF00042

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha61210011SdhLineSerdesController
    {
    tTha60210011Tfi5SerdesController super;
    }tTha61210011SdhLineSerdesController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSerdesControllerMethods m_AtSerdesControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void ResetTrigger(AtSerdesController self)
    {
    uint32 normalVal = AtSerdesControllerRead(self, cSerdesControl, cAtModuleSdh);
    AtSerdesControllerWrite(self, cSerdesControl, normalVal | cBit31, cAtModuleSdh);
    AtOsalUSleep(100000);
    AtSerdesControllerWrite(self, cSerdesControl, normalVal, cAtModuleSdh);
    AtOsalUSleep(100000);
    }

static eAtRet Reset(AtSerdesController self)
    {
    static const uint8 cSerdesTxDifferentialDefaultValue = 8;

    ResetTrigger(self);
    Tha60150011SdhLineSerdesControllerTransmitDifferentialSet(self, cSerdesTxDifferentialDefaultValue);

    return cAtOk;
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, mMethodsGet(self), sizeof(m_AtSerdesControllerOverride));
        mMethodOverride(m_AtSerdesControllerOverride, Reset);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtSerdesController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha61210011SdhLineSerdesController);
    }

static AtSerdesController ObjectInit(AtSerdesController self, AtSdhLine sdhLine, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011Tfi5SerdesControllerObjectInit(self, sdhLine, serdesId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController Tha61210011SdhLineSerdesControllerNew(AtSdhLine sdhLine, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, sdhLine, serdesId);
    }

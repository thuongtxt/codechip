/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha61210011Physical.h
 * 
 * Created Date: Jul 23, 2015
 *
 * Description : 61210011 Physical definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA61210011PHYSICAL_H_
#define _THA61210011PHYSICAL_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSerdesController Tha61210011SdhLineSerdesControllerNew(AtSdhLine sdhLine, uint32 serdesId);

#ifdef __cplusplus
}
#endif
#endif /* _THA61210011PHYSICAL_H_ */


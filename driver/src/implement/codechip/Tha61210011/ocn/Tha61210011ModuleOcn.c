/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : OCN
 *
 * File        : Tha61210011ModuleOcn.c
 *
 * Created Date: May 27, 2015
 *
 * Description : OCN Module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/ocn/Tha60210011ModuleOcnInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha61210011ModuleOcn
    {
    tTha60210011ModuleOcn super;
    }tTha61210011ModuleOcn;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleOcnMethods         m_ThaModuleOcnOverride;
static tTha60210011ModuleOcnMethods m_Tha60210011ModuleOcnOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool UseTfi5(Tha60210011ModuleOcn self)
    {
    AtUnused(self);

    /* Just temporarily return true here due to HW is debugging this feature.
     * Run on EP does not use TFI5 bus */
    return cAtTrue;
    }

static eBool NeedScramble(Tha60210011ModuleOcn self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool CanMovePathAisRxForcingPoint(ThaModuleOcn self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void OverrideThaModuleOcn(AtModule self)
    {
    ThaModuleOcn ocnModule = (ThaModuleOcn)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleOcnOverride, mMethodsGet(ocnModule), sizeof(m_ThaModuleOcnOverride));

        mMethodOverride(m_ThaModuleOcnOverride, CanMovePathAisRxForcingPoint);
        }

    mMethodsSet(ocnModule, &m_ThaModuleOcnOverride);
    }

static void OverrideTha60210011ModuleOcn(AtModule self)
    {
    Tha60210011ModuleOcn module = (Tha60210011ModuleOcn)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModuleOcnOverride, mMethodsGet(module), sizeof(m_Tha60210011ModuleOcnOverride));

        mMethodOverride(m_Tha60210011ModuleOcnOverride, UseTfi5);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, NeedScramble);
        }

    mMethodsSet(module, &m_Tha60210011ModuleOcnOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleOcn(self);
    OverrideTha60210011ModuleOcn(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha61210011ModuleOcn);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011ModuleOcnObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha61210011ModuleOcnNew(AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newModule, device);
    }

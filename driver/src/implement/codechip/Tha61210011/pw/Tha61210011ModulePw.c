/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Pseudowire
 *
 * File        : Tha61210011ModulePw.c
 *
 * Created Date: Sep 10, 2015
 *
 * Description : Module pw of 61210011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/pw/Tha60210011ModulePwInternal.h"
#include "../../Tha60210011/pw/interruptprocessor/Tha60210011PwInterruptProcessor.h"
#include "../../Tha60210011/pw/defectcontrollers/Tha60210011PwDefectController.h"


/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((tTha61210011ModulePw *)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha61210011ModulePw
    {
    tTha60210011ModulePw super;
    }tTha61210011ModulePw;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePwMethods  m_AtModulePwOverride;
static tThaModulePwMethods m_ThaModulePwOverride;

/* Save super implementation */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static ThaPwDefectController DefectControllerCreate(ThaModulePw self, AtPw pw)
    {
    AtUnused(self);
    return Tha60210011PwDefectControllerPmcNew(pw);
    }

static ThaPwInterruptProcessor InterruptProcessorCreate(ThaModulePw self)
    {
    return Tha60210011PwInterruptProcessorPmcNew(self);
    }

static uint32 MaxPwsGet(AtModulePw self)
    {
    AtUnused(self);
    return 1344;
    }

static uint32 DefaultCounterModule(ThaModulePw self)
    {
    return AtModuleTypeGet((AtModule)self);
    }

static void OverrideAtModulePw(AtModulePw self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePwOverride, mMethodsGet(self), sizeof(tAtModulePwMethods));

        mMethodOverride(m_AtModulePwOverride, MaxPwsGet);
        }

    mMethodsSet(self, &m_AtModulePwOverride);
    }

static void OverrideThaModulePw(AtModulePw self)
    {
    ThaModulePw pwModule = (ThaModulePw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePwOverride, mMethodsGet(pwModule), sizeof(m_ThaModulePwOverride));

        mMethodOverride(m_ThaModulePwOverride, DefectControllerCreate);
        mMethodOverride(m_ThaModulePwOverride, InterruptProcessorCreate);
        mMethodOverride(m_ThaModulePwOverride, DefaultCounterModule);
        }

    mMethodsSet(pwModule, &m_ThaModulePwOverride);
    }

static void Override(AtModulePw self)
    {
    OverrideAtModulePw(self);
    OverrideThaModulePw(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha61210011ModulePw);
    }

static AtModulePw ObjectInit(AtModulePw self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011ModulePwObjectInit((AtModulePw)self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePw Tha61210011ModulePwNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePw newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

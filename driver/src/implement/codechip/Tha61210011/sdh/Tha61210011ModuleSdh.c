/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha61210011ModuleSdh.c
 *
 * Created Date: May 27, 2015
 *
 * Description : SDH module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/sdh/Tha60210011ModuleSdhInternal.h"
#include "../physical/Tha61210011Physical.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha61210011ModuleSdh
    {
    tTha60210011ModuleSdh super;
    }tTha61210011ModuleSdh;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleSdhMethods          m_AtModuleSdhOverride;
static tThaModuleSdhMethods         m_ThaModuleSdhOverride;
static tTha60210011ModuleSdhMethods m_Tha60210011ModuleSdhOverride;

static uint8 numberOfSdhLines = 0;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 MaxNumHoLines(Tha60210011ModuleSdh self)
    {
    AtUnused(self);
    if (numberOfSdhLines  == 0)
        return 1;

    return numberOfSdhLines;
    }

static uint8 MaxNumLoLines(Tha60210011ModuleSdh self)
    {
    AtUnused(self);
    if (numberOfSdhLines  == 0)
        return 1;

    if (numberOfSdhLines  == 4)
        return 3;

    if (numberOfSdhLines  == 8)
        return 6;

    return numberOfSdhLines;
    }

static uint8 NumTfi5Lines(AtModuleSdh self)
    {
    AtUnused(self);
    if (numberOfSdhLines  == 0)
        return 1;

    return numberOfSdhLines;
    }

static eBool HasAps(ThaModuleSdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool LineSerdesPrbsIsSupported(ThaModuleSdh self, AtSdhLine line, AtSerdesController serdes)
    {
    AtUnused(self);
    AtUnused(line);
    AtUnused(serdes);
    return cAtFalse;
    }

static AtPrbsEngine LineSerdesPrbsEngineCreate(ThaModuleSdh self, AtSdhLine line, AtSerdesController serdes)
    {
    AtUnused(self);
    AtUnused(line);
    AtUnused(serdes);
    return NULL;
    }

static AtSerdesController SerdesControllerCreate(ThaModuleSdh self, AtSdhLine line, uint32 serdesId)
    {
    AtUnused(self);
    return Tha61210011SdhLineSerdesControllerNew(line, serdesId);
    }

static void OverrideThaModuleSdh(AtModuleSdh self)
    {
    ThaModuleSdh sdhModule = (ThaModuleSdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleSdhOverride, mMethodsGet(sdhModule), sizeof(m_ThaModuleSdhOverride));

        mMethodOverride(m_ThaModuleSdhOverride, HasAps);
        mMethodOverride(m_ThaModuleSdhOverride, LineSerdesPrbsEngineCreate);
        mMethodOverride(m_ThaModuleSdhOverride, LineSerdesPrbsIsSupported);
        mMethodOverride(m_ThaModuleSdhOverride, SerdesControllerCreate);
        }

    mMethodsSet(sdhModule, &m_ThaModuleSdhOverride);
    }

static void OverrideAtModuleSdh(AtModuleSdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleSdhOverride, mMethodsGet(self), sizeof(m_AtModuleSdhOverride));

        mMethodOverride(m_AtModuleSdhOverride, NumTfi5Lines);
        }

    mMethodsSet(self, &m_AtModuleSdhOverride);
    }

static void OverrideTha60210011ModuleSdh(AtModuleSdh self)
    {
    Tha60210011ModuleSdh module = (Tha60210011ModuleSdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModuleSdhOverride, mMethodsGet(module), sizeof(m_Tha60210011ModuleSdhOverride));

        mMethodOverride(m_Tha60210011ModuleSdhOverride, MaxNumHoLines);
        mMethodOverride(m_Tha60210011ModuleSdhOverride, MaxNumLoLines);
        }

    mMethodsSet(module, &m_Tha60210011ModuleSdhOverride);
    }

static void Override(AtModuleSdh self)
    {
    OverrideAtModuleSdh(self);
    OverrideThaModuleSdh(self);
    OverrideTha60210011ModuleSdh(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha61210011ModuleSdh);
    }

static AtModuleSdh ObjectInit(AtModuleSdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011ModuleSdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSdh Tha61210011ModuleSdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

void Tha61210011ModuleSdhNumLines(uint8 numLines);
void Tha61210011ModuleSdhNumLines(uint8 numLines)
    {
    numberOfSdhLines = numLines;
    }

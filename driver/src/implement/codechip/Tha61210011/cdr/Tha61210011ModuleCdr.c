/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : Tha61210011ModuleCdr.c
 *
 * Created Date: May 28, 2015
 *
 * Description : CDR module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/cdr/Tha60210011ModuleCdrInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha61210011ModuleCdr
    {
    tTha60210011ModuleCdr super;
    }tTha61210011ModuleCdr;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60210011ModuleCdrMethods m_Tha60210011ModuleCdrOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 MaxNumLoSlices(Tha60210011ModuleCdr self)
    {
    AtUnused(self);
    return 2;
    }

static void OverrideTha60210011ModuleCdr(AtModule self)
    {
    Tha60210011ModuleCdr module = (Tha60210011ModuleCdr)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModuleCdrOverride, mMethodsGet(module), sizeof(m_Tha60210011ModuleCdrOverride));

        mMethodOverride(m_Tha60210011ModuleCdrOverride, MaxNumLoSlices);
        }

    mMethodsSet(module, &m_Tha60210011ModuleCdrOverride);
    }

static void Override(AtModule self)
    {
    OverrideTha60210011ModuleCdr(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha61210011ModuleCdr);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011ModuleCdrObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha61210011ModuleCdrNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha61210011Device.c
 *
 * Created Date: May 27, 2015
 *
 * Description : Test device for product 60210011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/man/Tha60210011DeviceInternal.h"
#include "../../../default/prbs/ThaModulePrbs.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha61210011Device
    {
    tTha60210011Device super;
    }tTha61210011Device;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceMethods           m_AtDeviceOverride;
static tThaDeviceMethods          m_ThaDeviceOverride;

/* Super implementation */
static const tAtDeviceMethods *m_AtDeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const eAtModule *AllSupportedModulesGet(AtDevice self, uint8 *numModules)
    {
    static const eAtModule supportedModules[] = {cAtModulePdh,
                                                 cAtModulePw,
                                                 cAtModuleEth,
                                                 cAtModuleRam,
                                                 cAtModuleSdh,
                                                 cAtModulePktAnalyzer,
                                                 cAtModulePrbs};

    if (numModules)
        *numModules = mCount(supportedModules);

    AtUnused(self);
    return supportedModules;
    }

static eBool ModuleIsSupported(AtDevice self, eAtModule moduleId)
    {
    uint32 moduleValue = moduleId;

    switch (moduleValue)
        {
        case cAtModuleSur: return cAtFalse;
        default:
            return m_AtDeviceMethods->ModuleIsSupported(self, moduleId);
        }
    }

static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    uint32 moduleId_ = moduleId;

    if (moduleId_ == cAtModuleSdh)    return (AtModule)Tha61210011ModuleSdhNew(self);
    if (moduleId_ == cAtModuleEth)    return (AtModule)Tha61210011ModuleEthNew(self);
    if (moduleId_ == cAtModulePdh)    return (AtModule)Tha61210011ModulePdhNew(self);
    if (moduleId_ == cAtModulePw)     return (AtModule)Tha61210011ModulePwNew(self);
    if (moduleId_ == cThaModuleOcn)   return (AtModule)Tha61210011ModuleOcnNew(self);
    if (moduleId_ == cThaModuleMap)   return (AtModule)Tha61210011ModuleMapNew(self);
    if (moduleId_ == cThaModuleDemap) return (AtModule)Tha61210011ModuleDemapNew(self);
    if (moduleId_ == cThaModuleCdr)   return (AtModule)Tha61210011ModuleCdrNew(self);

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static AtLongRegisterAccess GlobalLongRegisterAccessCreate(AtDevice self)
    {
    AtUnused(self);
    return Tha32BitGlobalLongRegisterAccessNew();
    }

static eAtRet SdhSerdesReset(AtDevice self)
    {
    /* This is for testing on board K7 and it only has one port */
    AtSdhLine line = AtModuleSdhLineGet((AtModuleSdh)AtDeviceModuleGet(self, cAtModuleSdh), 0);
    AtSerdesController serdesController = AtSdhLineSerdesController(line);
    if (serdesController)
        return AtSerdesControllerReset(serdesController);
    return cAtOk;
    }

static eAtRet Init(AtDevice self)
    {
    eAtRet ret = m_AtDeviceMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    if (!ThaDeviceIsEp((ThaDevice)self))
        return cAtOk;

    return SdhSerdesReset(self);
    }
    

static ThaIntrController IntrControllerCreate(ThaDevice self, AtIpCore core)
    {
    AtUnused(self);
    return Tha61210011IntrControllerNew(core);
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(tAtDeviceMethods));

        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, AllSupportedModulesGet);
        mMethodOverride(m_AtDeviceOverride, ModuleIsSupported);
        mMethodOverride(m_AtDeviceOverride, GlobalLongRegisterAccessCreate);
        mMethodOverride(m_AtDeviceOverride, Init);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static void OverrideThaDevice(AtDevice self)
    {
    ThaDevice device = (ThaDevice)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDeviceOverride, mMethodsGet(device), sizeof(tThaDeviceMethods));

        mMethodOverride(m_ThaDeviceOverride, IntrControllerCreate);
        }

    mMethodsSet(device, &m_ThaDeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideThaDevice(self);
    OverrideAtDevice(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha61210011Device);
    }

static AtDevice ObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011DeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha61210011DeviceNew(AtDriver driver, uint32 productCode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDevice == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newDevice, driver, productCode);
    }

/*-----------------------------------------------------------------------------
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies. 
 *
 * Module      : DEVICE
 *
 * File        : Tha61210011InterruptController.c
 *
 * Created Date: Sep 11, 2015
 *
 * Description : Interrupt controller implementation.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../codechip/Tha60210011/man/Tha60210011InterruptControllerInternal.h"
#include "../../../codechip/Tha60210011/man/Tha60210011DeviceReg.h"
#include "../../../codechip/Tha60210011/man/Tha60210011Device.h"
#include "../../Tha60210031/pmc/Tha60210031PmcReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/
typedef struct tTha61210011IntrController
    {
    tTha60210011InterruptController super;
    } tTha61210011IntrController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tThaIpCoreMethods m_ThaIpCoreOverride;

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static uint32 BaseAddress(ThaIpCore self)
    {
    return Tha60210011DeviceInterruptBaseAddress((Tha60210011Device)AtIpCoreDeviceGet((AtIpCore)self));
    }

static void HelperInterruptHwEnable(ThaIpCore self, uint32 mask, eBool enable)
    {
    AtHal hal = AtIpCoreHalGet(mIpCore(self));
    uint32 interruptEnReg = AtHalRead(hal, BaseAddress(self) + cAf6Reg_global_interrupt_mask_enable);

    if (enable)
        interruptEnReg |= mask;
    else
        interruptEnReg &= ~mask;

    AtHalWrite(hal, BaseAddress(self) + cAf6Reg_global_interrupt_mask_enable, interruptEnReg);
    }

static void PwHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    AtHal hal = AtIpCoreHalGet(mIpCore(self));
    uint32 slice;
    const uint32 baseAddress = 0x1E00000;

    HelperInterruptHwEnable(self, cAf6_global_interrupt_mask_enable_PWEIntEnable_Mask, enable);

    /* Enable PW slice interrupt */
    for (slice = 0; slice < 2; slice++)
        AtHalWrite(hal, baseAddress + cAf6Reg_counter_per_group_intr_en_ctrl(slice), (enable) ? cAf6_counter_per_group_intr_en_ctrl_GroupIntrEn_Mask : 0x0);
    }

static void OverrideThaIpCore(ThaIntrController self)
    {
    ThaIpCore thaIp = (ThaIpCore)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaIpCoreOverride, mMethodsGet(thaIp), sizeof(m_ThaIpCoreOverride));
        mMethodOverride(m_ThaIpCoreOverride, PwHwInterruptEnable);
        }

    mMethodsSet(thaIp, &m_ThaIpCoreOverride);
    }

static void Override(ThaIntrController self)
    {
    OverrideThaIpCore(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha61210011IntrController);
    }

static ThaIntrController ObjectInit(ThaIntrController self, AtIpCore ipCore)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011IntrControllerObjectInit(self, ipCore) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;
    return self;
    }

/* Create controller object */
ThaIntrController Tha61210011IntrControllerNew(AtIpCore ipCore)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaIntrController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, ipCore);
    }

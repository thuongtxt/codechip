/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha60210061SerdesTurningReg.h
 * 
 * Created Date: Aug 25, 2016
 *
 * Description : SERDES tuning regs
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061SERDESTUNINGREG_H_
#define _THA60210061SERDESTUNINGREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cAf6Reg_SerdesTurning_BaseAddress     0xF45000

/*------------------------------------------------------------------------------
Reg Name   : Serdes PMA Reset
Reg Addr   : 0x00040
Reg Formula: Base Addr + 0x00040
    Where  :
Reg Desc   :
This is the global configuration register for the Global Serdes Control

------------------------------------------------------------------------------*/
#define cAf6Reg_Serdes_PMA_Reset_Base                                                                  0x00040
#define cAf6Reg_Serdes_PMA_Reset                                                                       0x00040
#define cAf6Reg_Serdes_PMA_Reset_WidthVal                                                                   32
#define cAf6Reg_Serdes_PMA_Reset_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: xfi2gt_txpmareset
BitField Type: RW
BitField Desc: XFI Serdes TX PMA Reset Lines 2 0: Release 1: Reset
BitField Bits: [7]
--------------------------------------*/
#define cAf6_Serdes_PMA_Reset_xfi2gt_txpmareset_Bit_Start                                                    7
#define cAf6_Serdes_PMA_Reset_xfi2gt_txpmareset_Bit_End                                                      7
#define cAf6_Serdes_PMA_Reset_xfi2gt_txpmareset_Mask                                                     cBit7
#define cAf6_Serdes_PMA_Reset_xfi2gt_txpmareset_Shift                                                        7
#define cAf6_Serdes_PMA_Reset_xfi2gt_txpmareset_MaxVal                                                     0x1
#define cAf6_Serdes_PMA_Reset_xfi2gt_txpmareset_MinVal                                                     0x0
#define cAf6_Serdes_PMA_Reset_xfi2gt_txpmareset_RstVal                                                     0x0

/*--------------------------------------
BitField Name: xfi1gt_txpmareset
BitField Type: RW
BitField Desc: XFI Serdes TX PMA Reset Lines 1 0: Release 1: Reset
BitField Bits: [6]
--------------------------------------*/
#define cAf6_Serdes_PMA_Reset_xfi1gt_txpmareset_Bit_Start                                                    6
#define cAf6_Serdes_PMA_Reset_xfi1gt_txpmareset_Bit_End                                                      6
#define cAf6_Serdes_PMA_Reset_xfi1gt_txpmareset_Mask                                                     cBit6
#define cAf6_Serdes_PMA_Reset_xfi1gt_txpmareset_Shift                                                        6
#define cAf6_Serdes_PMA_Reset_xfi1gt_txpmareset_MaxVal                                                     0x1
#define cAf6_Serdes_PMA_Reset_xfi1gt_txpmareset_MinVal                                                     0x0
#define cAf6_Serdes_PMA_Reset_xfi1gt_txpmareset_RstVal                                                     0x0

/*--------------------------------------
BitField Name: qsgmii2gt_txpmareset
BitField Type: RW
BitField Desc: QSGMII Serdes TX PMA Reset Lines 2 0: Release 1: Reset
BitField Bits: [5]
--------------------------------------*/
#define cAf6_Serdes_PMA_Reset_qsgmii2gt_txpmareset_Bit_Start                                                 5
#define cAf6_Serdes_PMA_Reset_qsgmii2gt_txpmareset_Bit_End                                                   5
#define cAf6_Serdes_PMA_Reset_qsgmii2gt_txpmareset_Mask                                                  cBit5
#define cAf6_Serdes_PMA_Reset_qsgmii2gt_txpmareset_Shift                                                     5
#define cAf6_Serdes_PMA_Reset_qsgmii2gt_txpmareset_MaxVal                                                  0x1
#define cAf6_Serdes_PMA_Reset_qsgmii2gt_txpmareset_MinVal                                                  0x0
#define cAf6_Serdes_PMA_Reset_qsgmii2gt_txpmareset_RstVal                                                  0x0

/*--------------------------------------
BitField Name: qsgmii1gt_txpmareset
BitField Type: RW
BitField Desc: QSGMII Serdes TX PMA Reset Lines 1 0: Release 1: Reset
BitField Bits: [4]
--------------------------------------*/
#define cAf6_Serdes_PMA_Reset_qsgmii1gt_txpmareset_Bit_Start                                                 4
#define cAf6_Serdes_PMA_Reset_qsgmii1gt_txpmareset_Bit_End                                                   4
#define cAf6_Serdes_PMA_Reset_qsgmii1gt_txpmareset_Mask                                                  cBit4
#define cAf6_Serdes_PMA_Reset_qsgmii1gt_txpmareset_Shift                                                     4
#define cAf6_Serdes_PMA_Reset_qsgmii1gt_txpmareset_MaxVal                                                  0x1
#define cAf6_Serdes_PMA_Reset_qsgmii1gt_txpmareset_MinVal                                                  0x0
#define cAf6_Serdes_PMA_Reset_qsgmii1gt_txpmareset_RstVal                                                  0x0

/*--------------------------------------
BitField Name: xfi2gt_rxpmareset
BitField Type: RW
BitField Desc: XFI Serdes RX PMA Reset Lines 2 0: Release 1: Reset
BitField Bits: [3]
--------------------------------------*/
#define cAf6_Serdes_PMA_Reset_xfi2gt_rxpmareset_Bit_Start                                                    3
#define cAf6_Serdes_PMA_Reset_xfi2gt_rxpmareset_Bit_End                                                      3
#define cAf6_Serdes_PMA_Reset_xfi2gt_rxpmareset_Mask                                                     cBit3
#define cAf6_Serdes_PMA_Reset_xfi2gt_rxpmareset_Shift                                                        3
#define cAf6_Serdes_PMA_Reset_xfi2gt_rxpmareset_MaxVal                                                     0x1
#define cAf6_Serdes_PMA_Reset_xfi2gt_rxpmareset_MinVal                                                     0x0
#define cAf6_Serdes_PMA_Reset_xfi2gt_rxpmareset_RstVal                                                     0x0

/*--------------------------------------
BitField Name: xfi1gt_rxpmareset
BitField Type: RW
BitField Desc: XFI Serdes RX PMA Reset Lines 1 0: Release 1: Reset
BitField Bits: [2]
--------------------------------------*/
#define cAf6_Serdes_PMA_Reset_xfi1gt_rxpmareset_Bit_Start                                                    2
#define cAf6_Serdes_PMA_Reset_xfi1gt_rxpmareset_Bit_End                                                      2
#define cAf6_Serdes_PMA_Reset_xfi1gt_rxpmareset_Mask                                                     cBit2
#define cAf6_Serdes_PMA_Reset_xfi1gt_rxpmareset_Shift                                                        2
#define cAf6_Serdes_PMA_Reset_xfi1gt_rxpmareset_MaxVal                                                     0x1
#define cAf6_Serdes_PMA_Reset_xfi1gt_rxpmareset_MinVal                                                     0x0
#define cAf6_Serdes_PMA_Reset_xfi1gt_rxpmareset_RstVal                                                     0x0

/*--------------------------------------
BitField Name: qsgmii2gt_rxpmareset
BitField Type: RW
BitField Desc: QSGMII Serdes RX PMA Reset Lines 2 0: Release 1: Reset
BitField Bits: [1]
--------------------------------------*/
#define cAf6_Serdes_PMA_Reset_qsgmii2gt_rxpmareset_Bit_Start                                                 1
#define cAf6_Serdes_PMA_Reset_qsgmii2gt_rxpmareset_Bit_End                                                   1
#define cAf6_Serdes_PMA_Reset_qsgmii2gt_rxpmareset_Mask                                                  cBit1
#define cAf6_Serdes_PMA_Reset_qsgmii2gt_rxpmareset_Shift                                                     1
#define cAf6_Serdes_PMA_Reset_qsgmii2gt_rxpmareset_MaxVal                                                  0x1
#define cAf6_Serdes_PMA_Reset_qsgmii2gt_rxpmareset_MinVal                                                  0x0
#define cAf6_Serdes_PMA_Reset_qsgmii2gt_rxpmareset_RstVal                                                  0x0

/*--------------------------------------
BitField Name: qsgmii1gt_rxpmareset
BitField Type: RW
BitField Desc: QSGMII Serdes RX PMA Reset Lines 1 0: Release 1: Reset
BitField Bits: [0]
--------------------------------------*/
#define cAf6_Serdes_PMA_Reset_qsgmii1gt_rxpmareset_Bit_Start                                                 0
#define cAf6_Serdes_PMA_Reset_qsgmii1gt_rxpmareset_Bit_End                                                   0
#define cAf6_Serdes_PMA_Reset_qsgmii1gt_rxpmareset_Mask                                                  cBit0
#define cAf6_Serdes_PMA_Reset_qsgmii1gt_rxpmareset_Shift                                                     0
#define cAf6_Serdes_PMA_Reset_qsgmii1gt_rxpmareset_MaxVal                                                  0x1
#define cAf6_Serdes_PMA_Reset_qsgmii1gt_rxpmareset_MinVal                                                  0x0
#define cAf6_Serdes_PMA_Reset_qsgmii1gt_rxpmareset_RstVal                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : Serdes txprecursor                                     `
Reg Addr   : 0x00041                                                `
Reg Formula: Base Addr + 0x00041                                    `
    Where  :
Reg Desc   :
This is the global configuration register for the Global Serdes Control

------------------------------------------------------------------------------*/
#define cAf6Reg_Serdes_txprecursor_xfi_qsgmii_Base                                                     0x00041
#define cAf6Reg_Serdes_txprecursor                                                                     0x00041
#define cAf6Reg_Serdes_txprecursor_WidthVal                                                                 32
#define cAf6Reg_Serdes_txprecursor_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: xfi2gt_txprecursor
BitField Type: RW
BitField Desc: XFI Port2 txprecursor
BitField Bits: [28:24]
--------------------------------------*/
#define cAf6_Serdes_txprecursor_xfi2gt_txprecursor_Bit_Start                                                24
#define cAf6_Serdes_txprecursor_xfi2gt_txprecursor_Bit_End                                                  28
#define cAf6_Serdes_txprecursor_xfi2gt_txprecursor_Mask                                              cBit28_24
#define cAf6_Serdes_txprecursor_xfi2gt_txprecursor_Shift                                                    24
#define cAf6_Serdes_txprecursor_xfi2gt_txprecursor_MaxVal                                                 0x1f
#define cAf6_Serdes_txprecursor_xfi2gt_txprecursor_MinVal                                                  0x0
#define cAf6_Serdes_txprecursor_xfi2gt_txprecursor_RstVal                                                  0x0

/*--------------------------------------
BitField Name: xfi1gt_txprecursor
BitField Type: RW
BitField Desc: XFI Port1 txprecursor
BitField Bits: [20:16]
--------------------------------------*/
#define cAf6_Serdes_txprecursor_xfi1gt_txprecursor_Bit_Start                                                16
#define cAf6_Serdes_txprecursor_xfi1gt_txprecursor_Bit_End                                                  20
#define cAf6_Serdes_txprecursor_xfi1gt_txprecursor_Mask                                              cBit20_16
#define cAf6_Serdes_txprecursor_xfi1gt_txprecursor_Shift                                                    16
#define cAf6_Serdes_txprecursor_xfi1gt_txprecursor_MaxVal                                                 0x1f
#define cAf6_Serdes_txprecursor_xfi1gt_txprecursor_MinVal                                                  0x0
#define cAf6_Serdes_txprecursor_xfi1gt_txprecursor_RstVal                                                  0x0

/*--------------------------------------
BitField Name: qsgmii2gt_txprecursor
BitField Type: RW
BitField Desc: QSGMII Port2 txprecursor
BitField Bits: [12:8]
--------------------------------------*/
#define cAf6_Serdes_txprecursor_qsgmii2gt_txprecursor_Bit_Start                                              8
#define cAf6_Serdes_txprecursor_qsgmii2gt_txprecursor_Bit_End                                               12
#define cAf6_Serdes_txprecursor_qsgmii2gt_txprecursor_Mask                                            cBit12_8
#define cAf6_Serdes_txprecursor_qsgmii2gt_txprecursor_Shift                                                  8
#define cAf6_Serdes_txprecursor_qsgmii2gt_txprecursor_MaxVal                                              0x1f
#define cAf6_Serdes_txprecursor_qsgmii2gt_txprecursor_MinVal                                               0x0
#define cAf6_Serdes_txprecursor_qsgmii2gt_txprecursor_RstVal                                               0x0

/*--------------------------------------
BitField Name: qsgmii1gt_txprecursor
BitField Type: RW
BitField Desc: QSGMII Port1 txprecursor
BitField Bits: [4:0]
--------------------------------------*/
#define cAf6_Serdes_txprecursor_qsgmii1gt_txprecursor_Bit_Start                                              0
#define cAf6_Serdes_txprecursor_qsgmii1gt_txprecursor_Bit_End                                                4
#define cAf6_Serdes_txprecursor_qsgmii1gt_txprecursor_Mask                                             cBit4_0
#define cAf6_Serdes_txprecursor_qsgmii1gt_txprecursor_Shift                                                  0
#define cAf6_Serdes_txprecursor_qsgmii1gt_txprecursor_MaxVal                                              0x1f
#define cAf6_Serdes_txprecursor_qsgmii1gt_txprecursor_MinVal                                               0x0
#define cAf6_Serdes_txprecursor_qsgmii1gt_txprecursor_RstVal                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : Serdes txpostcursor
Reg Addr   : 0x00042
Reg Formula: Base Addr + 0x00042
    Where  :
Reg Desc   :
This is the global configuration register for the Global Serdes Control

------------------------------------------------------------------------------*/
#define cAf6Reg_Serdes_txpostcursor_xfi_qsgmii_Base                                                    0x00042
#define cAf6Reg_Serdes_txpostcursor                                                                    0x00042
#define cAf6Reg_Serdes_txpostcursor_WidthVal                                                                32
#define cAf6Reg_Serdes_txpostcursor_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: xfi2gt_txpostcursor
BitField Type: RW
BitField Desc: XFI Port2 txpostcursor
BitField Bits: [28:24]
--------------------------------------*/
#define cAf6_Serdes_txpostcursor_xfi2gt_txpostcursor_Bit_Start                                              24
#define cAf6_Serdes_txpostcursor_xfi2gt_txpostcursor_Bit_End                                                28
#define cAf6_Serdes_txpostcursor_xfi2gt_txpostcursor_Mask                                            cBit28_24
#define cAf6_Serdes_txpostcursor_xfi2gt_txpostcursor_Shift                                                  24
#define cAf6_Serdes_txpostcursor_xfi2gt_txpostcursor_MaxVal                                               0x1f
#define cAf6_Serdes_txpostcursor_xfi2gt_txpostcursor_MinVal                                                0x0
#define cAf6_Serdes_txpostcursor_xfi2gt_txpostcursor_RstVal                                                0x0

/*--------------------------------------
BitField Name: xfi1gt_txpostcursor
BitField Type: RW
BitField Desc: XFI Port1 txpostcursor
BitField Bits: [20:16]
--------------------------------------*/
#define cAf6_Serdes_txpostcursor_xfi1gt_txpostcursor_Bit_Start                                              16
#define cAf6_Serdes_txpostcursor_xfi1gt_txpostcursor_Bit_End                                                20
#define cAf6_Serdes_txpostcursor_xfi1gt_txpostcursor_Mask                                            cBit20_16
#define cAf6_Serdes_txpostcursor_xfi1gt_txpostcursor_Shift                                                  16
#define cAf6_Serdes_txpostcursor_xfi1gt_txpostcursor_MaxVal                                               0x1f
#define cAf6_Serdes_txpostcursor_xfi1gt_txpostcursor_MinVal                                                0x0
#define cAf6_Serdes_txpostcursor_xfi1gt_txpostcursor_RstVal                                                0x0

/*--------------------------------------
BitField Name: qsgmii2gt_txpostcursor
BitField Type: RW
BitField Desc: QSGMII Port2 txpostcursor
BitField Bits: [12:8]
--------------------------------------*/
#define cAf6_Serdes_txpostcursor_qsgmii2gt_txpostcursor_Bit_Start                                            8
#define cAf6_Serdes_txpostcursor_qsgmii2gt_txpostcursor_Bit_End                                             12
#define cAf6_Serdes_txpostcursor_qsgmii2gt_txpostcursor_Mask                                          cBit12_8
#define cAf6_Serdes_txpostcursor_qsgmii2gt_txpostcursor_Shift                                                8
#define cAf6_Serdes_txpostcursor_qsgmii2gt_txpostcursor_MaxVal                                            0x1f
#define cAf6_Serdes_txpostcursor_qsgmii2gt_txpostcursor_MinVal                                             0x0
#define cAf6_Serdes_txpostcursor_qsgmii2gt_txpostcursor_RstVal                                             0x0

/*--------------------------------------
BitField Name: qsgmii1gt_txpostcursor
BitField Type: RW
BitField Desc: QSGMII Port1 txpostcursor
BitField Bits: [4:0]
--------------------------------------*/
#define cAf6_Serdes_txpostcursor_qsgmii1gt_txpostcursor_Bit_Start                                            0
#define cAf6_Serdes_txpostcursor_qsgmii1gt_txpostcursor_Bit_End                                              4
#define cAf6_Serdes_txpostcursor_qsgmii1gt_txpostcursor_Mask                                           cBit4_0
#define cAf6_Serdes_txpostcursor_qsgmii1gt_txpostcursor_Shift                                                0
#define cAf6_Serdes_txpostcursor_qsgmii1gt_txpostcursor_MaxVal                                            0x1f
#define cAf6_Serdes_txpostcursor_qsgmii1gt_txpostcursor_MinVal                                             0x0
#define cAf6_Serdes_txpostcursor_qsgmii1gt_txpostcursor_RstVal                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : Serdes_txprecursor
Reg Addr   : 0x00043
Reg Formula: Base Addr + 0x00043
    Where  :
Reg Desc   :
This is the global configuration register for the Global Serdes Control

------------------------------------------------------------------------------*/
#define cAf6Reg_Serdes_txprecursor_Base                                                                0x00043
#define cAf6Reg_Serdes_txprecursor_WidthVal                                                                 32
#define cAf6Reg_Serdes_txprecursor_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: sfpgt4_txprecursor
BitField Type: RW
BitField Desc: SFP Port4 txprecursor
BitField Bits: [28:24]
--------------------------------------*/
#define cAf6_Serdes_txprecursor_sfpgt4_txprecursor_Bit_Start                                                24
#define cAf6_Serdes_txprecursor_sfpgt4_txprecursor_Bit_End                                                  28
#define cAf6_Serdes_txprecursor_sfpgt4_txprecursor_Mask                                              cBit28_24
#define cAf6_Serdes_txprecursor_sfpgt4_txprecursor_Shift                                                    24
#define cAf6_Serdes_txprecursor_sfpgt4_txprecursor_MaxVal                                                 0x1f
#define cAf6_Serdes_txprecursor_sfpgt4_txprecursor_MinVal                                                  0x0
#define cAf6_Serdes_txprecursor_sfpgt4_txprecursor_RstVal                                                  0x0

/*--------------------------------------
BitField Name: sfpgt3_txprecursor
BitField Type: RW
BitField Desc: SFP Port3 txprecursor
BitField Bits: [20:16]
--------------------------------------*/
#define cAf6_Serdes_txprecursor_sfpgt3_txprecursor_Bit_Start                                                16
#define cAf6_Serdes_txprecursor_sfpgt3_txprecursor_Bit_End                                                  20
#define cAf6_Serdes_txprecursor_sfpgt3_txprecursor_Mask                                              cBit20_16
#define cAf6_Serdes_txprecursor_sfpgt3_txprecursor_Shift                                                    16
#define cAf6_Serdes_txprecursor_sfpgt3_txprecursor_MaxVal                                                 0x1f
#define cAf6_Serdes_txprecursor_sfpgt3_txprecursor_MinVal                                                  0x0
#define cAf6_Serdes_txprecursor_sfpgt3_txprecursor_RstVal                                                  0x0

/*--------------------------------------
BitField Name: sfpgt2_txprecursor
BitField Type: RW
BitField Desc: SFP Port2 txprecursor
BitField Bits: [12:8]
--------------------------------------*/
#define cAf6_Serdes_txprecursor_sfpgt2_txprecursor_Bit_Start                                                 8
#define cAf6_Serdes_txprecursor_sfpgt2_txprecursor_Bit_End                                                  12
#define cAf6_Serdes_txprecursor_sfpgt2_txprecursor_Mask                                               cBit12_8
#define cAf6_Serdes_txprecursor_sfpgt2_txprecursor_Shift                                                     8
#define cAf6_Serdes_txprecursor_sfpgt2_txprecursor_MaxVal                                                 0x1f
#define cAf6_Serdes_txprecursor_sfpgt2_txprecursor_MinVal                                                  0x0
#define cAf6_Serdes_txprecursor_sfpgt2_txprecursor_RstVal                                                  0x0

/*--------------------------------------
BitField Name: sfpgt1_txprecursor
BitField Type: RW
BitField Desc: SFP Port1 txprecursor
BitField Bits: [4:0]
--------------------------------------*/
#define cAf6_Serdes_txprecursor_sfpgt1_txprecursor_Bit_Start                                                 0
#define cAf6_Serdes_txprecursor_sfpgt1_txprecursor_Bit_End                                                   4
#define cAf6_Serdes_txprecursor_sfpgt1_txprecursor_Mask                                                cBit4_0
#define cAf6_Serdes_txprecursor_sfpgt1_txprecursor_Shift                                                     0
#define cAf6_Serdes_txprecursor_sfpgt1_txprecursor_MaxVal                                                 0x1f
#define cAf6_Serdes_txprecursor_sfpgt1_txprecursor_MinVal                                                  0x0
#define cAf6_Serdes_txprecursor_sfpgt1_txprecursor_RstVal                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : Serdes_txpostcursor
Reg Addr   : 0x00044
Reg Formula: Base Addr + 0x00044
    Where  :
Reg Desc   :
This is the global configuration register for the Global Serdes Control

------------------------------------------------------------------------------*/
#define cAf6Reg_Serdes_txpostcursor_sfp_Base                                                               0x00044

/*--------------------------------------
BitField Name: sfpgt4_txpostcursor
BitField Type: RW
BitField Desc: SFP Port4 txpostcursor
BitField Bits: [28:24]
--------------------------------------*/
#define cAf6_Serdes_txpostcursor_sfpgt4_txpostcursor_Bit_Start                                              24
#define cAf6_Serdes_txpostcursor_sfpgt4_txpostcursor_Bit_End                                                28
#define cAf6_Serdes_txpostcursor_sfpgt4_txpostcursor_Mask                                            cBit28_24
#define cAf6_Serdes_txpostcursor_sfpgt4_txpostcursor_Shift                                                  24
#define cAf6_Serdes_txpostcursor_sfpgt4_txpostcursor_MaxVal                                               0x1f
#define cAf6_Serdes_txpostcursor_sfpgt4_txpostcursor_MinVal                                                0x0
#define cAf6_Serdes_txpostcursor_sfpgt4_txpostcursor_RstVal                                                0x0

/*--------------------------------------
BitField Name: sfpgt3_txpostcursor
BitField Type: RW
BitField Desc: SFP Port3 txpostcursor
BitField Bits: [20:16]
--------------------------------------*/
#define cAf6_Serdes_txpostcursor_sfpgt3_txpostcursor_Bit_Start                                              16
#define cAf6_Serdes_txpostcursor_sfpgt3_txpostcursor_Bit_End                                                20
#define cAf6_Serdes_txpostcursor_sfpgt3_txpostcursor_Mask                                            cBit20_16
#define cAf6_Serdes_txpostcursor_sfpgt3_txpostcursor_Shift                                                  16
#define cAf6_Serdes_txpostcursor_sfpgt3_txpostcursor_MaxVal                                               0x1f
#define cAf6_Serdes_txpostcursor_sfpgt3_txpostcursor_MinVal                                                0x0
#define cAf6_Serdes_txpostcursor_sfpgt3_txpostcursor_RstVal                                                0x0

/*--------------------------------------
BitField Name: sfpgt2_txpostcursor
BitField Type: RW
BitField Desc: SFP Port2 txpostcursor
BitField Bits: [12:8]
--------------------------------------*/
#define cAf6_Serdes_txpostcursor_sfpgt2_txpostcursor_Bit_Start                                               8
#define cAf6_Serdes_txpostcursor_sfpgt2_txpostcursor_Bit_End                                                12
#define cAf6_Serdes_txpostcursor_sfpgt2_txpostcursor_Mask                                             cBit12_8
#define cAf6_Serdes_txpostcursor_sfpgt2_txpostcursor_Shift                                                   8
#define cAf6_Serdes_txpostcursor_sfpgt2_txpostcursor_MaxVal                                               0x1f
#define cAf6_Serdes_txpostcursor_sfpgt2_txpostcursor_MinVal                                                0x0
#define cAf6_Serdes_txpostcursor_sfpgt2_txpostcursor_RstVal                                                0x0

/*--------------------------------------
BitField Name: sfpgt1_txpostcursor
BitField Type: RW
BitField Desc: SFP Port1 txpostcursor
BitField Bits: [4:0]
--------------------------------------*/
#define cAf6_Serdes_txpostcursor_sfpgt1_txpostcursor_Bit_Start                                               0
#define cAf6_Serdes_txpostcursor_sfpgt1_txpostcursor_Bit_End                                                 4
#define cAf6_Serdes_txpostcursor_sfpgt1_txpostcursor_Mask                                              cBit4_0
#define cAf6_Serdes_txpostcursor_sfpgt1_txpostcursor_Shift                                                   0
#define cAf6_Serdes_txpostcursor_sfpgt1_txpostcursor_MaxVal                                               0x1f
#define cAf6_Serdes_txpostcursor_sfpgt1_txpostcursor_MinVal                                                0x0
#define cAf6_Serdes_txpostcursor_sfpgt1_txpostcursor_RstVal                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : Serdes txdiffctrl
Reg Addr   : 0x00045
Reg Formula: Base Addr + 0x00045
    Where  :
Reg Desc   :
This is the global configuration register for the Global Serdes Control

------------------------------------------------------------------------------*/
#define cAf6Reg_Serdes_txdiffctrl_xfi_qsgmii_Base                                                      0x00045
#define cAf6Reg_Serdes_txdiffctrl                                                                      0x00045
#define cAf6Reg_Serdes_txdiffctrl_WidthVal                                                                  32
#define cAf6Reg_Serdes_txdiffctrl_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: xfi2gt_txdiffctrl
BitField Type: RW
BitField Desc: XFI Port2 txdiffctrl
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_Serdes_txdiffctrl_xfi2gt_txdiffctrl_Bit_Start                                                  12
#define cAf6_Serdes_txdiffctrl_xfi2gt_txdiffctrl_Bit_End                                                    15
#define cAf6_Serdes_txdiffctrl_xfi2gt_txdiffctrl_Mask                                                cBit15_12
#define cAf6_Serdes_txdiffctrl_xfi2gt_txdiffctrl_Shift                                                      12
#define cAf6_Serdes_txdiffctrl_xfi2gt_txdiffctrl_MaxVal                                                    0xf
#define cAf6_Serdes_txdiffctrl_xfi2gt_txdiffctrl_MinVal                                                    0x0
#define cAf6_Serdes_txdiffctrl_xfi2gt_txdiffctrl_RstVal                                                    0xE

/*--------------------------------------
BitField Name: xfi1gt_txdiffctrl
BitField Type: RW
BitField Desc: XFI Port1 txdiffctrl
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_Serdes_txdiffctrl_xfi1gt_txdiffctrl_Bit_Start                                                   8
#define cAf6_Serdes_txdiffctrl_xfi1gt_txdiffctrl_Bit_End                                                    11
#define cAf6_Serdes_txdiffctrl_xfi1gt_txdiffctrl_Mask                                                 cBit11_8
#define cAf6_Serdes_txdiffctrl_xfi1gt_txdiffctrl_Shift                                                       8
#define cAf6_Serdes_txdiffctrl_xfi1gt_txdiffctrl_MaxVal                                                    0xf
#define cAf6_Serdes_txdiffctrl_xfi1gt_txdiffctrl_MinVal                                                    0x0
#define cAf6_Serdes_txdiffctrl_xfi1gt_txdiffctrl_RstVal                                                    0xE

/*--------------------------------------
BitField Name: qsgmii2gt_txdiffctrl
BitField Type: RW
BitField Desc: QSGMII Port2 txdiffctrl
BitField Bits: [7:4]
--------------------------------------*/
#define cAf6_Serdes_txdiffctrl_qsgmii2gt_txdiffctrl_Bit_Start                                                4
#define cAf6_Serdes_txdiffctrl_qsgmii2gt_txdiffctrl_Bit_End                                                  7
#define cAf6_Serdes_txdiffctrl_qsgmii2gt_txdiffctrl_Mask                                               cBit7_4
#define cAf6_Serdes_txdiffctrl_qsgmii2gt_txdiffctrl_Shift                                                    4
#define cAf6_Serdes_txdiffctrl_qsgmii2gt_txdiffctrl_MaxVal                                                 0xf
#define cAf6_Serdes_txdiffctrl_qsgmii2gt_txdiffctrl_MinVal                                                 0x0
#define cAf6_Serdes_txdiffctrl_qsgmii2gt_txdiffctrl_RstVal                                                 0x8

/*--------------------------------------
BitField Name: qsgmii1gt_txdiffctrl
BitField Type: RW
BitField Desc: QSGMII Port1 txdiffctrl
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_Serdes_txdiffctrl_qsgmii1gt_txdiffctrl_Bit_Start                                                0
#define cAf6_Serdes_txdiffctrl_qsgmii1gt_txdiffctrl_Bit_End                                                  3
#define cAf6_Serdes_txdiffctrl_qsgmii1gt_txdiffctrl_Mask                                               cBit3_0
#define cAf6_Serdes_txdiffctrl_qsgmii1gt_txdiffctrl_Shift                                                    0
#define cAf6_Serdes_txdiffctrl_qsgmii1gt_txdiffctrl_MaxVal                                                 0xf
#define cAf6_Serdes_txdiffctrl_qsgmii1gt_txdiffctrl_MinVal                                                 0x0
#define cAf6_Serdes_txdiffctrl_qsgmii1gt_txdiffctrl_RstVal                                                 0x8


/*------------------------------------------------------------------------------
Reg Name   : Serdes rxlpmen
Reg Addr   : 0x00046
Reg Formula: Base Addr + 0x00046
    Where  :
Reg Desc   :
This is the global configuration register for the Global Serdes Control

------------------------------------------------------------------------------*/
#define cAf6Reg_Serdes_rxlpmen_Base                                                                    0x00046
#define cAf6Reg_Serdes_rxlpmen                                                                         0x00046
#define cAf6Reg_Serdes_rxlpmen_WidthVal                                                                     32
#define cAf6Reg_Serdes_rxlpmen_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: xfi2gt_rxlpmen
BitField Type: RW
BitField Desc: XFI Port2 rxlpmen 0: DFE / 1 : LPM
BitField Bits: [07]
--------------------------------------*/
#define cAf6_Serdes_rxlpmen_xfi2gt_rxlpmen_Bit_Start                                                         7
#define cAf6_Serdes_rxlpmen_xfi2gt_rxlpmen_Bit_End                                                           7
#define cAf6_Serdes_rxlpmen_xfi2gt_rxlpmen_Mask                                                          cBit7
#define cAf6_Serdes_rxlpmen_xfi2gt_rxlpmen_Shift                                                             7
#define cAf6_Serdes_rxlpmen_xfi2gt_rxlpmen_MaxVal                                                          0x1
#define cAf6_Serdes_rxlpmen_xfi2gt_rxlpmen_MinVal                                                          0x0
#define cAf6_Serdes_rxlpmen_xfi2gt_rxlpmen_RstVal                                                          0x0

/*--------------------------------------
BitField Name: xfi1gt_rxlpmen
BitField Type: RW
BitField Desc: XFI Port1 rxlpmen 0: DFE / 1 : LPM
BitField Bits: [06]
--------------------------------------*/
#define cAf6_Serdes_rxlpmen_xfi1gt_rxlpmen_Bit_Start                                                         6
#define cAf6_Serdes_rxlpmen_xfi1gt_rxlpmen_Bit_End                                                           6
#define cAf6_Serdes_rxlpmen_xfi1gt_rxlpmen_Mask                                                          cBit6
#define cAf6_Serdes_rxlpmen_xfi1gt_rxlpmen_Shift                                                             6
#define cAf6_Serdes_rxlpmen_xfi1gt_rxlpmen_MaxVal                                                          0x1
#define cAf6_Serdes_rxlpmen_xfi1gt_rxlpmen_MinVal                                                          0x0
#define cAf6_Serdes_rxlpmen_xfi1gt_rxlpmen_RstVal                                                          0x0

/*--------------------------------------
BitField Name: qsgmii2gt_rxlpmen
BitField Type: RW
BitField Desc: QSGMII Port2 rxlpmen 0: DFE / 1 : LPM
BitField Bits: [05]
--------------------------------------*/
#define cAf6_Serdes_rxlpmen_qsgmii2gt_rxlpmen_Bit_Start                                                      5
#define cAf6_Serdes_rxlpmen_qsgmii2gt_rxlpmen_Bit_End                                                        5
#define cAf6_Serdes_rxlpmen_qsgmii2gt_rxlpmen_Mask                                                       cBit5
#define cAf6_Serdes_rxlpmen_qsgmii2gt_rxlpmen_Shift                                                          5
#define cAf6_Serdes_rxlpmen_qsgmii2gt_rxlpmen_MaxVal                                                       0x1
#define cAf6_Serdes_rxlpmen_qsgmii2gt_rxlpmen_MinVal                                                       0x0
#define cAf6_Serdes_rxlpmen_qsgmii2gt_rxlpmen_RstVal                                                       0x0

/*--------------------------------------
BitField Name: qsgmii1gt_rxlpmen
BitField Type: RW
BitField Desc: QSGMII Port1 rxlpmen 0: DFE / 1 : LPM
BitField Bits: [04]
--------------------------------------*/
#define cAf6_Serdes_rxlpmen_qsgmii1gt_rxlpmen_Bit_Start                                                      4
#define cAf6_Serdes_rxlpmen_qsgmii1gt_rxlpmen_Bit_End                                                        4
#define cAf6_Serdes_rxlpmen_qsgmii1gt_rxlpmen_Mask                                                       cBit4
#define cAf6_Serdes_rxlpmen_qsgmii1gt_rxlpmen_Shift                                                          4
#define cAf6_Serdes_rxlpmen_qsgmii1gt_rxlpmen_MaxVal                                                       0x1
#define cAf6_Serdes_rxlpmen_qsgmii1gt_rxlpmen_MinVal                                                       0x0
#define cAf6_Serdes_rxlpmen_qsgmii1gt_rxlpmen_RstVal                                                       0x0

/*--------------------------------------
BitField Name: sfpgt3_rxlpmen
BitField Type: RW
BitField Desc: SFP Port4 rxlpmen 0: DFE / 1 : LPM
BitField Bits: [03]
--------------------------------------*/
#define cAf6_Serdes_rxlpmen_sfpgt3_rxlpmen_Bit_Start                                                         3
#define cAf6_Serdes_rxlpmen_sfpgt3_rxlpmen_Bit_End                                                           3
#define cAf6_Serdes_rxlpmen_sfpgt3_rxlpmen_Mask                                                          cBit3
#define cAf6_Serdes_rxlpmen_sfpgt3_rxlpmen_Shift                                                             3
#define cAf6_Serdes_rxlpmen_sfpgt3_rxlpmen_MaxVal                                                          0x1
#define cAf6_Serdes_rxlpmen_sfpgt3_rxlpmen_MinVal                                                          0x0
#define cAf6_Serdes_rxlpmen_sfpgt3_rxlpmen_RstVal                                                          0x0

/*--------------------------------------
BitField Name: sfpgt2_rxlpmen
BitField Type: RW
BitField Desc: SFP Port3 rxlpmen 0: DFE / 1 : LPM
BitField Bits: [02]
--------------------------------------*/
#define cAf6_Serdes_rxlpmen_sfpgt2_rxlpmen_Bit_Start                                                         2
#define cAf6_Serdes_rxlpmen_sfpgt2_rxlpmen_Bit_End                                                           2
#define cAf6_Serdes_rxlpmen_sfpgt2_rxlpmen_Mask                                                          cBit2
#define cAf6_Serdes_rxlpmen_sfpgt2_rxlpmen_Shift                                                             2
#define cAf6_Serdes_rxlpmen_sfpgt2_rxlpmen_MaxVal                                                          0x1
#define cAf6_Serdes_rxlpmen_sfpgt2_rxlpmen_MinVal                                                          0x0
#define cAf6_Serdes_rxlpmen_sfpgt2_rxlpmen_RstVal                                                          0x0

/*--------------------------------------
BitField Name: sfpgt1_rxlpmen
BitField Type: RW
BitField Desc: SFP Port2 rxlpmen 0: DFE / 1 : LPM
BitField Bits: [01]
--------------------------------------*/
#define cAf6_Serdes_rxlpmen_sfpgt1_rxlpmen_Bit_Start                                                         1
#define cAf6_Serdes_rxlpmen_sfpgt1_rxlpmen_Bit_End                                                           1
#define cAf6_Serdes_rxlpmen_sfpgt1_rxlpmen_Mask                                                          cBit1
#define cAf6_Serdes_rxlpmen_sfpgt1_rxlpmen_Shift                                                             1
#define cAf6_Serdes_rxlpmen_sfpgt1_rxlpmen_MaxVal                                                          0x1
#define cAf6_Serdes_rxlpmen_sfpgt1_rxlpmen_MinVal                                                          0x0
#define cAf6_Serdes_rxlpmen_sfpgt1_rxlpmen_RstVal                                                          0x0

/*--------------------------------------
BitField Name: sfpgt0_rxlpmen
BitField Type: RW
BitField Desc: SFP Port1 rxlpmen 0: DFE / 1 : LPM
BitField Bits: [00]
--------------------------------------*/
#define cAf6_Serdes_rxlpmen_sfpgt0_rxlpmen_Bit_Start                                                         0
#define cAf6_Serdes_rxlpmen_sfpgt0_rxlpmen_Bit_End                                                           0
#define cAf6_Serdes_rxlpmen_sfpgt0_rxlpmen_Mask                                                          cBit0
#define cAf6_Serdes_rxlpmen_sfpgt0_rxlpmen_Shift                                                             0
#define cAf6_Serdes_rxlpmen_sfpgt0_rxlpmen_MaxVal                                                          0x1
#define cAf6_Serdes_rxlpmen_sfpgt0_rxlpmen_MinVal                                                          0x0
#define cAf6_Serdes_rxlpmen_sfpgt0_rxlpmen_RstVal                                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : Serdes gt_loopback
Reg Addr   : 0x00047
Reg Formula: Base Addr + 0x00047
    Where  :
Reg Desc   :
This is the global configuration register for the Global Serdes Control

------------------------------------------------------------------------------*/
#define cAf6Reg_Serdes_gt_loopback_Base                                                                0x00047
#define cAf6Reg_Serdes_gt_loopback                                                                     0x00047
#define cAf6Reg_Serdes_gt_loopback_WidthVal                                                                 32
#define cAf6Reg_Serdes_gt_loopback_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: xfi2gt_loopback
BitField Type: RW
BitField Desc: Loopback for XFI Serdes Port 2, The recommended do reset TX/RX
PMA after Loop/unloop serdes 000: Release operation 001: Near-End PCS Loopback
010: Near-End PMA Loopback 100: Far-End PMA Loopback 110: Far-End PCS Loopback
BitField Bits: [14:12]
--------------------------------------*/
#define cAf6_Serdes_gt_loopback_xfi2gt_loopback_Bit_Start                                                   12
#define cAf6_Serdes_gt_loopback_xfi2gt_loopback_Bit_End                                                     14
#define cAf6_Serdes_gt_loopback_xfi2gt_loopback_Mask                                                 cBit14_12
#define cAf6_Serdes_gt_loopback_xfi2gt_loopback_Shift                                                       12
#define cAf6_Serdes_gt_loopback_xfi2gt_loopback_MaxVal                                                     0x7
#define cAf6_Serdes_gt_loopback_xfi2gt_loopback_MinVal                                                     0x0
#define cAf6_Serdes_gt_loopback_xfi2gt_loopback_RstVal                                                     0x0

/*--------------------------------------
BitField Name: xfi1gt_loopback
BitField Type: RW
BitField Desc: Loopback for XFI Serdes Port 1, The recommended do reset TX/RX
PMA after Loop/unloop serdes 000: Release operation 001: Near-End PCS Loopback
010: Near-End PMA Loopback 100: Far-End PMA Loopback 110: Far-End PCS Loopback
BitField Bits: [10:8]
--------------------------------------*/
#define cAf6_Serdes_gt_loopback_xfi1gt_loopback_Bit_Start                                                    8
#define cAf6_Serdes_gt_loopback_xfi1gt_loopback_Bit_End                                                     10
#define cAf6_Serdes_gt_loopback_xfi1gt_loopback_Mask                                                  cBit10_8
#define cAf6_Serdes_gt_loopback_xfi1gt_loopback_Shift                                                        8
#define cAf6_Serdes_gt_loopback_xfi1gt_loopback_MaxVal                                                     0x7
#define cAf6_Serdes_gt_loopback_xfi1gt_loopback_MinVal                                                     0x0
#define cAf6_Serdes_gt_loopback_xfi1gt_loopback_RstVal                                                     0x0

/*--------------------------------------
BitField Name: qsgmii2gt_loopback
BitField Type: RW
BitField Desc: Loopback for QSGMII Serdes Port 2, The recommended do reset TX/RX
PMA after Loop/unloop serdes 000: Release operation 001: Near-End PCS Loopback
010: Near-End PMA Loopback 100: Far-End PMA Loopback 110: Far-End PCS Loopback
BitField Bits: [6:4]
--------------------------------------*/
#define cAf6_Serdes_gt_loopback_qsgmii2gt_loopback_Bit_Start                                                 4
#define cAf6_Serdes_gt_loopback_qsgmii2gt_loopback_Bit_End                                                   6
#define cAf6_Serdes_gt_loopback_qsgmii2gt_loopback_Mask                                                cBit6_4
#define cAf6_Serdes_gt_loopback_qsgmii2gt_loopback_Shift                                                     4
#define cAf6_Serdes_gt_loopback_qsgmii2gt_loopback_MaxVal                                                  0x7
#define cAf6_Serdes_gt_loopback_qsgmii2gt_loopback_MinVal                                                  0x0
#define cAf6_Serdes_gt_loopback_qsgmii2gt_loopback_RstVal                                                  0x0

/*--------------------------------------
BitField Name: qsgmii1gt_loopback
BitField Type: RW
BitField Desc: Loopback for QSGMII Serdes Port 1, The recommended do reset TX/RX
PMA after Loop/unloop serdes 000: Release operation 001: Near-End PCS Loopback
010: Near-End PMA Loopback 100: Far-End PMA Loopback 110: Far-End PCS Loopback
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_Serdes_gt_loopback_qsgmii1gt_loopback_Bit_Start                                                 0
#define cAf6_Serdes_gt_loopback_qsgmii1gt_loopback_Bit_End                                                   2
#define cAf6_Serdes_gt_loopback_qsgmii1gt_loopback_Mask                                                cBit2_0
#define cAf6_Serdes_gt_loopback_qsgmii1gt_loopback_Shift                                                     0
#define cAf6_Serdes_gt_loopback_qsgmii1gt_loopback_MaxVal                                                  0x7
#define cAf6_Serdes_gt_loopback_qsgmii1gt_loopback_MinVal                                                  0x0
#define cAf6_Serdes_gt_loopback_qsgmii1gt_loopback_RstVal                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : Serdes_txdiffctrl
Reg Addr   : 0x00048
Reg Formula: Base Addr + 0x00048
    Where  :
Reg Desc   :
This is the global configuration register for the Global Serdes Control

------------------------------------------------------------------------------*/
#define cAf6Reg_Serdes_txdiffctrl_sfp_Base                                                                 0x00048

/*--------------------------------------
BitField Name: sfpgt4_txdiffctrl
BitField Type: RW
BitField Desc: SFP Port4 txdiffctrl
BitField Bits: [28:24]
--------------------------------------*/
#define cAf6_Serdes_txdiffctrl_sfpgt4_txdiffctrl_Bit_Start                                                  24
#define cAf6_Serdes_txdiffctrl_sfpgt4_txdiffctrl_Bit_End                                                    28
#define cAf6_Serdes_txdiffctrl_sfpgt4_txdiffctrl_Mask                                                cBit28_24
#define cAf6_Serdes_txdiffctrl_sfpgt4_txdiffctrl_Shift                                                      24
#define cAf6_Serdes_txdiffctrl_sfpgt4_txdiffctrl_MaxVal                                                   0x1f
#define cAf6_Serdes_txdiffctrl_sfpgt4_txdiffctrl_MinVal                                                    0x0
#define cAf6_Serdes_txdiffctrl_sfpgt4_txdiffctrl_RstVal                                                   0x18

/*--------------------------------------
BitField Name: sfpgt3_txdiffctrl
BitField Type: RW
BitField Desc: SFP Port3 txdiffctrl
BitField Bits: [20:16]
--------------------------------------*/
#define cAf6_Serdes_txdiffctrl_sfpgt3_txdiffctrl_Bit_Start                                                  16
#define cAf6_Serdes_txdiffctrl_sfpgt3_txdiffctrl_Bit_End                                                    20
#define cAf6_Serdes_txdiffctrl_sfpgt3_txdiffctrl_Mask                                                cBit20_16
#define cAf6_Serdes_txdiffctrl_sfpgt3_txdiffctrl_Shift                                                      16
#define cAf6_Serdes_txdiffctrl_sfpgt3_txdiffctrl_MaxVal                                                   0x1f
#define cAf6_Serdes_txdiffctrl_sfpgt3_txdiffctrl_MinVal                                                    0x0
#define cAf6_Serdes_txdiffctrl_sfpgt3_txdiffctrl_RstVal                                                   0x18

/*--------------------------------------
BitField Name: sfpgt2_txdiffctrl
BitField Type: RW
BitField Desc: SFP Port2 txdiffctrl
BitField Bits: [12:8]
--------------------------------------*/
#define cAf6_Serdes_txdiffctrl_sfpgt2_txdiffctrl_Bit_Start                                                   8
#define cAf6_Serdes_txdiffctrl_sfpgt2_txdiffctrl_Bit_End                                                    12
#define cAf6_Serdes_txdiffctrl_sfpgt2_txdiffctrl_Mask                                                 cBit12_8
#define cAf6_Serdes_txdiffctrl_sfpgt2_txdiffctrl_Shift                                                       8
#define cAf6_Serdes_txdiffctrl_sfpgt2_txdiffctrl_MaxVal                                                   0x1f
#define cAf6_Serdes_txdiffctrl_sfpgt2_txdiffctrl_MinVal                                                    0x0
#define cAf6_Serdes_txdiffctrl_sfpgt2_txdiffctrl_RstVal                                                   0x18

/*--------------------------------------
BitField Name: sfpgt1_txdiffctrl
BitField Type: RW
BitField Desc: SFP Port1 txdiffctrl
BitField Bits: [4:0]
--------------------------------------*/
#define cAf6_Serdes_txdiffctrl_sfpgt1_txdiffctrl_Bit_Start                                                   0
#define cAf6_Serdes_txdiffctrl_sfpgt1_txdiffctrl_Bit_End                                                     4
#define cAf6_Serdes_txdiffctrl_sfpgt1_txdiffctrl_Mask                                                  cBit4_0
#define cAf6_Serdes_txdiffctrl_sfpgt1_txdiffctrl_Shift                                                       0
#define cAf6_Serdes_txdiffctrl_sfpgt1_txdiffctrl_MaxVal                                                   0x1f
#define cAf6_Serdes_txdiffctrl_sfpgt1_txdiffctrl_MinVal                                                    0x0
#define cAf6_Serdes_txdiffctrl_sfpgt1_txdiffctrl_RstVal                                                   0x18


/*------------------------------------------------------------------------------
Reg Name   : Serdes Turning Status 0
Reg Addr   : 0x00060
Reg Formula:
    Where  :
Reg Desc   :
This is status PMA reset for Serdes

------------------------------------------------------------------------------*/
#define cAf6Reg_Serdes_Turning_Status_Base                                                            0x00060

/*--------------------------------------
BitField Name: xfi2gt_txresetdone
BitField Type: RO
BitField Desc: XFI Port 2 tx reset done 1 : Reset Done 0 : Reset not complete
BitField Bits: [15]
--------------------------------------*/
#define cAf6_xfi2gt_txresetdone_Bit_Start                                                                  15
#define cAf6_xfi2gt_txresetdone_Bit_End                                                                    15
#define cAf6_xfi2gt_txresetdone_Mask                                                                   cBit15
#define cAf6_xfi2gt_txresetdone_Shift                                                                      15
#define cAf6_xfi2gt_txresetdone_MaxVal                                                                    0x1
#define cAf6_xfi2gt_txresetdone_MinVal                                                                    0x0
#define cAf6_xfi2gt_txresetdone_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: xfi1gt_txresetdone
BitField Type: RO
BitField Desc: XFI Port 1 tx reset done 1 : Reset Done 0 : Reset not complete
BitField Bits: [14]
--------------------------------------*/
#define cAf6_xfi1gt_txresetdone_Bit_Start                                                                  14
#define cAf6_xfi1gt_txresetdone_Bit_End                                                                    14
#define cAf6_xfi1gt_txresetdone_Mask                                                                   cBit14
#define cAf6_xfi1gt_txresetdone_Shift                                                                      14
#define cAf6_xfi1gt_txresetdone_MaxVal                                                                    0x1
#define cAf6_xfi1gt_txresetdone_MinVal                                                                    0x0
#define cAf6_xfi1gt_txresetdone_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: qsgmii2gt_txresetdone
BitField Type: RO
BitField Desc: QSGMII Port 2 tx reset done 1 : Reset Done 0 : Reset not complete
BitField Bits: [13]
--------------------------------------*/
#define cAf6_qsgmii2gt_txresetdone_Bit_Start                                                               13
#define cAf6_qsgmii2gt_txresetdone_Bit_End                                                                 13
#define cAf6_qsgmii2gt_txresetdone_Mask                                                                cBit13
#define cAf6_qsgmii2gt_txresetdone_Shift                                                                   13
#define cAf6_qsgmii2gt_txresetdone_MaxVal                                                                 0x1
#define cAf6_qsgmii2gt_txresetdone_MinVal                                                                 0x0
#define cAf6_qsgmii2gt_txresetdone_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: qsgmii1gt_txresetdone
BitField Type: RO
BitField Desc: QSGMII Port 1 tx reset done 1 : Reset Done 0 : Reset not complete
BitField Bits: [12]
--------------------------------------*/
#define cAf6_qsgmii1gt_txresetdone_Bit_Start                                                               12
#define cAf6_qsgmii1gt_txresetdone_Bit_End                                                                 12
#define cAf6_qsgmii1gt_txresetdone_Mask                                                                cBit12
#define cAf6_qsgmii1gt_txresetdone_Shift                                                                   12
#define cAf6_qsgmii1gt_txresetdone_MaxVal                                                                 0x1
#define cAf6_qsgmii1gt_txresetdone_MinVal                                                                 0x0
#define cAf6_qsgmii1gt_txresetdone_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: sfpgt3_txresetdone
BitField Type: RO
BitField Desc: SFP Port 4 tx reset done 1 : Reset Done 0 : Reset not complete
BitField Bits: [11]
--------------------------------------*/
#define cAf6_sfpgt4_txresetdone_Bit_Start                                                                  11
#define cAf6_sfpgt4_txresetdone_Bit_End                                                                    11
#define cAf6_sfpgt4_txresetdone_Mask                                                                   cBit11
#define cAf6_sfpgt4_txresetdone_Shift                                                                      11
#define cAf6_sfpgt4_txresetdone_MaxVal                                                                    0x1
#define cAf6_sfpgt4_txresetdone_MinVal                                                                    0x0
#define cAf6_sfpgt4_txresetdone_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: sfpgt3_txresetdone
BitField Type: RO
BitField Desc: SFP Port 3 tx reset done 1 : Reset Done 0 : Reset not complete
BitField Bits: [10]
--------------------------------------*/
#define cAf6_sfpgt3_txresetdone_Bit_Start                                                                  10
#define cAf6_sfpgt3_txresetdone_Bit_End                                                                    10
#define cAf6_sfpgt3_txresetdone_Mask                                                                   cBit10
#define cAf6_sfpgt3_txresetdone_Shift                                                                      10
#define cAf6_sfpgt3_txresetdone_MaxVal                                                                    0x1
#define cAf6_sfpgt3_txresetdone_MinVal                                                                    0x0
#define cAf6_sfpgt3_txresetdone_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: sfpgt2_txresetdone
BitField Type: RO
BitField Desc: SFP Port 1 tx reset done 1 : Reset Done 0 : Reset not complete
BitField Bits: [9]
--------------------------------------*/
#define cAf6_sfpgt2_txresetdone_Bit_Start                                                                   9
#define cAf6_sfpgt2_txresetdone_Bit_End                                                                     9
#define cAf6_sfpgt2_txresetdone_Mask                                                                    cBit9
#define cAf6_sfpgt2_txresetdone_Shift                                                                       9
#define cAf6_sfpgt2_txresetdone_MaxVal                                                                    0x1
#define cAf6_sfpgt2_txresetdone_MinVal                                                                    0x0
#define cAf6_sfpgt2_txresetdone_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: sfpgt1_txresetdone
BitField Type: RO
BitField Desc: SFP Port 1 tx reset done 1 : Reset Done 0 : Reset not complete
BitField Bits: [8]
--------------------------------------*/
#define cAf6_sfpgt1_txresetdone_Bit_Start                                                                   8
#define cAf6_sfpgt1_txresetdone_Bit_End                                                                     8
#define cAf6_sfpgt1_txresetdone_Mask                                                                    cBit8
#define cAf6_sfpgt1_txresetdone_Shift                                                                       8
#define cAf6_sfpgt1_txresetdone_MaxVal                                                                    0x1
#define cAf6_sfpgt1_txresetdone_MinVal                                                                    0x0
#define cAf6_sfpgt1_txresetdone_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: xfi2gt_rxresetdone
BitField Type: RO
BitField Desc: XFI Port 2 RX reset done 1 : Reset Done 0 : Reset not complete
BitField Bits: [7]
--------------------------------------*/
#define cAf6_xfi2gt_rxresetdone_Bit_Start                                                                   7
#define cAf6_xfi2gt_rxresetdone_Bit_End                                                                     7
#define cAf6_xfi2gt_rxresetdone_Mask                                                                    cBit7
#define cAf6_xfi2gt_rxresetdone_Shift                                                                       7
#define cAf6_xfi2gt_rxresetdone_MaxVal                                                                    0x1
#define cAf6_xfi2gt_rxresetdone_MinVal                                                                    0x0
#define cAf6_xfi2gt_rxresetdone_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: xfi1gt_rxresetdone
BitField Type: RO
BitField Desc: XFI Port 1 RX reset done 1 : Reset Done 0 : Reset not complete
BitField Bits: [6]
--------------------------------------*/
#define cAf6_xfi1gt_rxresetdone_Bit_Start                                                                   6
#define cAf6_xfi1gt_rxresetdone_Bit_End                                                                     6
#define cAf6_xfi1gt_rxresetdone_Mask                                                                    cBit6
#define cAf6_xfi1gt_rxresetdone_Shift                                                                       6
#define cAf6_xfi1gt_rxresetdone_MaxVal                                                                    0x1
#define cAf6_xfi1gt_rxresetdone_MinVal                                                                    0x0
#define cAf6_xfi1gt_rxresetdone_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: qsgmii2gt_rxresetdone
BitField Type: RO
BitField Desc: QSGMII Port 2 RX reset done 1 : Reset Done 0 : Reset not complete
BitField Bits: [5]
--------------------------------------*/
#define cAf6_qsgmii2gt_rxresetdone_Bit_Start                                                                5
#define cAf6_qsgmii2gt_rxresetdone_Bit_End                                                                  5
#define cAf6_qsgmii2gt_rxresetdone_Mask                                                                 cBit5
#define cAf6_qsgmii2gt_rxresetdone_Shift                                                                    5
#define cAf6_qsgmii2gt_rxresetdone_MaxVal                                                                 0x1
#define cAf6_qsgmii2gt_rxresetdone_MinVal                                                                 0x0
#define cAf6_qsgmii2gt_rxresetdone_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: qsgmii1gt_rxresetdone
BitField Type: RO
BitField Desc: QSGMII Port 1 RX reset done 1 : Reset Done 0 : Reset not complete
BitField Bits: [4]
--------------------------------------*/
#define cAf6_qsgmii1gt_rxresetdone_Bit_Start                                                                4
#define cAf6_qsgmii1gt_rxresetdone_Bit_End                                                                  4
#define cAf6_qsgmii1gt_rxresetdone_Mask                                                                 cBit4
#define cAf6_qsgmii1gt_rxresetdone_Shift                                                                    4
#define cAf6_qsgmii1gt_rxresetdone_MaxVal                                                                 0x1
#define cAf6_qsgmii1gt_rxresetdone_MinVal                                                                 0x0
#define cAf6_qsgmii1gt_rxresetdone_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: sfpgt3_rxresetdone
BitField Type: RO
BitField Desc: SFP Port 4 RX reset done 1 : Reset Done 0 : Reset not complete
BitField Bits: [3]
--------------------------------------*/
#define cAf6_sfpgt4_rxresetdone_Bit_Start                                                                   3
#define cAf6_sfpgt4_rxresetdone_Bit_End                                                                     3
#define cAf6_sfpgt4_rxresetdone_Mask                                                                    cBit3
#define cAf6_sfpgt4_rxresetdone_Shift                                                                       3
#define cAf6_sfpgt4_rxresetdone_MaxVal                                                                    0x1
#define cAf6_sfpgt4_rxresetdone_MinVal                                                                    0x0
#define cAf6_sfpgt4_rxresetdone_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: sfpgt3_rxresetdone
BitField Type: RO
BitField Desc: SFP Port 3 RX reset done 1 : Reset Done 0 : Reset not complete
BitField Bits: [2]
--------------------------------------*/
#define cAf6_sfpgt3_rxresetdone_Bit_Start                                                                   2
#define cAf6_sfpgt3_rxresetdone_Bit_End                                                                     2
#define cAf6_sfpgt3_rxresetdone_Mask                                                                    cBit2
#define cAf6_sfpgt3_rxresetdone_Shift                                                                       2
#define cAf6_sfpgt3_rxresetdone_MaxVal                                                                    0x1
#define cAf6_sfpgt3_rxresetdone_MinVal                                                                    0x0
#define cAf6_sfpgt3_rxresetdone_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: sfpgt2_rxresetdone
BitField Type: RO
BitField Desc: SFP Port 1 RX reset done 1 : Reset Done 0 : Reset not complete
BitField Bits: [1]
--------------------------------------*/
#define cAf6_sfpgt2_rxresetdone_Bit_Start                                                                   1
#define cAf6_sfpgt2_rxresetdone_Bit_End                                                                     1
#define cAf6_sfpgt2_rxresetdone_Mask                                                                    cBit1
#define cAf6_sfpgt2_rxresetdone_Shift                                                                       1
#define cAf6_sfpgt2_rxresetdone_MaxVal                                                                    0x1
#define cAf6_sfpgt2_rxresetdone_MinVal                                                                    0x0
#define cAf6_sfpgt2_rxresetdone_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: sfpgt1_rxresetdone
BitField Type: RO
BitField Desc: SFP Port 1 RX reset done 1 : Reset Done 0 : Reset not complete
BitField Bits: [0]
--------------------------------------*/
#define cAf6_sfpgt1_rxresetdone_Bit_Start                                                                   0
#define cAf6_sfpgt1_rxresetdone_Bit_End                                                                     0
#define cAf6_sfpgt1_rxresetdone_Mask                                                                    cBit0
#define cAf6_sfpgt1_rxresetdone_Shift                                                                       0
#define cAf6_sfpgt1_rxresetdone_MaxVal                                                                    0x1
#define cAf6_sfpgt1_rxresetdone_MinVal                                                                    0x0
#define cAf6_sfpgt1_rxresetdone_RstVal                                                                    0x0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60210061SERDESTUNINGREG_H_ */


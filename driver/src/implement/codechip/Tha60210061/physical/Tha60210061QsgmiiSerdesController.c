/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha60210061QsgmiiSerdesController.c
 *
 * Created Date: Jun 22, 2016
 *
 * Description : QSGMII SERDES controller of 60210061
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/versionreader/ThaVersionReader.h"
#include "../../../default/man/ThaDevice.h"
#include "../prbs/Tha60210061ModulePrbs.h"
#include "../eth/Tha60210061ModuleEth.h"
#include "Tha60210061SerdesInternal.h"
#include "Tha60210061SerdesManager.h"
#include "Tha60210061DrpReg.h"
#include "Tha60210061SerdesTuningReg.h"
#include "Tha60210061TopCtrReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6Reg_Qsgmii_Drp_BaseAddress 0xf57000
#define cResetTriggerTimeInMs 200
#define cNumLanes 4
/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSerdesControllerMethods m_AtSerdesControllerOverride;
static tTha6021EthPortSerdesControllerMethods m_Tha6021EthPortSerdesControllerOverride;

static const tAtSerdesControllerMethods *m_AtSerdesControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModulePrbs ModulePrbs(AtSerdesController self)
    {
    AtDevice device = AtChannelDeviceGet(AtSerdesControllerPhysicalPortGet(self));
    return (AtModulePrbs)AtDeviceModuleGet(device, cAtModulePrbs);
    }

static AtModuleEth ModuleEth(AtSerdesController self)
    {
    AtDevice device = AtChannelDeviceGet(AtSerdesControllerPhysicalPortGet(self));
    return (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    }

static eBool DiagPrbsEngineIsSupported(AtSerdesController self)
    {
    /* HW remove prbs engine to have resource for Ethernet pass through */
    return (Tha60210061EthPassThroughIsSupported(ModuleEth(self))) ? cAtFalse : cAtTrue;
    }

static AtPrbsEngine PrbsEngineCreate(AtSerdesController self)
    {
    AtModulePrbs modulePrbs = ModulePrbs(self);

    if (Tha60210061ModulePrbsRawPrsbIsSupported(modulePrbs))
        return Tha60210061GthSerdesRawPrbsEngineNew(self);

    if (Tha60210061ModulePrbsEthFramePrbsIsSupported(modulePrbs))
        return Tha60210061SerdesEthFramePrbsEngineNew(self);

    if (!DiagPrbsEngineIsSupported(self))
        return NULL;

    if (Tha60210061ModulePrbsSgmiiQsgmiiPerPortControl(modulePrbs))
        return Tha60210061QsgmiiSerdesPrbsEngineV2New(self);

    return Tha60210061QsgmiiSerdesPrbsEngineNew(self);
    }

static eBool IsControllable(AtSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static AtMdio Mdio(AtSerdesController self)
    {
    /* This kind of SERDES do not support MDIO */
    AtUnused(self);
    return NULL;
    }

static uint32 QsgmiiSerdesId(AtSerdesController self)
    {
    return (uint32)(AtSerdesControllerIdGet(self) - cTha60210061QsgmiiSerdesId0);
    }

static eBool EyeScanIsSupported(AtSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 EyeScanDrpBaseAddress(AtSerdesController self)
    {
    uint32 hwId = QsgmiiSerdesId((AtSerdesController)self);
    return cAf6Reg_Qsgmii_Drp_BaseAddress + (hwId * 0x400UL);
    }

static AtEyeScanController EyeScanControllerObjectCreate(AtSerdesController self, uint32 drpBaseAddress)
    {
    return Tha60210061EyeScanControllerGthE3New(self, drpBaseAddress);
    }

static uint32 TxPreCursorAddress(Tha6021EthPortSerdesController self)
    {
    AtUnused(self);
    return (uint32)(cAf6Reg_SerdesTurning_BaseAddress + cAf6Reg_Serdes_txprecursor_xfi_qsgmii_Base);
    }

static uint32 PreCursorMask(Tha6021EthPortSerdesController self, uint32 serdesId)
    {
    uint32 hwId = QsgmiiSerdesId((AtSerdesController)self);
    AtUnused(serdesId);
    return (uint32)(cAf6_Serdes_txprecursor_qsgmii1gt_txprecursor_Mask << (hwId * 8UL));
    }

static uint32 PreCursorShift(Tha6021EthPortSerdesController self, uint32 serdesId)
    {
    uint32 hwId = QsgmiiSerdesId((AtSerdesController)self);
    AtUnused(serdesId);
    return (uint32)(cAf6_Serdes_txprecursor_qsgmii1gt_txprecursor_Shift + (hwId * 8UL));
    }

static uint32 TxPostCursorAddress(Tha6021EthPortSerdesController self)
    {
    AtUnused(self);
    return (uint32)(cAf6Reg_SerdesTurning_BaseAddress + cAf6Reg_Serdes_txpostcursor_xfi_qsgmii_Base);
    }

static uint32 TxDiffCtrlAddress(Tha6021EthPortSerdesController self)
    {
    AtUnused(self);
    return (uint32)(cAf6Reg_SerdesTurning_BaseAddress + cAf6Reg_Serdes_txdiffctrl_xfi_qsgmii_Base);
    }

static uint32 DiffCtrlMask(Tha6021EthPortSerdesController self, uint32 serdesId)
    {
    uint32 hwId = QsgmiiSerdesId((AtSerdesController)self);
    AtUnused(serdesId);
    return (uint32)(cAf6_Serdes_txdiffctrl_qsgmii1gt_txdiffctrl_Mask << (hwId * 4UL));
    }

static uint32 DiffCtrlShift(Tha6021EthPortSerdesController self, uint32 serdesId)
    {
    uint32 hwId = QsgmiiSerdesId((AtSerdesController)self);
    AtUnused(serdesId);
    return (uint32)(cAf6_Serdes_txdiffctrl_qsgmii1gt_txdiffctrl_Shift + (hwId * 4UL));
    }

static uint32 LpmEnableRegister(Tha6021EthPortSerdesController self)
    {
    AtUnused(self);
    return (uint32)(cAf6Reg_SerdesTurning_BaseAddress + cAf6Reg_Serdes_rxlpmen_Base);
    }

static uint32 LpmEnableMask(Tha6021EthPortSerdesController self, uint32 serdesId)
    {
    uint32 hwId = QsgmiiSerdesId((AtSerdesController)self);
    AtUnused(serdesId);
    return (uint32)(cAf6_Serdes_rxlpmen_qsgmii1gt_rxlpmen_Mask << (hwId));
    }

static uint32 LpmEnableShift(Tha6021EthPortSerdesController self, uint32 serdesId)
    {
    uint32 hwId = QsgmiiSerdesId((AtSerdesController)self);
    AtUnused(serdesId);
    return (uint32)(cAf6_Serdes_rxlpmen_qsgmii1gt_rxlpmen_Shift + hwId);
    }

static eAtRet CoefReConfigure(Tha6021EthPortSerdesController self, eAtSerdesEqualizerMode mode)
    {
    AtUnused(self);
    AtUnused(mode);
    return cAtOk;
    }

static uint32 LoopbackAddress(AtSerdesController self)
    {
    AtUnused(self);
    return (uint32)(cAf6Reg_SerdesTurning_BaseAddress + cAf6Reg_Serdes_gt_loopback_Base);
    }

static uint32 LoopbackMask(AtSerdesController self)
    {
    uint32 hwId = QsgmiiSerdesId((AtSerdesController)self);
    return (uint32)(cAf6_Serdes_gt_loopback_qsgmii1gt_loopback_Mask << (hwId * 4UL));
    }

static uint32 LoopbackShift(AtSerdesController self)
    {
    uint32 hwId = QsgmiiSerdesId((AtSerdesController)self);
    return (uint32)(cAf6_Serdes_gt_loopback_qsgmii1gt_loopback_Shift + (hwId * 4UL));
    }

static eBool CanLoopback(AtSerdesController self, eAtLoopbackMode loopbackMode, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    switch ((uint32)loopbackMode)
        {
        case cAtLoopbackModeLocal:
            return cAtTrue;
        case cAtLoopbackModeRemote:
            return cAtTrue;
        case cAtLoopbackModeRelease:
            return cAtTrue;
        default:
            return cAtFalse;
        }
    }

static eAtRet HwLoopbackModeSet(AtSerdesController self, uint8 hwLoopbackMode)
    {
    uint32 address = LoopbackAddress(self);
    uint32 regVal = AtSerdesControllerRead(self, address, cAtModuleSdh);

    mFieldIns(&regVal, LoopbackMask(self), LoopbackShift(self), hwLoopbackMode);
    AtSerdesControllerWrite(self, address, regVal, cAtModuleSdh);

    return cAtOk;
    }

static uint8 HwLoopbackModeGet(AtSerdesController self)
    {
    uint32 regVal = AtSerdesControllerRead(self, LoopbackAddress(self), cAtModuleSdh);
    uint8 hwLoopbackMode;

    mFieldGet(regVal, LoopbackMask(self), LoopbackShift(self), uint8, &hwLoopbackMode);

    return hwLoopbackMode;
    }

static eAtRet LoopbackEnable(AtSerdesController self, eAtLoopbackMode loopbackMode, eBool enable)
    {
    return Tha60210061SerdesLoopbackEnable(self, loopbackMode, enable);
    }

static eBool LoopbackIsEnabled(AtSerdesController self, eAtLoopbackMode loopbackMode)
    {
    return Tha60210061SerdesLoopbackIsEnabled(self, loopbackMode);
    }

static eBool EqualizerCanControl(AtSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool EqualizerModeIsSupported(AtSerdesController self, eAtSerdesEqualizerMode mode)
    {
    AtUnused(self);
    if ((mode == cAtSerdesEqualizerModeDfe) || (mode == cAtSerdesEqualizerModeLpm))
        return cAtTrue;
    return cAtFalse;
    }

static eAtRet Reset(AtSerdesController self)
    {
    uint32 serdesId  = AtSerdesControllerIdGet(self);
    uint32 offset    = (serdesId - cTha60210061QsgmiiSerdesId0);
    uint32 regAddr   = cAf6Reg_SerdesTurning_BaseAddress + cAf6Reg_Serdes_PMA_Reset_Base;
    uint32 resetMask = cAf6_Serdes_PMA_Reset_qsgmii1gt_txpmareset_Mask << offset |
                       cAf6_Serdes_PMA_Reset_qsgmii1gt_rxpmareset_Mask << offset;

    return Tha60210061SerdesResetWithMask(self, regAddr, resetMask, cDontNeedToWaitResetDone, cDontNeedToWaitResetDone);
    }

static eAtRet LpmDfeReset(Tha6021EthPortSerdesController self)
    {
    AtSerdesController controller = (AtSerdesController)self;
    uint32 serdesId  = AtSerdesControllerIdGet(controller);
    uint32 regAddr   = cAf6Reg_SerdesTurning_BaseAddress + cAf6Reg_Serdes_PMA_Reset_Base;
    uint32 offset    = (serdesId - cTha60210061QsgmiiSerdesId0);
    uint32 resetMask = cAf6_Serdes_PMA_Reset_qsgmii1gt_rxpmareset_Mask << offset;

    return Tha60210061SerdesResetWithMask(controller, regAddr, resetMask, cDontNeedToWaitResetDone, cDontNeedToWaitResetDone);
    }

static eAtRet LayerLoopbackSet(AtSerdesController self, eAtSerdesPhysicalLayer layer, eAtLoopbackMode loopbackMode)
    {
    uint32 serdesId = AtSerdesControllerIdGet(self);
    uint32 regAddr = 0;
    uint32 regVal = 0;
    uint32 mask = 0;
    uint32 shift = 0;
    uint32 offset = ((serdesId - cTha60210061QsgmiiSerdesId0) * 4);

    if ((serdesId < cTha60210061QsgmiiSerdesId0) && (serdesId > cTha60210061QsgmiiSerdesId1))
        return cAtErrorOutOfRangParm;

    regAddr = cAf6Reg_SerdesTurning_BaseAddress + cAf6Reg_Serdes_gt_loopback_Base;
    mask = cAf6_Serdes_gt_loopback_qsgmii1gt_loopback_Mask << offset;
    shift = cAf6_Serdes_gt_loopback_qsgmii1gt_loopback_Shift + offset;

    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    switch (loopbackMode)
        {
        case cAtLoopbackModeLocal:
            mFieldIns(&regVal, mask, shift, (layer == cAtSerdesPhysicalLayerPcs) ? cAf6SerdesLoopbackNearEndPcs : cAf6SerdesLoopbackNearEndPma);
            break;
        case cAtLoopbackModeRemote:
            mFieldIns(&regVal, mask, shift, (layer == cAtSerdesPhysicalLayerPcs) ? cAf6SerdesLoopbackFarEndPcs : cAf6SerdesLoopbackFarEndPma);
            break;
        case cAtLoopbackModeRelease:
            mFieldIns(&regVal, mask, shift, 0x0);
            break;
        case cAtLoopbackModeDual:
        default:
            return cAtErrorModeNotSupport;
        }

    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);
    return cAtOk;
    }

static const char* Description(AtSerdesController self)
    {
    AtUnused(self);
    return "QSGMII";
    }

static uint8 HwSerdesIdFromSwId(uint32 serdeId)
    {
    if (serdeId == cTha60210061QsgmiiSerdesId0)
        return 0;

    /* Serdes#2 */
    return 1;
    }

static eAtRet TxAllLanesEnable(AtSerdesController self, eBool enable)
    {
    uint32 address, regVal;
    uint32 lane_i, serdesId;

    serdesId = HwSerdesIdFromSwId(AtSerdesControllerIdGet(self));
    address = cAf6Reg_o_control1 + cTha60210061TopBaseAddress;
    regVal = AtSerdesControllerRead(self, address, cAtModuleEth);
    for (lane_i = 0; lane_i < cNumLanes; lane_i++)
        {
        mFieldIns(&regVal, cThaReg_QSGMII_Mac0_Tx_Enable_Mask(serdesId, lane_i), cThaReg_QSGMII_Mac0_Tx_Enable_Shift(serdesId, lane_i), mBoolToBin(enable));
        }

    AtSerdesControllerWrite(self, address, regVal, cAtModuleEth);

    return cAtOk;
    }

static eBool TxAllLanesIsEnabled(AtSerdesController self)
    {
    uint32 address, regVal;
    uint32 lane_i, serdesId;

    serdesId = HwSerdesIdFromSwId(AtSerdesControllerIdGet(self));
    address = cAf6Reg_o_control1 + cTha60210061TopBaseAddress;
    regVal = AtSerdesControllerRead(self, address, cAtModuleEth);
    for (lane_i = 0; lane_i < cNumLanes; lane_i++)
        {
        uint8 hwEnable;

        mFieldGet(regVal, cThaReg_QSGMII_Mac0_Tx_Enable_Mask(serdesId, lane_i), cThaReg_QSGMII_Mac0_Tx_Enable_Shift(serdesId, lane_i), uint8, &hwEnable);
        if (hwEnable)
            return cAtTrue;
        }

    return cAtFalse;
    }

static eAtRet Enable(AtSerdesController self, eBool enable)
    {
    return TxAllLanesEnable(self, enable);
    }

static eBool IsEnabled(AtSerdesController self)
    {
    return TxAllLanesIsEnabled(self);
    }

static eBool CanEnable(AtSerdesController self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtTrue;
    }

static eAtRet Init(AtSerdesController self)
    {
    eAtRet ret = m_AtSerdesControllerMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return AtSerdesControllerEqualizerModeSet(self, cAtSerdesEqualizerModeLpm);
    }

static eAtSerdesLinkStatus LinkStatus(AtSerdesController self)
    {
    AtUnused(self);
    return cAtSerdesLinkStatusUnknown;
    }

static eBool PllIsLocked(AtSerdesController self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 NumLanes(AtSerdesController self)
    {
    /* Disable accessing lanes until there is requirement and HW support */
    AtUnused(self);
    return 0;
    }

static void OverrideTha6021EthPortSerdesController(AtSerdesController self)
    {
    Tha6021EthPortSerdesController controller = (Tha6021EthPortSerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6021EthPortSerdesControllerOverride, mMethodsGet(controller), sizeof(m_Tha6021EthPortSerdesControllerOverride));

        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, TxPreCursorAddress);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, TxPostCursorAddress);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, PreCursorMask);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, PreCursorShift);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, TxDiffCtrlAddress);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, DiffCtrlMask);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, DiffCtrlShift);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, LpmEnableRegister);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, LpmEnableMask);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, LpmEnableShift);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, CoefReConfigure);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, HwLoopbackModeSet);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, HwLoopbackModeGet);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, LpmDfeReset);
        }

    mMethodsSet(controller, &m_Tha6021EthPortSerdesControllerOverride);
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSerdesControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, m_AtSerdesControllerMethods, sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, PrbsEngineCreate);
        mMethodOverride(m_AtSerdesControllerOverride, IsControllable);
        mMethodOverride(m_AtSerdesControllerOverride, Mdio);
        mMethodOverride(m_AtSerdesControllerOverride, CanLoopback);
        mMethodOverride(m_AtSerdesControllerOverride, LoopbackEnable);
        mMethodOverride(m_AtSerdesControllerOverride, LoopbackIsEnabled);
        mMethodOverride(m_AtSerdesControllerOverride, LayerLoopbackSet);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerCanControl);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerModeIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, EyeScanIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, EyeScanControllerObjectCreate);
        mMethodOverride(m_AtSerdesControllerOverride, EyeScanDrpBaseAddress);
        mMethodOverride(m_AtSerdesControllerOverride, Description);
        mMethodOverride(m_AtSerdesControllerOverride, Enable);
        mMethodOverride(m_AtSerdesControllerOverride, IsEnabled);
        mMethodOverride(m_AtSerdesControllerOverride, CanEnable);
        mMethodOverride(m_AtSerdesControllerOverride, Init);
        mMethodOverride(m_AtSerdesControllerOverride, LinkStatus);
        mMethodOverride(m_AtSerdesControllerOverride, PllIsLocked);
        mMethodOverride(m_AtSerdesControllerOverride, NumLanes);
        mMethodOverride(m_AtSerdesControllerOverride, Reset);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtSerdesController(self);
    OverrideTha6021EthPortSerdesController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061QsgmiiSerdesController);
    }

AtSerdesController Tha60210061QsgmiiSerdesControllerObjectInit(AtSerdesController self, AtEthPort ethPort, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6021EthPortQsgmiiSerdesControllerObjectInit(self, ethPort, serdesId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController Tha60210061QsgmiiSerdesControllerNew(AtEthPort ethPort, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60210061QsgmiiSerdesControllerObjectInit(newController, ethPort, serdesId);
    }

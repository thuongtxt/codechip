/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha60210061EyeScanControllerGthE3.c
 *
 * Created Date: Aug 30, 2016
 *
 * Description : Eye scan controller
 *
 * Notes       : GTH-576:XFI/QSGMII
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/physical/AtEyeScanControllerInternal.h"
#include "../../../../generic/physical/AtSerdesControllerInternal.h"
#include "Tha60210061SerdesInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061EyeScanControllerGthE3
    {
    tAtEyeScanControllerGtyE3 super;
    }tTha60210061EyeScanControllerGthE3;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtEyeScanControllerMethods m_AtEyeScanControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtEyeScanLane LaneObjectCreate(AtEyeScanController self, uint8 laneId)
    {
    return AtEyeScanLaneFastV2New(self, laneId);
    }

static uint16 ES_CONTROL_STATUS_REG(AtEyeScanController self)
    {
    AtUnused(self);
    return 0x153;
    }

static uint16 ES_ERROR_COUNT_REG(AtEyeScanController self)
    {
    AtUnused(self);
    return 0x151;
    }

static uint16 ES_SAMPLE_COUNT_REG(AtEyeScanController self)
    {
    AtUnused(self);
    return 0x152;
    }

static eAtRet PmaReset(AtEyeScanController self)
    {
    return AtSerdesControllerReset(AtEyeScanControllerSerdesControllerGet(self));
    }

static float SpeedInGbps(AtEyeScanController self, AtEyeScanLane lane)
    {
    AtUnused(lane);
    return AtSerdesControllerSpeedInGbps(AtEyeScanControllerSerdesControllerGet(self));
    }

static uint32 TotalWidth(AtEyeScanController self)
    {
    AtUnused(self);
    return 64;
    }

static AtSerdesController Serdes(AtEyeScanController self)
    {
    return AtEyeScanControllerSerdesControllerGet(self);
    }

static eAtEyeScanEqualizerMode EqualizerModeGet(AtEyeScanController self)
    {
    eAtSerdesEqualizerMode serdesMode = AtSerdesControllerEqualizerModeGet(Serdes(self));
    if (serdesMode == cAtSerdesEqualizerModeDfe)
        return cAtEyeScanEqualizerModeDfe;
    return cAtEyeScanEqualizerModeLpm;
    }

static eAtRet EqualizerModeSet(AtEyeScanController self, eAtEyeScanEqualizerMode mode)
    {
    AtSerdesController serdes = Serdes(self);
    eAtSerdesEqualizerMode serdesMode = cAtSerdesEqualizerModeDfe;

    if (mode == cAtEyeScanEqualizerModeLpm)
        serdesMode = cAtSerdesEqualizerModeLpm;

    AtSerdesControllerEqualizerModeSet(serdes, serdesMode);
    return mMethodsGet(self)->PmaReset(self);
    }

static void OverrideAtEyeScanController(AtEyeScanController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtEyeScanControllerOverride, mMethodsGet(self), sizeof(m_AtEyeScanControllerOverride));

        mMethodOverride(m_AtEyeScanControllerOverride, LaneObjectCreate);
        mMethodOverride(m_AtEyeScanControllerOverride, PmaReset);
        mMethodOverride(m_AtEyeScanControllerOverride, SpeedInGbps);
        mMethodOverride(m_AtEyeScanControllerOverride, TotalWidth);
        mMethodOverride(m_AtEyeScanControllerOverride, EqualizerModeSet);
        mMethodOverride(m_AtEyeScanControllerOverride, EqualizerModeGet);
        mMethodOverride(m_AtEyeScanControllerOverride, ES_CONTROL_STATUS_REG);
        mMethodOverride(m_AtEyeScanControllerOverride, ES_ERROR_COUNT_REG);
        mMethodOverride(m_AtEyeScanControllerOverride, ES_SAMPLE_COUNT_REG);
        }

    mMethodsSet(self, &m_AtEyeScanControllerOverride);
    }

static void Override(AtEyeScanController self)
    {
    OverrideAtEyeScanController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061EyeScanControllerGthE3);
    }

static AtEyeScanController ObjectInit(AtEyeScanController self, AtSerdesController serdesController, uint32 drpBaseAddress)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtEyeScanControllerGthE3ObjectInit(self, serdesController, drpBaseAddress) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEyeScanController Tha60210061EyeScanControllerGthE3New(AtSerdesController serdesController, uint32 drpBaseAddress)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEyeScanController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newController, serdesController, drpBaseAddress);
    }

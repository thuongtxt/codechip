/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha60210061TopCtrReg.h
 * 
 * Created Date: Jun 30, 2016
 *
 * Description : 60210061 top registers
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061TOPCTRREG_H_
#define _THA60210061TOPCTRREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cTha60210061TopBaseAddress          0xF00000

/*------------------------------------------------------------------------------
Reg Name   : OCN/1Gbe Global Serdes Control 1
Reg Addr   : 0x00040
Reg Formula: Base Addr + 0x00040
    Where  :
Reg Desc   :
This is the global configuration register for the Global Serdes Control

------------------------------------------------------------------------------*/
#define cAf6Reg_o_control0_Base                                                                        0x00040
#define cAf6Reg_o_control0                                                                           0x00040UL
#define cAf6Reg_o_control0_WidthVal                                                                         32
#define cAf6Reg_o_control0_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: SerdesModeP4
BitField Type: RW
BitField Desc: Serdes mode for SFP port lines 4 0: 1Gbe mode 1: STM mode
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_o_control0_SerdesModeP4_Bit_Start                                                              12
#define cAf6_o_control0_SerdesModeP4_Bit_End                                                                12
#define cAf6_o_control0_SerdesModeP4_Mask                                                               cBit12
#define cAf6_o_control0_SerdesModeP4_Shift                                                                  12
#define cAf6_o_control0_SerdesModeP4_MaxVal                                                                0x1
#define cAf6_o_control0_SerdesModeP4_MinVal                                                                0x0
#define cAf6_o_control0_SerdesModeP4_RstVal                                                                0x0

/*--------------------------------------
BitField Name: SerdesModeP3
BitField Type: RW
BitField Desc: Serdes mode for SFP port lines 3 0: 1Gbe mode 1: STM mode
BitField Bits: [8:8]
--------------------------------------*/
#define cAf6_o_control0_SerdesModeP3_Bit_Start                                                               8
#define cAf6_o_control0_SerdesModeP3_Bit_End                                                                 8
#define cAf6_o_control0_SerdesModeP3_Mask                                                                cBit8
#define cAf6_o_control0_SerdesModeP3_Shift                                                                   8
#define cAf6_o_control0_SerdesModeP3_MaxVal                                                                0x1
#define cAf6_o_control0_SerdesModeP3_MinVal                                                                0x0
#define cAf6_o_control0_SerdesModeP3_RstVal                                                                0x0

/*--------------------------------------
BitField Name: SerdesModeP2
BitField Type: RW
BitField Desc: Serdes mode for SFP port lines 2 0: 1Gbe mode 1: STM mode
BitField Bits: [4:4]
--------------------------------------*/
#define cAf6_o_control0_SerdesModeP2_Bit_Start                                                               4
#define cAf6_o_control0_SerdesModeP2_Bit_End                                                                 4
#define cAf6_o_control0_SerdesModeP2_Mask                                                                cBit4
#define cAf6_o_control0_SerdesModeP2_Shift                                                                   4
#define cAf6_o_control0_SerdesModeP2_MaxVal                                                                0x1
#define cAf6_o_control0_SerdesModeP2_MinVal                                                                0x0
#define cAf6_o_control0_SerdesModeP2_RstVal                                                                0x0

/*--------------------------------------
BitField Name: SerdesModeP1
BitField Type: RW
BitField Desc: Serdes mode for SFP port lines 1 0: 1Gbe mode 1: STM mode
BitField Bits: [0:0]
--------------------------------------*/
#define cAf6_o_control0_SerdesModeP1_Bit_Start                                                               0
#define cAf6_o_control0_SerdesModeP1_Bit_End                                                                 0
#define cAf6_o_control0_SerdesModeP1_Mask                                                                cBit0
#define cAf6_o_control0_SerdesModeP1_Shift                                                                   0
#define cAf6_o_control0_SerdesModeP1_MaxVal                                                                0x1
#define cAf6_o_control0_SerdesModeP1_MinVal                                                                0x0
#define cAf6_o_control0_SerdesModeP1_RstVal                                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : Control 1
Reg Addr   : 0x00041
Reg Formula: Base Addr + 0x00041
    Where  :
Reg Desc   :
This is the global configuration register for the Global Serdes Control

------------------------------------------------------------------------------*/
#define cAf6Reg_o_control1_Base                                                                        0x00041
#define cAf6Reg_o_control1                                                                           0x00041UL
#define cAf6Reg_o_control1_WidthVal                                                                         32
#define cAf6Reg_o_control1_WriteMask                                                                       0x0

#define cThaReg_QSGMII_Mac0_Tx_Enable_Mask(serdesId, laneId)  (cBit16 << (((serdesId) * 4) + (laneId)))
#define cThaReg_QSGMII_Mac0_Tx_Enable_Shift(serdesId, laneId) (16 + ((serdesId) * 4) + (laneId))

#define cThaReg_QSGMII_Mac0_Rx_Sel_Mask            cBit12
#define cThaReg_QSGMII_Mac0_Rx_Sel_Shift           12

#define cThaReg_XFI_Mac0_Sel_Mask                  cBit9_8
#define cThaReg_XFI_Mac0_Sel_Shift                 8

#define cThaReg_XFI_Mac0_XFI0_Tx_Enable_Mask       cBit0
#define cThaReg_XFI_Mac0_XFI0_Tx_Enable_Shift      0
#define cThaReg_XFI_Mac0_XFI1_Tx_Enable_Mask       cBit2
#define cThaReg_XFI_Mac0_XFI1_Tx_Enable_Shift      2

#define cThaReg_XFI_Mac0_XAUI0_Tx_Enable_Mask      cBit1
#define cThaReg_XFI_Mac0_XAUI0_Tx_Enable_Shift     1
#define cThaReg_XFI_Mac0_XAUI1_Tx_Enable_Mask      cBit3
#define cThaReg_XFI_Mac0_XAUI1_Tx_Enable_Shift     3

/*------------------------------------------------------------------------------
Reg Name   : CPLD E1 Loopback  Control
Reg Addr   : 0x00042
Reg Formula: Base Addr + 0x00042
    Where  :
Reg Desc   :
This is the global configuration register for the CPLD E1 Loopback  Control

------------------------------------------------------------------------------*/
#define cAf6Reg_o_control2_Base                                                                        0x00042
#define cAf6Reg_o_control2                                                                           0x00042UL
#define cAf6Reg_o_control2_WidthVal                                                                         32
#define cAf6Reg_o_control2_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: FarEndLoop
BitField Type: RW
BitField Desc: E1 Far End Loopback for CPLD 0: Release 1: Loopback
BitField Bits: [4:4]
--------------------------------------*/
#define cAf6_o_control2_FarEndLoop_Bit_Start                                                                 4
#define cAf6_o_control2_FarEndLoop_Bit_End                                                                   4
#define cAf6_o_control2_FarEndLoop_Mask                                                                  cBit4
#define cAf6_o_control2_FarEndLoop_Shift                                                                     4
#define cAf6_o_control2_FarEndLoop_MaxVal                                                                  0x1
#define cAf6_o_control2_FarEndLoop_MinVal                                                                  0x0
#define cAf6_o_control2_FarEndLoop_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: NearEndLoop
BitField Type: RW
BitField Desc: E1 Near End Loopback for CPLD 0: Release 1: Loopback
BitField Bits: [0:0]
--------------------------------------*/
#define cAf6_o_control2_NearEndLoop_Bit_Start                                                                0
#define cAf6_o_control2_NearEndLoop_Bit_End                                                                  0
#define cAf6_o_control2_NearEndLoop_Mask                                                                 cBit0
#define cAf6_o_control2_NearEndLoop_Shift                                                                    0
#define cAf6_o_control2_NearEndLoop_MaxVal                                                                 0x1
#define cAf6_o_control2_NearEndLoop_MinVal                                                                 0x0
#define cAf6_o_control2_NearEndLoop_RstVal                                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : MIG (DDR/QDR) Diagnostic Control
Reg Addr   : 0x00043
Reg Formula: Base Addr + 0x00043
    Where  :
Reg Desc   :
This is the global configuration register for the MIG (DDR/QDR) Diagnostic Control

------------------------------------------------------------------------------*/
#define cAf6Reg_o_control3_Base                                                                        0x00043
#define cAf6Reg_o_control3                                                                           0x00043UL
#define cAf6Reg_o_control3_WidthVal                                                                         32
#define cAf6Reg_o_control3_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: XFI DiagEn
BitField Type: RW
BitField Desc: Enable Daignostic for XFI interface 0: Disable 1: Enable
BitField Bits: [19]
--------------------------------------*/
#define cAf6_o_control3_XFI_DiagEn_Bit_Start                                                                19
#define cAf6_o_control3_XFI_DiagEn_Bit_End                                                                  19
#define cAf6_o_control3_XFI_DiagEn_Mask                                                                 cBit19
#define cAf6_o_control3_XFI_DiagEn_Shift                                                                    19
#define cAf6_o_control3_XFI_DiagEn_MaxVal                                                                  0x1
#define cAf6_o_control3_XFI_DiagEn_MinVal                                                                  0x0
#define cAf6_o_control3_XFI_DiagEn_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: QSGMII DiagEn
BitField Type: RW
BitField Desc: Enable Daignostic for QSGMII interface 0: Disable 1: Enable
BitField Bits: [18]
--------------------------------------*/
#define cAf6_o_control3_QSGMII_DiagEn_Bit_Start                                                             18
#define cAf6_o_control3_QSGMII_DiagEn_Bit_End                                                               18
#define cAf6_o_control3_QSGMII_DiagEn_Mask                                                              cBit18
#define cAf6_o_control3_QSGMII_DiagEn_Shift                                                                 18
#define cAf6_o_control3_QSGMII_DiagEn_MaxVal                                                               0x1
#define cAf6_o_control3_QSGMII_DiagEn_MinVal                                                               0x0
#define cAf6_o_control3_QSGMII_DiagEn_RstVal                                                               0x0

/*--------------------------------------
BitField Name: SFP OCN DiagEn
BitField Type: RW
BitField Desc: Enable Daignostic for SFP OCN  interface 0: Disable 1: Enable
BitField Bits: [17]
--------------------------------------*/
#define cAf6_o_control3_SFP_OCN_DiagEn_Bit_Start                                                            17
#define cAf6_o_control3_SFP_OCN_DiagEn_Bit_End                                                              17
#define cAf6_o_control3_SFP_OCN_DiagEn_Mask                                                             cBit17
#define cAf6_o_control3_SFP_OCN_DiagEn_Shift                                                                17
#define cAf6_o_control3_SFP_OCN_DiagEn_MaxVal                                                              0x1
#define cAf6_o_control3_SFP_OCN_DiagEn_MinVal                                                              0x0
#define cAf6_o_control3_SFP_OCN_DiagEn_RstVal                                                              0x0

/*--------------------------------------
BitField Name: SFP SGMII DiagEn
BitField Type: RW
BitField Desc: Enable Daignostic for SFP SGMII interface 0: Disable 1: Enable
BitField Bits: [16]
--------------------------------------*/
#define cAf6_o_control3_SFP_SGMII_DiagEn_Bit_Start                                                          16
#define cAf6_o_control3_SFP_SGMII_DiagEn_Bit_End                                                            16
#define cAf6_o_control3_SFP_SGMII_DiagEn_Mask                                                           cBit16
#define cAf6_o_control3_SFP_SGMII_DiagEn_Shift                                                              16
#define cAf6_o_control3_SFP_SGMII_DiagEn_MaxVal                                                            0x1
#define cAf6_o_control3_SFP_SGMII_DiagEn_MinVal                                                            0x0
#define cAf6_o_control3_SFP_SGMII_DiagEn_RstVal                                                            0x0

/*--------------------------------------
BitField Name: QDR1 DiagEn
BitField Type: RW
BitField Desc: Enable Daignostic for QDR#1 0: Disable 1: Enable
BitField Bits: [6]
--------------------------------------*/
#define cAf6_o_control3_QDR1_DiagEn_Bit_Start                                                                6
#define cAf6_o_control3_QDR1_DiagEn_Bit_End                                                                  6
#define cAf6_o_control3_QDR1_DiagEn_Mask                                                                 cBit6
#define cAf6_o_control3_QDR1_DiagEn_Shift                                                                    6
#define cAf6_o_control3_QDR1_DiagEn_MaxVal                                                                 0x1
#define cAf6_o_control3_QDR1_DiagEn_MinVal                                                                 0x0
#define cAf6_o_control3_QDR1_DiagEn_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: DDR2 DiagEn
BitField Type: RW
BitField Desc: Enable Daignostic for DDR#2 0: Disable 1: Enable
BitField Bits: [5]
--------------------------------------*/
#define cAf6_o_control3_DDR2_DiagEn_Bit_Start                                                                5
#define cAf6_o_control3_DDR2_DiagEn_Bit_End                                                                  5
#define cAf6_o_control3_DDR2_DiagEn_Mask                                                                 cBit5
#define cAf6_o_control3_DDR2_DiagEn_Shift                                                                    5
#define cAf6_o_control3_DDR2_DiagEn_MaxVal                                                                 0x1
#define cAf6_o_control3_DDR2_DiagEn_MinVal                                                                 0x0
#define cAf6_o_control3_DDR2_DiagEn_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: DDR1 DiagEn
BitField Type: RW
BitField Desc: Enable Daignostic for DDR#1 0: Disable 1: Enable
BitField Bits: [4]
--------------------------------------*/
#define cAf6_o_control3_DDR1_DiagEn_Bit_Start                                                                4
#define cAf6_o_control3_DDR1_DiagEn_Bit_End                                                                  4
#define cAf6_o_control3_DDR1_DiagEn_Mask                                                                 cBit4
#define cAf6_o_control3_DDR1_DiagEn_Shift                                                                    4
#define cAf6_o_control3_DDR1_DiagEn_MaxVal                                                                 0x1
#define cAf6_o_control3_DDR1_DiagEn_MinVal                                                                 0x0
#define cAf6_o_control3_DDR1_DiagEn_RstVal                                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : Control 4
Reg Addr   : 0x00044
Reg Formula: Base Addr + 0x00044
    Where  :
Reg Desc   :
This is the global configuration register for the Global Serdes Control

------------------------------------------------------------------------------*/
#define cAf6Reg_o_control4_Base                                                                        0x00044
#define cAf6Reg_o_control4                                                                           0x00044UL
#define cAf6Reg_o_control4_WidthVal                                                                         32
#define cAf6Reg_o_control4_WriteMask                                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN/1Gbe Global Serdes Control 2
Reg Addr   : 0x00045
Reg Formula: Base Addr + 0x00045
    Where  :
Reg Desc   :
This is the global configuration register for the SFP Serder 1Gbe/OCN Reset and Loopback control 2

------------------------------------------------------------------------------*/
#define cAf6Reg_o_control5_Base                                                                        0x00045
#define cAf6Reg_o_control5                                                                           0x00045UL
#define cAf6Reg_o_control5_WidthVal                                                                         32
#define cAf6Reg_o_control5_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: RxCdr_hold4
BitField Type: RW
BitField Desc: To get the CDR to lock to reference for OC3 (only) mode
    {1} :  CDR to lock to reference
    {0} :  CDR Locked to Data %% RW  %% 0x0 %% 0x0
BitField Bits: [31]
--------------------------------------*/
#define cAf6_o_control5_RxCdr_hold4_Mask                                                                 cBit31
#define cAf6_o_control5_RxCdr_hold4_Shift                                                                31

/*--------------------------------------
BitField Name: SerdesLoopP4
BitField Type: RW
BitField Desc: Loopback for SFP Serdes Port 4, The recommended do reset TX/RX
PMA after Loop/unloop serdes 000: Normal operation 001: Near-End PCS Loopback
010: Near-End PMA Loopback 100: Far-End PMA Loopback 110: Far-End PCS Loopback
BitField Bits: [30:28]
--------------------------------------*/
#define cAf6_o_control5_SerdesLoopP4_Bit_Start                                                              28
#define cAf6_o_control5_SerdesLoopP4_Bit_End                                                                30
#define cAf6_o_control5_SerdesLoopP4_Mask                                                            cBit30_28
#define cAf6_o_control5_SerdesLoopP4_Shift                                                                  28
#define cAf6_o_control5_SerdesLoopP4_MaxVal                                                                0x7
#define cAf6_o_control5_SerdesLoopP4_MinVal                                                                0x0
#define cAf6_o_control5_SerdesLoopP4_RstVal                                                                0x0

/*--------------------------------------
BitField Name: RxPcsRstP4
BitField Type: RW
BitField Desc: Reset RX PCS Serdes for SFP Port 4,The recommended do reset after
change paraneter and Loop/unloop serdes 0: Release 1: Reset
BitField Bits: [27]
--------------------------------------*/
#define cAf6_o_control5_RxPcsRstP4_Bit_Start                                                                27
#define cAf6_o_control5_RxPcsRstP4_Bit_End                                                                  27
#define cAf6_o_control5_RxPcsRstP4_Mask                                                                 cBit27
#define cAf6_o_control5_RxPcsRstP4_Shift                                                                    27
#define cAf6_o_control5_RxPcsRstP4_MaxVal                                                                  0x1
#define cAf6_o_control5_RxPcsRstP4_MinVal                                                                  0x0
#define cAf6_o_control5_RxPcsRstP4_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: RxPmaRstP4
BitField Type: RW
BitField Desc: Reset RX PMA Serdes for SFP Port 4,The recommended do reset after
change paraneter and Loop/unloop serdes 0: Release 1: Reset
BitField Bits: [26]
--------------------------------------*/
#define cAf6_o_control5_RxPmaRstP4_Bit_Start                                                                26
#define cAf6_o_control5_RxPmaRstP4_Bit_End                                                                  26
#define cAf6_o_control5_RxPmaRstP4_Mask                                                                 cBit26
#define cAf6_o_control5_RxPmaRstP4_Shift                                                                    26
#define cAf6_o_control5_RxPmaRstP4_MaxVal                                                                  0x1
#define cAf6_o_control5_RxPmaRstP4_MinVal                                                                  0x0
#define cAf6_o_control5_RxPmaRstP4_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: TxPcsRstP4
BitField Type: RW
BitField Desc: Reset TX PCS Serdes for SFP Port 4,The recommended do reset after
change paraneter and Loop/unloop serdes 0: Release 1: Reset
BitField Bits: [25]
--------------------------------------*/
#define cAf6_o_control5_TxPcsRstP4_Bit_Start                                                                25
#define cAf6_o_control5_TxPcsRstP4_Bit_End                                                                  25
#define cAf6_o_control5_TxPcsRstP4_Mask                                                                 cBit25
#define cAf6_o_control5_TxPcsRstP4_Shift                                                                    25
#define cAf6_o_control5_TxPcsRstP4_MaxVal                                                                  0x1
#define cAf6_o_control5_TxPcsRstP4_MinVal                                                                  0x0
#define cAf6_o_control5_TxPcsRstP4_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: TxPmaRstP4
BitField Type: RW
BitField Desc: Reset TX PMA Serdes for SFP Port 4,The recommended do reset after
change paraneter and Loop/unloop serdes 0: Release 1: Reset
BitField Bits: [24]
--------------------------------------*/
#define cAf6_o_control5_TxPmaRstP4_Bit_Start                                                                24
#define cAf6_o_control5_TxPmaRstP4_Bit_End                                                                  24
#define cAf6_o_control5_TxPmaRstP4_Mask                                                                 cBit24
#define cAf6_o_control5_TxPmaRstP4_Shift                                                                    24
#define cAf6_o_control5_TxPmaRstP4_MaxVal                                                                  0x1
#define cAf6_o_control5_TxPmaRstP4_MinVal                                                                  0x0
#define cAf6_o_control5_TxPmaRstP4_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: RxCdr_hold3
BitField Type: RW
BitField Desc: To get the CDR to lock to reference for OC3 (only) mode
    {1} :  CDR to lock to reference
    {0} :  CDR Locked to Data %% RW  %% 0x0 %% 0x0
BitField Bits: [15]
--------------------------------------*/
#define cAf6_o_control5_RxCdr_hold3_Mask                                                                 cBit23
#define cAf6_o_control5_RxCdr_hold3_Shift                                                                23

/*--------------------------------------
BitField Name: SerdesLoopP3
BitField Type: RW
BitField Desc: Loopback for SFP Serdes Port 3, The recommended do reset TX/RX
PMA after Loop/unloop serdes 000: Normal operation 001: Near-End PCS Loopback
010: Near-End PMA Loopback 100: Far-End PMA Loopback 110: Far-End PCS Loopback
BitField Bits: [22:20]
--------------------------------------*/
#define cAf6_o_control5_SerdesLoopP3_Bit_Start                                                              20
#define cAf6_o_control5_SerdesLoopP3_Bit_End                                                                22
#define cAf6_o_control5_SerdesLoopP3_Mask                                                            cBit22_20
#define cAf6_o_control5_SerdesLoopP3_Shift                                                                  20
#define cAf6_o_control5_SerdesLoopP3_MaxVal                                                                0x7
#define cAf6_o_control5_SerdesLoopP3_MinVal                                                                0x0
#define cAf6_o_control5_SerdesLoopP3_RstVal                                                                0x0

/*--------------------------------------
BitField Name: RxPcsRstP3
BitField Type: RW
BitField Desc: Reset RX PCS Serdes for SFP Port 3,The recommended do reset after
change paraneter and Loop/unloop serdes 0: Release 1: Reset
BitField Bits: [19]
--------------------------------------*/
#define cAf6_o_control5_RxPcsRstP3_Bit_Start                                                                19
#define cAf6_o_control5_RxPcsRstP3_Bit_End                                                                  19
#define cAf6_o_control5_RxPcsRstP3_Mask                                                                 cBit19
#define cAf6_o_control5_RxPcsRstP3_Shift                                                                    19
#define cAf6_o_control5_RxPcsRstP3_MaxVal                                                                  0x1
#define cAf6_o_control5_RxPcsRstP3_MinVal                                                                  0x0
#define cAf6_o_control5_RxPcsRstP3_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: RxPmaRstP3
BitField Type: RW
BitField Desc: Reset RX PMA Serdes for SFP Port 3,The recommended do reset after
change paraneter and Loop/unloop serdes 0: Release 1: Reset
BitField Bits: [18]
--------------------------------------*/
#define cAf6_o_control5_RxPmaRstP3_Bit_Start                                                                18
#define cAf6_o_control5_RxPmaRstP3_Bit_End                                                                  18
#define cAf6_o_control5_RxPmaRstP3_Mask                                                                 cBit18
#define cAf6_o_control5_RxPmaRstP3_Shift                                                                    18
#define cAf6_o_control5_RxPmaRstP3_MaxVal                                                                  0x1
#define cAf6_o_control5_RxPmaRstP3_MinVal                                                                  0x0
#define cAf6_o_control5_RxPmaRstP3_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: TxPcsRstP3
BitField Type: RW
BitField Desc: Reset TX PCS Serdes for SFP Port 3,The recommended do reset after
change paraneter and Loop/unloop serdes 0: Release 1: Reset
BitField Bits: [17]
--------------------------------------*/
#define cAf6_o_control5_TxPcsRstP3_Bit_Start                                                                17
#define cAf6_o_control5_TxPcsRstP3_Bit_End                                                                  17
#define cAf6_o_control5_TxPcsRstP3_Mask                                                                 cBit17
#define cAf6_o_control5_TxPcsRstP3_Shift                                                                    17
#define cAf6_o_control5_TxPcsRstP3_MaxVal                                                                  0x1
#define cAf6_o_control5_TxPcsRstP3_MinVal                                                                  0x0
#define cAf6_o_control5_TxPcsRstP3_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: TxPmaRstP3
BitField Type: RW
BitField Desc: Reset TX PMA Serdes for SFP Port 3,The recommended do reset after
change paraneter and Loop/unloop serdes 0: Release 1: Reset
BitField Bits: [16]
--------------------------------------*/
#define cAf6_o_control5_TxPmaRstP3_Bit_Start                                                                16
#define cAf6_o_control5_TxPmaRstP3_Bit_End                                                                  16
#define cAf6_o_control5_TxPmaRstP3_Mask                                                                 cBit16
#define cAf6_o_control5_TxPmaRstP3_Shift                                                                    16
#define cAf6_o_control5_TxPmaRstP3_MaxVal                                                                  0x1
#define cAf6_o_control5_TxPmaRstP3_MinVal                                                                  0x0
#define cAf6_o_control5_TxPmaRstP3_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: RxCdr_hold2
BitField Type: RW
BitField Desc: To get the CDR to lock to reference for OC3 (only) mode
    {1} :  CDR to lock to reference
    {0} :  CDR Locked to Data %% RW  %% 0x0 %% 0x0
BitField Bits: [15]
--------------------------------------*/
#define cAf6_o_control5_RxCdr_hold2_Mask                                                                 cBit15
#define cAf6_o_control5_RxCdr_hold2_Shift                                                                15

/*--------------------------------------
BitField Name: SerdesLoopP2
BitField Type: RW
BitField Desc: Loopback for SFP Serdes Port 2, The recommended do reset TX/RX
PMA after Loop/unloop serdes 000: Normal operation 001: Near-End PCS Loopback
010: Near-End PMA Loopback 100: Far-End PMA Loopback 110: Far-End PCS Loopback
BitField Bits: [14:12]
--------------------------------------*/
#define cAf6_o_control5_SerdesLoopP2_Bit_Start                                                              12
#define cAf6_o_control5_SerdesLoopP2_Bit_End                                                                14
#define cAf6_o_control5_SerdesLoopP2_Mask                                                            cBit14_12
#define cAf6_o_control5_SerdesLoopP2_Shift                                                                  12
#define cAf6_o_control5_SerdesLoopP2_MaxVal                                                                0x7
#define cAf6_o_control5_SerdesLoopP2_MinVal                                                                0x0
#define cAf6_o_control5_SerdesLoopP2_RstVal                                                                0x0

/*--------------------------------------
BitField Name: RxPcsRstP2
BitField Type: RW
BitField Desc: Reset RX PCS Serdes for SFP Port 2,The recommended do reset after
change paraneter and Loop/unloop serdes 0: Release 1: Reset
BitField Bits: [11]
--------------------------------------*/
#define cAf6_o_control5_RxPcsRstP2_Bit_Start                                                                11
#define cAf6_o_control5_RxPcsRstP2_Bit_End                                                                  11
#define cAf6_o_control5_RxPcsRstP2_Mask                                                                 cBit11
#define cAf6_o_control5_RxPcsRstP2_Shift                                                                    11
#define cAf6_o_control5_RxPcsRstP2_MaxVal                                                                  0x1
#define cAf6_o_control5_RxPcsRstP2_MinVal                                                                  0x0
#define cAf6_o_control5_RxPcsRstP2_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: RxPmaRstP2
BitField Type: RW
BitField Desc: Reset RX PMA Serdes for SFP Port 2,The recommended do reset after
change paraneter and Loop/unloop serdes 0: Release 1: Reset
BitField Bits: [10]
--------------------------------------*/
#define cAf6_o_control5_RxPmaRstP2_Bit_Start                                                                10
#define cAf6_o_control5_RxPmaRstP2_Bit_End                                                                  10
#define cAf6_o_control5_RxPmaRstP2_Mask                                                                 cBit10
#define cAf6_o_control5_RxPmaRstP2_Shift                                                                    10
#define cAf6_o_control5_RxPmaRstP2_MaxVal                                                                  0x1
#define cAf6_o_control5_RxPmaRstP2_MinVal                                                                  0x0
#define cAf6_o_control5_RxPmaRstP2_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: TxPcsRstP2
BitField Type: RW
BitField Desc: Reset TX PCS Serdes for SFP Port 2,The recommended do reset after
change paraneter and Loop/unloop serdes 0: Release 1: Reset
BitField Bits: [9]
--------------------------------------*/
#define cAf6_o_control5_TxPcsRstP2_Bit_Start                                                                 9
#define cAf6_o_control5_TxPcsRstP2_Bit_End                                                                   9
#define cAf6_o_control5_TxPcsRstP2_Mask                                                                  cBit9
#define cAf6_o_control5_TxPcsRstP2_Shift                                                                     9
#define cAf6_o_control5_TxPcsRstP2_MaxVal                                                                  0x1
#define cAf6_o_control5_TxPcsRstP2_MinVal                                                                  0x0
#define cAf6_o_control5_TxPcsRstP2_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: TxPmaRstP2
BitField Type: RW
BitField Desc: Reset TX PMA Serdes for SFP Port 2,The recommended do reset after
change paraneter and Loop/unloop serdes 0: Release 1: Reset
BitField Bits: [8]
--------------------------------------*/
#define cAf6_o_control5_TxPmaRstP2_Bit_Start                                                                 8
#define cAf6_o_control5_TxPmaRstP2_Bit_End                                                                   8
#define cAf6_o_control5_TxPmaRstP2_Mask                                                                  cBit8
#define cAf6_o_control5_TxPmaRstP2_Shift                                                                     8
#define cAf6_o_control5_TxPmaRstP2_MaxVal                                                                  0x1
#define cAf6_o_control5_TxPmaRstP2_MinVal                                                                  0x0
#define cAf6_o_control5_TxPmaRstP2_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: RxCdr_hold1
BitField Type: RW
BitField Desc: To get the CDR to lock to reference for OC3 (only) mode
    {1} :  CDR to lock to reference
    {0} :  CDR Locked to Data %% RW  %% 0x0 %% 0x0
BitField Bits: [7]
--------------------------------------*/
#define cAf6_o_control5_RxCdr_hold1_Mask                                                                  cBit7
#define cAf6_o_control5_RxCdr_hold1_Shift                                                                 7

/*--------------------------------------
BitField Name: SerdesLoopP1
BitField Type: RW
BitField Desc: Loopback for SFP Serdes Port 1, The recommended do reset TX/RX
PMA after Loop/unloop serdes 000: Normal operation 001: Near-End PCS Loopback
010: Near-End PMA Loopback 100: Far-End PMA Loopback 110: Far-End PCS Loopback
BitField Bits: [6:4]
--------------------------------------*/
#define cAf6_o_control5_SerdesLoopP1_Bit_Start                                                               4
#define cAf6_o_control5_SerdesLoopP1_Bit_End                                                                 6
#define cAf6_o_control5_SerdesLoopP1_Mask                                                              cBit6_4
#define cAf6_o_control5_SerdesLoopP1_Shift                                                                   4
#define cAf6_o_control5_SerdesLoopP1_MaxVal                                                                0x7
#define cAf6_o_control5_SerdesLoopP1_MinVal                                                                0x0
#define cAf6_o_control5_SerdesLoopP1_RstVal                                                                0x0

/*--------------------------------------
BitField Name: RxPcsRstP1
BitField Type: RW
BitField Desc: Reset RX PCS Serdes for SFP Port 1,The recommended do reset after
change paraneter and Loop/unloop serdes 0: Release 1: Reset
BitField Bits: [3]
--------------------------------------*/
#define cAf6_o_control5_RxPcsRstP1_Bit_Start                                                                 3
#define cAf6_o_control5_RxPcsRstP1_Bit_End                                                                   3
#define cAf6_o_control5_RxPcsRstP1_Mask                                                                  cBit3
#define cAf6_o_control5_RxPcsRstP1_Shift                                                                     3
#define cAf6_o_control5_RxPcsRstP1_MaxVal                                                                  0x1
#define cAf6_o_control5_RxPcsRstP1_MinVal                                                                  0x0
#define cAf6_o_control5_RxPcsRstP1_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: RxPmaRstP1
BitField Type: RW
BitField Desc: Reset RX PMA Serdes for SFP Port 1,The recommended do reset after
change paraneter and Loop/unloop serdes 0: Release 1: Reset
BitField Bits: [2]
--------------------------------------*/
#define cAf6_o_control5_RxPmaRstP1_Bit_Start                                                                 2
#define cAf6_o_control5_RxPmaRstP1_Bit_End                                                                   2
#define cAf6_o_control5_RxPmaRstP1_Mask                                                                  cBit2
#define cAf6_o_control5_RxPmaRstP1_Shift                                                                     2
#define cAf6_o_control5_RxPmaRstP1_MaxVal                                                                  0x1
#define cAf6_o_control5_RxPmaRstP1_MinVal                                                                  0x0
#define cAf6_o_control5_RxPmaRstP1_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: TxPcsRstP1
BitField Type: RW
BitField Desc: Reset TX PCS Serdes for SFP Port 1,The recommended do reset after
change paraneter and Loop/unloop serdes 0: Release 1: Reset
BitField Bits: [1]
--------------------------------------*/
#define cAf6_o_control5_TxPcsRstP1_Bit_Start                                                                 1
#define cAf6_o_control5_TxPcsRstP1_Bit_End                                                                   1
#define cAf6_o_control5_TxPcsRstP1_Mask                                                                  cBit1
#define cAf6_o_control5_TxPcsRstP1_Shift                                                                     1
#define cAf6_o_control5_TxPcsRstP1_MaxVal                                                                  0x1
#define cAf6_o_control5_TxPcsRstP1_MinVal                                                                  0x0
#define cAf6_o_control5_TxPcsRstP1_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: TxPmaRstP1
BitField Type: RW
BitField Desc: Reset TX PMA Serdes for SFP Port 1,The recommended do reset after
change paraneter and Loop/unloop serdes 0: Release 1: Reset
BitField Bits: [0]
--------------------------------------*/
#define cAf6_o_control5_TxPmaRstP1_Bit_Start                                                                 0
#define cAf6_o_control5_TxPmaRstP1_Bit_End                                                                   0
#define cAf6_o_control5_TxPmaRstP1_Mask                                                                  cBit0
#define cAf6_o_control5_TxPmaRstP1_Shift                                                                     0
#define cAf6_o_control5_TxPmaRstP1_MaxVal                                                                  0x1
#define cAf6_o_control5_TxPmaRstP1_MinVal                                                                  0x0
#define cAf6_o_control5_TxPmaRstP1_RstVal                                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : QSGMII Serdes Configuration Vector
Reg Addr   : 0x00046
Reg Formula: Base Addr + 0x00046
    Where  :
Reg Desc   :
This is the global configuration register for QSGMII Serdes Configuration Vector

------------------------------------------------------------------------------*/
#define cAf6Reg_o_control6_Base                                                                        0x00046
#define cAf6Reg_o_control6                                                                           0x00046UL
#define cAf6Reg_o_control6_WidthVal                                                                         32
#define cAf6Reg_o_control6_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: AutoNegotiationEnable
BitField Type: RW
BitField Desc: This signal is valid only if the Auto-Negotiation (AN) module is
enabled through the Vivado IP catalog. 0: AN is disabled. 1: the signal enables
the AN feature.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_o_control6_AutoNegotiationEnable_Bit_Start                                                      4
#define cAf6_o_control6_AutoNegotiationEnable_Bit_End                                                        4
#define cAf6_o_control6_AutoNegotiationEnable_Mask                                                       cBit4
#define cAf6_o_control6_AutoNegotiationEnable_Shift                                                          4
#define cAf6_o_control6_AutoNegotiationEnable_MaxVal                                                       0x1
#define cAf6_o_control6_AutoNegotiationEnable_MinVal                                                       0x0
#define cAf6_o_control6_AutoNegotiationEnable_RstVal                                                       0x0

/*--------------------------------------
BitField Name: Isolate
BitField Type: RW
BitField Desc: Isolate 0: normal operation is enabled 1: the GMII should be
electrically isolated.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_o_control6_Isolate_Bit_Start                                                                    3
#define cAf6_o_control6_Isolate_Bit_End                                                                      3
#define cAf6_o_control6_Isolate_Mask                                                                     cBit3
#define cAf6_o_control6_Isolate_Shift                                                                        3
#define cAf6_o_control6_Isolate_MaxVal                                                                     0x1
#define cAf6_o_control6_Isolate_MinVal                                                                     0x0
#define cAf6_o_control6_Isolate_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: PowerDown
BitField Type: RW
BitField Desc: Power Down 0: Normal operation 1: the device-specific transceiver
is placed in a low-power state. A reset must be applied to clear.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_o_control6_PowerDown_Bit_Start                                                                  2
#define cAf6_o_control6_PowerDown_Bit_End                                                                    2
#define cAf6_o_control6_PowerDown_Mask                                                                   cBit2
#define cAf6_o_control6_PowerDown_Shift                                                                      2
#define cAf6_o_control6_PowerDown_MaxVal                                                                   0x1
#define cAf6_o_control6_PowerDown_MinVal                                                                   0x0
#define cAf6_o_control6_PowerDown_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: UnEnable
BitField Type: RW
BitField Desc: Unidirectional Enable 0: Normal operation 1: Enable Transmit
irrespective of the state of RX
BitField Bits: [0]
--------------------------------------*/
#define cAf6_o_control6_UnEnable_Bit_Start                                                                   0
#define cAf6_o_control6_UnEnable_Bit_End                                                                     0
#define cAf6_o_control6_UnEnable_Mask                                                                    cBit0
#define cAf6_o_control6_UnEnable_Shift                                                                       0
#define cAf6_o_control6_UnEnable_MaxVal                                                                    0x1
#define cAf6_o_control6_UnEnable_MinVal                                                                    0x0
#define cAf6_o_control6_UnEnable_RstVal                                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : Control 7
Reg Addr   : 0x00047
Reg Formula: Base Addr + 0x00047
    Where  :
Reg Desc   :
This is the global configuration register for the Global Serdes Control

------------------------------------------------------------------------------*/
#define cAf6Reg_o_control7_Base                                                                        0x00047
#define cAf6Reg_o_control7                                                                           0x00047UL
#define cAf6Reg_o_control7_WidthVal                                                                         32
#define cAf6Reg_o_control7_WriteMask                                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : TOP Status 0-a
Reg Addr   : 0x00060 - 0x0006a
Reg Formula:
    Where  :
Reg Desc   :

0x60 : XFI user clock (156.25 Mhz)
0x61 : QSGMII user clock (125 Mhz)
0x62 : System clock (155.52 Mhz)
0x63 : DDR4#1 user clock (250 Mhz)
0x64 : DDR4#1 user clock (250 Mhz)
0x65 : PCIe user clock (62.5 Mhz)
0x66 : QDR user clock (125 Mhz)
0x67 : SFP1 user clock (1Gbe : 125 Mhz /STM16+STM1 : 155.52 Mhz / STM4 : 38.88Mhz)
0x68 : SFP2 user clock (1Gbe : 125 Mhz /STM16+STM1 : 155.52 Mhz / STM4 : 38.88Mhz)
0x69 : SFP3 user clock (1Gbe : 125 Mhz /STM16+STM1 : 155.52 Mhz / STM4 : 38.88Mhz)
0x6a : SFP4 user clock (1Gbe : 125 Mhz /STM16+STM1 : 155.52 Mhz / STM4 : 38.88Mhz)

------------------------------------------------------------------------------*/
#define cAf6Reg_status_0_a_Base                                                                        0x00060
#define cAf6Reg_status_0_a                                                                           0x00060UL
#define cAf6Reg_status_0_a_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: i_statusx
BitField Type: RO
BitField Desc: Clock Value Monitor Change from hex format to DEC format to get
Clock Value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_status_0_a_i_statusx_Bit_Start                                                                  0
#define cAf6_status_0_a_i_statusx_Bit_End                                                                   31
#define cAf6_status_0_a_i_statusx_Mask                                                                cBit31_0
#define cAf6_status_0_a_i_statusx_Shift                                                                      0
#define cAf6_status_0_a_i_statusx_MaxVal                                                            0xffffffff
#define cAf6_status_0_a_i_statusx_MinVal                                                                   0x0
#define cAf6_status_0_a_i_statusx_RstVal                                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : TOP Status e
Reg Addr   : 0x0006e
Reg Formula:
    Where  :
Reg Desc   :
CPLD Convert Version

------------------------------------------------------------------------------*/
#define cAf6Reg_status_e_Base                                                                          0x0006e
#define cAf6Reg_status_e                                                                             0x0006eUL
#define cAf6Reg_status_e_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: Active
BitField Type: RO
BitField Desc: CPLD Image Avalible 10 : CPLD Downloaded xx : CPLD not downloaded
:
BitField Bits: [5:4]
--------------------------------------*/
#define cAf6_status_e_Active_Bit_Start                                                                       4
#define cAf6_status_e_Active_Bit_End                                                                         5
#define cAf6_status_e_Active_Mask                                                                      cBit5_4
#define cAf6_status_e_Active_Shift                                                                           4
#define cAf6_status_e_Active_MaxVal                                                                        0x3
#define cAf6_status_e_Active_MinVal                                                                        0x0
#define cAf6_status_e_Active_RstVal                                                                        0x2

/*--------------------------------------
BitField Name: Version
BitField Type: RO
BitField Desc: CPLD Image Version
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_status_e_Version_Bit_Start                                                                      0
#define cAf6_status_e_Version_Bit_End                                                                        3
#define cAf6_status_e_Version_Mask                                                                     cBit3_0
#define cAf6_status_e_Version_Shift                                                                          0
#define cAf6_status_e_Version_MaxVal                                                                       0xf
#define cAf6_status_e_Version_MinVal                                                                       0x0
#define cAf6_status_e_Version_RstVal                                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : Top Interface Alarm Sticky
Reg Addr   : 0x00051
Reg Formula:
    Where  :
Reg Desc   :
Top Interface Alarm Sticky

------------------------------------------------------------------------------*/
#define cAf6Reg_i_sticky1_Base                                                                         0x00051
#define cAf6Reg_i_sticky1                                                                            0x00051UL
#define cAf6Reg_i_sticky1_WidthVal                                                                          32
#define cAf6Reg_i_sticky1_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: QsgmiiLinkP2
BitField Type: W1C
BitField Desc: Set 1 while Link down state change event happens.
BitField Bits: [18]
--------------------------------------*/
#define cAf6_i_sticky1_QsgmiiLinkP2_Bit_Start                                                               18
#define cAf6_i_sticky1_QsgmiiLinkP2_Bit_End                                                                 18
#define cAf6_i_sticky1_QsgmiiLinkP2_Mask                                                                cBit18
#define cAf6_i_sticky1_QsgmiiLinkP2_Shift                                                                   18
#define cAf6_i_sticky1_QsgmiiLinkP2_MaxVal                                                                 0x1
#define cAf6_i_sticky1_QsgmiiLinkP2_MinVal                                                                 0x0
#define cAf6_i_sticky1_QsgmiiLinkP2_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: QsgmiiLinkP1
BitField Type: W1C
BitField Desc: Set 1 while Link down state change event happens.
BitField Bits: [16]
--------------------------------------*/
#define cAf6_i_sticky1_QsgmiiLinkP1_Bit_Start                                                               16
#define cAf6_i_sticky1_QsgmiiLinkP1_Bit_End                                                                 16
#define cAf6_i_sticky1_QsgmiiLinkP1_Mask                                                                cBit16
#define cAf6_i_sticky1_QsgmiiLinkP1_Shift                                                                   16
#define cAf6_i_sticky1_QsgmiiLinkP1_MaxVal                                                                 0x1
#define cAf6_i_sticky1_QsgmiiLinkP1_MinVal                                                                 0x0
#define cAf6_i_sticky1_QsgmiiLinkP1_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: XfiResetP2
BitField Type: W1C
BitField Desc: Set 1 while Reset not finish state change event happens.
BitField Bits: [15]
--------------------------------------*/
#define cAf6_i_sticky1_XfiResetP2_Bit_Start                                                                 15
#define cAf6_i_sticky1_XfiResetP2_Bit_End                                                                   15
#define cAf6_i_sticky1_XfiResetP2_Mask                                                                  cBit15
#define cAf6_i_sticky1_XfiResetP2_Shift                                                                     15
#define cAf6_i_sticky1_XfiResetP2_MaxVal                                                                   0x1
#define cAf6_i_sticky1_XfiResetP2_MinVal                                                                   0x0
#define cAf6_i_sticky1_XfiResetP2_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: XfiResetP1
BitField Type: W1C
BitField Desc: Set 1 while Reset not finish state change event happens.
BitField Bits: [14]
--------------------------------------*/
#define cAf6_i_sticky1_XfiResetP1_Bit_Start                                                                 14
#define cAf6_i_sticky1_XfiResetP1_Bit_End                                                                   14
#define cAf6_i_sticky1_XfiResetP1_Mask                                                                  cBit14
#define cAf6_i_sticky1_XfiResetP1_Shift                                                                     14
#define cAf6_i_sticky1_XfiResetP1_MaxVal                                                                   0x1
#define cAf6_i_sticky1_XfiResetP1_MinVal                                                                   0x0
#define cAf6_i_sticky1_XfiResetP1_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: XfiLinkP2
BitField Type: W1C
BitField Desc: Set 1 while Link down state change event happens.
BitField Bits: [13]
--------------------------------------*/
#define cAf6_i_sticky1_XfiLinkP2_Bit_Start                                                                  13
#define cAf6_i_sticky1_XfiLinkP2_Bit_End                                                                    13
#define cAf6_i_sticky1_XfiLinkP2_Mask                                                                   cBit13
#define cAf6_i_sticky1_XfiLinkP2_Shift                                                                      13
#define cAf6_i_sticky1_XfiLinkP2_MaxVal                                                                    0x1
#define cAf6_i_sticky1_XfiLinkP2_MinVal                                                                    0x0
#define cAf6_i_sticky1_XfiLinkP2_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: XfiLinkP1
BitField Type: W1C
BitField Desc: Set 1 while Link down state change event happens.
BitField Bits: [12]
--------------------------------------*/
#define cAf6_i_sticky1_XfiLinkP1_Bit_Start                                                                  12
#define cAf6_i_sticky1_XfiLinkP1_Bit_End                                                                    12
#define cAf6_i_sticky1_XfiLinkP1_Mask                                                                   cBit12
#define cAf6_i_sticky1_XfiLinkP1_Shift                                                                      12
#define cAf6_i_sticky1_XfiLinkP1_MaxVal                                                                    0x1
#define cAf6_i_sticky1_XfiLinkP1_MinVal                                                                    0x0
#define cAf6_i_sticky1_XfiLinkP1_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: SystemPll
BitField Type: W1C
BitField Desc: Set 1 while PLL Locked state change event happens when System PLL
not locked.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_i_sticky1_SystemPll_Bit_Start                                                                   8
#define cAf6_i_sticky1_SystemPll_Bit_End                                                                     8
#define cAf6_i_sticky1_SystemPll_Mask                                                                    cBit8
#define cAf6_i_sticky1_SystemPll_Shift                                                                       8
#define cAf6_i_sticky1_SystemPll_MaxVal                                                                    0x1
#define cAf6_i_sticky1_SystemPll_MinVal                                                                    0x0
#define cAf6_i_sticky1_SystemPll_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: QdrCalib
BitField Type: W1C
BitField Desc: Set 1 while Calib state change event happens when QDR Calib Fail.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_i_sticky1_QdrCalib_Bit_Start                                                                    2
#define cAf6_i_sticky1_QdrCalib_Bit_End                                                                      2
#define cAf6_i_sticky1_QdrCalib_Mask                                                                     cBit2
#define cAf6_i_sticky1_QdrCalib_Shift                                                                        2
#define cAf6_i_sticky1_QdrCalib_MaxVal                                                                     0x1
#define cAf6_i_sticky1_QdrCalib_MinVal                                                                     0x0
#define cAf6_i_sticky1_QdrCalib_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: Ddr42Calib
BitField Type: W1C
BitField Desc: Set 1 while Calib state change event happens when DDR Calib Fail.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_i_sticky1_Ddr42Calib_Bit_Start                                                                  1
#define cAf6_i_sticky1_Ddr42Calib_Bit_End                                                                    1
#define cAf6_i_sticky1_Ddr42Calib_Mask                                                                   cBit1
#define cAf6_i_sticky1_Ddr42Calib_Shift                                                                      1
#define cAf6_i_sticky1_Ddr42Calib_MaxVal                                                                   0x1
#define cAf6_i_sticky1_Ddr42Calib_MinVal                                                                   0x0
#define cAf6_i_sticky1_Ddr42Calib_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: Ddr41Calib
BitField Type: W1C
BitField Desc: Set 1 while Calib state change event happens when DDR Calib Fail.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_i_sticky1_Ddr41Calib_Bit_Start                                                                  0
#define cAf6_i_sticky1_Ddr41Calib_Bit_End                                                                    0
#define cAf6_i_sticky1_Ddr41Calib_Mask                                                                   cBit0
#define cAf6_i_sticky1_Ddr41Calib_Shift                                                                      0
#define cAf6_i_sticky1_Ddr41Calib_MaxVal                                                                   0x1
#define cAf6_i_sticky1_Ddr41Calib_MinVal                                                                   0x0
#define cAf6_i_sticky1_Ddr41Calib_RstVal                                                                   0x0

#define cRegClockExtractSelection                        0x12

#define cRegClockRef8KhzPrimaryClockChannelIdMask        cBit3_0
#define cRegClockRef8KhzPrimaryClockChannelIdShift       0

#define cRegClockRef8KhzSecondaryClockChannelIdMask      cBit7_4
#define cRegClockRef8KhzSecondaryClockChannelIdShift     4

/*
 * 3'd0: E3 LIU
   3'd1: DS3 LIU
   3'd2: EC1 LIU
   3'd3: E1 LIU
   3'd4: DS1 LIU
   3'd5: OCN
   3'd6: ETH */
#define cRegClockRef8KhzPrimaryClockSourceMask    cBit10_8
#define cRegClockRef8KhzPrimaryClockSourceShift   8

#define cRegClockRef8KhzSecondaryClockSourceMask  cBit13_11
#define cRegClockRef8KhzSecondaryClockSourceShift 11

#define cRegClockRef8KhzRefOutDis1Mask            cBit29
#define cRegClockRef8KhzRefOutDis1Shift               29

#define cRegClockRef8KhzRefOutDis0Mask            cBit28
#define cRegClockRef8KhzRefOutDis0Shift               28

#define cRegClockRef8KhzPrimaryLOSMask            cBit30
#define cRegClockRef8KhzPrimaryLOSShift           30

#define cRegClockRef8KhzSecondaryLOSMask          cBit31
#define cRegClockRef8KhzSecondaryLOSShift         31

#define cSourceE3Liu  0
#define cSourceDS3Liu 1
#define cSourceEC1Liu 2
#define cSourceE1Liu  3
#define cSourceDs1Liu 4
#define cSourceOCN    5
#define cSourceEth    6

/*------------------------------------------------------------------------------
Reg Name   : Global OCN LOS/LOF/AIS Masking clock extractor
Reg Addr   : 0x00000011
Reg Formula:
    Where  :
Reg Desc   :
This register configures clock source selected to output ref 8K or 19.44M or 25M

------------------------------------------------------------------------------*/
#define cAf6Reg_Global_OCN_Masking_clock_extractor_Base                                 0x00000011

/*--------------------------------------
BitField Name: SecondAISMask
BitField Type: RW
BitField Desc: The Secondary LOS masking
BitField Bits: [13]
--------------------------------------*/
#define cAf6_Global_OCN_Masking_clock_extractor_SecondAISMask_Mask                                  cBit13
#define cAf6_Global_OCN_Masking_clock_extractor_SecondAISMask_Shift                                     13

/*--------------------------------------
BitField Name: SecondLOFMask
BitField Type: RW
BitField Desc: The Secondary LOF masking
BitField Bits: [12]
--------------------------------------*/
#define cAf6_Global_OCN_Masking_clock_extractor_SecondLOFMask_Mask                                  cBit12
#define cAf6_Global_OCN_Masking_clock_extractor_SecondLOFMask_Shift                                     12

/*--------------------------------------
BitField Name: SecondLOSMask
BitField Type: RW
BitField Desc: The Secondary LOS masking
BitField Bits: [11]
--------------------------------------*/
#define cAf6_Global_OCN_Masking_clock_extractor_SecondLOSMask_Mask                                  cBit11
#define cAf6_Global_OCN_Masking_clock_extractor_SecondLOSMask_Shift                                     11

/*--------------------------------------
BitField Name: PrimaryAISMask
BitField Type: RW
BitField Desc: The Primary LOS masking
BitField Bits: [10]
--------------------------------------*/
#define cAf6_Global_OCN_Masking_clock_extractor_PrimaryAISMask_Mask                                  cBit10
#define cAf6_Global_OCN_Masking_clock_extractor_PrimaryAISMask_Shift                                     10

/*--------------------------------------
BitField Name: PrimaryLOFMask
BitField Type: RW
BitField Desc: The Primary LOF masking
BitField Bits: [9]
--------------------------------------*/
#define cAf6_Global_OCN_Masking_clock_extractor_PrimaryLOFMask_Mask                                   cBit9
#define cAf6_Global_OCN_Masking_clock_extractor_PrimaryLOFMask_Shift                                      9

/*--------------------------------------
BitField Name: PrimaryLOSMask
BitField Type: RW
BitField Desc: The Primary LOS masking
BitField Bits: [8]
--------------------------------------*/
#define cAf6_Global_OCN_Masking_clock_extractor_PrimaryLOSMask_Mask                                   cBit8
#define cAf6_Global_OCN_Masking_clock_extractor_PrimaryLOSMask_Shift                                      8


/*------------------------------------------------------------------------------
Reg Name   : Global Clock Monitoring
Reg Addr   : 0xF00070
Reg Formula:
    Where  :
Reg Desc   :
This register configures clock source selected to output ref 8K or 19.44M or 25M

------------------------------------------------------------------------------*/
#define cAf6Reg_Global_Clock_Monitoring_Base                                                          0xF00070

/*--------------------------------------
BitField Name: SecondLosMask
BitField Type: RW
BitField Desc: The Secondary LOS masking
BitField Bits: [31]
--------------------------------------*/
#define cAf6_Global_Clock_Monitoring_SecondLosMask_Mask                                                 cBit31
#define cAf6_Global_Clock_Monitoring_SecondLosMask_Shift                                                    31

/*------------------------------------------------------------------------------
Reg Name   : Global PDH Defect LOS/LOF/AIS Output for clock extractor
Reg Addr   : 0x00000013
Reg Formula:
    Where  :
Reg Desc   :
This register configures defect LOS/LOF/AIS per PDH ID to LIU ID

------------------------------------------------------------------------------*/
#define cAf6Reg_Global_PDH_Defect_Output_for_clock_extractor_Base                              0x00000013

/*--------------------------------------
BitField Name: SecondaryPDHDE3
BitField Type: RW
BitField Desc: The Secondary DE3 selection
BitField Bits: [27]
--------------------------------------*/
#define cAf6_Global_PDH_Defect_Output_for_clock_extractor_SecondaryPDHDE3_Mask                                  cBit27
#define cAf6_Global_PDH_Defect_Output_for_clock_extractor_SecondaryPDHDE3_Shift                                      27

/*--------------------------------------
BitField Name: SecondaryPDHOC24
BitField Type: RW
BitField Desc: The Secondary PDH ID OC24#1,2 selection
BitField Bits: [26]
--------------------------------------*/
#define cAf6_Global_PDH_Defect_Output_for_clock_extractor_SecondaryPDHOC24_Mask                                  cBit26
#define cAf6_Global_PDH_Defect_Output_for_clock_extractor_SecondaryPDHOC24_Shift                                      26

/*--------------------------------------
BitField Name: SecondaryPDHID
BitField Type: RW
BitField Desc: The Secondary PDH ID (DS1/E1/DS3/E3) selection
BitField Bits: [25:16]
--------------------------------------*/
#define cAf6_Global_PDH_Defect_Output_for_clock_extractor_SecondaryPDHID_Mask                               cBit25_16
#define cAf6_Global_PDH_Defect_Output_for_clock_extractor_SecondaryPDHID_Shift                                      16

/*--------------------------------------
BitField Name: PrimaryPDHDE3
BitField Type: RW
BitField Desc: The Primary DE3 selection
BitField Bits: [11]
--------------------------------------*/
#define cAf6_Global_PDH_Defect_Output_for_clock_extractor_PrimaryPDHDE3_Mask                                  cBit11
#define cAf6_Global_PDH_Defect_Output_for_clock_extractor_PrimaryPDHDE3_Shift                                      11

/*--------------------------------------
BitField Name: PrimaryPDHOC24
BitField Type: RW
BitField Desc: The Primary PDH ID OC24#1,2 selection
BitField Bits: [10]
--------------------------------------*/
#define cAf6_Global_PDH_Defect_Output_for_clock_extractor_PrimaryPDHOC24_Mask                                  cBit10
#define cAf6_Global_PDH_Defect_Output_for_clock_extractor_PrimaryPDHOC24_Shift                                      10

/*--------------------------------------
BitField Name: PrimaryPDHID
BitField Type: RW
BitField Desc: The Primary PDH ID (DS1/E1/DS3/E3) selection
BitField Bits: [9:0]
--------------------------------------*/
#define cAf6_Global_PDH_Defect_Output_for_clock_extractor_PrimaryPDHID_Mask                                 cBit9_0
#define cAf6_Global_PDH_Defect_Output_for_clock_extractor_PrimaryPDHID_Shift                                       0

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061TOPCTRREG_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha60210061SerdesManager.h
 * 
 * Created Date: Jun 17, 2016
 *
 * Description : 60210061 Serdes manager interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061SERDESMANAGER_H_
#define _THA60210061SERDESMANAGER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSerdesManager.h"
#include "AtEthPort.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cTha60210061SgmiiSerdesId0      0
#define cTha60210061SgmiiSerdesId1      1
#define cTha60210061SgmiiSerdesId2      2
#define cTha60210061SgmiiSerdesId3      3
#define cTha60210061RxauiSerdesId0      4
#define cTha60210061RxauiSerdesId1      5
#define cTha60210061XfiSerdesId0        6
#define cTha60210061XfiSerdesId1        7
#define cTha60210061QsgmiiSerdesId0     8
#define cTha60210061QsgmiiSerdesId1     9

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSerdesManager Tha60210061SerdesManagerNew(AtDevice device);
uint32 Tha60210061SerdesManagerSerdesIdSwToHw(AtSerdesManager self, uint32 serdesId);

/* Serdes controllers */
AtSerdesController Tha60210061XfiSerdesControllerNew(AtEthPort ethPort, uint32 serdesId);
AtSerdesController Tha60210061QsgmiiSerdesControllerNew(AtEthPort ethPort, uint32 serdesId);
AtSerdesController Tha60210061SgmiiSerdesControllerNew(AtEthPort ethPort, uint32 serdesId);
AtSerdesController Tha60210061RxauiSerdesControllerNew(AtEthPort ethPort, uint32 serdesId);

AtSerdesController Tha60210061XfiSerdesControllerV2New(AtEthPort ethPort, uint32 serdesId);
AtSerdesController Tha60210061QsgmiiSerdesControllerV2New(AtEthPort ethPort, uint32 serdesId);
AtSerdesController Tha60210061SgmiiSerdesControllerV2New(AtEthPort ethPort, uint32 serdesId);
AtSerdesController Tha60210061RxauiSerdesControllerV2New(AtEthPort ethPort, uint32 serdesId);

/* Set Dst Mac, Src Mac, EthType for prbs */
eAtRet Tha60210061SerdesPrbsEngineDestMacSet(AtPrbsEngine self, uint8* mac);
eAtRet Tha60210061SerdesPrbsEngineSrcMacSet(AtPrbsEngine self, uint8* mac);
eAtRet Tha60210061SerdesPrbsEngineEthernetTypeSet(AtPrbsEngine self, uint16 ethType);

eBool Tha60210061SerdesIsSgmii(AtSerdesController serdes);
eBool Tha60210061SerdesIsQsgmii(AtSerdesController serdes);
eBool Tha60210061SerdesIsXfi(AtSerdesController serdes);
uint32 Tha60210061RxauiSerdesControllerTuningBaseAddress(AtSerdesController serdes);

/* To control if we should return error when a SERDES resets timeout in device init */
void Tha60210061SerdesManagerSerdesResetResultCache(AtSerdesManager self, AtSerdesController serdes, eBool isOk);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061SERDESMANAGER_H_ */


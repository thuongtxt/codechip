/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha60210061RxauiLaneSerdesController.c
 *
 * Created Date: Mar 7, 2017
 *
 * Description : 60210061 RXAUI LANE SERDES controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "Tha60210061SerdesInternal.h"
#include "Tha60210061RxauiSerdesTuningReg.h"
#include "Tha60210061SerdesManager.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210061RxauiLaneSerdesController*)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061RxauiLaneSerdesController
    {
    tTha60210061RxauiSerdesController super;
    AtSerdesController parent;
    AtDrp drp;
    }tTha60210061RxauiLaneSerdesController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods           m_AtObjectOverride;
static tAtSerdesControllerMethods m_AtSerdesControllerOverride;

/* Save super implementation */
static const tAtObjectMethods           *m_AtObjectMethods = NULL;
static const tAtSerdesControllerMethods *m_AtSerdesControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 SerdesTuningBaseAddress(AtSerdesController self)
    {
    uint32 parrentId = AtSerdesControllerIdGet(AtSerdesControllerParentGet(self));
    return (parrentId == cTha60210061RxauiSerdesId0) ? cTha60210061RXaui0SerdesTuningBaseAddress : cTha60210061RXaui1SerdesTuningBaseAddress;
    }

static eBool IsSimulated(AtSerdesController self)
    {
    AtDevice device = AtChannelDeviceGet(AtSerdesControllerPhysicalPortGet(self));
    return AtDeviceIsSimulated(device);
    }

static eAtRet Reset(AtSerdesController self)
    {
    const uint32 cTriggerTimeInMs = 200;
    uint32 txRegAddr, rxRegAddr, txRegVal, rxRegVal;
    uint32 serdesId = AtSerdesControllerIdGet(self);

    uint32 regTriggerMask = (serdesId == 0) ? cAf6_SERDES_TX_Reset_txrst_trig0_Mask : cAf6_SERDES_TX_Reset_txrst_trig1_Mask;
    uint32 regTriggerShift = (serdesId == 0) ? cAf6_SERDES_TX_Reset_txrst_trig0_Shift : cAf6_SERDES_TX_Reset_txrst_trig1_Shift;

    /* Reset TX */
    txRegAddr = SerdesTuningBaseAddress(self) + cAf6Reg_SERDES_TX_Reset_Base;
    txRegVal = AtSerdesControllerRead(self, txRegAddr, cAtModuleEth);
    mRegFieldSet(txRegAddr, regTrigger, 1);
    AtSerdesControllerWrite(self, txRegAddr, txRegVal, cAtModuleEth);

    /* Reset RX */
    rxRegAddr = SerdesTuningBaseAddress(self) + cAf6Reg_SERDES_RX_Reset_Base;
    rxRegVal = AtSerdesControllerRead(self, rxRegAddr, cAtModuleEth);
    mRegFieldSet(rxRegVal, regTrigger, 1);
    AtSerdesControllerWrite(self, rxRegAddr, rxRegVal, cAtModuleEth);

    if (!IsSimulated(self))
        AtOsalUSleep(cTriggerTimeInMs * 1000);

    mRegFieldSet(txRegVal, regTrigger, 0);
    AtSerdesControllerWrite(self, txRegAddr, txRegVal, cAtModuleEth);

    mRegFieldSet(rxRegVal, regTrigger, 0);
    AtSerdesControllerWrite(self, rxRegAddr, rxRegVal, cAtModuleEth);

    return cAtOk;
    }

static uint32 LoopbackAddress(AtSerdesController self)
    {
    return (uint32)(SerdesTuningBaseAddress(self) + cAf6Reg_SERDES_LoopBack_Base);
    }

static uint8 LoopbackModeSw2Hw(AtSerdesController self, eAtLoopbackMode loopbackMode)
    {
    AtUnused(self);
    switch (loopbackMode)
        {
        case cAtLoopbackModeLocal:
            return 2;
        case cAtLoopbackModeRemote:
            return 4;
        case cAtLoopbackModeRelease:
            return 0;
        case cAtLoopbackModeDual:
        default:
            return 0;
        }
    }

static eAtLoopbackMode LoopbackModeHw2Sw(AtSerdesController self, uint32 hwLoopbackMode)
    {
    AtUnused(self);
    switch (hwLoopbackMode)
        {
        case 0:
            return cAtLoopbackModeRelease;
        case 2:
            return cAtLoopbackModeLocal;
        case 4:
            return cAtLoopbackModeRemote;
        default:
            return cAtLoopbackModeRelease;
        }
    }
    
static eAtRet HwLoopbackSet(AtSerdesController self, eAtLoopbackMode loopbackMode)
    {
    uint32 address = LoopbackAddress(self);
    uint32 regVal = AtSerdesControllerRead(self, address, cAtModuleEth);
    uint8 hwLoopbackMode = LoopbackModeSw2Hw(self, loopbackMode);
    uint32 serdesId = AtSerdesControllerIdGet(self);

    if (serdesId == 0)
        mRegFieldSet(regVal, cAf6_SERDES_LoopBack_lpback_subport0_, hwLoopbackMode);
    else
        mRegFieldSet(regVal, cAf6_SERDES_LoopBack_lpback_subport1_, hwLoopbackMode);

    AtSerdesControllerWrite(self, address, regVal, cAtModuleEth);

    return cAtOk;
    }

static eAtLoopbackMode HwLoopbackGet(AtSerdesController self)
    {
    uint32 address  = LoopbackAddress(self);
    uint32 regVal   = AtSerdesControllerRead(self, address, cAtModuleEth);
    uint32 serdesId = AtSerdesControllerIdGet(self);
    uint32 hwLoopbackMode;

    if (serdesId == 0)
        hwLoopbackMode = mRegField(regVal, cAf6_SERDES_LoopBack_lpback_subport0_);
    else
        hwLoopbackMode = mRegField(regVal, cAf6_SERDES_LoopBack_lpback_subport1_);

    return LoopbackModeHw2Sw(self, hwLoopbackMode);
    }
    
static eAtRet RemoteLoopbackEnable(AtSerdesController self, eBool enable)
    {
    if (enable)
        return HwLoopbackSet(self, cAtLoopbackModeRemote);

    return HwLoopbackSet(self, cAtLoopbackModeRelease);
    }

static eAtRet LocalLoopbackEnable(AtSerdesController self, eBool enable)
    {
    if (enable)
        return HwLoopbackSet(self, cAtLoopbackModeLocal);

    return HwLoopbackSet(self, cAtLoopbackModeRelease);
    }

static eAtRet LoopbackEnable(AtSerdesController self, eAtLoopbackMode loopbackMode, eBool enable)
    {
    if (loopbackMode == cAtLoopbackModeRemote)
        return RemoteLoopbackEnable(self, enable);

    if (loopbackMode == cAtLoopbackModeLocal)
        return LocalLoopbackEnable(self, enable);

    return HwLoopbackSet(self, cAtLoopbackModeRelease);
    }

static eBool LoopbackIsEnabled(AtSerdesController self, eAtLoopbackMode loopbackMode)
    {
    return (loopbackMode == HwLoopbackGet(self)) ? cAtTrue : cAtFalse;
    }
    
static eAtRet PowerDown(AtSerdesController self, eBool powerDown)
    {
    uint32 regAddress = SerdesTuningBaseAddress(self) + cAf6Reg_SERDES_POWER_DOWN_Base;
    uint32 regVal = AtSerdesControllerRead(self, regAddress, cAtModuleEth);
    uint32 serdesId = AtSerdesControllerIdGet(self);

    if (serdesId == 0)
        {
        mRegFieldSet(regVal, cAf6_SERDES_POWER_DOWN_RXPD0_, powerDown ? 0x3 : 0x0);
        mRegFieldSet(regVal, cAf6_SERDES_POWER_DOWN_TXPD0_, powerDown ? 0x3 : 0x0);
        }
    else
        {
        mRegFieldSet(regVal, cAf6_SERDES_POWER_DOWN_RXPD1_, powerDown ? 0x3 : 0x0);
        mRegFieldSet(regVal, cAf6_SERDES_POWER_DOWN_TXPD1_, powerDown ? 0x3 : 0x0);
        }

    AtSerdesControllerWrite(self, regAddress, regVal, cAtModuleEth);
    return cAtOk;
    }

static eBool PowerIsDown(AtSerdesController self)
    {
    uint32 regAddress = SerdesTuningBaseAddress(self) + cAf6Reg_SERDES_POWER_DOWN_Base;
    uint32 regVal = AtSerdesControllerRead(self, regAddress, cAtModuleEth);
    uint32 serdesId = AtSerdesControllerIdGet(self);
    uint32 hwPowerdown;

    if (serdesId == 0)
        {
        hwPowerdown = mRegField(regVal, cAf6_SERDES_POWER_DOWN_RXPD0_);
        hwPowerdown &= mRegField(regVal, cAf6_SERDES_POWER_DOWN_TXPD0_);
        }
    else
        {
        hwPowerdown = mRegField(regVal, cAf6_SERDES_POWER_DOWN_RXPD1_);
        hwPowerdown &= mRegField(regVal, cAf6_SERDES_POWER_DOWN_TXPD1_);
        }

    return (hwPowerdown == 0x3) ? cAtTrue : cAtFalse;
    }

static eAtRet HwTxCursorSet(AtSerdesController self, uint32 address, uint32 value)
    {
    uint32 regVal = AtSerdesControllerRead(self, address, cAtModuleEth);
    uint32 serdesId = AtSerdesControllerIdGet(self);

    if (serdesId == 0)
        mRegFieldSet(regVal, cAf6_SERDES_TXPOSTCURSOR_TXPOSTCURSOR_subport0_, value);
    else
        mRegFieldSet(regVal, cAf6_SERDES_TXPOSTCURSOR_TXPOSTCURSOR_subport1_, value);

    AtSerdesControllerWrite(self, address, regVal, cAtModuleEth);
    return cAtOk;
    }

static uint32 HwTxCursorGet(AtSerdesController self, uint32 address)
    {
    uint32 regVal = AtSerdesControllerRead(self, address, cAtModuleEth);
    uint32 serdesId = AtSerdesControllerIdGet(self);

    if (serdesId == 0)
        return mRegField(regVal, cAf6_SERDES_TXPOSTCURSOR_TXPOSTCURSOR_subport0_);

    return mRegField(regVal, cAf6_SERDES_TXPOSTCURSOR_TXPOSTCURSOR_subport1_);
    }
    
static eAtRet HwTxDiffCtrlSet(AtSerdesController self, uint32 value)
    {
    uint32 address = SerdesTuningBaseAddress(self) + cAf6Reg_SERDES_TXDIFFCTRL_Base;
    uint32 regVal = AtSerdesControllerRead(self, address, cAtModuleEth);
    uint32 serdesId = AtSerdesControllerIdGet(self);

    if (serdesId == 0)
        mRegFieldSet(regVal, cAf6_SERDES_TXDIFFCTRL_TXDIFFCTRL_subport0_, value);
    else
        mRegFieldSet(regVal, cAf6_SERDES_TXDIFFCTRL_TXDIFFCTRL_subport1_, value);

    AtSerdesControllerWrite(self, address, regVal, cAtModuleEth);
    return cAtOk;
    }

static uint32 HwTxDiffCtrlGet(AtSerdesController self)
    {
    uint32 address = SerdesTuningBaseAddress(self) + cAf6Reg_SERDES_TXDIFFCTRL_Base;
    uint32 regVal = AtSerdesControllerRead(self, address, cAtModuleEth);
    uint32 serdesId = AtSerdesControllerIdGet(self);

    if (serdesId == 0)
        return mRegField(regVal, cAf6_SERDES_TXDIFFCTRL_TXDIFFCTRL_subport0_);

    return mRegField(regVal, cAf6_SERDES_TXDIFFCTRL_TXDIFFCTRL_subport1_);
    }

static eAtRet HwPhysicalParamSet(AtSerdesController self, eAtSerdesParam param, uint32 value)
    {
    if (param == cAtSerdesParamTxPreCursor)
        return HwTxCursorSet(self, SerdesTuningBaseAddress(self) + cAf6Reg_SERDES_TXPRECURSOR_Base, value);

    if (param == cAtSerdesParamTxPostCursor)
        return HwTxCursorSet(self, SerdesTuningBaseAddress(self) + cAf6Reg_SERDES_TXPOSTCURSOR_Base, value);

    if (param == cAtSerdesParamTxDiffCtrl)
        return HwTxDiffCtrlSet(self, value);

    return cAtOk;
    }

static uint32 HwPhysicalParamGet(AtSerdesController self, eAtSerdesParam param)
    {
    if (param == cAtSerdesParamTxPreCursor)
        return HwTxCursorGet(self, SerdesTuningBaseAddress(self) + cAf6Reg_SERDES_TXPRECURSOR_Base);

    if (param == cAtSerdesParamTxPostCursor)
        return HwTxCursorGet(self, SerdesTuningBaseAddress(self) + cAf6Reg_SERDES_TXPOSTCURSOR_Base);

    if (param == cAtSerdesParamTxDiffCtrl)
        return HwTxDiffCtrlGet(self);

    return cInvalidUint32;
    }

static uint32 PhysicalParamGet(AtSerdesController self, eAtSerdesParam param)
    {
    return HwPhysicalParamGet(self, param);
    }

static eAtRet PhysicalParamSet(AtSerdesController self, eAtSerdesParam param, uint32 value)
    {
    if (mMethodsGet(self)->PhysicalParamValueIsInRange(self, param, value))
        return HwPhysicalParamSet(self, param, value);

    return cAtErrorOutOfRangParm;
    }

static uint32 LpmEnableRegister(AtSerdesController self)
    {
    return SerdesTuningBaseAddress(self) + cAf6Reg_SERDES_LPMDFE_Mode_Base;
    }

static void LpmDfeReset(AtSerdesController self)
    {
    const uint32 cTriggerTimeInMs = 200;
    uint32 regAddr = SerdesTuningBaseAddress(self) + cAf6Reg_SERDES_LPMDFE_Reset_Base;
    uint32 regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    uint32 serdesId = AtSerdesControllerIdGet(self);
    uint32 resetMask = (serdesId == 0) ? cAf6_SERDES_LPMDFE_Reset_lpmdfe_port0_reset_Mask : cAf6_SERDES_LPMDFE_Reset_lpmdfe_port1_reset_Mask;
    uint32 resetShift = (serdesId == 0) ? cAf6_SERDES_LPMDFE_Reset_lpmdfe_port0_reset_Shift : cAf6_SERDES_LPMDFE_Reset_lpmdfe_port1_reset_Shift;

    mRegFieldSet(regVal, reset, 1);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    if (!IsSimulated(self))
        AtOsalUSleep(cTriggerTimeInMs * 1000);

    mRegFieldSet(regVal, reset, 0);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);
    }

static uint32 EqualizerModeSw2Hw(eAtSerdesEqualizerMode mode)
    {
    if (mode == cAtSerdesEqualizerModeDfe)  return 0;
    if (mode == cAtSerdesEqualizerModeLpm)  return 1;

    return 0x0;
    }

static eAtRet EqualizerModeSet(AtSerdesController self, eAtSerdesEqualizerMode mode)
    {
    uint32 regVal, address, serdesId;

    if (!AtSerdesControllerEqualizerModeIsSupported(self, mode))
        return cAtErrorModeNotSupport;

    serdesId = AtSerdesControllerIdGet(self);
    address = LpmEnableRegister(self);
    regVal = AtSerdesControllerRead(self, address, cAtModuleEth);

    if (serdesId == 0)
        mRegFieldSet(regVal, cAf6_SERDES_LPMDFE_Mode_lpmdfe_port0_mode_, EqualizerModeSw2Hw(mode));
    else
        mRegFieldSet(regVal, cAf6_SERDES_LPMDFE_Mode_lpmdfe_port1_mode_, EqualizerModeSw2Hw(mode));

    AtSerdesControllerWrite(self, address, regVal, cAtModuleEth);

    LpmDfeReset(self);
    return cAtOk;
    }

static eAtSerdesEqualizerMode EqualizerModeHw2Sw(uint32 mode)
    {
    if (mode == 0) return cAtSerdesEqualizerModeDfe;
    if (mode == 1) return cAtSerdesEqualizerModeLpm;
    return cAtSerdesEqualizerModeUnknown;
    }

static eAtSerdesEqualizerMode EqualizerModeGet(AtSerdesController self)
    {
    uint32 address = LpmEnableRegister(self);
    uint32 regVal = AtSerdesControllerRead(self, address, cAtModuleEth);
    uint32 serdesId = AtSerdesControllerIdGet(self);

    if (serdesId == 0)
        return EqualizerModeHw2Sw(mRegField(regVal, cAf6_SERDES_LPMDFE_Mode_lpmdfe_port0_mode_));

    return EqualizerModeHw2Sw(mRegField(regVal, cAf6_SERDES_LPMDFE_Mode_lpmdfe_port1_mode_));
    }

static const char* Description(AtSerdesController self)
    {
    AtUnused(self);
    return "RXAUI_LANE";
    }
    
static AtSerdesController ParentGet(AtSerdesController self)
    {
    return mThis(self)->parent;
    }

static eBool FpgaIsEnabled(AtSerdesController self)
    {
    return AtSerdesControllerIsEnabled(AtSerdesControllerParentGet(self));
    }

static eAtRet FpgaEnable(AtSerdesController self, eBool enable)
    {
    /* Do not allow to enable/disable per lane */
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorModeNotSupport;
    }

static AtPrbsEngine PrbsEngineCreate(AtSerdesController self)
    {
    return Tha60210061RxauiSerdesLaneRawPrbsEngineNew(self);
    }

static uint32 EyeScanDrpBaseAddress(AtSerdesController self)
    {
    uint32 landId = AtSerdesControllerIdGet(self);
    return AtSerdesControllerDrpBaseAddress(AtSerdesControllerParentGet(self)) + landId * 0x400;
    }

static uint32 PortOffset(AtDrp drp, uint32 selectedPortId)
    {
    AtUnused(drp);
    AtUnused(selectedPortId);

    /* Each DRP for each lane already has correct base address */
    return 0x0;
    }

static tAtDrpAddressCalculator *AddressCalculator(void)
    {
    static tAtDrpAddressCalculator addrCalculator;
    static tAtDrpAddressCalculator *pAddrCalculator = NULL;

    if (pAddrCalculator)
        return pAddrCalculator;

    AtOsalMemInit(&addrCalculator, 0, sizeof(addrCalculator));
    addrCalculator.PortOffset = PortOffset;
    pAddrCalculator = &addrCalculator;

    return pAddrCalculator;
    }

static AtDrp DrpCreate(AtSerdesController self)
    {
    AtDevice device = AtSerdesControllerDeviceGet(self);
    AtHal hal = AtDeviceIpCoreHalGet(device, 0);
    AtDrp newDrp = AtDrpNew(AtSerdesControllerDrpBaseAddress(self), hal);
    if (!newDrp)
        return NULL;

    AtDrpNumberOfSerdesControllersSet(newDrp, AtSerdesControllerNumLanes(AtSerdesControllerParentGet(self)));
    AtDrpNeedSelectPortSet(newDrp, cAtFalse);
    AtDrpAddressCalculatorSet(newDrp, AddressCalculator());

    return newDrp;
    }

static AtDrp Drp(AtSerdesController self)
    {
    if (mThis(self)->drp)
        return mThis(self)->drp;

    mThis(self)->drp = DrpCreate(self);
    return mThis(self)->drp;
    }

static void Delete(AtObject self)
    {
    AtDrpDelete(mThis(self)->drp);
    mThis(self)->drp = NULL;

    /* Super deletion */
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210061RxauiLaneSerdesController *object = (tTha60210061RxauiLaneSerdesController *)self;

    m_AtObjectMethods->Serialize(self, encoder);

    AtSerdesControllerDrp((AtSerdesController)object);
    AtCoderEncodeString(encoder, AtDrpToString(object->drp), "serdesDrp");
    mEncodeObjectDescription(parent);
    }

static AtMdio Mdio(AtSerdesController self)
    {
    /* Do not support MDIO per lane */
    AtUnused(self);
    return NULL;
    }

static void OverrideAtObject(AtSerdesController self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSerdesControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, m_AtSerdesControllerMethods, sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, LoopbackEnable);
        mMethodOverride(m_AtSerdesControllerOverride, PowerDown);
        mMethodOverride(m_AtSerdesControllerOverride, PowerIsDown);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalParamSet);
        mMethodOverride(m_AtSerdesControllerOverride, Description);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerModeGet);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerModeSet);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalParamGet);
        mMethodOverride(m_AtSerdesControllerOverride, LoopbackIsEnabled);
        mMethodOverride(m_AtSerdesControllerOverride, ParentGet);
        mMethodOverride(m_AtSerdesControllerOverride, FpgaEnable);
        mMethodOverride(m_AtSerdesControllerOverride, FpgaIsEnabled);
        mMethodOverride(m_AtSerdesControllerOverride, PrbsEngineCreate);
        mMethodOverride(m_AtSerdesControllerOverride, Drp);
        mMethodOverride(m_AtSerdesControllerOverride, Mdio);
        mMethodOverride(m_AtSerdesControllerOverride, EyeScanDrpBaseAddress);
        mMethodOverride(m_AtSerdesControllerOverride, Reset);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtObject(self);
    OverrideAtSerdesController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061RxauiLaneSerdesController);
    }

static AtSerdesController ObjectInit(AtSerdesController self, AtSerdesController parent, AtEthPort ethPort, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210061RxauiSerdesControllerObjectInit(self, ethPort, serdesId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;
    mThis(self)->parent = parent;

    return self;
    }

AtSerdesController Tha60210061RxauiLaneSerdesControllerNew(AtSerdesController parent, AtEthPort ethPort, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, parent, ethPort, serdesId);
    }

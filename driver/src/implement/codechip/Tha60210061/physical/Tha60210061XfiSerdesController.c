/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha60210061XfiSerdesController.c
 *
 * Created Date: Jun 18, 2016
 *
 * Description : 60210061 XFI SERDES controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/versionreader/ThaVersionReader.h"
#include "../../../default/man/ThaDevice.h"
#include "../prbs/Tha60210061ModulePrbs.h"
#include "../eth/Tha60210061ModuleEth.h"
#include "../prbs/Tha60210061SerdesRawPrbsEngineReg.h"
#include "../physicalV2/Tha60210061SerdesControlV2Reg.h"
#include "Tha60210061SerdesManager.h"
#include "Tha60210061SerdesInternal.h"
#include "Tha60210061DrpReg.h"
#include "Tha60210061SerdesTuningReg.h"
#include "Tha60210061SerdesTuningReg.h"
#include "Tha60210061TopCtrReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6Reg_Xfi_Drp_BaseAddress 0xf43000

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210061XfiSerdesController)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSerdesControllerMethods                m_AtSerdesControllerOverride;
static tTha6021EthPortSerdesControllerMethods    m_Tha6021EthPortSerdesControllerOverride;
static tTha6021EthPortXfiSerdesControllerMethods m_Tha6021EthPortXfiSerdesControllerOverride;

/* Save super implementation */
static const tAtSerdesControllerMethods *m_AtSerdesControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 HwIdGet(AtSerdesController self)
    {
    uint32 serdesId = AtSerdesControllerIdGet(self);

    /* XFI ports MDIO: 1, 2 */
    if (serdesId == cTha60210061XfiSerdesId0) return 0;
    if (serdesId == cTha60210061XfiSerdesId1) return 1;

    return cInvalidUint32;
    }

static uint32 XfiSerdesId(AtSerdesController self)
    {
    return (uint32)(AtSerdesControllerIdGet(self) - cTha60210061XfiSerdesId0);
    }

static eBool IsControllable(AtSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool EyeScanIsSupported(AtSerdesController self)
	{
	AtUnused(self);
	return cAtTrue;
	}

static uint32 EyeScanDrpBaseAddress(AtSerdesController self)
    {
    uint32 hwId = XfiSerdesId((AtSerdesController) self);
    return cAf6Reg_Xfi_Drp_BaseAddress + (hwId* 0x1000UL);
    }

static AtEyeScanController EyeScanControllerObjectCreate(AtSerdesController self, uint32 drpBaseAddress)
    {
    return Tha60210061EyeScanControllerGthE3New(self, drpBaseAddress);
    }

static uint32 TxPreCursorAddress(Tha6021EthPortSerdesController self)
    {
    AtUnused(self);
    return (uint32)(cAf6Reg_SerdesTurning_BaseAddress + cAf6Reg_Serdes_txprecursor_xfi_qsgmii_Base);
    }

static uint32 PreCursorMask(Tha6021EthPortSerdesController self, uint32 serdesId)
    {
    uint32 hwId = XfiSerdesId((AtSerdesController) self);
    AtUnused(serdesId);
    return (uint32)(cAf6_Serdes_txprecursor_xfi1gt_txprecursor_Mask << (hwId * 8UL));
    }

static uint32 PreCursorShift(Tha6021EthPortSerdesController self, uint32 serdesId)
    {
    uint32 hwId = XfiSerdesId((AtSerdesController) self);
    AtUnused(serdesId);
    return (uint32)(cAf6_Serdes_txprecursor_xfi1gt_txprecursor_Shift + (hwId * 8UL));
    }

static uint32 TxPostCursorAddress(Tha6021EthPortSerdesController self)
    {
    AtUnused(self);
    return (uint32)(cAf6Reg_SerdesTurning_BaseAddress + cAf6Reg_Serdes_txpostcursor_xfi_qsgmii_Base);
    }

static uint32 TxDiffCtrlAddress(Tha6021EthPortSerdesController self)
    {
    AtUnused(self);
    return (uint32)(cAf6Reg_SerdesTurning_BaseAddress + cAf6Reg_Serdes_txdiffctrl_xfi_qsgmii_Base);
    }

static uint32 DiffCtrlMask(Tha6021EthPortSerdesController self, uint32 serdesId)
    {
    uint32 hwId = XfiSerdesId((AtSerdesController) self);
    AtUnused(serdesId);
    return (uint32)(cAf6_Serdes_txdiffctrl_xfi1gt_txdiffctrl_Mask << (hwId * 4UL));
    }

static uint32 DiffCtrlShift(Tha6021EthPortSerdesController self, uint32 serdesId)
    {
    uint32 hwId = XfiSerdesId((AtSerdesController) self);
    AtUnused(serdesId);
    return (uint32)(cAf6_Serdes_txdiffctrl_xfi1gt_txdiffctrl_Shift + (hwId * 4UL));
    }

static uint32 LpmEnableRegister(Tha6021EthPortSerdesController self)
    {
    AtUnused(self);
    return (uint32)(cAf6Reg_SerdesTurning_BaseAddress + cAf6Reg_Serdes_rxlpmen_Base);
    }

static uint32 LpmEnableMask(Tha6021EthPortSerdesController self, uint32 serdesId)
    {
    uint32 hwId = XfiSerdesId((AtSerdesController) self);
    AtUnused(serdesId);
    return (uint32)(cAf6_Serdes_rxlpmen_xfi1gt_rxlpmen_Mask << (hwId));
    }

static uint32 LpmEnableShift(Tha6021EthPortSerdesController self, uint32 serdesId)
    {
    uint32 hwId = XfiSerdesId((AtSerdesController) self);
    AtUnused(serdesId);
    return (uint32)(cAf6_Serdes_rxlpmen_xfi1gt_rxlpmen_Shift + (hwId));
    }

static uint32 CoefReConfigure(Tha6021EthPortSerdesController self, uint32 serdesId)
    {
    AtUnused(self);
    AtUnused(serdesId);
    return cAtOk;
    }

static uint32 TuningV2Offset(AtSerdesController self)
    {
    return cSerdesTuningV2BaseAddress + AtSerdesControllerIdGet(self) * 0x800 + cHssi_serdes_id * 0x40;
    }

static uint32 LoopbackAddress(AtSerdesController self)
    {
    AtUnused(self);
    return (uint32)(cAf6Reg_SerdesTurning_BaseAddress + cAf6Reg_Serdes_gt_loopback_Base);
    }

static uint32 LoopbackMask(AtSerdesController self)
    {
    uint32 hwId = XfiSerdesId((AtSerdesController)self);
    return (uint32)(cAf6_Serdes_gt_loopback_xfi1gt_loopback_Mask << (hwId * 4UL));
    }

static uint32 LoopbackShift(AtSerdesController self)
    {
    uint32 hwId = XfiSerdesId((AtSerdesController)self);
    return (uint32)(cAf6_Serdes_gt_loopback_xfi1gt_loopback_Shift  + (hwId * 4UL));
    }

static eBool CanLoopback(AtSerdesController self, eAtLoopbackMode loopbackMode, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    switch ((uint32)loopbackMode)
        {
        case cAtLoopbackModeLocal:   return cAtTrue;
        case cAtLoopbackModeRemote:  return cAtTrue;
        case cAtLoopbackModeRelease: return cAtTrue;
        default:                     return cAtFalse;
        }
    }

static eAtRet HwLoopbackModeSet(AtSerdesController self, uint8 loopbackMode)
    {
    uint32 address = LoopbackAddress(self);
    uint32 regVal = AtSerdesControllerRead(self, address, cAtModuleSdh);

    mFieldIns(&regVal, LoopbackMask(self), LoopbackShift(self), loopbackMode);
    AtSerdesControllerWrite(self, address, regVal, cAtModuleSdh);

    return cAtOk;
    }

static eAtRet HwLoopbackSet(AtSerdesController self, eAtLoopbackMode loopbackMode)
    {
    Tha6021EthPortSerdesController controller = (Tha6021EthPortSerdesController)self;
    return mMethodsGet(controller)->HwLoopbackModeSet(self, Tha60210061SerdesLoopbackModeSw2Hw(self, loopbackMode));
    }

static uint8 HwLoopbackModeGet(AtSerdesController self)
    {
    uint32 regVal = AtSerdesControllerRead(self, LoopbackAddress(self), cAtModuleSdh);
    uint8 hwLoopbackMode;

    mFieldGet(regVal, LoopbackMask(self), LoopbackShift(self), uint8, &hwLoopbackMode);
    return hwLoopbackMode;
    }

static eAtLoopbackMode HwLoopbackGet(AtSerdesController self)
    {
    Tha6021EthPortSerdesController controller = (Tha6021EthPortSerdesController)self;
    return Tha60210061SerdesLoopbackModeHw2Sw(self, mMethodsGet(controller)->HwLoopbackModeGet(self));
    }

static eAtRet RemoteLoopbackExtraEnable(AtSerdesController self, eBool enable)
    {
    uint32 address, regVal;
    AtMdio mdio = AtSerdesControllerMdio(self);

    /* Enable Test pattern */
    AtMdioPortSelect(mdio, AtSerdesControllerHwIdGet(self));
    regVal = AtMdioRead(mdio, cMdioRegSerdesControl1Register);
    mRegFieldSet(regVal, cPrbs31TxPattern, enable ? 1 : 0);
    mRegFieldSet(regVal, cPrbs31RxPattern, enable ? 1 : 0);
    AtMdioWrite(mdio, cMdioRegSerdesControl1Register, regVal);

    /* Enable Reset TX PCS */
    address = cAf6Reg_upen_addr_04_Base + TuningV2Offset(self);
    regVal = AtSerdesControllerRead(self, address, cAtModulePrbs);
    mRegFieldSet(regVal, cAf6_upen_addr_04_GTSUBXRESET_REQ_, 0x2);
    AtSerdesControllerWrite(self, address, regVal, cAtModulePrbs);

    /* Enable TX Gear Box and TX Buff */
    address = cTxGearBoxEnable + AtSerdesControllerDrpBaseAddress(self);
    regVal = AtSerdesControllerRead(self, address, cAtModulePrbs);
    mRegFieldSet(regVal, cTxGearBox_en_, enable ? 0x0 : 0x1);
    mRegFieldSet(regVal, cTxBuf_en_, enable ? 0x1 : 0x0);
    mRegFieldSet(regVal, cTX_RxDetect_Ref_, 0x4);
    AtSerdesControllerWrite(self, address, regVal, cAtModulePrbs);

    /* If enable, using TXOUTCLKPMA
     * If disable, using TXPROGDIVCLK */
    address = cAf6Reg_upen_addr_09_Base + TuningV2Offset(self);
    regVal = AtSerdesControllerRead(self, address, cAtModulePrbs);
    mRegFieldSet(regVal, cAf6_upen_addr_09_TXOUTCLKSEL_, enable ? 0x2 : 0x5);
    AtSerdesControllerWrite(self, address, regVal, cAtModulePrbs);

    /* Disable Reset TX PCS */
    address = cAf6Reg_upen_addr_04_Base + TuningV2Offset(self);
    regVal = AtSerdesControllerRead(self, address, cAtModulePrbs);
    mRegFieldSet(regVal, cAf6_upen_addr_04_GTSUBXRESET_REQ_, 0x0);
    AtSerdesControllerWrite(self, address, regVal, cAtModulePrbs);

    return cAtOk;
    }

static eAtRet RemoteLoopbackEnable(AtSerdesController self, eBool enable, eBool needExtraEnable)
    {
    eAtRet ret = cAtOk;
    eBool hwWasSet = cAtFalse;

    if (enable)
        {
        ret = HwLoopbackSet(self, cAtLoopbackModeRemote);
        hwWasSet = cAtTrue;
        }

    else if (HwLoopbackGet(self) == cAtLoopbackModeRemote)
        {
        ret = HwLoopbackSet(self, cAtLoopbackModeRelease);
        hwWasSet = cAtTrue;
        }

    if ((!needExtraEnable) || (!hwWasSet))
        return ret;

    ret |= RemoteLoopbackExtraEnable(self, enable);
    return ret;
    }

static eAtRet LocalLoopbackEnable(AtSerdesController self, eBool enable)
    {
    if (enable)
        return HwLoopbackSet(self, cAtLoopbackModeLocal);

    if (HwLoopbackGet(self)== cAtLoopbackModeLocal)
        return HwLoopbackSet(self, cAtLoopbackModeRelease);

    return cAtOk;
    }

static AtModulePrbs ModulePrbs(AtSerdesController self)
    {
    AtDevice device = AtChannelDeviceGet(AtSerdesControllerPhysicalPortGet(self));
    return (AtModulePrbs)AtDeviceModuleGet(device, cAtModulePrbs);
    }

static eAtRet LoopbackEnable(AtSerdesController self, eAtLoopbackMode loopbackMode, eBool enable)
    {
    eBool usingRawPrsb = Tha60210061ModulePrbsRawPrsbIsSupported(ModulePrbs(self));
    eBool prbsIsEnabled = cAtFalse;
    AtPrbsEngine prbsEngine = NULL;
    eAtRet ret;

    if (usingRawPrsb)
        {
        prbsEngine = self->prbsEngine;
        prbsIsEnabled = (eBool)((prbsEngine) ? AtPrbsEngineIsEnabled(prbsEngine) : cAtFalse);
        if (prbsIsEnabled)
            AtPrbsEngineEnable(prbsEngine, cAtFalse);
        }

    /* Need extra enable when raw prbs is supported and prbs is not enabled */
    if (loopbackMode == cAtLoopbackModeRemote)
        ret = RemoteLoopbackEnable(self, enable, (usingRawPrsb && !prbsIsEnabled));

    else if (loopbackMode == cAtLoopbackModeLocal)
        ret = LocalLoopbackEnable(self, enable);

    else
        ret = HwLoopbackSet(self, cAtLoopbackModeRelease);

    if ((usingRawPrsb) && (prbsIsEnabled))
        AtPrbsEngineEnable(prbsEngine, cAtTrue);

    return ret;
    }

static eBool LoopbackIsEnabled(AtSerdesController self, eAtLoopbackMode loopbackMode)
    {
    return Tha60210061SerdesLoopbackIsEnabled(self, loopbackMode);
    }

static eBool EqualizerModeIsSupported(AtSerdesController self, eAtSerdesEqualizerMode mode)
    {
    AtUnused(self);
    if ((mode == cAtSerdesEqualizerModeDfe) || (mode == cAtSerdesEqualizerModeLpm))
        return cAtTrue;
    return cAtFalse;
    }

static eBool EqualizerCanControl(AtSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet Reset(AtSerdesController self)
    {
    uint32 serdesId   = AtSerdesControllerIdGet(self);
    uint32 offset     = (serdesId - cTha60210061XfiSerdesId0);
    uint32 triggerReg = cAf6Reg_SerdesTurning_BaseAddress + cAf6Reg_Serdes_PMA_Reset_Base;
    uint32 resetMask  = (cAf6_Serdes_PMA_Reset_xfi1gt_txpmareset_Mask << offset) |
                        (cAf6_Serdes_PMA_Reset_xfi1gt_rxpmareset_Mask << offset);

    return Tha60210061SerdesResetWithMask((AtSerdesController)self, triggerReg, resetMask, cDontNeedToWaitResetDone, cDontNeedToWaitResetDone);
    }

static eAtRet LayerLoopbackSet(AtSerdesController self, eAtSerdesPhysicalLayer layer, eAtLoopbackMode loopbackMode)
    {
    uint32 serdesId = AtSerdesControllerIdGet(self);
    uint32 regAddr = 0;
    uint32 regVal = 0;
    uint32 mask = 0;
    uint32 shift = 0;
    uint32 offset = ((serdesId - cTha60210061XfiSerdesId0) * 4);

    if ((serdesId < cTha60210061XfiSerdesId0) && (serdesId > cTha60210061XfiSerdesId1))
        return cAtErrorOutOfRangParm;

    regAddr = cAf6Reg_SerdesTurning_BaseAddress + cAf6Reg_Serdes_gt_loopback_Base;
    mask = cAf6_Serdes_gt_loopback_xfi1gt_loopback_Mask << offset;
    shift = cAf6_Serdes_gt_loopback_xfi1gt_loopback_Shift + offset;

    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    switch (loopbackMode)
        {
        case cAtLoopbackModeLocal:
            mFieldIns(&regVal, mask, shift, (layer == cAtSerdesPhysicalLayerPcs) ? cAf6SerdesLoopbackNearEndPcs : cAf6SerdesLoopbackNearEndPma);
            break;
        case cAtLoopbackModeRemote:
            mFieldIns(&regVal, mask, shift, (layer == cAtSerdesPhysicalLayerPcs) ? cAf6SerdesLoopbackFarEndPcs : cAf6SerdesLoopbackFarEndPma);
            break;
        case cAtLoopbackModeRelease:
            mFieldIns(&regVal, mask, shift, 0x0);
            break;
        case cAtLoopbackModeDual:
        default:
            return cAtErrorModeNotSupport;
        }

    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);
    return cAtOk;
    }

static uint32 TxSerdesEnableMask(uint32 serdesHwId)
    {
    switch (serdesHwId)
        {
        case 0: return cBit0;
        case 1: return cBit2;
        default: return 0;
        }
    }

static uint32 TxSerdesEnableShift(uint32 serdesHwId)
    {
    switch (serdesHwId)
        {
        case 0: return 0;
        case 1: return 2;
        default: return 0;
        }
    }

static eBool DiagPrbsEngineIsSupported(AtSerdesController self)
    {
    AtDevice device = AtSerdesControllerDeviceGet(self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);

    return (hwVersion < Tha60210061StartVersionRemoveXfiDiagPrbsEngine()) ? cAtTrue : cAtFalse;
    }

static AtPrbsEngine PrbsEngineCreate(AtSerdesController self)
    {
    AtModulePrbs modulePrbs = ModulePrbs(self);

    if (Tha60210061ModulePrbsRawPrsbIsSupported(modulePrbs))
        return Tha60210061XfiSerdesRawPrbsEngineNew(self);

    if (Tha60210061ModulePrbsEthFramePrbsIsSupported(modulePrbs))
        return Tha60210061SerdesEthFramePrbsEngineNew(self);

    if (DiagPrbsEngineIsSupported(self))
        return m_AtSerdesControllerMethods->PrbsEngineCreate(self);

    return NULL;
    }

static eAtRet LpmDfeReset(Tha6021EthPortSerdesController self)
    {
    uint32 serdesId = AtSerdesControllerIdGet((AtSerdesController)self);
    uint32 offset = (serdesId - cTha60210061XfiSerdesId0);

    uint32 triggerReg = cAf6Reg_SerdesTurning_BaseAddress + cAf6Reg_Serdes_PMA_Reset_Base;
    uint32 triggerMask = cAf6_Serdes_PMA_Reset_xfi1gt_rxpmareset_Mask << offset;

    uint32 doneReg = (uint32)(cAf6Reg_SerdesTurning_BaseAddress + cAf6Reg_Serdes_Turning_Status_Base);
    uint32 doneMask = cAf6_xfi1gt_rxresetdone_Mask << offset;

    return Tha60210061SerdesResetWithMask((AtSerdesController)self, triggerReg, triggerMask, doneReg, doneMask);
    }

static void OverrideTha6021EthPortXfiSerdesController(AtSerdesController self)
    {
    Tha6021EthPortXfiSerdesController controller = (Tha6021EthPortXfiSerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6021EthPortXfiSerdesControllerOverride, mMethodsGet(controller), sizeof(m_Tha6021EthPortXfiSerdesControllerOverride));

        mMethodOverride(m_Tha6021EthPortXfiSerdesControllerOverride, TxSerdesEnableMask);
        mMethodOverride(m_Tha6021EthPortXfiSerdesControllerOverride, TxSerdesEnableShift);
        }

    mMethodsSet(controller, &m_Tha6021EthPortXfiSerdesControllerOverride);
    }

static const char* Description(AtSerdesController self)
    {
    AtUnused(self);
    return "XFI";
    }

static void OverrideTha6021EthPortSerdesController(AtSerdesController self)
    {
    Tha6021EthPortSerdesController controller = (Tha6021EthPortSerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6021EthPortSerdesControllerOverride, mMethodsGet(controller), sizeof(m_Tha6021EthPortSerdesControllerOverride));

        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, TxPreCursorAddress);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, TxPostCursorAddress);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, PreCursorMask);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, PreCursorShift);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, TxDiffCtrlAddress);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, DiffCtrlMask);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, DiffCtrlShift);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, LpmEnableRegister);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, LpmEnableMask);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, LpmEnableShift);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, CoefReConfigure);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, HwLoopbackModeSet);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, HwLoopbackModeGet);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, LpmDfeReset);
        }

    mMethodsSet(controller, &m_Tha6021EthPortSerdesControllerOverride);
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSerdesControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, m_AtSerdesControllerMethods, sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, HwIdGet);
        mMethodOverride(m_AtSerdesControllerOverride, IsControllable);
        mMethodOverride(m_AtSerdesControllerOverride, CanLoopback);
        mMethodOverride(m_AtSerdesControllerOverride, LoopbackEnable);
        mMethodOverride(m_AtSerdesControllerOverride, LoopbackIsEnabled);
        mMethodOverride(m_AtSerdesControllerOverride, LayerLoopbackSet);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerModeIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerCanControl);
        mMethodOverride(m_AtSerdesControllerOverride, EyeScanIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, EyeScanControllerObjectCreate);
        mMethodOverride(m_AtSerdesControllerOverride, EyeScanDrpBaseAddress);
        mMethodOverride(m_AtSerdesControllerOverride, PrbsEngineCreate);
        mMethodOverride(m_AtSerdesControllerOverride, Description);
        mMethodOverride(m_AtSerdesControllerOverride, Reset);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtSerdesController(self);
    OverrideTha6021EthPortSerdesController(self);
    OverrideTha6021EthPortXfiSerdesController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061XfiSerdesController);
    }

AtSerdesController Tha60210061XfiSerdesControllerObjectInit(AtSerdesController self, AtEthPort ethPort, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051EthPortXfiSerdesControllerObjectInit(self, ethPort, serdesId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController Tha60210061XfiSerdesControllerNew(AtEthPort ethPort, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60210061XfiSerdesControllerObjectInit(newController, ethPort, serdesId);
    }

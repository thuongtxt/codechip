/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha60210061SerdesManager.c
 *
 * Created Date: Jun 17, 2016
 *
 * Description : 60210061 SERDES manager
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/physical/AtSerdesManagerInternal.h"
#include "../../../default/man/ThaDevice.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../eth/Tha60210061ModuleEth.h"
#include "Tha60210061SerdesManager.h"

/*--------------------------- Define -----------------------------------------*/
#define cLosFromSfpEnableReg   0xf00047
#define cLosFromSfpEnableMask  cBit1
#define cLosFromSfpEnableShift 1
#define cNumFuncNextResetSerdesAsyncInit 1

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210061SerdesManager)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061SerdesManager * Tha60210061SerdesManager;
typedef struct tTha60210061SerdesManager
    {
    tAtSerdesManager super;
    uint32 asyncInitState;
    uint32 goodSERDESesBitmap;
    }tTha60210061SerdesManager;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods        m_AtObjectOverride;
static tAtSerdesManagerMethods m_AtSerdesManagerOverride;

/* Save super implementation */
static const tAtObjectMethods        *m_AtObjectMethods        = NULL;
static const tAtSerdesManagerMethods  *m_AtSerdesManagerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 NumSerdesControllers(AtSerdesManager self)
    {
    AtUnused(self);
    /*
     * This product supports 10 SERDESes with IDs are as below
     * 0..3    4xSGMII at line side
     * 4..5    RXAUI
     * 6..7    XFI
     * 8..9    QSGMII
     */
    return 10;
    }

static eBool SerdesIsSGMII(uint32 serdesId)
    {
    if ((serdesId == cTha60210061SgmiiSerdesId0) ||
        (serdesId == cTha60210061SgmiiSerdesId1) ||
        (serdesId == cTha60210061SgmiiSerdesId2) ||
        (serdesId == cTha60210061SgmiiSerdesId3))
        return cAtTrue;

    return cAtFalse;
    }

static eBool SerdesIsRXAUI(uint32 serdesId)
    {
    if ((serdesId == cTha60210061RxauiSerdesId0) ||
        (serdesId == cTha60210061RxauiSerdesId1))
        return cAtTrue;

    return cAtFalse;
    }

static eBool SerdesIsXFI(uint32 serdesId)
    {
    if ((serdesId == cTha60210061XfiSerdesId0) ||
        (serdesId == cTha60210061XfiSerdesId1))
        return cAtTrue;

    return cAtFalse;
    }

static eBool SerdesIsQSGMII(uint32 serdesId)
    {
    if ((serdesId == cTha60210061QsgmiiSerdesId0) ||
        (serdesId == cTha60210061QsgmiiSerdesId1))
        return cAtTrue;

    return cAtFalse;
    }

static AtModuleEth ModuleEth(AtSerdesManager self)
    {
    return (AtModuleEth)AtDeviceModuleGet(AtSerdesManagerDeviceGet(self), cAtModuleEth);
    }

static AtEthPort XFIPort(AtSerdesManager self)
    {
    return Tha60210061ModuleEthXfiPortGet(ModuleEth(self));
    }

static AtEthPort SGMIIPort(AtSerdesManager self, uint32 serdesId)
    {
    return Tha60210061ModuleEthSgmiiPortGet(ModuleEth(self), serdesId);
    }

static AtEthPort RXAUIPort(AtSerdesManager self)
    {
    return Tha60210061ModuleEthRxauiPortGet(ModuleEth(self));
    }

static AtEthPort QsgmiiPort(AtSerdesManager self)
    {
    return Tha60210061ModuleEthQsgmiiPortGet(ModuleEth(self));
    }

static uint32 StartVersionSupportTuningV2(void)
    {
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x3, 0x3, 0x1010);
    }

static eBool SerdesTuningV2IsSupported(AtSerdesManager self)
    {
    AtDevice device = AtSerdesManagerDeviceGet(self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);

    return (hwVersion >= StartVersionSupportTuningV2()) ? cAtTrue : cAtFalse;
    }

static AtSerdesController SGMIISerdesCreate(AtSerdesManager self, uint32 serdesId)
    {
    if (SerdesTuningV2IsSupported(self))
        return Tha60210061SgmiiSerdesControllerV2New(SGMIIPort(self, serdesId), serdesId);

    return Tha60210061SgmiiSerdesControllerNew(SGMIIPort(self, serdesId), serdesId);
    }

static AtSerdesController RXAUISerdesCreate(AtSerdesManager self, uint32 serdesId)
    {
    if (SerdesTuningV2IsSupported(self))
        return Tha60210061RxauiSerdesControllerV2New(RXAUIPort(self), serdesId);

    return Tha60210061RxauiSerdesControllerNew(RXAUIPort(self), serdesId);
    }

static AtSerdesController XFISerdesCreate(AtSerdesManager self, uint32 serdesId)
    {
    if (SerdesTuningV2IsSupported(self))
        return Tha60210061XfiSerdesControllerV2New(XFIPort(self), serdesId);

    return Tha60210061XfiSerdesControllerNew(XFIPort(self), serdesId);
    }

static AtSerdesController QSGMIISerdesCreate(AtSerdesManager self, uint32 serdesId)
    {
    if (SerdesTuningV2IsSupported(self))
        return Tha60210061QsgmiiSerdesControllerV2New(QsgmiiPort(self), serdesId);

    return Tha60210061QsgmiiSerdesControllerNew(QsgmiiPort(self), serdesId);
    }

static AtSerdesController SerdesControllerObjectCreate(AtSerdesManager self, uint32 serdesId)
    {
    if (SerdesIsSGMII(serdesId))
        return SGMIISerdesCreate(self, serdesId);

    if (SerdesIsRXAUI(serdesId))
        return RXAUISerdesCreate(self, serdesId);

    if (SerdesIsXFI(serdesId))
        return XFISerdesCreate(self, serdesId);

    if (SerdesIsQSGMII(serdesId))
        return QSGMIISerdesCreate(self, serdesId);

    return NULL;
    }

static eAtRet LosFromSfpEnable(AtSerdesManager self)
    {
    AtHal hal = AtDeviceIpCoreHalGet(AtSerdesManagerDeviceGet(self), 0);
    uint32 regVal = AtHalRead(hal, cLosFromSfpEnableReg);

    mRegFieldSet(regVal, cLosFromSfpEnable, 1);
    AtHalWrite(hal, cLosFromSfpEnableReg, regVal);

    return cAtOk;
    }

static eBool AtLeastOneQsgmiiOk(AtSerdesManager self)
    {
    if ((mThis(self)->goodSERDESesBitmap & (cBit0 << cTha60210061QsgmiiSerdesId0)) ||
        (mThis(self)->goodSERDESesBitmap & (cBit0 << cTha60210061QsgmiiSerdesId1)))
        return cAtTrue;

    return cAtFalse;
    }

static eBool AtLeastOneXfiRxauiOk(AtSerdesManager self)
    {
    if ((mThis(self)->goodSERDESesBitmap & (cBit0 << cTha60210061RxauiSerdesId0)) ||
        (mThis(self)->goodSERDESesBitmap & (cBit0 << cTha60210061RxauiSerdesId1)) ||
        (mThis(self)->goodSERDESesBitmap & (cBit0 << cTha60210061XfiSerdesId0))   ||
        (mThis(self)->goodSERDESesBitmap & (cBit0 << cTha60210061XfiSerdesId1)))
        return cAtTrue;

    return cAtFalse;
    }

static eBool AllSgmiisOk(AtSerdesManager self)
    {
    if ((mThis(self)->goodSERDESesBitmap & (cBit0 << cTha60210061SgmiiSerdesId0)) &&
        (mThis(self)->goodSERDESesBitmap & (cBit0 << cTha60210061SgmiiSerdesId1)) &&
        (mThis(self)->goodSERDESesBitmap & (cBit0 << cTha60210061SgmiiSerdesId2)) &&
        (mThis(self)->goodSERDESesBitmap & (cBit0 << cTha60210061SgmiiSerdesId3)))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet AllSerdesReset(AtSerdesManager self)
    {
    uint32 numSerdes = mMethodsGet(self)->NumSerdesControllers(self);
    uint32 serdes_i;
    eAtRet ret = cAtOk;

    for (serdes_i = 0; serdes_i < numSerdes; serdes_i++)
        ret |= AtSerdesControllerReset(mMethodsGet(self)->SerdesControllerGet(self, serdes_i));

    if (ret == cAtOk)
    return ret;

    if (AllSgmiisOk(self) && AtLeastOneQsgmiiOk(self) && AtLeastOneXfiRxauiOk(self))
        return cAtOk;

    return cAtErrorSerdesResetTimeout;
    }

static eAtRet AllSerdesPrbsInit(AtSerdesManager self)
    {
    uint32 numSerdes = mMethodsGet(self)->NumSerdesControllers(self);
    uint32 serdes_i;
    eAtRet ret = cAtOk;

    for (serdes_i = 0; serdes_i < numSerdes; serdes_i++)
        {
        AtSerdesController serdes = mMethodsGet(self)->SerdesControllerGet(self, serdes_i);
        AtPrbsEngine prbsEngine = AtSerdesControllerPrbsEngine(serdes);
        if (prbsEngine)
            ret |= AtPrbsEngineInit(prbsEngine);
        }

    return ret;
    }

static eAtRet V2Init(AtSerdesManager self)
    {
    eAtRet ret = m_AtSerdesManagerMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    ret = AllSerdesReset(self);
    ret |= LosFromSfpEnable(self);
    ret |= AllSerdesPrbsInit(self);

    return ret;
    }

static eAtRet Init(AtSerdesManager self)
    {
    if (!SerdesTuningV2IsSupported(self))
        return m_AtSerdesManagerMethods->Init(self);

    return V2Init(self);
    }

static eAtRet SerdesControllerAsyncReset(AtSerdesManager self)
    {
    uint32 serdesId = mThis(self)->asyncInitState - 1;
    AtSerdesControllerReset(mMethodsGet(self)->SerdesControllerGet(self, serdesId));

    if (serdesId < (AtSerdesManagerNumSerdesControllers(self) - 1))
        return cAtOk;

    /* Last serdes has done */
    if (AllSgmiisOk(self) && AtLeastOneQsgmiiOk(self) && AtLeastOneXfiRxauiOk(self))
        return cAtOk;

    return cAtErrorSerdesResetTimeout;
    }

static eAtRet SuperAsyncInit(AtObject self)
    {
    return m_AtSerdesManagerMethods->AsyncInit((AtSerdesManager)self);
    }

static tAtAsyncOperationFunc NextInitOp(AtObject self, uint32 state)
    {
    uint32 numSerdes = AtSerdesManagerNumSerdesControllers((AtSerdesManager)self);
    if (state == 0)
        return SuperAsyncInit;

    if (state < numSerdes + 1)
        return (tAtAsyncOperationFunc)SerdesControllerAsyncReset;

    if (state == numSerdes + 1)
        return (tAtAsyncOperationFunc)LosFromSfpEnable;

    if (state == numSerdes + 2)
        return (tAtAsyncOperationFunc)AllSerdesPrbsInit;

    return NULL;
    }

static eAtRet AsyncInit(AtSerdesManager self)
    {
    if (!SerdesTuningV2IsSupported(self))
        return m_AtSerdesManagerMethods->AsyncInit(self);

    return AtDeviceObjectAsyncProcess(AtSerdesManagerDeviceGet(self), (AtObject)self, &mThis(self)->asyncInitState, NextInitOp);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60210061SerdesManager object = (Tha60210061SerdesManager)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeNone(asyncInitState);
    mEncodeUInt(goodSERDESesBitmap);
    }

static void OverrideAtSerdesManager(AtSerdesManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSerdesManagerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesManagerOverride, m_AtSerdesManagerMethods, sizeof(m_AtSerdesManagerOverride));

        mMethodOverride(m_AtSerdesManagerOverride, NumSerdesControllers);
        mMethodOverride(m_AtSerdesManagerOverride, SerdesControllerObjectCreate);
        mMethodOverride(m_AtSerdesManagerOverride, Init);
        mMethodOverride(m_AtSerdesManagerOverride, AsyncInit);
        }

    mMethodsSet(self, &m_AtSerdesManagerOverride);
    }

static void OverrideAtObject(AtSerdesManager self)
    {
    AtObject manager = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(manager);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(manager, &m_AtObjectOverride);
    }

static void Override(AtSerdesManager self)
    {
    OverrideAtObject(self);
    OverrideAtSerdesManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061SerdesManager);
    }

static AtSerdesManager ObjectInit(AtSerdesManager self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtSerdesManagerObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesManager Tha60210061SerdesManagerNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesManager newManager = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newManager == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newManager, device);
    }

uint32 Tha60210061SerdesManagerSerdesIdSwToHw(AtSerdesManager self, uint32 serdesId)
    {
    AtUnused(self);

    /* XFI ports */
    if (serdesId == 6) return 0;
    if (serdesId == 7) return 1;

    /* RXAUI ports */
    if (serdesId == 4) return 2;
    if (serdesId == 5) return 3;

    /* QSGMII ports */
    if (serdesId == 8) return 4;
    if (serdesId == 9) return 5;

    return cInvalidUint32;
    }

eBool Tha60210061SerdesIsSgmii(AtSerdesController serdes)
    {
    return SerdesIsSGMII(AtSerdesControllerIdGet(serdes));
    }

eBool Tha60210061SerdesIsQsgmii(AtSerdesController serdes)
    {
    return SerdesIsQSGMII(AtSerdesControllerIdGet(serdes));
    }

eBool Tha60210061SerdesIsXfi(AtSerdesController serdes)
    {
    return SerdesIsXFI(AtSerdesControllerIdGet(serdes));
    }

/* To control if we should return error when a SERDES resets timeout in device init */
void Tha60210061SerdesManagerSerdesResetResultCache(AtSerdesManager self, AtSerdesController serdes, eBool isOk)
    {
    if (isOk)
        mThis(self)->goodSERDESesBitmap |= cBit0 << AtSerdesControllerIdGet(serdes);
    else
        mThis(self)->goodSERDESesBitmap &= ~(cBit0 << AtSerdesControllerIdGet(serdes));
    }

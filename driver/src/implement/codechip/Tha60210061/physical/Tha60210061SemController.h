/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAN
 * 
 * File        : Tha60210061SemController.h
 * 
 * Created Date: Oct 19, 2016
 *
 * Description : SEM Controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061SEMCONTROLLER_H_
#define _THA60210061SEMCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSemController Tha60210061SemControllerNew(AtDevice device, uint32 semId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061SEMCONTROLLER_H_ */


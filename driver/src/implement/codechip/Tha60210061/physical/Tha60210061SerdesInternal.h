/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha60210061SerdesInternal.h
 * 
 * Created Date: Jun 30, 2016
 *
 * Description : 60210061 SERDESes internal definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061SERDESINTERNAL_H_
#define _THA60210061SERDESINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210051/physical/Tha60210051EthPortXfiSerdesController.h"
#include "../../Tha60210031/physical/Tha6021EthPortQsgmiiSerdesControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cDontNeedToWaitResetDone 0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210061XfiSerdesController *Tha60210061XfiSerdesController;
typedef struct tTha60210061XfiSerdesController
    {
    tTha60210051EthPortXfiSerdesController super;
    }tTha60210061XfiSerdesController;

typedef struct tTha60210061RxauiSerdesController
    {
    tTha60210051EthPortXfiSerdesController super;
    AtSerdesController lanePorts[2];
    }tTha60210061RxauiSerdesController;

typedef struct tTha60210061SgmiiSerdesController
    {
    tTha60210061XfiSerdesController super;

    /* Private database */
    eAtSerdesMode mode;
    }tTha60210061SgmiiSerdesController;

typedef struct tTha60210061QsgmiiSerdesController
    {
    tTha6021EthPortQsgmiiSerdesController super;
    }tTha60210061QsgmiiSerdesController;

typedef struct tTha60210061XfiSerdesControllerV2
    {
    tTha60210061XfiSerdesController super;
    }tTha60210061XfiSerdesControllerV2;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSerdesController Tha60210061XfiSerdesControllerObjectInit(AtSerdesController self, AtEthPort ethPort, uint32 serdesId);
AtSerdesController Tha60210061RxauiSerdesControllerObjectInit(AtSerdesController self, AtEthPort ethPort, uint32 serdesId);
AtSerdesController Tha60210061SgmiiSerdesControllerObjectInit(AtSerdesController self, AtEthPort ethPort, uint32 serdesId);
AtSerdesController Tha60210061QsgmiiSerdesControllerObjectInit(AtSerdesController self, AtEthPort ethPort, uint32 serdesId);

eAtRet Tha60210061SerdesLoopbackEnable(AtSerdesController self, eAtLoopbackMode loopbackMode, eBool enable);
eBool Tha60210061SerdesLoopbackIsEnabled(AtSerdesController self, eAtLoopbackMode loopbackMode);
uint8 Tha60210061SerdesLoopbackModeSw2Hw(AtSerdesController self, eAtLoopbackMode loopbackMode);
eAtLoopbackMode Tha60210061SerdesLoopbackModeHw2Sw(AtSerdesController self, uint8 hwLoopbackMode);
eAtRet Tha60210061SerdesResetWithMask(AtSerdesController self, uint32 resetReg, uint32 resetMask, uint32 statusReg, uint32 doneMask);

AtSerdesController Tha60210061RxauiLaneSerdesControllerNew(AtSerdesController parent, AtEthPort ethPort, uint32 serdesId);
AtPrbsEngine Tha60210061RxauiSerdesLaneRawPrbsEngineNew(AtSerdesController serdesController);

AtEyeScanController Tha60210061EyeScanControllerGthE3New(AtSerdesController serdesController, uint32 drpBaseAddress);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061SERDESINTERNAL_H_ */


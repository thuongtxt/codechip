/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha60210061RxauiSerdesController.c
 *
 * Created Date: Jun 29, 2016
 *
 * Description : 60210061 RXAUI SERDES controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/man/versionreader/ThaVersionReader.h"
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60210051/physical/Tha60210051EthPortXfiSerdesController.h"
#include "../prbs/Tha60210061ModulePrbs.h"
#include "../eth/Tha60210061ModuleEth.h"
#include "Tha60210061SerdesManager.h"
#include "Tha60210061RxauiSerdesTuningReg.h"
#include "Tha60210061SerdesInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210061RxauiSerdesController*)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods                          m_AtObjectOverride;
static tAtSerdesControllerMethods                m_AtSerdesControllerOverride;
static tTha6021EthPortXfiSerdesControllerMethods m_Tha6021EthPortXfiSerdesControllerOverride;
static tTha6021EthPortSerdesControllerMethods    m_Tha6021EthPortSerdesControllerOverride;

/* Save super implementation */
static const tAtObjectMethods           *m_AtObjectMethods           = NULL;
static const tAtSerdesControllerMethods *m_AtSerdesControllerMethods = NULL;
static const tTha6021EthPortSerdesControllerMethods *m_Tha6021EthPortSerdesControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 HwIdGet(AtSerdesController self)
    {
    uint32 serdesId = AtSerdesControllerIdGet(self);

    if (serdesId == cTha60210061RxauiSerdesId0) return 6;
    if (serdesId == cTha60210061RxauiSerdesId1) return 7;

    return cInvalidUint32;
    }

static eBool EyeScanIsSupported(AtSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 EyeScanDrpBaseAddress(AtSerdesController self)
    {
    uint32 serdesId = AtSerdesControllerIdGet(self);
    if (serdesId == cTha60210061RxauiSerdesId0)
        return 0xF2B000UL;

    return 0xF2D000UL;
    }

static AtEyeScanController EyeScanControllerObjectCreate(AtSerdesController self, uint32 drpBaseAddress)
    {
    return Tha60210061EyeScanControllerGthE3New(self, drpBaseAddress);
    }

static eBool IsControllable(AtSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 TxSerdesEnableMask(uint32 serdesHwId)
    {
    switch (serdesHwId)
        {
        case 6: return cBit1;
        case 7: return cBit3;
        default: return 0;
        }
    }

static uint32 TxSerdesEnableShift(uint32 serdesHwId)
    {
    switch (serdesHwId)
        {
        case 6: return 1;
        case 7: return 3;
        default: return 0;
        }
    }

static uint32 SerdesTuningBaseAddress(AtSerdesController self)
    {
    uint32 hwId = AtSerdesControllerIdGet(self);
    return (hwId == cTha60210061RxauiSerdesId0) ? cTha60210061RXaui0SerdesTuningBaseAddress : cTha60210061RXaui1SerdesTuningBaseAddress;
    }

static eAtRet Reset(AtSerdesController self)
    {
    uint32 txRegAddr     = SerdesTuningBaseAddress(self) + cAf6Reg_SERDES_TX_Reset_Base;
    uint32 txTriggerMask = cAf6_SERDES_TX_Reset_txrst_trig_Mask;
    uint32 txDoneMask    = cAf6_SERDES_TX_Reset_txrst_done_Mask;

    uint32 rxRegAddr     = SerdesTuningBaseAddress(self) + cAf6Reg_SERDES_RX_Reset_Base;
    uint32 rxTriggerMask = cAf6_SERDES_RX_Reset_rxrst_trig_Mask;
    uint32 rxDoneMask    = cAf6_SERDES_RX_Reset_rxrst_done_Mask;

    eAtRet ret  = Tha60210061SerdesResetWithMask(self, txRegAddr, txTriggerMask, txRegAddr, txDoneMask);
    ret |= Tha60210061SerdesResetWithMask(self, rxRegAddr, rxTriggerMask, rxRegAddr, rxDoneMask);

    return ret;
    }

static uint32 LoopbackAddress(AtSerdesController self)
    {
    return (uint32)(SerdesTuningBaseAddress(self) + cAf6Reg_SERDES_LoopBack_Base);
    }

static eAtRet HwLoopbackModeSet(AtSerdesController self, uint8 hwLoopbackMode)
    {
    uint32 address = LoopbackAddress(self);
    uint32 regVal = AtSerdesControllerRead(self, address, cAtModuleEth);

    mRegFieldSet(regVal, cAf6_SERDES_LoopBack_lpback_subport0_, hwLoopbackMode);
    mRegFieldSet(regVal, cAf6_SERDES_LoopBack_lpback_subport1_, hwLoopbackMode);
    AtSerdesControllerWrite(self, address, regVal, cAtModuleEth);

    return cAtOk;
    }

static uint8 HwLoopbackModeGet(AtSerdesController self)
    {
    uint32 address = LoopbackAddress(self);
    uint32 regVal = AtSerdesControllerRead(self, address, cAtModuleEth);

    return (uint8)mRegField(regVal, cAf6_SERDES_LoopBack_lpback_subport0_);
    }

static eAtRet LoopbackEnable(AtSerdesController self, eAtLoopbackMode loopbackMode, eBool enable)
    {
    return Tha60210061SerdesLoopbackEnable(self, loopbackMode, enable);
    }

static eBool LoopbackIsEnabled(AtSerdesController self, eAtLoopbackMode loopbackMode)
    {
    return Tha60210061SerdesLoopbackIsEnabled(self, loopbackMode);
    }

static eBool CanLoopback(AtSerdesController self, eAtLoopbackMode loopbackMode, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);

    switch ((uint32)loopbackMode)
        {
        case cAtLoopbackModeLocal:   return cAtTrue;
        case cAtLoopbackModeRemote:  return cAtTrue;
        case cAtLoopbackModeRelease: return cAtTrue;
        default:                     return cAtFalse;
        }
    }

static eAtRet PowerDown(AtSerdesController self, eBool powerDown)
    {
    uint32 regAddress = SerdesTuningBaseAddress(self) + cAf6Reg_SERDES_POWER_DOWN_Base;
    uint32 regVal = AtSerdesControllerRead(self, regAddress, cAtModuleEth);

    mRegFieldSet(regVal, cAf6_SERDES_POWER_DOWN_RXPD0_, powerDown ? 0x3 : 0x0);
    mRegFieldSet(regVal, cAf6_SERDES_POWER_DOWN_TXPD0_, powerDown ? 0x3 : 0x0);
    mRegFieldSet(regVal, cAf6_SERDES_POWER_DOWN_RXPD1_, powerDown ? 0x3 : 0x0);
    mRegFieldSet(regVal, cAf6_SERDES_POWER_DOWN_TXPD1_, powerDown ? 0x3 : 0x0);

    AtSerdesControllerWrite(self, regAddress, regVal, cAtModuleEth);
    return cAtOk;
    }

static eBool PowerIsDown(AtSerdesController self)
    {
    uint32 regAddress = SerdesTuningBaseAddress(self) + cAf6Reg_SERDES_POWER_DOWN_Base;
    uint32 regVal = AtSerdesControllerRead(self, regAddress, cAtModuleEth);
    uint32 hwPowerdown;

    hwPowerdown = mRegField(regVal, cAf6_SERDES_POWER_DOWN_RXPD0_);
    hwPowerdown &= mRegField(regVal, cAf6_SERDES_POWER_DOWN_TXPD0_);
    hwPowerdown &= mRegField(regVal, cAf6_SERDES_POWER_DOWN_RXPD1_);
    hwPowerdown &= mRegField(regVal, cAf6_SERDES_POWER_DOWN_TXPD1_);

    return (hwPowerdown == 0x3) ? cAtTrue : cAtFalse;
    }

static eBool PowerCanControl(AtSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet HwTxCursorSet(AtSerdesController self, uint32 address, uint32 value)
    {
    uint32 regVal = AtSerdesControllerRead(self, address, cAtModuleEth);

    mRegFieldSet(regVal, cAf6_SERDES_TXPOSTCURSOR_TXPOSTCURSOR_subport0_, value);
    mRegFieldSet(regVal, cAf6_SERDES_TXPOSTCURSOR_TXPOSTCURSOR_subport1_, value);
    AtSerdesControllerWrite(self, address, regVal, cAtModuleEth);

    return cAtOk;
    }

static eAtRet HwTxDiffCtrlSet(AtSerdesController self, uint32 value)
    {
    uint32 address = SerdesTuningBaseAddress(self) + cAf6Reg_SERDES_TXDIFFCTRL_Base;
    uint32 regVal = AtSerdesControllerRead(self, address, cAtModuleEth);

    mRegFieldSet(regVal, cAf6_SERDES_TXDIFFCTRL_TXDIFFCTRL_subport0_, value);
    mRegFieldSet(regVal, cAf6_SERDES_TXDIFFCTRL_TXDIFFCTRL_subport1_, value);

    AtSerdesControllerWrite(self, address, regVal, cAtModuleEth);
    return cAtOk;
    }

static eAtRet HwPhysicalParamSet(AtSerdesController self, eAtSerdesParam param, uint32 value)
    {
    if (param == cAtSerdesParamTxPreCursor)
        return HwTxCursorSet(self, SerdesTuningBaseAddress(self) + cAf6Reg_SERDES_TXPRECURSOR_Base, value);

    if (param == cAtSerdesParamTxPostCursor)
        return HwTxCursorSet(self, SerdesTuningBaseAddress(self) + cAf6Reg_SERDES_TXPOSTCURSOR_Base, value);

    if (param == cAtSerdesParamTxDiffCtrl)
        return HwTxDiffCtrlSet(self, value);

    return cAtOk;
    }

static uint32 TxPreCursorAddress(Tha6021EthPortSerdesController self)
    {
    return SerdesTuningBaseAddress((AtSerdesController)self) + cAf6Reg_SERDES_TXPRECURSOR_Base;
    }

static uint32 TxPostCursorAddress(Tha6021EthPortSerdesController self)
    {
    return SerdesTuningBaseAddress((AtSerdesController)self) + cAf6Reg_SERDES_TXPOSTCURSOR_Base;
    }

static uint32 PreCursorMask(Tha6021EthPortSerdesController self, uint32 serdesId)
    {
    AtUnused(self);
    AtUnused(serdesId);
    return cAf6_SERDES_TXPOSTCURSOR_TXPOSTCURSOR_subport0_Mask;
    }

static uint32 PreCursorShift(Tha6021EthPortSerdesController self, uint32 serdesId)
    {
    AtUnused(self);
    AtUnused(serdesId);
    return cAf6_SERDES_TXPOSTCURSOR_TXPOSTCURSOR_subport0_Shift;
    }

static uint32 TxDiffCtrlAddress(Tha6021EthPortSerdesController self)
    {
    return SerdesTuningBaseAddress((AtSerdesController)self) + cAf6Reg_SERDES_TXDIFFCTRL_Base;
    }

static uint32 DiffCtrlMask(Tha6021EthPortSerdesController self, uint32 serdesId)
    {
    AtUnused(self);
    AtUnused(serdesId);
    return cAf6_SERDES_TXDIFFCTRL_TXDIFFCTRL_subport0_Mask;
    }

static uint32 DiffCtrlShift(Tha6021EthPortSerdesController self, uint32 serdesId)
    {
    AtUnused(self);
    AtUnused(serdesId);
    return cAf6_SERDES_TXDIFFCTRL_TXDIFFCTRL_subport0_Shift;
    }

static eAtRet PhysicalParamSet(AtSerdesController self, eAtSerdesParam param, uint32 value)
    {
    if (mMethodsGet(self)->PhysicalParamValueIsInRange(self, param, value))
        return HwPhysicalParamSet(self, param, value);

    return cAtErrorOutOfRangParm;
    }

static eAtRet DefaultParamsSet(AtSerdesController self)
    {
    eAtRet ret = cAtOk;

    ret |= AtSerdesControllerPhysicalParamSet(self, cAtSerdesParamTxDiffCtrl, 0xC);
    ret |= AtSerdesControllerPhysicalParamSet(self, cAtSerdesParamTxPostCursor, 0x14);
    ret |= AtSerdesControllerPhysicalParamSet(self, cAtSerdesParamTxPreCursor, 0);

    return ret;
    }

static eAtRet Init(AtSerdesController self)
    {
    eAtRet ret = m_AtSerdesControllerMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return DefaultParamsSet(self);
    }

static tAtAsyncOperationFunc AsyncOperationGet(AtSerdesController self, uint32 state)
    {
    AtUnused(self);
    if (state == 0) return (tAtAsyncOperationFunc)(m_AtSerdesControllerMethods->Init);
    if (state == 1) return (tAtAsyncOperationFunc)DefaultParamsSet;
    return NULL;
    }

static eBool EqualizerCanControl(AtSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool DiagPrbsEngineIsSupported(AtSerdesController self)
    {
    AtDevice device = AtSerdesControllerDeviceGet(self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);

    return (hwVersion < Tha60210061StartVersionRemoveXfiDiagPrbsEngine()) ? cAtTrue : cAtFalse;
    }

static AtModulePrbs ModulePrbs(AtSerdesController self)
    {
    AtDevice device = AtChannelDeviceGet(AtSerdesControllerPhysicalPortGet(self));
    return (AtModulePrbs)AtDeviceModuleGet(device, cAtModulePrbs);
    }

static AtPrbsEngine PrbsEngineCreate(AtSerdesController self)
    {
    AtModulePrbs modulePrbs = ModulePrbs(self);

    if (Tha60210061ModulePrbsRawPrsbIsSupported(modulePrbs))
        return Tha60210061RxauiSerdesRawPrbsEngineNew(self);

    if (Tha60210061ModulePrbsEthFramePrbsIsSupported(modulePrbs))
        return Tha60210061SerdesEthFramePrbsEngineNew(self);

    if (DiagPrbsEngineIsSupported(self))
        return m_AtSerdesControllerMethods->PrbsEngineCreate(self);

    return NULL;
    }

static uint32 LpmEnableRegister(AtSerdesController self)
    {
    return SerdesTuningBaseAddress(self) + cAf6Reg_SERDES_LPMDFE_Mode_Base;
    }

static eAtRet LpmDfeReset(AtSerdesController self)
    {
    uint32 regAddr   = SerdesTuningBaseAddress(self) + cAf6Reg_SERDES_LPMDFE_Reset_Base;
    uint32 resetMask = cAf6_SERDES_LPMDFE_Reset_lpmdfe_reset_Mask;

    return Tha60210061SerdesResetWithMask(self, regAddr, resetMask, cDontNeedToWaitResetDone, cDontNeedToWaitResetDone);
    }

static uint32 EqualizerModeSw2Hw(eAtSerdesEqualizerMode mode)
    {
    if (mode == cAtSerdesEqualizerModeDfe)  return 0;
    if (mode == cAtSerdesEqualizerModeLpm)  return 3;

    return 0x0;
    }

static eAtRet EqualizerModeSet(AtSerdesController self, eAtSerdesEqualizerMode mode)
    {
    uint32 regVal, address;

    if (!AtSerdesControllerEqualizerModeIsSupported(self, mode))
        return cAtErrorModeNotSupport;

    address = LpmEnableRegister(self);
    regVal = AtSerdesControllerRead(self, address, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_SERDES_LPMDFE_Mode_lpmdfe_mode_, EqualizerModeSw2Hw(mode));
    AtSerdesControllerWrite(self, address, regVal, cAtModuleEth);

    return LpmDfeReset(self);
    }

static eAtSerdesEqualizerMode EqualizerModeHw2Sw(uint32 mode)
    {
    if (mode == 0) return cAtSerdesEqualizerModeDfe;
    if (mode == 3) return cAtSerdesEqualizerModeLpm;
    return cAtSerdesEqualizerModeUnknown;
    }

static eAtSerdesEqualizerMode EqualizerModeGet(AtSerdesController self)
    {
    uint32 address = LpmEnableRegister(self);
    uint32 regVal = AtSerdesControllerRead(self, address, cAtModuleEth);
    uint32 lpmEnabled = mRegField(regVal, cAf6_SERDES_LPMDFE_Mode_lpmdfe_mode_);

    return EqualizerModeHw2Sw(lpmEnabled);
    }

static eBool EqualizerModeIsSupported(AtSerdesController self, eAtSerdesEqualizerMode mode)
    {
    AtUnused(self);
    if ((mode == cAtSerdesEqualizerModeDfe) || (mode == cAtSerdesEqualizerModeLpm))
        return cAtTrue;

    return cAtFalse;
    }

static const char* Description(AtSerdesController self)
    {
    AtUnused(self);
    return "RXAUI";
    }

static eAtSerdesEqualizerMode DefaultEqualizerMode(AtSerdesController self)
    {
    AtUnused(self);
    return cAtSerdesEqualizerModeLpm;
    }

static uint32 AlarmGet(AtSerdesController self)
    {
    AtUnused(self); /* Currently, HW does not support */
    return 0;
    }

static AtSerdesController LanePortObjectCreate(AtSerdesController self, uint32 laneIdx)
    {
    AtEthPort ethPort = (AtEthPort)AtSerdesControllerPhysicalPortGet(self);
    return Tha60210061RxauiLaneSerdesControllerNew(self, ethPort, laneIdx);
    }

static AtSerdesController LaneGet(AtSerdesController self, uint32 laneIdx)
    {
    if (mThis(self)->lanePorts[laneIdx] == NULL)
        {
        AtSerdesController serdesController = LanePortObjectCreate(self, laneIdx);
        mThis(self)->lanePorts[laneIdx] = serdesController;
        return serdesController;
        }

    return mThis(self)->lanePorts[laneIdx];
    }

static void AllLanePortDelete(AtSerdesController self)
    {
    AtSerdesController serdesController;
    uint32 laneIdx;
    uint32 maxNumLane = mMethodsGet(self)->NumLanes(self);

    for (laneIdx = 0; laneIdx < maxNumLane; laneIdx++)
        {
        serdesController = mThis(self)->lanePorts[laneIdx];

        if (serdesController != NULL)
            {
            AtObjectDelete((AtObject)serdesController);
            mThis(self)->lanePorts[laneIdx] = NULL;
            }
        }
    }

static void Delete(AtObject self)
    {
    AllLanePortDelete((AtSerdesController)self);

    /* Super deletion */
    m_AtObjectMethods->Delete(self);
    }

static uint32 NumLanes(AtSerdesController self)
    {
    AtUnused(self);
    return 2;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210061RxauiSerdesController *object = mThis(self);
    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObjects(lanePorts, mCount(object->lanePorts));
    }

static void OverrideTha6021EthPortSerdesController(AtSerdesController self)
    {
    Tha6021EthPortSerdesController controller = (Tha6021EthPortSerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha6021EthPortSerdesControllerMethods = mMethodsGet(controller);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6021EthPortSerdesControllerOverride, m_Tha6021EthPortSerdesControllerMethods, sizeof(m_Tha6021EthPortSerdesControllerOverride));

        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, HwLoopbackModeSet);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, HwLoopbackModeGet);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, TxPreCursorAddress);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, TxPostCursorAddress);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, PreCursorMask);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, PreCursorShift);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, TxDiffCtrlAddress);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, DiffCtrlMask);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, DiffCtrlShift);
        }

    mMethodsSet(controller, &m_Tha6021EthPortSerdesControllerOverride);
    }

static void OverrideTha6021EthPortXfiSerdesController(AtSerdesController self)
    {
    Tha6021EthPortXfiSerdesController controller = (Tha6021EthPortXfiSerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6021EthPortXfiSerdesControllerOverride, mMethodsGet(controller), sizeof(m_Tha6021EthPortXfiSerdesControllerOverride));

        mMethodOverride(m_Tha6021EthPortXfiSerdesControllerOverride, TxSerdesEnableMask);
        mMethodOverride(m_Tha6021EthPortXfiSerdesControllerOverride, TxSerdesEnableShift);
        }

    mMethodsSet(controller, &m_Tha6021EthPortXfiSerdesControllerOverride);
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSerdesControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, m_AtSerdesControllerMethods, sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, HwIdGet);
        mMethodOverride(m_AtSerdesControllerOverride, IsControllable);
        mMethodOverride(m_AtSerdesControllerOverride, EyeScanIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, EyeScanControllerObjectCreate);
        mMethodOverride(m_AtSerdesControllerOverride, EyeScanDrpBaseAddress);
        mMethodOverride(m_AtSerdesControllerOverride, LoopbackEnable);
        mMethodOverride(m_AtSerdesControllerOverride, LoopbackIsEnabled);
        mMethodOverride(m_AtSerdesControllerOverride, CanLoopback);
        mMethodOverride(m_AtSerdesControllerOverride, PowerDown);
        mMethodOverride(m_AtSerdesControllerOverride, PowerIsDown);
        mMethodOverride(m_AtSerdesControllerOverride, PowerCanControl);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalParamSet);
        mMethodOverride(m_AtSerdesControllerOverride, Init);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerCanControl);
        mMethodOverride(m_AtSerdesControllerOverride, PrbsEngineCreate);
        mMethodOverride(m_AtSerdesControllerOverride, Description);
        mMethodOverride(m_AtSerdesControllerOverride, AsyncOperationGet);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerModeSet);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerModeGet);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerModeIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, DefaultEqualizerMode);
        mMethodOverride(m_AtSerdesControllerOverride, AlarmGet);
        mMethodOverride(m_AtSerdesControllerOverride, LaneGet);
        mMethodOverride(m_AtSerdesControllerOverride, NumLanes);
        mMethodOverride(m_AtSerdesControllerOverride, Reset);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void OverrideAtObject(AtSerdesController self)
     {
     AtObject object = (AtObject)self;
     if (!m_methodsInit)
         {
         AtOsal osal = AtSharedDriverOsalGet();
         m_AtObjectMethods = mMethodsGet(object);
         mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

         mMethodOverride(m_AtObjectOverride, Delete);
         mMethodOverride(m_AtObjectOverride, Serialize);
         }

     mMethodsSet(object, &m_AtObjectOverride);
     }

static void Override(AtSerdesController self)
    {
    OverrideAtObject(self);
    OverrideAtSerdesController(self);
    OverrideTha6021EthPortSerdesController(self);
    OverrideTha6021EthPortXfiSerdesController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061RxauiSerdesController);
    }

AtSerdesController Tha60210061RxauiSerdesControllerObjectInit(AtSerdesController self, AtEthPort ethPort, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051EthPortXfiSerdesControllerObjectInit(self, ethPort, serdesId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController Tha60210061RxauiSerdesControllerNew(AtEthPort ethPort, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60210061RxauiSerdesControllerObjectInit(newController, ethPort, serdesId);
    }

uint32 Tha60210061RxauiSerdesControllerTuningBaseAddress(AtSerdesController self)
    {
    if (self)
        return SerdesTuningBaseAddress(self);

    return cInvalidUint32;
    }

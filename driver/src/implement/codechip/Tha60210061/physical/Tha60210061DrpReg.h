/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha60210061DrpReg.h
 * 
 * Created Date: Jun 27, 2016
 *
 * Description : DRP registers
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061DRPREG_H_
#define _THA60210061DRPREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cAf6Reg_drp_BaseAddress     0xf41000
#define cAf6SerdesLoopbackNormal        0x0
#define cAf6SerdesLoopbackNearEndPcs    0x1
#define cAf6SerdesLoopbackNearEndPma    0x2
#define cAf6SerdesLoopbackFarEndPcs     0x6
#define cAf6SerdesLoopbackFarEndPma     0x4

/*------------------------------------------------------------------------------
Reg Name   : DRP
Reg Addr   :
Reg Formula: Address + (0x100 * LineId * NumPort)
    Where  :
           + $Address
           + $LineId(0-3)
           + $NumPort = 4
Reg Desc   : Configure DRP for initializing SFP for STM1, STM4, STM16

------------------------------------------------------------------------------*/
#define cAf6Reg_drp(addr, LineId)                                          ((addr)+0x100*(LineId)*4)
#define cAf6Reg_drp_WidthVal                                                                      32
#define cAf6Reg_drp_WriteMask                                                                    0x0

/*------------------------------------------------------------------------------
Reg Name   : DRP ADDRESS 0x03
Reg Addr   : 0x03
Reg Formula: 0x03 + (0x100 * LineId * NumPort)
    Where  :
           + $LineId(0-3)
           + $NumPort = 4
Reg Desc   : Configure DRP for initializing SFP for STM1, STM4, STM16

------------------------------------------------------------------------------*/
#define cAf6Reg_drp_addr_03h_Base                                                                        0x03
#define cAf6Reg_drp_addr_03h(LineId)                                                  (0x03+0x100*(LineId)*4)
#define cAf6Reg_drp_addr_03h_WidthVal                                                                      32
#define cAf6Reg_drp_addr_03h_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: RX_DATA_WIDTH
BitField Type: r/w
BitField Desc:
BitField Bits: [8:5]
--------------------------------------*/
#define cAf6_drp_rx_data_width_Bit_Start                                                           5
#define cAf6_drp_rx_data_width_Bit_End                                                             8
#define cAf6_drp_rx_data_width_Mask                                                          cBit8_5
#define cAf6_drp_rx_data_width_Shift                                                               5
#define cAf6_drp_rx_data_width_MaxVal                                                            0xf
#define cAf6_drp_rx_data_width_MinVal                                                            0x0
#define cAf6_drp_rx_data_width_RstVal                                                            0x0

/*------------------------------------------------------------------------------
Reg Name   : DRP ADDRESS 0x10
Reg Addr   : 0x10
Reg Formula: 0x10 + (0x100 * LineId * NumPort)
    Where  :
           + $LineId(0-3)
           + $NumPort = 4
Reg Desc   : Configure DRP for initializing SFP for STM1, STM4, STM16

------------------------------------------------------------------------------*/
#define cAf6Reg_drp_addr_10h_Base                                                                        0x10
#define cAf6Reg_drp_addr_10h(LineId)                                                  (0x10+0x100*(LineId)*4)
#define cAf6Reg_drp_addr_10h_WidthVal                                                                      32
#define cAf6Reg_drp_addr_10h_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: RXCDR_CFG2
BitField Type: r/w
BitField Desc:
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_drp_rxcdr_cfg2_Bit_Start                                                           0
#define cAf6_drp_rxcdr_cfg2_Bit_End                                                            15
#define cAf6_drp_rxcdr_cfg2_Mask                                                         cBit15_0
#define cAf6_drp_rxcdr_cfg2_Shift                                                               0
#define cAf6_drp_rxcdr_cfg2_MaxVal                                                         0xffff
#define cAf6_drp_rxcdr_cfg2_MinVal                                                            0x0
#define cAf6_drp_rxcdr_cfg2_RstVal                                                            0x0

/*------------------------------------------------------------------------------
Reg Name   : DRP ADDRESS 0x1E
Reg Addr   : 0x1E
Reg Formula: 0x1E + (0x100 * LineId * NumPort)
    Where  :
           + $LineId(0-3)
           + $NumPort = 4
Reg Desc   : Configure DRP for initializing SFP for STM1, STM4, STM16

------------------------------------------------------------------------------*/
#define cAf6Reg_drp_addr_1eh_Base                                                                        0x1E
#define cAf6Reg_drp_addr_1eh(LineId)                                                  (0x1E+0x100*(LineId)*4)
#define cAf6Reg_drp_addr_1eh_WidthVal                                                                      32
#define cAf6Reg_drp_addr_1eh_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: CLK_COR_SEQ_1_1
BitField Type: r/w
BitField Desc:
BitField Bits: [9:0]
--------------------------------------*/
#define cAf6_drp_clk_cor_seq_1_1_Bit_Start                                                           0
#define cAf6_drp_clk_cor_seq_1_1_Bit_End                                                             9
#define cAf6_drp_clk_cor_seq_1_1_Mask                                                          cBit9_0
#define cAf6_drp_clk_cor_seq_1_1_Shift                                                               0
#define cAf6_drp_clk_cor_seq_1_1_MaxVal                                                          0x3ff
#define cAf6_drp_clk_cor_seq_1_1_MinVal                                                            0x0
#define cAf6_drp_clk_cor_seq_1_1_RstVal                                                            0x0

/*------------------------------------------------------------------------------
Reg Name   : DRP ADDRESS 0x1F
Reg Addr   : 0x1F
Reg Formula: 0x1F + (0x100 * LineId * NumPort)
    Where  :
           + $LineId(0-3)
           + $NumPort = 4
Reg Desc   : Configure DRP for initializing SFP for STM1, STM4, STM16

------------------------------------------------------------------------------*/
#define cAf6Reg_drp_addr_1fh_Base                                                                        0x1F
#define cAf6Reg_drp_addr_1fh(LineId)                                                  (0x1F+0x100*(LineId)*4)
#define cAf6Reg_drp_addr_1fh_WidthVal                                                                      32
#define cAf6Reg_drp_addr_1fh_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: CLK_COR_SEQ_1_2
BitField Type: r/w
BitField Desc:
BitField Bits: [9:0]
--------------------------------------*/
#define cAf6_drp_clk_cor_seq_1_2_Bit_Start                                                           0
#define cAf6_drp_clk_cor_seq_1_2_Bit_End                                                             9
#define cAf6_drp_clk_cor_seq_1_2_Mask                                                          cBit9_0
#define cAf6_drp_clk_cor_seq_1_2_Shift                                                               0
#define cAf6_drp_clk_cor_seq_1_2_MaxVal                                                          0x3ff
#define cAf6_drp_clk_cor_seq_1_2_MinVal                                                            0x0
#define cAf6_drp_clk_cor_seq_1_2_RstVal                                                            0x0

/*------------------------------------------------------------------------------
Reg Name   : DRP ADDRESS 0x20
Reg Addr   : 0x20
Reg Formula: 0x20 + (0x100 * LineId * NumPort)
    Where  :
           + $LineId(0-3)
           + $NumPort = 4
Reg Desc   : Configure DRP for initializing SFP for STM1, STM4, STM16

------------------------------------------------------------------------------*/
#define cAf6Reg_drp_addr_20h_Base                                                                        0x20
#define cAf6Reg_drp_addr_20h(LineId)                                                  (0x20+0x100*(LineId)*4)
#define cAf6Reg_drp_addr_20h_WidthVal                                                                      32
#define cAf6Reg_drp_addr_20h_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: CLK_COR_SEQ_1_3
BitField Type: r/w
BitField Desc:
BitField Bits: [9:0]
--------------------------------------*/
#define cAf6_drp_clk_cor_seq_1_3_Bit_Start                                                           0
#define cAf6_drp_clk_cor_seq_1_3_Bit_End                                                             9
#define cAf6_drp_clk_cor_seq_1_3_Mask                                                          cBit9_0
#define cAf6_drp_clk_cor_seq_1_3_Shift                                                               0
#define cAf6_drp_clk_cor_seq_1_3_MaxVal                                                          0x3ff
#define cAf6_drp_clk_cor_seq_1_3_MinVal                                                            0x0
#define cAf6_drp_clk_cor_seq_1_3_RstVal                                                            0x0

/*------------------------------------------------------------------------------
Reg Name   : DRP ADDRESS 0x21
Reg Addr   : 0x21
Reg Formula: 0x21 + (0x100 * LineId * NumPort)
    Where  :
           + $LineId(0-3)
           + $NumPort = 4
Reg Desc   : Configure DRP for initializing SFP for STM1, STM4, STM16

------------------------------------------------------------------------------*/
#define cAf6Reg_drp_addr_21h_Base                                                                        0x21
#define cAf6Reg_drp_addr_21h(LineId)                                                  (0x21+0x100*(LineId)*4)
#define cAf6Reg_drp_addr_21h_WidthVal                                                                      32
#define cAf6Reg_drp_addr_21h_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: CLK_COR_SEQ_1_4
BitField Type: r/w
BitField Desc:
BitField Bits: [9:0]
--------------------------------------*/
#define cAf6_drp_clk_cor_seq_1_4_Bit_Start                                                           0
#define cAf6_drp_clk_cor_seq_1_4_Bit_End                                                             9
#define cAf6_drp_clk_cor_seq_1_4_Mask                                                          cBit9_0
#define cAf6_drp_clk_cor_seq_1_4_Shift                                                               0
#define cAf6_drp_clk_cor_seq_1_4_MaxVal                                                          0x3ff
#define cAf6_drp_clk_cor_seq_1_4_MinVal                                                            0x0
#define cAf6_drp_clk_cor_seq_1_4_RstVal                                                            0x0

/*------------------------------------------------------------------------------
Reg Name   : DRP ADDRESS 0x22
Reg Addr   : 0x22
Reg Formula: 0x22 + (0x100 * LineId * NumPort)
    Where  :
           + $LineId(0-3)
           + $NumPort = 4
Reg Desc   : Configure DRP for initializing SFP for STM1, STM4, STM16

------------------------------------------------------------------------------*/
#define cAf6Reg_drp_addr_22h_Base                                                                        0x22
#define cAf6Reg_drp_addr_22h(LineId)                                                  (0x22+0x100*(LineId)*4)
#define cAf6Reg_drp_addr_22h_WidthVal                                                                      32
#define cAf6Reg_drp_addr_22h_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: CLK_COR_SEQ_2_1
BitField Type: r/w
BitField Desc:
BitField Bits: [9:0]
--------------------------------------*/
#define cAf6_drp_clk_cor_seq_2_1_Bit_Start                                                           0
#define cAf6_drp_clk_cor_seq_2_1_Bit_End                                                             9
#define cAf6_drp_clk_cor_seq_2_1_Mask                                                          cBit9_0
#define cAf6_drp_clk_cor_seq_2_1_Shift                                                               0
#define cAf6_drp_clk_cor_seq_2_1_MaxVal                                                          0x3ff
#define cAf6_drp_clk_cor_seq_2_1_MinVal                                                            0x0
#define cAf6_drp_clk_cor_seq_2_1_RstVal                                                            0x0

/*------------------------------------------------------------------------------
Reg Name   : DRP ADDRESS 0x23
Reg Addr   : 0x23
Reg Formula: 0x23 + (0x100 * LineId * NumPort)
    Where  :
           + $LineId(0-3)
           + $NumPort = 4
Reg Desc   : Configure DRP for initializing SFP for STM1, STM4, STM16

------------------------------------------------------------------------------*/
#define cAf6Reg_drp_addr_23h_Base                                                                        0x23
#define cAf6Reg_drp_addr_23h(LineId)                                                  (0x23+0x100*(LineId)*4)
#define cAf6Reg_drp_addr_23h_WidthVal                                                                      32
#define cAf6Reg_drp_addr_23h_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: CLK_COR_SEQ_2_2
BitField Type: r/w
BitField Desc:
BitField Bits: [9:0]
--------------------------------------*/
#define cAf6_drp_clk_cor_seq_2_2_Bit_Start                                                           0
#define cAf6_drp_clk_cor_seq_2_2_Bit_End                                                             9
#define cAf6_drp_clk_cor_seq_2_2_Mask                                                          cBit9_0
#define cAf6_drp_clk_cor_seq_2_2_Shift                                                               0
#define cAf6_drp_clk_cor_seq_2_2_MaxVal                                                          0x3ff
#define cAf6_drp_clk_cor_seq_2_2_MinVal                                                            0x0
#define cAf6_drp_clk_cor_seq_2_2_RstVal                                                            0x0

/*------------------------------------------------------------------------------
Reg Name   : DRP ADDRESS 0x24
Reg Addr   : 0x24
Reg Formula: 0x24 + (0x100 * LineId * NumPort)
    Where  :
           + $LineId(0-3)
           + $NumPort = 4
Reg Desc   : Configure DRP for initializing SFP for STM1, STM4, STM16

------------------------------------------------------------------------------*/
#define cAf6Reg_drp_addr_24h_Base                                                                        0x24
#define cAf6Reg_drp_addr_24h(LineId)                                                  (0x24+0x100*(LineId)*4)
#define cAf6Reg_drp_addr_24h_WidthVal                                                                      32
#define cAf6Reg_drp_addr_24h_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: CLK_COR_SEQ_2_3
BitField Type: r/w
BitField Desc:
BitField Bits: [9:0]
--------------------------------------*/
#define cAf6_drp_clk_cor_seq_2_3_Bit_Start                                                           0
#define cAf6_drp_clk_cor_seq_2_3_Bit_End                                                             9
#define cAf6_drp_clk_cor_seq_2_3_Mask                                                          cBit9_0
#define cAf6_drp_clk_cor_seq_2_3_Shift                                                               0
#define cAf6_drp_clk_cor_seq_2_3_MaxVal                                                          0x3ff
#define cAf6_drp_clk_cor_seq_2_3_MinVal                                                            0x0
#define cAf6_drp_clk_cor_seq_2_3_RstVal                                                            0x0

/*------------------------------------------------------------------------------
Reg Name   : DRP ADDRESS 0x25
Reg Addr   : 0x25
Reg Formula: 0x25 + (0x100 * LineId * NumPort)
    Where  :
           + $LineId(0-3)
           + $NumPort = 4
Reg Desc   : Configure DRP for initializing SFP for STM1, STM4, STM16

------------------------------------------------------------------------------*/
#define cAf6Reg_drp_addr_25h_Base                                                                        0x25
#define cAf6Reg_drp_addr_25h(LineId)                                                  (0x25+0x100*(LineId)*4)
#define cAf6Reg_drp_addr_25h_WidthVal                                                                      32
#define cAf6Reg_drp_addr_25h_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: CLK_COR_SEQ_2_4
BitField Type: r/w
BitField Desc:
BitField Bits: [9:0]
--------------------------------------*/
#define cAf6_drp_clk_cor_seq_2_4_Bit_Start                                                           0
#define cAf6_drp_clk_cor_seq_2_4_Bit_End                                                             9
#define cAf6_drp_clk_cor_seq_2_4_Mask                                                          cBit9_0
#define cAf6_drp_clk_cor_seq_2_4_Shift                                                               0
#define cAf6_drp_clk_cor_seq_2_4_MaxVal                                                          0x3ff
#define cAf6_drp_clk_cor_seq_2_4_MinVal                                                            0x0
#define cAf6_drp_clk_cor_seq_2_4_RstVal                                                            0x0

/*------------------------------------------------------------------------------
Reg Name   : DRP ADDRESS 0x27
Reg Addr   : 0x27
Reg Formula: 0x27 + (0x100 * LineId * NumPort)
    Where  :
           + $LineId(0-3)
           + $NumPort = 4
Reg Desc   : Configure DRP for initializing SFP for STM1, STM4, STM16

------------------------------------------------------------------------------*/
#define cAf6Reg_drp_addr_27h_Base                                                                        0x27
#define cAf6Reg_drp_addr_27h(LineId)                                                  (0x27+0x100*(LineId)*4)
#define cAf6Reg_drp_addr_27h_WidthVal                                                                      32
#define cAf6Reg_drp_addr_27h_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: ALIGN_COMMA_WORD
BitField Type: r/w
BitField Desc:
BitField Bits: [15:13]
--------------------------------------*/
#define cAf6_drp_align_comma_word_Bit_Start                                                          13
#define cAf6_drp_align_comma_word_Bit_End                                                            15
#define cAf6_drp_align_comma_word_Mask                                                        cBit15_13
#define cAf6_drp_align_comma_word_Shift                                                              13
#define cAf6_drp_align_comma_word_MaxVal                                                            0x7
#define cAf6_drp_align_comma_word_MinVal                                                            0x0
#define cAf6_drp_align_comma_word_RstVal                                                            0x0

/*--------------------------------------
BitField Name: ALIGN_COMMA_ENABLE
BitField Type: r/w
BitField Desc:
BitField Bits: [9:0]
--------------------------------------*/
#define cAf6_drp_align_comma_enable_Bit_Start                                                           0
#define cAf6_drp_align_comma_enable_Bit_End                                                             9
#define cAf6_drp_align_comma_enable_Mask                                                          cBit9_0
#define cAf6_drp_align_comma_enable_Shift                                                               0
#define cAf6_drp_align_comma_enable_MaxVal                                                          0x3ff
#define cAf6_drp_align_comma_enable_MinVal                                                            0x0
#define cAf6_drp_align_comma_enable_RstVal                                                            0x0

/*------------------------------------------------------------------------------
Reg Name   : DRP ADDRESS 0x28
Reg Addr   : 0x28
Reg Formula: 0x28 + (0x100 * LineId * NumPort)
    Where  :
           + $LineId(0-3)
           + $NumPort = 4
Reg Desc   : Configure DRP for initializing SFP for STM1, STM4, STM16

------------------------------------------------------------------------------*/
#define cAf6Reg_drp_addr_28h_Base                                                                        0x28
#define cAf6Reg_drp_addr_28h(LineId)                                                  (0x28+0x100*(LineId)*4)
#define cAf6Reg_drp_addr_28h_WidthVal                                                                      32
#define cAf6Reg_drp_addr_28h_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: CPLL_FBDIV
BitField Type: r/w
BitField Desc:
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_drp_cpll_fbdiv_Bit_Start                                                           8
#define cAf6_drp_cpll_fbdiv_Bit_End                                                            15
#define cAf6_drp_cpll_fbdiv_Mask                                                         cBit15_8
#define cAf6_drp_cpll_fbdiv_Shift                                                               8
#define cAf6_drp_cpll_fbdiv_MaxVal                                                         0xffff
#define cAf6_drp_cpll_fbdiv_MinVal                                                            0x0
#define cAf6_drp_cpll_fbdiv_RstVal                                                            0x0

/*------------------------------------------------------------------------------
Reg Name   : DRP ADDRESS 0x2C
Reg Addr   : 0x2C
Reg Formula: 0x2C + (0x100 * LineId * NumPort)
    Where  :
           + $LineId(0-3)
           + $NumPort = 4
Reg Desc   : Configure DRP for initializing SFP for STM1, STM4, STM16

------------------------------------------------------------------------------*/
#define cAf6Reg_drp_addr_2ch_Base                                                                        0x2C
#define cAf6Reg_drp_addr_2ch(LineId)                                                  (0x2C+0x100*(LineId)*4)
#define cAf6Reg_drp_addr_2ch_WidthVal                                                                      32
#define cAf6Reg_drp_addr_2ch_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: DEC_PCOMMA_DETECT
BitField Type: r/w
BitField Desc:
BitField Bits: [15]
--------------------------------------*/
#define cAf6_drp_dec_pcomma_detect_Bit_Start                                                          15
#define cAf6_drp_dec_pcomma_detect_Bit_End                                                            15
#define cAf6_drp_dec_pcomma_detect_Mask                                                           cBit15
#define cAf6_drp_dec_pcomma_detect_Shift                                                              15
#define cAf6_drp_dec_pcomma_detect_MaxVal                                                            0x1
#define cAf6_drp_dec_pcomma_detect_MinVal                                                            0x0
#define cAf6_drp_dec_pcomma_detect_RstVal                                                            0x0

/*------------------------------------------------------------------------------
Reg Name   : DRP ADDRESS 0x36
Reg Addr   : 0x36
Reg Formula: 0x36 + (0x100 * LineId * NumPort)
    Where  :
           + $LineId(0-3)
           + $NumPort = 4
Reg Desc   : Configure DRP for initializing SFP for STM1, STM4, STM16

------------------------------------------------------------------------------*/
#define cAf6Reg_drp_addr_36h_Base                                                                        0x36
#define cAf6Reg_drp_addr_36h(LineId)                                                  (0x36+0x100*(LineId)*4)
#define cAf6Reg_drp_addr_36h_WidthVal                                                                      32
#define cAf6Reg_drp_addr_36h_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: RXDFELPM_KL_CFG1
BitField Type: r/w
BitField Desc:
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_drp_rxdfelpm_kl_cfg1_Bit_Start                                                           0
#define cAf6_drp_rxdfelpm_kl_cfg1_Bit_End                                                            15
#define cAf6_drp_rxdfelpm_kl_cfg1_Mask                                                         cBit15_0
#define cAf6_drp_rxdfelpm_kl_cfg1_Shift                                                               0
#define cAf6_drp_rxdfelpm_kl_cfg1_MaxVal                                                         0xffff
#define cAf6_drp_rxdfelpm_kl_cfg1_MinVal                                                            0x0
#define cAf6_drp_rxdfelpm_kl_cfg1_RstVal                                                            0x0

/*------------------------------------------------------------------------------
Reg Name   : DRP ADDRESS 0x52
Reg Addr   : 0x52
Reg Formula: 0x52 + (0x100 * LineId * NumPort)
    Where  :
           + $LineId(0-3)
           + $NumPort = 4
Reg Desc   : Configure DRP for initializing SFP for STM1, STM4, STM16

------------------------------------------------------------------------------*/
#define cAf6Reg_drp_addr_52h_Base                                                                        0x52
#define cAf6Reg_drp_addr_52h(LineId)                                                  (0x52+0x100*(LineId)*4)
#define cAf6Reg_drp_addr_52h_WidthVal                                                                      32
#define cAf6Reg_drp_addr_52h_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: RX_DFE_AGC_CFG1
BitField Type: r/w
BitField Desc:
BitField Bits: [4:2]
--------------------------------------*/
#define cAf6_drp_rx_dfe_agc_cfg1_Bit_Start                                                           2
#define cAf6_drp_rx_dfe_agc_cfg1_Bit_End                                                             4
#define cAf6_drp_rx_dfe_agc_cfg1_Mask                                                          cBit4_2
#define cAf6_drp_rx_dfe_agc_cfg1_Shift                                                               2
#define cAf6_drp_rx_dfe_agc_cfg1_MaxVal                                                            0x7
#define cAf6_drp_rx_dfe_agc_cfg1_MinVal                                                            0x0
#define cAf6_drp_rx_dfe_agc_cfg1_RstVal                                                            0x0

/*------------------------------------------------------------------------------
Reg Name   : DRP ADDRESS 0x55
Reg Addr   : 0x55
Reg Formula: 0x55 + (0x100 * LineId * NumPort)
    Where  :
           + $LineId(0-3)
           + $NumPort = 4
Reg Desc   : Configure DRP for initializing SFP for STM1, STM4, STM16

------------------------------------------------------------------------------*/
#define cAf6Reg_drp_addr_55h_Base                                                                        0x55
#define cAf6Reg_drp_addr_55h(LineId)                                                  (0x55+0x100*(LineId)*4)
#define cAf6Reg_drp_addr_55h_WidthVal                                                                      32
#define cAf6Reg_drp_addr_55h_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: ALIGN_MCOMMA_DET
BitField Type: r/w
BitField Desc:
BitField Bits: [10]
--------------------------------------*/
#define cAf6_drp_align_mcomma_det_Bit_Start                                                          10
#define cAf6_drp_align_mcomma_det_Bit_End                                                            10
#define cAf6_drp_align_mcomma_det_Mask                                                           cBit10
#define cAf6_drp_align_mcomma_det_Shift                                                              10
#define cAf6_drp_align_mcomma_det_MaxVal                                                            0x1
#define cAf6_drp_align_mcomma_det_MinVal                                                            0x0
#define cAf6_drp_align_mcomma_det_RstVal                                                            0x0

/*------------------------------------------------------------------------------
Reg Name   : DRP ADDRESS 0x56
Reg Addr   : 0x56
Reg Formula: 0x56 + (0x100 * LineId * NumPort)
    Where  :
           + $LineId(0-3)
           + $NumPort = 4
Reg Desc   : Configure DRP for initializing SFP for STM1, STM4, STM16

------------------------------------------------------------------------------*/
#define cAf6Reg_drp_addr_56h_Base                                                                        0x56
#define cAf6Reg_drp_addr_56h(LineId)                                                  (0x56+0x100*(LineId)*4)
#define cAf6Reg_drp_addr_56h_WidthVal                                                                      32
#define cAf6Reg_drp_addr_56h_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: ALIGN_PCOMMA_DET
BitField Type: r/w
BitField Desc:
BitField Bits: [10]
--------------------------------------*/
#define cAf6_drp_align_pcomma_det_Bit_Start                                                          10
#define cAf6_drp_align_pcomma_det_Bit_End                                                            10
#define cAf6_drp_align_pcomma_det_Mask                                                           cBit10
#define cAf6_drp_align_pcomma_det_Shift                                                              10
#define cAf6_drp_align_pcomma_det_MaxVal                                                            0x1
#define cAf6_drp_align_pcomma_det_MinVal                                                            0x0
#define cAf6_drp_align_pcomma_det_RstVal                                                            0x0

/*------------------------------------------------------------------------------
Reg Name   : DRP ADDRESS 0x63
Reg Addr   : 0x63
Reg Formula: 0x63 + (0x100 * LineId * NumPort)
    Where  :
           + $LineId(0-3)
           + $NumPort = 4
Reg Desc   : Configure DRP for initializing SFP for STM1, STM4, STM16

------------------------------------------------------------------------------*/
#define cAf6Reg_drp_addr_63h_Base                                                                        0x63
#define cAf6Reg_drp_addr_63h(LineId)                                                  (0x63+0x100*(LineId)*4)
#define cAf6Reg_drp_addr_63h_WidthVal                                                                      32
#define cAf6Reg_drp_addr_63h_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: CBCC_DATA_SOURCE_SEL
BitField Type: r/w
BitField Desc:
BitField Bits: [15]
--------------------------------------*/
#define cAf6_drp_cbcc_data_source_sel_Bit_Start                                                          15
#define cAf6_drp_cbcc_data_source_sel_Bit_End                                                            15
#define cAf6_drp_cbcc_data_source_sel_Mask                                                           cBit15
#define cAf6_drp_cbcc_data_source_sel_Shift                                                              15
#define cAf6_drp_cbcc_data_source_sel_MaxVal                                                            0x1
#define cAf6_drp_cbcc_data_source_sel_MinVal                                                            0x0
#define cAf6_drp_cbcc_data_source_sel_RstVal                                                            0x0

/*--------------------------------------
BitField Name: RXOUT_DIV
BitField Type: r/w
BitField Desc:
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_drp_rxout_div_Bit_Start                                                           0
#define cAf6_drp_rxout_div_Bit_End                                                             2
#define cAf6_drp_rxout_div_Mask                                                          cBit2_0
#define cAf6_drp_rxout_div_Shift                                                               0
#define cAf6_drp_rxout_div_MaxVal                                                            0x7
#define cAf6_drp_rxout_div_MinVal                                                            0x0
#define cAf6_drp_rxout_div_RstVal                                                            0x0

/*------------------------------------------------------------------------------
Reg Name   : DRP ADDRESS 0x6D
Reg Addr   : 0x6D
Reg Formula: 0x6D + (0x100 * LineId * NumPort)
    Where  :
           + $LineId(0-3)
           + $NumPort = 4
Reg Desc   : Configure DRP for initializing SFP for STM1, STM4, STM16

------------------------------------------------------------------------------*/
#define cAf6Reg_drp_addr_6dh_Base                                                                        0x6D
#define cAf6Reg_drp_addr_6dh(LineId)                                                  (0x6D+0x100*(LineId)*4)
#define cAf6Reg_drp_addr_6dh_WidthVal                                                                      32
#define cAf6Reg_drp_addr_6dh_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: RX_CLK25_DIV
BitField Type: r/w
BitField Desc:
BitField Bits: [7:3]
--------------------------------------*/
#define cAf6_drp_rx_clk25_div_Bit_Start                                                           3
#define cAf6_drp_rx_clk25_div_Bit_End                                                             7
#define cAf6_drp_rx_clk25_div_Mask                                                          cBit7_3
#define cAf6_drp_rx_clk25_div_Shift                                                               3
#define cAf6_drp_rx_clk25_div_MaxVal                                                           0x1f
#define cAf6_drp_rx_clk25_div_MinVal                                                            0x0
#define cAf6_drp_rx_clk25_div_RstVal                                                            0x0

/*------------------------------------------------------------------------------
Reg Name   : DRP ADDRESS 0x7A
Reg Addr   : 0x7A
Reg Formula: 0x7A + (0x100 * LineId * NumPort)
    Where  :
           + $LineId(0-3)
           + $NumPort = 4
Reg Desc   : Configure DRP for initializing SFP for STM1, STM4, STM16

------------------------------------------------------------------------------*/
#define cAf6Reg_drp_addr_7ah_Base                                                                        0x7A
#define cAf6Reg_drp_addr_7ah(LineId)                                                  (0x7A+0x100*(LineId)*4)
#define cAf6Reg_drp_addr_7ah_WidthVal                                                                      32
#define cAf6Reg_drp_addr_7ah_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: TX_CLK25_DIV
BitField Type: r/w
BitField Desc:
BitField Bits: [15:11]
--------------------------------------*/
#define cAf6_drp_tx_clk25_div_Bit_Start                                                          11
#define cAf6_drp_tx_clk25_div_Bit_End                                                            15
#define cAf6_drp_tx_clk25_div_Mask                                                        cBit15_11
#define cAf6_drp_tx_clk25_div_Shift                                                              11
#define cAf6_drp_tx_clk25_div_MaxVal                                                           0x1f
#define cAf6_drp_tx_clk25_div_MinVal                                                            0x0
#define cAf6_drp_tx_clk25_div_RstVal                                                            0x0

/*--------------------------------------
BitField Name: TX_DATA_WIDTH
BitField Type: r/w
BitField Desc:
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_drp_tx_data_width_Bit_Start                                                           0
#define cAf6_drp_tx_data_width_Bit_End                                                             3
#define cAf6_drp_tx_data_width_Mask                                                          cBit3_0
#define cAf6_drp_tx_data_width_Shift                                                               0
#define cAf6_drp_tx_data_width_MaxVal                                                            0xf
#define cAf6_drp_tx_data_width_MinVal                                                            0x0
#define cAf6_drp_tx_data_width_RstVal                                                            0x0

/*------------------------------------------------------------------------------
Reg Name   : DRP ADDRESS 0x7C
Reg Addr   : 0x7C
Reg Formula: 0x7C + (0x100 * LineId * NumPort)
    Where  :
           + $LineId(0-3)
           + $NumPort = 4
Reg Desc   : Configure DRP for initializing SFP for STM1, STM4, STM16

------------------------------------------------------------------------------*/
#define cAf6Reg_drp_addr_7ch_Base                                                                        0x7C
#define cAf6Reg_drp_addr_7ch(LineId)                                                  (0x7C+0x100*(LineId)*4)
#define cAf6Reg_drp_addr_7ch_WidthVal                                                                      32
#define cAf6Reg_drp_addr_7ch_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: TXOUT_DIV
BitField Type: r/w
BitField Desc:
BitField Bits: [10:8]
--------------------------------------*/
#define cAf6_drp_txout_div_Bit_Start                                                           8
#define cAf6_drp_txout_div_Bit_End                                                            10
#define cAf6_drp_txout_div_Mask                                                         cBit10_8
#define cAf6_drp_txout_div_Shift                                                               8
#define cAf6_drp_txout_div_MaxVal                                                            0x7
#define cAf6_drp_txout_div_MinVal                                                            0x0
#define cAf6_drp_txout_div_RstVal                                                            0x0

/*------------------------------------------------------------------------------
Reg Name   : DRP ADDRESS 0x8C
Reg Addr   : 0x8C
Reg Formula: 0x8C + (0x100 * LineId * NumPort)
    Where  :
           + $LineId(0-3)
           + $NumPort = 4
Reg Desc   : Configure DRP for initializing SFP for STM1, STM4, STM16

------------------------------------------------------------------------------*/
#define cAf6Reg_drp_8ch_Base                                                                        0x8C
#define cAf6Reg_drp_8ch(LineId)                                                  (0x8C+0x100*(LineId)*4)
#define cAf6Reg_drp_8ch_WidthVal                                                                      32
#define cAf6Reg_drp_8ch_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: RX_DFE_KL_LPM_KL_CFG1
BitField Type: r/w
BitField Desc:
BitField Bits: [7:5]
--------------------------------------*/
#define cAf6_drp_rx_dfe_kl_lpm_kl_cfg1_Bit_Start                                                           5
#define cAf6_drp_rx_dfe_kl_lpm_kl_cfg1_Bit_End                                                             7
#define cAf6_drp_rx_dfe_kl_lpm_kl_cfg1_Mask                                                          cBit7_5
#define cAf6_drp_rx_dfe_kl_lpm_kl_cfg1_Shift                                                               5
#define cAf6_drp_rx_dfe_kl_lpm_kl_cfg1_MaxVal                                                            0x7
#define cAf6_drp_rx_dfe_kl_lpm_kl_cfg1_MinVal                                                            0x0
#define cAf6_drp_rx_dfe_kl_lpm_kl_cfg1_RstVal                                                            0x0

/*------------------------------------------------------------------------------
Reg Name   : DRP ADDRESS 0x9B
Reg Addr   : 0x9B
Reg Formula: 0x9B + (0x100 * LineId * NumPort)
    Where  :
           + $LineId(0-3)
           + $NumPort = 4
Reg Desc   : Configure DRP for initializing SFP for STM1, STM4, STM16

------------------------------------------------------------------------------*/
#define cAf6Reg_drp_9bh_Base                                                                        0x9B
#define cAf6Reg_drp_9bh(LineId)                                                  (0x9B+0x100*(LineId)*4)
#define cAf6Reg_drp_9bh_WidthVal                                                                      32
#define cAf6Reg_drp_9bh_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: RX_DFE_KL_LPM_KH_CFG1
BitField Type: r/w
BitField Desc:
BitField Bits: [7:5]
--------------------------------------*/
#define cAf6_drp_rx_dfe_kl_lpm_kh_cfg1_Bit_Start                                                           5
#define cAf6_drp_rx_dfe_kl_lpm_kh_cfg1_Bit_End                                                             7
#define cAf6_drp_rx_dfe_kl_lpm_kh_cfg1_Mask                                                          cBit7_5
#define cAf6_drp_rx_dfe_kl_lpm_kh_cfg1_Shift                                                               5
#define cAf6_drp_rx_dfe_kl_lpm_kh_cfg1_MaxVal                                                            0x7
#define cAf6_drp_rx_dfe_kl_lpm_kh_cfg1_MinVal                                                            0x0
#define cAf6_drp_rx_dfe_kl_lpm_kh_cfg1_RstVal                                                            0x0

/*------------------------------------------------------------------------------
Reg Name   : DRP ADDRESS 0x9C
Reg Addr   : 0x9C
Reg Formula: 0x9C + (0x100 * LineId * NumPort)
    Where  :
           + $LineId(0-3)
           + $NumPort = 4
Reg Desc   : Configure DRP for initializing SFP for STM1, STM4, STM16

------------------------------------------------------------------------------*/
#define cAf6Reg_drp_9ch_Base                                                                        0x9C
#define cAf6Reg_drp_9ch(LineId)                                                  (0x9C+0x100*(LineId)*4)
#define cAf6Reg_drp_9ch_WidthVal                                                                      32
#define cAf6Reg_drp_9ch_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: TXPI_CFG0
BitField Type: r/w
BitField Desc:
BitField Bits: [12:11]
--------------------------------------*/
#define cAf6_drp_txpi_cfg0_Bit_Start                                                          11
#define cAf6_drp_txpi_cfg0_Bit_End                                                            12
#define cAf6_drp_txpi_cfg0_Mask                                                        cBit12_11
#define cAf6_drp_txpi_cfg0_Shift                                                              11
#define cAf6_drp_txpi_cfg0_MaxVal                                                            0x3
#define cAf6_drp_txpi_cfg0_MinVal                                                            0x0
#define cAf6_drp_txpi_cfg0_RstVal                                                            0x0

/*--------------------------------------
BitField Name: TXPI_CFG1
BitField Type: r/w
BitField Desc:
BitField Bits: [10:9]
--------------------------------------*/
#define cAf6_drp_txpi_cfg1_Bit_Start                                                           9
#define cAf6_drp_txpi_cfg1_Bit_End                                                            10
#define cAf6_drp_txpi_cfg1_Mask                                                         cBit10_9
#define cAf6_drp_txpi_cfg1_Shift                                                               9
#define cAf6_drp_txpi_cfg1_MaxVal                                                            0x3
#define cAf6_drp_txpi_cfg1_MinVal                                                            0x0
#define cAf6_drp_txpi_cfg1_RstVal                                                            0x0

/*--------------------------------------
BitField Name: TXPI_CFG2
BitField Type: r/w
BitField Desc:
BitField Bits: [8:7]
--------------------------------------*/
#define cAf6_drp_txpi_cfg2_Bit_Start                                                           7
#define cAf6_drp_txpi_cfg2_Bit_End                                                             8
#define cAf6_drp_txpi_cfg2_Mask                                                          cBit8_7
#define cAf6_drp_txpi_cfg2_Shift                                                               7
#define cAf6_drp_txpi_cfg2_MaxVal                                                            0x3
#define cAf6_drp_txpi_cfg2_MinVal                                                            0x0
#define cAf6_drp_txpi_cfg2_RstVal                                                            0x0

/*--------------------------------------
BitField Name: TXPI_CFG5
BitField Type: r/w
BitField Desc:
BitField Bits: [4:2]
--------------------------------------*/
#define cAf6_drp_txpi_cfg5_Bit_Start                                                           2
#define cAf6_drp_txpi_cfg5_Bit_End                                                             4
#define cAf6_drp_txpi_cfg5_Mask                                                          cBit4_2
#define cAf6_drp_txpi_cfg5_Shift                                                               2
#define cAf6_drp_txpi_cfg5_MaxVal                                                            0x7
#define cAf6_drp_txpi_cfg5_MinVal                                                            0x0
#define cAf6_drp_txpi_cfg5_RstVal                                                            0x0

/*------------------------------------------------------------------------------
Reg Name   : DRP ADDRESS 0x9D
Reg Addr   : 0x9D
Reg Formula: 0x9D + (0x100 * LineId * NumPort)
    Where  :
           + $LineId(0-3)
           + $NumPort = 4
Reg Desc   : Configure DRP for initializing SFP for STM1, STM4, STM16

------------------------------------------------------------------------------*/
#define cAf6Reg_drp_9dh_Base                                                                        0x9D
#define cAf6Reg_drp_9dh(LineId)                                                  (0x9D+0x100*(LineId)*4)
#define cAf6Reg_drp_9dh_WidthVal                                                                      32
#define cAf6Reg_drp_9dh_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: RXPI_CFG
BitField Type: r/w
BitField Desc:
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_drp_rxpi_cfg_Bit_Start                                                           0
#define cAf6_drp_rxpi_cfg_Bit_End                                                            15
#define cAf6_drp_rxpi_cfg_Mask                                                         cBit15_0
#define cAf6_drp_rxpi_cfg_Shift                                                               0
#define cAf6_drp_rxpi_cfg_MaxVal                                                         0xffff
#define cAf6_drp_rxpi_cfg_MinVal                                                            0x0
#define cAf6_drp_rxpi_cfg_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : DRP ADDR 0xA4
Reg Addr   : 0xA4
Reg Formula: 0xA4 + (0x100 * LineId * NumPort)
    Where  :
           + $LineId(0-3)
           + $NumPort = 4
Reg Desc   : Configure DRP for initializing SFP for STM1, STM4, STM16

------------------------------------------------------------------------------*/
#define cAf6Reg_drp_addr_a4h_Base                                                                        0xA4
#define cAf6Reg_drp_addr_a4h(LineId)                                                  (0xA4+0x100*(LineId)*4)
#define cAf6Reg_drp_addr_a4h_WidthVal                                                                      32
#define cAf6Reg_drp_addr_a4h_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: RXCDR_CFG2_GEN3
BitField Type: r/w
BitField Desc:
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_drp_rxcdr_cfg2_gen3_Bit_Start                                                           0
#define cAf6_drp_rxcdr_cfg2_gen3_Bit_End                                                            15
#define cAf6_drp_rxcdr_cfg2_gen3_Mask                                                         cBit15_0
#define cAf6_drp_rxcdr_cfg2_gen3_Shift                                                               0
#define cAf6_drp_rxcdr_cfg2_gen3_MaxVal                                                         0xffff
#define cAf6_drp_rxcdr_cfg2_gen3_MinVal                                                            0x0
#define cAf6_drp_rxcdr_cfg2_gen3_RstVal                                                            0x0

/*------------------------------------------------------------------------------
Reg Name   : DRP ADDR 0xCD
Reg Addr   : 0xCD
Reg Formula: 0xCD + (0x100 * LineId * NumPort)
    Where  :
           + $LineId(0-3)
           + $NumPort = 4
Reg Desc   : Configure DRP for initializing SFP for STM1, STM4, STM16

------------------------------------------------------------------------------*/
#define cAf6Reg_drp_addr_cdh_Base                                                                        0xCD
#define cAf6Reg_drp_addr_cdh(LineId)                                                  (0xCD+0x100*(LineId)*4)
#define cAf6Reg_drp_addr_cdh_WidthVal                                                                      32
#define cAf6Reg_drp_addr_cdh_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: DEC_MCOMMA_DETECT
BitField Type: r/w
BitField Desc:
BitField Bits: [0]
--------------------------------------*/
#define cAf6_drp_dec_mcomma_detect_Bit_Start                                                           0
#define cAf6_drp_dec_mcomma_detect_Bit_End                                                             0
#define cAf6_drp_dec_mcomma_detect_Mask                                                            cBit0
#define cAf6_drp_dec_mcomma_detect_Shift                                                               0
#define cAf6_drp_dec_mcomma_detect_MaxVal                                                            0x1
#define cAf6_drp_dec_mcomma_detect_MinVal                                                            0x0
#define cAf6_drp_dec_mcomma_detect_RstVal                                                            0x0

/*------------------------------------------------------------------------------
Reg Name   : DRP ADDR 0xE0
Reg Addr   : 0xE0
Reg Formula: 0xE0 + (0x100 * LineId * NumPort)
    Where  :
           + $LineId(0-3)
           + $NumPort = 4
Reg Desc   : Configure DRP for initializing SFP for STM1, STM4, STM16

------------------------------------------------------------------------------*/
#define cAf6Reg_drp_addr_e0h_Base                                                                        0xE0
#define cAf6Reg_drp_addr_e0h(LineId)                                                  (0xE0+0x100*(LineId)*4)
#define cAf6Reg_drp_addr_e0h_WidthVal                                                                      32
#define cAf6Reg_drp_addr_e0h_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: RXPI_STARTCODE
BitField Type: r/w
BitField Desc:
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_drp_rxpi_startcode_Bit_End                                                             1
#define cAf6_drp_rxpi_startcode_Mask                                                          cBit1_0
#define cAf6_drp_rxpi_startcode_Shift                                                               0
#define cAf6_drp_rxpi_startcode_MaxVal                                                            0x3
#define cAf6_drp_rxpi_startcode_MinVal                                                            0x0
#define cAf6_drp_rxpi_startcode_RstVal                                                            0x0

/*------------------------------------------------------------------------------
Reg Name   : DRP ADDR 0xFF
Reg Addr   : 0xFF
Reg Formula: 0xFF + (0x100 * LineId * NumPort)
    Where  :
           + $LineId(0-3)
           + $NumPort = 4
Reg Desc   : Configure DRP for initializing SFP for STM1, STM4, STM16

------------------------------------------------------------------------------*/
#define cAf6Reg_drp_addr_ffh_Base                                                                       0xFF
#define cAf6Reg_drp_addr_ffh(LineId)                                                  (0xFF+0x100*(LineId)*4)
#define cAf6Reg_drp_addr_ffh_WidthVal                                                                      32
#define cAf6Reg_drp_addr_ffh_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: TX_PI_CFG0
BitField Type: r/w
BitField Desc:
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_drp_tx_pi_cfg0_Bit_Start                                                           0
#define cAf6_drp_tx_pi_cfg0_Bit_End                                                            15
#define cAf6_drp_tx_pi_cfg0_Mask                                                         cBit15_0
#define cAf6_drp_tx_pi_cfg0_Shift                                                               0
#define cAf6_drp_tx_pi_cfg0_MaxVal                                                         0xffff
#define cAf6_drp_tx_pi_cfg0_MinVal                                                            0x0
#define cAf6_drp_tx_pi_cfg0_RstVal                                                            0x0

#define cAf6Reg_drp_RX_XCLK_SELMask                                                      cBit2_1
#define cAf6Reg_drp_RX_XCLK_SELShift                                                     1


/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60210061DRPREG_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha60210061RxauiSerdesTurningReg.h
 * 
 * Created Date: Oct 3, 2016
 *
 * Description : RXAUI Serdes Turning registers
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061RXAUISERDESTUNINGREG_H_
#define _THA60210061RXAUISERDESTUNINGREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cTha60210061RXaui0SerdesTuningBaseAddress          0xF2A000
#define cTha60210061RXaui1SerdesTuningBaseAddress          0xF2C000

/*------------------------------------------------------------------------------
Reg Name   : RXAUI DRP
Reg Addr   : 0x1000
Reg Formula: 0x1000+$DRP
    Where  :
           + $DRP
Reg Desc   :
Read/Write DRP address of SERDES, DRP address, see UG578

------------------------------------------------------------------------------*/
#define cAf6Reg_RXAUI_DRP_Base                                                                          0x1000
#define cAf6Reg_RXAUI_DRP(DRP)\
                                                                                              (0x1000UL+(DRP))
#define cAf6Reg_RXAUI_DRP_WidthVal                                                                          32
#define cAf6Reg_RXAUI_DRP_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: drp_rw
BitField Type: R/W
BitField Desc: DRP read/write value
BitField Bits: [08:00]
--------------------------------------*/
#define cAf6_RXAUI_DRP_drp_rw_Bit_Start                                                                      0
#define cAf6_RXAUI_DRP_drp_rw_Bit_End                                                                        8
#define cAf6_RXAUI_DRP_drp_rw_Mask                                                                     cBit8_0
#define cAf6_RXAUI_DRP_drp_rw_Shift                                                                          0
#define cAf6_RXAUI_DRP_drp_rw_MaxVal                                                                     0x1ff
#define cAf6_RXAUI_DRP_drp_rw_MinVal                                                                       0x0
#define cAf6_RXAUI_DRP_drp_rw_RstVal                                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : SERDES LoopBack
Reg Addr   : 0x002-0x302 ( Not Support )
Reg Formula: 0x002+$G*0x100
    Where  :
           + $G(0-3) : Group of Ports
Reg Desc   :
Configurate LoopBack, there is 4 group (0-3), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7,Group 2 => Port8-11, Group 1 => Port 12-15,

------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_LoopBack_Base                                                                     0x002
#define cAf6Reg_SERDES_LoopBack(G)\
                                                                                         (0x002UL+(G)*0x100UL)
#define cAf6Reg_SERDES_LoopBack_WidthVal                                                                    32
#define cAf6Reg_SERDES_LoopBack_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: lpback_subport3
BitField Type: R/W
BitField Desc: Unused
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_SERDES_LoopBack_lpback_subport3_Bit_Start                                                      12
#define cAf6_SERDES_LoopBack_lpback_subport3_Bit_End                                                        15
#define cAf6_SERDES_LoopBack_lpback_subport3_Mask                                                    cBit15_12
#define cAf6_SERDES_LoopBack_lpback_subport3_Shift                                                          12
#define cAf6_SERDES_LoopBack_lpback_subport3_MaxVal                                                        0xf
#define cAf6_SERDES_LoopBack_lpback_subport3_MinVal                                                        0x0
#define cAf6_SERDES_LoopBack_lpback_subport3_RstVal                                                        0x0

/*--------------------------------------
BitField Name: lpback_subport2
BitField Type: R/W
BitField Desc: Unused
BitField Bits: [11:08]
--------------------------------------*/
#define cAf6_SERDES_LoopBack_lpback_subport2_Bit_Start                                                       8
#define cAf6_SERDES_LoopBack_lpback_subport2_Bit_End                                                        11
#define cAf6_SERDES_LoopBack_lpback_subport2_Mask                                                     cBit11_8
#define cAf6_SERDES_LoopBack_lpback_subport2_Shift                                                           8
#define cAf6_SERDES_LoopBack_lpback_subport2_MaxVal                                                        0xf
#define cAf6_SERDES_LoopBack_lpback_subport2_MinVal                                                        0x0
#define cAf6_SERDES_LoopBack_lpback_subport2_RstVal                                                        0x0

/*--------------------------------------
BitField Name: lpback_subport1
BitField Type: R/W
BitField Desc: Loopback subport 1
BitField Bits: [07:04]
--------------------------------------*/
#define cAf6_SERDES_LoopBack_lpback_subport1_Bit_Start                                                       4
#define cAf6_SERDES_LoopBack_lpback_subport1_Bit_End                                                         7
#define cAf6_SERDES_LoopBack_lpback_subport1_Mask                                                      cBit7_4
#define cAf6_SERDES_LoopBack_lpback_subport1_Shift                                                           4
#define cAf6_SERDES_LoopBack_lpback_subport1_MaxVal                                                        0xf
#define cAf6_SERDES_LoopBack_lpback_subport1_MinVal                                                        0x0
#define cAf6_SERDES_LoopBack_lpback_subport1_RstVal                                                        0x0

/*--------------------------------------
BitField Name: lpback_subport0
BitField Type: R/W
BitField Desc: Loopback subport 0
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_SERDES_LoopBack_lpback_subport0_Bit_Start                                                       0
#define cAf6_SERDES_LoopBack_lpback_subport0_Bit_End                                                         3
#define cAf6_SERDES_LoopBack_lpback_subport0_Mask                                                      cBit3_0
#define cAf6_SERDES_LoopBack_lpback_subport0_Shift                                                           0
#define cAf6_SERDES_LoopBack_lpback_subport0_MaxVal                                                        0xf
#define cAf6_SERDES_LoopBack_lpback_subport0_MinVal                                                        0x0
#define cAf6_SERDES_LoopBack_lpback_subport0_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : SERDES POWER DOWN
Reg Addr   : 0x003-0x303
Reg Formula: 0x003+$G*0x100
    Where  :
           + $G(0-3) : Group of Ports
Reg Desc   :
Configurate Power Down , there is 4 group (0-3), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7,Group 2 => Port8-11, Group 1 => Port 12-15,

------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_POWER_DOWN_Base                                                                   0x003
#define cAf6Reg_SERDES_POWER_DOWN(G)\
                                                                                         (0x003UL+(G)*0x100UL)
#define cAf6Reg_SERDES_POWER_DOWN_WidthVal                                                                  32
#define cAf6Reg_SERDES_POWER_DOWN_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: TXPD1
BitField Type: R/W
BitField Desc: Power Down subport 1
BitField Bits: [07:06]
--------------------------------------*/
#define cAf6_SERDES_POWER_DOWN_TXPD1_Bit_Start                                                               6
#define cAf6_SERDES_POWER_DOWN_TXPD1_Bit_End                                                                 7
#define cAf6_SERDES_POWER_DOWN_TXPD1_Mask                                                              cBit7_6
#define cAf6_SERDES_POWER_DOWN_TXPD1_Shift                                                                   6
#define cAf6_SERDES_POWER_DOWN_TXPD1_MaxVal                                                                0x3
#define cAf6_SERDES_POWER_DOWN_TXPD1_MinVal                                                                0x0
#define cAf6_SERDES_POWER_DOWN_TXPD1_RstVal                                                                0x0

/*--------------------------------------
BitField Name: TXPD0
BitField Type: R/W
BitField Desc: Power Down subport 0
BitField Bits: [05:04]
--------------------------------------*/
#define cAf6_SERDES_POWER_DOWN_TXPD0_Bit_Start                                                               4
#define cAf6_SERDES_POWER_DOWN_TXPD0_Bit_End                                                                 5
#define cAf6_SERDES_POWER_DOWN_TXPD0_Mask                                                              cBit5_4
#define cAf6_SERDES_POWER_DOWN_TXPD0_Shift                                                                   4
#define cAf6_SERDES_POWER_DOWN_TXPD0_MaxVal                                                                0x3
#define cAf6_SERDES_POWER_DOWN_TXPD0_MinVal                                                                0x0
#define cAf6_SERDES_POWER_DOWN_TXPD0_RstVal                                                                0x0

/*--------------------------------------
BitField Name: RXPD1
BitField Type: R/W
BitField Desc: Power Down subport 1
BitField Bits: [03:02]
--------------------------------------*/
#define cAf6_SERDES_POWER_DOWN_RXPD1_Bit_Start                                                               2
#define cAf6_SERDES_POWER_DOWN_RXPD1_Bit_End                                                                 3
#define cAf6_SERDES_POWER_DOWN_RXPD1_Mask                                                              cBit3_2
#define cAf6_SERDES_POWER_DOWN_RXPD1_Shift                                                                   2
#define cAf6_SERDES_POWER_DOWN_RXPD1_MaxVal                                                                0x3
#define cAf6_SERDES_POWER_DOWN_RXPD1_MinVal                                                                0x0
#define cAf6_SERDES_POWER_DOWN_RXPD1_RstVal                                                                0x0

/*--------------------------------------
BitField Name: RXPD0
BitField Type: R/W
BitField Desc: Power Down subport 0
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_SERDES_POWER_DOWN_RXPD0_Bit_Start                                                               0
#define cAf6_SERDES_POWER_DOWN_RXPD0_Bit_End                                                                 1
#define cAf6_SERDES_POWER_DOWN_RXPD0_Mask                                                              cBit1_0
#define cAf6_SERDES_POWER_DOWN_RXPD0_Shift                                                                   0
#define cAf6_SERDES_POWER_DOWN_RXPD0_MaxVal                                                                0x3
#define cAf6_SERDES_POWER_DOWN_RXPD0_MinVal                                                                0x0
#define cAf6_SERDES_POWER_DOWN_RXPD0_RstVal                                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : SERDES PLL Status
Reg Addr   : 0x00B-0x30B (Unused)
Reg Formula: 0x00B+$G*0x100
    Where  :
           + $G(0-3) : Group of Ports
Reg Desc   :
QPLL/CPLL status, there is 4 group (0-3), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7,Group 2 => Port8-11, Group 1 => Port 12-15,

------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_PLL_Status_Base                                                                   0x00B
#define cAf6Reg_SERDES_PLL_Status(G)\
                                                                                         (0x00BUL+(G)*0x100UL)
#define cAf6Reg_SERDES_PLL_Status_WidthVal                                                                  32
#define cAf6Reg_SERDES_PLL_Status_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: QPLL1_Lock
BitField Type: R_O
BitField Desc: QPLL0 is Locked, Group 0-1
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_SERDES_PLL_Status_QPLL1_Lock_Bit_Start                                                          3
#define cAf6_SERDES_PLL_Status_QPLL1_Lock_Bit_End                                                            3
#define cAf6_SERDES_PLL_Status_QPLL1_Lock_Mask                                                           cBit3
#define cAf6_SERDES_PLL_Status_QPLL1_Lock_Shift                                                              3
#define cAf6_SERDES_PLL_Status_QPLL1_Lock_MaxVal                                                           0x1
#define cAf6_SERDES_PLL_Status_QPLL1_Lock_MinVal                                                           0x0
#define cAf6_SERDES_PLL_Status_QPLL1_Lock_RstVal                                                           0x0

/*--------------------------------------
BitField Name: QPLL0_Lock
BitField Type: R_O
BitField Desc: QPLL0 is Locked, Group 0-1
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_SERDES_PLL_Status_QPLL0_Lock_Bit_Start                                                          2
#define cAf6_SERDES_PLL_Status_QPLL0_Lock_Bit_End                                                            2
#define cAf6_SERDES_PLL_Status_QPLL0_Lock_Mask                                                           cBit2
#define cAf6_SERDES_PLL_Status_QPLL0_Lock_Shift                                                              2
#define cAf6_SERDES_PLL_Status_QPLL0_Lock_MaxVal                                                           0x1
#define cAf6_SERDES_PLL_Status_QPLL0_Lock_MinVal                                                           0x0
#define cAf6_SERDES_PLL_Status_QPLL0_Lock_RstVal                                                           0x0

/*--------------------------------------
BitField Name: CPLL_Lock
BitField Type: R_O
BitField Desc: CPLL is Locked, bit per sub port,
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_SERDES_PLL_Status_CPLL_Lock_Bit_Start                                                           0
#define cAf6_SERDES_PLL_Status_CPLL_Lock_Bit_End                                                             1
#define cAf6_SERDES_PLL_Status_CPLL_Lock_Mask                                                          cBit1_0
#define cAf6_SERDES_PLL_Status_CPLL_Lock_Shift                                                               0
#define cAf6_SERDES_PLL_Status_CPLL_Lock_MaxVal                                                            0x3
#define cAf6_SERDES_PLL_Status_CPLL_Lock_MinVal                                                            0x0
#define cAf6_SERDES_PLL_Status_CPLL_Lock_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : SERDES TX TXELECIDLE
Reg Addr   : 0x004
Reg Formula: 0x004
    Where  :
           + $G(0-3) : Group of Ports
Reg Desc   :
TX TXELECIDLE, there is 4 group (0-3), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7,Group 2 => Port8-11, Group 1 => Port 12-15,

------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_TX_TXELECIDLE_Base                                                                0x004
#define cAf6Reg_SERDES_TX_TXELECIDLE(G)\
                                                                                                     (0x004UL)
#define cAf6Reg_SERDES_TX_TXELECIDLE_WidthVal                                                               32
#define cAf6Reg_SERDES_TX_TXELECIDLE_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: txelecidle
BitField Type: R/W
BitField Desc: Trige 0->1 to enable Tx elecidle, bit per sub port
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_SERDES_TX_TXELECIDLE_txelecidle_Bit_Start                                                       0
#define cAf6_SERDES_TX_TXELECIDLE_txelecidle_Bit_End                                                         1
#define cAf6_SERDES_TX_TXELECIDLE_txelecidle_Mask                                                      cBit1_0
#define cAf6_SERDES_TX_TXELECIDLE_txelecidle_Shift                                                           0
#define cAf6_SERDES_TX_TXELECIDLE_txelecidle_MaxVal                                                        0x3
#define cAf6_SERDES_TX_TXELECIDLE_txelecidle_MinVal                                                        0x0
#define cAf6_SERDES_TX_TXELECIDLE_txelecidle_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : SERDES TX Reset
Reg Addr   : 0x00C
Reg Formula: 0x00C
    Where  :
           + $G(0-3) : Group of Ports
Reg Desc   :
Reset TX SERDES, there is 4 group (0-3), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7,Group 2 => Port8-11, Group 1 => Port 12-15,

------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_TX_Reset_Base                                                                     0x00C
#define cAf6Reg_SERDES_TX_Reset(G)\
                                                                                                     (0x00CUL)
#define cAf6Reg_SERDES_TX_Reset_WidthVal                                                                    32
#define cAf6Reg_SERDES_TX_Reset_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: txrst_trig
BitField Type: R/W
BitField Desc: Trige 0->1 to start reset TX SERDES, bit per sub port
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_SERDES_TX_Reset_txrst_trig_Bit_Start                                                            0
#define cAf6_SERDES_TX_Reset_txrst_trig_Bit_End    
#define cAf6_SERDES_TX_Reset_txrst_trig_Mask                                                           cBit1_0
#define cAf6_SERDES_TX_Reset_txrst_trig_Shift                                                                0
#define cAf6_SERDES_TX_Reset_txrst_trig0_Mask                                                            cBit0
#define cAf6_SERDES_TX_Reset_txrst_trig0_Shift                                                               0
#define cAf6_SERDES_TX_Reset_txrst_trig1_Mask                                                            cBit1
#define cAf6_SERDES_TX_Reset_txrst_trig1_Shift                                                               1
#define cAf6_SERDES_TX_Reset_txrst_trig_MaxVal                                                             0x3
#define cAf6_SERDES_TX_Reset_txrst_trig_MinVal                                                             0x0
#define cAf6_SERDES_TX_Reset_txrst_trig_RstVal                                                             0x0
#define cAf6_SERDES_TX_Reset_txrst_done_Mask                                                         cBit17_16


/*------------------------------------------------------------------------------
Reg Name   : SERDES RX Reset
Reg Addr   : 0x00D
Reg Formula: 0x00D
    Where  :
           + $G(0-3) : Group of Ports
Reg Desc   :
Reset RX SERDES, there is 4 group (0-3), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7,Group 2 => Port8-11, Group 1 => Port 12-15,

------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_RX_Reset_Base                                                                     0x00D
#define cAf6Reg_SERDES_RX_Reset(G)\
                                                                                                     (0x00DUL)
#define cAf6Reg_SERDES_RX_Reset_WidthVal                                                                    32
#define cAf6Reg_SERDES_RX_Reset_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: rxrst_trig
BitField Type: R/W
BitField Desc: Trige 0->1 to start reset RX SERDES, bit per sub port
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_SERDES_RX_Reset_rxrst_trig_Bit_Start                                                            0
#define cAf6_SERDES_RX_Reset_rxrst_trig_Bit_End                                                              1
#define cAf6_SERDES_RX_Reset_rxrst_trig_Mask                                                           cBit1_0
#define cAf6_SERDES_RX_Reset_rxrst_trig_Shift                                                                0
#define cAf6_SERDES_RX_Reset_rxrst_trig_MaxVal                                                             0x3
#define cAf6_SERDES_RX_Reset_rxrst_trig_MinVal                                                             0x0
#define cAf6_SERDES_RX_Reset_rxrst_trig_RstVal                                                             0x0
#define cAf6_SERDES_RX_Reset_rxrst_done_Mask                                                         cBit17_16

/*------------------------------------------------------------------------------
Reg Name   : SERDES LPMDFE Mode
Reg Addr   : 0x00E-0x30E
Reg Formula: 0x00E+$G*0x100
    Where  :
           + $G(0-3) : Group of Ports
Reg Desc   :
Configure LPM/DFE mode , there is 4 group (0-3), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7,Group 2 => Port8-11, Group 1 => Port 12-15,

------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_LPMDFE_Mode_Base                                                                  0x00E
#define cAf6Reg_SERDES_LPMDFE_Mode(G)\
                                                                                         (0x00EUL+(G)*0x100UL)
#define cAf6Reg_SERDES_LPMDFE_Mode_WidthVal                                                                 32
#define cAf6Reg_SERDES_LPMDFE_Mode_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: lpmdfe_mode
BitField Type: R/W
BitField Desc: bit per sub port
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_SERDES_LPMDFE_Mode_lpmdfe_mode_Bit_Start                                                        0
#define cAf6_SERDES_LPMDFE_Mode_lpmdfe_mode_Bit_End                                                          3
#define cAf6_SERDES_LPMDFE_Mode_lpmdfe_mode_Mask                                                       cBit1_0
#define cAf6_SERDES_LPMDFE_Mode_lpmdfe_mode_Shift                                                            0
#define cAf6_SERDES_LPMDFE_Mode_lpmdfe_mode_MaxVal                                                         0xf
#define cAf6_SERDES_LPMDFE_Mode_lpmdfe_mode_MinVal                                                         0x0
#define cAf6_SERDES_LPMDFE_Mode_lpmdfe_mode_RstVal                                                         0x0

#define cAf6_SERDES_LPMDFE_Mode_lpmdfe_port0_mode_Mask                                                   cBit0
#define cAf6_SERDES_LPMDFE_Mode_lpmdfe_port0_mode_Shift                                                      0

#define cAf6_SERDES_LPMDFE_Mode_lpmdfe_port1_mode_Mask                                                   cBit1
#define cAf6_SERDES_LPMDFE_Mode_lpmdfe_port1_mode_Shift                                                      1

/*------------------------------------------------------------------------------
Reg Name   : SERDES LPMDFE Reset
Reg Addr   : 0x00F
Reg Formula: 0x00F
    Where  :
           + $G(0-3) : Group of Ports
Reg Desc   :
Reset LPM/DFE , there is 4 group (0-3), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7,Group 2 => Port8-11, Group 1 => Port 12-15,

------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_LPMDFE_Reset_Base                                                                 0x00F
#define cAf6Reg_SERDES_LPMDFE_Reset(G)\
                                                                                                     (0x00FUL)
#define cAf6Reg_SERDES_LPMDFE_Reset_WidthVal                                                                32
#define cAf6Reg_SERDES_LPMDFE_Reset_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: lpmdfe_reset
BitField Type: R/W
BitField Desc: bit per sub port, Must be toggled after switching between modes
to initialize adaptation
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_SERDES_LPMDFE_Reset_lpmdfe_reset_Bit_Start                                                      0
#define cAf6_SERDES_LPMDFE_Reset_lpmdfe_reset_Bit_End                                                        1
#define cAf6_SERDES_LPMDFE_Reset_lpmdfe_reset_Mask                                                     cBit1_0
#define cAf6_SERDES_LPMDFE_Reset_lpmdfe_reset_Shift                                                          0
#define cAf6_SERDES_LPMDFE_Reset_lpmdfe_reset_MaxVal                                                       0x3
#define cAf6_SERDES_LPMDFE_Reset_lpmdfe_reset_MinVal                                                       0x0
#define cAf6_SERDES_LPMDFE_Reset_lpmdfe_reset_RstVal                                                       0x0

#define cAf6_SERDES_LPMDFE_Reset_lpmdfe_port0_reset_Mask                                                 cBit0
#define cAf6_SERDES_LPMDFE_Reset_lpmdfe_port0_reset_Shift                                                    0

#define cAf6_SERDES_LPMDFE_Reset_lpmdfe_port1_reset_Mask                                                 cBit1
#define cAf6_SERDES_LPMDFE_Reset_lpmdfe_port1_reset_Shift                                                    1

/*------------------------------------------------------------------------------
Reg Name   : SERDES TXDIFFCTRL
Reg Addr   : 0x0010
Reg Formula: 0x0010
    Where  :
           + $G(0-3) : Group of Ports
Reg Desc   :
Driver Swing Control, see "Table 3-35: TX Configurable Driver Ports" page 158 of UG578 for more detail, there is 4 group (0-3), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7

------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_TXDIFFCTRL_Base                                                                  0x0010
#define cAf6Reg_SERDES_TXDIFFCTRL(G)\
                                                                                                    (0x0010UL)
#define cAf6Reg_SERDES_TXDIFFCTRL_WidthVal                                                                  32
#define cAf6Reg_SERDES_TXDIFFCTRL_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: TXDIFFCTRL_subport1
BitField Type: R/W
BitField Desc:
BitField Bits: [09:05]
--------------------------------------*/
#define cAf6_SERDES_TXDIFFCTRL_TXDIFFCTRL_subport1_Mask                                                cBit7_4
#define cAf6_SERDES_TXDIFFCTRL_TXDIFFCTRL_subport1_Shift                                                     4

/*--------------------------------------
BitField Name: TXDIFFCTRL_subport0
BitField Type: R/W
BitField Desc:
BitField Bits: [04:00]
--------------------------------------*/
#define cAf6_SERDES_TXDIFFCTRL_TXDIFFCTRL_subport0_Mask                                                cBit3_0
#define cAf6_SERDES_TXDIFFCTRL_TXDIFFCTRL_subport0_Shift                                                     0

/*------------------------------------------------------------------------------
Reg Name   : SERDES TXPOSTCURSOR
Reg Addr   : 0x0011
Reg Formula: 0x0011
    Where  :
           + $G(0-3) : Group of Ports
Reg Desc   :
Transmitter post-cursor TX pre-emphasis control, see "Table 3-35: TX Configurable Driver Ports" page 160 of UG578 for more detail, there is 4 group (0-3), each group has 2 sub ports

------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_TXPOSTCURSOR_Base                                                                0x0011
#define cAf6Reg_SERDES_TXPOSTCURSOR(G)\
                                                                                                    (0x0011UL)
#define cAf6Reg_SERDES_TXPOSTCURSOR_WidthVal                                                                32
#define cAf6Reg_SERDES_TXPOSTCURSOR_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: TXPOSTCURSOR_subport1
BitField Type: R/W
BitField Desc:
BitField Bits: [09:05]
--------------------------------------*/
#define cAf6_SERDES_TXPOSTCURSOR_TXPOSTCURSOR_subport1_Bit_Start                                             5
#define cAf6_SERDES_TXPOSTCURSOR_TXPOSTCURSOR_subport1_Bit_End                                               9
#define cAf6_SERDES_TXPOSTCURSOR_TXPOSTCURSOR_subport1_Mask                                            cBit9_5
#define cAf6_SERDES_TXPOSTCURSOR_TXPOSTCURSOR_subport1_Shift                                                 5
#define cAf6_SERDES_TXPOSTCURSOR_TXPOSTCURSOR_subport1_MaxVal                                             0x1f
#define cAf6_SERDES_TXPOSTCURSOR_TXPOSTCURSOR_subport1_MinVal                                              0x0
#define cAf6_SERDES_TXPOSTCURSOR_TXPOSTCURSOR_subport1_RstVal                                              0x0

/*--------------------------------------
BitField Name: TXPOSTCURSOR_subport0
BitField Type: R/W
BitField Desc:
BitField Bits: [04:00]
--------------------------------------*/
#define cAf6_SERDES_TXPOSTCURSOR_TXPOSTCURSOR_subport0_Bit_Start                                             0
#define cAf6_SERDES_TXPOSTCURSOR_TXPOSTCURSOR_subport0_Bit_End                                               4
#define cAf6_SERDES_TXPOSTCURSOR_TXPOSTCURSOR_subport0_Mask                                            cBit4_0
#define cAf6_SERDES_TXPOSTCURSOR_TXPOSTCURSOR_subport0_Shift                                                 0
#define cAf6_SERDES_TXPOSTCURSOR_TXPOSTCURSOR_subport0_MaxVal                                             0x1f
#define cAf6_SERDES_TXPOSTCURSOR_TXPOSTCURSOR_subport0_MinVal                                              0x0
#define cAf6_SERDES_TXPOSTCURSOR_TXPOSTCURSOR_subport0_RstVal                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : SERDES TXPRECURSOR
Reg Addr   : 0x0012
Reg Formula: 0x0012
    Where  :
           + $G(0-3) : Group of Ports
Reg Desc   :
Transmitter pre-cursor TX pre-emphasis control, see "Table 3-35: TX Configurable Driver Ports" page 161 of UG578 for more detail, there is 4 group (0-3), each group has 2 sub ports

------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_TXPRECURSOR_Base                                                                 0x0012
#define cAf6Reg_SERDES_TXPRECURSOR(G)\
                                                                                                    (0x0012UL)
#define cAf6Reg_SERDES_TXPRECURSOR_WidthVal                                                                 32
#define cAf6Reg_SERDES_TXPRECURSOR_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: TXPRECURSOR_subport1
BitField Type: R/W
BitField Desc:
BitField Bits: [09:05]
--------------------------------------*/
#define cAf6_SERDES_TXPRECURSOR_TXPRECURSOR_subport1_Bit_Start                                               5
#define cAf6_SERDES_TXPRECURSOR_TXPRECURSOR_subport1_Bit_End                                                 9
#define cAf6_SERDES_TXPRECURSOR_TXPRECURSOR_subport1_Mask                                              cBit9_5
#define cAf6_SERDES_TXPRECURSOR_TXPRECURSOR_subport1_Shift                                                   5
#define cAf6_SERDES_TXPRECURSOR_TXPRECURSOR_subport1_MaxVal                                               0x1f
#define cAf6_SERDES_TXPRECURSOR_TXPRECURSOR_subport1_MinVal                                                0x0
#define cAf6_SERDES_TXPRECURSOR_TXPRECURSOR_subport1_RstVal                                                0x0

/*--------------------------------------
BitField Name: TXPRECURSOR_subport0
BitField Type: R/W
BitField Desc:
BitField Bits: [04:00]
--------------------------------------*/
#define cAf6_SERDES_TXPRECURSOR_TXPRECURSOR_subport0_Bit_Start                                               0
#define cAf6_SERDES_TXPRECURSOR_TXPRECURSOR_subport0_Bit_End                                                 4
#define cAf6_SERDES_TXPRECURSOR_TXPRECURSOR_subport0_Mask                                              cBit4_0
#define cAf6_SERDES_TXPRECURSOR_TXPRECURSOR_subport0_Shift                                                   0
#define cAf6_SERDES_TXPRECURSOR_TXPRECURSOR_subport0_MaxVal                                               0x1f
#define cAf6_SERDES_TXPRECURSOR_TXPRECURSOR_subport0_MinVal                                                0x0
#define cAf6_SERDES_TXPRECURSOR_TXPRECURSOR_subport0_RstVal                                                0x0

/*------------------------------------------------------------------------------
Reg Name   : SERDES PRBS generator test pattern control
Reg Addr   : x0020
Reg Formula:
    Where  :
           + $G(0-1) : Group of Ports
Reg Desc   :
------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_PRBS_Generate_pattern_control                                                    0x0020
#define cAf6_SERDES_RX_PRBS_PRBSSEL_Lane0_test_pattern_Mask                                            cBit3_0
#define cAf6_SERDES_RX_PRBS_PRBSSEL_Lane0_test_pattern_Shift                                                 0
#define cAf6_SERDES_RX_PRBS_PRBSSEL_Lane1_test_pattern_Mask                                            cBit7_4
#define cAf6_SERDES_RX_PRBS_PRBSSEL_Lane1_test_pattern_Shift                                                 4

/*------------------------------------------------------------------------------
Reg Name   : SERDES prbsctr_pen
Reg Addr   : 0x0021
Reg Formula: 0x0011
    Where  :{$G(0-1) : Lane of Ports}
Reg Desc   :SERDES prbsctr_pen
------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_prbsctr_pen                                                                      0x0021
#define cAf6_SERDES_PRBS_CNTRESET_Lane1_Error_Counter_Mask                                               cBit5
#define cAf6_SERDES_PRBS_CNTRESET_Lane1_Error_Counter_Shift                                                  5
#define cAf6_SERDES_PRBS_CNTRESET_Lane0_Error_Counter_Mask                                               cBit4
#define cAf6_SERDES_PRBS_CNTRESET_Lane0_Error_Counter_Shift                                                  4
#define cAf6_SERDES_TXPRBSFORCEERR_Lane1_Mask                                                            cBit1
#define cAf6_SERDES_TXPRBSFORCEERR_Lane1_Shift                                                               1
#define cAf6_SERDES_TXPRBSFORCEERR_Lane0_Mask                                                            cBit0
#define cAf6_SERDES_TXPRBSFORCEERR_Lane0_Shift                                                               0

/*------------------------------------------------------------------------------
Reg Name   : SERDES PRBS Diag Status
Reg Addr   : 0x0022
Reg Formula:
    Where  :{$G(0-1) : Lane of Ports}
Reg Desc   :
------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_PRBS_Status                                                                      0x0022
#define cAf6_SERDES_PRBS_RXPRBSLOCKED_Mask                                                           cBit11_10
#define cAf6_SERDES_PRBS_RXPRBSLOCKED_Shift                                                                 10
#define cAf6_SERDES_PRBS_RXPRBSERR_Mask                                                                cBit9_8
#define cAf6_SERDES_PRBS_RXPRBSERR_Shift                                                                     8
#define cAf6_SERDES_PRBS_RXPRBSLOCKED_Change_Mask                                                      cBit7_4
#define cAf6_SERDES_PRBS_RXPRBSLOCKED_Change_Shift                                                           4
#define cAf6_SERDES_PRBS_RXPRBSERR_Change_Mask                                                         cBit3_0
#define cAf6_SERDES_PRBS_RXPRBSERR_Change_Shift                                                              0

/*------------------------------------------------------------------------------
Reg Name   : SERDES_PRBS_RX_PRBS_ERR_CNT_RO
Reg Addr   : 0x23 - 0x24
Reg Formula:
    Where  : {$G(0-1) : Lane of Ports}
Reg Desc   : Counter RX PRBS error RO for Port 1-4 ( 0x23 :Port 1 ....0x24 Port 2)
------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_PRBS_RX_PRBS_ERR_CNT_RO                                                            0x23
#define cAf6_SERDES_RX_PRBS_ERR_CNT_RO_Mask                                                           cBit31_0
#define cAf6_SERDES_RX_PRBS_ERR_CNT_RO_Shift                                                                 0

/*------------------------------------------------------------------------------
Reg Name   : SERDES PRBS RX_PRBS_ERR_CNT R2C
Reg Addr   : 0x33 - 0x34
Reg Formula:
    Where  : {$G(0-1) : Lane of Ports}
Reg Desc   : Counter RX PRBS error R2C for Port 1-4 ( 0x23 :Port 1 ....0x24 Port 2)
------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_PRBS_RX_PRBS_ERR_CNT_R2C                                                           0x33
#define cAf6_SERDES_RX_PRBS_ERR_CNT_R2C_Mask                                                          cBit31_0
#define cAf6_SERDES_RX_PRBS_ERR_CNT_R2C_Shift                                                                0
/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061RXAUISERDESTUNINGREG_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAN
 *
 * File        : Tha60210061SemController.c
 *
 * Created Date: Oct 19, 2016
 *
 * Description : Sem controller for 60210061
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/man/AtDriverInternal.h"
#include "../../Tha60210011/physical/Tha60210011SemControllerInternal.h"
#include "Tha60210061SemController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061SemController
    {
    tTha60210011SemController super;
    }tTha60210061SemController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaSemControllerMethods m_ThaSemControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 DefaultOffset(ThaSemController self)
    {
    AtUnused(self);
    return 0xF42000;
    }

static void OverrideThaSemController(AtSemController self)
    {
    ThaSemController controller = (ThaSemController)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSemControllerOverride, mMethodsGet(controller), sizeof(m_ThaSemControllerOverride));

        mMethodOverride(m_ThaSemControllerOverride, DefaultOffset);
        }

    mMethodsSet(controller, &m_ThaSemControllerOverride);
    }

static void Override(AtSemController self)
    {
    OverrideThaSemController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061SemController);
    }

static AtSemController ObjectInit(AtSemController self, AtDevice device, uint32 semId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011SemControllerObjectInit(self, device, semId) == NULL)
        return NULL;

    /* Setup methods */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSemController Tha60210061SemControllerNew(AtDevice device, uint32 semId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSemController controller = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (controller == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(controller, device, semId);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha60210061SgmiiSerdesController.c
 *
 * Created Date: Jun 23, 2016
 *
 * Description : SGMII Serdes controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/man/AtModuleInternal.h"
#include "../../../default/man/versionreader/ThaVersionReader.h"
#include "../../../default/man/ThaDevice.h"
#include "../eth/Tha60210061ModuleEth.h"
#include "../prbs/Tha60210061ModulePrbs.h"
#include "Tha60210061SerdesManager.h"
#include "Tha60210061SerdesInternal.h"
#include "Tha60210061DrpReg.h"
#include "Tha60210061TopCtrReg.h"
#include "Tha60210061DrpReg.h"
#include "Tha60210061SerdesTuningReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cMdioSgmiiStatus               0x1
#define cMdioSgmiiStatusLinkdown       cBit2

#define cAf6_Sgmii_Timing_Control_Mask(lineId)  ((cBit7) << (8 * (lineId)))
#define cAf6_Sgmii_Timing_Control_Shift(lineId) (7 + (8 * (lineId)))

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210061SgmiiSerdesController *)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods                          m_AtObjectOverride;
static tAtSerdesControllerMethods                m_AtSerdesControllerOverride;
static tTha6021EthPortSerdesControllerMethods    m_Tha6021EthPortSerdesControllerOverride;

/* Save super implementation */
static const tAtObjectMethods                       *m_AtObjectMethods           = NULL;
static const tAtSerdesControllerMethods             *m_AtSerdesControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtDevice Device(AtSerdesController self)
    {
    return AtChannelDeviceGet(self->physicalPort);
    }

static AtModuleEth ModuleEth(AtSerdesController self)
    {
    return (AtModuleEth)AtDeviceModuleGet(Device(self), cAtModuleEth);
    }

static AtModuleSdh ModuleSdh(AtSerdesController self)
    {
    return (AtModuleSdh)AtDeviceModuleGet(Device(self), cAtModuleSdh);
    }

static AtModulePrbs ModulePrbs(AtSerdesController self)
    {
    return (AtModulePrbs)AtDeviceModuleGet(Device(self), cAtModulePrbs);
    }

static eBool DiagPrbsEngineIsSupported(AtSerdesController self)
    {
    /* HW remove prbs engine to have resource for Ethernet pass through */
    return (Tha60210061EthPassThroughIsSupported(ModuleEth(self))) ? cAtFalse : cAtTrue;
    }

static AtPrbsEngine PrbsEngineCreate(AtSerdesController self)
    {
    AtModulePrbs modulePrbs = ModulePrbs(self);

    if (Tha60210061ModulePrbsRawPrsbIsSupported(modulePrbs))
        return Tha60210061SgmiiSerdesRawPrbsEngineNew(self);

    if (AtSerdesControllerModeGet(self) != cAtSerdesModeEth1G)
        return NULL;

    if (Tha60210061ModulePrbsEthFramePrbsIsSupported(modulePrbs))
        return Tha60210061SerdesEthFramePrbsEngineNew(self);

    if (!DiagPrbsEngineIsSupported(self))
        return NULL;

    if (Tha60210061ModulePrbsSgmiiQsgmiiPerPortControl(modulePrbs))
        return Tha60210061SgmiiSerdesPrbsEngineV2New(self);

    return Tha60210061SgmiiSerdesPrbsEngineNew(self);
    }

static eBool IsControllable(AtSerdesController self)
    {
	if (AtSerdesControllerModeIsOcn(AtSerdesControllerModeGet(self)))
		return cAtTrue;

	if (AtSerdesControllerModeGet(self) == cAtSerdesModeEth1G)
        return cAtTrue;
        
    return cAtFalse;
    }

static uint32 HwIdGet(AtSerdesController self)
    {
    uint32 serdesId = AtSerdesControllerIdGet(self);

    /* XFI ports */
    if (serdesId == cTha60210061SgmiiSerdesId0) return 3;
    if (serdesId == cTha60210061SgmiiSerdesId1) return 4;
    if (serdesId == cTha60210061SgmiiSerdesId2) return 5;
    if (serdesId == cTha60210061SgmiiSerdesId3) return 6;

    return cInvalidUint32;
    }

static eAtRet SerdesModeSet(AtSerdesController self, eAtSerdesMode serdesMode)
    {
    uint32 regAddr = cTha60210061TopBaseAddress + cAf6Reg_o_control0_Base;
    uint32 regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    uint32 serdesId = AtSerdesControllerIdGet(self);

    if (serdesId == cTha60210061SgmiiSerdesId0)
        mRegFieldSet(regVal, cAf6_o_control0_SerdesModeP1_, (serdesMode == cAtSerdesModeEth1G) ? 0 : 1);

    else if (serdesId == cTha60210061SgmiiSerdesId1)
        mRegFieldSet(regVal, cAf6_o_control0_SerdesModeP2_, (serdesMode == cAtSerdesModeEth1G) ? 0 : 1);

    else if (serdesId == cTha60210061SgmiiSerdesId2)
        mRegFieldSet(regVal, cAf6_o_control0_SerdesModeP3_, (serdesMode == cAtSerdesModeEth1G) ? 0 : 1);

    else if (serdesId == cTha60210061SgmiiSerdesId3)
        mRegFieldSet(regVal, cAf6_o_control0_SerdesModeP4_, (serdesMode == cAtSerdesModeEth1G) ? 0 : 1);

    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eBool IsSimulated(AtSerdesController self)
    {
    AtChannel port = AtSerdesControllerPhysicalPortGet(self);
    return AtDeviceIsSimulated(AtChannelDeviceGet(port));
    }

static eAtRet Reset(AtSerdesController self)
    {
    uint32 triggerValue = cAf6_o_control5_TxPmaRstP1_Mask |
                          cAf6_o_control5_TxPcsRstP1_Mask |
                          cAf6_o_control5_RxPmaRstP1_Mask |
                          cAf6_o_control5_RxPcsRstP1_Mask;
    uint32 serdesId    = AtSerdesControllerIdGet(self);
    uint32 triggerMask = triggerValue << (serdesId * 8);
    uint32 regAddr     = cTha60210061TopBaseAddress + cAf6Reg_o_control5_Base;

    return Tha60210061SerdesResetWithMask(self, regAddr, triggerMask, cDontNeedToWaitResetDone, cDontNeedToWaitResetDone);
    }

static eAtRet LpmDfeReset(Tha6021EthPortSerdesController self)
    {
    uint32 hwId = AtSerdesControllerIdGet((AtSerdesController) self);
    uint32 rxPmaResetAddr     = (uint32)(cTha60210061TopBaseAddress + cAf6Reg_o_control5_Base);
    uint32 rxPmaResetTrigger  = cAf6_o_control5_RxPmaRstP1_Shift + (hwId * 8UL);
    uint32 rxPmaStatusReg     = (uint32)(cAf6Reg_SerdesTurning_BaseAddress + cAf6Reg_Serdes_Turning_Status_Base);
    uint32 rxPmaResetDoneMask = (uint32)(cAf6_sfpgt1_rxresetdone_Mask << (hwId));

    return Tha60210061SerdesResetWithMask((AtSerdesController)self, rxPmaResetAddr, rxPmaResetTrigger, rxPmaStatusReg, rxPmaResetDoneMask);
    }

static void ClockCorrectionSequenceSet(AtSerdesController self, uint32 value)
    {
    uint32 serdesId = AtSerdesControllerIdGet(self);
    uint32 regAddr = 0;
    uint32 regVal = 0;

    /* CLK_COR_SEQ_1_1 */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x1e, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_clk_cor_seq_1_1_, value);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* CLK_COR_SEQ_1_2 */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x1f, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_clk_cor_seq_1_2_, value);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* CLK_COR_SEQ_1_3 */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x20, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_clk_cor_seq_1_3_, value);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* CLK_COR_SEQ_1_4 */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x21, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_clk_cor_seq_1_4_, value);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* CLK_COR_SEQ_2_1 */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x22, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_clk_cor_seq_2_1_, value);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* CLK_COR_SEQ_2_2 */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x23, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_clk_cor_seq_2_2_, value);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* CLK_COR_SEQ_2_3 */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x24, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_clk_cor_seq_2_3_, value);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* CLK_COR_SEQ_2_4 */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x25, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_clk_cor_seq_2_4_, value);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);
    }

static uint32 StartVersionSupportStm1WorkInOc12Rate(void)
    {
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x3, 0x7, 0x0);
    }

static eBool Stm1WorkInOc12RateIsSupported(AtSerdesController self)
    {
    AtDevice device = AtSerdesControllerDeviceGet(self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);

    return (hwVersion >= StartVersionSupportStm1WorkInOc12Rate()) ? cAtTrue : cAtFalse;
    }

static eBool SerdesShouldWorkInOc12Rate(AtSerdesController self, eAtSerdesMode mode)
    {
    if (mode == cAtSerdesModeStm4)
        return cAtTrue;

    if ((mode == cAtSerdesModeStm1) && Stm1WorkInOc12RateIsSupported(self))
        return cAtTrue;

    return cAtFalse;
    }

static void SfpOcnCfg(AtSerdesController self, eAtSerdesMode serdesMode)
    {
    uint32 regAddr = 0;
    uint32 regVal = 0;
    uint32 serdesId = AtSerdesControllerIdGet(self);
    eBool workInOc12Rate = SerdesShouldWorkInOc12Rate(self, serdesMode);

    /* CBCC_DATA_SOURCE_SEL */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x63, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_cbcc_data_source_sel_, 0);
    mRegFieldSet(regVal, cAf6_drp_rxout_div_, (workInOc12Rate) ? 3 : 1);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* CPLL_FBDIV */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x28, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_cpll_fbdiv_, 2);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* RXCDR_CFG2 */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x10, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_rxcdr_cfg2_, (workInOc12Rate) ? 0x239 : 0x259);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* RXCDR_CFG2_GEN3 */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0xa4, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_rxcdr_cfg2_gen3_, (workInOc12Rate) ? 0x239 : 0x259);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* RXPI_CFG */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x9d, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_rxpi_cfg_, 0x5768);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* RXDFELPM_KL_CFG1 */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x36, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_rxdfelpm_kl_cfg1_, 2);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* RX_DFE_AGC_CFG1 */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x52, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_rx_dfe_agc_cfg1_, 4);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* RX_DFE_KL_LPM_KH_CFG1 */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x9b, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_rx_dfe_kl_lpm_kh_cfg1_, 4);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* RX_DFE_KL_LPM_KL_CFG1 */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x8c, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_rx_dfe_kl_lpm_kl_cfg1_, 4);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* RX_XCLK_SEL */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x64, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6Reg_drp_RX_XCLK_SEL, 2);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* TXPI_CFG0, TXPI_CFG1, TXPI_CFG2, TXPI_CFG5 */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x9c, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_txpi_cfg0_, 0);
    mRegFieldSet(regVal, cAf6_drp_txpi_cfg1_, 0);
    mRegFieldSet(regVal, cAf6_drp_txpi_cfg2_, 0);
    mRegFieldSet(regVal, cAf6_drp_txpi_cfg5_, 0);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* TX_PI_CFG0 */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0xff, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_tx_pi_cfg0_, 0);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* ALIGN_COMMA_ENABLE, ALIGN_COMMA_WORD */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x27, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_align_comma_enable_, 0);
    mRegFieldSet(regVal, cAf6_drp_align_comma_word_, 1);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* ALIGN_MCOMMA_DET */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x55, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_align_mcomma_det_, 0);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* ALIGN_PCOMMA_DET */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x56, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_align_pcomma_det_, 0);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* DEC_MCOMMA_DETECT */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0xcd, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_dec_mcomma_detect_, 0);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* DEC_PCOMMA_DETECT */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x2c, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_dec_pcomma_detect_, 0);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* RXPI_STARTCODE */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0xe0, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_rxpi_startcode_, (workInOc12Rate) ? 2 : 1);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* RX_CLK25_DIV */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x6d, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_rx_clk25_div_, 6);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* RX_DATA_WIDTH */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x03, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_rx_data_width_, 2);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* TXOUT_DIV */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x7c, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_txout_div_, (workInOc12Rate) ? 3 : 1);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* TX_CLK25_DIV, TX_DATA_WIDTH */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x7a, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_tx_clk25_div_, 6);
    mRegFieldSet(regVal, cAf6_drp_tx_data_width_, 2);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    ClockCorrectionSequenceSet(self, 0);
    }

static void SfpEth1GCfg(AtSerdesController self)
    {
    uint32 serdesId = AtSerdesControllerIdGet(self);
    uint32 regAddr = 0;
    uint32 regVal = 0;

    /* CBCC_DATA_SOURCE_SEL */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x63, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_cbcc_data_source_sel_, 0x1);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* CPLL_FBDIV */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x28, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_cpll_fbdiv_, 3);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* RXCDR_CFG2 */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x10, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_rxcdr_cfg2_, 0x249);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* RXCDR_CFG2_GEN3 */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0xa4, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_rxcdr_cfg2_gen3_, 0x249);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* RXOUT_DIV */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x63, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_rxout_div_, 2);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* RXPI_CFG */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x9d, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_rxpi_cfg_, 0x5668);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* RXDFELPM_KL_CFG1 */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x36, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_rxdfelpm_kl_cfg1_, 0xe2);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* RX_DFE_AGC_CFG1 */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x52, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_rx_dfe_agc_cfg1_, 2);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* RX_DFE_KL_LPM_KH_CFG1 */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x9b, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_rx_dfe_kl_lpm_kh_cfg1_, 2);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* RX_DFE_KL_LPM_KL_CFG1 */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x8c, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_rx_dfe_kl_lpm_kl_cfg1_, 2);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* RX_XCLK_SEL */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x64, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6Reg_drp_RX_XCLK_SEL, 0);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* TXPI_CFG0, TXPI_CFG1, TXPI_CFG2, TXPI_CFG5 */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x9c, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_txpi_cfg0_, 1);
    mRegFieldSet(regVal, cAf6_drp_txpi_cfg1_, 1);
    mRegFieldSet(regVal, cAf6_drp_txpi_cfg2_, 1);
    mRegFieldSet(regVal, cAf6_drp_txpi_cfg5_, 3);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* TX_PI_CFG0 */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0xff, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_tx_pi_cfg0_, 2);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* ALIGN_COMMA_ENABLE, ALIGN_COMMA_WORD */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x27, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_align_comma_enable_, 0x3ff);
    mRegFieldSet(regVal, cAf6_drp_align_comma_word_, 2);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* ALIGN_MCOMMA_DET */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x55, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_align_mcomma_det_, 1);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* ALIGN_PCOMMA_DET */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x56, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_align_pcomma_det_, 1);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* DEC_MCOMMA_DETECT */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0xcd, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_dec_mcomma_detect_, 1);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* DEC_PCOMMA_DETECT */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x2c, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_dec_pcomma_detect_, 1);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* RXPI_STARTCODE */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0xe0, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_rxpi_startcode_, 2);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* RX_CLK25_DIV */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x6d, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_rx_clk25_div_, 4);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* RX_DATA_WIDTH */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x03, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_rx_data_width_, 3);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* TXOUT_DIV */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x7c, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_txout_div_, 2);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* TX_CLK25_DIV, TX_DATA_WIDTH */
    regAddr = cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x7a, serdesId);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_drp_tx_clk25_div_, 4);
    mRegFieldSet(regVal, cAf6_drp_tx_data_width_, 3);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    ClockCorrectionSequenceSet(self, 0x100);
    }

static eAtRet TranceiverInit(AtSerdesController self, eAtSerdesMode serdesMode)
    {
    if (AtSerdesControllerModeGet(self) == serdesMode)
        return cAtOk;

    if (serdesMode == cAtSerdesModeEth1G)
        {
        SfpEth1GCfg(self);
        AtSerdesControllerTimingModeSet(self, cAtSerdesTimingModeLockToData);
        }
    else
    	{
        SfpOcnCfg(self, serdesMode);
        
        /* Cfg timming mode, mode cAtSerdesTimingModeLockToRef is Only for OC3, the other mode is cAtSerdesTimingModeLockToData */
		AtSerdesControllerTimingModeSet(self, (serdesMode == cAtSerdesModeStm1) ? cAtSerdesTimingModeLockToRef : cAtSerdesTimingModeLockToData);
    	}

    /* Reset to apply new configure */
    return AtSerdesControllerReset(self);
    }

static eBool ModeIsSupported(AtSerdesController self, eAtSerdesMode mode)
    {
    AtUnused(self);

    if ((mode == cAtSerdesModeStm1) ||
        (mode == cAtSerdesModeStm4) ||
        (mode == cAtSerdesModeStm16)||
        (mode == cAtSerdesModeEth1G))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet ModeSet(AtSerdesController self, eAtSerdesMode mode)
    {
    eAtRet ret;
    AtMdio mdio;
    uint32 serdesId = AtSerdesControllerIdGet(self);

    if (!ModeIsSupported(self, mode))
        return cAtErrorModeNotSupport;

    if ((ret = SerdesModeSet(self, mode)) != cAtOk)
        return ret;

    if ((ret = TranceiverInit(self, mode)) != cAtOk)
        return ret;

    if (!AtSerdesControllerInAccessible(self))
        {
        /* Disable isolate control and auto-neg after initializing tranceiver, anytime tranceiver is initialized
         * it will be enabled again, but we do not support this feature */
        mdio = AtSerdesControllerMdio(self);
        AtMdioSgmiiIsolateControlEnable(mdio, cAtFalse);
        AtMdioSgmiiAutoNegEnable(mdio, cAtFalse);
        }

    mThis(self)->mode = mode;
    if (mode == cAtSerdesModeEth1G)
        AtSerdesControllerPhysicalPortSet(self, (AtChannel)Tha60210061ModuleEthSgmiiPortGet(ModuleEth(self), serdesId));
    else
        AtSerdesControllerPhysicalPortSet(self, (AtChannel)AtModuleSdhLineGet(ModuleSdh(self), (uint8)serdesId));

    return cAtOk;
    }

static eAtSerdesMode ModeGet(AtSerdesController self)
    {
    return mThis(self)->mode;
    }

static uint32 MdioAlarmGet(AtSerdesController self)
    {
    AtMdio mdio = AtSerdesControllerMdio(self);
    uint32 regVal = AtMdioRead(mdio, 1, cMdioSgmiiStatus);

    if ((regVal & cMdioSgmiiStatusLinkdown) == 0)
        return cAtSerdesAlarmTypeLinkDown;

    return 0;
    }

static uint32 AlarmGet(AtSerdesController self)
    {
    return MdioAlarmGet(self);
    }

static eAtSerdesLinkStatus LinkStatus(AtSerdesController self)
    {
    AtMdio mdio = AtSerdesControllerMdio(self);
    uint32 regVal = AtMdioRead(mdio, 1, cMdioSgmiiStatus);

    if ((regVal & cMdioSgmiiStatusLinkdown) == 0)
        return cAtSerdesLinkStatusDown;

    return cAtSerdesLinkStatusUp;
    }

static eAtRet LayerLoopbackSet(AtSerdesController self, eAtSerdesPhysicalLayer layer, eAtLoopbackMode loopbackMode)
    {
    uint32 serdesId = AtSerdesControllerIdGet(self);
    uint32 regAddr, regVal;
    uint32 loopMask, loopShift;
    uint32 offset = (serdesId * 8);

    if (serdesId > cTha60210061SgmiiSerdesId3)
        return cAtErrorOutOfRangParm;

    regAddr   = cTha60210061TopBaseAddress + cAf6Reg_o_control5_Base;
    loopMask  = cAf6_o_control5_SerdesLoopP1_Mask << offset;
    loopShift = cAf6_o_control5_SerdesLoopP1_Shift + offset;

    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    switch (loopbackMode)
        {
        case cAtLoopbackModeLocal:
            mRegFieldSet(regVal, loop, (layer == cAtSerdesPhysicalLayerPcs) ? cAf6SerdesLoopbackNearEndPcs : cAf6SerdesLoopbackNearEndPma);
            break;
        case cAtLoopbackModeRemote:
            mRegFieldSet(regVal, loop, (layer == cAtSerdesPhysicalLayerPcs) ? cAf6SerdesLoopbackFarEndPcs : cAf6SerdesLoopbackFarEndPma);
            break;
        case cAtLoopbackModeRelease:
            mRegFieldSet(regVal, loop, cAf6SerdesLoopbackNormal);
            break;
        case cAtLoopbackModeDual:
        default:
            return cAtErrorModeNotSupport;
        }

    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);
    return cAtOk;
    }
 
static uint32 TxPreCursorAddress(Tha6021EthPortSerdesController self)
    {
    AtUnused(self);
    return (uint32)(cAf6Reg_SerdesTurning_BaseAddress + cAf6Reg_Serdes_txprecursor_Base);
    }

static uint32 PreCursorMask(Tha6021EthPortSerdesController self, uint32 serdesId)
    {
    uint32 hwId = AtSerdesControllerIdGet((AtSerdesController) self);
    AtUnused(serdesId);
    return (uint32)(cAf6_Serdes_txprecursor_sfpgt1_txprecursor_Mask << (hwId * 8UL));
    }

static uint32 PreCursorShift(Tha6021EthPortSerdesController self, uint32 serdesId)
    {
    uint32 hwId = AtSerdesControllerIdGet((AtSerdesController) self);
    AtUnused(serdesId);
    return (uint32)(cAf6_Serdes_txprecursor_sfpgt1_txprecursor_Shift + (hwId * 8UL));
    }

static uint32 TxPostCursorAddress(Tha6021EthPortSerdesController self)
    {
    AtUnused(self);
    return (uint32)(cAf6Reg_SerdesTurning_BaseAddress + cAf6Reg_Serdes_txpostcursor_sfp_Base);
    }

static uint32 TxDiffCtrlAddress(Tha6021EthPortSerdesController self)
    {
    AtUnused(self);
    return (uint32)(cAf6Reg_SerdesTurning_BaseAddress + cAf6Reg_Serdes_txdiffctrl_sfp_Base);
    }

static uint32 DiffCtrlMask(Tha6021EthPortSerdesController self, uint32 serdesId)
    {
    uint32 hwId = AtSerdesControllerIdGet((AtSerdesController) self);
    AtUnused(serdesId);
    return (uint32)(cAf6_Serdes_txdiffctrl_sfpgt1_txdiffctrl_Mask << (hwId * 8UL));
    }

static uint32 DiffCtrlShift(Tha6021EthPortSerdesController self, uint32 serdesId)
    {
    uint32 hwId = AtSerdesControllerIdGet((AtSerdesController) self);
    AtUnused(serdesId);
    return (uint32)(cAf6_Serdes_txdiffctrl_sfpgt1_txdiffctrl_Shift + (hwId * 8UL));
    }

static uint32 LpmEnableRegister(Tha6021EthPortSerdesController self)
    {
    AtUnused(self);
    return (uint32)(cAf6Reg_SerdesTurning_BaseAddress + cAf6Reg_Serdes_rxlpmen_Base);
    }

static uint32 LpmEnableMask(Tha6021EthPortSerdesController self, uint32 serdesId)
    {
    uint32 hwId = AtSerdesControllerIdGet((AtSerdesController) self);
    AtUnused(serdesId);
    return (uint32)(cAf6_Serdes_rxlpmen_sfpgt0_rxlpmen_Mask << (hwId));
    }

static uint32 LpmEnableShift(Tha6021EthPortSerdesController self, uint32 serdesId)
    {
    uint32 hwId = AtSerdesControllerIdGet((AtSerdesController) self);
    AtUnused(serdesId);
    return (uint32)(cAf6_Serdes_rxlpmen_sfpgt0_rxlpmen_Shift + (hwId));
    }

static uint32 LoopbackAddress(AtSerdesController self)
    {
    AtUnused(self);
    return (uint32)(cTha60210061TopBaseAddress + cAf6Reg_o_control5_Base);
    }

static uint32 LoopbackMask(AtSerdesController self)
    {
    uint32 hwId = AtSerdesControllerIdGet((AtSerdesController) self);
    return (uint32)(cAf6_o_control5_SerdesLoopP1_Mask << (hwId*8));
    }

static uint32 LoopbackShift(AtSerdesController self)
    {
    uint32 hwId = AtSerdesControllerIdGet((AtSerdesController) self);
    return (uint32)(cAf6_o_control5_SerdesLoopP1_Shift + (hwId*8));
    }

static eBool CanLoopback(AtSerdesController self, eAtLoopbackMode loopbackMode, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    switch ((uint32)loopbackMode)
        {
        case cAtLoopbackModeLocal:   return cAtTrue;
        case cAtLoopbackModeRemote:  return cAtTrue;
        case cAtLoopbackModeRelease: return cAtTrue;
        default:                     return cAtFalse;
        }
    }

static eBool UsePmaLoopback(AtSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint8 PmaLoopbackModeSw2Hw(AtSerdesController self, uint8 loopbackMode)
    {
    AtUnused(self);
    switch (loopbackMode)
        {
        case cAtLoopbackModeLocal:
            return cAf6SerdesLoopbackNearEndPma;
        case cAtLoopbackModeRemote:
            return cAf6SerdesLoopbackFarEndPma;
        default:
            return cAf6SerdesLoopbackNormal;
        }
    }

static uint8 PmaLoopbackModeHw2Sw(AtSerdesController self, uint8 hwLoopbackMode)
    {
    AtUnused(self);
    switch (hwLoopbackMode)
        {
        case cAf6SerdesLoopbackNearEndPma:
            return cAtLoopbackModeLocal;
        case cAf6SerdesLoopbackFarEndPma:
            return cAtLoopbackModeRemote;
        default:
            return cAtLoopbackModeRelease;
        }
    }

static uint8 PcsLoopbackModeSw2Hw(AtSerdesController self, uint8 loopbackMode)
    {
    AtUnused(self);
    switch (loopbackMode)
        {
        case cAtLoopbackModeLocal:
            return cAf6SerdesLoopbackNearEndPcs;
        case cAtLoopbackModeRemote:
            return cAf6SerdesLoopbackFarEndPcs;
        default:
            return cAf6SerdesLoopbackNormal;
        }
    }

static uint8 PcsLoopbackModeHw2Sw(AtSerdesController self, uint8 hwLoopbackMode)
    {
    AtUnused(self);
    switch (hwLoopbackMode)
        {
        case cAf6SerdesLoopbackNearEndPcs:
            return cAtLoopbackModeLocal;
        case cAf6SerdesLoopbackFarEndPcs:
            return cAtLoopbackModeRemote;
        default:
            return cAtLoopbackModeRelease;
        }
    }

static eAtRet HwLoopbackModeSet(AtSerdesController self, uint8 loopbackMode)
    {
    uint32 address = LoopbackAddress(self);
    uint32 regVal = AtSerdesControllerRead(self, address, cAtModuleSdh);

    mFieldIns(&regVal, LoopbackMask(self), LoopbackShift(self), loopbackMode);
    AtSerdesControllerWrite(self, address, regVal, cAtModuleSdh);
    
    return cAtOk;
    }

static eAtRet HwLoopbackSet(AtSerdesController self, eAtLoopbackMode loopbackMode)
    {
    Tha6021EthPortSerdesController controller = (Tha6021EthPortSerdesController)self;
    return mMethodsGet(controller)->HwLoopbackModeSet(self, Tha60210061SerdesLoopbackModeSw2Hw(self, loopbackMode));
    }

static uint8 HwLoopbackModeGet(AtSerdesController self)
    {
    uint32 regVal = AtSerdesControllerRead(self, LoopbackAddress(self), cAtModuleSdh);
    uint8 hwLoopbackMode;
    mFieldGet(regVal, LoopbackMask(self), LoopbackShift(self), uint8, &hwLoopbackMode);
    
    return hwLoopbackMode;
    }

static eAtLoopbackMode HwLoopbackGet(AtSerdesController self)
    {
    Tha6021EthPortSerdesController controller = (Tha6021EthPortSerdesController)self;
    return Tha60210061SerdesLoopbackModeHw2Sw(self, mMethodsGet(controller)->HwLoopbackModeGet(self));
    }

static eAtRet RemoteLoopbackEnable(AtSerdesController self, eBool enable)
    {
    if (enable)
        return HwLoopbackSet(self, cAtLoopbackModeRemote);

    if (HwLoopbackGet(self) == cAtLoopbackModeRemote)
        return HwLoopbackSet(self, cAtLoopbackModeRelease);

    return cAtOk;
    }

static eAtRet LocalLoopbackEnable(AtSerdesController self, eBool enable)
    {
    if (enable)
        return HwLoopbackSet(self, cAtLoopbackModeLocal);

    if (HwLoopbackGet(self)== cAtLoopbackModeLocal)
        return HwLoopbackSet(self, cAtLoopbackModeRelease);

    return cAtOk;
    }

static eAtRet LoopbackEnable(AtSerdesController self, eAtLoopbackMode loopbackMode, eBool enable)
    {
    return Tha60210061SerdesLoopbackEnable(self, loopbackMode, enable);
    }

static eBool EqualizerModeIsSupported(AtSerdesController self, eAtSerdesEqualizerMode mode)
    {
    AtUnused(self);
    if ((mode == cAtSerdesEqualizerModeDfe) || (mode == cAtSerdesEqualizerModeLpm))
        return cAtTrue;
    return cAtFalse;
    }

static eBool EqualizerCanControl(AtSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool PhysicalParamValueIsInRange(AtSerdesController self, eAtSerdesParam param, uint32 value)
    {
    AtUnused(self);

    if (param == cAtSerdesParamTxPreCursor)
        return (value > 31) ? cAtFalse : cAtTrue;

    if (param == cAtSerdesParamTxPostCursor)
        return (value > 31) ? cAtFalse : cAtTrue;

    if (param == cAtSerdesParamTxDiffCtrl)
        return (value > 31) ? cAtFalse : cAtTrue;

    return cAtFalse;
    }

static eBool EyeScanIsSupported(AtSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 EyeScanDrpBaseAddress(AtSerdesController self)
    {
    uint32 serdesId = AtSerdesControllerIdGet(self);
    return cAf6Reg_drp_BaseAddress + cAf6Reg_drp(0x0, serdesId);
    }

static AtEyeScanController EyeScanControllerObjectCreate(AtSerdesController self, uint32 drpBaseAddress)
    {
    return AtEyeScanControllerGtyE3New(self, drpBaseAddress);
    }

static eAtRet TimingModeSet(AtSerdesController self, eAtSerdesTimingMode timingMode)
    {
    uint32 regVal, address;
    uint32 serdesId = AtSerdesControllerIdGet(self);

    if (timingMode == cAtSerdesTimingModeAuto)
        return cAtErrorModeNotSupport;

    /* Lock to data register, mode is lock-to-reference write 1, otherwise write 0 */
    address = LoopbackAddress(self);
    regVal = AtSerdesControllerRead(self, address, cAtModuleSdh);
    mFieldIns(&regVal,
              cAf6_Sgmii_Timing_Control_Mask(serdesId),
              cAf6_Sgmii_Timing_Control_Shift(serdesId),
              (timingMode == cAtSerdesTimingModeLockToRef) ? 1 : 0);

    AtSerdesControllerWrite(self, address, regVal, cAtModuleSdh);

    return cAtOk;
    }

static eAtRet Enable(AtSerdesController self, eBool enable)
    {
    AtUnused(self);
    return enable ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool IsEnabled(AtSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtSerdesTimingMode TimingModeGet(AtSerdesController self)
    {
    uint32 regVal, hwValue;
    uint32 serdesId = AtSerdesControllerIdGet(self);

    /* Lock to data register, mode is lock-to-data write 1, otherwise write 0 */
    regVal = AtSerdesControllerRead(self, LoopbackAddress(self), cAtModuleSdh);
    mFieldGet(regVal,
              cAf6_Sgmii_Timing_Control_Mask(serdesId),
              cAf6_Sgmii_Timing_Control_Shift(serdesId),
              uint32 ,
              &hwValue);
    if (hwValue == 1)
        return cAtSerdesTimingModeLockToRef;

    return cAtSerdesTimingModeLockToData;
    }

static eBool CanControlTimingMode(AtSerdesController self)
    {
    AtChannel line = AtSerdesControllerPhysicalPortGet(self);
    AtDevice device = AtChannelDeviceGet(line);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    uint32 startSupportedVersion = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x1, 0x0, 0x51);

    return (hwVersion >= startSupportedVersion) ? cAtTrue : cAtFalse;
    }

static const char* Description(AtSerdesController self)
    {
    eAtSerdesMode mode = AtSerdesControllerModeGet(self);

    if (mode == cAtSerdesModeEth1G) return "SGMII-1G";
    if (mode == cAtSerdesModeStm1)  return "STM-1";
    if (mode == cAtSerdesModeStm4)  return "STM-4";
    if (mode == cAtSerdesModeStm16) return "STM-16";

    return "Unknown";
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210061SgmiiSerdesController *object = (tTha60210061SgmiiSerdesController *)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeUInt(mode);
    }

static void ResetTrigger(AtSerdesController self, uint32 resetReg, uint32 resetMask)
    {
    const uint32 cTriggerTimeInMs = 200;
    uint32 regVal = AtSerdesControllerRead(self, resetReg, cAtModuleEth);

    AtSerdesControllerWrite(self, resetReg, regVal | resetMask, cAtModuleEth);

    if (!IsSimulated(self))
        AtOsalUSleep(cTriggerTimeInMs * 1000);

    AtSerdesControllerWrite(self, resetReg, regVal & (~resetMask), cAtModuleEth);
    }

static eAtRet ResetDone(AtSerdesController self, uint32 address, uint32 doneMask)
    {
    tAtOsalCurTime curTime, startTime;
    const uint32 cResetTimeoutInMs = 5000;
    AtSerdesController controller = (AtSerdesController)self;
    uint32 elapse = 0;
    AtDevice device = AtChannelDeviceGet(AtSerdesControllerPhysicalPortGet(controller));

    AtOsalCurTimeGet(&startTime);
    while (elapse < cResetTimeoutInMs)
        {
        uint32 regVal = AtSerdesControllerRead(controller, address, cAtModuleEth);

        if (AtDeviceIsSimulated(device))
            return cAtOk;

        if (regVal & doneMask)
            return cAtOk;

        /* Calculate elapse time and retry */
        AtOsalCurTimeGet(&curTime);
        elapse = mTimeIntervalInMsGet(startTime, curTime);
        }

    return cAtErrorSerdesResetTimeout;
    }

static void OverrideAtObject(AtSerdesController self)
    {
    AtObject object = (AtObject)self;
    
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideTha6021EthPortSerdesController(AtSerdesController self)
    {
    Tha6021EthPortSerdesController controller = (Tha6021EthPortSerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6021EthPortSerdesControllerOverride, mMethodsGet(controller), sizeof(m_Tha6021EthPortSerdesControllerOverride));

        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, TxPreCursorAddress);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, TxPostCursorAddress);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, PreCursorMask);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, PreCursorShift);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, TxDiffCtrlAddress);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, DiffCtrlMask);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, DiffCtrlShift);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, LpmEnableRegister);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, LpmEnableMask);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, LpmEnableShift);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, HwLoopbackModeSet);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, HwLoopbackModeGet);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, LpmDfeReset);
        }

    mMethodsSet(controller, &m_Tha6021EthPortSerdesControllerOverride);
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSerdesControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, m_AtSerdesControllerMethods, sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, PrbsEngineCreate);
        mMethodOverride(m_AtSerdesControllerOverride, IsControllable);
        mMethodOverride(m_AtSerdesControllerOverride, HwIdGet);
        mMethodOverride(m_AtSerdesControllerOverride, ModeSet);
        mMethodOverride(m_AtSerdesControllerOverride, ModeGet);
        mMethodOverride(m_AtSerdesControllerOverride, AlarmGet);
        mMethodOverride(m_AtSerdesControllerOverride, LinkStatus);
        mMethodOverride(m_AtSerdesControllerOverride, CanLoopback);
        mMethodOverride(m_AtSerdesControllerOverride, LoopbackEnable);
        mMethodOverride(m_AtSerdesControllerOverride, LayerLoopbackSet);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerModeIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerCanControl);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalParamValueIsInRange);
        mMethodOverride(m_AtSerdesControllerOverride, EyeScanIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, EyeScanDrpBaseAddress);
        mMethodOverride(m_AtSerdesControllerOverride, EyeScanControllerObjectCreate);
        mMethodOverride(m_AtSerdesControllerOverride, TimingModeGet);
        mMethodOverride(m_AtSerdesControllerOverride, TimingModeSet);
        mMethodOverride(m_AtSerdesControllerOverride, CanControlTimingMode);
        mMethodOverride(m_AtSerdesControllerOverride, Description);
        mMethodOverride(m_AtSerdesControllerOverride, Enable);
        mMethodOverride(m_AtSerdesControllerOverride, IsEnabled);
        mMethodOverride(m_AtSerdesControllerOverride, Reset);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtObject(self);
    OverrideAtSerdesController(self);
    OverrideTha6021EthPortSerdesController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061SgmiiSerdesController);
    }

AtSerdesController Tha60210061SgmiiSerdesControllerObjectInit(AtSerdesController self, AtEthPort ethPort, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210061XfiSerdesControllerObjectInit(self, ethPort, serdesId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController Tha60210061SgmiiSerdesControllerNew(AtEthPort ethPort, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60210061SgmiiSerdesControllerObjectInit(newController, ethPort, serdesId);
    }

eAtRet Tha60210061SerdesLoopbackEnable(AtSerdesController self, eAtLoopbackMode loopbackMode, eBool enable)
    {
    if (loopbackMode == cAtLoopbackModeRemote)
        return RemoteLoopbackEnable(self, enable);

    if (loopbackMode == cAtLoopbackModeLocal)
        return LocalLoopbackEnable(self, enable);

    return HwLoopbackSet(self, cAtLoopbackModeRelease);
    }

eBool Tha60210061SerdesLoopbackIsEnabled(AtSerdesController self, eAtLoopbackMode loopbackMode)
    {
    return (loopbackMode == HwLoopbackGet(self)) ? cAtTrue : cAtFalse;
    }

uint8 Tha60210061SerdesLoopbackModeSw2Hw(AtSerdesController self, eAtLoopbackMode loopbackMode)
    {
    if (UsePmaLoopback(self))
        return PmaLoopbackModeSw2Hw(self, loopbackMode);
    return PcsLoopbackModeSw2Hw(self, loopbackMode);
    }

eAtLoopbackMode Tha60210061SerdesLoopbackModeHw2Sw(AtSerdesController self, uint8 hwLoopbackMode)
    {
    if (UsePmaLoopback(self))
        return PmaLoopbackModeHw2Sw(self, hwLoopbackMode);

    return PcsLoopbackModeHw2Sw(self, hwLoopbackMode);
    }

eAtRet Tha60210061SerdesResetWithMask(AtSerdesController self, uint32 resetReg, uint32 resetMask, uint32 statusReg, uint32 doneMask)
    {
    if (AtSerdesControllerInAccessible(self))
        return cAtOk;

    ResetTrigger(self, resetReg, resetMask);
    if (doneMask != cDontNeedToWaitResetDone)
        return ResetDone(self, statusReg, doneMask);

    return cAtOk;
    }

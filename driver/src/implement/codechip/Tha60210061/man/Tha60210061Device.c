/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60210061Device.c
 *
 * Created Date: Jun 16, 2016
 *
 * Description : 60210061 Device implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/man/AtIpCoreInternal.h"
#include "../../../default/ber/ThaModuleBer.h"
#include "../../../default/cdr/ThaModuleCdr.h"
#include "../../../default/ocn/ThaModuleOcn.h"
#include "../../../default/sur/hard/ThaModuleHardSur.h"
#include "../../../default/xc/ThaModuleXc.h"
#include "../../Tha60210012/man/Tha60210012DeviceInternal.h"
#include "../../Tha60210011/common/Tha602100xxCommon.h"
#include "../physical/Tha60210061SerdesManager.h"
#include "../cla/Tha60210061ModuleCla.h"
#include "../physical/Tha60210061TopCtrReg.h"
#include "../physical/Tha60210061SemController.h"
#include "../ram/Tha60210061ModuleRam.h"
#include "../clock/Tha60210061ModuleClock.h"
#include "../pktanalyzer/Tha60210061ModulePktAnalyzer.h"
#include "../concate/Tha60210061ModuleConcate.h"
#include "../eth/Tha60210061ModuleEth.h"
#include "../mpeg/Tha60210061ModuleMpeg.h"
#include "../prbs/Tha60210061ModulePrbs.h"
#include "../encap/Tha60210061ModuleEncap.h"
#include "../mpig/Tha60210061ModuleMpig.h"
#include "Tha60210061InterruptController.h"

/*--------------------------- Define -----------------------------------------*/
#define cStickyReg             0xF00051
#define cDdr1CalibFailMask     cBit0
#define cDdr2CalibFailMask     cBit1
#define cQdrCalibFailMask      cBit2
#define cPllNotLockMask        cBit8

#define cLosPinsEnableReg      0xF00047
#define cLosPinsEnableMask     cBit0
#define cLosPinsEnableShift    0

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210061Device*)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061Device
    {
    tTha60210012Device super;
    uint32 asyncInitState;
    }tTha60210061Device;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods          m_AtObjectOverride;
static tAtDeviceMethods          m_AtDeviceOverride;
static tThaDeviceMethods         m_ThaDeviceOverride;
static tTha60210011DeviceMethods m_Tha60210011DeviceOverride;
static tTha60210051DeviceMethods m_Tha60210051DeviceOverride;
static tTha60150011DeviceMethods m_Tha60150011DeviceOverride;
static tTha60210012DeviceMethods m_Tha60210012DeviceOverride;

/* Super implementation */
static const tAtObjectMethods          *m_AtObjectMethods          = NULL;
static const tAtDeviceMethods          *m_AtDeviceMethods          = NULL;
static const tTha60150011DeviceMethods *m_Tha60150011DeviceMethods = NULL;
static const tTha60210012DeviceMethods *m_Tha60210012DeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    eThaPhyModule phyModule = (eThaPhyModule)moduleId;

    /* Public modules */
    if (moduleId  == cAtModuleEncap)        return (AtModule)Tha60210061ModuleEncapNew(self);
    if (moduleId  == cAtModuleEth)          return (AtModule)Tha60210061ModuleEthNew(self);
    if (moduleId  == cAtModuleSdh)          return (AtModule)Tha60210061ModuleSdhNew(self);
    if (moduleId  == cAtModulePdh)          return (AtModule)Tha60210061ModulePdhNew(self);
    if (moduleId  == cAtModulePrbs)         return (AtModule)Tha60210061ModulePrbsNew(self);
    if (moduleId  == cAtModuleBer)          return (AtModule)Tha60210061ModuleBerNew(self);
    if (moduleId  == cAtModulePw)           return (AtModule)Tha60210061ModulePwNew(self);
    if (moduleId  == cAtModuleSur)          return (AtModule)Tha60210061ModuleSurNew(self);
    if (moduleId  == cAtModuleXc)           return (AtModule)Tha60210061ModuleXcNew(self);
    if (moduleId  == cAtModuleRam)          return (AtModule)Tha60210061ModuleRamNew(self);
    if (moduleId  == cAtModuleClock)        return (AtModule)Tha60210061ModuleClockNew(self);
    if (moduleId  == cAtModuleConcate)      return (AtModule)Tha60210061ModuleConcateNew(self);
    if (moduleId  == cAtModulePktAnalyzer)  return (AtModule)Tha60210061ModulePktAnalyzerNew(self);

    /* Physical modules */
    if (phyModule == cThaModuleMap)  return Tha60210061ModuleMapNew(self);
    if (phyModule == cThaModuleDemap)  return Tha60210061ModuleDemapNew(self);
    if (phyModule == cThaModuleCdr)  return Tha60210061ModuleCdrNew(self);
    if (phyModule == cThaModuleOcn)  return Tha60210061ModuleOcnNew(self);
    if (phyModule == cThaModulePoh)  return Tha60210061ModulePohNew(self);
    if (phyModule == cThaModulePwe)  return Tha60210061ModulePweNew(self);
    if (phyModule == cThaModuleCla)  return Tha60210061ModuleClaNew(self);
    if (phyModule == cThaModulePda)  return Tha60210061ModulePdaNew(self);
    if (phyModule == cThaModuleMpeg) return Tha60210061ModuleMpegNew(self);
    if (phyModule == cThaModuleMpig) return Tha60210061ModuleMpigNew(self);

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static eBool XfiDiagPerGroupIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static ThaModuleEth ModuleEth(AtDevice self)
    {
    return (ThaModuleEth)AtDeviceModuleGet(self, cAtModuleEth);
    }

static AtModulePrbs ModulePrbs(AtDevice self)
    {
    return (AtModulePrbs)AtDeviceModuleGet(self, cAtModulePrbs);
    }

static eAtRet TopDiagnosticModeEnable(AtDevice self, eBool enable)
    {
    AtIpCore core = AtDeviceIpCoreGet(self, 0);
    uint32 regAddr = cTha60210061TopBaseAddress + cAf6Reg_o_control3_Base;
    uint32 regVal = AtIpCoreRead(core, regAddr);
    uint8 diag = enable ? 1 : 0;

    mRegFieldSet(regVal, cAf6_o_control3_DDR1_DiagEn_, diag);
    mRegFieldSet(regVal, cAf6_o_control3_DDR2_DiagEn_, diag);
    mRegFieldSet(regVal, cAf6_o_control3_QDR1_DiagEn_, diag);

    if (Tha60210061ModulePrbsEthFramePrbsIsSupported(ModulePrbs(self)))
        diag = 0; /* When common ethernet prbs is supported, these bits must be 0 in any case */

    mRegFieldSet(regVal, cAf6_o_control3_SFP_OCN_DiagEn_, diag);
    mRegFieldSet(regVal, cAf6_o_control3_XFI_DiagEn_, diag);
    mRegFieldSet(regVal, cAf6_o_control3_QSGMII_DiagEn_, diag);
    mRegFieldSet(regVal, cAf6_o_control3_SFP_SGMII_DiagEn_, diag);

    AtIpCoreWrite(core, regAddr, regVal);

    return cAtOk;
    }

static eAtRet HwDiagnosticModeEnable(AtDevice self, eBool enable)
    {
    eAtRet ret = cAtOk;
    AtModulePrbs modulePrbs = ModulePrbs(self);

    if (Tha60210061ModulePrbsEthFramePrbsIsSupported(modulePrbs))
        {
        ThaModuleEth moduleEth = (ThaModuleEth)ModuleEth(self);

        if (enable)
            ret = AtModuleInit((AtModule)moduleEth);

        else
            ret = Tha60210061DiagEthFramePrbsGenEngineEnable(modulePrbs, cAtFalse);

        ret |= Tha60210061ModuleEthCoreIsolationEnable(moduleEth, enable);
        }

    ret |= TopDiagnosticModeEnable(self, enable);

    return ret;
    }

static AtSerdesManager SerdesManagerObjectCreate(AtDevice self)
    {
    return Tha60210061SerdesManagerNew(self);
    }

static uint32 StartVersionSupportPowerControl(AtDevice self)
    {
    /* This product does not support */
    AtUnused(self);
    return cBit31_0;
    }

static eAtRet DiagnosticModeDisable(AtDevice self)
    {
    AtDeviceDiagnosticModeEnable(self, cAtFalse);
    return cAtOk;
    }

static uint8 NumUsedOc48Slice(Tha60210011Device self)
    {
    AtUnused(self);
    return 1;
    }

static ThaIntrController IntrControllerCreate(ThaDevice self, AtIpCore core)
    {
    AtUnused(self);
    return Tha60210061IntrControllerNew(core);
    }

static uint32 StartHwVersionSupportNewStickyRegiter(void)
    {
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x2, 0x1020);
    }

static eBool NewStickyRegiterIsSupported(AtDevice self)
    {
    ThaVersionReader versionReader = ThaDeviceVersionReader(self);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);

    if (AtDeviceIsSimulated(self))
        return cAtTrue;

    if (hwVersion >= StartHwVersionSupportNewStickyRegiter())
        return cAtTrue;

    return cAtFalse;
    }

static eBool PllIsLocked(AtDevice self)
    {
    const uint32 cTimeoutMs = 5000;

    if (NewStickyRegiterIsSupported(self))
        return m_Tha60150011DeviceMethods->PllIsLocked(self);

    return ThaDeviceStickyIsGood(self, cStickyReg, cPllNotLockMask, cTimeoutMs);
    }

static eAtRet QdrDiagnostic(AtDevice self)
    {
    static const uint32 cTimeoutMs = 5000;
    uint32 masks = cQdrCalibFailMask;
    eBool isGood;

    if (NewStickyRegiterIsSupported(self))
        return m_Tha60150011DeviceMethods->QdrDiagnostic(self);

    isGood = ThaDeviceStickyIsGood(self, cStickyReg, masks, cTimeoutMs);
    if (isGood)
        AtDeviceLog(self, cAtLogLevelInfo, AtSourceLocation, "%s: QDR calib done\r\n", AtFunction);
    else
        AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "%s: QDR calib fail\r\n", AtFunction);

    return isGood ? cAtOk : cAtErrorQdrCalibFail;
    }

static eBool DdrCalibIsGood(AtDevice self)
    {
    uint32 cTimeoutMs = 20000;
    uint32 regVal;
    uint32 masks = cDdr1CalibFailMask | cDdr2CalibFailMask;

    if (NewStickyRegiterIsSupported(self))
        return m_Tha60150011DeviceMethods->DdrCalibIsGood(self);

    if (ThaDeviceStickyIsGood(self, cStickyReg, masks, cTimeoutMs))
        return cAtTrue;

    regVal = AtIpCoreRead(AtDeviceIpCoreGet(self, 0), cStickyReg);
    if (regVal & cDdr1CalibFailMask)
        AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "%s: DDR#1 calib fail\r\n", AtFunction);
    if (regVal & cDdr2CalibFailMask)
        AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "%s: DDR#2 calib fail\r\n", AtFunction);

    return cAtFalse;
    }

static AtSemController SemControllerObjectCreate(AtDevice self, uint32 semId)
    {
        return Tha60210061SemControllerNew(self, semId);
    }

static uint8 NumSemControllersGet(AtDevice self)
    {
    AtUnused(self);
    return 1;
    }

static uint32 StartVersionSupportUart(Tha60150011Device self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x3, 0x1008);
    }

static eBool SemUartIsSupported(Tha60150011Device self)
    {
    uint32 startVersionHasThis = StartVersionSupportUart(self);
    return (AtDeviceVersionNumber((AtDevice)self) >= startVersionHasThis) ? cAtTrue : cAtFalse;
    }

static uint32 DdrCalibTimeoutMs(Tha60210011Device self)
    {
    AtUnused(self);
    return 30000;
    }

static eBool UseModuleDefault(void)
    {
    return Tha602100xxHwAccessOptimized();
    }

static eAtRet MapHoHwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;

    /* LO path loopback */
    ThaDeviceMemoryFlush(device, 0x100005, 0x100005, 0x0);

    /* HO MAP */
    ThaDeviceMemoryFlush(device, 0x60000, 0x600ff, 0x0);
    ThaDeviceMemoryFlush(device, 0x64000, 0x640ff, 0x0);
    ThaDeviceMemoryFlush(device, 0x68200, 0x68200, 0x0);
    ThaDeviceMemoryFlush(device, 0x68300, 0x68300, 0x0);
    ThaDeviceMemoryFlush(device, 0x68320, 0x68320, 0x0);
    ThaDeviceMemoryFlush(device, 0x68400, 0x68400, 0x0);
    ThaDeviceMemoryFlush(device, 0x68600, 0x68600, 0x0);
    ThaDeviceMemoryFlush(device, 0x68510, 0x68510, 0x0);
    ThaDeviceMemoryFlush(device, 0x68710, 0x68710, 0x0);

    return cAtOk;
    }

static eAtRet CdrHoHwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;
    /* HO CDR */
    ThaDeviceMemoryFlush(device, 0x300100, 0x30011f, 0x0);
    ThaDeviceMemoryFlush(device, 0x320800, 0x32082f, 0x0);

    return cAtOk;
    }

static eAtRet Pdh1_1HwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;
    /* PDH: OC24#0 */
    ThaDeviceMemoryFlush(device, 0x1024000, 0x10243ff, 0x0);
    ThaDeviceMemoryFlush(device, 0x1076000, 0x10763ff, 0x0);
    ThaDeviceMemoryFlush(device, 0x1040000, 0x104001f, 0x0);
    ThaDeviceMemoryFlush(device, 0x1080000, 0x108001f, 0x0);
    ThaDeviceMemoryFlush(device, 0x1041000, 0x10410ff, 0x0);
    ThaDeviceMemoryFlush(device, 0x1081000, 0x10810ff, 0x0);
    ThaDeviceMemoryFlush(device, 0x1054000, 0x10543ff, 0x0);

    ThaDeviceMemoryFlush(device, 0x1055000, 0x10553ff, 0x0);

    ThaDeviceMemoryFlush(device, 0x1094000, 0x10943ff, 0x0);
    return cAtOk;
    }

static eAtRet Pdh1_2HwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;
    /* PDH: OC24#01 */
    ThaDeviceMemoryFlush(device, 0x1190002, 0x1190003, 0x0);
    ThaDeviceMemoryFlush(device, 0x1130020, 0x113003f, 0x0);
    ThaDeviceMemoryFlush(device, 0x1124000, 0x11243ff, 0x0);
    ThaDeviceMemoryFlush(device, 0x1176000, 0x11763ff, 0x0);
    ThaDeviceMemoryFlush(device, 0x1140000, 0x114001f, 0x0);
    ThaDeviceMemoryFlush(device, 0x1180000, 0x118001f, 0x0);
    ThaDeviceMemoryFlush(device, 0x1141000, 0x11410ff, 0x0);
    ThaDeviceMemoryFlush(device, 0x1181000, 0x11810ff, 0x0);
    ThaDeviceMemoryFlush(device, 0x1154000, 0x11543ff, 0x0);

    ThaDeviceMemoryFlush(device, 0x1155000, 0x11553ff, 0x0);

    ThaDeviceMemoryFlush(device, 0x1194000, 0x11943ff, 0x0);

    return cAtOk;
    }

static eAtRet Map1_1HwFlush1(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;

    if (!UseModuleDefault())
        {
        ThaDeviceMemoryFlush(device, 0x800000, 0x800FFF, 0x0);
        ThaDeviceMemoryFlush(device, 0x801000, 0x801FFF, 0x0);
        ThaDeviceMemoryFlush(device, 0x802000, 0x802FFF, 0x0);
        ThaDeviceMemoryFlush(device, 0x803000, 0x803FFF, 0x0);
        }

    ThaDeviceMemoryFlush(device, 0x804000, 0x804FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x805000, 0x805FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x806000, 0x806FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x807000, 0x807FFF, 0x0);

    return cAtOk;
    }

static eAtRet Map1_1HwFlush2(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;

    if (!UseModuleDefault())
        {
        ThaDeviceMemoryFlush(device, 0x810000, 0x8103FF, 0x0);
        ThaDeviceMemoryFlush(device, 0x814000, 0x814FFF, 0x0);
        ThaDeviceMemoryFlush(device, 0x815000, 0x815FFF, 0x0);
        ThaDeviceMemoryFlush(device, 0x816000, 0x816FFF, 0x0);
        ThaDeviceMemoryFlush(device, 0x817000, 0x817FFF, 0x0);
        }

    ThaDeviceMemoryFlush(device, 0x818000, 0x818FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x819000, 0x819FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x81A000, 0x81AFFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x81B000, 0x81BFFF, 0x0);

    return cAtOk;
    }

static eAtRet Map1_2HwFlush1(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;

    if (UseModuleDefault())
        return cAtOk;

    ThaDeviceMemoryFlush(device, 0x840000, 0x800FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x841000, 0x801FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x842000, 0x802FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x843000, 0x803FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x844000, 0x804FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x845000, 0x805FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x846000, 0x806FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x847000, 0x807FFF, 0x0);

    return cAtOk;
    }

static eAtRet Map1_2HwFlush2(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;

    if (UseModuleDefault())
        return cAtOk;

    ThaDeviceMemoryFlush(device, 0x850000, 0x8103FF, 0x0);
    ThaDeviceMemoryFlush(device, 0x854000, 0x814FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x855000, 0x815FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x856000, 0x816FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x857000, 0x817FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x858000, 0x818FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x859000, 0x819FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x85A000, 0x81AFFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x85B000, 0x81BFFF, 0x0);

    return cAtOk;
    }

static eAtRet Cdr1_1HwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;
    ThaDeviceMemoryFlush(device, 0xc00800, 0xc0081f, 0x0);
    ThaDeviceMemoryFlush(device, 0xc00c00, 0xc00fff, 0x0);
    ThaDeviceMemoryFlush(device, 0xc20800, 0xc20bff, 0x0);
    ThaDeviceMemoryFlush(device, 0xc21000, 0xc213ff, 0x0);

    return cAtOk;
    }

static eAtRet Cdr1_2HwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;
    ThaDeviceMemoryFlush(device, 0xc40800, 0xc4081f, 0x0);
    ThaDeviceMemoryFlush(device, 0xc40c00, 0xc40fff, 0x0);
    ThaDeviceMemoryFlush(device, 0xc60800, 0xc60bff, 0x0);
    ThaDeviceMemoryFlush(device, 0xc61000, 0xc613ff, 0x0);

    return cAtOk;
    }

static eAtRet PweHwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;

    /* PWE */
    ThaDeviceMemoryFlush(device, 0x0070000, 0x0070FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x0071000, 0x0071FFF, 0x0);

    return cAtOk;
    }

static eAtRet PlaHwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;

    /* Payload Assembler Low-Order(LO) Payload Control */
    ThaDeviceMemoryFlush(device, 0x401000, 0x4013FF, 0x0);
    ThaDeviceMemoryFlush(device, 0x403000, 0x4033FF, 0x0);

    /* Payload Assembler Low-Order(LO) Add/Remove Pseudowire Protocol Control */
    ThaDeviceMemoryFlush(device, 0x401800, 0x401BFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x403800, 0x403BFF, 0x0);

    /* Payload Assembler Low-Order(LO) Lookup Control */
    ThaDeviceMemoryFlush(device, 0x404000, 0x4043FF, 0x0);
    ThaDeviceMemoryFlush(device, 0x404400, 0x4047FF, 0x0);

    /* Payload Assembler High-Order(HO) Payload Control */
    ThaDeviceMemoryFlush(device, 0x410080, 0x4100AF, 0x0);

    /* Payload Assembler High-Order(HO) Add/Remove Pseudowire Protocol Control */
    ThaDeviceMemoryFlush(device, 0x4100C0, 0x4100EF, 0x0);

    /* Payload Assembler High-Order(HO) Lookup Control */
    ThaDeviceMemoryFlush(device, 0x410200, 0x41022F, 0x0);

    /* Payload Assembler Common Pseudowire Control */
    ThaDeviceMemoryFlush(device, 0x418000, 0x4185FF, 0x0);

    /* Payload Assembler Protection Flow Control */
    ThaDeviceMemoryFlush(device, 0x430000, 0x4305FF, 0x0);

    /* Payload Assembler Flow UPSR Control */
    ThaDeviceMemoryFlush(device, 0x434000, 0x4345FF, 0x0);

    /* Payload Assembler Flow HSPW Control */
    ThaDeviceMemoryFlush(device, 0x438000, 0x4385FF, 0x0);

    /* Payload Assembler Block Buffer Control */
    ThaDeviceMemoryFlush(device, 0x424000, 0x424FFF, 0x0);

    /* Payload Assembler Lookup Flow Control */
    ThaDeviceMemoryFlush(device, 0x42C000, 0x42C6FF, 0x0);

    /* Payload Assembler Output Flow Control */
    ThaDeviceMemoryFlush(device, 0x428000, 0x4287FF, 0x0);

    /* Payload Assembler Block Buffer Control */
    ThaDeviceMemoryFlush(device, 0x424000, 0x424FFF, cBit14_0);

    return cAtOk;
    }

static eAtRet PdaHwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;

    /* Pseudowire PDA Jitter Buffer Control   */
    ThaDeviceMemoryLongFlush(device, 0x00510000, 0x005105FF);

    /* Pseudowire PDA Reorder Control */
    ThaDeviceMemoryFlush(device, 0x00520000, 0x005205FF, 0);

    /* Pseudowire PDA per OC48 Low Order TDM Mode Control */
    ThaDeviceMemoryFlush(device, 0x00530000, 0x005303FF, 0);
    ThaDeviceMemoryFlush(device, 0x00530400, 0x005307FF, 0);

    /* Pseudowire PDA Low Order TDM or MLPPP Look Up Control */
    ThaDeviceMemoryFlush(device, 0x00540000, 0x005403FF, 0);
    ThaDeviceMemoryFlush(device, 0x00540400, 0x005407FF, 0);

    /* Pseudowire PDA VCAT Look Up Control */
    ThaDeviceMemoryFlush(device, 0x00541000, 0x005410FF, 0);

    /* Pseudowire PDA High Order TDM Look Up Control */
    ThaDeviceMemoryFlush(device, 0x00541100, 0x0054112F, 0);

    /* Pseudowire PDA per OC48 High Order TDM Mode Control */
    ThaDeviceMemoryFlush(device, 0x00550000, 0x0055002F, 0);

    return cAtOk;
    }

static eAtRet OcnHwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;
    /* OCN */
    ThaDeviceMemoryFlush(device, 0x102000, 0x10202f, 0);
    ThaDeviceMemoryFlush(device, 0x103000, 0x10302f, 0);
    ThaDeviceMemoryFlush(device, 0x104000, 0x10402f, 0);
    ThaDeviceMemoryFlush(device, 0x104100, 0x10412f, 0);
    ThaDeviceMemoryFlush(device, 0x105000, 0x10502f, 0);
    ThaDeviceMemoryFlush(device, 0x110000, 0x11002f, 0);
    ThaDeviceMemoryFlush(device, 0x110800, 0x110fff, 0);
    ThaDeviceMemoryFlush(device, 0x120000, 0x12002f, 0);
    ThaDeviceMemoryFlush(device, 0x120800, 0x120fff, 0);
    ThaDeviceMemoryFlush(device, 0x106000, 0x10602f, 0);

    return cAtOk;
    }

static eAtRet ClaHbceLookupExtra0_3HwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;

    if (UseModuleDefault())
        return cAtOk;

    ThaDeviceMemoryLongFlush(device, 0x640000, 0x6403FF);
    ThaDeviceMemoryLongFlush(device, 0x641000, 0x6413FF);
    ThaDeviceMemoryLongFlush(device, 0x642000, 0x6423FF);
    ThaDeviceMemoryLongFlush(device, 0x643000, 0x6433FF);

    return cAtOk;
    }

static eAtRet ClaHbceLookupExtra4_7HwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;

    if (UseModuleDefault())
        return cAtOk;

    ThaDeviceMemoryLongFlush(device, 0x644000, 0x6443FF);
    ThaDeviceMemoryLongFlush(device, 0x645000, 0x6453FF);
    ThaDeviceMemoryLongFlush(device, 0x646000, 0x6463FF);
    ThaDeviceMemoryLongFlush(device, 0x647000, 0x6473FF);

    return cAtOk;
    }

static eAtRet ClaHbceHashTableHwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;

    if (UseModuleDefault())
        return cAtOk;

    ThaDeviceMemoryFlush(device, 0x620000, 0x621FFF, 0);

    return cAtOk;
    }

static eAtRet ClaHbceLookupCollision0HwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;

    if (UseModuleDefault())
        return cAtOk;

    ThaDeviceMemoryLongFlush(device, 0x680000, 0x681FFF);

    return cAtOk;
    }

static eAtRet ClaHbceLookupCollision1HwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;

    if (UseModuleDefault())
        return cAtOk;

    ThaDeviceMemoryLongFlush(device, 0x690000, 0x691FFF);

    return cAtOk;
    }

static eAtRet ClaHbceLookupCollision2HwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;

    if (UseModuleDefault())
        return cAtOk;

    ThaDeviceMemoryLongFlush(device, 0x6A0000, 0x6A1FFF);

    return cAtOk;
    }

static eAtRet ClaHbceLookupCollision3HwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;

    if (UseModuleDefault())
        return cAtOk;

    ThaDeviceMemoryLongFlush(device, 0x6B0000, 0x6B1FFF);

    return cAtOk;
    }

static eAtRet ClaHwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;

    if (!UseModuleDefault())
        ThaDeviceMemoryFlush(device, 0x670000, 0x6707FF, 0);

    ThaDeviceMemoryFlush(device, 0x650000, 0x650FFF, 0);
    ThaDeviceMemoryFlush(device, 0x6D0000, 0x6D0000, 0);
    ThaDeviceMemoryFlush(device, 0x6C0000, 0x6C05FF, 0);
    ThaDeviceMemoryFlush(device, 0x60D000, 0x60DFFF, 0);

    if (Tha602100xxHwAccessOptimized())
        ThaDeviceMemoryFlush(device, 0x604000, 0x6047FF, 0);
    else
        {
        ThaDeviceMemoryLongFlush(device, 0x608000, 0x6085FF);
        ThaDeviceMemoryLongFlush(device, 0x604000, 0x6047FF);
        }

    return cAtOk;
    }

static eAtRet BerHwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;

    ThaDeviceMemoryLongFlush(device, 0x262000, 0x26217F);
    ThaDeviceMemoryLongFlush(device, 0x262200, 0x26237F);
    ThaDeviceMemoryLongFlush(device, 0x262400, 0x26257F);

    return cAtOk;
    }

static eAtRet PohHwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;
    /* POH */
    ThaDeviceMemoryFlush(device, 0x240400, 0x24042f, 0x0);
    ThaDeviceMemoryFlush(device, 0x240430, 0x24045f, 0x0);

    ThaDeviceMemoryFlush(device, 0x244000, 0x24453F, 0x0);
    ThaDeviceMemoryFlush(device, 0x244540, 0x244A7F, 0x0);
    ThaDeviceMemoryFlush(device, 0x228000, 0x228A7F, 0x0);
    ThaDeviceMemoryFlush(device, 0x22A000, 0x22A17F, 0x0);

    return cAtOk;
    }

static eAtRet VcatHwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;
    /* LCAS/VCAT */
    ThaDeviceMemoryFlush(device, 0x1d21200, 0x1d2122f, 0x0);
    ThaDeviceMemoryFlush(device, 0x1d21400, 0x1d2142F, 0x0);
    ThaDeviceMemoryFlush(device, 0x1d22000, 0x1d225ff, 0x0);
    ThaDeviceMemoryFlush(device, 0x1d24000, 0x1d245ff, 0x0);
    ThaDeviceMemoryFlush(device, 0x1d40800, 0x1d4081a, 0x0);
    ThaDeviceMemoryFlush(device, 0x1d40840, 0x1d4085a, 0x0);
    ThaDeviceMemoryFlush(device, 0x1d40900, 0x1d4091a, 0x0);
    ThaDeviceMemoryFlush(device, 0x1d40940, 0x1d4095a, 0x0);
    ThaDeviceMemoryFlush(device, 0x1d40a00, 0x1d40a1a, 0x0);
    ThaDeviceMemoryFlush(device, 0x1d40a40, 0x1d40a5a, 0x0);
    ThaDeviceMemoryFlush(device, 0x1d40b00, 0x1d40b1a, 0x0);
    ThaDeviceMemoryFlush(device, 0x1d40b40, 0x1d40b5a, 0x0);
    ThaDeviceMemoryFlush(device, 0x1d49000, 0x1d495ff, 0x0);
    ThaDeviceMemoryFlush(device, 0x1d51000, 0x1d515ff, 0x0);

    return cAtOk;
    }

static eAtRet EthHwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;
    if (!Tha60210061EthPassThroughIsSupported((AtModuleEth)AtDeviceModuleGet((AtDevice)self, cAtModuleEth)))
        return cAtOk;

    /* Eth passthrough */
    ThaDeviceMemoryFlush(device, 0x40100, 0x4010F, 0x0);

    return cAtOk;
    }

static eAtRet DccHwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;
    if (!Tha60210061ModuleEncapDccIsSupported((AtModuleEncap)AtDeviceModuleGet((AtDevice)self, cAtModuleEncap)))
        return cAtOk;

    ThaDeviceMemoryFlush(device, 0x11, 0x11, 0x0);
    ThaDeviceMemoryFlush(device, 0x14, 0x16, 0x0);
    return cAtOk;
    }

static void HwFlush(Tha60150011Device self)
    {
    Tha60210011Device device = (Tha60210011Device)self;
    MapHoHwFlush(device);
    CdrHoHwFlush(device);
    Map1_1HwFlush1(device);
    Map1_1HwFlush2(device);
    Map1_2HwFlush1(device);
    Map1_2HwFlush2(device);
    Cdr1_1HwFlush(device);
    Cdr1_2HwFlush(device);
    Pdh1_1HwFlush(device);
    Pdh1_2HwFlush(device);
    mMethodsGet(device)->PweHwFlush(device);
    mMethodsGet(device)->PlaHwFlush(device);
    mMethodsGet(device)->PdaHwFlush(device);
    mMethodsGet(device)->OcnHwFlush(device);
    mMethodsGet(device)->ClaHwFlush(device);
    ClaHbceHashTableHwFlush(device);
    ClaHbceLookupCollision0HwFlush(device);
    ClaHbceLookupCollision1HwFlush(device);
    ClaHbceLookupCollision2HwFlush(device);
    ClaHbceLookupCollision3HwFlush(device);
    ClaHbceLookupExtra0_3HwFlush(device);
    ClaHbceLookupExtra4_7HwFlush(device);
    BerHwFlush(device);
    PohHwFlush(device);
    VcatHwFlush(device);
    EthHwFlush(device);
    DccHwFlush(device);
    }

static tAtAsyncOperationFunc AsyncFlushOpGet(Tha60210012Device self, uint32 state)
    {
    static tAtAsyncOperationFunc functions[] = {(tAtAsyncOperationFunc)MapHoHwFlush,
                                                (tAtAsyncOperationFunc)CdrHoHwFlush,
                                                (tAtAsyncOperationFunc)Map1_1HwFlush1,
                                                (tAtAsyncOperationFunc)Map1_1HwFlush2,
                                                (tAtAsyncOperationFunc)Map1_2HwFlush1,
                                                (tAtAsyncOperationFunc)Map1_2HwFlush2,
                                                (tAtAsyncOperationFunc)Cdr1_1HwFlush,
                                                (tAtAsyncOperationFunc)Cdr1_2HwFlush,
                                                (tAtAsyncOperationFunc)Pdh1_1HwFlush,
                                                (tAtAsyncOperationFunc)Pdh1_2HwFlush,
                                                (tAtAsyncOperationFunc)PweHwFlush,
                                                (tAtAsyncOperationFunc)PlaHwFlush,
                                                (tAtAsyncOperationFunc)PdaHwFlush,
                                                (tAtAsyncOperationFunc)OcnHwFlush,
                                                (tAtAsyncOperationFunc)ClaHwFlush,
                                                (tAtAsyncOperationFunc)ClaHbceHashTableHwFlush,
                                                (tAtAsyncOperationFunc)ClaHbceLookupCollision0HwFlush,
                                                (tAtAsyncOperationFunc)ClaHbceLookupCollision1HwFlush,
                                                (tAtAsyncOperationFunc)ClaHbceLookupCollision2HwFlush,
                                                (tAtAsyncOperationFunc)ClaHbceLookupCollision3HwFlush,
                                                (tAtAsyncOperationFunc)ClaHbceLookupExtra0_3HwFlush,
                                                (tAtAsyncOperationFunc)ClaHbceLookupExtra4_7HwFlush,
                                                (tAtAsyncOperationFunc)BerHwFlush,
                                                (tAtAsyncOperationFunc)PohHwFlush,
                                                (tAtAsyncOperationFunc)VcatHwFlush,
                                                (tAtAsyncOperationFunc)EthHwFlush,
                                                (tAtAsyncOperationFunc)DccHwFlush};

    AtUnused(self);
    if (state >= mCount(functions))
        return NULL;

    return functions[state];
    }

static uint32 StartVersionSupportEXaui(Tha60210012Device self)
    {
    AtUnused(self);
    return 0x0;
    }

static eAtRet EthPassthroughActive(Tha60210012Device self, eBool enable)
    {
    ThaModuleEth moduleEth = (ThaModuleEth)AtDeviceModuleGet((AtDevice)self, cAtModuleEth);
    if (Tha60210061EthPassthroughXfiIsSupported(moduleEth))
        {
        eAtRet ret = Tha60210061ModuleEthGeBypassBufferActivate(moduleEth, enable);
        ret |= Tha60210061ModuleEthCoreIsolationEnable(moduleEth, (!enable)); /* Isolate when de-activate */
        }

    return cAtOk;
    }

static uint8 NumLoSlice(AtDevice self)
    {
    return (uint8)(Tha60210011DeviceNumUsedOc48Slices((Tha60210011Device)self) * 2);
    }

static AtEncapBinder EncapBinder(AtDevice self)
    {
    Tha60210012Device device = (Tha60210012Device)self;
    uint32 numChannelPerLoSlice = mMethodsGet(device)->NumChannelPerLoSlice(device);
    return Tha60210061EncapBinderNew(NumLoSlice(self), self, numChannelPerLoSlice);
    }

static uint32 StartVersionApplyNewReset(Tha60210051Device self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x8, 0x1025);
    }

static eAtRet EnableLosPins(AtDevice self)
    {
    AtHal hal = AtDeviceIpCoreHalGet(self, 0);
    uint32 regVal = AtHalRead(hal, cLosPinsEnableReg);

    mRegFieldSet(regVal, cLosPinsEnable, 1);
    AtHalWrite(hal, cLosPinsEnableReg, regVal);
    return cAtOk;
    }

static eAtRet Init(AtDevice self)
    {
    eAtRet ret;

    DiagnosticModeDisable(self);
    ret = m_AtDeviceMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return EnableLosPins(self);
    }

static eAtRet SuperAsyncInit(AtObject self)
    {
    return m_AtDeviceMethods->AsyncInit((AtDevice)self);
    }

static tAtAsyncOperationFunc NextInitOperation(AtDevice self, uint32 state)
    {
    static tAtAsyncOperationFunc m_AsyncOperations[] = {(tAtAsyncOperationFunc)DiagnosticModeDisable,
                                                        (tAtAsyncOperationFunc)SuperAsyncInit,
                                                        (tAtAsyncOperationFunc)EnableLosPins};
    AtUnused(self);

    if (state >= mCount(m_AsyncOperations))
        return NULL;

    return m_AsyncOperations[state];
    }

static eAtRet AsyncInit(AtDevice self)
    {
    return AtDeviceAsyncProcess(self, &(mThis(self)->asyncInitState), NextInitOperation);
    }

static void ClockStatusDisplay(Tha60150011Device self)
    {
    AtUnused(self);
    }

static eBool ErrorGeneratorIsSupported(Tha60210051Device self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool ClockStateValueV2IsSupported(ThaDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeNone(asyncInitState);
    }

static void OverrideAtObject(AtDevice self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(m_AtDeviceOverride));

        mMethodOverride(m_AtDeviceOverride, SerdesManagerObjectCreate);
        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, Init);
        mMethodOverride(m_AtDeviceOverride, NumSemControllersGet);
        mMethodOverride(m_AtDeviceOverride, SemControllerObjectCreate);
        mMethodOverride(m_AtDeviceOverride, EncapBinder);
        mMethodOverride(m_AtDeviceOverride, Init);
        mMethodOverride(m_AtDeviceOverride, AsyncInit);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static void OverrideThaDevice(AtDevice self)
    {
    ThaDevice device = (ThaDevice)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDeviceOverride, mMethodsGet(device), sizeof(m_ThaDeviceOverride));

        mMethodOverride(m_ThaDeviceOverride, IntrControllerCreate);
        mMethodOverride(m_ThaDeviceOverride, ClockStateValueV2IsSupported);
        }

    mMethodsSet(device, &m_ThaDeviceOverride);
    }

static void OverrideTha60210011Device(AtDevice self)
    {
    Tha60210011Device device = (Tha60210011Device)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011DeviceOverride, mMethodsGet(device), sizeof(m_Tha60210011DeviceOverride));

        mMethodOverride(m_Tha60210011DeviceOverride, XfiDiagPerGroupIsSupported);
        mMethodOverride(m_Tha60210011DeviceOverride, HwDiagnosticModeEnable);
        mMethodOverride(m_Tha60210011DeviceOverride, NumUsedOc48Slice);
        mMethodOverride(m_Tha60210011DeviceOverride, DdrCalibTimeoutMs);
        mMethodOverride(m_Tha60210011DeviceOverride, PweHwFlush);
        mMethodOverride(m_Tha60210011DeviceOverride, ClaHwFlush);
        mMethodOverride(m_Tha60210011DeviceOverride, PlaHwFlush);
        mMethodOverride(m_Tha60210011DeviceOverride, PdaHwFlush);
        mMethodOverride(m_Tha60210011DeviceOverride, OcnHwFlush);
        }

    mMethodsSet(device, &m_Tha60210011DeviceOverride);
    }

static void OverrideTha60210051Device(AtDevice self)
    {
    Tha60210051Device device = (Tha60210051Device)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210051DeviceOverride, mMethodsGet(device), sizeof(m_Tha60210051DeviceOverride));

        mMethodOverride(m_Tha60210051DeviceOverride, StartVersionSupportPowerControl);
        mMethodOverride(m_Tha60210051DeviceOverride, StartVersionApplyNewReset);
        mMethodOverride(m_Tha60210051DeviceOverride, ErrorGeneratorIsSupported);
        }

    mMethodsSet(device, &m_Tha60210051DeviceOverride);
    }

static void OverrideTha60150011Device(AtDevice self)
    {
    Tha60150011Device device = (Tha60150011Device)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60150011DeviceMethods = mMethodsGet(device);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60150011DeviceOverride, m_Tha60150011DeviceMethods, sizeof(m_Tha60150011DeviceOverride));

        mMethodOverride(m_Tha60150011DeviceOverride, PllIsLocked);
        mMethodOverride(m_Tha60150011DeviceOverride, QdrDiagnostic);
        mMethodOverride(m_Tha60150011DeviceOverride, DdrCalibIsGood);
        mMethodOverride(m_Tha60150011DeviceOverride, SemUartIsSupported);
        mMethodOverride(m_Tha60150011DeviceOverride, ClockStatusDisplay);
        mMethodOverride(m_Tha60150011DeviceOverride, HwFlush);
        }

    mMethodsSet(device, &m_Tha60150011DeviceOverride);
    }

static void OverrideTha60210012Device(AtDevice self)
    {
    Tha60210012Device device = (Tha60210012Device)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210012DeviceMethods = mMethodsGet(device);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210012DeviceOverride, m_Tha60210012DeviceMethods, sizeof(m_Tha60210012DeviceOverride));

        mMethodOverride(m_Tha60210012DeviceOverride, AsyncFlushOpGet);
        mMethodOverride(m_Tha60210012DeviceOverride, StartVersionSupportEXaui);
        mMethodOverride(m_Tha60210012DeviceOverride, EthPassthroughActive);
        }

    mMethodsSet(device, &m_Tha60210012DeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtObject(self);
    OverrideAtDevice(self);
    OverrideThaDevice(self);
    OverrideTha60210011Device(self);
    OverrideTha60210051Device(self);
    OverrideTha60150011Device(self);
    OverrideTha60210012Device(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061Device);
    }

static AtDevice ObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012DeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha60210061DeviceNew(AtDriver driver, uint32 productCode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDevice == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newDevice, driver, productCode);
    }

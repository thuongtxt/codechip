/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device
 *
 * File        : Tha60210061InterruptController.c
 *
 * Created Date: Oct 4, 2016
 *
 * Description : Interrupt controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pdh/ThaModulePdhInternal.h"
#include "../../Tha60210012/man/Tha60210012InterruptControllerInternal.h"
#include "../../Tha60210031/pdh/Tha60210031ModulePdhReg.h"
#include "Tha60210061InterruptController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061InterruptController
    {
    tTha60210012InterruptController super;
    } tTha60210061InterruptController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tThaIpCoreMethods m_ThaIpCoreOverride;

/* Reference to super implementation */
static const tThaIpCoreMethods *m_ThaIpCoreImplement = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool CanControlInterruptRestore(ThaIpCore self)
    {
    ThaVersionReader versionReader = ThaDeviceVersionReader(AtModuleDeviceGet((AtModule)self));
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    if (hwVersion >= ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x1, 0x1002))
        return cAtTrue;

    return cAtFalse;
    }

static eBool InterruptRestoreIsSupported(ThaIpCore self)
    {
    return CanControlInterruptRestore(self);
    }

static eBool SdhCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    if (m_ThaIpCoreImplement->SdhCauseInterrupt(self, intrStatus))
        return cAtTrue;

    return (intrStatus & cAf6_global_interrupt_EC1IntEnable_Mask) ? cAtTrue : cAtFalse;
    }

static void SdhHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    m_ThaIpCoreImplement->SdhHwInterruptEnable(self, enable);

    ThaIpCoreHwInterruptEnable(self, cAf6_global_interrupt_EC1IntEnable_Mask, enable);
    }

static void OverrideThaIpCore(ThaIntrController self)
    {
    ThaIpCore core = (ThaIpCore)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaIpCoreImplement = mMethodsGet(core);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaIpCoreOverride, m_ThaIpCoreImplement, sizeof(m_ThaIpCoreOverride));

        mMethodOverride(m_ThaIpCoreOverride, InterruptRestoreIsSupported);
        mMethodOverride(m_ThaIpCoreOverride, SdhCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, SdhHwInterruptEnable);
        }

    mMethodsSet(core, &m_ThaIpCoreOverride);
    }

static void Override(ThaIntrController self)
    {
    OverrideThaIpCore(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061InterruptController);
    }

static ThaIntrController ObjectInit(ThaIntrController self, AtIpCore ipCore)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012IntrControllerObjectInit(self, ipCore) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaIntrController Tha60210061IntrControllerNew(AtIpCore core)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaIntrController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, core);
    }

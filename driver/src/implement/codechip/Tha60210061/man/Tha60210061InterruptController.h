/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device
 * 
 * File        : Tha60210061InterruptController.h
 * 
 * Created Date: Oct 4, 2016
 *
 * Description : Interrupt controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061INTERRUPTCONTROLLER_H_
#define _THA60210061INTERRUPTCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define cAf6_global_interrupt_TFI5IntEnable_Mask    cBit5
#define cAf6_global_interrupt_EC1IntEnable_Mask     cBit13

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaIntrController Tha60210061IntrControllerNew(AtIpCore core);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061INTERRUPTCONTROLLER_H_ */


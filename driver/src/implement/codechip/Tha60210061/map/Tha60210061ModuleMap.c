/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAP
 *
 * File        : Tha60210061ModuleMap.c
 *
 * Created Date: Jun 20, 2016
 *
 * Description : 60210061 module MAP
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210012/map/Tha60210012ModuleMapInternal.h"
#include "../../Tha60210011/map/Tha60210011ModuleMapLoReg.h"
#include "Tha60210061ModuleMap.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061ModuleMap
    {
    tTha60210012ModuleMap super;
    }tTha60210061ModuleMap;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleAbstractMapMethods m_ThaModuleAbstractMapOverride;
static tTha60210011ModuleMapMethods m_ThaTha60210011ModuleMapOverride;

/* To save super implementation */
static const tThaModuleAbstractMapMethods *m_ThaModuleAbstractMapMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet De1FrameModeSet(ThaModuleAbstractMap self, AtPdhDe1 de1, uint16 frameType)
    {
    AtPdhChannel pdhChannel = (AtPdhChannel)de1;
    if (AtPdhChannelVcInternalGet(pdhChannel) || AtPdhChannelParentChannelGet(pdhChannel))
        return m_ThaModuleAbstractMapMethods->De1FrameModeSet(self, de1, frameType);

    /* TODO: configure frame mode in case of LIU de1 */
    return cAtOk;
    }

static uint8 MaxNumStsInLoSlice(Tha60210011ModuleMap self)
    {
    AtUnused(self);
    return 27;
    }

static uint32 MapLineControlOffset(ThaModuleMap self, uint8 slice, uint8 hwSts, uint8 vtgId, uint8 vtId)
    {
    return (uint32)(hwSts << 5) + (uint32)(vtgId << 2) + vtId  + Tha60210011ModuleMapDemapLoSliceOffset((ThaModuleAbstractMap)self, slice);
    }

static void MapLineSigTypeDefaultSet(ThaModuleMap self, uint8 slice, uint8 hwSts, uint8 vtgId, uint8 vtId)
    {
    uint32 offset = MapLineControlOffset(self, slice, hwSts, vtgId, vtId);
    uint32 address = cAf6Reg_lomap_line_ctrl_Base + offset;
    uint32 regVal  = mModuleHwRead(self, address);

    /* slice 0: DS1,
     * slice 1: E1 */
    mRegFieldSet(regVal, cAf6_lomap_line_ctrl_MapSigType_, slice);
    mModuleHwWrite(self, address, regVal);
    }

static void OverrideTha60210011ModuleMap(AtModule self)
    {
    Tha60210011ModuleMap mapModule = (Tha60210011ModuleMap)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaTha60210011ModuleMapOverride, mMethodsGet(mapModule), sizeof(m_ThaTha60210011ModuleMapOverride));

        mMethodOverride(m_ThaTha60210011ModuleMapOverride, MaxNumStsInLoSlice);
        }

    mMethodsSet(mapModule, &m_ThaTha60210011ModuleMapOverride);
    }

static void OverrideThaModuleAbstractMap(AtModule self)
    {
    ThaModuleAbstractMap mapModule = (ThaModuleAbstractMap)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleAbstractMapMethods = mMethodsGet(mapModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleAbstractMapOverride, m_ThaModuleAbstractMapMethods, sizeof(m_ThaModuleAbstractMapOverride));

        mMethodOverride(m_ThaModuleAbstractMapOverride, De1FrameModeSet);
        }

    mMethodsSet(mapModule, &m_ThaModuleAbstractMapOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleAbstractMap(self);
    OverrideTha60210011ModuleMap(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061ModuleMap);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012ModuleMapObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210061ModuleMapNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

void Tha60210061ModuleMapLineControlSigTypeDefault(ThaModuleMap self)
    {
    uint8 vtg_i, vtId;
    const uint8 cMaxVtgNum = 7;
    const uint8 cMaxVtNum  = 4;
    static const uint8 cPdhDe1HwSts = 26;

    /* STS 26 is used for DE1 */
    for (vtg_i = 0; vtg_i < cMaxVtgNum; vtg_i++)
        {
        for (vtId = 0; vtId < cMaxVtNum; vtId++)
            {
            MapLineSigTypeDefaultSet(self, 0, cPdhDe1HwSts, vtg_i, vtId);

            if (vtId < 3) /* E1 mode has only 3 VT */
                MapLineSigTypeDefaultSet(self, 1, cPdhDe1HwSts, vtg_i, vtId);
            }
        }
    }

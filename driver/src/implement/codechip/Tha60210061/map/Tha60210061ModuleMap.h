/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAP
 * 
 * File        : Tha60210061ModuleMap.h
 * 
 * Created Date: Sep 21, 2016
 *
 * Description : Map
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061MODULEMAP_H_
#define _THA60210061MODULEMAP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/map/ThaModuleMap.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void Tha60210061ModuleMapLineControlSigTypeDefault(ThaModuleMap self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061MODULEMAP_H_ */


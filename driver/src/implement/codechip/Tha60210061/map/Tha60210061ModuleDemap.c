/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Demap
 *
 * File        : Tha60210061ModuleDeDemap.c
 *
 * Created Date: Nov 18, 2016
 *
 * Description : 60210061 module Demap
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210012/map/Tha60210012ModuleMapInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cRegAddressDemapVc3StuffEnableOffset 0x4301

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061ModuleDemap
    {
    tTha60210012ModuleDemap super;
    }tTha60210061ModuleDemap;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleDemapMethods       m_ThaModuleDemapOverride;

/* To save super implementation */
static const tThaModuleDemapMethods       * m_ThaModuleDemapMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 De3LiuOverCepDemapCtrl(ThaModuleDemap self)
    {
    AtUnused(self);
    return cRegAddressDemapVc3StuffEnableOffset;
    }

static void OverrideThaModuleDemap(AtModule self)
    {
    ThaModuleDemap mapModule = (ThaModuleDemap)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleDemapMethods = mMethodsGet(mapModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleDemapOverride, m_ThaModuleDemapMethods, sizeof(m_ThaModuleDemapOverride));

        /* Override bit fields */
        mMethodOverride(m_ThaModuleDemapOverride, De3LiuOverCepDemapCtrl);
        }

    mMethodsSet(mapModule, &m_ThaModuleDemapOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleDemap(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061ModuleDemap);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012ModuleDemapObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210061ModuleDemapNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

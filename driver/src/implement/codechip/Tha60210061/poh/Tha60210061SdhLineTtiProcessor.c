/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited 
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : Tha60210061SdhLineTtiProcessor.c
 *
 * Created Date: Aug 26, 2016 
 *
 * Description :
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210031/poh/Tha60210031PohProcessorInternal.h"
#include "../../Tha60210011/ocn/Tha60210011ModuleOcn.h"
#include "../../Tha60210011/poh/Tha60210011ModulePoh.h"
#include "../ocn/Tha60210061ModuleOcnReg.h"
#include "../ocn/Tha60210061ModuleOcn.h"
#include "../sdh/Tha60210061ModuleSdh.h"
#include "Tha60210061PohProcessor.h"

/*--------------------------- Define -----------------------------------------*/
#define cSlideId(isEc1)	((isEc1 == 1) ? 3 : 2)
#define cAf6Reg_EC1_tfmj0insctl_Base 0x0180

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061SdhLineTtiProcessor
    {
    tTha60210031SdhLineTtiProcessor super;
    }tTha60210061SdhLineTtiProcessor;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaSdhLineTtiProcessorV2Methods m_ThaSdhLineTtiProcessorV2Override;
static tTha60210031SdhLineTtiProcessorMethods m_Tha60210031SdhLineTtiProcessorOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleSdh ModuleSdh(ThaTtiProcessor self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)ThaTtiProcessorChannelGet((ThaTtiProcessor)self));
    return (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    }

static ThaModuleOcn ModuleOcn(ThaTtiProcessor self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)ThaTtiProcessorChannelGet((ThaTtiProcessor)self));
    return (ThaModuleOcn)AtDeviceModuleGet(device, cThaModuleOcn);
    }

static ThaModulePoh ModulePoh(ThaTtiProcessor self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)ThaTtiProcessorChannelGet((ThaTtiProcessor)self));
    return (ThaModulePoh)AtDeviceModuleGet(device, cThaModulePoh);
    }

static uint32 OcnJ0TxMsgBufDefaultOffset(ThaSdhLineTtiProcessorV2 self, uint8 bufId)
    {
    AtSdhChannel channel = ThaTtiProcessorChannelGet((ThaTtiProcessor)self);
    uint32 lineId = AtChannelIdGet((AtChannel)channel);
    AtModuleSdh moduleSdh = ModuleSdh((ThaTtiProcessor)self);

    if (Tha60210061ModuleSdhChannelIsInEc1Line(moduleSdh, channel))
        {
        lineId = lineId - AtModuleSdhStartEc1LineIdGet(moduleSdh);
        return lineId * 16UL + bufId + Tha60210061ModuleOcnEc1BaseAddress(ModuleOcn((ThaTtiProcessor)self));
        }

    return lineId * 256UL + bufId + Tha60210011ModuleOcnBaseAddress(ModuleOcn((ThaTtiProcessor)self));
    }

static uint32 TxJ0MessageBufferRegAddress(ThaSdhLineTtiProcessorV2 self)
    {
    AtSdhChannel channel = ThaTtiProcessorChannelGet((ThaTtiProcessor)self);
    if (Tha60210061ModuleSdhChannelIsInEc1Line(ModuleSdh((ThaTtiProcessor)self), channel))
        return cAf6Reg_EC1_tfmj0insctl_Base;

    return cAf6Reg_tfmj0insctl;
    }

static uint32 PohBufferOffset(Tha60210031SdhLineTtiProcessor self)
    {
    uint8 hwStsInSlice, slice;
    ThaTtiProcessor processor = (ThaTtiProcessor)self;
    AtSdhChannel line = ThaTtiProcessorChannelGet(processor);

    if (ThaSdhLineHwIdGet((AtSdhLine)line, cThaModulePoh, &slice, &hwStsInSlice) == cAtOk)
        return slice * 384UL + (hwStsInSlice) * 8UL + Tha60210011ModulePohBaseAddress(ModulePoh(processor));

    return 0;
    }

static uint32 PohCpeOffset(Tha60210031SdhLineTtiProcessor self)
    {
    uint8 hwStsInSlice, slice;
    ThaTtiProcessor processor = (ThaTtiProcessor) self;
    AtSdhChannel line = ThaTtiProcessorChannelGet(processor);

    if (ThaSdhLineHwIdGet((AtSdhLine)line, cThaModulePoh, &slice, &hwStsInSlice) == cAtOk)
        return (slice * 96UL) + (hwStsInSlice * 2UL) + Tha60210011ModulePohBaseAddress(ModulePoh(processor));

    return 0;
    }
    
static uint32 PohGrabberOffset(Tha60210031SdhLineTtiProcessor self)
    {
    uint8 hwStsInSlice, slice;
    ThaTtiProcessor processor = (ThaTtiProcessor) self;
    AtSdhChannel line = ThaTtiProcessorChannelGet(processor);

    if (ThaSdhLineHwIdGet((AtSdhLine)line, cThaModulePoh, &slice, &hwStsInSlice) == cAtOk)
        return hwStsInSlice * 8UL + slice + Tha60210011ModulePohBaseAddress(ModulePoh(processor));

    return 0;
    }

static uint32 PohInterruptRegOffset(Tha60210031SdhLineTtiProcessor self)
    {
    uint8 hwStsInSlice, slice;
    ThaTtiProcessor poh = (ThaTtiProcessor)self;
    AtSdhChannel channel = ThaTtiProcessorChannelGet(poh);

    if (ThaSdhLineHwIdGet((AtSdhLine)channel, cThaModulePoh, &slice, &hwStsInSlice) == cAtOk)
        {
        uint8 slice24, sts24;
        ThaModuleSdhHwSts48ToHwSts24Get(0, hwStsInSlice, &slice24, &sts24); /* TODO: why it is 0? */
        return sts24 + slice24 * 128UL + Tha60210011ModulePohBaseAddress(ModulePoh(poh));
        }

    return 0;
    }

static uint32 PohJ0StatusRegAddress(Tha60210031SdhLineTtiProcessor self)
    {
    AtUnused(self);
    return 0x2C600;
    }

static uint32 PohJ0StatusOffset(Tha60210031SdhLineTtiProcessor self)
    {
    ThaTtiProcessor processor = (ThaTtiProcessor) self;
    uint8 slice, hwStsInSlice;
    AtSdhChannel channel = ThaTtiProcessorChannelGet(processor);

    if (ThaSdhLineHwIdGet((AtSdhLine)channel, cThaModulePoh, &slice, &hwStsInSlice) == cAtOk)
        return hwStsInSlice + slice * 48U + Tha60210011ModulePohBaseAddress(ModulePoh(processor));

    return 0;
    }

static void OverrideThaSdhLineTtiProcessorV2(ThaTtiProcessor self)
    {
    ThaSdhLineTtiProcessorV2 processor = (ThaSdhLineTtiProcessorV2)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhLineTtiProcessorV2Override, mMethodsGet(processor), sizeof(m_ThaSdhLineTtiProcessorV2Override));

        mMethodOverride(m_ThaSdhLineTtiProcessorV2Override, OcnJ0TxMsgBufDefaultOffset);
        mMethodOverride(m_ThaSdhLineTtiProcessorV2Override, TxJ0MessageBufferRegAddress);
        }

    mMethodsSet(processor, &m_ThaSdhLineTtiProcessorV2Override);
    }

static void OverrideTha60210031SdhLineTtiProcessor(ThaTtiProcessor self)
    {
    Tha60210031SdhLineTtiProcessor processor = (Tha60210031SdhLineTtiProcessor)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210031SdhLineTtiProcessorOverride, mMethodsGet(processor), sizeof(m_Tha60210031SdhLineTtiProcessorOverride));

        mMethodOverride(m_Tha60210031SdhLineTtiProcessorOverride, PohBufferOffset);
        mMethodOverride(m_Tha60210031SdhLineTtiProcessorOverride, PohCpeOffset);
        mMethodOverride(m_Tha60210031SdhLineTtiProcessorOverride, PohGrabberOffset);
        mMethodOverride(m_Tha60210031SdhLineTtiProcessorOverride, PohInterruptRegOffset);
        mMethodOverride(m_Tha60210031SdhLineTtiProcessorOverride, PohJ0StatusRegAddress);
        mMethodOverride(m_Tha60210031SdhLineTtiProcessorOverride, PohJ0StatusOffset);
        }

    mMethodsSet(processor, &m_Tha60210031SdhLineTtiProcessorOverride);
    }

static void Override(ThaTtiProcessor self)
    {
    OverrideThaSdhLineTtiProcessorV2(self);
    OverrideTha60210031SdhLineTtiProcessor(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061SdhLineTtiProcessor);
    }

static ThaTtiProcessor ObjectInit(ThaTtiProcessor self, AtSdhChannel sdhChannel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031SdhLineTtiProcessorObjectInit(self, sdhChannel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaTtiProcessor Tha60210061SdhLineTtiProcessorNew(AtSdhChannel sdhChannel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaTtiProcessor newProcessor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newProcessor == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newProcessor, sdhChannel);
    }

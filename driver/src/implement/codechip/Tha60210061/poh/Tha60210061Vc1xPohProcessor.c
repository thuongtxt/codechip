/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : Tha60210061Vc1xPohProcessor.c
 *
 * Created Date: Sep 5, 2016
 *
 * Description : VC1x POH processor
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210051/poh/Tha60210051PohProcessorInternal.h"
#include "../../Tha60210011/ocn/Tha60210011ModuleOcn.h"
#include "../ocn/Tha60210061ModuleOcnReg.h"
#include "../ocn/Tha60210061ModuleOcnEc1Reg.h"
#include "../sdh/Tha60210061ModuleSdh.h"
#include "Tha60210061PohProcessor.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061Vc1xPohProcessor
    {
    tTha60210051Vc1xPohProcessor super;
    }tTha60210061Vc1xPohProcessor;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60210011AuVcPohProcessorMethods  m_Tha60210011AuVcPohProcessorOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 Vc1xCurrentStatusReg(Tha60210011AuVcPohProcessor self)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)ThaPohProcessorVcGet((ThaPohProcessor)self);
    AtModuleSdh moduleSdh = (AtModuleSdh)AtChannelModuleGet((AtChannel)sdhChannel);

    if (Tha60210061ModuleSdhChannelIsInEc1Line(moduleSdh, sdhChannel))
        return cAf6_Ec1Reg_upvtchstaram_Base;

    return cAf6Reg_upvtchstaram;
    }

static eBool HasPayloadUneqDefect(Tha60210011AuVcPohProcessor self)
    {
    uint32 offset = Tha60210011ModuleOcnVtDefaultOffset((AtSdhChannel)ThaPohProcessorVcGet((ThaPohProcessor)self));
    uint32 regAddr = Vc1xCurrentStatusReg(self) + offset;
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);

    if (regVal & cAf6_upvtchstaram_VtPiStsCepUneqCurStatus_Mask)
        return cAtTrue;

    return cAtFalse;
    }

static uint32 Vc1xInterruptReg(Tha60210011AuVcPohProcessor self)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)ThaPohProcessorVcGet((ThaPohProcessor)self);
    AtModuleSdh moduleSdh   = (AtModuleSdh)AtChannelModuleGet((AtChannel)sdhChannel);

    if (Tha60210061ModuleSdhChannelIsInEc1Line(moduleSdh, sdhChannel))
        return cAf6_Ec1Reg_upvtchstkram_Base;

    return cAf6Reg_upvtchstkram;
    }

static eBool HasPayloadUneqSticky(Tha60210011AuVcPohProcessor self, eBool r2c)
    {
    eBool hasSticky = cAtFalse;
    uint32 offset = Tha60210011ModuleOcnVtDefaultOffset((AtSdhChannel)ThaPohProcessorVcGet((ThaPohProcessor)self));
    uint32 regAddr = Vc1xInterruptReg(self) + offset;
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);

    if (regVal & cAf6_upvtchstaram_VtPiStsCepUneqCurStatus_Mask)
        hasSticky = cAtTrue;

    if (r2c)
        mChannelHwWrite(self, regAddr, cAf6_upvtchstaram_VtPiStsCepUneqCurStatus_Mask, cThaModuleOcn);

    return hasSticky;
    }

static void OverrideTha60210011AuVcPohProcessor(ThaPohProcessor self)
    {
    Tha60210011AuVcPohProcessor path = (Tha60210011AuVcPohProcessor)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011AuVcPohProcessorOverride, mMethodsGet(path), sizeof(m_Tha60210011AuVcPohProcessorOverride));
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, HasPayloadUneqDefect);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, HasPayloadUneqSticky);
        }

    mMethodsSet(path, &m_Tha60210011AuVcPohProcessorOverride);
    }

static void Override(ThaPohProcessor self)
    {
    OverrideTha60210011AuVcPohProcessor(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061Vc1xPohProcessor);
    }

static ThaPohProcessor ObjectInit(ThaPohProcessor self, AtSdhVc vc)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051Vc1xPohProcessorObjectInit(self, vc) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPohProcessor Tha60210061Vc1xPohProcessorNew(AtSdhVc vc)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPohProcessor newProcessor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newProcessor, vc);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH
 * 
 * File        : Tha60210061ModulePoh.h
 * 
 * Created Date: Nov 4, 2016
 *
 * Description : 60210061 module POH interfaces
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061MODULEPOH_H_
#define _THA60210061MODULEPOH_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha60210061ModulePohLineHwIdGet(AtSdhLine line, uint32 *hwSlice, uint32 *hwStsId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061MODULEPOH_H_ */


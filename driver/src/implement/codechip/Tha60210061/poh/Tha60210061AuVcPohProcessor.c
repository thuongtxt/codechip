/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : Tha60210061AuVcPohProcessor.c
 *
 * Created Date: Aug 30, 2016
 *
 * Description : AU VC POH processor
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210012/poh/Tha60210012PohProcessorInternal.h"
#include "../../Tha60210011/ocn/Tha60210011ModuleOcn.h"
#include "../ocn/Tha60210061ModuleOcnReg.h"
#include "../ocn/Tha60210061ModuleOcnEc1Reg.h"
#include "../sdh/Tha60210061ModuleSdh.h"
#include "Tha60210061PohProcessor.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061AuVcPohProcessor
    {
    tTha60210012AuVcPohProcessor super;
    }tTha60210061AuVcPohProcessor;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60210011AuVcPohProcessorMethods m_Tha60210011AuVcPohProcessorOverride;

static const tTha60210011AuVcPohProcessorMethods * m_Tha60210011AuVcPohProcessorMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 PohCpeStatusRegAddr(Tha60210011AuVcPohProcessor self)
    {
    AtUnused(self);
    return 0x2C600;
    }

static uint32 StsCurrentStatusReg(Tha60210011AuVcPohProcessor self)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)ThaPohProcessorVcGet((ThaPohProcessor)self);
    AtModuleSdh moduleSdh = (AtModuleSdh)AtChannelModuleGet((AtChannel)sdhChannel);

    if (Tha60210061ModuleSdhChannelIsInEc1Line(moduleSdh, sdhChannel))
        return cAf6_Ec1Reg_upstschstaram_Base;

    return cAf6Reg_upstschstaram;
    }

static eBool HasPayloadUneqDefect(Tha60210011AuVcPohProcessor self)
    {
    uint32 offset = Tha60210011ModuleOcnStsDefaultOffset((AtSdhChannel)ThaPohProcessorVcGet((ThaPohProcessor)self));
    uint32 regAddr = StsCurrentStatusReg(self) + offset;
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);

    if (regVal & cAf6_upstschstaram_StsPiStsCepUneqPCurStatus_Mask)
        return cAtTrue;

    return cAtFalse;
    }

static uint32 StsInterruptReg(Tha60210011AuVcPohProcessor self)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)ThaPohProcessorVcGet((ThaPohProcessor)self);
    AtModuleSdh moduleSdh   = (AtModuleSdh)AtChannelModuleGet((AtChannel)sdhChannel);

    if (Tha60210061ModuleSdhChannelIsInEc1Line(moduleSdh, sdhChannel))
        return cAf6_Ec1Reg_upstschstkram_Base;

    return cAf6Reg_upstschstkram;
    }

static eBool HasPayloadUneqSticky(Tha60210011AuVcPohProcessor self, eBool r2c)
    {
    eBool hasSticky = cAtFalse;
    uint32 offset   = Tha60210011ModuleOcnStsDefaultOffset((AtSdhChannel)ThaPohProcessorVcGet((ThaPohProcessor)self));
    uint32 regAddr  = StsInterruptReg(self) + offset;
    uint32 regVal   = mChannelHwRead(self, regAddr, cThaModuleOcn);

    if (regVal & cAf6_upstschstaram_StsPiStsCepUneqPCurStatus_Mask)
        hasSticky = cAtTrue;

    if (r2c)
        mChannelHwWrite(self, regAddr, cAf6_upstschstaram_StsPiStsCepUneqPCurStatus_Mask, cThaModuleOcn);

    return hasSticky;
    }

static eAtRet Ec1TxUneqForce(Tha60210011AuVcPohProcessor self, eBool enable)
    {
    uint32 regAddr = cAf6_Ec1Reg_spgramctl_Base + Tha60210011ModuleOcnStsDefaultOffset((AtSdhChannel)ThaPohProcessorVcGet((ThaPohProcessor)self));
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);

    mRegFieldSet(regVal, cAf6_Ec1_spgramctl_StsPiUeqFrc_, mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eAtRet TxUneqForce(Tha60210011AuVcPohProcessor self, eBool enable)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)ThaPohProcessorVcGet((ThaPohProcessor)self);
    AtModuleSdh moduleSdh   = (AtModuleSdh)AtChannelModuleGet((AtChannel)sdhChannel);

    if (!Tha60210061ModuleSdhChannelIsInEc1Line(moduleSdh, sdhChannel))
        return m_Tha60210011AuVcPohProcessorMethods->TxUneqForce(self, enable);

    return Ec1TxUneqForce(self, enable);
    }

static eBool Ec1TxUneqIsForced(Tha60210011AuVcPohProcessor self)
    {
    uint32 regAddr = cAf6_Ec1Reg_spgramctl_Base + Tha60210011ModuleOcnStsDefaultOffset((AtSdhChannel)ThaPohProcessorVcGet((ThaPohProcessor)self));
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);

    return mBinToBool(mRegField(regVal, cAf6_Ec1_spgramctl_StsPiUeqFrc_));
    }

static eBool TxUneqIsForced(Tha60210011AuVcPohProcessor self)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)ThaPohProcessorVcGet((ThaPohProcessor)self);
    AtModuleSdh moduleSdh   = (AtModuleSdh)AtChannelModuleGet((AtChannel)sdhChannel);

    if (!Tha60210061ModuleSdhChannelIsInEc1Line(moduleSdh, sdhChannel))
        return m_Tha60210011AuVcPohProcessorMethods->TxUneqIsForced(self);

    return Ec1TxUneqIsForced(self);
    }

static void OverrideTha60210011AuVcPohProcessor(ThaPohProcessor self)
    {
    Tha60210011AuVcPohProcessor path = (Tha60210011AuVcPohProcessor)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011AuVcPohProcessorMethods = mMethodsGet(path);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011AuVcPohProcessorOverride, m_Tha60210011AuVcPohProcessorMethods, sizeof(m_Tha60210011AuVcPohProcessorOverride));

        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, PohCpeStatusRegAddr);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, HasPayloadUneqDefect);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, HasPayloadUneqSticky);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, TxUneqForce);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, TxUneqIsForced);
        }

    mMethodsSet(path, &m_Tha60210011AuVcPohProcessorOverride);
    }

static void Override(ThaPohProcessor self)
    {
    OverrideTha60210011AuVcPohProcessor(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061AuVcPohProcessor);
    }

static ThaPohProcessor ObjectInit(ThaPohProcessor self, AtSdhVc vc)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012AuVcPohProcessorObjectInit(self, vc) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPohProcessor Tha60210061AuVcPohProcessorNew(AtSdhVc vc)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPohProcessor newProcessor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newProcessor, vc);
    }

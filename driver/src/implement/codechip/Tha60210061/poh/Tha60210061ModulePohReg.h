/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH
 * 
 * File        : Tha60210061ModulePohReg.h
 * 
 * Created Date: Sep 1, 2016
 *
 * Description : POH registers
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061MODULEPOHREG_H_
#define _THA60210061MODULEPOHREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#define cAf6RegLinePohEnableBase       0x10002

#define cAf6HoStmPointerPohEnableMask  cBit16
#define cAf6HoStmPointerPohEnableShift 16
#define cAf6HoEc1PointerPohEnableMask  cBit17
#define cAf6HoEc1PointerPohEnableShift 17
#define cAf6HoStmLinePohEnableMask     cBit18
#define cAf6HoStmLinePohEnableShift    18
#define cAf6HoEc1LinePohEnableMask     cBit19
#define cAf6HoEc1LinePohEnableShift    19

#define cAf6LoStmPointerPohEnableMask  cBit24
#define cAf6LoStmPointerPohEnableShift 24
#define cAf6LoEc1PointerPohEnableMask  cBit25
#define cAf6LoEc1PointerPohEnableShift 25

/*------------------------------------------------------------------------------
Reg Name   : POH Global Control
Reg Addr   : 0x00_0000
Reg Formula:
    Where  :
Reg Desc   :
This register is used to control Parity , Message Jn request.

------------------------------------------------------------------------------*/
#define cAf6Reg_pcfg_glbctr_Base                                                                      0x000000

/*--------------------------------------
BitField Name: terjnreqslen
BitField Type: RW
BitField Desc: Enable JN Ter request for 8 Line
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_pcfg_glbctr_terjnreqslen_Mask                                                           cBit31_24
#define cAf6_pcfg_glbctr_terjnreqslen_Shift                                                                 24

/*--------------------------------------
BitField Name: cpejnreqslen
BitField Type: RW
BitField Desc: Enable JN CPE request for 8 Line
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_pcfg_glbctr_cpejnreqslen_Mask                                                           cBit23_16
#define cAf6_pcfg_glbctr_cpejnreqslen_Shift                                                                 16

/*--------------------------------------
BitField Name: cpejnreqstsen
BitField Type: RW
BitField Desc: Enable JN CPE request for 3 group STS, 16 each
BitField Bits: [14:12]
--------------------------------------*/
#define cAf6_pcfg_glbctr_cpejnreqstsen_Mask                                                          cBit14_12
#define cAf6_pcfg_glbctr_cpejnreqstsen_Shift                                                                12

/*--------------------------------------
BitField Name: jnbdebound
BitField Type: RW
BitField Desc: Debound Threshold for Jn 1byte
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_pcfg_glbctr_jnbdebound_Mask                                                               cBit3_0
#define cAf6_pcfg_glbctr_jnbdebound_Shift                                                                    0

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061MODULEPOHREG_H_ */


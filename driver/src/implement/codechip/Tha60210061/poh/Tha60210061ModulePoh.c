/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : Tha60210061ModulePoh.c
 *
 * Created Date: Aug 16, 2016
 *
 * Description : Module POH concrete code
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210012/poh/Tha60210012ModulePohInternal.h"
#include "../../Tha60210031/poh/Tha60210031PohProcessorInternal.h"
#include "Tha60210061PohProcessor.h"
#include "Tha60210061ModulePoh.h"
#include "Tha60210061ModulePohReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061ModulePoh
    {
    tTha60210012ModulePoh super;
    }tTha60210061ModulePoh;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePohMethods         m_ThaModulePohOverride;
static tTha60210011ModulePohMethods m_Tha60210011ModulePohOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaTtiProcessor LineTtiProcessorCreate(ThaModulePoh self, AtSdhLine line)
    {
    AtUnused(self);
    return Tha60210061SdhLineTtiProcessorNew((AtSdhChannel)line);
    }

static ThaPohProcessor AuVcPohProcessorCreate(ThaModulePoh self, AtSdhVc vc)
    {
    AtUnused(self);
    return Tha60210061AuVcPohProcessorNew(vc);
    }

static ThaPohProcessor Vc1xPohProcessorCreate(ThaModulePoh self, AtSdhVc vc)
    {
    AtUnused(self);
    return Tha60210061Vc1xPohProcessorNew(vc);
    }

static ThaPohProcessor Tu3VcPohProcessorCreate(ThaModulePoh self, AtSdhVc vc)
    {
    AtUnused(self);
    return Tha60210061Tu3VcPohProcessorNew(vc);
    }

static eBool PslTtiInterruptIsSupported(ThaModulePoh self)
    {
    ThaVersionReader versionReader = ThaDeviceVersionReader(AtModuleDeviceGet((AtModule)self));
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    if (hwVersion >= ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x1, 0x1002))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet LineHwIdGet(AtSdhLine line, uint32 *hwSlice, uint32 *hwStsId)
    {
    eAtSdhLineRate rate = AtSdhLineRateGet(line);
    eBool isEc1Line = (rate == cAtSdhLineRateStm0);
    if (hwSlice)
        *hwSlice = isEc1Line ? 3 : 2;

    if (hwStsId)
        {
        *hwStsId = AtChannelIdGet((AtChannel)line);
        if (isEc1Line)
            {
            AtModuleSdh sdhModule = (AtModuleSdh) AtChannelModuleGet((AtChannel)line);
            *hwStsId = *hwStsId - AtModuleSdhStartEc1LineIdGet(sdhModule);
            }
        }

    return cAtOk;
    }

static uint32 PohMonEnableRegister(ThaModulePoh self, AtSdhLine line)
    {
    uint32 hwSlice = 0, hwStsId = 0;
    uint32 regAddrBase = Tha60210011ModulePohBaseAddress(self) + ThaModulePohCpestsctrBase(self);
    Tha60210061ModulePohLineHwIdGet(line, &hwSlice, &hwStsId);
    return (regAddrBase + (hwSlice * 96UL) + (hwStsId * 2UL));
    }

static eAtRet LinePohMonEnable(ThaModulePoh self, AtSdhLine line, eBool enabled)
    {
    uint32 regAddr = PohMonEnableRegister(self, line);
    uint32 regVal = mModuleHwRead(self, regAddr);
    mFieldIns(&regVal, cBit20, 20, mBinToBool(enabled));
    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eAtRet JnRequestDdrEnable(Tha60210011ModulePoh self, eBool enable)
    {
    uint32 address = cAf6Reg_pcfg_glbctr_Base + Tha60210011ModulePohBaseAddress((ThaModulePoh)self);
    uint32 enableVal = cAf6_pcfg_glbctr_terjnreqslen_Mask | cAf6_pcfg_glbctr_cpejnreqslen_Mask | cAf6_pcfg_glbctr_cpejnreqstsen_Mask | 0x5;

    mModuleHwWrite(self, address, enable ? enableVal : 0);
    return cAtOk;
    }

static void OverrideTha60210011ModulePoh(AtModule self)
    {
    Tha60210011ModulePoh pohModule = (Tha60210011ModulePoh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModulePohOverride, mMethodsGet(pohModule), sizeof(m_Tha60210011ModulePohOverride));

        mMethodOverride(m_Tha60210011ModulePohOverride, PslTtiInterruptIsSupported);
        mMethodOverride(m_Tha60210011ModulePohOverride, JnRequestDdrEnable);
        }

    mMethodsSet(pohModule, &m_Tha60210011ModulePohOverride);
    }

static void OverrideThaModulePoh(AtModule self)
    {
    ThaModulePoh pohModule = (ThaModulePoh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePohOverride, mMethodsGet(pohModule), sizeof(m_ThaModulePohOverride));

        mMethodOverride(m_ThaModulePohOverride, LineTtiProcessorCreate);
        mMethodOverride(m_ThaModulePohOverride, AuVcPohProcessorCreate);
        mMethodOverride(m_ThaModulePohOverride, Vc1xPohProcessorCreate);
        mMethodOverride(m_ThaModulePohOverride, Tu3VcPohProcessorCreate);
        mMethodOverride(m_ThaModulePohOverride, LinePohMonEnable);
        }

    mMethodsSet(pohModule, &m_ThaModulePohOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModulePoh(self);
    OverrideTha60210011ModulePoh(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061ModulePoh);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012ModulePohObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210061ModulePohNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

eAtRet Tha60210061ModulePohLineHwIdGet(AtSdhLine line, uint32 *hwSlice, uint32 *hwStsId)
    {
    if (line && hwSlice &&  hwStsId)
        return LineHwIdGet(line, hwSlice, hwStsId);
    return cAtErrorNullPointer;
    }

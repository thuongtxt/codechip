/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH
 * 
 * File        : Tha60210061PohProcessor.h
 * 
 * Created Date: Aug 30, 2016
 *
 * Description : 60210061 POH interfaces
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061POHPROCESSOR_H_
#define _THA60210061POHPROCESSOR_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPohProcessor Tha60210061AuVcPohProcessorNew(AtSdhVc vc);
ThaPohProcessor Tha60210061Vc1xPohProcessorNew(AtSdhVc vc);
ThaPohProcessor Tha60210061Tu3VcPohProcessorNew(AtSdhVc vc);
ThaTtiProcessor Tha60210061SdhLineTtiProcessorNew(AtSdhChannel sdhChannel);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061POHPROCESSOR_H_ */


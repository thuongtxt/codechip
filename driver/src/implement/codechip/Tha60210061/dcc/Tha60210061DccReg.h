/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Encap
 * 
 * File        : Tha60210061DccReg.h
 * 
 * Created Date: Jun 27, 2017
 *
 * Description : DCC control registers
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061DCCREG_H_
#define _THA60210061DCCREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*------------------------------------------------------------------------------
Reg Name   : GLB FPGA DCC Control1
Reg Addr   : 0x00_0016
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configure DCC channels

------------------------------------------------------------------------------*/
#define cAf6Reg_DCC_Control1_Base                                                                     0x000016

/*--------------------------------------
BitField Name: DCCMapStsID
BitField Type: RW
BitField Desc: STS ID carry 4 DCC channels (SDDC and LDCC is the same STS ID),
bit4_0 for SDCC0 and LDCC0
BitField Bits: [27:8]
--------------------------------------*/
#define cAf6_DCC_Control1_DCCMapStsID_Mask(lineId)                                  cBit12_8 << ((lineId) * 5)
#define cAf6_DCC_Control1_DCCMapStsID_Shift(lineId)                                         8 + ((lineId) * 5)

/*--------------------------------------
BitField Name: DCCEnable
BitField Type: RW
BitField Desc: Enable for 8 DCC channels, bit0 for SDCC0 and bit1 for LDCC0
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_DCC_Control1_DCCEnable_Mask(id)                                                     cBit0 << (id)
#define cAf6_DCC_Control1_DCCEnable_Shift(id)                                                             (id)


/*------------------------------------------------------------------------------
Reg Name   : GLB FPGA DCC Control2
Reg Addr   : 0x00_0015
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configure DCC channels

------------------------------------------------------------------------------*/
#define cAf6Reg_DCC_Control2_Base                                                                     0x000015

/*--------------------------------------
BitField Name: DCCMapSigType
BitField Type: RW
BitField Desc: Sig type the same MAP configuration, that carry 2 DCC channels
(SDDC and LDCC is the same), bit4_0 for SDCC0 and LDCC0
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_DCC_Control2_DCCMapSigType_Mask                                                         cBit31_16
#define cAf6_DCC_Control2_DCCMapSigType_Shift                                                               16

/*--------------------------------------
BitField Name: DCCMapFrmType
BitField Type: RW
BitField Desc: Frame type the same MAP configuration, that carry 2 DCC channels
(SDDC and LDCC is the same), bit1_0 for SDCC0 and LDCC0
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_DCC_Control2_DCCMapFrmType_Mask                                                          cBit15_8
#define cAf6_DCC_Control2_DCCMapFrmType_Shift                                                                8


/*------------------------------------------------------------------------------
Reg Name   : GLB FPGA DCC Control3
Reg Addr   : 0x00_0014
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configure DCC channels at side OCN -> ETH

------------------------------------------------------------------------------*/
#define cAf6Reg_DCC_Control3_Base                                                                     0x000014

/*--------------------------------------
BitField Name: DCCEnable
BitField Type: RW
BitField Desc: Enable for 8 DCC channels, bit0 for SDCC0 and bit1 for LDCC0
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_DCC_Control3_DCCEnable_Mask(id)                                                     cBit0 << (id)
#define cAf6_DCC_Control3_DCCEnable_Shift(id)                                                             (id)

/*------------------------------------------------------------------------------
Reg Name   : GLB FPGA DCC Control4
Reg Addr   : 0x00_0017 - 0x00_0019
Reg Formula: 0x00_0017 + DCCOffID
    Where  :
           + $DCCOffID(0-2): contain 8 DCC channels (88bits)
Reg Desc   :
This register is used to configure DCC channels

------------------------------------------------------------------------------*/
#define cAf6Reg_DCC_Control4_Base                                                                     0x000017

/*--------------------------------------
BitField Name: LDCC3ID
BitField Type: RW
BitField Desc: LDCC3 channel
BitField Bits: [87:77]
--------------------------------------*/
#define cAf6_DCC_Control4_LDCC3ID_Mask                                                               cBit23_13
#define cAf6_DCC_Control4_LDCC3ID_Shift                                                                     13

/*--------------------------------------
BitField Name: SDCC3ID
BitField Type: RW
BitField Desc: SDCC3 channel
BitField Bits: [76:66]
--------------------------------------*/
#define cAf6_DCC_Control4_SDCC3ID_Mask                                                                cBit12_2
#define cAf6_DCC_Control4_SDCC3ID_Shift                                                                      2

/*--------------------------------------
BitField Name: LDCC2ID
BitField Type: RW
BitField Desc: LDCC2 channel
BitField Bits: [65:55]
--------------------------------------*/
#define cAf6_DCC_Control4_LDCC2ID_Mask_01                                                            cBit31_23
#define cAf6_DCC_Control4_LDCC2ID_Shift_01                                                                  23
#define cAf6_DCC_Control4_LDCC2ID_Mask_02                                                              cBit1_0
#define cAf6_DCC_Control4_LDCC2ID_Shift_02                                                                   0

/*--------------------------------------
BitField Name: SDCC2ID
BitField Type: RW
BitField Desc: SDCC2 channel
BitField Bits: [54:44]
--------------------------------------*/
#define cAf6_DCC_Control4_SDCC2ID_Mask                                                               cBit22_12
#define cAf6_DCC_Control4_SDCC2ID_Shift                                                                     12

/*--------------------------------------
BitField Name: LDCC1ID
BitField Type: RW
BitField Desc: LDCC1 channel
BitField Bits: [43:33]
--------------------------------------*/
#define cAf6_DCC_Control4_LDCC1ID_Mask                                                                cBit11_1
#define cAf6_DCC_Control4_LDCC1ID_Shift                                                                      1

/*--------------------------------------
BitField Name: SDCC1ID
BitField Type: RW
BitField Desc: SDCC1 channel
BitField Bits: [32:22]
--------------------------------------*/
#define cAf6_DCC_Control4_SDCC1ID_Mask_01                                                            cBit31_22
#define cAf6_DCC_Control4_SDCC1ID_Shift_01                                                                  22
#define cAf6_DCC_Control4_SDCC1ID_Mask_02                                                                cBit0
#define cAf6_DCC_Control4_SDCC1ID_Shift_02                                                                   0

/*--------------------------------------
BitField Name: LDCC0ID
BitField Type: RW
BitField Desc: LDCC0 channel
BitField Bits: [21:11]
--------------------------------------*/
#define cAf6_DCC_Control4_LDCC0ID_Mask                                                               cBit21_11
#define cAf6_DCC_Control4_LDCC0ID_Shift                                                                     11

/*--------------------------------------
BitField Name: SDCC0ID
BitField Type: RW
BitField Desc: SDCC0 channel
BitField Bits: [10:0]
--------------------------------------*/
#define cAf6_DCC_Control4_SDCC0ID_Mask                                                                cBit10_0
#define cAf6_DCC_Control4_SDCC0ID_Shift                                                                      0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60210061DCCREG_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Encapsulation
 *
 * File        : Tha60210061DccHdlcLink.c
 *
 * Created Date: Feb 8, 2017
 *
 * Description : 60210061 dcc HDLC link
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/encap/dynamic/ThaModuleDynamicEncap.h"
#include "../../Tha60210012/encap/Tha60210012PhysicalCiscoHdlcLink.h"
#include "Tha60210061Dcc.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210061DccHdlcLink)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061DccHdlcLink * Tha60210061DccHdlcLink;

typedef struct tTha60210061DccHdlcLink
    {
    tTha60210012PhysicalCiscoHdlcLink super;
    uint32 networkProfileKey;
    }tTha60210061DccHdlcLink;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods  m_AtObjectOverride;
static tAtChannelMethods m_AtChannelOverride;

/* To save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static uint32 HwIdGet(AtChannel self)
    {
    const uint32 cStartDccChannelId = 1016;
    AtHdlcChannel hdlcChannel = AtHdlcLinkHdlcChannelGet((AtHdlcLink)self);
    return AtChannelIdGet((AtChannel)hdlcChannel) + cStartDccChannelId;
    }

static uint32 IdGet(AtChannel self)
    {
    return HwIdGet(self);
    }

static eAtRet CanBindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    AtUnused(self);
    AtUnused(pseudowire);
    return cAtErrorModeNotSupport;
    }

static AtPw BoundPseudowireGet(AtChannel self)
    {
    AtUnused(self);
    return NULL;
    }

static ThaProfile NetworkOamProfile(AtHdlcLink self)
    {
    ThaProfileManager manager      = ThaModuleEncapProfileManagerGet((AtModuleEncap)AtChannelModuleGet((AtChannel)self));
    ThaProfileFinder networkFinder = ThaProfileManagerNetworkProfileFinderGet(manager);

    ThaProfileFinderRuleSet(networkFinder, cThaProfileNetworkTypeAll, cThaProfileRuleControl);
    return ThaProfileFinderProfileForLinkFind(networkFinder, (AtHdlcLink)self);
    }

static ThaProfile NetworkProfileByKey(AtHdlcLink self)
    {
    ThaProfileManager manager      = ThaModuleEncapProfileManagerGet((AtModuleEncap)AtChannelModuleGet((AtChannel)self));
    ThaProfileFinder networkFinder = ThaProfileManagerNetworkProfileFinderGet(manager);

    ThaProfileFinderKeySet(networkFinder, mThis(self)->networkProfileKey);
    return ThaProfileFinderProfileForLinkFind(networkFinder, (AtHdlcLink)self);
    }

static eAtRet ProfileDefault(ThaPhysicalPppLink self)
    {
    ThaProfileManager manager      = ThaModuleEncapProfileManagerGet((AtModuleEncap)AtChannelModuleGet((AtChannel)self));
    ThaProfileFinder networkFinder = ThaProfileManagerNetworkProfileFinderGet(manager);

    ThaProfileFinderRuleSet(networkFinder, cThaProfileNetworkTypeAll, cThaProfileRuleData);
    ThaProfileFinderRuleSet(networkFinder, cThaProfileNetworkTypeCiscoDiscoveryProtocol | cThaProfileNetworkTypeOSINetwork, cThaProfileRuleControl);
    ThaPhysicalPppLinkNetworkProfileSet(self, ThaProfileFinderProfileForLinkFind(networkFinder, (AtHdlcLink)self));

    return cAtOk;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return ProfileDefault((ThaPhysicalPppLink)self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60210061DccHdlcLink object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeUInt(networkProfileKey);
    }

static void OverrideAtObject(AtPppLink self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtPppLink self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, HwIdGet);
        mMethodOverride(m_AtChannelOverride, CanBindToPseudowire);
        mMethodOverride(m_AtChannelOverride, BoundPseudowireGet);
        mMethodOverride(m_AtChannelOverride, IdGet);
        mMethodOverride(m_AtChannelOverride, Init);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061DccHdlcLink);
    }

static void Override(AtPppLink self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    }

static AtPppLink ObjectInit(AtPppLink self, AtHdlcChannel physicalChannel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012PhysicalCiscoHdlcLinkObjectInit(self, physicalChannel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPppLink Tha60210061DccHdlcLinkNew(AtHdlcChannel physicalChannel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPppLink newLink = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newLink == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newLink, physicalChannel);
    }

eAtRet Tha60210061DccHdlcLinkProfileUpdate(AtHdlcLink self, uint8 oldFrameType, uint8 frameType)
    {
    ThaPhysicalPppLink link = (ThaPhysicalPppLink)self;

    /* Only need to update link traffic profiles when switch to/from frame type LAPD */
    if (oldFrameType == cAtHdlcFrmLapd)
        return ThaPhysicalPppLinkNetworkProfileSet(link, NetworkProfileByKey(self));

    if (frameType == cAtHdlcFrmLapd)
        {
        ThaProfile networkProfile = ThaPhysicalPppLinkNetworkProfileGet(link);
        if (networkProfile)
            mThis(self)->networkProfileKey = ThaProfileKeyValueGet(networkProfile);

        return ThaPhysicalPppLinkNetworkProfileSet(link, NetworkOamProfile(self));
        }

    return cAtOk;
    }

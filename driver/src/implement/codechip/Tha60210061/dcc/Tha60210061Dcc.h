/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Encap
 * 
 * File        : Tha60210061Dcc.h
 * 
 * Created Date: Feb 8, 2017
 *
 * Description : Cisco-Hdlc Link
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061DCC_H_
#define _THA60210061DCC_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPppLink Tha60210061DccHdlcLinkNew(AtHdlcChannel physicalChannel);
eAtRet Tha60210061DccHdlcLinkProfileUpdate(AtHdlcLink self, uint8 oldFrameType, uint8 frameType);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061DCC_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Encap
 *
 * File        : Tha60210061Dcc.c
 *
 * Created Date: Feb 7, 2017
 *
 * Description : 60210061 DCC implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/man/ThaDevice.h"
#include "../../../default/pw/headercontrollers/ThaPwHeaderController.h"
#include "../../Tha60210012/encap/Tha60210012PhysicalHdlcChannelInternal.h"
#include "../../Tha60210012/pwe/Tha60210012ModulePwe.h"
#include "../../Tha60210012/pwe/Tha60210012ModulePweBlockManager.h"
#include "../../Tha60210011/pwe/Tha60210011ModulePwe.h"
#include "../../Tha60210012/common/Tha60210012Channel.h"
#include "../encap/Tha60210061ModuleEncap.h"
#include "../pwe/Tha60210061ModulePlaReg.h"
#include "../sdh/Tha60210061ModuleSdh.h"
#include "Tha60210061DccReg.h"
#include "Tha60210061Dcc.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210061Dcc)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061Dcc * Tha60210061Dcc;
typedef struct tTha60210061Dcc
    {
    tTha60210012PhysicalHdlcLoChannel super;
    AtSdhLine line;
    eAtSdhLineDccLayer layer;
    }tTha60210061Dcc;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods               m_AtObjectOverride;
static tAtChannelMethods              m_AtChannelOverride;
static tAtHdlcChannelMethods          m_AtHdlcChannelOverride;
static tThaHdlcChannelMethods         m_ThaHdlcChannelOverride;
static tAtEncapChannelMethods         m_AtEncapChannelOverride;
static tThaPhysicalHdlcChannelMethods m_ThaPhysicalHdlcChannelOverride;

/* Super implementation */
static const tAtObjectMethods * m_AtObjectMethods = NULL;
static const tAtChannelMethods * m_AtChannelMethods = NULL;
static const tAtHdlcChannelMethods * m_AtHdlcChannelMethods = NULL;
static const tThaPhysicalHdlcChannelMethods * m_ThaPhysicalHdlcChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 CircuitSliceGet(ThaHdlcChannel self)
    {
    /* HW use LO channels of slice 0 for DCC */
    AtUnused(self);
    return 0;
    }

static eBool CircuitIsHo(ThaHdlcChannel self)
    {
    /* HW use LO channels of slice 0 for DCC */
    AtUnused(self);
    return cAtFalse;
    }

static uint32 HwIdGet(AtChannel self)
    {
    return (uint32)(Tha60210061ModuleEncapStartChannelForDcc((AtModuleEncap)AtChannelModuleGet(self)) + AtChannelIdGet(self));
    }

static eAtRet LinkActivate(ThaPhysicalHdlcChannel self)
    {
    AtHdlcLink physicalLink = AtHdlcChannelHdlcLinkGet((AtHdlcChannel)self);
    eAtRet ret;

    /* Link has been activated, do nothing */
    if (physicalLink)
        return cAtOk;

    physicalLink = mMethodsGet(self)->LinkCreate(self);
    if (physicalLink == NULL)
        return cAtError;

    ret = AtChannelInit((AtChannel)physicalLink);
    if (ret != cAtOk)
        return ret;

    AtHdlcChannelHdlcLinkSet((AtHdlcChannel)self, physicalLink);
    return cAtOk;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    ret  = AtHdlcChannelFrameTypeSet((AtHdlcChannel)self, cAtHdlcFrmCiscoHdlc);
    ret |= LinkActivate((ThaPhysicalHdlcChannel)self);

    if (AtChannelHwResourceGet(self) == NULL)
        ret |= AtChannelHwResourceAllocate(self);

    return ret;
    }

static AtHdlcLink LinkCreate(ThaPhysicalHdlcChannel self)
    {
    return (AtHdlcLink)Tha60210061DccHdlcLinkNew((AtHdlcChannel)self);
    }

static Tha60210012ModulePwe ModulePwe(AtChannel self)
    {
    return (Tha60210012ModulePwe)AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModulePwe);
    }

static eAtRet HwResourceAllocate(AtChannel self)
    {
    AtHdlcLink physicalLink = AtHdlcChannelHdlcLinkGet((AtHdlcChannel)self);
    uint32 offset = AtChannelHwIdGet((AtChannel)physicalLink) + Tha60210012ModulePweIMsgLinkAddressOffset(ModulePwe(self));

    return Tha60210012ChannelHwLoBlockDynamicAllocate(self, offset);
    }

static void RegShow(AtChannel self, const char* regName, uint32 address)
    {
    ThaDeviceRegNameDisplay(regName);
    ThaDeviceChannelRegValueDisplay((AtChannel)self, address, cThaModulePwe);
    }

static eAtRet Debug(AtChannel self)
    {
    AtHdlcLink physicalLink = AtHdlcChannelHdlcLinkGet((AtHdlcChannel)self);
    uint32 offset = AtChannelHwIdGet((AtChannel)physicalLink) + Tha60210012ModulePweIMsgLinkAddressOffset(ModulePwe(self));
    offset = offset + Tha60210011ModulePlaBaseAddress();

    AtPrintc(cSevNormal, "\r\n-Block Allocation: \r\n");
    AtPrintc(cSevNormal, "\r\n-Hardware BlockId: %u\r\n", Tha60210012ChannelPlaBlockIdGet(self));
    RegShow(self, "Payload Assembler Block Buffer Control", cAf6Reg_pla_blk_buf_ctrl_Base + offset);
    return cAtOk;
    }

static eAtRet FrameTypeSet(AtHdlcChannel self, uint8 frameType)
    {
    AtPwHdlc pw;
    eAtRet ret;
    uint8 currentFrameType = AtHdlcChannelFrameTypeGet(self);

    if (currentFrameType == frameType)
        return cAtOk;

    pw = (AtPwHdlc)AtChannelBoundPwGet((AtChannel)self);
    ret = m_AtHdlcChannelMethods->FrameTypeSet(self, frameType);
    ret |= Tha60210012PhysicalHdlcChannelEncapTypeSet((Tha60210012PhysicalHdlcChannel)self, Tha60210012PhysicalHdlcChannelEncapType2Hw(self, frameType));
    if (ret != cAtOk)
        return ret;

    /* Need to update database frame type before reconfigure pw payload type as somewhere in pw payload type setting
     * frame type set may be called again */
    self->frameType = frameType;

    if (pw)
        AtPwHdlcPayloadTypeSet(pw, AtPwHdlcPayloadTypeGet(pw));

    return Tha60210061DccHdlcLinkProfileUpdate(AtHdlcChannelHdlcLinkGet(self), currentFrameType, frameType);
    }

static eAtRet HwResourceDeallocate(AtChannel self)
    {
    return Tha60210012ChannelBlockDeallocate(self);
    }

static void Delete(AtObject self)
    {
    AtChannelHwResourceDeallocate((AtChannel)self);
    AtChannelBoundPwObjectDelete((AtChannel)self);
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60210061Dcc object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);
    Tha60210012ChannelSerializeHwResource((AtChannel)self, encoder);

    mEncodeObjectDescription(line);
    mEncodeUInt(layer);
    }

static AtPw BoundPseudowireGet(AtChannel self)
    {
    return self->boundPw;
    }

static uint32 EncapHwIdAllocate(AtChannel self)
    {
    return AtChannelHwIdGet(self);
    }

static eAtRet ControlSizeSet(AtHdlcChannel self, uint8 controlSize)
    {
    AtUnused(self);
    return (controlSize == 1) ? cAtOk : cAtErrorModeNotSupport;
    }

static uint8 ControlSizeGet(AtHdlcChannel self)
    {
    AtUnused(self);
    return 1;
    }

static eAtRet TxAddressControlCompress(AtHdlcChannel self, eBool compress)
    {
    AtUnused(self);

    /* Compression is not supported so far */
    return (compress) ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool TxAddressControlIsCompressed(AtHdlcChannel self)
    {
    AtUnused(self);

    /* Compression is not supported so far */
    return cAtFalse;
    }

static AtChannel BoundPhyGet(AtEncapChannel self)
    {
    return (AtChannel)(mThis(self)->line);
    }

static eAtRet TxEncapConnectionEnable(AtChannel self, eBool enable)
    {
    uint32 regVal = mChannelHwRead(self, cAf6Reg_DCC_Control1_Base, cAtModuleEncap);
    uint32 channelId = AtChannelIdGet(self);
    const uint8 cDe1PayloadType = 0x0;

    mFieldIns(&regVal, cAf6_DCC_Control1_DCCEnable_Mask(channelId), cAf6_DCC_Control1_DCCEnable_Shift(channelId), mBoolToBin(enable));
    mChannelHwWrite(self, cAf6Reg_DCC_Control1_Base, regVal, cAtModuleEncap);

    if (enable)
        return Tha60210012PhysicalHdlcChannelTxServiceEnable((AtHdlcChannel)self, cDe1PayloadType);

    return Tha60210012PhysicalHdlcChannelTxServiceDisable((AtHdlcChannel)self);
    }

static eBool TxEncapConnectionIsEnabled(AtChannel self)
    {
    uint32 regVal = mChannelHwRead(self, cAf6Reg_DCC_Control1_Base, cAtModuleEncap);
    uint32 channelId = AtChannelIdGet(self);
    uint8 hwVal;

    mFieldGet(regVal, cAf6_DCC_Control1_DCCEnable_Mask(channelId), cAf6_DCC_Control1_DCCEnable_Shift(channelId), uint8, &hwVal);
    return (hwVal) ? cAtTrue : cAtFalse;
    }

static eAtRet RxEncapConnectionEnable(AtChannel self, eBool enable)
    {
    uint32 regVal = mChannelHwRead(self, cAf6Reg_DCC_Control3_Base, cAtModuleEncap);
    uint32 channelId = AtChannelIdGet(self);

    mFieldIns(&regVal, cAf6_DCC_Control3_DCCEnable_Mask(channelId), cAf6_DCC_Control3_DCCEnable_Shift(channelId), mBoolToBin(enable));
    mChannelHwWrite(self, cAf6Reg_DCC_Control3_Base, regVal, cAtModuleEncap);

    return cAtOk;
    }

static eBool RxEncapConnectionIsEnabled(AtChannel self)
    {
    uint32 regVal = mChannelHwRead(self, cAf6Reg_DCC_Control3_Base, cAtModuleEncap);
    uint32 channelId = AtChannelIdGet(self);
    uint8 hwVal;

    mFieldGet(regVal, cAf6_DCC_Control3_DCCEnable_Mask(channelId), cAf6_DCC_Control3_DCCEnable_Shift(channelId), uint8, &hwVal);
    return (hwVal) ? cAtTrue : cAtFalse;
    }

static const char *TypeString(AtChannel self)
    {
    AtUnused(self);
    return "dcc";
    }

static eAtRet PhyBind(AtEncapChannel self, AtChannel phyChannel)
    {
    return ((AtChannel)(mThis(self)->line) != phyChannel) ? cAtErrorModeNotSupport : cAtOk;
    }

static eAtRet RxTrafficEnable(AtChannel self, eBool rxTrafficEnable)
    {
    eAtRet ret = cAtOk;
    Tha60210012PhysicalHdlcChannel physicalChannel = (Tha60210012PhysicalHdlcChannel)self;

    if (rxTrafficEnable)
        {
        ret |= Tha60210012PhysicalHdlcChannelLinkLookupEnable(physicalChannel, rxTrafficEnable);
        ret |= mMethodsGet(self)->RxEncapConnectionEnable(self, rxTrafficEnable);

        return ret;
        }

    ret |= mMethodsGet(self)->RxEncapConnectionEnable(self, rxTrafficEnable);
    ret |= Tha60210012PhysicalHdlcChannelLinkLookupEnable(physicalChannel, rxTrafficEnable);

    return ret;
    }

static eBool RxTrafficIsEnabled(AtChannel self)
    {
    return mMethodsGet(self)->RxEncapConnectionIsEnabled(self);
    }

static eAtRet TxTrafficEnable(AtChannel self, eBool txTrafficEnable)
    {
    eAtRet ret = mMethodsGet(self)->TxEncapConnectionEnable(self, txTrafficEnable);
    ret = Tha60210061SdhLineDccBusEnable(mThis(self)->line, txTrafficEnable);

    return ret;
    }

static eBool TxTrafficIsEnabled(AtChannel self)
    {
    return mMethodsGet(self)->TxEncapConnectionIsEnabled(self);
    }

static ThaHdlcChannel PhysicalChannelGet(ThaHdlcChannel self)
    {
    return self;
    }

static const char *IdString(AtChannel self)
    {
    static char buf[30];
    uint32 bufferSize = sizeof(buf);

    if (mThis(self)->layer == cAtSdhLineDccLayerSection)
        AtSnprintf(buf, bufferSize, "%u.section", AtChannelIdGet((AtChannel)mThis(self)->line) + 1);
    else
        AtSnprintf(buf, bufferSize, "%u.line", AtChannelIdGet((AtChannel)mThis(self)->line) + 1);

    return buf;
    }

static eAtRet BindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    if (pseudowire)
        {
        ThaPwAdapter adapter = (ThaPwAdapter)pseudowire;

        /* Controller may exist */
        ThaPwAdapterHeaderControllerDelete(adapter);
        ThaPwAdapterHeaderControllerSet(adapter, Tha60210061DccPwHeaderControllerNew(adapter));
        }

    return m_AtChannelMethods->BindToPseudowire(self, pseudowire);
    }

static uint32 SurCounterId(ThaHdlcChannel self)
    {
    return AtChannelIdGet((AtChannel)AtHdlcChannelHdlcLinkGet((AtHdlcChannel)self));
    }

static void OverrideAtObject(AtHdlcChannel self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtHdlcChannel(AtHdlcChannel self)
    {
    AtHdlcChannel hdlcChannel = (AtHdlcChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtHdlcChannelMethods = mMethodsGet(hdlcChannel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtHdlcChannelOverride, m_AtHdlcChannelMethods, sizeof(m_AtHdlcChannelOverride));

        mMethodOverride(m_AtHdlcChannelOverride, FrameTypeSet);
        mMethodOverride(m_AtHdlcChannelOverride, ControlSizeSet);
        mMethodOverride(m_AtHdlcChannelOverride, ControlSizeGet);
        mMethodOverride(m_AtHdlcChannelOverride, TxAddressControlCompress);
        mMethodOverride(m_AtHdlcChannelOverride, TxAddressControlIsCompressed);
        }

    mMethodsSet(hdlcChannel, &m_AtHdlcChannelOverride);
    }

static void OverrideThaHdlcChannel(AtHdlcChannel self)
    {
    ThaHdlcChannel hdlcChannel = (ThaHdlcChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaHdlcChannelOverride, mMethodsGet(hdlcChannel), sizeof(m_ThaHdlcChannelOverride));
        mMethodOverride(m_ThaHdlcChannelOverride, CircuitSliceGet);
        mMethodOverride(m_ThaHdlcChannelOverride, CircuitIsHo);
        mMethodOverride(m_ThaHdlcChannelOverride, PhysicalChannelGet);
        mMethodOverride(m_ThaHdlcChannelOverride, SurCounterId);
        }

    mMethodsSet(hdlcChannel, &m_ThaHdlcChannelOverride);
    }

static void OverrideAtChannel(AtHdlcChannel self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, HwIdGet);
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, HwResourceAllocate);
        mMethodOverride(m_AtChannelOverride, HwResourceDeallocate);
        mMethodOverride(m_AtChannelOverride, Debug);
        mMethodOverride(m_AtChannelOverride, BoundPseudowireGet);
        mMethodOverride(m_AtChannelOverride, EncapHwIdAllocate);
        mMethodOverride(m_AtChannelOverride, TxEncapConnectionEnable);
        mMethodOverride(m_AtChannelOverride, TxEncapConnectionIsEnabled);
        mMethodOverride(m_AtChannelOverride, RxEncapConnectionEnable);
        mMethodOverride(m_AtChannelOverride, RxEncapConnectionIsEnabled);
        mMethodOverride(m_AtChannelOverride, TypeString);
        mMethodOverride(m_AtChannelOverride, RxTrafficEnable);
        mMethodOverride(m_AtChannelOverride, TxTrafficEnable);
        mMethodOverride(m_AtChannelOverride, RxTrafficIsEnabled);
        mMethodOverride(m_AtChannelOverride, TxTrafficIsEnabled);
        mMethodOverride(m_AtChannelOverride, IdString);
        mMethodOverride(m_AtChannelOverride, BindToPseudowire);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideThaPhysicalHdlcChannel(AtHdlcChannel self)
    {
    ThaPhysicalHdlcChannel channel = (ThaPhysicalHdlcChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPhysicalHdlcChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPhysicalHdlcChannelOverride, m_ThaPhysicalHdlcChannelMethods, sizeof(m_ThaPhysicalHdlcChannelOverride));

        mMethodOverride(m_ThaPhysicalHdlcChannelOverride, LinkCreate);
        }

    mMethodsSet(channel, &m_ThaPhysicalHdlcChannelOverride);
    }

static void OverrideAtEncapChannel(AtHdlcChannel self)
    {
    AtEncapChannel channel = (AtEncapChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtEncapChannelOverride, mMethodsGet(channel), sizeof(m_AtEncapChannelOverride));

        mMethodOverride(m_AtEncapChannelOverride, BoundPhyGet);
        mMethodOverride(m_AtEncapChannelOverride, PhyBind);
        }

    mMethodsSet(channel, &m_AtEncapChannelOverride);
    }

static void Override(AtHdlcChannel self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtEncapChannel(self);
    OverrideAtHdlcChannel(self);
    OverrideThaHdlcChannel(self);
    OverrideThaPhysicalHdlcChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061Dcc);
    }

static AtHdlcChannel ObjectInit(AtHdlcChannel self, uint32 channelId, AtModuleEncap module, AtSdhLine line, eAtSdhLineDccLayer layer)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012PhysicalHdlcLoChannelObjectInit(self, channelId, NULL, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    mThis(self)->line = line;
    mThis(self)->layer = layer;

    return self;
    }

AtHdlcChannel Tha60210061DccNew(uint32 channelId, AtModuleEncap module, AtSdhLine line, eAtSdhLineDccLayer layer)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtHdlcChannel newChannel = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newChannel == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newChannel, channelId, module, line, layer);
    }

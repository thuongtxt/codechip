/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Tha60210061ModulePw.h
 * 
 * Created Date: Sep 11, 2017
 *
 * Description : 60210061 module pw interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061MODULEPW_H_
#define _THA60210061MODULEPW_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 Tha60210061StartVersionHas1344PwsPerSts24Slice(ThaModulePw self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061MODULEPW_H_ */


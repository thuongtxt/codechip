/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60210061PwActivator.c
 *
 * Created Date: Jun 30, 2017
 *
 * Description : PW activator
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210012/pw/activator/Tha60210012PwActivator.h"
#include "../encap/Tha60210061ModuleEncap.h"
#include "../cla/Tha60210061ModuleCla.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210061PwActivator)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061PwActivator * Tha60210061PwActivator;
typedef struct tTha60210061PwActivator
	{
	tTha60210012PwActivator super;
	}tTha60210061PwActivator;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/*--------------------------- Forward declarations ---------------------------*/
/* Override */
static tTha60210011PwActivatorMethods m_Tha60210011PwActivatorOverride;

/* Super implementation */
static const tTha60210011PwActivatorMethods *m_Tha60210011PwActivatorMethods = NULL;

/*--------------------------- Implementation ---------------------------------*/
static AtModuleEncap ModuleEncap(ThaPwAdapter adapter)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)adapter);
    return (AtModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    }

static eBool PwBoundDcc(ThaPwAdapter adapter, uint8 slice, uint32 tdmPwId, eBool isLo)
    {
    if (!ThaPwTypeIsHdlcPppFr((AtPw)adapter))
        return cAtFalse;

    if ((slice == 0) && isLo && (tdmPwId >= Tha60210061ModuleEncapStartChannelForDcc(ModuleEncap(adapter))))
        return cAtTrue;

    return cAtFalse;
    }

static ThaModuleCla ModuleCla(ThaPwAdapter adapter)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)adapter);
    return (ThaModuleCla)AtDeviceModuleGet(device, cThaModuleCla);
    }

static uint32 MaxEthFlows(ThaPwAdapter adapter)
    {
    return AtModuleEthMaxFlowsGet((AtModuleEth)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)adapter), cAtModuleEth));
    }

static uint32 AssignedFlowId(ThaPwAdapter adapter)
    {
    return AtChannelIdGet((AtChannel)adapter) + MaxEthFlows(adapter);
    }

static ThaHwPw HwPwObjectCreate(Tha60210011PwActivator self, ThaPwAdapter adapter, uint8 slice, uint32 tdmPwId, eBool isLo)
    {
    ThaHwPw hwPw = m_Tha60210011PwActivatorMethods->HwPwObjectCreate(self, adapter, slice, tdmPwId, isLo);
    if (!Tha60210061ModuleEncapDccIsSupported(ModuleEncap(adapter)))
        return hwPw;

    if (!PwBoundDcc(adapter, slice, tdmPwId, isLo))
        return hwPw;

    Tha60210012HwPwHwFlowIdSet(hwPw, AssignedFlowId(adapter));
    ThaPwAdapterClaPwControllerSet(adapter, Tha60210061ModuleClaDccPwController(ModuleCla(adapter)));

    return hwPw;
    }

static void OverrideTha60210011PwActivator(ThaPwActivator self)
    {
    Tha60210011PwActivator activator = (Tha60210011PwActivator)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011PwActivatorMethods = mMethodsGet(activator);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011PwActivatorOverride, m_Tha60210011PwActivatorMethods, sizeof(m_Tha60210011PwActivatorOverride));

        mMethodOverride(m_Tha60210011PwActivatorOverride, HwPwObjectCreate);
        }

    mMethodsSet(activator, &m_Tha60210011PwActivatorOverride);
    }

static void Override(ThaPwActivator self)
    {
    OverrideTha60210011PwActivator(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061PwActivator);
    }

static ThaPwActivator ObjectInit(ThaPwActivator self, AtModulePw pwModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012PwActivatorObjectInit(self, pwModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwActivator Tha60210061PwActivatorNew(AtModulePw pwModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwActivator newObject = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newObject, pwModule);
    }

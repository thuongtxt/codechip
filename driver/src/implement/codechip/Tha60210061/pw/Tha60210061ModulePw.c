/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Pseudowire
 *
 * File        : Tha60210061ModulePw.c
 *
 * Created Date: Aug 22, 2016
 *
 * Description : Module PW of product 60210061
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210012/pw/Tha60210012ModulePw.h"
#include "../sdh/Tha60210061ModuleSdh.h"
#include "Tha60210061ModulePw.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061ModulePw
    {
    tTha60210012ModulePw super;
    }tTha60210061ModulePw;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePwMethods m_AtModulePwOverride;
static tThaModulePwMethods m_ThaModulePwOverride;
static tTha60210012ModulePwMethods m_Tha60210012ModulePwOverride;
static tTha60210011ModulePwMethods m_Tha60210011ModulePwOverride;

/* Save super implementation */
static const tAtModulePwMethods *m_AtModulePwMethods = NULL;
static const tTha60210011ModulePwMethods *m_Tha60210011ModulePwMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxPwsGet(AtModulePw self)
    {
    AtUnused(self);
    return 1536; /* TDM CES/CEP */
    }

static uint32 DefaultCounterModule(ThaModulePw self)
    {
    AtUnused(self);
    return cAtModuleSur;
    }

static uint32 MaxApsGroupsGet(AtModulePw self)
    {
    AtUnused(self);
    return 2048;
    }

static uint32 MaxHsGroupsGet(AtModulePw self)
    {
    AtUnused(self);
    return 1024;
    }

static AtModuleSdh ModuleSdh(AtSdhChannel circuit)
    {
    return (AtModuleSdh)AtChannelModuleGet((AtChannel)circuit);
    }

static eBool PwCircuitBelongsToLoLine(Tha60210011ModulePw self, AtPw pw)
    {
    if (AtPwTypeGet(pw) == cAtPwTypeCEP)
        {
        AtSdhChannel circuit = (AtSdhChannel)AtPwBoundCircuitGet(pw);
        if (Tha60210061ModuleSdhChannelIsInEc1Line(ModuleSdh(circuit), circuit))
            return cAtTrue;
        }

    return m_Tha60210011ModulePwMethods->PwCircuitBelongsToLoLine(self, pw);
    }

static eBool InternalCounterIsSupported(Tha60210012ModulePw self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static ThaPwActivator PwActivatorCreate(ThaModulePw self)
    {
    return Tha60210061PwActivatorNew((AtModulePw)self);
    }

static uint32 StartVersionHas1344PwsPerSts24Slice(Tha60210011ModulePw self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x3, 0x8, 0x0);
    }

static void OverrideAtModulePw(AtModulePw self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModulePwMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePwOverride, m_AtModulePwMethods, sizeof(tAtModulePwMethods));

        mMethodOverride(m_AtModulePwOverride, MaxPwsGet);
        mMethodOverride(m_AtModulePwOverride, MaxApsGroupsGet);
        mMethodOverride(m_AtModulePwOverride, MaxHsGroupsGet);
        }

    mMethodsSet(self, &m_AtModulePwOverride);
    }

static void OverrideThaModulePw(AtModulePw self)
    {
    ThaModulePw pwModule = (ThaModulePw)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePwOverride, mMethodsGet(pwModule), sizeof(m_ThaModulePwOverride));

        mMethodOverride(m_ThaModulePwOverride, DefaultCounterModule);
        mMethodOverride(m_ThaModulePwOverride, PwActivatorCreate);
        }

    mMethodsSet(pwModule, &m_ThaModulePwOverride);
    }

static void OverrideTha60210012ModulePw(AtModulePw self)
    {
    Tha60210012ModulePw pwModule = (Tha60210012ModulePw)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210012ModulePwOverride, mMethodsGet(pwModule), sizeof(m_Tha60210012ModulePwOverride));

        mMethodOverride(m_Tha60210012ModulePwOverride, InternalCounterIsSupported);
        }

    mMethodsSet(pwModule, &m_Tha60210012ModulePwOverride);
    }

static void OverrideTha60210011ModulePw(AtModulePw self)
    {
    Tha60210011ModulePw modulePw = (Tha60210011ModulePw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011ModulePwMethods = mMethodsGet(modulePw);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModulePwOverride, m_Tha60210011ModulePwMethods, sizeof(m_Tha60210011ModulePwOverride));

        mMethodOverride(m_Tha60210011ModulePwOverride, PwCircuitBelongsToLoLine);
        mMethodOverride(m_Tha60210011ModulePwOverride, StartVersionHas1344PwsPerSts24Slice);
        }

    mMethodsSet(modulePw, &m_Tha60210011ModulePwOverride);
    }

static void Override(AtModulePw self)
    {
    OverrideAtModulePw(self);
    OverrideThaModulePw(self);
    OverrideTha60210012ModulePw(self);
    OverrideTha60210011ModulePw(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061ModulePw);
    }

static AtModulePw ObjectInit(AtModulePw self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012ModulePwObjectInit((AtModulePw)self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePw Tha60210061ModulePwNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePw newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

uint32 Tha60210061StartVersionHas1344PwsPerSts24Slice(ThaModulePw self)
    {
    if (self)
        return StartVersionHas1344PwsPerSts24Slice((Tha60210011ModulePw)self);
    return cInvalidUint32;
    }


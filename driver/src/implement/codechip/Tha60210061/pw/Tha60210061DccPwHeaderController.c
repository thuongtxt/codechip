/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60210061DccPwHeaderController.c
 *
 * Created Date: Jul 11, 2017
 *
 * Description : DCC PW header controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPw.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../Tha60210011/pw/headercontroller/Tha60210011PwHeaderControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210061DccPwHeaderController)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061DccPwHeaderController * Tha60210061DccPwHeaderController;
typedef struct tTha60210061DccPwHeaderController
	{
	tTha60210011PwHeaderController super;
	eBool hasRoomForEthType;
	}tTha60210061DccPwHeaderController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods              m_AtObjectOverride;
static tThaPwHeaderControllerMethods m_ThaPwHeaderControllerOverride;

/* Super implementation */
static const tAtObjectMethods              *m_AtObjectMethods              = NULL;
static const tThaPwHeaderControllerMethods *m_ThaPwHeaderControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool ShouldEnableCwByDefault(ThaPwHeaderController self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModulePwRet HeaderLengthSet(ThaPwHeaderController self, uint8 hdrLenInByte)
    {
    AtPw adapter = (AtPw)ThaPwHeaderControllerAdapterGet(self);
    if ((AtPwPsnGet(adapter) == NULL) && (mThis(self)->hasRoomForEthType == cAtFalse))
        {
        hdrLenInByte = (uint8)(hdrLenInByte + 2); /* Have room for ethernet type event when there is no PSN header */
        mThis(self)->hasRoomForEthType = cAtTrue;
        }

    return m_ThaPwHeaderControllerMethods->HeaderLengthSet(self, hdrLenInByte);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60210061DccPwHeaderController object = mThis(self);
    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeUInt(hasRoomForEthType);
    }

static void OverrideAtObject(ThaPwHeaderController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideThaPwHeaderController(ThaPwHeaderController self)
    {
    ThaPwHeaderController controller = (ThaPwHeaderController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPwHeaderControllerMethods = mMethodsGet(controller);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwHeaderControllerOverride, m_ThaPwHeaderControllerMethods, sizeof(m_ThaPwHeaderControllerOverride));

        mMethodOverride(m_ThaPwHeaderControllerOverride, ShouldEnableCwByDefault);
        mMethodOverride(m_ThaPwHeaderControllerOverride, HeaderLengthSet);
        }

    mMethodsSet(controller, &m_ThaPwHeaderControllerOverride);
    }

static void Override(ThaPwHeaderController self)
    {
    OverrideAtObject(self);
    OverrideThaPwHeaderController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061DccPwHeaderController);
    }

static ThaPwHeaderController ObjectInit(ThaPwHeaderController self, ThaPwAdapter adapter)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011PwHeaderControllerObjectInit(self, adapter) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwHeaderController Tha60210061DccPwHeaderControllerNew(ThaPwAdapter adapter)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwHeaderController newObject = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newObject, adapter);
    }

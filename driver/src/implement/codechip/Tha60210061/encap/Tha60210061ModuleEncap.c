/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Encap
 *
 * File        : Tha60210061ModuleEncap.c
 *
 * Created Date: Dec 23, 2016
 *
 * Description : Encapsulation module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/man/versionreader/ThaVersionReader.h"
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60210012/encap/Tha60210012ModuleEncapInternal.h"
#include "../sdh/Tha60210061ModuleSdh.h"
#include "../pw/Tha60210061ModulePw.h"
#include "../dcc/Tha60210061DccReg.h"
#include "Tha60210061ModuleEncap.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210061ModuleEncap)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061ModuleEncap * Tha60210061ModuleEncap;
typedef struct tTha60210061ModuleEncap
    {
    tTha60210012ModuleEncap super;
    uint32 asyncState;
    }tTha60210061ModuleEncap;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods               m_AtObjectOverride;
static tAtModuleMethods               m_AtModuleOverride;
static tAtModuleEncapMethods          m_AtModuleEncapOverride;
static tThaModuleDynamicEncapMethods  m_ThaModuleDynamicEncapOverride;
static tTha60210012ModuleEncapMethods m_Tha60210012ModuleEncapOverride;

/* Save super implementation */
static const tAtObjectMethods              *m_AtObjectMethods = NULL;
static const tAtModuleMethods              *m_AtModuleMethods = NULL;
static const tThaModuleDynamicEncapMethods *m_ThaModuleDynamicEncapMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtEncapChannel PhysicalEncapChannelCreate(ThaModuleDynamicEncap self, uint32 channelId, AtEncapChannel logicalChannel)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)logicalChannel);
    AtEncapBinder binder = AtDeviceEncapBinder(device);

    if (AtEncapChannelEncapTypeGet(logicalChannel) == cAtEncapHdlc)
        {
        if (ThaEncapDynamicBinderHwIsHo((ThaEncapDynamicBinder)binder, channelId))
            return (AtEncapChannel)Tha60210061PhysicalHdlcChannelHoNew(channelId, (AtHdlcChannel)logicalChannel, (AtModuleEncap)self);
        }

    return m_ThaModuleDynamicEncapMethods->PhysicalEncapChannelCreate(self, channelId, logicalChannel);
    }

static uint32 StartVersionSupportProfileManager(Tha60210012ModuleEncap self)
    {
    AtUnused(self);
    return 0x0;
    }

static uint32 StartVersionSupportDcc(void)
    {
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x3, 0x4, 0x0);
    }

static ThaModulePw ModulePw(AtModuleEncap self)
    {
    return (ThaModulePw)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModulePw);
    }

static uint16 MaxChannelsGet(AtModuleEncap self)
    {
    if (Tha60210061ModuleEncapDccIsSupported(self))
    return 1016;

    return 1024;
    }

static eBool StsNcNeedSelect(Tha60210012ModuleEncap self, AtSdhChannel sdhChannel)
    {
    AtUnused(self);
    if (Tha60210061ModuleSdhChannelIsInEc1Line((AtModuleSdh)AtChannelModuleGet((AtChannel)sdhChannel), sdhChannel))
        return cAtFalse;

    return cAtTrue;
    }

static uint32 StartVersionSupportResequenceManager(Tha60210012ModuleEncap self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x0, 0x0);
    }

static eBool ShouldSetupWhenNoHal(AtModule self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool NeedInitDcc(AtModule self)
    {
    AtDevice device = AtModuleDeviceGet(self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);

    return (hwVersion >= Tha60210061StartVersionHas1344PwsPerSts24Slice(ModulePw((AtModuleEncap)self))) ? cAtTrue : cAtFalse;
    }

static eAtRet DccInit(AtModule self)
    {
    if (NeedInitDcc(self))
        {
        uint32 regVals[3];
        uint32 channelId = 1344;
        AtOsalMemInit(regVals, 0, sizeof(regVals));

        mRegFieldSet(regVals[0], cAf6_DCC_Control4_SDCC0ID_, channelId); channelId++;
        mRegFieldSet(regVals[0], cAf6_DCC_Control4_LDCC0ID_, channelId); channelId++;

        mFieldIns(&regVals[0], cAf6_DCC_Control4_SDCC1ID_Mask_01, cAf6_DCC_Control4_SDCC1ID_Shift_01, channelId);
        mFieldIns(&regVals[1], cAf6_DCC_Control4_SDCC1ID_Mask_02, cAf6_DCC_Control4_SDCC1ID_Shift_02, channelId >> 10);
        channelId++;

        mRegFieldSet(regVals[1], cAf6_DCC_Control4_LDCC1ID_, channelId); channelId++;
        mRegFieldSet(regVals[1], cAf6_DCC_Control4_SDCC2ID_, channelId); channelId++;

        mFieldIns(&regVals[1], cAf6_DCC_Control4_LDCC2ID_Mask_01, cAf6_DCC_Control4_LDCC2ID_Shift_01, channelId);
        mFieldIns(&regVals[2], cAf6_DCC_Control4_LDCC2ID_Mask_02, cAf6_DCC_Control4_LDCC2ID_Shift_02, channelId >> 9);
        channelId++;

        mRegFieldSet(regVals[2], cAf6_DCC_Control4_SDCC3ID_, channelId); channelId++;
        mRegFieldSet(regVals[2], cAf6_DCC_Control4_LDCC3ID_, channelId);

        mModuleHwWrite(self, cAf6Reg_DCC_Control4_Base, regVals[0]);
        mModuleHwWrite(self, cAf6Reg_DCC_Control4_Base + 1, regVals[1]);
        mModuleHwWrite(self, cAf6Reg_DCC_Control4_Base + 2, regVals[2]);
        }

    return cAtOk;
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return DccInit(self);
    }

static eAtRet SuperAsyncInit(AtObject self)
    {
    return m_AtModuleMethods->AsyncInit((AtModule)self);
    }

static tAtAsyncOperationFunc NextOp(AtObject self, uint32 state)
    {
    AtUnused(self);
    if (state == 0) return SuperAsyncInit;
    if (state == 1) return (tAtAsyncOperationFunc)DccInit;
    return NULL;
    }

static eAtRet AsyncInit(AtModule self)
    {
    return AtDeviceObjectAsyncProcess(AtModuleDeviceGet(self), (AtObject)self, &mThis(self)->asyncState, NextOp);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60210061ModuleEncap object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(asyncState);
    }

static void OverrideAtObject(AtModuleEncap self)
    {
    AtObject module = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(module, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModuleEncap self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, ShouldSetupWhenNoHal);
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtModuleEncap(AtModuleEncap self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleEncapOverride, mMethodsGet(self), sizeof(m_AtModuleEncapOverride));

        mMethodOverride(m_AtModuleEncapOverride, MaxChannelsGet);
        }

    mMethodsSet(self, &m_AtModuleEncapOverride);
    }

static void OverrideThaModuleDynamicEncap(AtModuleEncap self)
    {
    ThaModuleDynamicEncap encapModule = (ThaModuleDynamicEncap)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleDynamicEncapMethods = mMethodsGet(encapModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleDynamicEncapOverride, m_ThaModuleDynamicEncapMethods, sizeof(m_ThaModuleDynamicEncapOverride));

        mMethodOverride(m_ThaModuleDynamicEncapOverride, PhysicalEncapChannelCreate);
        }

    mMethodsSet(encapModule, &m_ThaModuleDynamicEncapOverride);
    }

static void OverrideTha60210012ModuleEncap(AtModuleEncap self)
    {
    Tha60210012ModuleEncap encapModule = (Tha60210012ModuleEncap)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210012ModuleEncapOverride, mMethodsGet(encapModule), sizeof(m_Tha60210012ModuleEncapOverride));

        mMethodOverride(m_Tha60210012ModuleEncapOverride, StsNcNeedSelect);
        mMethodOverride(m_Tha60210012ModuleEncapOverride, StartVersionSupportProfileManager);
        mMethodOverride(m_Tha60210012ModuleEncapOverride, StartVersionSupportResequenceManager);
        }

    mMethodsSet(encapModule, &m_Tha60210012ModuleEncapOverride);
    }

static void Override(AtModuleEncap self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    OverrideAtModuleEncap(self);
    OverrideThaModuleDynamicEncap(self);
    OverrideTha60210012ModuleEncap(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061ModuleEncap);
    }

static AtModuleEncap ObjectInit(AtModuleEncap self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012ModuleEncapObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;
    ((Tha60210012ModuleEncap)self)->counterModule = cAtModuleSur;

    return self;
    }

AtModuleEncap Tha60210061ModuleEncapNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEncap newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

eBool Tha60210061ModuleEncapDccIsSupported(AtModuleEncap self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);

    return (hwVersion >= StartVersionSupportDcc()) ? cAtTrue : cAtFalse;
    }

eBool Tha60210061ModuleEncapDccSharesSlice0(AtModuleEncap self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);

    if ((hwVersion >= StartVersionSupportDcc()) &&
        (hwVersion < Tha60210061StartVersionHas1344PwsPerSts24Slice(ModulePw(self))))
        return cAtTrue;

    return cAtFalse;
    }

uint32 Tha60210061ModuleEncapStartChannelForDcc(AtModuleEncap self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);

    if (hwVersion >= Tha60210061StartVersionHas1344PwsPerSts24Slice(ModulePw(self)))
        return 1344;

    return 1016;
    }


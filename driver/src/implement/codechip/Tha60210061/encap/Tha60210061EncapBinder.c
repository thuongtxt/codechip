/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Encap
 *
 * File        : Tha60210061EncapBinder.c
 *
 * Created Date: Feb 7, 2017
 *
 * Description : 60210061 encap binder
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/binder/ThaEncapBinderInternal.h"
#include "../sdh/Tha60210061ModuleSdh.h"
#include "Tha60210061ModuleEncap.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061EncapBinder
    {
    tThaEncapDynamicBinder super;
    }tTha60210061EncapBinder;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtEncapBinderMethods m_AtEncapBinderOverride;
static tThaEncapDynamicBinderMethods m_ThaEncapDynamicBinderOverride;

/* Save super implementation */
static const tAtEncapBinderMethods         *m_AtEncapBinderMethods = NULL;
static const tThaEncapDynamicBinderMethods *m_ThaEncapDynamicBinderMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleEncap ModuleEncap(ThaEncapDynamicBinder self)
    {
    return (AtModuleEncap)AtDeviceModuleGet(AtEncapBinderDeviceGet((AtEncapBinder)self), cAtModuleEncap);
    }

static ThaBitMask MaskInUsedChannelsForDcc(ThaEncapDynamicBinder self, ThaBitMask poolOfSlice0)
    {
    uint32 bit_i;
    uint32 startChannelForDcc = Tha60210061ModuleEncapStartChannelForDcc(ModuleEncap(self));
    const uint32 cNumDccChannel = 8;

    for (bit_i = 0; bit_i < cNumDccChannel; bit_i++)
        ThaBitMaskSetBit(poolOfSlice0, startChannelForDcc + bit_i);

    return poolOfSlice0;
    }

static ThaBitMask ChannelPoolCreate(ThaEncapDynamicBinder self, uint8 slice)
    {
    ThaBitMask poolOfSlice0;

    if ((slice > 0) || !Tha60210061ModuleEncapDccSharesSlice0(ModuleEncap(self)))
        return m_ThaEncapDynamicBinderMethods->ChannelPoolCreate(self, slice);

    poolOfSlice0 = m_ThaEncapDynamicBinderMethods->ChannelPoolCreate(self, slice);
    if (poolOfSlice0)
        return MaskInUsedChannelsForDcc(self, poolOfSlice0);

    return NULL;
    }

static AtModuleSdh ModuleSdh(AtSdhVc vc)
    {
    return (AtModuleSdh)AtChannelModuleGet((AtChannel)vc);
    }

static uint32 AuVcEncapHwIdAllocate(AtEncapBinder self, AtSdhVc auvc)
    {
    if (!Tha60210061ModuleSdhChannelIsInEc1Line(ModuleSdh(auvc), (AtSdhChannel)auvc))
        return m_AtEncapBinderMethods->AuVcEncapHwIdAllocate(self, auvc);

    return ThaEncapDynamicBinderLoVcEncapChannelHwIdGet(self, auvc);
    }

static void OverrideAtEncapBinder(AtEncapBinder self)
    {
    AtEncapBinder binder = (AtEncapBinder)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEncapBinderMethods = mMethodsGet(binder);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEncapBinderOverride, m_AtEncapBinderMethods, sizeof(m_AtEncapBinderOverride));

        mMethodOverride(m_AtEncapBinderOverride, AuVcEncapHwIdAllocate);
        }

    mMethodsSet(binder, &m_AtEncapBinderOverride);
    }

static void OverrideThaEncapDynamicBinder(AtEncapBinder self)
    {
    ThaEncapDynamicBinder binder = (ThaEncapDynamicBinder)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaEncapDynamicBinderMethods = mMethodsGet(binder);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEncapDynamicBinderOverride, m_ThaEncapDynamicBinderMethods, sizeof(m_ThaEncapDynamicBinderOverride));

        mMethodOverride(m_ThaEncapDynamicBinderOverride, ChannelPoolCreate);
        }

    mMethodsSet(binder, &m_ThaEncapDynamicBinderOverride);
    }

static void Override(AtEncapBinder self)
    {
    OverrideAtEncapBinder(self);
    OverrideThaEncapDynamicBinder(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061EncapBinder);
    }

static AtEncapBinder ObjectInit(AtEncapBinder self, AtDevice device, uint8 numPool, uint32 numChannelPerPool)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaEncapDynamicBinderObjectInit(self, device, numPool, numChannelPerPool) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEncapBinder Tha60210061EncapBinderNew(uint8 numPool, AtDevice device, uint32 numChannelPerPool)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEncapBinder newBinder = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newBinder == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newBinder, device, numPool, numChannelPerPool);
    }

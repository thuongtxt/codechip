/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : Tha60210061PhysicalHdlcHoChannel.c
 *
 * Created Date: Dec 29, 2016
 *
 * Description : 60210061 physical HDLC (High Path) channel
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210012/encap/Tha60210012PhysicalHdlcChannelInternal.h"
#include "../../Tha60210012/encap/Tha60210012ModuleDecapParserReg.h"
#include "Tha60210061ModuleEncap.h"

/*--------------------------- Define -----------------------------------------*/
#define mDefaultOffset(self) Tha60210012PhysicalHdlcChannelDefaultOffset((Tha60210012PhysicalHdlcChannel)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061PhysicalHdlcHoChannel
    {
    tTha60210012PhysicalHdlcHoChannel super;
    }tTha60210061PhysicalHdlcHoChannel;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtHdlcChannelMethods        m_AtHdlcChannelOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtModuleEncapRet IdlePatternSet(AtHdlcChannel self, uint8 idlePattern)
    {
    AtUnused(self);
    return (idlePattern == 0x7E) ? cAtOk : cAtErrorModeNotSupport;
    }

static uint8 IdlePatternGet(AtHdlcChannel self)
    {
    AtUnused(self);
    return 0x7E;
    }

static eAtHdlcStuffMode StuffModeDefault(AtHdlcChannel self)
    {
    AtUnused(self);
    return cAtHdlcStuffByte;
    }

static eBool StuffModeIsSupported(AtHdlcChannel self, eAtHdlcStuffMode stuffMode)
    {
    AtUnused(self);
    return (stuffMode == cAtHdlcStuffByte) ? cAtTrue : cAtFalse;
    }

static eAtRet StuffModeSet(AtHdlcChannel self, eAtHdlcStuffMode stuffMode)
    {
    AtUnused(self);
    return (stuffMode == cAtHdlcStuffByte) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtHdlcStuffMode StuffModeGet(AtHdlcChannel self)
    {
    AtUnused(self);
    return cAtHdlcStuffByte;
    }

static void OverrideAtHdlcChannel(AtHdlcChannel self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtHdlcChannelOverride, mMethodsGet(self), sizeof(tAtHdlcChannelMethods));
        mMethodOverride(m_AtHdlcChannelOverride, StuffModeSet);
        mMethodOverride(m_AtHdlcChannelOverride, StuffModeGet);
        mMethodOverride(m_AtHdlcChannelOverride, IdlePatternSet);
        mMethodOverride(m_AtHdlcChannelOverride, IdlePatternGet);
        mMethodOverride(m_AtHdlcChannelOverride, StuffModeDefault);
        mMethodOverride(m_AtHdlcChannelOverride, StuffModeIsSupported);
        }

    mMethodsSet(self, &m_AtHdlcChannelOverride);
    }

static void Override(AtHdlcChannel self)
    {
    OverrideAtHdlcChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061PhysicalHdlcHoChannel);
    }

static AtHdlcChannel ObjectInit(AtHdlcChannel self, uint32 channelId, AtHdlcChannel logicalChannel, AtModuleEncap module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012PhysicalHdlcHoChannelObjectInit(self, channelId, logicalChannel, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtHdlcChannel Tha60210061PhysicalHdlcChannelHoNew(uint32 channelId, AtHdlcChannel logicalChannel, AtModuleEncap module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtHdlcChannel newChannel = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newChannel == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newChannel, channelId, logicalChannel, module);
    }


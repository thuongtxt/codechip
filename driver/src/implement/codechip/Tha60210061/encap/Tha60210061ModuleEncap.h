/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Encap
 * 
 * File        : Tha60210061ModuleEncap.h
 * 
 * Created Date: Dec 29, 2016
 *
 * Description : Interface of Module Encap
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061MODULEENCAP_H_
#define _THA60210061MODULEENCAP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSdhLine.h"
#include "../../../default/encap/dynamic/ThaEncapDynamicBinder.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
#define cMaxDccChannel 8

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHdlcChannel Tha60210061PhysicalHdlcChannelHoNew(uint32 channelId, AtHdlcChannel logicalChannel, AtModuleEncap module);
AtHdlcChannel Tha60210061DccNew(uint32 channelId, AtModuleEncap module, AtSdhLine line, eAtSdhLineDccLayer layer);
AtEncapBinder Tha60210061EncapBinderNew(uint8 numPool, AtDevice device, uint32 numChannelPerPool);

eBool Tha60210061ModuleEncapDccIsSupported(AtModuleEncap self);
uint32 Tha60210061ModuleEncapStartChannelForDcc(AtModuleEncap self);
eBool Tha60210061ModuleEncapDccSharesSlice0(AtModuleEncap self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061MODULEENCAP_H_ */

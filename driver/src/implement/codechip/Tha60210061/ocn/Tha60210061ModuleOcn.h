/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : OCN
 * 
 * File        : Tha60210061ModuleOcn.h
 * 
 * Created Date: Dec 8, 2016
 *
 * Description : Module OCN interfaces
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061MODULEOCN_H_
#define _THA60210061MODULEOCN_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/ocn/Tha60210011ModuleOcn.h"
#include "../ocn/Tha60210061ModuleOcnReg.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 Tha60210061ModuleOcnEc1BaseAddress(ThaModuleOcn self);
void Tha60210061ModuleOcnVirtualFifoCounterEnable(ThaModuleOcn self, eBool enable);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061MODULEOCN_H_ */


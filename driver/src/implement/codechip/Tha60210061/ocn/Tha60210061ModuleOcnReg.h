/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : OCN
 * 
 * File        : Tha60210061ModuleOcnReg.h
 * 
 * Created Date: Aug 19, 2016
 *
 * Description : Module OCN register description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061MODULEOCNREG_H_
#define _THA60210061MODULEOCNREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/*------------------------------------------------------------------------------
Reg Name   : OCN Global Rx Framer Control
Reg Addr   : 0x00000
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the global configuration register for the Rx Framer

------------------------------------------------------------------------------*/
#define cAf6Reg_glbrfm_reg                                                                             0x00000

/*--------------------------------------
BitField Name: RxFrmBadFrmThresh
BitField Type: RW
BitField Desc: Threshold for A1A2 missing counter, that is used to change state
from FRAMED to HUNT.
BitField Bits: [14:12]
--------------------------------------*/
#define cAf6_glbrfm_reg_RxFrmBadFrmThresh_Mask                                                       cBit14_12
#define cAf6_glbrfm_reg_RxFrmBadFrmThresh_Shift                                                             12

/*--------------------------------------
BitField Name: RxFrmDescrEn
BitField Type: RW
BitField Desc: Enable/disable de-scrambling of the Rx coming data stream for 4
lines. 1: Enable 0: Disable
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_glbrfm_reg_RxFrmDescrEn_Mask                                                             cBit11_8
#define cAf6_glbrfm_reg_RxFrmDescrEn_Shift                                                                   8

/*--------------------------------------
BitField Name: RxFrmStmOcnMode
BitField Type: RW
BitField Desc: STM rate mode for Rx of  4 lines, each line use 2 bits.Bits[1:0]
for line 0 With OC48 mode, we can config to run with any port (1-4) but if one
of these ports is config as OC48, another ports MUST config is Disable. 0: OC48
1: OC12 2: OC3 3: Disable.
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_glbrfm_reg_RxFrmStmOcnMode_Mask                                                           cBit7_0
#define cAf6_glbrfm_reg_RxFrmStmOcnMode_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Rx Framer LOS Detecting Control 1
Reg Addr   : 0x00006
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the global configuration register for the Rx Framer LOS detecting 1

------------------------------------------------------------------------------*/
#define cAf6Reg_glbclkmon_reg                                                                          0x00006

/*--------------------------------------
BitField Name: RxFrmLosDetMod
BitField Type: RW
BitField Desc: Detected LOS mode. 1: the LOS declare when all zero or all one
detected 0: the LOS declare when all zero detected
BitField Bits: [18]
--------------------------------------*/
#define cAf6_glbclkmon_reg_RxFrmLosDetMod_Mask                                                          cBit18
#define cAf6_glbclkmon_reg_RxFrmLosDetMod_Shift                                                             18

/*--------------------------------------
BitField Name: RxFrmLosDetDis
BitField Type: RW
BitField Desc: Disable detect LOS. 1: Disable 0: Enable
BitField Bits: [17]
--------------------------------------*/
#define cAf6_glbclkmon_reg_RxFrmLosDetDis_Mask                                                          cBit17
#define cAf6_glbclkmon_reg_RxFrmLosDetDis_Shift                                                             17

/*--------------------------------------
BitField Name: RxFrmClkMonDis
BitField Type: RW
BitField Desc: Disable to generate LOS to Rx Framer if detecting error on Rx
line clock. 1: Disable 0: Enable
BitField Bits: [16]
--------------------------------------*/
#define cAf6_glbclkmon_reg_RxFrmClkMonDis_Mask                                                          cBit16
#define cAf6_glbclkmon_reg_RxFrmClkMonDis_Shift                                                             16

/*--------------------------------------
BitField Name: RxFrmClkMonThr
BitField Type: RW
BitField Desc: Threshold to generate LOS to Rx Framer if detecting error on Rx
line clock.(Threshold = PPM * 155.52/8 for STM16 and STM1). Default 0x3cc ~
50ppm for STM16 and STM1. With STM4 must config value that is this default value
divide by 4.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_glbclkmon_reg_RxFrmClkMonThr_Mask                                                        cBit15_0
#define cAf6_glbclkmon_reg_RxFrmClkMonThr_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Rx Framer LOS Detecting Control 2
Reg Addr   : 0x00007
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the global configuration register for the Rx Framer LOS detecting 2

------------------------------------------------------------------------------*/
#define cAf6Reg_glbdetlos_pen                                                                          0x00007

/*--------------------------------------
BitField Name: RxFrmLosClr2Thr
BitField Type: RW
BitField Desc: Configure the period of time during which there is no period of
consecutive data without transition, used for reset no transition counter. The
recommended value is 0x30 (~2.5us) for STM16 and STM1. With STM4 must config
value that is this default value divide by 4.
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_glbdetlos_pen_RxFrmLosClr2Thr_Mask                                                      cBit31_24
#define cAf6_glbdetlos_pen_RxFrmLosClr2Thr_Shift                                                            24

/*--------------------------------------
BitField Name: RxFrmLosSetThr
BitField Type: RW
BitField Desc: Configure the value that define the period of time in which there
is no transition detected from incoming data from SERDES. The recommended value
is 0x3cc (~50us) for STM16 and STM1. With STM4 must config value that is this
default value divide by 4.
BitField Bits: [23:12]
--------------------------------------*/
#define cAf6_glbdetlos_pen_RxFrmLosSetThr_Mask                                                       cBit23_12
#define cAf6_glbdetlos_pen_RxFrmLosSetThr_Shift                                                             12

/*--------------------------------------
BitField Name: RxFrmLosClr1Thr
BitField Type: RW
BitField Desc: Configure the period of time during which there is no period of
consecutive data without transition, used for counting at INFRM to change state
to LOS The recommended value is 0x3cc (~50us) for STM16 and STM1. With STM4 must
config value that is this default value divide by 4.
BitField Bits: [11:0]
--------------------------------------*/
#define cAf6_glbdetlos_pen_RxFrmLosClr1Thr_Mask                                                       cBit11_0
#define cAf6_glbdetlos_pen_RxFrmLosClr1Thr_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Rx Framer LOF Threshold
Reg Addr   : 0x00008
Reg Formula: 
    Where  : 
Reg Desc   : 
Configure thresholds for LOF detection at receive SONET/SDH framer,There are two thresholds

------------------------------------------------------------------------------*/
#define cAf6Reg_glblofthr_reg                                                                          0x00008

/*--------------------------------------
BitField Name: RxFrmLofDetMod
BitField Type: RW
BitField Desc: Detected LOF mode.Set 1 to clear OOF counter when state into
INFRAMED
BitField Bits: [16]
--------------------------------------*/
#define cAf6_glblofthr_reg_RxFrmLofDetMod_Mask                                                          cBit16
#define cAf6_glblofthr_reg_RxFrmLofDetMod_Shift                                                             16

/*--------------------------------------
BitField Name: RxFrmLofSetThr
BitField Type: RW
BitField Desc: Configure the OOF time counter threshold for entering LOF state.
Resolution of this threshold is one frame.
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_glblofthr_reg_RxFrmLofSetThr_Mask                                                        cBit15_8
#define cAf6_glblofthr_reg_RxFrmLofSetThr_Shift                                                              8

/*--------------------------------------
BitField Name: RxFrmLofClrThr
BitField Type: RW
BitField Desc: Configure the In-frame time counter threshold for exiting LOF
state. Resolution of this threshold is one frame.
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_glblofthr_reg_RxFrmLofClrThr_Mask                                                         cBit7_0
#define cAf6_glblofthr_reg_RxFrmLofClrThr_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Rx Framer - AIS dowstream and RDI Back toward enable
Reg Addr   : 0x0000c
Reg Formula: 
    Where  : 
Reg Desc   : 
Enable/disable for AIS dowstream and RDI Back toward at receive SONET/SDH framer of 4 lines, each line used 4bits,Bits[3:0] for line 0

------------------------------------------------------------------------------*/
#define cAf6Reg_glbrfmaisrdifwd_reg                                                                    0x0000c

/*--------------------------------------
BitField Name: RxFrmAislRdiLEn4
BitField Type: RW
BitField Desc: Enable/disable the insertion of RDI-L at TOH insertion when AIS_L
condition detected at line 4. 1: Enable 0: Disable
BitField Bits: [31]
--------------------------------------*/
#define cAf6_glbrfmaisrdifwd_reg_RxFrmAislRdiLEn4_Mask                                                  cBit31
#define cAf6_glbrfmaisrdifwd_reg_RxFrmAislRdiLEn4_Shift                                                     31

/*--------------------------------------
BitField Name: RxFrmTimRdiLEn4
BitField Type: RW
BitField Desc: Enable/disable the insertion of RDI-L at TOH insertion when TIM
condition detected at line 4. 1: Enable 0: Disable
BitField Bits: [30]
--------------------------------------*/
#define cAf6_glbrfmaisrdifwd_reg_RxFrmTimRdiLEn4_Mask                                                   cBit30
#define cAf6_glbrfmaisrdifwd_reg_RxFrmTimRdiLEn4_Shift                                                      30

/*--------------------------------------
BitField Name: RxFrmLofRdiLEn4
BitField Type: RW
BitField Desc: Enable/disable the insertion of RDI-L at TOH insertion when LOF
condition detected at line 4. 1: Enable 0: Disable
BitField Bits: [29]
--------------------------------------*/
#define cAf6_glbrfmaisrdifwd_reg_RxFrmLofRdiLEn4_Mask                                                   cBit29
#define cAf6_glbrfmaisrdifwd_reg_RxFrmLofRdiLEn4_Shift                                                      29

/*--------------------------------------
BitField Name: RxFrmLosRdiLEn4
BitField Type: RW
BitField Desc: Enable/disable the insertion of RDI-L at TOH insertion when LOS
condition detected at line 4. 1: Enable 0: Disable
BitField Bits: [28]
--------------------------------------*/
#define cAf6_glbrfmaisrdifwd_reg_RxFrmLosRdiLEn4_Mask                                                   cBit28
#define cAf6_glbrfmaisrdifwd_reg_RxFrmLosRdiLEn4_Shift                                                      28

/*--------------------------------------
BitField Name: RxFrmAislRdiLEn3
BitField Type: RW
BitField Desc: Enable/disable the insertion of RDI-L at TOH insertion when AIS_L
condition detected at line 3. 1: Enable 0: Disable
BitField Bits: [27]
--------------------------------------*/
#define cAf6_glbrfmaisrdifwd_reg_RxFrmAislRdiLEn3_Mask                                                  cBit27
#define cAf6_glbrfmaisrdifwd_reg_RxFrmAislRdiLEn3_Shift                                                     27

/*--------------------------------------
BitField Name: RxFrmTimRdiLEn3
BitField Type: RW
BitField Desc: Enable/disable the insertion of RDI-L at TOH insertion when TIM
condition detected at line 3. 1: Enable 0: Disable
BitField Bits: [26]
--------------------------------------*/
#define cAf6_glbrfmaisrdifwd_reg_RxFrmTimRdiLEn3_Mask                                                   cBit26
#define cAf6_glbrfmaisrdifwd_reg_RxFrmTimRdiLEn3_Shift                                                      26

/*--------------------------------------
BitField Name: RxFrmLofRdiLEn3
BitField Type: RW
BitField Desc: Enable/disable the insertion of RDI-L at TOH insertion when LOF
condition detected at line 3. 1: Enable 0: Disable
BitField Bits: [25]
--------------------------------------*/
#define cAf6_glbrfmaisrdifwd_reg_RxFrmLofRdiLEn3_Mask                                                   cBit25
#define cAf6_glbrfmaisrdifwd_reg_RxFrmLofRdiLEn3_Shift                                                      25

/*--------------------------------------
BitField Name: RxFrmLosRdiLEn3
BitField Type: RW
BitField Desc: Enable/disable the insertion of RDI-L at TOH insertion when LOS
condition detected at line 3. 1: Enable 0: Disable
BitField Bits: [24]
--------------------------------------*/
#define cAf6_glbrfmaisrdifwd_reg_RxFrmLosRdiLEn3_Mask                                                   cBit24
#define cAf6_glbrfmaisrdifwd_reg_RxFrmLosRdiLEn3_Shift                                                      24

/*--------------------------------------
BitField Name: RxFrmAislRdiLEn2
BitField Type: RW
BitField Desc: Enable/disable the insertion of RDI-L at TOH insertion when AIS_L
condition detected at line 2. 1: Enable 0: Disable
BitField Bits: [23]
--------------------------------------*/
#define cAf6_glbrfmaisrdifwd_reg_RxFrmAislRdiLEn2_Mask                                                  cBit23
#define cAf6_glbrfmaisrdifwd_reg_RxFrmAislRdiLEn2_Shift                                                     23

/*--------------------------------------
BitField Name: RxFrmTimRdiLEn2
BitField Type: RW
BitField Desc: Enable/disable the insertion of RDI-L at TOH insertion when TIM
condition detected at line 2. 1: Enable 0: Disable
BitField Bits: [22]
--------------------------------------*/
#define cAf6_glbrfmaisrdifwd_reg_RxFrmTimRdiLEn2_Mask                                                   cBit22
#define cAf6_glbrfmaisrdifwd_reg_RxFrmTimRdiLEn2_Shift                                                      22

/*--------------------------------------
BitField Name: RxFrmLofRdiLEn2
BitField Type: RW
BitField Desc: Enable/disable the insertion of RDI-L at TOH insertion when LOF
condition detected at line 2. 1: Enable 0: Disable
BitField Bits: [21]
--------------------------------------*/
#define cAf6_glbrfmaisrdifwd_reg_RxFrmLofRdiLEn2_Mask                                                   cBit21
#define cAf6_glbrfmaisrdifwd_reg_RxFrmLofRdiLEn2_Shift                                                      21

/*--------------------------------------
BitField Name: RxFrmLosRdiLEn2
BitField Type: RW
BitField Desc: Enable/disable the insertion of RDI-L at TOH insertion when LOS
condition detected at line 2. 1: Enable 0: Disable
BitField Bits: [20]
--------------------------------------*/
#define cAf6_glbrfmaisrdifwd_reg_RxFrmLosRdiLEn2_Mask                                                   cBit20
#define cAf6_glbrfmaisrdifwd_reg_RxFrmLosRdiLEn2_Shift                                                      20

/*--------------------------------------
BitField Name: RxFrmAislRdiLEn1
BitField Type: RW
BitField Desc: Enable/disable the insertion of RDI-L at TOH insertion when AIS_L
condition detected at line 1. 1: Enable 0: Disable
BitField Bits: [19]
--------------------------------------*/
#define cAf6_glbrfmaisrdifwd_reg_RxFrmAislRdiLEn1_Mask                                                  cBit19
#define cAf6_glbrfmaisrdifwd_reg_RxFrmAislRdiLEn1_Shift                                                     19

/*--------------------------------------
BitField Name: RxFrmTimRdiLEn1
BitField Type: RW
BitField Desc: Enable/disable the insertion of RDI-L at TOH insertion when TIM
condition detected at line 1. 1: Enable 0: Disable
BitField Bits: [18]
--------------------------------------*/
#define cAf6_glbrfmaisrdifwd_reg_RxFrmTimRdiLEn1_Mask                                                   cBit18
#define cAf6_glbrfmaisrdifwd_reg_RxFrmTimRdiLEn1_Shift                                                      18

/*--------------------------------------
BitField Name: RxFrmLofRdiLEn1
BitField Type: RW
BitField Desc: Enable/disable the insertion of RDI-L at TOH insertion when LOF
condition detected at line 1. 1: Enable 0: Disable
BitField Bits: [17]
--------------------------------------*/
#define cAf6_glbrfmaisrdifwd_reg_RxFrmLofRdiLEn1_Mask                                                   cBit17
#define cAf6_glbrfmaisrdifwd_reg_RxFrmLofRdiLEn1_Shift                                                      17

/*--------------------------------------
BitField Name: RxFrmLosRdiLEn1
BitField Type: RW
BitField Desc: Enable/disable the insertion of RDI-L at TOH insertion when LOS
condition detected at line 1. 1: Enable 0: Disable
BitField Bits: [16]
--------------------------------------*/
#define cAf6_glbrfmaisrdifwd_reg_RxFrmLosRdiLEn1_Mask                                                   cBit16
#define cAf6_glbrfmaisrdifwd_reg_RxFrmLosRdiLEn1_Shift                                                      16

/*--------------------------------------
BitField Name: RxFrmAislAisPEn4
BitField Type: RW
BitField Desc: Enable/disable the insertion of P-AIS at STS pointer interpreter
when AIS_L condition detected at line 4. 1: Enable 0: Disable
BitField Bits: [15]
--------------------------------------*/
#define cAf6_glbrfmaisrdifwd_reg_RxFrmAislAisPEn4_Mask                                                  cBit15
#define cAf6_glbrfmaisrdifwd_reg_RxFrmAislAisPEn4_Shift                                                     15

/*--------------------------------------
BitField Name: RxFrmTimAisPEn4
BitField Type: RW
BitField Desc: Enable/disable the insertion of P-AIS at STS pointer interpreter
when TIM condition detected at line 4. 1: Enable 0: Disable
BitField Bits: [14]
--------------------------------------*/
#define cAf6_glbrfmaisrdifwd_reg_RxFrmTimAisPEn4_Mask                                                   cBit14
#define cAf6_glbrfmaisrdifwd_reg_RxFrmTimAisPEn4_Shift                                                      14

/*--------------------------------------
BitField Name: RxFrmLofAisPEn4
BitField Type: RW
BitField Desc: Enable/disable the insertion of P-AIS at STS pointer interpreter
when LOF condition detected at line 4. 1: Enable 0: Disable
BitField Bits: [13]
--------------------------------------*/
#define cAf6_glbrfmaisrdifwd_reg_RxFrmLofAisPEn4_Mask                                                   cBit13
#define cAf6_glbrfmaisrdifwd_reg_RxFrmLofAisPEn4_Shift                                                      13

/*--------------------------------------
BitField Name: RxFrmLosAisPEn4
BitField Type: RW
BitField Desc: Enable/disable the insertion of P-AIS at STS pointer interpreter
when LOS condition detected at line 4. 1: Enable 0: Disable
BitField Bits: [12]
--------------------------------------*/
#define cAf6_glbrfmaisrdifwd_reg_RxFrmLosAisPEn4_Mask                                                   cBit12
#define cAf6_glbrfmaisrdifwd_reg_RxFrmLosAisPEn4_Shift                                                      12

/*--------------------------------------
BitField Name: RxFrmAislAisPEn3
BitField Type: RW
BitField Desc: Enable/disable the insertion of P-AIS at STS pointer interpreter
when AIS_L condition detected at line 3. 1: Enable 0: Disable
BitField Bits: [11]
--------------------------------------*/
#define cAf6_glbrfmaisrdifwd_reg_RxFrmAislAisPEn3_Mask                                                  cBit11
#define cAf6_glbrfmaisrdifwd_reg_RxFrmAislAisPEn3_Shift                                                     11

/*--------------------------------------
BitField Name: RxFrmTimAisPEn3
BitField Type: RW
BitField Desc: Enable/disable the insertion of P-AIS at STS pointer interpreter
when TIM condition detected at line 3. 1: Enable 0: Disable
BitField Bits: [10]
--------------------------------------*/
#define cAf6_glbrfmaisrdifwd_reg_RxFrmTimAisPEn3_Mask                                                   cBit10
#define cAf6_glbrfmaisrdifwd_reg_RxFrmTimAisPEn3_Shift                                                      10

/*--------------------------------------
BitField Name: RxFrmLofAisPEn3
BitField Type: RW
BitField Desc: Enable/disable the insertion of P-AIS at STS pointer interpreter
when LOF condition detected at line 3. 1: Enable 0: Disable
BitField Bits: [9]
--------------------------------------*/
#define cAf6_glbrfmaisrdifwd_reg_RxFrmLofAisPEn3_Mask                                                    cBit9
#define cAf6_glbrfmaisrdifwd_reg_RxFrmLofAisPEn3_Shift                                                       9

/*--------------------------------------
BitField Name: RxFrmLosAisPEn3
BitField Type: RW
BitField Desc: Enable/disable the insertion of P-AIS at STS pointer interpreter
when LOS condition detected at line 3. 1: Enable 0: Disable
BitField Bits: [8]
--------------------------------------*/
#define cAf6_glbrfmaisrdifwd_reg_RxFrmLosAisPEn3_Mask                                                    cBit8
#define cAf6_glbrfmaisrdifwd_reg_RxFrmLosAisPEn3_Shift                                                       8

/*--------------------------------------
BitField Name: RxFrmAislAisPEn2
BitField Type: RW
BitField Desc: Enable/disable the insertion of P-AIS at STS pointer interpreter
when AIS_L condition detected at line 2. 1: Enable 0: Disable
BitField Bits: [7]
--------------------------------------*/
#define cAf6_glbrfmaisrdifwd_reg_RxFrmAislAisPEn2_Mask                                                   cBit7
#define cAf6_glbrfmaisrdifwd_reg_RxFrmAislAisPEn2_Shift                                                      7

/*--------------------------------------
BitField Name: RxFrmTimAisPEn2
BitField Type: RW
BitField Desc: Enable/disable the insertion of P-AIS at STS pointer interpreter
when TIM condition detected at line 2. 1: Enable 0: Disable
BitField Bits: [6]
--------------------------------------*/
#define cAf6_glbrfmaisrdifwd_reg_RxFrmTimAisPEn2_Mask                                                    cBit6
#define cAf6_glbrfmaisrdifwd_reg_RxFrmTimAisPEn2_Shift                                                       6

/*--------------------------------------
BitField Name: RxFrmLofAisPEn2
BitField Type: RW
BitField Desc: Enable/disable the insertion of P-AIS at STS pointer interpreter
when LOF condition detected at line 2. 1: Enable 0: Disable
BitField Bits: [5]
--------------------------------------*/
#define cAf6_glbrfmaisrdifwd_reg_RxFrmLofAisPEn2_Mask                                                    cBit5
#define cAf6_glbrfmaisrdifwd_reg_RxFrmLofAisPEn2_Shift                                                       5

/*--------------------------------------
BitField Name: RxFrmLosAisPEn2
BitField Type: RW
BitField Desc: Enable/disable the insertion of P-AIS at STS pointer interpreter
when LOS condition detected at line 2. 1: Enable 0: Disable
BitField Bits: [4]
--------------------------------------*/
#define cAf6_glbrfmaisrdifwd_reg_RxFrmLosAisPEn2_Mask                                                    cBit4
#define cAf6_glbrfmaisrdifwd_reg_RxFrmLosAisPEn2_Shift                                                       4

/*--------------------------------------
BitField Name: RxFrmAislAisPEn1
BitField Type: RW
BitField Desc: Enable/disable the insertion of P-AIS at STS pointer interpreter
when AIS_L condition detected at line 1. 1: Enable 0: Disable
BitField Bits: [3]
--------------------------------------*/
#define cAf6_glbrfmaisrdifwd_reg_RxFrmAislAisPEn1_Mask                                                   cBit3
#define cAf6_glbrfmaisrdifwd_reg_RxFrmAislAisPEn1_Shift                                                      3

/*--------------------------------------
BitField Name: RxFrmTimAisPEn1
BitField Type: RW
BitField Desc: Enable/disable the insertion of P-AIS at STS pointer interpreter
when TIM condition detected at line 1. 1: Enable 0: Disable
BitField Bits: [2]
--------------------------------------*/
#define cAf6_glbrfmaisrdifwd_reg_RxFrmTimAisPEn1_Mask                                                    cBit2
#define cAf6_glbrfmaisrdifwd_reg_RxFrmTimAisPEn1_Shift                                                       2

/*--------------------------------------
BitField Name: RxFrmLofAisPEn1
BitField Type: RW
BitField Desc: Enable/disable the insertion of P-AIS at STS pointer interpreter
when LOF condition detected at line 1. 1: Enable 0: Disable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_glbrfmaisrdifwd_reg_RxFrmLofAisPEn1_Mask                                                    cBit1
#define cAf6_glbrfmaisrdifwd_reg_RxFrmLofAisPEn1_Shift                                                       1

/*--------------------------------------
BitField Name: RxFrmLosAisPEn1
BitField Type: RW
BitField Desc: Enable/disable the insertion of P-AIS at STS pointer interpreter
when LOS condition detected at line 1. 1: Enable 0: Disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_glbrfmaisrdifwd_reg_RxFrmLosAisPEn1_Mask                                                    cBit0
#define cAf6_glbrfmaisrdifwd_reg_RxFrmLosAisPEn1_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Tx Framer Control
Reg Addr   : 0x00001
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the global configuration register for the Tx Framer

------------------------------------------------------------------------------*/
#define cAf6Reg_glbtfm_reg                                                                             0x00001

#define cAf6_glbtfm_reg_TxFrmOhBusDccEnbMask(lineId)                                      (cBit20 << (lineId))
#define cAf6_glbtfm_reg_TxFrmOhBusDccEnbShift(lineId)                                          (20 + (lineId))

/*--------------------------------------
BitField Name: TxLineOofFrc
BitField Type: RW
BitField Desc: Enable/disable force OOF for 4 tx lines. Bit[0] for line 0.
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_glbtfm_reg_TxLineOofFrc_Mask                                                            cBit19_16
#define cAf6_glbtfm_reg_TxLineOofFrc_Shift                                                                  16

/*--------------------------------------
BitField Name: TxLineB1ErrFrc
BitField Type: RW
BitField Desc: Enable/disable force B1 error for 4 tx lines. Bit[0] for line 0.
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_glbtfm_reg_TxLineB1ErrFrc_Mask                                                          cBit15_12
#define cAf6_glbtfm_reg_TxLineB1ErrFrc_Shift                                                                12

/*--------------------------------------
BitField Name: TxFrmScrEn
BitField Type: RW
BitField Desc: Enable/disable scrambling for 4 tx lines. Bit[0] for line 0. 1:
Enable 0: Disable
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_glbtfm_reg_TxFrmScrEn_Mask                                                               cBit11_8
#define cAf6_glbtfm_reg_TxFrmScrEn_Shift                                                                     8

/*--------------------------------------
BitField Name: TxFrmStmOcnMode
BitField Type: RW
BitField Desc: STM rate mode for Tx of 4 lines, each line use 2 bits.Bits[1:0]
for line 0 With OC48 mode, we can config to run with any port (1-4) but if one
of these ports is config as OC48, another ports MUST config is Disable. 0: OC48
1: OC12 2: OC3 3: Disable.
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_glbtfm_reg_TxFrmStmOcnMode_Mask                                                           cBit7_0
#define cAf6_glbtfm_reg_TxFrmStmOcnMode_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Tx Framer RDI-L Insertion Threshold Control
Reg Addr   : 0x00009
Reg Formula: 
    Where  : 
Reg Desc   : 
Configure the number of frames in which L-RDI will be inserted when an L-RDI event is triggered. And Pattern to insert into Z0 bytes. The register contains two numbers

------------------------------------------------------------------------------*/
#define cAf6Reg_glbrdiinsthr_reg                                                                       0x00009

/*--------------------------------------
BitField Name: TxFrmRdiInsThr2
BitField Type: RW
BitField Desc: Threshold 2 for SDH mode
BitField Bits: [28:24]
--------------------------------------*/
#define cAf6_glbrdiinsthr_reg_TxFrmRdiInsThr2_Mask                                                   cBit28_24
#define cAf6_glbrdiinsthr_reg_TxFrmRdiInsThr2_Shift                                                         24

/*--------------------------------------
BitField Name: TxFrmRdiInsThr1
BitField Type: RW
BitField Desc: Threshold 1 for Sonet mode
BitField Bits: [20:16]
--------------------------------------*/
#define cAf6_glbrdiinsthr_reg_TxFrmRdiInsThr1_Mask                                                   cBit20_16
#define cAf6_glbrdiinsthr_reg_TxFrmRdiInsThr1_Shift                                                         16

/*--------------------------------------
BitField Name: OCNTxZ0Pat2
BitField Type: RW
BitField Desc: Pattern 2 to insert into Z0 bytes.
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_glbrdiinsthr_reg_OCNTxZ0Pat2_Mask                                                        cBit15_8
#define cAf6_glbrdiinsthr_reg_OCNTxZ0Pat2_Shift                                                              8

/*--------------------------------------
BitField Name: OCNTxZ0Pat1
BitField Type: RW
BitField Desc: Pattern 1 to insert into Z0 bytes.
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_glbrdiinsthr_reg_OCNTxZ0Pat1_Mask                                                         cBit7_0
#define cAf6_glbrdiinsthr_reg_OCNTxZ0Pat1_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : OCN Global STS Pointer Interpreter Control
Reg Addr   : 0x00002
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the global configuration register for the STS Pointer Interpreter

------------------------------------------------------------------------------*/
#define cAf6Reg_glbspi_reg                                                                             0x00002

/*--------------------------------------
BitField Name: StsPiAdjCntSel
BitField Type: RW
BitField Desc: Select pointer adjustment counter at "OCN Rx PP STS/VC Pointer
interpreter pointer adjustment per channel counter" 1: Counter for N/P event
which is transfered to PLA after Virtual FIFO module 0: Counter for Pointer
interpreter
BitField Bits: [31]
--------------------------------------*/
#define cAf6_glbspi_reg_StsPiAdjCntSel_Mask                                                             cBit31
#define cAf6_glbspi_reg_StsPiAdjCntSel_Shift                                                                31

/*--------------------------------------
BitField Name: RxPgNorPtrThresh
BitField Type: RW
BitField Desc: Threshold of number of normal pointers between two contiguous
frames within pointer adjustments for Virtual fifo PG.
BitField Bits: [30:29]
--------------------------------------*/
#define cAf6_glbspi_reg_RxPgNorPtrThresh_Mask                                                        cBit30_29
#define cAf6_glbspi_reg_RxPgNorPtrThresh_Shift                                                              29

/*--------------------------------------
BitField Name: Hwdebug
BitField Type: RW
BitField Desc: Hwdebug
BitField Bits: [28]
--------------------------------------*/
#define cAf6_glbspi_reg_Hwdebug_Mask                                                                    cBit28
#define cAf6_glbspi_reg_Hwdebug_Shift                                                                       28

/*--------------------------------------
BitField Name: RxPgFlowThresh
BitField Type: RW
BitField Desc: Overflow/underflow threshold to resynchronize read/write pointer.
BitField Bits: [27:24]
--------------------------------------*/
#define cAf6_glbspi_reg_RxPgFlowThresh_Mask                                                          cBit27_24
#define cAf6_glbspi_reg_RxPgFlowThresh_Shift                                                                24

/*--------------------------------------
BitField Name: RxPgAdjThresh
BitField Type: RW
BitField Desc: Adjustment threshold to make a pointer increment/decrement.
BitField Bits: [23:20]
--------------------------------------*/
#define cAf6_glbspi_reg_RxPgAdjThresh_Mask                                                           cBit23_20
#define cAf6_glbspi_reg_RxPgAdjThresh_Shift                                                                 20

/*--------------------------------------
BitField Name: StsPiAisAisPEn
BitField Type: RW
BitField Desc: Enable/Disable forwarding P_AIS when AIS state is detected at STS
Pointer interpreter. 1: Enable 0: Disable
BitField Bits: [18]
--------------------------------------*/
#define cAf6_glbspi_reg_StsPiAisAisPEn_Mask                                                             cBit18
#define cAf6_glbspi_reg_StsPiAisAisPEn_Shift                                                                18

/*--------------------------------------
BitField Name: StsPiLopAisPEn
BitField Type: RW
BitField Desc: Enable/Disable forwarding P_AIS when LOP state is detected at STS
Pointer interpreter. 1: Enable 0: Disable
BitField Bits: [17]
--------------------------------------*/
#define cAf6_glbspi_reg_StsPiLopAisPEn_Mask                                                             cBit17
#define cAf6_glbspi_reg_StsPiLopAisPEn_Shift                                                                17

/*--------------------------------------
BitField Name: StsPiMajorMode
BitField Type: RW
BitField Desc: Majority mode for detecting increment/decrement at STS pointer
Interpreter. It is used for rule n of 5. 1: n = 3 0: n = 5
BitField Bits: [16]
--------------------------------------*/
#define cAf6_glbspi_reg_StsPiMajorMode_Mask                                                             cBit16
#define cAf6_glbspi_reg_StsPiMajorMode_Shift                                                                16

/*--------------------------------------
BitField Name: StsPiNorPtrThresh
BitField Type: RW
BitField Desc: Threshold of number of normal pointers between two contiguous
frames within pointer adjustments.
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_glbspi_reg_StsPiNorPtrThresh_Mask                                                       cBit13_12
#define cAf6_glbspi_reg_StsPiNorPtrThresh_Shift                                                             12

/*--------------------------------------
BitField Name: StsPiNdfPtrThresh
BitField Type: RW
BitField Desc: Threshold of number of contiguous NDF pointers for entering LOP
state at FSM.
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_glbspi_reg_StsPiNdfPtrThresh_Mask                                                        cBit11_8
#define cAf6_glbspi_reg_StsPiNdfPtrThresh_Shift                                                              8

/*--------------------------------------
BitField Name: StsPiBadPtrThresh
BitField Type: RW
BitField Desc: Threshold of number of contiguous invalid pointers for entering
LOP state at FSM.
BitField Bits: [7:4]
--------------------------------------*/
#define cAf6_glbspi_reg_StsPiBadPtrThresh_Mask                                                         cBit7_4
#define cAf6_glbspi_reg_StsPiBadPtrThresh_Shift                                                              4

/*--------------------------------------
BitField Name: StsPiPohAisType
BitField Type: RW
BitField Desc: Enable/disable STS POH defect types to downstream AIS in case of
terminating the related STS such as the STS carries VT/TU. [0]: Enable for TIM
defect [1]: Enable for Unequiped defect [2]: Enable for VC-AIS [3]: Enable for
PLM defect
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_glbspi_reg_StsPiPohAisType_Mask                                                           cBit3_0
#define cAf6_glbspi_reg_StsPiPohAisType_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : OCN Global VTTU Pointer Interpreter Control
Reg Addr   : 0x00003
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the global configuration register for the VTTU Pointer Interpreter

------------------------------------------------------------------------------*/
#define cAf6Reg_glbvpi_reg                                                                             0x00003

/*--------------------------------------
BitField Name: VtPiLomAisPEn
BitField Type: RW
BitField Desc: Enable/Disable forwarding AIS when LOM is detected. 1: Enable 0:
Disable
BitField Bits: [29]
--------------------------------------*/
#define cAf6_glbvpi_reg_VtPiLomAisPEn_Mask                                                              cBit29
#define cAf6_glbvpi_reg_VtPiLomAisPEn_Shift                                                                 29

/*--------------------------------------
BitField Name: VtPiLomInvlCntMod
BitField Type: RW
BitField Desc: H4 monitoring mode. 1: Expected H4 is current frame in the
validated sequence plus one. 0: Expected H4 is the last received value plus one.
BitField Bits: [28]
--------------------------------------*/
#define cAf6_glbvpi_reg_VtPiLomInvlCntMod_Mask                                                          cBit28
#define cAf6_glbvpi_reg_VtPiLomInvlCntMod_Shift                                                             28

/*--------------------------------------
BitField Name: VtPiLomGoodThresh
BitField Type: RW
BitField Desc: Threshold of number of contiguous frames with validated sequence
of multi framers in LOM state for condition to entering IM state.
BitField Bits: [27:24]
--------------------------------------*/
#define cAf6_glbvpi_reg_VtPiLomGoodThresh_Mask                                                       cBit27_24
#define cAf6_glbvpi_reg_VtPiLomGoodThresh_Shift                                                             24

/*--------------------------------------
BitField Name: VtPiLomInvlThresh
BitField Type: RW
BitField Desc: Threshold of number of contiguous frames with invalidated
sequence of multi framers in IM state  for condition to entering LOM state.
BitField Bits: [23:20]
--------------------------------------*/
#define cAf6_glbvpi_reg_VtPiLomInvlThresh_Mask                                                       cBit23_20
#define cAf6_glbvpi_reg_VtPiLomInvlThresh_Shift                                                             20

/*--------------------------------------
BitField Name: VtPiAisAisPEn
BitField Type: RW
BitField Desc: Enable/Disable forwarding AIS when AIS state is detected at VTTU
Pointer interpreter. 1: Enable 0: Disable
BitField Bits: [18]
--------------------------------------*/
#define cAf6_glbvpi_reg_VtPiAisAisPEn_Mask                                                              cBit18
#define cAf6_glbvpi_reg_VtPiAisAisPEn_Shift                                                                 18

/*--------------------------------------
BitField Name: VtPiLopAisPEn
BitField Type: RW
BitField Desc: Enable/Disable forwarding AIS when LOP state is detected at VTTU
Pointer interpreter. 1: Enable 0: Disable
BitField Bits: [17]
--------------------------------------*/
#define cAf6_glbvpi_reg_VtPiLopAisPEn_Mask                                                              cBit17
#define cAf6_glbvpi_reg_VtPiLopAisPEn_Shift                                                                 17

/*--------------------------------------
BitField Name: VtPiMajorMode
BitField Type: RW
BitField Desc: Majority mode detecting increment/decrement in VTTU pointer
Interpreter. It is used for rule n of 5. 1: n = 3 0: n = 5
BitField Bits: [16]
--------------------------------------*/
#define cAf6_glbvpi_reg_VtPiMajorMode_Mask                                                              cBit16
#define cAf6_glbvpi_reg_VtPiMajorMode_Shift                                                                 16

/*--------------------------------------
BitField Name: VtPiNorPtrThresh
BitField Type: RW
BitField Desc: Threshold of number of normal pointers between two contiguous
frames within pointer adjustments.
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_glbvpi_reg_VtPiNorPtrThresh_Mask                                                        cBit13_12
#define cAf6_glbvpi_reg_VtPiNorPtrThresh_Shift                                                              12

/*--------------------------------------
BitField Name: VtPiNdfPtrThresh
BitField Type: RW
BitField Desc: Threshold of number of contiguous NDF pointers for entering LOP
state at FSM.
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_glbvpi_reg_VtPiNdfPtrThresh_Mask                                                         cBit11_8
#define cAf6_glbvpi_reg_VtPiNdfPtrThresh_Shift                                                               8

/*--------------------------------------
BitField Name: VtPiBadPtrThresh
BitField Type: RW
BitField Desc: Threshold of number of contiguous invalid pointers for entering
LOP state at FSM.
BitField Bits: [7:4]
--------------------------------------*/
#define cAf6_glbvpi_reg_VtPiBadPtrThresh_Mask                                                          cBit7_4
#define cAf6_glbvpi_reg_VtPiBadPtrThresh_Shift                                                               4

/*--------------------------------------
BitField Name: VtPiPohAisType
BitField Type: RW
BitField Desc: Enable/disable VTTU POH defect types to downstream AIS in case of
terminating the related VTTU. [0]: Enable for TIM defect [1]: Enable for Un-
equipment defect [2]: Enable for VC-AIS [3]: Enable for PLM defect
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_glbvpi_reg_VtPiPohAisType_Mask                                                            cBit3_0
#define cAf6_glbvpi_reg_VtPiPohAisType_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Pointer Generator Control
Reg Addr   : 0x00004
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the global configuration register for the Tx Pointer Generator

------------------------------------------------------------------------------*/
#define cAf6Reg_glbtpg_reg                                                                             0x00004

/*--------------------------------------
BitField Name: TxPgNorPtrThresh
BitField Type: RW
BitField Desc: Threshold of number of normal pointers between two contiguous
frames to make a condition of pointer adjustments.
BitField Bits: [9:8]
--------------------------------------*/
#define cAf6_glbtpg_reg_TxPgNorPtrThresh_Mask                                                          cBit9_8
#define cAf6_glbtpg_reg_TxPgNorPtrThresh_Shift                                                               8

/*--------------------------------------
BitField Name: TxPgFlowThresh
BitField Type: RW
BitField Desc: Overflow/underflow threshold to resynchronize read/write pointer
of TxFiFo.
BitField Bits: [7:4]
--------------------------------------*/
#define cAf6_glbtpg_reg_TxPgFlowThresh_Mask                                                            cBit7_4
#define cAf6_glbtpg_reg_TxPgFlowThresh_Shift                                                                 4

/*--------------------------------------
BitField Name: TxPgAdjThresh
BitField Type: RW
BitField Desc: Adjustment threshold to make a condition of pointer
increment/decrement.
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_glbtpg_reg_TxPgAdjThresh_Mask                                                             cBit3_0
#define cAf6_glbtpg_reg_TxPgAdjThresh_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Loopback
Reg Addr   : 0x00005
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the global configuration register for internal loopback

------------------------------------------------------------------------------*/
#define cAf6Reg_glbloop_reg                                                                            0x00005

/*--------------------------------------
BitField Name: LineLocalLoopBack
BitField Type: RW
BitField Desc: Line local loopback, Bit#0 for line#0. Set 1 to enable loop.
BitField Bits: [7:4]
--------------------------------------*/
#define cAf6_glbloop_reg_LineLocalLoopBack_Mask                                                        cBit7_4
#define cAf6_glbloop_reg_LineLocalLoopBack_Shift                                                             4

/*--------------------------------------
BitField Name: HwDb
BitField Type: RW
BitField Desc: HW debug
BitField Bits: [3:1]
--------------------------------------*/
#define cAf6_glbloop_reg_HwDb_Mask                                                                     cBit3_1
#define cAf6_glbloop_reg_HwDb_Shift                                                                          1

/*--------------------------------------
BitField Name: LoLoopBack
BitField Type: RW
BitField Desc: LoBus Loopback . 1: Loopback in 0: Normal (No loopback)
BitField Bits: [0]
--------------------------------------*/
#define cAf6_glbloop_reg_LoLoopBack_Mask                                                                 cBit0
#define cAf6_glbloop_reg_LoLoopBack_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : OCN STS Pointer Interpreter Per Channel Control
Reg Addr   : 0x02000 - 0x0202f
Reg Formula: 0x02000 + StsId
    Where  : 
           + $StsId(0-47)
Reg Desc   : 
Each register is used to configure for STS pointer interpreter engines of the STS-1/VC-3.
Backdoor		: irxpp_stspp.rxpp_stspiramctl.array

------------------------------------------------------------------------------*/
#define cAf6Reg_spiramctl                                                                              0x02000

/*--------------------------------------
BitField Name: StsPiRemoteLoopBack
BitField Type: RW
BitField Desc: Enable/disable Remote Loopback. This bit will be set to 1 send to
CPU a STS-AIS alarm 1: Enable. 0: Disable.
BitField Bits: [15]
--------------------------------------*/
#define cAf6_spiramctl_StsPiRemoteLoopBack_Mask                                                         cBit15
#define cAf6_spiramctl_StsPiRemoteLoopBack_Shift                                                            15

/*--------------------------------------
BitField Name: StsPiChkLom
BitField Type: RW
BitField Desc: Enable/disable LOM checking. This field will be set to 1 when
payload type of VC3/VC4 includes any VC11/VC12 1: Enable. 0: Disable.
BitField Bits: [14]
--------------------------------------*/
#define cAf6_spiramctl_StsPiChkLom_Mask                                                                 cBit14
#define cAf6_spiramctl_StsPiChkLom_Shift                                                                    14

/*--------------------------------------
BitField Name: StsPiSSDetPatt
BitField Type: RW
BitField Desc: Configure pattern SS bits that is used to compare with the
extracted SS bits from receive direction.
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_spiramctl_StsPiSSDetPatt_Mask                                                           cBit13_12
#define cAf6_spiramctl_StsPiSSDetPatt_Shift                                                                 12

/*--------------------------------------
BitField Name: StsPiAisFrc
BitField Type: RW
BitField Desc: Forcing SFM to AIS state. 1: Force 0: Not force
BitField Bits: [11]
--------------------------------------*/
#define cAf6_spiramctl_StsPiAisFrc_Mask                                                                 cBit11
#define cAf6_spiramctl_StsPiAisFrc_Shift                                                                    11

/*--------------------------------------
BitField Name: StsPiSSDetEn
BitField Type: RW
BitField Desc: Enable/disable checking SS bits in STSPI state machine. 1: Enable
0: Disable
BitField Bits: [10]
--------------------------------------*/
#define cAf6_spiramctl_StsPiSSDetEn_Mask                                                                cBit10
#define cAf6_spiramctl_StsPiSSDetEn_Shift                                                                   10

/*--------------------------------------
BitField Name: StsPiAdjRule
BitField Type: RW
BitField Desc: Configure the rule for detecting adjustment condition. 1: The n
of 5 rule is selected. This mode is applied for SDH mode 0: The 8 of 10 rule is
selected. This mode is applied for SONET mode
BitField Bits: [9]
--------------------------------------*/
#define cAf6_spiramctl_StsPiAdjRule_Mask                                                                 cBit9
#define cAf6_spiramctl_StsPiAdjRule_Shift                                                                    9

/*--------------------------------------
BitField Name: StsPiStsSlvInd
BitField Type: RW
BitField Desc: This is used to configure STS is slaver or master. 1: Slaver.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_spiramctl_StsPiStsSlvInd_Mask                                                               cBit8
#define cAf6_spiramctl_StsPiStsSlvInd_Shift                                                                  8

/*--------------------------------------
BitField Name: StsPiStsMstId
BitField Type: RW
BitField Desc: This is the ID of the master STS-1 in the concatenation that
contains this STS-1.
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_spiramctl_StsPiStsMstId_Mask                                                              cBit5_0
#define cAf6_spiramctl_StsPiStsMstId_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : OCN STS Pointer Generator Per Channel Control
Reg Addr   : 0x03000 - 0x0302f
Reg Formula: 0x03000 + StsId
    Where  : 
           + $StsId(0-47)
Reg Desc   : 
Each register is used to configure for STS pointer Generator engines.
Backdoor		: itxpp_stspp.itxpp_spg.txpg_stspgctl.array

------------------------------------------------------------------------------*/
#define cAf6Reg_spgramctl                                                                              0x03000

/*--------------------------------------
BitField Name: StsPgStsSlvInd
BitField Type: RW
BitField Desc: This is used to configure STS is slaver or master. 1: Slaver.
BitField Bits: [16]
--------------------------------------*/
#define cAf6_spgramctl_StsPgStsSlvInd_Mask                                                              cBit16
#define cAf6_spgramctl_StsPgStsSlvInd_Shift                                                                 16

/*--------------------------------------
BitField Name: StsPgStsMstId
BitField Type: RW
BitField Desc: This is the ID of the master STS-1 in the concatenation that
contains this STS-1.
BitField Bits: [13:8]
--------------------------------------*/
#define cAf6_spgramctl_StsPgStsMstId_Mask                                                             cBit13_8
#define cAf6_spgramctl_StsPgStsMstId_Shift                                                                   8

/*--------------------------------------
BitField Name: StsPgB3BipErrFrc
BitField Type: RW
BitField Desc: Forcing B3 Bip error. 1: Force 0: Not force
BitField Bits: [7]
--------------------------------------*/
#define cAf6_spgramctl_StsPgB3BipErrFrc_Mask                                                             cBit7
#define cAf6_spgramctl_StsPgB3BipErrFrc_Shift                                                                7

/*--------------------------------------
BitField Name: StsPgLopFrc
BitField Type: RW
BitField Desc: Forcing LOP. 1: Force 0: Not force
BitField Bits: [6]
--------------------------------------*/
#define cAf6_spgramctl_StsPgLopFrc_Mask                                                                  cBit6
#define cAf6_spgramctl_StsPgLopFrc_Shift                                                                     6

/*--------------------------------------
BitField Name: StsPgUeqFrc
BitField Type: RW
BitField Desc: Forcing SFM to UEQ state. 1: Force 0: Not force
BitField Bits: [5]
--------------------------------------*/
#define cAf6_spgramctl_StsPgUeqFrc_Mask                                                                  cBit5
#define cAf6_spgramctl_StsPgUeqFrc_Shift                                                                     5

/*--------------------------------------
BitField Name: StsPgAisFrc
BitField Type: RW
BitField Desc: Forcing SFM to AIS state. 1: Force 0: Not force
BitField Bits: [4]
--------------------------------------*/
#define cAf6_spgramctl_StsPgAisFrc_Mask                                                                  cBit4
#define cAf6_spgramctl_StsPgAisFrc_Shift                                                                     4

/*--------------------------------------
BitField Name: StsPgSSInsPatt
BitField Type: RW
BitField Desc: Configure pattern SS bits that is used to insert to pointer
value.
BitField Bits: [3:2]
--------------------------------------*/
#define cAf6_spgramctl_StsPgSSInsPatt_Mask                                                             cBit3_2
#define cAf6_spgramctl_StsPgSSInsPatt_Shift                                                                  2

/*--------------------------------------
BitField Name: StsPgSSInsEn
BitField Type: RW
BitField Desc: Enable/disable SS bits insertion. 1: Enable 0: Disable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_spgramctl_StsPgSSInsEn_Mask                                                                 cBit1
#define cAf6_spgramctl_StsPgSSInsEn_Shift                                                                    1

/*--------------------------------------
BitField Name: StsPgPohIns
BitField Type: RW
BitField Desc: Enable/disable POH Insertion. High to enable insertion of POH.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_spgramctl_StsPgPohIns_Mask                                                                  cBit0
#define cAf6_spgramctl_StsPgPohIns_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx SXC Control
Reg Addr   : 0x04000 - 0x0412f
Reg Formula: 0x04000 + 256*LineId + StsId
    Where  : 
           + $LineId(0-1)
           + $StsId(0-47)
Reg Desc   : 
Each register is used for each outgoing STS (to PDH) of any line (2 lines - 1 Lo and 1 Ho) can be randomly configured to //connect  to any ingoing STS of any ingoing line (from TFI-5 line - 8 lines).
Backdoor		: isdhsxc.isxc_rxrd[0].rxsxcramctl.array

------------------------------------------------------------------------------*/
#define cAf6Reg_rxsxcramctl                                                                            0x04000

/*--------------------------------------
BitField Name: RxSxcLineId
BitField Type: RW
BitField Desc: Contains the ingoing LineID (0-1) (In this project only config to
0) or used to Configure loop back to PDH (If value LineID is 3). Disconnect
(output is all one) if value LineID is other value (recommend is 2).
BitField Bits: [9:8]
--------------------------------------*/
#define cAf6_rxsxcramctl_RxSxcLineId_Mask                                                              cBit9_8
#define cAf6_rxsxcramctl_RxSxcLineId_Shift                                                                   8

/*--------------------------------------
BitField Name: RxSxcStsId
BitField Type: RW
BitField Desc: Contains the ingoing STSID (0-47).
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_rxsxcramctl_RxSxcStsId_Mask                                                               cBit5_0
#define cAf6_rxsxcramctl_RxSxcStsId_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : OCN Tx SXC Control
Reg Addr   : 0x05000 - 0x0502f
Reg Formula: 0x05000 + StsId
    Where  : 
           + $StsId(0-47)
Reg Desc   : 
Each register is used for each outgoing STS (to TFI-5 Line) of any ingoing line (from PDH) (2 lines - 0 Lo and 1 Ho).
Backdoor		: isdhsxc.isxc_txrd.txxcramctl.array

------------------------------------------------------------------------------*/
#define cAf6Reg_txsxcramctl                                                                            0x05000

/*--------------------------------------
BitField Name: TxSxcLineId
BitField Type: RW
BitField Desc: Contains the ingoing LineID (0-1: 0 for Lo, 1 for Ho) or used to
Configure loop back to TFI-5 (If value LineID is 3). Disconnect (output is all
one) if value LineID is other value (recommend is 2).
BitField Bits: [9:8]
--------------------------------------*/
#define cAf6_txsxcramctl_TxSxcLineId_Mask                                                              cBit9_8
#define cAf6_txsxcramctl_TxSxcLineId_Shift                                                                   8

/*--------------------------------------
BitField Name: TxSxcStsId
BitField Type: RW
BitField Desc: Contains the ingoing STSID (0-47).
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_txsxcramctl_TxSxcStsId_Mask                                                               cBit5_0
#define cAf6_txsxcramctl_TxSxcStsId_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : OCN RXPP Per STS payload Control
Reg Addr   : 0x10000 - 0x1002f
Reg Formula: 0x10000 + StsId
    Where  : 
           + $StsId(0-47)
Reg Desc   : 
Each register is used to configure VT payload mode per STS.
Backdoor		: irxpp_vtpp.rxpp_demramctl.array

------------------------------------------------------------------------------*/
#define cAf6Reg_demramctl                                                                              0x10000

/*--------------------------------------
BitField Name: PiDemAisDownst
BitField Type: RW
BitField Desc: Enable/disable AIS downstream to Lo Bus. (Used for Remote
loopback function) 1: Enable. 0: Disable.
BitField Bits: [17]
--------------------------------------*/
#define cAf6_demramctl_PiDemAisDownst_Mask                                                              cBit17
#define cAf6_demramctl_PiDemAisDownst_Shift                                                                 17

/*--------------------------------------
BitField Name: PiDemStsTerm
BitField Type: RW
BitField Desc: Enable to terminate the related STS/VC. It means that STS POH
defects related to the STS/VC to generate AIS to downstream. Must be set to 1.
1: Enable 0: Disable
BitField Bits: [16]
--------------------------------------*/
#define cAf6_demramctl_PiDemStsTerm_Mask                                                                cBit16
#define cAf6_demramctl_PiDemStsTerm_Shift                                                                   16

/*--------------------------------------
BitField Name: PiDemSpeType
BitField Type: RW
BitField Desc: Configure types of SPE. 0: Disable processing pointers of all
VT/TUs in this SPE. 1: VC-3 type or STS SPE containing VT/TU exception TU-3 2:
TUG-3 type containing VT/TU exception TU-3 3: TUG-3 type containing TU-3.
BitField Bits: [15:14]
--------------------------------------*/
#define cAf6_demramctl_PiDemSpeType_Mask                                                             cBit15_14
#define cAf6_demramctl_PiDemSpeType_Shift                                                                   14

/*--------------------------------------
BitField Name: PiDemTug26Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #6. 0: TU11 1: TU12
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_demramctl_PiDemTug26Type_Mask                                                           cBit13_12
#define cAf6_demramctl_PiDemTug26Type_Shift                                                                 12

/*--------------------------------------
BitField Name: PiDemTug25Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #5.
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_demramctl_PiDemTug25Type_Mask                                                           cBit11_10
#define cAf6_demramctl_PiDemTug25Type_Shift                                                                 10

/*--------------------------------------
BitField Name: PiDemTug24Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #4.
BitField Bits: [9:8]
--------------------------------------*/
#define cAf6_demramctl_PiDemTug24Type_Mask                                                             cBit9_8
#define cAf6_demramctl_PiDemTug24Type_Shift                                                                  8

/*--------------------------------------
BitField Name: PiDemTug23Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #3.
BitField Bits: [7:6]
--------------------------------------*/
#define cAf6_demramctl_PiDemTug23Type_Mask                                                             cBit7_6
#define cAf6_demramctl_PiDemTug23Type_Shift                                                                  6

/*--------------------------------------
BitField Name: PiDemTug22Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #2.
BitField Bits: [5:4]
--------------------------------------*/
#define cAf6_demramctl_PiDemTug22Type_Mask                                                             cBit5_4
#define cAf6_demramctl_PiDemTug22Type_Shift                                                                  4

/*--------------------------------------
BitField Name: PiDemTug21Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #1.
BitField Bits: [3:2]
--------------------------------------*/
#define cAf6_demramctl_PiDemTug21Type_Mask                                                             cBit3_2
#define cAf6_demramctl_PiDemTug21Type_Shift                                                                  2

/*--------------------------------------
BitField Name: PiDemTug20Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #0.
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_demramctl_PiDemTug20Type_Mask                                                             cBit1_0
#define cAf6_demramctl_PiDemTug20Type_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : OCN VTTU Pointer Interpreter Per Channel Control
Reg Addr   : 0x10800 - 0x10fff
Reg Formula: 0x10800 + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $StsId(0-47):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
Each register is used to configure for VTTU pointer interpreter engines.
Backdoor		: irxpp_vtpp.rxpp_vpiramctl.array

------------------------------------------------------------------------------*/
#define cAf6Reg_vpiramctl                                                                              0x10800

/*--------------------------------------
BitField Name: VtPiRemoteLoopBack
BitField Type: RW
BitField Desc: Enable/disable Remote Loopback. This bit will be set to 1 send to
CPU a VT-AIS alarm 1: Enable. 0: Disable.
BitField Bits: [6]
--------------------------------------*/
#define cAf6_vpiramctl_VtPiRemoteLoopBack_Mask                                                           cBit6
#define cAf6_vpiramctl_VtPiRemoteLoopBack_Shift                                                              6

/*--------------------------------------
BitField Name: VtPiLoTerm
BitField Type: RW
BitField Desc: Enable to terminate the related VTTU. It means that VTTU POH
defects related to the VTTU to generate AIS to downstream.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_vpiramctl_VtPiLoTerm_Mask                                                                   cBit5
#define cAf6_vpiramctl_VtPiLoTerm_Shift                                                                      5

/*--------------------------------------
BitField Name: VtPiAisFrc
BitField Type: RW
BitField Desc: Forcing SFM to AIS state. 1: Force 0: Not force
BitField Bits: [4]
--------------------------------------*/
#define cAf6_vpiramctl_VtPiAisFrc_Mask                                                                   cBit4
#define cAf6_vpiramctl_VtPiAisFrc_Shift                                                                      4

/*--------------------------------------
BitField Name: VtPiSSDetPatt
BitField Type: RW
BitField Desc: Configure pattern SS bits that is used to compare with the
extracted SS bits from receive direction.
BitField Bits: [3:2]
--------------------------------------*/
#define cAf6_vpiramctl_VtPiSSDetPatt_Mask                                                              cBit3_2
#define cAf6_vpiramctl_VtPiSSDetPatt_Shift                                                                   2

/*--------------------------------------
BitField Name: VtPiSSDetEn
BitField Type: RW
BitField Desc: Enable/disable checking SS bits in PI State Machine. 1: Enable 0:
Disable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_vpiramctl_VtPiSSDetEn_Mask                                                                  cBit1
#define cAf6_vpiramctl_VtPiSSDetEn_Shift                                                                     1

/*--------------------------------------
BitField Name: VtPiAdjRule
BitField Type: RW
BitField Desc: Configure the rule for detecting adjustment condition. 1: The n
of 5 rule is selected. This mode is applied for SDH mode 0: The 8 of 10 rule is
selected. This mode is applied for SONET mode
BitField Bits: [0]
--------------------------------------*/
#define cAf6_vpiramctl_VtPiAdjRule_Mask                                                                  cBit0
#define cAf6_vpiramctl_VtPiAdjRule_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : OCN TXPP Per STS Multiplexing Control
Reg Addr   : 0x20000 - 0x2002f
Reg Formula: 0x20000 + StsId
    Where  : 
           + $StsId(0-47)
Reg Desc   : 
Each register is used to configure VT payload mode per STS at Tx pointer generator.
Backdoor		: itxpp_vtpp.itxpp_vpg.txpg_pgdmctl.array

------------------------------------------------------------------------------*/
#define cAf6Reg_pgdemramctl                                                                            0x20000

/*--------------------------------------
BitField Name: PgDemSpeType
BitField Type: RW
BitField Desc: Configure types of SPE. 0: Disable processing pointers of all
VT/TUs in this SPE. 1: VC-3 type or STS SPE containing VT/TU exception TU-3 2:
TUG-3 type containing VT/TU exception TU-3 3: TUG-3 type containing TU-3.
BitField Bits: [15:14]
--------------------------------------*/
#define cAf6_pgdemramctl_PgDemSpeType_Mask                                                           cBit15_14
#define cAf6_pgdemramctl_PgDemSpeType_Shift                                                                 14

/*--------------------------------------
BitField Name: PgDemTug26Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #6. 0: TU11 1: TU12
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_pgdemramctl_PgDemTug26Type_Mask                                                         cBit13_12
#define cAf6_pgdemramctl_PgDemTug26Type_Shift                                                               12

/*--------------------------------------
BitField Name: PgDemTug25Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #5.
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_pgdemramctl_PgDemTug25Type_Mask                                                         cBit11_10
#define cAf6_pgdemramctl_PgDemTug25Type_Shift                                                               10

/*--------------------------------------
BitField Name: PgDemTug24Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #4.
BitField Bits: [9:8]
--------------------------------------*/
#define cAf6_pgdemramctl_PgDemTug24Type_Mask                                                           cBit9_8
#define cAf6_pgdemramctl_PgDemTug24Type_Shift                                                                8

/*--------------------------------------
BitField Name: PgDemTug23Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #3.
BitField Bits: [7:6]
--------------------------------------*/
#define cAf6_pgdemramctl_PgDemTug23Type_Mask                                                           cBit7_6
#define cAf6_pgdemramctl_PgDemTug23Type_Shift                                                                6

/*--------------------------------------
BitField Name: PgDemTug22Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #2.
BitField Bits: [5:4]
--------------------------------------*/
#define cAf6_pgdemramctl_PgDemTug22Type_Mask                                                           cBit5_4
#define cAf6_pgdemramctl_PgDemTug22Type_Shift                                                                4

/*--------------------------------------
BitField Name: PgDemTug21Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #1.
BitField Bits: [3:2]
--------------------------------------*/
#define cAf6_pgdemramctl_PgDemTug21Type_Mask                                                           cBit3_2
#define cAf6_pgdemramctl_PgDemTug21Type_Shift                                                                2

/*--------------------------------------
BitField Name: PgDemTug20Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #0.
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_pgdemramctl_PgDemTug20Type_Mask                                                           cBit1_0
#define cAf6_pgdemramctl_PgDemTug20Type_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : OCN VTTU Pointer Generator Per Channel Control
Reg Addr   : 0x20800 - 0x20fff
Reg Formula: 0x20800 + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $StsId(0-47):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
Each register is used to configure for VTTU pointer Generator engines.
Backdoor		: itxpp_vtpp.itxpp_vpg.txpg_pgctl.array

------------------------------------------------------------------------------*/
#define cAf6Reg_vpgramctl                                                                              0x20800

/*--------------------------------------
BitField Name: VtPgBipErrFrc
BitField Type: RW
BitField Desc: Forcing Bip error. 1: Force 0: Not force
BitField Bits: [7]
--------------------------------------*/
#define cAf6_vpgramctl_VtPgBipErrFrc_Mask                                                                cBit7
#define cAf6_vpgramctl_VtPgBipErrFrc_Shift                                                                   7

/*--------------------------------------
BitField Name: VtPgLopFrc
BitField Type: RW
BitField Desc: Forcing LOP. 1: Force 0: Not force
BitField Bits: [6]
--------------------------------------*/
#define cAf6_vpgramctl_VtPgLopFrc_Mask                                                                   cBit6
#define cAf6_vpgramctl_VtPgLopFrc_Shift                                                                      6

/*--------------------------------------
BitField Name: VtPgUeqFrc
BitField Type: RW
BitField Desc: Forcing SFM to UEQ state. 1: Force 0: Not force
BitField Bits: [5]
--------------------------------------*/
#define cAf6_vpgramctl_VtPgUeqFrc_Mask                                                                   cBit5
#define cAf6_vpgramctl_VtPgUeqFrc_Shift                                                                      5

/*--------------------------------------
BitField Name: VtPgAisFrc
BitField Type: RW
BitField Desc: Forcing SFM to AIS state. 1: Force 0: Not force
BitField Bits: [4]
--------------------------------------*/
#define cAf6_vpgramctl_VtPgAisFrc_Mask                                                                   cBit4
#define cAf6_vpgramctl_VtPgAisFrc_Shift                                                                      4

/*--------------------------------------
BitField Name: VtPgSSInsPatt
BitField Type: RW
BitField Desc: Configure pattern SS bits that is used to insert to Pointer
value.
BitField Bits: [3:2]
--------------------------------------*/
#define cAf6_vpgramctl_VtPgSSInsPatt_Mask                                                              cBit3_2
#define cAf6_vpgramctl_VtPgSSInsPatt_Shift                                                                   2

/*--------------------------------------
BitField Name: VtPgSSInsEn
BitField Type: RW
BitField Desc: Enable/disable SS bits insertion. 1: Enable 0: Disable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_vpgramctl_VtPgSSInsEn_Mask                                                                  cBit1
#define cAf6_vpgramctl_VtPgSSInsEn_Shift                                                                     1

/*--------------------------------------
BitField Name: VtPgPohIns
BitField Type: RW
BitField Desc: Enable/ disable POH Insertion. High to enable insertion of POH.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_vpgramctl_VtPgPohIns_Mask                                                                   cBit0
#define cAf6_vpgramctl_VtPgPohIns_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx STS/VC per Alarm Interrupt Status
Reg Addr   : 0x02140 - 0x0216f
Reg Formula: 0x02140 + StsId
    Where  : 
           + $StsId(0-47)
Reg Desc   : 
This is the per Alarm interrupt status of STS/VC pointer interpreter.  %%
Each register is used to store 6 sticky bits for 6 alarms in the STS/VC.

------------------------------------------------------------------------------*/
#define cAf6Reg_upstschstkram                                                                          0x02140

/*--------------------------------------
BitField Name: StsPiStsConcDetIntr
BitField Type: W1C
BitField Desc: Set to 1 while an Concatenation Detection event is detected at
STS/VC pointer interpreter. This event doesn't raise interrupt.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_upstschstkram_StsPiStsConcDetIntr_Mask                                                      cBit5
#define cAf6_upstschstkram_StsPiStsConcDetIntr_Shift                                                         5

/*--------------------------------------
BitField Name: StsPiStsNewDetIntr
BitField Type: W1C
BitField Desc: Set to 1 while an New Pointer Detection event is detected at
STS/VC pointer interpreter. This event doesn't raise interrupt.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_upstschstkram_StsPiStsNewDetIntr_Mask                                                       cBit4
#define cAf6_upstschstkram_StsPiStsNewDetIntr_Shift                                                          4

/*--------------------------------------
BitField Name: StsPiStsNdfIntr
BitField Type: W1C
BitField Desc: Set to 1 while an NDF event is detected at STS/VC pointer
interpreter. This event doesn't raise interrupt.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_upstschstkram_StsPiStsNdfIntr_Mask                                                          cBit3
#define cAf6_upstschstkram_StsPiStsNdfIntr_Shift                                                             3

/*--------------------------------------
BitField Name: StsPiCepUneqStatePChgIntr
BitField Type: W1C
BitField Desc: Set to 1  while there is change in CEP Un-equip Path in the
related STS/VC to generate an interrupt. Read the OCN Rx STS/VC per Alarm
Current Status register of the related STS/VC to know the STS/VC whether in CEP
Un-equip Path state or not.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_upstschstkram_StsPiCepUneqStatePChgIntr_Mask                                                cBit2
#define cAf6_upstschstkram_StsPiCepUneqStatePChgIntr_Shift                                                   2

/*--------------------------------------
BitField Name: StsPiStsAISStateChgIntr
BitField Type: W1C
BitField Desc: Set to 1 while there is change in AIS state in the related STS/VC
to generate an interrupt. Read the OCN Rx STS/VC per Alarm Current Status
register of the related STS/VC to know the STS/VC whether in AIS state or not.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_upstschstkram_StsPiStsAISStateChgIntr_Mask                                                  cBit1
#define cAf6_upstschstkram_StsPiStsAISStateChgIntr_Shift                                                     1

/*--------------------------------------
BitField Name: StsPiStsLopStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 to while there is change in LOP state in the related STS/VC
to generate an interrupt. Read the OCN Rx STS/VC per Alarm Current Status
register of the related STS/VC to know the STS/VC whether in LOP state or not.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upstschstkram_StsPiStsLopStateChgIntr_Mask                                                  cBit0
#define cAf6_upstschstkram_StsPiStsLopStateChgIntr_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx STS/VC per Alarm Current Status
Reg Addr   : 0x02180 - 0x021af
Reg Formula: 0x02180 + StsId
    Where  : 
           + $StsId(0-47)
Reg Desc   : 
This is the per Alarm current status of STS/VC pointer interpreter.  %%
Each register is used to store 3 bits to store current status of 3 alarms in the STS/VC.

------------------------------------------------------------------------------*/
#define cAf6Reg_upstschstaram                                                                          0x02180

/*--------------------------------------
BitField Name: StsPiStsCepUneqPCurStatus
BitField Type: RO
BitField Desc: CEP Un-eqip Path current status in the related STS/VC. When it
changes for 0 to 1 or vice versa, the  StsPiStsCepUeqPStateChgIntr bit in the
OCN Rx STS/VC per Alarm Interrupt Status register of the related STS/VC is set.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_upstschstaram_StsPiStsCepUneqPCurStatus_Mask                                                cBit2
#define cAf6_upstschstaram_StsPiStsCepUneqPCurStatus_Shift                                                   2

/*--------------------------------------
BitField Name: StsPiStsAisCurStatus
BitField Type: RO
BitField Desc: AIS current status in the related STS/VC. When it changes for 0
to 1 or vice versa, the  StsPiStsAISStateChgIntr bit in the OCN Rx STS/VC per
Alarm Interrupt Status register of the related STS/VC is set.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_upstschstaram_StsPiStsAisCurStatus_Mask                                                     cBit1
#define cAf6_upstschstaram_StsPiStsAisCurStatus_Shift                                                        1

/*--------------------------------------
BitField Name: StsPiStsLopCurStatus
BitField Type: RO
BitField Desc: LOP current status in the related STS/VC. When it changes for 0
to 1 or vice versa, the  StsPiStsLopStateChgIntr bit in the OCN Rx STS/VC per
Alarm Interrupt Status register of the related STS/VC is set.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upstschstaram_StsPiStsLopCurStatus_Mask                                                     cBit0
#define cAf6_upstschstaram_StsPiStsLopCurStatus_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx VT/TU per Alarm Interrupt Status
Reg Addr   : 0x12800 - 0x12fff
Reg Formula: 0x12800 + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $StsId(0-47):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This is the per Alarm interrupt status of VT/TU pointer interpreter . Each register is used to store 5 sticky bits for 5 alarms in the VT/TU.

------------------------------------------------------------------------------*/
#define cAf6Reg_upvtchstkram                                                                           0x12800

/*--------------------------------------
BitField Name: VtPiStsNewDetIntr
BitField Type: W1C
BitField Desc: Set to 1 while an New Pointer Detection event is detected at
VT/TU pointer interpreter. This event doesn't raise interrupt.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_upvtchstkram_VtPiStsNewDetIntr_Mask                                                         cBit4
#define cAf6_upvtchstkram_VtPiStsNewDetIntr_Shift                                                            4

/*--------------------------------------
BitField Name: VtPiStsNdfIntr
BitField Type: W1C
BitField Desc: Set to 1 while an NDF event is detected at VT/TU pointer
interpreter. This event doesn't raise interrupt.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_upvtchstkram_VtPiStsNdfIntr_Mask                                                            cBit3
#define cAf6_upvtchstkram_VtPiStsNdfIntr_Shift                                                               3

/*--------------------------------------
BitField Name: VtPiStsCepUneqVStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 to while there is change in Unequip state in the related
VT/TU to generate an interrupt. Read the OCN Rx VT/TU per Alarm Current Status
register of the related VT/TU to know the VT/TU whether in Uneqip state or not.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_upvtchstkram_VtPiStsCepUneqVStateChgIntr_Mask                                               cBit2
#define cAf6_upvtchstkram_VtPiStsCepUneqVStateChgIntr_Shift                                                  2

/*--------------------------------------
BitField Name: VtPiStsAISStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 to while there is change in AIS state in the related VT/TU
to generate an interrupt. Read the OCN Rx VT/TU per Alarm Current Status
register of the related VT/TU to know the VT/TU whether in LOP state or not.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_upvtchstkram_VtPiStsAISStateChgIntr_Mask                                                    cBit1
#define cAf6_upvtchstkram_VtPiStsAISStateChgIntr_Shift                                                       1

/*--------------------------------------
BitField Name: VtPiStsLopStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 to while there is change in LOP state in the related VT/TU
to generate an interrupt. Read the OCN Rx VT/TU per Alarm Current Status
register of the related VT/TU to know the VT/TU whether in AIS state or not.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upvtchstkram_VtPiStsLopStateChgIntr_Mask                                                    cBit0
#define cAf6_upvtchstkram_VtPiStsLopStateChgIntr_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx VT/TU per Alarm Current Status
Reg Addr   : 0x13000 - 0x13fff
Reg Formula: 0x13000 + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $StsId(0-47):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This is the per Alarm current status of VT/TU pointer interpreter. Each register is used to store 3 bits to store current status of 3 alarms in the VT/TU.

------------------------------------------------------------------------------*/
#define cAf6Reg_upvtchstaram                                                                           0x13000

/*--------------------------------------
BitField Name: VtPiStsCepUneqCurStatus
BitField Type: RO
BitField Desc: Unequip current status in the related VT/TU. When it changes for
0 to 1 or vice versa, the VtPiStsCepUneqVStateChgIntr bit in the OCN Rx VT/TU
per Alarm Interrupt Status register of the related VT/TU is set.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_upvtchstaram_VtPiStsCepUneqCurStatus_Mask                                                   cBit2
#define cAf6_upvtchstaram_VtPiStsCepUneqCurStatus_Shift                                                      2

/*--------------------------------------
BitField Name: VtPiStsAisCurStatus
BitField Type: RO
BitField Desc: AIS current status in the related VT/TU. When it changes for 0 to
1 or vice versa, the VtPiStsAISStateChgIntr bit in the OCN Rx VT/TU per Alarm
Interrupt Status register of the related VT/TU is set.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_upvtchstaram_VtPiStsAisCurStatus_Mask                                                       cBit1
#define cAf6_upvtchstaram_VtPiStsAisCurStatus_Shift                                                          1

/*--------------------------------------
BitField Name: VtPiStsLopCurStatus
BitField Type: RO
BitField Desc: LOP current status in the related VT/TU. When it changes for 0 to
1 or vice versa, the VtPiStsLopStateChgIntr bit in the OCN Rx VT/TU per Alarm
Interrupt Status register of the related VT/TU is set.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upvtchstaram_VtPiStsLopCurStatus_Mask                                                       cBit0
#define cAf6_upvtchstaram_VtPiStsLopCurStatus_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx PP STS/VC Pointer interpreter pointer adjustment per channel counter
Reg Addr   : 0x02080 - 0x020ff
Reg Formula: 0x02080 + 64*AdjMode + StsId
    Where  : 
           + $AdjMode(0-1)
           + $StsId(0-47)
Reg Desc   : 
Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in STS/VC pointer interpreter. These counters are in saturation mode

------------------------------------------------------------------------------*/
#define cAf6Reg_adjcntperstsram                                                                        0x02080

/*--------------------------------------
BitField Name: RxPpStsPiPtAdjCnt
BitField Type: RC
BitField Desc: The pointer Increment counter or decrease counter. Bit [6] of
address is used to indicate that the counter is pointer increment or decrement
counter. The counter will stop at maximum value (0x3FFFF).
BitField Bits: [17:0]
--------------------------------------*/
#define cAf6_adjcntperstsram_RxPpStsPiPtAdjCnt_Mask                                                   cBit17_0
#define cAf6_adjcntperstsram_RxPpStsPiPtAdjCnt_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx PP VT/TU Pointer interpreter pointer adjustment per channel counter
Reg Addr   : 0x11000 - 0x11fff
Reg Formula: 0x11000 + 2048*AdjMode + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $AdjMode(0-1)
           + $StsId(0-47):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in VT/TU pointer interpreter. These counters are in saturation mode

------------------------------------------------------------------------------*/
#define cAf6Reg_adjcntperstkram                                                                        0x11000

/*--------------------------------------
BitField Name: RxPpStsPiPtAdjCnt
BitField Type: RC
BitField Desc: The pointer Increment counter or decrease counter. Bit [11] of
address is used to indicate that the counter is pointer increment or decrement
counter. The counter will stop at maximum value (0x3FFFF).
BitField Bits: [17:0]
--------------------------------------*/
#define cAf6_adjcntperstkram_RxPpStsPiPtAdjCnt_Mask                                                   cBit17_0
#define cAf6_adjcntperstkram_RxPpStsPiPtAdjCnt_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : OCN TxPg STS per Alarm Interrupt Status
Reg Addr   : 0x03140 - 0x0316f
Reg Formula: 0x03140 +  StsId
    Where  : 
           + $StsId(0-47)
Reg Desc   : 
This is the per Alarm interrupt status of STS pointer generator . Each register is used to store 3 sticky bits for 3 alarms

------------------------------------------------------------------------------*/
#define cAf6Reg_stspgstkram                                                                            0x03140

/*--------------------------------------
BitField Name: StsPgAisIntr
BitField Type: W1C
BitField Desc: Set to 1 while an AIS status event (at h2pos) is detected at Tx
STS Pointer Generator.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_stspgstkram_StsPgAisIntr_Mask                                                               cBit3
#define cAf6_stspgstkram_StsPgAisIntr_Shift                                                                  3

/*--------------------------------------
BitField Name: StsPgFiFoOvfIntr
BitField Type: W1C
BitField Desc: Set to 1 while an FIFO Overflowed event is detected at Tx STS
Pointer Generator.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_stspgstkram_StsPgFiFoOvfIntr_Mask                                                           cBit2
#define cAf6_stspgstkram_StsPgFiFoOvfIntr_Shift                                                              2

/*--------------------------------------
BitField Name: StsPgNdfIntr
BitField Type: W1C
BitField Desc: Set to 1 while an NDF status event (at h2pos) is detected at Tx
STS Pointer Generator.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_stspgstkram_StsPgNdfIntr_Mask                                                               cBit1
#define cAf6_stspgstkram_StsPgNdfIntr_Shift                                                                  1


/*------------------------------------------------------------------------------
Reg Name   : OCN TxPg STS pointer adjustment per channel counter
Reg Addr   : 0x03080 - 0x030af
Reg Formula: 0x03080 + 64*AdjMode + StsId
    Where  : 
           + $AdjMode(0-1)
           + $StsId(0-47)
Reg Desc   : 
Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in STS pointer generator. These counters are in saturation mode

------------------------------------------------------------------------------*/
#define cAf6Reg_adjcntpgperstsram                                                                      0x03080

/*--------------------------------------
BitField Name: StsPgPtAdjCnt
BitField Type: RC
BitField Desc: The pointer Increment counter or decrease counter. Bit [11] of
address is used to indicate that the counter is pointer increment or decrement
counter. The counter will stop at maximum value (0x3FFFF).
BitField Bits: [17:0]
--------------------------------------*/
#define cAf6_adjcntpgperstsram_StsPgPtAdjCnt_Mask                                                     cBit17_0
#define cAf6_adjcntpgperstsram_StsPgPtAdjCnt_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : OCN TxPg VTTU per Alarm Interrupt Status
Reg Addr   : 0x22800 - 0x22fff
Reg Formula: 0x22800 + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $StsId(0-47):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This is the per Alarm interrupt status of STS/VT/TU pointer generator . Each register is used to store 3 sticky bits for 3 alarms

------------------------------------------------------------------------------*/
#define cAf6Reg_vtpgstkram                                                                             0x22800

/*--------------------------------------
BitField Name: VtPgAisIntr
BitField Type: W1C
BitField Desc: Set to 1 while an AIS status event (at h2pos) is detected at Tx
VTTU Pointer Generator.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_vtpgstkram_VtPgAisIntr_Mask                                                                 cBit3
#define cAf6_vtpgstkram_VtPgAisIntr_Shift                                                                    3

/*--------------------------------------
BitField Name: VtPgFiFoOvfIntr
BitField Type: W1C
BitField Desc: Set to 1 while an FIFO Overflowed event is detected at Tx VTTU
Pointer Generator.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_vtpgstkram_VtPgFiFoOvfIntr_Mask                                                             cBit2
#define cAf6_vtpgstkram_VtPgFiFoOvfIntr_Shift                                                                2

/*--------------------------------------
BitField Name: VtPgNdfIntr
BitField Type: W1C
BitField Desc: Set to 1 while an NDF status event (at h2pos) is detected at Tx
VTTU Pointer Generator.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_vtpgstkram_VtPgNdfIntr_Mask                                                                 cBit1
#define cAf6_vtpgstkram_VtPgNdfIntr_Shift                                                                    1


/*------------------------------------------------------------------------------
Reg Name   : OCN TxPg VTTU pointer adjustment per channel counter
Reg Addr   : 0x21000 - 0x21fff
Reg Formula: 0x21000 + 2048*AdjMode + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $AdjMode(0-1)
           + $StsId(0-47):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in VTTU pointer generator. These counters are in saturation mode

------------------------------------------------------------------------------*/
#define cAf6Reg_adjcntpgpervtram                                                                       0x21000

/*--------------------------------------
BitField Name: VtpgPtAdjCnt
BitField Type: RC
BitField Desc: The pointer Increment counter or decrease counter. Bit [11] of
address is used to indicate that the counter is pointer increment or decrement
counter. The counter will stop at maximum value (0x3FFFF).
BitField Bits: [17:0]
--------------------------------------*/
#define cAf6_adjcntpgpervtram_VtpgPtAdjCnt_Mask                                                       cBit17_0
#define cAf6_adjcntpgpervtram_VtpgPtAdjCnt_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx High Order Map concatenate configuration
Reg Addr   : 0x06000 - 0x0602f
Reg Formula: 0x06000 + StsId
    Where  : 
           + $StsId(0-47)
Reg Desc   : 
Each register is used to configure concatenation for each STS to High Order Map path.
Backdoor		: irxpp_outho.ohoramctl.array

------------------------------------------------------------------------------*/
#define cAf6Reg_rxhomapramctl                                                                          0x06000

/*--------------------------------------
BitField Name: HoMapAisDowns
BitField Type: RW
BitField Desc: Enable/disable AIS downstream to Ho Bus. (Used for Remote
loopback function) 1: Enable. 0: Disable.
BitField Bits: [9]
--------------------------------------*/
#define cAf6_rxhomapramctl_HoMapAisDowns_Mask                                                            cBit9
#define cAf6_rxhomapramctl_HoMapAisDowns_Shift                                                               9

/*--------------------------------------
BitField Name: HoMapStsSlvInd
BitField Type: RW
BitField Desc: This is used to configure STS is slaver or master. 1: Slaver.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_rxhomapramctl_HoMapStsSlvInd_Mask                                                           cBit8
#define cAf6_rxhomapramctl_HoMapStsSlvInd_Shift                                                              8

/*--------------------------------------
BitField Name: HoMapStsMstId
BitField Type: RW
BitField Desc: This is the ID of the master STS-1 in the concatenation that
contains this STS-1.
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_rxhomapramctl_HoMapStsMstId_Mask                                                          cBit5_0
#define cAf6_rxhomapramctl_HoMapStsMstId_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : OCN Tx Framer Per Channel Control
Reg Addr   : 0x01000 - 0x01300
Reg Formula: 0x01000 + 256*LineId
    Where  : 
           + $LineId(0-3)
Reg Desc   : 
Each register is used to configure operation modes for Tx framer engine and APS pattern inserted into APS bytes (K1 and K2 bytes) of the SONET/SDH line being relative with the address of the address of the register.

------------------------------------------------------------------------------*/
#define cAf6Reg_tfmregctl                                                                              0x01000

/*--------------------------------------
BitField Name: OCNTxS1Pat
BitField Type: RW
BitField Desc: Pattern to insert into S1 byte.
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_tfmregctl_OCNTxS1Pat_Mask                                                               cBit31_24
#define cAf6_tfmregctl_OCNTxS1Pat_Shift                                                                     24

/*--------------------------------------
BitField Name: OCNTxApsPat
BitField Type: RW
BitField Desc: Pattern to insert into APS bytes (K1, K2).
BitField Bits: [23:8]
--------------------------------------*/
#define cAf6_tfmregctl_OCNTxApsPat_Mask                                                               cBit23_8
#define cAf6_tfmregctl_OCNTxApsPat_Shift                                                                     8

/*--------------------------------------
BitField Name: OCNTxS1LDis
BitField Type: RW
BitField Desc: S1 insertion disable 1: Disable 0: Enable.
BitField Bits: [7]
--------------------------------------*/
#define cAf6_tfmregctl_OCNTxS1LDis_Mask                                                                  cBit7
#define cAf6_tfmregctl_OCNTxS1LDis_Shift                                                                     7

/*--------------------------------------
BitField Name: OCNTxApsDis
BitField Type: RW
BitField Desc: Disable to process APS 1: Disable 0: Enable.
BitField Bits: [6]
--------------------------------------*/
#define cAf6_tfmregctl_OCNTxApsDis_Mask                                                                  cBit6
#define cAf6_tfmregctl_OCNTxApsDis_Shift                                                                     6

/*--------------------------------------
BitField Name: OCNTxReiLDis
BitField Type: RW
BitField Desc: Auto REI_L insertion disable 1: Disable inserting REI_L
automatically. 0: Enable automatically inserting REI_L.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_tfmregctl_OCNTxReiLDis_Mask                                                                 cBit5
#define cAf6_tfmregctl_OCNTxReiLDis_Shift                                                                    5

/*--------------------------------------
BitField Name: OCNTxRdiLDis
BitField Type: RW
BitField Desc: Auto RDI_L insertion disable 1: Disable inserting RDI_L
automatically. 0: Enable automatically inserting RDI_L.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_tfmregctl_OCNTxRdiLDis_Mask                                                                 cBit4
#define cAf6_tfmregctl_OCNTxRdiLDis_Shift                                                                    4

/*--------------------------------------
BitField Name: OCNTxAutoB2Dis
BitField Type: RW
BitField Desc: Auto B2 disable 1: Disable inserting calculated B2 values into B2
positions automatically. 0: Enable automatically inserting calculated B2 values
into B2 positions.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_tfmregctl_OCNTxAutoB2Dis_Mask                                                               cBit3
#define cAf6_tfmregctl_OCNTxAutoB2Dis_Shift                                                                  3

/*--------------------------------------
BitField Name: OCNTxRdiLThresSel
BitField Type: RW
BitField Desc: Select the number of frames being inserted RDI-L defects when
receive direction requests generating RDI-L at transmit direction. 1: Threshold2
is selected. 0: Threshold1 is selected.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_tfmregctl_OCNTxRdiLThresSel_Mask                                                            cBit2
#define cAf6_tfmregctl_OCNTxRdiLThresSel_Shift                                                               2

/*--------------------------------------
BitField Name: OCNTxRdiLFrc
BitField Type: RW
BitField Desc: RDI-L force. 1: Force RDI-L defect into transmit data. 0: Not
force RDI-L
BitField Bits: [1]
--------------------------------------*/
#define cAf6_tfmregctl_OCNTxRdiLFrc_Mask                                                                 cBit1
#define cAf6_tfmregctl_OCNTxRdiLFrc_Shift                                                                    1

/*--------------------------------------
BitField Name: OCNTxAisLFrc
BitField Type: RW
BitField Desc: AIS-L force. 1: Force AIS-L defect into transmit data. 0: Not
force AIS-L
BitField Bits: [0]
--------------------------------------*/
#define cAf6_tfmregctl_OCNTxAisLFrc_Mask                                                                 cBit0
#define cAf6_tfmregctl_OCNTxAisLFrc_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : OCN Tx J0 Insertion Buffer
Reg Addr   : 0x01010 - 0x0131f
Reg Formula: 0x01010 + 256*LineId + DwId
    Where  : 
           + $LineId(0-3)
           + DwId(0-15): Double word (4bytes) ID
Reg Desc   : 
Each register is used to store one J0 double word of in 64-byte message of the SONET/SDH line being relative with the address of the address of the register inserted into J0 positions.

------------------------------------------------------------------------------*/
#define cAf6Reg_tfmj0insctl                                                                            0x01010

/*--------------------------------------
BitField Name: OCNTxJ0
BitField Type: RW
BitField Desc: J0 pattern for insertion.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_tfmj0insctl_OCNTxJ0_Mask                                                                 cBit31_0
#define cAf6_tfmj0insctl_OCNTxJ0_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Global K1 Stable Monitoring Threshold Control
Reg Addr   : 0x07000
Reg Formula: 
    Where  : 
Reg Desc   : 
Configure thresholds for monitoring change of K1 state at TOH monitoring. There are two thresholds for each kind of threshold.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohglbk1stbthr_reg                                                                     0x07000

/*--------------------------------------
BitField Name: TohK1StbThr2
BitField Type: RW
BitField Desc: The second threshold for detecting stable K1 status.
BitField Bits: [10:8]
--------------------------------------*/
#define cAf6_tohglbk1stbthr_reg_TohK1StbThr2_Mask                                                     cBit10_8
#define cAf6_tohglbk1stbthr_reg_TohK1StbThr2_Shift                                                           8

/*--------------------------------------
BitField Name: TohK1StbThr1
BitField Type: RW
BitField Desc: The first threshold for detecting stable K1 status.
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_tohglbk1stbthr_reg_TohK1StbThr1_Mask                                                      cBit2_0
#define cAf6_tohglbk1stbthr_reg_TohK1StbThr1_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Global K2 Stable Monitoring Threshold Control
Reg Addr   : 0x07001
Reg Formula: 
    Where  : 
Reg Desc   : 
Configure thresholds for monitoring change of K2 state at TOH monitoring. There are two thresholds for each kind of threshold.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohglbk2stbthr_reg                                                                     0x07001

/*--------------------------------------
BitField Name: TohK2StbThr2
BitField Type: RW
BitField Desc: The second threshold for detecting stable K2 status.
BitField Bits: [10:8]
--------------------------------------*/
#define cAf6_tohglbk2stbthr_reg_TohK2StbThr2_Mask                                                     cBit10_8
#define cAf6_tohglbk2stbthr_reg_TohK2StbThr2_Shift                                                           8

/*--------------------------------------
BitField Name: TohK2StbThr1
BitField Type: RW
BitField Desc: The first threshold for detecting stable K2 status.
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_tohglbk2stbthr_reg_TohK2StbThr1_Mask                                                      cBit2_0
#define cAf6_tohglbk2stbthr_reg_TohK2StbThr1_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Global S1 Stable Monitoring Threshold Control
Reg Addr   : 0x07002
Reg Formula: 
    Where  : 
Reg Desc   : 
Configure thresholds for monitoring change of S1 state at TOH monitoring. There are two thresholds for each kind of threshold.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohglbs1stbthr_reg                                                                     0x07002

/*--------------------------------------
BitField Name: TohS1StbThr2
BitField Type: RW
BitField Desc: The second threshold for detecting stable S1 status.
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_tohglbs1stbthr_reg_TohS1StbThr2_Mask                                                     cBit11_8
#define cAf6_tohglbs1stbthr_reg_TohS1StbThr2_Shift                                                           8

/*--------------------------------------
BitField Name: TohS1StbThr1
BitField Type: RW
BitField Desc: The first threshold for detecting stable S1 status.
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_tohglbs1stbthr_reg_TohS1StbThr1_Mask                                                      cBit3_0
#define cAf6_tohglbs1stbthr_reg_TohS1StbThr1_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Global RDI_L Detecting Threshold Control
Reg Addr   : 0x07003
Reg Formula: 
    Where  : 
Reg Desc   : 
Configure thresholds for detecting RDI_L at TOH monitoring. There are two thresholds for each kind of threshold.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohglbrdidetthr_reg                                                                    0x07003

/*--------------------------------------
BitField Name: TohRdiDetThr2
BitField Type: RW
BitField Desc: The second threshold for detecting RDI_L.
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_tohglbrdidetthr_reg_TohRdiDetThr2_Mask                                                   cBit11_8
#define cAf6_tohglbrdidetthr_reg_TohRdiDetThr2_Shift                                                         8

/*--------------------------------------
BitField Name: TohRdiDetThr1
BitField Type: RW
BitField Desc: The first threshold for detecting RDI_L.
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_tohglbrdidetthr_reg_TohRdiDetThr1_Mask                                                    cBit3_0
#define cAf6_tohglbrdidetthr_reg_TohRdiDetThr1_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Global AIS_L Detecting Threshold Control
Reg Addr   : 0x07004
Reg Formula: 
    Where  : 
Reg Desc   : 
Configure thresholds for detecting AIS_L at TOH monitoring. There are two thresholds for each kind of threshold.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohglbaisdetthr_reg                                                                    0x07004

/*--------------------------------------
BitField Name: TohAisDetThr2
BitField Type: RW
BitField Desc: The second threshold for detecting AIS_L.
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_tohglbaisdetthr_reg_TohAisDetThr2_Mask                                                   cBit11_8
#define cAf6_tohglbaisdetthr_reg_TohAisDetThr2_Shift                                                         8

/*--------------------------------------
BitField Name: TohAisDetThr1
BitField Type: RW
BitField Desc: The first threshold for detecting AIS_L.
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_tohglbaisdetthr_reg_TohAisDetThr1_Mask                                                    cBit3_0
#define cAf6_tohglbaisdetthr_reg_TohAisDetThr1_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Global K1 Sampling Threshold Control
Reg Addr   : 0x07005
Reg Formula: 
    Where  : 
Reg Desc   : 
Configure thresholds for sampling K1 bytes to detect APS defect at TOH monitoring. There are two thresholds.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohglbk1smpthr_reg                                                                     0x07005

/*--------------------------------------
BitField Name: TohK1SmpThr2
BitField Type: RW
BitField Desc: The second threshold for sampling K1 to detect APS defect.
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_tohglbk1smpthr_reg_TohK1SmpThr2_Mask                                                     cBit11_8
#define cAf6_tohglbk1smpthr_reg_TohK1SmpThr2_Shift                                                           8

/*--------------------------------------
BitField Name: TohK1SmpThr1
BitField Type: RW
BitField Desc: The first threshold for sampling K1 to detect APS defect.
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_tohglbk1smpthr_reg_TohK1SmpThr1_Mask                                                      cBit3_0
#define cAf6_tohglbk1smpthr_reg_TohK1SmpThr1_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Global Error Counter Control
Reg Addr   : 0x07006
Reg Formula: 
    Where  : 
Reg Desc   : 
Configure mode for counters in TOH monitoring.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohglberrcntmod_reg                                                                    0x07006

/*--------------------------------------
BitField Name: TohReiErrCntMod
BitField Type: RW
BitField Desc: REI counter Mode. 1: Saturation mode 0: Roll over mode
BitField Bits: [2]
--------------------------------------*/
#define cAf6_tohglberrcntmod_reg_TohReiErrCntMod_Mask                                                    cBit2
#define cAf6_tohglberrcntmod_reg_TohReiErrCntMod_Shift                                                       2

/*--------------------------------------
BitField Name: TohB2ErrCntMod
BitField Type: RW
BitField Desc: B2 counter Mode. 1: Saturation mode 0: Roll over mode
BitField Bits: [1]
--------------------------------------*/
#define cAf6_tohglberrcntmod_reg_TohB2ErrCntMod_Mask                                                     cBit1
#define cAf6_tohglberrcntmod_reg_TohB2ErrCntMod_Shift                                                        1

/*--------------------------------------
BitField Name: TohB1ErrCntMod
BitField Type: RW
BitField Desc: B1 counter Mode. 1: Saturation mode 0: Roll over mode
BitField Bits: [0]
--------------------------------------*/
#define cAf6_tohglberrcntmod_reg_TohB1ErrCntMod_Mask                                                     cBit0
#define cAf6_tohglberrcntmod_reg_TohB1ErrCntMod_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Montoring Affect Control
Reg Addr   : 0x07007
Reg Formula: 
    Where  : 
Reg Desc   : 
Configure affective mode for TOH monitoring.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohglbaffen_reg                                                                        0x07007

/*--------------------------------------
BitField Name: TohAisAffStbMon
BitField Type: RW
BitField Desc: AIS affects to Stable monitoring status of the line at which LOF
or LOS is detected 1: Disable 0: Enable
BitField Bits: [3]
--------------------------------------*/
#define cAf6_tohglbaffen_reg_TohAisAffStbMon_Mask                                                        cBit3
#define cAf6_tohglbaffen_reg_TohAisAffStbMon_Shift                                                           3

/*--------------------------------------
BitField Name: TohAisAffRdilMon
BitField Type: RW
BitField Desc: AIS affects to RDI-L monitoring status of the line at which LOF
or LOS is detected. 1: Disable 0: Enable
BitField Bits: [2]
--------------------------------------*/
#define cAf6_tohglbaffen_reg_TohAisAffRdilMon_Mask                                                       cBit2
#define cAf6_tohglbaffen_reg_TohAisAffRdilMon_Shift                                                          2

/*--------------------------------------
BitField Name: TohAisAffAislMon
BitField Type: RW
BitField Desc: AIS affects to AIS-L monitoring  status of the line at which LOF
or LOS is detected. 1: Disable 0: Enable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_tohglbaffen_reg_TohAisAffAislMon_Mask                                                       cBit1
#define cAf6_tohglbaffen_reg_TohAisAffAislMon_Shift                                                          1

/*--------------------------------------
BitField Name: TohAisAffErrCnt
BitField Type: RW
BitField Desc: AIS affects to error counters (B1,B2,REI) of the line at which
LOF or LOS is detected. 1: Disable 0: Enable.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_tohglbaffen_reg_TohAisAffErrCnt_Mask                                                        cBit0
#define cAf6_tohglbaffen_reg_TohAisAffErrCnt_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Monitoring Per Line Control
Reg Addr   : 0x07010 - 0x07017
Reg Formula: 0x07010  + LineId
    Where  : 
           + $LineId(0-3)
Reg Desc   : 
Each register is used to configure for TOH monitoring engine of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohramctl                                                                              0x07010

/*--------------------------------------
BitField Name: RxAisLFrc
BitField Type: RW
BitField Desc: Rx AIS-L force. 1: Force. 0: Not force
BitField Bits: [9]
--------------------------------------*/
#define cAf6_tohramctl_RxAisLFrc_Mask                                                                    cBit9
#define cAf6_tohramctl_RxAisLFrc_Shift                                                                       9

/*--------------------------------------
BitField Name: B1ErrCntBlkMod
BitField Type: RW
BitField Desc: B1 counter Block Mode for fedding PM. 1: Block count mode 0: Bit
count mode
BitField Bits: [8]
--------------------------------------*/
#define cAf6_tohramctl_B1ErrCntBlkMod_Mask                                                               cBit8
#define cAf6_tohramctl_B1ErrCntBlkMod_Shift                                                                  8

/*--------------------------------------
BitField Name: B2ErrCntBlkMod
BitField Type: RW
BitField Desc: B2 counter Block Mode for fedding PM. 1: Block count mode 0: Bit
count mode
BitField Bits: [7]
--------------------------------------*/
#define cAf6_tohramctl_B2ErrCntBlkMod_Mask                                                               cBit7
#define cAf6_tohramctl_B2ErrCntBlkMod_Shift                                                                  7

/*--------------------------------------
BitField Name: ReiErrCntBlkMod
BitField Type: RW
BitField Desc: REI counter Block Mode for fedding PM. 1: Block count mode 0: Bit
count mode
BitField Bits: [6]
--------------------------------------*/
#define cAf6_tohramctl_ReiErrCntBlkMod_Mask                                                              cBit6
#define cAf6_tohramctl_ReiErrCntBlkMod_Shift                                                                 6

/*--------------------------------------
BitField Name: StbRdiisLThresSel
BitField Type: RW
BitField Desc: Select the thresholds for detecting RDI_L 1: Threshold 2 is
selected 0: Threshold 1 is selected
BitField Bits: [5]
--------------------------------------*/
#define cAf6_tohramctl_StbRdiisLThresSel_Mask                                                            cBit5
#define cAf6_tohramctl_StbRdiisLThresSel_Shift                                                               5

/*--------------------------------------
BitField Name: StbAisLThresSel
BitField Type: RW
BitField Desc: Select the thresholds for detecting AIS_L 1: Threshold 2 is
selected 0: Threshold 1 is selected
BitField Bits: [4]
--------------------------------------*/
#define cAf6_tohramctl_StbAisLThresSel_Mask                                                              cBit4
#define cAf6_tohramctl_StbAisLThresSel_Shift                                                                 4

/*--------------------------------------
BitField Name: K2StbMd
BitField Type: RW
BitField Desc: Select K2[7:3] or K2[7:0] to detect validated K2 value 1: K2[7:0]
value is selected 0: K2[7:3] value is selected
BitField Bits: [3]
--------------------------------------*/
#define cAf6_tohramctl_K2StbMd_Mask                                                                      cBit3
#define cAf6_tohramctl_K2StbMd_Shift                                                                         3

/*--------------------------------------
BitField Name: StbK1K2ThresSel
BitField Type: RW
BitField Desc: Select the thresholds for detecting stable or non-stable K1/K2
status 1: Threshold 2 is selected 0: Threshold 1 is selected
BitField Bits: [2]
--------------------------------------*/
#define cAf6_tohramctl_StbK1K2ThresSel_Mask                                                              cBit2
#define cAf6_tohramctl_StbK1K2ThresSel_Shift                                                                 2

/*--------------------------------------
BitField Name: StbS1ThresSel
BitField Type: RW
BitField Desc: Select the thresholds for detecting stable or non-stable S1
status 1: Threshold 2 is selected 0: Threshold 1 is selected
BitField Bits: [1]
--------------------------------------*/
#define cAf6_tohramctl_StbS1ThresSel_Mask                                                                cBit1
#define cAf6_tohramctl_StbS1ThresSel_Shift                                                                   1

/*--------------------------------------
BitField Name: SmpK1ThresSel
BitField Type: RW
BitField Desc: Select the sample threshold for detecting APS defect. 1:
Threshold 2 is selected 0: Threshold 1 is selected
BitField Bits: [0]
--------------------------------------*/
#define cAf6_tohramctl_SmpK1ThresSel_Mask                                                                cBit0
#define cAf6_tohramctl_SmpK1ThresSel_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Monitoring B1 Error Read Only Counter
Reg Addr   : 0x07100 - 0x07107
Reg Formula: 0x07100 + LineId
    Where  : 
           + LineId(0-3)
Reg Desc   : 
Each register is used to store B1 error read only counter of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohb1errrocnt                                                                          0x07100

/*--------------------------------------
BitField Name: B1ErrRoCnt
BitField Type: RO
BitField Desc: B1 Error Read Only Counter.
BitField Bits: [22:0]
--------------------------------------*/
#define cAf6_tohb1errrocnt_B1ErrRoCnt_Mask                                                            cBit22_0
#define cAf6_tohb1errrocnt_B1ErrRoCnt_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Monitoring B1 Error Read to Clear Counter
Reg Addr   : 0x07140 - 0x07147
Reg Formula: 0x07140 + LineId
    Where  : 
           + LineId(0-3)
Reg Desc   : 
Each register is used to store B1 error read to clear counter of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohb1errr2ccnt                                                                         0x07140

/*--------------------------------------
BitField Name: B1ErrR2cCnt
BitField Type: RC
BitField Desc: B1 Error Read To Clear Counter.
BitField Bits: [22:0]
--------------------------------------*/
#define cAf6_tohb1errr2ccnt_B1ErrR2cCnt_Mask                                                          cBit22_0
#define cAf6_tohb1errr2ccnt_B1ErrR2cCnt_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Monitoring B1 Block Error Read Only Counter
Reg Addr   : 0x07180 - 0x07187
Reg Formula: 0x07180 + LineId
    Where  : 
           + LineId(0-3)
Reg Desc   : 
Each register is used to store B1 Block error read only counter of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohb1blkerrrocnt                                                                       0x07180

/*--------------------------------------
BitField Name: B1BlkErrRoCnt
BitField Type: RO
BitField Desc: B1 Block Error Read Only Counter.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_tohb1blkerrrocnt_B1BlkErrRoCnt_Mask                                                      cBit15_0
#define cAf6_tohb1blkerrrocnt_B1BlkErrRoCnt_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Monitoring B1 Block Error Read to Clear Counter
Reg Addr   : 0x071c0 - 0x071c7
Reg Formula: 0x071c0 + LineId
    Where  : 
           + LineId(0-3)
Reg Desc   : 
Each register is used to store B1 Block error read to clear counter of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohb1blkerrr2ccnt                                                                      0x071c0

/*--------------------------------------
BitField Name: B1BlkErrR2cCnt
BitField Type: RC
BitField Desc: B1 Block Error Read To Clear Counter.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_tohb1blkerrr2ccnt_B1BlkErrR2cCnt_Mask                                                    cBit15_0
#define cAf6_tohb1blkerrr2ccnt_B1BlkErrR2cCnt_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Monitoring B2 Error Read Only Counter
Reg Addr   : 0x07108 - 0x0710f
Reg Formula: 0x07108 + LineId
    Where  : 
           + LineId(0-3)
Reg Desc   : 
Each register is used to store B2 error read only counter of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohb2errrocnt                                                                          0x07108

/*--------------------------------------
BitField Name: B2ErrRoCnt
BitField Type: RO
BitField Desc: B2 Error Read Only Counter.
BitField Bits: [22:0]
--------------------------------------*/
#define cAf6_tohb2errrocnt_B2ErrRoCnt_Mask                                                            cBit22_0
#define cAf6_tohb2errrocnt_B2ErrRoCnt_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Monitoring B2 Error Read to Clear Counter
Reg Addr   : 0x07148 - 0x0714f
Reg Formula: 0x07148 + LineId
    Where  : 
           + LineId(0-3)
Reg Desc   : 
Each register is used to store B2 error read to clear counter of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohb2errr2ccnt                                                                         0x07148

/*--------------------------------------
BitField Name: B2ErrR2cCnt
BitField Type: RC
BitField Desc: B2 Error Read To Clear Counter.
BitField Bits: [22:0]
--------------------------------------*/
#define cAf6_tohb2errr2ccnt_B2ErrR2cCnt_Mask                                                          cBit22_0
#define cAf6_tohb2errr2ccnt_B2ErrR2cCnt_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Monitoring B2 Block Error Read Only Counter
Reg Addr   : 0x07188 - 0x0718f
Reg Formula: 0x07188 + LineId
    Where  : 
           + LineId(0-3)
Reg Desc   : 
Each register is used to store B2 Block error read only counter of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohb2blkerrrocnt                                                                       0x07188

/*--------------------------------------
BitField Name: B2BlkErrRoCnt
BitField Type: RO
BitField Desc: B2 Block Error Read Only Counter.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_tohb2blkerrrocnt_B2BlkErrRoCnt_Mask                                                      cBit15_0
#define cAf6_tohb2blkerrrocnt_B2BlkErrRoCnt_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Monitoring B2 Block Error Read to Clear Counter
Reg Addr   : 0x071c8 - 0x071cf
Reg Formula: 0x071c8 + LineId
    Where  : 
           + LineId(0-3)
Reg Desc   : 
Each register is used to store B2 Block error read to clear counter of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohb2blkerrr2ccnt                                                                      0x071c8

/*--------------------------------------
BitField Name: B2BlkErrR2cCnt
BitField Type: RC
BitField Desc: B2 Block Error Read To Clear Counter.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_tohb2blkerrr2ccnt_B2BlkErrR2cCnt_Mask                                                    cBit15_0
#define cAf6_tohb2blkerrr2ccnt_B2BlkErrR2cCnt_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Monitoring REI Error Read Only Counter
Reg Addr   : 0x07128 - 0x0712f
Reg Formula: 0x07128 + LineId
    Where  : 
           + LineId(0-3)
Reg Desc   : 
Each register is used to store REI error read only counter of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohreierrrocnt                                                                         0x07128

/*--------------------------------------
BitField Name: ReiErrRoCnt
BitField Type: RO
BitField Desc: REI Error Read Only Counter.
BitField Bits: [22:0]
--------------------------------------*/
#define cAf6_tohreierrrocnt_ReiErrRoCnt_Mask                                                          cBit22_0
#define cAf6_tohreierrrocnt_ReiErrRoCnt_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Monitoring REI Error Read to Clear Counter
Reg Addr   : 0x07168 - 0x0716f
Reg Formula: 0x07168 + LineId
    Where  : 
           + LineId(0-3)
Reg Desc   : 
Each register is used to store REI error read to clear counter of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohreierrr2ccnt                                                                        0x07168

/*--------------------------------------
BitField Name: ReiErrR2cCnt
BitField Type: RC
BitField Desc: REI Error Read To Clear Counter.
BitField Bits: [22:0]
--------------------------------------*/
#define cAf6_tohreierrr2ccnt_ReiErrR2cCnt_Mask                                                        cBit22_0
#define cAf6_tohreierrr2ccnt_ReiErrR2cCnt_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Monitoring REI Block Error Read Only Counter
Reg Addr   : 0x071a8 - 0x071af
Reg Formula: 0x071a8 + LineId
    Where  : 
           + LineId(0-3)
Reg Desc   : 
Each register is used to store REI Block error read only counter of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohreiblkerrrocnt                                                                      0x071a8

/*--------------------------------------
BitField Name: ReiBlkErrRoCnt
BitField Type: RO
BitField Desc: REI Block Error Read Only Counter.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_tohreiblkerrrocnt_ReiBlkErrRoCnt_Mask                                                    cBit15_0
#define cAf6_tohreiblkerrrocnt_ReiBlkErrRoCnt_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Monitoring REI Block Error Read to Clear Counter
Reg Addr   : 0x071e8 - 0x070ef
Reg Formula: 0x071e8 + LineId
    Where  : 
           + LineId(0-3)
Reg Desc   : 
Each register is used to store REI Block error read to clear counter of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohreiblkerrr2ccnt                                                                     0x071e8

/*--------------------------------------
BitField Name: ReiBlkErrR2cCnt
BitField Type: RC
BitField Desc: REI Block Error Read To Clear Counter.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_tohreiblkerrr2ccnt_ReiBlkErrR2cCnt_Mask                                                  cBit15_0
#define cAf6_tohreiblkerrr2ccnt_ReiBlkErrR2cCnt_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH K1 Monitoring Status
Reg Addr   : 0x07110 - 0x07117
Reg Formula: 0x07110 + LineId
    Where  : 
           + LineId(0-3)
Reg Desc   : 
Each register is used to store K1 Monitoring Status of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohk1monsta                                                                            0x07110

/*--------------------------------------
BitField Name: CurApsDef
BitField Type: RW
BitField Desc: current APS Defect. 1: APS defect is detected from APS bytes. 0:
APS defect is not detected from APS bytes.
BitField Bits: [23]
--------------------------------------*/
#define cAf6_tohk1monsta_CurApsDef_Mask                                                                 cBit23
#define cAf6_tohk1monsta_CurApsDef_Shift                                                                    23

/*--------------------------------------
BitField Name: K1SmpCnt
BitField Type: RW
BitField Desc: Sampling counter.
BitField Bits: [22:19]
--------------------------------------*/
#define cAf6_tohk1monsta_K1SmpCnt_Mask                                                               cBit22_19
#define cAf6_tohk1monsta_K1SmpCnt_Shift                                                                     19

/*--------------------------------------
BitField Name: SameK1Cnt
BitField Type: RW
BitField Desc: The number of same contiguous K1 bytes. It is held at StbK1Thr
value when the number of same contiguous K1 bytes is equal to or more than the
StbK1Thr value.In this case, K1 bytes are stable.
BitField Bits: [18:16]
--------------------------------------*/
#define cAf6_tohk1monsta_SameK1Cnt_Mask                                                              cBit18_16
#define cAf6_tohk1monsta_SameK1Cnt_Shift                                                                    16

/*--------------------------------------
BitField Name: K1StbVal
BitField Type: RW
BitField Desc: Stable K1 value. It is updated when detecting a new stable value.
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_tohk1monsta_K1StbVal_Mask                                                                cBit15_8
#define cAf6_tohk1monsta_K1StbVal_Shift                                                                      8

/*--------------------------------------
BitField Name: K1CurVal
BitField Type: RW
BitField Desc: Current K1 byte.
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_tohk1monsta_K1CurVal_Mask                                                                 cBit7_0
#define cAf6_tohk1monsta_K1CurVal_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH K2 Monitoring Status
Reg Addr   : 0x07118 - 0x0711f
Reg Formula: 0x07118 + LineId
    Where  : 
           + LineId(0-3)
Reg Desc   : 
Each register is used to store K2 Monitoring Status of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohk2monsta                                                                            0x07118

/*--------------------------------------
BitField Name: Internal
BitField Type: RW
BitField Desc: Internal.
BitField Bits: [28:21]
--------------------------------------*/
#define cAf6_tohk2monsta_Internal_Mask                                                               cBit28_21
#define cAf6_tohk2monsta_Internal_Shift                                                                     21

/*--------------------------------------
BitField Name: CurAisL
BitField Type: RW
BitField Desc: current AIS-L Defect. 1: AIS-L defect is detected from K2 bytes.
0: AIS-L defect is not detected from K2 bytes.
BitField Bits: [20]
--------------------------------------*/
#define cAf6_tohk2monsta_CurAisL_Mask                                                                   cBit20
#define cAf6_tohk2monsta_CurAisL_Shift                                                                      20

/*--------------------------------------
BitField Name: CurRdiL
BitField Type: RW
BitField Desc: current RDI-L Defect. 1: RDI-L defect is detected from K2 bytes.
0: RDI-L defect is not detected from K2 bytes.
BitField Bits: [19]
--------------------------------------*/
#define cAf6_tohk2monsta_CurRdiL_Mask                                                                   cBit19
#define cAf6_tohk2monsta_CurRdiL_Shift                                                                      19

/*--------------------------------------
BitField Name: SameK2Cnt
BitField Type: RW
BitField Desc: The number of same contiguous K2 bytes. It is held at StbK2Thr
value when the number of same contiguous K2 bytes is equal to or more than the
StbK2Thr value.In this case, K2 bytes are stable.
BitField Bits: [18:16]
--------------------------------------*/
#define cAf6_tohk2monsta_SameK2Cnt_Mask                                                              cBit18_16
#define cAf6_tohk2monsta_SameK2Cnt_Shift                                                                    16

/*--------------------------------------
BitField Name: K2StbVal
BitField Type: RW
BitField Desc: Stable K2 value. It is updated when detecting a new stable value.
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_tohk2monsta_K2StbVal_Mask                                                                cBit15_8
#define cAf6_tohk2monsta_K2StbVal_Shift                                                                      8

/*--------------------------------------
BitField Name: K2CurVal
BitField Type: RW
BitField Desc: Current K2 byte.
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_tohk2monsta_K2CurVal_Mask                                                                 cBit7_0
#define cAf6_tohk2monsta_K2CurVal_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH S1 Monitoring Status
Reg Addr   : 0x07120 - 0x07127
Reg Formula: 0x07120 + LineId
    Where  : 
           + LineId(0-3)
Reg Desc   : 
Each register is used to store S1 Monitoring Status of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohs1monsta                                                                            0x07120

/*--------------------------------------
BitField Name: SameS1Cnt
BitField Type: RW
BitField Desc: The number of same contiguous S1 bytes. It is held at StbS1Thr
value when the number of same contiguous S1 bytes is equal to or more than the
StbS1Thr value.In this case, S1 bytes are stable.
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_tohs1monsta_SameS1Cnt_Mask                                                              cBit19_16
#define cAf6_tohs1monsta_SameS1Cnt_Shift                                                                    16

/*--------------------------------------
BitField Name: S1StbVal
BitField Type: RW
BitField Desc: Stable S1 value. It is updated when detecting a new stable value.
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_tohs1monsta_S1StbVal_Mask                                                                cBit15_8
#define cAf6_tohs1monsta_S1StbVal_Shift                                                                      8

/*--------------------------------------
BitField Name: S1CurVal
BitField Type: RW
BitField Desc: Current S1 byte.
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_tohs1monsta_S1CurVal_Mask                                                                 cBit7_0
#define cAf6_tohs1monsta_S1CurVal_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx Line per Alarm Interrupt Enable Control
Reg Addr   : 0x07080 - 0x07087
Reg Formula: 0x07080 + LineId
    Where  : 
           + LineId(0-3)
Reg Desc   : 
This is the per Alarm interrupt enable of Rx framer and TOH monitoring. Each register is used to store 9 bits to enable interrupts when the related alarms in related line happen.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohintperalrenbctl                                                                     0x07080

/*--------------------------------------
BitField Name: TcaLStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable TCA-L state change event in the related line to
generate an interrupt.
BitField Bits: [12]
--------------------------------------*/
#define cAf6_tohintperalrenbctl_TcaLStateChgIntrEn_Mask                                                 cBit12
#define cAf6_tohintperalrenbctl_TcaLStateChgIntrEn_Shift                                                    12

/*--------------------------------------
BitField Name: SdLStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable SD-L state change event in the related line to
generate an interrupt.
BitField Bits: [11]
--------------------------------------*/
#define cAf6_tohintperalrenbctl_SdLStateChgIntrEn_Mask                                                  cBit11
#define cAf6_tohintperalrenbctl_SdLStateChgIntrEn_Shift                                                     11

/*--------------------------------------
BitField Name: SfLStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable SF-L state change event in the related line to
generate an interrupt.
BitField Bits: [10]
--------------------------------------*/
#define cAf6_tohintperalrenbctl_SfLStateChgIntrEn_Mask                                                  cBit10
#define cAf6_tohintperalrenbctl_SfLStateChgIntrEn_Shift                                                     10

/*--------------------------------------
BitField Name: TimLStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable TIM-L state change event in the related line to
generate an interrupt.
BitField Bits: [9]
--------------------------------------*/
#define cAf6_tohintperalrenbctl_TimLStateChgIntrEn_Mask                                                  cBit9
#define cAf6_tohintperalrenbctl_TimLStateChgIntrEn_Shift                                                     9

/*--------------------------------------
BitField Name: S1StbStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable S1 Stable state change event in the related line
to generate an interrupt.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_tohintperalrenbctl_S1StbStateChgIntrEn_Mask                                                 cBit8
#define cAf6_tohintperalrenbctl_S1StbStateChgIntrEn_Shift                                                    8

/*--------------------------------------
BitField Name: K1StbStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable K1 Stable state change event in the related line
to generate an interrupt.
BitField Bits: [7]
--------------------------------------*/
#define cAf6_tohintperalrenbctl_K1StbStateChgIntrEn_Mask                                                 cBit7
#define cAf6_tohintperalrenbctl_K1StbStateChgIntrEn_Shift                                                    7

/*--------------------------------------
BitField Name: ApsLStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable APS-L Defect Stable state change event in the
related line to generate an interrupt..
BitField Bits: [6]
--------------------------------------*/
#define cAf6_tohintperalrenbctl_ApsLStateChgIntrEn_Mask                                                  cBit6
#define cAf6_tohintperalrenbctl_ApsLStateChgIntrEn_Shift                                                     6

/*--------------------------------------
BitField Name: K2StbStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable K2 Stable state change event in the related line
to generate an interrupt.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_tohintperalrenbctl_K2StbStateChgIntrEn_Mask                                                 cBit5
#define cAf6_tohintperalrenbctl_K2StbStateChgIntrEn_Shift                                                    5

/*--------------------------------------
BitField Name: RdiLStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable RDI-L Defect Stable state change event in the
related line to generate an interrupt.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_tohintperalrenbctl_RdiLStateChgIntrEn_Mask                                                  cBit4
#define cAf6_tohintperalrenbctl_RdiLStateChgIntrEn_Shift                                                     4

/*--------------------------------------
BitField Name: AisLStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable AIS-L Defect Stable state change event in the
related line to generate an interrupt.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_tohintperalrenbctl_AisLStateChgIntrEn_Mask                                                  cBit3
#define cAf6_tohintperalrenbctl_AisLStateChgIntrEn_Shift                                                     3

/*--------------------------------------
BitField Name: OofStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable OOF Defect Stable state change event in the
related line to generate an interrupt.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_tohintperalrenbctl_OofStateChgIntrEn_Mask                                                   cBit2
#define cAf6_tohintperalrenbctl_OofStateChgIntrEn_Shift                                                      2

/*--------------------------------------
BitField Name: LofStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable LOF Defect Stable state change event in the
related line to generate an interrupt.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_tohintperalrenbctl_LofStateChgIntrEn_Mask                                                   cBit1
#define cAf6_tohintperalrenbctl_LofStateChgIntrEn_Shift                                                      1

/*--------------------------------------
BitField Name: LosStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable LOS Defect Stable state change event in the
related line to generate an interrupt.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_tohintperalrenbctl_LosStateChgIntrEn_Mask                                                   cBit0
#define cAf6_tohintperalrenbctl_LosStateChgIntrEn_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx Line per Alarm Interrupt Status
Reg Addr   : 0x07088 - 0x0708f
Reg Formula: 0x07088 + LineId
    Where  : 
           + LineId(0-3)
Reg Desc   : 
This is the per Alarm interrupt status of Rx framer and TOH monitoring. Each register is used to store 9 sticky bits for 9 alarms in the line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohintsta                                                                              0x07088

/*--------------------------------------
BitField Name: TcaLStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 while TCA-L state change detected in the related line, and
it is generated an interrupt if it is enabled.
BitField Bits: [12]
--------------------------------------*/
#define cAf6_tohintsta_TcaLStateChgIntr_Mask                                                            cBit12
#define cAf6_tohintsta_TcaLStateChgIntr_Shift                                                               12

/*--------------------------------------
BitField Name: SdLStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 while SD-L state change detected in the related line, and
it is generated an interrupt if it is enabled.
BitField Bits: [11]
--------------------------------------*/
#define cAf6_tohintsta_SdLStateChgIntr_Mask                                                             cBit11
#define cAf6_tohintsta_SdLStateChgIntr_Shift                                                                11

/*--------------------------------------
BitField Name: SfLStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 while SF-L state change detected in the related line, and
it is generated an interrupt if it is enabled.
BitField Bits: [10]
--------------------------------------*/
#define cAf6_tohintsta_SfLStateChgIntr_Mask                                                             cBit10
#define cAf6_tohintsta_SfLStateChgIntr_Shift                                                                10

/*--------------------------------------
BitField Name: TimLStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 while Tim-L state change detected in the related line, and
it is generated an interrupt if it is enabled.
BitField Bits: [9]
--------------------------------------*/
#define cAf6_tohintsta_TimLStateChgIntr_Mask                                                             cBit9
#define cAf6_tohintsta_TimLStateChgIntr_Shift                                                                9

/*--------------------------------------
BitField Name: S1StbStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 one new stable S1 value detected in the related line, and
it is generated an interrupt if it is enabled.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_tohintsta_S1StbStateChgIntr_Mask                                                            cBit8
#define cAf6_tohintsta_S1StbStateChgIntr_Shift                                                               8

/*--------------------------------------
BitField Name: K1StbStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 one new stable K1 value detected in the related line, and
it is generated an interrupt if it is enabled.
BitField Bits: [7]
--------------------------------------*/
#define cAf6_tohintsta_K1StbStateChgIntr_Mask                                                            cBit7
#define cAf6_tohintsta_K1StbStateChgIntr_Shift                                                               7

/*--------------------------------------
BitField Name: ApsLStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 while APS-L Defect state change event happens in the
related line, and it is generated an interrupt if it is enabled.
BitField Bits: [6]
--------------------------------------*/
#define cAf6_tohintsta_ApsLStateChgIntr_Mask                                                             cBit6
#define cAf6_tohintsta_ApsLStateChgIntr_Shift                                                                6

/*--------------------------------------
BitField Name: K2StbStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 one new stable K2 value detected in the related line, and
it is generated an interrupt if it is enabled.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_tohintsta_K2StbStateChgIntr_Mask                                                            cBit5
#define cAf6_tohintsta_K2StbStateChgIntr_Shift                                                               5

/*--------------------------------------
BitField Name: RdiLStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 while RDI-L Defect state change event happens in the
related line, and it is generated an interrupt if it is enabled.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_tohintsta_RdiLStateChgIntr_Mask                                                             cBit4
#define cAf6_tohintsta_RdiLStateChgIntr_Shift                                                                4

/*--------------------------------------
BitField Name: AisLStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 while AIS-L Defect state change event happens in the
related line, and it is generated an interrupt if it is enabled.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_tohintsta_AisLStateChgIntr_Mask                                                             cBit3
#define cAf6_tohintsta_AisLStateChgIntr_Shift                                                                3

/*--------------------------------------
BitField Name: OofStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 while OOF state change event happens in the related line,
and it is generated an interrupt if it is enabled.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_tohintsta_OofStateChgIntr_Mask                                                              cBit2
#define cAf6_tohintsta_OofStateChgIntr_Shift                                                                 2

/*--------------------------------------
BitField Name: LofStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 while LOF state change event happens in the related line,
and it is generated an interrupt if it is enabled.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_tohintsta_LofStateChgIntr_Mask                                                              cBit1
#define cAf6_tohintsta_LofStateChgIntr_Shift                                                                 1

/*--------------------------------------
BitField Name: LosStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 while LOS state change event happens in the related line,
and it is generated an interrupt if it is enabled.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_tohintsta_LosStateChgIntr_Mask                                                              cBit0
#define cAf6_tohintsta_LosStateChgIntr_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx Line per Alarm Current Status
Reg Addr   : 0x07090 - 0x07097
Reg Formula: 0x07090 + LineId
    Where  : 
           + LineId(0-3)
Reg Desc   : 
This is the per Alarm interrupt status of Rx framer and TOH monitoring. Each register is used to store 9 sticky bits for 9 alarms in the line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohcursta                                                                              0x07090

/*--------------------------------------
BitField Name: TcaLDefCurStatus
BitField Type: RW
BitField Desc: TCA-L Defect  in the related line. 1: TCA-L defect is set 0:
TCA-L defect is cleared.
BitField Bits: [12]
--------------------------------------*/
#define cAf6_tohcursta_TcaLDefCurStatus_Mask                                                            cBit12
#define cAf6_tohcursta_TcaLDefCurStatus_Shift                                                               12

/*--------------------------------------
BitField Name: SdLDefCurStatus
BitField Type: RW
BitField Desc: SD-L Defect  in the related line. 1: SD-L defect is set 0: SD-L
defect is cleared.
BitField Bits: [11]
--------------------------------------*/
#define cAf6_tohcursta_SdLDefCurStatus_Mask                                                             cBit11
#define cAf6_tohcursta_SdLDefCurStatus_Shift                                                                11

/*--------------------------------------
BitField Name: SfLDefCurStatus
BitField Type: RW
BitField Desc: SF-L Defect  in the related line. 1: SF-L defect is set 0: SF-L
defect is cleared.
BitField Bits: [10]
--------------------------------------*/
#define cAf6_tohcursta_SfLDefCurStatus_Mask                                                             cBit10
#define cAf6_tohcursta_SfLDefCurStatus_Shift                                                                10

/*--------------------------------------
BitField Name: TimLDefCurStatus
BitField Type: RW
BitField Desc: TIM-L Defect  in the related line. 1: TIM-L defect is set 0:
TIM-L defect is cleared.
BitField Bits: [9]
--------------------------------------*/
#define cAf6_tohcursta_TimLDefCurStatus_Mask                                                             cBit9
#define cAf6_tohcursta_TimLDefCurStatus_Shift                                                                9

/*--------------------------------------
BitField Name: S1StbStateChgIntr
BitField Type: RW
BitField Desc: S1 stable status  in the related line.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_tohcursta_S1StbStateChgIntr_Mask                                                            cBit8
#define cAf6_tohcursta_S1StbStateChgIntr_Shift                                                               8

/*--------------------------------------
BitField Name: K1StbCurStatus
BitField Type: RW
BitField Desc: K1 stable status  in the related line.
BitField Bits: [7]
--------------------------------------*/
#define cAf6_tohcursta_K1StbCurStatus_Mask                                                               cBit7
#define cAf6_tohcursta_K1StbCurStatus_Shift                                                                  7

/*--------------------------------------
BitField Name: ApsLCurStatus
BitField Type: RW
BitField Desc: APS-L Defect  in the related line. 1: APS-L defect is set 0:
APS-L defect is cleared.
BitField Bits: [6]
--------------------------------------*/
#define cAf6_tohcursta_ApsLCurStatus_Mask                                                                cBit6
#define cAf6_tohcursta_ApsLCurStatus_Shift                                                                   6

/*--------------------------------------
BitField Name: K2StbCurStatus
BitField Type: RW
BitField Desc: K2 stable status  in the related line.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_tohcursta_K2StbCurStatus_Mask                                                               cBit5
#define cAf6_tohcursta_K2StbCurStatus_Shift                                                                  5

/*--------------------------------------
BitField Name: RdiLDefCurStatus
BitField Type: RW
BitField Desc: RDI-L Defect  in the related line. 1: RDI-L defect is set 0:
RDI-L defect is cleared.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_tohcursta_RdiLDefCurStatus_Mask                                                             cBit4
#define cAf6_tohcursta_RdiLDefCurStatus_Shift                                                                4

/*--------------------------------------
BitField Name: AisLDefCurStatus
BitField Type: RW
BitField Desc: AIS-L Defect  in the related line. 1: AIS-L defect is set 0:
AIS-L defect is cleared.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_tohcursta_AisLDefCurStatus_Mask                                                             cBit3
#define cAf6_tohcursta_AisLDefCurStatus_Shift                                                                3

/*--------------------------------------
BitField Name: OofCurStatus
BitField Type: RW
BitField Desc: OOF current status in the related line. 1: OOF state 0: Not OOF
state.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_tohcursta_OofCurStatus_Mask                                                                 cBit2
#define cAf6_tohcursta_OofCurStatus_Shift                                                                    2

/*--------------------------------------
BitField Name: LofCurStatus
BitField Type: RW
BitField Desc: LOF current status in the related line. 1: LOF state 0: Not LOF
state.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_tohcursta_LofCurStatus_Mask                                                                 cBit1
#define cAf6_tohcursta_LofCurStatus_Shift                                                                    1

/*--------------------------------------
BitField Name: LosCurStatus
BitField Type: RW
BitField Desc: LOS current status in the related line. 1: LOS state 0: Not LOS
state.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_tohcursta_LosCurStatus_Mask                                                                 cBit0
#define cAf6_tohcursta_LosCurStatus_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx Line Per Line Interrupt Enable Control
Reg Addr   : 0x0709e
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 4 bits for 4 lines (Line Id: 0-3) at Rx side to enable interrupts when alarms of related lines happen. It is noted that this register is higher priority than the OCN Rx Line per Alarm Interrupt Enable Control register.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohintperlineenctl                                                                     0x0709e

/*--------------------------------------
BitField Name: OCNRxLLineIntrEn1
BitField Type: RW
BitField Desc: Bit #0 to enable for line #0.
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_tohintperlineenctl_OCNRxLLineIntrEn1_Mask                                                 cBit3_0
#define cAf6_tohintperlineenctl_OCNRxLLineIntrEn1_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx Line per Line Interrupt OR Status
Reg Addr   : 0x0709f
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 4 bits for 4 lines (Line Id: 0-3) at Rx side. Each bit is used to store Interrupt OR status of the related line. If there are any bits in this register and they are enabled to raise interrupt, the Rx line level interrupt bit is set.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohintperline                                                                          0x0709f

/*--------------------------------------
BitField Name: OCNRxLLineIntr1
BitField Type: RW
BitField Desc: Set to 1 to indicate that there is any interrupt status bit in
the OCN Rx Line per Alarm Interrupt Status register of the related STS/VC to be
set and they are enabled to raise interrupt. Bit 0 for line #0, respectively.
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_tohintperline_OCNRxLLineIntr1_Mask                                                        cBit3_0
#define cAf6_tohintperline_OCNRxLLineIntr1_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : OCN Parity Force Config
Reg Addr   : 0x00010
Reg Formula: 
    Where  : 
Reg Desc   : 
OCN Parity Force Config

------------------------------------------------------------------------------*/
#define cAf6Reg_parfrccfg                                                                              0x00010

/*--------------------------------------
BitField Name: SpiParErrFrc
BitField Type: RW
BitField Desc: Force Parity for ram config "OCN STS Pointer Interpreter Per
Channel Control".  Set 1 to force.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_parfrccfg_SpiParErrFrc_Mask                                                                 cBit8
#define cAf6_parfrccfg_SpiParErrFrc_Shift                                                                    8

/*--------------------------------------
BitField Name: RxHoParErrFrc
BitField Type: RW
BitField Desc: Force Parity for ram config "OCN Rx High Order Map concatenate
configuration". Set 1 to force.
BitField Bits: [7]
--------------------------------------*/
#define cAf6_parfrccfg_RxHoParErrFrc_Mask                                                                cBit7
#define cAf6_parfrccfg_RxHoParErrFrc_Shift                                                                   7

/*--------------------------------------
BitField Name: TxSxcParErrFrc
BitField Type: RW
BitField Desc: Force Parity for ram config "OCN Tx SXC Control".  Set 1 to
force.
BitField Bits: [6]
--------------------------------------*/
#define cAf6_parfrccfg_TxSxcParErrFrc_Mask                                                               cBit6
#define cAf6_parfrccfg_TxSxcParErrFrc_Shift                                                                  6

/*--------------------------------------
BitField Name: RxSxcParErrFrc1
BitField Type: RW
BitField Desc: Force Parity for ram config "OCN Rx SXC Control" for Slice1. Set
1 to force.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_parfrccfg_RxSxcParErrFrc1_Mask                                                              cBit5
#define cAf6_parfrccfg_RxSxcParErrFrc1_Shift                                                                 5

/*--------------------------------------
BitField Name: RxSxcParErrFrc0
BitField Type: RW
BitField Desc: Force Parity for ram config "OCN Rx SXC Control" for Slice0. Set
1 to force.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_parfrccfg_RxSxcParErrFrc0_Mask                                                              cBit4
#define cAf6_parfrccfg_RxSxcParErrFrc0_Shift                                                                 4

/*--------------------------------------
BitField Name: VpgCtlParErrFrc
BitField Type: RW
BitField Desc: Force Parity for ram config "OCN VTTU Pointer Generator Per
Channel Control".  Set 1 to force.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_parfrccfg_VpgCtlParErrFrc_Mask                                                              cBit3
#define cAf6_parfrccfg_VpgCtlParErrFrc_Shift                                                                 3

/*--------------------------------------
BitField Name: VpgDemParErrFrc
BitField Type: RW
BitField Desc: Force Parity for ram config "OCN TXPP Per STS Multiplexing
Control".  Set 1 to force.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_parfrccfg_VpgDemParErrFrc_Mask                                                              cBit2
#define cAf6_parfrccfg_VpgDemParErrFrc_Shift                                                                 2

/*--------------------------------------
BitField Name: VpiCtlParErrFrc
BitField Type: RW
BitField Desc: Force Parity for ram config "OCN VTTU Pointer Interpreter Per
Channel Control".  Set 1 to force.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_parfrccfg_VpiCtlParErrFrc_Mask                                                              cBit1
#define cAf6_parfrccfg_VpiCtlParErrFrc_Shift                                                                 1

/*--------------------------------------
BitField Name: VpiDemParErrFrc
BitField Type: RW
BitField Desc: Force Parity for ram config "OCN RXPP Per STS payload Control".
Set 1 to force.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_parfrccfg_VpiDemParErrFrc_Mask                                                              cBit0
#define cAf6_parfrccfg_VpiDemParErrFrc_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : OCN Parity Disable Config
Reg Addr   : 0x00011
Reg Formula: 
    Where  : 
Reg Desc   : 
OCN Parity Disable Config

------------------------------------------------------------------------------*/
#define cAf6Reg_pardiscfg                                                                              0x00011

/*--------------------------------------
BitField Name: SpgParErrFrc
BitField Type: RW
BitField Desc: Disable Parity for ram config "OCN STS Pointer Generator Per
Channel Control".  Set 1 to disable.
BitField Bits: [9]
--------------------------------------*/
#define cAf6_pardiscfg_SpgParErrFrc_Mask                                                                 cBit9
#define cAf6_pardiscfg_SpgParErrFrc_Shift                                                                    9

/*--------------------------------------
BitField Name: SpiParErrFrc
BitField Type: RW
BitField Desc: Disable Parity for ram config "OCN STS Pointer Interpreter Per
Channel Control".  Set 1 to disable.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_pardiscfg_SpiParErrFrc_Mask                                                                 cBit8
#define cAf6_pardiscfg_SpiParErrFrc_Shift                                                                    8

/*--------------------------------------
BitField Name: RxHoParErrFrc
BitField Type: RW
BitField Desc: Disable Parity for ram config "OCN Rx High Order Map concatenate
configuration". Set 1 to disable.
BitField Bits: [7]
--------------------------------------*/
#define cAf6_pardiscfg_RxHoParErrFrc_Mask                                                                cBit7
#define cAf6_pardiscfg_RxHoParErrFrc_Shift                                                                   7

/*--------------------------------------
BitField Name: TxSxcParErrFrc
BitField Type: RW
BitField Desc: Disable Parity for ram config "OCN Tx SXC Control".  Set 1 to
disable.
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pardiscfg_TxSxcParErrFrc_Mask                                                               cBit6
#define cAf6_pardiscfg_TxSxcParErrFrc_Shift                                                                  6

/*--------------------------------------
BitField Name: RxSxcParErrFrc1
BitField Type: RW
BitField Desc: Disable Parity for ram config "OCN Rx SXC Control" for Slice1.
Set 1 to disable.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pardiscfg_RxSxcParErrFrc1_Mask                                                              cBit5
#define cAf6_pardiscfg_RxSxcParErrFrc1_Shift                                                                 5

/*--------------------------------------
BitField Name: RxSxcParErrFrc0
BitField Type: RW
BitField Desc: Disable Parity for ram config "OCN Rx SXC Control" for Slice0.
Set 1 to disable.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pardiscfg_RxSxcParErrFrc0_Mask                                                              cBit4
#define cAf6_pardiscfg_RxSxcParErrFrc0_Shift                                                                 4

/*--------------------------------------
BitField Name: VpgCtlParErrFrc
BitField Type: RW
BitField Desc: Disable Parity for ram config "OCN VTTU Pointer Generator Per
Channel Control".  Set 1 to disable.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pardiscfg_VpgCtlParErrFrc_Mask                                                              cBit3
#define cAf6_pardiscfg_VpgCtlParErrFrc_Shift                                                                 3

/*--------------------------------------
BitField Name: VpgDemParErrFrc
BitField Type: RW
BitField Desc: Disable Parity for ram config "OCN TXPP Per STS Multiplexing
Control".  Set 1 to disable.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pardiscfg_VpgDemParErrFrc_Mask                                                              cBit2
#define cAf6_pardiscfg_VpgDemParErrFrc_Shift                                                                 2

/*--------------------------------------
BitField Name: VpiCtlParErrFrc
BitField Type: RW
BitField Desc: Disable Parity for ram config "OCN VTTU Pointer Interpreter Per
Channel Control".  Set 1 to disable.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pardiscfg_VpiCtlParErrFrc_Mask                                                              cBit1
#define cAf6_pardiscfg_VpiCtlParErrFrc_Shift                                                                 1

/*--------------------------------------
BitField Name: VpiDemParErrFrc
BitField Type: RW
BitField Desc: Disable Parity for ram config "OCN RXPP Per STS payload Control".
Set 1 to disable.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pardiscfg_VpiDemParErrFrc_Mask                                                              cBit0
#define cAf6_pardiscfg_VpiDemParErrFrc_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : OCN Parity Error Sticky
Reg Addr   : 0x00012
Reg Formula: 
    Where  : 
Reg Desc   : 
OCN Parity Error Sticky

------------------------------------------------------------------------------*/
#define cAf6Reg_parerrstk                                                                              0x00012

/*--------------------------------------
BitField Name: SpgParErrFrc
BitField Type: W1C
BitField Desc: Error Sticky for ram config "OCN STS Pointer Generator Per
Channel Control".
BitField Bits: [9]
--------------------------------------*/
#define cAf6_parerrstk_SpgParErrFrc_Mask                                                                 cBit9
#define cAf6_parerrstk_SpgParErrFrc_Shift                                                                    9

/*--------------------------------------
BitField Name: SpiParErrFrc
BitField Type: W1C
BitField Desc: Error Sticky for ram config "OCN STS Pointer Interpreter Per
Channel Control".
BitField Bits: [8]
--------------------------------------*/
#define cAf6_parerrstk_SpiParErrFrc_Mask                                                                 cBit8
#define cAf6_parerrstk_SpiParErrFrc_Shift                                                                    8

/*--------------------------------------
BitField Name: RxHoParErrFrc
BitField Type: W1C
BitField Desc: Error Sticky for ram config "OCN Rx High Order Map concatenate
configuration".
BitField Bits: [7]
--------------------------------------*/
#define cAf6_parerrstk_RxHoParErrFrc_Mask                                                                cBit7
#define cAf6_parerrstk_RxHoParErrFrc_Shift                                                                   7

/*--------------------------------------
BitField Name: TxSxcParErrFrc
BitField Type: W1C
BitField Desc: Error Sticky for ram config "OCN Tx SXC Control".
BitField Bits: [6]
--------------------------------------*/
#define cAf6_parerrstk_TxSxcParErrFrc_Mask                                                               cBit6
#define cAf6_parerrstk_TxSxcParErrFrc_Shift                                                                  6

/*--------------------------------------
BitField Name: RxSxcParErrFrc1
BitField Type: W1C
BitField Desc: Error Sticky for ram config "OCN Rx SXC Control" for Slice1.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_parerrstk_RxSxcParErrFrc1_Mask                                                              cBit5
#define cAf6_parerrstk_RxSxcParErrFrc1_Shift                                                                 5

/*--------------------------------------
BitField Name: RxSxcParErrFrc0
BitField Type: W1C
BitField Desc: Error Sticky for ram config "OCN Rx SXC Control" for Slice0.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_parerrstk_RxSxcParErrFrc0_Mask                                                              cBit4
#define cAf6_parerrstk_RxSxcParErrFrc0_Shift                                                                 4

/*--------------------------------------
BitField Name: VpgCtlParErrFrc
BitField Type: W1C
BitField Desc: Error Sticky for ram config "OCN VTTU Pointer Generator Per
Channel Control".
BitField Bits: [3]
--------------------------------------*/
#define cAf6_parerrstk_VpgCtlParErrFrc_Mask                                                              cBit3
#define cAf6_parerrstk_VpgCtlParErrFrc_Shift                                                                 3

/*--------------------------------------
BitField Name: VpgDemParErrFrc
BitField Type: W1C
BitField Desc: Error Sticky for ram config "OCN TXPP Per STS Multiplexing
Control".
BitField Bits: [2]
--------------------------------------*/
#define cAf6_parerrstk_VpgDemParErrFrc_Mask                                                              cBit2
#define cAf6_parerrstk_VpgDemParErrFrc_Shift                                                                 2

/*--------------------------------------
BitField Name: VpiCtlParErrFrc
BitField Type: W1C
BitField Desc: Error Sticky for ram config "OCN VTTU Pointer Interpreter Per
Channel Control".
BitField Bits: [1]
--------------------------------------*/
#define cAf6_parerrstk_VpiCtlParErrFrc_Mask                                                              cBit1
#define cAf6_parerrstk_VpiCtlParErrFrc_Shift                                                                 1

/*--------------------------------------
BitField Name: VpiDemParErrFrc
BitField Type: W1C
BitField Desc: Error Sticky for ram config "OCN RXPP Per STS payload Control".
BitField Bits: [0]
--------------------------------------*/
#define cAf6_parerrstk_VpiDemParErrFrc_Mask                                                              cBit0
#define cAf6_parerrstk_VpiDemParErrFrc_Shift                                                                 0

#define cAf6Reg_glbLoopback                                                                             0x0005
#define cAf6Reg_LineLocalLoopbackMask(lineId)                                              (cBit4 << (lineId))
#define cAf6Reg_LineLocalLoopbackShift(lineId)                                                  (4 + (lineId))

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061MODULEOCNREG_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : OCN
 *
 * File        : Tha60210061ModuleOcn.c
 *
 * Created Date: Aug 19, 2016
 *
 * Description : Tha60210061 Module OCN
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/sdh/AtSdhChannelInternal.h"
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60210012/ocn/Tha60210012ModuleOcnInternal.h"
#include "../../Tha60210011/sdh/Tha60210011ModuleSdh.h"
#include "../sdh/Tha60210061ModuleSdh.h"
#include "../poh/Tha60210061ModulePoh.h"
#include "Tha60210061ModuleOcnReg.h"
#include "Tha60210061ModuleOcnEc1Reg.h"
#include "Tha60210061ModuleOcn.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6Reg_ocn_ec1_pgdemramctl_Base               0x2000
#define cAf6Reg_ocn_ec1_vpiramctl_Base                 0x1100
#define cAf6Reg_ocn_ec1_vpgramctl_Base                 0x2100
#define cAf6Reg_ocn_ec1_spiramctl_Base                 0x0200
#define cAf6Reg_ocn_ec1_spgramctl_Base                 0x0300

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210061ModuleOcn*)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061ModuleOcn
    {
    tTha60210012ModuleOcn super;
    }tTha60210061ModuleOcn;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleOcnMethods         m_ThaModuleOcnOverride;
static tTha60210011ModuleOcnMethods m_Tha60210011ModuleOcnOverride;

/* Save super implementation */
static const tThaModuleOcnMethods         *m_ThaModuleOcnMethods = NULL;
static const tTha60210011ModuleOcnMethods *m_Tha60210011ModuleOcnMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool ChannelIsInEc1Line(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModuleSdh);
    return Tha60210061ModuleSdhChannelIsInEc1Line(sdhModule, channel);
    }

static uint32 AddressWithOffset(ThaModuleOcn self, uint32 addr)
    {
    return addr + Tha60210011ModuleOcnBaseAddress(self);
    }

static eAtRet RxDefaultSet(ThaModuleOcn self)
    {
    uint32 regAddr  = AddressWithOffset(self, cAf6Reg_glbrfm_reg);
    uint32 regValue = mModuleHwRead(self, regAddr);

    mRegFieldSet(regValue, cAf6_glbrfm_reg_RxFrmBadFrmThresh_, 4);
    mRegFieldSet(regValue, cAf6_glbrfm_reg_RxFrmDescrEn_, 0xF);
    mRegFieldSet(regValue, cAf6_glbrfm_reg_RxFrmStmOcnMode_, 0x55);
    mModuleHwWrite(self, regAddr, regValue);

    regAddr  = AddressWithOffset(self, cAf6Reg_glbrfmaisrdifwd_reg);
    regValue = mModuleHwRead(self, regAddr);
    mRegFieldSet(regValue, cAf6_glbrfmaisrdifwd_reg_RxFrmLosAisPEn1_, 0x1);
    mRegFieldSet(regValue, cAf6_glbrfmaisrdifwd_reg_RxFrmLosAisPEn2_, 0x1);
    mRegFieldSet(regValue, cAf6_glbrfmaisrdifwd_reg_RxFrmLosAisPEn3_, 0x1);
    mRegFieldSet(regValue, cAf6_glbrfmaisrdifwd_reg_RxFrmLosAisPEn4_, 0x1);
    mRegFieldSet(regValue, cAf6_glbrfmaisrdifwd_reg_RxFrmLosRdiLEn1_, 0x1);
    mRegFieldSet(regValue, cAf6_glbrfmaisrdifwd_reg_RxFrmLosRdiLEn2_, 0x1);
    mRegFieldSet(regValue, cAf6_glbrfmaisrdifwd_reg_RxFrmLosRdiLEn3_, 0x1);
    mRegFieldSet(regValue, cAf6_glbrfmaisrdifwd_reg_RxFrmLosRdiLEn4_, 0x1);

    mRegFieldSet(regValue, cAf6_glbrfmaisrdifwd_reg_RxFrmLofAisPEn1_, 0x1);
    mRegFieldSet(regValue, cAf6_glbrfmaisrdifwd_reg_RxFrmLofAisPEn2_, 0x1);
    mRegFieldSet(regValue, cAf6_glbrfmaisrdifwd_reg_RxFrmLofAisPEn3_, 0x1);
    mRegFieldSet(regValue, cAf6_glbrfmaisrdifwd_reg_RxFrmLofAisPEn4_, 0x1);
    mRegFieldSet(regValue, cAf6_glbrfmaisrdifwd_reg_RxFrmLofRdiLEn1_, 0x1);
    mRegFieldSet(regValue, cAf6_glbrfmaisrdifwd_reg_RxFrmLofRdiLEn2_, 0x1);
    mRegFieldSet(regValue, cAf6_glbrfmaisrdifwd_reg_RxFrmLofRdiLEn3_, 0x1);
    mRegFieldSet(regValue, cAf6_glbrfmaisrdifwd_reg_RxFrmLofRdiLEn4_, 0x1);

    mRegFieldSet(regValue, cAf6_glbrfmaisrdifwd_reg_RxFrmTimAisPEn1_, 0x0);
    mRegFieldSet(regValue, cAf6_glbrfmaisrdifwd_reg_RxFrmTimAisPEn2_, 0x0);
    mRegFieldSet(regValue, cAf6_glbrfmaisrdifwd_reg_RxFrmTimAisPEn3_, 0x0);
    mRegFieldSet(regValue, cAf6_glbrfmaisrdifwd_reg_RxFrmTimAisPEn4_, 0x0);
    mRegFieldSet(regValue, cAf6_glbrfmaisrdifwd_reg_RxFrmTimRdiLEn1_, 0x0);
    mRegFieldSet(regValue, cAf6_glbrfmaisrdifwd_reg_RxFrmTimRdiLEn2_, 0x0);
    mRegFieldSet(regValue, cAf6_glbrfmaisrdifwd_reg_RxFrmTimRdiLEn3_, 0x0);
    mRegFieldSet(regValue, cAf6_glbrfmaisrdifwd_reg_RxFrmTimRdiLEn4_, 0x0);

    mRegFieldSet(regValue, cAf6_glbrfmaisrdifwd_reg_RxFrmAislAisPEn1_, 0x1);
    mRegFieldSet(regValue, cAf6_glbrfmaisrdifwd_reg_RxFrmAislAisPEn2_, 0x1);
    mRegFieldSet(regValue, cAf6_glbrfmaisrdifwd_reg_RxFrmAislAisPEn3_, 0x1);
    mRegFieldSet(regValue, cAf6_glbrfmaisrdifwd_reg_RxFrmAislAisPEn4_, 0x1);
    mRegFieldSet(regValue, cAf6_glbrfmaisrdifwd_reg_RxFrmAislRdiLEn1_, 0x1);
    mRegFieldSet(regValue, cAf6_glbrfmaisrdifwd_reg_RxFrmAislRdiLEn2_, 0x1);
    mRegFieldSet(regValue, cAf6_glbrfmaisrdifwd_reg_RxFrmAislRdiLEn3_, 0x1);
    mRegFieldSet(regValue, cAf6_glbrfmaisrdifwd_reg_RxFrmAislRdiLEn4_, 0x1);
    mModuleHwWrite(self, regAddr, regValue);

    return cAtOk;
    }

static eAtRet RxThresholdDefaultSet(ThaModuleOcn self)
    {
    uint32 regAddr  = AddressWithOffset(self, cAf6Reg_glbclkmon_reg);
    uint32 regVal = 0;

    mRegFieldSet(regVal, cAf6_glbclkmon_reg_RxFrmLosDetMod_, 0x1);
    mRegFieldSet(regVal, cAf6_glbclkmon_reg_RxFrmLosDetDis_, 0x1);
    mRegFieldSet(regVal, cAf6_glbclkmon_reg_RxFrmClkMonDis_, 0x1);
    mRegFieldSet(regVal, cAf6_glbclkmon_reg_RxFrmClkMonThr_, 0x3CC);
    mModuleHwWrite(self, regAddr, regVal);

    regAddr  = AddressWithOffset(self, cAf6Reg_glbdetlos_pen);
    regVal = 0;
    mRegFieldSet(regVal, cAf6_glbdetlos_pen_RxFrmLosClr2Thr_, 0x30);
    mRegFieldSet(regVal, cAf6_glbdetlos_pen_RxFrmLosSetThr_, 0x3CC);
    mRegFieldSet(regVal, cAf6_glbdetlos_pen_RxFrmLosClr1Thr_, 0x3CC);
    mModuleHwWrite(self, regAddr, regVal);

    regAddr  = AddressWithOffset(self, cAf6Reg_glblofthr_reg);
    regVal = 0;
    mRegFieldSet(regVal, cAf6_glblofthr_reg_RxFrmLofDetMod_, 0x1);
    mRegFieldSet(regVal, cAf6_glblofthr_reg_RxFrmLofSetThr_, 0x18);
    mRegFieldSet(regVal, cAf6_glblofthr_reg_RxFrmLofClrThr_, 0x18);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet TxDefaultSet(ThaModuleOcn self)
    {
    uint32 regVal = 0;
    uint32 regAddr  = AddressWithOffset(self, cAf6Reg_glbtfm_reg);

    mRegFieldSet(regVal, cAf6_glbtfm_reg_TxLineOofFrc_, 0x0);
    mRegFieldSet(regVal, cAf6_glbtfm_reg_TxLineB1ErrFrc_, 0x0);
    mRegFieldSet(regVal, cAf6_glbtfm_reg_TxFrmScrEn_, 0xf);
    mRegFieldSet(regVal, cAf6_glbtfm_reg_TxFrmStmOcnMode_, 0x55);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static uint32 Ec1AddressWithOffset(ThaModuleOcn self, uint32 addr)
    {
    return addr + Tha60210061ModuleOcnEc1BaseAddress(self);
    }

static void Ec1RxDefaultSet(ThaModuleOcn self)
    {
    uint32 regAddr  = Ec1AddressWithOffset(self, cAf6_Ec1Reg_glbrfm_reg_Base);
    uint32 regVal = mModuleHwRead(self, regAddr);

    mRegFieldSet(regVal, cAf6_Ec1_glbrfm_reg_RxFrmAislRdiLEn_, 1);
    mRegFieldSet(regVal, cAf6_Ec1_glbrfm_reg_RxFrmTimRdiLEn_, 1);
    mRegFieldSet(regVal, cAf6_Ec1_glbrfm_reg_RxFrmOofRdiLEn_, 0);
    mRegFieldSet(regVal, cAf6_Ec1_glbrfm_reg_RxFrmLofRdiLEn_, 1);
    mRegFieldSet(regVal, cAf6_Ec1_glbrfm_reg_RxFrmLosRdiLEn_, 1);

    mRegFieldSet(regVal, cAf6_Ec1_glbrfm_reg_RxFrmAislAisPEn_, 1);
    mRegFieldSet(regVal, cAf6_Ec1_glbrfm_reg_RxFrmTimAisPEn_, 1);
    mRegFieldSet(regVal, cAf6_Ec1_glbrfm_reg_RxFrmOofAisPEn_, 0);
    mRegFieldSet(regVal, cAf6_Ec1_glbrfm_reg_RxFrmLofAisPEn_, 1);
    mRegFieldSet(regVal, cAf6_Ec1_glbrfm_reg_RxFrmLosAisPEn_, 1);

    mRegFieldSet(regVal, cAf6_Ec1_glbrfm_reg_RxFrmLofCfg_, 0);
    mRegFieldSet(regVal, cAf6_Ec1_glbrfm_reg_RxFrmLosDetDis_, 0);
    mRegFieldSet(regVal, cAf6_Ec1_glbrfm_reg_RxFrmLosCfg_, 1);
    mRegFieldSet(regVal, cAf6_Ec1_glbrfm_reg_RxFrmDescrEn_, 1);

    mModuleHwWrite(self, regAddr, regVal);
    }

static void Ec1RxThresholdDefaultSet(ThaModuleOcn self)
    {
    uint32 regAddr  = Ec1AddressWithOffset(self, cAf6_Ec1Reg_glblosthr_reg_Base);
    uint32 regVal = 0;

    /* Enable to detect LOS when signal PPM is greater than 20PPM */
    mRegFieldSet(regVal, cAf6_Ec1_glblosthr_reg_RxFrmNumValMonDis_, 0);
    mRegFieldSet(regVal, cAf6_Ec1_glblosthr_reg_RxFrmNumValMonThr_, 0x20);

    mRegFieldSet(regVal, cAf6_Ec1_glblosthr_reg_RxFrmLosClr2Thr_, 0x8);
    mRegFieldSet(regVal, cAf6_Ec1_glblosthr_reg_RxFrmLosSetThr_, 0x28);
    mRegFieldSet(regVal, cAf6_Ec1_glblosthr_reg_RxFrmLosClr1Thr_, 0x51);
    mModuleHwWrite(self, regAddr, regVal);

    regAddr  = Ec1AddressWithOffset(self, cAf6_Ec1Reg_glblofthr_reg_Base);
    regVal = 0;
    mRegFieldSet(regVal, cAf6_Ec1_glblofthr_reg_RxFrmLofSetThr_, 0x18);
    mRegFieldSet(regVal, cAf6_Ec1_glblofthr_reg_RxFrmLofClrThr_, 0x18);
    mModuleHwWrite(self, regAddr, regVal);
    }

static void Ec1TxDefaultSet(ThaModuleOcn self)
    {
    uint32 regVal = 0;
    uint32 regAddr  = Ec1AddressWithOffset(self, cAf6_Ec1Reg_glbtfm_reg_Base);

    mRegFieldSet(regVal, cAf6_Ec1_glbtfm_reg_TxLineOofFrc_, 0);
    mRegFieldSet(regVal, cAf6_Ec1_glbtfm_reg_TxLineB1errFrc_, 0);
    mRegFieldSet(regVal, cAf6_Ec1_glbtfm_reg_TxLineScrEn_, 1);
    mModuleHwWrite(self, regAddr, regVal);

    regAddr = Ec1AddressWithOffset(self, cAf6_Ec1Reg_glbclksel_reg1_Base);
    mModuleHwWrite(self, regAddr, 0);
    regAddr = Ec1AddressWithOffset(self, cAf6_Ec1Reg_glbclklooptime_reg1_Base);
    mModuleHwWrite(self, regAddr, 0);

    regVal = 0;
    regAddr  = Ec1AddressWithOffset(self, cAf6_Ec1Reg_glbrdiinsthr_reg_Base);
    mRegFieldSet(regVal, cAf6_Ec1_glbrdiinsthr_reg_TxFrmRdiInsThr2_, 0x8);
    mRegFieldSet(regVal, cAf6_Ec1_glbrdiinsthr_reg_TxFrmRdiInsThr1_, 0x14);
    mModuleHwWrite(self, regAddr, regVal);
    }

static void Ec1PointerDefaultSet(ThaModuleOcn self)
    {
    uint32 regAddr = Ec1AddressWithOffset(self, cAf6_Ec1Reg_glbspi_reg_Base);
    uint32 regVal = 0;

    mRegFieldSet(regVal, cAf6_glbspi_reg_StsPiAdjCntSel_, 0);
    mRegFieldSet(regVal, cAf6_glbspi_reg_RxPgNorPtrThresh_, 3);
    mRegFieldSet(regVal, cAf6_Ec1_glbspi_reg_RxPgFlowThresh_, 3);
    mRegFieldSet(regVal, cAf6_Ec1_glbspi_reg_RxPgAdjThresh_, 0xC);
    mRegFieldSet(regVal, cAf6_Ec1_glbspi_reg_StsPiAisAisPEn_, 1);
    mRegFieldSet(regVal, cAf6_Ec1_glbspi_reg_StsPiLopAisPEn_, 1);
    mRegFieldSet(regVal, cAf6_Ec1_glbspi_reg_StsPiMajorMode_, 1);
    mRegFieldSet(regVal, cAf6_Ec1_glbspi_reg_StsPiNorPtrThresh_, 3);
    mRegFieldSet(regVal, cAf6_Ec1_glbspi_reg_StsPiNdfPtrThresh_, 8);
    mRegFieldSet(regVal, cAf6_Ec1_glbspi_reg_StsPiBadPtrThresh_, 8);
    mRegFieldSet(regVal, cAf6_Ec1_glbspi_reg_StsPiNorPtrThresh_, 3);
    mRegFieldSet(regVal, cAf6_Ec1_glbspi_reg_StsPiPohAisType_, 0xF);
    mModuleHwWrite(self, regAddr, regVal);

    regAddr = Ec1AddressWithOffset(self, cAf6_Ec1Reg_glbvpi_reg_Base);
    regVal = 0;
    mRegFieldSet(regVal, cAf6_Ec1_glbvpi_reg_VtPiLomAisPEn_, 1);
    mRegFieldSet(regVal, cAf6_Ec1_glbvpi_reg_VtPiLomInvlCntMod_, 0);
    mRegFieldSet(regVal, cAf6_Ec1_glbvpi_reg_VtPiLomGoodThresh_, 3);
    mRegFieldSet(regVal, cAf6_Ec1_glbvpi_reg_VtPiLomInvlThresh_, 0xC);
    mRegFieldSet(regVal, cAf6_Ec1_glbvpi_reg_VtPiAisAisPEn_, 1);
    mRegFieldSet(regVal, cAf6_Ec1_glbvpi_reg_VtPiLopAisPEn_, 1);
    mRegFieldSet(regVal, cAf6_Ec1_glbvpi_reg_VtPiMajorMode_, 1);
    mRegFieldSet(regVal, cAf6_Ec1_glbvpi_reg_VtPiNorPtrThresh_, 3);
    mRegFieldSet(regVal, cAf6_Ec1_glbvpi_reg_VtPiNdfPtrThresh_, 8);
    mRegFieldSet(regVal, cAf6_Ec1_glbvpi_reg_VtPiBadPtrThresh_, 8);
    mRegFieldSet(regVal, cAf6_Ec1_glbvpi_reg_VtPiPohAisType_, 0xF);
    mModuleHwWrite(self, regAddr, regVal);

    regAddr = Ec1AddressWithOffset(self, cAf6_Ec1Reg_glbtpg_reg_Base);
    regVal = 0;
    mRegFieldSet(regVal, cAf6_Ec1_glbtpg_reg_TxPgNorPtrThresh_, 3);
    mRegFieldSet(regVal, cAf6_Ec1_glbtpg_reg_TxPgFlowThresh_, 3);
    mRegFieldSet(regVal, cAf6_Ec1_glbtpg_reg_TxPgAdjThresh_, 0xC);
    mModuleHwWrite(self, regAddr, regVal);
    }

static void Ec1TohThresholdDefaultSet(ThaModuleOcn self)
    {
    uint32 regAddr = Ec1AddressWithOffset(self, cAf6_Ec1Reg_tohglbk1stbthr_reg_Base);
    uint32 regVal = 0;
    mRegFieldSet(regVal, cAf6_Ec1_tohglbk1stbthr_reg_TohK1StbThr2_, 3);
    mRegFieldSet(regVal, cAf6_Ec1_tohglbk1stbthr_reg_TohK1StbThr1_, 4);
    mModuleHwWrite(self, regAddr, regVal);

    regAddr = Ec1AddressWithOffset(self, cAf6_Ec1Reg_tohglbk2stbthr_reg_Base);
    regVal = 0;
    mRegFieldSet(regVal, cAf6_Ec1_tohglbk2stbthr_reg_TohK2StbThr2_, 3);
    mRegFieldSet(regVal, cAf6_Ec1_tohglbk2stbthr_reg_TohK2StbThr1_, 4);
    mModuleHwWrite(self, regAddr, regVal);

    regAddr = Ec1AddressWithOffset(self, cAf6_Ec1Reg_tohglbs1stbthr_reg_Base);
    regVal = 0;
    mRegFieldSet(regVal, cAf6_Ec1_tohglbs1stbthr_reg_TohS1StbThr2_, 5);
    mRegFieldSet(regVal, cAf6_Ec1_tohglbs1stbthr_reg_TohS1StbThr1_, 5);
    mModuleHwWrite(self, regAddr, regVal);

    regAddr = Ec1AddressWithOffset(self, cAf6_Ec1Reg_tohglbs1stbthr_reg_Base);
    regVal = 0;
    mRegFieldSet(regVal, cAf6_Ec1_tohglbs1stbthr_reg_TohS1StbThr2_, 5);
    mRegFieldSet(regVal, cAf6_Ec1_tohglbs1stbthr_reg_TohS1StbThr1_, 5);
    mModuleHwWrite(self, regAddr, regVal);

    regAddr = Ec1AddressWithOffset(self, cAf6_Ec1Reg_tohglbrdidetthr_reg_Base);
    regVal = 0;
    mRegFieldSet(regVal, cAf6_Ec1_tohglbrdidetthr_reg_TohRdiDetThr2_, 0x4);
    mRegFieldSet(regVal, cAf6_Ec1_tohglbrdidetthr_reg_TohRdiDetThr1_, 0x9);
    mModuleHwWrite(self, regAddr, regVal);

    regAddr = Ec1AddressWithOffset(self, cAf6_Ec1Reg_tohglbaisdetthr_reg_Base);
    regVal = 0;
    mRegFieldSet(regVal, cAf6_Ec1_tohglbaisdetthr_reg_TohAisDetThr2_, 3);
    mRegFieldSet(regVal, cAf6_Ec1_tohglbaisdetthr_reg_TohAisDetThr1_, 4);
    mModuleHwWrite(self, regAddr, regVal);

    regAddr = Ec1AddressWithOffset(self, cAf6_Ec1Reg_tohglbk1smpthr_reg_Base);
    regVal = 0;
    mRegFieldSet(regVal, cAf6_Ec1_tohglbk1smpthr_reg_TohK1SmpThr2_, 7);
    mRegFieldSet(regVal, cAf6_Ec1_tohglbk1smpthr_reg_TohK1SmpThr1_, 7);
    mModuleHwWrite(self, regAddr, regVal);

    regAddr = Ec1AddressWithOffset(self, cAf6_Ec1Reg_tohglberrcntmod_reg_Base);
    regVal = 0;
    mRegFieldSet(regVal, cAf6_Ec1_tohglberrcntmod_reg_TohReiErrCntMod_, 1);
    mRegFieldSet(regVal, cAf6_Ec1_tohglberrcntmod_reg_TohB2ErrCntMod_, 1);
    mModuleHwWrite(self, regAddr, regVal);

    regAddr = Ec1AddressWithOffset(self, cAf6_Ec1Reg_tohglbaffen_reg_Base);
    regVal = 0;
    mRegFieldSet(regVal, cAf6_Ec1_tohglbaffen_reg_TohAisAffStbMon_, 0);
    mRegFieldSet(regVal, cAf6_Ec1_tohglbaffen_reg_TohAisAffRdilMon_, 0);
    mRegFieldSet(regVal, cAf6_Ec1_tohglbaffen_reg_TohAisAffAislMon_, 0);
    mRegFieldSet(regVal, cAf6_Ec1_tohglbaffen_reg_TohAisAffErrCnt_, 0);
    mModuleHwWrite(self, regAddr, regVal);
    }

static eAtRet Ec1DefaultSet(ThaModuleOcn self)
    {
    Ec1RxDefaultSet(self);
    Ec1RxThresholdDefaultSet(self);
    Ec1TxDefaultSet(self);
    Ec1PointerDefaultSet(self);
    Ec1TohThresholdDefaultSet(self);
    return cAtOk;
    }

static eAtRet DefaultSet(ThaModuleOcn self)
    {
    RxDefaultSet(self);
    RxThresholdDefaultSet(self);
    TxDefaultSet(self);

    /* Default configure for Ec1 */
    Ec1DefaultSet(self);
    return cAtOk;
    }

static uint32 Ec1BaseAndStmBaseOffset(Tha60210011ModuleOcn self)
    {
    return Tha60210061ModuleOcnEc1BaseAddress((ThaModuleOcn)self) - Tha60210011ModuleOcnBaseAddress((ThaModuleOcn)self);
    }

static uint32 HwStsDefaultOffset(Tha60210011ModuleOcn self, AtSdhChannel channel, uint8 hwSlice, uint8 hwSts)
    {
    AtUnused(hwSlice);
    if (!ChannelIsInEc1Line(self, channel))
        return hwSts;

    return hwSts + Ec1BaseAndStmBaseOffset(self);
    }

static uint32 RxSxcControlRegAddr(Tha60210011ModuleOcn self)
    {
    AtUnused(self);
    return cAf6Reg_rxsxcramctl;
    }

static uint32 TxSxcControlRegAddr(Tha60210011ModuleOcn self)
    {
    AtUnused(self);
    return cAf6Reg_txsxcramctl;
    }

static uint32 RxStsPayloadControl(ThaModuleOcn self, AtSdhChannel channel)
    {
    if (!ChannelIsInEc1Line((Tha60210011ModuleOcn)self, channel))
        return cAf6Reg_demramctl;

    return cAf6_Ec1Reg_demramctl_Base;
    }

static uint32 TxStsMultiplexingControl(ThaModuleOcn self, AtSdhChannel channel)
    {
    if (!ChannelIsInEc1Line((Tha60210011ModuleOcn)self, channel))
        return cAf6Reg_pgdemramctl;

    return cAf6Reg_ocn_ec1_pgdemramctl_Base;
    }

static uint32 LoStsPayloadHwFormula(Tha60210011ModuleOcn self, AtSdhChannel channel, uint8 hwSlice, uint8 hwSts)
    {
    AtUnused(hwSlice);
    if (!ChannelIsInEc1Line(self, channel))
        return hwSts;

    return hwSts + Ec1BaseAndStmBaseOffset(self);
    }

static uint32 OcnVtTuPointerInterpreterPerChannelControlRegAddr(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    if (!ChannelIsInEc1Line(self, channel))
        return cAf6Reg_vpiramctl;

    return cAf6Reg_ocn_ec1_vpiramctl_Base;
    }

static uint32 OcnVtTuPointerGeneratorPerChannelControlRegAddr(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    if (!ChannelIsInEc1Line(self, channel))
        return cAf6Reg_vpgramctl;

    return cAf6Reg_ocn_ec1_vpgramctl_Base;
    }

static uint32 OcnStsPointerInterpreterPerChannelControlRegAddr(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    if (!ChannelIsInEc1Line(self, channel))
        return cAf6Reg_spiramctl;

    return cAf6Reg_ocn_ec1_spiramctl_Base;
    }

static uint32 OcnStsPointerGeneratorPerChannelControlRegAddr(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    if (!ChannelIsInEc1Line(self, channel))
        return cAf6Reg_spgramctl;

    return cAf6Reg_ocn_ec1_spgramctl_Base;
    }

static uint32 OcnRxHoMapConcatenateReg(Tha60210011ModuleOcn self)
    {
    AtUnused(self);
    return cAf6Reg_rxhomapramctl;
    }

static uint8 LineOffSet(AtSdhChannel self)
    {
    uint8 lineId = AtSdhChannelLineGet(self);
    return (uint8)(lineId % 4);
    }

/*
 * +-------+---+---+---+---+----+----+----+----+----+----+----+----+
 * |SW-STS | 0 | 1 | 2 | 3 | 4  | 5  | 6  | 7  | 8  | 9  | 10 | 11 |
 * +-------+---+---+---+---+----+----+----+----+----+----+----+----+
 * |HW-STS | 0 | 4 | 8 | 12| 16 | 20 | 24 | 28 | 32 | 36 | 40 | 44 |
 * +-------+---+---+---+---+----+----+----+----+----+----+----+----+
 */
static uint8 Aug4ConvertToHwSts(AtSdhChannel self, uint8 sts1Id)
    {
    return (uint8)((sts1Id * 4) + LineOffSet(self));
    }

static uint8 Aug1OfStm4ConvertToHwSts(AtSdhChannel self, uint8 sts1Id)
    {
    /*
     * 0,1,2 -> 0,16,32
     * 3,4,5 -> 4,20,36
     * 6,7,8 -> 8,24,40
     *  ....
     *  1, 17, 33
     */
    AtUnused(self);
    return (uint8)(((sts1Id % 3) * 16) + ((sts1Id / 3) * 4) + LineOffSet(self));
    }

static uint8 Aug1OfStm1ConvertToHwSts(AtSdhChannel self, uint8 sts1Id)
    {
    /*
     * Line#1: 0,1,2 -> 0,16,32
     * Line#2: 0,1,2 -> 1,17,33
     * Line#3: 0,1,2 -> 2,18,34
     * Line#4: 0,1,2 -> 3,19,35
     *  ....
     */
    AtUnused(self);
    return (uint8)(((sts1Id % 3) * 16) + LineOffSet(self));
    }

static uint8 Stm4SdhChannelSwStsId2HwFlatId(AtSdhChannel channel, uint8 sts1Id)
    {
    eAtSdhChannelType layer = AtSdhChannelTypeGet(channel);

    if ((layer == cAtSdhChannelTypeAug4)  ||
        (layer == cAtSdhChannelTypeAu4_4c)||
        (layer == cAtSdhChannelTypeVc4_4c))
        return Aug4ConvertToHwSts(channel, sts1Id);

    return Aug1OfStm4ConvertToHwSts(channel, sts1Id);
    }

static uint8 Stm1SdhChannelSwStsId2HwFlatId(AtSdhChannel channel, uint8 sts1Id)
    {
    return Aug1OfStm1ConvertToHwSts(channel, sts1Id);
    }

static uint8 FakeVcSwStsId2HwFlatId(AtSdhChannel channel, uint8 sts1Id)
    {
    AtUnused(sts1Id);

    /* If fake VC is vc11: HW STS = 52
     * otherwise: HW STS = 53
     */
    if (AtSdhChannelTypeGet(channel) == cAtSdhChannelTypeVc11)
        return 52;

    return 53;
    }

static uint8 Ec1SdhChannelSwStsId2HwFlatId(AtSdhChannel channel, uint8 sts1Id)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)AtChannelModuleGet((AtChannel)channel);
    AtUnused(sts1Id);

    return (uint8)(AtSdhChannelLineGet(channel) - AtModuleSdhNumEc1LinesGet(sdhModule));
    }

static uint8 SdhChannelSwStsId2HwFlatId(Tha60210011ModuleOcn self, AtSdhChannel channel, uint8 sts1Id)
    {
    eAtSdhLineRate rate;

    if (Tha60210061ModuleSdhChannelIsFakeVc(channel))
        return FakeVcSwStsId2HwFlatId(channel, sts1Id);

    rate = AtSdhLineRateGet(AtSdhChannelLineObjectGet(channel));
    if (rate == cAtSdhLineRateStm0)
        return Ec1SdhChannelSwStsId2HwFlatId(channel, sts1Id);

    /* Not EC1 */
    if (AtSdhChannelTypeGet(channel) == cAtSdhChannelTypeLine)
        return (uint8)AtChannelIdGet((AtChannel)channel);

    if (rate == cAtSdhLineRateStm16)
        return m_Tha60210011ModuleOcnMethods->SdhChannelSwStsId2HwFlatId(self, channel, sts1Id);

    if (rate == cAtSdhLineRateStm1)
        return Stm1SdhChannelSwStsId2HwFlatId(channel, sts1Id);

    if (rate == cAtSdhLineRateStm4)
        return Stm4SdhChannelSwStsId2HwFlatId(channel, sts1Id);

    return 0;
    }

static uint8 SliceOfChannel(Tha60210011ModuleOcn self, AtSdhChannel channel, eAtModule phyModule)
    {
    if ((AtSdhChannelTypeGet(channel) == cAtSdhChannelTypeLine) && ((uint32)phyModule == cThaModulePoh))
        return (ChannelIsInEc1Line(self, channel)) ? 3 : 2;

    return (ChannelIsInEc1Line(self, channel)) ? 1 : 0;
    }

static eBool HoPathPhyModuleIsDividedToOc24(Tha60210011ModuleOcn self, eAtModule phyModule)
    {
    AtUnused(self);
    if ((phyModule == cAtModulePdh) ||
        (phyModule == cAtModuleSur))
        return cAtTrue;
        
    return cAtFalse;
    }

static void Oc48HwStsAndSliceConvertToOc24(Tha60210011ModuleOcn self, AtSdhChannel channel, uint8* sliceId, uint8* hwStsInSlice)
    {
    if (!ChannelIsInEc1Line(self, channel))
        {
        m_Tha60210011ModuleOcnMethods->Oc48HwStsAndSliceConvertToOc24(self, channel, sliceId, hwStsInSlice);
        return;
        }

    /*
     * In case of channel of EC1 lines, HW flat STS ID is [0..3] equal to line ID, and slice is 1
     * When accessing OC24 modules, HW STS is converted as below:
     * - Line 0: STS = 24, slice 0
     * - Line 2: STS = 25, slice 0
     * - Line 1: STS = 24, slice 1
     * - Line 3: STS = 25, slice 1
     */

    *sliceId = *hwStsInSlice % 2;
    *hwStsInSlice = (uint8)((*hwStsInSlice / 2) + 24);
    }

static eAtRet PiStsConcatMasterSet(ThaModuleOcn self, AtSdhChannel channel, uint8 masterStsId)
    {
    if (ChannelIsInEc1Line((Tha60210011ModuleOcn)self, channel))
        return cAtOk;

    return m_ThaModuleOcnMethods->PiStsConcatMasterSet(self, channel, masterStsId);
    }

static eAtRet PgStsConcatMasterSet(ThaModuleOcn self, AtSdhChannel channel, uint8 masterStsId)
    {
    if (ChannelIsInEc1Line((Tha60210011ModuleOcn)self, channel))
        return cAtOk;

    return m_ThaModuleOcnMethods->PgStsConcatMasterSet(self, channel, masterStsId);
    }

static eBool CanMovePathAisRxForcingPoint(ThaModuleOcn self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 supportedVersion = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x8, 0x1031);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    return (currentVersion >= supportedVersion) ? cAtTrue : cAtFalse;
    }

static eAtRet PathAisRxForcingPointMove(ThaModuleOcn self, AtSdhAu au, eBool move)
    {
    if (ChannelIsInEc1Line((Tha60210011ModuleOcn)self, (AtSdhChannel)au))
        return cAtOk;

    return m_ThaModuleOcnMethods->PathAisRxForcingPointMove(self, au, move);
    }

static eBool PathAisRxForcingPointIsMoved(ThaModuleOcn self, AtSdhAu au)
    {
    if (ChannelIsInEc1Line((Tha60210011ModuleOcn)self, (AtSdhChannel)au))
        return cAtFalse;

    return m_ThaModuleOcnMethods->PathAisRxForcingPointIsMoved(self, au);
    }

static uint32 TohGlobalS1StableMonitoringThresholdControlRegAddr(ThaModuleOcn self)
    {
    AtUnused(self);
    return cAf6Reg_tohglbs1stbthr_reg;
    }

static uint32 TohGlobalK1StableMonitoringThresholdControlRegAddr(ThaModuleOcn self)
    {
    AtUnused(self);
    return cAf6Reg_tohglbk1stbthr_reg;
    }

static uint32 TohGlobalK2StableMonitoringThresholdControlRegAddr(ThaModuleOcn self)
    {
    AtUnused(self);
    return cAf6Reg_tohglbk2stbthr_reg;
    }

static uint32 StsVtDefaultOffset(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId, uint8 vtgId, uint8 vtId)
    {
    uint8 hwSlice, hwSts;

    if (ThaSdhChannelHwStsGet(channel, cThaModuleOcn, stsId, &hwSlice, &hwSts) != cAtOk)
        return cBit31_0;

    if (ChannelIsInEc1Line((Tha60210011ModuleOcn)self, channel))
        return Tha60210061ModuleOcnEc1BaseAddress((ThaModuleOcn)self) + (32UL * hwSts) + (4UL * vtgId) + vtId;

    return Tha60210011ModuleOcnBaseAddress((ThaModuleOcn)self) + (32UL * hwSts) + (4UL * vtgId) + vtId;
    }

static eBool LoPathPhyModuleIsDividedToBlockOc24(Tha60210011ModuleOcn self, AtSdhChannel channel, eAtModule phyModule)
    {
    uint16 _phyModule = (uint16)phyModule;
    AtDevice device = AtChannelDeviceGet((AtChannel)channel);
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);

    AtUnused(self);
    if ((_phyModule == cAtModulePrbs) && Tha60210061ModuleSdhChannelIsInEc1Line(sdhModule, channel))
        return cAtTrue;

    return m_Tha60210011ModuleOcnMethods->LoPathPhyModuleIsDividedToBlockOc24(self, channel, phyModule);
    }

static AtModuleSdh ModuleSdh(ThaModuleOcn self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    }

static eAtRet LineIdSw2HwGet(ThaModuleOcn self, AtSdhLine sdhLine, eAtModule moduleId, uint8* sliceId, uint8 *hwLineInSlice)
    {
    uint8 phyModule = moduleId;
    uint8 lineId;

    if (moduleId == cAtModuleSur)
        return m_ThaModuleOcnMethods->LineIdSw2HwGet(self, sdhLine, moduleId, sliceId, hwLineInSlice);

    lineId = (uint8)AtChannelIdGet((AtChannel)sdhLine);
    if (Tha60210061ModuleSdhChannelIsInEc1Line(ModuleSdh(self), (AtSdhChannel)sdhLine))
        {
        *sliceId = 1;
        *hwLineInSlice = (uint8)(lineId - AtModuleSdhStartEc1LineIdGet(ModuleSdh(self)));
        }
    else
        {
        *sliceId = 0;
        *hwLineInSlice = lineId;
        }

    if (phyModule == cThaModulePoh)
        *sliceId = (uint8)(*sliceId + 2U);

    return cAtOk;
    }

static eAtRet HwVc3ToTu3VcEnable(ThaModuleOcn self, AtSdhChannel vc3, eBool enable)
    {
    uint32 regAddr = ThaModuleOcnRxStsPayloadControl(self, vc3) +
                     Tha60210011ModuleOcnLoStsDefaultOffset(vc3, AtSdhChannelSts1Get(vc3));
    uint32 regVal  = mChannelHwRead(vc3, regAddr, cThaModuleOcn);

    mRegFieldSet(regVal, cAf6_demramctl_PiDemStsDs3oTu3Cep_, enable ? 1 : 0);
    mChannelHwWrite(vc3, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eBool HwVc3ToTu3VcIsEnabled(ThaModuleOcn self, AtSdhChannel vc3)
    {
    uint32 regAddr = ThaModuleOcnRxStsPayloadControl(self, vc3) +
                     Tha60210011ModuleOcnLoStsDefaultOffset(vc3, AtSdhChannelSts1Get(vc3));
    uint32 regVal  = mChannelHwRead(vc3, regAddr, cThaModuleOcn);

    return mRegField(regVal, cAf6_demramctl_PiDemStsDs3oTu3Cep_) ? cAtTrue : cAtFalse;
    }

static eAtRet Vc3ToTu3VcEnable(ThaModuleOcn self, AtSdhChannel vc3, eBool enable)
    {
    if (ChannelIsInEc1Line((Tha60210011ModuleOcn)self, vc3))
        return HwVc3ToTu3VcEnable(self, vc3, enable);
    return m_ThaModuleOcnMethods->Vc3ToTu3VcEnable(self, vc3, enable);
    }

static eBool Vc3ToTu3VcIsEnabled(ThaModuleOcn self, AtSdhChannel vc3)
    {
    if (ChannelIsInEc1Line((Tha60210011ModuleOcn)self, vc3))
        return HwVc3ToTu3VcIsEnabled(self, vc3);
    return m_ThaModuleOcnMethods->Vc3ToTu3VcIsEnabled(self, vc3);
    }

static uint32 OcnStsPointerInterpreterPerChannelAdjustCountRegAddr(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    if (ChannelIsInEc1Line(self, channel))
        return cAf6_Ec1Reg_adjcntperstsram_Base;

    return cAf6Reg_adjcntperstsram;
    }

static uint32 OcnStsPointerGeneratorPerChannelAdjustCountRegAddr(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    if (ChannelIsInEc1Line(self, channel))
        return cAf6_Ec1Reg_adjcntpgperstsram_Base;

    return cAf6Reg_adjcntpgperstsram;
    }

static uint32 OcnStsPointerGeneratorPerChannelInterruptRegAddr(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    if (ChannelIsInEc1Line(self, channel))
        return cAf6_Ec1Reg_stspgstkram_Base;

    return cAf6Reg_stspgstkram;
    }

static uint32 OcnVtTuPointerInterpreterPerChannelInterruptRegAddr(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    if (ChannelIsInEc1Line(self, channel))
        return cAf6_Ec1Reg_upvtchstkram_Base;

    return cAf6Reg_upvtchstkram;
    }

static uint32 OcnVtTuPointerInterpreterPerChannelAlarmStatusRegAddr(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    if (ChannelIsInEc1Line(self, channel))
        return cAf6_Ec1Reg_upvtchstaram_Base;

    return cAf6Reg_upvtchstaram;
    }

static uint32 OcnVtTuPointerGeneratorPerChannelInterruptRegAddr(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    if (ChannelIsInEc1Line(self, channel))
        return cAf6_Ec1Reg_vtpgstkram_Base;

    return cAf6Reg_vtpgstkram;
    }

static uint32 OcnVtTuPointerGeneratorNdfIntrMask(Tha60210011ModuleOcn self)
    {
    AtUnused(self);
    return cAf6_vtpgstkram_VtPgNdfIntr_Mask;
    }

static uint32 OcnStsPointerInterpreterPerChannelInterruptRegAddr(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    if (ChannelIsInEc1Line(self, channel))
        return cAf6_Ec1Reg_upstschstkram_Base;

    return cAf6Reg_upstschstkram;
    }

static uint32 OcnStsPointerInterpreterPerChannelAlarmStatusRegAddr(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    if (ChannelIsInEc1Line(self, channel))
        return cAf6_Ec1Reg_upstschstaram_Base;

    return cAf6Reg_upstschstaram;
    }

static uint32 OcnVtTuPointerInterpreterPerChannelAdjustCountRegAddr(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    if (ChannelIsInEc1Line(self, channel))
        return cAf6_Ec1Reg_adjcntperstkram_Base;

    return cAf6Reg_adjcntperstkram;
    }

static uint32 OcnVtTuPointerGeneratorPerChannelAdjustCountRegAddr(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    if (ChannelIsInEc1Line(self, channel))
        return cAf6_Ec1Reg_adjcntpgpervtram_Base;

    return cAf6Reg_adjcntpgpervtram;
    }

static void OverrideThaModuleOcn(AtModule self)
    {
    ThaModuleOcn ocnModule = (ThaModuleOcn)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleOcnMethods = mMethodsGet(ocnModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleOcnOverride, m_ThaModuleOcnMethods, sizeof(m_ThaModuleOcnOverride));

        mMethodOverride(m_ThaModuleOcnOverride, DefaultSet);
        mMethodOverride(m_ThaModuleOcnOverride, PiStsConcatMasterSet);
        mMethodOverride(m_ThaModuleOcnOverride, PgStsConcatMasterSet);
        mMethodOverride(m_ThaModuleOcnOverride, CanMovePathAisRxForcingPoint);
        mMethodOverride(m_ThaModuleOcnOverride, PathAisRxForcingPointMove);
        mMethodOverride(m_ThaModuleOcnOverride, PathAisRxForcingPointIsMoved);
        mMethodOverride(m_ThaModuleOcnOverride, TohGlobalS1StableMonitoringThresholdControlRegAddr);
        mMethodOverride(m_ThaModuleOcnOverride, TohGlobalK1StableMonitoringThresholdControlRegAddr);
        mMethodOverride(m_ThaModuleOcnOverride, TohGlobalK2StableMonitoringThresholdControlRegAddr);
        mMethodOverride(m_ThaModuleOcnOverride, StsVtDefaultOffset);
        mMethodOverride(m_ThaModuleOcnOverride, RxStsPayloadControl);
        mMethodOverride(m_ThaModuleOcnOverride, TxStsMultiplexingControl);
        mMethodOverride(m_ThaModuleOcnOverride, LineIdSw2HwGet);
        mMethodOverride(m_ThaModuleOcnOverride, Vc3ToTu3VcEnable);
        mMethodOverride(m_ThaModuleOcnOverride, Vc3ToTu3VcIsEnabled);
        }

    mMethodsSet(ocnModule, &m_ThaModuleOcnOverride);
    }

static void OverrideTha60210011ModuleOcn(AtModule self)
    {
    Tha60210011ModuleOcn module = (Tha60210011ModuleOcn)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011ModuleOcnMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModuleOcnOverride, m_Tha60210011ModuleOcnMethods, sizeof(m_Tha60210011ModuleOcnOverride));

        mMethodOverride(m_Tha60210011ModuleOcnOverride, HwStsDefaultOffset);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, RxSxcControlRegAddr);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, TxSxcControlRegAddr);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, LoStsPayloadHwFormula);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, OcnVtTuPointerInterpreterPerChannelControlRegAddr);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, OcnVtTuPointerGeneratorPerChannelControlRegAddr);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, OcnStsPointerInterpreterPerChannelControlRegAddr);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, OcnStsPointerGeneratorPerChannelControlRegAddr);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, OcnRxHoMapConcatenateReg);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, SdhChannelSwStsId2HwFlatId);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, SliceOfChannel);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, HoPathPhyModuleIsDividedToOc24);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, Oc48HwStsAndSliceConvertToOc24);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, LoPathPhyModuleIsDividedToBlockOc24);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, OcnStsPointerInterpreterPerChannelAdjustCountRegAddr);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, OcnStsPointerGeneratorPerChannelAdjustCountRegAddr);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, OcnStsPointerGeneratorPerChannelInterruptRegAddr);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, OcnStsPointerInterpreterPerChannelInterruptRegAddr);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, OcnStsPointerInterpreterPerChannelAlarmStatusRegAddr);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, OcnVtTuPointerInterpreterPerChannelInterruptRegAddr);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, OcnVtTuPointerInterpreterPerChannelAlarmStatusRegAddr);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, OcnVtTuPointerGeneratorPerChannelInterruptRegAddr);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, OcnVtTuPointerGeneratorNdfIntrMask);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, OcnVtTuPointerInterpreterPerChannelAdjustCountRegAddr);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, OcnVtTuPointerGeneratorPerChannelAdjustCountRegAddr);
        }

    mMethodsSet(module, &m_Tha60210011ModuleOcnOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleOcn(self);
    OverrideTha60210011ModuleOcn(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061ModuleOcn);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012ModuleOcnObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210061ModuleOcnNew(AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newModule, device);
    }

uint32 Tha60210061ModuleOcnEc1BaseAddress(ThaModuleOcn self)
    {
    AtUnused(self);
    return 0xA80000;
    }

void Tha60210061ModuleOcnVirtualFifoCounterEnable(ThaModuleOcn self, eBool enable)
    {
    uint32 address, regVal;
    if (self == NULL)
        return;

    address = cAf6_Ec1Reg_glbspi_reg_Base + Tha60210061ModuleOcnEc1BaseAddress(self);
    regVal = mModuleHwRead(self, address);
    mRegFieldSet(regVal, cAf6_glbspi_reg_StsPiAdjCntSel_, mBoolToBin(enable));
    mModuleHwWrite(self, address, regVal);
    }

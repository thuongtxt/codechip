/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60210061PdhSerialLine.c
 *
 * Created Date: Dec 16, 2016
 *
 * Description : PDH Serial Line
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModulePdh.h"
#include "../../Tha60210031/pdh/Tha60210031PdhSerialLineInternal.h"
#include "../pdh/Tha60210061ModulePdh.h"
#include "Tha60210061PdhSerialLine.h"
#include "Tha60210061PdhDe3.h"
#include "Tha60210061LiuReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define cAf6_LIU_Rx_loopin_LiuLoopback_Mask(liuId)  (cBit0 << (liuId))
#define cAf6_LIU_Rx_loopin_LiuLoopback_Shift(liuId) (0 + (liuId))

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061PdhSerialLine * Tha60210061PdhSerialLine;
typedef struct tTha60210061PdhSerialLine
    {
    tTha60210031PdhSerialLine super;
    }tTha60210061PdhSerialLine;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods                m_AtChannelOverride;
static tAtPdhSerialLineMethods          m_AtPdhSerialLineOverride;
static tThaPdhDe3SerialLineMethods      m_ThaPdhDe3SerialLineOverride;

/* Save super implementation */
static const tAtPdhSerialLineMethods          *m_AtPdhSerialLineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModulePdh PdhModule(ThaPdhDe3SerialLine self)
    {
    return (AtModulePdh)AtChannelModuleGet((AtChannel)self);
    }

static AtPdhDe3 De3ObjectGet(ThaPdhDe3SerialLine self)
    {
    uint32 lineId = AtChannelIdGet((AtChannel)self);
    return Tha60210061PdhDe3Get(PdhModule(self), lineId);
    }

static eAtRet ModeSet(AtPdhSerialLine self, uint32 mode)
    {
    return m_AtPdhSerialLineMethods->ModeSet(self, mode);
    }

static uint32 LiuBaseAddress(void)
    {
    return Tha60210061ModulePdhLiuBaseAddress();
    }

static eAtRet MapEc1Line(ThaPdhDe3SerialLine self, AtSdhLine line)
    {
    uint32 portId  = AtChannelIdGet((AtChannel)self);
    uint32 regAddr;
    uint32 regVal;
    AtUnused(line);

    /* Select Tx Mode */
    regAddr = cAf6Reg_LIU_Tx_Channel_Lookup_Base + portId + LiuBaseAddress();
    regVal = mChannelHwRead(self, regAddr, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_LIU_Tx_Channel_Lookup_LIUTxModeSel_, 4);
    mRegFieldSet(regVal, cAf6_LIU_Tx_Channel_Lookup_LIUTxBusSel_, 4);
    mRegFieldSet(regVal, cAf6_LIU_Tx_Channel_Lookup_LIUTxPDHID_, portId << 5);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePdh);

    /* Select Rx Mode */
    regAddr = cAf6Reg_LIU_Rx_Channel_Lookup_Base + portId + LiuBaseAddress();
    regVal = mChannelHwRead(self, regAddr, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_LIU_Rx_Channel_Lookup_LIURxModeSel_, 4);
    mRegFieldSet(regVal, cAf6_LIU_Rx_Channel_Lookup_LIURxBusSel_, 4);
    mRegFieldSet(regVal, cAf6_LIU_Rx_Channel_Lookup_LIURxPDHID_, portId << 5);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

static eAtRet MapDe3Line(ThaPdhDe3SerialLine self, AtPdhDe3 de3)
    {
    uint32 regAddr, regVal;
    uint32 portId  = AtChannelIdGet((AtChannel)self);
    uint32 pdhId   = (uint32)(32 * (cStartStsIdForDe3LiuPort + (portId / cNumLiuDe3PerSlice)));
    uint32 busId   = portId % cNumLiuDe3PerSlice;
    eAtPdhDe3SerialLineMode mode = AtPdhSerialLineModeGet((AtPdhSerialLine)self);
    uint8 hwLiuMode = (mode == cAtPdhDe3SerialLineModeE3) ? 2 : 3;
    AtUnused(de3);

    regAddr = cAf6Reg_LIU_Tx_Channel_Lookup_Base + AtChannelIdGet((AtChannel)self) + LiuBaseAddress();
    regVal  = mChannelHwRead(self, regAddr, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_LIU_Tx_Channel_Lookup_LIUTxModeSel_, hwLiuMode);
    mRegFieldSet(regVal, cAf6_LIU_Tx_Channel_Lookup_LIUTxBusSel_, busId);
    mRegFieldSet(regVal, cAf6_LIU_Tx_Channel_Lookup_LIUTxPDHID_, pdhId);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePdh);

    /* Select Rx Mode */
    regAddr = cAf6Reg_LIU_Rx_Channel_Lookup_Base + AtChannelIdGet((AtChannel)self) + LiuBaseAddress();
    regVal = mChannelHwRead(self, regAddr, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_LIU_Rx_Channel_Lookup_LIURxModeSel_, hwLiuMode);
    mRegFieldSet(regVal, cAf6_LIU_Rx_Channel_Lookup_LIURxBusSel_, busId);
    mRegFieldSet(regVal, cAf6_LIU_Rx_Channel_Lookup_LIURxPDHID_, pdhId);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

static eAtRet LoopInEnable(AtChannel self, eBool enable)
    {
    uint32 portId  = AtChannelIdGet(self);
    uint32 regAddr = cAf6Reg_LIU_Rx_loopin_Base + LiuBaseAddress();
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModulePdh);

    mFieldIns(&regVal,
              cAf6_LIU_Rx_loopin_LiuLoopback_Mask(portId),
              cAf6_LIU_Rx_loopin_LiuLoopback_Shift(portId),
              mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

static eBool LoopInIsEnabled(AtChannel self)
    {
    uint32 portId  = AtChannelIdGet(self);
    uint8 hwValue;
    uint32 regAddr = cAf6Reg_LIU_Rx_loopin_Base + LiuBaseAddress();
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModulePdh);

    mFieldGet(regVal,
              cAf6_LIU_Rx_loopin_LiuLoopback_Mask(portId),
              cAf6_LIU_Rx_loopin_LiuLoopback_Shift(portId),
              uint8,
              &hwValue);

    return hwValue ? cAtTrue : cAtFalse;
    }

static eAtRet LoopOutEnable(AtChannel self, eBool enable)
    {
    uint32 portId  = AtChannelIdGet(self);
    uint32 regAddr = cAf6Reg_LIU_Tx_loopout_Base + LiuBaseAddress();
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModulePdh);

    mFieldIns(&regVal,
              cAf6_LIU_Rx_loopin_LiuLoopback_Mask(portId),
              cAf6_LIU_Rx_loopin_LiuLoopback_Shift(portId),
              mBoolToBin(enable));

    mChannelHwWrite(self, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

static eBool LoopOutIsEnabled(AtChannel self)
    {
    uint32 portId  = AtChannelIdGet((AtChannel)self);
    uint8 hwValue;
    uint32 regAddr = cAf6Reg_LIU_Tx_loopout_Base + LiuBaseAddress();
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModulePdh);

    mFieldGet(regVal,
              cAf6_LIU_Rx_loopin_LiuLoopback_Mask(portId),
              cAf6_LIU_Rx_loopin_LiuLoopback_Shift(portId),
              uint8,
              &hwValue);

    return hwValue ? cAtTrue : cAtFalse;
    }

static eAtRet LoopbackSet(AtChannel self, uint8 loopbackMode)
    {
    eAtRet ret = cAtOk;

    switch (loopbackMode)
        {
        case cAtLoopbackModeLocal:
            ret |= LoopInEnable(self, cAtTrue);
            ret |= LoopOutEnable(self, cAtFalse);
            break;

        case cAtLoopbackModeRemote:
            ret |= LoopInEnable(self, cAtFalse);
            ret |= LoopOutEnable(self, cAtTrue);
            break;

        case cAtLoopbackModeRelease:
            ret |= LoopInEnable(self, cAtFalse);
            ret |= LoopOutEnable(self, cAtFalse);
            break;

        default: return cAtErrorModeNotSupport;
        }

    return ret;
    }

static uint8 LoopbackGet(AtChannel self)
    {
    if (LoopInIsEnabled(self))
        return cAtLoopbackModeLocal;

    if (LoopOutIsEnabled(self))
        return cAtLoopbackModeRemote;

    return cAtLoopbackModeRelease;
    }

static eBool LoopbackIsSupported(AtChannel self, uint8 loopbackMode)
    {
    AtUnused(self);
    return (loopbackMode == cAtLoopbackModeDual) ? cAtFalse : cAtTrue;
    }

static void OverrideAtPdhSerialLine(AtPdhSerialLine self)
    {
    AtPdhSerialLine serialLine = (AtPdhSerialLine)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPdhSerialLineMethods = mMethodsGet(serialLine);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPdhSerialLineOverride, m_AtPdhSerialLineMethods, sizeof(tAtPdhSerialLineMethods));

        mMethodOverride(m_AtPdhSerialLineOverride, ModeSet);
        }

    mMethodsSet(serialLine, &m_AtPdhSerialLineOverride);
    }

static void OverrideThaPdhDe3SerialLine(AtPdhSerialLine self)
    {
    ThaPdhDe3SerialLine serialLine = (ThaPdhDe3SerialLine)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhDe3SerialLineOverride, mMethodsGet(serialLine), sizeof(m_ThaPdhDe3SerialLineOverride));

        mMethodOverride(m_ThaPdhDe3SerialLineOverride, De3ObjectGet);
        mMethodOverride(m_ThaPdhDe3SerialLineOverride, MapEc1Line);
        mMethodOverride(m_ThaPdhDe3SerialLineOverride, MapDe3Line);
        }

    mMethodsSet(serialLine, &m_ThaPdhDe3SerialLineOverride);
    }

static void OverrideAtChannel(AtPdhSerialLine self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, LoopbackIsSupported);
        mMethodOverride(m_AtChannelOverride, LoopbackSet);
        mMethodOverride(m_AtChannelOverride, LoopbackGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtPdhSerialLine self)
    {
    OverrideAtChannel(self);
    OverrideAtPdhSerialLine(self);
    OverrideThaPdhDe3SerialLine(self);
    }

static AtPdhSerialLine ObjectInit(AtPdhSerialLine self, uint32 channelId, AtModulePdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tTha60210031PdhSerialLine));

    /* Supper constructor */
    if (Tha60210031PdhSerialLineObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPdhSerialLine Tha60210061PdhSerialLineNew(uint32 channelId, AtModulePdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhSerialLine newSerialLine = mMethodsGet(osal)->MemAlloc(osal, sizeof(tTha60210061PdhSerialLine));
    if (newSerialLine == NULL)
        return NULL;

    /* construct it */
    return ObjectInit(newSerialLine, channelId, module);
    }

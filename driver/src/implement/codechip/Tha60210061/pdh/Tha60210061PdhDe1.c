/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60210061PdhDe1.c
 *
 * Created Date: Aug 12, 2016
 *
 * Description : LIU DE1 of 60210061 product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/concate/binder/AtVcgBinder.h"
#include "../../../../generic/concate/AtModuleConcateInternal.h"
#include "../../../default/man/ThaDevice.h"
#include "../../../default/man/versionreader/ThaVersionReader.h"
#include "../../Tha60210012/pdh/Tha60210012PdhDe1Internal.h"
#include "../../Tha60210012/pwe/Tha60210012ModulePweBlockManager.h"
#include "../../Tha60210012/common/Tha60210012Channel.h"
#include "Tha60210061LiuReg.h"
#include "Tha60210061ModulePdh.h"
#include "Tha60210061PdhDe1.h"
#include "retimingcontrollers/Tha60210061PdhDe1RetimingController.h"

/*--------------------------- Define -----------------------------------------*/
#define cStartDe1LiuId 4
typedef AtBerController (*BerControllerGet)(AtPdhChannel);

/*--------------------------- Macros -----------------------------------------*/
#define mDs1E1J1LiuOffset(self) Ds1E1J1LiuOffset((AtPdhChannel)self)
#define mThis(self) ((tTha60210061PdhDe1 *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061PdhDe1
    {
    tTha60210012PdhVcDe1 super;

    /* Private data */
    eBool isInInitProgress;
    uint16 frameType;
    }tTha60210061PdhDe1;

typedef struct tBerAllConfiguration
    {
    eBool needToRestore;
    eBool berIsLineLayer;
    eAtBerRate sd;
    eAtBerRate sf;
    eAtBerRate tca;
    }tBerAllConfiguration;
/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtPdhChannelMethods m_AtPdhChannelOverride;
static tThaPdhDe1Methods    m_ThaPdhDe1Override;
static tAtChannelMethods    m_AtChannelOverride;

/* Save super implementation */
static const tAtObjectMethods     *m_AtObjectMethods     = NULL;
static const tAtPdhChannelMethods *m_AtPdhChannelMethods = NULL;
static const tThaPdhDe1Methods    *m_ThaPdhDe1Methods    = NULL;
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool FrameTypeIsSwappedDs1E1Mode(AtPdhChannel self, uint16 frameType)
    {
    return (AtPdhDe1FrameTypeIsE1(mThis(self)->frameType) == AtPdhDe1FrameTypeIsE1(frameType)) ? cAtFalse : cAtTrue;
    }

static uint32 Ds1E1J1LiuOffset(AtPdhChannel self)
    {
    return AtChannelIdGet((AtChannel)self) + cStartDe1LiuId;
    }

static uint32 FrameModeSw2Hw(uint32 frameMode)
    {
    return AtPdhDe1FrameTypeIsE1(frameMode) ? 1 : 0;
    }

static uint32 LiuBaseAddress(void)
    {
    return Tha60210061ModulePdhLiuBaseAddress();
    }

static eAtRet De1LiuModeSet(AtPdhDe1 self, uint16 frameMode)
    {
    uint32 regVal, regAddr;
    uint32 de1DefaultOffset = mMethodsGet((ThaPdhDe1)self)->Ds1E1J1DefaultOffset((ThaPdhDe1)self);
    uint8 sliceId, stsId;

    ThaPdhChannelHwIdGet((AtPdhChannel)self, cAtModulePdh, &sliceId, &stsId);

    regAddr = cAf6Reg_LIU_Tx_Channel_Lookup_Base + mDs1E1J1LiuOffset(self) + LiuBaseAddress();
    regVal = mChannelHwRead(self, regAddr, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_LIU_Rx_Channel_Lookup_LIURxPDHID_, de1DefaultOffset);
    mRegFieldSet(regVal, cAf6_LIU_Tx_Channel_Lookup_LIUTxModeSel_, FrameModeSw2Hw(frameMode));
    mRegFieldSet(regVal, cAf6_LIU_Rx_Channel_Lookup_LIURxBusSel_, sliceId);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePdh);

    regAddr = cAf6Reg_LIU_Rx_Channel_Lookup_Base + mDs1E1J1LiuOffset(self) + LiuBaseAddress();
    regVal = mChannelHwRead(self, regAddr, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_LIU_Rx_Channel_Lookup_LIURxPDHID_, de1DefaultOffset);
    mRegFieldSet(regVal, cAf6_LIU_Rx_Channel_Lookup_LIURxModeSel_, FrameModeSw2Hw(frameMode));
    mRegFieldSet(regVal, cAf6_LIU_Rx_Channel_Lookup_LIURxBusSel_, sliceId);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

static void InInitProgressSet(AtPdhChannel self, eBool enable)
    {
    mThis(self)->isInInitProgress = enable;
    }

static eBool IsInInitProgress(AtPdhChannel self)
    {
    return mThis(self)->isInInitProgress;
    }

static void BerBackUpFunc(AtBerController controller, tBerAllConfiguration* allConfig)
    {
    allConfig->needToRestore = AtBerControllerIsEnabled(controller);

    if (!allConfig->needToRestore)
        return;

    allConfig->sf  = AtBerControllerSfThresholdGet(controller);
    allConfig->sd  = AtBerControllerSdThresholdGet(controller);
    allConfig->tca = AtBerControllerTcaThresholdGet(controller);
    }

static void BerBackUp(AtPdhChannel self, tBerAllConfiguration* allConfig)
    {
    if (self->pathBerController)
        {
        BerBackUpFunc(self->pathBerController, allConfig);
        if (allConfig->needToRestore)
        	{
       		allConfig->berIsLineLayer = cAtFalse;
	        return;
    	    }
		}
		
    if (self->lineBerController)
        {
        BerBackUpFunc(self->lineBerController, allConfig);
        if (allConfig->needToRestore)
            allConfig->berIsLineLayer = cAtTrue;
        }
    }

static eAtRet BerRestore(AtPdhChannel self, tBerAllConfiguration* allConfig)
    {
    AtBerController controller;
    eAtRet ret = cAtOk;

    if (allConfig->needToRestore == cAtFalse)
        return ret;

    /* Get Controller */
    if (allConfig->berIsLineLayer)
        controller = AtPdhChannelLineBerControllerGet(self);
    else
        controller = AtPdhChannelPathBerControllerGet(self);

    /* Re-Configure when change DS1 to E1 mode and otherwise */
    ret |= AtBerControllerSfThresholdSet(controller, allConfig->sf);
    ret |= AtBerControllerSdThresholdSet(controller, allConfig->sd);
    ret |= AtBerControllerTcaThresholdSet(controller, allConfig->tca);
    ret |= AtBerControllerEnable(controller, cAtTrue);

    return ret;
    }

static eAtRet FrameTypeSet(AtPdhChannel self, uint16 frameType)
    {
    tBerAllConfiguration allConfig;
    eAtRet ret = cAtOk;
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet((AtChannel)self);
    eAtRet dryRunRet = ThaPdhDe1BeforeChangeFrameTypeCheck(self, frameType);
    uint32 interruptMaskBackup = 0;

    if (dryRunRet != cAtRetMoveNext)
        return dryRunRet;

    AtOsalMemInit(&allConfig, 0, sizeof(allConfig));
    if (!IsInInitProgress(self) && FrameTypeIsSwappedDs1E1Mode(self, frameType))
        {
        /* Back Up BER Info */
        BerBackUp(self, &allConfig);
        /* Backup interrupt mask and clear it in HW to avoid flooding */
        interruptMaskBackup = AtChannelInterruptMaskGet((AtChannel)self);
        AtChannelInterruptMaskSet((AtChannel)self, interruptMaskBackup, 0);

        /* Backup interrupt mask and clear it in HW to avoid flooding */
        interruptMaskBackup = AtChannelInterruptMaskGet((AtChannel)self);
        AtChannelInterruptMaskSet((AtChannel)self, interruptMaskBackup, 0);

        ret |= Tha60210061ModulePdhLiuDe1FramerDeallocate(pdhModule, (AtPdhDe1)self, AtPdhDe1FrameTypeIsE1(mThis(self)->frameType));
        ret |= Tha60210061ModulePdhLiuDe1FramerAllocate(pdhModule, (AtPdhDe1)self, AtPdhDe1FrameTypeIsE1(frameType));
        if (ret != cAtOk)
            return ret;

        /* Reset CDR controller match with DE1 frame type */
        ThaPdhDe1CdrControllerDelete((ThaPdhDe1)self);

        InInitProgressSet(self, cAtTrue);
        ret |= AtChannelInit((AtChannel)self);

        /* Reconfigure interrupt mask */
        AtChannelInterruptMaskSet((AtChannel)self, interruptMaskBackup, interruptMaskBackup);
        InInitProgressSet(self, cAtFalse);
        if (ret != cAtOk)
            return ret;
        }

    /* Apply to its LIU */
    De1LiuModeSet((AtPdhDe1)self, frameType);
    mThis(self)->frameType = frameType;

    /* Restore BER Info */
    ret = BerRestore(self, &allConfig);
    if (ret != cAtOk)
        return ret;

    return m_AtPdhChannelMethods->FrameTypeSet(self, frameType);
    }

static void Delete(AtObject self)
    {
    AtSdhVc vc;
    if ((vc = AtPdhChannelVcInternalGet((AtPdhChannel)self)) != NULL)
        {
        AtObjectDelete((AtObject)vc);
        AtPdhChannelVcSet((AtPdhChannel)self, NULL);
        }

    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static uint8 PartId(ThaPdhDe1 self)
    {
    AtUnused(self);
    return 0;
    }

static const char *IdString(AtChannel self)
    {
    static char idString[16];

    AtSprintf(idString, "%u", AtChannelIdGet(self) + 1);

    return idString;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    /* Default reset this loopcode */
    if (!AtPdhDe1IsE1((AtPdhDe1)self))
        ret = AtPdhDe1TxLoopcodeSet((AtPdhDe1)self, cAtPdhDe1LoopcodeInvalid);

    return ret;
    }

static eAtRet DefaultThreshold(ThaPdhDe1 self, tThaCfgPdhDs1E1FrmRxThres *threshold)
    {
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet((AtChannel)self);
    eAtRet ret = ThaModulePdhDefaultDe1Threshold(pdhModule, threshold);
    if (ret != cAtOk)
        return ret;

    threshold->losCntThres = 3;
    return cAtOk;
    }

static AtSdhVc VcGet(AtPdhChannel self)
    {
    AtUnused(self);
    return NULL;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210061PdhDe1 *object = (tTha60210061PdhDe1 *)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeUInt(isInInitProgress);
    mEncodeUInt(frameType);
    }

static ThaModulePdh ModulePdh(AtChannel self)
    {
    return (ThaModulePdh)AtChannelModuleGet(self);
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    uint32 address;

    if (counterType != cAtPdhDe1CounterBpvExz)
        return m_AtChannelMethods->CounterGet(self, counterType);

    if (!Tha60210061ModulePdhBpvCounterIsSupported(ModulePdh(self)))
        return 0;

    address = cAf6_LIU_Rx_BPV_Counter_RO + LiuBaseAddress() + mDs1E1J1LiuOffset(self);
    return mChannelHwRead(self, address, cAtModulePdh);
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    uint32 address;

    if (counterType != cAtPdhDe1CounterBpvExz)
        return m_AtChannelMethods->CounterClear(self, counterType);

    if (!Tha60210061ModulePdhBpvCounterIsSupported(ModulePdh(self)))
        return 0;

    address = cAf6_LIU_Rx_BPV_Counter_R2C + LiuBaseAddress() + mDs1E1J1LiuOffset(self);
    return mChannelHwRead(self, address, cAtModulePdh);
    }

static eBool ShouldConsiderLofWhenLos(ThaPdhDe1 self)
    {
    /* As this work with LIU */
    AtUnused(self);
    return cAtTrue;
    }

static eBool RetimingIsSupported(ThaPdhDe1 self)
    {
    uint32 supportedVer = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x3, 0x2, 0x1050);
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);

    return hwVersion >= supportedVer ? cAtTrue : cAtFalse;
    }

static ThaPdhRetimingController RetimingControllerCreate(ThaPdhDe1 self)
    {
    return (ThaPdhRetimingController)Tha60210061PdhDe1RetimingControllerNew((AtPdhChannel)self);
    }

static eBool HasLineLayer(AtPdhChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool FrameTypeIsSupported(AtPdhChannel self, uint16 frameType)
    {
    AtUnused(self);
    return (mOutOfRange(frameType, cAtPdhDs1J1UnFrm, cAtPdhE1MFCrc)) ? cAtFalse : cAtTrue;
    }

static eAtRet LoopOutEnable(AtPdhDe1 self, eBool enable)
    {
    uint32 address = cAf6Reg_LIU_Tx_loopout_Base + LiuBaseAddress();
    uint32 regVal = mChannelHwRead(self, address, cAtModulePdh);
    uint32 loopbackMask = cBit0 << mDs1E1J1LiuOffset(self);
    uint32 loopbackShift = mDs1E1J1LiuOffset(self);

    mRegFieldSet(regVal, loopback, mBoolToBin(enable));
    mChannelHwWrite(self, address, regVal, cAtModulePdh);

    return cAtOk;
    }

static eBool LoopOutIsEnabled(AtPdhDe1 self)
    {
    uint32 address = cAf6Reg_LIU_Tx_loopout_Base + LiuBaseAddress();
    uint32 regVal = mChannelHwRead(self, address, cAtModulePdh);
    uint32 loopbackMask = cBit0 << mDs1E1J1LiuOffset(self);
    uint32 loopbackShift = mDs1E1J1LiuOffset(self);

    return (mRegField(regVal, loopback)) ? cAtTrue : cAtFalse;
    }

static eAtRet LoopInEnable(AtPdhDe1 self, eBool enable)
    {
    uint32 address = cAf6Reg_LIU_Rx_loopin_Base + LiuBaseAddress();
    uint32 regVal = mChannelHwRead(self, address, cAtModulePdh);
    uint32 loopbackMask = cBit0 << mDs1E1J1LiuOffset(self);
    uint32 loopbackShift = mDs1E1J1LiuOffset(self);

    mRegFieldSet(regVal, loopback, mBoolToBin(enable));
    mChannelHwWrite(self, address, regVal, cAtModulePdh);

    return cAtOk;
    }

static eBool LoopInIsEnabled(AtPdhDe1 self)
    {
    uint32 address = cAf6Reg_LIU_Rx_loopin_Base + LiuBaseAddress();
    uint32 regVal = mChannelHwRead(self, address, cAtModulePdh);
    uint32 loopbackMask = cBit0 << mDs1E1J1LiuOffset(self);
    uint32 loopbackShift = mDs1E1J1LiuOffset(self);

    return (mRegField(regVal, loopback)) ? cAtTrue : cAtFalse;
    }

static eAtRet LoopbackSet(AtChannel self, uint8 loopbackMode)
    {
    eAtRet ret;

    ret = m_AtChannelMethods->LoopbackSet(self, loopbackMode);
    if (ret != cAtOk)
        return ret;

    if (loopbackMode == cAtPdhLoopbackModeLocalLine)
        Tha60210061PdhDe1LiuLoopbackSet((AtPdhDe1)self, cAtLoopbackModeLocal);
    else
        Tha60210061PdhDe1LiuLoopbackSet((AtPdhDe1)self, cAtLoopbackModeRelease);

    return cAtOk;
    }

static void OverrideAtChannel(AtPdhDe1 self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, IdString);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, LoopbackSet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtObject(AtPdhDe1 self)
    {
    AtObject object = (AtObject)self;

    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtPdhChannel(AtPdhDe1 self)
    {
    AtPdhChannel channel = (AtPdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPdhChannelOverride, m_AtPdhChannelMethods, sizeof(tAtPdhChannelMethods));

        mMethodOverride(m_AtPdhChannelOverride, FrameTypeSet);
        mMethodOverride(m_AtPdhChannelOverride, VcGet);
        mMethodOverride(m_AtPdhChannelOverride, HasLineLayer);
        mMethodOverride(m_AtPdhChannelOverride, FrameTypeIsSupported);
        }

    mMethodsSet(channel, &m_AtPdhChannelOverride);
    }

static void OverrideThaPdhDe1(AtPdhDe1 self)
    {
    ThaPdhDe1 de1 = (ThaPdhDe1)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPdhDe1Methods = mMethodsGet(de1);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhDe1Override, m_ThaPdhDe1Methods, sizeof(tThaPdhDe1Methods));

        mMethodOverride(m_ThaPdhDe1Override, PartId);
        mMethodOverride(m_ThaPdhDe1Override, DefaultThreshold);
        mMethodOverride(m_ThaPdhDe1Override, ShouldConsiderLofWhenLos);
        mMethodOverride(m_ThaPdhDe1Override, RetimingControllerCreate);
        mMethodOverride(m_ThaPdhDe1Override, RetimingIsSupported);
        }

    mMethodsSet(de1, &m_ThaPdhDe1Override);
    }

static void Override(AtPdhDe1 self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtPdhChannel(self);
    OverrideThaPdhDe1(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061PdhDe1);
    }

static AtPdhDe1 ObjectInit(AtPdhDe1 self, uint32 channelId, AtModulePdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Supper constructor */
    if (Tha60210012PdhVcDe1ObjectInit(self, NULL, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Correct channel ID */
    AtChannelIdSet((AtChannel)self, channelId);

    return self;
    }

AtPdhDe1 Tha60210061PdhDe1New(uint32 channelId, AtModulePdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhDe1 newDe1 = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDe1 == NULL)
        return NULL;

    /* construct it */
    return ObjectInit(newDe1, channelId, module);
    }

eBool Tha60210061PdhVcDe1IsE1(AtPdhChannel channel)
    {
    eAtSdhChannelType vcType = AtSdhChannelTypeGet((AtSdhChannel)AtPdhChannelVcInternalGet(channel));
    return (vcType == cAtSdhChannelTypeVc12) ? cAtTrue : cAtFalse;
    }

uint32 Th60210061PdhDe1LiuPdhIdGet(AtPdhDe1 self, uint8 *sliceId)
    {
    uint32 regVal, regAddr;

    if (self == NULL)
        return cInvalidUint32;

    regAddr = cAf6Reg_LIU_Tx_Channel_Lookup_Base + mDs1E1J1LiuOffset(self) + LiuBaseAddress();
    regVal = mChannelHwRead(self, regAddr, cAtModulePdh);
    *sliceId = (uint8)mRegField(regVal, cAf6_LIU_Rx_Channel_Lookup_LIURxBusSel_);
    return mRegField(regVal, cAf6_LIU_Tx_Channel_Lookup_LIUTxPDHID_);
    }

AtPdhDe1 Tha60210061De1LiuObjectFromHwId(AtModulePdh pdhModule, uint8 slice, uint8 sts, uint8 vtgId, uint8 vtId)
    {
    AtPdhDe1 de1FromLiu = NULL;
    uint32 de1Id, de1StsId;
    uint8 liuSlice;

    de1StsId = (sts * 32UL) + (vtgId * 4UL) + vtId;
    for (de1Id = 0; de1Id < AtModulePdhNumberOfDe1sGet(pdhModule); de1Id++)
        {
        AtPdhDe1 de1Object = AtModulePdhDe1Get(pdhModule, de1Id);
        uint32 liuDe1Id = Th60210061PdhDe1LiuPdhIdGet(de1Object, &liuSlice);

        if ((de1StsId == liuDe1Id) && (liuSlice == slice))
            de1FromLiu = de1Object;
        }

    return de1FromLiu;
    }

uint32 Tha60210061Ds1E1J1LiuOffset(AtPdhChannel self)
    {
    return Ds1E1J1LiuOffset(self);
    }

eAtRet Tha60210061PdhDe1LiuLoopbackSet(AtPdhDe1 self, uint8 loopbackMode)
    {
    eAtRet ret = cAtOk;
    if (self == NULL)
        return cAtErrorNullPointer;

    if (!AtPdhChannelHasLineLayer((AtPdhChannel)self))
        return cAtErrorModeNotSupport;

    /* Just one mode should be enabled at a time */
    switch (loopbackMode)
        {
        case cAtLoopbackModeLocal:
            ret |= LoopOutEnable(self, cAtFalse);
            ret |= LoopInEnable(self, cAtTrue);
            break;

        case cAtLoopbackModeRemote:
            ret |= LoopInEnable(self, cAtFalse);
            ret |= LoopOutEnable(self, cAtTrue);
            break;

        case cAtLoopbackModeRelease:
            ret |= LoopInEnable(self, cAtFalse);
            ret |= LoopOutEnable(self, cAtFalse);
            break;

        default:
            return cAtErrorModeNotSupport;
        }

    return ret;
    }

uint8 Tha60210061PdhDe1LiuLoopbackGet(AtPdhDe1 self)
    {
    if ((self == NULL) || (!AtPdhChannelHasLineLayer((AtPdhChannel)self)))
        return cAtLoopbackModeRelease;

    if (LoopInIsEnabled(self))
        return cAtLoopbackModeLocal;

    if (LoopOutIsEnabled(self))
        return cAtLoopbackModeRemote;

    return cAtLoopbackModeRelease;
    }

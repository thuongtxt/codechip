/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60210061PdhPrmController.c
 *
 * Created Date: Sep 14, 2017
 *
 * Description : PRM controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/versionreader/ThaVersionReader.h"
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60210031/pdh/Tha60210031PdhPrmControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210061PdhPrmController)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061PdhPrmController * Tha60210061PdhPrmController;
typedef struct tTha60210061PdhPrmController
	{
	tTha60210031PdhPrmController super;
	}tTha60210061PdhPrmController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPdhPrmControllerMethods m_ThaPdhPrmControllerOverride;

/* Super implementation */
static const tThaPdhPrmControllerMethods *m_ThaPdhPrmControllerMethods = NULL;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 StartVersionHasRxEnabledCfgRegister(ThaPdhPrmController self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x3, 0x8, 0x1030);
    }

static void OverrideThaPdhPrmController(AtPdhPrmController self)
    {
    ThaPdhPrmController variable = (ThaPdhPrmController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPdhPrmControllerMethods = mMethodsGet(variable);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhPrmControllerOverride, m_ThaPdhPrmControllerMethods, sizeof(m_ThaPdhPrmControllerOverride));

        mMethodOverride(m_ThaPdhPrmControllerOverride, StartVersionHasRxEnabledCfgRegister);
        }

    mMethodsSet(variable, &m_ThaPdhPrmControllerOverride);
    }

static void Override(AtPdhPrmController self)
    {
    OverrideThaPdhPrmController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061PdhPrmController);
    }

static AtPdhPrmController ObjectInit(AtPdhPrmController self, AtPdhDe1 de1)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031PdhPrmControllerObjectInit(self, de1) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPdhPrmController Tha60210061PdhPrmControllerNew(AtPdhDe1 de1)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhPrmController newObject = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newObject, de1);
    }


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60210061PdhSerialLine.h
 * 
 * Created Date: Dec 16, 2016
 *
 * Description : PDH Serial Line
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061PDHSERIALLINE_H_
#define _THA60210061PDHSERIALLINE_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhSerialLine Tha60210061PdhSerialLineNew(uint32 channelId, AtModulePdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061PDHSERIALLINE_H_ */


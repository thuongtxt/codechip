/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60210061PdhDe1.h
 * 
 * Created Date: Aug 18, 2016
 *
 * Description : Tha60210061PdhDe1 declarations
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061PDHDE1_H_
#define _THA60210061PDHDE1_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eBool Tha60210061PdhVcDe1IsE1(AtPdhChannel channel);
uint32 Th60210061PdhDe1LiuPdhIdGet(AtPdhDe1 self, uint8 *sliceId);
AtPdhDe1 Tha60210061De1LiuObjectFromHwId(AtModulePdh pdhModule, uint8 slice, uint8 sts, uint8 vtgId, uint8 vtId);
uint32 Tha60210061Ds1E1J1LiuOffset(AtPdhChannel self);
eAtRet Tha60210061PdhDe1LiuLoopbackSet(AtPdhDe1 self, uint8 loopbackMode);
uint8 Tha60210061PdhDe1LiuLoopbackGet(AtPdhDe1 self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061PDHDE1_H_ */


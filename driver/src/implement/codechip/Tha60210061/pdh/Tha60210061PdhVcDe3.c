/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60210061PdhVcDe3.c
 *
 * Created Date: Sep 16, 2016
 *
 * Description : VC PDH De3
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210012/pdh/Tha60210012PdhDe3Internal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061PdhVcDe3
    {
    tTha60210012PdhDe3 super;
    }tTha60210061PdhVcDe3;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPdhDe3Methods         m_ThaPdhDe3Override;
static tTha60210031PdhDe3Methods m_Tha60210031PdhDe3Override;

/* Super implementation */
static const tThaPdhDe3Methods   *m_ThaPdhDe3Methods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool BpvCounterIsSupported(Tha60210031PdhDe3 self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool RetimingIsSupported(ThaPdhDe3 self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void OverrideThaPdhDe3(AtPdhDe3 self)
    {
    ThaPdhDe3 de3 = (ThaPdhDe3)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPdhDe3Methods = mMethodsGet(de3);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhDe3Override, m_ThaPdhDe3Methods, sizeof(m_ThaPdhDe3Override));

        mMethodOverride(m_ThaPdhDe3Override, RetimingIsSupported);
        }

    mMethodsSet(de3, &m_ThaPdhDe3Override);
    }

static void OverrideTha60210031PdhDe3(AtPdhDe3 self)
    {
    Tha60210031PdhDe3 channel = (Tha60210031PdhDe3)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210031PdhDe3Override, mMethodsGet(channel), sizeof(m_Tha60210031PdhDe3Override));

        mMethodOverride(m_Tha60210031PdhDe3Override, BpvCounterIsSupported);
        }

    mMethodsSet(channel, &m_Tha60210031PdhDe3Override);
    }

static void Override(AtPdhDe3 self)
    {
    OverrideThaPdhDe3(self);
    OverrideTha60210031PdhDe3(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061PdhVcDe3);
    }

static AtPdhDe3 ObjectInit(AtPdhDe3 self, uint32 channelId, AtModulePdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Supper constructor */
    if (Tha60210012PdhDe3ObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPdhDe3 Tha60210061PdhVcDe3New(uint32 channelId, AtModulePdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhDe3 newDe3 = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDe3 == NULL)
        return NULL;

    /* construct it */
    return ObjectInit(newDe3, channelId, module);
    }

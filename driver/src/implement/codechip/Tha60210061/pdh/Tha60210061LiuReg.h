/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60210061LiuReg.h
 * 
 * Created Date: Aug 16, 2016
 *
 * Description : LIU register description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061LIUREG_H_
#define _THA60210061LIUREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cNumLiuDe3PerSlice          2UL
/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/*------------------------------------------------------------------------------
Reg Name   : LIU Rx Clock Invert
Reg Addr   : 0x0000
Reg Formula:
    Where  :
Reg Desc   :
This register configures LIU Rx Clock Invert.

------------------------------------------------------------------------------*/
#define cAf6Reg_LIU_Rx_Clock_Invert_Base                                                                0x0000
#define cAf6Reg_LIU_Rx_Clock_Invert                                                                     0x0000
#define cAf6Reg_LIU_Rx_Clock_Invert_WidthVal                                                                32
#define cAf6Reg_LIU_Rx_Clock_Invert_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: Liurxinv
BitField Type: RW
BitField Desc: LIU Rx Clock Invert, each bit for each channel
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_LIU_Rx_Clock_Invert_Liurxinv_Bit_Start                                                          0
#define cAf6_LIU_Rx_Clock_Invert_Liurxinv_Bit_End                                                           15
#define cAf6_LIU_Rx_Clock_Invert_Liurxinv_Mask                                                        cBit15_0
#define cAf6_LIU_Rx_Clock_Invert_Liurxinv_Shift                                                              0
#define cAf6_LIU_Rx_Clock_Invert_Liurxinv_MaxVal                                                        0xffff
#define cAf6_LIU_Rx_Clock_Invert_Liurxinv_MinVal                                                           0x0
#define cAf6_LIU_Rx_Clock_Invert_Liurxinv_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : LIU Rx loopin
Reg Addr   : 0x0001
Reg Formula:
    Where  :
Reg Desc   :
This register configures LIU Rx loopin.

------------------------------------------------------------------------------*/
#define cAf6Reg_LIU_Rx_loopin_Base                                                                      0x0001
#define cAf6Reg_LIU_Rx_loopin                                                                           0x0001
#define cAf6Reg_LIU_Rx_loopin_WidthVal                                                                      32
#define cAf6Reg_LIU_Rx_loopin_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: Liurxloopin
BitField Type: RW
BitField Desc: LIU Rx loopin, each bit for each channel
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_LIU_Rx_loopin_Liurxloopin_Bit_Start                                                             0
#define cAf6_LIU_Rx_loopin_Liurxloopin_Bit_End                                                              15
#define cAf6_LIU_Rx_loopin_Liurxloopin_Mask                                                           cBit15_0
#define cAf6_LIU_Rx_loopin_Liurxloopin_Shift                                                                 0
#define cAf6_LIU_Rx_loopin_Liurxloopin_MaxVal                                                           0xffff
#define cAf6_LIU_Rx_loopin_Liurxloopin_MinVal                                                              0x0
#define cAf6_LIU_Rx_loopin_Liurxloopin_RstVal                                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : LIU Rx Channel Lookup
Reg Addr   : 0x0100 - 0x010F
Reg Formula: 0x0100 +  portid
    Where  :
           + $portid(0-15):
Reg Desc   :
The Control is use to configure for per LIU port Concatenation operation.

------------------------------------------------------------------------------*/
#define cAf6Reg_LIU_Rx_Channel_Lookup_Base                                                              0x0100
#define cAf6Reg_LIU_Rx_Channel_Lookup(portid)                                                (0x0100+(portid))
#define cAf6Reg_LIU_Rx_Channel_Lookup_WidthVal                                                              32
#define cAf6Reg_LIU_Rx_Channel_Lookup_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: LIURxPDHID
BitField Type: RW
BitField Desc: LIU Rx PDHID, In STSmode bit[4:0] = 0,
BitField Bits: [15:6]
--------------------------------------*/
#define cAf6_LIU_Rx_Channel_Lookup_LIURxPDHID_Bit_Start                                                      6
#define cAf6_LIU_Rx_Channel_Lookup_LIURxPDHID_Bit_End                                                       15
#define cAf6_LIU_Rx_Channel_Lookup_LIURxPDHID_Mask                                                    cBit15_6
#define cAf6_LIU_Rx_Channel_Lookup_LIURxPDHID_Shift                                                          6
#define cAf6_LIU_Rx_Channel_Lookup_LIURxPDHID_MaxVal                                                     0x3ff
#define cAf6_LIU_Rx_Channel_Lookup_LIURxPDHID_MinVal                                                       0x0
#define cAf6_LIU_Rx_Channel_Lookup_LIURxPDHID_RstVal                                                       0x0

/*--------------------------------------
BitField Name: LIURxModeSel
BitField Type: RW
BitField Desc: LIU Rx Mode select 4: EC1. 3: DS3. 2: E3. 1: E1. 0: DS1
BitField Bits: [5:3]
--------------------------------------*/
#define cAf6_LIU_Rx_Channel_Lookup_LIURxModeSel_Bit_Start                                                    3
#define cAf6_LIU_Rx_Channel_Lookup_LIURxModeSel_Bit_End                                                      5
#define cAf6_LIU_Rx_Channel_Lookup_LIURxModeSel_Mask                                                   cBit5_3
#define cAf6_LIU_Rx_Channel_Lookup_LIURxModeSel_Shift                                                        3
#define cAf6_LIU_Rx_Channel_Lookup_LIURxModeSel_MaxVal                                                     0x7
#define cAf6_LIU_Rx_Channel_Lookup_LIURxModeSel_MinVal                                                     0x0
#define cAf6_LIU_Rx_Channel_Lookup_LIURxModeSel_RstVal                                                     0x0

/*--------------------------------------
BitField Name: LIURxBusSel
BitField Type: RW
BitField Desc: LIU Rx Bus select 4: EC1. 1,3: PDH OC24 #2. 0,2: PDH OC24 #1
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_LIU_Rx_Channel_Lookup_LIURxBusSel_Bit_Start                                                     0
#define cAf6_LIU_Rx_Channel_Lookup_LIURxBusSel_Bit_End                                                       2
#define cAf6_LIU_Rx_Channel_Lookup_LIURxBusSel_Mask                                                    cBit2_0
#define cAf6_LIU_Rx_Channel_Lookup_LIURxBusSel_Shift                                                         0
#define cAf6_LIU_Rx_Channel_Lookup_LIURxBusSel_MaxVal                                                      0x7
#define cAf6_LIU_Rx_Channel_Lookup_LIURxBusSel_MinVal                                                      0x0
#define cAf6_LIU_Rx_Channel_Lookup_LIURxBusSel_RstVal                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : LIU Tx Clock Invert
Reg Addr   : 0x1000
Reg Formula:
    Where  :
Reg Desc   :
This register configures LIU Tx Clock Invert.

------------------------------------------------------------------------------*/
#define cAf6Reg_LIU_Tx_Clock_Invert_Base                                                                0x1000
#define cAf6Reg_LIU_Tx_Clock_Invert                                                                     0x1000
#define cAf6Reg_LIU_Tx_Clock_Invert_WidthVal                                                                32
#define cAf6Reg_LIU_Tx_Clock_Invert_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: Liutxinv
BitField Type: RW
BitField Desc: LIU Tx Clock Invert, each bit for each channel
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_LIU_Tx_Clock_Invert_Liutxinv_Bit_Start                                                          0
#define cAf6_LIU_Tx_Clock_Invert_Liutxinv_Bit_End                                                           15
#define cAf6_LIU_Tx_Clock_Invert_Liutxinv_Mask                                                        cBit15_0
#define cAf6_LIU_Tx_Clock_Invert_Liutxinv_Shift                                                              0
#define cAf6_LIU_Tx_Clock_Invert_Liutxinv_MaxVal                                                        0xffff
#define cAf6_LIU_Tx_Clock_Invert_Liutxinv_MinVal                                                           0x0
#define cAf6_LIU_Tx_Clock_Invert_Liutxinv_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : LIU Tx Clock External
Reg Addr   : 0x1001
Reg Formula:
    Where  :
Reg Desc   :
This register configures LIU Tx Clock External

------------------------------------------------------------------------------*/
#define cAf6Reg_LIU_Tx_Clock_External_Base                                                              0x1001
#define cAf6Reg_LIU_Tx_Clock_External                                                                   0x1001
#define cAf6Reg_LIU_Tx_Clock_External_WidthVal                                                              32
#define cAf6Reg_LIU_Tx_Clock_External_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: Liutxext
BitField Type: RW
BitField Desc: LIU Tx Clock External, each bit for each channel
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_LIU_Tx_Clock_External_Liutxext_Bit_Start                                                        0
#define cAf6_LIU_Tx_Clock_External_Liutxext_Bit_End                                                         15
#define cAf6_LIU_Tx_Clock_External_Liutxext_Mask                                                      cBit15_0
#define cAf6_LIU_Tx_Clock_External_Liutxext_Shift                                                            0
#define cAf6_LIU_Tx_Clock_External_Liutxext_MaxVal                                                      0xffff
#define cAf6_LIU_Tx_Clock_External_Liutxext_MinVal                                                         0x0
#define cAf6_LIU_Tx_Clock_External_Liutxext_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : LIU Tx Channel Enable
Reg Addr   : 0x1002
Reg Formula:
    Where  :
Reg Desc   :
This register configures LIU Tx Channel Enable.

------------------------------------------------------------------------------*/
#define cAf6Reg_LIU_Tx_Channel_Enable_Base                                                              0x1002
#define cAf6Reg_LIU_Tx_Channel_Enable                                                                   0x1002
#define cAf6Reg_LIU_Tx_Channel_Enable_WidthVal                                                              32
#define cAf6Reg_LIU_Tx_Channel_Enable_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: Liutxchen
BitField Type: RW
BitField Desc: LIU Tx Channel Enable, each bit for each channel
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_LIU_Tx_Channel_Enable_Liutxchen_Bit_Start                                                       0
#define cAf6_LIU_Tx_Channel_Enable_Liutxchen_Bit_End                                                        15
#define cAf6_LIU_Tx_Channel_Enable_Liutxchen_Mask                                                     cBit15_0
#define cAf6_LIU_Tx_Channel_Enable_Liutxchen_Shift                                                           0
#define cAf6_LIU_Tx_Channel_Enable_Liutxchen_MaxVal                                                     0xffff
#define cAf6_LIU_Tx_Channel_Enable_Liutxchen_MinVal                                                        0x0
#define cAf6_LIU_Tx_Channel_Enable_Liutxchen_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : LIU Tx loopout
Reg Addr   : 0x1003
Reg Formula:
    Where  :
Reg Desc   :
This register configures LIU Tx loopout.

------------------------------------------------------------------------------*/
#define cAf6Reg_LIU_Tx_loopout_Base                                                                     0x1003
#define cAf6Reg_LIU_Tx_loopout                                                                          0x1003
#define cAf6Reg_LIU_Tx_loopout_WidthVal                                                                     32
#define cAf6Reg_LIU_Tx_loopout_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: Liutxloopout
BitField Type: RW
BitField Desc: LIU Tx loopout, each bit for each channel
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_LIU_Tx_loopout_Liutxloopout_Bit_Start                                                           0
#define cAf6_LIU_Tx_loopout_Liutxloopout_Bit_End                                                            15
#define cAf6_LIU_Tx_loopout_Liutxloopout_Mask                                                         cBit15_0
#define cAf6_LIU_Tx_loopout_Liutxloopout_Shift                                                               0
#define cAf6_LIU_Tx_loopout_Liutxloopout_MaxVal                                                         0xffff
#define cAf6_LIU_Tx_loopout_Liutxloopout_MinVal                                                            0x0
#define cAf6_LIU_Tx_loopout_Liutxloopout_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : LIU Tx Channel Lookup
Reg Addr   : 0x1100 - 0x110F
Reg Formula: 0x1100 +  portid
    Where  :
           + $portid(0-15):
Reg Desc   :
The Control is use to configure for per LIU port Concatenation operation.

------------------------------------------------------------------------------*/
#define cAf6Reg_LIU_Tx_Channel_Lookup_Base                                                              0x1100
#define cAf6Reg_LIU_Tx_Channel_Lookup(portid)                                                (0x1100+(portid))
#define cAf6Reg_LIU_Tx_Channel_Lookup_WidthVal                                                              32
#define cAf6Reg_LIU_Tx_Channel_Lookup_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: LIUTxPDHID
BitField Type: RW
BitField Desc: LIU Rx PDHID,  In STSmode bit[4:0] = 0
BitField Bits: [15:6]
--------------------------------------*/
#define cAf6_LIU_Tx_Channel_Lookup_LIUTxPDHID_Bit_Start                                                      6
#define cAf6_LIU_Tx_Channel_Lookup_LIUTxPDHID_Bit_End                                                       15
#define cAf6_LIU_Tx_Channel_Lookup_LIUTxPDHID_Mask                                                    cBit15_6
#define cAf6_LIU_Tx_Channel_Lookup_LIUTxPDHID_Shift                                                          6
#define cAf6_LIU_Tx_Channel_Lookup_LIUTxPDHID_MaxVal                                                     0x3ff
#define cAf6_LIU_Tx_Channel_Lookup_LIUTxPDHID_MinVal                                                       0x0
#define cAf6_LIU_Tx_Channel_Lookup_LIUTxPDHID_RstVal                                                       0x0

/*--------------------------------------
BitField Name: LIUTxModeSel
BitField Type: RW
BitField Desc: LIU Rx Mode select 4: EC1. 3: DS3. 2: E3. 1: E1. 0: DS1
BitField Bits: [5:3]
--------------------------------------*/
#define cAf6_LIU_Tx_Channel_Lookup_LIUTxModeSel_Bit_Start                                                    3
#define cAf6_LIU_Tx_Channel_Lookup_LIUTxModeSel_Bit_End                                                      5
#define cAf6_LIU_Tx_Channel_Lookup_LIUTxModeSel_Mask                                                   cBit5_3
#define cAf6_LIU_Tx_Channel_Lookup_LIUTxModeSel_Shift                                                        3
#define cAf6_LIU_Tx_Channel_Lookup_LIUTxModeSel_MaxVal                                                     0x7
#define cAf6_LIU_Tx_Channel_Lookup_LIUTxModeSel_MinVal                                                     0x0
#define cAf6_LIU_Tx_Channel_Lookup_LIUTxModeSel_RstVal                                                     0x0

/*--------------------------------------
BitField Name: LIUTxBusSel
BitField Type: RW
BitField Desc: LIU Rx Bus select 4: EC1. 1,3: PDH OC24 #2. 0,2: PDH OC24 #1
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_LIU_Tx_Channel_Lookup_LIUTxBusSel_Bit_Start                                                     0
#define cAf6_LIU_Tx_Channel_Lookup_LIUTxBusSel_Bit_End                                                       2
#define cAf6_LIU_Tx_Channel_Lookup_LIUTxBusSel_Mask                                                    cBit2_0
#define cAf6_LIU_Tx_Channel_Lookup_LIUTxBusSel_Shift                                                         0
#define cAf6_LIU_Tx_Channel_Lookup_LIUTxBusSel_MaxVal                                                      0x7
#define cAf6_LIU_Tx_Channel_Lookup_LIUTxBusSel_MinVal                                                      0x0
#define cAf6_LIU_Tx_Channel_Lookup_LIUTxBusSel_RstVal                                                      0x0

#define cAf6_LIU_Rx_BPV_Counter_R2C 0x2000
#define cAf6_LIU_Rx_BPV_Counter_RO  0x2010

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061LIUREG_H_ */


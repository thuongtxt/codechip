/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60210061PdhDe3.h
 * 
 * Created Date: Oct 26, 2016
 *
 * Description : DE3
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061PDHDE3_H_
#define _THA60210061PDHDE3_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 Tha60210061PdhDe3LiuPdhIdGet(AtPdhDe3 self, uint8 *sliceId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061PDHDE3_H_ */


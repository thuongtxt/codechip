/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60210061PdhDe1RetimingController.c
 *
 * Created Date: May 29, 2017
 *
 * Description : DS1/E1 retiming controller implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/man/ThaDeviceInternal.h"
#include "../../../../default/pdh/ThaModulePdhReg.h"
#include "../Tha60210061ModulePdh.h"
#include "../Tha60210061PdhDe1.h"
#include "Tha60210061PdhDe1RetimingControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThaDe1RetimingController(self) ((ThaPdhDe1RetimingController)(self))

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tThaPdhRetimingControllerMethods    m_ThaPdhRetimingControllerOverride;
static tThaPdhDe1RetimingControllerMethods m_ThaPdhDe1RetimingControllerOverride;

/* Save super implementation */
static const tThaPdhRetimingControllerMethods    *m_ThaPdhRetimingControllerMethods    = NULL;
static const tThaPdhDe1RetimingControllerMethods *m_ThaPdhDe1RetimingControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaPdhDe1 De1(ThaPdhDe1RetimingController self)
    {
    return (ThaPdhDe1)ThaPdhRetimingControllerOwnerGet((ThaPdhRetimingController)self);
    }

static uint32 LiuBaseAddress(void)
    {
    return Tha60210061ModulePdhLiuBaseAddress();
    }

static uint32 SlipBufferTxFramerControlRegister(ThaPdhDe1RetimingController self)
    {
    AtPdhChannel channel = (AtPdhChannel)De1(self);
    return 0x50000 + LiuBaseAddress() + Tha60210061Ds1E1J1LiuOffset(channel);
    }

static uint32 SlipBufferTxFramerControlRegister1(ThaPdhDe1RetimingController self)
    {
    AtPdhChannel channel = (AtPdhChannel)De1(self);
    return 0x50600 + LiuBaseAddress() + Tha60210061Ds1E1J1LiuOffset(channel);
    }

static uint32 SlipBufferRxFramerControlRegister(ThaPdhDe1RetimingController self)
    {
    AtPdhChannel channel = (AtPdhChannel)De1(self);
    return 0x5c000 + LiuBaseAddress() + Tha60210061Ds1E1J1LiuOffset(channel);
    }

static uint32 SlipBufferSsmEnableRegister(ThaPdhDe1RetimingController self)
    {
    AtUnused(self);
    return 0x54000 + LiuBaseAddress();
    }

static uint32 SlipBufferSsmRxStatusRegister(ThaPdhDe1RetimingController self)
    {
    uint32 address;
    AtPdhChannel channel = (AtPdhChannel)De1(self);
    address = 0x5c400 + LiuBaseAddress() + Tha60210061Ds1E1J1LiuOffset(channel);
    return address;
    }

static uint32 SlipBufferSsmRxCrcCounterRegister(ThaPdhDe1RetimingController self)
    {
    uint32 address;
    AtPdhChannel channel = (AtPdhChannel)De1(self);

    address = 0x5e800 + 64 + LiuBaseAddress() + Tha60210061Ds1E1J1LiuOffset(channel);
    return address;
    }

static uint32 SlipBufferSsmRxReiCounterRegister(ThaPdhDe1RetimingController self)
    {
    uint32 address;
    AtPdhChannel channel = (AtPdhChannel)De1(self);

    address = 0x5f000 + 64 + LiuBaseAddress() + Tha60210061Ds1E1J1LiuOffset(channel);
    return address;
    }

static uint32 SlipBufferSsmRxFbeCounterRegister(ThaPdhDe1RetimingController self)
    {
    uint32 address;
    AtPdhChannel channel = (AtPdhChannel)De1(self);

    address = 0x5e000 + 64 + LiuBaseAddress() + Tha60210061Ds1E1J1LiuOffset(channel);
    return address;
    }

static void SlipBufferTxFramerControlTxHwSsmFrameTypeUpdate(ThaPdhDe1RetimingController self, uint16 frameType)
    {
    uint32 address;
    uint32 regVal;
    ThaPdhDe1 channel = De1(self);

    address = SlipBufferTxFramerControlRegister(mThaDe1RetimingController(self));
    regVal = mChannelHwRead(channel, address, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_txctrl_pen_TxDE1Md_, ThaPdhDe1FrameModeSw2Hw(channel, frameType));
    mChannelHwWrite(channel, address, regVal, cAtModulePdh);
    }

static uint32 DataSlipStickyRegister(void)
    {
    return 0x54001 + LiuBaseAddress();
    }

static uint32 FrameSlipStickyRegister(void)
    {
    return 0x54002 + LiuBaseAddress();
    }

static void SlipBufferTxHwDebugFrameStickyShow(ThaPdhDe1RetimingController self)
    {
    uint32 address, regVal, regField, fieldIdx;
    AtPdhChannel channel = (AtPdhChannel)De1(self);

    fieldIdx = Tha60210061Ds1E1J1LiuOffset(channel);
    /* show data slip sticky */
    address = DataSlipStickyRegister();
    regVal = mChannelHwRead(channel, address, cAtModulePdh);
    mFieldGet(regVal, (cBit0 << (fieldIdx%32)), (fieldIdx%32), uint32, &regField);
    AtPrintc(cSevNormal, "  Slip Buffer Data slip sticky: %s \n", regField ? "raise":"clear");
    if (regField)
        {
        regVal = 0;
        mFieldIns(&regVal, (cBit0 << (fieldIdx%32)), (fieldIdx%32), regField);
        mChannelHwWrite(channel, address, regVal, cAtModulePdh);
        }

    /* show frame slip sticky */
    address = FrameSlipStickyRegister();
    regVal = mChannelHwRead(channel, address, cAtModulePdh);
    mFieldGet(regVal, (cBit0 << (fieldIdx%32)), (fieldIdx%32), uint32, &regField);
    AtPrintc(cSevNormal, "  Slip Buffer Frame slip sticky: %s \n", regField ? "raise":"clear");
    if (regField)
        {
        regVal = 0;
        mFieldIns(&regVal, (cBit0 << (fieldIdx%32)), (fieldIdx%32), regField);
        mChannelHwWrite(channel, address, regVal, cAtModulePdh);
        }
    }

static uint32 SlipBufferSsmEnableRegMask(AtPdhChannel self)
    {
    return cBit0 << (Tha60210061Ds1E1J1LiuOffset(self) % 32);
    }

static uint32 SlipBufferSsmEnableRegShift(AtPdhChannel self)
    {
    return Tha60210061Ds1E1J1LiuOffset(self) % 32;
    }

static eAtRet SlipBufferEnable(ThaPdhRetimingController self, eBool enable)
    {
    AtPdhChannel channel = (AtPdhChannel)De1(mThaDe1RetimingController(self));
    uint32 address = SlipBufferSsmEnableRegister(mThaDe1RetimingController(self));
    uint32 mask = SlipBufferSsmEnableRegMask(channel);
    uint32 shift = SlipBufferSsmEnableRegShift(channel);
    uint32 regVal;

    regVal = mChannelHwRead(channel, address, cAtModulePdh);
    mFieldIns(&regVal, mask, shift, enable ? 1 : 0);
    mChannelHwWrite(channel, address, regVal, cAtModulePdh);

    return cAtOk;
    }

static eBool SlipBufferIsEnabled(ThaPdhRetimingController self)
    {
    AtPdhChannel channel = (AtPdhChannel)De1(mThaDe1RetimingController(self));
    uint32 address = SlipBufferSsmEnableRegister(mThaDe1RetimingController(self));
    uint32 mask = SlipBufferSsmEnableRegMask(channel);
    uint32 regVal;
    regVal = mChannelHwRead(channel , address, cAtModulePdh);
    return (regVal & mask) ? cAtTrue : cAtFalse;
    }

static void SlipBufferDefaultSet(ThaPdhRetimingController self)
    {
    AtPdhChannel channel = (AtPdhChannel)De1(mThaDe1RetimingController(self));
    uint32 address;
    uint32 regVal;

    address = SlipBufferTxFramerControlRegister(mThaDe1RetimingController(self));
    regVal = 0;
    mChannelHwWrite(channel, address, regVal, cAtModulePdh);

    address = SlipBufferTxFramerControlRegister1(mThaDe1RetimingController(self));
    regVal = 0;
    mFieldIns(&regVal, cAf6_txctrl_pen_E1_Bypass_Enable_Mask, cAf6_txctrl_pen_E1_Bypass_Enable_Shift, 1);
    mFieldIns(&regVal, cAf6_txctrl_pen_E2_Bypass_Enable_Mask, cAf6_txctrl_pen_E2_Bypass_Enable_Shift, 1);
    mChannelHwWrite(channel, address, regVal, cAtModulePdh);

    address = mMethodsGet(mThaDe1RetimingController(self))->SlipBufferRxFramerControlRegister(mThaDe1RetimingController(self));
    mChannelHwWrite(channel, address, 0, cAtModulePdh);

    SlipBufferEnable(self, cAtFalse);
    }


static uint32 UserNtimesToLocalNtimes(AtPdhDe1 self, uint32 nTimes)
    {
    uint32 maxNtimes = AtPdhDe1TxBomSentMaxNTimes(self);
    if (nTimes >= maxNtimes)
        return 0; /* send continuously */

    if (nTimes == 0) /* stop sent */
        return 1;

    return nTimes;
    }

static uint32 SlipBufferSsmBomThresholdGet(ThaPdhDe1RetimingController self)
    {
    AtPdhChannel channel = (AtPdhChannel)De1(self);
    uint32 address = SlipBufferTxFramerControlRegister(self);
    uint32 regVal;

    regVal = mChannelHwRead(channel, address, cAtModulePdh);
    return mRegField(regVal, cAf6_txctrl_pen_SSMCount_);
    }

static eAtRet SlipBufferSsmBomValueNtimesSet(ThaPdhDe1RetimingController self, uint8 value, uint32 nTimes)
    {
    AtPdhChannel channel = (AtPdhChannel)De1(self);
    uint32 address = SlipBufferTxFramerControlRegister(self);
    uint32 regVal;

    regVal = mChannelHwRead(channel, address, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_txctrl_pen_SSMMess_, value);
    mRegFieldSet(regVal, cAf6_txctrl_pen_SSMCount_, nTimes);
    mChannelHwWrite(channel, address, regVal, cAtModulePdh);

    return cAtOk;
    }

static eAtRet SlipBufferSsmBomValueSet(ThaPdhDe1RetimingController self, uint8 value, uint32 nTimes)
    {
    return SlipBufferSsmBomValueNtimesSet(self, value, nTimes);
    }

static eAtRet SlipBufferSsmBomValueForToggleSendingSet(ThaPdhDe1RetimingController self, uint8 value, uint32 nTimes, eBool isToggle)
    {
    AtPdhChannel channel = (AtPdhChannel)De1(self);
    uint32 regVal;
    uint32 address = SlipBufferTxFramerControlRegister(self);

    if (isToggle)
        {
        regVal = mChannelHwRead(channel, address, cAtModulePdh);
        mRegFieldSet(regVal, cAf6_txctrl_pen_SSM_Enable_, 0);
        mChannelHwWrite(channel, address, regVal, cAtModulePdh);
        }

    SlipBufferSsmBomValueNtimesSet(self, value, UserNtimesToLocalNtimes((AtPdhDe1)De1(self), nTimes));

    if (isToggle)
        {
        /* Must sleep to compliant to HW design */
        AtOsalUSleep(250);

        regVal = mChannelHwRead(channel, address, cAtModulePdh);
        mRegFieldSet(regVal, cAf6_txctrl_pen_SSM_Enable_, 1);
        mChannelHwWrite(channel, address, regVal, cAtModulePdh);
        }

    return cAtOk;
    }

static eBool SlipBufferBomSendingIsEnabled(ThaPdhDe1RetimingController self)
    {
    AtPdhChannel channel = (AtPdhChannel)De1(self);
    uint32 regVal;
    uint32 address = SlipBufferTxFramerControlRegister(self);

    regVal = mChannelHwRead(channel, address, cAtModulePdh);
    return mRegField(regVal, cAf6_txctrl_pen_SSM_Enable_) ? cAtTrue : cAtFalse;
    }

static uint32 SlipBufferTxFrameSsmStatusRegister(void)
    {
    return 0x50200 + LiuBaseAddress();
    }

static eBool SlipBufferBomSendingIsReady(ThaPdhDe1RetimingController self)
    {
    AtPdhChannel channel = (AtPdhChannel)De1(self);
    uint32 regVal;
    uint32 address;

    address = SlipBufferTxFrameSsmStatusRegister();
    regVal = mChannelHwRead(channel, address, cAtModulePdh);
    return (regVal & SlipBufferSsmEnableRegMask(channel)) ? cAtTrue : cAtFalse;
    }

static eBool SlipBufferBomSendingCanWaitForReady(ThaPdhDe1RetimingController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void SlipBufferSsmSaBitsMaskSet(ThaPdhDe1RetimingController self, uint8 saBitsMask)
    {
    AtPdhChannel channel = (AtPdhChannel)De1(mThaDe1RetimingController(self));
    uint32 address = SlipBufferTxFramerControlRegister(mThaDe1RetimingController(self));
    uint32 regVal;
    regVal = mChannelHwRead(channel, address, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_txctrl_pen_SSMSasel_, saBitsMask);
    mChannelHwWrite(channel, address, regVal, cAtModulePdh);
    }

static uint8 SlipBufferSsmSaBitsMaskGet(ThaPdhDe1RetimingController self)
    {
    AtPdhChannel channel = (AtPdhChannel)De1(mThaDe1RetimingController(self));
    uint32 address = SlipBufferTxFramerControlRegister(mThaDe1RetimingController(self));
    uint32 regVal;
    regVal = mChannelHwRead(channel, address, cAtModulePdh);
    return (uint8)mRegField(regVal, cAf6_txctrl_pen_SSMSasel_);
    }

static uint32 TxSlipCounterRegister(void)
    {
    return 0x54100;
    }

static ThaVersionReader VersionReader(ThaPdhRetimingController self)
    {
    AtPdhChannel channel = (AtPdhChannel)De1(mThaDe1RetimingController(self));
    AtDevice device = AtChannelDeviceGet((AtChannel)channel);

    return ThaDeviceVersionReader(device);
    }

static eBool IsTxCsSupported(ThaPdhRetimingController self)
    {
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader(self));
    uint32 startVersionHasThis = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x3, 0x2, 0x1063);
    return (currentVersion > startVersionHasThis) ? cAtTrue : cAtFalse;
    }

static uint32 TxCsCounter(ThaPdhRetimingController self, eBool r2c)
    {
    if (IsTxCsSupported(self))
        {
        AtPdhChannel channel = (AtPdhChannel)De1(mThaDe1RetimingController(self));
        uint32 channelId = Tha60210061Ds1E1J1LiuOffset(channel);
        uint32 offset = r2c ? (16 + channelId) : channelId;
        uint32 baseAddress = TxSlipCounterRegister() + LiuBaseAddress();
        return mChannelHwRead(channel, (baseAddress + offset), cAtModulePdh);
        }

    return 0;
    }

static eAtRet SlipBufferSsmEnable(ThaPdhRetimingController self, eBool enable)
    {
    AtPdhChannel channel = (AtPdhChannel)De1(mThaDe1RetimingController(self));
    uint32 address = SlipBufferTxFramerControlRegister(mThaDe1RetimingController(self));
    uint32 regVal;
    eBool isE1 = AtPdhDe1IsE1((AtPdhDe1)channel);

    regVal = mChannelHwRead(channel, address, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_txctrl_pen_SSM_Enable_, enable ? 1 : 0);
    mRegFieldSet(regVal, cAf6_txctrl_pen_SSMCount_, isE1 ? 0 : 10); /* Just need to send continously in case of E1 */
    mChannelHwWrite(channel, address, regVal, cAtModulePdh);

    return cAtOk;
    }

static eBool SlipBufferSsmIsEnabled(ThaPdhRetimingController self)
    {
    AtPdhChannel channel = (AtPdhChannel)De1(mThaDe1RetimingController(self));
    uint32 address = SlipBufferTxFramerControlRegister(mThaDe1RetimingController(self));
    uint32 regVal;
    regVal = mChannelHwRead(channel, address, cAtModulePdh);
    return (regVal & cAf6_txctrl_pen_SSM_Enable_Mask) ? cAtTrue : cAtFalse;
    }

static uint8 SlipBufferTxSsmGet(ThaPdhRetimingController self)
    {
    AtPdhChannel channel = (AtPdhChannel)De1(mThaDe1RetimingController(self));
    uint32 address = SlipBufferTxFramerControlRegister(mThaDe1RetimingController(self));
    uint32 regVal;

    regVal = mChannelHwRead(channel, address, cAtModulePdh);
    return (uint8)mRegField(regVal, cAf6_txctrl_pen_SSMMess_);
    }

static uint32 SlipBufferTxSsmSentThresholdGet(ThaPdhRetimingController self)
    {
    uint32 hwThreshold = SlipBufferSsmBomThresholdGet(mThaDe1RetimingController(self));
    if (hwThreshold == 0)
        return AtPdhDe1TxBomSentMaxNTimes((AtPdhDe1)De1(mThaDe1RetimingController(self)));

    return hwThreshold;
    }

static void OverrideThaPdhRetimingController(Tha60210061PdhDe1RetimingController self)
    {
    ThaPdhRetimingController variable = (ThaPdhRetimingController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPdhRetimingControllerMethods = mMethodsGet(variable);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhRetimingControllerOverride, m_ThaPdhRetimingControllerMethods, sizeof(m_ThaPdhRetimingControllerOverride));

        mMethodOverride(m_ThaPdhRetimingControllerOverride, TxCsCounter);
        mMethodOverride(m_ThaPdhRetimingControllerOverride, SlipBufferEnable);
        mMethodOverride(m_ThaPdhRetimingControllerOverride, SlipBufferIsEnabled);
        mMethodOverride(m_ThaPdhRetimingControllerOverride, SlipBufferSsmEnable);
        mMethodOverride(m_ThaPdhRetimingControllerOverride, SlipBufferSsmIsEnabled);
        mMethodOverride(m_ThaPdhRetimingControllerOverride, SlipBufferTxSsmGet);
        mMethodOverride(m_ThaPdhRetimingControllerOverride, SlipBufferTxSsmSentThresholdGet);
        mMethodOverride(m_ThaPdhRetimingControllerOverride, SlipBufferDefaultSet);
        }

    mMethodsSet(variable, &m_ThaPdhRetimingControllerOverride);
    }

static void OverrideThaPdhDe1RetimingController(Tha60210061PdhDe1RetimingController self)
    {
    ThaPdhDe1RetimingController variable = (ThaPdhDe1RetimingController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPdhDe1RetimingControllerMethods = mMethodsGet(variable);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhDe1RetimingControllerOverride, m_ThaPdhDe1RetimingControllerMethods, sizeof(m_ThaPdhDe1RetimingControllerOverride));

        /* Setup methods */
        mMethodOverride(m_ThaPdhDe1RetimingControllerOverride, SlipBufferRxFramerControlRegister);
        mMethodOverride(m_ThaPdhDe1RetimingControllerOverride, SlipBufferSsmRxStatusRegister);
        mMethodOverride(m_ThaPdhDe1RetimingControllerOverride, SlipBufferSsmRxCrcCounterRegister);
        mMethodOverride(m_ThaPdhDe1RetimingControllerOverride, SlipBufferSsmRxReiCounterRegister);
        mMethodOverride(m_ThaPdhDe1RetimingControllerOverride, SlipBufferSsmRxFbeCounterRegister);
        mMethodOverride(m_ThaPdhDe1RetimingControllerOverride, SlipBufferTxFramerControlTxHwSsmFrameTypeUpdate);
        mMethodOverride(m_ThaPdhDe1RetimingControllerOverride, SlipBufferSsmSaBitsMaskSet);
        mMethodOverride(m_ThaPdhDe1RetimingControllerOverride, SlipBufferSsmSaBitsMaskGet);
        mMethodOverride(m_ThaPdhDe1RetimingControllerOverride, SlipBufferSsmBomValueSet);
        mMethodOverride(m_ThaPdhDe1RetimingControllerOverride, SlipBufferBomSendingIsEnabled);
        mMethodOverride(m_ThaPdhDe1RetimingControllerOverride, SlipBufferBomSendingCanWaitForReady);
        mMethodOverride(m_ThaPdhDe1RetimingControllerOverride, SlipBufferBomSendingIsReady);
        mMethodOverride(m_ThaPdhDe1RetimingControllerOverride, SlipBufferSsmBomValueForToggleSendingSet);
        mMethodOverride(m_ThaPdhDe1RetimingControllerOverride, SlipBufferTxHwDebugFrameStickyShow);
        }

    mMethodsSet(variable, &m_ThaPdhDe1RetimingControllerOverride);
    }

static void Override(Tha60210061PdhDe1RetimingController self)
    {
    OverrideThaPdhRetimingController(self);
    OverrideThaPdhDe1RetimingController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061PdhDe1RetimingController);
    }

static Tha60210061PdhDe1RetimingController ObjectInit(Tha60210061PdhDe1RetimingController self, AtPdhChannel owner)
    {
    /* Clear memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhDe1RetimingControllerObjectInit((ThaPdhDe1RetimingController)self, owner) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

Tha60210061PdhDe1RetimingController Tha60210061PdhDe1RetimingControllerNew(AtPdhChannel owner)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    Tha60210061PdhDe1RetimingController newTha60210061PdhDe1RetimingController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newTha60210061PdhDe1RetimingController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newTha60210061PdhDe1RetimingController, owner);
    }


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60210061PdhDe1RetimingControllerInternal.h
 * 
 * Created Date: May 31, 2017
 *
 * Description : DS1/E1 retiming controller internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061PDHDE1RETIMINGCONTROLLERINTERNAL_H_
#define _THA60210061PDHDE1RETIMINGCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/pdh/retimingcontrollers/ThaPdhDe1RetimingControllerInternal.h"
#include "Tha60210061PdhDe1RetimingController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210061PdhDe1RetimingController
    {
    tThaPdhDe1RetimingController super;
    } tTha60210061PdhDe1RetimingController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THAPDHDE1RETIMINGCONTROLLERINTERNAL_H_ */


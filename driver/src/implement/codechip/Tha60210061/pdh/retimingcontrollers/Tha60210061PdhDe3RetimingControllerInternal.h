/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : ThaPdhDe3TimingControllerInternal.h
 * 
 * Created Date: Jun 2, 2017
 *
 * Description : DS3 retiming controller internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061PDHDE3TIMINGCONTROLLERINTERNAL_H_
#define _THA60210061PDHDE3TIMINGCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "Tha60210061PdhDe3RetimingController.h"
#include "../../../../default/pdh/retimingcontrollers/ThaPdhDe3RetimingControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210061PdhDe3RetimingController
    {
    tThaPdhDe3RetimingController super;
    } tTha60210061PdhDe3RetimingController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061PDHDE3TIMINGCONTROLLERINTERNAL_H_ */


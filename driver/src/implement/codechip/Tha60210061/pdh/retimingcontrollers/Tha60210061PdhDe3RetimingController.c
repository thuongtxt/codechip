/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : ThaPdhDe3RetimingController.c
 *
 * Created Date: May 29, 2017
 *
 * Description : Retiming controller implementation for DS3
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/man/ThaDeviceInternal.h"
#include "../Tha60210061ModulePdh.h"
#include "Tha60210061PdhDe3RetimingControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThaPdhDe3RetimingController(self) ((ThaPdhDe3RetimingController)(self))

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tThaPdhRetimingControllerMethods    m_ThaPdhRetimingControllerOverride;
static tThaPdhDe3RetimingControllerMethods m_ThaPdhDe3RetimingControllerOverride;

/* Save super implementation */
static const tThaPdhRetimingControllerMethods    *m_ThaPdhRetimingControllerMethods    = NULL;
static const tThaPdhDe3RetimingControllerMethods *m_ThaPdhDe3RetimingControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtChannel De3(ThaPdhDe3RetimingController self)
    {
    return (AtChannel)ThaPdhRetimingControllerOwnerGet((ThaPdhRetimingController)self);
    }

static uint32 LiuBaseAddress(void)
    {
    return Tha60210061ModulePdhLiuBaseAddress();
    }

static uint32 SlipBufferSsmEnableRegister(ThaPdhDe3RetimingController self)
    {
    AtUnused(self);
    return 0x00044000 + LiuBaseAddress();
    }

static uint32 SlipBufferTxFramerControlRegister(ThaPdhDe3RetimingController self)
    {
    return 0x00040000 + AtChannelIdGet(De3(self)) + LiuBaseAddress();
    }

static uint32 SlipBufferRxFramerControlRegister(ThaPdhDe3RetimingController self)
    {
    return 0x0004C000 + AtChannelIdGet(De3(self)) + LiuBaseAddress();
    }

static uint32 SlipBufferRxFrameStatusRegister(ThaPdhDe3RetimingController self)
    {
    /* show hw frame status */
    return 0x0004C400 + + AtChannelIdGet(De3(self)) + LiuBaseAddress();
    }

static uint32 SlipBufferTxSlipStickyRegister(ThaPdhDe3RetimingController self)
    {
    AtUnused(self);
    /* show data slip sticky */
    return 0x00044001 + LiuBaseAddress();
    }

static uint32 TxCsCounterRegister(void)
    {
    return 0x0044100;
    }


static ThaVersionReader VersionReader(ThaPdhRetimingController self)
    {
    AtPdhChannel channel = (AtPdhChannel)De3(mThaPdhDe3RetimingController(self));
    AtDevice device = AtChannelDeviceGet((AtChannel)channel);
    return ThaDeviceVersionReader(device);
    }

static eBool IsTxCsSupported(ThaPdhRetimingController self)
    {
    uint32 currentVersion = ThaVersionReaderHardwareVersion(VersionReader(self));
    uint32 startVersionHasThis = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x3, 0x2, 0x1063);
    return (currentVersion > startVersionHasThis) ? cAtTrue : cAtFalse;
    }

static uint32 TxCsCounter(ThaPdhRetimingController self, eBool r2c)
    {
    if (IsTxCsSupported(self))
        {
        uint32 offset1 = AtChannelIdGet(De3(mThaPdhDe3RetimingController(self))) + LiuBaseAddress();
        uint32 offset = r2c ? (16 + offset1) : offset1;
        uint32 address = TxCsCounterRegister() + offset;
        return mChannelHwRead(De3(mThaPdhDe3RetimingController(self)), address, cAtModulePdh);
        }

    return 0;
    }

static void OverrideThaPdhRetimingController(Tha60210061PdhDe3RetimingController self)
    {
    ThaPdhRetimingController controller = (ThaPdhRetimingController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPdhRetimingControllerMethods = mMethodsGet(controller);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhRetimingControllerOverride, m_ThaPdhRetimingControllerMethods, sizeof(m_ThaPdhRetimingControllerOverride));

        mMethodOverride(m_ThaPdhRetimingControllerOverride, TxCsCounter);
        }

    mMethodsSet(controller, &m_ThaPdhRetimingControllerOverride);
    }

static void OverrideThaPdhDe3RetimingController(Tha60210061PdhDe3RetimingController self)
    {
    ThaPdhDe3RetimingController controller = (ThaPdhDe3RetimingController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPdhDe3RetimingControllerMethods = mMethodsGet(controller);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhDe3RetimingControllerOverride, m_ThaPdhDe3RetimingControllerMethods, sizeof(m_ThaPdhDe3RetimingControllerOverride));

        /* Setup methods */
        mMethodOverride(m_ThaPdhDe3RetimingControllerOverride, SlipBufferTxFramerControlRegister);
        mMethodOverride(m_ThaPdhDe3RetimingControllerOverride, SlipBufferRxFramerControlRegister);
        mMethodOverride(m_ThaPdhDe3RetimingControllerOverride, SlipBufferSsmEnableRegister);
        mMethodOverride(m_ThaPdhDe3RetimingControllerOverride, SlipBufferRxFrameStatusRegister);
        mMethodOverride(m_ThaPdhDe3RetimingControllerOverride, SlipBufferTxSlipStickyRegister);
        }

    mMethodsSet(controller, &m_ThaPdhDe3RetimingControllerOverride);
    }

static void Override(Tha60210061PdhDe3RetimingController self)
    {
    OverrideThaPdhRetimingController(self);
    OverrideThaPdhDe3RetimingController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061PdhDe3RetimingController);
    }

static Tha60210061PdhDe3RetimingController ObjectInit(Tha60210061PdhDe3RetimingController self, AtPdhChannel owner)
    {
    /* Clear memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhDe3RetimingControllerObjectInit((ThaPdhDe3RetimingController)self, owner) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

Tha60210061PdhDe3RetimingController Tha60210061PdhDe3RetimingControllerNew(AtPdhChannel owner)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    Tha60210061PdhDe3RetimingController newTha60210061PdhDe3RetimingController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newTha60210061PdhDe3RetimingController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newTha60210061PdhDe3RetimingController, owner);
    }

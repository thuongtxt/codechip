/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60210061PdhDe1RetimingController.h
 * 
 * Created Date: May 29, 2017
 *
 * Description : DS1/E1 retiming interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061PDHDE1RETIMINGCONTROLLER_H_
#define _THA60210061PDHDE1RETIMINGCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210061PdhDe1RetimingController *Tha60210061PdhDe1RetimingController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
Tha60210061PdhDe1RetimingController Tha60210061PdhDe1RetimingControllerNew(AtPdhChannel owner);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061PDHDE1RETIMINGCONTROLLER_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60210061PdhDe3.c
 *
 * Created Date: Jun 23, 2016
 *
 * Description : 60210061 PDH DE3 implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPdhSerialLine.h"
#include "../../../default/man/ThaDevice.h"
#include "../../../default/man/versionreader/ThaVersionReader.h"
#include "../../Tha60210012/pdh/Tha60210012PdhDe3Internal.h"
#include "../../Tha60210031/pdh/Tha60210031PdhDe3Internal.h"
#include "../../Tha60210031/pdh/Tha60210031ModulePdh.h"
#include "../../Tha60210012/pwe/Tha60210012ModulePweBlockManager.h"
#include "Tha60210061LiuReg.h"
#include "Tha60210061ModulePdh.h"
#include "Tha60210061PdhDe3.h"
#include "retimingcontrollers/Tha60210061PdhDe3RetimingController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define cAf6Reg_Rxm23e23_Lcv_Cnt_Base     0x00740600
#define mThis(self) ((tTha60210061PdhDe3 *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061PdhDe3
    {
    tTha60210012PdhDe3 super;
    }tTha60210061PdhDe3;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods    m_AtChannelOverride;
static tAtPdhChannelMethods m_AtPdhChannelOverride;
static tThaPdhDe3Methods    m_ThaPdhDe3Override;
static tTha60210031PdhDe3Methods    m_Tha60210031PdhDe3Override;

/* Super implement */
static const tAtChannelMethods     *m_AtChannelMethods    = NULL;
static const tAtPdhChannelMethods  *m_AtPdhChannelMethods = NULL;
static const tThaPdhDe3Methods     *m_ThaPdhDe3Methods    = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 LiuBaseAddress(void)
    {
    return Tha60210061ModulePdhLiuBaseAddress();
    }

static eAtRet FrameTypeSet(AtPdhChannel self, uint16 frameType)
    {
    return m_AtPdhChannelMethods->FrameTypeSet(self, frameType);
    }

static eBool BpvCounterIsSupported(ThaPdhDe3 self)
    {
    return Tha60210061ModulePdhBpvCounterIsSupported((ThaModulePdh)AtChannelModuleGet((AtChannel)self));
    }

static uint32 BpvCounterRead2Clear(ThaPdhDe3 self, eBool clear)
    {
    uint32 offset, address;

    if (!BpvCounterIsSupported(self))
        return 0;

    address = (clear) ? cAf6_LIU_Rx_BPV_Counter_R2C : cAf6_LIU_Rx_BPV_Counter_RO;
    offset = LiuBaseAddress() + AtChannelIdGet((AtChannel)self);
    return mChannelHwRead(self, address + offset, cAtModulePdh);
    }

static eBool IsManagedByModulePdh(ThaPdhDe3 self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint8 DefaultLosThreshold(ThaPdhDe3 self)
    {
    if(Tha60210031PdhDe3IsDe3LiuMode(self))
        return 3;

    return 0;
    }

static uint32 De3ExpectBlockId(AtChannel de3)
    {
    uint8 slice, sts;

    ThaPdhChannelHwIdGet((AtPdhChannel)de3, cAtModulePdh,  &slice, &sts);
    return (uint32)(((slice % 2) * 16) + ((sts % 24) * 8) + 1);
    }

static eAtRet HwResourceAllocate(AtChannel self)
    {
    return Tha60210012PwCircuitDe3ExpectedBlockAssign(self, De3ExpectBlockId(self));
    }

static eBool PhyModuleIsOc48(eAtModule phyModule)
    {
    if (phyModule == cAtModulePrbs)
        return cAtTrue;
    return cAtFalse;
    }

static eAtRet HwIdFactorGet(ThaPdhDe3 self, uint32 phyModule, uint32 *slice, uint32 *hwIdInSlice)
    {
    uint32 de3Id;
    AtSdhChannel sdhChannel = (AtSdhChannel)AtPdhChannelVcInternalGet((AtPdhChannel)self);

    if (sdhChannel)
        return m_ThaPdhDe3Methods->HwIdFactorGet(self, phyModule, slice, hwIdInSlice);

    if (PhyModuleIsOc48(phyModule))
        {
        static const uint8 cStartStsIdOfOc48ForDe3LiuPort = 48;

        de3Id = AtChannelIdGet((AtChannel)self);
        *slice = 0;
        *hwIdInSlice = (uint8)(de3Id + cStartStsIdOfOc48ForDe3LiuPort);
        return cAtOk;
        }

    /* LIU DE3 */
    de3Id = AtChannelIdGet((AtChannel)self);
    *slice = (uint8)(de3Id % 2);
    *hwIdInSlice = (uint8)(de3Id / 2 + cStartStsIdForDe3LiuPort);

    return cAtOk;
    }

static eBool ShouldConsiderLofWhenLos(ThaPdhDe3 self)
    {
    /* As this works with LIU */
    AtUnused(self);
    return cAtTrue;
    }

static eBool RetimingIsSupported(ThaPdhDe3 self)
    {
    uint32 supportedVer = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x3, 0x2, 0x1050);
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);

    return hwVersion >= supportedVer ? cAtTrue : cAtFalse;
    }

static ThaPdhRetimingController RetimingControllerCreate(ThaPdhDe3 self)
    {
    return (ThaPdhRetimingController)Tha60210061PdhDe3RetimingControllerNew((AtPdhChannel)self);
    }

static eBool TxFramerSsmIsSupported(Tha60210031PdhDe3 self)
    {
    uint32 supportedVer = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x3, 0x5, 0x1006);
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);

    return (hwVersion >= supportedVer) ? cAtTrue : cAtFalse;
    }

static void OverrideAtPdhChannel(AtPdhDe3 self)
    {
    AtPdhChannel channel = (AtPdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPdhChannelOverride, m_AtPdhChannelMethods, sizeof(m_AtPdhChannelOverride));

        mMethodOverride(m_AtPdhChannelOverride, FrameTypeSet);
        }

    mMethodsSet(channel, &m_AtPdhChannelOverride);
    }

static void OverrideThaPdhDe3(AtPdhDe3 self)
    {
    ThaPdhDe3 channel = (ThaPdhDe3)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPdhDe3Methods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhDe3Override, m_ThaPdhDe3Methods, sizeof(m_ThaPdhDe3Override));

        mMethodOverride(m_ThaPdhDe3Override, IsManagedByModulePdh);
        mMethodOverride(m_ThaPdhDe3Override, BpvCounterRead2Clear);
        mMethodOverride(m_ThaPdhDe3Override, DefaultLosThreshold);
        mMethodOverride(m_ThaPdhDe3Override, HwIdFactorGet);
        mMethodOverride(m_ThaPdhDe3Override, ShouldConsiderLofWhenLos);
        mMethodOverride(m_ThaPdhDe3Override, RetimingControllerCreate);
        mMethodOverride(m_ThaPdhDe3Override, RetimingIsSupported);
        }

    mMethodsSet(channel, &m_ThaPdhDe3Override);
    }

static void OverrideTha60210031PdhDe3(AtPdhDe3 self)
    {
    Tha60210031PdhDe3 channel = (Tha60210031PdhDe3)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210031PdhDe3Override, mMethodsGet(channel), sizeof(m_Tha60210031PdhDe3Override));

        mMethodOverride(m_Tha60210031PdhDe3Override, TxFramerSsmIsSupported);
        }

    mMethodsSet(channel, &m_Tha60210031PdhDe3Override);
    }

static void OverrideAtChannel(AtPdhDe3 self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, HwResourceAllocate);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtPdhDe3 self)
    {
    OverrideAtChannel(self);
    OverrideAtPdhChannel(self);
    OverrideThaPdhDe3(self);
    OverrideTha60210031PdhDe3(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061PdhDe3);
    }

static AtPdhDe3 ObjectInit(AtPdhDe3 self, uint32 channelId, AtModulePdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Supper constructor */
    if (Tha60210012PdhDe3ObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPdhDe3 Tha60210061PdhDe3New(uint32 channelId, AtModulePdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhDe3 newDe3 = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDe3 == NULL)
        return NULL;

    /* construct it */
    return ObjectInit(newDe3, channelId, module);
    }

uint32 Tha60210061PdhDe3LiuPdhIdGet(AtPdhDe3 self, uint8 *sliceId)
    {
    uint32 regAddr, regVal;

    if (self == NULL)
        return cInvalidUint32;

    regAddr = cAf6Reg_LIU_Tx_Channel_Lookup_Base + AtChannelIdGet((AtChannel)self) + LiuBaseAddress();
    regVal = mChannelHwRead(self, regAddr, cAtModulePdh);
    *sliceId = (uint8)mRegField(regVal, cAf6_LIU_Tx_Channel_Lookup_LIUTxBusSel_);
    return mRegField(regVal, cAf6_LIU_Tx_Channel_Lookup_LIUTxPDHID_);
    }


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60210061PdhDebugger.c
 *
 * Created Date: Aug 22, 2017
 *
 * Description : PDH debugger
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210012/pdh/Tha60210012ModulePdhInternal.h"
#include "Tha60210061PdhDe1.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210061PdhDebugger)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061PdhDebugger * Tha60210061PdhDebugger;
typedef struct tTha60210061PdhDebugger
	{
	tTha60210011PdhDebugger super;
	}tTha60210061PdhDebugger;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/*--------------------------- Forward declarations ---------------------------*/
/* Override */
static tThaPdhDebuggerMethods m_ThaPdhDebuggerOverride;

/* Super implementation */
static const tThaPdhDebuggerMethods *m_ThaPdhDebuggerMethods = NULL;

/*--------------------------- Implementation ---------------------------------*/
static eAtRet De1LiuLoopbackSet(ThaPdhDebugger self, AtPdhDe1 de1, uint8 loopbackMode)
    {
    AtUnused(self);
    return Tha60210061PdhDe1LiuLoopbackSet(de1, loopbackMode);
    }

static uint8 De1LiuLoopbackGet(ThaPdhDebugger self, AtPdhDe1 de1)
    {
    AtUnused(self);
    return Tha60210061PdhDe1LiuLoopbackGet(de1);
    }

static void OverrideThaPdhDebugger(ThaPdhDebugger self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPdhDebuggerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhDebuggerOverride, m_ThaPdhDebuggerMethods, sizeof(m_ThaPdhDebuggerOverride));

        mMethodOverride(m_ThaPdhDebuggerOverride, De1LiuLoopbackSet);
        mMethodOverride(m_ThaPdhDebuggerOverride, De1LiuLoopbackGet);
        }

    mMethodsSet(self, &m_ThaPdhDebuggerOverride);
    }

static void Override(ThaPdhDebugger self)
    {
    OverrideThaPdhDebugger(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061PdhDebugger);
    }

static ThaPdhDebugger ObjectInit(ThaPdhDebugger self, ThaModulePdh modulePdh)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011PdhDebuggerObjectInit(self, modulePdh) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPdhDebugger Tha60210061PdhDebuggerNew(ThaModulePdh modulePdh)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPdhDebugger newObject = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newObject, modulePdh);
    }

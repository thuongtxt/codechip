/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : Tha60210061ModulePdhReg.h
 * 
 * Created Date: Aug 22, 2016
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061MODULEPDHREG_H_
#define _THA60210061MODULEPDHREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/*------------------------------------------------------------------------------
Reg Name   : RxM23E23 Control
Reg Addr   : 0x00040000 - 0x0004001F
Reg Formula: 0x00040000 +  de3id
    Where  :
           + $de3id(0-26):
Reg Desc   :
The RxM23E23 Control is use to configure for per channel Rx DS3/E3 operation.

------------------------------------------------------------------------------*/
#define cAf6Reg_rxm23e23_ctrl                                                                       0x00040000

/*--------------------------------------
BitField Name: RXDS3ForceAISLbit
BitField Type: RW
BitField Desc: Force DS3 AIS, L-bit to PSN, data all one to PSN for Payload/Line
Remote Loopback
BitField Bits: [19]
--------------------------------------*/
#define cAf6_rxm23e23_ctrl_RXDS3ForceAISLbit_Mask                                                       cBit19
#define cAf6_rxm23e23_ctrl_RXDS3ForceAISLbit_Shift                                                          19

/*--------------------------------------
BitField Name: RxDE3MonOnly
BitField Type: RW
BitField Desc: Rx Framer Monitor only
BitField Bits: [18]
--------------------------------------*/
#define cAf6_rxm23e23_ctrl_RxDE3MonOnly_Mask                                                            cBit18
#define cAf6_rxm23e23_ctrl_RxDE3MonOnly_Shift                                                               18

/*--------------------------------------
BitField Name: RxDS3E3ChEnb
BitField Type: RW
BitField Desc:
BitField Bits: [17]
--------------------------------------*/
#define cAf6_rxm23e23_ctrl_RxDS3E3ChEnb_Mask                                                            cBit17
#define cAf6_rxm23e23_ctrl_RxDS3E3ChEnb_Shift                                                               17

/*--------------------------------------
BitField Name: RxDS3E3LosThres
BitField Type: RW
BitField Desc:
BitField Bits: [16:14]
--------------------------------------*/
#define cAf6_rxm23e23_ctrl_RxDS3E3LosThres_Mask                                                      cBit16_14
#define cAf6_rxm23e23_ctrl_RxDS3E3LosThres_Shift                                                            14

/*--------------------------------------
BitField Name: RxDS3E3AisThres
BitField Type: RW
BitField Desc:
BitField Bits: [13:11]
--------------------------------------*/
#define cAf6_rxm23e23_ctrl_RxDS3E3AisThres_Mask                                                      cBit13_11
#define cAf6_rxm23e23_ctrl_RxDS3E3AisThres_Shift                                                            11

/*--------------------------------------
BitField Name: RxDS3E3LofThres
BitField Type: RW
BitField Desc:
BitField Bits: [10:8]
--------------------------------------*/
#define cAf6_rxm23e23_ctrl_RxDS3E3LofThres_Mask                                                       cBit10_8
#define cAf6_rxm23e23_ctrl_RxDS3E3LofThres_Shift                                                             8

/*--------------------------------------
BitField Name: RxDS3E3PayAllOne
BitField Type: RW
BitField Desc: This bit is set to 1 to force the DS3/E3 framer payload AIS
downstream
BitField Bits: [7]
--------------------------------------*/
#define cAf6_rxm23e23_ctrl_RxDS3E3PayAllOne_Mask                                                         cBit7
#define cAf6_rxm23e23_ctrl_RxDS3E3PayAllOne_Shift                                                            7

/*--------------------------------------
BitField Name: RxDS3E3LineAllOne
BitField Type: RW
BitField Desc: This bit is set to 1 to force the DS3/E3 framer line AIS
downstream
BitField Bits: [6]
--------------------------------------*/
#define cAf6_rxm23e23_ctrl_RxDS3E3LineAllOne_Mask                                                        cBit6
#define cAf6_rxm23e23_ctrl_RxDS3E3LineAllOne_Shift                                                           6

/*--------------------------------------
BitField Name: RxDS3E3LineLoop
BitField Type: RW
BitField Desc:
BitField Bits: [5]
--------------------------------------*/
#define cAf6_rxm23e23_ctrl_RxDS3E3LineLoop_Mask                                                          cBit5
#define cAf6_rxm23e23_ctrl_RxDS3E3LineLoop_Shift                                                             5

/*--------------------------------------
BitField Name: RxDS3E3FrcLof
BitField Type: RW
BitField Desc: This bit is set to 1 to force the Rx DS3/E3 framer into LOF
status. 1: force LOF. 0: not force LOF.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_rxm23e23_ctrl_RxDS3E3FrcLof_Mask                                                            cBit4
#define cAf6_rxm23e23_ctrl_RxDS3E3FrcLof_Shift                                                               4

/*--------------------------------------
BitField Name: RxDS3E3Md
BitField Type: RW
BitField Desc: Rx DS3/E3 framing mode 0000: DS3 Unframed 0001: DS3 C-bit parity
carrying 28 DS1s or 21 E1s 0010: DS3 M23 carrying 28 DS1s or 21 E1s 0011: DS3
C-bit map 0100: E3 Unframed 0101: E3 G832 0110: E3 G.751 carrying 16 E1s 0111:
E3 G.751 Map 1000: Bypass RxM13E13 (DS1/E1 mode)
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_rxm23e23_ctrl_RxDS3E3Md_Mask                                                              cBit3_0
#define cAf6_rxm23e23_ctrl_RxDS3E3Md_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : RxM23E23 HW Status
Reg Addr   : 0x00040080-0x0004009F
Reg Formula: 0x00040080 +  de3id
    Where  :
           + $de3id(0-26):
Reg Desc   :
for HW debug only

------------------------------------------------------------------------------*/
#define cAf6Reg_rxm23e23_hw_stat                                                                    0x00040080

/*--------------------------------------
BitField Name: RxDS3_CrrAISDownStr
BitField Type: RW
BitField Desc: AIS Down Stream due to HI_Level AIS of DS3E3
BitField Bits: [5]
--------------------------------------*/
#define cAf6_rxm23e23_hw_stat_RxDS3_CrrAISDownStr_Mask                                                   cBit5
#define cAf6_rxm23e23_hw_stat_RxDS3_CrrAISDownStr_Shift                                                      5

/*--------------------------------------
BitField Name: RxDS3_CrrLosAllZero
BitField Type: RO
BitField Desc: LOS All zero of DS3E3
BitField Bits: [4]
--------------------------------------*/
#define cAf6_rxm23e23_hw_stat_RxDS3_CrrLosAllZero_Mask                                                   cBit4
#define cAf6_rxm23e23_hw_stat_RxDS3_CrrLosAllZero_Shift                                                      4

/*--------------------------------------
BitField Name: RxDS3_CrrAISAllOne
BitField Type: RO
BitField Desc: AIS All one of DS3E3
BitField Bits: [3]
--------------------------------------*/
#define cAf6_rxm23e23_hw_stat_RxDS3_CrrAISAllOne_Mask                                                    cBit3
#define cAf6_rxm23e23_hw_stat_RxDS3_CrrAISAllOne_Shift                                                       3

/*--------------------------------------
BitField Name: RxDS3_CrrAISSignal
BitField Type: RO
BitField Desc: AIS signal of DS3
BitField Bits: [2]
--------------------------------------*/
#define cAf6_rxm23e23_hw_stat_RxDS3_CrrAISSignal_Mask                                                    cBit2
#define cAf6_rxm23e23_hw_stat_RxDS3_CrrAISSignal_Shift                                                       2

/*--------------------------------------
BitField Name: RxM23E23Status
BitField Type: RO
BitField Desc: for HW debug only
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_rxm23e23_hw_stat_RxM23E23Status_Mask                                                      cBit1_0
#define cAf6_rxm23e23_hw_stat_RxM23E23Status_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : RxDS3E3 OH Pro Control
Reg Addr   : 0x040420-0x04043F
Reg Formula: 0x040420 +  de3id
    Where  :
           + $de3id(0-26):
Reg Desc   :


------------------------------------------------------------------------------*/
#define cAf6Reg_rx_de3_oh_ctrl                                                                        0x040420
#define cAf6Reg_rx_de3_oh_ctrl_WidthVal                                                                     32

/*--------------------------------------
BitField Name: DLEn
BitField Type: RW
BitField Desc: DLEn 1: Data Link send to HDLC 0: Data Link not send to HDLC
BitField Bits: [5]
--------------------------------------*/
#define cAf6_rx_de3_oh_ctrl_DLEn_Mask                                                                    cBit5
#define cAf6_rx_de3_oh_ctrl_DLEn_Shift                                                                       5

/*--------------------------------------
BitField Name: Ds3E3DLSL
BitField Type: RW
BitField Desc: 00: DL 01: UDL 10: FEAC 11: NA
BitField Bits: [4:3]
--------------------------------------*/
#define cAf6_rx_de3_oh_ctrl_Ds3E3DLSL_Mask                                                             cBit4_3
#define cAf6_rx_de3_oh_ctrl_Ds3E3DLSL_Shift                                                                  3

/*--------------------------------------
BitField Name: Ds3FEACEn
BitField Type: RW
BitField Desc: Enable detection FEAC state change
BitField Bits: [2]
--------------------------------------*/
#define cAf6_rx_de3_oh_ctrl_Ds3FEACEn_Mask                                                               cBit2
#define cAf6_rx_de3_oh_ctrl_Ds3FEACEn_Shift                                                                  2

/*--------------------------------------
BitField Name: FBEFbitCntEn
BitField Type: RW
BitField Desc: Enable FBE counts F-Bit
BitField Bits: [1]
--------------------------------------*/
#define cAf6_rx_de3_oh_ctrl_FBEFbitCntEn_Mask                                                            cBit1
#define cAf6_rx_de3_oh_ctrl_FBEFbitCntEn_Shift                                                               1

/*--------------------------------------
BitField Name: FBEMbitCntEn
BitField Type: RW
BitField Desc: Enable FBE counts M-Bit
BitField Bits: [0]
--------------------------------------*/
#define cAf6_rx_de3_oh_ctrl_FBEMbitCntEn_Mask                                                            cBit0
#define cAf6_rx_de3_oh_ctrl_FBEMbitCntEn_Shift                                                               0

/*------------------------------------------------------------------------------
Reg Name   : TxM23E23 Control
Reg Addr   : 0x00080000 - 0x0008001F
Reg Formula: 0x00080000 +  de3id
    Where  :
           + $de3id(0-26)
Reg Desc   :
The TxM23E23 Control is use to configure for per channel Tx DS3/E3 operation.

------------------------------------------------------------------------------*/
#define cAf6Reg_txm23e23_ctrl                                                                       0x00080000

/*--------------------------------------
BitField Name: TxDS3E3ChEnb
BitField Type: RW
BitField Desc:
BitField Bits: [28]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3ChEnb_Mask                                                            cBit28
#define cAf6_txm23e23_ctrl_TxDS3E3ChEnb_Shift                                                               28

/*--------------------------------------
BitField Name: TxDS3E3DLKMode
BitField Type: RW
BitField Desc: 0:DS3DL/E3G751NA/E3G832GC; 1:DS3UDL/E3G832NR; 2: DS3FEAC; 3:DS3NA
BitField Bits: [27:26]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3DLKMode_Mask                                                       cBit27_26
#define cAf6_txm23e23_ctrl_TxDS3E3DLKMode_Shift                                                             26

/*--------------------------------------
BitField Name: TxDS3E3DLKEnb
BitField Type: RW
BitField Desc: Set 1 to enable DLK DS3
BitField Bits: [25]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3DLKEnb_Mask                                                           cBit25
#define cAf6_txm23e23_ctrl_TxDS3E3DLKEnb_Shift                                                              25

/*--------------------------------------
BitField Name: TxDS3E3FeacThres
BitField Type: RW
BitField Desc:
BitField Bits: [24:21]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3FeacThres_Mask                                                     cBit24_21
#define cAf6_txm23e23_ctrl_TxDS3E3FeacThres_Shift                                                           21

/*--------------------------------------
BitField Name: TxDS3E3LineAllOne
BitField Type: RW
BitField Desc:
BitField Bits: [20]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3LineAllOne_Mask                                                       cBit20
#define cAf6_txm23e23_ctrl_TxDS3E3LineAllOne_Shift                                                          20

/*--------------------------------------
BitField Name: TxDS3E3PayLoop
BitField Type: RW
BitField Desc:
BitField Bits: [19]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3PayLoop_Mask                                                          cBit19
#define cAf6_txm23e23_ctrl_TxDS3E3PayLoop_Shift                                                             19

/*--------------------------------------
BitField Name: TxDS3E3LineLoop
BitField Type: RW
BitField Desc:
BitField Bits: [18]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3LineLoop_Mask                                                         cBit18
#define cAf6_txm23e23_ctrl_TxDS3E3LineLoop_Shift                                                            18

/*--------------------------------------
BitField Name: TxDS3E3FrcYel
BitField Type: RW
BitField Desc:
BitField Bits: [17]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3FrcYel_Mask                                                           cBit17
#define cAf6_txm23e23_ctrl_TxDS3E3FrcYel_Shift                                                              17

/*--------------------------------------
BitField Name: TxDS3E3AutoYel
BitField Type: RW
BitField Desc:
BitField Bits: [16]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3AutoYel_Mask                                                          cBit16
#define cAf6_txm23e23_ctrl_TxDS3E3AutoYel_Shift                                                             16

/*--------------------------------------
BitField Name: TxDS3E3LoopMd
BitField Type: RW
BitField Desc: DS3 C-bit mode: [15:14]: FE Control: 00: idle code; 01: activate
cw; 10: de-activate cw; 11: alrm/status E3G832 mode: [15:14]: DL mode Normal:
Loop Mode configuration
BitField Bits: [15:14]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3LoopMd_Mask                                                        cBit15_14
#define cAf6_txm23e23_ctrl_TxDS3E3LoopMd_Shift                                                              14

/*--------------------------------------
BitField Name: TxDS3E3LoopEn
BitField Type: RW
BitField Desc: DS3 C-bit mode: [12:7]: FEAC Control Word E3G832 mode: [13:11]:
Payload type [10:7]:  SSM Config Normal: [13:7]: Loop en configuration per
channel
BitField Bits: [13:7]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3LoopEn_Mask                                                         cBit13_7
#define cAf6_txm23e23_ctrl_TxDS3E3LoopEn_Shift                                                               7

/*--------------------------------------
BitField Name: TxDS3E3Ohbypass
BitField Type: RW
BitField Desc:
BitField Bits: [6]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3Ohbypass_Mask                                                          cBit6
#define cAf6_txm23e23_ctrl_TxDS3E3Ohbypass_Shift                                                             6

/*--------------------------------------
BitField Name: TxDS3E3IdleSet
BitField Type: RW
BitField Desc:
BitField Bits: [5]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3IdleSet_Mask                                                           cBit5
#define cAf6_txm23e23_ctrl_TxDS3E3IdleSet_Shift                                                              5

/*--------------------------------------
BitField Name: TxDS3E3AisSet
BitField Type: RW
BitField Desc:
BitField Bits: [4]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3AisSet_Mask                                                            cBit4
#define cAf6_txm23e23_ctrl_TxDS3E3AisSet_Shift                                                               4

/*--------------------------------------
BitField Name: TxDS3E3Md
BitField Type: RW
BitField Desc: Tx DS3/E3 framing mode 0000: DS3 Unframed 0001: DS3 C-bit parity
carrying 28 DS1s or 21 E1s 0010: DS3 M23 carrying 28 DS1s or 21 E1s 0011: DS3
C-bit map 0100: E3 Unframed 0101: E3 G832 0110: E3 G.751 carrying 16 E1s 0111:
E3 G751 Map 1000: Bypass RxM13E13 (DS1/E1 mode)
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3Md_Mask                                                              cBit3_0
#define cAf6_txm23e23_ctrl_TxDS3E3Md_Shift                                                                   0

/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer Mux Control
Reg Addr   : 0x00730000 - 0x0003001F
Reg Formula: 0x00730000 +  stsid
    Where  :
           + $StsID(0-26):
Reg Desc   :
These registers are used to configure for the source data of Ds1/E1 which may be get from Serial PDH interface or from SONET/SDH.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rxfrm_mux_ctrl_Base                                                            0x00730000
#define cAf6Reg_dej1_rxfrm_mux_ctrl(stsid)                                                (0x00730000+(stsid))
#define cAf6Reg_dej1_rxfrm_mux_ctrl_WidthVal                                                                32
#define cAf6Reg_dej1_rxfrm_mux_ctrl_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: PDHRxMuxSel
BitField Type: RW
BitField Desc: 28 selector bit for 28 DS1/E1/J1 in a STS/VC 1: source selected
from PDH Interface 0 (default): source selected from SONET/SDH Interface
BitField Bits: [27:0]
--------------------------------------*/
#define cAf6_dej1_rxfrm_mux_ctrl_PDHRxMuxSel_Bit_Start                                                       0
#define cAf6_dej1_rxfrm_mux_ctrl_PDHRxMuxSel_Bit_End                                                        27
#define cAf6_dej1_rxfrm_mux_ctrl_PDHRxMuxSel_Mask                                                     cBit27_0
#define cAf6_dej1_rxfrm_mux_ctrl_PDHRxMuxSel_Shift                                                           0
#define cAf6_dej1_rxfrm_mux_ctrl_PDHRxMuxSel_MaxVal                                                  0xfffffff
#define cAf6_dej1_rxfrm_mux_ctrl_PDHRxMuxSel_MinVal                                                        0x0
#define cAf6_dej1_rxfrm_mux_ctrl_PDHRxMuxSel_RstVal                                                        0x0

/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer per STS/VC Interrupt Enable Control
Reg Addr   : 0x00055FFE
Reg Formula:
    Where  :
Reg Desc   :
The register consists of 27 interrupt enable bits for 27 STS/VCs in the Rx DS1/E1/J1 Framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rx_framer_per_stsvc_intr_en_ctrl                                               0x00055FFE

/*--------------------------------------
BitField Name: RxDE1StsIntrEn
BitField Type: RW
BitField Desc: Set to 1 to enable the related STS/VC to generate interrupt.
BitField Bits: [26:0]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_stsvc_intr_en_ctrl_RxDE1StsIntrEn_Mask                                cBit26_0
#define cAf6_dej1_rx_framer_per_stsvc_intr_en_ctrl_RxDE1StsIntrEn_Shift                                       0

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061MODULEPDHREG_H_ */


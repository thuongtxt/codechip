/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60210061ModulePdh.h
 * 
 * Created Date: Jun 28, 2016
 *
 * Description : 60210061 module PDH
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061MODULEPDH_H_
#define _THA60210061MODULEPDH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtCpld.h"
#include "../../../default/pdh/ThaModulePdh.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cNumLiuPorts                16UL
#define cStartStsIdForDe3LiuPort    24UL
#define cStartStsIdForDe1LiuPort    26UL
#define cNumTotalLiuDe3             4UL
#define cNumTu11InTug2              4UL
#define cNumTu12InTug2              3UL

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtCpld Tha60210061ModulePdhCpld(AtModulePdh self);
eBool Tha60210061ModulePdhChannelIsLiuChannel(AtPdhChannel pdhChannel);
eAtRet Tha60210061ModulePdhLiuDe1FramerAllocate(ThaModulePdh self, AtPdhDe1 de1, eBool isE1);
eAtRet Tha60210061ModulePdhLiuDe1FramerDeallocate(ThaModulePdh self, AtPdhDe1 de1, eBool isE1);
uint32 Tha60210061ModulePdhLiuBaseAddress(void);
void Tha60210061ModulePdhRxStsVtgTypeDefault(ThaModulePdh self);
eBool Tha60210061ModulePdhBpvCounterIsSupported(ThaModulePdh self);
AtPdhDe3 Tha60210061PdhDe3Get(AtModulePdh self, uint32 de3Id);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061MODULEPDH_H_ */


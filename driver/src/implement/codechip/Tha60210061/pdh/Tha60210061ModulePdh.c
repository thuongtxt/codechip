/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60210061ModulePdh.c
 *
 * Created Date: Jun 17, 2016
 *
 * Description : 60210061 Module PDH
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/sdh/AtModuleSdhInternal.h"
#include "../../../../generic/sdh/AtSdhChannelInternal.h"
#include "../../../../generic/sdh/AtSdhVcInternal.h"
#include "../../../default/util/ThaBitMask.h"
#include "../../../default/ocn/ThaModuleOcn.h"
#include "../../../default/man/ThaDevice.h"
#include "../../../default/pdh/ThaModulePdhForStmReg.h"
#include "../../../default/pdh/ThaPdhPrmController.h"
#include "../../../default/pdh/prm/ThaPdhPrmInterruptManager.h"
#include "../../../default/pdh/mdl/ThaPdhMdlInterruptManager.h"
#include "../../Tha60210012/pdh/Tha60210012ModulePdhInternal.h"
#include "../../Tha60210031/pdh/Tha60210031PdhSerialLine.h"
#include "../../Tha60210031/pdh/Tha60210031PdhDe1.h"
#include "../../Tha60210031/pdh/Tha60210031PdhMdlControllerReg.h"
#include "../../Tha60210012/sdh/Tha60210012ModuleSdh.h"
#include "../../Tha60150011/man/Tha60150011Device.h"
#include "../../Tha60210011/common/Tha602100xxCommon.h"
#include "../sdh/Tha60210061ModuleSdh.h"
#include "../cdr/Tha60210061ModuleCdr.h"
#include "../map/Tha60210061ModuleMap.h"
#include "Tha60210061ModulePdh.h"
#include "Tha60210061LiuReg.h"
#include "Tha60210061ModulePdhReg.h"
#include "Tha60210061PdhDe1.h"
#include "Tha60210061PdhDe3.h"
#include "Tha60210061PdhSerialLine.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)         ((tTha60210061ModulePdh *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061ModulePdh
    {
    tTha60210012ModulePdh super;

    /* Private data */
    AtCpld cpld;
    ThaBitMask ds1InUsedBitMask;
    ThaBitMask e1InUsedBitMask;
    }tTha60210061ModulePdh;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods             m_AtObjectOverride;
static tAtModuleMethods             m_AtModuleOverride;
static tAtModulePdhMethods          m_AtModulePdhOverride;
static tThaModulePdhMethods         m_ThaModulePdhOverride;
static tTha60210031ModulePdhMethods m_Tha60210031ModulePdhOverride;
static tTha60210011ModulePdhMethods m_Tha60210011ModulePdhOverride;

/* Save super implementation */
static const tAtObjectMethods     *m_AtObjectMethods      = NULL;
static const tAtModuleMethods     *m_AtModuleMethods      = NULL;
static const tAtModulePdhMethods  *m_AtModulePdhMethods   = NULL;
static const tThaModulePdhMethods *m_ThaModulePdhMethods  = NULL;
static const tTha60210031ModulePdhMethods *m_Tha60210031ModulePdhMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleSdh ModuleSdh(ThaModulePdh self)
    {
    return (AtModuleSdh)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModuleSdh);
    }

static void De1InusedBitMaskResourcesDelete(AtModulePdh self)
    {
    AtObjectDelete((AtObject)mThis(self)->ds1InUsedBitMask);
    mThis(self)->ds1InUsedBitMask = NULL;

    AtObjectDelete((AtObject)mThis(self)->e1InUsedBitMask);
    mThis(self)->e1InUsedBitMask = NULL;
    }

static eAtRet AllDe1sDelete(AtModulePdh self)
    {
    AtAssert(m_AtModulePdhMethods->AllDe1sDelete(self) == cAtOk);

    De1InusedBitMaskResourcesDelete(self);
    return cAtOk;
    }

static eAtRet AllLiuDe1FramerAllocate(AtModulePdh self)
    {
    eAtRet ret = cAtOk;
    uint32 de1_i;

    for (de1_i = 0; de1_i < AtModulePdhNumberOfDe1sGet(self); de1_i++)
        {
        AtPdhDe1 de1 = AtModulePdhDe1Get(self, de1_i);
        ret |= Tha60210061ModulePdhLiuDe1FramerAllocate((ThaModulePdh)self, de1, cAtFalse);
        }

    return ret;
    }

static eAtRet AllDe1Create(AtModulePdh self)
    {
    eAtRet ret = m_AtModulePdhMethods->AllDe1Create(self);
    if (ret != cAtOk)
        return ret;

    return AllLiuDe1FramerAllocate(self);
    }

static uint32 NumberOfDe3sGet(AtModulePdh self)
    {
    AtUnused(self);
    return 4;
    }

static uint32 NumberOfDe1sGet(AtModulePdh self)
    {
    AtUnused(self);
    return 12;
    }

static uint32 NumberOfDe3SerialLinesGet(AtModulePdh self)
    {
    AtUnused(self);
    return 4;
    }

static eBool ChannelIsDe1(AtPdhChannel channel)
    {
    eAtPdhChannelType channelType = AtPdhChannelTypeGet(channel);
    if ((channelType == cAtPdhChannelTypeDs1) ||
        (channelType == cAtPdhChannelTypeE1)  ||
        (channelType == cAtPdhChannelTypeDe1Unknown))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 NewLiuDe1DiagBaseAddress(ThaModulePdh self)
    {
    if (Tha60150011DeviceSemUartIsSupported(AtModuleDeviceGet((AtModule)self)))
        return 0xF2E000;

    return 0xF26000;
    }

static uint32 DiagLineBaseAddress(ThaModulePdh self, AtPdhChannel channel)
    {
    AtUnused(self);
    if (ChannelIsDe1(channel))
        return NewLiuDe1DiagBaseAddress(self);

    return 0xf28000;
    }

static eBool HasLiu(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static AtPdhDe1 De1Create(AtModulePdh self, uint32 de1Id)
    {
    return Tha60210061PdhDe1New(de1Id, self);
    }

static AtPdhSerialLine De3SerialCreate(AtModulePdh self, uint32 serialLineId)
    {
    return Tha60210061PdhSerialLineNew(serialLineId, self);
    }

static AtPdhDe3 De3Create(AtModulePdh self, uint32 de3Id)
    {
    return Tha60210061PdhDe3New(de3Id, self);
    }

static AtCpld Cpld(AtModulePdh self)
    {
    AtHal hal;
    AtDevice device;

    if (mThis(self)->cpld)
        return mThis(self)->cpld;

    device = AtModuleDeviceGet((AtModule)self);
    hal = AtDeviceIpCoreHalGet(device, AtModuleDefaultCoreGet((AtModule)self));
    mThis(self)->cpld = AtCpldNew(0, hal);

    return mThis(self)->cpld;
    }

static void Delete(AtObject self)
    {
    if (mThis(self)->cpld)
        {
        AtCpldDelete(mThis(self)->cpld);
        mThis(self)->cpld = NULL;
        }

    De1InusedBitMaskResourcesDelete((AtModulePdh)self);

    m_AtObjectMethods->Delete(self);
    }

static eBool InbandLoopcodeIsSupported(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 LiuBaseAddress(void)
    {
    return Tha60210061ModulePdhLiuBaseAddress();
    }

static void LiuInit(AtModule self)
    {
    uint32 regVal, pdhId;
    uint8 busId, portId;

    /* Initialize all LIU ports to no invert mode */
    mModuleHwWrite(self, cAf6Reg_LIU_Rx_Clock_Invert_Base + LiuBaseAddress(), 0);
    mModuleHwWrite(self, cAf6Reg_LIU_Tx_Clock_Invert_Base + LiuBaseAddress(), 0);

    /* Release loop-back for all LIU ports  */
    mModuleHwWrite(self, cAf6Reg_LIU_Rx_loopin_Base + LiuBaseAddress(), 0);
    mModuleHwWrite(self, cAf6Reg_LIU_Tx_loopout_Base + LiuBaseAddress(), 0);

    /* Initialize TX/RX lookup between LIU ports and PDH channels */
    for (portId = 0; portId < cNumLiuPorts; portId++)
        {
        regVal = 0;

        if (portId < cNumTotalLiuDe3)
            {
            pdhId = (uint32)(32 * (cStartStsIdForDe3LiuPort + (portId / cNumLiuDe3PerSlice)));
            busId = portId % cNumLiuDe3PerSlice;
            mRegFieldSet(regVal, cAf6_LIU_Rx_Channel_Lookup_LIURxPDHID_, pdhId);
            mRegFieldSet(regVal, cAf6_LIU_Rx_Channel_Lookup_LIURxModeSel_, 3);
            mRegFieldSet(regVal, cAf6_LIU_Rx_Channel_Lookup_LIURxBusSel_, busId);
            }
        else
            {
            pdhId = (uint32)(32 * cStartStsIdForDe1LiuPort) + (portId - cNumTotalLiuDe3);
            mRegFieldSet(regVal, cAf6_LIU_Rx_Channel_Lookup_LIURxPDHID_, pdhId);
            mRegFieldSet(regVal, cAf6_LIU_Rx_Channel_Lookup_LIURxModeSel_, 0);
            mRegFieldSet(regVal, cAf6_LIU_Rx_Channel_Lookup_LIURxBusSel_, 0);
            }

        mModuleHwWrite(self, cAf6Reg_LIU_Rx_Channel_Lookup_Base + portId + LiuBaseAddress(), regVal);
        mModuleHwWrite(self, cAf6Reg_LIU_Tx_Channel_Lookup_Base + portId + LiuBaseAddress(), regVal);
        }

    /* Enable Tx direction */
    mModuleHwWrite(self, cAf6Reg_LIU_Tx_Channel_Enable_Base + LiuBaseAddress(), 0xFFFF);
    }

static uint8 NumSliceForPdhLiu(ThaModulePdh self)
    {
    AtUnused(self);
    return 2;
    }

static void Ds1LiuModeDefault(ThaModulePdh self)
    {
    uint8 slice;
    uint32 regValues;

    for (slice = 0; slice < NumSliceForPdhLiu(self); slice++)
        {
        static const uint8 cPdhDe1HwSts = 26;
        uint32 offset = (uint32)(mMethodsGet(self)->SliceOffset(self, slice) + cPdhDe1HwSts);
        regValues = mModuleHwRead(self, cAf6Reg_dej1_rxfrm_mux_ctrl_Base + offset);
        mRegFieldSet(regValues, cAf6_dej1_rxfrm_mux_ctrl_PDHRxMuxSel_, 0xfffffff);
        mModuleHwWrite(self, cAf6Reg_dej1_rxfrm_mux_ctrl_Base + offset, regValues);
        }
    }

static void RxStsVtgTypePerSliceSet(ThaModulePdh self, eThaPdhStsPayloadType vtgType, uint8 slice, uint8 hwSts)
    {
    uint8 vtg_i;
    uint32 regValues;
    static const uint8 cMaxNumVtgId = 7;
    uint32 regBase = ThaModulePdhRxPerStsVCPldCtrlRegister(self);

    uint32 offset = (uint32)(mMethodsGet(self)->SliceOffset(self, slice) + hwSts);
    regValues = mModuleHwRead(self, regBase + offset);
    for (vtg_i = 0; vtg_i < cMaxNumVtgId; vtg_i++)
        mFieldIns(&regValues, cThaPdhVtRxMapTug2TypeMask2(vtg_i), cThaPdhVtRxMapTug2TypeShift2(vtg_i), vtgType);
    mModuleHwWrite(self, regBase + offset, regValues);
    }

static void RxStsVtgTypeDefault(ThaModulePdh self)
    {
    static const uint8 cPdhDe1HwSts = 26;
    RxStsVtgTypePerSliceSet(self, cThaPdhTu11Vt15, 0, cPdhDe1HwSts);
    RxStsVtgTypePerSliceSet(self, cThaPdhTu12Vt2, 1, cPdhDe1HwSts);
    }

static eAtRet Init(AtModule self)
    {
    ThaModuleCdr cdrModule;

    /* LIU init must be done before super init, while super init, default frame type of PDH channel are set
     * LIU init after super will flush all right things that done by super init */
    LiuInit(self);

    cdrModule = (ThaModuleCdr)AtDeviceModuleGet(AtModuleDeviceGet(self), cThaModuleCdr);
    Tha60210061ModuleCdrDe1LineModeControlDefault(cdrModule);
    Tha60210061ModuleCdrDe1LineTypeAndVtModeSet(cdrModule);
    Tha60210061ModuleMapLineControlSigTypeDefault((ThaModuleMap)AtDeviceModuleGet(AtModuleDeviceGet(self), cThaModuleMap));
    Ds1LiuModeDefault((ThaModulePdh)self);
    RxStsVtgTypeDefault((ThaModulePdh)self);
    
    return m_AtModuleMethods->Init(self);
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static uint32 M23E23ControlOffset(ThaModulePdh self, AtPdhChannel de3)
    {
    uint8 slice, hwIdInSlice;
    ThaPdhChannelHwIdGet(de3, cAtModulePdh, &slice, &hwIdInSlice);

    return (uint32)(mMethodsGet(self)->BaseAddress(self) + (slice * 0x100000UL) + hwIdInSlice);
    }

static void De3RegsShow(ThaModulePdh self, AtPdhChannel de3)
    {
    uint32 address = cAf6Reg_txm23e23_ctrl + M23E23ControlOffset(self, de3);

    ThaDeviceRegNameDisplay("TxM23E23 Control");
    ThaDeviceChannelRegValueDisplay((AtChannel)de3, address, cAtModulePdh);

    address = cAf6Reg_rxm23e23_ctrl + M23E23ControlOffset(self, de3);
    ThaDeviceRegNameDisplay("RxM23E23 Control");
    ThaDeviceChannelRegValueDisplay((AtChannel)de3, address, cAtModulePdh);

    address = cAf6Reg_rxm23e23_hw_stat + M23E23ControlOffset(self, de3);
    ThaDeviceRegNameDisplay("RxM23E23 HW Status");
    ThaDeviceChannelRegValueDisplay((AtChannel)de3, address, cAtModulePdh);

    address = cAf6Reg_rx_de3_oh_ctrl + M23E23ControlOffset(self, de3);
    ThaDeviceRegNameDisplay("RxDS3E3 OH Pro Control");
    ThaDeviceChannelRegValueDisplay((AtChannel)de3, address, cAtModulePdh);

    if (Tha60210061ModulePdhChannelIsLiuChannel(de3))
        {
        address = cAf6Reg_LIU_Tx_Channel_Lookup_Base + AtChannelIdGet((AtChannel)de3) + LiuBaseAddress();
        ThaDeviceRegNameDisplay("LIU Tx Channel Lookup");
        ThaDeviceChannelRegValueDisplay((AtChannel)de3, address, cAtModulePdh);

        address = cAf6Reg_LIU_Rx_Channel_Lookup_Base + AtChannelIdGet((AtChannel)de3) + LiuBaseAddress();
        ThaDeviceRegNameDisplay("LIU Rx Channel Lookup");
        ThaDeviceChannelRegValueDisplay((AtChannel)de3, address, cAtModulePdh);
        }
    }

static void PdhChannelRegsShow(ThaModulePdh self, AtPdhChannel pdhChannel)
    {
    eAtPdhChannelType channelType = AtPdhChannelTypeGet(pdhChannel);

    m_ThaModulePdhMethods->PdhChannelRegsShow(self, pdhChannel);
    if ((channelType == cAtPdhChannelTypeDs3) || (channelType == cAtPdhChannelTypeE3))
        De3RegsShow(self, pdhChannel);
    }

static AtPdhDe3 VcDe3Create(AtModulePdh self, AtSdhChannel sdhVc)
    {
    AtSdhLine line = AtSdhChannelLineObjectGet(sdhVc);
    AtModuleSdh sdhModule = (AtModuleSdh)AtChannelModuleGet((AtChannel)sdhVc);
    uint8 slice, hwSts;

    if (AtSdhLineRateGet(line) == cAtSdhLineRateStm0)
        {
        uint32 lineId = AtSdhChannelLineGet(sdhVc);
        return m_AtModulePdhMethods->De3Get(self, lineId - AtModuleSdhStartEc1LineIdGet(sdhModule));
        }

    ThaSdhChannel2HwMasterStsId(sdhVc, cAtModulePdh, &slice, &hwSts);
    return Tha60210061PdhVcDe3New(hwSts, self);
    }

static uint32 StartVersionSupportDe3FramedSatop(ThaModulePdh self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 De1PrmOffset(ThaModulePdh self, ThaPdhDe1 de1)
    {
    uint8 hwId, sliceId, tugOrDe2Id, tu1xOrDe1Id;
    uint32 offset, baseAddress;

    AtUnused(self);
    /*(SLICEID*1024) + (STSID*32 + VTGID*4 + VTID)*/
    if (ThaPdhChannelHwIdGet((AtPdhChannel)de1, cAtModulePdh, &sliceId, &hwId) != cAtOk)
        return cBit31_0;

    tu1xOrDe1Id = Tha60210031Tu1xOrDe1IdGet(de1, &tugOrDe2Id);
    offset = (uint32)((sliceId << 10) + (hwId << 5) + (tugOrDe2Id << 2) + tu1xOrDe1Id);
    baseAddress = ThaModulePdhPrmBaseAddress((ThaModulePdh)AtChannelModuleGet((AtChannel)de1));
    return (offset + baseAddress);
    }

static uint32 StartVersionSupportPrm(ThaModulePdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x16, 0x11, 0x08, 00);
    }

static uint32 StartVersionSupportMdlFilterMessageByType(ThaModulePdh self)
    {
    return StartVersionSupportPrm(self);
    }

static eBool MdlIsSupported(ThaModulePdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);

    if (AtDeviceIsSimulated(device))
        return cAtTrue;

    if (AtDeviceVersionNumber(device) > StartVersionSupportPrm(self))
       return cAtTrue;

    return cAtFalse;
    }

static eAtModulePdhRet PrmCrCompareEnable(AtModulePdh self, eBool enable)
    {
    return Tha60210031ModulePdhPrmCrCompareEnable(self, enable);
    }

static eBool PrmCrCompareIsEnabled(AtModulePdh self)
    {
    return Tha60210031ModulePdhPrmCrCompareIsEnabled(self);
    }

static eAtRet PrmDefaultSet(Tha60210011ModulePdh self)
    {
    if (!ThaModulePdhPrmIsSupported((ThaModulePdh)self))
        return cAtOk;

    return Tha60210031ModulePdhPrmDefaultSet((ThaModulePdh)self);
    }

static AtPdhPrmController PrmControllerObjectCreate(ThaModulePdh self, AtPdhDe1 de1)
    {
    AtUnused(self);
    return Tha60210061PdhPrmControllerNew(de1);
    }

static uint32 MdlRxBufWordOffset(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return 64UL;
    }

static uint32 MdlRxBufBaseAddress(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return 0x9800UL;
    }

static uint32 UpenRxmdlCfgtypeBase(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_upen_rxmdl_cfgtype_Base;
    }

static uint32 MdlRxStickyBaseAddress(Tha60210031ModulePdh self, uint32 bufferId)
    {
    AtUnused(self);
    return 0x0A043UL + (bufferId * 2UL);
    }

static eBool IsDe3OfStmPorts(uint8 hwDe3Id)
    {
    static const uint8 cStartDe3OfSerialLine = 24;
    return (hwDe3Id < cStartDe3OfSerialLine) ? cAtTrue : cAtFalse;
    }

static AtPdhDe1 De1ChannelFromHwIdGet(ThaModulePdh self, eAtModule phyModule, uint8 sliceId, uint8 de3Id, uint8 de2Id, uint8 de1Id)
    {
    uint32 serialId;
    AtPdhDe1 de1;

    if (IsDe3OfStmPorts(de3Id))
        return m_ThaModulePdhMethods->De1ChannelFromHwIdGet(self, phyModule, sliceId, de3Id, de2Id, de1Id);

    serialId = Tha6021ModulePdhSerialIdFromHwIdGet(self, sliceId, de3Id);
    de1 = Tha6021ModulePdhDe1FromSerialIdGet(self, serialId, de2Id, de1Id);
    if (de1)
        return de1;

    return Tha60210061De1LiuObjectFromHwId((AtModulePdh)self, sliceId, de3Id, de2Id, de1Id);
    }

static AtPdhDe3 De3ChannelFromHwIdOfStmPortsGet(ThaModulePdh self, uint8 sliceId, uint8 de3Id)
    {
    ThaModuleSdh moduleSdh = (ThaModuleSdh)ModuleSdh(self);
    AtSdhChannel au = (AtSdhChannel)ThaModuleSdhAuPathFromHwIdGet(moduleSdh, cAtModulePdh, sliceId, de3Id);
    AtSdhChannel vc3 = NULL;
    uint8 sts48;

    if (au == NULL)
        return NULL;

    sts48 = (uint8)((sliceId & 0x1U)|(uint8)(de3Id << 1));

    if (AtSdhChannelTypeGet(au) == cAtSdhChannelTypeAu3)
        vc3 = AtSdhChannelSubChannelGet(au, 0);

    else if (AtSdhChannelTypeGet(au) == cAtSdhChannelTypeAu4)
        {
        AtSdhChannel vc4, tug3;

        vc4 = AtSdhChannelSubChannelGet(au, 0);
        if (AtSdhChannelMapTypeGet(vc4) != cAtSdhVcMapTypeVc4Map3xTug3s)
            return NULL;

        tug3 = AtSdhChannelSubChannelGet(vc4, sts48 / 16);
        if (AtSdhChannelMapTypeGet(tug3) != cAtSdhTugMapTypeTug3MapVc3)
            return NULL;

        vc3 = AtSdhChannelSubChannelGet(AtSdhChannelSubChannelGet(tug3, 0), 0);
        }

    if (AtSdhChannelMapTypeGet(vc3) == cAtSdhVcMapTypeVc3MapDe3)
        return (AtPdhDe3)AtSdhChannelMapChannelGet(vc3);
    else
        return NULL;
    }

static AtPdhDe3 De3ChannelFromHwIdGet(ThaModulePdh self, eAtModule phyModule, uint8 sliceId, uint8 de3Id)
    {
    uint32 serialId;
    AtUnused(phyModule);

    if (IsDe3OfStmPorts(de3Id))
        return De3ChannelFromHwIdOfStmPortsGet(self, sliceId, de3Id);

    serialId = Tha6021ModulePdhSerialIdFromHwIdGet(self, sliceId, de3Id);
    return Tha6021ModulePdhDe3FromSerialIdGet(self, serialId);
    }

static uint8 NumStsInSlice(ThaModulePdh self)
    {
    AtUnused(self);
    return 27;
    }

static uint32 SerialIdFromHwIdGet(Tha60210031ModulePdh self, uint8 sliceId, uint8 de3InSlice)
    {
    static const uint8 cStartSerialStsInSlice = 24;
    AtUnused(self);

    if (de3InSlice < cStartSerialStsInSlice)
        return cInvalidUint32;

    return (uint32)((sliceId & 0x1)|((de3InSlice - cStartSerialStsInSlice) << 1));
    }

static ThaVersionReader VersionReader(ThaModulePdh self)
    {
    return ThaDeviceVersionReader(AtModuleDeviceGet((AtModule)self));
    }

static eBool De3FramedSatopIsSupported(ThaModulePdh self)
    {
    uint32 version = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader(self));
    uint32 startSupportedVersion = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x4, 0x1070);
    return (version >= startSupportedVersion) ? cAtTrue : cAtFalse;
    }

static eBool De3AisSelectablePatternIsSupported(ThaModulePdh self)
    {
    uint32 version = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader(self));
    uint32 startSupportedVersion = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x4, 0x1070);
    return (version >= startSupportedVersion) ? cAtTrue : cAtFalse;
    }

static eBool Ec1DiagLiuModeShouldEnable(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210061ModulePdh *object = (tTha60210061ModulePdh *)self;

    m_AtObjectMethods->Serialize(self, encoder);
    AtCoderEncodeString(encoder, AtCpldToString(object->cpld), "cpld");
    mEncodeObject(ds1InUsedBitMask);
    mEncodeObject(e1InUsedBitMask);
    }

static uint32 MdlRxPktOffset(Tha60210031ModulePdh self, uint32 type, eBool r2c)
    {
    AtUnused(self);
    switch (type)
        {
        case cAtPdhMdlControllerTypePathMessage: return (r2c) ? 0xB040 : 0xB240;
        case cAtPdhMdlControllerTypeIdleSignal:  return (r2c) ? 0xB000 : 0xB200;
        case cAtPdhMdlControllerTypeTestSignal:  return (r2c) ? 0xB080 : 0xB280;
        default:                                 return 0;
        }
    }

static uint32 MdlRxByteOffset(Tha60210031ModulePdh self, uint32 type, eBool r2c)
    {
    AtUnused(self);
    switch (type)
        {
        case cAtPdhMdlControllerTypePathMessage: return (r2c) ? 0xB440 : 0xB540;
        case cAtPdhMdlControllerTypeIdleSignal:  return (r2c) ? 0xB400 : 0xB500;
        case cAtPdhMdlControllerTypeTestSignal:  return (r2c) ? 0xB480 : 0xB580;
        default:                                 return 0;
        }
    }

static uint32 MdlRxDropOffset(Tha60210031ModulePdh self, uint32 type, eBool r2c)
    {
    AtUnused(self);
    switch (type)
        {
        case cAtPdhMdlControllerTypePathMessage: return (r2c) ? 0xB100 : 0xB300;
        case cAtPdhMdlControllerTypeIdleSignal:  return (r2c) ? 0xB0C0 : 0xB2C0;
        case cAtPdhMdlControllerTypeTestSignal:  return (r2c) ? 0xB140 : 0xB340;
        default:                                 return 0;
        }
    }

static uint32 TxBufferBaseGet(Tha60210031ModulePdh self, AtPdhDe3 de3)
    {
    uint8 hwIdInSlice, sliceId;
    uint32 addressBase, offset;

    ThaPdhChannelHwIdGet((AtPdhChannel)de3, cAtModulePdh, &sliceId, &hwIdInSlice);
    if (hwIdInSlice > 23)
        {
        hwIdInSlice = (uint8)(hwIdInSlice - 24);
        offset = (uint32)(hwIdInSlice * 0x13)+ ((uint32)sliceId * cAf6RegMdlTxOc24SliceOffset) +
                 ThaModulePdhMdlBaseAddress((ThaModulePdh)self);
        addressBase = 0x1C8 + offset;
        return addressBase;
        }

    return m_Tha60210031ModulePdhMethods->TxBufferBaseGet(self, de3);
    }

static uint32 TxBufferDwordOffset(Tha60210031ModulePdh self, uint8 sliceId, uint8 hwIdInSlice)
    {
    if (hwIdInSlice > 23)
        return 1;

    return m_Tha60210031ModulePdhMethods->TxBufferDwordOffset(self, sliceId, hwIdInSlice);
    }

static uint32 StartVersionSupportBpvCounter(void)
    {
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x8, 0x1035);
    }

static eBool De1LiuShouldReportLofWhenLos(ThaModulePdh self)
    {
    return Tha602100xxModulePdhDe1LiuShouldReportLofWhenLos(self);
    }

static eBool De3LiuShouldReportLofWhenLos(ThaModulePdh self)
    {
    return Tha602100xxModulePdhDe3LiuShouldReportLofWhenLos(self);
    }

static uint32 StartVersionSupportDe3Ssm(ThaModulePdh self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x0, 0x0, 0x0000);
    }

static uint32 StartVersionSupportDe1Ssm(ThaModulePdh self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x0, 0x0, 0x0000);
    }

static eBool PrmFcsBitOrderIsSupported(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet De1InterruptEnable(ThaModulePdh self, eBool enable)
    {
    uint32 sliceId, numSlices = ThaModulePdhNumSlices(self);
    uint32 regVal = (enable) ? cBit26_0 : 0x0;

    for (sliceId = 0; sliceId < numSlices; sliceId++)
        {
        uint32 sliceBase = (uint32)ThaModulePdhSliceBase(self, sliceId);
        mModuleHwWrite(self, sliceBase + cAf6Reg_dej1_rx_framer_per_stsvc_intr_en_ctrl, regVal);
        }

    return cAtOk;
    }

static AtPdhDe3 De3Get(AtModulePdh self, uint32 de3Id)
    {
    /* Return corresponding object in database */
    if (de3Id >= mMethodsGet(self)->NumberOfDe3SerialLinesGet(self))
        return m_AtModulePdhMethods->De3Get(self, de3Id);

    /* Hide de3 object if serial line mode is EC1 */
    if (AtPdhSerialLineModeGet(mMethodsGet(self)->De3SerialLineGet(self, de3Id)) == cAtPdhDe3SerialLineModeEc1)
        return NULL;

    return m_AtModulePdhMethods->De3Get(self, de3Id);
    }

static ThaPdhDebugger DebuggerObjectCreate(ThaModulePdh self)
    {
    return Tha60210061PdhDebuggerNew(self);
    }

static AtInterruptManager PrmInterruptManagerObjectCreate(ThaModulePdh self)
    {
    return Tha60210061PdhPrmInterruptManagerNew((AtModule)self);
    }

static AtInterruptManager MdlInterruptManagerObjectCreate(ThaModulePdh self)
    {
    return Tha60210061PdhMdlInterruptManagerNew((AtModule)self);
    }

static uint32 StartVersionSupportDe1LomfConsequentialAction(ThaModulePdh self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x3, 0x8, 0x1020);
    }

static void OverrideAtObject(AtModulePdh self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModulePdh self)
    {
    AtModule module = (AtModule)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideTha60210031ModulePdh(AtModulePdh self)
    {
    Tha60210031ModulePdh pdhModule = (Tha60210031ModulePdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210031ModulePdhMethods = mMethodsGet(pdhModule);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210031ModulePdhOverride, mMethodsGet(pdhModule), sizeof(m_Tha60210031ModulePdhOverride));

        mMethodOverride(m_Tha60210031ModulePdhOverride, HasLiu);
        mMethodOverride(m_Tha60210031ModulePdhOverride, StartVersionSupportMdlFilterMessageByType);
        mMethodOverride(m_Tha60210031ModulePdhOverride, MdlRxBufWordOffset);
        mMethodOverride(m_Tha60210031ModulePdhOverride, MdlRxBufBaseAddress);
        mMethodOverride(m_Tha60210031ModulePdhOverride, UpenRxmdlCfgtypeBase);
        mMethodOverride(m_Tha60210031ModulePdhOverride, MdlRxStickyBaseAddress);
        mMethodOverride(m_Tha60210031ModulePdhOverride, Ec1DiagLiuModeShouldEnable);
        mMethodOverride(m_Tha60210031ModulePdhOverride, MdlRxByteOffset);
        mMethodOverride(m_Tha60210031ModulePdhOverride, MdlRxPktOffset);
        mMethodOverride(m_Tha60210031ModulePdhOverride, MdlRxDropOffset);
        mMethodOverride(m_Tha60210031ModulePdhOverride, TxBufferBaseGet);
        mMethodOverride(m_Tha60210031ModulePdhOverride, TxBufferDwordOffset);
        mMethodOverride(m_Tha60210031ModulePdhOverride, SerialIdFromHwIdGet);
        }

    mMethodsSet(pdhModule, &m_Tha60210031ModulePdhOverride);
    }

static void OverrideAtModulePdh(AtModulePdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModulePdhMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePdhOverride, m_AtModulePdhMethods, sizeof(m_AtModulePdhOverride));

        mMethodOverride(m_AtModulePdhOverride, AllDe1sDelete);
        mMethodOverride(m_AtModulePdhOverride, AllDe1Create);
        mMethodOverride(m_AtModulePdhOverride, NumberOfDe3sGet);
        mMethodOverride(m_AtModulePdhOverride, NumberOfDe1sGet);
        mMethodOverride(m_AtModulePdhOverride, NumberOfDe3SerialLinesGet);
        mMethodOverride(m_AtModulePdhOverride, De3SerialCreate);
        mMethodOverride(m_AtModulePdhOverride, De1Create);
        mMethodOverride(m_AtModulePdhOverride, De3Create);
        mMethodOverride(m_AtModulePdhOverride, VcDe3Create);
        mMethodOverride(m_AtModulePdhOverride, PrmCrCompareEnable);
        mMethodOverride(m_AtModulePdhOverride, PrmCrCompareIsEnabled);
        mMethodOverride(m_AtModulePdhOverride, De3Get);
        }

    mMethodsSet(self, &m_AtModulePdhOverride);
    }

static void OverrideThaModulePdh(AtModulePdh self)
    {
    ThaModulePdh modulePdh = (ThaModulePdh)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePdhMethods = mMethodsGet(modulePdh);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdhOverride, m_ThaModulePdhMethods, sizeof(m_ThaModulePdhOverride));

        mMethodOverride(m_ThaModulePdhOverride, DiagLineBaseAddress);
        mMethodOverride(m_ThaModulePdhOverride, InbandLoopcodeIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, PdhChannelRegsShow);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportDe3FramedSatop);
        mMethodOverride(m_ThaModulePdhOverride, De1PrmOffset);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportPrm);
        mMethodOverride(m_ThaModulePdhOverride, MdlIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, PrmControllerObjectCreate);
        mMethodOverride(m_ThaModulePdhOverride, De1ChannelFromHwIdGet);
        mMethodOverride(m_ThaModulePdhOverride, De3ChannelFromHwIdGet);
        mMethodOverride(m_ThaModulePdhOverride, De3FramedSatopIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, De3AisSelectablePatternIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, De1LiuShouldReportLofWhenLos);
        mMethodOverride(m_ThaModulePdhOverride, De3LiuShouldReportLofWhenLos);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportDe1Ssm);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportDe3Ssm);
        mMethodOverride(m_ThaModulePdhOverride, PrmFcsBitOrderIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, De1InterruptEnable);
        mMethodOverride(m_ThaModulePdhOverride, NumStsInSlice);
        mMethodOverride(m_ThaModulePdhOverride, DebuggerObjectCreate);
        mMethodOverride(m_ThaModulePdhOverride, PrmInterruptManagerObjectCreate);
        mMethodOverride(m_ThaModulePdhOverride, MdlInterruptManagerObjectCreate);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportDe1LomfConsequentialAction);
        }

    mMethodsSet(modulePdh, &m_ThaModulePdhOverride);
    }

static void OverrideTha60210011ModulePdh(AtModulePdh self)
    {
    Tha60210011ModulePdh pdhModule = (Tha60210011ModulePdh)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModulePdhOverride, mMethodsGet(pdhModule), sizeof(m_Tha60210011ModulePdhOverride));

        mMethodOverride(m_Tha60210011ModulePdhOverride, PrmDefaultSet);
        }

    mMethodsSet(pdhModule, &m_Tha60210011ModulePdhOverride);
    }

static void Override(AtModulePdh self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    OverrideAtModulePdh(self);
    OverrideThaModulePdh(self);
    OverrideTha60210031ModulePdh(self);
    OverrideTha60210011ModulePdh(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061ModulePdh);
    }

static AtModulePdh ObjectInit(AtModulePdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012ModulePdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePdh Tha60210061ModulePdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

AtCpld Tha60210061ModulePdhCpld(AtModulePdh self)
    {
    if (self)
        return Cpld(self);
    return NULL;
    }

eBool Tha60210061ModulePdhChannelIsLiuChannel(AtPdhChannel pdhChannel)
    {
    if (AtPdhChannelParentChannelGet(pdhChannel))
        return cAtFalse;

    if (AtPdhChannelVcInternalGet(pdhChannel) == NULL)
        return cAtTrue;

    if (AtPdhChannelVcGet(pdhChannel) == NULL)
        return cAtTrue;

    return cAtFalse;
    }

static ThaBitMask Ds1BitMask(ThaModulePdh self)
    {
    static const uint8 cNumDs1InOneSts = 28;
    if (mThis(self)->ds1InUsedBitMask == NULL)
        mThis(self)->ds1InUsedBitMask = ThaBitMaskNew(cNumDs1InOneSts);
    return mThis(self)->ds1InUsedBitMask;
    }

static ThaBitMask E1BitMask(ThaModulePdh self)
    {
    static const uint8 cNumE1InOneSts = 21;
    if (mThis(self)->e1InUsedBitMask == NULL)
        mThis(self)->e1InUsedBitMask = ThaBitMaskNew(cNumE1InOneSts);
    return mThis(self)->e1InUsedBitMask;
    }

eAtRet Tha60210061ModulePdhLiuDe1FramerAllocate(ThaModulePdh self, AtPdhDe1 de1, eBool isE1)
    {
    AtSdhChannel vc1x;
    uint8 tu1xId, tug2Id;
    eAtSdhChannelType vc1xChannelType;
    uint32 de1FlatFreeId;
    ThaBitMask de1BitMask = isE1 ? E1BitMask(self) : Ds1BitMask(self);

    if (AtPdhChannelVcInternalGet((AtPdhChannel)de1))
        return cAtOk;

    de1FlatFreeId = ThaBitMaskFirstZeroBit(de1BitMask);
    if (de1FlatFreeId == cBit31_0)
        return cAtErrorRsrcNoAvail;

    if (isE1)
        {
        tug2Id = (uint8)(de1FlatFreeId / cNumTu12InTug2);
        tu1xId = (uint8)(de1FlatFreeId % cNumTu12InTug2);
        vc1xChannelType = cAtSdhChannelTypeVc12;
        }
    else
        {
        tug2Id = (uint8)(de1FlatFreeId / cNumTu11InTug2);
        tu1xId = (uint8)(de1FlatFreeId % cNumTu11InTug2);
        vc1xChannelType = cAtSdhChannelTypeVc11;
        }

    ThaBitMaskSetBit(de1BitMask, de1FlatFreeId);

    vc1x = (AtSdhChannel)Tha60210061SdhAbstractVc1xNew(0, vc1xChannelType, ModuleSdh(self));
    SdhChannelTug2IdSet(vc1x, tug2Id);
    SdhChannelTu1xIdSet(vc1x, tu1xId);
    SdhChannelSts1IdSet(vc1x, cStartStsIdForDe1LiuPort);

    AtPdhChannelVcSet((AtPdhChannel)de1, (AtSdhVc)vc1x);
    return cAtOk;
    }

eAtRet Tha60210061ModulePdhLiuDe1FramerDeallocate(ThaModulePdh self, AtPdhDe1 de1, eBool isE1)
    {
    ThaBitMask de1BitMask;
    AtSdhChannel vc = (AtSdhChannel)AtPdhChannelVcInternalGet((AtPdhChannel)de1);

    if (vc == NULL)
        return cAtOk;

    de1BitMask = isE1 ? E1BitMask(self) : Ds1BitMask(self);
    if (isE1)
        ThaBitMaskClearBit(de1BitMask, AtSdhChannelTug2Get(vc) * cNumTu12InTug2 + AtSdhChannelTu1xGet(vc));
    else
        ThaBitMaskClearBit(de1BitMask, AtSdhChannelTug2Get(vc) * cNumTu11InTug2 + AtSdhChannelTu1xGet(vc));

    AtObjectDelete((AtObject)vc);
    AtPdhChannelVcSet((AtPdhChannel)de1, NULL);

    return cAtOk;
    }

uint32 Tha60210061ModulePdhLiuBaseAddress(void)
    {
    return 0xB00000;
    }

eBool Tha60210061ModulePdhBpvCounterIsSupported(ThaModulePdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);

    return (hwVersion >= StartVersionSupportBpvCounter()) ? cAtTrue : cAtFalse;
    }

/* To always access de3 object as public API will be hidden when serial line is EC1 */
AtPdhDe3 Tha60210061PdhDe3Get(AtModulePdh self, uint32 de3Id)
    {
    if (self)
        return m_AtModulePdhMethods->De3Get(self, de3Id);
    return NULL;
    }

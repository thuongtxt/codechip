/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60210061PdhMdlInterruptManager.c
 *
 * Created Date: Sep 21, 2017
 *
 * Description : MDL interrupt manager implementation.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/pdh/mdl/ThaPdhMdlInterruptManagerInternal.h"
#include "../../../../default/pdh/ThaModulePdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061PdhMdlInterruptManager
    {
    tThaPdhMdlInterruptManager super;
    }tTha60210061PdhMdlInterruptManager;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtInterruptManagerMethods        m_AtInterruptManagerOverride;
static tThaInterruptManagerMethods       m_ThaInterruptManagerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(ThaInterruptManager self)
    {
    AtUnused(self);
    return 0x0380000UL;
    }

static eBool HasInterrupt(AtInterruptManager self, uint32 glbIntr, AtHal hal)
    {
    static const uint32 cAf6_global_interrupt_mask_enable_MdlIntEnable_Mask = cBit9;
    AtUnused(self);
    AtUnused(hal);
    return (glbIntr & cAf6_global_interrupt_mask_enable_MdlIntEnable_Mask) ? cAtTrue : cAtFalse;
    }

static uint32 NumStsInSlice(ThaInterruptManager self)
    {
    AtUnused(self);
    return 27;
    }

static void OverrideAtInterruptManager(AtInterruptManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtInterruptManagerOverride, mMethodsGet(self), sizeof(m_AtInterruptManagerOverride));

        mMethodOverride(m_AtInterruptManagerOverride, HasInterrupt);
        }

    mMethodsSet(self, &m_AtInterruptManagerOverride);
    }

static void OverrideThaInterruptManager(AtInterruptManager self)
    {
    ThaInterruptManager manager = (ThaInterruptManager)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInterruptManagerOverride, mMethodsGet(manager), sizeof(m_ThaInterruptManagerOverride));

        mMethodOverride(m_ThaInterruptManagerOverride, BaseAddress);
        mMethodOverride(m_ThaInterruptManagerOverride, NumStsInSlice);
        }

    mMethodsSet(manager, &m_ThaInterruptManagerOverride);
    }

static void Override(AtInterruptManager self)
    {
    OverrideAtInterruptManager(self);
    OverrideThaInterruptManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061PdhMdlInterruptManager);
    }

static AtInterruptManager ObjectInit(AtInterruptManager self, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhMdlInterruptManagerObjectInit(self, module) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInterruptManager Tha60210061PdhMdlInterruptManagerNew(AtModule module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInterruptManager newManager = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newManager == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newManager, module);
    }

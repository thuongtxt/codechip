/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60210061QgmiiLaneEthPort.h
 * 
 * Created Date: Dec 6, 2016
 *
 * Description : QGMII
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061QGMIILANEETHPORT_H_
#define _THA60210061QGMIILANEETHPORT_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEthPort Tha60210061QsgmiiLaneEthPortNew(uint8 portId, AtModuleEth module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061QGMIILANEETHPORT_H_ */


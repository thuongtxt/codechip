/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60210061SgmiiEthPort.h
 * 
 * Created Date: Dec 6, 2016
 *
 * Description : SGMII Port
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061SGMIIETHPORT_H_
#define _THA60210061SGMIIETHPORT_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPtchService.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint16 Tha60210061EthPortSW2HwCounterId(AtEthPort self, uint16 counterType);
eBool Tha60210061EthPort1GCounterIsSupported(AtChannel self, uint16 counterType);
AtPtchService Tha60210061SgmiiEthPassthroughPtchGet(AtEthPort self);
AtEthPort Tha60210061SgmiiEthPortByPassPort(AtEthPort self);
void Tha60210061SgmiiEthPortByPassPortCache(AtEthPort self, AtEthPort bypassPort);
AtPtchService Tha60210061SgmiiEthPassthroughPtchCreate(AtEthPort self);
eAtRet Tha60210061EthPort1GAllCountersReadToClear(AtChannel self, void* counters, eBool read2Clear);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061SGMIIETHPORT_H_ */


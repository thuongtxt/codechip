/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Ethernet
 * 
 * File        : Tha60210061GeBypassBufferReg.h
 * 
 * Created Date: Jan 4, 2017
 *
 * Description :
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061GEBYPASSBUFFERREG_H_
#define _THA60210061GEBYPASSBUFFERREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/*------------------------------------------------------------------------------
Reg Name   : Global Configure 0
Reg Addr   : 0x000
Reg Formula: 0x000
    Where  :
Reg Desc   :
Used to configure:
- Active MUX and eXAUI#0 Buffer
- MTU check

------------------------------------------------------------------------------*/
#define cAf6Reg_cfg0_pen_Base                                                                            0x000

/*--------------------------------------
BitField Name: MTU
BitField Type: R/W
BitField Desc: Maximum Transmit Unit for eXAUI#0 Buffer
BitField Bits: [31:04]
--------------------------------------*/
#define cAf6_cfg0_pen_MTU_Mask                                                                        cBit31_4
#define cAf6_cfg0_pen_MTU_Shift                                                                              4

/*--------------------------------------
BitField Name: Active
BitField Type: R/W
BitField Desc: Active Engine
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_cfg0_pen_Active_Mask                                                                        cBit0
#define cAf6_cfg0_pen_Active_Shift                                                                           0


#ifdef __cplusplus
}
#endif
#endif /* _THA60210061GEBYPASSBUFFERREG_H_ */


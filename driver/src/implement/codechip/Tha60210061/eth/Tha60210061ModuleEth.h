/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Ethernet
 * 
 * File        : Tha60210061ModuleEth.h
 * 
 * Created Date: Jun 20, 2016
 *
 * Description : 60210061 module Ethernet interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061MODULEETH_H_
#define _THA60210061MODULEETH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtEthPort.h"
#include "AtPtchService.h"
#include "../../../default/eth/ThaModuleEth.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define c10GPortId             0
#define cSgmiiPortStartId      1
#define cSgmiiPortEndId        4
#define cQsgmiiPortId          5
#define cQsgmiiLaneStartPortId 6
#define cMacEthCoreBase        0x40000
/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEthPort Tha60210061ModuleEthXfiPortGet(AtModuleEth self);
AtEthPort Tha60210061ModuleEthSgmiiPortGet(AtModuleEth self, uint32 serdesId);
AtEthPort Tha60210061ModuleEthQsgmiiPortGet(AtModuleEth self);
AtEthPort Tha60210061ModuleEthRxauiPortGet(AtModuleEth self);
AtPtchService Tha60210061ModuleEthGeBypassPtchServiceGet(AtModuleEth self, AtEthPort gePort);
AtSerdesController Tha60210061ModuleEthPartnerSerdes(AtModuleEth self, AtSerdesController serdes);

AtEthPort Tha60210061EthPort10GNew(uint8 portId, AtModuleEth module);
AtEthPort Tha60210061QsgmiiEthPortNew(uint8 portId, AtModuleEth module);
AtEthPort Tha60210061SgmiiEthPortNew(uint8 portId, AtModuleEth module);
AtPtchService Tha60210061EthPassthoughPtchNew(AtEthPort gePort);

eAtRet Tha60210061GePortBypassPortSet(AtEthPort port, AtEthPort bypassPort);
AtEthPort Tha60210061GePortBypassPortGet(AtEthPort port);
eAtRet Tha60210061GePortBypassEnable(AtEthPort gePort, eBool enable);
eBool Tha60210061GePortBypassIsEnabled(AtEthPort gePort);

eBool Tha60210061EthPassThroughIsSupported(AtModuleEth self);
eBool Tha60210061EthPassthroughXfiIsSupported(ThaModuleEth self);
eAtRet Tha60210061ModuleEthGeBypassBufferActivate(ThaModuleEth self, eBool enable);
uint32 Tha60210061StartVersionRemoveXfiDiagPrbsEngine(void);

AtPtchService Tha60210061CemPtchServiceNew(AtModuleEth ethModule);
AtPtchService Tha60210061ImsgEopPtchServiceNew(AtModuleEth ethModule);
eAtRet Tha60210061ModuleEthXfiEgressPtchHwSet(ThaModuleEth self, uint16 ptch, uint8 serviceType);
eAtRet Tha60210061ModuleEthXfiEgressPtchHwClear(ThaModuleEth self, uint16 ptch);
eAtRet Tha60210061ModuleEthCoreIsolationEnable(ThaModuleEth self, eBool enable);

eBool Tha60210061ModuleEthPortIs10G(AtEthPort port);
eBool Tha60210061ModuleEthPortIsQsgmii(AtEthPort port);
eBool Tha60210061ModuleEthPortIsSgmii(AtEthPort port);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061MODULEETH_H_ */


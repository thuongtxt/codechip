/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Ethernet
 *
 * File        : Tha60210061ModuleEth.c
 *
 * Created Date: Jun 18, 2016
 *
 * Description : 60210061 module Ethernet
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtMdio.h"
#include "../../../../generic/physical/AtSerdesManagerInternal.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/man/versionreader/ThaVersionReader.h"
#include "../../../default/man/ThaDevice.h"
#include "../../../default/man/ThaModuleClasses.h"
#include "../../Tha60210012/eth/Tha60210012ModuleEthInternal.h"
#include "../../Tha60210012/physical/Tha60210012PtchService.h"
#include "../physical/Tha60210061SerdesManager.h"
#include "../mpeg/Tha60210061ModuleMpeg.h"
#include "../pda/Tha60210061ModulePda.h"
#include "../prbs/Tha60210061ModulePrbs.h"
#include "Tha60210061ModuleEth.h"
#include "Tha60210061EthPassThroughReg.h"
#include "Tha60210061QgmiiLaneEthPort.h"
#include "Tha60210061SgmiiEthPort.h"
#include "Tha60210061GeBypassBufferReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6MacSwitchPortMask(portId) (cBit0 << (portId))
#define cGeBypassBaseAddress 0xE80000
#define cDisconnectPortId 1

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210061ModuleEth*)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061ModuleEth
    {
    tTha60210012ModuleEth super;

    /* Private data */
    AtMdio sgmiiMdio;
    AtDrp sgmiiDrp;
    AtDrp xfiDrp;
    AtDrp qsgmiiDrp;
    }tTha60210061ModuleEth;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtModuleMethods     m_AtModuleOverride;
static tAtModuleEthMethods  m_AtModuleEthOverride;
static tThaModuleEthMethods m_ThaModuleEthOverride;
static tTha60210012ModuleEthMethods m_Tha60210012ModuleEthOverride;

/* Save super implementation */
static const tAtObjectMethods     *m_AtObjectMethods = NULL;
static const tAtModuleMethods     *m_AtModuleMethods = NULL;
static const tAtModuleEthMethods  *m_AtModuleEthMethods = NULL;
static const tThaModuleEthMethods *m_ThaModuleEthMethods = NULL;
static const tTha60210012ModuleEthMethods *m_Tha60210012ModuleEthMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaEthSerdesManager SerdesManagerCreate(ThaModuleEth self)
    {
    AtUnused(self);
    /* This product uses SERDES manager of device */
    return NULL;
    }

static AtSerdesManager SerdesManager(AtModuleEth self)
    {
    return AtDeviceSerdesManagerGet(AtModuleDeviceGet((AtModule)self));
    }

static uint32 SerdesIdMax(AtModuleEth self)
    {
    return AtSerdesManagerNumSerdesControllers(SerdesManager(self));
    }

static uint32 XfiDrpBaseAddress(AtModuleEth self)
    {
    AtUnused(self);
    return 0xF43000UL;
    }

static uint32 QsgmiiDrpBaseAddress(AtModuleEth self)
    {
    AtUnused(self);
    return 0xF57000UL;
    }

static uint32 SgmiiDrpBaseAddress(AtModuleEth self)
    {
    AtUnused(self);
    return 0xF41000UL;
    }

static uint32 SgmiiDrpPortOffset(AtDrp drp, uint32 selectedPortId)
    {
    AtUnused(drp);
    if (selectedPortId <= cTha60210061SgmiiSerdesId3)
        return (selectedPortId - cTha60210061SgmiiSerdesId0) * 0x400UL;
    return cBit31_0;
    }

static uint32 XfiDrpPortOffset(AtDrp drp, uint32 selectedPortId)
    {
    AtUnused(drp);
    if ((selectedPortId <= cTha60210061XfiSerdesId1) && (selectedPortId >= cTha60210061XfiSerdesId0))
        return (selectedPortId - cTha60210061XfiSerdesId0) * 0x1000UL;
    return cBit31_0;
    }

static uint32 QsgmiiDrpPortOffset(AtDrp drp, uint32 selectedPortId)
    {
    AtUnused(drp);
    if ((selectedPortId <= cTha60210061QsgmiiSerdesId1) && (selectedPortId >= cTha60210061QsgmiiSerdesId0))
        return (selectedPortId - cTha60210061QsgmiiSerdesId0) * 0x400UL;
    return cBit31_0;
    }

static tAtDrpAddressCalculator *SerdesXfiDrpAddressCalculator(void)
    {
    static tAtDrpAddressCalculator xfiDrpAddrCalculator;
    static tAtDrpAddressCalculator *pXfiDrpAddrCalculator = NULL;

    if (pXfiDrpAddrCalculator)
        return pXfiDrpAddrCalculator;

    AtOsalMemInit(&xfiDrpAddrCalculator, 0, sizeof(tAtDrpAddressCalculator));
    xfiDrpAddrCalculator.PortOffset = XfiDrpPortOffset;
    pXfiDrpAddrCalculator = &xfiDrpAddrCalculator;

    return pXfiDrpAddrCalculator;
    }

static tAtDrpAddressCalculator *SerdesQsgmiiDrpAddressCalculator(void)
    {
    static tAtDrpAddressCalculator qsgmiiDrpAddrCalculator;
    static tAtDrpAddressCalculator *pQsgmiiDrpAddrCalculator = NULL;

    if (pQsgmiiDrpAddrCalculator)
        return pQsgmiiDrpAddrCalculator;

    AtOsalMemInit(&qsgmiiDrpAddrCalculator, 0, sizeof(tAtDrpAddressCalculator));
    qsgmiiDrpAddrCalculator.PortOffset = QsgmiiDrpPortOffset;
    pQsgmiiDrpAddrCalculator = &qsgmiiDrpAddrCalculator;

    return pQsgmiiDrpAddrCalculator;
    }

static tAtDrpAddressCalculator *SerdesSgmiiDrpAddressCalculator(void)
    {
    static tAtDrpAddressCalculator sgmiiDrpAddrCalculator;
    static tAtDrpAddressCalculator *pSgmiiDrpAddrCalculator = NULL;

    if (pSgmiiDrpAddrCalculator)
        return pSgmiiDrpAddrCalculator;

    AtOsalMemInit(&sgmiiDrpAddrCalculator, 0, sizeof(tAtDrpAddressCalculator));
    sgmiiDrpAddrCalculator.PortOffset = SgmiiDrpPortOffset;
    pSgmiiDrpAddrCalculator = &sgmiiDrpAddrCalculator;

    return pSgmiiDrpAddrCalculator;
    }

static AtDrp DrpCreate(AtModuleEth self, uint32 baseAddress)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    AtHal hal = AtDeviceIpCoreHalGet(device, AtModuleDefaultCoreGet((AtModule)self));
    AtDrp newDrp = AtDrpNew(baseAddress, hal);
    if (!newDrp)
        return newDrp;

    AtDrpNumberOfSerdesControllersSet(newDrp, SerdesIdMax(self));
    AtDrpNeedSelectPortSet(newDrp, cAtFalse);
    return newDrp;
    }

static AtDrp SerdesXfiDrpCreate(AtModuleEth self)
    {
    AtDrp newDrp = DrpCreate(self, XfiDrpBaseAddress(self));
    if (!newDrp)
        return newDrp;

    AtDrpAddressCalculatorSet(newDrp, SerdesXfiDrpAddressCalculator());
    return newDrp;
    }

static AtDrp SerdesQsgmiiDrpCreate(AtModuleEth self)
    {
    AtDrp newDrp = DrpCreate(self, QsgmiiDrpBaseAddress(self));
    if (!newDrp)
        return newDrp;

    AtDrpAddressCalculatorSet(newDrp, SerdesQsgmiiDrpAddressCalculator());
    return newDrp;
    }

static AtDrp SerdesSgmiiDrpCreate(AtModuleEth self)
    {
    AtDrp newDrp = DrpCreate(self, SgmiiDrpBaseAddress(self));
    if (!newDrp)
        return newDrp;

    AtDrpAddressCalculatorSet(newDrp, SerdesSgmiiDrpAddressCalculator());
    return newDrp;
    }

static AtDrp SerdesDrp(AtModuleEth self, uint32 serdesId)
    {
    if (serdesId <= cTha60210061SgmiiSerdesId3)
        {
        if (mThis(self)->sgmiiDrp == NULL)
            mThis(self)->sgmiiDrp = SerdesSgmiiDrpCreate(self);
        return mThis(self)->sgmiiDrp;
        }

    if ((serdesId <= cTha60210061QsgmiiSerdesId1)&& (serdesId >= cTha60210061QsgmiiSerdesId0))
        {
        if (mThis(self)->qsgmiiDrp == NULL)
            mThis(self)->qsgmiiDrp = SerdesQsgmiiDrpCreate(self);
        return mThis(self)->qsgmiiDrp;
        }

    if ((serdesId <= cTha60210061XfiSerdesId1)&& (serdesId >= cTha60210061XfiSerdesId0))
        {
        if (mThis(self)->xfiDrp == NULL)
            mThis(self)->xfiDrp = SerdesXfiDrpCreate(self);
        return mThis(self)->xfiDrp;
        }

    return NULL;
    }

static eBool SerdesIsEthernet(AtSerdesController serdes)
    {
    eAtSerdesMode mode = AtSerdesControllerModeGet(serdes);
    if ((mode == cAtSerdesModeEth10G) || (mode == cAtSerdesModeEth1G) || (mode == cAtSerdesModeGe))
        return cAtTrue;

    return cAtFalse;
    }

static AtSerdesController SerdesController(AtModuleEth self, uint32 serdesId)
    {
    AtSerdesManager serdesManager = SerdesManager(self);
    AtSerdesController serdes = AtSerdesManagerSerdesControllerGet(serdesManager, serdesId);
    if (serdes == NULL)
        return NULL;

    /* SGMII serdes mode is unknown when it has been created, but an ethernet port is associated, so set it to 1G mode
     * if application does not set mode for it */
    if ((AtSerdesControllerModeGet(serdes) == cAtSerdesModeUnknown) &&
        Tha60210061SerdesIsSgmii(serdes))
        AtSerdesControllerModeSet(serdes, cAtSerdesModeEth1G);

    if (!SerdesIsEthernet(serdes))
        return NULL;

    return serdes;
    }

static uint8 MaxPortsGet(AtModuleEth self)
    {
    /* Module Ethernet manages 6 ports as below:
     *
     * 0: 10G MAC, working in XAUI interface or XFI interface
     * 1..4: 4x1G MAC, working in SGMII interface
     * 5: Working in QSGMII interface
     */
    AtUnused(self);
    return 6;
    }

static uint32 SerdesIdSwToHw(ThaModuleEth self, uint32 serdesId)
    {
    return Tha60210061SerdesManagerSerdesIdSwToHw(SerdesManager((AtModuleEth)self), serdesId);
    }

static AtMdio SgmiiSerdesMdioCreate(AtModuleEth self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    AtHal hal = AtDeviceIpCoreHalGet(device, AtModuleDefaultCoreGet((AtModule)self));
    AtMdio mdio = AtMdioSgmiiNew(0xF46000, hal);

    AtMdioSimulateEnable(mdio, AtDeviceIsSimulated(device));

    if (AtDeviceTestbenchIsEnabled(device))
        AtMdioSgmiiTimeoutMsSet(mdio, AtDeviceTestbenchDefaultTimeoutMs());

    return mdio;
    }

static AtMdio SerdesMdio(AtModuleEth self, AtSerdesController serdes)
    {
    if (!Tha60210061SerdesIsSgmii(serdes))
        return m_AtModuleEthMethods->SerdesMdio(self, serdes);

    if (mThis(self)->sgmiiMdio == NULL)
        mThis(self)->sgmiiMdio = SgmiiSerdesMdioCreate(self);

    AtMdioPortSelect(mThis(self)->sgmiiMdio, AtSerdesControllerHwIdGet(serdes));
    return mThis(self)->sgmiiMdio;
    }

static void AllEthernetPassThroughPathsRelease(AtModuleEth self)
    {
    uint8 gePortId;
    for (gePortId = cSgmiiPortStartId; gePortId <= cSgmiiPortEndId; gePortId++)
        {
        AtEthPort gePort = AtModuleEthPortGet(self, gePortId);
        Tha60210061GePortBypassPortSet(gePort, NULL);
        }
    }

static void DeleteDrp(AtDrp *drp)
    {
    AtDrpDelete(*drp);
    *drp = NULL;
    }

static void Delete(AtObject self)
    {
    AtMdioDelete(mThis(self)->sgmiiMdio);
    mThis(self)->sgmiiMdio = NULL;

    DeleteDrp(&mThis(self)->xfiDrp);
    DeleteDrp(&mThis(self)->qsgmiiDrp);
    DeleteDrp(&mThis(self)->sgmiiDrp);
    AllEthernetPassThroughPathsRelease((AtModuleEth)self);

    /* Super deletion */
    m_AtObjectMethods->Delete(self);
    }

static uint32 EthPortIdToSerdesId(ThaModuleEth self, uint32 ethPortId)
    {
    AtUnused(self);
    if (ethPortId == c10GPortId)
        return cTha60210061XfiSerdesId0;

    if (ethPortId == cQsgmiiPortId)
        return cTha60210061QsgmiiSerdesId0;

    if (ethPortId >= cSgmiiPortStartId)
        return cTha60210061SgmiiSerdesId0 + (ethPortId - cSgmiiPortStartId);

    return cInvalidUint32;
    }

static AtEthPort PortCreate(AtModuleEth self, uint8 portId)
    {
    if (portId == c10GPortId)
        return Tha60210061EthPort10GNew(portId, self);

    if (portId == cQsgmiiPortId)
        return Tha60210061QsgmiiEthPortNew(portId, self);

    return Tha60210061SgmiiEthPortNew(portId, self);
    }

static eAtRet Setup(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Setup(self);
    if (ret != cAtOk)
        return ret;

    if (AtDeviceSerdesManagerSetup(AtModuleDeviceGet(self)) == NULL)
        return cAtErrorRsrcNoAvail;

    return cAtOk;
    }

static uint16 MaxFlowsGet(AtModuleEth self)
    {
    AtUnused(self);
    return 2040;
    }

static void EXauInit(Tha60210012ModuleEth self)
    {
    AtUnused(self);
    /* Nothing to do */
    }

static AtSerdesController DefaultBridgeSerdesOfPort(AtModuleEth self, AtEthPort port)
    {
    return Tha60210061ModuleEthPartnerSerdes(self, AtEthPortSerdesController(port));
    }

static eAtRet Port10GSerdesDefaultSetup(Tha60210012ModuleEth self)
    {
    eAtRet ret = cAtOk;
    AtModuleEth moduleEth = (AtModuleEth)self;

    AtEthPort port = AtModuleEthPortGet(moduleEth, c10GPortId);
    AtSerdesController serdes = AtEthPortSerdesController(port);

    ret |= AtEthPortTxSerdesSet(port, serdes);
    ret |= AtSerdesControllerPowerDown(serdes, cAtFalse);

    serdes = DefaultBridgeSerdesOfPort(moduleEth, port);
    ret |= AtEthPortTxSerdesBridge(port, serdes);
    ret |= AtSerdesControllerPowerDown(serdes, cAtFalse);

    return ret;
    }

static eAtRet PortQsgmiiSerdesDefaultSetup(Tha60210012ModuleEth self)
    {
    eAtRet ret = cAtOk;
    AtModuleEth moduleEth = (AtModuleEth)self;

    AtEthPort port = AtModuleEthPortGet(moduleEth, cQsgmiiPortId);
    AtSerdesController serdes = AtEthPortSerdesController(port);

    ret |= AtEthPortTxSerdesSet(port, serdes);

    serdes = DefaultBridgeSerdesOfPort(moduleEth, port);
    ret |= AtEthPortTxSerdesBridge(port, serdes);

    return ret;
    }

static eAtRet SerdesDefaultSetup(Tha60210012ModuleEth self)
    {
    eAtRet ret = Port10GSerdesDefaultSetup(self);
    ret |= PortQsgmiiSerdesDefaultSetup(self);

    return ret;
    }

static eAtRet AllPtchServiceCreate(Tha60210012ModuleEth self)
    {
    AtModuleEth moduleEth = (AtModuleEth)self;
    self->cemPtch     = Tha60210061CemPtchServiceNew(moduleEth);
    self->imsgEoPPtch = Tha60210061ImsgEopPtchServiceNew(moduleEth);

    return cAtOk;
    }

static eAtRet AllPtchServiceDelete(Tha60210012ModuleEth self)
    {
    AtObjectDelete((AtObject)self->cemPtch);
    AtObjectDelete((AtObject)self->imsgEoPPtch);

    return cAtOk;
    }

static uint32 StartVersionSupportNewMaxMinPacketSize(Tha60210012ModuleEth self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 StartVersionSupportEthPassThrough(AtModule self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x5, 0x1033);
    }

static eBool PortIs10G(AtEthPort port)
    {
    if (port == NULL)
        return cAtFalse;

    if (port == Tha60210061ModuleEthXfiPortGet((AtModuleEth)AtChannelModuleGet((AtChannel)port)))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 MacBaseAddress(ThaModuleEth self, AtEthPort port)
    {
    if (PortIs10G(port))
        {
        if (!Tha60210061EthPassthroughXfiIsSupported(self))
            return m_ThaModuleEthMethods->MacBaseAddress(self, port);

        return 0x41000;
        }

    /* SGMII Port and QSGMII port */
    if (!Tha60210061EthPassThroughIsSupported((AtModuleEth)self))
        return m_ThaModuleEthMethods->MacBaseAddress(self, port);

    return 0x41000;
    }

static uint32 StartVersionSupportNewPMCCounterOffset(Tha60210012ModuleEth self)
    {
    /* HW support this feature from the beginning */
    AtUnused(self);
    return 0;
    }

static void EthPassThroughPrbsDisable(AtModuleEth self)
    {
    uint32 regAddress;

    /* Generate */
    regAddress = cAf6Reg_upen_ethpgen_Base + cMacEthCoreBase;
    mModuleHwWrite(self, regAddress, 0);

    /* Monitor */
    regAddress = cAf6Reg_upen_ethpmon_Base + cMacEthCoreBase;
    mModuleHwWrite(self, regAddress, 0);
    }

static eAtRet XfiPtchInit(AtModuleEth self)
    {
    const uint8 cPtchDisable = 6;
    uint32 regAddress = cAf6Reg_upen_port_rx_Base + cMacEthCoreBase;
    uint32 regValues = 0;

    mRegFieldSet(regValues, cAf6_upen_port_rx_Out_En_, 1);
    mRegFieldSet(regValues, cAf6_upen_port_rx_Ser_Typ_, cPtchDisable);
    mModuleHwWrite(self, regAddress, regValues);

    return cAtOk;
    }

static eAtRet EthPassthroughInit(AtModuleEth self)
    {
    if (Tha60210061EthPassThroughIsSupported(self))
        {
        EthPassThroughPrbsDisable(self);
        AllEthernetPassThroughPathsRelease(self);
        }

    if (Tha60210061EthPassthroughXfiIsSupported((ThaModuleEth)self))
        XfiPtchInit(self);

    return cAtOk;
    }

static eAtRet Init(AtModule self)
    {
    AtModuleEth ethModule = (AtModuleEth)self;
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    ret = AtSerdesManagerInit(SerdesManager(ethModule));
    if (ret != cAtOk)
        return ret;

    return EthPassthroughInit(ethModule);
    }

static eAtRet SerdesManagerAsyncInit(AtModuleEth self)
    {
    return AtSerdesManagerAsyncInit(SerdesManager(self));
    }

static tAtAsyncOperationFunc* AllAsyncOperation(uint32 *numOps)
    {
    static tAtAsyncOperationFunc functions[] = {(tAtAsyncOperationFunc)SerdesManagerAsyncInit,
                                                (tAtAsyncOperationFunc)EthPassthroughInit};
    if (numOps) *numOps = mCount(functions);
    return functions;
    }

static uint32 NumAsyncOperation(Tha60210012ModuleEth self)
    {
    uint32 numOp;
    AtUnused(self);

    AllAsyncOperation(&numOp);
    return m_Tha60210012ModuleEthMethods->NumAsyncOperation(self) + numOp;
    }

static tAtAsyncOperationFunc AsyncOperationGet(Tha60210012ModuleEth self, uint32 state)
    {
    tAtAsyncOperationFunc func = m_Tha60210012ModuleEthMethods->AsyncOperationGet(self, state);
    uint32 numSuperOps;
    if (func)
        return func;

    if (state >= NumAsyncOperation(self))
        return NULL;

    numSuperOps = m_Tha60210012ModuleEthMethods->NumAsyncOperation(self);
    return AllAsyncOperation(NULL)[state - numSuperOps];
    }

static eBool PortIsGePort(AtEthPort port)
    {
    uint32 portId = AtChannelIdGet((AtChannel)port);
    if ((portId >= cSgmiiPortStartId) && (portId <= cSgmiiPortEndId))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet GePortConnectBypassPort(AtEthPort port, AtEthPort bypassPort)
    {
    uint32 gePortId = AtChannelHwIdGet((AtChannel)port);
    uint32 regAddress = cAf6Reg_upen_ethswc_Base + cMacEthCoreBase + gePortId;
    uint32 regValues = 0;

    if (!PortIs10G(bypassPort))
        {
        uint32 bypassPortId = AtChannelHwIdGet((AtChannel)bypassPort);
        mRegFieldSet(regValues, cAf6_upen_ethswc_Mac_Map_, cAf6MacSwitchPortMask(bypassPortId));
        }

    mChannelHwWrite(port, regAddress, regValues, cAtModuleEth);
    return cAtOk;
    }

static eAtRet GePortDisconnectBypassPort(AtEthPort port, AtEthPort bypassPort)
    {
    uint32 gePortId = AtChannelHwIdGet((AtChannel)port);
    uint32 regAddress = cAf6Reg_upen_ethswc_Base + cMacEthCoreBase + gePortId;
    uint32 regValues = mChannelHwRead(port, regAddress, cAtModuleEth);
    AtUnused(bypassPort);

    mRegFieldSet(regValues, cAf6_upen_ethswc_Mac_Map_, cAf6MacSwitchPortMask(cDisconnectPortId));
    mChannelHwWrite(port, regAddress, regValues, cAtModuleEth);

    return cAtOk;
    }

static eAtRet BypassPortConnectGePort(AtEthPort bypassPort, AtEthPort port)
    {
    if (!PortIs10G(bypassPort))
        {
        uint32 bypassPortId = AtChannelHwIdGet((AtChannel)bypassPort);
        uint32 gePortId = AtChannelHwIdGet((AtChannel)port);
        uint32 regAddress = cAf6Reg_upen_ethswc_Base + cMacEthCoreBase + bypassPortId;
        uint32 regValues = mChannelHwRead(port, regAddress, cAtModuleEth);

        mRegFieldSet(regValues, cAf6_upen_ethswc_Mac_Map_, cAf6MacSwitchPortMask(gePortId));
        mChannelHwWrite(port, regAddress, regValues, cAtModuleEth);
        }
    else
        Tha60210061SgmiiEthPassthroughPtchCreate(port);

    return cAtOk;
    }

static eAtRet BypassPortDisconnectGePort(AtEthPort bypassPort, AtEthPort port)
    {
    if (!PortIs10G(bypassPort))
        {
        uint32 bypassPortId = AtChannelHwIdGet((AtChannel)bypassPort);
        uint32 regAddress = cAf6Reg_upen_ethswc_Base + cMacEthCoreBase + bypassPortId;
        uint32 regValues = mChannelHwRead(port, regAddress, cAtModuleEth);

        mRegFieldSet(regValues, cAf6_upen_ethswc_Mac_Map_, cAf6MacSwitchPortMask(cDisconnectPortId));
        mChannelHwWrite(port, regAddress, regValues, cAtModuleEth);
        }

    return cAtOk;
    }

static uint32 StartVersionSupportEthPassThroughXfi(void)
    {
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x6, 0x0);
    }

static ThaModuleMpeg ModuleMpeg(AtEthPort gePort)
    {
    return (ThaModuleMpeg)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)gePort), cThaModuleMpeg);
    }

static ThaModulePda ModulePda(AtEthPort gePort)
    {
    return (ThaModulePda)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)gePort), cThaModulePda);
    }

static eAtRet XfiPtchEnable(Tha60210012ModuleEth self, eBool enable)
    {
    uint32 regAddress, regValues = 0;
    const uint8 cPtchCheckAndRemove = 0x2;

    regAddress = cAf6Reg_upen_ethswc_Base + cMacEthCoreBase;
    mRegFieldSet(regValues, cAf6_upen_ethswc_Ptch_Opt_, (enable) ? cPtchCheckAndRemove : 0);
    mModuleHwWrite(self, regAddress, regValues);

    return cAtOk;
    }

static eAtRet PtchLookupEnable(Tha60210012ModuleEth self, eBool enable)
    {
    eAtRet ret = m_Tha60210012ModuleEthMethods->PtchLookupEnable(self, enable);
    if ((ret != cAtOk) || !Tha60210061EthPassthroughXfiIsSupported((ThaModuleEth)self))
        return ret;

    return XfiPtchEnable(self, enable);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210061ModuleEth *object = (tTha60210061ModuleEth *)self;
    AtSerdesController serdes = AtSerdesManagerSerdesControllerGet(SerdesManager((AtModuleEth)object), 0);

    m_AtObjectMethods->Serialize(self, encoder);

    AtModuleEthSerdesMdio((AtModuleEth)object, serdes);
    AtCoderEncodeString(encoder, AtMdioToString(object->sgmiiMdio), "sgmiiMdio");
    AtCoderEncodeString(encoder, AtDrpToString(object->sgmiiDrp), "sgmiiDrp");
    AtCoderEncodeString(encoder, AtDrpToString(object->xfiDrp), "xfiDrp");
    AtCoderEncodeString(encoder, AtDrpToString(object->qsgmiiDrp), "qsgmiiDrp");
    }

static eBool ExternalPmcCounterIsSupported(Tha60210012ModuleEth self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideAtObject(AtModuleEth self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModuleEth self)
    {
    AtModule module = (AtModule)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Setup);
        mMethodOverride(m_AtModuleOverride, Init);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideThaModuleEth(AtModuleEth self)
    {
    ThaModuleEth ethModule = (ThaModuleEth)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleEthMethods = mMethodsGet(ethModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleEthOverride, mMethodsGet(ethModule), sizeof(m_ThaModuleEthOverride));

        mMethodOverride(m_ThaModuleEthOverride, SerdesManagerCreate);
        mMethodOverride(m_ThaModuleEthOverride, SerdesIdSwToHw);
        mMethodOverride(m_ThaModuleEthOverride, EthPortIdToSerdesId);
        mMethodOverride(m_ThaModuleEthOverride, MacBaseAddress);
        }

    mMethodsSet(ethModule, &m_ThaModuleEthOverride);
    }

static void OverrideAtModuleEth(AtModuleEth self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleEthMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleEthOverride, mMethodsGet(self), sizeof(m_AtModuleEthOverride));

        mMethodOverride(m_AtModuleEthOverride, SerdesController);
        mMethodOverride(m_AtModuleEthOverride, MaxPortsGet);
        mMethodOverride(m_AtModuleEthOverride, PortCreate);
        mMethodOverride(m_AtModuleEthOverride, SerdesMdio);
        mMethodOverride(m_AtModuleEthOverride, MaxFlowsGet);
        mMethodOverride(m_AtModuleEthOverride, SerdesDrp);
        }

    mMethodsSet(self, &m_AtModuleEthOverride);
    }

static void OverrideTha60210012ModuleEth(AtModuleEth self)
    {
    Tha60210012ModuleEth ethModule = (Tha60210012ModuleEth)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210012ModuleEthMethods = mMethodsGet(ethModule);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210012ModuleEthOverride, m_Tha60210012ModuleEthMethods, sizeof(m_Tha60210012ModuleEthOverride));

        mMethodOverride(m_Tha60210012ModuleEthOverride, EXauInit);
        mMethodOverride(m_Tha60210012ModuleEthOverride, SerdesDefaultSetup);
        mMethodOverride(m_Tha60210012ModuleEthOverride, AllPtchServiceCreate);
        mMethodOverride(m_Tha60210012ModuleEthOverride, AllPtchServiceDelete);
        mMethodOverride(m_Tha60210012ModuleEthOverride, StartVersionSupportNewMaxMinPacketSize);
        mMethodOverride(m_Tha60210012ModuleEthOverride, StartVersionSupportNewPMCCounterOffset);
        mMethodOverride(m_Tha60210012ModuleEthOverride, AsyncOperationGet);
        mMethodOverride(m_Tha60210012ModuleEthOverride, NumAsyncOperation);
        mMethodOverride(m_Tha60210012ModuleEthOverride, PtchLookupEnable);
        mMethodOverride(m_Tha60210012ModuleEthOverride, ExternalPmcCounterIsSupported);
        }

    mMethodsSet(ethModule, &m_Tha60210012ModuleEthOverride);
    }

static void Override(AtModuleEth self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    OverrideAtModuleEth(self);
    OverrideThaModuleEth(self);
    OverrideTha60210012ModuleEth(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061ModuleEth);
    }

static AtModuleEth ObjectInit(AtModuleEth self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012ModuleEthObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleEth Tha60210061ModuleEthNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEth newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

AtEthPort Tha60210061ModuleEthXfiPortGet(AtModuleEth self)
    {
    return AtModuleEthPortGet(self, c10GPortId);
    }

AtEthPort Tha60210061ModuleEthSgmiiPortGet(AtModuleEth self, uint32 serdesId)
    {
    return AtModuleEthPortGet(self, (uint8)(serdesId + cSgmiiPortStartId));
    }

AtEthPort Tha60210061ModuleEthQsgmiiPortGet(AtModuleEth self)
    {
    return AtModuleEthPortGet(self, cQsgmiiPortId);
    }

AtEthPort Tha60210061ModuleEthRxauiPortGet(AtModuleEth self)
    {
    return AtModuleEthPortGet(self, c10GPortId);
    }

AtPtchService Tha60210061ModuleEthGeBypassPtchServiceGet(AtModuleEth self, AtEthPort gePort)
    {
    AtUnused(self);
    if (!PortIsGePort(gePort))
        return NULL;

    if (!PortIs10G(Tha60210061GePortBypassPortGet(gePort)))
        return NULL;

    return Tha60210061SgmiiEthPassthroughPtchGet(gePort);
    }

AtSerdesController Tha60210061ModuleEthPartnerSerdes(AtModuleEth self, AtSerdesController serdes)
    {
    uint32 serdesId = AtSerdesControllerIdGet(serdes);

    if (serdesId == cTha60210061XfiSerdesId0)
        return AtModuleEthSerdesController(self, cTha60210061XfiSerdesId1);

    if (serdesId == cTha60210061XfiSerdesId1)
        return AtModuleEthSerdesController(self, cTha60210061XfiSerdesId0);

    if (serdesId == cTha60210061RxauiSerdesId0)
        return AtModuleEthSerdesController(self, cTha60210061RxauiSerdesId1);

    if (serdesId == cTha60210061RxauiSerdesId1)
        return AtModuleEthSerdesController(self, cTha60210061RxauiSerdesId0);

    if (serdesId == cTha60210061QsgmiiSerdesId0)
        return AtModuleEthSerdesController(self, cTha60210061QsgmiiSerdesId1);

    if (serdesId == cTha60210061QsgmiiSerdesId1)
        return AtModuleEthSerdesController(self, cTha60210061QsgmiiSerdesId0);

    return NULL;
    }

eAtRet Tha60210061GePortBypassPortSet(AtEthPort port, AtEthPort bypassPort)
    {
    eAtRet ret = cAtOk;
    ThaModuleEth ethModule = (ThaModuleEth)AtChannelModuleGet((AtChannel)port);
    AtEthPort currentBypassPort;
    AtPrbsEngine prbsEngine;
    eAtPrbsSide genSide = cAtPrbsSideTdm;
    eAtPrbsSide monSide = cAtPrbsSideTdm;
    eBool prbsEnabled = cAtFalse;

    if (!PortIsGePort(port))
        return cAtErrorInvlParm;

    currentBypassPort = Tha60210061GePortBypassPortGet(port);
    if (bypassPort == currentBypassPort)
        return cAtOk;

    if (PortIs10G(bypassPort) && !Tha60210061EthPassthroughXfiIsSupported(ethModule))
        return cAtErrorModeNotSupport;

    /* If PRBS engine gen/mon from PSN, switch to gen/mon from TDM */
    prbsEngine = AtChannelPrbsEngineGet((AtChannel)port);
    if (prbsEngine)
        {
        genSide = AtPrbsEngineGeneratingSideGet(prbsEngine);
        monSide = AtPrbsEngineMonitoringSideGet(prbsEngine);
        if (genSide == cAtPrbsSidePsn) AtPrbsEngineGeneratingSideSet(prbsEngine, cAtPrbsSideTdm);
        if (monSide == cAtPrbsSidePsn) AtPrbsEngineMonitoringSideSet(prbsEngine, cAtPrbsSideTdm);
        prbsEnabled = AtPrbsEngineIsEnabled(prbsEngine);
        if (prbsEnabled) AtPrbsEngineEnable(prbsEngine, cAtFalse);
        }

    if (currentBypassPort)
        {
        AtPtchService ptch;

        ret  = GePortDisconnectBypassPort(port, currentBypassPort);
        ret |= BypassPortDisconnectGePort(currentBypassPort, port);
        ptch = Tha60210061SgmiiEthPassthroughPtchGet(port);
        Tha60210012PtchServiceEgressPtchHwClear(ptch);
        }

    if (bypassPort)
        {
        ret  = GePortConnectBypassPort(port, bypassPort);
        ret |= BypassPortConnectGePort(bypassPort, port);
        }

    if (ret != cAtOk)
        return ret;

    Tha60210061SgmiiEthPortByPassPortCache(port, bypassPort);

    /* Restore PRBS engine gen/mon side */
    if (prbsEngine)
        {
        if (prbsEnabled) AtPrbsEngineEnable(prbsEngine, cAtTrue);
        if (bypassPort)
            {
            if (genSide == cAtPrbsSidePsn) AtPrbsEngineGeneratingSideSet(prbsEngine, cAtPrbsSidePsn);
            if (monSide == cAtPrbsSidePsn) AtPrbsEngineMonitoringSideSet(prbsEngine, cAtPrbsSidePsn);
            }
        }

    return cAtOk;
    }

AtEthPort Tha60210061GePortBypassPortGet(AtEthPort port)
    {
    if (!PortIsGePort(port))
        return NULL;

    return Tha60210061SgmiiEthPortByPassPort(port);
    }

eAtRet Tha60210061GePortBypassEnable(AtEthPort gePort, eBool enable)
    {
    eAtRet ret;
    AtEthPort bypassPort = Tha60210061GePortBypassPortGet(gePort);

    if ((bypassPort == NULL) && (enable))
        return cAtErrorInvalidOperation;

    ret  = (enable) ? GePortConnectBypassPort(gePort, bypassPort) : GePortDisconnectBypassPort(gePort, bypassPort);
    ret |= (enable) ? BypassPortConnectGePort(bypassPort, gePort) : BypassPortDisconnectGePort(bypassPort, gePort);
    ret |= Tha60210061ModulePdaGeBypassEnable(ModulePda(gePort), gePort, enable);
    ret |= Tha60210061ModuleMpegGeBypassEnable(ModuleMpeg(gePort), gePort, enable);

    return ret;
    }

eBool Tha60210061GePortBypassIsEnabled(AtEthPort gePort)
    {
    if (Tha60210061GePortBypassPortGet(gePort) == NULL)
        return cAtFalse;

    return Tha60210061ModuleMpegGeBypassIsEnabled(ModuleMpeg(gePort), gePort);
    }

eBool Tha60210061EthPassThroughIsSupported(AtModuleEth self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);

    if (AtDeviceIsSimulated(device))
        return cAtTrue;

    return (hwVersion >= StartVersionSupportEthPassThrough((AtModule)self)) ? cAtTrue : cAtFalse;
    }

eBool Tha60210061EthPassthroughXfiIsSupported(ThaModuleEth self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);

    return (hwVersion >= StartVersionSupportEthPassThroughXfi()) ? cAtTrue : cAtFalse;
    }

eAtRet Tha60210061ModuleEthGeBypassBufferActivate(ThaModuleEth self, eBool enable)
    {
    uint32 address = cGeBypassBaseAddress + cAf6Reg_cfg0_pen_Base;
    uint32 regVal = 0;

    mRegFieldSet(regVal, cAf6_cfg0_pen_MTU_, 0x3FFF);
    mRegFieldSet(regVal, cAf6_cfg0_pen_Active_, mBoolToBin(enable));
    mModuleHwWrite(self, address, regVal);

    return cAtOk;
    }

eAtRet Tha60210061ModuleEthXfiEgressPtchHwSet(ThaModuleEth self, uint16 ptch, uint8 serviceType)
    {
    uint32 regAddr = (uint32)(cAf6Reg_upen_ptch_rx_Base + cMacEthCoreBase + ptch); /* In mac id = 0, xfi */
    uint32 regVal = mModuleHwRead(self, regAddr);

    mRegFieldSet(regVal, cAf6_upen_ptch_rx_Ser_Typ_, serviceType);
    mRegFieldSet(regVal, cAf6_upen_ptch_rx_Out_En_, 1);

    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

eAtRet Tha60210061ModuleEthXfiEgressPtchHwClear(ThaModuleEth self, uint16 ptch)
    {
    uint32 regAddr = (uint32)(cAf6Reg_upen_ptch_rx_Base + cMacEthCoreBase + ptch); /* In mac id = 0, xfi */

    mModuleHwWrite(self, regAddr, 0x0);
    return cAtOk;
    }

eAtRet Tha60210061ModuleEthCoreIsolationEnable(ThaModuleEth self, eBool enable)
    {
    /* This register is to isolate ethernet core from other modules while diagnostic to prevent harming them */
    const uint32 cIsolate = 0;
    const uint32 cAllPortsEnable = cBit11_4  | cBit0;

    uint32 regAddr = (uint32)(cAf6Reg_upen_userport_en_Base + cMacEthCoreBase);
    uint32 regVal = mModuleHwRead(self, regAddr);

    mRegFieldSet(regVal, cAf6_upen_ethpgen_En_, mBoolToBin(enable));
    mRegFieldSet(regVal, cAf6_upen_ethpgen_Mac_RX_En_, (enable) ? cIsolate : cAllPortsEnable);
    mRegFieldSet(regVal, cAf6_upen_ethpgen_Mac_TX_En_, (enable) ? cIsolate : cAllPortsEnable);

    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

uint32 Tha60210061StartVersionRemoveXfiDiagPrbsEngine(void)
    {
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x4, 0x1097);
    }

eBool Tha60210061ModuleEthPortIs10G(AtEthPort port)
    {
    return PortIs10G(port);
    }

eBool Tha60210061ModuleEthPortIsQsgmii(AtEthPort port)
    {
    if (port == Tha60210061ModuleEthQsgmiiPortGet((AtModuleEth)AtChannelModuleGet((AtChannel)port)))
        return cAtTrue;

    return cAtFalse;
    }

eBool Tha60210061ModuleEthPortIsSgmii(AtEthPort port)
    {
    uint32 portId = AtChannelIdGet((AtChannel)port);
    if ((AtEthPortParentPortGet(port) == NULL) && (portId >= cSgmiiPortStartId) && (portId <= cSgmiiPortEndId))
        return cAtTrue;

    return cAtFalse;
    }


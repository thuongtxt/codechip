/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Ethernet
 *
 * File        : Tha60210061EthPort10G.c
 *
 * Created Date: Jul 4, 2016
 *
 * Description : 60210061 Ethernet port 10G
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../Tha60210012/eth/Tha60210012EthPortInternal.h"
#include "../../../default/man/ThaDevice.h"
#include "../physical/Tha60210061SerdesManager.h"
#include "../physical/Tha60210061TopCtrReg.h"
#include "Tha60210061ModuleEth.h"
#include "Tha60210061EthPort.h"
#include "Tha60210061EthPassThroughReg.h"
#include "Tha60210061SgmiiEthPort.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210061EthPort10G*)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061EthPort10G
    {
    tTha60210012EthPort super;
    AtSerdesController txSerdes;
    }tTha60210061EthPort10G;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods       m_AtObjectOverride;
static tAtEthPortMethods      m_AtEthPortOverride;
static tAtChannelMethods      m_AtChannelOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
static const tAtEthPortMethods *m_AtEthPortMethods = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleEth ModuleEth(AtEthPort self)
    {
    return (ThaModuleEth)AtChannelModuleGet((AtChannel)self);
    }

static AtSerdesController SerdesController(AtEthPort self)
    {
    ThaModuleEth ethModule = ModuleEth(self);
    uint32 portId = AtChannelHwIdGet((AtChannel)self);
    uint32 serdesId = mMethodsGet(ethModule)->EthPortIdToSerdesId(ethModule, portId);

    return AtModuleEthSerdesController((AtModuleEth)ethModule, serdesId);
    }

static eAtModuleEthRet RxSerdesSelect(AtEthPort self, AtSerdesController serdes)
    {
    uint32 regVal, serdesId;
    uint32 address = cAf6Reg_o_control1 + cTha60210061TopBaseAddress;

    serdesId = AtSerdesControllerIdGet(serdes);
    regVal = mChannelHwRead(self, address, cAtModuleEth);

    if (serdesId == cTha60210061XfiSerdesId0)
        mRegFieldSet(regVal, cThaReg_XFI_Mac0_Sel_, 0x0);

    else if (serdesId == cTha60210061XfiSerdesId1)
        mRegFieldSet(regVal, cThaReg_XFI_Mac0_Sel_, 0x2);

    else if (serdesId == cTha60210061RxauiSerdesId0)
        mRegFieldSet(regVal, cThaReg_XFI_Mac0_Sel_, 0x1);

    else if (serdesId == cTha60210061RxauiSerdesId1)
        mRegFieldSet(regVal, cThaReg_XFI_Mac0_Sel_, 0x3);

    else
        return cAtErrorModeNotSupport;

    mChannelHwWrite(self, address, regVal, cAtModuleEth);

    return cAtOk;
    }

static AtSerdesController RxSelectedSerdes(AtEthPort self)
    {
    uint32 regVal, hwValue;
    ThaModuleEth ethModule = ModuleEth(self);
    uint32 address = cAf6Reg_o_control1 + cTha60210061TopBaseAddress;

    regVal = mChannelHwRead(self, address, cAtModuleEth);
    hwValue = mRegField(regVal, cThaReg_XFI_Mac0_Sel_);

    if (hwValue == 0x2)
        return AtModuleEthSerdesController((AtModuleEth)ethModule, cTha60210061XfiSerdesId1);

    if (hwValue == 0x0)
        return AtModuleEthSerdesController((AtModuleEth)ethModule, cTha60210061XfiSerdesId0);

    if (hwValue == 0x1)
        return AtModuleEthSerdesController((AtModuleEth)ethModule, cTha60210061RxauiSerdesId0);

    if (hwValue == 0x3)
        return AtModuleEthSerdesController((AtModuleEth)ethModule, cTha60210061RxauiSerdesId1);

    return NULL;
    }

static eAtModuleEthRet TxSerdesHwBridge(AtEthPort self, AtSerdesController serdes)
    {
    AtUnused(self);
    return AtSerdesControllerEnable(serdes, cAtTrue);
    }

static eAtModuleEthRet TxSerdesBridgeHwRelease(AtEthPort self, AtSerdesController serdes)
    {
    AtUnused(self);
    return AtSerdesControllerEnable(serdes, cAtFalse);
    }

static AtSerdesController PartnerSerdes(AtEthPort self, AtSerdesController serdes)
    {
    return Tha60210061ModuleEthPartnerSerdes((AtModuleEth)ModuleEth(self), serdes);
    }

static eBool SerdesIsValid(AtSerdesController serdes)
    {
    uint32 serdesId = AtSerdesControllerIdGet(serdes);
    if ((serdesId == cTha60210061XfiSerdesId0)   ||
        (serdesId == cTha60210061XfiSerdesId1)   ||
        (serdesId == cTha60210061RxauiSerdesId0) ||
        (serdesId == cTha60210061RxauiSerdesId1))
        return cAtTrue;

    return cAtFalse;
    }

static eAtModuleEthRet TxSerdesBridge(AtEthPort self, AtSerdesController serdes)
    {
    AtSerdesController bridgedSerdes = AtEthPortTxBridgedSerdes(self);
    if (serdes == bridgedSerdes)
        return cAtOk;

    if (serdes)
        {
        if (!SerdesIsValid(serdes))
            return cAtErrorModeNotSupport;

        if (serdes != PartnerSerdes(self, AtEthPortTxSerdesGet(self)))
            return cAtErrorModeNotSupport;

        return TxSerdesHwBridge(self, serdes);
        }

    return TxSerdesBridgeHwRelease(self, bridgedSerdes);
    }

static AtSerdesController TxBridgedSerdes(AtEthPort self)
    {
    /* No tx, no bridge */
    AtSerdesController txSerdes = AtEthPortTxSerdesGet(self);
    AtSerdesController partnerSerdes;

    if (txSerdes == NULL)
        return NULL;

    partnerSerdes = PartnerSerdes(self, txSerdes);
    if (AtSerdesControllerIsEnabled(partnerSerdes))
        return partnerSerdes;

    return NULL;
    }

static eBool TxSerdesCanBridge(AtEthPort self, AtSerdesController serdes)
    {
    return Tha60210061TxSerdesCanBridge(self, serdes);
    }

static eAtModuleEthRet TxSerdesSet(AtEthPort self, AtSerdesController serdes)
    {
    eAtRet ret = cAtOk;

    /* Already set */
    AtSerdesController txSerdes = AtEthPortTxSerdesGet(self);
    if (serdes == txSerdes)
        return cAtOk;

    if (!SerdesIsValid(serdes))
        return cAtErrorModeNotSupport;

    /* Change XFI and RXAUI */
    if (txSerdes && (serdes != PartnerSerdes(self, txSerdes)))
        {
        AtEthPortTxSerdesBridge(self, NULL);
        TxSerdesBridgeHwRelease(self, txSerdes);
        }

    ret = TxSerdesHwBridge(self, serdes);
    if (ret != cAtOk)
        return ret;

    mThis(self)->txSerdes = serdes;
    return AtEthPortTxSerdesBridge(self, PartnerSerdes(self, serdes));
    }

static AtSerdesController TxSerdesGet(AtEthPort self)
    {
    return mThis(self)->txSerdes;
    }

static uint32 StartVersionSupporNewCounters(AtModule self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x7, 0x1002);
    }

static eBool NewCounterIsSupported(AtModuleEth self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);

    return (hwVersion >= StartVersionSupporNewCounters((AtModule)self)) ? cAtTrue : cAtFalse;
    }

static uint32 CounterReadToClear(AtChannel self, uint16 counterType, eBool read2Clear)
    {
    uint32 regAddress, offset;
    uint32 baseAdress, hwCounterId;

    baseAdress = cMacEthCoreBase;
    hwCounterId = Tha60210061EthPortSW2HwCounterId((AtEthPort)self, counterType);

    offset = (uint32)(AtChannelIdGet(self) * 0x100) + (uint32)(read2Clear * 0x40) + hwCounterId;
    regAddress = cAf6Reg_upen_count_Base + offset + baseAdress;
    return mChannelHwRead(self, regAddress, cAtModuleEth);
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    if ((!NewCounterIsSupported((AtModuleEth)ModuleEth((AtEthPort)self))) ||
        (counterType == cAtEthPortCounterRxErrPsnPackets))
        return m_AtChannelMethods->CounterGet(self, counterType);

    return CounterReadToClear(self, counterType, cAtFalse);
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    if ((!NewCounterIsSupported((AtModuleEth)ModuleEth((AtEthPort)self))) ||
         (counterType == cAtEthPortCounterRxErrPsnPackets))
        return m_AtChannelMethods->CounterClear(self, counterType);

    return CounterReadToClear(self, counterType, cAtTrue);
    }

static eBool CounterIsSupported(AtChannel self, uint16 counterType)
    {
    if (!NewCounterIsSupported((AtModuleEth)ModuleEth((AtEthPort)self)))
        return m_AtChannelMethods->CounterIsSupported(self, counterType);

    if ((counterType == cAtEthPortCounterTxBrdCastPackets) ||
        (counterType == cAtEthPortCounterTxMultCastPackets))
        return cAtFalse;

    return m_AtChannelMethods->CounterIsSupported(self, counterType);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210061EthPort10G *object = (tTha60210061EthPort10G *)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObjectDescription(txSerdes);
    }

static void OverrideAtObject(AtEthPort self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtEthPort(AtEthPort self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEthPortMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPortOverride, mMethodsGet(self), sizeof(m_AtEthPortOverride));

        mMethodOverride(m_AtEthPortOverride, SerdesController);
        mMethodOverride(m_AtEthPortOverride, RxSerdesSelect);
        mMethodOverride(m_AtEthPortOverride, RxSelectedSerdes);
        mMethodOverride(m_AtEthPortOverride, TxSerdesSet);
        mMethodOverride(m_AtEthPortOverride, TxSerdesGet);
        mMethodOverride(m_AtEthPortOverride, TxSerdesCanBridge);
        mMethodOverride(m_AtEthPortOverride, TxBridgedSerdes);
        mMethodOverride(m_AtEthPortOverride, TxSerdesBridge);
        }

    mMethodsSet(self, &m_AtEthPortOverride);
    }

static void OverrideAtChannel(AtEthPort self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, CounterIsSupported);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtEthPort self)
    {
    OverrideAtObject(self);
    OverrideAtEthPort(self);
    OverrideAtChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061EthPort10G);
    }

static AtEthPort ObjectInit(AtEthPort self, uint8 portId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012EthPortObjectInit((AtEthPort)self, portId, module) == NULL)
        return NULL;

    /* Initialize method */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEthPort Tha60210061EthPort10GNew(uint8 portId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthPort newPort = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPort == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPort, portId, module);
    }

eBool Tha60210061TxSerdesCanBridge(AtEthPort self, AtSerdesController serdes)
    {
    AtSerdesController bridgedSerdes = AtEthPortTxBridgedSerdes(self);
    AtSerdesController txSerdes = AtEthPortTxSerdesGet(self);

    if (bridgedSerdes && serdes && (serdes != bridgedSerdes))
        return cAtFalse;

    if (serdes == txSerdes)
        return cAtFalse;

    if (serdes != PartnerSerdes(self, txSerdes))
        return cAtFalse;

    return cAtTrue;
    }

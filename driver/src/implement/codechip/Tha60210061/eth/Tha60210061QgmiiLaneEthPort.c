/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60210061QgmiiLaneEthPort.c
 *
 * Created Date: Dec 5, 2016
 *
 * Description : QSGMII Lane Ethernet port
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/eth/ThaEthPortInternal.h"
#include "Tha60210061SgmiiEthPort.h"
#include "Tha60210061EthPassThroughReg.h"
#include "Tha60210061ModuleEth.h"
#include "Tha60210061QgmiiLaneEthPort.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210061QgmiiLaneEthPort*)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061QgmiiLaneEthPort
    {
    tThaEthPort super;
    eBool enabled;
    }tTha60210061QgmiiLaneEthPort;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods       m_AtObjectOverride;
static tAtChannelMethods      m_AtChannelOverride;
static tThaEthPortMethods     m_ThaEthPortOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 PortIdSw2Hw(AtChannel self)
    {
    uint32 channelId = AtChannelIdGet(self);

    if (channelId == 0) return 8;
    if (channelId == 1) return 9;
    if (channelId == 2) return 10;
    if (channelId == 3) return 11;

    return cInvalidUint8;
    }

static uint32 CounterReadToClear(AtChannel self, uint16 counterType, eBool read2Clear)
    {
    uint32 regAddress, offset;
    uint32 baseAdress = cMacEthCoreBase;
    uint16 hwCounterId = Tha60210061EthPortSW2HwCounterId((AtEthPort)self, counterType);

    offset = (uint32)(PortIdSw2Hw(self) * 0x100) + (uint32)(read2Clear * 0x40) + hwCounterId;
    regAddress = cAf6Reg_upen_count_Base + offset + baseAdress;
    return mChannelHwRead(self, regAddress, cAtModuleEth);
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    return CounterReadToClear(self, counterType, cAtFalse);
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    return CounterReadToClear(self, counterType, cAtTrue);
    }

static uint32 HwIdGet(AtChannel self)
    {
    return PortIdSw2Hw(self);
    }

static eBool CounterIsSupported(AtChannel self, uint16 counterType)
    {
    return Tha60210061EthPort1GCounterIsSupported(self, counterType);
    }

static eBool FcsByPassed(ThaEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet Enable(AtChannel self, eBool enable)
    {
    mThis(self)->enabled = enable;
    return cAtOk;
    }

static eBool IsEnabled(AtChannel self)
    {
    return mThis(self)->enabled;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210061QgmiiLaneEthPort *object = (tTha60210061QgmiiLaneEthPort *)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeUInt(enabled);
    }

static eAtRet AllCountersClear(AtChannel self, void *pAllCounters)
    {
    return Tha60210061EthPort1GAllCountersReadToClear(self, pAllCounters, cAtTrue);
    }

static eAtRet AllCountersGet(AtChannel self, void *pAllCounters)
    {
    return Tha60210061EthPort1GAllCountersReadToClear(self, pAllCounters, cAtFalse);
    }

static void OverrideAtChannel(AtEthPort self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, HwIdGet);
        mMethodOverride(m_AtChannelOverride, CounterIsSupported);
        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, IsEnabled);
        mMethodOverride(m_AtChannelOverride, AllCountersClear);
        mMethodOverride(m_AtChannelOverride, AllCountersGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideThaEthPort(AtEthPort self)
    {
    ThaEthPort port = (ThaEthPort)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthPortOverride, mMethodsGet(port), sizeof(m_ThaEthPortOverride));

        mMethodOverride(m_ThaEthPortOverride, FcsByPassed);
        }

    mMethodsSet(port, &m_ThaEthPortOverride);
    }

static void OverrideAtObject(AtEthPort self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtEthPort self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideThaEthPort(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061QgmiiLaneEthPort);
    }

static AtEthPort ObjectInit(AtEthPort self, uint8 portId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaEthPortObjectInit((AtEthPort)self, portId, module) == NULL)
        return NULL;

    /* Initialize method */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEthPort Tha60210061QsgmiiLaneEthPortNew(uint8 portId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthPort newPort = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPort == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPort, portId, module);
    }

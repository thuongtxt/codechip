/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Ethernet
 *
 * File        : Tha60210061ImsgEopPtchService.c
 *
 * Created Date: Jan 9, 2017
 *
 * Description : Imsg/Eop PTCH service
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210012/physical/Tha60210012PtchServiceInternal.h"
#include "../cla/Tha60210061ModuleCla.h"
#include "Tha60210061ModuleEth.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061ImsgEopPtchService
    {
    tTha60210012ImsgEopPtchService super;
    }tTha60210061ImsgEopPtchService;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPtchServiceMethods m_AtPtchServiceOverride;

/* Super implementation */
static const tAtPtchServiceMethods * m_AtPtchServiceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleEth ModuleEth(AtPtchService self)
    {
    return (ThaModuleEth)AtPtchServiceModuleGet(self);
    }

static ThaModuleCla ModuleCla(AtPtchService self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)ModuleEth(self));
    return (ThaModuleCla)AtDeviceModuleGet(device, cThaModuleCla);
    }

static eBool EgressPtchHasBeenSet(Tha60210012PtchService self)
    {
    return self->egressPtchSet;
    }

static eAtRet EgressPtchSet(AtPtchService self, uint16 ptch)
    {
    ThaModuleEth moduleEth = ModuleEth(self);
    eAtRet ret = cAtOk;

    if (Tha60210061EthPassthroughXfiIsSupported(moduleEth))
        {
        Tha60210012PtchService service = (Tha60210012PtchService)self;
        const uint8 cImsgEopService = 1;

        if (EgressPtchHasBeenSet(service))
            ret = Tha60210061ModuleEthXfiEgressPtchHwClear(moduleEth, service->ptchId);

        ret |= Tha60210061ModuleEthXfiEgressPtchHwSet(moduleEth, ptch, cImsgEopService);
        }

    if (!Tha60210061ModuleClaPtchControlIsRemoved(ModuleCla(self)))
        ret |= m_AtPtchServiceMethods->EgressPtchSet(self, ptch);

    return ret;
    }

static void OverrideAtPtchService(AtPtchService self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPtchServiceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPtchServiceOverride, m_AtPtchServiceMethods, sizeof(m_AtPtchServiceOverride));

        mMethodOverride(m_AtPtchServiceOverride, EgressPtchSet);
        }

    mMethodsSet(self, &m_AtPtchServiceOverride);
    }

static void Override(AtPtchService self)
    {
    OverrideAtPtchService(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061ImsgEopPtchService);
    }

static AtPtchService ObjectInit(AtPtchService self, AtModuleEth ethModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012ImsgEopPtchServiceObjectInit(self, ethModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPtchService Tha60210061ImsgEopPtchServiceNew(AtModuleEth ethModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPtchService newPtchService = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    return ObjectInit(newPtchService, ethModule);
    }

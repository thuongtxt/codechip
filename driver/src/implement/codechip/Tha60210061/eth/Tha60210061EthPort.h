/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60210061EthPort.h
 * 
 * Created Date: Sep 8, 2016
 *
 * Description : Ethernet Port
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061ETHPORT_H_
#define _THA60210061ETHPORT_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eBool Tha60210061TxSerdesCanBridge(AtEthPort self, AtSerdesController serdes);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061ETHPORT_H_ */


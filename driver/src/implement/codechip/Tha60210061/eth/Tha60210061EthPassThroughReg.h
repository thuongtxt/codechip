/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60210061EthPassThroughReg.h
 * 
 * Created Date: Dec 6, 2016
 *
 * Description : Ethernet pass-through register
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061ETHPASSTHROUGHREG_H_
#define _THA60210061ETHPASSTHROUGHREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name   : Ethernet Core Version
Reg Addr   : 0x00_0000
Reg Formula:
    Where  :
Reg Desc   :
This register is used to get Version of ETH Version

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ethver_Base                                                                      0x000000

/*--------------------------------------
BitField Name: ETHEngId
BitField Type: RO
BitField Desc: Engine ID
BitField Bits: [31:20]
--------------------------------------*/
#define cAf6_upen_ethver_ETHEngId_Mask                                                               cBit31_20
#define cAf6_upen_ethver_ETHEngId_Shift                                                                     20

/*--------------------------------------
BitField Name: ETHVerId
BitField Type: RO
BitField Desc: Version ID
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_upen_ethver_ETHVerId_Mask                                                               cBit19_16
#define cAf6_upen_ethver_ETHVerId_Shift                                                                     16

/*--------------------------------------
BitField Name: ETHEditId
BitField Type: RO
BitField Desc: Editor ID
BitField Bits: [15:14]
--------------------------------------*/
#define cAf6_upen_ethver_ETHEditId_Mask                                                              cBit15_14
#define cAf6_upen_ethver_ETHEditId_Shift                                                                    14

/*--------------------------------------
BitField Name: ETHYearId
BitField Type: RO
BitField Desc: Year
BitField Bits: [13:09]
--------------------------------------*/
#define cAf6_upen_ethver_ETHYearId_Mask                                                               cBit13_9
#define cAf6_upen_ethver_ETHYearId_Shift                                                                     9

/*--------------------------------------
BitField Name: ETHMonId
BitField Type: RO
BitField Desc: Month
BitField Bits: [08:05]
--------------------------------------*/
#define cAf6_upen_ethver_ETHMonId_Mask                                                                 cBit8_5
#define cAf6_upen_ethver_ETHMonId_Shift                                                                      5

/*--------------------------------------
BitField Name: ETHDayId
BitField Type: RO
BitField Desc: Day
BitField Bits: [04:00]
--------------------------------------*/
#define cAf6_upen_ethver_ETHDayId_Mask                                                                 cBit4_0
#define cAf6_upen_ethver_ETHDayId_Shift                                                                      0

/*------------------------------------------------------------------------------
Reg Name   : ETH Core User Port Enable
Reg Addr   : 0x00_0001
Reg Formula:
    Where  :
Reg Desc   :
This register  is used to disable interface with PDA/PLA

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_userport_en_Base                                                                0x000001

/*--------------------------------------
BitField Name: Mac_TX_En
BitField Type: R/W
BitField Desc: Mac TX enable, 0-> disable -tx_mac_en 0-0   : 10G mac -tx_mac_en
1-3   : Unused -tx_mac_en 4-7   : SGMII (share SFP with OCN) -tx_mac_en 8-11  :
QGMII -tx_mac_en 12-14 : Unused -tx_mac_en 15    : Gen engine
BitField Bits: [27:16]
--------------------------------------*/
#define cAf6_upen_ethpgen_Mac_TX_En_Mask                                                             cBit27_16
#define cAf6_upen_ethpgen_Mac_TX_En_Shift                                                                   16

/*--------------------------------------
BitField Name: Mac_RX_En
BitField Type: R/W
BitField Desc: Mac RX enable, 0-> disable -rx_mac_en 0-0   : 10G mac -rx_mac_en
1-3   : Unused -rx_mac_en 4-7   : SGMII (share SFP with OCN) -rx_mac_en 8-11  :
QGMII -rx_mac_en 12-14 : Unused -rx_mac_en 15    : Gen engine
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_upen_ethpgen_Mac_RX_En_Mask                                                              cBit11_0
#define cAf6_upen_ethpgen_Mac_RX_En_Shift                                                                    0

#define cAf6_upen_ethpgen_En_Mask                                                                       cBit15
#define cAf6_upen_ethpgen_En_Shift                                                                          15

/*------------------------------------------------------------------------------
Reg Name   : ETH Core Switch Control
Reg Addr   : 0x00_0100
Reg Formula: 0x00_0100 + $in_mac_id
    Where  :
           + $in_mac_id(0-11) : in_mac_id
Reg Desc   :
This register  is used to configure mac_map for output mac
- in_mac_id 0-0   : 10G mac (mux XFI and RXAUI serdes)
- in_mac_id 1-3   : Unused
- in_mac_id 4-7   : SGMII (share SFP with OCN)
- in_mac_id 8-11  : QGMII
- in_mac_id 12-15 : Unused

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ethswc_Base                                                                      0x000100

/*--------------------------------------
BitField Name: VLAN_profile
BitField Type: R/W
BitField Desc: Used to insert VLAN field base on VLAN_profile
BitField Bits: [31:28]
--------------------------------------*/
#define cAf6_upen_ethswc_VLAN_profile_Mask                                                           cBit31_28
#define cAf6_upen_ethswc_VLAN_profile_Shift                                                                 28

/*--------------------------------------
BitField Name: PTCH_profile
BitField Type: R/W
BitField Desc: Used to insert PTCH field base on PTCH_profile
BitField Bits: [27:24]
--------------------------------------*/
#define cAf6_upen_ethswc_PTCH_profile_Mask                                                           cBit27_24
#define cAf6_upen_ethswc_PTCH_profile_Shift                                                                 24

/*--------------------------------------
BitField Name: Vlan_Opt
BitField Type: R/W
BitField Desc: VLAN Option 0x0: none 0x1: check outer VLAN to lookup rxservice
0x2: check outer VLAN to lookup rxservice and remove this field 0x4: insert
VlanTag base on VLAN_profile other: unused
BitField Bits: [23:20]
--------------------------------------*/
#define cAf6_upen_ethswc_Vlan_Opt_Mask                                                               cBit23_20
#define cAf6_upen_ethswc_Vlan_Opt_Shift                                                                     20

/*--------------------------------------
BitField Name: Ptch_Opt
BitField Type: R/W
BitField Desc: PTCH Option 0x0: none 0x1: check  1 bytes PTCH to lookup
rxservice 0x2: check  1 bytes PTCH to lookup rxservice and remove this field
0x5: insert 1 bytes PTCH base on PTCH_profile 0x6: insert 2 bytes PTCH base on
PTCH_profile
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_upen_ethswc_Ptch_Opt_Mask                                                               cBit19_16
#define cAf6_upen_ethswc_Ptch_Opt_Shift                                                                     16

/*--------------------------------------
BitField Name: Mac_ID
BitField Type: R/W
BitField Desc: Output Mac id, used for SGMII -> XFI or SGMII <-> QSGMII Used to
send to engine 0x00     : CLA engine 0x01     : by pass CLA engine 0x04-0x07:
through GE buffer (of Hien) and forward to SGMII (share SFP with OCN) 0x08-0x11:
through GE buffer (of Hien) and forward to QSGMII other    : Unused
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_upen_ethswc_Mac_ID_Mask                                                                 cBit15_12
#define cAf6_upen_ethswc_Mac_ID_Shift                                                                       12

/*--------------------------------------
BitField Name: Mac_Map
BitField Type: R/W
BitField Desc: Output Mac id port map, used to interface with RSP#2 has been
release before. new feature must write all zero bit_0 correspond for Mac#0 (0 ->
Disable, 1 -> Enable) bit_1 correspond for Mac#1 (0 -> Disable, 1 -> Enable) ...
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_upen_ethswc_Mac_Map_Mask                                                                 cBit11_0
#define cAf6_upen_ethswc_Mac_Map_Shift                                                                       0

/*------------------------------------------------------------------------------
Reg Name   : ETH Core Pkt-Gen Control
Reg Addr   : 0x00_0200
Reg Formula:
    Where  :
Reg Desc   :
This register  is used to configure select packet gen engine to OUT_MAC_IDs

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ethpgen_Base                                                                     0x000200

/*--------------------------------------
BitField Name: Gen_Side
BitField Type: R/W
BitField Desc: Gen side 0x0: Gen engine -> Out_Mac_id 0x1: Gen engine -> change
in_mac_id = 0xf -> port_switch -> buffer -> Out_Mac_id
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_upen_ethpgen_Gen_Overwrite_Mask                                                            cBit12
#define cAf6_upen_ethpgen_Gen_Overwrite_Shift                                                               12

#define cAf6_upen_ethpgen_Gen_Side_Mask                                                                  cBit9
#define cAf6_upen_ethpgen_Gen_Side_Shift                                                                     9

/*--------------------------------------
BitField Name: Gen_En
BitField Type: R/W
BitField Desc: Gen enable (0 -> Disable, 1 -> Enable)
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_upen_ethpgen_Gen_En_Mask                                                                    cBit8
#define cAf6_upen_ethpgen_Gen_En_Shift                                                                       8

/*--------------------------------------
BitField Name: Gen_Mac_ID
BitField Type: R/W
BitField Desc: Gen Mac ID -gen_mac_id 0-0   : 10G mac -gen_mac_id 1-3   : Unused
-gen_mac_id 4-7   : SGMII (share SFP with OCN) -gen_mac_id 8-11  : QGMII
-gen_mac_id 12-15 : Unused
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_upen_ethpgen_Gen_Mac_ID_Mask                                                              cBit3_0
#define cAf6_upen_ethpgen_Gen_Mac_ID_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : ETH Core Pkt-Mon Control
Reg Addr   : 0x00_0300
Reg Formula: 0x00_0300 + $mon_id
    Where  :
           + $mon_id (0-8) : mon_id
Reg Desc   :
This register  is used to configure select packet mon engine from IN_MAC_IDs

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ethpmon_Base                                                                     0x000300

#define cAf6_upen_ethpmon_Mon_Source_Mask                                                               cBit12
#define cAf6_upen_ethpmon_Mon_Source_Shift                                                                  12

/*--------------------------------------
BitField Name: In_Mac_En
BitField Type: R/W
BitField Desc: Mon enable (0 -> Disable, 1 -> Enable)
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_upen_ethpmon_In_Mac_En_Mask                                                                 cBit8
#define cAf6_upen_ethpmon_In_Mac_En_Shift                                                                    8

/*--------------------------------------
BitField Name: In_Mac_ID
BitField Type: R/W
BitField Desc: Input Mac ID to mon engine -in_mac_id 0-0   : 10G mac -in_mac_id
1-3   : Unused -in_mac_id 4-7   : SGMII (share SFP with OCN) -in_mac_id 8-11  :
QGMII -in_mac_id 12-15 : Unused
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_upen_ethpmon_In_Mac_ID_Mask                                                               cBit3_0
#define cAf6_upen_ethpmon_In_Mac_ID_Shift                                                                    0

/*------------------------------------------------------------------------------
Reg Name   : ETH Core PTCH Tx Profile
Reg Addr   : 0x00_0400
Reg Formula: 0x00_0400 + $ptch_profile
    Where  :
           + $ptch_profile (0-15) : ptch_profile
Reg Desc   :
This register configures service base on PTCH Port

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ptch_tx_profile_Base                                                             0x000400

/*--------------------------------------
BitField Name: Ptch_TxValue
BitField Type: R/W
BitField Desc: Used to insert to Packets, bit_7_0 used in case PTCH 1bytes
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_ptch_tx_profile_Ptch_TxValue_Mask                                                   cBit15_0
#define cAf6_upen_ptch_tx_profile_Ptch_TxValue_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : ETH Core VLAN Tx Profile
Reg Addr   : 0x00_0500
Reg Formula: 0x00_0500 + $vlan_profile
    Where  :
           + $$vlan_profile (0-15) : vlan_profile
Reg Desc   :
This register configures service base on VLAN Port

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_vlan_tx_profile_Base                                                             0x000500

/*--------------------------------------
BitField Name: Vlan_TxValue
BitField Type: R/W
BitField Desc: Used to insert to Packets, after SA
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_vlan_tx_profile_Vlan_TxValue_Mask                                                   cBit31_0
#define cAf6_upen_vlan_tx_profile_Vlan_TxValue_Shift                                                         0

/*------------------------------------------------------------------------------
#    *************
#    * 2.1. ETH Core MAC Control
#    *************
#// Begin:
#// Register Full Name: ETH Core MAC Control
#// RTL Instant Name  : upen_emac
#//# {FunctionName,SubFunction_InstantName_Description}
#//# RTL Instant Name and Full Name MUST be unique within a register description file
#// Address      : 0x00_1000
#// Formula      : Address + $mac_id*0x100
#// Where        : {$mac_id(0-11) : Mac ID}
#// Description  : This register is used to control MAC 1G and 10G, detail ref AF6CCI0061_RD_ETHTRIP.atreg and AF6CCI0061_RD_ETH10G.atreg
#                   - mac_id 0-0    : 10G mac
#                   - mac_id 1-3    : Unused
#                   - mac_id 4-7    : SGMII (share SFP with OCN)
#                   - mac_id 8-11   : QGM
#// Width: 32
#// Register Type: {Config}
#//# Field: [Bit:Bit]   %% Name             %% Description                      %% Type             %% Reset            %% Default
#//     Field: [31:0]       %% mac_rd_reg   %% MAC value                        %% R/W              %% 0x0              %% 0x0
#// End:II
------------------------------------------------------------------------------*/
#define cAf6Reg_upen_emac_Base                                                                        0x001000

/*------------------------------------------------------------------------------
Reg Name   : ETH Core Packet Counters
Reg Addr   : 0x00_2000
Reg Formula: 0x00_2000 + $mac_id*0x100 + $r2c*0x40 + $cnt_id
    Where  :
           + $mac_id(0-11) : Mac ID
           + $r2c(0-1) : r2c bit
           + $cnt_id(0-63) : Counter ID
Reg Desc   :
This register is used to read some packet counters , support  both mode r2c and ro
- mac_id 0-0    : 10G mac
- mac_id 1-3    : Unused
- mac_id 4-7    : SGMII (share SFP with OCN)
- mac_id 8-11   : QGMII
- Counter ID details :
+ 0     : TxPkts
+ 1     : TxBytes
+ 2-3   : Unused
+ 4     : TxPktLen0_64
+ 5     : TxPktLen65_127
+ 6     : TxPktLen128_255
+ 7     : TxpktLen256_511
+ 8     : TxpktLen512_1024
+ 9     : TxpktLen1025_1528
+ 10    : TxpktLen1529_2047
+ 11    : TxpktJumbo
+ 12    : RxErrorFcs
+ 13    : RxOversize
+ 14    : RxUndersize
+ 15    : unused
+ 16    : RxPkts
+ 17    : RxBytes
+ 18-19 : Unused
+ 20    : RxPktLen0_64
+ 21    : RxPktLen65_127
+ 22    : RxPktLen128_255
+ 23    : RxpktLen256_511
+ 24    : RxpktLen512_1024
+ 25    : RxpktLen1025_1528
+ 26    : RxpktLen1529_2047
+ 27    : RxpktJumbo
+ 28    : RxBrdCastPkt
+ 29    : RxMulCastPkt
+ 30    : RxUniCastPkt
+ 31    : RxErrPsnpkt
+ 32-63 : Unused
- r2c       : enable mean read to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_count_Base                                                                       0x002000

/*--------------------------------------
BitField Name: cnt_val
BitField Type: R/W
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_count_cnt_val_Mask                                                                  cBit31_0
#define cAf6_upen_count_cnt_val_Shift                                                                        0

/*------------------------------------------------------------------------------
Reg Name   : ETH Core PORT RxService Control
Reg Addr   : 0x00_6000
Reg Formula: 0x00_6000 + $in_mac_id
    Where  :
           + $in_mac_id(0-11) : in_mac_id
Reg Desc   :
This register configures service base on PORT Port

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_port_rx_Base                                                                     0x006000

/*--------------------------------------
BitField Name: Ser_ID
BitField Type: R/W
BitField Desc: Output Service ID
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_upen_port_rx_Ser_ID_Mask                                                                cBit31_16
#define cAf6_upen_port_rx_Ser_ID_Shift                                                                      16

/*--------------------------------------
BitField Name: Out_En
BitField Type: R/W
BitField Desc: Output Enable - 0x0: Disable - 0x1: Enable
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_upen_port_rx_Out_En_Mask                                                                    cBit8
#define cAf6_upen_port_rx_Out_En_Shift                                                                       8

/*--------------------------------------
BitField Name: Ser_Typ
BitField Type: R/W
BitField Desc: This indicate the service type that the traffic following
BitField Bits: [02:00]
--------------------------------------*/
#define cAf6_upen_port_rx_Ser_Typ_Mask                                                                 cBit2_0
#define cAf6_upen_port_rx_Ser_Typ_Shift                                                                      0

/*------------------------------------------------------------------------------
Reg Name   : ETH Core PTCH RxService Control
Reg Addr   : 0x00_7000
Reg Formula: 0x00_7000 + $in_mac_id * 0x100 + $ptch_id
    Where  :
           + $in_mac_id(0-0) : in_mac_id
           + $ptch_id (0-255) : ptch_id
Reg Desc   :
This register configures service base on PTCH Port

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ptch_rx_Base                                                                     0x007000

/*--------------------------------------
BitField Name: Ser_ID
BitField Type: R/W
BitField Desc: Output Service ID
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_upen_ptch_rx_Ser_ID_Mask                                                                cBit31_16
#define cAf6_upen_ptch_rx_Ser_ID_Shift                                                                      16

/*--------------------------------------
BitField Name: Out_En
BitField Type: R/W
BitField Desc: Output Enable - 0x0: Disable - 0x1: Enable
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_upen_ptch_rx_Out_En_Mask                                                                    cBit8
#define cAf6_upen_ptch_rx_Out_En_Shift                                                                       8

/*--------------------------------------
BitField Name: Ser_Typ
BitField Type: R/W
BitField Desc: This indicate the service type that the traffic following
BitField Bits: [02:00]
--------------------------------------*/
#define cAf6_upen_ptch_rx_Ser_Typ_Mask                                                                 cBit2_0
#define cAf6_upen_ptch_rx_Ser_Typ_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : ETH Core VLAN RxService Control
Reg Addr   : 0x00_8000
Reg Formula: 0x00_8000 + $in_mac_id * 0x1000 + $vlan_id
    Where  :
           + $in_mac_id(0-0) : in_mac_id
           + $vlan_id (0-4095) : vlan_id
Reg Desc   :
This register configures service base on VLAN Port

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_vlan_rx_Base                                                                     0x008000

/*--------------------------------------
BitField Name: Ser_ID
BitField Type: R/W
BitField Desc: Output Service ID
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_upen_vlan_rx_Ser_ID_Mask                                                                cBit31_16
#define cAf6_upen_vlan_rx_Ser_ID_Shift                                                                      16

/*--------------------------------------
BitField Name: Out_En
BitField Type: R/W
BitField Desc: Output Enable - 0x0: Disable - 0x1: Enable
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_upen_vlan_rx_Out_En_Mask                                                                    cBit8
#define cAf6_upen_vlan_rx_Out_En_Shift                                                                       8

/*--------------------------------------
BitField Name: Ser_Typ
BitField Type: R/W
BitField Desc: This indicate the service type that the traffic following
BitField Bits: [02:00]
--------------------------------------*/
#define cAf6_upen_vlan_rx_Ser_Typ_Mask                                                                 cBit2_0
#define cAf6_upen_vlan_rx_Ser_Typ_Shift                                                                      0

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061ETHPASSTHROUGHREG_H_ */


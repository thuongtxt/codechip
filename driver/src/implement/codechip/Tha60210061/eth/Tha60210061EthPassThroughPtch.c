/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Ethernet
 *
 * File        : Tha60210061EthPassThroughPtch.c
 *
 * Created Date: Jan 3, 2017
 *
 * Description : Ethernet passthrough PTCH
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtEthPort.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/man/AtModuleInternal.h"
#include "../../../../generic/common/AtChannelInternal.h"
#include "../../../default/eth/ThaModuleEth.h"
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../Tha60210012/physical/Tha60210012PtchServiceInternal.h"
#include "../../Tha60210012/pwe/Tha60210012ModulePwe.h"
#include "Tha60210061EthPassThroughReg.h"
#include "Tha60210061ModuleEth.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210061EthPassThroughPtch*)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061EthPassThroughPtch
    {
    tTha60210012PtchService super;
    AtEthPort gePort;
    }tTha60210061EthPassThroughPtch;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods               m_AtObjectOverride;
static tTha60210012PtchServiceMethods m_Tha60210012PtchServiceOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 PortIdSw2Hw(AtEthPort gePort)
    {
    uint32 portId = AtChannelIdGet((AtChannel)gePort);

    if (portId == 1) return 0;
    if (portId == 2) return 1;
    if (portId == 3) return 2;
    if (portId == 4) return 3;

    return cInvalidUint8;
    }

static eAtRet EgressPtchHwClear(Tha60210012PtchService self, uint16 ptch)
    {
    AtEthPort gePort = mThis(self)->gePort;
    uint32 regAddr = (uint32)(cAf6Reg_upen_ptch_rx_Base + cMacEthCoreBase + ptch); /* In mac id = 0, xfi */

    mChannelHwWrite(gePort, regAddr, 0x0, cAtModuleEth);
    return cAtOk;
    }

static eAtRet EgressPtchHwSet(Tha60210012PtchService self, uint16 ptch)
    {
    AtEthPort gePort;
    uint32 regAddr, regVal;
    const uint8 cEthPassthoughType = 5;

    gePort = mThis(self)->gePort;
    regAddr = (uint32)(cAf6Reg_upen_ptch_rx_Base + cMacEthCoreBase + ptch); /* In mac id = 0, xfi */
    regVal = mChannelHwRead(gePort, regAddr, cAtModuleEth);

    mRegFieldSet(regVal, cAf6_upen_ptch_rx_Ser_ID_, PortIdSw2Hw(mThis(self)->gePort));
    mRegFieldSet(regVal, cAf6_upen_ptch_rx_Ser_Typ_, cEthPassthoughType);
    mRegFieldSet(regVal, cAf6_upen_ptch_rx_Out_En_, 1);

    mChannelHwWrite(gePort, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static uint32 HwServiceType(Tha60210012PtchService self)
    {
    return AtChannelIdGet((AtChannel)mThis(self)->gePort) + 3;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210061EthPassThroughPtch *object = (tTha60210061EthPassThroughPtch *)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObject(gePort);
    }

static void OverrideAtObject(AtPtchService self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideTha60210012PtchService(AtPtchService self)
    {
    Tha60210012PtchService service = (Tha60210012PtchService)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210012PtchServiceOverride, mMethodsGet(service), sizeof(m_Tha60210012PtchServiceOverride));

        mMethodOverride(m_Tha60210012PtchServiceOverride, HwServiceType);
        mMethodOverride(m_Tha60210012PtchServiceOverride, EgressPtchHwClear);
        mMethodOverride(m_Tha60210012PtchServiceOverride, EgressPtchHwSet);
        }

    mMethodsSet(service, &m_Tha60210012PtchServiceOverride);
    }

static void Override(AtPtchService self)
    {
    OverrideAtObject(self);	
    OverrideTha60210012PtchService(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061EthPassThroughPtch);
    }

static AtPtchService ObjectInit(AtPtchService self, AtEthPort gePort)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012PtchServiceObjectInit(self, cAtPtchServiceTypeGeBypass, (AtModuleEth)AtChannelModuleGet((AtChannel)gePort)) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;
    mThis(self)->gePort = gePort;

    return self;
    }

AtPtchService Tha60210061EthPassthoughPtchNew(AtEthPort gePort)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPtchService newPtchService = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    return ObjectInit(newPtchService, gePort);
    }


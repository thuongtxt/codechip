/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Ethernet
 *
 * File        : Tha60210061SgmiiEthPort.c
 *
 * Created Date: Sep 7, 2016
 *
 * Description : SGMII ethernet port
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/prbs/AtModulePrbsInternal.h"
#include "../../../default/eth/ThaEthPortInternal.h"
#include "../../../default/eth/ThaModuleEthInternal.h"
#include "../../../default/eth/ThaEthPortReg.h"
#include "../physical/Tha60210061SerdesManager.h"
#include "Tha60210061ModuleEth.h"
#include "Tha60210061EthPassThroughReg.h"
#include "Tha60210061SgmiiEthPort.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210061SgmiiEthPort*)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061SgmiiEthPort
    {
    tThaEthPort super;
    AtEthPort bypassPort;
    AtPtchService bypassPtch;
    eBool enable;
    AtPrbsEngine prbsEngine;
    }tTha60210061SgmiiEthPort;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods       m_AtObjectOverride;
static tAtChannelMethods      m_AtChannelOverride;
static tAtEthPortMethods      m_AtEthPortOverride;
static tThaEthPortMethods     m_ThaEthPortOverride;


/* Save super implementation */
static const tAtObjectMethods   *m_AtObjectMethods = NULL;
static const tAtChannelMethods  *m_AtChannelMethods = NULL;
static const tAtEthPortMethods  *m_AtEthPortMethods = NULL;
static const tThaEthPortMethods *m_ThaEthPortMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleEth ModuleEth(AtEthPort self)
    {
    return (ThaModuleEth)AtChannelModuleGet((AtChannel)self);
    }

static AtModulePrbs ModulePrbs(AtEthPort self)
    {
    return (AtModulePrbs)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cAtModulePrbs);
    }

static AtSerdesController SerdesController(AtEthPort self)
    {
    ThaModuleEth ethModule = ModuleEth(self);
    uint32 portId = AtChannelIdGet((AtChannel)self);
    uint32 serdesId = mMethodsGet(ethModule)->EthPortIdToSerdesId(ethModule, portId);

    return AtModuleEthSerdesController((AtModuleEth)ethModule, serdesId);
    }

static eAtModuleEthRet RxSerdesSelect(AtEthPort self, AtSerdesController serdes)
    {
    return (SerdesController(self) == serdes) ? cAtOk : cAtErrorModeNotSupport;
    }

static AtSerdesController RxSelectedSerdes(AtEthPort self)
    {
    return SerdesController(self);
    }

static eAtModuleEthRet TxSerdesBridge(AtEthPort self, AtSerdesController serdes)
    {
    AtUnused(self);
    AtUnused(serdes);
    return cAtErrorModeNotSupport;
    }

static AtSerdesController TxBridgedSerdes(AtEthPort self)
    {
    AtUnused(self);
    return NULL;
    }

static eBool TxSerdesCanBridge(AtEthPort self, AtSerdesController serdes)
    {
    AtUnused(self);
    AtUnused(serdes);
    return cAtFalse;
    }

static eAtModuleEthRet TxSerdesSet(AtEthPort self, AtSerdesController serdes)
    {
    return (SerdesController(self) == serdes) ? cAtOk : cAtErrorModeNotSupport;
    }

static AtSerdesController TxSerdesGet(AtEthPort self)
    {
    return SerdesController(self);
    }

static uint8 PortIdSw2Hw(AtChannel self)
    {
    uint32 portId = AtChannelIdGet(self);

    if (portId == 1) return 4;
    if (portId == 2) return 5;
    if (portId == 3) return 6;
    if (portId == 4) return 7;

    return cInvalidUint8;
    }

static uint32 CounterReadToClear(AtChannel self, uint16 counterType, eBool read2Clear)
    {
    uint32 regAddress, offset;
    uint32 baseAdress, hwCounterId;

    if (!Tha60210061EthPassThroughIsSupported((AtModuleEth)ModuleEth((AtEthPort)self)))
        return 0;

    baseAdress = cMacEthCoreBase;
    hwCounterId = Tha60210061EthPortSW2HwCounterId((AtEthPort)self, counterType);

    offset = (uint32)(PortIdSw2Hw(self) * 0x100) + (uint32)(read2Clear * 0x40) + hwCounterId;
    regAddress = cAf6Reg_upen_count_Base + offset + baseAdress;
    return mChannelHwRead(self, regAddress, cAtModuleEth);
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    return CounterReadToClear(self, counterType, cAtFalse);
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    return CounterReadToClear(self, counterType, cAtTrue);
    }

static uint32 HwIdGet(AtChannel self)
    {
    return PortIdSw2Hw(self);
    }

static eBool CounterIsSupported(AtChannel self, uint16 counterType)
    {
    return Tha60210061EthPort1GCounterIsSupported(self, counterType);
    }

static eBool FcsByPassed(ThaEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet MacActivate(ThaEthPort self, eBool activate)
    {
    if (Tha60210061EthPassThroughIsSupported((AtModuleEth)ModuleEth((AtEthPort)self)))
        return m_ThaEthPortMethods->MacActivate(self, activate);

    return cAtOk;
    }

static void HwFlush(AtChannel self)
    {
    ThaEthPort port = (ThaEthPort)self;
    uint32 offset = ThaEthPortDefaultOffset(port) + ThaEthPortMacBaseAddress(port);
    uint32 regAddress = cThaRegIPEthernetTripleSpdGlbCtrl + offset;
    uint32 regVal = mChannelHwRead(self, regAddress, cAtModuleEth);

    mRegFieldSet(regVal, cThaIpEthTripGlbCtlRst, 1);
    mChannelHwWrite(self, regAddress, regVal, cAtModuleEth);

    mRegFieldSet(regVal, cThaIpEthTripGlbCtlRst, 0);
    mChannelHwWrite(self, regAddress, regVal, cAtModuleEth);
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret;

    if (Tha60210061EthPassThroughIsSupported((AtModuleEth)ModuleEth((AtEthPort)self)))
        {
        ret = m_AtChannelMethods->Init(self);
        if (ret != cAtOk)
            return ret;

        AtChannelEnable(self, cAtTrue);
        HwFlush(self);
        return ret;
        }

    return cAtOk;
    }

static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject)(mThis(self)->bypassPtch));
    mThis(self)->bypassPtch = NULL;
    AtModulePrbsEngineObjectDelete(ModulePrbs((AtEthPort)self), mThis(self)->prbsEngine);
    mThis(self)->prbsEngine = NULL;

    /* Super deletion */
    m_AtObjectMethods->Delete(self);
    }

static void PrbsEngineSet(AtChannel self, AtPrbsEngine engine)
    {
    mThis(self)->prbsEngine = engine;
    }

static AtPrbsEngine PrbsEngineGet(AtChannel self)
    {
    return mThis(self)->prbsEngine;
    }

static eBool PrbsEngineIsCreated(AtChannel self)
    {
    if (mThis(self)->prbsEngine)
        return cAtTrue;

    return cAtFalse;
    }

static uint16 DefaultPtch(AtEthPort self)
    {
    return (uint16)(cAtPtchServiceTypeGeBypass + AtChannelIdGet((AtChannel)self));
    }

static eAtRet Enable(AtChannel self, eBool enable)
    {
    mThis(self)->enable = enable;
    return cAtOk;
    }

static eBool IsEnabled(AtChannel self)
    {
    return mThis(self)->enable;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210061SgmiiEthPort *object = (tTha60210061SgmiiEthPort *)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObjectDescription(bypassPort);
    mEncodeObjectDescription(bypassPtch);
    mEncodeUInt(enable);
    mEncodeObject(prbsEngine);
    }

static eAtRet AllCountersClear(AtChannel self, void *pAllCounters)
    {
    return Tha60210061EthPort1GAllCountersReadToClear(self, pAllCounters, cAtTrue);
    }

static eAtRet AllCountersGet(AtChannel self, void *pAllCounters)
    {
    return Tha60210061EthPort1GAllCountersReadToClear(self, pAllCounters, cAtFalse);
    }

static void OverrideAtObject(AtEthPort self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtEthPort(AtEthPort self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEthPortMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPortOverride, mMethodsGet(self), sizeof(m_AtEthPortOverride));

        mMethodOverride(m_AtEthPortOverride, SerdesController);
        mMethodOverride(m_AtEthPortOverride, RxSerdesSelect);
        mMethodOverride(m_AtEthPortOverride, RxSelectedSerdes);
        mMethodOverride(m_AtEthPortOverride, TxSerdesSet);
        mMethodOverride(m_AtEthPortOverride, TxSerdesGet);
        mMethodOverride(m_AtEthPortOverride, TxSerdesCanBridge);
        mMethodOverride(m_AtEthPortOverride, TxBridgedSerdes);
        mMethodOverride(m_AtEthPortOverride, TxSerdesBridge);
        }

    mMethodsSet(self, &m_AtEthPortOverride);
    }

static void OverrideAtChannel(AtEthPort self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, HwIdGet);
        mMethodOverride(m_AtChannelOverride, CounterIsSupported);
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, IsEnabled);
        mMethodOverride(m_AtChannelOverride, PrbsEngineSet);
        mMethodOverride(m_AtChannelOverride, PrbsEngineGet);
        mMethodOverride(m_AtChannelOverride, AllCountersClear);
        mMethodOverride(m_AtChannelOverride, AllCountersGet);
        mMethodOverride(m_AtChannelOverride, PrbsEngineIsCreated);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideThaEthPort(AtEthPort self)
    {
    ThaEthPort port = (ThaEthPort)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaEthPortMethods = mMethodsGet(port);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthPortOverride, m_ThaEthPortMethods, sizeof(m_ThaEthPortOverride));

        mMethodOverride(m_ThaEthPortOverride, FcsByPassed);
        mMethodOverride(m_ThaEthPortOverride, MacActivate);
        }

    mMethodsSet(port, &m_ThaEthPortOverride);
    }

static void Override(AtEthPort self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtEthPort(self);
    OverrideThaEthPort(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061SgmiiEthPort);
    }

static AtEthPort ObjectInit(AtEthPort self, uint8 portId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaEthPortObjectInit(self, portId, module) == NULL)
        return NULL;

    /* Initialize method */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEthPort Tha60210061SgmiiEthPortNew(uint8 portId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthPort newPort = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPort == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPort, portId, module);
    }

uint16 Tha60210061EthPortSW2HwCounterId(AtEthPort self, uint16 counterType)
    {
    AtUnused(self);
    switch (counterType)
        {
        case cAtEthPortCounterTxPackets             : return 0;
        case cAtEthPortCounterTxBytes               : return 1;
        case cAtEthPortCounterTxPacketsLen0_64      : return 4;
        case cAtEthPortCounterTxPacketsLen65_127    : return 5;
        case cAtEthPortCounterTxPacketsLen128_255   : return 6;
        case cAtEthPortCounterTxPacketsLen256_511   : return 7;
        case cAtEthPortCounterTxPacketsLen512_1024  : return 8;
        case cAtEthPortCounterTxPacketsLen1025_1528 : return 9;
        case cAtEthPortCounterTxPacketsLen1529_2047 : return 10;
        case cAtEthPortCounterTxPacketsJumbo        : return 11;
        case cAtEthPortCounterRxPackets             : return 16;
        case cAtEthPortCounterRxBytes               : return 17;
        case cAtEthPortCounterRxPacketsLen0_64      : return 20;
        case cAtEthPortCounterRxPacketsLen65_127    : return 21;
        case cAtEthPortCounterRxPacketsLen128_255   : return 22;
        case cAtEthPortCounterRxPacketsLen256_511   : return 23;
        case cAtEthPortCounterRxPacketsLen512_1024  : return 24;
        case cAtEthPortCounterRxPacketsLen1025_1528 : return 25;
        case cAtEthPortCounterRxPacketsLen1529_2047 : return 26;
        case cAtEthPortCounterRxPacketsJumbo        : return 27;
        case cAtEthPortCounterRxBrdCastPackets      : return 28;
        case cAtEthPortCounterRxMultCastPackets     : return 29;
        case cAtEthPortCounterRxUniCastPackets      : return 30;
        case cAtEthPortCounterRxErrPsnPackets       : return 31;
        case cAtEthPortCounterRxErrFcsPackets       : return 12;
        case cAtEthPortCounterRxOversizePackets     : return 13;
        case cAtEthPortCounterRxUndersizePackets    : return 14;
        default:
            return cInvalidUint16;
        }
    }

eBool Tha60210061EthPort1GCounterIsSupported(AtChannel self, uint16 counterType)
    {
    eAtEthPortCounterType typeCounter = counterType;

    AtUnused(self);
    if ((typeCounter == cAtEthPortCounterTxPackets)             ||
        (typeCounter == cAtEthPortCounterTxBytes)               ||
        (typeCounter == cAtEthPortCounterTxPacketsLen0_64)      ||
        (typeCounter == cAtEthPortCounterTxPacketsLen65_127)    ||
        (typeCounter == cAtEthPortCounterTxPacketsLen128_255)   ||
        (typeCounter == cAtEthPortCounterTxPacketsLen256_511)   ||
        (typeCounter == cAtEthPortCounterTxPacketsLen512_1024)  ||
        (typeCounter == cAtEthPortCounterTxPacketsLen1025_1528) ||
        (typeCounter == cAtEthPortCounterTxPacketsLen1529_2047) ||
        (typeCounter == cAtEthPortCounterTxPacketsJumbo)        ||
        (typeCounter == cAtEthPortCounterRxPackets)             ||
        (typeCounter == cAtEthPortCounterRxBytes)               ||
        (typeCounter == cAtEthPortCounterRxPacketsLen0_64)      ||
        (typeCounter == cAtEthPortCounterRxPacketsLen65_127)    ||
        (typeCounter == cAtEthPortCounterRxPacketsLen128_255)   ||
        (typeCounter == cAtEthPortCounterRxPacketsLen256_511)   ||
        (typeCounter == cAtEthPortCounterRxPacketsLen512_1024)  ||
        (typeCounter == cAtEthPortCounterRxPacketsLen1025_1528) ||
        (typeCounter == cAtEthPortCounterRxPacketsLen1529_2047) ||
        (typeCounter == cAtEthPortCounterRxPacketsJumbo)        ||
        (typeCounter == cAtEthPortCounterRxOversizePackets)     ||
        (typeCounter == cAtEthPortCounterRxUndersizePackets)    ||
        (typeCounter == cAtEthPortCounterRxUniCastPackets)      ||
        (typeCounter == cAtEthPortCounterRxBrdCastPackets)      ||
        (typeCounter == cAtEthPortCounterRxMultCastPackets)     ||
        (typeCounter == cAtEthPortCounterRxErrPsnPackets)       ||
        (typeCounter == cAtEthPortCounterRxErrFcsPackets))
        return cAtTrue;

    return cAtFalse;
    }

eAtRet Tha60210061EthPort1GAllCountersReadToClear(AtChannel self, void* counters, eBool read2Clear)
    {
    tAtEthPortCounters* portCounters = (tAtEthPortCounters*)counters;
    uint32 (*counterGetFunc)(AtChannel self, uint16 counterType);

    if (portCounters == NULL)
        return cAtErrorNullPointer;

    counterGetFunc = (read2Clear) ? mMethodsGet(self)->CounterClear : mMethodsGet(self)->CounterGet;
    portCounters->txPackets             = counterGetFunc(self, cAtEthPortCounterTxPackets);
    portCounters->txBytes               = counterGetFunc(self, cAtEthPortCounterTxBytes);
    portCounters->txPacketsLen0_64      = counterGetFunc(self, cAtEthPortCounterTxPacketsLen0_64);
    portCounters->txPacketsLen65_127    = counterGetFunc(self, cAtEthPortCounterTxPacketsLen65_127);
    portCounters->txPacketsLen128_255   = counterGetFunc(self, cAtEthPortCounterTxPacketsLen128_255);
    portCounters->txPacketsLen256_511   = counterGetFunc(self, cAtEthPortCounterTxPacketsLen256_511);
    portCounters->txPacketsLen512_1024  = counterGetFunc(self, cAtEthPortCounterTxPacketsLen512_1024);
    portCounters->txPacketsLen1025_1528 = counterGetFunc(self, cAtEthPortCounterTxPacketsLen1025_1528);
    portCounters->txPacketsLen1529_2047 = counterGetFunc(self, cAtEthPortCounterTxPacketsLen1529_2047);
    portCounters->txPacketsJumbo        = counterGetFunc(self, cAtEthPortCounterTxPacketsJumbo);

    portCounters->rxPackets             = counterGetFunc(self, cAtEthPortCounterRxPackets);
    portCounters->rxBytes               = counterGetFunc(self, cAtEthPortCounterRxBytes);
    portCounters->rxPacketsLen0_64      = counterGetFunc(self, cAtEthPortCounterRxPacketsLen0_64);
    portCounters->rxPacketsLen65_127    = counterGetFunc(self, cAtEthPortCounterRxPacketsLen65_127);
    portCounters->rxPacketsLen128_255   = counterGetFunc(self, cAtEthPortCounterRxPacketsLen128_255);
    portCounters->rxPacketsLen256_511   = counterGetFunc(self, cAtEthPortCounterRxPacketsLen256_511);
    portCounters->rxPacketsLen512_1024  = counterGetFunc(self, cAtEthPortCounterRxPacketsLen512_1024);
    portCounters->rxPacketsLen1025_1528 = counterGetFunc(self, cAtEthPortCounterRxPacketsLen1025_1528);
    portCounters->rxPacketsLen1529_2047 = counterGetFunc(self, cAtEthPortCounterRxPacketsLen1529_2047);
    portCounters->rxPacketsJumbo        = counterGetFunc(self, cAtEthPortCounterRxPacketsJumbo);
    portCounters->rxOversizePackets     = counterGetFunc(self, cAtEthPortCounterRxOversizePackets);
    portCounters->rxUndersizePackets    = counterGetFunc(self, cAtEthPortCounterRxUndersizePackets);
    portCounters->rxUniCastPackets      = counterGetFunc(self, cAtEthPortCounterRxUniCastPackets);
    portCounters->rxBrdCastPackets      = counterGetFunc(self, cAtEthPortCounterRxBrdCastPackets);
    portCounters->rxMultCastPackets     = counterGetFunc(self, cAtEthPortCounterRxMultCastPackets);
    portCounters->rxErrPsnPackets       = counterGetFunc(self, cAtEthPortCounterRxErrPsnPackets);
    portCounters->rxErrFcsPackets       = counterGetFunc(self, cAtEthPortCounterRxErrFcsPackets);

    return cAtOk;
    }

void Tha60210061SgmiiEthPortByPassPortCache(AtEthPort self, AtEthPort bypassPort)
    {
    if (self)
        mThis(self)->bypassPort = bypassPort;
    }

AtEthPort Tha60210061SgmiiEthPortByPassPort(AtEthPort self)
    {
    if (self)
        return mThis(self)->bypassPort;

    return NULL;
    }

AtPtchService Tha60210061SgmiiEthPassthroughPtchGet(AtEthPort self)
    {
    if (self == NULL)
        return NULL;

    return mThis(self)->bypassPtch;
    }

AtPtchService Tha60210061SgmiiEthPassthroughPtchCreate(AtEthPort self)
    {
    uint16 defaultPtch;

    if (self == NULL)
        return NULL;

    if (mThis(self)->bypassPtch)
        return mThis(self)->bypassPtch;

    mThis(self)->bypassPtch = Tha60210061EthPassthoughPtchNew(self);
    if (mThis(self)->bypassPtch == NULL)
        return NULL;

    defaultPtch = DefaultPtch(self);
    AtPtchServiceEgressPtchSet(mThis(self)->bypassPtch, defaultPtch);
    AtPtchServiceIngressPtchSet(mThis(self)->bypassPtch, (uint16)(defaultPtch << 8));

    return mThis(self)->bypassPtch;
    }


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Ethernet
 *
 * File        : Tha60210061QsgmiiEthPort.c
 *
 * Created Date: Sep 7, 2016
 *
 * Description : QSGMII Ethernet port
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../Tha60210012/eth/Tha60210012EthPortInternal.h"
#include "../physical/Tha60210061SerdesManager.h"
#include "../physical/Tha60210061TopCtrReg.h"
#include "Tha60210061ModuleEth.h"
#include "Tha60210061EthPort.h"
#include "Tha60210061QgmiiLaneEthPort.h"
#include "Tha60210061SgmiiEthPort.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210061QsgmiiEthPort*)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061QsgmiiEthPort
    {
    tTha60210012EthPort super;

    AtSerdesController txSerdes;
    }tTha60210061QsgmiiEthPort;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods       m_AtObjectOverride;
static tAtChannelMethods      m_AtChannelOverride;
static tAtEthPortMethods      m_AtEthPortOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;
static const tAtEthPortMethods *m_AtEthPortMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleEth ModuleEth(AtEthPort self)
    {
    return (ThaModuleEth)AtChannelModuleGet((AtChannel)self);
    }

static AtSerdesController SerdesController(AtEthPort self)
    {
    ThaModuleEth ethModule = ModuleEth(self);
    uint32 portId = AtChannelHwIdGet((AtChannel)self);
    uint32 serdesId = mMethodsGet(ethModule)->EthPortIdToSerdesId(ethModule, portId);

    return AtModuleEthSerdesController((AtModuleEth)ethModule, serdesId);
    }

static eAtModuleEthRet RxSerdesSelect(AtEthPort self, AtSerdesController serdes)
    {
    uint32 regVal, serdesId;
    uint32 address = cAf6Reg_o_control1 + cTha60210061TopBaseAddress;

    serdesId = AtSerdesControllerIdGet(serdes);
    regVal = mChannelHwRead(self, address, cAtModuleEth);

    if (serdesId == cTha60210061QsgmiiSerdesId0)
        mRegFieldSet(regVal, cThaReg_QSGMII_Mac0_Rx_Sel_, 0x0);

    else if (serdesId == cTha60210061QsgmiiSerdesId1)
        mRegFieldSet(regVal, cThaReg_QSGMII_Mac0_Rx_Sel_, 0x1);

    else
        return cAtErrorModeNotSupport;

    mChannelHwWrite(self, address, regVal, cAtModuleEth);

    return cAtOk;
    }

static AtSerdesController RxSelectedSerdes(AtEthPort self)
    {
    uint32 regVal, hwValue;
    ThaModuleEth ethModule = ModuleEth(self);
    uint32 address = cAf6Reg_o_control1 + cTha60210061TopBaseAddress;

    regVal = mChannelHwRead(self, address, cAtModuleEth);
    hwValue = mRegField(regVal, cThaReg_QSGMII_Mac0_Rx_Sel_);

    if (hwValue == 0x0)
        return AtModuleEthSerdesController((AtModuleEth)ethModule, cTha60210061QsgmiiSerdesId0);

    if (hwValue == 0x1)
        return AtModuleEthSerdesController((AtModuleEth)ethModule, cTha60210061QsgmiiSerdesId1);

    return NULL;
    }

static eAtModuleEthRet TxSerdesHwBridge(AtEthPort self, AtSerdesController serdes)
    {
    AtUnused(self);
    return AtSerdesControllerEnable(serdes, cAtTrue);
    }

static eAtModuleEthRet TxSerdesBridgeHwRelease(AtEthPort self, AtSerdesController serdes)
    {
    AtUnused(self);
    return AtSerdesControllerEnable(serdes, cAtFalse);
    }

static eBool SerdesIsValid(AtSerdesController serdes)
    {
    uint32 serdesId = AtSerdesControllerIdGet(serdes);

    if ((serdesId == cTha60210061QsgmiiSerdesId0) ||
        (serdesId == cTha60210061QsgmiiSerdesId1))
        return cAtTrue;

    return cAtFalse;
    }

static AtSerdesController PartnerSerdes(AtEthPort self, AtSerdesController serdes)
    {
    return Tha60210061ModuleEthPartnerSerdes((AtModuleEth)ModuleEth(self), serdes);
    }

static eAtModuleEthRet TxSerdesBridge(AtEthPort self, AtSerdesController serdes)
    {
    AtSerdesController bridgedSerdes = AtEthPortTxBridgedSerdes(self);
    if (serdes == bridgedSerdes)
        return cAtOk;

    if (serdes)
        {
        if (!SerdesIsValid(serdes))
            return cAtErrorModeNotSupport;

        return TxSerdesHwBridge(self, serdes);
        }

    return TxSerdesBridgeHwRelease(self, bridgedSerdes);
    }

static AtSerdesController TxBridgedSerdes(AtEthPort self)
    {
    AtSerdesController txSerdes = AtEthPortTxSerdesGet(self);
    AtSerdesController partnerSerdes;

    /* No tx, no bridge */
    if (txSerdes == NULL)
        return NULL;

    partnerSerdes = PartnerSerdes(self, txSerdes);
    if (AtSerdesControllerIsEnabled(partnerSerdes))
        return partnerSerdes;

    return NULL;
    }

static eBool TxSerdesCanBridge(AtEthPort self, AtSerdesController serdes)
    {
    return Tha60210061TxSerdesCanBridge(self, serdes);
    }

static eAtModuleEthRet TxSerdesSet(AtEthPort self, AtSerdesController serdes)
    {
    eAtRet ret = cAtOk;
    AtSerdesController txSerdes = AtEthPortTxSerdesGet(self);
    if (serdes == txSerdes)
        return cAtOk;

    if (!SerdesIsValid(serdes))
        return cAtErrorModeNotSupport;

    if (txSerdes)
        ret = TxSerdesBridgeHwRelease(self, txSerdes);

    ret |= TxSerdesHwBridge(self, serdes);
    if (ret == cAtOk)
        mThis(self)->txSerdes = serdes;

    return cAtOk;
    }

static AtSerdesController TxSerdesGet(AtEthPort self)
    {
    return mThis(self)->txSerdes;
    }

static AtEthPort SubPortObjectCreate(AtEthPort self, uint8 subPortId)
    {
    return Tha60210061QsgmiiLaneEthPortNew((uint8)subPortId, (AtModuleEth)ModuleEth(self));
    }

static uint32 MaxSubPortsGet(AtEthPort self)
    {
    ThaModuleEth moduleEth = ModuleEth(self);

    if (Tha60210061EthPassThroughIsSupported((AtModuleEth)moduleEth))
        return ThaModuleEthQsgmiiSerdesNumLanes(moduleEth, cTha60210061QsgmiiSerdesId0);
    return 0;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = AtEthPortAllSubPortsInit((AtEthPort)self);

    if (ret != cAtOk)
        return ret;

    AtChannelEnable(self, cAtTrue);

    return cAtOk;
    }

static eAtRet LoopbackSet(AtChannel self, uint8 loopbackMode)
    {
    AtUnused(self);
    return (loopbackMode == cAtLoopbackModeRelease) ? cAtOk : cAtErrorModeNotSupport;
    }

static uint8 LoopbackGet(AtChannel self)
    {
    AtUnused(self);
    return cAtLoopbackModeRelease;
    }

static eBool LoopbackIsSupported(AtChannel self, uint8 loopbackMode)
    {
    AtUnused(self);
    return (loopbackMode == cAtLoopbackModeRelease) ? cAtTrue : cAtFalse;
    }

static eAtRet AllSubPortsEnable(AtEthPort self, eBool enable)
    {
    eAtRet ret = cAtOk;
    uint32 numSubPorts = AtEthPortMaxSubPortsGet(self);
    uint32 port_i;

    for (port_i = 0; port_i < numSubPorts; port_i++)
        {
        AtEthPort subPort = AtEthPortSubPortGet(self, port_i);
        ret |= AtChannelEnable((AtChannel)subPort, enable);
        }

    return ret;
    }

static eAtRet Enable(AtChannel self, eBool enable)
    {
    AtSerdesController serdes = AtEthPortSerdesController((AtEthPort)self);

    if (serdes)
        AtSerdesControllerEnable(serdes, enable);
    return AllSubPortsEnable((AtEthPort)self, enable);
    }

static eBool IsEnabled(AtChannel self)
    {
    if (AtEthPortMaxSubPortsGet((AtEthPort)self))
        return AtChannelIsEnabled((AtChannel)AtEthPortSubPortGet((AtEthPort)self, 0));
    return AtSerdesControllerIsEnabled(AtEthPortSerdesController((AtEthPort)self));
    }

static uint32 CounterRead2Clear(AtChannel self, uint16 counterType, eBool read2Clear)
    {
    AtEthPort port = (AtEthPort)self;
    uint32 numSubPorts = AtEthPortMaxSubPortsGet(port);
    uint32 port_i;
    uint32 counter = 0;
    uint32 (*CounterGet)(AtChannel self, uint16 counterType) = NULL;

    CounterGet = read2Clear ? AtChannelCounterGet : AtChannelCounterClear;

    for (port_i = 0; port_i < numSubPorts; port_i++)
        {
        AtChannel subPort = (AtChannel)AtEthPortSubPortGet(port, port_i);
        counter += CounterGet(subPort, counterType);
        }

    return counter;
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtFalse);
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtTrue);
    }

static eBool CounterIsSupported(AtChannel self, uint16 counterType)
    {
    return Tha60210061EthPort1GCounterIsSupported(self, counterType);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210061QsgmiiEthPort *object = (tTha60210061QsgmiiEthPort *)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObjectDescription(txSerdes);
    }

static void OverrideAtEthPort(AtEthPort self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEthPortMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPortOverride, mMethodsGet(self), sizeof(m_AtEthPortOverride));

        mMethodOverride(m_AtEthPortOverride, SerdesController);
        mMethodOverride(m_AtEthPortOverride, RxSerdesSelect);
        mMethodOverride(m_AtEthPortOverride, RxSelectedSerdes);
        mMethodOverride(m_AtEthPortOverride, TxSerdesSet);
        mMethodOverride(m_AtEthPortOverride, TxSerdesGet);
        mMethodOverride(m_AtEthPortOverride, TxSerdesCanBridge);
        mMethodOverride(m_AtEthPortOverride, TxBridgedSerdes);
        mMethodOverride(m_AtEthPortOverride, TxSerdesBridge);
        mMethodOverride(m_AtEthPortOverride, MaxSubPortsGet);
        mMethodOverride(m_AtEthPortOverride, SubPortObjectCreate);
        }

    mMethodsSet(self, &m_AtEthPortOverride);
    }

static void OverrideAtChannel(AtEthPort self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, LoopbackSet);
        mMethodOverride(m_AtChannelOverride, LoopbackGet);
        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, IsEnabled);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, CounterIsSupported);
        mMethodOverride(m_AtChannelOverride, LoopbackIsSupported);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtObject(AtEthPort self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtEthPort self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtEthPort(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061QsgmiiEthPort);
    }

static AtEthPort ObjectInit(AtEthPort self, uint8 portId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012EthPortObjectInit((AtEthPort)self, portId, module) == NULL)
        return NULL;

    /* Initialize method */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEthPort Tha60210061QsgmiiEthPortNew(uint8 portId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthPort newPort = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPort == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPort, portId, module);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet Analyzer
 *
 * File        : Tha60210061EthPortPktAnalyzer.c
 *
 * Created Date: Dec 9, 2016
 *
 * Description : Port Packet Analyzer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210012/pktanalyzer/Tha60210012EthPortPktAnalyzerInternal.h"
#include "../../Tha60210012/pktanalyzer/Tha60210012InternalPktAnalyzer.h"
#include "../../Tha60210012/eth/Tha60210012EthPort.h"
#include "../eth/Tha60210061ModuleEth.h"
#include "Tha60210061EthPortPktAnalyzer.h"
#include "Tha60210061EthPacket.h"

/*--------------------------- Define -----------------------------------------*/

#define cAf6ChannelIdSelectMask     cBit7_4
#define cAf6ChannelIdSelectShift    4

#define cAf6ChannelIdDumpModeMask  cBit0
#define cAf6ChannelIdDumpModeShift 0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061EthPortPktAnalyzer
    {
    tTha60210012EthPortPktAnalyzer super;
    }tTha60210061EthPortPktAnalyzer;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPktAnalyzerMethods m_ThaPktAnalyzerOverride;
static tAtPktAnalyzerMethods m_AtPktAnalyzerOverride;

/* Supper Implementation */
static const tThaPktAnalyzerMethods *m_ThaPktAnalyzerMethods = NULL;
static const tAtPktAnalyzerMethods *m_AtPktAnalyzerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 StartAddress(ThaPktAnalyzer self)
    {
    AtUnused(self);
    return 0x4F000;
    }

static AtPacket Port10GPacketCreate(AtPktAnalyzer self, uint8 *data, uint32 length, eAtPktAnalyzerDirection direction)
    {
    AtEthPort port = (AtEthPort)AtPktAnalyzerChannelGet(self);

    if (!Tha60210012EthPortPtchIsEnabled(port))
        return Tha60210061EthPacketNew(data, length, cAtPacketCacheModeCacheData);

    return Tha60210061EthPacketWithPtchNew(data, length, cAtPacketCacheModeCacheData, direction);
    }

static AtPacket PacketCreate(AtPktAnalyzer self, uint8 *data, uint32 length, eAtPktAnalyzerDirection direction)
    {
    AtEthPort port = (AtEthPort)AtPktAnalyzerChannelGet(self);

    /* Port 10G */
    if (Tha60210061ModuleEthPortIs10G(port))
        return Port10GPacketCreate(self, data, length, direction);

    /* Other ports don't have PTCH */
    return Tha60210061EthPacketNew(data, length, cAtPacketCacheModeCacheData);
    }

static uint32 EthPortHwDumpMode(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode)
    {
    AtUnused(self);
    if (pktDumpMode == cThaPktAnalyzerDumpGbeTx)
        return 0;

    /* RX */
    return 1;
    }

static uint32 EthPortDumpModeHw2Sw(ThaPktAnalyzer self, uint32 pktDumpMode)
    {
    AtUnused(self);
    if (pktDumpMode == 0)
        return cThaPktAnalyzerDumpGbeTx;

    return cThaPktAnalyzerDumpGbeRx;
    }

static void ChannelIdSet(ThaPktAnalyzer self)
    {
    uint32 regAddr = cThaDiagPktDumpModeCtrl + ThaPktAnalyzerBaseAddress(self);
    uint32 regVal  = ThaPktAnalyzerRead(self, regAddr);
    uint32 channelId = AtChannelHwIdGet(AtPktAnalyzerChannelGet((AtPktAnalyzer)self));

    mRegFieldSet(regVal, cAf6ChannelIdSelect, channelId);
    ThaPktAnalyzerWrite(self, regAddr, regVal);
    }

static void Init(AtPktAnalyzer self)
    {
    ThaPktAnalyzer analyzer = (ThaPktAnalyzer)self;

    m_AtPktAnalyzerMethods->Init(self);
    mMethodsGet(analyzer)->ChannelIdSet(analyzer);
    AtPktAnalyzerPacketLengthSet(self, 2048);	
    }

static uint32 DumpModeSw2Hw(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode)
    {
    if (pktDumpMode == cThaPktAnalyzerDumpGbeTx)
        return mMethodsGet(self)->EthPortHwDumpMode(self, cThaPktAnalyzerDumpGbeTx);

    return mMethodsGet(self)->EthPortHwDumpMode(self, cThaPktAnalyzerDumpGbeRx);
    }

static uint8 PacketLengthModeSw2Hw(uint32 packetLength)
    {
    if (packetLength == 0)    return 0; /* Full packet */
    if (packetLength <= 32)   return 1;
    if (packetLength <= 64)   return 2;
    if (packetLength <= 128)  return 3;
    if (packetLength <= 256)  return 4;
    if (packetLength <= 512)  return 5;
    if (packetLength <= 1024) return 6;

    /* Default dump 2048 bytes */
    return 7;
    }

static void DumpModeSet(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode)
    {
    uint32 regAddr = cThaDiagPktDumpModeCtrl + ThaPktAnalyzerBaseAddress(self);
    uint32 regVal  = ThaPktAnalyzerRead(self, regAddr);

    mRegFieldSet(regVal, cAf6ChannelIdDumpMode, DumpModeSw2Hw(self, pktDumpMode));
    ThaPktAnalyzerWrite(self, regAddr, regVal);
    }

static uint32 DumpModeGet(ThaPktAnalyzer self)
    {
    uint32 regAddr = cThaDiagPktDumpModeCtrl + ThaPktAnalyzerBaseAddress(self);
    uint32 regVal  = ThaPktAnalyzerRead(self, regAddr);

    return mMethodsGet(self)->EthPortDumpModeHw2Sw(self, mRegField(regVal, cAf6ChannelIdDumpMode));
    }

static void PacketLengthSet(AtPktAnalyzer self, uint32 packetLength)
    {
    ThaPktAnalyzer analyzer = (ThaPktAnalyzer)self;
    uint32 regAddr = cThaDiagPktDumpModeCtrl + ThaPktAnalyzerBaseAddress(analyzer);
    uint32 regVal = ThaPktAnalyzerRead(analyzer, regAddr);

    mRegFieldSet(regVal, cThaDebugDumPktCtlLen, PacketLengthModeSw2Hw(packetLength));
    ThaPktAnalyzerWrite(analyzer, regAddr, regVal);
    }

static void OverrideThaPktAnalyzer(AtPktAnalyzer self)
    {
    ThaPktAnalyzer analyzer = (ThaPktAnalyzer)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPktAnalyzerMethods = mMethodsGet(analyzer);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPktAnalyzerOverride, mMethodsGet(analyzer), sizeof(m_ThaPktAnalyzerOverride));

        mMethodOverride(m_ThaPktAnalyzerOverride, StartAddress);
        mMethodOverride(m_ThaPktAnalyzerOverride, ChannelIdSet);
        mMethodOverride(m_ThaPktAnalyzerOverride, EthPortHwDumpMode);
        mMethodOverride(m_ThaPktAnalyzerOverride, EthPortDumpModeHw2Sw);
        mMethodOverride(m_ThaPktAnalyzerOverride, DumpModeSet);
        mMethodOverride(m_ThaPktAnalyzerOverride, DumpModeGet);
        }

    mMethodsSet(analyzer, &m_ThaPktAnalyzerOverride);
    }

static void OverrideAtPktAnalyzer(AtPktAnalyzer self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPktAnalyzerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPktAnalyzerOverride, mMethodsGet(self), sizeof(m_AtPktAnalyzerOverride));

        mMethodOverride(m_AtPktAnalyzerOverride, PacketCreate);
        mMethodOverride(m_AtPktAnalyzerOverride, Init);
        mMethodOverride(m_AtPktAnalyzerOverride, PacketLengthSet);
        }

    mMethodsSet(self, &m_AtPktAnalyzerOverride);
    }

static void Override(AtPktAnalyzer self)
    {
    OverrideThaPktAnalyzer(self);
    OverrideAtPktAnalyzer(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061EthPortPktAnalyzer);
    }

static AtPktAnalyzer ObjectInit(AtPktAnalyzer self, AtEthPort port)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012EthPortPktAnalyzerObjectInit(self, port) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPktAnalyzer Tha60210061EthPortPktAnalyzerNew(AtEthPort port)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtPktAnalyzer newPktAnalyzer = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPktAnalyzer == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPktAnalyzer, port);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet Analyzer
 * 
 * File        : Tha60210061EthPortPktAnalyzer.h
 * 
 * Created Date: Dec 9, 2016
 *
 * Description : Packet Analyzer
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061ETHPORTPKTANALYZER_H_
#define _THA60210061ETHPORTPKTANALYZER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPktAnalyzer Tha60210061EthPortPktAnalyzerNew(AtEthPort port);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061ETHPORTPKTANALYZER_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet Analyzer
 * 
 * File        : Tha60210061ModulePktAnalyzer.h
 * 
 * Created Date: Dec 9, 2016
 *
 * Description : Packet Analyzer
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061MODULEPKTANALYZER_H_
#define _THA60210061MODULEPKTANALYZER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePktAnalyzer Tha60210061ModulePktAnalyzerNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061MODULEPKTANALYZER_H_ */


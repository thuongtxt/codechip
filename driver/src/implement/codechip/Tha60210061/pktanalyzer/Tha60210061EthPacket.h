/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet Analyzer
 * 
 * File        : Tha60210061EthPacketWithPtch.h
 * 
 * Created Date: Feb 6, 2017
 *
 * Description : 60210061 Ethernet packet
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061ETHPACKET_H_
#define _THA60210061ETHPACKET_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPacket Tha60210061EthPacketNew(uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode);
AtPacket Tha60210061EthPacketWithPtchNew(uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPktAnalyzerDirection direction);

#endif /* _THA60210061ETHPACKET_H_ */


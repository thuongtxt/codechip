/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : BER
 * 
 * File        : Tha60210061PdhDe1BerControllerInternal.h
 * 
 * Created Date: Mar 17, 2017
 *
 * Description : BER controller of LIU PDH DE1 path.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061PDHDE1BERCONTROLLERINTERNAL_H_
#define _THA60210061PDHDE1BERCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/ber/Tha60210011SdhAuVcBerControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210061PdhDe1PathBerController * Tha60210061PdhDe1PathBerController;
typedef struct tTha60210061PdhDe1PathBerController
    {
    tTha60210011PdhDe1BerController super;

    /* Private data */
    eBool isLineLayer;
    }tTha60210061PdhDe1PathBerController;

typedef struct tTha60210061PdhDe1LineBerController
    {
    tTha60210061PdhDe1PathBerController super;
    }tTha60210061PdhDe1LineBerController;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtBerController Tha60210061PdhDe1PathBerControllerObjectInit(AtBerController self, uint32 controllerId, AtChannel channel, AtModuleBer berModule);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061PDHDE1BERCONTROLLERINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : Tha60210061PdhDe1LineBerController.c
 *
 * Created Date: Mar 17, 2017
 *
 * Description : BER controller of LIU PDH DE1 path.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210061PdhDe1BerControllerInternal.h"
#include "Tha60210061ModuleBer.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061PdhDe1LineBerController);
    }

static AtBerController ObjectInit(AtBerController self, uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210061PdhDe1PathBerControllerObjectInit(self, controllerId, channel, berModule) == NULL)
        return NULL;

    /* Setup class */
    ((Tha60210061PdhDe1PathBerController)self)->isLineLayer = cAtTrue;

    return self;
    }

AtBerController Tha60210061PdhDe1LineBerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtBerController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, controllerId, channel, berModule);
    }

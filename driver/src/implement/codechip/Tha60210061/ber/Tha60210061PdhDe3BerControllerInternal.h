/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : BER
 * 
 * File        : Tha60210061PdhDe3BerControllerInternal.h
 * 
 * Created Date: Mar 17, 2017
 *
 * Description : BER controller of LIU PDH DE3 path.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061PDHDE3BERCONTROLLERINTERNAL_H_
#define _THA60210061PDHDE3BERCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/ber/Tha60210011SdhAuVcBerControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210061PdhDe3PathBerController * Tha60210061PdhDe3PathBerController;
typedef struct tTha60210061PdhDe3PathBerController
    {
    tTha60210011SdhTu3VcBerController super;

    /* Private data */
    eBool isLineLayer;
    }tTha60210061PdhDe3PathBerController;

typedef struct tTha60210061PdhDe3LineBerController
    {
    tTha60210061PdhDe3PathBerController super;
    }tTha60210061PdhDe3LineBerController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtBerController Tha60210061PdhDe3PathBerObjectInit(AtBerController self, uint32 controllerId, AtChannel channel, AtModuleBer berModule);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061PDHDE3BERCONTROLLERINTERNAL_H_ */


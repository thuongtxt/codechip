/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CCI0061_RD_POH_BER_H_
#define _AF6_REG_AF6CCI0061_RD_POH_BER_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : POH Hold Register
Reg Addr   : 0x00_000A
Reg Formula: 0x00_000A + holdnum
    Where  : 
           + $holdnum(0-1): Hold register number
Reg Desc   : 
This register is used for access long register of Ber, Message.

------------------------------------------------------------------------------*/
#define cAf6Reg_holdreg_Base                                                                          0x00000A

/*--------------------------------------
BitField Name: holdvalue
BitField Type: RW
BitField Desc: Hold value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_holdreg_holdvalue_Mask                                                                   cBit31_0
#define cAf6_holdreg_holdvalue_Shift                                                                         0


/*------------------------------------------------------------------------------
Reg Name   : POH Hold Register
Reg Addr   : 0x01_000A
Reg Formula: 0x01_000A + holdnum
    Where  : 
           + $holdnum(0-1): Hold register number
Reg Desc   : 
This register is used for access long register of Grabber.

------------------------------------------------------------------------------*/
#define cAf6Reg_holdregclk2_Base                                                                      0x01000A

/*--------------------------------------
BitField Name: holdvalue
BitField Type: RW
BitField Desc: Hold value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_holdregclk2_holdvalue_Mask                                                               cBit31_0
#define cAf6_holdregclk2_holdvalue_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : POH Hold Indirect Control Register
Reg Addr   : 0x00_000C
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used for access long register of Ber, Message.

------------------------------------------------------------------------------*/
#define cAf6Reg_holdindctlreg_Base                                                                    0x00000C

/*--------------------------------------
BitField Name: holdvalue
BitField Type: RW
BitField Desc: Hold value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_holdindctlreg_holdvalue_Mask                                                             cBit31_0
#define cAf6_holdindctlreg_holdvalue_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : POH Hold Indirect Data Register
Reg Addr   : 0x00_000D
Reg Formula: 0x00_000D + holdnum
    Where  : 
           + $holdnum(0-2): Hold register number
Reg Desc   : 
This register is used for access long register of Ber, Message.

------------------------------------------------------------------------------*/
#define cAf6Reg_holdinddatreg_Base                                                                    0x00000D

/*--------------------------------------
BitField Name: holdvalue
BitField Type: RW
BitField Desc: Hold value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_holdinddatreg_holdvalue_Mask                                                             cBit31_0
#define cAf6_holdinddatreg_holdvalue_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : POH Hold Indirect Control Register
Reg Addr   : 0x01_000C
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used for access long register of Grabber.

------------------------------------------------------------------------------*/
#define cAf6Reg_holdindctlreg2_Base                                                                   0x01000C

/*--------------------------------------
BitField Name: holdvalue
BitField Type: RW
BitField Desc: Hold value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_holdindctlreg2_holdvalue_Mask                                                            cBit31_0
#define cAf6_holdindctlreg2_holdvalue_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : POH Hold Indirect Data Register
Reg Addr   : 0x01_000D
Reg Formula: 0x01_000D + holdnum
    Where  : 
           + $holdnum(0-2): Hold register number
Reg Desc   : 
This register is used for access long register of Grabber.

------------------------------------------------------------------------------*/
#define cAf6Reg_holdinddatreg2_Base                                                                   0x01000D

/*--------------------------------------
BitField Name: holdvalue
BitField Type: RW
BitField Desc: Hold value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_holdinddatreg2_holdvalue_Mask                                                            cBit31_0
#define cAf6_holdinddatreg2_holdvalue_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : POH Global Parity Force
Reg Addr   : 0x00_0010
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to control Parity , Message Jn request.

------------------------------------------------------------------------------*/
#define cAf6Reg_pcfg_glbparfrc_Base                                                                   0x000010

/*--------------------------------------
BitField Name: berparctrlfrc
BitField Type: RW
BitField Desc: Force parity POH BER Control VT/DSN, POH BER Control STS/TU3
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pcfg_glbparfrc_berparctrlfrc_Mask                                                           cBit5
#define cAf6_pcfg_glbparfrc_berparctrlfrc_Shift                                                              5

/*--------------------------------------
BitField Name: berpartrshfrc
BitField Type: RW
BitField Desc: Force parity  POH BER Threshold 2
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pcfg_glbparfrc_berpartrshfrc_Mask                                                           cBit4
#define cAf6_pcfg_glbparfrc_berpartrshfrc_Shift                                                              4

/*--------------------------------------
BitField Name: terparvttu3frc
BitField Type: RW
BitField Desc: Force parity POH Termintate Insert Control VT/TU3
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pcfg_glbparfrc_terparvttu3frc_Mask                                                          cBit3
#define cAf6_pcfg_glbparfrc_terparvttu3frc_Shift                                                             3

/*--------------------------------------
BitField Name: terparstsfrc
BitField Type: RW
BitField Desc: Force parity POH Termintate Insert Control STS
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pcfg_glbparfrc_terparstsfrc_Mask                                                            cBit2
#define cAf6_pcfg_glbparfrc_terparstsfrc_Shift                                                               2

/*--------------------------------------
BitField Name: cpeparvtfrc
BitField Type: RW
BitField Desc: Force parity POH CPE VT Control Register
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pcfg_glbparfrc_cpeparvtfrc_Mask                                                             cBit1
#define cAf6_pcfg_glbparfrc_cpeparvtfrc_Shift                                                                1

/*--------------------------------------
BitField Name: cpeparststu3frc
BitField Type: RW
BitField Desc: Force parity POH CPE STS/TU3 Control Register
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pcfg_glbparfrc_cpeparststu3frc_Mask                                                         cBit0
#define cAf6_pcfg_glbparfrc_cpeparststu3frc_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : POH Global Parity Disable
Reg Addr   : 0x00_0011
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to control Parity , Message Jn request.

------------------------------------------------------------------------------*/
#define cAf6Reg_pcfg_glbpardis_Base                                                                   0x000011

/*--------------------------------------
BitField Name: berparctrldis
BitField Type: RW
BitField Desc: Disable parity POH BER Control VT/DSN, POH BER Control STS/TU3
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pcfg_glbpardis_berparctrldis_Mask                                                           cBit5
#define cAf6_pcfg_glbpardis_berparctrldis_Shift                                                              5

/*--------------------------------------
BitField Name: berpartrshdis
BitField Type: RW
BitField Desc: Disable parity POH BER Threshold 2
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pcfg_glbpardis_berpartrshdis_Mask                                                           cBit4
#define cAf6_pcfg_glbpardis_berpartrshdis_Shift                                                              4

/*--------------------------------------
BitField Name: terparvttu3dis
BitField Type: RW
BitField Desc: Disable parity POH Termintate Insert Control VT/TU3
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pcfg_glbpardis_terparvttu3dis_Mask                                                          cBit3
#define cAf6_pcfg_glbpardis_terparvttu3dis_Shift                                                             3

/*--------------------------------------
BitField Name: terparstsdis
BitField Type: RW
BitField Desc: Disable parity POH Termintate Insert Control STS
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pcfg_glbpardis_terparstsdis_Mask                                                            cBit2
#define cAf6_pcfg_glbpardis_terparstsdis_Shift                                                               2

/*--------------------------------------
BitField Name: cpeparvtdis
BitField Type: RW
BitField Desc: Disable parity POH CPE VT Control Register
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pcfg_glbpardis_cpeparvtdis_Mask                                                             cBit1
#define cAf6_pcfg_glbpardis_cpeparvtdis_Shift                                                                1

/*--------------------------------------
BitField Name: cpeparststu3dis
BitField Type: RW
BitField Desc: Disable parity POH CPE STS/TU3 Control Register
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pcfg_glbpardis_cpeparststu3dis_Mask                                                         cBit0
#define cAf6_pcfg_glbpardis_cpeparststu3dis_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : POH Global Parity Alarm
Reg Addr   : 0x00_0012
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to show alarm of Parity and Debug

------------------------------------------------------------------------------*/
#define cAf6Reg_pcfg_glbparalm_Base                                                                   0x000012

/*--------------------------------------
BitField Name: berparctrlstk
BitField Type: RW
BitField Desc: Parity sticky POH BER Control VT/DSN, POH BER Control STS/TU3
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pcfg_glbparalm_berparctrlstk_Mask                                                           cBit5
#define cAf6_pcfg_glbparalm_berparctrlstk_Shift                                                              5

/*--------------------------------------
BitField Name: berpartrshstk
BitField Type: RW
BitField Desc: Parity sticky  POH BER Threshold 2
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pcfg_glbparalm_berpartrshstk_Mask                                                           cBit4
#define cAf6_pcfg_glbparalm_berpartrshstk_Shift                                                              4

/*--------------------------------------
BitField Name: terparvttu3stk
BitField Type: RW
BitField Desc: Parity sticky POH Termintate Insert Control VT/TU3
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pcfg_glbparalm_terparvttu3stk_Mask                                                          cBit3
#define cAf6_pcfg_glbparalm_terparvttu3stk_Shift                                                             3

/*--------------------------------------
BitField Name: terparstsstk
BitField Type: RW
BitField Desc: Parity sticky POH Termintate Insert Control STS
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pcfg_glbparalm_terparstsstk_Mask                                                            cBit2
#define cAf6_pcfg_glbparalm_terparstsstk_Shift                                                               2

/*--------------------------------------
BitField Name: cpeparvtstk
BitField Type: RW
BitField Desc: Parity sticky POH CPE VT Control Register
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pcfg_glbparalm_cpeparvtstk_Mask                                                             cBit1
#define cAf6_pcfg_glbparalm_cpeparvtstk_Shift                                                                1

/*--------------------------------------
BitField Name: cpeparststu3stk
BitField Type: RW
BitField Desc: Parity sticky POH CPE STS/TU3 Control Register
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pcfg_glbparalm_cpeparststu3stk_Mask                                                         cBit0
#define cAf6_pcfg_glbparalm_cpeparststu3stk_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : POH Global Control
Reg Addr   : 0x00_0000
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to control Parity , Message Jn request.

------------------------------------------------------------------------------*/
#define cAf6Reg_pcfg_glbctr_Base                                                                      0x000000

/*--------------------------------------
BitField Name: terjnreqslen
BitField Type: RW
BitField Desc: Enable JN Ter request for 8 Line
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_pcfg_glbctr_terjnreqslen_Mask                                                           cBit31_24
#define cAf6_pcfg_glbctr_terjnreqslen_Shift                                                                 24

/*--------------------------------------
BitField Name: cpejnreqslen
BitField Type: RW
BitField Desc: Enable JN CPE request for 8 Line
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_pcfg_glbctr_cpejnreqslen_Mask                                                           cBit23_16
#define cAf6_pcfg_glbctr_cpejnreqslen_Shift                                                                 16

/*--------------------------------------
BitField Name: cpejnreqstsen
BitField Type: RW
BitField Desc: Enable JN CPE request for 3 group STS, 16 each
BitField Bits: [14:12]
--------------------------------------*/
#define cAf6_pcfg_glbctr_cpejnreqstsen_Mask                                                          cBit14_12
#define cAf6_pcfg_glbctr_cpejnreqstsen_Shift                                                                12

/*--------------------------------------
BitField Name: jnbdebound
BitField Type: RW
BitField Desc: Debound Threshold for Jn 1byte
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_pcfg_glbctr_jnbdebound_Mask                                                               cBit3_0
#define cAf6_pcfg_glbctr_jnbdebound_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE Global Control
Reg Addr   : 0x01_0002
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to control whether receive bytes from OCN block .

------------------------------------------------------------------------------*/
#define cAf6Reg_pcfg_glbcpectr_Base                                                                   0x010002

/*--------------------------------------
BitField Name: cpevtline5en
BitField Type: RW
BitField Desc: Enable recevive bytes for VTs Line 5
BitField Bits: [29]
--------------------------------------*/
#define cAf6_pcfg_glbcpectr_cpevtline5en_Mask                                                           cBit29
#define cAf6_pcfg_glbcpectr_cpevtline5en_Shift                                                              29

/*--------------------------------------
BitField Name: cpevtline4en
BitField Type: RW
BitField Desc: Enable recevive bytes for VTs Line 4
BitField Bits: [28]
--------------------------------------*/
#define cAf6_pcfg_glbcpectr_cpevtline4en_Mask                                                           cBit28
#define cAf6_pcfg_glbcpectr_cpevtline4en_Shift                                                              28

/*--------------------------------------
BitField Name: cpevtline3en
BitField Type: RW
BitField Desc: Enable recevive bytes for VTs Line 3
BitField Bits: [27]
--------------------------------------*/
#define cAf6_pcfg_glbcpectr_cpevtline3en_Mask                                                           cBit27
#define cAf6_pcfg_glbcpectr_cpevtline3en_Shift                                                              27

/*--------------------------------------
BitField Name: cpevtline2en
BitField Type: RW
BitField Desc: Enable recevive bytes for VTs Line 2
BitField Bits: [26]
--------------------------------------*/
#define cAf6_pcfg_glbcpectr_cpevtline2en_Mask                                                           cBit26
#define cAf6_pcfg_glbcpectr_cpevtline2en_Shift                                                              26

/*--------------------------------------
BitField Name: cpevtline1en
BitField Type: RW
BitField Desc: Enable recevive bytes for VTs Line 1
BitField Bits: [25]
--------------------------------------*/
#define cAf6_pcfg_glbcpectr_cpevtline1en_Mask                                                           cBit25
#define cAf6_pcfg_glbcpectr_cpevtline1en_Shift                                                              25

/*--------------------------------------
BitField Name: cpevtline0en
BitField Type: RW
BitField Desc: Enable recevive bytes for VTs Line 0
BitField Bits: [24]
--------------------------------------*/
#define cAf6_pcfg_glbcpectr_cpevtline0en_Mask                                                           cBit24
#define cAf6_pcfg_glbcpectr_cpevtline0en_Shift                                                              24

/*--------------------------------------
BitField Name: cpestsline7en
BitField Type: RW
BitField Desc: Enable recevive bytes for STSs Line 7
BitField Bits: [23]
--------------------------------------*/
#define cAf6_pcfg_glbcpectr_cpestsline7en_Mask                                                          cBit23
#define cAf6_pcfg_glbcpectr_cpestsline7en_Shift                                                             23

/*--------------------------------------
BitField Name: cpestsline6en
BitField Type: RW
BitField Desc: Enable recevive bytes for STSs Line 6
BitField Bits: [22]
--------------------------------------*/
#define cAf6_pcfg_glbcpectr_cpestsline6en_Mask                                                          cBit22
#define cAf6_pcfg_glbcpectr_cpestsline6en_Shift                                                             22

/*--------------------------------------
BitField Name: cpestsline5en
BitField Type: RW
BitField Desc: Enable recevive bytes for STSs Line 5
BitField Bits: [21]
--------------------------------------*/
#define cAf6_pcfg_glbcpectr_cpestsline5en_Mask                                                          cBit21
#define cAf6_pcfg_glbcpectr_cpestsline5en_Shift                                                             21

/*--------------------------------------
BitField Name: cpestsline4en
BitField Type: RW
BitField Desc: Enable recevive bytes for STSs Line 4
BitField Bits: [20]
--------------------------------------*/
#define cAf6_pcfg_glbcpectr_cpestsline4en_Mask                                                          cBit20
#define cAf6_pcfg_glbcpectr_cpestsline4en_Shift                                                             20

/*--------------------------------------
BitField Name: cpestsline3en
BitField Type: RW
BitField Desc: Enable recevive bytes for STSs Line 3 - EC1 framer
BitField Bits: [19]
--------------------------------------*/
#define cAf6_pcfg_glbcpectr_cpestsline3en_Mask                                                          cBit19
#define cAf6_pcfg_glbcpectr_cpestsline3en_Shift                                                             19

/*--------------------------------------
BitField Name: cpestsline2en
BitField Type: RW
BitField Desc: Enable recevive bytes for STSs Line 2 - STM framer
BitField Bits: [18]
--------------------------------------*/
#define cAf6_pcfg_glbcpectr_cpestsline2en_Mask                                                          cBit18
#define cAf6_pcfg_glbcpectr_cpestsline2en_Shift                                                             18

/*--------------------------------------
BitField Name: cpestsline1en
BitField Type: RW
BitField Desc: Enable recevive bytes for STSs Line 1 - EC1 pointer
BitField Bits: [17]
--------------------------------------*/
#define cAf6_pcfg_glbcpectr_cpestsline1en_Mask                                                          cBit17
#define cAf6_pcfg_glbcpectr_cpestsline1en_Shift                                                             17

/*--------------------------------------
BitField Name: cpestsline0en
BitField Type: RW
BitField Desc: Enable recevive bytes for STSs Line 0 - STM pointer
BitField Bits: [16]
--------------------------------------*/
#define cAf6_pcfg_glbcpectr_cpestsline0en_Mask                                                          cBit16
#define cAf6_pcfg_glbcpectr_cpestsline0en_Shift                                                             16


/*------------------------------------------------------------------------------
Reg Name   : POH Global Alarm
Reg Addr   : 0x00_0002
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to show alarm of Parity and Debug

------------------------------------------------------------------------------*/
#define cAf6Reg_pcfg_glbalm_Base                                                                      0x000002

/*--------------------------------------
BitField Name: pmstk
BitField Type: RW
BitField Desc: PM monitor sticky
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pcfg_glbalm_pmstk_Mask                                                                      cBit6
#define cAf6_pcfg_glbalm_pmstk_Shift                                                                         6

/*--------------------------------------
BitField Name: berparctrlstk
BitField Type: RW
BitField Desc: Parity sticky POH BER Control VT/DSN, POH BER Control STS/TU3
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pcfg_glbalm_berparctrlstk_Mask                                                              cBit5
#define cAf6_pcfg_glbalm_berparctrlstk_Shift                                                                 5

/*--------------------------------------
BitField Name: berpartrshstk
BitField Type: RW
BitField Desc: Parity sticky  POH BER Threshold 2
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pcfg_glbalm_berpartrshstk_Mask                                                              cBit4
#define cAf6_pcfg_glbalm_berpartrshstk_Shift                                                                 4

/*--------------------------------------
BitField Name: terparvttu3stk
BitField Type: RW
BitField Desc: Parity sticky POH Termintate Insert Control VT/TU3
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pcfg_glbalm_terparvttu3stk_Mask                                                             cBit3
#define cAf6_pcfg_glbalm_terparvttu3stk_Shift                                                                3

/*--------------------------------------
BitField Name: terparstsstk
BitField Type: RW
BitField Desc: Parity sticky POH Termintate Insert Control STS
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pcfg_glbalm_terparstsstk_Mask                                                               cBit2
#define cAf6_pcfg_glbalm_terparstsstk_Shift                                                                  2

/*--------------------------------------
BitField Name: cpeparvtstk
BitField Type: RW
BitField Desc: Parity sticky POH CPE VT Control Register
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pcfg_glbalm_cpeparvtstk_Mask                                                                cBit1
#define cAf6_pcfg_glbalm_cpeparvtstk_Shift                                                                   1

/*--------------------------------------
BitField Name: cpeparststu3stk
BitField Type: RW
BitField Desc: Parity sticky POH CPE STS/TU3 Control Register
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pcfg_glbalm_cpeparststu3stk_Mask                                                            cBit0
#define cAf6_pcfg_glbalm_cpeparststu3stk_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : POH Threshold Global Control
Reg Addr   : 0x00_0003
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to set Threshold for stable detection.

------------------------------------------------------------------------------*/
#define cAf6Reg_pcfg_trshglbctr_Base                                                                  0x000003

/*--------------------------------------
BitField Name: jnmsgdebound
BitField Type: RW
BitField Desc: Debound Threshold for Jn 16/64byte
BitField Bits: [31:28]
--------------------------------------*/
#define cAf6_pcfg_trshglbctr_jnmsgdebound_Mask                                                       cBit31_28
#define cAf6_pcfg_trshglbctr_jnmsgdebound_Shift                                                             28

/*--------------------------------------
BitField Name: V5RFIStbTrsh
BitField Type: RW
BitField Desc: V5 RDI Stable Thershold
BitField Bits: [27:24]
--------------------------------------*/
#define cAf6_pcfg_trshglbctr_V5RFIStbTrsh_Mask                                                       cBit27_24
#define cAf6_pcfg_trshglbctr_V5RFIStbTrsh_Shift                                                             24

/*--------------------------------------
BitField Name: V5RDIStbTrsh
BitField Type: RW
BitField Desc: V5 RDI Stable Thershold
BitField Bits: [23:20]
--------------------------------------*/
#define cAf6_pcfg_trshglbctr_V5RDIStbTrsh_Mask                                                       cBit23_20
#define cAf6_pcfg_trshglbctr_V5RDIStbTrsh_Shift                                                             20

/*--------------------------------------
BitField Name: V5SlbStbTrsh
BitField Type: RW
BitField Desc: V5 Signal Lable Stable Thershold
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_pcfg_trshglbctr_V5SlbStbTrsh_Mask                                                       cBit19_16
#define cAf6_pcfg_trshglbctr_V5SlbStbTrsh_Shift                                                             16

/*--------------------------------------
BitField Name: G1RDIStbTrsh
BitField Type: RW
BitField Desc: G1 RDI Path Stable Thershold
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_pcfg_trshglbctr_G1RDIStbTrsh_Mask                                                       cBit15_12
#define cAf6_pcfg_trshglbctr_G1RDIStbTrsh_Shift                                                             12

/*--------------------------------------
BitField Name: C2PlmStbTrsh
BitField Type: RW
BitField Desc: C2 Path Signal Lable Stable Thershold
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_pcfg_trshglbctr_C2PlmStbTrsh_Mask                                                        cBit11_8
#define cAf6_pcfg_trshglbctr_C2PlmStbTrsh_Shift                                                              8

/*--------------------------------------
BitField Name: JnStbTrsh
BitField Type: RW
BitField Desc: J1/J2 Message Stable Threshold
BitField Bits: [7:4]
--------------------------------------*/
#define cAf6_pcfg_trshglbctr_JnStbTrsh_Mask                                                            cBit7_4
#define cAf6_pcfg_trshglbctr_JnStbTrsh_Shift                                                                 4

/*--------------------------------------
BitField Name: debound
BitField Type: RW
BitField Desc: Debound Threshold
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_pcfg_trshglbctr_debound_Mask                                                              cBit3_0
#define cAf6_pcfg_trshglbctr_debound_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : POH Hi-order Path Over Head Grabber
Reg Addr   : 0x02_4000
Reg Formula: 0x02_4000 + $stsid * 8 + $sliceid
    Where  : 
           + $sliceid(0-3): Slice Identification
           + $stsid(0-47): STS Identification
Reg Desc   : 
This register is used to grabber Hi-Order Path Overhead

------------------------------------------------------------------------------*/
#define cAf6Reg_pohstspohgrb_Base                                                                     0x024000
#define cAf6Reg_pohstspohgrb_WidthVal                                                                       96

/*--------------------------------------
BitField Name: hlais
BitField Type: RO
BitField Desc: High-level AIS from OCN
BitField Bits: [67]
--------------------------------------*/
#define cAf6_pohstspohgrb_hlais_Mask                                                                     cBit3
#define cAf6_pohstspohgrb_hlais_Shift                                                                        3

/*--------------------------------------
BitField Name: lom
BitField Type: RO
BitField Desc: LOM  from OCN
BitField Bits: [66]
--------------------------------------*/
#define cAf6_pohstspohgrb_lom_Mask                                                                       cBit2
#define cAf6_pohstspohgrb_lom_Shift                                                                          2

/*--------------------------------------
BitField Name: lop
BitField Type: RO
BitField Desc: LOP from OCN
BitField Bits: [65]
--------------------------------------*/
#define cAf6_pohstspohgrb_lop_Mask                                                                       cBit1
#define cAf6_pohstspohgrb_lop_Shift                                                                          1

/*--------------------------------------
BitField Name: ais
BitField Type: RO
BitField Desc: AIS from OCN
BitField Bits: [64]
--------------------------------------*/
#define cAf6_pohstspohgrb_ais_Mask                                                                       cBit0
#define cAf6_pohstspohgrb_ais_Shift                                                                          0

/*--------------------------------------
BitField Name: K3
BitField Type: RO
BitField Desc: K3 byte
BitField Bits: [63:56]
--------------------------------------*/
#define cAf6_pohstspohgrb_K3_Mask                                                                    cBit31_24
#define cAf6_pohstspohgrb_K3_Shift                                                                          24

/*--------------------------------------
BitField Name: F3
BitField Type: RO
BitField Desc: F3 byte
BitField Bits: [55:48]
--------------------------------------*/
#define cAf6_pohstspohgrb_F3_Mask                                                                    cBit23_16
#define cAf6_pohstspohgrb_F3_Shift                                                                          16

/*--------------------------------------
BitField Name: H4
BitField Type: RO
BitField Desc: H4 byte
BitField Bits: [47:40]
--------------------------------------*/
#define cAf6_pohstspohgrb_H4_Mask                                                                     cBit15_8
#define cAf6_pohstspohgrb_H4_Shift                                                                           8

/*--------------------------------------
BitField Name: F2
BitField Type: RO
BitField Desc: F2 byte
BitField Bits: [39:32]
--------------------------------------*/
#define cAf6_pohstspohgrb_F2_Mask                                                                      cBit7_0
#define cAf6_pohstspohgrb_F2_Shift                                                                           0

/*--------------------------------------
BitField Name: G1
BitField Type: RO
BitField Desc: G1 byte
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_pohstspohgrb_G1_Mask                                                                    cBit31_24
#define cAf6_pohstspohgrb_G1_Shift                                                                          24

/*--------------------------------------
BitField Name: C2
BitField Type: RO
BitField Desc: C2 byte
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_pohstspohgrb_C2_Mask                                                                    cBit23_16
#define cAf6_pohstspohgrb_C2_Shift                                                                          16

/*--------------------------------------
BitField Name: N1
BitField Type: RO
BitField Desc: N1 byte
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_pohstspohgrb_N1_Mask                                                                     cBit15_8
#define cAf6_pohstspohgrb_N1_Shift                                                                           8

/*--------------------------------------
BitField Name: J1
BitField Type: RO
BitField Desc: J1 byte
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_pohstspohgrb_J1_Mask                                                                      cBit7_0
#define cAf6_pohstspohgrb_J1_Shift                                                                           0


/*------------------------------------------------------------------------------
Reg Name   : POH Lo-order VT Over Head Grabber
Reg Addr   : 0x02_6000
Reg Formula: 0x02_6000 + $sliceid*1344 + $stsid*28 + $vtid
    Where  : 
           + $sliceid(0-1): Slice Identification
           + $stsid(0-47): STS Identification, 0-47 for slice 0, 0-3 for slice 1
           + $vtid(0-27): VT Identification
Reg Desc   : 
This register is used to grabber Lo-Order Path Overhead. Incase the TU3 mode, the $vtid = 0, using for Tu3 POH grabber.
Incase VT mode, the $vtid = 0-27, using for VT POH grabber.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohvtpohgrb_Base                                                                      0x026000
#define cAf6Reg_pohvtpohgrb_WidthVal                                                                        64

/*--------------------------------------
BitField Name: hlais
BitField Type: RO
BitField Desc: High-level AIS from OCN
BitField Bits: [35]
--------------------------------------*/
#define cAf6_pohvtpohgrb_hlais_Mask                                                                      cBit3
#define cAf6_pohvtpohgrb_hlais_Shift                                                                         3

/*--------------------------------------
BitField Name: lop
BitField Type: RO
BitField Desc: LOP from OCN
BitField Bits: [33]
--------------------------------------*/
#define cAf6_pohvtpohgrb_lop_Mask                                                                        cBit1
#define cAf6_pohvtpohgrb_lop_Shift                                                                           1

/*--------------------------------------
BitField Name: ais
BitField Type: RO
BitField Desc: AIS from OCN
BitField Bits: [32]
--------------------------------------*/
#define cAf6_pohvtpohgrb_ais_Mask                                                                     cBit32_0
#define cAf6_pohvtpohgrb_ais_Shift                                                                           0

/*--------------------------------------
BitField Name: Byte3
BitField Type: RO
BitField Desc: G1 byte or K3 byte or K4 byte
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_pohvtpohgrb_Byte3_Mask                                                                  cBit31_24
#define cAf6_pohvtpohgrb_Byte3_Shift                                                                        24

/*--------------------------------------
BitField Name: Byte2
BitField Type: RO
BitField Desc: C2 byte or F3 byte or N2 byte
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_pohvtpohgrb_Byte2_Mask                                                                  cBit23_16
#define cAf6_pohvtpohgrb_Byte2_Shift                                                                        16

/*--------------------------------------
BitField Name: Byte1
BitField Type: RO
BitField Desc: N1 byte or H4 byte or J2 byte
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_pohvtpohgrb_Byte1_Mask                                                                   cBit15_8
#define cAf6_pohvtpohgrb_Byte1_Shift                                                                         8

/*--------------------------------------
BitField Name: Byte0
BitField Type: RO
BitField Desc: J1 byte or F2 byte or V5 byte
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_pohvtpohgrb_Byte0_Mask                                                                    cBit7_0
#define cAf6_pohvtpohgrb_Byte0_Shift                                                                         0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE STS/TU3 Control Register
Reg Addr   : 0x02_A000
Reg Formula: 0x02_A000 + $sliceid*96 + $stsid*2 + $tu3en
    Where  : 
           + $sliceid(0-3): Slice Identification
           + $stsid(0-47): STS Identification
           + $tu3en(0-1): Tu3enable, 0: STS, 1:Tu3
Reg Desc   : 
This register is used to configure the POH Hi-order Path Monitoring.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohcpestsctr_Base                                                                     0x02A000

/*--------------------------------------
BitField Name: MonEnb
BitField Type: RW
BitField Desc: Alarm monitor enable
BitField Bits: [20]
--------------------------------------*/
#define cAf6_pohcpestsctr_MonEnb_Mask                                                                   cBit20
#define cAf6_pohcpestsctr_MonEnb_Shift                                                                      20

/*--------------------------------------
BitField Name: PlmEnb
BitField Type: RW
BitField Desc: PLM enable
BitField Bits: [19]
--------------------------------------*/
#define cAf6_pohcpestsctr_PlmEnb_Mask                                                                   cBit19
#define cAf6_pohcpestsctr_PlmEnb_Shift                                                                      19

/*--------------------------------------
BitField Name: VcaisDstren
BitField Type: RW
BitField Desc: VcaisDstren
BitField Bits: [18]
--------------------------------------*/
#define cAf6_pohcpestsctr_VcaisDstren_Mask                                                              cBit18
#define cAf6_pohcpestsctr_VcaisDstren_Shift                                                                 18

/*--------------------------------------
BitField Name: PlmDstren
BitField Type: RW
BitField Desc: PlmDstren
BitField Bits: [17]
--------------------------------------*/
#define cAf6_pohcpestsctr_PlmDstren_Mask                                                                cBit17
#define cAf6_pohcpestsctr_PlmDstren_Shift                                                                   17

/*--------------------------------------
BitField Name: UneqDstren
BitField Type: RW
BitField Desc: UneqDstren
BitField Bits: [16]
--------------------------------------*/
#define cAf6_pohcpestsctr_UneqDstren_Mask                                                               cBit16
#define cAf6_pohcpestsctr_UneqDstren_Shift                                                                  16

/*--------------------------------------
BitField Name: TimDstren
BitField Type: RW
BitField Desc: TimDstren
BitField Bits: [15]
--------------------------------------*/
#define cAf6_pohcpestsctr_TimDstren_Mask                                                                cBit15
#define cAf6_pohcpestsctr_TimDstren_Shift                                                                   15

/*--------------------------------------
BitField Name: Sdhmode
BitField Type: RW
BitField Desc: SDH mode
BitField Bits: [14]
--------------------------------------*/
#define cAf6_pohcpestsctr_Sdhmode_Mask                                                                  cBit14
#define cAf6_pohcpestsctr_Sdhmode_Shift                                                                     14

/*--------------------------------------
BitField Name: Blkmden
BitField Type: RW
BitField Desc: Block mode BIP
BitField Bits: [13]
--------------------------------------*/
#define cAf6_pohcpestsctr_Blkmden_Mask                                                                  cBit13
#define cAf6_pohcpestsctr_Blkmden_Shift                                                                     13

/*--------------------------------------
BitField Name: ERDIenb
BitField Type: RW
BitField Desc: Enable E-RDI
BitField Bits: [12]
--------------------------------------*/
#define cAf6_pohcpestsctr_ERDIenb_Mask                                                                  cBit12
#define cAf6_pohcpestsctr_ERDIenb_Shift                                                                     12

/*--------------------------------------
BitField Name: PslExp
BitField Type: RW
BitField Desc: C2 Expected Path Signal Lable Value
BitField Bits: [11:4]
--------------------------------------*/
#define cAf6_pohcpestsctr_PslExp_Mask                                                                 cBit11_4
#define cAf6_pohcpestsctr_PslExp_Shift                                                                       4

/*--------------------------------------
BitField Name: TimEnb
BitField Type: RW
BitField Desc: Enable Monitor TIM
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pohcpestsctr_TimEnb_Mask                                                                    cBit3
#define cAf6_pohcpestsctr_TimEnb_Shift                                                                       3

/*--------------------------------------
BitField Name: Reiblkmden
BitField Type: RW
BitField Desc: Block mode REI
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pohcpestsctr_Reiblkmden_Mask                                                                cBit2
#define cAf6_pohcpestsctr_Reiblkmden_Shift                                                                   2

/*--------------------------------------
BitField Name: J1mode
BitField Type: RW
BitField Desc: 0: 1Byte 1:16Byte 2:64byte 3:Floating
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_pohcpestsctr_J1mode_Mask                                                                  cBit1_0
#define cAf6_pohcpestsctr_J1mode_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE VT Control Register
Reg Addr   : 0x02_8000
Reg Formula: 0x02_8000 + $sliceid*1344 + $stsid*28 +$vtid
    Where  : 
           + $sliceid(0-1): Slice Identification
           + $stsid(0-47): STS Identification
           + $vtid(0-27):Vt Identification
Reg Desc   : 
This register is used to configure the POH Lo-order Path Monitoring.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohcpevtctr_Base                                                                      0x028000

/*--------------------------------------
BitField Name: MonEnb
BitField Type: RW
BitField Desc: Alarm monitor enable
BitField Bits: [15]
--------------------------------------*/
#define cAf6_pohcpevtctr_MonEnb_Mask                                                                    cBit15
#define cAf6_pohcpevtctr_MonEnb_Shift                                                                       15

/*--------------------------------------
BitField Name: PlmEnb
BitField Type: RW
BitField Desc: VcaisDstren
BitField Bits: [14]
--------------------------------------*/
#define cAf6_pohcpevtctr_PlmEnb_Mask                                                                    cBit14
#define cAf6_pohcpevtctr_PlmEnb_Shift                                                                       14

/*--------------------------------------
BitField Name: VcaisDstren
BitField Type: RW
BitField Desc: VcaisDstren
BitField Bits: [13]
--------------------------------------*/
#define cAf6_pohcpevtctr_VcaisDstren_Mask                                                               cBit13
#define cAf6_pohcpevtctr_VcaisDstren_Shift                                                                  13

/*--------------------------------------
BitField Name: PlmDstren
BitField Type: RW
BitField Desc: PlmDstren
BitField Bits: [12]
--------------------------------------*/
#define cAf6_pohcpevtctr_PlmDstren_Mask                                                                 cBit12
#define cAf6_pohcpevtctr_PlmDstren_Shift                                                                    12

/*--------------------------------------
BitField Name: UneqDstren
BitField Type: RW
BitField Desc: UneqDstren
BitField Bits: [11]
--------------------------------------*/
#define cAf6_pohcpevtctr_UneqDstren_Mask                                                                cBit11
#define cAf6_pohcpevtctr_UneqDstren_Shift                                                                   11

/*--------------------------------------
BitField Name: TimDstren
BitField Type: RW
BitField Desc: TimDstren
BitField Bits: [10]
--------------------------------------*/
#define cAf6_pohcpevtctr_TimDstren_Mask                                                                 cBit10
#define cAf6_pohcpevtctr_TimDstren_Shift                                                                    10

/*--------------------------------------
BitField Name: VSdhmode
BitField Type: RW
BitField Desc: SDH mode
BitField Bits: [9]
--------------------------------------*/
#define cAf6_pohcpevtctr_VSdhmode_Mask                                                                   cBit9
#define cAf6_pohcpevtctr_VSdhmode_Shift                                                                      9

/*--------------------------------------
BitField Name: VBlkmden
BitField Type: RW
BitField Desc: Block mode BIP
BitField Bits: [8]
--------------------------------------*/
#define cAf6_pohcpevtctr_VBlkmden_Mask                                                                   cBit8
#define cAf6_pohcpevtctr_VBlkmden_Shift                                                                      8

/*--------------------------------------
BitField Name: ERDIenb
BitField Type: RW
BitField Desc: Enable E-RDI
BitField Bits: [7]
--------------------------------------*/
#define cAf6_pohcpevtctr_ERDIenb_Mask                                                                    cBit7
#define cAf6_pohcpevtctr_ERDIenb_Shift                                                                       7

/*--------------------------------------
BitField Name: VslExp
BitField Type: RW
BitField Desc: V5 Expected Path Signal Lable Value
BitField Bits: [6:4]
--------------------------------------*/
#define cAf6_pohcpevtctr_VslExp_Mask                                                                   cBit6_4
#define cAf6_pohcpevtctr_VslExp_Shift                                                                        4

/*--------------------------------------
BitField Name: TimEnb
BitField Type: RW
BitField Desc: Enable Monitor TIM
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pohcpevtctr_TimEnb_Mask                                                                     cBit3
#define cAf6_pohcpevtctr_TimEnb_Shift                                                                        3

/*--------------------------------------
BitField Name: Reiblkmden
BitField Type: RW
BitField Desc: Block mode REI
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pohcpevtctr_Reiblkmden_Mask                                                                 cBit2
#define cAf6_pohcpevtctr_Reiblkmden_Shift                                                                    2

/*--------------------------------------
BitField Name: J2mode
BitField Type: RW
BitField Desc: 0: 1Byte 1:16Byte 2:64byte 3:Floating
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_pohcpevtctr_J2mode_Mask                                                                   cBit1_0
#define cAf6_pohcpevtctr_J2mode_Shift                                                                        0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE STS Status Register
Reg Addr   : 0x02_C600
Reg Formula: 0x02_C600 + $sliceid*48 + $stsid
    Where  : 
           + $sliceid(0-3): Slice Identification
           + $stsid(0-47): STS Identification
Reg Desc   : 
This register is used to get POH Hi-order status of monitoring.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohcpestssta_Base                                                                     0x02C600

/*--------------------------------------
BitField Name: C2stbsta
BitField Type: RO
BitField Desc: C2 stable status
BitField Bits: [12]
--------------------------------------*/
#define cAf6_pohcpestssta_C2stbsta_Mask                                                                 cBit12
#define cAf6_pohcpestssta_C2stbsta_Shift                                                                    12

/*--------------------------------------
BitField Name: C2stbcnt
BitField Type: RO
BitField Desc: C2 stable counter
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_pohcpestssta_C2stbcnt_Mask                                                               cBit11_8
#define cAf6_pohcpestssta_C2stbcnt_Shift                                                                     8

/*--------------------------------------
BitField Name: C2acpt
BitField Type: RO
BitField Desc: C2 accept byte
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_pohcpestssta_C2acpt_Mask                                                                  cBit7_0
#define cAf6_pohcpestssta_C2acpt_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE VT/TU3 Status Register
Reg Addr   : 0x02_C000
Reg Formula: 0x02_C000 + $sliceid*1344 + $stsid*28 +$vtid
    Where  : 
           + $sliceid(0-1): Slice Identification
           + $stsid(0-47): STS Identification
           + $vtid(0-27):Vt Identification
Reg Desc   : 
This register is used to get POH Lo-order status of monitoring.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohcpevtsta_Base                                                                      0x02C000

/*--------------------------------------
BitField Name: Vslstbsta
BitField Type: RO
BitField Desc: VSL stable status
BitField Bits: [13]
--------------------------------------*/
#define cAf6_pohcpevtsta_Vslstbsta_Mask                                                                 cBit13
#define cAf6_pohcpevtsta_Vslstbsta_Shift                                                                    13

/*--------------------------------------
BitField Name: Vslstbcnt
BitField Type: RO
BitField Desc: VSL stable counter
BitField Bits: [12:9]
--------------------------------------*/
#define cAf6_pohcpevtsta_Vslstbcnt_Mask                                                               cBit12_9
#define cAf6_pohcpevtsta_Vslstbcnt_Shift                                                                     9

/*--------------------------------------
BitField Name: Vslacpt
BitField Type: RO
BitField Desc: VSL accept byte
BitField Bits: [8:6]
--------------------------------------*/
#define cAf6_pohcpevtsta_Vslacpt_Mask                                                                  cBit8_6
#define cAf6_pohcpevtsta_Vslacpt_Shift                                                                       6

/*--------------------------------------
BitField Name: RFIstatus
BitField Type: RO
BitField Desc: RFI status
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_pohcpevtsta_RFIstatus_Mask                                                                cBit5_0
#define cAf6_pohcpevtsta_RFIstatus_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE J1 STS Expected Message buffer
Reg Addr   : 0x0B_0000
Reg Formula: 0x0B_0000 + $sliceid*384 + $stsid*8 + $msgid
    Where  : 
           + $sliceid(0-3): Slice Identification
           + $stsid(0-47): STS Identification
           + $msgid(0-7): Message ID
Reg Desc   : 
The J1 Expected Message Buffer.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohmsgstsexp_Base                                                                     0x0B0000

/*--------------------------------------
BitField Name: J1ExpMsg
BitField Type: RW
BitField Desc: J1 Expected Message
BitField Bits: [63:0]
--------------------------------------*/
#define cAf6_pohmsgstsexp_J1ExpMsg_Mask_01                                                            cBit31_0
#define cAf6_pohmsgstsexp_J1ExpMsg_Shift_01                                                                  0
#define cAf6_pohmsgstsexp_J1ExpMsg_Mask_02                                                            cBit31_0
#define cAf6_pohmsgstsexp_J1ExpMsg_Shift_02                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE J1 STS Current Message buffer
Reg Addr   : 0x0B_1000
Reg Formula: 0x0B_1000 + $sliceid*384 + $stsid*8 + $msgid
    Where  : 
           + $sliceid(0-3): Slice Identification
           + $stsid(0-47): STS Identification
           + $msgid(0-7): Message ID
Reg Desc   : 
The J1 Current Message Buffer.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohmsgstscur_Base                                                                     0x0B1000

/*--------------------------------------
BitField Name: J1CurMsg
BitField Type: RW
BitField Desc: J1 Current Message
BitField Bits: [63:0]
--------------------------------------*/
#define cAf6_pohmsgstscur_J1CurMsg_Mask_01                                                            cBit31_0
#define cAf6_pohmsgstscur_J1CurMsg_Shift_01                                                                  0
#define cAf6_pohmsgstscur_J1CurMsg_Mask_02                                                            cBit31_0
#define cAf6_pohmsgstscur_J1CurMsg_Shift_02                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE J2 Expected Message buffer
Reg Addr   : 0x08_0000
Reg Formula: 0x08_0000 + $sliceid*10752 + $stsid*224 + $vtid*8 + $msgid
    Where  : 
           + $sliceid(0-1): Slice Identification
           + $stsid(0-47): STS Identification
           + $vtid(0-27): VT Identification
           + $msgid(0-7): Message ID
Reg Desc   : 
The J2 Expected Message Buffer.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohmsgvtexp_Base                                                                      0x080000

/*--------------------------------------
BitField Name: J2ExpMsg
BitField Type: RW
BitField Desc: J2 Expected Message
BitField Bits: [63:0]
--------------------------------------*/
#define cAf6_pohmsgvtexp_J2ExpMsg_Mask_01                                                             cBit31_0
#define cAf6_pohmsgvtexp_J2ExpMsg_Shift_01                                                                   0
#define cAf6_pohmsgvtexp_J2ExpMsg_Mask_02                                                             cBit31_0
#define cAf6_pohmsgvtexp_J2ExpMsg_Shift_02                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE J2 Current Message buffer
Reg Addr   : 0x09_0000
Reg Formula: 0x09_0000 + $sliceid*10752 + $stsid*224 + $vtid*8 + $msgid
    Where  : 
           + $sliceid(0-1): Slice Identification
           + $stsid(0-47): STS Identification
           + $vtid(0-27): VT Identification
           + $msgid(0-7): Message ID
Reg Desc   : 
The J2 Current Message Buffer.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohmsgvtcur_Base                                                                      0x090000

/*--------------------------------------
BitField Name: J2CurMsg
BitField Type: RW
BitField Desc: J2 Current Message
BitField Bits: [63:0]
--------------------------------------*/
#define cAf6_pohmsgvtcur_J2CurMsg_Mask_01                                                             cBit31_0
#define cAf6_pohmsgvtcur_J2CurMsg_Shift_01                                                                   0
#define cAf6_pohmsgvtcur_J2CurMsg_Mask_02                                                             cBit31_0
#define cAf6_pohmsgvtcur_J2CurMsg_Shift_02                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE J1 Insert Message buffer
Reg Addr   : 0x0B_2000
Reg Formula: 0x0B_2000 + $sliceid*512 + $stsid*8 + $msgid
    Where  : 
           + $sliceid(0-1): Slice Identification
           + $stsid(0-47): STS Identification
           + $msgid(0-7): Message ID
Reg Desc   : 
The J1 Current Message Buffer.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohmsgstsins_Base                                                                     0x0B2000

/*--------------------------------------
BitField Name: J1InsMsg
BitField Type: RW
BitField Desc: J1 Insert Message
BitField Bits: [63:0]
--------------------------------------*/
#define cAf6_pohmsgstsins_J1InsMsg_Mask_01                                                            cBit31_0
#define cAf6_pohmsgstsins_J1InsMsg_Shift_01                                                                  0
#define cAf6_pohmsgstsins_J1InsMsg_Mask_02                                                            cBit31_0
#define cAf6_pohmsgstsins_J1InsMsg_Shift_02                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE J2 Insert Message buffer
Reg Addr   : 0x0A_0000
Reg Formula: 0x0A_0000 + $sliceid*10752 + $stsid*224 + $vtid*8 + $msgid
    Where  : 
           + $sliceid(0-1): Slice Identification
           + $stsid(0-47): STS Identification
           + $vtid(0-27): VT Identification
           + $msgid(0-7): Message ID
Reg Desc   : 
The J2 Insert Message Buffer.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohmsgvtins_Base                                                                      0x0A0000

/*--------------------------------------
BitField Name: J2InsMsg
BitField Type: RW
BitField Desc: J2 Insert Message
BitField Bits: [63:0]
--------------------------------------*/
#define cAf6_pohmsgvtins_J2InsMsg_Mask_01                                                             cBit31_0
#define cAf6_pohmsgvtins_J2InsMsg_Shift_01                                                                   0
#define cAf6_pohmsgvtins_J2InsMsg_Mask_02                                                             cBit31_0
#define cAf6_pohmsgvtins_J2InsMsg_Shift_02                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : POH Termintate Insert Control STS
Reg Addr   : 0x04_0400
Reg Formula: 0x04_0400 + $STS + $OCID*48
    Where  : 
           + $STS(0-47)  : STS
           + $OCID(0-1)  : Line ID
Reg Desc   : 
This register is used to control STS POH insert .

------------------------------------------------------------------------------*/
#define cAf6Reg_ter_ctrlhi_Base                                                                       0x040400

/*--------------------------------------
BitField Name: g1spare
BitField Type: RW
BitField Desc: G1 spare value
BitField Bits: [6]
--------------------------------------*/
#define cAf6_ter_ctrlhi_g1spare_Mask                                                                     cBit6
#define cAf6_ter_ctrlhi_g1spare_Shift                                                                        6

/*--------------------------------------
BitField Name: plm
BitField Type: RW
BitField Desc: 0 : Enable, 1: Disable send ERDI if PLM detected
BitField Bits: [5]
--------------------------------------*/
#define cAf6_ter_ctrlhi_plm_Mask                                                                         cBit5
#define cAf6_ter_ctrlhi_plm_Shift                                                                            5

/*--------------------------------------
BitField Name: uneq
BitField Type: RW
BitField Desc: 0 : Enable, 1: Disable send ERDI if UNEQ detected
BitField Bits: [4]
--------------------------------------*/
#define cAf6_ter_ctrlhi_uneq_Mask                                                                        cBit4
#define cAf6_ter_ctrlhi_uneq_Shift                                                                           4

/*--------------------------------------
BitField Name: timmsk
BitField Type: RW
BitField Desc: 0 : Enable, 1: Disable send ERDI if TIM detected
BitField Bits: [3]
--------------------------------------*/
#define cAf6_ter_ctrlhi_timmsk_Mask                                                                      cBit3
#define cAf6_ter_ctrlhi_timmsk_Shift                                                                         3

/*--------------------------------------
BitField Name: aislopmsk
BitField Type: RW
BitField Desc: 0 : Enable, 1: Disable send ERDI if AIS,LOP detected
BitField Bits: [2]
--------------------------------------*/
#define cAf6_ter_ctrlhi_aislopmsk_Mask                                                                   cBit2
#define cAf6_ter_ctrlhi_aislopmsk_Shift                                                                      2

/*--------------------------------------
BitField Name: jnfrmd
BitField Type: RW
BitField Desc: 0:1 byte, 1: 16/64byte
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_ter_ctrlhi_jnfrmd_Mask                                                                    cBit1_0
#define cAf6_ter_ctrlhi_jnfrmd_Shift                                                                         0


/*------------------------------------------------------------------------------
Reg Name   : POH Termintate Insert Control VT/TU3
Reg Addr   : 0x04_4000
Reg Formula: 0x04_4000 + $STS*28 + $OCID*1344 + $VT
    Where  : 
           + $STS(0-47)  : STS
           + $OCID(0-1)  : Line ID
           + $VT(0-27)
Reg Desc   : 
This register is used to control STS POH insert. TU3 is at VT ID = 0. Fields must be the same as ter_ctrlhi

------------------------------------------------------------------------------*/
#define cAf6Reg_ter_ctrllo_Base                                                                       0x044000

/*--------------------------------------
BitField Name: k4b0b1
BitField Type: RW
BitField Desc: K4b0b1 value
BitField Bits: [14:13]
--------------------------------------*/
#define cAf6_ter_ctrllo_k4b0b1_Mask                                                                  cBit14_13
#define cAf6_ter_ctrllo_k4b0b1_Shift                                                                        13

/*--------------------------------------
BitField Name: k4aps
BitField Type: RW
BitField Desc: K4aps value
BitField Bits: [12:11]
--------------------------------------*/
#define cAf6_ter_ctrllo_k4aps_Mask                                                                   cBit12_11
#define cAf6_ter_ctrllo_k4aps_Shift                                                                         11

/*--------------------------------------
BitField Name: k4spare
BitField Type: RW
BitField Desc: K4spare value
BitField Bits: [10]
--------------------------------------*/
#define cAf6_ter_ctrllo_k4spare_Mask                                                                    cBit10
#define cAf6_ter_ctrllo_k4spare_Shift                                                                       10

/*--------------------------------------
BitField Name: rfival
BitField Type: RW
BitField Desc: RFI value
BitField Bits: [9]
--------------------------------------*/
#define cAf6_ter_ctrllo_rfival_Mask                                                                      cBit9
#define cAf6_ter_ctrllo_rfival_Shift                                                                         9

/*--------------------------------------
BitField Name: vslval
BitField Type: RW
BitField Desc: VT signal label value
BitField Bits: [8:6]
--------------------------------------*/
#define cAf6_ter_ctrllo_vslval_Mask                                                                    cBit8_6
#define cAf6_ter_ctrllo_vslval_Shift                                                                         6

/*--------------------------------------
BitField Name: plm
BitField Type: RW
BitField Desc: 0 : Enable, 1: Disable send ERDI if PLM detected
BitField Bits: [5]
--------------------------------------*/
#define cAf6_ter_ctrllo_plm_Mask                                                                         cBit5
#define cAf6_ter_ctrllo_plm_Shift                                                                            5

/*--------------------------------------
BitField Name: uneq
BitField Type: RW
BitField Desc: 0 : Enable, 1: Disable send ERDI if UNEQ detected
BitField Bits: [4]
--------------------------------------*/
#define cAf6_ter_ctrllo_uneq_Mask                                                                        cBit4
#define cAf6_ter_ctrllo_uneq_Shift                                                                           4

/*--------------------------------------
BitField Name: timmsk
BitField Type: RW
BitField Desc: 0 : Enable, 1: Disable send ERDI if TIM detected
BitField Bits: [3]
--------------------------------------*/
#define cAf6_ter_ctrllo_timmsk_Mask                                                                      cBit3
#define cAf6_ter_ctrllo_timmsk_Shift                                                                         3

/*--------------------------------------
BitField Name: aislopmsk
BitField Type: RW
BitField Desc: 0 : Enable, 1: Disable send ERDI if AIS,LOP detected
BitField Bits: [2]
--------------------------------------*/
#define cAf6_ter_ctrllo_aislopmsk_Mask                                                                   cBit2
#define cAf6_ter_ctrllo_aislopmsk_Shift                                                                      2

/*--------------------------------------
BitField Name: jnfrmd
BitField Type: RW
BitField Desc: 0:1 byte, 1: 16/64byte
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_ter_ctrllo_jnfrmd_Mask                                                                    cBit1_0
#define cAf6_ter_ctrllo_jnfrmd_Shift                                                                         0


/*------------------------------------------------------------------------------
Reg Name   : POH Termintate Insert Buffer STS
Reg Addr   : 0x01_0800
Reg Formula: 0x01_0800 + $OCID*256 + $STS*4 + $BGRP
    Where  : 
           + $STS(0-47)  : STS
           + $OCID(0-1)  : Line ID
           + $BGRP(0-3)
Reg Desc   : 
This register is used for storing POH BYTEs inserted to Sonet/SDH. %%
BGRP = 0 : G1,J1  %%
BGRP = 1 : N1,C2  %%
BGRP = 2 : H4,F2  %%
BGRP = 3 : K3,F3

------------------------------------------------------------------------------*/
#define cAf6Reg_rtlpohccterbufhi_Base                                                                 0x010800

/*--------------------------------------
BitField Name: byte1msk
BitField Type: WO
BitField Desc: Enable/Disable (1/0)write to buffer
BitField Bits: [17]
--------------------------------------*/
#define cAf6_rtlpohccterbufhi_byte1msk_Mask                                                             cBit17
#define cAf6_rtlpohccterbufhi_byte1msk_Shift                                                                17

/*--------------------------------------
BitField Name: byte1
BitField Type: RW
BitField Desc: Byte1 (G1/N1/H4/K3)
BitField Bits: [16:9]
--------------------------------------*/
#define cAf6_rtlpohccterbufhi_byte1_Mask                                                              cBit16_9
#define cAf6_rtlpohccterbufhi_byte1_Shift                                                                    9

/*--------------------------------------
BitField Name: byte0msk
BitField Type: WO
BitField Desc: Enable/Disable (1/0) write to buffer
BitField Bits: [8]
--------------------------------------*/
#define cAf6_rtlpohccterbufhi_byte0msk_Mask                                                              cBit8
#define cAf6_rtlpohccterbufhi_byte0msk_Shift                                                                 8

/*--------------------------------------
BitField Name: byte0
BitField Type: RW
BitField Desc: Byte0 (J1/C2/F2/F3)
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_rtlpohccterbufhi_byte0_Mask                                                               cBit7_0
#define cAf6_rtlpohccterbufhi_byte0_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : POH Termintate Insert Buffer TU3/VT
Reg Addr   : 0x01_8000
Reg Formula: 0x01_8000 + $OCID*4096 + $STS*64 + $VT*2 + $BGRP
    Where  : 
           + $STS(0-47)  : STS
           + $OCID(0-1)  : Line ID
           + $VT(0-27)
           + $BGRP(0-1)
Reg Desc   : 
This register is used for storing POH BYTEs inserted to Sonet/SDH. TU3 is at VT ID = 0,1 %%
For VT %%
BGRP = 0 : J2,V5 %%
BGRP = 1 : K4,N2 %%
For TU3 %%
VT = 0, BGRP = 0 : G1,J1 %%
VT = 0, BGRP = 1 : N1,C2 %%
VT = 1, BGRP = 0 : H4,F2 %%
VT = 1, BGRP = 1 : K3,F3

------------------------------------------------------------------------------*/
#define cAf6Reg_rtlpohccterbuflo_Base                                                                 0x018000

/*--------------------------------------
BitField Name: byte1msk
BitField Type: WO
BitField Desc: Enable/Disable (1/0)write to buffer
BitField Bits: [17]
--------------------------------------*/
#define cAf6_rtlpohccterbuflo_byte1msk_Mask                                                             cBit17
#define cAf6_rtlpohccterbuflo_byte1msk_Shift                                                                17

/*--------------------------------------
BitField Name: byte1
BitField Type: RW
BitField Desc: Byte1 (J2/K4)
BitField Bits: [16:9]
--------------------------------------*/
#define cAf6_rtlpohccterbuflo_byte1_Mask                                                              cBit16_9
#define cAf6_rtlpohccterbuflo_byte1_Shift                                                                    9

/*--------------------------------------
BitField Name: byte0msk
BitField Type: WO
BitField Desc: Enable/Disable (1/0) write to buffer
BitField Bits: [8]
--------------------------------------*/
#define cAf6_rtlpohccterbuflo_byte0msk_Mask                                                              cBit8
#define cAf6_rtlpohccterbuflo_byte0msk_Shift                                                                 8

/*--------------------------------------
BitField Name: byte0
BitField Type: RW
BitField Desc: Byte0 (V5/N2)
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_rtlpohccterbuflo_byte0_Mask                                                               cBit7_0
#define cAf6_rtlpohccterbuflo_byte0_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Global Control
Reg Addr   : 0x06_0000
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to enable STS,VT,DSN globally.

------------------------------------------------------------------------------*/
#define cAf6Reg_pcfg_glbenb_Base                                                                      0x060000

/*--------------------------------------
BitField Name: timerenb
BitField Type: RW
BitField Desc: Enable timer
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pcfg_glbenb_timerenb_Mask                                                                   cBit3
#define cAf6_pcfg_glbenb_timerenb_Shift                                                                      3

/*--------------------------------------
BitField Name: stsenb
BitField Type: RW
BitField Desc: Enable STS/TU3 channel
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pcfg_glbenb_stsenb_Mask                                                                     cBit2
#define cAf6_pcfg_glbenb_stsenb_Shift                                                                        2

/*--------------------------------------
BitField Name: vtenb
BitField Type: RW
BitField Desc: Enable STS/TU3 channel
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pcfg_glbenb_vtenb_Mask                                                                      cBit1
#define cAf6_pcfg_glbenb_vtenb_Shift                                                                         1

/*--------------------------------------
BitField Name: dsnsenb
BitField Type: RW
BitField Desc: Enable STS/TU3 channel
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pcfg_glbenb_dsnsenb_Mask                                                                    cBit0
#define cAf6_pcfg_glbenb_dsnsenb_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Error Code DS3/E3 Line Select Control 0
Reg Addr   : 0x06_0004
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to select error to monitor .

------------------------------------------------------------------------------*/
#define cAf6Reg_pcfg_de3errsel0_Base                                                                  0x060004

/*--------------------------------------
BitField Name: linecodeen
BitField Type: RW
BitField Desc: 1: Line code violation , 0: Parity code. For
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_pcfg_de3errsel0_linecodeen_Mask                                                           cBit3_0
#define cAf6_pcfg_de3errsel0_linecodeen_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Error Code DS1/E1 Line Select Control 0
Reg Addr   : 0x06_0024
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to select error to monitor .

------------------------------------------------------------------------------*/
#define cAf6Reg_pcfg_de1errsel0_Base                                                                  0x060024
#define cAf6Reg_pcfg_de1errsel1_Base                                                                  0x060026
/*--------------------------------------
BitField Name: linecodeen
BitField Type: RW
BitField Desc: 1: Line code violation , 0: Parity code. For
BitField Bits: [11:0]
--------------------------------------*/
#define cAf6_pcfg_de1errsel0_linecodeen_Mask                                                          cBit11_0
#define cAf6_pcfg_de1errsel0_linecodeen_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Threshold 1 DS1/E1 Line
Reg Addr   : 0x06_0025
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to configure threshold of BER level 3 of DS1/E1 line.

------------------------------------------------------------------------------*/
#define cAf6Reg_pcfg_de1liutrsh_Base                                                                  0x060025

/*--------------------------------------
BitField Name: e1setthres
BitField Type: RW
BitField Desc: SetThreshold of  E1 line
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_pcfg_de1liutrsh_e1setthres_Mask                                                         cBit31_24
#define cAf6_pcfg_de1liutrsh_e1setthres_Shift                                                               24

/*--------------------------------------
BitField Name: e1winthres
BitField Type: RW
BitField Desc: WindowThreshold of E1 line
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_pcfg_de1liutrsh_e1winthres_Mask                                                         cBit23_16
#define cAf6_pcfg_de1liutrsh_e1winthres_Shift                                                               16

/*--------------------------------------
BitField Name: ds1setthres
BitField Type: RW
BitField Desc: SetThreshold    of  DS1 line
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_pcfg_de1liutrsh_ds1setthres_Mask                                                         cBit15_8
#define cAf6_pcfg_de1liutrsh_ds1setthres_Shift                                                               8

/*--------------------------------------
BitField Name: ds1winthres
BitField Type: RW
BitField Desc: WindowThreshold of DS1 line
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_pcfg_de1liutrsh_ds1winthres_Mask                                                          cBit7_0
#define cAf6_pcfg_de1liutrsh_ds1winthres_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Error Sticky
Reg Addr   : 0x06_0001
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to check error in BER engine.

------------------------------------------------------------------------------*/
#define cAf6Reg_stkalarm_Base                                                                         0x060001

/*--------------------------------------
BitField Name: stserr
BitField Type: W1C
BitField Desc: STS error
BitField Bits: [1]
--------------------------------------*/
#define cAf6_stkalarm_stserr_Mask                                                                        cBit1
#define cAf6_stkalarm_stserr_Shift                                                                           1

/*--------------------------------------
BitField Name: vterr
BitField Type: W1C
BitField Desc: VT error
BitField Bits: [0]
--------------------------------------*/
#define cAf6_stkalarm_vterr_Mask                                                                         cBit0
#define cAf6_stkalarm_vterr_Shift                                                                            0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Threshold 1
Reg Addr   : 0x06_2180
Reg Formula: 0x06_2180 + $Rate
    Where  : 
           + $Rate(0-127): STS Rate for rate from STS1,STS3,STS6...STS48,(0-16)...VT1.5,VT2,DS1,E1(65,67,69,71), OC3,OC12,OC48,OC96,OC192(48-52)....
Reg Desc   : 
This register is used to configure threshold of BER level 3.

------------------------------------------------------------------------------*/
#define cAf6Reg_imemrwptrsh1_Base                                                                     0x062180

/*--------------------------------------
BitField Name: setthres
BitField Type: RW
BitField Desc: SetThreshold,  SDH line with bit field [31:15]
BitField Bits: [18:9]
--------------------------------------*/
#define cAf6_imemrwptrsh1_setthres_Mask                                                               cBit18_9
#define cAf6_imemrwptrsh1_setthres_Shift                                                                     9

/*--------------------------------------
BitField Name: winthres
BitField Type: RW
BitField Desc: WindowThreshold, SDH line with bit field [15:0]
BitField Bits: [8:0]
--------------------------------------*/
#define cAf6_imemrwptrsh1_winthres_Mask                                                                cBit8_0
#define cAf6_imemrwptrsh1_winthres_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Threshold 2
Reg Addr   : 0x06_0400
Reg Formula: 0x06_0400 + $Rate*8 + $Thresloc
    Where  : 
           + $Rate(0-63): STS Rate for rate from STS1,STS3,STS6...STS48,....VT1.5,VT2,DS1,E1....
           + $Thresloc(0-7): Set/Clear/Window threshold for BER level from 4 to 8
Reg Desc   : 
This register is used to configure threshold of BER level 4 to level 8.

------------------------------------------------------------------------------*/
#define cAf6Reg_imemrwptrsh2_Base                                                                     0x060400
#define cAf6Reg_imemrwptrsh2_WidthVal                                                                       64

/*--------------------------------------
BitField Name: scwthres1
BitField Type: RW
BitField Desc: Set/Clear/Window Threshold
BitField Bits: [33:17]
--------------------------------------*/
#define cAf6_imemrwptrsh2_scwthres1_Mask_01                                                          cBit31_17
#define cAf6_imemrwptrsh2_scwthres1_Shift_01                                                                17
#define cAf6_imemrwptrsh2_scwthres1_Mask_02                                                            cBit1_0
#define cAf6_imemrwptrsh2_scwthres1_Shift_02                                                                 0

/*--------------------------------------
BitField Name: scwthres2
BitField Type: RW
BitField Desc: Set/Clear/Window Threshold
BitField Bits: [16:0]
--------------------------------------*/
#define cAf6_imemrwptrsh2_scwthres2_Mask                                                              cBit16_0
#define cAf6_imemrwptrsh2_scwthres2_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Control VT/DSN
Reg Addr   : 0x06_2000
Reg Formula: 0x06_2000 + $STS*8 + $OCID*512+ $VTG
    Where  : 
           + $STS(0-47): STS
           + $OCID(0-3)  : Line ID, 0-1: VT, 2-3: DE1
           + $VTG(0-6)   : VT group
Reg Desc   : 
This register is used to enable and set threshold SD SF .

------------------------------------------------------------------------------*/
#define cAf6Reg_imemrwpctrl1_Base                                                                     0x062000
#define cAf6Reg_imemrwpctrl1_WidthVal                                                                       64

/*--------------------------------------
BitField Name: tcatrsh4
BitField Type: RW
BitField Desc: TCA threshold raise channel 4
BitField Bits: [45:43]
--------------------------------------*/
#define cAf6_imemrwpctrl1_tcatrsh4_Mask                                                              cBit13_11
#define cAf6_imemrwpctrl1_tcatrsh4_Shift                                                                    11

/*--------------------------------------
BitField Name: tcatrsh3
BitField Type: RW
BitField Desc: TCA threshold raise channel 3
BitField Bits: [42:40]
--------------------------------------*/
#define cAf6_imemrwpctrl1_tcatrsh3_Mask                                                               cBit10_8
#define cAf6_imemrwpctrl1_tcatrsh3_Shift                                                                     8

/*--------------------------------------
BitField Name: tcatrsh2
BitField Type: RW
BitField Desc: TCA threshold raise channel 2
BitField Bits: [39:37]
--------------------------------------*/
#define cAf6_imemrwpctrl1_tcatrsh2_Mask                                                                cBit7_5
#define cAf6_imemrwpctrl1_tcatrsh2_Shift                                                                     5

/*--------------------------------------
BitField Name: tcatrsh1
BitField Type: RW
BitField Desc: TCA threshold raise channel 1
BitField Bits: [36:34]
--------------------------------------*/
#define cAf6_imemrwpctrl1_tcatrsh1_Mask                                                                cBit4_2
#define cAf6_imemrwpctrl1_tcatrsh1_Shift                                                                     2

/*--------------------------------------
BitField Name: etype4
BitField Type: RW
BitField Desc: 0: DS1/VT1.5 1: E1/VT2 channel 3
BitField Bits: [31]
--------------------------------------*/
#define cAf6_imemrwpctrl1_etype4_Mask                                                                   cBit31
#define cAf6_imemrwpctrl1_etype4_Shift                                                                      31

/*--------------------------------------
BitField Name: sftrsh4
BitField Type: RW
BitField Desc: SF threshold raise channel 3
BitField Bits: [30:28]
--------------------------------------*/
#define cAf6_imemrwpctrl1_sftrsh4_Mask                                                               cBit30_28
#define cAf6_imemrwpctrl1_sftrsh4_Shift                                                                     28

/*--------------------------------------
BitField Name: sdtrsh4
BitField Type: RW
BitField Desc: SD threshold raise channel 3
BitField Bits: [27:25]
--------------------------------------*/
#define cAf6_imemrwpctrl1_sdtrsh4_Mask                                                               cBit27_25
#define cAf6_imemrwpctrl1_sdtrsh4_Shift                                                                     25

/*--------------------------------------
BitField Name: ena4
BitField Type: RW
BitField Desc: Enable channel 3
BitField Bits: [24]
--------------------------------------*/
#define cAf6_imemrwpctrl1_ena4_Mask                                                                     cBit24
#define cAf6_imemrwpctrl1_ena4_Shift                                                                        24

/*--------------------------------------
BitField Name: etype3
BitField Type: RW
BitField Desc: 0: DS1/VT1.5 1: E1/VT2 channel 2
BitField Bits: [23]
--------------------------------------*/
#define cAf6_imemrwpctrl1_etype3_Mask                                                                   cBit23
#define cAf6_imemrwpctrl1_etype3_Shift                                                                      23

/*--------------------------------------
BitField Name: sftrsh3
BitField Type: RW
BitField Desc: SF threshold raise channel 2
BitField Bits: [22:20]
--------------------------------------*/
#define cAf6_imemrwpctrl1_sftrsh3_Mask                                                               cBit22_20
#define cAf6_imemrwpctrl1_sftrsh3_Shift                                                                     20

/*--------------------------------------
BitField Name: sdtrsh3
BitField Type: RW
BitField Desc: SD threshold raise channel 2
BitField Bits: [19:17]
--------------------------------------*/
#define cAf6_imemrwpctrl1_sdtrsh3_Mask                                                               cBit19_17
#define cAf6_imemrwpctrl1_sdtrsh3_Shift                                                                     17

/*--------------------------------------
BitField Name: ena3
BitField Type: RW
BitField Desc: Enable channel 2
BitField Bits: [16]
--------------------------------------*/
#define cAf6_imemrwpctrl1_ena3_Mask                                                                     cBit16
#define cAf6_imemrwpctrl1_ena3_Shift                                                                        16

/*--------------------------------------
BitField Name: etype2
BitField Type: RW
BitField Desc: 0: DS1/VT1.5 1: E1/VT2 channel 1
BitField Bits: [15]
--------------------------------------*/
#define cAf6_imemrwpctrl1_etype2_Mask                                                                   cBit15
#define cAf6_imemrwpctrl1_etype2_Shift                                                                      15

/*--------------------------------------
BitField Name: sftrsh2
BitField Type: RW
BitField Desc: SF threshold raise channel 1
BitField Bits: [14:12]
--------------------------------------*/
#define cAf6_imemrwpctrl1_sftrsh2_Mask                                                               cBit14_12
#define cAf6_imemrwpctrl1_sftrsh2_Shift                                                                     12

/*--------------------------------------
BitField Name: sdtrsh2
BitField Type: RW
BitField Desc: SD threshold raise channel 1
BitField Bits: [11:9]
--------------------------------------*/
#define cAf6_imemrwpctrl1_sdtrsh2_Mask                                                                cBit11_9
#define cAf6_imemrwpctrl1_sdtrsh2_Shift                                                                      9

/*--------------------------------------
BitField Name: ena2
BitField Type: RW
BitField Desc: Enable channel 1
BitField Bits: [8]
--------------------------------------*/
#define cAf6_imemrwpctrl1_ena2_Mask                                                                      cBit8
#define cAf6_imemrwpctrl1_ena2_Shift                                                                         8

/*--------------------------------------
BitField Name: etype1
BitField Type: RW
BitField Desc: 0: DS1/VT1.5 1: E1/VT2 channel 0
BitField Bits: [7]
--------------------------------------*/
#define cAf6_imemrwpctrl1_etype1_Mask                                                                    cBit7
#define cAf6_imemrwpctrl1_etype1_Shift                                                                       7

/*--------------------------------------
BitField Name: sftrsh1
BitField Type: RW
BitField Desc: SF threshold raise channel 0
BitField Bits: [6:4]
--------------------------------------*/
#define cAf6_imemrwpctrl1_sftrsh1_Mask                                                                 cBit6_4
#define cAf6_imemrwpctrl1_sftrsh1_Shift                                                                      4

/*--------------------------------------
BitField Name: sdtrsh1
BitField Type: RW
BitField Desc: SD threshold raise channel 0
BitField Bits: [3:1]
--------------------------------------*/
#define cAf6_imemrwpctrl1_sdtrsh1_Mask                                                                 cBit3_1
#define cAf6_imemrwpctrl1_sdtrsh1_Shift                                                                      1

/*--------------------------------------
BitField Name: ena1
BitField Type: RW
BitField Desc: Enable channel 0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_imemrwpctrl1_ena1_Mask                                                                      cBit0
#define cAf6_imemrwpctrl1_ena1_Shift                                                                         0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Control STS/TU3
Reg Addr   : 0x06_2007
Reg Formula: 0x06_2007 + $STS*8 + $OCID*512
    Where  : 
           + $STS(0-47)  : STS
           + $OCID(0-3)  : Line ID
Reg Desc   : 
This register is used to enable and set threshold SD SF. BER STS,EC1 pointer use with OCID 0-1. BER DE3 used with OCID 2-3, channel 1, BER SDH Line,EC1 framer used with OCID 2-3, channel 0.

------------------------------------------------------------------------------*/
#define cAf6Reg_imemrwpctrl2_Base                                                                     0x062007
#define cAf6Reg_imemrwpctrl2_WidthVal                                                                       64

/*--------------------------------------
BitField Name: tcatrsh2
BitField Type: RW
BitField Desc: TCA threshold raise channel 2
BitField Bits: [39:37]
--------------------------------------*/
#define cAf6_imemrwpctrl2_tcatrsh2_Mask                                                                cBit7_5
#define cAf6_imemrwpctrl2_tcatrsh2_Shift                                                                     5

/*--------------------------------------
BitField Name: tcatrsh1
BitField Type: RW
BitField Desc: TCA threshold raise channel 1
BitField Bits: [36:34]
--------------------------------------*/
#define cAf6_imemrwpctrl2_tcatrsh1_Mask                                                                cBit4_2
#define cAf6_imemrwpctrl2_tcatrsh1_Shift                                                                     2

/*--------------------------------------
BitField Name: rate2
BitField Type: RW
BitField Desc: STS Rate 0-63 type
BitField Bits: [27:21]
--------------------------------------*/
#define cAf6_imemrwpctrl2_rate2_Mask                                                                 cBit27_21
#define cAf6_imemrwpctrl2_rate2_Shift                                                                       21

/*--------------------------------------
BitField Name: sftrsh2
BitField Type: RW
BitField Desc: SF threshold raise channel 1
BitField Bits: [20:18]
--------------------------------------*/
#define cAf6_imemrwpctrl2_sftrsh2_Mask                                                               cBit20_18
#define cAf6_imemrwpctrl2_sftrsh2_Shift                                                                     18

/*--------------------------------------
BitField Name: sdtrsh2
BitField Type: RW
BitField Desc: SD threshold raise channel 1
BitField Bits: [17:15]
--------------------------------------*/
#define cAf6_imemrwpctrl2_sdtrsh2_Mask                                                               cBit17_15
#define cAf6_imemrwpctrl2_sdtrsh2_Shift                                                                     15

/*--------------------------------------
BitField Name: ena2
BitField Type: RW
BitField Desc: Enable channel 1
BitField Bits: [14]
--------------------------------------*/
#define cAf6_imemrwpctrl2_ena2_Mask                                                                     cBit14
#define cAf6_imemrwpctrl2_ena2_Shift                                                                        14

/*--------------------------------------
BitField Name: rate1
BitField Type: RW
BitField Desc: STS Rate 0-63 type
BitField Bits: [13:7]
--------------------------------------*/
#define cAf6_imemrwpctrl2_rate1_Mask                                                                  cBit13_7
#define cAf6_imemrwpctrl2_rate1_Shift                                                                        7

/*--------------------------------------
BitField Name: sftrsh1
BitField Type: RW
BitField Desc: SF threshold raise channel 0
BitField Bits: [6:4]
--------------------------------------*/
#define cAf6_imemrwpctrl2_sftrsh1_Mask                                                                 cBit6_4
#define cAf6_imemrwpctrl2_sftrsh1_Shift                                                                      4

/*--------------------------------------
BitField Name: sdtrsh1
BitField Type: RW
BitField Desc: SD threshold raise channel 0
BitField Bits: [3:1]
--------------------------------------*/
#define cAf6_imemrwpctrl2_sdtrsh1_Mask                                                                 cBit3_1
#define cAf6_imemrwpctrl2_sdtrsh1_Shift                                                                      1

/*--------------------------------------
BitField Name: ena1
BitField Type: RW
BitField Desc: Enable channel 0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_imemrwpctrl2_ena1_Mask                                                                      cBit0
#define cAf6_imemrwpctrl2_ena1_Shift                                                                         0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Report VT/DSN
Reg Addr   : 0x06_8000
Reg Formula: 0x06_8000 + $STS*28 + $OCID*1344 + $VT
    Where  : 
           + $STS(0-47)  : STS
           + $OCID(0-3)     : Line ID, VT:0-1,DSN:2-3
           + $VT(0-27)   : VT/DS1 ID
Reg Desc   : 
This register is used to get current BER rate .

------------------------------------------------------------------------------*/
#define cAf6Reg_ramberratevtds_Base                                                                   0x068000

/*--------------------------------------
BitField Name: hwsta
BitField Type: RW
BitField Desc: Hardware status
BitField Bits: [3]
--------------------------------------*/
#define cAf6_ramberratevtds_hwsta_Mask                                                                   cBit3
#define cAf6_ramberratevtds_hwsta_Shift                                                                      3

/*--------------------------------------
BitField Name: rate
BitField Type: RW
BitField Desc: BER rate
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_ramberratevtds_rate_Mask                                                                  cBit2_0
#define cAf6_ramberratevtds_rate_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Report STS/TU3
Reg Addr   : 0x06_9500
Reg Formula: 0x06_9500 + $STS*2 + $OCID*128 + $TU3TYPE
    Where  : 
           + $STS(0-47)  : STS
           + $OCID(0-3)     : Line ID
           + $TU3TYPE(0-1)  : Type TU3:1, STS:0
Reg Desc   : 
This register is used to get current BER rate . BER STS use with OCID 0-1. BER DE3 used with OCID 2-3, TU3TYPE = 1, BER SDH Line,EC1 used with OCID 2-3, TU3TYPE = 0.

------------------------------------------------------------------------------*/
#define cAf6Reg_ramberrateststu3_Base                                                                 0x069500

/*--------------------------------------
BitField Name: hwsta
BitField Type: RW
BitField Desc: Hardware status
BitField Bits: [3]
--------------------------------------*/
#define cAf6_ramberrateststu3_hwsta_Mask                                                                 cBit3
#define cAf6_ramberrateststu3_hwsta_Shift                                                                    3

/*--------------------------------------
BitField Name: rate
BitField Type: RW
BitField Desc: BER rate
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_ramberrateststu3_rate_Mask                                                                cBit2_0
#define cAf6_ramberrateststu3_rate_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : POH Counter Report STS
Reg Addr   : 0x0C_A000
Reg Formula: 0x0C_A000 + $STS + $OCID*48
    Where  : 
           + $STS(0-47)  : STS
           + $OCID(0-3)     : Line ID
Reg Desc   : 
This register is used to get POH Counter, Rx SDH pointer increase, decrease counter.

------------------------------------------------------------------------------*/
#define cAf6Reg_ipm_cnthi_Base                                                                        0x0CA000

/*--------------------------------------
BitField Name: reicnt
BitField Type: RC
BitField Desc: REI counter
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_ipm_cnthi_reicnt_Mask                                                                   cBit31_16
#define cAf6_ipm_cnthi_reicnt_Shift                                                                         16

/*--------------------------------------
BitField Name: bipcnt
BitField Type: RC
BitField Desc: BIP counter
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_ipm_cnthi_bipcnt_Mask                                                                    cBit15_0
#define cAf6_ipm_cnthi_bipcnt_Shift                                                                          0


/*------------------------------------------------------------------------------
Reg Name   : POH Counter Report TU3/VT
Reg Addr   : 0x0C_8000
Reg Formula: 0x0C_8000 + $STS*28 + $OCID*1344 + $VT
    Where  : 
           + $STS(0-47)  : STS
           + $OCID(0-3)   : Line ID
           + $VT(0-27)   : VT ID
Reg Desc   : 
This register is used to get POH Counter, Rx SDH pointer increase, decrease counter.

------------------------------------------------------------------------------*/
#define cAf6Reg_ipm_cntlo_Base                                                                        0x0C8000

/*--------------------------------------
BitField Name: reicnt
BitField Type: RC
BitField Desc: REI counter
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_ipm_cntlo_reicnt_Mask                                                                   cBit31_16
#define cAf6_ipm_cntlo_reicnt_Shift                                                                         16

/*--------------------------------------
BitField Name: bipcnt
BitField Type: RC
BitField Desc: BIP counter
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_ipm_cntlo_bipcnt_Mask                                                                    cBit15_0
#define cAf6_ipm_cntlo_bipcnt_Shift                                                                          0


/*------------------------------------------------------------------------------
Reg Name   : POH Alarm Status Mask Report STS
Reg Addr   : 0x0D_0000
Reg Formula: 0x0D_0000 + $STS + $OCID*128
    Where  : 
           + $STS(0-23)  : STS
           + $OCID(0-7) : Line ID, ID 0,2,4,6 : for STS 0,2,4..46, ID 1,3,5,7 for STS 1,3,5..47
Reg Desc   : 
This register is used to get POH alarm mask report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_mskhi_Base                                                                        0x0D0000

/*--------------------------------------
BitField Name: jnstachgmsk
BitField Type: RW
BitField Desc: pslstachg mask
BitField Bits: [13]
--------------------------------------*/
#define cAf6_alm_mskhi_jnstachgmsk_Mask                                                                 cBit13
#define cAf6_alm_mskhi_jnstachgmsk_Shift                                                                    13

/*--------------------------------------
BitField Name: pslstachgmsk
BitField Type: RW
BitField Desc: jnstachg mask
BitField Bits: [12]
--------------------------------------*/
#define cAf6_alm_mskhi_pslstachgmsk_Mask                                                                cBit12
#define cAf6_alm_mskhi_pslstachgmsk_Shift                                                                   12

/*--------------------------------------
BitField Name: bersdmsk
BitField Type: RW
BitField Desc: bersd mask
BitField Bits: [11]
--------------------------------------*/
#define cAf6_alm_mskhi_bersdmsk_Mask                                                                    cBit11
#define cAf6_alm_mskhi_bersdmsk_Shift                                                                       11

/*--------------------------------------
BitField Name: bersfmsk
BitField Type: RW
BitField Desc: bersf  mask
BitField Bits: [10]
--------------------------------------*/
#define cAf6_alm_mskhi_bersfmsk_Mask                                                                    cBit10
#define cAf6_alm_mskhi_bersfmsk_Shift                                                                       10

/*--------------------------------------
BitField Name: erdimsk
BitField Type: RW
BitField Desc: erdis/rdi mask
BitField Bits: [9]
--------------------------------------*/
#define cAf6_alm_mskhi_erdimsk_Mask                                                                      cBit9
#define cAf6_alm_mskhi_erdimsk_Shift                                                                         9

/*--------------------------------------
BitField Name: bertcamsk
BitField Type: RW
BitField Desc: bertca mask
BitField Bits: [8]
--------------------------------------*/
#define cAf6_alm_mskhi_bertcamsk_Mask                                                                    cBit8
#define cAf6_alm_mskhi_bertcamsk_Shift                                                                       8

/*--------------------------------------
BitField Name: erdicmsk
BitField Type: RW
BitField Desc: erdicmsk  mask
BitField Bits: [7]
--------------------------------------*/
#define cAf6_alm_mskhi_erdicmsk_Mask                                                                     cBit7
#define cAf6_alm_mskhi_erdicmsk_Shift                                                                        7

/*--------------------------------------
BitField Name: erdipmsk
BitField Type: RW
BitField Desc: erdipmsk  mask
BitField Bits: [6]
--------------------------------------*/
#define cAf6_alm_mskhi_erdipmsk_Mask                                                                     cBit6
#define cAf6_alm_mskhi_erdipmsk_Shift                                                                        6

/*--------------------------------------
BitField Name: rfimsk
BitField Type: RW
BitField Desc: rfi/lom mask
BitField Bits: [5]
--------------------------------------*/
#define cAf6_alm_mskhi_rfimsk_Mask                                                                       cBit5
#define cAf6_alm_mskhi_rfimsk_Shift                                                                          5

/*--------------------------------------
BitField Name: timmsk
BitField Type: RW
BitField Desc: tim mask
BitField Bits: [4]
--------------------------------------*/
#define cAf6_alm_mskhi_timmsk_Mask                                                                       cBit4
#define cAf6_alm_mskhi_timmsk_Shift                                                                          4

/*--------------------------------------
BitField Name: uneqmsk
BitField Type: RW
BitField Desc: uneq mask
BitField Bits: [3]
--------------------------------------*/
#define cAf6_alm_mskhi_uneqmsk_Mask                                                                      cBit3
#define cAf6_alm_mskhi_uneqmsk_Shift                                                                         3

/*--------------------------------------
BitField Name: plmmsk
BitField Type: RW
BitField Desc: plm mask
BitField Bits: [2]
--------------------------------------*/
#define cAf6_alm_mskhi_plmmsk_Mask                                                                       cBit2
#define cAf6_alm_mskhi_plmmsk_Shift                                                                          2

/*--------------------------------------
BitField Name: aismsk
BitField Type: RW
BitField Desc: ais mask
BitField Bits: [1]
--------------------------------------*/
#define cAf6_alm_mskhi_aismsk_Mask                                                                       cBit1
#define cAf6_alm_mskhi_aismsk_Shift                                                                          1

/*--------------------------------------
BitField Name: lopmsk
BitField Type: RW
BitField Desc: lop mask
BitField Bits: [0]
--------------------------------------*/
#define cAf6_alm_mskhi_lopmsk_Mask                                                                       cBit0
#define cAf6_alm_mskhi_lopmsk_Shift                                                                          0


/*------------------------------------------------------------------------------
Reg Name   : POH Alarm Status Report STS
Reg Addr   : 0x0D_0040
Reg Formula: 0x0D_0040 + $STS + $OCID*128
    Where  : 
           + $STS(0-23)  : STS
           + $OCID(0-7) : Line ID, ID 0,2,4,6 : for STS 0,2,4..46, ID 1,3,5,7 for STS 1,3,5..47
Reg Desc   : 
This register is used to get POH alarm status report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_stahi_Base                                                                        0x0D0040

/*--------------------------------------
BitField Name: bersdsta
BitField Type: W1C
BitField Desc: bersd  status
BitField Bits: [11]
--------------------------------------*/
#define cAf6_alm_stahi_bersdsta_Mask                                                                    cBit11
#define cAf6_alm_stahi_bersdsta_Shift                                                                       11

/*--------------------------------------
BitField Name: bersfsta
BitField Type: W1C
BitField Desc: bersf  status
BitField Bits: [10]
--------------------------------------*/
#define cAf6_alm_stahi_bersfsta_Mask                                                                    cBit10
#define cAf6_alm_stahi_bersfsta_Shift                                                                       10

/*--------------------------------------
BitField Name: erdista
BitField Type: RO
BitField Desc: erdis/rdi  status
BitField Bits: [9]
--------------------------------------*/
#define cAf6_alm_stahi_erdista_Mask                                                                      cBit9
#define cAf6_alm_stahi_erdista_Shift                                                                         9

/*--------------------------------------
BitField Name: bertcasta
BitField Type: RO
BitField Desc: bertca status
BitField Bits: [8]
--------------------------------------*/
#define cAf6_alm_stahi_bertcasta_Mask                                                                    cBit8
#define cAf6_alm_stahi_bertcasta_Shift                                                                       8

/*--------------------------------------
BitField Name: erdicsta
BitField Type: RO
BitField Desc: erdic status
BitField Bits: [7]
--------------------------------------*/
#define cAf6_alm_stahi_erdicsta_Mask                                                                     cBit7
#define cAf6_alm_stahi_erdicsta_Shift                                                                        7

/*--------------------------------------
BitField Name: erdipsta
BitField Type: RO
BitField Desc: erdip stable status
BitField Bits: [6]
--------------------------------------*/
#define cAf6_alm_stahi_erdipsta_Mask                                                                     cBit6
#define cAf6_alm_stahi_erdipsta_Shift                                                                        6

/*--------------------------------------
BitField Name: rfista
BitField Type: RO
BitField Desc: rfi/lom status
BitField Bits: [5]
--------------------------------------*/
#define cAf6_alm_stahi_rfista_Mask                                                                       cBit5
#define cAf6_alm_stahi_rfista_Shift                                                                          5

/*--------------------------------------
BitField Name: timsta
BitField Type: RO
BitField Desc: tim status
BitField Bits: [4]
--------------------------------------*/
#define cAf6_alm_stahi_timsta_Mask                                                                       cBit4
#define cAf6_alm_stahi_timsta_Shift                                                                          4

/*--------------------------------------
BitField Name: uneqsta
BitField Type: RO
BitField Desc: uneq status
BitField Bits: [3]
--------------------------------------*/
#define cAf6_alm_stahi_uneqsta_Mask                                                                      cBit3
#define cAf6_alm_stahi_uneqsta_Shift                                                                         3

/*--------------------------------------
BitField Name: plmsta
BitField Type: RO
BitField Desc: plm status
BitField Bits: [2]
--------------------------------------*/
#define cAf6_alm_stahi_plmsta_Mask                                                                       cBit2
#define cAf6_alm_stahi_plmsta_Shift                                                                          2

/*--------------------------------------
BitField Name: aissta
BitField Type: RO
BitField Desc: ais status
BitField Bits: [1]
--------------------------------------*/
#define cAf6_alm_stahi_aissta_Mask                                                                       cBit1
#define cAf6_alm_stahi_aissta_Shift                                                                          1

/*--------------------------------------
BitField Name: lopsta
BitField Type: RO
BitField Desc: lop status
BitField Bits: [0]
--------------------------------------*/
#define cAf6_alm_stahi_lopsta_Mask                                                                       cBit0
#define cAf6_alm_stahi_lopsta_Shift                                                                          0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt Status Report STS
Reg Addr   : 0x0D_0020
Reg Formula: 0x0D_0020 + $STS + $OCID*128
    Where  : 
           + $STS(0-23)  : STS
           + $OCID(0-7) : Line ID, ID 0,2,4,6 : for STS 0,2,4..46, ID 1,3,5,7 for STS 1,3,5..47
Reg Desc   : 
This register is used to get POH alarm change status report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_chghi_Base                                                                        0x0D0020

/*--------------------------------------
BitField Name: jnstachg
BitField Type: RW
BitField Desc: jn message change
BitField Bits: [13]
--------------------------------------*/
#define cAf6_alm_chghi_jnstachg_Mask                                                                    cBit13
#define cAf6_alm_chghi_jnstachg_Shift                                                                       13

/*--------------------------------------
BitField Name: pslstachg
BitField Type: RW
BitField Desc: psl byte change
BitField Bits: [12]
--------------------------------------*/
#define cAf6_alm_chghi_pslstachg_Mask                                                                   cBit12
#define cAf6_alm_chghi_pslstachg_Shift                                                                      12

/*--------------------------------------
BitField Name: bersdstachg
BitField Type: W1C
BitField Desc: bersd stable status change
BitField Bits: [11]
--------------------------------------*/
#define cAf6_alm_chghi_bersdstachg_Mask                                                                 cBit11
#define cAf6_alm_chghi_bersdstachg_Shift                                                                    11

/*--------------------------------------
BitField Name: bersfstachg
BitField Type: W1C
BitField Desc: bersf stable status change
BitField Bits: [10]
--------------------------------------*/
#define cAf6_alm_chghi_bersfstachg_Mask                                                                 cBit10
#define cAf6_alm_chghi_bersfstachg_Shift                                                                    10

/*--------------------------------------
BitField Name: erdistachg
BitField Type: W1C
BitField Desc: erdis/rdi status change
BitField Bits: [9]
--------------------------------------*/
#define cAf6_alm_chghi_erdistachg_Mask                                                                   cBit9
#define cAf6_alm_chghi_erdistachg_Shift                                                                      9

/*--------------------------------------
BitField Name: bertcastachg
BitField Type: W1C
BitField Desc: bertca status change
BitField Bits: [8]
--------------------------------------*/
#define cAf6_alm_chghi_bertcastachg_Mask                                                                 cBit8
#define cAf6_alm_chghi_bertcastachg_Shift                                                                    8

/*--------------------------------------
BitField Name: erdicstachg
BitField Type: W1C
BitField Desc: erdic status change
BitField Bits: [7]
--------------------------------------*/
#define cAf6_alm_chghi_erdicstachg_Mask                                                                  cBit7
#define cAf6_alm_chghi_erdicstachg_Shift                                                                     7

/*--------------------------------------
BitField Name: erdipstachg
BitField Type: W1C
BitField Desc: erdip status change
BitField Bits: [6]
--------------------------------------*/
#define cAf6_alm_chghi_erdipstachg_Mask                                                                  cBit6
#define cAf6_alm_chghi_erdipstachg_Shift                                                                     6

/*--------------------------------------
BitField Name: rfistachg
BitField Type: W1C
BitField Desc: rfi/lom status change
BitField Bits: [5]
--------------------------------------*/
#define cAf6_alm_chghi_rfistachg_Mask                                                                    cBit5
#define cAf6_alm_chghi_rfistachg_Shift                                                                       5

/*--------------------------------------
BitField Name: timstachg
BitField Type: W1C
BitField Desc: tim status change
BitField Bits: [4]
--------------------------------------*/
#define cAf6_alm_chghi_timstachg_Mask                                                                    cBit4
#define cAf6_alm_chghi_timstachg_Shift                                                                       4

/*--------------------------------------
BitField Name: uneqstachg
BitField Type: W1C
BitField Desc: uneq status change
BitField Bits: [3]
--------------------------------------*/
#define cAf6_alm_chghi_uneqstachg_Mask                                                                   cBit3
#define cAf6_alm_chghi_uneqstachg_Shift                                                                      3

/*--------------------------------------
BitField Name: plmstachg
BitField Type: W1C
BitField Desc: plm status change
BitField Bits: [2]
--------------------------------------*/
#define cAf6_alm_chghi_plmstachg_Mask                                                                    cBit2
#define cAf6_alm_chghi_plmstachg_Shift                                                                       2

/*--------------------------------------
BitField Name: aisstachg
BitField Type: W1C
BitField Desc: ais status change
BitField Bits: [1]
--------------------------------------*/
#define cAf6_alm_chghi_aisstachg_Mask                                                                    cBit1
#define cAf6_alm_chghi_aisstachg_Shift                                                                       1

/*--------------------------------------
BitField Name: lopstachg
BitField Type: W1C
BitField Desc: lop status change
BitField Bits: [0]
--------------------------------------*/
#define cAf6_alm_chghi_lopstachg_Mask                                                                    cBit0
#define cAf6_alm_chghi_lopstachg_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt Global Status Report STS
Reg Addr   : 0x0D_007F
Reg Formula: 0x0D_007F + $OCID*128
    Where  : 
           + $OCID(0-7) : Line ID, ID 0,2,4,6 : for STS 0,2,4..46, ID 1,3,5,7 for STS 1,3,5..47
Reg Desc   : 
This register is used to get POH alarm global change status report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_glbchghi_Base                                                                     0x0D007F

/*--------------------------------------
BitField Name: glbstachg
BitField Type: RO
BitField Desc: global status change bit
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_alm_glbchghi_glbstachg_Mask                                                              cBit23_0
#define cAf6_alm_glbchghi_glbstachg_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt Global Mask Report STS
Reg Addr   : 0x0D_007E
Reg Formula: 0x0D_007E + $OCID*128
    Where  : 
           + $OCID(0-7) : Line ID, ID 0,2,4,6 : for STS 0,2,4..46, ID 1,3,5,7 for STS 1,3,5..47
Reg Desc   : 
This register is used to get POH alarm global mask report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_glbmskhi_Base                                                                     0x0D007E

/*--------------------------------------
BitField Name: glbmsk
BitField Type: RW
BitField Desc: global mask
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_alm_glbmskhi_glbmsk_Mask                                                                 cBit23_0
#define cAf6_alm_glbmskhi_glbmsk_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : POH Alarm Status Mask Report VT/TU3
Reg Addr   : 0x0E_0000
Reg Formula: 0x0E_0000 + $STS*32 + $OCID*4096 + $VTID
    Where  : 
           + $STS(0-23)  : STS
           + $OCID(0-3) : Line ID, ID 0,2,4,6 : for STS 0,2,4..46, ID 1,3,5,7 for STS 1,3,5..47
           + $VTID(0-27)  : VT ID
Reg Desc   : 
This register is used to get POH alarm mask report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_msklo_Base                                                                        0x0E0000

/*--------------------------------------
BitField Name: jnstachgmsk
BitField Type: RW
BitField Desc: jnstachg mask
BitField Bits: [13]
--------------------------------------*/
#define cAf6_alm_msklo_jnstachgmsk_Mask                                                                 cBit13
#define cAf6_alm_msklo_jnstachgmsk_Shift                                                                    13

/*--------------------------------------
BitField Name: pslstachgmsk
BitField Type: RW
BitField Desc: pslstachg mask
BitField Bits: [12]
--------------------------------------*/
#define cAf6_alm_msklo_pslstachgmsk_Mask                                                                cBit12
#define cAf6_alm_msklo_pslstachgmsk_Shift                                                                   12

/*--------------------------------------
BitField Name: bersdmsk
BitField Type: RW
BitField Desc: bersd mask
BitField Bits: [11]
--------------------------------------*/
#define cAf6_alm_msklo_bersdmsk_Mask                                                                    cBit11
#define cAf6_alm_msklo_bersdmsk_Shift                                                                       11

/*--------------------------------------
BitField Name: bersfmsk
BitField Type: RW
BitField Desc: bersf  mask
BitField Bits: [10]
--------------------------------------*/
#define cAf6_alm_msklo_bersfmsk_Mask                                                                    cBit10
#define cAf6_alm_msklo_bersfmsk_Shift                                                                       10

/*--------------------------------------
BitField Name: erdimsk
BitField Type: RW
BitField Desc: erdis/rdi mask
BitField Bits: [9]
--------------------------------------*/
#define cAf6_alm_msklo_erdimsk_Mask                                                                      cBit9
#define cAf6_alm_msklo_erdimsk_Shift                                                                         9

/*--------------------------------------
BitField Name: bertcamsk
BitField Type: RW
BitField Desc: bertca mask
BitField Bits: [8]
--------------------------------------*/
#define cAf6_alm_msklo_bertcamsk_Mask                                                                    cBit8
#define cAf6_alm_msklo_bertcamsk_Shift                                                                       8

/*--------------------------------------
BitField Name: erdicmsk
BitField Type: RW
BitField Desc: erdic  mask
BitField Bits: [7]
--------------------------------------*/
#define cAf6_alm_msklo_erdicmsk_Mask                                                                     cBit7
#define cAf6_alm_msklo_erdicmsk_Shift                                                                        7

/*--------------------------------------
BitField Name: erdipmsk
BitField Type: RW
BitField Desc: erdip  mask
BitField Bits: [6]
--------------------------------------*/
#define cAf6_alm_msklo_erdipmsk_Mask                                                                     cBit6
#define cAf6_alm_msklo_erdipmsk_Shift                                                                        6

/*--------------------------------------
BitField Name: rfimsk
BitField Type: RW
BitField Desc: rfi mask
BitField Bits: [5]
--------------------------------------*/
#define cAf6_alm_msklo_rfimsk_Mask                                                                       cBit5
#define cAf6_alm_msklo_rfimsk_Shift                                                                          5

/*--------------------------------------
BitField Name: timmsk
BitField Type: RW
BitField Desc: tim mask
BitField Bits: [4]
--------------------------------------*/
#define cAf6_alm_msklo_timmsk_Mask                                                                       cBit4
#define cAf6_alm_msklo_timmsk_Shift                                                                          4

/*--------------------------------------
BitField Name: uneqmsk
BitField Type: RW
BitField Desc: uneq mask
BitField Bits: [3]
--------------------------------------*/
#define cAf6_alm_msklo_uneqmsk_Mask                                                                      cBit3
#define cAf6_alm_msklo_uneqmsk_Shift                                                                         3

/*--------------------------------------
BitField Name: plmmsk
BitField Type: RW
BitField Desc: plm mask
BitField Bits: [2]
--------------------------------------*/
#define cAf6_alm_msklo_plmmsk_Mask                                                                       cBit2
#define cAf6_alm_msklo_plmmsk_Shift                                                                          2

/*--------------------------------------
BitField Name: aismsk
BitField Type: RW
BitField Desc: ais mask
BitField Bits: [1]
--------------------------------------*/
#define cAf6_alm_msklo_aismsk_Mask                                                                       cBit1
#define cAf6_alm_msklo_aismsk_Shift                                                                          1

/*--------------------------------------
BitField Name: lopmsk
BitField Type: RW
BitField Desc: lop mask
BitField Bits: [0]
--------------------------------------*/
#define cAf6_alm_msklo_lopmsk_Mask                                                                       cBit0
#define cAf6_alm_msklo_lopmsk_Shift                                                                          0


/*------------------------------------------------------------------------------
Reg Name   : POH Alarm Status Report VT/TU3
Reg Addr   : 0x0E_0800
Reg Formula: 0x0E_0800 + $STS*32 + $OCID*4096 + $VTID
    Where  : 
           + $STS(0-23)  : STS
           + $OCID(0-3) : Line ID, ID 0,2,4,6 : for STS 0,2,4..46, ID 1,3,5,7 for STS 1,3,5..47
           + $VTID(0-27)  : VT ID
Reg Desc   : 
This register is used to get POH alarm status.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_stalo_Base                                                                        0x0E0800

/*--------------------------------------
BitField Name: bersdsta
BitField Type: RW
BitField Desc: bersd status
BitField Bits: [11]
--------------------------------------*/
#define cAf6_alm_stalo_bersdsta_Mask                                                                    cBit11
#define cAf6_alm_stalo_bersdsta_Shift                                                                       11

/*--------------------------------------
BitField Name: bersfsta
BitField Type: RW
BitField Desc: bersf  status
BitField Bits: [10]
--------------------------------------*/
#define cAf6_alm_stalo_bersfsta_Mask                                                                    cBit10
#define cAf6_alm_stalo_bersfsta_Shift                                                                       10

/*--------------------------------------
BitField Name: erdista
BitField Type: RO
BitField Desc: erdis/rdi status
BitField Bits: [9]
--------------------------------------*/
#define cAf6_alm_stalo_erdista_Mask                                                                      cBit9
#define cAf6_alm_stalo_erdista_Shift                                                                         9

/*--------------------------------------
BitField Name: bertcasta
BitField Type: RO
BitField Desc: bertca status
BitField Bits: [8]
--------------------------------------*/
#define cAf6_alm_stalo_bertcasta_Mask                                                                    cBit8
#define cAf6_alm_stalo_bertcasta_Shift                                                                       8

/*--------------------------------------
BitField Name: erdicsta
BitField Type: RO
BitField Desc: erdic status
BitField Bits: [7]
--------------------------------------*/
#define cAf6_alm_stalo_erdicsta_Mask                                                                     cBit7
#define cAf6_alm_stalo_erdicsta_Shift                                                                        7

/*--------------------------------------
BitField Name: erdipsta
BitField Type: RO
BitField Desc: erdip status
BitField Bits: [6]
--------------------------------------*/
#define cAf6_alm_stalo_erdipsta_Mask                                                                     cBit6
#define cAf6_alm_stalo_erdipsta_Shift                                                                        6

/*--------------------------------------
BitField Name: rfista
BitField Type: RO
BitField Desc: rfi status
BitField Bits: [5]
--------------------------------------*/
#define cAf6_alm_stalo_rfista_Mask                                                                       cBit5
#define cAf6_alm_stalo_rfista_Shift                                                                          5

/*--------------------------------------
BitField Name: timsta
BitField Type: RO
BitField Desc: tim status
BitField Bits: [4]
--------------------------------------*/
#define cAf6_alm_stalo_timsta_Mask                                                                       cBit4
#define cAf6_alm_stalo_timsta_Shift                                                                          4

/*--------------------------------------
BitField Name: uneqsta
BitField Type: RO
BitField Desc: uneq status
BitField Bits: [3]
--------------------------------------*/
#define cAf6_alm_stalo_uneqsta_Mask                                                                      cBit3
#define cAf6_alm_stalo_uneqsta_Shift                                                                         3

/*--------------------------------------
BitField Name: plmsta
BitField Type: RO
BitField Desc: plm status
BitField Bits: [2]
--------------------------------------*/
#define cAf6_alm_stalo_plmsta_Mask                                                                       cBit2
#define cAf6_alm_stalo_plmsta_Shift                                                                          2

/*--------------------------------------
BitField Name: aissta
BitField Type: RO
BitField Desc: ais status
BitField Bits: [1]
--------------------------------------*/
#define cAf6_alm_stalo_aissta_Mask                                                                       cBit1
#define cAf6_alm_stalo_aissta_Shift                                                                          1

/*--------------------------------------
BitField Name: lopsta
BitField Type: RO
BitField Desc: lop status
BitField Bits: [0]
--------------------------------------*/
#define cAf6_alm_stalo_lopsta_Mask                                                                       cBit0
#define cAf6_alm_stalo_lopsta_Shift                                                                          0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt Status Report STS
Reg Addr   : 0x0E_0400
Reg Formula: 0x0E_0400 + $STS*32 + $OCID*4096 + $VTID
    Where  : 
           + $STS(0-23)  : STS
           + $OCID(0-3) : Line ID, ID 0,2,4,6 : for STS 0,2,4..46, ID 1,3,5,7 for STS 1,3,5..47
           + $VTID(0-27)  : VT ID
Reg Desc   : 
This register is used to get POH alarm change status report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_chglo_Base                                                                        0x0E0400

/*--------------------------------------
BitField Name: jnstachg
BitField Type: RW
BitField Desc: jn message change
BitField Bits: [13]
--------------------------------------*/
#define cAf6_alm_chglo_jnstachg_Mask                                                                    cBit13
#define cAf6_alm_chglo_jnstachg_Shift                                                                       13

/*--------------------------------------
BitField Name: pslstachg
BitField Type: RW
BitField Desc: psl byte change
BitField Bits: [12]
--------------------------------------*/
#define cAf6_alm_chglo_pslstachg_Mask                                                                   cBit12
#define cAf6_alm_chglo_pslstachg_Shift                                                                      12

/*--------------------------------------
BitField Name: bersdstachg
BitField Type: W1C
BitField Desc: bersd stable status change
BitField Bits: [11]
--------------------------------------*/
#define cAf6_alm_chglo_bersdstachg_Mask                                                                 cBit11
#define cAf6_alm_chglo_bersdstachg_Shift                                                                    11

/*--------------------------------------
BitField Name: bersfstachg
BitField Type: W1C
BitField Desc: bersf stable status change
BitField Bits: [10]
--------------------------------------*/
#define cAf6_alm_chglo_bersfstachg_Mask                                                                 cBit10
#define cAf6_alm_chglo_bersfstachg_Shift                                                                    10

/*--------------------------------------
BitField Name: erdistachg
BitField Type: W1C
BitField Desc: erdis/rdi status change
BitField Bits: [9]
--------------------------------------*/
#define cAf6_alm_chglo_erdistachg_Mask                                                                   cBit9
#define cAf6_alm_chglo_erdistachg_Shift                                                                      9

/*--------------------------------------
BitField Name: bertcastachg
BitField Type: W1C
BitField Desc: bertca status change
BitField Bits: [8]
--------------------------------------*/
#define cAf6_alm_chglo_bertcastachg_Mask                                                                 cBit8
#define cAf6_alm_chglo_bertcastachg_Shift                                                                    8

/*--------------------------------------
BitField Name: erdicstachg
BitField Type: W1C
BitField Desc: erdic status change
BitField Bits: [7]
--------------------------------------*/
#define cAf6_alm_chglo_erdicstachg_Mask                                                                  cBit7
#define cAf6_alm_chglo_erdicstachg_Shift                                                                     7

/*--------------------------------------
BitField Name: erdipstachg
BitField Type: W1C
BitField Desc: erdip status change
BitField Bits: [6]
--------------------------------------*/
#define cAf6_alm_chglo_erdipstachg_Mask                                                                  cBit6
#define cAf6_alm_chglo_erdipstachg_Shift                                                                     6

/*--------------------------------------
BitField Name: rfistachg
BitField Type: W1C
BitField Desc: rfi status change
BitField Bits: [5]
--------------------------------------*/
#define cAf6_alm_chglo_rfistachg_Mask                                                                    cBit5
#define cAf6_alm_chglo_rfistachg_Shift                                                                       5

/*--------------------------------------
BitField Name: timstachg
BitField Type: W1C
BitField Desc: tim status change
BitField Bits: [4]
--------------------------------------*/
#define cAf6_alm_chglo_timstachg_Mask                                                                    cBit4
#define cAf6_alm_chglo_timstachg_Shift                                                                       4

/*--------------------------------------
BitField Name: uneqstachg
BitField Type: W1C
BitField Desc: uneq status change
BitField Bits: [3]
--------------------------------------*/
#define cAf6_alm_chglo_uneqstachg_Mask                                                                   cBit3
#define cAf6_alm_chglo_uneqstachg_Shift                                                                      3

/*--------------------------------------
BitField Name: plmstachg
BitField Type: W1C
BitField Desc: plm status change
BitField Bits: [2]
--------------------------------------*/
#define cAf6_alm_chglo_plmstachg_Mask                                                                    cBit2
#define cAf6_alm_chglo_plmstachg_Shift                                                                       2

/*--------------------------------------
BitField Name: aisstachg
BitField Type: W1C
BitField Desc: ais status change
BitField Bits: [1]
--------------------------------------*/
#define cAf6_alm_chglo_aisstachg_Mask                                                                    cBit1
#define cAf6_alm_chglo_aisstachg_Shift                                                                       1

/*--------------------------------------
BitField Name: lopstachg
BitField Type: W1C
BitField Desc: lop status change
BitField Bits: [0]
--------------------------------------*/
#define cAf6_alm_chglo_lopstachg_Mask                                                                    cBit0
#define cAf6_alm_chglo_lopstachg_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt Or Status Report VT/TU3
Reg Addr   : 0x0E_0C00
Reg Formula: 0x0E_0C00 + $STS + $OCID*4096
    Where  : 
           + $STS(0-23)  : STS
           + $OCID(0-3) : Line ID, ID 0,2,4,6 : for STS 0,2,4..46, ID 1,3,5,7 for STS 1,3,5..47
Reg Desc   : 
This register is used to get POH alarm or status change status report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_orstalo_Base                                                                      0x0E0C00

/*--------------------------------------
BitField Name: orstachg
BitField Type: RO
BitField Desc: or status change bit
BitField Bits: [27:0]
--------------------------------------*/
#define cAf6_alm_orstalo_orstachg_Mask                                                                cBit27_0
#define cAf6_alm_orstalo_orstachg_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt Global Status Report VT/TU3
Reg Addr   : 0x0E_0FFF
Reg Formula: 0x0E_0FFF + $OCID*4096
    Where  : 
           + $OCID(0-3) : Line ID, ID 0,2,4,6 : for STS 0,2,4..46, ID 1,3,5,7 for STS 1,3,5..47
Reg Desc   : 
This register is used to get POH alarm global change status report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_glbchglo_Base                                                                     0x0E0FFF

/*--------------------------------------
BitField Name: glbstachg
BitField Type: RO
BitField Desc: global status change bit
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_alm_glbchglo_glbstachg_Mask                                                              cBit23_0
#define cAf6_alm_glbchglo_glbstachg_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt Global Mask Report VT/TU3
Reg Addr   : 0x0E_0FFE
Reg Formula: 0x0E_0FFE + $OCID*4096
    Where  : 
           + $OCID(0-3) : Line ID, ID 0,2,4,6 : for STS 0,2,4..46, ID 1,3,5,7 for STS 1,3,5..47
Reg Desc   : 
This register is used to get POH alarm global mask report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_glbmsklo_Base                                                                     0x0E0FFE

/*--------------------------------------
BitField Name: glbmsk
BitField Type: RW
BitField Desc: global status change bit
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_alm_glbmsklo_glbmsk_Mask                                                                 cBit23_0
#define cAf6_alm_glbmsklo_glbmsk_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt Global Mask Report
Reg Addr   : 0x00_0004
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to get POH alarm global mask report for high,low order.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_glbmsk_Base                                                                       0x000004

/*--------------------------------------
BitField Name: glbmsklo
BitField Type: RW
BitField Desc: global mask bit for low order slice -
ocid11...ocid3,ocid1,ocid10...ocid2,ocid0
BitField Bits: [27:16]
--------------------------------------*/
#define cAf6_alm_glbmsk_glbmsklo_Mask                                                                cBit27_16
#define cAf6_alm_glbmsk_glbmsklo_Shift                                                                      16

/*--------------------------------------
BitField Name: glbmskhi
BitField Type: RW
BitField Desc: global mask change bit for high order slice -
ocid15...ocid3,ocid1,ocid14...ocid2,ocid0
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_alm_glbmsk_glbmskhi_Mask                                                                 cBit15_0
#define cAf6_alm_glbmsk_glbmskhi_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt Global Status Report
Reg Addr   : 0x00_0005
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to get POH alarm global change status report for high,low order.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_glbchg_Base                                                                       0x000005

/*--------------------------------------
BitField Name: glbstachglo
BitField Type: RO
BitField Desc: global status change bit for low order slice -
ocid11...ocid3,ocid1,ocid10...ocid2,ocid0
BitField Bits: [27:16]
--------------------------------------*/
#define cAf6_alm_glbchg_glbstachglo_Mask                                                             cBit27_16
#define cAf6_alm_glbchg_glbstachglo_Shift                                                                   16

/*--------------------------------------
BitField Name: glbstachghi
BitField Type: RO
BitField Desc: global status change bit for high order slice -
ocid15...ocid3,ocid1,ocid14...ocid2,ocid0
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_alm_glbchg_glbstachghi_Mask                                                              cBit15_0
#define cAf6_alm_glbchg_glbstachghi_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt  Global Status Out Report
Reg Addr   : 0x00_0006
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to get POH alarm global change status report for high,low order after ANDED with mask.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_glbchgo_Base                                                                      0x000006

/*--------------------------------------
BitField Name: glbstachglo
BitField Type: RO
BitField Desc: global status change bit for low order slice -
ocid11...ocid3,ocid1,ocid10...ocid2,ocid0
BitField Bits: [27:16]
--------------------------------------*/
#define cAf6_alm_glbchgo_glbstachglo_Mask                                                            cBit27_16
#define cAf6_alm_glbchgo_glbstachglo_Shift                                                                  16

/*--------------------------------------
BitField Name: glbstachghi
BitField Type: RO
BitField Desc: global status change bit for high order slice -
ocid15...ocid3,ocid1,ocid14...ocid2,ocid0
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_alm_glbchgo_glbstachghi_Mask                                                             cBit15_0
#define cAf6_alm_glbchgo_glbstachghi_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : AME version
Reg Addr   : 0x0F_0000
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to get Version of Alarm Monitoring Engine

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_amever_Base                                                                      0x0F0000

/*--------------------------------------
BitField Name: AMEEngId
BitField Type: RO
BitField Desc: Engine ID
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_upen_amever_AMEEngId_Mask                                                               cBit31_16
#define cAf6_upen_amever_AMEEngId_Shift                                                                     16

/*--------------------------------------
BitField Name: AMEGlbSTS
BitField Type: RO
BitField Desc: STS bits
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_upen_amever_AMEGlbSTS_Mask                                                              cBit15_12
#define cAf6_upen_amever_AMEGlbSTS_Shift                                                                    12

/*--------------------------------------
BitField Name: AMEGlbVT
BitField Type: RO
BitField Desc: VT bits
BitField Bits: [11:08]
--------------------------------------*/
#define cAf6_upen_amever_AMEGlbVT_Mask                                                                cBit11_8
#define cAf6_upen_amever_AMEGlbVT_Shift                                                                      8

/*--------------------------------------
BitField Name: AMEGlbVer
BitField Type: RO
BitField Desc: Version
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_upen_amever_AMEGlbVer_Mask                                                                cBit7_0
#define cAf6_upen_amever_AMEGlbVer_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : AME Control
Reg Addr   : 0x0F_0002
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to control operation of AM engine

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_amectl_Base                                                                      0x0F0002

/*--------------------------------------
BitField Name: AMECtlScale
BitField Type: RW
BitField Desc: Scale timer, devide by 2^AMECtlScale
BitField Bits: [05:04]
--------------------------------------*/
#define cAf6_upen_amectl_AMECtlScale_Mask                                                              cBit5_4
#define cAf6_upen_amectl_AMECtlScale_Shift                                                                   4

/*--------------------------------------
BitField Name: AMECtlForceInt
BitField Type: RW
BitField Desc: Force Interrupt Pin
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_amectl_AMECtlForceInt_Mask                                                             cBit2
#define cAf6_upen_amectl_AMECtlForceInt_Shift                                                                2

/*--------------------------------------
BitField Name: AMECtlClrHOff
BitField Type: RW
BitField Desc: Clear with hold_off_timer
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_amectl_AMECtlClrHOff_Mask                                                              cBit1
#define cAf6_upen_amectl_AMECtlClrHOff_Shift                                                                 1

/*--------------------------------------
BitField Name: AMECtlActive
BitField Type: RW
BitField Desc: Active
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_amectl_AMECtlActive_Mask                                                               cBit0
#define cAf6_upen_amectl_AMECtlActive_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : AME SF/SD Alarm Mask
Reg Addr   : 0x0F_0004
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to Select alarm {ais,lop,plm,ber_sd,ber_sf} into SF/SD table

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_amemask_Base                                                                     0x0F0004

/*--------------------------------------
BitField Name: AMESDAlmMask
BitField Type: RW
BitField Desc: SD Alarm Mask, set 1 to choose bit_0: choose BER_SF alarm bit_1:
choose BER_SD alarm bit_2: choose PLM alarm bit_3: choose LOP alarm bit_4:
choose AIS alarm
BitField Bits: [15:08]
--------------------------------------*/
#define cAf6_upen_amemask_AMESDAlmMask_Mask                                                           cBit15_8
#define cAf6_upen_amemask_AMESDAlmMask_Shift                                                                 8

/*--------------------------------------
BitField Name: AMESFAlmMask
BitField Type: RW
BitField Desc: SF Alarm Mask
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_upen_amemask_AMESFAlmMask_Mask                                                            cBit7_0
#define cAf6_upen_amemask_AMESFAlmMask_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : AME MASR Interrupt Mask
Reg Addr   : 0x0F_0010
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to enable/disable interrupt pin

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ameintdi_Base                                                                    0x0F0010

/*--------------------------------------
BitField Name: AMESDDis
BitField Type: RW
BitField Desc: SD Interrupt disable
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_ameintdi_AMESDDis_Mask                                                                 cBit2
#define cAf6_upen_ameintdi_AMESDDis_Shift                                                                    2

/*--------------------------------------
BitField Name: AMESFDis
BitField Type: RW
BitField Desc: SF Interrupt disable
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_ameintdi_AMESFDis_Mask                                                                 cBit1
#define cAf6_upen_ameintdi_AMESFDis_Shift                                                                    1

/*--------------------------------------
BitField Name: AMEIntDis
BitField Type: RW
BitField Desc: AME Interrupt disable
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_ameintdi_AMEIntDis_Mask                                                                cBit0
#define cAf6_upen_ameintdi_AMEIntDis_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : AME SF MASR register
Reg Addr   : 0x0F_0011
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to report SF row STS/VT table

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_sfrowsts_Base                                                                    0x0F0011

/*--------------------------------------
BitField Name: AMESFrowVT
BitField Type: RC
BitField Desc: SF VT ROW status
BitField Bits: [23:12]
--------------------------------------*/
#define cAf6_upen_sfrowsts_AMESFrowVT_Mask                                                           cBit23_12
#define cAf6_upen_sfrowsts_AMESFrowVT_Shift                                                                 12

/*--------------------------------------
BitField Name: AMESFrowSTS
BitField Type: RC
BitField Desc: SF STS ROW Status
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_upen_sfrowsts_AMESFrowSTS_Mask                                                           cBit11_0
#define cAf6_upen_sfrowsts_AMESFrowSTS_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : AME SD MASR register
Reg Addr   : 0x0F_0012
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to report SD row STS/VT table

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_sdrowsts_Base                                                                    0x0F0012

/*--------------------------------------
BitField Name: AMESDrowVT
BitField Type: RC
BitField Desc: SD VT ROW status bit_0: STS#0-STS#31 bit_1: STS#32-STS#63 ..
BitField Bits: [23:12]
--------------------------------------*/
#define cAf6_upen_sdrowsts_AMESDrowVT_Mask                                                           cBit23_12
#define cAf6_upen_sdrowsts_AMESDrowVT_Shift                                                                 12

/*--------------------------------------
BitField Name: AMESDrowSTS
BitField Type: RC
BitField Desc: SD STS ROW Status
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_upen_sdrowsts_AMESDrowSTS_Mask                                                           cBit11_0
#define cAf6_upen_sdrowsts_AMESDrowSTS_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : AME STS Status table
Reg Addr   : 0x0F_2000
Reg Formula: 0x0F_2000 + $sd_table*0x8000 + $sts_group
    Where  : 
           + $sd_table(0-1): 1 -> SD table, 0 -> SF table
           + $sts_group(0-11) sts group
Reg Desc   : 
This register is used to report STS status

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_xxstasts_Base                                                                    0x0F2000

/*--------------------------------------
BitField Name: AMEStaSTS
BitField Type: RO
BitField Desc: STS Table Status bit_0: STS#0 bit_1: STS#1 ...
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_xxstasts_AMEStaSTS_Mask                                                             cBit31_0
#define cAf6_upen_xxstasts_AMEStaSTS_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : AME STS Enable table
Reg Addr   : 0x0F_3000
Reg Formula: 0x0F_3000 + $sd_table*0x8000 + $sts_group
    Where  : 
           + $sd_table(0-1): 1 -> SD table, 0 -> SF table
           + $sts_group(0-11) sts group
Reg Desc   : 
This register is used to enable STS table

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_xxcfgsts_Base                                                                    0x0F3000

/*--------------------------------------
BitField Name: AMEEnaSTS
BitField Type: RW
BitField Desc: STS Table Enabl bit_0: STS#0 bit_1: STS#1 ...
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_xxcfgsts_AMEEnaSTS_Mask                                                             cBit31_0
#define cAf6_upen_xxcfgsts_AMEEnaSTS_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : AME ROW VT Status table
Reg Addr   : 0x0F_2100
Reg Formula: 0x0F_2100 + $sd_table*0x8000 + $sts_group
    Where  : 
           + $sd_table(0-1): 1 -> SD table, 0 -> SF table
           + $sts_group(0-11) sts_group
Reg Desc   : 
This register is used to report Row VT status

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_xxrowvt_Base                                                                     0x0F2100

/*--------------------------------------
BitField Name: AMEStaRowVT
BitField Type: RC
BitField Desc: Row VT Table Status bit_0: STS#0 bit_1: STS#1 ...
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_xxrowvt_AMEStaRowVT_Mask                                                            cBit31_0
#define cAf6_upen_xxrowvt_AMEStaRowVT_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : AME VT Status table
Reg Addr   : 0x0F_2400
Reg Formula: 0x0F_2400 + $sd_table*0x8000 + $sts_id
    Where  : 
           + $sd_table(0-1): 1 -> SD table, 0 -> SF table
           + $sts_id(0-383) sts id
Reg Desc   : 
This register is used to report VT status

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_xxstavt_Base                                                                     0x0F2400

/*--------------------------------------
BitField Name: AMEStaVT
BitField Type: RO
BitField Desc: VT Table Status bit_0: VT#0 bit_1: VT#1 ...
BitField Bits: [27:00]
--------------------------------------*/
#define cAf6_upen_xxstavt_AMEStaVT_Mask                                                               cBit27_0
#define cAf6_upen_xxstavt_AMEStaVT_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : AME VT Enable table
Reg Addr   : 0x0F_3400
Reg Formula: 0x0F_3400 + $sd_table*0x8000 + $sts_id
    Where  : 
           + $sd_table(0-1): 1 -> SD table, 0 -> SF table
           + $sts_id(0-383) sts_id
Reg Desc   : 
This register is used to enable VT table

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_xxcfgvt_Base                                                                     0x0F3400

/*--------------------------------------
BitField Name: AMEEnaVT
BitField Type: RW
BitField Desc: VT Table Enable bit_0: VT#0 bit_1: VT#1 ...
BitField Bits: [27:00]
--------------------------------------*/
#define cAf6_upen_xxcfgvt_AMEEnaVT_Mask                                                               cBit27_0
#define cAf6_upen_xxcfgvt_AMEEnaVT_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : AME STS Hold Off timer
Reg Addr   : 0x0F_3800
Reg Formula: 0x0F_3800 + $sts_id
    Where  : 
           + $sts_id(0-383) sts_id
Reg Desc   : 
This register is used to config hold off timer for STS alarm, hold_off_timer = AMETmsMax * AMETmsUnit

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_xxcfgtms_Base                                                                    0x0F3800

/*--------------------------------------
BitField Name: AMETmsMax
BitField Type: RW
BitField Desc: Hold off timer
BitField Bits: [09:03]
--------------------------------------*/
#define cAf6_upen_xxcfgtms_AMETmsMax_Mask                                                              cBit9_3
#define cAf6_upen_xxcfgtms_AMETmsMax_Shift                                                                   3

/*--------------------------------------
BitField Name: AMETmsUnit
BitField Type: RW
BitField Desc: Timer Unit
BitField Bits: [02:00]
--------------------------------------*/
#define cAf6_upen_xxcfgtms_AMETmsUnit_Mask                                                             cBit2_0
#define cAf6_upen_xxcfgtms_AMETmsUnit_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : AME VT Hold Off timer
Reg Addr   : 0x0F_4000
Reg Formula: 0x0F_4000 + $sts_id*0x20 + $vt_id
    Where  : 
           + $sts_id(0-383) vt_id
           + $vt_id(0-27)
Reg Desc   : 
This register is used to config hold off timer for VT alarm, hold_off_timer = AMETmvMax * AMETmvUnit

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_xxcfgtmv_Base                                                                    0x0F4000

/*--------------------------------------
BitField Name: AMETmvMax
BitField Type: RW
BitField Desc: Hold off timer
BitField Bits: [09:03]
--------------------------------------*/
#define cAf6_upen_xxcfgtmv_AMETmvMax_Mask                                                              cBit9_3
#define cAf6_upen_xxcfgtmv_AMETmvMax_Shift                                                                   3

/*--------------------------------------
BitField Name: AMETmvUnit
BitField Type: RW
BitField Desc: Timer Unit
BitField Bits: [02:00]
--------------------------------------*/
#define cAf6_upen_xxcfgtmv_AMETmvUnit_Mask                                                             cBit2_0
#define cAf6_upen_xxcfgtmv_AMETmvUnit_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : AME LO ID Lookup
Reg Addr   : 0x0F_1000
Reg Formula: 0x0F_1000 + $slice_id*0x40 + $sts_id
    Where  : 
           + $slice_id (0-7): slice id
           + $sts_id(0-47) sts_id
Reg Desc   : 
This register is used to loopkup LO {slice_id,sts_id} -> TFI-5 {slice_id,sts_id}

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_xxcfglkv_Base                                                                    0x0F1000

/*--------------------------------------
BitField Name: AMELkvLine
BitField Type: RW
BitField Desc: TFI-5 Line ID
BitField Bits: [08:06]
--------------------------------------*/
#define cAf6_upen_xxcfglkv_AMELkvLine_Mask                                                             cBit8_6
#define cAf6_upen_xxcfglkv_AMELkvLine_Shift                                                                  6

/*--------------------------------------
BitField Name: AMELkvSts
BitField Type: RW
BitField Desc: TFI-5 STS ID
BitField Bits: [05:00]
--------------------------------------*/
#define cAf6_upen_xxcfglkv_AMELkvSts_Mask                                                              cBit5_0
#define cAf6_upen_xxcfglkv_AMELkvSts_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : AME HO ID Lookup
Reg Addr   : 0x0F_1800
Reg Formula: 0x0F_1800 + $slice_id*0x40 + $sts_id
    Where  : 
           + $slice_id (0-7): slice id  (also line id)
           + $sts_id(0-47) sts_id
Reg Desc   : 
This register is used to loopkup HO {slice_id,sts_id} -> TFI-5 {slice_id,sts_id}

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_xxcfglks_Base                                                                    0x0F1800

/*--------------------------------------
BitField Name: AMELksLine
BitField Type: RW
BitField Desc: TFI-5 Line ID
BitField Bits: [08:06]
--------------------------------------*/
#define cAf6_upen_xxcfglks_AMELksLine_Mask                                                             cBit8_6
#define cAf6_upen_xxcfglks_AMELksLine_Shift                                                                  6

/*--------------------------------------
BitField Name: AMELksSts
BitField Type: RW
BitField Desc: TFI-5 STS ID
BitField Bits: [05:00]
--------------------------------------*/
#define cAf6_upen_xxcfglks_AMELksSts_Mask                                                              cBit5_0
#define cAf6_upen_xxcfglks_AMELksSts_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit3_0 Control
Reg Addr   : 0x000100
Reg Formula: 0x000100 + HaAddr3_0
    Where  : 
           + $HaAddr3_0(0-15): HA Address Bit3_0
Reg Desc   : 
This register is used to send HA read address bit3_0 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha3_0_control_Base                                                                  0x000100
#define cAf6Reg_rdha3_0_control_WidthVal                                                                    32

/*--------------------------------------
BitField Name: ReadAddr3_0
BitField Type: RW
BitField Desc: Read value will be 0x00100 plus HaAddr3_0
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha3_0_control_ReadAddr3_0_Mask                                                         cBit19_0
#define cAf6_rdha3_0_control_ReadAddr3_0_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit7_4 Control 155*
Reg Addr   : 0x000110
Reg Formula: 0x000110 + HaAddr7_4
    Where  : 
           + $HaAddr7_4(0-15): HA Address Bit7_4
Reg Desc   : 
This register is used to send HA read address bit7_4 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha7_4_control_Base                                                                  0x000110
#define cAf6Reg_rdha7_4_control_WidthVal                                                                    32

/*--------------------------------------
BitField Name: ReadAddr7_4
BitField Type: RW
BitField Desc: Read value will be 0x00100 plus HaAddr7_4
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha7_4_control_ReadAddr7_4_Mask                                                         cBit19_0
#define cAf6_rdha7_4_control_ReadAddr7_4_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit11_8 Control 155*
Reg Addr   : 0x000120
Reg Formula: 0x000120 + HaAddr11_8
    Where  : 
           + $HaAddr11_8(0-15): HA Address Bit11_8
Reg Desc   : 
This register is used to send HA read address bit11_8 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha11_8_control_Base                                                                 0x000120
#define cAf6Reg_rdha11_8_control_WidthVal                                                                   32

/*--------------------------------------
BitField Name: ReadAddr11_8
BitField Type: RW
BitField Desc: Read value will be 0x00100 plus HaAddr11_8
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha11_8_control_ReadAddr11_8_Mask                                                       cBit19_0
#define cAf6_rdha11_8_control_ReadAddr11_8_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit15_12 Control 155*
Reg Addr   : 0x000130
Reg Formula: 0x000130 + HaAddr15_12
    Where  : 
           + $HaAddr15_12(0-15): HA Address Bit15_12
Reg Desc   : 
This register is used to send HA read address bit15_12 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha15_12_control_Base                                                                0x000130
#define cAf6Reg_rdha15_12_control_WidthVal                                                                  32

/*--------------------------------------
BitField Name: ReadAddr15_12
BitField Type: RW
BitField Desc: Read value will be 0x00100 plus HaAddr15_12
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha15_12_control_ReadAddr15_12_Mask                                                     cBit19_0
#define cAf6_rdha15_12_control_ReadAddr15_12_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit19_16 Control 155*
Reg Addr   : 0x000140
Reg Formula: 0x000140 + HaAddr19_16
    Where  : 
           + $HaAddr19_16(0-15): HA Address Bit19_16
Reg Desc   : 
This register is used to send HA read address bit19_16 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha19_16_control_Base                                                                0x000140
#define cAf6Reg_rdha19_16_control_WidthVal                                                                  32

/*--------------------------------------
BitField Name: ReadAddr19_16
BitField Type: RW
BitField Desc: Read value will be 0x00100 plus HaAddr19_16
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha19_16_control_ReadAddr19_16_Mask                                                     cBit19_0
#define cAf6_rdha19_16_control_ReadAddr19_16_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit23_20 Control 155*
Reg Addr   : 0x000150
Reg Formula: 0x000150 + HaAddr23_20
    Where  : 
           + $HaAddr23_20(0-15): HA Address Bit23_20
Reg Desc   : 
This register is used to send HA read address bit23_20 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha23_20_control_Base                                                                0x000150
#define cAf6Reg_rdha23_20_control_WidthVal                                                                  32

/*--------------------------------------
BitField Name: ReadAddr23_20
BitField Type: RW
BitField Desc: Read value will be 0x00100 plus HaAddr23_20
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha23_20_control_ReadAddr23_20_Mask                                                     cBit19_0
#define cAf6_rdha23_20_control_ReadAddr23_20_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit24 and Data Control 155*
Reg Addr   : 0x000160
Reg Formula: 0x000160 + HaAddr24
    Where  : 
           + $HaAddr24(0-1): HA Address Bit24
Reg Desc   : 
This register is used to send HA read address bit24 to HA engine to read data

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha24data_control_Base                                                               0x000160

/*--------------------------------------
BitField Name: ReadHaData31_0
BitField Type: RW
BitField Desc: HA read data bit31_0
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdha24data_control_ReadHaData31_0_Mask                                                   cBit31_0
#define cAf6_rdha24data_control_ReadHaData31_0_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Hold Data63_32 155*
Reg Addr   : 0x000170
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to read HA dword2 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha_hold63_32_Base                                                                   0x000170

/*--------------------------------------
BitField Name: ReadHaData63_32
BitField Type: RW
BitField Desc: HA read data bit63_32
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdha_hold63_32_ReadHaData63_32_Mask                                                      cBit31_0
#define cAf6_rdha_hold63_32_ReadHaData63_32_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Hold Data95_64 155*
Reg Addr   : 0x000171
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to read HA dword3 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_rdindr_hold95_64_Base                                                                 0x000171

/*--------------------------------------
BitField Name: ReadHaData95_64
BitField Type: RW
BitField Desc: HA read data bit95_64
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdindr_hold95_64_ReadHaData95_64_Mask                                                    cBit31_0
#define cAf6_rdindr_hold95_64_ReadHaData95_64_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit3_0 Control 311*
Reg Addr   : 0x010100
Reg Formula: 0x010100 + HaAddr3_0
    Where  : 
           + $HaAddr3_0(0-15): HA Address Bit3_0
Reg Desc   : 
This register is used to send HA read address bit3_0 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha3_0_control311_Base                                                               0x010100
#define cAf6Reg_rdha3_0_control311_WidthVal                                                                 32

/*--------------------------------------
BitField Name: ReadAddr3_0
BitField Type: RW
BitField Desc: Read value will be 0x10100 plus HaAddr3_0
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha3_0_control311_ReadAddr3_0_Mask                                                      cBit19_0
#define cAf6_rdha3_0_control311_ReadAddr3_0_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit7_4 Control 311*
Reg Addr   : 0x010110
Reg Formula: 0x010110 + HaAddr7_4
    Where  : 
           + $HaAddr7_4(0-15): HA Address Bit7_4
Reg Desc   : 
This register is used to send HA read address bit7_4 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha7_4_control311_Base                                                               0x010110
#define cAf6Reg_rdha7_4_control311_WidthVal                                                                 32

/*--------------------------------------
BitField Name: ReadAddr7_4
BitField Type: RW
BitField Desc: Read value will be 0x10100 plus HaAddr7_4
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha7_4_control311_ReadAddr7_4_Mask                                                      cBit19_0
#define cAf6_rdha7_4_control311_ReadAddr7_4_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit11_8 Control 311*
Reg Addr   : 0x010120
Reg Formula: 0x010120 + HaAddr11_8
    Where  : 
           + $HaAddr11_8(0-15): HA Address Bit11_8
Reg Desc   : 
This register is used to send HA read address bit11_8 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha11_8_control311_Base                                                              0x010120
#define cAf6Reg_rdha11_8_control311_WidthVal                                                                32

/*--------------------------------------
BitField Name: ReadAddr11_8
BitField Type: RW
BitField Desc: Read value will be 0x10100 plus HaAddr11_8
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha11_8_control311_ReadAddr11_8_Mask                                                    cBit19_0
#define cAf6_rdha11_8_control311_ReadAddr11_8_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit15_12 Control 311*
Reg Addr   : 0x010130
Reg Formula: 0x010130 + HaAddr15_12
    Where  : 
           + $HaAddr15_12(0-15): HA Address Bit15_12
Reg Desc   : 
This register is used to send HA read address bit15_12 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha15_12_control311_Base                                                             0x010130
#define cAf6Reg_rdha15_12_control311_WidthVal                                                               32

/*--------------------------------------
BitField Name: ReadAddr15_12
BitField Type: RW
BitField Desc: Read value will be 0x10100 plus HaAddr15_12
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha15_12_control311_ReadAddr15_12_Mask                                                  cBit19_0
#define cAf6_rdha15_12_control311_ReadAddr15_12_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit19_16 Control 311*
Reg Addr   : 0x010140
Reg Formula: 0x010140 + HaAddr19_16
    Where  : 
           + $HaAddr19_16(0-15): HA Address Bit19_16
Reg Desc   : 
This register is used to send HA read address bit19_16 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha19_16_control311_Base                                                             0x010140
#define cAf6Reg_rdha19_16_control311_WidthVal                                                               32

/*--------------------------------------
BitField Name: ReadAddr19_16
BitField Type: RW
BitField Desc: Read value will be 0x10100 plus HaAddr19_16
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha19_16_control311_ReadAddr19_16_Mask                                                  cBit19_0
#define cAf6_rdha19_16_control311_ReadAddr19_16_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit23_20 Control 311*
Reg Addr   : 0x010150
Reg Formula: 0x010150 + HaAddr23_20
    Where  : 
           + $HaAddr23_20(0-15): HA Address Bit23_20
Reg Desc   : 
This register is used to send HA read address bit23_20 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha23_20_control311_Base                                                             0x010150
#define cAf6Reg_rdha23_20_control311_WidthVal                                                               32

/*--------------------------------------
BitField Name: ReadAddr23_20
BitField Type: RW
BitField Desc: Read value will be 0x10100 plus HaAddr23_20
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha23_20_control311_ReadAddr23_20_Mask                                                  cBit19_0
#define cAf6_rdha23_20_control311_ReadAddr23_20_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit24 and Data Control 311*
Reg Addr   : 0x010160
Reg Formula: 0x010160 + HaAddr24
    Where  : 
           + $HaAddr24(0-1): HA Address Bit24
Reg Desc   : 
This register is used to send HA read address bit24 to HA engine to read data

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha24data_control311_Base                                                            0x010160

/*--------------------------------------
BitField Name: ReadHaData31_0
BitField Type: RW
BitField Desc: HA read data bit31_0
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdha24data_control311_ReadHaData31_0_Mask                                                cBit31_0
#define cAf6_rdha24data_control311_ReadHaData31_0_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Hold Data63_32 311*
Reg Addr   : 0x010170
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to read HA dword2 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha_hold63_32clk311_Base                                                             0x010170

/*--------------------------------------
BitField Name: ReadHaData63_32
BitField Type: RW
BitField Desc: HA read data bit63_32
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdha_hold63_32clk311_ReadHaData63_32_Mask                                                cBit31_0
#define cAf6_rdha_hold63_32clk311_ReadHaData63_32_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Hold Data95_64 311*
Reg Addr   : 0x010171
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to read HA dword3 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_rdindr_hold95_64clk311_Base                                                           0x010171

/*--------------------------------------
BitField Name: ReadHaData95_64
BitField Type: RW
BitField Desc: HA read data bit95_64
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdindr_hold95_64clk311_ReadHaData95_64_Mask                                              cBit31_0
#define cAf6_rdindr_hold95_64clk311_ReadHaData95_64_Shift                                                    0

#endif /* _AF6_REG_AF6CCI0061_RD_POH_BER_H_ */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : Tha60210061PdhDe1PathBerController.c
 *
 * Created Date: Mar 17, 2017
 *
 * Description : BER controller of LIU PDH DE1 path.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "Tha60210061PdhDe1BerControllerInternal.h"
#include "Tha60210061ModuleBer.h"
#include "Tha60210061ModuleBerReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cHwOcIdForDe1Liu   3
#define cHwStsIdForDe1Liu  4

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)             ((Tha60210061PdhDe1PathBerController)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods                        m_AtObjectOverride;
static tAtBerControllerMethods                 m_AtBerControllerOverride;
static tTha60210011SdhAuVcBerControllerMethods m_Tha60210011SdhAuVcBerControllerOverride;

/* Cache super */
static const tAtObjectMethods        *m_AtObjectMethods        = NULL;
static const tAtBerControllerMethods *m_AtBerControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool IsLineLayer(AtBerController self)
    {
    return mThis(self)->isLineLayer;
    }

static uint32 MaskByLiuId(uint32 liuId)
    {
    return cBit0 << liuId;
    }

static uint32 ShiftByLiuId(uint32 liuId)
    {
    return liuId;
    }

static uint32 LiuFlatId(AtBerController self)
    {
    return AtChannelIdGet(AtBerControllerMonitoredChannel((AtBerController)self));
    }

static eBool IsE1Mode(AtBerController self)
    {
    AtPdhDe1 de1 = (AtPdhDe1)AtBerControllerMonitoredChannel((AtBerController)self);
    return AtPdhDe1IsE1(de1);
    }

static uint32 De1LineSelectorRegister(AtBerController self)
    {
    return IsE1Mode(self) ? cAf6Reg_pcfg_de1errsel1_Base : cAf6Reg_pcfg_de1errsel0_Base;
    }

static void BerDe1PathModeSet(AtBerController self, eBool isPathMode)
    {
    const uint8 cBerPdhLineMode = 1;
    const uint8 cBerPdhPathMode = 0;
    uint32 regAddr, regVal;
    uint32 liuId = LiuFlatId(self);
    uint32 baseAddress = AtModuleBerBaseAddress((AtModuleBer)AtBerControllerModuleGet(self));
    regAddr = baseAddress + De1LineSelectorRegister(self);
    regVal =  AtBerControllerRead(self, regAddr);
    mFieldIns(&regVal, MaskByLiuId(liuId), ShiftByLiuId(liuId), isPathMode ? cBerPdhPathMode : cBerPdhLineMode);
    AtBerControllerWrite(self, regAddr, regVal);
    }

static eBool BerDe1IsPathMode(AtBerController self)
    {
    uint32 regAddr, regVal;
    uint32 baseAddress = AtModuleBerBaseAddress((AtModuleBer)AtBerControllerModuleGet(self));
    uint32 liuId = LiuFlatId(self);

    regAddr = baseAddress + De1LineSelectorRegister(self);
    regVal =  AtBerControllerRead(self, regAddr);

    return(regVal & MaskByLiuId(liuId)) ? cAtFalse: cAtTrue;
    }

static eBool IsLineMode(AtBerController self)
    {
    return BerDe1IsPathMode(self) ? cAtFalse : cAtTrue;
    }

static uint32 BerControlOffset(Tha60210011SdhAuVcBerController self)
    {
    uint8 slice = 0, hwSts = 0;
    AtPdhChannel de1 = (AtPdhChannel)AtBerControllerMonitoredChannel((AtBerController)self);
    AtSdhChannel sdhChannel = (AtSdhChannel)AtPdhChannelVcInternalGet(de1);
    uint8 tug2Id = AtSdhChannelTug2Get(sdhChannel);
    ThaPdhChannelHwIdGet(de1, cThaModulePoh, &slice, &hwSts);

    hwSts = (uint8)(cHwStsIdForDe1Liu + slice);
    if (AtPdhDe1IsE1((AtPdhDe1)de1))
        hwSts = (uint8)(cHwStsIdForDe1Liu + slice + 1);

    return (uint32)((hwSts * 8UL) + (cHwOcIdForDe1Liu * 512UL) + tug2Id);
    }

static uint32 CurrentBerOffset(Tha60210011SdhAuVcBerController self)
    {
    AtPdhChannel de1 = (AtPdhChannel)AtBerControllerMonitoredChannel((AtBerController)self);
    AtSdhChannel sdhChannel = (AtSdhChannel)AtPdhChannelVcInternalGet(de1);
    uint8 slice = 0, hwSts = 0;
    uint8 tug2Id = AtSdhChannelTug2Get(sdhChannel);
    uint8 tuId = AtSdhChannelTu1xGet(sdhChannel);

    ThaPdhChannelHwIdGet(de1, cThaModulePoh, &slice, &hwSts);
    hwSts = (uint8)(cHwStsIdForDe1Liu + slice);
    if (AtPdhDe1IsE1((AtPdhDe1)de1))
        hwSts = (uint8)(cHwStsIdForDe1Liu + slice + 1);

    return (uint32)((hwSts * 28UL) + (cHwOcIdForDe1Liu * 1344UL) + tug2Id * 4UL + tuId);
    }

static uint32 MeasureEngineId(AtBerController self)
    {
    return CurrentBerOffset((Tha60210011SdhAuVcBerController)self);
    }

static eAtRet Enable(AtBerController self, eBool enable)
    {
    eAtRet ret = m_AtBerControllerMethods->Enable(self, enable);
    if ((ret == cAtOk) && (enable))
        BerDe1PathModeSet(self, IsLineLayer(self) ? cAtFalse : cAtTrue);

    return ret;
    }

static eBool IsEnabled(AtBerController self)
    {
    if (IsLineMode(self) != IsLineLayer(self))
        return cAtFalse;

    return m_AtBerControllerMethods->IsEnabled(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60210061PdhDe1PathBerController object = mThis(self);
    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeUInt(isLineLayer);
    }

static void OverrideAtObject(AtBerController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtBerController(AtBerController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtBerControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtBerControllerOverride, m_AtBerControllerMethods, sizeof(m_AtBerControllerOverride));

        mMethodOverride(m_AtBerControllerOverride, Enable);
        mMethodOverride(m_AtBerControllerOverride, IsEnabled);
        mMethodOverride(m_AtBerControllerOverride, MeasureEngineId);
        }

    mMethodsSet(self, &m_AtBerControllerOverride);
    }

static void OverrideTha60210011SdhAuVcBerController(AtBerController self)
    {
    Tha60210011SdhAuVcBerController auvcController = (Tha60210011SdhAuVcBerController)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011SdhAuVcBerControllerOverride, mMethodsGet(auvcController), sizeof(m_Tha60210011SdhAuVcBerControllerOverride));

        mMethodOverride(m_Tha60210011SdhAuVcBerControllerOverride, BerControlOffset);
        mMethodOverride(m_Tha60210011SdhAuVcBerControllerOverride, CurrentBerOffset);
        }

    mMethodsSet(auvcController, &m_Tha60210011SdhAuVcBerControllerOverride);
    }

static void Override(AtBerController self)
    {
    OverrideAtObject(self);
    OverrideAtBerController(self);
    OverrideTha60210011SdhAuVcBerController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061PdhDe1PathBerController);
    }

AtBerController Tha60210061PdhDe1PathBerControllerObjectInit(AtBerController self, uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011PdhDe1BerControllerObjectInit(self, controllerId, channel, berModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtBerController Tha60210061PdhDe1PathBerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtBerController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60210061PdhDe1PathBerControllerObjectInit(newController, controllerId, channel, berModule);
    }

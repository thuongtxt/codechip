/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : Tha60210061PdhDe3PathBerController.c
 *
 * Created Date: Mar 15, 2017
 *
 * Description : BER controller of LIU PDH DE3 path on Parity Error.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "Tha60210061PdhDe3BerControllerInternal.h"
#include "Tha60210061ModuleBer.h"
#include "Tha60210061ModuleBerReg.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self)             ((Tha60210061PdhDe3PathBerController)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods                        m_AtObjectOverride;
static tAtBerControllerMethods                 m_AtBerControllerOverride;
static tTha60210011SdhAuVcBerControllerMethods m_Tha60210011SdhAuVcBerControllerOverride;

/* Cache super */
static const tAtObjectMethods                 *m_AtObjectMethods        = NULL;
static const tAtBerControllerMethods          *m_AtBerControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool IsLineLayer(AtBerController self)
    {
    return mThis(self)->isLineLayer;
    }

static uint32 MaskByHwSts(uint32 hwSts)
    {
    return cBit0 << hwSts;
    }

static uint32 ShiftByHwSts(uint32 hwSts)
    {
    return hwSts;
    }

static uint32 HwStsId(AtBerController self)
    {
    return AtChannelIdGet(AtBerControllerMonitoredChannel((AtBerController)self));
    }

static void BerDe3PathModeSet(AtBerController self, eBool isPathMode)
    {
    const uint8 cBerPdhLineMode = 1;
    const uint8 cBerPdhPathMode = 0;
    uint32 regAddr, regVal;
    uint32 hwSts = HwStsId(self);
    uint32 baseAddress = AtModuleBerBaseAddress((AtModuleBer)AtBerControllerModuleGet(self));
    regAddr = baseAddress + cAf6Reg_pcfg_de3errsel0_Base;
    regVal =  AtBerControllerRead(self, regAddr);
    mFieldIns(&regVal, MaskByHwSts(hwSts), ShiftByHwSts(hwSts), isPathMode ? cBerPdhPathMode : cBerPdhLineMode);
    AtBerControllerWrite(self, regAddr, regVal);
    }

static eBool BerDe3IsPathMode(AtBerController self)
    {
    uint32 regAddr, regVal;
    uint32 baseAddress = AtModuleBerBaseAddress((AtModuleBer)AtBerControllerModuleGet(self));
    uint32 hwSts = HwStsId(self);

    regAddr = baseAddress + cAf6Reg_pcfg_de3errsel0_Base;
    regVal =  AtBerControllerRead(self, regAddr);

    return(regVal & MaskByHwSts(hwSts)) ? cAtFalse: cAtTrue;
    }

static eBool IsLineMode(AtBerController self)
    {
    return BerDe3IsPathMode(self) ? cAtFalse : cAtTrue;
    }

static uint32 CurrentBerOffset(Tha60210011SdhAuVcBerController self)
    {
    uint32 hwStsId = AtChannelIdGet(AtBerControllerMonitoredChannel((AtBerController)self));
    const uint32 cHwOcIdForDe3Liu = 3;

    return ((hwStsId * 2UL) + (cHwOcIdForDe3Liu * 128UL) + 1);
    }

static uint32 BerControlOffset(Tha60210011SdhAuVcBerController self)
    {
    uint32 hwStsId = AtChannelIdGet(AtBerControllerMonitoredChannel((AtBerController)self));
    const uint32 cHwOcIdForDe3Liu = 3;

    return ((hwStsId * 8UL) + (cHwOcIdForDe3Liu * 512UL));
    }

static uint32 MeasureEngineId(AtBerController self)
    {
    Tha60210011SdhAuVcBerController controller = (Tha60210011SdhAuVcBerController)self;
    Tha60210011ModuleBer module = (Tha60210011ModuleBer) AtBerControllerModuleGet(self);
    return mMethodsGet(controller)->CurrentBerOffset(controller) + Tha60210011BerMeasureStsChannelOffset(module);
    }

static eBool IsSd(AtBerController self)
    {
    AtChannel channel = AtBerControllerMonitoredChannel(self);
    if (AtChannelDefectGet(channel) & cAtPdhDe3AlarmSdBer)
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsSf(AtBerController self)
    {
    AtChannel channel = AtBerControllerMonitoredChannel(self);
    if (AtChannelDefectGet(channel) & cAtPdhDe3AlarmSfBer)
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsTca(AtBerController self)
    {
    AtChannel channel = AtBerControllerMonitoredChannel(self);
    if (AtChannelDefectGet(channel) & cAtPdhDe3AlarmBerTca)
        return cAtTrue;

    return cAtFalse;
    }

static uint32 HwChannelTypeValue(AtBerController self)
    {
    AtChannel pdhChannel = AtBerControllerMonitoredChannel((AtBerController)self);
    uint16 frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)pdhChannel);
    switch (frameType)
        {
        case cAtPdhE3Frmg832: return 18;
        case cAtPdhE3FrmG751: return 19;
        default: return 17;
        }
    }

static eAtRet Enable(AtBerController self, eBool enable)
    {
    eAtRet ret = m_AtBerControllerMethods->Enable(self, enable);
    if ((ret == cAtOk) && (enable))
        BerDe3PathModeSet(self, IsLineLayer(self) ? cAtFalse : cAtTrue);

    return ret;
    }

static eBool IsEnabled(AtBerController self)
    {
    if (IsLineMode(self) != IsLineLayer(self))
        return cAtFalse;

    return m_AtBerControllerMethods->IsEnabled(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60210061PdhDe3PathBerController object = mThis(self);
    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeUInt(isLineLayer);
    }

static void OverrideAtObject(AtBerController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtBerController(AtBerController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtBerControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtBerControllerOverride, m_AtBerControllerMethods, sizeof(m_AtBerControllerOverride));

        mMethodOverride(m_AtBerControllerOverride, Enable);
        mMethodOverride(m_AtBerControllerOverride, IsEnabled);
        mMethodOverride(m_AtBerControllerOverride, MeasureEngineId);
        mMethodOverride(m_AtBerControllerOverride, IsSd);
        mMethodOverride(m_AtBerControllerOverride, IsSf);
        mMethodOverride(m_AtBerControllerOverride, IsTca);
        }

    mMethodsSet(self, &m_AtBerControllerOverride);
    }

static void OverrideTha60210011SdhAuVcBerController(AtBerController self)
    {
    Tha60210011SdhAuVcBerController auvcController = (Tha60210011SdhAuVcBerController)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011SdhAuVcBerControllerOverride, mMethodsGet(auvcController), sizeof(m_Tha60210011SdhAuVcBerControllerOverride));

        mMethodOverride(m_Tha60210011SdhAuVcBerControllerOverride, BerControlOffset);
        mMethodOverride(m_Tha60210011SdhAuVcBerControllerOverride, CurrentBerOffset);
        mMethodOverride(m_Tha60210011SdhAuVcBerControllerOverride, HwChannelTypeValue);
        }

    mMethodsSet(auvcController, &m_Tha60210011SdhAuVcBerControllerOverride);
    }

static void Override(AtBerController self)
    {
    OverrideAtObject(self);
    OverrideAtBerController(self);
    OverrideTha60210011SdhAuVcBerController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061PdhDe3PathBerController);
    }

AtBerController Tha60210061PdhDe3PathBerObjectInit(AtBerController self, uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011SdhTu3VcBerControllerObjectInit(self, controllerId, channel, berModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;
    return self;
    }

AtBerController Tha60210061PdhDe3PathBerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtBerController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60210061PdhDe3PathBerObjectInit(newController, controllerId, channel, berModule);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : Tha60210061SdhLineBerController.c
 *
 * Created Date: Sep 29, 2015
 *
 * Description : SDH Line BER
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/ber/Tha60210011SdhAuVcBerControllerInternal.h"
#include "../poh/Tha60210061ModulePoh.h"
#include "Tha60210061ModuleBer.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061SdhLineBerController
    {
    tTha60210011SdhAuVcBerController super;
    }tTha60210061SdhLineBerController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtBerControllerMethods                 m_AtBerControllerOverride;
static tTha60210011SdhAuVcBerControllerMethods m_Tha60210011SdhAuVcBerControllerOverride;

/* Cache super */
static const tAtBerControllerMethods          *m_AtBerControllerMethods = NULL;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 CurrentBerOffset(Tha60210011SdhAuVcBerController self)
    {
    uint32 hwStsId, hwOcId;
    AtSdhLine line = (AtSdhLine)AtBerControllerMonitoredChannel((AtBerController)self);
    Tha60210061ModulePohLineHwIdGet(line, &hwOcId, &hwStsId);
    return ((hwStsId * 2UL) + (hwOcId * 128UL));
    }

static uint32 BerControlOffset(Tha60210011SdhAuVcBerController self)
    {
    uint32 hwStsId, hwOcId;
    AtSdhLine line = (AtSdhLine)AtBerControllerMonitoredChannel((AtBerController)self);
    Tha60210061ModulePohLineHwIdGet(line, &hwOcId, &hwStsId);
    return ((hwStsId * 8UL) + (hwOcId * 512UL));
    }

static uint32 MeasureEngineId(AtBerController self)
    {
    Tha60210011SdhAuVcBerController controller = (Tha60210011SdhAuVcBerController)self;
    Tha60210011ModuleBer module = (Tha60210011ModuleBer) AtBerControllerModuleGet(self);
    return mMethodsGet(controller)->CurrentBerOffset(controller) + Tha60210011BerMeasureStsChannelOffset(module);
    }


static eBool IsSd(AtBerController self)
    {
    AtChannel channel = AtBerControllerMonitoredChannel(self);
    if (AtChannelDefectGet(channel) & cAtSdhLineAlarmBerSd)
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsSf(AtBerController self)
    {
    AtChannel channel = AtBerControllerMonitoredChannel(self);
    if (AtChannelDefectGet(channel) & cAtSdhLineAlarmBerSf)
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsTca(AtBerController self)
    {
    AtChannel channel = AtBerControllerMonitoredChannel(self);
    if (AtChannelDefectGet(channel) & cAtSdhLineAlarmBerTca)
        return cAtTrue;

    return cAtFalse;
    }

static uint32 HwChannelTypeValue(AtBerController self)
    {
    AtChannel line = AtBerControllerMonitoredChannel((AtBerController)self);
    eAtSdhLineRate rate = AtSdhLineRateGet((AtSdhLine) line);
    switch ((uint32)rate)
        {
        case cAtSdhLineRateStm0:
            return 0;
        case cAtSdhLineRateStm1:
            return cOc3Threshold1Rate;
        case cAtSdhLineRateStm4:
            return cOc12Threshold1Rate;
        case cAtSdhLineRateStm16:
            return cOc48Threshold1Rate;
        default:
            return 0;
        }
    }

static eBool ThresholdIsSupported(Tha60210011SdhAuVcBerController self, eAtBerRate threshold)
    {
    AtUnused(self);
    if ((threshold >= cAtBerRate1E3) && (threshold <= cAtBerRate1E10))
        return cAtTrue;
        
    return cAtFalse;
    }

static void OverrideAtBerController(AtBerController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtBerControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtBerControllerOverride, m_AtBerControllerMethods, sizeof(m_AtBerControllerOverride));

        mMethodOverride(m_AtBerControllerOverride, MeasureEngineId);
        mMethodOverride(m_AtBerControllerOverride, IsSd);
        mMethodOverride(m_AtBerControllerOverride, IsSf);
        mMethodOverride(m_AtBerControllerOverride, IsTca);
        }

    mMethodsSet(self, &m_AtBerControllerOverride);
    }

static void OverrideTha60210011SdhAuVcBerController(AtBerController self)
    {
    Tha60210011SdhAuVcBerController auvcController = (Tha60210011SdhAuVcBerController)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011SdhAuVcBerControllerOverride, mMethodsGet(auvcController), sizeof(m_Tha60210011SdhAuVcBerControllerOverride));

        mMethodOverride(m_Tha60210011SdhAuVcBerControllerOverride, CurrentBerOffset);
        mMethodOverride(m_Tha60210011SdhAuVcBerControllerOverride, BerControlOffset);
        mMethodOverride(m_Tha60210011SdhAuVcBerControllerOverride, HwChannelTypeValue);
        mMethodOverride(m_Tha60210011SdhAuVcBerControllerOverride, ThresholdIsSupported);
        }

    mMethodsSet(auvcController, &m_Tha60210011SdhAuVcBerControllerOverride);
    }

static void Override(AtBerController self)
    {
    OverrideAtBerController(self);
    OverrideTha60210011SdhAuVcBerController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061SdhLineBerController);
    }

static AtBerController ObjectInit(AtBerController self, uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011SdhAuVcBerControllerObjectInit(self, controllerId, channel, berModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtBerController Tha60210061SdhLineBerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtBerController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, controllerId, channel, berModule);
    }

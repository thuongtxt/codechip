/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : BER
 * 
 * File        : Tha60210061ModuleBer.h
 * 
 * Created Date: Nov 3, 2016
 *
 * Description : BER
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061MODULEBER_H_
#define _THA60210061MODULEBER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtBerController Tha60210061SdhLineBerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule);
AtBerController Tha60210061PdhDe3PathBerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule);
AtBerController Tha60210061PdhDe3LineBerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule);
AtBerController Tha60210061PdhDe1PathBerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule);
AtBerController Tha60210061PdhDe1LineBerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061MODULEBER_H_ */


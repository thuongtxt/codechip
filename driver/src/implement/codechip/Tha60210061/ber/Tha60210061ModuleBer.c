/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : Tha60210061ModuleBer.c
 *
 * Created Date: Jul 6, 2016
 *
 * Description : 60210061 Module BER
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPdhChannel.h"
#include "../../Tha60210012/ber/Tha60210012ModuleBer.h"
#include "../../Tha60210021/ber/Tha60210021ModuleBer.h"
#include "../../Tha60210031/ber/Tha60210031ModuleBer.h"
#include "../pdh/Tha60210061ModulePdh.h"
#include "Tha60210061ModuleBer.h"
#include "Tha60210061ModuleBerReg.h"

/*--------------------------- Define -----------------------------------------*/
/* Define for LIU DS1/E1 in Mixing with SDH */
#define cLineDs1Threshold2Rate        24
#define cLineE1Threshold2Rate         25

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061ModuleBer
    {
    tTha60210012ModuleBer super;
    }tTha60210061ModuleBer;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleBerMethods           m_AtModuleBerOverride;
static tThaModuleHardBerMethods      m_ThaModuleHardBerOverride;
static tTha60210011ModuleBerMethods  m_Tha60210011ModuleBerOverride;

/* Save super implementation */
static const tAtModuleBerMethods          *m_AtModuleBerMethods = NULL;
static const tThaModuleHardBerMethods     *m_ThaModuleHardBerMethods = NULL;
static const tTha60210011ModuleBerMethods *m_Tha60210011ModuleBerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtBerController De1PathBerControllerCreate(ThaModuleHardBer self, AtPdhChannel pdhChannel)
    {
    if (Tha60210061ModulePdhChannelIsLiuChannel((AtPdhChannel)pdhChannel))
        return Tha60210061PdhDe1PathBerControllerNew(AtChannelIdGet((AtChannel)pdhChannel), (AtChannel)pdhChannel, (AtModuleBer)self);

    return m_ThaModuleHardBerMethods->De1PathBerControllerCreate(self, pdhChannel);
    }

static AtBerController De1LineBerControllerCreate(ThaModuleHardBer self, AtPdhChannel pdhChannel)
    {
    if (Tha60210061ModulePdhChannelIsLiuChannel((AtPdhChannel)pdhChannel))
        return Tha60210061PdhDe1LineBerControllerNew(AtChannelIdGet((AtChannel)pdhChannel), (AtChannel)pdhChannel, (AtModuleBer)self);

    return NULL;
    }

static AtBerController De3PathBerControllerCreate(ThaModuleHardBer self, AtPdhChannel pdhChannel)
    {
    if (Tha60210061ModulePdhChannelIsLiuChannel((AtPdhChannel)pdhChannel))
        return Tha60210061PdhDe3PathBerControllerNew(AtChannelIdGet((AtChannel)pdhChannel), (AtChannel)pdhChannel, (AtModuleBer)self);

    return m_ThaModuleHardBerMethods->De3PathBerControllerCreate(self, pdhChannel);
    }

static AtBerController De3LineBerControllerCreate(ThaModuleHardBer self, AtPdhChannel pdhChannel)
    {
    if (Tha60210061ModulePdhChannelIsLiuChannel((AtPdhChannel)pdhChannel))
        return Tha60210061PdhDe3LineBerControllerNew(AtChannelIdGet((AtChannel)pdhChannel), (AtChannel)pdhChannel, (AtModuleBer)self);
    return NULL;
    }

static AtBerController SdhLineMsBerControlerCreate(AtModuleBer self, AtChannel line)
    {
    return Tha60210061SdhLineBerControllerNew(AtChannelIdGet(line), line, self);
    }

static void BerOc3ThresholdStandardClearTimeInit(Tha60210011ModuleBer self)
    {
    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cOc3Threshold1Rate),     cOc3Threshold1);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc3Threshold2Rate),     cOc3Threshold2_0);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc3Threshold2Rate) + 1, cOc3Threshold2_1);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc3Threshold2Rate) + 2, cOc3Threshold2_2);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc3Threshold2Rate) + 3, cOc3Threshold2_3);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc3Threshold2Rate) + 4, cOc3Threshold2_4);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc3Threshold2Rate) + 5, cOc3Threshold2_5);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc3Threshold2Rate) + 6, cOc3Threshold2_6);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc3Threshold2Rate) + 7, cOc3Threshold2_7);
    }

static void BerOc12ThresholdStandardClearTimeInit(Tha60210011ModuleBer self)
    {
    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cOc12Threshold1Rate),     cOc12Threshold1);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc12Threshold2Rate),     cOc12Threshold2_0);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc12Threshold2Rate) + 1, cOc12Threshold2_1);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc12Threshold2Rate) + 2, cOc12Threshold2_2);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc12Threshold2Rate) + 3, cOc12Threshold2_3);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc12Threshold2Rate) + 4, cOc12Threshold2_4);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc12Threshold2Rate) + 5, cOc12Threshold2_5);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc12Threshold2Rate) + 6, cOc12Threshold2_6);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc12Threshold2Rate) + 7, cOc12Threshold2_7);
    }


static void BerOc48ThresholdStandardClearTimeInit(Tha60210011ModuleBer self)
    {
    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cOc48Threshold1Rate),     cOc48Threshold1);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc48Threshold2Rate),     cOc48Threshold2_0);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc48Threshold2Rate) + 1, cOc48Threshold2_1);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc48Threshold2Rate) + 2, cOc48Threshold2_2);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc48Threshold2Rate) + 3, cOc48Threshold2_3);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc48Threshold2Rate) + 4, cOc48Threshold2_4);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc48Threshold2Rate) + 5, cOc48Threshold2_5);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc48Threshold2Rate) + 6, cOc48Threshold2_6);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc48Threshold2Rate) + 7, cOc48Threshold2_7);
    }


static void BerLineDs1ThresholdStandardClearTimeInit(Tha60210011ModuleBer self)
    {
    mModuleHwWrite(self, AtModuleBerBaseAddress((AtModuleBer)self) + cAf6Reg_pcfg_de1liutrsh_Base, cLineDs1Threshold1);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs1Threshold2Rate),      cLineDs1Threshold2_0);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs1Threshold2Rate) + 1,  cLineDs1Threshold2_1);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs1Threshold2Rate) + 2,  cLineDs1Threshold2_2);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs1Threshold2Rate) + 3,  cLineDs1Threshold2_3);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs1Threshold2Rate) + 4,  cLineDs1Threshold2_4);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs1Threshold2Rate) + 5,  cLineDs1Threshold2_5);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs1Threshold2Rate) + 6,  cLineDs1Threshold2_6);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs1Threshold2Rate) + 7,  cLineDs1Threshold2_7);
    }


static void BerLineE1ThresholdStandardClearTimeInit(Tha60210011ModuleBer self)
    {
    mModuleHwWrite(self, AtModuleBerBaseAddress((AtModuleBer)self) + cAf6Reg_pcfg_de1liutrsh_Base, cLineE1Threshold1);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE1Threshold2Rate),       cLineE1Threshold2_0);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE1Threshold2Rate) + 1,   cLineE1Threshold2_1);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE1Threshold2Rate) + 2,   cLineE1Threshold2_2);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE1Threshold2Rate) + 3,   cLineE1Threshold2_3);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE1Threshold2Rate) + 4,   cLineE1Threshold2_4);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE1Threshold2Rate) + 5,   cLineE1Threshold2_5);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE1Threshold2Rate) + 6,   cLineE1Threshold2_6);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE1Threshold2Rate) + 7,   cLineE1Threshold2_7);
    }


static void BerOc3ThresholdInit(Tha60210011ModuleBer self)
    {
    if (mMethodsGet(self)->HasStandardClearTime(self))
        {
        BerOc3ThresholdStandardClearTimeInit(self);
        return;
        }

    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cOc3Threshold1Rate),     0x2DB0166);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc3Threshold2Rate),     0xEC0076);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc3Threshold2Rate) + 1, 0xEC0076);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc3Threshold2Rate) + 2, 0x7B);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc3Threshold2Rate) + 3, 0x98C0578);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc3Threshold2Rate) + 4, 0xA40059A);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc3Threshold2Rate) + 5, 0xA5205A5);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc3Threshold2Rate) + 6, 0xA5405A6);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc3Threshold2Rate) + 7, 0xA5405A6);
    }

static void BerOc12ThresholdInit(Tha60210011ModuleBer self)
    {
    if (mMethodsGet(self)->HasStandardClearTime(self))
        {
        BerOc12ThresholdStandardClearTimeInit(self);
        return;
        }

    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cOc12Threshold1Rate),     0xBB405CB);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc12Threshold2Rate),     0x3FC0202);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc12Threshold2Rate) + 1, 0x4040202);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc12Threshold2Rate) + 2, 0x212);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc12Threshold2Rate) + 3, 0x271615CC);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc12Threshold2Rate) + 4, 0x29F615EE);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc12Threshold2Rate) + 5, 0x2A421617);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc12Threshold2Rate) + 6, 0x2A4A161B);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc12Threshold2Rate) + 7, 0x2A4C161B);
    }

static void BerOc48ThresholdInit(Tha60210011ModuleBer self)
    {
    if (mMethodsGet(self)->HasStandardClearTime(self))
        {
        BerOc48ThresholdStandardClearTimeInit(self);
        return;
        }

    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cOc48Threshold1Rate),        0x2F601792);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc48Threshold2Rate),        0x108E0857);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc48Threshold2Rate) + 1,    0x10B00858);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc48Threshold2Rate) + 2,    0x896);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc48Threshold2Rate) + 3,    0x9E1C56AE);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc48Threshold2Rate) + 4,    0xA9BC56C5);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc48Threshold2Rate) + 5,    0xAAF65765);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc48Threshold2Rate) + 6,    0xAB165776);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc48Threshold2Rate) + 7,    0xAB1A5777);
    }

static void BerLineDs1ThresholdInit(Tha60210011ModuleBer self)
    {
    if (mMethodsGet(self)->HasStandardClearTime(self))
        {
        BerLineDs1ThresholdStandardClearTimeInit(self);
        return;
        }

    mModuleHwWrite(self, AtModuleBerBaseAddress((AtModuleBer)self) + cAf6Reg_pcfg_de1liutrsh_Base, 0x431E3015);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs1Threshold2Rate),     0x20001);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs1Threshold2Rate) + 1, 0x20001);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs1Threshold2Rate) + 2, 0x2);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs1Threshold2Rate) + 3, 0x560044);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs1Threshold2Rate) + 4, 0x560044);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs1Threshold2Rate) + 5, 0x560044);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs1Threshold2Rate) + 6, 0x560044);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs1Threshold2Rate) + 7, 0x560044);
    }

static void BerLineE1ThresholdInit(Tha60210011ModuleBer self)
    {
    if (mMethodsGet(self)->HasStandardClearTime(self))
        {
        BerLineE1ThresholdStandardClearTimeInit(self);
        return;
        }

    mModuleHwWrite(self, AtModuleBerBaseAddress((AtModuleBer)self) + cAf6Reg_pcfg_de1liutrsh_Base, 0x431E3015);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE1Threshold2Rate),    0x40002);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE1Threshold2Rate) + 1,0x40002);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE1Threshold2Rate) + 2,0x3);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE1Threshold2Rate) + 3,0x5a0058);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE1Threshold2Rate) + 4,0x5a0058);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE1Threshold2Rate) + 5,0x5a0058);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE1Threshold2Rate) + 6,0x5a0058);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE1Threshold2Rate) + 7,0x5a0058);
    }

static void DefaultSet(Tha60210011ModuleBer self)
    {
    m_Tha60210011ModuleBerMethods->DefaultSet(self);
    Tha60210031ModuleBerBerLineEc1ThresholdInit(self);
    BerOc3ThresholdInit(self);
    BerOc12ThresholdInit(self);
    BerOc48ThresholdInit(self);
    BerLineDs1ThresholdInit(self);
    BerLineE1ThresholdInit(self);
    Tha60210031ModuleBerLineDs3ThresholdInit(self);
    Tha60210031ModuleBerLineE3ThresholdInit(self);
    }

static void OverrideTha60210011ModuleBer(AtModuleBer self)
    {
    Tha60210011ModuleBer module = (Tha60210011ModuleBer)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011ModuleBerMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModuleBerOverride, m_Tha60210011ModuleBerMethods, sizeof(m_Tha60210011ModuleBerOverride));

        mMethodOverride(m_Tha60210011ModuleBerOverride, DefaultSet);
        }

    mMethodsSet(module, &m_Tha60210011ModuleBerOverride);
    }

static void OverrideThaModuleHardBer(AtModuleBer self)
    {
    ThaModuleHardBer module = (ThaModuleHardBer)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleHardBerMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleHardBerOverride, mMethodsGet(module), sizeof(m_ThaModuleHardBerOverride));

        mMethodOverride(m_ThaModuleHardBerOverride, De1PathBerControllerCreate);
        mMethodOverride(m_ThaModuleHardBerOverride, De3PathBerControllerCreate);
        mMethodOverride(m_ThaModuleHardBerOverride, De1LineBerControllerCreate);
        mMethodOverride(m_ThaModuleHardBerOverride, De3LineBerControllerCreate);
        }

    mMethodsSet(module, &m_ThaModuleHardBerOverride);
    }

static void OverrideAtModuleBer(AtModuleBer self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleBerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleBerOverride, m_AtModuleBerMethods, sizeof(m_AtModuleBerOverride));

        mMethodOverride(m_AtModuleBerOverride, SdhLineMsBerControlerCreate);
        }

    mMethodsSet(self, &m_AtModuleBerOverride);
    }

static void Override(AtModuleBer self)
    {
    OverrideAtModuleBer(self);
    OverrideThaModuleHardBer(self);
    OverrideTha60210011ModuleBer(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061ModuleBer);
    }

static AtModuleBer ObjectInit(AtModuleBer self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012ModuleBerObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleBer Tha60210061ModuleBerNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleBer newBerModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newBerModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newBerModule, device);
    }

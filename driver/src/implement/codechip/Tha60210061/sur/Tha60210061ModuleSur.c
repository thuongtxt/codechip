/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : Tha60210061ModuleSur.c
 *
 * Created Date: Aug 26, 2016
 *
 * Description : 60210061 module SUR
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../../../default/sur/hard/ThaSurInterruptManager.h"
#include "../../Tha60210012/sur/Tha60210012ModuleSur.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061ModuleSur
    {
    tTha60210012ModuleSur super;
    }tTha60210061ModuleSur;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods             m_AtModuleOverride;
static tAtModuleSurMethods          m_AtModuleSurOverride;
static tThaModuleHardSurMethods     m_ThaModuleHardSurOverride;
static tTha60210012ModuleSurMethods m_Tha60210012ModuleSurOverride;

/* Super implementation */
static const tAtModuleMethods *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool ExternalCounterIsSupported(Tha60210012ModuleSur self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool LineIsSupported(AtModuleSur self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static AtSurEngine SdhLineSurEngineObjectCreate(ThaModuleHardSur self, AtChannel line)
    {
    return ThaHardSurEngineSdhLineNew((AtModuleSur)self, line);
    }

static uint32 Clock125usDefaultValue(ThaModuleHardSur self)
    {
    AtUnused(self);
    return 0x556C;
    }

static AtInterruptManager InterruptManagerCreate(AtModule self, uint32 managerIndex)
    {
    AtUnused(managerIndex);
    return Tha60210061SurInterruptManagerNew(self);
    }

static eBool ShouldActivateFeatureFromVersion(AtModuleSur self, uint32 major, uint32 minor, uint32 betaBuild)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    uint32 startVersionHasThis = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(major, minor, betaBuild);
    return (currentVersion >= startVersionHasThis) ? cAtTrue : cAtFalse;
    }

static eBool FailureTimersConfigurable(AtModuleSur self)
    {
    return ShouldActivateFeatureFromVersion(self, 0x3, 0x4, 0x1000);
    }

static eBool FailureHoldOffTimerConfigurable(AtModuleSur self)
    {
    return FailureTimersConfigurable(self);
    }

static eBool FailureHoldOnTimerConfigurable(AtModuleSur self)
    {
    return FailureTimersConfigurable(self);
    }

static eBool FailureForwardingStatusIsSupported(ThaModuleHardSur self)
    {
    return ShouldActivateFeatureFromVersion((AtModuleSur)self, 0x3, 0x4, 0x1009);
    }

static void OverrideAtModule(AtModuleSur self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, InterruptManagerCreate);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideTha60210012ModuleSur(AtModuleSur self)
    {
    Tha60210012ModuleSur moduleSur = (Tha60210012ModuleSur)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210012ModuleSurOverride, mMethodsGet(moduleSur), sizeof(m_Tha60210012ModuleSurOverride));

        mMethodOverride(m_Tha60210012ModuleSurOverride, ExternalCounterIsSupported);
        }

    mMethodsSet(moduleSur, &m_Tha60210012ModuleSurOverride);
    }

static void OverrideAtModuleSur(AtModuleSur self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleSurOverride, mMethodsGet(self), sizeof(m_AtModuleSurOverride));

        mMethodOverride(m_AtModuleSurOverride, LineIsSupported);
        mMethodOverride(m_AtModuleSurOverride, FailureHoldOffTimerConfigurable);
        mMethodOverride(m_AtModuleSurOverride, FailureHoldOnTimerConfigurable);
        }

    mMethodsSet(self, &m_AtModuleSurOverride);
    }

static void OverrideThaModuleHardHardSur(AtModuleSur self)
    {
    ThaModuleHardSur surModule = (ThaModuleHardSur)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleHardSurOverride, mMethodsGet(surModule), sizeof(m_ThaModuleHardSurOverride));

        mMethodOverride(m_ThaModuleHardSurOverride, SdhLineSurEngineObjectCreate);
        mMethodOverride(m_ThaModuleHardSurOverride, Clock125usDefaultValue);
        mMethodOverride(m_ThaModuleHardSurOverride, FailureForwardingStatusIsSupported);
        }

    mMethodsSet(surModule, &m_ThaModuleHardSurOverride);
    }

static void Override(AtModuleSur self)
    {
    OverrideAtModule(self);
    OverrideAtModuleSur(self);
    OverrideThaModuleHardHardSur(self);
    OverrideTha60210012ModuleSur(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061ModuleSur);
    }

static AtModuleSur ObjectInit(AtModuleSur self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012ModuleSurObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSur Tha60210061ModuleSurNew(AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSur module = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (module == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(module, device);
    }

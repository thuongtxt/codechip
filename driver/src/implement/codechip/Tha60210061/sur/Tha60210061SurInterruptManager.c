/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : Tha60210061SurInterruptManager.c
 *
 * Created Date: Jun 13, 2017
 *
 * Description : SUR interrupt manager
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtChannel.h"
#include "../../../default/sur/hard/ThaSurInterruptManagerInternal.h"
#include "../../../default/sur/hard/ThaModuleHardSurReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210061SurInterruptManager)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061SurInterruptManager * Tha60210061SurInterruptManager;
typedef struct tTha60210061SurInterruptManager
	{
	tThaSurInterruptManager super;
	}tTha60210061SurInterruptManager;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/*--------------------------- Forward declarations ---------------------------*/
/* Override */
static tThaInterruptManagerMethods    m_ThaInterruptManagerOverride;
static tThaSurInterruptManagerMethods m_ThaSurInterruptManagerOverride;

/* Super implementation */
static const tThaInterruptManagerMethods    *m_ThaInterruptManagerMethods = NULL;
static const tThaSurInterruptManagerMethods *m_ThaSurInterruptManagerMethods = NULL;

/*--------------------------- Implementation ---------------------------------*/
static uint32 InterruptMask(ThaSurInterruptManager self)
    {
    AtUnused(self);
    return (cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_ECSTM_Mask  |
            cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_VTGTUG_Mask |
            cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_STSAU_Mask  |
            cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_DS3E3_Mask  |
            cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_DS1E1_Mask  |
            cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_PW_Mask);
    }

static eBool LineHasInterrupt(ThaSurInterruptManager self, uint32 glbIntr)
    {
    AtUnused(self);
    return (glbIntr & cAf6_FMPM_FM_Interrupt_Mask_FMPM_FM_Intr_MSK_ECSTM_Mask) ? cAtTrue : cAtFalse;
    }

static uint32 NumSlices(ThaInterruptManager self)
    {
    AtUnused(self);
    return 2;
    }

static uint32 NumStsInSlice(ThaInterruptManager self)
    {
    AtUnused(self);
    return 27;
    }

static void OverrideThaInterruptManager(AtInterruptManager self)
    {
    ThaInterruptManager manager = (ThaInterruptManager)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaInterruptManagerMethods = mMethodsGet(manager);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInterruptManagerOverride, m_ThaInterruptManagerMethods, sizeof(m_ThaInterruptManagerOverride));

        mMethodOverride(m_ThaInterruptManagerOverride, NumSlices);
        mMethodOverride(m_ThaInterruptManagerOverride, NumStsInSlice);
        }

    mMethodsSet(manager, &m_ThaInterruptManagerOverride);
    }

static void OverrideThaSurInterruptManager(AtInterruptManager self)
    {
    ThaSurInterruptManager manager = (ThaSurInterruptManager)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaSurInterruptManagerMethods = mMethodsGet(manager);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSurInterruptManagerOverride, m_ThaSurInterruptManagerMethods, sizeof(m_ThaSurInterruptManagerOverride));

        mMethodOverride(m_ThaSurInterruptManagerOverride, InterruptMask);
        mMethodOverride(m_ThaSurInterruptManagerOverride, LineHasInterrupt);
        }

    mMethodsSet(manager, &m_ThaSurInterruptManagerOverride);
    }

static void Override(AtInterruptManager self)
    {
    OverrideThaInterruptManager(self);
    OverrideThaSurInterruptManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061SurInterruptManager);
    }

static AtInterruptManager ObjectInit(AtInterruptManager self, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaSurInterruptManagerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInterruptManager Tha60210061SurInterruptManagerNew(AtModule module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInterruptManager newObject = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newObject, module);
    }

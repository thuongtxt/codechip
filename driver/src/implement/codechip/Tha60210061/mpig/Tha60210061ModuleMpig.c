/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MPIG
 *
 * File        : Tha60210061ModuleMpig.c
 *
 * Created Date: Jul 11, 2017
 *
 * Description : Module MPIG
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210012/mpig/Tha60210012ModuleMpigInternal.h"
#include "Tha60210061ModuleMpig.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061ModuleMpig
    {
    tTha60210012ModuleMpig super;
    }tTha60210061ModuleMpig;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60210012ModuleMpigMethods m_Tha60210012ModuleMpigOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eThaMpigQueuePriority DefaultPriorityOfQueue(Tha60210012ModuleMpig self, uint32 queueId)
    {
    const uint32 cQueueForPwService             = 0;
    const uint32 cQueueForPwHighPriority        = 6;
    const uint32 cQueueForEthPassThroughService = 7;

    AtUnused(self);
    if ((queueId == cQueueForPwService) ||
        (queueId == cQueueForPwHighPriority) ||
        (queueId == cQueueForEthPassThroughService))
        return cThaMpigQueuePriorityHighest;

    return cThaMpigQueuePriorityMedium;
    }

static void OverrideTha60210012ModuleMpig(AtModule self)
    {
    Tha60210012ModuleMpig mpig = (Tha60210012ModuleMpig)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210012ModuleMpigOverride, mMethodsGet(mpig), sizeof(m_Tha60210012ModuleMpigOverride));

        mMethodOverride(m_Tha60210012ModuleMpigOverride, DefaultPriorityOfQueue);
        }

    mMethodsSet(mpig, &m_Tha60210012ModuleMpigOverride);
    }

static void Override(AtModule self)
    {
    OverrideTha60210012ModuleMpig(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061ModuleMpig);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012ModuleMpigObjectInit(self, device) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210061ModuleMpigNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

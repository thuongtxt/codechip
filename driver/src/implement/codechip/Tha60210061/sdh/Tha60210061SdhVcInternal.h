/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha60210061SdhVcInternal.h
 * 
 * Created Date: Jun 29, 2017
 *
 * Description : SDH VC
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061SDHVCINTERNAL_H_
#define _THA60210061SDHVCINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210012/sdh/Tha60210012ModuleSdhInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210061SdhVc
    {
    tTha60210012Tfi5LineAuVc super;
    }tTha60210061SdhVc;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhVc Tha60210061SdhVcObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061SDHVCINTERNAL_H_ */


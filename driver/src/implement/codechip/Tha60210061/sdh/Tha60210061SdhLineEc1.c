/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60210061SdhLineEc1.c
 *
 * Created Date: Sep 23, 2016
 *
 * Description : SDH line for EC1 interface
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/sdh/AtSdhLineInternal.h"
#include "../../../../implement/default/poh/ThaModulePoh.h"
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60210011/ocn/Tha60210011ModuleOcnInternal.h"
#include "../../Tha60210031/sdh/Tha60210031SdhLineInternal.h"
#include "../../Tha60210031/pdh/Tha60210031PdhDe3.h"
#include "../ocn/Tha60210061ModuleOcnEc1Reg.h"
#include "../ocn/Tha60210061ModuleOcn.h"
#include "Tha60210061ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061SdhLineEc1
    {
    tTha60210031SdhLine super;
    }tTha60210061SdhLineEc1;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods    m_AtChannelOverride;
static tAtSdhLineMethods    m_AtSdhLineOverride;
static tThaSdhLineMethods    m_ThaSdhLineOverride;
static tTha60210031SdhLineMethods    m_Tha60210031SdhLineOverride;
static tAtSdhChannelMethods m_AtSdhChannelOverride;

/* To save super implementation */
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 RxLineAlarmInterruptStatusRegAddr(Tha60210031SdhLine self)
    {
    AtUnused(self);
    return cAf6_Ec1Reg_tohintsta_Base;
    }

static uint32 RxLineAlarmInterruptEnableControlRegAddr(Tha60210031SdhLine self)
    {
    AtUnused(self);
    return cAf6_Ec1Reg_tohintperalrenbctl_Base;
    }

static uint32 RxLineAlarmCurrentStatusRegAddr(Tha60210031SdhLine self)
    {
    AtUnused(self);
    return cAf6_Ec1Reg_tohcursta_Base;
    }

static uint32 TohMonitoringChannelControlRegAddr(Tha60210031SdhLine self)
    {
    AtUnused(self);
    return cAf6_Ec1Reg_tohramctl_Base;
    }

static uint32 B1CounterRegAddr(Tha60210031SdhLine self, eBool r2c)
    {
    AtUnused(self);
    return r2c ? cAf6_Ec1Reg_tohb1errr2ccnt_Base : cAf6_Ec1Reg_tohb1errrocnt_Base;
    }

static uint32 B2CounterRegAddr(Tha60210031SdhLine self, eBool r2c)
    {
    AtUnused(self);
    return r2c ? cAf6_Ec1Reg_tohb2errr2ccnt_Base : cAf6_Ec1Reg_tohb2errrocnt_Base;
    }

static uint32 ReiCounterRegAddr(Tha60210031SdhLine self, eBool r2c)
    {
    AtUnused(self);
    return r2c ? cAf6_Ec1Reg_tohreierrr2ccnt_Base : cAf6_Ec1Reg_tohreierrrocnt_Base;
    }

static uint32 B1BlockCounterRegAddr(Tha60210031SdhLine self, eBool r2c)
    {
    AtUnused(self);
    return r2c ? cAf6_Ec1Reg_tohb1blkerrr2ccnt_Base : cAf6_Ec1Reg_tohb1blkerrrocnt_Base;
    }

static uint32 B2BlockCounterRegAddr(Tha60210031SdhLine self, eBool r2c)
    {
    AtUnused(self);
    return r2c ? cAf6_Ec1Reg_tohb2blkerrr2ccnt_Base : cAf6_Ec1Reg_tohb2blkerrrocnt_Base;
    }

static uint32 ReiBlockCounterRegAddr(Tha60210031SdhLine self, eBool r2c)
    {
    AtUnused(self);
    return r2c ? cAf6_Ec1Reg_tohreiblkerrr2ccnt_Base : cAf6_Ec1Reg_tohreiblkerrrocnt_Base;
    }

static uint32 TxFramerPerChannelControlRegAddr(Tha60210031SdhLine self)
    {
    AtUnused(self);
    return cAf6_Ec1Reg_tfmramctl_Base;
    }

static uint32 TohGlobalK1StableMonitoringThresholdControlRegAddr(Tha60210031SdhLine self)
    {
    AtUnused(self);
    return cAf6_Ec1Reg_tohglbk1stbthr_reg_Base;
    }

static uint32 TohK1MonitoringStatusRegAddr(Tha60210031SdhLine self)
    {
    AtUnused(self);
    return cAf6_Ec1Reg_tohk1monsta_Base;
    }

static uint32 TohGlobalK2StableMonitoringThresholdControlRegAddr(Tha60210031SdhLine self)
    {
    AtUnused(self);
    return cAf6_Ec1Reg_tohglbk2stbthr_reg_Base;
    }

static uint32 TohK2MonitoringStatusRegAddr(Tha60210031SdhLine self)
    {
    AtUnused(self);
    return cAf6_Ec1Reg_tohk2monsta_Base;
    }

static uint32 TohGlobalS1StableMonitoringThresholdControlRegAddr(Tha60210031SdhLine self)
    {
    AtUnused(self);
    return cAf6_Ec1Reg_tohglbs1stbthr_reg_Base;
    }

static uint32 TohS1MonitoringStatusRegAddr(Tha60210031SdhLine self)
    {
    AtUnused(self);
    return cAf6_Ec1Reg_tohs1monsta_Base;
    }

static ThaModulePoh ModulePoh(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return (ThaModulePoh) AtDeviceModuleGet(device, cThaModulePoh);
    }

static ThaModuleOcn ModuleOcn(AtSdhLine self)
    {
    return (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cThaModuleOcn);
    }

static uint32 Ec1HwIdConvert(AtSdhLine self)
    {
    return AtChannelIdGet((AtChannel)self) % 4;
    }

static uint32 DefaultOffset(ThaSdhLine self)
    {
    return Ec1HwIdConvert((AtSdhLine)self) + Tha60210061ModuleOcnEc1BaseAddress(ModuleOcn((AtSdhLine)self));
    }

static uint32 OcnBaseAddress(AtSdhLine self)
    {
    ThaModuleOcn moduleOcn = ModuleOcn(self);
    return Tha60210061ModuleOcnEc1BaseAddress(moduleOcn);
    }

static uint32 ChannelOffset(Tha60210031SdhLine self)
    {
    ThaModuleOcn ocnModule = ModuleOcn((AtSdhLine)self);
    uint8 lineId = (uint8)AtChannelIdGet((AtChannel)self);
    uint8 stsId = lineId % 4;
    return stsId + Tha60210061ModuleOcnEc1BaseAddress(ocnModule);
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return ThaModulePohLinePohMonEnable(ModulePoh(self), (AtSdhLine)self, cAtTrue);
    }

static eBool RateIsSupported(AtSdhLine self, eAtSdhLineRate rate)
    {
    AtUnused(self);
    return ((rate == cAtSdhLineRateStm0) ? cAtTrue : cAtFalse);
    }

static eBool LoopbackIsSupported(AtChannel self, uint8 loopbackMode)
    {
    AtUnused(self);
    AtUnused(loopbackMode);
    return cAtFalse;
    }

static eAtRet Enable(AtChannel self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtOk;
    }

static eAtRet SubChannelsInterruptDisable(AtSdhChannel self, eBool onlySubChannels)
    {
    AtUnused(self);
    AtUnused(onlySubChannels);
    return cAtOk;
    }

static eAtRet InterruptEnableHelper(AtChannel self, eBool enable)
    {
    uint32 regAddr, regVal;
    uint8 lineId = (uint8)AtChannelHwIdGet(self);
    uint8 hwEnable = (uint8)mBoolToBin(enable);

    regAddr = cAf6_Ec1Reg_tohintperlineenctl_Base;
    regAddr = regAddr + OcnBaseAddress((AtSdhLine)self);
    regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    mFieldIns(&regVal, cBit0 << lineId, lineId, hwEnable);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eAtRet InterruptEnable(AtChannel self)
    {
    return InterruptEnableHelper(self, cAtTrue);
    }

static eAtRet InterruptDisable(AtChannel self)
    {
    return InterruptEnableHelper(self, cAtFalse);
    }

static eBool InterruptIsEnabled(AtChannel self)
    {
    uint8 hwEnable;
    uint32 regAddr, regVal;
    uint8 lineId = (uint8)AtChannelIdGet(self);

    regAddr = cAf6_Ec1Reg_tohintperlineenctl_Base;
    regAddr = regAddr + OcnBaseAddress((AtSdhLine)self);
    regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    mFieldGet(regVal, cBit0 << lineId, lineId, uint8, &hwEnable);
    return mBinToBool(hwEnable);
    }

static void LineTimingEnable(AtSdhLine self, uint32 address, eBool enable)
    {
    uint32 lineId = AtChannelIdGet((AtChannel)self);
    uint32 regVal, mask, shift;
    uint32 regAddr  = address;

    mask = cBit0 << (lineId);
    shift = lineId;

    regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    mFieldIns(&regVal, mask, shift, enable ? 1:0);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);
    }

static eBool LineTimingIsEnabled(AtSdhLine self, uint32 address)
    {
    uint32 lineId = AtChannelIdGet((AtChannel)self);
    uint32 regVal, mask, shift;
    uint32 regAddr  = address;
    uint8 ret;

    mask = cBit0 << (lineId);
    shift = lineId;

    regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    mFieldGet(regVal, mask, shift, uint8, &ret);

    return (ret == 1) ? cAtTrue:cAtFalse;
    }

static void OcnEc1TimingSourceSet(AtSdhLine line, eAtTimingMode timingMode)
    {
    uint32 extAddress  = cAf6_Ec1Reg_glbclksel_reg1_Base + OcnBaseAddress(line);
    uint32 looptimeAddr = cAf6_Ec1Reg_glbclklooptime_reg1_Base + OcnBaseAddress(line);

    switch ((uint32)timingMode)
        {
        case cAtTimingModeSys:
            LineTimingEnable(line, extAddress, cAtFalse);
            LineTimingEnable(line, looptimeAddr, cAtFalse);
            break;

        case cAtTimingModeLoop:
            LineTimingEnable(line, extAddress, cAtFalse);
            LineTimingEnable(line, looptimeAddr, cAtTrue);
            break;

        case cAtTimingModeExt1Ref:
            LineTimingEnable(line, extAddress, cAtTrue);
            LineTimingEnable(line, looptimeAddr, cAtFalse);
            break;

        default:
            break;
        }
    }

static eAtTimingMode OcnEc1TimingSourceGet(AtSdhLine line)
    {
    uint32 ext1Address  = cAf6_Ec1Reg_glbclksel_reg1_Base + OcnBaseAddress(line);
    uint32 looltimeAddress = cAf6_Ec1Reg_glbclklooptime_reg1_Base + OcnBaseAddress(line);
    eBool ext1 = LineTimingIsEnabled(line, ext1Address);
    eBool loop = LineTimingIsEnabled(line, looltimeAddress);

    if (!ext1 && !loop)
        return cAtTimingModeSys;

    if (loop && !ext1)
        return cAtTimingModeLoop;

    if (ext1 && !loop)
        return cAtTimingModeExt1Ref;

    return cAtTimingModeUnknown;
    }

static AtChannel SerialLineGet(AtChannel self)
    {
    AtModulePdh modulePdh = (AtModulePdh)AtDeviceModuleGet(AtChannelDeviceGet(self), cAtModulePdh);
    return (AtChannel)AtModulePdhDe3SerialLineGet(modulePdh, AtChannelHwIdGet(self));
    }

static eAtRet TimingSet(AtChannel self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    AtUnused(timingSource);

    if (!AtChannelTimingModeIsSupported(self, timingMode))
        return cAtErrorModeNotSupport;

    OcnEc1TimingSourceSet((AtSdhLine)self, timingMode);
    AtChannelTimingSet(SerialLineGet(self), timingMode, timingSource);

    return cAtOk;
    }

static eAtTimingMode TimingModeGet(AtChannel self)
    {
    if (ThaModuleSdhLineTimingIsSupported((ThaModuleSdh)AtChannelModuleGet(self)))
        return OcnEc1TimingSourceGet((AtSdhLine)self);
    return cAtTimingModeUnknown;
    }

static uint32 HwIdGet(AtChannel self)
    {
    return (uint32)(AtChannelIdGet(self) - 4);
    }

static eBool Ec1TxAisForceIsSupported(Tha60210031SdhLine self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideAtChannel(AtSdhLine self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(m_AtChannelOverride));
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, LoopbackIsSupported);
        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, InterruptEnable);
        mMethodOverride(m_AtChannelOverride, InterruptDisable);
        mMethodOverride(m_AtChannelOverride, InterruptIsEnabled);
        mMethodOverride(m_AtChannelOverride, TimingSet);
        mMethodOverride(m_AtChannelOverride, TimingModeGet);
        mMethodOverride(m_AtChannelOverride, HwIdGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtSdhChannel(AtSdhLine self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, mMethodsGet(channel), sizeof(m_AtSdhChannelOverride));
        mMethodOverride(m_AtSdhChannelOverride, SubChannelsInterruptDisable);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void OverrideAtSdhLine(AtSdhLine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhLineOverride, mMethodsGet(self), sizeof(m_AtSdhLineOverride));
        mMethodOverride(m_AtSdhLineOverride, RateIsSupported);
        }

    mMethodsSet(self, &m_AtSdhLineOverride);
    }

static void OverrideThaSdhLine(AtSdhLine self)
    {
    ThaSdhLine line = (ThaSdhLine)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhLineOverride, mMethodsGet(line), sizeof(m_ThaSdhLineOverride));

        mMethodOverride(m_ThaSdhLineOverride, DefaultOffset);
        }

    mMethodsSet(line, &m_ThaSdhLineOverride);
    }

static void OverrideTha60210031SdhLine(AtSdhLine self)
    {
    Tha60210031SdhLine line = (Tha60210031SdhLine)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210031SdhLineOverride, mMethodsGet(line), sizeof(m_Tha60210031SdhLineOverride));

        mMethodOverride(m_Tha60210031SdhLineOverride, ChannelOffset);
        mMethodOverride(m_Tha60210031SdhLineOverride, RxLineAlarmCurrentStatusRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, RxLineAlarmInterruptEnableControlRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, RxLineAlarmInterruptStatusRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, TohMonitoringChannelControlRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, B1CounterRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, B2CounterRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, ReiCounterRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, B1BlockCounterRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, B2BlockCounterRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, ReiBlockCounterRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, TxFramerPerChannelControlRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, TohGlobalK1StableMonitoringThresholdControlRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, TohK1MonitoringStatusRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, TohGlobalK2StableMonitoringThresholdControlRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, TohK2MonitoringStatusRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, TohGlobalS1StableMonitoringThresholdControlRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, TohS1MonitoringStatusRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, Ec1TxAisForceIsSupported);
        }

    mMethodsSet(line, &m_Tha60210031SdhLineOverride);
    }

static void Override(AtSdhLine self)
    {
    OverrideAtChannel(self);
    OverrideAtSdhChannel(self);
    OverrideAtSdhLine(self);
    OverrideThaSdhLine(self);
    OverrideTha60210031SdhLine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061SdhLineEc1);
    }

static AtSdhLine ObjectInit(AtSdhLine self, uint32 channelId, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031SdhLineObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhLine Tha60210061SdhLineEc1New(uint32 channelId, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhLine newLine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newLine == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newLine, channelId, module);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60210061SdhLineAuVc.c
 *
 * Created Date: Aug 25, 2016
 *
 * Description : SDH Vc
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/ocn/ThaModuleOcn.h"
#include "../../Tha60210011/ocn/Tha60210011ModuleOcn.h"
#include "../../Tha60210051/poh/Tha60210051PohReg.h"
#include "../../Tha60210011/poh/Tha60210011ModulePoh.h"
#include "../../Tha60210011/xc/Tha60210011ModuleXc.h"
#include "../ocn/Tha60210061ModuleOcnReg.h"
#include "../ocn/Tha60210061ModuleOcnEc1Reg.h"
#include "Tha60210061ModuleSdh.h"
#include "Tha60210061SdhVcInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSdhChannelMethods            m_AtSdhChannelOverride;

/* Save super implementation */
static const tAtSdhChannelMethods     *m_AtSdhChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void UpsrLookupHoTableSet(AtSdhChannel self, uint8 stsId)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    ThaModulePoh modulePoh = (ThaModulePoh)AtDeviceModuleGet(device, cThaModulePoh);
    Tha60210011ModuleXc xcModule = (Tha60210011ModuleXc)AtDeviceModuleGet(device, cAtModuleXc);
    uint8 lineId = AtSdhChannelLineGet(self);
    uint8 hwSts, slice, tfi5Line, tfi5Sts, sts_i;
    uint32 regAddr, regVal = 0;
    eAtRet ret;

    for (sts_i = 0; sts_i < AtSdhChannelNumSts(self); sts_i++)
        {
        uint8 subSts = (uint8)(stsId + sts_i);

        ret = ThaSdhChannelHwStsGet(self, cAtModuleXc, subSts, &slice, &hwSts);
        if (ret != cAtOk)
            return;

        Tha60210011ModuleXcTfi5StsFromOcnStsGet((AtModuleXc)xcModule, 0, hwSts, &tfi5Line, &tfi5Sts);
        regAddr = Tha60210011ModulePohBaseAddress(modulePoh) + (uint32)cAf6Reg_upen_xxcfglks(tfi5Line, tfi5Sts);
        mRegFieldSet(regVal, cAf6_upen_xxcfglks_AMELksLine_, lineId);
        mRegFieldSet(regVal, cAf6_upen_xxcfglks_AMELksSts_, subSts);
        mModuleHwWrite(xcModule, regAddr, regVal);
        }
    }

static void Sts1IdSet(AtSdhChannel self, uint8 stsId)
    {
    m_AtSdhChannelMethods->Sts1IdSet(self, stsId);
    UpsrLookupHoTableSet(self, stsId);
    }

static void OverrideAtSdhChannel(AtSdhVc self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, mMethodsGet(channel), sizeof(m_AtSdhChannelOverride));

        mMethodOverride(m_AtSdhChannelOverride, Sts1IdSet);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void Override(AtSdhVc self)
    {
    OverrideAtSdhChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061SdhVc);
    }

AtSdhVc Tha60210061SdhVcObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012Tfi5LineAuVcObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhVc Tha60210061SdhVcNew(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhVc self = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (self == NULL)
        return NULL;

    /* Construct it */
    return Tha60210061SdhVcObjectInit(self, channelId, channelType, module);
    }

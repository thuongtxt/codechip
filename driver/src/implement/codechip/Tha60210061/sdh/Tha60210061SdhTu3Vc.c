/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60210061SdhTu3Vc.c
 *
 * Created Date: May 15, 2017
 *
 * Description : 60210061 TU3 VC
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210012/sdh/Tha60210012ModuleSdhInternal.h"
#include "../../Tha60210011/ocn/Tha60210011ModuleOcn.h"
#include "../ocn/Tha60210061ModuleOcnReg.h"
#include "../ocn/Tha60210061ModuleOcnEc1Reg.h"
#include "Tha60210061ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210061SdhTu3Vc)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061SdhTu3Vc * Tha60210061SdhTu3Vc;
typedef struct tTha60210061SdhTu3Vc
    {
    tTha60210012Tfi5LineTu3Vc super;
    }tTha60210061SdhTu3Vc;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaSdhVcMethods m_ThaSdhVcOverride;

/* Save super implementation */
static const tThaSdhVcMethods *m_ThaSdhVcMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleOcn ModuleOcn(AtChannel self)
    {
    return (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModuleOcn);
    }

static uint32 OcnPJCounterOffset(ThaSdhVc self, eBool isPosAdj)
    {
    uint8 slice, hwStsInSlice;
    uint8 adjMode = (isPosAdj) ? 0 : 1;

    if (ThaSdhChannel2HwMasterStsId((AtSdhChannel)self, cThaModuleOcn, &slice, &hwStsInSlice) == cAtOk)
        return (uint32)(2048UL * adjMode + 32UL * hwStsInSlice + Tha60210011ModuleOcnBaseAddress(ModuleOcn((AtChannel)self)));

    return cBit31_0;
    }

static void OverrideThaSdhVc(AtSdhVc self)
    {
    ThaSdhVc object = (ThaSdhVc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaSdhVcMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhVcOverride, m_ThaSdhVcMethods, sizeof(m_ThaSdhVcOverride));

        mMethodOverride(m_ThaSdhVcOverride, OcnPJCounterOffset);
        }

    mMethodsSet(object, &m_ThaSdhVcOverride);
    }

static void Override(AtSdhVc self)
    {
    OverrideThaSdhVc(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061SdhTu3Vc);
    }

static AtSdhVc ObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012Tfi5LineTu3VcObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhVc Tha60210061SdhTu3VcNew(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhVc newObject = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newObject, channelId, channelType, module);
    }

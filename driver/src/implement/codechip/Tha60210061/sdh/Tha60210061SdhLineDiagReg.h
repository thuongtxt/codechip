/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha60210061SdhLineDiagReg.h
 * 
 * Created Date: Jun 16, 2016
 *
 * Description : Diagnostic register of line
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061SDHLINEDIAGREG_H_
#define _THA60210061SDHLINEDIAGREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*------------------------------------------------------------------------------
Reg Name   : OCN Rx Framer Status
Reg Addr   : 0x00010 - 0x00040
Reg Formula: 0x00010 + 16*LineId
    Where  :
           + $LineId(0-3)
Reg Desc   :
Rx Framer status

------------------------------------------------------------------------------*/
#define cAf6Reg_rxfrmsta_Base                                                                          0x00010
#define cAf6Reg_rxfrmsta(LineId)                                                         (0x00010+16*(LineId))
#define cAf6Reg_rxfrmsta_WidthVal                                                                           32
#define cAf6Reg_rxfrmsta_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: OofCurStatus
BitField Type: RW
BitField Desc: OOF current status in the related line. 1: OOF state 0: Not OOF
state.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_rxfrmsta_OofCurStatus_Bit_Start                                                                 2
#define cAf6_rxfrmsta_OofCurStatus_Bit_End                                                                   2
#define cAf6_rxfrmsta_OofCurStatus_Mask                                                                  cBit2
#define cAf6_rxfrmsta_OofCurStatus_Shift                                                                     2
#define cAf6_rxfrmsta_OofCurStatus_MaxVal                                                                  0x1
#define cAf6_rxfrmsta_OofCurStatus_MinVal                                                                  0x0
#define cAf6_rxfrmsta_OofCurStatus_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: LofCurStatus
BitField Type: RW
BitField Desc: LOF current status in the related line. 1: LOF state 0: Not LOF
state.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_rxfrmsta_LofCurStatus_Bit_Start                                                                 1
#define cAf6_rxfrmsta_LofCurStatus_Bit_End                                                                   1
#define cAf6_rxfrmsta_LofCurStatus_Mask                                                                  cBit1
#define cAf6_rxfrmsta_LofCurStatus_Shift                                                                     1
#define cAf6_rxfrmsta_LofCurStatus_MaxVal                                                                  0x1
#define cAf6_rxfrmsta_LofCurStatus_MinVal                                                                  0x0
#define cAf6_rxfrmsta_LofCurStatus_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: LosCurStatus
BitField Type: RW
BitField Desc: LOS current status in the related line. 1: LOS state 0: Not LOS
state.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_rxfrmsta_LosCurStatus_Bit_Start                                                                 0
#define cAf6_rxfrmsta_LosCurStatus_Bit_End                                                                   0
#define cAf6_rxfrmsta_LosCurStatus_Mask                                                                  cBit0
#define cAf6_rxfrmsta_LosCurStatus_Shift                                                                     0
#define cAf6_rxfrmsta_LosCurStatus_MaxVal                                                                  0x1
#define cAf6_rxfrmsta_LosCurStatus_MinVal                                                                  0x0
#define cAf6_rxfrmsta_LosCurStatus_RstVal                                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx Framer Sticky
Reg Addr   : 0x00011 - 0x00041
Reg Formula: 0x00011 + 16*LineId
    Where  :
           + $LineId(0-3)
Reg Desc   :
Rx Framer sticky

------------------------------------------------------------------------------*/
#define cAf6Reg_rxfrmstk_Base                                                                          0x00011
#define cAf6Reg_rxfrmstk(LineId)                                                         (0x00011+16*(LineId))
#define cAf6Reg_rxfrmstk_WidthVal                                                                           32
#define cAf6Reg_rxfrmstk_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: Oc3AlgErrStk
BitField Type: W1C
BitField Desc: Set 1 while any error detected at OC3 Aligner engine.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_rxfrmstk_Oc3AlgErrStk_Bit_Start                                                                 4
#define cAf6_rxfrmstk_Oc3AlgErrStk_Bit_End                                                                   4
#define cAf6_rxfrmstk_Oc3AlgErrStk_Mask                                                                  cBit4
#define cAf6_rxfrmstk_Oc3AlgErrStk_Shift                                                                     4
#define cAf6_rxfrmstk_Oc3AlgErrStk_MaxVal                                                                  0x1
#define cAf6_rxfrmstk_Oc3AlgErrStk_MinVal                                                                  0x0
#define cAf6_rxfrmstk_Oc3AlgErrStk_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: ClkMonErrStk
BitField Type: W1C
BitField Desc: Set 1 while any error detected at Error clock monitor engine.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_rxfrmstk_ClkMonErrStk_Bit_Start                                                                 3
#define cAf6_rxfrmstk_ClkMonErrStk_Bit_End                                                                   3
#define cAf6_rxfrmstk_ClkMonErrStk_Mask                                                                  cBit3
#define cAf6_rxfrmstk_ClkMonErrStk_Shift                                                                     3
#define cAf6_rxfrmstk_ClkMonErrStk_MaxVal                                                                  0x1
#define cAf6_rxfrmstk_ClkMonErrStk_MinVal                                                                  0x0
#define cAf6_rxfrmstk_ClkMonErrStk_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: OofStateChgStk
BitField Type: W1C
BitField Desc: Set 1 while OOF state change event happens in the related line.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_rxfrmstk_OofStateChgStk_Bit_Start                                                               2
#define cAf6_rxfrmstk_OofStateChgStk_Bit_End                                                                 2
#define cAf6_rxfrmstk_OofStateChgStk_Mask                                                                cBit2
#define cAf6_rxfrmstk_OofStateChgStk_Shift                                                                   2
#define cAf6_rxfrmstk_OofStateChgStk_MaxVal                                                                0x1
#define cAf6_rxfrmstk_OofStateChgStk_MinVal                                                                0x0
#define cAf6_rxfrmstk_OofStateChgStk_RstVal                                                                0x0

/*--------------------------------------
BitField Name: LofStateChgStk
BitField Type: W1C
BitField Desc: Set 1 while LOF state change event happens in the related line.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_rxfrmstk_LofStateChgStk_Bit_Start                                                               1
#define cAf6_rxfrmstk_LofStateChgStk_Bit_End                                                                 1
#define cAf6_rxfrmstk_LofStateChgStk_Mask                                                                cBit1
#define cAf6_rxfrmstk_LofStateChgStk_Shift                                                                   1
#define cAf6_rxfrmstk_LofStateChgStk_MaxVal                                                                0x1
#define cAf6_rxfrmstk_LofStateChgStk_MinVal                                                                0x0
#define cAf6_rxfrmstk_LofStateChgStk_RstVal                                                                0x0

/*--------------------------------------
BitField Name: LosStateChgStk
BitField Type: W1C
BitField Desc: Set 1 while LOS state change event happens in the related line.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_rxfrmstk_LosStateChgStk_Bit_Start                                                               0
#define cAf6_rxfrmstk_LosStateChgStk_Bit_End                                                                 0
#define cAf6_rxfrmstk_LosStateChgStk_Mask                                                                cBit0
#define cAf6_rxfrmstk_LosStateChgStk_Shift                                                                   0
#define cAf6_rxfrmstk_LosStateChgStk_MaxVal                                                                0x1
#define cAf6_rxfrmstk_LosStateChgStk_MinVal                                                                0x0
#define cAf6_rxfrmstk_LosStateChgStk_RstVal                                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx Framer B1 error counter read only
Reg Addr   : 0x00012 - 0x00042
Reg Formula: 0x00012 + 16*LineId
    Where  :
           + $LineId(0-3)
Reg Desc   :
Rx Framer B1 error counter read only

------------------------------------------------------------------------------*/
#define cAf6Reg_rxfrmb1cntro_Base                                                                      0x00012
#define cAf6Reg_rxfrmb1cntro(LineId)                                                     (0x00012+16*(LineId))
#define cAf6Reg_rxfrmb1cntro_WidthVal                                                                       32
#define cAf6Reg_rxfrmb1cntro_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: RxFrmB1ErrRo
BitField Type: RO
BitField Desc: Number of B1 error - read only
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rxfrmb1cntro_RxFrmB1ErrRo_Bit_Start                                                             0
#define cAf6_rxfrmb1cntro_RxFrmB1ErrRo_Bit_End                                                              31
#define cAf6_rxfrmb1cntro_RxFrmB1ErrRo_Mask                                                           cBit31_0
#define cAf6_rxfrmb1cntro_RxFrmB1ErrRo_Shift                                                                 0
#define cAf6_rxfrmb1cntro_RxFrmB1ErrRo_MaxVal                                                       0xffffffff
#define cAf6_rxfrmb1cntro_RxFrmB1ErrRo_MinVal                                                              0x0
#define cAf6_rxfrmb1cntro_RxFrmB1ErrRo_RstVal                                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx Framer B1 error counter read to clear
Reg Addr   : 0x00013 - 0x00043
Reg Formula: 0x00013 + 16*LineId
    Where  :
           + $LineId(0-3)
Reg Desc   :
Rx Framer B1 error counter read to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_rxfrmb1cntr2c_Base                                                                     0x00013
#define cAf6Reg_rxfrmb1cntr2c(LineId)                                                    (0x00013+16*(LineId))
#define cAf6Reg_rxfrmb1cntr2c_WidthVal                                                                      32
#define cAf6Reg_rxfrmb1cntr2c_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: RxFrmB1ErrR2C
BitField Type: RC
BitField Desc: Number of B1 error - read to clear
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rxfrmb1cntr2c_RxFrmB1ErrR2C_Bit_Start                                                           0
#define cAf6_rxfrmb1cntr2c_RxFrmB1ErrR2C_Bit_End                                                            31
#define cAf6_rxfrmb1cntr2c_RxFrmB1ErrR2C_Mask                                                         cBit31_0
#define cAf6_rxfrmb1cntr2c_RxFrmB1ErrR2C_Shift                                                               0
#define cAf6_rxfrmb1cntr2c_RxFrmB1ErrR2C_MaxVal                                                     0xffffffff
#define cAf6_rxfrmb1cntr2c_RxFrmB1ErrR2C_MinVal                                                            0x0
#define cAf6_rxfrmb1cntr2c_RxFrmB1ErrR2C_RstVal                                                            0x0

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061SDHLINEDIAGREG_H_ */


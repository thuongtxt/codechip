/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha60210061ModuleSdh.h
 * 
 * Created Date: Aug 18, 2016
 *
 * Description : Tha60210061ModuleSdh declarations
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061MODULESDH_H_
#define _THA60210061MODULESDH_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhLine Tha60210061SdhLineEc1New(uint32 channelId, AtModuleSdh module);
AtSdhVc Tha60210061SdhAbstractVc1xNew(uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhVc Tha60210061SdhVcNew(uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhVc Tha60210061SdhVc1xNew(uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhAug Tha60210061SdhAugNew(uint32 augId, uint8 augType, AtModuleSdh module);
AtSdhVc Tha60210061SdhTu3VcNew(uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhVc Tha60210061SdhVcEc1New(uint32 channelId, uint8 channelType, AtModuleSdh module);

eBool Tha60210061ModuleSdhChannelIsFakeVc(AtSdhChannel vc);
eBool Tha60210061ModuleSdhChannelIsInEc1Line(AtModuleSdh self, AtSdhChannel channel);
eBool Tha60210061ModuleSdhLineLoopbackIsSupported(AtModuleSdh self);
eAtRet Tha60210061SdhLineDccBusEnable(AtSdhLine self, eBool enable);
eAtRet Tha60210061ModuleSdhEc1PathVirtualFifoCounterGet(AtSdhPath path, tAtSdhPathCounters* allCounters);
eAtRet Tha60210061ModuleSdhEc1PathVirtualFifoCounterClear(AtSdhPath path, tAtSdhPathCounters* allCounters);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061MODULESDH_H_ */


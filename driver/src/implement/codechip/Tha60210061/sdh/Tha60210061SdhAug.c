/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60210061SdhAug.c
 *
 * Created Date: Sep 26, 2016
 *
 * Description : 60210061 SDH AUG
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/sdh/Tha60210011Tfi5LineAugInternal.h"
#include "Tha60210061ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061SdhAug
    {
    tTha60210011Tfi5LineAug super;
    }tTha60210061SdhAug;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaSdhAugMethods m_ThaSdhAugOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool NeedPdhConcate(ThaSdhAug self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool NeedMapConcate(ThaSdhAug self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideThaSdhAug(AtSdhAug self)
    {
    ThaSdhAug aug = (ThaSdhAug)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhAugOverride, mMethodsGet(aug), sizeof(m_ThaSdhAugOverride));

        mMethodOverride(m_ThaSdhAugOverride, NeedPdhConcate);
        mMethodOverride(m_ThaSdhAugOverride, NeedMapConcate);
        }

    mMethodsSet(aug, &m_ThaSdhAugOverride);
    }

static void Override(AtSdhAug self)
    {
    OverrideThaSdhAug(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061SdhAug);
    }

static AtSdhAug ObjectInit(AtSdhAug self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011Tfi5LineAugObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhAug Tha60210061SdhAugNew(uint32 augId, uint8 augType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhAug self = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (self == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(self, augId, augType, module);
    }


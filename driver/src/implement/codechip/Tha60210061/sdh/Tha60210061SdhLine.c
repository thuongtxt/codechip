/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60210061SdhLine.c
 *
 * Created Date: Jun 22, 2016
 *
 * Description : SDH line implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/physical/AtSerdesControllerInternal.h"
#include "../../../../implement/default/poh/ThaModulePoh.h"
#include "../../../default/man/ThaDevice.h"
#include "../../../default/sdh/ThaSdhLine.h"
#include "../../Tha60210031/sdh/Tha60210031SdhLineInternal.h"
#include "../../Tha60210011/ocn/Tha60210011ModuleOcn.h"
#include "../physical/Tha60210061DrpReg.h"
#include "../physical/Tha60210061TopCtrReg.h"
#include "../ocn/Tha60210061ModuleOcnReg.h"
#include "Tha60210061SdhLineDiagReg.h"
#include "Tha60210061ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/
#define mBaseAddress(self) Tha60210011ModuleOcnBaseAddress((ThaModuleOcn)ModuleOcn((AtSdhLine)self))
#define mDiagOffset(self)  DiagOffset((AtSdhLine)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061SdhLine
    {
    tTha60210031SdhLine super;
    }tTha60210061SdhLine;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods            m_AtChannelOverride;
static tAtSdhChannelMethods         m_AtSdhChannelOverride;
static tAtSdhLineMethods            m_AtSdhLineOverride;
static tThaSdhLineMethods           m_ThaSdhLineOverride;
static tTha60210031SdhLineMethods   m_Tha60210031SdhLineOverride;

/* Super implementation */
static const tAtChannelMethods      *m_AtChannelMethods = NULL;
static const tAtSdhChannelMethods   *m_AtSdhChannelMethods = NULL;
static const tAtSdhLineMethods      *m_AtSdhLineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleOcn ModuleOcn(AtSdhLine self)
    {
    return (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cThaModuleOcn);
    }

static ThaModulePoh PohModule(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return (ThaModulePoh)AtDeviceModuleGet(device, cThaModulePoh);
    }

static AtModuleSdh ModuleSdh(AtChannel self)
    {
    return (AtModuleSdh)AtChannelModuleGet(self);
    }

static uint32 ChannelOffset(Tha60210031SdhLine self)
    {
    ThaModuleOcn ocnModule = ModuleOcn((AtSdhLine)self);
    return 256 * AtChannelIdGet((AtChannel)self) + Tha60210011ModuleOcnBaseAddress(ocnModule);
    }

static eAtSdhLineRate LowRate(ThaSdhLine self)
    {
    AtUnused(self);
    return cAtSdhLineRateStm4;
    }

static eAtSdhLineRate HighRate(ThaSdhLine self)
    {
    AtUnused(self);
    return cAtSdhLineRateStm16;
    }

static uint32 RateMask(ThaSdhLine self)
    {
    return ((cBit1_0) << (2 * AtChannelIdGet((AtChannel)self)));
    }

static uint32 RateShift(ThaSdhLine self)
    {
    return (2 * AtChannelIdGet((AtChannel)self));
    }

static uint32 RateSw2Hw(eAtSdhLineRate rate)
    {
    if (rate == cAtSdhLineRateStm1) return 2;
    if (rate == cAtSdhLineRateStm4) return 1;

    return 0;
    }

static uint32 RateHw2Sw(uint32 rate)
    {
    if (rate == 2) return cAtSdhLineRateStm1;
    if (rate == 1) return cAtSdhLineRateStm4;
    if (rate == 0) return cAtSdhLineRateStm16;

    return cAtSdhLineRateInvalid;
    }

static eBool RateIsSupported(AtSdhLine self, eAtSdhLineRate rate)
    {
    AtUnused(self);

    switch ((uint32)rate)
        {
        case cAtSdhLineRateStm1:  return cAtTrue;
        case cAtSdhLineRateStm4:  return cAtTrue;
        case cAtSdhLineRateStm16: return cAtTrue;
        default: return cAtFalse;
        }
    }

static eBool HasRateHwConfiguration(ThaSdhLine self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool IsValidTxRxRate(uint8 tx, uint8 rx)
    {
    if (tx == rx)
        return cAtTrue;
        
    return cAtFalse;
    }

static uint8 NumNormalLines(ThaSdhLine self)
    {
    AtModuleSdh moduleSdh = (AtModuleSdh)AtChannelModuleGet((AtChannel)self);
    return (uint8)(AtModuleSdhMaxLinesGet(moduleSdh) - AtModuleSdhNumEc1LinesGet(moduleSdh));
    }

static void OtherLinesInvalidRateSet(ThaSdhLine self)
    {
    uint32 regVal, regAddr;
    uint8 lineIdx, numLine = NumNormalLines(self);
    uint32 currentLineId = AtChannelIdGet((AtChannel)self);
	const uint8 cInvalidRate = 0x3;
	
    for (lineIdx = 0; lineIdx < numLine; lineIdx++)
        {
        uint32 rateMask = (cBit1_0) << (2 * lineIdx);
        uint32 rateShift = (uint32)(2 * lineIdx);

        if (currentLineId == lineIdx)
            continue;

        regAddr = cAf6Reg_glbtfm_reg + mBaseAddress(self);
        regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
        mRegFieldSet(regVal, rate, cInvalidRate);
        mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

        regAddr = cAf6Reg_glbrfm_reg + mBaseAddress(self);
        regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
        mRegFieldSet(regVal, rate, cInvalidRate);
        mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);
        }
    }

static eAtRet HwRateSet(ThaSdhLine self, eAtSdhLineRate rate)
    {
    uint32 regVal, regAddr;
    uint32 rateMask = RateMask(self);
    uint32 rateShift = RateShift(self);

    regAddr = cAf6Reg_glbtfm_reg + mBaseAddress(self);
    regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    mRegFieldSet(regVal, rate, RateSw2Hw(rate));
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    regAddr = cAf6Reg_glbrfm_reg + mBaseAddress(self);
    regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    mRegFieldSet(regVal, rate, RateSw2Hw(rate));
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    /* If one line run High Rate, Set Invalid Rate for Other Line */
    if (rate == ThaSdhLineHighRate(self))
        OtherLinesInvalidRateSet(self);

    return cAtOk;
    }

static eAtSdhLineRate HwRateGet(ThaSdhLine self)
    {
    uint32 rateMask = RateMask(self);
    uint32 rateShift = RateShift(self);

    uint32 txRegVal = mChannelHwRead(self, cAf6Reg_glbtfm_reg + mBaseAddress(self), cThaModuleOcn);
    uint32 rxRegVal = mChannelHwRead(self, cAf6Reg_glbrfm_reg + mBaseAddress(self), cThaModuleOcn);
    uint8 txRate = (uint8)mRegField(txRegVal, rate);
    uint8 rxRate = (uint8)mRegField(rxRegVal, rate);
    if (!IsValidTxRxRate(txRate, rxRate))
        return cAtSdhLineRateInvalid;

    return RateHw2Sw(rxRate);
    }

static eAtSdhLineRate RateGet(AtSdhLine self)
    {
    return mMethodsGet((ThaSdhLine)self)->HwRateGet((ThaSdhLine)self);
    }

static eAtRet InterruptEnableHelper(AtChannel self, eBool enable)
    {
    uint32 regAddr, regVal;
    uint8 lineId = (uint8)AtChannelIdGet(self);
    uint32 interruptMask = cBit0 << lineId;
    uint32 interruptShift = lineId;

    regAddr = cAf6Reg_tohintperlineenctl + Tha60210011ModuleOcnBaseAddress(ModuleOcn((AtSdhLine)self));
    regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    mRegFieldSet(regVal, interrupt, enable);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eAtRet InterruptEnable(AtChannel self)
    {
    return InterruptEnableHelper(self, cAtTrue);
    }

static eAtRet InterruptDisable(AtChannel self)
    {
    return InterruptEnableHelper(self, cAtFalse);
    }

static eBool InterruptIsEnabled(AtChannel self)
    {
    uint32 regAddr, regVal;
    uint8 lineId = (uint8)AtChannelIdGet(self);
    uint32 interruptMask = cBit0 << lineId;
    uint32 interruptShift = lineId;

    regAddr = cAf6Reg_tohintperlineenctl + Tha60210011ModuleOcnBaseAddress(ModuleOcn((AtSdhLine)self));
    regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);

    return mRegField(regVal, interrupt) ? cAtTrue : cAtFalse;
    }

static uint32 ScrambleMask(AtSdhLine self)
    {
    return (cBit8 << AtChannelIdGet((AtChannel)self));
    }

static uint32 ScrambleShift(AtSdhLine self)
    {
    return 8 + AtChannelIdGet((AtChannel)self);
    }

static eAtModuleSdhRet ScrambleEnable(AtSdhLine self, eBool enable)
    {
    uint32 regVal, regAddr;

    regAddr = cAf6Reg_glbtfm_reg + mBaseAddress(self);
    regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    mFieldIns(&regVal, ScrambleMask(self), ScrambleShift(self), mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    regAddr = cAf6Reg_glbrfm_reg + mBaseAddress(self);
    regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    mFieldIns(&regVal, ScrambleMask(self), ScrambleShift(self), mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eBool ScrambleIsEnabled(AtSdhLine self)
    {
    uint32 regVal = mChannelHwRead(self, cAf6Reg_glbtfm_reg + mBaseAddress(self), cThaModuleOcn);
    uint32 scrambleMask = ScrambleMask(self);
    uint32 scrambleShift = ScrambleShift(self);
    uint32 txScrambleVal = mRegField(regVal, scramble);

    regVal = mChannelHwRead(self, cAf6Reg_glbrfm_reg + mBaseAddress(self), cThaModuleOcn);
    if (txScrambleVal != mRegField(regVal, scramble))
        return cAtFalse;

    return txScrambleVal ? cAtTrue : cAtFalse;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = cAtOk;
    if (AtDeviceDiagnosticModeIsEnabled(AtChannelDeviceGet(self)))
        return cAtOk;

    ret |= m_AtChannelMethods->Init(self);
    ret |= ThaModulePohLinePohMonEnable(PohModule(self), (AtSdhLine)self, cAtTrue);
    return ret;
    }

static uint32 ForcedTxOofMask(AtChannel self)
    {
    return (cBit16 << AtChannelIdGet(self));
    }

static uint32 ForcedTxOofShift(AtChannel self)
    {
    return 16 + AtChannelIdGet(self);
    }

static eAtRet TxOofForce(AtChannel self, eBool enable)
    {
    uint32 regAddr = cAf6Reg_glbtfm_reg + mBaseAddress(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    mFieldIns(&regVal, ForcedTxOofMask(self), ForcedTxOofShift(self), mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static uint32 TxOofForcedGet(AtChannel self)
    {
    uint32 regAddr = cAf6Reg_glbtfm_reg + mBaseAddress(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    uint32 alarmMask = 0;

    if (regVal & ForcedTxOofMask(self))
        alarmMask |= cAtSdhLineAlarmOof;

    return alarmMask;
    }

static uint32 ForcedTxB1Mask(AtChannel self)
    {
    return (cBit12 << AtChannelIdGet(self));
    }

static uint32 ForcedTxB1Shift(AtChannel self)
    {
    return 12 + AtChannelIdGet(self);
    }

static eBool TxB1IsForced(AtChannel self)
    {
    uint32 regAddr = cAf6Reg_glbtfm_reg + mBaseAddress(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);
    return (regVal & ForcedTxB1Mask(self)) ? cAtTrue : cAtFalse;
    }

static eAtRet TxB1Force(AtChannel self, eBool enable)
    {
    uint32 regAddr = cAf6Reg_glbtfm_reg + mBaseAddress(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    mFieldIns(&regVal, ForcedTxB1Mask(self), ForcedTxB1Shift(self), mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eAtRet B2Force(AtChannel self, eBool force)
    {
    uint32 regAddr = cAf6Reg_tfmregctl + mMethodsGet((Tha60210031SdhLine)self)->ChannelOffset((Tha60210031SdhLine)self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_tfmregctl_OCNTxAutoB2Dis_, force ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eBool B2IsForced(AtChannel self)
    {
    uint32 regAddr = cAf6Reg_tfmregctl + mMethodsGet((Tha60210031SdhLine)self)->ChannelOffset((Tha60210031SdhLine)self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);
    return (regVal & cAf6_tfmregctl_OCNTxAutoB2Dis_Mask) ? cAtTrue : cAtFalse;
    }

static eAtRet HelperTxErrorForce(AtChannel self, uint32 errorType, eBool enable)
    {
    if (errorType == cAtSdhLineErrorB1)
        return TxB1Force(self, enable);

    if (errorType == cAtSdhLineErrorB2)
        return B2Force(self, enable);

    return cAtErrorModeNotSupport;
    }

static eAtRet TxErrorForce(AtChannel self, uint32 errorType)
    {
    return HelperTxErrorForce(self, errorType, cAtTrue);
    }

static eAtRet TxErrorUnForce(AtChannel self, uint32 errorType)
    {
    return HelperTxErrorForce(self, errorType, cAtFalse);
    }

static uint32 TxForcedErrorGet(AtChannel self)
    {
    uint32 errorMask = 0;

    if (TxB1IsForced(self))
        errorMask |= cAtSdhLineErrorB1;

    if (B2IsForced(self))
        errorMask |= cAtSdhLineErrorB2;

    return errorMask;
    }

static eAtRet Enable(AtChannel self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtOk;
    }

static eBool IsEnabled(AtChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 TxForcibleAlarmsGet(AtChannel self)
    {
    AtUnused(self);
    return cAtSdhLineAlarmAis | cAtSdhLineAlarmRdi | cAtSdhLineAlarmOof;
    }

static eAtRet TxAlarmForce(AtChannel self, uint32 alarmType)
    {
    eAtRet ret = m_AtChannelMethods->TxAlarmForce(self, alarmType);

    if (ret != cAtOk)
        return ret;

    if (alarmType & cAtSdhLineAlarmOof)
        TxOofForce(self, cAtTrue);

    return cAtOk;
    }

static eAtRet TxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    m_AtChannelMethods->TxAlarmUnForce(self, alarmType);

    if (alarmType & cAtSdhLineAlarmOof)
        TxOofForce(self, cAtFalse);

    return cAtOk;
    }

static uint32 TxForcedAlarmGet(AtChannel self)
    {
    uint32 forcedAlarms = 0;
    forcedAlarms |= m_AtChannelMethods->TxForcedAlarmGet(self);
    forcedAlarms |= TxOofForcedGet(self);

    return forcedAlarms;
    }

static uint32 DiagBaseAddress(AtSdhLine self)
    {
    AtUnused(self);
    return 0xF40000;
    }

static uint32 DiagOffset(AtSdhLine self)
    {
    return DiagBaseAddress(self) + (16 * AtChannelIdGet((AtChannel)self));
    }

static uint32 DiagDefectGet(AtChannel self)
    {
    uint32 defectMask = 0;
    uint32 regAddr = cAf6Reg_rxfrmsta_Base + mDiagOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);

    if (regVal & cAf6_rxfrmsta_OofCurStatus_Mask)
        defectMask |= cAtSdhLineAlarmOof;

    if (regVal & cAf6_rxfrmsta_LofCurStatus_Mask)
        defectMask |= cAtSdhLineAlarmLof;

    if (regVal & cAf6_rxfrmsta_LosCurStatus_Mask)
        defectMask |= cAtSdhLineAlarmLos;

    return defectMask;
    }

static uint32 DefectGet(AtChannel self)
    {
    if (AtDeviceDiagnosticModeIsEnabled(AtChannelDeviceGet(self)))
        return DiagDefectGet(self);

    return m_AtChannelMethods->DefectGet(self);
    }

static uint32 DiagDefectHistoryReadToClear(AtChannel self, eBool r2c)
    {
    uint32 defectMask = 0;
    uint32 regAddr = cAf6Reg_rxfrmstk_Base + mDiagOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);

    if (regVal & cAf6_rxfrmstk_OofStateChgStk_Mask)
        defectMask |= cAtSdhLineAlarmOof;

    if (regVal & cAf6_rxfrmstk_LofStateChgStk_Mask)
        defectMask |= cAtSdhLineAlarmLof;

    if (regVal & cAf6_rxfrmstk_LosStateChgStk_Mask)
        defectMask |= cAtSdhLineAlarmLos;

    if (r2c)
        mChannelHwWrite(self, regAddr, 0xFFFFFFFF, cThaModuleOcn);

    return defectMask;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    if (AtDeviceDiagnosticModeIsEnabled(AtChannelDeviceGet(self)))
        return DiagDefectHistoryReadToClear(self, cAtFalse);

    return m_AtChannelMethods->DefectHistoryGet(self);
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    if (AtDeviceDiagnosticModeIsEnabled(AtChannelDeviceGet(self)))
        return DiagDefectHistoryReadToClear(self, cAtTrue);

    return m_AtChannelMethods->DefectHistoryClear(self);
    }

static uint32 DiagB1CounterReadToClear(AtChannel self, eBool r2c)
    {
    uint32 regAddr = r2c ? cAf6Reg_rxfrmb1cntr2c_Base : cAf6Reg_rxfrmb1cntro_Base;

    regAddr += mDiagOffset(self);
    return mChannelHwRead(self, regAddr, cThaModuleOcn);
    }
    
static AtSerdesController SerdesController(AtSdhLine self)
    {
    AtChannel channel = (AtChannel) self;
    AtSerdesManager serdesManager = AtDeviceSerdesManagerGet(AtChannelDeviceGet(channel));
    AtSerdesController controler = AtSerdesManagerSerdesControllerGet(serdesManager, AtChannelIdGet(channel));
    if (AtSerdesControllerModeIsOcn(AtSerdesControllerModeGet(controler)))
        {
        AtSerdesControllerPhysicalPortSet(controler, (AtChannel)self);
        return controler;
        }

    return NULL;
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    if (AtDeviceDiagnosticModeIsEnabled(AtChannelDeviceGet(self)))
        {
        if (counterType == cAtSdhLineCounterTypeB1)
            return DiagB1CounterReadToClear(self, cAtFalse);

        return 0;
        }

    return m_AtChannelMethods->CounterGet(self, counterType);
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    if (AtDeviceDiagnosticModeIsEnabled(AtChannelDeviceGet(self)))
        {
        if (counterType == cAtSdhLineCounterTypeB1)
            return DiagB1CounterReadToClear(self, cAtTrue);

        return 0;
        }

    return m_AtChannelMethods->CounterClear(self, counterType);
    }

static eBool CounterIsSupported(AtChannel self, uint16 counterType)
    {
    if (AtDeviceDiagnosticModeIsEnabled(AtChannelDeviceGet(self)))
        return (counterType == cAtSdhLineCounterTypeB1) ? cAtTrue : cAtFalse;

    return m_AtChannelMethods->CounterIsSupported(self, counterType);
    }

static uint32 B1CounterRegAddr(Tha60210031SdhLine self, eBool r2c)
    {
    AtUnused(self);
    return r2c ? cAf6Reg_tohb1errr2ccnt : cAf6Reg_tohb1errrocnt;
    }

static uint32 B2CounterRegAddr(Tha60210031SdhLine self, eBool r2c)
    {
    AtUnused(self);
    return r2c ? cAf6Reg_tohb2errr2ccnt : cAf6Reg_tohb2errrocnt;
    }

static uint32 ReiCounterRegAddr(Tha60210031SdhLine self, eBool r2c)
    {
    AtUnused(self);
    return r2c ? cAf6Reg_tohreierrr2ccnt : cAf6Reg_tohreierrrocnt;
    }

static uint32 B1BlockCounterRegAddr(Tha60210031SdhLine self, eBool r2c)
    {
    AtUnused(self);
    return r2c ? cAf6Reg_tohb1blkerrr2ccnt : cAf6Reg_tohb1blkerrrocnt;
    }

static uint32 B2BlockCounterRegAddr(Tha60210031SdhLine self, eBool r2c)
    {
    AtUnused(self);
    return r2c ? cAf6Reg_tohb2blkerrr2ccnt : cAf6Reg_tohb2blkerrrocnt;
    }

static uint32 ReiBlockCounterRegAddr(Tha60210031SdhLine self, eBool r2c)
    {
    AtUnused(self);
    return r2c ? cAf6Reg_tohreiblkerrr2ccnt : cAf6Reg_tohreiblkerrrocnt;
    }

static uint32 TohMonitoringChannelControlRegAddr(Tha60210031SdhLine self)
    {
    AtUnused(self);
    return cAf6Reg_tohramctl;
    }

static uint32 RxLineAlarmInterruptStatusRegAddr(Tha60210031SdhLine self)
    {
    AtUnused(self);
    return cAf6Reg_tohintsta;
    }

static uint32 RxLineAlarmInterruptEnableControlRegAddr(Tha60210031SdhLine self)
    {
    AtUnused(self);
    return cAf6Reg_tohintperalrenbctl;
    }

static uint32 RxLineAlarmCurrentStatusRegAddr(Tha60210031SdhLine self)
    {
    AtUnused(self);
    return cAf6Reg_tohcursta;
    }

static uint32 TxFramerPerChannelControlRegAddr(Tha60210031SdhLine self)
    {
    AtUnused(self);
    return cAf6Reg_tfmregctl;
    }

static uint32 TohGlobalS1StableMonitoringThresholdControlRegAddr(Tha60210031SdhLine self)
    {
    AtUnused(self);
    return cAf6Reg_tohglbs1stbthr_reg;
    }

static uint32 TohS1MonitoringStatusRegAddr(Tha60210031SdhLine self)
    {
    AtUnused(self);
    return cAf6Reg_tohs1monsta;
    }

static uint32 TohK1MonitoringStatusRegAddr(Tha60210031SdhLine self)
    {
    AtUnused(self);
    return cAf6Reg_tohk1monsta;
    }

static uint32 TohGlobalK1StableMonitoringThresholdControlRegAddr(Tha60210031SdhLine self)
    {
    AtUnused(self);
    return cAf6Reg_tohglbk1stbthr_reg;
    }

static uint32 TohK2MonitoringStatusRegAddr(Tha60210031SdhLine self)
    {
    AtUnused(self);
    return cAf6Reg_tohk2monsta;
    }

static uint32 TohGlobalK2StableMonitoringThresholdControlRegAddr(Tha60210031SdhLine self)
    {
    AtUnused(self);
    return cAf6Reg_tohglbk2stbthr_reg;
    }

static eBool LoopbackIsSupported(AtChannel self, uint8 loopbackMode)
    {
    if (!Tha60210061ModuleSdhLineLoopbackIsSupported(ModuleSdh(self)))
        return (loopbackMode == cAtLoopbackModeRelease) ?  cAtTrue : cAtFalse;

    if ((loopbackMode == cAtLoopbackModeLocal) || (loopbackMode == cAtLoopbackModeRelease))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet LoopbackSet(AtChannel self, uint8 loopbackMode)
    {
    uint32 regAddr, regVal, lineId;

    regAddr = cAf6Reg_glbLoopback + mBaseAddress(self);
    regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    lineId = AtChannelIdGet(self);

    mFieldIns(&regVal,
              cAf6Reg_LineLocalLoopbackMask(lineId),
              cAf6Reg_LineLocalLoopbackShift(lineId),
              (loopbackMode == cAtLoopbackModeLocal) ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static uint8 LoopbackGet(AtChannel self)
    {
    uint32 regAddr, regVal, lineId;
    uint8 loopbackMode;

    if (!Tha60210061ModuleSdhLineLoopbackIsSupported(ModuleSdh(self)))
        return cAtLoopbackModeRelease;

    regAddr = cAf6Reg_glbLoopback + mBaseAddress(self);
    regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    lineId = AtChannelIdGet(self);

    mFieldGet(regVal,
              cAf6Reg_LineLocalLoopbackMask(lineId),
              cAf6Reg_LineLocalLoopbackShift(lineId),
              uint8, &loopbackMode);

    return (loopbackMode == 1) ? cAtLoopbackModeLocal : cAtLoopbackModeRelease;
    }

static eAtRet RxAlarmForce(AtChannel self, uint32 alarmType)
    {
    uint32 regAddr, regVal;
    Tha60210031SdhLine sdhLine = (Tha60210031SdhLine)self;

    if ((alarmType & AtChannelRxForcableAlarmsGet(self)) == 0)
        return cAtErrorModeNotSupport;

    regAddr = mMethodsGet(sdhLine)->TohMonitoringChannelControlRegAddr(sdhLine) + mMethodsGet((ThaSdhLine)self)->DefaultOffset((ThaSdhLine)self);
    regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_tohramctl_RxAisLFrc_, 1);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eAtRet RxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    uint32 regAddr, regVal;
    Tha60210031SdhLine sdhLine = (Tha60210031SdhLine)self;

    if ((alarmType & AtChannelRxForcableAlarmsGet(self)) == 0)
        return cAtErrorModeNotSupport;

    regAddr = mMethodsGet(sdhLine)->TohMonitoringChannelControlRegAddr(sdhLine) + mMethodsGet((ThaSdhLine)self)->DefaultOffset((ThaSdhLine)self);
    regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_tohramctl_RxAisLFrc_, 0);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static uint32 RxForcedAlarmGet(AtChannel self)
    {
    uint32 regAddr, regVal;
    Tha60210031SdhLine sdhLine = (Tha60210031SdhLine)self;

    if (!Tha60210061ModuleSdhLineLoopbackIsSupported(ModuleSdh(self)))
        return 0;

    regAddr = mMethodsGet(sdhLine)->TohMonitoringChannelControlRegAddr(sdhLine) + mMethodsGet((ThaSdhLine)self)->DefaultOffset((ThaSdhLine)self);
    regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);
    return (mRegField(regVal, cAf6_tohramctl_RxAisLFrc_)) ? cAtSdhLineAlarmAis : 0;
    }

static uint32 RxForcibleAlarmsGet(AtChannel self)
    {
    if (!Tha60210061ModuleSdhLineLoopbackIsSupported(ModuleSdh(self)))
        return 0;

    return cAtSdhLineAlarmAis;
    }

static eAtModuleSdhRet TimAffectingEnable(AtSdhChannel self, eBool enable)
    {
    uint32 lineIdx = AtChannelHwIdGet((AtChannel)self) << 2;
    uint32 regAddr = Tha60210011ModuleOcnBaseAddress(ModuleOcn((AtSdhLine)self)) + cAf6Reg_glbrfmaisrdifwd_reg;
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    uint32 cTim2AisPMask  = cAf6_glbrfmaisrdifwd_reg_RxFrmTimAisPEn1_Mask << lineIdx;
    uint32 cTim2AisPShift = cAf6_glbrfmaisrdifwd_reg_RxFrmTimAisPEn1_Shift + lineIdx;
    uint32 cTim2RdiLMask  = cAf6_glbrfmaisrdifwd_reg_RxFrmTimRdiLEn1_Mask << lineIdx;
    uint32 cTim2RdiLShift = cAf6_glbrfmaisrdifwd_reg_RxFrmTimRdiLEn1_Shift + lineIdx;

    mRegFieldSet(regVal, cTim2AisP, enable ? 1 : 0);
    mRegFieldSet(regVal, cTim2RdiL, enable ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eAtModuleSdhRet AlarmAffectingEnable(AtSdhChannel self, uint32 alarmType, eBool enable)
    {
    eAtModuleSdhRet ret = m_AtSdhChannelMethods->AlarmAffectingEnable(self, alarmType, enable);
    if (ret != cAtOk)
        return ret;

    if ((alarmType & cAtSdhLineAlarmTim) == 0)
        return ret;

    return TimAffectingEnable(self, enable);
    }

static eBool Ec1TxAisForceIsSupported(Tha60210031SdhLine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void OverrideTha60210031SdhLine(AtSdhLine self)
    {
    Tha60210031SdhLine channel = (Tha60210031SdhLine)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210031SdhLineOverride, mMethodsGet(channel), sizeof(m_Tha60210031SdhLineOverride));

        mMethodOverride(m_Tha60210031SdhLineOverride, ChannelOffset);
        mMethodOverride(m_Tha60210031SdhLineOverride, B1CounterRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, B2CounterRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, ReiCounterRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, B1BlockCounterRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, B2BlockCounterRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, ReiBlockCounterRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, TohMonitoringChannelControlRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, RxLineAlarmInterruptStatusRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, RxLineAlarmInterruptEnableControlRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, RxLineAlarmCurrentStatusRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, TxFramerPerChannelControlRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, TohGlobalS1StableMonitoringThresholdControlRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, TohS1MonitoringStatusRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, TohK1MonitoringStatusRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, TohGlobalK1StableMonitoringThresholdControlRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, TohK2MonitoringStatusRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, TohGlobalK2StableMonitoringThresholdControlRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, Ec1TxAisForceIsSupported);
        }

    mMethodsSet(channel, &m_Tha60210031SdhLineOverride);
    }
    
static void OverrideThaSdhLine(AtSdhLine self)
    {
    ThaSdhLine channel = (ThaSdhLine)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhLineOverride, mMethodsGet(channel), sizeof(m_ThaSdhLineOverride));

        mMethodOverride(m_ThaSdhLineOverride, LowRate);
        mMethodOverride(m_ThaSdhLineOverride, HighRate);
        mMethodOverride(m_ThaSdhLineOverride, HasRateHwConfiguration);
        mMethodOverride(m_ThaSdhLineOverride, HwRateSet);
        mMethodOverride(m_ThaSdhLineOverride, HwRateGet);
        }

    mMethodsSet(channel, &m_ThaSdhLineOverride);
    }

static void OverrideAtSdhLine(AtSdhLine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhLineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhLineOverride, m_AtSdhLineMethods, sizeof(m_AtSdhLineOverride));

        mMethodOverride(m_AtSdhLineOverride, RateGet);
        mMethodOverride(m_AtSdhLineOverride, RateIsSupported);
        mMethodOverride(m_AtSdhLineOverride, ScrambleEnable);
        mMethodOverride(m_AtSdhLineOverride, ScrambleIsEnabled);
        mMethodOverride(m_AtSdhLineOverride, SerdesController);
        }

    mMethodsSet(self, &m_AtSdhLineOverride);
    }

static void OverrideAtSdhChannel(AtSdhLine self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(m_AtSdhChannelOverride));

        mMethodOverride(m_AtSdhChannelOverride, AlarmAffectingEnable);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void OverrideAtChannel(AtSdhLine self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, TxAlarmForce);
        mMethodOverride(m_AtChannelOverride, TxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcibleAlarmsGet);
        mMethodOverride(m_AtChannelOverride, TxForcedAlarmGet);
        mMethodOverride(m_AtChannelOverride, TxErrorForce);
        mMethodOverride(m_AtChannelOverride, TxErrorUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcedErrorGet);
        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, IsEnabled);
        mMethodOverride(m_AtChannelOverride, InterruptEnable);
        mMethodOverride(m_AtChannelOverride, InterruptDisable);
        mMethodOverride(m_AtChannelOverride, InterruptIsEnabled);
        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, CounterIsSupported);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, LoopbackIsSupported);
        mMethodOverride(m_AtChannelOverride, LoopbackSet);
        mMethodOverride(m_AtChannelOverride, LoopbackGet);
        mMethodOverride(m_AtChannelOverride, RxAlarmForce);
        mMethodOverride(m_AtChannelOverride, RxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, RxForcibleAlarmsGet);
        mMethodOverride(m_AtChannelOverride, RxForcedAlarmGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtSdhLine self)
    {
    OverrideAtChannel(self);
    OverrideAtSdhChannel(self);
    OverrideAtSdhLine(self);	
    OverrideThaSdhLine(self);
	OverrideTha60210031SdhLine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061SdhLine);
    }

static AtSdhLine ObjectInit(AtSdhLine self, uint32 channelId, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031SdhLineObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhLine Tha60210061SdhLineNew(uint32 channelId, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhLine newLine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newLine == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newLine, channelId, module);
    }

eAtRet Tha60210061SdhLineDccBusEnable(AtSdhLine self, eBool enable)
    {
    uint32 address = Tha60210011ModuleOcnBaseAddress(ModuleOcn(self)) + cAf6Reg_glbtfm_reg;
    uint32 regVal = mChannelHwRead(self, address, cThaModuleOcn);
    uint32 lineId = AtChannelIdGet((AtChannel)self);

    mFieldIns(&regVal, cAf6_glbtfm_reg_TxFrmOhBusDccEnbMask(lineId), cAf6_glbtfm_reg_TxFrmOhBusDccEnbShift(lineId), mBoolToBin(enable));
    mChannelHwWrite(self, address, regVal, cThaModuleOcn);

    return cAtOk;
    }

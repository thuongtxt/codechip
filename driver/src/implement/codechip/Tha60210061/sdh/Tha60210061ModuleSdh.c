/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60210061ModuleSdh.c
 *
 * Created Date: Jun 17, 2016
 *
 * Description : 60210061 module SDH
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/sdh/AtSdhLineInternal.h"
#include "../../Tha60210012/sdh/Tha60210012ModuleSdhInternal.h"
#include "../../Tha60210011/poh/Tha60210011ModulePoh.h"
#include "../../Tha60210011/ocn/Tha60210011ModuleOcn.h"
#include "../../Tha60210011/xc/Tha60210011ModuleXc.h"
#include "../man/Tha60210061InterruptController.h"
#include "../poh/Tha60210061ModulePohReg.h"
#include "../ocn/Tha60210061ModuleOcn.h"
#include "../ocn/Tha60210061ModuleOcnEc1Reg.h"
#include "../encap/Tha60210061ModuleEncap.h"
#include "Tha60210061ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define cInterruptBitMask(sliceId)    (cBit0 << (sliceId))
#define cStartSts24InPdhForEc1 24

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061ModuleSdh
    {
    tTha60210012ModuleSdh super;
    }tTha60210061ModuleSdh;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleSdhMethods          m_AtModuleSdhOverride;
static tThaModuleSdhMethods         m_ThaModuleSdhOverride;
static tTha60210011ModuleSdhMethods m_Tha60210011ModuleSdhOverride;

/* Save super implementation */
static const tAtModuleSdhMethods          *m_AtModuleSdhMethods;
static const tThaModuleSdhMethods         *m_ThaModuleSdhMethods;
static const tTha60210011ModuleSdhMethods *m_Tha60210011ModuleSdhMethods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 NumTfi5Lines(AtModuleSdh self)
    {
    AtUnused(self);
    return 0;
    }

static uint8 MaxLinesGet(AtModuleSdh self)
    {
    AtUnused(self);
    /*
     * There are 8 SDH framers:
     * 4 STM lines can be OC-48/12/3
     * 4 EC1 lines
     */
    return 8;
    }

static uint32 StartEc1LineIdGet(AtModuleSdh self)
    {
    AtUnused(self);
    return 4;
    }

static AtModulePdh ModulePdh(AtModuleSdh self)
    {
    return (AtModulePdh)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModulePdh);
    }

static uint32 NumEc1LinesGet(AtModuleSdh self)
    {
    return AtModulePdhNumberOfDe3SerialLinesGet(ModulePdh(self));
    }

static uint8 NumLinesPerPart(ThaModuleSdh self)
    {
    AtUnused(self);
    return AtModuleSdhMaxLinesGet((AtModuleSdh)self);
    }

static AtSdhChannel Tu3VcObjectCreate(Tha60210011ModuleSdh self, uint8 channelType, uint8 channelId)
    {
    return (AtSdhChannel)Tha60210061SdhTu3VcNew(channelId, channelType, (AtModuleSdh)self);
    }

static AtSdhChannel AuVcObjectCreate(Tha60210011ModuleSdh self, uint8 lineId, uint8 channelType, uint8 channelId)
    {
    if (lineId < AtModuleSdhStartEc1LineIdGet((AtModuleSdh)self))
        return (AtSdhChannel)Tha60210061SdhVcNew(channelId, channelType, (AtModuleSdh)self);

    return (AtSdhChannel)Tha60210061SdhVcEc1New(channelId, channelType, (AtModuleSdh)self);
    }

static AtSdhChannel Vc1xObjectCreate(Tha60210011ModuleSdh self, uint8 channelType, uint8 channelId)
    {
    return (AtSdhChannel)Tha60210061SdhVc1xNew(channelId, channelType, (AtModuleSdh)self);
    }

static AtSdhLine LineCreate(AtModuleSdh self, uint32 channelId)
    {
    if (channelId < AtModuleSdhStartEc1LineIdGet(self))
        return Tha60210061SdhLineNew(channelId, self);

    return Tha60210061SdhLineEc1New(channelId, self);
    }

static AtSdhChannel ChannelCreate(AtModuleSdh self, uint8 lineId, AtSdhChannel parent, uint8 channelType, uint8 channelId)
    {
    switch (channelType)
        {
        /* Create AUG */
        case cAtSdhChannelTypeAug16:
        case cAtSdhChannelTypeAug4:
        case cAtSdhChannelTypeAug1:
            return (AtSdhChannel)Tha60210061SdhAugNew(channelId, channelType, self);

        /* Create VC */
        case cAtSdhChannelTypeLine:
            return (AtSdhChannel)LineCreate(self, channelId);

        default:
            return m_AtModuleSdhMethods->ChannelCreate(self, lineId, parent, channelType, channelId);
        }
    }

static eAtSdhLineRate LineDefaultRate(ThaModuleSdh self, AtSdhLine line)
    {
    uint32 lineId = AtChannelIdGet((AtChannel)line);
    if (lineId < AtModuleSdhStartEc1LineIdGet((AtModuleSdh)self))
        return cAtSdhLineRateStm4;

    return cAtSdhLineRateStm0;
    }

static AtSerdesController SerdesControllerCreate(ThaModuleSdh self, AtSdhLine line, uint32 serdesId)
    {
    AtUnused(self);
    AtUnused(line);
    AtUnused(serdesId);
    return NULL;
    }

static uint8 MaxNumHoLines(Tha60210011ModuleSdh self)
    {
    AtUnused(self);
    return 1;
    }

static uint8 MaxNumLoLines(Tha60210011ModuleSdh self)
    {
    AtUnused(self);
    return 1;
    }

static uint8 HoLineStartId(Tha60210011ModuleSdh self)
    {
    AtUnused(self);
    return 0;
    }

static uint8 LoLineStartId(Tha60210011ModuleSdh self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet PohMonitorDefaultSet(Tha60210011ModuleSdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaModulePoh pohModule = (ThaModulePoh)AtDeviceModuleGet(device, cThaModulePoh);
    uint32 regAddr = cAf6RegLinePohEnableBase + Tha60210011ModulePohBaseAddress(pohModule);
    uint32 regVal = 0;

    mRegFieldSet(regVal, cAf6HoStmPointerPohEnable, 1);
    mRegFieldSet(regVal, cAf6HoStmLinePohEnable, 1);
    mRegFieldSet(regVal, cAf6LoStmPointerPohEnable, 1);

    mRegFieldSet(regVal, cAf6HoEc1PointerPohEnable, 1);
    mRegFieldSet(regVal, cAf6HoEc1LinePohEnable, 1);
    mRegFieldSet(regVal, cAf6LoEc1PointerPohEnable, 1);

    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool ShouldUpdateSerdesTimingWhenLineRateChange(ThaModuleSdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool HasLineInterrupt(ThaModuleSdh self, uint32 glbIntr, AtIpCore ipCore)
    {
    /* For 4 STM lines. */
    if (m_ThaModuleSdhMethods->HasLineInterrupt(self, glbIntr, ipCore))
        return cAtTrue;

    /* For 4 EC1 lines. */
    return (glbIntr & cAf6_global_interrupt_EC1IntEnable_Mask) ? cAtTrue : cAtFalse;
    }

static ThaModuleOcn ModuleOcn(ThaModuleSdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return (ThaModuleOcn)AtDeviceModuleGet(device, cThaModuleOcn);
    }

static void StmLinesInterruptProcess(ThaModuleSdh self, AtHal hal)
    {
    uint8 lineId;
    uint32 baseAddress = Tha60210011ModuleOcnBaseAddress(ModuleOcn(self));
    uint32 intrLineVal = AtHalRead(hal, (uint32)cAf6Reg_tohintperline + baseAddress);
    uint32 intrLineMask = AtHalRead(hal, (uint32)cAf6Reg_tohintperlineenctl + baseAddress);

    intrLineVal &= intrLineMask;
    for (lineId = 0; lineId < 4; lineId++)
        {
        if (intrLineVal & cInterruptBitMask(lineId))
            {
            AtSdhLine line = AtModuleSdhLineGet((AtModuleSdh)self, lineId);
            AtSdhLineInterruptProcess(line, lineId, hal);
            }
        }
    }

static void Ec1LinesInterruptProcess(ThaModuleSdh self, AtHal hal)
    {
    uint8 lineId;
    uint32 baseAddress = Tha60210061ModuleOcnEc1BaseAddress(ModuleOcn(self));
    uint32 intrLineVal = AtHalRead(hal, cAf6_Ec1Reg_tohintperline_Base + baseAddress);
    uint32 intrLineMask = AtHalRead(hal, cAf6_Ec1Reg_tohintperlineenctl_Base + baseAddress);
    uint32 startEc1LineId = AtModuleSdhStartEc1LineIdGet((AtModuleSdh)self);

    intrLineVal &= intrLineMask;
    for (lineId = 0; lineId < 4; lineId++)
        {
        if (intrLineVal & cInterruptBitMask(lineId))
            {
            AtSdhLine line = AtModuleSdhLineGet((AtModuleSdh)self, (uint8)(lineId + startEc1LineId));
            AtSdhLineInterruptProcess(line, lineId, hal);
            }
        }
    }

static void LineInterruptProcess(ThaModuleSdh self, uint32 glbIntr, AtHal hal)
    {
    if (glbIntr & cAf6_global_interrupt_TFI5IntEnable_Mask)
        StmLinesInterruptProcess(self, hal);

    if (glbIntr & cAf6_global_interrupt_EC1IntEnable_Mask)
        Ec1LinesInterruptProcess(self, hal);
    }

static AtSdhChannel SdhAuFromStm4HwStsGet(AtSdhLine line, uint8 ocnPpStsId)
    {
    AtSdhChannel aug4 = (AtSdhChannel)AtSdhLineAug4Get(line, 0);

    if (AtSdhChannelMapTypeGet(aug4) == cAtSdhAugMapTypeAug4MapVc4_4c)
        return AtSdhChannelSubChannelGet(aug4, 0);
    else
        {
        if (AtSdhChannelMapTypeGet(aug4) == cAtSdhAugMapTypeAug4Map4xAug1s)
            {
            uint8 aug1Id = (uint8)((ocnPpStsId % 16) / 4);
            AtSdhChannel aug1 = (AtSdhChannel)AtSdhLineAug1Get(line, aug1Id);

            if (AtSdhChannelMapTypeGet(aug1) == cAtSdhAugMapTypeAug1MapVc4)
                return AtSdhChannelSubChannelGet(aug1, 0);

            if (AtSdhChannelMapTypeGet(aug1) == cAtSdhAugMapTypeAug1Map3xVc3s)
                return AtSdhChannelSubChannelGet(aug1, (uint8)(ocnPpStsId / 16));
            }
        }

    return NULL;
    }

static AtSdhChannel SdhAuFromStm1HwStsGet(AtSdhLine line, uint8 ocnPpStsId)
    {
    AtSdhChannel aug1 = (AtSdhChannel)AtSdhLineAug1Get(line, 0);

    if (AtSdhChannelMapTypeGet(aug1) == cAtSdhAugMapTypeAug1MapVc4)
        return AtSdhChannelSubChannelGet(aug1, 0);

    return AtSdhChannelSubChannelGet(aug1, (uint8)(ocnPpStsId / 16)); /* Aug1 to 3xVc3s */
    }

static AtSdhChannel AuPathFromHwStsOfMutirateStmLineGet(AtSdhLine sdhLine, uint8 ocnPpStsId)
    {
    eAtSdhLineRate lineRate = AtSdhLineRateGet(sdhLine);

    if (lineRate == cAtSdhLineRateStm16)
        return Tha60210011ModuleSdhAuFromHwStsGet(sdhLine, ocnPpStsId);

    if (lineRate == cAtSdhLineRateStm4)
        return SdhAuFromStm4HwStsGet(sdhLine, ocnPpStsId);

    if (lineRate == cAtSdhLineRateStm1)
        return SdhAuFromStm1HwStsGet(sdhLine, ocnPpStsId);

    return NULL;
    }

static uint8 RateHw2Sw(uint8 rate)
    {
    if (rate == 2) return cAtSdhLineRateStm1;
    if (rate == 1) return cAtSdhLineRateStm4;
    if (rate == 0) return cAtSdhLineRateStm16;

    return cAtSdhLineRateInvalid;
    }

static AtSdhLine Stm16LineGet(AtModuleSdh self)
    {
    uint8 line_i, txRate;
    ThaModuleOcn ocnModule = (ThaModuleOcn)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModuleOcn);
    uint32 regAddr = cAf6Reg_glbtfm_reg + Tha60210011ModuleOcnBaseAddress(ocnModule);
    uint32 regVal  = mModuleHwRead(ocnModule, regAddr);

    for (line_i = 0; line_i < 4; line_i++)
        {
        uint32 rateMask =  ((cBit1_0) << (2 * line_i));
        uint32 rateShift = (uint32)(2 * line_i);

        txRate = RateHw2Sw((uint8)mRegField(regVal, rate));
        if (txRate == cAtSdhLineRateStm16)
            return AtModuleSdhLineGet(self, line_i);
        }

    return NULL;
    }

static eBool IsSliceOfEc1LinesInSdhModule(uint8 sliceId)
    {
    static const uint8 cStartSlice24ForEc1 = 2;
    return (sliceId < cStartSlice24ForEc1) ? cAtFalse : cAtTrue;
    }

static AtSdhLine Ec1LineFromOcnPpHwIdGet(AtModuleSdh self, uint8 ocnPpStsId)
    {
    return AtModuleSdhLineGet(self, (uint8)(AtModuleSdhStartEc1LineIdGet(self) + ocnPpStsId));
    }

static AtSdhLine StmLineFromOcnPpHwIdGet(AtModuleSdh self, uint8 ocnPpStsId)
    {
    AtSdhLine sdhLine = Stm16LineGet(self);
    if (sdhLine)
        return sdhLine;

    return AtModuleSdhLineGet((AtModuleSdh)self, (uint8)(ocnPpStsId % 4));
    }

static AtSdhPath AuPathFromHwIdGetInSdhModule(ThaModuleSdh self, uint8 sliceId, uint8 stsId)
    {
    AtSdhLine sdhLine;
    uint8 ocnPpSts, ocnPpSlice;

    /* In SDH module, sliceId/stsId has following valid ranges as:
     * slice#0, sts#0-23 of even STSs for STM16/STM4/STM1 interface
     * slice#1, sts#0-23 of odd STSs for STM16/STM4/STM1 interface
     * slice#2, sts#0-1 of even EC1 interface.
     * slice#3, sts#0-1 of odd EC1 interface.
     */

    /* Convert interrupt ID to OCN PP ID */
    ThaModuleSdhHwSts24ToHwSts48Get(self, sliceId, stsId, &ocnPpSlice, &ocnPpSts);

    if (IsSliceOfEc1LinesInSdhModule(sliceId))
        {
        sdhLine = Ec1LineFromOcnPpHwIdGet((AtModuleSdh)self, ocnPpSts);
        if (sdhLine == NULL)
            return NULL;

        return (AtSdhPath)AtSdhChannelSubChannelGet((AtSdhChannel)sdhLine, 0);
        }

    sdhLine = StmLineFromOcnPpHwIdGet((AtModuleSdh)self, ocnPpSts);
    if (sdhLine == NULL)
        return NULL;

    return (AtSdhPath)AuPathFromHwStsOfMutirateStmLineGet(sdhLine, ocnPpSts);
    }

static eBool IsStsOfEc1LinesInPdhModule(uint8 stsId)
    {
    static const uint8 cStartStsForEc1 = 24;
    return (stsId < cStartStsForEc1) ? cAtFalse : cAtTrue;
    }

static AtSdhPath AuPathFromHwIdGetInOtherModules(ThaModuleSdh self, uint8 sliceId, uint8 stsId)
    {
    /* In other modules, sliceId/stsId has following valid ranges as:
     * slice#0, sts#0-23 of even STSs for STM16/STM4/STM1 interface
     *          sts#24-25 for even STSs of EC1 interface
     * slice#1, sts#0-23 of odd STSs for STM16/STM4/STM1 interface
     *          sts#24-25 for even STSs of EC1 interface
     */
    if (IsStsOfEc1LinesInPdhModule(stsId))
        {
        /* Convert ID to SDH module ID format. */
        sliceId = (uint8)(sliceId + 2);
        stsId   = (uint8)((stsId - 24) >> 1);
        }

    return AuPathFromHwIdGetInSdhModule(self, sliceId, stsId);
    }

static AtSdhPath AuPathFromHwIdGet(ThaModuleSdh self, eAtModule phyModule, uint8 sliceId, uint8 stsId)
    {
    if (phyModule == cAtModuleSdh)
        return AuPathFromHwIdGetInSdhModule(self, sliceId, stsId);

    return AuPathFromHwIdGetInOtherModules(self, sliceId, stsId);
    }

static AtSdhPath TuPathFromHwIdGet(ThaModuleSdh self, eAtModule phyModule, uint8 sliceId, uint8 stsId, uint8 vtgId, uint8 vtId)
    {
    AtSdhChannel au = (AtSdhChannel)ThaModuleSdhAuPathFromHwIdGet(self, phyModule, sliceId, stsId);
    uint8 ocnPpSts, ocnPpSlice;

    /* Convert interrupt ID to OCN PP ID */
    ThaModuleSdhHwSts24ToHwSts48Get(self, sliceId, stsId, &ocnPpSlice, &ocnPpSts);

    return (AtSdhPath)Tha60210011SdhTuPathFromAuPathHwStsGet(au, ocnPpSts, vtgId, vtId);
    }

static eBool SdhChannelIsHoPath(Tha60210011ModuleSdh self, AtSdhChannel channel)
    {
    if (Tha60210061ModuleSdhChannelIsInEc1Line((AtModuleSdh)self, channel))
        return cAtFalse;

    return m_Tha60210011ModuleSdhMethods->SdhChannelIsHoPath(self, channel);
    }

static uint32 MaxNumDccHdlcChannels(ThaModuleSdh self)
    {
    AtUnused(self);
    return 8;
    }

static eBool DccLayerIsSupported(AtModuleSdh self, AtSdhLine line, eAtSdhLineDccLayer layer)
    {
    AtUnused(self);
    AtUnused(line);

    switch ((uint32)layer)
        {
        case cAtSdhLineDccLayerSection: return cAtTrue;
        case cAtSdhLineDccLayerLine   : return cAtTrue;
        default: return cAtFalse;
        }
    }

static uint32 StartVersionSupportLineLoopback(void)
    {
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x3, 0x1, 0x1030);
    }

static AtModuleEncap ModuleEncap(ThaModuleSdh self)
    {
    return (AtModuleEncap)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModuleEncap);
    }

static AtHdlcChannel DccChannelObjectCreate(ThaModuleSdh self, uint32 dccId, AtSdhLine line, eAtSdhLineDccLayer layer)
    {
    AtModuleEncap moduleEncap = ModuleEncap(self);
    AtHdlcChannel dcc;

    if (!Tha60210061ModuleEncapDccIsSupported(moduleEncap))
        return NULL;

    dcc = Tha60210061DccNew(dccId, moduleEncap, line, layer);
    if (AtChannelInit((AtChannel)dcc) != cAtOk)
        {
        AtObjectDelete((AtObject)dcc);
        return NULL;
        }

    return dcc;
    }

static eBool SdhVcBelongsToLoLine(Tha60210011ModuleSdh self, AtSdhChannel vc)
    {
    if (Tha60210061ModuleSdhChannelIsInEc1Line((AtModuleSdh)self, vc))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet Ec1PathVirtualFifoCounterGet(AtSdhPath path, tAtSdhPathCounters* allCounters, uint32 (*CounterGet)(AtChannel, uint16))
    {
    ThaModuleOcn moduleOcn;

    if ((path == NULL) || (allCounters == NULL))
        return cAtErrorNullPointer;

    AtOsalMemInit(allCounters, 0, sizeof(tAtSdhPathCounters));
    if (!Tha60210061ModuleSdhChannelIsInEc1Line((AtModuleSdh)AtChannelModuleGet((AtChannel)path), (AtSdhChannel)path))
        return cAtOk;

    moduleOcn = ThaModuleOcnFromChannel((AtChannel)path);
    Tha60210061ModuleOcnVirtualFifoCounterEnable(moduleOcn, cAtTrue);

    allCounters->rxNPJC = CounterGet((AtChannel)path, cAtSdhPathCounterTypeRxNPJC);
    allCounters->txNPJC = CounterGet((AtChannel)path, cAtSdhPathCounterTypeTxNPJC);
    allCounters->rxPPJC = CounterGet((AtChannel)path, cAtSdhPathCounterTypeRxPPJC);
    allCounters->txPPJC = CounterGet((AtChannel)path, cAtSdhPathCounterTypeTxPPJC);

    Tha60210061ModuleOcnVirtualFifoCounterEnable(moduleOcn, cAtFalse);
    return cAtOk;
    }

static void OverrideAtModuleSdh(AtModuleSdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleSdhMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleSdhOverride, mMethodsGet(self), sizeof(m_AtModuleSdhOverride));

        mMethodOverride(m_AtModuleSdhOverride, NumTfi5Lines);
        mMethodOverride(m_AtModuleSdhOverride, MaxLinesGet);
        mMethodOverride(m_AtModuleSdhOverride, ChannelCreate);
        mMethodOverride(m_AtModuleSdhOverride, StartEc1LineIdGet);
        mMethodOverride(m_AtModuleSdhOverride, NumEc1LinesGet);
        mMethodOverride(m_AtModuleSdhOverride, DccLayerIsSupported);
        }

    mMethodsSet(self, &m_AtModuleSdhOverride);
    }

static void OverrideThaModuleSdh(AtModuleSdh self)
    {
    ThaModuleSdh moduleSdh = (ThaModuleSdh)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleSdhMethods = mMethodsGet(moduleSdh);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleSdhOverride, mMethodsGet(moduleSdh), sizeof(m_ThaModuleSdhOverride));

        mMethodOverride(m_ThaModuleSdhOverride, NumLinesPerPart);
        mMethodOverride(m_ThaModuleSdhOverride, LineDefaultRate);
        mMethodOverride(m_ThaModuleSdhOverride, SerdesControllerCreate);
        mMethodOverride(m_ThaModuleSdhOverride, ShouldUpdateSerdesTimingWhenLineRateChange);
        mMethodOverride(m_ThaModuleSdhOverride, LineInterruptProcess);
        mMethodOverride(m_ThaModuleSdhOverride, AuPathFromHwIdGet);
        mMethodOverride(m_ThaModuleSdhOverride, MaxNumDccHdlcChannels);
        mMethodOverride(m_ThaModuleSdhOverride, DccChannelObjectCreate);
        mMethodOverride(m_ThaModuleSdhOverride, TuPathFromHwIdGet);
        mMethodOverride(m_ThaModuleSdhOverride, HasLineInterrupt);
        }

    mMethodsSet(moduleSdh, &m_ThaModuleSdhOverride);
    }

static void OverrideTha60210011ModuleSdh(AtModuleSdh self)
    {
    Tha60210011ModuleSdh module = (Tha60210011ModuleSdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011ModuleSdhMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModuleSdhOverride, m_Tha60210011ModuleSdhMethods, sizeof(m_Tha60210011ModuleSdhOverride));

        mMethodOverride(m_Tha60210011ModuleSdhOverride, MaxNumHoLines);
        mMethodOverride(m_Tha60210011ModuleSdhOverride, MaxNumLoLines);
        mMethodOverride(m_Tha60210011ModuleSdhOverride, HoLineStartId);
        mMethodOverride(m_Tha60210011ModuleSdhOverride, LoLineStartId);
        mMethodOverride(m_Tha60210011ModuleSdhOverride, PohMonitorDefaultSet);
        mMethodOverride(m_Tha60210011ModuleSdhOverride, SdhChannelIsHoPath);
        mMethodOverride(m_Tha60210011ModuleSdhOverride, SdhVcBelongsToLoLine);
        mMethodOverride(m_Tha60210011ModuleSdhOverride, AuVcObjectCreate);
        mMethodOverride(m_Tha60210011ModuleSdhOverride, Tu3VcObjectCreate);
        mMethodOverride(m_Tha60210011ModuleSdhOverride, Vc1xObjectCreate);
        }

    mMethodsSet(module, &m_Tha60210011ModuleSdhOverride);
    }

static void Override(AtModuleSdh self)
    {
    OverrideAtModuleSdh(self);
    OverrideThaModuleSdh(self);
    OverrideTha60210011ModuleSdh(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061ModuleSdh);
    }

static AtModuleSdh ObjectInit(AtModuleSdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012ModuleSdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSdh Tha60210061ModuleSdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

eBool Tha60210061ModuleSdhChannelIsFakeVc(AtSdhChannel vc)
    {
    eAtSdhChannelType vcType = AtSdhChannelTypeGet(vc);
    if ((vcType != cAtSdhChannelTypeVc12) && (vcType != cAtSdhChannelTypeVc11))
        return cAtFalse;

    /* Abstract VC does not map to any upper layer */
    if (AtSdhChannelParentChannelGet(vc))
        return cAtFalse;

    return cAtTrue;
    }

eBool Tha60210061ModuleSdhChannelIsInEc1Line(AtModuleSdh self, AtSdhChannel channel)
    {
    uint32 lineId = (uint32)AtSdhChannelLineGet(channel);
    if (lineId < AtModuleSdhStartEc1LineIdGet(self))
        return cAtFalse;

    return cAtTrue;
    }

eBool Tha60210061ModuleSdhLineLoopbackIsSupported(AtModuleSdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);

    return (hwVersion >= StartVersionSupportLineLoopback()) ? cAtTrue : cAtFalse;
    }

eAtRet Tha60210061ModuleSdhEc1PathVirtualFifoCounterGet(AtSdhPath path, tAtSdhPathCounters* allCounters)
    {
    return Ec1PathVirtualFifoCounterGet(path, allCounters, AtChannelCounterGet);
    }

eAtRet Tha60210061ModuleSdhEc1PathVirtualFifoCounterClear(AtSdhPath path, tAtSdhPathCounters* allCounters)
    {
    return Ec1PathVirtualFifoCounterGet(path, allCounters, AtChannelCounterClear);
    }


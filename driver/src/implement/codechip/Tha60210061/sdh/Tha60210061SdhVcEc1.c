/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 *
 * File        : Tha60210061SdhVcEc1.c
 *
 * Created Date: Jun 9, 2017
 *
 * Description : VC for EC1
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../ocn/Tha60210061ModuleOcn.h"
#include "../../Tha60210031/ocn/Tha60210031ModuleOcn.h"
#include "../../Tha60210031/sdh/Tha60210031SdhVc.h"
#include "../../Tha60210031/pdh/Tha60210031PdhSerialLine.h"
#include "../../Tha60210031/pdh/Tha60210031ModulePdh.h"
#include "../../../default/map/ThaModuleDemap.h"
#include "Tha60210061SdhVcInternal.h"
#include "Tha60210061ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061SdhVcEc1
    {
    tTha60210061SdhVc super;
    }tTha60210061SdhVcEc1;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods               m_AtChannelOverride;
static tAtSdhChannelMethods            m_AtSdhChannelOverride;
static tAtSdhVcMethods                 m_AtSdhVcOverride;
static tThaSdhVcMethods                m_ThaSdhVcOverride;
static tThaSdhAuVcMethods              m_ThaSdhAuVcOverride;

/* Save super implementation */
static const tAtChannelMethods               *m_AtChannelMethods = NULL;
static const tAtSdhChannelMethods            *m_AtSdhChannelMethods = NULL;
static const tAtSdhVcMethods                 *m_AtSdhVcMethods = NULL;
static const tThaSdhVcMethods                *m_ThaSdhVcMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    /* With path EC1 do not mapping via AUG path, need enable when channel setup */
    return AtSdhPathPohMonitorEnable((AtSdhPath)self, cAtTrue);
    }

static uint32 OcnPJCounterOffset(ThaSdhVc self, eBool isPosAdj)
    {
    uint8 slice, hwStsInSlice;
    uint8 adjMode = (isPosAdj) ? 0 : 1;
    AtModuleSdh sdhModule =(AtModuleSdh)AtChannelModuleGet((AtChannel)self);
    ThaModuleOcn ocnModule = (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cThaModuleOcn);
    eBool isEc1Channel = Tha60210061ModuleSdhChannelIsInEc1Line(sdhModule, (AtSdhChannel)self);

    if (!isEc1Channel)
        return m_ThaSdhVcMethods->OcnPJCounterOffset(self, isPosAdj);

    ThaSdhChannel2HwMasterStsId((AtSdhChannel)self, cThaModuleOcn, &slice, &hwStsInSlice);
    return (uint32)(4UL * adjMode + hwStsInSlice + Tha60210061ModuleOcnEc1BaseAddress(ocnModule));
    }

static eAtRet Debug(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Debug(self);
    if (ret != cAtOk)
        return ret;
    if (ThaOcnVc3MapDe3LiuIsEnabled((AtSdhChannel)self))
        AtPrintc(cSevInfo, " LIU Over SONET is Enable! \r\n");
    else
        AtPrintc(cSevInfo, " LIU Over SONET is Disable! \r\n");
    return ret;
    }

static eAtModuleSdhRet CanChangeMapping(AtSdhChannel self, uint8 mapType)
    {
    AtUnused(self);
    AtUnused(mapType);
    return cAtOk;
    }

static eBool NeedConfigurePdh(ThaSdhAuVc self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void LiuOverVc3MapUpdate(AtSdhChannel self)
    {
    Tha60210031SdhVcLiuOverVc3MapUpdate(self);
    }

static eAtRet PwCepSet(ThaSdhVc self)
    {
    eAtRet ret = m_ThaSdhVcMethods->PwCepSet(self);
    if (ret != cAtOk)
        return ret;

    if (ThaSdhAuVcMapDe3LiuIsEnabled((ThaSdhAuVc)self))
        {
        ret |= AtSdhPathPohInsertionEnable((AtSdhPath)self, cAtTrue);
        LiuOverVc3MapUpdate((AtSdhChannel)self);
        }

    return ret;
    }

static ThaModuleDemap ModuleDemap(AtSdhVc self)
    {
    AtDevice dev = AtChannelDeviceGet((AtChannel)self);
    ThaModuleDemap module = (ThaModuleDemap)AtDeviceModuleGet(dev, cThaModuleDemap);
    return module;
    }

static eAtModuleSdhRet MapTypeSet(AtSdhChannel self, uint8 mapType)
    {
    eAtRet ret = cAtOk;

    if (AtSdhChannelMapTypeGet(self) != mapType)
        ret = AtChannelLoopbackSet((AtChannel)self, cAtLoopbackModeRelease);

    ret |= m_AtSdhChannelMethods->MapTypeSet(self, mapType);

    if (mapType == cAtSdhVcMapTypeVc3MapDe3)
        LiuOverVc3MapUpdate(self);

    return ret;
    }

static eAtRet StuffEnable(AtSdhVc self, eBool enable)
    {
    eAtRet ret;
    ThaSdhVc vc = (ThaSdhVc)self;

    /* Calling super implementation */
    ret = m_AtSdhVcMethods->StuffEnable(self, enable);
    if (ret != cAtOk)
        return ret;

    if (mMethodsGet(vc)->HasOcnStuffing(vc))
        ret |= ThaModuleDemapVc3StuffEnable(ModuleDemap(self), self, enable);

    return ret;
    }

static eBool HasOcnStuffing(ThaSdhVc self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    uint32 supportedVersion = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x3, 0x9, 0x1028);

    if (currentVersion >= supportedVersion)
        return cAtTrue;

    return cAtFalse;
    }

static void OverrideAtChannel(AtSdhVc self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, Debug);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtSdhChannel(AtSdhVc self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(m_AtSdhChannelOverride));

        mMethodOverride(m_AtSdhChannelOverride, CanChangeMapping);
        mMethodOverride(m_AtSdhChannelOverride, MapTypeSet);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void OverrideAtSdhVc(AtSdhVc self)
    {

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhVcMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhVcOverride, m_AtSdhVcMethods, sizeof(m_AtSdhVcOverride));

        mMethodOverride(m_AtSdhVcOverride, StuffEnable);
        }

    mMethodsSet(self, &m_AtSdhVcOverride);
    }

static void OverrideThaSdhVc(AtSdhVc self)
    {
    ThaSdhVc vc = (ThaSdhVc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaSdhVcMethods = mMethodsGet(vc);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhVcOverride, mMethodsGet(vc), sizeof(m_ThaSdhVcOverride));
        mMethodOverride(m_ThaSdhVcOverride, HasOcnStuffing);
        mMethodOverride(m_ThaSdhVcOverride, PwCepSet);
        mMethodOverride(m_ThaSdhVcOverride, OcnPJCounterOffset);
        }

    mMethodsSet(vc, &m_ThaSdhVcOverride);
    }

static void OverrideThaSdhAuVc(AtSdhVc self)
    {
    ThaSdhAuVc sdhAuVc = (ThaSdhAuVc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhAuVcOverride, mMethodsGet(sdhAuVc), sizeof(m_ThaSdhAuVcOverride));

        mMethodOverride(m_ThaSdhAuVcOverride, NeedConfigurePdh);
        }

    mMethodsSet(sdhAuVc, &m_ThaSdhAuVcOverride);
    }

static void Override(AtSdhVc self)
    {
    OverrideAtChannel(self);
    OverrideAtSdhChannel(self);
    OverrideAtSdhVc(self);
    OverrideThaSdhVc(self);
    OverrideThaSdhAuVc(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061SdhVcEc1);
    }

static AtSdhVc ObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210061SdhVcObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhVc Tha60210061SdhVcEc1New(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhVc self = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (self == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(self, channelId, channelType, module);
    }

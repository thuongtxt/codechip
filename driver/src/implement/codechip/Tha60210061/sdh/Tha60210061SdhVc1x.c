/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60210061SdhVc1x.c
 *
 * Created Date: Aug 26, 2016
 *
 * Description : Vc1x Channel
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/ocn/Tha60210011ModuleOcn.h"
#include "../../Tha60210012/sdh/Tha60210012ModuleSdhInternal.h"
#include "../ocn/Tha60210061ModuleOcnReg.h"
#include "../ocn/Tha60210061ModuleOcnEc1Reg.h"
#include "../ocn/Tha60210061ModuleOcn.h"
#include "Tha60210061ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061SdhVc1x
    {
    tTha60210012Tfi5LineVc1x super;
    }tTha60210061SdhVc1x;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaSdhVcMethods m_ThaSdhVcOverride;

/* Save super implementation */
static const tThaSdhVcMethods *m_ThaSdhVcMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 OcnPJCounterOffset(ThaSdhVc self, eBool isIncrease)
    {
    uint8 slice, hwStsInSlice;
    uint8 adjMode = (isIncrease) ? 0 : 1;
    AtSdhChannel sdhChannel = (AtSdhChannel)self;
    uint8 vtgId = AtSdhChannelTug2Get(sdhChannel);
    uint8 vtId = AtSdhChannelTu1xGet(sdhChannel);
    AtModuleSdh sdhModule =(AtModuleSdh)AtChannelModuleGet((AtChannel)self);
    eBool channelIsEc1 = Tha60210061ModuleSdhChannelIsInEc1Line(sdhModule, (AtSdhChannel)self);
    ThaModuleOcn ocnModule = (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cThaModuleOcn);

    if (!channelIsEc1)
        return m_ThaSdhVcMethods->OcnPJCounterOffset(self, isIncrease);

    if (ThaSdhChannel2HwMasterStsId((AtSdhChannel)self, cThaModuleOcn, &slice, &hwStsInSlice) == cAtOk)
        return (uint32)(128UL * adjMode + 32UL * hwStsInSlice + 4UL * vtgId + vtId + Tha60210061ModuleOcnEc1BaseAddress(ocnModule));

    return cBit31_0;
    }

static void OverrideThaSdhVc(AtSdhVc self)
    {
    ThaSdhVc channel = (ThaSdhVc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaSdhVcMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhVcOverride, mMethodsGet(channel), sizeof(m_ThaSdhVcOverride));

        mMethodOverride(m_ThaSdhVcOverride, OcnPJCounterOffset);
        }

    mMethodsSet(channel, &m_ThaSdhVcOverride);
    }

static void Override(AtSdhVc self)
    {
    OverrideThaSdhVc(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061SdhVc1x);
    }

static AtSdhVc ObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012Tfi5LineVc1xObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhVc Tha60210061SdhVc1xNew(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhVc self = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (self == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(self, channelId, channelType, module);
    }

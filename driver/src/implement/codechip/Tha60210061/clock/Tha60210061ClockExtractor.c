/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLOCK
 *
 * File        : Tha60210061ClockExtractor.c
 *
 * Created Date: Nov 9, 2016
 *
 * Description : Clock Extractor
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPdhDe1.h"
#include "AtPdhDe3.h"
#include "../../../../generic/clock/AtClockExtractorInternal.h"
#include "../../../../generic/physical/AtSerdesControllerInternal.h"
#include "../../../default/man/versionreader/ThaVersionReader.h"
#include "../../../default/man/ThaDevice.h"
#include "../physical/Tha60210061TopCtrReg.h"
#include "../pdh/Tha60210061PdhDe1.h"
#include "../pdh/Tha60210061PdhDe3.h"
#include "Tha60210061ClockExtractor.h"
#include "Tha60210061ModuleClock.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061ClockExtractor
    {
    tAtClockExtractor super;
    }tTha60210061ClockExtractor;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtClockExtractorMethods  m_AtClockExtractorOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 StartVersionSupportClockExtractDisabling(void)
    {
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x7, 0x1020);
    }

static eBool ClockExtractDisablingIsSupported(AtClockExtractor self)
    {
    AtModuleClock clockModule = AtClockExtractorModuleGet(self);
    AtDevice device = AtModuleDeviceGet((AtModule)clockModule);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);

    if (hwVersion >= StartVersionSupportClockExtractDisabling())
        return cAtTrue;

    return cAtFalse;
    }

static eAtModuleClockRet HwClockExtractSet(AtClockExtractor self, uint32 channelId, uint8 hwClockSource)
    {
    uint32 regValue = AtClockExtractorRead(self, cRegClockExtractSelection);

    if (AtClockExtractorIdGet(self) == 0)
        {
        mRegFieldSet(regValue, cRegClockRef8KhzPrimaryClockSource, hwClockSource);
        mRegFieldSet(regValue, cRegClockRef8KhzPrimaryClockChannelId, channelId);
        mRegFieldSet(regValue, cRegClockRef8KhzPrimaryLOS, cClockMaskingValue);
        }
    else
        {
        mRegFieldSet(regValue, cRegClockRef8KhzSecondaryClockSource, hwClockSource);
        mRegFieldSet(regValue, cRegClockRef8KhzSecondaryClockChannelId, channelId);
        mRegFieldSet(regValue, cRegClockRef8KhzSecondaryLOS, cClockMaskingValue);
        }

    AtClockExtractorWrite(self, cRegClockExtractSelection, regValue);
    return cAtOk;
    }

static uint8 HwClockSourceGet(AtClockExtractor self)
    {
    uint32 regValue = AtClockExtractorRead(self, cRegClockExtractSelection);

    if (AtClockExtractorIdGet(self) == 0)
        return (uint8)mRegField(regValue, cRegClockRef8KhzPrimaryClockSource);

    return (uint8)mRegField(regValue, cRegClockRef8KhzSecondaryClockSource);
    }

static uint32 HwChannelIdGet(AtClockExtractor self)
    {
    uint32 regValue = AtClockExtractorRead(self, cRegClockExtractSelection);

    if (AtClockExtractorIdGet(self) == 0)
        return mRegField(regValue, cRegClockRef8KhzPrimaryClockChannelId);

    return mRegField(regValue, cRegClockRef8KhzSecondaryClockChannelId);
    }

static eAtModuleClockRet HwClockExtractMaskingSet(AtClockExtractor self, eBool isDe3, uint8 slice, uint32 pdhId)
    {
    uint32 regValue = AtClockExtractorRead(self, cAf6Reg_Global_PDH_Defect_Output_for_clock_extractor_Base);

    if (AtClockExtractorIdGet(self) == 0)
        {
        mRegFieldSet(regValue, cAf6_Global_PDH_Defect_Output_for_clock_extractor_PrimaryPDHDE3_, mBoolToBin(isDe3));
        mRegFieldSet(regValue, cAf6_Global_PDH_Defect_Output_for_clock_extractor_PrimaryPDHOC24_, slice);
        mRegFieldSet(regValue, cAf6_Global_PDH_Defect_Output_for_clock_extractor_PrimaryPDHID_, pdhId);
        }
    else
        {
        mRegFieldSet(regValue, cAf6_Global_PDH_Defect_Output_for_clock_extractor_SecondaryPDHDE3_, mBoolToBin(isDe3));
        mRegFieldSet(regValue, cAf6_Global_PDH_Defect_Output_for_clock_extractor_SecondaryPDHOC24_, slice);
        mRegFieldSet(regValue, cAf6_Global_PDH_Defect_Output_for_clock_extractor_SecondaryPDHID_, pdhId);
        }

    AtClockExtractorWrite(self, cAf6Reg_Global_PDH_Defect_Output_for_clock_extractor_Base, regValue);
    return cAtOk;
    }

static void De1FrameTypeDidChange(AtPdhChannel de1, uint16 preFrameType, uint16 newFrameType, void *userData)
    {
    AtClockExtractor extractor = (AtClockExtractor)userData;
    eAtRet ret;

    /* Reconfigure extractor to apply new frame type */
    uint32 channelId = AtChannelIdGet((AtChannel)de1);
    uint8 hwClockSource = AtPdhDe1FrameTypeIsE1(newFrameType) ? cSourceE1Liu : cSourceDs1Liu;

    ret = HwClockExtractSet((AtClockExtractor)userData, channelId, hwClockSource);
    if ((ret != cAtOk) || (!Tha60210061ModuleClockAlarmMaskingIsSupported(AtClockExtractorModuleGet(extractor))))
        return;

    if (AtPdhDe1FrameTypeIsE1(preFrameType) != AtPdhDe1FrameTypeIsE1(newFrameType))
        {
        uint8 slice;
        uint32 pdhId = Th60210061PdhDe1LiuPdhIdGet((AtPdhDe1)de1, &slice);
        HwClockExtractMaskingSet(extractor, cAtFalse, slice, pdhId);
        }
    }

static tAtPdhChannelPropertyListener* De1Listener(void)
    {
    static tAtPdhChannelPropertyListener* listener = NULL;
    static tAtPdhChannelPropertyListener listenerHolder;

    if (listener)
        return listener;

    AtOsalMemInit(&listenerHolder, 0, sizeof(listenerHolder));
    listenerHolder.FrameTypeDidChange = De1FrameTypeDidChange;
    listener = &listenerHolder;

    return listener;
    }

static eAtRet De1ListenerAdd(AtClockExtractor self, AtPdhDe1 de1)
    {
    return AtPdhChannelPropertyListenerAdd((AtPdhChannel)de1, De1Listener(), self);
    }

static eAtRet De1ListenerRemove(AtClockExtractor self, AtPdhDe1 de1)
    {
    return AtPdhChannelPropertyListenerRemove((AtPdhChannel)de1, De1Listener(), self);
    }

static uint32 StartDe3ClockId(AtClockExtractor self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)AtClockExtractorModuleGet(self));
    return AtModulePdhNumberOfDe1sGet((AtModulePdh)AtDeviceModuleGet(device, cAtModulePdh));
    }

static void De3FrameTypeDidChange(AtPdhChannel de3, uint16 preFrameType, uint16 newFrameType, void* userData)
    {
    AtClockExtractor extractor = (AtClockExtractor)userData;
    uint32 hwClockId = StartDe3ClockId(extractor) + AtChannelIdGet((AtChannel)de3);
    uint8 hwClockSource = AtPdhDe3FrameTypeIsE3(newFrameType) ? cSourceE3Liu : cSourceDS3Liu;
    AtUnused(preFrameType);

    HwClockExtractSet(extractor, hwClockId, hwClockSource);
    }

static tAtPdhChannelPropertyListener* De3Listener(void)
    {
    static tAtPdhChannelPropertyListener* listener = NULL;
    static tAtPdhChannelPropertyListener listenerHolder;

    if (listener)
        return listener;

    AtOsalMemInit(&listenerHolder, 0, sizeof(listenerHolder));
    listenerHolder.FrameTypeDidChange = De3FrameTypeDidChange;
    listener = &listenerHolder;

    return listener;
    }

static eAtRet De3ListenerAdd(AtClockExtractor self, AtPdhDe3 de3)
    {
    return AtPdhChannelPropertyListenerAdd((AtPdhChannel)de3, De3Listener(), self);
    }

static eAtRet De3ListenerRemove(AtClockExtractor self, AtPdhDe3 de3)
    {
    return AtPdhChannelPropertyListenerRemove((AtPdhChannel)de3, De3Listener(), self);
    }

static void SerdesModeDidChange(AtSerdesController serdes, eAtSerdesMode mode, void* userData)
    {
    uint32 channelId = AtSerdesControllerIdGet(serdes);
    uint8 hwClockSource = (mode == cAtSerdesModeEth1G) ? cSourceEth : cSourceOCN;

    HwClockExtractSet((AtClockExtractor)userData, channelId, hwClockSource);
    }

static tAtSerdesControllerPropertyListener* SerdesListener(void)
    {
    static tAtSerdesControllerPropertyListener* listener = NULL;
    static tAtSerdesControllerPropertyListener listenerHolder;

    if (listener)
        return listener;

    AtOsalMemInit(&listenerHolder, 0, sizeof(listenerHolder));
    listenerHolder.ModeDidChange = SerdesModeDidChange;
    listener = &listenerHolder;

    return listener;
    }

static eAtRet SerdesListenerAdd(AtClockExtractor self, AtSerdesController serdes)
    {
    return AtSerdesControllerPropertyListenerAdd(serdes, SerdesListener(), self);
    }

static eAtRet SerdesListenerRemove(AtClockExtractor self, AtSerdesController serdes)
    {
    return AtSerdesControllerPropertyListenerRemove(serdes, SerdesListener(), self);
    }

static void CurrentSourceListenerRemove(AtClockExtractor self)
    {
    AtPdhDe3 de3;
    AtPdhDe1 de1;
    AtSerdesController serdes;

    de1 = AtClockExtractorPdhDe1Get(self);
    if (de1)
        De1ListenerRemove(self, de1);

    de3 = AtClockExtractorPdhDe3Get(self);
    if (de3)
        De3ListenerRemove(self, de3);

    serdes = AtClockExtractorSerdesGet(self);
    if (serdes)
        SerdesListenerRemove(self, serdes);
    }

static eAtModuleClockRet PdhDe1LiuClockExtract(AtClockExtractor self, AtPdhDe1 de1)
    {
    uint32 channelId, pdhId;
    uint8 hwClockSource, slice;
    eAtRet ret;

    if (AtClockExtractorPdhDe1Get(self) == de1)
        return cAtOk;

    CurrentSourceListenerRemove(self);
    channelId = AtChannelIdGet((AtChannel)de1);
    hwClockSource = AtPdhDe1IsE1(de1) ? cSourceE1Liu : cSourceDs1Liu;

    ret = HwClockExtractSet(self, channelId, hwClockSource);
    if (ret != cAtOk)
        return ret;

    ret = De1ListenerAdd(self, de1);
    if ((ret != cAtOk) || (!Tha60210061ModuleClockAlarmMaskingIsSupported(AtClockExtractorModuleGet(self))))
        return ret;

    pdhId = Th60210061PdhDe1LiuPdhIdGet(de1, &slice);
    return HwClockExtractMaskingSet(self, cAtFalse, slice, pdhId);
    }

static eAtModuleClockRet PdhDe3LiuClockExtract(AtClockExtractor self, AtPdhDe3 de3)
    {
    uint32 hwClockId, pdhId;
    uint8 hwClockSource, slice;
    eAtRet ret;

    if (AtClockExtractorPdhDe3Get(self) == de3)
        return cAtOk;

    hwClockId = StartDe3ClockId(self) + AtChannelIdGet((AtChannel)de3);
    hwClockSource = AtPdhDe3IsE3(de3) ? cSourceE3Liu : cSourceDS3Liu;

    ret = HwClockExtractSet(self, hwClockId, hwClockSource);
    if (ret != cAtOk)
        return ret;

    ret = De3ListenerAdd(self, de3);
    if ((ret != cAtOk) || (!Tha60210061ModuleClockAlarmMaskingIsSupported(AtClockExtractorModuleGet(self))))
        return ret;

    pdhId = Tha60210061PdhDe3LiuPdhIdGet(de3, &slice);
    return HwClockExtractMaskingSet(self, cAtTrue, slice, pdhId);
    }

static eAtModuleClockRet SerdesClockExtract(AtClockExtractor self, AtSerdesController serdes)
    {
    uint32 channelId;
    uint8 hwClockSource;
    eAtRet ret;

    if (AtClockExtractorSerdesGet(self) == serdes)
        return cAtOk;

    channelId = AtSerdesControllerIdGet(serdes);
    hwClockSource = (AtSerdesControllerModeGet(serdes) == cAtSerdesModeEth1G) ? cSourceEth : cSourceOCN;

    ret = HwClockExtractSet(self, channelId, hwClockSource);
    if (ret == cAtOk)
        return SerdesListenerAdd(self, serdes);

    return ret;
    }

static eBool PdhDe1LiuClockIsExtracted(AtClockExtractor self)
    {
    uint8 hwClockSource = HwClockSourceGet(self);

    if ((hwClockSource == cSourceE1Liu) || (hwClockSource == cSourceDs1Liu))
        return cAtTrue;

    return cAtFalse;
    }

static eBool PdhDe3LiuClockIsExtracted(AtClockExtractor self)
    {
    uint8 hwClockSource = HwClockSourceGet(self);

    if ((hwClockSource == cSourceE3Liu) || (hwClockSource == cSourceDS3Liu))
        return cAtTrue;

    return cAtFalse;
    }

static AtSerdesController SerdesGet(AtClockExtractor self)
    {
    AtDevice device;
    uint8 hwClockSource;
    AtSerdesManager manager;

    hwClockSource = HwClockSourceGet(self);
    device = AtModuleDeviceGet((AtModule)AtClockExtractorModuleGet(self));
    manager = AtDeviceSerdesManagerGet(device);
    if ((hwClockSource == cSourceEth) || (hwClockSource == cSourceOCN))
        return AtSerdesManagerSerdesControllerGet(manager, HwChannelIdGet(self));

    return NULL;
    }

static AtModulePdh PdhModuleGet(AtClockExtractor self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)AtClockExtractorModuleGet(self));
    return (AtModulePdh)AtDeviceModuleGet(device, cAtModulePdh);
    }

static AtPdhDe3 PdhDe3Get(AtClockExtractor self)
    {
    uint8 hwClockSource = HwClockSourceGet(self);
    uint32 startDe3ClockId = StartDe3ClockId(self);

    if ((hwClockSource == cSourceE3Liu) || (hwClockSource == cSourceDS3Liu))
        return AtModulePdhDe3Get(PdhModuleGet(self), HwChannelIdGet(self) - startDe3ClockId);

    return NULL;
    }

static AtPdhDe1 PdhDe1Get(AtClockExtractor self)
    {
    uint8 hwClockSource = HwClockSourceGet(self);

    if ((hwClockSource == cSourceE1Liu) || (hwClockSource == cSourceDs1Liu))
        return AtModulePdhDe1Get(PdhModuleGet(self), HwChannelIdGet(self));

    return NULL;
    }

static eAtModuleClockRet HwEnable(AtClockExtractor self, eBool enable)
    {
    uint32 regValue = AtClockExtractorRead(self, cRegClockExtractSelection);

    if (AtClockExtractorIdGet(self) == 0)
        mRegFieldSet(regValue, cRegClockRef8KhzRefOutDis0, enable ? 0 : 1);
    else
        mRegFieldSet(regValue, cRegClockRef8KhzRefOutDis1, enable ? 0 : 1);
        
    AtClockExtractorWrite(self, cRegClockExtractSelection, regValue);

    return cAtOk;
    }

static eBool HwIsEnabled(AtClockExtractor self)
    {
    uint32 regValue = AtClockExtractorRead(self, cRegClockExtractSelection);

    if (AtClockExtractorIdGet(self) == 0)
       return mRegField(regValue, cRegClockRef8KhzRefOutDis0) == 0 ? cAtTrue : cAtFalse;
       
    return mRegField(regValue, cRegClockRef8KhzRefOutDis1) == 0 ? cAtTrue : cAtFalse;
    }

static eAtModuleClockRet Enable(AtClockExtractor self, eBool enable)
    {
    if (ClockExtractDisablingIsSupported(self))
        return HwEnable(self, enable);

    return (enable) ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool IsEnabled(AtClockExtractor self)
    {
    if (ClockExtractDisablingIsSupported(self))
        return HwIsEnabled(self);

    return cAtTrue;
    }

static ThaModuleClock ThaModuleClockGet(AtClockExtractor self)
    {
    return (ThaModuleClock)AtClockExtractorModuleGet(self);
    }

static eAtModuleClockRet SquelchingOptionSet(AtClockExtractor self, eAtClockExtractorSquelching options)
    {
    return ThaModuleClockSquelchingOptionSet(ThaModuleClockGet(self), self, options);
    }

static uint32 SquelchingOptionGet(AtClockExtractor self)
    {
    return ThaModuleClockSquelchingOptionGet(ThaModuleClockGet(self), self);
    }

static uint32 OutputCounterGet(AtClockExtractor self)
    {
    return ThaModuleClockOutputCounterGet(ThaModuleClockGet(self), self);
    }

static void DebugClockCopiedExtractor(AtClockExtractor self)
    {
    uint32 id = AtClockExtractorIdGet(self);

    AtPrintf("Clock extractor #%d, output counter: %d Khz\r\n", id, AtClockExtractorOutputCounterGet(self));
    if (id == 0)
        {
        AtPrintf("Copied Clock extractor #%d, output counter: %d Khz\r\n", id, AtClockExtractorRead(self, 0xF0007E));
        }
    else if (id == 1)
        {
        AtPrintf("Copied Clock extractor #%d, output counter: %d Khz\r\n", id, AtClockExtractorRead(self, 0xF0007F));
        }
    }

static void Debug(AtClockExtractor self)
    {
    if (AtClockExtractorSquelchingIsSupported(self))
        {
        DebugClockCopiedExtractor(self);
        }
    }

static eBool SquelchingIsSupported(AtClockExtractor self)
    {
    AtModule module = (AtModule)AtClockExtractorModuleGet(self);

    return ThaModuleClockSquelchingIsSupported((ThaModuleClock)module);
    }

static void OverrideAtClockExtractor(AtClockExtractor self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtClockExtractorOverride, mMethodsGet(self), sizeof(m_AtClockExtractorOverride));

        mMethodOverride(m_AtClockExtractorOverride, PdhDe1Get);
        mMethodOverride(m_AtClockExtractorOverride, PdhDe3Get);
        mMethodOverride(m_AtClockExtractorOverride, SerdesGet);
        mMethodOverride(m_AtClockExtractorOverride, PdhDe1LiuClockExtract);
        mMethodOverride(m_AtClockExtractorOverride, PdhDe3LiuClockExtract);
        mMethodOverride(m_AtClockExtractorOverride, SerdesClockExtract);
        mMethodOverride(m_AtClockExtractorOverride, PdhDe1LiuClockIsExtracted);
        mMethodOverride(m_AtClockExtractorOverride, PdhDe3LiuClockIsExtracted);
        mMethodOverride(m_AtClockExtractorOverride, Enable);
        mMethodOverride(m_AtClockExtractorOverride, IsEnabled);
        mMethodOverride(m_AtClockExtractorOverride, SquelchingOptionSet);
        mMethodOverride(m_AtClockExtractorOverride, SquelchingOptionGet);
        mMethodOverride(m_AtClockExtractorOverride, OutputCounterGet);
        mMethodOverride(m_AtClockExtractorOverride, Debug);
        mMethodOverride(m_AtClockExtractorOverride, SquelchingIsSupported);
        }

    mMethodsSet(self, &m_AtClockExtractorOverride);
    }

static void Override(AtClockExtractor self)
    {
    OverrideAtClockExtractor(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061ClockExtractor);
    }

static AtClockExtractor ObjectInit(AtClockExtractor self, AtModuleClock clockModule, uint8 extractorId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtClockExtractorObjectInit(self, clockModule, extractorId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtClockExtractor Tha60210061ClockExtractorNew(AtModuleClock clockModule, uint8 extractorId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtClockExtractor newExtractor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newExtractor == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newExtractor, clockModule, extractorId);
    }


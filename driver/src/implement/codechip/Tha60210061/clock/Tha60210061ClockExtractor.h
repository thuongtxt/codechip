/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLOCK
 * 
 * File        : Tha60210061ClockExtractor.h
 * 
 * Created Date: Nov 9, 2016
 *
 * Description : Clock Extractor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061CLOCKEXTRACTOR_H_
#define _THA60210061CLOCKEXTRACTOR_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtClockExtractor Tha60210061ClockExtractorNew(AtModuleClock clockModule, uint8 extractorId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061CLOCKEXTRACTOR_H_ */


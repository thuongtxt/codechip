/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLOCK
 *
 * File        : Tha60210061ModuleClock.c
 *
 * Created Date: Nov 9, 2016
 *
 * Description : Clock module of 60210061
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210051/clock/Tha60210051ModuleClockInternal.h"
#include "../physical/Tha60210061TopCtrReg.h"
#include "Tha60210061ClockExtractor.h"
#include "Tha60210061ModuleClock.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061ModuleClock
    {
    tTha60210051ModuleClock super;
    }tTha60210061ModuleClock;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods       m_AtModuleOverride;
static tAtModuleClockMethods  m_AtModuleClockOverride;
static tThaModuleClockMethods m_ThaModuleClockOverride;

static const tAtModuleMethods *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 StartHwVersionSupportClockExtractor(void)
    {
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x3, 0x1028);
    }

static eBool ClockExtractorIsSupported(AtModuleClock self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);

    if (hwVersion >= StartHwVersionSupportClockExtractor())
        return cAtTrue;

    return cAtFalse;
    }

static AtClockExtractor ExtractorCreate(ThaModuleClock self, uint8 extractorId)
    {
    return Tha60210061ClockExtractorNew((AtModuleClock)self, extractorId);
    }

static uint8 NumExtractors(AtModuleClock self)
    {
    if (ClockExtractorIsSupported(self))
        return 2;

    return 0;
    }

static AtModulePdh ModulePdh(AtModule self)
    {
    return (AtModulePdh)AtDeviceModuleGet(AtModuleDeviceGet(self), cAtModulePdh);
    }

static eAtRet AllClockExtractorsInit(AtModule self)
    {
    uint8 extractor_i;

    for (extractor_i = 0; extractor_i < AtModuleClockNumExtractors((AtModuleClock)self); extractor_i++)
        {
        AtClockExtractor extractor = AtModuleClockExtractorGet((AtModuleClock)self, extractor_i);
        AtPdhDe1 de1 = AtModulePdhDe1Get(ModulePdh(self), 0);

        /* There is case when number of extractor > 0 but extractor has not been created due to version compatible checking,
         * check here to not cause log message */
        if (extractor)
            {
            AtClockExtractorPdhDe1LiuClockExtract(extractor, de1);
            AtClockExtractorEnable(extractor, cAtFalse);
            }
        }

    return cAtOk;
    }

static eAtRet ExtractorsMaskingInit(AtModule self)
    {
    uint32 regVal = mModuleHwRead(self, cAf6Reg_Global_OCN_Masking_clock_extractor_Base);

    mRegFieldSet(regVal, cAf6_Global_OCN_Masking_clock_extractor_SecondAISMask_, cClockMaskingValue);
    mRegFieldSet(regVal, cAf6_Global_OCN_Masking_clock_extractor_SecondLOFMask_, cClockMaskingValue);
    mRegFieldSet(regVal, cAf6_Global_OCN_Masking_clock_extractor_SecondLOSMask_, cClockMaskingValue);

    mRegFieldSet(regVal, cAf6_Global_OCN_Masking_clock_extractor_PrimaryAISMask_, cClockMaskingValue);
    mRegFieldSet(regVal, cAf6_Global_OCN_Masking_clock_extractor_PrimaryLOFMask_, cClockMaskingValue);
    mRegFieldSet(regVal, cAf6_Global_OCN_Masking_clock_extractor_PrimaryLOSMask_, cClockMaskingValue);

    mModuleHwWrite(self, cAf6Reg_Global_OCN_Masking_clock_extractor_Base, regVal);
    return cAtOk;
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    ret |= AllClockExtractorsInit(self);

    if (Tha60210061ModuleClockAlarmMaskingIsSupported((AtModuleClock)self))
        ret |= ExtractorsMaskingInit(self);

    return ret;
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static uint32 StartVersionSupportAlarmMasking(void)
    {
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x8, 0x1037);
    }

static uint32 SquelchingHwAddress(ThaModuleClock self, AtClockExtractor extractor)
    {
    AtUnused(self);
    AtUnused(extractor);

    return 0x11;
    }

static uint32 SquelchingHwMask(ThaModuleClock self, AtClockExtractor extractor)
    {
    uint32 id = AtClockExtractorIdGet(extractor);

    AtUnused(self);
    return (cBit10_8 << (3 * id));
    }

static uint32 SquelchingHwShift(ThaModuleClock self, AtClockExtractor extractor)
    {
    uint32 id = AtClockExtractorIdGet(extractor);

    AtUnused(self);
    return (8 + (3 * id));
    }

static uint32 OutputCounterHwAddress(ThaModuleClock self, AtClockExtractor extractor)
    {
    uint32 id = AtClockExtractorIdGet(extractor);

    AtUnused(self);

    if (id == 0)
        return 0xF0007C;

    if (id == 1)
        return 0xF0007D;

    return cBit31_0;
    }

static uint32 OutputCounterHwMask(ThaModuleClock self, AtClockExtractor extractor)
    {
    AtUnused(self);
    AtUnused(extractor);
    return cBit31_0;
    }

static uint32 OutputCounterHwShift(ThaModuleClock self, AtClockExtractor extractor)
    {
    AtUnused(self);
    AtUnused(extractor);
    return 0;
    }

static eBool SquelchingIsSupported(ThaModuleClock self)
    {
    AtModule module = (AtModule)self;
    AtDevice device = AtModuleDeviceGet(module);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 startVersionSupportClockSquel = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x8, 0x1043);

    if (ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader) >= startVersionSupportClockSquel)
        return cAtTrue;

    return cAtFalse;
    }

static void OverrideThaModuleClock(AtModuleClock self)
    {
    ThaModuleClock clockModule = (ThaModuleClock)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleClockOverride, mMethodsGet(clockModule), sizeof(m_ThaModuleClockOverride));

        mMethodOverride(m_ThaModuleClockOverride, ExtractorCreate);
        mMethodOverride(m_ThaModuleClockOverride, SquelchingHwAddress);
        mMethodOverride(m_ThaModuleClockOverride, SquelchingHwMask);
        mMethodOverride(m_ThaModuleClockOverride, SquelchingHwShift);
        mMethodOverride(m_ThaModuleClockOverride, OutputCounterHwAddress);
        mMethodOverride(m_ThaModuleClockOverride, OutputCounterHwMask);
        mMethodOverride(m_ThaModuleClockOverride, OutputCounterHwShift);
        mMethodOverride(m_ThaModuleClockOverride, SquelchingIsSupported);
        }

    mMethodsSet(clockModule, &m_ThaModuleClockOverride);
    }

static void OverrideAtModuleClock(AtModuleClock self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleClockOverride, mMethodsGet(self), sizeof(m_AtModuleClockOverride));

        mMethodOverride(m_AtModuleClockOverride, NumExtractors);
        }

    mMethodsSet(self, &m_AtModuleClockOverride);
    }

static void OverrideAtModule(AtModuleClock self)
    {
    AtModule module = (AtModule)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void Override(AtModuleClock self)
    {
    OverrideAtModuleClock(self);
    OverrideThaModuleClock(self);
    OverrideAtModule(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061ModuleClock);
    }

static AtModuleClock ObjectInit(AtModuleClock self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051ModuleClockObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleClock Tha60210061ModuleClockNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleClock newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

eBool Tha60210061ModuleClockAlarmMaskingIsSupported(AtModuleClock self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);

    return (hwVersion >= StartVersionSupportAlarmMasking()) ? cAtTrue : cAtFalse;
    }

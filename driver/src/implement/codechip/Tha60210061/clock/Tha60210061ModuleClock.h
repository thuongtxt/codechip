/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLOCK
 * 
 * File        : Tha60210061ModuleClock.h
 * 
 * Created Date: Nov 9, 2016
 *
 * Description : Module clock
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061MODULECLOCK_H_
#define _THA60210061MODULECLOCK_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cClockMaskingValue 0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleClock Tha60210061ModuleClockNew(AtDevice device);
eBool Tha60210061ModuleClockAlarmMaskingIsSupported(AtModuleClock self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061MODULECLOCK_H_ */


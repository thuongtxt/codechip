/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDA
 *
 * File        : Tha60210061ModulePda.c
 *
 * Created Date: Aug 29, 2016
 *
 * Description : 60210061 module PDA
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210012/pda/Tha60210012ModulePdaReg.h"
#include "../../Tha60210011/pw/Tha60210011ModulePw.h"
#include "Tha60210061ModulePda.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061ModulePda
    {
    tTha60210012ModulePda super;
    }tTha60210061ModulePda;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePdaMethods         m_ThaModulePdaOverride;
static tTha60210011ModulePdaMethods m_Tha60210011ModulePdaOverride;
static tTha60210012ModulePdaMethods m_Tha60210012ModulePdaOverride;

/* Save super implementation */
static const tTha60210011ModulePdaMethods *m_Tha60210011ModulePdaMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool UseNewLookUpControlRegister(Tha60210012ModulePda self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 BaseAddress(void)
    {
    return 0x500000;
    }

static uint32 StartVersionSupportLowRateCesop(Tha60210011ModulePda self)
    {
    AtUnused(self);
    return 0x0;
    }

static eBool PwCwAutoRxMBitIsConfigurable(ThaModulePda self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    uint32 startVersionSupport = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x3, 0x4, 0x1020);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(ThaDeviceVersionReader(device));
    return (currentVersion >= startVersionSupport) ? cAtTrue : cAtFalse;
    }

static uint32 MaxNumJitterBufferBlocks(ThaModulePda self)
    {
    AtUnused(self);
    return 524288;
    }

static uint32 NumFreeBlocksHwGet(ThaModulePda self)
    {
    return mModuleHwRead(self, cHwBlockCheckReg) & cBit19_0;
    }

static void OverrideThaModulePda(AtModule self)
    {
    ThaModulePda pdaModue = (ThaModulePda)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdaOverride, mMethodsGet(pdaModue), sizeof(m_ThaModulePdaOverride));

        mMethodOverride(m_ThaModulePdaOverride, PwCwAutoRxMBitIsConfigurable);
        mMethodOverride(m_ThaModulePdaOverride, MaxNumJitterBufferBlocks);
        mMethodOverride(m_ThaModulePdaOverride, NumFreeBlocksHwGet);
        }

    mMethodsSet(pdaModue, &m_ThaModulePdaOverride);
    }

static void OverrideTha60210011ModulePda(AtModule self)
    {
    Tha60210011ModulePda pdaModule = (Tha60210011ModulePda)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011ModulePdaMethods = mMethodsGet(pdaModule);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModulePdaOverride, m_Tha60210011ModulePdaMethods, sizeof(m_Tha60210011ModulePdaOverride));

        mMethodOverride(m_Tha60210011ModulePdaOverride, StartVersionSupportLowRateCesop);
        }

    mMethodsSet(pdaModule, &m_Tha60210011ModulePdaOverride);
    }

static void OverrideTha60210012ModulePda(AtModule self)
    {
    Tha60210012ModulePda pdaModule = (Tha60210012ModulePda)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210012ModulePdaOverride, mMethodsGet(pdaModule), sizeof(m_Tha60210012ModulePdaOverride));

        mMethodOverride(m_Tha60210012ModulePdaOverride, UseNewLookUpControlRegister);
        }

    mMethodsSet(pdaModule, &m_Tha60210012ModulePdaOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModulePda(self);
    OverrideTha60210011ModulePda(self);
    OverrideTha60210012ModulePda(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061ModulePda);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012ModulePdaObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210061ModulePdaNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

eAtRet Tha60210061ModulePdaGeBypassEnable(ThaModulePda self, AtEthPort gePort, eBool enabled)
    {
    uint32 hwId    = AtChannelIdGet((AtChannel)gePort) - 1;
    uint32 regAddr = BaseAddress() + Tha60210012ModulePdaGeBypassLookUpControlReg(self) + hwId;
    uint32 regVal  = mModuleHwRead(self, regAddr);

    mRegFieldSet(regVal, cAf6_ramgelkupcfg_GeLkEnable_, enabled ? 1 : 0);
    mRegFieldSet(regVal, cAf6_ramgelkupcfg_GeServiceID_, hwId);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

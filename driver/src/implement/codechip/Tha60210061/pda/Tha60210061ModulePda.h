/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDA
 * 
 * File        : Tha60210061ModulePda.h
 * 
 * Created Date: Sep 14, 2016
 *
 * Description : 60210061 PDA interfaces
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061MODULEPDA_H_
#define _THA60210061MODULEPDA_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtEthPort.h"
#include "../../Tha60210012/pda/Tha60210012ModulePda.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 Tha60210061ModulePdaPwPrbsAlarmGet(AtPw pw);
eAtRet Tha60210061ModulePdaGeBypassEnable(ThaModulePda self, AtEthPort gePort, eBool enabled);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061MODULEPDA_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha60210061SgmiiSerdesPrbsEngine.c
 *
 * Created Date: Jun 23, 2016
 *
 * Description : 60210061 SGMII serdes PRBS engine
 *
 * Notes       : PRBS engine
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/eth/AtModuleEthInternal.h"
#include "../../../default/eth/ThaModuleEth.h"
#include "Tha60210061ModulePrbs.h"
#include "Tha60210061SerdesPrbsEngineInternal.h"
#include "Tha60210061SgmiiQsgmiiSerdesPrbsEngineRegv2.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)         ((tTha60210061SgmiiSerdesPrbsEngine*)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061SgmiiSerdesPrbsEnginev2
    {
    tTha60210061SgmiiSerdesPrbsEngine super;
    }tTha60210061SgmiiSerdesPrbsEnginev2;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPrbsEngineMethods m_AtPrbsEngineOverride;
static tTha60210061QsgmiiSerdesPrbsEngineMethods m_Tha60210061QsgmiiSerdesPrbsEngineOverride;

/* Save super implementation */
static const tAtPrbsEngineMethods *m_AtPrbsEngineMethods = NULL;
static const tTha60210061QsgmiiSerdesPrbsEngineMethods *m_Tha60210061QsgmiiSerdesPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet DestMacSet(AtPrbsEngine self, uint8* mac)
    {
    uint32 engineId = AtPrbsEngineIdGet(self);
    return Tha60210061SerdesPrbsQsgmiiDestMacSubChannelSet(self, engineId, mac);
    }

static eAtRet SrcMacSet(AtPrbsEngine self, uint8* mac)
    {
    uint32 engineId = AtPrbsEngineIdGet(self);
    return Tha60210061SerdesPrbsQsgmiiSrcMacSubChannelSet(self, engineId, mac);
    }

static eAtRet EthernetTypeSet(AtPrbsEngine self, uint16 ethType)
    {
    uint32 engineId = AtPrbsEngineIdGet(self);
    return Tha60210061SerdesPrbsQsgmiiEthernetTypeSubChannelSet(self, engineId, ethType);
    }

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    uint32 engineId = AtPrbsEngineIdGet(self);
    return Tha60210061SerdesPrbsQsgmiiSubChannelEnable(self, engineId, enable);
    }

static eBool IsEnabled(AtPrbsEngine self)
    {
    uint32 engineId = AtPrbsEngineIdGet(self);
    return Tha60210061SerdesPrbsQsgmiiSubChannelIsEnabled(self, engineId);
    }

static eAtModulePrbsRet ErrorForce(AtPrbsEngine self, eBool force)
    {
    uint32 engineId = AtPrbsEngineIdGet(self);
    return Tha60210061SerdesPrbsQsgmiiSubChannelErrorForce(self, engineId, force);
    }

static eBool ErrorIsForced(AtPrbsEngine self)
    {
    uint32 engineId = AtPrbsEngineIdGet(self);
    return Tha60210061SerdesPrbsQsgmiiSubChannelErrorIsForced(self, engineId);
    }

static uint32 AlarmHistoryHelper(AtPrbsEngine self, eBool readToClear)
    {
    uint32 engineId = AtPrbsEngineIdGet(self);
    return Tha60210061SerdesPrbsQsgmiiSubChannelAlarmHistoryHelper(self, engineId, readToClear);
    }

static uint32 AlarmHistoryGet(AtPrbsEngine self)
    {
    return AlarmHistoryHelper(self, cAtFalse);
    }

static uint32 AlarmHistoryClear(AtPrbsEngine self)
    {
    return AlarmHistoryHelper(self, cAtTrue);
    }

static uint32 CounterGet(AtPrbsEngine self, uint16 counterType)
    {
    uint32 engineId = AtPrbsEngineIdGet(self);
    return Tha60210061SerdesPrbsQsgmiiSubChannelCounterGet(self, engineId, counterType);
    }

static uint32 CounterClear(AtPrbsEngine self, uint16 counterType)
    {
    uint32 engineId = AtPrbsEngineIdGet(self);
    return Tha60210061SerdesPrbsQsgmiiSubChannelCounterClear(self, engineId, counterType);
    }

static eAtModulePrbsRet ModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    uint32 engineId = AtPrbsEngineIdGet(self);
    return Tha60210061SerdesPrbsQsgmiiSubChannelModeSet(self, engineId, prbsMode);
    }

static eAtPrbsMode ModeGet(AtPrbsEngine self)
    {
    uint32 engineId = AtPrbsEngineIdGet(self);
    return Tha60210061SerdesPrbsQsgmiiSubChannelModeGet(self, engineId);
    }

static eAtModulePrbsRet FixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    uint32 engineId = AtPrbsEngineIdGet(self);
    return Tha60210061SerdesPrbsQsgmiiSubChannelFixedPatternSet(self, engineId, fixedPattern);
    }

static uint32 FixedPatternGet(AtPrbsEngine self)
    {
    uint32 engineId = AtPrbsEngineIdGet(self);
    return Tha60210061SerdesPrbsQsgmiiSubChannelFixedPatternGet(self, engineId);
    }

static void HwDefault(AtPrbsEngine self)
    {
    uint32 engineId = AtPrbsEngineIdGet(self);
    Tha60210061SerdesPrbsQsgmiiSubChannelHwDefault(self, engineId);
    }

static void MacInit(AtPrbsEngine self)
    {
    static uint8 destMac[] = {0xC0, 0xCA, 0xC0, 0xCA, 0xC0, 0xCA};
    static uint8 srcMac[] = {0xCA, 0xFE, 0xCA, 0xFE, 0xCA, 0xFE};

    DestMacSet(self, destMac);
    SrcMacSet(self, srcMac);
    EthernetTypeSet(self, 0x8847);
    }

static eAtModulePrbsRet Init(AtPrbsEngine self)
    {
    eAtRet ret = cAtOk;

    ret |= AtPrbsEngineErrorForce(self, cAtFalse);
    ret |= AtPrbsEngineEnable(self, cAtFalse);
    MacInit(self);
    HwDefault(self);
    return ret;
    }

static void Debug(AtPrbsEngine self)
    {
    uint32 engineId = AtPrbsEngineIdGet(self);
    Tha60210061SerdesPrbsQsgmiiSubChannelDebug(self, engineId);
    }

static void OverrideAtPrbsEngineSerdes(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, mMethodsGet(self), sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Enable);
        mMethodOverride(m_AtPrbsEngineOverride, IsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, CounterGet);
        mMethodOverride(m_AtPrbsEngineOverride, CounterClear);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorForce);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorIsForced);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryClear);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryGet);
        mMethodOverride(m_AtPrbsEngineOverride, Init);
        mMethodOverride(m_AtPrbsEngineOverride, ModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, FixedPatternSet);
        mMethodOverride(m_AtPrbsEngineOverride, FixedPatternGet);
        mMethodOverride(m_AtPrbsEngineOverride, Debug);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void OverrideTha60210061QsgmiiSerdesPrbsEngine(AtPrbsEngine self)
    {
    Tha60210061QsgmiiSerdesPrbsEngine engine = (Tha60210061QsgmiiSerdesPrbsEngine)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210061QsgmiiSerdesPrbsEngineMethods = mMethodsGet(engine);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210061QsgmiiSerdesPrbsEngineOverride, mMethodsGet(engine), sizeof(m_Tha60210061QsgmiiSerdesPrbsEngineOverride));

        mMethodOverride(m_Tha60210061QsgmiiSerdesPrbsEngineOverride, DestMacSet);
        mMethodOverride(m_Tha60210061QsgmiiSerdesPrbsEngineOverride, SrcMacSet);
        mMethodOverride(m_Tha60210061QsgmiiSerdesPrbsEngineOverride, EthernetTypeSet);
        }

    mMethodsSet(engine, &m_Tha60210061QsgmiiSerdesPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtPrbsEngineSerdes(self);
    OverrideTha60210061QsgmiiSerdesPrbsEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061SgmiiSerdesPrbsEnginev2);
    }

static AtPrbsEngine ObjectInit(AtPrbsEngine self, AtSerdesController serdesController)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210061SgmiiSerdesPrbsEngineObjectInit(self, serdesController) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha60210061SgmiiSerdesPrbsEngineV2New(AtSerdesController serdesController)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newEngine, serdesController);
    }

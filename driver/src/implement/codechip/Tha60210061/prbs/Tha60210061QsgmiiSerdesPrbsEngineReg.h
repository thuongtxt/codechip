/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60210061QsgmiiSerdesPrbsEngineReg.h
 * 
 * Created Date: Jun 23, 2016
 *
 * Description : Register for QSGMII diag
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061QSGMIISERDESPRBSENGINEREG_H_
#define _THA60210061QSGMIISERDESPRBSENGINEREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/*------------------------------------------------------------------------------
Reg Name   : QSGMII control global 0
Reg Addr   : 0x00
Reg Formula:
    Where  :
Reg Desc   :
Register to test QSGMII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_control_global1_pen_Base                                                                  0x00
#define cAf6Reg_control_global1_pen                                                                       0x00
#define cAf6Reg_control_global1_pen_WidthVal                                                                32
#define cAf6Reg_control_global1_pen_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: Port enable
BitField Type: R/W
BitField Desc: Port enable for 4 lane
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_control_global1_pen_Port_enable_Bit_Start                                                       0
#define cAf6_control_global1_pen_Port_enable_Bit_End                                                         3
#define cAf6_control_global1_pen_Port_enable_Mask                                                      cBit3_0
#define cAf6_control_global1_pen_Port_enable_Shift                                                           0
#define cAf6_control_global1_pen_Port_enable_MaxVal                                                        0xf
#define cAf6_control_global1_pen_Port_enable_MinVal                                                        0x0
#define cAf6_control_global1_pen_Port_enable_RstVal                                                        0x0

/*--------------------------------------
BitField Name: Force data error
BitField Type: R/W
BitField Desc: Force data error for 4 lane
BitField Bits: [7:4]
--------------------------------------*/
#define cAf6_control_global1_pen_Force_data_error_Bit_Start                                                  4
#define cAf6_control_global1_pen_Force_data_error_Bit_End                                                    7
#define cAf6_control_global1_pen_Force_data_error_Mask                                                 cBit7_4
#define cAf6_control_global1_pen_Force_data_error_Shift                                                      4
#define cAf6_control_global1_pen_Force_data_error_MaxVal                                                   0xf
#define cAf6_control_global1_pen_Force_data_error_MinVal                                                   0x0
#define cAf6_control_global1_pen_Force_data_error_RstVal                                                   0x0

/*--------------------------------------
BitField Name: Force FCS error
BitField Type: R/W
BitField Desc: Force FCS error for 4 lane
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_control_global1_pen_Force_FCS_error_Bit_Start                                                   8
#define cAf6_control_global1_pen_Force_FCS_error_Bit_End                                                    11
#define cAf6_control_global1_pen_Force_FCS_error_Mask                                                 cBit11_8
#define cAf6_control_global1_pen_Force_FCS_error_Shift                                                       8
#define cAf6_control_global1_pen_Force_FCS_error_MaxVal                                                    0xf
#define cAf6_control_global1_pen_Force_FCS_error_MinVal                                                    0x0
#define cAf6_control_global1_pen_Force_FCS_error_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : QSGMII sticky global 1
Reg Addr   : 0x01
Reg Formula:
    Where  :
Reg Desc   :
Register to test QSGMII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_alarm_pen_Base                                                                            0x01
#define cAf6Reg_alarm_pen                                                                                 0x01
#define cAf6Reg_alarm_pen_WidthVal                                                                          32
#define cAf6Reg_alarm_pen_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: Monitor data error
BitField Type: R/W/C
BitField Desc: Monitor data error for 4 lane
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_alarm_pen_Monitor_data_error_Bit_Start                                                          0
#define cAf6_alarm_pen_Monitor_data_error_Bit_End                                                            3
#define cAf6_alarm_pen_Monitor_data_error_Mask                                                         cBit3_0
#define cAf6_alarm_pen_Monitor_data_error_Shift                                                              0
#define cAf6_alarm_pen_Monitor_data_error_MaxVal                                                           0xf
#define cAf6_alarm_pen_Monitor_data_error_MinVal                                                           0x0
#define cAf6_alarm_pen_Monitor_data_error_RstVal                                                           0x0

/*--------------------------------------
BitField Name: Monitor package len error
BitField Type: R/W/C
BitField Desc: Monitor package len error for 4 lane
BitField Bits: [7:4]
--------------------------------------*/
#define cAf6_alarm_pen_Monitor_package_len_error_Bit_Start                                                   4
#define cAf6_alarm_pen_Monitor_package_len_error_Bit_End                                                     7
#define cAf6_alarm_pen_Monitor_package_len_error_Mask                                                  cBit7_4
#define cAf6_alarm_pen_Monitor_package_len_error_Shift                                                       4
#define cAf6_alarm_pen_Monitor_package_len_error_MaxVal                                                    0xf
#define cAf6_alarm_pen_Monitor_package_len_error_MinVal                                                    0x0
#define cAf6_alarm_pen_Monitor_package_len_error_RstVal                                                    0x0

/*--------------------------------------
BitField Name: Monitor Rx FCS error
BitField Type: R/W/C
BitField Desc: Monitor Rx FCS error for 4 lane
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_alarm_pen_Monitor_Rx_FCS_error_Bit_Start                                                        8
#define cAf6_alarm_pen_Monitor_Rx_FCS_error_Bit_End                                                         11
#define cAf6_alarm_pen_Monitor_Rx_FCS_error_Mask                                                      cBit11_8
#define cAf6_alarm_pen_Monitor_Rx_FCS_error_Shift                                                            8
#define cAf6_alarm_pen_Monitor_Rx_FCS_error_MaxVal                                                         0xf
#define cAf6_alarm_pen_Monitor_Rx_FCS_error_MinVal                                                         0x0
#define cAf6_alarm_pen_Monitor_Rx_FCS_error_RstVal                                                         0x0

/*--------------------------------------
BitField Name: Monitor Rx Data sync
BitField Type: R/W/C
BitField Desc: Monitor Rx Data sync for 4 lane
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_alarm_pen_Monitor_Rx_Data_sync_Bit_Start                                                       12
#define cAf6_alarm_pen_Monitor_Rx_Data_sync_Bit_End                                                         15
#define cAf6_alarm_pen_Monitor_Rx_Data_sync_Mask                                                     cBit15_12
#define cAf6_alarm_pen_Monitor_Rx_Data_sync_Shift                                                           12
#define cAf6_alarm_pen_Monitor_Rx_Data_sync_MaxVal                                                         0xf
#define cAf6_alarm_pen_Monitor_Rx_Data_sync_MinVal                                                         0x0
#define cAf6_alarm_pen_Monitor_Rx_Data_sync_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : QSGMII control global 2
Reg Addr   : 0x02
Reg Formula:
    Where  :
Reg Desc   :
Register to test QSGMII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_control_global2_pen_Base                                                                  0x02
#define cAf6Reg_control_global2_pen                                                                       0x02
#define cAf6Reg_control_global2_pen_WidthVal                                                                32
#define cAf6Reg_control_global2_pen_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: Config data mode
BitField Type: R/W
BitField Desc: Config data for test 0/1/2/3 -> Fix/Seq/Inc/PRBS
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_control_global2_pen_Config_data_mode_Bit_Start                                                  0
#define cAf6_control_global2_pen_Config_data_mode_Bit_End                                                    1
#define cAf6_control_global2_pen_Config_data_mode_Mask                                                 cBit1_0
#define cAf6_control_global2_pen_Config_data_mode_Shift                                                      0
#define cAf6_control_global2_pen_Config_data_mode_MaxVal                                                   0x3
#define cAf6_control_global2_pen_Config_data_mode_MinVal                                                   0x0
#define cAf6_control_global2_pen_Config_data_mode_RstVal                                                   0x0

/*--------------------------------------
BitField Name: Config fix data
BitField Type: R/W
BitField Desc: Config fix data for mode fix data
BitField Bits: [11:4]
--------------------------------------*/
#define cAf6_control_global2_pen_Config_fix_data_Bit_Start                                                   4
#define cAf6_control_global2_pen_Config_fix_data_Bit_End                                                    11
#define cAf6_control_global2_pen_Config_fix_data_Mask                                                 cBit11_4
#define cAf6_control_global2_pen_Config_fix_data_Shift                                                       4
#define cAf6_control_global2_pen_Config_fix_data_MaxVal                                                   0xff
#define cAf6_control_global2_pen_Config_fix_data_MinVal                                                    0x0
#define cAf6_control_global2_pen_Config_fix_data_RstVal                                                    0x0

/*--------------------------------------
BitField Name: Config data length
BitField Type: R/W
BitField Desc: Config payload size for ethernet framer
BitField Bits: [31:20]
--------------------------------------*/
#define cAf6_control_global2_pen_Config_data_length_Bit_Start                                               20
#define cAf6_control_global2_pen_Config_data_length_Bit_End                                                 31
#define cAf6_control_global2_pen_Config_data_length_Mask                                             cBit31_20
#define cAf6_control_global2_pen_Config_data_length_Shift                                                   20
#define cAf6_control_global2_pen_Config_data_length_MaxVal                                               0xfff
#define cAf6_control_global2_pen_Config_data_length_MinVal                                                 0x0
#define cAf6_control_global2_pen_Config_data_length_RstVal                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : QSGMII control global 3
Reg Addr   : 0x03
Reg Formula:
    Where  :
Reg Desc   :
Register to test QSGMII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_control_global3_pen_Base                                                                  0x03
#define cAf6Reg_control_global3_pen                                                                       0x03
#define cAf6Reg_control_global3_pen_WidthVal                                                                32
#define cAf6Reg_control_global3_pen_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: Config number inter-package gap
BitField Type: R/W
BitField Desc: Config number inter-package gap byte 0/1/2/3->12/24/36/48 byte
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_control_global3_pen_Config_number_inter_package_gap_Bit_Start                                       0
#define cAf6_control_global3_pen_Config_number_inter_package_gap_Bit_End                                       1
#define cAf6_control_global3_pen_Config_number_inter_package_gap_Mask                                  cBit1_0
#define cAf6_control_global3_pen_Config_number_inter_package_gap_Shift                                       0
#define cAf6_control_global3_pen_Config_number_inter_package_gap_MaxVal                                     0x3
#define cAf6_control_global3_pen_Config_number_inter_package_gap_MinVal                                     0x0
#define cAf6_control_global3_pen_Config_number_inter_package_gap_RstVal                                     0x0

/*--------------------------------------
BitField Name: Config invert FCS data
BitField Type: R/W
BitField Desc: Config enable invert FCS data 0/1 -> Dis/En
BitField Bits: [4]
--------------------------------------*/
#define cAf6_control_global3_pen_Config_invert_FCS_data_Bit_Start                                            4
#define cAf6_control_global3_pen_Config_invert_FCS_data_Bit_End                                              4
#define cAf6_control_global3_pen_Config_invert_FCS_data_Mask                                             cBit4
#define cAf6_control_global3_pen_Config_invert_FCS_data_Shift                                                4
#define cAf6_control_global3_pen_Config_invert_FCS_data_MaxVal                                             0x1
#define cAf6_control_global3_pen_Config_invert_FCS_data_MinVal                                             0x0
#define cAf6_control_global3_pen_Config_invert_FCS_data_RstVal                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : QSGMII control ethernet MAC 0
Reg Addr   : 0x10
Reg Formula:
    Where  :
Reg Desc   :
Register to test QSGMII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_control_dest_mac0_pen_Base                                                                0x10
#define cAf6Reg_control_dest_mac0_pen                                                                     0x10
#define cAf6Reg_control_dest_mac0_pen_WidthVal                                                              32
#define cAf6Reg_control_dest_mac0_pen_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: Config MAC-0
BitField Type: R/W
BitField Desc: Config Destination MAC
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_control_dest_mac0_pen_Config_MAC_0_Bit_Start                                                    0
#define cAf6_control_dest_mac0_pen_Config_MAC_0_Bit_End                                                     31
#define cAf6_control_dest_mac0_pen_Config_MAC_0_Mask                                                  cBit31_0
#define cAf6_control_dest_mac0_pen_Config_MAC_0_Shift                                                        0
#define cAf6_control_dest_mac0_pen_Config_MAC_0_MaxVal                                              0xffffffff
#define cAf6_control_dest_mac0_pen_Config_MAC_0_MinVal                                                     0x0
#define cAf6_control_dest_mac0_pen_Config_MAC_0_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : QSGMII control ethernet MAC 1
Reg Addr   : 0x11
Reg Formula:
    Where  :
Reg Desc   :
Register to test QSGMII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_control_dest_mac1_pen_Base                                                                0x11
#define cAf6Reg_control_dest_mac1_pen                                                                     0x11
#define cAf6Reg_control_dest_mac1_pen_WidthVal                                                              32
#define cAf6Reg_control_dest_mac1_pen_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: Config MAC-1
BitField Type: R/W
BitField Desc: Config Destination MAC
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_control_dest_mac1_pen_Config_MAC_1_Bit_Start                                                    0
#define cAf6_control_dest_mac1_pen_Config_MAC_1_Bit_End                                                     15
#define cAf6_control_dest_mac1_pen_Config_MAC_1_Mask                                                  cBit15_0
#define cAf6_control_dest_mac1_pen_Config_MAC_1_Shift                                                        0
#define cAf6_control_dest_mac1_pen_Config_MAC_1_MaxVal                                                  0xffff
#define cAf6_control_dest_mac1_pen_Config_MAC_1_MinVal                                                     0x0
#define cAf6_control_dest_mac1_pen_Config_MAC_1_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : QSGMII control ethernet MAC 2
Reg Addr   : 0x12
Reg Formula:
    Where  :
Reg Desc   :
Register to test QSGMII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_control_src_mac0_pen_Base                                                                 0x12
#define cAf6Reg_control_src_mac0_pen                                                                      0x12
#define cAf6Reg_control_src_mac0_pen_WidthVal                                                               32
#define cAf6Reg_control_src_mac0_pen_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: Config MAC-2
BitField Type: R/W
BitField Desc: Config Source MAC
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_control_src_mac0_pen_Config_MAC_2_Bit_Start                                                     0
#define cAf6_control_src_mac0_pen_Config_MAC_2_Bit_End                                                      31
#define cAf6_control_src_mac0_pen_Config_MAC_2_Mask                                                   cBit31_0
#define cAf6_control_src_mac0_pen_Config_MAC_2_Shift                                                         0
#define cAf6_control_src_mac0_pen_Config_MAC_2_MaxVal                                               0xffffffff
#define cAf6_control_src_mac0_pen_Config_MAC_2_MinVal                                                      0x0
#define cAf6_control_src_mac0_pen_Config_MAC_2_RstVal                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : QSGMII control ethernet MAC 3
Reg Addr   : 0x13
Reg Formula:
    Where  :
Reg Desc   :
Register to test QSGMII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_control_src_mac1_pen_Base                                                                 0x13
#define cAf6Reg_control_src_mac1_pen                                                                      0x13
#define cAf6Reg_control_src_mac1_pen_WidthVal                                                               32
#define cAf6Reg_control_src_mac1_pen_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: Config MAC-3
BitField Type: R/W
BitField Desc: Config Source MAC
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_control_src_mac1_pen_Config_MAC_3_Bit_Start                                                     0
#define cAf6_control_src_mac1_pen_Config_MAC_3_Bit_End                                                      15
#define cAf6_control_src_mac1_pen_Config_MAC_3_Mask                                                   cBit15_0
#define cAf6_control_src_mac1_pen_Config_MAC_3_Shift                                                         0
#define cAf6_control_src_mac1_pen_Config_MAC_3_MaxVal                                                   0xffff
#define cAf6_control_src_mac1_pen_Config_MAC_3_MinVal                                                      0x0
#define cAf6_control_src_mac1_pen_Config_MAC_3_RstVal                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : QSGMII control ethernet MAC 4
Reg Addr   : 0x14
Reg Formula:
    Where  :
Reg Desc   :
Register to test QSGMII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_control_eth_type_Base                                                                     0x14
#define cAf6Reg_control_eth_type                                                                          0x14
#define cAf6Reg_control_eth_type_WidthVal                                                                   32
#define cAf6Reg_control_eth_type_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: Config MAC-4
BitField Type: R/W
BitField Desc: Config Ethernet type
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_control_eth_type_Config_MAC_4_Bit_Start                                                         0
#define cAf6_control_eth_type_Config_MAC_4_Bit_End                                                          15
#define cAf6_control_eth_type_Config_MAC_4_Mask                                                       cBit15_0
#define cAf6_control_eth_type_Config_MAC_4_Shift                                                             0
#define cAf6_control_eth_type_Config_MAC_4_MaxVal                                                       0xffff
#define cAf6_control_eth_type_Config_MAC_4_MinVal                                                          0x0
#define cAf6_control_eth_type_Config_MAC_4_RstVal                                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : QSGMII Rx/Tx counter channel 0
Reg Addr   : 0x21
Reg Formula:
    Where  :
Reg Desc   :
Register to test QSGMII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_counter_channel0_Base                                                                     0x21
#define cAf6Reg_counter_channel0                                                                          0x21
#define cAf6Reg_counter_channel0_WidthVal                                                                   32
#define cAf6Reg_counter_channel0_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: Rx counter
BitField Type: R/W
BitField Desc: Rx counter channel 0
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_counter_channel0_Rx_counter_Bit_Start                                                          16
#define cAf6_counter_channel0_Rx_counter_Bit_End                                                            31
#define cAf6_counter_channel0_Rx_counter_Mask                                                        cBit31_16
#define cAf6_counter_channel0_Rx_counter_Shift                                                              16
#define cAf6_counter_channel0_Rx_counter_MaxVal                                                         0xffff
#define cAf6_counter_channel0_Rx_counter_MinVal                                                            0x0
#define cAf6_counter_channel0_Rx_counter_RstVal                                                            0x0

/*--------------------------------------
BitField Name: Tx counter
BitField Type: R/W
BitField Desc: Tx counter channel 0
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_counter_channel0_Tx_counter_Bit_Start                                                           0
#define cAf6_counter_channel0_Tx_counter_Bit_End                                                            15
#define cAf6_counter_channel0_Tx_counter_Mask                                                         cBit15_0
#define cAf6_counter_channel0_Tx_counter_Shift                                                               0
#define cAf6_counter_channel0_Tx_counter_MaxVal                                                         0xffff
#define cAf6_counter_channel0_Tx_counter_MinVal                                                            0x0
#define cAf6_counter_channel0_Tx_counter_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : QSGMII Rx/Tx counter channel 1
Reg Addr   : 0x22
Reg Formula:
    Where  :
Reg Desc   :
Register to test QSGMII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_counter_channel1_Base                                                                     0x22
#define cAf6Reg_counter_channel1                                                                          0x22
#define cAf6Reg_counter_channel1_WidthVal                                                                   32
#define cAf6Reg_counter_channel1_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: Rx counter
BitField Type: R/W
BitField Desc: Rx counter channel 1
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_counter_channel1_Rx_counter_Bit_Start                                                          16
#define cAf6_counter_channel1_Rx_counter_Bit_End                                                            31
#define cAf6_counter_channel1_Rx_counter_Mask                                                        cBit31_16
#define cAf6_counter_channel1_Rx_counter_Shift                                                              16
#define cAf6_counter_channel1_Rx_counter_MaxVal                                                         0xffff
#define cAf6_counter_channel1_Rx_counter_MinVal                                                            0x0
#define cAf6_counter_channel1_Rx_counter_RstVal                                                            0x0

/*--------------------------------------
BitField Name: Tx counter
BitField Type: R/W
BitField Desc: Tx counter channel 1
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_counter_channel1_Tx_counter_Bit_Start                                                           0
#define cAf6_counter_channel1_Tx_counter_Bit_End                                                            15
#define cAf6_counter_channel1_Tx_counter_Mask                                                         cBit15_0
#define cAf6_counter_channel1_Tx_counter_Shift                                                               0
#define cAf6_counter_channel1_Tx_counter_MaxVal                                                         0xffff
#define cAf6_counter_channel1_Tx_counter_MinVal                                                            0x0
#define cAf6_counter_channel1_Tx_counter_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : QSGMII Rx/Tx counter channel 2
Reg Addr   : 0x23
Reg Formula:
    Where  :
Reg Desc   :
Register to test QSGMII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_counter_channel2_Base                                                                     0x23
#define cAf6Reg_counter_channel2                                                                          0x23
#define cAf6Reg_counter_channel2_WidthVal                                                                   32
#define cAf6Reg_counter_channel2_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: Rx counter
BitField Type: R/W
BitField Desc: Rx counter channel 2
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_counter_channel2_Rx_counter_Bit_Start                                                          16
#define cAf6_counter_channel2_Rx_counter_Bit_End                                                            31
#define cAf6_counter_channel2_Rx_counter_Mask                                                        cBit31_16
#define cAf6_counter_channel2_Rx_counter_Shift                                                              16
#define cAf6_counter_channel2_Rx_counter_MaxVal                                                         0xffff
#define cAf6_counter_channel2_Rx_counter_MinVal                                                            0x0
#define cAf6_counter_channel2_Rx_counter_RstVal                                                            0x0

/*--------------------------------------
BitField Name: Tx counter
BitField Type: R/W
BitField Desc: Tx counter channel 2
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_counter_channel2_Tx_counter_Bit_Start                                                           0
#define cAf6_counter_channel2_Tx_counter_Bit_End                                                            15
#define cAf6_counter_channel2_Tx_counter_Mask                                                         cBit15_0
#define cAf6_counter_channel2_Tx_counter_Shift                                                               0
#define cAf6_counter_channel2_Tx_counter_MaxVal                                                         0xffff
#define cAf6_counter_channel2_Tx_counter_MinVal                                                            0x0
#define cAf6_counter_channel2_Tx_counter_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : QSGMII Rx/Tx counter channel 3
Reg Addr   : 0x24
Reg Formula:
    Where  :
Reg Desc   :
Register to test QSGMII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_counter_channel3_Base                                                                     0x24
#define cAf6Reg_counter_channel3                                                                          0x24
#define cAf6Reg_counter_channel3_WidthVal                                                                   32
#define cAf6Reg_counter_channel3_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: Rx counter
BitField Type: R/W
BitField Desc: Rx counter channel 3
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_counter_channel3_Rx_counter_Bit_Start                                                          16
#define cAf6_counter_channel3_Rx_counter_Bit_End                                                            31
#define cAf6_counter_channel3_Rx_counter_Mask                                                        cBit31_16
#define cAf6_counter_channel3_Rx_counter_Shift                                                              16
#define cAf6_counter_channel3_Rx_counter_MaxVal                                                         0xffff
#define cAf6_counter_channel3_Rx_counter_MinVal                                                            0x0
#define cAf6_counter_channel3_Rx_counter_RstVal                                                            0x0

/*--------------------------------------
BitField Name: Tx counter
BitField Type: R/W
BitField Desc: Tx counter channel 3
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_counter_channel3_Tx_counter_Bit_Start                                                           0
#define cAf6_counter_channel3_Tx_counter_Bit_End                                                            15
#define cAf6_counter_channel3_Tx_counter_Mask                                                         cBit15_0
#define cAf6_counter_channel3_Tx_counter_Shift                                                               0
#define cAf6_counter_channel3_Tx_counter_MaxVal                                                         0xffff
#define cAf6_counter_channel3_Tx_counter_MinVal                                                            0x0
#define cAf6_counter_channel3_Tx_counter_RstVal                                                            0x0

/*------------------------------------------------------------------------------
Reg Name   : QSGMII Tx counter clear channel 0
Reg Addr   : 0x31
Reg Formula:
    Where  :
Reg Desc   :
Register to test QSGMII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_tx_counter_clear_channel0_Base                                                                     0x31
#define cAf6Reg_tx_counter_clear_channel0                                                                          0x31
#define cAf6Reg_tx_counter_clear_channel0_WidthVal                                                                   32
#define cAf6Reg_tx_counter_clear_channel0_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: Tx counter clear
BitField Type: R/W
BitField Desc: Tx counter clear channel 0
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_tx_counter_clear_channel0_Bit_Start                                                           0
#define cAf6_tx_counter_clear_channel0_Bit_End                                                            31
#define cAf6_tx_counter_clear_channel0_Mask                                                         cBit31_0
#define cAf6_tx_counter_clear_channel0_Shift                                                               0
#define cAf6_tx_counter_clear_channel0_MaxVal                                                     0xffffffff
#define cAf6_tx_counter_clear_channel0_MinVal                                                            0x0
#define cAf6_tx_counter_clear_channel0_RstVal                                                            0x0

/*------------------------------------------------------------------------------
Reg Name   : QSGMII Rx counter clear channel 0
Reg Addr   : 0x41
Reg Formula:
    Where  :
Reg Desc   :
Register to test QSGMII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_rx_counter_clear_channel0_Base                                                                     0x41
#define cAf6Reg_rx_counter_clear_channel0                                                                          0x41
#define cAf6Reg_rx_counter_clear_channel0_WidthVal                                                                   32
#define cAf6Reg_rx_counter_clear_channel0_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: Rx counter clear
BitField Type: R/W
BitField Desc: Rx counter clear channel 0
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rx_counter_clear_channel0_Bit_Start                                                          0
#define cAf6_rx_counter_clear_channel0_Bit_End                                                           31
#define cAf6_rx_counter_clear_channel0_Mask                                                        cBit31_0
#define cAf6_rx_counter_clear_channel0_Shift                                                              0
#define cAf6_rx_counter_clear_channel0_MaxVal                                                    0xffffffff
#define cAf6_rx_counter_clear_channel0_MinVal                                                           0x0
#define cAf6_rx_counter_clear_channel0_RstVal                                                           0x0

/*------------------------------------------------------------------------------
Reg Name   : QSGMII Tx counter clear channel 1
Reg Addr   : 0x32
Reg Formula:
    Where  :
Reg Desc   :
Register to test QSGMII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_tx_counter_clear_channel1_Base                                                                     0x32
#define cAf6Reg_tx_counter_clear_channel1                                                                          0x32
#define cAf6Reg_tx_counter_clear_channel1_WidthVal                                                                   32
#define cAf6Reg_tx_counter_clear_channel1_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: Tx counter clear
BitField Type: R/W
BitField Desc: Tx counter clear channel 1
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_tx_counter_clear_channel1_Bit_Start                                                           0
#define cAf6_tx_counter_clear_channel1_Bit_End                                                            31
#define cAf6_tx_counter_clear_channel1_Mask                                                         cBit31_0
#define cAf6_tx_counter_clear_channel1_Shift                                                               0
#define cAf6_tx_counter_clear_channel1_MaxVal                                                     0xffffffff
#define cAf6_tx_counter_clear_channel1_MinVal                                                            0x0
#define cAf6_tx_counter_clear_channel1_RstVal                                                            0x0

/*------------------------------------------------------------------------------
Reg Name   : QSGMII Rx counter clear channel 1
Reg Addr   : 0x42
Reg Formula:
    Where  :
Reg Desc   :
Register to test QSGMII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_rx_counter_clear_channel1_Base                                                                     0x42
#define cAf6Reg_rx_counter_clear_channel1                                                                          0x42
#define cAf6Reg_rx_counter_clear_channel1_WidthVal                                                                   32
#define cAf6Reg_rx_counter_clear_channel1_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: Rx counter clear
BitField Type: R/W
BitField Desc: Rx counter clear channel 1
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rx_counter_clear_channel1_Bit_Start                                                          0
#define cAf6_rx_counter_clear_channel1_Bit_End                                                           31
#define cAf6_rx_counter_clear_channel1_Mask                                                        cBit31_0
#define cAf6_rx_counter_clear_channel1_Shift                                                              0
#define cAf6_rx_counter_clear_channel1_MaxVal                                                    0xffffffff
#define cAf6_rx_counter_clear_channel1_MinVal                                                           0x0
#define cAf6_rx_counter_clear_channel1_RstVal                                                           0x0

/*------------------------------------------------------------------------------
Reg Name   : QSGMII Tx counter clear channel 2
Reg Addr   : 0x33
Reg Formula:
    Where  :
Reg Desc   :
Register to test QSGMII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_tx_counter_clear_channel2_Base                                                                     0x33
#define cAf6Reg_tx_counter_clear_channel2                                                                          0x33
#define cAf6Reg_tx_counter_clear_channel2_WidthVal                                                                   32
#define cAf6Reg_tx_counter_clear_channel2_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: Tx counter clear
BitField Type: R/W
BitField Desc: Tx counter clear channel 2
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_tx_counter_clear_channel2_Bit_Start                                                           0
#define cAf6_tx_counter_clear_channel2_Bit_End                                                            31
#define cAf6_tx_counter_clear_channel2_Mask                                                         cBit31_0
#define cAf6_tx_counter_clear_channel2_Shift                                                               0
#define cAf6_tx_counter_clear_channel2_MaxVal                                                     0xffffffff
#define cAf6_tx_counter_clear_channel2_MinVal                                                            0x0
#define cAf6_tx_counter_clear_channel2_RstVal                                                            0x0

/*------------------------------------------------------------------------------
Reg Name   : QSGMII Rx counter clear channel 2
Reg Addr   : 0x43
Reg Formula:
    Where  :
Reg Desc   :
Register to test QSGMII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_rx_counter_clear_channel2_Base                                                                     0x43
#define cAf6Reg_rx_counter_clear_channel2                                                                          0x43
#define cAf6Reg_rx_counter_clear_channel2_WidthVal                                                                   32
#define cAf6Reg_rx_counter_clear_channel2_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: Rx counter clear
BitField Type: R/W
BitField Desc: Rx counter clear channel 2
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rx_counter_clear_channel2_Bit_Start                                                          0
#define cAf6_rx_counter_clear_channel2_Bit_End                                                           31
#define cAf6_rx_counter_clear_channel2_Mask                                                        cBit31_0
#define cAf6_rx_counter_clear_channel2_Shift                                                              0
#define cAf6_rx_counter_clear_channel2_MaxVal                                                    0xffffffff
#define cAf6_rx_counter_clear_channel2_MinVal                                                           0x0
#define cAf6_rx_counter_clear_channel2_RstVal                                                           0x0

/*------------------------------------------------------------------------------
Reg Name   : QSGMII Tx counter clear channel 3
Reg Addr   : 0x34
Reg Formula:
    Where  :
Reg Desc   :
Register to test QSGMII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_tx_counter_clear_channel3_Base                                                                     0x34
#define cAf6Reg_tx_counter_clear_channel3                                                                          0x34
#define cAf6Reg_tx_counter_clear_channel3_WidthVal                                                                   32
#define cAf6Reg_tx_counter_clear_channel3_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: Tx counter clear
BitField Type: R/W
BitField Desc: Tx counter clear channel 3
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_tx_counter_clear_channel3_Bit_Start                                                           0
#define cAf6_tx_counter_clear_channel3_Bit_End                                                            31
#define cAf6_tx_counter_clear_channel3_Mask                                                         cBit31_0
#define cAf6_tx_counter_clear_channel3_Shift                                                               0
#define cAf6_tx_counter_clear_channel3_MaxVal                                                     0xffffffff
#define cAf6_tx_counter_clear_channel3_MinVal                                                            0x0
#define cAf6_tx_counter_clear_channel3_RstVal                                                            0x0

/*------------------------------------------------------------------------------
Reg Name   : QSGMII Rx counter clear channel 3
Reg Addr   : 0x44
Reg Formula:
    Where  :
Reg Desc   :
Register to test QSGMII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_rx_counter_clear_channel3_Base                                                                     0x44
#define cAf6Reg_rx_counter_clear_channel3                                                                          0x44
#define cAf6Reg_rx_counter_clear_channel3_WidthVal                                                                   32
#define cAf6Reg_rx_counter_clear_channel3_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: Rx counter clear
BitField Type: R/W
BitField Desc: Rx counter clear channel 3
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rx_counter_clear_channel3_Bit_Start                                                          0
#define cAf6_rx_counter_clear_channel3_Bit_End                                                           31
#define cAf6_rx_counter_clear_channel3_Mask                                                        cBit31_0
#define cAf6_rx_counter_clear_channel3_Shift                                                              0
#define cAf6_rx_counter_clear_channel3_MaxVal                                                    0xffffffff
#define cAf6_rx_counter_clear_channel3_MinVal                                                           0x0
#define cAf6_rx_counter_clear_channel3_RstVal                                                           0x0


#ifdef __cplusplus
}
#endif
#endif /* _THA60210061QSGMIISERDESPRBSENGINEREG_H_ */


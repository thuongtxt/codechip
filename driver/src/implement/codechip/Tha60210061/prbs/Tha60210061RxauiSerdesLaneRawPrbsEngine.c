/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60210061RxauiSerdesLaneRawPrbsEngine.c
 *
 * Created Date: Mar 23, 2017
 *
 * Description : Tha60210061 RXAUI SERDES Lande Raw Prbs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../physical/Tha60210061SerdesInternal.h"
#include "../physical/Tha60210061SerdesManager.h"
#include "Tha60210061ModulePrbs.h"
#include "Tha60210061SerdesRawPrbsEngineReg.h"
#include "Tha60210061SerdesPrbsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210061SgmiiSerdesRawPrbsEngine*)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061RxauiSerdesLaneRawPrbsEngine
    {
    tTha60210061GthSerdesRawPrbsEngine super;
    }tTha60210061RxauiSerdesLaneRawPrbsEngine;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60210061SgmiiSerdesRawPrbsEngineMethods m_Tha60210061SgmiiSerdesRawPrbsEngineOverride;

/* Save super implementation */
static const tTha60210061SgmiiSerdesRawPrbsEngineMethods *m_Tha60210061SgmiiSerdesRawPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 PortSerdesId(Tha60210061SgmiiSerdesRawPrbsEngine self)
    {
    AtSerdesController parentSerdes = AtSerdesControllerParentGet(mThis(self)->serdesController);
    uint32 hssiPortId = AtSerdesControllerIdGet(parentSerdes);

    return hssiPortId;
    }

static uint32 HssiSerdesId(Tha60210061SgmiiSerdesRawPrbsEngine self)
    {
    uint32 serdesId = AtPrbsEngineIdGet((AtPrbsEngine)self);
    return serdesId;
    }

static void OverrideTha60210061SgmiiSerdesRawPrbsEngine(AtPrbsEngine self)
    {
    Tha60210061SgmiiSerdesRawPrbsEngine serdes = (Tha60210061SgmiiSerdesRawPrbsEngine)self;
    if (!m_methodsInit)
       {
       AtOsal osal = AtSharedDriverOsalGet();
       m_Tha60210061SgmiiSerdesRawPrbsEngineMethods = mMethodsGet(serdes);
       mMethodsGet(osal)->MemCpy(osal, &m_Tha60210061SgmiiSerdesRawPrbsEngineOverride, mMethodsGet(serdes), sizeof(m_Tha60210061SgmiiSerdesRawPrbsEngineOverride));

       mMethodOverride(m_Tha60210061SgmiiSerdesRawPrbsEngineOverride, PortSerdesId);
       mMethodOverride(m_Tha60210061SgmiiSerdesRawPrbsEngineOverride, HssiSerdesId);
       }

   mMethodsSet(serdes, &m_Tha60210061SgmiiSerdesRawPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideTha60210061SgmiiSerdesRawPrbsEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061RxauiSerdesLaneRawPrbsEngine);
    }

static AtPrbsEngine ObjectInit(AtPrbsEngine self, AtSerdesController serdesController)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210061GthSerdesRawPrbsEngineObjectInit(self, serdesController) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha60210061RxauiSerdesLaneRawPrbsEngineNew(AtSerdesController serdesController)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine prbsEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (prbsEngine == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(prbsEngine, serdesController);
    }

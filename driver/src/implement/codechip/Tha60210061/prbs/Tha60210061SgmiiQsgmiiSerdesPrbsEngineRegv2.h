/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS register
 * 
 * File        : Tha60210061SgmiiQsgmiiSerdesPrbsEngineRegv2.h
 * 
 * Created Date: Jul 17, 2016
 *
 * Description : Register for QSGMII diag (v2)
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061SGMIIQSGMIISERDESPRBSENGINEREGV2_H_
#define _THA60210061SGMIIQSGMIISERDESPRBSENGINEREGV2_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*------------------------------------------------------------------------------
Reg Name   : SGMII/QSGMII Channels Enable.
Reg Addr   : 0x0
Reg Formula: Channel_ID+0x0
    Where  :
           + $Channel_ID: 0x00 -> Channel-0 0x10 -> Channel-1 0x20 -> Channel-2 0x30 -> Channel-3
Reg Desc   :
Register to test SGMII/QSGMII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_control_channels_pen_Base                                                                  0x0
#define cAf6Reg_control_channels_pen(ChannelID)                                            ((ChannelID)+0x0UL)
#define cAf6Reg_control_channels_pen_WidthVal                                                               32
#define cAf6Reg_control_channels_pen_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: Enable
BitField Type: R/W
BitField Desc: Test enable: 0/1 -> Dis/En
BitField Bits: [0]
--------------------------------------*/
#define cAf6_control_channels_pen_Enable_Bit_Start                                                           0
#define cAf6_control_channels_pen_Enable_Bit_End                                                             0
#define cAf6_control_channels_pen_Enable_Mask                                                            cBit0
#define cAf6_control_channels_pen_Enable_Shift                                                               0
#define cAf6_control_channels_pen_Enable_MaxVal                                                            0x1
#define cAf6_control_channels_pen_Enable_MinVal                                                            0x0
#define cAf6_control_channels_pen_Enable_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : SGMII/QSGMII Burst test enable gen package.
Reg Addr   : 0xF
Reg Formula: Channel_ID+0xF
    Where  :
           + $Channel_ID: 0x00 -> Channel-0 0x10 -> Channel-1 0x20 -> Channel-2 0x30 -> Channel-3
Reg Desc   :
Register to test SGMII/QSGMII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_control_burst_pen_Base                                                                     0xF
#define cAf6Reg_control_burst_pen(ChannelID)                                               ((ChannelID)+0xFUL)
#define cAf6Reg_control_burst_pen_WidthVal                                                                  32
#define cAf6Reg_control_burst_pen_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: Burst_gen_package_number
BitField Type: R/W
BitField Desc: Burst gen package number, must configure burst number > 0 to
generator package, hardware will clear number package configure when finish.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_control_burst_pen_Burst_gen_package_number_Bit_Start                                            0
#define cAf6_control_burst_pen_Burst_gen_package_number_Bit_End                                             31
#define cAf6_control_burst_pen_Burst_gen_package_number_Mask                                          cBit31_0
#define cAf6_control_burst_pen_Burst_gen_package_number_Shift                                                0
#define cAf6_control_burst_pen_Burst_gen_package_number_MaxVal                                      0xffffffff
#define cAf6_control_burst_pen_Burst_gen_package_number_MinVal                                             0x0
#define cAf6_control_burst_pen_Burst_gen_package_number_RstVal                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : SGMII/QSGMII Channels Sticky alarm.
Reg Addr   : 0x1
Reg Formula: Channel_ID+0x1
    Where  :
           + $Channel_ID: 0x00 -> Channel-0 0x10 -> Channel-1 0x20 -> Channel-2 0x30 -> Channel-3
Reg Desc   :
Register to test SGMII/QSGMII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_Alarm_sticky_Base                                                                          0x1
#define cAf6Reg_Alarm_sticky(ChannelID)                                                    ((ChannelID)+0x1UL)
#define cAf6Reg_Alarm_sticky_WidthVal                                                                       32
#define cAf6Reg_Alarm_sticky_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: Mon_package_error
BitField Type: R/W/C
BitField Desc: Monitor package error.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_Alarm_sticky_Mon_package_error_Bit_Start                                                        0
#define cAf6_Alarm_sticky_Mon_package_error_Bit_End                                                          0
#define cAf6_Alarm_sticky_Mon_package_error_Mask                                                         cBit0
#define cAf6_Alarm_sticky_Mon_package_error_Shift                                                            0
#define cAf6_Alarm_sticky_Mon_package_error_MaxVal                                                         0x1
#define cAf6_Alarm_sticky_Mon_package_error_MinVal                                                         0x0
#define cAf6_Alarm_sticky_Mon_package_error_RstVal                                                         0x0

/*--------------------------------------
BitField Name: Mon_package_len_error
BitField Type: R/W/C
BitField Desc: Monitor package len error.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_Alarm_sticky_Mon_package_len_error_Bit_Start                                                    1
#define cAf6_Alarm_sticky_Mon_package_len_error_Bit_End                                                      1
#define cAf6_Alarm_sticky_Mon_package_len_error_Mask                                                     cBit1
#define cAf6_Alarm_sticky_Mon_package_len_error_Shift                                                        1
#define cAf6_Alarm_sticky_Mon_package_len_error_MaxVal                                                     0x1
#define cAf6_Alarm_sticky_Mon_package_len_error_MinVal                                                     0x0
#define cAf6_Alarm_sticky_Mon_package_len_error_RstVal                                                     0x0

/*--------------------------------------
BitField Name: Mon_FCS_error
BitField Type: R/W/C
BitField Desc: Monitor FCS error.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_Alarm_sticky_Mon_FCS_error_Bit_Start                                                            2
#define cAf6_Alarm_sticky_Mon_FCS_error_Bit_End                                                              2
#define cAf6_Alarm_sticky_Mon_FCS_error_Mask                                                             cBit2
#define cAf6_Alarm_sticky_Mon_FCS_error_Shift                                                                2
#define cAf6_Alarm_sticky_Mon_FCS_error_MaxVal                                                             0x1
#define cAf6_Alarm_sticky_Mon_FCS_error_MinVal                                                             0x0
#define cAf6_Alarm_sticky_Mon_FCS_error_RstVal                                                             0x0

/*--------------------------------------
BitField Name: Mon_data_error
BitField Type: R/W/C
BitField Desc: Monitor data error.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_Alarm_sticky_Mon_data_error_Bit_Start                                                           3
#define cAf6_Alarm_sticky_Mon_data_error_Bit_End                                                             3
#define cAf6_Alarm_sticky_Mon_data_error_Mask                                                            cBit3
#define cAf6_Alarm_sticky_Mon_data_error_Shift                                                               3
#define cAf6_Alarm_sticky_Mon_data_error_MaxVal                                                            0x1
#define cAf6_Alarm_sticky_Mon_data_error_MinVal                                                            0x0
#define cAf6_Alarm_sticky_Mon_data_error_RstVal                                                            0x0

/*--------------------------------------
BitField Name: Mon_MAC_header_error
BitField Type: R/W/C
BitField Desc: Monitor MAC header error.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_Alarm_sticky_Mon_MAC_header_error_Bit_Start                                                     4
#define cAf6_Alarm_sticky_Mon_MAC_header_error_Bit_End                                                       4
#define cAf6_Alarm_sticky_Mon_MAC_header_error_Mask                                                      cBit4
#define cAf6_Alarm_sticky_Mon_MAC_header_error_Shift                                                         4
#define cAf6_Alarm_sticky_Mon_MAC_header_error_MaxVal                                                      0x1
#define cAf6_Alarm_sticky_Mon_MAC_header_error_MinVal                                                      0x0
#define cAf6_Alarm_sticky_Mon_MAC_header_error_RstVal                                                      0x0

/*--------------------------------------
BitField Name: Mon_Data_sync
BitField Type: R/W/C
BitField Desc: Monitor Data sync.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_Alarm_sticky_Mon_Data_sync_Bit_Start                                                            5
#define cAf6_Alarm_sticky_Mon_Data_sync_Bit_End                                                              5
#define cAf6_Alarm_sticky_Mon_Data_sync_Mask                                                             cBit5
#define cAf6_Alarm_sticky_Mon_Data_sync_Shift                                                                5
#define cAf6_Alarm_sticky_Mon_Data_sync_MaxVal                                                             0x1
#define cAf6_Alarm_sticky_Mon_Data_sync_MinVal                                                             0x0
#define cAf6_Alarm_sticky_Mon_Data_sync_RstVal                                                             0x0

/*--------------------------------------
BitField Name: Link_status
BitField Type: R/W/C
BitField Desc: Link status.
BitField Bits: [6]
--------------------------------------*/
#define cAf6_Alarm_sticky_Link_status_Bit_Start                                                              6
#define cAf6_Alarm_sticky_Link_status_Bit_End                                                                6
#define cAf6_Alarm_sticky_Link_status_Mask                                                               cBit6
#define cAf6_Alarm_sticky_Link_status_Shift                                                                  6
#define cAf6_Alarm_sticky_Link_status_MaxVal                                                               0x1
#define cAf6_Alarm_sticky_Link_status_MinVal                                                               0x0
#define cAf6_Alarm_sticky_Link_status_RstVal                                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : SGMII/QSGMII Channels Status alarm.
Reg Addr   : 0xE
Reg Formula: Channel_ID+0xE
    Where  :
           + $Channel_ID: 0x00 -> Channel-0 0x10 -> Channel-1 0x20 -> Channel-2 0x30 -> Channel-3
Reg Desc   :
Register to test SGMII/QSGMII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_Alarm_status_Base                                                                          0xE
#define cAf6Reg_Alarm_status(ChannelID)                                                    ((ChannelID)+0xEUL)
#define cAf6Reg_Alarm_status_WidthVal                                                                       32
#define cAf6Reg_Alarm_status_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: Mon_package_error
BitField Type: RO
BitField Desc: Monitor package error.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_Alarm_status_Mon_package_error_Bit_Start                                                        0
#define cAf6_Alarm_status_Mon_package_error_Bit_End                                                          0
#define cAf6_Alarm_status_Mon_package_error_Mask                                                         cBit0
#define cAf6_Alarm_status_Mon_package_error_Shift                                                            0
#define cAf6_Alarm_status_Mon_package_error_MaxVal                                                         0x1
#define cAf6_Alarm_status_Mon_package_error_MinVal                                                         0x0
#define cAf6_Alarm_status_Mon_package_error_RstVal                                                         0x0

/*--------------------------------------
BitField Name: Mon_package_len_error
BitField Type: RO
BitField Desc: Monitor package len error.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_Alarm_status_Mon_package_len_error_Bit_Start                                                    1
#define cAf6_Alarm_status_Mon_package_len_error_Bit_End                                                      1
#define cAf6_Alarm_status_Mon_package_len_error_Mask                                                     cBit1
#define cAf6_Alarm_status_Mon_package_len_error_Shift                                                        1
#define cAf6_Alarm_status_Mon_package_len_error_MaxVal                                                     0x1
#define cAf6_Alarm_status_Mon_package_len_error_MinVal                                                     0x0
#define cAf6_Alarm_status_Mon_package_len_error_RstVal                                                     0x0

/*--------------------------------------
BitField Name: Mon_FCS_error
BitField Type: RO
BitField Desc: Monitor FCS error.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_Alarm_status_Mon_FCS_error_Bit_Start                                                            2
#define cAf6_Alarm_status_Mon_FCS_error_Bit_End                                                              2
#define cAf6_Alarm_status_Mon_FCS_error_Mask                                                             cBit2
#define cAf6_Alarm_status_Mon_FCS_error_Shift                                                                2
#define cAf6_Alarm_status_Mon_FCS_error_MaxVal                                                             0x1
#define cAf6_Alarm_status_Mon_FCS_error_MinVal                                                             0x0
#define cAf6_Alarm_status_Mon_FCS_error_RstVal                                                             0x0

/*--------------------------------------
BitField Name: Mon_data_error
BitField Type: RO
BitField Desc: Monitor data error.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_Alarm_status_Mon_data_error_Bit_Start                                                           3
#define cAf6_Alarm_status_Mon_data_error_Bit_End                                                             3
#define cAf6_Alarm_status_Mon_data_error_Mask                                                            cBit3
#define cAf6_Alarm_status_Mon_data_error_Shift                                                               3
#define cAf6_Alarm_status_Mon_data_error_MaxVal                                                            0x1
#define cAf6_Alarm_status_Mon_data_error_MinVal                                                            0x0
#define cAf6_Alarm_status_Mon_data_error_RstVal                                                            0x0

/*--------------------------------------
BitField Name: Mon_MAC_header_error
BitField Type: RO
BitField Desc: Monitor MAC header error.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_Alarm_status_Mon_MAC_header_error_Bit_Start                                                     4
#define cAf6_Alarm_status_Mon_MAC_header_error_Bit_End                                                       4
#define cAf6_Alarm_status_Mon_MAC_header_error_Mask                                                      cBit4
#define cAf6_Alarm_status_Mon_MAC_header_error_Shift                                                         4
#define cAf6_Alarm_status_Mon_MAC_header_error_MaxVal                                                      0x1
#define cAf6_Alarm_status_Mon_MAC_header_error_MinVal                                                      0x0
#define cAf6_Alarm_status_Mon_MAC_header_error_RstVal                                                      0x0

/*--------------------------------------
BitField Name: Mon_Data_sync
BitField Type: RO
BitField Desc: Monitor Data sync.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_Alarm_status_Mon_Data_sync_Bit_Start                                                            5
#define cAf6_Alarm_status_Mon_Data_sync_Bit_End                                                              5
#define cAf6_Alarm_status_Mon_Data_sync_Mask                                                             cBit5
#define cAf6_Alarm_status_Mon_Data_sync_Shift                                                                5
#define cAf6_Alarm_status_Mon_Data_sync_MaxVal                                                             0x1
#define cAf6_Alarm_status_Mon_Data_sync_MinVal                                                             0x0
#define cAf6_Alarm_status_Mon_Data_sync_RstVal                                                             0x0

/*--------------------------------------
BitField Name: Link_status
BitField Type: R/W/C
BitField Desc: Link status.
BitField Bits: [6]
--------------------------------------*/
#define cAf6_Alarm_status_Link_status_Bit_Start                                                              6
#define cAf6_Alarm_status_Link_status_Bit_End                                                                6
#define cAf6_Alarm_status_Link_status_Mask                                                               cBit6
#define cAf6_Alarm_status_Link_status_Shift                                                                  6
#define cAf6_Alarm_status_Link_status_MaxVal                                                               0x1
#define cAf6_Alarm_status_Link_status_MinVal                                                               0x0
#define cAf6_Alarm_status_Link_status_RstVal                                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : SGMII/QSGMII Channels Test mode control.
Reg Addr   : 0x2
Reg Formula: Channel_ID+0x2
    Where  :
           + $Channel_ID: 0x00 -> Channel-0 0x10 -> Channel-1 0x20 -> Channel-2 0x30 -> Channel-3
Reg Desc   :
Register to test SGMII/QSGMII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_control_channel_test_mode_Base                                                             0x2
#define cAf6Reg_control_channel_test_mode(ChannelID)                                       ((ChannelID)+0x2UL)
#define cAf6Reg_control_channel_test_mode_WidthVal                                                          32
#define cAf6Reg_control_channel_test_mode_WriteMask                                                        0x0

/*--------------------------------------
BitField Name: Loopout_enable
BitField Type: R/W
BitField Desc: FPGA loopout enable: 0/1 -> Dis/En.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_control_channel_test_mode_Loopout_enable_Bit_Start                                              0
#define cAf6_control_channel_test_mode_Loopout_enable_Bit_End                                                0
#define cAf6_control_channel_test_mode_Loopout_enable_Mask                                               cBit0
#define cAf6_control_channel_test_mode_Loopout_enable_Shift                                                  0
#define cAf6_control_channel_test_mode_Loopout_enable_MaxVal                                               0x1
#define cAf6_control_channel_test_mode_Loopout_enable_MinVal                                               0x0
#define cAf6_control_channel_test_mode_Loopout_enable_RstVal                                               0x0

/*--------------------------------------
BitField Name: Mac_header_check_enable
BitField Type: R/W
BitField Desc: Mac header check enable: 0/1 -> Dis/En.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_control_channel_test_mode_Mac_header_check_enable_Bit_Start                                       1
#define cAf6_control_channel_test_mode_Mac_header_check_enable_Bit_End                                       1
#define cAf6_control_channel_test_mode_Mac_header_check_enable_Mask                                      cBit1
#define cAf6_control_channel_test_mode_Mac_header_check_enable_Shift                                         1
#define cAf6_control_channel_test_mode_Mac_header_check_enable_MaxVal                                      0x1
#define cAf6_control_channel_test_mode_Mac_header_check_enable_MinVal                                      0x0
#define cAf6_control_channel_test_mode_Mac_header_check_enable_RstVal                                      0x0

/*--------------------------------------
BitField Name: Configure_test_mode
BitField Type: R/W
BitField Desc: Configure test mode: 0/1 -> Continues/Burst.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_control_channel_test_mode_Configure_test_mode_Bit_Start                                         2
#define cAf6_control_channel_test_mode_Configure_test_mode_Bit_End                                           2
#define cAf6_control_channel_test_mode_Configure_test_mode_Mask                                          cBit2
#define cAf6_control_channel_test_mode_Configure_test_mode_Shift                                             2
#define cAf6_control_channel_test_mode_Configure_test_mode_MaxVal                                          0x1
#define cAf6_control_channel_test_mode_Configure_test_mode_MinVal                                          0x0
#define cAf6_control_channel_test_mode_Configure_test_mode_RstVal                                          0x0

/*--------------------------------------
BitField Name: Invert_FCS_enable
BitField Type: R/W
BitField Desc: Invert FCS enable: 0/1 -> Dis/En.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_control_channel_test_mode_Invert_FCS_enable_Bit_Start                                           3
#define cAf6_control_channel_test_mode_Invert_FCS_enable_Bit_End                                             3
#define cAf6_control_channel_test_mode_Invert_FCS_enable_Mask                                            cBit3
#define cAf6_control_channel_test_mode_Invert_FCS_enable_Shift                                               3
#define cAf6_control_channel_test_mode_Invert_FCS_enable_MaxVal                                            0x1
#define cAf6_control_channel_test_mode_Invert_FCS_enable_MinVal                                            0x0
#define cAf6_control_channel_test_mode_Invert_FCS_enable_RstVal                                            0x1

/*--------------------------------------
BitField Name: Force_error
BitField Type: R/W
BitField Desc: Force error: 0/1/2/3 -> Normal/Normal/Force FCS error/Force
Framer Error.
BitField Bits: [5:4]
--------------------------------------*/
#define cAf6_control_channel_test_mode_Force_error_Bit_Start                                                 4
#define cAf6_control_channel_test_mode_Force_error_Bit_End                                                   5
#define cAf6_control_channel_test_mode_Force_error_Mask                                                cBit5_4
#define cAf6_control_channel_test_mode_Force_error_Shift                                                     4
#define cAf6_control_channel_test_mode_Force_error_MaxVal                                                  0x3
#define cAf6_control_channel_test_mode_Force_error_MinVal                                                  0x0
#define cAf6_control_channel_test_mode_Force_error_RstVal                                                  0x0

/*--------------------------------------
BitField Name: Bandwidth
BitField Type: R/W
BitField Desc: Bandwidth: 0/1/2/3 -> 100%/90%/80%/70%.
BitField Bits: [7:6]
--------------------------------------*/
#define cAf6_control_channel_test_mode_Bandwidth_Bit_Start                                                   6
#define cAf6_control_channel_test_mode_Bandwidth_Bit_End                                                     7
#define cAf6_control_channel_test_mode_Bandwidth_Mask                                                  cBit7_6
#define cAf6_control_channel_test_mode_Bandwidth_Shift                                                       6
#define cAf6_control_channel_test_mode_Bandwidth_MaxVal                                                    0x3
#define cAf6_control_channel_test_mode_Bandwidth_MinVal                                                    0x0
#define cAf6_control_channel_test_mode_Bandwidth_RstVal                                                    0x0

/*--------------------------------------
BitField Name: Data_mode
BitField Type: R/W
BitField Desc: Data mode: 0/1/2/3 -> PRBS15/PRBS23/FIX/SEQ.
BitField Bits: [9:8]
--------------------------------------*/
#define cAf6_control_channel_test_mode_Data_mode_Bit_Start                                                   8
#define cAf6_control_channel_test_mode_Data_mode_Bit_End                                                     9
#define cAf6_control_channel_test_mode_Data_mode_Mask                                                  cBit9_8
#define cAf6_control_channel_test_mode_Data_mode_Shift                                                       8
#define cAf6_control_channel_test_mode_Data_mode_MaxVal                                                    0x3
#define cAf6_control_channel_test_mode_Data_mode_MinVal                                                    0x0
#define cAf6_control_channel_test_mode_Data_mode_RstVal                                                    0x0

/*--------------------------------------
BitField Name: Fix_data_value
BitField Type: R/W
BitField Desc: Fix data value.
BitField Bits: [19:12]
--------------------------------------*/
#define cAf6_control_channel_test_mode_Fix_data_value_Bit_Start                                             12
#define cAf6_control_channel_test_mode_Fix_data_value_Bit_End                                               19
#define cAf6_control_channel_test_mode_Fix_data_value_Mask                                           cBit19_12
#define cAf6_control_channel_test_mode_Fix_data_value_Shift                                                 12
#define cAf6_control_channel_test_mode_Fix_data_value_MaxVal                                              0xff
#define cAf6_control_channel_test_mode_Fix_data_value_MinVal                                               0x0
#define cAf6_control_channel_test_mode_Fix_data_value_RstVal                                              0xAA

/*--------------------------------------
BitField Name: Payload_size
BitField Type: R/W
BitField Desc: Payload size.
BitField Bits: [31:20]
--------------------------------------*/
#define cAf6_control_channel_test_mode_Payload_size_Bit_Start                                               20
#define cAf6_control_channel_test_mode_Payload_size_Bit_End                                                 31
#define cAf6_control_channel_test_mode_Payload_size_Mask                                             cBit31_20
#define cAf6_control_channel_test_mode_Payload_size_Shift                                                   20
#define cAf6_control_channel_test_mode_Payload_size_MaxVal                                               0xfff
#define cAf6_control_channel_test_mode_Payload_size_MinVal                                                 0x0
#define cAf6_control_channel_test_mode_Payload_size_RstVal                                                0x40


/*------------------------------------------------------------------------------
Reg Name   : SGMII/QSGMII Channels Configure Mac DA MSB.
Reg Addr   : 0x3
Reg Formula: Channel_ID+0x3
    Where  :
           + $Channel_ID: 0x00 -> Channel-0 0x10 -> Channel-1 0x20 -> Channel-2 0x30 -> Channel-3
Reg Desc   :
Register to test SGMII/QSGMII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_control_mac_da_msb_Base                                                                    0x3
#define cAf6Reg_control_mac_da_msb(ChannelID)                                              ((ChannelID)+0x3UL)
#define cAf6Reg_control_mac_da_msb_WidthVal                                                                 32
#define cAf6Reg_control_mac_da_msb_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: Mac_DA_configure_MSB
BitField Type: R/W
BitField Desc: Mac DA MSB configure value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_control_mac_da_msb_Mac_DA_configure_MSB_Bit_Start                                               0
#define cAf6_control_mac_da_msb_Mac_DA_configure_MSB_Bit_End                                                31
#define cAf6_control_mac_da_msb_Mac_DA_configure_MSB_Mask                                             cBit31_0
#define cAf6_control_mac_da_msb_Mac_DA_configure_MSB_Shift                                                   0
#define cAf6_control_mac_da_msb_Mac_DA_configure_MSB_MaxVal                                         0xffffffff
#define cAf6_control_mac_da_msb_Mac_DA_configure_MSB_MinVal                                                0x0
#define cAf6_control_mac_da_msb_Mac_DA_configure_MSB_RstVal                                         0x01010101


/*------------------------------------------------------------------------------
Reg Name   : SGMII/QSGMII Channels Configure Mac DA LSB.
Reg Addr   : 0x4
Reg Formula: Channel_ID+0x4
    Where  :
           + $Channel_ID: 0x00 -> Channel-0 0x10 -> Channel-1 0x20 -> Channel-2 0x30 -> Channel-3
Reg Desc   :
Register to test SGMII/QSGMII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_control_mac_da_lsb_Base                                                                    0x4
#define cAf6Reg_control_mac_da_lsb(ChannelID)                                              ((ChannelID)+0x4UL)
#define cAf6Reg_control_mac_da_lsb_WidthVal                                                                 32
#define cAf6Reg_control_mac_da_lsb_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: Mac_DA_configure_LSB
BitField Type: R/W
BitField Desc: Mac DA LSB configure value
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_control_mac_da_lsb_Mac_DA_configure_LSB_Bit_Start                                               0
#define cAf6_control_mac_da_lsb_Mac_DA_configure_LSB_Bit_End                                                15
#define cAf6_control_mac_da_lsb_Mac_DA_configure_LSB_Mask                                             cBit15_0
#define cAf6_control_mac_da_lsb_Mac_DA_configure_LSB_Shift                                                   0
#define cAf6_control_mac_da_lsb_Mac_DA_configure_LSB_MaxVal                                             0xffff
#define cAf6_control_mac_da_lsb_Mac_DA_configure_LSB_MinVal                                                0x0
#define cAf6_control_mac_da_lsb_Mac_DA_configure_LSB_RstVal                                             0xC0CA


/*------------------------------------------------------------------------------
Reg Name   : SGMII/QSGMII Channels Configure Mac SA MSB.
Reg Addr   : 0x5
Reg Formula: Channel_ID+0x5
    Where  :
           + $Channel_ID: 0x00 -> Channel-0 0x10 -> Channel-1 0x20 -> Channel-2 0x30 -> Channel-3
Reg Desc   :
Register to test SGMII/QSGMII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_control_mac_sa_msb_Base                                                                    0x5
#define cAf6Reg_control_mac_sa_msb(ChannelID)                                              ((ChannelID)+0x5UL)
#define cAf6Reg_control_mac_sa_msb_WidthVal                                                                 32
#define cAf6Reg_control_mac_sa_msb_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: Mac_SA_configure_MSB
BitField Type: R/W
BitField Desc: Mac SA MSB configure value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_control_mac_sa_msb_Mac_SA_configure_MSB_Bit_Start                                               0
#define cAf6_control_mac_sa_msb_Mac_SA_configure_MSB_Bit_End                                                31
#define cAf6_control_mac_sa_msb_Mac_SA_configure_MSB_Mask                                             cBit31_0
#define cAf6_control_mac_sa_msb_Mac_SA_configure_MSB_Shift                                                   0
#define cAf6_control_mac_sa_msb_Mac_SA_configure_MSB_MaxVal                                         0xffffffff
#define cAf6_control_mac_sa_msb_Mac_SA_configure_MSB_MinVal                                                0x0
#define cAf6_control_mac_sa_msb_Mac_SA_configure_MSB_RstVal                                         0x01010101


/*------------------------------------------------------------------------------
Reg Name   : SGMII/QSGMII Channels Configure Mac SA LSB.
Reg Addr   : 0x6
Reg Formula: Channel_ID+0x6
    Where  :
           + $Channel_ID: 0x00 -> Channel-0 0x10 -> Channel-1 0x20 -> Channel-2 0x30 -> Channel-3
Reg Desc   :
Register to test SGMII/QSGMII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_control_mac_sa_lsb_Base                                                                    0x6
#define cAf6Reg_control_mac_sa_lsb(ChannelID)                                              ((ChannelID)+0x6UL)
#define cAf6Reg_control_mac_sa_lsb_WidthVal                                                                 32
#define cAf6Reg_control_mac_sa_lsb_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: Mac_SA_configure_LSB
BitField Type: R/W
BitField Desc: Mac SA LSB configure value
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_control_mac_sa_lsb_Mac_SA_configure_LSB_Bit_Start                                               0
#define cAf6_control_mac_sa_lsb_Mac_SA_configure_LSB_Bit_End                                                15
#define cAf6_control_mac_sa_lsb_Mac_SA_configure_LSB_Mask                                             cBit15_0
#define cAf6_control_mac_sa_lsb_Mac_SA_configure_LSB_Shift                                                   0
#define cAf6_control_mac_sa_lsb_Mac_SA_configure_LSB_MaxVal                                             0xffff
#define cAf6_control_mac_sa_lsb_Mac_SA_configure_LSB_MinVal                                                0x0
#define cAf6_control_mac_sa_lsb_Mac_SA_configure_LSB_RstVal                                             0xC0CA


/*------------------------------------------------------------------------------
Reg Name   : SGMII/QSGMII Channels Configure Mac Type.
Reg Addr   : 0x7
Reg Formula: Channel_ID+0x7
    Where  :
           + $Channel_ID: 0x00 -> Channel-0 0x10 -> Channel-1 0x20 -> Channel-2 0x30 -> Channel-3
Reg Desc   :
Register to test SGMII/QSGMII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_control_mac_type_Base                                                                      0x7
#define cAf6Reg_control_mac_type(ChannelID)                                                ((ChannelID)+0x7UL)
#define cAf6Reg_control_mac_type_WidthVal                                                                   32
#define cAf6Reg_control_mac_type_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: Mac_Type_configure
BitField Type: R/W
BitField Desc: Mac Type configure value
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_control_mac_type_Mac_Type_configure_Bit_Start                                                   0
#define cAf6_control_mac_type_Mac_Type_configure_Bit_End                                                    15
#define cAf6_control_mac_type_Mac_Type_configure_Mask                                                 cBit15_0
#define cAf6_control_mac_type_Mac_Type_configure_Shift                                                       0
#define cAf6_control_mac_type_Mac_Type_configure_MaxVal                                                 0xffff
#define cAf6_control_mac_type_Mac_Type_configure_MinVal                                                    0x0
#define cAf6_control_mac_type_Mac_Type_configure_RstVal                                                 0x0800


/*------------------------------------------------------------------------------
Reg Name   : SGMII/QSGMII Tx counter R2C.
Reg Addr   : 0x8
Reg Formula: Channel_ID+0x8
    Where  :
           + $Channel_ID: 0x00 -> Channel-0 0x10 -> Channel-1 0x20 -> Channel-2 0x30 -> Channel-3
Reg Desc   :
Register to test SGMII/QSGMII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_counter_tx_r2c_Base                                                                        0x8
#define cAf6Reg_counter_tx_r2c(ChannelID)                                                  ((ChannelID)+0x8UL)
#define cAf6Reg_counter_tx_r2c_WidthVal                                                                     32
#define cAf6Reg_counter_tx_r2c_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: Tx_counter_R2C
BitField Type: R2C
BitField Desc: Tx counter R2C
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_counter_tx_r2c_Tx_counter_R2C_Bit_Start                                                         0
#define cAf6_counter_tx_r2c_Tx_counter_R2C_Bit_End                                                          31
#define cAf6_counter_tx_r2c_Tx_counter_R2C_Mask                                                       cBit31_0
#define cAf6_counter_tx_r2c_Tx_counter_R2C_Shift                                                             0
#define cAf6_counter_tx_r2c_Tx_counter_R2C_MaxVal                                                   0xffffffff
#define cAf6_counter_tx_r2c_Tx_counter_R2C_MinVal                                                          0x0
#define cAf6_counter_tx_r2c_Tx_counter_R2C_RstVal                                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : SGMII/QSGMII Rx counter R2C.
Reg Addr   : 0x9
Reg Formula: Channel_ID+0x9
    Where  :
           + $Channel_ID: 0x00 -> Channel-0 0x10 -> Channel-1 0x20 -> Channel-2 0x30 -> Channel-3
Reg Desc   :
Register to test SGMII/QSGMII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_counter_rx_r2c_Base                                                                        0x9
#define cAf6Reg_counter_rx_r2c(ChannelID)                                                  ((ChannelID)+0x9UL)
#define cAf6Reg_counter_rx_r2c_WidthVal                                                                     32
#define cAf6Reg_counter_rx_r2c_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: Rx_counter_R2C
BitField Type: R2C
BitField Desc: Rx counter R2C
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_counter_rx_r2c_Rx_counter_R2C_Bit_Start                                                         0
#define cAf6_counter_rx_r2c_Rx_counter_R2C_Bit_End                                                          31
#define cAf6_counter_rx_r2c_Rx_counter_R2C_Mask                                                       cBit31_0
#define cAf6_counter_rx_r2c_Rx_counter_R2C_Shift                                                             0
#define cAf6_counter_rx_r2c_Rx_counter_R2C_MaxVal                                                   0xffffffff
#define cAf6_counter_rx_r2c_Rx_counter_R2C_MinVal                                                          0x0
#define cAf6_counter_rx_r2c_Rx_counter_R2C_RstVal                                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : SGMII/QSGMII FCS error counter R2C.
Reg Addr   : 0xa
Reg Formula: Channel_ID+0xa
    Where  :
           + $Channel_ID: 0x00 -> Channel-0 0x10 -> Channel-1 0x20 -> Channel-2 0x30 -> Channel-3
Reg Desc   :
Register to test SGMII/QSGMII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_counter_fcs_error_r2c_Base                                                                 0xa
#define cAf6Reg_counter_fcs_error_r2c(ChannelID)                                           ((ChannelID)+0xaUL)
#define cAf6Reg_counter_fcs_error_r2c_WidthVal                                                              32
#define cAf6Reg_counter_fcs_error_r2c_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: FCS_error_counter_R2C
BitField Type: R2C
BitField Desc: FCS error counter R2C
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_counter_fcs_error_r2c_FCS_error_counter_R2C_Bit_Start                                           0
#define cAf6_counter_fcs_error_r2c_FCS_error_counter_R2C_Bit_End                                            31
#define cAf6_counter_fcs_error_r2c_FCS_error_counter_R2C_Mask                                         cBit31_0
#define cAf6_counter_fcs_error_r2c_FCS_error_counter_R2C_Shift                                               0
#define cAf6_counter_fcs_error_r2c_FCS_error_counter_R2C_MaxVal                                     0xffffffff
#define cAf6_counter_fcs_error_r2c_FCS_error_counter_R2C_MinVal                                            0x0
#define cAf6_counter_fcs_error_r2c_FCS_error_counter_R2C_RstVal                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : SGMII/QSGMII Tx counter RO.
Reg Addr   : 0xb
Reg Formula: Channel_ID+0xb
    Where  :
           + $Channel_ID: 0x00 -> Channel-0 0x10 -> Channel-1 0x20 -> Channel-2 0x30 -> Channel-3
Reg Desc   :
Register to test SGMII/QSGMII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_counter_tx_ro_Base                                                                         0xb
#define cAf6Reg_counter_tx_ro(ChannelID)                                                   ((ChannelID)+0xbUL)
#define cAf6Reg_counter_tx_ro_WidthVal                                                                      32
#define cAf6Reg_counter_tx_ro_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: Tx_counter_RO
BitField Type: RO
BitField Desc: Tx counter RO
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_counter_tx_ro_Tx_counter_RO_Bit_Start                                                           0
#define cAf6_counter_tx_ro_Tx_counter_RO_Bit_End                                                            31
#define cAf6_counter_tx_ro_Tx_counter_RO_Mask                                                         cBit31_0
#define cAf6_counter_tx_ro_Tx_counter_RO_Shift                                                               0
#define cAf6_counter_tx_ro_Tx_counter_RO_MaxVal                                                     0xffffffff
#define cAf6_counter_tx_ro_Tx_counter_RO_MinVal                                                            0x0
#define cAf6_counter_tx_ro_Tx_counter_RO_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : SGMII/QSGMII Rx counter RO.
Reg Addr   : 0xc
Reg Formula: Channel_ID+0xc
    Where  :
           + $Channel_ID: 0x00 -> Channel-0 0x10 -> Channel-1 0x20 -> Channel-2 0x30 -> Channel-3
Reg Desc   :
Register to test SGMII/QSGMII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_counter_rx_ro_Base                                                                         0xc
#define cAf6Reg_counter_rx_ro(ChannelID)                                                   ((ChannelID)+0xcUL)
#define cAf6Reg_counter_rx_ro_WidthVal                                                                      32
#define cAf6Reg_counter_rx_ro_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: Rx_counter_RO
BitField Type: RO
BitField Desc: Rx counter RO
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_counter_rx_ro_Rx_counter_RO_Bit_Start                                                           0
#define cAf6_counter_rx_ro_Rx_counter_RO_Bit_End                                                            31
#define cAf6_counter_rx_ro_Rx_counter_RO_Mask                                                         cBit31_0
#define cAf6_counter_rx_ro_Rx_counter_RO_Shift                                                               0
#define cAf6_counter_rx_ro_Rx_counter_RO_MaxVal                                                     0xffffffff
#define cAf6_counter_rx_ro_Rx_counter_RO_MinVal                                                            0x0
#define cAf6_counter_rx_ro_Rx_counter_RO_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : SGMII/QSGMII FCS error counter RO.
Reg Addr   : 0xd
Reg Formula: Channel_ID+0xd
    Where  :
           + $Channel_ID: 0x00 -> Channel-0 0x10 -> Channel-1 0x20 -> Channel-2 0x30 -> Channel-3
Reg Desc   :
Register to test SGMII/QSGMII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_counter_fcs_error_ro_Base                                                                  0xd
#define cAf6Reg_counter_fcs_error_ro(ChannelID)                                            ((ChannelID)+0xdUL)
#define cAf6Reg_counter_fcs_error_ro_WidthVal                                                               32
#define cAf6Reg_counter_fcs_error_ro_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: FCS_error_counter_RO
BitField Type: RO
BitField Desc: FCS error counter RO
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_counter_fcs_error_ro_FCS_error_counter_RO_Bit_Start                                             0
#define cAf6_counter_fcs_error_ro_FCS_error_counter_RO_Bit_End                                              31
#define cAf6_counter_fcs_error_ro_FCS_error_counter_RO_Mask                                           cBit31_0
#define cAf6_counter_fcs_error_ro_FCS_error_counter_RO_Shift                                                 0
#define cAf6_counter_fcs_error_ro_FCS_error_counter_RO_MaxVal                                       0xffffffff
#define cAf6_counter_fcs_error_ro_FCS_error_counter_RO_MinVal                                              0x0
#define cAf6_counter_fcs_error_ro_FCS_error_counter_RO_RstVal                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : SGMII/QSGMII PHY link up sticky.
Reg Addr   : 0x300
Reg Formula:
    Where  :
Reg Desc   :
Register to test SGMII/QSGMII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_phy_sticky_Base                                                                          0x300
#define cAf6Reg_phy_sticky                                                                             0x300UL
#define cAf6Reg_phy_sticky_WidthVal                                                                         32
#define cAf6Reg_phy_sticky_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: Channel_0_link_up
BitField Type: R/W/C
BitField Desc: Channel 0 link up status.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_phy_sticky_Channel_0_link_up_Bit_Start                                                          0
#define cAf6_phy_sticky_Channel_0_link_up_Bit_End                                                            0
#define cAf6_phy_sticky_Channel_0_link_up_Mask                                                           cBit0
#define cAf6_phy_sticky_Channel_0_link_up_Shift                                                              0
#define cAf6_phy_sticky_Channel_0_link_up_MaxVal                                                           0x1
#define cAf6_phy_sticky_Channel_0_link_up_MinVal                                                           0x0
#define cAf6_phy_sticky_Channel_0_link_up_RstVal                                                           0x0

/*--------------------------------------
BitField Name: Channel_1_link_up
BitField Type: R/W/C
BitField Desc: Channel 1 link up status.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_phy_sticky_Channel_1_link_up_Bit_Start                                                          1
#define cAf6_phy_sticky_Channel_1_link_up_Bit_End                                                            1
#define cAf6_phy_sticky_Channel_1_link_up_Mask                                                           cBit1
#define cAf6_phy_sticky_Channel_1_link_up_Shift                                                              1
#define cAf6_phy_sticky_Channel_1_link_up_MaxVal                                                           0x1
#define cAf6_phy_sticky_Channel_1_link_up_MinVal                                                           0x0
#define cAf6_phy_sticky_Channel_1_link_up_RstVal                                                           0x0

/*--------------------------------------
BitField Name: Channel_2_link_up
BitField Type: R/W/C
BitField Desc: Channel 2 link up status.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_phy_sticky_Channel_2_link_up_Bit_Start                                                          2
#define cAf6_phy_sticky_Channel_2_link_up_Bit_End                                                            2
#define cAf6_phy_sticky_Channel_2_link_up_Mask                                                           cBit2
#define cAf6_phy_sticky_Channel_2_link_up_Shift                                                              2
#define cAf6_phy_sticky_Channel_2_link_up_MaxVal                                                           0x1
#define cAf6_phy_sticky_Channel_2_link_up_MinVal                                                           0x0
#define cAf6_phy_sticky_Channel_2_link_up_RstVal                                                           0x0

/*--------------------------------------
BitField Name: Channel_3_link_up
BitField Type: R/W/C
BitField Desc: Channel 3 link up status.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_phy_sticky_Channel_3_link_up_Bit_Start                                                          3
#define cAf6_phy_sticky_Channel_3_link_up_Bit_End                                                            3
#define cAf6_phy_sticky_Channel_3_link_up_Mask                                                           cBit3
#define cAf6_phy_sticky_Channel_3_link_up_Shift                                                              3
#define cAf6_phy_sticky_Channel_3_link_up_MaxVal                                                           0x1
#define cAf6_phy_sticky_Channel_3_link_up_MinVal                                                           0x0
#define cAf6_phy_sticky_Channel_3_link_up_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : SGMII/QSGMII PHY link up status.
Reg Addr   : 0x301
Reg Formula:
    Where  :
Reg Desc   :
Register to test SGMII/QSGMII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_phy_status_Base                                                                          0x301
#define cAf6Reg_phy_status                                                                             0x301UL
#define cAf6Reg_phy_status_WidthVal                                                                         32
#define cAf6Reg_phy_status_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: Channel_0_link_up
BitField Type: RO
BitField Desc: Channel 0 link up status.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_phy_status_Channel_0_link_up_Bit_Start                                                          0
#define cAf6_phy_status_Channel_0_link_up_Bit_End                                                            0
#define cAf6_phy_status_Channel_0_link_up_Mask                                                           cBit0
#define cAf6_phy_status_Channel_0_link_up_Shift                                                              0
#define cAf6_phy_status_Channel_0_link_up_MaxVal                                                           0x1
#define cAf6_phy_status_Channel_0_link_up_MinVal                                                           0x0
#define cAf6_phy_status_Channel_0_link_up_RstVal                                                           0x0

/*--------------------------------------
BitField Name: Channel_1_link_up
BitField Type: RO
BitField Desc: Channel 1 link up status.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_phy_status_Channel_1_link_up_Bit_Start                                                          1
#define cAf6_phy_status_Channel_1_link_up_Bit_End                                                            1
#define cAf6_phy_status_Channel_1_link_up_Mask                                                           cBit1
#define cAf6_phy_status_Channel_1_link_up_Shift                                                              1
#define cAf6_phy_status_Channel_1_link_up_MaxVal                                                           0x1
#define cAf6_phy_status_Channel_1_link_up_MinVal                                                           0x0
#define cAf6_phy_status_Channel_1_link_up_RstVal                                                           0x0

/*--------------------------------------
BitField Name: Channel_2_link_up
BitField Type: RO
BitField Desc: Channel 2 link up status.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_phy_status_Channel_2_link_up_Bit_Start                                                          2
#define cAf6_phy_status_Channel_2_link_up_Bit_End                                                            2
#define cAf6_phy_status_Channel_2_link_up_Mask                                                           cBit2
#define cAf6_phy_status_Channel_2_link_up_Shift                                                              2
#define cAf6_phy_status_Channel_2_link_up_MaxVal                                                           0x1
#define cAf6_phy_status_Channel_2_link_up_MinVal                                                           0x0
#define cAf6_phy_status_Channel_2_link_up_RstVal                                                           0x0

/*--------------------------------------
BitField Name: Channel_3_link_up
BitField Type: RO
BitField Desc: Channel 3 link up status.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_phy_status_Channel_3_link_up_Bit_Start                                                          3
#define cAf6_phy_status_Channel_3_link_up_Bit_End                                                            3
#define cAf6_phy_status_Channel_3_link_up_Mask                                                           cBit3
#define cAf6_phy_status_Channel_3_link_up_Shift                                                              3
#define cAf6_phy_status_Channel_3_link_up_MaxVal                                                           0x1
#define cAf6_phy_status_Channel_3_link_up_MinVal                                                           0x0
#define cAf6_phy_status_Channel_3_link_up_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : SGMII/QSGMII PHY configure near-end loopback.
Reg Addr   : 0x302
Reg Formula:
    Where  :
Reg Desc   :
Register to test SGMII/QSGMII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_control_phy_Base                                                                         0x302
#define cAf6Reg_control_phy                                                                            0x302UL
#define cAf6Reg_control_phy_WidthVal                                                                        32
#define cAf6Reg_control_phy_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: Channel_0_nearend_loopback_enable
BitField Type: R/W
BitField Desc: Channel 0 near-end loopback enable.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_control_phy_Channel_0_nearend_loopback_enable_Bit_Start                                         0
#define cAf6_control_phy_Channel_0_nearend_loopback_enable_Bit_End                                           0
#define cAf6_control_phy_Channel_0_nearend_loopback_enable_Mask                                          cBit0
#define cAf6_control_phy_Channel_0_nearend_loopback_enable_Shift                                             0
#define cAf6_control_phy_Channel_0_nearend_loopback_enable_MaxVal                                          0x1
#define cAf6_control_phy_Channel_0_nearend_loopback_enable_MinVal                                          0x0
#define cAf6_control_phy_Channel_0_nearend_loopback_enable_RstVal                                          0x0

/*--------------------------------------
BitField Name: Channel_1_nearend_loopback_enable
BitField Type: R/W
BitField Desc: Channel 1 near-end loopback enable.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_control_phy_Channel_1_nearend_loopback_enable_Bit_Start                                         1
#define cAf6_control_phy_Channel_1_nearend_loopback_enable_Bit_End                                           1
#define cAf6_control_phy_Channel_1_nearend_loopback_enable_Mask                                          cBit1
#define cAf6_control_phy_Channel_1_nearend_loopback_enable_Shift                                             1
#define cAf6_control_phy_Channel_1_nearend_loopback_enable_MaxVal                                          0x1
#define cAf6_control_phy_Channel_1_nearend_loopback_enable_MinVal                                          0x0
#define cAf6_control_phy_Channel_1_nearend_loopback_enable_RstVal                                          0x0

/*--------------------------------------
BitField Name: Channel_2_nearend_loopback_enable
BitField Type: R/W
BitField Desc: Channel 2 near-end loopback enable.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_control_phy_Channel_2_nearend_loopback_enable_Bit_Start                                         2
#define cAf6_control_phy_Channel_2_nearend_loopback_enable_Bit_End                                           2
#define cAf6_control_phy_Channel_2_nearend_loopback_enable_Mask                                          cBit2
#define cAf6_control_phy_Channel_2_nearend_loopback_enable_Shift                                             2
#define cAf6_control_phy_Channel_2_nearend_loopback_enable_MaxVal                                          0x1
#define cAf6_control_phy_Channel_2_nearend_loopback_enable_MinVal                                          0x0
#define cAf6_control_phy_Channel_2_nearend_loopback_enable_RstVal                                          0x0

/*--------------------------------------
BitField Name: Channel_3_nearend_loopback_enable
BitField Type: R/W
BitField Desc: Channel 3 near-end loopback enable.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_control_phy_Channel_3_nearend_loopback_enable_Bit_Start                                         3
#define cAf6_control_phy_Channel_3_nearend_loopback_enable_Bit_End                                           3
#define cAf6_control_phy_Channel_3_nearend_loopback_enable_Mask                                          cBit3
#define cAf6_control_phy_Channel_3_nearend_loopback_enable_Shift                                             3
#define cAf6_control_phy_Channel_3_nearend_loopback_enable_MaxVal                                          0x1
#define cAf6_control_phy_Channel_3_nearend_loopback_enable_MinVal                                          0x0
#define cAf6_control_phy_Channel_3_nearend_loopback_enable_RstVal                                          0x0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60210061SGMIIQSGMIISERDESPRBSENGINEREGV2_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60210061SerdesPrbsEngineInternal.h
 * 
 * Created Date: Jun 23, 2016
 *
 * Description :
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061SERDESPRBSENGINEINTERNAL_H_
#define _THA60210061SERDESPRBSENGINEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/prbs/AtPrbsEngineInternal.h"
#include "../../../../generic/physical/AtSerdesControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210061QsgmiiSerdesPrbsEngine *Tha60210061QsgmiiSerdesPrbsEngine;
typedef struct tTha60210061QsgmiiSerdesPrbsEngineMethods
    {
    uint32 (*BaseAddress)(Tha60210061QsgmiiSerdesPrbsEngine self);
    eAtRet (*DestMacSet)(AtPrbsEngine self, uint8* mac);
    eAtRet (*SrcMacSet)(AtPrbsEngine self, uint8* mac);
    eAtRet (*EthernetTypeSet)(AtPrbsEngine self, uint16 ethType);
    void   (*HwDefaultSet)(Tha60210061QsgmiiSerdesPrbsEngine self);
    }tTha60210061QsgmiiSerdesPrbsEngineMethods;

typedef struct tTha60210061QsgmiiSerdesPrbsEngine
    {
    tAtPrbsEngine super;
    const tTha60210061QsgmiiSerdesPrbsEngineMethods *methods;

    }tTha60210061QsgmiiSerdesPrbsEngine;

typedef struct tTha60210061SgmiiSerdesPrbsEngine
    {
    tTha60210061QsgmiiSerdesPrbsEngine super;
    }tTha60210061SgmiiSerdesPrbsEngine;

typedef struct tTha60210061SerdesEthFramePrbsEngine * Tha60210061SerdesEthFramePrbsEngine;
typedef struct tTha60210061SerdesEthFramePrbsEngine
    {
    tAtPrbsEngine super;
    AtSerdesController serdesController;
    }tTha60210061SerdesEthFramePrbsEngine;

typedef struct tTha60210061SgmiiSerdesRawPrbsEngine *Tha60210061SgmiiSerdesRawPrbsEngine;
typedef struct tTha60210061SgmiiSerdesRawPrbsEngineMethods
    {
    uint32 (*HssiSerdesId)(Tha60210061SgmiiSerdesRawPrbsEngine self);
    uint32 (*PortSerdesId)(Tha60210061SgmiiSerdesRawPrbsEngine self);
    uint32 (*CounterOffset)(Tha60210061SgmiiSerdesRawPrbsEngine self);
    uint32 (*ControlledByArriveRegisterEnable)(Tha60210061SgmiiSerdesRawPrbsEngine self);
    }tTha60210061SgmiiSerdesRawPrbsEngineMethods;

typedef struct tTha60210061SgmiiSerdesRawPrbsEngine
    {
    tAtPrbsEngine super;
    const tTha60210061SgmiiSerdesRawPrbsEngineMethods *methods;

    AtSerdesController serdesController;
    eAtPrbsMode txPrbsMode;
    eAtPrbsMode rxPrbsMode;
    }tTha60210061SgmiiSerdesRawPrbsEngine;

typedef struct tTha60210061GthSerdesRawPrbsEngine
    {
    tTha60210061SgmiiSerdesRawPrbsEngine super;
    }tTha60210061GthSerdesRawPrbsEngine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngine Tha60210061QsgmiiSerdesPrbsEngineObjectInit(AtPrbsEngine self, AtSerdesController serdesController);
AtPrbsEngine Tha60210061SgmiiSerdesPrbsEngineObjectInit(AtPrbsEngine self, AtSerdesController serdesController);
AtPrbsEngine Tha60210061SerdesEthFramePrbsEngineObjectInit(AtPrbsEngine self, AtSerdesController serdesController);
AtPrbsEngine Tha60210061SgmiiSerdesRawPrbsObjectInit(AtPrbsEngine self, AtSerdesController serdesController);
AtPrbsEngine Tha60210061GthSerdesRawPrbsEngineObjectInit(AtPrbsEngine self, AtSerdesController serdesController);

eAtRet Tha60210061SerdesPrbsQsgmiiDestMacSubChannelSet(AtPrbsEngine self, uint32 subChannel, uint8* mac);
eAtRet Tha60210061SerdesPrbsQsgmiiSrcMacSubChannelSet(AtPrbsEngine self, uint32 subChannel, uint8* mac);
eAtRet Tha60210061SerdesPrbsQsgmiiEthernetTypeSubChannelSet(AtPrbsEngine self, uint32 subChannel, uint16 ethType);
eAtModulePrbsRet Tha60210061SerdesPrbsQsgmiiSubChannelEnable(AtPrbsEngine self, uint32 subChannel, eBool enable);
eBool Tha60210061SerdesPrbsQsgmiiSubChannelIsEnabled(AtPrbsEngine self, uint32 subChannel);
void Tha60210061SerdesPrbsQsgmiiSubChannelHwDefault(AtPrbsEngine self, uint32 subChannel);
eAtModulePrbsRet Tha60210061SerdesPrbsQsgmiiSubChannelErrorForce(AtPrbsEngine self, uint32 subChannel, eBool force);
eBool Tha60210061SerdesPrbsQsgmiiSubChannelErrorIsForced(AtPrbsEngine self, uint32 subChannel);
uint32 Tha60210061SerdesPrbsQsgmiiSubChannelAlarmHistoryHelper(AtPrbsEngine self, uint32 subChannel, eBool readToClear);
uint32 Tha60210061SerdesPrbsQsgmiiSubChannelCounterGet(AtPrbsEngine self, uint32 subChannel, uint16 counterType);
uint32 Tha60210061SerdesPrbsQsgmiiSubChannelCounterClear(AtPrbsEngine self, uint32 subChannel, uint16 counterType);
eAtModulePrbsRet Tha60210061SerdesPrbsQsgmiiSubChannelModeSet(AtPrbsEngine self, uint32 subChannel, eAtPrbsMode prbsMode);
eAtPrbsMode Tha60210061SerdesPrbsQsgmiiSubChannelModeGet(AtPrbsEngine self, uint32 subChannel);
eAtModulePrbsRet Tha60210061SerdesPrbsQsgmiiSubChannelFixedPatternSet(AtPrbsEngine self, uint32 subChannel, uint32 fixedPattern);
uint32 Tha60210061SerdesPrbsQsgmiiSubChannelFixedPatternGet(AtPrbsEngine self, uint32 subChannel);
void Tha60210061SerdesPrbsQsgmiiSubChannelDebug(AtPrbsEngine self, uint32 subChannel);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061SERDESPRBSENGINEINTERNAL_H_ */


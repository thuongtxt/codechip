/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60210061EthFramedPrbsEngine.c
 *
 * Created Date: Feb 15, 2017
 *
 * Description : Ethernet framed PRBS engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/prbs/AtPrbsEngineInternal.h"
#include "../../../../generic/common/AtChannelInternal.h"
#include "../../../../generic/man/AtModuleInternal.h"
#include "../eth/Tha60210061ModuleEth.h"
#include "../eth/Tha60210061EthPassThroughReg.h"
#include "Tha60210061EthFramePrbsEngineReg.h"
#include "Tha60210061ModulePrbs.h"

/*--------------------------- Define -----------------------------------------*/
#define cGenToTxMac    0
#define cGenToCore     1
#define cMonFromMac    0
#define cMonFromPda    1
#define cRxHwFrameGoodCount           1
#define cRxHwFrameFcsErrorCount       2
#define cRxHwFrameFrameableErrorCount 3
#define cRxHwFrameReceiveFrErrCount   4
#define cRxHwFrameFrIPGErrorCount     5
#define cRxHwFrameFrDataErrorCount    6

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210061EthFramePrbsEngine)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061EthFramePrbsEngine * Tha60210061EthFramePrbsEngine;
typedef struct tTha60210061EthFramePrbsEngine
    {
    tAtPrbsEngine super;
    eAtPrbsMode txMode;
    }tTha60210061EthFramePrbsEngine;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtPrbsEngineMethods m_AtPrbsEngineOverride;

/* Save super implementation */
static const tAtObjectMethods     *m_AtObjectMethods     = NULL;
static const tAtPrbsEngineMethods *m_AtPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 GenBaseAddress(void)
    {
    return cEthFrameGenBaseAddress;
    }

static uint32 MonEngineId(AtPrbsEngine self)
    {
    AtUnused(self);
    return 0; /* Only one mon engine */
    }

static uint32 MonBaseAddress(AtPrbsEngine self, uint32 monId)
    {
    AtUnused(self);
    return 0x04D000 + monId * 0x100;
    }

static const char *TypeString(AtPrbsEngine self)
    {
    AtUnused(self);
    return "Ethernet_frame";
    }

static uint8 SwMode2HwMode(eAtPrbsMode mode)
    {
    if (mode == cAtPrbsModePrbs31)  return 0xF;
    if (mode == cAtPrbsModePrbs15)  return 0xA;
    if (mode == cAtPrbsModePrbsSeq) return 0x0;
    if (mode == cAtPrbsModePrbsFixedPattern1Byte) return 0x2;

    return 0;
    }

static eAtPrbsMode HwMode2SwMode(uint32 mode)
    {
    if (mode == 0xF) return cAtPrbsModePrbs31;
    if (mode == 0xA) return cAtPrbsModePrbs15;
    if (mode == 0x0) return cAtPrbsModePrbsSeq;
    if (mode == 0x2) return cAtPrbsModePrbsFixedPattern1Byte;

    return cAtPrbsModeInvalid;
    }

static eBool ShouldEnableFcsMon(AtPrbsEngine self)
    {
    AtEthPort port = (AtEthPort)AtPrbsEngineChannelGet(self);
    if (Tha60210061ModuleEthPortIs10G(port))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 MacId(AtPrbsEngine self)
    {
    return AtChannelHwIdGet(AtPrbsEngineChannelGet(self));
    }

static eBool TxIsEnabled(AtPrbsEngine self)
    {
    uint32 address = cAf6Reg_upen_ethpgen_Base + cMacEthCoreBase;
    uint32 regVal = AtPrbsEngineRead(self, address, cAtModulePrbs);

    return (mRegField(regVal, cAf6_upen_ethpgen_Gen_En_)) ? cAtTrue : cAtFalse;
    }

static void TxConfigureApply(AtPrbsEngine self)
    {
    uint32 address = cAf6Reg_egen_active_Base + GenBaseAddress();

    AtPrbsEngineWrite(self, address, 0, cAtModulePrbs);
    AtPrbsEngineWrite(self, address, cAf6_egen_active_Restart_Mask | cAf6_egen_active_GenEn_Mask | cAf6_egen_active_LoadEn_Mask, cAtModulePrbs);
    }

static eAtRet TxHwModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    uint32 address = cAf6Reg_egen_modopt_Base + GenBaseAddress();
    uint32 regVal = AtPrbsEngineRead(self, address, cAtModulePrbs);
    uint32 hwMode = SwMode2HwMode(prbsMode);

    mRegFieldSet(regVal, cAf6_egen_modopt_DataMod_, hwMode);
    AtPrbsEngineWrite(self, address, regVal, cAtModulePrbs);
    if (TxIsEnabled(self))
        TxConfigureApply(self);

    return cAtOk;
    }

static eAtModulePrbsRet TxHwEnable(AtPrbsEngine self, eBool enable)
    {
    uint32 address = cAf6Reg_upen_ethpgen_Base + cMacEthCoreBase;
    uint32 regVal = AtPrbsEngineRead(self, address, cAtModulePrbs);

    /* Enable in core block */
    mRegFieldSet(regVal, cAf6_upen_ethpgen_Gen_En_, mBoolToBin(enable));
    mRegFieldSet(regVal, cAf6_upen_ethpgen_Gen_Mac_ID_, MacId(self));
    AtPrbsEngineWrite(self, address, regVal, cAtModulePrbs);

    /* Enable in gen block */
    address = cAf6Reg_egen_active_Base + GenBaseAddress();
    regVal = (enable) ? cAf6_egen_active_Restart_Mask | cAf6_egen_active_GenEn_Mask | cAf6_egen_active_LoadEn_Mask : 0;
    AtPrbsEngineWrite(self, address, regVal, cAtModulePrbs);

    return cAtOk;
    }

static AtEthPort BypassPort(AtPrbsEngine self)
    {
    AtEthPort gePort = (AtEthPort)AtPrbsEngineChannelGet(self);
    return Tha60210061GePortBypassPortGet(gePort);
    }

static eAtModulePrbsRet PortRxMacTrafficEnable(AtEthPort port, eBool enable)
    {
    uint32 regAddr = (uint32)(cAf6Reg_upen_userport_en_Base + cMacEthCoreBase);
    uint32 regVal = mChannelHwRead(port, regAddr, cAtModulePrbs);
    uint32 enableFieldMask  = cBit0 << AtChannelHwIdGet((AtChannel)port);
    uint32 enableFieldShift = AtChannelHwIdGet((AtChannel)port);

    mRegFieldSet(regVal, enableField, mBoolToBin(enable));
    mChannelHwWrite(port, regAddr, regVal, cAtModulePrbs);

    return cAtOk;
    }

static eAtModulePrbsRet PortTxMacTrafficEnable(AtEthPort port, eBool enable)
    {
    uint32 regAddr = (uint32)(cAf6Reg_upen_userport_en_Base + cMacEthCoreBase);
    uint32 regVal = mChannelHwRead(port, regAddr, cAtModulePrbs);
    uint32 enableFieldMask  = cBit16 << AtChannelHwIdGet((AtChannel)port);
    uint32 enableFieldShift = AtChannelHwIdGet((AtChannel)port) + 16;

    mRegFieldSet(regVal, enableField, mBoolToBin(enable));
    mChannelHwWrite(port, regAddr, regVal, cAtModulePrbs);

    return cAtOk;
    }

static void GenToPsnEngineEnable(AtPrbsEngine self, eBool enable)
    {
    uint32 address = (uint32)(cAf6Reg_upen_userport_en_Base + cMacEthCoreBase);
    uint32 regVal = AtPrbsEngineRead(self, address, cAtModulePrbs);

    mRegFieldSet(regVal, cAf6_upen_ethpgen_En_, mBoolToBin(enable));
    AtPrbsEngineWrite(self, address, regVal, cAtModulePrbs);
    }

static eAtModulePrbsRet NormalTrafficIsolate(AtPrbsEngine self, eAtPrbsSide genSide)
    {
    AtEthPort port = (AtEthPort)AtPrbsEngineChannelGet(self);
    AtEthPort bypassPort = BypassPort(self);

    PortTxMacTrafficEnable(port, (genSide == cAtPrbsSideTdm) ? cAtFalse : cAtTrue);
    PortRxMacTrafficEnable(port, (genSide == cAtPrbsSideTdm) ? cAtTrue  : cAtFalse);

    /* If generate to TDM and bypass to QSGMII, rx QSGMII need to isolate also */
	if ((bypassPort) && (!Tha60210061ModuleEthPortIs10G(bypassPort)))
	    PortRxMacTrafficEnable(bypassPort, (genSide == cAtPrbsSideTdm) ? cAtFalse : cAtTrue);

    if (genSide == cAtPrbsSidePsn)
        GenToPsnEngineEnable(self, cAtTrue);

    return cAtOk;
    }

static eAtModulePrbsRet NormalTrafficEnable(AtPrbsEngine self, eAtPrbsSide genSide)
    {
    AtEthPort port = (AtEthPort)AtPrbsEngineChannelGet(self);
    AtEthPort bypassPort = BypassPort(self);

    if (genSide == cAtPrbsSidePsn)
        GenToPsnEngineEnable(self, cAtFalse);

    PortTxMacTrafficEnable(port, cAtTrue);
    PortRxMacTrafficEnable(port, cAtTrue);

    /* If generate to TDM and bypass to QSGMII, rx QSGMII need to isolate also */
    if ((bypassPort) && (!Tha60210061ModuleEthPortIs10G(bypassPort)))
        PortRxMacTrafficEnable(bypassPort, cAtTrue);

    return cAtOk;
    }

static eAtModulePrbsRet TxEnable(AtPrbsEngine self, eBool enable)
    {
    eAtPrbsSide genSide = mMethodsGet(self)->GeneratingSideGet(self);
    if (mMethodsGet(self)->TxIsEnabled(self) == enable)
        return cAtOk;

    if (enable)
        {
        NormalTrafficIsolate(self, genSide);
        TxHwEnable(self, cAtTrue);
        }
    else
        {
        TxHwEnable(self, cAtFalse);
        NormalTrafficEnable(self, genSide);
        }

    return cAtOk;
    }

static eAtModulePrbsRet RxEnable(AtPrbsEngine self, eBool enable)
    {
    uint32 monEngineId = MonEngineId(self);
    uint32 address = cAf6Reg_upen_ethpmon_Base + cMacEthCoreBase + monEngineId;
    uint32 regVal = AtPrbsEngineRead(self, address, cAtModulePrbs);

    mRegFieldSet(regVal, cAf6_upen_ethpmon_In_Mac_En_ , mBoolToBin(enable));
    mRegFieldSet(regVal, cAf6_upen_ethpmon_In_Mac_ID_ , AtChannelHwIdGet(AtPrbsEngineChannelGet(self)));

    AtPrbsEngineWrite(self, address, regVal, cAtModulePrbs);

    return cAtOk;
    }

static eBool RxIsEnabled(AtPrbsEngine self)
    {
    uint32 monEngineId = MonEngineId(self);
    uint32 address = cAf6Reg_upen_ethpmon_Base + cMacEthCoreBase + monEngineId;
    uint32 regVal = AtPrbsEngineRead(self, address, cAtModulePrbs);

    return (regVal & cAf6_upen_ethpmon_In_Mac_En_Mask) ? cAtTrue : cAtFalse;
    }

static eAtModulePrbsRet TxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    if (!AtPrbsEngineModeIsSupported(self, prbsMode))
        return cAtErrorModeNotSupport;

    return TxHwModeSet(self, prbsMode);
    }

static eAtPrbsMode TxModeGet(AtPrbsEngine self)
    {
    uint32 hwMode, address, regVal;

    address = cAf6Reg_egen_modopt_Base + GenBaseAddress();
    regVal = AtPrbsEngineRead(self, address, cAtModulePrbs);
    hwMode = mRegField(regVal, cAf6_egen_modopt_DataMod_);

    return HwMode2SwMode(hwMode);
    }

static eAtModulePrbsRet RxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    uint32 address, regVal, hwMode;
    uint32 monEngineId;

    if (!AtPrbsEngineModeIsSupported(self, prbsMode))
        return cAtErrorModeNotSupport;

    monEngineId = MonEngineId(self);
    address = cAf6Reg_emon_data_Base + MonBaseAddress(self, monEngineId);
    regVal = AtPrbsEngineRead(self, address, cAtModulePrbs);
    hwMode = SwMode2HwMode(prbsMode);

    mRegFieldSet(regVal, cAf6_emon_data_DataMod_, hwMode);
    AtPrbsEngineWrite(self, address, regVal, cAtModulePrbs);

    return cAtOk;
    }

static eAtPrbsMode RxModeGet(AtPrbsEngine self)
    {
    uint32 hwMode;
    uint32 addressEthMon = cAf6Reg_emon_data_Base + MonBaseAddress(self, MonEngineId(self));
    uint32 regValEthMon = AtPrbsEngineRead(self, addressEthMon, cAtModulePrbs);

    hwMode = mRegField(regValEthMon, cAf6_emon_data_DataMod_);
    return HwMode2SwMode(hwMode);
    }

static eAtModulePrbsRet TxFixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    uint32 address, regVal;

    if (mMethodsGet(self)->TxModeGet(self) != cAtPrbsModePrbsFixedPattern1Byte)
        return cAtErrorNotApplicable;

    address = cAf6Reg_egen_modopt_Base + GenBaseAddress();
    regVal = AtPrbsEngineRead(self, address, cAtModulePrbs);

    mRegFieldSet(regVal, cAf6_egen_modopt_DataFix_, fixedPattern);
    AtPrbsEngineWrite(self, address, regVal, cAtModulePrbs);

    if (TxIsEnabled(self))
        TxConfigureApply(self);

    return cAtOk;
    }

static uint32 TxFixedPatternGet(AtPrbsEngine self)
    {
    uint32 addressEthGen, regValEthGen;

    addressEthGen = cAf6Reg_egen_modopt_Base + GenBaseAddress();
    regValEthGen = AtPrbsEngineRead(self, addressEthGen, cAtModulePrbs);

    return mRegField(regValEthGen, cAf6_egen_modopt_DataFix_);
    }

static eAtModulePrbsRet RxFixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    uint32 addressEthMon = cAf6Reg_emon_data_Base + MonBaseAddress(self, MonEngineId(self));
    uint32 regValEthMon = AtPrbsEngineRead(self, addressEthMon, cAtModulePrbs);
    uint32 hwMode = mRegField(regValEthMon, cAf6_emon_data_DataMod_);

    if (HwMode2SwMode(hwMode) != cAtPrbsModePrbsFixedPattern1Byte)
       return cAtErrorNotApplicable;

    mRegFieldSet(regValEthMon, cAf6_emon_data_DataFix_, fixedPattern);
    AtPrbsEngineWrite(self, addressEthMon, regValEthMon, cAtModulePrbs);

    return cAtOk;
    }

static uint32 RxFixedPatternGet(AtPrbsEngine self)
    {
    uint32 addressEthMon = cAf6Reg_emon_data_Base + MonBaseAddress(self, MonEngineId(self));
    uint32 regValEthMon = AtPrbsEngineRead(self, addressEthMon, cAtModulePrbs);

    return mRegField(regValEthMon, cAf6_emon_data_DataFix_);
    }

static uint32 MonCounterOffset(AtPrbsEngine self, uint32 monId, eBool r2c, uint32 type)
    {
    if (r2c)
        return (MonBaseAddress(self, monId) + 0x10 + type);

    return (MonBaseAddress(self, monId) + type);
    }

static uint32 GenTxPacketsCounterGet(AtPrbsEngine self, eBool r2c)
    {
    AtEthPort port = (AtEthPort)AtPrbsEngineChannelGet(self);
    if (r2c)
        return AtChannelCounterClear((AtChannel)port, cAtEthPortCounterTxPackets);

    return AtChannelCounterGet((AtChannel)port, cAtEthPortCounterTxPackets);
    }

static uint32 MonCounterGet(AtPrbsEngine self, eBool r2c, uint32 hwType)
    {
    uint32 address = cAf6Reg_emon_pktcnt_Base + MonCounterOffset(self, MonEngineId(self), r2c, hwType);
    uint32 counters = AtPrbsEngineRead(self, address, cAtModulePrbs);

    return counters;
    }

static uint32 AlarmHistoryRead2Clear(AtPrbsEngine self, eBool r2c)
    {
    uint32 address = cAf6Reg_emon_sticky_Base + MonBaseAddress(self, MonEngineId(self));
    uint32 regValue = AtPrbsEngineRead(self, address, cAtModulePrbs);

    if (r2c)
        AtPrbsEngineWrite(self, address, regValue, cAtModulePrbs);

    if (mRegField(regValue, cAf6_emon_sticky_PktErr_))
        return cAtPrbsEngineAlarmTypeError;

    return cAtPrbsEngineAlarmTypeNone;
    }

static uint32 AlarmHistoryGet(AtPrbsEngine self)
    {
    return AlarmHistoryRead2Clear(self, cAtFalse);
    }

static uint32 AlarmHistoryClear(AtPrbsEngine self)
    {
    return AlarmHistoryRead2Clear(self, cAtTrue);
    }

static uint32 CounterGet(AtPrbsEngine self, uint16 counterType)
    {
    if (counterType == cAtPrbsEngineCounterRxFrame)
        return MonCounterGet(self, cAtFalse, 0);

    if (counterType == cAtPrbsEngineCounterTxFrame)
        return (AtPrbsEngineTxIsEnabled(self)) ? GenTxPacketsCounterGet(self, cAtFalse) : 0;

    if (counterType == cAtPrbsEngineCounterRxSync)
        return MonCounterGet(self, cAtFalse, 1);

    if (counterType == cAtPrbsEngineCounterRxErrorFrame)
        return MonCounterGet(self, cAtFalse, 6);

    return 0x0;
    }

static uint32 CounterClear(AtPrbsEngine self, uint16 counterType)
    {
    if (counterType == cAtPrbsEngineCounterRxFrame)
        return MonCounterGet(self, cAtTrue, 0);

    if (counterType == cAtPrbsEngineCounterTxFrame)
        return (AtPrbsEngineTxIsEnabled(self)) ? GenTxPacketsCounterGet(self, cAtTrue) : 0;

    if (counterType == cAtPrbsEngineCounterRxSync)
        return MonCounterGet(self, cAtTrue, 1);

    if (counterType == cAtPrbsEngineCounterRxErrorFrame)
        return MonCounterGet(self, cAtTrue, 6);

    return 0x0;
    }

static eBool CounterIsSupported(AtPrbsEngine self, uint16 counterType)
    {
    AtUnused(self);
    if ((counterType == cAtPrbsEngineCounterTxFrame) ||
        (counterType == cAtPrbsEngineCounterRxFrame) ||
        (counterType == cAtPrbsEngineCounterRxSync)  ||
        (counterType == cAtPrbsEngineCounterRxErrorFrame))
        return cAtTrue;

    return cAtFalse;
    }

static eBool ModeIsSupported(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    AtUnused(self);
    if ((prbsMode == cAtPrbsModePrbs15) ||
        (prbsMode == cAtPrbsModePrbs31) ||
        (prbsMode == cAtPrbsModePrbsSeq)||
        (prbsMode == cAtPrbsModePrbsFixedPattern1Byte))
        return cAtTrue;

    return cAtFalse;
    }

static eBool SeparateTwoDirections(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool CanGenerateToPsn(AtPrbsEngine self)
    {
    /* Can not generate to PSN when ethernet pass through path has not been setup */
    return (BypassPort(self)) ? cAtTrue : cAtFalse;
    }

static uint32 TxEnableMask(AtEthPort bypassPort)
    {
    return cBit0 << AtChannelHwIdGet((AtChannel)bypassPort);
    }

static uint32 TxEnableShift(AtEthPort bypassPort)
    {
    return AtChannelHwIdGet((AtChannel)bypassPort);
    }

static void GenToCoreEngineSet(AtPrbsEngine self)
    {
    AtEthPort bypassPort = BypassPort(self);
    uint32 address = cAf6Reg_upen_ethswc_Base + cMacEthCoreBase + cHwIdGenToCore;
    uint32 regVal = 0;

    /* Write 0 to clear previous status */
    AtPrbsEngineWrite(self, address, 0, cAtModulePrbs);

    /* Do nothing more in case of bypass to 10G port */
    if (!Tha60210061ModuleEthPortIs10G(bypassPort))
        {
        /* Assume bypass port is QSGMII sub-port */
        mFieldIns(&regVal, TxEnableMask(bypassPort), TxEnableShift(bypassPort), 1);
        AtPrbsEngineWrite(self, address, regVal, cAtModulePrbs);
        }
    }

static eAtModulePrbsRet GeneratingSideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    uint32 address, regVal;

    if (side == cAtPrbsSidePsn)
        {
        if (!CanGenerateToPsn(self))
            return cAtErrorNotApplicable;

        GenToCoreEngineSet(self);
        }

    address = cAf6Reg_upen_ethpgen_Base + cMacEthCoreBase;
    regVal = AtPrbsEngineRead(self, address, cAtModulePrbs);

    mRegFieldSet(regVal, cAf6_upen_ethpgen_Gen_Side_, (side == cAtPrbsSideTdm) ? cGenToTxMac : cGenToCore);
    mRegFieldSet(regVal, cAf6_upen_ethpgen_Gen_Overwrite_, (side == cAtPrbsSideTdm) ? 0 : 1);
    AtPrbsEngineWrite(self, address, regVal, cAtModulePrbs);

    if (TxIsEnabled(self))
        NormalTrafficIsolate(self, side);

    return cAtOk;
    }

static eAtPrbsSide GeneratingSideGet(AtPrbsEngine self)
    {
    uint32 address = cAf6Reg_upen_ethpgen_Base + cMacEthCoreBase;
    uint32 regVal = AtPrbsEngineRead(self, address, cAtModulePrbs);

    return ((mRegField(regVal, cAf6_upen_ethpgen_Gen_Side_) == cGenToTxMac) ? cAtPrbsSideTdm : cAtPrbsSidePsn);
    }

static eAtModulePrbsRet MonitoringSideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    uint32 monEngineId, address, regVal;
    AtEthPort gePort = (AtEthPort)AtPrbsEngineChannelGet(self);
    AtEthPort bypassPort = Tha60210061GePortBypassPortGet(gePort);

    if ((side == cAtPrbsSidePsn) && (bypassPort == NULL))
        return cAtErrorNotApplicable;

    monEngineId = MonEngineId(self);
    address = cAf6Reg_upen_ethpmon_Base + cMacEthCoreBase + monEngineId;
    regVal = AtPrbsEngineRead(self, address, cAtModulePrbs);

    if (Tha60210061ModuleEthPortIs10G(bypassPort))
        mRegFieldSet(regVal, cAf6_upen_ethpmon_Mon_Source_ , (side == cAtPrbsSideTdm) ? cMonFromMac : cMonFromPda);
    else
        {
        AtEthPort monPort = (side == cAtPrbsSideTdm) ? gePort : bypassPort;
        mRegFieldSet(regVal, cAf6_upen_ethpmon_In_Mac_ID_ , AtChannelHwIdGet((AtChannel)monPort));
        }

    AtPrbsEngineWrite(self, address, regVal, cAtModulePrbs);
    return cAtOk;
    }

static eAtPrbsSide MonitoringSideGet(AtPrbsEngine self)
    {
    uint32 monEngineId, address, regVal;
    AtEthPort gePort = (AtEthPort)AtPrbsEngineChannelGet(self);
    AtEthPort bypassPort = Tha60210061GePortBypassPortGet(gePort);

    if (bypassPort == NULL)
        return cAtPrbsSideTdm;

    monEngineId = MonEngineId(self);
    address = cAf6Reg_upen_ethpmon_Base + cMacEthCoreBase + monEngineId;
    regVal = AtPrbsEngineRead(self, address, cAtModulePrbs);

    if (Tha60210061ModuleEthPortIs10G(bypassPort))
        return ((mRegField(regVal, cAf6_upen_ethpmon_Mon_Source_) == cMonFromMac) ? cAtPrbsSideTdm : cAtPrbsSidePsn);

    return ((mRegField(regVal, cAf6_upen_ethpmon_In_Mac_ID_) == AtChannelHwIdGet((AtChannel)gePort)) ? cAtPrbsSideTdm : cAtPrbsSidePsn);
    }

static eBool SideIsSupported(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    AtUnused(side);
    return cAtTrue;
    }

static eBool InversionIsSupported(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void Debug(AtPrbsEngine self)
    {
    AtPrintc(cSevNormal, "EngineId = %u\r\n", AtPrbsEngineIdGet(self) + 1);
    AtPrintc(cSevNormal, "Monitor engine ID   : %u\r\n", MonEngineId(self));
    AtPrintc(cSevNormal, "Rx Good frames      : %u\r\n", MonCounterGet(self, cAtTrue, cRxHwFrameGoodCount));
    AtPrintc(cSevNormal, "Rx FCS Error        : %u\r\n", MonCounterGet(self, cAtTrue, cRxHwFrameFcsErrorCount));
    AtPrintc(cSevNormal, "Rx Frame-able Error : %u\r\n", MonCounterGet(self, cAtTrue, cRxHwFrameFrameableErrorCount));
    AtPrintc(cSevNormal, "Rx PCS->MAC Error   : %u\r\n", MonCounterGet(self, cAtTrue, cRxHwFrameReceiveFrErrCount));
    AtPrintc(cSevNormal, "Rx IPG Error        : %u\r\n", MonCounterGet(self, cAtTrue, cRxHwFrameFrIPGErrorCount));
    AtPrintc(cSevNormal, "Rx Data Error       : %u\r\n", MonCounterGet(self, cAtTrue, cRxHwFrameFrDataErrorCount));
    }

static uint32 AlarmGet(AtPrbsEngine self)
    {
    uint32 numRxFrame = AtPrbsEngineCounterGet(self, cAtPrbsEngineCounterRxFrame);
    AtOsalUSleep(10 * 1000);

    if (numRxFrame == AtPrbsEngineCounterGet(self, cAtPrbsEngineCounterRxFrame))
        return cAtPrbsEngineAlarmTypeError | cAtPrbsEngineAlarmTypeLossSync;

    return 0;
    }

static eAtRet MonDefautSet(AtPrbsEngine self)
    {
    uint32 address = cAf6Reg_emon_data_Base + MonBaseAddress(self, MonEngineId(self));
    uint32 regVal = AtPrbsEngineRead(self, address, cAtModulePrbs);

    mRegFieldSet(regVal, cAf6_emon_data_FCSEn_, mBoolToBin(ShouldEnableFcsMon(self)));
    AtPrbsEngineWrite(self, address, regVal, cAtModulePrbs);

    return cAtOk;
    }

static eAtModulePrbsRet Init(AtPrbsEngine self)
    {
    eAtRet ret = m_AtPrbsEngineMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    ret  = mMethodsGet(self)->Enable(self, cAtFalse);
    ret |= mMethodsGet(self)->ModeSet(self, cAtPrbsModePrbs15);
    ret |= mMethodsGet(self)->GeneratingSideSet(self, cAtPrbsSideTdm);
    ret |= mMethodsGet(self)->MonitoringSideSet(self, cAtPrbsSideTdm);
    ret |= MonDefautSet(self);

    return ret;
    }

static eAtModulePrbsRet ErrorForce(AtPrbsEngine self, eBool force)
    {
    AtUnused(self);
    return (force) ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool ErrorIsForced(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool ErrorForcingIsSupported(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModulePrbsRet TxBitOrderSet(AtPrbsEngine self, eAtPrbsBitOrder order)
    {
    AtUnused(self);
    return (order == cAtPrbsBitOrderMsb) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtPrbsBitOrder TxBitOrderGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsBitOrderMsb;
    }

static eAtModulePrbsRet RxBitOrderSet(AtPrbsEngine self, eAtPrbsBitOrder order)
    {
    AtUnused(self);
    return (order == cAtPrbsBitOrderMsb) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtPrbsBitOrder RxBitOrderGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsBitOrderMsb;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60210061EthFramePrbsEngine object = mThis(self);
    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeUInt(txMode);
    }

static void OverrideAtObject(AtPrbsEngine self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, mMethodsGet(self), sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, TypeString);
        mMethodOverride(m_AtPrbsEngineOverride, TxEnable);
        mMethodOverride(m_AtPrbsEngineOverride, TxIsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, RxEnable);
        mMethodOverride(m_AtPrbsEngineOverride, RxIsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, TxFixedPatternSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxFixedPatternGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxFixedPatternSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxFixedPatternGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryClear);
        mMethodOverride(m_AtPrbsEngineOverride, CounterGet);
        mMethodOverride(m_AtPrbsEngineOverride, CounterClear);
        mMethodOverride(m_AtPrbsEngineOverride, CounterIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, ModeIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, SeparateTwoDirections);
        mMethodOverride(m_AtPrbsEngineOverride, GeneratingSideGet);
        mMethodOverride(m_AtPrbsEngineOverride, GeneratingSideSet);
        mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideGet);
        mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideSet);
        mMethodOverride(m_AtPrbsEngineOverride, SideIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, InversionIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, Debug);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmGet);
        mMethodOverride(m_AtPrbsEngineOverride, Init);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorForce);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorIsForced);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorForcingIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, TxBitOrderSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxBitOrderGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxBitOrderSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxBitOrderGet);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtObject(self);
    OverrideAtPrbsEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061EthFramePrbsEngine);
    }

static AtPrbsEngine ObjectInit(AtPrbsEngine self, AtEthPort port, uint32 engineId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPrbsEngineObjectInit(self, (AtChannel)port, engineId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha60210061EthFramePrbsEngineNew(AtEthPort port, uint32 engineId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newEngine, port, engineId);
    }

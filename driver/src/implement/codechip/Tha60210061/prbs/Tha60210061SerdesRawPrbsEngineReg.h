/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive
 * Technologies. The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60210061SerdesRawPrbsEngineReg.h
 *
 * Created Date: March 17, 2017
 *
 * Description : This file contain all constance definitions of  block.
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CCI0061_RD_CSR_XILINX_GT_H_
#define _AF6_REG_AF6CCI0061_RD_CSR_XILINX_GT_H_

/*--------------------------- Define -----------------------------------------*/
#define cPrbs31TxPatternMask                                 cBit4
#define cPrbs31TxPatternShift                                    4

#define cPrbs31RxPatternMask                                 cBit5
#define cPrbs31RxPatternShift                                    5

/*------------------------------------------------------------------------------
Reg Name   : Serdes PRBS GEN Control
Reg Addr   : 0x20
Reg Formula: 0x20 + $hssi_port_id * 0x800 + $hssi_serdes_id * 0x40
    Where  :
           + $hssi_port_id (00-09) : high speed serial interface
           + $hssi_serdes_id (00-01): serdes id of each high speed serial interface}
Reg Desc   :
This register  is used to control PRBS GEN

PRBS pattern description
+ PRBS-07 (1 + X6  + X7)  -> Used to test channels with 8B/10B.
+ PRBS-09 (1 + X5  + X9)  -> ITU-T Recommendation O.150, Section 5.1. PRBS-9 is
one of the recommended test patterns for SFP+.
+ PRBS-15 (1 + X14 + X15) -> ITU-T Recommendation O.150, Section 5.3. PRBS-15 is
often used for jitter measurement because it is the
longest pattern the Agilent DCA-J sampling scope can
+ PRBS-23 (1 + X18 + X23) -> ITU-T Recommendation O.150, Section 5.6. PRBS-23 is
often used for non-8B/10B encoding schemes. It is one
of the recommended test patterns in the SONET specification.
+ PRBS-31 (1 + X28 + X31) -> ITU-T Recommendation O.150, Section 5.8. PRBS-31 is often used for
non-8B/10B encoding schemes. It is a recommended PRBS test pattern for
10 Gigabit Ethernet. See IEEE 802.3ae-2002.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_addr_20_Base                                                                         0x20

/*--------------------------------------
BitField Name: TX_PRBS_FORCE
BitField Type: R/W
BitField Desc: Force PRBS ERROR 0x0: un-force 0x1: force
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_addr_20_TX_PRBS_FORCE_Mask                                                             cBit5
#define cAf6_upen_addr_20_TX_PRBS_FORCE_Shift                                                                5

/*--------------------------------------
BitField Name: TX_ARRV_REG
BitField Type: R/W
BitField Desc: For XFI interface, Transmitter PRBS generator has been controlled
by 0x0: Xinlix PCS (MDIO) 0x1: Arrive Register
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_addr_20_TX_ARRV_REG_Mask                                                               cBit4
#define cAf6_upen_addr_20_TX_ARRV_REG_Shift                                                                  4

/*--------------------------------------
BitField Name: TX_PRBS_SEL
BitField Type: R/W
BitField Desc: Transmitter PRBS generator test pattern control. 0x0: Standard
operation mode (test pattern generation is off) 0x1: PRBS-7 0x2: PRBS-9 0x3:
PRBS-15 0x4: PRBS-23 0x5: PRBS-31 0x8: PCI Express compliance pattern. Only
works with internal data width 20/40/80 bit modes 0x9: Square wave with 2 UI
(alternating 0s/1s) 0xA: Square wave with 16/20/32/44/64/80 UI period (based on
internal data width)
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_upen_addr_20_TX_PRBS_SEL_Mask                                                             cBit3_0
#define cAf6_upen_addr_20_TX_PRBS_SEL_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Serdes TXUSERCLK2 Monitor
Reg Addr   : 0x2e
Reg Formula: 0x2e + $hssi_port_id * 0x800 + $hssi_serdes_id * 0x40 + $high_dword
    Where  :
           + $hssi_port_id (00-09) : high speed serial interface
           + $hssi_serdes_id (00-01): serdes id of each high speed serial interface
           + $high_dword (00-01): high dword }
Reg Desc   :
This register  is used to show TXUSERCLK2 freq

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_addr_2e_Base                                                                         0x2e

/*--------------------------------------
BitField Name: TX_PRBS_FORCE
BitField Type: R/W
BitField Desc: Force PRBS ERROR 0x0: un-force 0x1: force
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_addr_2e_TX_PRBS_FORCE_Mask                                                             cBit5
#define cAf6_upen_addr_2e_TX_PRBS_FORCE_Shift                                                                5

/*--------------------------------------
BitField Name: TX_ARRV_REG
BitField Type: R/W
BitField Desc: For XFI interface, Transmitter PRBS generator has been controlled
by 0x0: Xinlix PCS (MDIO) 0x1: Arrive Register
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_addr_2e_TX_ARRV_REG_Mask                                                               cBit4
#define cAf6_upen_addr_2e_TX_ARRV_REG_Shift                                                                  4

/*--------------------------------------
BitField Name: TX_PRBS_SEL
BitField Type: R/W
BitField Desc: Transmitter PRBS generator test pattern control. 0x0: Standard
operation mode (test pattern generation is off) 0x1: PRBS-7 0x2: PRBS-9 0x3:
PRBS-15 0x4: PRBS-23 0x5: PRBS-31 0x8: PCI Express compliance pattern. Only
works with internal data width 20/40/80 bit modes 0x9: Square wave with 2 UI
(alternating 0s/1s) 0xA: Square wave with 16/20/32/44/64/80 UI period (based on
internal data width)
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_upen_addr_2e_TX_PRBS_SEL_Mask                                                             cBit3_0
#define cAf6_upen_addr_2e_TX_PRBS_SEL_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Serdes PRBS CHECK Control
Reg Addr   : 0x30
Reg Formula: 0x30 + $hssi_port_id * 0x800 + $hssi_serdes_id * 0x40
    Where  :
           + $hssi_port_id (00-09) : high speed serial interface
           + $hssi_serdes_id (00-01): serdes id of each high speed serial interface}
Reg Desc   :
This register  is used to contron PRBS check

PRBS pattern description
+ PRBS-07 (1 + X6  + X7)  -> Used to test channels with 8B/10B.
+ PRBS-09 (1 + X5  + X9)  -> ITU-T Recommendation O.150, Section 5.1. PRBS-9 is
one of the recommended test patterns for SFP+.
+ PRBS-15 (1 + X14 + X15) -> ITU-T Recommendation O.150, Section 5.3. PRBS-15 is
often used for jitter measurement because it is the
longest pattern the Agilent DCA-J sampling scope can
+ PRBS-23 (1 + X18 + X23) -> ITU-T Recommendation O.150, Section 5.6. PRBS-23 is
often used for non-8B/10B encoding schemes. It is one
of the recommended test patterns in the SONET specification.
+ PRBS-31 (1 + X28 + X31) -> ITU-T Recommendation O.150, Section 5.8. PRBS-31 is often used for
non-8B/10B encoding schemes. It is a recommended PRBS test pattern for
10 Gigabit Ethernet. See IEEE 802.3ae-2002.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_addr_30_Base                                                                         0x30

/*--------------------------------------
BitField Name: RXPRBSCNTRESET
BitField Type: R/W
BitField Desc: Reset PRBS Error Counter 0x0: Normal 0x1: Reset Request, should
be write zero after set
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_addr_30_RXPRBSCNTRESET_Mask                                                            cBit5
#define cAf6_upen_addr_30_RXPRBSCNTRESET_Shift                                                               5

/*--------------------------------------
BitField Name: RX_ARRV_REG
BitField Type: R/W
BitField Desc: For XFI interface, Transmitter PRBS generator has been controlled
by 0x0: Xinlix PCS (MDIO) 0x1: Arrive Register
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_addr_30_RX_ARRV_REG_Mask                                                               cBit4
#define cAf6_upen_addr_30_RX_ARRV_REG_Shift                                                                  4

/*--------------------------------------
BitField Name: RX_PRBS_SEL
BitField Type: R/W
BitField Desc: Transmitter PRBS generator test pattern control. After changing
patterns, perform a reset of the RX (GTRXRESET, RXPMARESET, or RXPCSRESET) or a
reset of the PRBS error counter (RXPRBSCNTRESET) such that the RX pattern
checker can attempt to reestablish the link acquired. No checking is done for
non-PRBS patterns. 0x0: Standard operation mode (PRBS check is off) 0x1: PRBS-7
0x2: PRBS-9 0x3: PRBS-15 0x4: PRBS-23 0x5: PRBS-31
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_upen_addr_30_RX_PRBS_SEL_Mask                                                             cBit3_0
#define cAf6_upen_addr_30_RX_PRBS_SEL_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Serdes PRBS CHECK Sticky
Reg Addr   : 0x31
Reg Formula: 0x31 + $hssi_port_id * 0x800 + $hssi_serdes_id 00-01
    Where  :
           + $hssi_port_id (00-09) : high speed serial interface
           + $hssi_serdes_id (00-01): serdes id of each high speed serial interface}
Reg Desc   :
This register  is used to configure mac_map for output mac

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_addr_31_Base                                                                         0x31

/*--------------------------------------
BitField Name: RX_PRBS_LOCKED
BitField Type: W1C
BitField Desc: This sticky status output indicate that the RX PRBS checker has
been error free for RXPRBS_LINKACQ_CNT XCLK cycles after reset. Once asserted
High, RXPRBSLOCKED does not deassert until reset of the RX pattern checker via a
reset of the RX (GTRXRESET, RXPMARESET, or RXPCSRESET in sequential mode) or a
reset of the PRBS error counter (RXPRBSCNTRESET). 0x0: Xinlix PCS (MDIO) 0x1:
Arrive Register
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_addr_31_RX_PRBS_LOCKED_Mask                                                            cBit1
#define cAf6_upen_addr_31_RX_PRBS_LOCKED_Shift                                                               1

/*--------------------------------------
BitField Name: RX_PRBS_ERR
BitField Type: W1C
BitField Desc: This sticky status output indicates that PRBS errors have
occurred.
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_addr_31_RX_PRBS_ERR_Mask                                                               cBit0
#define cAf6_upen_addr_31_RX_PRBS_ERR_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Serdes PRBS CHECK Status
Reg Addr   : 0x32
Reg Formula: 0x32 + $hssi_port_id * 0x800 + $hssi_serdes_id 00-01
    Where  :
           + $hssi_port_id (00-09) : high speed serial interface
           + $hssi_serdes_id (00-01): serdes id of each high speed serial interface}
Reg Desc   :
This register  is used to show PRBS CHECK status

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_addr_32_Base                                                                         0x32

/*--------------------------------------
BitField Name: RX_PRBS_LOCKED_RO
BitField Type: RO
BitField Desc: This non-sticky status output indicate that the RX PRBS checker
has been error free for RXPRBS_LINKACQ_CNT XCLK cycles after reset. Once
asserted High, RXPRBSLOCKED does not deassert until reset of the RX pattern
checker via a reset of the RX (GTRXRESET, RXPMARESET, or RXPCSRESET in
sequential mode) or a reset of the PRBS error counter (RXPRBSCNTRESET).
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_addr_32_RX_PRBS_LOCKED_RO_Mask                                                         cBit1
#define cAf6_upen_addr_32_RX_PRBS_LOCKED_RO_Shift                                                            1

/*--------------------------------------
BitField Name: RO_PRBS_ERR_RO
BitField Type: RO
BitField Desc: This non-sticky status output indicates that PRBS errors have
occurred.
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_addr_32_RO_PRBS_ERR_RO_Mask                                                            cBit0
#define cAf6_upen_addr_32_RO_PRBS_ERR_RO_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Serdes PRBS ERROR Counter DRP
Reg Addr   : 0x00_0000
Reg Formula: 0x00_0000 + $hssi_port_drp_off + $hssi_serdes_id * 0x400 + $gty_en * 100 + 0x15E + $high_word_cnt
    Where  :
           + $hssi_port_drp_off : high speed serial interface DRP address offset, description below
           + $hssi_serdes_id (00-01): serdes id of each high speed interface
           + $gty_en (00-01): 0 -> GTH, 1 -> GTY of each high speed serdes interface , depend on
           + $high_word_cnt (00-01): high word counter, 0 -> counter bits [15:00], and 1 -> counter bit [31:16] }
Reg Desc   :
This register  is used to show PRBS ERROR Counter at DRP
- hssi_port_drp_off = 0xF41000 -> hssi_serdes_id (00-00) : GTYE3 - Multirate SFP port#00
- hssi_port_drp_off = 0xF41400 -> hssi_serdes_id (00-00) : GTYE3 - Multirate SFP port#01
- hssi_port_drp_off = 0xF41800 -> hssi_serdes_id (00-00) : GTYE3 - Multirate SFP port#02
- hssi_port_drp_off = 0xF41C00 -> hssi_serdes_id (00-00) : GTYE3 - Multirate SFP port#03
- hssi_port_drp_off = 0xF2B000 -> hssi_serdes_id (00-01) : GTHE3 - RXAUI port#00
- hssi_port_drp_off = 0xF2D000 -> hssi_serdes_id (00-01) : GTHE3 - RXAUI port#01
- hssi_port_drp_off = 0xF43000 -> hssi_serdes_id (00-00) : GTHE3 - XFI port#00
- hssi_port_drp_off = 0xF44000 -> hssi_serdes_id (00-00) : GTHE3 - XFI port#01
- hssi_port_drp_off = 0xF57000 -> hssi_serdes_id (00-00) : GTHE3 - QSGMII port#00
- hssi_port_drp_off = 0xF57400 -> hssi_serdes_id (00-00) : GTHE3 - QSGMII port#01

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rawprbs_err_cnt_drp_Base                                                         0x000000

/*--------------------------------------
BitField Name: RX_PRBS_CNT_ERR
BitField Type: R/W
BitField Desc: PRBS error counter This counter can be reset by asserting
RXPRBSCNTRESET. When a single bit error in incoming data occurs, this counter
increments by 1. Single bit errors are counted thus when multiple bit errors
occur in incoming data. The counter increments by the actual number of bit
errors. Counting begins after RXPRBSLOCKED is asserted High. The counter
saturates at 32'hFFFFFFFF. This error counter can only be accessed via the DRP
interface. Because the DRP only outputs 16 bits of data per operation, two DRP
transactions must be completed to read out the complete 32-bit value. To
properly read out the error counter, read out the lower 16 bits at address 0x25E
- GTYE3 (0x15E - for GTHE3) first, followed by the upper 16 bits at address
0x25F - GTYE3 (0x15F - for GTHE3)
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_rawprbs_err_cnt_drp_RX_PRBS_CNT_ERR_Mask                                            cBit15_0
#define cAf6_upen_rawprbs_err_cnt_drp_RX_PRBS_CNT_ERR_Shift                                                  0

#endif /* _AF6_REG_AF6CCI0061_RD_CSR_XILINX_GT_H_ */

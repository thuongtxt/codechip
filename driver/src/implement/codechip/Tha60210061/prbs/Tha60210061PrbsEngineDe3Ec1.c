/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60210061PrbsEngineDe3Ec1.c
 *
 * Created Date: Sep 5, 2017
 *
 * Description : De3 Prbs engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/prbs/ThaPrbsEngineDe3Internal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061PrbsEngineDe3Ec1
    {
    tThaPrbsEngineDe3 super;
    }tTha60210061PrbsEngineDe3Ec1;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPrbsEngineMethods            m_ThaPrbsEngineOverride;

/* Save super implementation */
static const tThaPrbsEngineMethods  *m_ThaPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ChannelId(ThaPrbsEngine self)
    {
    uint8 slice24, hwSts24, slice48;
    AtPdhChannel de3 = (AtPdhChannel)AtPrbsEngineChannelGet((AtPrbsEngine)self);

    ThaPdhChannelHwIdGet(de3, cAtModulePrbs, &slice24, &hwSts24);
    slice48 = slice24 / 2;
    return (uint32)(slice24 << 5) | (uint32)(hwSts24 << 6) | (uint32)(slice48 << 11);
    }

static void OverrideThaPrbsEngine(ThaPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPrbsEngineOverride, m_ThaPrbsEngineMethods, sizeof(m_ThaPrbsEngineOverride));

        mMethodOverride(m_ThaPrbsEngineOverride, ChannelId);
        }

    mMethodsSet(self, &m_ThaPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideThaPrbsEngine((ThaPrbsEngine)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061PrbsEngineDe3Ec1);
    }

static AtPrbsEngine ObjectInit(AtPrbsEngine self, AtPdhDe3 de3, uint32 engineId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPrbsEngineDe3ObjectInit(self, de3, engineId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha60210061PrbsEngineDe3Ec1New(AtPdhDe3 de3, uint32 engineId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newEngine, de3, engineId);
    }

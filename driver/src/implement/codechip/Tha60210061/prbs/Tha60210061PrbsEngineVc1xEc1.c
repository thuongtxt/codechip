/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60210061PrbsEngineVc1xEc1.c
 *
 * Created Date: Aug 25, 2017
 *
 * Description : VC1x PRBS Engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/prbs/ThaPrbsEngineVc1xInternal.h"
#include "../../../default/ocn/ThaModuleOcn.h"
#include "Tha60210061ModulePrbs.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061PrbsEngineVc1xEc1
    {
    tThaPrbsEngineVc1x super;
    }tTha60210061PrbsEngineVc1xEc1;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPrbsEngineMethods       m_ThaPrbsEngineOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ChannelId(ThaPrbsEngine self)
    {
    AtSdhChannel vc = (AtSdhChannel)AtPrbsEngineChannelGet((AtPrbsEngine)self);
    uint8 slice24, hwSts24;
    uint8 vtgId = AtSdhChannelTug2Get(vc);
    uint8 vtId = AtSdhChannelTu1xGet(vc);

    ThaSdhChannel2HwMasterStsId(vc, cAtModulePrbs, &slice24, &hwSts24);

    return Tha60210061PrbsEngineVc1xEc1ChannelId((AtPrbsEngine)self, slice24, hwSts24, vtgId, vtId);
    }

static void OverrideThaPrbsEngine(ThaPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPrbsEngineOverride, mMethodsGet(self), sizeof(m_ThaPrbsEngineOverride));

        mMethodOverride(m_ThaPrbsEngineOverride, ChannelId);
        }

    mMethodsSet(self, &m_ThaPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideThaPrbsEngine((ThaPrbsEngine)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061PrbsEngineVc1xEc1);
    }

static AtPrbsEngine ObjectInit(AtPrbsEngine self, AtSdhChannel vc1x, uint32 engineId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPrbsEngineVc1xObjectInit(self, vc1x, engineId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha60210061PrbsEngineVc1xEc1New(AtSdhChannel vc1x, uint32 engineId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newEngine, vc1x, engineId);
    }

uint32 Tha60210061PrbsEngineVc1xEc1ChannelId(AtPrbsEngine self, uint8 slice24, uint8 hwSts24, uint8 vtgId, uint8 vtId)
    {
    ThaModulePrbs module = ThaPrbsEngineModuleGet(self);

    /* Channel ID: 14 bit
     * Bit 13->11: slice48
     * Bit 10->6 : sts24
     * Bit 5     : slice24
     * bit4_0    : vtgId
     *  */
    uint32 vtgIdOffset = (ThaModulePrbsTdmChannelIdVtgFactor(module) * vtgId) +
                         (ThaModulePrbsTdmChannelIdVtFactor(module) * vtId);

    uint8 slice48 = slice24 / 2;
    return (vtgIdOffset & cBit4_0) | (uint32)(slice24 << 5) | (uint32)(hwSts24 << 6) | (uint32)(slice48 << 11);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60210061XfierdesRawPrbsEngine.c
 *
 * Created Date: Apr 3, 2017
 *
 * Description : Xfi Serdes Raw Prbs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../physical/Tha60210061SerdesInternal.h"
#include "../physical/Tha60210061SerdesManager.h"
#include "../physicalV2/Tha60210061SerdesControlV2Reg.h"
#include "AtMdio.h"
#include "Tha60210061ModulePrbs.h"
#include "Tha60210061SerdesRawPrbsEngineReg.h"
#include "Tha60210061SerdesPrbsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210061SgmiiSerdesRawPrbsEngine*)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061XfiSerdesRawPrbsEngine
    {
    tTha60210061GthSerdesRawPrbsEngine super;
    }tTha60210061XfiSerdesRawPrbsEngine;

/*--------------------------- Global variables -------------------------------*/

static uint8 m_methodsInit = 0;

/* Override */
static tAtPrbsEngineMethods  m_AtPrbsEngineOverride;

/* Save super implementation */
static const tAtPrbsEngineMethods *m_AtPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ConfigureOffset(AtPrbsEngine self)
    {
    return cSerdesTuningV2BaseAddress + AtSerdesControllerIdGet(mThis(self)->serdesController) * 0x800 + cHssi_serdes_id * 0x40;
    }

static uint32 DrpBaseAddress(AtPrbsEngine self)
    {
    return AtSerdesControllerDrpBaseAddress(mThis(self)->serdesController);
    }

static void MdioSelect(AtPrbsEngine self, AtMdio mdio)
    {
    AtSerdesController controller = mThis(self)->serdesController;
    AtMdioPortSelect(mdio, AtSerdesControllerHwIdGet(controller));
    }

static eAtModulePrbsRet TxEnable(AtPrbsEngine self, eBool enable)
    {
    uint32 address, regVal;
    AtMdio mdio = AtSerdesControllerMdio(mThis(self)->serdesController);

    /* Enable Reset TX PCS */
    address = cAf6Reg_upen_addr_04_Base + ConfigureOffset(self);
    regVal = AtPrbsEngineRead(self, address, cAtModulePrbs);
    mRegFieldSet(regVal, cAf6_upen_addr_04_GTSUBXRESET_REQ_, 0x2);
    AtPrbsEngineWrite(self, address, regVal, cAtModulePrbs);

    /* Enable TX Gear Box and TX Buff */
    address = cTxGearBoxEnable + DrpBaseAddress(self);
    regVal = AtPrbsEngineRead(self, address, cAtModulePrbs);
    mRegFieldSet(regVal, cTxGearBox_en_, enable ? 0x0 : 0x1);
    mRegFieldSet(regVal, cTxBuf_en_, enable ? 0x1 : 0x0);
    mRegFieldSet(regVal, cTX_RxDetect_Ref_, 0x4);
    AtPrbsEngineWrite(self, address, regVal, cAtModulePrbs);

    /* If enable, using TXOUTCLKPMA
     * If disable, using TXPROGDIVCLK */
    address = cAf6Reg_upen_addr_09_Base + ConfigureOffset(self);
    regVal = AtPrbsEngineRead(self, address, cAtModulePrbs);
    mRegFieldSet(regVal, cAf6_upen_addr_09_TXOUTCLKSEL_, enable ? 0x2 : 0x5);
    AtPrbsEngineWrite(self, address, regVal, cAtModulePrbs);

    m_AtPrbsEngineMethods->TxEnable(self, enable);

    /* Disable Reset TX PCS */
    address = cAf6Reg_upen_addr_04_Base + ConfigureOffset(self);
    regVal = AtPrbsEngineRead(self, address, cAtModulePrbs);
    mRegFieldSet(regVal, cAf6_upen_addr_04_GTSUBXRESET_REQ_, 0x0);
    AtPrbsEngineWrite(self, address, regVal, cAtModulePrbs);

    /* Enable Test pattern */
    MdioSelect(self, mdio);
    regVal = AtMdioRead(mdio, cMdioRegSerdesControl1Register);
    mRegFieldSet(regVal, cPrbs31TxPattern, enable ? 1 : 0);
    AtMdioWrite(mdio, cMdioRegSerdesControl1Register, regVal);

    return cAtOk;
    }

static eAtModulePrbsRet RxEnable(AtPrbsEngine self, eBool enable)
    {
    uint32 address, regVal;
    AtMdio mdio = AtSerdesControllerMdio(mThis(self)->serdesController);

    /* Enable RX pattern */
    MdioSelect(self, mdio);
    regVal = AtMdioRead(mdio, cMdioRegSerdesControl1Register);
    mRegFieldSet(regVal, cPrbs31RxPattern, enable ? 0x1 : 0);
    AtMdioWrite(mdio, cMdioRegSerdesControl1Register, regVal);

    address = cAf6Reg_upen_addr_09_Base + ConfigureOffset(self);
    regVal = AtPrbsEngineRead(self, address, cAtModulePrbs);
    mRegFieldSet(regVal, cAf6_upen_addr_09_RXOUTCLKSEL_, 0x5);
    AtPrbsEngineWrite(self, address, regVal, cAtModulePrbs);

    return m_AtPrbsEngineMethods->RxEnable(self, enable);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, m_AtPrbsEngineMethods, sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, TxEnable);
        mMethodOverride(m_AtPrbsEngineOverride, RxEnable);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtPrbsEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061XfiSerdesRawPrbsEngine);
    }

static AtPrbsEngine ObjectInit(AtPrbsEngine self, AtSerdesController serdesController)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210061GthSerdesRawPrbsEngineObjectInit(self, serdesController) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha60210061XfiSerdesRawPrbsEngineNew(AtSerdesController serdesController)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine prbsEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (prbsEngine == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(prbsEngine, serdesController);
    }


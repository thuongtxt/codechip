/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60210061SerdesEthFramePrbsEngine.c
 *
 * Created Date: Jan 13, 2017
 *
 * Description : This PRBS engine wraps Ethernet port PRBS engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/common/AtChannelInternal.h"
#include "../../../../generic/man/AtModuleInternal.h"
#include "../eth/Tha60210061EthPassThroughReg.h"
#include "../eth/Tha60210061ModuleEth.h"
#include "Tha60210061ModulePrbs.h"
#include "Tha60210061SerdesPrbsEngineInternal.h"
#include "Tha60210061EthFramePrbsEngineReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cRxHwFrameGoodCount           1
#define cRxHwFrameFcsErrorCount       2
#define cRxHwFrameFrameableErrorCount 3
#define cRxHwFrameReceiveFrErrCount   4
#define cRxHwFrameFrIPGErrorCount     5
#define cRxHwFrameFrDataErrorCount    6

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210061SerdesEthFramePrbsEngine)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtPrbsEngineMethods m_AtPrbsEngineOverride;

/* Save super implementation */
static const tAtObjectMethods     *m_AtObjectMethods     = NULL;
static const tAtPrbsEngineMethods *m_AtPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *TypeString(AtPrbsEngine self)
    {
    AtUnused(self);
    return "Ethernet Frame PRBS";
    }

static uint32 GenBaseAddress(void)
    {
    return cEthFrameGenBaseAddress;
    }

static uint32 MonEngineId(AtPrbsEngine self)
    {
    return AtChannelIdGet(AtPrbsEngineChannelGet(self));
    }

static uint32 MonBaseAddress(AtPrbsEngine self, uint32 monId)
    {
    AtUnused(self);
    return 0x04D000 + monId * 0x100;
    }

static uint8 SwMode2HwMode(eAtPrbsMode mode)
    {
    if (mode == cAtPrbsModePrbs31) return 0xF;
    if (mode == cAtPrbsModePrbs15) return 0xA;

    return 0;
    }

static eAtPrbsMode HwMode2SwMode(uint32 mode)
    {
    if (mode == 0xF) return cAtPrbsModePrbs31;
    if (mode == 0xA) return cAtPrbsModePrbs15;

    return cAtPrbsModeInvalid;
    }

static eBool ShouldEnableFcsMon(AtPrbsEngine self)
    {
    AtEthPort port = (AtEthPort)AtPrbsEngineChannelGet(self);
    if (Tha60210061ModuleEthPortIs10G(port))
        return cAtTrue;

    return cAtFalse;
    }

static eBool PortIsQsgmiiPort(AtEthPort port)
    {
    return Tha60210061ModuleEthPortIsQsgmii(port);
    }

static uint32 TxEnableMask(AtPrbsEngine self)
    {
    AtEthPort port = (AtEthPort)AtPrbsEngineChannelGet(self);
    if (PortIsQsgmiiPort(port))
        return cBit11_8;

    return cBit0 << AtChannelHwIdGet((AtChannel)port);
    }

static uint32 TxEnableShift(AtPrbsEngine self)
    {
    AtEthPort port = (AtEthPort)AtPrbsEngineChannelGet(self);
    if (PortIsQsgmiiPort(port))
        return 8;

    return AtChannelHwIdGet((AtChannel)port);
    }

static uint32 TxEnableValue(AtPrbsEngine self)
    {
    AtEthPort port = (AtEthPort)AtPrbsEngineChannelGet(self);
    if (PortIsQsgmiiPort(port))
        return cBit3_0;

    return 1;
    }

static AtModulePrbs ModulePrbs(AtPrbsEngine self)
    {
    AtChannel port = (AtChannel)AtPrbsEngineChannelGet(self);
    return (AtModulePrbs)AtDeviceModuleGet(AtChannelDeviceGet(port), cAtModulePrbs);
    }

static eAtRet TxEnablePrepare(AtPrbsEngine self)
    {
    AtChannel port = AtPrbsEngineChannelGet(self);
    return AtEthPortTxSerdesSet((AtEthPort)port, mThis(self)->serdesController);
    }

static eAtModulePrbsRet TxEnable(AtPrbsEngine self, eBool enable)
    {
    uint32 address, regVal;
    AtModulePrbs module = ModulePrbs(self);

    if (enable)
        {
        TxEnablePrepare(self);
        Tha60210061DiagEthFramePrbsGenEngineEnable(module, enable);
        }

    address = cAf6Reg_upen_ethswc_Base + cMacEthCoreBase + cHwIdGenToCore;
    regVal = AtPrbsEngineRead(self, address, cAtModulePrbs);
    mFieldIns(&regVal, TxEnableMask(self), TxEnableShift(self), (enable) ? TxEnableValue(self) : 0);

    AtPrbsEngineWrite(self, address, regVal, cAtModulePrbs);
    return cAtOk;
    }

static eBool SerdesIsSelected(AtPrbsEngine self)
    {
    AtEthPort port = (AtEthPort)AtPrbsEngineChannelGet(self);
    if (AtEthPortRxSelectedSerdes(port) == mThis(self)->serdesController)
        return cAtTrue;

    return cAtFalse;
    }

static eBool TxIsEnabled(AtPrbsEngine self)
    {
    if (SerdesIsSelected(self))
        {
        uint32 address = cAf6Reg_upen_ethswc_Base + cMacEthCoreBase + cHwIdGenToCore;
        uint32 regVal = AtPrbsEngineRead(self, address, cAtModulePrbs);
        uint8 bitVal;

        mFieldGet(regVal, TxEnableMask(self), TxEnableShift(self), uint8, &bitVal);
        return (bitVal != 0) ? cAtTrue : cAtFalse;
        }

    return cAtFalse;
    }

static uint32 MacId(AtPrbsEngine self, uint32 monId)
    {
    AtEthPort port = (AtEthPort)AtPrbsEngineChannelGet(self);
    if (PortIsQsgmiiPort(port))
        return 8 + monId;

    return AtChannelHwIdGet((AtChannel)port);
    }

static eAtRet PortSerdesSelect(AtPrbsEngine self)
    {
    AtEthPort port = (AtEthPort)AtPrbsEngineChannelGet(self);
    return AtEthPortRxSerdesSelect(port, mThis(self)->serdesController);
    }

static uint8 NumMonEngine(AtPrbsEngine self)
    {
    AtEthPort port = (AtEthPort)AtPrbsEngineChannelGet(self);
    if (PortIsQsgmiiPort(port))
        return 4;

    return 1;
    }

static eAtModulePrbsRet RxEnable(AtPrbsEngine self, eBool enable)
    {
    uint8 mon_i;

    if (enable)
        {
        eAtRet ret = PortSerdesSelect(self);
        if (ret != cAtOk)
            return ret;
        }

    for (mon_i = 0; mon_i < NumMonEngine(self); mon_i++)
        {
        uint32 monEngineId = MonEngineId(self);
        uint32 address = cAf6Reg_upen_ethpmon_Base + cMacEthCoreBase + monEngineId + mon_i;
        uint32 regVal = AtPrbsEngineRead(self, address, cAtModulePrbs);

        mRegFieldSet(regVal,cAf6_upen_ethpmon_In_Mac_En_ , mBoolToBin(enable));
        mRegFieldSet(regVal,cAf6_upen_ethpmon_In_Mac_ID_ , MacId(self, mon_i));

        AtPrbsEngineWrite(self, address, regVal, cAtModulePrbs);
        }

    return cAtOk;
    }

static eBool RxIsEnabled(AtPrbsEngine self)
    {
    if (SerdesIsSelected(self))
        {
        uint32 monEngineId = AtChannelIdGet(AtPrbsEngineChannelGet(self));
        uint32 address = cAf6Reg_upen_ethpmon_Base + cMacEthCoreBase + monEngineId;
        uint32 regVal = AtPrbsEngineRead(self, address, cAtModulePrbs);

        return (regVal & cAf6_upen_ethpmon_In_Mac_En_Mask) ? cAtTrue : cAtFalse;
        }

    return cAtFalse;
    }

static eAtModulePrbsRet TxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    uint32 address, regVal, hwMode;

    if (!AtPrbsEngineModeIsSupported(self, prbsMode))
        return cAtErrorModeNotSupport;

    address = cAf6Reg_egen_modopt_Base + GenBaseAddress();
    regVal = AtPrbsEngineRead(self, address, cAtModulePrbs);
    hwMode = SwMode2HwMode(prbsMode);

    mRegFieldSet(regVal, cAf6_egen_modopt_DataMod_, hwMode);
    AtPrbsEngineWrite(self, address, regVal, cAtModulePrbs);

    return cAtOk;
    }

static eAtPrbsMode TxModeGet(AtPrbsEngine self)
    {
    uint32 hwMode;
    uint32 address = cAf6Reg_egen_modopt_Base + GenBaseAddress();
    uint32 regVal = AtPrbsEngineRead(self, address, cAtModulePrbs);

    hwMode = mRegField(regVal, cAf6_egen_modopt_DataMod_);
    return HwMode2SwMode(hwMode);
    }

static eAtModulePrbsRet RxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    uint32 address, regVal, hwMode;
    uint32 mon_i, monEngineId;

    if (!AtPrbsEngineModeIsSupported(self, prbsMode))
        return cAtErrorModeNotSupport;

    monEngineId = MonEngineId(self);
    for (mon_i = 0; mon_i < NumMonEngine(self); mon_i++)
        {
        address = cAf6Reg_emon_data_Base + MonBaseAddress(self, monEngineId + mon_i);
        regVal = AtPrbsEngineRead(self, address, cAtModulePrbs);
        hwMode = SwMode2HwMode(prbsMode);

        mRegFieldSet(regVal, cAf6_emon_data_DataMod_, hwMode);
        AtPrbsEngineWrite(self, address, regVal, cAtModulePrbs);
        }

    return cAtOk;
    }

static eAtPrbsMode RxModeGet(AtPrbsEngine self)
    {
    uint32 hwMode;
    uint32 addressEthMon = cAf6Reg_emon_data_Base + MonBaseAddress(self, MonEngineId(self));
    uint32 regValEthMon = AtPrbsEngineRead(self, addressEthMon, cAtModulePrbs);

    hwMode = mRegField(regValEthMon, cAf6_emon_data_DataMod_);
    return HwMode2SwMode(hwMode);
    }

static eAtModulePrbsRet TxFixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    uint32 addressEthGen = cAf6Reg_egen_modopt_Base + GenBaseAddress();
    uint32 regValEthGen = AtPrbsEngineRead(self, addressEthGen, cAtModulePrbs);
    uint32 hwMode = mRegField(regValEthGen, cAf6_egen_modopt_DataMod_);

    if (HwMode2SwMode(hwMode) != cAtPrbsModePrbsFixedPattern1Byte)
        return cAtErrorNotApplicable;

    mRegFieldSet(regValEthGen, cAf6_egen_modopt_DataFix_, fixedPattern);
    AtPrbsEngineWrite(self, addressEthGen, regValEthGen, cAtModulePrbs);

    return cAtOk;
    }

static uint32 TxFixedPatternGet(AtPrbsEngine self)
    {
    uint32 addressEthGen = cAf6Reg_egen_modopt_Base + GenBaseAddress();
    uint32 regValEthGen = AtPrbsEngineRead(self, addressEthGen, cAtModulePrbs);

    return mRegField(regValEthGen, cAf6_egen_modopt_DataFix_);
    }

static eAtModulePrbsRet RxFixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    uint32 mon_i;
    uint32 monEngineId = MonEngineId(self);

    for (mon_i = 0; mon_i < NumMonEngine(self); mon_i++)
        {
        uint32 addressEthMon = cAf6Reg_emon_data_Base + MonBaseAddress(self, monEngineId + mon_i);
        uint32 regValEthMon = AtPrbsEngineRead(self, addressEthMon, cAtModulePrbs);
        uint32 hwMode = mRegField(regValEthMon, cAf6_emon_data_DataMod_);

        if (HwMode2SwMode(hwMode) != cAtPrbsModePrbsFixedPattern1Byte)
           return cAtErrorNotApplicable;

        mRegFieldSet(regValEthMon, cAf6_emon_data_DataFix_, fixedPattern);
        AtPrbsEngineWrite(self, addressEthMon, regValEthMon, cAtModulePrbs);
        }

    return cAtOk;
    }

static uint32 RxFixedPatternGet(AtPrbsEngine self)
    {
    uint32 addressEthMon = cAf6Reg_emon_data_Base + MonBaseAddress(self, MonEngineId(self));
    uint32 regValEthMon = AtPrbsEngineRead(self, addressEthMon, cAtModulePrbs);

    return mRegField(regValEthMon, cAf6_emon_data_DataFix_);
    }

static uint32 MonCounterOffset(AtPrbsEngine self, uint32 monId, eBool r2c, uint32 type)
    {
    if (r2c)
        return (MonBaseAddress(self, monId) + 0x10 + type);

    return (MonBaseAddress(self, monId) + type);
    }

static uint32 GenCounterGet(AtPrbsEngine self, eBool r2c, uint32 hwType)
    {
    AtEthPort port = (AtEthPort)AtPrbsEngineChannelGet(self);
    AtUnused(hwType);
    if (r2c)
        return AtChannelCounterClear((AtChannel)port, cAtEthPortCounterTxPackets);

    return AtChannelCounterGet((AtChannel)port, cAtEthPortCounterTxPackets);
    }

static uint32 MonCounterGet(AtPrbsEngine self, eBool r2c, uint32 hwType)
    {
    uint32 mon_i;
    uint32 monEngineId = MonEngineId(self);
    uint32 counters = 0;

    for (mon_i = 0; mon_i < NumMonEngine(self); mon_i++)
        {
        uint32 address = cAf6Reg_emon_pktcnt_Base + MonCounterOffset(self, monEngineId + mon_i, r2c, hwType);
        counters += AtPrbsEngineRead(self, address, cAtModulePrbs);
        }

    return counters;
    }

static uint32 AlarmHistoryRead2Clear(AtPrbsEngine self, eBool r2c)
    {
    uint32 mon_i;
    uint32 monEngineId = MonEngineId(self);
    uint32 alarm = cAtPrbsEngineAlarmTypeNone;

    for (mon_i = 0; mon_i < NumMonEngine(self); mon_i++)
        {
        uint32 address = cAf6Reg_emon_sticky_Base + MonBaseAddress(self, monEngineId + mon_i);
        uint32 regValue = AtPrbsEngineRead(self, address, cAtModulePrbs);

        if (r2c)
            AtPrbsEngineWrite(self, address, regValue, cAtModulePrbs);

        if (mRegField(regValue, cAf6_emon_sticky_PktErr_))
            alarm = cAtPrbsEngineAlarmTypeError;
        }

    return alarm;
    }

static uint32 AlarmHistoryGet(AtPrbsEngine self)
    {
    return AlarmHistoryRead2Clear(self, cAtFalse);
    }

static uint32 AlarmHistoryClear(AtPrbsEngine self)
    {
    return AlarmHistoryRead2Clear(self, cAtTrue);
    }

static uint32 CounterGet(AtPrbsEngine self, uint16 counterType)
    {
    if (counterType == cAtPrbsEngineCounterRxFrame)
        return MonCounterGet(self, cAtFalse, 0);

    if (counterType == cAtPrbsEngineCounterTxFrame)
        return (AtPrbsEngineTxIsEnabled(self)) ? GenCounterGet(self, cAtFalse, 0) : 0;

    if (counterType == cAtPrbsEngineCounterRxSync)
        return MonCounterGet(self, cAtFalse, 1);

    if (counterType == cAtPrbsEngineCounterRxErrorFrame)
        return MonCounterGet(self, cAtFalse, 6);

    return 0x0;
    }

static uint32 CounterClear(AtPrbsEngine self, uint16 counterType)
    {
    if (counterType == cAtPrbsEngineCounterRxFrame)
        return MonCounterGet(self, cAtTrue, 0);

    if (counterType == cAtPrbsEngineCounterTxFrame)
        return (AtPrbsEngineTxIsEnabled(self)) ? GenCounterGet(self, cAtTrue, 0) : 0;

    if (counterType == cAtPrbsEngineCounterRxSync)
        return MonCounterGet(self, cAtTrue, 1);

    if (counterType == cAtPrbsEngineCounterRxErrorFrame)
        return MonCounterGet(self, cAtTrue, 6);

    return 0x0;
    }

static eBool CounterIsSupported(AtPrbsEngine self, uint16 counterType)
    {
    AtUnused(self);
    if ((counterType == cAtPrbsEngineCounterTxFrame) ||
        (counterType == cAtPrbsEngineCounterRxFrame) ||
        (counterType == cAtPrbsEngineCounterRxSync)  ||
        (counterType == cAtPrbsEngineCounterRxErrorFrame))
        return cAtTrue;

    return cAtFalse;
    }

static eBool ModeIsSupported(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    AtUnused(self);
    if ((prbsMode == cAtPrbsModePrbs15) || (prbsMode == cAtPrbsModePrbs31))
        return cAtTrue;

    return cAtFalse;
    }

static eBool SeparateTwoDirections(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtPrbsSide GeneratingSideGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsSidePsn;
    }

static eAtPrbsSide MonitoringSideGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsSidePsn;
    }

static eAtModulePrbsRet GeneratingSideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return ((side == cAtPrbsSidePsn) ? cAtOk : cAtErrorModeNotSupport);
    }

static eAtModulePrbsRet MonitoringSideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return ((side == cAtPrbsSidePsn) ? cAtOk : cAtErrorModeNotSupport);
    }

static eBool SideIsSupported(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return (side == cAtPrbsSidePsn) ? cAtTrue : cAtFalse;
    }

static eBool InversionIsSupported(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void Debug(AtPrbsEngine self)
    {
    AtPrintc(cSevNormal, "EngineId = %u\r\n", AtPrbsEngineIdGet(self) + 1);
    AtPrintc(cSevNormal, "Monitor engine ID   : %u\r\n", MonEngineId(self));
    AtPrintc(cSevNormal, "Rx Good frames      : %u\r\n", MonCounterGet(self, cAtTrue, cRxHwFrameGoodCount));
    AtPrintc(cSevNormal, "Rx FCS Error        : %u\r\n", MonCounterGet(self, cAtTrue, cRxHwFrameFcsErrorCount));
    AtPrintc(cSevNormal, "Rx Frame-able Error : %u\r\n", MonCounterGet(self, cAtTrue, cRxHwFrameFrameableErrorCount));
    AtPrintc(cSevNormal, "Rx PCS->MAC Error   : %u\r\n", MonCounterGet(self, cAtTrue, cRxHwFrameReceiveFrErrCount));
    AtPrintc(cSevNormal, "Rx IPG Error        : %u\r\n", MonCounterGet(self, cAtTrue, cRxHwFrameFrIPGErrorCount));
    AtPrintc(cSevNormal, "Rx Data Error       : %u\r\n", MonCounterGet(self, cAtTrue, cRxHwFrameFrDataErrorCount));
    }

static uint32 AlarmGet(AtPrbsEngine self)
    {
    uint32 numRxFrame = AtPrbsEngineCounterGet(self, cAtPrbsEngineCounterRxFrame);
    AtOsalUSleep(10 * 1000);

    if (numRxFrame == AtPrbsEngineCounterGet(self, cAtPrbsEngineCounterRxFrame))
        return cAtPrbsEngineAlarmTypeError | cAtPrbsEngineAlarmTypeLossSync;

    return 0;
    }

static eAtRet MonDefautSet(AtPrbsEngine self)
    {
    uint32 mon_i;
    uint32 monEngineId = MonEngineId(self);

    for (mon_i = 0; mon_i < NumMonEngine(self); mon_i++)
        {
        uint32 address = cAf6Reg_emon_data_Base + MonBaseAddress(self, monEngineId + mon_i);
        uint32 regVal = AtPrbsEngineRead(self, address, cAtModulePrbs);
        const uint8 cPrbs31 = 0xf;

        mRegFieldSet(regVal, cAf6_emon_data_DataMod_, cPrbs31);
        mRegFieldSet(regVal, cAf6_emon_data_FCSEn_, mBoolToBin(ShouldEnableFcsMon(self)));

        AtPrbsEngineWrite(self, address, regVal, cAtModulePrbs);
        }

    return cAtOk;
    }

static eAtModulePrbsRet Init(AtPrbsEngine self)
    {
    eAtRet ret = m_AtPrbsEngineMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    ret = AtPrbsEngineEnable(self, cAtFalse);
    ret |= MonDefautSet(self);

    return ret;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60210061SerdesEthFramePrbsEngine object = (Tha60210061SerdesEthFramePrbsEngine)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObjectDescription(serdesController);
    }

static uint32 MacIdForRefClock(void)
    {
    return 8;
    }

static eBool CommonGenEngineIsEnabled(AtModulePrbs modulePrbs)
    {
    uint32 address = cAf6Reg_upen_ethpgen_Base + cMacEthCoreBase;
    uint32 regVal = mModuleHwRead(modulePrbs, address);

    return ((regVal & cAf6_upen_ethpgen_Gen_En_Mask) ? cAtTrue : cAtFalse);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, mMethodsGet(self), sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, TypeString);
        mMethodOverride(m_AtPrbsEngineOverride, TxEnable);
        mMethodOverride(m_AtPrbsEngineOverride, TxIsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, RxEnable);
        mMethodOverride(m_AtPrbsEngineOverride, RxIsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, TxFixedPatternSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxFixedPatternGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxFixedPatternSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxFixedPatternGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryClear);
        mMethodOverride(m_AtPrbsEngineOverride, CounterGet);
        mMethodOverride(m_AtPrbsEngineOverride, CounterClear);
        mMethodOverride(m_AtPrbsEngineOverride, CounterIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, ModeIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, SeparateTwoDirections);
        mMethodOverride(m_AtPrbsEngineOverride, GeneratingSideGet);
        mMethodOverride(m_AtPrbsEngineOverride, GeneratingSideSet);
        mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideGet);
        mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideSet);
        mMethodOverride(m_AtPrbsEngineOverride, SideIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, InversionIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, Debug);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmGet);
        mMethodOverride(m_AtPrbsEngineOverride, Init);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void OverrideAtObject(AtPrbsEngine self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtObject(self);
    OverrideAtPrbsEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061SerdesEthFramePrbsEngine);
    }

static AtPrbsEngine ObjectInit(AtPrbsEngine self, AtSerdesController serdesController)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPrbsEngineObjectInit(self,
                               AtSerdesControllerPhysicalPortGet(serdesController),
                               AtSerdesControllerIdGet(serdesController)) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;
    mThis(self)->serdesController = serdesController;

    return self;
    }

AtPrbsEngine Tha60210061SerdesEthFramePrbsEngineNew(AtSerdesController serdesController)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newEngine, serdesController);
    }

eAtRet Tha60210061DiagEthFramePrbsGenEngineEnable(AtModulePrbs modulePrbs, eBool enable)
    {
    uint32 address = cAf6Reg_upen_ethpgen_Base + cMacEthCoreBase;
    uint32 regVal = mModuleHwRead(modulePrbs, address);
    const uint8 cGenBeforeBuffer = 1;

    if (CommonGenEngineIsEnabled(modulePrbs) == enable)
        return cAtOk;

    if (enable)
        Tha60210061ModulePrbsCommonGenEngineDefautSet(modulePrbs);

    /* Enable in core block */
    mRegFieldSet(regVal, cAf6_upen_ethpgen_Gen_En_, mBoolToBin(enable));
    mRegFieldSet(regVal, cAf6_upen_ethpgen_Gen_Side_, cGenBeforeBuffer);
    mRegFieldSet(regVal, cAf6_upen_ethpgen_Gen_Mac_ID_, MacIdForRefClock());
    mModuleHwWrite(modulePrbs, address, regVal);

    /* Enable in gen block */
    address = cAf6Reg_egen_active_Base + GenBaseAddress();
    regVal = (enable) ? cAf6_egen_active_Restart_Mask | cAf6_egen_active_GenEn_Mask : 0;
    mModuleHwWrite(modulePrbs, address, regVal);

    /* Reset fan-out MAC bitmap */
    address = cAf6Reg_upen_ethswc_Base + cMacEthCoreBase + cHwIdGenToCore;
    mModuleHwWrite(modulePrbs, address, 0x0);

    return cAtOk;
    }

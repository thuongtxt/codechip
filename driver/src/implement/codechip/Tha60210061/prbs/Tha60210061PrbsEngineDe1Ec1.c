/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60210061PrbsEngineDe1Ec1.c
 *
 * Created Date: Sep 5, 2017
 *
 * Description : DE1 Prbs engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/prbs/ThaPrbsEngineDe1Internal.h"
#include "../../../default/ocn/ThaModuleOcn.h"
#include "../../../default/pdh/ThaPdhDe2De1.h"
#include "Tha60210061ModulePrbs.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061PrbsEngineDe1Ec1
    {
    tThaPrbsEngineDe1 super;
    }tTha60210061PrbsEngineDe1Ec1;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPrbsEngineMethods            m_ThaPrbsEngineOverride;

/* Save super implementation */
static const tThaPrbsEngineMethods  *m_ThaPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet ChannelHwIdGet(AtPdhDe1 de1, uint8 *hwSlice, uint8 *hwSts, uint8 *hwVtg, uint8 *hwVt)
    {
    AtPdhChannel pdhChannel = (AtPdhChannel)de1;
    AtPdhChannel de2 = (AtPdhChannel)AtPdhChannelParentChannelGet(pdhChannel);
    AtSdhChannel vc  = (AtSdhChannel)AtPdhChannelVcInternalGet(pdhChannel);

    /* DS1/E1 over SDH */
    if (vc)
        {
        *hwVtg = AtSdhChannelTug2Get(vc);
        *hwVt  = AtSdhChannelTu1xGet(vc);
        return ThaSdhChannel2HwMasterStsId(vc, cAtModulePrbs, hwSlice, hwSts);
        }

    /* M13 */
    if (de2)
        return ThaPdhDe2De1HwIdGet((ThaPdhDe1)de1, hwSlice, hwSts, hwVtg, hwVt, cAtModulePrbs);

    return cAtError;
    }

static uint32 ChannelId(ThaPrbsEngine self)
    {
    AtPdhDe1 de1 = (AtPdhDe1)AtPrbsEngineChannelGet((AtPrbsEngine)self);
    uint8 hwSlice, hwSts, hwVtg, hwVt;

    if (ChannelHwIdGet(de1, &hwSlice, &hwSts, &hwVtg, &hwVt) != cAtOk)
        return cInvalidUint32;

    return Tha60210061PrbsEngineVc1xEc1ChannelId((AtPrbsEngine)self, hwSlice, hwSts, hwVtg, hwVt);
    }

static void OverrideThaPrbsEngine(ThaPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPrbsEngineOverride, m_ThaPrbsEngineMethods, sizeof(m_ThaPrbsEngineOverride));

        mMethodOverride(m_ThaPrbsEngineOverride, ChannelId);
        }

    mMethodsSet(self, &m_ThaPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideThaPrbsEngine((ThaPrbsEngine)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061PrbsEngineDe1Ec1);
    }

static AtPrbsEngine ObjectInit(AtPrbsEngine self, AtPdhDe1 de1, uint32 engineId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPrbsEngineDe1ObjectInit(self, de1, engineId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha60210061PrbsEngineDe1Ec1New(AtPdhDe1 de1, uint32 engineId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newEngine, de1, engineId);
    }

uint32 Tha60210061PrbsEngineDe1Ec1ChannelId(AtPrbsEngine self, AtPdhDe1 de1)
    {
    uint8 hwSlice, hwSts, hwVtg, hwVt;

    if (ChannelHwIdGet(de1, &hwSlice, &hwSts, &hwVtg, &hwVt) != cAtOk)
        return cInvalidUint32;

    return Tha60210061PrbsEngineVc1xEc1ChannelId(self, hwSlice, hwSts, hwVtg, hwVt);
    }

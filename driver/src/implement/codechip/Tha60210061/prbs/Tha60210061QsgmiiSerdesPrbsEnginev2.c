/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60210061QsgmiiSerdesPrbsEngine.c
 *
 * Created Date: Jun 22, 2016
 *
 * Description : PRBS engine of Qsgmii SERDES
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/eth/AtModuleEthInternal.h"
#include "../../../default/eth/ThaModuleEth.h"
#include "../physical/Tha60210061SerdesManager.h"
#include "Tha60210061ModulePrbs.h"
#include "Tha60210061SerdesPrbsEngineInternal.h"
#include "Tha60210061SgmiiQsgmiiSerdesPrbsEngineRegv2.h"

/*--------------------------- Define -----------------------------------------*/
#define cNumLanes 4

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210061QsgmiiSerdesPrbsEngine)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061QsgmiiSerdesPrbsEnginev2
    {
    tTha60210061QsgmiiSerdesPrbsEngine super;
    }tTha60210061QsgmiiSerdesPrbsEnginev2;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPrbsEngineMethods                      m_AtPrbsEngineOverride;
static tTha60210061QsgmiiSerdesPrbsEngineMethods m_Tha60210061QsgmiiSerdesPrbsEngineOverride;

/* Save super implementation */
static const tAtPrbsEngineMethods *m_AtPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 OffSet(AtPrbsEngine self)
    {
    return mMethodsGet(mThis(self))->BaseAddress(mThis(self));
    }

static uint32 EngineId2HwId(uint32 channelId)
    {
    return channelId << 4;
    }

static eBool EngineIdIsValid(uint32 subChannel)
    {
    return (subChannel <= 3) ? cAtTrue : cAtFalse;
    }

static eAtRet DestMacSubChannelSet(AtPrbsEngine self, uint32 subChannel, uint8* mac)
    {
    uint32 regAddr;
    uint32 hwMacAddress = 0;

    if (!EngineIdIsValid(subChannel))
        return cAtErrorOutOfRangParm;

    hwMacAddress  = (uint32)(mac[0] << 24);
    hwMacAddress |= (uint32)(mac[1] << 16);
    hwMacAddress |= (uint32)(mac[2] << 8);
    hwMacAddress |= (uint32)(mac[3]);
    regAddr = cAf6Reg_control_mac_da_msb(EngineId2HwId(subChannel)) + OffSet(self);
    AtPrbsEngineWrite(self, regAddr, hwMacAddress, cAtModulePrbs);

    hwMacAddress  = (uint32)(mac[4]);
    hwMacAddress |= (uint32)(mac[5] << 8);
    regAddr = cAf6Reg_control_mac_da_lsb(EngineId2HwId(subChannel)) + OffSet(self);
    AtPrbsEngineWrite(self, regAddr, hwMacAddress, cAtModulePrbs);

    return cAtOk;
    }

static eAtRet DestMacSet(AtPrbsEngine self, uint8* mac)
    {
    uint32 subChannel;
    eAtRet ret = cAtOk;

    for (subChannel = 0; subChannel < cNumLanes; subChannel++)
        ret |= DestMacSubChannelSet(self, subChannel, mac);

    return ret;
    }

static eAtRet SrcMacSubChannelSet(AtPrbsEngine self, uint32 subChannel, uint8* mac)
    {
    uint32 regAddr;
    uint32 hwMacAddress = 0;

    if (!EngineIdIsValid(subChannel))
        return cAtErrorOutOfRangParm;

    hwMacAddress  = (uint32)(mac[0] << 24);
    hwMacAddress |= (uint32)(mac[1] << 16);
    hwMacAddress |= (uint32)(mac[2] << 8);
    hwMacAddress |= (uint32)(mac[3]);
    regAddr = cAf6Reg_control_mac_sa_msb(EngineId2HwId(subChannel)) + OffSet(self);
    AtPrbsEngineWrite(self, regAddr, hwMacAddress, cAtModulePrbs);

    hwMacAddress  = (uint32)(mac[4]);
    hwMacAddress |= (uint32)(mac[5] << 8);
    regAddr = cAf6Reg_control_mac_sa_lsb(EngineId2HwId(subChannel)) + OffSet(self);
    AtPrbsEngineWrite(self, regAddr, hwMacAddress, cAtModulePrbs);

    return cAtOk;
    }

static eAtRet SrcMacSet(AtPrbsEngine self, uint8* mac)
    {
    uint32 subChannel;
    eAtRet ret = cAtOk;

    for(subChannel = 0; subChannel < cNumLanes; subChannel ++)
        ret |= SrcMacSubChannelSet(self, subChannel, mac);

    return ret;
    }

static eAtRet EthernetTypeSubChannelSet(AtPrbsEngine self, uint32 subChannel, uint16 ethType)
    {
    if (!EngineIdIsValid(subChannel))
        return cAtErrorOutOfRangParm;

    AtPrbsEngineWrite(self, cAf6Reg_control_mac_type(EngineId2HwId(subChannel)) + OffSet(self), ethType, cAtModulePrbs);
    return cAtOk;
    }

static eAtRet EthernetTypeSet(AtPrbsEngine self, uint16 ethType)
    {
    uint32 subChannel;
    eAtRet ret = cAtOk;

    for (subChannel = 0; subChannel < cNumLanes; subChannel++)
        ret |= EthernetTypeSubChannelSet(self, subChannel, ethType);

    return ret;
    }

static void MacInit(AtPrbsEngine self)
    {
    static uint8 destMac[] = {0xC0, 0xCA, 0xC0, 0xCA, 0xC0, 0xCA};
    static uint8 srcMac[] = {0xCA, 0xFE, 0xCA, 0xFE, 0xCA, 0xFE};

    DestMacSet(self, destMac);
    SrcMacSet(self, srcMac);
    EthernetTypeSet(self, 0x8847);
    }

static void SubChannelHwDefault(AtPrbsEngine self, uint32 subChannel)
    {
    uint32 regAddr, regValue;
    static const uint16 cDefaultPayload = 1500;

    if (!EngineIdIsValid(subChannel))
        return;

    /* Default payload, Enable invert FCS data */
    regAddr  = cAf6Reg_control_channel_test_mode(EngineId2HwId(subChannel)) + OffSet(self);
    regValue = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);

    mRegFieldSet(regValue, cAf6_control_channel_test_mode_Payload_size_, cDefaultPayload);
    mRegFieldSet(regValue, cAf6_control_channel_test_mode_Invert_FCS_enable_, 1);
    mRegFieldSet(regValue, cAf6_control_channel_test_mode_Loopout_enable_, 0);
    mRegFieldSet(regValue, cAf6_control_channel_test_mode_Mac_header_check_enable_, 0);
    mRegFieldSet(regValue, cAf6_control_channel_test_mode_Configure_test_mode_, 0);
    mRegFieldSet(regValue, cAf6_control_channel_test_mode_Force_error_, 0);
    mRegFieldSet(regValue, cAf6_control_channel_test_mode_Bandwidth_, 0);
    mRegFieldSet(regValue, cAf6_control_channel_test_mode_Data_mode_, 0);
    mRegFieldSet(regValue, cAf6_control_channel_test_mode_Fix_data_value_, 0xAA);

    AtPrbsEngineWrite(self, regAddr, regValue,cAtModulePrbs);
    }

static void HwDefault(AtPrbsEngine self)
    {
    uint32 subChannel;

    for(subChannel = 0; subChannel < cNumLanes; subChannel++)
        SubChannelHwDefault(self, subChannel);
    }

static void HwDefaultSet(Tha60210061QsgmiiSerdesPrbsEngine self)
    {
    MacInit((AtPrbsEngine)self);
    HwDefault((AtPrbsEngine)self);
    }

static eAtModulePrbsRet SubChannelErrorForce(AtPrbsEngine self, uint32 subChannel, eBool force)
    {
    uint32 regAddr;
    uint32 regValue;
    uint32 mask, shift;

    if (!EngineIdIsValid(subChannel))
        return cAtErrorOutOfRangParm;

    regAddr = cAf6Reg_control_channel_test_mode(EngineId2HwId(subChannel)) + OffSet(self);
    mask = cAf6_control_channel_test_mode_Force_error_Mask;
    shift = cAf6_control_channel_test_mode_Force_error_Shift;
    regValue = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    mFieldIns(&regValue, mask, shift, mBoolToBin(force));
    AtPrbsEngineWrite(self, regAddr, regValue, cAtModulePrbs);

    return cAtOk;
    }

static eAtModulePrbsRet ErrorForce(AtPrbsEngine self, eBool force)
    {
    uint32 subChannel;
    eAtRet ret = cAtOk;

    for (subChannel = 0; subChannel < cNumLanes; subChannel++)
        ret |= SubChannelErrorForce(self, subChannel, force);

    return ret;
    }

static eBool SubChannelErrorIsForced(AtPrbsEngine self, uint32 subChannel)
    {
    uint32 regAddr;
    uint32 regValue;
    uint32 mask, shift;
    uint8 hwForced;

    if (!EngineIdIsValid(subChannel))
        return cAtErrorOutOfRangParm;

    regAddr = cAf6Reg_control_channel_test_mode(EngineId2HwId(subChannel)) + OffSet(self);
    mask = cAf6_control_channel_test_mode_Force_error_Mask;
    shift = cAf6_control_channel_test_mode_Force_error_Shift;

    regValue = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    mFieldGet(regValue, mask, shift, uint8, &hwForced);

    return hwForced ? cAtTrue : cAtFalse;
    }

static eBool ErrorIsForced(AtPrbsEngine self)
    {
    uint32 subChannel;
    eBool isForced = cAtTrue;

    for (subChannel = 0; subChannel < cNumLanes; subChannel ++)
        isForced &= SubChannelErrorIsForced(self, subChannel);

    return isForced;
    }

static uint32 SubChannelAlarmHistoryHelper(AtPrbsEngine self, uint32 subChannel, eBool readToClear)
    {
    uint32 regAddr;
    uint32 regValue;
    uint32 alarm = 0;
    eBool dataLosSyns;

    if (!EngineIdIsValid(subChannel))
        return alarm;

    regAddr = cAf6Reg_Alarm_sticky(EngineId2HwId(subChannel))+ OffSet(self);
    regValue = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);

    if (regValue & (cAf6_Alarm_sticky_Mon_package_error_Mask     |
                    cAf6_Alarm_sticky_Mon_package_len_error_Mask |
                    cAf6_Alarm_sticky_Mon_FCS_error_Mask         |
                    cAf6_Alarm_sticky_Mon_data_error_Mask        |
                    cAf6_Alarm_sticky_Mon_MAC_header_error_Mask))
        alarm |= cAtPrbsEngineAlarmTypeError;

    mFieldGet(regValue, cAf6_Alarm_sticky_Mon_Data_sync_Mask, cAf6_Alarm_sticky_Mon_Data_sync_Shift, eBool, &dataLosSyns);
    if (!dataLosSyns)
        alarm |= cAtPrbsEngineAlarmTypeLossSync;

    if (readToClear)
        AtPrbsEngineWrite(self, regAddr, regValue, cAtModulePrbs);

    return alarm;
    }

static uint32 AlarmHistoryHelper(AtPrbsEngine self, eBool readToClear)
    {
    uint32 subChannel;
    uint32 alarm = 0;

    for (subChannel = 0; subChannel < cNumLanes; subChannel++)
        alarm |= SubChannelAlarmHistoryHelper(self, subChannel, readToClear);

    return alarm;
    }

static uint32 AlarmHistoryGet(AtPrbsEngine self)
    {
    return AlarmHistoryHelper(self, cAtFalse);
    }

static uint32 AlarmHistoryClear(AtPrbsEngine self)
    {
    return AlarmHistoryHelper(self, cAtTrue);
    }

static eAtModulePrbsRet SubChannelEnable(AtPrbsEngine self, uint32 subChannel, eBool enable)
    {
    uint32 regAddr;
    uint32 regValue;

    if (!EngineIdIsValid(subChannel))
        return cAtErrorOutOfRangParm;

    regAddr = cAf6Reg_control_channels_pen(EngineId2HwId(subChannel)) + OffSet(self);
    regValue = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    mFieldIns(&regValue, cAf6_control_channels_pen_Enable_Mask, cAf6_control_channels_pen_Enable_Shift, mBoolToBin(enable));
    AtPrbsEngineWrite(self, regAddr, regValue, cAtModulePrbs);

    return cAtOk;
    }

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    uint32 subChannel;

    for (subChannel = 0; subChannel < cNumLanes; subChannel++)
        SubChannelEnable(self, subChannel, enable);

    return cAtOk;
    }

static eBool SubChannelIsEnabled(AtPrbsEngine self, uint32 subChannel)
    {
    uint8 hwEnable;
    uint8 hwEnableTotal = 1;
    uint32 regAddr, regValue;

    if (!EngineIdIsValid(subChannel))
        return cAtFalse;

    regAddr = cAf6Reg_control_channels_pen(EngineId2HwId(subChannel)) + OffSet(self);
    regValue = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    mFieldGet(regValue, cAf6_control_channels_pen_Enable_Mask, cAf6_control_channels_pen_Enable_Shift, uint8, &hwEnable);
    hwEnableTotal &= hwEnable;

    return mBinToBool(hwEnableTotal);
    }

static eBool IsEnabled(AtPrbsEngine self)
    {
    uint32 subChannel;
    eBool hwEnableTotal = cAtTrue;

    for (subChannel = 0; subChannel < cNumLanes; subChannel++)
        hwEnableTotal &= SubChannelIsEnabled(self, subChannel);

    return hwEnableTotal;
    }

static uint8 ModeSw2Hw(eAtPrbsMode prbsMode)
    {
    if (prbsMode == cAtPrbsModePrbs15)                return 0;
    if (prbsMode == cAtPrbsModePrbs23)                return 1;
    if (prbsMode == cAtPrbsModePrbsFixedPattern1Byte) return 2;
    if (prbsMode == cAtPrbsModePrbsSeq)               return 3;

    return 0xF;
    }

static eAtModulePrbsRet SubChannelModeSet(AtPrbsEngine self, uint32 subChannel, eAtPrbsMode prbsMode)
    {
    uint32 regAddr;
    uint32 regValue;

    if (!EngineIdIsValid(subChannel))
        return cAtErrorOutOfRangParm;

    regAddr = cAf6Reg_control_channels_pen(EngineId2HwId(subChannel)) + OffSet(self);
    regValue = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    mFieldIns(&regValue, cAf6_control_channel_test_mode_Data_mode_Mask, cAf6_control_channel_test_mode_Data_mode_Shift, ModeSw2Hw(prbsMode));
    AtPrbsEngineWrite(self, regAddr, regValue, cAtModulePrbs);

    return cAtOk;
    }

static eAtModulePrbsRet ModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    uint32 subChannel;
    eAtRet ret = cAtOk;

    for (subChannel = 0; subChannel < cNumLanes; subChannel++)
        ret |= SubChannelModeSet(self, subChannel, prbsMode);

    return ret;
    }

static eAtPrbsMode ModeHw2Sw(uint8 hwMode)
    {
    if (hwMode == 0x0) return cAtPrbsModePrbs15;
    if (hwMode == 0x1) return cAtPrbsModePrbs23;
    if (hwMode == 0x2) return cAtPrbsModePrbsFixedPattern1Byte;
    if (hwMode == 0x3) return cAtPrbsModePrbsSeq;

    return cAtPrbsModeInvalid;
    }

static eAtPrbsMode SubChannelModeGet(AtPrbsEngine self, uint32 subChannel)
    {
    uint32 regAddr;
    uint32 regValue;
    uint8 prbsMode;

    if (!EngineIdIsValid(subChannel))
        return cAtPrbsModeInvalid;

    regAddr = cAf6Reg_control_channels_pen(EngineId2HwId(subChannel)) + OffSet(self);
    regValue = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    mFieldGet(regValue, cAf6_control_channel_test_mode_Data_mode_Mask, cAf6_control_channel_test_mode_Data_mode_Shift, uint8, &prbsMode);

    return ModeHw2Sw(prbsMode);
    }

static eAtPrbsMode ModeGet(AtPrbsEngine self)
    {
    return SubChannelModeGet(self, 0);
    }

static eBool ModeIsSupported(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    AtUnused(self);

    if ((prbsMode == cAtPrbsModePrbs15)                 ||
        (prbsMode == cAtPrbsModePrbsSeq)                ||
        (prbsMode == cAtPrbsModePrbsFixedPattern1Byte)  ||
        (prbsMode == cAtPrbsModePrbs23))
        return cAtTrue;

    return cAtFalse;
    }

static eBool CounterIsSupported(uint16 counterType)
    {
    if ((counterType == cAtPrbsEngineCounterTxFrame) || (counterType == cAtPrbsEngineCounterRxFrame))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 SubChannelCounterGet(AtPrbsEngine self, uint32 subChannel, uint16 counterType)
    {
    uint32 regAddress;

    if (!EngineIdIsValid(subChannel))
        return 0;

    if (!CounterIsSupported(counterType))
        return 0;

    if (counterType == cAtPrbsEngineCounterTxFrame)
        regAddress = cAf6Reg_counter_tx_ro(EngineId2HwId(subChannel)) + OffSet(self);

    else
        regAddress = cAf6Reg_counter_rx_ro(EngineId2HwId(subChannel)) + OffSet(self);

    return AtPrbsEngineRead(self, regAddress, cAtModulePrbs);
    }

static uint32 CounterGet(AtPrbsEngine self, uint16 counterType)
    {
    uint32 counterAllChannel = 0;
    uint32 subChannel;

    if (!CounterIsSupported(counterType))
        return 0;

    for (subChannel = 0; subChannel < cNumLanes; subChannel++)
        counterAllChannel += SubChannelCounterGet(self, subChannel, counterType);

    return counterAllChannel;
    }

static uint32 SubChannelCounterClear(AtPrbsEngine self, uint32 subChannel, uint16 counterType)
    {
    uint32 regAddress;

    if (!EngineIdIsValid(subChannel))
        return 0;

    if (!CounterIsSupported(counterType))
        return 0;

    if (counterType == cAtPrbsEngineCounterTxFrame)
        regAddress = cAf6Reg_counter_tx_r2c(EngineId2HwId(subChannel)) + OffSet(self);

    else
        regAddress = cAf6Reg_counter_rx_r2c(EngineId2HwId(subChannel)) + OffSet(self);

    return AtPrbsEngineRead(self, regAddress, cAtModulePrbs);
    }

static uint32 CounterClear(AtPrbsEngine self, uint16 counterType)
    {
    uint32 counterAllChannel = 0;
    uint32 subChannel;

    if (!CounterIsSupported(counterType))
        return 0;

    for (subChannel = 0; subChannel < cNumLanes; subChannel++)
        counterAllChannel += SubChannelCounterClear(self, subChannel, counterType);

    return counterAllChannel;
    }

static eAtModulePrbsRet SubChannelFixedPatternSet(AtPrbsEngine self, uint32 subChannel, uint32 fixedPattern)
    {
    uint32 regAddr, regValue;

    if (ModeGet(self) != cAtPrbsModePrbsFixedPattern1Byte)
        return cAtErrorModeNotSupport;

    if (!EngineIdIsValid(subChannel))
        return cAtErrorOutOfRangParm;

    regAddr = cAf6Reg_control_channel_test_mode(EngineId2HwId(subChannel)) + OffSet(self);
    regValue = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    mRegFieldSet(regValue, cAf6_control_channel_test_mode_Fix_data_value_, fixedPattern);
    AtPrbsEngineWrite(self, regAddr, regValue, cAtModulePrbs);

    return cAtOk;
    }

static eAtModulePrbsRet FixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    uint32 subChannel;
    eAtRet ret = cAtOk;

    if (ModeGet(self) != cAtPrbsModePrbsFixedPattern1Byte)
        return cAtErrorModeNotSupport;

    for (subChannel = 0; subChannel < cNumLanes; subChannel++)
        ret |= SubChannelFixedPatternSet(self, subChannel, fixedPattern);

    return ret;
    }

static uint32 SubChannelFixedPatternGet(AtPrbsEngine self, uint32 subChannel)
    {
    uint32 regAddr, regValue;

    if (ModeGet(self) != cAtPrbsModePrbsFixedPattern1Byte)
        return 0;

    if (!EngineIdIsValid(subChannel))
        return 0;

    regAddr = cAf6Reg_control_channel_test_mode(EngineId2HwId(subChannel)) + OffSet(self);
    regValue = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);

    return mRegField(regValue, cAf6_control_channel_test_mode_Fix_data_value_);
    }

static uint32 FixedPatternGet(AtPrbsEngine self)
    {
    return SubChannelFixedPatternGet(self, 0);
    }

static void SubChannelDebug(AtPrbsEngine self, uint32 subChannel)
    {
    uint32 regAddr, regValue;

    if (!EngineIdIsValid(subChannel))
        return;

    regAddr  = cAf6Reg_control_channel_test_mode(EngineId2HwId(subChannel)) + OffSet(self);
    regValue = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);

    AtPrintc(cSevNormal, "Payload Size: %u\r\n", mRegField(regValue, cAf6_control_channel_test_mode_Payload_size_));
    AtPrintc(cSevNormal, "FCS Invert: %s\r\n", mRegField(regValue, cAf6_control_channel_test_mode_Invert_FCS_enable_) ? "en" : "dis");

    return;
    }

static void Debug(AtPrbsEngine self)
    {
    SubChannelDebug(self, 0);
    }

static void OverrideAtPrbsEngineSerdes(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, mMethodsGet(self), sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, ErrorForce);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorIsForced);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryClear);
        mMethodOverride(m_AtPrbsEngineOverride, Enable);
        mMethodOverride(m_AtPrbsEngineOverride, IsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, ModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, CounterGet);
        mMethodOverride(m_AtPrbsEngineOverride, CounterClear);
        mMethodOverride(m_AtPrbsEngineOverride, Debug);
        mMethodOverride(m_AtPrbsEngineOverride, FixedPatternSet);
        mMethodOverride(m_AtPrbsEngineOverride, FixedPatternGet);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void OverrideTha60210061QsgmiiSerdesPrbsEngine(AtPrbsEngine self)
    {
    Tha60210061QsgmiiSerdesPrbsEngine engine = (Tha60210061QsgmiiSerdesPrbsEngine)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210061QsgmiiSerdesPrbsEngineOverride, mMethodsGet(engine), sizeof(m_Tha60210061QsgmiiSerdesPrbsEngineOverride));

        mMethodOverride(m_Tha60210061QsgmiiSerdesPrbsEngineOverride, DestMacSet);
        mMethodOverride(m_Tha60210061QsgmiiSerdesPrbsEngineOverride, SrcMacSet);
        mMethodOverride(m_Tha60210061QsgmiiSerdesPrbsEngineOverride, EthernetTypeSet);
        mMethodOverride(m_Tha60210061QsgmiiSerdesPrbsEngineOverride, HwDefaultSet);
        }

    mMethodsSet(engine, &m_Tha60210061QsgmiiSerdesPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtPrbsEngineSerdes(self);
    OverrideTha60210061QsgmiiSerdesPrbsEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061QsgmiiSerdesPrbsEnginev2);
    }

static AtPrbsEngine ObjectInit(AtPrbsEngine self, AtSerdesController serdesController)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210061QsgmiiSerdesPrbsEngineObjectInit(self, serdesController) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha60210061QsgmiiSerdesPrbsEngineV2New(AtSerdesController serdesController)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newEngine, serdesController);
    }

eAtRet Tha60210061SerdesPrbsQsgmiiDestMacSubChannelSet(AtPrbsEngine self, uint32 subChannel, uint8* mac)
    {
    if (self != NULL)
        return DestMacSubChannelSet(self, subChannel, mac);
    return cAtErrorNullPointer;
    }

eAtRet Tha60210061SerdesPrbsQsgmiiSrcMacSubChannelSet(AtPrbsEngine self, uint32 subChannel, uint8* mac)
    {
    if (self != NULL)
        return SrcMacSubChannelSet(self, subChannel, mac);
    return cAtErrorNullPointer;
    }

eAtRet Tha60210061SerdesPrbsQsgmiiEthernetTypeSubChannelSet(AtPrbsEngine self, uint32 subChannel, uint16 ethType)
    {
    if (self != NULL)
        return EthernetTypeSubChannelSet(self, subChannel, ethType);
    return cAtErrorNullPointer;
    }

eAtModulePrbsRet Tha60210061SerdesPrbsQsgmiiSubChannelEnable(AtPrbsEngine self, uint32 subChannel, eBool enable)
    {
    if (self != NULL)
        return SubChannelEnable(self, subChannel, enable);
    return cAtErrorNullPointer;
    }

eBool Tha60210061SerdesPrbsQsgmiiSubChannelIsEnabled(AtPrbsEngine self, uint32 subChannel)
    {
    if (self != NULL)
            return SubChannelIsEnabled(self, subChannel);
    return cAtFalse;
    }

void Tha60210061SerdesPrbsQsgmiiSubChannelHwDefault(AtPrbsEngine self, uint32 subChannel)
    {
    if (self != NULL)
        SubChannelHwDefault(self, subChannel);
    }

eAtModulePrbsRet Tha60210061SerdesPrbsQsgmiiSubChannelErrorForce(AtPrbsEngine self, uint32 subChannel, eBool force)
    {
    if (self != NULL)
        return SubChannelErrorForce(self, subChannel, force);
    return cAtErrorNullPointer;
    }

eBool Tha60210061SerdesPrbsQsgmiiSubChannelErrorIsForced(AtPrbsEngine self, uint32 subChannel)
    {
    if (self != NULL)
        return SubChannelErrorIsForced(self, subChannel);
    return cAtFalse;
    }

uint32 Tha60210061SerdesPrbsQsgmiiSubChannelAlarmHistoryHelper(AtPrbsEngine self, uint32 subChannel, eBool readToClear)
    {
    if (self != NULL)
        return SubChannelAlarmHistoryHelper(self, subChannel, readToClear);
    return 0;
    }

uint32 Tha60210061SerdesPrbsQsgmiiSubChannelCounterGet(AtPrbsEngine self, uint32 subChannel, uint16 counterType)
    {
    if (self != NULL)
        return SubChannelCounterGet(self, subChannel, counterType);
    return 0;
    }

uint32 Tha60210061SerdesPrbsQsgmiiSubChannelCounterClear(AtPrbsEngine self, uint32 subChannel, uint16 counterType)
    {
    if (self != NULL)
        return SubChannelCounterClear(self, subChannel, counterType);
    return 0;
    }

eAtModulePrbsRet Tha60210061SerdesPrbsQsgmiiSubChannelModeSet(AtPrbsEngine self, uint32 subChannel, eAtPrbsMode prbsMode)
    {
    if (self != NULL)
        return SubChannelModeSet(self, subChannel, prbsMode);
    return cAtErrorNullPointer;
    }

eAtPrbsMode Tha60210061SerdesPrbsQsgmiiSubChannelModeGet(AtPrbsEngine self, uint32 subChannel)
    {
    if (self != NULL)
        return SubChannelModeGet(self, subChannel);
    return cAtPrbsModeInvalid;
    }

eAtModulePrbsRet Tha60210061SerdesPrbsQsgmiiSubChannelFixedPatternSet(AtPrbsEngine self, uint32 subChannel, uint32 fixedPattern)
    {
    if (self != NULL)
        return SubChannelFixedPatternSet(self, subChannel, fixedPattern);
    return cAtErrorNullPointer;
    }

uint32 Tha60210061SerdesPrbsQsgmiiSubChannelFixedPatternGet(AtPrbsEngine self, uint32 subChannel)
    {
    if (self != NULL)
        return SubChannelFixedPatternGet(self, subChannel);
    return 0;
    }

void Tha60210061SerdesPrbsQsgmiiSubChannelDebug(AtPrbsEngine self, uint32 subChannel)
    {
    if (self != NULL)
        SubChannelDebug(self, subChannel);
    }

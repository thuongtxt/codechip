/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha60210061QsgmiiSerdesControllerRawPrbsEngine.c
 *
 * Created Date: Feb 13, 2017
 *
 * Description : QSGMII Serdes controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../physical/Tha60210061SerdesManager.h"
#include "../physicalV2/Tha60210061SerdesControlV2Reg.h"
#include "Tha60210061ModulePrbs.h"
#include "Tha60210061SerdesRawPrbsEngineReg.h"
#include "Tha60210061SerdesPrbsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPrbsEngineMethods m_AtPrbsEngineOverride;
static tTha60210061SgmiiSerdesRawPrbsEngineMethods m_Tha60210061SgmiiSerdesRawPrbsEngineOverride;

/* Save super implementation */
static const tAtPrbsEngineMethods *m_AtPrbsEngineMethods = NULL;
static const tTha60210061SgmiiSerdesRawPrbsEngineMethods *m_Tha60210061SgmiiSerdesRawPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 CounterOffset(Tha60210061SgmiiSerdesRawPrbsEngine self)
    {
    AtUnused(self);
    return cCounterLowDrpAddressGthE3;
    }

static eAtPrbsSide GeneratingSideGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsSidePsn;
    }

static eAtPrbsSide MonitoringSideGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsSidePsn;
    }

static eAtModulePrbsRet GeneratingSideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return ((side == cAtPrbsSidePsn) ? cAtOk : cAtErrorModeNotSupport);
    }

static eAtModulePrbsRet MonitoringSideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return ((side == cAtPrbsSidePsn) ? cAtOk : cAtErrorModeNotSupport);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, m_AtPrbsEngineMethods, sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, GeneratingSideSet);
        mMethodOverride(m_AtPrbsEngineOverride, GeneratingSideGet);
        mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideSet);
        mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideGet);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void OverrideMenthodsTha60210061SgmiiSerdesRawPrbsEngine(AtPrbsEngine self)
    {
    Tha60210061SgmiiSerdesRawPrbsEngine serdes = (Tha60210061SgmiiSerdesRawPrbsEngine)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210061SgmiiSerdesRawPrbsEngineMethods = mMethodsGet(serdes);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210061SgmiiSerdesRawPrbsEngineOverride, mMethodsGet(serdes), sizeof(m_Tha60210061SgmiiSerdesRawPrbsEngineOverride));

        mMethodOverride(m_Tha60210061SgmiiSerdesRawPrbsEngineOverride, CounterOffset);
        }

    mMethodsSet(serdes, &m_Tha60210061SgmiiSerdesRawPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtPrbsEngine(self);
    OverrideMenthodsTha60210061SgmiiSerdesRawPrbsEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061GthSerdesRawPrbsEngine);
    }

AtPrbsEngine Tha60210061GthSerdesRawPrbsEngineObjectInit(AtPrbsEngine self, AtSerdesController serdesController)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210061SgmiiSerdesRawPrbsObjectInit(self, serdesController) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha60210061GthSerdesRawPrbsEngineNew(AtSerdesController serdesController)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine prbsEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (prbsEngine == NULL)
        return NULL;

    /* Construct it */
    return Tha60210061GthSerdesRawPrbsEngineObjectInit(prbsEngine, serdesController);
    }

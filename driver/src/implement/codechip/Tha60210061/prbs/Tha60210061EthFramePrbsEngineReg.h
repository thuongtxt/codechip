/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60210061EthFramePrbsEngineReg.h
 * 
 * Created Date: Jan 16, 2017
 *
 * Description : Common PRBS engine registers
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061ETHFRAMEPRBSENGINEREG_H_
#define _THA60210061ETHFRAMEPRBSENGINEREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#define cEthFrameGenBaseAddress 0x04C000
#define cHwIdGenToCore 15

/*------------------------------------------------------------------------------
Reg Name   : ETH Gen Active Control
Reg Addr   : 0x00
Reg Formula: 0x00 + $gen_id * 0x100
    Where  :
           + $gen_id(0-0): gen_id support maximum rate 2.5Gbps
Reg Desc   :
This register checks version ID

------------------------------------------------------------------------------*/
#define cAf6Reg_egen_active_Base                                                                          0x00
#define cAf6Reg_egen_active_WidthVal                                                                        32

/*--------------------------------------
BitField Name: LoadEn
BitField Type: R/W
BitField Desc: Load config
BitField Bits: [1:1]
--------------------------------------*/
#define cAf6_egen_active_LoadEn_Mask                                                                     cBit1
#define cAf6_egen_active_LoadEn_Shift                                                                        1

/*--------------------------------------
BitField Name: Restart
BitField Type: R/W
BitField Desc: Restart gen
BitField Bits: [1:1]
--------------------------------------*/
#define cAf6_egen_active_Restart_Mask                                                                    cBit2
#define cAf6_egen_active_Restart_Shift                                                                       2

/*--------------------------------------
BitField Name: GenEn
BitField Type: R/W
BitField Desc: Gen Enable
BitField Bits: [0:0]
--------------------------------------*/
#define cAf6_egen_active_GenEn_Mask                                                                      cBit0
#define cAf6_egen_active_GenEn_Shift                                                                         0


/*------------------------------------------------------------------------------
Reg Name   : ETH Gen Rate Control
Reg Addr   : 0x01
Reg Formula: 0x01 + $gen_id * 0x100
    Where  :
           + $gen_id(0-0): gen_id support maximum rate 2.5Gbps
Reg Desc   :
This register checks version ID

------------------------------------------------------------------------------*/
#define cAf6Reg_egen_rate_Base                                                                            0x01

/*--------------------------------------
BitField Name: GenRate
BitField Type: R/W
BitField Desc: Config rate, unit 1Kbps
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_egen_rate_GenRate_Mask                                                                   cBit31_0
#define cAf6_egen_rate_GenRate_Shift                                                                         0


/*------------------------------------------------------------------------------
Reg Name   : ETH Pkt Num Control
Reg Addr   : 0x02
Reg Formula: 0x02 + $gen_id * 0x100
    Where  :
           + $gen_id(0-0): gen_id support maximum rate 2.5Gbps
Reg Desc   :
This register checks version ID

------------------------------------------------------------------------------*/
#define cAf6Reg_egen_pktnum_Base                                                                          0x02

/*--------------------------------------
BitField Name: GenPNum
BitField Type: R/W
BitField Desc: Number of packet need gen
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_egen_pktnum_GenPNum_Mask                                                                 cBit31_0
#define cAf6_egen_pktnum_GenPNum_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : ETH Gen Pkt Len Control
Reg Addr   : 0x03
Reg Formula: 0x03 + $gen_id * 0x100
    Where  :
           + $gen_id(0-0): gen_id support maximum rate 2.5Gbps
Reg Desc   :
This register checks version ID

------------------------------------------------------------------------------*/
#define cAf6Reg_egen_pktlen_Base                                                                          0x03
#define cAf6Reg_egen_pktlen_WidthVal                                                                        32

/*--------------------------------------
BitField Name: MaxPLen
BitField Type: R/W
BitField Desc: Max Length of packet
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_egen_pktlen_MaxPLen_Mask                                                                cBit31_16
#define cAf6_egen_pktlen_MaxPLen_Shift                                                                      16

/*--------------------------------------
BitField Name: MinPLen
BitField Type: R/W
BitField Desc: Min Length of packet
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_egen_pktlen_MinPLen_Mask                                                                 cBit15_0
#define cAf6_egen_pktlen_MinPLen_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : ETH Gen Option Control
Reg Addr   : 0x04
Reg Formula: 0x04 + $gen_id * 0x100
    Where  :
           + $gen_id(0-0): gen_id support maximum rate 2.5Gbps
Reg Desc   :
This register checks version ID

------------------------------------------------------------------------------*/
#define cAf6Reg_egen_modopt_Base                                                                          0x04
#define cAf6Reg_egen_modopt_WidthVal                                                                        32

/*--------------------------------------
BitField Name: DataMod
BitField Type: R/W
BitField Desc: Data mode 0x0    : Inc 0x1    : Dec 0x2    : Fix 0x3    : PRBS31
0x4-0x7: Unused 0x8    : PRBS9     (reserved for future) 0x9    : PRBS11
(reserved for future) 0xA    : PRBS15    (reserved for future) 0xB    : PRBS20.1
(reserved for future) 0xC    : PRBS20.2  (reserved for future) 0xD    : PRBS23
(reserved for future) 0xE    : PRBS29    (reserved for future) 0xF    : PRBS31
(reserved for future)
BitField Bits: [27:24]
--------------------------------------*/
#define cAf6_egen_modopt_DataMod_Mask                                                                cBit27_24
#define cAf6_egen_modopt_DataMod_Shift                                                                      24

/*--------------------------------------
BitField Name: DataFix
BitField Type: R/W
BitField Desc: Data Fix pattern
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_egen_modopt_DataFix_Mask                                                                cBit23_16
#define cAf6_egen_modopt_DataFix_Shift                                                                      16

/*--------------------------------------
BitField Name: RateFcsEn
BitField Type: R/W
BitField Desc: Calc rate with FCS
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_egen_modopt_RateFcsEn_Mask                                                                  cBit3
#define cAf6_egen_modopt_RateFcsEn_Shift                                                                     3

/*--------------------------------------
BitField Name: GenAllEn
BitField Type: R/W
BitField Desc: Gen Continuous Enable
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_egen_modopt_GenAllEn_Mask                                                                   cBit2
#define cAf6_egen_modopt_GenAllEn_Shift                                                                      2

/*--------------------------------------
BitField Name: LENIncEn
BitField Type: R/W
BitField Desc: Packet length increase
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_egen_modopt_LENIncEn_Mask                                                                   cBit1
#define cAf6_egen_modopt_LENIncEn_Shift                                                                      1

/*--------------------------------------
BitField Name: DAIncEn
BitField Type: R/W
BitField Desc: DA address increase
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_egen_modopt_DAIncEn_Mask                                                                    cBit0
#define cAf6_egen_modopt_DAIncEn_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : ETH Pkt Hdr Control
Reg Addr   : 0x05
Reg Formula: 0x05 + $gen_id * 0x100
    Where  :
           + $gen_id(0-0): gen_id support maximum rate 2.5Gbps
Reg Desc   :
This register used to config length of header

------------------------------------------------------------------------------*/
#define cAf6Reg_egen_pktHdr_Base                                                                          0x05

/*--------------------------------------
BitField Name: HdrLen
BitField Type: R/W
BitField Desc: Header length of packet
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_egen_pktHdr_HdrLen_Mask                                                                   cBit7_0
#define cAf6_egen_pktHdr_HdrLen_Shift                                                                        0


/*------------------------------------------------------------------------------
Reg Name   : ETH Mode Option Control
Reg Addr   : 0x40
Reg Formula: 0x40 + $mon_id * 0x100 + $r2c * 0x10 + type
    Where  :
           + $mon_id(0-8): mon_id
           + $r2c(0-0) Read to clear
           + $type (0-15) type counter
Reg Desc   :
This register show counter type
type = 00   : rxPkt
type = 01   : rxGood
type = 02   : rxFcser
type = 03   : rxPrmer
type = 04   : rxRcver
type = 05   : rxIpger
type = 06   : rxDataer
type = 07   : rxPauOn
type = 08   : rxPauOff
type = 09   : rxLen64
type = 0A   : rxLen128
type = 0B   : rxLen256
type = 0C   : rxLen512
type = 0D   : rxLen1024
type = 0E   : rxLen2048
type = 0F   : rxLenJumbo

------------------------------------------------------------------------------*/
#define cAf6Reg_egen_pktcnt_Base                                                                          0x40
#define cAf6Reg_egen_pktcnt_WidthVal                                                                        32

/*--------------------------------------
BitField Name: CntValue
BitField Type: R/W
BitField Desc: Counter value
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_egen_pktcnt_CntValue_Mask                                                                cBit31_0
#define cAf6_egen_pktcnt_CntValue_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : ETH Pkt Hdr Value
Reg Addr   : 0x80
Reg Formula: 0x80 + $gen_id * 0x100 + $dword_id
    Where  :
           + $gen_id(0-0): gen_id support maximum rate 2.5Gbps
           + $dword_id(0-31)
Reg Desc   :
This register checks version ID

------------------------------------------------------------------------------*/
#define cAf6Reg_egen_Hdrcfg_Base                                                                          0x80

/*--------------------------------------
BitField Name: Hdrdata
BitField Type: R/W
BitField Desc: Header of packet need gen
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_egen_Hdrcfg_Hdrdata_Mask                                                                 cBit31_0
#define cAf6_egen_Hdrcfg_Hdrdata_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : ETH Mon Active Control
Reg Addr   : 0x10
Reg Formula: 0x10 + $mon_id * 0x100
    Where  :
           + $mon_id(0-8): mon_id support maximum rate 2.5Gbps
Reg Desc   :
This register checks version ID

------------------------------------------------------------------------------*/
#define cAf6Reg_emon_data_Base                                                                            0x10

/*--------------------------------------
BitField Name: FCSEn
BitField Type: R/W
BitField Desc: Present FCS field
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_emon_data_FCSEn_Mask                                                                       cBit28
#define cAf6_emon_data_FCSEn_Shift                                                                          28

/*--------------------------------------
BitField Name: DataMod
BitField Type: R/W
BitField Desc: Data Mode 0x0    : Inc 0x1    : Dec 0x2    : Fix 0x3    : PRBS31
0x4-0x7: Unused 0x8    : PRBS9     (reserved for future) 0x9    : PRBS11
(reserved for future) 0xA    : PRBS15    (reserved for future) 0xB    : PRBS20.1
(reserved for future) 0xC    : PRBS20.2  (reserved for future) 0xD    : PRBS23
(reserved for future) 0xE    : PRBS29    (reserved for future) 0xF    : PRBS31
(reserved for future)
BitField Bits: [11:08]
--------------------------------------*/
#define cAf6_emon_data_DataMod_Mask                                                                   cBit11_8
#define cAf6_emon_data_DataMod_Shift                                                                         8

/*--------------------------------------
BitField Name: DataFix
BitField Type: R/W
BitField Desc: Data Fix
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_emon_data_DataFix_Mask                                                                    cBit7_0
#define cAf6_emon_data_DataFix_Shift                                                                         0


/*------------------------------------------------------------------------------
Reg Name   : ETH Mon Mask Control
Reg Addr   : 0x11
Reg Formula: 0x11 + $mon_id * 0x100
    Where  :
           + $mon_id(0-8): mon_id support maximum rate 2.5Gbps
Reg Desc   :
This register checks version ID

------------------------------------------------------------------------------*/
#define cAf6Reg_emon_mask_Base                                                                            0x11

/*--------------------------------------
BitField Name: PauEnMsk
BitField Type: R/W
BitField Desc: Set 1 to counter pause frame
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_emon_mask_PauEnMsk_Mask                                                                     cBit8
#define cAf6_emon_mask_PauEnMsk_Shift                                                                        8

/*--------------------------------------
BitField Name: DataErMsk
BitField Type: R/W
BitField Desc: Set 1 to counter Data error
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_emon_mask_DataErMsk_Mask                                                                    cBit4
#define cAf6_emon_mask_DataErMsk_Shift                                                                       4

/*--------------------------------------
BitField Name: IpgErMsk
BitField Type: R/W
BitField Desc: Set 1 to counter Ipg error
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_emon_mask_IpgErMsk_Mask                                                                     cBit3
#define cAf6_emon_mask_IpgErMsk_Shift                                                                        3

/*--------------------------------------
BitField Name: RxErMsk
BitField Type: R/W
BitField Desc: Set 1 to counter RX error
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_emon_mask_RxErMsk_Mask                                                                      cBit2
#define cAf6_emon_mask_RxErMsk_Shift                                                                         2

/*--------------------------------------
BitField Name: FrmErMsk
BitField Type: R/W
BitField Desc: Set 1 to counter preamble error
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_emon_mask_FrmErMsk_Mask                                                                     cBit1
#define cAf6_emon_mask_FrmErMsk_Shift                                                                        1

/*--------------------------------------
BitField Name: FCSErMsk
BitField Type: R/W
BitField Desc: Set 1 to counter fcs error
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_emon_mask_FCSErMsk_Mask                                                                     cBit0
#define cAf6_emon_mask_FCSErMsk_Shift                                                                        0


/*------------------------------------------------------------------------------
Reg Name   : ETH Pkt Hdr Control
Reg Addr   : 0x15
Reg Formula: 0x15 + $mon_id * 0x100
    Where  :
           + $mon_id(0-8): mon_id support maximum rate 2.5Gbps
Reg Desc   :
This register used to config length of header

------------------------------------------------------------------------------*/
#define cAf6Reg_emon_pktHdr_Base                                                                          0x15

/*--------------------------------------
BitField Name: HdrLen
BitField Type: R/W
BitField Desc: Header length of packet
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_emon_pktHdr_HdrLen_Mask                                                                   cBit7_0
#define cAf6_emon_pktHdr_HdrLen_Shift                                                                        0


/*------------------------------------------------------------------------------
Reg Name   : ETH Mode Option Control
Reg Addr   : 0x40
Reg Formula: 0x40 + $mon_id * 0x100 + $r2c * 0x10 + type
    Where  :
           + $mon_id(0-8): mon_id
           + $r2c(0-0) Read to clear
           + $type (0-15) type counter
Reg Desc   :
This register show counter type
type = 00   : rxPkt
type = 01   : rxGood
type = 02   : rxFcser
type = 03   : rxPrmer
type = 04   : rxRcver
type = 05   : rxIpger
type = 06   : rxDataer
type = 07   : rxPauOn
type = 08   : rxPauOff
type = 09   : rxLen64
type = 0A   : rxLen128
type = 0B   : rxLen256
type = 0C   : rxLen512
type = 0D   : rxLen1024
type = 0E   : rxLen2048
type = 0F   : rxLenJumbo

------------------------------------------------------------------------------*/
#define cAf6Reg_emon_pktcnt_Base                                                                          0x40
#define cAf6Reg_emon_pktcnt_WidthVal                                                                        32

/*--------------------------------------
BitField Name: CntValue
BitField Type: R/W
BitField Desc: Counter value
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_emon_pktcnt_CntValue_Mask                                                                cBit31_0
#define cAf6_emon_pktcnt_CntValue_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : ETH Pkt Hdr Value
Reg Addr   : 0x80
Reg Formula: 0x80 + $mon_id * 0x100 + $dword_id
    Where  :
           + $mon_id(0-8): mon_id support maximum rate 2.5Gbps
           + $dword_id(0-31)
Reg Desc   :
This register checks version ID

------------------------------------------------------------------------------*/
#define cAf6Reg_emon_Hdrcfg_Base                                                                          0x80

/*--------------------------------------
BitField Name: Hdrdata
BitField Type: R/W
BitField Desc: Header of packet need mon
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_emon_Hdrcfg_Hdrdata_Mask                                                                 cBit31_0
#define cAf6_emon_Hdrcfg_Hdrdata_Shift                                                                       0

/*------------------------------------------------------------------------------
Reg Name   : ETH Mon Sticky
Reg Addr   : 0x20
Reg Formula: 0x20 + $mon_id * 0x100
    Where  :
           + $mon_id(0-8): mon_id support maximum rate 2.5Gbps
Reg Desc   :
This register checks version ID

------------------------------------------------------------------------------*/
#define cAf6Reg_emon_sticky_Base                                                                          0x20

/*--------------------------------------
BitField Name: PktErr
BitField Type: W1C
BitField Desc: detected error packet
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_emon_sticky_PktErr_Mask                                                                     cBit2
#define cAf6_emon_sticky_PktErr_Shift                                                                        2

/*--------------------------------------
BitField Name: PktGood
BitField Type: W1C
BitField Desc: detected good packet
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_emon_sticky_PktGood_Mask                                                                    cBit1
#define cAf6_emon_sticky_PktGood_Shift                                                                       1

/*--------------------------------------
BitField Name: PktRcv
BitField Type: W1C
BitField Desc: receive packet
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_emon_sticky_PktRcv_Mask                                                                     cBit0
#define cAf6_emon_sticky_PktRcv_Shift                                                                        0

#endif /* _THA60210061ETHFRAMEPRBSENGINEREG_H_ */


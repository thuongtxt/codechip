/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60210061RxauiSerdesControllerPrbsEngine.c
 *
 * Created Date: Dec 28, 2016
 *
 * Description : RXAUI Serdes Raw Prbs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../physical/Tha60210061SerdesManager.h"
#include "Tha60210061ModulePrbs.h"
#include "Tha60210061SerdesRawPrbsEngineReg.h"
#include "Tha60210061SerdesPrbsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cNumMaxLane 2

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210061SgmiiSerdesRawPrbsEngine*)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061RxauiSerdesRawPrbsEngine
    {
    tTha60210061SgmiiSerdesRawPrbsEngine super;
    }tTha60210061RxauiSerdesRawPrbsEngine;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPrbsEngineMethods  m_AtPrbsEngineOverride;
static tTha60210061SgmiiSerdesRawPrbsEngineMethods m_Tha60210061SgmiiSerdesRawPrbsEngineOverride;

/* Save super implementation */
static const tAtPrbsEngineMethods *m_AtPrbsEngineMethods = NULL;
static const tTha60210061SgmiiSerdesRawPrbsEngineMethods *m_Tha60210061SgmiiSerdesRawPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ControlledByArriveRegisterEnable(Tha60210061SgmiiSerdesRawPrbsEngine self)
    {
    Tha60210061SgmiiSerdesRawPrbsEngine lane1, lane2;
    lane1 = (Tha60210061SgmiiSerdesRawPrbsEngine)AtSerdesControllerPrbsEngine(AtSerdesControllerLaneGet((mThis(self)->serdesController), 0));
    lane2 = (Tha60210061SgmiiSerdesRawPrbsEngine)AtSerdesControllerPrbsEngine(AtSerdesControllerLaneGet((mThis(self)->serdesController), 1));

    mMethodsGet(lane1)->ControlledByArriveRegisterEnable(lane1);
    mMethodsGet(lane2)->ControlledByArriveRegisterEnable(lane2);
    return cAtOk;
    }

static eAtModulePrbsRet TxEnable(AtPrbsEngine self, eBool enable)
    {
    AtPrbsEngine lane1, lane2;
    eAtRet ret;

    lane1 = AtSerdesControllerPrbsEngine(AtSerdesControllerLaneGet((mThis(self)->serdesController), 0));
    lane2 = AtSerdesControllerPrbsEngine(AtSerdesControllerLaneGet((mThis(self)->serdesController), 1));

    ret = AtPrbsEngineTxEnable(lane1, enable);
    ret |= AtPrbsEngineTxEnable(lane2, enable);

    return ret;
    }

static eBool TxIsEnabled(AtPrbsEngine self)
    {
    AtPrbsEngine lane1, lane2;
    eBool ret;

    lane1 = AtSerdesControllerPrbsEngine(AtSerdesControllerLaneGet((mThis(self)->serdesController), 0));
    lane2 = AtSerdesControllerPrbsEngine(AtSerdesControllerLaneGet((mThis(self)->serdesController), 1));

    ret = AtPrbsEngineTxIsEnabled(lane1);
    ret |= AtPrbsEngineTxIsEnabled(lane2);

    return ret;
    }

static eAtModulePrbsRet TxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    AtPrbsEngine lane1, lane2;
    eAtRet ret;

    lane1 = AtSerdesControllerPrbsEngine(AtSerdesControllerLaneGet((mThis(self)->serdesController), 0));
    lane2 = AtSerdesControllerPrbsEngine(AtSerdesControllerLaneGet((mThis(self)->serdesController), 1));

    ret = AtPrbsEngineTxModeSet(lane1, prbsMode);
    ret |= AtPrbsEngineTxModeSet(lane2, prbsMode);

    return ret;
    }

static eAtPrbsMode TxModeGet(AtPrbsEngine self)
    {
    AtPrbsEngine lane1, lane2;
    eAtPrbsMode mode;

    lane1 = AtSerdesControllerPrbsEngine(AtSerdesControllerLaneGet((mThis(self)->serdesController), 0));
    lane2 = AtSerdesControllerPrbsEngine(AtSerdesControllerLaneGet((mThis(self)->serdesController), 1));

    mode = AtPrbsEngineTxModeGet(lane1);
    mode |= AtPrbsEngineTxModeGet(lane2);

    return mode;
    }

static eAtModulePrbsRet RxEnable(AtPrbsEngine self, eBool enable)
    {
    AtPrbsEngine lane1, lane2;
    eAtRet ret;

    lane1 = AtSerdesControllerPrbsEngine(AtSerdesControllerLaneGet((mThis(self)->serdesController), 0));
    lane2 = AtSerdesControllerPrbsEngine(AtSerdesControllerLaneGet((mThis(self)->serdesController), 1));

    ret = AtPrbsEngineRxEnable(lane1, enable);
    ret |= AtPrbsEngineRxEnable(lane2, enable);

    return ret;
    }

static eBool RxIsEnabled(AtPrbsEngine self)
    {
    AtPrbsEngine lane1, lane2;
    eBool ret;

    lane1 = AtSerdesControllerPrbsEngine(AtSerdesControllerLaneGet((mThis(self)->serdesController), 0));
    lane2 = AtSerdesControllerPrbsEngine(AtSerdesControllerLaneGet((mThis(self)->serdesController), 1));

    ret = AtPrbsEngineRxIsEnabled(lane1);
    ret |= AtPrbsEngineRxIsEnabled(lane2);

    return ret;
    }

static eAtModulePrbsRet RxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    AtPrbsEngine lane1, lane2;
    eAtRet ret;

    lane1 = AtSerdesControllerPrbsEngine(AtSerdesControllerLaneGet((mThis(self)->serdesController), 0));
    lane2 = AtSerdesControllerPrbsEngine(AtSerdesControllerLaneGet((mThis(self)->serdesController), 1));

    ret = AtPrbsEngineRxModeSet(lane1, prbsMode);
    ret |= AtPrbsEngineRxModeSet(lane2, prbsMode);

    return ret;
    }

static eAtPrbsMode RxModeGet(AtPrbsEngine self)
    {
    AtPrbsEngine lane1, lane2;
    eAtPrbsMode ret;

    lane1 = AtSerdesControllerPrbsEngine(AtSerdesControllerLaneGet((mThis(self)->serdesController), 0));
    lane2 = AtSerdesControllerPrbsEngine(AtSerdesControllerLaneGet((mThis(self)->serdesController), 1));

    ret = AtPrbsEngineRxModeGet(lane1);
    ret |= AtPrbsEngineRxModeGet(lane2);

    return ret;
    }

static eAtModulePrbsRet ErrorForce(AtPrbsEngine self, eBool force)
    {
    AtPrbsEngine lane1, lane2;
    eAtRet ret;

    lane1 = AtSerdesControllerPrbsEngine(AtSerdesControllerLaneGet((mThis(self)->serdesController), 0));
    lane2 = AtSerdesControllerPrbsEngine(AtSerdesControllerLaneGet((mThis(self)->serdesController), 1));

    ret = AtPrbsEngineErrorForce(lane1, force);
    ret |= AtPrbsEngineErrorForce(lane2, force);

    return ret;
    }

static eBool ErrorIsForced(AtPrbsEngine self)
    {
    AtPrbsEngine lane1, lane2;
    eBool ret;

    lane1 = AtSerdesControllerPrbsEngine(AtSerdesControllerLaneGet((mThis(self)->serdesController), 0));
    lane2 = AtSerdesControllerPrbsEngine(AtSerdesControllerLaneGet((mThis(self)->serdesController), 1));

    ret = AtPrbsEngineErrorIsForced(lane1);
    ret |= AtPrbsEngineErrorIsForced(lane2);

    return ret;
    }

static uint32 AlarmGet(AtPrbsEngine self)
    {
    AtPrbsEngine lane1, lane2;
    uint32 ret;

    lane1 = AtSerdesControllerPrbsEngine(AtSerdesControllerLaneGet((mThis(self)->serdesController), 0));
    lane2 = AtSerdesControllerPrbsEngine(AtSerdesControllerLaneGet((mThis(self)->serdesController), 1));

    ret = AtPrbsEngineAlarmGet(lane1);
    ret |= AtPrbsEngineAlarmGet(lane2);

    return ret;
    }

static uint32 AlarmHistoryGet(AtPrbsEngine self)
    {
    AtPrbsEngine lane1, lane2;
    uint32 ret;

    lane1 = AtSerdesControllerPrbsEngine(AtSerdesControllerLaneGet((mThis(self)->serdesController), 0));
    lane2 = AtSerdesControllerPrbsEngine(AtSerdesControllerLaneGet((mThis(self)->serdesController), 1));

    ret = AtPrbsEngineAlarmHistoryGet(lane1);
    ret |= AtPrbsEngineAlarmHistoryGet(lane2);

    return ret;
    }

static uint32 AlarmHistoryClear(AtPrbsEngine self)
    {
    AtPrbsEngine lane1, lane2;
    uint32 ret;

    lane1 = AtSerdesControllerPrbsEngine(AtSerdesControllerLaneGet((mThis(self)->serdesController), 0));
    lane2 = AtSerdesControllerPrbsEngine(AtSerdesControllerLaneGet((mThis(self)->serdesController), 1));

    ret = AtPrbsEngineAlarmHistoryClear(lane1);
    ret |= AtPrbsEngineAlarmHistoryClear(lane2);

    return ret;
    }

static uint32 CounterGet(AtPrbsEngine self, uint16 counterType)
    {
    AtPrbsEngine lane1, lane2;
    uint32 ret;

    lane1 = AtSerdesControllerPrbsEngine(AtSerdesControllerLaneGet((mThis(self)->serdesController), 0));
    lane2 = AtSerdesControllerPrbsEngine(AtSerdesControllerLaneGet((mThis(self)->serdesController), 1));

    ret  = AtPrbsEngineCounterGet(lane1, counterType);
    ret += AtPrbsEngineCounterGet(lane2, counterType);

    return ret;
    }

static uint32 CounterClear(AtPrbsEngine self, uint16 counterType)
    {
    AtPrbsEngine lane1, lane2;
    uint32 ret;

    lane1 = AtSerdesControllerPrbsEngine(AtSerdesControllerLaneGet((mThis(self)->serdesController), 0));
    lane2 = AtSerdesControllerPrbsEngine(AtSerdesControllerLaneGet((mThis(self)->serdesController), 1));

    ret  = AtPrbsEngineCounterClear(lane1, counterType);
    ret += AtPrbsEngineCounterClear(lane2, counterType);

    return ret;
    }

static eAtPrbsSide GeneratingSideGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsSidePsn;
    }

static eAtPrbsSide MonitoringSideGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsSidePsn;
    }

static eAtModulePrbsRet GeneratingSideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return ((side == cAtPrbsSidePsn) ? cAtOk : cAtErrorModeNotSupport);
    }

static eAtModulePrbsRet MonitoringSideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return ((side == cAtPrbsSidePsn) ? cAtOk : cAtErrorModeNotSupport);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, m_AtPrbsEngineMethods, sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, TxEnable);
        mMethodOverride(m_AtPrbsEngineOverride, TxIsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, RxEnable);
        mMethodOverride(m_AtPrbsEngineOverride, RxIsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorForce);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorIsForced);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, CounterGet);
        mMethodOverride(m_AtPrbsEngineOverride, CounterClear);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryClear);
        mMethodOverride(m_AtPrbsEngineOverride, GeneratingSideSet);
        mMethodOverride(m_AtPrbsEngineOverride, GeneratingSideGet);
        mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideSet);
        mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideGet);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void OverrideTha60210061SgmiiSerdesRawPrbsEngine(AtPrbsEngine self)
    {
    Tha60210061SgmiiSerdesRawPrbsEngine serdes = (Tha60210061SgmiiSerdesRawPrbsEngine)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210061SgmiiSerdesRawPrbsEngineMethods = mMethodsGet(serdes);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210061SgmiiSerdesRawPrbsEngineOverride, mMethodsGet(serdes), sizeof(m_Tha60210061SgmiiSerdesRawPrbsEngineOverride));

        mMethodOverride(m_Tha60210061SgmiiSerdesRawPrbsEngineOverride, ControlledByArriveRegisterEnable);
        }

    mMethodsSet(serdes, &m_Tha60210061SgmiiSerdesRawPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtPrbsEngine(self);
    OverrideTha60210061SgmiiSerdesRawPrbsEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061RxauiSerdesRawPrbsEngine);
    }

static AtPrbsEngine ObjectInit(AtPrbsEngine self, AtSerdesController serdesController)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210061SgmiiSerdesRawPrbsObjectInit(self, serdesController) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;
    mThis(self)->serdesController = serdesController;

    return self;
    }

AtPrbsEngine Tha60210061RxauiSerdesRawPrbsEngineNew(AtSerdesController serdesController)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine prbsEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (prbsEngine == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(prbsEngine, serdesController);
    }

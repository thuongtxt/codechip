/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha60210061SgmiiSerdesPrbsEngine.c
 *
 * Created Date: Jun 23, 2016
 *
 * Description : 60210061 SGMII serdes PRBS engine
 *
 * Notes       : PRBS engine
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/eth/AtModuleEthInternal.h"
#include "../../../default/eth/ThaModuleEth.h"
#include "Tha60210061ModulePrbs.h"
#include "Tha60210061QsgmiiSerdesPrbsEngineReg.h"
#include "Tha60210061SerdesPrbsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define cMonitorDataErrorMask(engineId)      (cBit0 << (engineId))
#define cMonitorPacketLenErrorMask(engineId) (cBit4 << (engineId))
#define cMonitorRxFcsErrorMask(engineId)     (cBit8 << (engineId))
#define cMonitorRxDataSyncErrorMask(engineId)(cBit12 << (engineId))
#define cMonitorRxDataSyncErrorShift(engineId) ((engineId) + 12)

#define cEnableMask(engineId)                (cBit0 << (engineId))
#define cEnableShift(engineId)               (0 + (engineId))
#define cForceErrorMask(engineId)            (cBit4 << (engineId))
#define cForceErrorShift(engineId)           (4 + (engineId))

#define mThis(self)         ((tTha60210061SgmiiSerdesPrbsEngine*)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPrbsEngineMethods m_AtPrbsEngineOverride;
static tTha60210061QsgmiiSerdesPrbsEngineMethods m_Tha60210061QsgmiiSerdesPrbsEngineOverride;

/* Save super implementation */
static const tAtPrbsEngineMethods *m_AtPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(Tha60210061QsgmiiSerdesPrbsEngine self)
    {
    AtUnused(self);
    return 0xf56000;
    }

static uint32 OffSet(AtPrbsEngine self)
    {
    Tha60210061QsgmiiSerdesPrbsEngine engine = (Tha60210061QsgmiiSerdesPrbsEngine)self;
    return mMethodsGet(engine)->BaseAddress(engine);
    }

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    uint32 engineId = AtPrbsEngineIdGet(self);
    uint32 regAddr  = cAf6Reg_control_global1_pen_Base + OffSet(self);
    uint32 regValue = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);

    mFieldIns(&regValue, cEnableMask(engineId), cEnableShift(engineId), mBoolToBin(enable));
    AtPrbsEngineWrite(self, regAddr, regValue, cAtModulePrbs);

    return cAtOk;
    }

static eBool IsEnabled(AtPrbsEngine self)
    {
    uint32 regAddr, regValue, engineId;
    uint8 hwEnable;

    engineId = AtPrbsEngineIdGet(self);
    regAddr  = cAf6Reg_control_global1_pen_Base + OffSet(self);
    regValue = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    mFieldGet(regValue, cEnableMask(engineId), cEnableShift(engineId), uint8, &hwEnable);

    return hwEnable ? cAtTrue : cAtFalse;
    }

static eAtModulePrbsRet ErrorForce(AtPrbsEngine self, eBool force)
    {
    uint32 engineId = AtPrbsEngineIdGet(self);
    uint32 regAddr  = cAf6Reg_control_global1_pen_Base + OffSet(self);
    uint32 regValue = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);

    mFieldIns(&regValue, cForceErrorMask(engineId), cForceErrorShift(engineId), mBoolToBin(force));
    AtPrbsEngineWrite(self, regAddr, regValue, cAtModulePrbs);

    return cAtOk;
    }

static eBool ErrorIsForced(AtPrbsEngine self)
    {
    uint32 engineId, regAddr, regValue;
    uint8 hwForced;

    engineId = AtPrbsEngineIdGet(self);
    regAddr  = cAf6Reg_control_global1_pen_Base + OffSet(self);
    regValue = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    mFieldGet(regValue, cForceErrorMask(engineId), cForceErrorShift(engineId), uint8, &hwForced);

    return hwForced ? cAtTrue : cAtFalse;
    }

static uint32 AlarmHistoryHelper(AtPrbsEngine self, eBool readToClear)
    {
    uint32 engineId = AtPrbsEngineIdGet(self);
    uint32 regAddr  = cAf6Reg_alarm_pen_Base + OffSet(self);
    uint32 regValue = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    uint32 alarm    = 0;
    eBool dataLosSyns;

    if (regValue & (cMonitorDataErrorMask(engineId)        |
                    cMonitorPacketLenErrorMask(engineId)   |
                    cMonitorRxFcsErrorMask(engineId)))
        alarm |= cAtPrbsEngineAlarmTypeError;

    mFieldGet(regValue, cMonitorRxDataSyncErrorMask(engineId), cMonitorRxDataSyncErrorShift(engineId), eBool, &dataLosSyns);
    if (!dataLosSyns)
        alarm |= cAtPrbsEngineAlarmTypeLossSync;

    if (readToClear)
        AtPrbsEngineWrite(self, regAddr, regValue, cAtModulePrbs);

    return alarm;
    }

static uint32 AlarmHistoryGet(AtPrbsEngine self)
    {
    return AlarmHistoryHelper(self, cAtFalse);
    }

static uint32 AlarmHistoryClear(AtPrbsEngine self)
    {
    return AlarmHistoryHelper(self, cAtTrue);
    }

static uint32 CounterGetAddress(AtPrbsEngine self)
    {
    uint32 engineId = AtPrbsEngineIdGet(self);

    if (engineId == 0) return cAf6Reg_counter_channel0_Base;
    if (engineId == 1) return cAf6Reg_counter_channel1_Base;
    if (engineId == 2) return cAf6Reg_counter_channel2_Base;

    /* Engine 3 */
    return cAf6Reg_counter_channel3_Base;
    }

static uint32 TxCounterClearAddress(AtPrbsEngine self)
    {
    uint32 engineId = AtPrbsEngineIdGet(self);

    if (engineId == 0) return cAf6Reg_tx_counter_clear_channel0_Base;
    if (engineId == 1) return cAf6Reg_tx_counter_clear_channel1_Base;
    if (engineId == 2) return cAf6Reg_tx_counter_clear_channel2_Base;

    /* Engine 3 */
    return cAf6Reg_tx_counter_clear_channel3_Base;
    }

static uint32 RxCounterClearAddress(AtPrbsEngine self)
    {
    uint32 engineId = AtPrbsEngineIdGet(self);

    if (engineId == 0) return cAf6Reg_rx_counter_clear_channel0_Base;
    if (engineId == 1) return cAf6Reg_rx_counter_clear_channel1_Base;
    if (engineId == 2) return cAf6Reg_rx_counter_clear_channel2_Base;

    /* Engine 3 */
    return cAf6Reg_rx_counter_clear_channel3_Base;
    }

static uint32 CounterGet(AtPrbsEngine self, uint16 counterType)
    {
    uint32 regAddress, regValue;
    uint32 counterAllChannel = 0;

    if (counterType == cAtPrbsEngineCounterTxFrame)
        {
        regAddress = CounterGetAddress(self) + OffSet(self);
        regValue = AtPrbsEngineRead(self, regAddress, cAtModulePrbs);
        counterAllChannel = mRegField(regValue, cAf6_counter_channel0_Tx_counter_);
        }

    if (counterType == cAtPrbsEngineCounterRxFrame)
        {
        regAddress = CounterGetAddress(self) + OffSet(self);
        regValue = AtPrbsEngineRead(self, regAddress, cAtModulePrbs);
        counterAllChannel = mRegField(regValue, cAf6_counter_channel0_Rx_counter_);
        }

    return counterAllChannel;
    }

static uint32 CounterClear(AtPrbsEngine self, uint16 counterType)
    {
    uint32 regAddress, regValue;
    uint32 counterAllChannel = 0;

    if (counterType == cAtPrbsEngineCounterTxFrame)
        {
        regAddress = TxCounterClearAddress(self) + OffSet(self);
        regValue = AtPrbsEngineRead(self, regAddress, cAtModulePrbs);
        counterAllChannel = mRegField(regValue, cAf6_tx_counter_clear_channel0_);
        }

    if (counterType == cAtPrbsEngineCounterRxFrame)
        {
        regAddress = RxCounterClearAddress(self) + OffSet(self);
        regValue = AtPrbsEngineRead(self, regAddress, cAtModulePrbs);
        counterAllChannel = mRegField(regValue, cAf6_rx_counter_clear_channel0_);
        }

    return counterAllChannel;
    }

static void OverrideAtPrbsEngineSerdes(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, mMethodsGet(self), sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Enable);
        mMethodOverride(m_AtPrbsEngineOverride, IsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, CounterGet);
        mMethodOverride(m_AtPrbsEngineOverride, CounterClear);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorForce);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorIsForced);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryClear);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryGet);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void OverrideTha60210061QsgmiiSerdesPrbsEngine(AtPrbsEngine self)
    {
    Tha60210061QsgmiiSerdesPrbsEngine engine = (Tha60210061QsgmiiSerdesPrbsEngine)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210061QsgmiiSerdesPrbsEngineOverride, mMethodsGet(engine), sizeof(m_Tha60210061QsgmiiSerdesPrbsEngineOverride));

        mMethodOverride(m_Tha60210061QsgmiiSerdesPrbsEngineOverride, BaseAddress);
        }

    mMethodsSet(engine, &m_Tha60210061QsgmiiSerdesPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtPrbsEngineSerdes(self);
    OverrideTha60210061QsgmiiSerdesPrbsEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061SgmiiSerdesPrbsEngine);
    }

AtPrbsEngine Tha60210061SgmiiSerdesPrbsEngineObjectInit(AtPrbsEngine self, AtSerdesController serdesController)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210061QsgmiiSerdesPrbsEngineObjectInit(self, serdesController) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha60210061SgmiiSerdesPrbsEngineNew(AtSerdesController serdesController)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return Tha60210061SgmiiSerdesPrbsEngineObjectInit(newEngine, serdesController);
    }

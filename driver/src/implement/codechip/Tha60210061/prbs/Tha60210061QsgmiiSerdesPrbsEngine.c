/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60210061QsgmiiSerdesPrbsEngine.c
 *
 * Created Date: Jun 22, 2016
 *
 * Description : PRBS engine of Qsgmii SERDES
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/eth/AtModuleEthInternal.h"
#include "../../../default/eth/ThaModuleEth.h"
#include "../physical/Tha60210061SerdesManager.h"
#include "Tha60210061ModulePrbs.h"
#include "Tha60210061QsgmiiSerdesPrbsEngineReg.h"
#include "Tha60210061SerdesPrbsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210061QsgmiiSerdesPrbsEngine)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210061QsgmiiSerdesPrbsEngineMethods m_methods;

/* Override */
static tAtPrbsEngineMethods m_AtPrbsEngineOverride;

/* Save super implementation */
static const tAtPrbsEngineMethods *m_AtPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 OffSet(AtPrbsEngine self)
    {
    return mMethodsGet(mThis(self))->BaseAddress(mThis(self));
    }

static eAtRet DestMacSet(AtPrbsEngine self, uint8* mac)
    {
    uint32 hwMacAddress = 0;
    uint32 regAddr;

    if (mac == NULL)
        return cAtErrorNullPointer;

    hwMacAddress  = (uint32)(mac[0] << 24);
    hwMacAddress |= (uint32)(mac[1] << 16);
    hwMacAddress |= (uint32)(mac[2] << 8);
    hwMacAddress |= (uint32)(mac[3]);
    regAddr = cAf6Reg_control_dest_mac0_pen_Base + OffSet(self);
    AtPrbsEngineWrite(self, regAddr, hwMacAddress, cAtModulePrbs);

    hwMacAddress  = (uint32)(mac[4]);
    hwMacAddress |= (uint32)(mac[5] << 8);
    regAddr = cAf6Reg_control_dest_mac1_pen_Base + OffSet(self);
    AtPrbsEngineWrite(self, regAddr, hwMacAddress, cAtModulePrbs);

    return cAtOk;
    }

static eAtRet SrcMacSet(AtPrbsEngine self, uint8* mac)
    {
    uint32 hwMacAddress = 0;
    uint32 regAddr;

    if (mac == NULL)
        return cAtErrorNullPointer;

    hwMacAddress  = (uint32)(mac[0] << 24);
    hwMacAddress |= (uint32)(mac[1] << 16);
    hwMacAddress |= (uint32)(mac[2] << 8);
    hwMacAddress |= (uint32)(mac[3]);
    regAddr = cAf6Reg_control_src_mac0_pen_Base + OffSet(self);
    AtPrbsEngineWrite(self, regAddr, hwMacAddress, cAtModulePrbs);

    hwMacAddress  = (uint32)(mac[4]);
    hwMacAddress |= (uint32)(mac[5] << 8);
    regAddr = cAf6Reg_control_src_mac1_pen_Base + OffSet(self);
    AtPrbsEngineWrite(self, regAddr, hwMacAddress, cAtModulePrbs);

    return cAtOk;
    }

static eAtRet EthernetTypeSet(AtPrbsEngine self, uint16 ethType)
    {
    AtPrbsEngineWrite(self, cAf6Reg_control_eth_type_Base + OffSet(self), ethType, cAtModulePrbs);
    return cAtOk;
    }

static void MacInit(AtPrbsEngine self)
    {
    static uint8 destMac[] = {0xC0, 0xCA, 0xC0, 0xCA, 0xC0, 0xCA};
    static uint8 srcMac[] = {0xCA, 0xFE, 0xCA, 0xFE, 0xCA, 0xFE};
    DestMacSet(self, destMac);
    SrcMacSet(self, srcMac);
    EthernetTypeSet(self, 0x8847);
    }

static void HwDefault(AtPrbsEngine self)
    {
    uint32 regAddr, regValue;
    static const uint16 cDefaultPayload = 1500;
    static const uint8 cPacketGap = 0;/* 12Byte */

    /* Default payload */
    regAddr  = cAf6Reg_control_global2_pen_Base + OffSet(self);
    regValue = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    mRegFieldSet(regValue, cAf6_control_global2_pen_Config_data_length_, cDefaultPayload);
    AtPrbsEngineWrite(self, regAddr, regValue,cAtModulePrbs);

    /* Default packet GAP 12bytes */
    regAddr  = cAf6Reg_control_global3_pen_Base + OffSet(self);
    regValue = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    mRegFieldSet(regValue, cAf6_control_global3_pen_Config_number_inter_package_gap_, cPacketGap);
    AtPrbsEngineWrite(self, regAddr, regValue,cAtModulePrbs);

    /* Enable invert FCS data */
    regAddr  = cAf6Reg_control_global3_pen_Base + OffSet(self);
    regValue = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    mRegFieldSet(regValue, cAf6_control_global3_pen_Config_invert_FCS_data_, 1);
    AtPrbsEngineWrite(self, regAddr, regValue,cAtModulePrbs);
    }

static void HwDefaultSet(Tha60210061QsgmiiSerdesPrbsEngine self)
    {
    MacInit((AtPrbsEngine)self);
    HwDefault((AtPrbsEngine)self);
    }

static eAtModulePrbsRet Init(AtPrbsEngine self)
    {
    eAtRet ret = m_AtPrbsEngineMethods->Init(self);

    ret |= AtPrbsEngineErrorForce(self, cAtFalse);
    ret |= AtPrbsEngineEnable(self, cAtFalse);

    mMethodsGet(mThis(self))->HwDefaultSet(mThis(self));
    return ret;
    }

static eAtModulePrbsRet ErrorForce(AtPrbsEngine self, eBool force)
    {
    uint32 regAddr  = cAf6Reg_control_global1_pen_Base + OffSet(self);
    uint32 regValue = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);

    mRegFieldSet(regValue, cAf6_control_global1_pen_Force_data_error_, force ? 0xF : 0x0);
    AtPrbsEngineWrite(self, regAddr, regValue, cAtModulePrbs);

    return cAtOk;
    }

static eBool ErrorIsForced(AtPrbsEngine self)
    {
    uint32 regAddr  = cAf6Reg_control_global1_pen_Base + OffSet(self);
    uint32 regValue = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    return mBinToBool(mRegField(regValue, cAf6_control_global1_pen_Force_data_error_));
    }

static uint32 AlarmHistoryHelper(AtPrbsEngine self, eBool readToClear)
    {
    uint32 regAddr  = cAf6Reg_alarm_pen_Base + OffSet(self);
    uint32 regValue = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    uint32 alarm = 0;

    if (regValue & (cAf6_alarm_pen_Monitor_data_error_Mask        |
                    cAf6_alarm_pen_Monitor_package_len_error_Mask |
                    cAf6_alarm_pen_Monitor_Rx_FCS_error_Mask))
        alarm |= cAtPrbsEngineAlarmTypeError;
        
    if (mRegField(regValue, cAf6_alarm_pen_Monitor_Rx_Data_sync_) == 0x0)
        alarm |= cAtPrbsEngineAlarmTypeLossSync;

    if (readToClear)
        AtPrbsEngineWrite(self, regAddr, regValue, cAtModulePrbs);

    return alarm;
    }

static uint32 AlarmHistoryGet(AtPrbsEngine self)
    {
    return AlarmHistoryHelper(self, cAtFalse);
    }

static uint32 AlarmHistoryClear(AtPrbsEngine self)
    {
    return AlarmHistoryHelper(self, cAtTrue);
    }

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    const uint8 cAllLanesEnable = 0xf;
    uint32 regAddr  = cAf6Reg_control_global1_pen_Base + OffSet(self);
    uint32 regValue = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);

    mRegFieldSet(regValue, cAf6_control_global1_pen_Port_enable_, enable ? cAllLanesEnable : 0x0);
    AtPrbsEngineWrite(self, regAddr, regValue, cAtModulePrbs);

    return cAtOk;
    }

static eBool IsEnabled(AtPrbsEngine self)
    {
    uint32 regAddr  = cAf6Reg_control_global1_pen_Base + OffSet(self);
    uint32 regValue = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    return mBinToBool(mRegField(regValue, cAf6_control_global1_pen_Port_enable_));
    }

static uint8 ModeSw2Hw(eAtPrbsMode prbsMode)
    {
    if (prbsMode == cAtPrbsModePrbs15)                return 3;
    if (prbsMode == cAtPrbsModePrbsSeq)               return 1;
    if (prbsMode == cAtPrbsModePrbsFixedPattern1Byte) return 0;

    return 0xF;
    }

static eAtModulePrbsRet ModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    uint32 regAddr, regValue;

    if (!AtPrbsEngineModeIsSupported(self, prbsMode))
        return cAtErrorModeNotSupport;

    regAddr  = cAf6Reg_control_global2_pen_Base + OffSet(self);
    regValue = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    mRegFieldSet(regValue, cAf6_control_global2_pen_Config_data_mode_, ModeSw2Hw(prbsMode));
    AtPrbsEngineWrite(self, regAddr, regValue, cAtModulePrbs);

    return cAtOk;
    }

static eAtPrbsMode ModeHw2Sw(uint32 hwMode)
    {
    if (hwMode == 0x3) return cAtPrbsModePrbs15;
    if (hwMode == 0x1) return cAtPrbsModePrbsSeq;
    if (hwMode == 0x0) return cAtPrbsModePrbsFixedPattern1Byte;

    return cAtPrbsModeInvalid;
    }

static eAtPrbsMode ModeGet(AtPrbsEngine self)
    {
    uint32 regAddr  = cAf6Reg_control_global2_pen_Base + OffSet(self);
    uint32 regValue = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    return ModeHw2Sw(mRegField(regValue, cAf6_control_global2_pen_Config_data_mode_));
    }

static eBool ModeIsSupported(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    AtUnused(self);

    if ((prbsMode == cAtPrbsModePrbs15) ||
        (prbsMode == cAtPrbsModePrbsSeq)||
        (prbsMode == cAtPrbsModePrbsFixedPattern1Byte))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 TxFrameCounterGet(AtPrbsEngine self)
    {
    uint32 regAddress = cAf6Reg_counter_channel0_Base + OffSet(self);
    uint32 regValue = AtPrbsEngineRead(self, regAddress, cAtModulePrbs);
    uint32 counterAllChannel = mRegField(regValue, cAf6_counter_channel0_Tx_counter_);

    regAddress = cAf6Reg_counter_channel1_Base + OffSet(self);
    regValue += AtPrbsEngineRead(self, regAddress, cAtModulePrbs);
    counterAllChannel += mRegField(regValue, cAf6_counter_channel1_Tx_counter_);

    regAddress = cAf6Reg_counter_channel2_Base + OffSet(self);
    regValue += AtPrbsEngineRead(self, regAddress, cAtModulePrbs);
    counterAllChannel += mRegField(regValue, cAf6_counter_channel2_Tx_counter_);

    regAddress = cAf6Reg_counter_channel3_Base + OffSet(self);
    regValue += AtPrbsEngineRead(self, regAddress, cAtModulePrbs);
    counterAllChannel += mRegField(regValue, cAf6_counter_channel3_Tx_counter_);

    return counterAllChannel;
    }

static uint32 RxFrameCounterGet(AtPrbsEngine self)
    {
    uint32 regAddress = cAf6Reg_counter_channel0_Base + OffSet(self);
    uint32 regValue = AtPrbsEngineRead(self, regAddress, cAtModulePrbs);
    uint32 counterAllChannel = mRegField(regValue, cAf6_counter_channel0_Rx_counter_);

    regAddress = cAf6Reg_counter_channel1_Base + OffSet(self);
    regValue += AtPrbsEngineRead(self, regAddress, cAtModulePrbs);
    counterAllChannel += mRegField(regValue, cAf6_counter_channel1_Rx_counter_);

    regAddress = cAf6Reg_counter_channel2_Base + OffSet(self);
    regValue += AtPrbsEngineRead(self, regAddress, cAtModulePrbs);
    counterAllChannel += mRegField(regValue, cAf6_counter_channel2_Rx_counter_);

    regAddress = cAf6Reg_counter_channel3_Base + OffSet(self);
    regValue += AtPrbsEngineRead(self, regAddress, cAtModulePrbs);
    counterAllChannel += mRegField(regValue, cAf6_counter_channel3_Rx_counter_);

    return counterAllChannel;
    }

static uint32 CounterGet(AtPrbsEngine self, uint16 counterType)
    {
    if (counterType == cAtPrbsEngineCounterTxFrame)
        return TxFrameCounterGet(self);

    if (counterType == cAtPrbsEngineCounterRxFrame)
        return RxFrameCounterGet(self);

    return 0;
    }

static uint32 TxFrameCounterClear(AtPrbsEngine self)
    {
    uint32 regAddress = cAf6Reg_tx_counter_clear_channel0_Base + OffSet(self);
    uint32 regValue = AtPrbsEngineRead(self, regAddress, cAtModulePrbs);
    uint32 counterAllChannel = mRegField(regValue, cAf6_tx_counter_clear_channel0_);

    regAddress = cAf6Reg_tx_counter_clear_channel1_Base + OffSet(self);
    regValue += AtPrbsEngineRead(self, regAddress, cAtModulePrbs);
    counterAllChannel += mRegField(regValue, cAf6_tx_counter_clear_channel1_);

    regAddress = cAf6Reg_tx_counter_clear_channel2_Base + OffSet(self);
    regValue += AtPrbsEngineRead(self, regAddress, cAtModulePrbs);
    counterAllChannel += mRegField(regValue, cAf6_tx_counter_clear_channel2_);

    regAddress = cAf6Reg_tx_counter_clear_channel3_Base + OffSet(self);
    regValue += AtPrbsEngineRead(self, regAddress, cAtModulePrbs);
    counterAllChannel += mRegField(regValue, cAf6_tx_counter_clear_channel3_);

    return counterAllChannel;
    }

static uint32 RxFrameCounterClear(AtPrbsEngine self)
    {
    uint32 regAddress = cAf6Reg_rx_counter_clear_channel0_Base + OffSet(self);
    uint32 regValue = AtPrbsEngineRead(self, regAddress, cAtModulePrbs);
    uint32 counterAllChannel = mRegField(regValue, cAf6_rx_counter_clear_channel0_);

    regAddress = cAf6Reg_rx_counter_clear_channel1_Base + OffSet(self);
    regValue += AtPrbsEngineRead(self, regAddress, cAtModulePrbs);
    counterAllChannel += mRegField(regValue, cAf6_rx_counter_clear_channel1_);

    regAddress = cAf6Reg_rx_counter_clear_channel2_Base + OffSet(self);
    regValue += AtPrbsEngineRead(self, regAddress, cAtModulePrbs);
    counterAllChannel += mRegField(regValue, cAf6_rx_counter_clear_channel2_);

    regAddress = cAf6Reg_rx_counter_clear_channel3_Base + OffSet(self);
    regValue += AtPrbsEngineRead(self, regAddress, cAtModulePrbs);
    counterAllChannel += mRegField(regValue, cAf6_rx_counter_clear_channel3_);

    return counterAllChannel;
    }

static uint32 CounterClear(AtPrbsEngine self, uint16 counterType)
    {
    if (counterType == cAtPrbsEngineCounterTxFrame)
        return TxFrameCounterClear(self);

    if (counterType == cAtPrbsEngineCounterRxFrame)
        return RxFrameCounterClear(self);

    return 0;
    }

static eAtModulePrbsRet FixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    uint32 regAddr, regValue;

    if (ModeGet(self) != cAtPrbsModePrbsFixedPattern1Byte)
        return cAtErrorModeNotSupport;

    regAddr  = cAf6Reg_control_global2_pen_Base + OffSet(self);
    regValue = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    mRegFieldSet(regValue, cAf6_control_global2_pen_Config_fix_data_, fixedPattern);
    AtPrbsEngineWrite(self, regAddr, regValue, cAtModulePrbs);

    return cAtOk;
    }

static uint32 FixedPatternGet(AtPrbsEngine self)
    {
    uint32 regAddr, regValue;

    if (ModeGet(self) != cAtPrbsModePrbsFixedPattern1Byte)
        return cAtErrorModeNotSupport;

    regAddr  = cAf6Reg_control_global2_pen_Base + OffSet(self);
    regValue = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    return mRegField(regValue, cAf6_control_global2_pen_Config_fix_data_);
    }

static const char *HwGap2String(uint32 hwPktGap)
    {
    if (hwPktGap == 0) return "12bytes";
    if (hwPktGap == 1) return "24bytes";
    if (hwPktGap == 2) return "36bytes";
    if (hwPktGap == 3) return "48bytes";

    return "none";
    }

static void Debug(AtPrbsEngine self)
    {
    uint32 regAddr, regValue;

    AtPrintc(cSevNormal, "QSGMII:       %u\r\n", AtPrbsEngineIdGet(self) + 1);

    regAddr  = cAf6Reg_control_global2_pen_Base + OffSet(self);
    regValue = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    AtPrintc(cSevNormal, "Payload Size: %u\r\n", mRegField(regValue, cAf6_control_global2_pen_Config_data_length_));

    regAddr  = cAf6Reg_control_global3_pen_Base + OffSet(self);
    regValue = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    AtPrintc(cSevNormal, "Packet Gap: %s\r\n", HwGap2String(mRegField(regValue, cAf6_control_global3_pen_Config_number_inter_package_gap_)));

    regAddr  = cAf6Reg_control_global3_pen_Base + OffSet(self);
    regValue = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    AtPrintc(cSevNormal, "FCS Invert: %s\r\n", mRegField(regValue, cAf6_control_global3_pen_Config_invert_FCS_data_) ? "en" : "dis");
    }

static uint32 BaseAddress(Tha60210061QsgmiiSerdesPrbsEngine self)
    {
    if (AtPrbsEngineIdGet((AtPrbsEngine)self) == cTha60210061QsgmiiSerdesId1)
        return 0xF55000; /* Port#2 */

    /* Port#1 */
    return 0xF54000;
    }

static eBool CounterIsSupported(AtPrbsEngine self, uint16 counterType)
    {
    AtUnused(self);

    if ((counterType == cAtPrbsEngineCounterTxFrame) ||
        (counterType == cAtPrbsEngineCounterRxFrame))
        return cAtTrue;

    return cAtFalse;
    }

static eBool EthernetFrameIsSupported(AtPrbsEngine self)
    {
    /* Serdes Id and engine ID are equal */
    uint32 serdesId = AtPrbsEngineIdGet(self);

    if ((serdesId == cTha60210061QsgmiiSerdesId0) ||
        (serdesId == cTha60210061QsgmiiSerdesId1) ||
        (serdesId == cTha60210061SgmiiSerdesId0)  ||
        (serdesId == cTha60210061SgmiiSerdesId1)  ||
        (serdesId == cTha60210061SgmiiSerdesId2)  ||
        (serdesId == cTha60210061SgmiiSerdesId3))
        return cAtTrue;

    return cAtFalse;
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, mMethodsGet(self), sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Init);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorForce);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorIsForced);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryClear);
        mMethodOverride(m_AtPrbsEngineOverride, Enable);
        mMethodOverride(m_AtPrbsEngineOverride, IsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, ModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, CounterGet);
        mMethodOverride(m_AtPrbsEngineOverride, CounterClear);
        mMethodOverride(m_AtPrbsEngineOverride, CounterIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, Debug);
        mMethodOverride(m_AtPrbsEngineOverride, FixedPatternSet);
        mMethodOverride(m_AtPrbsEngineOverride, FixedPatternGet);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtPrbsEngine(self);
    }

static void MethodsInit(Tha60210061QsgmiiSerdesPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, BaseAddress);
        mMethodOverride(m_methods, DestMacSet);
        mMethodOverride(m_methods, SrcMacSet);
        mMethodOverride(m_methods, EthernetTypeSet);
        mMethodOverride(m_methods, HwDefaultSet);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061QsgmiiSerdesPrbsEngine);
    }

AtPrbsEngine Tha60210061QsgmiiSerdesPrbsEngineObjectInit(AtPrbsEngine self, AtSerdesController serdesController)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPrbsEngineObjectInit(self,
                               AtSerdesControllerPhysicalPortGet(serdesController),
                               AtSerdesControllerIdGet(serdesController)) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(mThis(self));
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha60210061QsgmiiSerdesPrbsEngineNew(AtSerdesController serdesController)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return Tha60210061QsgmiiSerdesPrbsEngineObjectInit(newEngine, serdesController);
    }

eAtRet Tha60210061SerdesPrbsEngineDestMacSet(AtPrbsEngine self, uint8* mac)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (!EthernetFrameIsSupported(self))
        return cAtErrorModeNotSupport;

    return mMethodsGet(mThis(self))->DestMacSet(self, mac);
    }

eAtRet Tha60210061SerdesPrbsEngineSrcMacSet(AtPrbsEngine self, uint8* mac)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (!EthernetFrameIsSupported(self))
        return cAtErrorModeNotSupport;

    return mMethodsGet(mThis(self))->SrcMacSet(self, mac);
    }

eAtRet Tha60210061SerdesPrbsEngineEthernetTypeSet(AtPrbsEngine self, uint16 ethType)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (!EthernetFrameIsSupported(self))
        return cAtErrorModeNotSupport;

    return mMethodsGet(mThis(self))->EthernetTypeSet(self, ethType);
    }

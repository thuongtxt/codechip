/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60210061AuVcPrbsEngineEc1.c
 *
 * Created Date: Aug 24, 2017
 *
 * Description : AU VC Prbs Engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/prbs/ThaPrbsEngineAuVcInternal.h"
#include "../../../default/ocn/ThaModuleOcn.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061AuVcPrbsEngineEc1
    {
    tThaPrbsEngineAuVc super;
    }tTha60210061AuVcPrbsEngineEc1;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPrbsEngineMethods m_ThaPrbsEngineOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ChannelId(ThaPrbsEngine self)
    {
    uint8 slice24, hwSts24, slice48;
    AtSdhChannel vc = (AtSdhChannel)AtPrbsEngineChannelGet((AtPrbsEngine)self);

    /* Channel ID: 14 bit
     * Bit 13->11: slice48
     * Bit 10->6 : sts24
     * Bit 5     : slice24
     * bit4_0    : vtgId
     *  */
    ThaSdhChannelHwStsGet(vc, cAtModulePrbs, AtSdhChannelSts1Get(vc), &slice24, &hwSts24);
    slice48 = slice24 / 2;
    return (uint32)(slice24 << 5) | (uint32)(hwSts24 << 6) | (uint32)(slice48 << 11);
    }

static void OverrideThaPrbsEngine(ThaPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPrbsEngineOverride, mMethodsGet(self), sizeof(m_ThaPrbsEngineOverride));

        mMethodOverride(m_ThaPrbsEngineOverride, ChannelId);
        }

    mMethodsSet(self, &m_ThaPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideThaPrbsEngine((ThaPrbsEngine)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061AuVcPrbsEngineEc1);
    }

static AtPrbsEngine ObjectInit(AtPrbsEngine self, AtSdhChannel vc, uint32 engineId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPrbsEngineAuVcObjectInit(self, vc, engineId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha60210061AuVcPrbsEngineEc1New(AtSdhChannel vc, uint32 engineId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newEngine, vc, engineId);
    }

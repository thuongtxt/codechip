/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Module PRBS
 * 
 * File        : Tha60210061ModulePrbs.h
 * 
 * Created Date: Jul 22, 2016
 *
 * Description : Module PRBS
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061MODULEPRBS_H_
#define _THA60210061MODULEPRBS_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"
#include "AtPrbsEngine.h"
#include "AtSerdesController.h"
#include "AtEthPort.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eBool Tha60210061ModulePrbsSgmiiQsgmiiPerPortControl(AtModulePrbs self);
eAtRet Tha60210061DiagEthFramePrbsGenEngineEnable(AtModulePrbs self, eBool enable);
eBool Tha60210061ModulePrbsRawPrsbIsSupported(AtModulePrbs self);
eBool Tha60210061ModulePrbsEthFramePrbsIsSupported(AtModulePrbs self);
eAtRet Tha60210061ModulePrbsCommonGenEngineDefautSet(AtModulePrbs self);
uint32 Tha60210061PrbsEngineVc1xEc1ChannelId(AtPrbsEngine self, uint8 slice24, uint8 hwSts24, uint8 vtgId, uint8 vtId);
uint32 Tha60210061PrbsEngineDe1Ec1ChannelId(AtPrbsEngine self, AtPdhDe1 de1);

/* SERDES TOP PRBS, Ethernet frame PRBS implemented at TOP */
AtPrbsEngine Tha60210061QsgmiiSerdesPrbsEngineNew(AtSerdesController serdesController);
AtPrbsEngine Tha60210061SgmiiSerdesPrbsEngineNew(AtSerdesController serdesController);
AtPrbsEngine Tha60210061SgmiiSerdesPrbsEngineV2New(AtSerdesController serdesController);
AtPrbsEngine Tha60210061QsgmiiSerdesPrbsEngineV2New(AtSerdesController serdesController);

/* SERDES Ethernet frame PRBS implemented in core */
AtPrbsEngine Tha60210061EthFramePrbsEngineNew(AtEthPort port, uint32 engineId);
AtPrbsEngine Tha60210061SerdesEthFramePrbsEngineNew(AtSerdesController serdesController);

/* SERDES raw PRBS, using logic of verdor's SERDES controller */
AtPrbsEngine Tha60210061RxauiSerdesRawPrbsEngineNew(AtSerdesController serdesController);
AtPrbsEngine Tha60210061GthSerdesRawPrbsEngineNew(AtSerdesController serdesController);
AtPrbsEngine Tha60210061SgmiiSerdesRawPrbsEngineNew(AtSerdesController serdesController);
AtPrbsEngine Tha60210061XfiSerdesRawPrbsEngineNew(AtSerdesController serdesController);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061MODULEPRBS_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60210061SgmiiSerdesControllerRawPrbsEngine.c
 *
 * Created Date: Feb 14, 2017
 *
 * Description : SGMII Serdes Raw Prbs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../physical/Tha60210061SerdesManager.h"
#include "../physicalV2/Tha60210061SerdesControlV2Reg.h"
#include "Tha60210061ModulePrbs.h"
#include "Tha60210061SerdesRawPrbsEngineReg.h"
#include "Tha60210061SerdesPrbsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210061SgmiiSerdesRawPrbsEngine)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210061SgmiiSerdesRawPrbsEngineMethods m_methods;

/* Override */
static tAtObjectMethods      m_AtObjectOverride;
static tAtPrbsEngineMethods  m_AtPrbsEngineOverride;

/* Save super implementation */
static const tAtObjectMethods     *m_AtObjectMethods     = NULL;
static const tAtPrbsEngineMethods *m_AtPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 HssiSgmiiSerdesId(AtPrbsEngine self)
    {
    return mMethodsGet(mThis(self))->HssiSerdesId(mThis(self));
    }

static uint32 HssiSerdesId(Tha60210061SgmiiSerdesRawPrbsEngine self)
    {
    AtUnused(self);
    return 0x0;
    }

static uint32 CounterOffset(Tha60210061SgmiiSerdesRawPrbsEngine self)
    {
    AtUnused(self);
    return cCounterLowDrpAddressGtyE3;
    }

static uint32 HssiPortId(AtPrbsEngine self)
    {
    return mMethodsGet(mThis(self))->PortSerdesId(mThis(self));
    }

static uint32 PortSerdesId(Tha60210061SgmiiSerdesRawPrbsEngine self)
    {
    return AtSerdesControllerIdGet(mThis(self)->serdesController);
    }

static uint32 ConfigureOffset(AtPrbsEngine self)
    {
    return cSerdesTuningV2BaseAddress + HssiPortId(self) * 0x800 + HssiSgmiiSerdesId(self) * 0x40;
    }

static uint32 ControlledByArriveRegisterEnable(Tha60210061SgmiiSerdesRawPrbsEngine self)
    {
    AtPrbsEngine prbsEngine = (AtPrbsEngine)self;
    uint32 address1 = cAf6Reg_upen_addr_20_Base + ConfigureOffset(prbsEngine);
    uint32 regVal1 = 0;
    uint32 address2 = cAf6Reg_upen_addr_30_Base + ConfigureOffset(prbsEngine);
    uint32 regVal2 = 0;

    mRegFieldSet(regVal1, cAf6_upen_addr_20_TX_ARRV_REG_, 0x1);
    mRegFieldSet(regVal2, cAf6_upen_addr_30_RX_ARRV_REG_, 0x1);

    AtPrbsEngineWrite(prbsEngine, address1, regVal1, cAtModulePrbs);
    AtPrbsEngineWrite(prbsEngine, address2, regVal2, cAtModulePrbs);

    return cAtOk;
    }

static uint8 SwMode2HwMode(eAtPrbsMode mode)
    {
    if (mode == cAtPrbsModePrbs7)  return 1;
    if (mode == cAtPrbsModePrbs9)  return 2;
    if (mode == cAtPrbsModePrbs15) return 3;
    if (mode == cAtPrbsModePrbs23) return 4;
    if (mode == cAtPrbsModePrbs31) return 5;

    return cInvalidUint8;
    }

static eAtModulePrbsRet Init(AtPrbsEngine self)
    {
    eAtRet ret = m_AtPrbsEngineMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    ret  = mMethodsGet(mThis(self))->ControlledByArriveRegisterEnable(mThis(self));
    ret |= mMethodsGet(self)->Enable(self, cAtFalse);
    ret |= mMethodsGet(self)->ModeSet(self, cAtPrbsModePrbs15);

    return ret;
    }

static eAtModulePrbsRet RxMonitoringReset(AtPrbsEngine self)
    {
    uint32 address = cAf6Reg_upen_addr_30_Base + ConfigureOffset(self);
    uint32 regVal = AtPrbsEngineRead(self, address, cAtModulePrbs);

    mRegFieldSet(regVal, cAf6_upen_addr_30_RXPRBSCNTRESET_, 0x1);
    AtPrbsEngineWrite(self, address, regVal, cAtModulePrbs);

    mRegFieldSet(regVal, cAf6_upen_addr_30_RXPRBSCNTRESET_, 0x0);
    AtPrbsEngineWrite(self, address, regVal, cAtModulePrbs);
    return cAtOk;
    }

static eAtModulePrbsRet TxSerdesHwModeSet(AtPrbsEngine self, uint8 mode)
    {
    uint32 address, regVal;

    address = cAf6Reg_upen_addr_20_Base + ConfigureOffset(self);
    regVal = AtPrbsEngineRead(self, address, cAtModulePrbs);

    mRegFieldSet(regVal, cAf6_upen_addr_20_TX_PRBS_SEL_, mode);
    AtPrbsEngineWrite(self, address, regVal, cAtModulePrbs);

    return cAtOk;
    }

static eAtModulePrbsRet TxEnable(AtPrbsEngine self, eBool enable)
    {
    if (!enable)
        return TxSerdesHwModeSet(self, 0x0);

    return TxSerdesHwModeSet(self, SwMode2HwMode(AtPrbsEngineModeGet(self)));
    }

static eBool TxIsEnabled(AtPrbsEngine self)
    {
    uint32 address = cAf6Reg_upen_addr_20_Base + ConfigureOffset(self);
    uint32 regVal = AtPrbsEngineRead(self, address, cAtModulePrbs);

    return (regVal & cAf6_upen_addr_20_TX_PRBS_SEL_Mask) ? cAtTrue : cAtFalse;
    }

static eAtModulePrbsRet TxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    uint32 address = cAf6Reg_upen_addr_20_Base + ConfigureOffset(self);
    uint32 regVal = AtPrbsEngineRead(self, address, cAtModulePrbs);

    if (!AtPrbsEngineModeIsSupported(self, prbsMode))
        return cAtErrorModeNotSupport;

    if (TxIsEnabled(self))
        {
        mRegFieldSet(regVal, cAf6_upen_addr_20_TX_PRBS_SEL_, SwMode2HwMode(prbsMode));
        AtPrbsEngineWrite(self, address, regVal, cAtModulePrbs);
        }

    mThis(self)->txPrbsMode = prbsMode;
    return cAtOk;
    }

static eAtPrbsMode TxModeGet(AtPrbsEngine self)
    {
    return mThis(self)->txPrbsMode;
    }

static eAtModulePrbsRet RxSerdesHwModeSet(AtPrbsEngine self, uint8 mode)
    {
    uint32 address = cAf6Reg_upen_addr_30_Base + ConfigureOffset(self);
    uint32 regVal = AtPrbsEngineRead(self, address, cAtModulePrbs);

    mRegFieldSet(regVal, cAf6_upen_addr_30_RX_PRBS_SEL_, mode);
    AtPrbsEngineWrite(self, address, regVal, cAtModulePrbs);

    return cAtOk;
    }

static eAtModulePrbsRet RxEnable(AtPrbsEngine self, eBool enable)
    {
    if (!enable)
        return RxSerdesHwModeSet(self, 0x0);
    
    RxSerdesHwModeSet(self, SwMode2HwMode(AtPrbsEngineModeGet(self)));
    return RxMonitoringReset(self);
    }

static eBool RxIsEnabled(AtPrbsEngine self)
    {
    uint32 address = cAf6Reg_upen_addr_30_Base + ConfigureOffset(self);
    uint32 regVal = AtPrbsEngineRead(self, address, cAtModulePrbs);

    return (regVal & cAf6_upen_addr_30_RX_PRBS_SEL_Mask) ? cAtTrue : cAtFalse;
    }

static eAtModulePrbsRet RxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    uint32 address = cAf6Reg_upen_addr_30_Base + ConfigureOffset(self);
    uint32 regVal = AtPrbsEngineRead(self, address, cAtModulePrbs);

    if (!AtPrbsEngineModeIsSupported(self, prbsMode))
        return cAtErrorModeNotSupport;

    if (RxIsEnabled(self))
        {
        mRegFieldSet(regVal, cAf6_upen_addr_30_RX_PRBS_SEL_, SwMode2HwMode(prbsMode));
        AtPrbsEngineWrite(self, address, regVal, cAtModulePrbs);
        RxMonitoringReset(self);
        }

    mThis(self)->rxPrbsMode = prbsMode;
    return cAtOk;
    }

static eAtPrbsMode RxModeGet(AtPrbsEngine self)
    {
    return mThis(self)->rxPrbsMode;
    }

static eBool SeparateTwoDirections(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 AlarmGet(AtPrbsEngine self)
    {
    uint32 address = cAf6Reg_upen_addr_32_Base + ConfigureOffset(self);
    uint32 regVal = AtPrbsEngineRead(self, address, cAtModulePrbs);
    uint32 alarm = 0;

    if (regVal & cAf6_upen_addr_32_RO_PRBS_ERR_RO_Mask)
        alarm |= cAtPrbsEngineAlarmTypeError;

    if ((regVal & cAf6_upen_addr_32_RX_PRBS_LOCKED_RO_Mask) == 0)
        alarm |= cAtPrbsEngineAlarmTypeLossSync;

    return alarm;
    }

static eAtModulePrbsRet ErrorForce(AtPrbsEngine self, eBool force)
    {
    uint32 address = cAf6Reg_upen_addr_20_Base + ConfigureOffset(self);
    uint32 regVal = AtPrbsEngineRead(self, address, cAtModulePrbs);

    mRegFieldSet(regVal, cAf6_upen_addr_20_TX_PRBS_FORCE_, force ? 0x1 : 0x0);
    AtPrbsEngineWrite(self, address, regVal, cAtModulePrbs);

    return cAtOk;
    }

static eBool ErrorIsForced(AtPrbsEngine self)
    {
    uint32 address = cAf6Reg_upen_addr_20_Base + ConfigureOffset(self);
    uint32 regVal = AtPrbsEngineRead(self, address, cAtModulePrbs);

    return (regVal & cAf6_upen_addr_20_TX_PRBS_FORCE_Mask)? cAtTrue : cAtFalse;
    }

static eBool ModeIsSupported(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    AtUnused(self);

    if ((prbsMode == cAtPrbsModePrbs7)  ||
        (prbsMode == cAtPrbsModePrbs9)  ||
        (prbsMode == cAtPrbsModePrbs15) ||
        (prbsMode == cAtPrbsModePrbs23) ||
        (prbsMode == cAtPrbsModePrbs31))
        return cAtTrue;

    return cAtFalse;
    }

static eAtModulePrbsRet SideSet(AtPrbsEngine self, eAtPrbsSide option)
    {
    AtUnused(self);
    return (option == cAtPrbsSidePsn) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtPrbsSide SideGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsSidePsn;
    }

static eBool SideIsSupported(AtPrbsEngine self, eAtPrbsSide option)
    {
    AtUnused(self);
    return ((option == cAtPrbsSidePsn) ? cAtTrue : cAtFalse);
    }

static eAtModulePrbsRet Invert(AtPrbsEngine self, eBool invert)
    {
    AtUnused(self);
    if (invert)
        return cAtErrorModeNotSupport;

    return cAtOk;
    }

static eBool IsInverted(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool InversionIsSupported(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 AlarmHistoryWrite2Clear(AtPrbsEngine self, eBool w2c)
    {
    uint32 address = cAf6Reg_upen_addr_31_Base + ConfigureOffset(self);
    uint32 regVal = AtPrbsEngineRead(self, address, cAtModulePrbs);
    uint32 alarm = 0;

    if (w2c)
        AtPrbsEngineWrite(self, address, regVal, cAtModulePrbs);

    if (regVal & cAf6_upen_addr_31_RX_PRBS_ERR_Mask)
        alarm |= cAtPrbsEngineAlarmTypeError;

    if ((regVal & cAf6_upen_addr_31_RX_PRBS_LOCKED_Mask) == 0)
        alarm |= cAtPrbsEngineAlarmTypeLossSync;

    if (w2c)
        RxMonitoringReset(self);

    return alarm;
    }

static uint32 AlarmHistoryGet(AtPrbsEngine self)
    {
    return AlarmHistoryWrite2Clear(self, cAtFalse);
    }

static uint32 AlarmHistoryClear(AtPrbsEngine self)
    {
    return AlarmHistoryWrite2Clear(self, cAtTrue);
    }

static eAtPrbsSide GeneratingSideGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsSideTdm;
    }

static eAtPrbsSide MonitoringSideGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsSideTdm;
    }

static eAtModulePrbsRet GeneratingSideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return ((side == cAtPrbsSideTdm) ? cAtOk : cAtErrorModeNotSupport);
    }

static eAtModulePrbsRet MonitoringSideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return ((side == cAtPrbsSideTdm) ? cAtOk : cAtErrorModeNotSupport);
    }

static uint32 CounterDrpOffsetGet(AtPrbsEngine self)
    {
    uint32 drpBaseAddress = AtSerdesControllerDrpBaseAddress(mThis(self)->serdesController);
    return drpBaseAddress + mMethodsGet(mThis(self))->CounterOffset(mThis(self));
    }

static uint32 CounterRead2Clear(AtPrbsEngine self, uint16 counterType, eBool r2c)
    {
    uint32 addrLowCnt;
    uint32 addrHighCnt;
    uint32 regValLowCnt;
    uint32 regValHighCnt;

    if (!AtPrbsEngineCounterIsSupported(self, counterType))
        return 0;

    addrLowCnt = cAf6Reg_upen_rawprbs_err_cnt_drp_Base + CounterDrpOffsetGet(self);
    addrHighCnt = addrLowCnt + 0x1;
    regValLowCnt = AtPrbsEngineRead(self, addrLowCnt, cAtModulePrbs);
    regValHighCnt = AtPrbsEngineRead(self, addrHighCnt, cAtModulePrbs);

    if (r2c)
        RxMonitoringReset(self);

    return regValLowCnt + (regValHighCnt << 16);
    }

static uint32 CounterGet(AtPrbsEngine self, uint16 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtFalse);
    }

static uint32 CounterClear(AtPrbsEngine self, uint16 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtTrue);
    }

static eBool CounterIsSupported(AtPrbsEngine self, uint16 counterType)
    {
    AtUnused(self);
    if (counterType != cAtPrbsEngineCounterRxBitError)
        return cAtFalse;

    return cAtTrue;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60210061SgmiiSerdesRawPrbsEngine object = (Tha60210061SgmiiSerdesRawPrbsEngine)self;
    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(serdesController);
    mEncodeUInt(txPrbsMode);
    mEncodeUInt(rxPrbsMode);
    }

static void OverrideAtObject(AtPrbsEngine self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, m_AtPrbsEngineMethods, sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Init);
        mMethodOverride(m_AtPrbsEngineOverride, TxEnable);
        mMethodOverride(m_AtPrbsEngineOverride, TxIsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorForce);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorIsForced);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmGet);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, SeparateTwoDirections);
        mMethodOverride(m_AtPrbsEngineOverride, ModeIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, SideSet);
        mMethodOverride(m_AtPrbsEngineOverride, SideGet);
        mMethodOverride(m_AtPrbsEngineOverride, SideIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, Invert);
        mMethodOverride(m_AtPrbsEngineOverride, IsInverted);
        mMethodOverride(m_AtPrbsEngineOverride, InversionIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryClear);
        mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideSet);
        mMethodOverride(m_AtPrbsEngineOverride, GeneratingSideSet);
        mMethodOverride(m_AtPrbsEngineOverride, GeneratingSideGet);
        mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideGet);
        mMethodOverride(m_AtPrbsEngineOverride, CounterClear);
        mMethodOverride(m_AtPrbsEngineOverride, CounterGet);
        mMethodOverride(m_AtPrbsEngineOverride, CounterIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, RxEnable);
        mMethodOverride(m_AtPrbsEngineOverride, RxIsEnabled);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtObject(self);
    OverrideAtPrbsEngine(self);
    }

static void MethodsInit(Tha60210061SgmiiSerdesRawPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, HssiSerdesId);
        mMethodOverride(m_methods, CounterOffset);
        mMethodOverride(m_methods, PortSerdesId);
        mMethodOverride(m_methods, ControlledByArriveRegisterEnable);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061SgmiiSerdesRawPrbsEngine);
    }

AtPrbsEngine Tha60210061SgmiiSerdesRawPrbsObjectInit(AtPrbsEngine self, AtSerdesController serdesController)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPrbsEngineObjectInit(self,
                               AtSerdesControllerPhysicalPortGet(serdesController),
                               AtSerdesControllerIdGet(serdesController)) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(mThis(self));
    Override(self);
    m_methodsInit = 1;
    mThis(self)->serdesController = serdesController;

    return self;
    }

AtPrbsEngine Tha60210061SgmiiSerdesRawPrbsEngineNew(AtSerdesController serdesController)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine prbsEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (prbsEngine == NULL)
        return NULL;

    /* Construct it */
    return Tha60210061SgmiiSerdesRawPrbsObjectInit(prbsEngine, serdesController);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60210061PrbsEngineNxDs0Ec1.c
 *
 * Created Date: Oct 2, 2017
 *
 * Description : PRBS engine of NxDs0 over EC1
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPdhNxDs0.h"
#include "../../../default/prbs/ThaPrbsEngineNxDs0Internal.h"
#include "Tha60210061ModulePrbs.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210061PrbsEngineNxDs0Ec1)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061PrbsEngineNxDs0Ec1 * Tha60210061PrbsEngineNxDs0Ec1;
typedef struct tTha60210061PrbsEngineNxDs0Ec1
	{
	tThaPrbsEngineNxDs0 super;
	}tTha60210061PrbsEngineNxDs0Ec1;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPrbsEngineMethods m_ThaPrbsEngineOverride;

/* Super implementation */
static const tThaPrbsEngineMethods *m_ThaPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ChannelId(ThaPrbsEngine self)
    {
    AtPdhNxDS0 nxDs0 = (AtPdhNxDS0)AtPrbsEngineChannelGet((AtPrbsEngine)self);
    AtPdhDe1 de1 = AtPdhNxDS0De1Get(nxDs0);
    return Tha60210061PrbsEngineDe1Ec1ChannelId((AtPrbsEngine)self, de1);
    }

static void OverrideThaPrbsEngine(AtPrbsEngine self)
    {
    ThaPrbsEngine engine = (ThaPrbsEngine)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPrbsEngineMethods = mMethodsGet(engine);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPrbsEngineOverride, m_ThaPrbsEngineMethods, sizeof(m_ThaPrbsEngineOverride));

        mMethodOverride(m_ThaPrbsEngineOverride, ChannelId);
        }

    mMethodsSet(engine, &m_ThaPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideThaPrbsEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061PrbsEngineNxDs0Ec1);
    }

static AtPrbsEngine ObjectInit(AtPrbsEngine self, AtPdhNxDS0 nxDs0, uint32 engineId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPrbsEngineNxDs0ObjectInit(self, nxDs0, engineId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha60210061PrbsEngineNxDs0Ec1New(AtPdhNxDS0 nxDs0, uint32 engineId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newObject = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newObject, nxDs0, engineId);
    }

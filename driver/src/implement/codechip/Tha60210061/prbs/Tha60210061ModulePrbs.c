/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60210061ModulePrbs.c
 *
 * Created Date: Jun 18, 2016
 *
 * Description : 60210061 module PRBS implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPdhNxDs0.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/man/ThaDevice.h"
#include "../../../default/man/versionreader/ThaVersionReader.h"
#include "../../Tha60210012/prbs/Tha60210012ModulePrbsInternal.h"
#include "../eth/Tha60210061ModuleEth.h"
#include "../physical/Tha60210061SerdesManager.h"
#include "../sdh/Tha60210061ModuleSdh.h"
#include "Tha60210061ModulePrbs.h"
#include "Tha60210061SerdesPrbsEngineInternal.h"
#include "Tha60210061EthFramePrbsEngineReg.h"
#include "Tha60210061SerdesRawPrbsEngineReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210061ModulePrbs)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061ModulePrbs * Tha60210061ModulePrbs;
typedef struct tTha60210061ModulePrbs
    {
    tTha60210012ModulePrbs super;
    AtPrbsEngine ethEngine;
    }tTha60210061ModulePrbs;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods      m_AtObjectOverride;
static tAtModuleMethods      m_AtModuleOverride;
static tAtModulePrbsMethods  m_AtModulePrbsOverride;
static tThaModulePrbsMethods m_ThaModulePrbsOverride;

/* Save super implementation */
static const tAtObjectMethods      *m_AtObjectMethods      = NULL;
static const tAtModuleMethods      *m_AtModuleMethods      = NULL;
static const tAtModulePrbsMethods  *m_AtModulePrbsMethods  = NULL;
static const tThaModulePrbsMethods *m_ThaModulePrbsMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 StartDe3LineId(AtModulePrbs self)
    {
    AtUnused(self);
    return 12;
    }

static AtPrbsEngine De1LinePrbsEngineCreate(AtModulePrbs self, AtPdhDe1 de1)
    {
    AtUnused(self);
    return Tha6021PdhLinePrbsEngineNew((AtChannel)de1, AtChannelIdGet((AtChannel)de1));
    }

static AtPrbsEngine De3LinePrbsEngineCreate(AtModulePrbs self, AtPdhDe3 de3)
    {
    AtUnused(self);
    return Tha6021PdhLinePrbsEngineNew((AtChannel)de3, AtChannelIdGet((AtChannel)de3) + StartDe3LineId(self));
    }

static eAtRet CommonGenEngineDefautPktLengthSet(AtModulePrbs self)
    {
    const uint32 cMinPktLength = 64;
    const uint32 cMaxPktLength = 1500;

    uint32 address = cAf6Reg_egen_pktlen_Base + cEthFrameGenBaseAddress;
    uint32 regVal = mModuleHwRead(self, address);

    mRegFieldSet(regVal, cAf6_egen_pktlen_MinPLen_, cMinPktLength);
    mRegFieldSet(regVal, cAf6_egen_pktlen_MaxPLen_, cMaxPktLength);
    mModuleHwWrite(self, address, regVal);

    return cAtOk;
    }

static eAtRet CommonGenEngineDefautSet(AtModulePrbs self)
    {
    uint32 address = cAf6Reg_egen_modopt_Base + cEthFrameGenBaseAddress;
    uint32 regVal = mModuleHwRead(self, address);
    const uint8 cPrbs31 = 0xf;

    mRegFieldSet(regVal, cAf6_egen_modopt_DataMod_, cPrbs31);
    mRegFieldSet(regVal, cAf6_egen_modopt_RateFcsEn_, 0x1);
    mRegFieldSet(regVal, cAf6_egen_modopt_GenAllEn_, 0x1);
    mRegFieldSet(regVal, cAf6_egen_modopt_LENIncEn_, 0x1);
    mRegFieldSet(regVal, cAf6_egen_modopt_DAIncEn_, 0x0);

    mModuleHwWrite(self, address, regVal);

    return CommonGenEngineDefautPktLengthSet(self);
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Init(self);
    AtModulePrbs module = (AtModulePrbs)self;

    if (ret != cAtOk)
        return ret;

    if (Tha60210061ModulePrbsEthFramePrbsIsSupported(module))
        CommonGenEngineDefautSet(module);

    return cAtOk;
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static uint32 StartVersionSupportRawPrbsEngine(void)
    {
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x3, 0x1, 0x1061);
    }

static uint32 StartVersionSupportGePortPrbsEngine(void)
    {
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x3, 0x1, 0x1060);
    }

static eBool GePortPrsbIsSupported(AtModulePrbs self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);

    return (hwVersion >= StartVersionSupportGePortPrbsEngine()) ? cAtTrue : cAtFalse;
    }

static AtPrbsEngine EthPrbsEngineObjectCreate(ThaModulePrbs self, uint32 engineId, AtEthPort port)
    {
    AtPrbsEngine engine;

    /* Only support port PRBS engine for SGMII Port */
    if (!GePortPrsbIsSupported((AtModulePrbs)self) || !Tha60210061ModuleEthPortIsSgmii(port))
        return NULL;

    /* Only one engine */
    if (mThis(self)->ethEngine)
        return NULL;

    engine = Tha60210061EthFramePrbsEngineNew(port, engineId);
    mThis(self)->ethEngine = engine;

    return engine;
    }

static uint32 StartVersionSupportCommonEthPrbsEngine(void)
    {
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x7, 0x0);
    }

static const char *CapacityDescription(AtModule self)
    {
    if (!GePortPrsbIsSupported((AtModulePrbs)self))
        return m_AtModuleMethods->CapacityDescription(self);

    return "PRBS engines: 16 engines (vc4, vc3, de3, vc1x, de1, nxds0), 4096 engines (pw), 1 engine (eth)";
    }

static uint32 MaxNumEngines(AtModulePrbs self)
    {
    if (!GePortPrsbIsSupported(self))
        return m_AtModulePrbsMethods->MaxNumEngines(self);

    return m_AtModulePrbsMethods->MaxNumEngines(self) + 1;
    }

static void EthPrbsEngineDelete(AtModulePrbs self)
    {
    AtPrbsEngine engine = mThis(self)->ethEngine;
    if (engine)
        {
        AtChannel channel = AtPrbsEngineChannelGet(engine);
        AtChannelPrbsEngineSet(channel, NULL);

        /* Try to disable and unforce engine before deleting */
        AtPrbsEngineEnable(engine, cAtFalse);
        AtPrbsEngineErrorForce(engine, cAtFalse);

        AtObjectDelete((AtObject)engine);
        mThis(self)->ethEngine = NULL;
        }
    }

static eAtRet EngineDelete(AtModulePrbs self, uint32 engineId)
    {
    AtPrbsEngine engine = AtModulePrbsEngineGet(self, engineId);

    if (engine != mThis(self)->ethEngine)
        return m_AtModulePrbsMethods->EngineDelete(self, engineId);

    if (engine == NULL)
        return cAtErrorNullPointer;

    EthPrbsEngineDelete(self);
    return cAtOk;
    }

static eAtRet EngineObjectDelete(AtModulePrbs self, AtPrbsEngine engine)
    {
    if (engine == NULL)
        return cAtOk;

    if (engine != mThis(self)->ethEngine)
        return m_AtModulePrbsMethods->EngineObjectDelete(self, engine);

    EthPrbsEngineDelete(self);
    return cAtOk;
    }

static AtPrbsEngine CachePrbsEngine(ThaModulePrbs self, uint32 engineId, AtPrbsEngine engine)
    {
    if (engine == mThis(self)->ethEngine)
        return engine;

    return m_ThaModulePrbsMethods->CachePrbsEngine(self, engineId, engine);
    }

static AtPrbsEngine PrbsEngineGet(AtModulePrbs self, uint32 engineId)
    {
    if ((mThis(self)->ethEngine) && (engineId == AtPrbsEngineIdGet(mThis(self)->ethEngine)))
        return mThis(self)->ethEngine;

    return m_AtModulePrbsMethods->PrbsEngineGet(self, engineId);
    }

static void Delete(AtObject self)
    {
    /* Delete private data */
    EthPrbsEngineDelete((AtModulePrbs)self);

    /* Call super function to fully delete */
    m_AtObjectMethods->Delete((AtObject)self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60210061ModulePrbs object = (Tha60210061ModulePrbs)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObject(ethEngine);
    }

static AtPrbsEngine AuVcPrbsEngineObjectCreate(ThaModulePrbs self, uint32 engineId, AtSdhChannel sdhVc)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)AtChannelModuleGet((AtChannel)sdhVc);

    if (!Tha60210061ModuleSdhChannelIsInEc1Line(sdhModule, sdhVc))
        return m_ThaModulePrbsMethods->AuVcPrbsEngineObjectCreate(self, engineId, sdhVc);

    return Tha60210061AuVcPrbsEngineEc1New(sdhVc, engineId);
    }

static AtPrbsEngine Vc1xPrbsEngineObjectCreate(ThaModulePrbs self, uint32 engineId, AtSdhChannel sdhVc)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)AtChannelModuleGet((AtChannel)sdhVc);

    if (!Tha60210061ModuleSdhChannelIsInEc1Line(sdhModule, sdhVc))
        return m_ThaModulePrbsMethods->Vc1xPrbsEngineObjectCreate(self, engineId, sdhVc);

    return Tha60210061PrbsEngineVc1xEc1New(sdhVc, engineId);
    }

static AtPrbsEngine De3PrbsEngineObjectCreate(ThaModulePrbs self, uint32 engineId, AtPdhDe3 de3)
    {
    AtSdhChannel vc = (AtSdhChannel)AtPdhChannelVcInternalGet((AtPdhChannel)de3);
    AtModuleSdh sdhModule = (AtModuleSdh)AtChannelModuleGet((AtChannel)vc);

    if (vc && Tha60210061ModuleSdhChannelIsInEc1Line(sdhModule, vc))
        return Tha60210061PrbsEngineDe3Ec1New(de3, engineId);

    return m_ThaModulePrbsMethods->De3PrbsEngineObjectCreate(self, engineId, de3);
    }

static AtSdhChannel VcGetIfPossible(AtPdhDe1 de1)
    {
    AtSdhChannel vc = (AtSdhChannel)AtPdhChannelVcInternalGet((AtPdhChannel)de1);
    AtPdhChannel de2;

    if (vc != NULL)
        return vc;

    if ((de2 = AtPdhChannelParentChannelGet((AtPdhChannel)de1)) != NULL)
        return (AtSdhChannel)AtPdhChannelVcInternalGet(AtPdhChannelParentChannelGet(de2));

    return NULL;
    }

static AtPrbsEngine De1PrbsEngineObjectCreate(ThaModulePrbs self, uint32 engineId, AtPdhDe1 de1)
    {
    AtSdhChannel vc = VcGetIfPossible(de1);
    AtModuleSdh sdhModule = (AtModuleSdh)AtChannelModuleGet((AtChannel)vc);

    if (vc && Tha60210061ModuleSdhChannelIsInEc1Line(sdhModule, vc))
        return Tha60210061PrbsEngineDe1Ec1New(de1, engineId);

    return m_ThaModulePrbsMethods->De1PrbsEngineObjectCreate(self, engineId, de1);
    }

static AtPrbsEngine NxDs0PrbsEngineObjectCreate(ThaModulePrbs self, uint32 engineId, AtPdhNxDS0 nxDs0)
    {
    AtPdhDe1 de1 = AtPdhNxDS0De1Get(nxDs0);
    AtSdhChannel vc = VcGetIfPossible(de1);
    AtModuleSdh sdhModule = (AtModuleSdh)AtChannelModuleGet((AtChannel)vc);

    if (vc && Tha60210061ModuleSdhChannelIsInEc1Line(sdhModule, vc))
        return Tha60210061PrbsEngineNxDs0Ec1New(nxDs0, engineId);

    return m_ThaModulePrbsMethods->NxDs0PrbsEngineObjectCreate(self, engineId, nxDs0);
    }

static void OverrideAtModule(AtModulePrbs self)
    {
    AtModule module = (AtModule)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, CapacityDescription);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtModulePrbs(AtModulePrbs self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModulePrbsMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePrbsOverride, m_AtModulePrbsMethods, sizeof(m_AtModulePrbsOverride));

        mMethodOverride(m_AtModulePrbsOverride, De1LinePrbsEngineCreate);
        mMethodOverride(m_AtModulePrbsOverride, De3LinePrbsEngineCreate);
        mMethodOverride(m_AtModulePrbsOverride, MaxNumEngines);
        mMethodOverride(m_AtModulePrbsOverride, EngineDelete);
        mMethodOverride(m_AtModulePrbsOverride, PrbsEngineGet);
        mMethodOverride(m_AtModulePrbsOverride, EngineObjectDelete);
        }

    mMethodsSet(self, &m_AtModulePrbsOverride);
    }

static void OverrideThaModulePrbs(AtModulePrbs self)
    {
    ThaModulePrbs module = (ThaModulePrbs)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePrbsMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePrbsOverride, m_ThaModulePrbsMethods, sizeof(m_ThaModulePrbsOverride));

        mMethodOverride(m_ThaModulePrbsOverride, EthPrbsEngineObjectCreate);
        mMethodOverride(m_ThaModulePrbsOverride, CachePrbsEngine);
        mMethodOverride(m_ThaModulePrbsOverride, AuVcPrbsEngineObjectCreate);
        mMethodOverride(m_ThaModulePrbsOverride, Vc1xPrbsEngineObjectCreate);
        mMethodOverride(m_ThaModulePrbsOverride, De1PrbsEngineObjectCreate);
        mMethodOverride(m_ThaModulePrbsOverride, De3PrbsEngineObjectCreate);
        mMethodOverride(m_ThaModulePrbsOverride, NxDs0PrbsEngineObjectCreate);
        }

    mMethodsSet(module, &m_ThaModulePrbsOverride);
    }

static void OverrideAtObject(AtModulePrbs self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtModulePrbs self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    OverrideAtModulePrbs(self);
    OverrideThaModulePrbs(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061ModulePrbs);
    }

static AtModulePrbs ObjectInit(AtModulePrbs self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012ModulePrbsObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePrbs Tha60210061ModulePrbsNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePrbs newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

eBool Tha60210061ModulePrbsSgmiiQsgmiiPerPortControl(AtModulePrbs self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);

    if (hwVersion >= ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x1, 0x2, 0x0))
        return cAtTrue;

    return cAtFalse;
    }

eBool Tha60210061ModulePrbsRawPrsbIsSupported(AtModulePrbs self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);

    return (hwVersion >= StartVersionSupportRawPrbsEngine()) ? cAtTrue : cAtFalse;
    }

eBool Tha60210061ModulePrbsEthFramePrbsIsSupported(AtModulePrbs self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);

    return (hwVersion >= StartVersionSupportCommonEthPrbsEngine()) ? cAtTrue : cAtFalse;
    }

eAtRet Tha60210061ModulePrbsCommonGenEngineDefautSet(AtModulePrbs self)
    {
    if (self)
        return CommonGenEngineDefautSet(self);

    return cAtErrorNullPointer;
    }

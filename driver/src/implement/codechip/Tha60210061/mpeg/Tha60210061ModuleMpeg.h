/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MPEG
 * 
 * File        : Tha60210061ModuleMpeg.h
 * 
 * Created Date: Jan 4, 2017
 *
 * Description : 60210061 MPEG interfaces
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061MODULEMPEG_H_
#define _THA60210061MODULEMPEG_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtEthPort.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60210061ModuleMpegNew(AtDevice device);

eAtRet Tha60210061ModuleMpegGeBypassEnable(ThaModuleMpeg self, AtEthPort gePort, eBool enable);
eBool Tha60210061ModuleMpegGeBypassIsEnabled(ThaModuleMpeg self, AtEthPort gePort);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061MODULEMPEG_H_ */


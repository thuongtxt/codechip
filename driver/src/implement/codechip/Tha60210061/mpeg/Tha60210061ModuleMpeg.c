/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MPEG
 *
 * File        : Tha60210061ModuleMpeg.c
 *
 * Created Date: Jan 4, 2017
 *
 * Description : 60210061 module MPEG
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210012/mpeg/Tha60210012ModuleMpegInternal.h"
#include "Tha60210061ModuleMpeg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061ModuleMpeg
    {
    tTha60210012ModuleMpeg super;
    }tTha60210061ModuleMpeg;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60210012ModuleMpegMethods m_Tha60210012ModuleMpegOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet EthPassthroughQueuesInit(Tha60210012ModuleMpeg self)
    {
    eAtRet ret = cAtOk;
    uint32 port_i;
    const uint32 cNumGePort = 4;

    for (port_i = 0; port_i < cNumGePort; port_i++)
        {
        ret |= Tha60210012ModuleMpegEthBypassQueueMaxThresholdSet(self, port_i, 0, 0x1E, 0x07);
        ret |= Tha60210012ModuleMpegEthBypassQueueMinThresholdSet(self, port_i, 0, 0x1E, 0x02);
        ret |= Tha60210012ModuleMpegEthBypassQueuePrioritySet(self, port_i, 0, cThaMpegQueuePriorityHighest);
        ret |= Tha60210012ModuleMpegEthBypassQueuePrioritySet(self, port_i, 1, cThaMpegQueuePriorityHighest);
        ret |= Tha60210012ModuleMpegEthBypassQueueEnable(self, port_i, 0, cAtFalse);
        ret |= Tha60210012ModuleMpegEthBypassQueueEnable(self, port_i, 1, cAtFalse);
        }

    return ret;
    }

static uint32 PortId(AtEthPort gePort)
    {
    uint32 portId = AtChannelIdGet((AtChannel)gePort);

    if (portId == 1) return 0;
    if (portId == 2) return 1;
    if (portId == 3) return 2;
    if (portId == 4) return 3;

    return cInvalidUint8;
    }

static uint8 NumQueuesForBundle(Tha60210012ModuleMpeg self)
    {
    AtUnused(self);
    return 1;
    }

static uint32 MaxNumLinks(Tha60210012ModuleMpeg self)
    {
    AtUnused(self);
    return 1024;
    }

static void OverrideTha60210012ModuleMpeg(AtModule self)
    {
    Tha60210012ModuleMpeg module = (Tha60210012ModuleMpeg)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210012ModuleMpegOverride, mMethodsGet(module), sizeof(m_Tha60210012ModuleMpegOverride));
        mMethodOverride(m_Tha60210012ModuleMpegOverride, EthPassthroughQueuesInit);
        mMethodOverride(m_Tha60210012ModuleMpegOverride, NumQueuesForBundle);
        mMethodOverride(m_Tha60210012ModuleMpegOverride, MaxNumLinks);
        }

    mMethodsSet(module, &m_Tha60210012ModuleMpegOverride);
    }

static void Override(AtModule self)
    {
    OverrideTha60210012ModuleMpeg(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061ModuleMpeg);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012ModuleMpegObjectInit(self, device) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210061ModuleMpegNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

eAtRet Tha60210061ModuleMpegGeBypassEnable(ThaModuleMpeg self, AtEthPort gePort, eBool enable)
    {
    return Tha60210012ModuleMpegEthBypassQueueEnable((Tha60210012ModuleMpeg)self, PortId(gePort), 0, enable);
    }

eBool Tha60210061ModuleMpegGeBypassIsEnabled(ThaModuleMpeg self, AtEthPort gePort)
    {
    return Tha60210012ModuleMpegEthBypassQueueIsEnabled((Tha60210012ModuleMpeg)self, PortId(gePort), 0);
    }


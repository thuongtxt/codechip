/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60210061ClaPwController.c
 *
 * Created Date: Aug 23, 2016
 *
 * Description : CLA PW Controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../Tha60210061ModuleClaReg.h"
#include "../Tha60210061ModuleCla.h"
#include "Tha60210061ClaPwController.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaCLAHbceFlowNumMask                         cAf6_cla_hbce_hash_table_CLAHbceLink_Mask
#define cThaCLAHbceFlowNumShift                        cAf6_cla_hbce_hash_table_CLAHbceLink_Shift
#define cThaCLAHbceMemoryStartPtrMask                  cAf6_cla_hbce_hash_table_CLAHbceMemoryExtraStartPointer_Mask
#define cThaCLAHbceMemoryStartPtrShift                 cAf6_cla_hbce_hash_table_CLAHbceMemoryExtraStartPointer_Shift

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaClaPwControllerMethods         m_ThaClaPwControllerOverride;

/* Save super implemenation */
static const tThaClaPwControllerMethods   *m_ThaClaPwControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
mDefineMaskShift(ThaClaPwController, CLAHbceFlowNum)
mDefineMaskShift(ThaClaPwController, CLAHbceMemoryStartPtr)

static ThaHbce HbceCreate(ThaClaPwController self, uint8 hbceId, AtIpCore core)
    {
    return Tha60210061HbceNew(hbceId, (AtModule)ThaClaControllerModuleGet((ThaClaController)self), core);
    }

static void OverrideThaClaPwController(ThaClaPwController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaClaPwControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaPwControllerOverride, mMethodsGet(self), sizeof(m_ThaClaPwControllerOverride));

        mMethodOverride(m_ThaClaPwControllerOverride, HbceCreate);
        mBitFieldOverride(ThaClaPwController, m_ThaClaPwControllerOverride, CLAHbceFlowNum)
        mBitFieldOverride(ThaClaPwController, m_ThaClaPwControllerOverride, CLAHbceMemoryStartPtr)
        }

    mMethodsSet(self, &m_ThaClaPwControllerOverride);
    }

static void Override(ThaClaPwController self)
    {
    OverrideThaClaPwController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061ClaPwController);
    }

ThaClaPwController Tha60210061ClaPwControllerObjectInit(ThaClaPwController self, ThaModuleCla cla)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012ClaPwControllerObjectInit(self, cla) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaClaPwController Tha60210061ClaPwControllerNew(ThaModuleCla cla)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaClaPwController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60210061ClaPwControllerObjectInit(newController, cla);
    }

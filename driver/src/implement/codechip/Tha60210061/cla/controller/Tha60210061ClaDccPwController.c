/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60210061ClaDccPwController.c
 *
 * Created Date: Jul 10, 2017
 *
 * Description : CLA DCC PW controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../Tha60210012/pw/Tha60210012ModulePw.h"
#include "../../../Tha60210012/cla/Tha60210012ModuleClaInternal.h"
#include "../Tha60210061ModuleCla.h"
#include "../Tha60210061ModuleClaReg.h"
#include "Tha60210061ClaPwController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210061ClaDccPwController)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061ClaDccPwController * Tha60210061ClaDccPwController;
typedef struct tTha60210061ClaDccPwController
	{
    tTha60210061ClaPwController super;
	}tTha60210061ClaDccPwController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/*--------------------------- Forward declarations ---------------------------*/
/* Override */
static tThaClaPwControllerMethods m_ThaClaPwControllerOverride;

/* Super implementation */
static const tThaClaPwControllerMethods *m_ThaClaPwControllerMethods = NULL;

/*--------------------------- Implementation ---------------------------------*/
static eAtPwHdlcPayloadType DefaultPayloadType(ThaClaPwController self)
    {
    AtUnused(self);
    return cAtPwHdlcPayloadTypePdu;
    }

static void PwExpectedCVlanClear(ThaClaPwController self, AtPw pw, tAtEthVlanTag *cVlan)
    {
    ThaModuleCla moduleCla = ThaClaControllerModuleGet((ThaClaController)self);
    uint32 address = cAf6Reg_cla_vlan_lk_Base + Tha60210011ModuleClaBaseAddress(moduleCla) + cVlan->vlanId;
    mChannelHwWrite(pw, address, 0x0, cThaModuleCla);
    }

static eAtRet PwExpectedCVlanSet(ThaClaPwController self, AtPw pw, tAtEthVlanTag *cVlan)
    {
    uint32 address, regVal;
    Tha60210012ModuleCla moduleCla = (Tha60210012ModuleCla)ThaClaControllerModuleGet((ThaClaController)self);
    tAtEthVlanTag currentVlan;

    if (AtPwEthExpectedCVlanGet(pw, &currentVlan))
        PwExpectedCVlanClear(self, pw, &currentVlan);

    if (cVlan == NULL)
        return cAtOk;

    address = cAf6Reg_cla_vlan_lk_Base + Tha60210011ModuleClaBaseAddress((ThaModuleCla)moduleCla) + cVlan->vlanId;
    regVal = mChannelHwRead(pw, address, cThaModuleCla);

    mPolyRegFieldSet(regVal, moduleCla, CLAVlanFlowID, Tha60210012PwAdapterHwFlowIdGet((ThaPwAdapter)pw));
    mPolyRegFieldSet(regVal, moduleCla, CLAVlanEn, 1);
    mPolyRegFieldSet(regVal, moduleCla, CLAVlanLabelEn, cLookupByVlanId);
    mPolyRegFieldSet(regVal, moduleCla, CLAVlanFlowDirect, 1);

    mChannelHwWrite(pw, address, regVal, cThaModuleCla);
    return cAtOk;
    }

static void OverrideThaClaPwController(ThaClaPwController self)
    {
    ThaClaPwController controller = (ThaClaPwController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaClaPwControllerMethods = mMethodsGet(controller);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaPwControllerOverride, m_ThaClaPwControllerMethods, sizeof(m_ThaClaPwControllerOverride));

        mMethodOverride(m_ThaClaPwControllerOverride, DefaultPayloadType);
        mMethodOverride(m_ThaClaPwControllerOverride, PwExpectedCVlanSet);
        }

    mMethodsSet(controller, &m_ThaClaPwControllerOverride);
    }

static void Override(ThaClaPwController self)
    {
    OverrideThaClaPwController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061ClaDccPwController);
    }

static ThaClaPwController ObjectInit(ThaClaPwController self, ThaModuleCla moduleCla)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210061ClaPwControllerObjectInit(self, moduleCla) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaClaPwController Tha60210061ClaDccPwControllerNew(ThaModuleCla moduleCla)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaClaPwController newObject = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newObject, moduleCla);
    }

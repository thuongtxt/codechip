/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Tha60210061ClaPwController.h
 * 
 * Created Date: Jul 3, 2017
 *
 * Description : CLA PW controller interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061CLAPWCONTROLLER_H_
#define _THA60210061CLAPWCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../Tha60210012/cla/controller/Tha60210012ClaPwControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210061ClaPwController
    {
    tTha60210012ClaPwController super;
    }tTha60210061ClaPwController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaClaPwController Tha60210061ClaPwControllerObjectInit(ThaClaPwController self, ThaModuleCla cla);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061CLAPWCONTROLLER_H_ */


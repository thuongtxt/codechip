/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60210061ModuleCla.c
 *
 * Created Date: Aug 22, 2016
 *
 * Description : CLA module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../Tha60210012/cla/Tha60210012ModuleClaInternal.h"
#include "../eth/Tha60210061ModuleEth.h"
#include "../encap/Tha60210061ModuleEncap.h"
#include "Tha60210061ModuleCla.h"
#include "Tha60210061ModuleClaReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaCLAVlanLabelEnMask      cAf6_cla_vlan_lk_CLAVlanLabelEn_Mask
#define cThaCLAVlanLabelEnShift     cAf6_cla_vlan_lk_CLAVlanLabelEn_Shift
#define cThaCLAVlanEnMask           cAf6_cla_vlan_lk_CLAVlanEn_Mask
#define cThaCLAVlanEnShift          cAf6_cla_vlan_lk_CLAVlanEn_Shift
#define cThaCLAVlanFlowDirectMask   cAf6_cla_vlan_lk_CLAVlanFlowDirect_Mask
#define cThaCLAVlanFlowDirectShift  cAf6_cla_vlan_lk_CLAVlanFlowDirect_Shift
#define cThaCLAVlanGrpWorkingMask   cAf6_cla_vlan_lk_CLAVlanGrpWorking_Mask
#define cThaCLAVlanGrpWorkingShift  cAf6_cla_vlan_lk_CLAVlanGrpWorking_Shift
#define cThaCLAVlanGrpIDFlowMask    cAf6_cla_vlan_lk_CLAVlanGrpIDFlow_Mask
#define cThaCLAVlanGrpIDFlowShift   cAf6_cla_vlan_lk_CLAVlanGrpIDFlow_Shift
#define cThaCLAVlanFlowIDMask       cAf6_cla_vlan_lk_CLAVlanFlowID_Mask
#define cThaCLAVlanFlowIDShift      cAf6_cla_vlan_lk_CLAVlanFlowID_Shift

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210061ModuleCla*)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061ModuleCla
    {
    tTha60210012ModuleCla super;

    uint32 asyncState;
    ThaClaPwController claDccController;
    }tTha60210061ModuleCla;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods             m_AtObjectOverride;
static tAtModuleMethods             m_AtModuleOverride;
static tThaModuleClaMethods         m_ThaModuleClaOverride;
static tThaModuleClaPwMethods       m_ThaModuleClaPwOverride;
static tTha60210011ModuleClaMethods m_Tha60210011ModuleClaOverride;
static tTha60210012ModuleClaMethods m_Tha60210012ModuleClaOverride;

/* Save super implementation */
static const tAtObjectMethods             * m_AtObjectMethods = NULL;
static const tAtModuleMethods             * m_AtModuleMethods = NULL;
static const tTha60210011ModuleClaMethods * m_Tha60210011ModuleClaMethods = NULL;
static const tTha60210012ModuleClaMethods * m_Tha60210012ModuleClaMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
mDefineMaskShift(Tha60210012ModuleCla, CLAVlanLabelEn)
mDefineMaskShift(Tha60210012ModuleCla, CLAVlanEn)
mDefineMaskShift(Tha60210012ModuleCla, CLAVlanFlowDirect)
mDefineMaskShift(Tha60210012ModuleCla, CLAVlanGrpWorking)
mDefineMaskShift(Tha60210012ModuleCla, CLAVlanGrpIDFlow)
mDefineMaskShift(Tha60210012ModuleCla, CLAVlanFlowID)

static uint32 HbceFlowDirectMask(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceFlowDirect_Mask;
    }

static uint32 HbceFlowDirectShift(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceFlowDirect_Shift;
    }

static uint32 HbceGroupWorkingMask(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceGrpWorking_Mask;
    }

static uint32 HbceGroupWorkingShift(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceGrpWorking_Shift;
    }

static uint32 HbceGroupIdFlowMask1(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceGrpIDFlow_Mask;
    }

static uint32 HbceGroupIdFlowShift1(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceGrpIDFlow_Shift;
    }

static uint32 HbceGroupIdFlowMask2(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 HbceGroupIdFlowShift2(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 HbceFlowIdMask(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceFlowID_Mask;
    }

static uint32 HbceFlowIdShift(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceFlowID_Shift;
    }

static uint32 HbceFlowEnableMask(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb_Mask;
    }

static uint32 HbceFlowEnableShift(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb_Shift;
    }

static uint32 HbceStoreIdMask(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceStoreID_Mask;
    }

static uint32 HbceStoreIdShift(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceStoreID_Shift;
    }

static uint32 GroupEnableControlWorkingOffset(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return 0x400;
    }

static uint32 RxPwTableLanFcsRmvMask(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_per_flow_table_RxPwTableLanFcsRmv_Mask;
    }

static uint32 RxPwTableLanFcsRmvShift(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_per_flow_table_RxPwTableLanFcsRmv_Shift;
    }

static uint32 RxPwTableQ922lenMask1(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_per_flow_table_RxPwTableQ922len_Mask_01;
    }

static uint32 RxPwTableQ922lenShift1(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_per_flow_table_RxPwTableQ922len_Shift_01;
    }

static uint32 RxPwTableQ922lenMask2(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_per_flow_table_RxPwTableQ922len_Mask_02;
    }

static uint32 RxPwTableQ922lenShift2(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_per_flow_table_RxPwTableQ922len_Shift_02;
    }

static uint32 RxPwTableQ922FieldMask1(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_per_flow_table_RxPwTableQ922Field_Mask_01;
    }

static uint32 RxPwTableQ922FieldShift1(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_per_flow_table_RxPwTableQ922Field_Shift_01;
    }

static uint32 RxPwTableQ922FieldMask2(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_per_flow_table_RxPwTableQ922Field_Mask_02;
    }

static uint32 RxPwTableQ922FieldShift2(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_per_flow_table_RxPwTableQ922Field_Shift_02;
    }

static uint32 RxPwTableSubSerTypeMask(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_per_flow_table_RxPwTableSubSerType_Mask;
    }

static uint32 RxPwTableSubSerTypeShift(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_per_flow_table_RxPwTableSubSerType_Shift;
    }

static uint32 RxPwTableTdmActMask(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_per_flow_table_RxPwTableTdmAct_Mask;
    }

static uint32 RxPwTableTdmActShift(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_per_flow_table_RxPwTableTdmAct_Shift;
    }

static uint32 RxPwTableEncActMask(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_per_flow_table_RxPwTableEncAct_Mask;
    }

static uint32 RxPwTableEncActShift(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_per_flow_table_RxPwTableEncAct_Shift;
    }

static uint32 RxPwTablePsnActMask(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_per_flow_table_RxPwTablePsnAct_Mask;
    }

static uint32 RxPwTablePsnActShift(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_per_flow_table_RxPwTablePsnAct_Shift;
    }

static uint32 RxPwTableFrgActMask(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_per_flow_table_RxPwTableFrgAct_Mask;
    }

static uint32 RxPwTableFrgActShift(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_per_flow_table_RxPwTableFrgAct_Shift;
    }

static uint32 RxPwTableSerTypeMask(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_per_flow_table_RxPwTableSerType_Mask;
    }

static uint32 RxPwTableSerTypeShift(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_per_flow_table_RxPwTableSerType_Shift;
    }

static uint32 RxPwTableSeridMask(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_per_flow_table_RxPwTableSerid_Mask;
    }

static uint32 RxPwTableSeridShift(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_per_flow_table_RxPwTableSerid_Shift;
    }

static ThaClaPwController PwControllerCreate(ThaModuleCla self)
    {
    return Tha60210061ClaPwControllerNew(self);
    }

static uint32 StartVersionUseNewAddressRxErrorPsnPacketCounter(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x0, 0x0, 0x0);
    }

static eBool CanControlPwSuppression(ThaModuleClaPw self)
    {
    /* Always support this feature */
    AtUnused(self);
    return cAtTrue;
    }

static uint32 CounterValuesGet(ThaModuleCla self, AtEthPort port, uint32 regAddress)
    {
    if (Tha60210061ModuleEthPortIs10G(port))
        return mModuleHwRead(self, regAddress + Tha60210011ModuleClaBaseAddress(self));

    return 0;
    }

static ThaModuleCla ClaModuleFromEthPort(AtEthPort self)
    {
    return (ThaModuleCla)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cThaModuleCla);
    }

static uint32 StartVersionRemovePtchControl(void)
    {
    /* From this version, HW remove PTCH control at CLA and let ethernet core do that job */
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x3, 0x0, 0x0);
    }

static AtModuleEncap ModuleEncap(AtModule self)
    {
    return (AtModuleEncap)AtDeviceModuleGet(AtModuleDeviceGet(self), cAtModuleEncap);
    }

static eAtRet DccEnable(AtModule self)
    {
    uint32 address = cAf6Reg_cla_glb_dcc_Base + Tha60210011ModuleClaBaseAddress((ThaModuleCla)self);
    uint32 regVal  = mModuleHwRead(self, address);
    const uint8 cAllLinesDccOamEnable = 0xF;

    mRegFieldSet(regVal, cAf6_cla_glb_dcc_RxDCCEn_, 1);
    mRegFieldSet(regVal, cAf6_cla_glb_dcc_RxDCCctrlEn_, cAllLinesDccOamEnable);
    mModuleHwWrite(self, address, regVal);

    return cAtOk;
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    if (Tha60210061ModuleEncapDccIsSupported(ModuleEncap(self)))
        return DccEnable(self);

    return cAtOk;
    }

static eAtRet SuperAsyncInit(AtModule self)
    {
    return m_AtModuleMethods->AsyncInit(self);
    }

static tAtAsyncOperationFunc NextAsyncOp(AtObject self, uint32 state)
    {
    static tAtAsyncOperationFunc func[] = {(tAtAsyncOperationFunc)SuperAsyncInit,
                                           (tAtAsyncOperationFunc)DccEnable};
    uint32 numFuncs = mCount(func);

    if (!Tha60210061ModuleEncapDccIsSupported(ModuleEncap((AtModule)self)))
        numFuncs = numFuncs - 1;

    if (state >= numFuncs)
        return NULL;

    return func[state];
    }

static eAtRet AsyncInit(AtModule self)
    {
    return AtDeviceObjectAsyncProcess(AtModuleDeviceGet(self), (AtObject)self, &(mThis(self)->asyncState), NextAsyncOp);
    }

static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject)mThis(self)->claDccController);
    mThis(self)->claDccController = NULL;
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210061ModuleCla *object = mThis(self);
    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeNone(asyncState);
    mEncodeObject(claDccController);
    }

static void OverrideAtObject(AtModule self)
    {
    AtObject module = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(module, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModule self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideThaModuleCla(AtModule self)
    {
    ThaModuleCla claModule = (ThaModuleCla)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleClaOverride, mMethodsGet(claModule), sizeof(m_ThaModuleClaOverride));

        mMethodOverride(m_ThaModuleClaOverride, PwControllerCreate);
        }

    mMethodsSet(claModule, &m_ThaModuleClaOverride);
    }

static void OverrideTha60210011ModuleCla(AtModule self)
    {
    Tha60210011ModuleCla claModule = (Tha60210011ModuleCla)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011ModuleClaMethods = mMethodsGet(claModule);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModuleClaOverride, m_Tha60210011ModuleClaMethods, sizeof(m_Tha60210011ModuleClaOverride));

        mMethodOverride(m_Tha60210011ModuleClaOverride, HbceFlowDirectMask);
        mMethodOverride(m_Tha60210011ModuleClaOverride, HbceFlowDirectShift);
        mMethodOverride(m_Tha60210011ModuleClaOverride, HbceGroupWorkingMask);
        mMethodOverride(m_Tha60210011ModuleClaOverride, HbceGroupWorkingShift);
        mMethodOverride(m_Tha60210011ModuleClaOverride, HbceGroupIdFlowMask1);
        mMethodOverride(m_Tha60210011ModuleClaOverride, HbceGroupIdFlowShift1);
        mMethodOverride(m_Tha60210011ModuleClaOverride, HbceGroupIdFlowMask2);
        mMethodOverride(m_Tha60210011ModuleClaOverride, HbceGroupIdFlowShift2);
        mMethodOverride(m_Tha60210011ModuleClaOverride, HbceFlowIdMask);
        mMethodOverride(m_Tha60210011ModuleClaOverride, HbceFlowIdShift);
        mMethodOverride(m_Tha60210011ModuleClaOverride, HbceFlowEnableMask);
        mMethodOverride(m_Tha60210011ModuleClaOverride, HbceFlowEnableShift);
        mMethodOverride(m_Tha60210011ModuleClaOverride, HbceStoreIdMask);
        mMethodOverride(m_Tha60210011ModuleClaOverride, HbceStoreIdShift);
        mMethodOverride(m_Tha60210011ModuleClaOverride, GroupEnableControlWorkingOffset);
        }

    mMethodsSet(claModule, &m_Tha60210011ModuleClaOverride);
    }

static void OverrideTha60210012ModuleCla(AtModule self)
    {
    Tha60210012ModuleCla claModule = (Tha60210012ModuleCla)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210012ModuleClaMethods = mMethodsGet(claModule);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210012ModuleClaOverride, m_Tha60210012ModuleClaMethods, sizeof(m_Tha60210012ModuleClaOverride));

        mMethodOverride(m_Tha60210012ModuleClaOverride, RxPwTableLanFcsRmvMask);
        mMethodOverride(m_Tha60210012ModuleClaOverride, RxPwTableLanFcsRmvShift);
        mMethodOverride(m_Tha60210012ModuleClaOverride, RxPwTableQ922lenMask1);
        mMethodOverride(m_Tha60210012ModuleClaOverride, RxPwTableQ922lenShift1);
        mMethodOverride(m_Tha60210012ModuleClaOverride, RxPwTableQ922lenMask2);
        mMethodOverride(m_Tha60210012ModuleClaOverride, RxPwTableQ922lenShift2);
        mMethodOverride(m_Tha60210012ModuleClaOverride, RxPwTableQ922FieldMask1);
        mMethodOverride(m_Tha60210012ModuleClaOverride, RxPwTableQ922FieldShift1);
        mMethodOverride(m_Tha60210012ModuleClaOverride, RxPwTableQ922FieldMask2);
        mMethodOverride(m_Tha60210012ModuleClaOverride, RxPwTableQ922FieldShift2);
        mMethodOverride(m_Tha60210012ModuleClaOverride, RxPwTableSubSerTypeMask);
        mMethodOverride(m_Tha60210012ModuleClaOverride, RxPwTableSubSerTypeShift);
        mMethodOverride(m_Tha60210012ModuleClaOverride, RxPwTableTdmActMask);
        mMethodOverride(m_Tha60210012ModuleClaOverride, RxPwTableTdmActShift);
        mMethodOverride(m_Tha60210012ModuleClaOverride, RxPwTableEncActMask);
        mMethodOverride(m_Tha60210012ModuleClaOverride, RxPwTableEncActShift);
        mMethodOverride(m_Tha60210012ModuleClaOverride, RxPwTablePsnActMask);
        mMethodOverride(m_Tha60210012ModuleClaOverride, RxPwTablePsnActShift);
        mMethodOverride(m_Tha60210012ModuleClaOverride, RxPwTableFrgActMask);
        mMethodOverride(m_Tha60210012ModuleClaOverride, RxPwTableFrgActShift);
        mMethodOverride(m_Tha60210012ModuleClaOverride, RxPwTableSerTypeMask);
        mMethodOverride(m_Tha60210012ModuleClaOverride, RxPwTableSerTypeShift);
        mMethodOverride(m_Tha60210012ModuleClaOverride, RxPwTableSeridMask);
        mMethodOverride(m_Tha60210012ModuleClaOverride, RxPwTableSeridShift);
        mMethodOverride(m_Tha60210012ModuleClaOverride, StartVersionUseNewAddressRxErrorPsnPacketCounter);

        mBitFieldOverride(ThaModuleCla, m_Tha60210012ModuleClaOverride, CLAVlanLabelEn)
        mBitFieldOverride(ThaModuleCla, m_Tha60210012ModuleClaOverride, CLAVlanEn)
        mBitFieldOverride(ThaModuleCla, m_Tha60210012ModuleClaOverride, CLAVlanFlowDirect)
        mBitFieldOverride(ThaModuleCla, m_Tha60210012ModuleClaOverride, CLAVlanGrpWorking)
        mBitFieldOverride(ThaModuleCla, m_Tha60210012ModuleClaOverride, CLAVlanGrpIDFlow)
        mBitFieldOverride(ThaModuleCla, m_Tha60210012ModuleClaOverride, CLAVlanFlowID)
        }

    mMethodsSet(claModule, &m_Tha60210012ModuleClaOverride);
    }

static void OverrideThaModuleClaPw(AtModule self)
    {
    ThaModuleClaPw claModule = (ThaModuleClaPw)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleClaPwOverride, mMethodsGet(claModule), sizeof(m_ThaModuleClaPwOverride));

        mMethodOverride(m_ThaModuleClaPwOverride, CanControlPwSuppression);
        }

    mMethodsSet(claModule, &m_ThaModuleClaPwOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    OverrideThaModuleCla(self);
    OverrideThaModuleClaPw(self);
    OverrideTha60210011ModuleCla(self);
    OverrideTha60210012ModuleCla(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061ModuleCla);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012ModuleClaObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210061ModuleClaNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

uint32 Tha60210061ModuleClaTotalPktCountPerService(AtEthPort ethPort, eBool r2c)
    {
    uint32 address = r2c ? cAf6Reg_Eth_Total_rc_Base : cAf6Reg_Eth_Total_ro_Base;
    return CounterValuesGet(ClaModuleFromEthPort(ethPort), ethPort, address);
    }

uint32 Tha60210061ModuleClaDiscardPktCountPerService(AtEthPort ethPort, eBool r2c)
    {
    uint32 address = r2c ? cAf6Reg_Eth_DiscardCnt_rc_Base : cAf6Reg_Eth_DiscardCnt_ro_Base;
    return CounterValuesGet(ClaModuleFromEthPort(ethPort), ethPort, address);
    }

uint32 Tha60210061ModuleClaCESPktCountPerService(AtEthPort ethPort, eBool r2c)
    {
    uint32 address = r2c ? cAf6Reg_Eth_CESCnt_rc_Base : cAf6Reg_Eth_CESCnt_ro_Base;
    return CounterValuesGet(ClaModuleFromEthPort(ethPort), ethPort, address);
    }

uint32 Tha60210061ModuleClaCEPPktCountPerService(AtEthPort ethPort, eBool r2c)
    {
    uint32 address = r2c ? cAf6Reg_Eth_CEPCnt_rc_Base : cAf6Reg_Eth_CEPCnt_ro_Base;
    return CounterValuesGet(ClaModuleFromEthPort(ethPort), ethPort, address);
    }

uint32 Tha60210061ModuleClaEOPPktCountPerService(AtEthPort ethPort, eBool r2c)
    {
    uint32 address = r2c ? cAf6Reg_Eth_EoPCnt_rc_Base : cAf6Reg_Eth_EoPCnt_ro_Base;
    return CounterValuesGet(ClaModuleFromEthPort(ethPort), ethPort, address);
    }

uint32 Tha60210061ModuleClaHDLCPktCountPerService(AtEthPort ethPort, eBool r2c)
    {
    uint32 address = r2c ? cAf6Reg_Eth_HDLCCnt_rc_Base : cAf6Reg_Eth_HDLCCnt_ro_Base;
    return CounterValuesGet(ClaModuleFromEthPort(ethPort), ethPort, address);
    }

uint32 Tha60210061ModuleClacHDLCPacketCountPerService(AtEthPort ethPort, eBool r2c)
    {
    uint32 address = r2c ? cAf6Reg_Eth_cHDLCCnt_rc_Base : cAf6Reg_Eth_cHDLCCnt_ro_Base;
    return CounterValuesGet(ClaModuleFromEthPort(ethPort), ethPort, address);
    }

uint32 Tha60210061ModuleClaPppBridgePktCountPerService(AtEthPort ethPort, eBool r2c)
    {
    uint32 address = r2c ? cAf6Reg_Eth_PPPBrigdeCnt_rc_Base : cAf6Reg_Eth_PPPBrigdeCnt_ro_Base;
    return CounterValuesGet(ClaModuleFromEthPort(ethPort), ethPort, address);
    }

uint32 Tha60210061ModuleClaPppOamPktCountPerService(AtEthPort ethPort, eBool r2c)
    {
    uint32 address = r2c ? cAf6Reg_Eth_PPPOamCnt_rc_Base : cAf6Reg_Eth_PPPOamCnt_ro_Base;
    return CounterValuesGet(ClaModuleFromEthPort(ethPort), ethPort, address);
    }

uint32 Tha60210061ModuleClaPppDataPktCountPerService(AtEthPort ethPort, eBool r2c)
    {
    uint32 address = r2c ? cAf6Reg_Eth_PPPData_rc_Base : cAf6Reg_Eth_PPPData_ro_Base;
    return CounterValuesGet(ClaModuleFromEthPort(ethPort), ethPort, address);
    }

uint32 Tha60210061ModuleClaMlpppOamPktCountPerService(AtEthPort ethPort, eBool r2c)
    {
    uint32 address = r2c ? cAf6Reg_Eth_MLPPPOam_rc_Base : cAf6Reg_Eth_MLPPPOam_ro_Base;
    return CounterValuesGet(ClaModuleFromEthPort(ethPort), ethPort, address);
    }

uint32 Tha60210061ModuleClaMlpppDataPktCountPerService(AtEthPort ethPort, eBool r2c)
    {
    uint32 address = r2c ? cAf6Reg_Eth_MLPPPData_rc_Base : cAf6Reg_Eth_MLPPPData_ro_Base;
    return CounterValuesGet(ClaModuleFromEthPort(ethPort), ethPort, address);
    }

uint32 Tha60210061ModuleClaFrOamPktCountPerService(AtEthPort self, eBool r2c)
    {
    uint32 address = r2c ? cAf6Reg_Eth_FrOAM_rc_Base : cAf6Reg_Eth_FrOAM_ro_Base;
    return CounterValuesGet(ClaModuleFromEthPort(self), self, address);
    }

uint32 Tha60210061ModuleClaFrDataPktCountPerService(AtEthPort ethPort, eBool r2c)
    {
    uint32 address = r2c ? cAf6Reg_Eth_FrData_rc_Base : cAf6Reg_Eth_FrData_ro_Base;
    return CounterValuesGet(ClaModuleFromEthPort(ethPort), ethPort, address);
    }

uint32 Tha60210061ModuleClaMFrOamPktCountPerService(AtEthPort ethPort, eBool r2c)
    {
    uint32 address = r2c ? cAf6Reg_Eth_MLFROam_rc_Base : cAf6Reg_Eth_MLFROam_ro_Base;
    return CounterValuesGet(ClaModuleFromEthPort(ethPort), ethPort, address);
    }

uint32 Tha60210061ModuleClaEthPassThroughPktCountPerService(AtEthPort ethPort, eBool r2c)
    {
    uint32 address = r2c ? cAf6Reg_Eth_PassCnt_rc_Base : cAf6Reg_Eth_PassCnt_ro_Base;
    return CounterValuesGet(ClaModuleFromEthPort(ethPort), ethPort, address);
    }

eBool Tha60210061ModuleClaPtchControlIsRemoved(ThaModuleCla self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);

    return (hwVersion >= StartVersionRemovePtchControl()) ? cAtTrue : cAtFalse;
    }

ThaClaPwController Tha60210061ModuleClaDccPwController(ThaModuleCla self)
    {
    if (self == NULL)
        return NULL;

    if (mThis(self)->claDccController == NULL)
        mThis(self)->claDccController = Tha60210061ClaDccPwControllerNew(self);

    return mThis(self)->claDccController;
    }

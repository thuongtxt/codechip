/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : Tha60210061ModuleCla.h
 * 
 * Created Date: Aug 22, 2016
 *
 * Description : 60210061 module CLA interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061MODULECLA_H_
#define _THA60210061MODULECLA_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/cla/pw/ThaModuleClaPw.h"
#include "../../../default/man/ThaDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
AtModule Tha60210061ModuleClaNew(AtDevice device);
ThaClaPwController Tha60210061ClaPwControllerNew(ThaModuleCla cla);
ThaHbce Tha60210061HbceNew(uint8 hbceId, AtModule claModule, AtIpCore core);
ThaClaPwController Tha60210061ClaDccPwControllerNew(ThaModuleCla moduleCla);
ThaClaPwController Tha60210061ModuleClaDccPwController(ThaModuleCla self);

/*--------------------------- Entries ----------------------------------------*/
uint32 Tha60210061ModuleClaTotalPktCountPerService(AtEthPort ethPort, eBool r2c);
uint32 Tha60210061ModuleClaDiscardPktCountPerService(AtEthPort ethPort, eBool r2c);
uint32 Tha60210061ModuleClaCESPktCountPerService(AtEthPort ethPort, eBool r2c);
uint32 Tha60210061ModuleClaCEPPktCountPerService(AtEthPort ethPort, eBool r2c);
uint32 Tha60210061ModuleClaEOPPktCountPerService(AtEthPort ethPort, eBool r2c);
uint32 Tha60210061ModuleClaHDLCPktCountPerService(AtEthPort ethPort, eBool r2c);
uint32 Tha60210061ModuleClacHDLCPacketCountPerService(AtEthPort ethPort, eBool r2c);
uint32 Tha60210061ModuleClaPppBridgePktCountPerService(AtEthPort ethPort, eBool r2c);
uint32 Tha60210061ModuleClaPppOamPktCountPerService(AtEthPort ethPort, eBool r2c);
uint32 Tha60210061ModuleClaPppDataPktCountPerService(AtEthPort ethPort, eBool r2c);
uint32 Tha60210061ModuleClaMlpppOamPktCountPerService(AtEthPort ethPort, eBool r2c);
uint32 Tha60210061ModuleClaMlpppDataPktCountPerService(AtEthPort ethPort, eBool r2c);
uint32 Tha60210061ModuleClaFrOamPktCountPerService(AtEthPort ethPort, eBool r2c);
uint32 Tha60210061ModuleClaFrDataPktCountPerService(AtEthPort ethPort, eBool r2c);
uint32 Tha60210061ModuleClaMFrOamPktCountPerService(AtEthPort ethPort, eBool r2c);
uint32 Tha60210061ModuleClaEthPassThroughPktCountPerService(AtEthPort ethPort, eBool r2c);
eBool Tha60210061ModuleClaPtchControlIsRemoved(ThaModuleCla self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061MODULECLA_H_ */


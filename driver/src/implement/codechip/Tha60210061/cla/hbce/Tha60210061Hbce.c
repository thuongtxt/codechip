/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60210061Hbce.c
 *
 * Created Date: Aug 23, 2016
 *
 * Description : HBCE
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../Tha60210012/cla/hbce/Tha60210012HbceInternal.h"
#include "../Tha60210061ModuleCla.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaHbcePwLabelMask                            cBit21_2
#define cThaHbcePwLabelShift                           2

#define cThaHbcePsnTypeMask                            cBit1_0
#define cThaHbcePsnTypeShift                           0

#define cThaHbcePatern21_12Mask                        cBit21_12
#define cThaHbcePatern21_12Shift                       12
#define cThaHbcePatern21_12DwIndex                     0

#define cThaHbcePatern11_0Mask                         cBit11_0
#define cThaHbcePatern11_0Shift                        0
#define cThaHbcePatern11_0DwIndex                      0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061Hbce
    {
    tTha60210012Hbce super;
    }tTha60210061Hbce;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaHbceMethods             m_ThaHbceOverride;
static tTha60210011HbceMethods     m_Tha60210011HbceOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 NumExtraMemoryPointer(Tha60210011Hbce self)
    {
    AtUnused(self);
    return 1024;
    }

static uint8 HbcePktType(eAtPwPsnType psnType)
    {
    if (psnType == cAtPwPsnTypeUdp)  return 1;
    if (psnType == cAtPwPsnTypeMpls) return 2;
    if (psnType == cAtPwPsnTypeMef)  return 0;

    return 0;
    }

static uint32 HwHbceLabel(ThaHbce self, AtEthPort ethPort, uint32 label, eAtPwPsnType psnType)
    {
    uint32 hwLabel = 0;

    AtUnused(self);
    AtUnused(ethPort);

    mRegFieldSet(hwLabel, cThaHbcePwLabel, label);
    mRegFieldSet(hwLabel, cThaHbcePsnType, HbcePktType(psnType));

    return hwLabel;
    }

static eAtRet RemainBitsGet(uint32 hwLabel, uint32 *remainBits)
    {
    remainBits[0] = mRegField(hwLabel, cThaHbcePatern21_12);
    return cAtOk;
    }

static uint32 HashIndexGet(ThaHbce self, uint32 hwLabel)
    {
    uint32 swHbcePatern21_12 = mRegField(hwLabel, cThaHbcePatern21_12);
    uint32 swHbcePatern11_0  = mRegField(hwLabel, cThaHbcePatern11_0);
    AtUnused(self);

    /* Calculate hash index */
    return swHbcePatern11_0 ^ swHbcePatern21_12;
    }

static uint32 PsnHash(ThaHbce self, uint8 partId, AtEthPort ethPort, uint32 label, eAtPwPsnType psnType, uint32 *remainBits)
    {
    uint32 hwLabel = HwHbceLabel(self, ethPort, label, psnType);
    uint32 hashIndex = HashIndexGet(self, hwLabel);

    AtUnused(partId);

    if (remainBits)
        RemainBitsGet(hwLabel, remainBits);

    return hashIndex;
    }

static void OverrideThaHbce(ThaHbce self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaHbceOverride, mMethodsGet(self), sizeof(m_ThaHbceOverride));

        mMethodOverride(m_ThaHbceOverride, PsnHash);
        }

    mMethodsSet(self, &m_ThaHbceOverride);
    }

static void Override60210011ThaHbce(ThaHbce self)
    {
    Tha60210011Hbce hbce = (Tha60210011Hbce)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011HbceOverride, mMethodsGet(hbce), sizeof(m_Tha60210011HbceOverride));

        mMethodOverride(m_Tha60210011HbceOverride, NumExtraMemoryPointer);
        }

    mMethodsSet(hbce, &m_Tha60210011HbceOverride);
    }

static void Override(ThaHbce self)
    {
    OverrideThaHbce(self);
    Override60210011ThaHbce(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061Hbce);
    }

static ThaHbce ObjectInit(ThaHbce self, uint8 hbceId, AtModule claModule, AtIpCore core)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012HbceObjectInit(self, hbceId, claModule, core) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaHbce Tha60210061HbceNew(uint8 hbceId, AtModule claModule, AtIpCore core)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaHbce newHbce = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newHbce == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newHbce, hbceId, claModule, core);
    }

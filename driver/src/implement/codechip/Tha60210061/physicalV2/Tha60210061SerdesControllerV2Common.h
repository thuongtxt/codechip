/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha60210061SerdesControllerV2Common.h
 * 
 * Created Date: Jun 9, 2017
 *
 * Description : Serdes controller common methods
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Includes ---------------------------------------*/
#include "../physical/Tha60210061DrpReg.h"
#include "Tha60210061SerdesControlV2Reg.h"

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Override */
static tAtSerdesControllerMethods             m_AtSerdesControllerOverride;
static tTha6021EthPortSerdesControllerMethods m_Tha6021EthPortSerdesControllerOverride;

/* Save super implementation */
static const tAtSerdesControllerMethods       *m_AtSerdesControllerMethods = NULL;

static uint32 Offset(AtSerdesController self)
    {
    return cSerdesTuningV2BaseAddress + AtSerdesControllerIdGet(self) * 0x800 + cHssi_serdes_id * 0x40;
    }

static eAtRet DefaultAutoResetSet(AtSerdesController self)
    {
    uint32 address = cAf6Reg_upen_addr_03_Base + Offset(self);
    uint32 regVal = AtSerdesControllerRead(self, address, cAtModuleEth);

    /* Enable Remote_Faul_Tx */
    mRegFieldSet(regVal, cAf6_upen_addr_03_Remote_Faul_Tx_, 0x1);

    /* Enable AUTORST_RX_SIGNAL */
    mRegFieldSet(regVal, cAf6_upen_addr_03_AUTORST_RX_SIGNAL_, 0x1);

    /* Enable AUTORST_RX */
    mRegFieldSet(regVal, cAf6_upen_addr_03_AUTORST_RX_, 0x1);

    /* Disable AUTORST_LB */
    mRegFieldSet(regVal, cAf6_upen_addr_03_AUTORST_LB_, 0x0);

    /* Disable AUTORST_RX_LINKDOWN */
    mRegFieldSet(regVal, cAf6_upen_addr_03_AUTORST_RX_LINKDOWN_, 0x0);

    AtSerdesControllerWrite(self, address, regVal, cAtModuleEth);
    return cAtOk;
    }

static uint32 DefaultTxDiffValue(AtSerdesController self)
    {
    if (Tha60210061SerdesIsSgmii(self))
        return 0x18;

    return 0xC;
    }

static uint32 DefaultTxPostCursorValue(AtSerdesController self)
    {
    if (Tha60210061SerdesIsQsgmii(self))
        return 0x14;

    return 0x0;
    }

static eAtRet DefaultParamsSet(AtSerdesController self)
    {
    eAtRet ret = cAtOk;

    ret |= AtSerdesControllerPhysicalParamSet(self, cAtSerdesParamTxDiffCtrl, DefaultTxDiffValue(self));
    ret |= AtSerdesControllerPhysicalParamSet(self, cAtSerdesParamTxPreCursor, 0);
    ret |= AtSerdesControllerPhysicalParamSet(self, cAtSerdesParamTxPostCursor, DefaultTxPostCursorValue(self));

    return ret;
    }

static eAtRet Init(AtSerdesController self)
    {
    eAtRet ret = m_AtSerdesControllerMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    ret  = DefaultAutoResetSet(self);
    ret |= DefaultParamsSet(self);

    return ret;
    }

static eAtRet WaitForResetDone(AtSerdesController self, uint32 statusReg, uint32 notDoneMask)
    {
    tAtOsalCurTime curTime, startTime;
    const uint32 cResetTimeoutInMs = 5000;
    uint32 elapse = 0;
    uint32 regVal = 0;

    AtOsalCurTimeGet(&startTime);
    while (elapse < cResetTimeoutInMs)
        {
        regVal = AtSerdesControllerRead(self, statusReg, cAtModuleEth);
        if ((regVal & notDoneMask) == 0)
            return cAtOk;

        /* Calculate elapse time and retry */
        AtOsalCurTimeGet(&curTime);
        elapse = mTimeIntervalInMsGet(startTime, curTime);
        }

    AtDeviceLog(AtSerdesControllerDeviceGet(self), cAtLogLevelWarning, AtSourceLocation,
                "SERDES %s.%u reset timeout, the corresponding RSP may not exist, status = 0x%X\r\n",
                AtSerdesControllerDescription(self), AtSerdesControllerIdGet(self) + 1, regVal);

    return cAtErrorSerdesResetTimeout;
    }

static eBool IsSimulated(AtSerdesController self)
    {
    AtChannel port = AtSerdesControllerPhysicalPortGet(self);
    return AtDeviceIsSimulated(AtChannelDeviceGet(port));
    }

static eAtRet ResetByMask(AtSerdesController self, uint32 resetReg, uint32 resetMask, uint32 statusReg, uint32 notDoneMask, uint32 stickyReg)
    {
    uint32 regVal = AtSerdesControllerRead(self, resetReg, cAtModuleEth);
    const uint32 cResetTriggerTime = 1000;
    eAtRet ret = cAtOk;

    if (AtSerdesControllerInAccessible(self))
        return cAtOk;

    /* Request */
    AtSerdesControllerWrite(self, resetReg, regVal | resetMask, cAtModuleEth);
    if (!IsSimulated(self))
        AtOsalUSleep(cResetTriggerTime);

    AtSerdesControllerWrite(self, resetReg, regVal & (~resetMask), cAtModuleEth);

    /* Some reset does not have not done mask, don't need to wait in that case */
    if (notDoneMask)
        ret = WaitForResetDone(self, statusReg, notDoneMask);

    AtSerdesControllerWrite(self, stickyReg, cBit31_0, cAtModuleEth);
    return ret;
    }

static uint32 LpmEnableRegister(Tha6021EthPortSerdesController self)
    {
    AtUnused(self);
    return (uint32)(cAf6Reg_upen_addr_02_Base + Offset((AtSerdesController)self));
    }

static uint32 LpmEnableMask(Tha6021EthPortSerdesController self, uint32 serdesId)
    {
    AtUnused(self);
    AtUnused(serdesId);
    return cAf6_upen_addr_02_RXLPMEN_Mask;
    }

static uint32 LpmEnableShift(Tha6021EthPortSerdesController self, uint32 serdesId)
    {
    AtUnused(self);
    AtUnused(serdesId);
    return cAf6_upen_addr_02_RXLPMEN_Shift;
    }

static uint32 TxPreCursorAddress(Tha6021EthPortSerdesController self)
    {
    AtUnused(self);
    return (uint32)(cAf6Reg_upen_addr_08_Base + Offset((AtSerdesController)self));
    }

static uint32 PreCursorMask(Tha6021EthPortSerdesController self, uint32 serdesId)
    {
    AtUnused(self);
    AtUnused(serdesId);
    return cAf6_upen_addr_08_TXPRECURSORL_Mask;
    }

static uint32 PreCursorShift(Tha6021EthPortSerdesController self, uint32 serdesId)
    {
    AtUnused(self);
    AtUnused(serdesId);
    return cAf6_upen_addr_08_TXPRECURSORL_Shift;
    }

static uint32 TxPostCursorAddress(Tha6021EthPortSerdesController self)
    {
    AtUnused(self);
    return (uint32)(cAf6Reg_upen_addr_08_Base + Offset((AtSerdesController)self));
    }

static uint32 PostCursorMask(Tha6021EthPortSerdesController self, uint32 serdesId)
    {
    AtUnused(self);
    AtUnused(serdesId);
    return cAf6_upen_addr_08_TXPOSCURSORL_Mask;
    }

static uint32 PostCursorShift(Tha6021EthPortSerdesController self, uint32 serdesId)
    {
    AtUnused(self);
    AtUnused(serdesId);
    return cAf6_upen_addr_08_TXPOSCURSORL_Shift;
    }

static uint32 TxDiffCtrlAddress(Tha6021EthPortSerdesController self)
    {
    AtUnused(self);
    return (uint32)(cAf6Reg_upen_addr_08_Base + Offset((AtSerdesController)self));
    }

static uint32 DiffCtrlMask(Tha6021EthPortSerdesController self, uint32 serdesId)
    {
    AtUnused(self);
    AtUnused(serdesId);
    return cAf6_upen_addr_08_TXDIFFCTL_Mask;
    }

static uint32 DiffCtrlShift(Tha6021EthPortSerdesController self, uint32 serdesId)
    {
    AtUnused(self);
    AtUnused(serdesId);
    return cAf6_upen_addr_08_TXDIFFCTL_Shift;
    }

static eAtRet LpmDfeReset(Tha6021EthPortSerdesController self)
    {
    uint32 resetReg = cAf6Reg_upen_addr_04_Base + Offset((AtSerdesController)self);
    uint32 stickyReg = cAf6Reg_upen_addr_05_Base + Offset((AtSerdesController)self);
    return ResetByMask((AtSerdesController)self, resetReg, cAf6_upen_addr_04_RXDFELPMRESET_Mask, 0, 0, stickyReg);
    }

static uint8 HwLoopbackModeGet(AtSerdesController self)
    {
    uint32 addr = cAf6Reg_upen_addr_03_Base + Offset(self);
    uint32 regVal = AtSerdesControllerRead(self, addr, cAtModuleEth);

    return (uint8)mRegField(regVal, cAf6_upen_addr_03_LOOPBACK_);
    }

static uint32 LoopbackResetMask(uint8 loopbackMode, uint8 currentMode)
    {
    if (loopbackMode == cAf6SerdesLoopbackNearEndPma)
        return cAf6_upen_addr_04_GTRXRESET_Mask;

    if (loopbackMode == cAf6SerdesLoopbackFarEndPma)
        return cAf6_upen_addr_04_GTTXRESET_Mask;

    return (currentMode == cAf6SerdesLoopbackNearEndPma) ? cAf6_upen_addr_04_GTRXRESET_Mask : cAf6_upen_addr_04_GTTXRESET_Mask;
    }

static uint32 LoopbackNotDoneMask(uint8 loopbackMode, uint8 currentMode)
    {
    if (loopbackMode == cAf6SerdesLoopbackNearEndPma)
        return cAf6_upen_addr_06_GTRXRESET_NDONE_Mask;

    if (loopbackMode == cAf6SerdesLoopbackFarEndPma)
        return cAf6_upen_addr_06_GTTXRESET_NDONE_Mask;

    return (currentMode == cAf6SerdesLoopbackNearEndPma) ? cAf6_upen_addr_06_GTRXRESET_NDONE_Mask : cAf6_upen_addr_06_GTTXRESET_NDONE_Mask;
    }

static eAtRet HwLoopbackModeSet(AtSerdesController self, uint8 hwLoopbackMode)
    {
    uint32 addr, regVal, statusReg, stickyReg;
    uint8 currentMode = HwLoopbackModeGet(self);

    if (currentMode == hwLoopbackMode)
        return cAtOk;

    addr = cAf6Reg_upen_addr_03_Base + Offset(self);
    regVal = AtSerdesControllerRead(self, addr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_upen_addr_03_LOOPBACK_, hwLoopbackMode);
    AtSerdesControllerWrite(self, addr, regVal, cAtModuleEth);

    addr = cAf6Reg_upen_addr_04_Base + Offset(self);
    statusReg = cAf6Reg_upen_addr_06_Base + Offset(self);
    stickyReg = cAf6Reg_upen_addr_05_Base + Offset(self);

    return ResetByMask(self, addr, LoopbackResetMask(hwLoopbackMode, currentMode), statusReg, LoopbackNotDoneMask(hwLoopbackMode, currentMode), stickyReg);
    }

static eBool PllIsLocked(AtSerdesController self)
    {
    uint32 address = cAf6Reg_upen_addr_06_Base + Offset(self);
    uint32 regVal  = AtSerdesControllerRead(self, address, cAtModuleEth);

    if (regVal & cAf6_upen_addr_06_GTPLL_NLOCK_Mask)
        return cAtFalse;

    return cAtTrue;
    }

static uint32 AlarmGet(AtSerdesController self)
    {
    uint32 address = cAf6Reg_upen_addr_06_Base + Offset(self);
    uint32 regVal  = AtSerdesControllerRead(self, address, cAtModuleEth);

    if (regVal & cAf6_upen_addr_06_LINK_DOWN_Mask)
        return cAtSerdesAlarmTypeLinkDown;

    return 0;
    }

static eAtSerdesLinkStatus LinkStatus(AtSerdesController self)
    {
    uint32 address = cAf6Reg_upen_addr_06_Base + Offset(self);
    uint32 regVal  = AtSerdesControllerRead(self, address, cAtModuleEth);

    if (regVal & cAf6_upen_addr_06_LINK_DOWN_Mask)
        return cAtSerdesLinkStatusDown;

    return cAtSerdesLinkStatusUp;
    }

static uint32 AlarmHistoryGet(AtSerdesController self)
    {
    uint32 address = cAf6Reg_upen_addr_05_Base + Offset(self);
    uint32 regVal  = AtSerdesControllerRead(self, address, cAtModuleEth);

    if (regVal & cAf6_upen_addr_05_LINK_DOWN_Mask)
        return cAtSerdesAlarmTypeLinkDown;

    return 0;
    }

static uint32 AlarmHistoryClear(AtSerdesController self)
    {
    uint32 address = cAf6Reg_upen_addr_05_Base + Offset(self);
    uint32 regVal  = AtSerdesControllerRead(self, address, cAtModuleEth);

    AtSerdesControllerWrite(self, address, cAf6_upen_addr_05_LINK_DOWN_Mask, cAtModuleEth);
    if (regVal & cAf6_upen_addr_05_LINK_DOWN_Mask)
        return cAtSerdesAlarmTypeLinkDown;

    return 0;
    }

static eAtRet Reset(AtSerdesController self)
    {
    uint32 resetReg  = cAf6Reg_upen_addr_04_Base + Offset(self);
    uint32 statusReg = cAf6Reg_upen_addr_06_Base + Offset(self);
    uint32 stickyReg = cAf6Reg_upen_addr_05_Base + Offset(self);

    eAtRet ret = ResetByMask(self, resetReg, cAf6_upen_addr_04_GTRESET_Mask, statusReg, cAf6_upen_addr_06_GTRESET_NDONE_Mask, stickyReg);
    Tha60210061SerdesManagerSerdesResetResultCache(AtSerdesControllerManagerGet(self), self, (ret == cAtOk));

    return ret;
    }

static eAtRet RxReset(AtSerdesController self)
    {
    uint32 resetReg  = cAf6Reg_upen_addr_04_Base + Offset(self);
    uint32 statusReg = cAf6Reg_upen_addr_06_Base + Offset(self);
    uint32 stickyReg = cAf6Reg_upen_addr_05_Base + Offset(self);

	return ResetByMask(self, resetReg, cAf6_upen_addr_04_GTRXRESET_Mask, statusReg, cAf6_upen_addr_06_GTRXRESET_NDONE_Mask, stickyReg);
    }

static eAtRet TxReset(AtSerdesController self)
    {
    uint32 resetReg  = cAf6Reg_upen_addr_04_Base + Offset(self);
    uint32 statusReg = cAf6Reg_upen_addr_06_Base + Offset(self);
    uint32 stickyReg = cAf6Reg_upen_addr_05_Base + Offset(self);

	return ResetByMask(self, resetReg, cAf6_upen_addr_04_GTTXRESET_Mask, statusReg, cAf6_upen_addr_06_GTTXRESET_NDONE_Mask, stickyReg);
    }

static eBool PowerCanControl(AtSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void QPLLPowerDown(AtSerdesController self, eBool powerDown)
    {
    uint32 address = cAf6Reg_upen_addr_0b_Base + Offset(self);
    uint32 regVal = AtSerdesControllerRead(self, address, cAtModuleEth);

    mRegFieldSet(regVal, cAf6_upen_addr_0b_QPLL1PD_, mBoolToBin(powerDown));
    mRegFieldSet(regVal, cAf6_upen_addr_0b_QPLL0PD_, mBoolToBin(powerDown));
    AtSerdesControllerWrite(self, address, regVal, cAtModuleEth);
    }

static void CPLLPowerDown(AtSerdesController self, eBool powerDown)
    {
    uint32 address = cAf6Reg_upen_addr_0a_Base + Offset(self);
    uint32 regVal = AtSerdesControllerRead(self, address, cAtModuleEth);

    mRegFieldSet(regVal, cAf6_upen_addr_0a_CPLL0PD_, mBoolToBin(powerDown));
    AtSerdesControllerWrite(self, address, regVal, cAtModuleEth);
    }

static void PmaPowerDown(AtSerdesController self, eBool powerDown)
    {
    uint32 address = cAf6Reg_upen_addr_0f_Base + Offset(self);
    uint32 regVal = AtSerdesControllerRead(self, address, cAtModuleEth);

    mRegFieldSet(regVal, cAf6_upen_addr_0f_GT_TXPD_, (powerDown) ? 0x3 : 0x0);
    mRegFieldSet(regVal, cAf6_upen_addr_0f_GT_RXPD_, (powerDown) ? 0x3 : 0x0);
    AtSerdesControllerWrite(self, address, regVal, cAtModuleEth);
    }

static eBool PmaPowerIsDown(AtSerdesController self)
    {
    uint32 address = cAf6Reg_upen_addr_0f_Base + Offset(self);
    uint32 regVal = AtSerdesControllerRead(self, address, cAtModuleEth);

    if (mRegField(regVal, cAf6_upen_addr_0f_GT_TXPD_) && mRegField(regVal, cAf6_upen_addr_0f_GT_RXPD_))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet PowerDown(AtSerdesController self, eBool powerDown)
    {
    PmaPowerDown(self, powerDown);
    QPLLPowerDown(self, powerDown);
    CPLLPowerDown(self, powerDown);

    return cAtOk;
    }

static eBool PowerIsDown(AtSerdesController self)
    {
    return PmaPowerIsDown(self);
    }

static eAtRet TimingModeSet(AtSerdesController self, eAtSerdesTimingMode timingMode)
    {
    uint32 address, regVal;
    eAtRet ret = m_AtSerdesControllerMethods->TimingModeSet(self, timingMode);
    if (ret != cAtOk)
        return ret;

    address = cAf6Reg_upen_addr_02_Base + Offset(self);
    regVal = AtSerdesControllerRead(self, address, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_upen_addr_02_RXCDRHOLD_, (timingMode == cAtSerdesTimingModeLockToRef) ? 1 : 0);

    AtSerdesControllerWrite(self, address, regVal, cAtModuleEth);
    return cAtOk;
    }

static eAtSerdesTimingMode TimingModeGet(AtSerdesController self)
    {
    uint32 address = cAf6Reg_upen_addr_02_Base + Offset(self);
    uint32 regVal = AtSerdesControllerRead(self, address, cAtModuleEth);

    if (mRegField(regVal, cAf6_upen_addr_02_RXCDRHOLD_) == 1)
        return cAtSerdesTimingModeLockToRef;

    return cAtSerdesTimingModeLockToData;
    }

static eAtSerdesEqualizerMode DefaultEqualizerMode(AtSerdesController self)
    {
    AtUnused(self);
    return cAtSerdesEqualizerModeLpm;
    }

static eBool ShouldInitPrbsEngine(Tha6021EthPortSerdesController self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSerdesControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, m_AtSerdesControllerMethods, sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, Init);
        mMethodOverride(m_AtSerdesControllerOverride, PllIsLocked);
        mMethodOverride(m_AtSerdesControllerOverride, AlarmHistoryGet);
        mMethodOverride(m_AtSerdesControllerOverride, AlarmHistoryClear);
        mMethodOverride(m_AtSerdesControllerOverride, AlarmGet);
        mMethodOverride(m_AtSerdesControllerOverride, LinkStatus);
        mMethodOverride(m_AtSerdesControllerOverride, Reset);
        mMethodOverride(m_AtSerdesControllerOverride, RxReset);
        mMethodOverride(m_AtSerdesControllerOverride, TxReset);
        mMethodOverride(m_AtSerdesControllerOverride, PowerCanControl);
        mMethodOverride(m_AtSerdesControllerOverride, PowerDown);
        mMethodOverride(m_AtSerdesControllerOverride, PowerIsDown);
        mMethodOverride(m_AtSerdesControllerOverride, TimingModeSet);
        mMethodOverride(m_AtSerdesControllerOverride, TimingModeGet);
        mMethodOverride(m_AtSerdesControllerOverride, DefaultEqualizerMode);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void OverrideTha6021EthPortSerdesController(AtSerdesController self)
    {
    Tha6021EthPortSerdesController controller = (Tha6021EthPortSerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6021EthPortSerdesControllerOverride, mMethodsGet(controller), sizeof(m_Tha6021EthPortSerdesControllerOverride));

        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, LpmEnableRegister);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, LpmEnableMask);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, LpmEnableShift);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, LpmDfeReset);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, TxPostCursorAddress);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, PostCursorMask);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, PostCursorShift);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, TxPreCursorAddress);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, PreCursorMask);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, PreCursorShift);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, TxDiffCtrlAddress);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, DiffCtrlMask);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, DiffCtrlShift);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, HwLoopbackModeSet);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, HwLoopbackModeGet);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, ShouldInitPrbsEngine);
        }

    mMethodsSet(controller, &m_Tha6021EthPortSerdesControllerOverride);
    }


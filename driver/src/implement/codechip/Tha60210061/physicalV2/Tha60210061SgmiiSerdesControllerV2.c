/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha60210061SgmiiSerdesController.c
 *
 * Created Date: Jun 23, 2016
 *
 * Description : 60210061 SGMII SERDES controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../physical/Tha60210061SerdesInternal.h"
#include "../physical/Tha60210061SerdesManager.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/\

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061SgmiiSerdesControllerV2
    {
    tTha60210061SgmiiSerdesController super;
    }tTha60210061SgmiiSerdesControllerV2;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
#include "Tha60210061SerdesControllerV2Common.h"

static void Override(AtSerdesController self)
    {
    OverrideAtSerdesController(self);
    OverrideTha6021EthPortSerdesController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061SgmiiSerdesControllerV2);
    }

static AtSerdesController ObjectInit(AtSerdesController self, AtEthPort ethPort, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210061SgmiiSerdesControllerObjectInit(self, ethPort, serdesId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController Tha60210061SgmiiSerdesControllerV2New(AtEthPort ethPort, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, ethPort, serdesId);
    }

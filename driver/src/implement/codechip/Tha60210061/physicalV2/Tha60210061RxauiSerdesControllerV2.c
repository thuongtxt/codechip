/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha60210061RxauiSerdesControllerV2.c
 *
 * Created Date: Apr 18, 2017
 *
 * Description : 60210061 RXAUI Serdes controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../physical/Tha60210061SerdesInternal.h"
#include "../physical/Tha60210061SerdesManager.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061RxauiSerdesControllerV2
    {
    tTha60210061RxauiSerdesController super;
    }tTha60210061RxauiSerdesControllerV2;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
#include "Tha60210061SerdesControllerV2Common.h"

static uint32 LaneOffset(AtSerdesController self, uint32 laneId)
    {
    return (uint32)(cSerdesTuningV2BaseAddress + AtSerdesControllerIdGet(self) * 0x800 + laneId * 0x40);
    }

static eAtRet HwTxPostCursorSet(AtSerdesController self, uint32 value)
    {
    uint32 address, regVal;
    uint32 lane_i;

    for (lane_i = 0; lane_i < AtSerdesControllerNumLanes(self); lane_i++)
        {
        address = cAf6Reg_upen_addr_08_Base + LaneOffset(self, lane_i);
        regVal = AtSerdesControllerRead(self, address, cAtModuleEth);

        mRegFieldSet(regVal, cAf6_upen_addr_08_TXPOSCURSORL_, value);
        AtSerdesControllerWrite(self, address, regVal, cAtModuleEth);
        }

    return cAtOk;
    }

static eAtRet HwTxPreCursorSet(AtSerdesController self, uint32 value)
    {
    uint32 address, regVal;
    uint8 lane_i;

    for (lane_i = 0; lane_i < AtSerdesControllerNumLanes(self); lane_i++)
        {
        address = cAf6Reg_upen_addr_08_Base + LaneOffset(self, lane_i);
        regVal = AtSerdesControllerRead(self, address, cAtModuleEth);

        mRegFieldSet(regVal, cAf6_upen_addr_08_TXPRECURSORL_, value);
        AtSerdesControllerWrite(self, address, regVal, cAtModuleEth);
        }

    return cAtOk;
    }

static eAtRet HwTxDiffCtrlSet(AtSerdesController self, uint32 value)
    {
    uint32 address, regVal;
    uint8 lane_i;

    for (lane_i = 0; lane_i < AtSerdesControllerNumLanes(self); lane_i++)
        {
        address = cAf6Reg_upen_addr_08_Base + LaneOffset(self, lane_i);
        regVal = AtSerdesControllerRead(self, address, cAtModuleEth);

        mRegFieldSet(regVal, cAf6_upen_addr_08_TXDIFFCTL_, value);
        AtSerdesControllerWrite(self, address, regVal, cAtModuleEth);
        }

    return cAtOk;
    }

static eAtRet HwPhysicalParamSet(AtSerdesController self, eAtSerdesParam param, uint32 value)
    {
    if (param == cAtSerdesParamTxPreCursor)
        return HwTxPreCursorSet(self, value);

    if (param == cAtSerdesParamTxPostCursor)
        return HwTxPostCursorSet(self, value);

    if (param == cAtSerdesParamTxDiffCtrl)
        return HwTxDiffCtrlSet(self, value);

    return cAtOk;
    }

static eAtRet PhysicalParamSet(AtSerdesController self, eAtSerdesParam param, uint32 value)
    {
    if (mMethodsGet(self)->PhysicalParamValueIsInRange(self, param, value))
        return HwPhysicalParamSet(self, param, value);

    return cAtErrorOutOfRangParm;
    }

static eAtRet LpmDfeLaneReset(AtSerdesController self, uint32 laneId)
    {
    uint32 address = cAf6Reg_upen_addr_04_Base + LaneOffset(self, laneId);
    uint32 regVal = AtSerdesControllerRead(self, address, cAtModuleEth);
    const uint32 cResetTriggerTime = 1000;

    if (AtSerdesControllerInAccessible(self))
        return cAtOk;

    /* Request */
    AtSerdesControllerWrite(self, address, regVal | cAf6_upen_addr_04_RXDFELPMRESET_Mask, cAtModuleEth);

    if (!IsSimulated(self))
        AtOsalUSleep(cResetTriggerTime);

    AtSerdesControllerWrite(self, address, regVal & (~cAf6_upen_addr_04_RXDFELPMRESET_Mask), cAtModuleEth);
    return cAtOk;
    }

static eAtRet EqualizerModeSet(AtSerdesController self, eAtSerdesEqualizerMode mode)
    {
    uint32 regVal, address, lane_i;
    eAtRet ret = cAtOk;

    if (!AtSerdesControllerEqualizerModeIsSupported(self, mode))
        return cAtErrorModeNotSupport;

    for (lane_i = 0; lane_i < AtSerdesControllerNumLanes(self); lane_i++)
        {
        address = (uint32)(cAf6Reg_upen_addr_02_Base + LaneOffset(self, lane_i));
        regVal = AtSerdesControllerRead(self, address, cAtModuleEth);

        mRegFieldSet(regVal, cAf6_upen_addr_02_RXLPMEN_, (mode == cAtSerdesEqualizerModeLpm) ? 1 : 0);
        AtSerdesControllerWrite(self, address, regVal, cAtModuleEth);

        ret |= LpmDfeLaneReset(self, lane_i);
        }

    return ret;
    }

static eAtSerdesEqualizerMode EqualizerModeGet(AtSerdesController self)
    {
    uint32 address = (uint32)(cAf6Reg_upen_addr_02_Base + LaneOffset(self, 0));
    uint32 regVal = AtSerdesControllerRead(self, address, cAtModuleEth);
    uint32 lpmEnabled = mRegField(regVal, cAf6_upen_addr_02_RXLPMEN_);

    return (lpmEnabled) ? cAtSerdesEqualizerModeLpm : cAtSerdesEqualizerModeDfe;
    }

static eAtRet AllLanesHwLoopbackModeSet(AtSerdesController self, uint8 hwLoopbackMode)
    {
    uint32 lane_i;
    uint8 currentMode = HwLoopbackModeGet(self);
    eAtRet ret = cAtOk;

    if (currentMode == hwLoopbackMode)
        return cAtOk;

    for (lane_i = 0; lane_i < AtSerdesControllerNumLanes(self); lane_i++)
        {
        uint32 landOffset = LaneOffset(self, lane_i);
        uint32 addr       = cAf6Reg_upen_addr_03_Base + landOffset;
        uint32 regVal     = AtSerdesControllerRead(self, addr, cAtModuleEth);
        uint32 statusReg, stickyReg;

        mRegFieldSet(regVal, cAf6_upen_addr_03_LOOPBACK_, hwLoopbackMode);
        AtSerdesControllerWrite(self, addr, regVal, cAtModuleEth);

        addr = cAf6Reg_upen_addr_04_Base + landOffset;
        statusReg = cAf6Reg_upen_addr_06_Base + landOffset;
        stickyReg = cAf6Reg_upen_addr_05_Base + landOffset;

        ret |= ResetByMask(self, addr, LoopbackResetMask(hwLoopbackMode, currentMode), statusReg, LoopbackNotDoneMask(hwLoopbackMode, currentMode), stickyReg);
        }

    return ret;
    }

static void LinkStatusLatch(AtSerdesController self)
    {
    uint32 address = cAf6Reg_upen_addr_10_Base + Offset(self);
    const uint32 cTriggerValue = 0xC;

    AtSerdesControllerWrite(self, address, 0, cAtModuleEth);
    AtSerdesControllerWrite(self, address, cTriggerValue, cAtModuleEth);
    }

static uint32 RxauiAlarmGet(AtSerdesController self)
    {
    uint32 address, regVal;

    LinkStatusLatch(self);
    address = cAf6Reg_upen_addr_06_Base + Offset(self);
    regVal  = AtSerdesControllerRead(self, address, cAtModuleEth);

    if (regVal & cAf6_upen_addr_06_LINK_DOWN_Mask)
        return cAtSerdesAlarmTypeLinkDown;

    return 0;
    }

static eAtSerdesLinkStatus RxauiLinkStatus(AtSerdesController self)
    {
    uint32 address, regVal;

    LinkStatusLatch(self);
    address = cAf6Reg_upen_addr_06_Base + Offset(self);
    regVal  = AtSerdesControllerRead(self, address, cAtModuleEth);
    if (regVal & cAf6_upen_addr_06_LINK_DOWN_Mask)
        return cAtSerdesLinkStatusDown;

    return cAtSerdesLinkStatusUp;
    }

static void AllLanesPmaPowerDown(AtSerdesController self, eBool powerDown)
    {
    uint32 lane_i;

    for (lane_i = 0; lane_i < AtSerdesControllerNumLanes(self); lane_i++)
        {
        uint32 landOffset = LaneOffset(self, lane_i);
        uint32 address = cAf6Reg_upen_addr_0f_Base + landOffset;
        uint32 regVal = AtSerdesControllerRead(self, address, cAtModuleEth);

        mRegFieldSet(regVal, cAf6_upen_addr_0f_GT_TXPD_, (powerDown) ? 0x3 : 0x0);
        mRegFieldSet(regVal, cAf6_upen_addr_0f_GT_RXPD_, (powerDown) ? 0x3 : 0x0);
        AtSerdesControllerWrite(self, address, regVal, cAtModuleEth);
        }
    }

static eAtRet RxauiPowerDown(AtSerdesController self, eBool powerDown)
    {
    AllLanesPmaPowerDown(self, powerDown);
    QPLLPowerDown(self, powerDown);
    CPLLPowerDown(self, powerDown);

    return cAtOk;
    }

static eAtRet AllLanesReset(AtSerdesController self)
    {
    uint32 lane_i;
    eAtRet ret = cAtOk;

    for (lane_i = 0; lane_i < AtSerdesControllerNumLanes(self); lane_i++)
        {
        uint32 landOffset = LaneOffset(self, lane_i);
        uint32 resetReg = cAf6Reg_upen_addr_04_Base + landOffset;
        uint32 statusReg = cAf6Reg_upen_addr_06_Base + landOffset;
        uint32 stickyReg = cAf6Reg_upen_addr_05_Base + landOffset;

        ret |= ResetByMask(self, resetReg, cAf6_upen_addr_04_GTRESET_Mask, statusReg, cAf6_upen_addr_06_GTRESET_NDONE_Mask, stickyReg);
        }

    Tha60210061SerdesManagerSerdesResetResultCache(AtSerdesControllerManagerGet(self), self, (ret == cAtOk));
    return ret;
    }

static eAtRet RxLanesReset(AtSerdesController self)
    {
    uint32 lane_i;
    eAtRet ret = cAtOk;

    for (lane_i = 0; lane_i < AtSerdesControllerNumLanes(self); lane_i++)
        {
        uint32 landOffset = LaneOffset(self, lane_i);
        uint32 resetReg = cAf6Reg_upen_addr_04_Base + landOffset;
        uint32 statusReg = cAf6Reg_upen_addr_06_Base + landOffset;
        uint32 stickyReg = cAf6Reg_upen_addr_05_Base + landOffset;

        ret |= ResetByMask(self, resetReg, cAf6_upen_addr_04_GTRXRESET_Mask, statusReg, cAf6_upen_addr_06_GTRXRESET_NDONE_Mask, stickyReg);
        }

    return ret;
    }

static eAtRet TxLanesReset(AtSerdesController self)
    {
    uint32 lane_i;
    eAtRet ret = cAtOk;

    for (lane_i = 0; lane_i < AtSerdesControllerNumLanes(self); lane_i++)
        {
        uint32 landOffset = LaneOffset(self, lane_i);
        uint32 resetReg = cAf6Reg_upen_addr_04_Base + landOffset;
        uint32 statusReg = cAf6Reg_upen_addr_06_Base + landOffset;
        uint32 stickyReg = cAf6Reg_upen_addr_05_Base + landOffset;

        ret |= ResetByMask(self, resetReg, cAf6_upen_addr_04_GTTXRESET_Mask, statusReg, cAf6_upen_addr_06_GTTXRESET_NDONE_Mask, stickyReg);
        }

    return ret;
    }

static eAtRet AllLanesDefaultSet(AtSerdesController self)
    {
    uint32 lane_i;

    for (lane_i = 0; lane_i < AtSerdesControllerNumLanes(self); lane_i++)
        {
        uint32 address = cAf6Reg_upen_addr_03_Base + LaneOffset(self, lane_i);
        uint32 regVal = AtSerdesControllerRead(self, address, cAtModuleEth);

        /* Enable Remote_Faul_Tx */
        mRegFieldSet(regVal, cAf6_upen_addr_03_Remote_Faul_Tx_, 0x1);

        /* Enable AUTORST_RX_SIGNAL */
        mRegFieldSet(regVal, cAf6_upen_addr_03_AUTORST_RX_SIGNAL_, 0x1);

        /* Enable AUTORST_RX */
        mRegFieldSet(regVal, cAf6_upen_addr_03_AUTORST_RX_, 0x1);

        /* Disable AUTORST_LB */
        mRegFieldSet(regVal, cAf6_upen_addr_03_AUTORST_LB_, 0x0);

        /* Disable AUTORST_RX_LINKDOWN */
        mRegFieldSet(regVal, cAf6_upen_addr_03_AUTORST_RX_LINKDOWN_, 0x1);

        AtSerdesControllerWrite(self, address, regVal, cAtModuleEth);
        }

    return cAtOk;
    }

static eAtRet RxauiInit(AtSerdesController self)
    {
    eAtRet ret = m_AtSerdesControllerMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return AllLanesDefaultSet(self);
    }

static tAtAsyncOperationFunc AsyncOperationGet(AtSerdesController self, uint32 state)
    {
    return (state < 1) ? (tAtAsyncOperationFunc)(mMethodsGet(self)->Init) : NULL;
    }

static void OverrideAtSerdesControllerCont(AtSerdesController self)
    {
    AtUnused(self);
    if (!m_methodsInit)
        {
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalParamSet);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerModeGet);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerModeSet);
        mMethodOverride(m_AtSerdesControllerOverride, AsyncOperationGet);
        m_AtSerdesControllerOverride.LinkStatus        = RxauiLinkStatus;
        m_AtSerdesControllerOverride.AlarmGet          = RxauiAlarmGet;
        m_AtSerdesControllerOverride.PowerDown         = RxauiPowerDown;
        m_AtSerdesControllerOverride.Reset             = AllLanesReset;
        m_AtSerdesControllerOverride.RxReset           = RxLanesReset;
        m_AtSerdesControllerOverride.TxReset           = TxLanesReset;
        m_AtSerdesControllerOverride.Init              = RxauiInit;
        }
    }

static void OverrideTha6021EthPortSerdesControllerCont(AtSerdesController self)
    {
    AtUnused(self);
    if (!m_methodsInit)
        m_Tha6021EthPortSerdesControllerOverride.HwLoopbackModeSet = AllLanesHwLoopbackModeSet;
    }

static void Override(AtSerdesController self)
    {
    OverrideAtSerdesController(self);
    OverrideAtSerdesControllerCont(self);
    OverrideTha6021EthPortSerdesController(self);
    OverrideTha6021EthPortSerdesControllerCont(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061RxauiSerdesControllerV2);
    }

static AtSerdesController ObjectInit(AtSerdesController self, AtEthPort ethPort, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210061RxauiSerdesControllerObjectInit(self, ethPort, serdesId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController Tha60210061RxauiSerdesControllerV2New(AtEthPort ethPort, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, ethPort, serdesId);
    }

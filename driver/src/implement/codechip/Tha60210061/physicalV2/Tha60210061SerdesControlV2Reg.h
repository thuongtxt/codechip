/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha60210061SerdesControlV2Reg.h
 * 
 * Created Date: Jun 8, 2017
 *
 * Description : Serdes control version 2 registers
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061SERDESCONTROLV2REG_H_
#define _THA60210061SERDESCONTROLV2REG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cHssi_serdes_id 0x0

/* Base address register serdes */
#define cSerdesTuningV2BaseAddress 0xF70000

/* Gear box, buf, detect access via DRP */
#define cTxGearBoxEnable                                      0x7C
#define cTxGearBox_en_Mask                                  cBit13
#define cTxGearBox_en_Shift                                     13

#define cTxBuf_en_Mask                                       cBit7
#define cTxBuf_en_Shift                                          7

#define cTxBuf_Reset_ON_RATE_CHANGE_Mask                     cBit6
#define cTxBuf_Reset_ON_RATE_CHANGE_Shift                        6

#define cTX_RxDetect_Ref_Mask                              cBit5_3
#define cTX_RxDetect_Ref_Shift                                   3

/* Drp base address serdes */
#define cDrpBaseAddressSgmii1                             0xF41000
#define cDrpBaseAddressSgmii2                             0xF41400
#define cDrpBaseAddressSgmii3                             0xF41800
#define cDrpBaseAddressSgmii4                             0xF41C00

#define cDrpBaseAddressXfi1                               0xF43000
#define cDrpBaseAddressXfi2                               0xF44000

#define cDrpBaseAddressQsgmii1                            0xF57000
#define cDrpBaseAddressQsgmii2                            0xF57400

#define cDrpBaseAddressRxaui1_1                           0xF2B000
#define cDrpBaseAddressRxaui1_2                           0xF2B400
#define cDrpBaseAddressRxaui2_1                           0xF2D000
#define cDrpBaseAddressRxaui2_2                           0xF2D400

/* Drp counter address */
#define cCounterLowDrpAddressGtyE3                           0x25E
#define cCounterLowDrpAddressGthE3                           0x15E

/* Hssi serdes ID */
#define cHssiSerdesIdRxaui1                                    0x0
#define cHssiSerdesIdRxaui2                                    0x1

/* Xfi register through MDIO */
#define cMdioRegSerdesControl1Register     cMdioRegRegister(3, 42)

/*------------------------------------------------------------------------------
Reg Name   : Serdes Signature
Reg Addr   : 0x0_0000
Reg Formula: 0x0_0000 + $hssi_quad_id * 0x800 + $hssi_lane_id * 0x40
    Where  :
           + $hssi_quad_id (00-09) : high speed serial interface
           + $hssi_lane_id (00-03): serdes id of each high speed serial interface}
Reg Desc   :
This register  is used to show Serdes Signature

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_addr_00_Base                                                                      0x00000

/*--------------------------------------
BitField Name: Signature
BitField Type: RO
BitField Desc: Serdes Signature 16'hA403
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_addr_00_Signature_Mask                                                              cBit15_0
#define cAf6_upen_addr_00_Signature_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Serdes Version 1
Reg Addr   : 0x0_0000
Reg Formula: 0x0_0000 + $hssi_quad_id * 0x800 + $hssi_lane_id * 0x40
    Where  :
           + $hssi_quad_id (00-09) : high speed serial interface
           + $hssi_lane_id (00-03): serdes id of each high speed serial interface}
Reg Desc   :
This register  is used to show serdes vesion

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_addr_01_Base                                                                      0x00000

/*--------------------------------------
BitField Name: Vendor
BitField Type: R/W
BitField Desc: Serdes Vendor 0x1  : Altera 0x2  : Xilinx Other: Reserved
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_upen_addr_01_Vendor_Mask                                                                cBit15_12
#define cAf6_upen_addr_01_Vendor_Shift                                                                      12

/*--------------------------------------
BitField Name: Interface
BitField Type:
BitField Desc: Interface support 0x0  : Multi-Rate/Multi service 0x1  :
STM-1/STM-4/STM-16 0x2  : 1000Base-x/SGMII/2.5G Base-x 0x3  : XFI (10GBase-R
/10GBase-KR) 0x4  : RXAUI 0x5  : QSGMII Other: Reserved
BitField Bits: [11:08]
--------------------------------------*/
#define cAf6_upen_addr_01_Interface_Mask                                                              cBit11_8
#define cAf6_upen_addr_01_Interface_Shift                                                                    8

/*--------------------------------------
BitField Name: Type
BitField Type:
BitField Desc: Serdes Type, user for config SDRDES param by DRP.. For Xilinx
Vendor 0x2  : GTX 0x3  : GTH 0x4  : GTY Other: Reserved
BitField Bits: [07:04]
--------------------------------------*/
#define cAf6_upen_addr_01_Type_Mask                                                                    cBit7_4
#define cAf6_upen_addr_01_Type_Shift                                                                         4

/*--------------------------------------
BitField Name: Version
BitField Type: RO
BitField Desc: Serdes version , user for config SDRDES param by DRP.. For Xilinx
Vendor 0x1  : GT*E1 0x2  : GT*E2 -> 7 seria fpga 0x3  : GT*E3 -> Ultrascale fpga
0x4  : GT*E4 -> Ultrascale plus fpga Other: Reserved
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_upen_addr_01_Version_Mask                                                                 cBit3_0
#define cAf6_upen_addr_01_Version_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Serdes User Interface Control
Reg Addr   : 0x0_0002
Reg Formula: 0x0_0002 + $hssi_quad_id * 0x800 + $hssi_lane_id * 0x40
    Where  :
           + $hssi_quad_id (00-09) : high speed serial interface
           + $hssi_lane_id (00-03): serdes id of each high speed serial interface}
Reg Desc   :
This register  is used to control Interface when dynamic Interface

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_addr_02_Base                                                                      0x00002

/*--------------------------------------
BitField Name: RX8B10BEN
BitField Type: R/W
BitField Desc: TX8B10BEN
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_upen_addr_02_RX8B10BEN_Mask                                                                cBit15
#define cAf6_upen_addr_02_RX8B10BEN_Shift                                                                   15

/*--------------------------------------
BitField Name: RXRATE
BitField Type: R/W
BitField Desc: RXRATE
BitField Bits: [14:12]
--------------------------------------*/
#define cAf6_upen_addr_02_RXRATE_Mask                                                                cBit14_12
#define cAf6_upen_addr_02_RXRATE_Shift                                                                      12

/*--------------------------------------
BitField Name: TX8B10BEN
BitField Type: R/W
BitField Desc: TX8B10BEN
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_upen_addr_02_TX8B10BEN_Mask                                                                cBit11
#define cAf6_upen_addr_02_TX8B10BEN_Shift                                                                   11

/*--------------------------------------
BitField Name: TXRATE
BitField Type: R/W
BitField Desc: TXRATE
BitField Bits: [10:08]
--------------------------------------*/
#define cAf6_upen_addr_02_TXRATE_Mask                                                                 cBit10_8
#define cAf6_upen_addr_02_TXRATE_Shift                                                                       8

/*--------------------------------------
BitField Name: RXLPMEN
BitField Type: R/W
BitField Desc: RXLPMEN
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_addr_02_RXLPMEN_Mask                                                                   cBit5
#define cAf6_upen_addr_02_RXLPMEN_Shift                                                                      5

/*--------------------------------------
BitField Name: RXCDRHOLD
BitField Type: R/W
BitField Desc: RXCDRHOLD
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_addr_02_RXCDRHOLD_Mask                                                                 cBit4
#define cAf6_upen_addr_02_RXCDRHOLD_Shift                                                                    4

/*--------------------------------------
BitField Name: User_Intf_Mode
BitField Type: R/W
BitField Desc: User Interface Control 0x0  : STM-64/OC-192 0x1  : STM-16/OC-48
0x2  : STM-4/OC-12 0x3  : STM-1/OC-3 0x4  : XFI/10Base-R 0x5  : 1000Base-X/SGMII
0x6  : 100Base-FX other: Unused
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_upen_addr_02_User_Intf_Mode_Mask                                                          cBit3_0
#define cAf6_upen_addr_02_User_Intf_Mode_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Serdes LOOPBACK Control
Reg Addr   : 0x0_0003
Reg Formula: 0x0_0003 + $hssi_quad_id * 0x800 + $hssi_lane_id * 0x40
    Where  :
           + $hssi_quad_id (00-09) : high speed serial interface
           + $hssi_lane_id (00-03): serdes id of each high speed serial interface}
Reg Desc   :
This register  is used to control LOOPBACK

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_addr_03_Base                                                                      0x00003

/*--------------------------------------
BitField Name: LOOPBACK
BitField Type: R/W
BitField Desc: Loopback selection 0x0  : Normal 0x1  : Near-end PCS Loopback 0x2
: Near-end PMA Loopback 0x4  : Far-end PMA Loopback 0x6  : Far-end PCS Loopback
other: Unused
BitField Bits: [02:00]
--------------------------------------*/
#define cAf6_upen_addr_03_LOOPBACK_Mask                                                                cBit2_0
#define cAf6_upen_addr_03_LOOPBACK_Shift                                                                     0

#define cAf6_upen_addr_03_Remote_Faul_Tx_Mask                                                            cBit8
#define cAf6_upen_addr_03_Remote_Faul_Tx_Shift                                                               8

#define cAf6_upen_addr_03_AUTORST_RX_LINKDOWN_Mask                                                       cBit7
#define cAf6_upen_addr_03_AUTORST_RX_LINKDOWN_Shift                                                          7

#define cAf6_upen_addr_03_AUTORST_RX_SIGNAL_Mask                                                         cBit6
#define cAf6_upen_addr_03_AUTORST_RX_SIGNAL_Shift                                                            6

#define cAf6_upen_addr_03_AUTORST_RX_Mask                                                                cBit5
#define cAf6_upen_addr_03_AUTORST_RX_Shift                                                                   5

#define cAf6_upen_addr_03_AUTORST_LB_Mask                                                                cBit4
#define cAf6_upen_addr_03_AUTORST_LB_Shift                                                                   4

#define cAf6_upen_addr_03_LOOPBACK_Mask                                                                cBit2_0
#define cAf6_upen_addr_03_LOOPBACK_Shift                                                                     0

/*------------------------------------------------------------------------------
Reg Name   : Serdes RESET Control
Reg Addr   : 0x0_0004
Reg Formula: 0x0_0004 + $hssi_quad_id * 0x800 + $hssi_lane_id * 0x40
    Where  :
           + $hssi_quad_id (00-09) : high speed serial interface
           + $hssi_lane_id (00-03) : serdes id of each high speed serial interface}
Reg Desc   :
This register  is used to control RESET PER GT serdes

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_addr_04_Base                                                                      0x00004

/*--------------------------------------
BitField Name: GTRXSUBRESET_REQ
BitField Type: R/W
BitField Desc: This reset is used for special cases and specific subsection RX
resets while the GT transceiver is in normal operation bit_0: RXPMARESET bit_1:
RXPCSRESET bit_2: RXPROGDIVRESET bit_3: RXCDRRESET bit_4: RXDFELPMRESET bit_5:
RXBUFRESET bit_6: RXOOBRESET bit_7: Unused
BitField Bits: [15:08]
--------------------------------------*/
#define cAf6_upen_addr_04_GTRXSUBRESET_REQ_Mask                                                       cBit15_8
#define cAf6_upen_addr_04_GTRXSUBRESET_REQ_Shift                                                             8

/*                                         bit_0: RXPMARESET
//                                         bit_1: RXPCSRESET
//                                         bit_2: RXPROGDIVRESET
//                                         bit_3: RXCDRRESET
//                                         bit_4: RXDFELPMRESET
//                                         bit_5: RXBUFRESET
//                                         bit_6: RXOOBRESET
*/
#define cAf6_upen_addr_04_RXPMARESET_Mask     cBit8
#define cAf6_upen_addr_04_RXPCSRESET_Mask     cBit9
#define cAf6_upen_addr_04_RXPROGDIVRESET_Mask cBit10

#define cAf6_upen_addr_04_RXCDRRESET_Mask     cBit11
#define cAf6_upen_addr_04_RXDFELPMRESET_Mask  cBit12
#define cAf6_upen_addr_04_RXBUFRESET_Mask     cBit13
#define cAf6_upen_addr_04_RXOOBRESET_Mask     cBit14

/*--------------------------------------
BitField Name: GTSUBXRESET_REQ
BitField Type: R/W
BitField Desc: This reset is used for special cases and specific subsection TX
resets while the GT transceiver is in normal operation bit_0: TXPMARESET bit_1:
TXPCSRESET bit_2: TXPROGDIVRESET other: Unused
BitField Bits: [07:04]
--------------------------------------*/
#define cAf6_upen_addr_04_GTSUBXRESET_REQ_Mask                                                         cBit7_4
#define cAf6_upen_addr_04_GTSUBXRESET_REQ_Shift                                                              4

/*                                         bit_0: TXPMARESET
//                                         bit_1: TXPCSRESET
//                                         bit_2: TXPROGDIVRESET
*/
#define cAf6_upen_addr_04_TXPMARESET_Mask     cBit4
#define cAf6_upen_addr_04_TXPCSRESET_Mask     cBit5
#define cAf6_upen_addr_04_TXPROGDIVRESET_Mask cBit6

/*--------------------------------------
BitField Name: GTRESET_REQ
BitField Type: R/W
BitField Desc: Reinitialize or change loopback the GTH transceiver TX and RX
bit_0: GTRESET bit_1: GTTXRESET bit_2: GTRXRESET bit_3: Unused
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_upen_addr_04_GTRESET_REQ_Mask                                                             cBit3_0
#define cAf6_upen_addr_04_GTRESET_REQ_Shift                                                                  0
/*                                         bit_0: GTRESET
//                                         bit_1: GTTXRESET
//                                         bit_2: GTRXRESET
*/
#define cAf6_upen_addr_04_GTRESET_Mask     cBit0
#define cAf6_upen_addr_04_GTTXRESET_Mask   cBit1
#define cAf6_upen_addr_04_GTRXRESET_Mask   cBit2

/*------------------------------------------------------------------------------
Reg Name   : Serdes RESET Sticky
Reg Addr   : 0x0_0005
Reg Formula: 0x0_0005 + $hssi_quad_id * 0x800 + $hssi_lane_id * 0x40
    Where  :
           + $hssi_quad_id (00-09) : high speed serial interface
           + $hssi_lane_id (00-03): serdes id of each high speed serial interface}
Reg Desc   :
This register  is used to sticky RESET NOT DONE PER GT serdes

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_addr_05_Base                                                                      0x00005

/*--------------------------------------
BitField Name: LINK_DOWN
BitField Type: R/W
BitField Desc: LINK DOWN 0x0 : LINK_UP 0x1 : LINK_DOWN
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_upen_addr_05_LINK_DOWN_Mask                                                                cBit15
#define cAf6_upen_addr_05_LINK_DOWN_Shift                                                                   15

/*--------------------------------------
BitField Name: GTPLL_NLOCK
BitField Type: R/W
BitField Desc: This field use to sticky reset not lock of pll bit_0: CPLL_NLOCK
bit_1: QPLL0_NLOCK bit_2: QPLL1_NLOCK 0x0: Lock 0x1: Not Lock
BitField Bits: [14:12]
--------------------------------------*/
#define cAf6_upen_addr_05_GTPLL_NLOCK_Mask                                                           cBit14_12
#define cAf6_upen_addr_05_GTPLL_NLOCK_Shift                                                                 12

/*--------------------------------------
BitField Name: GTRXSUBRESET_NDONE
BitField Type: R/W
BitField Desc: This field use to sticky reset not done at rx side bit_0:
RXPMARESET_NDONE bit_1: RXPCSRESET_NDONE bit_2: RXPROGDIVRESET_NDONE bit_3:
RXCDRRESET_NDONE 0x0: reset done 0x1: reset not done
BitField Bits: [11:08]
--------------------------------------*/
#define cAf6_upen_addr_05_GTRXSUBRESET_NDONE_Mask                                                     cBit11_8
#define cAf6_upen_addr_05_GTRXSUBRESET_NDONE_Shift                                                           8

#define cAf6_upen_addr_05_RXPMARESET_NDONE_Mask     cBit8
#define cAf6_upen_addr_05_RXPCSRESET_NDONE_Mask     cBit9
#define cAf6_upen_addr_05_RXPROGDIVRESET_NDONE_Mask cBit10

/*--------------------------------------
BitField Name: GTTXSUBRESET_NDONE
BitField Type: R/W
BitField Desc: This reset is used for special cases and specific subsection TX
resets while the GT transceiver is in normal operation bit_0: TXPMARESET_NDONE
bit_1: TXPCSRESET_NDONE bit_2: TXPROGDIVRESET_NDONE other: Unused
BitField Bits: [07:04]
--------------------------------------*/
#define cAf6_upen_addr_05_GTTXSUBRESET_NDONE_Mask                                                      cBit7_4
#define cAf6_upen_addr_05_GTTXSUBRESET_NDONE_Shift                                                           4

#define cAf6_upen_addr_05_TXPMARESET_NDONE_Mask     cBit4
#define cAf6_upen_addr_05_TXPCSRESET_NDONE_Mask     cBit5
#define cAf6_upen_addr_05_TXPROGDIVRESET_NDONE_Mask cBit6

/*--------------------------------------
BitField Name: GTRESET_REQ_NDONE
BitField Type: R/W
BitField Desc: Reinitialize the GTH transceiver TX and RX bit_0: GTRESET_NDONE
0x0: reset done 0x1: reset not done bit_1: GTTXRESET_NDONE bit_2:
GTRXRESET_NDONE
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_upen_addr_05_GTRESET_REQ_NDONE_Mask                                                       cBit3_0
#define cAf6_upen_addr_05_GTRESET_REQ_NDONE_Shift                                                            0
#define cAf6_upen_addr_05_GTRESET_NDONE_Mask     cBit0
#define cAf6_upen_addr_05_GTTXRESET_NDONE_Mask   cBit1
#define cAf6_upen_addr_05_GTRXRESET_NDONE_Mask   cBit2

/*------------------------------------------------------------------------------
Reg Name   : Serdes RESET Status
Reg Addr   : 0x06
Reg Formula: 0x06 + $hssi_quad_id * 0x800 + $hssi_lane_id * 0x40
    Where  :
           + $hssi_quad_id (00-09) : high speed serial interface
           + $hssi_lane_id (00-03): serdes id of each high speed serial interface}
Reg Desc   :
This register  is used to show current RESET DONE PER GT serdes

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_addr_06_Base                                                                         0x06

/*--------------------------------------
BitField Name: LINK_DOWN
BitField Type: R/W
BitField Desc: LINK DOWN 0x0 : LINK_UP 0x1 : LINK_DOWN
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_upen_addr_06_LINK_DOWN_Mask                                                                cBit15
#define cAf6_upen_addr_06_LINK_DOWN_Shift                                                                   15

/*--------------------------------------
BitField Name: GTPLL_NLOCK
BitField Type: R/W
BitField Desc: PLL NOT LOCK bit_0: CPLL_NLOCK bit_1: QPLL0_NLOCK bit_2:
QPLL1_NLOCK 0x0: Lock 0x1: Not Lock
BitField Bits: [14:12]
--------------------------------------*/
#define cAf6_upen_addr_06_GTPLL_NLOCK_Mask                                                           cBit14_12
#define cAf6_upen_addr_06_GTPLL_NLOCK_Shift                                                                 12

/*--------------------------------------
BitField Name: GTRXSUBRESET_NDONE
BitField Type: R/W
BitField Desc: RX PATH RESET NOT DONE bit_0: RXPMARESET_NDONE bit_1:
RXPCSRESET_NDONE bit_2: RXPROGDIVRESET_NDONE bit_3: RXCDRRESET_NDONE 0x0: reset
done 0x1: reset not done
BitField Bits: [11:08]
--------------------------------------*/
#define cAf6_upen_addr_06_GTRXSUBRESET_NDONE_Mask                                                     cBit11_8
#define cAf6_upen_addr_06_GTRXSUBRESET_NDONE_Shift                                                           8

#define cAf6_upen_addr_06_RXPMARESET_NDONE_Mask     cBit8
#define cAf6_upen_addr_06_RXPCSRESET_NDONE_Mask     cBit9
#define cAf6_upen_addr_06_RXPROGDIVRESET_NDONE_Mask cBit10

/*--------------------------------------
BitField Name: GTTXSUBRESET_NDONE
BitField Type: R/W
BitField Desc: TX PATH RESET NOT DONE bit_0: TXPMARESET_NDONE bit_1:
TXPCSRESET_NDONE bit_2: TXPROGDIVRESET_NDONE other: Unused
BitField Bits: [07:04]
--------------------------------------*/
#define cAf6_upen_addr_06_GTTXSUBRESET_NDONE_Mask                                                      cBit7_4
#define cAf6_upen_addr_06_GTTXSUBRESET_NDONE_Shift                                                           4

#define cAf6_upen_addr_06_TXPMARESET_NDONE_Mask     cBit4
#define cAf6_upen_addr_06_TXPCSRESET_NDONE_Mask     cBit5
#define cAf6_upen_addr_06_TXPROGDIVRESET_NDONE_Mask cBit6

/*--------------------------------------
BitField Name: GTRESET_REQ_NDONE
BitField Type: R/W
BitField Desc: GT RESET NOT DONE bit_0: GTRESET_NDONE 0: reset done 1: reset not
done bit_1: GTTXRESET_NDONE bit_2: GTRXRESET_NDONE
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_upen_addr_06_GTRESET_REQ_NDONE_Mask                                                       cBit3_0
#define cAf6_upen_addr_06_GTRESET_REQ_NDONE_Shift                                                            0

#define cAf6_upen_addr_06_GTRESET_NDONE_Mask     cBit0
#define cAf6_upen_addr_06_GTTXRESET_NDONE_Mask   cBit1
#define cAf6_upen_addr_06_GTRXRESET_NDONE_Mask   cBit2

/*------------------------------------------------------------------------------
Reg Name   : Serdes PMA RESETSEL
Reg Addr   : 0x0_0007
Reg Formula: 0x0_0007 + $hssi_quad_id * 0x800 + $hssi_lane_id * 0x40
    Where  :
           + $hssi_quad_id (00-09) : high speed serial interface
           + $hssi_lane_id (00-03): serdes id of each high speed serial interface}
Reg Desc   :
This register  is used to control PER GT serdes

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_addr_07_Base                                                                      0x00007

/*--------------------------------------
BitField Name: GTRESETSEL
BitField Type: R/W
BitField Desc: Transceiver Reset Mode bit_0: GTRESETSEL (UltraScale FPGAs only)
Reset mode enable port. 0x0: Sequential mode (recommended). 0x1: Single mode
bit_1: GTTXRESETSEL (UltraScale+ FPGAs only), Reset mode enable port for TX
bit_2: GTRXRESETSEL (UltraScale+ FPGAs only), Reset mode enable port for RX
bit_3: Unused
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_upen_addr_07_GTRESETSEL_Mask                                                              cBit3_0
#define cAf6_upen_addr_07_GTRESETSEL_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Serdes PMA TX Driver
Reg Addr   : 0x0_0008
Reg Formula: 0x0_0008 + $hssi_quad_id * 0x800 + $hssi_lane_id * 0x40
    Where  :
           + $hssi_quad_id (00-09) : high speed serial interface
           + $hssi_lane_id (00-03): serdes id of each high speed serial interface}
Reg Desc   :
This register  is used to control PMA

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_addr_08_Base                                                                      0x00008

/*--------------------------------------
BitField Name: TXDIFFCTL
BitField Type: R/W
BitField Desc: Driver Swing Control, value config depend on GTHE3/GTYE3
specified
BitField Bits: [14:10]
--------------------------------------*/
#define cAf6_upen_addr_08_TXDIFFCTL_Mask                                                             cBit14_10
#define cAf6_upen_addr_08_TXDIFFCTL_Shift                                                                   10

/*--------------------------------------
BitField Name: TXPOSCURSORL
BitField Type: R/W
BitField Desc: Transmitter post-cursor TX pre-emphasis control.
BitField Bits: [09:05]
--------------------------------------*/
#define cAf6_upen_addr_08_TXPOSCURSORL_Mask                                                            cBit9_5
#define cAf6_upen_addr_08_TXPOSCURSORL_Shift                                                                 5

/*--------------------------------------
BitField Name: TXPRECURSORL
BitField Type: R/W
BitField Desc: Transmitter pre-cursor TX pre-emphasis control.
BitField Bits: [04:00]
--------------------------------------*/
#define cAf6_upen_addr_08_TXPRECURSORL_Mask                                                            cBit4_0
#define cAf6_upen_addr_08_TXPRECURSORL_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Serdes PMA Clock Selection
Reg Addr   : 0x0_0009
Reg Formula: 0x0_0009 + $hssi_quad_id * 0x800 + $hssi_lane_id * 0x40
    Where  :
           + $hssi_quad_id (00-09) : high speed serial interface
           + $hssi_lane_id (00-03): serdes id of each high speed serial interface}
Reg Desc   :
This register  is used to control RESET PER GT serdes

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_addr_09_Base                                                                      0x00009

/*--------------------------------------
BitField Name: RXPLLCLKSEL
BitField Type: R/W
BitField Desc: Selects the PLL to drive the RX datapath 00: CPLL 10: QPLL1 11:
QPLL0
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_upen_addr_09_RXPLLCLKSEL_Mask                                                           cBit13_12
#define cAf6_upen_addr_09_RXPLLCLKSEL_Shift                                                                 12

/*--------------------------------------
BitField Name: RXPOLARITY
BitField Type: R/W
BitField Desc: The RXPOLARITY port is used to invert the polarity of outgoing
data 0x0: Not inverted. RXP is positive, and RXN is negative 0x1: Inverted. RXP
is negative, and RXN is positive
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_upen_addr_09_RXPOLARITY_Mask                                                               cBit11
#define cAf6_upen_addr_09_RXPOLARITY_Shift                                                                  11

/*--------------------------------------
BitField Name: RXOUTCLKSEL
BitField Type: R/W
BitField Desc: This port controls the input selector and allows these clocks to
be output via the RXOUTCLK port 3'b000: Static 1 3'b001: RXOUTCLKPCS path
3'b010: RXOUTCLKPMA path 3'b011: RXPLLREFCLK_DIV1 path 3'b100: RXPLLREFCLK_DIV2
path 3'b101: RXPROGDIVCLK Others: Reserved
BitField Bits: [10:08]
--------------------------------------*/
#define cAf6_upen_addr_09_RXOUTCLKSEL_Mask                                                            cBit10_8
#define cAf6_upen_addr_09_RXOUTCLKSEL_Shift                                                                  8

/*--------------------------------------
BitField Name: TXPLLCLKSEL
BitField Type: R/W
BitField Desc: Selects the PLL to drive the TX datapath 00: CPLL 10: QPLL1 11:
QPLL0
BitField Bits: [05:04]
--------------------------------------*/
#define cAf6_upen_addr_09_TXPLLCLKSEL_Mask                                                             cBit5_4
#define cAf6_upen_addr_09_TXPLLCLKSEL_Shift                                                                  4

/*--------------------------------------
BitField Name: TXPOLARITY
BitField Type: R/W
BitField Desc: The TXPOLARITY port is used to invert the polarity of outgoing
data 0x0: Not inverted. TXP is positive, and TXN is negative 0x1: Inverted. TXP
is negative, and TXN is positive
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_addr_09_TXPOLARITY_Mask                                                                cBit3
#define cAf6_upen_addr_09_TXPOLARITY_Shift                                                                   3

/*--------------------------------------
BitField Name: TXOUTCLKSEL
BitField Type: R/W
BitField Desc: This port controls the input selector and allows these clocks to
be output via the TXOUTCLK port 3'b000: Static 1 3'b001: TXOUTCLKPCS path
3'b010: TXOUTCLKPMA path 3'b011: TXPLLREFCLK_DIV1 path 3'b100: TXPLLREFCLK_DIV2
path 3'b101: TXPROGDIVCLK Others: Reserved
BitField Bits: [02:00]
--------------------------------------*/
#define cAf6_upen_addr_09_TXOUTCLKSEL_Mask                                                             cBit2_0
#define cAf6_upen_addr_09_TXOUTCLKSEL_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Serdes CPLL Control
Reg Addr   : 0x0_000a
Reg Formula: 0x0_000a + $hssi_quad_id * 0x800 + $hssi_lane_id * 0x40
    Where  :
           + $hssi_quad_id (00-09) : high speed serial interface
           + $hssi_lane_id (00-03): serdes id of each high speed serial interface}
Reg Desc   :
This register  is used to control CPLL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_addr_0a_Base                                                                      0x0000a

/*--------------------------------------
BitField Name: CPLL0PD
BitField Type: R/W
BitField Desc: CPLL0 Power down
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_addr_0a_CPLL0PD_Mask                                                                   cBit5
#define cAf6_upen_addr_0a_CPLL0PD_Shift                                                                      5

/*--------------------------------------
BitField Name: CPLL0RESET
BitField Type: R/W
BitField Desc: CPLL0 RESET
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_addr_0a_CPLL0RESET_Mask                                                                cBit4
#define cAf6_upen_addr_0a_CPLL0RESET_Shift                                                                   4

/*--------------------------------------
BitField Name: CPLL0REFSEL
BitField Type: R/W
BitField Desc: CPLL0 REFCLOCK selection 000: Reserved 001: GTREFCLK00 selected
010: GTREFCLK10 selected 011: GTNORTHREFCLK00 selected 100: GTNORTHREFCLK10
selected 101: GTSOUTHREFCLK00 selected 110: GTSOUTHREFCLK10 selected 111:
GTGREFCLK0 selected
BitField Bits: [02:00]
--------------------------------------*/
#define cAf6_upen_addr_0a_CPLL0REFSEL_Mask                                                             cBit2_0
#define cAf6_upen_addr_0a_CPLL0REFSEL_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Serdes QPLL Control
Reg Addr   : 0x0_000b
Reg Formula: 0x0_000b + $hssi_quad_id * 0x800
    Where  :
           + $hssi_quad_id (00-09) : high speed serial interface
Reg Desc   :
This register  is used to control QPLL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_addr_0b_Base                                                                      0x0000b

/*--------------------------------------
BitField Name: QPLL0PD
BitField Type: R/W
BitField Desc: QPLL0 Power down
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_upen_addr_0b_QPLL1PD_Mask                                                                  cBit13
#define cAf6_upen_addr_0b_QPLL1PD_Shift                                                                     13

/*--------------------------------------
BitField Name: QPLL0RESET
BitField Type: R/W
BitField Desc: QPLL0 RESET
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_upen_addr_0b_QPLL1RESET_Mask                                                               cBit12
#define cAf6_upen_addr_0b_QPLL1RESET_Shift                                                                  12

/*--------------------------------------
BitField Name: QPLL0REFSEL
BitField Type:
BitField Desc: QPLL0 REFCLOCK selection
BitField Bits: [10:08]
--------------------------------------*/
#define cAf6_upen_addr_0b_QPLL1REFSEL_Mask                                                            cBit10_8
#define cAf6_upen_addr_0b_QPLL1REFSEL_Shift                                                                  8

/*--------------------------------------
BitField Name: QPLL0PD
BitField Type: R/W
BitField Desc: QPLL0 Power down
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_addr_0b_QPLL0PD_Mask                                                                   cBit5
#define cAf6_upen_addr_0b_QPLL0PD_Shift                                                                      5

/*--------------------------------------
BitField Name: QPLL0RESET
BitField Type: R/W
BitField Desc: QPLL0 RESET
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_addr_0b_QPLL0RESET_Mask                                                                cBit4
#define cAf6_upen_addr_0b_QPLL0RESET_Shift                                                                   4

/*--------------------------------------
BitField Name: QPLL0REFSEL
BitField Type: R/W
BitField Desc: QPLL0 REFCLOCK selection 000: Reserved 001: GTREFCLK00 selected
010: GTREFCLK10 selected 011: GTNORTHREFCLK00 selected 100: GTNORTHREFCLK10
selected 101: GTSOUTHREFCLK00 selected 110: GTSOUTHREFCLK10 selected 111:
GTGREFCLK0 selected
BitField Bits: [02:00]
--------------------------------------*/
#define cAf6_upen_addr_0b_QPLL0REFSEL_Mask                                                             cBit2_0
#define cAf6_upen_addr_0b_QPLL0REFSEL_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Serdes PMA Power down
Reg Addr   : 0x0_000f
Reg Formula: 0x0_000f + $hssi_quad_id * 0x800 + $hssi_lane_id * 0x40
    Where  :
           + $hssi_quad_id (00-09) : high speed serial interface
           + $hssi_lane_id (00-03): serdes id of each high speed serial interface}
Reg Desc   :
This register  is used to control power down serdes

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_addr_0f_Base                                                                      0x0000f

/*--------------------------------------
BitField Name: GT_TXPDELECIDLEMODE
BitField Type: R/W
BitField Desc: Determines if TXELECIDLE and TXPOWERDOWN should be treated as
synchronous or asynchronous signals. 1: Asynchronous 0: Synchronous
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_upen_addr_0f_GT_TXPDELECIDLEMODE_Mask                                                       cBit7
#define cAf6_upen_addr_0f_GT_TXPDELECIDLEMODE_Shift                                                          7

/*--------------------------------------
BitField Name: GT_TXELECIDLE
BitField Type: R/W
BitField Desc: When High, this signal forces MGTYTXP and MGTYTXN both to Common
mode, creating an electrical idle signal
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_upen_addr_0f_GT_TXELECIDLE_Mask                                                             cBit6
#define cAf6_upen_addr_0f_GT_TXELECIDLE_Shift                                                                6

/*--------------------------------------
BitField Name: GT_TXPD
BitField Type: R/W
BitField Desc: Powers down the TX lane
BitField Bits: [03:02]
--------------------------------------*/
#define cAf6_upen_addr_0f_GT_TXPD_Mask                                                                 cBit3_2
#define cAf6_upen_addr_0f_GT_TXPD_Shift                                                                      2

/*--------------------------------------
BitField Name: GT_RXPD
BitField Type: R/W
BitField Desc: Powers down the RX lane
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_upen_addr_0f_GT_RXPD_Mask                                                                 cBit1_0
#define cAf6_upen_addr_0f_GT_RXPD_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Serdes PMA PCSRSVDIN
Reg Addr   : 0x0_000e
Reg Formula: 0x0_000e + $hssi_quad_id * 0x800 + $hssi_lane_id * 0x40
    Where  :
           + $hssi_quad_id (00-09) : high speed serial interface
           + $hssi_lane_id (00-03): serdes id of each high speed serial interface}
Reg Desc   :
This register  is used to control power down serdes
UltraScale FPGAs only:
DRP reset. Reading read-only registers while the
XCLK is not toggling (e.g., during reset or change of
reference clocks) causes the DRP to not return a
DRPRDY signal and prevent further DRP
transactions. In such an event, PCSRSVDIN[2] must
be pulsed to reset the DRP interface before
initiating further DRP transactions.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_addr_0e_Base                                                                      0x0000e

/*--------------------------------------
BitField Name: PCSRSVDIN
BitField Type: R/W
BitField Desc: PCSRSVDIN
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_addr_0e_PCSRSVDIN_Mask                                                              cBit15_0
#define cAf6_upen_addr_0e_PCSRSVDIN_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Serdes PMA Power down
Reg Addr   : 0x0_000f
Reg Formula: 0x0_000f + $hssi_quad_id * 0x800 + $hssi_lane_id * 0x40
    Where  :
           + $hssi_quad_id (00-09) : high speed serial interface
           + $hssi_lane_id (00-03): serdes id of each high speed serial interface}
Reg Desc   :
This register  is used to control power down serdes

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_addr_0f_Base                                                                      0x0000f

/*--------------------------------------
BitField Name: GT_TXPDELECIDLEMODE
BitField Type: R/W
BitField Desc: Determines if TXELECIDLE and TXPOWERDOWN should be treated as
synchronous or asynchronous signals. 1: Asynchronous 0: Synchronous
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_upen_addr_0f_GT_TXPDELECIDLEMODE_Mask                                                       cBit7
#define cAf6_upen_addr_0f_GT_TXPDELECIDLEMODE_Shift                                                          7

/*--------------------------------------
BitField Name: GT_TXELECIDLE
BitField Type: R/W
BitField Desc: When High, this signal forces MGTYTXP and MGTYTXN both to Common
mode, creating an electrical idle signal
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_upen_addr_0f_GT_TXELECIDLE_Mask                                                             cBit6
#define cAf6_upen_addr_0f_GT_TXELECIDLE_Shift                                                                6

/*--------------------------------------
BitField Name: GT_TXPD
BitField Type: R/W
BitField Desc: Powers down the TX lane
BitField Bits: [03:02]
--------------------------------------*/
#define cAf6_upen_addr_0f_GT_TXPD_Mask                                                                 cBit3_2
#define cAf6_upen_addr_0f_GT_TXPD_Shift                                                                      2

/*--------------------------------------
BitField Name: GT_RXPD
BitField Type: R/W
BitField Desc: Powers down the RX lane
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_upen_addr_0f_GT_RXPD_Mask                                                                 cBit1_0
#define cAf6_upen_addr_0f_GT_RXPD_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Serdes PCS Config_Vector
Reg Addr   : 0x0_0010
Reg Formula: 0x0_0010 + $hssi_quad_id * 0x800 + $hssi_lane_id * 0x40 + $hssi_chan_id
    Where  :
           + $hssi_quad_id (00-09) : high speed serial interface
           + $hssi_lane_id (00-03): serdes lane id of each high speed serial interface
           + $hssi_chan_id (00-03): serdes chan id of each high speed serial interface in case QSGMII }
Reg Desc   :
This register  is used to control power down serdes

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_addr_10_Base                                                                      0x00010

/*--------------------------------------
BitField Name: CONFIG_VECTOR
BitField Type: R/W
BitField Desc: configuration_vector, depend RXAUI/XAUI/SGMII/QSGMII ... PCS
function
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_addr_10_CONFIG_VECTOR_Mask                                                          cBit15_0
#define cAf6_upen_addr_10_CONFIG_VECTOR_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : Serdes PCS Status_Vector
Reg Addr   : 0x0_0011
Reg Formula: 0x0_0011 + $hssi_quad_id * 0x800 + $hssi_lane_id * 0x40 + $hssi_chan_id
    Where  :
           + $hssi_quad_id (00-09) : high speed serial interface
           + $hssi_lane_id (00-03): serdes lane id of each high speed serial interface
           + $hssi_chan_id (00-03): serdes chan id of each high speed serial interface in case QSGMII }
Reg Desc   :
This register  is used to control power down serdes

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_addr_11_Base                                                                      0x00011

/*--------------------------------------
BitField Name: STATUS_VECTOR
BitField Type: R/W
BitField Desc: status_vector, depend RXAUI/XAUI/SGMII/QSGMII ... PCS function
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_addr_11_STATUS_VECTOR_Mask                                                          cBit15_0
#define cAf6_upen_addr_11_STATUS_VECTOR_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : Serdes PCS An_Config_Vector
Reg Addr   : 0x0_0012
Reg Formula: 0x0_0012 + $hssi_quad_id * 0x800 + $hssi_lane_id * 0x40 + $hssi_chan_id
    Where  :
           + $hssi_quad_id (00-09) : high speed serial interface
           + $hssi_lane_id (00-03): serdes lane id of each high speed serial interface
           + $hssi_chan_id (00-03): serdes chan id of each high speed serial interface in case QSGMII }
Reg Desc   :
This register  is used to control power down serdes

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_addr_12_Base                                                                      0x00012

/*--------------------------------------
BitField Name: AN_CONFIG_VECTOR
BitField Type: R/W
BitField Desc: An_Configuration_vector, depend RXAUI/XAUI/SGMII/QSGMII ... PCS
function
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_addr_12_AN_CONFIG_VECTOR_Mask                                                       cBit15_0
#define cAf6_upen_addr_12_AN_CONFIG_VECTOR_Shift                                                             0

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061SERDESCONTROLV2REG_H_ */


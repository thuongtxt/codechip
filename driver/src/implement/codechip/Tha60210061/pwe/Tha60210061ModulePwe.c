/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PWE
 *
 * File        : Tha60210061ModulePwe.c
 *
 * Created Date: Aug 22, 2016
 *
 * Description : Module PWE of 60210061
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210012/pwe/Tha60210012ModulePweInternal.h"
#include "../cdr/Tha60210061ModuleCdr.h"
#include "../encap/Tha60210061ModuleEncap.h"
#include "Tha60210061ModulePlaReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061ModulePwe * Tha60210061ModulePwe;
typedef struct tTha60210061ModulePwe
    {
    tTha60210012ModulePwe super;
    }tTha60210061ModulePwe;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

static tThaModulePweMethods             m_ThaModulePweOverride;
static tTha60210011ModulePweMethods     m_Tha60210011ModulePweOverride;
static tTha60210012ModulePweMethods     m_Tha60210012ModulePweOverride;

/* Save super implementation */
static const tThaModulePweMethods       *m_ThaModulePweMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 PwLookupIdMask(Tha60210012ModulePwe self)
    {
    AtUnused(self);
    return cAf6_pla_lo_lk_ctrl_PlaLoLkPWIDCtrl_Mask;
    }

static uint32 PwLookupIdShift(Tha60210012ModulePwe self)
    {
    AtUnused(self);
    return cAf6_pla_lo_lk_ctrl_PlaLoLkPWIDCtrl_Shift;
    }

static uint32 HspwGroupMask(Tha60210012ModulePwe self)
    {
    AtUnused(self);
    return cAf6_pla_prot_flow_ctrl_PlaOutHspwGrpCtr_Mask;
    }

static uint32 HspwGroupShift(Tha60210012ModulePwe self)
    {
    AtUnused(self);
    return cAf6_pla_prot_flow_ctrl_PlaOutHspwGrpCtr_Shift;
    }

static uint32 UpsrGroupMask(Tha60210012ModulePwe self)
    {
    AtUnused(self);
    return cAf6_pla_prot_flow_ctrl_PlaOutUpsrGrpCtr_Mask;
    }

static uint32 UpsrGroupShift(Tha60210012ModulePwe self)
    {
    AtUnused(self);
    return cAf6_pla_prot_flow_ctrl_PlaOutUpsrGrpCtr_Shift;
    }

static uint8 NumSlice48(Tha60210012ModulePwe self)
    {
    AtUnused(self);
    return 1;
    }

static eBool PtchEnableOnModuleIsSupported(Tha60210012ModulePwe self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 StartVersionSupportLanFcsRemove(Tha60210012ModulePwe self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x4, 0x1096);
    }

static uint32 VcgFlowLookUpOffset(Tha60210012ModulePwe self)
    {
    AtUnused(self);
    return 1536UL;
    }

static uint32 NumLoBlocks(Tha60210012ModulePwe self)
    {
    AtUnused(self);
    return 2048UL;
    }

static uint32 IMsgLinkAddressOffset(Tha60210012ModulePwe self)
    {
    AtUnused(self);
    return 1536UL;
    }

static uint32 VcgAddressOffset(Tha60210012ModulePwe self)
    {
    AtUnused(self);
    return 2560UL;
    }

static uint32 StartVersionControlJitterAttenuator(Tha60210011ModulePwe self)
    {
    ThaModuleCdr moduleCdr = (ThaModuleCdr)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModuleCdr);
    return Tha60210061ModuleCdrStartVersionShouldEnableJA(moduleCdr);
    }

static uint32 StartVersionSupportInsertEXaui(Tha60210012ModulePwe self)
    {
    AtUnused(self);
    /* Exaui is not support on this product */
    return cInvalidUint32;
    }

static uint32 MaxWriteCaches(Tha60210012ModulePwe self)
    {
    AtUnused(self);
    return 0x1000;
    }

static eAtRet PwEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    uint32 regAddr, regVal;
    eAtRet ret;

    /* Implement super firstly */
    ret = m_ThaModulePweMethods->PwEnable(self, pw, enable);
    if (ret != cAtOk)
        return ret;

    if (!ThaModulePweUtilPwCircuitIsLiuDe1(pw))
        return cAtOk;

    /* As hardware recommend, this code only apply for PWs bind with DS1 LIU
     * that need improve timing for those PWs */
    regAddr = cAf6Reg_pla_comm_pw_ctrl_Base + AtChannelIdGet((AtChannel)pw) + Tha60210011ModulePlaBaseAddress();
    regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cAf6_pla_comm_pw_ctrl_PlaLoPwQueueCtrl_, enable ? 1 : 0);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eAtRet PwJitterAttenuatorEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    eBool shouldEnable = enable;

    /* HW recommends only enable Tx Shaper for DE1 over STM (async paths) */
    if (!ThaModulePweUtilPwCircuitIsDe1OverStm(pw))
        shouldEnable = cAtFalse;

    return m_ThaModulePweMethods->PwJitterAttenuatorEnable(self, pw, shouldEnable);
    }

static void OverrideTha60210011ModulePwe(AtModule self)
    {
    Tha60210011ModulePwe modulePwe = (Tha60210011ModulePwe)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModulePweOverride, mMethodsGet(modulePwe), sizeof(m_Tha60210011ModulePweOverride));

        mMethodOverride(m_Tha60210011ModulePweOverride, StartVersionControlJitterAttenuator);
        }

    mMethodsSet(modulePwe, &m_Tha60210011ModulePweOverride);
    }

static void OverrideTha60210012ModulePwe(AtModule self)
    {
    Tha60210012ModulePwe modulePwe = (Tha60210012ModulePwe)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210012ModulePweOverride, mMethodsGet(modulePwe), sizeof(m_Tha60210012ModulePweOverride));
        mMethodOverride(m_Tha60210012ModulePweOverride, PwLookupIdMask);
        mMethodOverride(m_Tha60210012ModulePweOverride, PwLookupIdShift);
        mMethodOverride(m_Tha60210012ModulePweOverride, HspwGroupMask);
        mMethodOverride(m_Tha60210012ModulePweOverride, HspwGroupShift);
        mMethodOverride(m_Tha60210012ModulePweOverride, UpsrGroupMask);
        mMethodOverride(m_Tha60210012ModulePweOverride, UpsrGroupShift);
        mMethodOverride(m_Tha60210012ModulePweOverride, NumSlice48);
        mMethodOverride(m_Tha60210012ModulePweOverride, PtchEnableOnModuleIsSupported);
        mMethodOverride(m_Tha60210012ModulePweOverride, StartVersionSupportLanFcsRemove);
        mMethodOverride(m_Tha60210012ModulePweOverride, StartVersionSupportInsertEXaui);
        mMethodOverride(m_Tha60210012ModulePweOverride, VcgFlowLookUpOffset);
        mMethodOverride(m_Tha60210012ModulePweOverride, NumLoBlocks);
        mMethodOverride(m_Tha60210012ModulePweOverride, IMsgLinkAddressOffset);
        mMethodOverride(m_Tha60210012ModulePweOverride, VcgAddressOffset);
        mMethodOverride(m_Tha60210012ModulePweOverride, MaxWriteCaches);
        }

    mMethodsSet(modulePwe, &m_Tha60210012ModulePweOverride);
    }

static void OverrideThaModulePwe(AtModule self)
    {
    ThaModulePwe modulePwe = (ThaModulePwe)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePweMethods = mMethodsGet(modulePwe);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePweOverride, mMethodsGet(modulePwe), sizeof(m_ThaModulePweOverride));

        mMethodOverride(m_ThaModulePweOverride, PwEnable);
        mMethodOverride(m_ThaModulePweOverride, PwJitterAttenuatorEnable);
        }

    mMethodsSet(modulePwe, &m_ThaModulePweOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModulePwe(self);
    OverrideTha60210011ModulePwe(self);
    OverrideTha60210012ModulePwe(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012ModulePwe);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012ModulePweObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210061ModulePweNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

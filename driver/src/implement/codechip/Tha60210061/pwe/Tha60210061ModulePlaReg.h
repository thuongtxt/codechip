/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PWE
 * 
 * File        : Tha60210061ModulePlaReg.h
 * 
 * Created Date: Aug 22, 2016
 *
 * Description : Module PWE register of 60210061
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061MODULEPLAREG_H_
#define _THA60210061MODULEPLAREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Low-Order Payload Control
Reg Addr   : 0x0_1000-0x0_33FF
Reg Formula: 0x0_1000 + $LoOc24Slice*8192 + $LoPwid
    Where  :
           + $LoOc24Slice(0-1): LO OC-24 slices, there are total 2xOC-24 slices per LO OC-48 slice
           + $LoPwid(0-1023): pseudowire channel for each LO OC-24 slice
Reg Desc   :
This register is used to configure payload in each LO Pseudowire channels

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_lo_pld_ctrl_Base                                                                   0x01000
#define cAf6Reg_pla_lo_pld_ctrl(LoOc24Slice, LoPwid)                     (0x01000+(LoOc24Slice)*8192+(LoPwid))
#define cAf6Reg_pla_lo_pld_ctrl_WidthVal                                                                    32
#define cAf6Reg_pla_lo_pld_ctrl_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: PlaLoPldTypeCtrl
BitField Type: RW
BitField Desc: Payload Type 0: satop 1: ces without cas 2: cep
BitField Bits: [16:15]
--------------------------------------*/
#define cAf6_pla_lo_pld_ctrl_PlaLoPldTypeCtrl_Bit_Start                                                     15
#define cAf6_pla_lo_pld_ctrl_PlaLoPldTypeCtrl_Bit_End                                                       16
#define cAf6_pla_lo_pld_ctrl_PlaLoPldTypeCtrl_Mask                                                   cBit16_15
#define cAf6_pla_lo_pld_ctrl_PlaLoPldTypeCtrl_Shift                                                         15
#define cAf6_pla_lo_pld_ctrl_PlaLoPldTypeCtrl_MaxVal                                                       0x3
#define cAf6_pla_lo_pld_ctrl_PlaLoPldTypeCtrl_MinVal                                                       0x0
#define cAf6_pla_lo_pld_ctrl_PlaLoPldTypeCtrl_RstVal                                                       0x0

/*--------------------------------------
BitField Name: PlaLoPldSizeCtrl
BitField Type: RW
BitField Desc: Payload Size satop/cep mode: bit[13:0] payload size (ex: value as
0x100 is payload size 256 bytes) ces mode: bit[5:0] number of DS0 timeslot
bit[14:6] number of NxDS0 frame
BitField Bits: [14:0]
--------------------------------------*/
#define cAf6_pla_lo_pld_ctrl_PlaLoPldSizeCtrl_Bit_Start                                                      0
#define cAf6_pla_lo_pld_ctrl_PlaLoPldSizeCtrl_Bit_End                                                       14
#define cAf6_pla_lo_pld_ctrl_PlaLoPldSizeCtrl_Mask                                                    cBit14_0
#define cAf6_pla_lo_pld_ctrl_PlaLoPldSizeCtrl_Shift                                                          0
#define cAf6_pla_lo_pld_ctrl_PlaLoPldSizeCtrl_MaxVal                                                    0x7fff
#define cAf6_pla_lo_pld_ctrl_PlaLoPldSizeCtrl_MinVal                                                       0x0
#define cAf6_pla_lo_pld_ctrl_PlaLoPldSizeCtrl_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Low-Order Add/Remove Pseudowire Protocol Control
Reg Addr   : 0x0_1800-0x0_3BFF
Reg Formula: 0x0_1800 + $LoOc24Slice*8192 + $LoPwid
    Where  :
           + $LoOc24Slice(0-1): LO OC-24 slices, there are total 2xOC-24 slices per LO OC-48 slice
           + $LoPwid(0-1023): pseudowire channel for each LO OC-24 slice
Reg Desc   :
This register is used to add/remove pseudowire to prevent burst packets to other packet processing engines in each LO OC-24 slice

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_lo_add_rmv_pw_ctrl_Base                                                            0x01800
#define cAf6Reg_pla_lo_add_rmv_pw_ctrl(LoOc24Slice, LoPwid)              (0x01800+(LoOc24Slice)*8192+(LoPwid))
#define cAf6Reg_pla_lo_add_rmv_pw_ctrl_WidthVal                                                             32
#define cAf6Reg_pla_lo_add_rmv_pw_ctrl_WriteMask                                                           0x0

/*--------------------------------------
BitField Name: PlaLoStaAddRmvCtrl
BitField Type: RW
BitField Desc: protocol state to add/remove pw Step1: Add intitial or pw idle,
the protocol state value is "zero" Step2: For adding the pw, CPU will Write "1"
value for the protocol state to start adding pw. The protocol state value is
"1". Step3: CPU enables pw at demap to finish the adding pw process. HW will
automatically change the protocol state value to "2" to run the pw, and keep
this state. Step4: For removing the pw, CPU will write "3" value for the
protocol state to start removing pw. The protocol state value is "3". Step5:
Poll the protocol state until return value "0" value, after that CPU disables pw
at demap to finish the removing pw process
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_pla_lo_add_rmv_pw_ctrl_PlaLoStaAddRmvCtrl_Bit_Start                                             0
#define cAf6_pla_lo_add_rmv_pw_ctrl_PlaLoStaAddRmvCtrl_Bit_End                                               1
#define cAf6_pla_lo_add_rmv_pw_ctrl_PlaLoStaAddRmvCtrl_Mask                                            cBit1_0
#define cAf6_pla_lo_add_rmv_pw_ctrl_PlaLoStaAddRmvCtrl_Shift                                                 0
#define cAf6_pla_lo_add_rmv_pw_ctrl_PlaLoStaAddRmvCtrl_MaxVal                                              0x3
#define cAf6_pla_lo_add_rmv_pw_ctrl_PlaLoStaAddRmvCtrl_MinVal                                              0x0
#define cAf6_pla_lo_add_rmv_pw_ctrl_PlaLoStaAddRmvCtrl_RstVal                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Low-Order Lookup Control
Reg Addr   : 0x0_4000-0x0_47FF
Reg Formula: 0x0_4000 + $LoOc24Slice*1024 + $LoPwid
    Where  :
           + $LoOc24Slice(0-1): LO OC-24 slices, there are total 2xOC-24 slices per LO OC-48 slice
           + $LoPwid(0-1023): pseudowire channel for each LO OC-24 slice
Reg Desc   :
This register is used to lookup pseudowire per OC-24 slice to a common pseudowire number for both HO & LO path (total is 1536 psedowires)

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_lo_lk_ctrl_Base                                                                    0x04000
#define cAf6Reg_pla_lo_lk_ctrl(LoOc24Slice, LoPwid)                      (0x04000+(LoOc24Slice)*1024+(LoPwid))
#define cAf6Reg_pla_lo_lk_ctrl_WidthVal                                                                     32
#define cAf6Reg_pla_lo_lk_ctrl_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: PlaLoLkPWIDCtrl
BitField Type: RW
BitField Desc: Lookup to a common psedowire
BitField Bits: [10:0]
--------------------------------------*/
#define cAf6_pla_lo_lk_ctrl_PlaLoLkPWIDCtrl_Bit_Start                                                        0
#define cAf6_pla_lo_lk_ctrl_PlaLoLkPWIDCtrl_Bit_End                                                         10
#define cAf6_pla_lo_lk_ctrl_PlaLoLkPWIDCtrl_Mask                                                      cBit10_0
#define cAf6_pla_lo_lk_ctrl_PlaLoLkPWIDCtrl_Shift                                                            0
#define cAf6_pla_lo_lk_ctrl_PlaLoLkPWIDCtrl_MaxVal                                                       0x7ff
#define cAf6_pla_lo_lk_ctrl_PlaLoLkPWIDCtrl_MinVal                                                         0x0
#define cAf6_pla_lo_lk_ctrl_PlaLoLkPWIDCtrl_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler High-Order Payload Control
Reg Addr   : 0x1_0080-0x1_00AF
Reg Formula: 0x1_0080 + $HoPwid
    Where  :
           + $HoPwid(0-47): pseudowire channel for each HO OC-48 slice
Reg Desc   :
This register is used to configure payload in each HO Pseudowire channels

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_ho_pldd_ctrl_Base                                                                  0x10080
#define cAf6Reg_pla_ho_pldd_ctrl(HoPwid)                                                    (0x10080+(HoPwid))
#define cAf6Reg_pla_ho_pldd_ctrl_WidthVal                                                                   32
#define cAf6Reg_pla_ho_pldd_ctrl_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: PlaLoPldSizeCtrl
BitField Type: RW
BitField Desc: Payload Size
BitField Bits: [13:0]
--------------------------------------*/
#define cAf6_pla_ho_pldd_ctrl_PlaLoPldSizeCtrl_Bit_Start                                                     0
#define cAf6_pla_ho_pldd_ctrl_PlaLoPldSizeCtrl_Bit_End                                                      13
#define cAf6_pla_ho_pldd_ctrl_PlaLoPldSizeCtrl_Mask                                                   cBit13_0
#define cAf6_pla_ho_pldd_ctrl_PlaLoPldSizeCtrl_Shift                                                         0
#define cAf6_pla_ho_pldd_ctrl_PlaLoPldSizeCtrl_MaxVal                                                   0x3fff
#define cAf6_pla_ho_pldd_ctrl_PlaLoPldSizeCtrl_MinVal                                                      0x0
#define cAf6_pla_ho_pldd_ctrl_PlaLoPldSizeCtrl_RstVal                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Hig-Order Add/Remove Pseudowire Protocol Control
Reg Addr   : 0x1_00C0-0x1_00EF
Reg Formula: 0x1_00C0 + $HoPwid
    Where  :
           + $HoPwid(0-47): pseudowire channel for each HO OC-48 slice
Reg Desc   :
This register is used to add/remove pseudowire to prevent burst packets to other packet processing engines in each HO OC-48 slice

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_ho_add_rmv_pw_ctrl_Base                                                            0x100C0
#define cAf6Reg_pla_ho_add_rmv_pw_ctrl(HoPwid)                                              (0x100C0+(HoPwid))
#define cAf6Reg_pla_ho_add_rmv_pw_ctrl_WidthVal                                                             32
#define cAf6Reg_pla_ho_add_rmv_pw_ctrl_WriteMask                                                           0x0

/*--------------------------------------
BitField Name: PlaLoStaAddRmvCtrl
BitField Type: RW
BitField Desc: protocol state to add/remove pw Step1: Add intitial or pw idle,
the protocol state value is "zero" Step2: For adding the pw, CPU will Write "1"
value for the protocol state to start adding pw. The protocol state value is
"1". Step3: CPU enables pw at demap to finish the adding pw process. HW will
automatically change the protocol state value to "2" to run the pw, and keep
this state. Step4: For removing the pw, CPU will write "3" value for the
protocol state to start removing pw. The protocol state value is "3". Step5:
Poll the protocol state until return value "0" value, after that CPU disables pw
at demap to finish the removing pw process
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_pla_ho_add_rmv_pw_ctrl_PlaLoStaAddRmvCtrl_Bit_Start                                             0
#define cAf6_pla_ho_add_rmv_pw_ctrl_PlaLoStaAddRmvCtrl_Bit_End                                               1
#define cAf6_pla_ho_add_rmv_pw_ctrl_PlaLoStaAddRmvCtrl_Mask                                            cBit1_0
#define cAf6_pla_ho_add_rmv_pw_ctrl_PlaLoStaAddRmvCtrl_Shift                                                 0
#define cAf6_pla_ho_add_rmv_pw_ctrl_PlaLoStaAddRmvCtrl_MaxVal                                              0x3
#define cAf6_pla_ho_add_rmv_pw_ctrl_PlaLoStaAddRmvCtrl_MinVal                                              0x0
#define cAf6_pla_ho_add_rmv_pw_ctrl_PlaLoStaAddRmvCtrl_RstVal                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler High-Order Payload Control
Reg Addr   : 0x1_0200-0x1_022F
Reg Formula: 0x1_0200 + $HoPwid
    Where  :
           + $HoPwid(0-47): pseudowire channel for each HO OC-48 slice
Reg Desc   :
This register is used to lookup pseudowire per OC-484 slice to a common pseudowire number for both HO & LO path (total is 1536 psedowires)

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_ho_lk_ctrl_Base                                                                    0x10200
#define cAf6Reg_pla_ho_lk_ctrl(HoPwid)                                                      (0x10200+(HoPwid))
#define cAf6Reg_pla_ho_lk_ctrl_WidthVal                                                                     32
#define cAf6Reg_pla_ho_lk_ctrl_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: PlaHoLkPWIDCtrl
BitField Type: RW
BitField Desc: Lookup to a common psedowire
BitField Bits: [10:0]
--------------------------------------*/
#define cAf6_pla_ho_lk_ctrl_PlaHoLkPWIDCtrl_Bit_Start                                                        0
#define cAf6_pla_ho_lk_ctrl_PlaHoLkPWIDCtrl_Bit_End                                                         10
#define cAf6_pla_ho_lk_ctrl_PlaHoLkPWIDCtrl_Mask                                                      cBit10_0
#define cAf6_pla_ho_lk_ctrl_PlaHoLkPWIDCtrl_Shift                                                            0
#define cAf6_pla_ho_lk_ctrl_PlaHoLkPWIDCtrl_MaxVal                                                       0x7ff
#define cAf6_pla_ho_lk_ctrl_PlaHoLkPWIDCtrl_MinVal                                                         0x0
#define cAf6_pla_ho_lk_ctrl_PlaHoLkPWIDCtrl_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Common Pseudowire Control
Reg Addr   : 0x1_8000-0x1_85FF
Reg Formula: 0x1_8000 + $pwid
    Where  :
           + $pwid(0-1535): pseudowire channel
Reg Desc   :
This register is used to config for the common psedowire channels

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_comm_pw_ctrl_Base                                                                  0x18000
#define cAf6Reg_pla_comm_pw_ctrl(pwid)                                                        (0x18000+(pwid))
#define cAf6Reg_pla_comm_pw_ctrl_WidthVal                                                                   32
#define cAf6Reg_pla_comm_pw_ctrl_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: PlaLoPwQueueCtrl
BitField Type: RW
BitField Desc: Queue control, used to config DS1 LIU into high queue 1: high
queue 0: low queue
BitField Bits: [9]
--------------------------------------*/
#define cAf6_pla_comm_pw_ctrl_PlaLoPwQueueCtrl_Mask                                                      cBit9
#define cAf6_pla_comm_pw_ctrl_PlaLoPwQueueCtrl_Shift                                                         9

/*--------------------------------------
BitField Name: PlaLoPwSuprCtrl
BitField Type: RW
BitField Desc: Suppresion Enable 1: enable 0: disable
BitField Bits: [8]
--------------------------------------*/
#define cAf6_pla_comm_pw_ctrl_PlaLoPwSuprCtrl_Bit_Start                                                      8
#define cAf6_pla_comm_pw_ctrl_PlaLoPwSuprCtrl_Bit_End                                                        8
#define cAf6_pla_comm_pw_ctrl_PlaLoPwSuprCtrl_Mask                                                       cBit8
#define cAf6_pla_comm_pw_ctrl_PlaLoPwSuprCtrl_Shift                                                          8
#define cAf6_pla_comm_pw_ctrl_PlaLoPwSuprCtrl_MaxVal                                                       0x1
#define cAf6_pla_comm_pw_ctrl_PlaLoPwSuprCtrl_MinVal                                                       0x0
#define cAf6_pla_comm_pw_ctrl_PlaLoPwSuprCtrl_RstVal                                                       0x0

/*--------------------------------------
BitField Name: PlaPwRbitDisCtrl
BitField Type: RW
BitField Desc: R bit disable 1: disable R bit in the Control Word (assign zero
in the packet) 0: normal
BitField Bits: [7]
--------------------------------------*/
#define cAf6_pla_comm_pw_ctrl_PlaPwRbitDisCtrl_Bit_Start                                                     7
#define cAf6_pla_comm_pw_ctrl_PlaPwRbitDisCtrl_Bit_End                                                       7
#define cAf6_pla_comm_pw_ctrl_PlaPwRbitDisCtrl_Mask                                                      cBit7
#define cAf6_pla_comm_pw_ctrl_PlaPwRbitDisCtrl_Shift                                                         7
#define cAf6_pla_comm_pw_ctrl_PlaPwRbitDisCtrl_MaxVal                                                      0x1
#define cAf6_pla_comm_pw_ctrl_PlaPwRbitDisCtrl_MinVal                                                      0x0
#define cAf6_pla_comm_pw_ctrl_PlaPwRbitDisCtrl_RstVal                                                      0x0

/*--------------------------------------
BitField Name: PlaPwMbitDisCtrl
BitField Type: RW
BitField Desc: M or NP bits disable 1: disable M or NP bits in the Control Word
(assign zero in the packet) 0: normal
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pla_comm_pw_ctrl_PlaPwMbitDisCtrl_Bit_Start                                                     6
#define cAf6_pla_comm_pw_ctrl_PlaPwMbitDisCtrl_Bit_End                                                       6
#define cAf6_pla_comm_pw_ctrl_PlaPwMbitDisCtrl_Mask                                                      cBit6
#define cAf6_pla_comm_pw_ctrl_PlaPwMbitDisCtrl_Shift                                                         6
#define cAf6_pla_comm_pw_ctrl_PlaPwMbitDisCtrl_MaxVal                                                      0x1
#define cAf6_pla_comm_pw_ctrl_PlaPwMbitDisCtrl_MinVal                                                      0x0
#define cAf6_pla_comm_pw_ctrl_PlaPwMbitDisCtrl_RstVal                                                      0x0

/*--------------------------------------
BitField Name: PlaPwLbitDisCtrl
BitField Type: RW
BitField Desc: L bit disable 1: disable L bit in the Control Word (assign zero
in the packet) 0: normal
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pla_comm_pw_ctrl_PlaPwLbitDisCtrl_Bit_Start                                                     5
#define cAf6_pla_comm_pw_ctrl_PlaPwLbitDisCtrl_Bit_End                                                       5
#define cAf6_pla_comm_pw_ctrl_PlaPwLbitDisCtrl_Mask                                                      cBit5
#define cAf6_pla_comm_pw_ctrl_PlaPwLbitDisCtrl_Shift                                                         5
#define cAf6_pla_comm_pw_ctrl_PlaPwLbitDisCtrl_MaxVal                                                      0x1
#define cAf6_pla_comm_pw_ctrl_PlaPwLbitDisCtrl_MinVal                                                      0x0
#define cAf6_pla_comm_pw_ctrl_PlaPwLbitDisCtrl_RstVal                                                      0x0

/*--------------------------------------
BitField Name: PlaPwRbitCPUCtrl
BitField Type: RW
BitField Desc: R bit value from CPU (low priority than PlaPwRbitDisCtrl)
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pla_comm_pw_ctrl_PlaPwRbitCPUCtrl_Bit_Start                                                     4
#define cAf6_pla_comm_pw_ctrl_PlaPwRbitCPUCtrl_Bit_End                                                       4
#define cAf6_pla_comm_pw_ctrl_PlaPwRbitCPUCtrl_Mask                                                      cBit4
#define cAf6_pla_comm_pw_ctrl_PlaPwRbitCPUCtrl_Shift                                                         4
#define cAf6_pla_comm_pw_ctrl_PlaPwRbitCPUCtrl_MaxVal                                                      0x1
#define cAf6_pla_comm_pw_ctrl_PlaPwRbitCPUCtrl_MinVal                                                      0x0
#define cAf6_pla_comm_pw_ctrl_PlaPwRbitCPUCtrl_RstVal                                                      0x0

/*--------------------------------------
BitField Name: PlaPwMbitCPUCtrl
BitField Type: RW
BitField Desc: M or NP bits value from CPU (low priority than
PlaLoPwMbitDisCtrl)
BitField Bits: [3:2]
--------------------------------------*/
#define cAf6_pla_comm_pw_ctrl_PlaPwMbitCPUCtrl_Bit_Start                                                     2
#define cAf6_pla_comm_pw_ctrl_PlaPwMbitCPUCtrl_Bit_End                                                       3
#define cAf6_pla_comm_pw_ctrl_PlaPwMbitCPUCtrl_Mask                                                    cBit3_2
#define cAf6_pla_comm_pw_ctrl_PlaPwMbitCPUCtrl_Shift                                                         2
#define cAf6_pla_comm_pw_ctrl_PlaPwMbitCPUCtrl_MaxVal                                                      0x3
#define cAf6_pla_comm_pw_ctrl_PlaPwMbitCPUCtrl_MinVal                                                      0x0
#define cAf6_pla_comm_pw_ctrl_PlaPwMbitCPUCtrl_RstVal                                                      0x0

/*--------------------------------------
BitField Name: PlaPwLbitCPUCtrl
BitField Type: RW
BitField Desc: L bit value from CPU (low priority than PlaLoPwLbitDisCtrl)
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pla_comm_pw_ctrl_PlaPwLbitCPUCtrl_Bit_Start                                                     1
#define cAf6_pla_comm_pw_ctrl_PlaPwLbitCPUCtrl_Bit_End                                                       1
#define cAf6_pla_comm_pw_ctrl_PlaPwLbitCPUCtrl_Mask                                                      cBit1
#define cAf6_pla_comm_pw_ctrl_PlaPwLbitCPUCtrl_Shift                                                         1
#define cAf6_pla_comm_pw_ctrl_PlaPwLbitCPUCtrl_MaxVal                                                      0x1
#define cAf6_pla_comm_pw_ctrl_PlaPwLbitCPUCtrl_MinVal                                                      0x0
#define cAf6_pla_comm_pw_ctrl_PlaPwLbitCPUCtrl_RstVal                                                      0x0

/*--------------------------------------
BitField Name: PlaPwEnCtrl
BitField Type: RW
BitField Desc: Pseudowire enable 1: enable 0: disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pla_comm_pw_ctrl_PlaPwEnCtrl_Bit_Start                                                          0
#define cAf6_pla_comm_pw_ctrl_PlaPwEnCtrl_Bit_End                                                            0
#define cAf6_pla_comm_pw_ctrl_PlaPwEnCtrl_Mask                                                           cBit0
#define cAf6_pla_comm_pw_ctrl_PlaPwEnCtrl_Shift                                                              0
#define cAf6_pla_comm_pw_ctrl_PlaPwEnCtrl_MaxVal                                                           0x1
#define cAf6_pla_comm_pw_ctrl_PlaPwEnCtrl_MinVal                                                           0x0
#define cAf6_pla_comm_pw_ctrl_PlaPwEnCtrl_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Protection Flow Control
Reg Addr   : 0x3_0000-0x2_05FF
Reg Formula: 0x3_0000 + $flow
    Where  :
           + $flow(0-1535): flow channel
Reg Desc   :
This register is used to config UPSR/HSPW for protect flow

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_prot_flow_ctrl_Base                                                                0x30000
#define cAf6Reg_pla_prot_flow_ctrl(flow)                                                      (0x30000+(flow))
#define cAf6Reg_pla_prot_flow_ctrl_WidthVal                                                                 32
#define cAf6Reg_pla_prot_flow_ctrl_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: PlaOutUpsrGrpCtr
BitField Type: RW
BitField Desc: UPRS group
BitField Bits: [23:13]
--------------------------------------*/
#define cAf6_pla_prot_flow_ctrl_PlaOutUpsrGrpCtr_Bit_Start                                                  13
#define cAf6_pla_prot_flow_ctrl_PlaOutUpsrGrpCtr_Bit_End                                                    23
#define cAf6_pla_prot_flow_ctrl_PlaOutUpsrGrpCtr_Mask                                                cBit23_13
#define cAf6_pla_prot_flow_ctrl_PlaOutUpsrGrpCtr_Shift                                                      13
#define cAf6_pla_prot_flow_ctrl_PlaOutUpsrGrpCtr_MaxVal                                                  0x7ff
#define cAf6_pla_prot_flow_ctrl_PlaOutUpsrGrpCtr_MinVal                                                    0x0
#define cAf6_pla_prot_flow_ctrl_PlaOutUpsrGrpCtr_RstVal                                                    0x0

/*--------------------------------------
BitField Name: PlaOutHspwGrpCtr
BitField Type: RW
BitField Desc: HSPW group
BitField Bits: [12:2]
--------------------------------------*/
#define cAf6_pla_prot_flow_ctrl_PlaOutHspwGrpCtr_Bit_Start                                                   2
#define cAf6_pla_prot_flow_ctrl_PlaOutHspwGrpCtr_Bit_End                                                    12
#define cAf6_pla_prot_flow_ctrl_PlaOutHspwGrpCtr_Mask                                                 cBit12_2
#define cAf6_pla_prot_flow_ctrl_PlaOutHspwGrpCtr_Shift                                                       2
#define cAf6_pla_prot_flow_ctrl_PlaOutHspwGrpCtr_MaxVal                                                  0x7ff
#define cAf6_pla_prot_flow_ctrl_PlaOutHspwGrpCtr_MinVal                                                    0x0
#define cAf6_pla_prot_flow_ctrl_PlaOutHspwGrpCtr_RstVal                                                    0x0

/*--------------------------------------
BitField Name: PlaFlowUPSRUsedCtrl
BitField Type: RW
BitField Desc: Flow UPSR is used or not 0:not used, all configurations for UPSR
will be not effected 1:used
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pla_prot_flow_ctrl_PlaFlowUPSRUsedCtrl_Bit_Start                                                1
#define cAf6_pla_prot_flow_ctrl_PlaFlowUPSRUsedCtrl_Bit_End                                                  1
#define cAf6_pla_prot_flow_ctrl_PlaFlowUPSRUsedCtrl_Mask                                                 cBit1
#define cAf6_pla_prot_flow_ctrl_PlaFlowUPSRUsedCtrl_Shift                                                    1
#define cAf6_pla_prot_flow_ctrl_PlaFlowUPSRUsedCtrl_MaxVal                                                 0x1
#define cAf6_pla_prot_flow_ctrl_PlaFlowUPSRUsedCtrl_MinVal                                                 0x0
#define cAf6_pla_prot_flow_ctrl_PlaFlowUPSRUsedCtrl_RstVal                                                 0x0

/*--------------------------------------
BitField Name: PlaFlowHSPWUsedCtrl
BitField Type: RW
BitField Desc: Flow HSPW is used or not 0:not used, all configurations for HSPW
will be not effected 1:used
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pla_prot_flow_ctrl_PlaFlowHSPWUsedCtrl_Bit_Start                                                0
#define cAf6_pla_prot_flow_ctrl_PlaFlowHSPWUsedCtrl_Bit_End                                                  0
#define cAf6_pla_prot_flow_ctrl_PlaFlowHSPWUsedCtrl_Mask                                                 cBit0
#define cAf6_pla_prot_flow_ctrl_PlaFlowHSPWUsedCtrl_Shift                                                    0
#define cAf6_pla_prot_flow_ctrl_PlaFlowHSPWUsedCtrl_MaxVal                                                 0x1
#define cAf6_pla_prot_flow_ctrl_PlaFlowHSPWUsedCtrl_MinVal                                                 0x0
#define cAf6_pla_prot_flow_ctrl_PlaFlowHSPWUsedCtrl_RstVal                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Flow UPSR Control
Reg Addr   : 0x3_4000-0x3_45FF
Reg Formula: 0x3_4000 + $upsrgrp
    Where  :
           + $upsrgrp(0-1535): UPSR group address
Reg Desc   :
This register is used to config Flow UPSR group

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_flow_upsr_ctrl_Base                                                                0x34000
#define cAf6Reg_pla_flow_upsr_ctrl(upsrgrp)                                                (0x34000+(upsrgrp))
#define cAf6Reg_pla_flow_upsr_ctrl_WidthVal                                                                 32
#define cAf6Reg_pla_flow_upsr_ctrl_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: PlaFlowUpsrEnCtr
BitField Type: RW
BitField Desc: enable/disable for upsr group 1: enable 0: disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pla_flow_upsr_ctrl_PlaFlowUpsrEnCtr_Bit_Start                                                   0
#define cAf6_pla_flow_upsr_ctrl_PlaFlowUpsrEnCtr_Bit_End                                                     0
#define cAf6_pla_flow_upsr_ctrl_PlaFlowUpsrEnCtr_Mask                                                    cBit0
#define cAf6_pla_flow_upsr_ctrl_PlaFlowUpsrEnCtr_Shift                                                       0
#define cAf6_pla_flow_upsr_ctrl_PlaFlowUpsrEnCtr_MaxVal                                                    0x1
#define cAf6_pla_flow_upsr_ctrl_PlaFlowUpsrEnCtr_MinVal                                                    0x0
#define cAf6_pla_flow_upsr_ctrl_PlaFlowUpsrEnCtr_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Flow HSPW Control
Reg Addr   : 0x1_6000-0x1_65FF
Reg Formula: 0x1_6000 + $hspwgrp
    Where  :
           + $hspwgrp(0-1535): HSPW group address
Reg Desc   :
This register is used to config PW HSPW group

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_flow_hspw_ctrl_Base                                                                0x16000
#define cAf6Reg_pla_flow_hspw_ctrl(hspwgrp)                                                (0x16000+(hspwgrp))
#define cAf6Reg_pla_flow_hspw_ctrl_WidthVal                                                                 32
#define cAf6Reg_pla_flow_hspw_ctrl_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: PlaPWHSPWPSNCtr
BitField Type: RW
BitField Desc: page for hspw grop 1: PSN page#1 0: PSN page#0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pla_flow_hspw_ctrl_PlaPWHSPWPSNCtr_Bit_Start                                                    0
#define cAf6_pla_flow_hspw_ctrl_PlaPWHSPWPSNCtr_Bit_End                                                      0
#define cAf6_pla_flow_hspw_ctrl_PlaPWHSPWPSNCtr_Mask                                                     cBit0
#define cAf6_pla_flow_hspw_ctrl_PlaPWHSPWPSNCtr_Shift                                                        0
#define cAf6_pla_flow_hspw_ctrl_PlaPWHSPWPSNCtr_MaxVal                                                     0x1
#define cAf6_pla_flow_hspw_ctrl_PlaPWHSPWPSNCtr_MinVal                                                     0x0
#define cAf6_pla_flow_hspw_ctrl_PlaPWHSPWPSNCtr_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Block Buffer Control
Reg Addr   : 0x2_4000-0x2_4DF0
Reg Formula: 0x2_4000 + $cid
    Where  :
           + $cid(0-9215): channel id address (0-1535): Psedowire ID (1536-2560): iMSG link (2561-3568): VCAT lineID}
Reg Desc   :
This register is used to configure BlockID for the buffer for all services (Psedowire/iMSG/VCAT)

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_blk_buf_ctrl_Base                                                                  0x24000
#define cAf6Reg_pla_blk_buf_ctrl(cid)                                                          (0x24000+(cid))
#define cAf6Reg_pla_blk_buf_ctrl_WidthVal                                                                   32
#define cAf6Reg_pla_blk_buf_ctrl_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: PlaBlkIDCtr
BitField Type: RW
BitField Desc: Block Buffer Number There are 4096 blocks for LO rate (BlkType =
0), a block location for eache LO rate channel. There are 96 block location for
HO rate (BlkType != 0), a block location for eache DS3/E3/STS1 rate channel,
With the higer bandwidth rate channels, need to configure start block of a rate
channels, next blocks are increase one-by-one and roll-over when meet max number
block, max number block is depend on BlockType (STS3c = 3,STS12c = 12,STS24c =
24,STS48c = 48)
BitField Bits: [14:3]
--------------------------------------*/
#define cAf6_pla_blk_buf_ctrl_PlaBlkIDCtr_Bit_Start                                                          3
#define cAf6_pla_blk_buf_ctrl_PlaBlkIDCtr_Bit_End                                                           14
#define cAf6_pla_blk_buf_ctrl_PlaBlkIDCtr_Mask                                                        cBit14_3
#define cAf6_pla_blk_buf_ctrl_PlaBlkIDCtr_Shift                                                              3
#define cAf6_pla_blk_buf_ctrl_PlaBlkIDCtr_MaxVal                                                         0xfff
#define cAf6_pla_blk_buf_ctrl_PlaBlkIDCtr_MinVal                                                           0x0
#define cAf6_pla_blk_buf_ctrl_PlaBlkIDCtr_RstVal                                                           0x0

/*--------------------------------------
BitField Name: PlaBlkTypeCtr
BitField Type: RW
BitField Desc: Type of TDM rate used this block 0: DS0/DS1/E1/VC1x 1:
DS3/E3/STS1 2: STS3c 3: STS12c 4: STS24c 5: STS48c
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_pla_blk_buf_ctrl_PlaBlkTypeCtr_Bit_Start                                                        0
#define cAf6_pla_blk_buf_ctrl_PlaBlkTypeCtr_Bit_End                                                          2
#define cAf6_pla_blk_buf_ctrl_PlaBlkTypeCtr_Mask                                                       cBit2_0
#define cAf6_pla_blk_buf_ctrl_PlaBlkTypeCtr_Shift                                                            0
#define cAf6_pla_blk_buf_ctrl_PlaBlkTypeCtr_MaxVal                                                         0x7
#define cAf6_pla_blk_buf_ctrl_PlaBlkTypeCtr_MinVal                                                         0x0
#define cAf6_pla_blk_buf_ctrl_PlaBlkTypeCtr_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Lookup Flow Control
Reg Addr   : 0x2_C000-0x2_C6FF
Reg Formula: 0x2_C000 + $cid
    Where  :
           + $cid (0-4095): channel id channel (0-1535): Psedowire ID (1536-1791): VCG ID}
Reg Desc   :
This register is used to lookup from (PW/VCG) to flow ID

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_lk_flow_ctrl_Base                                                                  0x2C000
#define cAf6Reg_pla_lk_flow_ctrl(cid)                                                          (0x2C000+(cid))
#define cAf6Reg_pla_lk_flow_ctrl_WidthVal                                                                   32
#define cAf6Reg_pla_lk_flow_ctrl_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: PlaOutLkFlowCtrl
BitField Type: RW
BitField Desc: Lookup FlowID
BitField Bits: [11:0]
--------------------------------------*/
#define cAf6_pla_lk_flow_ctrl_PlaOutLkFlowCtrl_Bit_Start                                                     0
#define cAf6_pla_lk_flow_ctrl_PlaOutLkFlowCtrl_Bit_End                                                      11
#define cAf6_pla_lk_flow_ctrl_PlaOutLkFlowCtrl_Mask                                                   cBit11_0
#define cAf6_pla_lk_flow_ctrl_PlaOutLkFlowCtrl_Shift                                                         0
#define cAf6_pla_lk_flow_ctrl_PlaOutLkFlowCtrl_MaxVal                                                    0xfff
#define cAf6_pla_lk_flow_ctrl_PlaOutLkFlowCtrl_MinVal                                                      0x0
#define cAf6_pla_lk_flow_ctrl_PlaOutLkFlowCtrl_RstVal                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Output Flow Control
Reg Addr   : 0x2_8000-0x2_87FF
Reg Formula: 0x2_8000 + $flow
    Where  :
           + $flow(0-2047): flow channel
Reg Desc   :
This register is used to config flow at output

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_out_flow_ctrl_Base                                                                 0x28000
#define cAf6Reg_pla_out_flow_ctrl(flow)                                                       (0x28000+(flow))
#define cAf6Reg_pla_out_flow_ctrl_WidthVal                                                                  32
#define cAf6Reg_pla_out_flow_ctrl_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: PlaOutPSNLenCtrl
BitField Type: RW
BitField Desc: PSN length
BitField Bits: [6:0]
--------------------------------------*/
#define cAf6_pla_out_flow_ctrl_PlaOutPSNLenCtrl_Bit_Start                                                    0
#define cAf6_pla_out_flow_ctrl_PlaOutPSNLenCtrl_Bit_End                                                      6
#define cAf6_pla_out_flow_ctrl_PlaOutPSNLenCtrl_Mask                                                   cBit6_0
#define cAf6_pla_out_flow_ctrl_PlaOutPSNLenCtrl_Shift                                                        0
#define cAf6_pla_out_flow_ctrl_PlaOutPSNLenCtrl_MaxVal                                                    0x7f
#define cAf6_pla_out_flow_ctrl_PlaOutPSNLenCtrl_MinVal                                                     0x0
#define cAf6_pla_out_flow_ctrl_PlaOutPSNLenCtrl_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Output PSN Process Control
Reg Addr   : 0x2_0000
Reg Formula:
    Where  :
Reg Desc   :
This register is used to control PSN configuration

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_out_psnpro_ctrl_Base                                                               0x20000
#define cAf6Reg_pla_out_psnpro_ctrl                                                                    0x20000
#define cAf6Reg_pla_out_psnpro_ctrl_WidthVal                                                                32
#define cAf6Reg_pla_out_psnpro_ctrl_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: PlaOutPsnProReqCtr
BitField Type: RW
BitField Desc: Request to process PSN 1: request to write/read PSN header
buffer. The CPU need to prepare a complete PSN header into PSN buffer before
request write OR read out a complete PSN header in PSN buffer after request read
done 0: HW will automatically set to 0 when a request done
BitField Bits: [31]
--------------------------------------*/
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProReqCtr_Bit_Start                                               31
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProReqCtr_Bit_End                                                 31
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProReqCtr_Mask                                                cBit31
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProReqCtr_Shift                                                   31
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProReqCtr_MaxVal                                                 0x1
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProReqCtr_MinVal                                                 0x0
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProReqCtr_RstVal                                                 0x0

/*--------------------------------------
BitField Name: PlaOutPsnProRnWCtr
BitField Type: RW
BitField Desc: read or write PSN 1: read PSN 0: write PSN
BitField Bits: [30]
--------------------------------------*/
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProRnWCtr_Bit_Start                                               30
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProRnWCtr_Bit_End                                                 30
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProRnWCtr_Mask                                                cBit30
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProRnWCtr_Shift                                                   30
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProRnWCtr_MaxVal                                                 0x1
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProRnWCtr_MinVal                                                 0x0
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProRnWCtr_RstVal                                                 0x0

/*--------------------------------------
BitField Name: PlaOutPsnProLenCtr
BitField Type: RW
BitField Desc: Length of a complete PSN need to read/write
BitField Bits: [22:16]
--------------------------------------*/
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProLenCtr_Bit_Start                                               16
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProLenCtr_Bit_End                                                 22
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProLenCtr_Mask                                             cBit22_16
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProLenCtr_Shift                                                   16
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProLenCtr_MaxVal                                                0x7f
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProLenCtr_MinVal                                                 0x0
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProLenCtr_RstVal                                                 0x0

/*--------------------------------------
BitField Name: PlaOutPsnProPageCtr
BitField Type: RW
BitField Desc: there is 2 pages PSN location per PWID, depend on the
configuration of "PlaOutHspwPsnCtr", the engine will choice which the page to
encapsulate into the packet.
BitField Bits: [13]
--------------------------------------*/
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProPageCtr_Bit_Start                                              13
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProPageCtr_Bit_End                                                13
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProPageCtr_Mask                                               cBit13
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProPageCtr_Shift                                                  13
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProPageCtr_MaxVal                                                0x1
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProPageCtr_MinVal                                                0x0
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProPageCtr_RstVal                                                0x0

/*--------------------------------------
BitField Name: PlaOutPsnProFlowCtr
BitField Type: RW
BitField Desc: Flow channel
BitField Bits: [11:0]
--------------------------------------*/
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProFlowCtr_Bit_Start                                               0
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProFlowCtr_Bit_End                                                11
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProFlowCtr_Mask                                             cBit11_0
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProFlowCtr_Shift                                                   0
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProFlowCtr_MaxVal                                              0xfff
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProFlowCtr_MinVal                                                0x0
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProFlowCtr_RstVal                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Output PSN Buffer Control
Reg Addr   : 0x2_0010-0x2_0015
Reg Formula: 0x2_0010 + $segid
    Where  :
           + $segid(0-5): segment 16-byte PSN, total is 6 segments for maximum 96 bytes PSN  %
Reg Desc   :
This register is used to store PSN data which is before CPU request write or after CPU request read %%
The header format from entry#0 to entry#4 is as follow: %%
{DA(6-byte),SA(6-byte),VLAN1(4-byte,optional),VLAN2(4-byte, optional),EthType(2-byte),PSN Header(4 to 96 bytes)} %%
Depends on specific PSN selected for each pseudowire, the EthType and PSN Header field may have the following values and formats: %%

PSN header is MEF-8: %%
EthType:  0x88D8 %%
4-byte PSN Header with format: {ECID[19:0], 0x102} where ECID is pseodowire identification for  remote  receive side. %%
PSN header is MPLS:  %%
EthType:  0x8847 %%
4/8/12-byte PSN Header with format: {OuterLabel2[31:0](optional), OuterLabel1[31:0](optional),  InnerLabel[31:0]}   %%
Where InnerLabel[31:12] is pseodowire identification for remote receive side. %%
Label format: {Idenfifier[19:0],Exp[2:0],StackBit,TTL[7:0]} where StackBit =  for InnerLabel %%
PSN header is UDP/Ipv4:  %%
EthType:  0x0800 %%
28-byte PSN Header with format {Ipv4 Header(20 bytes), UDP Header(8 byte)} as below: %%
{IP_Ver[3:0], IP_IHL[3:0], IP_ToS[7:0], IP_Header_Sum[31:16]} %%
{IP_Iden[15:0], IP_Flag[2:0], IP_Frag_Offset[12:0]}  %%
{IP_TTL[7:0], IP_Protocol[7:0], IP_Header_Sum[15:0]} %%
{IP_SrcAdr[31:0]} %%
{IP_DesAdr[31:0]} %%
{UDP_SrcPort[15:0], UDP_DesPort[15:0]} %%
{UDP_Sum[31:0] (optional)} %%
Case: %%
IP_Protocol[7:0]: 0x11 to signify UDP  %%
IP_Protocol[7:0]: 0x11 to signify UDP  %%
IP_Header_Sum[31:0]:  CPU calculate these fields as a temporarily for HW before plus rest fields %%
{IP_Ver[3:0], IP_IHL[3:0], IP_ToS[7:0]} +%%
IP_Iden[15:0] + {IP_Flag[2:0], IP_Frag_Offset[12:0]}  +%%
{IP_TTL[7:0], IP_Protocol[7:0]} + %%
IP_SrcAdr[31:16] + IP_SrcAdr[15:0] +%%
IP_DesAdr[31:16] + IP_DesAdr[15:0]%%
UDP_SrcPort: used as remote pseudowire identification or 0x85E if unused%%
UDP_DesPort : used as remote pseudowire identification or 0x85E if unused%%
UDP_Sum[31:0] (optional): CPU calculate these fields as a temporarily for HW before plus rest fields%%
IP_SrcAdr[127:112] +    IP_SrcAdr[111:96] +     %%
IP_SrcAdr[95:80]  + IP_SrcAdr[79:64] +  %%
IP_SrcAdr[63:48]  + IP_SrcAdr[47:32] +  %%
IP_SrcAdr[31:16]  + IP_SrcAdr[15:0] +   %%
IP_DesAdr[127:112] +    IP_DesAdr[111:96] +     %%
IP_DesAdr[95:80]  + IP_DesAdr[79:64] +  %%
IP_DesAdr[63:48]  + IP_DesAdr[47:32] +  %%
IP_DesAdr[31:16]  + IP_DesAdr[15:0] +%%
{8-bit zeros, IP_Protocol[7:0]} +%%
UDP_SrcPort[15:0] +  UDP_DesPort[15:0]%%
PSN header is UDP/Ipv6:  %%
EthType:  0x86DD%%
48-byte PSN Header with format {Ipv6 Header(40 bytes), UDP Header(8 byte)} as below:%%
{IP_Ver[3:0], IP_Traffic_Class[7:0], IP_Flow_Label[19:0]}%%
{16-bit zeros, IP_Next_Header[7:0], IP_Hop_Limit[7:0]} %%
{IP_SrcAdr[127:96]}%%
{IP_SrcAdr[95:64]}%%
{IP_SrcAdr[63:32]}%%
{IP_SrcAdr[31:0]}%%
{IP_DesAdr[127:96]}%%
{IP_DesAdr[95:64]}%%
{IP_DesAdr[63:32]}%%
{IP_DesAdr[31:0]}%%
{UDP_SrcPort[15:0], UDP_DesPort[15:0]}%%
{UDP_Sum[31:0]}%%
Case:%%
IP_Next_Header[7:0]: 0x11 to signify UDP %%
UDP_Sum[31:0]:  CPU calculate these fields as a temporarily for HW before plus rest fields%%
IP_SrcAdr[127:112] +    IP_SrcAdr[111:96] +     %%
IP_SrcAdr[95:80]  + IP_SrcAdr[79:64] +  %%
IP_SrcAdr[63:48]  + IP_SrcAdr[47:32] +  %%
IP_SrcAdr[31:16]  + IP_SrcAdr[15:0] +   %%
IP_DesAdr[127:112] +    IP_DesAdr[111:96] +     %%
IP_DesAdr[95:80]  + IP_DesAdr[79:64] +  %%
IP_DesAdr[63:48]  + IP_DesAdr[47:32] +  %%
IP_DesAdr[31:16]  + IP_DesAdr[15:0] +%%
{8-bit zeros, IP_Next_Header[7:0]} +%%
UDP_SrcPort[15:0] +  UDP_DesPort[15:0]%%
UDP_SrcPort: used as remote pseudowire identification or 0x85E if unused%%
UDP_DesPort : used as remote pseudowire identification or 0x85E if unused%%
User can select either source or destination port for pseodowire identification. See IP/UDP standards for more description about other field. %%
PSN header is MPLS over Ipv4:%%
IPv4 header must have IP_Protocol[7:0] value 0x89 to signify MPLS%%
After IPv4 header is MPLS labels where inner label is used for PW identification%%
PSN header is MPLS over Ipv6:%%
IPv6 header must have IP_Next_Header[7:0] value 0x89 to signify MPLS%%
After IPv6 header is MPLS labels where inner label is used for PW identification%%
PSN header (all modes) with RTP enable: RTP used for TDM PW (define in RFC3550), is in the first 8-byte of this buffer (bit[127:64] of segid == 0), following is PSN header (start from bit[63:0] of segid = 0)%%
Case:%%
RTPSSRC[31:0] : This is the SSRC value of RTP header%%
RtpPtValue[6:0]: This is the PT value of RTP header, define in http://www.iana.org/assignments/rtp-parameters/rtp-parameters.xml

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_out_psnbuf_ctrl_Base                                                               0x20010
#define cAf6Reg_pla_out_psnbuf_ctrl(segid)                                                   (0x20010+(segid))
#define cAf6Reg_pla_out_psnbuf_ctrl_WidthVal                                                               128
#define cAf6Reg_pla_out_psnbuf_ctrl_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: PlaOutPsnProReqCtr
BitField Type: RW
BitField Desc: PSN buffer
BitField Bits: [127:0]
--------------------------------------*/
#define cAf6_pla_out_psnbuf_ctrl_PlaOutPsnProReqCtr_Bit_Start                                                0
#define cAf6_pla_out_psnbuf_ctrl_PlaOutPsnProReqCtr_Bit_End                                                127
#define cAf6_pla_out_psnbuf_ctrl_PlaOutPsnProReqCtr_Mask_01                                           cBit31_0
#define cAf6_pla_out_psnbuf_ctrl_PlaOutPsnProReqCtr_Shift_01                                                 0
#define cAf6_pla_out_psnbuf_ctrl_PlaOutPsnProReqCtr_Mask_02                                           cBit31_0
#define cAf6_pla_out_psnbuf_ctrl_PlaOutPsnProReqCtr_Shift_02                                                 0
#define cAf6_pla_out_psnbuf_ctrl_PlaOutPsnProReqCtr_Mask_03                                           cBit31_0
#define cAf6_pla_out_psnbuf_ctrl_PlaOutPsnProReqCtr_Shift_03                                                 0
#define cAf6_pla_out_psnbuf_ctrl_PlaOutPsnProReqCtr_Mask_04                                           cBit31_0
#define cAf6_pla_out_psnbuf_ctrl_PlaOutPsnProReqCtr_Shift_04                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Hold Register Control
Reg Addr   : 0x2_1000-0x2_1002
Reg Formula: 0x2_1000 + $holdreg
    Where  :
           + $holdreg(0-2): hold register with (holdreg=0) for bit63_32, (holdreg=1) for bit95_64, (holdreg=2) for bit127_96
Reg Desc   :
This register is used to control hold register.

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_hold_reg_ctrl_Base                                                                 0x21000
#define cAf6Reg_pla_hold_reg_ctrl(holdreg)                                                 (0x21000+(holdreg))
#define cAf6Reg_pla_hold_reg_ctrl_WidthVal                                                                  32
#define cAf6Reg_pla_hold_reg_ctrl_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: PlaHoldRegCtr
BitField Type: RW
BitField Desc: hold register value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_pla_hold_reg_ctrl_PlaHoldRegCtr_Bit_Start                                                       0
#define cAf6_pla_hold_reg_ctrl_PlaHoldRegCtr_Bit_End                                                        31
#define cAf6_pla_hold_reg_ctrl_PlaHoldRegCtr_Mask                                                     cBit31_0
#define cAf6_pla_hold_reg_ctrl_PlaHoldRegCtr_Shift                                                           0
#define cAf6_pla_hold_reg_ctrl_PlaHoldRegCtr_MaxVal                                                 0xffffffff
#define cAf6_pla_hold_reg_ctrl_PlaHoldRegCtr_MinVal                                                        0x0
#define cAf6_pla_hold_reg_ctrl_PlaHoldRegCtr_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit3_0 Control
Reg Addr   : 0x2_1100 - 0x2_110F
Reg Formula: 0x2_1100 + HaAddr3_0
    Where  :
           + $HaAddr3_0(0-15): HA Address Bit3_0
Reg Desc   :
This register is used to send HA read address bit3_0 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha3_0_control_Base                                                                   0x21100
#define cAf6Reg_rdha3_0_control(HaAddr30)                                                 (0x21100+(HaAddr30))
#define cAf6Reg_rdha3_0_control_WidthVal                                                                    32
#define cAf6Reg_rdha3_0_control_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: ReadAddr3_0
BitField Type: RW
BitField Desc: Read value will be 0x8C100 plus HaAddr3_0
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha3_0_control_ReadAddr3_0_Bit_Start                                                           0
#define cAf6_rdha3_0_control_ReadAddr3_0_Bit_End                                                            19
#define cAf6_rdha3_0_control_ReadAddr3_0_Mask                                                         cBit19_0
#define cAf6_rdha3_0_control_ReadAddr3_0_Shift                                                               0
#define cAf6_rdha3_0_control_ReadAddr3_0_MaxVal                                                        0xfffff
#define cAf6_rdha3_0_control_ReadAddr3_0_MinVal                                                            0x0
#define cAf6_rdha3_0_control_ReadAddr3_0_RstVal                                                            0xx


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit7_4 Control
Reg Addr   : 0x2_1110 - 0x2_111F
Reg Formula: 0x2_1110 + HaAddr7_4
    Where  :
           + $HaAddr7_4(0-15): HA Address Bit7_4
Reg Desc   :
This register is used to send HA read address bit7_4 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha7_4_control_Base                                                                   0x21110
#define cAf6Reg_rdha7_4_control(HaAddr74)                                                 (0x21110+(HaAddr74))
#define cAf6Reg_rdha7_4_control_WidthVal                                                                    32
#define cAf6Reg_rdha7_4_control_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: ReadAddr7_4
BitField Type: RW
BitField Desc: Read value will be 0x8C100 plus HaAddr7_4
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha7_4_control_ReadAddr7_4_Bit_Start                                                           0
#define cAf6_rdha7_4_control_ReadAddr7_4_Bit_End                                                            19
#define cAf6_rdha7_4_control_ReadAddr7_4_Mask                                                         cBit19_0
#define cAf6_rdha7_4_control_ReadAddr7_4_Shift                                                               0
#define cAf6_rdha7_4_control_ReadAddr7_4_MaxVal                                                        0xfffff
#define cAf6_rdha7_4_control_ReadAddr7_4_MinVal                                                            0x0
#define cAf6_rdha7_4_control_ReadAddr7_4_RstVal                                                            0xx


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit11_8 Control
Reg Addr   : 0x2_1120 - 0x2_112F
Reg Formula: 0x2_1120 + HaAddr11_8
    Where  :
           + $HaAddr11_8(0-15): HA Address Bit11_8
Reg Desc   :
This register is used to send HA read address bit11_8 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha11_8_control_Base                                                                  0x21120
#define cAf6Reg_rdha11_8_control(HaAddr118)                                              (0x21120+(HaAddr118))
#define cAf6Reg_rdha11_8_control_WidthVal                                                                   32
#define cAf6Reg_rdha11_8_control_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: ReadAddr11_8
BitField Type: RW
BitField Desc: Read value will be 0x8C100 plus HaAddr11_8
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha11_8_control_ReadAddr11_8_Bit_Start                                                         0
#define cAf6_rdha11_8_control_ReadAddr11_8_Bit_End                                                          19
#define cAf6_rdha11_8_control_ReadAddr11_8_Mask                                                       cBit19_0
#define cAf6_rdha11_8_control_ReadAddr11_8_Shift                                                             0
#define cAf6_rdha11_8_control_ReadAddr11_8_MaxVal                                                      0xfffff
#define cAf6_rdha11_8_control_ReadAddr11_8_MinVal                                                          0x0
#define cAf6_rdha11_8_control_ReadAddr11_8_RstVal                                                          0xx


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit15_12 Control
Reg Addr   : 0x2_1130 - 0x2_113F
Reg Formula: 0x2_1130 + HaAddr15_12
    Where  :
           + $HaAddr15_12(0-15): HA Address Bit15_12
Reg Desc   :
This register is used to send HA read address bit15_12 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha15_12_control_Base                                                                 0x21130
#define cAf6Reg_rdha15_12_control(HaAddr1512)                                           (0x21130+(HaAddr1512))
#define cAf6Reg_rdha15_12_control_WidthVal                                                                  32
#define cAf6Reg_rdha15_12_control_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: ReadAddr15_12
BitField Type: RW
BitField Desc: Read value will be 0x8C100 plus HaAddr15_12
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha15_12_control_ReadAddr15_12_Bit_Start                                                       0
#define cAf6_rdha15_12_control_ReadAddr15_12_Bit_End                                                        19
#define cAf6_rdha15_12_control_ReadAddr15_12_Mask                                                     cBit19_0
#define cAf6_rdha15_12_control_ReadAddr15_12_Shift                                                           0
#define cAf6_rdha15_12_control_ReadAddr15_12_MaxVal                                                    0xfffff
#define cAf6_rdha15_12_control_ReadAddr15_12_MinVal                                                        0x0
#define cAf6_rdha15_12_control_ReadAddr15_12_RstVal                                                        0xx


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit19_16 Control
Reg Addr   : 0x2_1140 - 0x2_114F
Reg Formula: 0x2_1140 + HaAddr19_16
    Where  :
           + $HaAddr19_16(0-15): HA Address Bit19_16
Reg Desc   :
This register is used to send HA read address bit19_16 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha19_16_control_Base                                                                 0x21140
#define cAf6Reg_rdha19_16_control(HaAddr1916)                                           (0x21140+(HaAddr1916))
#define cAf6Reg_rdha19_16_control_WidthVal                                                                  32
#define cAf6Reg_rdha19_16_control_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: ReadAddr19_16
BitField Type: RW
BitField Desc: Read value will be 0x8C100 plus HaAddr19_16
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha19_16_control_ReadAddr19_16_Bit_Start                                                       0
#define cAf6_rdha19_16_control_ReadAddr19_16_Bit_End                                                        19
#define cAf6_rdha19_16_control_ReadAddr19_16_Mask                                                     cBit19_0
#define cAf6_rdha19_16_control_ReadAddr19_16_Shift                                                           0
#define cAf6_rdha19_16_control_ReadAddr19_16_MaxVal                                                    0xfffff
#define cAf6_rdha19_16_control_ReadAddr19_16_MinVal                                                        0x0
#define cAf6_rdha19_16_control_ReadAddr19_16_RstVal                                                        0xx


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit23_20 Control
Reg Addr   : 0x2_1150 - 0x2_115F
Reg Formula: 0x2_1150 + HaAddr23_20
    Where  :
           + $HaAddr23_20(0-15): HA Address Bit23_20
Reg Desc   :
This register is used to send HA read address bit23_20 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha23_20_control_Base                                                                 0x21150
#define cAf6Reg_rdha23_20_control(HaAddr2320)                                           (0x21150+(HaAddr2320))
#define cAf6Reg_rdha23_20_control_WidthVal                                                                  32
#define cAf6Reg_rdha23_20_control_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: ReadAddr23_20
BitField Type: RW
BitField Desc: Read value will be 0x8C100 plus HaAddr23_20
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha23_20_control_ReadAddr23_20_Bit_Start                                                       0
#define cAf6_rdha23_20_control_ReadAddr23_20_Bit_End                                                        19
#define cAf6_rdha23_20_control_ReadAddr23_20_Mask                                                     cBit19_0
#define cAf6_rdha23_20_control_ReadAddr23_20_Shift                                                           0
#define cAf6_rdha23_20_control_ReadAddr23_20_MaxVal                                                    0xfffff
#define cAf6_rdha23_20_control_ReadAddr23_20_MinVal                                                        0x0
#define cAf6_rdha23_20_control_ReadAddr23_20_RstVal                                                        0xx


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit24 and Data Control
Reg Addr   : 0x2_1160 - 0x2_1161
Reg Formula: 0x2_1160 + HaAddr24
    Where  :
           + $HaAddr24(0-1): HA Address Bit24
Reg Desc   :
This register is used to send HA read address bit24 to HA engine to read data

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha24data_control_Base                                                                0x21160
#define cAf6Reg_rdha24data_control(HaAddr24)                                              (0x21160+(HaAddr24))
#define cAf6Reg_rdha24data_control_WidthVal                                                                 32
#define cAf6Reg_rdha24data_control_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: ReadHaData31_0
BitField Type: RW
BitField Desc: HA read data bit31_0
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdha24data_control_ReadHaData31_0_Bit_Start                                                     0
#define cAf6_rdha24data_control_ReadHaData31_0_Bit_End                                                      31
#define cAf6_rdha24data_control_ReadHaData31_0_Mask                                                   cBit31_0
#define cAf6_rdha24data_control_ReadHaData31_0_Shift                                                         0
#define cAf6_rdha24data_control_ReadHaData31_0_MaxVal                                               0xffffffff
#define cAf6_rdha24data_control_ReadHaData31_0_MinVal                                                      0x0
#define cAf6_rdha24data_control_ReadHaData31_0_RstVal                                                      0xx


/*------------------------------------------------------------------------------
Reg Name   : Read HA Hold Data63_32
Reg Addr   : 0x2_1170
Reg Formula:
    Where  :
Reg Desc   :
This register is used to read HA dword2 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha_hold63_32_Base                                                                    0x21170
#define cAf6Reg_rdha_hold63_32                                                                         0x21170
#define cAf6Reg_rdha_hold63_32_WidthVal                                                                     32
#define cAf6Reg_rdha_hold63_32_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: ReadHaData63_32
BitField Type: RW
BitField Desc: HA read data bit63_32
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdha_hold63_32_ReadHaData63_32_Bit_Start                                                        0
#define cAf6_rdha_hold63_32_ReadHaData63_32_Bit_End                                                         31
#define cAf6_rdha_hold63_32_ReadHaData63_32_Mask                                                      cBit31_0
#define cAf6_rdha_hold63_32_ReadHaData63_32_Shift                                                            0
#define cAf6_rdha_hold63_32_ReadHaData63_32_MaxVal                                                  0xffffffff
#define cAf6_rdha_hold63_32_ReadHaData63_32_MinVal                                                         0x0
#define cAf6_rdha_hold63_32_ReadHaData63_32_RstVal                                                         0xx


/*------------------------------------------------------------------------------
Reg Name   : Read HA Hold Data95_64
Reg Addr   : 0x2_1171
Reg Formula:
    Where  :
Reg Desc   :
This register is used to read HA dword3 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_rdindr_hold95_64_Base                                                                  0x21171
#define cAf6Reg_rdindr_hold95_64                                                                       0x21171
#define cAf6Reg_rdindr_hold95_64_WidthVal                                                                   32
#define cAf6Reg_rdindr_hold95_64_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: ReadHaData95_64
BitField Type: RW
BitField Desc: HA read data bit95_64
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdindr_hold95_64_ReadHaData95_64_Bit_Start                                                      0
#define cAf6_rdindr_hold95_64_ReadHaData95_64_Bit_End                                                       31
#define cAf6_rdindr_hold95_64_ReadHaData95_64_Mask                                                    cBit31_0
#define cAf6_rdindr_hold95_64_ReadHaData95_64_Shift                                                          0
#define cAf6_rdindr_hold95_64_ReadHaData95_64_MaxVal                                                0xffffffff
#define cAf6_rdindr_hold95_64_ReadHaData95_64_MinVal                                                       0x0
#define cAf6_rdindr_hold95_64_ReadHaData95_64_RstVal                                                       0xx


/*------------------------------------------------------------------------------
Reg Name   : Read HA Hold Data127_96
Reg Addr   : 0x2_1172
Reg Formula:
    Where  :
Reg Desc   :
This register is used to read HA dword4 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_rdindr_hold127_96_Base                                                                 0x21172
#define cAf6Reg_rdindr_hold127_96                                                                      0x21172
#define cAf6Reg_rdindr_hold127_96_WidthVal                                                                  32
#define cAf6Reg_rdindr_hold127_96_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: ReadHaData127_96
BitField Type: RW
BitField Desc: HA read data bit127_96
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdindr_hold127_96_ReadHaData127_96_Bit_Start                                                    0
#define cAf6_rdindr_hold127_96_ReadHaData127_96_Bit_End                                                     31
#define cAf6_rdindr_hold127_96_ReadHaData127_96_Mask                                                  cBit31_0
#define cAf6_rdindr_hold127_96_ReadHaData127_96_Shift                                                        0
#define cAf6_rdindr_hold127_96_ReadHaData127_96_MaxVal                                              0xffffffff
#define cAf6_rdindr_hold127_96_ReadHaData127_96_MinVal                                                     0x0
#define cAf6_rdindr_hold127_96_ReadHaData127_96_RstVal                                                     0xx


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Output PSN Process Control
Reg Addr   : 0x2_0001
Reg Formula:
    Where  :
Reg Desc   :
This register is used to control PSN configuration

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_out_psnpro_ha_ctrl_Base                                                            0x20001
#define cAf6Reg_pla_out_psnpro_ha_ctrl                                                                 0x20001
#define cAf6Reg_pla_out_psnpro_ha_ctrl_WidthVal                                                             32
#define cAf6Reg_pla_out_psnpro_ha_ctrl_WriteMask                                                           0x0

/*--------------------------------------
BitField Name: PlaOutPsnProReqCtr
BitField Type: RW
BitField Desc: Request to process PSN 1: request to write/read PSN header
buffer. The CPU need to prepare a complete PSN header into PSN buffer before
request write OR read out a complete PSN header in PSN buffer after request read
done 0: HW will automatically set to 0 when a request done
BitField Bits: [31]
--------------------------------------*/
#define cAf6_pla_out_psnpro_ha_ctrl_PlaOutPsnProReqCtr_Bit_Start                                            31
#define cAf6_pla_out_psnpro_ha_ctrl_PlaOutPsnProReqCtr_Bit_End                                              31
#define cAf6_pla_out_psnpro_ha_ctrl_PlaOutPsnProReqCtr_Mask                                             cBit31
#define cAf6_pla_out_psnpro_ha_ctrl_PlaOutPsnProReqCtr_Shift                                                31
#define cAf6_pla_out_psnpro_ha_ctrl_PlaOutPsnProReqCtr_MaxVal                                              0x1
#define cAf6_pla_out_psnpro_ha_ctrl_PlaOutPsnProReqCtr_MinVal                                              0x0
#define cAf6_pla_out_psnpro_ha_ctrl_PlaOutPsnProReqCtr_RstVal                                              0x0

/*--------------------------------------
BitField Name: PlaOutPsnProLenCtr
BitField Type: RW
BitField Desc: Length of a complete PSN need to read/write
BitField Bits: [22:16]
--------------------------------------*/
#define cAf6_pla_out_psnpro_ha_ctrl_PlaOutPsnProLenCtr_Bit_Start                                            16
#define cAf6_pla_out_psnpro_ha_ctrl_PlaOutPsnProLenCtr_Bit_End                                              22
#define cAf6_pla_out_psnpro_ha_ctrl_PlaOutPsnProLenCtr_Mask                                          cBit22_16
#define cAf6_pla_out_psnpro_ha_ctrl_PlaOutPsnProLenCtr_Shift                                                16
#define cAf6_pla_out_psnpro_ha_ctrl_PlaOutPsnProLenCtr_MaxVal                                             0x7f
#define cAf6_pla_out_psnpro_ha_ctrl_PlaOutPsnProLenCtr_MinVal                                              0x0
#define cAf6_pla_out_psnpro_ha_ctrl_PlaOutPsnProLenCtr_RstVal                                              0x0

/*--------------------------------------
BitField Name: PlaOutPsnProPageCtr
BitField Type: RW
BitField Desc: there is 2 pages PSN location per PWID, depend on the
configuration of "PlaOutHspwPsnCtr", the engine will choice which the page to
encapsulate into the packet.
BitField Bits: [13]
--------------------------------------*/
#define cAf6_pla_out_psnpro_ha_ctrl_PlaOutPsnProPageCtr_Bit_Start                                           13
#define cAf6_pla_out_psnpro_ha_ctrl_PlaOutPsnProPageCtr_Bit_End                                             13
#define cAf6_pla_out_psnpro_ha_ctrl_PlaOutPsnProPageCtr_Mask                                            cBit13
#define cAf6_pla_out_psnpro_ha_ctrl_PlaOutPsnProPageCtr_Shift                                               13
#define cAf6_pla_out_psnpro_ha_ctrl_PlaOutPsnProPageCtr_MaxVal                                             0x1
#define cAf6_pla_out_psnpro_ha_ctrl_PlaOutPsnProPageCtr_MinVal                                             0x0
#define cAf6_pla_out_psnpro_ha_ctrl_PlaOutPsnProPageCtr_RstVal                                             0x0

/*--------------------------------------
BitField Name: PlaOutPsnProFlowCtr
BitField Type: RW
BitField Desc: Flow channel
BitField Bits: [11:0]
--------------------------------------*/
#define cAf6_pla_out_psnpro_ha_ctrl_PlaOutPsnProFlowCtr_Bit_Start                                            0
#define cAf6_pla_out_psnpro_ha_ctrl_PlaOutPsnProFlowCtr_Bit_End                                             11
#define cAf6_pla_out_psnpro_ha_ctrl_PlaOutPsnProFlowCtr_Mask                                          cBit11_0
#define cAf6_pla_out_psnpro_ha_ctrl_PlaOutPsnProFlowCtr_Shift                                                0
#define cAf6_pla_out_psnpro_ha_ctrl_PlaOutPsnProFlowCtr_MaxVal                                           0xfff
#define cAf6_pla_out_psnpro_ha_ctrl_PlaOutPsnProFlowCtr_MinVal                                             0x0
#define cAf6_pla_out_psnpro_ha_ctrl_PlaOutPsnProFlowCtr_RstVal                                             0x0

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061MODULEPLAREG_H_ */


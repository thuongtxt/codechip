/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : XC
 *
 * File        : Tha60210061ModuleXc.c
 *
 * Created Date: Aug 30, 2016
 *
 * Description : Module XC of 60210061
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210012/xc/Tha60210012ModuleXc.h"
#include "../../Tha60210051/poh/Tha60210051PohReg.h"
#include "../../Tha60210011/poh/Tha60210011ModulePoh.h"
#include "../ocn/Tha60210061ModuleOcnReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061ModuleXc
    {
    tTha60210012ModuleXc super;
    }tTha60210061ModuleXc;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60210011ModuleXcMethods m_Tha60210011ModuleXcOverride;

/* Save super implementation */
static const tTha60210011ModuleXcMethods *m_Tha60210011ModuleXcMethods = NULL;


/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 StartHoLineId(Tha60210011ModuleXc self)
    {
    AtUnused(self);
    return 1;
    }

static eAtRet UpsrLookupHoTableInit(Tha60210011ModuleXc self)
    {
    static const uint8 cNumStsPerLine = 48;
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    uint8 numLines = AtModuleSdhMaxLinesGet((AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh));
    ThaModulePoh modulePoh = (ThaModulePoh)AtDeviceModuleGet(device, cThaModulePoh);
    eAtRet ret = cAtOk;
    uint8 lineId;

    for (lineId = 0; lineId < numLines; lineId++)
        {
        uint8 sts_i;

        for (sts_i = 0; sts_i < cNumStsPerLine; sts_i++)
            {
            uint32 regAddr = Tha60210011ModulePohBaseAddress(modulePoh) + (uint32)cAf6Reg_upen_xxcfglks(lineId, sts_i);
            mModuleHwWrite(self, regAddr, cStsUnusedValue);
            }
        }

    return ret;
    }

static uint8 HwSts2SwStsOfStm4Get(uint8 hwSts, uint8 lineId)
    {
    hwSts = (uint8)(hwSts - (lineId % 4));
    return (uint8)((((hwSts % 16) / 4) * 3) + (hwSts / 16));
    }

static uint8 HwSts2SwStsOfStm1Get(uint8 hwSts)
    {
    return (uint8)(hwSts / 16);
    }

static uint8 HwSts2SwStsGet(Tha60210011ModuleXc self, uint8 hwSts, uint8 lineId)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    AtSdhLine line = AtModuleSdhLineGet((AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh), lineId);
    eAtSdhLineRate rate = AtSdhLineRateGet(line);

    if (rate == cAtSdhLineRateStm16)
        return m_Tha60210011ModuleXcMethods->HwSts2SwStsGet(self, hwSts, lineId);
    if (rate == cAtSdhLineRateStm4)
        return HwSts2SwStsOfStm4Get(hwSts, lineId);
    if (rate == cAtSdhLineRateStm1)
        return HwSts2SwStsOfStm1Get(hwSts);

    return cInvalidUint8;
    }
    
static uint32 RxSxcLineIdMask(Tha60210011ModuleXc self)
    {
    AtUnused(self);
    return cAf6_rxsxcramctl_RxSxcLineId_Mask;
    }

static uint8 RxSxcLineIdShift(Tha60210011ModuleXc self)
    {
    AtUnused(self);
    return cAf6_rxsxcramctl_RxSxcLineId_Shift;
    }

static uint32 TxSxcLineIdMask(Tha60210011ModuleXc self)
    {
    AtUnused(self);
    return cAf6_txsxcramctl_TxSxcLineId_Mask;
    }

static uint8 TxSxcLineIdShift(Tha60210011ModuleXc self)
    {
    AtUnused(self);
    return cAf6_txsxcramctl_TxSxcLineId_Shift;
    }

static void OverrideTha60210011ModuleXc(AtModuleXc self)
    {
    Tha60210011ModuleXc module = (Tha60210011ModuleXc)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011ModuleXcMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModuleXcOverride, mMethodsGet(module), sizeof(m_Tha60210011ModuleXcOverride));

        mMethodOverride(m_Tha60210011ModuleXcOverride, StartHoLineId);
        mMethodOverride(m_Tha60210011ModuleXcOverride, HwSts2SwStsGet);
        mMethodOverride(m_Tha60210011ModuleXcOverride, UpsrLookupHoTableInit);
        mMethodOverride(m_Tha60210011ModuleXcOverride, RxSxcLineIdMask);
        mMethodOverride(m_Tha60210011ModuleXcOverride, RxSxcLineIdShift);
        mMethodOverride(m_Tha60210011ModuleXcOverride, TxSxcLineIdMask);
        mMethodOverride(m_Tha60210011ModuleXcOverride, TxSxcLineIdShift);
        }

    mMethodsSet(module, &m_Tha60210011ModuleXcOverride);
    }

static void Override(AtModuleXc self)
    {
    OverrideTha60210011ModuleXc(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061ModuleXc);
    }

static AtModuleXc ObjectInit(AtModuleXc self, AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012ModuleXcObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleXc Tha60210061ModuleXcNew(AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleXc newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newModule, device);
    }


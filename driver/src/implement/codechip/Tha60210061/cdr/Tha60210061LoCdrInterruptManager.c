/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : Tha60210061LoCdrInterruptManager.c
 *
 * Created Date: Oct 24, 2016
 *
 * Description : LO CDR Interrupt
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../../default/sdh/ThaSdhVc.h"
#include "../../Tha60210031/cdr/Tha60210031CdrInterruptManager.h"
#include "../../Tha60210011/cdr/Tha60210011CdrInterruptManager.h"
#include "../../Tha60210031/cdr/Tha60210031CdrReg.h"
#include "../../Tha60210031/pdh/Tha60210031ModulePdh.h"
#include "../pdh/Tha60210061PdhDe1.h"
#include "../pdh/Tha60210061PdhDe3.h"
#include "Tha60210061LoCdrInterruptManager.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061LoCdrInterruptManager
    {
    tTha60210011LoCdrInterruptManager super;
    }tTha60210061LoCdrInterruptManager;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtInterruptManagerMethods             m_AtInterruptManagerOverride;
static tThaInterruptManagerMethods            m_ThaInterruptManagerOverride;
static tThaCdrInterruptManagerMethods         m_ThaCdrInterruptManagerOverride;

/* Save super implementation */
static const tThaCdrInterruptManagerMethods  *m_ThaCdrInterruptManagerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 NumSlices(ThaInterruptManager self)
    {
    AtUnused(self);
    return 2;
    }

static void InterruptOnIpCoreEnable(AtInterruptManager self, eBool enable, AtIpCore ipCore)
    {
    AtHal hal = AtIpCoreHalGet(ipCore);
    uint32 baseAddress = ThaInterruptManagerBaseAddress(mIntrManager(self));
    uint32 numSlices = mMethodsGet(mIntrManager(self))->NumSlices(mIntrManager(self));
    uint8 sliceId;

    for (sliceId = 0; sliceId < numSlices; sliceId++)
        {
        uint32 sliceBase = baseAddress + mMethodsGet(mIntrManager(self))->SliceOffset(mIntrManager(self), sliceId);
        AtHalWrite(hal, sliceBase + cAf6Reg_cdr_per_stsvc_intr_en_ctrl_Base, (enable) ? cBit26_0 : 0);
        }
    }

static ThaModulePdh ModulePdh(ThaCdrInterruptManager self)
    {
    AtModule cdrModule = AtInterruptManagerModuleGet((AtInterruptManager)self);
    return (ThaModulePdh)AtDeviceModuleGet(AtModuleDeviceGet(cdrModule), cAtModulePdh);
    }

static eBool IsStsOfStmPorts(uint8 sts27)
    {
    static const uint8 cStartStsOfSerialLine = 24;
    return (sts27 < cStartStsOfSerialLine) ? cAtTrue : cAtFalse;
    }

static ThaCdrController AcrDcrControllerFromHwIdOfStmPortsGet(ThaCdrInterruptManager self, uint8 slice, uint8 sts24, uint8 vt)
    {
    AtModule cdrModule = AtInterruptManagerModuleGet((AtInterruptManager)self);
    AtDevice device = AtModuleDeviceGet(cdrModule);
    ThaModuleSdh sdhModule = (ThaModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    AtSdhPath au;
    uint8 sts48;

    sts48 = (uint8)((slice & 0x1U)|(uint8)(sts24 << 1));

    au = (AtSdhPath)ThaModuleSdhAuPathFromHwIdGet(sdhModule, cThaModuleCdr, slice, sts24);
    return Tha6021CdrControllerFromAuPathGet(au, sts48, vt);
    }

static ThaCdrController AcrDcrControllerFromHwIdGet(ThaCdrInterruptManager self, uint8 slice, uint8 sts27, uint8 vt)
    {
    AtPdhDe1 de1;
    ThaModulePdh pdhModule;
    ThaCdrController controller;
    uint32 serialId;
    uint8 vtId, vtgId;

    if (IsStsOfStmPorts(sts27))
        return AcrDcrControllerFromHwIdOfStmPortsGet(self, slice, sts27, vt);

    vtgId = vt >> 2;
    vtId  = vt & 0x3;

    /* Serial line DS3/E3/EC1 */
    pdhModule = ModulePdh(self);
    serialId = Tha6021ModulePdhSerialIdFromHwIdGet(pdhModule, slice, sts27);
    controller = Tha6021CdrControllerFromSerialLineIdGet(self, serialId, vt);
    if (controller)
        return controller;

    /* DS1 LIU */
    de1 = Tha60210061De1LiuObjectFromHwId((AtModulePdh)pdhModule, slice, sts27, vtgId, vtId);
    if (de1)
        return ThaPdhDe1CdrControllerGet((ThaPdhDe1)de1);

    return NULL;
    }

static void OverrideAtInterruptManager(AtInterruptManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtInterruptManagerOverride, mMethodsGet(self), sizeof(m_AtInterruptManagerOverride));

        mMethodOverride(m_AtInterruptManagerOverride, InterruptOnIpCoreEnable);
        }

    mMethodsSet(self, &m_AtInterruptManagerOverride);
    }

static void OverrideThaInterruptManager(AtInterruptManager self)
    {
    ThaInterruptManager manager = (ThaInterruptManager)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInterruptManagerOverride, mMethodsGet(manager), sizeof(m_ThaInterruptManagerOverride));

        mMethodOverride(m_ThaInterruptManagerOverride, NumSlices);
        }

    mMethodsSet(manager, &m_ThaInterruptManagerOverride);
    }

static void OverrideThaCdrInterruptManager(AtInterruptManager self)
    {
    ThaCdrInterruptManager manager = (ThaCdrInterruptManager)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaCdrInterruptManagerMethods = mMethodsGet(manager);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaCdrInterruptManagerOverride, mMethodsGet(manager), sizeof(m_ThaCdrInterruptManagerOverride));

        mMethodOverride(m_ThaCdrInterruptManagerOverride, AcrDcrControllerFromHwIdGet);
        }

    mMethodsSet(manager, &m_ThaCdrInterruptManagerOverride);
    }

static void Override(AtInterruptManager self)
    {
    OverrideAtInterruptManager(self);
    OverrideThaInterruptManager(self);
    OverrideThaCdrInterruptManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061LoCdrInterruptManager);
    }

static AtInterruptManager Tha60210061LoCdrInterruptManagerObjectInit(AtInterruptManager self, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011LoCdrInterruptManagerObjectInit(self, module) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInterruptManager Tha60210061LoCdrInterruptManagerNew(AtModule module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInterruptManager newManager = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newManager == NULL)
        return NULL;

    /* Construct it */
    return Tha60210061LoCdrInterruptManagerObjectInit(newManager, module);
    }

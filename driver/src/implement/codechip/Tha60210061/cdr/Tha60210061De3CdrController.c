/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : Tha60210061De3CdrController.c
 *
 * Created Date: Sep 21, 2016
 *
 * Description : DE3 CDR Controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210031/cdr/Tha60210031De3CdrControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061De3CdrController
    {
    tTha60210031De3CdrController super;
    }tTha60210061De3CdrController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaCdrControllerMethods m_ThaCdrControllerOverride;

/* Super implement */
static const tThaCdrControllerMethods *m_ThaCdrControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddressBySlice(void)
    {
    return 0x0C00000;
    }

static uint32 ChannelDefaultOffset(ThaCdrController self)
    {
    uint32 defaultOffset = m_ThaCdrControllerMethods->ChannelDefaultOffset(self);
    return BaseAddressBySlice() + defaultOffset;
    }

static uint32 DefaultOffset(ThaCdrController self)
    {
    uint32 defaultOffset = m_ThaCdrControllerMethods->DefaultOffset(self);
    return defaultOffset + BaseAddressBySlice();
    }

static uint32 TimingCtrlOffset(ThaCdrController self)
    {
    return ThaModuleCdrTimingCtrlDe3Offset(self);
    }

static eAtRet TimingPacketLenInBitsSet(ThaCdrController self, uint32 packetLengthInBits)
    {
    uint32 regAddr, regVal;
    AtChannel de3Channel;
    eAtRet ret = m_ThaCdrControllerMethods->TimingPacketLenInBitsSet(self, packetLengthInBits);
    if (ret != cAtOk)
        return ret;

    /* Updated line type */
    de3Channel = ThaCdrControllerChannelGet(self);
    regAddr    = ThaModuleCdrEngineTimingCtrlRegister(ThaCdrControllerModuleGet(self)) + mMethodsGet(self)->TimingCtrlOffset(self);
    regVal     = ThaCdrControllerRead(self, regAddr, cThaModuleCdr);
    mRegFieldSet(regVal, cThaLineType, AtPdhDe3IsE3((AtPdhDe3)de3Channel) ? 4 : 5);
    ThaCdrControllerWrite(self, regAddr, regVal, cThaModuleCdr);

    return cAtOk;
    }

static void OverrideThaCdrController(ThaCdrController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaCdrControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaCdrControllerOverride, m_ThaCdrControllerMethods, sizeof(m_ThaCdrControllerOverride));

        mMethodOverride(m_ThaCdrControllerOverride, ChannelDefaultOffset);
        mMethodOverride(m_ThaCdrControllerOverride, DefaultOffset);
        mMethodOverride(m_ThaCdrControllerOverride, TimingPacketLenInBitsSet);
        mMethodOverride(m_ThaCdrControllerOverride, TimingCtrlOffset);
        }

    mMethodsSet(self, &m_ThaCdrControllerOverride);
    }

static void Override(ThaCdrController self)
    {
    OverrideThaCdrController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061De3CdrController);
    }

static ThaCdrController ObjectInit(ThaCdrController self, uint32 engineId, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031De3CdrControllerObjectInit(self, engineId, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaCdrController Tha60210061De3CdrControllerNew(uint32 engineId, AtChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaCdrController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, engineId, channel);
    }

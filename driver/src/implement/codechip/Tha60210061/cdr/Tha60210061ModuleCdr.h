/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : Tha60210061ModuleCdr.h
 * 
 * Created Date: Aug 22, 2016
 *
 * Description : Tha60210061ModuleCdr declarations
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061MODULECDR_H_
#define _THA60210061MODULECDR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/cdr/ThaModuleCdr.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void Tha60210061ModuleCdrDe1LineModeControlDefault(ThaModuleCdr self);
void Tha60210061ModuleCdrDe1LineTypeAndVtModeSet(ThaModuleCdr self);
uint32 Tha60210061ModuleCdrStartVersionShouldEnableJA(ThaModuleCdr self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061MODULECDR_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : Tha60210061Ec1HoVcCdrController.c
 *
 * Created Date: Jul 3, 2017
 *
 * Description : HO CDR controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../Tha60210011/cdr/controllers/Tha60210011HoLineVcCdrControllerInternal.h"
#include "../../sdh/Tha60210061ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061Ec1HoVcCdrController
    {
    tTha60210011HoLineVcCdrController super;
    }tTha60210061Ec1HoVcCdrController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaCdrControllerMethods           m_ThaCdrControllerOverride;

/* Super implement */
static const tThaCdrControllerMethods           *m_ThaCdrControllerMethods = NULL;


/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtInterruptManager InterruptManager(ThaCdrController self)
    {
    AtModule cdrModule = (AtModule)ThaCdrControllerModuleGet(self);
    return AtModuleInterruptManagerAtIndexGet(cdrModule, 0); /* LO CDR */
    }

static void OverrideThaCdrController(ThaCdrController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaCdrControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaCdrControllerOverride, m_ThaCdrControllerMethods, sizeof(m_ThaCdrControllerOverride));

        mMethodOverride(m_ThaCdrControllerOverride, InterruptManager);
        }

    mMethodsSet(self, &m_ThaCdrControllerOverride);
    }

static void Override(ThaCdrController self)
    {
    OverrideThaCdrController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061Ec1HoVcCdrController);
    }

static ThaCdrController ObjectInit(ThaCdrController self, uint32 engineId, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011HoLineVcCdrControllerObjectInit(self, engineId, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaCdrController Tha60210061Ec1HoVcCdrControllerNew(uint32 engineId, AtChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaCdrController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, engineId, channel);
    }

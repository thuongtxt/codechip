/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : Tha60210061LoCdrInterruptManager.h
 * 
 * Created Date: Oct 24, 2016
 *
 * Description : LO CDR Interrupt
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061LOCDRINTERRUPTMANAGER_H_
#define _THA60210061LOCDRINTERRUPTMANAGER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtInterruptManager Tha60210061LoCdrInterruptManagerNew(AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061LOCDRINTERRUPTMANAGER_H_ */


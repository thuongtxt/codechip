/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : Tha60210061ModuleCdr.c
 *
 * Created Date: Jul 6, 2016
 *
 * Description : 60210061 module CDR
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../../default/cdr/ThaModuleCdrStmReg.h"
#include "../../Tha60210012/cdr/Tha60210012ModuleCdr.h"
#include "../../Tha60210031/cdr/Tha60210031ModuleCdr.h"
#include "../pdh/Tha60210061ModulePdh.h"
#include "../sdh/Tha60210061ModuleSdh.h"

#include "Tha60210061ModuleCdr.h"
#include "Tha60210061LoCdrInterruptManager.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define cMaxVtgNum 7
#define cMaxVtNum 4
#define cStartStsDe1Liu 26

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061ModuleCdr
    {
    tTha60210012ModuleCdr super;
    }tTha60210061ModuleCdr;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleCdrMethods         m_ThaModuleCdrOverride;
static tTha60150011ModuleCdrMethods m_Tha60150011ModuleCdrOverride;
static tAtModuleMethods             m_AtModuleOverride;

/* Save super implementation */
static const tThaModuleCdrMethods   *m_ThaModuleCdrMethods = NULL;
static const tTha60150011ModuleCdrMethods *m_Tha60150011ModuleCdrMethods = NULL;
static const tAtModuleMethods        *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 StartVersionSupportInterrupt(ThaModuleCdr self)
    {
    AtUnused(self);
    return 0;
    }

static ThaCdrController De3CdrControllerCreate(ThaModuleCdr self, AtPdhDe3 de3)
    {
    return Tha60210061De3CdrControllerNew(ThaModuleCdrStmEngineIdOfDe3(self, de3), (AtChannel)de3);
    }

static ThaCdrController VcDe3CdrControllerCreate(ThaModuleCdr self, AtPdhDe3 de3)
    {
    return m_ThaModuleCdrMethods->VcDe3CdrControllerCreate(self, de3);
    }

static uint32 LineModeCtrlOffset(Tha60150011ModuleCdr self, uint32 sliceOffset, uint8 hwStsId, uint8 partId)
    {
    /* In one slice:
     * STS: 0,2,4,...,22  -> group = 0
     * STS: 1,3,5,...,23  -> group = 1
     * STS: 24, 25, 26    -> group = 2
     */
    if (hwStsId < 24)
        return m_Tha60150011ModuleCdrMethods->LineModeCtrlOffset(self, sliceOffset, hwStsId, partId);

    /* User group ID = 2 for other STSs */
    return (sliceOffset + 2 + ThaModuleCdrPartOffset((ThaModuleCdr)self, partId));
    }

static uint8 LocalHwStsInGroup12(Tha60150011ModuleCdr self, uint8 hwSts)
    {
    AtUnused(self);
    if (hwSts < 24)
        return hwSts / 2;

    return (uint8)(hwSts - 24);
    }

static void LineModeControlDefaultPerVtg(ThaModuleCdr self, uint8 slice, uint8 hwSts, uint8 vtgId)
    {
    const uint8 cLiuPdhGroup = 2;
    uint32 regAddr;
    uint32 longReg[cThaLongRegMaxSize];
    uint8 hwStsLocalInGroup12 = LocalHwStsInGroup12((Tha60150011ModuleCdr)self, hwSts);
    AtIpCore core = AtDeviceIpCoreGet(AtModuleDeviceGet((AtModule)self), 0);
    uint32 lineModeCtrlOffset = Tha60210011ModuleCdrLoSliceOffset(self, slice);

    /* slice 0: DS1,
     * slice 1: E1 */
    regAddr = mMethodsGet(self)->CDRLineModeControlRegister(self) + lineModeCtrlOffset + cLiuPdhGroup;
    mModuleHwLongRead(self, regAddr, longReg, cThaLongRegMaxSize, core);
    mFieldIns(&longReg[hwStsLocalInGroup12 / 4],
              cThaStsVtTypeMask(hwStsLocalInGroup12, vtgId),
              cThaStsVtTypeShift(hwStsLocalInGroup12, vtgId),
              (slice == 0) ? 1 : 0);
    mModuleHwLongWrite(self, regAddr, longReg, cThaLongRegMaxSize, core);
    }

static uint32 SdhVcCdrIdCalculate(uint8 hwStsId, uint8 vtgId, uint8 vtId)
    {
    return (uint32)((hwStsId * 32UL) + (vtgId * 4UL) + vtId);
    }

static void AcrEngineLineTypeSet(ThaModuleCdr self, uint8 slice, uint8 hwStsId, uint8 vtgId, uint8 vtId, uint8 lineType)
    {
    uint32 regAddr, regVal, offset;

    offset = SdhVcCdrIdCalculate(hwStsId, vtgId, vtId) + Tha60210011ModuleCdrLoSliceOffset(self, slice);
    regAddr = mMethodsGet(self)->EngineTimingCtrlRegister(self) + offset;
    regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cThaLineType, lineType);
    mModuleHwWrite(self, regAddr, regVal);
    }

static void MapVtModeSet(ThaModuleCdr self, uint8 slice, uint8 hwStsId, uint8 vtgId, uint8 vtId)
    {
    uint32 regAddr, regVal, offset;
    ThaModuleCdrStm stmCdrModule = (ThaModuleCdrStm)self;

    offset = SdhVcCdrIdCalculate(hwStsId, vtgId, vtId) + Tha60210011ModuleCdrLoSliceOffset(self, slice);
    regAddr = mMethodsGet(stmCdrModule)->VtTimingControlReg(stmCdrModule) + offset;
    regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cThaMapVtMd, 1); /* Payload DS1/E1 mode */
    mModuleHwWrite(self, regAddr, regVal);
    }

static eBool Vc1xCdrControllerShouldInitAfterCreate(ThaModuleCdr self, AtSdhVc vc)
    {
    if (Tha60210061ModuleSdhChannelIsFakeVc((AtSdhChannel)vc))
        return cAtFalse;

    return m_ThaModuleCdrMethods->Vc1xCdrControllerShouldInitAfterCreate(self, vc);
    }

static void E1LineTypeAndVtModeSet(ThaModuleCdr self)
    {
    uint8 vtg_i, vt_i;

    for (vtg_i = 0; vtg_i < cMaxVtgNum; vtg_i++)
        {
        for (vt_i = 0; vt_i < (cMaxVtNum - 1); vt_i++)
            {
            AcrEngineLineTypeSet(self, 1, cStartStsDe1Liu, vtg_i, vt_i, 0);
            MapVtModeSet(self, 1, cStartStsDe1Liu, vtg_i, vt_i);
            }
        }
    }

static void DS1LineTypeAndVtModeSet(ThaModuleCdr self)
    {
    uint8 vtg_i, vt_i;

    for (vtg_i = 0; vtg_i < cMaxVtgNum; vtg_i++)
        {
        for (vt_i = 0; vt_i < cMaxVtNum; vt_i++)
            {
            AcrEngineLineTypeSet(self, 0, cStartStsDe1Liu, vtg_i, vt_i, 1);
            MapVtModeSet(self, 0, cStartStsDe1Liu, vtg_i, vt_i);
            }
        }
    }

static AtInterruptManager InterruptManagerCreate(AtModule self, uint32 managerIndex)
    {
    if (!ThaModuleCdrInterruptIsSupported((ThaModuleCdr)self))
        return NULL;

    if (managerIndex == 0)
        return Tha60210061LoCdrInterruptManagerNew(self);

    return m_AtModuleMethods->InterruptManagerCreate(self, managerIndex);
    }

static uint32 StartVersionShouldEnableJA(void)
    {
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x6, 0x00);
    }

static eBool ShouldEnableJitterAttenuator(ThaModuleCdr self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    uint32 supportedJaVersion = StartVersionShouldEnableJA();

    if (hwVersion >= supportedJaVersion)
        return cAtTrue;

    return cAtFalse;
    }

static uint32 EngineIdOfHoVcInEc1(AtSdhVc vc)
    {
    uint8 slice, hwSts;
    ThaSdhChannel2HwMasterStsId((AtSdhChannel)vc, cThaModuleCdr, &slice, &hwSts);

    return (uint32)(hwSts * 32);
    }

static ThaCdrController HoVcCdrControllerCreate(ThaModuleCdr self, AtSdhVc vc)
    {
    AtDevice device = (AtDevice)AtModuleDeviceGet((AtModule)self);
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);

    if (!Tha60210061ModuleSdhChannelIsInEc1Line(sdhModule, (AtSdhChannel)vc))
        return m_ThaModuleCdrMethods->HoVcCdrControllerCreate(self, vc);

    return Tha60210061Ec1HoVcCdrControllerNew(EngineIdOfHoVcInEc1(vc), (AtChannel)vc);
    }

static void OverrideThaModuleCdr(AtModule self)
    {
    ThaModuleCdr cdrModule = (ThaModuleCdr)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleCdrMethods = mMethodsGet(cdrModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleCdrOverride, m_ThaModuleCdrMethods, sizeof(m_ThaModuleCdrOverride));

        mMethodOverride(m_ThaModuleCdrOverride, StartVersionSupportInterrupt);
        mMethodOverride(m_ThaModuleCdrOverride, VcDe3CdrControllerCreate);
        mMethodOverride(m_ThaModuleCdrOverride, Vc1xCdrControllerShouldInitAfterCreate);
        mMethodOverride(m_ThaModuleCdrOverride, ShouldEnableJitterAttenuator);
        mMethodOverride(m_ThaModuleCdrOverride, HoVcCdrControllerCreate);
        mMethodOverride(m_ThaModuleCdrOverride, De3CdrControllerCreate);
        }

    mMethodsSet(cdrModule, &m_ThaModuleCdrOverride);
    }

static void OverrideTha60150011ModuleCdr(AtModule self)
    {
    Tha60150011ModuleCdr cdrModule = (Tha60150011ModuleCdr)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60150011ModuleCdrMethods = mMethodsGet(cdrModule);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60150011ModuleCdrOverride, m_Tha60150011ModuleCdrMethods, sizeof(m_Tha60150011ModuleCdrOverride));

        mMethodOverride(m_Tha60150011ModuleCdrOverride, LineModeCtrlOffset);
        mMethodOverride(m_Tha60150011ModuleCdrOverride, LocalHwStsInGroup12);
        }

    mMethodsSet(cdrModule, &m_Tha60150011ModuleCdrOverride);
    }

static void OverrideAtModuleCdr(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, InterruptManagerCreate);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModuleCdr(self);
    OverrideThaModuleCdr(self);
    OverrideTha60150011ModuleCdr(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061ModuleCdr);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012ModuleCdrObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210061ModuleCdrNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

void Tha60210061ModuleCdrDe1LineModeControlDefault(ThaModuleCdr self)
    {
    uint8 vtg_i;

    /* STS 26 is used for DE1 */
    for (vtg_i = 0; vtg_i < cMaxVtgNum; vtg_i++)
        {
        LineModeControlDefaultPerVtg(self, 0, cStartStsDe1Liu, vtg_i);
        LineModeControlDefaultPerVtg(self, 1, cStartStsDe1Liu, vtg_i);
        }
    }

void Tha60210061ModuleCdrDe1LineTypeAndVtModeSet(ThaModuleCdr self)
    {
    E1LineTypeAndVtModeSet(self);
    DS1LineTypeAndVtModeSet(self);
    }

uint32 Tha60210061ModuleCdrStartVersionShouldEnableJA(ThaModuleCdr self)
    {
    if (self)
        return StartVersionShouldEnableJA();

    return cInvalidUint32;
    }

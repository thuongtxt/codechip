/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Concate
 * 
 * File        : Tha60210061ModuleConcate.h
 * 
 * Created Date: Dec 6, 2016
 *
 * Description : Tha60210061ModuleConcate
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061MODULECONCATE_H_
#define _THA60210061MODULECONCATE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210012/concate/Tha60210012ModuleConcate.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210061ModuleConcate * Tha60210061ModuleConcate;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleConcate Tha60210061ModuleConcateNew(AtDevice device);
uint8 Tha60210061ModuleConcateHwStsAllocate(Tha60210012ModuleConcate self, uint8 hwSlice, uint8 hwStsInSlice, uint8 hwVtId);
eAtRet Tha60210061ModuleConcateHwStsDeAllocate(Tha60210012ModuleConcate self, uint8 hwSlice, uint8 hwStsInSlice, uint8 hwVtId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061MODULECONCATE_H_ */


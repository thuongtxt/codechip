/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CCI0061_RD_VCAT_H_
#define _AF6_REG_AF6CCI0061_RD_VCAT_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Version ID Register
Reg Addr   : 0x00000
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is version of VCAT block.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_version_pen_Base                                                                0x00000

/*--------------------------------------
BitField Name: year
BitField Type: RO
BitField Desc: year
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_vcat5g_version_pen_year_Mask                                                            cBit15_12
#define cAf6_vcat5g_version_pen_year_Shift                                                                  12

/*--------------------------------------
BitField Name: week
BitField Type: RO
BitField Desc: week number
BitField Bits: [11:04]
--------------------------------------*/
#define cAf6_vcat5g_version_pen_week_Mask                                                             cBit11_4
#define cAf6_vcat5g_version_pen_week_Shift                                                                   4

/*--------------------------------------
BitField Name: number
BitField Type: RO
BitField Desc: number in week
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_vcat5g_version_pen_number_Mask                                                            cBit3_0
#define cAf6_vcat5g_version_pen_number_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Master Configuration
Reg Addr   : 0x00001
Reg Formula: 
    Where  : 
Reg Desc   : 
This register control global configuration for MAP.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_master_pen_Base                                                                 0x00001

/*--------------------------------------
BitField Name: VcatLoop
BitField Type: R/W
BitField Desc: Non-VCAT Mode
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_vcat5g_master_pen_VcatLoop_Mask                                                             cBit2
#define cAf6_vcat5g_master_pen_VcatLoop_Shift                                                                2

/*--------------------------------------
BitField Name: NonVcatMd
BitField Type: R/W
BitField Desc: Non-VCAT Mode
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_vcat5g_master_pen_NonVcatMd_Mask                                                            cBit1
#define cAf6_vcat5g_master_pen_NonVcatMd_Shift                                                               1

/*--------------------------------------
BitField Name: Active
BitField Type: R/W
BitField Desc: Engine Active
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_vcat5g_master_pen_Active_Mask                                                               cBit0
#define cAf6_vcat5g_master_pen_Active_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Write Access Data Holding 1
Reg Addr   : 0x00002
Reg Formula: 
    Where  : 
Reg Desc   : 
Used to write configuration or status that is greater than 32-bit wide.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_wrhold1_pen_Base                                                                0x00002

/*--------------------------------------
BitField Name: WrDat_Hold1
BitField Type: R/W
BitField Desc:
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_vcat5g_wrhold1_pen_WrDat_Hold1_Mask                                                      cBit31_0
#define cAf6_vcat5g_wrhold1_pen_WrDat_Hold1_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Write Access Data Holding 2
Reg Addr   : 0x00003
Reg Formula: 
    Where  : 
Reg Desc   : 
Used to write configuration or status that is greater than 32-bit wide.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_wrhold2_pen_Base                                                                0x00003

/*--------------------------------------
BitField Name: WrDat_Hold2
BitField Type: R/W
BitField Desc:
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_vcat5g_wrhold2_pen_WrDat_Hold2_Mask                                                      cBit31_0
#define cAf6_vcat5g_wrhold2_pen_WrDat_Hold2_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Read Access Data Holding 1
Reg Addr   : 0x00004
Reg Formula: 
    Where  : 
Reg Desc   : 
Used to read configuration or status that is greater than 32-bit wide.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_rdhold1_pen_Base                                                                0x00004

/*--------------------------------------
BitField Name: RdDat_Hold1
BitField Type: R/W
BitField Desc:
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_vcat5g_rdhold1_pen_RdDat_Hold1_Mask                                                      cBit31_0
#define cAf6_vcat5g_rdhold1_pen_RdDat_Hold1_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Read Access Data Holding 2
Reg Addr   : 0x00005
Reg Formula: 
    Where  : 
Reg Desc   : 
Used to read configuration or status that is greater than 32-bit wide.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_rdhold2_pen_Base                                                                0x00005

/*--------------------------------------
BitField Name: RdDat_Hold2
BitField Type: R/W
BitField Desc:
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_vcat5g_rdhold2_pen_RdDat_Hold2_Mask                                                      cBit31_0
#define cAf6_vcat5g_rdhold2_pen_RdDat_Hold2_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Source VC4 Mode Configuration
Reg Addr   : 0x21040
Reg Formula: 
    Where  : 
Reg Desc   : 
Source VC4 Mode Enable Control. For VC4 mode, if STS master is n,
the two slave-STSs will be n+64 and n+128 (where n = 0 - 63)

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_so_vc4md_pen_Base                                                               0x21040

/*--------------------------------------
BitField Name: So_VC4_Ena
BitField Type: R/W
BitField Desc: Source VC4 Mode Enable
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_vcat5g_so_vc4md_pen_So_VC4_Ena_Mask                                                      cBit15_0
#define cAf6_vcat5g_so_vc4md_pen_So_VC4_Ena_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Source Mode Configuration
Reg Addr   : 0x21200-0x2122f
Reg Formula: 0x21200 + $sts_id
    Where  : 
           + $sts_id(0-47)
Reg Desc   : 
This register is used to config timing mode for VCAT Source side.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_so_ctrl_pen_Base                                                                0x21200

/*--------------------------------------
BitField Name: So_HoMd
BitField Type: R/W
BitField Desc: 00: SPE-VC3 Mode, 01: DS3 Mode, 10: E3 Mode, 11: Lo-order Mode
(DS1 or E1 or VT1.5 or VT2)
BitField Bits: [17:16]
--------------------------------------*/
#define cAf6_vcat5g_so_ctrl_pen_So_HoMd_Mask                                                         cBit17_16
#define cAf6_vcat5g_so_ctrl_pen_So_HoMd_Shift                                                               16

/*--------------------------------------
BitField Name: So_LoMd_Grp_6
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_6_Mask                                                   cBit13_12
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_6_Shift                                                         12

/*--------------------------------------
BitField Name: So_LoMd_Grp_5
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_5_Mask                                                   cBit11_10
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_5_Shift                                                         10

/*--------------------------------------
BitField Name: So_LoMd_Grp_4
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [09:08]
--------------------------------------*/
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_4_Mask                                                     cBit9_8
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_4_Shift                                                          8

/*--------------------------------------
BitField Name: So_LoMd_Grp_3
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [07:06]
--------------------------------------*/
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_3_Mask                                                     cBit7_6
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_3_Shift                                                          6

/*--------------------------------------
BitField Name: So_LoMd_Grp_2
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [05:04]
--------------------------------------*/
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_2_Mask                                                     cBit5_4
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_2_Shift                                                          4

/*--------------------------------------
BitField Name: So_LoMd_Grp_1
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [03:02]
--------------------------------------*/
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_1_Mask                                                     cBit3_2
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_1_Shift                                                          2

/*--------------------------------------
BitField Name: So_LoMd_Grp_0
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_0_Mask                                                     cBit1_0
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_0_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Source E1-Unframed Mode Configuration
Reg Addr   : 0x21400-0x2142f
Reg Formula: 0x21400 + $sts_id
    Where  : 
           + $sts_id(0-47)
Reg Desc   : 
This register is used to config mode for VCAT Source side.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_so_unfrm_pen_Base                                                               0x21400

/*--------------------------------------
BitField Name: So_E1Unfrm
BitField Type: R/W
BitField Desc: 1: E1-Unframed Mode, 0: E1-Framed Mode (default)
BitField Bits: [27:00]
--------------------------------------*/
#define cAf6_vcat5g_so_unfrm_pen_So_E1Unfrm_Mask                                                      cBit27_0
#define cAf6_vcat5g_so_unfrm_pen_So_E1Unfrm_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Source Member Control Configuration
Reg Addr   : 0x22000-0x225ff
Reg Formula: 0x22000 + $sts_id*32 + $vtg_id*4 + $vt_id
    Where  : 
           + $sts_id(0-47), $vtg_id(0-6), $vt_id(0-3)
Reg Desc   : 
This register is used to control engine VCAT Source side.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_so_memctrl_pen_Base                                                             0x22000

/*--------------------------------------
BitField Name: So_Mem_Ena
BitField Type: R/W
BitField Desc: 1: Enable Member, 0: Disable Member
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_vcat5g_so_memctrl_pen_So_Mem_Ena_Mask                                                      cBit16
#define cAf6_vcat5g_so_memctrl_pen_So_Mem_Ena_Shift                                                         16

/*--------------------------------------
BitField Name: So_G8040_Md
BitField Type: R/W
BitField Desc: 1: ITU G.8040 Mode, 0: ITU G.804 Mode
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_vcat5g_so_memctrl_pen_So_G8040_Md_Mask                                                     cBit15
#define cAf6_vcat5g_so_memctrl_pen_So_G8040_Md_Shift                                                        15

/*--------------------------------------
BitField Name: So_Vcat_Md
BitField Type: R/W
BitField Desc: 1: VCAT Mode, 0: non-VCAT Mode
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_vcat5g_so_memctrl_pen_So_Vcat_Md_Mask                                                      cBit14
#define cAf6_vcat5g_so_memctrl_pen_So_Vcat_Md_Shift                                                         14

/*--------------------------------------
BitField Name: So_Lcas_Md
BitField Type: R/W
BitField Desc: LCAS Enable
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_vcat5g_so_memctrl_pen_So_Lcas_Md_Mask                                                      cBit13
#define cAf6_vcat5g_so_memctrl_pen_So_Lcas_Md_Shift                                                         13

/*--------------------------------------
BitField Name: So_Nms_Cmd
BitField Type: R/W
BitField Desc: Network Management Command
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_vcat5g_so_memctrl_pen_So_Nms_Cmd_Mask                                                      cBit12
#define cAf6_vcat5g_so_memctrl_pen_So_Nms_Cmd_Shift                                                         12

/*--------------------------------------
BitField Name: So_VcgId
BitField Type: R/W
BitField Desc: VCG ID 0-511
BitField Bits: [08:00]
--------------------------------------*/
#define cAf6_vcat5g_so_memctrl_pen_So_VcgId_Mask                                                       cBit8_0
#define cAf6_vcat5g_so_memctrl_pen_So_VcgId_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Source Member Status
Reg Addr   : 0x23000-0x235ff
Reg Formula: 0x23000 + $sts_id*32 + $vtg_id*4 + $vt_id
    Where  : 
           + $sts_id(0-47), $vtg_id(0-6), $vt_id(0-3)
Reg Desc   : 
This register is used to show status of member at Source side.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_so_memsta_pen_Base                                                              0x23000

/*--------------------------------------
BitField Name: So_Mem_Fsm
BitField Type: RO
BitField Desc: Member FSM
BitField Bits: [18:16]
--------------------------------------*/
#define cAf6_vcat5g_so_memsta_pen_So_Mem_Fsm_Mask                                                    cBit18_16
#define cAf6_vcat5g_so_memsta_pen_So_Mem_Fsm_Shift                                                          16

/*--------------------------------------
BitField Name: So_Mem_Sta
BitField Type: RO
BitField Desc: Member Status
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_vcat5g_so_memsta_pen_So_Mem_Sta_Mask                                                       cBit15
#define cAf6_vcat5g_so_memsta_pen_So_Mem_Sta_Shift                                                          15

/*--------------------------------------
BitField Name: So_Mem_Act
BitField Type: RO
BitField Desc: Member Active
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_vcat5g_so_memsta_pen_So_Mem_Act_Mask                                                       cBit14
#define cAf6_vcat5g_so_memsta_pen_So_Mem_Act_Shift                                                          14

/*--------------------------------------
BitField Name: So_Mem_TxSeq
BitField Type: RO
BitField Desc: Source Member Sequence Transmitted
BitField Bits: [13:07]
--------------------------------------*/
#define cAf6_vcat5g_so_memsta_pen_So_Mem_TxSeq_Mask                                                   cBit13_7
#define cAf6_vcat5g_so_memsta_pen_So_Mem_TxSeq_Shift                                                         7

/*--------------------------------------
BitField Name: So_Mem_AcpSeq
BitField Type: RO
BitField Desc: Source Member Accept Sequence
BitField Bits: [06:00]
--------------------------------------*/
#define cAf6_vcat5g_so_memsta_pen_So_Mem_AcpSeq_Mask                                                   cBit6_0
#define cAf6_vcat5g_so_memsta_pen_So_Mem_AcpSeq_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Source Transmit Sequence Configuration
Reg Addr   : 0x24000-0x245ff
Reg Formula: 0x24000 + $sts_id*32 + $vtg_id*4 + $vt_id
    Where  : 
           + $sts_id(0-47), $vtg_id(0-6), $vt_id(0-3)
Reg Desc   : 
This register is used to assign sequence transmitted for VCAT mode only.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_so_txseqmem_pen_Base                                                            0x24000

/*--------------------------------------
BitField Name: So_TxSeqEna
BitField Type: R/W
BitField Desc: 0: Disable config, 1: Enable config
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_vcat5g_so_txseqmem_pen_So_TxSeqEna_Mask                                                     cBit8
#define cAf6_vcat5g_so_txseqmem_pen_So_TxSeqEna_Shift                                                        8

/*--------------------------------------
BitField Name: So_TxSeq
BitField Type: R/W
BitField Desc: Source Member Sequence Transmitted
BitField Bits: [06:00]
--------------------------------------*/
#define cAf6_vcat5g_so_txseqmem_pen_So_TxSeq_Mask                                                      cBit6_0
#define cAf6_vcat5g_so_txseqmem_pen_So_TxSeq_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Output OC-27 STS Connection Memory
Reg Addr   : 0x25000-0x2502f
Reg Formula: 0x25000 + $sts_id
    Where  : 
           + $sts_id(0-47)
Reg Desc   : 
Connection Memory for Output 2-Slice OC-27

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_so_stsxc_pen_Base                                                               0x25000

/*--------------------------------------
BitField Name: OC24_OutSlice
BitField Type: R/W
BitField Desc: Output Slice 0-1
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_vcat5g_so_stsxc_pen_OC24_OutSlice_Mask                                                      cBit8
#define cAf6_vcat5g_so_stsxc_pen_OC24_OutSlice_Shift                                                         8

/*--------------------------------------
BitField Name: OC27_OutSTS
BitField Type: R/W
BitField Desc: Output STS 0-26
BitField Bits: [04:00]
--------------------------------------*/
#define cAf6_vcat5g_so_stsxc_pen_OC27_OutSTS_Mask                                                      cBit4_0
#define cAf6_vcat5g_so_stsxc_pen_OC27_OutSTS_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Input OC-27 STS Connection Memory
Reg Addr   : 0x42000-0x4203f
Reg Formula: 0x42000 + $slice_id*32 + $sts_id
    Where  : 
           + $slice_id(0-1)
           + $sts_id(0-26)
Reg Desc   : 
Connection Memory for Input 2-Slice OC-27 to OC-48

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sk_stsxc_pen_Base                                                               0x42000

/*--------------------------------------
BitField Name: OC48_OutSTS
BitField Type: R/W
BitField Desc: Output STS 0-47
BitField Bits: [05:00]
--------------------------------------*/
#define cAf6_vcat5g_sk_stsxc_pen_OC48_OutSTS_Mask                                                      cBit5_0
#define cAf6_vcat5g_sk_stsxc_pen_OC48_OutSTS_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Sink Slice 0 Mode Configuration
Reg Addr   : 0x40800-0x4081a
Reg Formula: 0x40800 + $sts_id
    Where  : 
           + $sts_id(0-26)
Reg Desc   : 
This register is used to config timing mode for VCAT Sink side.
HDL_PATH     : isink.itiming.itiming[0].ctrl_buf.ram1.ram.ram[$sts_id]

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sk_sl0_ctrl_pen_Base                                                            0x40800

/*--------------------------------------
BitField Name: Sk_sl0_HoMd
BitField Type: R/W
BitField Desc: 00: SPE-VC3 Mode, 01: DS3 Mode, 10: E3 Mode, 11: Lo-order Mode
(DS1 or E1 or VT1.5 or VT2)
BitField Bits: [17:16]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_HoMd_Mask                                                 cBit17_16
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_HoMd_Shift                                                       16

/*--------------------------------------
BitField Name: Sk_sl0_LoMd_Grp_6
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_6_Mask                                           cBit13_12
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_6_Shift                                                 12

/*--------------------------------------
BitField Name: Sk_sl0_LoMd_Grp_5
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_5_Mask                                           cBit11_10
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_5_Shift                                                 10

/*--------------------------------------
BitField Name: Sk_sl0_LoMd_Grp_4
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [09:08]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_4_Mask                                             cBit9_8
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_4_Shift                                                  8

/*--------------------------------------
BitField Name: Sk_sl0_LoMd_Grp_3
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [07:06]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_3_Mask                                             cBit7_6
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_3_Shift                                                  6

/*--------------------------------------
BitField Name: Sk_sl0_LoMd_Grp_2
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [05:04]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_2_Mask                                             cBit5_4
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_2_Shift                                                  4

/*--------------------------------------
BitField Name: Sk_sl0_LoMd_Grp_1
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [03:02]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_1_Mask                                             cBit3_2
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_1_Shift                                                  2

/*--------------------------------------
BitField Name: Sk_sl0_LoMd_Grp_0
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_0_Mask                                             cBit1_0
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_0_Shift                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Sink Slice 0 E1-Unframed Mode Configuration
Reg Addr   : 0x40840-0x4085a
Reg Formula: 0x40840 + $sts_id
    Where  : 
           + $sts_id(0-26)
Reg Desc   : 
This register is used to config mode for VCAT Sink side.
HDL_PATH     : isink.itiming.itiming[0].unfrm_buf.ram.ram[$sts_id]

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sk_sl0_unfrm_pen_Base                                                           0x40840

/*--------------------------------------
BitField Name: Sk_sl0_E1Unfrm
BitField Type: R/W
BitField Desc: 1: E1-Unframed Mode, 0: E1-Framed Mode (default)
BitField Bits: [27:00]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl0_unfrm_pen_Sk_sl0_E1Unfrm_Mask                                              cBit27_0
#define cAf6_vcat5g_sk_sl0_unfrm_pen_Sk_sl0_E1Unfrm_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Sink Slice 0 Input Mux Configuration
Reg Addr   : 0x40880-0x4089a
Reg Formula: 0x40880 + $sts_id
    Where  : 
           + $sts_id(0-26)
Reg Desc   : 
This register is used to select bus running at Sink side input.
HDL_PATH     : isink.itiming.itiming[0].imux.selector.ram1.array.ram[$sts_id]

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sk_sl0_mux_pen_Base                                                             0x40880

/*--------------------------------------
BitField Name: Sk_sl0_Mux
BitField Type: R/W
BitField Desc: 1: Select from PDH bus (DS3/E3/DS1/E1), 0: Select from STS/VT Bus
BitField Bits: [27:00]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl0_mux_pen_Sk_sl0_Mux_Mask                                                    cBit27_0
#define cAf6_vcat5g_sk_sl0_mux_pen_Sk_sl0_Mux_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Sink Slice 1 Mode Configuration
Reg Addr   : 0x40900-0x4091a
Reg Formula: 0x40900 + $sts_id
    Where  : 
           + $sts_id(0-26)
Reg Desc   : 
This register is used to config timing mode for VCAT Sink side.
HDL_PATH     : isink.itiming.itiming[1].ctrl_buf.ram1.ram.ram[$sts_id]

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sk_sl1_ctrl_pen_Base                                                            0x40900

/*--------------------------------------
BitField Name: Sk_sl1_HoMd
BitField Type: R/W
BitField Desc: 00: SPE-VC3 Mode, 01: DS3 Mode, 10: E3 Mode, 11: Lo-order Mode
(DS1 or E1 or VT1.5 or VT2)
BitField Bits: [17:16]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_HoMd_Mask                                                 cBit17_16
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_HoMd_Shift                                                       16

/*--------------------------------------
BitField Name: Sk_sl1_LoMd_Grp_6
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_6_Mask                                           cBit13_12
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_6_Shift                                                 12

/*--------------------------------------
BitField Name: Sk_sl1_LoMd_Grp_5
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_5_Mask                                           cBit11_10
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_5_Shift                                                 10

/*--------------------------------------
BitField Name: Sk_sl1_LoMd_Grp_4
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [09:08]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_4_Mask                                             cBit9_8
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_4_Shift                                                  8

/*--------------------------------------
BitField Name: Sk_sl1_LoMd_Grp_3
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [07:06]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_3_Mask                                             cBit7_6
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_3_Shift                                                  6

/*--------------------------------------
BitField Name: Sk_sl1_LoMd_Grp_2
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [05:04]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_2_Mask                                             cBit5_4
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_2_Shift                                                  4

/*--------------------------------------
BitField Name: Sk_sl1_LoMd_Grp_1
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [03:02]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_1_Mask                                             cBit3_2
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_1_Shift                                                  2

/*--------------------------------------
BitField Name: Sk_sl1_LoMd_Grp_0
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_0_Mask                                             cBit1_0
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_0_Shift                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Sink Slice 1 E1-Unframed Mode Configuration
Reg Addr   : 0x40940-0x4095a
Reg Formula: 0x40940 + $sts_id
    Where  : 
           + $sts_id(0-26)
Reg Desc   : 
This register is used to config mode for VCAT Sink side.
HDL_PATH     : isink.itiming.itiming[1].unfrm_buf.ram.ram[$sts_id]

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sk_sl1_unfrm_pen_Base                                                           0x40940

/*--------------------------------------
BitField Name: Sk_sl1_E1Unfrm
BitField Type: R/W
BitField Desc: 1: E1-Unframed Mode, 0: E1-Framed Mode (default)
BitField Bits: [27:00]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl1_unfrm_pen_Sk_sl1_E1Unfrm_Mask                                              cBit27_0
#define cAf6_vcat5g_sk_sl1_unfrm_pen_Sk_sl1_E1Unfrm_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Sink Slice 1 Input Mux Configuration
Reg Addr   : 0x40980-0x4099a
Reg Formula: 0x40980 + $sts_id
    Where  : 
           + $sts_id(0-26)
Reg Desc   : 
This register is used to select bus running at Sink side input.
HDL_PATH     : isink.itiming.itiming[1].imux.selector.ram1.array.ram[$sts_id]

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sk_sl1_mux_pen_Base                                                             0x40980

/*--------------------------------------
BitField Name: Sk_sl1_Mux
BitField Type: R/W
BitField Desc: 1: Select from PDH bus (DS3/E3/DS1/E1), 0: Select from STS/VT Bus
BitField Bits: [27:00]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl1_mux_pen_Sk_sl1_Mux_Mask                                                    cBit27_0
#define cAf6_vcat5g_sk_sl1_mux_pen_Sk_sl1_Mux_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Sink Member Control Configuration
Reg Addr   : 0x49000-0x495ff
Reg Formula: 0x49000 + $sts_id*32 + $vtg_id*4 + $vt_id
    Where  : 
           + $sts_id(0-47), $vtg_id(0-6), $vt_id(0-3)
Reg Desc   : 
This register is used to control engine VCAT Sink side.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sk_memctrl_pen_Base                                                             0x49000

/*--------------------------------------
BitField Name: Sk_Mem_Ena
BitField Type: R/W
BitField Desc: 1: Enable Member, 0: Disable Member
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_vcat5g_sk_memctrl_pen_Sk_Mem_Ena_Mask                                                      cBit16
#define cAf6_vcat5g_sk_memctrl_pen_Sk_Mem_Ena_Shift                                                         16

/*--------------------------------------
BitField Name: Sk_G8040_Md
BitField Type: R/W
BitField Desc: 1: ITU G.8040 Mode, 0: ITU G.804 Mode
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_vcat5g_sk_memctrl_pen_Sk_G8040_Md_Mask                                                     cBit15
#define cAf6_vcat5g_sk_memctrl_pen_Sk_G8040_Md_Shift                                                        15

/*--------------------------------------
BitField Name: Sk_Vcat_Md
BitField Type: R/W
BitField Desc: 1: VCAT Mode, 0: non-VCAT Mode
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_vcat5g_sk_memctrl_pen_Sk_Vcat_Md_Mask                                                      cBit14
#define cAf6_vcat5g_sk_memctrl_pen_Sk_Vcat_Md_Shift                                                         14

/*--------------------------------------
BitField Name: Sk_Lcas_Md
BitField Type: R/W
BitField Desc: LCAS Enable
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_vcat5g_sk_memctrl_pen_Sk_Lcas_Md_Mask                                                      cBit13
#define cAf6_vcat5g_sk_memctrl_pen_Sk_Lcas_Md_Shift                                                         13

/*--------------------------------------
BitField Name: Sk_Nms_Cmd
BitField Type: R/W
BitField Desc: Network Management Command
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_vcat5g_sk_memctrl_pen_Sk_Nms_Cmd_Mask                                                      cBit12
#define cAf6_vcat5g_sk_memctrl_pen_Sk_Nms_Cmd_Shift                                                         12

/*--------------------------------------
BitField Name: Sk_VcgId
BitField Type: R/W
BitField Desc: VCG ID 0-511
BitField Bits: [08:00]
--------------------------------------*/
#define cAf6_vcat5g_sk_memctrl_pen_Sk_VcgId_Mask                                                       cBit8_0
#define cAf6_vcat5g_sk_memctrl_pen_Sk_VcgId_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Sink Member Status
Reg Addr   : 0x51000-0x515ff
Reg Formula: 0x51000 + $sts_id*32 + $vtg_id*4 + $vt_id
    Where  : 
           + $sts_id(0-47), $vtg_id(0-6), $vt_id(0-3)
Reg Desc   : 
This register is used to show status of member at Sink side.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sk_memsta_pen_Base                                                              0x51000

/*--------------------------------------
BitField Name: Sk_Mem_EnaSta
BitField Type: RO
BitField Desc: Member Enable Status
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_vcat5g_sk_memsta_pen_Sk_Mem_EnaSta_Mask                                                    cBit20
#define cAf6_vcat5g_sk_memsta_pen_Sk_Mem_EnaSta_Shift                                                       20

/*--------------------------------------
BitField Name: Sk_Mem_AcpSeq
BitField Type: RO
BitField Desc: Sink Member Accepted Sequence
BitField Bits: [19:13]
--------------------------------------*/
#define cAf6_vcat5g_sk_memsta_pen_Sk_Mem_AcpSeq_Mask                                                 cBit19_13
#define cAf6_vcat5g_sk_memsta_pen_Sk_Mem_AcpSeq_Shift                                                       13

/*--------------------------------------
BitField Name: Sk_Mem_RecSeq
BitField Type: RO
BitField Desc: Sink Member Sequence Received
BitField Bits: [12:06]
--------------------------------------*/
#define cAf6_vcat5g_sk_memsta_pen_Sk_Mem_RecSeq_Mask                                                  cBit12_6
#define cAf6_vcat5g_sk_memsta_pen_Sk_Mem_RecSeq_Shift                                                        6

/*--------------------------------------
BitField Name: Sk_Mem_Fsm
BitField Type: RO
BitField Desc: Member FSM
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_vcat5g_sk_memsta_pen_Sk_Mem_Fsm_Mask                                                      cBit1_0
#define cAf6_vcat5g_sk_memsta_pen_Sk_Mem_Fsm_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Sink Member Expected Sequence Control
Reg Addr   : 0x53000-0x535ff
Reg Formula: 0x53000 + $sts_id*32 + $vtg_id*4 + $vt_id
    Where  : 
           + $sts_id(0-47), $vtg_id(0-6), $vt_id(0-3)
Reg Desc   : 
This register is used to Control Expected Sequence of member at Sink side.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sk_expseq_pen_Base                                                              0x53000

/*--------------------------------------
BitField Name: Sk_Mem_ExpSeq
BitField Type: R/W
BitField Desc: Member Expected Sequence
BitField Bits: [06:00]
--------------------------------------*/
#define cAf6_vcat5g_sk_expseq_pen_Sk_Mem_ExpSeq_Mask                                                   cBit6_0
#define cAf6_vcat5g_sk_expseq_pen_Sk_Mem_ExpSeq_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Sink Global Member Deskew Lantecy Control
Reg Addr   : 0x48020-0x48023
Reg Formula: 0x48020 + $mode
    Where  : 
           + $mode(0-3)
Reg Desc   : 
This register is used to Control Deskew latency of member at Sink side.
with $mode:
2'd0: n x 4-frame DS1/E1
2'd1: n x frame DS3/E3
2'd2: n x frame VC3 for VC3 VCAT
2'd3: n x frame VC3 for VC4 VCAT

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sk_deskew_pen_Base                                                              0x48020

/*--------------------------------------
BitField Name: Sk_Mem_DskwEna
BitField Type: R/W
BitField Desc: Member Deskew Enable
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_vcat5g_sk_deskew_pen_Sk_Mem_DskwEna_Mask                                                   cBit12
#define cAf6_vcat5g_sk_deskew_pen_Sk_Mem_DskwEna_Shift                                                      12

/*--------------------------------------
BitField Name: Sk_Mem_DskwVal
BitField Type: R/W
BitField Desc: Member Deskew latentcy(frames)
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_vcat5g_sk_deskew_pen_Sk_Mem_DskwVal_Mask                                                 cBit11_0
#define cAf6_vcat5g_sk_deskew_pen_Sk_Mem_DskwVal_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Sink Channel Interrupt Enable Control
Reg Addr   : 0x58000-0x585FF
Reg Formula: 0x58000 + $sts_id*32 + $vtg_id*4 + $vt_id
    Where  : 
           + $sts_id(0-47), $vtg_id(0-6), $vt_id(0-3)
Reg Desc   : 
This register is used to enable Interrupt, write 1 to enable interrupt for event.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sk_intrctrl_pen_Base                                                            0x58000

/*--------------------------------------
BitField Name: SQNC_Intr_En
BitField Type: R/W
BitField Desc: Interrupt Enable for SQNC event
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_vcat5g_sk_intrctrl_pen_SQNC_Intr_En_Mask                                                    cBit5
#define cAf6_vcat5g_sk_intrctrl_pen_SQNC_Intr_En_Shift                                                       5

/*--------------------------------------
BitField Name: MND_Intr_En
BitField Type: R/W
BitField Desc: Interrupt Enable for MND event
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_vcat5g_sk_intrctrl_pen_MND_Intr_En_Mask                                                     cBit4
#define cAf6_vcat5g_sk_intrctrl_pen_MND_Intr_En_Shift                                                        4

/*--------------------------------------
BitField Name: LOA_Intr_En
BitField Type: R/W
BitField Desc: Interrupt Enable for LOA event
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_vcat5g_sk_intrctrl_pen_LOA_Intr_En_Mask                                                     cBit3
#define cAf6_vcat5g_sk_intrctrl_pen_LOA_Intr_En_Shift                                                        3

/*--------------------------------------
BitField Name: SQM_Intr_En
BitField Type: R/W
BitField Desc: Interrupt Enable for SQM event
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_vcat5g_sk_intrctrl_pen_SQM_Intr_En_Mask                                                     cBit2
#define cAf6_vcat5g_sk_intrctrl_pen_SQM_Intr_En_Shift                                                        2

/*--------------------------------------
BitField Name: OOM_Intr_En
BitField Type: R/W
BitField Desc: Interrupt Enable for OOM event
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_vcat5g_sk_intrctrl_pen_OOM_Intr_En_Mask                                                     cBit1
#define cAf6_vcat5g_sk_intrctrl_pen_OOM_Intr_En_Shift                                                        1

/*--------------------------------------
BitField Name: LOM_Intr_En
BitField Type: R/W
BitField Desc: Interrupt Enable for LOM event
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_vcat5g_sk_intrctrl_pen_LOM_Intr_En_Mask                                                     cBit0
#define cAf6_vcat5g_sk_intrctrl_pen_LOM_Intr_En_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Sink Channel Interrupt Status
Reg Addr   : 0x59000-0x595FF
Reg Formula: 0x59000 + $sts_id*32 + $vtg_id*4 + $vt_id
    Where  : 
           + $sts_id(0-47), $vtg_id(0-6), $vt_id(0-3)
Reg Desc   : 
This register is used to show the status of event,which generate interrupt.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sk_intrstk_pen_Base                                                             0x59000

/*--------------------------------------
BitField Name: SQNC_Intr_Sta
BitField Type: R/W
BitField Desc: SQNC event state change
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_vcat5g_sk_intrstk_pen_SQNC_Intr_Sta_Mask                                                    cBit5
#define cAf6_vcat5g_sk_intrstk_pen_SQNC_Intr_Sta_Shift                                                       5

/*--------------------------------------
BitField Name: MND_Intr_Sta
BitField Type: R/W
BitField Desc: MND event state change
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_vcat5g_sk_intrstk_pen_MND_Intr_Sta_Mask                                                     cBit4
#define cAf6_vcat5g_sk_intrstk_pen_MND_Intr_Sta_Shift                                                        4

/*--------------------------------------
BitField Name: LOA_Intr_Sta
BitField Type: R/W
BitField Desc: LOA event state change
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_vcat5g_sk_intrstk_pen_LOA_Intr_Sta_Mask                                                     cBit3
#define cAf6_vcat5g_sk_intrstk_pen_LOA_Intr_Sta_Shift                                                        3

/*--------------------------------------
BitField Name: SQM_Intr_Sta
BitField Type: R/W
BitField Desc: SQM event state change
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_vcat5g_sk_intrstk_pen_SQM_Intr_Sta_Mask                                                     cBit2
#define cAf6_vcat5g_sk_intrstk_pen_SQM_Intr_Sta_Shift                                                        2

/*--------------------------------------
BitField Name: OOM_Intr_Sta
BitField Type: R/W
BitField Desc: OOM event state change
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_vcat5g_sk_intrstk_pen_OOM_Intr_Sta_Mask                                                     cBit1
#define cAf6_vcat5g_sk_intrstk_pen_OOM_Intr_Sta_Shift                                                        1

/*--------------------------------------
BitField Name: LOM_Intr_Sta
BitField Type: R/W
BitField Desc: LOM event state change
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_vcat5g_sk_intrstk_pen_LOM_Intr_Sta_Mask                                                     cBit0
#define cAf6_vcat5g_sk_intrstk_pen_LOM_Intr_Sta_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Sink Channel Current Status
Reg Addr   : 0x5A000-0x5A5FF
Reg Formula: 0x5A000 + $sts_id*32 + $vtg_id*4 + $vt_id
    Where  : 
           + $sts_id(0-47), $vtg_id(0-6), $vt_id(0-3)
Reg Desc   : 
This register is used to show the current status of event.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sk_intrsta_pen_Base                                                             0x5A000

/*--------------------------------------
BitField Name: SQNC_Intr_CrrSta
BitField Type: RO
BitField Desc: SQNC event Current state
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_vcat5g_sk_intrsta_pen_SQNC_Intr_CrrSta_Mask                                                 cBit5
#define cAf6_vcat5g_sk_intrsta_pen_SQNC_Intr_CrrSta_Shift                                                    5

/*--------------------------------------
BitField Name: MND_Intr_CrrSta
BitField Type: RO
BitField Desc: MND event Current state
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_vcat5g_sk_intrsta_pen_MND_Intr_CrrSta_Mask                                                  cBit4
#define cAf6_vcat5g_sk_intrsta_pen_MND_Intr_CrrSta_Shift                                                     4

/*--------------------------------------
BitField Name: LOA_Intr_CrrSta
BitField Type: RO
BitField Desc: LOA event Current state
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_vcat5g_sk_intrsta_pen_LOA_Intr_CrrSta_Mask                                                  cBit3
#define cAf6_vcat5g_sk_intrsta_pen_LOA_Intr_CrrSta_Shift                                                     3

/*--------------------------------------
BitField Name: SQM_Intr_CrrSta
BitField Type: RO
BitField Desc: SQM event Current state
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_vcat5g_sk_intrsta_pen_SQM_Intr_CrrSta_Mask                                                  cBit2
#define cAf6_vcat5g_sk_intrsta_pen_SQM_Intr_CrrSta_Shift                                                     2

/*--------------------------------------
BitField Name: OOM_Intr_CrrSta
BitField Type: RO
BitField Desc: OOM event Current state
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_vcat5g_sk_intrsta_pen_OOM_Intr_CrrSta_Mask                                                  cBit1
#define cAf6_vcat5g_sk_intrsta_pen_OOM_Intr_CrrSta_Shift                                                     1

/*--------------------------------------
BitField Name: LOM_Intr_CrrSta
BitField Type: RO
BitField Desc: LOM event Current state
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_vcat5g_sk_intrsta_pen_LOM_Intr_CrrSta_Mask                                                  cBit0
#define cAf6_vcat5g_sk_intrsta_pen_LOM_Intr_CrrSta_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Sink Channel Interrupt Or Status
Reg Addr   : 0x5B000-0x5B02F
Reg Formula: 0x5B000 + $sts_id
    Where  : 
           + $sts_id(0-47)
Reg Desc   : 
The register consists of 28 bits for 28 VT/TUs or 28 DS1/E1s of the related STS/VC.
Each bit is used to store Interrupt OR status of the related VT/TU/DS1/E1.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sk_introsta_pen_Base                                                            0x5B000

/*--------------------------------------
BitField Name: IntrOrSta
BitField Type: R/W
BitField Desc: Interrupt Or Status
BitField Bits: [27:00]
--------------------------------------*/
#define cAf6_vcat5g_sk_introsta_pen_IntrOrSta_Mask                                                    cBit27_0
#define cAf6_vcat5g_sk_introsta_pen_IntrOrSta_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Sink STS/VC Interrupt Enable Control
Reg Addr   : 0x5FFFE
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 96 interrupt enable bits for 96 STS/VCs.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sk_vcintrctrl_pen_Base                                                          0x5FFFE

/*--------------------------------------
BitField Name: STS_Intr_En
BitField Type: R/W
BitField Desc: Interrupt Enable for STS(0-96)
BitField Bits: [95:00]
--------------------------------------*/
#define cAf6_vcat5g_sk_vcintrctrl_pen_STS_Intr_En_Mask_01                                             cBit31_0
#define cAf6_vcat5g_sk_vcintrctrl_pen_STS_Intr_En_Shift_01                                                   0
#define cAf6_vcat5g_sk_vcintrctrl_pen_STS_Intr_En_Mask_02                                             cBit31_0
#define cAf6_vcat5g_sk_vcintrctrl_pen_STS_Intr_En_Shift_02                                                   0
#define cAf6_vcat5g_sk_vcintrctrl_pen_STS_Intr_En_Mask_03                                             cBit31_0
#define cAf6_vcat5g_sk_vcintrctrl_pen_STS_Intr_En_Shift_03                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Sink STS/VC Interrupt Or Status
Reg Addr   : 0x5FFFF
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 96 bits for 16 STS/VCs. Each bit is used to store Interrupt OR status of the related STS/VC.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sk_vcintrosta_pen_Base                                                          0x5FFFF

/*--------------------------------------
BitField Name: STS_IntrOrSta
BitField Type: R/W
BitField Desc: Interrupt Status for STS(0-96)
BitField Bits: [95:00]
--------------------------------------*/
#define cAf6_vcat5g_sk_vcintrosta_pen_STS_IntrOrSta_Mask_01                                           cBit31_0
#define cAf6_vcat5g_sk_vcintrosta_pen_STS_IntrOrSta_Shift_01                                                 0
#define cAf6_vcat5g_sk_vcintrosta_pen_STS_IntrOrSta_Mask_02                                           cBit31_0
#define cAf6_vcat5g_sk_vcintrosta_pen_STS_IntrOrSta_Shift_02                                                 0
#define cAf6_vcat5g_sk_vcintrosta_pen_STS_IntrOrSta_Mask_03                                           cBit31_0
#define cAf6_vcat5g_sk_vcintrosta_pen_STS_IntrOrSta_Shift_03                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Debug Vcat Interface Dump Control
Reg Addr   : 0x0000A
Reg Formula: 
    Where  : 
Reg Desc   : 
.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_dumpctrl_pen_Base                                                               0x0000A

/*--------------------------------------
BitField Name: Cfg_Dump_Mod
BitField Type: R/W
BitField Desc: Data dump mode 1'b0     : Dump full 1'b1     : Dump at frame 0
(DS1 mode)
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_vcat5g_dumpctrl_pen_Cfg_Dump_Mod_Mask                                                       cBit3
#define cAf6_vcat5g_dumpctrl_pen_Cfg_Dump_Mod_Shift                                                          3

/*--------------------------------------
BitField Name: Cfg_Dump_Sel
BitField Type: R/W
BitField Desc: Select bus info to dump 3'd0     : Source to PDH FiFo Input Data
3'd1     : Source PDH FiFo Output Data 3'd2     : Sink PDH Input Data 3'd3     :
ENC-VCAT Data 3'd4-3'd7: Reserved
BitField Bits: [02:00]
--------------------------------------*/
#define cAf6_vcat5g_dumpctrl_pen_Cfg_Dump_Sel_Mask                                                     cBit2_0
#define cAf6_vcat5g_dumpctrl_pen_Cfg_Dump_Sel_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Debug Vcat Interface Dump Data
Reg Addr   : 0x60000-0x61FFF
Reg Formula: 0x60000 + $dump_id
    Where  : 
           + $dump_id(0-8191)
Reg Desc   : 
.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_dumpdat_pen_Base                                                                0x60000

/*--------------------------------------
BitField Name: Dump_Dat_Vli
BitField Type: RO
BitField Desc: VLI Indication
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_vcat5g_dumpdat_pen_Dump_Dat_Vli_Mask                                                        cBit8
#define cAf6_vcat5g_dumpdat_pen_Dump_Dat_Vli_Shift                                                           8

/*--------------------------------------
BitField Name: Dump_Dat
BitField Type: RO
BitField Desc: Interface Dump Datat
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_vcat5g_dumpdat_pen_Dump_Dat_Mask                                                          cBit7_0
#define cAf6_vcat5g_dumpdat_pen_Dump_Dat_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Source Debug Control
Reg Addr   : 0x2010B
Reg Formula: 
    Where  : 
Reg Desc   : 
This register used to control VCAT debug.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sodbctrl_pen_Base                                                               0x2010B

/*--------------------------------------
BitField Name: Cfg_Dbg_Ena
BitField Type: R/W
BitField Desc: Enable Debug
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_vcat5g_sodbctrl_pen_Cfg_Dbg_Ena_Mask                                                       cBit12
#define cAf6_vcat5g_sodbctrl_pen_Cfg_Dbg_Ena_Shift                                                          12

/*--------------------------------------
BitField Name: Cfg_Dbg_Slc
BitField Type: R/W
BitField Desc: Select Slice ID to Debug
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_vcat5g_sodbctrl_pen_Cfg_Dbg_Slc_Mask                                                    cBit11_10
#define cAf6_vcat5g_sodbctrl_pen_Cfg_Dbg_Slc_Shift                                                          10

/*--------------------------------------
BitField Name: Cfg_Dbg_Lid
BitField Type: R/W
BitField Desc: Select Channel ID to Debug
BitField Bits: [09:00]
--------------------------------------*/
#define cAf6_vcat5g_sodbctrl_pen_Cfg_Dbg_Lid_Mask                                                      cBit9_0
#define cAf6_vcat5g_sodbctrl_pen_Cfg_Dbg_Lid_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Debug VCAT Source Sticky
Reg Addr   : 0x2010A
Reg Formula: 
    Where  : 
Reg Desc   : 
.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sostk_pen_Base                                                                  0x2010A

/*--------------------------------------
BitField Name: So_Slc3_LO_Empt
BitField Type: R/W
BitField Desc: Slice 3 LO PDH Adapt FiFo Empt
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_Slc3_LO_Empt_Mask                                                      cBit31
#define cAf6_vcat5g_sostk_pen_So_Slc3_LO_Empt_Shift                                                         31

/*--------------------------------------
BitField Name: So_Slc3_HO_Empt
BitField Type: R/W
BitField Desc: Slice 3 HO PDH Adapt FiFo Empt
BitField Bits: [30:30]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_Slc3_HO_Empt_Mask                                                      cBit30
#define cAf6_vcat5g_sostk_pen_So_Slc3_HO_Empt_Shift                                                         30

/*--------------------------------------
BitField Name: So_Slc3_LO_Full
BitField Type: R/W
BitField Desc: Slice 3 LO PDH Adapt FiFo Full
BitField Bits: [29:29]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_Slc3_LO_Full_Mask                                                      cBit29
#define cAf6_vcat5g_sostk_pen_So_Slc3_LO_Full_Shift                                                         29

/*--------------------------------------
BitField Name: So_Slc3_HO_Full
BitField Type: R/W
BitField Desc: Slice 3 HO PDH Adapt FiFo Full
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_Slc3_HO_Full_Mask                                                      cBit28
#define cAf6_vcat5g_sostk_pen_So_Slc3_HO_Full_Shift                                                         28

/*--------------------------------------
BitField Name: So_Slc2_LO_Empt
BitField Type: R/W
BitField Desc: Slice 2 LO PDH Adapt FiFo Empt
BitField Bits: [27:27]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_Slc2_LO_Empt_Mask                                                      cBit27
#define cAf6_vcat5g_sostk_pen_So_Slc2_LO_Empt_Shift                                                         27

/*--------------------------------------
BitField Name: So_Slc2_HO_Empt
BitField Type: R/W
BitField Desc: Slice 2 HO PDH Adapt FiFo Empt
BitField Bits: [26:26]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_Slc2_HO_Empt_Mask                                                      cBit26
#define cAf6_vcat5g_sostk_pen_So_Slc2_HO_Empt_Shift                                                         26

/*--------------------------------------
BitField Name: So_Slc2_LO_Full
BitField Type: R/W
BitField Desc: Slice 2 LO PDH Adapt FiFo Full
BitField Bits: [25:25]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_Slc2_LO_Full_Mask                                                      cBit25
#define cAf6_vcat5g_sostk_pen_So_Slc2_LO_Full_Shift                                                         25

/*--------------------------------------
BitField Name: So_Slc2_HO_Full
BitField Type: R/W
BitField Desc: Slice 2 HO PDH Adapt FiFo Full
BitField Bits: [24:24]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_Slc2_HO_Full_Mask                                                      cBit24
#define cAf6_vcat5g_sostk_pen_So_Slc2_HO_Full_Shift                                                         24

/*--------------------------------------
BitField Name: So_Slc1_LO_Empt
BitField Type: R/W
BitField Desc: Slice 1 LO PDH Adapt FiFo Empt
BitField Bits: [23:23]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_Slc1_LO_Empt_Mask                                                      cBit23
#define cAf6_vcat5g_sostk_pen_So_Slc1_LO_Empt_Shift                                                         23

/*--------------------------------------
BitField Name: So_Slc1_HO_Empt
BitField Type: R/W
BitField Desc: Slice 1 HO PDH Adapt FiFo Empt
BitField Bits: [22:22]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_Slc1_HO_Empt_Mask                                                      cBit22
#define cAf6_vcat5g_sostk_pen_So_Slc1_HO_Empt_Shift                                                         22

/*--------------------------------------
BitField Name: So_Slc1_LO_Full
BitField Type: R/W
BitField Desc: Slice 1 LO PDH Adapt FiFo Full
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_Slc1_LO_Full_Mask                                                      cBit21
#define cAf6_vcat5g_sostk_pen_So_Slc1_LO_Full_Shift                                                         21

/*--------------------------------------
BitField Name: So_Slc1_HO_Full
BitField Type: R/W
BitField Desc: Slice 1 HO PDH Adapt FiFo Full
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_Slc1_HO_Full_Mask                                                      cBit20
#define cAf6_vcat5g_sostk_pen_So_Slc1_HO_Full_Shift                                                         20

/*--------------------------------------
BitField Name: So_Slc0_LO_Empt
BitField Type: R/W
BitField Desc: Slice 0 LO PDH Adapt FiFo Empt
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_Slc0_LO_Empt_Mask                                                      cBit19
#define cAf6_vcat5g_sostk_pen_So_Slc0_LO_Empt_Shift                                                         19

/*--------------------------------------
BitField Name: So_Slc0_HO_Empt
BitField Type: R/W
BitField Desc: Slice 0 HO PDH Adapt FiFo Empt
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_Slc0_HO_Empt_Mask                                                      cBit18
#define cAf6_vcat5g_sostk_pen_So_Slc0_HO_Empt_Shift                                                         18

/*--------------------------------------
BitField Name: So_Slc0_LO_Full
BitField Type: R/W
BitField Desc: Slice 0 LO PDH Adapt FiFo Full
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_Slc0_LO_Full_Mask                                                      cBit17
#define cAf6_vcat5g_sostk_pen_So_Slc0_LO_Full_Shift                                                         17

/*--------------------------------------
BitField Name: So_Slc0_HO_Full
BitField Type: R/W
BitField Desc: Slice 0 HO PDH Adapt FiFo Full
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_Slc0_HO_Full_Mask                                                      cBit16
#define cAf6_vcat5g_sostk_pen_So_Slc0_HO_Full_Shift                                                         16

/*--------------------------------------
BitField Name: So_DS3VLI_Err
BitField Type: R/W
BitField Desc: Ouput DS3 VLI Position Error
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_DS3VLI_Err_Mask                                                        cBit15
#define cAf6_vcat5g_sostk_pen_So_DS3VLI_Err_Shift                                                           15

/*--------------------------------------
BitField Name: So_E3VLI_Err
BitField Type: R/W
BitField Desc: Ouput E3 VLI Position Error
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_E3VLI_Err_Mask                                                         cBit14
#define cAf6_vcat5g_sostk_pen_So_E3VLI_Err_Shift                                                            14

/*--------------------------------------
BitField Name: So_DS1VLI_Err
BitField Type: R/W
BitField Desc: Ouput DS1 VLI Position Error
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_DS1VLI_Err_Mask                                                        cBit13
#define cAf6_vcat5g_sostk_pen_So_DS1VLI_Err_Shift                                                           13

/*--------------------------------------
BitField Name: So_E1VLI_Err
BitField Type: R/W
BitField Desc: Ouput E1 VLI Position Error
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_E1VLI_Err_Mask                                                         cBit12
#define cAf6_vcat5g_sostk_pen_So_E1VLI_Err_Shift                                                            12

/*--------------------------------------
BitField Name: So_PDH_Vli
BitField Type: R/W
BitField Desc: Output VLI from VCAT to PDH
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_PDH_Vli_Mask                                                           cBit10
#define cAf6_vcat5g_sostk_pen_So_PDH_Vli_Shift                                                              10

/*--------------------------------------
BitField Name: So_PDH_Vld
BitField Type: R/W
BitField Desc: Output Valid from VCAT to PDH
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_PDH_Vld_Mask                                                            cBit9
#define cAf6_vcat5g_sostk_pen_So_PDH_Vld_Shift                                                               9

/*--------------------------------------
BitField Name: IPDH_Req
BitField Type: R/W
BitField Desc: Request from PDH to VCAT
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_IPDH_Req_Mask                                                              cBit8
#define cAf6_vcat5g_sostk_pen_IPDH_Req_Shift                                                                 8

/*--------------------------------------
BitField Name: So_Slc3_Vld
BitField Type: R/W
BitField Desc: Source Slice 3 Read Valid
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_Slc3_Vld_Mask                                                           cBit7
#define cAf6_vcat5g_sostk_pen_So_Slc3_Vld_Shift                                                              7

/*--------------------------------------
BitField Name: So_Slc2_Vld
BitField Type: R/W
BitField Desc: Source Slice 2 Read Valid
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_Slc2_Vld_Mask                                                           cBit6
#define cAf6_vcat5g_sostk_pen_So_Slc2_Vld_Shift                                                              6

/*--------------------------------------
BitField Name: So_Slc1_Vld
BitField Type: R/W
BitField Desc: Source Slice 1 Read Valid
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_Slc1_Vld_Mask                                                           cBit5
#define cAf6_vcat5g_sostk_pen_So_Slc1_Vld_Shift                                                              5

/*--------------------------------------
BitField Name: So_Slc0_Vld
BitField Type: R/W
BitField Desc: Source Slice 0 Read Valid
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_Slc0_Vld_Mask                                                           cBit4
#define cAf6_vcat5g_sostk_pen_So_Slc0_Vld_Shift                                                              4

/*--------------------------------------
BitField Name: So_Mem_Vld
BitField Type: R/W
BitField Desc: Source Member Valid
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_Mem_Vld_Mask                                                            cBit1
#define cAf6_vcat5g_sostk_pen_So_Mem_Vld_Shift                                                               1

/*--------------------------------------
BitField Name: So_Enc_Req
BitField Type: R/W
BitField Desc: Request from VCAT to ENC
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_Enc_Req_Mask                                                            cBit0
#define cAf6_vcat5g_sostk_pen_So_Enc_Req_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : VCAT Data Control
Reg Addr   : 0x000FD
Reg Formula: 
    Where  : 
Reg Desc   : 
This register used to control Data Generator/Monitor for VCAT debug.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sodat_pen_Base                                                                  0x000FD

/*--------------------------------------
BitField Name: Cfg_Ber_Mod
BitField Type: R/W
BitField Desc: BER Mode
BitField Bits: [18:16]
--------------------------------------*/
#define cAf6_vcat5g_sodat_pen_Cfg_Ber_Mod_Mask                                                       cBit18_16
#define cAf6_vcat5g_sodat_pen_Cfg_Ber_Mod_Shift                                                             16

/*--------------------------------------
BitField Name: Cfg_Ber_Ena
BitField Type: R/W
BitField Desc: BER Control
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_vcat5g_sodat_pen_Cfg_Ber_Ena_Mask                                                          cBit15
#define cAf6_vcat5g_sodat_pen_Cfg_Ber_Ena_Shift                                                             15

/*--------------------------------------
BitField Name: Cfg_Swp_Mod
BitField Type: R/W
BitField Desc: Data Swap Mode
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_vcat5g_sodat_pen_Cfg_Swp_Mod_Mask                                                          cBit14
#define cAf6_vcat5g_sodat_pen_Cfg_Swp_Mod_Shift                                                             14

/*--------------------------------------
BitField Name: Cfg_Inv_Mod
BitField Type: R/W
BitField Desc: Data Invert Mode
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_vcat5g_sodat_pen_Cfg_Inv_Mod_Mask                                                          cBit13
#define cAf6_vcat5g_sodat_pen_Cfg_Inv_Mod_Shift                                                             13

/*--------------------------------------
BitField Name: Cfg_Err_Mod
BitField Type: R/W
BitField Desc: Force Data Error
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_vcat5g_sodat_pen_Cfg_Err_Mod_Mask                                                          cBit12
#define cAf6_vcat5g_sodat_pen_Cfg_Err_Mod_Shift                                                             12

/*--------------------------------------
BitField Name: Cfg_Dat_Mod
BitField Type: R/W
BitField Desc: Select Mode for Data Generator/Monitor
BitField Bits: [11:09]
--------------------------------------*/
#define cAf6_vcat5g_sodat_pen_Cfg_Dat_Mod_Mask                                                        cBit11_9
#define cAf6_vcat5g_sodat_pen_Cfg_Dat_Mod_Shift                                                              9

/*--------------------------------------
BitField Name: Cfg_Dat_Ena
BitField Type: R/W
BitField Desc: Enable Data Generator/Monitor
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_vcat5g_sodat_pen_Cfg_Dat_Ena_Mask                                                           cBit8
#define cAf6_vcat5g_sodat_pen_Cfg_Dat_Ena_Shift                                                              8

/*--------------------------------------
BitField Name: Cfg_Dat_VCG
BitField Type: R/W
BitField Desc: Select VCG for Data Generator/Monitor
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_vcat5g_sodat_pen_Cfg_Dat_VCG_Mask                                                         cBit7_0
#define cAf6_vcat5g_sodat_pen_Cfg_Dat_VCG_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : VCAT Data Error Counter
Reg Addr   : 0x000FA-0x000FB
Reg Formula: 0x000FA + $r2c
    Where  : 
           + $r2c(0-1): 1 for read to clear
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sodatmon_pen_Base                                                               0x000FA

/*--------------------------------------
BitField Name: Dat_Err_Cnt
BitField Type: RO
BitField Desc: Data Error Counter
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_vcat5g_sodatmon_pen_Dat_Err_Cnt_Mask                                                      cBit7_0
#define cAf6_vcat5g_sodatmon_pen_Dat_Err_Cnt_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Debug VCAT Data Error Sticky
Reg Addr   : 0x000FF
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_dbdatstk_pen_Base                                                               0x000FF

/*--------------------------------------
BitField Name: Dat_Err_Sta
BitField Type: RO
BitField Desc: Data Error Status
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_vcat5g_dbdatstk_pen_Dat_Err_Sta_Mask                                                        cBit1
#define cAf6_vcat5g_dbdatstk_pen_Dat_Err_Sta_Shift                                                           1

/*--------------------------------------
BitField Name: Dat_Err_Stk
BitField Type: RO
BitField Desc: Data Error Sticky
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_vcat5g_dbdatstk_pen_Dat_Err_Stk_Mask                                                        cBit0
#define cAf6_vcat5g_dbdatstk_pen_Dat_Err_Stk_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Debug VCAT Sink Sticky
Reg Addr   : 0x50004
Reg Formula: 
    Where  : 
Reg Desc   : 
.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_skstk_pen_Base                                                                  0x50004

/*--------------------------------------
BitField Name: Sk_Alig_FFFull
BitField Type: R/W
BitField Desc: Sink Aligment FIFO Full
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_vcat5g_skstk_pen_Sk_Alig_FFFull_Mask                                                       cBit20
#define cAf6_vcat5g_skstk_pen_Sk_Alig_FFFull_Shift                                                          20

/*--------------------------------------
BitField Name: Sk_Slc3_FFFull
BitField Type: R/W
BitField Desc: Sink Slice 3 FIFO Full
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_vcat5g_skstk_pen_Sk_Slc3_FFFull_Mask                                                       cBit19
#define cAf6_vcat5g_skstk_pen_Sk_Slc3_FFFull_Shift                                                          19

/*--------------------------------------
BitField Name: Sk_Slc2_FFFull
BitField Type: R/W
BitField Desc: Sink Slice 2 FIFO Full
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_vcat5g_skstk_pen_Sk_Slc2_FFFull_Mask                                                       cBit18
#define cAf6_vcat5g_skstk_pen_Sk_Slc2_FFFull_Shift                                                          18

/*--------------------------------------
BitField Name: Sk_Slc1_FFFull
BitField Type: R/W
BitField Desc: Sink Slice 1 FIFO Full
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_vcat5g_skstk_pen_Sk_Slc1_FFFull_Mask                                                       cBit17
#define cAf6_vcat5g_skstk_pen_Sk_Slc1_FFFull_Shift                                                          17

/*--------------------------------------
BitField Name: Sk_Slc0_FFFull
BitField Type: R/W
BitField Desc: Sink Slice 0 FIFO Full
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_vcat5g_skstk_pen_Sk_Slc0_FFFull_Mask                                                       cBit16
#define cAf6_vcat5g_skstk_pen_Sk_Slc0_FFFull_Shift                                                          16

/*--------------------------------------
BitField Name: Sk_Seq_oVld
BitField Type: R/W
BitField Desc: Sequence Processing Output Valid to DEC
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_vcat5g_skstk_pen_Sk_Seq_oVld_Mask                                                           cBit2
#define cAf6_vcat5g_skstk_pen_Sk_Seq_oVld_Shift                                                              2

/*--------------------------------------
BitField Name: Sk_Seq_iVld
BitField Type: R/W
BitField Desc: Sequence Processing Input Valid
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_vcat5g_skstk_pen_Sk_Seq_iVld_Mask                                                           cBit1
#define cAf6_vcat5g_skstk_pen_Sk_Seq_iVld_Shift                                                              1

/*--------------------------------------
BitField Name: Sk_Seq_iLom
BitField Type: R/W
BitField Desc: Sequence Processing Input LOM
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_vcat5g_skstk_pen_Sk_Seq_iLom_Mask                                                           cBit0
#define cAf6_vcat5g_skstk_pen_Sk_Seq_iLom_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : Debug DDR Cache Sticky
Reg Addr   : 0x01001
Reg Formula: 
    Where  : 
Reg Desc   : 
.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_ddrstk_pen_Base                                                                 0x01001

/*--------------------------------------
BitField Name: Vcat_Wrca_Sos
BitField Type: R/W
BitField Desc: VCAT Write Cache Start of Segment
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_vcat5g_ddrstk_pen_Vcat_Wrca_Sos_Mask                                                       cBit21
#define cAf6_vcat5g_ddrstk_pen_Vcat_Wrca_Sos_Shift                                                          21

/*--------------------------------------
BitField Name: Vcat_Wrca_Eos
BitField Type: R/W
BitField Desc: VCAT Write Cache End of Segment
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_vcat5g_ddrstk_pen_Vcat_Wrca_Eos_Mask                                                       cBit20
#define cAf6_vcat5g_ddrstk_pen_Vcat_Wrca_Eos_Shift                                                          20

/*--------------------------------------
BitField Name: Vcat_Rdca_Rdy
BitField Type: R/W
BitField Desc: VCAT Free Read Cache Ready
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_vcat5g_ddrstk_pen_Vcat_Rdca_Rdy_Mask                                                       cBit16
#define cAf6_vcat5g_ddrstk_pen_Vcat_Rdca_Rdy_Shift                                                          16

/*--------------------------------------
BitField Name: Vcat_Rdca_Err
BitField Type: R/W
BitField Desc: VCAT Free Cache Read Error
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_vcat5g_ddrstk_pen_Vcat_Rdca_Err_Mask                                                       cBit14
#define cAf6_vcat5g_ddrstk_pen_Vcat_Rdca_Err_Shift                                                          14

/*--------------------------------------
BitField Name: Vcat_Rdca_Get
BitField Type: R/W
BitField Desc: VCAT Free Cache Read Get
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_vcat5g_ddrstk_pen_Vcat_Rdca_Get_Mask                                                       cBit13
#define cAf6_vcat5g_ddrstk_pen_Vcat_Rdca_Get_Shift                                                          13

/*--------------------------------------
BitField Name: Vcat_Reqff_Full
BitField Type: R/W
BitField Desc: VCAT Read Request FiFo Full
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_vcat5g_ddrstk_pen_Vcat_Reqff_Full_Mask                                                     cBit12
#define cAf6_vcat5g_ddrstk_pen_Vcat_Reqff_Full_Shift                                                        12

/*--------------------------------------
BitField Name: Vcat_Rdca_LLfull
BitField Type: R/W
BitField Desc: VCAT Read Cache Link FiFo Full
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_vcat5g_ddrstk_pen_Vcat_Rdca_LLfull_Mask                                                    cBit10
#define cAf6_vcat5g_ddrstk_pen_Vcat_Rdca_LLfull_Shift                                                       10

/*--------------------------------------
BitField Name: Vcat_FreeCa_Empt
BitField Type: R/W
BitField Desc: VCAT Free Cache FiFo Empty
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_vcat5g_ddrstk_pen_Vcat_FreeCa_Empt_Mask                                                     cBit8
#define cAf6_vcat5g_ddrstk_pen_Vcat_FreeCa_Empt_Shift                                                        8

/*--------------------------------------
BitField Name: DDR_Wr_Done
BitField Type: R/W
BitField Desc: DDR Data Write Done
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_vcat5g_ddrstk_pen_DDR_Wr_Done_Mask                                                          cBit7
#define cAf6_vcat5g_ddrstk_pen_DDR_Wr_Done_Shift                                                             7

/*--------------------------------------
BitField Name: DDR_Rd_Ena
BitField Type: R/W
BitField Desc: DDR Read Data Enable from PLA
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_vcat5g_ddrstk_pen_DDR_Rd_Ena_Mask                                                           cBit6
#define cAf6_vcat5g_ddrstk_pen_DDR_Rd_Ena_Shift                                                              6

/*--------------------------------------
BitField Name: DDR_Rd_Eop
BitField Type: R/W
BitField Desc: DDR Read Data End of Segment
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_vcat5g_ddrstk_pen_DDR_Rd_Eop_Mask                                                           cBit5
#define cAf6_vcat5g_ddrstk_pen_DDR_Rd_Eop_Shift                                                              5

/*--------------------------------------
BitField Name: DDR_Rd_Vld
BitField Type: R/W
BitField Desc: DDR Read Data Valid
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_vcat5g_ddrstk_pen_DDR_Rd_Vld_Mask                                                           cBit4
#define cAf6_vcat5g_ddrstk_pen_DDR_Rd_Vld_Shift                                                              4

/*--------------------------------------
BitField Name: VCAT_Read
BitField Type: R/W
BitField Desc: Request Read from VCAT to PLA
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_vcat5g_ddrstk_pen_VCAT_Read_Mask                                                            cBit3
#define cAf6_vcat5g_ddrstk_pen_VCAT_Read_Shift                                                               3

/*--------------------------------------
BitField Name: VCAT_Write
BitField Type: R/W
BitField Desc: Request Write from VCAT to PLA
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_vcat5g_ddrstk_pen_VCAT_Write_Mask                                                           cBit2
#define cAf6_vcat5g_ddrstk_pen_VCAT_Write_Shift                                                              2

/*--------------------------------------
BitField Name: Vcat_MFI_Read
BitField Type: R/W
BitField Desc: Delay Compensate request Read
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_vcat5g_ddrstk_pen_Vcat_MFI_Read_Mask                                                        cBit1
#define cAf6_vcat5g_ddrstk_pen_Vcat_MFI_Read_Shift                                                           1

/*--------------------------------------
BitField Name: Vcat_MFI_Write
BitField Type: R/W
BitField Desc: Delay Compensate request Write
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_vcat5g_ddrstk_pen_Vcat_MFI_Write_Mask                                                       cBit0
#define cAf6_vcat5g_ddrstk_pen_Vcat_MFI_Write_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Debug DDR Free Read Cache Number
Reg Addr   : 0x01002
Reg Formula: 
    Where  : 
Reg Desc   : 
.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_ddrblk_pen_Base                                                                 0x01002

/*--------------------------------------
BitField Name: RDCA_NUM
BitField Type: R/W
BitField Desc: VCAT Free Read Cache Number
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_vcat5g_ddrblk_pen_RDCA_NUM_Mask                                                          cBit11_0
#define cAf6_vcat5g_ddrblk_pen_RDCA_NUM_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : Debug DDR Interface Dump Control
Reg Addr   : 0x01004
Reg Formula: 
    Where  : 
Reg Desc   : 
.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_ddrdumpctrl_pen_Base                                                            0x01004

/*--------------------------------------
BitField Name: DDR_Dump_Lid
BitField Type: R/W
BitField Desc: DDR Dump Line ID
BitField Bits: [13:02]
--------------------------------------*/
#define cAf6_vcat5g_ddrdumpctrl_pen_DDR_Dump_Lid_Mask                                                 cBit13_2
#define cAf6_vcat5g_ddrdumpctrl_pen_DDR_Dump_Lid_Shift                                                       2

/*--------------------------------------
BitField Name: DDR_Dump_Sel
BitField Type: R/W
BitField Desc: Select bus info to dump 2'd0     : Write Data from VCAT to PLA
2'd1     : Read Data from PLA to VCAT 2'd2-2'd3: Reserved
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_vcat5g_ddrdumpctrl_pen_DDR_Dump_Sel_Mask                                                  cBit1_0
#define cAf6_vcat5g_ddrdumpctrl_pen_DDR_Dump_Sel_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : DDR Dump Data Buffer
Reg Addr   : 0x01100-0x011FF
Reg Formula: 0x01100 + $dump_id
    Where  : 
           + $dump_id(0-255)
Reg Desc   : 
.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_ddrdumpdat_pen_Base                                                             0x01100

/*--------------------------------------
BitField Name: DDR_Dump_Data
BitField Type: R/W
BitField Desc: DDR DUmp Data
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_vcat5g_ddrdumpdat_pen_DDR_Dump_Data_Mask                                                 cBit31_0
#define cAf6_vcat5g_ddrdumpdat_pen_DDR_Dump_Data_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Source Enable Parity Error Control
Reg Addr   : 0x20110
Reg Formula: 
    Where  : 
Reg Desc   : 
This register used to enable error parity force for control registers.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_soparena_pen_Base                                                               0x20110

/*--------------------------------------
BitField Name: SOXC_CTRL_PARENA
BitField Type: R/W
BitField Desc: Enable Force STS XC Control Register
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_vcat5g_soparena_pen_SOXC_CTRL_PARENA_Mask                                                   cBit2
#define cAf6_vcat5g_soparena_pen_SOXC_CTRL_PARENA_Shift                                                      2

/*--------------------------------------
BitField Name: SOMEM_CTRL_PARENA
BitField Type: R/W
BitField Desc: Enable Force Source Member Control Register
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_vcat5g_soparena_pen_SOMEM_CTRL_PARENA_Mask                                                  cBit1
#define cAf6_vcat5g_soparena_pen_SOMEM_CTRL_PARENA_Shift                                                     1

/*--------------------------------------
BitField Name: SOSEQ_CTRL_PARENA
BitField Type: R/W
BitField Desc: Enable Force Source Member Sequence Register
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_vcat5g_soparena_pen_SOSEQ_CTRL_PARENA_Mask                                                  cBit0
#define cAf6_vcat5g_soparena_pen_SOSEQ_CTRL_PARENA_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Source Disable Parity Error Control
Reg Addr   : 0x20111
Reg Formula: 
    Where  : 
Reg Desc   : 
This register used to disable parity error for control registers.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sopardis_pen_Base                                                               0x20111

/*--------------------------------------
BitField Name: SOXC_CTRL_PARDIS
BitField Type: R/W
BitField Desc: Disable STS XC Control Register
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_vcat5g_sopardis_pen_SOXC_CTRL_PARDIS_Mask                                                   cBit2
#define cAf6_vcat5g_sopardis_pen_SOXC_CTRL_PARDIS_Shift                                                      2

/*--------------------------------------
BitField Name: SOMEM_CTRL_PARDIS
BitField Type: R/W
BitField Desc: Disable Source Member Control Register
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_vcat5g_sopardis_pen_SOMEM_CTRL_PARDIS_Mask                                                  cBit1
#define cAf6_vcat5g_sopardis_pen_SOMEM_CTRL_PARDIS_Shift                                                     1

/*--------------------------------------
BitField Name: SOSEQ_CTRL_PARDIS
BitField Type: R/W
BitField Desc: Disable Source Member Sequence Register
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_vcat5g_sopardis_pen_SOSEQ_CTRL_PARDIS_Mask                                                  cBit0
#define cAf6_vcat5g_sopardis_pen_SOSEQ_CTRL_PARDIS_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Source Parity Error Sticky
Reg Addr   : 0x20112
Reg Formula: 
    Where  : 
Reg Desc   : 
.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_soparstk_pen_Base                                                               0x20112

/*--------------------------------------
BitField Name: SOXC_CTRL_PARERR
BitField Type: R/W
BitField Desc: Source STS XC Control Register Parity Error
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_vcat5g_soparstk_pen_SOXC_CTRL_PARERR_Mask                                                   cBit2
#define cAf6_vcat5g_soparstk_pen_SOXC_CTRL_PARERR_Shift                                                      2

/*--------------------------------------
BitField Name: SOMEM_CTRL_PARERR
BitField Type: R/W
BitField Desc: Source Member Control Register Parity Error
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_vcat5g_soparstk_pen_SOMEM_CTRL_PARERR_Mask                                                  cBit1
#define cAf6_vcat5g_soparstk_pen_SOMEM_CTRL_PARERR_Shift                                                     1

/*--------------------------------------
BitField Name: SOSEQ_CTRL_PARERR
BitField Type: R/W
BitField Desc: Source Sequence Control Register Parity Error
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_vcat5g_soparstk_pen_SOSEQ_CTRL_PARERR_Mask                                                  cBit0
#define cAf6_vcat5g_soparstk_pen_SOSEQ_CTRL_PARERR_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Sink Enable Parity Error Control
Reg Addr   : 0x40110
Reg Formula: 
    Where  : 
Reg Desc   : 
This register used to enable error parity force for control registers.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_skparena_pen_Base                                                               0x40110

/*--------------------------------------
BitField Name: SKXC_CTRL_PARENA
BitField Type: R/W
BitField Desc: Enable Force Sink STS XC Control Register
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_vcat5g_skparena_pen_SKXC_CTRL_PARENA_Mask                                                   cBit2
#define cAf6_vcat5g_skparena_pen_SKXC_CTRL_PARENA_Shift                                                      2

/*--------------------------------------
BitField Name: SKMEM_CTRL_PARENA
BitField Type: R/W
BitField Desc: Enable Force Sink Member Control Register
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_vcat5g_skparena_pen_SKMEM_CTRL_PARENA_Mask                                                  cBit1
#define cAf6_vcat5g_skparena_pen_SKMEM_CTRL_PARENA_Shift                                                     1

/*--------------------------------------
BitField Name: SKSEQ_CTRL_PARENA
BitField Type: R/W
BitField Desc: Enable Force Sink Member Sequence Register
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_vcat5g_skparena_pen_SKSEQ_CTRL_PARENA_Mask                                                  cBit0
#define cAf6_vcat5g_skparena_pen_SKSEQ_CTRL_PARENA_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Sink Disable Parity Error Control
Reg Addr   : 0x40111
Reg Formula: 
    Where  : 
Reg Desc   : 
This register used to disable parity error for control registers.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_skpardis_pen_Base                                                               0x40111

/*--------------------------------------
BitField Name: SKXC_CTRL_PARDIS
BitField Type: R/W
BitField Desc: Disable Sink STS XC Control Register
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_vcat5g_skpardis_pen_SKXC_CTRL_PARDIS_Mask                                                   cBit2
#define cAf6_vcat5g_skpardis_pen_SKXC_CTRL_PARDIS_Shift                                                      2

/*--------------------------------------
BitField Name: SKMEM_CTRL_PARDIS
BitField Type: R/W
BitField Desc: Disable Sink Member Control Register
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_vcat5g_skpardis_pen_SKMEM_CTRL_PARDIS_Mask                                                  cBit1
#define cAf6_vcat5g_skpardis_pen_SKMEM_CTRL_PARDIS_Shift                                                     1

/*--------------------------------------
BitField Name: SKSEQ_CTRL_PARDIS
BitField Type: R/W
BitField Desc: Disable Sink Member Sequence Register
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_vcat5g_skpardis_pen_SKSEQ_CTRL_PARDIS_Mask                                                  cBit0
#define cAf6_vcat5g_skpardis_pen_SKSEQ_CTRL_PARDIS_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Sink Parity Error Sticky
Reg Addr   : 0x40112
Reg Formula: 
    Where  : 
Reg Desc   : 
.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_skparstk_pen_Base                                                               0x40112

/*--------------------------------------
BitField Name: SKXC_CTRL_PARERR
BitField Type: R/W
BitField Desc: Sink STS XC Control Register Parity Error
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_vcat5g_skparstk_pen_SKXC_CTRL_PARERR_Mask                                                   cBit2
#define cAf6_vcat5g_skparstk_pen_SKXC_CTRL_PARERR_Shift                                                      2

/*--------------------------------------
BitField Name: SKMEM_CTRL_PARERR
BitField Type: R/W
BitField Desc: Sink Member Control Register Parity Error
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_vcat5g_skparstk_pen_SKMEM_CTRL_PARERR_Mask                                                  cBit1
#define cAf6_vcat5g_skparstk_pen_SKMEM_CTRL_PARERR_Shift                                                     1

/*--------------------------------------
BitField Name: SKSEQ_CTRL_PARERR
BitField Type: R/W
BitField Desc: Sink Sequence Control Register Parity Error
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_vcat5g_skparstk_pen_SKSEQ_CTRL_PARERR_Mask                                                  cBit0
#define cAf6_vcat5g_skparstk_pen_SKSEQ_CTRL_PARERR_Shift                                                     0

#endif /* _AF6_REG_AF6CCI0061_RD_VCAT_H_ */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Concate
 *
 * File        : Tha60210061ModuleConcate.c
 *
 * Created Date: Dec 6, 2016
 *
 * Description : 60210061 Concate Module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/util/ThaBitMask.h"
#include "../../Tha60210012/concate/Tha60210012ModuleConcateInternal.h"
#include "../../Tha60210012/concate/binder/Tha60210012VcgBinder.h"
#include "Tha60210061ModuleConcate.h"
#include "Tha60210061ModuleConcateReg.h"
#include "binder/Tha60210061VcgBinder.h"

/*--------------------------- Define -----------------------------------------*/
#define cNumberHwSts   48
#define cNumberHwVt    28

#define cInputSinkNumberSts     27
#define cInputSinkNumberSlice   2
#define cSourceStsNumbs         54

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210061ModuleConcate)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210061ModuleConcate
    {
    tTha60210012ModuleConcate super;

    /* Internal data */
    ThaBitMask stsProvMask;
    ThaBitMask vtProvMask[cNumberHwSts];
    uint32 asyncState;
    }tTha60210061ModuleConcate;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods                     m_AtObjectOverride;
static tAtModuleMethods                     m_AtModuleOverride;
static tAtModuleConcateMethods              m_AtModuleConcateOverride;
static tTha60210012ModuleConcateMethods     m_Tha60210012ModuleConcateOverride;

/* Save super implementations */
static const tAtObjectMethods               *m_AtObjectMethods = NULL;
static const tAtModuleMethods               *m_AtModuleMethods = NULL;
static const tAtModuleConcateMethods        *m_AtModuleConcateMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxStsChannels(Tha60210061ModuleConcate self)
    {
    AtUnused(self);
    return cNumberHwSts;
    }

static ThaBitMask CreateMask(Tha60210061ModuleConcate self)
    {
    return ThaBitMaskNew(MaxStsChannels(self));
    }

static ThaBitMask HwStsProvMask(Tha60210061ModuleConcate self)
    {
    if (self->stsProvMask == NULL)
        self->stsProvMask = CreateMask(self);
    return self->stsProvMask;
    }

static uint32 AllocateStsId(Tha60210061ModuleConcate self)
    {
    ThaBitMask mask = HwStsProvMask(self);
    uint32 freeBit = ThaBitMaskFirstZeroBit(mask);

    if (!ThaBitMaskBitPositionIsValid(mask, freeBit))
        return cInvalidUint32;

    ThaBitMaskSetBit(mask, freeBit);
    return freeBit;
    }

static void DeAllocateStsId(Tha60210061ModuleConcate self, uint32 stsId)
    {
    ThaBitMask mask = HwStsProvMask(self);
    ThaBitMaskClearBit(mask, stsId);
    }

static ThaBitMask HwVtProvMask(Tha60210061ModuleConcate self, uint8 hwStsId)
    {
    if (self->vtProvMask[hwStsId] == NULL)
        self->vtProvMask[hwStsId] = ThaBitMaskNew(cNumberHwVt);
    return self->vtProvMask[hwStsId];
    }

static eBool AllVtIsNotProv(Tha60210061ModuleConcate self, uint8 hwStsId)
    {
    uint8 bit_i;
    ThaBitMask mask = HwVtProvMask(self, hwStsId);
    for (bit_i = 0; bit_i < cNumberHwVt; bit_i++)
    	{
        if (ThaBitMaskBitVal(mask, bit_i) == 1)
            return cAtFalse;
		}
		
    return cAtTrue;
    }

static void HwVtProv(Tha60210061ModuleConcate self, uint8 hwStsId, uint8 hwVtId)
    {
    uint8 bit_i;
    ThaBitMask mask = HwVtProvMask(self, hwStsId);
    if (hwVtId == cInvalidUint8)
        {
        for (bit_i = 0; bit_i < cNumberHwVt; bit_i++)
            ThaBitMaskSetBit(mask, bit_i);
        }
    else
        ThaBitMaskSetBit(mask, hwVtId);
    }

static eBool HwVtDeProv(Tha60210061ModuleConcate self, uint8 hwStsId, uint8 hwVtId)
    {
    uint8 bit_i;
    ThaBitMask mask = HwVtProvMask(self, hwStsId);
    if (hwVtId != cInvalidUint8)
         {
         ThaBitMaskClearBit(mask, hwVtId);
         return AllVtIsNotProv(self, hwStsId);
         }

    for (bit_i = 0; bit_i < cNumberHwVt; bit_i++)
        ThaBitMaskClearBit(mask, bit_i);
        
    return cAtTrue;
    }

static eAtRet Setup(AtModule self)
    {
    eAtRet ret;
    uint32 hwStsId;

    /* Super job */
    ret = m_AtModuleMethods->Setup(self);
    if (ret != cAtOk)
        return ret;

    /* STS Prov BitMask */
    ThaBitMaskInit(mThis(self)->stsProvMask);

    /* VT Prov BitMask */
    for (hwStsId = 0; hwStsId < cNumberHwSts; hwStsId++)
        ThaBitMaskInit(mThis(self)->vtProvMask[hwStsId]);

    return cAtOk;
    }

static void DeleteProvMask(ThaBitMask *mask)
    {
    AtObjectDelete((AtObject)*mask);
    *mask = NULL;
    }

static void Delete(AtObject self)
    {
    uint32 hwStsId;
    for (hwStsId = 0; hwStsId < cNumberHwSts; hwStsId++)
        DeleteProvMask(&mThis(self)->vtProvMask[hwStsId]);

    DeleteProvMask(&mThis(self)->stsProvMask);
    m_AtObjectMethods->Delete(self);
    }

static uint32 SourceStsOffset(uint8 hwSlice, uint8 hwStsInSlice)
    {
    return (uint32) ((hwSlice * cInputSinkNumberSts) + hwStsInSlice);
    }

static uint32 SinkStsOffset(uint8 hwSlice, uint8 hwStsInSlice)
    {
    return (uint32) ((hwSlice * 32UL) + hwStsInSlice);
    }

static eAtRet HwSourceStsProvision(Tha60210012ModuleConcate self, uint8 hwSlice, uint8 hwStsInSlice, uint8 sts48ProvId)
    {
    uint32 sourceSts = SourceStsOffset(hwSlice, hwStsInSlice);
    uint32 regVal = 0;
    uint32 baseAddress = Tha60210012ModuleConcateBaseAddress(self);
    if (sourceSts < cSourceStsNumbs)
        {
        mRegFieldSet(regVal, cAf6_vcat5g_so_stsxc_pen_OC24_OutSlice_,  hwSlice);
        mRegFieldSet(regVal, cAf6_vcat5g_so_stsxc_pen_OC27_OutSTS_,  hwStsInSlice);
        }
    else
        {
        mRegFieldSet(regVal, cAf6_vcat5g_so_stsxc_pen_OC24_OutSlice_,  cInvalidUint8);
        mRegFieldSet(regVal, cAf6_vcat5g_so_stsxc_pen_OC27_OutSTS_,  cInvalidUint8);
        }

    mModuleHwWrite(self, baseAddress + (uint32)(cAf6Reg_vcat5g_so_stsxc_pen_Base + sts48ProvId), regVal);
    return cAtOk;
    }

static eAtRet HwSinkStsProvision(Tha60210012ModuleConcate self, uint8 hwSlice, uint8 hwStsInSlice, uint8 sts48ProvId)
    {
    uint32 regVal = 0;
    uint32 baseAddress = Tha60210012ModuleConcateBaseAddress(self);
    mRegFieldSet(regVal, cAf6_vcat5g_sk_stsxc_pen_OC48_OutSTS_,  sts48ProvId);
    mModuleHwWrite(self, (uint32)(baseAddress +  cAf6Reg_vcat5g_sk_stsxc_pen_Base + SinkStsOffset(hwSlice, hwStsInSlice)), regVal);
    return cAtOk;
    }

static eBool StsIsValid(uint8 sts48ProvId)
    {
    return (sts48ProvId < cNumberHwSts) ? cAtTrue: cAtFalse;
    }

static eBool SinkStsIsProvisioned(AtModuleConcate self, uint8 sts48ProvId)
    {
    ThaBitMask mask = HwStsProvMask(mThis(self));
    return (ThaBitMaskBitVal(mask, sts48ProvId) == 1) ? cAtTrue : cAtFalse;
    }

static uint8 HwSinkProvisionedStsGet(AtModuleConcate self, uint8 hwSlice, uint8 hwStsInSlice)
    {
    uint32 baseAddress, regVal;
    uint8 hwSts;

    if (AtModuleInAccessible((AtModule)self))
        return cInvalidUint8;

    baseAddress = Tha60210012ModuleConcateBaseAddress((Tha60210012ModuleConcate)self);
    regVal = mModuleHwRead(self, (uint32)(baseAddress +  cAf6Reg_vcat5g_sk_stsxc_pen_Base + SinkStsOffset(hwSlice, hwStsInSlice)));
    hwSts = (uint8)mRegField(regVal, cAf6_vcat5g_sk_stsxc_pen_OC48_OutSTS_);
    if (!StsIsValid(hwSts))
        return cInvalidUint8;

    if (SinkStsIsProvisioned(self, hwSts))
        return hwSts;

    return cInvalidUint8;
    }

static void HwSourceProvisionedStsGet(Tha60210012ModuleConcate self, uint8 mapStsId, uint8 *hwPdhSliceId, uint8 *hwPdhStsId)
    {
    uint32 baseAddress = Tha60210012ModuleConcateBaseAddress(self);
    uint32 regVal = mModuleHwRead(self, baseAddress + (uint32)(cAf6Reg_vcat5g_so_stsxc_pen_Base + mapStsId));

    *hwPdhSliceId = (uint8)mRegField(regVal, cAf6_vcat5g_so_stsxc_pen_OC24_OutSlice_);
    *hwPdhStsId = (uint8)mRegField(regVal, cAf6_vcat5g_so_stsxc_pen_OC27_OutSTS_);
    }


static void HwMapStsIdSliceSts24Get(Tha60210012ModuleConcate self, uint8 mapStsId, uint8 *hwPdhSliceId, uint8 *hwPdhStsId)
    {
    if (SinkStsIsProvisioned((AtModuleConcate)self, mapStsId))
        {
        /* Return from database */
        HwSourceProvisionedStsGet(self, mapStsId, hwPdhSliceId, hwPdhStsId);
        return;
        }

    *hwPdhSliceId = cInvalidUint8;
    *hwPdhStsId = cInvalidUint8;
    }

static void HwXcReset(Tha60210012ModuleConcate self)
	{
    uint8 sts_i, slice_i;

    /* Source: DeProvsion */
    for (sts_i = 0; sts_i < cNumberHwSts; sts_i++)
        HwSourceStsProvision(self, cInvalidUint8, cInvalidUint8, sts_i);

    /* Sink : DeProvsion */
    for (slice_i = 0; slice_i < cInputSinkNumberSlice; slice_i++)
        {
        for (sts_i = 0; sts_i < cInputSinkNumberSts; sts_i++)
            HwSinkStsProvision(self, slice_i, sts_i, cInvalidUint8);
        }
	}

static void  HwE1UnframeReset(Tha60210012ModuleConcate self)
	{
    uint32 sts_i;
    uint32 baseAddress = Tha60210012ModuleConcateBaseAddress(self);

    /* Source: FrameMode as default */
    for (sts_i = 0; sts_i < cNumberHwSts; sts_i++)
        mModuleHwWrite(self, (uint32)(baseAddress +  cAf6Reg_vcat5g_so_unfrm_pen_Base + sts_i), 0x0);

    /* Sink: FrameMode as default */
    for (sts_i = 0; sts_i < cInputSinkNumberSts; sts_i ++)
        {
        mModuleHwWrite(self, (uint32)(baseAddress +  cAf6Reg_vcat5g_sk_sl0_unfrm_pen_Base + sts_i), 0x0);/* Default Frame Mode */
        mModuleHwWrite(self, (uint32)(baseAddress +  cAf6Reg_vcat5g_sk_sl1_unfrm_pen_Base + sts_i), 0x0);
        }
	}

static void HwMuxReset(Tha60210012ModuleConcate self)
	{
    const uint32 cMuxSelPdhDefault = cBit31_0;
    uint32 sts_i;
    uint32 baseAddress = Tha60210012ModuleConcateBaseAddress(self);
    for (sts_i = 0; sts_i < cInputSinkNumberSts; sts_i ++)
        {
        mModuleHwWrite(self, (uint32)(baseAddress +  cAf6Reg_vcat5g_sk_sl0_mux_pen_Base + sts_i), cMuxSelPdhDefault); /* Default select from PDH */
        mModuleHwWrite(self, (uint32)(baseAddress +  cAf6Reg_vcat5g_sk_sl1_mux_pen_Base + sts_i), cMuxSelPdhDefault); /* Default select from PDH */
        }
	}

static AtVcgBinder CreateVcgBinderForDe1(AtModuleConcate self, AtPdhDe1 de1)
    {
    AtUnused(self);
    return (AtVcgBinder)Tha60210061VcgBinderPdhDe1New(de1);
    }

static AtVcgBinder CreateVcgBinderForDe3(AtModuleConcate self, AtPdhDe3 de3)
    {
    AtUnused(self);
    return (AtVcgBinder)Tha60210061VcgBinderPdhDe3New(de3);
    }

static uint8 HwStsSlice24ToMapStsId(Tha60210012ModuleConcate self,  uint8 hwPdhSliceId, uint8 hwPdhStsId)
    {
    return HwSinkProvisionedStsGet((AtModuleConcate)self, hwPdhSliceId, hwPdhStsId);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210061ModuleConcate* object = (tTha60210061ModuleConcate*)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObject(stsProvMask);
    mEncodeObjects(vtProvMask, mCount(object->vtProvMask));
    mEncodeNone(asyncState);
    }

static uint32 DeskewLatencyResolutionInFrames(AtModuleConcate self, eAtConcateMemberType memberType)
    {
    AtUnused(self);
    switch ((uint32) memberType)
        {
        case cAtConcateMemberTypeVc4:  return 1;
        case cAtConcateMemberTypeVc3:  return 1;
        case cAtConcateMemberTypeDs3:  return 1;
        case cAtConcateMemberTypeE3:   return 1;
        case cAtConcateMemberTypeDs1:  return 4;
        case cAtConcateMemberTypeE1:   return 4;
        default: return 0;
        }
    }

static uint32 DeskewLatencyOffset(AtModuleConcate self, eAtConcateMemberType memberType)
    {
    AtUnused(self);
    switch ((uint32) memberType)
        {
        case cAtConcateMemberTypeVc4:  return 3;
        case cAtConcateMemberTypeVc3:  return 2;
        case cAtConcateMemberTypeDs3:  return 1;
        case cAtConcateMemberTypeE3:   return 1;
        case cAtConcateMemberTypeDs1:  return 0;
        case cAtConcateMemberTypeE1:   return 0;
        default: return cInvalidUint32;
        }
    }

static eAtRet HwDeskewLatencyThresholdSet(AtModuleConcate self, uint32 offset, uint32 thresholdInRes)
    {
    uint32 baseAddress = Tha60210012ModuleConcateBaseAddress((Tha60210012ModuleConcate)self);
    uint32 regAddr = (uint32)(baseAddress +  cAf6Reg_vcat5g_sk_deskew_pen_Base + offset);
    uint32 regVal = mModuleHwRead(self, regAddr);

    mRegFieldSet(regVal, cAf6_vcat5g_sk_deskew_pen_Sk_Mem_DskwEna_,  1);
    mRegFieldSet(regVal, cAf6_vcat5g_sk_deskew_pen_Sk_Mem_DskwVal_,  thresholdInRes);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static uint32 HwDeskewLatencyThresholdGet(AtModuleConcate self, uint32 offset)
    {
    uint32 baseAddress = Tha60210012ModuleConcateBaseAddress((Tha60210012ModuleConcate)self);
    uint32 regAddr = (uint32)(baseAddress +  cAf6Reg_vcat5g_sk_deskew_pen_Base + offset);
    uint32 regVal = mModuleHwRead(self, regAddr);
    if ((regVal & cAf6_vcat5g_sk_deskew_pen_Sk_Mem_DskwEna_Mask))
        return mRegField(regVal, cAf6_vcat5g_sk_deskew_pen_Sk_Mem_DskwVal_);
    return 0;
    }

static eBool ThresholdInResInRange(uint32 thresholdInRes)
    {
    if (thresholdInRes == 0)
        return cAtFalse;

    return (thresholdInRes > cAf6_vcat5g_sk_deskew_pen_Sk_Mem_DskwVal_Mask) ? cAtFalse : cAtTrue;
    }

static eAtRet DeskewLatencyThresholdSet(AtModuleConcate self, eAtConcateMemberType memberType, uint32 thresholdInRes)
    {
    uint32 offset = DeskewLatencyOffset(self, memberType);
    if (offset == cInvalidUint32)
        return cAtErrorModeNotSupport;

    if (ThresholdInResInRange(thresholdInRes))
        return HwDeskewLatencyThresholdSet(self, offset, thresholdInRes);

    return cAtErrorOutOfRangParm;
    }

static uint32 DeskewLatencyThresholdGet(AtModuleConcate self, eAtConcateMemberType memberType)
    {
    uint32 offset = DeskewLatencyOffset(self, memberType);
    if (offset == cInvalidUint32)
        return 0;
    return HwDeskewLatencyThresholdGet(self, offset);
    }

static eAtRet DeskewLatencyDefault(AtModuleConcate self)
    {
    eAtRet ret = cAtOk;
    uint32 memberType;

    for (memberType = cAtConcateMemberTypeStart; memberType <= cAtConcateMemberTypeEnd; memberType ++)
        {
        if (AtModuleConcateMemberTypeIsSupported(self, memberType))
            ret |= AtModuleConcateDeskewLatencyThresholdSet(self, memberType, 1);
        }

    return ret;
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return DeskewLatencyDefault((AtModuleConcate)self);
    }

static eAtRet SuperAsyncInit(AtModule self)
    {
    return m_AtModuleMethods->AsyncInit(self);
    }

static tAtAsyncOperationFunc NextAsyncOp(AtObject self, uint32 state)
    {
    static tAtAsyncOperationFunc func[] = {(tAtAsyncOperationFunc)SuperAsyncInit,
                                           (tAtAsyncOperationFunc)DeskewLatencyDefault};
    AtUnused(self);
    if (state >= mCount(func))
        return NULL;

    return func[state];
    }

static eAtRet AsyncInit(AtModule self)
    {
    return AtDeviceObjectAsyncProcess(AtModuleDeviceGet(self), (AtObject)self, &mThis(self)->asyncState, NextAsyncOp);
    }

static void OverrideTha60210012ModuleConcate(AtModuleConcate self)
    {
	Tha60210012ModuleConcate concate = (Tha60210012ModuleConcate)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210012ModuleConcateOverride, mMethodsGet(concate), sizeof(m_Tha60210012ModuleConcateOverride));

        /* Setup methods */
        mMethodOverride(m_Tha60210012ModuleConcateOverride, HwXcReset);
        mMethodOverride(m_Tha60210012ModuleConcateOverride, HwE1UnframeReset);
        mMethodOverride(m_Tha60210012ModuleConcateOverride, HwMuxReset);
        mMethodOverride(m_Tha60210012ModuleConcateOverride, HwStsSlice24ToMapStsId);
        mMethodOverride(m_Tha60210012ModuleConcateOverride, HwMapStsIdSliceSts24Get);
        }

    mMethodsSet(concate, &m_Tha60210012ModuleConcateOverride);
    }

static void OverrideAtModuleConcate(AtModuleConcate self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleConcateMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleConcateOverride, mMethodsGet(self), sizeof(m_AtModuleConcateOverride));

        mMethodOverride(m_AtModuleConcateOverride, CreateVcgBinderForDe1);
        mMethodOverride(m_AtModuleConcateOverride, CreateVcgBinderForDe3);

        mMethodOverride(m_AtModuleConcateOverride, DeskewLatencyResolutionInFrames);
        mMethodOverride(m_AtModuleConcateOverride, DeskewLatencyThresholdSet);
        mMethodOverride(m_AtModuleConcateOverride, DeskewLatencyThresholdGet);
        }

    mMethodsSet(self, &m_AtModuleConcateOverride);
    }

static void OverrideAtModule(AtModuleConcate self)
    {
    AtModule module = (AtModule) self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Setup);
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtObject(AtModuleConcate self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtModuleConcate self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    OverrideAtModuleConcate(self);
    OverrideTha60210012ModuleConcate(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061ModuleConcate);
    }

static AtModuleConcate ObjectInit(AtModuleConcate self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012ModuleConcateObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleConcate Tha60210061ModuleConcateNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleConcate concateModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (concateModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(concateModule, device);
    }

uint8 Tha60210061ModuleConcateHwStsAllocate(Tha60210012ModuleConcate self, uint8 hwSlice, uint8 hwStsInSlice, uint8 hwVtId)
    {
    uint8 provStsId = Tha60210012ModuleConcateHwStsSlice24ToMapStsId(self, hwSlice, hwStsInSlice);
    if (provStsId != cInvalidUint8)
        {
        /* VT Allocate */
        HwVtProv(mThis(self), provStsId, hwVtId);
        return provStsId;
        }

    provStsId = (uint8)AllocateStsId(mThis(self));
    if (provStsId == cInvalidUint8)
        return provStsId;

    /* Set up hardware */
    HwSourceStsProvision(self, hwSlice, hwStsInSlice, provStsId);
    HwSinkStsProvision(self, hwSlice, hwStsInSlice, provStsId);

    /* VT Allocate */
    HwVtProv(mThis(self), provStsId, hwVtId);
    return provStsId;
    }

eAtRet Tha60210061ModuleConcateHwStsDeAllocate(Tha60210012ModuleConcate self, uint8 hwSlice, uint8 hwStsInSlice, uint8 hwVtId)
    {
    uint8 provStsId = Tha60210012ModuleConcateHwStsSlice24ToMapStsId(self, hwSlice, hwStsInSlice);
    if (provStsId == cInvalidUint8)
        return cAtErrorRsrcNoAvail;

    if (HwVtDeProv(mThis(self), provStsId, hwVtId))
        {
        /* DeProv hardare */
        HwSourceStsProvision(self, cInvalidUint8, cInvalidUint8, provStsId);
        HwSinkStsProvision(self, hwSlice, hwStsInSlice, cInvalidUint8);
        DeAllocateStsId(mThis(self), provStsId);
        }

    return cAtOk;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Concate
 *
 * File        : Tha60210012VcgBinderPdhDe3Binder.c
 *
 * Created Date: Dec 7, 2016
 *
 * Description : De3 Binder
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210061VcgBinderInternal.h"
#include "Tha60210061VcgBinder.h"
#include "../Tha60210061ModuleConcate.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods                     m_AtObjectOverride;
static tAtVcgBinderMethods                  m_AtVcgBinderOverride;

/* Save super implementation */
static const tAtObjectMethods               *m_AtObjectMethods = NULL;
static const tAtVcgBinderMethods            *m_AtVcgBinderMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPdhChannel PdhChannel(ThaVcgBinder self)
    {
    return (AtPdhChannel)AtVcgBinderChannelGet((AtVcgBinder)self);
    }

static Tha60210012ModuleConcate ModuleConcate(AtPdhChannel channel)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel) channel);
    return (Tha60210012ModuleConcate)AtDeviceModuleGet(device, cAtModuleConcate);
    }

static eAtRet ResourceAllocate(AtVcgBinder self)
    {
    uint8 hwSliceId, hwStsId, hwFlatStsId;
    AtPdhChannel channel = PdhChannel((ThaVcgBinder) self);
    Tha60210012ModuleConcate concateModule = ModuleConcate(channel);

    ThaPdhChannelHwIdGet(channel, cAtModulePdh, &hwSliceId, &hwStsId);
    hwFlatStsId = Tha60210061ModuleConcateHwStsAllocate(concateModule, hwSliceId, hwStsId, cInvalidUint8);
    if (hwFlatStsId == cInvalidUint8)
        {
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelWarning, AtSourceLocation, "Concate resource is full!\r\n");
        return cAtErrorRsrcNoAvail;
        }

    return cAtOk;
    }

static AtConcateMember MemberObjectCreate(AtVcgBinder self, AtConcateGroup vcg)
    {
    if (ResourceAllocate(self) == cAtOk)
        return m_AtVcgBinderMethods->MemberObjectCreate(self, vcg);

    return NULL;
    }

static void ResourceDeAllocate(AtVcgBinder self)
    {
    uint8 hwSliceId, hwStsId;
    AtPdhChannel channel = PdhChannel((ThaVcgBinder) self);
    
    Tha60210012ModuleConcate concateModule = ModuleConcate(channel);
    ThaPdhChannelHwIdGet(channel, cAtModulePdh, &hwSliceId, &hwStsId);
    Tha60210061ModuleConcateHwStsDeAllocate(concateModule, hwSliceId, hwStsId, cInvalidUint8);
    }

static void Delete(AtObject self)
    {
    ResourceDeAllocate((AtVcgBinder) self);
    m_AtObjectMethods->Delete(self);
    }

static void OverrideAtVcgBinder(ThaVcgBinder self)
    {
    AtVcgBinder binder = (AtVcgBinder)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtVcgBinderMethods = mMethodsGet(binder);
        mMethodsGet(osal)->MemCpy(osal, &m_AtVcgBinderOverride, m_AtVcgBinderMethods, sizeof(m_AtVcgBinderOverride));

        mMethodOverride(m_AtVcgBinderOverride, MemberObjectCreate);
        }

    mMethodsSet(binder, &m_AtVcgBinderOverride);
    }

static void OverrideAtObject(ThaVcgBinder self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(ThaVcgBinder self)
    {
    OverrideAtObject(self);
    OverrideAtVcgBinder(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210061VcgBinderPdhDe3);
    }

static ThaVcgBinder ObjectInit(ThaVcgBinder self, AtPdhDe3 de3)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012VcgBinderPdhDe3ObjectInit(self, de3) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaVcgBinder Tha60210061VcgBinderPdhDe3New(AtPdhDe3 de3)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaVcgBinder newBinder = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newBinder == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newBinder, de3);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Concate
 * 
 * File        : Tha60210061VcgBinderInternal.h
 * 
 * Created Date: Dec 7, 2016
 *
 * Description : Binder
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061VCGBINDERINTERNAL_H_
#define _THA60210061VCGBINDERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../Tha60210012/concate/binder/Tha60210012VcgBinderInternal.h"
#include "../../../Tha60210012/concate/Tha60210012ModuleConcate.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

typedef struct tTha60210061VcgBinderPdhDe1
    {
    tTha60210012VcgBinderPdhDe1 super;
    }tTha60210061VcgBinderPdhDe1;

typedef struct tTha60210061VcgBinderPdhDe3
    {
    tTha60210012VcgBinderPdhDe3 super;
    }tTha60210061VcgBinderPdhDe3;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60210061VCGBINDERINTERNAL_H_ */


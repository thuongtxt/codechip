/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Concate
 * 
 * File        : Tha60210061VcgBinder.h
 * 
 * Created Date: Dec 7, 2016
 *
 * Description : Binder
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210061VCGBINDER_H_
#define _THA60210061VCGBINDER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaVcgBinder Tha60210061VcgBinderPdhDe3New(AtPdhDe3 de3);
ThaVcgBinder Tha60210061VcgBinderPdhDe1New(AtPdhDe1 de1);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210061VCGBINDER_H_ */


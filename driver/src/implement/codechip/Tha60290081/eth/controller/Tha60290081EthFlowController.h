/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Ethernet
 * 
 * File        : Tha60290081EthFlowController.h
 * 
 * Created Date: Jul 16, 2019
 *
 * Description : 60290081 Ethernet flow controllers
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290081ETHFLOWCONTROLLER_H_
#define _THA60290081ETHFLOWCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleEth.h"
#include "../../../../default/eth/controller/ThaEthFlowController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaEthFlowController Tha60290081EosEthFlowControllerNew(AtModuleEth module);
ThaEthFlowController Tha60290081PwEthFlowControllerNew(AtModuleEth module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290081ETHFLOWCONTROLLER_H_ */


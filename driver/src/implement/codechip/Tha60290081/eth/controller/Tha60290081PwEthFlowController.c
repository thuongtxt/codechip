/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Ethernet
 *
 * File        : Tha60290081PwEthFlowController.c
 *
 * Created Date: Jul 16, 2019
 *
 * Description : 60290081 PW Ethernet flow controller implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/man/ThaDevice.h"
#include "../../pda/Tha60290081ModulePda.h"
#include "../../cla/Tha60290081ModuleCla.h"
#include "Tha60290081EthFlowController.h"
#include "Tha60290081EthFlowControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaEthFlowControllerMethods         m_ThaEthFlowControllerOverride;

/* Supper */
static const tThaEthFlowControllerMethods *m_ThaEthFlowControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleCla ModuleCla(AtEthFlow flow)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)flow);
    return (ThaModuleCla)AtDeviceModuleGet(device, cThaModuleCla);
    }

static eBool MacIsProgrammable(ThaEthFlowController self, AtEthFlow ethFlow)
    {
    AtUnused(self);
    AtUnused(ethFlow);
    return cAtTrue;
    }

static eBool HasEthernetType(ThaEthFlowController self, AtEthFlow flow)
    {
    AtUnused(flow);
    AtUnused(self);
    /* Ethernet type should be controlled in PW PSN header */
    return cAtFalse;
    }

static eAtRet Activate(ThaEthFlowController self, AtEthFlow flow, AtChannel channel, uint8 classNumber)
    {
    eAtRet ret;

    AtEthFlowTypeSet(flow, cAtEthFlowTypePw);

    ret = m_ThaEthFlowControllerMethods->Activate(self, flow, channel, classNumber);
    if (ret != cAtOk)
        return ret;

    return ret;
    }

static eAtRet DeActivate(ThaEthFlowController self, AtEthFlow flow)
    {
    eAtRet ret = m_ThaEthFlowControllerMethods->DeActivate(self, flow);
    if (ret != cAtOk)
        return ret;

    return cAtOk;
    }

static eAtRet AllIngressVlansAdd(ThaEthFlowController self, AtEthFlow flow)
    {
    eAtRet ret = cAtOk;
    uint8  vlan_i;

    for (vlan_i = 0; vlan_i < AtEthFlowIngressNumVlansGet(flow); vlan_i++)
        {
        tAtEthVlanDesc vlan;
        AtEthFlowIngressVlanAtIndex(flow, vlan_i, &vlan);
        ret |= mMethodsGet(self)->IngressVlanEnable(self, flow, &vlan);
        }

    return ret;
    }

static eAtRet HelperIngressVlanEnable(ThaEthFlowController self, AtEthFlow flow, const tAtEthVlanDesc *desc, eBool enable)
    {
    ThaModuleCla moduleCla = ModuleCla(flow);
    eAtRet ret = cAtErrorModeNotSupport;
    tAtEthVlanTag cVlan, sVlan;
    AtUnused(self);

    if (desc->numberOfVlans == 0)
        return cAtOk;

    if (desc->numberOfVlans == 1)
        {
        cVlan = desc->vlans[0];
        ret  = Tha60290081ModuleClaPwEthFlowFirstVlanEnable(moduleCla, flow, &cVlan, enable);
        }

    if (desc->numberOfVlans == 2)
        {
        sVlan = desc->vlans[1];
        cVlan = desc->vlans[0];
        ret  = Tha60290081ModuleClaPwEthFlowFirstVlanEnable(moduleCla, flow, &sVlan, enable);
        ret |= Tha60290081ModuleClaPwEthFlowSecondVlanEnable(moduleCla, flow, &cVlan, enable);
        }

    return ret;
    }

static eAtRet IngressVlanDisable(ThaEthFlowController self, AtEthFlow flow, const tAtEthVlanDesc *desc)
    {
    return HelperIngressVlanEnable(self, flow, desc, cAtFalse);
    }

static eAtRet IngressVlanEnable(ThaEthFlowController self, AtEthFlow flow, const tAtEthVlanDesc *desc)
    {
    return HelperIngressVlanEnable(self, flow, desc, cAtTrue);
    }

static void OverrideThaEthFlowController(ThaEthFlowController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaEthFlowControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthFlowControllerOverride, m_ThaEthFlowControllerMethods, sizeof(m_ThaEthFlowControllerOverride));
        mMethodOverride(m_ThaEthFlowControllerOverride, MacIsProgrammable);
        mMethodOverride(m_ThaEthFlowControllerOverride, HasEthernetType);
        mMethodOverride(m_ThaEthFlowControllerOverride, Activate);
        mMethodOverride(m_ThaEthFlowControllerOverride, DeActivate);
        mMethodOverride(m_ThaEthFlowControllerOverride, AllIngressVlansAdd);
        mMethodOverride(m_ThaEthFlowControllerOverride, IngressVlanDisable);
        mMethodOverride(m_ThaEthFlowControllerOverride, IngressVlanEnable);
        }

    mMethodsSet(self, &m_ThaEthFlowControllerOverride);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081PwEthFlowController);
    }

static void Override(ThaEthFlowController self)
    {
    OverrideThaEthFlowController(self);
    }

static ThaEthFlowController ObjectInit(ThaEthFlowController self, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290081EthFlowControllerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaEthFlowController Tha60290081PwEthFlowControllerNew(AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaEthFlowController newFlow = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newFlow == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newFlow, module);
    }

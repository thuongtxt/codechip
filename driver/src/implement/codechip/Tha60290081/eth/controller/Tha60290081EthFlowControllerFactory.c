/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Ethernet
 *
 * File        : Tha60290081EthFlowControllerFactory.c
 *
 * Created Date: Jul 16, 2019
 *
 * Description : 60290081 Flow controller factory implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/eth/controller/ThaEthFlowControllerFactoryInternal.h"
#include "Tha60290081EthFlowController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290081EthFlowControllerFactory
    {
    tThaEthFlowControllerFactory super;
    }tTha60290081EthFlowControllerFactory;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaEthFlowControllerFactoryMethods m_ThaEthFlowControllerFactoryOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaEthFlowController PppFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule)
    {
    AtUnused(self);
    AtUnused(ethModule);
    return NULL;
    }

static ThaEthFlowController OamFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule)
    {
    AtUnused(self);
    AtUnused(ethModule);
    return NULL;
    }

static ThaEthFlowController MpFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule)
    {
    AtUnused(self);
    AtUnused(ethModule);
    return NULL;
    }

static ThaEthFlowController CiscoHdlcFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule)
    {
    AtUnused(self);
    AtUnused(ethModule);
    return NULL;
    }

static ThaEthFlowController FrFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule)
    {
    AtUnused(self);
    AtUnused(ethModule);
    return NULL;
    }

static ThaEthFlowController EncapFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule)
    {
    AtUnused(self);
    return Tha60290081EosEthFlowControllerNew(ethModule);
    }

static ThaEthFlowController PwFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule)
    {
    AtUnused(self);
    return Tha60290081PwEthFlowControllerNew(ethModule);
    }

static void OverrideThaEthFlowControllerFactory(ThaEthFlowControllerFactory self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthFlowControllerFactoryOverride, mMethodsGet(self), sizeof(m_ThaEthFlowControllerFactoryOverride));

        mMethodOverride(m_ThaEthFlowControllerFactoryOverride, PppFlowControllerCreate);
        mMethodOverride(m_ThaEthFlowControllerFactoryOverride, MpFlowControllerCreate);
        mMethodOverride(m_ThaEthFlowControllerFactoryOverride, CiscoHdlcFlowControllerCreate);
        mMethodOverride(m_ThaEthFlowControllerFactoryOverride, FrFlowControllerCreate);
        mMethodOverride(m_ThaEthFlowControllerFactoryOverride, OamFlowControllerCreate);
        mMethodOverride(m_ThaEthFlowControllerFactoryOverride, EncapFlowControllerCreate);
        mMethodOverride(m_ThaEthFlowControllerFactoryOverride, PwFlowControllerCreate);
        }

    mMethodsSet(self, &m_ThaEthFlowControllerFactoryOverride);
    }

static void Override(ThaEthFlowControllerFactory self)
    {
    OverrideThaEthFlowControllerFactory(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081EthFlowControllerFactory);
    }

static ThaEthFlowControllerFactory ObjectInit(ThaEthFlowControllerFactory self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Constructor */
    if (ThaEthFlowControllerFactoryObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaEthFlowControllerFactory Tha60290081EthFlowControllerFactory(void)
    {
    static ThaEthFlowControllerFactory m_factory = NULL;
    static tTha60290081EthFlowControllerFactory factory;

    if (m_factory == NULL)
        m_factory = ObjectInit((ThaEthFlowControllerFactory)&factory);

    return m_factory;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Ethernet
 *
 * File        : Tha60290081EthFlowController.c
 *
 * Created Date: Jul 16, 2019
 *
 * Description : 60290081 ETH flow controller implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/man/ThaDevice.h"
#include "../../../../default/eth/ThaEthFlowInternal.h"
#include "../../../../default/pmc/ThaModulePmc.h"
#include "../../pwe/Tha60290081ModulePwe.h"
#include "../../cla/Tha60290081ModuleCla.h"
#include "../../mpeg/Tha60290081ModuleMpeg.h"
#include "Tha60290081EthFlowController.h"
#include "Tha60290081EthFlowControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mCounterByTypeGet(eType, CntGet)                                       \
        {                                                                      \
        if (counterType == eType)                                              \
            return CntGet(modulePmc, flow, r2c);                               \
        }                                                                      \

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaEthFlowControllerMethods m_ThaEthFlowControllerOverride;

/* Supper */
static const tThaEthFlowControllerMethods *m_ThaEthFlowControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static Tha60290081ModulePwe ModulePwe(AtEthFlow flow)
    {
    return (Tha60290081ModulePwe)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)flow), cThaModulePwe);
    }

static ThaModuleCla ModuleCla(AtEthFlow flow)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)flow);
    return (ThaModuleCla)AtDeviceModuleGet(device, cThaModuleCla);
    }

static Tha60290081ModuleMpeg ModuleMpeg(AtEthFlow flow)
    {
    return (Tha60290081ModuleMpeg)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)flow), cThaModuleMpeg);
    }

static eAtRet PsnBufferGet(ThaEthFlowController self, AtEthFlow flow, uint8 *pBuffer, uint32 *psnLen)
    {
    Tha60290081ModulePwe modulePwe = ModulePwe(flow);
    uint32 psnLength = Tha60290081ModulePweFlowHeaderLengthGet(modulePwe, flow);

    *psnLen = psnLength;
    if (psnLength == 0)
        {
        *psnLen = (uint32)mMethodsGet(self)->EthHeaderNoVlanLengthGet(self, flow);
        return cAtOk;
        }

    return Tha60290081ModulePweFlowHeaderRead(ModulePwe(flow), flow, pBuffer, psnLen);
    }

static eAtRet PsnBufferSet(ThaEthFlowController self, AtEthFlow flow, uint8 *pBuffer, uint32 psnLen)
    {
    Tha60290081ModulePwe modulePwe = ModulePwe(flow);
    eAtRet ret = cAtOk;
    AtUnused(self);

    ret |= Tha60290081ModulePweFlowHeaderWrite(modulePwe, flow, pBuffer, psnLen);
    ret |= Tha60290081ModulePweFlowHeaderLengthSet(modulePwe, flow, psnLen);

    return ret;
    }

static eAtRet Activate(ThaEthFlowController self, AtEthFlow flow, AtChannel channel, uint8 classNumber)
    {
    eAtRet ret = m_ThaEthFlowControllerMethods->Activate(self, flow, channel, classNumber);
    if (ret != cAtOk)
        return ret;

    ret |= Tha60290081ModuleClaEthFlowChannelSet(ModuleCla(flow), flow, channel);
    return ret;
    }

static eAtRet DeActivate(ThaEthFlowController self, AtEthFlow flow)
    {
    return m_ThaEthFlowControllerMethods->DeActivate(self, flow);
    }

static eAtRet NumberEgVlanSet(ThaEthFlowController self, AtEthFlow flow, uint8 vlanTagNum)
    {
    AtUnused(self);
    AtUnused(flow);
    AtUnused(vlanTagNum);
    return cAtOk;
    }

static void PsnLengthReset(ThaEthFlowController self, AtEthFlow flow)
    {
    AtUnused(self);
    AtUnused(flow);
    }

static uint16 EthTypeStartIndex(ThaEthFlowController self, AtEthFlow flow)
    {
    uint32 startEthHeader = mMethodsGet(self)->StartEthHeaderInPsnBuffer(self);
    uint32 numMacAddr = mMethodsGet(self)->MacIsProgrammable(self, flow) ? 2 : 0;
    uint32 numVlanTags = AtEthFlowEgressNumVlansGet(flow);

    return (uint16)(startEthHeader + cAtMacAddressLen * numMacAddr + cNumByteOfVlanTag * numVlanTags);
    }

static eAtRet EthernetTypeSet(ThaEthFlowController self, AtEthFlow flow)
    {
    eAtRet ret;
    uint8  buffer[cThaPsnHdrMaxLength];
    uint32 psnLen;
    uint16 ethTypeStartPosition = EthTypeStartIndex(self, flow);
    AtOsal osal = AtSharedDriverOsalGet();

    /* Get header */
    mMethodsGet(osal)->MemInit(osal, buffer, 0, cThaPsnHdrMaxLength);
    ret = mMethodsGet(self)->PsnBufferGet(self, flow, buffer, &psnLen);
    if (ret != cAtOk)
        return ret;

    /* Put Ethernet type */
    buffer[ethTypeStartPosition]     = 0x88;
    buffer[ethTypeStartPosition + 1] = 0x63;

    return mMethodsGet(self)->PsnBufferSet(self, flow, buffer, psnLen);
    }

static eAtRet HelperIngressVlanEnable(ThaEthFlowController self, AtEthFlow flow, const tAtEthVlanDesc *desc, eBool enable)
    {
    eAtRet ret = cAtErrorModeNotSupport;
    tAtEthVlanTag cVlan, sVlan;
    AtUnused(self);

    if (desc == NULL)
        return cAtErrorNullPointer;

    if (desc->numberOfVlans == 0)
        return cAtErrorRsrcNoAvail;

    if (desc->numberOfVlans == 1)
        {
        cVlan = desc->vlans[0];
        ret = Tha60290081ModuleClaEthFlowExpectedFirstVlanSet(ModuleCla(flow), flow, &cVlan, enable);
        }

    if (desc->numberOfVlans == 2)
        {
        sVlan = desc->vlans[1];
        cVlan = desc->vlans[0];
        ret  = Tha60290081ModuleClaEthFlowExpectedFirstVlanSet(ModuleCla(flow), flow, &sVlan, enable);
        ret |= Tha60290081ModuleClaEthFlowExpectedSecondVlanSet(ModuleCla(flow), flow, &cVlan, enable);
        }

    return ret;
    }

static eAtRet IngressVlanDisable(ThaEthFlowController self, AtEthFlow flow, const tAtEthVlanDesc *desc)
    {
    return HelperIngressVlanEnable(self, flow, desc, cAtFalse);
    }

static eAtRet IngressVlanEnable(ThaEthFlowController self, AtEthFlow flow, const tAtEthVlanDesc *desc)
    {
    return HelperIngressVlanEnable(self, flow, desc, cAtTrue);
    }

static eAtRet HelperEgressVlanEnable(ThaEthFlowController self, AtEthFlow flow, const tAtEthVlanDesc *desc, eBool enable)
    {
    eAtRet ret;
    tAtEthVlanTag cVlan;
    AtUnused(self);

    if (desc == NULL)
        return cAtErrorNullPointer;

    if (desc->numberOfVlans == 0)
        return cAtErrorRsrcNoAvail;

    if (desc->numberOfVlans == 1)
        {
        cVlan = desc->vlans[0];

        ret = Tha60290081ModulePweEthFlowCVlanEnable(ModulePwe(flow), flow, enable);
        if (enable)
            {
            ret |= Tha60290081ModulePweEthFlowCVlanSet(ModulePwe(flow), flow, &cVlan);
            ret |= Tha60290081ModulePweEthFlowCVlanTpidSet(ModulePwe(flow), flow, AtEthFlowEgressCVlanTpIdGet(flow));
            }

        return ret;
        }

    return cAtErrorModeNotSupport;
    }

static eAtRet EgressVlanEnable(ThaEthFlowController self, AtEthFlow flow, const tAtEthVlanDesc *desc)
    {
    return HelperEgressVlanEnable(self, flow, desc, cAtTrue);
    }

static eAtRet EgressVlanDisable(ThaEthFlowController self, AtEthFlow flow, const tAtEthVlanDesc *desc)
    {
    return HelperEgressVlanEnable(self, flow, desc, cAtFalse);
    }

static eAtRet EgressCVlanTpidSet(ThaEthFlowController self, AtEthFlow flow, uint16 cVlanTpid)
    {
    AtUnused(self);
    return Tha60290081ModulePweEthFlowCVlanTpidSet(ModulePwe(flow), flow, cVlanTpid);
    }

static eAtRet RxTrafficEnable(ThaEthFlowController self, AtEthFlow flow, eBool enable)
    {
    AtUnused(self);
    AtUnused(flow);
    AtUnused(enable);
    return cAtOk;
    }

static uint8 PartId(ThaEthFlowController self, AtEthFlow flow)
    {
    AtUnused(flow);
    AtUnused(self);
    return 0;
    }

static void FlowControlThresholdInit(ThaEthFlowController self, AtEthFlow ethFlow, AtChannel channel)
    {
    AtUnused(channel);
    AtUnused(ethFlow);
    AtUnused(self);
    }

static ThaModulePmc ModulePmc(AtEthFlow self)
    {
    return (ThaModulePmc)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cThaModulePmc);
    }

static uint32 PmcCounterRead(ThaEthFlowController self, AtEthFlow flow, uint16 counterType, eBool r2c)
    {
    ThaModulePmc modulePmc = ModulePmc(flow);
    AtUnused(self);

    mCounterByTypeGet(cAtEthFlowCounterTxBytes,               ThaModulePmcEthFlowTxBytesGet);
    mCounterByTypeGet(cAtEthFlowCounterTxPackets,             ThaModulePmcEthFlowTxPacketsGet);
    mCounterByTypeGet(cAtEthFlowCounterTxFragmentPackets,     ThaModulePmcEthFlowTxFragmentPacketsGet);
    mCounterByTypeGet(cAtEthFlowCounterTxNullFragmentPackets, ThaModulePmcEthFlowTxNullFragmentPacketsGet);
    mCounterByTypeGet(cAtEthFlowCounterTxDiscardPackets,      ThaModulePmcEthFlowTxDiscardPacketsGet);
    mCounterByTypeGet(cAtEthFlowCounterTxDiscardBytes,        ThaModulePmcEthFlowTxDiscardBytesGet);
    mCounterByTypeGet(cAtEthFlowCounterTxBECNPackets,         ThaModulePmcEthFlowTxBECNPacketsGet);
    mCounterByTypeGet(cAtEthFlowCounterTxFECNPackets,         ThaModulePmcEthFlowTxFECNPacketsGet);
    mCounterByTypeGet(cAtEthFlowCounterTxDEPackets,           ThaModulePmcEthFlowTxDEPacketsGet);

    mCounterByTypeGet(cAtEthFlowCounterRxBytes,               ThaModulePmcEthFlowRxBytesGet);
    mCounterByTypeGet(cAtEthFlowCounterRxPackets,             ThaModulePmcEthFlowRxPacketsGet);
    mCounterByTypeGet(cAtEthFlowCounterRxFragmentPackets,     ThaModulePmcEthFlowRxFragmentPacketsGet);
    mCounterByTypeGet(cAtEthFlowCounterRxNullFragmentPackets, ThaModulePmcEthFlowRxNullFragmentPacketsGet);
    mCounterByTypeGet(cAtEthFlowCounterRxDiscardPackets,      ThaModulePmcEthFlowRxDiscardPacketsGet);
    mCounterByTypeGet(cAtEthFlowCounterRxDiscardBytes,        ThaModulePmcEthFlowRxDiscardBytesGet);
    mCounterByTypeGet(cAtEthFlowCounterRxBECNPackets,         ThaModulePmcEthFlowRxBECNPacketsGet);
    mCounterByTypeGet(cAtEthFlowCounterRxFECNPackets,         ThaModulePmcEthFlowRxFECNPacketsGet);
    mCounterByTypeGet(cAtEthFlowCounterRxDEPackets,           ThaModulePmcEthFlowRxDEPacketsGet);

    return 0;
    }

static uint32 CounterGet(ThaEthFlowController self, AtEthFlow flow, uint16 counterType)
    {
    return PmcCounterRead(self, flow, counterType, cAtFalse);
    }

static uint32 CounterClear(ThaEthFlowController self, AtEthFlow flow, uint16 counterType)
    {
    return PmcCounterRead(self, flow, counterType, cAtTrue);
    }

static eAtRet Debug(ThaEthFlowController self, AtEthFlow flow)
    {
    AtUnused(self);
    return Tha60290081ModuleMpegEthFlowDebug(ModuleMpeg(flow), flow);
    }

static uint8 StartEthHeaderInPsnBuffer(ThaEthFlowController self)
    {
    AtUnused(self);
    return 16;
    }

static void OverrideThaEthFlowController(ThaEthFlowController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaEthFlowControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthFlowControllerOverride, m_ThaEthFlowControllerMethods, sizeof(m_ThaEthFlowControllerOverride));
        mMethodOverride(m_ThaEthFlowControllerOverride, PsnBufferGet);
        mMethodOverride(m_ThaEthFlowControllerOverride, PsnBufferSet);
        mMethodOverride(m_ThaEthFlowControllerOverride, Activate);
        mMethodOverride(m_ThaEthFlowControllerOverride, DeActivate);
        mMethodOverride(m_ThaEthFlowControllerOverride, NumberEgVlanSet);
        mMethodOverride(m_ThaEthFlowControllerOverride, PsnLengthReset);
        mMethodOverride(m_ThaEthFlowControllerOverride, EthernetTypeSet);
        mMethodOverride(m_ThaEthFlowControllerOverride, IngressVlanDisable);
        mMethodOverride(m_ThaEthFlowControllerOverride, IngressVlanEnable);
        mMethodOverride(m_ThaEthFlowControllerOverride, RxTrafficEnable);
        mMethodOverride(m_ThaEthFlowControllerOverride, PartId);
        mMethodOverride(m_ThaEthFlowControllerOverride, FlowControlThresholdInit);
        mMethodOverride(m_ThaEthFlowControllerOverride, CounterGet);
        mMethodOverride(m_ThaEthFlowControllerOverride, CounterClear);
        mMethodOverride(m_ThaEthFlowControllerOverride, EgressVlanEnable);
        mMethodOverride(m_ThaEthFlowControllerOverride, EgressVlanDisable);
        mMethodOverride(m_ThaEthFlowControllerOverride, EgressCVlanTpidSet);
        mMethodOverride(m_ThaEthFlowControllerOverride, Debug);
        mMethodOverride(m_ThaEthFlowControllerOverride, StartEthHeaderInPsnBuffer);
        }

    mMethodsSet(self, &m_ThaEthFlowControllerOverride);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081EthFlowController);
    }

static void Override(ThaEthFlowController self)
    {
    OverrideThaEthFlowController(self);
    }

ThaEthFlowController Tha60290081EthFlowControllerObjectInit(ThaEthFlowController self, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaEthFlowControllerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60290081EthFlowHeaderProvider.c
 *
 * Created Date: Aug 14, 2019
 *
 * Description : 60290081 ETH flow header provider implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/eth/controller/ThaEthFlowHeaderProviderInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290081EthFlowHeaderProvider
    {
    tThaEthFlowHeaderProvider super;
    }tTha60290081EthFlowHeaderProvider;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaEthFlowHeaderProviderMethods m_ThaEthFlowHeaderProviderOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint16 EthHeaderNoVlanLengthGet(ThaEthFlowHeaderProvider self)
    {
    static const uint16 cPsnReservedBytes = 16;
    AtUnused(self);
    return (uint16)(cPsnReservedBytes + cNumByteOfDestMac + cNumByteOfSourceMac + cNumByteOfEthernetType);
    }

static void Override(ThaEthFlowHeaderProvider self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthFlowHeaderProviderOverride, mMethodsGet(self), sizeof(m_ThaEthFlowHeaderProviderOverride));

        mMethodOverride(m_ThaEthFlowHeaderProviderOverride, EthHeaderNoVlanLengthGet);
        }

    mMethodsSet(self, &m_ThaEthFlowHeaderProviderOverride);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081EthFlowHeaderProvider);
    }

static ThaEthFlowHeaderProvider ObjectInit(ThaEthFlowHeaderProvider self, AtModuleEth ethModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaEthFlowHeaderProviderObjectInit(self, ethModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaEthFlowHeaderProvider Tha60290081EthFlowHeaderProviderNew(AtModuleEth ethModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaEthFlowHeaderProvider newProvider = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newProvider, ethModule);
    }

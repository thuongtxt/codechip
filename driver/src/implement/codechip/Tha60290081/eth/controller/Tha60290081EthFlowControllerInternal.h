/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60290081EthFlowControllerInternal.h
 * 
 * Created Date: Jul 16, 2019
 *
 * Description : 60290081 Ethernet flow controller representation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290081ETHFLOWCONTROLLERINTERNAL_H_
#define _THA60290081ETHFLOWCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/eth/controller/ThaEthFlowControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290081EthFlowController
    {
    tThaEthFlowController super;
    }tTha60290081EthFlowController;

typedef struct tTha60290081EosEthFlowController
    {
    tTha60290081EthFlowController super;
    }tTha60290081EosEthFlowController;

typedef struct tTha60290081PwEthFlowController
    {
    tTha60290081EthFlowController super;
    }tTha60290081PwEthFlowController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaEthFlowController Tha60290081EthFlowControllerObjectInit(ThaEthFlowController self, AtModuleEth module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290081ETHFLOWCONTROLLERINTERNAL_H_ */


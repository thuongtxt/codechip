/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60290081EosEthFlowController.c
 *
 * Created Date: Jul 25, 2019
 *
 * Description : 60290081 EoS Ethernet Flow controller implementation.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../pwe/Tha60290081ModulePwe.h"
#include "../../pda/Tha60290081ModulePda.h"
#include "../../cla/Tha60290081ModuleCla.h"
#include "Tha60290081EthFlowControllerInternal.h"
#include "Tha60290081EthFlowController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaEthFlowControllerMethods m_ThaEthFlowControllerOverride;

/* Supper */
static const tThaEthFlowControllerMethods *m_ThaEthFlowControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static Tha60290081ModulePwe ModulePwe(AtEthFlow flow)
    {
    return (Tha60290081ModulePwe)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)flow), cThaModulePwe);
    }

static Tha60290081ModulePda ModulePda(AtEthFlow flow)
    {
    return (Tha60290081ModulePda)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)flow), cThaModulePda);
    }

static ThaModuleCla ModuleCla(AtEthFlow flow)
    {
    return (ThaModuleCla)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)flow), cThaModuleCla);
    }

static eBool MacIsProgrammable(ThaEthFlowController self, AtEthFlow ethFlow)
    {
    AtUnused(self);
    AtUnused(ethFlow);
    return cAtFalse;
    }

static eBool HasEthernetType(ThaEthFlowController self, AtEthFlow flow)
    {
    AtUnused(flow);
    AtUnused(self);
    return cAtFalse;
    }

static uint16 EthHeaderNoVlanLengthGet(ThaEthFlowController self, AtEthFlow flow)
    {
    AtUnused(self);
    AtUnused(flow);
    return 0;
    }

static eAtRet TxTrafficEnable(ThaEthFlowController self, AtEthFlow flow, eBool enable)
    {
    AtUnused(self);
    AtUnused(flow);
    AtUnused(enable);
    return cAtOk;
    }

static eBool TxTrafficIsEnabled(ThaEthFlowController self, AtEthFlow flow)
    {
    AtUnused(self);
    AtUnused(flow);
    return cAtTrue;
    }

static uint8 FlowType(AtEthFlow flow, AtChannel channel)
    {
    AtConcateGroup group = (AtConcateGroup)AtEncapChannelBoundPhyGet((AtEncapChannel)channel);
    uint8 memberType = AtConcateGroupMemberTypeGet(group);
    AtUnused(flow);

    switch (memberType)
        {
        /* SDH VCs */
        case cAtConcateMemberTypeVc4_64c: return cAtEthFlowTypeEos;
        case cAtConcateMemberTypeVc4_16c: return cAtEthFlowTypeEos;
        case cAtConcateMemberTypeVc4_4c:  return cAtEthFlowTypeEos;
        case cAtConcateMemberTypeVc4_nc:  return cAtEthFlowTypeEos;
        case cAtConcateMemberTypeVc4:     return cAtEthFlowTypeEos;
        case cAtConcateMemberTypeVc3:     return cAtEthFlowTypeEos;
        case cAtConcateMemberTypeVc12:    return cAtEthFlowTypeEos;
        case cAtConcateMemberTypeVc11:    return cAtEthFlowTypeEos;

        /* PDH */
        case cAtConcateMemberTypeDs1:     return cAtEthFlowTypeEop;
        case cAtConcateMemberTypeE1:      return cAtEthFlowTypeEop;
        case cAtConcateMemberTypeDs3:     return cAtEthFlowTypeEop;
        case cAtConcateMemberTypeE3:      return cAtEthFlowTypeEop;

        default:    return cAtEthFlowTypeAny;
        }
    }

static eAtRet Activate(ThaEthFlowController self, AtEthFlow flow, AtChannel channel, uint8 classNumber)
    {
    eAtRet ret = cAtOk;

    AtEthFlowTypeSet(flow, FlowType(flow, channel));

    ret = m_ThaEthFlowControllerMethods->Activate(self, flow, channel, classNumber);
    if (ret != cAtOk)
        return ret;

    AtEthFlowTypeSet(flow, FlowType(flow, channel));

    ret |= Tha60290081ModulePwePlaEosEthFlowLookup(ModulePwe(flow), flow, channel, cAtTrue);

    ret |= Tha60290081ModulePdaEosEthFlowModeSet(ModulePda(flow), flow, channel);
    ret |= Tha60290081ModulePdaEosEthFlowLookupSet(ModulePda(flow), flow, channel);
    ret |= Tha60290081ModulePdaEosEthFlowBandwidthUpdate(ModulePda(flow), flow, channel);

    ret |= Tha60290081ModuleClaEthFlowRemoveVlan(ModuleCla(flow), flow);

    return ret;
    }

static eAtRet DeActivate(ThaEthFlowController self, AtEthFlow flow)
    {
    AtChannel channel;
    eAtRet ret = cAtOk;

    channel = (AtChannel)AtEthFlowBoundEncapChannelGet(flow);
    ret |= Tha60290081ModulePwePlaEosEthFlowLookup(ModulePwe(flow), flow, channel, cAtFalse);
    ret |= Tha60290081ModulePdaEosEthFlowLookupReset(ModulePda(flow), flow, channel);
    if (ret != cAtOk)
        return ret;

    return m_ThaEthFlowControllerMethods->DeActivate(self, flow);
    }

static eBool ShouldControlEgressVlanByPsnBuffer(ThaEthFlowController self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void OverrideThaEthFlowController(ThaEthFlowController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaEthFlowControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthFlowControllerOverride, m_ThaEthFlowControllerMethods, sizeof(m_ThaEthFlowControllerOverride));
        mMethodOverride(m_ThaEthFlowControllerOverride, MacIsProgrammable);
        mMethodOverride(m_ThaEthFlowControllerOverride, HasEthernetType);
        mMethodOverride(m_ThaEthFlowControllerOverride, EthHeaderNoVlanLengthGet);
        mMethodOverride(m_ThaEthFlowControllerOverride, TxTrafficEnable);
        mMethodOverride(m_ThaEthFlowControllerOverride, TxTrafficIsEnabled);
        mMethodOverride(m_ThaEthFlowControllerOverride, Activate);
        mMethodOverride(m_ThaEthFlowControllerOverride, DeActivate);
        mMethodOverride(m_ThaEthFlowControllerOverride, ShouldControlEgressVlanByPsnBuffer);
        }

    mMethodsSet(self, &m_ThaEthFlowControllerOverride);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081EosEthFlowController);
    }

static void Override(ThaEthFlowController self)
    {
    OverrideThaEthFlowController(self);
    }

static ThaEthFlowController ObjectInit(ThaEthFlowController self, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290081EthFlowControllerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaEthFlowController Tha60290081EosEthFlowControllerNew(AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaEthFlowController newFlow = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newFlow == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newFlow, module);
    }


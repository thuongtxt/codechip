/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Ethernet
 *
 * File        : Tha60290081EthFlow.c
 *
 * Created Date: Jul 15, 2019
 *
 * Description : 60290081 Ethernet flow implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/eth/ThaEthFlowInternal.h"
#include "Tha60290081EthFlow.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290081EthFlow
    {
    tThaEthFlow super;
    }tTha60290081EthFlow;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtEthFlowMethods        m_AtEthFlowOverride;
static tAtChannelMethods        m_AtChannelOverride;

/* Save super implementation */
static const tAtEthFlowMethods *m_AtEthFlowMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    ThaEthFlowController controller = ThaEthFlowControllerGet((ThaEthFlow)self);
    if (controller)
        return mMethodsGet(controller)->CounterGet(controller, (AtEthFlow)self, counterType);

    return 0;
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    ThaEthFlowController controller = ThaEthFlowControllerGet((ThaEthFlow)self);
    if (controller)
        return mMethodsGet(controller)->CounterClear(controller, (AtEthFlow)self, counterType);

    return 0;
    }

static eBool CounterIsSupported(AtChannel self, uint16 counterType)
    {
    AtUnused(self);
    AtUnused(counterType);
    return cAtFalse;
    }

static uint32 HwIdGet(AtChannel self)
    {
    return AtChannelIdGet(self);
    }

static eBool IsBindingTdmPw(AtEthFlow self)
    {
    AtPw pw = AtEthFlowPwGet(self);
    if (pw == NULL)
        return cAtFalse;

    return AtPwIsTdmPw(pw);
    }

static eAtRet IngressVlanDisable(AtEthFlow self, const tAtEthVlanDesc *desc)
    {
    ThaEthFlowController controller = ThaEthFlowControllerGet((ThaEthFlow)self);
    if (controller)
        return mMethodsGet(controller)->IngressVlanDisable(controller, self, desc);
    return cAtOk;
    }

static eAtRet IngressVlanEnable(AtEthFlow self, const tAtEthVlanDesc *desc)
    {
    ThaEthFlowController controller = ThaEthFlowControllerGet((ThaEthFlow)self);
    if (controller)
        return mMethodsGet(controller)->IngressVlanEnable(controller, self, desc);
    return cAtOk;
    }

static eBool EgressVlanCanUse(AtEthFlow self, const tAtEthVlanDesc *desc)
    {
    AtUnused(desc);

    /* Support single egress VLAN descriptor */
    if (AtEthFlowEgressNumVlansGet(self) > 0)
        return cAtFalse;

    return cAtTrue;
    }

static eBool IngressVlanCanUse(AtEthFlow self, const tAtEthVlanDesc *desc)
    {
    if (IsBindingTdmPw(self))
        return cAtFalse;

    if (!m_AtEthFlowMethods->IngressVlanCanUse(self, desc))
        return cAtFalse;

    /* Support single ingress VLAN descriptor */
    if (AtEthFlowIngressNumVlansGet(self) > 0)
        return cAtFalse;

    return cAtTrue;
    }

static eAtModuleEthRet EgressVlanAdd(AtEthFlow self, const tAtEthVlanDesc *desc)
    {
    eAtRet ret = m_AtEthFlowMethods->EgressVlanAdd(self, desc);
    if (ret != cAtOk)
        return ret;

    /* Ingress VLANs must match egress VLANs if PW is bound */
    if (IsBindingTdmPw(self))
        return IngressVlanEnable(self, desc);

    return cAtOk;
    }

static eAtModuleEthRet EgressVlanRemove(AtEthFlow self, const tAtEthVlanDesc *desc)
    {
    eAtRet ret;

    if (!IsBindingTdmPw(self))
        return m_AtEthFlowMethods->EgressVlanRemove(self, desc);

    /* Ingress VLANs must match egress VLANs if PW is bound */
    ret = IngressVlanDisable(self, desc);
    if (ret != cAtOk)
        return ret;

    return m_AtEthFlowMethods->EgressVlanRemove(self, desc);
    }

static eAtModuleEthRet IngressVlanAdd(AtEthFlow self, const tAtEthVlanDesc *desc)
    {
    if (IsBindingTdmPw(self))
        return cAtErrorModeNotSupport;

    return m_AtEthFlowMethods->IngressVlanAdd(self, desc);
    }

static eAtModuleEthRet IngressVlanRemove(AtEthFlow self, const tAtEthVlanDesc *desc)
    {
    if (IsBindingTdmPw(self))
        return cAtErrorModeNotSupport;

    return m_AtEthFlowMethods->IngressVlanRemove(self, desc);
    }

static uint32 IngressNumVlansGet(AtEthFlow self)
    {
    if (IsBindingTdmPw(self))
        return mMethodsGet(self)->EgressNumVlansGet(self);

    return m_AtEthFlowMethods->IngressNumVlansGet(self);
    }

static tAtEthVlanDesc *IngressVlanAtIndex(AtEthFlow self, uint32 vlanIndex, tAtEthVlanDesc *descs)
    {
    if (IsBindingTdmPw(self))
        return mMethodsGet(self)->EgressVlanAtIndex(self, vlanIndex, descs);

    return m_AtEthFlowMethods->IngressVlanAtIndex(self, vlanIndex, descs);
    }

static void OverrideAtEthFlow(AtEthFlow self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEthFlowMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthFlowOverride, mMethodsGet(self), sizeof(m_AtEthFlowOverride));

        mMethodOverride(m_AtEthFlowOverride, EgressVlanCanUse);
        mMethodOverride(m_AtEthFlowOverride, EgressVlanAdd);
        mMethodOverride(m_AtEthFlowOverride, EgressVlanRemove);
        mMethodOverride(m_AtEthFlowOverride, IngressVlanCanUse);
        mMethodOverride(m_AtEthFlowOverride, IngressVlanAdd);
        mMethodOverride(m_AtEthFlowOverride, IngressVlanRemove);
        mMethodOverride(m_AtEthFlowOverride, IngressNumVlansGet);
        mMethodOverride(m_AtEthFlowOverride, IngressVlanAtIndex);
        }

    mMethodsSet(self, &m_AtEthFlowOverride);
    }

static void OverrideAtChannel(AtEthFlow self)
    {
    AtChannel channel = (AtChannel)self;
    
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterIsSupported);
        mMethodOverride(m_AtChannelOverride, HwIdGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtEthFlow self)
    {
    OverrideAtChannel(self);
    OverrideAtEthFlow(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081EthFlow);
    }

static AtEthFlow ObjectInit(AtEthFlow self, uint32 flowId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaEthFlowObjectInit(self, flowId, module) == NULL)
        return NULL;

    /* Setup class */
     Override(self);
     m_methodsInit = 1;

    return self;
    }

AtEthFlow Tha60290081EthFlowNew(uint32 flowId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthFlow newFlow = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newFlow == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newFlow, flowId, module);
    }

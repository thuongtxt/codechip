/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60290081ModuleEth.h
 * 
 * Created Date: Jul 15, 2019
 *
 * Description : 60290081 Module ETH
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290081MODULEETH_H_
#define _THA60290081MODULEETH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtChannel.h"
#include "AtModuleEth.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

/* Ports */
AtEthPort Tha60290081FaceplateSerdesEthPortNew(uint8 portId, AtModuleEth module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290081MODULEETH_H_ */


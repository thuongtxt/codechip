/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60290081ModuleEth.c
 *
 * Created Date: Jul 15, 2019
 *
 * Description : 60290081 Module ETH implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/eth/ThaEthFlow.h"
#include "Tha60290081ModuleEthInternal.h"
#include "Tha60290081ModuleEth.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods             m_AtModuleOverride;
static tAtModuleEthMethods          m_AtModuleEthOverride;
static tThaModuleEthMethods         m_ThaModuleEthOverride;
static tTha60290021ModuleEthMethods m_Tha60290021ModuleEthOverride;

/* Save super implementation */
static const tAtModuleMethods      *m_AtModuleMethods     = NULL;
static const tAtModuleEthMethods   *m_AtModuleEthMethods  = NULL;
static const tThaModuleEthMethods  *m_ThaModuleEthMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint16 MaxFlowsGet(AtModuleEth self)
    {
    AtUnused(self);
    return 5376;
    }

static AtEthFlow FlowObjectCreate(AtModuleEth self, uint16 flowId)
    {
    return Tha60290081EthFlowNew(flowId, self);
    }

static ThaEthFlowControllerFactory FlowControllerFactory(ThaModuleEth self)
    {
    AtUnused(self);
    return Tha60290081EthFlowControllerFactory();
    }

static AtEthPort FaceplatePortCreate(Tha60290021ModuleEth self, uint8 portId)
    {
    return Tha60290081FaceplateSerdesEthPortNew(portId, (AtModuleEth)self);
    }

static uint8 NumberOf10GFaceplatePorts(Tha60290021ModuleEth self)
    {
    AtUnused(self);
    return 1;
    }

static ThaEthFlowHeaderProvider FlowHeaderProviderCreate(ThaModuleEth self)
    {
    return Tha60290081EthFlowHeaderProviderNew((AtModuleEth)self);
    }

static void OverrideAtModule(AtModuleEth self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtModuleEth(AtModuleEth self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleEthMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleEthOverride, mMethodsGet(self), sizeof(m_AtModuleEthOverride));

        mMethodOverride(m_AtModuleEthOverride, MaxFlowsGet);
        mMethodOverride(m_AtModuleEthOverride, FlowObjectCreate);
        }

    mMethodsSet(self, &m_AtModuleEthOverride);
    }

static void OverrideThaModuleEth(AtModuleEth self)
    {
    ThaModuleEth module = (ThaModuleEth)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleEthMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleEthOverride, mMethodsGet(module), sizeof(m_ThaModuleEthOverride));

        mMethodOverride(m_ThaModuleEthOverride, FlowControllerFactory);
        mMethodOverride(m_ThaModuleEthOverride, FlowHeaderProviderCreate);
        }

    mMethodsSet(module, &m_ThaModuleEthOverride);
    }

static void OverrideTha60290021ModuleEth(AtModuleEth self)
    {
    Tha60290021ModuleEth module = (Tha60290021ModuleEth)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021ModuleEthOverride, mMethodsGet(module), sizeof(m_Tha60290021ModuleEthOverride));

        mMethodOverride(m_Tha60290021ModuleEthOverride, FaceplatePortCreate);
        mMethodOverride(m_Tha60290021ModuleEthOverride, NumberOf10GFaceplatePorts);
        }

    mMethodsSet(module, &m_Tha60290021ModuleEthOverride);
    }

static void Override(AtModuleEth self)
    {
    OverrideAtModule(self);
    OverrideAtModuleEth(self);
    OverrideThaModuleEth(self);
    OverrideTha60290021ModuleEth(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081ModuleEth);
    }

AtModuleEth Tha60290081ModuleEthObjectInit(AtModuleEth self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022ModuleEthObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleEth Tha60290081ModuleEthNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEth newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290081ModuleEthObjectInit(newModule, device);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Ethernet
 * 
 * File        : Tha60290081EthFlow.h
 * 
 * Created Date: Jul 16, 2019
 *
 * Description : 60290081 Ethernet flow
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290081ETHFLOW_H_
#define _THA60290081ETHFLOW_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/eth/AtEthFlowInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290081EthFlow *Tha60290081EthFlow;

typedef enum eTha6029EthFlowType
    {
    cTha6029EthFlowTypeAny      = cAtEthFlowTypeAny,
    cTha6029EthFlowTypePw       = cAtEthFlowTypePw,
    cTha6029EthFlowTypePos      = cAtEthFlowTypePos,
    cTha6029EthFlowTypeEos      = cAtEthFlowTypeEos,
    cTha6029EthFlowTypeEop      = cAtEthFlowTypeEop,

    cTha6029EthFlowTypeDcc,
    cTha6029EthFlowTypeEthPass,
    cTha6029EthFlowTypePtp,
    }eTha6029EthFlowType;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THA60290081ETHFLOW_H_ */


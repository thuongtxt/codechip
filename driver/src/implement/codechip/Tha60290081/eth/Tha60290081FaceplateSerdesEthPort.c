/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60290081FaceplateSerdesEthPort.c
 *
 * Created Date: Sep 3, 2019
 *
 * Description : 60290081 Faceplate SERDES Ethernet port implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290022/eth/Tha60290022FaceplateSerdesEthPortInternal.h"
#include "Tha60290081ModuleEth.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290081FaceplateSerdesEthPort
    {
    tTha60290022FaceplateSerdesEthPortV3 super;
    }tTha60290081FaceplateSerdesEthPort;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60290021FaceplateSerdesEthPortMethods    m_Tha60290021FaceplateSerdesEthPortOverride;

/* Cache super implementation */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool HasClockMonitor(Tha60290021FaceplateSerdesEthPort self)
    {
    uint32 portId = AtChannelHwIdGet((AtChannel)self);

    if (portId == 0)
        return cAtTrue;

    return cAtFalse;
    }

static void OverrideTha60290021FaceplateSerdesEthPort(AtEthPort self)
    {
    Tha60290021FaceplateSerdesEthPort port = (Tha60290021FaceplateSerdesEthPort)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021FaceplateSerdesEthPortOverride,
                                  mMethodsGet(port), sizeof(m_Tha60290021FaceplateSerdesEthPortOverride));

        mMethodOverride(m_Tha60290021FaceplateSerdesEthPortOverride, HasClockMonitor);
        }

    mMethodsSet(port, &m_Tha60290021FaceplateSerdesEthPortOverride);
    }

static void Override(AtEthPort self)
    {
    OverrideTha60290021FaceplateSerdesEthPort(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081FaceplateSerdesEthPort);
    }

static AtEthPort ObjectInit(AtEthPort self, uint8 portId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022FaceplateSerdesEthPortV3ObjectInit(self, portId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEthPort Tha60290081FaceplateSerdesEthPortNew(uint8 portId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthPort newPort = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPort == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPort, portId, module);
    }

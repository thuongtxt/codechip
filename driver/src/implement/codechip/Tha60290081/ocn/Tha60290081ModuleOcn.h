/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : OCN
 * 
 * File        : Tha60290081ModuleOcn.h
 * 
 * Created Date: Jun 15, 2019
 *
 * Description : 60290081 Module OCN
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290081MODULEOCN_H_
#define _THA60290081MODULEOCN_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSdhChannel.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290081ModuleOcn *Tha60290081ModuleOcn;

typedef enum eTha60290081OcnTerminatedAuVcBusType
    {
    cTha60290081OcnTerminatedAuVcBusTypePw,
    cTha60290081OcnTerminatedAuVcBusTypeEos,
    cTha60290081OcnTerminatedAuVcBusTypePos,
    cTha60290081OcnTerminatedAuVcBusTypeDisable
    }eTha60290081OcnTerminatedAuVcBusType;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha60290081ModuleOcnTerminatedAuVcBusTypeSet(Tha60290081ModuleOcn self, AtSdhChannel auVc, uint8 busType);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290081MODULEOCN_H_ */


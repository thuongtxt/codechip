/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : OCN
 *
 * File        : Tha60290081ModuleOcn.c
 *
 * Created Date: Jun 15, 2019
 *
 * Description : 60290081 Module OCN implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60290021/sdh/Tha60290021ModuleSdh.h"
#include "../../Tha60290022/ram/Tha60290022InternalRam.h"
#include "Tha60290081ModuleOcnInternal.h"
#include "Tha60290081ModuleOcn.h"
#include "Tha60290081ModuleOcnReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods                 m_AtModuleOverride;
static tThaModuleOcnMethods             m_ThaModuleOcnOverride;
static tTha60210011ModuleOcnMethods     m_Tha60210011ModuleOcnOverride;
static tTha60290021ModuleOcnMethods     m_Tha60290021ModuleOcnOverride;
static tTha60290022ModuleOcnMethods     m_Tha60290022ModuleOcnOverride;

/* Save super implementation */
static const tThaModuleOcnMethods           *m_ThaModuleOcnMethods         = NULL;
static const tTha60210011ModuleOcnMethods   *m_Tha60210011ModuleOcnMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtInternalRam InternalRamCreate(AtModule self, uint32 ramId, uint32 localRamId)
    {
    return Tha60290022InternalRamOcnNew(self, ramId, localRamId);
    }

static const char **AllInternalRamsDescription(AtModule self, uint32 *numRams)
    {
    static const char * description[] =
        {
         "TFI-5 OCN STS Pointer Interpreter Per Channel Control 0",
         "TFI-5 OCN STS Pointer Interpreter Per Channel Control 1",
         "TFI-5 OCN STS Pointer Interpreter Per Channel Control 2",
         "TFI-5 OCN STS Pointer Interpreter Per Channel Control 3",
         "TFI-5 OCN STS Pointer Generator Per Channel Control 0",
         "TFI-5 OCN STS Pointer Generator Per Channel Control 1",
         "TFI-5 OCN STS Pointer Generator Per Channel Control 2",
         "TFI-5 OCN STS Pointer Generator Per Channel Control 3",

         "Line Side OCN Tx J0 Insertion Buffer 0",
         "Line Side OCN Tx J0 Insertion Buffer 1",
         "Line Side OCN Tx J0 Insertion Buffer 2",
         "Line Side OCN Tx J0 Insertion Buffer 3",
         "Line Side OCN STS Pointer Interpreter Per Channel Control 0",
         "Line Side OCN STS Pointer Interpreter Per Channel Control 1",
         "Line Side OCN STS Pointer Interpreter Per Channel Control 2",
         "Line Side OCN STS Pointer Interpreter Per Channel Control 3",
         "Line Side OCN STS Pointer Generator Per Channel Control 0",
         "Line Side OCN STS Pointer Generator Per Channel Control 1",
         "Line Side OCN STS Pointer Generator Per Channel Control 2",
         "Line Side OCN STS Pointer Generator Per Channel Control 3",
         "Line Side OCN TOH Monitoring Per Line Control",

         "OCN RXPP Per STS payload Control 0",
         "OCN RXPP Per STS payload Control 1",
         "OCN RXPP Per STS payload Control 2",
         "OCN RXPP Per STS payload Control 3",
         "OCN VTTU Pointer Interpreter Per Channel Control 0",
         "OCN VTTU Pointer Interpreter Per Channel Control 1",
         "OCN VTTU Pointer Interpreter Per Channel Control 2",
         "OCN VTTU Pointer Interpreter Per Channel Control 3",
         "OCN TXPP Per STS Multiplexing Control 0",
         "OCN TXPP Per STS Multiplexing Control 1",
         "OCN TXPP Per STS Multiplexing Control 2",
         "OCN TXPP Per STS Multiplexing Control 3",
         "OCN VTTU Pointer Generator Per Channel Control 0",
         "OCN VTTU Pointer Generator Per Channel Control 1",
         "OCN VTTU Pointer Generator Per Channel Control 2",
         "OCN VTTU Pointer Generator Per Channel Control 3",
         "OCN Rx High Order Map concatenate configuration 0",
         "OCN Rx High Order Map concatenate configuration 1",
         "OCN Rx High Order Map concatenate configuration 2",
         "OCN Rx High Order Map concatenate configuration 3",

         "OCN SXC Control 1 - Config Page 0 and Page 1 of SXC 0",
         "OCN SXC Control 1 - Config Page 0 and Page 1 of SXC 1",
         "OCN SXC Control 1 - Config Page 0 and Page 1 of SXC 2",
         "OCN SXC Control 1 - Config Page 0 and Page 1 of SXC 3",
         "OCN SXC Control 1 - Config Page 0 and Page 1 of SXC 4",
         "OCN SXC Control 1 - Config Page 0 and Page 1 of SXC 5",
         "OCN SXC Control 1 - Config Page 0 and Page 1 of SXC 6",
         "OCN SXC Control 1 - Config Page 0 and Page 1 of SXC 7",
         "OCN SXC Control 1 - Config Page 0 and Page 1 of SXC 8",
         "OCN SXC Control 1 - Config Page 0 and Page 1 of SXC 9",
         "OCN SXC Control 1 - Config Page 0 and Page 1 of SXC 10",
         "OCN SXC Control 1 - Config Page 0 and Page 1 of SXC 11",

         "OCN SXC Control 2 - Config Page 2 of SXC 0",
         "OCN SXC Control 2 - Config Page 2 of SXC 1",
         "OCN SXC Control 2 - Config Page 2 of SXC 2",
         "OCN SXC Control 2 - Config Page 2 of SXC 3",
         "OCN SXC Control 2 - Config Page 2 of SXC 4",
         "OCN SXC Control 2 - Config Page 2 of SXC 5",
         "OCN SXC Control 2 - Config Page 2 of SXC 6",
         "OCN SXC Control 2 - Config Page 2 of SXC 7",
         "OCN SXC Control 2 - Config Page 2 of SXC 8",
         "OCN SXC Control 2 - Config Page 2 of SXC 9",
         "OCN SXC Control 2 - Config Page 2 of SXC 10",
         "OCN SXC Control 2 - Config Page 2 of SXC 11",

         "OCN SXC Control 3 - Config APS 0"
         "OCN SXC Control 3 - Config APS 1",
         "OCN SXC Control 3 - Config APS 2",
         "OCN SXC Control 3 - Config APS 3",
         "OCN SXC Control 3 - Config APS 4",
         "OCN SXC Control 3 - Config APS 5",
         "OCN SXC Control 3 - Config APS 6",
         "OCN SXC Control 3 - Config APS 7",
         "OCN SXC Control 3 - Config APS 8",
         "OCN SXC Control 3 - Config APS 9",
         "OCN SXC Control 3 - Config APS 10",
         "OCN SXC Control 3 - Config APS 11",
        };
    AtUnused(self);

    if (numRams)
        *numRams = mCount(description);

    return description;
    }

static eAtRet DefaultSet(ThaModuleOcn self)
    {
    eAtRet ret;
    ret = m_ThaModuleOcnMethods->DefaultSet(self);
    return ret;
    }

static eBool SohOverEthIsSupported(ThaModuleOcn self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet TohThresholdDefaultSet(ThaModuleOcn self)
    {
    static const uint8 cNotMaskingTohStableMonitoring = 1;
    uint32 baseAddress = ThaModuleOcnBaseAddress(self);
    uint32 regAddr = ThaModuleOcnTohGlobalK1StableMonitoringThresholdControlRegAddr(self) + baseAddress;
    uint32 regVal = 0;
    mRegFieldSet(regVal, cAf6_tohglbk1stbthr_reg_TohK1StbThr2_, 3);
    mRegFieldSet(regVal, cAf6_tohglbk1stbthr_reg_TohK1StbThr1_, 4);
    mModuleHwWrite(self, regAddr, regVal);

    regAddr = ThaModuleOcnTohGlobalK2StableMonitoringThresholdControlRegAddr(self) + baseAddress;
    regVal = 0;
    mRegFieldSet(regVal, cAf6_tohglbk2stbthr_reg_TohK2StbThr2_, 3);
    mRegFieldSet(regVal, cAf6_tohglbk2stbthr_reg_TohK2StbThr1_, 4);
    mModuleHwWrite(self, regAddr, regVal);

    regAddr = ThaModuleOcnTohGlobalS1StableMonitoringThresholdControlRegAddr(self) + baseAddress;
    regVal = 0;
    mRegFieldSet(regVal, cAf6_tohglbs1stbthr_reg_TohS1StbThr2_, 5);
    mRegFieldSet(regVal, cAf6_tohglbs1stbthr_reg_TohS1StbThr1_, 5);
    mModuleHwWrite(self, regAddr, regVal);

    regAddr = cAf6Reg_tohglbrdidetthr_reg + baseAddress;
    regVal = 0;
    mRegFieldSet(regVal, cAf6_tohglbrdidetthr_reg_TohRdiDetThr2_, 0x4);
    mRegFieldSet(regVal, cAf6_tohglbrdidetthr_reg_TohRdiDetThr1_, 0x9);
    mModuleHwWrite(self, regAddr, regVal);

    regAddr = cAf6Reg_tohglbaisdetthr_reg + baseAddress;
    regVal = 0;
    mRegFieldSet(regVal, cAf6_tohglbaisdetthr_reg_TohAisDetThr2_, 3);
    mRegFieldSet(regVal, cAf6_tohglbaisdetthr_reg_TohAisDetThr1_, 4);
    mModuleHwWrite(self, regAddr, regVal);

    regAddr = cAf6Reg_tohglbk1smpthr_reg + baseAddress;
    regVal = 0;
    mRegFieldSet(regVal, cAf6_tohglbk1smpthr_reg_TohK1SmpThr2_, 7);
    mRegFieldSet(regVal, cAf6_tohglbk1smpthr_reg_TohK1SmpThr1_, 7);
    mModuleHwWrite(self, regAddr, regVal);

    regAddr = cAf6Reg_tohglberrcntmod_reg + baseAddress;
    regVal = 0;
    mRegFieldSet(regVal, cAf6_tohglberrcntmod_reg_TohReiErrCntMod_, 1);
    mRegFieldSet(regVal, cAf6_tohglberrcntmod_reg_TohB2ErrCntMod_, 1);
    mRegFieldSet(regVal, cAf6_tohglberrcntmod_reg_TohB1ErrCntMod_, 1);
    mModuleHwWrite(self, regAddr, regVal);

    regAddr = cAf6Reg_tohglbaffen_reg + baseAddress;
    regVal = 0;
    mRegFieldSet(regVal, cAf6_tohglbaffen_reg_TohAisAffStbMon_, cNotMaskingTohStableMonitoring);
    mRegFieldSet(regVal, cAf6_tohglbaffen_reg_TohAisAffRdilMon_, 0);
    mRegFieldSet(regVal, cAf6_tohglbaffen_reg_TohAisAffAislMon_, 0);
    mRegFieldSet(regVal, cAf6_tohglbaffen_reg_TohAisAffErrCnt_, 0);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet ChannelStsIdSw2HwGet(ThaModuleOcn self, AtSdhChannel channel, eAtModule phyModule, uint8 swSts, uint8* sliceId, uint8 *hwStsInSlice)
    {
    /* FIXME: slice ID for module APS may need to be updated. */
    return m_ThaModuleOcnMethods->ChannelStsIdSw2HwGet(self, channel, phyModule, swSts, sliceId, hwStsInSlice);
    }

static uint32 HwStsDefaultOffset(Tha60210011ModuleOcn self, AtSdhChannel channel, uint8 hwSlice, uint8 hwSts)
    {
    AtUnused(self);
    AtUnused(channel);
    return (512UL * hwSlice) + hwSts;
    }

static uint32 NumFaceplateSlices(Tha60290021ModuleOcn self)
    {
    AtUnused(self);
    return 4;
    }

static uint32 FaceplateXcStartLineId(Tha60290021ModuleOcn self)
    {
    AtUnused(self);
    return 4;
    }

static uint32 TerminatedXcStartLineId(Tha60290021ModuleOcn self)
    {
    AtUnused(self);
    return 8;
    }

static uint32 NumXcLines(Tha60290021ModuleOcn self)
    {
    AtUnused(self);
    return 12;
    }

static eAtRet ClockExtractorSquelchingAisLEnable(Tha60290022ModuleOcn self, uint8 lineId, eBool enable)
    {
    uint32 address = cAf6Reg_glb8kaislsquel_reg + ThaModuleOcnBaseAddress((ThaModuleOcn)self);
    uint32 fieldShift = lineId;
    uint32 fieldMask  = cBit0 << lineId;
    uint32 regVal = mModuleHwRead(self, address);
    mRegFieldSet(regVal, field, (enable) ? 0 : 1);
    mModuleHwWrite(self, address, regVal);

    return cAtOk;
    }

static eBool ClockExtractorSquelchingAisLIsEnabled(Tha60290022ModuleOcn self, uint8 lineId)
    {
    uint32 address = cAf6Reg_glb8kaislsquel_reg + ThaModuleOcnBaseAddress((ThaModuleOcn)self);
    uint32 fieldShift = lineId;
    uint32 fieldMask  = cBit0 << lineId;
    uint32 regVal = mModuleHwRead(self, address);
    return mRegField(regVal, field) ? cAtFalse : cAtTrue;
    }

static uint32 FaceplateGroupOffset(Tha60290021ModuleOcn self, uint32 groupId)
    {
    AtUnused(self);
    return groupId * 256U;
    }

static uint32 FaceplateLineDefaultOffset(Tha60290021ModuleOcn self, uint32 faceplateLineId)
    {
    return ThaModuleOcnBaseAddress((ThaModuleOcn)self) + faceplateLineId;
    }

static eAtRet FaceplateStsAssign(Tha60290021ModuleOcn self, uint32 faceplateLineId, uint32 sts48Id, uint32 sts12Id, uint32 sts3Id)
    {
    return Tha60290021ModuleOcnFaceplateStsAssign((ThaModuleOcn)self, faceplateLineId, sts48Id, sts12Id, sts3Id);
    }

static eAtRet FaceplateStsMappingDefaultSet(Tha60290021ModuleOcn self)
    {
    eAtRet ret = cAtOk;
    uint32 faceplateId = 0;
    uint32 oc48_i;

    /* See MRO10MS_sts_assignment.xlsx, sheet "Rate setting" */
    for (oc48_i = 0; oc48_i < 4; oc48_i++)
        {
        ret |= FaceplateStsAssign(self, faceplateId++, oc48_i, 0, 0);
        ret |= FaceplateStsAssign(self, faceplateId++, oc48_i, 1, 0);
        ret |= FaceplateStsAssign(self, faceplateId++, oc48_i, 2, 0);
        ret |= FaceplateStsAssign(self, faceplateId++, oc48_i, 3, 0);
        }

    return ret;
    }

static uint32 TohMonitoringChannelControlRegAddr(Tha60290021ModuleOcn self)
    {
    AtUnused(self);
    return cAf6Reg_tohramctl_Base;
    }

static uint32 FaceplateLineRxOverheadByteRegAddr(Tha60290021ModuleOcn self, uint32 overheadByte)
    {
    AtUnused(self);
    switch (overheadByte)
        {
        case cAtSdhLineRsOverheadByteE1: return cAf6Reg_tohe1byte_Base;
        case cAtSdhLineRsOverheadByteF1: return cAf6Reg_tohf1byte_Base;
        case cAtSdhLineMsOverheadByteE2: return cAf6Reg_tohe2byte_Base;
        case cAtSdhLineMsOverheadByteZ1: return cAf6Reg_tohz1byte_Base;
        case cAtSdhLineMsOverheadByteZ2: return cAf6Reg_tohz2byte_Base;
        default: return cInvalidUint32;
        }
    }

static uint32 FaceplateLineTxDefaultOffset(Tha60290021ModuleOcn self, uint32 lineId)
    {
    return ThaModuleOcnBaseAddress((ThaModuleOcn)self) + 256U * lineId;
    }

static eBool IsSts192c(eAtSdhChannelType channelType)
    {
    if ((channelType == cAtSdhChannelTypeVc4_64c) ||
        (channelType == cAtSdhChannelTypeAu4_64c))
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsSts48c(eAtSdhChannelType channelType)
    {
    if ((channelType == cAtSdhChannelTypeVc4_16c) ||
        (channelType == cAtSdhChannelTypeAu4_16c))
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsSts12c(eAtSdhChannelType channelType)
    {
    if ((channelType == cAtSdhChannelTypeVc4_4c) ||
        (channelType == cAtSdhChannelTypeAu4_4c))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet FaceplateStsIdSw2Hw(Tha60290021ModuleOcn self, AtSdhChannel sdhChannel, uint8 swSts, uint8* sliceId, uint8 *hwStsInSlice)
    {
    uint32 localSwSts48, localSwSts12;
    uint32 sts48Id = 0, sts12Id = 0, sts3Id = 0, sts1Id = 0;
    AtSdhLine line = AtSdhChannelLineObjectGet(sdhChannel);
    eAtSdhLineRate rate = AtSdhLineRateGet(line);
    uint32 lineId = AtChannelIdGet((AtChannel)line);
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(sdhChannel);
    AtUnused(self);

    localSwSts48 = (swSts % 48);
    localSwSts12 = (localSwSts48 % 12);

    switch (rate)
        {
        case cAtSdhLineRateStm64:
            sts48Id = (swSts / 48);

            if (IsSts192c(channelType) || IsSts48c(channelType))
                {
                *sliceId      = (uint8)sts48Id;
                *hwStsInSlice = (uint8)localSwSts48;
                return cAtOk;
                }

            sts12Id = (localSwSts48 / 12);
            sts3Id  = (localSwSts12 / 3);
            sts1Id  = (localSwSts12 % 3);
            break;

        case cAtSdhLineRateStm16:
            sts48Id = (lineId / 4);

            if (IsSts48c(channelType))
                {
                *sliceId      = (uint8)sts48Id;
                *hwStsInSlice = (uint8)localSwSts48;
                return cAtOk;
                }

            sts12Id = (localSwSts48 / 12);
            sts3Id  = (localSwSts12 / 3);
            sts1Id  = (localSwSts12 % 3);
            break;

        case cAtSdhLineRateStm0:
        case cAtSdhLineRateStm1:
        case cAtSdhLineRateStm4:
            sts48Id = (lineId / 4);
            sts12Id = (lineId % 4);
            sts3Id  = (localSwSts12 / 3);
            sts1Id  = (localSwSts12 % 3);
            break;

        case cAtSdhLineRateInvalid:
        default:
            AtChannelLog((AtChannel)sdhChannel, cAtLogLevelCritical, AtSourceLocation,
                         "Cannot convert hardware STS because of unknown line rate: %d\r\n",
                         rate);
            return cAtErrorInvlParm;
        }

    if (sliceId)
        *sliceId = (uint8)sts48Id;

    if (hwStsInSlice)
        {
        if (IsSts12c(channelType))
            {
            if (rate == cAtSdhLineRateStm4)
                *hwStsInSlice = (uint8) ((localSwSts12 % 4) + ((localSwSts12 / 4) * 16) + ((localSwSts48 / 12) * 4) + (sts12Id * 4));
            else
                *hwStsInSlice = (uint8) ((localSwSts12 % 4) + ((localSwSts12 / 4) * 16) + ((localSwSts48 / 12) * 4));
            }
        else
            {
            *hwStsInSlice = (uint8)((sts1Id * 16) + (sts3Id) + (sts12Id * 4));
            }
        }

    return cAtOk;
    }

static uint32 HoStsDefaultOffset(Tha60290081ModuleOcn self, AtSdhChannel auVc, uint8 slice, uint8 hwSts)
    {
    AtUnused(auVc);
    return ThaModuleOcnBaseAddress((ThaModuleOcn)self) + (uint32)(256U * slice + hwSts);
    }

static eAtRet TerminatedAuVcBusTypeSet(Tha60290081ModuleOcn self, AtSdhChannel auVc, uint8 busType)
    {
    uint8 numSts = AtSdhChannelNumSts(auVc);
    uint8 startSts1 = AtSdhChannelSts1Get(auVc);
    uint8 sts_i;

    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint32 regAddr, regVal;
        uint8 hwSlice, hwSts;
        uint32 offset;
        eAtRet ret;

        ret = ThaSdhChannelHwStsGet(auVc, cThaModuleOcn, (uint8)(startSts1 + sts_i), &hwSlice, &hwSts);
        if (ret != cAtOk)
            {
            AtModuleLog((AtModule)self, cAtLogLevelCritical, AtSourceLocation, "Can not convert to hardware id\r\n");
            return ret;
            }

        offset = HoStsDefaultOffset(self, auVc, hwSlice, hwSts);
        regAddr = cAf6Reg_glbtxhosel_reg_Base + offset;
        regVal = mModuleHwRead(self, regAddr);
        mRegFieldSet(regVal, cAf6_glbtxhosel_reg_TxHoSelCtl_, busType);
        mModuleHwWrite(self, regAddr, regVal);
        }

    return cAtOk;
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(self), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, InternalRamCreate);
        mMethodOverride(m_AtModuleOverride, AllInternalRamsDescription);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void OverrideThaModuleOcn(AtModule self)
    {
    ThaModuleOcn module = (ThaModuleOcn)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleOcnMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleOcnOverride, m_ThaModuleOcnMethods, sizeof(m_ThaModuleOcnOverride));

        mMethodOverride(m_ThaModuleOcnOverride, DefaultSet);
        mMethodOverride(m_ThaModuleOcnOverride, SohOverEthIsSupported);
        mMethodOverride(m_ThaModuleOcnOverride, TohThresholdDefaultSet);
        mMethodOverride(m_ThaModuleOcnOverride, ChannelStsIdSw2HwGet);
        }

    mMethodsSet(module, &m_ThaModuleOcnOverride);
    }

static void OverrideTha60210011ModuleOcn(AtModule self)
    {
    Tha60210011ModuleOcn module = (Tha60210011ModuleOcn)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011ModuleOcnMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModuleOcnOverride, mMethodsGet(module), sizeof(m_Tha60210011ModuleOcnOverride));

        mMethodOverride(m_Tha60210011ModuleOcnOverride, HwStsDefaultOffset);
        }

    mMethodsSet(module, &m_Tha60210011ModuleOcnOverride);
    }

static void OverrideTha60290021ModuleOcn(AtModule self)
    {
    Tha60290021ModuleOcn module = (Tha60290021ModuleOcn)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021ModuleOcnOverride, mMethodsGet(module), sizeof(m_Tha60290021ModuleOcnOverride));

        mMethodOverride(m_Tha60290021ModuleOcnOverride, NumFaceplateSlices);
        mMethodOverride(m_Tha60290021ModuleOcnOverride, FaceplateXcStartLineId);
        mMethodOverride(m_Tha60290021ModuleOcnOverride, TerminatedXcStartLineId);
        mMethodOverride(m_Tha60290021ModuleOcnOverride, NumXcLines);
        mMethodOverride(m_Tha60290021ModuleOcnOverride, FaceplateGroupOffset);
        mMethodOverride(m_Tha60290021ModuleOcnOverride, FaceplateLineDefaultOffset);
        mMethodOverride(m_Tha60290021ModuleOcnOverride, FaceplateLineTxDefaultOffset);
        mMethodOverride(m_Tha60290021ModuleOcnOverride, FaceplateStsMappingDefaultSet);
        mMethodOverride(m_Tha60290021ModuleOcnOverride, TohMonitoringChannelControlRegAddr);
        mMethodOverride(m_Tha60290021ModuleOcnOverride, FaceplateLineRxOverheadByteRegAddr);
        mMethodOverride(m_Tha60290021ModuleOcnOverride, FaceplateStsIdSw2Hw);
        }

    mMethodsSet(module, &m_Tha60290021ModuleOcnOverride);
    }

static void OverrideTha60290022ModuleOcn(AtModule self)
    {
    Tha60290022ModuleOcn module = (Tha60290022ModuleOcn)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290022ModuleOcnOverride, mMethodsGet(module), sizeof(m_Tha60290022ModuleOcnOverride));

        mMethodOverride(m_Tha60290022ModuleOcnOverride, ClockExtractorSquelchingAisLEnable);
        mMethodOverride(m_Tha60290022ModuleOcnOverride, ClockExtractorSquelchingAisLIsEnabled);
        }

    mMethodsSet(module, &m_Tha60290022ModuleOcnOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideThaModuleOcn(self);
    OverrideTha60210011ModuleOcn(self);
    OverrideTha60290021ModuleOcn(self);
    OverrideTha60290022ModuleOcn(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081ModuleOcn);
    }

AtModule Tha60290081ModuleOcnObjectInit(AtModule self, AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60291022ModuleOcnObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60290081ModuleOcnNew(AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha60290081ModuleOcnObjectInit(newModule, device);
    }

eAtRet Tha60290081ModuleOcnTerminatedAuVcBusTypeSet(Tha60290081ModuleOcn self, AtSdhChannel auVc, uint8 busType)
    {
    if (self)
        return TerminatedAuVcBusTypeSet(self, auVc, busType);
    return cAtErrorNullPointer;
    }

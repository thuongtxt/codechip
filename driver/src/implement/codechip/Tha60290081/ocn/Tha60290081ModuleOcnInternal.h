/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : OCN
 * 
 * File        : Tha60290081ModuleOcnInternal.h
 * 
 * Created Date: Jun 15, 2019
 *
 * Description : 60290081 Module OCN representation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290081MODULEOCNINTERNAL_H_
#define _THA60290081MODULEOCNINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60291022/ocn/Tha60291022ModuleOcnInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290081ModuleOcn
    {
    tTha60291022ModuleOcn super;
    }tTha60290081ModuleOcn;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60290081ModuleOcnObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290081MODULEOCNINTERNAL_H_ */


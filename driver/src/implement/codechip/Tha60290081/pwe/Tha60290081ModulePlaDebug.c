/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PWE
 *
 * File        : Tha60290081ModulePlaDebug.c
 *
 * Created Date: Aug 16, 2019
 *
 * Description : Debug PLA
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/man/AtModuleInternal.h"
#include "../../Tha60210021/man/Tha6021DebugPrint.h"
#include "Tha60290081ModulePlaDebugReg.h"
#include "Tha60290081ModulePwe.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6029BitFieldInfo
    {
    const char *name;
    uint32 mask;
    }tTha6029BitFieldInfo;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void PlaStickyDebug(ThaModulePwe self, const char *addressName, uint32 address,
                           const tTha6029BitFieldInfo* bitInfo, uint8 numOfBits)
    {
    AtDebugger debugger = NULL;
    uint32 regAddr = ThaModulePwePlaBaseAddress(self) + address;
    uint32 regVal, i;

    regVal = mModuleHwRead(self, regAddr);

    Tha6021DebugPrintRegName(debugger, addressName, regAddr, regVal);
    for (i = 0; i < numOfBits; i++)
        Tha6021DebugPrintErrorBit(debugger, bitInfo[i].name, regVal, bitInfo[i].mask);
    Tha6021DebugPrintStop(debugger);

    /* Clear sticky */
    mModuleHwWrite(self, regAddr, regVal);
    }

static void DebugPlaIntf1Stk(ThaModulePwe self)
    {
    const tTha6029BitFieldInfo m_PlaIntf1Stk[] = {
                                                {"Output PWE expected Flow",     cBit31},
                                                {"Eth Vld Input PLA",            cBit30},
                                                {"Eth Err Input PLA",            cBit29},
                                                {"Output PLA Buf Get",           cBit28},
                                                {"Input Info from RSQ",          cBit27},
                                                {"Output Enable to RSQ",         cBit26},
                                                {"Output Info to RSQ",           cBit25},
                                                {"Input DEC Error",              cBit24},
                                                {"Input DEC Start Packet",       cBit23},
                                                {"Input DEC End Packet",         cBit22},
                                                {"Input DEC expected Lid",       cBit21},
                                                {"Input DEC Valid",              cBit20},
                                                {"Input VCG Start Packet",       cBit19},
                                                {"Input VCG End Packet",         cBit18},
                                                {"Input VCG expected Lid",       cBit17},
                                                {"Input VCG Valid",              cBit16},
                                                {"Input VCG Error",              cBit15},
                                                {"Output VCAT Pkt expected Lid", cBit13},
                                                {"Output VCAT Pkt Vld",          cBit12},
                                                {"Output VCAT Pkt Enable",       cBit10},
                                                {"Input VCAT Pkt expected Lid",  cBit9},
                                                {"Input VCAT Pkt Valid",         cBit8},
                                                {"Output PLAPSN Port 0 BuffErr", cBit7},
                                                {"Output PLA Near Full",         cBit6},
                                                {"Output VCAT TDM expected Lid", cBit5},
                                                {"Output VCAT TDM Valid",        cBit4},
                                                {"Input VCAT TDM Start Packet",  cBit3},
                                                {"Input VCAT TDM End Packet",    cBit2},
                                                {"Input VCAT TDM expected Lid",  cBit1},
                                                {"Input VCAT TDM Valid",         cBit0}
                                                };

    PlaStickyDebug(self, "Payload Assembler Interface#1 Sticky", cAf6Reg_pla_interface1_stk_Base,
                   m_PlaIntf1Stk, mCount(m_PlaIntf1Stk));
    }

static void DebugPlaIntf2Stk(ThaModulePwe self)
    {
    const tTha6029BitFieldInfo m_PlaIntf2Stk[] = {
                                                {"ReadPort0 PSN Infor Req ",    cBit31},
                                                {"ReadPort0 ACK Cnt Enable",    cBit30},
                                                {"ReadPort0 Req Cnt Enable",    cBit29},
                                                {"ReadPort0 Pkt Enable",        cBit28},
                                                {"ReadPort0 PSN Queue 3 Full",  cBit27},
                                                {"ReadPort0 PSN Queue 2 Full",  cBit26},
                                                {"ReadPort0 PSN Queue 1 Full",  cBit25},
                                                {"ReadPort0 PSN Queue 0 Full",  cBit24},
                                                {"ReadPort0 Pkt OutPut Read",   cBit23},
                                                {"ReadPort0 pkt Cache Done",    cBit22},
                                                {"ReadPort0 pkt data fifo err", cBit21},
                                                {"ReadPort0 pkt info fifo err", cBit20},
                                                {"ReadBuf0 PW pkt VLD fifo err",cBit19},
                                                {"ReadBuf0 pkt info fifo err",  cBit18},
                                                {"ReadBuf0 PSN ACK fifo err",   cBit17},
                                                {"ReadBuf0 PSN VLD fifo err",   cBit16},
                                                {"ReadPort0 Dat Ready",         cBit15},
                                                {"Output PW Core Start Packet", cBit14},
                                                {"Output PW Core End Packet",   cBit13},
                                                {"Output PW Core Valid",        cBit12},
                                                {"ReadPort0 Ready",             cBit11},
                                                {"Input PSN Vld",               cBit10},
                                                {"Input PSN Ack",               cBit9},
                                                {"Output PSN Request",          cBit8},
                                                {"ReadPort0 Buf Output Req Full",cBit7},
                                                {"Input Read DDR Vld",          cBit6},
                                                {"Input Read DDR Ack",          cBit5},
                                                {"Output Read DDR Request",     cBit4},
                                                {"ReadPort0 Buf Output Req",    cBit3},
                                                {"Input Write DDR Vld",         cBit2},
                                                {"Input Write DDR Ack",         cBit1},
                                                {"Output Write DDR Request",    cBit0}
                                                };

    PlaStickyDebug(self, "Payload Assembler Interface#2 Sticky", cAf6Reg_pla_interface2_stk_Base,
                   m_PlaIntf2Stk, mCount(m_PlaIntf2Stk));
    }

static void DebugPlaStk(ThaModulePwe self)
    {
    const tTha6029BitFieldInfo m_PlaDebugStk[] = {
                                                {"CRC check error",             cBit31},
                                                {"Write Cache same err",        cBit30},
                                                {"Write Cache empty err",       cBit29},
                                                {"Write Cache err",             cBit28},
                                                {"Enque VCG Block empty err",   cBit26},
                                                {"Enque VCG Block same err",    cBit25},
                                                {"WriteDDR Req Full",           cBit24},
                                                {"WriteDDR ACK fifo err",       cBit23},
                                                {"Enque Block Cfg err",         cBit22},
                                                {"WriteDDR VLD ready fifo err", cBit21},
                                                {"WriteDDR VLD fifo err",       cBit20},
                                                {"Mux PW LO OC48_4 err",        cBit15},
                                                {"Mux PW LO OC48_3 err",        cBit14},
                                                {"Mux PW LO OC48_2 err",        cBit13},
                                                {"Mux PW LO OC48_1 err",        cBit12},
                                                {"Mux VCG input3 VCAT err",     cBit6},
                                                {"Mux VCG input2 MSG err",      cBit5},
                                                {"Mux VCG input1 PW err",       cBit4},
                                                {"Mux VCG input3 Eth err",      cBit2},
                                                {"Mux VCG input2 VCG err",      cBit1},
                                                {"Mux VCG input1 PW err",       cBit0},
                                                };

    PlaStickyDebug(self, "Payload Assembler debug#1 Sticky", cAf6Reg_pla_debug1_stk_Base,
                   m_PlaDebugStk, mCount(m_PlaDebugStk));
    }

static void DebugPlaStk2(ThaModulePwe self)
    {
    const tTha6029BitFieldInfo m_PlaDebugStk[] = {{"Input VCG Miss Len   Packet", cBit28},
                                                  {"Input VCG Miss End   Packet", cBit27},
                                                  {"Input VCG Miss Start Packet", cBit26},
                                                  {"Input VCG Miss End   Packet", cBit25},
                                                  {"Input VCG Miss Start Packet", cBit24},
                                                  {"PLA Out Sel P1"             , cBit7},
                                                  {"PLA Out Sel P0"             , cBit6},
                                                  {"PLA Out RSQ P0"             , cBit5},
                                                  {"PLA Out RSQ P0"             , cBit4},
                                                  {"PLA Out Req PWE P0"         , cBit3},
                                                  {"PLA Out Req PWE P0"         , cBit2},
                                                  {"PWE Get Data Port1"         , cBit1},
                                                  {"PWE Get Data Port0"         , cBit0}
                                                  };

    PlaStickyDebug(self, "Payload Assembler debug#2 Sticky", cAf6Reg_pla_debug2_stk_Base,
                   m_PlaDebugStk, mCount(m_PlaDebugStk));
    }

void Tha60290081ModulePwePlaCacheDebug(ThaModulePwe self)
    {
    AtDebugger debugger = NULL;
    uint32 regAddr = ThaModulePwePlaBaseAddress(self) + cAf6Reg_pla_debug_sta1;
    uint32 regVal = mModuleHwRead(self, regAddr);

    Tha6021DebugPrintRegName(debugger, "Payload Assembler Debug Status#1", regAddr, regVal);
    AtPrintc(cSevNormal, "    %-50s: %d\r\n", "VCG Block Number", mRegField(regVal, cAf6_pla_debug_sta1_PlaVCGBlkNum_));
    AtPrintc(cSevNormal, "    %-50s: %d\r\n", "Write cache number", mRegField(regVal, cAf6_pla_debug_sta1_PlaWrCacheNum_));
    Tha6021DebugPrintStop(debugger);

    mModuleHwWrite(self, regAddr, regVal);
    }

void Tha60290081ModulePwePlaDebug(ThaModulePwe self)
    {
    DebugPlaIntf1Stk(self);
    DebugPlaIntf2Stk(self);
    DebugPlaStk(self);
    DebugPlaStk2(self);
    }


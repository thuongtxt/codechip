/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PWE
 * 
 * File        : Tha60290081ModulePwe.h
 * 
 * Created Date: Jul 15, 2019
 *
 * Description : 60290081 Module PWE
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290081MODULEPWE_H_
#define _THA60290081MODULEPWE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/pwe/ThaModulePwe.h"
#include "../../../default/man/ThaDevice.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290081ModulePwe *Tha60290081ModulePwe;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha60290081ModulePweFlowHeaderRead(Tha60290081ModulePwe self, AtEthFlow flow, uint8 *buffer, uint32 *bufferSize);
eAtRet Tha60290081ModulePweFlowHeaderWrite(Tha60290081ModulePwe self, AtEthFlow flow, uint8 *buffer, uint32 headerLenInByte);
eAtRet Tha60290081ModulePweFlowHeaderLengthSet(Tha60290081ModulePwe self, AtEthFlow flow, uint32 headerLenInBytes);
uint32 Tha60290081ModulePweFlowHeaderLengthGet(Tha60290081ModulePwe self, AtEthFlow flow);

/* VLAN control */
eAtRet Tha60290081ModulePweEthFlowCVlanTpidSet(Tha60290081ModulePwe self, AtEthFlow flow, uint16 cVlanTpid);
eAtRet Tha60290081ModulePweEthFlowCVlanSet(Tha60290081ModulePwe self, AtEthFlow flow, tAtEthVlanTag *cVlan);
eAtRet Tha60290081ModulePweEthFlowCVlanEnable(Tha60290081ModulePwe self, AtEthFlow flow, eBool enable);

uint32 Tha60290081ModulePwePwFlowId(ThaModulePwe self, AtPw pw);

/* Flow lookup control */
eAtRet Tha60290081ModulePwePlaPwEthFlowLookup(Tha60290081ModulePwe self, AtEthFlow flow, AtPw pw, eBool enable);
eAtRet Tha60290081ModulePwePlaEosEthFlowLookup(Tha60290081ModulePwe self, AtEthFlow flow, AtChannel channel, eBool enable);

/* PLA debugging */
void Tha60290081ModulePwePlaDebug(ThaModulePwe self);
void Tha60290081ModulePwePlaCacheDebug(ThaModulePwe self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290081MODULEPWE_H_ */


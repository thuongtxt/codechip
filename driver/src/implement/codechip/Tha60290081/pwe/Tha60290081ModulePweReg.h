/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PWE
 * 
 * File        : Tha60290081ModulePweReg.h
 * 
 * Created Date: Aug 2, 2019
 *
 * Description : 60290081 module PWE
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290081MODULEPWEREG_H_
#define _THA60290081MODULEPWEREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Transmit Ethernet Header Mode Control
Reg Addr   : 0x00000000 - 0x001FFF #The address format for these registers is 0x00000000 + PWID
Reg Formula: 0x00000000 +  PWID
    Where  :
           + $PWID(0-5375): Pseudowire ID
Reg Desc   :
This register is used to control read/write Pseudowire Transmit Ethernet Header Value from/to DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_pw_txeth_hdr_mode                                                                   0x00000000
#define cAf6Reg_pw_txeth_hdr_mode_WidthVal                                                                  32

/*--------------------------------------
BitField Name: TxEthCwVlanDis
BitField Type: RW
BitField Desc: this is configuration to disable CW or VLAN insertion 1: Disable
CW for PW mode or disable insert VLAN for EoS mode 0: Enable (default) CW for PW
mode or enable insert VLAN for EoS mode
BitField Bits: [11]
--------------------------------------*/
#define cAf6_pw_txeth_hdr_mode_TxEthCwVlanDis_Mask                                                      cBit11
#define cAf6_pw_txeth_hdr_mode_TxEthCwVlanDis_Shift                                                         11

/*--------------------------------------
BitField Name: TxEthMsPW
BitField Type: RW
BitField Desc: this is configuration for MS PW service
BitField Bits: [10]
--------------------------------------*/
#define cAf6_pw_txeth_hdr_mode_TxEthMsPW_Mask                                                           cBit10
#define cAf6_pw_txeth_hdr_mode_TxEthMsPW_Shift                                                              10

/*--------------------------------------
BitField Name: TxEthPwSubVlanSel
BitField Type: RW
BitField Desc: Tx PW Subport VLAN selection 1: Select second global transmit PW
subport VLAN 0: Select first global transmit PW subport VLAN
BitField Bits: [9]
--------------------------------------*/
#define cAf6_pw_txeth_hdr_mode_TxEthPwSubVlanSel_Mask                                                    cBit9
#define cAf6_pw_txeth_hdr_mode_TxEthPwSubVlanSel_Shift                                                       9

/*--------------------------------------
BitField Name: TxEthPadMode
BitField Type: RW
BitField Desc: this is configuration for insertion PAD in short packets 0:
Insert PAD when total Ethernet packet length is less than 64 bytes. Refer to
IEEE 802.3 1: Insert PAD when control word plus payload length is less than 64
bytes 2: Insert PAD when total Ethernet packet length minus VLANs and MPLS outer
labels(if exist) is less than 64 bytes
BitField Bits: [8:7]
--------------------------------------*/
#define cAf6_pw_txeth_hdr_mode_TxEthPadMode_Mask                                                       cBit8_7
#define cAf6_pw_txeth_hdr_mode_TxEthPadMode_Shift                                                            7

/*--------------------------------------
BitField Name: TxEthPwRtpEn
BitField Type: RW
BitField Desc: this is RTP enable for TDM PW packet 1: Enable RTP 0: Disable RTP
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pw_txeth_hdr_mode_TxEthPwRtpEn_Mask                                                         cBit6
#define cAf6_pw_txeth_hdr_mode_TxEthPwRtpEn_Shift                                                            6

/*--------------------------------------
BitField Name: TxEthPwPsnType
BitField Type: RW
BitField Desc: this is Transmit PSN header mode working 1: PW PSN header is
UDP/IPv4 2: PW PSN header is UDP/IPv6 3: PW MPLS no outer label over Ipv4 (total
1 MPLS label) 4: PW MPLS no outer label over Ipv6 (total 1 MPLS label) 5: PW
MPLS one outer label over Ipv4 (total 2 MPLS label) 6: PW MPLS one outer label
over Ipv6 (total 2 MPLS label) 7: PW MPLS two outer label over Ipv4 (total 3
MPLS label) 8: PW MPLS two outer label over Ipv6 (total 3 MPLS label) Others:
for other PW PSN header type
BitField Bits: [5:2]
--------------------------------------*/
#define cAf6_pw_txeth_hdr_mode_TxEthPwPsnType_Mask                                                     cBit5_2
#define cAf6_pw_txeth_hdr_mode_TxEthPwPsnType_Shift                                                          2

/*--------------------------------------
BitField Name: TxEthPwNumVlan
BitField Type: RW
BitField Desc: This is number of vlan in Transmit Ethernet packet 0: no vlan 1:
1 vlan 2: 2 vlan
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_pw_txeth_hdr_mode_TxEthPwNumVlan_Mask                                                     cBit1_0
#define cAf6_pw_txeth_hdr_mode_TxEthPwNumVlan_Shift                                                          0

/*------------------------------------------------------------------------------
Reg Name   : Pseudowire VLAN Insertion
Reg Addr   : 0x0000A000 - 0x00BFFF
Reg Formula: 0x0000A000 +  CH_ID
    Where  :
           + $CH_ID(0-5375): Flow ID
Reg Desc   :
This register is used to configuration VLAN insertion base on CH_ID

------------------------------------------------------------------------------*/
#define cAf6Reg_TxEth_VlanIns                                                                       0x0000A000

/*--------------------------------------
BitField Name: TxEth_VlanIns
BitField Type: RW
BitField Desc: this is configuration VLAN insertion
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_TxEth_VlanIns_TxEth_TpidIns_Mask                                                         cBit31_16
#define cAf6_TxEth_VlanIns_TxEth_TpidIns_Shift                                                               16

#define cAf6_TxEth_VlanIns_TxEth_VlanIns_Mask                                                         cBit15_0
#define cAf6_TxEth_VlanIns_TxEth_VlanIns_Shift                                                               0

#ifdef __cplusplus
}
#endif
#endif /* _THA60290081MODULEPWEREG_H_ */


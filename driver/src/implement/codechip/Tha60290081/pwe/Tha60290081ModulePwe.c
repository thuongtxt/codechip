/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PWE
 *
 * File        : Tha60290081ModulePwe.c
 *
 * Created Date: Jun 17, 2019
 *
 * Description : 60291022 module PWE implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/pw/Tha60210011ModulePw.h"
#include "../encap/Tha60290081ModuleEncap.h"
#include "Tha60290081ModulePweInternal.h"
#include "Tha60290081ModulePlaReg.h"
#include "Tha60290081ModulePwe.h"
#include "Tha60290081ModulePweReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods                     m_AtModuleOverride;
static tThaModulePweMethods                 m_ThaModulePweOverride;
static tTha60210011ModulePweMethods         m_Tha60210011ModulePweOverride;
static tTha60210051ModulePweMethods         m_Tha60210051ModulePweOverride;
static tTha60290022ModulePweMethods         m_Tha60290022ModulePweOverride;

/* Save super implementation */
static const tAtModuleMethods              *m_AtModuleMethods = NULL;
static const tThaModulePweMethods          *m_ThaModulePweMethods = NULL;
static const tTha60210011ModulePweMethods  *m_Tha60210011ModulePweMethods = NULL;
static const tTha60290022ModulePweMethods  *m_Tha60290022ModulePweMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool InternalRamIsReserved(AtModule self, AtInternalRam ram)
    {
    const char *ramName = AtInternalRamName(ram);
    static char reservedRamOc96Slice2[] = "OC96#2";
    static char reservedRamOc96Slice3[] = "OC96#3";

    if ((AtStrstr(ramName, reservedRamOc96Slice2) != NULL) ||
        (AtStrstr(ramName, reservedRamOc96Slice3) != NULL))
        return cAtTrue;

    return m_AtModuleMethods->InternalRamIsReserved(self, ram);
    }

static eAtRet HeaderRead(ThaModulePwe self, AtPw pw, uint8 *buffer, uint8 bufferSize)
    {
    if (AtPwEthFlowGet(pw) == NULL)
        return cAtErrorNotReady;

    return m_ThaModulePweMethods->HeaderRead(self, pw, buffer, bufferSize);
    }

static eAtRet HeaderWrite(ThaModulePwe self, AtPw pw, uint8 *buffer, uint8 headerLenInByte, AtPwPsn psn)
    {
    if (AtPwEthFlowGet(pw) == NULL)
        return cAtErrorNotReady;

    return m_ThaModulePweMethods->HeaderWrite(self, pw, buffer, headerLenInByte, psn);
    }

static eAtRet RawHeaderWrite(ThaModulePwe self, AtPw pw, uint8 *buffer, uint8 headerLenInByte)
    {
    if (AtPwEthFlowGet(pw) == NULL)
        return cAtErrorNotReady;

    return m_ThaModulePweMethods->RawHeaderWrite(self, pw, buffer, headerLenInByte);
    }

static uint32 PwPweDefaultOffset(ThaModulePwe self, AtPw pw)
    {
    return Tha60290081ModulePwePwFlowId(self, pw) + ThaModulePweBaseAddress(self);
    }

static uint32 PsnChannelIdMask(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return cAf6_pla_out_psnpro_ctrl_PlaOutPsnProFlowCtr_Mask;
    }

static uint32 PsnChannelIdShift(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return cAf6_pla_out_psnpro_ctrl_PlaOutPsnProFlowCtr_Shift;
    }

static uint32 PsnPwHwId(Tha60210011ModulePwe self, AtPw adapter)
    {
    return Tha60290081ModulePwePwFlowId((ThaModulePwe)self, adapter);
    }

static uint32 ByteMask(uint8 byte_i)
    {
    return (cBit7_0 << (byte_i * 8));
    }

static uint8 ByteShift(uint8 byte_i)
    {
    return (uint8)(byte_i * 8);
    }

static uint32 PutByteToDword(uint8 value, uint8 byte_i)
    {
    return (((uint32)value) << ByteShift(byte_i)) & ByteMask(byte_i);
    }

static uint32 PlaPsnHeaderControl(Tha60210011ModulePwe self)
    {
    return mMethodsGet(self)->PsnControlReg(self) + ThaModulePwePlaBaseAddress((ThaModulePwe)self);
    }

static uint32 PlaPsnHeaderBuffer(Tha60210011ModulePwe self)
    {
    return mMethodsGet(self)->PsnBufferCtrlReg(self) + ThaModulePwePlaBaseAddress((ThaModulePwe)self);
    }

static eAtRet WaitForHwDone(Tha60210011ModulePwe self)
    {
    const uint32 cHeaderProcessTimeOutInMs = 100;
    tAtOsalCurTime curTime, startTime;
    uint32 elapse = 0;
    uint32 regVal = 0;
    uint32 regAddr;

    regAddr = PlaPsnHeaderControl(self);
    AtOsalCurTimeGet(&startTime);
    while (elapse < cHeaderProcessTimeOutInMs)
        {
        regVal = mModuleHwRead(self, regAddr);

        if (AtDeviceIsSimulated(AtModuleDeviceGet((AtModule)self)))
            return cAtOk;

        if ((regVal & cAf6_pla_out_psnpro_ctrl_PlaOutPsnProReqCtr_Mask) == 0)
            return cAtOk;

        /* Calculate elapse time and retry */
        AtOsalCurTimeGet(&curTime);
        elapse = mTimeIntervalInMsGet(startTime, curTime);
        }

    return cAtErrorIndrAcsTimeOut;
    }

static eAtRet ReadRequest(Tha60210011ModulePwe self, AtEthFlow flow, uint8 page, uint32 headerLenInByte)
    {
    const uint8 cReadCommand = 1;
    const uint8 cRequest = 1;
    uint32 regVal = 0;
    uint32 regAddr = PlaPsnHeaderControl(self);
    eAtRet ret;
    uint32 hwFlowId = AtChannelHwIdGet((AtChannel)flow);
    uint32 fieldMask, fieldShift;

    fieldMask = mMethodsGet(self)->PsnChannelIdMask(self);
    fieldShift = mMethodsGet(self)->PsnChannelIdShift(self);
    mRegFieldSet(regVal, field, hwFlowId);

    fieldMask = mMethodsGet(self)->PlaOutPsnProPageCtrMask(self);
    fieldShift = mMethodsGet(self)->PlaOutPsnProPageCtrShift(self);
    mRegFieldSet(regVal, field, page);

    mRegFieldSet(regVal, cAf6_pla_out_psnpro_ctrl_PlaOutPsnProRnWCtr_, cReadCommand);
    mRegFieldSet(regVal, cAf6_pla_out_psnpro_ctrl_PlaOutPsnProReqCtr_, cRequest);
    mRegFieldSet(regVal, cAf6_pla_out_psnpro_ctrl_PlaOutPsnProLenCtr_, headerLenInByte);
    mModuleHwWrite(self, regAddr, regVal);

    ret = WaitForHwDone(self);
    if (ret != cAtOk)
        AtChannelLog((AtChannel)flow, cAtLogLevelCritical, AtSourceLocation, "Read request fail, header length: %u bytes, page %u (0x%08X = 0x%08X)\r\n",
                     headerLenInByte, page, regAddr, mModuleHwRead(self, regAddr));
    return ret;
    }

static eAtRet WriteRequest(Tha60210011ModulePwe self, AtEthFlow flow, uint8 page, uint32 headerLenInByte)
    {
    const uint8 cWriteCommand = 0;
    const uint8 cRequest = 1;
    uint32 regVal = 0;
    uint32 regAddr = PlaPsnHeaderControl(self);
    eAtRet ret;
    uint32 hwFlowId = AtChannelHwIdGet((AtChannel)flow);
    uint32 fieldMask, fieldShift;

    fieldMask = mMethodsGet(self)->PsnChannelIdMask(self);
    fieldShift = mMethodsGet(self)->PsnChannelIdShift(self);
    mRegFieldSet(regVal, field, hwFlowId);

    fieldMask = mMethodsGet(self)->PlaOutPsnProPageCtrMask(self);
    fieldShift = mMethodsGet(self)->PlaOutPsnProPageCtrShift(self);
    mRegFieldSet(regVal, field, page);

    mRegFieldSet(regVal, cAf6_pla_out_psnpro_ctrl_PlaOutPsnProLenCtr_, headerLenInByte);
    mRegFieldSet(regVal, cAf6_pla_out_psnpro_ctrl_PlaOutPsnProRnWCtr_, cWriteCommand);
    mRegFieldSet(regVal, cAf6_pla_out_psnpro_ctrl_PlaOutPsnProReqCtr_, cRequest);

    mModuleHwWrite(self, regAddr, regVal);

    if (!mMethodsGet(self)->CanWaitForHwDone(self))
        return cAtOk;

    ret = WaitForHwDone(self);
    if (ret != cAtOk)
        AtModuleLog((AtModule)self, cAtLogLevelCritical, AtSourceLocation, "Write request fail, header length: %u bytes, page %u (0x%08X = 0x%08X)\r\n",
                     headerLenInByte, page, regAddr, mModuleHwRead(self, regAddr));

    return ret;
    }

static AtIpCore IpCoreGet(Tha60210011ModulePwe self)
    {
    return AtDeviceIpCoreGet(AtModuleDeviceGet((AtModule)self), 0);
    }

static eAtRet ReadHeaderFromBuffer(Tha60210011ModulePwe self, AtEthFlow flow, uint8 *buffer, uint32 headerLenInByte)
    {
    AtIpCore ipCore = IpCoreGet(self);
    uint32 num16BytesSegment, segment_i;
    uint32 longRegVal[cThaLongRegMaxSize];
    uint8 byte_i, byteInDw_i;
    int8 dword_i;
    uint32 regAddr = PlaPsnHeaderBuffer(self);
    uint32 numRequestedByte = headerLenInByte;
    AtUnused(flow);

    if (headerLenInByte > cMaxSupportedHeaderLengthInByte)
        {
        mChannelLog(flow, cAtLogLevelCritical, "Ethernet header is too long");
        return cAtErrorModeNotSupport;
        }

    num16BytesSegment = numRequestedByte / 16;
    byte_i = 0;

    for (segment_i = 0; segment_i < num16BytesSegment; segment_i++)
        {
        mModuleHwLongRead(self, regAddr + segment_i, longRegVal, 4, ipCore);
        for (dword_i = 3; dword_i >= 0; dword_i--)
            {
            buffer[byte_i++] = (uint8)(longRegVal[dword_i] >> 24);
            buffer[byte_i++] = (uint8)(longRegVal[dword_i] >> 16);
            buffer[byte_i++] = (uint8)(longRegVal[dword_i] >> 8);
            buffer[byte_i++] = (uint8)(longRegVal[dword_i]);
            }
        }

    if (byte_i >= numRequestedByte)
        return cAtOk;

    /* Last segment */
    mModuleHwLongRead(self, regAddr + segment_i, longRegVal, 4, ipCore);
    byteInDw_i = 3;
    dword_i = 3;
    while (byte_i < headerLenInByte)
        {
        buffer[byte_i++] = (uint8)(longRegVal[dword_i] >> ByteShift(byteInDw_i));
        if (byteInDw_i == 0)
            {
            byteInDw_i = 3;
            dword_i--;
            }
        else
            byteInDw_i = (uint8)(byteInDw_i - 1);
        }

    return cAtOk;
    }

static eAtRet WriteHeaderToBuffer(Tha60210011ModulePwe self, AtEthFlow flow, uint8 *buffer, uint32 headerLenInByte)
    {
    AtIpCore ipCore = IpCoreGet(self);
    uint32 num16BytesSegment, segment_i, writtenNumDword, writtenNumByte;
    uint32 longRegVal[cThaLongRegMaxSize];
    uint8 byteInDword_i;
    int8 dword_i;
    uint32 regAddr = PlaPsnHeaderBuffer(self);
    uint32 numByteToWrite = headerLenInByte;

    if (numByteToWrite > cMaxSupportedHeaderLengthInByte)
        {
        mChannelLog(flow, cAtLogLevelCritical, "Ethernet header is too long");
        return cAtErrorModeNotSupport;
        }

    num16BytesSegment = numByteToWrite / 16;
    writtenNumDword = 0;
    for (segment_i = 0; segment_i < num16BytesSegment; segment_i++)
        {
        for (dword_i = 3; dword_i >= 0; dword_i--)
            {
            longRegVal[dword_i] = ThaPktUtilPutDataToRegister(buffer, (uint8)writtenNumDword);
            writtenNumDword = writtenNumDword + 1;
            }

        mModuleHwLongWrite(self, regAddr + segment_i, longRegVal, 4, ipCore);
        }

    /* Maybe there are still some last bytes if headerLenInByte is not multiple of 16 */
    writtenNumByte = writtenNumDword * 4;
    if (writtenNumByte >= headerLenInByte)
        return cAtOk;

    AtOsalMemInit(longRegVal, 0, sizeof(longRegVal));
    byteInDword_i = 3;
    dword_i = 4;

    while ((writtenNumByte < headerLenInByte) && (dword_i > 0))
        {
        longRegVal[dword_i - 1] |= PutByteToDword(buffer[writtenNumByte], byteInDword_i);

        writtenNumByte++;
        if (byteInDword_i == 0)
            {
            byteInDword_i = 3;
            dword_i--;
            }
        else
            byteInDword_i = (uint8)(byteInDword_i - 1);
        }

    mModuleHwLongWrite(self, regAddr + segment_i, longRegVal, 4, ipCore);

    return cAtOk;
    }

static eAtRet HelperFlowHeaderRead(Tha60210011ModulePwe self, AtEthFlow flow, uint8 page, uint8 *buffer, uint32 *headerLenInByte)
    {
    uint32 lengthToRequest = *headerLenInByte;

    /* It's not superfluous, request 0 byte will make HW hung */
    if (lengthToRequest == 0)
        return cAtOk;

    if (ReadRequest(self, flow, page, lengthToRequest) != cAtOk)
        mChannelReturn(flow, cAtErrorIndrAcsTimeOut);

    return ReadHeaderFromBuffer(self, flow, buffer, lengthToRequest);
    }

static eAtRet HelperFlowHeaderWrite(Tha60210011ModulePwe self, AtEthFlow flow, uint8 page, uint8 *buffer, uint32 headerLenInByte)
    {
    eAtRet ret;
    uint32 writeRequestNumByte = headerLenInByte;

    if (WriteHeaderToBuffer(self, flow, buffer, headerLenInByte) != cAtOk)
        return cAtErrorModeNotSupport;

    /* It's not superfluous, request 0 byte will make HW hung */
    if (writeRequestNumByte == 0)
        return cAtOk;

    ret = WriteRequest(self, flow, page, writeRequestNumByte);
    if (ret != cAtOk)
        mChannelReturn(flow, ret);

    return ret;
    }

static void PlaInterfaceDebug(Tha60210011ModulePwe self)
    {
    Tha60290081ModulePwePlaDebug((ThaModulePwe)self);
    }

static void CacheUsageGet(ThaModulePwe self, tThaPweCache *cache)
    {
    AtUnused(self);
    AtUnused(cache);
    }

static eBool CanAccessCacheUsage(ThaModulePwe self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void PlaCacheShow(Tha60210051ModulePwe self)
    {
    Tha60290081ModulePwePlaCacheDebug((ThaModulePwe)self);
    }

static uint32 PlaPwFlowLookupOffset(Tha60210011ModulePwe self, AtPw pw)
    {
    AtUnused(self);
    return AtChannelIdGet((AtChannel)pw);
    }

static eAtRet PwPlaFlowLookupControl(Tha60210011ModulePwe self, AtPw pw)
    {
    return cAf6Reg_pla_lk_flow_ctrl_Base + PlaPwFlowLookupOffset(self, pw) +
           ThaModulePwePlaBaseAddress((ThaModulePwe)self);
    }

static eAtRet PwPlaFlowBlockBufferControl(Tha60210011ModulePwe self, AtPw pw)
    {
    return cAf6Reg_pla_blk_buf_ctrl_Base + AtChannelIdGet((AtChannel)pw) +
           ThaModulePwePlaBaseAddress((ThaModulePwe)self);
    }

static void PwPlaRegsShow(Tha60210011ModulePwe self, AtPw pw)
    {
    AtChannel circuit = AtPwBoundCircuitGet(pw);

    if (circuit == NULL)
        {
        AtPrintc(cSevInfo, "   - Pw has not bound circuit\r\n");
        return;
        }

    m_Tha60210011ModulePweMethods->PwPlaRegsShow(self, pw);

    ThaModulePweRegDisplay(pw, "Payload Assembler Lookup Flow Control", PwPlaFlowLookupControl(self, pw));
    ThaModulePweRegDisplay(pw, "Payload Assembler Block Buffer Control", PwPlaFlowBlockBufferControl(self, pw));
    }

static uint8 PlaPwBufferBlockType(Tha60210011ModulePwe self, AtPw pw)
    {
    eAtPwType pwType = AtPwTypeGet(pw);
    AtUnused(self);

    if (pwType == cAtPwTypeCEP)
        {
        AtSdhChannel sdhChannel = (AtSdhChannel)AtPwBoundCircuitGet(pw);
        eAtSdhChannelType vcType = AtSdhChannelTypeGet(sdhChannel);

        if (vcType == cAtSdhChannelTypeVc3)     return 1;
        if (vcType == cAtSdhChannelTypeVc4)     return 2;
        if (vcType == cAtSdhChannelTypeVc4_4c)  return 3;
        if (vcType == cAtSdhChannelTypeVc4_nc)  return 4;
        if (vcType == cAtSdhChannelTypeVc4_16c) return 5;

        return 0;
        }

    if (pwType == cAtPwTypeSAToP)
        {
        AtPdhChannel pdhChannel = (AtPdhChannel)AtPwBoundCircuitGet(pw);
        eAtPdhChannelType channelType = AtPdhChannelTypeGet(pdhChannel);

        if (channelType == cAtPdhChannelTypeDs3) return 1;
        if (channelType == cAtPdhChannelTypeE3)  return 1;

        return 0;
        }

    /* CESoP */
    return 0;
    }

static uint32 HoPlaPwStartBufferId(Tha60210011ModulePwe self, AtPw pw)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)AtPwBoundCircuitGet(pw);
    uint8 slice, stsInSlice;
    AtUnused(self);

    if (ThaSdhChannel2HwMasterStsId(sdhChannel, cThaModulePwe, &slice, &stsInSlice) != cAtOk)
        return cInvalidUint8;

    return (uint32)(slice * 48U + AtSdhChannelSts1Get(sdhChannel));
    }

static uint32 LoPlaPwBufferId(Tha60210011ModulePwe self, AtPw pw)
    {
    /* PW buffer ID shall be based on Flow ID as same as other service. */
    return Tha60290081ModulePwePwFlowId((ThaModulePwe)self, pw);
    }

static uint32 PlaPwBufferId(Tha60210011ModulePwe self, AtPw pw)
    {
    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        return LoPlaPwBufferId(self, pw);

    return HoPlaPwStartBufferId(self, pw);
    }

static eAtRet PlaPwBufferSet(Tha60210011ModulePwe self, AtPw pw)
    {
    uint32 regAddr, regVal;

    regAddr = PwPlaFlowBlockBufferControl(self, pw);
    regVal = mChannelHwRead(pw, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cAf6_pla_blk_buf_ctrl_PlaBlkIDCtr_, PlaPwBufferId(self, pw));
    mRegFieldSet(regVal, cAf6_pla_blk_buf_ctrl_PlaBlkTypeCtr_, PlaPwBufferBlockType(self, pw));
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static void PlaPwDefaultHwConfigure(Tha60210011ModulePwe self, ThaPwAdapter pwAdapter)
    {
    m_Tha60210011ModulePweMethods->PlaPwDefaultHwConfigure(self, pwAdapter);
    PlaPwBufferSet(self, (AtPw)pwAdapter);
    }

static uint32 NumGroupsPerRegister(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return 8;
    }

static uint32 PlaLkPWIDCtrlMask(Tha60290022ModulePwe self)
    {
    AtUnused(self);
    return cAf6_pla_pld_ctrl_PlaLkPWIDCtrl_Mask;
    }

static uint8 PlaLkPWIDCtrlShift(Tha60290022ModulePwe self)
    {
    AtUnused(self);
    return cAf6_pla_pld_ctrl_PlaLkPWIDCtrl_Shift;
    }

static uint32 HwNumOc96Slices(Tha60290022ModulePwe self)
    {
    AtUnused(self);
    return 2;
    }

static uint32 PlaOutUpsrGrpCtrlMask(Tha60290022ModulePwe self)
    {
    AtUnused(self);
    return cAf6_pla_pw_pro_ctrl_PlaOutUpsrGrpCtr_Mask;
    }

static uint8 PlaOutUpsrGrpCtrlShift(Tha60290022ModulePwe self)
    {
    AtUnused(self);
    return cAf6_pla_pw_pro_ctrl_PlaOutUpsrGrpCtr_Shift;
    }

static uint32 PlaOutHspwGrpCtrlMask(Tha60290022ModulePwe self)
    {
    AtUnused(self);
    return cAf6_pla_pw_pro_ctrl_PlaOutHspwGrpCtr_Mask;
    }

static uint8 PlaOutHspwGrpCtrlShift(Tha60290022ModulePwe self)
    {
    AtUnused(self);
    return cAf6_pla_pw_pro_ctrl_PlaOutHspwGrpCtr_Shift;
    }

static eBool HasPlaHiRateCtrl(Tha60290022ModulePwe self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet FlowHeaderRead(Tha60290081ModulePwe self, AtEthFlow flow, uint8 *buffer, uint32 *bufferSize)
    {
    return HelperFlowHeaderRead((Tha60210011ModulePwe)self, flow, cWorkingPsnPage, buffer, bufferSize);
    }

static eAtRet FlowHeaderWrite(Tha60290081ModulePwe self, AtEthFlow flow, uint8 *buffer, uint32 headerLenInByte)
    {
    return HelperFlowHeaderWrite((Tha60210011ModulePwe)self, flow, cWorkingPsnPage, buffer, headerLenInByte);
    }

static uint32 PlaFlowLookupControl(Tha60290081ModulePwe self)
    {
    return cAf6Reg_pla_lk_flow_ctrl_Base + ThaModulePwePlaBaseAddress((ThaModulePwe)self);
    }

static AtModuleEncap ModuleEncap(Tha60290081ModulePwe self)
    {
    return (AtModuleEncap)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModuleEncap);
    }

static uint32 PlaGfpChannelOffset(Tha60290081ModulePwe self, AtChannel channel)
    {
    static const uint32 cPlaStartLoGfpOffset = 5376U;
    static const uint32 cPlaStartHoGfpOffset = 5504U;

    if (Tha60290081ModuleEncapChannelIsHoGfpChannel(ModuleEncap(self), channel))
        return cPlaStartHoGfpOffset + AtChannelHwIdGet(channel);

    return cPlaStartLoGfpOffset + AtChannelHwIdGet(channel);
    }

static eAtRet PlaEthFlowLookup(Tha60290081ModulePwe self, AtEthFlow flow, uint32 channelOffset, eBool enable)
    {
    uint32 regAddr = PlaFlowLookupControl(self) + channelOffset;
    uint32 outFlow;
    uint32 regVal;

    if (enable)
        outFlow = AtChannelHwIdGet((AtChannel)flow);
    else
        outFlow = cAf6_pla_lk_flow_ctrl_PlaOutLkFlowCtrl_Mask;

    regVal = mChannelHwRead(flow, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cAf6_pla_lk_flow_ctrl_PlaOutLkFlowCtrl_, outFlow);
    mChannelHwWrite(flow, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eAtRet PlaEosEthFlowLookup(Tha60290081ModulePwe self, AtEthFlow flow, AtChannel channel, eBool enable)
    {
    return PlaEthFlowLookup(self, flow, PlaGfpChannelOffset(self, channel), enable);
    }

static eAtRet PlaPwEthFlowLookup(Tha60290081ModulePwe self, AtEthFlow flow, AtPw pw, eBool enable)
    {
    return PlaEthFlowLookup(self, flow, PlaPwFlowLookupOffset((Tha60210011ModulePwe)self, pw), enable);
    }

static void OverrideTha60290022ModulePwe(AtModule self)
    {
    Tha60290022ModulePwe pweModule = (Tha60290022ModulePwe)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290022ModulePweMethods = mMethodsGet(pweModule);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290022ModulePweOverride, m_Tha60290022ModulePweMethods, sizeof(m_Tha60290022ModulePweOverride));

        mMethodOverride(m_Tha60290022ModulePweOverride, PlaLkPWIDCtrlMask);
        mMethodOverride(m_Tha60290022ModulePweOverride, PlaLkPWIDCtrlShift);
        mMethodOverride(m_Tha60290022ModulePweOverride, HwNumOc96Slices);
        mMethodOverride(m_Tha60290022ModulePweOverride, PlaOutUpsrGrpCtrlMask);
        mMethodOverride(m_Tha60290022ModulePweOverride, PlaOutUpsrGrpCtrlShift);
        mMethodOverride(m_Tha60290022ModulePweOverride, PlaOutHspwGrpCtrlMask);
        mMethodOverride(m_Tha60290022ModulePweOverride, PlaOutHspwGrpCtrlShift);
        mMethodOverride(m_Tha60290022ModulePweOverride, HasPlaHiRateCtrl);
        }

    mMethodsSet(pweModule, &m_Tha60290022ModulePweOverride);
    }

static void OverrideTha60210051ModulePwe(AtModule self)
    {
    Tha60210051ModulePwe modulePwe = (Tha60210051ModulePwe)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210051ModulePweOverride, mMethodsGet(modulePwe), sizeof(m_Tha60210051ModulePweOverride));

        mMethodOverride(m_Tha60210051ModulePweOverride, PlaCacheShow);
        }

    mMethodsSet(modulePwe, &m_Tha60210051ModulePweOverride);
    }

static void OverrideTha60210011ModulePwe(AtModule self)
    {
    Tha60210011ModulePwe modulePwe = (Tha60210011ModulePwe)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011ModulePweMethods = mMethodsGet(modulePwe);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModulePweOverride, mMethodsGet(modulePwe), sizeof(m_Tha60210011ModulePweOverride));

        mMethodOverride(m_Tha60210011ModulePweOverride, PsnPwHwId);
        mMethodOverride(m_Tha60210011ModulePweOverride, PsnChannelIdMask);
        mMethodOverride(m_Tha60210011ModulePweOverride, PsnChannelIdShift);
        mMethodOverride(m_Tha60210011ModulePweOverride, PlaInterfaceDebug);
        mMethodOverride(m_Tha60210011ModulePweOverride, PwPlaRegsShow);
        mMethodOverride(m_Tha60210011ModulePweOverride, PlaPwDefaultHwConfigure);
        mMethodOverride(m_Tha60210011ModulePweOverride, NumGroupsPerRegister);
        }

    mMethodsSet(modulePwe, &m_Tha60210011ModulePweOverride);
    }

static void OverrideThaModulePwe(AtModule self)
    {
    ThaModulePwe pweModule = (ThaModulePwe)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePweMethods = mMethodsGet(pweModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePweOverride, m_ThaModulePweMethods, sizeof(m_ThaModulePweOverride));

        mMethodOverride(m_ThaModulePweOverride, HeaderRead);
        mMethodOverride(m_ThaModulePweOverride, HeaderWrite);
        mMethodOverride(m_ThaModulePweOverride, RawHeaderWrite);
        mMethodOverride(m_ThaModulePweOverride, PwPweDefaultOffset);
        mMethodOverride(m_ThaModulePweOverride, CacheUsageGet);
        mMethodOverride(m_ThaModulePweOverride, CanAccessCacheUsage);
        }

    mMethodsSet(pweModule, &m_ThaModulePweOverride);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(self), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, InternalRamIsReserved);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideThaModulePwe(self);
    OverrideTha60210011ModulePwe(self);
    OverrideTha60210051ModulePwe(self);
    OverrideTha60290022ModulePwe(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081ModulePwe);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022ModulePweObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60290081ModulePweNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

eAtRet Tha60290081ModulePweFlowHeaderRead(Tha60290081ModulePwe self, AtEthFlow flow, uint8 *buffer, uint32 *bufferSize)
    {
    if (self)
        return FlowHeaderRead(self, flow, buffer, bufferSize);
    return cAtErrorNullPointer;
    }

eAtRet Tha60290081ModulePweFlowHeaderWrite(Tha60290081ModulePwe self, AtEthFlow flow, uint8 *buffer, uint32 headerLenInByte)
    {
    if (self)
        return FlowHeaderWrite(self, flow, buffer, headerLenInByte);
    return cAtErrorNullPointer;
    }

eAtRet Tha60290081ModulePweFlowHeaderLengthSet(Tha60290081ModulePwe self, AtEthFlow flow, uint32 headerLenInBytes)
    {
    uint32 flowId = AtChannelHwIdGet((AtChannel)flow);
    uint32 regAddr = cAf6Reg_pla_pw_psn_ctrl_Base + flowId + Tha60210011ModulePlaBaseAddress();
    uint32 regVal;
    AtUnused(self);
    regVal = mChannelHwRead(flow, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cAf6_pla_pw_psn_ctrl_PlaOutPSNLenCtrl_, headerLenInBytes);
    mChannelHwWrite(flow, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

uint32 Tha60290081ModulePweFlowHeaderLengthGet(Tha60290081ModulePwe self, AtEthFlow flow)
    {
    uint32 flowId = AtChannelHwIdGet((AtChannel)flow);
    uint32 regAddr = cAf6Reg_pla_pw_psn_ctrl_Base + flowId + Tha60210011ModulePlaBaseAddress();
    uint32 regVal;
    AtUnused(self);
    regVal = mChannelHwRead(flow, regAddr, cThaModulePwe);
    return mRegField(regVal, cAf6_pla_pw_psn_ctrl_PlaOutPSNLenCtrl_);
    }

uint32 Tha60290081ModulePwePwFlowId(ThaModulePwe self, AtPw pw)
    {
    AtUnused(self);
    return AtChannelHwIdGet((AtChannel)AtPwEthFlowGet(pw));
    }

eAtRet Tha60290081ModulePwePlaPwEthFlowLookup(Tha60290081ModulePwe self, AtEthFlow flow, AtPw pw, eBool enable)
    {
    if (self)
        return PlaPwEthFlowLookup(self, flow, pw, enable);
    return cAtErrorNullPointer;
    }

eAtRet Tha60290081ModulePwePlaEosEthFlowLookup(Tha60290081ModulePwe self, AtEthFlow flow, AtChannel channel, eBool enable)
    {
    if (self)
        return PlaEosEthFlowLookup(self, flow, channel, enable);
    return cAtErrorNullPointer;
    }

eAtRet Tha60290081ModulePweEthFlowCVlanTpidSet(Tha60290081ModulePwe self, AtEthFlow flow, uint16 cVlanTpid)
    {
    uint32 flowId = AtChannelHwIdGet((AtChannel)flow);
    uint32 regAddr = cAf6Reg_TxEth_VlanIns + flowId + Tha60210011ModulePweBaseAddress();
    uint32 regVal;
    AtUnused(self);

    regVal = mChannelHwRead(flow, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cAf6_TxEth_VlanIns_TxEth_TpidIns_, cVlanTpid);
    mChannelHwWrite(flow, regAddr, regVal, cThaModulePwe);
    return cAtOk;
    }

eAtRet Tha60290081ModulePweEthFlowCVlanSet(Tha60290081ModulePwe self, AtEthFlow flow, tAtEthVlanTag *cVlan)
    {
    uint32 regAddr;
    uint32 regVal;
    uint32 flowId = AtChannelHwIdGet((AtChannel)flow);
    uint16 vlan = ThaPktUtilVlanTagToUint16(cVlan);

    regAddr = cAf6Reg_TxEth_VlanIns + flowId + ThaModulePweBaseAddress((ThaModulePwe)self);
    regVal = mChannelHwRead(flow, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cAf6_TxEth_VlanIns_TxEth_VlanIns_, vlan);
    mChannelHwWrite(flow, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

eAtRet Tha60290081ModulePweEthFlowCVlanEnable(Tha60290081ModulePwe self, AtEthFlow flow, eBool enable)
    {
    uint32 regAddr;
    uint32 regVal;
    uint32 flowId = AtChannelHwIdGet((AtChannel)flow);

    regAddr = cAf6Reg_pw_txeth_hdr_mode + flowId + ThaModulePweBaseAddress((ThaModulePwe)self);
    regVal = mChannelHwRead(flow, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cAf6_pw_txeth_hdr_mode_TxEthCwVlanDis_, enable ? 0x0 : 0x1);
    mChannelHwWrite(flow, regAddr, regVal, cThaModulePwe);
    return cAtOk;
    }

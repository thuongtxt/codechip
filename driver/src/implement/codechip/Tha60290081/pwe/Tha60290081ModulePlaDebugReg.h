/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PLA
 * 
 * File        : Tha60290081ModulePlaDebugReg.h
 * 
 * Created Date: Aug 16, 2019
 *
 * Description : Pla debug
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290081MODULEPLADEBUGREG_H_
#define _THA60290081MODULEPLADEBUGREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler clk311 Control
Reg Addr   : 0x0_0000 - 0x1A000
Reg Formula: 0x0_0000 + $Oc96Slice*32768 + $Oc48Slice*8192
    Where  :
           + $Oc96Slice(0-3): OC-96 slices, there are total 4xOC-96 slices for 20G
           + $Oc48Slice(0-1): OC-48 slices, there are total 2xOC-48 slices per OC-96 slice
Reg Desc   :
This register is used to control pwid for debug

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_out_clk311_ctrl_Base                                                               0x00000

/*--------------------------------------
BitField Name: PWID_for_debug
BitField Type: RW
BitField Desc:
BitField Bits: [22:12]
--------------------------------------*/
#define cAf6_pla_out_clk311_ctrl_PWID_for_debug_Mask                                                 cBit22_12
#define cAf6_pla_out_clk311_ctrl_PWID_for_debug_Shift                                                       12


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler OC48 clk311 Sticky
Reg Addr   : 0x0_0001 - 0x1_A001
Reg Formula: 0x0_0001 + $Oc96Slice*32768 + $Oc48Slice*8192
    Where  :
           + $Oc96Slice(0-3): OC-96 slices, there are total 4xOC-96 slices for 20G
           + $Oc48Slice(0-1): OC-48 slices, there are total 2xOC-48 slices per OC-96 slice
Reg Desc   :
This register is used to used to sticky some alarms for debug per OC-48 @clk311.02 domain

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_oc48_clk311_stk_Base                                                               0x00001
#define cAf6Reg_pla_oc48_clk311_stk_WidthVal                                                                32

/*--------------------------------------
BitField Name: Pla311OC48_ConvErr
BitField Type: W2C
BitField Desc: OC48 conver clock error
BitField Bits: [11]
--------------------------------------*/
#define cAf6_pla_oc48_clk311_stk_Pla311OC48_ConvErr_Mask                                                cBit11
#define cAf6_pla_oc48_clk311_stk_Pla311OC48_ConvErr_Shift                                                   11

/*--------------------------------------
BitField Name: Pla311OC48_InPosPW
BitField Type: W2C
BitField Desc: OC48 input CEP Pos based PW
BitField Bits: [9]
--------------------------------------*/
#define cAf6_pla_oc48_clk311_stk_Pla311OC48_InPosPW_Mask                                                 cBit9
#define cAf6_pla_oc48_clk311_stk_Pla311OC48_InPosPW_Shift                                                    9

/*--------------------------------------
BitField Name: Pla311OC48_InNegPW
BitField Type: W2C
BitField Desc: OC48 input CEP Neg based PW
BitField Bits: [8]
--------------------------------------*/
#define cAf6_pla_oc48_clk311_stk_Pla311OC48_InNegPW_Mask                                                 cBit8
#define cAf6_pla_oc48_clk311_stk_Pla311OC48_InNegPW_Shift                                                    8

/*--------------------------------------
BitField Name: Pla311OC48_InValodPW
BitField Type: W2C
BitField Desc: OC48 input Valid based PW
BitField Bits: [7]
--------------------------------------*/
#define cAf6_pla_oc48_clk311_stk_Pla311OC48_InValodPW_Mask                                               cBit7
#define cAf6_pla_oc48_clk311_stk_Pla311OC48_InValodPW_Shift                                                  7

/*--------------------------------------
BitField Name: Pla311OC48_InValid
BitField Type: W2C
BitField Desc: OC48 input valid
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pla_oc48_clk311_stk_Pla311OC48_InValid_Mask                                                 cBit6
#define cAf6_pla_oc48_clk311_stk_Pla311OC48_InValid_Shift                                                    6

/*--------------------------------------
BitField Name: Pla311OC48_InAisPW
BitField Type: W2C
BitField Desc: OC48 input AIS based PW
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pla_oc48_clk311_stk_Pla311OC48_InAisPW_Mask                                                 cBit5
#define cAf6_pla_oc48_clk311_stk_Pla311OC48_InAisPW_Shift                                                    5

/*--------------------------------------
BitField Name: Pla311OC48_InAIS
BitField Type: W2C
BitField Desc: OC48 input AIS
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pla_oc48_clk311_stk_Pla311OC48_InAIS_Mask                                                   cBit4
#define cAf6_pla_oc48_clk311_stk_Pla311OC48_InAIS_Shift                                                      4

/*--------------------------------------
BitField Name: Pla311OC48_InJ1PosPW
BitField Type: W2C
BitField Desc: OC48 input CEP J1 Posiion based PW
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pla_oc48_clk311_stk_Pla311OC48_InJ1PosPW_Mask                                               cBit3
#define cAf6_pla_oc48_clk311_stk_Pla311OC48_InJ1PosPW_Shift                                                  3

/*--------------------------------------
BitField Name: Pla311OC48_InPos
BitField Type: W2C
BitField Desc: OC48 input CEP Pos Pointer
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pla_oc48_clk311_stk_Pla311OC48_InPos_Mask                                                   cBit2
#define cAf6_pla_oc48_clk311_stk_Pla311OC48_InPos_Shift                                                      2

/*--------------------------------------
BitField Name: Pla311OC48_InNeg
BitField Type: W2C
BitField Desc: OC48 input CEP Neg Pointer
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pla_oc48_clk311_stk_Pla311OC48_InNeg_Mask                                                   cBit1
#define cAf6_pla_oc48_clk311_stk_Pla311OC48_InNeg_Shift                                                      1

/*--------------------------------------
BitField Name: Pla311OC48_InJ1Pos
BitField Type: W2C
BitField Desc: OC48 input CEP J1 Position
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pla_oc48_clk311_stk_Pla311OC48_InJ1Pos_Mask                                                 cBit0
#define cAf6_pla_oc48_clk311_stk_Pla311OC48_InJ1Pos_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Interface#1 Sticky
Reg Addr   : 0x4_2000
Reg Formula:
    Where  :
Reg Desc   :
This register is used to used to sticky some intefrace of PLA

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_interface1_stk_Base                                                                0x42000

/*--------------------------------------
BitField Name: PlaOutPWEFlowVld
BitField Type: W2C
BitField Desc: Output PWE expected Flow
BitField Bits: [31]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaOutPWEFlowVld_Mask                                                   cBit31
#define cAf6_pla_interface1_stk_PlaOutPWEFlowVld_Shift                                                      31

/*--------------------------------------
BitField Name: PlaInEthVld
BitField Type: W2C
BitField Desc: Eth Vld Input PLA
BitField Bits: [30]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaInEthVld_Mask                                                        cBit30
#define cAf6_pla_interface1_stk_PlaInEthVld_Shift                                                           30

/*--------------------------------------
BitField Name: PlaInEthErr
BitField Type: W2C
BitField Desc: Eth Err Input PLA
BitField Bits: [29]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaInEthErr_Mask                                                        cBit29
#define cAf6_pla_interface1_stk_PlaInEthErr_Shift                                                           29

/*--------------------------------------
BitField Name: PlaInfBufGet
BitField Type: W2C
BitField Desc: Output PLA Buf Get
BitField Bits: [28]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaInfBufGet_Mask                                                       cBit28
#define cAf6_pla_interface1_stk_PlaInfBufGet_Shift                                                          28

/*--------------------------------------
BitField Name: PlaInVldFromRSQ
BitField Type: W2C
BitField Desc: Input Info from RSQ
BitField Bits: [27]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaInVldFromRSQ_Mask                                                    cBit27
#define cAf6_pla_interface1_stk_PlaInVldFromRSQ_Shift                                                       27

/*--------------------------------------
BitField Name: PlaOutEn2RSQ
BitField Type: W2C
BitField Desc: Output Enable to RSQ
BitField Bits: [26]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaOutEn2RSQ_Mask                                                       cBit26
#define cAf6_pla_interface1_stk_PlaOutEn2RSQ_Shift                                                          26

/*--------------------------------------
BitField Name: PlaOutVld2RSQ
BitField Type: W2C
BitField Desc: Output Info to RSQ
BitField Bits: [25]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaOutVld2RSQ_Mask                                                      cBit25
#define cAf6_pla_interface1_stk_PlaOutVld2RSQ_Shift                                                         25

/*--------------------------------------
BitField Name: PlaInDecErr
BitField Type: W2C
BitField Desc: Input MSG Error
BitField Bits: [24]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaInDecErr_Mask                                                        cBit24
#define cAf6_pla_interface1_stk_PlaInDecErr_Shift                                                           24

/*--------------------------------------
BitField Name: PlaInDecSoP
BitField Type: W2C
BitField Desc: Input MSG Start Packet
BitField Bits: [23]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaInDecSoP_Mask                                                        cBit23
#define cAf6_pla_interface1_stk_PlaInDecSoP_Shift                                                           23

/*--------------------------------------
BitField Name: PlaInDecEoP
BitField Type: W2C
BitField Desc: Input MSG End Packet
BitField Bits: [22]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaInDecEoP_Mask                                                        cBit22
#define cAf6_pla_interface1_stk_PlaInDecEoP_Shift                                                           22

/*--------------------------------------
BitField Name: PlaInDecLidVld
BitField Type: W2C
BitField Desc: Input MSG expected Lid
BitField Bits: [21]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaInDecLidVld_Mask                                                     cBit21
#define cAf6_pla_interface1_stk_PlaInDecLidVld_Shift                                                        21

/*--------------------------------------
BitField Name: PlaInDecVld
BitField Type: W2C
BitField Desc: Input MSG Valid
BitField Bits: [20]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaInDecVld_Mask                                                        cBit20
#define cAf6_pla_interface1_stk_PlaInDecVld_Shift                                                           20

/*--------------------------------------
BitField Name: PlaInVCGSoP
BitField Type: W2C
BitField Desc: Input VCG Start Packet
BitField Bits: [19]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaInVCGSoP_Mask                                                        cBit19
#define cAf6_pla_interface1_stk_PlaInVCGSoP_Shift                                                           19

/*--------------------------------------
BitField Name: PlaInVCGEoP
BitField Type: W2C
BitField Desc: Input VCG End Packet
BitField Bits: [18]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaInVCGEoP_Mask                                                        cBit18
#define cAf6_pla_interface1_stk_PlaInVCGEoP_Shift                                                           18

/*--------------------------------------
BitField Name: PlaInVCGLidVld
BitField Type: W2C
BitField Desc: Input VCG expected Lid
BitField Bits: [17]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaInVCGLidVld_Mask                                                     cBit17
#define cAf6_pla_interface1_stk_PlaInVCGLidVld_Shift                                                        17

/*--------------------------------------
BitField Name: PlaInVCGVld
BitField Type: W2C
BitField Desc: Input VCG Valid
BitField Bits: [16]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaInVCGVld_Mask                                                        cBit16
#define cAf6_pla_interface1_stk_PlaInVCGVld_Shift                                                           16

/*--------------------------------------
BitField Name: PlaInVCGErr
BitField Type: W2C
BitField Desc: Input VCG Error
BitField Bits: [15]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaInVCGErr_Mask                                                        cBit15
#define cAf6_pla_interface1_stk_PlaInVCGErr_Shift                                                           15

/*--------------------------------------
BitField Name: PlaOutVcatPkLidVld
BitField Type: W2C
BitField Desc: Output VCAT Pkt expected Lid
BitField Bits: [13]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaOutVcatPkLidVld_Mask                                                 cBit13
#define cAf6_pla_interface1_stk_PlaOutVcatPkLidVld_Shift                                                    13

/*--------------------------------------
BitField Name: PlaOutVcatPkVld
BitField Type: W2C
BitField Desc: Output VCAT Pkt Vld
BitField Bits: [12]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaOutVcatPkVld_Mask                                                    cBit12
#define cAf6_pla_interface1_stk_PlaOutVcatPkVld_Shift                                                       12

/*--------------------------------------
BitField Name: PlaOutVcatPkEn
BitField Type: W2C
BitField Desc: Output VCAT Pkt Enable
BitField Bits: [10]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaOutVcatPkEn_Mask                                                     cBit10
#define cAf6_pla_interface1_stk_PlaOutVcatPkEn_Shift                                                        10

/*--------------------------------------
BitField Name: PlaInVcatPkLidVld
BitField Type: W2C
BitField Desc: Input VCAT Pkt expected Lid
BitField Bits: [9]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaInVcatPkLidVld_Mask                                                   cBit9
#define cAf6_pla_interface1_stk_PlaInVcatPkLidVld_Shift                                                      9

/*--------------------------------------
BitField Name: PlaInVcatPkVld
BitField Type: W2C
BitField Desc: Input VCAT Pkt Valid
BitField Bits: [8]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaInVcatPkVld_Mask                                                      cBit8
#define cAf6_pla_interface1_stk_PlaInVcatPkVld_Shift                                                         8

/*--------------------------------------
BitField Name: PlaOutPSNBuffErrP0
BitField Type: W2C
BitField Desc: Output PLAPSN Port 0 BuffErr
BitField Bits: [7]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaOutPSNBuffErrP0_Mask                                                  cBit7
#define cAf6_pla_interface1_stk_PlaOutPSNBuffErrP0_Shift                                                     7

/*--------------------------------------
BitField Name: PlaOutSegnearfull
BitField Type: W2C
BitField Desc: Output PLA  Near Full
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaOutSegnearfull_Mask                                                   cBit6
#define cAf6_pla_interface1_stk_PlaOutSegnearfull_Shift                                                      6

/*--------------------------------------
BitField Name: PlaOutVCATTDMLidVld
BitField Type: W2C
BitField Desc: Output VCAT TDM expected Lid
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaOutVCATTDMLidVld_Mask                                                 cBit5
#define cAf6_pla_interface1_stk_PlaOutVCATTDMLidVld_Shift                                                    5

/*--------------------------------------
BitField Name: PlaOutVcatTDMVld
BitField Type: W2C
BitField Desc: Output VCAT TDM Valid
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaOutVcatTDMVld_Mask                                                    cBit4
#define cAf6_pla_interface1_stk_PlaOutVcatTDMVld_Shift                                                       4

/*--------------------------------------
BitField Name: PlaInVcatTDMSoP
BitField Type: W2C
BitField Desc: Input VCAT TDM Start Packet
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaInVcatTDMSoP_Mask                                                     cBit3
#define cAf6_pla_interface1_stk_PlaInVcatTDMSoP_Shift                                                        3

/*--------------------------------------
BitField Name: PlaInVcatTDMEoP
BitField Type: W2C
BitField Desc: Input VCAT TDM End Packet
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaInVcatTDMEoP_Mask                                                     cBit2
#define cAf6_pla_interface1_stk_PlaInVcatTDMEoP_Shift                                                        2

/*--------------------------------------
BitField Name: PlaInVcatTDMLidVld
BitField Type: W2C
BitField Desc: Input VCAT TDM expected Lid
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaInVcatTDMLidVld_Mask                                                  cBit1
#define cAf6_pla_interface1_stk_PlaInVcatTDMLidVld_Shift                                                     1

/*--------------------------------------
BitField Name: PlaInVcatTDMVld
BitField Type: W2C
BitField Desc: Input VCAT TDM Valid
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaInVcatTDMVld_Mask                                                     cBit0
#define cAf6_pla_interface1_stk_PlaInVcatTDMVld_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Interface#2 Sticky
Reg Addr   : 0x4_2001
Reg Formula:
    Where  :
Reg Desc   :
This register is used to used to sticky some intefrace of PLA

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_interface2_stk_Base                                                                0x42001

/*--------------------------------------
BitField Name: PlaPSNInfReq
BitField Type: W2C
BitField Desc: ReadPort0 PSN Infor Req
BitField Bits: [31]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaPSNInfReq_Mask                                                       cBit31
#define cAf6_pla_interface2_stk_PlaPSNInfReq_Shift                                                          31

/*--------------------------------------
BitField Name: PlaPWACKCnt
BitField Type: W2C
BitField Desc: ReadPort0 ACK Cnt Enable
BitField Bits: [30]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaPWACKCnt_Mask                                                        cBit30
#define cAf6_pla_interface2_stk_PlaPWACKCnt_Shift                                                           30

/*--------------------------------------
BitField Name: PlaPWReqCnt
BitField Type: W2C
BitField Desc: ReadPort0 Req Cnt Enable
BitField Bits: [29]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaPWReqCnt_Mask                                                        cBit29
#define cAf6_pla_interface2_stk_PlaPWReqCnt_Shift                                                           29

/*--------------------------------------
BitField Name: PlaPWPktNum
BitField Type: W2C
BitField Desc: ReadPort0 Pkt Enable
BitField Bits: [28]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaPWPktNum_Mask                                                        cBit28
#define cAf6_pla_interface2_stk_PlaPWPktNum_Shift                                                           28

/*--------------------------------------
BitField Name: PlaFFReqPSNQ3Full
BitField Type: W2C
BitField Desc: ReadPort0 PSN Queue 3 Full
BitField Bits: [27]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaFFReqPSNQ3Full_Mask                                                  cBit27
#define cAf6_pla_interface2_stk_PlaFFReqPSNQ3Full_Shift                                                     27

/*--------------------------------------
BitField Name: PlaFFReqPSNQ2Full
BitField Type: W2C
BitField Desc: ReadPort0 PSN Queue 2 Full
BitField Bits: [26]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaFFReqPSNQ2Full_Mask                                                  cBit26
#define cAf6_pla_interface2_stk_PlaFFReqPSNQ2Full_Shift                                                     26

/*--------------------------------------
BitField Name: PlaFFReqPSNQ1Full
BitField Type: W2C
BitField Desc: ReadPort0 PSN Queue 1 Full
BitField Bits: [25]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaFFReqPSNQ1Full_Mask                                                  cBit25
#define cAf6_pla_interface2_stk_PlaFFReqPSNQ1Full_Shift                                                     25

/*--------------------------------------
BitField Name: PlaFFReqPSNQ0Full
BitField Type: W2C
BitField Desc: ReadPort0 PSN Queue 0 Full
BitField Bits: [24]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaFFReqPSNQ0Full_Mask                                                  cBit24
#define cAf6_pla_interface2_stk_PlaFFReqPSNQ0Full_Shift                                                     24

/*--------------------------------------
BitField Name: PlaRdPortBufRdy
BitField Type: W2C
BitField Desc: ReadPort0 Pkt OutPut Read
BitField Bits: [23]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaRdPortBufRdy_Mask                                                    cBit23
#define cAf6_pla_interface2_stk_PlaRdPortBufRdy_Shift                                                       23

/*--------------------------------------
BitField Name: PlaRdPortCacheDone
BitField Type: W2C
BitField Desc: ReadPort0 pkt Cache Done
BitField Bits: [22]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaRdPortCacheDone_Mask                                                 cBit22
#define cAf6_pla_interface2_stk_PlaRdPortCacheDone_Shift                                                    22

/*--------------------------------------
BitField Name: PlaRdPortDatFFErr
BitField Type: W2C
BitField Desc: ReadPort0 pkt data fifo err
BitField Bits: [21]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaRdPortDatFFErr_Mask                                                  cBit21
#define cAf6_pla_interface2_stk_PlaRdPortDatFFErr_Shift                                                     21

/*--------------------------------------
BitField Name: PlaRdPortInfFFErr
BitField Type: W2C
BitField Desc: ReadPort0 pkt info fifo err
BitField Bits: [20]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaRdPortInfFFErr_Mask                                                  cBit20
#define cAf6_pla_interface2_stk_PlaRdPortInfFFErr_Shift                                                     20

/*--------------------------------------
BitField Name: PlaRdBufPWPktVldFFErr
BitField Type: W2C
BitField Desc: ReadBuf0 PW pkt VLD fifo err
BitField Bits: [19]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaRdBufPWPktVldFFErr_Mask                                              cBit19
#define cAf6_pla_interface2_stk_PlaRdBufPWPktVldFFErr_Shift                                                 19

/*--------------------------------------
BitField Name: PlaRdBufPkFFErr
BitField Type: W2C
BitField Desc: ReadBuf0 pkt VLD fifo err
BitField Bits: [18]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaRdBufPkFFErr_Mask                                                    cBit18
#define cAf6_pla_interface2_stk_PlaRdBufPkFFErr_Shift                                                       18

/*--------------------------------------
BitField Name: PlaRdBufPSNAckFFErr
BitField Type: W2C
BitField Desc: ReadBuf0 PSN ACK fifo err
BitField Bits: [17]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaRdBufPSNAckFFErr_Mask                                                cBit17
#define cAf6_pla_interface2_stk_PlaRdBufPSNAckFFErr_Shift                                                   17

/*--------------------------------------
BitField Name: PlaRdBufPSNVldFFErr
BitField Type: W2C
BitField Desc: ReadBuf0 PSN VLD fifo err
BitField Bits: [16]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaRdBufPSNVldFFErr_Mask                                                cBit16
#define cAf6_pla_interface2_stk_PlaRdBufPSNVldFFErr_Shift                                                   16

/*--------------------------------------
BitField Name: PlaRdPortPWDatRdy
BitField Type: W2C
BitField Desc: ReadPort0 Dat Ready
BitField Bits: [15]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaRdPortPWDatRdy_Mask                                                  cBit15
#define cAf6_pla_interface2_stk_PlaRdPortPWDatRdy_Shift                                                     15

/*--------------------------------------
BitField Name: PlaPWCoreSoP
BitField Type: W2C
BitField Desc: Output PW Core Start Packet
BitField Bits: [14]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaPWCoreSoP_Mask                                                       cBit14
#define cAf6_pla_interface2_stk_PlaPWCoreSoP_Shift                                                          14

/*--------------------------------------
BitField Name: PlaPWCoreEoP
BitField Type: W2C
BitField Desc: Output PW Core End Packet
BitField Bits: [13]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaPWCoreEoP_Mask                                                       cBit13
#define cAf6_pla_interface2_stk_PlaPWCoreEoP_Shift                                                          13

/*--------------------------------------
BitField Name: PlaPWCoreVld
BitField Type: W2C
BitField Desc: Output PW Core Valid
BitField Bits: [12]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaPWCoreVld_Mask                                                       cBit12
#define cAf6_pla_interface2_stk_PlaPWCoreVld_Shift                                                          12

/*--------------------------------------
BitField Name: PlaRdPortRdy
BitField Type: W2C
BitField Desc: ReadPort0 Ready
BitField Bits: [11]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaRdPortRdy_Mask                                                       cBit11
#define cAf6_pla_interface2_stk_PlaRdPortRdy_Shift                                                          11

/*--------------------------------------
BitField Name: PlaInPSNVld
BitField Type: W2C
BitField Desc: Input PSN Vld
BitField Bits: [10]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaInPSNVld_Mask                                                        cBit10
#define cAf6_pla_interface2_stk_PlaInPSNVld_Shift                                                           10

/*--------------------------------------
BitField Name: PlaInPSNAck
BitField Type: W2C
BitField Desc: Input PSN Ack
BitField Bits: [9]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaInPSNAck_Mask                                                         cBit9
#define cAf6_pla_interface2_stk_PlaInPSNAck_Shift                                                            9

/*--------------------------------------
BitField Name: PlaOutPSNReq
BitField Type: W2C
BitField Desc: Output PSN Request
BitField Bits: [8]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaOutPSNReq_Mask                                                        cBit8
#define cAf6_pla_interface2_stk_PlaOutPSNReq_Shift                                                           8

/*--------------------------------------
BitField Name: PlaRdPortBufReqFull
BitField Type: W2C
BitField Desc: ReadPort0 Buf Output Req Full
BitField Bits: [7]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaRdPortBufReqFull_Mask                                                 cBit7
#define cAf6_pla_interface2_stk_PlaRdPortBufReqFull_Shift                                                    7

/*--------------------------------------
BitField Name: PlaInDDRRdVld
BitField Type: W2C
BitField Desc: Input Read DDR Vld
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaInDDRRdVld_Mask                                                       cBit6
#define cAf6_pla_interface2_stk_PlaInDDRRdVld_Shift                                                          6

/*--------------------------------------
BitField Name: PlaInDDRRdAck
BitField Type: W2C
BitField Desc: Input Read DDR Ack
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaInDDRRdAck_Mask                                                       cBit5
#define cAf6_pla_interface2_stk_PlaInDDRRdAck_Shift                                                          5

/*--------------------------------------
BitField Name: PlaOutDDRRdReq
BitField Type: W2C
BitField Desc: Output Read DDR Request
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaOutDDRRdReq_Mask                                                      cBit4
#define cAf6_pla_interface2_stk_PlaOutDDRRdReq_Shift                                                         4

/*--------------------------------------
BitField Name: PlaRdPortBufReq
BitField Type: W2C
BitField Desc: ReadPort0 Buf Output Req
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaRdPortBufReq_Mask                                                     cBit3
#define cAf6_pla_interface2_stk_PlaRdPortBufReq_Shift                                                        3

/*--------------------------------------
BitField Name: PlaInDDRWrVld
BitField Type: W2C
BitField Desc: Input Write DDR Vld
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaInDDRWrVld_Mask                                                       cBit2
#define cAf6_pla_interface2_stk_PlaInDDRWrVld_Shift                                                          2

/*--------------------------------------
BitField Name: PlaInDDRWrAck
BitField Type: W2C
BitField Desc: Input Write DDR Ack
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaInDDRWrAck_Mask                                                       cBit1
#define cAf6_pla_interface2_stk_PlaInDDRWrAck_Shift                                                          1

/*--------------------------------------
BitField Name: PlaOutDDRWrReq
BitField Type: W2C
BitField Desc: Output Write DDR Request
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaOutDDRWrReq_Mask                                                      cBit0
#define cAf6_pla_interface2_stk_PlaOutDDRWrReq_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler debug Sticky
Reg Addr   : 0x4_2002
Reg Formula:
    Where  :
Reg Desc   :
This register is used to used to sticky some debug info of PLA

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_debug1_stk_Base                                                                    0x42002

/*--------------------------------------
BitField Name: PlaCRCCheckErr
BitField Type: W2C
BitField Desc: CRC check error
BitField Bits: [31]
--------------------------------------*/
#define cAf6_pla_debug1_stk_PlaCRCCheckErr_Mask                                                         cBit31
#define cAf6_pla_debug1_stk_PlaCRCCheckErr_Shift                                                            31

/*--------------------------------------
BitField Name: PlaWrCacheSameErr
BitField Type: W2C
BitField Desc: Write Cache same err
BitField Bits: [30]
--------------------------------------*/
#define cAf6_pla_debug1_stk_PlaWrCacheSameErr_Mask                                                      cBit30
#define cAf6_pla_debug1_stk_PlaWrCacheSameErr_Shift                                                         30

/*--------------------------------------
BitField Name: PlaWrCacheEmpErr
BitField Type: W2C
BitField Desc: Write Cache empty err
BitField Bits: [29]
--------------------------------------*/
#define cAf6_pla_debug1_stk_PlaWrCacheEmpErr_Mask                                                       cBit29
#define cAf6_pla_debug1_stk_PlaWrCacheEmpErr_Shift                                                          29

/*--------------------------------------
BitField Name: PlaWrCacheErr
BitField Type: W2C
BitField Desc: Write Cache err
BitField Bits: [28]
--------------------------------------*/
#define cAf6_pla_debug1_stk_PlaWrCacheErr_Mask                                                          cBit28
#define cAf6_pla_debug1_stk_PlaWrCacheErr_Shift                                                             28

/*--------------------------------------
BitField Name: PlaEnqVCGBlkEmpErr
BitField Type: W2C
BitField Desc: Enque VCG Block empty err
BitField Bits: [26]
--------------------------------------*/
#define cAf6_pla_debug1_stk_PlaEnqVCGBlkEmpErr_Mask                                                     cBit26
#define cAf6_pla_debug1_stk_PlaEnqVCGBlkEmpErr_Shift                                                        26

/*--------------------------------------
BitField Name: PlaEnqVCGBlkSameErr
BitField Type: W2C
BitField Desc: Enque VCG Block same err
BitField Bits: [25]
--------------------------------------*/
#define cAf6_pla_debug1_stk_PlaEnqVCGBlkSameErr_Mask                                                    cBit25
#define cAf6_pla_debug1_stk_PlaEnqVCGBlkSameErr_Shift                                                       25

/*--------------------------------------
BitField Name: PlaWrDDRReqFull
BitField Type: W2C
BitField Desc: WriteDDR Req Full
BitField Bits: [24]
--------------------------------------*/
#define cAf6_pla_debug1_stk_PlaWrDDRReqFull_Mask                                                        cBit24
#define cAf6_pla_debug1_stk_PlaWrDDRReqFull_Shift                                                           24

/*--------------------------------------
BitField Name: PlaWrDDRACKFFErr
BitField Type: W2C
BitField Desc: WriteDDR ACK fifo err
BitField Bits: [23]
--------------------------------------*/
#define cAf6_pla_debug1_stk_PlaWrDDRACKFFErr_Mask                                                       cBit23
#define cAf6_pla_debug1_stk_PlaWrDDRACKFFErr_Shift                                                          23

/*--------------------------------------
BitField Name: PlaEnqBlkCfgErr
BitField Type: W2C
BitField Desc: Enque Block Cfg err
BitField Bits: [22]
--------------------------------------*/
#define cAf6_pla_debug1_stk_PlaEnqBlkCfgErr_Mask                                                        cBit22
#define cAf6_pla_debug1_stk_PlaEnqBlkCfgErr_Shift                                                           22

/*--------------------------------------
BitField Name: PlaWrDDRVldRdyFFErr
BitField Type: W2C
BitField Desc: WriteDDR VLD ready fifo err
BitField Bits: [21]
--------------------------------------*/
#define cAf6_pla_debug1_stk_PlaWrDDRVldRdyFFErr_Mask                                                    cBit21
#define cAf6_pla_debug1_stk_PlaWrDDRVldRdyFFErr_Shift                                                       21

/*--------------------------------------
BitField Name: PlaWrDDRVldFFErr
BitField Type: W2C
BitField Desc: WriteDDR VLD fifo err
BitField Bits: [20]
--------------------------------------*/
#define cAf6_pla_debug1_stk_PlaWrDDRVldFFErr_Mask                                                       cBit20
#define cAf6_pla_debug1_stk_PlaWrDDRVldFFErr_Shift                                                          20

/*--------------------------------------
BitField Name: PlaMuxPWLOOC48_4Err
BitField Type: W2C
BitField Desc: Mux PW LO OC48_4 err
BitField Bits: [15]
--------------------------------------*/
#define cAf6_pla_debug1_stk_PlaMuxPWLOOC48_4Err_Mask                                                    cBit15
#define cAf6_pla_debug1_stk_PlaMuxPWLOOC48_4Err_Shift                                                       15

/*--------------------------------------
BitField Name: PlaMuxPWLOOC48_3Err
BitField Type: W2C
BitField Desc: Mux PW LO OC48_3 err
BitField Bits: [14]
--------------------------------------*/
#define cAf6_pla_debug1_stk_PlaMuxPWLOOC48_3Err_Mask                                                    cBit14
#define cAf6_pla_debug1_stk_PlaMuxPWLOOC48_3Err_Shift                                                       14

/*--------------------------------------
BitField Name: PlaMuxPWLOOC48_2Err
BitField Type: W2C
BitField Desc: Mux PW LO OC48_2 err
BitField Bits: [13]
--------------------------------------*/
#define cAf6_pla_debug1_stk_PlaMuxPWLOOC48_2Err_Mask                                                    cBit13
#define cAf6_pla_debug1_stk_PlaMuxPWLOOC48_2Err_Shift                                                       13

/*--------------------------------------
BitField Name: PlaMuxPWLOOC48_1Err
BitField Type: W2C
BitField Desc: Mux PW LO OC48_1 err
BitField Bits: [12]
--------------------------------------*/
#define cAf6_pla_debug1_stk_PlaMuxPWLOOC48_1Err_Mask                                                    cBit12
#define cAf6_pla_debug1_stk_PlaMuxPWLOOC48_1Err_Shift                                                       12

/*--------------------------------------
BitField Name: PlaMuxSrcIn3Err
BitField Type: W2C
BitField Desc: Mux VCG input3 VCAT err
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pla_debug1_stk_PlaMuxSrcIn3Err_Mask                                                         cBit6
#define cAf6_pla_debug1_stk_PlaMuxSrcIn3Err_Shift                                                            6

/*--------------------------------------
BitField Name: PlaMuxSrcIn2Err
BitField Type: W2C
BitField Desc: Mux VCG input2 MSG err
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pla_debug1_stk_PlaMuxSrcIn2Err_Mask                                                         cBit5
#define cAf6_pla_debug1_stk_PlaMuxSrcIn2Err_Shift                                                            5

/*--------------------------------------
BitField Name: PlaMuxSrcIn1Err
BitField Type: W2C
BitField Desc: Mux VCG input1 PW err
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pla_debug1_stk_PlaMuxSrcIn1Err_Mask                                                         cBit4
#define cAf6_pla_debug1_stk_PlaMuxSrcIn1Err_Shift                                                            4

/*--------------------------------------
BitField Name: PlaMuxVCGInEthErr
BitField Type: W2C
BitField Desc: Mux VCG input3 Eth err
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pla_debug1_stk_PlaMuxVCGInEthErr_Mask                                                       cBit2
#define cAf6_pla_debug1_stk_PlaMuxVCGInEthErr_Shift                                                          2

/*--------------------------------------
BitField Name: PlaMuxVCGInVCGErr
BitField Type: W2C
BitField Desc: Mux VCG input2 VCG err
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pla_debug1_stk_PlaMuxVCGInVCGErr_Mask                                                       cBit1
#define cAf6_pla_debug1_stk_PlaMuxVCGInVCGErr_Shift                                                          1

/*--------------------------------------
BitField Name: PlaMuxVCGInPWErr
BitField Type: W2C
BitField Desc: Mux VCG input1 PW err
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pla_debug1_stk_PlaMuxVCGInPWErr_Mask                                                        cBit0
#define cAf6_pla_debug1_stk_PlaMuxVCGInPWErr_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler debug Sticky#2
Reg Addr   : 0x4_2004
Reg Formula:
    Where  :
Reg Desc   :
This register is used to used to sticky some debug info of PLA

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_debug2_stk_Base                                                                    0x42004

/*--------------------------------------
BitField Name: PlaInVCATMisLen
BitField Type: W2C
BitField Desc: Input VCG Miss Len   Packet
BitField Bits: [28]
--------------------------------------*/
#define cAf6_pla_debug2_stk_PlaInVCATMisLen_Mask                                                        cBit28
#define cAf6_pla_debug2_stk_PlaInVCATMisLen_Shift                                                           28

/*--------------------------------------
BitField Name: PlaInVCATMisEoP
BitField Type: W2C
BitField Desc: Input VCG Miss End   Packet
BitField Bits: [27]
--------------------------------------*/
#define cAf6_pla_debug2_stk_PlaInVCATMisEoP_Mask                                                        cBit27
#define cAf6_pla_debug2_stk_PlaInVCATMisEoP_Shift                                                           27

/*--------------------------------------
BitField Name: PlaInVCATMisSoP
BitField Type: W2C
BitField Desc: Input VCG Miss Start Packet
BitField Bits: [26]
--------------------------------------*/
#define cAf6_pla_debug2_stk_PlaInVCATMisSoP_Mask                                                        cBit26
#define cAf6_pla_debug2_stk_PlaInVCATMisSoP_Shift                                                           26

/*--------------------------------------
BitField Name: PlaInVCGMisEoP
BitField Type: W2C
BitField Desc: Input VCG Miss End   Packet
BitField Bits: [25]
--------------------------------------*/
#define cAf6_pla_debug2_stk_PlaInVCGMisEoP_Mask                                                         cBit25
#define cAf6_pla_debug2_stk_PlaInVCGMisEoP_Shift                                                            25

/*--------------------------------------
BitField Name: PlaInVCGMisSoP
BitField Type: W2C
BitField Desc: Input VCG Miss Start Packet
BitField Bits: [24]
--------------------------------------*/
#define cAf6_pla_debug2_stk_PlaInVCGMisSoP_Mask                                                         cBit24
#define cAf6_pla_debug2_stk_PlaInVCGMisSoP_Shift                                                            24

/*--------------------------------------
BitField Name: PLAOutSelRdyP1
BitField Type: W2C
BitField Desc: PLA Out Sel P1
BitField Bits: [7]
--------------------------------------*/
#define cAf6_pla_debug2_stk_PLAOutSelRdyP1_Mask                                                          cBit7
#define cAf6_pla_debug2_stk_PLAOutSelRdyP1_Shift                                                             7

/*--------------------------------------
BitField Name: PLAOutSelRdyP0
BitField Type: W2C
BitField Desc: PLA Out Sel P0
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pla_debug2_stk_PLAOutSelRdyP0_Mask                                                          cBit6
#define cAf6_pla_debug2_stk_PLAOutSelRdyP0_Shift                                                             6

/*--------------------------------------
BitField Name: PLAOutRSQRdyP1
BitField Type: W2C
BitField Desc: PLA Out RSQ P1
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pla_debug2_stk_PLAOutRSQRdyP1_Mask                                                          cBit5
#define cAf6_pla_debug2_stk_PLAOutRSQRdyP1_Shift                                                             5

/*--------------------------------------
BitField Name: PLAOutRSQRdyP0
BitField Type: W2C
BitField Desc: PLA Out RSQ P0
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pla_debug2_stk_PLAOutRSQRdyP0_Mask                                                          cBit4
#define cAf6_pla_debug2_stk_PLAOutRSQRdyP0_Shift                                                             4

/*--------------------------------------
BitField Name: PLAInPWEReq1
BitField Type: W2C
BitField Desc: PLA Out Req PWE P1
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pla_debug2_stk_PLAInPWEReq1_Mask                                                            cBit3
#define cAf6_pla_debug2_stk_PLAInPWEReq1_Shift                                                               3

/*--------------------------------------
BitField Name: PLAInPWEReq0
BitField Type: W2C
BitField Desc: PLA Out Req PWE P0
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pla_debug2_stk_PLAInPWEReq0_Mask                                                            cBit2
#define cAf6_pla_debug2_stk_PLAInPWEReq0_Shift                                                               2

/*--------------------------------------
BitField Name: PLAPWEGet1
BitField Type: W2C
BitField Desc: PWE Get Data Port1
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pla_debug2_stk_PLAPWEGet1_Mask                                                              cBit1
#define cAf6_pla_debug2_stk_PLAPWEGet1_Shift                                                                 1

/*--------------------------------------
BitField Name: PLAPWEGet0
BitField Type: W2C
BitField Desc: PWE Get Data Port0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pla_debug2_stk_PLAPWEGet0_Mask                                                              cBit0
#define cAf6_pla_debug2_stk_PLAPWEGet0_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Debug Status#1
Reg Addr   : 0x4_2010
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to used to get some debug info of PLA

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_debug_sta1                                                                         0x42010

/*--------------------------------------
BitField Name: PlaVCGBlkNum
BitField Type: W2C
BitField Desc: VCG Block Number(max 1024)
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_pla_debug_sta1_PlaVCGBlkNum_Mask                                                        cBit31_16
#define cAf6_pla_debug_sta1_PlaVCGBlkNum_Shift                                                              16

/*--------------------------------------
BitField Name: PlaWrCacheNum
BitField Type: W2C
BitField Desc: Write cache number (max 16384)
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_pla_debug_sta1_PlaWrCacheNum_Mask                                                        cBit15_0
#define cAf6_pla_debug_sta1_PlaWrCacheNum_Shift                                                              0

#ifdef __cplusplus
}
#endif
#endif /* _THA60290081MODULEPLADEBUGREG_H_ */


/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      : PLA
 *                                                                              
 * File        : Tha60290081ModulePlaReg.h
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constant definitions of PLA block.
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _THA60290081MODULEPLAREG_H_
#define _THA60290081MODULEPLAREG_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Low-Order Payload Control
Reg Addr   : 0x0_1000-0x0_B7FF
Reg Formula: 0x0_1000 + $Oc96Slice*32768 + $Oc48Slice*8192 + $Pwid
    Where  : 
           + $Oc96Slice(0-1): OC-96 slices, there are total 2xOC-96 slices for 10G
           + $Oc48Slice(0-1): OC-48 slices, there are total 2xOC-48 slices per OC-96 slice
           + $Pwid(0-2047): pseudowire channel for each OC-48 slice
Reg Desc   : 
This register is used to configure payload in each Pseudowire channels per slice
HDL_PATH  : ipwcore.ipwslcore.isl48core[$Oc96Slice*2 + $Oc48Slice].irtlpla.cfgpwctrl.membuf.ram.ram[$Pwid]

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_pld_ctrl_Base                                                                      0x01000
#define cAf6Reg_pla_pld_ctrl(Oc96Slice, Oc48Slice, Pwid)              (0x01000+(Oc96Slice)*32768+(Oc48Slice)*8192+(Pwid))

/*--------------------------------------
BitField Name: PlaLkPWIDCtrl
BitField Type: RW
BitField Desc: Lookup to a output psedowire
BitField Bits: [30:18]
--------------------------------------*/
#define cAf6_pla_pld_ctrl_PlaLkPWIDCtrl_Mask                                                         cBit30_18
#define cAf6_pla_pld_ctrl_PlaLkPWIDCtrl_Shift                                                               18

/*--------------------------------------
BitField Name: PlaLoPldTSSrcCtrl
BitField Type: RW
BitField Desc: Timestamp Source 0: PRC global 1: TS from CDR engine
BitField Bits: [17]
--------------------------------------*/
#define cAf6_pla_pld_ctrl_PlaLoPldTSSrcCtrl_Mask                                                        cBit17
#define cAf6_pla_pld_ctrl_PlaLoPldTSSrcCtrl_Shift                                                           17

/*--------------------------------------
BitField Name: PlaLoPldTypeCtrl
BitField Type: RW
BitField Desc: Payload Type 0: satop 1: ces without cas 2: cep
BitField Bits: [16:15]
--------------------------------------*/
#define cAf6_pla_pld_ctrl_PlaLoPldTypeCtrl_Mask                                                      cBit16_15
#define cAf6_pla_pld_ctrl_PlaLoPldTypeCtrl_Shift                                                            15

/*--------------------------------------
BitField Name: PlaLoPldSizeCtrl
BitField Type: RW
BitField Desc: Payload Size satop/cep mode: bit[13:0] payload size (ex: value as
0x100 is payload size 256 bytes) ces mode: bit[5:0] number of DS0 timeslot
bit[14:6] number of NxDS0 frame
BitField Bits: [14:0]
--------------------------------------*/
#define cAf6_pla_pld_ctrl_PlaLoPldSizeCtrl_Mask                                                       cBit14_0
#define cAf6_pla_pld_ctrl_PlaLoPldSizeCtrl_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Add/Remove Pseudowire Protocol Control
Reg Addr   : 0x0_1800-0x0_BFFF
Reg Formula: 0x0_1800 + $Oc96Slice*32768 + $Oc48Slice*8192 + $Pwid
    Where  : 
           + $Oc96Slice(0-1): OC-96 slices, there are total 2xOC-96 slices for 10G
           + $Oc48Slice(0-1): OC-48 slices, there are total 2xOC-48 slices per OC-96 slice
           + $Pwid(0-2047): pseudowire channel for each OC-48 slice
Reg Desc   : 
This register is used to add/remove pseudowire to prevent burst packets to other packet processing engines in each OC-48 slice

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_add_rmv_pw_ctrl_Base                                                               0x01800
#define cAf6Reg_pla_add_rmv_pw_ctrl(Oc96Slice, Oc48Slice, Pwid)       (0x01800+(Oc96Slice)*32768+(Oc48Slice)*8192+(Pwid))
#define cAf6Reg_pla_add_rmv_pw_ctrl_WidthVal                                                                32

/*--------------------------------------
BitField Name: PlaLoStaAddRmvCtrl
BitField Type: RW
BitField Desc: protocol state to add/remove pw Step1: Add intitial or pw idle,
the protocol state value is "zero" Step2: For adding the pw, CPU will Write "1"
value for the protocol state to start adding pw. The protocol state value is
"1". Step3: CPU enables pw at demap to finish the adding pw process. HW will
automatically change the protocol state value to "2" to run the pw, and keep
this state. Step4: For removing the pw, CPU will write "3" value for the
protocol state to start removing pw. The protocol state value is "3". Step5:
Poll the protocol state until return value "0" value, after that CPU disables pw
at demap to finish the removing pw process
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_pla_add_rmv_pw_ctrl_PlaLoStaAddRmvCtrl_Mask                                               cBit1_0
#define cAf6_pla_add_rmv_pw_ctrl_PlaLoStaAddRmvCtrl_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Low-Order Payload Control
Reg Addr   : 0x2_8000
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to configure payload in each Pseudowire channels per slice

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_192c_pld_ctrl                                                                      0x28000
#define cAf6Reg_pla_192c_pld_ctrl_WidthVal                                                                  32

/*--------------------------------------
BitField Name: Pla192PWIDCtrl
BitField Type: RW
BitField Desc: lookup pwid
BitField Bits: [29:16]
--------------------------------------*/
#define cAf6_pla_192c_pld_ctrl_Pla192PWIDCtrl_Mask                                                   cBit29_16
#define cAf6_pla_192c_pld_ctrl_Pla192PWIDCtrl_Shift                                                         16

/*--------------------------------------
BitField Name: Pla192PldCtrl
BitField Type: RW
BitField Desc: Payload Size
BitField Bits: [13:0]
--------------------------------------*/
#define cAf6_pla_192c_pld_ctrl_Pla192PldCtrl_Mask                                                     cBit13_0
#define cAf6_pla_192c_pld_ctrl_Pla192PldCtrl_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler OC192c Add/Remove Pseudowire Protocol Control
Reg Addr   : 0x2_8001
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to add/remove pseudowire to prevent burst packets to other packet processing engines in each OC-192 slice

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_oc192c_add_rmv_pw_ctrl                                                             0x28001
#define cAf6Reg_pla_oc192c_add_rmv_pw_ctrl_WidthVal                                                         32

/*--------------------------------------
BitField Name: PlaLoStaAddRmvCtrl
BitField Type: RW
BitField Desc: protocol state to add/remove pw Step1: Add intitial or pw idle,
the protocol state value is "zero" Step2: For adding the pw, CPU will Write "1"
value for the protocol state to start adding pw. The protocol state value is
"1". Step3: CPU enables pw at demap to finish the adding pw process. HW will
automatically change the protocol state value to "2" to run the pw, and keep
this state. Step4: For removing the pw, CPU will write "3" value for the
protocol state to start removing pw. The protocol state value is "3". Step5:
Poll the protocol state until return value "0" value, after that CPU disables pw
at demap to finish the removing pw process
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_pla_oc192c_add_rmv_pw_ctrl_PlaLoStaAddRmvCtrl_Mask                                        cBit1_0
#define cAf6_pla_oc192c_add_rmv_pw_ctrl_PlaLoStaAddRmvCtrl_Shift                                             0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Pseudowire Header Control
Reg Addr   : 0x2_0000-0x2_14FF
Reg Formula: 0x2_0000 + $pwid
    Where  : 
           + $pwid(0-5375): pseudowire channel
Reg Desc   : 
This register is used to config Control Word for the psedowire channels
HDL_PATH  : ipwcore.ipwcfg.membuf.ram.ram[$pwid]

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_pw_hdr_ctrl_Base                                                                   0x20000
#define cAf6Reg_pla_pw_hdr_ctrl(pwid)                                                         (0x20000+(pwid))
#define cAf6Reg_pla_pw_hdr_ctrl_WidthVal                                                                    32

/*--------------------------------------
BitField Name: PlaPwSuprCtrl
BitField Type: RW
BitField Desc: Suppresion Enable 1: enable 0: disable
BitField Bits: [8]
--------------------------------------*/
#define cAf6_pla_pw_hdr_ctrl_PlaPwSuprCtrl_Mask                                                          cBit8
#define cAf6_pla_pw_hdr_ctrl_PlaPwSuprCtrl_Shift                                                             8

/*--------------------------------------
BitField Name: PlaPwRbitDisCtrl
BitField Type: RW
BitField Desc: R bit disable 1: disable R bit in the Control Word (assign zero
in the packet) 0: normal
BitField Bits: [7]
--------------------------------------*/
#define cAf6_pla_pw_hdr_ctrl_PlaPwRbitDisCtrl_Mask                                                       cBit7
#define cAf6_pla_pw_hdr_ctrl_PlaPwRbitDisCtrl_Shift                                                          7

/*--------------------------------------
BitField Name: PlaPwMbitDisCtrl
BitField Type: RW
BitField Desc: M or NP bits disable 1: disable M or NP bits in the Control Word
(assign zero in the packet) 0: normal
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pla_pw_hdr_ctrl_PlaPwMbitDisCtrl_Mask                                                       cBit6
#define cAf6_pla_pw_hdr_ctrl_PlaPwMbitDisCtrl_Shift                                                          6

/*--------------------------------------
BitField Name: PlaPwLbitDisCtrl
BitField Type: RW
BitField Desc: L bit disable 1: disable L bit in the Control Word (assign zero
in the packet) 0: normal
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pla_pw_hdr_ctrl_PlaPwLbitDisCtrl_Mask                                                       cBit5
#define cAf6_pla_pw_hdr_ctrl_PlaPwLbitDisCtrl_Shift                                                          5

/*--------------------------------------
BitField Name: PlaPwRbitCPUCtrl
BitField Type: RW
BitField Desc: R bit value from CPU (low priority than PlaPwRbitDisCtrl)
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pla_pw_hdr_ctrl_PlaPwRbitCPUCtrl_Mask                                                       cBit4
#define cAf6_pla_pw_hdr_ctrl_PlaPwRbitCPUCtrl_Shift                                                          4

/*--------------------------------------
BitField Name: PlaPwMbitCPUCtrl
BitField Type: RW
BitField Desc: M or NP bits value from CPU (low priority than
PlaLoPwMbitDisCtrl)
BitField Bits: [3:2]
--------------------------------------*/
#define cAf6_pla_pw_hdr_ctrl_PlaPwMbitCPUCtrl_Mask                                                     cBit3_2
#define cAf6_pla_pw_hdr_ctrl_PlaPwMbitCPUCtrl_Shift                                                          2

/*--------------------------------------
BitField Name: PlaPwLbitCPUCtrl
BitField Type: RW
BitField Desc: L bit value from CPU (low priority than PlaLoPwLbitDisCtrl)
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pla_pw_hdr_ctrl_PlaPwLbitCPUCtrl_Mask                                                       cBit1
#define cAf6_pla_pw_hdr_ctrl_PlaPwLbitCPUCtrl_Shift                                                          1

/*--------------------------------------
BitField Name: PlaPwEnCtrl
BitField Type: RW
BitField Desc: Pseudowire enable 1: enable 0: disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pla_pw_hdr_ctrl_PlaPwEnCtrl_Mask                                                            cBit0
#define cAf6_pla_pw_hdr_ctrl_PlaPwEnCtrl_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Block Buffer Control
Reg Addr   : 0x7_8000-0x7_9607
Reg Formula: 0x7_8000 + $cid
    Where  : 
           + $cid(0-8328): channel id address (0-5375): 5376xPsedowire ID (5376-5503): 128xiMSG link for HO iMSG (5304-5636): 336xiMSG link for LO iMSG}
Reg Desc   : 
This register is used to configure BlockID for the buffer for all services (Psedowire/iMSG/VCAT)
HDL_PATH:     IF($cid<4096)
iblkcfg_ram.iram_tdp_low.ram_name[$cid]
ELSE
iblkcfg_ram.iram_tdp_high.ram_name[$cid-4096]

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_blk_buf_ctrl_Base                                                                  0x78000
#define cAf6Reg_pla_blk_buf_ctrl(cid)                                                          (0x78000+(cid))
#define cAf6Reg_pla_blk_buf_ctrl_WidthVal                                                                   32

/*--------------------------------------
BitField Name: PlaBlkIDCtr
BitField Type: RW
BitField Desc: Block Buffer Number There are 5376 blocks for LO rate (BlkType =
0), a block location for eache LO rate channel. There are 256 block location for
HO rate (BlkType != 0), a block location for eache DS3/E3/STS1 rate channel,
block from 192 to 255 for TOH PW/DCC With the higer bandwidth rate channels,
need to configure start block of a rate channels, next blocks are increase one-
by-one and roll-over when meet max number block, max number block is depend on
BlockType (STS3c = 3,STS12c = 12,STS24c = 24,STS48c = 48)
BitField Bits: [15:3]
--------------------------------------*/
#define cAf6_pla_blk_buf_ctrl_PlaBlkIDCtr_Mask                                                        cBit15_3
#define cAf6_pla_blk_buf_ctrl_PlaBlkIDCtr_Shift                                                              3

/*--------------------------------------
BitField Name: PlaBlkTypeCtr
BitField Type: RW
BitField Desc: Type of TDM rate used this block 0: DS0/DS1/E1/VC1x 1:
DS3/E3/STS1 2: STS3c 3: STS12c 4: STS24c 5: STS48c
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_pla_blk_buf_ctrl_PlaBlkTypeCtr_Mask                                                       cBit2_0
#define cAf6_pla_blk_buf_ctrl_PlaBlkTypeCtr_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Lookup Flow Control
Reg Addr   : 0x6_8000-0x6_967F
Reg Formula: 0x6_8000 + $cid
    Where  : 
           + $cid (0-5759): channel id channel (0-5375): Psedowire ID (5376-5631): 256xVCG LO ID (5632-5759): 128xVCG HO ID}
Reg Desc   : 
This register is used to lookup from (PW/VCG) to flow ID
HDL_PATH  : iflowcfg.membuf.ram.ram[$cid]

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_lk_flow_ctrl_Base                                                                  0x68000
#define cAf6Reg_pla_lk_flow_ctrl(cid)                                                          (0x68000+(cid))
#define cAf6Reg_pla_lk_flow_ctrl_WidthVal                                                                   32

/*--------------------------------------
BitField Name: PlaOutLkFlowCtrl
BitField Type: RW
BitField Desc: Lookup FlowID
BitField Bits: [12:0]
--------------------------------------*/
#define cAf6_pla_lk_flow_ctrl_PlaOutLkFlowCtrl_Mask                                                   cBit12_0
#define cAf6_pla_lk_flow_ctrl_PlaOutLkFlowCtrl_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Flow PSN Length Control
Reg Addr   : 0x4_8000-0x4_94FF
Reg Formula: 0x4_8000 + $pwid
    Where  : 
           + $pwid(0-5375): pseudowire channel
Reg Desc   : 
This register is used to config flow PSN length
HDL_PATH  : ideque.iflowcfg.membuf.ram.ram[$pwid]

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_pw_psn_ctrl_Base                                                                   0x48000
#define cAf6Reg_pla_pw_psn_ctrl(pwid)                                                         (0x48000+(pwid))
#define cAf6Reg_pla_pw_psn_ctrl_WidthVal                                                                    32

/*--------------------------------------
BitField Name: PlaOutPSNLenCtrl
BitField Type: RW
BitField Desc: PSN length
BitField Bits: [6:0]
--------------------------------------*/
#define cAf6_pla_pw_psn_ctrl_PlaOutPSNLenCtrl_Mask                                                     cBit6_0
#define cAf6_pla_pw_psn_ctrl_PlaOutPSNLenCtrl_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Psedowire Protection Control
Reg Addr   : 0x5_0000-0x5_14FF
Reg Formula: 0x5_0000 + $pwid
    Where  : 
           + $pwid(0-5375): flow channel
Reg Desc   : 
This register is used to config UPSR/HSPW for protect psedowire
HDL_PATH  : ideque.iapslkcfg.membuf.ram.ram[$pwid]

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_pw_pro_ctrl_Base                                                                   0x50000
#define cAf6Reg_pla_pw_pro_ctrl(pwid)                                                         (0x50000+(pwid))
#define cAf6Reg_pla_pw_pro_ctrl_WidthVal                                                                    32

/*--------------------------------------
BitField Name: PlaOutUpsrGrpCtr
BitField Type: RW
BitField Desc: UPRS group
BitField Bits: [27:15]
--------------------------------------*/
#define cAf6_pla_pw_pro_ctrl_PlaOutUpsrGrpCtr_Mask                                                   cBit27_15
#define cAf6_pla_pw_pro_ctrl_PlaOutUpsrGrpCtr_Shift                                                         15

/*--------------------------------------
BitField Name: PlaOutHspwGrpCtr
BitField Type: RW
BitField Desc: HSPW group
BitField Bits: [14:2]
--------------------------------------*/
#define cAf6_pla_pw_pro_ctrl_PlaOutHspwGrpCtr_Mask                                                    cBit14_2
#define cAf6_pla_pw_pro_ctrl_PlaOutHspwGrpCtr_Shift                                                          2

/*--------------------------------------
BitField Name: PlaFlowUPSRUsedCtrl
BitField Type: RW
BitField Desc: Flow UPSR is used or not 0:not used, all configurations for UPSR
will be not effected 1:used
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pla_pw_pro_ctrl_PlaFlowUPSRUsedCtrl_Mask                                                    cBit1
#define cAf6_pla_pw_pro_ctrl_PlaFlowUPSRUsedCtrl_Shift                                                       1

/*--------------------------------------
BitField Name: PlaFlowHSPWUsedCtrl
BitField Type: RW
BitField Desc: Flow HSPW is used or not 0:not used, all configurations for HSPW
will be not effected 1:used
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pla_pw_pro_ctrl_PlaFlowHSPWUsedCtrl_Mask                                                    cBit0
#define cAf6_pla_pw_pro_ctrl_PlaFlowHSPWUsedCtrl_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Output UPSR Control
Reg Addr   : 0x5_8000-0x5_829F
Reg Formula: 0x5_8000 + $upsrgrp
    Where  : 
           + $upsrgrp(0-671): UPSR group address
Reg Desc   : 
This register is used to config UPSR group
HDL_PATH  : ideque.iupsrcfg.membuf.ram.ram[$upsrgrp]

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_out_upsr_ctrl_Base                                                                 0x58000
#define cAf6Reg_pla_out_upsr_ctrl(upsrgrp)                                                 (0x58000+(upsrgrp))
#define cAf6Reg_pla_out_upsr_ctrl_WidthVal                                                                  32

/*--------------------------------------
BitField Name: PlaOutUpsrEnCtr
BitField Type: RW
BitField Desc: Total is 5376 upsrID, divide into 672 group address for
configuration, 8-bit is correlative with 8 upsrID each group. Bit#0 is for
upsrID#0, bit#1 is for upsrID#1... 1: enable 0: disable
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_pla_out_upsr_ctrl_PlaOutUpsrEnCtr_Mask                                                    cBit7_0
#define cAf6_pla_out_upsr_ctrl_PlaOutUpsrEnCtr_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Output HSPW Control
Reg Addr   : 0x6_0000-0x6_029F
Reg Formula: 0x6_0000 + $hspwgrp
    Where  : 
           + $hspwgrp(0-671): HSPW group address
Reg Desc   : 
This register is used to config HSPW group
HDL_PATH  : ideque.ihspwcfg.membuf.ram.ram[$hspwgrp]

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_out_hspw_ctrl_Base                                                                 0x60000
#define cAf6Reg_pla_out_hspw_ctrl(hspwgrp)                                                 (0x60000+(hspwgrp))
#define cAf6Reg_pla_out_hspw_ctrl_WidthVal                                                                  32

/*--------------------------------------
BitField Name: PlaOutHSPWCtr
BitField Type: RW
BitField Desc: Total is 5376 hspwID, divide into 672 group address for
configuration, 8-bit is correlative with 8 hspwID each group. Bit#0 is for
hspwID#0, bit#1 is for hspwID#1...
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_pla_out_hspw_ctrl_PlaOutHSPWCtr_Mask                                                      cBit7_0
#define cAf6_pla_out_hspw_ctrl_PlaOutHSPWCtr_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Output PSN Process Control
Reg Addr   : 0x4_0000
Reg Formula: 0x4_0000
    Where  : 
Reg Desc   : 
This register is used to control PSN configuration

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_out_psnpro_ctrl                                                                    0x40000

/*--------------------------------------
BitField Name: PlaOutPsnProReqCtr
BitField Type: RW
BitField Desc: Request to process PSN 1: request to write/read PSN header
buffer. The CPU need to prepare a complete PSN header into PSN buffer before
request write OR read out a complete PSN header in PSN buffer after request read
done 0: HW will automatically set to 0 when a request done
BitField Bits: [31]
--------------------------------------*/
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProReqCtr_Mask                                                cBit31
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProReqCtr_Shift                                                   31

/*--------------------------------------
BitField Name: PlaOutPsnProRnWCtr
BitField Type: RW
BitField Desc: read or write PSN 1: read PSN 0: write PSN
BitField Bits: [30]
--------------------------------------*/
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProRnWCtr_Mask                                                cBit30
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProRnWCtr_Shift                                                   30

/*--------------------------------------
BitField Name: PlaOutPsnProLenCtr
BitField Type: RW
BitField Desc: Length of a complete PSN need to read/write
BitField Bits: [22:16]
--------------------------------------*/
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProLenCtr_Mask                                             cBit22_16
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProLenCtr_Shift                                                   16

/*--------------------------------------
BitField Name: PlaOutPsnProPageCtr
BitField Type: RW
BitField Desc: there is 2 pages PSN location per PWID, depend on the
configuration of "PlaOutHspwPsnCtr", the engine will choice which the page to
encapsulate into the packet.
BitField Bits: [15]
--------------------------------------*/
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProPageCtr_Mask                                               cBit15
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProPageCtr_Shift                                                  15

/*--------------------------------------
BitField Name: PlaOutPsnProFlowCtr
BitField Type: RW
BitField Desc: Flow channel
BitField Bits: [12:0]
--------------------------------------*/
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProFlowCtr_Mask                                             cBit12_0
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProFlowCtr_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Output PSN Buffer Control
Reg Addr   : 0x4_0010-0x4_0015
Reg Formula: 0x4_0010 + $segid
    Where  : 
           + $segid(0-5): segment 16-byte PSN, total is 6 segments for maximum 96 bytes PSN
Reg Desc   : 
This register is used to store PSN data which is before CPU request write or after CPU request read %%
The header format from entry#0 to entry#4 is as follow: %%
{DA(6-byte),SA(6-byte),VLAN1(4-byte,optional),VLAN2(4-byte, optional),EthType(2-byte),PSN Header(4 to 96 bytes)} %%
Depends on specific PSN selected for each pseudowire, the EthType and PSN Header field may have the following values and formats: %%

PSN header is MEF-8: %%
EthType:  0x88D8 %%
4-byte PSN Header with format: {ECID[19:0], 0x102} where ECID is pseodowire identification for  remote  receive side. %%
PSN header is MPLS:  %%
EthType:  0x8847 %%
4/8/12-byte PSN Header with format: {OuterLabel2[31:0](optional), OuterLabel1[31:0](optional),  InnerLabel[31:0]}   %%
Where InnerLabel[31:12] is pseodowire identification for remote receive side. %%
Label format: {Idenfifier[19:0],Exp[2:0],StackBit,TTL[7:0]} where StackBit =  for InnerLabel %%
PSN header is UDP/Ipv4:  %%
EthType:  0x0800 %%
28-byte PSN Header with format {Ipv4 Header(20 bytes), UDP Header(8 byte)} as below: %%
{IP_Ver[3:0], IP_IHL[3:0], IP_ToS[7:0], IP_Header_Sum[31:16]} %%
{IP_Iden[15:0], IP_Flag[2:0], IP_Frag_Offset[12:0]}  %%
{IP_TTL[7:0], IP_Protocol[7:0], IP_Header_Sum[15:0]} %%
{IP_SrcAdr[31:0]} %%
{IP_DesAdr[31:0]} %%
{UDP_SrcPort[15:0], UDP_DesPort[15:0]} %%
{UDP_Sum[31:0] (optional)} %%
Case: %%
IP_Protocol[7:0]: 0x11 to signify UDP  %%
IP_Protocol[7:0]: 0x11 to signify UDP  %%
IP_Header_Sum[31:0]:  CPU calculate these fields as a temporarily for HW before plus rest fields %%
{IP_Ver[3:0], IP_IHL[3:0], IP_ToS[7:0]} +%%
IP_Iden[15:0] + {IP_Flag[2:0], IP_Frag_Offset[12:0]}  +%%
{IP_TTL[7:0], IP_Protocol[7:0]} + %%
IP_SrcAdr[31:16] + IP_SrcAdr[15:0] +%%
IP_DesAdr[31:16] + IP_DesAdr[15:0]%%
UDP_SrcPort: used as remote pseudowire identification or 0x85E if unused%%
UDP_DesPort : used as remote pseudowire identification or 0x85E if unused%%
UDP_Sum[31:0] (optional): CPU calculate these fields as a temporarily for HW before plus rest fields%%
IP_SrcAdr[127:112] +    IP_SrcAdr[111:96] +     %%
IP_SrcAdr[95:80]  + IP_SrcAdr[79:64] +  %%
IP_SrcAdr[63:48]  + IP_SrcAdr[47:32] +  %%
IP_SrcAdr[31:16]  + IP_SrcAdr[15:0] +   %%
IP_DesAdr[127:112] +    IP_DesAdr[111:96] +     %%
IP_DesAdr[95:80]  + IP_DesAdr[79:64] +  %%
IP_DesAdr[63:48]  + IP_DesAdr[47:32] +  %%
IP_DesAdr[31:16]  + IP_DesAdr[15:0] +%%
{8-bit zeros, IP_Protocol[7:0]} +%%
UDP_SrcPort[15:0] +  UDP_DesPort[15:0]%%
PSN header is UDP/Ipv6:  %%
EthType:  0x86DD%%
48-byte PSN Header with format {Ipv6 Header(40 bytes), UDP Header(8 byte)} as below:%%
{IP_Ver[3:0], IP_Traffic_Class[7:0], IP_Flow_Label[19:0]}%%
{16-bit zeros, IP_Next_Header[7:0], IP_Hop_Limit[7:0]} %%
{IP_SrcAdr[127:96]}%%
{IP_SrcAdr[95:64]}%%
{IP_SrcAdr[63:32]}%%
{IP_SrcAdr[31:0]}%%
{IP_DesAdr[127:96]}%%
{IP_DesAdr[95:64]}%%
{IP_DesAdr[63:32]}%%
{IP_DesAdr[31:0]}%%
{UDP_SrcPort[15:0], UDP_DesPort[15:0]}%%
{UDP_Sum[31:0]}%%
Case:%%
IP_Next_Header[7:0]: 0x11 to signify UDP %%
UDP_Sum[31:0]:  CPU calculate these fields as a temporarily for HW before plus rest fields%%
IP_SrcAdr[127:112] +    IP_SrcAdr[111:96] +     %%
IP_SrcAdr[95:80]  + IP_SrcAdr[79:64] +  %%
IP_SrcAdr[63:48]  + IP_SrcAdr[47:32] +  %%
IP_SrcAdr[31:16]  + IP_SrcAdr[15:0] +   %%
IP_DesAdr[127:112] +    IP_DesAdr[111:96] +     %%
IP_DesAdr[95:80]  + IP_DesAdr[79:64] +  %%
IP_DesAdr[63:48]  + IP_DesAdr[47:32] +  %%
IP_DesAdr[31:16]  + IP_DesAdr[15:0] +%%
{8-bit zeros, IP_Next_Header[7:0]} +%%
UDP_SrcPort[15:0] +  UDP_DesPort[15:0]%%
UDP_SrcPort: used as remote pseudowire identification or 0x85E if unused%%
UDP_DesPort : used as remote pseudowire identification or 0x85E if unused%%
User can select either source or destination port for pseodowire identification. See IP/UDP standards for more description about other field. %%
PSN header is MPLS over Ipv4:%%
IPv4 header must have IP_Protocol[7:0] value 0x89 to signify MPLS%%
After IPv4 header is MPLS labels where inner label is used for PW identification%%
PSN header is MPLS over Ipv6:%%
IPv6 header must have IP_Next_Header[7:0] value 0x89 to signify MPLS%%
After IPv6 header is MPLS labels where inner label is used for PW identification%%
PSN header (all modes) with RTP enable: RTP used for TDM PW (define in RFC3550), is in the first 8-byte of this buffer (bit[127:64] of segid == 0), following is PSN header (start from bit[63:0] of segid = 0)%%
Case:%%
RTPSSRC[31:0] : This is the SSRC value of RTP header%%
RtpPtValue[6:0]: This is the PT value of RTP header, define in http://www.iana.org/assignments/rtp-parameters/rtp-parameters.xml

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_out_psnbuf_ctrl_Base                                                               0x40010
#define cAf6Reg_pla_out_psnbuf_ctrl(segid)                                                   (0x40010+(segid))

/*--------------------------------------
BitField Name: PlaOutPsnProReqCtr
BitField Type: RW
BitField Desc: PSN buffer
BitField Bits: [127:0]
--------------------------------------*/
#define cAf6_pla_out_psnbuf_ctrl_PlaOutPsnProReqCtr_Mask_01                                           cBit31_0
#define cAf6_pla_out_psnbuf_ctrl_PlaOutPsnProReqCtr_Shift_01                                                 0
#define cAf6_pla_out_psnbuf_ctrl_PlaOutPsnProReqCtr_Mask_02                                           cBit31_0
#define cAf6_pla_out_psnbuf_ctrl_PlaOutPsnProReqCtr_Shift_02                                                 0
#define cAf6_pla_out_psnbuf_ctrl_PlaOutPsnProReqCtr_Mask_03                                           cBit31_0
#define cAf6_pla_out_psnbuf_ctrl_PlaOutPsnProReqCtr_Shift_03                                                 0
#define cAf6_pla_out_psnbuf_ctrl_PlaOutPsnProReqCtr_Mask_04                                           cBit31_0
#define cAf6_pla_out_psnbuf_ctrl_PlaOutPsnProReqCtr_Shift_04                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Hold Register Control
Reg Addr   : 0x7_0000-0x7_0002
Reg Formula: 0x7_0000 + $holdreg
    Where  : 
           + $holdreg(0-2): hold register with (holdreg=0) for bit63_32, (holdreg=1) for bit95_64, (holdreg=2) for bit127_96
Reg Desc   : 
This register is used to control hold register.

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_hold_reg_ctrl_Base                                                                 0x70000
#define cAf6Reg_pla_hold_reg_ctrl(holdreg)                                                 (0x70000+(holdreg))

/*--------------------------------------
BitField Name: PlaHoldRegCtr
BitField Type: RW
BitField Desc: hold register value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_pla_hold_reg_ctrl_PlaHoldRegCtr_Mask                                                     cBit31_0
#define cAf6_pla_hold_reg_ctrl_PlaHoldRegCtr_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Force Parity Error Control
Reg Addr   : 0x42040
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to force parity error

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_force_par_err_control                                                              0x42040
#define cAf6Reg_pla_force_par_err_control_WidthVal                                                          32

/*--------------------------------------
BitField Name: ForceParErrOutUPSRCtrReg
BitField Type: RW
BitField Desc: Force Parity Error Ouput UPSR Control Register
BitField Bits: [20]
--------------------------------------*/
#define cAf6_pla_force_par_err_control_ForceParErrOutUPSRCtrReg_Mask                                    cBit20
#define cAf6_pla_force_par_err_control_ForceParErrOutUPSRCtrReg_Shift                                       20

/*--------------------------------------
BitField Name: ForceParErrPWProCtrReg
BitField Type: RW
BitField Desc: Force Parity Error PW Protection Control Register
BitField Bits: [18]
--------------------------------------*/
#define cAf6_pla_force_par_err_control_ForceParErrPWProCtrReg_Mask                                      cBit18
#define cAf6_pla_force_par_err_control_ForceParErrPWProCtrReg_Shift                                         18

/*--------------------------------------
BitField Name: ForceParErrPWPSNCtrReg
BitField Type: RW
BitField Desc: Force Parity Error PW PSNr Control Register
BitField Bits: [17]
--------------------------------------*/
#define cAf6_pla_force_par_err_control_ForceParErrPWPSNCtrReg_Mask                                      cBit17
#define cAf6_pla_force_par_err_control_ForceParErrPWPSNCtrReg_Shift                                         17

/*--------------------------------------
BitField Name: ForceParErrPWHdrCtrReg
BitField Type: RW
BitField Desc: Force Parity Error PW Header Control Register
BitField Bits: [16]
--------------------------------------*/
#define cAf6_pla_force_par_err_control_ForceParErrPWHdrCtrReg_Mask                                      cBit16
#define cAf6_pla_force_par_err_control_ForceParErrPWHdrCtrReg_Shift                                         16

/*--------------------------------------
BitField Name: ForceParErrLkPWCtrRegOC96_3
BitField Type: RW
BitField Desc: Force Parity Error Lookup PW  Control Register OC96#3
BitField Bits: [14]
--------------------------------------*/
#define cAf6_pla_force_par_err_control_ForceParErrLkPWCtrRegOC96_3_Mask                                  cBit14
#define cAf6_pla_force_par_err_control_ForceParErrLkPWCtrRegOC96_3_Shift                                      14

/*--------------------------------------
BitField Name: ForceParErrPldCtrRegOC96_3_OC48_1
BitField Type: RW
BitField Desc: Force Parity Error Payload Control Register OC48#1 of OC96#3
BitField Bits: [13]
--------------------------------------*/
#define cAf6_pla_force_par_err_control_ForceParErrPldCtrRegOC96_3_OC48_1_Mask                                  cBit13
#define cAf6_pla_force_par_err_control_ForceParErrPldCtrRegOC96_3_OC48_1_Shift                                      13

/*--------------------------------------
BitField Name: ForceParErrPldCtrRegOC96_3_OC48_0
BitField Type: RW
BitField Desc: Force Parity Error Payload Control Register OC48#0 of OC96#3
BitField Bits: [12]
--------------------------------------*/
#define cAf6_pla_force_par_err_control_ForceParErrPldCtrRegOC96_3_OC48_0_Mask                                  cBit12
#define cAf6_pla_force_par_err_control_ForceParErrPldCtrRegOC96_3_OC48_0_Shift                                      12

/*--------------------------------------
BitField Name: ForceParErrLkPWCtrRegOC96_2
BitField Type: RW
BitField Desc: Force Parity Error Lookup PW  Control Register OC96#2
BitField Bits: [10]
--------------------------------------*/
#define cAf6_pla_force_par_err_control_ForceParErrLkPWCtrRegOC96_2_Mask                                  cBit10
#define cAf6_pla_force_par_err_control_ForceParErrLkPWCtrRegOC96_2_Shift                                      10

/*--------------------------------------
BitField Name: ForceParErrPldCtrRegOC96_2_OC48_1
BitField Type: RW
BitField Desc: Force Parity Error Payload Control Register OC48#1 of OC96#2
BitField Bits: [9]
--------------------------------------*/
#define cAf6_pla_force_par_err_control_ForceParErrPldCtrRegOC96_2_OC48_1_Mask                                   cBit9
#define cAf6_pla_force_par_err_control_ForceParErrPldCtrRegOC96_2_OC48_1_Shift                                       9

/*--------------------------------------
BitField Name: ForceParErrPldCtrRegOC96_2_OC48_0
BitField Type: RW
BitField Desc: Force Parity Error Payload Control Register OC48#0 of OC96#2
BitField Bits: [8]
--------------------------------------*/
#define cAf6_pla_force_par_err_control_ForceParErrPldCtrRegOC96_2_OC48_0_Mask                                   cBit8
#define cAf6_pla_force_par_err_control_ForceParErrPldCtrRegOC96_2_OC48_0_Shift                                       8

/*--------------------------------------
BitField Name: ForceParErrLkPWCtrRegOC96_1
BitField Type: RW
BitField Desc: Force Parity Error Lookup PW  Control Register OC96#1
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pla_force_par_err_control_ForceParErrLkPWCtrRegOC96_1_Mask                                   cBit6
#define cAf6_pla_force_par_err_control_ForceParErrLkPWCtrRegOC96_1_Shift                                       6

/*--------------------------------------
BitField Name: ForceParErrPldCtrRegOC96_1_OC48_1
BitField Type: RW
BitField Desc: Force Parity Error Payload Control Register OC48#1 of OC96#1
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pla_force_par_err_control_ForceParErrPldCtrRegOC96_1_OC48_1_Mask                                   cBit5
#define cAf6_pla_force_par_err_control_ForceParErrPldCtrRegOC96_1_OC48_1_Shift                                       5

/*--------------------------------------
BitField Name: ForceParErrPldCtrRegOC96_1_OC48_0
BitField Type: RW
BitField Desc: Force Parity Error Payload Control Register OC48#0 of OC96#1
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pla_force_par_err_control_ForceParErrPldCtrRegOC96_1_OC48_0_Mask                                   cBit4
#define cAf6_pla_force_par_err_control_ForceParErrPldCtrRegOC96_1_OC48_0_Shift                                       4

/*--------------------------------------
BitField Name: ForceParErrLkPWCtrRegOC96_0
BitField Type: RW
BitField Desc: Force Parity Error Lookup PW  Control Register OC96#0
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pla_force_par_err_control_ForceParErrLkPWCtrRegOC96_0_Mask                                   cBit2
#define cAf6_pla_force_par_err_control_ForceParErrLkPWCtrRegOC96_0_Shift                                       2

/*--------------------------------------
BitField Name: ForceParErrPldCtrRegOC96_0_OC48_1
BitField Type: RW
BitField Desc: Force Parity Error Payload Control Register OC48#1 of OC96#0
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pla_force_par_err_control_ForceParErrPldCtrRegOC96_0_OC48_1_Mask                                   cBit1
#define cAf6_pla_force_par_err_control_ForceParErrPldCtrRegOC96_0_OC48_1_Shift                                       1

/*--------------------------------------
BitField Name: ForceParErrPldCtrRegOC96_0_OC48_0
BitField Type: RW
BitField Desc: Force Parity Error Payload Control Register OC48#0 of OC96#0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pla_force_par_err_control_ForceParErrPldCtrRegOC96_0_OC48_0_Mask                                   cBit0
#define cAf6_pla_force_par_err_control_ForceParErrPldCtrRegOC96_0_OC48_0_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Disable Parity Control
Reg Addr   : 0x42041
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to disable parity check

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_dis_par_control                                                                    0x42041
#define cAf6Reg_pla_dis_par_control_WidthVal                                                                32

/*--------------------------------------
BitField Name: DisParErrOutUPSRCtrReg
BitField Type: RW
BitField Desc: Dis Parity Error Ouput UPSR Control Register
BitField Bits: [20]
--------------------------------------*/
#define cAf6_pla_dis_par_control_DisParErrOutUPSRCtrReg_Mask                                            cBit20
#define cAf6_pla_dis_par_control_DisParErrOutUPSRCtrReg_Shift                                               20

/*--------------------------------------
BitField Name: DisParErrPWProCtrReg
BitField Type: RW
BitField Desc: Dis Parity Error PW Protection Control Register
BitField Bits: [18]
--------------------------------------*/
#define cAf6_pla_dis_par_control_DisParErrPWProCtrReg_Mask                                              cBit18
#define cAf6_pla_dis_par_control_DisParErrPWProCtrReg_Shift                                                 18

/*--------------------------------------
BitField Name: DisParErrPWPSNCtrReg
BitField Type: RW
BitField Desc: Dis Parity Error PW PSNr Control Register
BitField Bits: [17]
--------------------------------------*/
#define cAf6_pla_dis_par_control_DisParErrPWPSNCtrReg_Mask                                              cBit17
#define cAf6_pla_dis_par_control_DisParErrPWPSNCtrReg_Shift                                                 17

/*--------------------------------------
BitField Name: DisParErrPWHdrCtrReg
BitField Type: RW
BitField Desc: Dis Parity Error PW Header Control Register
BitField Bits: [16]
--------------------------------------*/
#define cAf6_pla_dis_par_control_DisParErrPWHdrCtrReg_Mask                                              cBit16
#define cAf6_pla_dis_par_control_DisParErrPWHdrCtrReg_Shift                                                 16

/*--------------------------------------
BitField Name: DisParErrLkPWCtrRegOC96_3
BitField Type: RW
BitField Desc: Dis Parity Error Lookup PW  Control Register OC96#3
BitField Bits: [14]
--------------------------------------*/
#define cAf6_pla_dis_par_control_DisParErrLkPWCtrRegOC96_3_Mask                                         cBit14
#define cAf6_pla_dis_par_control_DisParErrLkPWCtrRegOC96_3_Shift                                            14

/*--------------------------------------
BitField Name: DisParErrPldCtrRegOC96_3_OC48_1
BitField Type: RW
BitField Desc: Dis Parity Error Payload Control Register OC48#1 of OC96#3
BitField Bits: [13]
--------------------------------------*/
#define cAf6_pla_dis_par_control_DisParErrPldCtrRegOC96_3_OC48_1_Mask                                   cBit13
#define cAf6_pla_dis_par_control_DisParErrPldCtrRegOC96_3_OC48_1_Shift                                      13

/*--------------------------------------
BitField Name: DisParErrPldCtrRegOC96_3_OC48_0
BitField Type: RW
BitField Desc: Dis Parity Error Payload Control Register OC48#0 of OC96#3
BitField Bits: [12]
--------------------------------------*/
#define cAf6_pla_dis_par_control_DisParErrPldCtrRegOC96_3_OC48_0_Mask                                   cBit12
#define cAf6_pla_dis_par_control_DisParErrPldCtrRegOC96_3_OC48_0_Shift                                      12

/*--------------------------------------
BitField Name: DisParErrLkPWCtrRegOC96_2
BitField Type: RW
BitField Desc: Dis Parity Error Lookup PW  Control Register OC96#2
BitField Bits: [10]
--------------------------------------*/
#define cAf6_pla_dis_par_control_DisParErrLkPWCtrRegOC96_2_Mask                                         cBit10
#define cAf6_pla_dis_par_control_DisParErrLkPWCtrRegOC96_2_Shift                                            10

/*--------------------------------------
BitField Name: DisParErrPldCtrRegOC96_2_OC48_1
BitField Type: RW
BitField Desc: Dis Parity Error Payload Control Register OC48#1 of OC96#2
BitField Bits: [9]
--------------------------------------*/
#define cAf6_pla_dis_par_control_DisParErrPldCtrRegOC96_2_OC48_1_Mask                                    cBit9
#define cAf6_pla_dis_par_control_DisParErrPldCtrRegOC96_2_OC48_1_Shift                                       9

/*--------------------------------------
BitField Name: DisParErrPldCtrRegOC96_2_OC48_0
BitField Type: RW
BitField Desc: Dis Parity Error Payload Control Register OC48#0 of OC96#2
BitField Bits: [8]
--------------------------------------*/
#define cAf6_pla_dis_par_control_DisParErrPldCtrRegOC96_2_OC48_0_Mask                                    cBit8
#define cAf6_pla_dis_par_control_DisParErrPldCtrRegOC96_2_OC48_0_Shift                                       8

/*--------------------------------------
BitField Name: DisParErrLkPWCtrRegOC96_1
BitField Type: RW
BitField Desc: Dis Parity Error Lookup PW  Control Register OC96#1
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pla_dis_par_control_DisParErrLkPWCtrRegOC96_1_Mask                                          cBit6
#define cAf6_pla_dis_par_control_DisParErrLkPWCtrRegOC96_1_Shift                                             6

/*--------------------------------------
BitField Name: DisParErrPldCtrRegOC96_1_OC48_1
BitField Type: RW
BitField Desc: Dis Parity Error Payload Control Register OC48#1 of OC96#1
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pla_dis_par_control_DisParErrPldCtrRegOC96_1_OC48_1_Mask                                    cBit5
#define cAf6_pla_dis_par_control_DisParErrPldCtrRegOC96_1_OC48_1_Shift                                       5

/*--------------------------------------
BitField Name: DisParErrPldCtrRegOC96_1_OC48_0
BitField Type: RW
BitField Desc: Dis Parity Error Payload Control Register OC48#0 of OC96#1
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pla_dis_par_control_DisParErrPldCtrRegOC96_1_OC48_0_Mask                                    cBit4
#define cAf6_pla_dis_par_control_DisParErrPldCtrRegOC96_1_OC48_0_Shift                                       4

/*--------------------------------------
BitField Name: DisParErrLkPWCtrRegOC96_0
BitField Type: RW
BitField Desc: Dis Parity Error Lookup PW  Control Register OC96#0
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pla_dis_par_control_DisParErrLkPWCtrRegOC96_0_Mask                                          cBit2
#define cAf6_pla_dis_par_control_DisParErrLkPWCtrRegOC96_0_Shift                                             2

/*--------------------------------------
BitField Name: DisParErrPldCtrRegOC96_0_OC48_1
BitField Type: RW
BitField Desc: Dis Parity Error Payload Control Register OC48#1 of OC96#0
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pla_dis_par_control_DisParErrPldCtrRegOC96_0_OC48_1_Mask                                    cBit1
#define cAf6_pla_dis_par_control_DisParErrPldCtrRegOC96_0_OC48_1_Shift                                       1

/*--------------------------------------
BitField Name: DisParErrPldCtrRegOC96_0_OC48_0
BitField Type: RW
BitField Desc: Dis Parity Error Payload Control Register OC48#0 of OC96#0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pla_dis_par_control_DisParErrPldCtrRegOC96_0_OC48_0_Mask                                    cBit0
#define cAf6_pla_dis_par_control_DisParErrPldCtrRegOC96_0_OC48_0_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Parity Error Sticky
Reg Addr   : 0x42042
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to sticky parity check

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_par_err_stk                                                                        0x42042
#define cAf6Reg_pla_par_err_stk_WidthVal                                                                    32

/*--------------------------------------
BitField Name: ParErrOutUPSRCtrStk
BitField Type: W1C
BitField Desc: Dis Parity Error Ouput UPSR Control Register
BitField Bits: [20]
--------------------------------------*/
#define cAf6_pla_par_err_stk_ParErrOutUPSRCtrStk_Mask                                                   cBit20
#define cAf6_pla_par_err_stk_ParErrOutUPSRCtrStk_Shift                                                      20

/*--------------------------------------
BitField Name: ParErrPWProCtrStk
BitField Type: W1C
BitField Desc: Dis Parity Error PW Protection Control Register
BitField Bits: [18]
--------------------------------------*/
#define cAf6_pla_par_err_stk_ParErrPWProCtrStk_Mask                                                     cBit18
#define cAf6_pla_par_err_stk_ParErrPWProCtrStk_Shift                                                        18

/*--------------------------------------
BitField Name: ParErrPWPSNCtrStk
BitField Type: W1C
BitField Desc: Dis Parity Error PW PSNr Control Register
BitField Bits: [17]
--------------------------------------*/
#define cAf6_pla_par_err_stk_ParErrPWPSNCtrStk_Mask                                                     cBit17
#define cAf6_pla_par_err_stk_ParErrPWPSNCtrStk_Shift                                                        17

/*--------------------------------------
BitField Name: ParErrPWHdrCtrStk
BitField Type: W1C
BitField Desc: Dis Parity Error PW Header Control Register
BitField Bits: [16]
--------------------------------------*/
#define cAf6_pla_par_err_stk_ParErrPWHdrCtrStk_Mask                                                     cBit16
#define cAf6_pla_par_err_stk_ParErrPWHdrCtrStk_Shift                                                        16

/*--------------------------------------
BitField Name: ParErrLkPWCtrRegOC96_3Stk
BitField Type: W1C
BitField Desc: Dis Parity Error Lookup PW  Control Register OC96#3
BitField Bits: [14]
--------------------------------------*/
#define cAf6_pla_par_err_stk_ParErrLkPWCtrRegOC96_3Stk_Mask                                             cBit14
#define cAf6_pla_par_err_stk_ParErrLkPWCtrRegOC96_3Stk_Shift                                                14

/*--------------------------------------
BitField Name: ParErrPldCtrRegOC96_3_OC48_1Stk
BitField Type: W1C
BitField Desc: Dis Parity Error Payload Control Register OC48#1 of OC96#3
BitField Bits: [13]
--------------------------------------*/
#define cAf6_pla_par_err_stk_ParErrPldCtrRegOC96_3_OC48_1Stk_Mask                                       cBit13
#define cAf6_pla_par_err_stk_ParErrPldCtrRegOC96_3_OC48_1Stk_Shift                                          13

/*--------------------------------------
BitField Name: ParErrPldCtrRegOC96_3_OC48_0Stk
BitField Type: W1C
BitField Desc: Dis Parity Error Payload Control Register OC48#0 of OC96#3
BitField Bits: [12]
--------------------------------------*/
#define cAf6_pla_par_err_stk_ParErrPldCtrRegOC96_3_OC48_0Stk_Mask                                       cBit12
#define cAf6_pla_par_err_stk_ParErrPldCtrRegOC96_3_OC48_0Stk_Shift                                          12

/*--------------------------------------
BitField Name: ParErrLkPWCtrRegOC96_2Stk
BitField Type: W1C
BitField Desc: Dis Parity Error Lookup PW  Control Register OC96#2
BitField Bits: [10]
--------------------------------------*/
#define cAf6_pla_par_err_stk_ParErrLkPWCtrRegOC96_2Stk_Mask                                             cBit10
#define cAf6_pla_par_err_stk_ParErrLkPWCtrRegOC96_2Stk_Shift                                                10

/*--------------------------------------
BitField Name: ParErrPldCtrRegOC96_2_OC48_1Stk
BitField Type: W1C
BitField Desc: Dis Parity Error Payload Control Register OC48#1 of OC96#2
BitField Bits: [9]
--------------------------------------*/
#define cAf6_pla_par_err_stk_ParErrPldCtrRegOC96_2_OC48_1Stk_Mask                                        cBit9
#define cAf6_pla_par_err_stk_ParErrPldCtrRegOC96_2_OC48_1Stk_Shift                                           9

/*--------------------------------------
BitField Name: ParErrPldCtrRegOC96_2_OC48_0Stk
BitField Type: W1C
BitField Desc: Dis Parity Error Payload Control Register OC48#0 of OC96#2
BitField Bits: [8]
--------------------------------------*/
#define cAf6_pla_par_err_stk_ParErrPldCtrRegOC96_2_OC48_0Stk_Mask                                        cBit8
#define cAf6_pla_par_err_stk_ParErrPldCtrRegOC96_2_OC48_0Stk_Shift                                           8

/*--------------------------------------
BitField Name: ParErrLkPWCtrRegOC96_1Stk
BitField Type: W1C
BitField Desc: Dis Parity Error Lookup PW  Control Register OC96#1
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pla_par_err_stk_ParErrLkPWCtrRegOC96_1Stk_Mask                                              cBit6
#define cAf6_pla_par_err_stk_ParErrLkPWCtrRegOC96_1Stk_Shift                                                 6

/*--------------------------------------
BitField Name: ParErrPldCtrRegOC96_1_OC48_1Stk
BitField Type: W1C
BitField Desc: Dis Parity Error Payload Control Register OC48#1 of OC96#1
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pla_par_err_stk_ParErrPldCtrRegOC96_1_OC48_1Stk_Mask                                        cBit5
#define cAf6_pla_par_err_stk_ParErrPldCtrRegOC96_1_OC48_1Stk_Shift                                           5

/*--------------------------------------
BitField Name: ParErrPldCtrRegOC96_1_OC48_0Stk
BitField Type: W1C
BitField Desc: Dis Parity Error Payload Control Register OC48#0 of OC96#1
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pla_par_err_stk_ParErrPldCtrRegOC96_1_OC48_0Stk_Mask                                        cBit4
#define cAf6_pla_par_err_stk_ParErrPldCtrRegOC96_1_OC48_0Stk_Shift                                           4

/*--------------------------------------
BitField Name: ParErrLkPWCtrRegOC96_0Stk
BitField Type: W1C
BitField Desc: Dis Parity Error Lookup PW  Control Register OC96#0
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pla_par_err_stk_ParErrLkPWCtrRegOC96_0Stk_Mask                                              cBit2
#define cAf6_pla_par_err_stk_ParErrLkPWCtrRegOC96_0Stk_Shift                                                 2

/*--------------------------------------
BitField Name: ParErrPldCtrRegOC96_0_OC48_1Stk
BitField Type: W1C
BitField Desc: Dis Parity Error Payload Control Register OC48#1 of OC96#0
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pla_par_err_stk_ParErrPldCtrRegOC96_0_OC48_1Stk_Mask                                        cBit1
#define cAf6_pla_par_err_stk_ParErrPldCtrRegOC96_0_OC48_1Stk_Shift                                           1

/*--------------------------------------
BitField Name: ParErrPldCtrRegOC96_0_OC48_0Stk
BitField Type: W1C
BitField Desc: Dis Parity Error Payload Control Register OC48#0 of OC96#0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pla_par_err_stk_ParErrPldCtrRegOC96_0_OC48_0Stk_Mask                                        cBit0
#define cAf6_pla_par_err_stk_ParErrPldCtrRegOC96_0_OC48_0Stk_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Force CRC Error Control
Reg Addr   : 0x42030
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to force CRC error

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_force_crc_err_control                                                              0x42030
#define cAf6Reg_pla_force_crc_err_control_WidthVal                                                          32

/*--------------------------------------
BitField Name: PLACrcErrForeverCfg
BitField Type: RW
BitField Desc: Force crc error mode  1: forever when field PLACrcErrNumberCfg
differ zero 0: burst
BitField Bits: [28]
--------------------------------------*/
#define cAf6_pla_force_crc_err_control_PLACrcErrForeverCfg_Mask                                         cBit28
#define cAf6_pla_force_crc_err_control_PLACrcErrForeverCfg_Shift                                            28

/*--------------------------------------
BitField Name: PLACrcErrNumberCfg
BitField Type: RW
BitField Desc: number of CRC error inserted to PLA DDR
BitField Bits: [27:0]
--------------------------------------*/
#define cAf6_pla_force_crc_err_control_PLACrcErrNumberCfg_Mask                                        cBit27_0
#define cAf6_pla_force_crc_err_control_PLACrcErrNumberCfg_Shift                                              0


/*------------------------------------------------------------------------------
Reg Name   : PLA CRC Error Sticky
Reg Addr   : 0x42031
Reg Formula: 
    Where  : 
Reg Desc   : 
This register report sticky of PLA CRC error.

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_crc_sticky                                                                         0x42031
#define cAf6Reg_pla_crc_sticky_WidthVal                                                                     32

/*--------------------------------------
BitField Name: PLACrcError
BitField Type: WC
BitField Desc: PLA CRC error
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pla_crc_sticky_PLACrcError_Mask                                                             cBit0
#define cAf6_pla_crc_sticky_PLACrcError_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : PLA CRC Error Counter
Reg Addr   : 0x42032
Reg Formula: 0x42032 + R2C
    Where  : 
           + $R2C(0-1): value zero for read only,value 1 for read to clear
Reg Desc   : 
This register counts PLA CRC error.

------------------------------------------------------------------------------*/
#define cAf6Reg_PLA_crc_counter_Base                                                                   0x42032
#define cAf6Reg_PLA_crc_counter(R2C)                                                           (0x42032+(R2C))
#define cAf6Reg_PLA_crc_counter_WidthVal                                                                    32

/*--------------------------------------
BitField Name: PLACrcErrorCounter
BitField Type: RO
BitField Desc: PLA Crc error counter
BitField Bits: [27:0]
--------------------------------------*/
#define cAf6_PLA_crc_counter_PLACrcErrorCounter_Mask                                                  cBit27_0
#define cAf6_PLA_crc_counter_PLACrcErrorCounter_Shift                                                        0

#endif /* _THA60290081MODULEPLAREG_H_ */

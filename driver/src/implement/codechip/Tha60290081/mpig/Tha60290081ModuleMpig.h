/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MPIG
 * 
 * File        : Tha60290081ModuleMpig.h
 * 
 * Created Date: Jul 30, 2019
 *
 * Description : 60290081 Module MPIG
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290081MODULEMPIG_H_
#define _THA60290081MODULEMPIG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60290081ModuleMpigNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290081MODULEMPIG_H_ */


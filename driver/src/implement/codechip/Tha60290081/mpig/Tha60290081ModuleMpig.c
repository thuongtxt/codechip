/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MPIG
 *
 * File        : Tha60290081ModuleMpig.c
 *
 * Created Date: Jul 30, 2019
 *
 * Description : 60290081 Module MPIG implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210012/mpig/Tha60210012ModuleMpigInternal.h"
#include "Tha60290081ModuleMpig.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290081ModuleMpig
    {
    tTha60210012ModuleMpig super;
    }tTha60290081ModuleMpig;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods              m_AtModuleOverride;
static tTha60210012ModuleMpigMethods m_Tha60210012ModuleMpigOverride;

/* Save super implementation */
static const tAtModuleMethods * m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(Tha60210012ModuleMpig self)
    {
    AtUnused(self);
    return 0x960000UL;
    }

static void StatusClear(AtModule self)
    {
    tTha60210012MpigGlobalCounter counters;

    Tha60210012ModuleMpigGlobalCounterDebug((Tha60210012ModuleMpig)self, &counters, cAtTrue);
    m_AtModuleMethods->StatusClear(self);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, StatusClear);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }


static void OverrideTha60210012ModuleMpig(AtModule self)
    {
    Tha60210012ModuleMpig mpig = (Tha60210012ModuleMpig)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210012ModuleMpigOverride, mMethodsGet(mpig), sizeof(m_Tha60210012ModuleMpigOverride));

        mMethodOverride(m_Tha60210012ModuleMpigOverride, BaseAddress);
        }

    mMethodsSet(mpig, &m_Tha60210012ModuleMpigOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideTha60210012ModuleMpig(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081ModuleMpig);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012ModuleMpigObjectInit(self, device) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60290081ModuleMpigNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

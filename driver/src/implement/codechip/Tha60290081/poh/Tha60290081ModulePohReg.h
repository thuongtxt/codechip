/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      : POH
 *                                                                              
 * File        : Tha60290081ModulePohReg.h
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constant definitions of POH block.
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _THA60290081MODULEPOHREG_H_
#define _THA60290081MODULEPOHREG_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : POH Hold Register
Reg Addr   : 0x00_000A
Reg Formula: 0x00_000A + holdnum
    Where  : 
           + $holdnum(0-1): Hold register number
Reg Desc   : 
This register is used for access long register of Ber, Message.

------------------------------------------------------------------------------*/
#define cAf6Reg_holdreg_Base                                                                          0x00000A
#define cAf6Reg_holdreg(holdnum)                                                          (0x00000A+(holdnum))

/*--------------------------------------
BitField Name: holdvalue
BitField Type: RW
BitField Desc: Hold value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_holdreg_holdvalue_Mask                                                                   cBit31_0
#define cAf6_holdreg_holdvalue_Shift                                                                         0


/*------------------------------------------------------------------------------
Reg Name   : POH Hold Register
Reg Addr   : 0x01_000A
Reg Formula: 0x01_000A + holdnum
    Where  : 
           + $holdnum(0-1): Hold register number
Reg Desc   : 
This register is used for access long register of Grabber.

------------------------------------------------------------------------------*/
#define cAf6Reg_holdregclk2_Base                                                                      0x01000A
#define cAf6Reg_holdregclk2(holdnum)                                                      (0x01000A+(holdnum))

/*--------------------------------------
BitField Name: holdvalue
BitField Type: RW
BitField Desc: Hold value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_holdregclk2_holdvalue_Mask                                                               cBit31_0
#define cAf6_holdregclk2_holdvalue_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : POH Global Parity Force
Reg Addr   : 0x00_0010
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to control Parity , Message Jn request.

------------------------------------------------------------------------------*/
#define cAf6Reg_pcfg_glbparfrc                                                                        0x000010

/*--------------------------------------
BitField Name: berparctrlfrc
BitField Type: RW
BitField Desc: Force parity POH BER Control VT/DSN, POH BER Control STS/TU3
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pcfg_glbparfrc_berparctrlfrc_Mask                                                           cBit5
#define cAf6_pcfg_glbparfrc_berparctrlfrc_Shift                                                              5

/*--------------------------------------
BitField Name: berpartrshfrc
BitField Type: RW
BitField Desc: Force parity  POH BER Threshold 2
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pcfg_glbparfrc_berpartrshfrc_Mask                                                           cBit4
#define cAf6_pcfg_glbparfrc_berpartrshfrc_Shift                                                              4

/*--------------------------------------
BitField Name: terparvttu3frc
BitField Type: RW
BitField Desc: Force parity POH Termintate Insert Control VT/TU3
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pcfg_glbparfrc_terparvttu3frc_Mask                                                          cBit3
#define cAf6_pcfg_glbparfrc_terparvttu3frc_Shift                                                             3

/*--------------------------------------
BitField Name: terparstsfrc
BitField Type: RW
BitField Desc: Force parity POH Termintate Insert Control STS
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pcfg_glbparfrc_terparstsfrc_Mask                                                            cBit2
#define cAf6_pcfg_glbparfrc_terparstsfrc_Shift                                                               2

/*--------------------------------------
BitField Name: cpeparvtfrc
BitField Type: RW
BitField Desc: Force parity POH CPE VT Control Register
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pcfg_glbparfrc_cpeparvtfrc_Mask                                                             cBit1
#define cAf6_pcfg_glbparfrc_cpeparvtfrc_Shift                                                                1

/*--------------------------------------
BitField Name: cpeparststu3frc
BitField Type: RW
BitField Desc: Force parity POH CPE STS/TU3 Control Register
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pcfg_glbparfrc_cpeparststu3frc_Mask                                                         cBit0
#define cAf6_pcfg_glbparfrc_cpeparststu3frc_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : POH Global Parity Disable
Reg Addr   : 0x00_0011
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to control Parity , Message Jn request.

------------------------------------------------------------------------------*/
#define cAf6Reg_pcfg_glbpardis                                                                        0x000011

/*--------------------------------------
BitField Name: berparctrldis
BitField Type: RW
BitField Desc: Disable parity POH BER Control VT/DSN, POH BER Control STS/TU3
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pcfg_glbpardis_berparctrldis_Mask                                                           cBit5
#define cAf6_pcfg_glbpardis_berparctrldis_Shift                                                              5

/*--------------------------------------
BitField Name: berpartrshdis
BitField Type: RW
BitField Desc: Disable parity POH BER Threshold 2
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pcfg_glbpardis_berpartrshdis_Mask                                                           cBit4
#define cAf6_pcfg_glbpardis_berpartrshdis_Shift                                                              4

/*--------------------------------------
BitField Name: terparvttu3dis
BitField Type: RW
BitField Desc: Disable parity POH Termintate Insert Control VT/TU3
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pcfg_glbpardis_terparvttu3dis_Mask                                                          cBit3
#define cAf6_pcfg_glbpardis_terparvttu3dis_Shift                                                             3

/*--------------------------------------
BitField Name: terparstsdis
BitField Type: RW
BitField Desc: Disable parity POH Termintate Insert Control STS
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pcfg_glbpardis_terparstsdis_Mask                                                            cBit2
#define cAf6_pcfg_glbpardis_terparstsdis_Shift                                                               2

/*--------------------------------------
BitField Name: cpeparvtdis
BitField Type: RW
BitField Desc: Disable parity POH CPE VT Control Register
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pcfg_glbpardis_cpeparvtdis_Mask                                                             cBit1
#define cAf6_pcfg_glbpardis_cpeparvtdis_Shift                                                                1

/*--------------------------------------
BitField Name: cpeparststu3dis
BitField Type: RW
BitField Desc: Disable parity POH CPE STS/TU3 Control Register
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pcfg_glbpardis_cpeparststu3dis_Mask                                                         cBit0
#define cAf6_pcfg_glbpardis_cpeparststu3dis_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : POH Global Parity Alarm
Reg Addr   : 0x00_0012
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to show alarm of Parity and Debug

------------------------------------------------------------------------------*/
#define cAf6Reg_pcfg_glbparalm                                                                        0x000012

/*--------------------------------------
BitField Name: berparctrlstk
BitField Type: W1C
BitField Desc: Parity sticky POH BER Control VT/DSN, POH BER Control STS/TU3
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pcfg_glbparalm_berparctrlstk_Mask                                                           cBit5
#define cAf6_pcfg_glbparalm_berparctrlstk_Shift                                                              5

/*--------------------------------------
BitField Name: berpartrshstk
BitField Type: W1C
BitField Desc: Parity sticky  POH BER Threshold 2
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pcfg_glbparalm_berpartrshstk_Mask                                                           cBit4
#define cAf6_pcfg_glbparalm_berpartrshstk_Shift                                                              4

/*--------------------------------------
BitField Name: terparvttu3stk
BitField Type: W1C
BitField Desc: Parity sticky POH Termintate Insert Control VT/TU3
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pcfg_glbparalm_terparvttu3stk_Mask                                                          cBit3
#define cAf6_pcfg_glbparalm_terparvttu3stk_Shift                                                             3

/*--------------------------------------
BitField Name: terparstsstk
BitField Type: W1C
BitField Desc: Parity sticky POH Termintate Insert Control STS
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pcfg_glbparalm_terparstsstk_Mask                                                            cBit2
#define cAf6_pcfg_glbparalm_terparstsstk_Shift                                                               2

/*--------------------------------------
BitField Name: cpeparvtstk
BitField Type: W1C
BitField Desc: Parity sticky POH CPE VT Control Register
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pcfg_glbparalm_cpeparvtstk_Mask                                                             cBit1
#define cAf6_pcfg_glbparalm_cpeparvtstk_Shift                                                                1

/*--------------------------------------
BitField Name: cpeparststu3stk
BitField Type: W1C
BitField Desc: Parity sticky POH CPE STS/TU3 Control Register
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pcfg_glbparalm_cpeparststu3stk_Mask                                                         cBit0
#define cAf6_pcfg_glbparalm_cpeparststu3stk_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : POH Global Control
Reg Addr   : 0x00_0000
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to control Parity , Message Jn request.

------------------------------------------------------------------------------*/
#define cAf6Reg_pcfg_glbctr                                                                           0x000000

/*--------------------------------------
BitField Name: cpujnreqen
BitField Type: RW
BitField Desc: Enable JN CPU request
BitField Bits: [31]
--------------------------------------*/
#define cAf6_pcfg_glbctr_cpujnreqen_Mask                                                                cBit31
#define cAf6_pcfg_glbctr_cpujnreqen_Shift                                                                   31

/*--------------------------------------
BitField Name: cpejnreqstsen
BitField Type: RW
BitField Desc: Enable JN CPE request for 3 group STS, 16 each
BitField Bits: [30:28]
--------------------------------------*/
#define cAf6_pcfg_glbctr_cpejnreqstsen_Mask                                                          cBit30_28
#define cAf6_pcfg_glbctr_cpejnreqstsen_Shift                                                                28

/*--------------------------------------
BitField Name: cpejnreqlineen
BitField Type: RW
BitField Desc: Enable JN CPE request for 9 line
BitField Bits: [12:4]
--------------------------------------*/
#define cAf6_pcfg_glbctr_cpejnreqlineen_Mask                                                          cBit12_4
#define cAf6_pcfg_glbctr_cpejnreqlineen_Shift                                                                4

/*--------------------------------------
BitField Name: jnbdebound
BitField Type: RW
BitField Desc: Debound Threshold for Jn 1byte
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_pcfg_glbctr_jnbdebound_Mask                                                               cBit3_0
#define cAf6_pcfg_glbctr_jnbdebound_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : POH Global Control
Reg Addr   : 0x00_0020
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to control Parity , Message Jn request.

------------------------------------------------------------------------------*/
#define cAf6Reg_pcfg_glbctr2                                                                          0x000020

/*--------------------------------------
BitField Name: terjnreqslen
BitField Type: RW
BitField Desc: Enable JN Ter request for 8 Line
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_pcfg_glbctr2_terjnreqslen_Mask                                                           cBit15_0
#define cAf6_pcfg_glbctr2_terjnreqslen_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : POH Global Alarm
Reg Addr   : 0x00_0002
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to show alarm of Parity and Debug

------------------------------------------------------------------------------*/
#define cAf6Reg_pcfg_glbalm                                                                           0x000002

/*--------------------------------------
BitField Name: hwberbusy
BitField Type: RO
BitField Desc: Hardware BER is busy
BitField Bits: [27]
--------------------------------------*/
#define cAf6_pcfg_glbalm_hwberbusy_Mask                                                                 cBit27
#define cAf6_pcfg_glbalm_hwberbusy_Shift                                                                    27

/*--------------------------------------
BitField Name: hwbusy
BitField Type: RO
BitField Desc: Hardware Jn is busy
BitField Bits: [11]
--------------------------------------*/
#define cAf6_pcfg_glbalm_hwbusy_Mask                                                                    cBit11
#define cAf6_pcfg_glbalm_hwbusy_Shift                                                                       11

/*--------------------------------------
BitField Name: pmstk
BitField Type: W1C
BitField Desc: PM monitor sticky
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pcfg_glbalm_pmstk_Mask                                                                      cBit6
#define cAf6_pcfg_glbalm_pmstk_Shift                                                                         6

/*--------------------------------------
BitField Name: berparctrlstk
BitField Type: W1C
BitField Desc: Parity sticky POH BER Control VT/DSN, POH BER Control STS/TU3
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pcfg_glbalm_berparctrlstk_Mask                                                              cBit5
#define cAf6_pcfg_glbalm_berparctrlstk_Shift                                                                 5

/*--------------------------------------
BitField Name: berpartrshstk
BitField Type: W1C
BitField Desc: Parity sticky  POH BER Threshold 2
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pcfg_glbalm_berpartrshstk_Mask                                                              cBit4
#define cAf6_pcfg_glbalm_berpartrshstk_Shift                                                                 4

/*--------------------------------------
BitField Name: terparvttu3stk
BitField Type: W1C
BitField Desc: Parity sticky POH Termintate Insert Control VT/TU3
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pcfg_glbalm_terparvttu3stk_Mask                                                             cBit3
#define cAf6_pcfg_glbalm_terparvttu3stk_Shift                                                                3

/*--------------------------------------
BitField Name: terparstsstk
BitField Type: W1C
BitField Desc: Parity sticky POH Termintate Insert Control STS
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pcfg_glbalm_terparstsstk_Mask                                                               cBit2
#define cAf6_pcfg_glbalm_terparstsstk_Shift                                                                  2

/*--------------------------------------
BitField Name: cpeparvtstk
BitField Type: W1C
BitField Desc: Parity sticky POH CPE VT Control Register
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pcfg_glbalm_cpeparvtstk_Mask                                                                cBit1
#define cAf6_pcfg_glbalm_cpeparvtstk_Shift                                                                   1

/*--------------------------------------
BitField Name: cpeparststu3stk
BitField Type: W1C
BitField Desc: Parity sticky POH CPE STS/TU3 Control Register
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pcfg_glbalm_cpeparststu3stk_Mask                                                            cBit0
#define cAf6_pcfg_glbalm_cpeparststu3stk_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE Global Control
Reg Addr   : 0x01_0002
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to control whether receive bytes from OCN block .

------------------------------------------------------------------------------*/
#define cAf6Reg_pcfg_glbcpectr                                                                        0x010002

/*--------------------------------------
BitField Name: cpevtline7en
BitField Type: RW
BitField Desc: Enable recevive bytes for VTs Line 7
BitField Bits: [31]
--------------------------------------*/
#define cAf6_pcfg_glbcpectr_cpevtline7en_Mask                                                           cBit31
#define cAf6_pcfg_glbcpectr_cpevtline7en_Shift                                                              31

/*--------------------------------------
BitField Name: cpevtline6en
BitField Type: RW
BitField Desc: Enable recevive bytes for VTs Line 6
BitField Bits: [30]
--------------------------------------*/
#define cAf6_pcfg_glbcpectr_cpevtline6en_Mask                                                           cBit30
#define cAf6_pcfg_glbcpectr_cpevtline6en_Shift                                                              30

/*--------------------------------------
BitField Name: cpevtline5en
BitField Type: RW
BitField Desc: Enable recevive bytes for VTs Line 5
BitField Bits: [29]
--------------------------------------*/
#define cAf6_pcfg_glbcpectr_cpevtline5en_Mask                                                           cBit29
#define cAf6_pcfg_glbcpectr_cpevtline5en_Shift                                                              29

/*--------------------------------------
BitField Name: cpevtline4en
BitField Type: RW
BitField Desc: Enable recevive bytes for VTs Line 4
BitField Bits: [28]
--------------------------------------*/
#define cAf6_pcfg_glbcpectr_cpevtline4en_Mask                                                           cBit28
#define cAf6_pcfg_glbcpectr_cpevtline4en_Shift                                                              28

/*--------------------------------------
BitField Name: cpevtline3en
BitField Type: RW
BitField Desc: Enable recevive bytes for VTs Line 3
BitField Bits: [27]
--------------------------------------*/
#define cAf6_pcfg_glbcpectr_cpevtline3en_Mask                                                           cBit27
#define cAf6_pcfg_glbcpectr_cpevtline3en_Shift                                                              27

/*--------------------------------------
BitField Name: cpevtline2en
BitField Type: RW
BitField Desc: Enable recevive bytes for VTs Line 2
BitField Bits: [26]
--------------------------------------*/
#define cAf6_pcfg_glbcpectr_cpevtline2en_Mask                                                           cBit26
#define cAf6_pcfg_glbcpectr_cpevtline2en_Shift                                                              26

/*--------------------------------------
BitField Name: cpevtline1en
BitField Type: RW
BitField Desc: Enable recevive bytes for VTs Line 1
BitField Bits: [25]
--------------------------------------*/
#define cAf6_pcfg_glbcpectr_cpevtline1en_Mask                                                           cBit25
#define cAf6_pcfg_glbcpectr_cpevtline1en_Shift                                                              25

/*--------------------------------------
BitField Name: cpevtline0en
BitField Type: RW
BitField Desc: Enable recevive bytes for VTs Line 0
BitField Bits: [24]
--------------------------------------*/
#define cAf6_pcfg_glbcpectr_cpevtline0en_Mask                                                           cBit24
#define cAf6_pcfg_glbcpectr_cpevtline0en_Shift                                                              24

/*--------------------------------------
BitField Name: cpestsline8en
BitField Type: RW
BitField Desc: Enable recevive bytes for STSs Line 8
BitField Bits: [8]
--------------------------------------*/
#define cAf6_pcfg_glbcpectr_cpestsline8en_Mask                                                           cBit8
#define cAf6_pcfg_glbcpectr_cpestsline8en_Shift                                                              8

/*--------------------------------------
BitField Name: cpestsline7en
BitField Type: RW
BitField Desc: Enable recevive bytes for STSs Line 7
BitField Bits: [7]
--------------------------------------*/
#define cAf6_pcfg_glbcpectr_cpestsline7en_Mask                                                           cBit7
#define cAf6_pcfg_glbcpectr_cpestsline7en_Shift                                                              7

/*--------------------------------------
BitField Name: cpestsline6en
BitField Type: RW
BitField Desc: Enable recevive bytes for STSs Line 6
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pcfg_glbcpectr_cpestsline6en_Mask                                                           cBit6
#define cAf6_pcfg_glbcpectr_cpestsline6en_Shift                                                              6

/*--------------------------------------
BitField Name: cpestsline5en
BitField Type: RW
BitField Desc: Enable recevive bytes for STSs Line 5
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pcfg_glbcpectr_cpestsline5en_Mask                                                           cBit5
#define cAf6_pcfg_glbcpectr_cpestsline5en_Shift                                                              5

/*--------------------------------------
BitField Name: cpestsline4en
BitField Type: RW
BitField Desc: Enable recevive bytes for STSs Line 4
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pcfg_glbcpectr_cpestsline4en_Mask                                                           cBit4
#define cAf6_pcfg_glbcpectr_cpestsline4en_Shift                                                              4

/*--------------------------------------
BitField Name: cpestsline3en
BitField Type: RW
BitField Desc: Enable recevive bytes for STSs Line 3
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pcfg_glbcpectr_cpestsline3en_Mask                                                           cBit3
#define cAf6_pcfg_glbcpectr_cpestsline3en_Shift                                                              3

/*--------------------------------------
BitField Name: cpestsline2en
BitField Type: RW
BitField Desc: Enable recevive bytes for STSs Line 2
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pcfg_glbcpectr_cpestsline2en_Mask                                                           cBit2
#define cAf6_pcfg_glbcpectr_cpestsline2en_Shift                                                              2

/*--------------------------------------
BitField Name: cpestsline1en
BitField Type: RW
BitField Desc: Enable recevive bytes for STSs Line 1
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pcfg_glbcpectr_cpestsline1en_Mask                                                           cBit1
#define cAf6_pcfg_glbcpectr_cpestsline1en_Shift                                                              1

/*--------------------------------------
BitField Name: cpestsline0en
BitField Type: RW
BitField Desc: Enable recevive bytes for STSs Line 0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pcfg_glbcpectr_cpestsline0en_Mask                                                           cBit0
#define cAf6_pcfg_glbcpectr_cpestsline0en_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : POH TER Global Control
Reg Addr   : 0x01_0003
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to control whether transmit bytes from OCN block .

------------------------------------------------------------------------------*/
#define cAf6Reg_pcfg_glbterctr                                                                        0x010003

/*--------------------------------------
BitField Name: tervtline7en
BitField Type: RW
BitField Desc: Enable transmit bytes for VTs Line 7
BitField Bits: [15]
--------------------------------------*/
#define cAf6_pcfg_glbterctr_tervtline7en_Mask                                                           cBit15
#define cAf6_pcfg_glbterctr_tervtline7en_Shift                                                              15

/*--------------------------------------
BitField Name: tervtline6en
BitField Type: RW
BitField Desc: Enable transmit bytes for VTs Line 6
BitField Bits: [14]
--------------------------------------*/
#define cAf6_pcfg_glbterctr_tervtline6en_Mask                                                           cBit14
#define cAf6_pcfg_glbterctr_tervtline6en_Shift                                                              14

/*--------------------------------------
BitField Name: tervtline5en
BitField Type: RW
BitField Desc: Enable transmit bytes for VTs Line 5
BitField Bits: [13]
--------------------------------------*/
#define cAf6_pcfg_glbterctr_tervtline5en_Mask                                                           cBit13
#define cAf6_pcfg_glbterctr_tervtline5en_Shift                                                              13

/*--------------------------------------
BitField Name: tervtline4en
BitField Type: RW
BitField Desc: Enable transmit bytes for VTs Line 4
BitField Bits: [12]
--------------------------------------*/
#define cAf6_pcfg_glbterctr_tervtline4en_Mask                                                           cBit12
#define cAf6_pcfg_glbterctr_tervtline4en_Shift                                                              12

/*--------------------------------------
BitField Name: tervtline3en
BitField Type: RW
BitField Desc: Enable transmit bytes for VTs Line 3
BitField Bits: [11]
--------------------------------------*/
#define cAf6_pcfg_glbterctr_tervtline3en_Mask                                                           cBit11
#define cAf6_pcfg_glbterctr_tervtline3en_Shift                                                              11

/*--------------------------------------
BitField Name: tervtline2en
BitField Type: RW
BitField Desc: Enable transmit bytes for VTs Line 2
BitField Bits: [10]
--------------------------------------*/
#define cAf6_pcfg_glbterctr_tervtline2en_Mask                                                           cBit10
#define cAf6_pcfg_glbterctr_tervtline2en_Shift                                                              10

/*--------------------------------------
BitField Name: tervtline1en
BitField Type: RW
BitField Desc: Enable transmit bytes for VTs Line 1
BitField Bits: [9]
--------------------------------------*/
#define cAf6_pcfg_glbterctr_tervtline1en_Mask                                                            cBit9
#define cAf6_pcfg_glbterctr_tervtline1en_Shift                                                               9

/*--------------------------------------
BitField Name: tervtline0en
BitField Type: RW
BitField Desc: Enable transmit bytes for VTs Line 0
BitField Bits: [8]
--------------------------------------*/
#define cAf6_pcfg_glbterctr_tervtline0en_Mask                                                            cBit8
#define cAf6_pcfg_glbterctr_tervtline0en_Shift                                                               8

/*--------------------------------------
BitField Name: terstsline7en
BitField Type: RW
BitField Desc: Enable transmit bytes for STSs Line 7
BitField Bits: [7]
--------------------------------------*/
#define cAf6_pcfg_glbterctr_terstsline7en_Mask                                                           cBit7
#define cAf6_pcfg_glbterctr_terstsline7en_Shift                                                              7

/*--------------------------------------
BitField Name: terstsline6en
BitField Type: RW
BitField Desc: Enable transmit bytes for STSs Line 6
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pcfg_glbterctr_terstsline6en_Mask                                                           cBit6
#define cAf6_pcfg_glbterctr_terstsline6en_Shift                                                              6

/*--------------------------------------
BitField Name: terstsline5en
BitField Type: RW
BitField Desc: Enable transmit bytes for STSs Line 5
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pcfg_glbterctr_terstsline5en_Mask                                                           cBit5
#define cAf6_pcfg_glbterctr_terstsline5en_Shift                                                              5

/*--------------------------------------
BitField Name: terstsline4en
BitField Type: RW
BitField Desc: Enable transmit bytes for STSs Line 4
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pcfg_glbterctr_terstsline4en_Mask                                                           cBit4
#define cAf6_pcfg_glbterctr_terstsline4en_Shift                                                              4

/*--------------------------------------
BitField Name: terstsline3en
BitField Type: RW
BitField Desc: Enable transmit bytes for STSs Line 3
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pcfg_glbterctr_terstsline3en_Mask                                                           cBit3
#define cAf6_pcfg_glbterctr_terstsline3en_Shift                                                              3

/*--------------------------------------
BitField Name: terstsline2en
BitField Type: RW
BitField Desc: Enable transmit bytes for STSs Line 2
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pcfg_glbterctr_terstsline2en_Mask                                                           cBit2
#define cAf6_pcfg_glbterctr_terstsline2en_Shift                                                              2

/*--------------------------------------
BitField Name: terstsline1en
BitField Type: RW
BitField Desc: Enable transmit bytes for STSs Line 1
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pcfg_glbterctr_terstsline1en_Mask                                                           cBit1
#define cAf6_pcfg_glbterctr_terstsline1en_Shift                                                              1

/*--------------------------------------
BitField Name: terstsline0en
BitField Type: RW
BitField Desc: Enable transmit bytes for STSs Line 0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pcfg_glbterctr_terstsline0en_Mask                                                           cBit0
#define cAf6_pcfg_glbterctr_terstsline0en_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : POH Threshold Global Control
Reg Addr   : 0x00_0003
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to set Threshold for stable detection.

------------------------------------------------------------------------------*/
#define cAf6Reg_pcfg_trshglbctr                                                                       0x000003

/*--------------------------------------
BitField Name: jnmsgdebound
BitField Type: RW
BitField Desc: Debound Threshold for Jn 16/64byte
BitField Bits: [31:28]
--------------------------------------*/
#define cAf6_pcfg_trshglbctr_jnmsgdebound_Mask                                                       cBit31_28
#define cAf6_pcfg_trshglbctr_jnmsgdebound_Shift                                                             28

/*--------------------------------------
BitField Name: V5RFIStbTrsh
BitField Type: RW
BitField Desc: V5 RDI Stable Thershold
BitField Bits: [27:24]
--------------------------------------*/
#define cAf6_pcfg_trshglbctr_V5RFIStbTrsh_Mask                                                       cBit27_24
#define cAf6_pcfg_trshglbctr_V5RFIStbTrsh_Shift                                                             24

/*--------------------------------------
BitField Name: V5RDIStbTrsh
BitField Type: RW
BitField Desc: V5 RDI Stable Thershold
BitField Bits: [23:20]
--------------------------------------*/
#define cAf6_pcfg_trshglbctr_V5RDIStbTrsh_Mask                                                       cBit23_20
#define cAf6_pcfg_trshglbctr_V5RDIStbTrsh_Shift                                                             20

/*--------------------------------------
BitField Name: V5SlbStbTrsh
BitField Type: RW
BitField Desc: V5 Signal Lable Stable Thershold
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_pcfg_trshglbctr_V5SlbStbTrsh_Mask                                                       cBit19_16
#define cAf6_pcfg_trshglbctr_V5SlbStbTrsh_Shift                                                             16

/*--------------------------------------
BitField Name: G1RDIStbTrsh
BitField Type: RW
BitField Desc: G1 RDI Path Stable Thershold
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_pcfg_trshglbctr_G1RDIStbTrsh_Mask                                                       cBit15_12
#define cAf6_pcfg_trshglbctr_G1RDIStbTrsh_Shift                                                             12

/*--------------------------------------
BitField Name: C2PlmStbTrsh
BitField Type: RW
BitField Desc: C2 Path Signal Lable Stable Thershold
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_pcfg_trshglbctr_C2PlmStbTrsh_Mask                                                        cBit11_8
#define cAf6_pcfg_trshglbctr_C2PlmStbTrsh_Shift                                                              8

/*--------------------------------------
BitField Name: JnStbTrsh
BitField Type: RW
BitField Desc: J1/J2 Message Stable Threshold
BitField Bits: [7:4]
--------------------------------------*/
#define cAf6_pcfg_trshglbctr_JnStbTrsh_Mask                                                            cBit7_4
#define cAf6_pcfg_trshglbctr_JnStbTrsh_Shift                                                                 4

/*--------------------------------------
BitField Name: debound
BitField Type: RW
BitField Desc: Debound Threshold for bytes exclude Jn
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_pcfg_trshglbctr_debound_Mask                                                              cBit3_0
#define cAf6_pcfg_trshglbctr_debound_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : POH Hi-order Path Over Head Grabber
Reg Addr   : 0x02_0000
Reg Formula: 0x02_0000 + $stsid + $sliceid*$stsmax+1
    Where  : 
           + $sliceid(0-3): Slice Identification for Line 0-3
           + $stsmax(47-47): STS max
           + $stsid(0-stsmax): STS Identification
Reg Desc   : 
This register is used to grabber Hi-Order Path Overhead

------------------------------------------------------------------------------*/
#define cAf6Reg_pohstspohgrb_Base                                                                     0x020000
#define cAf6Reg_pohstspohgrb(sliceid, stsmax, stsid)                   (0x020000+(stsid)+(sliceid)*(stsmax)+1)
#define cAf6Reg_pohstspohgrb_WidthVal                                                                       96

/*--------------------------------------
BitField Name: hlais
BitField Type: RO
BitField Desc: High-level AIS from OCN
BitField Bits: [67]
--------------------------------------*/
#define cAf6_pohstspohgrb_hlais_Mask                                                                     cBit3
#define cAf6_pohstspohgrb_hlais_Shift                                                                        3

/*--------------------------------------
BitField Name: lom
BitField Type: RO
BitField Desc: LOM  from OCN
BitField Bits: [66]
--------------------------------------*/
#define cAf6_pohstspohgrb_lom_Mask                                                                       cBit2
#define cAf6_pohstspohgrb_lom_Shift                                                                          2

/*--------------------------------------
BitField Name: lop
BitField Type: RO
BitField Desc: LOP from OCN
BitField Bits: [65]
--------------------------------------*/
#define cAf6_pohstspohgrb_lop_Mask                                                                       cBit1
#define cAf6_pohstspohgrb_lop_Shift                                                                          1

/*--------------------------------------
BitField Name: ais
BitField Type: RO
BitField Desc: AIS from OCN
BitField Bits: [64]
--------------------------------------*/
#define cAf6_pohstspohgrb_ais_Mask                                                                       cBit0
#define cAf6_pohstspohgrb_ais_Shift                                                                          0

/*--------------------------------------
BitField Name: K3
BitField Type: RO
BitField Desc: K3 byte
BitField Bits: [63:56]
--------------------------------------*/
#define cAf6_pohstspohgrb_K3_Mask                                                                    cBit31_24
#define cAf6_pohstspohgrb_K3_Shift                                                                          24

/*--------------------------------------
BitField Name: F3
BitField Type: RO
BitField Desc: F3 byte
BitField Bits: [55:48]
--------------------------------------*/
#define cAf6_pohstspohgrb_F3_Mask                                                                    cBit23_16
#define cAf6_pohstspohgrb_F3_Shift                                                                          16

/*--------------------------------------
BitField Name: H4
BitField Type: RO
BitField Desc: H4 byte
BitField Bits: [47:40]
--------------------------------------*/
#define cAf6_pohstspohgrb_H4_Mask                                                                     cBit15_8
#define cAf6_pohstspohgrb_H4_Shift                                                                           8

/*--------------------------------------
BitField Name: F2
BitField Type: RO
BitField Desc: F2 byte
BitField Bits: [39:32]
--------------------------------------*/
#define cAf6_pohstspohgrb_F2_Mask                                                                      cBit7_0
#define cAf6_pohstspohgrb_F2_Shift                                                                           0

/*--------------------------------------
BitField Name: G1
BitField Type: RO
BitField Desc: G1 byte
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_pohstspohgrb_G1_Mask                                                                    cBit31_24
#define cAf6_pohstspohgrb_G1_Shift                                                                          24

/*--------------------------------------
BitField Name: C2
BitField Type: RO
BitField Desc: C2 byte
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_pohstspohgrb_C2_Mask                                                                    cBit23_16
#define cAf6_pohstspohgrb_C2_Shift                                                                          16

/*--------------------------------------
BitField Name: N1
BitField Type: RO
BitField Desc: N1 byte
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_pohstspohgrb_N1_Mask                                                                     cBit15_8
#define cAf6_pohstspohgrb_N1_Shift                                                                           8

/*--------------------------------------
BitField Name: J1
BitField Type: RO
BitField Desc: J1 byte
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_pohstspohgrb_J1_Mask                                                                      cBit7_0
#define cAf6_pohstspohgrb_J1_Shift                                                                           0


/*------------------------------------------------------------------------------
Reg Name   : POH Lo-order VT Over Head Grabber
Reg Addr   : 0x02_2000
Reg Formula: 0x02_2000 + $sliceid*32 + $stsid*128 + $vtid
    Where  : 
           + $sliceid(0-3): Slice Identification for Line 0-3
           + $stsid(0-47): STS Identification
           + $vtid(0-27): VT Identification
Reg Desc   : 
This register is used to grabber Lo-Order Path Overhead. Incase the TU3 mode, the $vtid = 0, using for Tu3 POH grabber. %%
Incase VT mode, the $vtid = 0-27, using for VT POH grabber.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohvtpohgrb_Base                                                                      0x022000
#define cAf6Reg_pohvtpohgrb(sliceid, stsid, vtid)                     (0x022000+(sliceid)*32+(stsid)*128+(vtid))
#define cAf6Reg_pohvtpohgrb_WidthVal                                                                        64

/*--------------------------------------
BitField Name: hlais
BitField Type: RO
BitField Desc: High-level AIS from OCN
BitField Bits: [35]
--------------------------------------*/
#define cAf6_pohvtpohgrb_hlais_Mask                                                                      cBit3
#define cAf6_pohvtpohgrb_hlais_Shift                                                                         3

/*--------------------------------------
BitField Name: lop
BitField Type: RO
BitField Desc: LOP from OCN
BitField Bits: [33]
--------------------------------------*/
#define cAf6_pohvtpohgrb_lop_Mask                                                                        cBit1
#define cAf6_pohvtpohgrb_lop_Shift                                                                           1

/*--------------------------------------
BitField Name: ais
BitField Type: RO
BitField Desc: AIS from OCN
BitField Bits: [32]
--------------------------------------*/
#define cAf6_pohvtpohgrb_ais_Mask                                                                     cBit32_0
#define cAf6_pohvtpohgrb_ais_Shift                                                                           0

/*--------------------------------------
BitField Name: Byte3
BitField Type: RO
BitField Desc: G1 byte or K3 byte or K4 byte
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_pohvtpohgrb_Byte3_Mask                                                                  cBit31_24
#define cAf6_pohvtpohgrb_Byte3_Shift                                                                        24

/*--------------------------------------
BitField Name: Byte2
BitField Type: RO
BitField Desc: C2 byte or F3 byte or N2 byte
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_pohvtpohgrb_Byte2_Mask                                                                  cBit23_16
#define cAf6_pohvtpohgrb_Byte2_Shift                                                                        16

/*--------------------------------------
BitField Name: Byte1
BitField Type: RO
BitField Desc: N1 byte or H4 byte or J2 byte
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_pohvtpohgrb_Byte1_Mask                                                                   cBit15_8
#define cAf6_pohvtpohgrb_Byte1_Shift                                                                         8

/*--------------------------------------
BitField Name: Byte0
BitField Type: RO
BitField Desc: J1 byte or F2 byte or V5 byte
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_pohvtpohgrb_Byte0_Mask                                                                    cBit7_0
#define cAf6_pohvtpohgrb_Byte0_Shift                                                                         0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE STS/TU3 Control Register
Reg Addr   : 0x02_A000
Reg Formula: 0x02_A000 + $sliceid*$stsmax+1*2 + $stsid*2 + $tu3en
    Where  : 
           + $sliceid(0-3): Slice Identification
           + $stsmax(47-47): STS max
           + $stsid(0-stsmax): STS Identification
           + $tu3en(0-1): Tu3enable, 0: STS, 1:Tu3
Reg Desc   : 
This register is used to configure the POH Hi-order Path Monitoring.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohcpestsctr_Base                                                                     0x02A000
#define cAf6Reg_pohcpestsctr(sliceid, stsmax, stsid, tu3en)           (0x02A000+(sliceid)*(stsmax)+1*2+(stsid)*2+(tu3en))

/*--------------------------------------
BitField Name: MonEnb
BitField Type: RW
BitField Desc: Alarm monitor enable
BitField Bits: [20]
--------------------------------------*/
#define cAf6_pohcpestsctr_MonEnb_Mask                                                                   cBit20
#define cAf6_pohcpestsctr_MonEnb_Shift                                                                      20

/*--------------------------------------
BitField Name: PlmEnb
BitField Type: RW
BitField Desc: PLM enable
BitField Bits: [19]
--------------------------------------*/
#define cAf6_pohcpestsctr_PlmEnb_Mask                                                                   cBit19
#define cAf6_pohcpestsctr_PlmEnb_Shift                                                                      19

/*--------------------------------------
BitField Name: VcaisDstren
BitField Type: RW
BitField Desc: VcaisDstren
BitField Bits: [18]
--------------------------------------*/
#define cAf6_pohcpestsctr_VcaisDstren_Mask                                                              cBit18
#define cAf6_pohcpestsctr_VcaisDstren_Shift                                                                 18

/*--------------------------------------
BitField Name: PlmDstren
BitField Type: RW
BitField Desc: PlmDstren
BitField Bits: [17]
--------------------------------------*/
#define cAf6_pohcpestsctr_PlmDstren_Mask                                                                cBit17
#define cAf6_pohcpestsctr_PlmDstren_Shift                                                                   17

/*--------------------------------------
BitField Name: UneqDstren
BitField Type: RW
BitField Desc: UneqDstren
BitField Bits: [16]
--------------------------------------*/
#define cAf6_pohcpestsctr_UneqDstren_Mask                                                               cBit16
#define cAf6_pohcpestsctr_UneqDstren_Shift                                                                  16

/*--------------------------------------
BitField Name: TimDstren
BitField Type: RW
BitField Desc: TimDstren
BitField Bits: [15]
--------------------------------------*/
#define cAf6_pohcpestsctr_TimDstren_Mask                                                                cBit15
#define cAf6_pohcpestsctr_TimDstren_Shift                                                                   15

/*--------------------------------------
BitField Name: Sdhmode
BitField Type: RW
BitField Desc: SDH mode
BitField Bits: [14]
--------------------------------------*/
#define cAf6_pohcpestsctr_Sdhmode_Mask                                                                  cBit14
#define cAf6_pohcpestsctr_Sdhmode_Shift                                                                     14

/*--------------------------------------
BitField Name: Blkmden
BitField Type: RW
BitField Desc: Block mode BIP
BitField Bits: [13]
--------------------------------------*/
#define cAf6_pohcpestsctr_Blkmden_Mask                                                                  cBit13
#define cAf6_pohcpestsctr_Blkmden_Shift                                                                     13

/*--------------------------------------
BitField Name: ERDIenb
BitField Type: RW
BitField Desc: Enable E-RDI
BitField Bits: [12]
--------------------------------------*/
#define cAf6_pohcpestsctr_ERDIenb_Mask                                                                  cBit12
#define cAf6_pohcpestsctr_ERDIenb_Shift                                                                     12

/*--------------------------------------
BitField Name: PslExp
BitField Type: RW
BitField Desc: C2 Expected Path Signal Lable Value
BitField Bits: [11:4]
--------------------------------------*/
#define cAf6_pohcpestsctr_PslExp_Mask                                                                 cBit11_4
#define cAf6_pohcpestsctr_PslExp_Shift                                                                       4

/*--------------------------------------
BitField Name: TimEnb
BitField Type: RW
BitField Desc: Enable check expect for TIM, Out of frame or unstable alwayse
raise TIM
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pohcpestsctr_TimEnb_Mask                                                                    cBit3
#define cAf6_pohcpestsctr_TimEnb_Shift                                                                       3

/*--------------------------------------
BitField Name: Reiblkmden
BitField Type: RW
BitField Desc: Block mode REI
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pohcpestsctr_Reiblkmden_Mask                                                                cBit2
#define cAf6_pohcpestsctr_Reiblkmden_Shift                                                                   2

/*--------------------------------------
BitField Name: J1mode
BitField Type: RW
BitField Desc: 0: 1Byte 1:16Byte 2:64byte 3:Floating
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_pohcpestsctr_J1mode_Mask                                                                  cBit1_0
#define cAf6_pohcpestsctr_J1mode_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE VT Control Register
Reg Addr   : 0x02_8000
Reg Formula: 0x02_8000 + $sliceid*$stsmax+1*$vtmax+1 + $stsid*$vtmax+1 +$vtid
    Where  : 
           + $sliceid(0-3): Slice Identification
           + $stsmax(47-47): STS max
           + $vtmax(27-27): VT max
           + $stsid(0-stsmax): STS Identification
           + $vtid(0-vtmax):Vt Identification
Reg Desc   : 
This register is used to configure the POH Lo-order Path Monitoring.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohcpevtctr_Base                                                                      0x028000
#define cAf6Reg_pohcpevtctr(sliceid, stsmax, vtmax, stsid, vtid)      (0x028000+(sliceid)*(stsmax)+1*(vtmax)+1+(stsid)*(vtmax)+1+(vtid))

/*--------------------------------------
BitField Name: MonEnb
BitField Type: RW
BitField Desc: Alarm monitor enable
BitField Bits: [15]
--------------------------------------*/
#define cAf6_pohcpevtctr_MonEnb_Mask                                                                    cBit15
#define cAf6_pohcpevtctr_MonEnb_Shift                                                                       15

/*--------------------------------------
BitField Name: PlmEnb
BitField Type: RW
BitField Desc: VcaisDstren
BitField Bits: [14]
--------------------------------------*/
#define cAf6_pohcpevtctr_PlmEnb_Mask                                                                    cBit14
#define cAf6_pohcpevtctr_PlmEnb_Shift                                                                       14

/*--------------------------------------
BitField Name: VcaisDstren
BitField Type: RW
BitField Desc: VcaisDstren
BitField Bits: [13]
--------------------------------------*/
#define cAf6_pohcpevtctr_VcaisDstren_Mask                                                               cBit13
#define cAf6_pohcpevtctr_VcaisDstren_Shift                                                                  13

/*--------------------------------------
BitField Name: PlmDstren
BitField Type: RW
BitField Desc: PlmDstren
BitField Bits: [12]
--------------------------------------*/
#define cAf6_pohcpevtctr_PlmDstren_Mask                                                                 cBit12
#define cAf6_pohcpevtctr_PlmDstren_Shift                                                                    12

/*--------------------------------------
BitField Name: UneqDstren
BitField Type: RW
BitField Desc: UneqDstren
BitField Bits: [11]
--------------------------------------*/
#define cAf6_pohcpevtctr_UneqDstren_Mask                                                                cBit11
#define cAf6_pohcpevtctr_UneqDstren_Shift                                                                   11

/*--------------------------------------
BitField Name: TimDstren
BitField Type: RW
BitField Desc: TimDstren
BitField Bits: [10]
--------------------------------------*/
#define cAf6_pohcpevtctr_TimDstren_Mask                                                                 cBit10
#define cAf6_pohcpevtctr_TimDstren_Shift                                                                    10

/*--------------------------------------
BitField Name: VSdhmode
BitField Type: RW
BitField Desc: SDH mode
BitField Bits: [9]
--------------------------------------*/
#define cAf6_pohcpevtctr_VSdhmode_Mask                                                                   cBit9
#define cAf6_pohcpevtctr_VSdhmode_Shift                                                                      9

/*--------------------------------------
BitField Name: VBlkmden
BitField Type: RW
BitField Desc: Block mode BIP
BitField Bits: [8]
--------------------------------------*/
#define cAf6_pohcpevtctr_VBlkmden_Mask                                                                   cBit8
#define cAf6_pohcpevtctr_VBlkmden_Shift                                                                      8

/*--------------------------------------
BitField Name: ERDIenb
BitField Type: RW
BitField Desc: Enable E-RDI
BitField Bits: [7]
--------------------------------------*/
#define cAf6_pohcpevtctr_ERDIenb_Mask                                                                    cBit7
#define cAf6_pohcpevtctr_ERDIenb_Shift                                                                       7

/*--------------------------------------
BitField Name: VslExp
BitField Type: RW
BitField Desc: V5 Expected Path Signal Lable Value
BitField Bits: [6:4]
--------------------------------------*/
#define cAf6_pohcpevtctr_VslExp_Mask                                                                   cBit6_4
#define cAf6_pohcpevtctr_VslExp_Shift                                                                        4

/*--------------------------------------
BitField Name: TimEnb
BitField Type: RW
BitField Desc: Enable Monitor TIM
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pohcpevtctr_TimEnb_Mask                                                                     cBit3
#define cAf6_pohcpevtctr_TimEnb_Shift                                                                        3

/*--------------------------------------
BitField Name: Reiblkmden
BitField Type: RW
BitField Desc: Block mode REI
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pohcpevtctr_Reiblkmden_Mask                                                                 cBit2
#define cAf6_pohcpevtctr_Reiblkmden_Shift                                                                    2

/*--------------------------------------
BitField Name: J2mode
BitField Type: RW
BitField Desc: 0: 1Byte 1:16Byte 2:64byte 3:Floating
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_pohcpevtctr_J2mode_Mask                                                                   cBit1_0
#define cAf6_pohcpevtctr_J2mode_Shift                                                                        0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE STS Status Register
Reg Addr   : 0x02_D500
Reg Formula: 0x02_D500 + $sliceid*$stsmax+1 + $stsid
    Where  : 
           + $sliceid(0-3): Slice Identification
           + $stsmax(47-47): STS max
           + $stsid(0-stsmax): STS Identification
Reg Desc   : 
This register is used to get POH Hi-order status of monitoring.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohcpestssta_Base                                                                     0x02D500
#define cAf6Reg_pohcpestssta(sliceid, stsmax, stsid)                   (0x02D500+(sliceid)*(stsmax)+1+(stsid))

/*--------------------------------------
BitField Name: Jnstbcntoo
BitField Type: RO
BitField Desc: Jn state machine
BitField Bits: [30:27]
--------------------------------------*/
#define cAf6_pohcpestssta_Jnstbcntoo_Mask                                                            cBit30_27
#define cAf6_pohcpestssta_Jnstbcntoo_Shift                                                                  27

/*--------------------------------------
BitField Name: Jntimstao
BitField Type: RO
BitField Desc: Jn tim status
BitField Bits: [26]
--------------------------------------*/
#define cAf6_pohcpestssta_Jntimstao_Mask                                                                cBit26
#define cAf6_pohcpestssta_Jntimstao_Shift                                                                   26

/*--------------------------------------
BitField Name: Jnmismsgo
BitField Type: RO
BitField Desc: Jn mis-match current message
BitField Bits: [25]
--------------------------------------*/
#define cAf6_pohcpestssta_Jnmismsgo_Mask                                                                cBit25
#define cAf6_pohcpestssta_Jnmismsgo_Shift                                                                   25

/*--------------------------------------
BitField Name: Jnmispato
BitField Type: RO
BitField Desc: Jn mis-match expect message
BitField Bits: [24]
--------------------------------------*/
#define cAf6_pohcpestssta_Jnmispato_Mask                                                                cBit24
#define cAf6_pohcpestssta_Jnmispato_Shift                                                                   24

/*--------------------------------------
BitField Name: Jnstate
BitField Type: RO
BitField Desc: Jn state machine
BitField Bits: [23:22]
--------------------------------------*/
#define cAf6_pohcpestssta_Jnstate_Mask                                                               cBit23_22
#define cAf6_pohcpestssta_Jnstate_Shift                                                                     22

/*--------------------------------------
BitField Name: JnFrmcnt
BitField Type: RO
BitField Desc: Jn frame counter
BitField Bits: [21:16]
--------------------------------------*/
#define cAf6_pohcpestssta_JnFrmcnt_Mask                                                              cBit21_16
#define cAf6_pohcpestssta_JnFrmcnt_Shift                                                                    16

/*--------------------------------------
BitField Name: Jntimalm
BitField Type: RO
BitField Desc: Jn tim alarm
BitField Bits: [15]
--------------------------------------*/
#define cAf6_pohcpestssta_Jntimalm_Mask                                                                 cBit15
#define cAf6_pohcpestssta_Jntimalm_Shift                                                                    15

/*--------------------------------------
BitField Name: Jnstableoo
BitField Type: RO
BitField Desc: Jn stable
BitField Bits: [14]
--------------------------------------*/
#define cAf6_pohcpestssta_Jnstableoo_Mask                                                               cBit14
#define cAf6_pohcpestssta_Jnstableoo_Shift                                                                  14

/*--------------------------------------
BitField Name: C2stbsta
BitField Type: RO
BitField Desc: C2 stable status
BitField Bits: [12]
--------------------------------------*/
#define cAf6_pohcpestssta_C2stbsta_Mask                                                                 cBit12
#define cAf6_pohcpestssta_C2stbsta_Shift                                                                    12

/*--------------------------------------
BitField Name: C2stbcnt
BitField Type: RO
BitField Desc: C2 stable counter
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_pohcpestssta_C2stbcnt_Mask                                                               cBit11_8
#define cAf6_pohcpestssta_C2stbcnt_Shift                                                                     8

/*--------------------------------------
BitField Name: C2acpt
BitField Type: RO
BitField Desc: C2 accept byte
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_pohcpestssta_C2acpt_Mask                                                                  cBit7_0
#define cAf6_pohcpestssta_C2acpt_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE VT/TU3 Status Register
Reg Addr   : 0x02_C000
Reg Formula: 0x02_C000 + $sliceid*1344 + $stsid*28 +$vtid
    Where  : 
           + $sliceid(0-3): Slice Identification
           + $stsid(0-47): STS Identification
           + $vtid(0-27):Vt Identification
Reg Desc   : 
This register is used to get POH Lo-order status of monitoring.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohcpevtsta_Base                                                                      0x02C000
#define cAf6Reg_pohcpevtsta(sliceid, stsid, vtid)                     (0x02C000+(sliceid)*1344+(stsid)*28+(vtid))

/*--------------------------------------
BitField Name: Jnstbcntoo
BitField Type: RO
BitField Desc: Jn state machine
BitField Bits: [30:27]
--------------------------------------*/
#define cAf6_pohcpevtsta_Jnstbcntoo_Mask                                                             cBit30_27
#define cAf6_pohcpevtsta_Jnstbcntoo_Shift                                                                   27

/*--------------------------------------
BitField Name: Jntimstao
BitField Type: RO
BitField Desc: Jn tim status
BitField Bits: [26]
--------------------------------------*/
#define cAf6_pohcpevtsta_Jntimstao_Mask                                                                 cBit26
#define cAf6_pohcpevtsta_Jntimstao_Shift                                                                    26

/*--------------------------------------
BitField Name: Jnmismsgo
BitField Type: RO
BitField Desc: Jn mis-match current message
BitField Bits: [25]
--------------------------------------*/
#define cAf6_pohcpevtsta_Jnmismsgo_Mask                                                                 cBit25
#define cAf6_pohcpevtsta_Jnmismsgo_Shift                                                                    25

/*--------------------------------------
BitField Name: Jnmispato
BitField Type: RO
BitField Desc: Jn mis-match expect message
BitField Bits: [24]
--------------------------------------*/
#define cAf6_pohcpevtsta_Jnmispato_Mask                                                                 cBit24
#define cAf6_pohcpevtsta_Jnmispato_Shift                                                                    24

/*--------------------------------------
BitField Name: Jnstate
BitField Type: RO
BitField Desc: Jn state machine
BitField Bits: [23:22]
--------------------------------------*/
#define cAf6_pohcpevtsta_Jnstate_Mask                                                                cBit23_22
#define cAf6_pohcpevtsta_Jnstate_Shift                                                                      22

/*--------------------------------------
BitField Name: JnFrmcnt
BitField Type: RO
BitField Desc: Jn frame counter
BitField Bits: [21:16]
--------------------------------------*/
#define cAf6_pohcpevtsta_JnFrmcnt_Mask                                                               cBit21_16
#define cAf6_pohcpevtsta_JnFrmcnt_Shift                                                                     16

/*--------------------------------------
BitField Name: Jntimalm
BitField Type: RO
BitField Desc: Jn tim alarm
BitField Bits: [15]
--------------------------------------*/
#define cAf6_pohcpevtsta_Jntimalm_Mask                                                                  cBit15
#define cAf6_pohcpevtsta_Jntimalm_Shift                                                                     15

/*--------------------------------------
BitField Name: Jnstableoo
BitField Type: RO
BitField Desc: Jn stable
BitField Bits: [14]
--------------------------------------*/
#define cAf6_pohcpevtsta_Jnstableoo_Mask                                                                cBit14
#define cAf6_pohcpevtsta_Jnstableoo_Shift                                                                   14

/*--------------------------------------
BitField Name: Vslstbsta
BitField Type: RO
BitField Desc: VSL stable status
BitField Bits: [13]
--------------------------------------*/
#define cAf6_pohcpevtsta_Vslstbsta_Mask                                                                 cBit13
#define cAf6_pohcpevtsta_Vslstbsta_Shift                                                                    13

/*--------------------------------------
BitField Name: Vslstbcnt
BitField Type: RO
BitField Desc: VSL stable counter
BitField Bits: [12:9]
--------------------------------------*/
#define cAf6_pohcpevtsta_Vslstbcnt_Mask                                                               cBit12_9
#define cAf6_pohcpevtsta_Vslstbcnt_Shift                                                                     9

/*--------------------------------------
BitField Name: Vslacpt
BitField Type: RO
BitField Desc: VSL accept byte
BitField Bits: [8:6]
--------------------------------------*/
#define cAf6_pohcpevtsta_Vslacpt_Mask                                                                  cBit8_6
#define cAf6_pohcpevtsta_Vslacpt_Shift                                                                       6

/*--------------------------------------
BitField Name: RFIstatus
BitField Type: RO
BitField Desc: RFI status
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_pohcpevtsta_RFIstatus_Mask                                                                cBit5_0
#define cAf6_pohcpevtsta_RFIstatus_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE J1 STS Expected Message buffer
Reg Addr   : 0x0F_8000
Reg Formula: 0x0F_8000 + $sliceid*$stsmax+1*$msgmax+1 + $stsid*$msgmax+1 + $msgid
    Where  : 
           + $sliceid(0-3): Slice Identification
           + $stsmax(47-47): STS max
           + $msgmax(7-7): MSG max
           + $stsid(0-stsmax): STS Identification
           + $msgid(0-msgmax): Message ID
Reg Desc   : 
The J1 Expected Message Buffer.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohmsgstsexp_Base                                                                     0x0F8000
#define cAf6Reg_pohmsgstsexp(sliceid, stsmax, msgmax, stsid, msgid)   (0x0F8000+(sliceid)*(stsmax)+1*(msgmax)+1+(stsid)*(msgmax)+1+(msgid))

/*--------------------------------------
BitField Name: J1ExpMsg
BitField Type: RW
BitField Desc: J1 Expected Message
BitField Bits: [63:0]
--------------------------------------*/
#define cAf6_pohmsgstsexp_J1ExpMsg_Mask_01                                                            cBit31_0
#define cAf6_pohmsgstsexp_J1ExpMsg_Shift_01                                                                  0
#define cAf6_pohmsgstsexp_J1ExpMsg_Mask_02                                                            cBit31_0
#define cAf6_pohmsgstsexp_J1ExpMsg_Shift_02                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE J1 STS Current Message buffer
Reg Addr   : 0x0F_A000
Reg Formula: 0x0F_A000 + $sliceid*$stsmax+1*$msgmax+1 + $stsid*$msgmax+1 + $msgid
    Where  : 
           + $sliceid(0-3): Slice Identification
           + $stsmax(47-47): STS max
           + $msgmax(7-7): MSG max
           + $stsid(0-stsmax): STS Identification
           + $msgid(0-msgmax): Message ID
Reg Desc   : 
The J1 Current Message Buffer.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohmsgstscur_Base                                                                     0x0FA000
#define cAf6Reg_pohmsgstscur(sliceid, stsmax, msgmax, stsid, msgid)   (0x0FA000+(sliceid)*(stsmax)+1*(msgmax)+1+(stsid)*(msgmax)+1+(msgid))

/*--------------------------------------
BitField Name: J1CurMsg
BitField Type: RW
BitField Desc: J1 Current Message
BitField Bits: [63:0]
--------------------------------------*/
#define cAf6_pohmsgstscur_J1CurMsg_Mask_01                                                            cBit31_0
#define cAf6_pohmsgstscur_J1CurMsg_Shift_01                                                                  0
#define cAf6_pohmsgstscur_J1CurMsg_Mask_02                                                            cBit31_0
#define cAf6_pohmsgstscur_J1CurMsg_Shift_02                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE J2 Expected Message buffer
Reg Addr   : 0x08_0000
Reg Formula: 0x08_0000 + $sliceid*$stsmax+1*$vtmax+1*$msgmax+1 + $stsid*$vtmax+1*$msgmax+1 + $vtid*$msgmax+1 + $msgid
    Where  : 
           + $sliceid(0-3): Slice Identification
           + $stsmax(47-47): STS max
           + $vtmax(27-27): VT max
           + $msgmax(7-7): MSG max
           + $stsid(0-stsmax): STS Identification
           + $vtid(0-vtmax): VT Identification
           + $msgid(0-msgmax): Message ID
Reg Desc   : 
The J2 Expected Message Buffer.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohmsgvtexp_Base                                                                      0x080000
#define cAf6Reg_pohmsgvtexp(sliceid, stsmax, vtmax, msgmax, stsid, vtid, msgid)(0x080000+(sliceid)*(stsmax)+1*(vtmax)+1*(msgmax)+1+(stsid)*(vtmax)+1*(msgmax)+1+(vtid)*(msgmax)+1+(msgid))

/*--------------------------------------
BitField Name: J2ExpMsg
BitField Type: RW
BitField Desc: J2 Expected Message
BitField Bits: [63:0]
--------------------------------------*/
#define cAf6_pohmsgvtexp_J2ExpMsg_Mask_01                                                             cBit31_0
#define cAf6_pohmsgvtexp_J2ExpMsg_Shift_01                                                                   0
#define cAf6_pohmsgvtexp_J2ExpMsg_Mask_02                                                             cBit31_0
#define cAf6_pohmsgvtexp_J2ExpMsg_Shift_02                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE J2 Current Message buffer
Reg Addr   : 0x0A_0000
Reg Formula: 0x0A_0000 + $sliceid*$stsmax+1*$vtmax+1*$msgmax+1 + $stsid*$vtmax+1*$msgmax+1 + $vtid*$msgmax+1 + $msgid
    Where  : 
           + $sliceid(0-3): Slice Identification
           + $stsmax(47-47): STS max
           + $vtmax(27-27): VT max
           + $msgmax(7-7): MSG max
           + $stsid(0-stsmax): STS Identification
           + $vtid(0-vtmax): VT Identification
           + $msgid(0-msgmax): Message ID
Reg Desc   : 
The J2 Current Message Buffer.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohmsgvtcur_Base                                                                      0x0A0000
#define cAf6Reg_pohmsgvtcur(sliceid, stsmax, vtmax, msgmax, stsid, vtid, msgid)(0x0A0000+(sliceid)*(stsmax)+1*(vtmax)+1*(msgmax)+1+(stsid)*(vtmax)+1*(msgmax)+1+(vtid)*(msgmax)+1+(msgid))

/*--------------------------------------
BitField Name: J2CurMsg
BitField Type: RW
BitField Desc: J2 Current Message
BitField Bits: [63:0]
--------------------------------------*/
#define cAf6_pohmsgvtcur_J2CurMsg_Mask_01                                                             cBit31_0
#define cAf6_pohmsgvtcur_J2CurMsg_Shift_01                                                                   0
#define cAf6_pohmsgvtcur_J2CurMsg_Mask_02                                                             cBit31_0
#define cAf6_pohmsgvtcur_J2CurMsg_Shift_02                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE J1 Insert Message buffer
Reg Addr   : 0x0F_C000
Reg Formula: 0x0F_C000 + $sliceid*$stsmax+1*$msgmax+1 + $stsid*$msgmax+1 + $msgid
    Where  : 
           + $sliceid(0-3): Slice Identification
           + $stsmax(47-47): STS max
           + $msgmax(7-7): MSG max
           + $stsid(0-stsmax): STS Identification
           + $msgid(0-msgmax): Message ID
Reg Desc   : 
The J1 Current Message Buffer.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohmsgstsins_Base                                                                     0x0FC000
#define cAf6Reg_pohmsgstsins(sliceid, stsmax, msgmax, stsid, msgid)   (0x0FC000+(sliceid)*(stsmax)+1*(msgmax)+1+(stsid)*(msgmax)+1+(msgid))

/*--------------------------------------
BitField Name: J1InsMsg
BitField Type: RW
BitField Desc: J1 Insert Message
BitField Bits: [63:0]
--------------------------------------*/
#define cAf6_pohmsgstsins_J1InsMsg_Mask_01                                                            cBit31_0
#define cAf6_pohmsgstsins_J1InsMsg_Shift_01                                                                  0
#define cAf6_pohmsgstsins_J1InsMsg_Mask_02                                                            cBit31_0
#define cAf6_pohmsgstsins_J1InsMsg_Shift_02                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE J2 Insert Message buffer
Reg Addr   : 0x0C_0000
Reg Formula: 0x0C_0000 + $sliceid*$stsmax+1*$vtmax+1*$msgmax+1 + $stsid*$vtmax+1*$msgmax+1 + $vtid*$msgmax+1 + $msgid
    Where  : 
           + $sliceid(0-3): Slice Identification
           + $stsmax(47-47): STS max
           + $vtmax(27-27): VT max
           + $msgmax(7-7): MSG max
           + $stsid(0-stsmax): STS Identification
           + $vtid(0-vtmax): VT Identification
           + $msgid(0-msgmax): Message ID
Reg Desc   : 
The J2 Insert Message Buffer.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohmsgvtins_Base                                                                      0x0C0000
#define cAf6Reg_pohmsgvtins(sliceid, stsmax, vtmax, msgmax, stsid, vtid, msgid)(0x0C0000+(sliceid)*(stsmax)+1*(vtmax)+1*(msgmax)+1+(stsid)*(vtmax)+1*(msgmax)+1+(vtid)*(msgmax)+1+(msgid))

/*--------------------------------------
BitField Name: J2InsMsg
BitField Type: RW
BitField Desc: J2 Insert Message
BitField Bits: [63:0]
--------------------------------------*/
#define cAf6_pohmsgvtins_J2InsMsg_Mask_01                                                             cBit31_0
#define cAf6_pohmsgvtins_J2InsMsg_Shift_01                                                                   0
#define cAf6_pohmsgvtins_J2InsMsg_Mask_02                                                             cBit31_0
#define cAf6_pohmsgvtins_J2InsMsg_Shift_02                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : POH Termintate Insert Control STS
Reg Addr   : 0x04_0400
Reg Formula: 0x04_0400 + $STS + $OCID*$stsmax+1
    Where  : 
           + $stsmax(47-47): STS max
           + $STS(0-stsmax)  : STS
           + $OCID(0-3)  : Line ID
Reg Desc   : 
This register is used to control STS POH insert .

------------------------------------------------------------------------------*/
#define cAf6Reg_ter_ctrlhi_Base                                                                       0x040400
#define cAf6Reg_ter_ctrlhi(stsmax, STS, OCID)                               (0x040400+(STS)+(OCID)*(stsmax)+1)

/*--------------------------------------
BitField Name: rdien
BitField Type: RW
BitField Desc: 1: send old version RDI code G1 bit6,7 : send ERDI version code
G1 bit6,7
BitField Bits: [8]
--------------------------------------*/
#define cAf6_ter_ctrlhi_rdien_Mask                                                                       cBit8
#define cAf6_ter_ctrlhi_rdien_Shift                                                                          8

/*--------------------------------------
BitField Name: autoreidis
BitField Type: RW
BitField Desc: 1: auto insert rei disable, 0: auto insert rei enable
BitField Bits: [7]
--------------------------------------*/
#define cAf6_ter_ctrlhi_autoreidis_Mask                                                                  cBit7
#define cAf6_ter_ctrlhi_autoreidis_Shift                                                                     7

/*--------------------------------------
BitField Name: g1spare
BitField Type: RW
BitField Desc: G1 spare value
BitField Bits: [6]
--------------------------------------*/
#define cAf6_ter_ctrlhi_g1spare_Mask                                                                     cBit6
#define cAf6_ter_ctrlhi_g1spare_Shift                                                                        6

/*--------------------------------------
BitField Name: plm
BitField Type: RW
BitField Desc: 0 : Enable, 1: Disable send ERDI if PLM detected
BitField Bits: [5]
--------------------------------------*/
#define cAf6_ter_ctrlhi_plm_Mask                                                                         cBit5
#define cAf6_ter_ctrlhi_plm_Shift                                                                            5

/*--------------------------------------
BitField Name: uneq
BitField Type: RW
BitField Desc: 0 : Enable, 1: Disable send ERDI if UNEQ detected
BitField Bits: [4]
--------------------------------------*/
#define cAf6_ter_ctrlhi_uneq_Mask                                                                        cBit4
#define cAf6_ter_ctrlhi_uneq_Shift                                                                           4

/*--------------------------------------
BitField Name: timmsk
BitField Type: RW
BitField Desc: 0 : Enable, 1: Disable send ERDI if TIM detected
BitField Bits: [3]
--------------------------------------*/
#define cAf6_ter_ctrlhi_timmsk_Mask                                                                      cBit3
#define cAf6_ter_ctrlhi_timmsk_Shift                                                                         3

/*--------------------------------------
BitField Name: aislopmsk
BitField Type: RW
BitField Desc: 0 : Enable, 1: Disable send ERDI if AIS,LOP detected
BitField Bits: [2]
--------------------------------------*/
#define cAf6_ter_ctrlhi_aislopmsk_Mask                                                                   cBit2
#define cAf6_ter_ctrlhi_aislopmsk_Shift                                                                      2

/*--------------------------------------
BitField Name: jnfrmd
BitField Type: RW
BitField Desc: 0:1 byte, 1: 16/64byte
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_ter_ctrlhi_jnfrmd_Mask                                                                    cBit1_0
#define cAf6_ter_ctrlhi_jnfrmd_Shift                                                                         0


/*------------------------------------------------------------------------------
Reg Name   : POH Termintate Insert Control VT/TU3
Reg Addr   : 0x04_4000
Reg Formula: 0x04_4000 + $STS*$vtmax+1 + $OCID*$stsmax+1*$vtmax+1 + $VT
    Where  : 
           + $stsmax(47-47): STS max
           + $vtmax(27-27): VT max
           + $STS(0-stsmax)  : STS
           + $OCID(0-3)  : Line ID
           + $VT(0-vtmax)
Reg Desc   : 
This register is used to control STS POH insert. TU3 is at VT ID = 0. Fields must be the same as ter_ctrlhi

------------------------------------------------------------------------------*/
#define cAf6Reg_ter_ctrllo_Base                                                                       0x044000
#define cAf6Reg_ter_ctrllo(stsmax, vtmax, STS, OCID, VT)              (0x044000+(STS)*(vtmax)+1+(OCID)*(stsmax)+1*(vtmax)+1+(VT))

/*--------------------------------------
BitField Name: rdien
BitField Type: RW
BitField Desc: 1: send old version RDI code Z7 bit5,6,7 : send ERDI version code
Z7 bit5,6,7
BitField Bits: [16]
--------------------------------------*/
#define cAf6_ter_ctrllo_rdien_Mask                                                                      cBit16
#define cAf6_ter_ctrllo_rdien_Shift                                                                         16

/*--------------------------------------
BitField Name: autoreidis
BitField Type: RW
BitField Desc: 1: auto insert rei disable, 0: auto insert rei enable
BitField Bits: [15]
--------------------------------------*/
#define cAf6_ter_ctrllo_autoreidis_Mask                                                                 cBit15
#define cAf6_ter_ctrllo_autoreidis_Shift                                                                    15

/*--------------------------------------
BitField Name: k4b0b1
BitField Type: RW
BitField Desc: K4b0b1 value
BitField Bits: [14:13]
--------------------------------------*/
#define cAf6_ter_ctrllo_k4b0b1_Mask                                                                  cBit14_13
#define cAf6_ter_ctrllo_k4b0b1_Shift                                                                        13

/*--------------------------------------
BitField Name: k4aps
BitField Type: RW
BitField Desc: K4aps value
BitField Bits: [12:11]
--------------------------------------*/
#define cAf6_ter_ctrllo_k4aps_Mask                                                                   cBit12_11
#define cAf6_ter_ctrllo_k4aps_Shift                                                                         11

/*--------------------------------------
BitField Name: k4spare
BitField Type: RW
BitField Desc: K4spare value
BitField Bits: [10]
--------------------------------------*/
#define cAf6_ter_ctrllo_k4spare_Mask                                                                    cBit10
#define cAf6_ter_ctrllo_k4spare_Shift                                                                       10

/*--------------------------------------
BitField Name: rfival
BitField Type: RW
BitField Desc: RFI value
BitField Bits: [9]
--------------------------------------*/
#define cAf6_ter_ctrllo_rfival_Mask                                                                      cBit9
#define cAf6_ter_ctrllo_rfival_Shift                                                                         9

/*--------------------------------------
BitField Name: vslval
BitField Type: RW
BitField Desc: VT signal label value
BitField Bits: [8:6]
--------------------------------------*/
#define cAf6_ter_ctrllo_vslval_Mask                                                                    cBit8_6
#define cAf6_ter_ctrllo_vslval_Shift                                                                         6

/*--------------------------------------
BitField Name: plm
BitField Type: RW
BitField Desc: 0 : Enable, 1: Disable send ERDI if PLM detected
BitField Bits: [5]
--------------------------------------*/
#define cAf6_ter_ctrllo_plm_Mask                                                                         cBit5
#define cAf6_ter_ctrllo_plm_Shift                                                                            5

/*--------------------------------------
BitField Name: uneq
BitField Type: RW
BitField Desc: 0 : Enable, 1: Disable send ERDI if UNEQ detected
BitField Bits: [4]
--------------------------------------*/
#define cAf6_ter_ctrllo_uneq_Mask                                                                        cBit4
#define cAf6_ter_ctrllo_uneq_Shift                                                                           4

/*--------------------------------------
BitField Name: timmsk
BitField Type: RW
BitField Desc: 0 : Enable, 1: Disable send ERDI if TIM detected
BitField Bits: [3]
--------------------------------------*/
#define cAf6_ter_ctrllo_timmsk_Mask                                                                      cBit3
#define cAf6_ter_ctrllo_timmsk_Shift                                                                         3

/*--------------------------------------
BitField Name: aislopmsk
BitField Type: RW
BitField Desc: 0 : Enable, 1: Disable send ERDI if AIS,LOP detected
BitField Bits: [2]
--------------------------------------*/
#define cAf6_ter_ctrllo_aislopmsk_Mask                                                                   cBit2
#define cAf6_ter_ctrllo_aislopmsk_Shift                                                                      2

/*--------------------------------------
BitField Name: jnfrmd
BitField Type: RW
BitField Desc: 0:1 byte, 1: 16/64byte
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_ter_ctrllo_jnfrmd_Mask                                                                    cBit1_0
#define cAf6_ter_ctrllo_jnfrmd_Shift                                                                         0


/*------------------------------------------------------------------------------
Reg Name   : POH Termintate Insert Buffer STS
Reg Addr   : 0x01_0800
Reg Formula: 0x01_0800 + $OCID*256 + $STS*4 + $BGRP
    Where  : 
           + $STS(0-47)  : STS
           + $OCID(0-3)  : Line ID
           + $BGRP(0-3)
Reg Desc   : 
This register is used for storing POH BYTEs inserted to Sonet/SDH. %%
BGRP = 0 : G1,J1  %%
BGRP = 1 : N1,C2  %%
BGRP = 2 : H4,F2  %%
BGRP = 3 : K3,F3

------------------------------------------------------------------------------*/
#define cAf6Reg_rtlpohccterbufhi_Base                                                                 0x010800
#define cAf6Reg_rtlpohccterbufhi(STS, OCID, BGRP)                         (0x010800+(OCID)*256+(STS)*4+(BGRP))

/*--------------------------------------
BitField Name: byte1msk
BitField Type: WO
BitField Desc: Enable/Disable (1/0)write to buffer
BitField Bits: [17]
--------------------------------------*/
#define cAf6_rtlpohccterbufhi_byte1msk_Mask                                                             cBit17
#define cAf6_rtlpohccterbufhi_byte1msk_Shift                                                                17

/*--------------------------------------
BitField Name: byte1
BitField Type: RW
BitField Desc: Byte1 (G1/N1/H4/K3)
BitField Bits: [16:9]
--------------------------------------*/
#define cAf6_rtlpohccterbufhi_byte1_Mask                                                              cBit16_9
#define cAf6_rtlpohccterbufhi_byte1_Shift                                                                    9

/*--------------------------------------
BitField Name: byte0msk
BitField Type: WO
BitField Desc: Enable/Disable (1/0) write to buffer
BitField Bits: [8]
--------------------------------------*/
#define cAf6_rtlpohccterbufhi_byte0msk_Mask                                                              cBit8
#define cAf6_rtlpohccterbufhi_byte0msk_Shift                                                                 8

/*--------------------------------------
BitField Name: byte0
BitField Type: RW
BitField Desc: Byte0 (J1/C2/F2/F3)
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_rtlpohccterbufhi_byte0_Mask                                                               cBit7_0
#define cAf6_rtlpohccterbufhi_byte0_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : POH Termintate Insert Buffer TU3/VT
Reg Addr   : 0x01_8000
Reg Formula: 0x01_8000 + $OCID*4096 + $STS*64 + $VT*2 + $BGRP
    Where  : 
           + $STS(0-47)  : STS
           + $OCID(0-3)  : Line ID
           + $VT(0-27)
           + $BGRP(0-1)
Reg Desc   : 
This register is used for storing POH BYTEs inserted to Sonet/SDH. TU3 is at VT ID = 0,1 %%
For VT %%
BGRP = 0 : J2,V5 %%
BGRP = 1 : K4,N2 %%
For TU3 %%
VT = 0, BGRP = 0 : G1,J1 %%
VT = 0, BGRP = 1 : N1,C2 %%
VT = 1, BGRP = 0 : H4,F2 %%
VT = 1, BGRP = 1 : K3,F3

------------------------------------------------------------------------------*/
#define cAf6Reg_rtlpohccterbuflo_Base                                                                 0x018000
#define cAf6Reg_rtlpohccterbuflo(STS, OCID, VT, BGRP)                 (0x018000+(OCID)*4096+(STS)*64+(VT)*2+(BGRP))

/*--------------------------------------
BitField Name: byte1msk
BitField Type: WO
BitField Desc: Enable/Disable (1/0)write to buffer
BitField Bits: [17]
--------------------------------------*/
#define cAf6_rtlpohccterbuflo_byte1msk_Mask                                                             cBit17
#define cAf6_rtlpohccterbuflo_byte1msk_Shift                                                                17

/*--------------------------------------
BitField Name: byte1
BitField Type: RW
BitField Desc: Byte1 (J2/K4)
BitField Bits: [16:9]
--------------------------------------*/
#define cAf6_rtlpohccterbuflo_byte1_Mask                                                              cBit16_9
#define cAf6_rtlpohccterbuflo_byte1_Shift                                                                    9

/*--------------------------------------
BitField Name: byte0msk
BitField Type: WO
BitField Desc: Enable/Disable (1/0) write to buffer
BitField Bits: [8]
--------------------------------------*/
#define cAf6_rtlpohccterbuflo_byte0msk_Mask                                                              cBit8
#define cAf6_rtlpohccterbuflo_byte0msk_Shift                                                                 8

/*--------------------------------------
BitField Name: byte0
BitField Type: RW
BitField Desc: Byte0 (V5/N2)
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_rtlpohccterbuflo_byte0_Mask                                                               cBit7_0
#define cAf6_rtlpohccterbuflo_byte0_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Global Control
Reg Addr   : 0x06_0000
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to enable STS,VT,DSN globally.

------------------------------------------------------------------------------*/
#define cAf6Reg_pcfg_glbenb                                                                           0x060000

/*--------------------------------------
BitField Name: timerenb
BitField Type: RW
BitField Desc: Enable timer
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pcfg_glbenb_timerenb_Mask                                                                   cBit3
#define cAf6_pcfg_glbenb_timerenb_Shift                                                                      3

/*--------------------------------------
BitField Name: stsenb
BitField Type: RW
BitField Desc: Enable STS/TU3 channel
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pcfg_glbenb_stsenb_Mask                                                                     cBit2
#define cAf6_pcfg_glbenb_stsenb_Shift                                                                        2

/*--------------------------------------
BitField Name: vtenb
BitField Type: RW
BitField Desc: Enable VT channel
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pcfg_glbenb_vtenb_Mask                                                                      cBit1
#define cAf6_pcfg_glbenb_vtenb_Shift                                                                         1

/*--------------------------------------
BitField Name: dsnsenb
BitField Type: RW
BitField Desc: Enable DSN channel
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pcfg_glbenb_dsnsenb_Mask                                                                    cBit0
#define cAf6_pcfg_glbenb_dsnsenb_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Error Sticky
Reg Addr   : 0x06_0001
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to check error in BER engine.

------------------------------------------------------------------------------*/
#define cAf6Reg_stkalarm                                                                              0x060001

/*--------------------------------------
BitField Name: stserr
BitField Type: W1C
BitField Desc: STS error
BitField Bits: [1]
--------------------------------------*/
#define cAf6_stkalarm_stserr_Mask                                                                        cBit1
#define cAf6_stkalarm_stserr_Shift                                                                           1

/*--------------------------------------
BitField Name: vterr
BitField Type: W1C
BitField Desc: VT error
BitField Bits: [0]
--------------------------------------*/
#define cAf6_stkalarm_vterr_Mask                                                                         cBit0
#define cAf6_stkalarm_vterr_Shift                                                                            0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Threshold 1
Reg Addr   : 0x06_2180
Reg Formula: 0x06_2180 + $Rate
    Where  : 
           + $Rate(0-127): STS Rate for rate from STS1,STS3,STS6...STS48,STS192(0-16,47)... VT1.5,VT2,DS1,E1(65,67,69,71).... OC3,OC12,OC48,OC96,OC192(48-52)....
Reg Desc   : 
This register is used to configure threshold of BER level 3.

------------------------------------------------------------------------------*/
#define cAf6Reg_imemrwptrsh1_Base                                                                     0x062180
#define cAf6Reg_imemrwptrsh1(Rate)                                                           (0x062180+(Rate))

/*--------------------------------------
BitField Name: setthres
BitField Type: RW
BitField Desc: SetThreshold,  SDH line with bit field [31:15]
BitField Bits: [18:9]
--------------------------------------*/
#define cAf6_imemrwptrsh1_setthres_Mask                                                               cBit18_9
#define cAf6_imemrwptrsh1_setthres_Shift                                                                     9

/*--------------------------------------
BitField Name: winthres
BitField Type: RW
BitField Desc: WindowThreshold, SDH line with bit field [15:0]
BitField Bits: [8:0]
--------------------------------------*/
#define cAf6_imemrwptrsh1_winthres_Mask                                                                cBit8_0
#define cAf6_imemrwptrsh1_winthres_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Threshold 2
Reg Addr   : 0x06_0400
Reg Formula: 0x06_0400 + $Rate*8 + $Thresloc
    Where  : 
           + $Rate(0-63): STS Rate for rate from STS1,STS3,STS6...STS48,....VT1.5,VT2,DS1,E1....
           + $Thresloc(0-7): Set/Clear/Window threshold for BER level from 4 to 8
Reg Desc   : 
This register is used to configure threshold of BER level 4 to level 8. %%
Rate STS1,STS3,STS6...STS48,STS192 : 0-16,47 %%
Rate Ds3,E3G832,E3G751,DS3Line,E3Line,EC1,DS1Line,E1Line : 17,18,19,20,21,22,24,25 %%
Rate VT1.5,VT2,DS1,E1: 60-63 %%
Rate OC3,OC12,OC48,OC96,OC192 : 48-52 %%
Thresloc = 0:   win_thres5, win_thres6 %%
Thresloc = 1:   win_thres7, win_thres8 %%
Thresloc = 2:   unused    , win_thres4 %%
Thresloc = 3:   set_thres4  clr_thres4 %%
Thresloc = 4:   set_thres5  clr_thres5 %%
Thresloc = 5:   set_thres6  clr_thres6 %%
Thresloc = 6:   set_thres7  clr_thres7 %%
Thresloc = 7:   set_thres8  clr_thres8 .

------------------------------------------------------------------------------*/
#define cAf6Reg_imemrwptrsh2_Base                                                                     0x060400
#define cAf6Reg_imemrwptrsh2(Rate, Thresloc)                                    (0x060400+(Rate)*8+(Thresloc))
#define cAf6Reg_imemrwptrsh2_WidthVal                                                                       64

/*--------------------------------------
BitField Name: scwthres1
BitField Type: RW
BitField Desc: Set/Clear/Window Threshold
BitField Bits: [33:17]
--------------------------------------*/
#define cAf6_imemrwptrsh2_scwthres1_Mask_01                                                          cBit31_17
#define cAf6_imemrwptrsh2_scwthres1_Shift_01                                                                17
#define cAf6_imemrwptrsh2_scwthres1_Mask_02                                                            cBit1_0
#define cAf6_imemrwptrsh2_scwthres1_Shift_02                                                                 0

/*--------------------------------------
BitField Name: scwthres2
BitField Type: RW
BitField Desc: Set/Clear/Window Threshold
BitField Bits: [16:0]
--------------------------------------*/
#define cAf6_imemrwptrsh2_scwthres2_Mask                                                              cBit16_0
#define cAf6_imemrwptrsh2_scwthres2_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Control VT/DSN
Reg Addr   : 0x06_2000
Reg Formula: 0x06_2000 + $STS*8 + $OCID*512+ $VTG
    Where  : 
           + $STS(0-47): STS
           + $OCID(0-15)  : Line ID, 0-7: VT, 8-15: DE1
           + $VTG(0-6)   : VT group
Reg Desc   : 
This register is used to enable and set threshold SD SF .

------------------------------------------------------------------------------*/
#define cAf6Reg_imemrwpctrl1_Base                                                                     0x062000
#define cAf6Reg_imemrwpctrl1(STS, OCID, VTG)                               (0x062000+(STS)*8+(OCID)*512+(VTG))
#define cAf6Reg_imemrwpctrl1_WidthVal                                                                       64

/*--------------------------------------
BitField Name: tcatrsh4
BitField Type: RW
BitField Desc: TCA threshold raise channel 4
BitField Bits: [45:43]
--------------------------------------*/
#define cAf6_imemrwpctrl1_tcatrsh4_Mask                                                              cBit13_11
#define cAf6_imemrwpctrl1_tcatrsh4_Shift                                                                    11

/*--------------------------------------
BitField Name: tcatrsh3
BitField Type: RW
BitField Desc: TCA threshold raise channel 3
BitField Bits: [42:40]
--------------------------------------*/
#define cAf6_imemrwpctrl1_tcatrsh3_Mask                                                               cBit10_8
#define cAf6_imemrwpctrl1_tcatrsh3_Shift                                                                     8

/*--------------------------------------
BitField Name: tcatrsh2
BitField Type: RW
BitField Desc: TCA threshold raise channel 2
BitField Bits: [39:37]
--------------------------------------*/
#define cAf6_imemrwpctrl1_tcatrsh2_Mask                                                                cBit7_5
#define cAf6_imemrwpctrl1_tcatrsh2_Shift                                                                     5

/*--------------------------------------
BitField Name: tcatrsh1
BitField Type: RW
BitField Desc: TCA threshold raise channel 1
BitField Bits: [36:34]
--------------------------------------*/
#define cAf6_imemrwpctrl1_tcatrsh1_Mask                                                                cBit4_2
#define cAf6_imemrwpctrl1_tcatrsh1_Shift                                                                     2

/*--------------------------------------
BitField Name: etype4
BitField Type: RW
BitField Desc: 0: DS1/VT1.5 1: E1/VT2 channel 3
BitField Bits: [31]
--------------------------------------*/
#define cAf6_imemrwpctrl1_etype4_Mask                                                                   cBit31
#define cAf6_imemrwpctrl1_etype4_Shift                                                                      31

/*--------------------------------------
BitField Name: sftrsh4
BitField Type: RW
BitField Desc: SF threshold raise channel 3
BitField Bits: [30:28]
--------------------------------------*/
#define cAf6_imemrwpctrl1_sftrsh4_Mask                                                               cBit30_28
#define cAf6_imemrwpctrl1_sftrsh4_Shift                                                                     28

/*--------------------------------------
BitField Name: sdtrsh4
BitField Type: RW
BitField Desc: SD threshold raise channel 3
BitField Bits: [27:25]
--------------------------------------*/
#define cAf6_imemrwpctrl1_sdtrsh4_Mask                                                               cBit27_25
#define cAf6_imemrwpctrl1_sdtrsh4_Shift                                                                     25

/*--------------------------------------
BitField Name: ena4
BitField Type: RW
BitField Desc: Enable channel 3
BitField Bits: [24]
--------------------------------------*/
#define cAf6_imemrwpctrl1_ena4_Mask                                                                     cBit24
#define cAf6_imemrwpctrl1_ena4_Shift                                                                        24

/*--------------------------------------
BitField Name: etype3
BitField Type: RW
BitField Desc: 0: DS1/VT1.5 1: E1/VT2 channel 2
BitField Bits: [23]
--------------------------------------*/
#define cAf6_imemrwpctrl1_etype3_Mask                                                                   cBit23
#define cAf6_imemrwpctrl1_etype3_Shift                                                                      23

/*--------------------------------------
BitField Name: sftrsh3
BitField Type: RW
BitField Desc: SF threshold raise channel 2
BitField Bits: [22:20]
--------------------------------------*/
#define cAf6_imemrwpctrl1_sftrsh3_Mask                                                               cBit22_20
#define cAf6_imemrwpctrl1_sftrsh3_Shift                                                                     20

/*--------------------------------------
BitField Name: sdtrsh3
BitField Type: RW
BitField Desc: SD threshold raise channel 2
BitField Bits: [19:17]
--------------------------------------*/
#define cAf6_imemrwpctrl1_sdtrsh3_Mask                                                               cBit19_17
#define cAf6_imemrwpctrl1_sdtrsh3_Shift                                                                     17

/*--------------------------------------
BitField Name: ena3
BitField Type: RW
BitField Desc: Enable channel 2
BitField Bits: [16]
--------------------------------------*/
#define cAf6_imemrwpctrl1_ena3_Mask                                                                     cBit16
#define cAf6_imemrwpctrl1_ena3_Shift                                                                        16

/*--------------------------------------
BitField Name: etype2
BitField Type: RW
BitField Desc: 0: DS1/VT1.5 1: E1/VT2 channel 1
BitField Bits: [15]
--------------------------------------*/
#define cAf6_imemrwpctrl1_etype2_Mask                                                                   cBit15
#define cAf6_imemrwpctrl1_etype2_Shift                                                                      15

/*--------------------------------------
BitField Name: sftrsh2
BitField Type: RW
BitField Desc: SF threshold raise channel 1
BitField Bits: [14:12]
--------------------------------------*/
#define cAf6_imemrwpctrl1_sftrsh2_Mask                                                               cBit14_12
#define cAf6_imemrwpctrl1_sftrsh2_Shift                                                                     12

/*--------------------------------------
BitField Name: sdtrsh2
BitField Type: RW
BitField Desc: SD threshold raise channel 1
BitField Bits: [11:9]
--------------------------------------*/
#define cAf6_imemrwpctrl1_sdtrsh2_Mask                                                                cBit11_9
#define cAf6_imemrwpctrl1_sdtrsh2_Shift                                                                      9

/*--------------------------------------
BitField Name: ena2
BitField Type: RW
BitField Desc: Enable channel 1
BitField Bits: [8]
--------------------------------------*/
#define cAf6_imemrwpctrl1_ena2_Mask                                                                      cBit8
#define cAf6_imemrwpctrl1_ena2_Shift                                                                         8

/*--------------------------------------
BitField Name: etype1
BitField Type: RW
BitField Desc: 0: DS1/VT1.5 1: E1/VT2 channel 0
BitField Bits: [7]
--------------------------------------*/
#define cAf6_imemrwpctrl1_etype1_Mask                                                                    cBit7
#define cAf6_imemrwpctrl1_etype1_Shift                                                                       7

/*--------------------------------------
BitField Name: sftrsh1
BitField Type: RW
BitField Desc: SF threshold raise channel 0
BitField Bits: [6:4]
--------------------------------------*/
#define cAf6_imemrwpctrl1_sftrsh1_Mask                                                                 cBit6_4
#define cAf6_imemrwpctrl1_sftrsh1_Shift                                                                      4

/*--------------------------------------
BitField Name: sdtrsh1
BitField Type: RW
BitField Desc: SD threshold raise channel 0
BitField Bits: [3:1]
--------------------------------------*/
#define cAf6_imemrwpctrl1_sdtrsh1_Mask                                                                 cBit3_1
#define cAf6_imemrwpctrl1_sdtrsh1_Shift                                                                      1

/*--------------------------------------
BitField Name: ena1
BitField Type: RW
BitField Desc: Enable channel 0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_imemrwpctrl1_ena1_Mask                                                                      cBit0
#define cAf6_imemrwpctrl1_ena1_Shift                                                                         0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Control STS/TU3
Reg Addr   : 0x06_2007
Reg Formula: 0x06_2007 + $STS*8 + $OCID*512
    Where  : 
           + $STS(0-47)  : STS
           + $OCID(0-15)  : Line ID
Reg Desc   : 
This register is used to enable and set threshold SD SF. %%
BER STS used with OCID 0-7 channel 0  %%
BER EC1 pointer used with OCID - not supported, channel 0  %%
TU3 used with OCID 0-7 channel 1 %%
BER SDH Line used with OCID 8, channel 0%%
BER SDH EC1 framer used with OCID - not supported, channel 0%%
BER DE3 used with OCID 8-15 , channel 1.

------------------------------------------------------------------------------*/
#define cAf6Reg_imemrwpctrl2_Base                                                                     0x062007
#define cAf6Reg_imemrwpctrl2(STS, OCID)                                          (0x062007+(STS)*8+(OCID)*512)
#define cAf6Reg_imemrwpctrl2_WidthVal                                                                       64

/*--------------------------------------
BitField Name: tcatrsh2
BitField Type: RW
BitField Desc: TCA threshold raise channel 1
BitField Bits: [39:37]
--------------------------------------*/
#define cAf6_imemrwpctrl2_tcatrsh2_Mask                                                                cBit7_5
#define cAf6_imemrwpctrl2_tcatrsh2_Shift                                                                     5

/*--------------------------------------
BitField Name: tcatrsh1
BitField Type: RW
BitField Desc: TCA threshold raise channel 0
BitField Bits: [36:34]
--------------------------------------*/
#define cAf6_imemrwpctrl2_tcatrsh1_Mask                                                                cBit4_2
#define cAf6_imemrwpctrl2_tcatrsh1_Shift                                                                     2

/*--------------------------------------
BitField Name: rate2
BitField Type: RW
BitField Desc: STS Rate 0-63 type
BitField Bits: [27:21]
--------------------------------------*/
#define cAf6_imemrwpctrl2_rate2_Mask                                                                 cBit27_21
#define cAf6_imemrwpctrl2_rate2_Shift                                                                       21

/*--------------------------------------
BitField Name: sftrsh2
BitField Type: RW
BitField Desc: SF threshold raise channel 1
BitField Bits: [20:18]
--------------------------------------*/
#define cAf6_imemrwpctrl2_sftrsh2_Mask                                                               cBit20_18
#define cAf6_imemrwpctrl2_sftrsh2_Shift                                                                     18

/*--------------------------------------
BitField Name: sdtrsh2
BitField Type: RW
BitField Desc: SD threshold raise channel 1
BitField Bits: [17:15]
--------------------------------------*/
#define cAf6_imemrwpctrl2_sdtrsh2_Mask                                                               cBit17_15
#define cAf6_imemrwpctrl2_sdtrsh2_Shift                                                                     15

/*--------------------------------------
BitField Name: ena2
BitField Type: RW
BitField Desc: Enable channel 1
BitField Bits: [14]
--------------------------------------*/
#define cAf6_imemrwpctrl2_ena2_Mask                                                                     cBit14
#define cAf6_imemrwpctrl2_ena2_Shift                                                                        14

/*--------------------------------------
BitField Name: rate1
BitField Type: RW
BitField Desc: STS Rate 0-63 type
BitField Bits: [13:7]
--------------------------------------*/
#define cAf6_imemrwpctrl2_rate1_Mask                                                                  cBit13_7
#define cAf6_imemrwpctrl2_rate1_Shift                                                                        7

/*--------------------------------------
BitField Name: sftrsh1
BitField Type: RW
BitField Desc: SF threshold raise channel 0
BitField Bits: [6:4]
--------------------------------------*/
#define cAf6_imemrwpctrl2_sftrsh1_Mask                                                                 cBit6_4
#define cAf6_imemrwpctrl2_sftrsh1_Shift                                                                      4

/*--------------------------------------
BitField Name: sdtrsh1
BitField Type: RW
BitField Desc: SD threshold raise channel 0
BitField Bits: [3:1]
--------------------------------------*/
#define cAf6_imemrwpctrl2_sdtrsh1_Mask                                                                 cBit3_1
#define cAf6_imemrwpctrl2_sdtrsh1_Shift                                                                      1

/*--------------------------------------
BitField Name: ena1
BitField Type: RW
BitField Desc: Enable channel 0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_imemrwpctrl2_ena1_Mask                                                                      cBit0
#define cAf6_imemrwpctrl2_ena1_Shift                                                                         0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Report VT/DSN
Reg Addr   : 0x06_8000
Reg Formula: 0x06_8000 + $STS*28 + $OCID*1344 + $VT
    Where  : 
           + $STS(0-47)  : STS
           + $OCID(0-15)     : Line ID, VT:0-7,DSN:8-15
           + $VT(0-27)   : VT/DS1 ID
Reg Desc   : 
This register is used to get current BER rate .

------------------------------------------------------------------------------*/
#define cAf6Reg_ramberratevtds_Base                                                                   0x068000
#define cAf6Reg_ramberratevtds(STS, OCID, VT)                             (0x068000+(STS)*28+(OCID)*1344+(VT))

/*--------------------------------------
BitField Name: hwsta
BitField Type: RW
BitField Desc: Hardware status
BitField Bits: [3]
--------------------------------------*/
#define cAf6_ramberratevtds_hwsta_Mask                                                                   cBit3
#define cAf6_ramberratevtds_hwsta_Shift                                                                      3

/*--------------------------------------
BitField Name: rate
BitField Type: RW
BitField Desc: BER rate 3'd1: BER = 10-3 3'd2: BER = 10-4 3'd3: BER = 10-5 3'd4:
BER = 10-6 3'd5: BER = 10-7 3'd6: BER = 10-8 3'd7: BER = 10-9 3'd0: BER = 10-10
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_ramberratevtds_rate_Mask                                                                  cBit2_0
#define cAf6_ramberratevtds_rate_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Report STS/TU3
Reg Addr   : 0x06_D400
Reg Formula: 0x06_D400 + $STS*2 + $OCID*128 + $TU3TYPE
    Where  : 
           + $STS(0-47)  : STS
           + $OCID(0-15)     : Line ID
           + $TU3TYPE(0-1)  : Type TU3:1, STS:0
Reg Desc   : 
This register is used to get current BER rate. %%
BER STS used with OCID 0-7 TU3TYPE 0  %%
BER EC1 pointer used with OCID - not supported, TU3TYPE 0  %%
TU3 used with OCID 0-7 TU3TYPE 1 %%
BER SDH Line used with OCID 8, TU3TYPE 0%%
BER SDH EC1 framer used with OCID - not supported, TU3TYPE 0%%
BER DE3 used with OCID 8-15 , TU3TYPE 1.

------------------------------------------------------------------------------*/
#define cAf6Reg_ramberrateststu3_Base                                                                 0x06D400
#define cAf6Reg_ramberrateststu3(STS, OCID, TU3TYPE)                   (0x06D400+(STS)*2+(OCID)*128+(TU3TYPE))

/*--------------------------------------
BitField Name: hwsta
BitField Type: RW
BitField Desc: Hardware status
BitField Bits: [3]
--------------------------------------*/
#define cAf6_ramberrateststu3_hwsta_Mask                                                                 cBit3
#define cAf6_ramberrateststu3_hwsta_Shift                                                                    3

/*--------------------------------------
BitField Name: rate
BitField Type: RW
BitField Desc: BER rate 3'd1: BER = 10-3 3'd2: BER = 10-4 3'd3: BER = 10-5 3'd4:
BER = 10-6 3'd5: BER = 10-7 3'd6: BER = 10-8 3'd7: BER = 10-9 3'd0: BER = 10-10
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_ramberrateststu3_rate_Mask                                                                cBit2_0
#define cAf6_ramberrateststu3_rate_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : POH Counter Report STS
Reg Addr   : 0x0F_1500
Reg Formula: 0x0F_1500 + $STS + $OCID*$stsmax+1
    Where  : 
           + $stsmax(47-47): STS max
           + $STS(0-stsmax)  : STS
           + $OCID(0-3)     : Line ID
Reg Desc   : 
This register is used to get POH Counter, Rx SDH pointer increase, decrease counter.

------------------------------------------------------------------------------*/
#define cAf6Reg_ipm_cnthi_Base                                                                        0x0F1500
#define cAf6Reg_ipm_cnthi(stsmax, STS, OCID)                                (0x0F1500+(STS)+(OCID)*(stsmax)+1)

/*--------------------------------------
BitField Name: reicnt
BitField Type: RC
BitField Desc: REI counter
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_ipm_cnthi_reicnt_Mask                                                                   cBit31_16
#define cAf6_ipm_cnthi_reicnt_Shift                                                                         16

/*--------------------------------------
BitField Name: bipcnt
BitField Type: RC
BitField Desc: BIP counter
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_ipm_cnthi_bipcnt_Mask                                                                    cBit15_0
#define cAf6_ipm_cnthi_bipcnt_Shift                                                                          0


/*------------------------------------------------------------------------------
Reg Name   : POH Counter Report TU3/VT
Reg Addr   : 0x0F_0000
Reg Formula: 0x0F_0000 + $STS*$vtmax+1 + $OCID*$stsmax+1*$vtmax+1 + $VT
    Where  : 
           + $stsmax(47-47): STS max
           + $vtmax(27-27): VT max
           + $STS(0-stsmax)  : STS
           + $OCID(0-3))   : Line ID
           + $VT(0-vtmax)   : VT ID
Reg Desc   : 
This register is used to get POH Counter, Rx SDH pointer increase, decrease counter.

------------------------------------------------------------------------------*/
#define cAf6Reg_ipm_cntlo_Base                                                                        0x0F0000
#define cAf6Reg_ipm_cntlo(stsmax, vtmax, STS, OCID, VT)               (0x0F0000+(STS)*(vtmax)+1+(OCID)*(stsmax)+1*(vtmax)+1+(VT))

/*--------------------------------------
BitField Name: reicnt
BitField Type: RC
BitField Desc: REI counter
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_ipm_cntlo_reicnt_Mask                                                                   cBit31_16
#define cAf6_ipm_cntlo_reicnt_Shift                                                                         16

/*--------------------------------------
BitField Name: bipcnt
BitField Type: RC
BitField Desc: BIP counter
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_ipm_cntlo_bipcnt_Mask                                                                    cBit15_0
#define cAf6_ipm_cntlo_bipcnt_Shift                                                                          0


/*------------------------------------------------------------------------------
Reg Name   : POH Alarm Status Mask Report STS
Reg Addr   : 0x0F_4000
Reg Formula: 0x0F_4000 + $STS*$SINTOFF + $OCID
    Where  : 
           + $STS(0-47)  : STS
           + $OCID(0-8) : Line ID
           + $SINTOFF(16-16): sts interrupt offset
Reg Desc   : 
This register is used to get POH alarm mask report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_mskhi_Base                                                                        0x0F4000
#define cAf6Reg_alm_mskhi(STS, OCID, SINTOFF)                                (0x0F4000+(STS)*(SINTOFF)+(OCID))

/*--------------------------------------
BitField Name: rdimsk
BitField Type: RW
BitField Desc: rdi mask
BitField Bits: [14]
--------------------------------------*/
#define cAf6_alm_mskhi_rdimsk_Mask                                                                      cBit14
#define cAf6_alm_mskhi_rdimsk_Shift                                                                         14

/*--------------------------------------
BitField Name: jnstachgmsk
BitField Type: RW
BitField Desc: pslstachg mask
BitField Bits: [13]
--------------------------------------*/
#define cAf6_alm_mskhi_jnstachgmsk_Mask                                                                 cBit13
#define cAf6_alm_mskhi_jnstachgmsk_Shift                                                                    13

/*--------------------------------------
BitField Name: pslstachgmsk
BitField Type: RW
BitField Desc: jnstachg mask
BitField Bits: [12]
--------------------------------------*/
#define cAf6_alm_mskhi_pslstachgmsk_Mask                                                                cBit12
#define cAf6_alm_mskhi_pslstachgmsk_Shift                                                                   12

/*--------------------------------------
BitField Name: bersdmsk
BitField Type: RW
BitField Desc: bersd mask
BitField Bits: [11]
--------------------------------------*/
#define cAf6_alm_mskhi_bersdmsk_Mask                                                                    cBit11
#define cAf6_alm_mskhi_bersdmsk_Shift                                                                       11

/*--------------------------------------
BitField Name: bersfmsk
BitField Type: RW
BitField Desc: bersf  mask
BitField Bits: [10]
--------------------------------------*/
#define cAf6_alm_mskhi_bersfmsk_Mask                                                                    cBit10
#define cAf6_alm_mskhi_bersfmsk_Shift                                                                       10

/*--------------------------------------
BitField Name: erdimsk
BitField Type: RW
BitField Desc: erdis mask
BitField Bits: [9]
--------------------------------------*/
#define cAf6_alm_mskhi_erdimsk_Mask                                                                      cBit9
#define cAf6_alm_mskhi_erdimsk_Shift                                                                         9

/*--------------------------------------
BitField Name: bertcamsk
BitField Type: RW
BitField Desc: bertca mask
BitField Bits: [8]
--------------------------------------*/
#define cAf6_alm_mskhi_bertcamsk_Mask                                                                    cBit8
#define cAf6_alm_mskhi_bertcamsk_Shift                                                                       8

/*--------------------------------------
BitField Name: erdicmsk
BitField Type: RW
BitField Desc: erdicmsk  mask
BitField Bits: [7]
--------------------------------------*/
#define cAf6_alm_mskhi_erdicmsk_Mask                                                                     cBit7
#define cAf6_alm_mskhi_erdicmsk_Shift                                                                        7

/*--------------------------------------
BitField Name: erdipmsk
BitField Type: RW
BitField Desc: erdipmsk  mask
BitField Bits: [6]
--------------------------------------*/
#define cAf6_alm_mskhi_erdipmsk_Mask                                                                     cBit6
#define cAf6_alm_mskhi_erdipmsk_Shift                                                                        6

/*--------------------------------------
BitField Name: rfimsk
BitField Type: RW
BitField Desc: rfi/lom mask
BitField Bits: [5]
--------------------------------------*/
#define cAf6_alm_mskhi_rfimsk_Mask                                                                       cBit5
#define cAf6_alm_mskhi_rfimsk_Shift                                                                          5

/*--------------------------------------
BitField Name: timmsk
BitField Type: RW
BitField Desc: tim mask
BitField Bits: [4]
--------------------------------------*/
#define cAf6_alm_mskhi_timmsk_Mask                                                                       cBit4
#define cAf6_alm_mskhi_timmsk_Shift                                                                          4

/*--------------------------------------
BitField Name: uneqmsk
BitField Type: RW
BitField Desc: uneq mask
BitField Bits: [3]
--------------------------------------*/
#define cAf6_alm_mskhi_uneqmsk_Mask                                                                      cBit3
#define cAf6_alm_mskhi_uneqmsk_Shift                                                                         3

/*--------------------------------------
BitField Name: plmmsk
BitField Type: RW
BitField Desc: plm mask
BitField Bits: [2]
--------------------------------------*/
#define cAf6_alm_mskhi_plmmsk_Mask                                                                       cBit2
#define cAf6_alm_mskhi_plmmsk_Shift                                                                          2

/*--------------------------------------
BitField Name: aismsk
BitField Type: RW
BitField Desc: ais mask
BitField Bits: [1]
--------------------------------------*/
#define cAf6_alm_mskhi_aismsk_Mask                                                                       cBit1
#define cAf6_alm_mskhi_aismsk_Shift                                                                          1

/*--------------------------------------
BitField Name: lopmsk
BitField Type: RW
BitField Desc: lop mask
BitField Bits: [0]
--------------------------------------*/
#define cAf6_alm_mskhi_lopmsk_Mask                                                                       cBit0
#define cAf6_alm_mskhi_lopmsk_Shift                                                                          0


/*------------------------------------------------------------------------------
Reg Name   : POH Alarm Status Report STS
Reg Addr   : 0x0F_4800
Reg Formula: 0x0F_4800 + $STS*$SINTOFF + $OCID
    Where  : 
           + $STS(0-47)  : STS
           + $OCID(0-8) : Line ID
           + $SINTOFF(16-16): sts interrupt offset
Reg Desc   : 
This register is used to get POH alarm status report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_stahi_Base                                                                        0x0F4800
#define cAf6Reg_alm_stahi(STS, OCID, SINTOFF)                                (0x0F4800+(STS)*(SINTOFF)+(OCID))

/*--------------------------------------
BitField Name: rdista
BitField Type: RO
BitField Desc: rdi  status
BitField Bits: [14]
--------------------------------------*/
#define cAf6_alm_stahi_rdista_Mask                                                                      cBit14
#define cAf6_alm_stahi_rdista_Shift                                                                         14

/*--------------------------------------
BitField Name: bersdsta
BitField Type: RO
BitField Desc: bersd  status
BitField Bits: [11]
--------------------------------------*/
#define cAf6_alm_stahi_bersdsta_Mask                                                                    cBit11
#define cAf6_alm_stahi_bersdsta_Shift                                                                       11

/*--------------------------------------
BitField Name: bersfsta
BitField Type: RO
BitField Desc: bersf  status
BitField Bits: [10]
--------------------------------------*/
#define cAf6_alm_stahi_bersfsta_Mask                                                                    cBit10
#define cAf6_alm_stahi_bersfsta_Shift                                                                       10

/*--------------------------------------
BitField Name: erdista
BitField Type: RO
BitField Desc: erdis  status
BitField Bits: [9]
--------------------------------------*/
#define cAf6_alm_stahi_erdista_Mask                                                                      cBit9
#define cAf6_alm_stahi_erdista_Shift                                                                         9

/*--------------------------------------
BitField Name: bertcasta
BitField Type: RO
BitField Desc: bertca status
BitField Bits: [8]
--------------------------------------*/
#define cAf6_alm_stahi_bertcasta_Mask                                                                    cBit8
#define cAf6_alm_stahi_bertcasta_Shift                                                                       8

/*--------------------------------------
BitField Name: erdicsta
BitField Type: RO
BitField Desc: erdic status
BitField Bits: [7]
--------------------------------------*/
#define cAf6_alm_stahi_erdicsta_Mask                                                                     cBit7
#define cAf6_alm_stahi_erdicsta_Shift                                                                        7

/*--------------------------------------
BitField Name: erdipsta
BitField Type: RO
BitField Desc: erdip stable status
BitField Bits: [6]
--------------------------------------*/
#define cAf6_alm_stahi_erdipsta_Mask                                                                     cBit6
#define cAf6_alm_stahi_erdipsta_Shift                                                                        6

/*--------------------------------------
BitField Name: rfista
BitField Type: RO
BitField Desc: rfi/lom status
BitField Bits: [5]
--------------------------------------*/
#define cAf6_alm_stahi_rfista_Mask                                                                       cBit5
#define cAf6_alm_stahi_rfista_Shift                                                                          5

/*--------------------------------------
BitField Name: timsta
BitField Type: RO
BitField Desc: tim status
BitField Bits: [4]
--------------------------------------*/
#define cAf6_alm_stahi_timsta_Mask                                                                       cBit4
#define cAf6_alm_stahi_timsta_Shift                                                                          4

/*--------------------------------------
BitField Name: uneqsta
BitField Type: RO
BitField Desc: uneq status
BitField Bits: [3]
--------------------------------------*/
#define cAf6_alm_stahi_uneqsta_Mask                                                                      cBit3
#define cAf6_alm_stahi_uneqsta_Shift                                                                         3

/*--------------------------------------
BitField Name: plmsta
BitField Type: RO
BitField Desc: plm status
BitField Bits: [2]
--------------------------------------*/
#define cAf6_alm_stahi_plmsta_Mask                                                                       cBit2
#define cAf6_alm_stahi_plmsta_Shift                                                                          2

/*--------------------------------------
BitField Name: aissta
BitField Type: RO
BitField Desc: ais status
BitField Bits: [1]
--------------------------------------*/
#define cAf6_alm_stahi_aissta_Mask                                                                       cBit1
#define cAf6_alm_stahi_aissta_Shift                                                                          1

/*--------------------------------------
BitField Name: lopsta
BitField Type: RO
BitField Desc: lop status
BitField Bits: [0]
--------------------------------------*/
#define cAf6_alm_stahi_lopsta_Mask                                                                       cBit0
#define cAf6_alm_stahi_lopsta_Shift                                                                          0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt Status Report STS
Reg Addr   : 0x0F_4400
Reg Formula: 0x0F_4400 + $STS*$SINTOFF + $OCID
    Where  : 
           + $STS(0-47)  : STS
           + $OCID(0-8) : Line ID
           + $SINTOFF(16-16): sts interrupt offset
Reg Desc   : 
This register is used to get POH alarm change status report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_chghi_Base                                                                        0x0F4400
#define cAf6Reg_alm_chghi(STS, OCID, SINTOFF)                                (0x0F4400+(STS)*(SINTOFF)+(OCID))

/*--------------------------------------
BitField Name: rdistachg
BitField Type: W1C
BitField Desc: rdi  status change
BitField Bits: [14]
--------------------------------------*/
#define cAf6_alm_chghi_rdistachg_Mask                                                                   cBit14
#define cAf6_alm_chghi_rdistachg_Shift                                                                      14

/*--------------------------------------
BitField Name: jnstachg
BitField Type: W1C
BitField Desc: jn message change
BitField Bits: [13]
--------------------------------------*/
#define cAf6_alm_chghi_jnstachg_Mask                                                                    cBit13
#define cAf6_alm_chghi_jnstachg_Shift                                                                       13

/*--------------------------------------
BitField Name: pslstachg
BitField Type: W1C
BitField Desc: psl byte change
BitField Bits: [12]
--------------------------------------*/
#define cAf6_alm_chghi_pslstachg_Mask                                                                   cBit12
#define cAf6_alm_chghi_pslstachg_Shift                                                                      12

/*--------------------------------------
BitField Name: bersdstachg
BitField Type: W1C
BitField Desc: bersd stable status change
BitField Bits: [11]
--------------------------------------*/
#define cAf6_alm_chghi_bersdstachg_Mask                                                                 cBit11
#define cAf6_alm_chghi_bersdstachg_Shift                                                                    11

/*--------------------------------------
BitField Name: bersfstachg
BitField Type: W1C
BitField Desc: bersf stable status change
BitField Bits: [10]
--------------------------------------*/
#define cAf6_alm_chghi_bersfstachg_Mask                                                                 cBit10
#define cAf6_alm_chghi_bersfstachg_Shift                                                                    10

/*--------------------------------------
BitField Name: erdistachg
BitField Type: W1C
BitField Desc: erdis status change
BitField Bits: [9]
--------------------------------------*/
#define cAf6_alm_chghi_erdistachg_Mask                                                                   cBit9
#define cAf6_alm_chghi_erdistachg_Shift                                                                      9

/*--------------------------------------
BitField Name: bertcastachg
BitField Type: W1C
BitField Desc: bertca status change
BitField Bits: [8]
--------------------------------------*/
#define cAf6_alm_chghi_bertcastachg_Mask                                                                 cBit8
#define cAf6_alm_chghi_bertcastachg_Shift                                                                    8

/*--------------------------------------
BitField Name: erdicstachg
BitField Type: W1C
BitField Desc: erdic status change
BitField Bits: [7]
--------------------------------------*/
#define cAf6_alm_chghi_erdicstachg_Mask                                                                  cBit7
#define cAf6_alm_chghi_erdicstachg_Shift                                                                     7

/*--------------------------------------
BitField Name: erdipstachg
BitField Type: W1C
BitField Desc: erdip status change
BitField Bits: [6]
--------------------------------------*/
#define cAf6_alm_chghi_erdipstachg_Mask                                                                  cBit6
#define cAf6_alm_chghi_erdipstachg_Shift                                                                     6

/*--------------------------------------
BitField Name: rfistachg
BitField Type: W1C
BitField Desc: rfi/lom status change
BitField Bits: [5]
--------------------------------------*/
#define cAf6_alm_chghi_rfistachg_Mask                                                                    cBit5
#define cAf6_alm_chghi_rfistachg_Shift                                                                       5

/*--------------------------------------
BitField Name: timstachg
BitField Type: W1C
BitField Desc: tim status change
BitField Bits: [4]
--------------------------------------*/
#define cAf6_alm_chghi_timstachg_Mask                                                                    cBit4
#define cAf6_alm_chghi_timstachg_Shift                                                                       4

/*--------------------------------------
BitField Name: uneqstachg
BitField Type: W1C
BitField Desc: uneq status change
BitField Bits: [3]
--------------------------------------*/
#define cAf6_alm_chghi_uneqstachg_Mask                                                                   cBit3
#define cAf6_alm_chghi_uneqstachg_Shift                                                                      3

/*--------------------------------------
BitField Name: plmstachg
BitField Type: W1C
BitField Desc: plm status change
BitField Bits: [2]
--------------------------------------*/
#define cAf6_alm_chghi_plmstachg_Mask                                                                    cBit2
#define cAf6_alm_chghi_plmstachg_Shift                                                                       2

/*--------------------------------------
BitField Name: aisstachg
BitField Type: W1C
BitField Desc: ais status change
BitField Bits: [1]
--------------------------------------*/
#define cAf6_alm_chghi_aisstachg_Mask                                                                    cBit1
#define cAf6_alm_chghi_aisstachg_Shift                                                                       1

/*--------------------------------------
BitField Name: lopstachg
BitField Type: W1C
BitField Desc: lop status change
BitField Bits: [0]
--------------------------------------*/
#define cAf6_alm_chghi_lopstachg_Mask                                                                    cBit0
#define cAf6_alm_chghi_lopstachg_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt Or Status Report STS
Reg Addr   : 0x0F_4C00
Reg Formula: 0x0F_4C00 + $STS 
    Where  : 
           + $STS(0-47)  : STS
Reg Desc   : 
This register is used to get POH alarm or changing status report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_orstahi_Base                                                                      0x0F4C00
#define cAf6Reg_alm_orstahi(STS)                                                              (0x0F4C00+(STS))

/*--------------------------------------
BitField Name: orstachg
BitField Type: RO
BitField Desc: or changing status  bit, each bit is represent for each Line
changing status line8,...line1,line0. Line 8 for debug at Framer level
BitField Bits: [8:0]
--------------------------------------*/
#define cAf6_alm_orstahi_orstachg_Mask                                                                 cBit8_0
#define cAf6_alm_orstahi_orstachg_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt Global Status Report STS
Reg Addr   : 0x0F_4FFE
Reg Formula: 0x0F_4FFE + $GRPID
    Where  : 
           + $GRPID(0-1) : Group ID, each group contain 32 STS divided from 48 STS
Reg Desc   : 
This register is used to get POH alarm global change status report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_glbchghi_Base                                                                     0x0F4FFE
#define cAf6Reg_alm_glbchghi(GRPID)                                                         (0x0F4FFE+(GRPID))

/*--------------------------------------
BitField Name: glbstachg
BitField Type: RO
BitField Desc: global status change bit for 32 STS STS31,...STS1,STS0
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_alm_glbchghi_glbstachg_Mask                                                              cBit31_0
#define cAf6_alm_glbchghi_glbstachg_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt Global Mask Report STS
Reg Addr   : 0x0F_4FFC
Reg Formula: 0x0F_4FFC + $GRPID
    Where  : 
           + $GRPID(0-1) : Group ID, each group contain 32 STS divided from 48 STS
Reg Desc   : 
This register is used to get POH alarm global mask report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_glbmskhi_Base                                                                     0x0F4FFC
#define cAf6Reg_alm_glbmskhi(GRPID)                                                         (0x0F4FFC+(GRPID))

/*--------------------------------------
BitField Name: glbmsk
BitField Type: RW
BitField Desc: global mask for 32 STS STS31,...STS1,STS0
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_alm_glbmskhi_glbmsk_Mask                                                                 cBit31_0
#define cAf6_alm_glbmskhi_glbmsk_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : POH Alarm Status Mask Report VT/TU3
Reg Addr   : 0x0E_0000
Reg Formula: 0x0E_0000 + $STS*32 + $OCID*$VINTOFF + $VTID
    Where  : 
           + $STS(0-47)  : STS
           + $OCID(0-3) : Line ID
           + $VTID(0-27)  : VT ID
           + $VINTOFF(8192-8192): VT interrupt offset
Reg Desc   : 
This register is used to get POH alarm mask report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_msklo_Base                                                                        0x0E0000
#define cAf6Reg_alm_msklo(STS, OCID, VTID, VINTOFF)                   (0x0E0000+(STS)*32+(OCID)*(VINTOFF)+(VTID))

/*--------------------------------------
BitField Name: rdimsk
BitField Type: RW
BitField Desc: rdi mask
BitField Bits: [14]
--------------------------------------*/
#define cAf6_alm_msklo_rdimsk_Mask                                                                      cBit14
#define cAf6_alm_msklo_rdimsk_Shift                                                                         14

/*--------------------------------------
BitField Name: jnstachgmsk
BitField Type: RW
BitField Desc: pslstachg mask
BitField Bits: [13]
--------------------------------------*/
#define cAf6_alm_msklo_jnstachgmsk_Mask                                                                 cBit13
#define cAf6_alm_msklo_jnstachgmsk_Shift                                                                    13

/*--------------------------------------
BitField Name: pslstachgmsk
BitField Type: RW
BitField Desc: jnstachg mask
BitField Bits: [12]
--------------------------------------*/
#define cAf6_alm_msklo_pslstachgmsk_Mask                                                                cBit12
#define cAf6_alm_msklo_pslstachgmsk_Shift                                                                   12

/*--------------------------------------
BitField Name: bersdmsk
BitField Type: RW
BitField Desc: bersd mask
BitField Bits: [11]
--------------------------------------*/
#define cAf6_alm_msklo_bersdmsk_Mask                                                                    cBit11
#define cAf6_alm_msklo_bersdmsk_Shift                                                                       11

/*--------------------------------------
BitField Name: bersfmsk
BitField Type: RW
BitField Desc: bersf  mask
BitField Bits: [10]
--------------------------------------*/
#define cAf6_alm_msklo_bersfmsk_Mask                                                                    cBit10
#define cAf6_alm_msklo_bersfmsk_Shift                                                                       10

/*--------------------------------------
BitField Name: erdimsk
BitField Type: RW
BitField Desc: erdis mask
BitField Bits: [9]
--------------------------------------*/
#define cAf6_alm_msklo_erdimsk_Mask                                                                      cBit9
#define cAf6_alm_msklo_erdimsk_Shift                                                                         9

/*--------------------------------------
BitField Name: bertcamsk
BitField Type: RW
BitField Desc: bertca mask
BitField Bits: [8]
--------------------------------------*/
#define cAf6_alm_msklo_bertcamsk_Mask                                                                    cBit8
#define cAf6_alm_msklo_bertcamsk_Shift                                                                       8

/*--------------------------------------
BitField Name: erdicmsk
BitField Type: RW
BitField Desc: erdic  mask
BitField Bits: [7]
--------------------------------------*/
#define cAf6_alm_msklo_erdicmsk_Mask                                                                     cBit7
#define cAf6_alm_msklo_erdicmsk_Shift                                                                        7

/*--------------------------------------
BitField Name: erdipmsk
BitField Type: RW
BitField Desc: erdip  mask
BitField Bits: [6]
--------------------------------------*/
#define cAf6_alm_msklo_erdipmsk_Mask                                                                     cBit6
#define cAf6_alm_msklo_erdipmsk_Shift                                                                        6

/*--------------------------------------
BitField Name: rfimsk
BitField Type: RW
BitField Desc: rfi mask
BitField Bits: [5]
--------------------------------------*/
#define cAf6_alm_msklo_rfimsk_Mask                                                                       cBit5
#define cAf6_alm_msklo_rfimsk_Shift                                                                          5

/*--------------------------------------
BitField Name: timmsk
BitField Type: RW
BitField Desc: tim mask
BitField Bits: [4]
--------------------------------------*/
#define cAf6_alm_msklo_timmsk_Mask                                                                       cBit4
#define cAf6_alm_msklo_timmsk_Shift                                                                          4

/*--------------------------------------
BitField Name: uneqmsk
BitField Type: RW
BitField Desc: uneq mask
BitField Bits: [3]
--------------------------------------*/
#define cAf6_alm_msklo_uneqmsk_Mask                                                                      cBit3
#define cAf6_alm_msklo_uneqmsk_Shift                                                                         3

/*--------------------------------------
BitField Name: plmmsk
BitField Type: RW
BitField Desc: plm mask
BitField Bits: [2]
--------------------------------------*/
#define cAf6_alm_msklo_plmmsk_Mask                                                                       cBit2
#define cAf6_alm_msklo_plmmsk_Shift                                                                          2

/*--------------------------------------
BitField Name: aismsk
BitField Type: RW
BitField Desc: ais mask
BitField Bits: [1]
--------------------------------------*/
#define cAf6_alm_msklo_aismsk_Mask                                                                       cBit1
#define cAf6_alm_msklo_aismsk_Shift                                                                          1

/*--------------------------------------
BitField Name: lopmsk
BitField Type: RW
BitField Desc: lop mask
BitField Bits: [0]
--------------------------------------*/
#define cAf6_alm_msklo_lopmsk_Mask                                                                       cBit0
#define cAf6_alm_msklo_lopmsk_Shift                                                                          0


/*------------------------------------------------------------------------------
Reg Name   : POH Alarm Status Report VT/TU3
Reg Addr   : 0x0E_1000
Reg Formula: 0x0E_1000 + $STS*32 + $OCID*$VINTOFF + $VTID
    Where  : 
           + $STS(0-47)  : STS
           + $OCID(0-3) : Line ID
           + $VTID(0-27)  : VT ID
           + $VINTOFF(8192-8192): VT interrupt offset
Reg Desc   : 
This register is used to get POH alarm status.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_stalo_Base                                                                        0x0E1000
#define cAf6Reg_alm_stalo(STS, OCID, VTID, VINTOFF)                   (0x0E1000+(STS)*32+(OCID)*(VINTOFF)+(VTID))

/*--------------------------------------
BitField Name: rdista
BitField Type: RO
BitField Desc: rdi  status
BitField Bits: [14]
--------------------------------------*/
#define cAf6_alm_stalo_rdista_Mask                                                                      cBit14
#define cAf6_alm_stalo_rdista_Shift                                                                         14

/*--------------------------------------
BitField Name: bersdsta
BitField Type: RW
BitField Desc: bersd status
BitField Bits: [11]
--------------------------------------*/
#define cAf6_alm_stalo_bersdsta_Mask                                                                    cBit11
#define cAf6_alm_stalo_bersdsta_Shift                                                                       11

/*--------------------------------------
BitField Name: bersfsta
BitField Type: RW
BitField Desc: bersf  status
BitField Bits: [10]
--------------------------------------*/
#define cAf6_alm_stalo_bersfsta_Mask                                                                    cBit10
#define cAf6_alm_stalo_bersfsta_Shift                                                                       10

/*--------------------------------------
BitField Name: erdista
BitField Type: RO
BitField Desc: erdis  status
BitField Bits: [9]
--------------------------------------*/
#define cAf6_alm_stalo_erdista_Mask                                                                      cBit9
#define cAf6_alm_stalo_erdista_Shift                                                                         9

/*--------------------------------------
BitField Name: bertcasta
BitField Type: RO
BitField Desc: bertca status
BitField Bits: [8]
--------------------------------------*/
#define cAf6_alm_stalo_bertcasta_Mask                                                                    cBit8
#define cAf6_alm_stalo_bertcasta_Shift                                                                       8

/*--------------------------------------
BitField Name: erdicsta
BitField Type: RO
BitField Desc: erdic status
BitField Bits: [7]
--------------------------------------*/
#define cAf6_alm_stalo_erdicsta_Mask                                                                     cBit7
#define cAf6_alm_stalo_erdicsta_Shift                                                                        7

/*--------------------------------------
BitField Name: erdipsta
BitField Type: RO
BitField Desc: erdip status
BitField Bits: [6]
--------------------------------------*/
#define cAf6_alm_stalo_erdipsta_Mask                                                                     cBit6
#define cAf6_alm_stalo_erdipsta_Shift                                                                        6

/*--------------------------------------
BitField Name: rfista
BitField Type: RO
BitField Desc: rfi status
BitField Bits: [5]
--------------------------------------*/
#define cAf6_alm_stalo_rfista_Mask                                                                       cBit5
#define cAf6_alm_stalo_rfista_Shift                                                                          5

/*--------------------------------------
BitField Name: timsta
BitField Type: RO
BitField Desc: tim status
BitField Bits: [4]
--------------------------------------*/
#define cAf6_alm_stalo_timsta_Mask                                                                       cBit4
#define cAf6_alm_stalo_timsta_Shift                                                                          4

/*--------------------------------------
BitField Name: uneqsta
BitField Type: RO
BitField Desc: uneq status
BitField Bits: [3]
--------------------------------------*/
#define cAf6_alm_stalo_uneqsta_Mask                                                                      cBit3
#define cAf6_alm_stalo_uneqsta_Shift                                                                         3

/*--------------------------------------
BitField Name: plmsta
BitField Type: RO
BitField Desc: plm status
BitField Bits: [2]
--------------------------------------*/
#define cAf6_alm_stalo_plmsta_Mask                                                                       cBit2
#define cAf6_alm_stalo_plmsta_Shift                                                                          2

/*--------------------------------------
BitField Name: aissta
BitField Type: RO
BitField Desc: ais status
BitField Bits: [1]
--------------------------------------*/
#define cAf6_alm_stalo_aissta_Mask                                                                       cBit1
#define cAf6_alm_stalo_aissta_Shift                                                                          1

/*--------------------------------------
BitField Name: lopsta
BitField Type: RO
BitField Desc: lop status
BitField Bits: [0]
--------------------------------------*/
#define cAf6_alm_stalo_lopsta_Mask                                                                       cBit0
#define cAf6_alm_stalo_lopsta_Shift                                                                          0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt Status Report VT/TU3
Reg Addr   : 0x0E_0800
Reg Formula: 0x0E_0800 + $STS*32 + $OCID*$VINTOFF + $VTID
    Where  : 
           + $STS(0-47)  : STS
           + $OCID(0-3) : Line ID
           + $VTID(0-27)  : VT ID
           + $VINTOFF(8192-8192): VT interrupt offset
Reg Desc   : 
This register is used to get POH alarm change status report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_chglo_Base                                                                        0x0E0800
#define cAf6Reg_alm_chglo(STS, OCID, VTID, VINTOFF)                   (0x0E0800+(STS)*32+(OCID)*(VINTOFF)+(VTID))

/*--------------------------------------
BitField Name: rdistachg
BitField Type: W1C
BitField Desc: rdi  status change
BitField Bits: [14]
--------------------------------------*/
#define cAf6_alm_chglo_rdistachg_Mask                                                                   cBit14
#define cAf6_alm_chglo_rdistachg_Shift                                                                      14

/*--------------------------------------
BitField Name: jnstachg
BitField Type: W1C
BitField Desc: jn message change
BitField Bits: [13]
--------------------------------------*/
#define cAf6_alm_chglo_jnstachg_Mask                                                                    cBit13
#define cAf6_alm_chglo_jnstachg_Shift                                                                       13

/*--------------------------------------
BitField Name: pslstachg
BitField Type: W1C
BitField Desc: psl byte change
BitField Bits: [12]
--------------------------------------*/
#define cAf6_alm_chglo_pslstachg_Mask                                                                   cBit12
#define cAf6_alm_chglo_pslstachg_Shift                                                                      12

/*--------------------------------------
BitField Name: bersdstachg
BitField Type: W1C
BitField Desc: bersd stable status change
BitField Bits: [11]
--------------------------------------*/
#define cAf6_alm_chglo_bersdstachg_Mask                                                                 cBit11
#define cAf6_alm_chglo_bersdstachg_Shift                                                                    11

/*--------------------------------------
BitField Name: bersfstachg
BitField Type: W1C
BitField Desc: bersf stable status change
BitField Bits: [10]
--------------------------------------*/
#define cAf6_alm_chglo_bersfstachg_Mask                                                                 cBit10
#define cAf6_alm_chglo_bersfstachg_Shift                                                                    10

/*--------------------------------------
BitField Name: erdistachg
BitField Type: W1C
BitField Desc: erdis status change
BitField Bits: [9]
--------------------------------------*/
#define cAf6_alm_chglo_erdistachg_Mask                                                                   cBit9
#define cAf6_alm_chglo_erdistachg_Shift                                                                      9

/*--------------------------------------
BitField Name: bertcastachg
BitField Type: W1C
BitField Desc: bertca status change
BitField Bits: [8]
--------------------------------------*/
#define cAf6_alm_chglo_bertcastachg_Mask                                                                 cBit8
#define cAf6_alm_chglo_bertcastachg_Shift                                                                    8

/*--------------------------------------
BitField Name: erdicstachg
BitField Type: W1C
BitField Desc: erdic status change
BitField Bits: [7]
--------------------------------------*/
#define cAf6_alm_chglo_erdicstachg_Mask                                                                  cBit7
#define cAf6_alm_chglo_erdicstachg_Shift                                                                     7

/*--------------------------------------
BitField Name: erdipstachg
BitField Type: W1C
BitField Desc: erdip status change
BitField Bits: [6]
--------------------------------------*/
#define cAf6_alm_chglo_erdipstachg_Mask                                                                  cBit6
#define cAf6_alm_chglo_erdipstachg_Shift                                                                     6

/*--------------------------------------
BitField Name: rfistachg
BitField Type: W1C
BitField Desc: rfi status change
BitField Bits: [5]
--------------------------------------*/
#define cAf6_alm_chglo_rfistachg_Mask                                                                    cBit5
#define cAf6_alm_chglo_rfistachg_Shift                                                                       5

/*--------------------------------------
BitField Name: timstachg
BitField Type: W1C
BitField Desc: tim status change
BitField Bits: [4]
--------------------------------------*/
#define cAf6_alm_chglo_timstachg_Mask                                                                    cBit4
#define cAf6_alm_chglo_timstachg_Shift                                                                       4

/*--------------------------------------
BitField Name: uneqstachg
BitField Type: W1C
BitField Desc: uneq status change
BitField Bits: [3]
--------------------------------------*/
#define cAf6_alm_chglo_uneqstachg_Mask                                                                   cBit3
#define cAf6_alm_chglo_uneqstachg_Shift                                                                      3

/*--------------------------------------
BitField Name: plmstachg
BitField Type: W1C
BitField Desc: plm status change
BitField Bits: [2]
--------------------------------------*/
#define cAf6_alm_chglo_plmstachg_Mask                                                                    cBit2
#define cAf6_alm_chglo_plmstachg_Shift                                                                       2

/*--------------------------------------
BitField Name: aisstachg
BitField Type: W1C
BitField Desc: ais status change
BitField Bits: [1]
--------------------------------------*/
#define cAf6_alm_chglo_aisstachg_Mask                                                                    cBit1
#define cAf6_alm_chglo_aisstachg_Shift                                                                       1

/*--------------------------------------
BitField Name: lopstachg
BitField Type: W1C
BitField Desc: lop status change
BitField Bits: [0]
--------------------------------------*/
#define cAf6_alm_chglo_lopstachg_Mask                                                                    cBit0
#define cAf6_alm_chglo_lopstachg_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt Or Status Report VT/TU3
Reg Addr   : 0x0E_1800
Reg Formula: 0x0E_1800 + $STS + $OCID*64
    Where  : 
           + $STS(0-47)  : STS
           + $OCID(0-3) : Line ID
Reg Desc   : 
This register is used to get POH alarm or status change status report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_orstalo_Base                                                                      0x0E1800
#define cAf6Reg_alm_orstalo(STS, OCID)                                              (0x0E1800+(STS)+(OCID)*64)

/*--------------------------------------
BitField Name: orstachg
BitField Type: RO
BitField Desc: or status change bit
BitField Bits: [27:0]
--------------------------------------*/
#define cAf6_alm_orstalo_orstachg_Mask                                                                cBit27_0
#define cAf6_alm_orstalo_orstachg_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt Global Status Report VT/TU3
Reg Addr   : 0x0E_FFF0
Reg Formula: 0x0E_FFF0 + $GRPID
    Where  : 
           + $GRPID(0-15) : Group ID, each group contain 32 or 16 STS divided from 384 STS of 8 Line Group Even 0,2,4...14 for STS from 0-31. Group Odd 1,3,5...15 for STS from 32-48.}
Reg Desc   : 
This register is used to get POH alarm global change status report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_glbchglo_Base                                                                     0x0EFFF0
#define cAf6Reg_alm_glbchglo(GRPID)                                                         (0x0EFFF0+(GRPID))

/*--------------------------------------
BitField Name: glbstachg
BitField Type: RO
BitField Desc: global status change bit for 32 STS STS31,...STS1,STS0 - Group
Even or 16 STS STS47,STS46,...STS0 - Group Odd.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_alm_glbchglo_glbstachg_Mask                                                              cBit31_0
#define cAf6_alm_glbchglo_glbstachg_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt Global Mask Report VT/TU3
Reg Addr   : 0x0E_FFE0
Reg Formula: 0x0E_FFE0 + $GRPID
    Where  : 
           + $GRPID(0-15) : Group ID, each group contain 32 or 16 STS divided from 384 STS of 8 Line Group Even 0,2,4...14 for STS from 0-31. Group Odd 1,3,5...15 for STS from 32-48.}
Reg Desc   : 
This register is used to get POH alarm global mask report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_glbmsklo_Base                                                                     0x0EFFE0
#define cAf6Reg_alm_glbmsklo(GRPID)                                                         (0x0EFFE0+(GRPID))

/*--------------------------------------
BitField Name: glbmsk
BitField Type: RW
BitField Desc: global status change bit for 32 STS STS31,...STS1,STS0 - Group
Even or 16 STS STS47,STS46,...STS32 - Group Odd.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_alm_glbmsklo_glbmsk_Mask                                                                 cBit31_0
#define cAf6_alm_glbmsklo_glbmsk_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt Global Mask Report
Reg Addr   : 0x00_0004
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to get POH,PDH alarm global mask report for high order.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_glbmskhislice                                                                     0x000004

/*--------------------------------------
BitField Name: glbmskhi
BitField Type: RW
BitField Desc: global mask change bit for high order group -
pdhgrp1,pdhgrp0,pohgrp1,pohgrp0
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_alm_glbmskhislice_glbmskhi_Mask                                                           cBit3_0
#define cAf6_alm_glbmskhislice_glbmskhi_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt Global Mask Report
Reg Addr   : 0x00_0024
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to get POH,PDH alarm global mask report for low order.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_glbmsk1oslice                                                                     0x000024

/*--------------------------------------
BitField Name: glbmsklo
BitField Type: RW
BitField Desc: Global mask bit for low order group -
pdhgrp15...,pdhgrp1,pdhgrp0,pohgrp15...pohgrp1,pohgrp0
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_alm_glbmsk1oslice_glbmsklo_Mask                                                          cBit31_0
#define cAf6_alm_glbmsk1oslice_glbmsklo_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt Global Status Report
Reg Addr   : 0x00_0005
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to get POH,PDH alarm global change status report for high order.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_glbchghislice                                                                     0x000005

/*--------------------------------------
BitField Name: glbstachghi
BitField Type: RO
BitField Desc: global status change bit for high order group -
pdhgrp1,pdhgrp0,pohgrp1,pohgrp0
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_alm_glbchghislice_glbstachghi_Mask                                                        cBit3_0
#define cAf6_alm_glbchghislice_glbstachghi_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt Global Status Report
Reg Addr   : 0x00_0025
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to get POH,PDH alarm global change status report for low order.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_glbchgloslice                                                                     0x000025

/*--------------------------------------
BitField Name: glbstachglo
BitField Type: RO
BitField Desc: global status change bit for low order group -
pdhgrp15...pdhgrp1,pdhgrp0,grp15...grp1,grp0
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_alm_glbchgloslice_glbstachglo_Mask                                                       cBit23_0
#define cAf6_alm_glbchgloslice_glbstachglo_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt  Global Status Out Report HI
Reg Addr   : 0x00_0006
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to get POH,PDH alarm global change status report for high order after ANDED with mask.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_glbchgohi                                                                         0x000006

/*--------------------------------------
BitField Name: glbstachghi
BitField Type: RO
BitField Desc: global status change bit for high order group ,
pdhgrp1,pdhgrp0,pohgrp1,pohgrp0
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_alm_glbchgohi_glbstachghi_Mask                                                            cBit3_0
#define cAf6_alm_glbchgohi_glbstachghi_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt  Global Status Out Report LO
Reg Addr   : 0x00_0026
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to get POH,PDH alarm global change status report for low order after ANDED with mask.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_glbchgolo                                                                         0x000026

/*--------------------------------------
BitField Name: glbstachglo
BitField Type: RO
BitField Desc: global status change bit for low order group ,
pdhgrp15...pdhgrp1,pdhgrp0,grp15...grp1,grp0
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_alm_glbchgolo_glbstachglo_Mask                                                           cBit31_0
#define cAf6_alm_glbchgolo_glbstachglo_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Measured STS/VT Channel Control 1
Reg Addr   : 0x06_0003
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to config measured channel .

------------------------------------------------------------------------------*/
#define cAf6Reg_ramberstscontrl                                                                       0x060003

/*--------------------------------------
BitField Name: chncontrl
BitField Type: RW
BitField Desc: Channel control in STS*2 + OCID*128 + TU3TYPE + 10752
BitField Bits: [14:0]
--------------------------------------*/
#define cAf6_ramberstscontrl_chncontrl_Mask                                                           cBit14_0
#define cAf6_ramberstscontrl_chncontrl_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Measured STS/VT Channel Control 2
Reg Addr   : 0x06_0013
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to config measured channel .

------------------------------------------------------------------------------*/
#define cAf6Reg_ramberstscontrl2                                                                      0x060013

/*--------------------------------------
BitField Name: inputerrdis
BitField Type: RW
BitField Desc: 1 : Disable receive error outside, 0 : Enable receive error
outside
BitField Bits: [20]
--------------------------------------*/
#define cAf6_ramberstscontrl2_inputerrdis_Mask                                                          cBit20
#define cAf6_ramberstscontrl2_inputerrdis_Shift                                                             20

/*--------------------------------------
BitField Name: measuremode
BitField Type: RW
BitField Desc: 1 : Error At BER interface , 0 : Reserve
BitField Bits: [18]
--------------------------------------*/
#define cAf6_ramberstscontrl2_measuremode_Mask                                                          cBit18
#define cAf6_ramberstscontrl2_measuremode_Shift                                                             18

/*--------------------------------------
BitField Name: chncontrl
BitField Type: RW
BitField Desc: Channel control in STS*2 + OCID*128 + TU3TYPE + 10752
BitField Bits: [14:0]
--------------------------------------*/
#define cAf6_ramberstscontrl2_chncontrl_Mask                                                          cBit14_0
#define cAf6_ramberstscontrl2_chncontrl_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Measured Dettetion Time
Reg Addr   : 0x06_0012
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to show Detection time .

------------------------------------------------------------------------------*/
#define cAf6Reg_ramberdettime                                                                         0x060012

/*--------------------------------------
BitField Name: dettime
BitField Type: RC
BitField Desc: Number of 125us frame counted
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_ramberdettime_dettime_Mask                                                               cBit31_0
#define cAf6_ramberdettime_dettime_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Measured Clear Time
Reg Addr   : 0x06_0032
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to show Clear time .

------------------------------------------------------------------------------*/
#define cAf6Reg_rambercleartime                                                                       0x060032

/*--------------------------------------
BitField Name: cleartime
BitField Type: RC
BitField Desc: Number of 125us frame counted
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rambercleartime_cleartime_Mask                                                           cBit31_0
#define cAf6_rambercleartime_cleartime_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : PDH Alarm Status Mask Report DE3
Reg Addr   : 0x06_6000
Reg Formula: 0x06_6000 + $DE3*$SINTOFF + $OCID
    Where  : 
           + $DE3(0-47)  : DE3
           + $OCID(0-3) : Line ID
           + $SINTOFF(16-16): DE3 interrupt offset
Reg Desc   : 
This register is used to get PDH alarm mask report.

------------------------------------------------------------------------------*/
#define cAf6Reg_dealm_mskhi_Base                                                                      0x066000
#define cAf6Reg_dealm_mskhi(DE3, OCID, SINTOFF)                              (0x066000+(DE3)*(SINTOFF)+(OCID))

/*--------------------------------------
BitField Name: bertcamsk
BitField Type: RW
BitField Desc: bertca mask
BitField Bits: [2]
--------------------------------------*/
#define cAf6_dealm_mskhi_bertcamsk_Mask                                                                  cBit2
#define cAf6_dealm_mskhi_bertcamsk_Shift                                                                     2

/*--------------------------------------
BitField Name: bersdmsk
BitField Type: RW
BitField Desc: bersd mask
BitField Bits: [1]
--------------------------------------*/
#define cAf6_dealm_mskhi_bersdmsk_Mask                                                                   cBit1
#define cAf6_dealm_mskhi_bersdmsk_Shift                                                                      1

/*--------------------------------------
BitField Name: bersfmsk
BitField Type: RW
BitField Desc: bersf  mask
BitField Bits: [0]
--------------------------------------*/
#define cAf6_dealm_mskhi_bersfmsk_Mask                                                                   cBit0
#define cAf6_dealm_mskhi_bersfmsk_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : PDH Alarm Status Report DE3
Reg Addr   : 0x06_6800
Reg Formula: 0x06_6800 + $DE3*$SINTOFF + $OCID
    Where  : 
           + $DE3(0-47)  : DE3
           + $OCID(0-3) : Line ID
           + $SINTOFF(16-16): DE3 interrupt offset
Reg Desc   : 
This register is used to get PDH alarm status report.

------------------------------------------------------------------------------*/
#define cAf6Reg_dealm_stahi_Base                                                                      0x066800
#define cAf6Reg_dealm_stahi(DE3, OCID, SINTOFF)                              (0x066800+(DE3)*(SINTOFF)+(OCID))

/*--------------------------------------
BitField Name: bertcasta
BitField Type: RO
BitField Desc: bertca status
BitField Bits: [2]
--------------------------------------*/
#define cAf6_dealm_stahi_bertcasta_Mask                                                                  cBit2
#define cAf6_dealm_stahi_bertcasta_Shift                                                                     2

/*--------------------------------------
BitField Name: bersdsta
BitField Type: RO
BitField Desc: bersd  status
BitField Bits: [1]
--------------------------------------*/
#define cAf6_dealm_stahi_bersdsta_Mask                                                                   cBit1
#define cAf6_dealm_stahi_bersdsta_Shift                                                                      1

/*--------------------------------------
BitField Name: bersfsta
BitField Type: RO
BitField Desc: bersf  status
BitField Bits: [0]
--------------------------------------*/
#define cAf6_dealm_stahi_bersfsta_Mask                                                                   cBit0
#define cAf6_dealm_stahi_bersfsta_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : PDH Interrupt Status Report DE3
Reg Addr   : 0x06_6400
Reg Formula: 0x06_6400 + $DE3*$SINTOFF + $OCID
    Where  : 
           + $DE3(0-47)  : DE3
           + $OCID(0-3) : Line ID
           + $SINTOFF(16-16): DE3 interrupt offset
Reg Desc   : 
This register is used to get PDH alarm change status report.

------------------------------------------------------------------------------*/
#define cAf6Reg_dealm_chghi_Base                                                                      0x066400
#define cAf6Reg_dealm_chghi(DE3, OCID, SINTOFF)                              (0x066400+(DE3)*(SINTOFF)+(OCID))

/*--------------------------------------
BitField Name: bertcastachg
BitField Type: W1C
BitField Desc: bertca status change
BitField Bits: [2]
--------------------------------------*/
#define cAf6_dealm_chghi_bertcastachg_Mask                                                               cBit2
#define cAf6_dealm_chghi_bertcastachg_Shift                                                                  2

/*--------------------------------------
BitField Name: bersdstachg
BitField Type: W1C
BitField Desc: bersd stable status change
BitField Bits: [1]
--------------------------------------*/
#define cAf6_dealm_chghi_bersdstachg_Mask                                                                cBit1
#define cAf6_dealm_chghi_bersdstachg_Shift                                                                   1

/*--------------------------------------
BitField Name: bersfstachg
BitField Type: W1C
BitField Desc: bersf stable status change
BitField Bits: [0]
--------------------------------------*/
#define cAf6_dealm_chghi_bersfstachg_Mask                                                                cBit0
#define cAf6_dealm_chghi_bersfstachg_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : PDH Interrupt Or Status Report DE3
Reg Addr   : 0x06_6C00
Reg Formula: 0x06_6C00 + $DE3 
    Where  : 
           + $DE3(0-47)  : DE3
Reg Desc   : 
This register is used to get PDH alarm or changing status report.

------------------------------------------------------------------------------*/
#define cAf6Reg_dealm_orstahi_Base                                                                    0x066C00
#define cAf6Reg_dealm_orstahi(DE3)                                                            (0x066C00+(DE3))

/*--------------------------------------
BitField Name: orstachg
BitField Type: RO
BitField Desc: or changing status  bit, each bit is represent for each Line
changing status line7,...line1,line0.
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_dealm_orstahi_orstachg_Mask                                                               cBit7_0
#define cAf6_dealm_orstahi_orstachg_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : PDH Interrupt Global Status Report DE3
Reg Addr   : 0x06_6FFE
Reg Formula: 0x06_6FFE + $GRPID
    Where  : 
           + $GRPID(0-1) : Group ID, each group contain 32 DE3 divided from 48 DE3
Reg Desc   : 
This register is used to get PDH alarm global change status report.

------------------------------------------------------------------------------*/
#define cAf6Reg_dealm_glbchghi_Base                                                                   0x066FFE
#define cAf6Reg_dealm_glbchghi(GRPID)                                                       (0x066FFE+(GRPID))

/*--------------------------------------
BitField Name: glbstachg
BitField Type: RO
BitField Desc: global status change bit for 32 DE3 DE331,...DE31,DE30
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_dealm_glbchghi_glbstachg_Mask                                                            cBit31_0
#define cAf6_dealm_glbchghi_glbstachg_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : PDH Interrupt Global Mask Report DE3
Reg Addr   : 0x06_6FFC
Reg Formula: 0x06_6FFC + $GRPID
    Where  : 
           + $GRPID(0-1) : Group ID, each group contain 32 DE3 divided from 48 DE3
Reg Desc   : 
This register is used to get PDH alarm global mask report.

------------------------------------------------------------------------------*/
#define cAf6Reg_dealm_glbmskhi_Base                                                                   0x066FFC
#define cAf6Reg_dealm_glbmskhi(GRPID)                                                       (0x066FFC+(GRPID))

/*--------------------------------------
BitField Name: glbmsk
BitField Type: RW
BitField Desc: global mask for 32 DE3 DE331,...DE31,DE30
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_dealm_glbmskhi_glbmsk_Mask                                                               cBit31_0
#define cAf6_dealm_glbmskhi_glbmsk_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : PDH Alarm Status Mask Report DE1
Reg Addr   : 0x07_0000
Reg Formula: 0x07_0000 + $DE3*32 + $OCID*$VINTOFF + $DE1ID
    Where  : 
           + $DE3(0-47)  : DE3
           + $OCID(0-3) : Line ID
           + $DE1ID(0-27)  : DE1 ID
           + $VINTOFF(8192-8192): DE1 interrupt offset
Reg Desc   : 
This register is used to get PDH alarm mask report.

------------------------------------------------------------------------------*/
#define cAf6Reg_dealm_msklo_Base                                                                      0x070000
#define cAf6Reg_dealm_msklo(DE3, OCID, DE1ID, VINTOFF)                (0x070000+(DE3)*32+(OCID)*(VINTOFF)+(DE1ID))

/*--------------------------------------
BitField Name: bertcamsk
BitField Type: RW
BitField Desc: bertca mask
BitField Bits: [2]
--------------------------------------*/
#define cAf6_dealm_msklo_bertcamsk_Mask                                                                  cBit2
#define cAf6_dealm_msklo_bertcamsk_Shift                                                                     2

/*--------------------------------------
BitField Name: bersdmsk
BitField Type: RW
BitField Desc: bersd mask
BitField Bits: [1]
--------------------------------------*/
#define cAf6_dealm_msklo_bersdmsk_Mask                                                                   cBit1
#define cAf6_dealm_msklo_bersdmsk_Shift                                                                      1

/*--------------------------------------
BitField Name: bersfmsk
BitField Type: RW
BitField Desc: bersf  mask
BitField Bits: [0]
--------------------------------------*/
#define cAf6_dealm_msklo_bersfmsk_Mask                                                                   cBit0
#define cAf6_dealm_msklo_bersfmsk_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : PDH Alarm Status Report DE1
Reg Addr   : 0x07_1000
Reg Formula: 0x07_1000 + $DE3*32 + $OCID*$VINTOFF + $DE1ID
    Where  : 
           + $DE3(0-47)  : DE3
           + $OCID(0-3) : Line ID
           + $DE1ID(0-27)  : DE1 ID
           + $VINTOFF(8192-8192): DE1 interrupt offset
Reg Desc   : 
This register is used to get PDH alarm status.

------------------------------------------------------------------------------*/
#define cAf6Reg_dealm_stalo_Base                                                                      0x071000
#define cAf6Reg_dealm_stalo(DE3, OCID, DE1ID, VINTOFF)                (0x071000+(DE3)*32+(OCID)*(VINTOFF)+(DE1ID))

/*--------------------------------------
BitField Name: bertcasta
BitField Type: RO
BitField Desc: bertca status
BitField Bits: [2]
--------------------------------------*/
#define cAf6_dealm_stalo_bertcasta_Mask                                                                  cBit2
#define cAf6_dealm_stalo_bertcasta_Shift                                                                     2

/*--------------------------------------
BitField Name: bersdsta
BitField Type: RW
BitField Desc: bersd status
BitField Bits: [1]
--------------------------------------*/
#define cAf6_dealm_stalo_bersdsta_Mask                                                                   cBit1
#define cAf6_dealm_stalo_bersdsta_Shift                                                                      1

/*--------------------------------------
BitField Name: bersfsta
BitField Type: RW
BitField Desc: bersf  status
BitField Bits: [0]
--------------------------------------*/
#define cAf6_dealm_stalo_bersfsta_Mask                                                                   cBit0
#define cAf6_dealm_stalo_bersfsta_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : PDH Interrupt Status Report DE1
Reg Addr   : 0x07_0800
Reg Formula: 0x07_0800 + $DE3*32 + $OCID*$VINTOFF + $DE1ID
    Where  : 
           + $DE3(0-47)  : DE3
           + $OCID(0-3) : Line ID
           + $DE1ID(0-27)  : DE1 ID
           + $VINTOFF(8192-8192): DE1 interrupt offset
Reg Desc   : 
This register is used to get PDH alarm change status report.

------------------------------------------------------------------------------*/
#define cAf6Reg_dealm_chglo_Base                                                                      0x070800
#define cAf6Reg_dealm_chglo(DE3, OCID, DE1ID, VINTOFF)                (0x070800+(DE3)*32+(OCID)*(VINTOFF)+(DE1ID))

/*--------------------------------------
BitField Name: bertcastachg
BitField Type: W1C
BitField Desc: bertca status change
BitField Bits: [2]
--------------------------------------*/
#define cAf6_dealm_chglo_bertcastachg_Mask                                                               cBit2
#define cAf6_dealm_chglo_bertcastachg_Shift                                                                  2

/*--------------------------------------
BitField Name: bersdstachg
BitField Type: W1C
BitField Desc: bersd stable status change
BitField Bits: [1]
--------------------------------------*/
#define cAf6_dealm_chglo_bersdstachg_Mask                                                                cBit1
#define cAf6_dealm_chglo_bersdstachg_Shift                                                                   1

/*--------------------------------------
BitField Name: bersfstachg
BitField Type: W1C
BitField Desc: bersf stable status change
BitField Bits: [0]
--------------------------------------*/
#define cAf6_dealm_chglo_bersfstachg_Mask                                                                cBit0
#define cAf6_dealm_chglo_bersfstachg_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : PDH Interrupt Or Status Report DE1
Reg Addr   : 0x07_1800
Reg Formula: 0x07_1800 + $DE3 + $OCID*64
    Where  : 
           + $DE3(0-47)  : DE3
           + $OCID(0-3) : Line ID
Reg Desc   : 
This register is used to get PDH alarm or status change status report.

------------------------------------------------------------------------------*/
#define cAf6Reg_dealm_orstalo_Base                                                                    0x071800
#define cAf6Reg_dealm_orstalo(DE3, OCID)                                            (0x071800+(DE3)+(OCID)*64)

/*--------------------------------------
BitField Name: orstachg
BitField Type: RO
BitField Desc: or status change bit
BitField Bits: [27:0]
--------------------------------------*/
#define cAf6_dealm_orstalo_orstachg_Mask                                                              cBit27_0
#define cAf6_dealm_orstalo_orstachg_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : PDH Interrupt Global Status Report DE1
Reg Addr   : 0x07_FFF0
Reg Formula: 0x07_FFF0 + $GRPID
    Where  : 
           + $GRPID(0-11) : Group ID, each group contain 32 DE3 divided from 384 DE3 of 8 Line
Reg Desc   : 
This register is used to get PDH alarm global change status report.

------------------------------------------------------------------------------*/
#define cAf6Reg_dealm_glbchglo_Base                                                                   0x07FFF0
#define cAf6Reg_dealm_glbchglo(GRPID)                                                       (0x07FFF0+(GRPID))

/*--------------------------------------
BitField Name: glbstachg
BitField Type: RO
BitField Desc: global status change bit for 32 DE3 DE331,...DE31,DE30
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_dealm_glbchglo_glbstachg_Mask                                                            cBit31_0
#define cAf6_dealm_glbchglo_glbstachg_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : PDH Interrupt Global Mask Report DE1
Reg Addr   : 0x07_FFE0
Reg Formula: 0x07_FFE0 + $GRPID
    Where  : 
           + $GRPID(0-11) : Group ID, each group contain 32 DE3 divided from 384 DE3 of 8 Line
Reg Desc   : 
This register is used to get PDH alarm global mask report.

------------------------------------------------------------------------------*/
#define cAf6Reg_dealm_glbmsklo_Base                                                                   0x07FFE0
#define cAf6Reg_dealm_glbmsklo(GRPID)                                                       (0x07FFE0+(GRPID))

/*--------------------------------------
BitField Name: glbmsk
BitField Type: RW
BitField Desc: global status change bit for 32 DE3 DE331,...DE31,DE30
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_dealm_glbmsklo_glbmsk_Mask                                                               cBit31_0
#define cAf6_dealm_glbmsklo_glbmsk_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : POH J0 Overhead Grabber
Reg Addr   : 0x02_00C0
Reg Formula: 0x02_00C0 + $lineid 
    Where  : 
           + $lineid(0-15): Line Identification
Reg Desc   : 
This register is used to grabber Section/Line Overhead

------------------------------------------------------------------------------*/
#define cAf6Reg_pohj0grb_Base                                                                         0x0200C0
#define cAf6Reg_pohj0grb(lineid)                                                           (0x0200C0+(lineid))
#define cAf6Reg_pohj0grb_WidthVal                                                                           96

/*--------------------------------------
BitField Name: ais
BitField Type: RO
BitField Desc: LOS/LOF from OCN
BitField Bits: [64]
--------------------------------------*/
#define cAf6_pohj0grb_ais_Mask                                                                           cBit0
#define cAf6_pohj0grb_ais_Shift                                                                              0

/*--------------------------------------
BitField Name: B2
BitField Type: RO
BitField Desc: B2 LSB counter
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_pohj0grb_B2_Mask                                                                        cBit23_16
#define cAf6_pohj0grb_B2_Shift                                                                              16

/*--------------------------------------
BitField Name: J0
BitField Type: RO
BitField Desc: J0 byte
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_pohj0grb_J0_Mask                                                                          cBit7_0
#define cAf6_pohj0grb_J0_Shift                                                                               0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE J0 Control Register
Reg Addr   : 0x02_A180
Reg Formula: 0x02_A180 + $lineid*2
    Where  : 
           + $lineid(0-15): Line Identification
Reg Desc   : 
This register is used to configure the J0 Monitoring.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohcpej0ctr_Base                                                                      0x02A180
#define cAf6Reg_pohcpej0ctr(lineid)                                                      (0x02A180+(lineid)*2)

/*--------------------------------------
BitField Name: MonEnb
BitField Type: RW
BitField Desc: Alarm monitor enable
BitField Bits: [20]
--------------------------------------*/
#define cAf6_pohcpej0ctr_MonEnb_Mask                                                                    cBit20
#define cAf6_pohcpej0ctr_MonEnb_Shift                                                                       20

/*--------------------------------------
BitField Name: TimDstren
BitField Type: RW
BitField Desc: TimDstren
BitField Bits: [15]
--------------------------------------*/
#define cAf6_pohcpej0ctr_TimDstren_Mask                                                                 cBit15
#define cAf6_pohcpej0ctr_TimDstren_Shift                                                                    15

/*--------------------------------------
BitField Name: TimEnb
BitField Type: RW
BitField Desc: Enable check expect for TIM, Out of frame or unstable alwayse
raise TIM
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pohcpej0ctr_TimEnb_Mask                                                                     cBit3
#define cAf6_pohcpej0ctr_TimEnb_Shift                                                                        3

/*--------------------------------------
BitField Name: J0mode
BitField Type: RW
BitField Desc: 0: 1Byte 1:16Byte 2:64byte 3:Floating
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_pohcpej0ctr_J0mode_Mask                                                                   cBit1_0
#define cAf6_pohcpej0ctr_J0mode_Shift                                                                        0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE J0 Expected Message buffer
Reg Addr   : 0x0F_8600
Reg Formula: 0x0F_8600 + $lineid*$msgmax+1 + $msgid
    Where  : 
           + $msgmax(7-7): MSG max
           + $lineid(0-15): Line Identification
           + $msgid(0-msgmax): Message ID
Reg Desc   : 
The J0 Expected Message Buffer.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohmsgj0exp_Base                                                                      0x0F8600
#define cAf6Reg_pohmsgj0exp(msgmax, lineid, msgid)                      (0x0F8600+(lineid)*(msgmax)+1+(msgid))

/*--------------------------------------
BitField Name: J0ExpMsg
BitField Type: RW
BitField Desc: J0 Expected Message
BitField Bits: [63:0]
--------------------------------------*/
#define cAf6_pohmsgj0exp_J0ExpMsg_Mask_01                                                             cBit31_0
#define cAf6_pohmsgj0exp_J0ExpMsg_Shift_01                                                                   0
#define cAf6_pohmsgj0exp_J0ExpMsg_Mask_02                                                             cBit31_0
#define cAf6_pohmsgj0exp_J0ExpMsg_Shift_02                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE J0 Current Message buffer
Reg Addr   : 0x0F_A600
Reg Formula: 0x0F_A600 + $lineid*$msgmax+1 + $msgid
    Where  : 
           + $msgmax(7-7): MSG max
           + $lineid(0-15): Line Identification
           + $msgid(0-msgmax): Message ID
Reg Desc   : 
The J0 Current Message Buffer.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohmsgj0cur_Base                                                                      0x0FA600
#define cAf6Reg_pohmsgj0cur(msgmax, lineid, msgid)                      (0x0FA600+(lineid)*(msgmax)+1+(msgid))

/*--------------------------------------
BitField Name: J0CurMsg
BitField Type: RW
BitField Desc: J0 Current Message
BitField Bits: [63:0]
--------------------------------------*/
#define cAf6_pohmsgj0cur_J0CurMsg_Mask_01                                                             cBit31_0
#define cAf6_pohmsgj0cur_J0CurMsg_Shift_01                                                                   0
#define cAf6_pohmsgj0cur_J0CurMsg_Mask_02                                                             cBit31_0
#define cAf6_pohmsgj0cur_J0CurMsg_Shift_02                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE STS Status Register
Reg Addr   : 0x02_D5C0
Reg Formula: 0x02_D5C0 + $lineid
    Where  : 
           + $lineid(0-15): Line Identification
Reg Desc   : 
This register is used to get J0 status of monitoring.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohcpej0sta_Base                                                                      0x02D5C0
#define cAf6Reg_pohcpej0sta(lineid)                                                        (0x02D5C0+(lineid))

/*--------------------------------------
BitField Name: Jnstbcntoo
BitField Type: RO
BitField Desc: Jn state machine
BitField Bits: [30:27]
--------------------------------------*/
#define cAf6_pohcpej0sta_Jnstbcntoo_Mask                                                             cBit30_27
#define cAf6_pohcpej0sta_Jnstbcntoo_Shift                                                                   27

/*--------------------------------------
BitField Name: Jntimstao
BitField Type: RO
BitField Desc: Jn tim status
BitField Bits: [26]
--------------------------------------*/
#define cAf6_pohcpej0sta_Jntimstao_Mask                                                                 cBit26
#define cAf6_pohcpej0sta_Jntimstao_Shift                                                                    26

/*--------------------------------------
BitField Name: Jnmismsgo
BitField Type: RO
BitField Desc: Jn mis-match current message
BitField Bits: [25]
--------------------------------------*/
#define cAf6_pohcpej0sta_Jnmismsgo_Mask                                                                 cBit25
#define cAf6_pohcpej0sta_Jnmismsgo_Shift                                                                    25

/*--------------------------------------
BitField Name: Jnmispato
BitField Type: RO
BitField Desc: Jn mis-match expect message
BitField Bits: [24]
--------------------------------------*/
#define cAf6_pohcpej0sta_Jnmispato_Mask                                                                 cBit24
#define cAf6_pohcpej0sta_Jnmispato_Shift                                                                    24

/*--------------------------------------
BitField Name: Jnstate
BitField Type: RO
BitField Desc: Jn state machine
BitField Bits: [23:22]
--------------------------------------*/
#define cAf6_pohcpej0sta_Jnstate_Mask                                                                cBit23_22
#define cAf6_pohcpej0sta_Jnstate_Shift                                                                      22

/*--------------------------------------
BitField Name: JnFrmcnt
BitField Type: RO
BitField Desc: Jn frame counter
BitField Bits: [21:16]
--------------------------------------*/
#define cAf6_pohcpej0sta_JnFrmcnt_Mask                                                               cBit21_16
#define cAf6_pohcpej0sta_JnFrmcnt_Shift                                                                     16

/*--------------------------------------
BitField Name: Jntimalm
BitField Type: RO
BitField Desc: Jn tim alarm
BitField Bits: [15]
--------------------------------------*/
#define cAf6_pohcpej0sta_Jntimalm_Mask                                                                  cBit15
#define cAf6_pohcpej0sta_Jntimalm_Shift                                                                     15

/*--------------------------------------
BitField Name: Jnstableoo
BitField Type: RO
BitField Desc: Jn stable
BitField Bits: [14]
--------------------------------------*/
#define cAf6_pohcpej0sta_Jnstableoo_Mask                                                                cBit14
#define cAf6_pohcpej0sta_Jnstableoo_Shift                                                                   14


/*------------------------------------------------------------------------------
Reg Name   : POH Counter Report B2
Reg Addr   : 0x0F_15C0
Reg Formula: 0x0F_15C0 + $lineid 
    Where  : 
           + $lineid(0-15): Line Identification
Reg Desc   : 
This register is used to get B2 Counter

------------------------------------------------------------------------------*/
#define cAf6Reg_ipm_cntb2_Base                                                                        0x0F15C0
#define cAf6Reg_ipm_cntb2(lineid)                                                          (0x0F15C0+(lineid))

/*--------------------------------------
BitField Name: bipcnt
BitField Type: RC
BitField Desc: BIP counter
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_ipm_cntb2_bipcnt_Mask                                                                    cBit15_0
#define cAf6_ipm_cntb2_bipcnt_Shift                                                                          0

#endif /* _THA60290081MODULEPOHREG_H_ */

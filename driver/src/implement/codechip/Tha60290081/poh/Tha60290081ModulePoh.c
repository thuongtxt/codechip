/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : Tha60290081ModulePoh.c
 *
 * Created Date: Jul 5, 2019
 *
 * Description : 60290081 Module POH implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290021/ocn/Tha60290021ModuleOcn.h"
#include "Tha60290081ModulePoh.h"
#include "Tha60290081ModulePohInternal.h"
#include "Tha60290081ModulePohReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePohMethods         m_ThaModulePohOverride;
static tTha60210011ModulePohMethods m_Tha60210011ModulePohOverride;
static tTha60290021ModulePohMethods m_Tha60290021ModulePohOverride;

/* Save super implementation */
static const tTha60210011ModulePohMethods *m_Tha60210011ModulePohMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 AlmStahiBase(ThaModulePoh self)
    {
    AtUnused(self);
    return cAf6Reg_alm_stahi_Base;
    }

static uint32 AlmChghiBase(ThaModulePoh self)
    {
    AtUnused(self);
    return cAf6Reg_alm_chghi_Base;
    }

static uint32 CpestsctrBase(ThaModulePoh self)
    {
    AtUnused(self);
    return cAf6Reg_pohcpestsctr_Base;
    }

static uint32 CpeStsStatus(ThaModulePoh self)
    {
    AtUnused(self);
    return cAf6Reg_pohcpestssta_Base;
    }

static uint32 IpmCnthiBase(ThaModulePoh self)
    {
    AtUnused(self);
    return cAf6Reg_ipm_cnthi_Base;
    }

static uint32 HoPathInterruptOffset(Tha60210011ModulePoh self, uint8 hwSlice, uint8 hwSts)
    {
    AtUnused(self);
    return (hwSts * 16UL) + hwSlice;
    }

static ThaTtiProcessor FaceplateLineTtiProcessorObjectCreate(Tha60290021ModulePoh self, AtSdhLine line)
    {
    AtUnused(self);
    return Tha60290081FaceplateLineTtiProcessorNew((AtSdhChannel)line);
    }

static Tha60290021ModuleOcn ModuleOcn(ThaModulePoh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return (Tha60290021ModuleOcn)AtDeviceModuleGet(device, cThaModuleOcn);
    }

static eAtRet MonitoringDefaultSet(ThaModulePoh self)
    {
    uint32 numSlices = Tha60290021ModuleOcnNumFaceplateSlices(ModuleOcn(self));
    uint32 slice_i;
    uint32 regAddr = ThaModulePohBaseAddress(self) + cAf6Reg_pcfg_glbcpectr;
    uint32 regVal = 0;

    for (slice_i = 0; slice_i < numSlices; slice_i++)
        {
        uint32 fieldMask, fieldShift;

        /* Enable STS layer */
        fieldMask = cAf6_pcfg_glbcpectr_cpestsline0en_Mask << slice_i;
        fieldShift = slice_i;
        mRegFieldSet(regVal, field, 1);

        /* Enable VT layer */
        fieldMask = cAf6_pcfg_glbcpectr_cpevtline0en_Mask << slice_i;
        fieldShift = slice_i + cAf6_pcfg_glbcpectr_cpevtline0en_Shift;
        mRegFieldSet(regVal, field, 1);
        }

    /* Line layer will start from 4 */
    mRegFieldSet(regVal, cAf6_pcfg_glbcpectr_cpestsline4en_, 1);

    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static void OverrideTha60210011ModulePoh(AtModule self)
    {
    Tha60210011ModulePoh pohModule = (Tha60210011ModulePoh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011ModulePohMethods = mMethodsGet(pohModule);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModulePohOverride, m_Tha60210011ModulePohMethods, sizeof(m_Tha60210011ModulePohOverride));

        mMethodOverride(m_Tha60210011ModulePohOverride, HoPathInterruptOffset);
        }

    mMethodsSet(pohModule, &m_Tha60210011ModulePohOverride);
    }

static void OverrideThaModulePoh(AtModule self)
    {
    ThaModulePoh pohModule = (ThaModulePoh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePohOverride, mMethodsGet(pohModule), sizeof(m_ThaModulePohOverride));

        mMethodOverride(m_ThaModulePohOverride, AlmStahiBase);
        mMethodOverride(m_ThaModulePohOverride, AlmChghiBase);
        mMethodOverride(m_ThaModulePohOverride, CpestsctrBase);
        mMethodOverride(m_ThaModulePohOverride, CpeStsStatus);
        mMethodOverride(m_ThaModulePohOverride, IpmCnthiBase);
        }

    mMethodsSet(pohModule, &m_ThaModulePohOverride);
    }

static void OverrideTha60290021ModulePoh(AtModule self)
    {
    Tha60290021ModulePoh module = (Tha60290021ModulePoh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021ModulePohOverride, mMethodsGet(module), sizeof(m_Tha60290021ModulePohOverride));

        mMethodOverride(m_Tha60290021ModulePohOverride, FaceplateLineTtiProcessorObjectCreate);
        }

    mMethodsSet(module, &m_Tha60290021ModulePohOverride);
    }


static void Override(AtModule self)
    {
    OverrideThaModulePoh(self);
    OverrideTha60210011ModulePoh(self);
    OverrideTha60290021ModulePoh(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081ModulePoh);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022ModulePohObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60290081ModulePohNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

eAtRet Tha60290081ModulePohMonitoringDefaultSet(ThaModulePoh self)
    {
    if (self)
        return MonitoringDefaultSet(self);
    return cAtErrorNullPointer;
   }


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH
 * 
 * File        : Tha60290081ModulePoh.h
 * 
 * Created Date: Jul 11, 2019
 *
 * Description : 60290081 Module POH
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290081MODULEPOH_H_
#define _THA60290081MODULEPOH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/poh/ThaModulePoh.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha60290081ModulePohMonitoringDefaultSet(ThaModulePoh self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290081MODULEPOH_H_ */


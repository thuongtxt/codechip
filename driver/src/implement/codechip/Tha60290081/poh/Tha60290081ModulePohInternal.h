/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH
 * 
 * File        : Tha60290081ModulePohInternal.h
 * 
 * Created Date: Jul 5, 2019
 *
 * Description : 60290081 Module POH representation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290081MODULEPOHINTERNAL_H_
#define _THA60290081MODULEPOHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290022/poh/Tha60290022ModulePohInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290081ModulePoh
    {
    tTha60290022ModulePoh super;
    }tTha60290081ModulePoh;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60290081MODULEPOHINTERNAL_H_ */


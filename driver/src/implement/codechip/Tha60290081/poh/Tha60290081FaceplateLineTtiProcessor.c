/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : Tha60290081FaceplateLineTtiProcessor.c
 *
 * Created Date: Jul 10, 2019
 *
 * Description : Tha60290081FaceplateLineTtiProcessor implementations
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290021/poh/Tha60290021FaceplateLineTtiProcessorInternal.h"
#include "../../Tha60210011/poh/Tha60210011ModulePoh.h"
#include "Tha60290081ModulePohReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290081FaceplateLineTtiProcessor
    {
    tTha60290021FaceplateLineTtiProcessor super;
    }tTha60290081FaceplateLineTtiProcessor;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaSdhLineTtiProcessorV2Methods             m_ThaSdhLineTtiProcessorV2Override;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePoh PohModule(ThaTtiProcessor self)
    {
    AtChannel channel = (AtChannel)ThaTtiProcessorChannelGet(self);
    return (ThaModulePoh)AtDeviceModuleGet(AtChannelDeviceGet(channel), cThaModulePoh);
    }

static uint32 LineHwId(ThaSdhLineTtiProcessorV2 self)
    {
    return AtChannelHwIdGet((AtChannel)ThaTtiProcessorChannelGet((ThaTtiProcessor)self));
    }

static uint32 PohBaseAddress(ThaTtiProcessor self)
    {
    return Tha60210011ModulePohBaseAddress(PohModule(self));
    }

static uint32 PohExpectedTtiAddressGet(ThaSdhLineTtiProcessorV2 self)
    {
    AtUnused(self);
    return cAf6Reg_pohmsgj0exp_Base;
    }

static uint32 PohCapturedTtiAddressGet(ThaSdhLineTtiProcessorV2 self)
    {
    AtUnused(self);
    return cAf6Reg_pohmsgj0cur_Base;
    }

static uint32 ExpTtiBufferRegOffset(ThaSdhLineTtiProcessorV2 self)
    {
    return (uint32)(8 * LineHwId(self) + ThaTtiProcessorPartOffset((ThaTtiProcessor)self) + PohBaseAddress((ThaTtiProcessor)self));
    }

static uint32 PohJ0Control(ThaSdhLineTtiProcessorV2 self)
    {
    AtUnused(self);
    return cAf6Reg_pohcpej0ctr_Base;
    }

static uint32 DefaultOffset(ThaSdhLineTtiProcessorV2 self)
    {
    return (uint32)(2 * LineHwId(self) + ThaTtiProcessorPartOffset((ThaTtiProcessor)self) + PohBaseAddress((ThaTtiProcessor)self));
    }

static uint32 PohJ0Status(ThaSdhLineTtiProcessorV2 self)
    {
    AtUnused(self);
    return cAf6Reg_pohcpej0sta_Base;
    }

static uint32 PohJ0StatusOffset(ThaSdhLineTtiProcessorV2 self)
    {
    return (uint32)(LineHwId(self) + ThaTtiProcessorPartOffset((ThaTtiProcessor)self) + PohBaseAddress((ThaTtiProcessor)self));
    }

static uint32 PohJ0Grabber(ThaSdhLineTtiProcessorV2 self)
    {
    AtUnused(self);
    return cAf6Reg_pohj0grb_Base;
    }

static uint32 PohJ0GrabberOffset(ThaSdhLineTtiProcessorV2 self)
    {
    return PohJ0StatusOffset(self);
    }

static void OverrideThaSdhLineTtiProcessorV2(ThaTtiProcessor self)
    {
    ThaSdhLineTtiProcessorV2 processor = (ThaSdhLineTtiProcessorV2)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhLineTtiProcessorV2Override, mMethodsGet(processor), sizeof(m_ThaSdhLineTtiProcessorV2Override));

        mMethodOverride(m_ThaSdhLineTtiProcessorV2Override, PohCapturedTtiAddressGet);
        mMethodOverride(m_ThaSdhLineTtiProcessorV2Override, PohExpectedTtiAddressGet);
        mMethodOverride(m_ThaSdhLineTtiProcessorV2Override, ExpTtiBufferRegOffset);
        mMethodOverride(m_ThaSdhLineTtiProcessorV2Override, PohJ0Control);
        mMethodOverride(m_ThaSdhLineTtiProcessorV2Override, DefaultOffset);
        mMethodOverride(m_ThaSdhLineTtiProcessorV2Override, PohJ0Status);
        mMethodOverride(m_ThaSdhLineTtiProcessorV2Override, PohJ0StatusOffset);
        mMethodOverride(m_ThaSdhLineTtiProcessorV2Override, PohJ0Grabber);
        mMethodOverride(m_ThaSdhLineTtiProcessorV2Override, PohJ0GrabberOffset);
        }

    mMethodsSet(processor, &m_ThaSdhLineTtiProcessorV2Override);
    }

static void Override(ThaTtiProcessor self)
    {
    OverrideThaSdhLineTtiProcessorV2(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081FaceplateLineTtiProcessor);
    }

static ThaTtiProcessor ObjectInit(ThaTtiProcessor self, AtSdhChannel sdhChannel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021FaceplateLineTtiProcessorObjectInit(self, sdhChannel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaTtiProcessor Tha60290081FaceplateLineTtiProcessorNew(AtSdhChannel sdhChannel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaTtiProcessor newProcessor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newProcessor == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newProcessor, sdhChannel);
    }

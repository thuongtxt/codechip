/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : COS
 *
 * File        : Tha60290081ModuleCos.c
 *
 * Created Date: Jul 19, 2019
 *
 * Description : 60290081 Module COS implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290022/cos/Tha60290022ModuleCosInternal.h"
#include "../pwe/Tha60290081ModulePwe.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290081ModuleCos
    {
    tTha60290022ModuleCos super;
    }tTha60290081ModuleCos;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleCosMethods m_ThaModuleCosOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static Tha60290081ModulePwe PweModule(ThaModuleCos self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return (Tha60290081ModulePwe)AtDeviceModuleGet(device, cThaModulePwe);
    }

static eAtRet PwHeaderLengthHwSet(ThaModuleCos self, AtPw pw, uint8 hwValue)
    {
    return Tha60290081ModulePweFlowHeaderLengthSet(PweModule(self), AtPwEthFlowGet(pw), hwValue);
    }

static uint8 PwHeaderLengthHwGet(ThaModuleCos self, AtPw pw)
    {
    return (uint8)Tha60290081ModulePweFlowHeaderLengthGet(PweModule(self), AtPwEthFlowGet(pw));
    }

static void OverrideThaModuleCos(AtModule self)
    {
    ThaModuleCos cos = (ThaModuleCos)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleCosOverride, mMethodsGet(cos), sizeof(m_ThaModuleCosOverride));

        mMethodOverride(m_ThaModuleCosOverride, PwHeaderLengthHwSet);
        mMethodOverride(m_ThaModuleCosOverride, PwHeaderLengthHwGet);
        }

    mMethodsSet(cos, &m_ThaModuleCosOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleCos(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081ModuleCos);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022ModuleCosObjectInit(self, device) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60290081ModuleCosNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }


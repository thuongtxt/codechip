/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PMC
 * 
 * File        : Tha60290081ModulePmc.h
 * 
 * Created Date: Jul 16, 2019
 *
 * Description : 60290081 module PMC
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290081MODULEPMC_H_
#define _THA60290081MODULEPMC_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/pmc/ThaModulePmc.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 Tha60290081ModulePmcGlobalCounterGet(AtModule self, uint32 postion, uint32 counterId, eBool r2c);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290081MODULEPMC_H_ */


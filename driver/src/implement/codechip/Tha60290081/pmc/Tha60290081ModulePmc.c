/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PMC
 *
 * File        : Tha60290081ModulePmc.c
 *
 * Created Date: Jul 15, 2019
 *
 * Description : 60290081 module PMC implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtGfpChannel.h"
#include "../../Tha60290022/pmc/Tha60290022ModulePmcInternal.h"
#include "../encap/Tha60290081ModuleEncap.h"
#include "Tha60290081ModulePmc.h"
#include "Tha60290081ModulePmcMsReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290081ModulePmc
    {
    tTha60290022ModulePmcV2 super;
    }tTha60290081ModulePmc;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePmcMethods             m_ThaModulePmcOverride;
static tTha60290022ModulePmcV2Methods   m_Tha60290022ModulePmcV2Override;

/* Save super implementation */
static const tThaModulePmcMethods   *m_ThaModulePmcMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleEncap ModuleEncap(AtGfpChannel self)
    {
    return (AtModuleEncap)AtChannelModuleGet((AtChannel)self);
    }

static eBool IsHoGfpChannel(AtGfpChannel self)
    {
    if (AtChannelIdGet((AtChannel)self) < Tha60290081ModuleEncapNumHoGfpChannels(ModuleEncap(self)))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 GfpChannelOffset(AtGfpChannel self)
    {
    return (IsHoGfpChannel(self)) ? 0 : 0x3000UL;
    }

static uint32 GfpCounterOffset(ThaModulePmc self, AtGfpChannel channel, uint32 cntPosId, uint32 cntIndex, eBool clear)
    {
    return m_ThaModulePmcMethods->GfpCounterOffset(self, channel, cntPosId, cntIndex, clear) + GfpChannelOffset(channel);
    }

static uint32 BaseAddress(ThaModulePmc self, uint8 partId)
    {
    AtUnused(self);
    if (partId == 0) return 0x1F00000UL;
    if (partId == 1) return 0x1C00000UL;

    return cInvalidUint32;
    }

static uint32 AuVcDefaultOffset(ThaModulePmc self, AtSdhChannel channel)
    {
    uint8 slice, hwStsInSlice;
    AtUnused(self);

    if (ThaSdhChannel2HwMasterStsId(channel, cThaModuleOcn, &slice, &hwStsInSlice) == cAtOk)
        return (uint32)(slice + 4UL * hwStsInSlice);

    return cInvalidUint32;
    }

static uint32 Vc1xDefaultOffset(ThaModulePmc self, AtSdhChannel sdhChannel)
    {
    uint8 slice, hwStsInSlice;
    uint8 vtgId = AtSdhChannelTug2Get(sdhChannel);
    uint8 vtId = AtSdhChannelTu1xGet(sdhChannel);
    AtUnused(self);

    if (ThaSdhChannel2HwMasterStsId(sdhChannel, cThaModuleOcn, &slice, &hwStsInSlice) == cAtOk)
        return (uint32)(slice + (128UL * hwStsInSlice) + (16UL * vtgId) + (4UL * vtId));

    return cBit31_0;
    }

static uint32 De1DefaultOffset(ThaModulePmc self, AtPdhChannel de1)
    {
    uint8 slice, hwIdInSlice, vtg, vt;
    AtUnused(self);

    if (ThaPdhDe1HwIdGet((ThaPdhDe1)de1, cAtModulePdh, &slice, &hwIdInSlice, &vtg, &vt) == cAtOk)
        return (slice + (128UL * hwIdInSlice) + (16UL * vtg) + (4UL * vt));

    return cInvalidUint32;
    }

static uint32 De3DefaultOffset(ThaModulePmc self, AtPdhChannel de3)
    {
    uint8 sliceId, hwIdInSlice;
    AtUnused(self);

    if (ThaPdhChannelHwIdGet((AtPdhChannel)de3, cAtModulePdh, &sliceId, &hwIdInSlice) == cAtOk)
        return (4UL * hwIdInSlice) + sliceId;

    return cInvalidUint32;
    }

static eAtRet LoPwCounterCoeff(Tha60290022ModulePmcV2 self, uint32 *r2cCoeff, uint32 *typeCoeff)
    {
    AtUnused(self);
    *r2cCoeff  = 8192;
    *typeCoeff = 16384;
    return cAtOk;
    }

static eAtRet AuVcCounterCoeff(Tha60290022ModulePmcV2 self, uint32 *r2cCoeff, uint32 *typeCoeff)
    {
    AtUnused(self);
    *r2cCoeff  = 256;
    *typeCoeff = 512;
    return cAtOk;
    }

static eAtRet Vc1xCounterCoeff(Tha60290022ModulePmcV2 self, uint32 *r2cCoeff, uint32 *typeCoeff)
    {
    AtUnused(self);
    *r2cCoeff  = 8192;
    *typeCoeff = 16384;
    return cAtOk;
    }

static eAtRet De3CounterCoeff(Tha60290022ModulePmcV2 self, uint32 *r2cCoeff, uint32 *typeCoeff)
    {
    AtUnused(self);
    *r2cCoeff  = 256;
    *typeCoeff = 512;
    return cAtOk;
    }

static eAtRet De1CounterCoeff(Tha60290022ModulePmcV2 self, uint32 *r2cCoeff, uint32 *typeCoeff)
    {
    AtUnused(self);
    *r2cCoeff  = 8192;
    *typeCoeff = 16384;
    return cAtOk;
    }

static void OverrideThaModulePmc(AtModule self)
    {
    ThaModulePmc module = (ThaModulePmc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePmcMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePmcOverride, mMethodsGet(module), sizeof(m_ThaModulePmcOverride));

        mMethodOverride(m_ThaModulePmcOverride, GfpCounterOffset);
        mMethodOverride(m_ThaModulePmcOverride, BaseAddress);
        mMethodOverride(m_ThaModulePmcOverride, AuVcDefaultOffset);
        mMethodOverride(m_ThaModulePmcOverride, Vc1xDefaultOffset);
        mMethodOverride(m_ThaModulePmcOverride, De3DefaultOffset);
        mMethodOverride(m_ThaModulePmcOverride, De1DefaultOffset);
        }

    mMethodsSet(module, &m_ThaModulePmcOverride);
    }

static void OverrideTha60290022ModulePmcV2(AtModule self)
    {
    Tha60290022ModulePmcV2 module = (Tha60290022ModulePmcV2)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290022ModulePmcV2Override, mMethodsGet(module), sizeof(m_Tha60290022ModulePmcV2Override));

        mMethodOverride(m_Tha60290022ModulePmcV2Override, LoPwCounterCoeff);
        mMethodOverride(m_Tha60290022ModulePmcV2Override, AuVcCounterCoeff);
        mMethodOverride(m_Tha60290022ModulePmcV2Override, Vc1xCounterCoeff);
        mMethodOverride(m_Tha60290022ModulePmcV2Override, De3CounterCoeff);
        mMethodOverride(m_Tha60290022ModulePmcV2Override, De1CounterCoeff);
        }

    mMethodsSet(module, &m_Tha60290022ModulePmcV2Override);
    }

static void Override(AtModule self)
    {
    OverrideThaModulePmc(self);
    OverrideTha60290022ModulePmcV2(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081ModulePmc);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022ModulePmcV2ObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60290081ModulePmcNew(AtDevice self)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule module = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (module == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(module, self);
    }

uint32 Tha60290081ModulePmcGlobalCounterGet(AtModule self, uint32 postion, uint32 counterId, eBool r2c)
    {
    uint32 offset, address;

    offset = ThaModulePmcBaseAddress((ThaModulePmc)self) + counterId + (postion * 0x10) + (uint32)(r2c * 0x8);
    address = offset + cAf6Reg_upen_glb_inf_count;
    return mModuleHwRead(self, address);
    }

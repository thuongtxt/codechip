/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PMC
 * 
 * File        : Tha60290081ModulePmcMsReg.h
 * 
 * Created Date: Jul 15, 2019
 *
 * Description : Module PMC MS
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290081MODULEPMCMSREG_H_
#define _THA60290081MODULEPMCMSREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : MPEG Lookup id
Reg Addr   : 0x0C_2000
Reg Formula: 0x0C_2000 + $rxflow_id
    Where  :
           + $rxflow_id(0-5375) : Counter ID
Reg Desc   :
This register is used to convert rxflow id to slice id and enc id for 4 intance enc (only use for oam ans plain at enc counter)

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mpeglk                                                                           0x0C2000
#define cAf6Reg_upen_mpeglk_WidthVal                                                                        32

/*--------------------------------------
BitField Name: enc lookup en
BitField Type: R/W
BitField Desc: 1 : lk enable
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_upen_mpeglk_enc_lookup_en_Mask                                                             cBit13
#define cAf6_upen_mpeglk_enc_lookup_en_Shift                                                                13

/*--------------------------------------
BitField Name: enc slice
BitField Type: R/W
BitField Desc: enc slice
BitField Bits: [12:11]
--------------------------------------*/
#define cAf6_upen_mpeglk_enc_slice_Mask                                                              cBit12_11
#define cAf6_upen_mpeglk_enc_slice_Shift                                                                    11

/*--------------------------------------
BitField Name: enc id
BitField Type: R/W
BitField Desc: enc id
BitField Bits: [10:0]
--------------------------------------*/
#define cAf6_upen_mpeglk_enc_id_Mask                                                                  cBit10_0
#define cAf6_upen_mpeglk_enc_id_Shift                                                                        0


/*------------------------------------------------------------------------------
Reg Name   : PMC GLB INF Counter enc
Reg Addr   : 0x00_0000
Reg Formula: 0x00_0000 +  $idx_id*0x1_0000 + $slc_id*0x4000 +  $pos_id*0x1000 + $r2c*0x800 + $enc_id
    Where  :
           + $idx_id(0-1)
           + $slc_id (0-0)
           + $pos_id(0-3) : Position ID
           + $r2c(0-1) : r2c bit
           + $enc_id(0-1343) : Counter ID
Reg Desc   :
This register is used to read some packet counters , support  both mode r2c and ro
- in sace of idx_id = 0
+ pos_id = 0 : encabort
+ pos_id = 1 : encoam
+ pos_id = 2 : encplain
+ pos_id = 3 : encfrg
- in sace of idx_id = 1
+ pos_id = 0 : encbytegood
+ pos_id = 1 : encpktgood
+ pos_id = 2 : enctotalbyte
+ pos_id = 3 : enctotalpkt
- r2c       : enable mean read to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_enc                                                                              0x000000

/*--------------------------------------
BitField Name: cnt_enc
BitField Type: R/W
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_enc_cnt_enc_Mask                                                                    cBit31_0
#define cAf6_upen_enc_cnt_enc_Shift                                                                          0


/*------------------------------------------------------------------------------
Reg Name   : PMC GLB INF Counter enc1
Reg Addr   : 0x0E_0000
Reg Formula: 0x0E_0000 +  $idx_id*0x1_0000 + $slc_id*0x4000 +  $pos_id*0x1000 + $r2c*0x800 + $enc_id
    Where  :
           + $idx_id(0-0)
           + $slc_id (0-0)
           + $pos_id(0-3) : Position ID
           + $r2c(0-1) : r2c bit
           + $enc_id(0-511) : Counter ID
Reg Desc   :
This register is used to read some packet counters , support  both mode r2c and ro
- in sace of idx_id = 0
+ pos_id = 0 : encidle
+ pos_id = 1 : encmtu
+ pos_id = 2 : unused
+ pos_id = 3 : unused
- r2c       : enable mean read to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_enc1                                                                             0x0E0000

/*--------------------------------------
BitField Name: cnt_enc1
BitField Type: R/W
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_enc1_cnt_enc1_Mask                                                                  cBit31_0
#define cAf6_upen_enc1_cnt_enc1_Shift                                                                        0


/*------------------------------------------------------------------------------
Reg Name   : PMC GLB INF Counter dec
Reg Addr   : 0x02_0000
Reg Formula: 0x02_0000 + $idx_id*0x1_0000 +  $pos_id*0x4000 + $r2c*0x2000 + $dec_id
    Where  :
           + $idx_id(0-2)
           + $pos_id(0-3) : Position ID
           + $r2c(0-1) : r2c bit
           + $dec_id(0-511) : Counter ID
Reg Desc   :
This register is used to read some packet counters , support  both mode r2c and ro
- in sace of idx_id = 0
+ pos_id = 0 : decmru
+ pos_id = 1 : decdispkt
+ pos_id = 2 : decfcserr
+ pos_id = 3 : decidle
- in sace of idx_id = 1
+ pos_id = 0 : decabort
+ pos_id = 1 : decoam
+ pos_id = 2 : decplain
+ pos_id = 3 : decfrg
- in sace of idx_id = 2
+ pos_id = 0 : decpktgood
+ pos_id = 1 : dectotalbyte
+ pos_id = 2 : dectotalpkt
+ pos_id = 3 : decbytegood
- r2c       : enable mean read to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dec                                                                              0x020000

/*--------------------------------------
BitField Name: cnt_dec
BitField Type: R/W
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dec_cnt_dec_Mask                                                                    cBit31_0
#define cAf6_upen_dec_cnt_dec_Shift                                                                          0


/*------------------------------------------------------------------------------
Reg Name   : PMC GLB INF Counter rxflow
Reg Addr   : 0x05_0000
Reg Formula: 0x05_0000 + $idx_id*0x1_0000 +  $pos_id*0x4000 + $r2c*0x2000 + $rxflow_id
    Where  :
           + $idx_id(0-2)
           + $pos_id(0-3) : Position ID
           + $r2c(0-1) : r2c bit
           + $rxflow_id(0-5375) : Counter ID
Reg Desc   :
This register is used to read some packet counters , support  both mode r2c and ro
- in sace of idx_id = 0
+ pos_id = 0 : rxbytecnt
+ pos_id = 1 : rxpktcnt
+ pos_id = 2 : rxdisbyte
+ pos_id = 3 : rxdispkt
- in sace of idx_id = 1
+ pos_id = 0 : rxfrg
+ pos_id = 1 : rxnullfrg
+ pos_id = 2 : rxBECN
+ pos_id = 3 : rxFECN
- in sace of idx_id = 2
+ pos_id = 0 : rxDE
+ pos_id = 1 : unused
+ pos_id = 2 : unused
+ pos_id = 3 : unused
- r2c       : enable mean read to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxflow                                                                           0x050000

/*--------------------------------------
BitField Name: cnt_rxflow
BitField Type: R/W
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_rxflow_cnt_rxflow_Mask                                                              cBit31_0
#define cAf6_upen_rxflow_cnt_rxflow_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : PMC GLB INF Counter txflow
Reg Addr   : 0x08_0000
Reg Formula: 0x08_0000 + $idx_id*0x1_0000 +  $pos_id*0x4000 + $r2c*0x2000 + $txflow_id
    Where  :
           + $idx_id(0-2)
           + $pos_id(0-3) : Position ID
           + $r2c(0-1) : r2c bit
           + $txflow_id(0-5375) : Counter ID
Reg Desc   :
This register is used to read some packet counters , support  both mode r2c and ro
- in sace of idx_id = 0
+ pos_id = 0 : txbytecnt
+ pos_id = 1 : txpktcnt
+ pos_id = 2 : txdisbyte
+ pos_id = 3 : txdispkt
- in sace of idx_id = 1
+ pos_id = 0 : txfrg
+ pos_id = 1 : txnullfrg
+ pos_id = 2 : txBECN
+ pos_id = 3 : txFECN
- in sace of idx_id = 2
+ pos_id = 0 : txDE
+ pos_id = 1 : unused
+ pos_id = 2 : unused
+ pos_id = 3 : unused
- r2c       : enable mean read to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txflow                                                                           0x080000

/*--------------------------------------
BitField Name: cnt_txflow
BitField Type: R/W
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_txflow_cnt_txflow_Mask                                                              cBit31_0
#define cAf6_upen_txflow_cnt_txflow_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : PMC GLB INF Counter txbundle
Reg Addr   : 0x0B_0000
Reg Formula: 0x0B_0000 + $idx_id*0x1000 +  $pos_id*0x400 + $r2c*0x200 + $txbundle_id
    Where  :
           + $idx_id(0-1)
           + $pos_id(0-3) : Position ID
           + $r2c(0-1) : r2c bit
           + $txbundle_id(0-511) : Counter ID
Reg Desc   :
This register is used to read some packet counters , support  both mode r2c and ro
- in sace of idx_id = 0
+ pos_id = 0 : buntxfrg
+ pos_id = 1 : buntxpktdiscard
+ pos_id = 2 : buntxpktgood
+ pos_id = 3 : unused
- in sace of idx_id = 1
+ pos_id = 0 : buntxbyte
+ pos_id = 1 : unused
+ pos_id = 2 : unused
+ pos_id = 3 : unused
- r2c       : enable mean read to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txbundle                                                                         0x0B0000

/*--------------------------------------
BitField Name: cnt_txbundle
BitField Type: R/W
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_txbundle_cnt_txbundle_Mask                                                          cBit31_0
#define cAf6_upen_txbundle_cnt_txbundle_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : PMC GLB INF Counter rxbundle
Reg Addr   : 0x0B_2000
Reg Formula: 0x0B_2000 + $idx_id*0x1000 +  $pos_id*0x400 + $r2c*0x200 + $rxbundle_id
    Where  :
           + $idx_id(0-1)
           + $pos_id(0-3) : Position ID
           + $r2c(0-1) : r2c bit
           + $rxbundle_id(0-511) : Counter ID
Reg Desc   :
This register is used to read some packet counters , support  both mode r2c and ro
- in sace of idx_id = 0
+ pos_id = 0 : bunrxfrg
+ pos_id = 1 : bunrxpktdiscard
+ pos_id = 2 : bunrxpktgood
+ pos_id = 3 : unused
- in sace of idx_id = 1
+ pos_id = 0 : bunrxbyte
+ pos_id = 1 : unused
+ pos_id = 2 : unused
+ pos_id = 3 : unused
- r2c       : enable mean read to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxbundle                                                                         0x0B2000

/*--------------------------------------
BitField Name: cnt_rxbundle
BitField Type: R/W
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_rxbundle_cnt_rxbundle_Mask                                                          cBit31_0
#define cAf6_upen_rxbundle_cnt_rxbundle_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : PMC GLB INF Counter GFP ENC HO
Reg Addr   : 0x0B_4000
Reg Formula: 0x0B_4000 +  $idx_id*0x400 + $pos_id*0x100 + $r2c*0x80 + $encho_id
    Where  :
           + $idx_id(0-1) : index
           + $pos_id(0-3) : Position ID
           + $r2c(0-1) : r2c bit
           + $encho_id(0-127) : Counter ID
Reg Desc   :
This register is used to read some packet counters , support  both mode r2c and ro
- in sace of idx_id = 0
+ pos_id = 0 : byte
+ pos_id = 1 : pkt
+ pos_id = 2 : idle
+ pos_id = 3 : unused
- in sace of idx_id = 1
+ pos_id = 0 : GFPF-SO-DATA-FRM-CNT
+ pos_id = 1 : GFPF-SO-CMF-CSF-FRM-CNT
+ pos_id = 2 : GFPF-SO-CMF-FRM-CNT
+ pos_id = 3 : GFPF-SO-MCF-FRM-CNT
- r2c       : enable mean read to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_enc_ho                                                                           0x0B4000

/*--------------------------------------
BitField Name: cnt_encho
BitField Type: R/W
BitField Desc: Counter enc high order
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_enc_ho_cnt_encho_Mask                                                               cBit31_0
#define cAf6_upen_enc_ho_cnt_encho_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : PMC GLB INF Counter GFP DEC HO
Reg Addr   : 0x0B_5000
Reg Formula: 0x0B_5000 +  $idx_id*0x400 + $pos_id*0x100 + $r2c*0x80 + $decho_id
    Where  :
           + $idx_id(0-5)
           + $pos_id(0-3) : Position ID
           + $r2c(0-1) : r2c bit
           + $decho_id(0-127) : Counter ID
Reg Desc   :
This register is used to read some packet counters , support  both mode r2c and ro
- in sace of idx_id = 0
+ pos_id = 0 : err
+ pos_id = 1 : pkt
+ pos_id = 2 : idle
+ pos_id = 3 : byte
- in sace of idx_id = 1
+ pos_id = 0 : GFPF-SK-DATA-FRM-CNT
+ pos_id = 1 : GFPF-SK-CMF-FRM-CNT
+ pos_id = 2 : GFPF-SK-THEC-HIT-CNT
+ pos_id = 3 : GFPF-SK-THEC-ERR-CNT
- in sace of idx_id = 2
+ pos_id = 0 : GFPF-SK-THEC-TYPE-CORR-CNT
+ pos_id = 1 : GFPF-SK-THEC-CRC-COR-CNT
+ pos_id = 2 : GFPF-SK-EHEC-HIT-CNT
+ pos_id = 3 : GFPF-SK-EHEC-ERR-CNT
- in sace of idx_id = 3
+ pos_id = 0 : GFPF-SK-CHEC-HIT-CNT
+ pos_id = 1 : GFPF-SK-CHEC-ERR-CNT
+ pos_id = 2 : GFPF-SK-CHEC-PLI-COR-CNT
+ pos_id = 3 : GFPF-SK-CHEC-CRC-COR-CNT
- in sace of idx_id = 4
+ pos_id = 0 : GFPF-SK-PFCS-HIT-CNT
+ pos_id = 1 : GFPF-SK-PFCS-ERR-CNT
+ pos_id = 2 : GFPF-SK-MAX-LEN-ERR-CNT
+ pos_id = 3 : GFPF-SK-MIN-LEN-ERR-CNT
- in sace of idx_id = 5
+ pos_id = 0 : GFPF-SK-PTI-UPI-DISCARDED-CNT
+ pos_id = 1 : unused
+ pos_id = 2 : unused
+ pos_id = 3 : unused
- r2c       : enable mean read to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dec_ho                                                                           0x0B5000

/*--------------------------------------
BitField Name: cnt_decho
BitField Type: R/W
BitField Desc: Counter dec high order
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dec_ho_cnt_decho_Mask                                                               cBit31_0
#define cAf6_upen_dec_ho_cnt_decho_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : PMC GLB INF Counter GFP ENC LO
Reg Addr   : 0x0B_7000
Reg Formula: 0x0B_7000 +  $idx_id*0x400 + $pos_id*0x100 + $r2c*0x80 + $encho_id
    Where  :
           + $idx_id(0-1) : index
           + $pos_id(0-3) : Position ID
           + $r2c(0-1) : r2c bit
           + $encho_id(0-127) : Counter ID
Reg Desc   :
This register is used to read some packet counters , support  both mode r2c and ro
- in sace of idx_id = 0
+ pos_id = 0 : byte
+ pos_id = 1 : pkt
+ pos_id = 2 : idle
+ pos_id = 3 : unused
- in sace of idx_id = 1
+ pos_id = 0 : GFPF-SO-DATA-FRM-CNT
+ pos_id = 1 : GFPF-SO-CMF-CSF-FRM-CNT
+ pos_id = 2 : GFPF-SO-CMF-FRM-CNT
+ pos_id = 3 : GFPF-SO-MCF-FRM-CNT
- r2c       : enable mean read to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_enc_lo                                                                           0x0B7000

/*--------------------------------------
BitField Name: cnt_enclo
BitField Type: R/W
BitField Desc: Counter enc low order
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_enc_lo_cnt_enclo_Mask                                                               cBit31_0
#define cAf6_upen_enc_lo_cnt_enclo_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : PMC GLB INF Counter GFP DEC LO
Reg Addr   : 0x0B_8000
Reg Formula: 0x0B_8000 +  $idx_id*0x400 + $pos_id*0x100 + $r2c*0x80 + $decho_id
    Where  :
           + $idx_id(0-5)
           + $pos_id(0-3) : Position ID
           + $r2c(0-1) : r2c bit
           + $decho_id(0-127) : Counter ID
Reg Desc   :
This register is used to read some packet counters , support  both mode r2c and ro
- in sace of idx_id = 0
+ pos_id = 0 : err
+ pos_id = 1 : pkt
+ pos_id = 2 : idle
+ pos_id = 3 : byte
- in sace of idx_id = 1
+ pos_id = 0 : GFPF-SK-DATA-FRM-CNT
+ pos_id = 1 : GFPF-SK-CMF-FRM-CNT
+ pos_id = 2 : GFPF-SK-THEC-HIT-CNT
+ pos_id = 3 : GFPF-SK-THEC-ERR-CNT
- in sace of idx_id = 2
+ pos_id = 0 : GFPF-SK-THEC-TYPE-CORR-CNT
+ pos_id = 1 : GFPF-SK-THEC-CRC-COR-CNT
+ pos_id = 2 : GFPF-SK-EHEC-HIT-CNT
+ pos_id = 3 : GFPF-SK-EHEC-ERR-CNT
- in sace of idx_id = 3
+ pos_id = 0 : GFPF-SK-CHEC-HIT-CNT
+ pos_id = 1 : GFPF-SK-CHEC-ERR-CNT
+ pos_id = 2 : GFPF-SK-CHEC-PLI-COR-CNT
+ pos_id = 3 : GFPF-SK-CHEC-CRC-COR-CNT
- in sace of idx_id = 4
+ pos_id = 0 : GFPF-SK-PFCS-HIT-CNT
+ pos_id = 1 : GFPF-SK-PFCS-ERR-CNT
+ pos_id = 2 : GFPF-SK-MAX-LEN-ERR-CNT
+ pos_id = 3 : GFPF-SK-MIN-LEN-ERR-CNT
- in sace of idx_id = 5
+ pos_id = 0 : GFPF-SK-PTI-UPI-DISCARDED-CNT
+ pos_id = 1 : unused
+ pos_id = 2 : unused
+ pos_id = 3 : unused
- r2c       : enable mean read to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dec_lo                                                                           0x0B8000

/*--------------------------------------
BitField Name: cnt_declo
BitField Type: R/W
BitField Desc: Counter dec low order
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dec_lo_cnt_declo_Mask                                                               cBit31_0
#define cAf6_upen_dec_lo_cnt_declo_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : PMC GLB INF Counters
Reg Addr   : 0x0C_0000
Reg Formula: 0x0C_0000 + $pos_id*0x10 + $r2c*0x8 + $cnt_id
    Where  :
           + $pos_id(0-15) : Position ID
           + $r2c(0-1) : r2c bit
           + $cnt_id(0-7) : Counter ID
Reg Desc   :
This register is used to read some packet counters , support  both mode r2c and ro
- Pos_id : Position counters
+ 0  : MAC -> CLA port 0
+ 1  : MAC -> CLA port 1
+ 2  : CLA -> PDA port 0
+ 3  : CLA -> PDA port 1
+ 4  : PDA -> ENC#0
+ 5  : PDA -> ENC#1
+ 6  : PDA -> ENC#2
+ 7  : PDA -> ENC#3
+ 8  : PLA -> PKA#0
+ 9  : PLA -> PKA#1
+ 10 : PKA#0 -> PW#0
+ 11 : PKA#0 -> PW#1
+ 12 : PW#0 -> ETH
+ 13 : PW#1 -> ETH
+ 14 : PDA -> ETH PASS 1G
+ 15 : PDA -> ETH PASS 10G
- Counter ID details :
+ 0     : vld_counters
+ 1     : sop_counters
+ 2     : eop_counters
+ 3     : err_counters
+ 4     : byte_counters
+ 5-7   : Unused
- r2c       : enable mean read to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_glb_inf_count                                                                    0x0C0000

/*--------------------------------------
BitField Name: cnt_val
BitField Type: R/W
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_glb_inf_count_cnt_val_Mask                                                          cBit31_0
#define cAf6_upen_glb_inf_count_cnt_val_Shift                                                                0

#ifdef __cplusplus
}
#endif
#endif /* _THA60290081MODULEPMCMSREG_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Concate
 *
 * File        : Tha60290081VcgNonVcat.c
 *
 * Created Date: Sep 8, 2019
 *
 * Description : 60290081 CCAT/NonVCAT group implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../../../default/concate/ThaVcgNonVcatInternal.h"
#include "../../../default/concate/ThaVcg.h"
#include "Tha60290081ModuleConcate.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290081VcgNonVcat
    {
    tThaVcgNonVcat super;
    }tTha60290081VcgNonVcat;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods       m_AtObjectOverride;
static tAtChannelMethods      m_AtChannelOverride;
static tAtConcateGroupMethods m_AtConcateGroupOverride;

/* To save super implementation */
static const tAtObjectMethods       *m_AtObjectMethods       = NULL;
static const tAtChannelMethods      *m_AtChannelMethods      = NULL;
static const tAtConcateGroupMethods *m_AtConcateGroupMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
#include "Tha60290081VcgCommon.h"

static void OverrideAtObject(AtConcateGroup self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtConcateGroup self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, ServiceIsRunning);
        mMethodOverride(m_AtChannelOverride, HwIdGet);
        mMethodOverride(m_AtChannelOverride, Init);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtConcateGroup(AtConcateGroup self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtConcateGroupMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtConcateGroupOverride, m_AtConcateGroupMethods, sizeof(m_AtConcateGroupOverride));

        mMethodOverride(m_AtConcateGroupOverride, EncapTypeIsSupported);
        mMethodOverride(m_AtConcateGroupOverride, EncapTypeGet);
        mMethodOverride(m_AtConcateGroupOverride, SourceMemberProvision);
        mMethodOverride(m_AtConcateGroupOverride, SourceMemberDeprovision);
        }

    mMethodsSet(self, &m_AtConcateGroupOverride);
    }

static void Override(AtConcateGroup self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtConcateGroup(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081VcgNonVcat);
    }

static AtConcateGroup ObjectInit(AtConcateGroup self, AtModuleConcate concateModule, uint32 vcgId, eAtConcateMemberType memberType, eAtConcateGroupType concateType)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaVcgNonVcatObjectInit(self, concateModule, vcgId, memberType, concateType) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtConcateGroup Tha60290081VcgNonVcatNew(AtModuleConcate module, uint32 vcgId, eAtConcateMemberType memberType, eAtConcateGroupType concateType)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtConcateGroup newVcg = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newVcg == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newVcg, module, vcgId, memberType, concateType);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CONCATE
 * 
 * File        : Tha60290081VcgInternal.h
 * 
 * Created Date: Sep 3, 2019
 *
 * Description : 60290081 VCAT VCG representation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290081VCGVCATINTERNAL_H_
#define _THA60290081VCGVCATINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/concate/ThaVcgVcatInternal.h"
#include "Tha60290081VcgVcat.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290081VcgVcatMethods
    {
    eAtRet (*SinkMstReset)(Tha60290081VcgVcat self);
    eAtRet (*SourceMstReset)(Tha60290081VcgVcat self);
    eAtRet (*SinkReset)(Tha60290081VcgVcat self);
    eAtRet (*SourceReset)(Tha60290081VcgVcat self);
    }tTha60290081VcgVcatMethods;

typedef struct tTha60290081VcgVcat
    {
    tThaVcgVcat super;
    const tTha60290081VcgVcatMethods *methods;
    }tTha60290081VcgVcat;

typedef struct tTha60290081VcgHoVcat
    {
    tTha60290081VcgVcat super;
    }tTha60290081VcgHoVcat;

typedef struct tTha60290081VcgLoVcat
    {
    tTha60290081VcgVcat super;
    }tTha60290081VcgLoVcat;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtVcg Tha60290081VcgVcatObjectInit(AtVcg self, AtModuleConcate concateModule, uint32 vcgId, eAtConcateMemberType memberType);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290081VCGVCATINTERNAL_H_ */


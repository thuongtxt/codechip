/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CONCATE
 * 
 * File        : Tha60290081ModuleConcateInternal.h
 * 
 * Created Date: Jun 18, 2019
 *
 * Description : 60290081 Module CONCATE representation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290081MODULECONCATEINTERNAL_H_
#define _THA60290081MODULECONCATEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/util/AtLongRegisterAccess.h"
#include "../../../default/concate/ThaModuleConcateInternal.h"
#include "../../../default/util/ThaBitMask.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef enum eTha60290081LoStsPldType
    {
    cTha60290081LoStsPldTypeNone,     /* Unused STS */
    cTha60290081LoStsPldTypeDe3,      /* STS used for DS3/E3 */
    cTha60290081LoStsPldTypeVt2E1,    /* STS used for 21 VT2/E1 */
    cTha60290081LoStsPldTypeVt15Ds1   /* STS used for 28 VT15/DS1 */
    }eTha60290081LoStsPldType;

typedef struct tTha60290081LoVcatSts
    {
    uint8 pldType;
    ThaBitMask usedVTs;
    }tTha60290081LoVcatSts;

typedef struct tTha60290081ModuleConcate
    {
    tThaModuleConcate super;

    /* Private data */
    AtLongRegisterAccess hoLongRegisterAccess;
    AtLongRegisterAccess loLongRegisterAccess;
    tTha60290081LoVcatSts loSts[48];
    }tTha60290081ModuleConcate;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleConcate Tha60290081ModuleConcateObjectInit(AtModuleConcate self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290081MODULECONCATEINTERNAL_H_ */


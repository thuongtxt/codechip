/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Concate
 *
 * File        : Tha60290081VcgHoVcat.c
 *
 * Created Date: Jul 14, 2019
 *
 * Description : 60290081 HO VCAT VCG implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "Tha60290081ModuleConcate.h"
#include "Tha60290081ModuleHoVcatReg.h"
#include "Tha60290081VcgVcatInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cLcasHoldOffTimerUnit   100
#define cLcasWtrTimerUnit       100
#define cLcasRmvTimerUnit       2

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtVcgMethods                m_AtVcgOverride;
static tThaVcgVcatMethods           m_ThaVcgVcatOverride;
static tTha60290081VcgVcatMethods   m_Tha60290081VcgVcatOverride;

/* Save super implementation */
static const tAtVcgMethods      *m_AtVcgVcatMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static Tha60290081ModuleConcate ModuleConcate(AtVcg self)
    {
    return (Tha60290081ModuleConcate)AtChannelModuleGet((AtChannel)self);
    }

static uint32 BaseAddress(AtVcg self)
    {
    return Tha60290081ModuleConcateHoBaseAddress(ModuleConcate(self));
    }

static uint32 DefaultOffset(AtVcg self)
    {
    return BaseAddress(self) + AtChannelHwIdGet((AtChannel)self);
    }

static uint32 TimerSw2Hw(uint32 timerInMs, uint32 timerUnitInMs)
    {
    uint32 value;

    if (timerUnitInMs == 0)
        return 0;

    value = timerInMs / timerUnitInMs;
    if (timerInMs % timerUnitInMs)
        value += 1U;

    return value;
    }

static uint32 TimerHw2Sw(uint32 hwValue, uint32 timerUnitInMs)
    {
    return hwValue * timerUnitInMs;
    }

static eAtModuleConcateRet HoldOffTimerSet(AtVcg self, uint32 timerInMs)
    {
    uint32 regAddr = cAf6Reg_ho_sk_thr_pen_Base + DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);
    mRegFieldSet(regVal, cAf6_ho_sk_thr_pen_Sk_Ho_Dis_, (timerInMs) ? 0 : 1);
    mRegFieldSet(regVal, cAf6_ho_sk_thr_pen_Sk_Ho_Thr_, TimerSw2Hw(timerInMs, cLcasHoldOffTimerUnit));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static uint32 HoldOffTimerGet(AtVcg self)
    {
    uint32 regAddr = cAf6Reg_ho_sk_thr_pen_Base + DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);
    uint32 hwValue = mRegField(regVal, cAf6_ho_sk_thr_pen_Sk_Ho_Thr_);

    if (mRegField(regVal, cAf6_ho_sk_thr_pen_Sk_Ho_Dis_))
        return 0;

    return TimerHw2Sw(hwValue, cLcasHoldOffTimerUnit);
    }

static eAtModuleConcateRet WtrTimerSet(AtVcg self, uint32 timerInMs)
    {
    uint32 regAddr = cAf6Reg_ho_sk_thr_pen_Base + DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);
    mRegFieldSet(regVal, cAf6_ho_sk_thr_pen_Sk_WTR_Dis_, (timerInMs) ? 0 : 1);
    mRegFieldSet(regVal, cAf6_ho_sk_thr_pen_Sk_WTR_Thr_, TimerSw2Hw(timerInMs, cLcasWtrTimerUnit));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static uint32 WtrTimerGet(AtVcg self)
    {
    uint32 regAddr = cAf6Reg_ho_sk_thr_pen_Base + DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);
    uint32 hwValue = mRegField(regVal, cAf6_ho_sk_thr_pen_Sk_WTR_Thr_);

    if (mRegField(regVal, cAf6_ho_sk_thr_pen_Sk_WTR_Dis_))
        return 0;

    return TimerHw2Sw(hwValue, cLcasWtrTimerUnit);
    }

static eAtModuleConcateRet RmvTimerSet(AtVcg self, uint32 timerInMs)
    {
    uint32 regAddr = cAf6Reg_ho_sk_thr_pen_Base + DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);
    mRegFieldSet(regVal, cAf6_ho_sk_thr_pen_Sk_RMV_Dis_, (timerInMs) ? 0 : 1);
    mRegFieldSet(regVal, cAf6_ho_sk_thr_pen_Sk_RMV_Thr_, TimerSw2Hw(timerInMs, cLcasRmvTimerUnit));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static uint32 RmvTimerGet(AtVcg self)
    {
    uint32 regAddr = cAf6Reg_ho_sk_thr_pen_Base + DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);
    uint32 hwValue = mRegField(regVal, cAf6_ho_sk_thr_pen_Sk_RMV_Thr_);

    if (mRegField(regVal, cAf6_ho_sk_thr_pen_Sk_RMV_Dis_))
        return 0;

    return TimerHw2Sw(hwValue, cLcasRmvTimerUnit);
    }

static uint8 SourceRsAckGet(AtVcg self)
    {
    uint32 regAddr = cAf6Reg_ho_sk_rsack_pen_Base + DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);
    return (uint8)mRegField(regVal, cAf6_ho_sk_rsack_pen_Sk_Rx_RSACK_);
    }

static uint8 SinkRsAckGet(AtVcg self)
    {
    uint32 regAddr = cAf6Reg_ho_sk_rsack_pen_Base + DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);
    return (uint8)mRegField(regVal, cAf6_ho_sk_rsack_pen_Sk_Tx_RSACK_);
    }

static uint32 SinkMstRegOffset(AtVcg self, AtConcateMember member)
    {
    uint32 vcgId = AtChannelIdGet((AtChannel)self);
    uint32 sqValue = AtConcateMemberSinkSequenceGet(member);
    return BaseAddress(self) + vcgId * 32U + sqValue / 8U;
    }

static uint8 SinkMstGet(AtVcg self, AtConcateMember member)
    {
    uint32 regAddr = cAf6Reg_ho_sk_txmst_pen_Base + SinkMstRegOffset(self, member);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);
    uint32 sqValue = AtConcateMemberSinkSequenceGet(member);
    uint32 mstValShift = sqValue % 8U;
    uint32 mstValMask = cBit0 << mstValShift;
    return (uint8)mRegField(regVal, mstVal);
    }

static uint32 SourceMstRegOffset(AtVcg self, AtConcateMember member)
    {
    uint32 vcgId = AtChannelIdGet((AtChannel)self);
    uint32 sqValue = AtConcateMemberSourceSequenceGet(member);
    return BaseAddress(self) + vcgId * 8U + sqValue / 32U;
    }

static uint8 SourceMstGet(AtVcg self, AtConcateMember member)
    {
    uint32 regAddr = cAf6Reg_ho_sk_rxmst_pen_Base + SourceMstRegOffset(self, member);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleConcate);
    uint32 sqValue = AtConcateMemberSourceSequenceGet(member);
    uint32 mstValShift = sqValue % 32U;
    uint32 mstValMask = cBit0 << mstValShift;
    return (uint8)mRegField(regVal, mstVal);
    }

static eAtRet SinkMstReset(Tha60290081VcgVcat self)
    {
    uint32 regAddr = BaseAddress((AtVcg)self) + cAf6Reg_ho_sk_txmst_pen_Base;
    uint32 regVal  = cAf6_ho_sk_txmst_pen_Sk_TxMST_Mask;
    uint32 vcgId = AtChannelIdGet((AtChannel)self);
    uint32 sqGroup;

    for (sqGroup = 0; sqGroup < 32; sqGroup++)
        {
        uint32 offset = vcgId * 32U + sqGroup;
        mChannelHwWrite(self, regAddr + offset, regVal, cAtModuleConcate);
        }

    return cAtOk;
    }

static eAtRet SourceMstReset(Tha60290081VcgVcat self)
    {
    uint32 regAddr = BaseAddress((AtVcg)self) + cAf6Reg_ho_sk_rxmst_pen_Base;
    uint32 regVal = cAf6_ho_sk_rxmst_pen_Sk_RxMST_Mask;
    uint32 vcgId = AtChannelIdGet((AtChannel)self);
    uint32 sqGroup;

    for (sqGroup = 0; sqGroup < 8; sqGroup++)
        {
        uint32 offset = vcgId * 8 + sqGroup;
        mChannelHwWrite(self, regAddr + offset, regVal, cAtModuleConcate);
        }

    return cAtOk;
    }

static eAtRet SinkReset(Tha60290081VcgVcat self)
    {
    uint32 regAddr;
    uint32 longRegVal[cThaLongRegMaxSize];

    AtOsalMemInit(longRegVal, 0, sizeof(longRegVal));

    /* Clear VCG status */
    regAddr = cAf6Reg_ho_sk_vcgsta_pen_Base + DefaultOffset((AtVcg)self);
    mChannelHwLongWrite(self, regAddr, longRegVal, cThaLongRegMaxSize, cAtModuleConcate);

    return cAtOk;
    }

static eAtRet SourceReset(Tha60290081VcgVcat self)
    {
    uint32 regAddr;

    /* Clear VCG status */
    regAddr = cAf6Reg_ho_so_vcgsta_pen_Base + DefaultOffset((AtVcg)self);
    mChannelHwWrite(self, regAddr, 0, cAtModuleConcate);

    return cAtOk;
    }

static void OverrideAtVcg(AtVcg self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtVcgVcatMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtVcgOverride, m_AtVcgVcatMethods, sizeof(m_AtVcgOverride));

        mMethodOverride(m_AtVcgOverride, HoldOffTimerSet);
        mMethodOverride(m_AtVcgOverride, HoldOffTimerGet);
        mMethodOverride(m_AtVcgOverride, RmvTimerSet);
        mMethodOverride(m_AtVcgOverride, RmvTimerGet);
        mMethodOverride(m_AtVcgOverride, WtrTimerSet);
        mMethodOverride(m_AtVcgOverride, WtrTimerGet);
        mMethodOverride(m_AtVcgOverride, SourceRsAckGet);
        mMethodOverride(m_AtVcgOverride, SinkRsAckGet);
        mMethodOverride(m_AtVcgOverride, SinkMstGet);
        mMethodOverride(m_AtVcgOverride, SourceMstGet);
        }

    mMethodsSet(self, &m_AtVcgOverride);
    }

static void OverrideThaVcgVcat(AtVcg self)
    {
    ThaVcgVcat vcg = (ThaVcgVcat)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaVcgVcatOverride, mMethodsGet(vcg), sizeof(m_ThaVcgVcatOverride));

        }

    mMethodsSet(vcg, &m_ThaVcgVcatOverride);
    }

static void OverrideTha60290081VcgVcat(AtVcg self)
    {
    Tha60290081VcgVcat vcg = (Tha60290081VcgVcat)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290081VcgVcatOverride, mMethodsGet(vcg), sizeof(m_Tha60290081VcgVcatOverride));

        mMethodOverride(m_Tha60290081VcgVcatOverride, SinkMstReset);
        mMethodOverride(m_Tha60290081VcgVcatOverride, SourceMstReset);
        mMethodOverride(m_Tha60290081VcgVcatOverride, SinkReset);
        mMethodOverride(m_Tha60290081VcgVcatOverride, SourceReset);
        }

    mMethodsSet(vcg, &m_Tha60290081VcgVcatOverride);
    }

static void Override(AtVcg self)
    {
    OverrideAtVcg(self);
    OverrideThaVcgVcat(self);
    OverrideTha60290081VcgVcat(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081VcgHoVcat);
    }

static AtVcg ObjectInit(AtVcg self, AtModuleConcate concateModule, uint32 vcgId, eAtConcateMemberType memberType)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290081VcgVcatObjectInit(self, concateModule, vcgId, memberType) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtVcg Tha60290081VcgHoVcatNew(AtModuleConcate module, uint32 vcgId, eAtConcateMemberType memberType)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtVcg newVcg = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newVcg == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newVcg, module, vcgId, memberType);
    }

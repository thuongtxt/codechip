/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Concate
 * 
 * File        : Tha60290081VcgMemberInternal.h
 * 
 * Created Date: Jun 18, 2019
 *
 * Description : 60290081 VCG member representation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290081VCGMEMBERINTERNAL_H_
#define _THA60290081VCGMEMBERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/concate/member/ThaVcgMemberInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290081VcgMemberSdhVc
    {
    tThaVcgMemberSdhVc super;
    }tTha60290081VcgMemberSdhVc;

typedef struct tTha60290081VcgMemberSdhVc1x
    {
    tThaVcgMemberSdhVc1x super;
    }tTha60290081VcgMemberSdhVc1x;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THA60290081VCGMEMBERINTERNAL_H_ */


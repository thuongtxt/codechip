/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CONCATE
 * 
 * File        : Tha60290081VcgMember.h
 * 
 * Created Date: Jul 17, 2019
 *
 * Description : 60290081 VCG members
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290081VCGMEMBER_H_
#define _THA60290081VCGMEMBER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtChannel.h"
#include "AtConcateGroup.h"
#include "AtConcateMember.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtConcateMember Tha60290081VcgMemberSdhVcNew(AtConcateGroup vcg, AtChannel channel);
AtConcateMember Tha60290081VcgMemberSdhVc1xNew(AtConcateGroup vcg, AtChannel channel);

/* Defects */
eBool Tha60290081VcgMemberDefectIsSupported(AtConcateMember self, uint32 defectType);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290081VCGMEMBER_H_ */


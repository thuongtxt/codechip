/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Concate
 *
 * File        : Tha60290081VcgMemberSdhVc.c
 *
 * Created Date: Jun 18, 2019
 *
 * Description : 60290081 VCG member for VC-4x/VC-4/VC-3 implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../../generic/concate/AtConcateGroupInternal.h"
#include "../../../../default/man/ThaDevice.h"
#include "../../../../default/concate/ThaModuleConcate.h"
#include "../Tha60290081ModuleConcate.h"
#include "../Tha60290081ModuleHoVcatReg.h"
#include "Tha60290081VcgMemberInternal.h"
#include "Tha60290081VcgMember.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mDefaultOffset(self)        ThaVcgMemberDefaultOffset((ThaVcgMember)self)

#define mSetIntrBitMask(alarmType_, defectMask_, enableMask_, hwMask_)         \
    do {                                                                       \
        if (defectMask_ & alarmType_)                                          \
            regVal = (enableMask_ & alarmType_) ? (regVal | hwMask_) : (regVal & (~hwMask_)); \
    } while(0)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods            m_AtChannelOverride;
static tAtConcateMemberMethods      m_AtConcateMemberOverride;
static tThaVcgMemberMethods         m_ThaVcgMemberOverride;

/* Save super implementation */
static const tAtChannelMethods       *m_AtChannelMethods   = NULL;
static const tAtConcateMemberMethods *m_AtConcateMemberMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtLcasMemberSourceState SourceStateHw2Sw(uint8 hwState)
    {
    switch (hwState)
        {
        case 0: return cAtLcasMemberSourceStateIdle;
        case 1: return cAtLcasMemberSourceStateAdd;
        case 2: return cAtLcasMemberSourceStateRmv;
        case 3: return cAtLcasMemberSourceStateNorm;
        case 4: return cAtLcasMemberSourceStateDnu;
        case 7: return cAtLcasMemberSourceStateFixed;
        default:
            return cAtLcasMemberSourceStateInvl;
        }
    }

static eAtLcasMemberSourceState SourceStateGet(AtConcateMember self)
    {
    uint32 regAddr = cAf6Reg_ho_so_memsta_pen_Base + mDefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleConcate);
    return SourceStateHw2Sw(mRegField(regVal, cAf6_ho_so_memsta_pen_So_FSM_Sta_));
    }

static eAtLcasMemberCtrl SourceControlGet(AtConcateMember self)
    {
    uint32 regAddr = cAf6Reg_ho_so_memsta_pen_Base + mDefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleConcate);
    return mRegField(regVal, cAf6_ho_so_memsta_pen_So_Ctrl_Word_);
    }

static eBool LcasIsEnabled(AtConcateMember self)
    {
    return AtVcgLcasIsEnabled((AtVcg)AtConcateMemberConcateGroupGet(self));
    }

static eAtModuleConcateRet SourceSequenceSet(AtConcateMember self, uint32 sourceSequence)
    {
    uint32 regAddr = cAf6Reg_ho_so_txseq_pen_Base + mDefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleConcate);
    mRegFieldSet(regVal, cAf6_ho_so_txseq_pen_So_TxSeq_Ena_, LcasIsEnabled(self) ? 0 : 1);
    mRegFieldSet(regVal, cAf6_ho_so_txseq_pen_So_TxSeq_, sourceSequence);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static uint32 VcatSourceSequenceGet(AtConcateMember self)
    {
    uint32 regAddr = cAf6Reg_ho_so_txseq_pen_Base + mDefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleConcate);
    return mRegField(regVal, cAf6_ho_so_txseq_pen_So_TxSeq_);
    }

static uint32 LcasSourceSequenceGet(AtConcateMember self)
    {
    uint32 regAddr = cAf6Reg_ho_so_memsta_pen_Base + mDefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleConcate);
    return mRegField(regVal, cAf6_ho_so_memsta_pen_So_Mem_Seq_);
    }

static uint32 SourceSequenceGet(AtConcateMember self)
    {
    if (LcasIsEnabled(self))
        return LcasSourceSequenceGet(self);

    return VcatSourceSequenceGet(self);
    }

static eAtLcasMemberSinkState SinkStateHw2Sw(uint8 hwState)
    {
    switch (hwState)
        {
        case 0: return cAtLcasMemberSinkStateIdle;
        case 1: return cAtLcasMemberSinkStateFail;
        case 2: return cAtLcasMemberSinkStateOk;
        case 3: return cAtLcasMemberSinkStateFixed;
        default:
            return cAtLcasMemberSinkStateInvl;
        }
    }

static eAtLcasMemberSinkState SinkStateGet(AtConcateMember self)
    {
    uint32 regAddr = cAf6Reg_ho_sk_memsta_pen_Base + mDefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleConcate);
    return SinkStateHw2Sw(mRegField(regVal, cAf6_ho_sk_memsta_pen_Sk_FSM_Sta_));
    }

static eAtModuleConcateRet SinkExpectedSequenceSet(AtConcateMember self, uint32 sinkSequence)
    {
    uint32 regAddr = cAf6Reg_ho_sk_expseq_pen_Base + mDefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleConcate);
    mRegFieldSet(regVal, cAf6_ho_sk_expseq_pen_Sk_ExpSeq_, sinkSequence);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static uint32 SinkExpectedSequenceGet(AtConcateMember self)
    {
    uint32 regAddr = cAf6Reg_ho_sk_expseq_pen_Base + mDefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleConcate);
    return mRegField(regVal, cAf6_ho_sk_expseq_pen_Sk_ExpSeq_);
    }

static uint32 SinkSequenceGet(AtConcateMember self)
    {
    uint32 regAddr = cAf6Reg_ho_sk_memsta_pen_Base + mDefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleConcate);
    return mRegField(regVal, cAf6_ho_sk_memsta_pen_Sk_Mem_Seq_);
    }

static eAtLcasMemberCtrl SinkControlGet(AtConcateMember self)
    {
    uint32 regAddr = cAf6Reg_ho_sk_memsta_pen_Base + mDefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleConcate);
    return mRegField(regVal, cAf6_ho_sk_memsta_pen_Sk_Ctrl_Word_);
    }

static Tha60290081ModuleConcate ModuleConcate(ThaVcgMember self)
    {
    return (Tha60290081ModuleConcate)AtChannelModuleGet((AtChannel)self);
    }

static eAtRet TxEnable(AtChannel self, eBool enable)
    {
    uint32 regAddr, regVal;

    regAddr = cAf6Reg_ho_so_memctrl_pen_Base + mDefaultOffset(self);
    regVal = mChannelHwRead(self, regAddr, cAtModuleConcate);
    mRegFieldSet(regVal, cAf6_ho_so_memctrl_pen_So_Mem_Ena_, (enable) ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static eBool TxIsEnabled(AtChannel self)
    {
    uint32 regAddr, regVal;

    regAddr = cAf6Reg_ho_so_memctrl_pen_Base + mDefaultOffset(self);
    regVal = mChannelHwRead(self, regAddr, cAtModuleConcate);
    return mRegField(regVal, cAf6_ho_so_memctrl_pen_So_Mem_Ena_) ? cAtTrue : cAtFalse;
    }

static eAtRet RxEnable(AtChannel self, eBool enable)
    {
    uint32 regAddr, regVal;

    regAddr = cAf6Reg_ho_sk_memctrl_pen_Base + mDefaultOffset(self);
    regVal = mChannelHwRead(self, regAddr, cAtModuleConcate);
    mRegFieldSet(regVal, cAf6_ho_sk_memctrl_pen_Sk_Mem_Ena_, (enable) ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static eBool RxIsEnabled(AtChannel self)
    {
    uint32 regAddr, regVal;

    regAddr = cAf6Reg_ho_sk_memctrl_pen_Base + mDefaultOffset(self);
    regVal = mChannelHwRead(self, regAddr, cAtModuleConcate);
    return mRegField(regVal, cAf6_ho_sk_memctrl_pen_Sk_Mem_Ena_) ? cAtTrue : cAtFalse;
    }

static uint32 SinkDefectHw2Sw(uint32 regVal)
    {
    uint32 alarm = 0;

    if (regVal & cAf6_ho_sk_intrsta_pen_Sk_MFSM_Intr_CrrSta_Mask)
        alarm |= cAtConcateMemberAlarmTypeSinkStateChange;

    if (regVal & cAf6_ho_sk_intrsta_pen_GIDM_Intr_CrrSta_Mask)
        alarm |= cAtConcateMemberAlarmTypeGidM;

    if (regVal & cAf6_ho_sk_intrsta_pen_CRCErr_Intr_CrrSta_Mask)
        alarm |= cAtConcateMemberAlarmTypeCrc;

    if (regVal & cAf6_ho_sk_intrsta_pen_SQNC_Intr_CrrSta_Mask)
        alarm |= cAtConcateMemberAlarmTypeSqnc;

    if (regVal & cAf6_ho_sk_intrsta_pen_MND_Intr_CrrSta_Mask)
        alarm |= cAtConcateMemberAlarmTypeMnd;

    if (regVal & cAf6_ho_sk_intrsta_pen_LOA_Intr_CrrSta_Mask)
        alarm |= cAtConcateMemberAlarmTypeLoa;

    if (regVal & cAf6_ho_sk_intrsta_pen_SQM_Intr_CrrSta_Mask)
        alarm |= cAtConcateMemberAlarmTypeSqm;

    if (regVal & cAf6_ho_sk_intrsta_pen_OOM_Intr_CrrSta_Mask)
        alarm |= cAtConcateMemberAlarmTypeOom;

    if (regVal & cAf6_ho_sk_intrsta_pen_LOM_Intr_CrrSta_Mask)
        alarm |= cAtConcateMemberAlarmTypeLom;

    return alarm;
    }

static uint32 SourceDefectHw2Sw(uint32 regVal)
    {
    uint32 alarm = 0;

    if (regVal & cAf6_ho_so_intrsta_pen_So_MFSM_Intr_CrrSta_Mask)
        alarm |= cAtConcateMemberAlarmTypeSourceStateChange;

    return alarm;
    }

static uint32 SinkDefectGet(AtConcateMember self)
    {
    uint32 regAddr = cAf6Reg_ho_sk_intrsta_pen_Base + mDefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);
    return SinkDefectHw2Sw(regVal);
    }

static uint32 SourceDefectGet(AtConcateMember self)
    {
    uint32 regAddr = cAf6Reg_ho_so_intrsta_pen_Base + mDefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);
    return SourceDefectHw2Sw(regVal);
    }

static uint32 SinkDefectHistoryRead2Clear(AtConcateMember self, eBool read2Clear)
    {
    uint32 regAddr = cAf6Reg_ho_sk_intrstk_pen_Base + mDefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);

    if (read2Clear)
        mChannelHwWrite(self, regAddr, regVal, cAtModuleConcate);

    return SinkDefectHw2Sw(regVal);
    }

static uint32 SinkDefectHistoryGet(AtConcateMember self)
    {
    return SinkDefectHistoryRead2Clear(self, cAtFalse);
    }

static uint32 SinkDefectHistoryClear(AtConcateMember self)
    {
    return SinkDefectHistoryRead2Clear(self, cAtTrue);
    }

static uint32 SourceDefectHistoryRead2Clear(AtConcateMember self, eBool read2Clear)
    {
    uint32 regAddr = cAf6Reg_ho_so_intrstk_pen_Base + mDefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);

    if (read2Clear)
        mChannelHwWrite(self, regAddr, regVal, cAtModuleConcate);

    return SourceDefectHw2Sw(regVal);
    }

static uint32 SourceDefectHistoryGet(AtConcateMember self)
    {
    return SourceDefectHistoryRead2Clear(self, cAtFalse);
    }

static uint32 SourceDefectHistoryClear(AtConcateMember self)
    {
    return SourceDefectHistoryRead2Clear(self, cAtTrue);
    }

static uint32 SinkSupportedInterruptMasks(AtConcateMember self)
    {
    static uint32 supportedMasks = (cAtConcateMemberAlarmTypeSqnc|
                                    cAtConcateMemberAlarmTypeMnd |
                                    cAtConcateMemberAlarmTypeOom |
                                    cAtConcateMemberAlarmTypeLom |
                                    cAtConcateMemberAlarmTypeCrc |
                                    cAtConcateMemberAlarmTypeGidM|
                                    cAtConcateMemberAlarmTypeLoa |
                                    cAtConcateMemberAlarmTypeSqm |
                                    cAtConcateMemberAlarmTypeSinkStateChange);
    AtUnused(self);
    return supportedMasks;
    }

static uint32 SourceSupportedInterruptMasks(AtConcateMember self)
    {
    static uint32 supportedMasks = (cAtConcateMemberAlarmTypeSourceStateChange);
    AtUnused(self);
    return supportedMasks;
    }

static eAtRet SinkInterruptMaskSet(AtConcateMember self, uint32 defectMask, uint32 enableMask)
    {
    uint32 regAddr = cAf6Reg_ho_sk_intrctrl_pen_Base + mDefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);

    mSetIntrBitMask(cAtConcateMemberAlarmTypeSinkStateChange, defectMask, enableMask, cAf6_ho_sk_intrctrl_pen_Sk_MFSM_Intr_En_Mask);
    mSetIntrBitMask(cAtConcateMemberAlarmTypeGidM, defectMask, enableMask, cAf6_ho_sk_intrctrl_pen_GIDM_Intr_En_Mask);
    mSetIntrBitMask(cAtConcateMemberAlarmTypeCrc,  defectMask, enableMask, cAf6_ho_sk_intrctrl_pen_CRCErr_Intr_En_Mask);
    mSetIntrBitMask(cAtConcateMemberAlarmTypeSqnc, defectMask, enableMask, cAf6_ho_sk_intrctrl_pen_SQNC_Intr_En_Mask);
    mSetIntrBitMask(cAtConcateMemberAlarmTypeMnd,  defectMask, enableMask, cAf6_ho_sk_intrctrl_pen_MND_Intr_En_Mask);
    mSetIntrBitMask(cAtConcateMemberAlarmTypeLoa,  defectMask, enableMask, cAf6_ho_sk_intrctrl_pen_LOA_Intr_En_Mask);
    mSetIntrBitMask(cAtConcateMemberAlarmTypeSqm,  defectMask, enableMask, cAf6_ho_sk_intrctrl_pen_SQM_Intr_En_Mask);
    mSetIntrBitMask(cAtConcateMemberAlarmTypeOom,  defectMask, enableMask, cAf6_ho_sk_intrctrl_pen_OOM_Intr_En_Mask);
    mSetIntrBitMask(cAtConcateMemberAlarmTypeLom,  defectMask, enableMask, cAf6_ho_sk_intrctrl_pen_LOM_Intr_En_Mask);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static eAtRet SourceInterruptMaskSet(AtConcateMember self, uint32 defectMask, uint32 enableMask)
    {
    uint32 regAddr = cAf6Reg_ho_so_intrctrl_pen_Base + mDefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);

    mSetIntrBitMask(cAtConcateMemberAlarmTypeSourceStateChange, defectMask, enableMask, cAf6_ho_so_intrctrl_pen_So_MFSM_Intr_En_Mask);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static uint32 SinkInterruptMaskGet(AtConcateMember self)
    {
    uint32 regAddr = cAf6Reg_ho_sk_intrctrl_pen_Base + mDefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);
    return SinkDefectHw2Sw(regVal);
    }

static uint32 SourceInterruptMaskGet(AtConcateMember self)
    {
    uint32 regAddr = cAf6Reg_ho_so_intrctrl_pen_Base + mDefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);
    return SourceDefectHw2Sw(regVal);
    }

static eBool AlarmIsSupported(AtChannel self, uint32 alarmType)
    {
    return Tha60290081VcgMemberDefectIsSupported((AtConcateMember)self, alarmType);
    }

static void SourceInterruptProcess(AtConcateMember self)
    {
    uint32 offset = mDefaultOffset(self);
    uint32 intrMask = mChannelHwRead(self, cAf6Reg_ho_so_intrctrl_pen_Base + offset, cAtModuleConcate);
    uint32 intrStatus = mChannelHwRead(self, cAf6Reg_ho_so_intrstk_pen_Base + offset, cAtModuleConcate);
    uint32 events = 0;
    uint32 defects = 0;

    /* Clear interrupt, then get current status */
    intrStatus &= intrMask;

    /* Clear interrupt */
    mChannelHwWrite(self, cAf6Reg_ho_so_intrstk_pen_Base + offset, intrStatus, cAtModuleConcate);

    /* Source state change */
    if (intrStatus & cAf6_ho_so_intrstk_pen_So_MFSM_Intr_Sta_Mask)
        events |= cAtConcateMemberAlarmTypeSourceStateChange;

    AtChannelAllAlarmListenersCall((AtChannel)self, events, defects);
    }

static void SinkInterruptProcess(AtConcateMember self)
    {
    uint32 offset = mDefaultOffset(self);
    uint32 intrMask = mChannelHwRead(self, cAf6Reg_ho_sk_intrctrl_pen_Base + offset, cAtModuleConcate);
    uint32 intrStatus = mChannelHwRead(self, cAf6Reg_ho_sk_intrstk_pen_Base + offset, cAtModuleConcate);
    uint32 currentStatus;
    uint32 events = 0;
    uint32 defects = 0;

    /* Filter un-masked interrupts */
    intrStatus &= intrMask;

    /* Clear interrupt, then get current status */
    mChannelHwWrite(self, cAf6Reg_ho_sk_intrstk_pen_Base + offset, intrStatus, cAtModuleConcate);
    currentStatus = mChannelHwRead(self, cAf6Reg_ho_sk_intrsta_pen_Base + offset, cAtModuleConcate);

    /* Sink state change */
    if (intrStatus & cAf6_ho_sk_intrstk_pen_Sk_MFSM_Intr_Sta_Mask)
        events |= cAtConcateMemberAlarmTypeSinkStateChange;

    /* GIDM */
    if (intrStatus & cAf6_ho_sk_intrstk_pen_GIDM_Intr_Sta_Mask)
        {
        events |= cAtConcateMemberAlarmTypeGidM;
        if (currentStatus & cAf6_ho_sk_intrstk_pen_GIDM_Intr_Sta_Mask)
            defects |= cAtConcateMemberAlarmTypeGidM;
        }

    /* CRC error */
    if (intrStatus & cAf6_ho_sk_intrstk_pen_CRCErr_Intr_Sta_Mask)
        {
        events |= cAtConcateMemberAlarmTypeCrc;
        if (currentStatus & cAf6_ho_sk_intrstk_pen_CRCErr_Intr_Sta_Mask)
            defects |= cAtConcateMemberAlarmTypeCrc;
        }

    /* SQNC */
    if (intrStatus & cAf6_ho_sk_intrstk_pen_SQNC_Intr_Sta_Mask)
        {
        events |= cAtConcateMemberAlarmTypeSqnc;
        if (currentStatus & cAf6_ho_sk_intrstk_pen_SQNC_Intr_Sta_Mask)
            defects |= cAtConcateMemberAlarmTypeSqnc;
        }

    /* MND */
    if (intrStatus & cAf6_ho_sk_intrstk_pen_MND_Intr_Sta_Mask)
        {
        events |= cAtConcateMemberAlarmTypeMnd;
        if (currentStatus & cAf6_ho_sk_intrstk_pen_MND_Intr_Sta_Mask)
            defects |= cAtConcateMemberAlarmTypeMnd;
        }

    /* LOA */
    if (intrStatus & cAf6_ho_sk_intrstk_pen_LOA_Intr_Sta_Mask)
        {
        events |= cAtConcateMemberAlarmTypeLoa;
        if (currentStatus & cAf6_ho_sk_intrstk_pen_LOA_Intr_Sta_Mask)
            defects |= cAtConcateMemberAlarmTypeLoa;
        }

    /* SQM */
    if (intrStatus & cAf6_ho_sk_intrstk_pen_SQM_Intr_Sta_Mask)
        {
        events |= cAtConcateMemberAlarmTypeSqm;
        if (currentStatus & cAf6_ho_sk_intrstk_pen_SQM_Intr_Sta_Mask)
            defects |= cAtConcateMemberAlarmTypeSqm;
        }

    /* OOM */
    if (intrStatus & cAf6_ho_sk_intrstk_pen_OOM_Intr_Sta_Mask)
        {
        events |= cAtConcateMemberAlarmTypeOom;
        if (currentStatus & cAf6_ho_sk_intrstk_pen_OOM_Intr_Sta_Mask)
            defects |= cAtConcateMemberAlarmTypeOom;
        }

    /* LOM */
    if (intrStatus & cAf6_ho_sk_intrstk_pen_LOM_Intr_Sta_Mask)
        {
        events |= cAtConcateMemberAlarmTypeLom;
        if (currentStatus & cAf6_ho_sk_intrstk_pen_LOM_Intr_Sta_Mask)
            defects |= cAtConcateMemberAlarmTypeLom;
        }

    AtChannelAllAlarmListenersCall((AtChannel)self, events, defects);
    }

static eAtRet SourceAdd(ThaVcgMember self, AtConcateGroup vcg)
    {
    AtUnused(vcg);
    return Tha60290081ModuleConcateSdhVcSourceMemberAdd(ModuleConcate(self), self);
    }

static eAtRet SinkAdd(ThaVcgMember self, AtConcateGroup vcg, eBool lcasEnable)
    {
    AtUnused(vcg);
    return Tha60290081ModuleConcateSdhVcSinkMemberAdd(ModuleConcate(self), self, lcasEnable);
    }

static eAtRet SinkRemove(ThaVcgMember self)
    {
    return Tha60290081ModuleConcateSdhVcSinkMemberRemove(ModuleConcate(self), self);
    }

static eAtRet SourceRemove(ThaVcgMember self)
    {
    return Tha60290081ModuleConcateSdhVcSourceMemberRemove(ModuleConcate(self), self);
    }

static uint32 DefaultOffset(ThaVcgMember self)
    {
    AtSdhChannel channel = (AtSdhChannel)AtConcateMemberChannelGet((AtConcateMember)self);
    uint16 vcatSts;

    if (ThaSdhChannelConcateHwStsGet(channel, AtSdhChannelSts1Get(channel), &vcatSts) != cAtOk)
        return cInvalidUint32;

    return Tha60290081ModuleConcateHoBaseAddress(ModuleConcate(self)) + vcatSts;
    }

static eAtRet SourceReset(ThaVcgMember self)
    {
    uint32 regAddr;
    uint32 offset = mDefaultOffset(self);

    /* Clear member status */
    regAddr = cAf6Reg_ho_so_memsta_pen_Base + offset;
    mChannelHwWrite(self, regAddr, 0, cAtModuleConcate);

    regAddr = cAf6Reg_ho_so_txseq_pen_Base + offset;
    mChannelHwWrite(self, regAddr, 0, cAtModuleConcate);

    return cAtOk;
    }

static eAtRet SinkReset(ThaVcgMember self)
    {
    uint32 regAddr;
    uint32 offset = mDefaultOffset(self);

    /* Clear member status */
    regAddr = cAf6Reg_ho_sk_memsta_pen_Base + offset;
    mChannelHwWrite(self, regAddr, 0, cAtModuleConcate);

    regAddr = cAf6Reg_ho_sk_expseq_pen_Base + offset;
    mChannelHwWrite(self, regAddr, 0, cAtModuleConcate);

    return cAtOk;
    }

static void OverrideAtChannel(AtConcateMember self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, TxEnable);
        mMethodOverride(m_AtChannelOverride, TxIsEnabled);
        mMethodOverride(m_AtChannelOverride, RxEnable);
        mMethodOverride(m_AtChannelOverride, RxIsEnabled);
        mMethodOverride(m_AtChannelOverride, AlarmIsSupported);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtConcateMember(AtConcateMember self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtConcateMemberMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtConcateMemberOverride, m_AtConcateMemberMethods, sizeof(m_AtConcateMemberOverride));

        mMethodOverride(m_AtConcateMemberOverride, SourceStateGet);
        mMethodOverride(m_AtConcateMemberOverride, SourceControlGet);
        mMethodOverride(m_AtConcateMemberOverride, SourceSequenceSet);
        mMethodOverride(m_AtConcateMemberOverride, SourceSequenceGet);
        mMethodOverride(m_AtConcateMemberOverride, SinkStateGet);
        mMethodOverride(m_AtConcateMemberOverride, SinkExpectedSequenceSet);
        mMethodOverride(m_AtConcateMemberOverride, SinkExpectedSequenceGet);
        mMethodOverride(m_AtConcateMemberOverride, SinkSequenceGet);
        mMethodOverride(m_AtConcateMemberOverride, SinkControlGet);
        mMethodOverride(m_AtConcateMemberOverride, SourceDefectGet);
        mMethodOverride(m_AtConcateMemberOverride, SinkDefectGet);
        mMethodOverride(m_AtConcateMemberOverride, SourceDefectHistoryGet);
        mMethodOverride(m_AtConcateMemberOverride, SourceDefectHistoryClear);
        mMethodOverride(m_AtConcateMemberOverride, SinkDefectHistoryGet);
        mMethodOverride(m_AtConcateMemberOverride, SinkDefectHistoryClear);
        mMethodOverride(m_AtConcateMemberOverride, SourceInterruptMaskSet);
        mMethodOverride(m_AtConcateMemberOverride, SourceInterruptMaskGet);
        mMethodOverride(m_AtConcateMemberOverride, SinkInterruptMaskSet);
        mMethodOverride(m_AtConcateMemberOverride, SinkInterruptMaskGet);
        mMethodOverride(m_AtConcateMemberOverride, SourceSupportedInterruptMasks);
        mMethodOverride(m_AtConcateMemberOverride, SinkSupportedInterruptMasks);
        mMethodOverride(m_AtConcateMemberOverride, SourceInterruptProcess);
        mMethodOverride(m_AtConcateMemberOverride, SinkInterruptProcess);
        }

    mMethodsSet(self, &m_AtConcateMemberOverride);
    }

static void OverrideThaVcgMember(AtConcateMember self)
    {
    ThaVcgMember member = (ThaVcgMember)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaVcgMemberOverride, mMethodsGet(member), sizeof(m_ThaVcgMemberOverride));

        mMethodOverride(m_ThaVcgMemberOverride, DefaultOffset);
        mMethodOverride(m_ThaVcgMemberOverride, SourceAdd);
        mMethodOverride(m_ThaVcgMemberOverride, SinkAdd);
        mMethodOverride(m_ThaVcgMemberOverride, SinkRemove);
        mMethodOverride(m_ThaVcgMemberOverride, SourceRemove);
        mMethodOverride(m_ThaVcgMemberOverride, SinkReset);
        mMethodOverride(m_ThaVcgMemberOverride, SourceReset);
        }

    mMethodsSet(member, &m_ThaVcgMemberOverride);
    }

static void Override(AtConcateMember self)
    {
    OverrideAtChannel(self);
    OverrideAtConcateMember(self);
    OverrideThaVcgMember(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081VcgMemberSdhVc);
    }

static AtConcateMember ObjectInit(AtConcateMember self, AtConcateGroup vcg, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaVcgMemberSdhVcObjectInit(self, vcg, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtConcateMember Tha60290081VcgMemberSdhVcNew(AtConcateGroup vcg, AtChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtConcateMember newMember = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newMember == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newMember, vcg, channel);
    }

eBool Tha60290081VcgMemberDefectIsSupported(AtConcateMember self, uint32 defectType)
    {
    static const uint32 cNonSupportedDefects = (cAtConcateMemberAlarmTypeTsf |
                                                cAtConcateMemberAlarmTypeTsd |
                                                cAtConcateMemberAlarmTypeMsu);
    AtUnused(self);
    if (defectType & cNonSupportedDefects)
        return cAtFalse;

    return cAtTrue;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CONCATE
 * 
 * File        : Tha60290081LoVcgMemberCommon.h
 * 
 * Created Date: Sep 5, 2019
 *
 * Description : 60290081 Lo-order VCG member common implementation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290081LOVCGMEMBERCOMMON_H_
#define _THA60290081LOVCGMEMBERCOMMON_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../Tha60290081ModuleLoVcatReg.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define mDefaultOffset(self)        ThaVcgMemberDefaultOffset((ThaVcgMember)self)

#define mSetIntrBitMask(alarmType_, defectMask_, enableMask_, hwMask_)         \
    do {                                                                       \
        if (defectMask_ & alarmType_)                                          \
            regVal = (enableMask_ & alarmType_) ? (regVal | hwMask_) : (regVal & (~hwMask_)); \
    } while(0)

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit;

/* Override */
static tAtObjectMethods             m_AtObjectOverride;
static tAtChannelMethods            m_AtChannelOverride;
static tAtConcateMemberMethods      m_AtConcateMemberOverride;

/* Save super implementation */
static const tAtObjectMethods        *m_AtObjectMethods   = NULL;
static const tAtChannelMethods       *m_AtChannelMethods   = NULL;
static const tAtConcateMemberMethods *m_AtConcateMemberMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static Tha60290081ModuleConcate ModuleConcate(ThaVcgMember self)
    {
    return (Tha60290081ModuleConcate)AtChannelModuleGet((AtChannel)self);
    }

static uint32 DefaultOffset(ThaVcgMember self)
    {
    return Tha60290081ModuleConcateLoMemberEngineOffset(ModuleConcate(self), (AtConcateMember)self);
    }

static eAtLcasMemberSourceState SourceStateHw2Sw(uint8 hwState)
    {
    switch (hwState)
        {
        case 0: return cAtLcasMemberSourceStateIdle;
        case 1: return cAtLcasMemberSourceStateAdd;
        case 2: return cAtLcasMemberSourceStateRmv;
        case 3: return cAtLcasMemberSourceStateNorm;
        case 4: return cAtLcasMemberSourceStateDnu;
        case 7: return cAtLcasMemberSourceStateFixed;
        default:
            return cAtLcasMemberSourceStateInvl;
        }
    }

static eAtRet Init(AtChannel self)
    {
    return m_AtChannelMethods->Init(self);
    }

static eAtRet TxEnable(AtChannel self, eBool enable)
    {
    uint32 regAddr, regVal;

    regAddr = cAf6Reg_map_source_mem_ctrl + mDefaultOffset(self);
    regVal = mChannelHwRead(self, regAddr, cAtModuleConcate);
    mRegFieldSet(regVal, cAf6_map_source_mem_ctrl_SoMemEna_, (enable) ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static eBool TxIsEnabled(AtChannel self)
    {
    uint32 regAddr, regVal;

    regAddr = cAf6Reg_map_source_mem_ctrl + mDefaultOffset(self);
    regVal = mChannelHwRead(self, regAddr, cAtModuleConcate);
    return mRegField(regVal, cAf6_map_source_mem_ctrl_SoMemEna_) ? cAtTrue : cAtFalse;
    }

static eAtRet RxEnable(AtChannel self, eBool enable)
    {
    uint32 regAddr, regVal;

    regAddr = cAf6Reg_map_sink_mem_ctrl + mDefaultOffset(self);
    regVal = mChannelHwRead(self, regAddr, cAtModuleConcate);
    mRegFieldSet(regVal, cAf6_map_sink_mem_ctrl_SkMemEna_, (enable) ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static eBool RxIsEnabled(AtChannel self)
    {
    uint32 regAddr, regVal;

    regAddr = cAf6Reg_map_sink_mem_ctrl + mDefaultOffset(self);
    regVal = mChannelHwRead(self, regAddr, cAtModuleConcate);
    return mRegField(regVal, cAf6_map_sink_mem_ctrl_SkMemEna_) ? cAtTrue : cAtFalse;
    }

static eBool AlarmIsSupported(AtChannel self, uint32 alarmType)
    {
    return Tha60290081VcgMemberDefectIsSupported((AtConcateMember)self, alarmType);
    }

static eAtLcasMemberSourceState SourceStateGet(AtConcateMember self)
    {
    uint32 regAddr = cAf6Reg_map_source_memsta + mDefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleConcate);
    return SourceStateHw2Sw(mRegField(regVal, cAf6_map_source_memsta_SoMemFsm_));
    }

static eAtLcasMemberCtrl SourceControlGet(AtConcateMember self)
    {
    uint32 regAddr = cAf6Reg_map_source_memsta + mDefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleConcate);
    return mRegField(regVal, cAf6_map_source_memsta_SoMemTxCw_);
    }

static eBool LcasIsEnabled(AtConcateMember self)
    {
    return AtVcgLcasIsEnabled((AtVcg)AtConcateMemberConcateGroupGet(self));
    }

static eAtModuleConcateRet SourceSequenceSet(AtConcateMember self, uint32 sourceSequence)
    {
    uint32 regAddr = cAf6Reg_map_source_txseq_config + mDefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleConcate);
    mRegFieldSet(regVal, cAf6_map_source_txseq_config_SoTxSeqEna_, LcasIsEnabled(self) ? 0 : 1);
    mRegFieldSet(regVal, cAf6_map_source_txseq_config_SoTxSeq_, sourceSequence);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static uint32 VcatSourceSequenceGet(AtConcateMember self)
    {
    uint32 regAddr = cAf6Reg_map_source_txseq_config + mDefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleConcate);
    return mRegField(regVal, cAf6_map_source_txseq_config_SoTxSeq_);
    }

static uint32 LcasSourceSequenceGet(AtConcateMember self)
    {
    uint32 regAddr = cAf6Reg_map_source_memsta + mDefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleConcate);
    return mRegField(regVal, cAf6_map_source_memsta_SoMemTxSeq_);
    }

static uint32 SourceSequenceGet(AtConcateMember self)
    {
    if (LcasIsEnabled(self))
        return LcasSourceSequenceGet(self);

    return VcatSourceSequenceGet(self);
    }

static eAtLcasMemberSinkState SinkStateHw2Sw(uint8 hwState)
    {
    switch (hwState)
        {
        case 0: return cAtLcasMemberSinkStateIdle;
        case 1: return cAtLcasMemberSinkStateFail;
        case 2: return cAtLcasMemberSinkStateOk;
        case 3: return cAtLcasMemberSinkStateFixed;
        default:
            return cAtLcasMemberSinkStateInvl;
        }
    }

static eAtLcasMemberSinkState SinkStateGet(AtConcateMember self)
    {
    uint32 regAddr = cAf6Reg_map_sink_memsta + mDefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleConcate);
    return SinkStateHw2Sw(mRegField(regVal, cAf6_map_sink_memsta_SkMemFsm_));
    }

static uint32 SinkSequenceGet(AtConcateMember self)
    {
    uint32 regAddr = cAf6Reg_map_sink_memsta + mDefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleConcate);
    return mRegField(regVal, cAf6_map_sink_memsta_SkMemAcpSeq_);
    }

static eAtLcasMemberCtrl SinkControlGet(AtConcateMember self)
    {
    uint32 regAddr = cAf6Reg_map_sink_memsta + mDefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleConcate);
    return mRegField(regVal, cAf6_map_sink_memsta_SkMemRxCw_);
    }

static eAtRet SourceReset(ThaVcgMember self)
    {
    uint32 regAddr;

    /* Clear member status */
    regAddr = cAf6Reg_map_source_memsta + mDefaultOffset(self);
    mChannelHwWrite(self, regAddr, 0, cAtModuleConcate);

    /* Clear VCG status */
    regAddr = cAf6Reg_map_source_vcgsta + mDefaultOffset(self);
    mChannelHwWrite(self, regAddr, 0, cAtModuleConcate);

    return cAtOk;
    }

static eAtRet SinkReset(ThaVcgMember self)
    {
    uint32 regAddr;

    /* Clear member status */
    regAddr = cAf6Reg_map_sink_memsta + mDefaultOffset(self);
    mChannelHwWrite(self, regAddr, 0, cAtModuleConcate);

    /* Clear VCG status */
    regAddr = cAf6Reg_map_sink_vcgsta + mDefaultOffset(self);
    mChannelHwWrite(self, regAddr, 0, cAtModuleConcate);

    return cAtOk;
    }

static uint32 SourceDefectHw2Sw(uint32 regVal)
    {
    uint32 alarm = 0;

    if (regVal & cAf6_map_source_chnint_sta_SoStaChgIntrCurSta_Mask)
        alarm |= cAtConcateMemberAlarmTypeSourceStateChange;

    return alarm;
    }

static uint32 SourceDefectGet(AtConcateMember self)
    {
    uint32 regAddr = cAf6Reg_map_source_chnint_sta_Base + mDefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);
    return SourceDefectHw2Sw(regVal);
    }

static uint32 SinkDefectHw2Sw(uint32 regVal)
    {
    uint32 alarm = 0;

    if (regVal & cAf6_map_sink_chnint_sta_SkStaChgIntrCurSta_Mask)
        alarm |= cAtConcateMemberAlarmTypeSinkStateChange;

    if (regVal & cAf6_map_sink_chnint_sta_SkGIDMIntrCurSta_Mask)
        alarm |= cAtConcateMemberAlarmTypeGidM;

    if (regVal & cAf6_map_sink_chnint_sta_SkCRCErrIntrCurSta_Mask)
        alarm |= cAtConcateMemberAlarmTypeCrc;

    if (regVal & cAf6_map_sink_chnint_sta_SkSQNCIntrCurSta_Mask)
        alarm |= cAtConcateMemberAlarmTypeSqnc;

    if (regVal & cAf6_map_sink_chnint_sta_SkMNDIntrCurSta_Mask)
        alarm |= cAtConcateMemberAlarmTypeMnd;

    if (regVal & cAf6_map_sink_chnint_sta_SkLOAIntrCurSta_Mask)
        alarm |= cAtConcateMemberAlarmTypeLoa;

    if (regVal & cAf6_map_sink_chnint_sta_SkSQMIntrCurSta_Mask)
        alarm |= cAtConcateMemberAlarmTypeSqm;

    if (regVal & cAf6_map_sink_chnint_sta_SkOOMIntrCurSta_Mask)
        alarm |= cAtConcateMemberAlarmTypeOom;

    if (regVal & cAf6_map_sink_chnint_sta_SkLOMIntrCurSta_Mask)
        alarm |= cAtConcateMemberAlarmTypeLom;

    return alarm;
    }

static uint32 SinkDefectGet(AtConcateMember self)
    {
    uint32 regAddr = cAf6Reg_map_sink_chnint_sta + mDefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);
    return SinkDefectHw2Sw(regVal);
    }

static uint32 SourceDefectHistoryRead2Clear(AtConcateMember self, eBool read2Clear)
    {
    uint32 regAddr = cAf6Reg_map_source_chnint_event_Base + mDefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);

    if (read2Clear)
        mChannelHwWrite(self, regAddr, regVal, cAtModuleConcate);

    return SourceDefectHw2Sw(regVal);
    }

static uint32 SourceDefectHistoryGet(AtConcateMember self)
    {
    return SourceDefectHistoryRead2Clear(self, cAtFalse);
    }

static uint32 SourceDefectHistoryClear(AtConcateMember self)
    {
    return SourceDefectHistoryRead2Clear(self, cAtTrue);
    }

static uint32 SinkDefectHistoryRead2Clear(AtConcateMember self, eBool read2Clear)
    {
    uint32 regAddr = cAf6Reg_map_sink_chnint_event + mDefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);

    if (read2Clear)
        mChannelHwWrite(self, regAddr, regVal, cAtModuleConcate);

    return SinkDefectHw2Sw(regVal);
    }

static uint32 SinkDefectHistoryGet(AtConcateMember self)
    {
    return SinkDefectHistoryRead2Clear(self, cAtFalse);
    }

static uint32 SinkDefectHistoryClear(AtConcateMember self)
    {
    return SinkDefectHistoryRead2Clear(self, cAtTrue);
    }

static eAtRet SourceInterruptMaskSet(AtConcateMember self, uint32 defectMask, uint32 enableMask)
    {
    uint32 regAddr = cAf6Reg_map_source_chnint_en_Base + mDefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);

    mSetIntrBitMask(cAtConcateMemberAlarmTypeSourceStateChange, defectMask, enableMask, cAf6_map_source_chnint_en_SoStaChgIntren_Mask);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static uint32 SourceInterruptMaskGet(AtConcateMember self)
    {
    uint32 regAddr = cAf6Reg_map_source_chnint_en_Base + mDefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);
    return SourceDefectHw2Sw(regVal);
    }

static eAtRet SinkInterruptMaskSet(AtConcateMember self, uint32 defectMask, uint32 enableMask)
    {
    uint32 regAddr = cAf6Reg_map_sink_chnint_en + mDefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);

    mSetIntrBitMask(cAtConcateMemberAlarmTypeSinkStateChange, defectMask, enableMask, cAf6_map_sink_chnint_en_SkStaChgIntren_Mask);
    mSetIntrBitMask(cAtConcateMemberAlarmTypeGidM, defectMask, enableMask, cAf6_map_sink_chnint_en_SkGIDMIntren_Mask);
    mSetIntrBitMask(cAtConcateMemberAlarmTypeCrc,  defectMask, enableMask, cAf6_map_sink_chnint_en_SkCRCErrIntren_Mask);
    mSetIntrBitMask(cAtConcateMemberAlarmTypeSqnc, defectMask, enableMask, cAf6_map_sink_chnint_en_SkSQNCIntren_Mask);
    mSetIntrBitMask(cAtConcateMemberAlarmTypeMnd,  defectMask, enableMask, cAf6_map_sink_chnint_en_SkMNDIntren_Mask);
    mSetIntrBitMask(cAtConcateMemberAlarmTypeLoa,  defectMask, enableMask, cAf6_map_sink_chnint_en_SkLOAIntren_Mask);
    mSetIntrBitMask(cAtConcateMemberAlarmTypeSqm,  defectMask, enableMask, cAf6_map_sink_chnint_en_SkSQMIntren_Mask);
    mSetIntrBitMask(cAtConcateMemberAlarmTypeOom,  defectMask, enableMask, cAf6_map_sink_chnint_en_SkOOMIntren_Mask);
    mSetIntrBitMask(cAtConcateMemberAlarmTypeLom,  defectMask, enableMask, cAf6_map_sink_chnint_en_SkLOMIntren_Mask);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static uint32 SinkInterruptMaskGet(AtConcateMember self)
    {
    uint32 regAddr = cAf6Reg_map_sink_chnint_en + mDefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);
    return SinkDefectHw2Sw(regVal);
    }

static uint32 SourceSupportedInterruptMasks(AtConcateMember self)
    {
    static uint32 supportedMasks = (cAtConcateMemberAlarmTypeSourceStateChange);
    AtUnused(self);
    return supportedMasks;
    }

static uint32 SinkSupportedInterruptMasks(AtConcateMember self)
    {
    static uint32 supportedMasks = (cAtConcateMemberAlarmTypeSqnc|
                                    cAtConcateMemberAlarmTypeMnd |
                                    cAtConcateMemberAlarmTypeOom |
                                    cAtConcateMemberAlarmTypeLom |
                                    cAtConcateMemberAlarmTypeCrc |
                                    cAtConcateMemberAlarmTypeGidM|
                                    cAtConcateMemberAlarmTypeLoa |
                                    cAtConcateMemberAlarmTypeSqm |
                                    cAtConcateMemberAlarmTypeSinkStateChange);
    AtUnused(self);
    return supportedMasks;
    }

static eAtModuleConcateRet SinkExpectedSequenceSet(AtConcateMember self, uint32 sinkSequence)
    {
    uint32 regAddr = cAf6Reg_map_sink_expseq_conf + mDefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleConcate);
    mRegFieldSet(regVal, cAf6_map_sink_expseq_conf_SkTxSeq_, sinkSequence);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static uint32 SinkExpectedSequenceGet(AtConcateMember self)
    {
    uint32 regAddr = cAf6Reg_map_sink_expseq_conf + mDefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleConcate);
    return mRegField(regVal, cAf6_map_sink_expseq_conf_SkTxSeq_);
    }


static void SourceInterruptProcess(AtConcateMember self)
    {
    uint32 offset = mDefaultOffset(self);
    uint32 intrMask = mChannelHwRead(self, cAf6Reg_map_source_chnint_en_Base + offset, cAtModuleConcate);
    uint32 intrStatus = mChannelHwRead(self, cAf6Reg_map_source_chnint_event_Base + offset, cAtModuleConcate);
    uint32 events = 0;
    uint32 defects = 0;

    /* Clear interrupt, then get current status */
    intrStatus &= intrMask;

    /* Clear interrupt */
    mChannelHwWrite(self, cAf6Reg_map_source_chnint_event_Base + offset, intrStatus, cAtModuleConcate);

    /* Source state change */
    if (intrStatus & cAf6_map_source_chnint_event_SoStaChgIntrEvn_Mask)
        events |= cAtConcateMemberAlarmTypeSourceStateChange;

    AtChannelAllAlarmListenersCall((AtChannel)self, events, defects);
    }

static void SinkInterruptProcess(AtConcateMember self)
    {
    uint32 offset = mDefaultOffset(self);
    uint32 intrMask = mChannelHwRead(self, cAf6Reg_map_sink_chnint_en + offset, cAtModuleConcate);
    uint32 intrStatus = mChannelHwRead(self, cAf6Reg_map_sink_chnint_event + offset, cAtModuleConcate);
    uint32 currentStatus;
    uint32 events = 0;
    uint32 defects = 0;

    /* Filter un-masked interrupts */
    intrStatus &= intrMask;

    /* Clear interrupt, then get current status */
    mChannelHwWrite(self, cAf6Reg_map_sink_chnint_event + offset, intrStatus, cAtModuleConcate);
    currentStatus = mChannelHwRead(self, cAf6Reg_map_sink_chnint_sta + offset, cAtModuleConcate);

    /* Sink state change */
    if (intrStatus & cAf6_map_sink_chnint_event_SkStaChgIntrEvn_Mask)
        events |= cAtConcateMemberAlarmTypeSinkStateChange;

    /* GIDM */
    if (intrStatus & cAf6_map_sink_chnint_event_SkGIDMIntrEvn_Mask)
        {
        events |= cAtConcateMemberAlarmTypeGidM;
        if (currentStatus & cAf6_map_sink_chnint_event_SkGIDMIntrEvn_Mask)
            defects |= cAtConcateMemberAlarmTypeGidM;
        }

    /* CRC error */
    if (intrStatus & cAf6_map_sink_chnint_event_SkCRCErrIntrEvn_Mask)
        {
        events |= cAtConcateMemberAlarmTypeCrc;
        if (currentStatus & cAf6_map_sink_chnint_event_SkCRCErrIntrEvn_Mask)
            defects |= cAtConcateMemberAlarmTypeCrc;
        }

    /* SQNC */
    if (intrStatus & cAf6_map_sink_chnint_event_SkSQNCIntrEvn_Mask)
        {
        events |= cAtConcateMemberAlarmTypeSqnc;
        if (currentStatus & cAf6_map_sink_chnint_event_SkSQNCIntrEvn_Mask)
            defects |= cAtConcateMemberAlarmTypeSqnc;
        }

    /* MND */
    if (intrStatus & cAf6_map_sink_chnint_event_SkMNDIntrEvn_Mask)
        {
        events |= cAtConcateMemberAlarmTypeMnd;
        if (currentStatus & cAf6_map_sink_chnint_event_SkMNDIntrEvn_Mask)
            defects |= cAtConcateMemberAlarmTypeMnd;
        }

    /* LOA */
    if (intrStatus & cAf6_map_sink_chnint_event_SkLOAIntrEvn_Mask)
        {
        events |= cAtConcateMemberAlarmTypeLoa;
        if (currentStatus & cAf6_map_sink_chnint_event_SkLOAIntrEvn_Mask)
            defects |= cAtConcateMemberAlarmTypeLoa;
        }

    /* SQM */
    if (intrStatus & cAf6_map_sink_chnint_event_SkSQMIntrEvn_Mask)
        {
        events |= cAtConcateMemberAlarmTypeSqm;
        if (currentStatus & cAf6_map_sink_chnint_event_SkSQMIntrEvn_Mask)
            defects |= cAtConcateMemberAlarmTypeSqm;
        }

    /* OOM */
    if (intrStatus & cAf6_map_sink_chnint_event_SkOOMIntrEvn_Mask)
        {
        events |= cAtConcateMemberAlarmTypeOom;
        if (currentStatus & cAf6_map_sink_chnint_event_SkOOMIntrEvn_Mask)
            defects |= cAtConcateMemberAlarmTypeOom;
        }

    /* LOM */
    if (intrStatus & cAf6_map_sink_chnint_event_SkLOMIntrEvn_Mask)
        {
        events |= cAtConcateMemberAlarmTypeLom;
        if (currentStatus & cAf6_map_sink_chnint_event_SkLOMIntrEvn_Mask)
            defects |= cAtConcateMemberAlarmTypeLom;
        }

    AtChannelAllAlarmListenersCall((AtChannel)self, events, defects);
    }

static eAtRet MemberHwIdAllocate(AtConcateMember self)
    {
    uint32 allocHwId;

    if (AtChannelHwIdGet((AtChannel)self) != cInvalidUint32)
        return cAtOk;

    allocHwId = ThaVcgMemberHwIdAllocate((ThaVcgMember)self);
    if (allocHwId == cInvalidUint32)
        return cAtErrorRsrcNoAvail;

    ThaVcgMemberHwIdSet((ThaVcgMember)self, allocHwId);
    return cAtOk;
    }

static eAtModuleConcateRet SourceInit(AtConcateMember self)
    {
    eAtRet ret;

    ret = MemberHwIdAllocate(self);
    if (ret != cAtOk)
        return ret;

    return m_AtConcateMemberMethods->SourceInit(self);
    }

static eAtModuleConcateRet SinkInit(AtConcateMember self)
    {
    eAtRet ret;

    ret = MemberHwIdAllocate(self);
    if (ret != cAtOk)
        return ret;

    return m_AtConcateMemberMethods->SinkInit(self);
    }

static void MemberHwIdDeAllocate(ThaVcgMember self)
    {
    ThaVcgMemberHwIdDeAllocate(self);
    ThaVcgMemberHwIdSet(self, cInvalidUint32);
    }

static void Delete(AtObject self)
    {
    MemberHwIdDeAllocate((ThaVcgMember)self);
    m_AtObjectMethods->Delete(self);
    }

static void OverrideAtObject(AtConcateMember self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtConcateMember self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, TxEnable);
        mMethodOverride(m_AtChannelOverride, TxIsEnabled);
        mMethodOverride(m_AtChannelOverride, RxEnable);
        mMethodOverride(m_AtChannelOverride, RxIsEnabled);
        mMethodOverride(m_AtChannelOverride, AlarmIsSupported);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtConcateMember(AtConcateMember self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtConcateMemberMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtConcateMemberOverride, m_AtConcateMemberMethods, sizeof(m_AtConcateMemberOverride));

        mMethodOverride(m_AtConcateMemberOverride, SourceStateGet);
        mMethodOverride(m_AtConcateMemberOverride, SourceControlGet);
        mMethodOverride(m_AtConcateMemberOverride, SourceSequenceSet);
        mMethodOverride(m_AtConcateMemberOverride, SourceSequenceGet);
        mMethodOverride(m_AtConcateMemberOverride, SinkStateGet);
        mMethodOverride(m_AtConcateMemberOverride, SinkExpectedSequenceSet);
        mMethodOverride(m_AtConcateMemberOverride, SinkExpectedSequenceGet);
        mMethodOverride(m_AtConcateMemberOverride, SinkSequenceGet);
        mMethodOverride(m_AtConcateMemberOverride, SinkControlGet);
        mMethodOverride(m_AtConcateMemberOverride, SourceDefectGet);
        mMethodOverride(m_AtConcateMemberOverride, SinkDefectGet);
        mMethodOverride(m_AtConcateMemberOverride, SourceDefectHistoryGet);
        mMethodOverride(m_AtConcateMemberOverride, SourceDefectHistoryClear);
        mMethodOverride(m_AtConcateMemberOverride, SinkDefectHistoryGet);
        mMethodOverride(m_AtConcateMemberOverride, SinkDefectHistoryClear);
        mMethodOverride(m_AtConcateMemberOverride, SourceInterruptMaskSet);
        mMethodOverride(m_AtConcateMemberOverride, SourceInterruptMaskGet);
        mMethodOverride(m_AtConcateMemberOverride, SinkInterruptMaskSet);
        mMethodOverride(m_AtConcateMemberOverride, SinkInterruptMaskGet);
        mMethodOverride(m_AtConcateMemberOverride, SourceSupportedInterruptMasks);
        mMethodOverride(m_AtConcateMemberOverride, SinkSupportedInterruptMasks);
        mMethodOverride(m_AtConcateMemberOverride, SourceInterruptProcess);
        mMethodOverride(m_AtConcateMemberOverride, SinkInterruptProcess);
        mMethodOverride(m_AtConcateMemberOverride, SourceInit);
        mMethodOverride(m_AtConcateMemberOverride, SinkInit);
        }

    mMethodsSet(self, &m_AtConcateMemberOverride);
    }

#ifdef __cplusplus
}
#endif
#endif /* _THA60290081LOVCGMEMBERCOMMON_H_ */


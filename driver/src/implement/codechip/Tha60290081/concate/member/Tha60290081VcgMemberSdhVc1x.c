/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Concate
 *
 * File        : Tha60290081VcgMemberSdhVc1x.c
 *
 * Created Date: Jul 26, 2019
 *
 * Description : 60290081 VCG member for Vc1x implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../Tha60290081ModuleConcate.h"
#include "Tha60290081VcgMember.h"
#include "Tha60290081VcgMemberInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaVcgMemberMethods         m_ThaVcgMemberOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
#include "Tha60290081LoVcgMemberCommon.h"

static uint32 HwIdAllocate(ThaVcgMember self)
    {
    return Tha60290081ModuleConcateVc1xHwIdAllocate(ModuleConcate(self), (AtConcateMember)self);
    }

static void HwIdDeAllocate(ThaVcgMember self)
    {
    Tha60290081ModuleConcateVc1xHwIdDeAllocate(ModuleConcate(self), (AtConcateMember)self);
    }

static void OverrideThaVcgMember(AtConcateMember self)
    {
    ThaVcgMember member = (ThaVcgMember)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaVcgMemberOverride, mMethodsGet(member), sizeof(m_ThaVcgMemberOverride));

        mMethodOverride(m_ThaVcgMemberOverride, DefaultOffset);
        mMethodOverride(m_ThaVcgMemberOverride, SinkReset);
        mMethodOverride(m_ThaVcgMemberOverride, SourceReset);
        mMethodOverride(m_ThaVcgMemberOverride, HwIdAllocate);
        mMethodOverride(m_ThaVcgMemberOverride, HwIdDeAllocate);
        }

    mMethodsSet(member, &m_ThaVcgMemberOverride);
    }

static void Override(AtConcateMember self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtConcateMember(self);
    OverrideThaVcgMember(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081VcgMemberSdhVc1x);
    }

static AtConcateMember ObjectInit(AtConcateMember self, AtConcateGroup vcg, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaVcgMemberSdhVc1xObjectInit(self, vcg, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtConcateMember Tha60290081VcgMemberSdhVc1xNew(AtConcateGroup vcg, AtChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtConcateMember newMember = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newMember == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newMember, vcg, channel);
    }

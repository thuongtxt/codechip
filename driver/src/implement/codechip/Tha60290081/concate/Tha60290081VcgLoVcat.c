/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CONCATE
 *
 * File        : Tha60290081VcgLoVcat.c
 *
 * Created Date: Jul 29, 2019
 *
 * Description : 60290081 LO VCAT VCG implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "Tha60290081ModuleConcate.h"
#include "Tha60290081ModuleLoVcatReg.h"
#include "Tha60290081VcgVcatInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtVcgMethods                m_AtVcgOverride;
static tThaVcgVcatMethods           m_ThaVcgVcatOverride;
static tTha60290081VcgVcatMethods   m_Tha60290081VcgVcatOverride;

/* Save super implementation */
static const tAtVcgMethods          *m_AtVcgVcatMethods = NULL;
static const tThaVcgVcatMethods     *m_ThaVcgVcatMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static Tha60290081ModuleConcate ModuleConcate(AtVcg self)
    {
    return (Tha60290081ModuleConcate)AtChannelModuleGet((AtChannel)self);
    }

static uint32 BaseAddress(AtVcg self)
    {
    return Tha60290081ModuleConcateLoBaseAddress(ModuleConcate(self));
    }

static uint32 DefaultOffset(AtVcg self)
    {
    return BaseAddress(self) + AtChannelHwIdGet((AtChannel)self);
    }

static uint32 TimerSw2Hw(uint32 timerInMs, uint32 timerUnitInMs)
    {
    uint32 value;

    if (timerUnitInMs == 0)
        return 0;

    value = timerInMs / timerUnitInMs;
    if (timerInMs % timerUnitInMs)
        value += 1U;

    return value;
    }

static uint32 TimerHw2Sw(uint32 hwValue, uint32 timerUnitInMs)
    {
    return hwValue * timerUnitInMs;
    }

static uint32 HoldTimerUnitInMs(AtVcg self)
    {
    uint8 memberType = AtConcateGroupMemberTypeGet((AtConcateGroup)self);
    switch (memberType)
        {
        case cAtConcateMemberTypeE3:    return 100;
        case cAtConcateMemberTypeDs3:   return 100; /* Accuracy 100.444 ms */
        case cAtConcateMemberTypeDs1:   return 96;
        case cAtConcateMemberTypeE1:    return 96;
        case cAtConcateMemberTypeVc12:  return 96;
        case cAtConcateMemberTypeVc11:  return 96;

        default: break;
        }

    return 100;
    }

static eAtModuleConcateRet HoldOffTimerSet(AtVcg self, uint32 timerInMs)
    {
    uint32 regAddr = cAf6Reg_map_sink_thres_conf + DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);
    mRegFieldSet(regVal, cAf6_map_sink_thres_conf_SkHODis_, (timerInMs) ? 0 : 1);
    mRegFieldSet(regVal, cAf6_map_sink_thres_conf_SkHOThres_, TimerSw2Hw(timerInMs, HoldTimerUnitInMs(self)));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static uint32 HoldOffTimerGet(AtVcg self)
    {
    uint32 regAddr = cAf6Reg_map_sink_thres_conf + DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);
    uint32 hwValue = mRegField(regVal, cAf6_map_sink_thres_conf_SkHOThres_);

    if (mRegField(regVal, cAf6_map_sink_thres_conf_SkHODis_))
        return 0;

    return TimerHw2Sw(hwValue, HoldTimerUnitInMs(self));
    }

static uint32 WtrTimerUnitInMs(AtVcg self)
    {
    return HoldTimerUnitInMs(self);
    }

static eAtModuleConcateRet WtrTimerSet(AtVcg self, uint32 timerInMs)
    {
    uint32 regAddr = cAf6Reg_map_sink_thres_conf + DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);
    mRegFieldSet(regVal, cAf6_map_sink_thres_conf_SkWTRDis_, (timerInMs) ? 0 : 1);
    mRegFieldSet(regVal, cAf6_map_sink_thres_conf_SkWTRThres_, TimerSw2Hw(timerInMs, WtrTimerUnitInMs(self)));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static uint32 WtrTimerGet(AtVcg self)
    {
    uint32 regAddr = cAf6Reg_map_sink_thres_conf + DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);
    uint32 hwValue = mRegField(regVal, cAf6_map_sink_thres_conf_SkWTRThres_);

    if (mRegField(regVal, cAf6_map_sink_thres_conf_SkWTRDis_))
        return 0;

    return TimerHw2Sw(hwValue, WtrTimerUnitInMs(self));
    }

static uint32 RmvTimerUnitInMs(AtVcg self)
    {
    uint8 memberType = AtConcateGroupMemberTypeGet((AtConcateGroup)self);
    switch (memberType)
        {
        case cAtConcateMemberTypeE3:    return 2;
        case cAtConcateMemberTypeDs3:   return 2; /* Accuracy 100.444 ms */
        case cAtConcateMemberTypeDs1:   return 48;
        case cAtConcateMemberTypeE1:    return 32;
        case cAtConcateMemberTypeVc12:  return 16;
        case cAtConcateMemberTypeVc11:  return 16;

        default: break;
        }

    return 100;
    }

static eAtModuleConcateRet RmvTimerSet(AtVcg self, uint32 timerInMs)
    {
    uint32 regAddr = cAf6Reg_map_sink_thres_conf + DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);
    mRegFieldSet(regVal, cAf6_map_sink_thres_conf_SkRMVDis_, (timerInMs) ? 0 : 1);
    mRegFieldSet(regVal, cAf6_map_sink_thres_conf_SkRMVThres_, TimerSw2Hw(timerInMs, RmvTimerUnitInMs(self)));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static uint32 RmvTimerGet(AtVcg self)
    {
    uint32 regAddr = cAf6Reg_map_sink_thres_conf + DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);
    uint32 hwValue = mRegField(regVal, cAf6_map_sink_thres_conf_SkRMVThres_);

    if (mRegField(regVal, cAf6_map_sink_thres_conf_SkRMVDis_))
        return 0;

    return TimerHw2Sw(hwValue, RmvTimerUnitInMs(self));
    }

static uint8 SourceRsAckGet(AtVcg self)
    {
    uint32 regAddr = cAf6Reg_map_rsack_sta_Base + DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);
    return (uint8)mRegField(regVal, cAf6_map_rsack_sta_RxRsAck_);
    }

static uint8 SinkRsAckGet(AtVcg self)
    {
    uint32 regAddr = cAf6Reg_map_rsack_sta_Base + DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);
    return (uint8)mRegField(regVal, cAf6_map_rsack_sta_TxRsAck_);
    }

static uint8 SinkMstGet(AtVcg self, AtConcateMember member)
    {
    uint32 regAddr = cAf6Reg_map_sink_txmst + DefaultOffset(self);
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 sqValue = AtConcateMemberSinkSequenceGet(member);
    uint32 mstValShift = sqValue % 32U;
    uint32 mstValMask = cBit0 << mstValShift;
    uint32 mstWord = sqValue / 32U;

    mChannelHwLongRead(self, regAddr, longRegVal, cThaLongRegMaxSize, cAtModuleConcate);
    return (uint8)mRegField(longRegVal[mstWord], mstVal);
    }

static uint8 SourceMstGet(AtVcg self, AtConcateMember member)
    {
    uint32 regAddr = cAf6Reg_map_sink_rxmst + DefaultOffset(self);
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 sqValue = AtConcateMemberSourceSequenceGet(member);
    uint32 mstValShift = sqValue % 32U;
    uint32 mstValMask = cBit0 << mstValShift;
    uint32 mstWord = sqValue / 32U;

    mChannelHwLongRead(self, regAddr, longRegVal, cThaLongRegMaxSize, cAtModuleConcate);
    return (uint8)mRegField(longRegVal[mstWord], mstVal);
    }

static eAtRet SourceReset(Tha60290081VcgVcat self)
    {
    uint32 regAddr;

    /* Clear VCG status */
    regAddr = cAf6Reg_map_source_vcgsta + DefaultOffset((AtVcg)self);
    mChannelHwWrite(self, regAddr, 0, cAtModuleConcate);

    return cAtOk;
    }

static eAtRet SinkReset(Tha60290081VcgVcat self)
    {
    uint32 regAddr;

    /* Clear VCG status */
    regAddr = cAf6Reg_map_sink_vcgsta + DefaultOffset((AtVcg)self);
    mChannelHwWrite(self, regAddr, 0, cAtModuleConcate);

    return cAtOk;
    }

static eAtRet SinkMstReset(Tha60290081VcgVcat self)
    {
    uint32 regAddr = cAf6Reg_map_sink_txmst + DefaultOffset((AtVcg)self);
    uint32 longRegVal[cThaLongRegMaxSize];

    AtOsalMemInit(longRegVal, 0xFFFFFFFF, sizeof(longRegVal));
    mChannelHwLongWrite(self, regAddr, longRegVal, cThaLongRegMaxSize, cAtModuleConcate);

    return cAtOk;
    }

static eAtRet SourceMstReset(Tha60290081VcgVcat self)
    {
    uint32 regAddr = cAf6Reg_map_sink_rxmst + DefaultOffset((AtVcg)self);
    uint32 longRegVal[cThaLongRegMaxSize];

    AtOsalMemInit(longRegVal, 0xFFFFFFFF, sizeof(longRegVal));
    mChannelHwLongWrite(self, regAddr, longRegVal, cThaLongRegMaxSize, cAtModuleConcate);

    return cAtOk;
    }

static void OverrideAtVcg(AtVcg self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtVcgVcatMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtVcgOverride, m_AtVcgVcatMethods, sizeof(m_AtVcgOverride));

        mMethodOverride(m_AtVcgOverride, HoldOffTimerSet);
        mMethodOverride(m_AtVcgOverride, HoldOffTimerGet);
        mMethodOverride(m_AtVcgOverride, RmvTimerSet);
        mMethodOverride(m_AtVcgOverride, RmvTimerGet);
        mMethodOverride(m_AtVcgOverride, WtrTimerSet);
        mMethodOverride(m_AtVcgOverride, WtrTimerGet);
        mMethodOverride(m_AtVcgOverride, SourceRsAckGet);
        mMethodOverride(m_AtVcgOverride, SinkRsAckGet);
        mMethodOverride(m_AtVcgOverride, SinkMstGet);
        mMethodOverride(m_AtVcgOverride, SourceMstGet);
        }

    mMethodsSet(self, &m_AtVcgOverride);
    }

static void OverrideThaVcgVcat(AtVcg self)
    {
    ThaVcgVcat vcg = (ThaVcgVcat)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaVcgVcatMethods = mMethodsGet(vcg);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaVcgVcatOverride, mMethodsGet(vcg), sizeof(m_ThaVcgVcatOverride));

        }

    mMethodsSet(vcg, &m_ThaVcgVcatOverride);
    }

static void OverrideTha60290081VcgVcat(AtVcg self)
    {
    Tha60290081VcgVcat vcg = (Tha60290081VcgVcat)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290081VcgVcatOverride, mMethodsGet(vcg), sizeof(m_Tha60290081VcgVcatOverride));

        mMethodOverride(m_Tha60290081VcgVcatOverride, SinkMstReset);
        mMethodOverride(m_Tha60290081VcgVcatOverride, SourceMstReset);
        mMethodOverride(m_Tha60290081VcgVcatOverride, SinkReset);
        mMethodOverride(m_Tha60290081VcgVcatOverride, SourceReset);
        }

    mMethodsSet(vcg, &m_Tha60290081VcgVcatOverride);
    }

static void Override(AtVcg self)
    {
    OverrideAtVcg(self);
    OverrideThaVcgVcat(self);
    OverrideTha60290081VcgVcat(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081VcgLoVcat);
    }

static AtVcg ObjectInit(AtVcg self, AtModuleConcate concateModule, uint32 vcgId, eAtConcateMemberType memberType)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290081VcgVcatObjectInit(self, concateModule, vcgId, memberType) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtVcg Tha60290081VcgLoVcatNew(AtModuleConcate module, uint32 vcgId, eAtConcateMemberType memberType)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtVcg newVcg = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newVcg == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newVcg, module, vcgId, memberType);
    }

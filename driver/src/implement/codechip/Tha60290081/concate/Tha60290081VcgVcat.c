/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CONCATE
 *
 * File        : Tha60290081VcgVcat.c
 *
 * Created Date: Sep 3, 2019
 *
 * Description : 60290081 Common VCAT VCG implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../../../../generic/encap/AtEncapChannelInternal.h"
#include "../encap/Tha60290081ModuleEncapInternal.h"
#include "../encap/Tha60290081GfpChannel.h"
#include "../pda/Tha60290081ModulePda.h"
#include "Tha60290081ModuleConcate.h"
#include "Tha60290081ModuleHoVcatReg.h"
#include "Tha60290081VcgVcatInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self)         ((Tha60290081VcgVcat)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60290081VcgVcatMethods m_methods;

/* Override */
static tAtObjectMethods         m_AtObjectOverride;
static tAtChannelMethods        m_AtChannelOverride;
static tAtConcateGroupMethods   m_AtConcateGroupOverride;
static tAtVcgMethods            m_AtVcgOverride;
static tThaVcgVcatMethods       m_ThaVcgVcatOverride;

/* Save super implementation */
static const tAtObjectMethods       *m_AtObjectMethods       = NULL;
static const tAtChannelMethods      *m_AtChannelMethods      = NULL;
static const tAtConcateGroupMethods *m_AtConcateGroupMethods = NULL;
static const tAtVcgMethods          *m_AtVcgVcatMethods      = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
#include "Tha60290081VcgCommon.h"

static eAtRet DefaultSet(ThaVcgVcat self)
    {
    eAtRet ret = cAtOk;
    ret |= mMethodsGet(mThis(self))->SinkMstReset(mThis(self));
    ret |= mMethodsGet(mThis(self))->SourceMstReset(mThis(self));
    return ret;
    }

static eAtModuleConcateRet LcasEnable(AtVcg self, eBool enable)
    {
    return m_AtVcgVcatMethods->LcasEnable(self, enable);
    }

static eAtRet SinkMstReset(Tha60290081VcgVcat self)
    {
    AtUnused(self);
    /* Let subclass implement */
    return cAtOk;
    }

static eAtRet SourceMstReset(Tha60290081VcgVcat self)
    {
    AtUnused(self);
    /* Let subclass implement */
    return cAtOk;
    }

static eAtRet SinkReset(Tha60290081VcgVcat self)
    {
    AtUnused(self);
    /* Let subclass implement */
    return cAtOk;
    }

static eAtRet SourceReset(Tha60290081VcgVcat self)
    {
    AtUnused(self);
    /* Let subclass implement */
    return cAtOk;
    }

static void HwReset(ThaVcgVcat self)
    {
    Tha60290081VcgVcat vcg = (Tha60290081VcgVcat)self;

    Tha60290081VcgVcatMembersReset(self);

    mMethodsGet(vcg)->SinkMstReset(vcg);
    mMethodsGet(vcg)->SourceMstReset(vcg);
    mMethodsGet(vcg)->SinkReset(vcg);
    mMethodsGet(vcg)->SourceReset(vcg);
    }

static void OverrideAtObject(AtVcg self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtVcg self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, ServiceIsRunning);
        mMethodOverride(m_AtChannelOverride, HwIdGet);
        mMethodOverride(m_AtChannelOverride, Init);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtConcateGroup(AtVcg self)
    {
    AtConcateGroup group = (AtConcateGroup)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtConcateGroupMethods = mMethodsGet(group);
        mMethodsGet(osal)->MemCpy(osal, &m_AtConcateGroupOverride, m_AtConcateGroupMethods, sizeof(m_AtConcateGroupOverride));

        mMethodOverride(m_AtConcateGroupOverride, EncapTypeIsSupported);
        mMethodOverride(m_AtConcateGroupOverride, EncapTypeGet);
        mMethodOverride(m_AtConcateGroupOverride, SourceMemberProvision);
        mMethodOverride(m_AtConcateGroupOverride, SourceMemberDeprovision);
        }

    mMethodsSet(group, &m_AtConcateGroupOverride);
    }

static void OverrideAtVcg(AtVcg self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtVcgVcatMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtVcgOverride, m_AtVcgVcatMethods, sizeof(m_AtVcgOverride));

        mMethodOverride(m_AtVcgOverride, LcasEnable);
        }

    mMethodsSet(self, &m_AtVcgOverride);
    }

static void OverrideThaVcgVcat(AtVcg self)
    {
    ThaVcgVcat vcg = (ThaVcgVcat)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaVcgVcatOverride, mMethodsGet(vcg), sizeof(m_ThaVcgVcatOverride));

        mMethodOverride(m_ThaVcgVcatOverride, DefaultSet);
        mMethodOverride(m_ThaVcgVcatOverride, HwReset);
        }

    mMethodsSet(vcg, &m_ThaVcgVcatOverride);
    }

static void Override(AtVcg self)
    {
    OverrideAtObject(self);
    OverrideAtConcateGroup(self);
    OverrideAtChannel(self);
    OverrideAtVcg(self);
    OverrideThaVcgVcat(self);
    }

static void MethodsInit(Tha60290081VcgVcat self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, SinkMstReset);
        mMethodOverride(m_methods, SourceMstReset);
        mMethodOverride(m_methods, SinkReset);
        mMethodOverride(m_methods, SourceReset);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081VcgVcat);
    }

AtVcg Tha60290081VcgVcatObjectInit(AtVcg self, AtModuleConcate concateModule, uint32 vcgId, eAtConcateMemberType memberType)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaVcgVcatObjectInit(self, concateModule, vcgId, memberType) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

eAtRet Tha60290081VcgVcatMembersReset(ThaVcgVcat self)
    {
    eAtRet ret = cAtOk;
    AtConcateGroup vcg = (AtConcateGroup)self;
    uint32 numSourceMembers = AtConcateGroupNumSourceMembersGet(vcg);
    uint32 numSinkMembers = AtConcateGroupNumSinkMembersGet(vcg);
    AtConcateMember member;
    uint32 member_i;

    for (member_i = 0; member_i < numSourceMembers; member_i++)
        {
        member = AtConcateGroupSourceMemberGetByIndex(vcg, member_i);
        ret |= ThaVcgMemberSourceReset((ThaVcgMember)member);
        }

    for (member_i = 0; member_i < numSinkMembers; member_i++)
        {
        member = AtConcateGroupSinkMemberGetByIndex(vcg, member_i);
        ret |= ThaVcgMemberSinkReset((ThaVcgMember)member);
        }

    return ret;
    }

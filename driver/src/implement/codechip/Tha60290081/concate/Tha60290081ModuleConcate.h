/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CONCATE
 * 
 * File        : Tha60290081ModuleConcate.h
 * 
 * Created Date: Jun 18, 2019
 *
 * Description : 60290081 Module CONCATE
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290081MODULECONCATE_H_
#define _THA60290081MODULECONCATE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleConcate.h"
#include "../../../default/concate/member/ThaVcgMember.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290081ModuleConcate  *Tha60290081ModuleConcate;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

uint32 Tha60290081ModuleConcateHoBaseAddress(Tha60290081ModuleConcate self);
uint32 Tha60290081ModuleConcateLoBaseAddress(Tha60290081ModuleConcate self);
uint32 Tha60290081ModuleConcateNumHoConcateGroups(Tha60290081ModuleConcate self);
uint32 Tha60290081ModuleConcateNumLoConcateGroups(Tha60290081ModuleConcate self);
uint32 Tha60290081ModuleConcateGroupLocalId(Tha60290081ModuleConcate self, AtConcateGroup group);

/* ID conversion */
eAtRet Tha60290081ModuleConcateLoMemberHwIdGet(AtConcateMember member, uint16 *hwStsId, uint8 *hwVtgId, uint8 *hwVtId);
eAtRet Tha60290081ModuleConcateLoMemberHwEngineIdGet(AtConcateMember member, uint16 *hwStsEngID, uint8 *hwVtgEngID, uint8 *hwVtEngID);
uint32 Tha60290081ModuleConcateLoMemberEngineOffset(Tha60290081ModuleConcate self, AtConcateMember member);

/* HW ID allocation */
uint32 Tha60290081ModuleConcateDe3HwIdAllocate(Tha60290081ModuleConcate self, AtConcateMember member);
void Tha60290081ModuleConcateDe3HwIdDeAllocate(Tha60290081ModuleConcate self, AtConcateMember member);
uint32 Tha60290081ModuleConcateDe1HwIdAllocate(Tha60290081ModuleConcate self, AtConcateMember member);
void Tha60290081ModuleConcateDe1HwIdDeAllocate(Tha60290081ModuleConcate self, AtConcateMember member);
uint32 Tha60290081ModuleConcateVc1xHwIdAllocate(Tha60290081ModuleConcate self, AtConcateMember member);
void Tha60290081ModuleConcateVc1xHwIdDeAllocate(Tha60290081ModuleConcate self, AtConcateMember member);

eAtRet Tha60290081ModuleConcateSdhVcSourceMemberAdd(Tha60290081ModuleConcate self, ThaVcgMember member);
eAtRet Tha60290081ModuleConcateSdhVcSourceMemberRemove(Tha60290081ModuleConcate self, ThaVcgMember member);
eAtRet Tha60290081ModuleConcateSdhVcSinkMemberAdd(Tha60290081ModuleConcate self, ThaVcgMember member, eBool lcasEnable);
eAtRet Tha60290081ModuleConcateSdhVcSinkMemberRemove(Tha60290081ModuleConcate self, ThaVcgMember member);

eAtRet Tha60290081ModuleConcateHoVcSourcePayloadTypeSet(Tha60290081ModuleConcate self, AtConcateMember member, eBool enable);
eAtRet Tha60290081ModuleConcateHoVcSinkPayloadTypeSet(Tha60290081ModuleConcate self, AtConcateMember member, eBool enable);

/* Register flush */
void Tha60290081ModuleConcateHwFlush(Tha60290081ModuleConcate self);

/* Interrupt processing */
AtConcateMember Tha60290081ModuleConcateHoVcatSourceMemberFromHwIdGet(Tha60290081ModuleConcate self, uint16 hwVcatSts);
AtConcateMember Tha60290081ModuleConcateHoVcatSinkMemberFromHwIdGet(Tha60290081ModuleConcate self, uint16 hwVcatSts);
AtConcateMember Tha60290081ModuleConcateLoVcatSourceMemberFromHwIdGet(Tha60290081ModuleConcate self, uint32 hwEngineId);
AtConcateMember Tha60290081ModuleConcateLoVcatSinkMemberFromHwIdGet(Tha60290081ModuleConcate self, uint32 hwEngineId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290081MODULECONCATE_H_ */


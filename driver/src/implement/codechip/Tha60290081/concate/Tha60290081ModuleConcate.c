/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Concate
 *
 * File        : Tha60290081ModuleConcate.c
 *
 * Created Date: Jun 18, 2019
 *
 * Description : 60290081 Module CONCATE implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/concate/AtConcateGroupInternal.h"
#include "../../../default/man/ThaDevice.h"
#include "../../../default/concate/ThaVcg.h"
#include "../../../default/ocn/ThaModuleOcn.h"
#include "../../Tha60210011/man/Tha60210011Device.h"
#include "member/Tha60290081VcgMember.h"
#include "Tha60290081ModuleConcate.h"
#include "Tha60290081ModuleConcateInternal.h"
#include "Tha60290081ModuleHoVcatReg.h"
#include "Tha60290081ModuleLoVcatReg.h"
#include "Tha60290081VcatInterruptManager.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)         ((Tha60290081ModuleConcate)self)

#define cLoVcatHwStsIDMask     cBit10_5
#define cLoVcatHwStsIDShift           5
#define cLoVcatHwVtgIDMask      cBit4_2
#define cLoVcatHwVtgIDShift           2
#define cLoVcatHwVtIDMask       cBit1_0
#define cLoVcatHwVtIDShift            0

#define mVcatRegDisplay(channel, regName, regAddr)                           \
        ThaModuleConcateRegDisplay((AtChannel)channel, regName, regAddr);

/*--------------------------- Local typedefs ---------------------------------*/
typedef eAtRet (*MemberTimingIdGet)(AtChannel, uint8*, uint8*, uint8*, uint8*);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods             m_AtObjectOverride;
static tAtModuleMethods             m_AtModuleOverride;
static tAtModuleConcateMethods      m_AtModuleConcateOverride;
static tThaModuleConcateMethods     m_ThaModuleConcateOverride;

/* Save super implementation */
static const tAtObjectMethods        *m_AtObjectMethods        = NULL;
static const tAtModuleMethods        *m_AtModuleMethods        = NULL;
static const tAtModuleConcateMethods *m_AtModuleConcateMethods = NULL;
static const tThaModuleConcateMethods *m_ThaModuleConcateMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 HoBaseAddress(Tha60290081ModuleConcate self)
    {
    AtUnused(self);
    return 0x1600000;
    }

static uint32 LoBaseAddress(Tha60290081ModuleConcate self)
    {
    AtUnused(self);
    return 0x1500000;
    }

static uint32 *HoldRegistersGet(AtModule self,
                                uint32 *holdRegisters, uint16 numHoldRegisters,
                                uint16 *numberOfHoldRegisters, uint32 baseAddress)
    {
    uint32 i;
    AtUnused(self);

    for (i = 0; i < numHoldRegisters; i++)
        holdRegisters[i] = holdRegisters[i] + baseAddress;

    if (numberOfHoldRegisters)
        *numberOfHoldRegisters = numHoldRegisters;

    return holdRegisters;
    }

static uint32 *HoHoldReadRegistersGet(AtModule self, uint16 *numberOfHoldRegisters)
    {
    static uint32 holdRegisters[] = {0x00028, 0x00029, 0x0002A, 0x0002B, 0x0002C};
    uint32 baseAddress = Tha60290081ModuleConcateHoBaseAddress(mThis(self));
    return HoldRegistersGet(self, holdRegisters, mCount(holdRegisters), numberOfHoldRegisters, baseAddress);
    }

static uint32 *HoHoldWriteRegistersGet(AtModule self, uint16 *numberOfHoldRegisters)
    {
    static uint32 holdRegisters[] = {0x00020, 0x00021, 0x00022, 0x00023, 0x00024};
    uint32 baseAddress = Tha60290081ModuleConcateHoBaseAddress(mThis(self));
    return HoldRegistersGet(self, holdRegisters, mCount(holdRegisters), numberOfHoldRegisters, baseAddress);
    }

static uint32 *LoHoldReadRegistersGet(AtModule self, uint16 *numberOfHoldRegisters)
    {
    static uint32 holdRegisters[] = {0x00004};
    uint32 baseAddress = Tha60290081ModuleConcateLoBaseAddress(mThis(self));
    return HoldRegistersGet(self, holdRegisters, mCount(holdRegisters), numberOfHoldRegisters, baseAddress);
    }

static uint32 *LoHoldWriteRegistersGet(AtModule self, uint16 *numberOfHoldRegisters)
    {
    static uint32 holdRegisters[] = {0x00002};
    uint32 baseAddress = Tha60290081ModuleConcateLoBaseAddress(mThis(self));
    return HoldRegistersGet(self, holdRegisters, mCount(holdRegisters), numberOfHoldRegisters, baseAddress);
    }

static AtLongRegisterAccess LongRegisterAccessCreate(AtModule self,
                                                     uint32 *(*HoldReadRegistersGet)(AtModule, uint16 *),
                                                     uint32 *(*HoldWriteRegistersGet)(AtModule, uint16 *))
    {
    uint16 numHoldReadRegisters = 0;
    uint16 numHoldWriteRegisters = 0;
    uint32 *holdReadRegisters = HoldReadRegistersGet(self, &numHoldReadRegisters);
    uint32 *holdWriteRegisters = HoldWriteRegistersGet(self, &numHoldWriteRegisters);

    return AtDefaultLongRegisterAccessNew(holdReadRegisters, numHoldReadRegisters,
                                          holdWriteRegisters, numHoldWriteRegisters);
    }

static AtLongRegisterAccess HoLongRegisterAccessCreate(AtModule self)
    {
    return LongRegisterAccessCreate(self, HoHoldReadRegistersGet, HoHoldWriteRegistersGet);
    }

static AtLongRegisterAccess LoLongRegisterAccessCreate(AtModule self)
    {
    return LongRegisterAccessCreate(self, LoHoldReadRegistersGet, LoHoldWriteRegistersGet);
    }

static AtLongRegisterAccess HoLongRegisterAccess(AtModule self)
    {
    if (mThis(self)->hoLongRegisterAccess)
        return mThis(self)->hoLongRegisterAccess;

    mThis(self)->hoLongRegisterAccess = HoLongRegisterAccessCreate(self);
    return mThis(self)->hoLongRegisterAccess;
    }

static AtLongRegisterAccess LoLongRegisterAccess(AtModule self)
    {
    if (mThis(self)->loLongRegisterAccess)
        return mThis(self)->loLongRegisterAccess;

    mThis(self)->loLongRegisterAccess = LoLongRegisterAccessCreate(self);
    return mThis(self)->loLongRegisterAccess;
    }

static eBool AddressBelongToHoPart(AtModule self, uint32 localAddress)
    {
    uint32 baseAddress = localAddress & cBit24_20;
    AtUnused(self);

    if (baseAddress == 0x1600000)
        return cAtTrue;

    return cAtFalse;
    }

static AtLongRegisterAccess LongRegisterAccess(AtModule self, uint32 localAddress)
    {
    if (AddressBelongToHoPart(self, localAddress))
        return HoLongRegisterAccess(self);

    return LoLongRegisterAccess(self);
    }

static void AllLongRegisterAccessDelete(Tha60290081ModuleConcate self)
    {
    AtObjectDelete((AtObject)self->hoLongRegisterAccess);
    self->hoLongRegisterAccess = NULL;
    AtObjectDelete((AtObject)self->loLongRegisterAccess);
    self->loLongRegisterAccess = NULL;
    }

static void LoVcatStsCleanup(tTha60290081LoVcatSts *stsInfo)
    {
    if (stsInfo->usedVTs)
        {
        AtObjectDelete((AtObject)stsInfo->usedVTs);
        stsInfo->usedVTs = NULL;
        }
    stsInfo->pldType = cTha60290081LoStsPldTypeNone;
    }

static void AllLoVcatStsCleanup(Tha60290081ModuleConcate self)
    {
    uint8 sts_i;
    for (sts_i = 0; sts_i < 48; sts_i++)
        LoVcatStsCleanup(&self->loSts[sts_i]);
    }

static void Delete(AtObject self)
    {
    AllLongRegisterAccessDelete(mThis(self));
    AllLoVcatStsCleanup(mThis(self));

    /* Call super to fully delete itself */
    m_AtObjectMethods->Delete(self);
    }

static void LoVcatStsSerialize(AtObject loSts, AtCoder encoder)
    {
    tTha60290081LoVcatSts *object = (tTha60290081LoVcatSts *)loSts;

    mEncodeUInt(pldType);
    mEncodeObject(usedVTs);
    }

static AtObject LoVcatStsAtIndex(AtCoder self, void *objects, uint32 objectIndex)
    {
    tTha60290081LoVcatSts *loSts = (tTha60290081LoVcatSts *)objects;
    AtObject object = (AtObject)((void *)&(loSts[objectIndex]));
    AtUnused(self);
    return object;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60290081ModuleConcate object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObject(hoLongRegisterAccess);
    mEncodeObject(loLongRegisterAccess);
    AtCoderEncodeObjectsWithHandlers(encoder, object->loSts, 48,
                                     LoVcatStsAtIndex, "loSts",
                                     LoVcatStsSerialize);
    }

static uint32 HoMasterRegister(AtModule self)
    {
    return cAf6Reg_ho_master_pen + Tha60290081ModuleConcateHoBaseAddress(mThis(self));
    }

static eAtRet HoVcatActivate(AtModule self, eBool active)
    {
    uint32 regAddr = HoMasterRegister(self);
    uint32 regVal;

    regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_ho_master_pen_Active_, (active) ? 0x1 : 0x0);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static uint32 LoMasterRegister(AtModule self)
    {
    return cAf6Reg_map_master_contr + Tha60290081ModuleConcateLoBaseAddress(mThis(self));
    }

static eAtRet LoVcatActivate(AtModule self, eBool active)
    {
    uint32 regAddr = LoMasterRegister(self);
    uint32 regVal;

    regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_map_master_contr_MapUpActive_, (active) ? 0x1 : 0x0);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet Activate(AtModule self)
    {
    eAtRet ret = cAtOk;
    ret |= HoVcatActivate(self, cAtTrue);
    ret |= LoVcatActivate(self, cAtTrue);
    return ret;
    }

static eAtRet Deactivate(AtModule self)
    {
    eAtRet ret = cAtOk;
    ret |= HoVcatActivate(self, cAtFalse);
    ret |= LoVcatActivate(self, cAtFalse);
    return ret;
    }

static eBool HoVcatIsActive(AtModule self)
    {
    uint32 regAddr = HoMasterRegister(self);
    uint32 regVal = mModuleHwRead(self, regAddr);
    return mRegField(regVal, cAf6_ho_master_pen_Active_) ? cAtTrue : cAtFalse;
    }

static eBool LoVcatIsActive(AtModule self)
    {
    uint32 regAddr = LoMasterRegister(self);
    uint32 regVal = mModuleHwRead(self, regAddr);
    return mRegField(regVal, cAf6_map_master_contr_MapUpActive_) ? cAtTrue : cAtFalse;
    }

static eBool IsActive(AtModule self)
    {
    if (HoVcatIsActive(self) || LoVcatIsActive(self))
        return cAtTrue;

    return cAtFalse;
    }

static AtInterruptManager InterruptManagerCreate(AtModule self, uint32 managerIndex)
    {
    if (managerIndex == 0)
        return Tha60290081HoVcatInterruptManagerNew(self);

    return Tha60290081LoVcatInterruptManagerNew(self);
    }

static uint32 NumInterruptManagers(AtModule self)
    {
    AtUnused(self);
    return 2;
    }

static uint32 NumOc48Slices(ThaModuleConcate self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return Tha60210011DeviceNumUsedOc48Slices((Tha60210011Device)device);
    }

static uint32 OcnHwStsId2VcatFlatId(ThaModuleConcate self, uint32 slice, uint32 stsInSlice)
    {
    uint32 localStsId = (stsInSlice % 16U) / 4U + (stsInSlice % 4U) * 4U + (stsInSlice / 16) * 16U;
    AtUnused(self);
    return (localStsId * 4U) + slice; /* Slice multiplexing */
    }

static eAtRet HoXcDefaultSet(ThaModuleConcate self)
    {
    uint32 baseAddress = Tha60290081ModuleConcateHoBaseAddress(mThis(self));
    uint32 numOc48Slices = NumOc48Slices(self);
    uint32 slice, stsInSlice;
    uint32 vcatStsId;

    for (slice = 0; slice < numOc48Slices; slice++)
        {
        for (stsInSlice = 0; stsInSlice < 48; stsInSlice ++)
            {
            uint32 regAddr, regVal;
            uint32 offset;

            vcatStsId = OcnHwStsId2VcatFlatId(self, slice, stsInSlice);

            /* Source ID mapping */
            regAddr = baseAddress + cAf6Reg_ho_so_stsxc_pen_Base + vcatStsId;
            regVal = 0x0;
            mRegFieldSet(regVal, cAf6_ho_so_stsxc_pen_OC48_OutSlice_, slice);
            mRegFieldSet(regVal, cAf6_ho_so_stsxc_pen_OC48_OutSTS_, stsInSlice);
            mModuleHwWrite(self, regAddr, regVal);

            /* Sink ID mapping */
            offset = stsInSlice * 4U + slice;
            regAddr = baseAddress + cAf6Reg_ho_sk_stsxc_pen_Base + offset;
            regVal = 0x0;
            mRegFieldSet(regVal, cAf6_ho_sk_stsxc_pen_OC192_STSEna_, 1);
            mRegFieldSet(regVal, cAf6_ho_sk_stsxc_pen_OC192_OutSTS_, vcatStsId);
            mModuleHwWrite(self, regAddr, regVal);
            }
        }

    return cAtOk;
    }

static eAtRet HoDeskewLatencyDefault(ThaModuleConcate self)
    {
    eAtRet ret = cAtOk;
    static const uint32 cMaxDeskew128ms = 1024;
    static const uint32 cMaxDeskew256ms = 2048;

    ret |= AtModuleConcateDeskewLatencyThresholdSet((AtModuleConcate)self, cAtConcateMemberTypeVc4, cMaxDeskew128ms);
    ret |= AtModuleConcateDeskewLatencyThresholdSet((AtModuleConcate)self, cAtConcateMemberTypeVc3, cMaxDeskew256ms);
    return ret;
    }

static eAtRet HoDefaultSet(ThaModuleConcate self)
    {
    eAtRet ret;

    ret  = HoXcDefaultSet(self);
    ret |= HoDeskewLatencyDefault(self);

    return ret;
    }

static eAtRet LoDefaultSet(ThaModuleConcate self)
    {
    /*TODO: continue update*/
    AtUnused(self);
    return cAtOk;
    }

static eAtRet DefaultSet(ThaModuleConcate self)
    {
    eAtRet ret = cAtOk;

    ret |= HoDefaultSet(self);
    ret |= LoDefaultSet(self);
    return ret;
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    AllLoVcatStsCleanup(mThis(self));
    return ret;
    }

static uint32 MaxGroupGet(AtModuleConcate self)
    {
    AtUnused(self);
    return 256;
    }

static eBool GroupMemberTypeIsSupported(AtModuleConcate self, eAtConcateMemberType memberType)
    {
    AtUnused(self);
    switch ((uint32)memberType)
        {
        case cAtConcateMemberTypeVc4_64c: return cAtTrue;
        case cAtConcateMemberTypeVc4_16c: return cAtTrue;
        case cAtConcateMemberTypeVc4_4c:  return cAtTrue;
        case cAtConcateMemberTypeVc4_nc:  return cAtTrue;
        case cAtConcateMemberTypeVc4:     return cAtTrue;
        case cAtConcateMemberTypeVc3:     return cAtTrue;
        case cAtConcateMemberTypeVc12:    return cAtTrue;
        case cAtConcateMemberTypeVc11:    return cAtTrue;
        case cAtConcateMemberTypeDs1:     return cAtTrue;
        case cAtConcateMemberTypeE1:      return cAtTrue;
        case cAtConcateMemberTypeDs3:     return cAtTrue;
        case cAtConcateMemberTypeE3:      return cAtTrue;
        default: return cAtFalse;
        }
    }

static eBool GroupTypeIsSupported(AtModuleConcate self, eAtConcateGroupType concateType)
    {
    AtUnused(self);
    switch ((uint32)concateType)
        {
        case cAtConcateGroupTypeCcat:           return cAtTrue;
        case cAtConcateGroupTypeVcat:           return cAtTrue;
        case cAtConcateGroupTypeNVcat:          return cAtTrue;
        case cAtConcateGroupTypeNVcat_g804:     return cAtTrue;
        case cAtConcateGroupTypeNVcat_g8040:    return cAtTrue;
        default: return cAtFalse;
        }
    }

static eBool IsHoConcateGroup(AtModuleConcate self, uint32 vcgId)
    {
    if (vcgId < Tha60290081ModuleConcateNumHoConcateGroups(mThis(self)))
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsLoConcateGroup(AtModuleConcate self, uint32 vcgId)
    {
    return IsHoConcateGroup(self, vcgId) ? cAtFalse : cAtTrue;
    }

static AtConcateGroup VcatVcgObjectCreate(AtModuleConcate self, uint32 vcgId, eAtConcateMemberType memberType)
    {
    if (IsHoConcateGroup(self, vcgId))
        return (AtConcateGroup)Tha60290081VcgHoVcatNew(self, vcgId, memberType);

    return (AtConcateGroup)Tha60290081VcgLoVcatNew(self, vcgId, memberType);
    }

static AtConcateGroup NonVcatVcgObjectCreate(AtModuleConcate self, uint32 vcgId, eAtConcateMemberType memberType, eAtConcateGroupType concateType)
    {
    return Tha60290081VcgNonVcatNew(self, vcgId, memberType, concateType);
    }

static eBool CanCreateCcatGroup(AtModuleConcate self, uint32 vcgId, eAtConcateMemberType memberType)
    {
    if ((memberType == cAtConcateMemberTypeVc4_64c) ||
        (memberType == cAtConcateMemberTypeVc4_16c) ||
        (memberType == cAtConcateMemberTypeVc4_4c)  ||
        (memberType == cAtConcateMemberTypeVc4_nc)  ||
        (memberType == cAtConcateMemberTypeVc4)     ||
        (memberType == cAtConcateMemberTypeVc3))
        return IsHoConcateGroup(self, vcgId);

    return cAtFalse;
    }

static eBool CanCreateVcatGroup(AtModuleConcate self, uint32 vcgId, eAtConcateMemberType memberType)
    {
    if ((memberType == cAtConcateMemberTypeVc4_4c)  ||
        (memberType == cAtConcateMemberTypeVc4)     ||
        (memberType == cAtConcateMemberTypeVc3))
        return IsHoConcateGroup(self, vcgId);

    if ((memberType == cAtConcateMemberTypeVc12) ||
        (memberType == cAtConcateMemberTypeVc11) ||
        (memberType == cAtConcateMemberTypeDs3)  ||
        (memberType == cAtConcateMemberTypeE3)   ||
        (memberType == cAtConcateMemberTypeDs1)  ||
        (memberType == cAtConcateMemberTypeE1))
        return IsLoConcateGroup(self, vcgId);

    return cAtFalse;
    }

static eBool CanCreateNVcatGroup(AtModuleConcate self, uint32 vcgId, eAtConcateMemberType memberType)
    {
    /* Non-VCAT is defined for PDH channel. */
    if ((memberType == cAtConcateMemberTypeDs3)  ||
        (memberType == cAtConcateMemberTypeE3)   ||
        (memberType == cAtConcateMemberTypeDs1)  ||
        (memberType == cAtConcateMemberTypeE1))
        return IsLoConcateGroup(self, vcgId);

    return cAtFalse;
    }

static eBool CanCreateGroup(AtModuleConcate self, uint32 vcgId, eAtConcateGroupType concateType, eAtConcateMemberType memberType)
    {
    switch ((uint32)concateType)
        {
        case cAtConcateGroupTypeCcat:         return CanCreateCcatGroup(self, vcgId, memberType);
        case cAtConcateGroupTypeVcat:         return CanCreateVcatGroup(self, vcgId, memberType);
        case cAtConcateGroupTypeNVcat:        return CanCreateNVcatGroup(self, vcgId, memberType);
        case cAtConcateGroupTypeNVcat_g804:   return CanCreateNVcatGroup(self, vcgId, memberType);
        case cAtConcateGroupTypeNVcat_g8040:  return CanCreateNVcatGroup(self, vcgId, memberType);
        default:    return cAtFalse;
        }
    }

static eBool IsVcgFixedToGfpChannel(AtModuleConcate self)
    {
    AtUnused(self);
    /* Default implement assigns a fixed binding between GFP channel and a
     * concatenation group. */
    return cAtTrue;
    }

static AtConcateMember CreateConcateMemberForPdhDe1(AtModuleConcate self, AtPdhDe1 de1, AtConcateGroup group)
    {
    AtUnused(self);
    AtUnused(de1);
    AtUnused(group);
    return NULL;
    }

static AtConcateMember CreateConcateMemberForPdhDe3(AtModuleConcate self, AtPdhDe3 de3, AtConcateGroup group)
    {
    AtUnused(self);
    AtUnused(de3);
    AtUnused(group);
    return NULL;
    }

static uint32 DeskewLatencyResolutionInFrames(AtModuleConcate self, eAtConcateMemberType memberType)
    {
    AtUnused(self);
    switch ((uint32) memberType)
        {
        case cAtConcateMemberTypeVc4_4c: return 1;
        case cAtConcateMemberTypeVc4:    return 1;
        case cAtConcateMemberTypeVc3:    return 1;
        default: return 0;
        }
    }

static uint32 HoDeskewLatencyAddress(AtModuleConcate self)
    {
    return Tha60290081ModuleConcateHoBaseAddress(mThis(self)) + cAf6Reg_ho_sk_mndthr_pen;
    }

static uint32 ThresholdSw2Hw(uint32 thresholdInRes)
    {
    return thresholdInRes - 1;
    }

static eBool ThresholdIsInRange(uint32 thresholdInRes, uint32 maxThreshold)
    {
    if (0 < thresholdInRes && thresholdInRes <= maxThreshold)
        return cAtTrue;
    return cAtFalse;
    }

static eAtRet Vc4DeskewLatencyThresholdSet(AtModuleConcate self, uint32 thresholdInRes)
    {
    uint32 regAddr;
    uint32 regVal;

    if (!ThresholdIsInRange(thresholdInRes, 1024))
        return cAtErrorOutOfRangParm;

    regAddr = HoDeskewLatencyAddress(self);
    regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_ho_sk_mndthr_pen_VC4_MND_Ena_, 1);
    mRegFieldSet(regVal, cAf6_ho_sk_mndthr_pen_VC4_MND_Thr_, ThresholdSw2Hw(thresholdInRes));
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet Vc3DeskewLatencyThresholdSet(AtModuleConcate self, uint32 thresholdInRes)
    {
    uint32 regAddr;
    uint32 regVal;

    if (!ThresholdIsInRange(thresholdInRes, 2048))
        return cAtErrorOutOfRangParm;

    regAddr = HoDeskewLatencyAddress(self);
    regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_ho_sk_mndthr_pen_VC3_MND_Ena_, 1);
    mRegFieldSet(regVal, cAf6_ho_sk_mndthr_pen_VC3_MND_Thr_, ThresholdSw2Hw(thresholdInRes));
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet DeskewLatencyThresholdSet(AtModuleConcate self, eAtConcateMemberType memberType, uint32 thresholdInRes)
    {
    if ((memberType == cAtConcateMemberTypeVc4_4c) ||
        (memberType == cAtConcateMemberTypeVc4))
        return Vc4DeskewLatencyThresholdSet(self, thresholdInRes);

    if (memberType == cAtConcateMemberTypeVc3)
        return Vc3DeskewLatencyThresholdSet(self, thresholdInRes);

    return cAtErrorModeNotSupport;
    }

static uint32 ThresholdHw2Sw(uint32 hwThreshold)
    {
    return hwThreshold + 1;
    }

static uint32 Vc4DeskewLatencyThresholdGet(AtModuleConcate self)
    {
    uint32 regAddr = HoDeskewLatencyAddress(self);
    uint32 regVal = mModuleHwRead(self, regAddr);

    if ((regVal & cAf6_ho_sk_mndthr_pen_VC4_MND_Ena_Mask))
        return ThresholdHw2Sw(mRegField(regVal, cAf6_ho_sk_mndthr_pen_VC4_MND_Thr_));

    return 0;
    }

static uint32 Vc3DeskewLatencyThresholdGet(AtModuleConcate self)
    {
    uint32 regAddr = HoDeskewLatencyAddress(self);
    uint32 regVal = mModuleHwRead(self, regAddr);

    if ((regVal & cAf6_ho_sk_mndthr_pen_VC3_MND_Ena_Mask))
        return ThresholdHw2Sw(mRegField(regVal, cAf6_ho_sk_mndthr_pen_VC3_MND_Thr_));

    return 0;
    }

static uint32 DeskewLatencyThresholdGet(AtModuleConcate self, eAtConcateMemberType memberType)
    {
    if ((memberType == cAtConcateMemberTypeVc4_4c) ||
        (memberType == cAtConcateMemberTypeVc4))
        return Vc4DeskewLatencyThresholdGet(self);

    if (memberType == cAtConcateMemberTypeVc3)
        return Vc3DeskewLatencyThresholdGet(self);

    return 0;
    }

static const char *LoVcatStsPldTypeString(uint8 pldType)
    {
    switch (pldType)
        {
        case cTha60290081LoStsPldTypeNone:      return "UNUSED";
        case cTha60290081LoStsPldTypeDe3:       return "DS3/E3";
        case cTha60290081LoStsPldTypeVt2E1:     return "VC12/E1";
        case cTha60290081LoStsPldTypeVt15Ds1:   return "VC11/DS1";
        default:                                return "ERROR";
        }
    }

static eAtRet LoVcatStsDebug(Tha60290081ModuleConcate self)
    {
    tTha60290081LoVcatSts *stsInfo;
    uint8 sts_i;

    AtPrintc(cSevInfo, "\r\n* LOVCAT debug information\r\n");
    AtPrintc(cSevInfo, "========================================================\r\n");

    for (sts_i = 0; sts_i < 48; sts_i++)
        {
        stsInfo = &self->loSts[sts_i];
        AtPrintf("STS #%02d: %-10s ", sts_i, LoVcatStsPldTypeString(stsInfo->pldType));
        if (stsInfo->usedVTs)
            ThaBitMaskDebug(stsInfo->usedVTs);
        else
            AtPrintf("\r\n");
        }

    return cAtOk;
    }

static eAtRet Debug(AtModule self)
    {
    return LoVcatStsDebug((Tha60290081ModuleConcate)self);
    }

static AtConcateMember CreateVcgMemberObjectForSdhVc(ThaModuleConcate self, AtSdhVc vc, AtConcateGroup group)
    {
    AtUnused(self);
    return Tha60290081VcgMemberSdhVcNew(group, (AtChannel)vc);
    }

static AtConcateMember CreateVcgMemberObjectForSdhVc1x(ThaModuleConcate self, AtSdhVc vc1x, AtConcateGroup group)
    {
    AtUnused(self);
    return Tha60290081VcgMemberSdhVc1xNew(group, (AtChannel)vc1x);
    }

static uint32 HoVcDefaultOffset(ThaModuleConcate self, AtChannel hoVc)
    {
    AtSdhChannel channel = (AtSdhChannel)hoVc;
    uint16 vcatSts;
    AtUnused(self);

    if (ThaSdhChannelConcateHwStsGet(channel, AtSdhChannelSts1Get(channel), &vcatSts) != cAtOk)
        return cInvalidUint32;

    return Tha60290081ModuleConcateHoBaseAddress(mThis(self)) + vcatSts;
    }

static uint32 Tu3VcDefaultOffset(ThaModuleConcate self, AtChannel tu3Vc)
    {
    /* TU3 VC belongs to HOVCAT */
    return HoVcDefaultOffset(self, tu3Vc);
    }

static uint32 Vc1xDefaultOffset(ThaModuleConcate self, AtChannel vc1x)
    {
    AtUnused(self);
    AtUnused(vc1x);
    /* Using this function is applicable only if the capacity of TDM VC1x and
     * VCAT VC1x are equal to together. */
    return 0xDEADCAFE;
    }

static uint32 De1DefaultOffset(ThaModuleConcate self, AtChannel de1)
    {
    AtUnused(self);
    AtUnused(de1);
    return 0xDEADCAFE;
    }

static uint32 De3DefaultOffset(ThaModuleConcate self, AtChannel de3)
    {
    AtUnused(self);
    AtUnused(de3);
    return 0xDEADCAFE;
    }

static eAtRet HwVc4PayloadTypeSet(ThaModuleConcate self, AtChannel hoVc, uint16 vcatSts, uint32 localAddr, eBool enable)
    {
    uint32 regAddr = Tha60290081ModuleConcateHoBaseAddress(mThis(self)) + localAddr;
    uint32 regVal[cThaLongRegMaxSize] = {0};
    uint32 wordIndex = (vcatSts < 32) ? 0 : 1;
    uint32 masterSts = (vcatSts < 32) ? vcatSts : (vcatSts - 32U);
    uint32 vcatMasterStsMask = cBit0 << masterSts;
    uint32 vcatMasterStsShift = masterSts;

    mChannelHwLongRead(hoVc, regAddr, regVal, cThaLongRegMaxSize, cAtModuleConcate);
    mRegFieldSet(regVal[wordIndex], vcatMasterSts, (enable) ? 1 : 0);
    mChannelHwLongWrite(hoVc, regAddr, regVal, cThaLongRegMaxSize, cAtModuleConcate);

    return cAtOk;
    }

static eAtRet HwVc4_4cPayloadTypeSet(ThaModuleConcate self, AtChannel hoVc, uint16 vcatSts, uint32 localAddr, eBool enable)
    {
    uint32 regAddr = Tha60290081ModuleConcateHoBaseAddress(mThis(self)) + localAddr;
    uint32 regVal;
    uint32 vcatMasterStsMask = cBit0 << vcatSts;
    uint32 vcatMasterStsShift = vcatSts;

    regVal = mChannelHwRead(hoVc, regAddr, cAtModuleConcate);
    mRegFieldSet(regVal, vcatMasterSts, (enable) ? 1 : 0);
    mChannelHwWrite(hoVc, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static eAtRet HwVc4_64cPayloadTypeSet(ThaModuleConcate self, AtChannel hoVc, uint32 localAddr, eBool enable)
    {
    uint32 regAddr = Tha60290081ModuleConcateHoBaseAddress(mThis(self)) + localAddr;
    mChannelHwWrite(hoVc, regAddr, (enable) ? 1 : 0, cAtModuleConcate);

    return cAtOk;
    }

static eAtRet VcatHoVcSinkPayloadTypeSet(ThaModuleConcate self, AtChannel hoVc, eBool enable)
    {
    eAtRet ret;
    AtSdhChannel channel = (AtSdhChannel)hoVc;
    uint8 channelType = AtSdhChannelTypeGet(channel);
    uint16 vcatSts;

    ret = ThaSdhChannelConcateHwStsGet(channel, AtSdhChannelSts1Get(channel), &vcatSts);
    if (ret != cAtOk)
        return ret;

    if (channelType == cAtSdhChannelTypeVc3)
        return cAtOk;

    if (channelType == cAtSdhChannelTypeVc4)
        return HwVc4PayloadTypeSet(self, hoVc, vcatSts, cAf6Reg_ho_sk_vc4md_pen, enable);

    if (channelType == cAtSdhChannelTypeVc4_4c)
        return HwVc4_4cPayloadTypeSet(self, hoVc, vcatSts, cAf6Reg_ho_sk_vc4ncmd_pen, enable);

    return cAtErrorModeNotSupport;
    }

static eAtRet VcatHoVcSourcePayloadTypeSet(ThaModuleConcate self, AtChannel hoVc, eBool enable)
    {
    eAtRet ret;
    AtSdhChannel channel = (AtSdhChannel)hoVc;
    uint8 channelType = AtSdhChannelTypeGet(channel);
    uint16 vcatSts;

    ret = ThaSdhChannelConcateHwStsGet(channel, AtSdhChannelSts1Get(channel), &vcatSts);
    if (ret != cAtOk)
        return ret;

    if (channelType == cAtSdhChannelTypeVc3)
        return cAtOk;

    if (channelType == cAtSdhChannelTypeVc4)
        return HwVc4PayloadTypeSet(self, hoVc, vcatSts, cAf6Reg_ho_so_vc4md_pen, enable);

    if (channelType == cAtSdhChannelTypeVc4_4c)
        return HwVc4_4cPayloadTypeSet(self, hoVc, vcatSts, cAf6Reg_ho_so_vc4ncmd_pen, enable);

    return cAtErrorModeNotSupport;
    }

static eAtRet CcatHoVcSinkPayloadTypeSet(ThaModuleConcate self, AtChannel hoVc, eBool enable)
    {
    AtSdhChannel channel = (AtSdhChannel)hoVc;
    uint8 channelType = AtSdhChannelTypeGet(channel);

    if (channelType == cAtSdhChannelTypeVc4_64c)
        return HwVc4_64cPayloadTypeSet(self, hoVc, cAf6Reg_ho_sk_192cmd_pen, enable);

    return cAtOk;
    }

static eAtRet CcatHoVcSourcePayloadTypeSet(ThaModuleConcate self, AtChannel hoVc, eBool enable)
    {
    AtSdhChannel channel = (AtSdhChannel)hoVc;
    uint8 channelType = AtSdhChannelTypeGet(channel);

    if (channelType == cAtSdhChannelTypeVc4_64c)
        return HwVc4_64cPayloadTypeSet(self, hoVc, cAf6Reg_ho_so_192cmd_pen, enable);

    return cAtOk;
    }

static eAtRet SourceMemberVcxPayloadSet(ThaModuleConcate self, AtConcateMember member, eBool enable)
    {
    return Tha60290081ModuleConcateHoVcSourcePayloadTypeSet(mThis(self), member, enable);
    }

static eAtRet SinkMemberVcxPayloadSet(ThaModuleConcate self, AtConcateMember member, eBool enable)
    {
    return Tha60290081ModuleConcateHoVcSinkPayloadTypeSet(mThis(self), member, enable);
    }

static eAtRet HwVc1xPayloadTypeSet(ThaModuleConcate self, AtConcateMember member, eBool enable, uint32 localAddress)
    {
    eAtRet ret = cAtOk;
    uint16 hwStsEngId;
    uint8  hwVtgEngId;
    uint8  hwVtEngId;
    uint32 offset = AtChannelHwIdGet((AtChannel)member);
    uint32 regAddr = Tha60290081ModuleConcateLoBaseAddress(mThis(self)) + localAddress + offset;
    uint32 regVal = 0;

    /* We intend to map 1-1 between TDM STS/VTG/VT ID and VCAT STS/VTG/VT engine ID */
    ret = Tha60290081ModuleConcateLoMemberHwIdGet(member, &hwStsEngId, &hwVtgEngId, &hwVtEngId);
    if (ret != cAtOk)
        return ret;

    if (enable)
        {
        mRegFieldSet(regVal, cAf6_map_source_logrp28_mode_cfg_SoGrp28Unfrm_,  0);
        mRegFieldSet(regVal, cAf6_map_source_logrp28_mode_cfg_SoGrp28IDEn_,   1);
        mRegFieldSet(regVal, cAf6_map_source_logrp28_mode_cfg_SoGrp28VT15Md_, 1);
        mRegFieldSet(regVal, cAf6_map_source_logrp28_mode_cfg_SoGrp28DS1Md_,  0);
        mRegFieldSet(regVal, cAf6_map_source_logrp28_mode_cfg_SoGrp28StsEngID_, hwStsEngId);
        mRegFieldSet(regVal, cAf6_map_source_logrp28_mode_cfg_SoGrp28VtgEngID_, hwVtgEngId);
        mRegFieldSet(regVal, cAf6_map_source_logrp28_mode_cfg_SoGrp28VtEngID_,  hwVtEngId);
        }
    mChannelHwWrite(member, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static eAtRet SourceMemberVc1xPayloadSet(ThaModuleConcate self, AtConcateMember member, eBool enable)
    {
    AtSdhChannel vc1x = (AtSdhChannel)AtConcateMemberSourceChannelGet(member);
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(vc1x);

    if (channelType == cAtSdhChannelTypeVc12)
        return HwVc1xPayloadTypeSet(self, member, enable, cAf6Reg_map_source_logrp21_mode_cfg);

    if (channelType == cAtSdhChannelTypeVc11)
        return HwVc1xPayloadTypeSet(self, member, enable, cAf6Reg_map_source_logrp28_mode_cfg);

    return cAtErrorModeNotSupport;
    }

static eAtRet SinkMemberVc1xPayloadSet(ThaModuleConcate self, AtConcateMember member, eBool enable)
    {
    AtSdhChannel vc1x = (AtSdhChannel)AtConcateMemberSinkChannelGet(member);
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(vc1x);

    if (channelType == cAtSdhChannelTypeVc12)
        return HwVc1xPayloadTypeSet(self, member, enable, cAf6Reg_map_sink_logrp21_mode_cfg);

    if (channelType == cAtSdhChannelTypeVc11)
        return HwVc1xPayloadTypeSet(self, member, enable, cAf6Reg_map_sink_logrp28_mode_cfg);

    return cAtErrorModeNotSupport;
    }

static eAtRet BindHoVcToSinkVcatGroup(ThaModuleConcate self, AtChannel hoVc, AtConcateGroup group)
    {
    uint32 offset = mMethodsGet(self)->HoVcDefaultOffset(self, hoVc);
    uint32 regAddr = cAf6Reg_ho_sk_memctrl_pen_Base + offset;
    uint32 regVal = 0;

    /* Control master STS only. */
    if (group)
        {
        mRegFieldSet(regVal, cAf6_ho_sk_memctrl_pen_Sk_Vcat_Md_, 1);
        mRegFieldSet(regVal, cAf6_ho_sk_memctrl_pen_Sk_Lcas_Md_, AtConcateGroupIsLcasVcg(group) ? 1 : 0);
        mRegFieldSet(regVal, cAf6_ho_sk_memctrl_pen_Sk_Vcgid_, AtChannelHwIdGet((AtChannel)group));
        }
    mChannelHwWrite(hoVc, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static eAtRet AllVcatHwStsGet(ThaModuleConcate self, AtSdhChannel vc, uint16 *allHwSts)
    {
    eAtRet ret = cAtOk;
    uint8 numSts = AtSdhChannelNumSts(vc);
    uint8 sts1 = AtSdhChannelSts1Get(vc);
    uint8 sts_i;
    AtUnused(self);

    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        ret = ThaSdhChannelConcateHwStsGet(vc, (uint8)(sts1 + sts_i), &allHwSts[sts_i]);
        if (ret != cAtOk)
            return ret;
        }

    AtStdSort(AtStdSharedStdGet(), allHwSts, numSts, sizeof(uint16), AtStdUint16CompareFunction);

    return cAtOk;
    }

static eAtRet BindHoVcToSouceCcatHandler(ThaModuleConcate self, AtChannel hoVc, uint16 *allVcatHwSts, uint32 regVal)
    {
    uint8 sts_i;
    uint8 numSts = AtSdhChannelNumSts((AtSdhChannel)hoVc);
    uint32 baseAddress = Tha60290081ModuleConcateHoBaseAddress(mThis(self));
    uint32 memCtrlAddr = baseAddress + cAf6Reg_ho_so_memctrl_pen_Base;
    uint32 sqCtrlAddr = baseAddress + cAf6Reg_ho_so_txseq_pen_Base;
    uint32 sqVal;

    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        /* Member control */
        mChannelHwWrite(hoVc, memCtrlAddr + allVcatHwSts[sts_i], regVal, cAtModuleConcate);

        /* Sequence control */
        sqVal = 0;
        mRegFieldSet(sqVal, cAf6_ho_so_txseq_pen_So_TxSeq_Ena_, 1);
        mRegFieldSet(sqVal, cAf6_ho_so_txseq_pen_So_TxSeq_, sts_i);
        mChannelHwWrite(hoVc, sqCtrlAddr + allVcatHwSts[sts_i], sqVal, cAtModuleConcate);
        }

    return cAtOk;
    }

static eAtRet BindHoVcToSinkCcatHandler(ThaModuleConcate self, AtChannel hoVc, uint16 *allVcatHwSts, uint32 regVal)
    {
    uint8 sts_i;
    uint8 numSts = AtSdhChannelNumSts((AtSdhChannel)hoVc);
    uint32 baseAddress = Tha60290081ModuleConcateHoBaseAddress(mThis(self));
    uint32 memCtrlAddr = baseAddress + cAf6Reg_ho_sk_memctrl_pen_Base;
    uint32 sqCtrlAddr = baseAddress + cAf6Reg_ho_sk_expseq_pen_Base;
    uint32 sqVal;

    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        /* Member control */
        mChannelHwWrite(hoVc, memCtrlAddr + allVcatHwSts[sts_i], regVal, cAtModuleConcate);

        /* Sequence control */
        sqVal = 0;
        mRegFieldSet(sqVal, cAf6_ho_sk_expseq_pen_Sk_ExpSeq_, sts_i);
        mChannelHwWrite(hoVc, sqCtrlAddr + allVcatHwSts[sts_i], sqVal, cAtModuleConcate);
        }

    return cAtOk;
    }

static eAtRet BindHoVcToCcatGroupWithHandler(ThaModuleConcate self, AtChannel hoVc, AtConcateGroup group,
                                             eAtRet (*BindHoVcHandler)(ThaModuleConcate self, AtChannel, uint16 *, uint32))
    {
    AtSdhChannel channel = (AtSdhChannel)hoVc;
    uint8 numSts = AtSdhChannelNumSts(channel);
    uint8 sts1 = AtSdhChannelSts1Get(channel);
    uint8 sts_i;
    eAtRet ret = cAtOk;
    uint16 allVcatHwSts[192];
    uint16 masterSts;
    uint32 regVal = 0;

    ret = ThaSdhChannelConcateHwStsGet(channel, sts1, &masterSts);
    if (ret != cAtOk)
        return ret;

    if (group)
        {
        mRegFieldSet(regVal, cAf6_ho_sk_memctrl_pen_Sk_Mem_Ena_, 1);
        mRegFieldSet(regVal, cAf6_ho_sk_memctrl_pen_Sk_Ccat_Mst_, masterSts);
        mRegFieldSet(regVal, cAf6_ho_sk_memctrl_pen_Sk_Ccat_Md_, 1);
        mRegFieldSet(regVal, cAf6_ho_sk_memctrl_pen_Sk_Nms_Cmd_, 1);
        mRegFieldSet(regVal, cAf6_ho_sk_memctrl_pen_Sk_Vcgid_, AtChannelHwIdGet((AtChannel)group));
        }

    ret = AllVcatHwStsGet(self, channel, allVcatHwSts);
    if (ret != cAtOk)
        return ret;

    for (sts_i = 0; sts_i < numSts; sts_i++)
        ret |= BindHoVcHandler(self, hoVc, allVcatHwSts, regVal);

    return ret;
    }

static eAtRet BindHoVcToSinkCcatGroup(ThaModuleConcate self, AtChannel hoVc, AtConcateGroup group)
    {
    return BindHoVcToCcatGroupWithHandler(self, hoVc, group, BindHoVcToSinkCcatHandler);
    }

static eAtRet BindHoVcToSinkConcateGroup(ThaModuleConcate self, AtChannel hoVc, AtConcateGroup group)
    {
    AtConcateGroup currentGroup = AtChannelSinkGroupGet(hoVc);

    if (AtConcateGroupIsVcatVcg(currentGroup))
        return BindHoVcToSinkVcatGroup(self, hoVc, group);

    return BindHoVcToSinkCcatGroup(self, hoVc, group);
    }

static eAtRet BindTu3VcToSinkConcateGroup(ThaModuleConcate self, AtChannel tu3Vc, AtConcateGroup group)
    {
    return BindHoVcToSinkConcateGroup(self, tu3Vc, group);
    }

static eAtRet Vc1xHwIdGet(AtChannel vc1x, uint8 *slice, uint8 *hwSts, uint8 *hwVtg, uint8 *hwVt)
    {
    AtSdhChannel channel = (AtSdhChannel)vc1x;
    eAtRet ret;

    ret = ThaSdhChannelHwStsGet(channel, cThaModuleOcn, AtSdhChannelSts1Get(channel), slice, hwSts);
    if (ret != cAtOk)
        return ret;

    *hwVtg = AtSdhChannelTug2Get(channel);
    *hwVt  = AtSdhChannelTu1xGet(channel);
    return cAtOk;
    }

static eAtRet De3HwIdGet(AtChannel de3, uint8 *slice, uint8 *hwSts, uint8 *hwVtg, uint8 *hwVt)
    {
    eAtRet ret;

    ret = ThaPdhChannelHwIdGet((AtPdhChannel)de3, cAtModulePdh, slice, hwSts);
    if (ret != cAtOk)
        return ret;

    *hwVtg = 0;
    *hwVt  = 0;
    return cAtOk;
    }

static eAtRet De1HwIdGet(AtChannel de1, uint8 *slice, uint8 *hwSts, uint8 *hwVtg, uint8 *hwVt)
    {
    return ThaPdhDe1HwIdGet((ThaPdhDe1)de1, cAtModulePdh, slice, hwSts, hwVtg, hwVt);
    }

static eAtRet BindLoMemberToConcateGroup(ThaModuleConcate self, AtConcateMember member, AtConcateGroup group, uint32 localAddress)
    {
    uint32 regAddr = localAddress + Tha60290081ModuleConcateLoMemberEngineOffset(mThis(self), member);
    uint32 regVal = 0;

    if (group)
        {
        mRegFieldSet(regVal, cAf6_map_sink_mem_ctrl_SkG8040Md_, AtConcateGroupIsNoneVcatG8040(group) ? 1 : 0);
        mRegFieldSet(regVal, cAf6_map_sink_mem_ctrl_SkVcatMd_, AtConcateGroupIsVcatVcg(group) ? 1 : 0);
        mRegFieldSet(regVal, cAf6_map_sink_mem_ctrl_SkLcasMd_, AtConcateGroupIsLcasVcg(group) ? 1 : 0);
        mRegFieldSet(regVal, cAf6_map_sink_mem_ctrl_SkVcgId_, AtChannelHwIdGet((AtChannel)group));
        }
    mChannelHwWrite(member, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static eAtRet Vc1xDe1SinkTimingSourceSet(ThaModuleConcate self, AtConcateMember member, AtChannel timingSource,
                                         MemberTimingIdGet HwIdGet, uint32 localAddress)
    {
    uint8 hwSlice = 0, hwSts = 0, hwVtg = 0, hwVt = 0;
    eAtRet ret;
    uint32 regAddr;
    uint32 regVal;
    uint32 offset = AtChannelHwIdGet((AtChannel)member);

    if (timingSource)
        {
        ret = HwIdGet(timingSource, &hwSlice, &hwSts, &hwVtg, &hwVt);
        if (ret != cAtOk)
            return ret;
        }

    regAddr = localAddress + offset + Tha60290081ModuleConcateLoBaseAddress(mThis(self));
    regVal = mChannelHwRead(member, regAddr, cAtModuleConcate);
    mRegFieldSet(regVal, cAf6_map_sink_logrp28_mode_cfg_SkGrp28PDHIdSlc_, hwSlice);
    mRegFieldSet(regVal, cAf6_map_sink_logrp28_mode_cfg_SkGrp28PDHIdSts_, hwSts);
    mRegFieldSet(regVal, cAf6_map_sink_logrp28_mode_cfg_SkGrp28PDHIdVtg_, hwVtg);
    mRegFieldSet(regVal, cAf6_map_sink_logrp28_mode_cfg_SkGrp28PDHIdVtn_, hwVt);
    mChannelHwWrite(member, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static eAtRet De3SinkTimingSourceSet(ThaModuleConcate self, AtConcateMember member, AtChannel timingSource, MemberTimingIdGet HwIdGet)
    {
    uint8 hwSlice = 0, hwSts = 0, hwVtg = 0, hwVt = 0;
    eAtRet ret;
    uint32 regAddr;
    uint32 regVal;
    uint32 offset = AtChannelHwIdGet((AtChannel)member);

    if (timingSource)
        {
        ret = HwIdGet(timingSource, &hwSlice, &hwSts, &hwVtg, &hwVt);
        if (ret != cAtOk)
            return ret;
        }

    regAddr = cAf6Reg_map_sink_ho_mode_cfg + offset + Tha60290081ModuleConcateLoBaseAddress(mThis(self));
    regVal = mChannelHwRead(member, regAddr, cAtModuleConcate);
    mRegFieldSet(regVal, cAf6_map_sink_ho_mode_cfg_SkHoPDHIdSlc_, hwSlice);
    mRegFieldSet(regVal, cAf6_map_sink_ho_mode_cfg_SkHoPDHIdSts_, hwSts);
    mRegFieldSet(regVal, cAf6_map_sink_ho_mode_cfg_SkHoPDHIdVtg_, hwVtg);
    mRegFieldSet(regVal, cAf6_map_sink_ho_mode_cfg_SkHoPDHIdVtn_, hwVt);
    mChannelHwWrite(member, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static eAtRet SinkMemberTimingSourceSet(ThaModuleConcate self, AtConcateMember member,
                                        AtChannel timingSource, MemberTimingIdGet HwIdGet)
    {
    AtConcateGroup group = AtConcateMemberConcateGroupGet(member);
    uint16 memberType = AtConcateGroupMemberTypeGet(group);

    if ((memberType == cAtConcateMemberTypeVc12) ||
        (memberType == cAtConcateMemberTypeE1))
        return Vc1xDe1SinkTimingSourceSet(self, member, timingSource, HwIdGet, cAf6Reg_map_sink_logrp21_mode_cfg);

    if ((memberType == cAtConcateMemberTypeVc11) ||
        (memberType == cAtConcateMemberTypeDs1))
        return Vc1xDe1SinkTimingSourceSet(self, member, timingSource, HwIdGet, cAf6Reg_map_sink_logrp28_mode_cfg);

    if ((memberType == cAtConcateMemberTypeDs3) ||
        (memberType == cAtConcateMemberTypeE3))
        return De3SinkTimingSourceSet(self, member, timingSource, HwIdGet);

    return cAtErrorModeNotSupport;
    }

static eAtRet BindLoChannelToSinkConcateGroup(ThaModuleConcate self, AtChannel channel, AtConcateGroup group, MemberTimingIdGet HwIdGet)
    {
    eAtRet ret = cAtOk;
    AtVcgBinder vcgBinder = AtChannelVcgBinder(channel);
    AtConcateMember member = AtVcgBinderSinkMemberGet(vcgBinder);

    if (group)
        {
        ret |= BindLoMemberToConcateGroup(self, member, group, cAf6Reg_map_sink_mem_ctrl);
        ret |= SinkMemberTimingSourceSet(self, member, channel, HwIdGet);
        }
    else
        {
        ret |= BindLoMemberToConcateGroup(self, member, NULL, cAf6Reg_map_sink_mem_ctrl);
        ret |= SinkMemberTimingSourceSet(self, member, NULL, HwIdGet);
        }

    return ret;
    }

static eAtRet BindVc1xToSinkConcateGroup(ThaModuleConcate self, AtChannel vc1x, AtConcateGroup group)
    {
    return BindLoChannelToSinkConcateGroup(self, vc1x, group, Vc1xHwIdGet);
    }

static eAtRet BindDe3ToSinkConcateGroup(ThaModuleConcate self, AtChannel de3, AtConcateGroup group)
    {
    return BindLoChannelToSinkConcateGroup(self, de3, group, De3HwIdGet);
    }

static eAtRet BindDe1ToSinkConcateGroup(ThaModuleConcate self, AtChannel de1, AtConcateGroup group)
    {
    return BindLoChannelToSinkConcateGroup(self, de1, group, De1HwIdGet);
    }

static eAtRet BindHoVcToSourceVcatGroup(ThaModuleConcate self, AtChannel hoVc, AtConcateGroup group)
    {
    uint32 offset = mMethodsGet(self)->HoVcDefaultOffset(self, hoVc);
    uint32 regAddr = cAf6Reg_ho_so_memctrl_pen_Base + offset;
    uint32 regVal = 0;

    /* Control master STS only. */
    if (group)
        {
        mRegFieldSet(regVal, cAf6_ho_so_memctrl_pen_So_Vcat_Md_, 1);
        mRegFieldSet(regVal, cAf6_ho_so_memctrl_pen_So_Lcas_Md_, AtConcateGroupIsLcasVcg(group) ? 1 : 0);
        mRegFieldSet(regVal, cAf6_ho_so_memctrl_pen_So_Vcgid_, AtChannelHwIdGet((AtChannel)group));
        }
    mChannelHwWrite(hoVc, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static eAtRet BindHoVcToSourceCcatGroup(ThaModuleConcate self, AtChannel hoVc, AtConcateGroup group)
    {
    return BindHoVcToCcatGroupWithHandler(self, hoVc, group, BindHoVcToSouceCcatHandler);
    }

static eAtRet BindHoVcToSourceConcateGroup(ThaModuleConcate self, AtChannel hoVc, AtConcateGroup group)
    {
    AtConcateGroup currentGroup = AtChannelSourceGroupGet(hoVc);

    if (AtConcateGroupIsVcatVcg(currentGroup))
        return BindHoVcToSourceVcatGroup(self, hoVc, group);

    return BindHoVcToSourceCcatGroup(self, hoVc, group);
    }

static eAtRet BindTu3VcToSourceConcateGroup(ThaModuleConcate self, AtChannel tu3Vc, AtConcateGroup group)
    {
    return BindHoVcToSourceConcateGroup(self, tu3Vc, group);
    }

static eAtRet SourceMemberTimingSourceSet(ThaModuleConcate self, AtConcateMember member, AtChannel timingSource, MemberTimingIdGet HwIdGet)
    {
    uint8 hwSlice = 0, hwSts = 0, hwVtg = 0, hwVt = 0;
    eAtRet ret;
    uint32 regAddr;
    uint32 regVal;

    if (timingSource)
        {
        ret = HwIdGet(timingSource, &hwSlice, &hwSts, &hwVtg, &hwVt);
        if (ret != cAtOk)
            return ret;
        }

    regAddr = cAf6Reg_map_source_timing_pdhid + Tha60290081ModuleConcateLoMemberEngineOffset(mThis(self), member);
    regVal = mChannelHwRead(member, regAddr, cAtModuleConcate);
    mRegFieldSet(regVal, cAf6_map_source_timing_pdhid_SoPDHIdSlc_, hwSlice);
    mRegFieldSet(regVal, cAf6_map_source_timing_pdhid_SoPDHIdSts_, hwSts);
    mRegFieldSet(regVal, cAf6_map_source_timing_pdhid_SoPDHIdVtg_, hwVtg);
    mRegFieldSet(regVal, cAf6_map_source_timing_pdhid_SoPDHIdVtn_, hwVt);
    mChannelHwWrite(member, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static eAtRet BindLoChannelToSourceConcateGroup(ThaModuleConcate self, AtChannel channel, AtConcateGroup group, MemberTimingIdGet HwIdGet)
    {
    eAtRet ret = cAtOk;
    AtVcgBinder vcgBinder = AtChannelVcgBinder(channel);
    AtConcateMember member = AtVcgBinderSourceMemberGet(vcgBinder);

    if (group)
        {
        ret |= BindLoMemberToConcateGroup(self, member, group, cAf6Reg_map_source_mem_ctrl);
        ret |= SourceMemberTimingSourceSet(self, member, channel, HwIdGet);
        }
    else
        {
        ret |= BindLoMemberToConcateGroup(self, member, NULL, cAf6Reg_map_source_mem_ctrl);
        ret |= SourceMemberTimingSourceSet(self, member, NULL, HwIdGet);
        }

    return ret;
    }

static eAtRet BindVc1xToSourceConcateGroup(ThaModuleConcate self, AtChannel vc1x, AtConcateGroup group)
    {
    return BindLoChannelToSourceConcateGroup(self, vc1x, group, Vc1xHwIdGet);
    }

static eAtRet BindDe3ToSourceConcateGroup(ThaModuleConcate self, AtChannel de3, AtConcateGroup group)
    {
    return BindLoChannelToSourceConcateGroup(self, de3, group, De3HwIdGet);
    }

static eAtRet BindDe1ToSourceConcateGroup(ThaModuleConcate self, AtChannel de1, AtConcateGroup group)
    {
    return BindLoChannelToSourceConcateGroup(self, de1, group, De1HwIdGet);
    }

static eBool SdhChannelIsHoVcat(AtSdhChannel sdhChannel)
    {
    uint8 channelType = AtSdhChannelTypeGet(sdhChannel);

    if ((channelType == cAtSdhChannelTypeVc4_64c) ||
        (channelType == cAtSdhChannelTypeVc4_16c) ||
        (channelType == cAtSdhChannelTypeVc4_4c)  ||
        (channelType == cAtSdhChannelTypeVc4_nc)  ||
        (channelType == cAtSdhChannelTypeVc4)     ||
        (channelType == cAtSdhChannelTypeVc3))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet HoVcatSdhChannelStsIdSw2Hw(ThaModuleConcate self, AtSdhChannel sdhChannel, uint8 swSts, uint16 *hwSts)
    {
    eAtRet ret;
    uint8 slice, stsInSlice;

    ret = ThaSdhChannelHwStsGet(sdhChannel, cThaModuleOcn, swSts, &slice, &stsInSlice);
    if (ret != cAtOk)
        return ret;

    if (hwSts)
        *hwSts = (uint16)OcnHwStsId2VcatFlatId(self, slice, stsInSlice);

    return cAtOk;
    }

static eAtRet LoVcatSdhChannelStsIdSw2Hw(ThaModuleConcate self, AtSdhChannel sdhChannel, uint8 swSts, uint16 *hwSts)
    {
    uint8 slice;
    AtUnused(self);

    return ThaSdhChannelHwStsGet(sdhChannel, cThaModuleOcn, swSts, &slice, (uint8*)hwSts);
    }

static eAtRet SdhChannelStsIdSw2HwGet(ThaModuleConcate self, AtSdhChannel sdhChannel, uint8 swSts, uint16 *hwSts)
    {
    if (SdhChannelIsHoVcat(sdhChannel))
        return HoVcatSdhChannelStsIdSw2Hw(self, sdhChannel, swSts, hwSts);

    /* Low-order VCAT */
    return LoVcatSdhChannelStsIdSw2Hw(self, sdhChannel, swSts, hwSts);
    }

static AtConcateMember MemberGet(AtChannel channel)
    {
    AtVcgBinder binder = AtChannelVcgBinder(channel);
    AtConcateMember member;

    member = AtVcgBinderSourceMemberGet(binder);
    if (member)
        return member;

    return AtVcgBinderSinkMemberGet(binder);
    }

static eAtRet PdhChannelHwIdGet(ThaModuleConcate self, AtPdhChannel pdhChannel, uint16 *hwSts, uint8 *hwVtg, uint8 *hwVt)
    {
    AtConcateMember member = MemberGet((AtChannel)pdhChannel);
    AtUnused(self);
    return Tha60290081ModuleConcateLoMemberHwIdGet(member, hwSts, hwVtg, hwVt);
    }

static eAtRet SdhChannelHwIdGet(ThaModuleConcate self, AtSdhChannel sdhChannel, uint16 *hwSts, uint8 *hwVtg, uint8 *hwVt)
    {
    AtConcateMember member = MemberGet((AtChannel)sdhChannel);
    AtUnused(self);
    return Tha60290081ModuleConcateLoMemberHwIdGet(member, hwSts, hwVtg, hwVt);
    }

static eAtRet SourceMemberAdd(ThaModuleConcate self, ThaVcgMember member)
    {
    uint32 regAddr, regVal;
    AtUnused(self);

    regAddr = cAf6Reg_map_source_mem_ctrl + ThaVcgMemberDefaultOffset(member);
    regVal = mChannelHwRead(member, regAddr, cAtModuleConcate);
    mRegFieldSet(regVal, cAf6_map_source_mem_ctrl_SoAddCmd_, 1);
    mChannelHwWrite(member, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static eAtRet SourceMemberRemove(ThaModuleConcate self, ThaVcgMember member)
    {
    uint32 regAddr, regVal;
    AtUnused(self);

    regAddr = cAf6Reg_map_source_mem_ctrl + ThaVcgMemberDefaultOffset(member);
    regVal = mChannelHwRead(member, regAddr, cAtModuleConcate);
    mRegFieldSet(regVal, cAf6_map_source_mem_ctrl_SoAddCmd_, 0);
    mChannelHwWrite(member, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static eAtRet SinkMemberAdd(ThaModuleConcate self, ThaVcgMember member, eBool lcasEnable)
    {
    uint32 regAddr, regVal;
    AtUnused(self);
    AtUnused(lcasEnable);

    regAddr = cAf6Reg_map_sink_mem_ctrl + ThaVcgMemberDefaultOffset(member);
    regVal = mChannelHwRead(member, regAddr, cAtModuleConcate);
    mRegFieldSet(regVal, cAf6_map_sink_mem_ctrl_SkAddCmd_, 1);
    mChannelHwWrite(member, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static eAtRet SinkMemberRemove(ThaModuleConcate self, ThaVcgMember member)
    {
    uint32 regAddr, regVal;
    AtUnused(self);

    regAddr = cAf6Reg_map_sink_mem_ctrl + ThaVcgMemberDefaultOffset(member);
    regVal = mChannelHwRead(member, regAddr, cAtModuleConcate);
    mRegFieldSet(regVal, cAf6_map_sink_mem_ctrl_SkAddCmd_, 0);
    mChannelHwWrite(member, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static eAtRet SdhVcSourceMemberAdd(Tha60290081ModuleConcate self, ThaVcgMember member)
    {
    uint32 regAddr, regVal;
    AtUnused(self);

    regAddr = cAf6Reg_ho_so_memctrl_pen_Base + ThaVcgMemberDefaultOffset(member);
    regVal = mChannelHwRead(member, regAddr, cAtModuleConcate);
    mRegFieldSet(regVal, cAf6_ho_so_memctrl_pen_So_Nms_Cmd_, 1);
    mChannelHwWrite(member, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static eAtRet SdhVcSourceMemberRemove(Tha60290081ModuleConcate self, ThaVcgMember member)
    {
    uint32 regAddr, regVal;
    AtUnused(self);

    regAddr = cAf6Reg_ho_so_memctrl_pen_Base + ThaVcgMemberDefaultOffset(member);
    regVal = mChannelHwRead(member, regAddr, cAtModuleConcate);
    mRegFieldSet(regVal, cAf6_ho_so_memctrl_pen_So_Nms_Cmd_, 0);
    mChannelHwWrite(member, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static eAtRet SdhVcSinkMemberAdd(Tha60290081ModuleConcate self, ThaVcgMember member, eBool lcasEnable)
    {
    uint32 regAddr, regVal;
    AtUnused(self);
    AtUnused(lcasEnable);

    regAddr = cAf6Reg_ho_sk_memctrl_pen_Base + ThaVcgMemberDefaultOffset(member);
    regVal = mChannelHwRead(member, regAddr, cAtModuleConcate);
    mRegFieldSet(regVal, cAf6_ho_sk_memctrl_pen_Sk_Nms_Cmd_, 1);
    mChannelHwWrite(member, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static eAtRet SdhVcSinkMemberRemove(Tha60290081ModuleConcate self, ThaVcgMember member)
    {
    uint32 regAddr, regVal;
    AtUnused(self);

    regAddr = cAf6Reg_ho_sk_memctrl_pen_Base + ThaVcgMemberDefaultOffset(member);
    regVal = mChannelHwRead(member, regAddr, cAtModuleConcate);
    mRegFieldSet(regVal, cAf6_ho_sk_memctrl_pen_Sk_Nms_Cmd_, 0);
    mChannelHwWrite(member, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static uint32 NumHoConcateGroups(Tha60290081ModuleConcate self)
    {
    AtUnused(self);
    return 128;
    }

static uint32 NumLoConcateGroups(Tha60290081ModuleConcate self)
    {
    AtUnused(self);
    return 128;
    }

static uint32 GroupLocalId(Tha60290081ModuleConcate self, AtConcateGroup group)
    {
    uint32 channelId = AtChannelIdGet((AtChannel)group);

    if (channelId < Tha60290081ModuleConcateNumHoConcateGroups(self))
        return channelId;

    return (uint32)(channelId - Tha60290081ModuleConcateNumHoConcateGroups(self));
    }

static eBool HasRegister(AtModule self, uint32 localAddress)
    {
    AtUnused(self);
    if (mInRange(localAddress, 0x1500000, 0x16FFFFF))
        return cAtTrue;
    return cAtFalse;
    }

static void HoVcatRegDisplay(ThaModuleConcate self, AtSdhChannel sdhChannel, const char* regName, uint32 localAddress)
    {
    uint32 baseAddress = Tha60290081ModuleConcateHoBaseAddress(mThis(self));
    uint32 regAddr;
    uint16 vcatSts;
    uint8 sts1 = AtSdhChannelSts1Get(sdhChannel);

    if (ThaSdhChannelConcateHwStsGet(sdhChannel, sts1, &vcatSts) != cAtOk)
        return;

    regAddr = baseAddress + localAddress + vcatSts;
    ThaModuleConcateRegDisplay((AtChannel)sdhChannel, regName, regAddr);
    }

static void HoVcatMultiRegsDisplay(ThaModuleConcate self, AtSdhChannel sdhChannel, const char* regName, uint32 localAddress)
    {
    uint32 baseAddress = Tha60290081ModuleConcateHoBaseAddress(mThis(self));
    uint16 vcatSts;
    uint8 sts1 = AtSdhChannelSts1Get(sdhChannel);
    uint8 numSts = AtSdhChannelNumSts(sdhChannel);
    uint8 sts_i;

    ThaDeviceRegNameDisplay(regName);
    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint8 stsId =  (uint8)(sts1 + sts_i);
        uint32 regAddr;

        if (ThaSdhChannelConcateHwStsGet(sdhChannel, stsId, &vcatSts) != cAtOk)
            return;

        regAddr = baseAddress + localAddress + vcatSts;
        ThaDeviceChannelRegValueDisplay((AtChannel)sdhChannel, regAddr, cAtModuleConcate);
        }
    }

static void HoVcatMultiRegsDisplayWithOcnOffset(ThaModuleConcate self, AtSdhChannel sdhChannel, const char* regName, uint32 localAddress)
    {
    uint32 baseAddress = Tha60290081ModuleConcateHoBaseAddress(mThis(self));
    uint8 sts1 = AtSdhChannelSts1Get(sdhChannel);
    uint8 numSts = AtSdhChannelNumSts(sdhChannel);
    uint8 sts_i;

    ThaDeviceRegNameDisplay(regName);
    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint8 slice, hwSts;
        uint8 stsId =  (uint8)(sts1 + sts_i);
        uint32 regAddr;

        if (ThaSdhChannelHwStsGet(sdhChannel, cThaModuleOcn, stsId, &slice, &hwSts) != cAtOk)
            return;

        regAddr = baseAddress + localAddress + (uint32)(hwSts * 4 + slice);
        ThaDeviceChannelRegValueDisplay((AtChannel)sdhChannel, regAddr, cAtModuleConcate);
        }
    }

static void HoVcatSdhChannelRegsShow(ThaModuleConcate self, AtSdhChannel sdhChannel)
    {
    AtChannel channel = (AtChannel)sdhChannel;
    AtConcateGroup sinkGroup = AtChannelSinkGroupGet(channel);
    AtConcateGroup sourceGroup = AtChannelSourceGroupGet(channel);
    void (*RegDisplayer)(ThaModuleConcate, AtSdhChannel, const char*, uint32) = NULL;

    if (sourceGroup)
        {
        if (AtConcateGroupIsVcatVcg(sourceGroup))
            RegDisplayer = HoVcatRegDisplay; /* For VCAT, we need to dump master STS only. */
        else
            RegDisplayer = HoVcatMultiRegsDisplay;   /* For CCAT, we need to dump full STS. */

        RegDisplayer(self, sdhChannel, "Source Member Control", cAf6Reg_ho_so_memctrl_pen_Base);
        RegDisplayer(self, sdhChannel, "Source Tx Sequence Configuration", cAf6Reg_ho_so_txseq_pen_Base);
        HoVcatMultiRegsDisplay(self, sdhChannel, "Source Output OC-48 STS Connection Memory", cAf6Reg_ho_so_stsxc_pen_Base);
        }

    if (sinkGroup)
        {
        if (AtConcateGroupIsVcatVcg(sinkGroup))
            RegDisplayer = HoVcatRegDisplay; /* For VCAT, we need to dump master STS only. */
        else
            RegDisplayer = HoVcatMultiRegsDisplay;   /* For CCAT, we need to dump full STS. */

        RegDisplayer(self, sdhChannel, "Sink Member Control", cAf6Reg_ho_sk_memctrl_pen_Base);
        RegDisplayer(self, sdhChannel, "Sink Member Expected Sequence", cAf6Reg_ho_sk_expseq_pen_Base);
        HoVcatMultiRegsDisplayWithOcnOffset(self, sdhChannel, "Source Input OC-48 STS Connection Memory", cAf6Reg_ho_sk_stsxc_pen_Base);
        }
    }

static uint32 LoEngineOffset(ThaModuleConcate self, AtConcateMember member)
    {
    return Tha60290081ModuleConcateLoMemberEngineOffset(mThis(self), member);
    }

static uint32 LoMemberOffset(ThaModuleConcate self, AtConcateMember member)
    {
    return Tha60290081ModuleConcateLoBaseAddress(mThis(self)) + AtChannelHwIdGet((AtChannel)member);
    }

static void LoVcatChannelRegsShow(ThaModuleConcate self, AtChannel channel)
    {
    AtConcateGroup sinkGroup = AtChannelSinkGroupGet(channel);
    AtConcateGroup sourceGroup = AtChannelSourceGroupGet(channel);
    AtVcgBinder binder = AtChannelVcgBinder(channel);

    if (sourceGroup)
        {
        AtConcateMember member = AtVcgBinderSourceMemberGet(binder);
        uint32 memberOffset = LoMemberOffset(self, member);
        uint32 engineOffset = LoEngineOffset(self, member);

        mVcatRegDisplay(channel, "MAP Source Low Order Group 28 Mode Config", cAf6Reg_map_source_logrp28_mode_cfg + memberOffset);
        mVcatRegDisplay(channel, "MAP Source Low Order Group 21 Mode Config", cAf6Reg_map_source_logrp21_mode_cfg + memberOffset);
        mVcatRegDisplay(channel, "MAP Source Member Control",                 cAf6Reg_map_source_mem_ctrl         + engineOffset);
        mVcatRegDisplay(channel, "MAP Source Tx Sequence Config",             cAf6Reg_map_source_txseq_config     + engineOffset);
        mVcatRegDisplay(channel, "MAP Source Timing To PDH MAP",              cAf6Reg_map_source_timing_pdhid     + engineOffset);
        }

    if (sinkGroup)
        {
        AtConcateMember member = AtVcgBinderSinkMemberGet(binder);
        uint32 memberOffset = LoMemberOffset(self, member);
        uint32 engineOffset = LoEngineOffset(self, member);

        mVcatRegDisplay(channel, "MAP Sink Low Order Group 28 Mode Config",  cAf6Reg_map_sink_logrp28_mode_cfg + memberOffset);
        mVcatRegDisplay(channel, "MAP Sink Low Order Group 21 Mode Config",  cAf6Reg_map_sink_logrp21_mode_cfg + memberOffset);
        mVcatRegDisplay(channel, "MAP Sink Member Control",                  cAf6Reg_map_sink_mem_ctrl         + engineOffset);
        mVcatRegDisplay(channel, "MAP Sink Expected Sequence Configuration", cAf6Reg_map_sink_expseq_conf      + engineOffset);
        }
    }

static void LoVcatSdhChannelRegsShow(ThaModuleConcate self, AtSdhChannel sdhChannel)
    {
    LoVcatChannelRegsShow(self, (AtChannel)sdhChannel);
    }

static void SdhChannelRegsShow(ThaModuleConcate self, AtSdhChannel sdhChannel)
    {
    m_ThaModuleConcateMethods->SdhChannelRegsShow(self, sdhChannel);

    if (SdhChannelIsHoVcat(sdhChannel))
        HoVcatSdhChannelRegsShow(self, sdhChannel);
    else
        LoVcatSdhChannelRegsShow(self, sdhChannel);
    }

static uint32 HoVcgIdFromHwIdGet(Tha60290081ModuleConcate self, uint32 localAddress, uint16 hwVcatSts)
    {
    uint32 baseAddress = Tha60290081ModuleConcateHoBaseAddress(self);
    uint32 regAddr = baseAddress + localAddress + hwVcatSts;
    uint32 regVal = mModuleHwRead(self, regAddr);
    return mRegField(regVal, cAf6_ho_so_memctrl_pen_So_Vcgid_);
    }

static AtConcateMember HoSourceMemberFromGroupGet(Tha60290081ModuleConcate self, AtConcateGroup group, uint16 expectedVcatSts)
    {
    uint32 numSourceMembers = AtConcateGroupNumSourceMembersGet(group);
    uint32 member_i;

    /* TODO: this solution may be not so good if list of members becomes longer */
    for (member_i = 0; member_i < numSourceMembers; member_i++)
        {
        AtConcateMember member = AtConcateGroupSourceMemberGetByIndex(group, member_i);
        AtSdhChannel channel = (AtSdhChannel)AtConcateMemberSourceChannelGet(member);
        uint16 vcatSts;

        if (HoVcatSdhChannelStsIdSw2Hw((ThaModuleConcate)self, channel, AtSdhChannelSts1Get(channel), &vcatSts) != cAtOk)
            continue;

        if (vcatSts == expectedVcatSts)
            return member;
        }

    return NULL;
    }

static AtConcateMember HoSinkMemberFromGroupGet(Tha60290081ModuleConcate self, AtConcateGroup group, uint16 expectedVcatSts)
    {
    uint32 numSinkMembers = AtConcateGroupNumSinkMembersGet(group);
    uint32 member_i;

    for (member_i = 0; member_i < numSinkMembers; member_i++)
        {
        AtConcateMember member = AtConcateGroupSinkMemberGetByIndex(group, member_i);
        AtSdhChannel channel = (AtSdhChannel)AtConcateMemberSinkChannelGet(member);
        uint16 vcatSts;

        if (HoVcatSdhChannelStsIdSw2Hw((ThaModuleConcate)self, channel, AtSdhChannelSts1Get(channel), &vcatSts) != cAtOk)
            continue;

        if (vcatSts == expectedVcatSts)
            return member;
        }

    return NULL;
    }

static AtConcateMember HoVcatSourceMemberFromHwIdGet(Tha60290081ModuleConcate self, uint16 hwVcatSts)
    {
    uint32 hoVcgId = HoVcgIdFromHwIdGet(self, cAf6Reg_ho_so_memctrl_pen_Base, hwVcatSts);
    AtConcateGroup group = AtModuleConcateGroupGet((AtModuleConcate)self, hoVcgId);
    return HoSourceMemberFromGroupGet(self, group, hwVcatSts);
    }

static AtConcateMember HoVcatSinkMemberFromHwIdGet(Tha60290081ModuleConcate self, uint16 hwVcatSts)
    {
    uint32 hoVcgId = HoVcgIdFromHwIdGet(self, cAf6Reg_ho_sk_memctrl_pen_Base, hwVcatSts);
    AtConcateGroup group = AtModuleConcateGroupGet((AtModuleConcate)self, hoVcgId);
    return HoSinkMemberFromGroupGet(self, group, hwVcatSts);
    }

static void HoVcatHwFlush(Tha60290081ModuleConcate self)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    HoVcatActivate((AtModule)self, cAtFalse);

    /* Source */
    ThaDeviceMemoryLongFlush(device, 0x1620003, 0x1620003);

    ThaDeviceMemoryFlush(device, 0x1620004, 0x1620005, 0);
    ThaDeviceMemoryFlush(device, 0x1620800, 0x16208BF, 0);
    ThaDeviceMemoryFlush(device, 0x1625000, 0x16250BF, 0);
    ThaDeviceMemoryFlush(device, 0x1624000, 0x16240BF, 0);

    ThaDeviceMemoryFlush(device, 0x1621000, 0x16210BF, 0);
    ThaDeviceMemoryFlush(device, 0x1621800, 0x162187F, 0);

    /* Sink */
    ThaDeviceMemoryLongFlush(device, 0x1640023, 0x1640023);

    ThaDeviceMemoryFlush(device, 0x1640024, 0x1640025, 0);
    ThaDeviceMemoryFlush(device, 0x1648800, 0x16488BF, 0);
    ThaDeviceMemoryFlush(device, 0x1650800, 0x16508BF, 0);
    ThaDeviceMemoryFlush(device, 0x1655400, 0x165547F, 0);
    ThaDeviceMemoryFlush(device, 0x1640200, 0x16402BF, 0);
    ThaDeviceMemoryFlush(device, 0x1656000, 0x16560BF, 0);

    ThaDeviceMemoryFlush(device, 0x1651800, 0x16518BF, 0);
    ThaDeviceMemoryLongFlush(device, 0x1651000, 0x165107F);

    HoVcatActivate((AtModule)self, cAtTrue);
    }

static void LoVcatHwFlush(Tha60290081ModuleConcate self)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    LoVcatActivate((AtModule)self, cAtFalse);

    /* Source */
    ThaDeviceMemoryFlush(device, 0x1520800, 0x152082F, 0);
    ThaDeviceMemoryFlush(device, 0x1521000, 0x15215FF, 0);
    ThaDeviceMemoryFlush(device, 0x1521800, 0x1521DFF, 0);
    ThaDeviceMemoryFlush(device, 0x1522000, 0x15225FF, 0);
    ThaDeviceMemoryFlush(device, 0x1534000, 0x15345FF, 0);
    ThaDeviceMemoryFlush(device, 0x1524000, 0x15245FF, 0);
    ThaDeviceMemoryFlush(device, 0x1538000, 0x15385FF, 0);
    ThaDeviceMemoryFlush(device, 0x153A000, 0x153A5FF, 0);

    ThaDeviceMemoryFlush(device, 0x1526000, 0x152607F, 0);

    /* Sink */
    ThaDeviceMemoryFlush(device, 0x1540800, 0x154082F, 0);
    ThaDeviceMemoryFlush(device, 0x1541000, 0x15415FF, 0);
    ThaDeviceMemoryFlush(device, 0x1541800, 0x1541DFF, 0);
    ThaDeviceMemoryFlush(device, 0x1548800, 0x1548DFF, 0);
    ThaDeviceMemoryFlush(device, 0x1551800, 0x1551DFF, 0);
    ThaDeviceMemoryFlush(device, 0x1550800, 0x1550DFF, 0);
    ThaDeviceMemoryFlush(device, 0x1556000, 0x15565FF, 0);

    ThaDeviceMemoryFlush(device, 0x1551000, 0x155107F, 0);

    ThaDeviceMemoryLongFlush(device, 0x1555100, 0x155517F);
    ThaDeviceMemoryLongFlush(device, 0x1555000, 0x155507F);

    LoVcatActivate((AtModule)self, cAtTrue);
    }

static ThaBitMask LoStsMaskGet(tTha60290081LoVcatSts *stsInfo, uint8 numVtInVtg)
    {
    if (stsInfo->usedVTs == NULL)
        stsInfo->usedVTs = ThaBitMaskNew((uint32)(7 * numVtInVtg));
    return stsInfo->usedVTs;
    }

static uint32 LoHwIdAllocate(tTha60290081LoVcatSts *stsInfo, uint8 sts_i, uint8 numVtInVtg)
    {
    ThaBitMask loStsMask;
    uint8 vtg_i, vt_i;
    uint32 vtId = 0;

    loStsMask = LoStsMaskGet(stsInfo, numVtInVtg);
    if (loStsMask == NULL)
        return cInvalidUint32;

    for (vtg_i = 0; vtg_i < 7; vtg_i++)
        {
        for (vt_i = 0; vt_i < numVtInVtg; vt_i++)
            {
            if (ThaBitMaskBitVal(loStsMask, vtId) == 0)
                {
                uint32 allocHwId = 0;

                /* Mark timeslot as used */
                ThaBitMaskSetBit(loStsMask, vtId);

                mRegFieldSet(allocHwId, cLoVcatHwStsID, sts_i);
                mRegFieldSet(allocHwId, cLoVcatHwVtgID, vtg_i);
                mRegFieldSet(allocHwId, cLoVcatHwVtID, vt_i);
                return allocHwId;
                }

            vtId++;
            }
        }

    return cInvalidUint32;
    }

static uint32 VtDe1HwIdAllocate(Tha60290081ModuleConcate self, uint8 numVtInVtg, uint8 pldType)
    {
    tTha60290081LoVcatSts *stsInfo;
    uint32 allocHwId = cInvalidUint32;
    uint8 sts_i;

    for (sts_i = 0; sts_i < 48; sts_i++)
        {
        stsInfo = &self->loSts[sts_i];

        /* If STS already in use, try next STS */
        if (stsInfo->pldType == cTha60290081LoStsPldTypeDe3)
            continue;

        /* Look for STS which allocated some VT/DE1s previously. We shall not
         * look for an unused STS in the first search. It is to prevent the
         * STS resource fragmentation */
        if (stsInfo->pldType == pldType)
            {
            allocHwId = LoHwIdAllocate(stsInfo, sts_i, numVtInVtg);
            if (allocHwId != cInvalidUint32)
                return allocHwId;
            }
        }

    for (sts_i = 0; sts_i < 48; sts_i++)
        {
        stsInfo = &self->loSts[sts_i];

        /* Look for an unused STS */
        if (stsInfo->pldType == cTha60290081LoStsPldTypeNone)
            {
            allocHwId = LoHwIdAllocate(stsInfo, sts_i, numVtInVtg);
            if (allocHwId != cInvalidUint32)
                {
                stsInfo->pldType = pldType;
                return allocHwId;
                }
            }
        }

    return cInvalidUint32;
    }

static uint32 Vt2E1HwIdAllocate(Tha60290081ModuleConcate self)
    {
    static const uint8 cNumVt2InVtg = 3;
    return VtDe1HwIdAllocate(self, cNumVt2InVtg, cTha60290081LoStsPldTypeVt2E1);
    }

static uint32 Vt15Ds1HwIdAllocate(Tha60290081ModuleConcate self)
    {
    static const uint8 cNumVt15InVtg = 4;
    return VtDe1HwIdAllocate(self, cNumVt15InVtg, cTha60290081LoStsPldTypeVt15Ds1);
    }

static uint8 MemberTypeGet(AtConcateMember member)
    {
    AtConcateGroup group = AtConcateMemberConcateGroupGet(member);
    return AtConcateGroupMemberTypeGet(group);
    }

static uint32 De3HwIdAllocate(Tha60290081ModuleConcate self, AtConcateMember member)
    {
    tTha60290081LoVcatSts *stsInfo;
    uint32 allocHwId;
    uint8 sts_i;
    AtUnused(member);

    for (sts_i = 0; sts_i < 48; sts_i++)
        {
        stsInfo = &self->loSts[sts_i];

        /* This STS was already in use, try next */
        if (stsInfo->pldType != cTha60290081LoStsPldTypeNone)
            continue;

        stsInfo->pldType = cTha60290081LoStsPldTypeDe3;

        allocHwId = 0;
        mRegFieldSet(allocHwId, cLoVcatHwStsID, sts_i);
        mRegFieldSet(allocHwId, cLoVcatHwVtgID, 0);
        mRegFieldSet(allocHwId, cLoVcatHwVtID, 0);

        return allocHwId;
        }

    return cInvalidUint32;
    }

static uint32 De1HwIdAllocate(Tha60290081ModuleConcate self, AtConcateMember member)
    {
    if (MemberTypeGet(member) == cAtConcateMemberTypeE1)
        return Vt2E1HwIdAllocate(self);

    return Vt15Ds1HwIdAllocate(self);
    }

static uint32 Vc1xHwIdAllocate(Tha60290081ModuleConcate self, AtConcateMember member)
    {
    if (MemberTypeGet(member) == cAtConcateMemberTypeVc12)
        return Vt2E1HwIdAllocate(self);

    return Vt15Ds1HwIdAllocate(self);
    }

static void LoHwIdDeAllocate(Tha60290081ModuleConcate self, AtConcateMember member)
    {
    uint32 allocHwId = AtChannelHwIdGet((AtChannel)member);
    uint32 stsId = mRegField(allocHwId, cLoVcatHwStsID);
    uint32 vtgId = mRegField(allocHwId, cLoVcatHwVtgID);
    uint32 vtId  = mRegField(allocHwId, cLoVcatHwVtID);
    tTha60290081LoVcatSts *stsInfo = &self->loSts[stsId];
    ThaBitMask loStsMask = stsInfo->usedVTs;
    uint32 numVtInVtg = 4;
    uint32 vtIdx;

    if (stsInfo->pldType == cTha60290081LoStsPldTypeVt2E1)
        numVtInVtg = 3;

    vtIdx = vtgId * numVtInVtg + vtId;
    ThaBitMaskClearBit(loStsMask, vtIdx);

    /* Check if all resource are deallocated, then free STS */
    if (ThaBitMaskIsAllZeroBits(loStsMask))
        LoVcatStsCleanup(stsInfo);
    }

static void De3HwIdDeAllocate(Tha60290081ModuleConcate self, AtConcateMember member)
    {
    uint32 allocHwId = AtChannelHwIdGet((AtChannel)member);
    uint32 stsId = mRegField(allocHwId, cLoVcatHwStsID);
    LoVcatStsCleanup(&self->loSts[stsId]);
    }

static void De1HwIdDeAllocate(Tha60290081ModuleConcate self, AtConcateMember member)
    {
    LoHwIdDeAllocate(self, member);
    }

static void Vc1xHwIdDeAllocate(Tha60290081ModuleConcate self, AtConcateMember member)
    {
    LoHwIdDeAllocate(self, member);
    }

static uint32 LoMemberHwEngineId(AtConcateMember member)
    {
    uint16 hwStsEngID;
    uint8 hwVtgEngID, hwVtEngID;

    if (Tha60290081ModuleConcateLoMemberHwEngineIdGet(member, &hwStsEngID, &hwVtgEngID, &hwVtEngID) != cAtOk)
        return cInvalidUint32;

    return (uint32)(32U * hwStsEngID + 4U * hwVtgEngID + hwVtEngID);
    }

static AtConcateMember LoSourceMemberFromGroupGet(Tha60290081ModuleConcate self, AtConcateGroup group, uint32 expectedHwEngineId)
    {
    uint32 numSourceMembers = AtConcateGroupNumSourceMembersGet(group);
    uint32 member_i;
    AtUnused(self);

    /* TODO: this solution may be not so good if list of members becomes longer */
    for (member_i = 0; member_i < numSourceMembers; member_i++)
        {
        AtConcateMember member = AtConcateGroupSourceMemberGetByIndex(group, member_i);

        if (LoMemberHwEngineId(member) == expectedHwEngineId)
            return member;
        }

    return NULL;
    }

static AtConcateMember LoSinkMemberFromGroupGet(Tha60290081ModuleConcate self, AtConcateGroup group, uint32 expectedHwEngineId)
    {
    uint32 numSinkMembers = AtConcateGroupNumSinkMembersGet(group);
    uint32 member_i;
    AtUnused(self);

    for (member_i = 0; member_i < numSinkMembers; member_i++)
        {
        AtConcateMember member = AtConcateGroupSinkMemberGetByIndex(group, member_i);

        if (LoMemberHwEngineId(member) == expectedHwEngineId)
            return member;
        }

    return NULL;
    }

static uint32 LoVcgIdFromHwIdGet(Tha60290081ModuleConcate self, uint32 localAddress, uint32 hwEngineId)
    {
    uint32 baseAddress = Tha60290081ModuleConcateLoBaseAddress(self);
    uint32 regAddr = baseAddress + localAddress + hwEngineId;
    uint32 regVal = mModuleHwRead(self, regAddr);
    return mRegField(regVal, cAf6_map_source_mem_ctrl_SoVcgId_);
    }

static AtConcateMember LoVcatSourceMemberFromHwIdGet(Tha60290081ModuleConcate self, uint32 hwEngineId)
    {
    uint32 numHoGroup = Tha60290081ModuleConcateNumHoConcateGroups(mThis(self));
    uint32 loVcgId = LoVcgIdFromHwIdGet(self, cAf6Reg_map_source_mem_ctrl, hwEngineId);
    AtConcateGroup group = AtModuleConcateGroupGet((AtModuleConcate)self, loVcgId + numHoGroup);
    return LoSourceMemberFromGroupGet(self, group, hwEngineId);
    }

static AtConcateMember LoVcatSinkMemberFromHwIdGet(Tha60290081ModuleConcate self, uint32 hwEngineId)
    {
    uint32 numHoGroup = Tha60290081ModuleConcateNumHoConcateGroups(mThis(self));
    uint32 loVcgId = LoVcgIdFromHwIdGet(self, cAf6Reg_map_sink_mem_ctrl, hwEngineId);
    AtConcateGroup group = AtModuleConcateGroupGet((AtModuleConcate)self, loVcgId + numHoGroup);
    return LoSinkMemberFromGroupGet(self, group, hwEngineId);
    }

static uint32 PdhDe3Offset(ThaModuleConcate self, AtConcateMember member)
    {
    uint16 hwStsId;

    if (Tha60290081ModuleConcateLoMemberHwIdGet(member, &hwStsId, NULL, NULL) != cAtOk)
        return cInvalidUint32;

    return Tha60290081ModuleConcateLoBaseAddress((Tha60290081ModuleConcate)self) + hwStsId;
    }

static void VcatPdhDe3RegsShow(ThaModuleConcate self, AtPdhChannel de3)
    {
    AtChannel channel = (AtChannel)de3;
    AtConcateGroup sinkGroup = AtChannelSinkGroupGet(channel);
    AtConcateGroup sourceGroup = AtChannelSourceGroupGet(channel);
    AtVcgBinder binder = AtChannelVcgBinder(channel);

    if (sourceGroup)
        {
        AtConcateMember member = AtVcgBinderSourceMemberGet(binder);
        uint32 pdhDe3Offset = PdhDe3Offset(self, member);
        uint32 engineOffset = LoEngineOffset(self, member);

        mVcatRegDisplay(de3, "MAP Source High Order Mode Config", cAf6Reg_map_source_ho_mode_cfg  + pdhDe3Offset);
        mVcatRegDisplay(de3, "MAP Source Member Control",         cAf6Reg_map_source_mem_ctrl     + engineOffset);
        mVcatRegDisplay(de3, "MAP Source Tx Sequence Config",     cAf6Reg_map_source_txseq_config + engineOffset);
        mVcatRegDisplay(de3, "MAP Source Timing To PDH MAP",      cAf6Reg_map_source_timing_pdhid + engineOffset);
        }

    if (sinkGroup)
        {
        AtConcateMember member = AtVcgBinderSinkMemberGet(binder);
        uint32 pdhDe3Offset = PdhDe3Offset(self, member);
        uint32 engineOffset = LoEngineOffset(self, member);

        mVcatRegDisplay(de3, "MAP Sink High Order Mode Config",          cAf6Reg_map_sink_ho_mode_cfg + pdhDe3Offset);
        mVcatRegDisplay(de3, "MAP Sink Member Control",                  cAf6Reg_map_sink_mem_ctrl    + engineOffset);
        mVcatRegDisplay(de3, "MAP Sink Expected Sequence Configuration", cAf6Reg_map_sink_expseq_conf + engineOffset);
        }
    }

static void VcatPdhDe1RegsShow(ThaModuleConcate self, AtPdhChannel de1)
    {
    LoVcatChannelRegsShow(self, (AtChannel)de1);
    }

static void PdhChannelRegsShow(ThaModuleConcate self, AtPdhChannel pdhChannel)
    {
    eAtPdhChannelType channelType = AtPdhChannelTypeGet(pdhChannel);

    if ((channelType == cAtPdhChannelTypeDs1) || (channelType == cAtPdhChannelTypeE1))
        VcatPdhDe1RegsShow(self, pdhChannel);

    if ((channelType == cAtPdhChannelTypeDs3) || (channelType == cAtPdhChannelTypeE3))
        VcatPdhDe3RegsShow(self, pdhChannel);
    }

static void OverrideAtObject(AtModuleConcate self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModuleConcate self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, Debug);
        mMethodOverride(m_AtModuleOverride, LongRegisterAccess);
        mMethodOverride(m_AtModuleOverride, Activate);
        mMethodOverride(m_AtModuleOverride, Deactivate);
        mMethodOverride(m_AtModuleOverride, IsActive);
        mMethodOverride(m_AtModuleOverride, HasRegister);
        mMethodOverride(m_AtModuleOverride, InterruptManagerCreate);
        mMethodOverride(m_AtModuleOverride, NumInterruptManagers);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtModuleConcate(AtModuleConcate self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleConcateMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleConcateOverride, mMethodsGet(self), sizeof(m_AtModuleConcateOverride));

        mMethodOverride(m_AtModuleConcateOverride, MaxGroupGet);
        mMethodOverride(m_AtModuleConcateOverride, GroupMemberTypeIsSupported);
        mMethodOverride(m_AtModuleConcateOverride, GroupTypeIsSupported);
        mMethodOverride(m_AtModuleConcateOverride, NonVcatVcgObjectCreate);
        mMethodOverride(m_AtModuleConcateOverride, VcatVcgObjectCreate);
        mMethodOverride(m_AtModuleConcateOverride, CanCreateGroup);
        mMethodOverride(m_AtModuleConcateOverride, IsVcgFixedToGfpChannel);
        mMethodOverride(m_AtModuleConcateOverride, CreateConcateMemberForPdhDe1);
        mMethodOverride(m_AtModuleConcateOverride, CreateConcateMemberForPdhDe3);
        mMethodOverride(m_AtModuleConcateOverride, DeskewLatencyResolutionInFrames);
        mMethodOverride(m_AtModuleConcateOverride, DeskewLatencyThresholdSet);
        mMethodOverride(m_AtModuleConcateOverride, DeskewLatencyThresholdGet);
        }

    mMethodsSet(self, &m_AtModuleConcateOverride);
    }

static void OverrideThaModuleConcate(AtModuleConcate self)
    {
    ThaModuleConcate object = (ThaModuleConcate)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleConcateMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleConcateOverride, m_ThaModuleConcateMethods, sizeof(m_ThaModuleConcateOverride));

        mMethodOverride(m_ThaModuleConcateOverride, DefaultSet);
        mMethodOverride(m_ThaModuleConcateOverride, HoVcDefaultOffset);
        mMethodOverride(m_ThaModuleConcateOverride, Tu3VcDefaultOffset);
        mMethodOverride(m_ThaModuleConcateOverride, Vc1xDefaultOffset);
        mMethodOverride(m_ThaModuleConcateOverride, De1DefaultOffset);
        mMethodOverride(m_ThaModuleConcateOverride, De3DefaultOffset);

        mMethodOverride(m_ThaModuleConcateOverride, CreateVcgMemberObjectForSdhVc);
        mMethodOverride(m_ThaModuleConcateOverride, CreateVcgMemberObjectForSdhVc1x);

        mMethodOverride(m_ThaModuleConcateOverride, BindVc1xToSinkConcateGroup);
        mMethodOverride(m_ThaModuleConcateOverride, BindHoVcToSinkConcateGroup);
        mMethodOverride(m_ThaModuleConcateOverride, BindTu3VcToSinkConcateGroup);
        mMethodOverride(m_ThaModuleConcateOverride, BindDe3ToSinkConcateGroup);
        mMethodOverride(m_ThaModuleConcateOverride, BindDe1ToSinkConcateGroup);
        mMethodOverride(m_ThaModuleConcateOverride, BindVc1xToSourceConcateGroup);
        mMethodOverride(m_ThaModuleConcateOverride, BindHoVcToSourceConcateGroup);
        mMethodOverride(m_ThaModuleConcateOverride, BindTu3VcToSourceConcateGroup);
        mMethodOverride(m_ThaModuleConcateOverride, BindDe3ToSourceConcateGroup);
        mMethodOverride(m_ThaModuleConcateOverride, BindDe1ToSourceConcateGroup);

        mMethodOverride(m_ThaModuleConcateOverride, SdhChannelStsIdSw2HwGet);
        mMethodOverride(m_ThaModuleConcateOverride, PdhChannelHwIdGet);
        mMethodOverride(m_ThaModuleConcateOverride, SdhChannelHwIdGet);

        mMethodOverride(m_ThaModuleConcateOverride, SourceMemberVcxPayloadSet);
        mMethodOverride(m_ThaModuleConcateOverride, SinkMemberVcxPayloadSet);
        mMethodOverride(m_ThaModuleConcateOverride, SourceMemberVc1xPayloadSet);
        mMethodOverride(m_ThaModuleConcateOverride, SinkMemberVc1xPayloadSet);
        mMethodOverride(m_ThaModuleConcateOverride, SdhChannelRegsShow);
        mMethodOverride(m_ThaModuleConcateOverride, SourceMemberAdd);
        mMethodOverride(m_ThaModuleConcateOverride, SourceMemberRemove);
        mMethodOverride(m_ThaModuleConcateOverride, SinkMemberAdd);
        mMethodOverride(m_ThaModuleConcateOverride, SinkMemberRemove);
        mMethodOverride(m_ThaModuleConcateOverride, PdhChannelRegsShow);
        }

    mMethodsSet(object, &m_ThaModuleConcateOverride);
    }

static void Override(AtModuleConcate self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    OverrideAtModuleConcate(self);
    OverrideThaModuleConcate(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081ModuleConcate);
    }

AtModuleConcate Tha60290081ModuleConcateObjectInit(AtModuleConcate self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleConcateObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    /*MethodsInit(mThis(self));*/
    m_methodsInit = 1;

    return self;
    }

AtModuleConcate Tha60290081ModuleConcateNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleConcate newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290081ModuleConcateObjectInit(newModule, device);
    }

uint32 Tha60290081ModuleConcateHoBaseAddress(Tha60290081ModuleConcate self)
    {
    if (self)
        return HoBaseAddress(self);
    return cInvalidUint32;
    }

uint32 Tha60290081ModuleConcateLoBaseAddress(Tha60290081ModuleConcate self)
    {
    if (self)
        return LoBaseAddress(self);
    return cInvalidUint32;
    }

uint32 Tha60290081ModuleConcateNumHoConcateGroups(Tha60290081ModuleConcate self)
    {
    if (self)
        return NumHoConcateGroups(self);
    return 0;
    }

uint32 Tha60290081ModuleConcateNumLoConcateGroups(Tha60290081ModuleConcate self)
    {
    if (self)
        return NumLoConcateGroups(self);
    return 0;
    }

eAtRet Tha60290081ModuleConcateSdhVcSourceMemberAdd(Tha60290081ModuleConcate self, ThaVcgMember member)
    {
    if (self)
        return SdhVcSourceMemberAdd(self, member);
    return cAtErrorNullPointer;
    }

eAtRet Tha60290081ModuleConcateSdhVcSourceMemberRemove(Tha60290081ModuleConcate self, ThaVcgMember member)
    {
    if (self)
        return SdhVcSourceMemberRemove(self, member);
    return cAtErrorNullPointer;
    }

eAtRet Tha60290081ModuleConcateSdhVcSinkMemberAdd(Tha60290081ModuleConcate self, ThaVcgMember member, eBool lcasEnable)
    {
    if (self)
        return SdhVcSinkMemberAdd(self, member, lcasEnable);
    return cAtErrorNullPointer;
    }

eAtRet Tha60290081ModuleConcateSdhVcSinkMemberRemove(Tha60290081ModuleConcate self, ThaVcgMember member)
    {
    if (self)
        return SdhVcSinkMemberRemove(self, member);
    return cAtErrorNullPointer;
    }

uint32 Tha60290081ModuleConcateGroupLocalId(Tha60290081ModuleConcate self, AtConcateGroup group)
    {
    if (self)
        return GroupLocalId(self, group);
    return cInvalidUint32;
    }

eAtRet Tha60290081ModuleConcateHoVcSourcePayloadTypeSet(Tha60290081ModuleConcate self, AtConcateMember member, eBool enable)
    {
    AtConcateGroup group = AtConcateMemberConcateGroupGet(member);
    AtChannel hoVc = AtConcateMemberChannelGet(member);

    if (AtConcateGroupIsVcatVcg(group))
        return VcatHoVcSourcePayloadTypeSet((ThaModuleConcate)self, hoVc, enable);

    return CcatHoVcSourcePayloadTypeSet((ThaModuleConcate)self, hoVc, enable);
    }

eAtRet Tha60290081ModuleConcateHoVcSinkPayloadTypeSet(Tha60290081ModuleConcate self, AtConcateMember member, eBool enable)
    {
    AtConcateGroup group = AtConcateMemberConcateGroupGet(member);
    AtChannel hoVc = AtConcateMemberChannelGet(member);

    if (AtConcateGroupIsVcatVcg(group))
        return VcatHoVcSinkPayloadTypeSet((ThaModuleConcate)self, hoVc, enable);

    return CcatHoVcSinkPayloadTypeSet((ThaModuleConcate)self, hoVc, enable);
    }

AtConcateMember Tha60290081ModuleConcateHoVcatSourceMemberFromHwIdGet(Tha60290081ModuleConcate self, uint16 hwVcatSts)
    {
    if (self)
        return HoVcatSourceMemberFromHwIdGet(self, hwVcatSts);
    return NULL;
    }

AtConcateMember Tha60290081ModuleConcateHoVcatSinkMemberFromHwIdGet(Tha60290081ModuleConcate self, uint16 hwVcatSts)
    {
    if (self)
        return HoVcatSinkMemberFromHwIdGet(self, hwVcatSts);
    return NULL;
    }

void Tha60290081ModuleConcateHwFlush(Tha60290081ModuleConcate self)
    {
    if (self == NULL)
        return;

    HoVcatHwFlush(self);
    LoVcatHwFlush(self);
    }

eAtRet Tha60290081ModuleConcateLoMemberHwIdGet(AtConcateMember member, uint16 *hwStsId, uint8 *hwVtgId, uint8 *hwVtId)
    {
    uint32 hwId;

    if (member == NULL)
        return cAtErrorNullPointer;

    /* Hardware ID must be correctly allocated */
    hwId = AtChannelHwIdGet((AtChannel)member);
    if (hwId == cInvalidUint32)
        return cAtErrorRsrcNoAvail;

    if (hwStsId)
        *hwStsId = (uint16)mRegField(hwId, cLoVcatHwStsID);

    if (hwVtId)
        *hwVtgId = (uint8)mRegField(hwId, cLoVcatHwVtgID);

    if (hwVtId)
        *hwVtId  = (uint8)mRegField(hwId, cLoVcatHwVtID);

    return cAtOk;
    }

eAtRet Tha60290081ModuleConcateLoMemberHwEngineIdGet(AtConcateMember member, uint16 *hwStsEngID, uint8 *hwVtgEngID, uint8 *hwVtEngID)
    {
    AtConcateGroup group = AtConcateMemberConcateGroupGet(member);
    uint16 memberType = AtConcateGroupMemberTypeGet(group);
    uint16 hwSts;
    uint8 hwVtg, hwVt;
    eAtRet ret;

    ret = Tha60290081ModuleConcateLoMemberHwIdGet(member, &hwSts, &hwVtg, &hwVt);
    if (ret != cAtOk)
        return ret;

    if ((memberType == cAtConcateMemberTypeDs3) ||
        (memberType == cAtConcateMemberTypeE3))
        {
        *hwStsEngID = hwSts;
        *hwVtgEngID = 7;
        *hwVtEngID  = 3;
        }

    *hwStsEngID = hwSts;
    *hwVtgEngID = hwVtg;
    *hwVtEngID  = hwVt;
    return cAtOk;
    }

uint32 Tha60290081ModuleConcateLoMemberEngineOffset(Tha60290081ModuleConcate self, AtConcateMember member)
    {
    uint32 hwEngineID = LoMemberHwEngineId(member);
    if (hwEngineID == cInvalidUint32)
        return cInvalidUint32;

    return Tha60290081ModuleConcateLoBaseAddress(self) + hwEngineID;
    }

uint32 Tha60290081ModuleConcateDe3HwIdAllocate(Tha60290081ModuleConcate self, AtConcateMember member)
    {
    if (self)
        return De3HwIdAllocate(self, member);
    return cInvalidUint32;
    }

void Tha60290081ModuleConcateDe3HwIdDeAllocate(Tha60290081ModuleConcate self, AtConcateMember member)
    {
    if (self)
        De3HwIdDeAllocate(self, member);
    }

uint32 Tha60290081ModuleConcateDe1HwIdAllocate(Tha60290081ModuleConcate self, AtConcateMember member)
    {
    if (self)
        return De1HwIdAllocate(self, member);
    return cInvalidUint32;
    }

void Tha60290081ModuleConcateDe1HwIdDeAllocate(Tha60290081ModuleConcate self, AtConcateMember member)
    {
    if (self)
        De1HwIdDeAllocate(self, member);
    }

uint32 Tha60290081ModuleConcateVc1xHwIdAllocate(Tha60290081ModuleConcate self, AtConcateMember member)
    {
    if (self)
        return Vc1xHwIdAllocate(self, member);
    return cInvalidUint32;
    }

void Tha60290081ModuleConcateVc1xHwIdDeAllocate(Tha60290081ModuleConcate self, AtConcateMember member)
    {
    if (self)
        Vc1xHwIdDeAllocate(self, member);
    }
    
AtConcateMember Tha60290081ModuleConcateLoVcatSourceMemberFromHwIdGet(Tha60290081ModuleConcate self, uint32 hwEngineId)
    {
    if (self)
        return LoVcatSourceMemberFromHwIdGet(self, hwEngineId);
    return NULL;
    }

AtConcateMember Tha60290081ModuleConcateLoVcatSinkMemberFromHwIdGet(Tha60290081ModuleConcate self, uint32 hwEngineId)
    {
    if (self)
        return LoVcatSinkMemberFromHwIdGet(self, hwEngineId);
    return NULL;
    }

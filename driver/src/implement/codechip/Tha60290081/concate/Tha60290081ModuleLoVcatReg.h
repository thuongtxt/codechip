/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CONCATE
 * 
 * File        : Tha60290081ModuleLoVcatReg.h
 * 
 * Created Date: Jul 26, 2019
 *
 * Description : This file contain all constant definitions of CONCATE block.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290081MODULELOVCATREG_H_
#define _THA60290081MODULELOVCATREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name   : MAP Master Control
Reg Addr   : 0x000001
Reg Formula:
    Where  :
Reg Desc   :
This register is used to control the operation of MAP

------------------------------------------------------------------------------*/
#define cAf6Reg_map_master_contr                                                                      0x000001

/*--------------------------------------
BitField Name: NonVCATMd
BitField Type: RW
BitField Desc: Non_VCAT mode config
BitField Bits: [1]
--------------------------------------*/
#define cAf6_map_master_contr_NonVCATMd_Mask                                                             cBit1
#define cAf6_map_master_contr_NonVCATMd_Shift                                                                1

/*--------------------------------------
BitField Name: MapUpActive
BitField Type: RW
BitField Desc: Enable MAP Operation
BitField Bits: [0]
--------------------------------------*/
#define cAf6_map_master_contr_MapUpActive_Mask                                                           cBit0
#define cAf6_map_master_contr_MapUpActive_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : MAP Write Hold Register
Reg Addr   : 0x000002
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used for CPU to write to registers that has more than 32 bits. CPU must always write high double word value to this register first.

------------------------------------------------------------------------------*/
#define cAf6Reg_map_wrhold_reg                                                                        0x000002

/*--------------------------------------
BitField Name: MapWrDatHold
BitField Type: RW
BitField Desc: MAP Write Data Hold
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_map_wrhold_reg_MapWrDatHold_Mask                                                         cBit31_0
#define cAf6_map_wrhold_reg_MapWrDatHold_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : MAP Read Hold Register
Reg Addr   : 0x000004
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used for CPU to read registers that has more than 32 bits. CPU must always read low double word data before read this to get high double word.

------------------------------------------------------------------------------*/
#define cAf6Reg_map_rdhold_reg                                                                        0x000004

/*--------------------------------------
BitField Name: MapRdDatHold
BitField Type: RW
BitField Desc: MAP Read Data Hold
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_map_rdhold_reg_MapRdDatHold_Mask                                                         cBit31_0
#define cAf6_map_rdhold_reg_MapRdDatHold_Shift                                                               0

/*------------------------------------------------------------------------------
Reg Name   : MAP Global Interrupt Enable
Reg Addr   : 0x000020
Reg Formula:
    Where  :
Reg Desc   :
This register is used to enable interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_map_glb_intenb                                                                        0x000020

/*--------------------------------------
BitField Name: MapGlbIntEnb
BitField Type: RW
BitField Desc: MAP Global Interrupt Enable 0: disable 1: enable
BitField Bits: [2]
--------------------------------------*/
#define cAf6_map_glb_intenb_MapGlbIntEnb_Mask                                                            cBit2
#define cAf6_map_glb_intenb_MapGlbIntEnb_Shift                                                               2

/*--------------------------------------
BitField Name: MapSkIntEnb
BitField Type: RW
BitField Desc: MAP Sink Interrupt Enable 0: disable 1: enable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_map_glb_intenb_MapSkIntEnb_Mask                                                             cBit1
#define cAf6_map_glb_intenb_MapSkIntEnb_Shift                                                                1

/*--------------------------------------
BitField Name: MapSoIntEnb
BitField Type: RW
BitField Desc: MAP Source Interrupt Enable 0: disable 1: enable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_map_glb_intenb_MapSoIntEnb_Mask                                                             cBit0
#define cAf6_map_glb_intenb_MapSoIntEnb_Shift                                                                0

/*------------------------------------------------------------------------------
Reg Name   : MAP Global Interrupt Status
Reg Addr   : 0x000021
Reg Formula:
    Where  :
Reg Desc   :
This register is used to read interrupt status

------------------------------------------------------------------------------*/
#define cAf6Reg_map_glb_intsta                                                                        0x000021

/*--------------------------------------
BitField Name: MapSkIntSta
BitField Type: RO
BitField Desc: MAP Sink Interrupt Status
BitField Bits: [1]
--------------------------------------*/
#define cAf6_map_glb_intsta_MapSkIntSta_Mask                                                             cBit1
#define cAf6_map_glb_intsta_MapSkIntSta_Shift                                                                1

/*--------------------------------------
BitField Name: MapSoIntSta
BitField Type: RO
BitField Desc: MAP Source Interrupt Status
BitField Bits: [0]
--------------------------------------*/
#define cAf6_map_glb_intsta_MapSoIntSta_Mask                                                             cBit0
#define cAf6_map_glb_intsta_MapSoIntSta_Shift                                                                0

/*------------------------------------------------------------------------------
Reg Name   : MAP Source High Order Mode Config
Reg Addr   : 0x20800
Reg Formula: 0x20800 + $sts_id
    Where  :
           + $sts_id(0-47)
Reg Desc   :
This register is used to config mode for MAP Source Side High Order DS3/E3. HO engine ID will be {sts_id,5'b1_1111}.

------------------------------------------------------------------------------*/
#define cAf6Reg_map_source_ho_mode_cfg                                                                 0x20800

/*--------------------------------------
BitField Name: SoHoUnfrm
BitField Type: RW
BitField Desc: High Order Unframed Mode 1: Enable 0: Disable
BitField Bits: [3]
--------------------------------------*/
#define cAf6_map_source_ho_mode_cfg_SoHoUnfrm_Mask                                                       cBit3
#define cAf6_map_source_ho_mode_cfg_SoHoUnfrm_Shift                                                          3

/*--------------------------------------
BitField Name: SoHoIDEn
BitField Type: RW
BitField Desc: High Order ID Enable 1: Enable 0: Disable
BitField Bits: [2]
--------------------------------------*/
#define cAf6_map_source_ho_mode_cfg_SoHoIDEn_Mask                                                        cBit2
#define cAf6_map_source_ho_mode_cfg_SoHoIDEn_Shift                                                           2

/*--------------------------------------
BitField Name: SoHoDS3Md
BitField Type: RW
BitField Desc: High Order DS3 Mode 1: Enable 0: Disable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_map_source_ho_mode_cfg_SoHoDS3Md_Mask                                                       cBit1
#define cAf6_map_source_ho_mode_cfg_SoHoDS3Md_Shift                                                          1

/*--------------------------------------
BitField Name: SoHoE3Md
BitField Type: RW
BitField Desc: High Order E3 Mode 1: Enable 0: Disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_map_source_ho_mode_cfg_SoHoE3Md_Mask                                                        cBit0
#define cAf6_map_source_ho_mode_cfg_SoHoE3Md_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : MAP Source Low Order Group 28 Mode Config
Reg Addr   : 0x21000
Reg Formula: 0x21000 + $sts_id*0x20 + $vtg_id*0x4 + $vt_id
    Where  :
           + $sts_id(0-47)
           + $vtg_id(0-6)
           + $vt_id(0-3)
Reg Desc   :
This register is used to config mode for MAP Source Side High Order DS1/VT15

------------------------------------------------------------------------------*/
#define cAf6Reg_map_source_logrp28_mode_cfg                                                            0x21000

/*--------------------------------------
BitField Name: SoGrp28Unfrm
BitField Type: RW
BitField Desc: Low Order Group-28 Unframed Mode 1: Enable 0: Disable
BitField Bits: [15]
--------------------------------------*/
#define cAf6_map_source_logrp28_mode_cfg_SoGrp28Unfrm_Mask                                              cBit15
#define cAf6_map_source_logrp28_mode_cfg_SoGrp28Unfrm_Shift                                                 15

/*--------------------------------------
BitField Name: SoGrp28IDEn
BitField Type: RW
BitField Desc: Low Order Group-28 ID Enable 1: Enable 0: Disable
BitField Bits: [14]
--------------------------------------*/
#define cAf6_map_source_logrp28_mode_cfg_SoGrp28IDEn_Mask                                               cBit14
#define cAf6_map_source_logrp28_mode_cfg_SoGrp28IDEn_Shift                                                  14

/*--------------------------------------
BitField Name: SoGrp28VT15Md
BitField Type: RW
BitField Desc: Low Order Group-28 VT15 Mode 1: Enable 0: Disable
BitField Bits: [13]
--------------------------------------*/
#define cAf6_map_source_logrp28_mode_cfg_SoGrp28VT15Md_Mask                                             cBit13
#define cAf6_map_source_logrp28_mode_cfg_SoGrp28VT15Md_Shift                                                13

/*--------------------------------------
BitField Name: SoGrp28DS1Md
BitField Type: RW
BitField Desc: Low Order Group-28 DS1 Mode 1: Enable 0: Disable
BitField Bits: [12]
--------------------------------------*/
#define cAf6_map_source_logrp28_mode_cfg_SoGrp28DS1Md_Mask                                              cBit12
#define cAf6_map_source_logrp28_mode_cfg_SoGrp28DS1Md_Shift                                                 12

/*--------------------------------------
BitField Name: SoGrp28EngID
BitField Type: RW
BitField Desc: Source Group-28 Engine ID assign [10:5]: Engine STS ID (0-47) [
4:2]: Engine VTg ID (0-6) [ 1:0]: Engine VTn ID (0-3)
BitField Bits: [10:0]
--------------------------------------*/
#define cAf6_map_source_logrp28_mode_cfg_SoGrp28EngID_Mask                                            cBit10_0
#define cAf6_map_source_logrp28_mode_cfg_SoGrp28EngID_Shift                                                  0

#define cAf6_map_source_logrp28_mode_cfg_SoGrp28StsEngID_Mask                                         cBit10_5
#define cAf6_map_source_logrp28_mode_cfg_SoGrp28StsEngID_Shift                                               5
#define cAf6_map_source_logrp28_mode_cfg_SoGrp28VtgEngID_Mask                                          cBit4_2
#define cAf6_map_source_logrp28_mode_cfg_SoGrp28VtgEngID_Shift                                               2
#define cAf6_map_source_logrp28_mode_cfg_SoGrp28VtEngID_Mask                                           cBit1_0
#define cAf6_map_source_logrp28_mode_cfg_SoGrp28VtEngID_Shift                                                0

/*------------------------------------------------------------------------------
Reg Name   : MAP Source Low Order Group 21 Mode Config
Reg Addr   : 0x21800
Reg Formula: 0x21800 + $sts_id*0x20 + $vtg_id*0x4 + $vt_id
    Where  :
           + $sts_id(0-47)
           + $vtg_id(0-6)
           + $vt_id(0-2)
Reg Desc   :
This register is used to config mode for MAP Source Side High Order E1/VT2

------------------------------------------------------------------------------*/
#define cAf6Reg_map_source_logrp21_mode_cfg                                                            0x21800

/*--------------------------------------
BitField Name: SoGrp21Unfrm
BitField Type: RW
BitField Desc: Low Order Group-21 Unframed Mode 1: Enable 0: Disable
BitField Bits: [15]
--------------------------------------*/
#define cAf6_map_source_logrp21_mode_cfg_SoGrp21Unfrm_Mask                                              cBit15
#define cAf6_map_source_logrp21_mode_cfg_SoGrp21Unfrm_Shift                                                 15

/*--------------------------------------
BitField Name: SoGrp21IDEn
BitField Type: RW
BitField Desc: Low Order Group-21 ID Enable 1: Enable 0: Disable
BitField Bits: [14]
--------------------------------------*/
#define cAf6_map_source_logrp21_mode_cfg_SoGrp21IDEn_Mask                                               cBit14
#define cAf6_map_source_logrp21_mode_cfg_SoGrp21IDEn_Shift                                                  14

/*--------------------------------------
BitField Name: SoGrp21VT2Md
BitField Type: RW
BitField Desc: Low Order Group-21 VT2 Mode 1: Enable 0: Disable
BitField Bits: [13]
--------------------------------------*/
#define cAf6_map_source_logrp21_mode_cfg_SoGrp21VT2Md_Mask                                              cBit13
#define cAf6_map_source_logrp21_mode_cfg_SoGrp21VT2Md_Shift                                                 13

/*--------------------------------------
BitField Name: SoGrp21E1Md
BitField Type: RW
BitField Desc: Low Order Group-21 E1 Mode 1: Enable 0: Disable
BitField Bits: [12]
--------------------------------------*/
#define cAf6_map_source_logrp21_mode_cfg_SoGrp21E1Md_Mask                                               cBit12
#define cAf6_map_source_logrp21_mode_cfg_SoGrp21E1Md_Shift                                                  12

/*--------------------------------------
BitField Name: SoGrp21EngID
BitField Type: RW
BitField Desc: Source Group-21 Engine ID assign [10:5]: Engine STS ID (0-47) [
4:2]: Engine VTg ID (0-6) [ 1:0]: Engine VTn ID (0-3)
BitField Bits: [10:0]
--------------------------------------*/
#define cAf6_map_source_logrp21_mode_cfg_SoGrp21EngID_Mask                                            cBit10_0
#define cAf6_map_source_logrp21_mode_cfg_SoGrp21EngID_Shift                                                  0


/*------------------------------------------------------------------------------
Reg Name   : MAP Source Member Control
Reg Addr   : 0x22000
Reg Formula: 0x22000 + $eng_sts_id*0x20 + $eng_vtg_id*0x4 + $eng_vt_id
    Where  :
           + $eng_sts_id(0-47)
           + $eng_vtg_id(0-7)
           + $eng_vt_id(0-3)
Reg Desc   :
This register is used to config MAP Source member

------------------------------------------------------------------------------*/
#define cAf6Reg_map_source_mem_ctrl                                                                    0x22000

/*--------------------------------------
BitField Name: SoMemEna
BitField Type: RW
BitField Desc: Source Member enable 1: Enable 0: Disable
BitField Bits: [16]
--------------------------------------*/
#define cAf6_map_source_mem_ctrl_SoMemEna_Mask                                                          cBit16
#define cAf6_map_source_mem_ctrl_SoMemEna_Shift                                                             16

/*--------------------------------------
BitField Name: SoG8040Md
BitField Type: RW
BitField Desc: Source G8040/G804 Mode 1: G8040 0: G804
BitField Bits: [15]
--------------------------------------*/
#define cAf6_map_source_mem_ctrl_SoG8040Md_Mask                                                         cBit15
#define cAf6_map_source_mem_ctrl_SoG8040Md_Shift                                                            15

/*--------------------------------------
BitField Name: SoVcatMd
BitField Type: RW
BitField Desc: Source VCAT Mode 1: VCAT Mode 0: Non_VCAT Mode
BitField Bits: [14]
--------------------------------------*/
#define cAf6_map_source_mem_ctrl_SoVcatMd_Mask                                                          cBit14
#define cAf6_map_source_mem_ctrl_SoVcatMd_Shift                                                             14

/*--------------------------------------
BitField Name: SoLcasMd
BitField Type: RW
BitField Desc: Source LCAS Mode 1: LCAS Mode 0: Non_LCAS Mode
BitField Bits: [13]
--------------------------------------*/
#define cAf6_map_source_mem_ctrl_SoLcasMd_Mask                                                          cBit13
#define cAf6_map_source_mem_ctrl_SoLcasMd_Shift                                                             13

/*--------------------------------------
BitField Name: SoAddCmd
BitField Type: RW
BitField Desc: Source Add Member Command 1: Add member, must set when running
VCAT/LCAS 0: Remove member
BitField Bits: [12]
--------------------------------------*/
#define cAf6_map_source_mem_ctrl_SoAddCmd_Mask                                                          cBit12
#define cAf6_map_source_mem_ctrl_SoAddCmd_Shift                                                             12

/*--------------------------------------
BitField Name: SoVcgId
BitField Type: RW
BitField Desc: Source VCG ID
BitField Bits: [6:0]
--------------------------------------*/
#define cAf6_map_source_mem_ctrl_SoVcgId_Mask                                                          cBit6_0
#define cAf6_map_source_mem_ctrl_SoVcgId_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : MAP Source Tx Sequence Config
Reg Addr   : 0x34000
Reg Formula: 0x34000 + $eng_sts_id*0x20 + $eng_vtg_id*0x4 + $eng_vt_id
    Where  :
           + $eng_sts_id(0-47)
           + $eng_vtg_id(0-7)
           + $eng_vt_id(0-3)
Reg Desc   :
This register is used to config transmit sequence for MAP Source (VCAT mode only)

------------------------------------------------------------------------------*/
#define cAf6Reg_map_source_txseq_config                                                                0x34000

/*--------------------------------------
BitField Name: SoTxSeqEna
BitField Type: RW
BitField Desc: Source Transmit Sequence Enable 1: Enable 0: Disable
BitField Bits: [8]
--------------------------------------*/
#define cAf6_map_source_txseq_config_SoTxSeqEna_Mask                                                     cBit8
#define cAf6_map_source_txseq_config_SoTxSeqEna_Shift                                                        8

/*--------------------------------------
BitField Name: SoTxSeq
BitField Type: RW
BitField Desc: Source Transmit Sequence
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_map_source_txseq_config_SoTxSeq_Mask                                                      cBit5_0
#define cAf6_map_source_txseq_config_SoTxSeq_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : MAP Source Member Status
Reg Addr   : 0x24000
Reg Formula: 0x24000 + $eng_sts_id*0x20 + $eng_vtg_id*0x4 + $eng_vt_id
    Where  :
           + $eng_sts_id(0-47)
           + $eng_vtg_id(0-7)
           + $eng_vt_id(0-3)
Reg Desc   :
This register is used to check status of members LCAS/VCAT. Must be clear after delete member out of VCG.

------------------------------------------------------------------------------*/
#define cAf6Reg_map_source_memsta                                                                      0x24000

/*--------------------------------------
BitField Name: SoMemIdlCnt
BitField Type: RW
BitField Desc: Source Member State IDLE counter
BitField Bits: [26:25]
--------------------------------------*/
#define cAf6_map_source_memsta_SoMemIdlCnt_Mask                                                      cBit26_25
#define cAf6_map_source_memsta_SoMemIdlCnt_Shift                                                            25

/*--------------------------------------
BitField Name: SoMemProCnt
BitField Type: RW
BitField Desc: Source Member Provisioning counter
BitField Bits: [24:23]
--------------------------------------*/
#define cAf6_map_source_memsta_SoMemProCnt_Mask                                                      cBit24_23
#define cAf6_map_source_memsta_SoMemProCnt_Shift                                                            23

/*--------------------------------------
BitField Name: SoMemTxCw
BitField Type: RW
BitField Desc: Source Member Transmit Control Word 0000: FIXED 0001: ADD 0010:
NORM 0101: IDLE 1111: DNU
BitField Bits: [21:18]
--------------------------------------*/
#define cAf6_map_source_memsta_SoMemTxCw_Mask                                                        cBit21_18
#define cAf6_map_source_memsta_SoMemTxCw_Shift                                                              18

/*--------------------------------------
BitField Name: SoMemTxSeq
BitField Type: RW
BitField Desc: Source Member Transmit Sequence
BitField Bits: [17:12]
--------------------------------------*/
#define cAf6_map_source_memsta_SoMemTxSeq_Mask                                                       cBit17_12
#define cAf6_map_source_memsta_SoMemTxSeq_Shift                                                             12

/*--------------------------------------
BitField Name: SoMemRmvSta
BitField Type: RW
BitField Desc: Source Member Remove Status 1: removal done 0: removal is being
or member is deleted out of a VCG.
BitField Bits: [10]
--------------------------------------*/
#define cAf6_map_source_memsta_SoMemRmvSta_Mask                                                         cBit10
#define cAf6_map_source_memsta_SoMemRmvSta_Shift                                                            10

/*--------------------------------------
BitField Name: SoMemAct
BitField Type: RW
BitField Desc: Source Member Active 1: Data are being requested 0: Data are
stopped
BitField Bits: [9]
--------------------------------------*/
#define cAf6_map_source_memsta_SoMemAct_Mask                                                             cBit9
#define cAf6_map_source_memsta_SoMemAct_Shift                                                                9

/*--------------------------------------
BitField Name: SoMemAcpSeq
BitField Type: RW
BitField Desc: Source Member Sequence
BitField Bits: [8:3]
--------------------------------------*/
#define cAf6_map_source_memsta_SoMemAcpSeq_Mask                                                        cBit8_3
#define cAf6_map_source_memsta_SoMemAcpSeq_Shift                                                             3

/*--------------------------------------
BitField Name: SoMemFsm
BitField Type: RW
BitField Desc: Source Member State 000: IDLE state 001: ADD state 010: REMOVE
state 011: NORM or EOS state 100: DNU state 111: FIX state (used for VCAT none-
LCAS mode)
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_map_source_memsta_SoMemFsm_Mask                                                           cBit2_0
#define cAf6_map_source_memsta_SoMemFsm_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : MAP Source VCG Status
Reg Addr   : 0x26000
Reg Formula: 0x26000 + $vcg_id
    Where  :
           + $vcg_id(0-127)
Reg Desc   :
This register is used to check status of VCG. Must be clear after delete VCG.

------------------------------------------------------------------------------*/
#define cAf6Reg_map_source_vcgsta                                                                      0x26000

/*--------------------------------------
BitField Name: SoVcgSta
BitField Type: WO
BitField Desc: Source VCG Status
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_map_source_vcgsta_SoVcgSta_Mask                                                          cBit31_0
#define cAf6_map_source_vcgsta_SoVcgSta_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : MAP Source Timing To PDH MAP
Reg Addr   : 0x38000
Reg Formula: 0x38000 + $eng_sts_id*0x20 + $eng_vtg_id*0x4 + $eng_vt_id
    Where  :
           + $eng_sts_id(0-47)
           + $eng_vtg_id(0-7)
           + $eng_vt_id(0-3)
Reg Desc   :
This register is used to config timing selection for per member at Source.

------------------------------------------------------------------------------*/
#define cAf6Reg_map_source_timing_pdhid                                                                0x38000

/*--------------------------------------
BitField Name: SoPDHIdSlc
BitField Type: RW
BitField Desc: Source PDH ID Slice ID (0-3)
BitField Bits: [12:11]
--------------------------------------*/
#define cAf6_map_source_timing_pdhid_SoPDHIdSlc_Mask                                                 cBit12_11
#define cAf6_map_source_timing_pdhid_SoPDHIdSlc_Shift                                                       11

/*--------------------------------------
BitField Name: SoPDHIdSts
BitField Type: RW
BitField Desc: Source PDH ID STS ID (0-47)
BitField Bits: [10:5]
--------------------------------------*/
#define cAf6_map_source_timing_pdhid_SoPDHIdSts_Mask                                                  cBit10_5
#define cAf6_map_source_timing_pdhid_SoPDHIdSts_Shift                                                        5

/*--------------------------------------
BitField Name: SoPDHIdVtg
BitField Type: RW
BitField Desc: Source PDH ID Vtg ID (0-6)
BitField Bits: [4:2]
--------------------------------------*/
#define cAf6_map_source_timing_pdhid_SoPDHIdVtg_Mask                                                   cBit4_2
#define cAf6_map_source_timing_pdhid_SoPDHIdVtg_Shift                                                        2

/*--------------------------------------
BitField Name: SoPDHIdVtn
BitField Type: RW
BitField Desc: Source PDH ID Vtn ID (0-2/0-3)
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_map_source_timing_pdhid_SoPDHIdVtn_Mask                                                   cBit1_0
#define cAf6_map_source_timing_pdhid_SoPDHIdVtn_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : MAP Source Channel Interrupt Enable
Reg Addr   : 0x3A000
Reg Formula: 0x3A000 + $eng_sts_id*0x20 + $eng_vtg_id*0x4 + $eng_vt_id
    Where  : 
           + $eng_sts_id(0-47)
           + $eng_vtg_id(0-7)
           + $eng_vt_id(0-3)
Reg Desc   : 
This register is used to enable channel interrupt.

------------------------------------------------------------------------------*/
#define cAf6Reg_map_source_chnint_en_Base                                                              0x3A000
#define cAf6Reg_map_source_chnint_en(engstsid, engvtgid, engvtid)     (0x3A000+(engstsid)*0x20+(engvtgid)*0x4+(engvtid))

/*--------------------------------------
BitField Name: SoStaChgIntren
BitField Type: RW
BitField Desc: Source State change interrupt enable 1: Enable 0: Disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_map_source_chnint_en_SoStaChgIntren_Mask                                                    cBit0
#define cAf6_map_source_chnint_en_SoStaChgIntren_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : MAP Source Channel Interrupt Event
Reg Addr   : 0x3A800
Reg Formula: 0x3A800 + $eng_sts_id*0x20 + $eng_vtg_id*0x4 + $eng_vt_id
    Where  : 
           + $eng_sts_id(0-47)
           + $eng_vtg_id(0-7)
           + $eng_vt_id(0-3)
Reg Desc   : 
This register is used to check channel interrupt event.

------------------------------------------------------------------------------*/
#define cAf6Reg_map_source_chnint_event_Base                                                           0x3A800
#define cAf6Reg_map_source_chnint_event(engstsid, engvtgid, engvtid)  (0x3A800+(engstsid)*0x20+(engvtgid)*0x4+(engvtid))

/*--------------------------------------
BitField Name: SoStaChgIntrEvn
BitField Type: RW
BitField Desc: Source State change interrupt event
BitField Bits: [0]
--------------------------------------*/
#define cAf6_map_source_chnint_event_SoStaChgIntrEvn_Mask                                                cBit0
#define cAf6_map_source_chnint_event_SoStaChgIntrEvn_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : MAP Source Channel Interrupt Status
Reg Addr   : 0x3B000
Reg Formula: 0x3B000 + $eng_sts_id*0x20 + $eng_vtg_id*0x4 + $eng_vt_id
    Where  : 
           + $eng_sts_id(0-47)
           + $eng_vtg_id(0-7)
           + $eng_vt_id(0-3)
Reg Desc   : 
This register is used to check channel interrupt current status.

------------------------------------------------------------------------------*/
#define cAf6Reg_map_source_chnint_sta_Base                                                             0x3B000
#define cAf6Reg_map_source_chnint_sta(engstsid, engvtgid, engvtid)    (0x3B000+(engstsid)*0x20+(engvtgid)*0x4+(engvtid))

/*--------------------------------------
BitField Name: SoStaChgIntrCurSta
BitField Type: RW
BitField Desc: Source State change interrupt current status
BitField Bits: [0]
--------------------------------------*/
#define cAf6_map_source_chnint_sta_SoStaChgIntrCurSta_Mask                                               cBit0
#define cAf6_map_source_chnint_sta_SoStaChgIntrCurSta_Shift                                                  0


/*------------------------------------------------------------------------------
Reg Name   : MAP Source Channel Interrupt OR Status
Reg Addr   : 0x3B800
Reg Formula: 0x3B800 + $eng_sts_id
    Where  : 
           + $eng_sts_id(0-47)
Reg Desc   : 
This register consists of 28 bits for 28 VT/TUs or 28 DS1/E1s of the related STS/VC. Each bit is used to store Interrupt OR status of the related VT/TU/DS1/E1.

------------------------------------------------------------------------------*/
#define cAf6Reg_map_source_chnint_or_sta_Base                                                          0x3B800
#define cAf6Reg_map_source_chnint_or_sta(engstsid)                                        (0x3B800+(engstsid))

/*--------------------------------------
BitField Name: SoHOIntrOrSta
BitField Type: RO
BitField Desc: Source HO mode Channel Interrupt OR Status
BitField Bits: [31]
--------------------------------------*/
#define cAf6_map_source_chnint_or_sta_SoHOIntrOrSta_Mask                                                cBit31
#define cAf6_map_source_chnint_or_sta_SoHOIntrOrSta_Shift                                                   31

/*--------------------------------------
BitField Name: SoLOIntrOrSta
BitField Type: RO
BitField Desc: Source LO mode Channel Interrupt OR Status Set to 1 if any
interrupt status bit of corresponding to VT/TU/DS1/E1 is set and its interrupt
is enabled
BitField Bits: [27:0]
--------------------------------------*/
#define cAf6_map_source_chnint_or_sta_SoLOIntrOrSta_Mask                                              cBit27_0
#define cAf6_map_source_chnint_or_sta_SoLOIntrOrSta_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : MAP Source STS/VC Interrupt Enable Control
Reg Addr   : 0x3BFFC
Reg Formula: 0x3BFFC + $grp_id
    Where  : 
           + $grp_id(0-1)
Reg Desc   : 
This register consists of 32 interrupt enable bits for 32 STS/VCs.

------------------------------------------------------------------------------*/
#define cAf6Reg_map_source_stsvcint_enb_ctrl_Base                                                      0x3BFFC
#define cAf6Reg_map_source_stsvcint_enb_ctrl(grpid)                                          (0x3BFFC+(grpid))

/*--------------------------------------
BitField Name: SoStsIntrEn
BitField Type: RO
BitField Desc: Source STS/VC Interrupt Enable Control Set to 1 to enable the
related STS/VC to generate interrupt
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_map_source_stsvcint_enb_ctrl_SoStsIntrEn_Mask                                            cBit31_0
#define cAf6_map_source_stsvcint_enb_ctrl_SoStsIntrEn_Shift                                                  0


/*------------------------------------------------------------------------------
Reg Name   : MAP Source STS/VC Interrupt OR Status
Reg Addr   : 0x3BFFE
Reg Formula: 0x3BFFE + $grp_id
    Where  : 
           + $grp_id(0-1)
Reg Desc   : 
This register consists of 32 bits for 32 STS/VCs. Each bit is used to store Interrupt OR status of the related STS/VC.

------------------------------------------------------------------------------*/
#define cAf6Reg_map_source_stsvcint_or_sta_Base                                                        0x3BFFE
#define cAf6Reg_map_source_stsvcint_or_sta(grpid)                                            (0x3BFFE+(grpid))

/*--------------------------------------
BitField Name: SoStsIntrOrSta
BitField Type: RO
BitField Desc: Source STS/VC Interrupt OR Status Set to 1 if any interrupt
status bit of corresponding STS/VC is set and its interrupt is enabled
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_map_source_stsvcint_or_sta_SoStsIntrOrSta_Mask                                           cBit31_0
#define cAf6_map_source_stsvcint_or_sta_SoStsIntrOrSta_Shift                                                 0


/*------------------------------------------------------------------------------
Reg Name   : MAP Sink High Order Mode Config
Reg Addr   : 0x40800
Reg Formula: 0x40800 + $sts_id
    Where  :
           + $sts_id(0-47)
Reg Desc   :
This register is used to config mode for MAP Sink Side High Order DS3/E3. HO engine ID will be {sts_id,5'b1_1111}.

------------------------------------------------------------------------------*/
#define cAf6Reg_map_sink_ho_mode_cfg                                                                   0x40800

/*--------------------------------------
BitField Name: SkHoPDHIdSlc
BitField Type: RW
BitField Desc: Sink High Order PDH ID Slice ID (0-3)
BitField Bits: [17:16]
--------------------------------------*/
#define cAf6_map_sink_ho_mode_cfg_SkHoPDHIdSlc_Mask                                                  cBit17_16
#define cAf6_map_sink_ho_mode_cfg_SkHoPDHIdSlc_Shift                                                        16

/*--------------------------------------
BitField Name: SkHoPDHIdSts
BitField Type: RW
BitField Desc: Sink High Order PDH ID STS ID (0-47)
BitField Bits: [14:9]
--------------------------------------*/
#define cAf6_map_sink_ho_mode_cfg_SkHoPDHIdSts_Mask                                                   cBit14_9
#define cAf6_map_sink_ho_mode_cfg_SkHoPDHIdSts_Shift                                                         9

/*--------------------------------------
BitField Name: SkHoPDHIdVtg
BitField Type: RW
BitField Desc: Sink High Order PDH ID Vtg ID (0-6)
BitField Bits: [8:6]
--------------------------------------*/
#define cAf6_map_sink_ho_mode_cfg_SkHoPDHIdVtg_Mask                                                    cBit8_6
#define cAf6_map_sink_ho_mode_cfg_SkHoPDHIdVtg_Shift                                                         6

/*--------------------------------------
BitField Name: SkHoPDHIdVtn
BitField Type: RW
BitField Desc: Sink High Order PDH ID Vtn ID (0-2/0-3)
BitField Bits: [5:4]
--------------------------------------*/
#define cAf6_map_sink_ho_mode_cfg_SkHoPDHIdVtn_Mask                                                    cBit5_4
#define cAf6_map_sink_ho_mode_cfg_SkHoPDHIdVtn_Shift                                                         4

/*--------------------------------------
BitField Name: SkHoUnfrm
BitField Type: RW
BitField Desc: High Order Unframed Mode 1: Enable 0: Disable
BitField Bits: [3]
--------------------------------------*/
#define cAf6_map_sink_ho_mode_cfg_SkHoUnfrm_Mask                                                         cBit3
#define cAf6_map_sink_ho_mode_cfg_SkHoUnfrm_Shift                                                            3

/*--------------------------------------
BitField Name: SkHoIDEn
BitField Type: RW
BitField Desc: High Order ID Enable 1: Enable 0: Disable
BitField Bits: [2]
--------------------------------------*/
#define cAf6_map_sink_ho_mode_cfg_SkHoIDEn_Mask                                                          cBit2
#define cAf6_map_sink_ho_mode_cfg_SkHoIDEn_Shift                                                             2

/*--------------------------------------
BitField Name: SkHoDS3Md
BitField Type: RW
BitField Desc: High Order DS3 Mode 1: Enable 0: Disable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_map_sink_ho_mode_cfg_SkHoDS3Md_Mask                                                         cBit1
#define cAf6_map_sink_ho_mode_cfg_SkHoDS3Md_Shift                                                            1

/*--------------------------------------
BitField Name: SkHoE3Md
BitField Type: RW
BitField Desc: High Order E3 Mode 1: Enable 0: Disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_map_sink_ho_mode_cfg_SkHoE3Md_Mask                                                          cBit0
#define cAf6_map_sink_ho_mode_cfg_SkHoE3Md_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : MAP Sink Low Order Group 28 Mode Config
Reg Addr   : 0x41000
Reg Formula: 0x41000 + $sts_id*0x20 + $vtg_id*0x4 + $vt_id
    Where  :
           + $sts_id(0-47)
           + $vtg_id(0-6)
           + $vt_id(0-3)
Reg Desc   :
This register is used to config mode for MAP Sink Side Low Order Group 28 DS1/VT15

------------------------------------------------------------------------------*/
#define cAf6Reg_map_sink_logrp28_mode_cfg                                                              0x41000

/*--------------------------------------
BitField Name: SkGrp28PDHIdSlc
BitField Type: RW
BitField Desc: Sink Low Order Group 28 PDH ID Slice ID (0-3)
BitField Bits: [29:28]
--------------------------------------*/
#define cAf6_map_sink_logrp28_mode_cfg_SkGrp28PDHIdSlc_Mask                                          cBit29_28
#define cAf6_map_sink_logrp28_mode_cfg_SkGrp28PDHIdSlc_Shift                                                28

/*--------------------------------------
BitField Name: SkGrp28PDHIdSts
BitField Type: RW
BitField Desc: Sink Low Order Group 28 PDH ID STS ID (0-47)
BitField Bits: [26:21]
--------------------------------------*/
#define cAf6_map_sink_logrp28_mode_cfg_SkGrp28PDHIdSts_Mask                                          cBit26_21
#define cAf6_map_sink_logrp28_mode_cfg_SkGrp28PDHIdSts_Shift                                                21

/*--------------------------------------
BitField Name: SkGrp28PDHIdVtg
BitField Type: RW
BitField Desc: Sink Low Order Group 28 PDH ID Vtg ID (0-6)
BitField Bits: [20:18]
--------------------------------------*/
#define cAf6_map_sink_logrp28_mode_cfg_SkGrp28PDHIdVtg_Mask                                          cBit20_18
#define cAf6_map_sink_logrp28_mode_cfg_SkGrp28PDHIdVtg_Shift                                                18

/*--------------------------------------
BitField Name: SkGrp28PDHIdVtn
BitField Type: RW
BitField Desc: Sink Low Order Group 28 PDH ID Vtn ID (0-2/0-3)
BitField Bits: [17:16]
--------------------------------------*/
#define cAf6_map_sink_logrp28_mode_cfg_SkGrp28PDHIdVtn_Mask                                          cBit17_16
#define cAf6_map_sink_logrp28_mode_cfg_SkGrp28PDHIdVtn_Shift                                                16

/*--------------------------------------
BitField Name: SkGrp28Unfrm
BitField Type: RW
BitField Desc: Low Order Group-28 Unframed Mode 1: Enable 0: Disable
BitField Bits: [15]
--------------------------------------*/
#define cAf6_map_sink_logrp28_mode_cfg_SkGrp28Unfrm_Mask                                                cBit15
#define cAf6_map_sink_logrp28_mode_cfg_SkGrp28Unfrm_Shift                                                   15

/*--------------------------------------
BitField Name: SkGrp28IDEn
BitField Type: RW
BitField Desc: Low Order Group-28 ID Enable 1: Enable 0: Disable
BitField Bits: [14]
--------------------------------------*/
#define cAf6_map_sink_logrp28_mode_cfg_SkGrp28IDEn_Mask                                                 cBit14
#define cAf6_map_sink_logrp28_mode_cfg_SkGrp28IDEn_Shift                                                    14

/*--------------------------------------
BitField Name: SkGrp28VT15Md
BitField Type: RW
BitField Desc: Low Order Group-28 VT15 Mode 1: Enable 0: Disable
BitField Bits: [13]
--------------------------------------*/
#define cAf6_map_sink_logrp28_mode_cfg_SkGrp28VT15Md_Mask                                               cBit13
#define cAf6_map_sink_logrp28_mode_cfg_SkGrp28VT15Md_Shift                                                  13

/*--------------------------------------
BitField Name: SkGrp28DS1Md
BitField Type: RW
BitField Desc: Low Order Group-28 DS1 Mode 1: Enable 0: Disable
BitField Bits: [12]
--------------------------------------*/
#define cAf6_map_sink_logrp28_mode_cfg_SkGrp28DS1Md_Mask                                                cBit12
#define cAf6_map_sink_logrp28_mode_cfg_SkGrp28DS1Md_Shift                                                   12

/*--------------------------------------
BitField Name: SkGrp28EngID
BitField Type: RW
BitField Desc: Sink Group-28 Engine ID assign [10:5]: Engine STS ID (0-47) [
4:2]: Engine VTg ID (0-6) [ 1:0]: Engine VTn ID (0-3)
BitField Bits: [10:0]
--------------------------------------*/
#define cAf6_map_sink_logrp28_mode_cfg_SkGrp28EngID_Mask                                              cBit10_0
#define cAf6_map_sink_logrp28_mode_cfg_SkGrp28EngID_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : MAP Sink Low Order Group 21 Mode Config
Reg Addr   : 0x41800
Reg Formula: 0x41800 + $sts_id*0x20 + $vtg_id*0x4 + $vt_id
    Where  :
           + $sts_id(0-47)
           + $vtg_id(0-6)
           + $vt_id(0-2)
Reg Desc   :
This register is used to config mode for MAP Sink Side Low Order Group 21 E1/VT2

------------------------------------------------------------------------------*/
#define cAf6Reg_map_sink_logrp21_mode_cfg                                                              0x41800

/*--------------------------------------
BitField Name: SkGrp21PDHIdSlc
BitField Type: RW
BitField Desc: Sink Low Order Group 21 PDH ID Slice ID (0-3)
BitField Bits: [29:28]
--------------------------------------*/
#define cAf6_map_sink_logrp21_mode_cfg_SkGrp21PDHIdSlc_Mask                                          cBit29_28
#define cAf6_map_sink_logrp21_mode_cfg_SkGrp21PDHIdSlc_Shift                                                28

/*--------------------------------------
BitField Name: SkGrp21PDHIdSts
BitField Type: RW
BitField Desc: Sink Low Order Group 21 PDH ID STS ID (0-47)
BitField Bits: [26:21]
--------------------------------------*/
#define cAf6_map_sink_logrp21_mode_cfg_SkGrp21PDHIdSts_Mask                                          cBit26_21
#define cAf6_map_sink_logrp21_mode_cfg_SkGrp21PDHIdSts_Shift                                                21

/*--------------------------------------
BitField Name: SkGrp21PDHIdVtg
BitField Type: RW
BitField Desc: Sink Low Order Group 21 PDH ID Vtg ID (0-6)
BitField Bits: [20:18]
--------------------------------------*/
#define cAf6_map_sink_logrp21_mode_cfg_SkGrp21PDHIdVtg_Mask                                          cBit20_18
#define cAf6_map_sink_logrp21_mode_cfg_SkGrp21PDHIdVtg_Shift                                                18

/*--------------------------------------
BitField Name: SkGrp21PDHIdVtn
BitField Type: RW
BitField Desc: Sink Low Order Group 21 PDH ID Vtn ID (0-2/0-3)
BitField Bits: [17:16]
--------------------------------------*/
#define cAf6_map_sink_logrp21_mode_cfg_SkGrp21PDHIdVtn_Mask                                          cBit17_16
#define cAf6_map_sink_logrp21_mode_cfg_SkGrp21PDHIdVtn_Shift                                                16

/*--------------------------------------
BitField Name: SkGrp21Unfrm
BitField Type: RW
BitField Desc: Low Order Group-21 Unframed Mode 1: Enable 0: Disable
BitField Bits: [15]
--------------------------------------*/
#define cAf6_map_sink_logrp21_mode_cfg_SkGrp21Unfrm_Mask                                                cBit15
#define cAf6_map_sink_logrp21_mode_cfg_SkGrp21Unfrm_Shift                                                   15

/*--------------------------------------
BitField Name: SkGrp21IDEn
BitField Type: RW
BitField Desc: Low Order Group-21 ID Enable 1: Enable 0: Disable
BitField Bits: [14]
--------------------------------------*/
#define cAf6_map_sink_logrp21_mode_cfg_SkGrp21IDEn_Mask                                                 cBit14
#define cAf6_map_sink_logrp21_mode_cfg_SkGrp21IDEn_Shift                                                    14

/*--------------------------------------
BitField Name: SkGrp21VT2Md
BitField Type: RW
BitField Desc: Low Order Group-21 VT2 Mode 1: Enable 0: Disable
BitField Bits: [13]
--------------------------------------*/
#define cAf6_map_sink_logrp21_mode_cfg_SkGrp21VT2Md_Mask                                                cBit13
#define cAf6_map_sink_logrp21_mode_cfg_SkGrp21VT2Md_Shift                                                   13

/*--------------------------------------
BitField Name: SkGrp21E1Md
BitField Type: RW
BitField Desc: Low Order Group-21 E1 Mode 1: Enable 0: Disable
BitField Bits: [12]
--------------------------------------*/
#define cAf6_map_sink_logrp21_mode_cfg_SkGrp21E1Md_Mask                                                 cBit12
#define cAf6_map_sink_logrp21_mode_cfg_SkGrp21E1Md_Shift                                                    12

/*--------------------------------------
BitField Name: SkGrp21EngID
BitField Type: RW
BitField Desc: Sink Group-21 Engine ID assign [10:5]: Engine STS ID (0-47) [
4:2]: Engine VTg ID (0-6) [ 1:0]: Engine VTn ID (0-3)
BitField Bits: [10:0]
--------------------------------------*/
#define cAf6_map_sink_logrp21_mode_cfg_SkGrp21EngID_Mask                                              cBit10_0
#define cAf6_map_sink_logrp21_mode_cfg_SkGrp21EngID_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : MAP Sink Member Control
Reg Addr   : 0x48800
Reg Formula: 0x48800 + $eng_sts_id*0x20 + $eng_vtg_id*0x4 + $eng_vt_id
    Where  :
           + $eng_sts_id(0-47)
           + $eng_vtg_id(0-7)
           + $eng_vt_id(0-3)
Reg Desc   :
This register is used to config MAP Sink member

------------------------------------------------------------------------------*/
#define cAf6Reg_map_sink_mem_ctrl                                                                      0x48800

/*--------------------------------------
BitField Name: SkMemEna
BitField Type: RW
BitField Desc: Sink Member enable 1: Enable 0: Disable
BitField Bits: [16]
--------------------------------------*/
#define cAf6_map_sink_mem_ctrl_SkMemEna_Mask                                                            cBit16
#define cAf6_map_sink_mem_ctrl_SkMemEna_Shift                                                               16

/*--------------------------------------
BitField Name: SkG8040Md
BitField Type: RW
BitField Desc: Sink G8040/G804 Mode 1: G8040 0: G804
BitField Bits: [15]
--------------------------------------*/
#define cAf6_map_sink_mem_ctrl_SkG8040Md_Mask                                                           cBit15
#define cAf6_map_sink_mem_ctrl_SkG8040Md_Shift                                                              15

/*--------------------------------------
BitField Name: SkVcatMd
BitField Type: RW
BitField Desc: Sink VCAT Mode 1: VCAT Mode 0: Non_VCAT Mode
BitField Bits: [14]
--------------------------------------*/
#define cAf6_map_sink_mem_ctrl_SkVcatMd_Mask                                                            cBit14
#define cAf6_map_sink_mem_ctrl_SkVcatMd_Shift                                                               14

/*--------------------------------------
BitField Name: SkLcasMd
BitField Type: RW
BitField Desc: Sink LCAS Mode 1: LCAS Mode 0: Non_LCAS Mode
BitField Bits: [13]
--------------------------------------*/
#define cAf6_map_sink_mem_ctrl_SkLcasMd_Mask                                                            cBit13
#define cAf6_map_sink_mem_ctrl_SkLcasMd_Shift                                                               13

/*--------------------------------------
BitField Name: SkAddCmd
BitField Type: RW
BitField Desc: Sink Add Member Command 1: Add member, must set when running
VCAT/LCAS 0: Remove member
BitField Bits: [12]
--------------------------------------*/
#define cAf6_map_sink_mem_ctrl_SkAddCmd_Mask                                                            cBit12
#define cAf6_map_sink_mem_ctrl_SkAddCmd_Shift                                                               12

/*--------------------------------------
BitField Name: SkVcgId
BitField Type: RW
BitField Desc: Sink VCG ID
BitField Bits: [6:0]
--------------------------------------*/
#define cAf6_map_sink_mem_ctrl_SkVcgId_Mask                                                            cBit6_0
#define cAf6_map_sink_mem_ctrl_SkVcgId_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : MAP Sink Threshold Config
Reg Addr   : 0x55400
Reg Formula: 0x55400 + $vcg_id
    Where  :
           + $vcg_id(0-127)
Reg Desc   :
This register is used to config timer threshold at Sink side

------------------------------------------------------------------------------*/
#define cAf6Reg_map_sink_thres_conf                                                                    0x55400

/*--------------------------------------
BitField Name: SkRMVDis
BitField Type: RW
BitField Desc: Sink Remove Timer disable 1: Disable 0: Enable
BitField Bits: [31]
--------------------------------------*/
#define cAf6_map_sink_thres_conf_SkRMVDis_Mask                                                          cBit31
#define cAf6_map_sink_thres_conf_SkRMVDis_Shift                                                             31

/*--------------------------------------
BitField Name: SkWTRDis
BitField Type: RW
BitField Desc: Sink Wait-To-Restore Timer disable 1: Disable 0: Enable
BitField Bits: [30]
--------------------------------------*/
#define cAf6_map_sink_thres_conf_SkWTRDis_Mask                                                          cBit30
#define cAf6_map_sink_thres_conf_SkWTRDis_Shift                                                             30

/*--------------------------------------
BitField Name: SkHODis
BitField Type: RW
BitField Desc: Sink Hold-Off Timer disable 1: Disable 0: Enable
BitField Bits: [29]
--------------------------------------*/
#define cAf6_map_sink_thres_conf_SkHODis_Mask                                                           cBit29
#define cAf6_map_sink_thres_conf_SkHODis_Shift                                                              29

/*--------------------------------------
BitField Name: SkRMVThres
BitField Type: RW
BitField Desc: Sink Remove Timer Threshold Value is from 0 to 255. Should be
init value 256ms when create VCG. For VC3/VC4/E3 mode: each step is 2 ms For DS3
mode: each step is ~ 1.702 ms For DS1 mode: each step is 48 ms For E1 mode: each
step is 32 ms For VC11/VC12 mode: each step is 16 ms
BitField Bits: [28:21]
--------------------------------------*/
#define cAf6_map_sink_thres_conf_SkRMVThres_Mask                                                     cBit28_21
#define cAf6_map_sink_thres_conf_SkRMVThres_Shift                                                           21

/*--------------------------------------
BitField Name: SkWTRThres
BitField Type: RW
BitField Desc: Sink Wait-To-Restore Timer Threshold Value is from 0 to 8063. For
VC3/VC4/E3 mode: each step is 100ms. The maximum time to support is 13.44
minutes. For DS3 mode: each step is ~ 100.444ms. The maximum time to support is
~13.50 minutes. For DS1/E1/VC11/VC12 mode: each step is 96 ms. The maximum time
to support is ~ 12.90 minutes.
BitField Bits: [20:8]
--------------------------------------*/
#define cAf6_map_sink_thres_conf_SkWTRThres_Mask                                                      cBit20_8
#define cAf6_map_sink_thres_conf_SkWTRThres_Shift                                                            8

/*--------------------------------------
BitField Name: SkHOThres
BitField Type: RW
BitField Desc: Sink Hold-Off Timer Threshold Value is from 0 to 255. For
VC3/VC4/E3 mode: each step is 100ms. The maximum time to support is 25.6
seconds. For DS3 mode: each step is ~ 100.444ms. The maximum time to support is
~25.71 seconds. For DS1/E1/VC11/VC12 mode: each step is 96 ms. The maximum time
to support is  24.576 seconds.
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_map_sink_thres_conf_SkHOThres_Mask                                                        cBit7_0
#define cAf6_map_sink_thres_conf_SkHOThres_Shift                                                             0

/*------------------------------------------------------------------------------
Reg Name   : MAP Sink Expected Sequence Configuration
Reg Addr   : 0x51800
Reg Formula: 0x51800 + $eng_sts_id*0x20 + $eng_vtg_id*0x4 + $eng_vt_id
    Where  :
           + $eng_sts_id(0-47)
           + $eng_vtg_id(0-7)
           + $eng_vt_id(0-3)
Reg Desc   :
This register is used to config Sink expected sequence to check SQM.

------------------------------------------------------------------------------*/
#define cAf6Reg_map_sink_expseq_conf                                                                   0x51800

/*--------------------------------------
BitField Name: SkTxSeq
BitField Type: RW
BitField Desc: Sink Expected Sequence
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_map_sink_expseq_conf_SkTxSeq_Mask                                                         cBit5_0
#define cAf6_map_sink_expseq_conf_SkTxSeq_Shift                                                              0

/*------------------------------------------------------------------------------
Reg Name   : MAP Sink Member Status
Reg Addr   : 0x50800
Reg Formula: 0x50800 + $eng_sts_id*0x20 + $eng_vtg_id*0x4 + $eng_vt_id
    Where  :
           + $eng_sts_id(0-47)
           + $eng_vtg_id(0-7)
           + $eng_vt_id(0-3)
Reg Desc   :
This register is used to check status of members LCAS/VCAT. Must be clear after delete member out of VCG.

------------------------------------------------------------------------------*/
#define cAf6Reg_map_sink_memsta                                                                        0x50800

/*--------------------------------------
BitField Name: SkWTRSta
BitField Type: RW
BitField Desc: Sink Member in WTR State IDLE counter
BitField Bits: [24]
--------------------------------------*/
#define cAf6_map_sink_memsta_SkWTRSta_Mask                                                              cBit24
#define cAf6_map_sink_memsta_SkWTRSta_Shift                                                                 24

/*--------------------------------------
BitField Name: SkMemIdlCnt
BitField Type: RW
BitField Desc: Sink Member State IDLE counter
BitField Bits: [23:22]
--------------------------------------*/
#define cAf6_map_sink_memsta_SkMemIdlCnt_Mask                                                        cBit23_22
#define cAf6_map_sink_memsta_SkMemIdlCnt_Shift                                                              22

/*--------------------------------------
BitField Name: SkMemRxCw
BitField Type: RW
BitField Desc: Sink Member Received Control Word 0000: FIXED 0001: ADD 0010:
NORM 0101: IDLE 1111: DNU
BitField Bits: [17:14]
--------------------------------------*/
#define cAf6_map_sink_memsta_SkMemRxCw_Mask                                                          cBit17_14
#define cAf6_map_sink_memsta_SkMemRxCw_Shift                                                                14

/*--------------------------------------
BitField Name: SkMemAcpSeq
BitField Type: RW
BitField Desc: Sink Member Sequence
BitField Bits: [9:4]
--------------------------------------*/
#define cAf6_map_sink_memsta_SkMemAcpSeq_Mask                                                          cBit9_4
#define cAf6_map_sink_memsta_SkMemAcpSeq_Shift                                                               4

/*--------------------------------------
BitField Name: SkMemDatSta
BitField Type: RW
BitField Desc: Sink Member Data Status
BitField Bits: [3]
--------------------------------------*/
#define cAf6_map_sink_memsta_SkMemDatSta_Mask                                                            cBit3
#define cAf6_map_sink_memsta_SkMemDatSta_Shift                                                               3

/*--------------------------------------
BitField Name: SkMemFsm
BitField Type: RW
BitField Desc: Sink Member State 00: IDLE state 01: FAIL state 10: OK state 11:
FIX state (used for VCAT none-LCAS mode)
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_map_sink_memsta_SkMemFsm_Mask                                                             cBit1_0
#define cAf6_map_sink_memsta_SkMemFsm_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : MAP Sink VCG Status
Reg Addr   : 0x51000
Reg Formula: 0x51000 + $vcg_id
    Where  :
           + $vcg_id(0-127)
Reg Desc   :
This register is used to check status of VCG. Must be clear after delete VCG.

------------------------------------------------------------------------------*/
#define cAf6Reg_map_sink_vcgsta                                                                        0x51000

/*--------------------------------------
BitField Name: SkVcgSta
BitField Type: WO
BitField Desc: Sink VCG Status
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_map_sink_vcgsta_SkVcgSta_Mask                                                            cBit31_0
#define cAf6_map_sink_vcgsta_SkVcgSta_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : MAP Sink Transmit MST
Reg Addr   : 0x55100
Reg Formula: 0x55100 + $vcg_id
    Where  :
           + $vcg_id(0-127)
Reg Desc   :
This register is used to check status of VCG TxMST. Must be filled when create VCG. For VCG have more than 32 members, need to use hold registers to read, write.

------------------------------------------------------------------------------*/
#define cAf6Reg_map_sink_txmst                                                                         0x55100

/*--------------------------------------
BitField Name: SkTxMst
BitField Type: RW
BitField Desc: Sink Transmit MST Each bit for each member in VCG. Must write
init value '1' for each member when create VCG in LCAS mode.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_map_sink_txmst_SkTxMst_Mask                                                              cBit31_0
#define cAf6_map_sink_txmst_SkTxMst_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : MAP Sink Receive MST
Reg Addr   : 0x55000
Reg Formula: 0x55000 + $vcg_id
    Where  :
           + $vcg_id(0-127)
Reg Desc   :
This register is used to check status of VCG RxMST. Must be filled when create VCG. For VCG have more than 32 members, need to use hold registers to read, write.

------------------------------------------------------------------------------*/
#define cAf6Reg_map_sink_rxmst                                                                         0x55000

/*--------------------------------------
BitField Name: SkRxMst
BitField Type: RW
BitField Desc: Sink Receive MST Each bit for each member in VCG. Must write init
value '1' for each member when create VCG in LCAS mode.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_map_sink_rxmst_SkRxMst_Mask                                                              cBit31_0
#define cAf6_map_sink_rxmst_SkRxMst_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : MAP Rs ACK Status
Reg Addr   : 0x55200
Reg Formula: 0x55200 + $vcg_id
    Where  : 
           + $vcg_id(0-127)
Reg Desc   : 
This register is used to check status of VCG RxMST. Must be filled when create VCG. For VCG have more than 32 members, need to use hold registers to read, write.

------------------------------------------------------------------------------*/
#define cAf6Reg_map_rsack_sta_Base                                                                     0x55200
#define cAf6Reg_map_rsack_sta(vcgid)                                                         (0x55200+(vcgid))

/*--------------------------------------
BitField Name: RxRsAck
BitField Type: RW
BitField Desc: Rx Rs ACK value
BitField Bits: [1]
--------------------------------------*/
#define cAf6_map_rsack_sta_RxRsAck_Mask                                                                  cBit1
#define cAf6_map_rsack_sta_RxRsAck_Shift                                                                     1

/*--------------------------------------
BitField Name: TxRsAck
BitField Type: RW
BitField Desc: Tx Rs ACK value
BitField Bits: [0]
--------------------------------------*/
#define cAf6_map_rsack_sta_TxRsAck_Mask                                                                  cBit0
#define cAf6_map_rsack_sta_TxRsAck_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : MAP Sink Channel Interrupt Enable
Reg Addr   : 0x56000
Reg Formula: 0x56000 + $eng_sts_id*0x20 + $eng_vtg_id*0x4 + $eng_vt_id
    Where  :
           + $eng_sts_id(0-47)
           + $eng_vtg_id(0-7)
           + $eng_vt_id(0-3)
Reg Desc   :
This register is used to enable channel interrupt.

------------------------------------------------------------------------------*/
#define cAf6Reg_map_sink_chnint_en                                                                     0x56000

/*--------------------------------------
BitField Name: SkStaChgIntren
BitField Type: RW
BitField Desc: Sink State change interrupt enable 1: Enable 0: Disable
BitField Bits: [8]
--------------------------------------*/
#define cAf6_map_sink_chnint_en_SkStaChgIntren_Mask                                                      cBit8
#define cAf6_map_sink_chnint_en_SkStaChgIntren_Shift                                                         8

/*--------------------------------------
BitField Name: SkGIDMIntren
BitField Type: RW
BitField Desc: Sink GID Mismatch interrupt enable 1: Enable 0: Disable
BitField Bits: [7]
--------------------------------------*/
#define cAf6_map_sink_chnint_en_SkGIDMIntren_Mask                                                        cBit7
#define cAf6_map_sink_chnint_en_SkGIDMIntren_Shift                                                           7

/*--------------------------------------
BitField Name: SkCRCErrIntren
BitField Type: RW
BitField Desc: Sink CRC Error interrupt enable 1: Enable 0: Disable
BitField Bits: [6]
--------------------------------------*/
#define cAf6_map_sink_chnint_en_SkCRCErrIntren_Mask                                                      cBit6
#define cAf6_map_sink_chnint_en_SkCRCErrIntren_Shift                                                         6

/*--------------------------------------
BitField Name: SkSQNCIntren
BitField Type: RW
BitField Desc: Sink SQNC interrupt enable 1: Enable 0: Disable
BitField Bits: [5]
--------------------------------------*/
#define cAf6_map_sink_chnint_en_SkSQNCIntren_Mask                                                        cBit5
#define cAf6_map_sink_chnint_en_SkSQNCIntren_Shift                                                           5

/*--------------------------------------
BitField Name: SkMNDIntren
BitField Type: RW
BitField Desc: Sink MND interrupt enable 1: Enable 0: Disable
BitField Bits: [4]
--------------------------------------*/
#define cAf6_map_sink_chnint_en_SkMNDIntren_Mask                                                         cBit4
#define cAf6_map_sink_chnint_en_SkMNDIntren_Shift                                                            4

/*--------------------------------------
BitField Name: SkLOAIntren
BitField Type: RW
BitField Desc: Sink LOA interrupt enable 1: Enable 0: Disable
BitField Bits: [3]
--------------------------------------*/
#define cAf6_map_sink_chnint_en_SkLOAIntren_Mask                                                         cBit3
#define cAf6_map_sink_chnint_en_SkLOAIntren_Shift                                                            3

/*--------------------------------------
BitField Name: SkSQMIntren
BitField Type: RW
BitField Desc: Sink Sequence Mismatch interrupt enable 1: Enable 0: Disable
BitField Bits: [2]
--------------------------------------*/
#define cAf6_map_sink_chnint_en_SkSQMIntren_Mask                                                         cBit2
#define cAf6_map_sink_chnint_en_SkSQMIntren_Shift                                                            2

/*--------------------------------------
BitField Name: SkOOMIntren
BitField Type: RW
BitField Desc: Sink OOM interrupt enable 1: Enable 0: Disable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_map_sink_chnint_en_SkOOMIntren_Mask                                                         cBit1
#define cAf6_map_sink_chnint_en_SkOOMIntren_Shift                                                            1

/*--------------------------------------
BitField Name: SkLOMIntren
BitField Type: RW
BitField Desc: Sink LOM interrupt enable 1: Enable 0: Disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_map_sink_chnint_en_SkLOMIntren_Mask                                                         cBit0
#define cAf6_map_sink_chnint_en_SkLOMIntren_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : MAP Sink Channel Interrupt Event
Reg Addr   : 0x56800
Reg Formula: 0x56800 + $eng_sts_id*0x20 + $eng_vtg_id*0x4 + $eng_vt_id
    Where  :
           + $eng_sts_id(0-47)
           + $eng_vtg_id(0-7)
           + $eng_vt_id(0-3)
Reg Desc   :
This register is used to check channel interrupt event.

------------------------------------------------------------------------------*/
#define cAf6Reg_map_sink_chnint_event                                                                  0x56800

/*--------------------------------------
BitField Name: SkStaChgIntrEvn
BitField Type: RW
BitField Desc: Sink State change interrupt event
BitField Bits: [8]
--------------------------------------*/
#define cAf6_map_sink_chnint_event_SkStaChgIntrEvn_Mask                                                  cBit8
#define cAf6_map_sink_chnint_event_SkStaChgIntrEvn_Shift                                                     8

/*--------------------------------------
BitField Name: SkGIDMIntrEvn
BitField Type: RW
BitField Desc: Sink GID Mismatch interrupt event
BitField Bits: [7]
--------------------------------------*/
#define cAf6_map_sink_chnint_event_SkGIDMIntrEvn_Mask                                                    cBit7
#define cAf6_map_sink_chnint_event_SkGIDMIntrEvn_Shift                                                       7

/*--------------------------------------
BitField Name: SkCRCErrIntrEvn
BitField Type: RW
BitField Desc: Sink CRC Error interrupt event
BitField Bits: [6]
--------------------------------------*/
#define cAf6_map_sink_chnint_event_SkCRCErrIntrEvn_Mask                                                  cBit6
#define cAf6_map_sink_chnint_event_SkCRCErrIntrEvn_Shift                                                     6

/*--------------------------------------
BitField Name: SkSQNCIntrEvn
BitField Type: RW
BitField Desc: Sink SQNC interrupt event
BitField Bits: [5]
--------------------------------------*/
#define cAf6_map_sink_chnint_event_SkSQNCIntrEvn_Mask                                                    cBit5
#define cAf6_map_sink_chnint_event_SkSQNCIntrEvn_Shift                                                       5

/*--------------------------------------
BitField Name: SkMNDIntrEvn
BitField Type: RW
BitField Desc: Sink MND interrupt event
BitField Bits: [4]
--------------------------------------*/
#define cAf6_map_sink_chnint_event_SkMNDIntrEvn_Mask                                                     cBit4
#define cAf6_map_sink_chnint_event_SkMNDIntrEvn_Shift                                                        4

/*--------------------------------------
BitField Name: SkLOAIntrEvn
BitField Type: RW
BitField Desc: Sink LOA interrupt event
BitField Bits: [3]
--------------------------------------*/
#define cAf6_map_sink_chnint_event_SkLOAIntrEvn_Mask                                                     cBit3
#define cAf6_map_sink_chnint_event_SkLOAIntrEvn_Shift                                                        3

/*--------------------------------------
BitField Name: SkSQMIntrEvn
BitField Type: RW
BitField Desc: Sink Sequence Mismatch interrupt event
BitField Bits: [2]
--------------------------------------*/
#define cAf6_map_sink_chnint_event_SkSQMIntrEvn_Mask                                                     cBit2
#define cAf6_map_sink_chnint_event_SkSQMIntrEvn_Shift                                                        2

/*--------------------------------------
BitField Name: SkOOMIntrEvn
BitField Type: RW
BitField Desc: Sink OOM interrupt event
BitField Bits: [1]
--------------------------------------*/
#define cAf6_map_sink_chnint_event_SkOOMIntrEvn_Mask                                                     cBit1
#define cAf6_map_sink_chnint_event_SkOOMIntrEvn_Shift                                                        1

/*--------------------------------------
BitField Name: SkLOMIntrEvn
BitField Type: RW
BitField Desc: Sink LOM interrupt event
BitField Bits: [0]
--------------------------------------*/
#define cAf6_map_sink_chnint_event_SkLOMIntrEvn_Mask                                                     cBit0
#define cAf6_map_sink_chnint_event_SkLOMIntrEvn_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : MAP Sink Channel Interrupt Status
Reg Addr   : 0x57000
Reg Formula: 0x57000 + $eng_sts_id*0x20 + $eng_vtg_id*0x4 + $eng_vt_id
    Where  :
           + $eng_sts_id(0-47)
           + $eng_vtg_id(0-7)
           + $eng_vt_id(0-3)
Reg Desc   :
This register is used to check channel interrupt current status.

------------------------------------------------------------------------------*/
#define cAf6Reg_map_sink_chnint_sta                                                                    0x57000

/*--------------------------------------
BitField Name: SkStaChgIntrCurSta
BitField Type: RW
BitField Desc: Sink State change interrupt current status
BitField Bits: [8]
--------------------------------------*/
#define cAf6_map_sink_chnint_sta_SkStaChgIntrCurSta_Mask                                                 cBit8
#define cAf6_map_sink_chnint_sta_SkStaChgIntrCurSta_Shift                                                    8

/*--------------------------------------
BitField Name: SkGIDMIntrCurSta
BitField Type: RW
BitField Desc: Sink GID Mismatch interrupt current status
BitField Bits: [7]
--------------------------------------*/
#define cAf6_map_sink_chnint_sta_SkGIDMIntrCurSta_Mask                                                   cBit7
#define cAf6_map_sink_chnint_sta_SkGIDMIntrCurSta_Shift                                                      7

/*--------------------------------------
BitField Name: SkCRCErrIntrCurSta
BitField Type: RW
BitField Desc: Sink CRC Error interrupt current status
BitField Bits: [6]
--------------------------------------*/
#define cAf6_map_sink_chnint_sta_SkCRCErrIntrCurSta_Mask                                                 cBit6
#define cAf6_map_sink_chnint_sta_SkCRCErrIntrCurSta_Shift                                                    6

/*--------------------------------------
BitField Name: SkSQNCIntrCurSta
BitField Type: RW
BitField Desc: Sink SQNC interrupt current status
BitField Bits: [5]
--------------------------------------*/
#define cAf6_map_sink_chnint_sta_SkSQNCIntrCurSta_Mask                                                   cBit5
#define cAf6_map_sink_chnint_sta_SkSQNCIntrCurSta_Shift                                                      5

/*--------------------------------------
BitField Name: SkMNDIntrCurSta
BitField Type: RW
BitField Desc: Sink MND interrupt current status
BitField Bits: [4]
--------------------------------------*/
#define cAf6_map_sink_chnint_sta_SkMNDIntrCurSta_Mask                                                    cBit4
#define cAf6_map_sink_chnint_sta_SkMNDIntrCurSta_Shift                                                       4

/*--------------------------------------
BitField Name: SkLOAIntrCurSta
BitField Type: RW
BitField Desc: Sink LOA interrupt current status
BitField Bits: [3]
--------------------------------------*/
#define cAf6_map_sink_chnint_sta_SkLOAIntrCurSta_Mask                                                    cBit3
#define cAf6_map_sink_chnint_sta_SkLOAIntrCurSta_Shift                                                       3

/*--------------------------------------
BitField Name: SkSQMIntrCurSta
BitField Type: RW
BitField Desc: Sink Sequence Mismatch interrupt current status
BitField Bits: [2]
--------------------------------------*/
#define cAf6_map_sink_chnint_sta_SkSQMIntrCurSta_Mask                                                    cBit2
#define cAf6_map_sink_chnint_sta_SkSQMIntrCurSta_Shift                                                       2

/*--------------------------------------
BitField Name: SkOOMIntrCurSta
BitField Type: RW
BitField Desc: Sink OOM interrupt current status
BitField Bits: [1]
--------------------------------------*/
#define cAf6_map_sink_chnint_sta_SkOOMIntrCurSta_Mask                                                    cBit1
#define cAf6_map_sink_chnint_sta_SkOOMIntrCurSta_Shift                                                       1

/*--------------------------------------
BitField Name: SkLOMIntrCurSta
BitField Type: RW
BitField Desc: Sink LOM interrupt current status
BitField Bits: [0]
--------------------------------------*/
#define cAf6_map_sink_chnint_sta_SkLOMIntrCurSta_Mask                                                    cBit0
#define cAf6_map_sink_chnint_sta_SkLOMIntrCurSta_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : MAP Sink Channel Interrupt OR Status
Reg Addr   : 0x57800
Reg Formula: 0x57800 + $eng_sts_id
    Where  : 
           + $eng_sts_id(0-47)
Reg Desc   : 
This register consists of 28 bits for 28 VT/TUs or 28 DS1/E1s of the related STS/VC. Each bit is used to store Interrupt OR status of the related VT/TU/DS1/E1.

------------------------------------------------------------------------------*/
#define cAf6Reg_map_sink_chnint_or_sta_Base                                                            0x57800
#define cAf6Reg_map_sink_chnint_or_sta(engstsid)                                          (0x57800+(engstsid))

/*--------------------------------------
BitField Name: SkHOIntrOrSta
BitField Type: RO
BitField Desc: Sink HO mode Channel Interrupt OR Status
BitField Bits: [31]
--------------------------------------*/
#define cAf6_map_sink_chnint_or_sta_SkHOIntrOrSta_Mask                                                  cBit31
#define cAf6_map_sink_chnint_or_sta_SkHOIntrOrSta_Shift                                                     31

/*--------------------------------------
BitField Name: SkLOIntrOrSta
BitField Type: RO
BitField Desc: Sink LO mode Channel Interrupt OR Status Set to 1 if any
interrupt status bit of corresponding to VT/TU/DS1/E1 is set and its interrupt
is enabled
BitField Bits: [27:0]
--------------------------------------*/
#define cAf6_map_sink_chnint_or_sta_SkLOIntrOrSta_Mask                                                cBit27_0
#define cAf6_map_sink_chnint_or_sta_SkLOIntrOrSta_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : MAP Sink STS/VC Interrupt Enable Control
Reg Addr   : 0x57FFC
Reg Formula: 0x57FFC + $grp_id
    Where  : 
           + $grp_id(0-1)
Reg Desc   : 
This register consists of 32 interrupt enable bits for 32 STS/VCs.

------------------------------------------------------------------------------*/
#define cAf6Reg_map_sink_stsvcint_enb_ctrl_Base                                                        0x57FFC
#define cAf6Reg_map_sink_stsvcint_enb_ctrl(grpid)                                            (0x57FFC+(grpid))

/*--------------------------------------
BitField Name: SkStsIntrEn
BitField Type: RO
BitField Desc: Sink STS/VC Interrupt Enable Control Set to 1 to enable the
related STS/VC to generate interrupt
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_map_sink_stsvcint_enb_ctrl_SkStsIntrEn_Mask                                              cBit31_0
#define cAf6_map_sink_stsvcint_enb_ctrl_SkStsIntrEn_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : MAP Sink STS/VC Interrupt OR Status
Reg Addr   : 0x57FFE
Reg Formula: 0x57FFE + $grp_id
    Where  : 
           + $grp_id(0-1)
Reg Desc   : 
This register consists of 32 bits for 32 STS/VCs. Each bit is used to store Interrupt OR status of the related STS/VC.

------------------------------------------------------------------------------*/
#define cAf6Reg_map_sink_stsvcint_or_sta_Base                                                          0x57FFE
#define cAf6Reg_map_sink_stsvcint_or_sta(grpid)                                              (0x57FFE+(grpid))

/*--------------------------------------
BitField Name: SkStsIntrOrSta
BitField Type: RO
BitField Desc: Sink STS/VC Interrupt OR Status Set to 1 if any interrupt status
bit of corresponding STS/VC is set and its interrupt is enabled
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_map_sink_stsvcint_or_sta_SkStsIntrOrSta_Mask                                             cBit31_0
#define cAf6_map_sink_stsvcint_or_sta_SkStsIntrOrSta_Shift                                                   0

#ifdef __cplusplus
}
#endif
#endif /* _THA60290081MODULELOVCATREG_H_ */


/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      : CONCATE
 *                                                                              
 * File        : Tha60290081ModuleHoVcatReg.h
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constant definitions of CONCATE block.
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _THA60290081MODULEHOVCATREG_H_
#define _THA60290081MODULEHOVCATREG_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Hi-Order VCAT Block Version
Reg Addr   : 0x00000
Reg Formula: 
    Where  : 
Reg Desc   : 
Hi-Order VCAT Block Version

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_version_pen                                                                         0x00000

/*--------------------------------------
BitField Name: year
BitField Type: R_O
BitField Desc: year
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_ho_version_pen_year_Mask                                                                cBit15_12
#define cAf6_ho_version_pen_year_Shift                                                                      12

/*--------------------------------------
BitField Name: week
BitField Type: R_O
BitField Desc: week number
BitField Bits: [11:04]
--------------------------------------*/
#define cAf6_ho_version_pen_week_Mask                                                                 cBit11_4
#define cAf6_ho_version_pen_week_Shift                                                                       4

/*--------------------------------------
BitField Name: number
BitField Type: R_O
BitField Desc: number in week
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_ho_version_pen_number_Mask                                                                cBit3_0
#define cAf6_ho_version_pen_number_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Master Configuration
Reg Addr   : 0x00001
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_ho_master_pen                                                                          0x00001

/*--------------------------------------
BitField Name: Vcat_Loop
BitField Type: R/W
BitField Desc: Set 1 to Ho-Vcat internal source-sink loopback
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_ho_master_pen_Vcat_Loop_Mask                                                                cBit4
#define cAf6_ho_master_pen_Vcat_Loop_Shift                                                                   4

/*--------------------------------------
BitField Name: FlushFiFo1
BitField Type: R/W
BitField Desc: Set 1 to flush FiFo @ddr clock
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_ho_master_pen_FlushFiFo1_Mask                                                               cBit3
#define cAf6_ho_master_pen_FlushFiFo1_Shift                                                                  3

/*--------------------------------------
BitField Name: FlushFiFo0
BitField Type: R/W
BitField Desc: Set 1 to flush FiFo @155.52M
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_ho_master_pen_FlushFiFo0_Mask                                                               cBit2
#define cAf6_ho_master_pen_FlushFiFo0_Shift                                                                  2

/*--------------------------------------
BitField Name: IntMemMode
BitField Type: R/W
BitField Desc: Internal Memory Delay Compensation (only for nVCAT mode)
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_ho_master_pen_IntMemMode_Mask                                                               cBit1
#define cAf6_ho_master_pen_IntMemMode_Shift                                                                  1

/*--------------------------------------
BitField Name: Active
BitField Type: R/W
BitField Desc: Block Active
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_ho_master_pen_Active_Mask                                                                   cBit0
#define cAf6_ho_master_pen_Active_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Data Write Hold Register 0
Reg Addr   : 0x00020
Reg Formula: 
    Where  : 
Reg Desc   : 
Used to write configuration or status that is greater than 32-bit wide

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_wrhold0_pen                                                                         0x00020

/*--------------------------------------
BitField Name: WrHold0
BitField Type: R/W
BitField Desc:
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_ho_wrhold0_pen_WrHold0_Mask                                                              cBit31_0
#define cAf6_ho_wrhold0_pen_WrHold0_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Data Write Hold Register 1
Reg Addr   : 0x00021
Reg Formula: 
    Where  : 
Reg Desc   : 
Used to write configuration or status that is greater than 64-bit wide

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_wrhold1_pen                                                                         0x00021

/*--------------------------------------
BitField Name: WrHold1
BitField Type: R/W
BitField Desc:
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_ho_wrhold1_pen_WrHold1_Mask                                                              cBit31_0
#define cAf6_ho_wrhold1_pen_WrHold1_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Data Write Hold Register 2
Reg Addr   : 0x00022
Reg Formula: 
    Where  : 
Reg Desc   : 
Used to write configuration or status that is greater than 96-bit wide

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_wrhold2_pen                                                                         0x00022

/*--------------------------------------
BitField Name: WrHold2
BitField Type: R/W
BitField Desc:
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_ho_wrhold2_pen_WrHold2_Mask                                                              cBit31_0
#define cAf6_ho_wrhold2_pen_WrHold2_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Data Write Hold Register 3
Reg Addr   : 0x00023
Reg Formula: 
    Where  : 
Reg Desc   : 
Used to write configuration or status that is greater than 128-bit wide

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_wrhold3_pen                                                                         0x00023

/*--------------------------------------
BitField Name: WrHold3
BitField Type: R/W
BitField Desc:
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_ho_wrhold3_pen_WrHold3_Mask                                                              cBit31_0
#define cAf6_ho_wrhold3_pen_WrHold3_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Data Write Hold Register 4
Reg Addr   : 0x00024
Reg Formula: 
    Where  : 
Reg Desc   : 
Used to write configuration or status that is greater than 160-bit wide

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_wrhold4_pen                                                                         0x00024

/*--------------------------------------
BitField Name: WrHold4
BitField Type: R/W
BitField Desc:
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_ho_wrhold4_pen_WrHold4_Mask                                                              cBit31_0
#define cAf6_ho_wrhold4_pen_WrHold4_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Data Read Hold Register 0
Reg Addr   : 0x00028
Reg Formula: 
    Where  : 
Reg Desc   : 
Used to read configuration or status that is greater than 32-bit wide

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_rdhold0_pen                                                                         0x00028

/*--------------------------------------
BitField Name: RdHold0
BitField Type: R/W
BitField Desc:
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_ho_rdhold0_pen_RdHold0_Mask                                                              cBit31_0
#define cAf6_ho_rdhold0_pen_RdHold0_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Data Read Hold Register 1
Reg Addr   : 0x00029
Reg Formula: 
    Where  : 
Reg Desc   : 
Used to read configuration or status that is greater than 64-bit wide

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_rdhold1_pen                                                                         0x00029

/*--------------------------------------
BitField Name: RdHold1
BitField Type: R/W
BitField Desc:
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_ho_rdhold1_pen_RdHold1_Mask                                                              cBit31_0
#define cAf6_ho_rdhold1_pen_RdHold1_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Data Read Hold Register 2
Reg Addr   : 0x0002a
Reg Formula: 
    Where  : 
Reg Desc   : 
Used to read configuration or status that is greater than 96-bit wide

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_rdhold2_pen                                                                         0x0002a

/*--------------------------------------
BitField Name: RdHold2
BitField Type: R/W
BitField Desc:
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_ho_rdhold2_pen_RdHold2_Mask                                                              cBit31_0
#define cAf6_ho_rdhold2_pen_RdHold2_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Data Read Hold Register 3
Reg Addr   : 0x0002b
Reg Formula: 
    Where  : 
Reg Desc   : 
Used to read configuration or status that is greater than 128-bit wide

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_rdhold3_pen                                                                         0x0002b

/*--------------------------------------
BitField Name: RdHold3
BitField Type: R/W
BitField Desc:
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_ho_rdhold3_pen_RdHold3_Mask                                                              cBit31_0
#define cAf6_ho_rdhold3_pen_RdHold3_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Data Read Hold Register 4
Reg Addr   : 0x0002c
Reg Formula: 
    Where  : 
Reg Desc   : 
Used to read configuration or status that is greater than 160-bit wide

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_rdhold4_pen                                                                         0x0002c

/*--------------------------------------
BitField Name: RdHold4
BitField Type: R/W
BitField Desc:
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_ho_rdhold4_pen_RdHold4_Mask                                                              cBit31_0
#define cAf6_ho_rdhold4_pen_RdHold4_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Source VC4 Mode Configuration
Reg Addr   : 0x20003
Reg Formula: 
    Where  : 
Reg Desc   : 
Source VC4 Mode Enable Control. For VC4 mode, if STS master is n,
the two slave-STSs will be n+64 and n+128 (where n = 0 - 63)

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_so_vc4md_pen                                                                        0x20003

/*--------------------------------------
BitField Name: So_VC4_Ena
BitField Type: R/W
BitField Desc: Source VC4 Mode Enable
BitField Bits: [63:00]
--------------------------------------*/
#define cAf6_ho_so_vc4md_pen_So_VC4_Ena_Mask_01                                                       cBit31_0
#define cAf6_ho_so_vc4md_pen_So_VC4_Ena_Shift_01                                                             0
#define cAf6_ho_so_vc4md_pen_So_VC4_Ena_Mask_02                                                       cBit31_0
#define cAf6_ho_so_vc4md_pen_So_VC4_Ena_Shift_02                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Source VC4-4c Mode Configuration
Reg Addr   : 0x20004
Reg Formula: 
    Where  : 
Reg Desc   : 
Source VC4-4c Mode Enable Control. For VC4-4c mode, if STS master is n(0-15),
the 11 slave-STSs will be n+16*m (where m = 1 - 11)

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_so_vc4ncmd_pen                                                                      0x20004

/*--------------------------------------
BitField Name: So_VC44c_Ena
BitField Type: R/W
BitField Desc: Source VC4-4c Mode Enable
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_ho_so_vc4ncmd_pen_So_VC44c_Ena_Mask                                                      cBit15_0
#define cAf6_ho_so_vc4ncmd_pen_So_VC44c_Ena_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Source OC192c Mode Configuration
Reg Addr   : 0x20005
Reg Formula: 
    Where  : 
Reg Desc   : 
Source OC-192c Mode Enable Control

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_so_192cmd_pen                                                                       0x20005

/*--------------------------------------
BitField Name: So_OC192c_Ena
BitField Type: R/W
BitField Desc: Source OC-192c Mode Enable
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_ho_so_192cmd_pen_So_OC192c_Ena_Mask                                                         cBit0
#define cAf6_ho_so_192cmd_pen_So_OC192c_Ena_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Source Member Control
Reg Addr   : 0x20800-208BF
Reg Formula: 0x20800 + $sts_id
    Where  : 
           + $sts_id(0-191)
Reg Desc   : 
Source Member Control

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_so_memctrl_pen_Base                                                                 0x20800
#define cAf6Reg_ho_so_memctrl_pen(stsid)                                                     (0x20800+(stsid))

/*--------------------------------------
BitField Name: So_Mem_Ena
BitField Type: R/W
BitField Desc: Member Enable
BitField Bits: [25:25]
--------------------------------------*/
#define cAf6_ho_so_memctrl_pen_So_Mem_Ena_Mask                                                          cBit25
#define cAf6_ho_so_memctrl_pen_So_Mem_Ena_Shift                                                             25

/*--------------------------------------
BitField Name: So_Ccat_Mst
BitField Type: R/W
BitField Desc: Ccat Master ID
BitField Bits: [24:17]
--------------------------------------*/
#define cAf6_ho_so_memctrl_pen_So_Ccat_Mst_Mask                                                      cBit24_17
#define cAf6_ho_so_memctrl_pen_So_Ccat_Mst_Shift                                                            17

/*--------------------------------------
BitField Name: So_Ccat_Md
BitField Type: R/W
BitField Desc: Ccat Enable
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_ho_so_memctrl_pen_So_Ccat_Md_Mask                                                          cBit16
#define cAf6_ho_so_memctrl_pen_So_Ccat_Md_Shift                                                             16

/*--------------------------------------
BitField Name: So_G8040_Md
BitField Type: R/W
BitField Desc: G.8040/G.804 Enable
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_ho_so_memctrl_pen_So_G8040_Md_Mask                                                         cBit15
#define cAf6_ho_so_memctrl_pen_So_G8040_Md_Shift                                                            15

/*--------------------------------------
BitField Name: So_Vcat_Md
BitField Type: R/W
BitField Desc: VCAT Enable
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_ho_so_memctrl_pen_So_Vcat_Md_Mask                                                          cBit14
#define cAf6_ho_so_memctrl_pen_So_Vcat_Md_Shift                                                             14

/*--------------------------------------
BitField Name: So_Lcas_Md
BitField Type: R/W
BitField Desc: LCAS Enable
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_ho_so_memctrl_pen_So_Lcas_Md_Mask                                                          cBit13
#define cAf6_ho_so_memctrl_pen_So_Lcas_Md_Shift                                                             13

/*--------------------------------------
BitField Name: So_Nms_Cmd
BitField Type: R/W
BitField Desc: Network Management Command
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_ho_so_memctrl_pen_So_Nms_Cmd_Mask                                                          cBit12
#define cAf6_ho_so_memctrl_pen_So_Nms_Cmd_Shift                                                             12

/*--------------------------------------
BitField Name: So_Vcgid
BitField Type: R/W
BitField Desc: VCG ID: 0-127
BitField Bits: [06:00]
--------------------------------------*/
#define cAf6_ho_so_memctrl_pen_So_Vcgid_Mask                                                           cBit6_0
#define cAf6_ho_so_memctrl_pen_So_Vcgid_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : Source Tx Sequence Configuration
Reg Addr   : 0x25000-250BF
Reg Formula: 0x25000 + $sts_id
    Where  : 
           + $sts_id(0-191)
Reg Desc   : 
Source Sequence Assignment in case none-LCAS/CCAT mode

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_so_txseq_pen_Base                                                                   0x25000
#define cAf6Reg_ho_so_txseq_pen(stsid)                                                       (0x25000+(stsid))

/*--------------------------------------
BitField Name: So_TxSeq_Ena
BitField Type: R/W
BitField Desc: Sequence Assignment Enable
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_ho_so_txseq_pen_So_TxSeq_Ena_Mask                                                           cBit8
#define cAf6_ho_so_txseq_pen_So_TxSeq_Ena_Shift                                                              8

/*--------------------------------------
BitField Name: So_TxSeq
BitField Type: R/W
BitField Desc: Sequence Assignment Value
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_ho_so_txseq_pen_So_TxSeq_Mask                                                             cBit7_0
#define cAf6_ho_so_txseq_pen_So_TxSeq_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Source Member Status
Reg Addr   : 0x21000-210BF
Reg Formula: 0x21000 + $sts_id
    Where  : 
           + $sts_id(0-191)
Reg Desc   : 
Source Sequence Assignment none-LCAS mode

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_so_memsta_pen_Base                                                                  0x21000
#define cAf6Reg_ho_so_memsta_pen(stsid)                                                      (0x21000+(stsid))

/*--------------------------------------
BitField Name: So_Idle_Cnt
BitField Type: R/W
BitField Desc: Member Idle Counter
BitField Bits: [27:24]
--------------------------------------*/
#define cAf6_ho_so_memsta_pen_So_Idle_Cnt_Mask                                                       cBit27_24
#define cAf6_ho_so_memsta_pen_So_Idle_Cnt_Shift                                                             24

/*--------------------------------------
BitField Name: So_Rem_Sta
BitField Type: R/W
BitField Desc: Remove Status 0: remove done 1: remove in progress
BitField Bits: [23:23]
--------------------------------------*/
#define cAf6_ho_so_memsta_pen_So_Rem_Sta_Mask                                                           cBit23
#define cAf6_ho_so_memsta_pen_So_Rem_Sta_Shift                                                              23

/*--------------------------------------
BitField Name: So_Mem_Acpseq
BitField Type: R/W
BitField Desc: Member Accept Sequence
BitField Bits: [22:15]
--------------------------------------*/
#define cAf6_ho_so_memsta_pen_So_Mem_Acpseq_Mask                                                     cBit22_15
#define cAf6_ho_so_memsta_pen_So_Mem_Acpseq_Shift                                                           15

/*--------------------------------------
BitField Name: So_Mem_Seq
BitField Type: R/W
BitField Desc: Member Sequence
BitField Bits: [14:07]
--------------------------------------*/
#define cAf6_ho_so_memsta_pen_So_Mem_Seq_Mask                                                         cBit14_7
#define cAf6_ho_so_memsta_pen_So_Mem_Seq_Shift                                                               7

/*--------------------------------------
BitField Name: So_Ctrl_Word
BitField Type: R/W
BitField Desc: Member Control Word
BitField Bits: [06:03]
--------------------------------------*/
#define cAf6_ho_so_memsta_pen_So_Ctrl_Word_Mask                                                        cBit6_3
#define cAf6_ho_so_memsta_pen_So_Ctrl_Word_Shift                                                             3

/*--------------------------------------
BitField Name: So_FSM_Sta
BitField Type: R/W
BitField Desc: Member Source State
BitField Bits: [02:00]
--------------------------------------*/
#define cAf6_ho_so_memsta_pen_So_FSM_Sta_Mask                                                          cBit2_0
#define cAf6_ho_so_memsta_pen_So_FSM_Sta_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Source VCG Status
Reg Addr   : 0x21800-2187F
Reg Formula: 0x21800 + $vcg_id
    Where  : 
           + $vcg_id(0-127)
Reg Desc   : 
Source VCG Status

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_so_vcgsta_pen_Base                                                                  0x21800
#define cAf6Reg_ho_so_vcgsta_pen(vcgid)                                                      (0x21800+(vcgid))

/*--------------------------------------
BitField Name: So_VCG_Sta
BitField Type: WO
BitField Desc: Source VCG Status
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_ho_so_vcgsta_pen_So_VCG_Sta_Mask                                                         cBit31_0
#define cAf6_ho_so_vcgsta_pen_So_VCG_Sta_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Source Output OC-48 STS Connection Memory
Reg Addr   : 0x24000-240BF
Reg Formula: 0x24000 + $sts_id
    Where  : 
           + $sts_id(0-191)
Reg Desc   : 
Connection Memory for Output 4-Slice OC-48

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_so_stsxc_pen_Base                                                                   0x24000
#define cAf6Reg_ho_so_stsxc_pen(stsid)                                                       (0x24000+(stsid))

/*--------------------------------------
BitField Name: OC48_OutSlice
BitField Type: R/W
BitField Desc: Output Slice 0-3
BitField Bits: [09:08]
--------------------------------------*/
#define cAf6_ho_so_stsxc_pen_OC48_OutSlice_Mask                                                        cBit9_8
#define cAf6_ho_so_stsxc_pen_OC48_OutSlice_Shift                                                             8

/*--------------------------------------
BitField Name: OC48_OutSTS
BitField Type: R/W
BitField Desc: Output STS 0-47
BitField Bits: [05:00]
--------------------------------------*/
#define cAf6_ho_so_stsxc_pen_OC48_OutSTS_Mask                                                          cBit5_0
#define cAf6_ho_so_stsxc_pen_OC48_OutSTS_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Source Input OC-48 STS Connection Memory
Reg Addr   : 0x40200-0x402BF
Reg Formula: 0x40200 + $sts_id*4 + $slice_id
    Where  : 
           + $slice_id(0-3)
           + $sts_id(0-47)
Reg Desc   : 
Connection Memory for Input 4-Slice OC-48

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_sk_stsxc_pen_Base                                                                   0x40200
#define cAf6Reg_ho_sk_stsxc_pen(sliceid, stsid)                                  (0x40200+(stsid)*4+(sliceid))

/*--------------------------------------
BitField Name: OC192_STSEna
BitField Type: R/W
BitField Desc: Output STS(0-191) Enable
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_ho_sk_stsxc_pen_OC192_STSEna_Mask                                                           cBit8
#define cAf6_ho_sk_stsxc_pen_OC192_STSEna_Shift                                                              8

/*--------------------------------------
BitField Name: OC192_OutSTS
BitField Type: R/W
BitField Desc: Output STS(0-191)
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_ho_sk_stsxc_pen_OC192_OutSTS_Mask                                                         cBit7_0
#define cAf6_ho_sk_stsxc_pen_OC192_OutSTS_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : Sink VC4 Mode Configuration
Reg Addr   : 0x40023
Reg Formula: 
    Where  : 
Reg Desc   : 
Sink VC4 Mode Enable Control. For VC4 mode, if STS master is n,
the two slave-STSs will be n+64 and n+128 (where n = 0 - 63)

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_sk_vc4md_pen                                                                        0x40023

/*--------------------------------------
BitField Name: Sk_VC4_Ena
BitField Type: R/W
BitField Desc: Sink VC4 Mode Enable
BitField Bits: [63:00]
--------------------------------------*/
#define cAf6_ho_sk_vc4md_pen_Sk_VC4_Ena_Mask_01                                                       cBit31_0
#define cAf6_ho_sk_vc4md_pen_Sk_VC4_Ena_Shift_01                                                             0
#define cAf6_ho_sk_vc4md_pen_Sk_VC4_Ena_Mask_02                                                       cBit31_0
#define cAf6_ho_sk_vc4md_pen_Sk_VC4_Ena_Shift_02                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Sink VC4-4c Mode Configuration
Reg Addr   : 0x40024
Reg Formula: 
    Where  : 
Reg Desc   : 
Sink VC4-4c Mode Enable Control. For VC4-4c mode, if STS master is n(0-15),
the 11 slave-STSs will be n+16*m (where m = 1 - 11)

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_sk_vc4ncmd_pen                                                                      0x40024

/*--------------------------------------
BitField Name: Sk_VC44c_Ena
BitField Type: R/W
BitField Desc: Sink VC4-4c Mode Enable
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_ho_sk_vc4ncmd_pen_Sk_VC44c_Ena_Mask                                                      cBit15_0
#define cAf6_ho_sk_vc4ncmd_pen_Sk_VC44c_Ena_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Sink OC192c Mode Configuration
Reg Addr   : 0x40025
Reg Formula: 
    Where  : 
Reg Desc   : 
Sink OC-192c Mode Enable Control

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_sk_192cmd_pen                                                                       0x40025

/*--------------------------------------
BitField Name: Sk_OC192c_Ena
BitField Type: R/W
BitField Desc: Sink OC-192c Mode Enable
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_ho_sk_192cmd_pen_Sk_OC192c_Ena_Mask                                                         cBit0
#define cAf6_ho_sk_192cmd_pen_Sk_OC192c_Ena_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Sink Member Control
Reg Addr   : 0x48800-0x488BF
Reg Formula: 0x48800 + $sts_id
    Where  : 
           + $sts_id(0-191)
Reg Desc   : 
Sink Member Control

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_sk_memctrl_pen_Base                                                                 0x48800
#define cAf6Reg_ho_sk_memctrl_pen(stsid)                                                     (0x48800+(stsid))

/*--------------------------------------
BitField Name: Sk_Mem_Ena
BitField Type: R/W
BitField Desc: Member Enable
BitField Bits: [25:25]
--------------------------------------*/
#define cAf6_ho_sk_memctrl_pen_Sk_Mem_Ena_Mask                                                          cBit25
#define cAf6_ho_sk_memctrl_pen_Sk_Mem_Ena_Shift                                                             25

/*--------------------------------------
BitField Name: Sk_Ccat_Mst
BitField Type: R/W
BitField Desc: Ccat Master ID
BitField Bits: [24:17]
--------------------------------------*/
#define cAf6_ho_sk_memctrl_pen_Sk_Ccat_Mst_Mask                                                      cBit24_17
#define cAf6_ho_sk_memctrl_pen_Sk_Ccat_Mst_Shift                                                            17

/*--------------------------------------
BitField Name: Sk_Ccat_Md
BitField Type: R/W
BitField Desc: Ccat Enable
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_ho_sk_memctrl_pen_Sk_Ccat_Md_Mask                                                          cBit16
#define cAf6_ho_sk_memctrl_pen_Sk_Ccat_Md_Shift                                                             16

/*--------------------------------------
BitField Name: Sk_G8040_Md
BitField Type: R/W
BitField Desc: G.8040/G.804 Enable
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_ho_sk_memctrl_pen_Sk_G8040_Md_Mask                                                         cBit15
#define cAf6_ho_sk_memctrl_pen_Sk_G8040_Md_Shift                                                            15

/*--------------------------------------
BitField Name: Sk_Vcat_Md
BitField Type: R/W
BitField Desc: VCAT Enable
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_ho_sk_memctrl_pen_Sk_Vcat_Md_Mask                                                          cBit14
#define cAf6_ho_sk_memctrl_pen_Sk_Vcat_Md_Shift                                                             14

/*--------------------------------------
BitField Name: Sk_Lcas_Md
BitField Type: R/W
BitField Desc: LCAS Enable
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_ho_sk_memctrl_pen_Sk_Lcas_Md_Mask                                                          cBit13
#define cAf6_ho_sk_memctrl_pen_Sk_Lcas_Md_Shift                                                             13

/*--------------------------------------
BitField Name: Sk_Nms_Cmd
BitField Type: R/W
BitField Desc: Network Management Command
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_ho_sk_memctrl_pen_Sk_Nms_Cmd_Mask                                                          cBit12
#define cAf6_ho_sk_memctrl_pen_Sk_Nms_Cmd_Shift                                                             12

/*--------------------------------------
BitField Name: Sk_Vcgid
BitField Type: R/W
BitField Desc: VCG ID: 0-127
BitField Bits: [06:00]
--------------------------------------*/
#define cAf6_ho_sk_memctrl_pen_Sk_Vcgid_Mask                                                           cBit6_0
#define cAf6_ho_sk_memctrl_pen_Sk_Vcgid_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : Sink Member Expected Sequence
Reg Addr   : 0x50800-0x508BF
Reg Formula: 0x50800 + $sts_id
    Where  : 
           + $sts_id(0-191)
Reg Desc   : 
Sink Member Expected Sequence

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_sk_expseq_pen_Base                                                                  0x50800
#define cAf6Reg_ho_sk_expseq_pen(stsid)                                                      (0x50800+(stsid))

/*--------------------------------------
BitField Name: Sk_ExpSeq
BitField Type: R/W
BitField Desc: Expected Sequence
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_ho_sk_expseq_pen_Sk_ExpSeq_Mask                                                           cBit7_0
#define cAf6_ho_sk_expseq_pen_Sk_ExpSeq_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : Sink Member Status
Reg Addr   : 0x51800-0x518BF
Reg Formula: 0x51800 + $sts_id
    Where  : 
           + $sts_id(0-191)
Reg Desc   : 
Sink Member Status

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_sk_memsta_pen_Base                                                                  0x51800
#define cAf6Reg_ho_sk_memsta_pen(stsid)                                                      (0x51800+(stsid))

/*--------------------------------------
BitField Name: Sk_Dat_Ena
BitField Type: R/W
BitField Desc: Receive Data processing Enable
BitField Bits: [22:22]
--------------------------------------*/
#define cAf6_ho_sk_memsta_pen_Sk_Dat_Ena_Mask                                                           cBit22
#define cAf6_ho_sk_memsta_pen_Sk_Dat_Ena_Shift                                                              22

/*--------------------------------------
BitField Name: Sk_Mem_AcpSeq
BitField Type: R/W
BitField Desc: Receive Accept Sequence (0-191)
BitField Bits: [21:14]
--------------------------------------*/
#define cAf6_ho_sk_memsta_pen_Sk_Mem_AcpSeq_Mask                                                     cBit21_14
#define cAf6_ho_sk_memsta_pen_Sk_Mem_AcpSeq_Shift                                                           14

/*--------------------------------------
BitField Name: Sk_Mem_Seq
BitField Type: R/W
BitField Desc: Receive Sequence (0-191)
BitField Bits: [13:06]
--------------------------------------*/
#define cAf6_ho_sk_memsta_pen_Sk_Mem_Seq_Mask                                                         cBit13_6
#define cAf6_ho_sk_memsta_pen_Sk_Mem_Seq_Shift                                                               6

/*--------------------------------------
BitField Name: Sk_Ctrl_Word
BitField Type: R/W
BitField Desc: Receive Control Word
BitField Bits: [05:02]
--------------------------------------*/
#define cAf6_ho_sk_memsta_pen_Sk_Ctrl_Word_Mask                                                        cBit5_2
#define cAf6_ho_sk_memsta_pen_Sk_Ctrl_Word_Shift                                                             2

/*--------------------------------------
BitField Name: Sk_FSM_Sta
BitField Type: R/W
BitField Desc: Member Sink State
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_ho_sk_memsta_pen_Sk_FSM_Sta_Mask                                                          cBit1_0
#define cAf6_ho_sk_memsta_pen_Sk_FSM_Sta_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Sink VCG Status
Reg Addr   : 0x51000-5107F
Reg Formula: 0x51000 + $vcg_id
    Where  : 
           + $vcg_id(0-127)
Reg Desc   : 
Sink VCG Status

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_sk_vcgsta_pen_Base                                                                  0x51000
#define cAf6Reg_ho_sk_vcgsta_pen(vcgid)                                                      (0x51000+(vcgid))

/*--------------------------------------
BitField Name: Sk_VCG_Sta
BitField Type: WO
BitField Desc: Sink VCG Status
BitField Bits: [41:00]
--------------------------------------*/
#define cAf6_ho_sk_vcgsta_pen_Sk_VCG_Sta_Mask_01                                                      cBit31_0
#define cAf6_ho_sk_vcgsta_pen_Sk_VCG_Sta_Shift_01                                                            0
#define cAf6_ho_sk_vcgsta_pen_Sk_VCG_Sta_Mask_02                                                       cBit9_0
#define cAf6_ho_sk_vcgsta_pen_Sk_VCG_Sta_Shift_02                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Member Status (Tx-MST)
Reg Addr   : 0x53000-0x53FFF
Reg Formula: 0x53000 + $vcg_id*32 + $mem_grp
    Where  : 
           + $vcg_id(0-127)
           + $mem_grp(0-31)
Reg Desc   : 
Transmit Member Status.Total 256 bit status is devided into 32 groups, each group has 8-member.

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_sk_txmst_pen_Base                                                                   0x53000
#define cAf6Reg_ho_sk_txmst_pen(vcgid, memgrp)                                   (0x53000+(vcgid)*32+(memgrp))

/*--------------------------------------
BitField Name: Sk_TxMST
BitField Type: R/W
BitField Desc: Transmit Member Status 0: OK 1: FAIL
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_ho_sk_txmst_pen_Sk_TxMST_Mask                                                             cBit7_0
#define cAf6_ho_sk_txmst_pen_Sk_TxMST_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Receive Member Status (Rx-MST)
Reg Addr   : 0x52000-0x523FF
Reg Formula: 0x52000 + $vcg_id*8 + $mem_grp
    Where  : 
           + $vcg_id(0-127)
           + $mem_grp(0-7)
Reg Desc   : 
Receive Member Status. Total 256 bit status is devided into 8 groups, each group has 32-member.

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_sk_rxmst_pen_Base                                                                   0x52000
#define cAf6Reg_ho_sk_rxmst_pen(vcgid, memgrp)                                    (0x52000+(vcgid)*8+(memgrp))

/*--------------------------------------
BitField Name: Sk_RxMST
BitField Type: R/W
BitField Desc: Transmit Member Status
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_ho_sk_rxmst_pen_Sk_RxMST_Mask                                                            cBit31_0
#define cAf6_ho_sk_rxmst_pen_Sk_RxMST_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Re-Sequence Acknowledgement Status (Rx-ACK)
Reg Addr   : 0x55200-0x5527F
Reg Formula: 0x55200 + $vcg_id
    Where  : 
           + $vcg_id(0-127)
Reg Desc   : 
Re-Sequence Acknowledgement

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_sk_rsack_pen_Base                                                                   0x55200
#define cAf6Reg_ho_sk_rsack_pen(vcgid)                                                       (0x55200+(vcgid))

/*--------------------------------------
BitField Name: Sk_Rx_RSACK
BitField Type: R/W
BitField Desc: Receive RS_ACK Status
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_ho_sk_rsack_pen_Sk_Rx_RSACK_Mask                                                            cBit1
#define cAf6_ho_sk_rsack_pen_Sk_Rx_RSACK_Shift                                                               1

/*--------------------------------------
BitField Name: Sk_Tx_RSACK
BitField Type: R/W
BitField Desc: Transmit RS_ACK Status
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_ho_sk_rsack_pen_Sk_Tx_RSACK_Mask                                                            cBit0
#define cAf6_ho_sk_rsack_pen_Sk_Tx_RSACK_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Sink Threshold Configuration
Reg Addr   : 0x55400-0x5547F
Reg Formula: 0x55400 + $vcg_id
    Where  : 
           + $vcg_id(0-127)
Reg Desc   : 
Configure threshold for: remove timer, hold-off timer, WTR timer for per VCG.
{RMV} : value 0-255,  step = 2ms   => max value = 2 miliseconds
{WTR} : value 0-8063, step = 100ms => max value = 25.6 seconds
{HO}  : value 0-255,  step = 100ms => max value = 13.44 minutes

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_sk_thr_pen_Base                                                                     0x55400
#define cAf6Reg_ho_sk_thr_pen(vcgid)                                                         (0x55400+(vcgid))

/*--------------------------------------
BitField Name: Sk_RMV_Dis
BitField Type: R/W
BitField Desc: Set 1 to disable timer
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_ho_sk_thr_pen_Sk_RMV_Dis_Mask                                                              cBit31
#define cAf6_ho_sk_thr_pen_Sk_RMV_Dis_Shift                                                                 31

/*--------------------------------------
BitField Name: Sk_WTR_Dis
BitField Type: R/W
BitField Desc: Set 1 to disable timer
BitField Bits: [30:30]
--------------------------------------*/
#define cAf6_ho_sk_thr_pen_Sk_WTR_Dis_Mask                                                              cBit30
#define cAf6_ho_sk_thr_pen_Sk_WTR_Dis_Shift                                                                 30

/*--------------------------------------
BitField Name: Sk_Ho_Dis
BitField Type: R/W
BitField Desc: Set 1 to disable tmier
BitField Bits: [29:29]
--------------------------------------*/
#define cAf6_ho_sk_thr_pen_Sk_Ho_Dis_Mask                                                               cBit29
#define cAf6_ho_sk_thr_pen_Sk_Ho_Dis_Shift                                                                  29

/*--------------------------------------
BitField Name: Sk_RMV_Thr
BitField Type: R/W
BitField Desc: RMV Threshold
BitField Bits: [28:21]
--------------------------------------*/
#define cAf6_ho_sk_thr_pen_Sk_RMV_Thr_Mask                                                           cBit28_21
#define cAf6_ho_sk_thr_pen_Sk_RMV_Thr_Shift                                                                 21

/*--------------------------------------
BitField Name: Sk_WTR_Thr
BitField Type: R/W
BitField Desc: WTR Threshold
BitField Bits: [20:08]
--------------------------------------*/
#define cAf6_ho_sk_thr_pen_Sk_WTR_Thr_Mask                                                            cBit20_8
#define cAf6_ho_sk_thr_pen_Sk_WTR_Thr_Shift                                                                  8

/*--------------------------------------
BitField Name: Sk_Ho_Thr
BitField Type: R/W
BitField Desc: Hold-Off Threshold
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_ho_sk_thr_pen_Sk_Ho_Thr_Mask                                                              cBit7_0
#define cAf6_ho_sk_thr_pen_Sk_Ho_Thr_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Sink Channel Interrupt Enable Control
Reg Addr   : 0x56000-0x560BF
Reg Formula: 0x56000 + $sts_id
    Where  : 
           + $sts_id(0-191)
Reg Desc   : 
This register is used to enable Interrupt, write 1 to enable interrupt for event.

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_sk_intrctrl_pen_Base                                                                0x56000
#define cAf6Reg_ho_sk_intrctrl_pen(stsid)                                                    (0x56000+(stsid))

/*--------------------------------------
BitField Name: Sk_MFSM_Intr_En
BitField Type: R/W
BitField Desc: Interrupt Enable for Member State Change event
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_ho_sk_intrctrl_pen_Sk_MFSM_Intr_En_Mask                                                     cBit8
#define cAf6_ho_sk_intrctrl_pen_Sk_MFSM_Intr_En_Shift                                                        8

/*--------------------------------------
BitField Name: GIDM_Intr_En
BitField Type: R/W
BitField Desc: Interrupt Enable for GID mismatch event
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_ho_sk_intrctrl_pen_GIDM_Intr_En_Mask                                                        cBit7
#define cAf6_ho_sk_intrctrl_pen_GIDM_Intr_En_Shift                                                           7

/*--------------------------------------
BitField Name: CRCErr_Intr_En
BitField Type: R/W
BitField Desc: Interrupt Enable for CRC Error event
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_ho_sk_intrctrl_pen_CRCErr_Intr_En_Mask                                                      cBit6
#define cAf6_ho_sk_intrctrl_pen_CRCErr_Intr_En_Shift                                                         6

/*--------------------------------------
BitField Name: SQNC_Intr_En
BitField Type: R/W
BitField Desc: Interrupt Enable for SQNC event
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_ho_sk_intrctrl_pen_SQNC_Intr_En_Mask                                                        cBit5
#define cAf6_ho_sk_intrctrl_pen_SQNC_Intr_En_Shift                                                           5

/*--------------------------------------
BitField Name: MND_Intr_En
BitField Type: R/W
BitField Desc: Interrupt Enable for MND event
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_ho_sk_intrctrl_pen_MND_Intr_En_Mask                                                         cBit4
#define cAf6_ho_sk_intrctrl_pen_MND_Intr_En_Shift                                                            4

/*--------------------------------------
BitField Name: LOA_Intr_En
BitField Type: R/W
BitField Desc: Interrupt Enable for LOA event
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_ho_sk_intrctrl_pen_LOA_Intr_En_Mask                                                         cBit3
#define cAf6_ho_sk_intrctrl_pen_LOA_Intr_En_Shift                                                            3

/*--------------------------------------
BitField Name: SQM_Intr_En
BitField Type: R/W
BitField Desc: Interrupt Enable for SQM event
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_ho_sk_intrctrl_pen_SQM_Intr_En_Mask                                                         cBit2
#define cAf6_ho_sk_intrctrl_pen_SQM_Intr_En_Shift                                                            2

/*--------------------------------------
BitField Name: OOM_Intr_En
BitField Type: R/W
BitField Desc: Interrupt Enable for OOM event
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_ho_sk_intrctrl_pen_OOM_Intr_En_Mask                                                         cBit1
#define cAf6_ho_sk_intrctrl_pen_OOM_Intr_En_Shift                                                            1

/*--------------------------------------
BitField Name: LOM_Intr_En
BitField Type: R/W
BitField Desc: Interrupt Enable for LOM event
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_ho_sk_intrctrl_pen_LOM_Intr_En_Mask                                                         cBit0
#define cAf6_ho_sk_intrctrl_pen_LOM_Intr_En_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Sink Channel Interrupt Status
Reg Addr   : 0x56100-0x561BF
Reg Formula: 0x56100 + $sts_id
    Where  : 
           + $sts_id(0-191)
Reg Desc   : 
This register is used to show the status of event,which generate interrupt.

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_sk_intrstk_pen_Base                                                                 0x56100
#define cAf6Reg_ho_sk_intrstk_pen(stsid)                                                     (0x56100+(stsid))

/*--------------------------------------
BitField Name: Sk_MFSM_Intr_Sta
BitField Type: R/W
BitField Desc: Member state change event interrupt status
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_ho_sk_intrstk_pen_Sk_MFSM_Intr_Sta_Mask                                                     cBit8
#define cAf6_ho_sk_intrstk_pen_Sk_MFSM_Intr_Sta_Shift                                                        8

/*--------------------------------------
BitField Name: GIDM_Intr_Sta
BitField Type: R/W
BitField Desc: GID Mismatch event state change
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_ho_sk_intrstk_pen_GIDM_Intr_Sta_Mask                                                        cBit7
#define cAf6_ho_sk_intrstk_pen_GIDM_Intr_Sta_Shift                                                           7

/*--------------------------------------
BitField Name: CRCErr_Intr_Sta
BitField Type: R/W
BitField Desc: CRC Error interrupt event
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_ho_sk_intrstk_pen_CRCErr_Intr_Sta_Mask                                                      cBit6
#define cAf6_ho_sk_intrstk_pen_CRCErr_Intr_Sta_Shift                                                         6

/*--------------------------------------
BitField Name: SQNC_Intr_Sta
BitField Type: R/W
BitField Desc: SQNC interrupt event
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_ho_sk_intrstk_pen_SQNC_Intr_Sta_Mask                                                        cBit5
#define cAf6_ho_sk_intrstk_pen_SQNC_Intr_Sta_Shift                                                           5

/*--------------------------------------
BitField Name: MND_Intr_Sta
BitField Type: R/W
BitField Desc: MND interrupt event
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_ho_sk_intrstk_pen_MND_Intr_Sta_Mask                                                         cBit4
#define cAf6_ho_sk_intrstk_pen_MND_Intr_Sta_Shift                                                            4

/*--------------------------------------
BitField Name: LOA_Intr_Sta
BitField Type: R/W
BitField Desc: LOA interrupt event
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_ho_sk_intrstk_pen_LOA_Intr_Sta_Mask                                                         cBit3
#define cAf6_ho_sk_intrstk_pen_LOA_Intr_Sta_Shift                                                            3

/*--------------------------------------
BitField Name: SQM_Intr_Sta
BitField Type: R/W
BitField Desc: SQM interrupt event
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_ho_sk_intrstk_pen_SQM_Intr_Sta_Mask                                                         cBit2
#define cAf6_ho_sk_intrstk_pen_SQM_Intr_Sta_Shift                                                            2

/*--------------------------------------
BitField Name: OOM_Intr_Sta
BitField Type: R/W
BitField Desc: OOM interrupt event
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_ho_sk_intrstk_pen_OOM_Intr_Sta_Mask                                                         cBit1
#define cAf6_ho_sk_intrstk_pen_OOM_Intr_Sta_Shift                                                            1

/*--------------------------------------
BitField Name: LOM_Intr_Sta
BitField Type: R/W
BitField Desc: LOM interrupt event
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_ho_sk_intrstk_pen_LOM_Intr_Sta_Mask                                                         cBit0
#define cAf6_ho_sk_intrstk_pen_LOM_Intr_Sta_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Sink Channel Current Status
Reg Addr   : 0x56200-0x562BF
Reg Formula: 0x56200 + $sts_id
    Where  : 
           + $sts_id(0-191)
Reg Desc   : 
This register is used to show the current status of event.

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_sk_intrsta_pen_Base                                                                 0x56200
#define cAf6Reg_ho_sk_intrsta_pen(stsid)                                                     (0x56200+(stsid))

/*--------------------------------------
BitField Name: Sk_MFSM_Intr_CrrSta
BitField Type: RO
BitField Desc: Member State change event current status
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_ho_sk_intrsta_pen_Sk_MFSM_Intr_CrrSta_Mask                                                  cBit8
#define cAf6_ho_sk_intrsta_pen_Sk_MFSM_Intr_CrrSta_Shift                                                     8

/*--------------------------------------
BitField Name: GIDM_Intr_CrrSta
BitField Type: RO
BitField Desc: GID Mismath event Current state
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_ho_sk_intrsta_pen_GIDM_Intr_CrrSta_Mask                                                     cBit7
#define cAf6_ho_sk_intrsta_pen_GIDM_Intr_CrrSta_Shift                                                        7

/*--------------------------------------
BitField Name: CRCErr_Intr_CrrSta
BitField Type: RO
BitField Desc: CRC Error event Current status
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_ho_sk_intrsta_pen_CRCErr_Intr_CrrSta_Mask                                                   cBit6
#define cAf6_ho_sk_intrsta_pen_CRCErr_Intr_CrrSta_Shift                                                      6

/*--------------------------------------
BitField Name: SQNC_Intr_CrrSta
BitField Type: RO
BitField Desc: SQNC event Current status
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_ho_sk_intrsta_pen_SQNC_Intr_CrrSta_Mask                                                     cBit5
#define cAf6_ho_sk_intrsta_pen_SQNC_Intr_CrrSta_Shift                                                        5

/*--------------------------------------
BitField Name: MND_Intr_CrrSta
BitField Type: RO
BitField Desc: MND event Current status
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_ho_sk_intrsta_pen_MND_Intr_CrrSta_Mask                                                      cBit4
#define cAf6_ho_sk_intrsta_pen_MND_Intr_CrrSta_Shift                                                         4

/*--------------------------------------
BitField Name: LOA_Intr_CrrSta
BitField Type: RO
BitField Desc: LOA event Current status
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_ho_sk_intrsta_pen_LOA_Intr_CrrSta_Mask                                                      cBit3
#define cAf6_ho_sk_intrsta_pen_LOA_Intr_CrrSta_Shift                                                         3

/*--------------------------------------
BitField Name: SQM_Intr_CrrSta
BitField Type: RO
BitField Desc: SQM event Current status
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_ho_sk_intrsta_pen_SQM_Intr_CrrSta_Mask                                                      cBit2
#define cAf6_ho_sk_intrsta_pen_SQM_Intr_CrrSta_Shift                                                         2

/*--------------------------------------
BitField Name: OOM_Intr_CrrSta
BitField Type: RO
BitField Desc: OOM event Current status
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_ho_sk_intrsta_pen_OOM_Intr_CrrSta_Mask                                                      cBit1
#define cAf6_ho_sk_intrsta_pen_OOM_Intr_CrrSta_Shift                                                         1

/*--------------------------------------
BitField Name: LOM_Intr_CrrSta
BitField Type: RO
BitField Desc: LOM event Current status
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_ho_sk_intrsta_pen_LOM_Intr_CrrSta_Mask                                                      cBit0
#define cAf6_ho_sk_intrsta_pen_LOM_Intr_CrrSta_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Sink STS/VC Group Interrupt Enable Control
Reg Addr   : 0x56FFE
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 6 bit nterrupt enable bits for 6 group,
192 STS is devived to 6 group, each group contain 32 STS.

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_sk_vcintrctrl_pen                                                                   0x56FFE

/*--------------------------------------
BitField Name: STS_Intr_En
BitField Type: R/W
BitField Desc: Interrupt Enable for 32 STS(0-31)
BitField Bits: [05:00]
--------------------------------------*/
#define cAf6_ho_sk_vcintrctrl_pen_STS_Intr_En_Mask                                                     cBit5_0
#define cAf6_ho_sk_vcintrctrl_pen_STS_Intr_En_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Sink STS/VC Group Interrupt Or Status
Reg Addr   : 0x56FFF
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 6 bit for 6 STS group,
each bit used to stored Interrupt Or Status of the related selected group.

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_sk_grpintrosta_pen                                                                  0x56FFF

/*--------------------------------------
BitField Name: Grp_IntrOrSta
BitField Type: R/W
BitField Desc: Interrupt Or Status of STS groups (0-5)
BitField Bits: [05:00]
--------------------------------------*/
#define cAf6_ho_sk_grpintrosta_pen_Grp_IntrOrSta_Mask                                                  cBit5_0
#define cAf6_ho_sk_grpintrosta_pen_Grp_IntrOrSta_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Sink STS/VC Interrupt Or Status
Reg Addr   : 0x56300-0x56305
Reg Formula: 0x56300 + $grp_id
    Where  : 
           + $grp_id(0-5)
Reg Desc   : 
The register consists of 192 bits for 192 STS/VCs. Each bit is used to store Interrupt OR status of the related STS/VC.
192 bits is devived to 6 group, each group contain 32 bit for 32 STS.

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_sk_vcintrosta_pen_Base                                                              0x56300
#define cAf6Reg_ho_sk_vcintrosta_pen(grpid)                                                  (0x56300+(grpid))

/*--------------------------------------
BitField Name: STS_IntrOrSta
BitField Type: RO
BitField Desc: Interrupt Status for 32 STS(0-31)
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_ho_sk_vcintrosta_pen_STS_IntrOrSta_Mask                                                  cBit31_0
#define cAf6_ho_sk_vcintrosta_pen_STS_IntrOrSta_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Source Channel Interrupt Enable Control
Reg Addr   : 0x27000-0x270BF
Reg Formula: 0x27000 + $sts_id
    Where  : 
           + $sts_id(0-191)
Reg Desc   : 
This register is used to enable Interrupt, write 1 to enable interrupt for event.

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_so_intrctrl_pen_Base                                                                0x27000
#define cAf6Reg_ho_so_intrctrl_pen(stsid)                                                    (0x27000+(stsid))

/*--------------------------------------
BitField Name: So_MFSM_Intr_En
BitField Type: R/W
BitField Desc: Interrupt Enable for Member State Change event
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_ho_so_intrctrl_pen_So_MFSM_Intr_En_Mask                                                     cBit0
#define cAf6_ho_so_intrctrl_pen_So_MFSM_Intr_En_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Source Channel Interrupt Status
Reg Addr   : 0x27100-0x271BF
Reg Formula: 0x27100 + $sts_id
    Where  : 
           + $sts_id(0-191)
Reg Desc   : 
This register is used to show the status of event,which generate interrupt.

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_so_intrstk_pen_Base                                                                 0x27100
#define cAf6Reg_ho_so_intrstk_pen(stsid)                                                     (0x27100+(stsid))

/*--------------------------------------
BitField Name: So_MFSM_Intr_Sta
BitField Type: R/W
BitField Desc: Member state change event interrupt status
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_ho_so_intrstk_pen_So_MFSM_Intr_Sta_Mask                                                     cBit0
#define cAf6_ho_so_intrstk_pen_So_MFSM_Intr_Sta_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Source Channel Current Status
Reg Addr   : 0x27200-0x272BF
Reg Formula: 0x27200 + $sts_id
    Where  : 
           + $sts_id(0-191)
Reg Desc   : 
This register is used to show the current status of event.

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_so_intrsta_pen_Base                                                                 0x27200
#define cAf6Reg_ho_so_intrsta_pen(stsid)                                                     (0x27200+(stsid))

/*--------------------------------------
BitField Name: So_MFSM_Intr_CrrSta
BitField Type: RO
BitField Desc: Member state change event current status
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_ho_so_intrsta_pen_So_MFSM_Intr_CrrSta_Mask                                                  cBit0
#define cAf6_ho_so_intrsta_pen_So_MFSM_Intr_CrrSta_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Source STS/VC Group Interrupt Enable Control
Reg Addr   : 0x27FFE
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 6 bit nterrupt enable bits for 6 group,
192 STS is devived to 6 group, each group contain 32 STS.

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_so_vcintrctrl_pen                                                                   0x27FFE

/*--------------------------------------
BitField Name: So_STS_Intr_En
BitField Type: R/W
BitField Desc: Interrupt Enable for 32 STS(0-31)
BitField Bits: [05:00]
--------------------------------------*/
#define cAf6_ho_so_vcintrctrl_pen_So_STS_Intr_En_Mask                                                  cBit5_0
#define cAf6_ho_so_vcintrctrl_pen_So_STS_Intr_En_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Soure STS/VC Group Interrupt Or Status
Reg Addr   : 0x27FFF
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 6 bit Interrupt enable bits for 6 STS group,
each bit used to stored Interrupt Or Status of the related selected group.

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_so_grpintrctrl_pen                                                                  0x27FFF

/*--------------------------------------
BitField Name: So_Grp_IntrOrSta
BitField Type: R/W
BitField Desc: Interrupt Or Status of STS groups (0-5)
BitField Bits: [05:00]
--------------------------------------*/
#define cAf6_ho_so_grpintrctrl_pen_So_Grp_IntrOrSta_Mask                                               cBit5_0
#define cAf6_ho_so_grpintrctrl_pen_So_Grp_IntrOrSta_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Source STS/VC Interrupt Or Status
Reg Addr   : 0x27300-0x27305
Reg Formula: 0x27300 + $grp_id
    Where  : 
           + $grp_id(0-5)
Reg Desc   : 
The register consists of 192 bits for 192 STS/VCs. Each bit is used to store Interrupt OR status of the related STS/VC.
192 bits is devived to 6 group, each group contain 32 bit for 32 STS.

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_so_vcintrosta_pen_Base                                                              0x27300
#define cAf6Reg_ho_so_vcintrosta_pen(grpid)                                                  (0x27300+(grpid))

/*--------------------------------------
BitField Name: So_STS_IntrOrSta
BitField Type: RO
BitField Desc: Interrupt Status for 32 STS(0-31)
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_ho_so_vcintrosta_pen_So_STS_IntrOrSta_Mask                                               cBit31_0
#define cAf6_ho_so_vcintrosta_pen_So_STS_IntrOrSta_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Interrupt Global Control
Reg Addr   : 0x00030
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to enable HOVCAT Interrupt , write 1 to enable interrupt.

------------------------------------------------------------------------------*/
#define cAf6Reg_hovcat_intrctrl_pen                                                                    0x00030

/*--------------------------------------
BitField Name: Glb_Intr_Ena
BitField Type: R/W
BitField Desc: Global Interrupt Enable
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_hovcat_intrctrl_pen_Glb_Intr_Ena_Mask                                                       cBit2
#define cAf6_hovcat_intrctrl_pen_Glb_Intr_Ena_Shift                                                          2

/*--------------------------------------
BitField Name: Sk_Intr_Ena
BitField Type: R/W
BitField Desc: Sink Interrupt Enable
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_hovcat_intrctrl_pen_Sk_Intr_Ena_Mask                                                        cBit1
#define cAf6_hovcat_intrctrl_pen_Sk_Intr_Ena_Shift                                                           1

/*--------------------------------------
BitField Name: So_Intr_Ena
BitField Type: R/W
BitField Desc: Source Interrupt Enable
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_hovcat_intrctrl_pen_So_Intr_Ena_Mask                                                        cBit0
#define cAf6_hovcat_intrctrl_pen_So_Intr_Ena_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Global Interrupt Or Status
Reg Addr   : 0x00031
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to store Interrupt Or Status of related selected side.

------------------------------------------------------------------------------*/
#define cAf6Reg_hovcat_introsta_pen                                                                    0x00031

/*--------------------------------------
BitField Name: Sk_IntrOrSta
BitField Type: RO
BitField Desc: Sink Interrupt Or Status
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_hovcat_introsta_pen_Sk_IntrOrSta_Mask                                                       cBit1
#define cAf6_hovcat_introsta_pen_Sk_IntrOrSta_Shift                                                          1

/*--------------------------------------
BitField Name: So_IntrOrSta
BitField Type: RO
BitField Desc: Source Interrupt Or Status
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_hovcat_introsta_pen_So_IntrOrSta_Mask                                                       cBit0
#define cAf6_hovcat_introsta_pen_So_IntrOrSta_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Sink Member Deskew Latency Threshold Control
Reg Addr   : 0x48002
Reg Formula: 
    Where  : 
Reg Desc   : 
The register used to congfigure threshold for VC4/VC3 member deskew latency in VCG

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_sk_mndthr_pen                                                                       0x48002

/*--------------------------------------
BitField Name: VC4_MND_Ena
BitField Type: R/W
BitField Desc: Set 1 to enable VC4 MND Alarm
BitField Bits: [23:23]
--------------------------------------*/
#define cAf6_ho_sk_mndthr_pen_VC4_MND_Ena_Mask                                                          cBit23
#define cAf6_ho_sk_mndthr_pen_VC4_MND_Ena_Shift                                                             23

/*--------------------------------------
BitField Name: VC3_MND_Ena
BitField Type: R/W
BitField Desc: Set 1 to enable VC3 MND Alarm
BitField Bits: [22:22]
--------------------------------------*/
#define cAf6_ho_sk_mndthr_pen_VC3_MND_Ena_Mask                                                          cBit22
#define cAf6_ho_sk_mndthr_pen_VC3_MND_Ena_Shift                                                             22

/*--------------------------------------
BitField Name: VC4_MND_Thr
BitField Type: R/W
BitField Desc: Number of frame VC3 for VC4 VCAT (0-2047)
BitField Bits: [21:12]
--------------------------------------*/
#define cAf6_ho_sk_mndthr_pen_VC4_MND_Thr_Mask                                                       cBit21_12
#define cAf6_ho_sk_mndthr_pen_VC4_MND_Thr_Shift                                                             12

/*--------------------------------------
BitField Name: VC3_MND_Thr
BitField Type: R/W
BitField Desc: Number of frame VC3 for VC3 VCAT (0-2047)
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_ho_sk_mndthr_pen_VC3_MND_Thr_Mask                                                        cBit11_0
#define cAf6_ho_sk_mndthr_pen_VC3_MND_Thr_Shift                                                              0

#endif /* _THA60290081MODULEHOVCATREG_H_ */

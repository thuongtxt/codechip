/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CONCATE
 *
 * File        : Tha60290081LoVcatInterruptManager.c
 *
 * Created Date: Sep 6, 2019
 *
 * Description : 60290081 LOVCAT interrupt manager implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/concate/AtConcateMemberInternal.h"
#include "../../../default/man/ThaIpCoreInternal.h"
#include "../../../default/man/intrcontroller/ThaInterruptManagerInternal.h"
#include "../man/Tha60290081IntrReg.h"
#include "Tha60290081ModuleConcate.h"
#include "Tha60290081ModuleLoVcatReg.h"
#include "Tha60290081VcatInterruptManager.h"

/*--------------------------- Define -----------------------------------------*/
#define cNumLoVcatSts   48

/*--------------------------- Macros -----------------------------------------*/
#define mManager(self) ((ThaInterruptManager)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290081LoVcatInterruptManager
    {
    tThaInterruptManager super;
    }tTha60290081LoVcatInterruptManager;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtInterruptManagerMethods       m_AtInterruptManagerOverride;
static tThaInterruptManagerMethods      m_ThaInterruptManagerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static Tha60290081ModuleConcate ModuleConcate(AtInterruptManager self)
    {
    return (Tha60290081ModuleConcate)AtInterruptManagerModuleGet(self);
    }

static void SourceInterruptProcess(AtInterruptManager self, AtHal hal)
    {
    uint32 baseAddress = ThaInterruptManagerBaseAddress(mManager(self));
    uint32 group;

    for (group = 0; group < 2; group++)
        {
        uint32 stsIntrEn = AtHalRead(hal, baseAddress + cAf6Reg_map_source_stsvcint_enb_ctrl_Base + group);
        uint32 stsIntrOR = AtHalRead(hal, baseAddress + cAf6Reg_map_source_stsvcint_or_sta_Base + group);
        uint8 stsInGroup;

        stsIntrOR &= stsIntrEn;
        for (stsInGroup = 0; stsInGroup < 32; stsInGroup++)
            {
            uint32 hwSts = (uint32)((group << 5) + stsInGroup);

            if (hwSts >= cNumLoVcatSts)
                return;

            if (stsIntrOR & cIteratorMask(stsInGroup))
                {
                uint32 vtIntrOR = AtHalRead(hal, baseAddress + cAf6Reg_map_source_chnint_or_sta_Base + hwSts);
                uint32 hwVtn;

                for (hwVtn = 0; hwVtn < 32; hwVtn++)
                    {
                    if (vtIntrOR & cIteratorMask(hwVtn))
                        {
                        uint32 hwEngineId =  (uint32)(hwSts * 32U + hwVtn);
                        AtConcateMember member = Tha60290081ModuleConcateLoVcatSourceMemberFromHwIdGet(ModuleConcate(self), hwEngineId);

                        if (member)
                            AtConcateMemberSourceInterruptProcess(member);
                        }
                    }
                }
            }
        }
    }

static void SinkInterruptProcess(AtInterruptManager self, AtHal hal)
    {
    uint32 baseAddress = ThaInterruptManagerBaseAddress(mManager(self));
    uint32 group;

    for (group = 0; group < 2; group++)
        {
        uint32 stsIntrOR = AtHalRead(hal, baseAddress + cAf6Reg_map_sink_stsvcint_or_sta_Base + group);
        uint32 stsIntrEn = AtHalRead(hal, baseAddress + cAf6Reg_map_sink_stsvcint_enb_ctrl_Base + group);
        uint8 stsInGroup;

        stsIntrOR &= stsIntrEn;
        for (stsInGroup = 0; stsInGroup < 32; stsInGroup++)
            {
            uint32 hwSts = (uint32)((group << 5) + stsInGroup);

            if (hwSts >= cNumLoVcatSts)
                return;

            if (stsIntrOR & cIteratorMask(stsInGroup))
                {
                uint32 vtIntrOR = AtHalRead(hal, baseAddress + cAf6Reg_map_sink_chnint_or_sta_Base + hwSts);
                uint32 hwVtn;

                for (hwVtn = 0; hwVtn < 32; hwVtn++)
                    {
                    if (vtIntrOR & cIteratorMask(hwVtn))
                        {
                        uint32 hwEngineId =  (uint32)(hwSts * 32U + hwVtn);
                        AtConcateMember member = Tha60290081ModuleConcateLoVcatSinkMemberFromHwIdGet(ModuleConcate(self), hwEngineId);

                        if (member)
                            AtConcateMemberSinkInterruptProcess(member);
                        }
                    }
                }
            }
        }
    }

static void InterruptProcess(AtInterruptManager self, uint32 glbIntr, AtIpCore ipCore)
    {
    AtHal hal = AtIpCoreHalGet(ipCore);
    uint32 baseAddress = ThaInterruptManagerBaseAddress(mManager(self));
    uint32 glbIntrEn = AtHalRead(hal, baseAddress + cAf6Reg_map_glb_intenb);
    uint32 glbIntrOR = AtHalRead(hal, baseAddress + cAf6Reg_map_glb_intsta);
    AtUnused(glbIntr);

    glbIntrOR &= glbIntrEn;

    if (glbIntrOR & cAf6_map_glb_intsta_MapSkIntSta_Mask)
        SinkInterruptProcess(self, hal);

    if (glbIntrOR & cAf6_map_glb_intsta_MapSoIntSta_Mask)
        SourceInterruptProcess(self, hal);
    }

static void InterruptHwEnable(ThaInterruptManager self, eBool enable, AtIpCore ipCore)
    {
    AtHal hal = AtIpCoreHalGet(ipCore);
    uint32 baseAddress = ThaInterruptManagerBaseAddress(mManager(self));
    uint32 group;

    /* Global interrupt enable */
    AtHalWrite(hal, baseAddress + cAf6Reg_map_glb_intenb, (enable) ? cBit2_0 : 0);

    for (group = 0; group < 2; group++)
        {
        /* Source interrupt enable */
        AtHalWrite(hal, baseAddress + cAf6Reg_map_source_stsvcint_enb_ctrl(group), (enable) ? cBit31_0 : 0);

        /* Sink interrupt enable */
        AtHalWrite(hal, baseAddress + cAf6Reg_map_sink_stsvcint_enb_ctrl(group), (enable) ? cBit31_0 : 0);
        }

    ThaIpCoreConcateHwInterruptEnable((ThaIpCore)ipCore, enable);
    }

static uint32 BaseAddress(ThaInterruptManager self)
    {
    return Tha60290081ModuleConcateLoBaseAddress(ModuleConcate((AtInterruptManager)self));
    }

static eBool HasInterrupt(AtInterruptManager self, uint32 glbIntr, AtHal hal)
    {
    AtUnused(self);
    AtUnused(hal);
    return (glbIntr & cAf6_global_interrupt_status_VcatLOIntStatus_Mask) ? cAtTrue : cAtFalse;
    }

static void OverrideAtInterruptManager(AtInterruptManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtInterruptManagerOverride, mMethodsGet(self), sizeof(m_AtInterruptManagerOverride));

        mMethodOverride(m_AtInterruptManagerOverride, InterruptProcess);
        mMethodOverride(m_AtInterruptManagerOverride, HasInterrupt);
        }

    mMethodsSet(self, &m_AtInterruptManagerOverride);
    }

static void OverrideThaInterruptManager(AtInterruptManager self)
    {
    ThaInterruptManager manager = (ThaInterruptManager)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInterruptManagerOverride, mMethodsGet(manager), sizeof(m_ThaInterruptManagerOverride));

        mMethodOverride(m_ThaInterruptManagerOverride, BaseAddress);
        mMethodOverride(m_ThaInterruptManagerOverride, InterruptHwEnable);
        }

    mMethodsSet(manager, &m_ThaInterruptManagerOverride);
    }

static void Override(AtInterruptManager self)
    {
    OverrideAtInterruptManager(self);
    OverrideThaInterruptManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081LoVcatInterruptManager);
    }

static AtInterruptManager ObjectInit(AtInterruptManager self, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaInterruptManagerObjectInit(self, module) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInterruptManager Tha60290081LoVcatInterruptManagerNew(AtModule module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInterruptManager newManager = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newManager == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newManager, module);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CONCATE
 * 
 * File        : Tha60290081VcatInterruptManager.h
 * 
 * Created Date: Aug 29, 2019
 *
 * Description : 60290081 HOVCAT interrupt manager
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290081VCATINTERRUPTMANAGER_H_
#define _THA60290081VCATINTERRUPTMANAGER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtInterruptManager Tha60290081HoVcatInterruptManagerNew(AtModule module);
AtInterruptManager Tha60290081LoVcatInterruptManagerNew(AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290081VCATINTERRUPTMANAGER_H_ */


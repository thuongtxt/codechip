/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CONCATE
 * 
 * File        : Tha60290081Vcg.h
 * 
 * Created Date: Aug 15, 2019
 *
 * Description : 60290081 VCG header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290081VCGVCAT_H_
#define _THA60290081VCGVCAT_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/concate/ThaVcg.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290081VcgVcat *Tha60290081VcgVcat;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha60290081VcgVcatMembersReset(ThaVcgVcat self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290081VCGVCAT_H_ */


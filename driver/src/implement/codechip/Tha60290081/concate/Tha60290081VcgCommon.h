/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CONCATE
 * 
 * File        : Tha60290081VcgCommon.h
 * 
 * Created Date: Jul 22, 2019
 *
 * Description : 60290081 Module CONCATE
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290081VCGCOMMON_H_
#define _THA60290081VCGCOMMON_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/encap/AtEncapChannelInternal.h"
#include "../encap/Tha60290081ModuleEncapInternal.h"
#include "../encap/Tha60290081GfpChannel.h"
#include "../pda/Tha60290081ModulePda.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static Tha60290081ModuleConcate ModuleConcate(AtVcg self)
    {
    return (Tha60290081ModuleConcate)AtChannelModuleGet((AtChannel)self);
    }

static AtModuleEncap ModuleEncap(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return (AtModuleEncap) AtDeviceModuleGet(device, cAtModuleEncap);
    }

static Tha60290081ModulePda ModulePda(AtConcateGroup self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (Tha60290081ModulePda) AtDeviceModuleGet(device, cThaModulePda);
    }

static void EncapChannelDelete(AtChannel self)
    {
    uint32 groupId = AtChannelIdGet((AtChannel)self);
    AtEncapChannel channel = AtChannelBoundEncapChannel(self);

    if (channel)
        {
        AtEncapChannelPhysicalChannelSet(channel, NULL);
        AtModuleEncapChannelDelete(ModuleEncap(self), (uint16)groupId);
        }

    AtChannelBoundEncapChannelSet(self, NULL);
    }

static void Delete(AtObject self)
    {
    EncapChannelDelete((AtChannel)self);
    m_AtObjectMethods->Delete(self);
    }

static eBool EncapTypeIsSupported(AtConcateGroup self, eAtEncapType encapType)
    {
    AtUnused(self);
    return (encapType == cAtEncapGfp) ? cAtTrue : cAtFalse;
    }

static uint32 EncapTypeGet(AtConcateGroup self)
    {
    AtUnused(self);
    return cAtEncapGfp;
    }

static eBool ServiceIsRunning(AtChannel self)
    {
    AtEncapChannel channel = AtConcateGroupEncapChannelGet((AtConcateGroup)self);
    return AtChannelServiceIsRunning((AtChannel)channel);
    }

static uint32 HwIdGet(AtChannel self)
    {
    return Tha60290081ModuleConcateGroupLocalId(ModuleConcate((AtVcg)self), (AtConcateGroup)self);
    }

static AtEncapChannel GfpChannelGet(AtChannel self)
    {
    uint32 groupId = AtChannelIdGet((AtChannel)self);
    return (AtEncapChannel)AtModuleEncapGfpChannelCreate(ModuleEncap(self), (uint16)groupId);
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret;
    AtEncapChannel encapChannel;

    ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    encapChannel = GfpChannelGet(self);
    ret |= AtChannelBindToEncapChannel(self, encapChannel);
    ret |= AtEncapChannelPhyBind(encapChannel, self);
    return ret;
    }

static eAtRet EosEthFlowBwUpdate(AtConcateGroup self)
    {
    AtEthFlow flow;
    AtEncapChannel encapChannel;

    encapChannel = AtChannelBoundEncapChannel((AtChannel)self);
    if (encapChannel == NULL)
        return cAtOk;

    flow = AtEncapChannelBoundFlowGet(encapChannel);
    if (flow == NULL)
        return cAtOk;

    return Tha60290081ModulePdaEosEthFlowBandwidthUpdate(ModulePda(self), flow, (AtChannel)encapChannel);
    }

static eAtRet EosGfpChannelBwUpdate(AtConcateGroup self)
    {
    AtEncapChannel encapChannel = AtChannelBoundEncapChannel((AtChannel)self);
    if (encapChannel)
        return Tha60290081GfpChannelTxBandwidthUpdate((AtGfpChannel)encapChannel);
    return cAtOk;
    }

static AtConcateMember SourceMemberProvision(AtConcateGroup self, AtChannel channel)
    {
    AtConcateMember member = m_AtConcateGroupMethods->SourceMemberProvision(self, channel);
    if (member)
        {
        EosGfpChannelBwUpdate(self);
        EosEthFlowBwUpdate(self);
        }
    return member;
    }

static eAtModuleConcateRet SourceMemberDeprovision(AtConcateGroup self, AtChannel channel)
    {
    eAtRet ret = m_AtConcateGroupMethods->SourceMemberDeprovision(self, channel);
    if (ret != cAtOk)
        return ret;

    ret |= EosEthFlowBwUpdate(self);
    ret |= EosGfpChannelBwUpdate(self);
    return ret;
    }

#endif /* _THA60290081VCGCOMMON_H_ */


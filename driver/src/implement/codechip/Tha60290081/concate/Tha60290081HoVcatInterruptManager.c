/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CONCATE
 *
 * File        : Tha60290081HoVcatInterruptManager.c
 *
 * Created Date: Aug 29, 2019
 *
 * Description : 60290081 HOVCAT interrupt manager implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/concate/AtConcateMemberInternal.h"
#include "../../../default/man/ThaIpCoreInternal.h"
#include "../../../default/man/intrcontroller/ThaInterruptManagerInternal.h"
#include "../man/Tha60290081IntrReg.h"
#include "Tha60290081ModuleConcate.h"
#include "Tha60290081ModuleHoVcatReg.h"
#include "Tha60290081VcatInterruptManager.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mManager(self) ((ThaInterruptManager)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290081HoVcatInterruptManager
    {
    tThaInterruptManager super;
    }tTha60290081HoVcatInterruptManager;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtInterruptManagerMethods       m_AtInterruptManagerOverride;
static tThaInterruptManagerMethods      m_ThaInterruptManagerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static Tha60290081ModuleConcate ModuleConcate(AtInterruptManager self)
    {
    return (Tha60290081ModuleConcate)AtInterruptManagerModuleGet(self);
    }

static void SourceInterruptProcess(AtInterruptManager self, AtHal hal)
    {
    uint32 baseAddress = ThaInterruptManagerBaseAddress(mManager(self));
    uint32 grpIntrOR = AtHalRead(hal, baseAddress + cAf6Reg_ho_so_grpintrctrl_pen);
    uint32 grpIntrEn = AtHalRead(hal, baseAddress + cAf6Reg_ho_so_vcintrctrl_pen);
    uint32 group;

    grpIntrOR &= grpIntrEn;
    for (group = 0; group < 6; group++)
        {
        if (grpIntrOR & cIteratorMask(group))
            {
            uint32 stsIntrOR = AtHalRead(hal, baseAddress + cAf6Reg_ho_so_vcintrosta_pen_Base + group);
            uint8 stsInGroup;

            for (stsInGroup = 0; stsInGroup < 32; stsInGroup++)
                {
                uint16 sts192 = (uint16)((group << 5) + stsInGroup);

                if (stsIntrOR & cIteratorMask(stsInGroup))
                    {
                    AtConcateMember member = Tha60290081ModuleConcateHoVcatSourceMemberFromHwIdGet(ModuleConcate(self), sts192);
                    if (member)
                        AtConcateMemberSourceInterruptProcess(member);
                    }
                }
            }
        }
    }

static void SinkInterruptProcess(AtInterruptManager self, AtHal hal)
    {
    uint32 baseAddress = ThaInterruptManagerBaseAddress(mManager(self));
    uint32 grpIntrOR = AtHalRead(hal, baseAddress + cAf6Reg_ho_sk_grpintrosta_pen);
    uint32 grpIntrEn = AtHalRead(hal, baseAddress + cAf6Reg_ho_sk_vcintrctrl_pen);
    uint32 group;

    grpIntrOR &= grpIntrEn;
    for (group = 0; group < 6; group++)
        {
        if (grpIntrOR & cIteratorMask(group))
            {
            uint32 stsIntrOR = AtHalRead(hal, baseAddress + cAf6Reg_ho_sk_vcintrosta_pen_Base + group);
            uint8 stsInGroup;

            for (stsInGroup = 0; stsInGroup < 32; stsInGroup++)
                {
                uint16 sts192 = (uint16)((group << 5) + stsInGroup);

                if (stsIntrOR & cIteratorMask(stsInGroup))
                    {
                    AtConcateMember member = Tha60290081ModuleConcateHoVcatSinkMemberFromHwIdGet(ModuleConcate(self), sts192);
                    if (member)
                        AtConcateMemberSinkInterruptProcess(member);
                    }
                }
            }
        }
    }

static void InterruptProcess(AtInterruptManager self, uint32 glbIntr, AtIpCore ipCore)
    {
    AtHal hal = AtIpCoreHalGet(ipCore);
    uint32 baseAddress = ThaInterruptManagerBaseAddress(mManager(self));
    uint32 glbIntrEn = AtHalRead(hal, baseAddress + cAf6Reg_hovcat_intrctrl_pen);
    uint32 glbIntrOR = AtHalRead(hal, baseAddress + cAf6Reg_hovcat_introsta_pen);
    AtUnused(glbIntr);

    glbIntrOR &= glbIntrEn;

    if (glbIntrOR & cAf6_hovcat_introsta_pen_Sk_IntrOrSta_Mask)
        SinkInterruptProcess(self, hal);

    if (glbIntrOR & cAf6_hovcat_introsta_pen_So_IntrOrSta_Mask)
        SourceInterruptProcess(self, hal);
    }

static void InterruptHwEnable(ThaInterruptManager self, eBool enable, AtIpCore ipCore)
    {
    AtHal hal = AtIpCoreHalGet(ipCore);
    uint32 baseAddress = ThaInterruptManagerBaseAddress(mManager(self));

    AtHalWrite(hal, baseAddress + cAf6Reg_hovcat_intrctrl_pen,  (enable) ? cBit2_0 : 0);
    AtHalWrite(hal, baseAddress + cAf6Reg_ho_so_vcintrctrl_pen, (enable) ? cBit5_0 : 0);
    AtHalWrite(hal, baseAddress + cAf6Reg_ho_sk_vcintrctrl_pen, (enable) ? cBit5_0 : 0);

    ThaIpCoreConcateHwInterruptEnable((ThaIpCore)ipCore, enable);
    }

static uint32 BaseAddress(ThaInterruptManager self)
    {
    return Tha60290081ModuleConcateHoBaseAddress(ModuleConcate((AtInterruptManager)self));
    }

static eBool HasInterrupt(AtInterruptManager self, uint32 glbIntr, AtHal hal)
    {
    AtUnused(self);
    AtUnused(hal);
    return (glbIntr & cAf6_global_interrupt_status_VcatHOIntStatus_Mask) ? cAtTrue : cAtFalse;
    }

static void OverrideAtInterruptManager(AtInterruptManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtInterruptManagerOverride, mMethodsGet(self), sizeof(m_AtInterruptManagerOverride));

        mMethodOverride(m_AtInterruptManagerOverride, InterruptProcess);
        mMethodOverride(m_AtInterruptManagerOverride, HasInterrupt);
        }

    mMethodsSet(self, &m_AtInterruptManagerOverride);
    }

static void OverrideThaInterruptManager(AtInterruptManager self)
    {
    ThaInterruptManager manager = (ThaInterruptManager)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInterruptManagerOverride, mMethodsGet(manager), sizeof(m_ThaInterruptManagerOverride));

        mMethodOverride(m_ThaInterruptManagerOverride, BaseAddress);
        mMethodOverride(m_ThaInterruptManagerOverride, InterruptHwEnable);
        }

    mMethodsSet(manager, &m_ThaInterruptManagerOverride);
    }

static void Override(AtInterruptManager self)
    {
    OverrideAtInterruptManager(self);
    OverrideThaInterruptManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081HoVcatInterruptManager);
    }

static AtInterruptManager ObjectInit(AtInterruptManager self, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaInterruptManagerObjectInit(self, module) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInterruptManager Tha60290081HoVcatInterruptManagerNew(AtModule module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInterruptManager newManager = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newManager == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newManager, module);
    }

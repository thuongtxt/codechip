/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha60290081ModuleSdh.h
 * 
 * Created Date: Jun 15, 2019
 *
 * Description : 60290081 Module SDH
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290081MODULESDH_H_
#define _THA60290081MODULESDH_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Line */
AtSdhLine Tha60290081SdhFacePlateLineNew(uint32 lineId, AtModuleSdh module);

/* AU VC */
AtSdhVc Tha60290081SdhTerminatedLineAuVcNew(uint32 channelId, uint8 channelType, AtModuleSdh module);

/* TU3 VC */
AtSdhVc Tha60290081SdhTerminatedLineTu3VcNew(uint32 channelId, uint8 channelType, AtModuleSdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290081MODULESDH_H_ */


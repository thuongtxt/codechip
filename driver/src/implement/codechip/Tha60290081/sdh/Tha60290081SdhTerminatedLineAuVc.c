/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60290081SdhTerminatedLineAuVc.c
 *
 * Created Date: Jun 15, 2019
 *
 * Description : 60290081 Terminated AUVC implementation.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290022/sdh/Tha60290022SdhTerminatedLineAuVcInternal.h"
#include "../ocn/Tha60290081ModuleOcn.h"
#include "Tha60290081ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290081SdhTerminatedLineAuVc
    {
    tTha60290022SdhTerminatedLineAuVc super;
    }tTha60290081SdhTerminatedLineAuVc;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSdhVcMethods                          m_AtSdhVcOverride;
static tThaSdhVcMethods                         m_ThaSdhVcOverride;
static tAtChannelMethods                        m_AtChannelOverride;
static tAtSdhPathMethods                        m_AtSdhPathOverride;

/* Save super implementation */
static const tAtChannelMethods                    *m_AtChannelMethods    = NULL;
static const tAtSdhVcMethods                      *m_AtSdhVcMethods      = NULL;
static const tAtSdhPathMethods                    *m_AtSdhPathMethods    = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static Tha60290081ModuleOcn ModuleOcn(AtChannel self)
    {
    return (Tha60290081ModuleOcn)ThaModuleOcnFromChannel(self);
    }

static eAtRet BusTypeSet(AtChannel self, uint8 busType)
    {
    return Tha60290081ModuleOcnTerminatedAuVcBusTypeSet(ModuleOcn(self), (AtSdhChannel)self, busType);
    }

static eAtRet DefaultBusTypeSet(AtChannel self)
    {
    static uint8 defaultBusType = cTha60290081OcnTerminatedAuVcBusTypePw;
    return BusTypeSet(self, defaultBusType);
    }

static eAtRet BindToChannelWithBusType(AtChannel self, AtChannel channel, uint8 busType)
    {
    if (channel)
        return BusTypeSet(self, busType);

    return DefaultBusTypeSet(self);
    }

static eAtRet BindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    eAtRet ret = m_AtChannelMethods->BindToPseudowire(self, pseudowire);
    if (ret != cAtOk)
        return ret;

    return BindToChannelWithBusType(self, (AtChannel)pseudowire, cTha60290081OcnTerminatedAuVcBusTypePw);
    }

static eAtRet BindToEncapChannel(AtChannel self, AtEncapChannel encapChannel)
    {
    eAtRet ret = m_AtChannelMethods->BindToEncapChannel(self, encapChannel);
    if (ret != cAtOk)
        return ret;

    return BindToChannelWithBusType(self, (AtChannel)encapChannel, cTha60290081OcnTerminatedAuVcBusTypePos);
    }

static eAtRet BindToSourceGroup(AtChannel self, AtConcateGroup group)
    {
    eAtRet ret = m_AtChannelMethods->BindToSourceGroup(self, group);
    if (ret != cAtOk)
        return ret;

    return BindToChannelWithBusType(self, (AtChannel)group, cTha60290081OcnTerminatedAuVcBusTypeEos);
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return DefaultBusTypeSet(self);
    }

static void OverrideAtChannel(AtSdhVc self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, BindToPseudowire);
        mMethodOverride(m_AtChannelOverride, BindToEncapChannel);
        mMethodOverride(m_AtChannelOverride, BindToSourceGroup);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtSdhPath(AtSdhVc self)
    {
    AtSdhPath path = (AtSdhPath)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhPathMethods = mMethodsGet(path);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhPathOverride, m_AtSdhPathMethods, sizeof(m_AtSdhPathOverride));

        }

    mMethodsSet(path, &m_AtSdhPathOverride);
    }

static void OverrideThaSdhVc(AtSdhVc self)
    {
    ThaSdhVc vc = (ThaSdhVc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhVcOverride, mMethodsGet(vc), sizeof(m_ThaSdhVcOverride));
        }

    mMethodsSet(vc, &m_ThaSdhVcOverride);
    }

static void OverrideAtSdhVc(AtSdhVc self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhVcMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhVcOverride, mMethodsGet(self), sizeof(m_AtSdhVcOverride));
        }

    mMethodsSet(self, &m_AtSdhVcOverride);
    }

static void Override(AtSdhVc self)
    {
    OverrideThaSdhVc(self);
    OverrideAtSdhVc(self);
    OverrideAtChannel(self);
    OverrideAtSdhPath(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081SdhTerminatedLineAuVc);
    }

static AtSdhVc ObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022SdhTerminatedLineAuVcObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhVc Tha60290081SdhTerminatedLineAuVcNew(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhVc self = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (self == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(self, channelId, channelType, module);
    }

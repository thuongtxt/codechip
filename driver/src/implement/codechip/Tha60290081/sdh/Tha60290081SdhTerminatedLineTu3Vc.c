/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60290081SdhTerminatedLineTu3Vc.c
 *
 * Created Date: Sep 18, 2019
 *
 * Description : 60290081 Terminated TU3 VC implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290022/sdh/Tha60290022SdhTerminatedLineTu3VcInternal.h"
#include "../ocn/Tha60290081ModuleOcn.h"
#include "Tha60290081ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290081SdhTerminatedLineTu3Vc
    {
    tTha60290022SdhTerminatedLineTu3Vc super;
    }tTha60290081SdhTerminatedLineTu3Vc;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods m_AtChannelOverride;
static tAtSdhVcMethods   m_AtSdhVcOverride;
static tThaSdhVcMethods  m_ThaSdhVcOverride;

/* Save super implementation */
static const tAtChannelMethods *m_AtChannelMethods = NULL;
static const tAtSdhVcMethods   *m_AtSdhVcMethods   = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static Tha60290081ModuleOcn ModuleOcn(AtChannel self)
    {
    return (Tha60290081ModuleOcn)ThaModuleOcnFromChannel(self);
    }

static eAtRet BusTypeSet(AtChannel self, uint8 busType)
    {
    return Tha60290081ModuleOcnTerminatedAuVcBusTypeSet(ModuleOcn(self), (AtSdhChannel)self, busType);
    }

static eAtRet DefaultBusTypeSet(AtChannel self)
    {
    static uint8 defaultBusType = cTha60290081OcnTerminatedAuVcBusTypePw;
    return BusTypeSet(self, defaultBusType);
    }

static eAtRet BindToChannelWithBusType(AtChannel self, AtChannel channel, uint8 busType)
    {
    if (channel)
        return BusTypeSet(self, busType);

    return DefaultBusTypeSet(self);
    }

static eAtRet BindToConcateGroup(AtChannel self, AtConcateGroup group)
    {
    eAtRet ret = cAtOk;
    AtSdhChannel tu3Vc = (AtSdhChannel)self;

    if (AtSdhChannelMapTypeGet(tu3Vc) != cAtSdhVcMapTypeVc3MapC3)
        ret |= mMethodsGet(tu3Vc)->MapTypeSet(tu3Vc, cAtSdhVcMapTypeVc3MapC3);

    if (ret != cAtOk)
        return ret;

    /* Route TU3VC over HO bus (to HOVCAT) as same as what we made in
     * TU3VC to AU3VC CEP interworking model. */
    ret |= ThaOcnTu3VcToVc3Enable(tu3Vc, (group) ? cAtTrue : cAtFalse);

    return ret;
    }

static eAtRet BindToSourceGroup(AtChannel self, AtConcateGroup group)
    {
    eAtRet ret = m_AtChannelMethods->BindToSourceGroup(self, group);
    if (ret != cAtOk)
        return ret;

    ret  = BindToChannelWithBusType(self, (AtChannel)group, cTha60290081OcnTerminatedAuVcBusTypeEos);
    ret |= BindToConcateGroup(self, group);

    return ret;
    }

static eAtRet BindToSinkGroup(AtChannel self, AtConcateGroup group)
    {
    eAtRet ret = m_AtChannelMethods->BindToSinkGroup(self, group);
    if (ret != cAtOk)
        return ret;

    return BindToConcateGroup(self, group);
    }

static void OverrideThaSdhVc(AtSdhVc self)
    {
    ThaSdhVc vc = (ThaSdhVc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhVcOverride, mMethodsGet(vc), sizeof(m_ThaSdhVcOverride));
        }

    mMethodsSet(vc, &m_ThaSdhVcOverride);
    }

static void OverrideAtSdhVc(AtSdhVc self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhVcMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhVcOverride, mMethodsGet(self), sizeof(m_AtSdhVcOverride));
        }

    mMethodsSet(self, &m_AtSdhVcOverride);
    }

static void OverrideAtChannel(AtSdhVc self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, BindToSourceGroup);
        mMethodOverride(m_AtChannelOverride, BindToSinkGroup);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtSdhVc self)
    {
    OverrideThaSdhVc(self);
    OverrideAtSdhVc(self);
    OverrideAtChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081SdhTerminatedLineTu3Vc);
    }

static AtSdhVc ObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022SdhTerminatedLineTu3VcObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhVc Tha60290081SdhTerminatedLineTu3VcNew(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhVc self = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (self == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(self, channelId, channelType, module);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60290081SdhFacePlateLine.c
 *
 * Created Date: Jul 1, 2019
 *
 * Description : 60290081 Faceplate line implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290021/sdh/Tha60290021ModuleSdh.h"
#include "../../Tha60290022/sdh/Tha60290022ModuleSdh.h"
#include "../../Tha60290022/sdh/Tha60290022SdhFacePlateLineInternal.h"
#include "../ocn/Tha60290081ModuleOcnReg.h"
#include "Tha60290081ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290081SdhFacePlateLine
    {
    tTha60290022SdhFacePlateLine super;
    }tTha60290081SdhFacePlateLine;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods            m_AtChannelOverride;
static tAtSdhLineMethods            m_AtSdhLineOverride;
static tThaSdhLineMethods           m_ThaSdhLineOverride;
static tTha60210031SdhLineMethods   m_Tha60210031SdhLineOverride;

/* Save super implementation */
static const tAtChannelMethods          *m_AtChannelMethods          = NULL;
static const tAtSdhLineMethods          *m_AtSdhLineMethods          = NULL;
static const tThaSdhLineMethods         *m_ThaSdhLineMethods         = NULL;
static const tTha60210031SdhLineMethods *m_Tha60210031SdhLineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleSdh ModuleSdh(AtSdhLine self)
    {
    return (AtModuleSdh)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cAtModuleSdh);
    }

static ThaModuleOcn ModuleOcn(AtSdhLine self)
    {
    return (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cThaModuleOcn);
    }

static uint32 InterruptEnableMask(AtChannel self)
    {
    return (cBit0 << AtChannelIdGet(self));
    }

static uint32 InterruptEnableShift(AtChannel self)
    {
    return AtChannelIdGet(self);
    }

static uint32 AddressWithLocalAddress(AtChannel self, uint32 localAddress)
    {
    return ThaModuleOcnBaseAddress(ModuleOcn((AtSdhLine)self)) + localAddress;
    }

static eAtRet HelperInterruptEnable(AtChannel self, eBool enabled)
    {
    uint32 regAddr = AddressWithLocalAddress(self, cAf6Reg_tohintperlineenctl);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);
    uint32 interruptMask  = InterruptEnableMask(self);
    uint32 interruptShift = InterruptEnableShift(self);
    mRegFieldSet(regVal, interrupt, (enabled ? 1 : 0));
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);
    return cAtOk;
    }

static eBool InterruptIsEnabled(AtChannel self)
    {
    uint32 regAddr = AddressWithLocalAddress(self, cAf6Reg_tohintperlineenctl);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);
    uint32 interruptMask  = InterruptEnableMask(self);
    return (regVal & interruptMask) ? cAtTrue : cAtFalse;
    }

static eAtRet InterruptEnable(AtChannel self)
    {
    return HelperInterruptEnable(self, cAtTrue);
    }

static eAtRet InterruptDisable(AtChannel self)
    {
    return HelperInterruptEnable(self, cAtFalse);
    }

static uint8 LocalIdGet(AtSdhLine self)
    {
    return ThaModuleSdhLineIdLocalId((ThaModuleSdh)ModuleSdh(self), (uint8)AtChannelIdGet((AtChannel)self));
    }

static AtSdhLine HideLineGet(AtSdhLine self, uint8 sts1)
    {
    AtModuleSdh sdhModule = ModuleSdh(self);
    AtSdhLine terminatedLine = Tha60290021ModuleSdhTerminatedLineGet(sdhModule, 0);

    /* FIXME: it is identical copy of 10G CAS. need to revise under testing? */
    /* If faceplate line is not STM64, the terminated line must be STM16. */
    if (AtSdhLineRateGet(self) != cAtSdhLineRateStm64)
        {
        if (AtSdhLineRateGet(terminatedLine) == cAtSdhLineRateStm64)
            Tha60290022SdhTerminatedLineRateSet(terminatedLine, cAtSdhLineRateStm16);

        /* Four consecutive faceplate lines are mapped to one terminated line STM16. */
        return Tha60290021ModuleSdhTerminatedLineGet(sdhModule, (uint8)(LocalIdGet(self) / 4));
        }

    /* One faceplate line STM64 is mapped to one terminated line STM64. */
    if (AtSdhLineRateGet(terminatedLine) == cAtSdhLineRateStm64)
        return terminatedLine;

    /* One faceplate line STM64 is mapped to four terminated lines STM16. */
    return Tha60290021ModuleSdhTerminatedLineGet(sdhModule, (uint8)(sts1 / 48));
    }

static uint32 DefaultOffset(ThaSdhLine self)
    {
    return ThaModuleOcnBaseAddress(ModuleOcn((AtSdhLine)self)) + AtChannelIdGet((AtChannel)self);
    }

static uint32 B1CounterRegAddr(Tha60210031SdhLine self, eBool r2c)
    {
    AtUnused(self);
    return r2c ? cAf6Reg_tohb1errr2ccnt_Base : cAf6Reg_tohb1errrocnt_Base;
    }

static uint32 B2CounterRegAddr(Tha60210031SdhLine self, eBool r2c)
    {
    AtUnused(self);
    return r2c ? cAf6Reg_tohb2errr2ccnt_Base : cAf6Reg_tohb2errrocnt_Base;
    }

static uint32 ReiCounterRegAddr(Tha60210031SdhLine self, eBool r2c)
    {
    AtUnused(self);
    return r2c ? cAf6Reg_tohreierrr2ccnt_Base : cAf6Reg_tohreierrrocnt_Base;
    }

static uint32 B1BlockCounterRegAddr(Tha60210031SdhLine self, eBool r2c)
    {
    AtUnused(self);
    return r2c ? cAf6Reg_tohb1blkerrr2ccnt_Base : cAf6Reg_tohb1blkerrrocnt_Base;
    }

static uint32 B2BlockCounterRegAddr(Tha60210031SdhLine self, eBool r2c)
    {
    AtUnused(self);
    return r2c ? cAf6Reg_tohb2blkerrr2ccnt_Base : cAf6Reg_tohb2blkerrrocnt_Base;
    }

static uint32 ReiBlockCounterRegAddr(Tha60210031SdhLine self, eBool r2c)
    {
    AtUnused(self);
    return r2c ? cAf6Reg_tohreiblkerrr2ccnt_Base : cAf6Reg_tohreiblkerrrocnt_Base;
    }

static uint32 RxLineAlarmCurrentStatusRegAddr(Tha60210031SdhLine self)
    {
    AtUnused(self);
    return cAf6Reg_tohcursta_Base;
    }

static uint32 RxLineAlarmInterruptEnableControlRegAddr(Tha60210031SdhLine self)
    {
    AtUnused(self);
    return cAf6Reg_tohintperalrenbctl_Base;
    }

static uint32 RxLineAlarmInterruptStatusRegAddr(Tha60210031SdhLine self)
    {
    AtUnused(self);
    return cAf6Reg_tohintsta_Base;
    }

static uint32 TohS1MonitoringStatusRegAddr(Tha60210031SdhLine self)
    {
    AtUnused(self);
    return cAf6Reg_tohs1monsta_Base;
    }

static uint32 TohK1MonitoringStatusRegAddr(Tha60210031SdhLine self)
    {
    AtUnused(self);
    return cAf6Reg_tohk1monsta_Base;
    }

static uint32 TohK2MonitoringStatusRegAddr(Tha60210031SdhLine self)
    {
    AtUnused(self);
    return cAf6Reg_tohk2monsta_Base;
    }

static uint32 TohGlobalS1StableMonitoringThresholdControlRegAddr(Tha60210031SdhLine self)
    {
    return ThaModuleOcnTohGlobalS1StableMonitoringThresholdControlRegAddr(ModuleOcn((AtSdhLine)self));
    }

static uint32 TohGlobalK1StableMonitoringThresholdControlRegAddr(Tha60210031SdhLine self)
    {
    return ThaModuleOcnTohGlobalK1StableMonitoringThresholdControlRegAddr(ModuleOcn((AtSdhLine)self));
    }

static uint32 TohGlobalK2StableMonitoringThresholdControlRegAddr(Tha60210031SdhLine self)
    {
    return ThaModuleOcnTohGlobalK2StableMonitoringThresholdControlRegAddr(ModuleOcn((AtSdhLine)self));
    }

static uint32 ChannelOffset(Tha60210031SdhLine self)
    {
    return (AtChannelIdGet((AtChannel)self) * 256UL) + ThaModuleOcnBaseAddress(ModuleOcn((AtSdhLine)self));
    }

static void OverrideTha60210031SdhLine(AtSdhLine self)
    {
    Tha60210031SdhLine line = (Tha60210031SdhLine)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210031SdhLineMethods = mMethodsGet(line);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210031SdhLineOverride, mMethodsGet(line), sizeof(m_Tha60210031SdhLineOverride));

        mMethodOverride(m_Tha60210031SdhLineOverride, ChannelOffset);
        mMethodOverride(m_Tha60210031SdhLineOverride, B1CounterRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, B2CounterRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, ReiCounterRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, B1BlockCounterRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, B2BlockCounterRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, ReiBlockCounterRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, RxLineAlarmCurrentStatusRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, RxLineAlarmInterruptEnableControlRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, RxLineAlarmInterruptStatusRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, TohS1MonitoringStatusRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, TohK1MonitoringStatusRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, TohK2MonitoringStatusRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, TohGlobalS1StableMonitoringThresholdControlRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, TohGlobalK1StableMonitoringThresholdControlRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, TohGlobalK2StableMonitoringThresholdControlRegAddr);
        }

    mMethodsSet(line, &m_Tha60210031SdhLineOverride);
    }

static void OverrideThaSdhLine(AtSdhLine self)
    {
    ThaSdhLine line = (ThaSdhLine)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaSdhLineMethods = mMethodsGet(line);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhLineOverride, m_ThaSdhLineMethods, sizeof(m_ThaSdhLineOverride));

        mMethodOverride(m_ThaSdhLineOverride, DefaultOffset);
        }

    mMethodsSet(line, &m_ThaSdhLineOverride);
    }

static void OverrideAtSdhLine(AtSdhLine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhLineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhLineOverride, mMethodsGet(self), sizeof(m_AtSdhLineOverride));

        mMethodOverride(m_AtSdhLineOverride, HideLineGet);
        }

    mMethodsSet(self, &m_AtSdhLineOverride);
    }

static void OverrideAtChannel(AtSdhLine self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, InterruptEnable);
        mMethodOverride(m_AtChannelOverride, InterruptDisable);
        mMethodOverride(m_AtChannelOverride, InterruptIsEnabled);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtSdhLine self)
    {
    OverrideTha60210031SdhLine(self);
    OverrideThaSdhLine(self);
    OverrideAtSdhLine(self);
    OverrideAtChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081SdhFacePlateLine);
    }

static AtSdhLine ObjectInit(AtSdhLine self, uint32 channelId, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022SdhFacePlateLineObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhLine Tha60290081SdhFacePlateLineNew(uint32 lineId, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhLine newLine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newLine == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newLine, lineId, module);
    }

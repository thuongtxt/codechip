/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60290081ModuleSdh.c
 *
 * Created Date: Jun 15, 2019
 *
 * Description : 60290081 Module SDH implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/sdh/AtSdhLineInternal.h"
#include "../../../default/man/ThaDevice.h"
#include "../../../default/ocn/ThaModuleOcn.h"
#include "../ocn/Tha60290081ModuleOcnReg.h"
#include "../poh/Tha60290081ModulePoh.h"
#include "Tha60290081ModuleSdhInternal.h"
#include "Tha60290081ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define cIteratorMask(_idx)    (cBit0 << (_idx))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleSdhMethods          m_AtModuleSdhOverride;
static tThaModuleSdhMethods         m_ThaModuleSdhOverride;
static tTha60210011ModuleSdhMethods m_Tha60210011ModuleSdhOverride;
static tTha60290021ModuleSdhMethods m_Tha60290021ModuleSdhOverride;

/* Save super implementation */
static const tAtModuleSdhMethods          *m_AtModuleSdhMethods          = NULL;
static const tThaModuleSdhMethods         *m_ThaModuleSdhMethods         = NULL;
static const tTha60290021ModuleSdhMethods *m_Tha60290021ModuleSdhMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleOcn ModuleOcn(ThaModuleSdh self)
    {
    return (ThaModuleOcn)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModuleOcn);
    }

static void LineInterruptProcess(ThaModuleSdh self, uint32 tohIntr, AtHal hal)
    {
    static uint8 cIgnoreSlice = 0;
    uint32 baseAddress = ThaModuleOcnBaseAddress(ModuleOcn(self));
    uint32 intrLineVal = AtHalRead(hal, (uint32)cAf6Reg_tohintperline + baseAddress);
    uint32 intrLineMask = AtHalRead(hal, (uint32)cAf6Reg_tohintperlineenctl + baseAddress);
    uint32 numFaceplateLines = Tha60290021ModuleSdhNumFaceplateLines((AtModuleSdh)self);
    uint8 line_i;
    AtUnused(tohIntr);

    intrLineVal &= intrLineMask;
    for (line_i = 0; line_i < numFaceplateLines; line_i++)
        {
        if (intrLineVal & cIteratorMask(line_i))
            {
            AtSdhLine line = ThaModuleSdhLineFromHwIdGet(self, cAtModuleSdh, cIgnoreSlice, line_i);
            uint8 lineId = (uint8)AtChannelIdGet((AtChannel)self);
            AtSdhLineInterruptProcess(line, lineId, hal);
            }
        }
    }

static AtSdhLine LineFromHwIdGet(ThaModuleSdh self, eAtModule phyModule, uint8 sliceId, uint8 lineId)
    {
    if (phyModule == cAtModuleSdh)
        return AtModuleSdhLineGet((AtModuleSdh)self, lineId);

    return m_ThaModuleSdhMethods->LineFromHwIdGet(self, phyModule, sliceId, lineId);
    }

static eAtRet PohMonitorDefaultSet(Tha60210011ModuleSdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return Tha60290081ModulePohMonitoringDefaultSet((ThaModulePoh)AtDeviceModuleGet(device, cThaModulePoh));
    }

static AtSdhChannel Tu3VcObjectCreate(Tha60210011ModuleSdh self, uint8 channelType, uint8 channelId)
    {
    return (AtSdhChannel)Tha60290081SdhTerminatedLineTu3VcNew(channelId, channelType, (AtModuleSdh)self);
    }

static AtSdhVc TerminatedLineAuVcCreate(Tha60290021ModuleSdh self, uint32 channelId, uint8 channelType)
    {
    return Tha60290081SdhTerminatedLineAuVcNew(channelId, channelType, (AtModuleSdh)self);
    }

static AtSdhLine FaceplateLineCreate(Tha60290021ModuleSdh self, uint8 lineId)
    {
    return Tha60290081SdhFacePlateLineNew(lineId, (AtModuleSdh)self);
    }

static AtSdhPath FaceplateAuPathFromHwIdGet(Tha60290021ModuleSdh self, uint8 slice48, uint8 stsInSlice)
    {
    AtSdhLine faceplateLine;
    uint8 faceplateId;

    /* Only first line supports STM64 */
    faceplateLine = AtModuleSdhLineGet((AtModuleSdh)self, 0);
    if (faceplateLine == NULL)
        return NULL;

    if (AtSdhLineRateGet(faceplateLine) == cAtSdhLineRateStm64)
        return (AtSdhPath)Tha60290021SdhLineStm64AuPathFromHwStsGet(faceplateLine, slice48, stsInSlice);

    /* There are 4 possible-STM16 line IDs such as 0, 4, 8 and 12 */
    faceplateId = (uint8)(slice48 * 4);
    faceplateLine = AtModuleSdhLineGet((AtModuleSdh)self, faceplateId);
    if (faceplateLine == NULL)
        return NULL;

    if (AtSdhLineRateGet(faceplateLine) == cAtSdhLineRateStm16)
        return (AtSdhPath)Tha60210011ModuleSdhAuFromHwStsGet(faceplateLine, stsInSlice);
    else
        {
        uint8 sts12IndexInSts48 = (uint8)((stsInSlice % 16) / 4);
        uint8 sts1IndexInSts12;

        faceplateId = (uint8)(faceplateId + sts12IndexInSts48);
        faceplateLine = AtModuleSdhLineGet((AtModuleSdh)self, faceplateId);
        if (faceplateLine == NULL)
            return NULL;

        /* STM-4/STM-1/STM-0 lookup */
        sts1IndexInSts12 = (uint8)((stsInSlice % 4) + (stsInSlice / 16) * 4);
        return (AtSdhPath)Tha60290021SdhLineSubStm16AuPathFromHwStsGet(faceplateLine, sts1IndexInSts12);
        }
    }

static void OverrideTha60290021ModuleSdh(AtModuleSdh self)
    {
    Tha60290021ModuleSdh sdh = (Tha60290021ModuleSdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290021ModuleSdhMethods = mMethodsGet(sdh);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021ModuleSdhOverride, mMethodsGet(sdh), sizeof(m_Tha60290021ModuleSdhOverride));

        mMethodOverride(m_Tha60290021ModuleSdhOverride, TerminatedLineAuVcCreate);
        mMethodOverride(m_Tha60290021ModuleSdhOverride, FaceplateLineCreate);
        mMethodOverride(m_Tha60290021ModuleSdhOverride, FaceplateAuPathFromHwIdGet);
        }

    mMethodsSet(sdh, &m_Tha60290021ModuleSdhOverride);
    }

static void OverrideTha60210011ModuleSdh(AtModuleSdh self)
    {
    Tha60210011ModuleSdh sdh = (Tha60210011ModuleSdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModuleSdhOverride, mMethodsGet(sdh), sizeof(m_Tha60210011ModuleSdhOverride));

        mMethodOverride(m_Tha60210011ModuleSdhOverride, PohMonitorDefaultSet);
        mMethodOverride(m_Tha60210011ModuleSdhOverride, Tu3VcObjectCreate);
        }

    mMethodsSet(sdh, &m_Tha60210011ModuleSdhOverride);
    }

static void OverrideThaModuleSdh(AtModuleSdh self)
    {
    ThaModuleSdh sdhModule = (ThaModuleSdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleSdhMethods = mMethodsGet(sdhModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleSdhOverride, m_ThaModuleSdhMethods, sizeof(m_ThaModuleSdhOverride));

        mMethodOverride(m_ThaModuleSdhOverride, LineInterruptProcess);
        mMethodOverride(m_ThaModuleSdhOverride, LineFromHwIdGet);
        }

    mMethodsSet(sdhModule, &m_ThaModuleSdhOverride);
    }

static void OverrideAtModuleSdh(AtModuleSdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleSdhMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleSdhOverride, m_AtModuleSdhMethods, sizeof(m_AtModuleSdhOverride));

        }

    mMethodsSet(self, &m_AtModuleSdhOverride);
    }

static void Override(AtModuleSdh self)
    {
    OverrideTha60290021ModuleSdh(self);
    OverrideTha60210011ModuleSdh(self);
    OverrideThaModuleSdh(self);
    OverrideAtModuleSdh(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081ModuleSdh);
    }

AtModuleSdh Tha60290081ModuleSdhObjectInit(AtModuleSdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60291022ModuleSdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSdh Tha60290081ModuleSdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290081ModuleSdhObjectInit(newModule, device);
    }

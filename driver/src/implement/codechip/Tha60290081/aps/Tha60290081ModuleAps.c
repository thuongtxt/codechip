/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APS
 *
 * File        : Tha60290081ModuleAps.c
 *
 * Created Date: Sep 16, 2019
 *
 * Description : 60290081 Module APS implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290081ModuleApsInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleApsMethods          m_AtModuleApsOverride;

/* Save super implementation */
static const tAtModuleApsMethods    *m_AtModuleApsMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxNumUpsrEngines(AtModuleAps self)
    {
    AtUnused(self);
    return 192;
    }

static uint32 MaxNumSelectors(AtModuleAps self)
    {
    AtUnused(self);
    return 576; /* 192 * 3 */
    }

static void OverrideAtModuleAps(AtModuleAps self)
    {

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleApsMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleApsOverride, m_AtModuleApsMethods, sizeof(m_AtModuleApsOverride));

        mMethodOverride(m_AtModuleApsOverride, MaxNumUpsrEngines);
        mMethodOverride(m_AtModuleApsOverride, MaxNumSelectors);
        }

    mMethodsSet(self, &m_AtModuleApsOverride);
    }

static void Override(AtModuleAps self)
    {
    OverrideAtModuleAps(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081ModuleAps);
    }

static AtModuleAps ObjectInit(AtModuleAps self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022ModuleApsObjectInit(self, device) == NULL)
        return NULL;

    /* Setup Class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleAps Tha60290081ModuleApsNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleAps object = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (object == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(object, device);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : Tha60290081ModuleApsInternal.h
 * 
 * Created Date: Sep 16, 2019
 *
 * Description : 60290081 Module APS representation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290081MODULEAPSINTERNAL_H_
#define _THA60290081MODULEAPSINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290022/aps/Tha60290022ModuleApsInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290081ModuleAps
    {
    tTha60290022ModuleAps super;
    }tTha60290081ModuleAps;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60290081MODULEAPSINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MPEG
 * 
 * File        : Tha60290081ModuleMpegReg.h
 * 
 * Created Date: Aug 27, 2019
 *
 * Description : This file contain all constant definitions of MPEG
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290081MODULEMPEGREG_H_
#define _THA60290081MODULEMPEGREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name   : Select channel ID
Reg Addr   : 0x40000
Reg Formula: 0x40000
    Where  :
Reg Desc   :


------------------------------------------------------------------------------*/
#define cAf6Reg_chanlel_pen_Base                                                                       0x40000

/*--------------------------------------
BitField Name: chanel_id
BitField Type: R/W
BitField Desc: Config channel moniter for regs below
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_chanlel_pen_chanel_id_Mask                                                               cBit31_0
#define cAf6_chanlel_pen_chanel_id_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Input write pkt rate
Reg Addr   : 0x40003
Reg Formula: 0x40003
    Where  :
Reg Desc   :


------------------------------------------------------------------------------*/
#define cAf6Reg_maxwr_pen_Base                                                                         0x40003

/*--------------------------------------
BitField Name: maxwr
BitField Type: R/W
BitField Desc: Maximum PKT Rate/s PDA write  MPEG,              wr 0 to clr &
re-update
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_maxwr_pen_maxwr_Mask                                                                     cBit31_0
#define cAf6_maxwr_pen_maxwr_Shift                                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Req read pkt rate
Reg Addr   : 0x40004
Reg Formula: 0x40004
    Where  :
Reg Desc   :


------------------------------------------------------------------------------*/
#define cAf6Reg_maxrd_pen_Base                                                                         0x40004

/*--------------------------------------
BitField Name: max_req_rd
BitField Type: R/W
BitField Desc: Total Maximum PKT Rate/s PDA req  MPEG,  wr 0 to clr & re-update
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_maxrd_pen_max_req_rd_Mask                                                                cBit31_0
#define cAf6_maxrd_pen_max_req_rd_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Req read pkt rate per chanel
Reg Addr   : 0x40005
Reg Formula: 0x40005
    Where  :
Reg Desc   :


------------------------------------------------------------------------------*/
#define cAf6Reg_maxrd_chanel_Req_Base                                                                  0x40005

/*--------------------------------------
BitField Name: max_req_per_chanel
BitField Type: R/W
BitField Desc: Maximum PKT Rate/s PDA req per chanel MPEG,              wr 0 to
clr & re-update
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_maxrd_chanel_Req_max_req_per_chanel_Mask                                                 cBit31_0
#define cAf6_maxrd_chanel_Req_max_req_per_chanel_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : pkt rate MPEG out
Reg Addr   : 0x40006
Reg Formula: 0x40006
    Where  :
Reg Desc   :


------------------------------------------------------------------------------*/
#define cAf6Reg_max_mpeg_out_Base                                                                      0x40006

/*--------------------------------------
BitField Name: max_mpeg_out
BitField Type: R/W
BitField Desc: Maximum PKT Rate/s MPEG out to PDA,              wr 0 to clr &
re-update
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_max_mpeg_out_max_mpeg_out_Mask                                                           cBit31_0
#define cAf6_max_mpeg_out_max_mpeg_out_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : MPEG req wr DDR
Reg Addr   : 0x4000c
Reg Formula: 0x4000c
    Where  :
Reg Desc   :


------------------------------------------------------------------------------*/
#define cAf6Reg_max_ddr_req_wr_Base                                                                    0x4000c

/*--------------------------------------
BitField Name: max_mpeg_ddr_wr
BitField Type: R/W
BitField Desc: Maximum PKT Rate/s MPEG req wr DDR = value x2 ,          wr 0 to
clr & re-update
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_max_ddr_req_wr_max_mpeg_ddr_wr_Mask                                                      cBit31_0
#define cAf6_max_ddr_req_wr_max_mpeg_ddr_wr_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : MPEG req rd DDR
Reg Addr   : 0x4000d
Reg Formula: 0x4000d
    Where  :
Reg Desc   :


------------------------------------------------------------------------------*/
#define cAf6Reg_max_ddr_req_rd_Base                                                                    0x4000d

/*--------------------------------------
BitField Name: max_mpeg_ddr_rd
BitField Type: R/W
BitField Desc: Maximum PKT Rate/s MPEG req rd DDR = value x2 ,          wr 0 to
clr & re-update
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_max_ddr_req_rd_max_mpeg_ddr_rd_Mask                                                      cBit31_0
#define cAf6_max_ddr_req_rd_max_mpeg_ddr_rd_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : DDR vld wr
Reg Addr   : 0x4000e
Reg Formula: 0x4000e
    Where  :
Reg Desc   :


------------------------------------------------------------------------------*/
#define cAf6Reg_max_mpeg_vl_wr_Base                                                                    0x4000e

/*--------------------------------------
BitField Name: max_mpeg_ddr_vldwr
BitField Type: R/W
BitField Desc: Maximum PKT Rate/s DDR return wr  = value x2 ,           wr 0 to
clr & re-update
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_max_mpeg_vl_wr_max_mpeg_ddr_vldwr_Mask                                                   cBit31_0
#define cAf6_max_mpeg_vl_wr_max_mpeg_ddr_vldwr_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : DDR vld rd
Reg Addr   : 0x4000f
Reg Formula: 0x4000f
    Where  :
Reg Desc   :


------------------------------------------------------------------------------*/
#define cAf6Reg_max_mpeg_vl_rd_Base                                                                    0x4000f

/*--------------------------------------
BitField Name: max_mpeg_ddr_vldrd
BitField Type: R/W
BitField Desc: Maximum PKT Rate/s DDR return rd  = value x2 ,           wr 0 to
clr & re-update
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_max_mpeg_vl_rd_max_mpeg_ddr_vldrd_Mask                                                   cBit31_0
#define cAf6_max_mpeg_vl_rd_max_mpeg_ddr_vldrd_Shift                                                         0

/*------------------------------------------------------------------------------
Reg Name   : External/Internal Queue Lenght
Reg Addr   : 0x40022
Reg Formula: 0x40022
    Where  :
Reg Desc   :


------------------------------------------------------------------------------*/
#define cAf6Reg_max_tenge_req_Base                                                                     0x40022

/*--------------------------------------
BitField Name: max_tenge_req
BitField Type: R/W
BitField Desc: Maximum  Rate/s tenge vld  = value x64 ( bps) ,          wr 0 to
clr & re-update
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_max_tenge_req_max_tenge_req_Mask                                                         cBit31_0
#define cAf6_max_tenge_req_max_tenge_req_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : External/Internal Queue Lenght
Reg Addr   : 0x40023
Reg Formula: 0x40023
    Where  :
Reg Desc   :


------------------------------------------------------------------------------*/
#define cAf6Reg_max_ge_req_Base                                                                        0x40023

/*--------------------------------------
BitField Name: max_ge_req
BitField Type: R/W
BitField Desc: Maximum  Rate/s tenge vld  = value x64 ( bps) ,          wr 0 to
clr & re-update
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_max_ge_req_max_ge_req_Mask                                                               cBit31_0
#define cAf6_max_ge_req_max_ge_req_Shift                                                                     0

#ifdef __cplusplus
}
#endif
#endif /* _THA60290081MODULEMPEGREG_H_ */


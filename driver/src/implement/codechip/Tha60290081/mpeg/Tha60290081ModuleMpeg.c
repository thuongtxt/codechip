/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MPEG
 *
 * File        : Tha60290081ModuleMpeg.c
 *
 * Created Date: Jul 31, 2019
 *
 * Description : 60290081 Module MPEG implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/common/AtChannelInternal.h"
#include "../../Tha60210012/mpeg/Tha60210012ModuleMpegInternal.h"
#include "Tha60290081ModuleMpeg.h"
#include "Tha60290081ModuleMpegReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290081ModuleMpeg
    {
    tTha60210012ModuleMpeg super;
    }tTha60290081ModuleMpeg;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods              m_AtModuleOverride;
static tTha60210012ModuleMpegMethods m_Tha60210012ModuleMpegOverride;

/* Save super implementation */
static const tAtModuleMethods * m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(Tha60210012ModuleMpeg self)
    {
    AtUnused(self);
    return 0x1D00000UL;
    }

static uint32 MpegSpeedGetAndClear(Tha60290081ModuleMpeg self, uint32 localAddress, eBool clear)
    {
    uint32 regAddr = localAddress + Tha60210012ModuleMpegBaseAddress((Tha60210012ModuleMpeg)self);
    uint32 value = mModuleHwRead(self, regAddr);

    if (clear)
        mModuleHwWrite(self, regAddr, 0);

    return value;
    }

static eAtRet EthFlowSelect(Tha60290081ModuleMpeg self, AtEthFlow flow)
    {
    uint32 regAddr = cAf6Reg_chanlel_pen_Base + Tha60210012ModuleMpegBaseAddress((Tha60210012ModuleMpeg)self);
    mModuleHwWrite(self, regAddr, AtChannelHwIdGet((AtChannel)flow));

    return cAtOk;
    }

static void PrintSpeedInfo(const char* title, uint32 value)
    {
    AtPrintc(cSevNormal, "    %-30s: %d (packets/s)\r\n", title, value);
    }

static eAtRet EthFlowSpeedDebug(Tha60290081ModuleMpeg self)
    {
    AtPrintc(cSevInfo, "\r\nMPEG debug information\r\n");
    PrintSpeedInfo("MAX PDA WRITE MPEG", MpegSpeedGetAndClear(self, cAf6Reg_maxwr_pen_Base, cAtTrue));
    PrintSpeedInfo("MAX PDA READ MPEG", MpegSpeedGetAndClear(self, cAf6Reg_maxrd_pen_Base, cAtTrue));
    PrintSpeedInfo("MAX PDA REQ CHANNEL MPEG", MpegSpeedGetAndClear(self, cAf6Reg_maxrd_chanel_Req_Base, cAtTrue));
    PrintSpeedInfo("MAX MPEG OUT", MpegSpeedGetAndClear(self, cAf6Reg_max_mpeg_out_Base, cAtTrue));
    PrintSpeedInfo("MAX DDR MPEG REQ WRITE", MpegSpeedGetAndClear(self, cAf6Reg_max_ddr_req_wr_Base, cAtTrue));
    PrintSpeedInfo("MAX DDR MPEG REQ READ", MpegSpeedGetAndClear(self, cAf6Reg_max_ddr_req_rd_Base, cAtTrue));
    PrintSpeedInfo("MAX DDR MPEG VALID WRITE", MpegSpeedGetAndClear(self, cAf6Reg_max_mpeg_vl_wr_Base, cAtTrue));
    PrintSpeedInfo("MAX DDR MPEG VALID READ", MpegSpeedGetAndClear(self, cAf6Reg_max_mpeg_vl_rd_Base, cAtTrue));
    PrintSpeedInfo("MAX VALID TENGE REQUEST", MpegSpeedGetAndClear(self, cAf6Reg_max_tenge_req_Base, cAtTrue));
    PrintSpeedInfo("MAX VALID GE REQUEST", MpegSpeedGetAndClear(self, cAf6Reg_max_ge_req_Base, cAtTrue));

    return cAtOk;
    }

static eAtRet EthFlowDebug(Tha60290081ModuleMpeg self, AtEthFlow flow)
    {
    eAtRet ret;
    ret  = EthFlowSelect(self, flow);
    ret |= EthFlowSpeedDebug(self);
    return ret;
    }

static void StatusClear(AtModule self)
    {
    uint8 cntId;
    static const char* cMpegQueueCounterString[] = {
                                      "Total good packet enqueue",
                                      "CLA Declare Drop",
                                      "MPEG enable for PDA read",
                                      "PDA request read MPEG",
                                      "MPEG return valid to PDA",
                                      "MPEG packet valid to PDA",
                                      "PDA discard full",
                                      "QMXFF request Write DDR",
                                      "QMXFF request Read DDR",
                                      "DDR return ACK to MPEG",
                                      "DDR return Valid Write to MPEG",
                                      "DDR return Valid Read to MPEG"
    };

    for (cntId = 0; cntId < mCount(cMpegQueueCounterString); cntId++)
        Tha60210012ModuleMpegDebugCounterGet((Tha60210012ModuleMpeg)self, cntId, cAtTrue);

    m_AtModuleMethods->StatusClear(self);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, StatusClear);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void OverrideTha60210012ModuleMpeg(AtModule self)
    {
    Tha60210012ModuleMpeg module = (Tha60210012ModuleMpeg)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210012ModuleMpegOverride, mMethodsGet(module), sizeof(m_Tha60210012ModuleMpegOverride));
        mMethodOverride(m_Tha60210012ModuleMpegOverride, BaseAddress);
        }

    mMethodsSet(module, &m_Tha60210012ModuleMpegOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideTha60210012ModuleMpeg(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081ModuleMpeg);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012ModuleMpegObjectInit(self, device) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60290081ModuleMpegNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

eAtRet Tha60290081ModuleMpegEthFlowDebug(Tha60290081ModuleMpeg self, AtEthFlow flow)
    {
    if (self)
        return EthFlowDebug(self, flow);
    return cAtErrorNullPointer;
    }

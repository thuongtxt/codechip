/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MPEG
 * 
 * File        : Tha60290081ModuleMpeg.h
 * 
 * Created Date: Jul 31, 2019
 *
 * Description : 60290081 module MPEG
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290081MODULEMPEG_H_
#define _THA60290081MODULEMPEG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290081ModuleMpeg *Tha60290081ModuleMpeg;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60290081ModuleMpegNew(AtDevice device);

/* Debugging */
eAtRet Tha60290081ModuleMpegEthFlowDebug(Tha60290081ModuleMpeg self, AtEthFlow flow);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290081MODULEMPEG_H_ */


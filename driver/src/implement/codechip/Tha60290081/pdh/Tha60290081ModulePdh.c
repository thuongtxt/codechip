/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60290081ModulePdh.c
 *
 * Created Date: Jun 15, 2019
 *
 * Description : 60290081 module PDH implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60291011/pdh/Tha60291011ModulePdh.h"
#include "Tha60290081ModulePdhInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods             m_AtModuleOverride;
static tThaModulePdhMethods         m_ThaModulePdhOverride;

/* Save super implementation */
static const tThaModulePdhMethods  *m_ThaModulePdhMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 TimeslotDefaultOffset(ThaModulePdh self, ThaPdhDe1 de1)
    {
    return Tha60291011ModulePdhTimeslotDefaultOffset(self, de1);
    }

static uint8 NumSlices(ThaModulePdh self)
    {
    AtUnused(self);
    return 4;
    }

static eBool HasRegister(AtModule self, uint32 localAddress)
    {
    AtUnused(self);
    if (mInRange(localAddress, 0x1000000, 0x13FFFFF))
        return cAtTrue;
    return cAtFalse;
    }

static void OverrideAtModule(AtModulePdh self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(module), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, HasRegister);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideThaModulePdh(AtModulePdh self)
    {
    ThaModulePdh module = (ThaModulePdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePdhMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdhOverride, m_ThaModulePdhMethods, sizeof(m_ThaModulePdhOverride));

        mMethodOverride(m_ThaModulePdhOverride, TimeslotDefaultOffset);
        mMethodOverride(m_ThaModulePdhOverride, NumSlices);
        }

    mMethodsSet(module, &m_ThaModulePdhOverride);
    }

static void Override(AtModulePdh self)
    {
    OverrideAtModule(self);
    OverrideThaModulePdh(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081ModulePdh);
    }

AtModulePdh Tha60290081ModulePdhObjectInit(AtModulePdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022ModulePdhV3ObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePdh Tha60290081ModulePdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290081ModulePdhObjectInit(newModule, device);
    }

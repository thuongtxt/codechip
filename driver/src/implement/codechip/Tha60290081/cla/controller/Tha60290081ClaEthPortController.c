/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60290081ClaEthPortController.c
 *
 * Created Date: Jul 30, 2019
 *
 * Description : 60290081 CLA Ethernet port controller implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/man/ThaDevice.h"
#include "../../../Tha60210011/cla/Tha60210011ModuleCla.h"
#include "../../../Tha60210051/cla/controllers/Tha60210051ClaPwEthPortControllerInternal.h"
#include "../Tha60290081ModuleClaReg.h"
#include "Tha60290081ClaEthPortControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaClaControllerMethods        m_ThaClaControllerOverride;
static tThaClaEthPortControllerMethods m_ThaClaEthPortControllerOverride;

/* Save super implementation */
static const tThaClaEthPortControllerMethods *m_ThaClaEthPortControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleCla ModuleCla(ThaClaEthPortController self)
    {
    return (ThaModuleCla)ThaClaControllerModuleGet((ThaClaController)self);
    }

static uint32 BaseAddress(ThaClaEthPortController self)
    {
    return Tha60210011ModuleClaBaseAddress(ModuleCla(self));
    }

static uint32 GlbMruControl(ThaClaEthPortController self)
    {
    return BaseAddress(self) + cAf6Reg_upen_mru_val_Base;
    }

static uint32 MaxPacketSizeGet(ThaClaEthPortController self, AtEthPort port)
    {
    uint32 regAddr = GlbMruControl(self);
    uint32 regVal = mChannelHwRead(port, regAddr, cThaModuleCla);
    return mRegField(regVal, cAf6_upen_mru_val_cfg_mru_val_);
    }

static eAtRet MaxPacketSizeSet(ThaClaEthPortController self, AtEthPort port, uint32 maxPacketSize)
    {
    uint32 regAddr = GlbMruControl(self);
    uint32 regVal;

    if (maxPacketSize > cAf6_upen_mru_val_cfg_mru_val_Mask)
        return cAtErrorOutOfRangParm;

    regVal = mChannelHwRead(port, regAddr, cThaModuleCla);
    mRegFieldSet(regVal, cAf6_upen_mru_val_cfg_mru_en_, 1);
    mRegFieldSet(regVal, cAf6_upen_mru_val_cfg_mru_val_, maxPacketSize);
    mChannelHwWrite(port, regAddr, regVal, cThaModuleCla);

    return cAtOk;
    }

static eAtRet MinPacketSizeSet(ThaClaEthPortController self, AtEthPort port, uint32 minPacketSize)
    {
    /* TODO: implement me */
    return m_ThaClaEthPortControllerMethods->MinPacketSizeSet(self, port, minPacketSize);
    }

static uint32 MinPacketSizeGet(ThaClaEthPortController self, AtEthPort port)
    {
    /* TODO: implement me */
    return m_ThaClaEthPortControllerMethods->MinPacketSizeGet(self, port);
    }

static uint32 PortOffset(AtEthPort port)
    {
    return AtChannelIdGet((AtChannel)port);
    }

static uint32 PsnDAMacCheckingControl(ThaClaEthPortController self, AtEthPort port)
    {
    return BaseAddress(self) + cAf6Reg_upen_dachkcam_Base + PortOffset(port);
    }

static eAtRet MacSet(ThaClaEthPortController self, AtEthPort port, uint8 *macAddr)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address = PsnDAMacCheckingControl(self, port);

    mChannelHwLongRead(port, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    longRegVal[1]  = (uint32)(macAddr[5] << 9);
    longRegVal[1] |= (uint32)((macAddr[4] << 1) | (macAddr[3] >> 7));

    longRegVal[0]  = (longRegVal[0] & cBit0);
    longRegVal[0] |= (uint32)(macAddr[3] << 25);
    longRegVal[0] |= (uint32)(macAddr[2] << 17);
    longRegVal[0] |= (uint32)(macAddr[1] << 9);
    longRegVal[0] |= (uint32)(macAddr[0] << 1);
    mChannelHwLongWrite(port, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static eAtRet MacGet(ThaClaEthPortController self, AtEthPort port, uint8 *macAddr)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address = PsnDAMacCheckingControl(self, port);

    mChannelHwLongRead(port, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    macAddr[5] = (uint8)(longRegVal[1] >> 9);
    macAddr[4] = (uint8)(longRegVal[1] >> 1);

    macAddr[3] = (uint8)((longRegVal[1] << 7) | (longRegVal[0] >> 25));
    macAddr[2] = (uint8)(longRegVal[0] >> 17);
    macAddr[1] = (uint8)(longRegVal[0] >> 9);
    macAddr[0] = (uint8)(longRegVal[0] >> 1);

    return cAtOk;
    }

static uint32 IncomPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    AtUnused(self);
    AtUnused(port);
    AtUnused(r2c);
    return 0;
    }

static uint32 IncombyteRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    AtUnused(self);
    AtUnused(port);
    AtUnused(r2c);
    return 0;
    }

static uint32 EthPktoversizeRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    AtUnused(self);
    AtUnused(port);
    AtUnused(r2c);
    return 0;
    }

static uint32 EthPktundersizeRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    AtUnused(self);
    AtUnused(port);
    AtUnused(r2c);
    return 0;
    }

static uint32 EthPktLensmallerthan64bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    AtUnused(self);
    AtUnused(port);
    AtUnused(r2c);
    return 0;
    }

static uint32 EthPktLenfrom65to127bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    AtUnused(self);
    AtUnused(port);
    AtUnused(r2c);
    return 0;
    }

static uint32 EthPktLenfrom128to255bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    AtUnused(self);
    AtUnused(port);
    AtUnused(r2c);
    return 0;
    }

static uint32 EthPktLenfrom256to511bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    AtUnused(self);
    AtUnused(port);
    AtUnused(r2c);
    return 0;
    }

static uint32 EthPktLenfrom512to1024bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    AtUnused(self);
    AtUnused(port);
    AtUnused(r2c);
    return 0;
    }

static uint32 EthPktLenfrom1025to1528bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    AtUnused(self);
    AtUnused(port);
    AtUnused(r2c);
    return 0;
    }

static uint32 EthPktLenfrom1529to2047bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    AtUnused(self);
    AtUnused(port);
    AtUnused(r2c);
    return 0;
    }

static uint32 EthPktJumboLenRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    AtUnused(self);
    AtUnused(port);
    AtUnused(r2c);
    return 0;
    }

static uint32 UnicastRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    AtUnused(self);
    AtUnused(port);
    AtUnused(r2c);
    return 0;
    }

static uint32 BcastRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    AtUnused(self);
    AtUnused(port);
    AtUnused(r2c);
    return 0;
    }

static uint32 McastRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    AtUnused(self);
    AtUnused(port);
    AtUnused(r2c);
    return 0;
    }

static uint32 PhysicalErrorRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    AtUnused(self);
    AtUnused(port);
    AtUnused(r2c);
    return 0;
    }

static uint32 EthPktFcsErrRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    AtUnused(self);
    AtUnused(port);
    AtUnused(r2c);
    return 0;
    }

static uint32 EthPktDscdRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    uint32 r2cOffset = (r2c) ? 0x1U : 0x0U;
    uint32 offset = BaseAddress(self) + r2cOffset;
    return mChannelHwRead(port, cAf6Reg_upen_cntdis0 + offset, cThaModuleCla);
    }

static eAtRet EthPortMacCheckEnable(ThaClaEthPortController self, AtEthPort port, eBool enable)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address = PsnDAMacCheckingControl(self, port);

    mChannelHwLongRead(port, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mRegFieldSet(longRegVal[0], cAf6_upen_dachkcam_dachk_vld0_, (enable) ? 1 : 0);
    mChannelHwLongWrite(port, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static eBool EthPortMacCheckIsEnabled(ThaClaEthPortController self, AtEthPort port)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address = PsnDAMacCheckingControl(self, port);

    mChannelHwLongRead(port, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    return mRegField(longRegVal[0], cAf6_upen_dachkcam_dachk_vld0_) ? cAtTrue : cAtFalse;
    }

static uint32 EthPortServiceControl(ThaClaController self, AtChannel channel)
    {
    return BaseAddress((ThaClaEthPortController)self) + cAf6Reg_upen_portser_Base + PortOffset((AtEthPort)channel);
    }

static eAtRet ChannelEnable(ThaClaController self, AtChannel channel, eBool enable)
    {
    uint32 regAddr, regVal = 0;

    regAddr = EthPortServiceControl(self, channel);
    mRegFieldSet(regVal, cAf6_upen_portser_outen_, (enable) ? 1 : 0);
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleCla);

    return cAtOk;
    }

static eBool ChannelIsEnabled(ThaClaController self, AtChannel channel)
    {
    uint32 regAddr = EthPortServiceControl(self, channel);
    uint32 regVal = mChannelHwRead(channel, regAddr, cThaModuleCla);
    return mRegField(regVal, cAf6_upen_portser_outen_) ? cAtTrue : cAtFalse;
    }

static void OverrideThaClaEthPortController(ThaClaEthPortController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaClaEthPortControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaEthPortControllerOverride, mMethodsGet(self), sizeof(m_ThaClaEthPortControllerOverride));

        mMethodOverride(m_ThaClaEthPortControllerOverride, MaxPacketSizeSet);
        mMethodOverride(m_ThaClaEthPortControllerOverride, MaxPacketSizeGet);
        mMethodOverride(m_ThaClaEthPortControllerOverride, MinPacketSizeSet);
        mMethodOverride(m_ThaClaEthPortControllerOverride, MinPacketSizeGet);
        mMethodOverride(m_ThaClaEthPortControllerOverride, MacSet);
        mMethodOverride(m_ThaClaEthPortControllerOverride, MacGet);
        mMethodOverride(m_ThaClaEthPortControllerOverride, IncomPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, IncombyteRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktoversizeRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktundersizeRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktLensmallerthan64bytesRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktLenfrom65to127bytesRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktLenfrom128to255bytesRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktLenfrom256to511bytesRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktLenfrom512to1024bytesRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktLenfrom1025to1528bytesRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktLenfrom1529to2047bytesRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktJumboLenRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, BcastRxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, McastRxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, UnicastRxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, PhysicalErrorRxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktFcsErrRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktDscdRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPortMacCheckEnable);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPortMacCheckIsEnabled);
        }

    mMethodsSet(self, &m_ThaClaEthPortControllerOverride);
    }

static void OverrideThaClaController(ThaClaEthPortController self)
    {
    ThaClaController claController = (ThaClaController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaControllerOverride, mMethodsGet(claController), sizeof(m_ThaClaControllerOverride));

        mMethodOverride(m_ThaClaControllerOverride, ChannelEnable);
        mMethodOverride(m_ThaClaControllerOverride, ChannelIsEnabled);
        }

    mMethodsSet(claController, &m_ThaClaControllerOverride);
    }

static void Override(ThaClaEthPortController self)
    {
    OverrideThaClaEthPortController(self);
    OverrideThaClaController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081ClaEthPortController);
    }

ThaClaEthPortController Tha60290081ClaEthPortControllerObjectInit(ThaClaEthPortController self, ThaModuleCla cla)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051ClaPwEthPortControllerObjectInit(self, cla) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaClaEthPortController Tha60290081ClaEthPortControllerNew(ThaModuleCla cla)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaClaEthPortController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60290081ClaEthPortControllerObjectInit(newController, cla);
    }

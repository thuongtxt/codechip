/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60290081ClaPwController.c
 *
 * Created Date: Jul 7, 2019
 *
 * Description : CLA Controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/man/ThaDevice.h"
#include "../../../Tha60210011/cla/Tha60210011ModuleCla.h"
#include "../../../Tha60290021/cla/controller/Tha60290021ClaPwControllerInternal.h"
#include "../../../Tha60210011/pw/Tha60210011ModulePw.h"
#include "../Tha60290081ModuleClaReg.h"
#include "../Tha60290081ModuleCla.h"
#include "Tha60290081ClaPwController.h"

/*--------------------------- Define -----------------------------------------*/
#define cClaHeaderKeep      0
#define cClaHeaderRemove    1

#define cAf6_upen_pwctl_sw_ssrcval_01_Mask          cBit21_0
#define cAf6_upen_pwctl_sw_ssrcval_01_Shift         0
#define cAf6_upen_pwctl_sw_ssrcval_02_Mask          cBit31_22
#define cAf6_upen_pwctl_sw_ssrcval_02_Shift         22

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60290081ClaPwController *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290081ClaPwController
    {
    tTha60290021ClaPwController super;
    }tTha60290081ClaPwController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaClaControllerMethods m_ThaClaControllerOverride;
static tThaClaPwControllerMethods m_ThaClaPwControllerOverride;
static tTha60210011ClaPwControllerMethods m_Tha60210011ClaPwControllerOverride;

/* Super implementation */
static const tThaClaControllerMethods *m_ThaClaControllerMethods = NULL;
static const tThaClaPwControllerMethods *m_ThaClaPwControllerMethods = NULL;
static const tTha60210011ClaPwControllerMethods *m_Tha60210011ClaPwControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaHbce HbceCreate(ThaClaPwController self, uint8 hbceId, AtIpCore core)
    {
    return Tha60290081HbceNew(hbceId, (AtModule)ThaClaControllerModuleGet((ThaClaController)self), core);
    }

static uint8 HbceMaxFlowsPerEntry(ThaClaPwController self)
    {
    AtUnused(self);
    return 1;
    }

static ThaModuleCla ModuleCla(ThaClaPwController self)
    {
    return ThaClaControllerModuleGet((ThaClaController)self);
    }

static uint32 PwFlowControlReg(ThaClaPwController self, AtPw pwAdapter)
    {
    ThaModuleCla claModule = ModuleCla(self);
    return Tha60290081ModuleClaFlowTableControlReg(claModule) +
           Tha60290081ModuleClaPwFlowLookupOffset(claModule, pwAdapter);
    }

static uint32 PwControlReg(ThaClaPwController self, AtPw pw)
    {
    ThaModuleCla claModule = ModuleCla(self);
    return Tha60290081ModuleClaPwControlTableReg(claModule) +
           Tha60210011ModuleClaBaseAddress(claModule) + mClaPwOffset(self, pw);
    }

static eAtRet PayloadSizeSet(ThaClaPwController self, AtPw pwAdapter, uint16 payloadSize)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddress = PwControlReg(self, pwAdapter);

    mChannelHwLongRead(pwAdapter, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mRegFieldSet(longRegVal[1], cAf6_upen_pwctl_pwlen_, payloadSize);
    mChannelHwLongWrite(pwAdapter, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    return cAtOk;
    }

static uint16 PayloadSizeGet(ThaClaPwController self, AtPw pw)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddress = PwControlReg(self, pw);

    mChannelHwLongRead(pw, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    return (uint16)(mRegField(longRegVal[1], cAf6_upen_pwctl_pwlen_));
    }

static eAtRet PwRtpEnable(ThaClaPwController self, AtPw pw, eBool enable)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddress = PwFlowControlReg(self, pw);

    mChannelHwLongRead(pw, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mRegFieldSet(longRegVal[1], cAf6_upen_flowtab_rtp_en_, enable ? 0x1 : 0x0);
    mRegFieldSet(longRegVal[1], cAf6_upen_flowtab_rtp_act_, enable ? cClaHeaderRemove : cClaHeaderKeep);
    mChannelHwLongWrite(pw, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static eBool PwRtpIsEnabled(ThaClaPwController self, AtPw pw)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddress = PwFlowControlReg( self, pw);

    mChannelHwLongRead(pw, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    return mRegField(longRegVal[1], cAf6_upen_flowtab_rtp_en_)? cAtTrue : cAtFalse;
    }

static eAtRet PwRtpPayloadTypeSet(ThaClaPwController self, AtPw pw, uint8 payloadType)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddress = PwControlReg(self, pw);

    mChannelHwLongRead(pw, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mRegFieldSet(longRegVal[0], cAf6_upen_pwctl_ptval_, payloadType);
    mChannelHwLongWrite(pw, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static uint8 PwRtpPayloadTypeGet(ThaClaPwController self, AtPw pw)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddress = PwControlReg(self, pw);

    mChannelHwLongRead(pw, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    return (uint8)mRegField(longRegVal[0], cAf6_upen_pwctl_ptval_);
    }

static eAtRet PwRtpSsrcSet(ThaClaPwController self, AtPw pw, uint32 ssrc)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddress = PwControlReg(self, pw);
    uint32 ssrc22BitLow = ssrc & cBit21_0;
    uint32 ssrc10BitHigh = (ssrc & cBit31_22) >> 22;

    mChannelHwLongRead(pw, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mRegFieldSet(longRegVal[1], cAf6_upen_pwctl_ssrcval_02_, ssrc10BitHigh);
    mRegFieldSet(longRegVal[0], cAf6_upen_pwctl_ssrcval_01_, ssrc22BitLow);
    mChannelHwLongWrite(pw, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static uint32 PwRtpSsrcGet(ThaClaPwController self, AtPw pw)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddress = PwControlReg(self, pw);
    uint32 msb, lsb;
    uint32 ssrc = 0;

    mChannelHwLongRead(pw, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    msb = mRegField(longRegVal[1], cAf6_upen_pwctl_ssrcval_02_);
    lsb = mRegField(longRegVal[0], cAf6_upen_pwctl_ssrcval_01_);
    mRegFieldSet(ssrc, cAf6_upen_pwctl_sw_ssrcval_02_, msb);
    mRegFieldSet(ssrc, cAf6_upen_pwctl_sw_ssrcval_01_, lsb);

    return ssrc;
    }

static eAtRet PwSuppressEnable(ThaClaPwController self, AtPw pw, eBool enable)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddress = PwControlReg(self, pw);

    mChannelHwLongRead(pw, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mRegFieldSet(longRegVal[0], cAf6_upen_pwctl_sup_en_, enable ? 0x1 : 0x0);
    mChannelHwLongWrite(pw, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static eBool PwSuppressIsEnabled(ThaClaPwController self, AtPw pw)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddress = PwControlReg(self, pw);

    mChannelHwLongRead(pw, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    return (longRegVal[0] & cAf6_upen_pwctl_sup_en_Mask) ? cAtTrue : cAtFalse;
    }

static eBool CheckingModeIsSupported(eThaPwErrorCheckingMode checkMode)
    {
    if ((checkMode == cThaPwErrorCheckingModeCheckCountAndDiscard) ||
        (checkMode == cThaPwErrorCheckingModeNoCheck))
        return cAtTrue;

    return cAtFalse;
    }

static uint8 CheckingModeSw2Hw(eThaPwErrorCheckingMode checkMode)
    {
    return (checkMode == cThaPwErrorCheckingModeCheckCountAndDiscard) ? 1 : 0;
    }

static eAtRet PwRtpPldTypeMismatchCheckModeSet(ThaClaPwController self, AtPw pw, eThaPwErrorCheckingMode checkMode)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddress = PwControlReg(self, pw);
    uint8  pldTypeMismatchCheckEn = CheckingModeSw2Hw(checkMode);

    if (!CheckingModeIsSupported(checkMode))
        return cAtErrorModeNotSupport;

    mChannelHwLongRead(pw, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mRegFieldSet(longRegVal[0], cAf6_upen_pwctl_ptchk_en_, pldTypeMismatchCheckEn);
    mChannelHwLongWrite(pw, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    return cAtOk;
    }

static eThaPwErrorCheckingMode PwRtpPldTypeMismatchCheckModeGet(ThaClaPwController self, AtPw pw)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddress = PwControlReg(self, pw);

    mChannelHwLongRead(pw, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    if (longRegVal[0] & cAf6_upen_pwctl_ptchk_en_Mask)
        return cThaPwErrorCheckingModeCheckCountAndDiscard;

    return cThaPwErrorCheckingModeNoCheck;
    }

static eAtRet PwRtpSsrcMismatchCheckModeSet(ThaClaPwController self, AtPw pw, eThaPwErrorCheckingMode checkMode)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddress = PwControlReg(self, pw);
    uint8  ssrcMismatchCheckEn = CheckingModeSw2Hw(checkMode);

    if (!CheckingModeIsSupported(checkMode))
        return cAtErrorModeNotSupport;

    mChannelHwLongRead(pw, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mRegFieldSet(longRegVal[0], cAf6_upen_pwctl_ssrcchk_en_, ssrcMismatchCheckEn);
    mChannelHwLongWrite(pw, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static eThaPwErrorCheckingMode PwRtpSsrcMismatchCheckModeGet(ThaClaPwController self, AtPw pw)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddress = PwControlReg(self, pw);

    mChannelHwLongRead(pw, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    if (longRegVal[0] & cAf6_upen_pwctl_ssrcchk_en_Mask)
        return cThaPwErrorCheckingModeCheckCountAndDiscard;

    return cThaPwErrorCheckingModeNoCheck;
    }

static eAtRet PwMalformCheckModeSet(ThaClaPwController self, AtPw pw, eThaPwErrorCheckingMode checkMode)
    {
    uint16 payloadLength;
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = PwControlReg(self, pw);

    if (!CheckingModeIsSupported(checkMode))
        return cAtErrorModeNotSupport;

    payloadLength = ThaPwAdapterPayloadSizeGet((ThaPwAdapter)pw);
    if (checkMode != cThaPwErrorCheckingModeNoCheck)
        payloadLength = 0;

    mChannelHwLongRead(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mRegFieldSet(longRegVal[1], cAf6_upen_pwctl_pwlen_, payloadLength);
    mChannelHwLongWrite(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    return cAtOk;
    }

static eAtHdlcPduType PwCircuitPduTypeGet(AtPw pw, eAtPwType pwType)
    {
    AtHdlcLink hdlcLink = NULL;

    if (pwType == cAtPwTypeMlppp)
        return AtHdlcBundlePduTypeGet((AtHdlcBundle)AtPwBoundCircuitGet(pw));

    if (pwType == cAtPwTypeHdlc)
        hdlcLink = AtHdlcChannelHdlcLinkGet((AtHdlcChannel)AtPwBoundCircuitGet(pw));

    if (pwType == cAtPwTypePpp)
        hdlcLink = (AtHdlcLink)AtPwBoundCircuitGet(pw);

    return (hdlcLink) ? AtHdlcLinkPduTypeGet(hdlcLink) : cAtHdlcPduTypeAny;
    }

static uint8 PduOverMplsHwType(AtPw pw, eAtPwType pwType)
    {
    eAtHdlcPduType pduType = PwCircuitPduTypeGet(pw, pwType);

    if (pduType == cAtHdlcPduTypeIPv4)     return 1;
    if (pduType == cAtHdlcPduTypeIPv6)     return 2;
    if (pduType == cAtHdlcPduTypeEthernet) return 3;
    return 0;
    }

static uint8 PsnType2Hw(AtPw pw, AtPwPsn psn, eAtPwHdlcPayloadType payloadType)
    {
    eAtPwType pwType = AtPwTypeGet(pw);

    if ((AtPwPsnTypeGet(psn) != cAtPwPsnTypeMpls) || ThaPwTypeIsCesCep(pw))
        return 0;

    if (payloadType == cAtPwHdlcPayloadTypePdu)
        return PduOverMplsHwType(pw, pwType);

    if (pwType == cAtPwTypeHdlc) return 4;
    if (pwType == cAtPwTypePpp)  return 5;
    if (pwType == cAtPwTypeFr)   return 6;

    return 0;
    }

static eAtRet PwPsnTypeSet(ThaClaPwController self, AtPw pw, AtPwPsn psn, eAtPwHdlcPayloadType payloadType)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddress = PwFlowControlReg(self, pw);

    mChannelHwLongRead(pw, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mRegFieldSet(longRegVal[0], cAf6_upen_flowtab_mplsstyp_, PsnType2Hw(pw, psn, payloadType));
    mChannelHwLongWrite(pw, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static uint32 PwTypeSw2Hw(ThaClaPwController self, AtPw adapter)
    {
    eAtPwType pwType = AtPwTypeGet(adapter);

    AtUnused(self);
    if (pwType == cAtPwTypeCEP)
        return 0x2;

    if (pwType == cAtPwTypeCESoP)
        return 0x1;

    /* SAToP or unknown mode */
    return 0;
    }

static eAtRet PwTypeSet(ThaClaPwController self, AtPw pw, eAtPwType pwType)
    {
    static const uint8 cCEPBasicMode = 0;
    uint32 regAddr;
    uint32 longRegVal[cThaLongRegMaxSize];

    if (!ThaClaPwControllerPwTypeIsSupported(self, pwType))
        return cAtErrorModeNotSupport;

    regAddr = PwFlowControlReg(self, pw);
    mChannelHwLongRead(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mRegFieldSet(longRegVal[0], cAf6_upen_flowtab_cemmode_, PwTypeSw2Hw(self, pw));
    mRegFieldSet(longRegVal[0], cAf6_upen_flowtab_cepmode_, cCEPBasicMode);
    mChannelHwLongWrite(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static uint32 PwCdrControlReg(ThaClaPwController self, AtPw pw)
    {
    ThaModuleCla claModule = ModuleCla(self);
    return Tha60290081ModuleClaPwCdrControlReg(claModule) +
           Tha60210011ModuleClaBaseAddress(claModule) + mClaPwOffset(self, pw);
    }

static eAtRet CdrEnable(ThaClaPwController self, AtPw pw, uint32 cdrId, eBool enable)
    {
    uint32 address = PwCdrControlReg(self, pw);
    uint32 regVal = mChannelHwRead(pw, address, cThaModuleCla);

    AtUnused(cdrId);
    mRegFieldSet(regVal, cAf6_upen_cdrctrl_PwCdrEn_, mBoolToBin(enable));
    mChannelHwWrite(pw, address, regVal, cThaModuleCla);

    return cAtOk;
    }

static eBool CdrIsEnabled(ThaClaPwController self, AtPw pw)
    {
    uint32 address = PwCdrControlReg(self, pw);
    uint32 regVal = mChannelHwRead(pw, address, cThaModuleCla);

    return mBinToBool(mRegField(regVal, cAf6_upen_cdrctrl_PwCdrEn_));
    }

static eAtRet CdrCircuitSliceSet(ThaClaPwController self, AtPw pw, uint8 slice)
    {
    uint32 regAddr = PwCdrControlReg(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModuleCla);
    uint32 tdmLineId = Tha60210011ModulePwTdmLineIdOfPw(pw, cThaModuleCla);

    mRegFieldSet(regVal, cAf6_upen_cdrctrl_PwHoLoOc48Id_, slice);
    mRegFieldSet(regVal, cAf6_upen_cdrctrl_PwTdmLineId_, tdmLineId);
    mChannelHwWrite(pw, regAddr, regVal, cThaModuleCla);

    return cAtOk;
    }

static uint32 PwCdrIdGet(Tha60210011ClaPwController self, AtPw pw)
    {
    uint32 address = PwCdrControlReg((ThaClaPwController)self, pw);
    uint32 regVal = mChannelHwRead(pw, address, cThaModuleCla);
    uint32 cdrIdMask = cAf6_upen_cdrctrl_PwHoLoOc48Id_Mask|cAf6_upen_cdrctrl_PwTdmLineId_Mask;
    return regVal & cdrIdMask;
    }

static eAtRet ChannelEnable(ThaClaController self, AtChannel channel, eBool enable)
    {
    return m_ThaClaControllerMethods->ChannelEnable(self, channel, enable);
    }

static eAtRet EncapHeaderClear(ThaClaPwController self, AtPw adapter)
    {
    uint32 address = PwFlowControlReg(self, adapter);
    uint32 longRegVal[cThaLongRegMaxSize];

    mChannelHwLongRead(adapter, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mRegFieldSet(longRegVal[1], cAf6_upen_flowtab_tdmenc_len_, 0);
    mRegFieldSet(longRegVal[1], cAf6_upen_flowtab_tdmenc_en_, 0);
    mChannelHwLongWrite(adapter, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static void EncapHeaderHwBufferWrite(ThaClaPwController self, AtPw adapter, const uint8* header, uint8 numBytes)
    {
    uint8 byte_i, reg_i;
    uint32 longRegVal[cThaLongRegMaxSize];
    ThaModuleCla claModule = ModuleCla(self);
    uint32 address = Tha60290081ModuleClaTdmHeaderEncapsulationForPduReg(claModule) + AtChannelHwIdGet((AtChannel)adapter) +
                     Tha60210011ModuleClaBaseAddress(claModule);

    if (numBytes > cMaxNumHeaderBytes)
        numBytes = cMaxNumHeaderBytes;

    if (numBytes <= 0)
        return;

    AtOsalMemInit(longRegVal, 0, sizeof(longRegVal));
    byte_i = (uint8)(numBytes - 1);
    reg_i = 0;
    while ((reg_i * 4) < numBytes)
        {
        uint32 regVal = 0;

        regVal |= header[byte_i];

        if (byte_i >= 1)
            regVal |= (uint32)(header[byte_i - 1] << 8);

        if (byte_i >= 2)
            regVal |= (uint32)(header[byte_i - 2] << 16);

        if (byte_i >= 3)
            regVal |= (uint32)(header[byte_i - 3] << 24);

        longRegVal[reg_i++] = regVal;

        byte_i = (uint8)((byte_i >= 4) ? (byte_i - 4) : byte_i);
        }

    mChannelHwLongWrite(adapter, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    }

static eAtRet EncapHeaderLengthSet(ThaClaPwController self, AtPw adapter, uint8 numBytes)
    {
    uint32 address = PwFlowControlReg(self, adapter);
    uint32 longRegVal[cThaLongRegMaxSize];
    uint8 first8Bytes, second8Bytes;

    mChannelHwLongRead(adapter, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    first8Bytes = (uint8)((numBytes < 8) ? numBytes : 8);
    second8Bytes = (uint8)((numBytes < 8) ? 0 : numBytes - 8);
    if (AtPwTypeGet(adapter) == cAtPwTypeMlppp)
        {
        /* RTL recommend that this mode need write the tdmencap_len same as MLPPP header
         * subencap_len need update by PPP header */
        first8Bytes = 2; /* PPP header contain 2 bytes */
        second8Bytes = (uint8)(numBytes - first8Bytes);
        }

    /* Write the first 8 bytes to Sub encap header field */
    mRegFieldSet(longRegVal[1], cAf6_upen_flowtab_subenc_len_, (first8Bytes > 0) ? (first8Bytes - 1) : 0);
    mRegFieldSet(longRegVal[1], cAf6_upen_flowtab_subenc_en_, (first8Bytes > 0) ? 1 : 0);

    /* Write the second 8 bytes to TDM header field */
    mRegFieldSet(longRegVal[1], cAf6_upen_flowtab_tdmenc_len_, (second8Bytes > 0) ? (second8Bytes - 1) : 0);
    mRegFieldSet(longRegVal[1], cAf6_upen_flowtab_tdmenc_en_, (second8Bytes > 0) ? 1 : 0);

    mChannelHwLongWrite(adapter, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static eAtRet EncapHeaderSet(ThaClaPwController self, AtPw adapter, const uint8* header, uint8 numBytes)
    {
    EncapHeaderHwBufferWrite(self, adapter, header, numBytes);
    return EncapHeaderLengthSet(self, adapter, numBytes);
    }

static eAtRet EncapHeaderUpdate(ThaClaPwController self, AtPw adapter, eAtPwHdlcPayloadType payloadType)
    {
    const uint8* circuitEncapHeader;
    uint8 numBytes;

    if (payloadType != cAtPwHdlcPayloadTypePdu)
        return EncapHeaderClear(self, adapter);

    circuitEncapHeader = ThaHdlcPwAdapterCircuitEncapHeader((ThaPwHdlcAdapter)adapter, &numBytes);
    if ((numBytes == 0) || (circuitEncapHeader == NULL))
        return EncapHeaderClear(self, adapter);

    return EncapHeaderSet(self, adapter, circuitEncapHeader, numBytes);
    }

static uint8 TdmHwEncapType(AtPw adapter, eAtPwHdlcPayloadType payloadType)
    {
    eAtPwType pwType = AtPwTypeGet(adapter);

    if (pwType == cAtPwTypePpp)
        {
        if ((payloadType == cAtPwHdlcPayloadTypePdu) && (PwCircuitPduTypeGet(adapter, pwType) == cAtHdlcPduTypeEthernet))
            return 3;

        return 2;
        }

    if (pwType == cAtPwTypeMlppp)
        return 4;

    if ((pwType == cAtPwTypeHdlc) && (payloadType == cAtPwHdlcPayloadTypePdu))
        return 1;

    return 0;
    }

static eAtRet TdmEncapTypeSet(ThaClaPwController self, AtPw adapter, eAtPwHdlcPayloadType payloadType)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddress = PwFlowControlReg(self, adapter);

    mChannelHwLongRead(adapter, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mRegFieldSet(longRegVal[0], cAf6_upen_flowtab_tdmenc_, TdmHwEncapType(adapter, payloadType));
    mChannelHwLongWrite(adapter, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static eAtRet HdlcPwPayloadTypeSet(ThaClaPwController self, AtPw adapter, eAtPwHdlcPayloadType payloadType)
    {
    eAtRet ret;

    ret  = EncapHeaderUpdate(self, adapter, payloadType);
    ret |= PwPsnTypeSet(self, adapter, AtPwPsnGet(adapter), payloadType);
    ret |= TdmEncapTypeSet(self, adapter, payloadType);

    return ret;
    }

static eAtRet MlpppPwPayloadTypeSet(ThaClaPwController self, AtPw adapter, eAtPwHdlcPayloadType payloadType)
    {
    return ThaClaPwControllerHdlcPwPayloadTypeSet(self, adapter, payloadType);
    }

static eAtRet PwCwEnable(ThaClaPwController self, AtPw adapter, eBool enable)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddress = PwFlowControlReg(self, adapter);

    mChannelHwLongRead(adapter, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mRegFieldSet(longRegVal[1], cAf6_upen_flowtab_cw_en_, mBoolToBin(enable));
    mRegFieldSet(longRegVal[1], cAf6_upen_flowtab_cw_act_, (enable) ? cClaHeaderRemove : cClaHeaderKeep);
    mChannelHwLongWrite(adapter, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static eAtRet SendLbitPacketToCdrEnable(ThaClaPwController self, eBool enable)
    {
    AtUnused(enable);
    AtUnused(self);
    return cAtOk;
    }

static void OverrideThaClaController(ThaClaPwController self)
    {
    ThaClaController controller = (ThaClaController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaClaControllerMethods = mMethodsGet(controller);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaControllerOverride, m_ThaClaControllerMethods, sizeof(m_ThaClaControllerOverride));

        mMethodOverride(m_ThaClaControllerOverride, ChannelEnable);
        }

    mMethodsSet(controller, &m_ThaClaControllerOverride);
    }

static void OverrideThaClaPwController(ThaClaPwController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaClaPwControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaPwControllerOverride, m_ThaClaPwControllerMethods, sizeof(m_ThaClaPwControllerOverride));

        mMethodOverride(m_ThaClaPwControllerOverride, HbceCreate);
        mMethodOverride(m_ThaClaPwControllerOverride, HbceMaxFlowsPerEntry);
        mMethodOverride(m_ThaClaPwControllerOverride, PwTypeSet);
        mMethodOverride(m_ThaClaPwControllerOverride, CdrEnable);
        mMethodOverride(m_ThaClaPwControllerOverride, CdrIsEnabled);
        mMethodOverride(m_ThaClaPwControllerOverride, CdrCircuitSliceSet);
        mMethodOverride(m_ThaClaPwControllerOverride, HdlcPwPayloadTypeSet);
        mMethodOverride(m_ThaClaPwControllerOverride, MlpppPwPayloadTypeSet);
        mMethodOverride(m_ThaClaPwControllerOverride, PwCwEnable);
        mMethodOverride(m_ThaClaPwControllerOverride, PayloadSizeSet);
        mMethodOverride(m_ThaClaPwControllerOverride, PayloadSizeGet);
        mMethodOverride(m_ThaClaPwControllerOverride, PwRtpIsEnabled);
        mMethodOverride(m_ThaClaPwControllerOverride, PwRtpEnable);
        mMethodOverride(m_ThaClaPwControllerOverride, PwRtpPayloadTypeSet);
        mMethodOverride(m_ThaClaPwControllerOverride, PwRtpPayloadTypeGet);
        mMethodOverride(m_ThaClaPwControllerOverride, PwRtpSsrcSet);
        mMethodOverride(m_ThaClaPwControllerOverride, PwRtpSsrcGet);
        mMethodOverride(m_ThaClaPwControllerOverride, PwRtpPldTypeMismatchCheckModeSet);
        mMethodOverride(m_ThaClaPwControllerOverride, PwRtpPldTypeMismatchCheckModeGet);
        mMethodOverride(m_ThaClaPwControllerOverride, PwRtpSsrcMismatchCheckModeSet);
        mMethodOverride(m_ThaClaPwControllerOverride, PwRtpSsrcMismatchCheckModeGet);
        mMethodOverride(m_ThaClaPwControllerOverride, PwSuppressEnable);
        mMethodOverride(m_ThaClaPwControllerOverride, PwSuppressIsEnabled);
        mMethodOverride(m_ThaClaPwControllerOverride, PwMalformCheckModeSet);
        mMethodOverride(m_ThaClaPwControllerOverride, SendLbitPacketToCdrEnable);
        }

    mMethodsSet(self, &m_ThaClaPwControllerOverride);
    }

static void OverrideTha60210011ClaPwController(ThaClaPwController self)
    {
    Tha60210011ClaPwController controller = (Tha60210011ClaPwController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011ClaPwControllerMethods = mMethodsGet(controller);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ClaPwControllerOverride, m_Tha60210011ClaPwControllerMethods, sizeof(m_Tha60210011ClaPwControllerOverride));

        mMethodOverride(m_Tha60210011ClaPwControllerOverride, PwCdrIdGet);
        }

    mMethodsSet(controller, &m_Tha60210011ClaPwControllerOverride);
    }

static void Override(ThaClaPwController self)
    {
    OverrideThaClaController(self);
    OverrideThaClaPwController(self);
    OverrideTha60210011ClaPwController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081ClaPwController);
    }

static ThaClaPwController ObjectInit(ThaClaPwController self, ThaModuleCla cla)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021ClaPwControllerObjectInit(self, cla) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaClaPwController Tha60290081ClaPwControllerNew(ThaModuleCla cla)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaClaPwController newObject = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newObject, cla);
    }


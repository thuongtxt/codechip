/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : Tha60290081ClaEthPortControllerInternal.h
 * 
 * Created Date: Oct 11, 2019
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60290081_CLA_CONTROLLER_THA60290081CLAETHPORTCONTROLLERINTERNAL_H_
#define _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60290081_CLA_CONTROLLER_THA60290081CLAETHPORTCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../Tha60210051/cla/controllers/Tha60210051ClaControllerInternal.h"
#include "../../../Tha60210051/cla/controllers/Tha60210051ClaPwEthPortControllerInternal.h"
#include "../../../../default/cla/ThaModuleCla.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290081ClaEthPortController
    {
    tTha60210051ClaPwEthPortController super;
    }tTha60290081ClaEthPortController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaClaEthPortController Tha60290081ClaEthPortControllerObjectInit(ThaClaEthPortController self, ThaModuleCla cla);

#ifdef __cplusplus
}
#endif
#endif /* _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60290081_CLA_CONTROLLER_THA60290081CLAETHPORTCONTROLLERINTERNAL_H_ */


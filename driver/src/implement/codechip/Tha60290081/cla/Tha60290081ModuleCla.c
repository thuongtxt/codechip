/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60290081ModuleCla.c
 *
 * Created Date: Jul 5, 2019
 *
 * Description : Module CLA
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/eth/AtEthFlowControlInternal.h"
#include "../../../default/pw/headercontrollers/ThaPwHeaderController.h"
#include "../../Tha60210051/man/Tha60210051Device.h"
#include "../eth/Tha60290081EthFlow.h"
#include "Tha60290081ModuleClaInternal.h"
#include "Tha60290081ModuleCla.h"
#include "Tha60290081ModuleClaReg.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self)     ((Tha60290081ModuleCla)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods       m_AtObjectOverride;
static tAtModuleMethods       m_AtModuleOverride;
static tThaModuleClaMethods   m_ThaModuleClaOverride;

/* Save super implementations */
static const tAtObjectMethods *m_AtObjectMethods = NULL;
static const tAtModuleMethods *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModulePw ModulePw(ThaModuleCla self)
    {
    return (AtModulePw)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModulePw);
    }

static AtModuleEncap ModuleEncap(ThaModuleCla self)
    {
    return (AtModuleEncap)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModuleEncap);
    }

static ThaModuleEth ModuleEth(ThaModuleCla self)
    {
    return (ThaModuleEth)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModuleEth);
    }

static uint8 EthernetType2Entry(ThaModuleCla self, uint16 ethType)
    {
    AtUnused(self);

    if (ethType == 0x0800) return 0;
    if (ethType == 0x86DD) return 1;
    if (ethType == 0x8847) return 2;
    if (ethType == 0x8848) return 3;
    if (ethType == 0x8857) return 16;
    if (ethType == 0x88D8) return 22;

    return 0xFF;
    }

static uint32 EntryOffset(ThaModuleCla self, uint16 ethType)
    {
    return Tha60210011ModuleClaBaseAddress(self) + EthernetType2Entry(self, ethType);
    }

static eAtRet ProtocolIdTableEntrySet(ThaModuleCla self, uint16 ethType, uint16 protocolId)
    {
    uint32 regVal = 0;
    uint32 regAddr = cAf6Reg_upen_ptlenc_Base + EntryOffset(self, ethType);

    mRegFieldSet(regVal, cAf6_upen_ptlenc_chdlc_pid_, ethType);
    mRegFieldSet(regVal, cAf6_upen_ptlenc_ppp_pid_,   protocolId);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet ProtocolIdTableDefault(ThaModuleCla self)
    {
    eAtRet ret = cAtOk;

    ret |= ProtocolIdTableEntrySet(self, cThaEthernetTypeIpV4, cThaPppProtocolIpV4);
    ret |= ProtocolIdTableEntrySet(self, cThaEthernetTypeIpV6, cThaPppProtocolIpV6);
    ret |= ProtocolIdTableEntrySet(self, cThaEthernetTypeMPLSUnicast, cThaPppProtocolMPLSUnicast);
    ret |= ProtocolIdTableEntrySet(self, cThaEthernetTypeMPLSMulticast, cThaPppProtocolMPLSMulticast);
    ret |= ProtocolIdTableEntrySet(self, 0x8857, 0x8857); /* TODO: look for exact standard assignment */
    ret |= ProtocolIdTableEntrySet(self, cThaEthernetTypeMef, cThaEthernetTypeMef); /* TODO: look for PPP protocol assigned for MEF */

    return ret;
    }

static eAtRet EthernetTypeEntrySet(ThaModuleCla self, uint16 ethType)
    {
    uint32 regAddr = cAf6Reg_upen_etypcam_Base + EntryOffset(self, ethType);
    uint32 regVal = 0;

    mRegFieldSet(regVal, cAf6_upen_etypcam_cametyp_vld_, 1);
    mRegFieldSet(regVal, cAf6_upen_etypcam_ethtyp_, ethType);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet EthernetTypeDefault(ThaModuleCla self)
    {
    eAtRet ret = cAtOk;

    ret |= EthernetTypeEntrySet(self, cThaEthernetTypeIpV4);
    ret |= EthernetTypeEntrySet(self, cThaEthernetTypeIpV6);
    ret |= EthernetTypeEntrySet(self, cThaEthernetTypeMPLSUnicast);
    ret |= EthernetTypeEntrySet(self, cThaEthernetTypeMPLSMulticast);
    ret |= EthernetTypeEntrySet(self, 0x8857);
    ret |= EthernetTypeEntrySet(self, cThaEthernetTypeMef);

    return cAtOk;
    }

static eAtRet GlobalPwDefaultSet(ThaModuleCla self)
    {
    uint32 baseAddress = Tha60210011ModuleClaBaseAddress(self);

    mModuleHwWrite(self, baseAddress + cAf6Reg_upen_mef_etyp_Base, cThaEthernetTypeMef);
    mModuleHwWrite(self, baseAddress + cAf6Reg_upen_udpprt_sel_Base, 0);
    mModuleHwWrite(self, baseAddress + cAf6Reg_upen_udpprt_mode_Base, 0);

    return cAtOk;
    }

static uint32 ActivateRegister(ThaModuleCla self)
    {
    return Tha60210011ModuleClaBaseAddress(self) + cAf6Reg_upen_cla_rdy_Base;
    }

static eAtRet HwReadyEnable(ThaModuleCla self, eBool enable)
    {
    uint32 regAddr = ActivateRegister(self);
    uint32 regVal = mModuleHwRead(self, regAddr);

    mRegFieldSet(regVal, cAf6_upen_cla_rdy_cfg_cla_rdy_, enable ? 1 : 0);
    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eAtRet Activate(AtModule self)
    {
    return HwReadyEnable((ThaModuleCla)self, cAtTrue);
    }

static eAtRet Deactivate(AtModule self)
    {
    return HwReadyEnable((ThaModuleCla)self, cAtFalse);
    }

static eBool IsActive(AtModule self)
    {
    uint32 regAddr = ActivateRegister((ThaModuleCla)self);
    uint32 regVal = mModuleHwRead(self, regAddr);
    return mRegField(regVal, cAf6_upen_cla_rdy_cfg_cla_rdy_) ? cAtTrue : cAtFalse;
    }

static eAtRet RxEosFcsDefaultSet(ThaModuleCla self, eBool insertionEnable)
    {
    uint32 regAddr = ActivateRegister(self);
    uint32 regVal = mModuleHwRead(self, regAddr);

    mRegFieldSet(regVal, cAf6_upen_cla_rdy_cfg_eth_fcsen_, insertionEnable ? 1 : 0);
    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static uint32 LookupRuleControlReg(ThaModuleCla self, uint8 ruleId)
    {
    return Tha60210011ModuleClaBaseAddress(self) + (uint32)(cAf6Reg_upen_rulechk_Base + ruleId);
    }

static eAtRet PortLookupRuleSet(ThaModuleCla self)
    {
    static const uint8 cClaPortRuleId = 0;
    uint32 address = LookupRuleControlReg(self, cClaPortRuleId);
    uint32 regVal = 0;

    mRegFieldSet(regVal, cAf6_upen_rulechk_outen_, 0);
    mRegFieldSet(regVal, cAf6_upen_rulechk_sertyp_tabid_, cTha60290081ClaLookupPort);
    mRegFieldSet(regVal, cAf6_upen_rulechk_flowid_tabid_, cTha60290081ClaLookupPort);
    mRegFieldSet(regVal, cAf6_upen_rulechk_linkid_tabid_, cTha60290081ClaLookupPort);
    mModuleHwWrite(self, address, regVal);

    return cAtOk;
    }

static eAtRet PtchLookupRuleSet(ThaModuleCla self)
    {
    static const uint8 cClaPtchRuleId = 1;
    uint32 address = LookupRuleControlReg(self, cClaPtchRuleId);
    uint32 regVal = 0;

    mRegFieldSet(regVal, cAf6_upen_rulechk_outen_, 0);
    mRegFieldSet(regVal, cAf6_upen_rulechk_sertyp_tabid_, cTha60290081ClaLookupPort);
    mRegFieldSet(regVal, cAf6_upen_rulechk_flowid_tabid_, cTha60290081ClaLookupPort);
    mRegFieldSet(regVal, cAf6_upen_rulechk_linkid_tabid_, cTha60290081ClaLookupFirstVlan);
    mModuleHwWrite(self, address, regVal);

    return cAtOk;
    }

static eAtRet FirstVlanLookupRuleSet(ThaModuleCla self)
    {
    static const uint8 cClaFirstVlanRuleId = 2;
    uint32 address = LookupRuleControlReg(self, cClaFirstVlanRuleId);
    uint32 regVal = 0;

    mRegFieldSet(regVal, cAf6_upen_rulechk_outen_, 1);
    mRegFieldSet(regVal, cAf6_upen_rulechk_sertyp_tabid_, cTha60290081ClaLookupFirstVlan);
    mRegFieldSet(regVal, cAf6_upen_rulechk_flowid_tabid_, cTha60290081ClaLookupFirstVlan);
    mRegFieldSet(regVal, cAf6_upen_rulechk_linkid_tabid_, cTha60290081ClaLookupFirstVlan);
    mModuleHwWrite(self, address, regVal);

    return cAtOk;
    }

static eAtRet SecondVlanLookupRuleSet(ThaModuleCla self)
    {
    static const uint8 cClaSecondVlanRuleId = 3;
    uint32 address = LookupRuleControlReg(self, cClaSecondVlanRuleId);
    uint32 regVal = 0;

    mRegFieldSet(regVal, cAf6_upen_rulechk_outen_, 1);
    mRegFieldSet(regVal, cAf6_upen_rulechk_sertyp_tabid_, cTha60290081ClaLookupSecondVlan);
    mRegFieldSet(regVal, cAf6_upen_rulechk_flowid_tabid_, cTha60290081ClaLookupSecondVlan);
    mRegFieldSet(regVal, cAf6_upen_rulechk_linkid_tabid_, cTha60290081ClaLookupSecondVlan);
    mModuleHwWrite(self, address, regVal);

    return cAtOk;
    }

static eAtRet PsnLookupRuleSet(ThaModuleCla self)
    {
    static const uint8 cClaHashRuleId = 4;
    uint32 address = LookupRuleControlReg(self, cClaHashRuleId);
    uint32 regVal = 0;

    mRegFieldSet(regVal, cAf6_upen_rulechk_outen_, 1);
    mRegFieldSet(regVal, cAf6_upen_rulechk_sertyp_tabid_, cTha60290081ClaLookupHash);
    mRegFieldSet(regVal, cAf6_upen_rulechk_flowid_tabid_, cTha60290081ClaLookupHash);
    mRegFieldSet(regVal, cAf6_upen_rulechk_linkid_tabid_, cTha60290081ClaLookupPort);
    mModuleHwWrite(self, address, regVal);

    return cAtOk;
    }

static eAtRet DefaultLookupRulesSet(ThaModuleCla self)
    {
    eAtRet ret = cAtOk;
    ret |= PortLookupRuleSet(self);
    ret |= PtchLookupRuleSet(self);
    ret |= FirstVlanLookupRuleSet(self);
    ret |= SecondVlanLookupRuleSet(self);
    ret |= PsnLookupRuleSet(self);
    return ret;
    }

static eAtRet GlobalErrorCheckEnable(ThaModuleCla self, eBool enable)
    {
    uint32 regVal;
    uint32 address = Tha60210011ModuleClaBaseAddress(self) + cAf6Reg_upen_errset_en_Base;
    uint32 enabled = (enable) ? 1 : 0;

    regVal = mModuleHwRead(self, address);
    mRegFieldSet(regVal, cAf6_upen_errset_en_malerr_en0_, enabled);
    mRegFieldSet(regVal, cAf6_upen_errset_en_pterr_en0_, enabled);
    mRegFieldSet(regVal, cAf6_upen_errset_en_ssrcerr_en0_, enabled);
    mRegFieldSet(regVal, cAf6_upen_errset_en_mruerr_en0_, enabled);
    mRegFieldSet(regVal, cAf6_upen_errset_en_buferr_en0_, enabled);
    mRegFieldSet(regVal, cAf6_upen_errset_en_lkuerr_en0_, enabled);
    mRegFieldSet(regVal, cAf6_upen_errset_en_dachkerr_en0_, enabled);
    mRegFieldSet(regVal, cAf6_upen_errset_en_etherr_en0_, enabled);
    mModuleHwWrite(self, address, regVal);

    return cAtOk;
    }

static eAtRet DefaultSet(ThaModuleCla self)
    {
    eAtRet ret = cAtOk;

    /* Common */
    ret |= GlobalErrorCheckEnable(self, cAtTrue);
    ret |= GlobalPwDefaultSet(self);
    ret |= DefaultLookupRulesSet(self);

    /* POS service */
    ret |= EthernetTypeDefault(self);
    ret |= ProtocolIdTableDefault(self);

    /* EOS service */
    ret |= RxEosFcsDefaultSet(self, cAtTrue);

    return ret;
    }

static eAtRet HwFlush(ThaModuleCla self)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);

    /* Port service control */
    ThaDeviceMemoryFlush(device, 0x6051C0, 0x6051CF, 0);

    /* VLAN service control */
    ThaDeviceMemoryFlush(device, 0x601000, 0x601FFF, 0);
    ThaDeviceMemoryFlush(device, 0x604000, 0x604FFF, 0);

    /* Rule check control */
    ThaDeviceMemoryFlush(device, 0x6051A0, 0x6051A7, 0);

    /* MAC DA check CAM */
    ThaDeviceMemoryFlush(device, 0x605220, 0x605223, 0);

    return cAtOk;
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret;
    ThaModuleCla moduleCla = (ThaModuleCla)self;

    ret = ThaModuleClaInit(self);
    if (ret != cAtOk)
        return ret;

    ret |= AtModuleActivate(self);
    ret |= mMethodsGet(moduleCla)->DefaultSet(moduleCla);
    return ret;
    }

static uint32 *HoldRegistersGet(AtModule self, uint16 *numberOfHoldRegisters)
    {
    static uint32 holdRegisters[] = {0x0600000, 0x0600001, 0x0600002};
    AtUnused(self);

    if (numberOfHoldRegisters)
        *numberOfHoldRegisters = mCount(holdRegisters);

    return holdRegisters;
    }

static AtLongRegisterAccess LongRegisterAccessCreate(AtModule self)
    {
    uint16 numHoldRegisters;
    uint32 *holdRegisters = AtModuleHoldRegistersGet(self, &numHoldRegisters);
    return AtDefaultLongRegisterAccessNew(holdRegisters, numHoldRegisters, holdRegisters, numHoldRegisters);
    }

static uint32 CellOffset(uint32 cellIndex)
    {
    static const uint32 cNumEntriesInTable = 2048;
    uint32 tableId = cellIndex / cNumEntriesInTable;
    uint32 entryIdInTable = cellIndex % cNumEntriesInTable;

    return tableId * 0x800 + entryIdInTable;
    }

static uint32 FlowTableControlReg(ThaModuleCla self)
    {
    AtUnused(self);
    return cAf6Reg_upen_flowtab_Base;
    }

static uint32 PwControlTableReg(ThaModuleCla self)
    {
    AtUnused(self);
    return cAf6Reg_upen_pwctl_Base;
    }

static uint32 PwCdrControlReg(ThaModuleCla self)
    {
    AtUnused(self);
    return cAf6Reg_upen_cdrctrl_Base;
    }

static uint32 TdmHeaderEncapsulationForPduReg(ThaModuleCla self)
    {
    AtUnused(self);
    return cAf6Reg_upen_hdrenc_Base;
    }

static uint32 PwFlowControlReg(ThaModuleCla self, AtPw pwAdapter)
    {
    return Tha60290081ModuleClaFlowTableControlReg(self) +
           Tha60290081ModuleClaPwFlowLookupOffset(self, pwAdapter);
    }

static uint8 Psn2HeaderRemovingAction(AtPw pw, eBool enable)
    {
    eAtPwPsnType psnType;
    AtPwPsn psn;

    if (!enable)
        return 0;

    psn = AtPwPsnGet(pw);
    if (psn == NULL)
        return 2;

    psnType = AtPwPsnTypeGet(psn);
    if (psnType == cAtPwPsnTypeMef)
        return 3;
    if (psnType == cAtPwPsnTypeMpls)
        return 4;

    if (psnType != cAtPwPsnTypeUdp)
        return 0; /* Not controlled if not UDP */

    psn = AtPwPsnLowerPsnGet(psn);
    psnType = AtPwPsnTypeGet(psn);
    if (psnType == cAtPwPsnTypeIPv4)
        return 5;
    if (psnType == cAtPwPsnTypeIPv6)
        return 6;

    return 0;
    }

static eAtRet RemovePwHeader(ThaModuleCla self, AtPw pw, eBool enable)
    {
    uint32 regAddress;
    uint32 longRegVal[cThaLongRegMaxSize];

    regAddress = PwFlowControlReg(self, pw);
    mChannelHwLongRead(pw, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    mRegFieldSet(longRegVal[0], cAf6_upen_flowtab_psnhdr_act_, Psn2HeaderRemovingAction(pw, enable));
    mRegFieldSet(longRegVal[1], cAf6_upen_flowtab_rtp_act_, enable ? 1 : 0);
    mRegFieldSet(longRegVal[1], cAf6_upen_flowtab_cw_act_, enable ? 1 : 0);

    mChannelHwLongWrite(pw, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    return cAtOk;
    }

static eAtRet PwPsnHeaderUpdate(ThaModuleCla self, AtPw adapter)
    {
    return Tha60290081ModuleClaRemovePwHeader(self, adapter, cAtTrue);
    }

static uint32 EthTypeCamReg(ThaModuleCla self)
    {
    AtUnused(self);
    return cAf6Reg_upen_etypcam_Base;
    }

static const char* EntryId2EthTypeString(AtModule self, uint8 entryId)
    {
    ThaModuleCla claModule = (ThaModuleCla)self;

    if (EthernetType2Entry(claModule, cThaEthernetTypeIpV4) == entryId)
        return "IPv4";
    if (EthernetType2Entry(claModule, cThaEthernetTypeIpV6) == entryId)
        return "IPv6";
    if (EthernetType2Entry(claModule, cThaEthernetTypeMPLSUnicast) == entryId)
        return "MPLS UNICAST";
    if (EthernetType2Entry(claModule, cThaEthernetTypeMPLSMulticast) == entryId)
        return "MPLS MULTICAST";
    if (EthernetType2Entry(claModule, 0x8857) == entryId)
        return "OAM (0x8857)";
    if (EthernetType2Entry(claModule, cThaEthernetTypeMef) == entryId)
        return "MEF";

    return "UNKNOWN";
    }

static uint16 EntryId2ProtocolId(AtModule self, uint8 entryId, eBool isPpp)
    {
    uint32 regAddr = cAf6Reg_upen_ptlenc_Base + Tha60210011ModuleClaBaseAddress((ThaModuleCla)self);
    uint32 regVal = mModuleHwRead(self, regAddr + entryId);

    if (isPpp)
        return (uint16)mRegField(regVal, cAf6_upen_ptlenc_ppp_pid_);
    return (uint16)mRegField(regVal, cAf6_upen_ptlenc_chdlc_pid_);
    }

static void EthTypesAndPidOnCamTableShow(AtModule self)
    {
    uint8 i;
    uint32 regVal;
    const uint8 cNumCamEntries = 32;
    uint32 ethTypeCamReg = EthTypeCamReg((ThaModuleCla)self) + Tha60210011ModuleClaBaseAddress((ThaModuleCla)self);

    AtPrintc(cSevInfo, "* Ethernet types on CAM table:\r\n");
    AtPrintc(cSevInfo, "    Entry ID |  Ethernet Type  | Ethernet value | PID (Cisco) | PID (PPP) \r\n");

    for (i = 0; i < cNumCamEntries; i++)
        {
        regVal = mModuleHwRead(self, ethTypeCamReg + i);

        if (mRegField(regVal, cAf6_upen_etypcam_cametyp_vld_) == 0x0)
            continue;

        AtPrintc(cSevNormal,
                 "      %02u     | %15s |     0x%04X     |    0x%04X   | 0x%04X\r\n",
                 i,
                 EntryId2EthTypeString(self, i),
                 mRegField(regVal, cAf6_upen_etypcam_ethtyp_),
                 EntryId2ProtocolId(self, i, cAtFalse),
                 EntryId2ProtocolId(self, i, cAtTrue));
        }
    }

static eAtRet Debug(AtModule self)
    {
    Tha60290081ModuleClaDebug((ThaModuleCla)self);

    /* CAM table */
    EthTypesAndPidOnCamTableShow(self);
    return cAtOk;
    }

static uint32 PwControlReg(ThaModuleCla self, ThaPwAdapter pwAdapter)
    {
    ThaClaPwController controller = ThaModuleClaPwControllerGet(self);
    return Tha60290081ModuleClaPwControlTableReg(self) +
           Tha60210011ModuleClaBaseAddress(self) +
           mClaPwOffset(controller, (AtPw)pwAdapter);
    }

static uint32 TdmHeaderControlReg(ThaModuleCla self, ThaPwAdapter pwAdapter)
    {
    return Tha60290081ModuleClaTdmHeaderEncapsulationForPduReg(self) +
           AtChannelHwIdGet((AtChannel)pwAdapter) +
           Tha60210011ModuleClaBaseAddress(self);
    }

static uint32 Pw2CdrControlReg(ThaModuleCla self, ThaPwAdapter pwAdapter)
    {
    ThaClaPwController controller = ThaModuleClaPwControllerGet(self);
    return Tha60290081ModuleClaPwCdrControlReg(self) +
           Tha60210011ModuleClaBaseAddress(self) +
           mClaPwOffset(controller, (AtPw)pwAdapter);
    }

static void PwClaRegShow(ThaModuleCla self, AtPw pw)
    {
    ThaPwAdapter pwAdapter = ThaPwAdapterGet(pw);

    ThaModuleClaLongRegDisplay(pw, "Classify Per Flow Table Control", PwFlowControlReg(self, (AtPw)pwAdapter));

    if (ThaPwTypeIsCesCep((AtPw)pwAdapter))
        {
        ThaModuleClaLongRegDisplay(pw, "PW Control Table", PwControlReg(self, pwAdapter));
        ThaModuleClaRegDisplay(pw, "Classify Per Pseudowire Identification to CDR Control", Pw2CdrControlReg(self, pwAdapter));
        }
    else
        {
        ThaModuleClaLongRegDisplay(pw, "TDM Header Encapsulation for PDU", TdmHeaderControlReg(self, pwAdapter));
        }
    }

static void HbceShow(ThaModuleCla self, ThaPwHeaderController headerController)
    {
    ThaPwAdapter pwAdapter = ThaPwHeaderControllerAdapterGet(headerController);
    ThaHbce hbce = ThaPwAdapterHbceGet((AtPw)pwAdapter);
    ThaHbceMemoryCellContent cellContent;
    ThaHbceMemoryCell cell;
    ThaHbceEntry hbceEntry;
    uint32 cellIndex;
    AtUnused(self);

    cellContent = ThaPwHeaderControllerHbceMemoryCellContentGet(headerController);
    if (cellContent == NULL)
        return;

    if (ThaPwHeaderControllerIsPrimary(headerController))
        AtPrintc(cSevInfo, "* Pw HBCE information:\r\n");
    else
        AtPrintc(cSevInfo, "* Pw Backup HBCE information:\r\n");

    hbceEntry = ThaHbceMemoryCellContentHashEntryGet(cellContent);
    cell = ThaHbceMemoryCellContentCellGet(cellContent);
    cellIndex = ThaHbceMemoryCellIndexGet(cell);
    AtPrintc(cSevInfo, "   - Hash Index: %d\r\n", ThaHbceEntryIndexGet(hbceEntry));
    AtPrintc(cSevInfo, "   - SW cell Index: %d\r\n", cellIndex);
    if (cellIndex < ThaHbceMaxNumEntries(hbce))
        ThaModuleClaLongRegDisplay((AtPw)pwAdapter, "Classify HBCE Looking Up Information Control",
                                   Tha60290081HbceMemoryCellAddress(self, cellIndex));
    else
        ThaModuleClaLongRegDisplay((AtPw)pwAdapter, "CAM for PW HASH",
                                   Tha60290081HbceMemoryCellAddress(self, cellIndex));
    }

static void PwVlansRegShow(ThaModuleCla self, AtPw pw)
    {
    tAtEthVlanTag sVlan, cVlan;
    uint32 baseAddress = Tha60210011ModuleClaBaseAddress(self);
    uint32 address;

    if (AtPwEthSVlanGet(pw, &sVlan))
        {
        address = (uint32)(cAf6Reg_upen_fvlnser_Base + baseAddress + sVlan.vlanId);
        ThaModuleClaLongRegDisplay(pw, "First VLAN (S-VLAN) service control", address);

        AtPwEthCVlanGet(pw, &cVlan);

        address = (uint32)(cAf6Reg_upen_svlnser_Base + baseAddress + cVlan.vlanId);
        ThaModuleClaLongRegDisplay(pw, "Second VLAN (C-VLAN) service control", address);
        }

    if (AtPwEthCVlanGet(pw, &cVlan))
        {
        address = (uint32)(cAf6Reg_upen_fvlnser_Base + baseAddress + cVlan.vlanId);
        ThaModuleClaLongRegDisplay(pw, "First VLAN (C-VLAN) service control", address);
        }
    }

static void PwRegsShow(ThaModuleCla self, AtPw pw)
    {
    ThaPwAdapter pwAdapter = ThaPwAdapterGet(pw);

    PwClaRegShow(self, pw);
    HbceShow(self, ThaPwAdapterHeaderController(pwAdapter));
    PwVlansRegShow(self, pw);
    }

static void EthPortRegsShow(ThaModuleCla self, AtEthPort port)
    {
    uint32 baseAddress = Tha60210011ModuleClaBaseAddress(self);
    uint32 address;

    address = (uint32)(cAf6Reg_upen_portser_Base + baseAddress + AtChannelIdGet((AtChannel)port));
    ThaDeviceRegNameDisplay("PORT service control");
    ThaDeviceChannelRegValueDisplay((AtChannel)port, address, cThaModuleCla);

    address = (uint32)(cAf6Reg_upen_dachkcam_Base + baseAddress + AtChannelIdGet((AtChannel)port));
    ThaDeviceRegNameDisplay("MAC DA check CAM");
    ThaDeviceChannelRegLongValueDisplay((AtChannel)port, address, cThaModuleCla);
    }

static ThaClaPwController PwControllerCreate(ThaModuleCla self)
    {
    return Tha60290081ClaPwControllerNew(self);
    }

static AtPw HwFlowPwGet(ThaModuleCla self, uint32 serviceHwType, uint32 hwFlowId)
    {
    uint32 pwId;
    uint32 regAddr, regVal;
    ThaModuleCla moduleCla = (ThaModuleCla)self;

    regAddr = Tha60290081ModuleClaFlowTableControlReg(moduleCla) + hwFlowId + Tha60210011ModuleClaBaseAddress(moduleCla);
    regVal = mModuleHwRead(self, regAddr);
    pwId = mRegField(regVal, cAf6_upen_flowtab_serid_);

    if (serviceHwType == cClaServiceTypeCem)
        return AtModulePwGetPw(ModulePw(moduleCla), pwId);

    else
        {
        AtModuleEncap encapModule = ModuleEncap(moduleCla);
        AtEncapChannel encapChannel = AtModuleEncapChannelGet(encapModule, (uint16)pwId);
        AtPw pw;

        pw = AtChannelBoundPwGet((AtChannel)encapChannel);
        if (pw)
            return pw;

        if (AtEncapChannelEncapTypeGet(encapChannel) == cAtEncapHdlc)
            return AtChannelBoundPwGet((AtChannel)AtHdlcChannelHdlcLinkGet((AtHdlcChannel)encapChannel));

        return NULL;
        }

    return NULL;
    }

static eBool ShouldBalanceCrcPolynomialsUsage(ThaModuleCla self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static ThaClaEthPortController EthPortControllerCreate(ThaModuleCla self)
    {
    return Tha60290081ClaEthPortControllerNew(self);
    }

static uint8 EthFlowHwFlowType(ThaModuleCla self, AtEthFlow flow)
    {
    uint8 flowType = AtEthFlowTypeGet(flow);
    AtUnused(self);

    switch (flowType)
        {
        case cTha6029EthFlowTypePw:     return 1;
        case cTha6029EthFlowTypePos:    return 2;
        case cTha6029EthFlowTypeEos:    return 6;
        case cTha6029EthFlowTypeEop:    return 7;

        case cTha6029EthFlowTypeDcc:    return 3;
        case cTha6029EthFlowTypeEthPass:  return 4;
        case cTha6029EthFlowTypePtp:    return 5;

        case cTha6029EthFlowTypeAny:    break;
        default:                        break;
        }

    AtChannelLog((AtChannel)flow, cAtLogLevelWarning, AtSourceLocation,
                 "Does not have correct flow type for VLAN lookup\r\n");
    return 0;
    }

static eAtRet HelperEthFlowExpectedVlanSet(ThaModuleCla self, AtEthFlow flow,
                                           tAtEthVlanTag *vlanTag, eBool enable,
                                           uint32 localAddress)
    {
    static const uint32 cVlanTraffic = 1;
    uint32 address = localAddress + Tha60210011ModuleClaBaseAddress(self) + vlanTag->vlanId;
    uint32 flowId = AtChannelHwIdGet((AtChannel)flow);
    uint32 longRegVal[cThaLongRegMaxSize];

    mChannelHwLongRead(flow, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    if (enable)
        {
        mRegFieldSet(longRegVal[1], cAf6_upen_fvlnser_linknxt0_, 0);
        mRegFieldSet(longRegVal[0], cAf6_upen_fvlnser_flowid0_, flowId);
        mRegFieldSet(longRegVal[0], cAf6_upen_fvlnser_outen0_, 1);
        mRegFieldSet(longRegVal[0], cAf6_upen_fvlnser_serstyp0_, cVlanTraffic);
        mRegFieldSet(longRegVal[0], cAf6_upen_fvlnser_sertyp0_, EthFlowHwFlowType(self, flow));
        }
    else
        {
        longRegVal[0] = 0;
        longRegVal[1] = 0;
        mRegFieldSet(longRegVal[1], cAf6_upen_fvlnser_linknxt0_, 1);
        }
    mChannelHwLongWrite(flow, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static eAtRet EthFlowExpectedFirstVlanSet(ThaModuleCla self, AtEthFlow flow, tAtEthVlanTag *vlanTag, eBool enable)
    {
    return HelperEthFlowExpectedVlanSet(self, flow, vlanTag, enable, cAf6Reg_upen_fvlnser_Base);
    }

static eAtRet EthFlowExpectedSecondVlanSet(ThaModuleCla self, AtEthFlow flow, tAtEthVlanTag *vlanTag, eBool enable)
    {
    return HelperEthFlowExpectedVlanSet(self, flow, vlanTag, enable, cAf6Reg_upen_svlnser_Base);
    }

static uint32 EthFlowOffset(ThaModuleCla self, AtEthFlow flow)
    {
    return Tha60210011ModuleClaBaseAddress(self) + AtChannelHwIdGet((AtChannel)flow);
    }

static eAtRet EthFlowChannelSet(ThaModuleCla self, AtEthFlow flow, AtChannel channel)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = FlowTableControlReg(self) + EthFlowOffset(self, flow);

    mChannelHwLongRead(flow, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mRegFieldSet(longRegVal[0], cAf6_upen_flowtab_serid_, AtChannelIdGet(channel));
    mChannelHwLongWrite(flow, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static eAtRet EthFlowRemoveVlan(ThaModuleCla self, AtEthFlow flow)
    {
    static const uint32 firstVlan = 1;
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = FlowTableControlReg(self) + EthFlowOffset(self, flow);

    mChannelHwLongRead(flow, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mRegFieldSet(longRegVal[0], cAf6_upen_flowtab_psnhdr_act_, firstVlan);
    mChannelHwLongWrite(flow, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static uint32 PwFlowLookupOffset(ThaModuleCla self, AtPw pwAdapter)
    {
    AtEthFlow flow = AtPwEthFlowGet(pwAdapter);
    if (flow == NULL)
        return cInvalidUint32;
    return Tha60210011ModuleClaBaseAddress(self) + AtChannelHwIdGet((AtChannel)flow);
    }

static AtList *FirstVlanPool(ThaModuleCla self)
    {
    return mThis(self)->vlan1Pool;
    }

static AtList *SecondVlanPool(ThaModuleCla self)
    {
    return mThis(self)->vlan2Pool;
    }

static AtList FlowListGet(AtList *vlanPool, uint16 vlanId)
    {
    if (vlanPool[vlanId] == NULL)
        vlanPool[vlanId] = AtListCreate(0);
    return vlanPool[vlanId];
    }

static eAtRet VlanFlowAdd(AtList flowList, AtEthFlow flow)
    {
    if (AtListContainsObject(flowList, (AtObject)flow))
        return cAtOk;

    return AtListObjectAdd(flowList, (AtObject)flow);
    }

static eAtRet VlanFlowRemove(AtList flowList, AtEthFlow flow)
    {
    if (!AtListContainsObject(flowList, (AtObject)flow))
        return cAtOk;

    return AtListObjectRemove(flowList, (AtObject)flow);
    }

static eAtRet HelperPwEthFlowVlanEnable(ThaModuleCla self, AtEthFlow flow,
                                        tAtEthVlanTag *vlanTag, eBool enable,
                                        uint32 localAddress)
    {
    uint32 address = localAddress + Tha60210011ModuleClaBaseAddress(self) + vlanTag->vlanId;
    uint32 longRegVal[cThaLongRegMaxSize];

    mChannelHwLongRead(flow, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mRegFieldSet(longRegVal[1], cAf6_upen_fvlnser_linknxt0_, 1);
    mRegFieldSet(longRegVal[0], cAf6_upen_fvlnser_outen0_, (enable) ? 1 : 0);
    mChannelHwLongWrite(flow, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static eAtRet PwEthFlowFirstVlanEnable(ThaModuleCla self, AtEthFlow flow, tAtEthVlanTag *vlanTag, eBool enable)
    {
    AtList flowList = FlowListGet(FirstVlanPool(self), vlanTag->vlanId);

    /* The following code is to support multiple PWs using same VLANs as MRO.
     * If at least one PW has VLAN content as same of this VLAN, this VLAN entry
     * needs to be enabled for forwarding classifying in hash table. */
    if (enable)
        {
        if (AtListLengthGet(flowList) == 0)
            {
            if (ThaModuleEthIngressVlanIsInused(ModuleEth(self), flow, vlanTag->vlanId))
                return cAtErrorRsrcNoAvail;

            ThaModuleEthIngressVlanUse(ModuleEth(self), flow, vlanTag->vlanId);
            }

        VlanFlowAdd(flowList, flow);
        }
    else
        {
        VlanFlowRemove(flowList, flow);

        if (AtListLengthGet(flowList) > 0)
            return cAtOk;

        ThaModuleEthIngressVlanUnUse(ModuleEth(self), flow, vlanTag->vlanId);
        }

    return HelperPwEthFlowVlanEnable(self, flow, vlanTag, enable, cAf6Reg_upen_fvlnser_Base);
    }

static eAtRet PwEthFlowSecondVlanEnable(ThaModuleCla self, AtEthFlow flow, tAtEthVlanTag *vlanTag, eBool enable)
    {
    AtList flowList = FlowListGet(SecondVlanPool(self), vlanTag->vlanId);

    if (enable)
        VlanFlowAdd(flowList, flow);
    else
        {
        VlanFlowRemove(flowList, flow);
        if (AtListLengthGet(flowList) > 0)
            return cAtOk;
        }

    return HelperPwEthFlowVlanEnable(self, flow, vlanTag, enable, cAf6Reg_upen_svlnser_Base);
    }

static void VlanFlowListSerialize(AtObject list, AtCoder encoder)
    {
    AtCoderEncodeObjectDescriptionInList(encoder, (AtList)list, "flowList");
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60290081ModuleCla object = (Tha60290081ModuleCla)self;

    m_AtObjectMethods->Serialize(self, encoder);

    AtCoderEncodeObjectsWithHandler(encoder, (AtObject *)object->vlan1Pool, 4096, "vlan1Pool", VlanFlowListSerialize);
    AtCoderEncodeObjectsWithHandler(encoder, (AtObject *)object->vlan2Pool, 4096, "vlan2Pool", VlanFlowListSerialize);
    }

static void VlanPoolDelete(AtList *vlanPool)
    {
    uint16 i;
    for (i = 0; i < 4096; i++)
        {
        AtObjectDelete((AtObject)vlanPool[i]);
        vlanPool[i] = NULL;
        }
    }

static void Delete(AtObject self)
    {
    VlanPoolDelete(FirstVlanPool((ThaModuleCla)self));
    VlanPoolDelete(SecondVlanPool((ThaModuleCla)self));

    m_AtObjectMethods->Delete(self);
    }

static void OverrideThaModuleCla(AtModule self)
    {
    ThaModuleCla claModule = (ThaModuleCla)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleClaOverride, mMethodsGet(claModule), sizeof(m_ThaModuleClaOverride));

        mMethodOverride(m_ThaModuleClaOverride, DefaultSet);
        mMethodOverride(m_ThaModuleClaOverride, PwPsnHeaderUpdate);
        mMethodOverride(m_ThaModuleClaOverride, PwRegsShow);
        mMethodOverride(m_ThaModuleClaOverride, EthPortRegsShow);
        mMethodOverride(m_ThaModuleClaOverride, PwControllerCreate);
        mMethodOverride(m_ThaModuleClaOverride, ShouldBalanceCrcPolynomialsUsage);
        mMethodOverride(m_ThaModuleClaOverride, EthPortControllerCreate);
        }

    mMethodsSet(claModule, &m_ThaModuleClaOverride);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, LongRegisterAccessCreate);
        mMethodOverride(m_AtModuleOverride, HoldRegistersGet);
        mMethodOverride(m_AtModuleOverride, Debug);
        mMethodOverride(m_AtModuleOverride, Activate);
        mMethodOverride(m_AtModuleOverride, Deactivate);
        mMethodOverride(m_AtModuleOverride, IsActive);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void OverrideAtObject(AtModule self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    OverrideThaModuleCla(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081ModuleCla);
    }

AtModule Tha60290081ModuleClaObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021ModuleClaObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60290081ModuleClaNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290081ModuleClaObjectInit(newModule, device);
    }

uint32 Tha60290081HbceMemoryCellAddress(ThaModuleCla self, uint32 cellIndex)
    {
    static const uint32 cMaxNumHashCells = 8192;
    if (cellIndex < cMaxNumHashCells)
        return cAf6Reg_upen_pwhash_Base + Tha60210011ModuleClaBaseAddress(self) + CellOffset(cellIndex);

    return cAf6Reg_upen_pwhashcam_Base + Tha60210011ModuleClaBaseAddress(self) + (cellIndex - cMaxNumHashCells);
    }

uint32 Tha60290081ModuleClaFlowTableControlReg(ThaModuleCla self)
    {
    if (self)
        return FlowTableControlReg(self);
    return 0;
    }

uint32 Tha60290081ModuleClaPwControlTableReg(ThaModuleCla self)
    {
    if (self)
        return PwControlTableReg(self);
    return 0;
    }

uint32 Tha60290081ModuleClaPwCdrControlReg(ThaModuleCla self)
    {
    if (self)
        return PwCdrControlReg(self);
    return cInvalidUint32;
    }

uint32 Tha60290081ModuleClaTdmHeaderEncapsulationForPduReg(ThaModuleCla self)
    {
    if (self)
        return TdmHeaderEncapsulationForPduReg(self);
    return 0;
    }

eAtRet Tha60290081ModuleClaRemovePwHeader(ThaModuleCla self, AtPw pw, eBool enable)
    {
    if (self)
        return RemovePwHeader(self, pw, enable);

    return cAtErrorNullPointer;
    }

AtPw Tha602900681ModuleClaFlowControlPwGet(ThaModuleCla self, uint32 serviceHwType, uint32 hwFlowId)
    {
    if (self)
        return HwFlowPwGet(self, serviceHwType, hwFlowId);
    return NULL;
    }

void Tha60290081ModuleClaHwFlush(ThaModuleCla self)
    {
    HwFlush(self);
    }

eAtRet Tha60290081ModuleClaEthFlowExpectedFirstVlanSet(ThaModuleCla self, AtEthFlow flow, tAtEthVlanTag *vlan, eBool enable)
    {
    if (self)
        return EthFlowExpectedFirstVlanSet(self, flow, vlan, enable);
    return cAtErrorNullPointer;
    }

eAtRet Tha60290081ModuleClaEthFlowExpectedSecondVlanSet(ThaModuleCla self, AtEthFlow flow, tAtEthVlanTag *vlan, eBool enable)
    {
    if (self)
        return EthFlowExpectedSecondVlanSet(self, flow, vlan, enable);
    return cAtErrorNullPointer;
    }

eAtRet Tha60290081ModuleClaEthFlowChannelSet(ThaModuleCla self, AtEthFlow flow, AtChannel channel)
    {
    if (self)
        return EthFlowChannelSet(self, flow, channel);
    return cAtErrorNullPointer;
    }

eAtRet Tha60290081ModuleClaEthFlowRemoveVlan(ThaModuleCla self, AtEthFlow flow)
    {
    if (self)
        return EthFlowRemoveVlan(self, flow);
    return cAtErrorNullPointer;
    }

uint8 Tha60290081ModuleClaEthFlowHwFlowType(ThaModuleCla self, AtEthFlow flow)
    {
    if (self)
        return EthFlowHwFlowType(self, flow);
    return 0;
    }

uint32 Tha60290081ModuleClaPwFlowLookupOffset(ThaModuleCla self, AtPw pwAdapter)
    {
    if (self)
        return PwFlowLookupOffset(self, pwAdapter);
    return cInvalidUint32;
    }

eAtRet Tha60290081ModuleClaPwEthFlowFirstVlanEnable(ThaModuleCla self, AtEthFlow flow, tAtEthVlanTag *vlan, eBool enable)
    {
    if (self)
        return PwEthFlowFirstVlanEnable(self, flow, vlan, enable);
    return cAtErrorNullPointer;
    }

eAtRet Tha60290081ModuleClaPwEthFlowSecondVlanEnable(ThaModuleCla self, AtEthFlow flow, tAtEthVlanTag *vlan, eBool enable)
    {
    if (self)
        return PwEthFlowSecondVlanEnable(self, flow, vlan, enable);
    return cAtErrorNullPointer;
    }

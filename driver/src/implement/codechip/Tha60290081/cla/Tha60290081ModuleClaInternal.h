/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : Tha60290081ModuleClaInternal.h
 * 
 * Created Date: Jul 27, 2019
 *
 * Description : 60290081 Module CLA representation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290081MODULECLAINTERNAL_H_
#define _THA60290081MODULECLAINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/cla/Tha60290021ModuleClaInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290081ModuleCla
    {
    tTha60290021ModuleCla super;

    /* Private data */
    AtList vlan1Pool[4096];
    AtList vlan2Pool[4096];
    }tTha60290081ModuleCla;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60290081MODULECLAINTERNAL_H_ */


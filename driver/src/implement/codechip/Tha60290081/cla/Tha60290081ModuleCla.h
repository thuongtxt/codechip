/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : Tha60290081ModuleClaMs.h
 * 
 * Created Date: Jul 5, 2019
 *
 * Description : Module Cla
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290081MODULECLA_H_
#define _THA60290081MODULECLA_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/cla/ThaModuleCla.h"
#include "../../../default/util/crchash/ThaCrcHash.h"
#include "../../../default/cla/hbce/ThaHbceMemoryPool.h"
#include "../../../default/cla/hbce/ThaHbceMemoryCell.h"
#include "../../../default/cla/hbce/ThaHbceEntry.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define cMaxNumHeaderBytes  16

#define cClaServiceTypeDrop    0
#define cClaServiceTypeCem     1
#define cClaServiceTypeImsg    2
#define cClaServiceTypeDcc     3

#define cClaServiceSubTypePw   0
#define cClaServiceSubTypeData 1
#define cClaServiceSubTypeOam  2

#define cClaServiceSubTypeNext      cClaServiceSubTypePw
#define cClaServiceSubTypeTerminate cClaServiceSubTypeData

#define cThaEthernetTypeIpV4                0x0800
#define cThaEthernetTypeIpV6                0x86DD
#define cThaEthernetTypeMPLSUnicast         0x8847
#define cThaEthernetTypeMPLSMulticast       0x8848
#define cThaEthernetTypeMef                 0x88D8

#define cThaPppProtocolIpV4                 0x0021
#define cThaPppProtocolIpV6                 0x0057
#define cThaPppProtocolMPLSUnicast          0x0281
#define cThaPppProtocolMPLSMulticast        0x0283

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290081ModuleCla * Tha60290081ModuleCla;

typedef enum eTha60290081ClaLookupRule
    {
    cTha60290081ClaLookupPort       = 0,
    cTha60290081ClaLookupPtch       = 1,
    cTha60290081ClaLookupFirstVlan  = 2,
    cTha60290081ClaLookupSecondVlan = 3,
    cTha60290081ClaLookupHash       = 4
    }eTha60290081ClaLookupRule;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 Tha60290081HbceMemoryCellAddress(ThaModuleCla self, uint32 cellIndex);

uint32 Tha60290081ModuleClaPwControlTableReg(ThaModuleCla self);
uint32 Tha60290081ModuleClaFlowTableControlReg(ThaModuleCla self);
uint32 Tha60290081ModuleClaPwCdrControlReg(ThaModuleCla self);
uint32 Tha60290081ModuleClaTdmHeaderEncapsulationForPduReg(ThaModuleCla self);
eAtRet Tha60290081ModuleClaRemovePwHeader(ThaModuleCla self, AtPw pw, eBool enable);
AtPw Tha602900681ModuleClaFlowControlPwGet(ThaModuleCla self, uint32 serviceHwType, uint32 hwFlowId);

/* Ethernet flow */
eAtRet Tha60290081ModuleClaEthFlowExpectedFirstVlanSet(ThaModuleCla self, AtEthFlow flow, tAtEthVlanTag *sVlan, eBool enable);
eAtRet Tha60290081ModuleClaEthFlowExpectedSecondVlanSet(ThaModuleCla self, AtEthFlow flow, tAtEthVlanTag *cVlan, eBool enable);
eAtRet Tha60290081ModuleClaEthFlowChannelSet(ThaModuleCla self, AtEthFlow flow, AtChannel channel);
eAtRet Tha60290081ModuleClaEthFlowRemoveVlan(ThaModuleCla self, AtEthFlow flow);
uint8 Tha60290081ModuleClaEthFlowHwFlowType(ThaModuleCla self, AtEthFlow flow);

/* PW */
uint32 Tha60290081ModuleClaPwFlowLookupOffset(ThaModuleCla self, AtPw pwAdapter);
eAtRet Tha60290081ModuleClaPwEthFlowFirstVlanEnable(ThaModuleCla self, AtEthFlow flow, tAtEthVlanTag *vlan, eBool enable);
eAtRet Tha60290081ModuleClaPwEthFlowSecondVlanEnable(ThaModuleCla self, AtEthFlow flow, tAtEthVlanTag *vlan, eBool enable);

void Tha60290081ModuleClaHwFlush(ThaModuleCla self);

/* Debug */
eAtRet Tha60290081ModuleClaDebug(ThaModuleCla self);
AtModule Tha60290081ModuleClaObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290081MODULECLA_H_ */


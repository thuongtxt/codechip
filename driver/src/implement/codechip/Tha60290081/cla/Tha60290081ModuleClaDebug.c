/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : Tha60290081ModuleClaDebug.c
 * 
 * Created Date: Jul 8, 2019
 *
 * Description : Debug sticky
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/man/AtModuleInternal.h"
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60210011/cla/Tha60210011ModuleCla.h"
#include "Tha60290081ModuleCla.h"
#include "Tha60290081ModuleClaReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cNumEthPorts     9
#define cAf6_upen_egrhdr_stk_Mask(id)             (cBit0 << (id))
#define cAf6_upen_allc_stk_ffge4_full_id_Mask(id) (cBit12 << (id))

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Local variables --------------------------------*/
/* Define for function Debug */
static uint8 slashCount = 0;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
static AtIpCore IpCoreGet(AtModule self)
    {
    AtDevice device = AtModuleDeviceGet(self);
    return AtDeviceIpCoreGet(device, AtModuleDefaultCoreGet(self));
    }

static void LongStickyPrint(AtModule self, uint32 localAddress, const char* name, eBool needClear, uint32 *longRegVal)
    {
    AtIpCore core = IpCoreGet(self);
    uint32 address = localAddress + Tha60210011ModuleClaBaseAddress((ThaModuleCla)self);

    AtOsalMemInit(longRegVal, 0, cThaLongRegMaxSize * sizeof(uint32));
    mModuleHwLongRead(self, address, longRegVal, cThaLongRegMaxSize, core);

    if (needClear)
        AtPrintc(cSevInfo, "\r\n");
    AtPrintc(cSevNormal, "%-20s (0x%08X): 0x%08x.%08x.%08x.%08x\r\n", name, address, longRegVal[3], longRegVal[2], longRegVal[1], longRegVal[0]);

    if (needClear)
        mModuleHwLongWrite(self, address, longRegVal, cThaLongRegMaxSize, core);
    }

static uint32 StickyPrint(AtModule self, uint32 address, const char* name, eBool needClear)
    {
    uint32 regVal;
    address = address + Tha60210011ModuleClaBaseAddress((ThaModuleCla)self);

    regVal = mModuleHwRead(self, address);

    if (needClear)
        AtPrintc(cSevInfo, "\r\n");

    AtPrintc(cSevNormal, "%-20s (0x%08X): 0x%08x\r\n", name, address, regVal);

    if (needClear)
        mModuleHwWrite(self, address, regVal);

    return regVal;
    }

static void DebugCounterPrint(AtModule self, uint32 address, const char* name)
    {
    uint32 regVal;
    address = address + Tha60210011ModuleClaBaseAddress((ThaModuleCla)self);

    regVal = mModuleHwRead(self, address);
    AtPrintc(cSevNormal, "%-20s (0x%08X): %u\r\n", name, address, regVal);
    }

static uint32 NumCheckingRules(AtModule self)
    {
    AtUnused(self);
    return 8;
    }

static void RuleDescriptionPrint(AtModule self, uint32 ruleId)
    {
    AtUnused(self);
    AtPrintc(cSevNormal, "[");
    if (ruleId == cTha60290081ClaLookupPort)       AtPrintc(cSevNormal, " Port");
    if (ruleId == cTha60290081ClaLookupPtch)       AtPrintc(cSevNormal, " Ptch");
    if (ruleId == cTha60290081ClaLookupFirstVlan)  AtPrintc(cSevNormal, " FirstVlan");
    if (ruleId == cTha60290081ClaLookupSecondVlan) AtPrintc(cSevNormal, " SecondVlan");
    if (ruleId == cTha60290081ClaLookupHash)       AtPrintc(cSevNormal, " Hash");
    AtPrintc(cSevNormal, " ]:\r\n");
    }

static const char* RuleCheckHw2String(uint8 value)
    {
    if (value == cTha60290081ClaLookupPort)       return "Port";
    if (value == cTha60290081ClaLookupPtch)       return "Ptch";
    if (value == cTha60290081ClaLookupFirstVlan)  return "FirstVlan";
    if (value == cTha60290081ClaLookupSecondVlan) return "SecondVlan";
    if (value == cTha60290081ClaLookupHash)       return "Hash";

    return "Unknown";
    }

static void LinkIdRulePrint(AtModule self, uint32 regVal)
    {
    uint32 linkIdTabIdMask  = cAf6_upen_rulechk_linkid_tabid_Mask;
    uint32 linkIdTabIdShift = cAf6_upen_rulechk_linkid_tabid_Shift;

    AtUnused(self);
    AtPrintc(cSevNormal, "    - Link ID     : ");
    AtPrintc(cSevInfo, "%s\r\n", RuleCheckHw2String((uint8)mRegField(regVal, linkIdTabId)));
    }

static void ServiceTypeRulePrint(uint32 regVal)
    {
    AtPrintc(cSevNormal, "    - Service     : ");
    AtPrintc(cSevInfo, "%s\r\n", RuleCheckHw2String((uint8)mRegField(regVal, cAf6_upen_rulechk_sertyp_tabid_)));
    }

static void FlowIdRulePrint(uint32 regVal)
    {
    AtPrintc(cSevNormal, "    - Flow ID     : ");
    AtPrintc(cSevInfo, "%s\r\n", RuleCheckHw2String(mRegField(regVal, cAf6_upen_rulechk_flowid_tabid_)));
    }

static eBool IsInUsed(uint32 regVal)
    {
    return (mRegField(regVal, cAf6_upen_rulechk_outen_) > 0);
    }

static void AllCheckingRulesPrint(AtModule self)
    {
    uint32 rule_i;

    AtPrintc(cSevInfo, "\r\n* CLA in-used checking rules\r\n");
    AtPrintc(cSevInfo, "========================================================\r\n");
    for (rule_i = 0; rule_i < NumCheckingRules(self); rule_i++)
        {
        uint32 address = cAf6Reg_upen_rulechk_Base + rule_i + Tha60210011ModuleClaBaseAddress((ThaModuleCla)self);
        uint32 regVal = mModuleHwRead(self, address);
        if (IsInUsed(regVal))
            {
            AtPrintc(cSevNormal, "Rule %02u (0x%08X) ", rule_i, address);
            RuleDescriptionPrint(self, rule_i);
            LinkIdRulePrint(self, regVal);

            ServiceTypeRulePrint(regVal);
            FlowIdRulePrint(regVal);
            AtPrintc(cSevNormal, "\r\n");
            }
        }
    }

static void PrintAlignment(void)
    {
    if (slashCount == 2)
        {
        AtPrintc(cSevNormal, "\r\n");
        slashCount = 0;
        }
    else
        {
        AtPrintc(cSevWarning, "|");
        slashCount++;
        }
    }

static void DebugPrintErrorBit(const char* title, uint32 hwValue, uint32 mask)
    {
    AtPrintc(cSevNormal, "    %-25s: ", title);
    if (hwValue & mask)
        AtPrintc(cSevCritical, "%-10s", "SET");
    else
        AtPrintc(cSevInfo, "%-10s", "CLEAR");

    PrintAlignment();
    AtStdFlush();
    }

static void DebugPrintNeutralBit(const char* title, uint32 hwValue, uint32 mask)
    {
    AtPrintc(cSevNormal, "    %-25s: ", title);
    if (hwValue & mask)
        AtPrintc(cSevInfo, "%-10s", "SET");
    else
        AtPrintc(cSevNormal, "%-10s", "CLEAR");

    PrintAlignment();
    AtStdFlush();
    }

static void DebugPrintStart(void)
    {
    if (slashCount > 0)
        {
        AtPrintc(cSevNormal, "\r\n");
        slashCount = 0;
        }
    }

static void DebugPrintStop(void)
    {
    if (slashCount > 0)
        AtPrintc(cSevNormal, "\r\n");

    slashCount = 0;
    }

static void DatashiftSticky(AtModule self, uint32 *longRegVal)
    {
    AtUnused(self);
    DebugPrintStart();

    DebugPrintNeutralBit("DATSHF_0 SSER CPU   ", longRegVal[1], cAf6_upen_datashidt_stk0_datsh0_ssercpu_Mask);
    DebugPrintNeutralBit("DATSHF_0 SSER OAM   ", longRegVal[1], cAf6_upen_datashidt_stk0_datsh0_sseroam_Mask);
    DebugPrintNeutralBit("DATSHF_0 SSER DATA  ", longRegVal[1], cAf6_upen_datashidt_stk0_datsh0_sserdata_Mask);
    DebugPrintNeutralBit("DATSHF_0 SSER PW    ", longRegVal[1], cAf6_upen_datashidt_stk0_datsh0_sserpw_Mask);
    DebugPrintNeutralBit("DATSHF_0 SER PTP    ", longRegVal[1], cAf6_upen_datashidt_stk0_datsh0_serptp_Mask);
    DebugPrintNeutralBit("DATSHF_0 SER EPASS  ", longRegVal[1], cAf6_upen_datashidt_stk0_datsh0_serepass_Mask);
    DebugPrintNeutralBit("DATSHF_0 SER DCC    ", longRegVal[1], cAf6_upen_datashidt_stk0_datsh0_serdcc_Mask);
    DebugPrintNeutralBit("DATSHF_0 SER IMSG   ", longRegVal[1], cAf6_upen_datashidt_stk0_datsh0_serimsg_Mask);
    DebugPrintNeutralBit("DATSHF_0 SER CEM    ", longRegVal[1], cAf6_upen_datashidt_stk0_datsh0_sercem_Mask);
    DebugPrintNeutralBit("DATSHF_0 SER DROP   ", longRegVal[1], cAf6_upen_datashidt_stk0_datsh0_serdrop_Mask);
    DebugPrintNeutralBit("DATSHF_0 MEF        ", longRegVal[0], cAf6_upen_datashidt_stk0_datsh0_mef_Mask);
    DebugPrintNeutralBit("DATSHF_0 OAM        ", longRegVal[0], cAf6_upen_datashidt_stk0_datsh0_oam_Mask);
    DebugPrintNeutralBit("DATSHF_0 MPLS MUL   ", longRegVal[0], cAf6_upen_datashidt_stk0_datsh0_mmpls_Mask);
    DebugPrintNeutralBit("DATSHF_0 MPLS UNI   ", longRegVal[0], cAf6_upen_datashidt_stk0_datsh0_umpls_Mask);
    DebugPrintNeutralBit("DATSHF_0 IPv6       ", longRegVal[0], cAf6_upen_datashidt_stk0_datsh0_ipv6_Mask);
    DebugPrintNeutralBit("DATSHF_0 IPv4       ", longRegVal[0], cAf6_upen_datashidt_stk0_datsh0_ipv4_Mask);
    DebugPrintNeutralBit("DATSHF_0 4LABEL :   ", longRegVal[0], cAf6_upen_datashidt_stk0_datsh0_4label_Mask);
    DebugPrintNeutralBit("DATSHF_0 3LABEL :   ", longRegVal[0], cAf6_upen_datashidt_stk0_datsh0_3label_Mask);
    DebugPrintNeutralBit("DATSHF_0 2LABEL :   ", longRegVal[0], cAf6_upen_datashidt_stk0_datsh0_2label_Mask);
    DebugPrintNeutralBit("DATSHF_0 1LABEL :   ", longRegVal[0], cAf6_upen_datashidt_stk0_datsh0_1label_Mask);
    DebugPrintNeutralBit("DATSHF_0 0LABEL :   ", longRegVal[0], cAf6_upen_datashidt_stk0_datsh0_0label_Mask);
    DebugPrintNeutralBit("DATSHF_0 2VLAN      ", longRegVal[0], cAf6_upen_datashidt_stk0_datsh0_2vlan_Mask);
    DebugPrintNeutralBit("DATSHF_0 1VLAN      ", longRegVal[0], cAf6_upen_datashidt_stk0_datsh0_1vlan_Mask);
    DebugPrintNeutralBit("DATSHF_0 0VLAN      ", longRegVal[0], cAf6_upen_datashidt_stk0_datsh0_0vlan_Mask);
    DebugPrintNeutralBit("DATSHF_0 PTP  IND   ", longRegVal[0], cAf6_upen_datashidt_stk0_datsh0_ptpind_Mask);
    DebugPrintNeutralBit("DATSHF_0 UDP  IND   ", longRegVal[0], cAf6_upen_datashidt_stk0_datsh0_udpind_Mask);
    DebugPrintNeutralBit("DATSHF_0 LKUP WIN   ", longRegVal[0], cAf6_upen_datashidt_stk0_datsh0_sserpw_Mask);
    DebugPrintErrorBit("DATSHF_0 MRU ERROR  ", longRegVal[0], cAf6_upen_datashidt_stk0_datsh0_err4_Mask);
    DebugPrintErrorBit("DATSHF_0 BLK EMPTY  ", longRegVal[0], cAf6_upen_datashidt_stk0_datsh0_err3_Mask);
    DebugPrintErrorBit("DATSHF_0 LKUP FAIL  ", longRegVal[0], cAf6_upen_datashidt_stk0_datsh0_err2_Mask);
    DebugPrintErrorBit("DATSHF_0 ETH ERROR  ", longRegVal[0], cAf6_upen_datashidt_stk0_datsh0_err0_Mask);
    DebugPrintNeutralBit("DATSHF_0 EOP        ", longRegVal[0], cAf6_upen_datashidt_stk0_datsh0_eop_Mask);
    DebugPrintNeutralBit("DATSHF_0 SOP        ", longRegVal[0], cAf6_upen_datashidt_stk0_datsh0_sop_Mask);
    DebugPrintNeutralBit("DATSHF_0 VALID      ", longRegVal[0], cAf6_upen_datashidt_stk0_datsh0_vld_Mask);

    DebugPrintStop();
    }

static void Eth2ClaSticky(AtModule self, uint32 regVal)
    {
    uint32 i = 0;
    char buf[100];

    AtUnused(self);
    DebugPrintStart();

    AtSnprintf(buf, sizeof(buf), "Eth#%u %s", i, "VALID");
    DebugPrintNeutralBit(buf, regVal, cAf6_upen_eth2cla_stk_eth0_vld_Mask);

    AtSnprintf(buf, sizeof(buf), "Eth#%u %s", i, "SOP");
    DebugPrintNeutralBit(buf, regVal, cAf6_upen_eth2cla_stk_eth0_sop_Mask);

    AtSnprintf(buf, sizeof(buf), "Eth#%u %s", i, "EOP");
    DebugPrintNeutralBit(buf, regVal, cAf6_upen_eth2cla_stk_eth0_eop_Mask);

    AtSnprintf(buf, sizeof(buf), "Eth#%u %s", i, "ERR");
    DebugPrintErrorBit(buf, regVal, cAf6_upen_eth2cla_stk_eth0_err_Mask);

    DebugPrintStop();
    }

static void Cla2EPassSticky(AtModule self, uint32 hwValue)
    {
    AtUnused(self);
    DebugPrintStart();
    DebugPrintNeutralBit("sticky cla to epa eth#0",
                    cAf6_upen_cla2epa_stk_cla2epa_stk0_Mask,
                    hwValue);
    DebugPrintNeutralBit("sticky cla to epa eth#1",
                    cAf6_upen_cla2epa_stk_cla2epa_stk1_Mask,
                    hwValue);
    DebugPrintStop();
    }

static void Cla2PTPSticky(AtModule self, uint32 hwValue)
    {
    AtUnused(self);
    DebugPrintStart();
    DebugPrintNeutralBit("sticky cla to ptp eth#0",
                    cAf6_upen_cla2ptp_stk_cla2ptp_stk0_Mask,
                    hwValue);
    DebugPrintNeutralBit("sticky cla to ptp eth#1",
                    cAf6_upen_cla2ptp_stk_cla2ptp_stk1_Mask,
                    hwValue);
    DebugPrintStop();
    }

static void Cla2PdaCEM_ImsgSticky(AtModule self, uint32 *longRegVal)
    {
    AtUnused(self);
    DebugPrintNeutralBit("ER MLFR DATA", longRegVal[1], cAf6_upen_cla2pda_stk_ser_mlfrdata_Mask);
    DebugPrintNeutralBit("SER MLFR OAM", longRegVal[1], cAf6_upen_cla2pda_stk_ser_mlfroam_Mask);
    DebugPrintNeutralBit("SER FR DATA", longRegVal[1], cAf6_upen_cla2pda_stk_ser_frdata_Mask);
    DebugPrintNeutralBit("SER FR OAM ", longRegVal[1], cAf6_upen_cla2pda_stk_ser_froam_Mask);

    DebugPrintNeutralBit("SER MLPPP DATA", longRegVal[0], cAf6_upen_cla2pda_stk_ser_mlpppdata_Mask);
    DebugPrintNeutralBit("SER MLPPP OAM", longRegVal[0], cAf6_upen_cla2pda_stk_ser_mlpppoam_Mask);
    DebugPrintNeutralBit("SER PPP DATA", longRegVal[0], cAf6_upen_cla2pda_stk_ser_pppdata_Mask);
    DebugPrintNeutralBit("SER PPP OAM", longRegVal[0], cAf6_upen_cla2pda_stk_ser_pppoam_Mask);

    DebugPrintNeutralBit("SER PPP BRD", longRegVal[0], cAf6_upen_cla2pda_stk_ser_pppbrd_Mask);
    DebugPrintNeutralBit("SER cHDLC", longRegVal[0], cAf6_upen_cla2pda_stk_ser_chdlc_Mask);
    DebugPrintNeutralBit("SER EPASS", longRegVal[0], cAf6_upen_cla2pda_stk_ser_epass_Mask);
    DebugPrintNeutralBit("SER HDLC", longRegVal[0], cAf6_upen_cla2pda_stk_ser_hdlc_Mask);
    DebugPrintNeutralBit("SER CEP", longRegVal[0], cAf6_upen_cla2pda_stk_ser_cep_Mask);
    DebugPrintNeutralBit("SER EOP/EOS", longRegVal[0], cAf6_upen_cla2pda_stk_ser_eop_Mask);

    DebugPrintNeutralBit("SER CES", longRegVal[0], cAf6_upen_cla2pda_stk_ser_ces_Mask);
    DebugPrintNeutralBit("SER DISCARD", longRegVal[0], cAf6_upen_cla2pda_stk_ser_discard_Mask);

    DebugPrintNeutralBit("ETHTYPE MEF", longRegVal[0], cAf6_upen_cla2pda_stk_etyp_mef_Mask);
    DebugPrintNeutralBit("ETHTYPE OAM", longRegVal[0], cAf6_upen_cla2pda_stk_etyp_oamind_Mask);
    DebugPrintNeutralBit("ETHTYPE MPLS multicast", longRegVal[0], cAf6_upen_cla2pda_stk_etyp_mmplsind_Mask);
    DebugPrintNeutralBit("ETHTYPE MPLS unicast", longRegVal[0], cAf6_upen_cla2pda_stk_etyp_umplsind_Mask);
    DebugPrintNeutralBit("ETHTYPE IPV6", longRegVal[0], cAf6_upen_cla2pda_stk_etyp_ipv6ind_Mask);
    DebugPrintNeutralBit("ETHTYPE IPV4", longRegVal[0], cAf6_upen_cla2pda_stk_etyp_ipv4ind_Mask);

    DebugPrintErrorBit("DISCARD SERVICE", longRegVal[0], cAf6_upen_cla2pda_stk_cla2pda_err8_Mask);
    DebugPrintErrorBit("MALFORM ERROR", longRegVal[0], cAf6_upen_cla2pda_stk_cla2pda_err7_Mask);
    DebugPrintErrorBit("PT ERROR", longRegVal[0], cAf6_upen_cla2pda_stk_cla2pda_err6_Mask);
    DebugPrintErrorBit("SSRC ERROR", longRegVal[0], cAf6_upen_cla2pda_stk_cla2pda_err5_Mask);
    DebugPrintErrorBit("MRU ERROR", longRegVal[0], cAf6_upen_cla2pda_stk_cla2pda_err4_Mask);
    DebugPrintErrorBit("BUFFER FULL", longRegVal[0], cAf6_upen_cla2pda_stk_cla2pda_err3_Mask);
    DebugPrintErrorBit("LOOKUP ERROR", longRegVal[0], cAf6_upen_cla2pda_stk_cla2pda_err2_Mask);
    DebugPrintErrorBit("MACDA ERROR", longRegVal[0], cAf6_upen_cla2pda_stk_cla2pda_err1_Mask);

    DebugPrintErrorBit("CLA2PDA ETH ERROR", longRegVal[0], cAf6_upen_cla2pda_stk_cla2pda_err0_Mask);
    DebugPrintNeutralBit("CLA2PDA EOP", longRegVal[0], cAf6_upen_cla2pda_stk_cla2pda_eop_Mask);
    DebugPrintNeutralBit("CLA2PDA SOP", longRegVal[0], cAf6_upen_cla2pda_stk_cla2pda_sop_Mask);
    DebugPrintNeutralBit("CLA2PDA VALID", longRegVal[0], cAf6_upen_cla2pda_stk_cla2pda_vld_Mask);
    }

static void Cla2PdaSticky(AtModule self, uint32 *longRegVal)
    {
    char buf[100];

    DebugPrintStart();
    AtSnprintf(buf, sizeof(buf), "cla2pda eth# sticky");
    LongStickyPrint(self, cAf6Reg_upen_cla2pda_stk_Base, buf, cAtTrue, longRegVal);

    DebugPrintNeutralBit("EOF", longRegVal[2], cAf6_upen_cla2pda_stk_eof_Mask);
    DebugPrintNeutralBit("SOF", longRegVal[2], cAf6_upen_cla2pda_stk_sof_Mask);
    DebugPrintNeutralBit("OCW_LBIT", longRegVal[2], cAf6_upen_cla2pda_stk_ocw_lbit_Mask);
    DebugPrintNeutralBit("OCW_RBIT", longRegVal[2], cAf6_upen_cla2pda_stk_ocw_rbit_Mask);
    DebugPrintNeutralBit("OCW_PBITt", longRegVal[2], cAf6_upen_cla2pda_stk_ocw_pbit_Mask);
    DebugPrintNeutralBit("OCW_FBIT1", longRegVal[2], cAf6_upen_cla2pda_stk_ocw_fbit1_Mask);
    DebugPrintNeutralBit("OCW_FBIT0", longRegVal[2], cAf6_upen_cla2pda_stk_ocw_fbit0_Mask);
    Cla2PdaCEM_ImsgSticky(self, longRegVal);
    DebugPrintStop();
    }

static void ParserSticky0(AtModule self, uint32 *longRegVal)
    {
    AtUnused(self);
    DebugPrintStart();
    DebugPrintNeutralBit("PARSER_0 FREE BLK", longRegVal[0], cAf6_upen_parser_stk0_par0_free_Mask);
    DebugPrintNeutralBit("PARSER_0 EOSEG", longRegVal[0], cAf6_upen_parser_stk0_par0_eoseg_Mask);
    DebugPrintErrorBit("PARSER_0 BLK EMPTY", longRegVal[0], cAf6_upen_parser_stk0_par0_blkemp_Mask);
    DebugPrintErrorBit("PARSER_0 MRU ERROR", longRegVal[0], cAf6_upen_parser_stk0_par0_errmru_Mask);
    DebugPrintErrorBit("PARSER_0 PKT SHORT", longRegVal[0], cAf6_upen_parser_stk0_par0_err_Mask);
    DebugPrintNeutralBit("PARSER_0 DONE", longRegVal[0], cAf6_upen_parser_stk0_par0_done_Mask);
    DebugPrintStop();
    }

static void ParserSticky1(AtModule self, uint32 *longRegVal)
    {
    AtUnused(self);
    DebugPrintStart();
    DebugPrintNeutralBit("PARSER_0 UDP PRT", longRegVal[0], cAf6_upen_parser_stk1_par0_udpprt_Mask);
    DebugPrintNeutralBit("PARSER_0 IP DA", longRegVal[0], cAf6_upen_parser_stk1_par0_ipda_Mask);
    DebugPrintNeutralBit("PARSER_0 IP SA", longRegVal[0], cAf6_upen_parser_stk1_par0_ipsa_Mask);
    DebugPrintNeutralBit("PARSER_0 UDP PTL", longRegVal[0], cAf6_upen_parser_stk1_par0_udpptl_Mask);
    DebugPrintNeutralBit("PARSER_0 IPVER", longRegVal[0], cAf6_upen_parser_stk1_par0_ipver_Mask);
    DebugPrintNeutralBit("PARSER_0 MAC", longRegVal[0], cAf6_upen_parser_stk1_par0_mac_Mask);
    DebugPrintNeutralBit("PARSER_0 MEFECID", longRegVal[0], cAf6_upen_parser_stk1_par0_mefecid_Mask);
    DebugPrintNeutralBit("PARSER_0 PWLABEL", longRegVal[0], cAf6_upen_parser_stk1_par0_pwlabel_Mask);
    DebugPrintNeutralBit("PARSER_0 ETHTYPE", longRegVal[0], cAf6_upen_parser_stk1_par0_ethtyp_Mask);
    DebugPrintNeutralBit("PARSER_0 SNDVLAN", longRegVal[0], cAf6_upen_parser_stk1_par0_sndvlan_Mask);
    DebugPrintNeutralBit("PARSER_0 FSTVLAN", longRegVal[0], cAf6_upen_parser_stk1_par0_fstvlan_Mask);
    DebugPrintNeutralBit("PARSER_0 PTCH", longRegVal[0], cAf6_upen_parser_stk1_par0_ptch_Mask);
    DebugPrintStop();
    }

static void LookupSticky(AtModule self, uint32 *longRegVal)
    {
    AtUnused(self);
    DebugPrintStart();
    DebugPrintNeutralBit("PORT    TABLE WIN", longRegVal[2], cAf6_upen_lkup_stk_porttab_win_Mask);
    DebugPrintNeutralBit("FSTVLAN TABLE WIN", longRegVal[2], cAf6_upen_lkup_stk_fvlntab_win_xfi0_Mask);
    DebugPrintNeutralBit("SNDVLAN TABLE WIN", longRegVal[2], cAf6_upen_lkup_stk_svlntab_win_xfi0_Mask);
    DebugPrintNeutralBit("PWHASH  TABLE WIN", longRegVal[2], cAf6_upen_lkup_stk_pwhash_win_Mask);
    DebugPrintNeutralBit("ETHTYPE TABLE WIN", longRegVal[2], cAf6_upen_lkup_stk_etypcam_win_Mask);
    DebugPrintNeutralBit("RULECHK TABLE WIN", longRegVal[2], cAf6_upen_lkup_stk_rulechk_win_Mask);
    DebugPrintNeutralBit("PWHASH  TABLE WIN", longRegVal[2], cAf6_upen_lkup_stk_pwhash_win_Mask);

    DebugPrintNeutralBit("ETHTYPE TABLE WIN", longRegVal[2], cAf6_upen_lkup_stk_etypcam_win_Mask);
    DebugPrintNeutralBit("RULECHK TABLE WIN", longRegVal[2], cAf6_upen_lkup_stk_rulechk_win_Mask);
    DebugPrintNeutralBit("LOOKUP_0 PTP IND  ", longRegVal[0], cAf6_upen_lkup_stk_lkup0_ptpind_Mask);
    DebugPrintNeutralBit("LOOKUP_0 UDP IND  ", longRegVal[0], cAf6_upen_lkup_stk_lkup0_udpind_Mask);
    DebugPrintNeutralBit("LOOKUP_0 EOSEG    ", longRegVal[0], cAf6_upen_lkup_stk_lkup0_eoseg_Mask);
    DebugPrintNeutralBit("LOOKUP_0 BLK EMPTY", longRegVal[0], cAf6_upen_lkup_stk_lkup0_blkemp_Mask);
    DebugPrintErrorBit("LOOKUP_0 MRU ERR  ", longRegVal[0], cAf6_upen_lkup_stk_lkup0_errmru_Mask);
    DebugPrintErrorBit("LOOKUP_0 FAIL     ", longRegVal[0], cAf6_upen_lkup_stk_lkup0_fail_Mask);
    DebugPrintNeutralBit("LOOKUP_0 DONE     ", longRegVal[0], cAf6_upen_lkup_stk_lkup0_done_Mask);
    DebugPrintStop();
    }

static void EgrhdrSticky(AtModule self, uint32 regVal)
    {
    uint8 i, j;
    uint32 maskId = 0;
    char buf[100];
    static const char *cAtEgrhdrTypeStr[] = {"EGRLK", "PSNPRO", "RMVRTP", "RMVPAD", "SUBENC", "TDMENC"};
    static const char *cAtSticktyTypeStr[] = {"VALID", "SOP", "EOP", "ERROR"};

    AtUnused(self);
    DebugPrintStart();

    for (i = 0; i < mCount(cAtEgrhdrTypeStr); i++)
        {
        for (j = 0; j < mCount(cAtSticktyTypeStr); j++)
            {
            AtSnprintf(buf, 100, "%s %s", cAtEgrhdrTypeStr[i], cAtSticktyTypeStr[j]);
            DebugPrintNeutralBit(buf, regVal, cAf6_upen_egrhdr_stk_Mask(maskId));
            maskId = maskId + 1;
            }
        }

    DebugPrintStop();
    }

static void Shift64Sticky(AtModule self, uint32 regVal)
    {
    AtUnused(self);
    DebugPrintStart();
    DebugPrintNeutralBit("SHIFT64 FIFO FULL", regVal, cAf6_upen_shift64_stk_shift64_fffull_Mask);
    DebugPrintStop();
    }

static void AllStickies(AtModule self, uint32 regVal)
    {
    uint8 i;
    char buf[100];

    AtUnused(self);
    DebugPrintStart();

    DebugPrintNeutralBit("SAME FREE", regVal, cAf6_upen_allc_stk_samefree_Mask);
    DebugPrintNeutralBit("BLK EMPTY", regVal, cAf6_upen_allc_stk_blkemp_Mask);
    DebugPrintNeutralBit("PARSER FREE", regVal, cAf6_upen_allc_stk_parfre_Mask);
    DebugPrintNeutralBit("BUFFER FREE", regVal, cAf6_upen_allc_stk_buffre_Mask);
    DebugPrintNeutralBit("BLK REQUEST", regVal, cAf6_upen_allc_stk_blkreq_Mask);
    DebugPrintNeutralBit("FIFO XFI FULL", regVal, cAf6_upen_allc_stk_ffxfi_full_Mask);

    for (i = 0; i < cNumEthPorts; i++)
        {
        AtSnprintf(buf, 100, "FIFO GE%i", i + 4);
        DebugPrintNeutralBit(buf, regVal, cAf6_upen_allc_stk_ffge4_full_id_Mask(i));
        }

    DebugPrintStop();
    }

eAtRet Tha60290081ModuleClaDebug(ThaModuleCla self)
    {
    AtModule module = (AtModule)self;
    uint32 regVal;
    uint32 longRegVal[cThaLongRegMaxSize];

    AtPrintc(cSevInfo, "\r\n* CLA debug information\r\n");
    AtPrintc(cSevInfo, "========================================================\r\n");

    /* Keep general debug stickies information for HW look */
    LongStickyPrint(module, cAf6Reg_upen_eth2cla_stk_Base, "eth2cla sticky", cAtFalse, longRegVal);
    LongStickyPrint(module, cAf6Reg_upen_cla2epa_stk_Base, "cla2epa sticky", cAtFalse, longRegVal);
    LongStickyPrint(module, cAf6Reg_upen_cla2ptp_stk_Base, "cla2ptp sticky", cAtFalse, longRegVal);
    LongStickyPrint(module, cAf6Reg_upen_cla2pda_stk_Base, "cla2pda sticky", cAtFalse, longRegVal);
    LongStickyPrint(module, cAf6Reg_upen_parser_stk0_Base, "parser sticky 0", cAtFalse, longRegVal);
    LongStickyPrint(module, cAf6Reg_upen_parser_stk1_Base, "parser sticky 1", cAtFalse, longRegVal);
    LongStickyPrint(module, cAf6Reg_upen_lkup_stk_Base, "lookup sticky", cAtFalse, longRegVal);
    LongStickyPrint(module, cAf6Reg_upen_datashidt_stk0_Base, "datashift sticky", cAtFalse, longRegVal);
    StickyPrint(module, cAf6Reg_upen_egrhdr_stk_Base, "egrhdr sticky", cAtFalse);
    StickyPrint(module, cAf6Reg_upen_shift64_stk_Base, "shift64 sticky", cAtFalse);
    StickyPrint(module, cAf6Reg_upen_allc_stk_Base, "allc sticky", cAtFalse);
    DebugCounterPrint(module, cAf6Reg_upen_allc_blkfre_cnt_Base, "allc_blkfre_cnt");
    DebugCounterPrint(module, cAf6Reg_upen_allc_blkiss_cnt_Base, "allc_blkiss_cnt");

    /* Detail prints */
    regVal = StickyPrint(module, cAf6Reg_upen_eth2cla_stk_Base, "eth2cla sticky", cAtTrue);
    Eth2ClaSticky(module, regVal);

    regVal = StickyPrint(module, cAf6Reg_upen_cla2epa_stk_Base, "cla2epa sticky", cAtTrue);
    Cla2EPassSticky(module, regVal);
    regVal = StickyPrint(module, cAf6Reg_upen_cla2ptp_stk_Base, "cla2ptp sticky", cAtTrue);
    Cla2PTPSticky(module, regVal);

    LongStickyPrint(module, cAf6Reg_upen_cla2pda_stk_Base, "cla2pda sticky", cAtTrue, longRegVal);
    Cla2PdaSticky(module, longRegVal);
    LongStickyPrint(module, cAf6Reg_upen_parser_stk0_Base, "parser sticky 0", cAtTrue, longRegVal);
    ParserSticky0(module, longRegVal);
    LongStickyPrint(module, cAf6Reg_upen_parser_stk1_Base, "parser sticky 1", cAtTrue, longRegVal);
    ParserSticky1(module, longRegVal);
    LongStickyPrint(module, cAf6Reg_upen_lkup_stk_Base, "lookup sticky", cAtTrue, longRegVal);
    LookupSticky(module,longRegVal);
    LongStickyPrint(module, cAf6Reg_upen_datashidt_stk0_Base, "datashift sticky", cAtTrue, longRegVal);
    DatashiftSticky(module, longRegVal);

    regVal = StickyPrint(module, cAf6Reg_upen_egrhdr_stk_Base, "egrhdr sticky", cAtTrue);
    EgrhdrSticky(module, regVal);
    regVal = StickyPrint(module, cAf6Reg_upen_shift64_stk_Base, "shift64 sticky", cAtTrue);
    Shift64Sticky(module, regVal);
    regVal = StickyPrint(module, cAf6Reg_upen_allc_stk_Base, "allc sticky", cAtTrue);
    AllStickies(module, regVal);

    /* Rules */
    AllCheckingRulesPrint(module);

    return cAtOk;
    }


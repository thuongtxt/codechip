/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60290081Hbce.c
 *
 * Created Date: Jul 5, 2019
 *
 * Description : HBCE
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/util/crchash/ThaCrcHashInternal.h"
#include "../../../../default/util/ThaBitMask.h"
#include "../../../../default/man/ThaDevice.h"
#include "../../../../default/cla/hbce/ThaHbceEntry.h"
#include "../../../../default/util/crchash/ThaCrcHashInternal.h"
#include "../../../Tha60210011/cla/Tha60210011ModuleCla.h"
#include "../../../Tha60210011/cla/hbce/Tha60210011HbceInternal.h"
#include "Tha60290081Hbce.h"
#include "../Tha60290081ModuleCla.h"
#include "../Tha60290081ModuleClaReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60290081Hbce)self)

#define cThaHbcePwLabelMask  cBit19_0
#define cThaHbcePwLabelShift 0

#define cThaHbcePsnTypeMask  cBit21_20
#define cThaHbcePsnTypeShift 20

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290081Hbce * Tha60290081Hbce;
typedef struct tTha60290081Hbce
    {
    tTha60210011Hbce super;

    ThaCrcHash crcHash;
    }tTha60290081Hbce;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods m_AtObjectOverride;
static tThaHbceMethods m_ThaHbceOverride;
static tThaHbceEntityMethods m_ThaHbceEntityOverride;
static tTha60210011HbceMethods m_Tha60210011HbceOverride;

/* Super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;
static const tThaHbceMethods *m_ThaHbceMethods = NULL;
static const tThaHbceEntityMethods *m_ThaHbceEntityMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 HbcePktType(eAtPwPsnType psnType)
    {
    if (psnType == cAtPwPsnTypeMpls) return 0;
    if (psnType == cAtPwPsnTypeMef)  return 1;
    if (psnType == cAtPwPsnTypeUdp)  return 2;

    return 0;
    }

static uint32 HwHbceLabel(ThaHbce self, AtEthPort ethPort, uint32 label, eAtPwPsnType psnType)
    {
    uint32 hwLabel = 0;
    AtUnused(self);
    AtUnused(ethPort);

    mRegFieldSet(hwLabel, cThaHbcePwLabel, label);
    mRegFieldSet(hwLabel, cThaHbcePsnType, HbcePktType(psnType));

    return hwLabel;
    }

static AtEthPort EthPortFromHeaderController(ThaPwHeaderController controller)
    {
    AtPw pwAdapter = (AtPw)ThaPwHeaderControllerAdapterGet(controller);
    return AtPwEthPortGet(pwAdapter);
    }

static uint32 NumKeyBit(ThaHbce self)
    {
    AtUnused(self);
    return 22;
    }

static ThaCrcHash HashGet(ThaHbce self)
    {
    return mThis(self)->crcHash;
    }

static uint32 HwHbceLabelGet(ThaHbce self, ThaPwHeaderController controller, AtPwPsn newPsn)
    {
    return HwHbceLabel(self,
                       EthPortFromHeaderController(controller),
                       ThaHbcePsnLabelForHashing(self, newPsn),
                       ThaHbcePsnTypeForHashing(self, newPsn));
    }

static eBool NewPwLabelExistInHashEntry(ThaHbce self, ThaPwHeaderController controller, AtPwPsn newPsn, ThaHbceEntry hashEntry)
    {
    uint32 hwLabel = HwHbceLabelGet(self, controller, newPsn);
    ThaCrcHashEntry crcEntry;
    ThaHbceMemoryCellContent cellContent;
    ThaPwHeaderController existingController;
    AtUnused(hashEntry);

    /* If there is an existing entry with this key */
    crcEntry = ThaCrcHashEntryGetByKey(HashGet(self), &hwLabel, NumKeyBit(self));
    if (crcEntry == NULL)
        return cAtFalse;

    cellContent = ThaHbceEntryCellContentAtIndex(hashEntry, 0);
    existingController = ThaHbceMemoryCellContentPwHeaderControllerGet(cellContent);

    /* Ignore itself */
    return (existingController != controller) ? cAtTrue : cAtFalse;
    }

static uint32 ExistHashIndexGet(ThaHbce self, uint32 hwLabel)
    {
    uint32 camCell_i;
    ThaHbceMemoryPool memPool = ThaHbceMemoryPoolGet(self);
    ThaCrcHashEntry entry = ThaCrcHashEntryGetByKey(HashGet(self), &hwLabel, NumKeyBit(self));
    if (entry)
        return ThaCrcHashEntryFlatIdGet(entry);

    /* Find in CAM */
    for (camCell_i = ThaHbceMaxNumEntries(self); camCell_i < ThaHbceMemoryPoolMaxNumCells(memPool); camCell_i++)
        {
        ThaHbceMemoryCell memCell = ThaHbceMemoryPoolCellAtIndex(memPool, camCell_i);
        ThaHbceMemoryCellContent content = ThaHbceMemoryCellContentGet(memCell);
        uint32* remainedBits;
        uint8 sizeInDword;

        if (content == NULL)
            continue;

        remainedBits = ThaHbceMemoryCellContentRemainedBitsGet(content, &sizeInDword);
        if ((sizeInDword == 0) || (remainedBits[0] != hwLabel))
            continue;

        return camCell_i;
        }

    return cInvalidUint32;
    }

static uint32 AllocateExtraIndex(ThaHbce self)
    {
    ThaHbceMemoryPool memPool = ThaHbceMemoryPoolGet(self);
    return Tha60210011HbceMemoryPoolFreeExtraPointerListPop(memPool);
    }

static void ExtraIndexUseSet(ThaHbce self, uint32 extraIndex)
    {
    ThaHbceMemoryPool memPool = ThaHbceMemoryPoolGet(self);
    Tha60210011HbceMemoryPoolFreeExtraPointerInUsedSet(memPool, extraIndex);
    }

static void FreeExtraIndex(ThaHbce self, uint32 extraIndex)
    {
    ThaHbceMemoryPool memPool = ThaHbceMemoryPoolGet(self);
    Tha60210011HbceMemoryPoolFreeExtraPointerListPush(memPool, extraIndex);
    }

static uint32 PsnHash(ThaHbce self, uint8 partId, AtEthPort ethPort, uint32 label, eAtPwPsnType psnType, uint32 *remainBits)
    {
    uint32 hwLabel = HwHbceLabel(self, ethPort, label, psnType);
    ThaCrcHashEntry entry;
    uint32 freeCam, existingHash;
    AtUnused(partId);

    /* Entry maybe has been allocated for this label before */
    existingHash = ExistHashIndexGet(self, hwLabel);
    if (existingHash != cInvalidUint32)
        return existingHash;

    if (remainBits)
        remainBits[0] = hwLabel;

    entry = ThaCrcHashFreeEntryGet(HashGet(self), &hwLabel, NumKeyBit(self));
    if (entry)
        return ThaCrcHashEntryFlatIdGet(entry);

    /* Can not find CRC entry, try using CAM */
    freeCam = AllocateExtraIndex(self);
    if (freeCam == cInvalidUint32)
        return freeCam;

    return freeCam + ThaHbceMaxNumEntries(self);
    }

static ThaHbceMemoryCell AllocateCellForPw(ThaHbce self, ThaPwHeaderController controller, uint32 hashIndex)
    {
    ThaHbceMemoryPool memPool = ThaHbceMemoryPoolGet(self);
    ThaHbceEntry hashEntry = ThaHbceEntryAtIndex(self, hashIndex);
    AtPwPsn psn = ThaPwHeaderControllerPsnGet(controller);

    /* Check to see if there is existing pw that has same label as adding pw, if yes, return NULL */
    if (ThaHbceNewPwLabelExistInHashEntry(self, controller, psn, hashEntry))
        return NULL;

    if (hashIndex < ThaHbceMaxNumEntries(self))
        {
        uint32 hashKey = HwHbceLabelGet(self, controller, psn);
        ThaCrcHashUseEntry(HashGet(self), hashIndex, &hashKey, NumKeyBit(self));
        }
    else
        {
        uint32 cellIndex = hashIndex - ThaHbceMaxNumEntries(self);
        ExtraIndexUseSet(self, cellIndex);
        }

    return ThaHbceMemoryPoolUseCell(memPool, hashIndex);
    }

static ThaHbceEntry EntryObjectCreate(ThaHbce self, uint32 entryIndex)
    {
    return ThaHbceEntryNew(self, entryIndex);
    }

static eAtRet HashEntryTabCtrlApply(Tha60210011Hbce self, uint32 entryIndex)
    {
    /* Nothing to do */
    AtUnused(self);
    AtUnused(entryIndex);
    return cAtOk;
    }

static ThaHbceMemoryPool MemoryPoolCreate(ThaHbce self)
    {
    return Tha60290081HbceMemoryPoolNew(self);
    }

static void CrcHashSetup(ThaCrcHash hash)
    {
    ThaCrcHashTableCrcPolyAdd(ThaCrcHashTableGet(hash, 0), 0x8C3); /* x^11+x^7+x^6+x^1+x^0 */
    ThaCrcHashTableCrcPolyAdd(ThaCrcHashTableGet(hash, 0), 0xA31); /* x^11+x^9+x^5+x^4+x^0 */
    ThaCrcHashTableCrcPolyAdd(ThaCrcHashTableGet(hash, 1), 0xB29); /* x^11+x^9+x^8+x^5+x^3+x^0 */
    ThaCrcHashTableCrcPolyAdd(ThaCrcHashTableGet(hash, 1), 0xBC3); /* x^11+x^9+x^8+x^7+x^6+x^1+x^0 */
    ThaCrcHashTableCrcPolyAdd(ThaCrcHashTableGet(hash, 2), 0xD2B); /* x^11+x^10+x^8+x^5+x^3+x^1+x^0 */
    ThaCrcHashTableCrcPolyAdd(ThaCrcHashTableGet(hash, 2), 0xDDB); /* x^11+x^10+x^8+x^7+x^6+x^4+x^3+x^1+x^0 */
    ThaCrcHashTableCrcPolyAdd(ThaCrcHashTableGet(hash, 3), 0xF7F); /* x^11+x^10+x^9+x^8+x^6+x^5+x^4+x^3+x^2+x^1+x^0 */
    ThaCrcHashTableCrcPolyAdd(ThaCrcHashTableGet(hash, 3), 0xFD3); /* x^11+x^10+x^9+x^8+x^7+x^6+x^4+x^1+x^0 */
    }

static void CellContentCopy(ThaHbceMemoryCellContent fromContent, ThaHbceMemoryCellContent toContent)
    {
    uint32* remainedBits;
    uint8 sizeInDwords;

    ThaHbceMemoryCellContentEnable(toContent, ThaHbceMemoryCellContentIsEnabled(fromContent));
    ThaHbceMemoryCellContentPwHeaderControllerSet(toContent, ThaHbceMemoryCellContentPwHeaderControllerGet(fromContent));

    remainedBits = ThaHbceMemoryCellContentRemainedBitsGet(fromContent, &sizeInDwords);
    ThaHbceMemoryCellContentRemainedBitsSet(toContent, remainedBits, sizeInDwords);
    }

static AtIpCore IpCore(ThaHbce self)
    {
    return ThaHbceCoreGet(self);
    }

static uint32 CopyMovingEntryToCam(ThaHbce self, ThaHbceMemoryCell fromCell)
    {
    uint32 freeCam = AllocateExtraIndex(self);
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address, cellIndex;
    ThaModuleCla moduleCla = (ThaModuleCla)ThaHbceEntityClaModuleGet((ThaHbceEntity)self);

    if (freeCam == cInvalidUint32)
        return cInvalidUint32;

    ExtraIndexUseSet(self, freeCam);
    cellIndex = ThaHbceMemoryCellIndexGet(fromCell);
    address = Tha60290081HbceMemoryCellAddress(moduleCla, cellIndex);
    mModuleHwLongRead(moduleCla, address, longRegVal, cThaLongRegMaxSize, IpCore(self));

    address = cAf6Reg_upen_pwhashcam_Base + freeCam + Tha60210011ModuleClaBaseAddress(moduleCla);
    mModuleHwLongWrite(moduleCla, address, longRegVal, cThaLongRegMaxSize, IpCore(self));

    return freeCam;
    }

static void CamReset(ThaHbce self, uint32 camId)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    ThaModuleCla moduleCla = (ThaModuleCla)ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    uint32 address = cAf6Reg_upen_pwhashcam_Base + camId + Tha60210011ModuleClaBaseAddress(moduleCla);

    AtOsalMemInit(longRegVal, 0, sizeof(longRegVal));
    mModuleHwLongWrite(moduleCla, address, longRegVal, cThaLongRegMaxSize, IpCore(self));

    FreeExtraIndex(self, camId);
    }

static eAtRet EntriesWillMove(ThaCrcHash hash, ThaCrcHashEntry fromEntry, ThaCrcHashEntry toEntry, void* userData)
    {
    AtPwPsn psn;
    uint32 hashKey;
    ThaHbce hbce = (ThaHbce)userData;
    ThaHbceMemoryPool memPool = ThaHbceMemoryPoolGet(hbce);
    uint32 fromCellIndex = ThaCrcHashEntryFlatIdGet(fromEntry);
    uint32 toCellIndex   = ThaCrcHashEntryFlatIdGet(toEntry);
    ThaHbceMemoryCell fromCell = ThaHbceMemoryPoolCellAtIndex(memPool, fromCellIndex);
    ThaHbceMemoryCell toCell   = ThaHbceMemoryPoolCellAtIndex(memPool, toCellIndex);
    ThaHbceMemoryCellContent fromCellContent = ThaHbceMemoryCellContentGet(fromCell);
    ThaHbceMemoryCellContent toCellContent;
    ThaPwHeaderController headerController   = ThaHbceMemoryCellContentPwHeaderControllerGet(fromCellContent);
    uint32 camId;

    if ((camId = CopyMovingEntryToCam(hbce, fromCell)) == cInvalidUint32)
        return cAtErrorRsrcNoAvail;

    ThaHbceMemoryPoolUseCell(memPool, toCellIndex);
    toCellContent = ThaHbceMemoryCellContentGet(toCell);

    CellContentCopy(fromCellContent, toCellContent);
    ThaHbcePwRemove(hbce, headerController);

    /* Add this cell to this entry */
    ThaHbceEntryCellContentAdd(ThaHbceEntryAtIndex(hbce, toCellIndex), toCellContent);
    ThaPwHeaderControllerHbceMemoryCellContentSet(headerController, toCellContent);

    /* Let CRC has use this entry */
    psn = ThaPwHeaderControllerPsnGet(headerController);
    hashKey = HwHbceLabelGet(hbce, headerController, psn);
    ThaCrcHashUseEntry(hash, toCellIndex, &hashKey, NumKeyBit(hbce));

    /* Apply new cell to HW */
    Tha60210011HbceMemoryPoolApplyCellToHardware(memPool, toCellIndex);
    CamReset(hbce, camId);

    return cAtOk;
    }

static eAtRet RegisterListener(ThaCrcHash hash, ThaHbce self)
    {
    tThaCrcHashEntryMovingListener listener;
    listener.EntriesWillMove = EntriesWillMove;
    listener.EntriesDidMove = NULL;
    return ThaCrcHashEntryMovingListenerAdd(hash, &listener, self);
    }

static eAtRet CrcHashInit(ThaHbceEntity self)
    {
    static const uint32 cNumTable = 4;
    static const uint32 cNumEntriesInTable = 2048;

    if (mThis(self)->crcHash)
        AtObjectDelete((AtObject)mThis(self)->crcHash);

    mThis(self)->crcHash = ThaCrcHashNew(cNumTable, cNumEntriesInTable, (ThaHbce)self);
    if (mThis(self)->crcHash == NULL)
        return cAtError;

    CrcHashSetup(mThis(self)->crcHash);
    RegisterListener(mThis(self)->crcHash, (ThaHbce) self);

    return cAtOk;
    }

static eAtRet Init(ThaHbceEntity self)
    {
    eAtRet ret = m_ThaHbceEntityMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return CrcHashInit(self);
    }

static void Delete(AtObject self)
    {
    ThaHbceAllEntriesDelete((ThaHbce)self);
    AtObjectDelete((AtObject)(mThis(self)->crcHash));
    m_AtObjectMethods->Delete(self);
    }

static void Debug(ThaHbceEntity self)
    {
    m_ThaHbceEntityMethods->Debug(self);
    ThaCrcHashDebug(HashGet((ThaHbce)self));
    }

static uint32 MaxNumEntries(ThaHbce self)
    {
    AtUnused(self);
    return 8192;
    }

static eBool HasPages(ThaHbce self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet Apply2Hardware(ThaHbce self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eBool ShouldOptimizeInit(ThaHbceEntity self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60290081Hbce object = mThis(self);
    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObject(crcHash);
    }

static eAtRet StandbyPwAdd(ThaHbce self, ThaPwHeaderController controller)
    {
    AtPwPsn psn = ThaPwHeaderControllerPsnGet(controller);
    tThaHbceStandbyEntry *newHa;
    uint32 hwLabel;

    if (psn == NULL)
        return cAtOk;

    /* Create new data structure to store */
    hwLabel = HwHbceLabelGet(self, controller, psn);
    newHa = AtOsalMemAlloc(sizeof(tThaHbceStandbyEntry));
    AtOsalMemInit(newHa, 0, sizeof(tThaHbceStandbyEntry));
    newHa->hitCounts = 1;
    newHa->entryIndex = hwLabel;

    /* Cache it */
    AtListObjectAdd(self->standby.entries, (AtObject)newHa);
    return cAtOk;
    }

static eAtRet StandbyPwRemove(ThaHbce self, ThaPwHeaderController controller)
    {
    AtPwPsn psn = ThaPwHeaderControllerPsnGet(controller);
    uint32 hwLabel;
    AtIterator iterator;
    tThaHbceStandbyEntry *standByCache = NULL;

    if (psn == NULL)
        return cAtOk;

    hwLabel = HwHbceLabelGet(self, controller, psn);
    iterator = AtListIteratorGet(self->standby.entries);
    AtIteratorRestart(iterator);

    while((standByCache = (tThaHbceStandbyEntry *)AtIteratorNext(iterator)) != NULL)
        {
        if (standByCache->entryIndex == hwLabel)
            break;
        }

    if (standByCache != NULL)
        {
        AtListObjectRemove(self->standby.entries, (AtObject)standByCache);
        AtOsalMemFree(standByCache);
        }

    return cAtOk;
    }

static void StandbyCleanup(ThaHbce self)
    {
    while (AtListLengthGet(self->standby.entries) > 0)
        {
        tThaHbceStandbyEntry *ha = (tThaHbceStandbyEntry *)AtListObjectRemoveAtIndex(self->standby.entries, 0);
        AtOsalMemFree(ha);
        }

    AtObjectDelete((AtObject)self->standby.entries);
    self->standby.entries = NULL;
    }

static uint32 Restore(ThaHbceEntity self, uint8 page)
    {
    ThaHbce hbce = (ThaHbce)self;
    uint32 remained = 0;
    AtIterator iterator;
    tThaHbceStandbyEntry *haEntry;
    ThaHbceMemoryPool memPool = ThaHbceMemoryPoolGet((ThaHbce)self);
    AtUnused(page);

    iterator = AtListIteratorCreate(hbce->standby.entries);
    while ((haEntry = (tThaHbceStandbyEntry *)AtIteratorNext(iterator)) != NULL)
        remained = remained + Tha60290081HbceMemoryPoolCellRestore(memPool, haEntry->entryIndex);

    AtObjectDelete((AtObject)iterator);

    /* Done, need to cleanup standby database */
    if (!AtDriverKeepDatabaseAfterRestore())
        StandbyCleanup(hbce);

    return remained;
    }

static void MasterInit(ThaHbce self)
    {
    AtUnused(self);
    }

static void AllEntriesInit(ThaHbce self)
    {
    AtUnused(self);
    }

static uint32 NumExtraMemoryPointer(Tha60210011Hbce self)
    {
    AtUnused(self);
    return 16;
    }

static uint8 NumHashCollisionInOneHashIndex(Tha60210011Hbce self)
    {
    AtUnused(self);
    return 1;
    }

static uint8 NumHashCollisionInOneExtraPointer(Tha60210011Hbce self)
    {
    AtUnused(self);
    return 1;
    }

static void OverrideAtObject(ThaHbce self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideThaHbce(ThaHbce self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaHbceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaHbceOverride, m_ThaHbceMethods, sizeof(m_ThaHbceOverride));

        mMethodOverride(m_ThaHbceOverride, PsnHash);
        mMethodOverride(m_ThaHbceOverride, AllocateCellForPw);
        mMethodOverride(m_ThaHbceOverride, EntryObjectCreate);
        mMethodOverride(m_ThaHbceOverride, NewPwLabelExistInHashEntry);
        mMethodOverride(m_ThaHbceOverride, MemoryPoolCreate);
        mMethodOverride(m_ThaHbceOverride, MaxNumEntries);
        mMethodOverride(m_ThaHbceOverride, HasPages);
        mMethodOverride(m_ThaHbceOverride, Apply2Hardware);
        mMethodOverride(m_ThaHbceOverride, StandbyPwAdd);
        mMethodOverride(m_ThaHbceOverride, StandbyPwRemove);
        mMethodOverride(m_ThaHbceOverride, StandbyCleanup);
        mMethodOverride(m_ThaHbceOverride, MasterInit);
        mMethodOverride(m_ThaHbceOverride, AllEntriesInit);
        }

    mMethodsSet(self, &m_ThaHbceOverride);
    }

static void OverrideThaHbceEntity(ThaHbce self)
    {
    ThaHbceEntity entity = (ThaHbceEntity)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaHbceEntityMethods = mMethodsGet(entity);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaHbceEntityOverride, m_ThaHbceEntityMethods, sizeof(m_ThaHbceEntityOverride));

        mMethodOverride(m_ThaHbceEntityOverride, Init);
        mMethodOverride(m_ThaHbceEntityOverride, Debug);
        mMethodOverride(m_ThaHbceEntityOverride, ShouldOptimizeInit);
        mMethodOverride(m_ThaHbceEntityOverride, Restore);
        }

    mMethodsSet(entity, &m_ThaHbceEntityOverride);
    }

static void OverrideTha60210011Hbce(ThaHbce self)
    {
    Tha60210011Hbce hbce = (Tha60210011Hbce)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011HbceOverride, mMethodsGet(hbce),
                                  sizeof(m_Tha60210011HbceOverride));

        mMethodOverride(m_Tha60210011HbceOverride, HashEntryTabCtrlApply);
        mMethodOverride(m_Tha60210011HbceOverride, NumExtraMemoryPointer);
        mMethodOverride(m_Tha60210011HbceOverride, NumHashCollisionInOneHashIndex);
        mMethodOverride(m_Tha60210011HbceOverride, NumHashCollisionInOneExtraPointer);
        }

    mMethodsSet(hbce, &m_Tha60210011HbceOverride);
    }

static void Override(ThaHbce self)
    {
    OverrideAtObject(self);
    OverrideThaHbceEntity(self);
    OverrideThaHbce(self);
    OverrideTha60210011Hbce(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081Hbce);
    }

static ThaHbce ObjectInit(ThaHbce self, uint8 hbceId, AtModule claModule, AtIpCore core)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011HbceObjectInit(self, hbceId, claModule, core) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaHbce Tha60290081HbceNew(uint8 hbceId, AtModule claModule, AtIpCore core)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaHbce newObject = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newObject, hbceId, claModule, core);
    }

ThaCrcHash Tha60290081HbceCrcHash(ThaHbce self)
    {
    if (self)
        return mThis(self)->crcHash;
    return NULL;
    }

uint32 Tha60290081HbceNumKeyBit(ThaHbce self)
    {
    if (self)
        return NumKeyBit(self);
    return 0;
    }

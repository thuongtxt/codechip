/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : Tha60290081MsHbce.h
 * 
 * Created Date: Jul 5, 2019
 *
 * Description : HBCE
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290081HBCE_H_
#define _THA60290081HBCE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/pw/headercontrollers/ThaPwHeaderController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaCrcHash Tha60290081HbceCrcHash(ThaHbce self);
uint32 Tha60290081HbceNumKeyBit(ThaHbce self);
uint32 Tha60290081HbceMemoryPoolCellRestore(ThaHbceMemoryPool self, uint32 hwLabel);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290081HBCE_H_ */


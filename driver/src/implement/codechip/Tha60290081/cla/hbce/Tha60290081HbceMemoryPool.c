/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60290081MsHbceMemoryPool.c
 *
 * Created Date: Jul 5, 2019
 *
 * Description : HBCE Memory pool
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/man/ThaDeviceInternal.h"
#include "../../../../default/pw/headercontrollers/ThaPwHeaderController.h"
#include "../../../Tha60210011/cla/hbce/Tha60210011HbceInternal.h"
#include "../../../Tha60210011/cla/Tha60210011ModuleCla.h"
#include "../Tha60290081ModuleCla.h"
#include "../Tha60290081ModuleClaReg.h"
#include "../controller/Tha60290081ClaPwController.h"
#include "Tha60290081Hbce.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60290081HbceMemoryPool)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290081HbceMemoryPool * Tha60290081HbceMemoryPool;
typedef struct tTha60290081HbceMemoryPool
    {
    tTha60210011HbceMemoryPool super;
    }tTha60290081HbceMemoryPool;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaHbceMemoryPoolMethods m_ThaHbceMemoryPoolOverride;
static tTha60210011HbceMemoryPoolMethods m_Tha60210011HbceMemoryPoolOverride;

/* Super implementation */
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxNumCells(ThaHbceMemoryPool self)
    {
    ThaHbce hbce = ThaHbceMemoryPoolHbceGet(self);
    uint32 numHashEntries = ThaHbceMaxNumEntries(hbce);
    uint32 numExtraPointers = Tha60210011HbceNumExtraMemoryPointer(hbce);
    return numHashEntries + numExtraPointers;
    }

static ThaHbceMemoryCell UseCell(ThaHbceMemoryPool self, uint32 cellIndex)
    {
    ThaHbceMemoryCell cell = ThaHbceMemoryPoolCellAtIndex(self, cellIndex);
    if (cell == NULL)
        return NULL;

    /* Clean it */
    ThaHbceEntityInit((ThaHbceEntity)cell);
    self->numUsedCells = self->numUsedCells + 1;

    return cell;
    }

static AtDevice Device(ThaHbceMemoryPool self)
    {
    AtModule claModule = ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    return AtModuleDeviceGet(claModule);
    }

static eAtRet UnUseCell(ThaHbceMemoryPool self, ThaHbceMemoryCell cell)
    {
    ThaHbce hbce = ThaHbceMemoryPoolHbceGet(self);
    uint32 cellIndex;

    if (cell == NULL)
        return cAtErrorNullPointer;

    /* Clean this cell */
    cellIndex = ThaHbceMemoryCellIndexGet(cell);
    ThaHbceEntityInit((ThaHbceEntity)cell);
    ThaHbceMemoryCellContentRemainedBitsInvalidate(ThaHbceMemoryCellContentGet(cell));

    if (cellIndex < ThaHbceMaxNumEntries(hbce))
        ThaCrcHashUnuseEntry(Tha60290081HbceCrcHash(hbce), cellIndex);
    else
        Tha60210011HbceMemoryPoolFreeExtraPointerListPush(self, cellIndex - ThaHbceMaxNumEntries(hbce));

    self->numUsedCells = self->numUsedCells - 1;
    Tha60210011HbceMemoryCellUse(cell, cAtFalse);

    if (!AtDeviceIsDeleting(Device(self)))
        Tha60210011HbceMemoryPoolApplyCellToHardware(self, cellIndex);

    return cAtOk;
    }

static AtIpCore IpCore(ThaHbceMemoryPool self)
    {
    return ThaHbceCoreGet(self->hbce);
    }

static eAtRet CellEnable(ThaHbceMemoryPool self, ThaHbceMemoryCell cell, eBool enable)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    ThaModuleCla moduleCla = (ThaModuleCla)ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    ThaHbceMemoryCellContent cellContent;
    uint32 address = Tha60290081HbceMemoryCellAddress(moduleCla, ThaHbceMemoryCellIndexGet(cell));

    mModuleHwLongRead(moduleCla, address, longRegVal, cThaLongRegMaxSize, IpCore(self));
    mRegFieldSet(longRegVal[1], cAf6_upen_pwhash_Outen_, mBoolToBin(enable));
    mModuleHwLongWrite(moduleCla, address, longRegVal, cThaLongRegMaxSize, IpCore(self));

    /* Update software database */
    cellContent = ThaHbceMemoryCellContentGet(cell);
    ThaHbceMemoryCellContentEnable(cellContent, enable);

    return cAtOk;
    }

static eBool CellIsEnabled(ThaHbceMemoryPool self, ThaHbceMemoryCell cell)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    ThaModuleCla moduleCla = (ThaModuleCla)ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    uint32 address = Tha60290081HbceMemoryCellAddress(moduleCla, ThaHbceMemoryCellIndexGet(cell));

    mModuleHwLongRead(moduleCla, address, longRegVal, cThaLongRegMaxSize, IpCore(self));
    return (mRegField(longRegVal[1], cAf6_upen_pwhash_Outen_)) ? cAtTrue : cAtFalse;
    }

static void ApplyCell(Tha60210011HbceMemoryPool self, uint32 cellIndex)
    {
    static const uint8 cHashTraffic = 1;
    ThaHbceMemoryCellContent cellContent;
    ThaHbceMemoryPool memPool = (ThaHbceMemoryPool)self;
    ThaModuleCla moduleCla = (ThaModuleCla)ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    ThaPwHeaderController headerController;
    ThaPwAdapter pwAdapter;
    uint32 address, flowId;
    uint32 longRegVal[cThaLongRegMaxSize];
    uint8 hashValid, flowType;
    uint32 hashKey;
    uint32 numBits;
    AtEthFlow flow;

    cellContent = ThaHbceMemoryCellContentGet(ThaHbceMemoryPoolCellAtIndex(memPool, cellIndex));
    if (cellContent == NULL)
        return;

    address = Tha60290081HbceMemoryCellAddress(moduleCla, cellIndex);
    headerController = ThaHbceMemoryCellContentPwHeaderControllerGet(cellContent);
    pwAdapter = ThaPwHeaderControllerAdapterGet(headerController);
    flow = ThaPwAdapterEthFlowGet(pwAdapter);

    if (pwAdapter == NULL)
        {
        flowId = 0;
        hashValid = 0;
        hashKey = 0;
        flowType = 0;
        }
    else
        {
        flowId = AtChannelHwIdGet((AtChannel)flow);
        hashValid = 1;
        hashKey = ThaHbceMemoryCellContentRemainedBitsGet(cellContent, NULL)[0];
        flowType = Tha60290081ModuleClaEthFlowHwFlowType(moduleCla, flow);
        }

    mModuleHwLongRead(moduleCla, address, longRegVal, cThaLongRegMaxSize, IpCore(memPool));
    mRegFieldSet(longRegVal[0], cAf6_upen_pwhash_Hash_Valid_, hashValid);
    mRegFieldSet(longRegVal[0], cAf6_upen_pwhash_Hash_Key_, hashKey);
    mRegFieldSet(longRegVal[0], cAf6_upen_pwhash_FlowID_01_, flowId);
    numBits = AtUtilNumBitMaskGet(cAf6_upen_pwhash_FlowID_01_Mask);
    mRegFieldSet(longRegVal[1], cAf6_upen_pwhash_FlowID_02_, flowId >> numBits);
    mRegFieldSet(longRegVal[1], cAf6_upen_pwhash_Sertyp_, flowType);
    mRegFieldSet(longRegVal[1], cAf6_upen_pwhash_Serstyp_, cHashTraffic);
    mModuleHwLongWrite(moduleCla, address, longRegVal, cThaLongRegMaxSize, IpCore(memPool));

    }

static uint32 PwFlowControlReg(ThaModuleCla moduleCla, uint32 hwFlowId)
    {
    return Tha60290081ModuleClaFlowTableControlReg(moduleCla) + hwFlowId + Tha60210011ModuleClaBaseAddress(moduleCla);
    }

static ThaPwAdapter PwAdapterGet(ThaModuleCla moduleCla, uint32 serviceHwType, uint32 channelId)
    {
    return ThaPwAdapterGet(Tha602900681ModuleClaFlowControlPwGet(moduleCla, serviceHwType, channelId));
    }

static uint32 PwRestore(ThaHbceMemoryPool self, uint32 serviceHwType, uint32 hwLabel, uint32 cellIndex, uint32 hwFlowId, eBool isEnabled)
    {
    uint32 regAddress;
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 channelId;
    ThaPwAdapter pwAdapter;
    ThaHbceMemoryCell cell;
    ThaHbceMemoryCellContent cellContent;
    ThaPwHeaderController headerController;
    uint32 remaingingBits[2];
    eAtRet ret;
    ThaHbceEntry hbceEntry;
    ThaModuleCla moduleCla = (ThaModuleCla)ThaHbceEntityClaModuleGet((ThaHbceEntity)self);

    regAddress = PwFlowControlReg(moduleCla, hwFlowId);
    mModuleHwLongRead(moduleCla, regAddress, longRegVal, cThaLongRegMaxSize, IpCore(self));
    channelId = mRegField(longRegVal[0], cAf6_upen_flowtab_serid_);

    pwAdapter = PwAdapterGet(moduleCla, serviceHwType, channelId);
    if (pwAdapter == NULL)
        {
        AtModuleLog((AtModule)moduleCla, cAtLogLevelCritical, AtSourceLocation,
                    "Cannot find pw adapter for cell %d with HW flow ID %d\r\n",
                    cellIndex + 1, hwFlowId);
        return 0;
        }

    /* Enabling should match */
    if (isEnabled != ThaPwAdapterIsEnabled(pwAdapter))
        return 0;

    ThaHbceMemoryPoolUseCell(self, cellIndex);
    cell = ThaHbceMemoryPoolCellAtIndex(self, cellIndex);
    cellContent = ThaHbceMemoryCellContentGet(cell);
    if (cellContent == NULL)
        return 0;

    headerController = ThaPwAdapterHeaderController(pwAdapter);
    ThaHbceMemoryCellContentPwHeaderControllerSet(cellContent, headerController);
    ThaPwHeaderControllerHbceMemoryCellContentSet(headerController, cellContent);

    ThaHbceMemoryCellContentEnable(cellContent, isEnabled);
    AtOsalMemInit(remaingingBits, 0, sizeof(remaingingBits));
    remaingingBits[0] = hwLabel;
    ThaHbceMemoryCellContentRemainedBitsSet(cellContent, remaingingBits, mCount(remaingingBits));

    hbceEntry = ThaHbceEntryAtIndex(self->hbce, cellIndex);
    ret = ThaHbceEntryCellContentAdd(hbceEntry, cellContent);
    if (ret != cAtOk)
        {
        AtModuleLog((AtModule)moduleCla, cAtLogLevelCritical, AtSourceLocation,
                    "Cannot add cell %d to entry %d, ret = %s\r\n",
                    cellIndex + 1, cellIndex + 1, AtRet2String(ret));
        }

    return 0;
    }

static void CrcEntryUse(ThaHbceMemoryPool self, uint32 hwLabel, uint32 cellIndex)
    {
    ThaHbce hbce = ThaHbceMemoryPoolHbceGet(self);
    ThaCrcHash crcHash = Tha60290081HbceCrcHash(hbce);
    ThaCrcHashUseEntry(crcHash, cellIndex, &hwLabel, Tha60290081HbceNumKeyBit(hbce));
    }

static uint32 PwInCamRestore(ThaHbceMemoryPool self, uint32 hwLabel)
    {
    uint32 address, cam_i;
    ThaHbce hbce = ThaHbceMemoryPoolHbceGet(self);
    ThaModuleCla moduleCla = (ThaModuleCla)ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 flowIdTail, flowIdHead, flowId;
    uint32 serviceHwType;
    uint32 startExtraIndex = ThaHbceMaxNumEntries(hbce);
    eBool isEnabled;
    uint32 ret = 0;

    for (cam_i = 0; cam_i < Tha60210011HbceNumExtraMemoryPointer(hbce); cam_i++)
        {
        address = cAf6Reg_upen_pwhashcam_Base + Tha60210011ModuleClaBaseAddress(moduleCla) + cam_i;
        mModuleHwLongRead(moduleCla, address, longRegVal, cThaLongRegMaxSize, IpCore(self));

        if ((mRegField(longRegVal[0], cAf6_upen_pwhashcam_Hash_Valid_) == cAtFalse) ||
            (mRegField(longRegVal[0], cAf6_upen_pwhashcam_Hash_Key_) != hwLabel))
            continue;

        /* Found cell with same hw label */
        flowIdTail = mRegField(longRegVal[0], cAf6_upen_pwhashcam_FlowID_01_);
        flowIdHead = mRegField(longRegVal[1], cAf6_upen_pwhashcam_FlowID_02_);
        flowId = (flowIdHead << AtUtilNumBitMaskGet(cAf6_upen_pwhashcam_FlowID_01_Mask)) | flowIdTail;
        serviceHwType = mRegField(longRegVal[1], cAf6_upen_pwhashcam_Sertyp_);
        isEnabled = (mRegField(longRegVal[1], cAf6_upen_pwhashcam_Outen_) > 0) ? cAtTrue : cAtFalse;

        ret += PwRestore(self, serviceHwType, hwLabel, startExtraIndex + cam_i, flowId, isEnabled);
        break;
        }

    return ret;
    }

static eAtRet CellInit(ThaHbceMemoryPool self, uint32 cellIndex)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    ThaModuleCla moduleCla = (ThaModuleCla)ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    uint32 regAddr;
    uint32 longRegVal[cLongRegSizeInDwords];

    if (moduleCla == NULL)
        return cAtErrorObjectNotExist;

    mMethodsGet(osal)->MemInit(osal, longRegVal, 0, sizeof(longRegVal));
    regAddr = Tha60290081HbceMemoryCellAddress(moduleCla, cellIndex);
    mModuleHwLongWrite(moduleCla, regAddr, longRegVal, cLongRegSizeInDwords, IpCore(self));

    return cAtOk;
    }

static void OverrideThaHbceMemoryPool(ThaHbceMemoryPool self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaHbceMemoryPoolOverride, mMethodsGet(self), sizeof(m_ThaHbceMemoryPoolOverride));

        mMethodOverride(m_ThaHbceMemoryPoolOverride, MaxNumCells);
        mMethodOverride(m_ThaHbceMemoryPoolOverride, UseCell);
        mMethodOverride(m_ThaHbceMemoryPoolOverride, UnUseCell);
        mMethodOverride(m_ThaHbceMemoryPoolOverride, CellEnable);
        mMethodOverride(m_ThaHbceMemoryPoolOverride, CellIsEnabled);
        mMethodOverride(m_ThaHbceMemoryPoolOverride, CellInit);
        }

    mMethodsSet(self, &m_ThaHbceMemoryPoolOverride);
    }

static void OverrideTha60210011HbceMemoryPool(ThaHbceMemoryPool self)
    {
    Tha60210011HbceMemoryPool memPool = (Tha60210011HbceMemoryPool)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011HbceMemoryPoolOverride, mMethodsGet(memPool),
                                  sizeof(m_Tha60210011HbceMemoryPoolOverride));

        mMethodOverride(m_Tha60210011HbceMemoryPoolOverride, ApplyCell);
        }

    mMethodsSet(memPool, &m_Tha60210011HbceMemoryPoolOverride);
    }

static void Override(ThaHbceMemoryPool self)
    {
    OverrideThaHbceMemoryPool(self);
    OverrideTha60210011HbceMemoryPool(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081HbceMemoryPool);
    }

static ThaHbceMemoryPool ObjectInit(ThaHbceMemoryPool self, ThaHbce hbce)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011HbceMemoryPoolObjectInit(self, hbce) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaHbceMemoryPool Tha60290081HbceMemoryPoolNew(ThaHbce hbce)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaHbceMemoryPool newObject = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newObject, hbce);
    }

uint32 Tha60290081HbceMemoryPoolCellRestore(ThaHbceMemoryPool self, uint32 hwLabel)
    {
    ThaHbce hbce = ThaHbceMemoryPoolHbceGet(self);
    ThaCrcHash crcHash = Tha60290081HbceCrcHash(hbce);
    AtIterator iterator = ThaCrcHashConflictedEntriesIteratorCreate(crcHash, &hwLabel, Tha60290081HbceNumKeyBit(hbce));
    ThaCrcHashEntry entry;
    uint32 ret = 0;
    eBool foundInHash = cAtFalse;

    /* Scan all entries to find entry with hw label */
    while ((entry = (ThaCrcHashEntry)AtIteratorNext(iterator)) != NULL)
        {
        uint32 cellIndex = ThaCrcHashEntryFlatIdGet(entry);
        ThaModuleCla moduleCla = (ThaModuleCla)ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
        uint32 address = Tha60290081HbceMemoryCellAddress(moduleCla, cellIndex);
        uint32 longRegVal[cThaLongRegMaxSize];
        uint32 flowIdTail, flowIdHead, flowId;
        uint32 serviceHwType;
        eBool isEnabled;

        mModuleHwLongRead(moduleCla, address, longRegVal, cThaLongRegMaxSize, IpCore(self));

        if ((mRegField(longRegVal[0], cAf6_upen_pwhash_Hash_Valid_) == cAtFalse) ||
            (mRegField(longRegVal[0], cAf6_upen_pwhash_Hash_Key_) != hwLabel))
            continue;

        /* Found cell with same hw label */
        flowIdTail = mRegField(longRegVal[0], cAf6_upen_pwhash_FlowID_01_);
        flowIdHead = mRegField(longRegVal[1], cAf6_upen_pwhash_FlowID_02_);
        flowId = (flowIdHead << AtUtilNumBitMaskGet(cAf6_upen_pwhash_FlowID_01_Mask)) | flowIdTail;
        serviceHwType = mRegField(longRegVal[1], cAf6_upen_pwhash_Sertyp_);
        isEnabled = (mRegField(longRegVal[1], cAf6_upen_pwhash_Outen_) > 0) ? cAtTrue : cAtFalse;

        ret += PwRestore(self, serviceHwType, hwLabel, cellIndex, flowId, isEnabled);
        CrcEntryUse(self, hwLabel, cellIndex);
        foundInHash = cAtTrue;
        break;
        }

    AtObjectDelete((AtObject)iterator);

    if (!foundInHash)
        ret += PwInCamRestore(self, hwLabel);

    return ret;
    }

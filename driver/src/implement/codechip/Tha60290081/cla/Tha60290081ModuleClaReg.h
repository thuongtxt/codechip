/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : Tha60290081ModuleClaReg.h
 * 
 * Created Date: Jul 5, 2019
 *
 * Description : Module CLA
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290081MODULECLAREG_H_
#define _THA60290081MODULECLAREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name   : Hold Register 1
Reg Addr   : 0x00_0000
Reg Formula:
    Where  :
Reg Desc   :
This register hold value 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_hold1_Base                                                                       0x000000

/*--------------------------------------
BitField Name: cfg_hold1
BitField Type: RW
BitField Desc: hold value 1
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_hold1_cfg_hold1_Mask                                                                cBit31_0
#define cAf6_upen_hold1_cfg_hold1_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Hold Register 2
Reg Addr   : 0x00_0001
Reg Formula:
    Where  :
Reg Desc   :
This register hold value 2

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_hold2_Base                                                                       0x000001

/*--------------------------------------
BitField Name: cfg_hold2
BitField Type: RW
BitField Desc: hold value 2
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_hold2_cfg_hold2_Mask                                                                cBit31_0
#define cAf6_upen_hold2_cfg_hold2_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Hold Register 3
Reg Addr   : 0x00_0002
Reg Formula:
    Where  :
Reg Desc   :
This register hold value

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_hold3_Base                                                                       0x000002

/*--------------------------------------
BitField Name: cfg_hold3
BitField Type: RW
BitField Desc: hold value 3
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_hold3_cfg_hold3_Mask                                                                cBit31_0
#define cAf6_upen_hold3_cfg_hold3_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : config ptp service type
Reg Addr   : 0x00_000F
Reg Formula:
    Where  :
Reg Desc   :
is used to indicate PTP service type

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ptp_sertyp_Base                                                                  0x00000F

/*--------------------------------------
BitField Name: cfg_ptp_sertyp
BitField Type: RW
BitField Desc: ptp service type
BitField Bits: [02:00]
--------------------------------------*/
#define cAf6_upen_ptp_sertyp_cfg_ptp_sertyp_Mask                                                       cBit2_0
#define cAf6_upen_ptp_sertyp_cfg_ptp_sertyp_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Config cpu flush
Reg Addr   : 0x00_0010
Reg Formula:
    Where  :
Reg Desc   :
Used to config cpu flush

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cpu_flush_Base                                                                   0x000010

/*--------------------------------------
BitField Name: cfg_cpu_flush
BitField Type: RW
BitField Desc: cpu flush
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_cpu_flush_cfg_cpu_flush_Mask                                                           cBit0
#define cAf6_upen_cpu_flush_cfg_cpu_flush_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : CLA Ready
Reg Addr   : 0x00_0011
Reg Formula:
    Where  :
Reg Desc   :
This register is used to indicate config ptp done

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cla_rdy_Base                                                                     0x000011

/*--------------------------------------
BitField Name: cfg_eth_fcsen
BitField Type: RW
BitField Desc: '1' : insert FCS in EOS path
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_cla_rdy_cfg_eth_fcsen_Mask                                                             cBit1
#define cAf6_upen_cla_rdy_cfg_eth_fcsen_Shift                                                                1

/*--------------------------------------
BitField Name: cfg_cla_rdy
BitField Type: RW
BitField Desc: '1' : ready
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_cla_rdy_cfg_cla_rdy_Mask                                                               cBit0
#define cAf6_upen_cla_rdy_cfg_cla_rdy_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Force Value for Service Lookup
Reg Addr   : 0x00_0012
Reg Formula:
    Where  :
Reg Desc   :
This register is used to config Force Value for test Lookup

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_force_val_Base                                                                   0x000012

/*--------------------------------------
BitField Name: Outen
BitField Type: RW
BitField Desc: Output enable 0: disable 1: enable
BitField Bits: [22:22]
--------------------------------------*/
#define cAf6_upen_force_val_Outen_Mask                                                                  cBit22
#define cAf6_upen_force_val_Outen_Shift                                                                     22

/*--------------------------------------
BitField Name: Ethtyp
BitField Type: RW
BitField Desc: Ethernet Type Indicator 0 : ipv4 1 : ipv6 2 : mpls unicast 3 :
mpls multicast 4 : OAM 5 : MEF 6-15: reserved
BitField Bits: [21:18]
--------------------------------------*/
#define cAf6_upen_force_val_Ethtyp_Mask                                                              cBit21_18
#define cAf6_upen_force_val_Ethtyp_Shift                                                                    18

/*--------------------------------------
BitField Name: Sertyp
BitField Type: RW
BitField Desc: service type 0: DROP 1: CEM 2: iMSG 3: DCC 4: Eth PASS 5: PTP 6:
EoS 7: EoP
BitField Bits: [17:15]
--------------------------------------*/
#define cAf6_upen_force_val_Sertyp_Mask                                                              cBit17_15
#define cAf6_upen_force_val_Sertyp_Shift                                                                    15

/*--------------------------------------
BitField Name: Serstyp
BitField Type: RW
BitField Desc: service subtype 0: PW 1: DATA 2: OAM 3: CPU
BitField Bits: [14:13]
--------------------------------------*/
#define cAf6_upen_force_val_Serstyp_Mask                                                             cBit14_13
#define cAf6_upen_force_val_Serstyp_Shift                                                                   13

/*--------------------------------------
BitField Name: FlowID
BitField Type: RW
BitField Desc: Flow ID
BitField Bits: [12:00]
--------------------------------------*/
#define cAf6_upen_force_val_FlowID_Mask                                                               cBit12_0
#define cAf6_upen_force_val_FlowID_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : MEF EtherType
Reg Addr   : 0x00_0013
Reg Formula:
    Where  :
Reg Desc   :
This register is used to define MEF ethernet type

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mef_etyp_Base                                                                    0x000013

/*--------------------------------------
BitField Name: cfg_mef_etyp
BitField Type: RW
BitField Desc: user define MEF ethernet type
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_mef_etyp_cfg_mef_etyp_Mask                                                          cBit15_0
#define cAf6_upen_mef_etyp_cfg_mef_etyp_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : UDP Port Selection
Reg Addr   : 0x00_0014
Reg Formula:
    Where  :
Reg Desc   :
This register is applicable for IPv4/IPv6 using udp src/des port to identify PW packet from ETH Side.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_udpprt_sel_Base                                                                  0x000014

/*--------------------------------------
BitField Name: udpprt_sel
BitField Type: RW
BitField Desc: 0: select udp destination port to identify PW packet 1: select
udp source port to identify PW packet
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_udpprt_sel_udpprt_sel_Mask                                                             cBit0
#define cAf6_upen_udpprt_sel_udpprt_sel_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : UDP Port Mode
Reg Addr   : 0x00_0015
Reg Formula:
    Where  :
Reg Desc   :
This register is used to config udp port mode

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_udpprt_mode_Base                                                                 0x000015

/*--------------------------------------
BitField Name: udpprt_mode
BitField Type: RW
BitField Desc: 0: Classify engine will automatically search for value 0x85E in
source or destination UDP port. The remaining UDP port is used to identify
pseudowire packet 1: Classify engine uses udpprt_sel to decide which UDP port
(Source or Destination) is used to identify pseudowire packet
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_udpprt_mode_udpprt_mode_Mask                                                           cBit0
#define cAf6_upen_udpprt_mode_udpprt_mode_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : PTP layer force value
Reg Addr   : 0x00_0016
Reg Formula:
    Where  :
Reg Desc   :
This register is used to config ptp layer force value

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_force_ptplayer_Base                                                              0x000016

/*--------------------------------------
BitField Name: ptp_layer
BitField Type: RW
BitField Desc: 3'b000: PTP L2 3'b001: IPv4 3'b010: IPv6 3'b011: IPv4 MPLS
unicast 3'b100: IPv6 MPLS unicast
BitField Bits: [03:01]
--------------------------------------*/
#define cAf6_upen_force_ptplayer_ptp_layer_Mask                                                        cBit3_1
#define cAf6_upen_force_ptplayer_ptp_layer_Shift                                                             1

/*--------------------------------------
BitField Name: ptp_ind
BitField Type: RW
BitField Desc: ptp indicate: 0: disable 1: enable
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_force_ptplayer_ptp_ind_Mask                                                            cBit0
#define cAf6_upen_force_ptplayer_ptp_ind_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : MRU Packet Size
Reg Addr   : 0x00_0017
Reg Formula:
    Where  :
Reg Desc   :
This register is used to config MRU Value

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mru_val_Base                                                                     0x000017

/*--------------------------------------
BitField Name: cfg_mru_en
BitField Type: RW
BitField Desc: MRU enable 1: enable 0: disable
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_upen_mru_val_cfg_mru_en_Mask                                                               cBit14
#define cAf6_upen_mru_val_cfg_mru_en_Shift                                                                  14

/*--------------------------------------
BitField Name: cfg_mru_val
BitField Type: RW
BitField Desc: MRU value(Byte)
BitField Bits: [13:00]
--------------------------------------*/
#define cAf6_upen_mru_val_cfg_mru_val_Mask                                                            cBit13_0
#define cAf6_upen_mru_val_cfg_mru_val_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Classify Per Flow Table Control
Reg Addr   : 0x00_A000-0xBFFF
Reg Formula: 0x00_A000 + $flowid
    Where  :
           + $flowid(0-8191)
Reg Desc   :
This register configures table action for flow traffic
HDL_PATH     : inscorepi.label0.insflowtb.membuf.ram.ram[$flowid]

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_flowtab_Base                                                                     0x00A000

/*--------------------------------------
BitField Name: ovwpid_en
BitField Type: RW
BitField Desc: Overwrite protocol ID enable 0: disable 1: enable
BitField Bits: [50:50]
--------------------------------------*/
#define cAf6_upen_flowtab_ovwpid_en_Mask                                                                cBit18
#define cAf6_upen_flowtab_ovwpid_en_Shift                                                                   18

/*--------------------------------------
BitField Name: compid_en
BitField Type: RW
BitField Desc: Compress protocol ID enable 0: disable 1: enable
BitField Bits: [49:49]
--------------------------------------*/
#define cAf6_upen_flowtab_compid_en_Mask                                                                cBit17
#define cAf6_upen_flowtab_compid_en_Shift                                                                   17

/*--------------------------------------
BitField Name: tdmenc_len
BitField Type: RW
BitField Desc: length of TDM header encap 0: 1 byte 1: 2 byte 2: 3 byte 3: 4
byte 4: 5 byte 5: 6 byte 6: 7 byte 7: 8 byte
BitField Bits: [48:46]
--------------------------------------*/
#define cAf6_upen_flowtab_tdmenc_len_Mask                                                            cBit16_14
#define cAf6_upen_flowtab_tdmenc_len_Shift                                                                  14

/*--------------------------------------
BitField Name: tdmenc_en
BitField Type: RW
BitField Desc: tdm encap enable 0: disable 1: enable
BitField Bits: [45:45]
--------------------------------------*/
#define cAf6_upen_flowtab_tdmenc_en_Mask                                                                cBit13
#define cAf6_upen_flowtab_tdmenc_en_Shift                                                                   13

/*--------------------------------------
BitField Name: subenc_len
BitField Type: RW
BitField Desc: length of protocol header encap 0: 1 byte 1: 2 byte 2: 3 byte 3:
4 byte 4: 5 byte 5: 6 byte 6: 7 byte 7: 8 byte
BitField Bits: [44:42]
--------------------------------------*/
#define cAf6_upen_flowtab_subenc_len_Mask                                                            cBit12_10
#define cAf6_upen_flowtab_subenc_len_Shift                                                                  10

/*--------------------------------------
BitField Name: subenc_en
BitField Type: RW
BitField Desc: protocol encap enable 0: disable 1: enable
BitField Bits: [41:41]
--------------------------------------*/
#define cAf6_upen_flowtab_subenc_en_Mask                                                                 cBit9
#define cAf6_upen_flowtab_subenc_en_Shift                                                                    9

/*--------------------------------------
BitField Name: q922mode
BitField Type: RW
BitField Desc: q922 mode 0: 2 bytes 1: 3 bytes 2: 4 bytes
BitField Bits: [40:39]
--------------------------------------*/
#define cAf6_upen_flowtab_q922mode_Mask                                                                cBit8_7
#define cAf6_upen_flowtab_q922mode_Shift                                                                     7

/*--------------------------------------
BitField Name: mlpppmode
BitField Type: RW
BitField Desc: mlppp mode 0: long   mode 1: short  mode
BitField Bits: [38:38]
--------------------------------------*/
#define cAf6_upen_flowtab_mlpppmode_Mask                                                                 cBit6
#define cAf6_upen_flowtab_mlpppmode_Shift                                                                    6

/*--------------------------------------
BitField Name: mlfrmode
BitField Type: RW
BitField Desc: mlfr mode 0: end2end mode 1: uni/nni mode
BitField Bits: [37:37]
--------------------------------------*/
#define cAf6_upen_flowtab_mlfrmode_Mask                                                                  cBit5
#define cAf6_upen_flowtab_mlfrmode_Shift                                                                     5

/*--------------------------------------
BitField Name: frmode
BitField Type: RW
BitField Desc: frompls mode 0: one2one mode 1:  port   mode
BitField Bits: [36:36]
--------------------------------------*/
#define cAf6_upen_flowtab_frmode_Mask                                                                    cBit4
#define cAf6_upen_flowtab_frmode_Shift                                                                       4

/*--------------------------------------
BitField Name: rtp_en
BitField Type: RW
BitField Desc: enable support RTP header 0: disable 1: enable
BitField Bits: [35:35]
--------------------------------------*/
#define cAf6_upen_flowtab_rtp_en_Mask                                                                    cBit3
#define cAf6_upen_flowtab_rtp_en_Shift                                                                       3

/*--------------------------------------
BitField Name: rtp_act
BitField Type: RW
BitField Desc: remove RTP header 0: keep 1: remove
BitField Bits: [34:34]
--------------------------------------*/
#define cAf6_upen_flowtab_rtp_act_Mask                                                                   cBit2
#define cAf6_upen_flowtab_rtp_act_Shift                                                                      2

/*--------------------------------------
BitField Name: cw_act
BitField Type: RW
BitField Desc: remove Control Word 0: keep 1: remove
BitField Bits: [33:33]
--------------------------------------*/
#define cAf6_upen_flowtab_cw_act_Mask                                                                    cBit1
#define cAf6_upen_flowtab_cw_act_Shift                                                                       1

/*--------------------------------------
BitField Name: cw_en
BitField Type: RW
BitField Desc: enable support CW 0: disable 1: enable
BitField Bits: [32:32]
--------------------------------------*/
#define cAf6_upen_flowtab_cw_en_Mask                                                                  cBit0
#define cAf6_upen_flowtab_cw_en_Shift                                                                     0

/*--------------------------------------
BitField Name: psnhdr_act
BitField Type: RW
BitField Desc: PSN Header remove action 0: no remove 1: remove first vlan 2:
remove DA::SA::VLAN1::VLAN2::ETHTYPE 3: remove
DA::SA::VLAN1::VLAN2::ETHTYPE::MEF HEADER 4: remove
DA::SA::VLAN1::VLAN2::ETHTYPE::MPLS HEADER 5: remove
DA::SA::VLAN1::VLAN2::ETHTYPE::IPV4 HEADER::UDP HEADER 6: remove
DA::SA::VLAN1::VLAN2::ETHTYPE::IPV6 HEADER::UDP HEADER 7-63: unused
BitField Bits: [31:26]
--------------------------------------*/
#define cAf6_upen_flowtab_psnhdr_act_Mask                                                            cBit31_26
#define cAf6_upen_flowtab_psnhdr_act_Shift                                                                  26

/*--------------------------------------
BitField Name: cepmode
BitField Type: RW
BitField Desc: CEP mode 0: Basic Mode 1: Extend Mode - DBA 2: Extend Mode -
Service-Specific - Fraction STS-1(VC-3) Encapsulation 3: Extend Mode - Service-
Specific - Asynchronous T3/E3 STS-1(VC-3) Encapsulation. 4: Extend Mode -
Service-Specific - Fraction VC-4 Encapsulation.
BitField Bits: [25:23]
--------------------------------------*/
#define cAf6_upen_flowtab_cepmode_Mask                                                               cBit25_23
#define cAf6_upen_flowtab_cepmode_Shift                                                                     23

/*--------------------------------------
BitField Name: cemmode
BitField Type: RW
BitField Desc: CEM mode 0: SAToP 1: CESoPSN 2: CEP
BitField Bits: [22:21]
--------------------------------------*/
#define cAf6_upen_flowtab_cemmode_Mask                                                               cBit22_21
#define cAf6_upen_flowtab_cemmode_Shift                                                                     21

/*--------------------------------------
BitField Name: ifrtyp
BitField Type: RW
BitField Desc: iFR subtype 0: NLPID 1: SNAP
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_upen_flowtab_ifrtyp_Mask                                                                   cBit20
#define cAf6_upen_flowtab_ifrtyp_Shift                                                                      20

/*--------------------------------------
BitField Name: mplsstyp
BitField Type: RW
BitField Desc: MPLS subtype 0 : none 1 : ipv4ompls 2 : ipv6ompls 3 : ethompls 4
: hdlcompls 5 : pppompls 6 : frompls 7 : mplsvlan
BitField Bits: [19:17]
--------------------------------------*/
#define cAf6_upen_flowtab_mplsstyp_Mask                                                              cBit19_17
#define cAf6_upen_flowtab_mplsstyp_Shift                                                                    17

/*--------------------------------------
BitField Name: tdmenc
BitField Type: RW
BitField Desc: TDM encap type 0 : HDLC 1 : cHDLC 2 : PPP 3 : PPP Bridge 4 :
MLPPP 5 : cFR 6 : cMLFR 7 : iFR 8 : iMLFR
BitField Bits: [16:13]
--------------------------------------*/
#define cAf6_upen_flowtab_tdmenc_Mask                                                                cBit16_13
#define cAf6_upen_flowtab_tdmenc_Shift                                                                      13

/*--------------------------------------
BitField Name: serid
BitField Type: RW
BitField Desc: Service ID PW    : 5376 VCG   : 256 Link  : 3072 Bundle: 512
BitField Bits: [12:00]
--------------------------------------*/
#define cAf6_upen_flowtab_serid_Mask                                                                  cBit12_0
#define cAf6_upen_flowtab_serid_Shift                                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Classify Per Pseudowire Slice to CDR Control
Reg Addr   : 0x00_2000-0x3FFF
Reg Formula: 0x00_2000 + $pwid
    Where  :
           + $pwid(0-8191)
Reg Desc   :
This register configures Slice ID to CDR
HDL_PATH     : inscorepi.label0.inscdrctl.membuf.ram.ram[$pwid]

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cdrctrl_Base                                                                     0x002000

/*--------------------------------------
BitField Name: PwCdrEn
BitField Type: RW
BitField Desc: Indicate Pseudowire enable 1: enable 0: disabel
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_upen_cdrctrl_PwCdrEn_Mask                                                                  cBit15
#define cAf6_upen_cdrctrl_PwCdrEn_Shift                                                                     15

/*--------------------------------------
BitField Name: PwHoLoOc48Id
BitField Type: RW
BitField Desc: Indicate 8x Hi order OC48 or 6x Low order OC48 slice
BitField Bits: [13:11]
--------------------------------------*/
#define cAf6_upen_cdrctrl_PwHoLoOc48Id_Mask                                                          cBit13_11
#define cAf6_upen_cdrctrl_PwHoLoOc48Id_Shift                                                                11

/*--------------------------------------
BitField Name: PwTdmLineId
BitField Type: RW
BitField Desc: Pseudo-wire(PW) corresponding TDM line ID. If the PW belong to
Low order path, this is the OC24 TDM line ID that is using in Lo CDR,PDH and
MAP. If the PW belong to Hi order CEP path, this is the OC48 master STS ID
BitField Bits: [10:00]
--------------------------------------*/
#define cAf6_upen_cdrctrl_PwTdmLineId_Mask                                                            cBit10_0
#define cAf6_upen_cdrctrl_PwTdmLineId_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : First VLAN service control
Reg Addr   : 0x00_1000-0x1FFF
Reg Formula: 0x00_1000 + $firstvlanid
    Where  :
           + $firstvlanid(0-4095)
Reg Desc   :
This register configures service base on first vlan
HDL_PATH     : inscorepi.label0.insfvlnser.membuf.ram.ram[$firstvlanid]

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_fvlnser_Base                                                                     0x001000

/*--------------------------------------
BitField Name: linknxt0
BitField Type: RW
BitField Desc: link Next port0 0: Termination 1: Next
BitField Bits: [32:32]
--------------------------------------*/
#define cAf6_upen_fvlnser_linknxt0_Mask                                                                  cBit0
#define cAf6_upen_fvlnser_linknxt0_Shift                                                                     0

/*--------------------------------------
BitField Name: linken0
BitField Type: RW
BitField Desc: link id enable port0 0: disable 1: enable
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_upen_fvlnser_linken0_Mask                                                                  cBit31
#define cAf6_upen_fvlnser_linken0_Shift                                                                     31

/*--------------------------------------
BitField Name: linkid0
BitField Type: RW
BitField Desc: Link ID for OAM
BitField Bits: [30:19]
--------------------------------------*/
#define cAf6_upen_fvlnser_linkid0_Mask                                                               cBit30_19
#define cAf6_upen_fvlnser_linkid0_Shift                                                                     19

/*--------------------------------------
BitField Name: outen0
BitField Type: RW
BitField Desc: Output enable port0 0: disable 1: enable
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_upen_fvlnser_outen0_Mask                                                                   cBit18
#define cAf6_upen_fvlnser_outen0_Shift                                                                      18

/*--------------------------------------
BitField Name: sertyp0
BitField Type: RW
BitField Desc: service type port0 0: DROP 1: CEM 2: iMSG 3: DCC 4: Eth PASS 5:
PTP 6: EoS 7: EoP
BitField Bits: [17:15]
--------------------------------------*/
#define cAf6_upen_fvlnser_sertyp0_Mask                                                               cBit17_15
#define cAf6_upen_fvlnser_sertyp0_Shift                                                                     15

/*--------------------------------------
BitField Name: serstyp0
BitField Type: RW
BitField Desc: service subtype port0 0: PW 1: DATA 2: OAM 3: CPU
BitField Bits: [14:13]
--------------------------------------*/
#define cAf6_upen_fvlnser_serstyp0_Mask                                                              cBit14_13
#define cAf6_upen_fvlnser_serstyp0_Shift                                                                    13

/*--------------------------------------
BitField Name: flowid0
BitField Type: RW
BitField Desc: Flow ID port0
BitField Bits: [12:00]
--------------------------------------*/
#define cAf6_upen_fvlnser_flowid0_Mask                                                                cBit12_0
#define cAf6_upen_fvlnser_flowid0_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Second VLAN service control
Reg Addr   : 0x00_4000-0x4FFF
Reg Formula: 0x00_4000 + $secondvlanid
    Where  :
           + $secondvlanid(0-4095)
Reg Desc   :
This register configures service base on second vlan
HDL_PATH     : inscorepi.label0.inssvlnser.membuf.ram.ram[$secondvlanid]

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_svlnser_Base                                                                     0x004000

/*--------------------------------------
BitField Name: linknxt0
BitField Type: RW
BitField Desc: link Next port0 0: Termination 1: Next
BitField Bits: [32:32]
--------------------------------------*/
#define cAf6_upen_svlnser_linknxt0_Mask                                                               cBit32_0
#define cAf6_upen_svlnser_linknxt0_Shift                                                                     0

/*--------------------------------------
BitField Name: linken0
BitField Type: RW
BitField Desc: link id enable port0 0: disable 1: enable
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_upen_svlnser_linken0_Mask                                                                  cBit31
#define cAf6_upen_svlnser_linken0_Shift                                                                     31

/*--------------------------------------
BitField Name: linkid0
BitField Type: RW
BitField Desc: Link ID for OAM
BitField Bits: [30:19]
--------------------------------------*/
#define cAf6_upen_svlnser_linkid0_Mask                                                               cBit30_19
#define cAf6_upen_svlnser_linkid0_Shift                                                                     19

/*--------------------------------------
BitField Name: outen0
BitField Type: RW
BitField Desc: Output enable port0 0: disable 1: enable
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_upen_svlnser_outen0_Mask                                                                   cBit18
#define cAf6_upen_svlnser_outen0_Shift                                                                      18

/*--------------------------------------
BitField Name: sertyp0
BitField Type: RW
BitField Desc: service type port0 0: DROP 1: CEM 2: iMSG 3: DCC 4: Eth PASS 5:
PTP 6: EoS 7: EoP
BitField Bits: [17:15]
--------------------------------------*/
#define cAf6_upen_svlnser_sertyp0_Mask                                                               cBit17_15
#define cAf6_upen_svlnser_sertyp0_Shift                                                                     15

/*--------------------------------------
BitField Name: serstyp0
BitField Type: RW
BitField Desc: service subtype port0 0: PW 1: DATA 2: OAM 3: CPU
BitField Bits: [14:13]
--------------------------------------*/
#define cAf6_upen_svlnser_serstyp0_Mask                                                              cBit14_13
#define cAf6_upen_svlnser_serstyp0_Shift                                                                    13

/*--------------------------------------
BitField Name: flowid0
BitField Type: RW
BitField Desc: Flow ID       port0
BitField Bits: [12:00]
--------------------------------------*/
#define cAf6_upen_svlnser_flowid0_Mask                                                                cBit12_0
#define cAf6_upen_svlnser_flowid0_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : PORT service control
Reg Addr   : 0x00_51C0-0x51CF
Reg Formula: 0x00_51C0 + $pid
    Where  :
           + $pid(0-15)
Reg Desc   :
This register configures service base on port id value
HDL_PATH     : inscorepi.insportser.membuf.ram.ram[$pid]

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_portser_Base                                                                     0x0051C0

/*--------------------------------------
BitField Name: outen
BitField Type: RW
BitField Desc: Output enable 0: disable 1: enable
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_upen_portser_outen_Mask                                                                    cBit18
#define cAf6_upen_portser_outen_Shift                                                                       18

/*--------------------------------------
BitField Name: sertyp
BitField Type: RW
BitField Desc: service type 0: DROP 1: CEM 2: iMSG 3: DCC 4: Eth PASS 5: PTP 6:
EoS 7: EoP
BitField Bits: [17:15]
--------------------------------------*/
#define cAf6_upen_portser_sertyp_Mask                                                                cBit17_15
#define cAf6_upen_portser_sertyp_Shift                                                                      15

/*--------------------------------------
BitField Name: serstyp
BitField Type: RW
BitField Desc: service subtype 0: PW 1: DATA 2: OAM 3: CPU
BitField Bits: [14:13]
--------------------------------------*/
#define cAf6_upen_portser_serstyp_Mask                                                               cBit14_13
#define cAf6_upen_portser_serstyp_Shift                                                                     13

/*--------------------------------------
BitField Name: flowid
BitField Type: RW
BitField Desc: Flow ID
BitField Bits: [12:00]
--------------------------------------*/
#define cAf6_upen_portser_flowid_Mask                                                                 cBit12_0
#define cAf6_upen_portser_flowid_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : rule check control
Reg Addr   : 0x00_51A0-0x51BF
Reg Formula: 0x00_51A0 + $numtab
    Where  :
           + $numtab(0-7)
Reg Desc   :
is used to chose serid, linkid, sertyp from port/ptch/fvlans/svlan/pwhash table
numtab = 0 when terminate at port   table (  port_ssertyp = DATA)
numtab = 1 when terminate at ptch   table (  ptch_ssertyp = DATA)
numtab = 2 when terminate at fvlan  table ( fvlan_ssertyp = DATA)
numtab = 3 when terminate at svlan  table ( svlan_ssertyp = DATA)
numtab = 4 when terminate at pwhash table (pwhash_ssertyp = PW)
HDL_PATH     : inscorepi.insrulechk.membuf.ram.ram[$numtab]

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rulechk_Base                                                                     0x0051A0

/*--------------------------------------
BitField Name: outen
BitField Type: RW
BitField Desc: Output enable 0: disable 1: enable
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_upen_rulechk_outen_Mask                                                                     cBit9
#define cAf6_upen_rulechk_outen_Shift                                                                        9

/*--------------------------------------
BitField Name: sertyp_tabid
BitField Type: RW
BitField Desc: select sertype from 0: PORT service table 1: PTCH service table
2: FVLN service table 3: SVLN service table 4: PWHASH service table
BitField Bits: [08:06]
--------------------------------------*/
#define cAf6_upen_rulechk_sertyp_tabid_Mask                                                            cBit8_6
#define cAf6_upen_rulechk_sertyp_tabid_Shift                                                                 6

/*--------------------------------------
BitField Name: linkid_tabid
BitField Type: RW
BitField Desc: select sertype from 2: FVLN service table 3: SVLN service table
BitField Bits: [05:03]
--------------------------------------*/
#define cAf6_upen_rulechk_linkid_tabid_Mask                                                            cBit5_3
#define cAf6_upen_rulechk_linkid_tabid_Shift                                                                 3

/*--------------------------------------
BitField Name: flowid_tabid
BitField Type: RW
BitField Desc: select FlowID from 0: PORT service table 1: PTCH service table 2:
FVLN service table 3: SVLN service table 4: PWHASH service table
BitField Bits: [02:00]
--------------------------------------*/
#define cAf6_upen_rulechk_flowid_tabid_Mask                                                            cBit2_0
#define cAf6_upen_rulechk_flowid_tabid_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : TDM Header Encapsulation for PDU
Reg Addr   : 0x00_C000-0x00_DFFF
Reg Formula: 0x00_C000 + $flowid
    Where  :
           + $flowid(0-8191)
Reg Desc   :
This register configures TDM header value for TDM encapsulation
HDL_PATH     : inscorepi.label2.insptl.membuf.ram.ram[$flowid]

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_hdrenc_Base                                                                      0x00C000

/*--------------------------------------
BitField Name: hdrenc_val
BitField Type: RW
BitField Desc: Header Encapsulation (= TDM header + PROTOCOL Header) Upto 16
byte TDM      header: upto 8 byte PROTOCOL header: upto 8 byte
BitField Bits: [127:00]
--------------------------------------*/
#define cAf6_upen_hdrenc_hdrenc_val_01_Mask                                                           cBit31_0
#define cAf6_upen_hdrenc_hdrenc_val_01_Shift                                                                 0
#define cAf6_upen_hdrenc_hdrenc_val_02_Mask                                                           cBit31_0
#define cAf6_upen_hdrenc_hdrenc_val_02_Shift                                                                 0
#define cAf6_upen_hdrenc_hdrenc_val_03_Mask                                                           cBit31_0
#define cAf6_upen_hdrenc_hdrenc_val_03_Shift                                                                 0
#define cAf6_upen_hdrenc_hdrenc_val_04_Mask                                                           cBit31_0
#define cAf6_upen_hdrenc_hdrenc_val_04_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Classify HCBE Looking Up Information Control
Reg Addr   : 0x00_8000-0x00_9FFF
Reg Formula: 0x00_8000 + $table_id*0x800 + $hash_id
    Where  :
           + $table_id (0-3)
           + $hash_id(0-2047): also CRC_ID
Reg Desc   :
Depend on Hash CRC Pattern Control register
Input key  (22bits):
Hash_Key[21:20]: PW Type(0: mpls,1: mef , 2: udpoip)
Hash_Key[19:00]: PW Label ID
HDL_PATH: begin:
IF($table_id == 0)
inscorepi.label5.inshashpw.h_inst[0].itable.iram_cfg.iram.iram.ram[$hash_id]
ELSEIF($table_id == 1)
inscorepi.label5.inshashpw.h_inst[1].itable.iram_cfg.iram.iram.ram[$hash_id]
ELSEIF($table_id == 2)
inscorepi.label5.inshashpw.h_inst[2].itable.iram_cfg.iram.iram.ram[$hash_id]
ELSE
inscorepi.label5.inshashpw.h_inst[3].itable.iram_cfg.iram.iram.ram[$hash_id]
HDL_PATH: end:

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_pwhash_Base                                                                      0x008000

/*--------------------------------------
BitField Name: Outen
BitField Type: RW
BitField Desc: Output enable 0: disable 1: enable
BitField Bits: [41:41]
--------------------------------------*/
#define cAf6_upen_pwhash_Outen_Mask                                                                      cBit9
#define cAf6_upen_pwhash_Outen_Shift                                                                         9

/*--------------------------------------
BitField Name: Sertyp
BitField Type: RW
BitField Desc: service type 0: DROP 1: CEM 2: iMSG 3: DCC 4: Eth PASS 5: PTP 6:
EoS 7: EoP
BitField Bits: [40:38]
--------------------------------------*/
#define cAf6_upen_pwhash_Sertyp_Mask                                                                   cBit8_6
#define cAf6_upen_pwhash_Sertyp_Shift                                                                        6

/*--------------------------------------
BitField Name: Serstyp
BitField Type: RW
BitField Desc: service subtype 0: PW 1: DATA 2: OAM 3: CPU
BitField Bits: [37:36]
--------------------------------------*/
#define cAf6_upen_pwhash_Serstyp_Mask                                                                  cBit5_4
#define cAf6_upen_pwhash_Serstyp_Shift                                                                       4

/*--------------------------------------
BitField Name: FlowID
BitField Type: RW
BitField Desc: Flow ID
BitField Bits: [35:23]
--------------------------------------*/
#define cAf6_upen_pwhash_FlowID_01_Mask                                                              cBit31_23
#define cAf6_upen_pwhash_FlowID_01_Shift                                                                    23
#define cAf6_upen_pwhash_FlowID_02_Mask                                                                cBit3_0
#define cAf6_upen_pwhash_FlowID_02_Shift                                                                     0

/*--------------------------------------
BitField Name: Hash_Key
BitField Type: RW
BitField Desc: Hash_Key
BitField Bits: [22:01]
--------------------------------------*/
#define cAf6_upen_pwhash_Hash_Key_Mask                                                                cBit22_1
#define cAf6_upen_pwhash_Hash_Key_Shift                                                                      1

/*--------------------------------------
BitField Name: Hash_Valid
BitField Type: RW
BitField Desc: 0: LookUp Fail
BitField Bits: [0:0]
--------------------------------------*/
#define cAf6_upen_pwhash_Hash_Valid_Mask                                                                 cBit0
#define cAf6_upen_pwhash_Hash_Valid_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Type CAM
Reg Addr   : 0x00_51E0-0x00_51FF
Reg Formula: 0x00_51E0 + $entry
    Where  :
           + $entry(0-31)
Reg Desc   :
This register configures ethernet type of packet

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_etypcam_Base                                                                     0x0051E0

/*--------------------------------------
BitField Name: ethtyp
BitField Type: RW
BitField Desc: Ethernet type entry 0: 0x0800 (IPv4) entry 1: 0x86DD (IPv6) entry
2: 0x8847 (MPLS unicast) entry 3: 0x8848 (MPLS multicast) entry 4-15: reserved
for DATA entry 16: 0x8857 (OAM) entry 17-21: reserved for OAM entry 22: MEF
ethernet type entry 23 - entry 31: unused
BitField Bits: [16:01]
--------------------------------------*/
#define cAf6_upen_etypcam_ethtyp_Mask                                                                 cBit16_1
#define cAf6_upen_etypcam_ethtyp_Shift                                                                       1

/*--------------------------------------
BitField Name: cametyp_vld
BitField Type: RW
BitField Desc: 0: lookup fail
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_etypcam_cametyp_vld_Mask                                                               cBit0
#define cAf6_upen_etypcam_cametyp_vld_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Type CAM
Reg Addr   : 0x00_5210-0x00_521F
Reg Formula: 0x00_5210 + $id
    Where  :
           + $id(0-15)
Reg Desc   :
use CAM in case Hash Table is fail

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_pwhashcam_Base                                                                   0x005210

/*--------------------------------------
BitField Name: Outen
BitField Type: RW
BitField Desc: Output enable 0: disable 1: enable
BitField Bits: [40:40]
--------------------------------------*/
#define cAf6_upen_pwhashcam_Outen_Mask                                                                   cBit8
#define cAf6_upen_pwhashcam_Outen_Shift                                                                      8

/*--------------------------------------
BitField Name: Sertyp
BitField Type: RW
BitField Desc: service type 0: DROP 1: CEM 2: iMSG 3: DCC 4: Eth PASS 5: PTP 6:
EoS 7: Eop
BitField Bits: [39:37]
--------------------------------------*/
#define cAf6_upen_pwhashcam_Sertyp_Mask                                                                cBit7_5
#define cAf6_upen_pwhashcam_Sertyp_Shift                                                                     5

/*--------------------------------------
BitField Name: Serstyp
BitField Type: RW
BitField Desc: service subtype 0: PW 1: DATA 2: OAM 3: CPU
BitField Bits: [36:35]
--------------------------------------*/
#define cAf6_upen_pwhashcam_Serstyp_Mask                                                               cBit4_3
#define cAf6_upen_pwhashcam_Serstyp_Shift                                                                    3

/*--------------------------------------
BitField Name: FlowID
BitField Type: RW
BitField Desc: Flow ID
BitField Bits: [34:23]
--------------------------------------*/
#define cAf6_upen_pwhashcam_FlowID_01_Mask                                                           cBit31_23
#define cAf6_upen_pwhashcam_FlowID_01_Shift                                                                 23
#define cAf6_upen_pwhashcam_FlowID_02_Mask                                                             cBit2_0
#define cAf6_upen_pwhashcam_FlowID_02_Shift                                                                  0

/*--------------------------------------
BitField Name: Hash_Key
BitField Type: RW
BitField Desc: Hash_Key
BitField Bits: [22:01]
--------------------------------------*/
#define cAf6_upen_pwhashcam_Hash_Key_Mask                                                             cBit22_1
#define cAf6_upen_pwhashcam_Hash_Key_Shift                                                                   1

/*--------------------------------------
BitField Name: Hash_Valid
BitField Type: RW
BitField Desc: 0: LookUp Fail
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_pwhashcam_Hash_Valid_Mask                                                              cBit0
#define cAf6_upen_pwhashcam_Hash_Valid_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : eth2cla sticky
Reg Addr   : 0x00_0100
Reg Formula:
    Where  :
Reg Desc   :
sticky of eth2cla interface

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_eth2cla_stk_Base                                                                 0x000100

/*--------------------------------------
BitField Name: eth0_err
BitField Type: W1C
BitField Desc: ETH_0 ERR  : 0: clear  1: set(XFI#0)
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_eth2cla_stk_eth0_err_Mask                                                              cBit3
#define cAf6_upen_eth2cla_stk_eth0_err_Shift                                                                 3

/*--------------------------------------
BitField Name: eth0_eop
BitField Type: W1C
BitField Desc: ETH_0 EOP  : 0: clear  1: set
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_eth2cla_stk_eth0_eop_Mask                                                              cBit2
#define cAf6_upen_eth2cla_stk_eth0_eop_Shift                                                                 2

/*--------------------------------------
BitField Name: eth0_sop
BitField Type: W1C
BitField Desc: ETH_0 SOP  : 0: clear  1: set
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_eth2cla_stk_eth0_sop_Mask                                                              cBit1
#define cAf6_upen_eth2cla_stk_eth0_sop_Shift                                                                 1

/*--------------------------------------
BitField Name: eth0_vld
BitField Type: W1C
BitField Desc: ETH_0 VALID: 0: clear  1: set
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_eth2cla_stk_eth0_vld_Mask                                                              cBit0
#define cAf6_upen_eth2cla_stk_eth0_vld_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : cla2epa sticky
Reg Addr   : 0x00_0101
Reg Formula:
    Where  :
Reg Desc   :
sticky of cla2epa interface

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cla2epa_stk_Base                                                                 0x000101

/*--------------------------------------
BitField Name: cla2epa_stk1
BitField Type: W1C
BitField Desc: sticky cla to epa (XFI#1)
BitField Bits: [15:08]
--------------------------------------*/
#define cAf6_upen_cla2epa_stk_cla2epa_stk1_Mask                                                       cBit15_8
#define cAf6_upen_cla2epa_stk_cla2epa_stk1_Shift                                                             8

/*--------------------------------------
BitField Name: cla2epa_stk0
BitField Type: W1C
BitField Desc: sticky cla to epa (XFI#0)
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_upen_cla2epa_stk_cla2epa_stk0_Mask                                                        cBit7_0
#define cAf6_upen_cla2epa_stk_cla2epa_stk0_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : cla2ptp sticky
Reg Addr   : 0x00_0102
Reg Formula:
    Where  :
Reg Desc   :
sticky of cla2ptp interface

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cla2ptp_stk_Base                                                                 0x000102

/*--------------------------------------
BitField Name: cla2ptp_stk1
BitField Type: W1C
BitField Desc: sticky cla to ptp (XFI#1)
BitField Bits: [15:08]
--------------------------------------*/
#define cAf6_upen_cla2ptp_stk_cla2ptp_stk1_Mask                                                       cBit15_8
#define cAf6_upen_cla2ptp_stk_cla2ptp_stk1_Shift                                                             8

/*--------------------------------------
BitField Name: cla2ptp_stk0
BitField Type: W1C
BitField Desc: sticky cla to ptp (XFI#0)
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_upen_cla2ptp_stk_cla2ptp_stk0_Mask                                                        cBit7_0
#define cAf6_upen_cla2ptp_stk_cla2ptp_stk0_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : cla2pda sticky
Reg Addr   : 0x00_0103
Reg Formula:
    Where  :
Reg Desc   :
sticky of cla2pda interface

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cla2pda_stk_Base                                                                 0x000103

/*--------------------------------------
BitField Name: eof
BitField Type: W1C
BitField Desc: end   of frgment : 0: clear , 1:set
BitField Bits: [77:77]
--------------------------------------*/
#define cAf6_upen_cla2pda_stk_eof_Mask                                                                  cBit13
#define cAf6_upen_cla2pda_stk_eof_Shift                                                                     13

/*--------------------------------------
BitField Name: sof
BitField Type: W1C
BitField Desc: start of frgment : 0: clear , 1:set
BitField Bits: [76:76]
--------------------------------------*/
#define cAf6_upen_cla2pda_stk_sof_Mask                                                                  cBit12
#define cAf6_upen_cla2pda_stk_sof_Shift                                                                     12

/*--------------------------------------
BitField Name: ocw_lbit
BitField Type: W1C
BitField Desc: ocw_lbit      : 0: clear , 1:set
BitField Bits: [69:69]
--------------------------------------*/
#define cAf6_upen_cla2pda_stk_ocw_lbit_Mask                                                              cBit5
#define cAf6_upen_cla2pda_stk_ocw_lbit_Shift                                                                 5

/*--------------------------------------
BitField Name: ocw_rbit
BitField Type: W1C
BitField Desc: ocw_rbit      : 0: clear , 1:set
BitField Bits: [68:68]
--------------------------------------*/
#define cAf6_upen_cla2pda_stk_ocw_rbit_Mask                                                              cBit4
#define cAf6_upen_cla2pda_stk_ocw_rbit_Shift                                                                 4

/*--------------------------------------
BitField Name: ocw_nbit
BitField Type: W1C
BitField Desc: ocw_nbit      : 0: clear , 1:set
BitField Bits: [67:67]
--------------------------------------*/
#define cAf6_upen_cla2pda_stk_ocw_nbit_Mask                                                              cBit3
#define cAf6_upen_cla2pda_stk_ocw_nbit_Shift                                                                 3

/*--------------------------------------
BitField Name: ocw_pbit
BitField Type: W1C
BitField Desc: ocw_pbit      : 0: clear , 1:set
BitField Bits: [66:66]
--------------------------------------*/
#define cAf6_upen_cla2pda_stk_ocw_pbit_Mask                                                              cBit2
#define cAf6_upen_cla2pda_stk_ocw_pbit_Shift                                                                 2

/*--------------------------------------
BitField Name: ocw_fbit1
BitField Type: W1C
BitField Desc: ocw_fbit1     : 0: clear , 1:set
BitField Bits: [65:65]
--------------------------------------*/
#define cAf6_upen_cla2pda_stk_ocw_fbit1_Mask                                                             cBit1
#define cAf6_upen_cla2pda_stk_ocw_fbit1_Shift                                                                1

/*--------------------------------------
BitField Name: ocw_fbit0
BitField Type: W1C
BitField Desc: ocw_fbit0     : 0: clear , 1:set
BitField Bits: [64:64]
--------------------------------------*/
#define cAf6_upen_cla2pda_stk_ocw_fbit0_Mask                                                             cBit0
#define cAf6_upen_cla2pda_stk_ocw_fbit0_Shift                                                                0

/*--------------------------------------
BitField Name: ser_mlfrdata
BitField Type: W1C
BitField Desc: SER MLFR  DATA : 0: clear , 1:set
BitField Bits: [35:35]
--------------------------------------*/
#define cAf6_upen_cla2pda_stk_ser_mlfrdata_Mask                                                          cBit3
#define cAf6_upen_cla2pda_stk_ser_mlfrdata_Shift                                                             3

/*--------------------------------------
BitField Name: ser_mlfroam
BitField Type: W1C
BitField Desc: SER MLFR  OAM  : 0: clear , 1:set
BitField Bits: [34:34]
--------------------------------------*/
#define cAf6_upen_cla2pda_stk_ser_mlfroam_Mask                                                           cBit2
#define cAf6_upen_cla2pda_stk_ser_mlfroam_Shift                                                              2

/*--------------------------------------
BitField Name: ser_frdata
BitField Type: W1C
BitField Desc: SER FR    DATA : 0: clear , 1:set
BitField Bits: [33:33]
--------------------------------------*/
#define cAf6_upen_cla2pda_stk_ser_frdata_Mask                                                            cBit1
#define cAf6_upen_cla2pda_stk_ser_frdata_Shift                                                               1

/*--------------------------------------
BitField Name: ser_froam
BitField Type: W1C
BitField Desc: SER FR    OAM  : 0: clear , 1:set
BitField Bits: [32:32]
--------------------------------------*/
#define cAf6_upen_cla2pda_stk_ser_froam_Mask                                                          cBit0
#define cAf6_upen_cla2pda_stk_ser_froam_Shift                                                             0

/*--------------------------------------
BitField Name: ser_mlpppdata
BitField Type: W1C
BitField Desc: SER MLPPP DATA : 0: clear , 1:set
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_upen_cla2pda_stk_ser_mlpppdata_Mask                                                        cBit31
#define cAf6_upen_cla2pda_stk_ser_mlpppdata_Shift                                                           31

/*--------------------------------------
BitField Name: ser_mlpppoam
BitField Type: W1C
BitField Desc: SER MLPPP OAM  : 0: clear , 1:set
BitField Bits: [30:30]
--------------------------------------*/
#define cAf6_upen_cla2pda_stk_ser_mlpppoam_Mask                                                         cBit30
#define cAf6_upen_cla2pda_stk_ser_mlpppoam_Shift                                                            30

/*--------------------------------------
BitField Name: ser_pppdata
BitField Type: W1C
BitField Desc: SER PPP   DATA : 0: clear , 1:set
BitField Bits: [29:29]
--------------------------------------*/
#define cAf6_upen_cla2pda_stk_ser_pppdata_Mask                                                          cBit29
#define cAf6_upen_cla2pda_stk_ser_pppdata_Shift                                                             29

/*--------------------------------------
BitField Name: ser_pppoam
BitField Type: W1C
BitField Desc: SER PPP   OAM  : 0: clear , 1:set
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_upen_cla2pda_stk_ser_pppoam_Mask                                                           cBit28
#define cAf6_upen_cla2pda_stk_ser_pppoam_Shift                                                              28

/*--------------------------------------
BitField Name: ser_pppbrd
BitField Type: W1C
BitField Desc: SER PPP   BRD  : 0: clear , 1:set
BitField Bits: [27:27]
--------------------------------------*/
#define cAf6_upen_cla2pda_stk_ser_pppbrd_Mask                                                           cBit27
#define cAf6_upen_cla2pda_stk_ser_pppbrd_Shift                                                              27

/*--------------------------------------
BitField Name: ser_chdlc
BitField Type: W1C
BitField Desc: SER cHDLC  :   0: clear , 1:set
BitField Bits: [26:26]
--------------------------------------*/
#define cAf6_upen_cla2pda_stk_ser_chdlc_Mask                                                            cBit26
#define cAf6_upen_cla2pda_stk_ser_chdlc_Shift                                                               26

/*--------------------------------------
BitField Name: ser_epass
BitField Type: W1C
BitField Desc: SER EPASS  :   0: clear , 1:set
BitField Bits: [25:25]
--------------------------------------*/
#define cAf6_upen_cla2pda_stk_ser_epass_Mask                                                            cBit25
#define cAf6_upen_cla2pda_stk_ser_epass_Shift                                                               25

/*--------------------------------------
BitField Name: ser_hdlc
BitField Type: W1C
BitField Desc: SER HDLC   :   0: clear , 1:set
BitField Bits: [24:24]
--------------------------------------*/
#define cAf6_upen_cla2pda_stk_ser_hdlc_Mask                                                             cBit24
#define cAf6_upen_cla2pda_stk_ser_hdlc_Shift                                                                24

/*--------------------------------------
BitField Name: ser_cep
BitField Type: W1C
BitField Desc: SER CEP    :   0: clear , 1:set
BitField Bits: [23:23]
--------------------------------------*/
#define cAf6_upen_cla2pda_stk_ser_cep_Mask                                                              cBit23
#define cAf6_upen_cla2pda_stk_ser_cep_Shift                                                                 23

/*--------------------------------------
BitField Name: ser_eop
BitField Type: W1C
BitField Desc: SER EOP/EOS    : 0: clear , 1:set
BitField Bits: [22:22]
--------------------------------------*/
#define cAf6_upen_cla2pda_stk_ser_eop_Mask                                                              cBit22
#define cAf6_upen_cla2pda_stk_ser_eop_Shift                                                                 22

/*--------------------------------------
BitField Name: ser_ces
BitField Type: W1C
BitField Desc: SER CES    :   0: clear , 1:set
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_upen_cla2pda_stk_ser_ces_Mask                                                              cBit21
#define cAf6_upen_cla2pda_stk_ser_ces_Shift                                                                 21

/*--------------------------------------
BitField Name: ser_discard
BitField Type: W1C
BitField Desc: SER DISCARD    : 0: clear , 1:set
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_upen_cla2pda_stk_ser_discard_Mask                                                          cBit20
#define cAf6_upen_cla2pda_stk_ser_discard_Shift                                                             20

/*--------------------------------------
BitField Name: etyp_mef
BitField Type: W1C
BitField Desc: ETYP MEF       : 0: clear , 1: set
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_upen_cla2pda_stk_etyp_mef_Mask                                                             cBit17
#define cAf6_upen_cla2pda_stk_etyp_mef_Shift                                                                17

/*--------------------------------------
BitField Name: etyp_oamind
BitField Type: W1C
BitField Desc: ETYP OAM       : 0: clear , 1: set
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_upen_cla2pda_stk_etyp_oamind_Mask                                                          cBit16
#define cAf6_upen_cla2pda_stk_etyp_oamind_Shift                                                             16

/*--------------------------------------
BitField Name: etyp_mmplsind
BitField Type: W1C
BitField Desc: ETYP MPLS MUL  : 0: clear , 1: set
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_upen_cla2pda_stk_etyp_mmplsind_Mask                                                        cBit15
#define cAf6_upen_cla2pda_stk_etyp_mmplsind_Shift                                                           15

/*--------------------------------------
BitField Name: etyp_umplsind
BitField Type: W1C
BitField Desc: ETYP MPLS UNI  : 0: clear , 1: set
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_upen_cla2pda_stk_etyp_umplsind_Mask                                                        cBit14
#define cAf6_upen_cla2pda_stk_etyp_umplsind_Shift                                                           14

/*--------------------------------------
BitField Name: etyp_ipv6ind
BitField Type: W1C
BitField Desc: ETYP IPV6      : 0: clear , 1: set
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_upen_cla2pda_stk_etyp_ipv6ind_Mask                                                         cBit13
#define cAf6_upen_cla2pda_stk_etyp_ipv6ind_Shift                                                            13

/*--------------------------------------
BitField Name: etyp_ipv4ind
BitField Type: W1C
BitField Desc: ETYP IPV4      : 0: clear , 1: set
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_upen_cla2pda_stk_etyp_ipv4ind_Mask                                                         cBit12
#define cAf6_upen_cla2pda_stk_etyp_ipv4ind_Shift                                                            12

/*--------------------------------------
BitField Name: cla2pda_err8
BitField Type: W1C
BitField Desc: DISCARD SERVICE : 0: clear, 1: set
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_upen_cla2pda_stk_cla2pda_err8_Mask                                                         cBit11
#define cAf6_upen_cla2pda_stk_cla2pda_err8_Shift                                                            11

/*--------------------------------------
BitField Name: cla2pda_err7
BitField Type: W1C
BitField Desc: MALFORM ERROR   : 0: clear, 1: set
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_upen_cla2pda_stk_cla2pda_err7_Mask                                                         cBit10
#define cAf6_upen_cla2pda_stk_cla2pda_err7_Shift                                                            10

/*--------------------------------------
BitField Name: cla2pda_err6
BitField Type: W1C
BitField Desc: PT      ERROR   : 0: clear, 1: set
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_upen_cla2pda_stk_cla2pda_err6_Mask                                                          cBit9
#define cAf6_upen_cla2pda_stk_cla2pda_err6_Shift                                                             9

/*--------------------------------------
BitField Name: cla2pda_err5
BitField Type: W1C
BitField Desc: SSRC    ERROR   : 0: clear, 1: set
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_upen_cla2pda_stk_cla2pda_err5_Mask                                                          cBit8
#define cAf6_upen_cla2pda_stk_cla2pda_err5_Shift                                                             8

/*--------------------------------------
BitField Name: cla2pda_err4
BitField Type: W1C
BitField Desc: MRU     ERROR   : 0: clear, 1: set
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_upen_cla2pda_stk_cla2pda_err4_Mask                                                          cBit7
#define cAf6_upen_cla2pda_stk_cla2pda_err4_Shift                                                             7

/*--------------------------------------
BitField Name: cla2pda_err3
BitField Type: W1C
BitField Desc: BUFFER  FULL    : 0: clear, 1: set
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_upen_cla2pda_stk_cla2pda_err3_Mask                                                          cBit6
#define cAf6_upen_cla2pda_stk_cla2pda_err3_Shift                                                             6

/*--------------------------------------
BitField Name: cla2pda_err2
BitField Type: W1C
BitField Desc: LOOKUP  ERROR   : 0: clear, 1: set
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_cla2pda_stk_cla2pda_err2_Mask                                                          cBit5
#define cAf6_upen_cla2pda_stk_cla2pda_err2_Shift                                                             5

/*--------------------------------------
BitField Name: cla2pda_err1
BitField Type: W1C
BitField Desc: MACDA   ERROR   : 0: clear, 1: set
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_cla2pda_stk_cla2pda_err1_Mask                                                          cBit4
#define cAf6_upen_cla2pda_stk_cla2pda_err1_Shift                                                             4

/*--------------------------------------
BitField Name: cla2pda_err0
BitField Type: W1C
BitField Desc: ETH     ERROR   : 0: clear, 1: set
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_cla2pda_stk_cla2pda_err0_Mask                                                          cBit3
#define cAf6_upen_cla2pda_stk_cla2pda_err0_Shift                                                             3

/*--------------------------------------
BitField Name: cla2pda_eop
BitField Type: W1C
BitField Desc: EOP             : 0: clear, 1: set
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_cla2pda_stk_cla2pda_eop_Mask                                                           cBit2
#define cAf6_upen_cla2pda_stk_cla2pda_eop_Shift                                                              2

/*--------------------------------------
BitField Name: cla2pda_sop
BitField Type: W1C
BitField Desc: SOP             : 0: clear, 1: set
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_cla2pda_stk_cla2pda_sop_Mask                                                           cBit1
#define cAf6_upen_cla2pda_stk_cla2pda_sop_Shift                                                              1

/*--------------------------------------
BitField Name: cla2pda_vld
BitField Type: W1C
BitField Desc: VALID           : 0: clear, 1: set
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_cla2pda_stk_cla2pda_vld_Mask                                                           cBit0
#define cAf6_upen_cla2pda_stk_cla2pda_vld_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : parser sticky 0
Reg Addr   : 0x00_0104
Reg Formula:
    Where  :
Reg Desc   :
sticky 0  of parser

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_parser_stk0_Base                                                                 0x000104

/*--------------------------------------
BitField Name: par0_free
BitField Type: W1C
BitField Desc: PARSER_0 FREE BLK : 1: clear , 0: set
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_parser_stk0_par0_free_Mask                                                             cBit5
#define cAf6_upen_parser_stk0_par0_free_Shift                                                                5

/*--------------------------------------
BitField Name: par0_eoseg
BitField Type: W1C
BitField Desc: PARSER_0 EOSEG    : 1: clear , 0: set
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_parser_stk0_par0_eoseg_Mask                                                            cBit4
#define cAf6_upen_parser_stk0_par0_eoseg_Shift                                                               4

/*--------------------------------------
BitField Name: par0_blkemp
BitField Type: W1C
BitField Desc: PARSER_0 BLK EMPTY: 1: clear , 0: set
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_parser_stk0_par0_blkemp_Mask                                                           cBit3
#define cAf6_upen_parser_stk0_par0_blkemp_Shift                                                              3

/*--------------------------------------
BitField Name: par0_errmru
BitField Type: W1C
BitField Desc: PARSER_0 MRU ERROR: 1: clear , 0: set
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_parser_stk0_par0_errmru_Mask                                                           cBit2
#define cAf6_upen_parser_stk0_par0_errmru_Shift                                                              2

/*--------------------------------------
BitField Name: par0_err
BitField Type: W1C
BitField Desc: PARSER_0 PKT SHORT: 1: clear , 0: set
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_parser_stk0_par0_err_Mask                                                              cBit1
#define cAf6_upen_parser_stk0_par0_err_Shift                                                                 1

/*--------------------------------------
BitField Name: par0_done
BitField Type: W1C
BitField Desc: PARSER_0 DONE:      1: clear , 0: set
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_parser_stk0_par0_done_Mask                                                             cBit0
#define cAf6_upen_parser_stk0_par0_done_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : parser sticky 1
Reg Addr   : 0x00_0105
Reg Formula:
    Where  :
Reg Desc   :
sticky 1  of parser

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_parser_stk1_Base                                                                 0x000105

/*--------------------------------------
BitField Name: par0_udpprt
BitField Type: W1C
BitField Desc: PARSER_0 UDP PRT  : 0: clear, 1: set
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_upen_parser_stk1_par0_udpprt_Mask                                                          cBit11
#define cAf6_upen_parser_stk1_par0_udpprt_Shift                                                             11

/*--------------------------------------
BitField Name: par0_ipda
BitField Type: W1C
BitField Desc: PARSER_0 IP DA    : 0: clear, 1: set
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_upen_parser_stk1_par0_ipda_Mask                                                            cBit10
#define cAf6_upen_parser_stk1_par0_ipda_Shift                                                               10

/*--------------------------------------
BitField Name: par0_ipsa
BitField Type: W1C
BitField Desc: PARSER_0 IP SA    : 0: clear, 1: set
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_upen_parser_stk1_par0_ipsa_Mask                                                             cBit9
#define cAf6_upen_parser_stk1_par0_ipsa_Shift                                                                9

/*--------------------------------------
BitField Name: par0_udpptl
BitField Type: W1C
BitField Desc: PARSER_0 UDP PTL  : 0: clear, 1: set
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_upen_parser_stk1_par0_udpptl_Mask                                                           cBit8
#define cAf6_upen_parser_stk1_par0_udpptl_Shift                                                              8

/*--------------------------------------
BitField Name: par0_ipver
BitField Type: W1C
BitField Desc: PARSER_0 IPVER  : 0: clear, 1: set
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_upen_parser_stk1_par0_ipver_Mask                                                            cBit7
#define cAf6_upen_parser_stk1_par0_ipver_Shift                                                               7

/*--------------------------------------
BitField Name: par0_mac
BitField Type: W1C
BitField Desc: PARSER_0 MAC    : 0: clear, 1: set
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_upen_parser_stk1_par0_mac_Mask                                                              cBit6
#define cAf6_upen_parser_stk1_par0_mac_Shift                                                                 6

/*--------------------------------------
BitField Name: par0_mefecid
BitField Type: W1C
BitField Desc: PARSER_0 MEFECID: 0: clear, 1: set
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_parser_stk1_par0_mefecid_Mask                                                          cBit5
#define cAf6_upen_parser_stk1_par0_mefecid_Shift                                                             5

/*--------------------------------------
BitField Name: par0_pwlabel
BitField Type: W1C
BitField Desc: PARSER_0 PWLABEL: 0: clear, 1: set
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_parser_stk1_par0_pwlabel_Mask                                                          cBit4
#define cAf6_upen_parser_stk1_par0_pwlabel_Shift                                                             4

/*--------------------------------------
BitField Name: par0_ethtyp
BitField Type: W1C
BitField Desc: PARSER_0 ETHTYPE: 0: clear, 1: set
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_parser_stk1_par0_ethtyp_Mask                                                           cBit3
#define cAf6_upen_parser_stk1_par0_ethtyp_Shift                                                              3

/*--------------------------------------
BitField Name: par0_sndvlan
BitField Type: W1C
BitField Desc: PARSER_0 SNDVLAN: 0: clear, 1: set
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_parser_stk1_par0_sndvlan_Mask                                                          cBit2
#define cAf6_upen_parser_stk1_par0_sndvlan_Shift                                                             2

/*--------------------------------------
BitField Name: par0_fstvlan
BitField Type: W1C
BitField Desc: PARSER_0 FSTVLAN: 0: clear, 1: set
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_parser_stk1_par0_fstvlan_Mask                                                          cBit1
#define cAf6_upen_parser_stk1_par0_fstvlan_Shift                                                             1

/*--------------------------------------
BitField Name: par0_ptch
BitField Type: W1C
BitField Desc: PARSER_0 PTCH   : 0: clear, 1: set
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_parser_stk1_par0_ptch_Mask                                                             cBit0
#define cAf6_upen_parser_stk1_par0_ptch_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : lookup sticky
Reg Addr   : 0x00_0106
Reg Formula:
    Where  :
Reg Desc   :
sticky of lookup

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_lkup_stk_Base                                                                    0x000106

/*--------------------------------------
BitField Name: porttab_win
BitField Type: W1C
BitField Desc: PORT    TABLE WIN: 0: clear 1: set
BitField Bits: [22:22]
--------------------------------------*/
#define cAf6_upen_lkup_stk_porttab_win_Mask                                                             cBit22
#define cAf6_upen_lkup_stk_porttab_win_Shift                                                                22

/*--------------------------------------
BitField Name: fvlntab_win_xfi0
BitField Type: W1C
BitField Desc: FSTVLAN TABLE WIN: 0: clear 1: set
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_upen_lkup_stk_fvlntab_win_xfi0_Mask                                                        cBit20
#define cAf6_upen_lkup_stk_fvlntab_win_xfi0_Shift                                                           20

/*--------------------------------------
BitField Name: svlntab_win_xfi0
BitField Type: W1C
BitField Desc: SNDVLAN TABLE WIN: 0: clear 1: set
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_upen_lkup_stk_svlntab_win_xfi0_Mask                                                        cBit19
#define cAf6_upen_lkup_stk_svlntab_win_xfi0_Shift                                                           19

/*--------------------------------------
BitField Name: pwhash_win
BitField Type: W1C
BitField Desc: PWHASH  TABLE WIN: 0: clear 1: set
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_upen_lkup_stk_pwhash_win_Mask                                                              cBit18
#define cAf6_upen_lkup_stk_pwhash_win_Shift                                                                 18

/*--------------------------------------
BitField Name: etypcam_win
BitField Type: W1C
BitField Desc: ETHTYPE TABLE WIN: 0: clear 1: set
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_upen_lkup_stk_etypcam_win_Mask                                                             cBit17
#define cAf6_upen_lkup_stk_etypcam_win_Shift                                                                17

/*--------------------------------------
BitField Name: rulechk_win
BitField Type: W1C
BitField Desc: RULECHK TABLE WIN: 0: clear 1: set
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_upen_lkup_stk_rulechk_win_Mask                                                             cBit16
#define cAf6_upen_lkup_stk_rulechk_win_Shift                                                                16

/*--------------------------------------
BitField Name: lkup0_ptpind
BitField Type: W1C
BitField Desc: LOOKUP_0 PTP IND   : 0: clear 1: set
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_upen_lkup_stk_lkup0_ptpind_Mask                                                             cBit7
#define cAf6_upen_lkup_stk_lkup0_ptpind_Shift                                                                7

/*--------------------------------------
BitField Name: lkup0_udpind
BitField Type: W1C
BitField Desc: LOOKUP_0 UDP IND   : 0: clear 1: set
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_upen_lkup_stk_lkup0_udpind_Mask                                                             cBit6
#define cAf6_upen_lkup_stk_lkup0_udpind_Shift                                                                6

/*--------------------------------------
BitField Name: lkup0_eoseg
BitField Type: W1C
BitField Desc: LOOKUP_0 EOSEG     : 0: clear 1: set
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_lkup_stk_lkup0_eoseg_Mask                                                              cBit5
#define cAf6_upen_lkup_stk_lkup0_eoseg_Shift                                                                 5

/*--------------------------------------
BitField Name: lkup0_blkemp
BitField Type: W1C
BitField Desc: LOOKUP_0 BLK EMPTY : 0: clear 1: set
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_lkup_stk_lkup0_blkemp_Mask                                                             cBit4
#define cAf6_upen_lkup_stk_lkup0_blkemp_Shift                                                                4

/*--------------------------------------
BitField Name: lkup0_errmru
BitField Type: W1C
BitField Desc: LOOKUP_0 MRU ERR: 0: clear 1: set
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_lkup_stk_lkup0_errmru_Mask                                                             cBit3
#define cAf6_upen_lkup_stk_lkup0_errmru_Shift                                                                3

/*--------------------------------------
BitField Name: lkup0_fail
BitField Type: W1C
BitField Desc: LOOKUP_0 FAIL   : 0: clear 1: set
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_lkup_stk_lkup0_fail_Mask                                                               cBit2
#define cAf6_upen_lkup_stk_lkup0_fail_Shift                                                                  2

/*--------------------------------------
BitField Name: lkup0_stk0
BitField Type: W1C
BitField Desc: LOOKUP_0 sticky 0
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_lkup_stk_lkup0_stk0_Mask                                                               cBit1
#define cAf6_upen_lkup_stk_lkup0_stk0_Shift                                                                  1

/*--------------------------------------
BitField Name: lkup0_done
BitField Type: W1C
BitField Desc: LOOKUP_0 DONE   : 0: clear 1: set
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_lkup_stk_lkup0_done_Mask                                                               cBit0
#define cAf6_upen_lkup_stk_lkup0_done_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : datashift sticky0
Reg Addr   : 0x00_0107
Reg Formula:
    Where  :
Reg Desc   :
sticky of datashift XFI0

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_datashidt_stk0_Base                                                              0x000107

/*--------------------------------------
BitField Name: datsh0_ssercpu
BitField Type: W1C
BitField Desc: DATSHF_0 SSER CPU : 0: clear 1: set
BitField Bits: [43:43]
--------------------------------------*/
#define cAf6_upen_datashidt_stk0_datsh0_ssercpu_Mask                                                    cBit11
#define cAf6_upen_datashidt_stk0_datsh0_ssercpu_Shift                                                       11

/*--------------------------------------
BitField Name: datsh0_sseroam
BitField Type: W1C
BitField Desc: DATSHF_0 SSER OAM : 0: clear 1: set
BitField Bits: [42:42]
--------------------------------------*/
#define cAf6_upen_datashidt_stk0_datsh0_sseroam_Mask                                                    cBit10
#define cAf6_upen_datashidt_stk0_datsh0_sseroam_Shift                                                       10

/*--------------------------------------
BitField Name: datsh0_sserdata
BitField Type: W1C
BitField Desc: DATSHF_0 SSER DATA: 0: clear 1: set
BitField Bits: [41:41]
--------------------------------------*/
#define cAf6_upen_datashidt_stk0_datsh0_sserdata_Mask                                                    cBit9
#define cAf6_upen_datashidt_stk0_datsh0_sserdata_Shift                                                       9

/*--------------------------------------
BitField Name: datsh0_sserpw
BitField Type: W1C
BitField Desc: DATSHF_0 SSER PW  : 0: clear 1: set
BitField Bits: [40:40]
--------------------------------------*/
#define cAf6_upen_datashidt_stk0_datsh0_sserpw_Mask                                                      cBit8
#define cAf6_upen_datashidt_stk0_datsh0_sserpw_Shift                                                         8

/*--------------------------------------
BitField Name: datsh0_serptp
BitField Type: W1C
BitField Desc: DATSHF_0 SER PTP   : 0: clear 1: set
BitField Bits: [37:37]
--------------------------------------*/
#define cAf6_upen_datashidt_stk0_datsh0_serptp_Mask                                                      cBit5
#define cAf6_upen_datashidt_stk0_datsh0_serptp_Shift                                                         5

/*--------------------------------------
BitField Name: datsh0_serepass
BitField Type: W1C
BitField Desc: DATSHF_0 SER EPASS : 0: clear 1: set
BitField Bits: [36:36]
--------------------------------------*/
#define cAf6_upen_datashidt_stk0_datsh0_serepass_Mask                                                    cBit4
#define cAf6_upen_datashidt_stk0_datsh0_serepass_Shift                                                       4

/*--------------------------------------
BitField Name: datsh0_serdcc
BitField Type: W1C
BitField Desc: DATSHF_0 SER DCC   : 0: clear 1: set
BitField Bits: [35:35]
--------------------------------------*/
#define cAf6_upen_datashidt_stk0_datsh0_serdcc_Mask                                                      cBit3
#define cAf6_upen_datashidt_stk0_datsh0_serdcc_Shift                                                         3

/*--------------------------------------
BitField Name: datsh0_serimsg
BitField Type: W1C
BitField Desc: DATSHF_0 SER IMSG  : 0: clear 1: set
BitField Bits: [34:34]
--------------------------------------*/
#define cAf6_upen_datashidt_stk0_datsh0_serimsg_Mask                                                     cBit2
#define cAf6_upen_datashidt_stk0_datsh0_serimsg_Shift                                                        2

/*--------------------------------------
BitField Name: datsh0_sercem
BitField Type: W1C
BitField Desc: DATSHF_0 SER CEM   : 0: clear 1: set
BitField Bits: [33:33]
--------------------------------------*/
#define cAf6_upen_datashidt_stk0_datsh0_sercem_Mask                                                      cBit1
#define cAf6_upen_datashidt_stk0_datsh0_sercem_Shift                                                         1

/*--------------------------------------
BitField Name: datsh0_serdrop
BitField Type: W1C
BitField Desc: DATSHF_0 SER DROP  : 0: clear 1: set
BitField Bits: [32:32]
--------------------------------------*/
#define cAf6_upen_datashidt_stk0_datsh0_serdrop_Mask                                                  cBit0
#define cAf6_upen_datashidt_stk0_datsh0_serdrop_Shift                                                        0

/*--------------------------------------
BitField Name: datsh0_mef
BitField Type: W1C
BitField Desc: DATSHF_0 MEF      : 0: clear 1: set
BitField Bits: [29:29]
--------------------------------------*/
#define cAf6_upen_datashidt_stk0_datsh0_mef_Mask                                                        cBit29
#define cAf6_upen_datashidt_stk0_datsh0_mef_Shift                                                           29

/*--------------------------------------
BitField Name: datsh0_oam
BitField Type: W1C
BitField Desc: DATSHF_0 OAM      : 0: clear 1: set
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_upen_datashidt_stk0_datsh0_oam_Mask                                                        cBit28
#define cAf6_upen_datashidt_stk0_datsh0_oam_Shift                                                           28

/*--------------------------------------
BitField Name: datsh0_mmpls
BitField Type: W1C
BitField Desc: DATSHF_0 MPLS MUL : 0: clear 1: set
BitField Bits: [27:27]
--------------------------------------*/
#define cAf6_upen_datashidt_stk0_datsh0_mmpls_Mask                                                      cBit27
#define cAf6_upen_datashidt_stk0_datsh0_mmpls_Shift                                                         27

/*--------------------------------------
BitField Name: datsh0_umpls
BitField Type: W1C
BitField Desc: DATSHF_0 MPLS UNI : 0: clear 1: set
BitField Bits: [26:26]
--------------------------------------*/
#define cAf6_upen_datashidt_stk0_datsh0_umpls_Mask                                                      cBit26
#define cAf6_upen_datashidt_stk0_datsh0_umpls_Shift                                                         26

/*--------------------------------------
BitField Name: datsh0_ipv6
BitField Type: W1C
BitField Desc: DATSHF_0 IPv6     : 0: clear 1: set
BitField Bits: [25:25]
--------------------------------------*/
#define cAf6_upen_datashidt_stk0_datsh0_ipv6_Mask                                                       cBit25
#define cAf6_upen_datashidt_stk0_datsh0_ipv6_Shift                                                          25

/*--------------------------------------
BitField Name: datsh0_ipv4
BitField Type: W1C
BitField Desc: DATSHF_0 IPv4     : 0: clear 1: set
BitField Bits: [24:24]
--------------------------------------*/
#define cAf6_upen_datashidt_stk0_datsh0_ipv4_Mask                                                       cBit24
#define cAf6_upen_datashidt_stk0_datsh0_ipv4_Shift                                                          24

/*--------------------------------------
BitField Name: datsh0_4label
BitField Type: W1C
BitField Desc: DATSHF_0 4LABEL : 0: clear 1: set
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_upen_datashidt_stk0_datsh0_4label_Mask                                                     cBit20
#define cAf6_upen_datashidt_stk0_datsh0_4label_Shift                                                        20

/*--------------------------------------
BitField Name: datsh0_3label
BitField Type: W1C
BitField Desc: DATSHF_0 3LABEL : 0: clear 1: set
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_upen_datashidt_stk0_datsh0_3label_Mask                                                     cBit19
#define cAf6_upen_datashidt_stk0_datsh0_3label_Shift                                                        19

/*--------------------------------------
BitField Name: datsh0_2label
BitField Type: W1C
BitField Desc: DATSHF_0 2LABEL : 0: clear 1: set
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_upen_datashidt_stk0_datsh0_2label_Mask                                                     cBit18
#define cAf6_upen_datashidt_stk0_datsh0_2label_Shift                                                        18

/*--------------------------------------
BitField Name: datsh0_1label
BitField Type: W1C
BitField Desc: DATSHF_0 1LABEL : 0: clear 1: set
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_upen_datashidt_stk0_datsh0_1label_Mask                                                     cBit17
#define cAf6_upen_datashidt_stk0_datsh0_1label_Shift                                                        17

/*--------------------------------------
BitField Name: datsh0_0label
BitField Type: W1C
BitField Desc: DATSHF_0 0LABEL : 0: clear 1: set
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_upen_datashidt_stk0_datsh0_0label_Mask                                                     cBit16
#define cAf6_upen_datashidt_stk0_datsh0_0label_Shift                                                        16

/*--------------------------------------
BitField Name: datsh0_2vlan
BitField Type: W1C
BitField Desc: DATSHF_0 2VLAN : 0: clear 1: set
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_upen_datashidt_stk0_datsh0_2vlan_Mask                                                      cBit14
#define cAf6_upen_datashidt_stk0_datsh0_2vlan_Shift                                                         14

/*--------------------------------------
BitField Name: datsh0_1vlan
BitField Type: W1C
BitField Desc: DATSHF_0 1VLAN : 0: clear 1: set
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_upen_datashidt_stk0_datsh0_1vlan_Mask                                                      cBit13
#define cAf6_upen_datashidt_stk0_datsh0_1vlan_Shift                                                         13

/*--------------------------------------
BitField Name: datsh0_0vlan
BitField Type: W1C
BitField Desc: DATSHF_0 0VLAN : 0: clear 1: set
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_upen_datashidt_stk0_datsh0_0vlan_Mask                                                      cBit12
#define cAf6_upen_datashidt_stk0_datsh0_0vlan_Shift                                                         12

/*--------------------------------------
BitField Name: datsh0_ptpind
BitField Type: W1C
BitField Desc: DATSHF_0 PTP  IND : 0: clear 1: set
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_upen_datashidt_stk0_datsh0_ptpind_Mask                                                     cBit10
#define cAf6_upen_datashidt_stk0_datsh0_ptpind_Shift                                                        10

/*--------------------------------------
BitField Name: datsh0_udpind
BitField Type: W1C
BitField Desc: DATSHF_0 UDP  IND : 0: clear 1: set
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_upen_datashidt_stk0_datsh0_udpind_Mask                                                      cBit9
#define cAf6_upen_datashidt_stk0_datsh0_udpind_Shift                                                         9

/*--------------------------------------
BitField Name: datsh0_lkwin
BitField Type: W1C
BitField Desc: DATSHF_0 LKUP WIN : 0: clear 1: set
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_upen_datashidt_stk0_datsh0_lkwin_Mask                                                       cBit8
#define cAf6_upen_datashidt_stk0_datsh0_lkwin_Shift                                                          8

/*--------------------------------------
BitField Name: datsh0_err4
BitField Type: W1C
BitField Desc: DATSHF_0 MRU ERROR: 0: clear 1: set
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_upen_datashidt_stk0_datsh0_err4_Mask                                                        cBit7
#define cAf6_upen_datashidt_stk0_datsh0_err4_Shift                                                           7

/*--------------------------------------
BitField Name: datsh0_err3
BitField Type: W1C
BitField Desc: DATSHF_0 BLK EMPTY: 0: clear 1: set
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_upen_datashidt_stk0_datsh0_err3_Mask                                                        cBit6
#define cAf6_upen_datashidt_stk0_datsh0_err3_Shift                                                           6

/*--------------------------------------
BitField Name: datsh0_err2
BitField Type: W1C
BitField Desc: DATSHF_0 LKUP FAIL: 0: clear 1: set
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_datashidt_stk0_datsh0_err2_Mask                                                        cBit5
#define cAf6_upen_datashidt_stk0_datsh0_err2_Shift                                                           5

/*--------------------------------------
BitField Name: datsh0_err0
BitField Type: W1C
BitField Desc: DATSHF_0 ETH ERROR: 0: clear 1: set
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_datashidt_stk0_datsh0_err0_Mask                                                        cBit3
#define cAf6_upen_datashidt_stk0_datsh0_err0_Shift                                                           3

/*--------------------------------------
BitField Name: datsh0_eop
BitField Type: W1C
BitField Desc: DATSHF_0 EOP      : 0: clear 1: set
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_datashidt_stk0_datsh0_eop_Mask                                                         cBit2
#define cAf6_upen_datashidt_stk0_datsh0_eop_Shift                                                            2

/*--------------------------------------
BitField Name: datsh0_sop
BitField Type: W1C
BitField Desc: DATSHF_0 SOP      : 0: clear 1: set
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_datashidt_stk0_datsh0_sop_Mask                                                         cBit1
#define cAf6_upen_datashidt_stk0_datsh0_sop_Shift                                                            1

/*--------------------------------------
BitField Name: datsh0_vld
BitField Type: W1C
BitField Desc: DATSHF_0 VALID    : 0: clear 1: set
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_datashidt_stk0_datsh0_vld_Mask                                                         cBit0
#define cAf6_upen_datashidt_stk0_datsh0_vld_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : egrhdr sticky
Reg Addr   : 0x00_0108
Reg Formula:
    Where  :
Reg Desc   :
sticky of egress header

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_egrhdr_stk_Base                                                                  0x000108

/*--------------------------------------
BitField Name: egrlk_err0
BitField Type: W1C
BitField Desc: EGRLK ERROR: 0: clear 1:set
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_egrhdr_stk_egrlk_err0_Mask                                                             cBit3
#define cAf6_upen_egrhdr_stk_egrlk_err0_Shift                                                                3

/*--------------------------------------
BitField Name: egrlk_eop0
BitField Type: W1C
BitField Desc: EGRLK EOP  : 0: clear 1:set
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_egrhdr_stk_egrlk_eop0_Mask                                                             cBit2
#define cAf6_upen_egrhdr_stk_egrlk_eop0_Shift                                                                2

/*--------------------------------------
BitField Name: egrlk_sop0
BitField Type: W1C
BitField Desc: EGRLK SOP  : 0: clear 1:set
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_egrhdr_stk_egrlk_sop0_Mask                                                             cBit1
#define cAf6_upen_egrhdr_stk_egrlk_sop0_Shift                                                                1

/*--------------------------------------
BitField Name: egrlk_vld0
BitField Type: W1C
BitField Desc: EGRLK VALID: 0: clear 1:set
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_egrhdr_stk_egrlk_vld0_Mask                                                             cBit0
#define cAf6_upen_egrhdr_stk_egrlk_vld0_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : shift64 sticky
Reg Addr   : 0x00_0109
Reg Formula:
    Where  :
Reg Desc   :
sticky of shift64 module

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_shift64_stk_Base                                                                 0x000109

/*--------------------------------------
BitField Name: shift64_stk
BitField Type: W1C
BitField Desc: shift64 sticky
BitField Bits: [31:01]
--------------------------------------*/
#define cAf6_upen_shift64_stk_shift64_stk_Mask                                                        cBit31_1
#define cAf6_upen_shift64_stk_shift64_stk_Shift                                                              1

/*--------------------------------------
BitField Name: shift64_fffull
BitField Type: W1C
BitField Desc: SHIFT64 FIFO FULL : 0: clear 1: set
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_shift64_stk_shift64_fffull_Mask                                                        cBit0
#define cAf6_upen_shift64_stk_shift64_fffull_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : allc sticky
Reg Addr   : 0x00_010A
Reg Formula:
    Where  :
Reg Desc   :
sticky of egress header

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_allc_stk_Base                                                                    0x00010A

/*--------------------------------------
BitField Name: ffge11_full
BitField Type: W1C
BitField Desc: FIFO GE11 FULL: 0: clear 1:set
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_upen_allc_stk_ffge11_full_Mask                                                             cBit19
#define cAf6_upen_allc_stk_ffge11_full_Shift                                                                19

/*--------------------------------------
BitField Name: ffge10_full
BitField Type: W1C
BitField Desc: FIFO GE10 FULL: 0: clear 1:set
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_upen_allc_stk_ffge10_full_Mask                                                             cBit18
#define cAf6_upen_allc_stk_ffge10_full_Shift                                                                18

/*--------------------------------------
BitField Name: ffge9_full
BitField Type: W1C
BitField Desc: FIFO GE9  FULL: 0: clear 1:set
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_upen_allc_stk_ffge9_full_Mask                                                              cBit17
#define cAf6_upen_allc_stk_ffge9_full_Shift                                                                 17

/*--------------------------------------
BitField Name: ffge8_full
BitField Type: W1C
BitField Desc: FIFO GE8  FULL: 0: clear 1:set
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_upen_allc_stk_ffge8_full_Mask                                                              cBit16
#define cAf6_upen_allc_stk_ffge8_full_Shift                                                                 16

/*--------------------------------------
BitField Name: ffge7_full
BitField Type: W1C
BitField Desc: FIFO GE7  FULL: 0: clear 1:set
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_upen_allc_stk_ffge7_full_Mask                                                              cBit15
#define cAf6_upen_allc_stk_ffge7_full_Shift                                                                 15

/*--------------------------------------
BitField Name: ffge6_full
BitField Type: W1C
BitField Desc: FIFO GE6  FULL: 0: clear 1:set
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_upen_allc_stk_ffge6_full_Mask                                                              cBit14
#define cAf6_upen_allc_stk_ffge6_full_Shift                                                                 14

/*--------------------------------------
BitField Name: ffge5_full
BitField Type: W1C
BitField Desc: FIFO GE5  FULL: 0: clear 1:set
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_upen_allc_stk_ffge5_full_Mask                                                              cBit13
#define cAf6_upen_allc_stk_ffge5_full_Shift                                                                 13

/*--------------------------------------
BitField Name: ffge4_full
BitField Type: W1C
BitField Desc: FIFO GE4  FULL: 0: clear 1:set
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_upen_allc_stk_ffge4_full_Mask                                                              cBit12
#define cAf6_upen_allc_stk_ffge4_full_Shift                                                                 12

/*--------------------------------------
BitField Name: ffxfi_full
BitField Type: W1C
BitField Desc: FIFO XFI FULL: 0: clear 1:set
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_upen_allc_stk_ffxfi_full_Mask                                                               cBit8
#define cAf6_upen_allc_stk_ffxfi_full_Shift                                                                  8

/*--------------------------------------
BitField Name: blkreq
BitField Type: W1C
BitField Desc: BLK REQUEST: 0: clear 1:set
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_allc_stk_blkreq_Mask                                                                   cBit4
#define cAf6_upen_allc_stk_blkreq_Shift                                                                      4

/*--------------------------------------
BitField Name: buffre
BitField Type: W1C
BitField Desc: BUFFER FREE: 0: clear 1:set
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_allc_stk_buffre_Mask                                                                   cBit3
#define cAf6_upen_allc_stk_buffre_Shift                                                                      3

/*--------------------------------------
BitField Name: parfre
BitField Type: W1C
BitField Desc: PARSER FREE: 0: clear 1:set
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_allc_stk_parfre_Mask                                                                   cBit2
#define cAf6_upen_allc_stk_parfre_Shift                                                                      2

/*--------------------------------------
BitField Name: blkemp
BitField Type: W1C
BitField Desc: BLK EMPTY  : 0: clear 1:set
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_allc_stk_blkemp_Mask                                                                   cBit1
#define cAf6_upen_allc_stk_blkemp_Shift                                                                      1

/*--------------------------------------
BitField Name: samefree
BitField Type: W1C
BitField Desc: SAME FREE  : 0: clear 1:set
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_allc_stk_samefree_Mask                                                                 cBit0
#define cAf6_upen_allc_stk_samefree_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : allc_blkfre_cnt
Reg Addr   : 0x00_0200
Reg Formula:
    Where  :
Reg Desc   :
allc block free counter

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_allc_blkfre_cnt_Base                                                             0x000200

/*--------------------------------------
BitField Name: allc_blkfre_cnt
BitField Type: RO
BitField Desc: block free counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_allc_blkfre_cnt_allc_blkfre_cnt_Mask                                                cBit31_0
#define cAf6_upen_allc_blkfre_cnt_allc_blkfre_cnt_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : allc_blkiss_cnt
Reg Addr   : 0x00_0201
Reg Formula:
    Where  :
Reg Desc   :
allc block issue counter

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_allc_blkiss_cnt_Base                                                             0x000201

/*--------------------------------------
BitField Name: allc_blkiss_cnt
BitField Type: RO
BitField Desc: block issue counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_allc_blkiss_cnt_allc_blkiss_cnt_Mask                                                cBit31_0
#define cAf6_upen_allc_blkiss_cnt_allc_blkiss_cnt_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : PW Control Table
Reg Addr   : 0x00_6000-0x00_7FFF
Reg Formula: 0x00_6000 + $pwid
    Where  :
           + $pwid(0-8191)
Reg Desc   :
This register configures pw control table
HDL_PATH     : inscorepi.label0.inspwctl.membuf.ram.ram[$pwid]

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_pwctl_Base                                                                       0x006000

/*--------------------------------------
BitField Name: lenchk_en
BitField Type: RW
BitField Desc: Length check enable 0: enable 1: disable
BitField Bits: [56:56]
--------------------------------------*/
#define cAf6_upen_pwctl_lenchk_en_Mask                                                                  cBit24
#define cAf6_upen_pwctl_lenchk_en_Shift                                                                     24

/*--------------------------------------
BitField Name: pwlen
BitField Type: RW
BitField Desc: length of packet to check malform
BitField Bits: [55:42]
--------------------------------------*/
#define cAf6_upen_pwctl_pwlen_Mask                                                                   cBit23_10
#define cAf6_upen_pwctl_pwlen_Shift                                                                         10

/*--------------------------------------
BitField Name: ssrcval
BitField Type: RW
BitField Desc: this value is used to compare with SSRC value in RTP header
BitField Bits: [41:10]
--------------------------------------*/
#define cAf6_upen_pwctl_ssrcval_01_Mask                                                              cBit31_10
#define cAf6_upen_pwctl_ssrcval_01_Shift                                                                    10
#define cAf6_upen_pwctl_ssrcval_02_Mask                                                                cBit9_0
#define cAf6_upen_pwctl_ssrcval_02_Shift                                                                     0

/*--------------------------------------
BitField Name: ptval
BitField Type: RW
BitField Desc: this value is used to compare with PT value in RTP header
BitField Bits: [09:03]
--------------------------------------*/
#define cAf6_upen_pwctl_ptval_Mask                                                                     cBit9_3
#define cAf6_upen_pwctl_ptval_Shift                                                                          3

/*--------------------------------------
BitField Name: sup_en
BitField Type: RW
BitField Desc: Suppress Enable 0: disable 1: enable
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_pwctl_sup_en_Mask                                                                      cBit2
#define cAf6_upen_pwctl_sup_en_Shift                                                                         2

/*--------------------------------------
BitField Name: ssrcchk_en
BitField Type: RW
BitField Desc: Enable SSRC field of RTP header checking 0: disable 1: enable
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_pwctl_ssrcchk_en_Mask                                                                  cBit1
#define cAf6_upen_pwctl_ssrcchk_en_Shift                                                                     1

/*--------------------------------------
BitField Name: ptchk_en
BitField Type: RW
BitField Desc: Enable PT field of RTP header checking 0: disable 1: enable
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_pwctl_ptchk_en_Mask                                                                    cBit0
#define cAf6_upen_pwctl_ptchk_en_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : MAC DA check CAM
Reg Addr   : 0x00_5220-0x00_5223
Reg Formula: 0x00_5220 + $id
    Where  :
           + $id(0-3)
Reg Desc   :
This register configures for MAC DA checking of CEM PW
id(0-3) : support 4 MAC DA  for DA checking for CEM PW

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dachkcam_Base                                                                    0x005220

/*--------------------------------------
BitField Name: dachk_val0
BitField Type: RW
BitField Desc: MAC DA value #XFI0 id= 0: mac da 0 id= 1: mac da 1 id= 2: mac da
2 id= 3: mac da 3
BitField Bits: [48:01]
--------------------------------------*/
#define cAf6_upen_dachkcam_dachk_val0_01_Mask                                                         cBit31_1
#define cAf6_upen_dachkcam_dachk_val0_01_Shift                                                               1
#define cAf6_upen_dachkcam_dachk_val0_02_Mask                                                         cBit16_0
#define cAf6_upen_dachkcam_dachk_val0_02_Shift                                                               0

/*--------------------------------------
BitField Name: dachk_vld0
BitField Type: RW
BitField Desc: 0: lookup fail #XFI0
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_dachkcam_dachk_vld0_Mask                                                               cBit0
#define cAf6_upen_dachkcam_dachk_vld0_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Global Error Enable
Reg Addr   : 0x00_0018
Reg Formula:
    Where  :
Reg Desc   :
This register configure Error Enable

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_errset_en_Base                                                                   0x000018

/*--------------------------------------
BitField Name: malerr_en0
BitField Type: RW
BitField Desc: Malformed packet error 0: disable 1: enable
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_upen_errset_en_malerr_en0_Mask                                                              cBit7
#define cAf6_upen_errset_en_malerr_en0_Shift                                                                 7

/*--------------------------------------
BitField Name: pterr_en0
BitField Type: RW
BitField Desc: PT Mismatch error      0: disable 1: enable
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_upen_errset_en_pterr_en0_Mask                                                               cBit6
#define cAf6_upen_errset_en_pterr_en0_Shift                                                                  6

/*--------------------------------------
BitField Name: ssrcerr_en0
BitField Type: RW
BitField Desc: SSRC Mismatch error    0: disable 1: enable
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_errset_en_ssrcerr_en0_Mask                                                             cBit5
#define cAf6_upen_errset_en_ssrcerr_en0_Shift                                                                5

/*--------------------------------------
BitField Name: mruerr_en0
BitField Type: RW
BitField Desc: MRU error              0: disable 1: enable
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_errset_en_mruerr_en0_Mask                                                              cBit4
#define cAf6_upen_errset_en_mruerr_en0_Shift                                                                 4

/*--------------------------------------
BitField Name: buferr_en0
BitField Type: RW
BitField Desc: Buffer full error      0: disable 1: enable
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_errset_en_buferr_en0_Mask                                                              cBit3
#define cAf6_upen_errset_en_buferr_en0_Shift                                                                 3

/*--------------------------------------
BitField Name: lkuerr_en0
BitField Type: RW
BitField Desc: Lookup error           0: disable 1: enable
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_errset_en_lkuerr_en0_Mask                                                              cBit2
#define cAf6_upen_errset_en_lkuerr_en0_Shift                                                                 2

/*--------------------------------------
BitField Name: dachkerr_en0
BitField Type: RW
BitField Desc: DA check error(CEMPW)  0: disable 1: enable
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_errset_en_dachkerr_en0_Mask                                                            cBit1
#define cAf6_upen_errset_en_dachkerr_en0_Shift                                                               1

/*--------------------------------------
BitField Name: etherr_en0
BitField Type: RW
BitField Desc: ETH Core error         0: disable 1: enable
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_errset_en_etherr_en0_Mask                                                              cBit0
#define cAf6_upen_errset_en_etherr_en0_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Protocol ID Encap table
Reg Addr   : 0x00_5240-0x525F
Reg Formula: 0x00_5240 + $entryid
    Where  :
           + $entryid(0-31) ethtype entry ID
Reg Desc   :
This register configures Protocol ID Encap
HDL_PATH     : inscorepi.label0.insptlenc.membuf.ram.ram[$entryid]

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ptlenc_Base                                                                      0x005240

/*--------------------------------------
BitField Name: chdlc_pid
BitField Type: RW
BitField Desc: cHDLC protocol ID
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_upen_ptlenc_chdlc_pid_Mask                                                              cBit31_16
#define cAf6_upen_ptlenc_chdlc_pid_Shift                                                                    16

/*--------------------------------------
BitField Name: ppp_pid
BitField Type: RW
BitField Desc: PPP   protocol ID
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_ptlenc_ppp_pid_Mask                                                                 cBit15_0
#define cAf6_upen_ptlenc_ppp_pid_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : CLA Ready
Reg Addr   : 0x00_001a
Reg Formula:
    Where  :
Reg Desc   :
This register is used to enable remove dcc padding

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rmv_dccpad_en_Base                                                               0x00001a

/*--------------------------------------
BitField Name: rmv_dccpad_en
BitField Type: RW
BitField Desc: remove DCC padding enable 0: disable 1: enable
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_rmv_dccpad_en_rmv_dccpad_en_Mask                                                       cBit0
#define cAf6_upen_rmv_dccpad_en_rmv_dccpad_en_Shift                                                          0

/*------------------------------------------------------------------------------
Reg Name   : CLA discard Counters #XFI0
Reg Addr   : 0x00_0204-0x00_0205
Reg Formula: 
    Where  : 
Reg Desc   : 
0x00_0204 : RO , 0x00_0205 : R2C .
This register count discard frames

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cntdis0                                                                          0x000204

/*--------------------------------------
BitField Name: cntdis0
BitField Type: RW
BitField Desc: cntdis0
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_cntdis0_cntdis0_Mask                                                                cBit31_0
#define cAf6_upen_cntdis0_cntdis0_Shift                                                                      0

#ifdef __cplusplus
}
#endif
#endif /* _THA60290081MODULECLAREG_H_ */


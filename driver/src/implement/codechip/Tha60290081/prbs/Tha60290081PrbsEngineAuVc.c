/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60290081PrbsEngineAuVc.c
 *
 * Created Date: Sep 11, 2019
 *
 * Description : 60290081 AU VC PRBS engine implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../../../default/map/ThaModuleStmMap.h"
#include "../../../default/ocn/ThaModuleOcn.h"
#include "../../Tha60290022/prbs/Tha60290022PrbsEngineAuVcInternal.h"
#include "../map/Tha60290081ModuleMapReg.h"
#include "Tha60290081PrbsEngineAuVc.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290081PrbsEngineAuVc
    {
    tTha60290022PrbsEngineAuVc super;
    }tTha60290081PrbsEngineAuVc;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPrbsEngineMethods         m_AtPrbsEngineOverride;
static tThaPrbsEngineMethods        m_ThaPrbsEngineOverride;

/* Save super implementation */
static const tAtPrbsEngineMethods       *m_AtPrbsEngineMethods  = NULL;
static const tThaPrbsEngineMethods      *m_ThaPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtDevice DeviceGet(ThaPrbsEngine self)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    return AtChannelDeviceGet(channel);
    }

static ThaModuleStmMap ModuleMap(ThaPrbsEngine self)
    {
    return (ThaModuleStmMap)AtDeviceModuleGet(DeviceGet(self), cThaModuleMap);
    }

static eBool IsVc4HwMasterSts(uint8 hwSts, uint8 startHwMasterSts, uint8 stopHwMasterSts)
    {
    if ((hwSts >= startHwMasterSts) && (hwSts < stopHwMasterSts))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 MapLineCtrlOffset(ThaModuleAbstractMap mapModule, uint8 hwSlice, uint8 hwSts)
    {
    return ThaModuleAbstractMapHoSliceOffset(mapModule, hwSlice) + (uint32)(hwSts << 5);
    }

static void HoBertHwDefault(ThaPrbsEngine self)
    {
    ThaModuleStmMap moduleMap = ModuleMap(self);
    AtSdhChannel channel = (AtSdhChannel)AtPrbsEngineChannelGet((AtPrbsEngine)self);
    uint32 mapLineRegAddress = ThaModuleStmMapMapLineCtrl(moduleMap, channel);
    uint8 startHwMasterSts = 0, stopHwMasterSts = 0;
    uint8 numSts = AtSdhChannelNumSts(channel);
    uint8 startSts = AtSdhChannelSts1Get(channel);
    uint8 numSts3 = (uint8)(numSts / 3);
    uint8 sts_i;

    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint32 regAddr, regVal;
        uint8 hwSlice, hwSts;
        uint8 sts1Id = (uint8)(startSts + sts_i);

        if (ThaSdhChannelHwStsGet(channel, cThaModuleMap, sts1Id, &hwSlice, &hwSts) != cAtOk)
            mChannelLog(channel, cAtLogLevelCritical, "Cannot get HW sts ID and slice");

        if (sts_i == 0)
            {
            startHwMasterSts = hwSts;
            stopHwMasterSts = (uint8)(hwSts + numSts3);
            }

        regAddr = mapLineRegAddress + MapLineCtrlOffset((ThaModuleAbstractMap)moduleMap, hwSlice, hwSts);
        regVal  = mChannelHwRead(channel, regAddr, cThaModuleMap);
        mRegFieldSet(regVal, cAf6_map_line_ctrl_VC4Master_, IsVc4HwMasterSts(hwSts, startHwMasterSts, stopHwMasterSts) ? 1 : 0);
        mChannelHwWrite(channel, regAddr, regVal, cThaModuleMap);
        }
    }

static void HoBertHwCleanUp(ThaPrbsEngine self)
    {
    ThaModuleStmMap moduleMap = ModuleMap(self);
    AtSdhChannel channel = (AtSdhChannel)AtPrbsEngineChannelGet((AtPrbsEngine)self);
    uint32 mapLineRegAddress = ThaModuleStmMapMapLineCtrl(moduleMap, channel);
    uint8 numSts = AtSdhChannelNumSts(channel);
    uint8 startSts = AtSdhChannelSts1Get(channel);
    uint8 sts_i;

    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint8 sts1Id = (uint8)(startSts + sts_i);
        uint8 hwSlice, hwSts;
        uint32 regAddr, regVal;

        if (ThaSdhChannelHwStsGet(channel, cThaModuleMap, sts1Id, &hwSlice, &hwSts) != cAtOk)
            mChannelLog(channel, cAtLogLevelCritical, "Cannot get HW sts ID and slice");

        regAddr = mapLineRegAddress + MapLineCtrlOffset((ThaModuleAbstractMap)moduleMap, hwSlice, hwSts);
        regVal  = mChannelHwRead(channel, regAddr, cThaModuleMap);
        mRegFieldSet(regVal, cAf6_map_line_ctrl_VC4Master_, 0);
        mChannelHwWrite(channel, regAddr, regVal, cThaModuleMap);
        }
    }

static eAtRet HwCleanup(AtPrbsEngine self)
    {
    eAtRet ret = m_AtPrbsEngineMethods->HwCleanup(self);
    HoBertHwCleanUp((ThaPrbsEngine)self);

    return ret;
    }

static void HwDefault(ThaPrbsEngine self)
    {
    m_ThaPrbsEngineMethods->HwDefault(self);
    HoBertHwDefault(self);
    }

static void OverrideThaPrbsEngine(AtPrbsEngine self)
    {
    ThaPrbsEngine engine = (ThaPrbsEngine)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPrbsEngineMethods = mMethodsGet(engine);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPrbsEngineOverride, mMethodsGet(engine), sizeof(m_ThaPrbsEngineOverride));

        mMethodOverride(m_ThaPrbsEngineOverride, HwDefault);
        }

    mMethodsSet(engine, &m_ThaPrbsEngineOverride);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, mMethodsGet(self), sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, HwCleanup);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideThaPrbsEngine(self);
    OverrideAtPrbsEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081PrbsEngineAuVc);
    }

static AtPrbsEngine ObjectInit(AtPrbsEngine self, AtSdhChannel vc, uint32 engineId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022PrbsEngineAuVcObjectInit(self, vc, engineId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha60290081PrbsEngineAuVcNew(AtSdhChannel vc, uint32 engineId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newEngine, vc, engineId);
    }

void Tha60290081PrbsEngineAuVcHoBertHwDefault(ThaPrbsEngine self, AtSdhChannel channel)
    {
    AtUnused(channel);
    if (self)
        HoBertHwDefault(self);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60290081ModulePrbs.c
 *
 * Created Date: Sep 11, 2019
 *
 * Description : 60290081 Module PRBS implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290081ModulePrbsInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60290021ModulePrbsMethods m_Tha60290021ModulePrbsOverride;

/* Save super implementation */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 NumAuVc4_64cEngines(Tha60290021ModulePrbs self)
    {
    AtUnused(self);
    return 1;
    }

static AtPrbsEngine AuVcxPrbsEngineObjectCreate(Tha60290021ModulePrbs self, uint32 engineId, AtSdhChannel sdhVc)
    {
    AtUnused(self);
    return Tha60290081PrbsEngineAuVcNew(sdhVc, engineId);
    }

static void OverrideTha60290021ModulePrbs(AtModulePrbs self)
    {
    Tha60290021ModulePrbs module = (Tha60290021ModulePrbs)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021ModulePrbsOverride, mMethodsGet(module), sizeof(m_Tha60290021ModulePrbsOverride));

        mMethodOverride(m_Tha60290021ModulePrbsOverride, AuVcxPrbsEngineObjectCreate);
        mMethodOverride(m_Tha60290021ModulePrbsOverride, NumAuVc4_64cEngines);
        }

    mMethodsSet(module, &m_Tha60290021ModulePrbsOverride);
    }

static void Override(AtModulePrbs self)
    {
    OverrideTha60290021ModulePrbs(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081ModulePrbs);
    }

static AtModulePrbs ObjectInit(AtModulePrbs self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022ModulePrbsV2ObjectInit(self, device) == NULL)
        return NULL;

    /* Over-ride */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePrbs Tha60290081ModulePrbsNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePrbs newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : Tha60290081PrbsEngineAuVc.h
 * 
 * Created Date: Oct 21, 2019
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60290081_PRBS_THA60290081PRBSENGINEAUVC_H_
#define _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60290081_PRBS_THA60290081PRBSENGINEAUVC_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/prbs/ThaPrbsEngine.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void Tha60290081PrbsEngineAuVcHoBertHwDefault(ThaPrbsEngine self, AtSdhChannel channel);

#ifdef __cplusplus
}
#endif
#endif /* _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60290081_PRBS_THA60290081PRBSENGINEAUVC_H_ */


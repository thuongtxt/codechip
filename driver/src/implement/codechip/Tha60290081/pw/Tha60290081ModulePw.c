/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60290081ModulePw.c
 *
 * Created Date: Jun 15, 2019
 *
 * Description : 60290081 Module PW implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290022/pw/Tha60290022PwKByte.h"
#include "Tha60290081ModulePwInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePwMethods          m_AtModulePwOverride;
static tThaModulePwMethods         m_ThaModulePwOverride;
static tTha60290021ModulePwMethods m_Tha60290021ModulePwOverride;

/* Save super implementation */
static const tAtModulePwMethods     *m_AtModulePwMethods = NULL;
static const tThaModulePwMethods    *m_ThaModulePwMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxPwsGet(AtModulePw self)
    {
    AtUnused(self);
    return 5376;
    }

static AtPwHdlc HdlcPwObjectCreate(AtModulePw self, AtHdlcChannel hdlcChannel)
    {
    return Tha60290021PwDccV2New(hdlcChannel, self);
    }

static ThaPwActivator PwActivatorCreate(ThaModulePw self)
    {
    return Tha60290081PwActivatorNew((AtModulePw)self);
    }

static eBool CanBindEthPort(ThaModulePw self, AtPw pw, AtEthPort port)
    {
    /* Need flow bind priorly */
    if (AtPwEthFlowGet(pw) == NULL)
        return cAtFalse;

    return m_ThaModulePwMethods->CanBindEthPort(self, pw, port);
    }

static eBool CanBindEthFlow(ThaModulePw self, AtPw pw, AtEthFlow flow)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(flow);
    return cAtTrue;
    }

static ThaPwHeaderController HeaderControllerObjectCreate(ThaModulePw self, AtPw adapter)
    {
    AtUnused(self);
    return Tha60290081PwHeaderControllerNew((ThaPwAdapter)adapter);
    }

static AtPw KBytePwObjectCreate(Tha60290021ModulePw self, uint32 pwId)
    {
    return Tha60290022PwKbyteV2New(pwId, (AtModulePw)self);
    }

static eBool DccKbyteInterruptIsSupported(Tha60290021ModulePw self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static AtPwKbyteChannel KByteChannelObjectCreate(Tha60290021ModulePw self, uint32 channelId, AtPw pw)
    {
    return Tha60290022PwKbyteChannelNew(channelId, pw, (AtModulePw)self);
    }

static void OverrideAtModulePw(AtModulePw self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModulePwMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePwOverride, m_AtModulePwMethods, sizeof(tAtModulePwMethods));

        mMethodOverride(m_AtModulePwOverride, MaxPwsGet);
        mMethodOverride(m_AtModulePwOverride, HdlcPwObjectCreate);
        }

    mMethodsSet(self, &m_AtModulePwOverride);
    }

static void OverrideThaModulePw(AtModulePw self)
    {
    ThaModulePw pwModule = (ThaModulePw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePwMethods = mMethodsGet(pwModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePwOverride, mMethodsGet(pwModule), sizeof(m_ThaModulePwOverride));

        mMethodOverride(m_ThaModulePwOverride, PwActivatorCreate);
        mMethodOverride(m_ThaModulePwOverride, CanBindEthPort);
        mMethodOverride(m_ThaModulePwOverride, CanBindEthFlow);
        mMethodOverride(m_ThaModulePwOverride, HeaderControllerObjectCreate);
        }

    mMethodsSet(pwModule, &m_ThaModulePwOverride);
    }

static void OverrideTha60290021ModulePw(AtModulePw self)
    {
    Tha60290021ModulePw module = (Tha60290021ModulePw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021ModulePwOverride, mMethodsGet(module), sizeof(m_Tha60290021ModulePwOverride));

        mMethodOverride(m_Tha60290021ModulePwOverride, KBytePwObjectCreate);
        mMethodOverride(m_Tha60290021ModulePwOverride, DccKbyteInterruptIsSupported);
        mMethodOverride(m_Tha60290021ModulePwOverride, KByteChannelObjectCreate);
        }

    mMethodsSet(module, &m_Tha60290021ModulePwOverride);
    }

static void Override(AtModulePw self)
    {
    OverrideAtModulePw(self);
    OverrideThaModulePw(self);
    OverrideTha60290021ModulePw(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081ModulePw);
    }

AtModulePw Tha60290081ModulePwObjectInit(AtModulePw self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022ModulePwObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePw Tha60290081ModulePwNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePw newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290081ModulePwObjectInit(newModule, device);
    }

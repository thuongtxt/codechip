/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60290081PwActivator.c
 *
 * Created Date: Jul 15, 2019
 *
 * Description : 60290081 PW activator implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../Tha60290021/pw/Tha60290021PwActivatorInternal.h"
#include "../../pwe/Tha60290081ModulePwe.h"
#include "../Tha60290081ModulePwInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPwActivatorMethods           m_ThaPwActivatorOverride;
static tTha60210011PwActivatorMethods   m_Tha60210011PwActivatorOverride;

/* Save super implementation */
static const tThaPwActivatorMethods *m_ThaPwActivatorMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static Tha60290081ModulePwe ModulePwe(ThaPwAdapter adapter)
    {
    return (Tha60290081ModulePwe)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)adapter), cThaModulePwe);
    }

static eBool PwCanBeActivatedWithEthPort(ThaPwActivator self, ThaPwAdapter adapter, AtEthPort ethPort)
    {
    if (ThaPwAdapterEthFlowGet(adapter) == NULL)
        return cAtFalse;

    return m_ThaPwActivatorMethods->PwCanBeActivatedWithEthPort(self, adapter, ethPort);
    }

static eAtRet PwEthFlowBind(ThaPwActivator self, ThaPwAdapter adapter, AtEthFlow flow)
    {
    eAtRet ret = m_ThaPwActivatorMethods->PwEthFlowBind(self, adapter, flow);
    if (ret != cAtOk)
        return ret;

    return Tha60290081ModulePwePlaPwEthFlowLookup(ModulePwe(adapter), flow, (AtPw)adapter, cAtTrue);
    }

static eAtRet PwEthFlowUnbind(ThaPwActivator self, ThaPwAdapter adapter)
    {
    eAtRet ret;
    AtEthFlow currentFlow = AtPwEthFlowGet((AtPw)adapter);

    ret  = Tha60290081ModulePwePlaPwEthFlowLookup(ModulePwe(adapter), currentFlow, (AtPw)adapter, cAtFalse);
    ret |= m_ThaPwActivatorMethods->PwEthFlowUnbind(self, adapter);

    return ret;
    }

static void OverrideThaPwActivator(ThaPwActivator self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPwActivatorMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwActivatorOverride, mMethodsGet(self), sizeof(m_ThaPwActivatorOverride));

        mMethodOverride(m_ThaPwActivatorOverride, PwCanBeActivatedWithEthPort);
        mMethodOverride(m_ThaPwActivatorOverride, PwEthFlowBind);
        mMethodOverride(m_ThaPwActivatorOverride, PwEthFlowUnbind);
        }

    mMethodsSet(self, &m_ThaPwActivatorOverride);
    }

static void OverrideTha60210011PwActivator(ThaPwActivator self)
    {
    Tha60210011PwActivator activator = (Tha60210011PwActivator)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011PwActivatorOverride, mMethodsGet(activator), sizeof(m_Tha60210011PwActivatorOverride));

        }

    mMethodsSet(activator, &m_Tha60210011PwActivatorOverride);
    }

static void Override(ThaPwActivator self)
    {
    OverrideThaPwActivator(self);
    OverrideTha60210011PwActivator(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081PwActivator);
    }

ThaPwActivator Tha60290081PwActivatorObjectInit(ThaPwActivator self, AtModulePw pwModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021PwActivatorObjectInit(self, pwModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwActivator Tha60290081PwActivatorNew(AtModulePw pwModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwActivator newActivator = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newActivator == NULL)
        return NULL;

    /* Construct it */
    return Tha60290081PwActivatorObjectInit(newActivator, pwModule);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Tha60290081ModulePwInternal.h
 * 
 * Created Date: Jun 15, 2019
 *
 * Description : 60290081 Module PW representation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290081MODULEPWINTERNAL_H_
#define _THA60290081MODULEPWINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290022/pw/Tha60290022ModulePwInternal.h"
#include "../../Tha60290021/pw/Tha60290021PwActivatorInternal.h"
#include "../../Tha60210011/pw/headercontroller/Tha60210011PwHeaderControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290081ModulePw
    {
    tTha60290022ModulePw super;
    }tTha60290081ModulePw;

typedef struct tTha60290081PwActivator
    {
    tTha60290021PwActivator super;
    }tTha60290081PwActivator;

typedef struct tTha60290081PwHeaderController
    {
    tTha60210011PwHeaderController super;
    }tTha60290081PwHeaderController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPwActivator Tha60290081PwActivatorObjectInit(ThaPwActivator self, AtModulePw pwModule);
ThaPwHeaderController Tha60290081PwHeaderControllerObjectInit(ThaPwHeaderController self, ThaPwAdapter adapter);
AtModulePw Tha60290081ModulePwObjectInit(AtModulePw self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290081MODULEPWINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60290081PwHeaderController.c
 *
 * Created Date: Jul 16, 2019
 *
 * Description : 60290081 PW header coontroller implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../Tha60210011/pw/headercontroller/Tha60210011PwHeaderControllerInternal.h"
#include "../Tha60290081ModulePwInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mDevice(self) AtChannelDeviceGet((AtChannel)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPwHeaderControllerMethods m_ThaPwHeaderControllerOverride;

/* Save super implementation */
static const tThaPwHeaderControllerMethods *m_ThaPwHeaderControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool VlansAreTheSame(const tAtEthVlanTag *vlan1, const tAtEthVlanTag *vlan2)
    {
    if ((vlan1 == NULL) && (vlan2 == NULL))
        return cAtTrue;

    if ((vlan1 == NULL) || (vlan2 == NULL))
        return cAtFalse;

    if ((vlan1->priority == vlan2->priority) &&
        (vlan1->cfi      == vlan2->cfi) &&
        (vlan1->vlanId   == vlan2->vlanId))
        return cAtTrue;

    return cAtFalse;
    }

static eBool VlanDescIsChanged(tAtEthVlanDesc *vlanDesc, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan)
    {
    tAtEthVlanTag *cVlanFromDesc = NULL;
    tAtEthVlanTag *sVlanFromDesc = NULL;

    if (vlanDesc)
        {
        if (vlanDesc->numberOfVlans > 0)
            cVlanFromDesc = &(vlanDesc->vlans[0]);

        if (vlanDesc->numberOfVlans > 1)
            sVlanFromDesc = &(vlanDesc->vlans[1]);
        }

    if (!VlansAreTheSame(cVlan, cVlanFromDesc))
        return cAtTrue;

    if (!VlansAreTheSame(sVlan, sVlanFromDesc))
        return cAtTrue;

    return cAtFalse;
    }

static eAtModulePwRet EthHeaderSet(ThaPwHeaderController self, uint8 *destMac, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan)
    {
    eAtRet ret;
    ThaPwAdapter pwAdapter = ThaPwHeaderControllerAdapterGet(self);
    AtEthFlow flow;
    tAtEthVlanDesc vlanDesc, *pDesc;

    ret = m_ThaPwHeaderControllerMethods->EthHeaderSet(self, destMac, cVlan, sVlan);
    if (ret != cAtOk)
        return ret;

    /* PW not bound Ethernet flow, nothing to do */
    flow = ThaPwAdapterEthFlowGet(pwAdapter);
    if (flow == NULL)
        return cAtOk;

    pDesc = AtEthFlowEgressVlanAtIndex(flow, 0, &vlanDesc);
    if (!VlanDescIsChanged(pDesc, cVlan, sVlan))
        return cAtOk;

    /* Update Ethernet flow egress VLAN descriptor to match PW Ethernet header */
    if (pDesc)
        ret |= AtEthFlowEgressVlanRemove(flow, &vlanDesc);

    if (cVlan)
        {
        AtOsalMemInit(&vlanDesc, 0, sizeof(vlanDesc));

        vlanDesc.numberOfVlans = 1;
        vlanDesc.vlans[0] = *cVlan;

        if (sVlan)
            {
            vlanDesc.numberOfVlans = 2;
            vlanDesc.vlans[1] = *sVlan;
            }

        ret |= AtEthFlowEgressVlanAdd(flow, &vlanDesc);
        }

    return ret;
    }

static void OverrideThaPwHeaderController(ThaPwHeaderController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPwHeaderControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwHeaderControllerOverride, m_ThaPwHeaderControllerMethods, sizeof(m_ThaPwHeaderControllerOverride));

        mMethodOverride(m_ThaPwHeaderControllerOverride, EthHeaderSet);
        }

    mMethodsSet(self, &m_ThaPwHeaderControllerOverride);
    }

static void Override(ThaPwHeaderController self)
    {
    OverrideThaPwHeaderController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081PwHeaderController);
    }

ThaPwHeaderController Tha60290081PwHeaderControllerObjectInit(ThaPwHeaderController self, ThaPwAdapter adapter)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011PwHeaderControllerObjectInit(self, adapter) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwHeaderController Tha60290081PwHeaderControllerNew(ThaPwAdapter adapter)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwHeaderController newProvider = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newProvider == NULL)
        return NULL;

    /* Construct it */
    return Tha60290081PwHeaderControllerObjectInit(newProvider, adapter);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : Tha60290081ModuleDemapInternal.h
 * 
 * Created Date: Oct 17, 2019
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60290081_MAP_THA60290081MODULEDEMAPINTERNAL_H_
#define _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60290081_MAP_THA60290081MODULEDEMAPINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290022/map/Tha60290022ModuleDemapInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290081ModuleDemap
    {
    tTha60290022ModuleDemap super;
    }tTha60290081ModuleDemap;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60290081ModuleDemapObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60290081_MAP_THA60290081MODULEDEMAPINTERNAL_H_ */


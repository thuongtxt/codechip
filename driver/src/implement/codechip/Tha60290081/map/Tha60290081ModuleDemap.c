/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : DEMAP
 *
 * File        : Tha60290081ModuleDemap.c
 *
 * Created Date: Jun 16, 2019
 *
 * Description : 60290081 Module DEMAP implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290022/map/Tha60290022ModuleDemapInternal.h"
#include "Tha60290081ModuleDemapInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define cAf6_demapho_channel_ctrl_DemapPWIdField_Mask                   cBit19_8
#define cAf6_demapho_channel_ctrl_DemapPWIdField_Shift                         8

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleAbstractMapMethods   m_ThaModuleAbstractMapOverride;
static tThaModuleDemapMethods         m_ThaModuleDemapOverride;
static tTha60210011ModuleDemapMethods m_Tha60210011ModuleDemapOverride;
static tTha60290022ModuleDemapMethods m_Tha60290022ModuleDemapOverride;

/* Save super implementation */
static const tThaModuleAbstractMapMethods   *m_ThaModuleAbstractMapMethods = NULL;
static const tThaModuleDemapMethods         *m_ThaModuleDemapMethods = NULL;
static const tTha60210011ModuleDemapMethods *m_Tha60210011ModuleDemapMethods = NULL;
static const tTha60290022ModuleDemapMethods       *m_Tha60290022ModuleDemapMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 HoDemapPWIdFieldMask(Tha60290022ModuleDemap self)
    {
    AtUnused(self);
    return cAf6_demapho_channel_ctrl_DemapPWIdField_Mask;
    }

static uint32 HoDemapPWIdFieldShift(Tha60290022ModuleDemap self)
    {
    AtUnused(self);
    return cAf6_demapho_channel_ctrl_DemapPWIdField_Shift;
    }

static AtChannel ConcateChannelForBinding(ThaModuleAbstractMap self, AtChannel channel, AtConcateGroup concateGroup)
    {
    AtVcgBinder vcgBinder = AtChannelVcgBinder(channel);
    AtUnused(self);
    AtUnused(concateGroup);
    /* Sink member HW ID shall be filled into DEMAP control PW ID. */
    return (AtChannel)AtVcgBinderSinkMemberGet(vcgBinder);
    }

static void OverrideThaModuleAbstractMap(AtModule self)
    {
    ThaModuleAbstractMap mapModule = (ThaModuleAbstractMap)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleAbstractMapMethods = mMethodsGet(mapModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleAbstractMapOverride, m_ThaModuleAbstractMapMethods, sizeof(m_ThaModuleAbstractMapOverride));

        mMethodOverride(m_ThaModuleAbstractMapOverride, ConcateChannelForBinding);
        }

    mMethodsSet(mapModule, &m_ThaModuleAbstractMapOverride);
    }

static void OverrideThaModuleDemap(AtModule self)
    {
    ThaModuleDemap demapModule = (ThaModuleDemap)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleDemapMethods = mMethodsGet(demapModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleDemapOverride, mMethodsGet(demapModule), sizeof(m_ThaModuleDemapOverride));

        }

    mMethodsSet(demapModule, &m_ThaModuleDemapOverride);
    }

static void OverrideTha60210011ModuleDemap(AtModule self)
    {
    Tha60210011ModuleDemap module = (Tha60210011ModuleDemap)self;

    if (!m_methodsInit)
        {
        /* Copy super implementation */
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011ModuleDemapMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModuleDemapOverride, mMethodsGet(module), sizeof(m_Tha60210011ModuleDemapOverride));

        }

    mMethodsSet(module, &m_Tha60210011ModuleDemapOverride);
    }

static void OverrideTha60290022ModuleDemap(AtModule self)
    {
    Tha60290022ModuleDemap demapModule = (Tha60290022ModuleDemap)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290022ModuleDemapMethods = mMethodsGet(demapModule);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290022ModuleDemapOverride, mMethodsGet(demapModule), sizeof(m_Tha60290022ModuleDemapOverride));

        mMethodOverride(m_Tha60290022ModuleDemapOverride, HoDemapPWIdFieldMask);
        mMethodOverride(m_Tha60290022ModuleDemapOverride, HoDemapPWIdFieldShift);
        }

    mMethodsSet(demapModule, &m_Tha60290022ModuleDemapOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleAbstractMap(self);
    OverrideThaModuleDemap(self);
    OverrideTha60210011ModuleDemap(self);
    OverrideTha60290022ModuleDemap(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081ModuleDemap);
    }

AtModule Tha60290081ModuleDemapObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022ModuleDemapObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60290081ModuleDemapNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290081ModuleDemapObjectInit(newModule, device);
    }

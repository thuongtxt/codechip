/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAP
 *
 * File        : Tha60290081ModuleMap.c
 *
 * Created Date: Jun 16, 2019
 *
 * Description : 60290081 Module MAP implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290022/map/Tha60290022ModuleMapInternal.h"
#include "Tha60290081ModuleMapInternal.h"
#include "Tha60290081ModuleMapReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleAbstractMapMethods m_ThaModuleAbstractMapOverride;
static tThaModuleMapMethods         m_ThaModuleMapOverride;
static tTha60210011ModuleMapMethods m_Tha60210011ModuleMapOverride;

/* Save super implementation */
static const tThaModuleAbstractMapMethods *m_ThaModuleAbstractMapMethods = NULL;
static const tThaModuleMapMethods         *m_ThaModuleMapMethods         = NULL;
static const tTha60210011ModuleMapMethods *m_Tha60210011ModuleMapMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 HoLineVcPwAllocate(Tha60210011ModuleMap self, ThaPwAdapter adapter, uint8 *slice)
    {
    return m_Tha60210011ModuleMapMethods->HoLineVcPwAllocate(self, adapter, slice);
    }

static eAtRet BindAuVcToPseudowire(ThaModuleAbstractMap self, AtSdhVc vc, AtPw pw)
    {
    return m_ThaModuleAbstractMapMethods->BindAuVcToPseudowire(self, vc, pw);
    }

static eAtRet AuVcFrameModeSet(ThaModuleAbstractMap self, AtSdhVc vc)
    {
    return m_ThaModuleAbstractMapMethods->AuVcFrameModeSet(self, vc);
    }

static eAtRet VcxEncapConnectionEnable(ThaModuleAbstractMap self, AtSdhVc vcx, eBool enable)
    {
    return m_ThaModuleAbstractMapMethods->VcxEncapConnectionEnable(self, vcx, enable);
    }

static AtChannel ConcateChannelForBinding(ThaModuleAbstractMap self, AtChannel channel, AtConcateGroup concateGroup)
    {
    AtVcgBinder vcgBinder = AtChannelVcgBinder(channel);
    AtUnused(self);
    AtUnused(concateGroup);
    /* Source member HW ID shall be filled into MAP control PW ID. */
    return (AtChannel)AtVcgBinderSourceMemberGet(vcgBinder);
    }

static void OverrideTha60210011ModuleMap(AtModule self)
    {
    Tha60210011ModuleMap module = (Tha60210011ModuleMap)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011ModuleMapMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModuleMapOverride, mMethodsGet(module), sizeof(m_Tha60210011ModuleMapOverride));

        mMethodOverride(m_Tha60210011ModuleMapOverride, HoLineVcPwAllocate);
        }

    mMethodsSet(module, &m_Tha60210011ModuleMapOverride);
    }

static void OverrideThaModuleMap(AtModule self)
    {
    ThaModuleMap mapModule = (ThaModuleMap)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleMapMethods = mMethodsGet(mapModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleMapOverride, mMethodsGet(mapModule), sizeof(m_ThaModuleMapOverride));

        }

    mMethodsSet(mapModule, &m_ThaModuleMapOverride);
    }

static void OverrideThaModuleAbstractMap(AtModule self)
    {
    ThaModuleAbstractMap mapModule = (ThaModuleAbstractMap)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleAbstractMapMethods = mMethodsGet(mapModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleAbstractMapOverride, m_ThaModuleAbstractMapMethods, sizeof(m_ThaModuleAbstractMapOverride));

        mMethodOverride(m_ThaModuleAbstractMapOverride, BindAuVcToPseudowire);
        mMethodOverride(m_ThaModuleAbstractMapOverride, AuVcFrameModeSet);
        mMethodOverride(m_ThaModuleAbstractMapOverride, VcxEncapConnectionEnable);
        mMethodOverride(m_ThaModuleAbstractMapOverride, ConcateChannelForBinding);
        }

    mMethodsSet(mapModule, &m_ThaModuleAbstractMapOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleAbstractMap(self);
    OverrideThaModuleMap(self);
    OverrideTha60210011ModuleMap(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081ModuleMap);
    }

AtModule Tha60290081ModuleMapObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022ModuleMapObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60290081ModuleMapNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290081ModuleMapObjectInit(newModule, device);
    }


/*
eAtRet Tha60290081ModuleMapVc4xMasterStsIndicate(Tha60290081ModuleMap self, AtSdhVc vc)
    {
    static const uint8 cVtgDontCare = 0;
    static const uint8 cVtDontCare = 0;
    AtSdhChannel sdhChannel = (AtSdhChannel)vc;
    uint8 numSts = AtSdhChannelNumSts(sdhChannel);
    ThaModuleStmMap stmMapModule = (ThaModuleStmMap)self;
    uint8 numSts3InVc4x = (uint8)(numSts / 3);
    uint8  sts_i;

    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint32 address = ThaModuleStmMapMapLineCtrl(stmMapModule, sdhChannel) +
                         ThaModuleStmMapSdhChannelMapLineOffset(stmMapModule, sdhChannel,
                                                                (uint8)(AtSdhChannelSts1GetAtIndex(sdhChannel, sts_i)),
                                                                cVtgDontCare, cVtDontCare);
        uint32 regVal  = mChannelHwRead(vc, address, cThaModuleMap);

        mRegFieldSet(regVal, cAf6_map_line_ctrl_VC4Master_, (sts_i < numSts3InVc4x) ? 1 : 0);
        mChannelHwWrite(vc, address, regVal, cThaModuleMap);
        }

    return cAtOk;
    }
*/

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Encap
 * 
 * File        : Tha60290081ModuleEncapGfpReg.h
 * 
 * Created Date: Jul 9, 2019
 *
 * Description :
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290081MODULEENCAPGFPREG_H_
#define _THA60290081MODULEENCAPGFPREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name   : Decapsulation control
Reg Addr   : 0x000480-0x0004FF
Reg Formula: 0x000480 + $VCG
    Where  :
           + $VCG(0-127) : VC group - ID
Reg Desc   :


------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcap_c                                                                           0x000480

/*--------------------------------------
BitField Name: cfgithec_dcap
BitField Type: R/W
BitField Desc: tHEC correction enable
BitField Bits: [9]
--------------------------------------*/
#define cAf6_upen_dcap_c_cfgithec_dcap_Mask                                                              cBit9
#define cAf6_upen_dcap_c_cfgithec_dcap_Shift                                                                 9

/*--------------------------------------
BitField Name: cfgiescm_dcap
BitField Type: R/W
BitField Desc: GFP - F scramble enable on decapsulation
BitField Bits: [8]
--------------------------------------*/
#define cAf6_upen_dcap_c_cfgiescm_dcap_Mask                                                              cBit8
#define cAf6_upen_dcap_c_cfgiescm_dcap_Shift                                                                 8

/*--------------------------------------
BitField Name: cfgipupi_dcap
BitField Type: R/W
BitField Desc: GFP - F UPI Field Control on decapsulation
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_upen_dcap_c_cfgipupi_dcap_Mask                                                            cBit7_0
#define cAf6_upen_dcap_c_cfgipupi_dcap_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Encapsulation control
Reg Addr   : 0x000400-0x00047F
Reg Formula: 0x000400 + $VCG
    Where  :
           + $VCG(0-127) : VC group - ID
Reg Desc   :


------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ecap_c                                                                           0x000400

/*--------------------------------------
BitField Name: cfgieau3_ecap
BitField Type: R/W
BitField Desc: Enable bit when VCG in VC3-1V or VC3-2V mode
BitField Bits: [10]
--------------------------------------*/
#define cAf6_upen_ecap_c_cfgieau3_ecap_Mask                                                             cBit10
#define cAf6_upen_ecap_c_cfgieau3_ecap_Shift                                                                10

/*--------------------------------------
BitField Name: cfgipfcs_ecap
BitField Type: R/W
BitField Desc: Payload FCS enable
BitField Bits: [9]
--------------------------------------*/
#define cAf6_upen_ecap_c_cfgipfcs_ecap_Mask                                                              cBit9
#define cAf6_upen_ecap_c_cfgipfcs_ecap_Shift                                                                 9

/*--------------------------------------
BitField Name: cfgiescm_ecap
BitField Type: R/W
BitField Desc: GFP - F scramble enable on encapsulation
BitField Bits: [8]
--------------------------------------*/
#define cAf6_upen_ecap_c_cfgiescm_ecap_Mask                                                              cBit8
#define cAf6_upen_ecap_c_cfgiescm_ecap_Shift                                                                 8

/*--------------------------------------
BitField Name: cfgipupi_ecap
BitField Type: R/W
BitField Desc: GFP - F UPI Field Control on encapsulation
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_upen_ecap_c_cfgipupi_ecap_Mask                                                            cBit7_0
#define cAf6_upen_ecap_c_cfgipupi_ecap_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : CMF C-HEC, T-HEC ecapsulation control.
Reg Addr   : 0x000C00-0x000C7F
Reg Formula: 0x000C00 + $VCG
    Where  : 
           + $VCG(0-127) : VC group - ID
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ecmf_header_Base                                                                 0x000C00
#define cAf6Reg_upen_ecmf_header(VCG)                                                         (0x000C00+(VCG))

/*--------------------------------------
BitField Name: chec_cmf
BitField Type: R/W
BitField Desc: C-HEC header octets with CRC-16 excepted in CMF message
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_upen_ecmf_header_chec_cmf_Mask                                                          cBit31_16
#define cAf6_upen_ecmf_header_chec_cmf_Shift                                                                16

/*--------------------------------------
BitField Name: thec_cmf
BitField Type: R/W
BitField Desc: T-HEC header octets with CRC-16 excepted in CMF message
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_upen_ecmf_header_thec_cmf_Mask                                                           cBit15_0
#define cAf6_upen_ecmf_header_thec_cmf_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : MCF C-HEC, T-HEC ecapsulation control.
Reg Addr   : 0x000C80-0x000CFF
Reg Formula: 0x000C80 + $VCG
    Where  : 
           + $VCG(0-127) : VC group - ID
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_upen_emcf_header_Base                                                                 0x000C80
#define cAf6Reg_upen_emcf_header(VCG)                                                         (0x000C80+(VCG))

/*--------------------------------------
BitField Name: chec_mcf
BitField Type: R/W
BitField Desc: C-HEC header octets with CRC-16 excepted in MCF message
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_upen_emcf_header_chec_mcf_Mask                                                          cBit31_16
#define cAf6_upen_emcf_header_chec_mcf_Shift                                                                16

/*--------------------------------------
BitField Name: thec_mcf
BitField Type: R/W
BitField Desc: T-HEC header octets with CRC-16 excepted in MCF message
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_upen_emcf_header_thec_mcf_Mask                                                           cBit15_0
#define cAf6_upen_emcf_header_thec_mcf_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : CMF buffer ecapsulation control.
Reg Addr   : 0x000D00-0x000D7F
Reg Formula: 0x000D00 + $VCG
    Where  : 
           + $VCG(0-127) : VC group - ID
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ecmf_buffer_Base                                                                 0x000D00
#define cAf6Reg_upen_ecmf_buffer(VCG)                                                         (0x000D00+(VCG))

/*--------------------------------------
BitField Name: icmfcnob
BitField Type: R/W
BitField Desc: Indicate number of byte in use on 64 bits width at the end
location
BitField Bits: [26:24]
--------------------------------------*/
#define cAf6_upen_ecmf_buffer_icmfcnob_Mask                                                          cBit26_24
#define cAf6_upen_ecmf_buffer_icmfcnob_Shift                                                                24

/*--------------------------------------
BitField Name: icmfsbin
BitField Type: R/W
BitField Desc: Indicate start location of internal buffer
BitField Bits: [23:12]
--------------------------------------*/
#define cAf6_upen_ecmf_buffer_icmfsbin_Mask                                                          cBit23_12
#define cAf6_upen_ecmf_buffer_icmfsbin_Shift                                                                12

/*--------------------------------------
BitField Name: icmfebin
BitField Type: R/W
BitField Desc: Indicate end location of internal buffer
BitField Bits: [11:0]
--------------------------------------*/
#define cAf6_upen_ecmf_buffer_icmfebin_Mask                                                           cBit11_0
#define cAf6_upen_ecmf_buffer_icmfebin_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : MCF buffer ecapsulation control.
Reg Addr   : 0x000D80-0x000DFF
Reg Formula: 0x000D80 + $VCG
    Where  : 
           + $VCG(0-127) : VC group - ID
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_upen_emcf_buffer_Base                                                                 0x000D80
#define cAf6Reg_upen_emcf_buffer(VCG)                                                         (0x000D80+(VCG))

/*--------------------------------------
BitField Name: imcfcnob
BitField Type: R/W
BitField Desc: Indicate number of byte in use on 64 bits width at the end
location
BitField Bits: [26:24]
--------------------------------------*/
#define cAf6_upen_emcf_buffer_imcfcnob_Mask                                                          cBit26_24
#define cAf6_upen_emcf_buffer_imcfcnob_Shift                                                                24

/*--------------------------------------
BitField Name: imcfsbin
BitField Type: R/W
BitField Desc: Indicate start location of internal buffer
BitField Bits: [23:12]
--------------------------------------*/
#define cAf6_upen_emcf_buffer_imcfsbin_Mask                                                          cBit23_12
#define cAf6_upen_emcf_buffer_imcfsbin_Shift                                                                12

/*--------------------------------------
BitField Name: imcfebin
BitField Type: R/W
BitField Desc: Indicate end location of internal buffer
BitField Bits: [11:0]
--------------------------------------*/
#define cAf6_upen_emcf_buffer_imcfebin_Mask                                                           cBit11_0
#define cAf6_upen_emcf_buffer_imcfebin_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : OAM master control.
Reg Addr   : 0x000E00-0x000E7F
Reg Formula: 0x000E00 + $VCG
    Where  : 
           + $VCG(0-127) : VC group - ID
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_upen_eoam_master_Base                                                                 0x000E00
#define cAf6Reg_upen_eoam_master(VCG)                                                         (0x000E00+(VCG))

/*--------------------------------------
BitField Name: imcfctrl
BitField Type: R/W
BitField Desc: MCF buffer enable
BitField Bits: [4]
--------------------------------------*/
#define cAf6_upen_eoam_master_imcfctrl_Mask                                                              cBit4
#define cAf6_upen_eoam_master_imcfctrl_Shift                                                                 4

/*--------------------------------------
BitField Name: icmfctrl
BitField Type: R/W
BitField Desc: CMF buffer enable
BitField Bits: [3]
--------------------------------------*/
#define cAf6_upen_eoam_master_icmfctrl_Mask                                                              cBit3
#define cAf6_upen_eoam_master_icmfctrl_Shift                                                                 3

/*--------------------------------------
BitField Name: imcfenaf
BitField Type: R/W
BitField Desc: MCF enable
BitField Bits: [2]
--------------------------------------*/
#define cAf6_upen_eoam_master_imcfenaf_Mask                                                              cBit2
#define cAf6_upen_eoam_master_imcfenaf_Shift                                                                 2

/*--------------------------------------
BitField Name: icmfenaf
BitField Type: R/W
BitField Desc: CMF enable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_upen_eoam_master_icmfenaf_Mask                                                              cBit1
#define cAf6_upen_eoam_master_icmfenaf_Shift                                                                 1

/*--------------------------------------
BitField Name: idcienaf
BitField Type: R/W
BitField Desc: DCI enable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upen_eoam_master_idcienaf_Mask                                                              cBit0
#define cAf6_upen_eoam_master_idcienaf_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : GFP-F master control
Reg Addr   : 0x000000
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_upen_gfpf_c                                                                           0x000000

/*--------------------------------------
BitField Name: LBCTRL
BitField Type: R/W
BitField Desc: ENC to DEC looback when set one
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upen_gfpf_c_LBCTRL_Mask                                                                     cBit0
#define cAf6_upen_gfpf_c_LBCTRL_Shift                                                                        0


/*------------------------------------------------------------------------------
Reg Name   : GFP-F cycle control
Reg Addr   : 0x000001
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cycl_c                                                                           0x000001

/*--------------------------------------
BitField Name: icsfdcyc
BitField Type: R/W
BitField Desc: Number of 1000MS(Nx1000ms) for clearing the defect condition of
CSF on decapsulation
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_upen_cycl_c_icsfdcyc_Mask                                                                 cBit3_0
#define cAf6_upen_cycl_c_icsfdcyc_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Alarm Interrupt Enable Control
Reg Addr   : 0x000200-0x00027F
Reg Formula: 0x000200 + $VCG
    Where  :
           + $VCG(0-127): VC group - ID
Reg Desc   :
These registers are used to enable interruption in VCGs.

------------------------------------------------------------------------------*/
#define cAf6Reg_upchcfgpen                                                                            0x000200

/*--------------------------------------
BitField Name: c_dupm_ena
BitField Type: R/W
BitField Desc: Sink Detected User Payload Identify (UPI) mismatch for data frame
(PTI=000)
BitField Bits: [6]
--------------------------------------*/
#define cAf6_upchcfgpen_c_dupm_ena_Mask                                                                  cBit6
#define cAf6_upchcfgpen_c_dupm_ena_Shift                                                                     6

/*--------------------------------------
BitField Name: c_ddci_ena
BitField Type: R/W
BitField Desc: Sink Detected Client defect clear indication (DCI)
BitField Bits: [5]
--------------------------------------*/
#define cAf6_upchcfgpen_c_ddci_ena_Mask                                                                  cBit5
#define cAf6_upchcfgpen_c_ddci_ena_Shift                                                                     5

/*--------------------------------------
BitField Name: c_dfdi_ena
BitField Type: R/W
BitField Desc: Sink Detected client forward defect indication (FDI)
BitField Bits: [4]
--------------------------------------*/
#define cAf6_upchcfgpen_c_dfdi_ena_Mask                                                                  cBit4
#define cAf6_upchcfgpen_c_dfdi_ena_Shift                                                                     4

/*--------------------------------------
BitField Name: c_drdi_ena
BitField Type: R/W
BitField Desc: Sink Detected client reverse defect indication (RDI)
BitField Bits: [3]
--------------------------------------*/
#define cAf6_upchcfgpen_c_drdi_ena_Mask                                                                  cBit3
#define cAf6_upchcfgpen_c_drdi_ena_Shift                                                                     3

/*--------------------------------------
BitField Name: c_dloc_ena
BitField Type: R/W
BitField Desc: Sink Detected Client Loss of character synchronization
BitField Bits: [2]
--------------------------------------*/
#define cAf6_upchcfgpen_c_dloc_ena_Mask                                                                  cBit2
#define cAf6_upchcfgpen_c_dloc_ena_Shift                                                                     2

/*--------------------------------------
BitField Name: c_dlos_ena
BitField Type: R/W
BitField Desc: Sink Detected Client Loss Of Signal
BitField Bits: [1]
--------------------------------------*/
#define cAf6_upchcfgpen_c_dlos_ena_Mask                                                                  cBit1
#define cAf6_upchcfgpen_c_dlos_ena_Shift                                                                     1

/*--------------------------------------
BitField Name: c_dlfd_ena
BitField Type: R/W
BitField Desc: Sink Detected Loss-Of-Frame Delineation
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upchcfgpen_c_dlfd_ena_Mask                                                                  cBit0
#define cAf6_upchcfgpen_c_dlfd_ena_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Alarm Interrupt Sticky
Reg Addr   : 0x000280-0x0002FF
Reg Formula: 0x000280 + $VCG
    Where  :
           + $VCG(0-127): VC group - ID
Reg Desc   :
These registers are used to store interruption bits in VCGs.

------------------------------------------------------------------------------*/
#define cAf6Reg_upchstypen                                                                            0x000280

/*--------------------------------------
BitField Name: c_dupm_sticky
BitField Type: R/W
BitField Desc: Sink Detected User Payload Identify (UPI) mismatch for data frame
(PTI=000)
BitField Bits: [6]
--------------------------------------*/
#define cAf6_upchstypen_c_dupm_sticky_Mask                                                               cBit6
#define cAf6_upchstypen_c_dupm_sticky_Shift                                                                  6

/*--------------------------------------
BitField Name: c_ddci_sticky
BitField Type: R/W
BitField Desc: Sink Detected Client defect clear indication (DCI)
BitField Bits: [5]
--------------------------------------*/
#define cAf6_upchstypen_c_ddci_sticky_Mask                                                               cBit5
#define cAf6_upchstypen_c_ddci_sticky_Shift                                                                  5

/*--------------------------------------
BitField Name: c_dfdi_sticky
BitField Type: R/W
BitField Desc: Sink Detected client forward defect indication (FDI)
BitField Bits: [4]
--------------------------------------*/
#define cAf6_upchstypen_c_dfdi_sticky_Mask                                                               cBit4
#define cAf6_upchstypen_c_dfdi_sticky_Shift                                                                  4

/*--------------------------------------
BitField Name: c_drdi_sticky
BitField Type: R/W
BitField Desc: Sink Detected client reverse defect indication (RDI)
BitField Bits: [3]
--------------------------------------*/
#define cAf6_upchstypen_c_drdi_sticky_Mask                                                               cBit3
#define cAf6_upchstypen_c_drdi_sticky_Shift                                                                  3

/*--------------------------------------
BitField Name: c_dloc_sticky
BitField Type: R/W
BitField Desc: Sink Detected Client Loss of character synchronization
BitField Bits: [2]
--------------------------------------*/
#define cAf6_upchstypen_c_dloc_sticky_Mask                                                               cBit2
#define cAf6_upchstypen_c_dloc_sticky_Shift                                                                  2

/*--------------------------------------
BitField Name: c_dlos_sticky
BitField Type: R/W
BitField Desc: Sink Detected Client Loss Of Signal
BitField Bits: [1]
--------------------------------------*/
#define cAf6_upchstypen_c_dlos_sticky_Mask                                                               cBit1
#define cAf6_upchstypen_c_dlos_sticky_Shift                                                                  1

/*--------------------------------------
BitField Name: c_dlfd_sticky
BitField Type: R/W
BitField Desc: Sink Detected Loss-Of-Frame Delineation
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upchstypen_c_dlfd_sticky_Mask                                                               cBit0
#define cAf6_upchstypen_c_dlfd_sticky_Shift                                                                  0

/*------------------------------------------------------------------------------
Reg Name   : Alarm Interrupt Status
Reg Addr   : 0x000300-0x00037F
Reg Formula: 0x000300 + $VCG
    Where  :
           + $VCG(0-127): VC group - ID
Reg Desc   :
These registers are used to store current status of bits are interruptted in VCGs.

------------------------------------------------------------------------------*/
#define cAf6Reg_upchstapen                                                                            0x000300

/*--------------------------------------
BitField Name: c_dfdi_status
BitField Type: R/W
BitField Desc: Sink Detected client forward defect indication (FDI)
BitField Bits: [4]
--------------------------------------*/
#define cAf6_upchstapen_c_dfdi_status_Mask                                                               cBit4
#define cAf6_upchstapen_c_dfdi_status_Shift                                                                  4

/*--------------------------------------
BitField Name: c_drdi_status
BitField Type: R/W
BitField Desc: Sink Detected client reverse defect indication (RDI)
BitField Bits: [3]
--------------------------------------*/
#define cAf6_upchstapen_c_drdi_status_Mask                                                               cBit3
#define cAf6_upchstapen_c_drdi_status_Shift                                                                  3

/*--------------------------------------
BitField Name: c_dloc_status
BitField Type: R/W
BitField Desc: Sink Detected Client Loss of character synchronization
BitField Bits: [2]
--------------------------------------*/
#define cAf6_upchstapen_c_dloc_status_Mask                                                               cBit2
#define cAf6_upchstapen_c_dloc_status_Shift                                                                  2

/*--------------------------------------
BitField Name: c_dlos_status
BitField Type: R/W
BitField Desc: Sink Detected Client Loss Of Signal
BitField Bits: [1]
--------------------------------------*/
#define cAf6_upchstapen_c_dlos_status_Mask                                                               cBit1
#define cAf6_upchstapen_c_dlos_status_Shift                                                                  1

/*--------------------------------------
BitField Name: c_dlfd_status
BitField Type: R/W
BitField Desc: Sink Detected Loss-Of-Frame Delineation
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upchstapen_c_dlfd_status_Mask                                                               cBit0
#define cAf6_upchstapen_c_dlfd_status_Shift                                                                  0

/*------------------------------------------------------------------------------
Reg Name   : OR Interrupt Status per VCG
Reg Addr   : 0x000380-0x000387
Reg Formula: 0x000380+$addr
    Where  :
           + $addr(0-7): Address of RAM
Reg Desc   :
The register consists of 16 bits for 128 VCGs. Each bit is used to store Interrupt Status per VCG.

------------------------------------------------------------------------------*/
#define cAf6Reg_upsegpen                                                                              0x000380

/*--------------------------------------
BitField Name: intsta
BitField Type: R_O
BitField Desc: Every bit corresponding with Interrupt Status per VCG
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_upsegpen_intsta_Mask                                                                     cBit15_0
#define cAf6_upsegpen_intsta_Shift                                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Global Interrupt Enable Configuration
Reg Addr   : 0x0003FE
Reg Formula:
    Where  :
Reg Desc   :
The register consists of 16 bits used to configurate the row of OR Interrupt Status per VCG Interrup Enable.

------------------------------------------------------------------------------*/
#define cAf6Reg_cfgglbintenpen                                                                        0x0003FE

/*--------------------------------------
BitField Name: intenb
BitField Type: R/W
BitField Desc: Every bit used to Enable the row of OR Interrupt Status per VCG
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_cfgglbintenpen_intenb_Mask                                                                cBit7_0
#define cAf6_cfgglbintenpen_intenb_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Interrupt Status at a Row
Reg Addr   : 0x0003FF
Reg Formula:
    Where  :
Reg Desc   :
The register consists of 16 bits used to know the row of OR Interrupt Status per VCG (32-VCG) Interrup

------------------------------------------------------------------------------*/
#define cAf6Reg_upglbintpen                                                                           0x0003FF

/*--------------------------------------
BitField Name: intst
BitField Type: R/W
BitField Desc: Every bit corresponding the row of OR Interrupt Status per VCG
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_upglbintpen_intst_Mask                                                                    cBit7_0
#define cAf6_upglbintpen_intst_Shift                                                                         0

#ifdef __cplusplus
}
#endif
#endif /* _THA60290081MODULEENCAPGFPREG_H_ */


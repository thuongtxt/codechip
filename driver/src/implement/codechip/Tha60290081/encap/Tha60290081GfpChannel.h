/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : Tha60290081GfpChannel.h
 * 
 * Created Date: Aug 27, 2019
 *
 * Description : 60290081 GFP-F channel
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290081GFPCHANNEL_H_
#define _THA60290081GFPCHANNEL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtGfpChannel.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290081GfpChannel *Tha60290081GfpChannel;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha60290081GfpChannelTxBandwidthUpdate(AtGfpChannel self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290081GFPCHANNEL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : Tha60290081ModuleEncapInternal.h
 * 
 * Created Date: Jul 15, 2019
 *
 * Description : 60290081 Module ENCAP representation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290081MODULEENCAPINTERNAL_H_
#define _THA60290081MODULEENCAPINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/encap/ThaModuleEncapInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290081ModuleEncap
    {
    tThaModuleEncap super;
    }tTha60290081ModuleEncap;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleEncap Tha60290081ModuleEncapObjectInit(AtModuleEncap self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290081MODULEENCAPINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Encap
 *
 * File        : Tha60290081EncapBinder.c
 *
 * Created Date: Jul 16, 2019
 *
 * Description : 60290081 Encap channel binder
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSdhChannel.h"
#include "AtPdhNxDs0.h"
#include "AtHdlcChannel.h"
#include "../../../../generic/encap/AtEncapChannelInternal.h"
#include "../../../default/binder/ThaEncapBinderInternal.h"
#include "../../../default/man/ThaDevice.h"
#include "../../../default/map/ThaModuleMap.h"
#include "../../Tha60210011/map/Tha60210011ModuleMap.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290081EncapBinder
    {
    tThaEncapBinder super;
    }tTha60290081EncapBinder;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtEncapBinderMethods  m_AtEncapBinderOverride;
static tThaEncapBinderMethods m_ThaEncapBinderOverride;

/* Save super implementation */
static const tAtEncapBinderMethods *m_AtEncapBinderMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleMap ModuleMap(AtEncapBinder self)
    {
    return (ThaModuleMap)AtDeviceModuleGet(AtEncapBinderDeviceGet(self), cThaModuleMap);
    }

static uint32 De1EncapHwIdAllocate(AtEncapBinder self, AtPdhDe1 de1)
    {
    return Tha60210011ModuleMapDe1EncapHwIdAllocate(ModuleMap(self), de1);
    }

static uint32 De3EncapHwIdAllocate(AtEncapBinder self, AtPdhDe3 de3)
    {
    return Tha60210011ModuleMapDe3EncapHwIdAllocate(ModuleMap(self), de3);
    }

static uint32 NxDS0EncapHwIdAllocate(AtEncapBinder self, AtPdhNxDS0 ds0)
    {
    return Tha60210011ModuleMapNxDs0EncapHwIdAllocate(ModuleMap(self), ds0);
    }

static uint32 AuVcEncapHwIdAllocate(AtEncapBinder self, AtSdhVc auVc)
    {
    return Tha60210011ModuleMapHoLineVcEncapHwIdAllocate(ModuleMap(self), auVc);
    }

static uint32 Vc1xEncapHwIdAllocate(AtEncapBinder self, AtSdhVc vc1x)
    {
    return Tha60210011ModuleMapLoLineVcEncapHwIdAllocate(ModuleMap(self), vc1x);
    }

static uint32 Tu3VcEncapHwIdAllocate(AtEncapBinder self, AtSdhVc tu3Vc)
    {
    return Tha60210011ModuleMapLoLineVcEncapHwIdAllocate(ModuleMap(self), tu3Vc);
    }

static void EncapHwIdDeallocate(AtEncapBinder self, uint32 hwId)
    {
    eBool isLoEncap = cAtTrue;  /* FIXME: need to find the way to have these indication */
    uint8 slice = 0;
    Tha60210011ModuleMapEncapHwIdDeallocate(ModuleMap(self), isLoEncap, slice, hwId);
    }

static eAtRet BindConcateGroupToEncapChannel(AtEncapBinder self, AtChannel concateGroup, AtEncapChannel encapChannel)
    {
    AtUnused(self);

    if (encapChannel)
        AtEncapChannelPhysicalChannelSet(encapChannel, concateGroup);
    else
        AtEncapChannelPhysicalChannelSet(concateGroup->boundEncapChannel, NULL);

    return cAtOk;
    }

static eBool ShouldAllocateHwBlockOnBinding(ThaEncapBinder self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void OverrideAtEncapBinder(AtEncapBinder self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEncapBinderMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEncapBinderOverride, mMethodsGet(self), sizeof(m_AtEncapBinderOverride));

        mMethodOverride(m_AtEncapBinderOverride, De1EncapHwIdAllocate);
        mMethodOverride(m_AtEncapBinderOverride, De3EncapHwIdAllocate);
        mMethodOverride(m_AtEncapBinderOverride, NxDS0EncapHwIdAllocate);
        mMethodOverride(m_AtEncapBinderOverride, Vc1xEncapHwIdAllocate);
        mMethodOverride(m_AtEncapBinderOverride, Tu3VcEncapHwIdAllocate);
        mMethodOverride(m_AtEncapBinderOverride, AuVcEncapHwIdAllocate);
        mMethodOverride(m_AtEncapBinderOverride, EncapHwIdDeallocate);
        mMethodOverride(m_AtEncapBinderOverride, BindConcateGroupToEncapChannel);
        }

    mMethodsSet(self, &m_AtEncapBinderOverride);
    }

static void OverrideThaEncapBinder(AtEncapBinder self)
    {
    ThaEncapBinder binder = (ThaEncapBinder)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEncapBinderOverride, mMethodsGet(binder), sizeof(m_ThaEncapBinderOverride));

        mMethodOverride(m_ThaEncapBinderOverride, ShouldAllocateHwBlockOnBinding);
        }

    mMethodsSet(binder, &m_ThaEncapBinderOverride);
    }

static void Override(AtEncapBinder self)
    {
    OverrideAtEncapBinder(self);
    OverrideThaEncapBinder(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081EncapBinder);
    }

static AtEncapBinder ObjectInit(AtEncapBinder self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaEncapBinderObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEncapBinder Tha60290081EncapBinder(AtDevice device)
    {
    static tTha60290081EncapBinder m_binder;
    static AtEncapBinder m_sharedBinder = NULL;
    if (m_sharedBinder == NULL)
        m_sharedBinder = ObjectInit((AtEncapBinder)&m_binder, device);
    return m_sharedBinder;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : Tha60290081ModuleEncap.h
 * 
 * Created Date: Jul 15, 2019
 *
 * Description : 60290081 Module ENCAP
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290081MODULEENCAP_H_
#define _THA60290081MODULEENCAP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleEncap.h"
#include "AtGfpChannel.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290081ModuleEncap * Tha60290081ModuleEncap;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 Tha60290081ModuleEncapNumLoGfpChannels(AtModuleEncap self);
uint32 Tha60290081ModuleEncapNumHoGfpChannels(AtModuleEncap self);
uint32 Tha60290081ModuleEncapGfpChannelLocalId(AtModuleEncap self, AtGfpChannel channel);
eBool Tha60290081ModuleEncapChannelIsHoGfpChannel(AtModuleEncap self, AtChannel channel);

uint32 Tha60290081ModuleEncapHoGfpBaseAddress(AtModuleEncap self);
uint32 Tha60290081ModuleEncapLoGfpBaseAddress(AtModuleEncap self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290081MODULEENCAP_H_ */


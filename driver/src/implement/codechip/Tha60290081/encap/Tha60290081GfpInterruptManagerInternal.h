/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : Tha60290081GfpInterruptManagerInternal.h
 * 
 * Created Date: Sep 20, 2019
 *
 * Description : 60290081 gfp interrupt manager
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290081GFPINTERRUPTMANAGERINTERNAL_H_
#define _THA60290081GFPINTERRUPTMANAGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/encap/gfp/ThaGfpInterruptManagerInternal.h"
#include "Tha60290081GfpInterruptManager.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290081HoGfpInterruptManager
    {
    tThaGfpInterruptManager super;
    }tTha60290081HoGfpInterruptManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtInterruptManager Tha60290081HoGfpInterruptManagerObjectInit(AtInterruptManager self, AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290081GFPINTERRUPTMANAGERINTERNAL_H_ */


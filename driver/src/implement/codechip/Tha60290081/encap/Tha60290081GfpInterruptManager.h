/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : Tha60290081GfpInterruptManager.h
 * 
 * Created Date: Sep 11, 2019
 *
 * Description : 60290081 GFP interrupt manager
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290081GFPINTERRUPTMANAGER_H_
#define _THA60290081GFPINTERRUPTMANAGER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290081HoGfpInterruptManager * Tha60290081HoGfpInterruptManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtInterruptManager Tha60290081HoGfpInterruptManagerNew(AtModule module);
AtInterruptManager Tha60290081LoGfpInterruptManagerNew(AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290081GFPINTERRUPTMANAGER_H_ */


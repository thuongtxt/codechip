/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : Tha60290081HoGfpInterruptManager.c
 *
 * Created Date: Sep 11, 2019
 *
 * Description : 60290081 HO GFP channel interrupt manager implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaIpCoreInternal.h"
#include "Tha60290081GfpInterruptManagerInternal.h"
#include "../man/Tha60290081IntrReg.h"
#include "Tha60290081GfpInterruptManager.h"
#include "Tha60290081ModuleEncap.h"
#include "Tha60290081ModuleEncapGfpReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60290081HoGfpInterruptManager)self)
#define mManager(self) ((ThaInterruptManager)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtInterruptManagerMethods       m_AtInterruptManagerOverride;
static tThaInterruptManagerMethods      m_ThaInterruptManagerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleEncap ModuleEncap(AtInterruptManager self)
    {
    return (AtModuleEncap)AtInterruptManagerModuleGet(self);
    }

static void InterruptProcess(AtInterruptManager self, uint32 glbIntr, AtIpCore ipCore)
    {
    AtHal hal = AtIpCoreHalGet(ipCore);
    uint32 baseAddress = ThaInterruptManagerBaseAddress(mManager(self));
    uint32 glbIntrEn = AtHalRead(hal, baseAddress + cAf6Reg_cfgglbintenpen);
    uint32 glbIntrOR = AtHalRead(hal, baseAddress + cAf6Reg_upglbintpen);
    uint8 group;
    AtUnused(glbIntr);

    glbIntrOR &= glbIntrEn;
    for (group = 0; group < 8; group++)
        {
        if (glbIntrOR & cIteratorMask(group))
            {
            uint32 vcgIntrOR =  AtHalRead(hal, baseAddress + cAf6Reg_upsegpen + group);
            uint8 gfpInGroup;

            for (gfpInGroup = 0; gfpInGroup < 16; gfpInGroup++)
                {
                if (vcgIntrOR & cIteratorMask(gfpInGroup))
                    {
                    uint16 gfpId = (uint16)((group << 4) + gfpInGroup);
                    AtGfpChannel encapChannel = ThaGfpInterruptManagerGfpChannelFromHwIdGet((ThaGfpInterruptManager)self, gfpId);

                    if (encapChannel)
                        AtChannelInterruptProcess((AtChannel)encapChannel, 0);
                    }
                }
            }
        }
    }

static void InterruptHwEnable(ThaInterruptManager self, eBool enable, AtIpCore ipCore)
    {
    AtHal hal = AtIpCoreHalGet(ipCore);
    uint32 baseAddress = ThaInterruptManagerBaseAddress(mManager(self));

    AtHalWrite(hal, baseAddress + cAf6Reg_cfgglbintenpen, (enable) ? cBit7_0 : 0);
    ThaIpCoreEncapHwInterruptEnable((ThaIpCore)ipCore, enable);
    }

static uint32 BaseAddress(ThaInterruptManager self)
    {
    return Tha60290081ModuleEncapHoGfpBaseAddress(ModuleEncap((AtInterruptManager)self));
    }

static eBool HasInterrupt(AtInterruptManager self, uint32 glbIntr, AtHal hal)
    {
    AtUnused(self);
    AtUnused(hal);
    return (glbIntr & cAf6_global_interrupt_status_GfpHOIntStatus_Mask) ? cAtTrue : cAtFalse;
    }

static void OverrideAtInterruptManager(AtInterruptManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtInterruptManagerOverride, mMethodsGet(self), sizeof(m_AtInterruptManagerOverride));

        mMethodOverride(m_AtInterruptManagerOverride, InterruptProcess);
        mMethodOverride(m_AtInterruptManagerOverride, HasInterrupt);
        }

    mMethodsSet(self, &m_AtInterruptManagerOverride);
    }

static void OverrideThaInterruptManager(AtInterruptManager self)
    {
    ThaInterruptManager manager = (ThaInterruptManager)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInterruptManagerOverride, mMethodsGet(manager), sizeof(m_ThaInterruptManagerOverride));

        mMethodOverride(m_ThaInterruptManagerOverride, BaseAddress);
        mMethodOverride(m_ThaInterruptManagerOverride, InterruptHwEnable);
        }

    mMethodsSet(manager, &m_ThaInterruptManagerOverride);
    }

static void Override(AtInterruptManager self)
    {
    OverrideAtInterruptManager(self);
    OverrideThaInterruptManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081HoGfpInterruptManager);
    }

AtInterruptManager Tha60290081HoGfpInterruptManagerObjectInit(AtInterruptManager self, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaGfpInterruptManagerObjectInit(self, module) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInterruptManager Tha60290081HoGfpInterruptManagerNew(AtModule module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInterruptManager newManager = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newManager == NULL)
        return NULL;

    /* Construct it */
    return Tha60290081HoGfpInterruptManagerObjectInit(newManager, module);
    }

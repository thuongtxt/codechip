/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Encap
 *
 * File        : Tha60290081GfpChannel.c
 *
 * Created Date: Jul 9, 2019
 *
 * Description : 60290081 GFP-F channel implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/concate/AtConcateGroupInternal.h"
#include "../../../default/encap/gfp/ThaGfpChannelInternal.h"
#include "../../../default/pmc/ThaModulePmc.h"
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60210012/mpeg/Tha60210012ModuleMpeg.h"
#include "../pmc/Tha60290081ModulePmc.h"
#include "Tha60290081ModuleEncap.h"
#include "Tha60290081ModuleEncapGfpReg.h"
#include "Tha60290081GfpChannel.h"

/*--------------------------- Define -----------------------------------------*/
#define cGfpTypeHeaderPtiMask       cBit15_13
#define cGfpTypeHeaderPtiShift             13
#define cGfpTypeHeaderPfiMask          cBit12
#define cGfpTypeHeaderPfiShift             12
#define cGfpTypeHeaderExiMask        cBit11_8
#define cGfpTypeHeaderExiShift              8
#define cGfpTypeHeaderUpiMask         cBit7_0
#define cGfpTypeHeaderUpiShift              0

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60290081GfpChannel)self)

#define mDefaultOffset(self)        DefaultOffset((AtGfpChannel)self)

#define mSetIntrBitMask(alarmType_, defectMask_, enableMask_, hwMask_)         \
    do {                                                                       \
        if (defectMask_ & alarmType_)                                          \
            regVal = (enableMask_ & alarmType_) ? (regVal | hwMask_) : (regVal & (~hwMask_)); \
    } while(0)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290081GfpChannel
    {
    tThaGfpChannelV2 super;

    /* Private data */
    uint8 txCsfUpi;
    }tTha60290081GfpChannel;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods       m_AtObjectOverride;
static tAtChannelMethods      m_AtChannelOverride;
static tAtGfpChannelMethods   m_AtGfpChannelOverride;
static tAtEncapChannelMethods m_AtEncapChannelOverride;

/* Save super implementation */
static const tAtObjectMethods       *m_AtObjectMethods       = NULL;
static const tAtChannelMethods      *m_AtChannelMethods      = NULL;
static const tAtGfpChannelMethods   *m_AtGfpChannelMethods   = NULL;
static const tAtEncapChannelMethods *m_AtEncapChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleEncap ModuleEncap(AtGfpChannel self)
    {
    return (AtModuleEncap)AtChannelModuleGet((AtChannel)self);
    }

static eBool IsHoGfpChannel(AtGfpChannel self)
    {
    return Tha60290081ModuleEncapChannelIsHoGfpChannel(ModuleEncap(self), (AtChannel)self);
    }

static uint32 BaseAddress(AtGfpChannel self)
    {
    if (IsHoGfpChannel(self))
        return Tha60290081ModuleEncapHoGfpBaseAddress(ModuleEncap(self));

    return Tha60290081ModuleEncapLoGfpBaseAddress(ModuleEncap(self));
    }

static uint32 DefaultOffset(AtGfpChannel self)
    {
    return BaseAddress(self) + AtChannelHwIdGet((AtChannel)self);
    }

static eAtModuleEncapRet TxFcsEnable(AtGfpChannel self, eBool enable)
    {
    uint32 regAddr = cAf6Reg_upen_ecap_c + DefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);

    mRegFieldSet(regVal, cAf6_upen_ecap_c_cfgipfcs_ecap_, mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);
    return cAtOk;
    }

static eBool TxFcsIsEnabled(AtGfpChannel self)
    {
    uint32 regAddr = cAf6Reg_upen_ecap_c + DefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);

    return mRegField(regVal, cAf6_upen_ecap_c_cfgipfcs_ecap_) ? cAtTrue : cAtFalse;
    }

static eAtModuleEncapRet PayloadScrambleEnable(AtGfpChannel self, eBool enable)
    {
    uint32 offset = DefaultOffset(self);
    uint32 regAddr;
    uint32 regVal;

    /* Encap */
    regAddr = cAf6Reg_upen_ecap_c + offset;
    regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
    mRegFieldSet(regVal, cAf6_upen_ecap_c_cfgiescm_ecap_, mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);

    /* Decap */
    regAddr = cAf6Reg_upen_dcap_c + offset;
    regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
    mRegFieldSet(regVal, cAf6_upen_dcap_c_cfgiescm_dcap_, mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);

    return cAtOk;
    }

static eBool PayloadScrambleIsEnabled(AtGfpChannel self)
    {
    uint32 offset = DefaultOffset(self);
    uint32 regAddr;
    uint32 regVal;

    /* Encap */
    regAddr = cAf6Reg_upen_ecap_c + offset;
    regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
    if (!mRegField(regVal, cAf6_upen_ecap_c_cfgiescm_ecap_))
        return cAtFalse;

    /* Decap */
    regAddr = cAf6Reg_upen_dcap_c + offset;
    regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
    if (!mRegField(regVal, cAf6_upen_dcap_c_cfgiescm_dcap_))
        return cAtFalse;

    return cAtTrue;
    }

static eAtModuleEncapRet TxUpiSet(AtGfpChannel self, uint8 txUpi)
    {
    uint32 regAddr = cAf6Reg_upen_ecap_c + DefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);

    mRegFieldSet(regVal, cAf6_upen_ecap_c_cfgipupi_ecap_, txUpi);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);
    return cAtOk;
    }

static uint8 TxUpiGet(AtGfpChannel self)
    {
    uint32 regAddr = cAf6Reg_upen_ecap_c + DefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);

    return (uint8)mRegField(regVal, cAf6_upen_ecap_c_cfgipupi_ecap_);
    }

static eAtModuleEncapRet ExpectedUpiSet(AtGfpChannel self, uint8 expectedUpi)
    {
    uint32 regAddr = cAf6Reg_upen_dcap_c + DefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);

    mRegFieldSet(regVal, cAf6_upen_dcap_c_cfgipupi_dcap_, expectedUpi);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);
    return cAtOk;
    }

static uint8 ExpectedUpiGet(AtGfpChannel self)
    {
    uint32 regAddr = cAf6Reg_upen_dcap_c + DefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);

    return (uint8)mRegField(regVal, cAf6_upen_dcap_c_cfgipupi_dcap_);
    }

static eAtRet TxCmfEnable(AtGfpChannel self, eBool enable)
    {
    uint32 regAddr = cAf6Reg_upen_eoam_master_Base + DefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
    mRegFieldSet(regVal, cAf6_upen_eoam_master_icmfenaf_, enable ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);

    return cAtOk;
    }

static eBool TxCmfIsEnabled(AtGfpChannel self)
    {
    uint32 regAddr = cAf6Reg_upen_eoam_master_Base + DefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
    return mRegField(regVal, cAf6_upen_eoam_master_icmfenaf_) ? cAtTrue : cAtFalse;
    }

static uint16 PayloadHeaderBuild(uint8 pti, uint8 pfi, uint8 exi, uint8 upi)
    {
    uint32 value = 0;
    mRegFieldSet(value, cGfpTypeHeaderPti, pti);
    mRegFieldSet(value, cGfpTypeHeaderPfi, pfi);
    mRegFieldSet(value, cGfpTypeHeaderExi, exi);
    mRegFieldSet(value, cGfpTypeHeaderUpi, upi);
    return (uint16)value;
    }

static eAtRet TxOamPayloadHeaderSet(AtGfpChannel self, uint32 localAddress, uint8 txPti, uint8 txPfi, uint8 txUpi)
    {
    uint16 typeHeader = PayloadHeaderBuild(txPti, txPfi, AtGfpChannelTxExiGet(self), txUpi);
    uint32 regAddr = localAddress + DefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
    mRegFieldSet(regVal, cAf6_upen_ecmf_header_thec_cmf_, typeHeader);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);

    return cAtOk;
    }

static eAtRet TxOamCoreHeaderSet(AtGfpChannel self, uint32 localAddress, uint16 txPli)
    {
    uint32 regAddr = localAddress + DefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
    mRegFieldSet(regVal, cAf6_upen_ecmf_header_chec_cmf_, txPli);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);

    return cAtOk;
    }

static eAtRet TxCmfLengthSet(AtGfpChannel self, uint16 txPli)
    {
    return TxOamCoreHeaderSet(self, cAf6Reg_upen_ecmf_header_Base, txPli);
    }

static eAtRet TxCmfUpiSet(AtGfpChannel self, uint8 txUpi)
    {
    static const uint8 cGfpPtiForCmfMesg = 0x4;
    static const uint8 cGfpPfiForCmfMesg = 0;
    return TxOamPayloadHeaderSet(self, cAf6Reg_upen_ecmf_header_Base, cGfpPtiForCmfMesg, cGfpPfiForCmfMesg, txUpi);
    }

static eAtModuleEncapRet TxCsfEnable(AtGfpChannel self, eBool enable)
    {
    static const uint16 cGfpPliForCsfMesg = 4;
    eAtRet ret = cAtOk;

    if (enable)
        {
        /* Apply UPI value from database to CMF OAM buffer */
        ret |= AtGfpChannelTxCsfUpiSet(self, AtGfpChannelTxCsfUpiGet(self));
        ret |= TxCmfLengthSet(self, cGfpPliForCsfMesg);
        }

    ret |= TxCmfEnable(self, enable);

    return ret;
    }

static eBool TxCsfIsEnabled(AtGfpChannel self)
    {
    return TxCmfIsEnabled(self);
    }

static eAtModuleEncapRet TxCsfUpiSet(AtGfpChannel self, uint8 txUpi)
    {
    eAtRet ret = cAtOk;

    ret = TxCmfUpiSet(self, txUpi);
    if (ret != cAtOk)
        return ret;

    /* Because CMF buffer is shared for transmitting different CMF message types,
     * we cannot save user CSF UPI in hardware register. */
    mThis(self)->txCsfUpi = txUpi;

    return ret;
    }

static uint8 TxCsfUpiGet(AtGfpChannel self)
    {
    return mThis(self)->txCsfUpi;
    }

static eAtRet UnbindFlow(AtEncapChannel self)
    {
    eAtRet ret = cAtOk;

    if (AtEncapChannelBoundFlowGet(self) == NULL)
        return cAtOk;

    ret = AtChannelQueueEnable((AtChannel)self, cAtFalse);
    if (ret != cAtOk)
        return ret;

    return m_AtEncapChannelMethods->FlowBind(self, NULL);
    }

static eAtRet BindFlow(AtEncapChannel self, AtEthFlow flow)
    {
    eAtRet ret = m_AtEncapChannelMethods->FlowBind(self, flow);
    if (ret != cAtOk)
        return ret;

    return AtChannelQueueEnable((AtChannel)self, cAtTrue);
    }

static eAtModuleEncapRet FlowBind(AtEncapChannel self, AtEthFlow flow)
    {
    if (flow)
        return BindFlow(self, flow);

    return UnbindFlow(self);
    }

static uint32 HwIdGet(AtChannel self)
    {
    return Tha60290081ModuleEncapGfpChannelLocalId(ModuleEncap((AtGfpChannel)self), (AtGfpChannel)self);
    }

static eAtRet DecTHecCorrectionEnable(AtGfpChannel self, eBool enable)
    {
    uint32 regAddr = cAf6Reg_upen_dcap_c + DefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);

    mRegFieldSet(regVal, cAf6_upen_dcap_c_cfgithec_dcap_, mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);
    return cAtOk;
    }

static eAtRet DefaultSet(AtGfpChannel self)
    {
    eAtRet ret;

    ret  = AtGfpChannelTxFcsEnable(self, cAtTrue);
    ret |= AtGfpChannelPayloadScrambleEnable(self, cAtTrue);
    ret |= DecTHecCorrectionEnable(self, cAtTrue);

    return ret;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return DefaultSet((AtGfpChannel)self);
    }

static eAtRet Debug(AtChannel self)
    {
    return m_AtChannelMethods->Debug(self);
    }

static eAtRet Enable(AtChannel self, eBool enable)
    {
    AtUnused(self);
    return enable ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool IsEnabled(AtChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static ThaModulePmc ModulePmc(AtChannel self)
    {
    return (ThaModulePmc)AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModulePmc);
    }

static eBool CounterIsSupported(AtChannel self, uint16 counterType)
    {
    AtUnused(self);
    switch (counterType)
        {
        case cAtGfpChannelCounterTypeTxBytes            : return cAtTrue;
        case cAtGfpChannelCounterTypeTxFrames           : return cAtTrue;
        case cAtGfpChannelCounterTypeTxIdleFrames       : return cAtTrue;
        case cAtGfpChannelCounterTypeTxCmf              : return cAtTrue;
        case cAtGfpChannelCounterTypeTxCsf              : return cAtTrue;

        case cAtGfpChannelCounterTypeRxBytes            : return cAtTrue;
        case cAtGfpChannelCounterTypeRxFrames           : return cAtTrue;
        case cAtGfpChannelCounterTypeRxIdleFrames       : return cAtTrue;
        case cAtGfpChannelCounterTypeRxCmf              : return cAtTrue;
        case cAtGfpChannelCounterTypeRxCHecCorrected    : return cAtTrue;
        case cAtGfpChannelCounterTypeRxCHecError        : return cAtTrue;
        case cAtGfpChannelCounterTypeRxTHecCorrected    : return cAtTrue;
        case cAtGfpChannelCounterTypeRxTHecError        : return cAtTrue;
        case cAtGfpChannelCounterTypeRxEHecError        : return cAtTrue;
        case cAtGfpChannelCounterTypeRxFcsError         : return cAtTrue;
        case cAtGfpChannelCounterTypeRxDrops            : return cAtTrue;
        case cAtGfpChannelCounterTypeRxUpm              : return cAtTrue;

        default:
            return cAtFalse;
        }
    }

static uint32 PmcCounterRead(AtChannel self, uint16 counterType, eBool r2c)
    {
    ThaModulePmc modulePmc = ModulePmc(self);
    AtGfpChannel channel = (AtGfpChannel)self;

    switch (counterType)
        {
        case cAtGfpChannelCounterTypeTxBytes:           return ThaModulePmcGfpTxBytesGet(modulePmc, channel, r2c);
        case cAtGfpChannelCounterTypeTxFrames:          return ThaModulePmcGfpTxFramesGet(modulePmc, channel, r2c);
        case cAtGfpChannelCounterTypeTxIdleFrames:      return ThaModulePmcGfpTxIdleFramesGet(modulePmc, channel, r2c);
        case cAtGfpChannelCounterTypeTxCmf:             return ThaModulePmcGfpTxCmfGet(modulePmc, channel, r2c);
        case cAtGfpChannelCounterTypeTxCsf:             return ThaModulePmcGfpTxCsfGet(modulePmc, channel, r2c);

        case cAtGfpChannelCounterTypeRxBytes:           return ThaModulePmcGfpRxBytesGet(modulePmc, channel, r2c);
        case cAtGfpChannelCounterTypeRxFrames:          return ThaModulePmcGfpRxFramesGet(modulePmc, channel, r2c);
        case cAtGfpChannelCounterTypeRxIdleFrames:      return ThaModulePmcGfpRxIdleFramesGet(modulePmc, channel, r2c);
        case cAtGfpChannelCounterTypeRxCmf:             return ThaModulePmcGfpRxCmfGet(modulePmc, channel, r2c);
        case cAtGfpChannelCounterTypeRxCHecCorrected:   return ThaModulePmcGfpRxCHecCorrected(modulePmc, channel, r2c);
        case cAtGfpChannelCounterTypeRxCHecError:       return ThaModulePmcGfpRxCHecUncorrErrorGet(modulePmc, channel, r2c);
        case cAtGfpChannelCounterTypeRxTHecCorrected:   return ThaModulePmcGfpRxTHecCorrected(modulePmc, channel, r2c);
        case cAtGfpChannelCounterTypeRxTHecError:       return ThaModulePmcGfpRxTHecUncorrErrorGet(modulePmc, channel, r2c);
        case cAtGfpChannelCounterTypeRxFcsError:        return ThaModulePmcGfpRxFcsError(modulePmc, channel, r2c);
        case cAtGfpChannelCounterTypeRxDrops:           return ThaModulePmcGfpRxDrops(modulePmc, channel, r2c);
        case cAtGfpChannelCounterTypeRxUpm:             return ThaModulePmcGfpRxUpmGet(modulePmc, channel, r2c);
        case cAtGfpChannelCounterTypeRxEHecError:       return ThaModulePmcGfpRxEHecErrorGet(modulePmc, channel, r2c);

        default:
            break;
        }

    return 0;
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    return PmcCounterRead(self, counterType, cAtFalse);
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    return PmcCounterRead(self, counterType, cAtTrue);
    }

static Tha60210012ModuleMpeg ModuleMpeg(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel) self);
    return (Tha60210012ModuleMpeg)AtDeviceModuleGet(device, cThaModuleMpeg);
    }

static eAtRet QueueEnable(AtChannel self, eBool enable)
    {
    eAtRet ret = cAtOk;
    uint32 vcgId = AtChannelIdGet(self);
    Tha60210012ModuleMpeg moduleMpeg = ModuleMpeg(self);

    if (enable)
        {
        ret |= Tha60210012ModuleMpegVcgQueueEnable(moduleMpeg, vcgId, cAtFalse);
        ret |= Tha60210012ModuleMpegQueueVcgPrioritySet(moduleMpeg, vcgId, cThaMpegQueuePriorityLowest);
        ret |= Tha60210012ModuleMpegVcgQueueEnable(moduleMpeg, vcgId, cAtTrue);
        }
    else
        {
        ret |= Tha60210012ModuleMpegVcgQueueEnable(moduleMpeg, vcgId, cAtFalse);
        Tha60210012ModuleMpegQueueWaitBufferIsEmpty(moduleMpeg);
        ret |= Tha60210012ModuleMpegQueueVcgPrioritySet(moduleMpeg, vcgId, cThaMpegQueuePriorityUnknown);
        }

    return ret;
    }

static void WillDelete(AtObject self)
    {
    AtChannel phyChannel = AtEncapChannelBoundPhyGet((AtEncapChannel)self);
    if (phyChannel)
        {
        AtEncapChannelPhysicalChannelSet((AtEncapChannel)self, NULL);
        AtChannelBoundEncapChannelSet(phyChannel, NULL);
        }

    m_AtObjectMethods->WillDelete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60290081GfpChannel object = (Tha60290081GfpChannel)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeUInt(txCsfUpi);
    }

static uint32 DefectHw2Sw(uint32 regVal)
    {
    uint32 alarm = 0x0;

    if (regVal & (cAf6_upchstapen_c_dlos_status_Mask|cAf6_upchstapen_c_dloc_status_Mask))
        alarm |= cAtGfpChannelAlarmCsf;

    if (regVal & cAf6_upchstapen_c_dlfd_status_Mask)
        alarm |= cAtGfpChannelAlarmLfd;

    return alarm;
    }

static uint32 HwDefect2Clear(AtChannel self)
    {
    AtUnused(self);
    return (cAf6_upchstapen_c_dlos_status_Mask|
            cAf6_upchstapen_c_dloc_status_Mask|
            cAf6_upchstapen_c_dlfd_status_Mask);
    }

static uint32 DefectHistoryReadAndClear(AtChannel self, eBool clear)
    {
    uint32 regAddr = cAf6Reg_upchstypen + mDefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleEncap);
    uint32 alarm = DefectHw2Sw(regVal);

    if (clear)
        mChannelHwWrite(self, regAddr, HwDefect2Clear(self), cAtModuleEncap);

    return alarm;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    return DefectHistoryReadAndClear(self, cAtFalse);
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    return DefectHistoryReadAndClear(self, cAtTrue);
    }

static uint32 DefectGet(AtChannel self)
    {
    uint32 regAddr = cAf6Reg_upchstapen + mDefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleEncap);
    return DefectHw2Sw(regVal);
    }

static eBool AlarmIsSupported(AtChannel self, uint32 alarmType)
    {
    uint32 supportedDefects = AtChannelSupportedInterruptMasks(self);
    return (alarmType & supportedDefects) ? cAtTrue : cAtFalse;
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
    static uint32 supportedMasks = (cAtGfpChannelAlarmCsf |
                                    cAtGfpChannelAlarmLfd);
    AtUnused(self);
    return supportedMasks;
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    uint32 regAddr = cAf6Reg_upchcfgpen + mDefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);

    mSetIntrBitMask(cAtGfpChannelAlarmCsf, defectMask, enableMask, (cAf6_upchstapen_c_dlos_status_Mask|cAf6_upchstapen_c_dloc_status_Mask));
    mSetIntrBitMask(cAtGfpChannelAlarmLfd, defectMask, enableMask, cAf6_upchstapen_c_dlfd_status_Mask);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    uint32 regAddr = cAf6Reg_upchcfgpen + mDefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);
    return DefectHw2Sw(regVal);
    }

static eBool CanBindPhyChannel(AtEncapChannel self, AtChannel phyChannel)
    {
    /* Hard-wire 1-to-1 mapping to physical VCG */
    if ((phyChannel == NULL) || (phyChannel != AtEncapChannelBoundPhyGet(self)))
        return cAtFalse;

    return cAtTrue;
    }

static eAtRet HoGfpTxLowBandwidthEnable(AtGfpChannel self, eBool lowBandwidth)
    {
    uint32 regAddr = cAf6Reg_upen_ecap_c + DefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
    mRegFieldSet(regVal, cAf6_upen_ecap_c_cfgieau3_ecap_, mBoolToBin(lowBandwidth));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);

    return cAtOk;
    }

static eBool IsLowBandwidth(AtGfpChannel self)
    {
    AtConcateGroup group = (AtConcateGroup)AtEncapChannelBoundPhyGet((AtEncapChannel)self);

    /* VC3-1v/VC3-2v or VC3-1c/VC3-2c */
    if (AtConcateGroupSourceBandwidthInNumStsGet(group) < 3)
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet TxBandwidthUpdate(AtGfpChannel self)
    {
    if (IsHoGfpChannel(self))
        return HoGfpTxLowBandwidthEnable(self, IsLowBandwidth(self));
    return cAtOk;
    }

static eAtRet PhyBind(AtEncapChannel self, AtChannel phyChannel)
    {
    eAtRet ret = m_AtEncapChannelMethods->PhyBind(self, phyChannel);
    if (ret != cAtOk)
        return ret;

    return TxBandwidthUpdate((AtGfpChannel)self);
    }

static uint32 InterruptProcess(AtChannel self, uint32 offset)
    {
    uint32 gfpOffset = mDefaultOffset(self);
    uint32 intrMask = mChannelHwRead(self, cAf6Reg_upchcfgpen + gfpOffset, cAtModuleEncap);
    uint32 intrStatus = mChannelHwRead(self, cAf6Reg_upchstypen + gfpOffset, cAtModuleEncap);
    uint32 currentStatus;
    uint32 events = 0;
    uint32 defects = 0;
    AtUnused(offset);

    /* Filter un-masked interrupts */
    intrStatus &= intrMask;

    /* Clear interrupt, then get current status */
    mChannelHwWrite(self, cAf6Reg_upchstypen + gfpOffset, intrStatus, cAtModuleEncap);
    currentStatus = mChannelHwRead(self, cAf6Reg_upchstapen + gfpOffset, cAtModuleEncap);

    /* CSF */
    if (intrStatus & (cAf6_upchstapen_c_dlos_status_Mask|cAf6_upchstapen_c_dloc_status_Mask))
        {
        events |= cAtGfpChannelAlarmCsf;
        if (currentStatus & (cAf6_upchstapen_c_dlos_status_Mask|cAf6_upchstapen_c_dloc_status_Mask))
            defects |= cAtGfpChannelAlarmCsf;
        }

    /* LFD */
    if (intrStatus & cAf6_upchstapen_c_dlfd_status_Mask)
        {
        events |= cAtGfpChannelAlarmLfd;
        if (currentStatus & cAf6_upchstapen_c_dlfd_status_Mask)
            defects |= cAtGfpChannelAlarmLfd;
        }

    AtChannelAllAlarmListenersCall((AtChannel)self, events, defects);
    return intrStatus;
    }

static void OverrideAtObject(AtGfpChannel self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, mMethodsGet(object), sizeof(tAtObjectMethods));
        mMethodOverride(m_AtObjectOverride, WillDelete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtGfpChannel self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));
        mMethodOverride(m_AtChannelOverride, HwIdGet);
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, IsEnabled);
        mMethodOverride(m_AtChannelOverride, Debug);
        mMethodOverride(m_AtChannelOverride, CounterIsSupported);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, QueueEnable);
        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, AlarmIsSupported);
        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, InterruptProcess);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtEncapChannel(AtGfpChannel self)
    {
    AtEncapChannel channel = (AtEncapChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEncapChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEncapChannelOverride, m_AtEncapChannelMethods, sizeof(tAtEncapChannelMethods));

        mMethodOverride(m_AtEncapChannelOverride, FlowBind);
        mMethodOverride(m_AtEncapChannelOverride, PhyBind);
        mMethodOverride(m_AtEncapChannelOverride, CanBindPhyChannel);
        }

    mMethodsSet(channel, &m_AtEncapChannelOverride);
    }
    
static void OverrideAtGfpChannel(AtGfpChannel self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtGfpChannelMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtGfpChannelOverride, m_AtGfpChannelMethods, sizeof(m_AtGfpChannelOverride));

        mMethodOverride(m_AtGfpChannelOverride, TxFcsEnable);
        mMethodOverride(m_AtGfpChannelOverride, TxFcsIsEnabled);
        mMethodOverride(m_AtGfpChannelOverride, PayloadScrambleEnable);
        mMethodOverride(m_AtGfpChannelOverride, PayloadScrambleIsEnabled);
        mMethodOverride(m_AtGfpChannelOverride, TxUpiSet);
        mMethodOverride(m_AtGfpChannelOverride, TxUpiGet);
        mMethodOverride(m_AtGfpChannelOverride, ExpectedUpiSet);
        mMethodOverride(m_AtGfpChannelOverride, ExpectedUpiGet);
        mMethodOverride(m_AtGfpChannelOverride, TxCsfEnable);
        mMethodOverride(m_AtGfpChannelOverride, TxCsfIsEnabled);
        mMethodOverride(m_AtGfpChannelOverride, TxCsfUpiSet);
        mMethodOverride(m_AtGfpChannelOverride, TxCsfUpiGet);
        }

    mMethodsSet(self, &m_AtGfpChannelOverride);
    }

static void Override(AtGfpChannel self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtEncapChannel(self);
    OverrideAtGfpChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081GfpChannel);
    }

static AtGfpChannel ObjectInit(AtGfpChannel self, uint32 channelId, AtModuleEncap module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaGfpChannelV2ObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtGfpChannel Tha60290081GfpChannelNew(uint32 channelId, AtModuleEncap module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtGfpChannel newChannel = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newChannel == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newChannel, channelId, module);
    }

eAtRet Tha60290081GfpChannelTxBandwidthUpdate(AtGfpChannel self)
    {
    if (self)
        return TxBandwidthUpdate(self);
    return cAtErrorNullPointer;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : Tha60290081LoGfpInterruptManager.c
 *
 * Created Date: Sep 20, 2019
 *
 * Description : 60290081 LO GFP channel interrupt manager implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290081GfpInterruptManagerInternal.h"
#include "../man/Tha60290081IntrReg.h"
#include "Tha60290081GfpInterruptManager.h"
#include "Tha60290081ModuleEncap.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290081LoGfpInterruptManager
    {
    tTha60290081HoGfpInterruptManager super;
    }tTha60290081LoGfpInterruptManager;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtInterruptManagerMethods               m_AtInterruptManagerOverride;
static tThaInterruptManagerMethods              m_ThaInterruptManagerOverride;
static tThaGfpInterruptManagerMethods           m_ThaGfpInterruptManagerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleEncap ModuleEncap(AtInterruptManager self)
    {
    return (AtModuleEncap)AtInterruptManagerModuleGet(self);
    }

static uint32 BaseAddress(ThaInterruptManager self)
    {
    return Tha60290081ModuleEncapLoGfpBaseAddress(ModuleEncap((AtInterruptManager)self));
    }

static eBool HasInterrupt(AtInterruptManager self, uint32 glbIntr, AtHal hal)
    {
    AtUnused(self);
    AtUnused(hal);
    return (glbIntr & cAf6_global_interrupt_status_GfpLOIntStatus_Mask) ? cAtTrue : cAtFalse;
    }

static uint32 StartLoChannelId(ThaGfpInterruptManager self)
    {
    return Tha60290081ModuleEncapNumHoGfpChannels(ModuleEncap((AtInterruptManager)self));
    }

static AtGfpChannel GfpChannelFromHwIdGet(ThaGfpInterruptManager self, uint32 hwLocalId)
    {
    uint32 channelId = StartLoChannelId(self) + hwLocalId;
    return (AtGfpChannel)AtModuleEncapChannelGet(ModuleEncap((AtInterruptManager)self), (uint16)channelId);
    }

static void OverrideAtInterruptManager(AtInterruptManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtInterruptManagerOverride, mMethodsGet(self), sizeof(m_AtInterruptManagerOverride));

        mMethodOverride(m_AtInterruptManagerOverride, HasInterrupt);
        }

    mMethodsSet(self, &m_AtInterruptManagerOverride);
    }

static void OverrideThaInterruptManager(AtInterruptManager self)
    {
    ThaInterruptManager manager = (ThaInterruptManager)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInterruptManagerOverride, mMethodsGet(manager), sizeof(m_ThaInterruptManagerOverride));

        mMethodOverride(m_ThaInterruptManagerOverride, BaseAddress);
        }

    mMethodsSet(manager, &m_ThaInterruptManagerOverride);
    }

static void OverrideThaGfpInterruptManager(AtInterruptManager self)
    {
    ThaGfpInterruptManager manager = (ThaGfpInterruptManager)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaGfpInterruptManagerOverride, mMethodsGet(manager), sizeof(m_ThaGfpInterruptManagerOverride));

        mMethodOverride(m_ThaGfpInterruptManagerOverride, GfpChannelFromHwIdGet);
        }

    mMethodsSet(manager, &m_ThaGfpInterruptManagerOverride);
    }

static void Override(AtInterruptManager self)
    {
    OverrideAtInterruptManager(self);
    OverrideThaInterruptManager(self);
    OverrideThaGfpInterruptManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081LoGfpInterruptManager);
    }

static AtInterruptManager ObjectInit(AtInterruptManager self, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290081HoGfpInterruptManagerObjectInit(self, module) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInterruptManager Tha60290081LoGfpInterruptManagerNew(AtModule module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInterruptManager newManager = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newManager == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newManager, module);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : Tha60290081ModuleEncap.c
 *
 * Created Date: Jul 9, 2019
 *
 * Description : Tha60290081ModuleEncap implementations
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../../../default/encap/gfp/ThaGfpChannel.h"
#include "Tha60290081ModuleEncap.h"
#include "Tha60290081ModuleEncapInternal.h"
#include "Tha60290081ModuleEncapGfpReg.h"
#include "Tha60290081GfpInterruptManager.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60290081ModuleEncap)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods         m_AtModuleOverride;
static tAtModuleEncapMethods    m_AtModuleEncapOverride;
static tThaModuleEncapMethods   m_ThaModuleEncapOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtInterruptManager InterruptManagerCreate(AtModule self, uint32 managerIndex)
    {
    if (managerIndex == 0)
        return Tha60290081HoGfpInterruptManagerNew(self);

    return Tha60290081LoGfpInterruptManagerNew(self);
    }

static uint32 NumInterruptManagers(AtModule self)
    {
    AtUnused(self);
    return 2;
    }

static uint16 MaxChannelsGet(AtModuleEncap self)
    {
    AtUnused(self);
    return 256;
    }

static AtGfpChannel GfpChannelObjectCreate(AtModuleEncap self, uint32 channelId)
    {
    return Tha60290081GfpChannelNew(channelId, self);
    }

static uint32 NumLoGfpChannels(AtModuleEncap self)
    {
    AtUnused(self);
    return 128;
    }

static uint32 NumHoGfpChannels(AtModuleEncap self)
    {
    AtUnused(self);
    return 128;
    }

static eBool IsHoGfpChannel(AtModuleEncap self, AtChannel channel)
    {
    if (AtChannelIdGet(channel) < Tha60290081ModuleEncapNumHoGfpChannels(self))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 GfpChannelLocalId(AtModuleEncap self, AtGfpChannel channel)
    {
    uint32 channelId = AtChannelIdGet((AtChannel)channel);

    if (IsHoGfpChannel(self, (AtChannel)channel))
        return channelId;

    return (uint32)(channelId - Tha60290081ModuleEncapNumHoGfpChannels(self));
    }

static eAtRet HwGfpCsfClearThresholdSet(ThaModuleEncap self, uint32 baseAddress, uint32 seconds)
    {
    uint32 regAddr = baseAddress + cAf6Reg_upen_cycl_c;
    uint32 regVal  = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_upen_cycl_c_icsfdcyc_, seconds);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool GfpCsfClearThresholdIsSupported(ThaModuleEncap self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return ThaDeviceSupportFeatureFromVersion(device, 0x1, 0x2, 0x1006);
    }

static eAtRet GfpCsfClearThresholdSet(ThaModuleEncap self, uint32 seconds)
    {
    eAtRet ret;
    if (!GfpCsfClearThresholdIsSupported(self))
        return cAtOk;

    ret  = HwGfpCsfClearThresholdSet(self, Tha60290081ModuleEncapHoGfpBaseAddress((AtModuleEncap)self), seconds);
    ret |= HwGfpCsfClearThresholdSet(self, Tha60290081ModuleEncapLoGfpBaseAddress((AtModuleEncap)self), seconds);
    return ret;
    }

static eAtRet DefaultSet(ThaModuleEncap self)
    {
    static const uint32 cGfpCsfClearThresInSeconds = 3;
    return GfpCsfClearThresholdSet(self, cGfpCsfClearThresInSeconds);
    }

static void OverrideThaModuleEncap(AtModuleEncap self)
    {
    ThaModuleEncap module = (ThaModuleEncap)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleEncapOverride, mMethodsGet(module), sizeof(m_ThaModuleEncapOverride));

        mMethodOverride(m_ThaModuleEncapOverride, DefaultSet);
        }

    mMethodsSet(module, &m_ThaModuleEncapOverride);
    }

static void OverrideAtModuleEncap(AtModuleEncap self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleEncapOverride, mMethodsGet(self), sizeof(m_AtModuleEncapOverride));

        mMethodOverride(m_AtModuleEncapOverride, MaxChannelsGet);
        mMethodOverride(m_AtModuleEncapOverride, GfpChannelObjectCreate);
        }

    mMethodsSet(self, &m_AtModuleEncapOverride);
    }

static void OverrideAtModule(AtModuleEncap self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(module), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, NumInterruptManagers);
        mMethodOverride(m_AtModuleOverride, InterruptManagerCreate);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void Override(AtModuleEncap self)
    {
    OverrideThaModuleEncap(self);
    OverrideAtModuleEncap(self);
    OverrideAtModule(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081ModuleEncap);
    }

AtModuleEncap Tha60290081ModuleEncapObjectInit(AtModuleEncap self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleEncapObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleEncap Tha60290081ModuleEncapNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEncap newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha60290081ModuleEncapObjectInit(newModule, device);
    }

uint32 Tha60290081ModuleEncapNumLoGfpChannels(AtModuleEncap self)
    {
    if (self)
        return NumLoGfpChannels(self);
    return 0;
    }

uint32 Tha60290081ModuleEncapNumHoGfpChannels(AtModuleEncap self)
    {
    if (self)
        return NumHoGfpChannels(self);
    return 0;
    }

uint32 Tha60290081ModuleEncapGfpChannelLocalId(AtModuleEncap self, AtGfpChannel channel)
    {
    if (self)
        return GfpChannelLocalId(self, channel);
    return cInvalidUint32;
    }

eBool Tha60290081ModuleEncapChannelIsHoGfpChannel(AtModuleEncap self, AtChannel channel)
    {
    if (self)
        return IsHoGfpChannel(self, channel);
    return cAtFalse;
    }

uint32 Tha60290081ModuleEncapHoGfpBaseAddress(AtModuleEncap self)
    {
    AtUnused(self);
    return 0x1700000UL;
    }

uint32 Tha60290081ModuleEncapLoGfpBaseAddress(AtModuleEncap self)
    {
    AtUnused(self);
    return 0x1900000UL;
    }

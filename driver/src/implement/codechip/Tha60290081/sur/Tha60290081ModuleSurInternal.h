/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SUR
 * 
 * File        : Tha60290081ModuleSurInternal.h
 * 
 * Created Date: Jun 15, 2019
 *
 * Description : 60290081 Module SUR representation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290081MODULESURINTERNAL_H_
#define _THA60290081MODULESURINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290022/sur/Tha60290022ModuleSurInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290081ModuleSur
    {
    tTha60290022ModuleSur super;
    }tTha60290081ModuleSur;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60290081MODULESURINTERNAL_H_ */


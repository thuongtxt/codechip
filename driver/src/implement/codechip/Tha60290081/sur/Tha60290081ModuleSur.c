/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : Tha60290081ModuleSur.c
 *
 * Created Date: Jun 15, 2019
 *
 * Description : 60290081 Surveillance module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290081ModuleSurInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleSurMethods      m_AtModuleSurOverride;
static tThaModuleHardSurMethods m_ThaModuleHardSurOverride;

/* Save super implementation */
static const tThaModuleHardSurMethods *m_ThaModuleHardSurMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool FailureForwardingStatusIsSupported(ThaModuleHardSur self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool FailureHoldOnTimerConfigurable(AtModuleSur self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideAtModuleSur(AtModuleSur self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleSurOverride, mMethodsGet(self), sizeof(m_AtModuleSurOverride));

        mMethodOverride(m_AtModuleSurOverride, FailureHoldOnTimerConfigurable);
        }

    mMethodsSet(self, &m_AtModuleSurOverride);
    }

static void OverrideThaModuleHardSur(AtModuleSur self)
    {
    ThaModuleHardSur surModule = (ThaModuleHardSur)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleHardSurMethods = mMethodsGet(surModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleHardSurOverride, m_ThaModuleHardSurMethods, sizeof(m_ThaModuleHardSurOverride));

        mMethodOverride(m_ThaModuleHardSurOverride, FailureForwardingStatusIsSupported);
        }

    mMethodsSet(surModule, &m_ThaModuleHardSurOverride);
    }

static void Override(AtModuleSur self)
    {
    OverrideAtModuleSur(self);
    OverrideThaModuleHardSur(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081ModuleSur);
    }

static AtModuleSur ObjectInit(AtModuleSur self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022ModuleSurObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSur Tha60290081ModuleSurNew(AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSur module = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (module == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(module, device);
    }

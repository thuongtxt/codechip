/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAN
 *
 * File        : Tha60290081InterruptController.c
 *
 * Created Date: Aug 26, 2019
 *
 * Description : 60290081 interrupt controller implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290081InterruptControllerInternal.h"
#include "Tha60290081InterruptController.h"
#include "Tha60290081IntrReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tThaIpCoreMethods m_ThaIpCoreOverride;

/* Reference to super implementation */
static const tThaIpCoreMethods *m_ThaIpCoreImplement = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void ConcateHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    ThaIpCoreHwInterruptEnable(self, cAf6_global_interrupt_status_VcatHOIntStatus_Mask, enable);
    ThaIpCoreHwInterruptEnable(self, cAf6_global_interrupt_status_VcatLOIntStatus_Mask, enable);
    }

static void EncapHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    ThaIpCoreHwInterruptEnable(self, cAf6_global_interrupt_status_GfpHOIntStatus_Mask, enable);
    ThaIpCoreHwInterruptEnable(self, cAf6_global_interrupt_status_GfpLOIntStatus_Mask, enable);
    }

static eBool ConcateCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    AtUnused(self);
    return (intrStatus & (cAf6_global_interrupt_status_VcatHOIntStatus_Mask|
                          cAf6_global_interrupt_status_VcatLOIntStatus_Mask)) ? cAtTrue : cAtFalse;
    }

static eBool EncapCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    AtUnused(self);
    return (intrStatus & (cAf6_global_interrupt_status_GfpHOIntStatus_Mask|
                          cAf6_global_interrupt_status_GfpLOIntStatus_Mask)) ? cAtTrue : cAtFalse;
    }

static void OverrideThaIpCore(ThaIntrController self)
    {
    ThaIpCore core = (ThaIpCore)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaIpCoreImplement = mMethodsGet(core);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaIpCoreOverride, m_ThaIpCoreImplement, sizeof(m_ThaIpCoreOverride));

        mMethodOverride(m_ThaIpCoreOverride, ConcateHwInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, EncapHwInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, ConcateCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, EncapCauseInterrupt);
        }

    mMethodsSet(core, &m_ThaIpCoreOverride);
    }

static void Override(ThaIntrController self)
    {
    OverrideThaIpCore(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081InterruptController);
    }

static ThaIntrController ObjectInit(ThaIntrController self, AtIpCore ipCore)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022IntrControllerObjectInit(self, ipCore) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaIntrController Tha60290081IntrControllerNew(AtIpCore ipCore)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaIntrController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, ipCore);
    }

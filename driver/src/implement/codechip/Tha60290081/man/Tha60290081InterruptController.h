/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAN
 * 
 * File        : Tha60290081InterruptController.h
 * 
 * Created Date: Aug 26, 2019
 *
 * Description : 60290081 Interrupt controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290081INTERRUPTCONTROLLER_H_
#define _THA60290081INTERRUPTCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtIpCore.h"
#include "../../../default/man/intrcontroller/ThaIntrController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaIntrController Tha60290081IntrControllerNew(AtIpCore ipCore);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290081INTERRUPTCONTROLLER_H_ */


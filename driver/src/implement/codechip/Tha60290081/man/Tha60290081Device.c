/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAN
 *
 * File        : Tha60290081Device.c
 *
 * Created Date: May 30, 2019
 *
 * Description : 60290081 Device management module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/binder/ThaEncapBinder.h"
#include "../../../default/aps/ThaModuleHardAps.h"
#include "../cla/Tha60290081ModuleCla.h"
#include "../mpig/Tha60290081ModuleMpig.h"
#include "../mpeg/Tha60290081ModuleMpeg.h"
#include "../concate/Tha60290081ModuleConcate.h"
#include "Tha60290081DeviceInternal.h"
#include "Tha60290081InterruptController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define cAf6Reg_DebugMacLoopControl_Base        0x000013
#define cAf6_DebugMacLoopControl_PtpDis_Mask       cBit2
#define cAf6_DebugMacLoopControl_PtpDis_Shift          2

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceMethods          m_AtDeviceOverride;
static tThaDeviceMethods         m_ThaDeviceOverride;
static tTha60150011DeviceMethods m_Tha60150011DeviceOverride;
static tTha60210011DeviceMethods m_Tha60210011DeviceOverride;
static tTha60290021DeviceMethods m_Tha60290021DeviceOverride;
static tTha60290022DeviceMethods m_Tha60290022DeviceOverride;

/* Super implementation */
static const tAtDeviceMethods          *m_AtDeviceMethods          = NULL;
static const tTha60150011DeviceMethods *m_Tha60150011DeviceMethods = NULL;
static const tTha60210011DeviceMethods *m_Tha60210011DeviceMethods = NULL;
static const tTha60290022DeviceMethods *m_Tha60290022DeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    uint32 phyModule = moduleId;

    /* Public modules */
    if (moduleId == cAtModulePdh)       return (AtModule)Tha60290081ModulePdhNew(self);
    if (moduleId == cAtModulePw)        return (AtModule)Tha60290081ModulePwNew(self);
    if (moduleId == cAtModuleSdh)       return (AtModule)Tha60290081ModuleSdhNew(self);
    if (moduleId == cAtModuleSur)       return (AtModule)Tha60290081ModuleSurNew(self);
    if (moduleId == cAtModuleEncap)     return (AtModule)Tha60290081ModuleEncapNew(self);
    if (moduleId == cAtModuleConcate)   return (AtModule)Tha60290081ModuleConcateNew(self);
    if (moduleId == cAtModuleEth)       return (AtModule)Tha60290081ModuleEthNew(self);
    if (moduleId == cAtModulePrbs)      return (AtModule)Tha60290081ModulePrbsNew(self);
    if (moduleId == cAtModuleAps)       return (AtModule)Tha60290081ModuleApsNew(self);

    /* Private modules */
    if (phyModule == cThaModuleOcn)     return Tha60290081ModuleOcnNew(self);
    if (phyModule == cThaModuleMap)     return Tha60290081ModuleMapNew(self);
    if (phyModule == cThaModuleDemap)   return Tha60290081ModuleDemapNew(self);
    if (phyModule == cThaModulePwe)     return Tha60290081ModulePweNew(self);
    if (phyModule == cThaModulePda)     return Tha60290081ModulePdaNew(self);
    if (phyModule == cThaModulePoh)     return Tha60290081ModulePohNew(self);
    if (phyModule == cThaModulePmc)     return Tha60290081ModulePmcNew(self);
    if (phyModule == cThaModuleMpig)    return Tha60290081ModuleMpigNew(self);
    if (phyModule == cThaModuleMpeg)    return Tha60290081ModuleMpegNew(self);
    if (phyModule == cThaModuleCla)     return Tha60290081ModuleClaNew(self);
    if (phyModule == cThaModuleCos)     return Tha60290081ModuleCosNew(self);

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static const eAtModule *AllSupportedModulesGet(AtDevice self, uint8 *numModules)
    {
    static const eAtModule supportedModules[] = {cAtModulePdh,
                                                 cAtModulePw,
                                                 cAtModuleEth,
                                                 cAtModuleRam,
                                                 cAtModuleXc,
                                                 cAtModuleSdh,
                                                 cAtModuleSur,
                                                 cAtModulePktAnalyzer,
                                                 cAtModuleBer,
                                                 cAtModulePrbs,
                                                 cAtModuleClock,
                                                 cAtModuleAps,
                                                 cAtModulePtp,
                                                 cAtModuleEncap,
                                                 cAtModuleConcate};

    if (numModules)
        *numModules = mCount(supportedModules);

    AtUnused(self);
    return supportedModules;
    }

static eBool ModuleIsSupported(AtDevice self, eAtModule moduleId)
    {
    uint32 moduleValue = (uint32)moduleId;

    switch (moduleValue)
        {
        case cAtModuleEncap:    return cAtTrue;
        case cAtModuleConcate:  return cAtTrue;
        case cAtModulePpp:      return cAtTrue;
        case cThaModuleMpig:    return cAtTrue;
        case cThaModuleMpeg:    return cAtTrue;
        default:
            break;
        }

    return m_AtDeviceMethods->ModuleIsSupported(self, moduleId);
    }

static eBool SemIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 NumberOfMapLoPwPerSlice(AtDevice self)
    {
    AtUnused(self);
    return 1344;
    }

static eBool PtpSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool HasApsWtr3SecondResolution(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool HasSoftResetForSem(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool PwDccKbyteInterruptIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool ModuleHwReady(AtDevice self, eAtModule moduleId)
    {
    AtUnused(self);

    if ((moduleId == cAtModulePpp)     ||
        (moduleId == cAtModulePtp))
        return cAtFalse;

    return cAtTrue;
    }

static AtEncapBinder EncapBinder(AtDevice self)
    {
    return Tha60290081EncapBinder(self);
    }

static eAtRet DefaultSet(AtDevice self)
    {
    AtHal hal = AtDeviceIpCoreHalGet(self, 0);
    uint32 regVal = 0;

    mRegFieldSet(regVal, cAf6_DebugMacLoopControl_PtpDis_, 1);
    AtHalWrite(hal, cAf6Reg_DebugMacLoopControl_Base, regVal);

    return cAtOk;
    }

static eAtRet Init(AtDevice self)
    {
    eAtRet ret;

    ret = m_AtDeviceMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return DefaultSet(self);
    }

static ThaIntrController IntrControllerCreate(ThaDevice self, AtIpCore core)
    {
    AtUnused(self);
    return Tha60290081IntrControllerNew(core);
    }

static void VcatHwFlush(ThaDevice self)
    {
    Tha60290081ModuleConcateHwFlush((Tha60290081ModuleConcate)AtDeviceModuleGet((AtDevice)self, cAtModuleConcate));
    }

static void ClaHwFlush(ThaDevice self)
    {
    Tha60290081ModuleClaHwFlush((ThaModuleCla)AtDeviceModuleGet((AtDevice)self, cThaModuleCla));
    }

static eAtRet PweHwFlush(ThaDevice self)
    {
    /* PWE */
    ThaDeviceMemoryFlush(self, 0x0070000, 0x00714FF, 0x0);

    return cAtOk;
    }

static void HwFlush(Tha60150011Device self)
    {
    ThaDevice device = (ThaDevice)self;

    m_Tha60150011DeviceMethods->HwFlush(self);

    VcatHwFlush(device);
    ClaHwFlush(device);
    PweHwFlush(device);
    }

static void OverrideTha60290021Device(AtDevice self)
    {
    Tha60290021Device device = (Tha60290021Device)self;
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021DeviceOverride, mMethodsGet(device), sizeof(m_Tha60290021DeviceOverride));

        }

    mMethodsSet(device, &m_Tha60290021DeviceOverride);
    }

static void OverrideTha60290022Device(AtDevice self)
    {
    Tha60290022Device device = (Tha60290022Device)self;
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290022DeviceMethods = mMethodsGet(device);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290022DeviceOverride, m_Tha60290022DeviceMethods, sizeof(m_Tha60290022DeviceOverride));

        mMethodOverride(m_Tha60290022DeviceOverride, SemIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, NumberOfMapLoPwPerSlice);
        mMethodOverride(m_Tha60290022DeviceOverride, PtpSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, HasApsWtr3SecondResolution);
        mMethodOverride(m_Tha60290022DeviceOverride, HasSoftResetForSem);
        mMethodOverride(m_Tha60290022DeviceOverride, PwDccKbyteInterruptIsSupported);
        }

    mMethodsSet(device, &m_Tha60290022DeviceOverride);
    }

static void OverrideTha60210011Device(AtDevice self)
    {
    Tha60210011Device this = (Tha60210011Device)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011DeviceMethods = mMethodsGet(this);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011DeviceOverride, m_Tha60210011DeviceMethods, sizeof(m_Tha60210011DeviceOverride));

        }

    mMethodsSet(this, &m_Tha60210011DeviceOverride);
    }

static void OverrideTha60150011Device(AtDevice self)
    {
    Tha60150011Device device = (Tha60150011Device)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60150011DeviceMethods = mMethodsGet(device);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60150011DeviceOverride, m_Tha60150011DeviceMethods, sizeof(m_Tha60150011DeviceOverride));

        mMethodOverride(m_Tha60150011DeviceOverride, HwFlush);
        }

    mMethodsSet(device, &m_Tha60150011DeviceOverride);
    }

static void OverrideThaDevice(AtDevice self)
    {
    ThaDevice device = (ThaDevice)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDeviceOverride, mMethodsGet(device), sizeof(m_ThaDeviceOverride));

        mMethodOverride(m_ThaDeviceOverride, IntrControllerCreate);
        }

    mMethodsSet(device, &m_ThaDeviceOverride);
    }

static void OverrideAtDevice(AtDevice self)
    {
    AtDevice object = (AtDevice)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(m_AtDeviceOverride));

        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, AllSupportedModulesGet);
        mMethodOverride(m_AtDeviceOverride, ModuleIsSupported);
        mMethodOverride(m_AtDeviceOverride, ModuleHwReady);
        mMethodOverride(m_AtDeviceOverride, EncapBinder);
        mMethodOverride(m_AtDeviceOverride, Init);
        }

    mMethodsSet(object, &m_AtDeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtDevice(self);
    OverrideThaDevice(self);
    OverrideTha60150011Device(self);
    OverrideTha60210011Device(self);
    OverrideTha60290021Device(self);
    OverrideTha60290022Device(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081Device);
    }

AtDevice Tha60290081DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60291022DeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha60290081DeviceNew(AtDriver driver, uint32 productCode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDevice == NULL)
        return NULL;

    /* Construct it */
    return Tha60290081DeviceObjectInit(newDevice, driver, productCode);
    }

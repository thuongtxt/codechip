/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDA
 * 
 * File        : Tha60290081ModulePdaInternal.h
 * 
 * Created Date: Jun 18, 2019
 *
 * Description : 60290081 Module PDA representation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290081MODULEPDAINTERNAL_H_
#define _THA60290081MODULEPDAINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290022/pda/Tha60290022ModulePdaInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290081ModulePda
    {
    tTha60290022ModulePda super;
    }tTha60290081ModulePda;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60290081ModulePdaObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290081MODULEPDAINTERNAL_H_ */


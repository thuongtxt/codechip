/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDA
 *
 * File        : Tha60290081ModulePda.c
 *
 * Created Date: Jun 18, 2019
 *
 * Description : 60290081 Module PDA implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/concate/AtConcateGroupInternal.h"
#include "../../Tha60210011/pw/Tha60210011ModulePw.h"
#include "../encap/Tha60290081ModuleEncap.h"
#include "Tha60290081ModulePdaInternal.h"
#include "Tha60290081ModulePdaReg.h"
#include "Tha60290081ModulePda.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
/* 0-8191: 4 slice OC48 CES ID with formula OC48ID * 2048 + Slice Pseudowire ID */
#define cPdaTdmPwStartId    0
#define cPdaTdmPwNumIds     8192

/* 8320-8655: 336 Low Order ENCAP POS ID */
#define cPdaLoPosStartId    8320
#define cPdaLoPosNumIds     336

/* 8784-8911: 128 High Order ENCAP POS ID */
#define cPdaHoPosStartId    8784
#define cPdaHoPosNumIds     128

/* 8912: 192C CEP PW ID */
#define cPda192cPwStartId   (cPdaHoPosStartId + cPdaHoPosNumIds)
#define cPda192cPwNumIds    1

/* 8913-8928: 16 Port Eth Pass Through ID where only physical port#0 and port#8 can be 10G */
#define cPdaEthPassStartId  (cPda192cPwStartId + cPda192cPwNumIds)
#define cPdaEthPassNumIds   16

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePdaMethods         m_ThaModulePdaOverride;
static tTha60210011ModulePdaMethods m_Tha60210011ModulePdaOverride;
static tTha60290022ModulePdaMethods m_Tha60290022ModulePdaOverride;

/* Save super implementation */
static const tThaModulePdaMethods         *m_ThaModulePdaMethods = NULL;
static const tTha60210011ModulePdaMethods *m_Tha60210011ModulePdaMethods = NULL;
static const tTha60290022ModulePdaMethods *m_Tha60290022ModulePdaMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 HoLoOc48IdMask(Tha60210011ModulePda self)
    {
    AtUnused(self);
    return cAf6_ramjitbufcfg_PwSliceOc48Id_Mask;
    }

static uint32 HoLoOc48IdShift(Tha60210011ModulePda self)
    {
    AtUnused(self);
    return cAf6_ramjitbufcfg_PwSliceOc48Id_Shift;
    }

static uint32 PwCEPModeMask(Tha60210011ModulePda self)
    {
    AtUnused(self);
    return cAf6_ramjitbufcfg_PwCEPMode_Mask;
    }

static uint32 PwCEPModeShift(Tha60210011ModulePda self)
    {
    AtUnused(self);
    return cAf6_ramjitbufcfg_PwCEPMode_Shift;
    }

static uint32 PwLowDs0ModeMask(Tha60210011ModulePda self)
    {
    AtUnused(self);
    return cAf6_ramjitbufcfg_PwLowDs0Mode_Mask;
    }

static uint32 PwLowDs0ModeShift(Tha60210011ModulePda self)
    {
    AtUnused(self);
    return cAf6_ramjitbufcfg_PwLowDs0Mode_Shift;
    }

static uint32 PwLookupPwIDMask(Tha60210011ModulePda self)
    {
    AtUnused(self);
    return cAf6_ramlotdmlkupcfg_PwID_Mask;
    }

static uint8 PwLookupPwIDShift(Tha60210011ModulePda self)
    {
    AtUnused(self);
    return cAf6_ramlotdmlkupcfg_PwID_Shift;
    }

static uint32 LoTdmSmallDS0Control(Tha60210011ModulePda self)
    {
    AtUnused(self);
    return cAf6Reg_ramlotdmsmallds0control_Base;
    }

static uint32 TdmOc48Pwid(AtPw pw)
    {
    return AtChannelHwIdGet((AtChannel)pw);
    }

static uint32 PwTdmLowRateOffset(Tha60210011ModulePda self, AtPw pw)
    {
    uint8 slice;
    AtUnused(self);

    if (Tha60210011PwCircuitSliceAndHwIdInSliceGet(pw, &slice, NULL) != cAtOk)
        return cInvalidUint32;

    return (slice * 16384UL) + TdmOc48Pwid(pw);
    }

static uint32 TdmLookupControl(Tha60210011ModulePda self, AtPw pw)
    {
    return mMethodsGet(self)->LoTdmLookupCtrl(self, pw) +
           ThaModulePdaBaseAddress((ThaModulePda)self) +
           mMethodsGet(self)->PwLoTdmLookupOffset(self, pw);
    }

static eAtRet PwPdaTdmLookupEnable(ThaModulePda self, AtPw pw, eBool enable)
    {
    uint32 regAddr = TdmLookupControl((Tha60210011ModulePda)self, pw);
    uint32 regVal = mChannelHwRead(pw, regAddr, cThaModulePda);
    mRegFieldSet(regVal, cAf6_ramlotdmlkupcfg_PwLkEnable_, mBoolToBin(enable));
    mRegFieldSet(regVal, cAf6_ramlotdmlkupcfg_PwCesEnable_, mBoolToBin(enable));
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePda);
    return cAtOk;
    }

static eAtRet PwPdaTdmLookupModeSet(ThaModulePda self, AtPw pw, eBool isHoTdm, uint8 hwMasterSts)
    {
    uint32 regAddr = TdmLookupControl((Tha60210011ModulePda)self, pw);
    uint32 regVal = mChannelHwRead(pw, regAddr, cThaModulePda);
    mRegFieldSet(regVal, cAf6_ramlotdmlkupcfg_PwHoPathInd_, mBoolToBin(isHoTdm));
    mRegFieldSet(regVal, cAf6_ramlotdmlkupcfg_PwHomstsid_, hwMasterSts);
    mRegFieldSet(regVal, cAf6_ramlotdmlkupcfg_MlpppBundId_, 0);
    mRegFieldSet(regVal, cAf6_ramlotdmlkupcfg_MlpppEn_, 0);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePda);
    return cAtOk;
    }

static eAtRet PdaHoTdmLookupModeSet(ThaModulePda self, AtPw pw, AtSdhChannel sdhVc)
    {
    eAtRet ret;
    uint8 slice, stsInSlice;

    ret = ThaSdhChannel2HwMasterStsId(sdhVc, cThaModulePda, &slice, &stsInSlice);
    if (ret != cAtOk)
        return ret;

    return PwPdaTdmLookupModeSet(self, pw, cAtTrue, stsInSlice);
    }

static eAtRet PdaLoTdmLookupModeSet(ThaModulePda self, AtPw pw)
    {
    return PwPdaTdmLookupModeSet(self, pw, cAtFalse, 0);
    }

static eAtRet SAToPModeSet(ThaModulePda self, ThaPwAdapter adapter, AtChannel circuit)
    {
    eAtRet ret;
    uint8 channelType = AtPdhChannelTypeGet((AtPdhChannel)circuit);

    ret = m_ThaModulePdaMethods->SAToPModeSet(self, adapter, circuit);
    if (ret != cAtOk)
        return ret;

    if ((channelType == cAtPdhChannelTypeDs1) ||
        (channelType == cAtPdhChannelTypeE1))
        return PdaLoTdmLookupModeSet(self, (AtPw)adapter);

    return PdaHoTdmLookupModeSet(self, (AtPw)adapter, (AtSdhChannel)AtPdhChannelVcGet((AtPdhChannel)circuit));
    }

static eAtRet CepModeSet(ThaModulePda self, ThaPwAdapter adapter, AtSdhChannel sdhVc)
    {
    eAtRet ret;
    uint8 channelType = AtSdhChannelTypeGet(sdhVc);

    ret = m_ThaModulePdaMethods->CepModeSet(self, adapter, sdhVc);
    if (ret != cAtOk)
        return ret;

    if ((channelType == cAtSdhChannelTypeVc11) ||
        (channelType == cAtSdhChannelTypeVc12))
        return PdaLoTdmLookupModeSet(self, (AtPw)adapter);

    return PdaHoTdmLookupModeSet(self, (AtPw)adapter, sdhVc);
    }

static eAtRet CESoPModeSet(ThaModulePda self, ThaPwAdapter adapter)
    {
    eAtRet ret = m_ThaModulePdaMethods->CESoPModeSet(self, adapter);
    if (ret != cAtOk)
        return ret;

    return PdaLoTdmLookupModeSet(self, (AtPw)adapter);
    }

static uint32 TdmLookupOffset(Tha60290022ModulePda self, uint8 oc48Slice, uint32 tdmPwId)
    {
    AtUnused(self);
    return oc48Slice * 2048U + tdmPwId;
    }

static uint32 NumHwPwInOc48Slice(Tha60290022ModulePda self)
    {
    AtUnused(self);
    return 2048U;
    }

static uint32 Oc192cTdmModeCtrl(Tha60290022ModulePda self)
    {
    AtUnused(self);
    return cAf6Reg_ramtdmmodecfg_Base + cPda192cPwStartId;
    }

static uint32 Oc192cTdmLookupCtrl(Tha60290022ModulePda self)
    {
    AtUnused(self);
    return cAf6Reg_ramlotdmlkupcfg_Base + cPda192cPwStartId;
    }

static AtModuleEncap ModuleEncap(Tha60290081ModulePda self)
    {
    return (AtModuleEncap)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModuleEncap);
    }

static uint32 PdaFlowLookupControl(Tha60290081ModulePda self)
    {
    return cAf6Reg_ramlotdmlkupcfg_Base + ThaModulePdaBaseAddress((ThaModulePda)self);
    }

static uint32 GfpLookupOffset(Tha60290081ModulePda self, AtChannel channel)
    {
    static const uint32 cPdaStartLoGfpOffset = 8192U;
    static const uint32 cPdaStartHoGfpOffset = 8656U;

    if (Tha60290081ModuleEncapChannelIsHoGfpChannel(ModuleEncap(self), channel))
        return cPdaStartHoGfpOffset + AtChannelHwIdGet(channel);

    return cPdaStartLoGfpOffset + AtChannelHwIdGet(channel);
    }

static eAtRet PdaEthFlowLookupSet(Tha60290081ModulePda self, AtEthFlow flow, uint32 flowLkupOffset, AtChannel channel)
    {
    uint32 regAddr = PdaFlowLookupControl(self) + flowLkupOffset;
    uint32 regVal;

    regVal = mChannelHwRead(flow, regAddr, cThaModulePda);
    mRegFieldSet(regVal, cAf6_ramlotdmlkupcfg_PwCesEnable_, 0); /* Packet mode */
    mRegFieldSet(regVal, cAf6_ramlotdmlkupcfg_PwLkEnable_, 1);
    mRegFieldSet(regVal, cAf6_ramlotdmlkupcfg_PwID_, AtChannelIdGet(channel));
    mChannelHwWrite(flow, regAddr, regVal, cThaModulePda);

    return cAtOk;
    }

static eAtRet PdaEthFlowLookupReset(Tha60290081ModulePda self, AtEthFlow flow, uint32 flowLkupOffset)
    {
    uint32 regAddr = PdaFlowLookupControl(self) + flowLkupOffset;
    uint32 resetValue = cAf6_ramlotdmlkupcfg_PwID_Mask;
    mChannelHwWrite(flow, regAddr, resetValue, cThaModulePda);

    return cAtOk;
    }

static eAtRet PdaGfpEthFlowLookupSet(Tha60290081ModulePda self, AtEthFlow flow, AtChannel channel)
    {
    return PdaEthFlowLookupSet(self, flow, GfpLookupOffset(self, channel), channel);
    }

static eAtRet PdaGfpEthFlowLookupReset(Tha60290081ModulePda self, AtEthFlow flow, AtChannel channel)
    {
    return PdaEthFlowLookupReset(self, flow, GfpLookupOffset(self, channel));
    }

static uint32 PdaFlowModeCtrl(Tha60290081ModulePda self)
    {
    return cAf6Reg_ramtdmmodecfg_Base + ThaModulePdaBaseAddress((ThaModulePda)self);
    }

static uint8 PdaEosSignalType(AtConcateGroup vcg)
    {
    uint8 type = AtConcateGroupMemberTypeGet(vcg);

    switch (type)
        {
        case cAtConcateMemberTypeE3:      return 0;
        case cAtConcateMemberTypeDs3:     return 0;
        case cAtConcateMemberTypeVc3:     return 0;

        case cAtConcateMemberTypeVc4:     return 1;
        case cAtConcateMemberTypeVc4_nc:  return 2;
        case cAtConcateMemberTypeVc4_4c:  return 2;
        case cAtConcateMemberTypeVc4_16c: return 3;
        case cAtConcateMemberTypeVc4_64c: return 3;

        case cAtConcateMemberTypeVc11:    return 4;
        case cAtConcateMemberTypeVc12:    return 4;
        case cAtConcateMemberTypeE1:      return 4;
        case cAtConcateMemberTypeDs1:     return 4;

        default: return cInvalidUint8;
        }
    }

static eAtRet PdaEosEthFlowModeSet(Tha60290081ModulePda self, AtEthFlow flow, AtChannel channel)
    {
    static const uint32 cPdaModeEos = 0;
    AtConcateGroup vcg = (AtConcateGroup)AtEncapChannelBoundPhyGet((AtEncapChannel)channel);
    uint32 regAddr = PdaFlowModeCtrl(self) + GfpLookupOffset(self, channel);
    uint32 regVal = 0;

    mRegFieldSet(regVal, cAf6_ramtdmmodecfg_PDAMode_, cPdaModeEos);
    mRegFieldSet(regVal, cAf6_ramtdmmodecfg_PDASigType_, PdaEosSignalType(vcg));
    mChannelHwWrite(flow, regAddr, regVal, cThaModulePda);

    return cAtOk;
    }

static eAtRet PdaEosEthFlowBandwidthUpdate(Tha60290081ModulePda self, AtEthFlow flow, AtChannel channel)
    {
    AtConcateGroup vcg = (AtConcateGroup)AtEncapChannelBoundPhyGet((AtEncapChannel)channel);
    uint32 regAddr = PdaFlowModeCtrl(self) + GfpLookupOffset(self, channel);
    uint32 regVal = mChannelHwRead(flow, regAddr, cThaModulePda);

    mRegFieldSet(regVal, cAf6_ramtdmmodecfg_PDAIdleCode_PktBW_, AtConcateGroupSourceBandwidthInNumStsGet(vcg));
    mChannelHwWrite(flow, regAddr, regVal, cThaModulePda);
    return cAtOk;
    }

static void OverrideThaModulePda(AtModule self)
    {
    ThaModulePda pdaModule = (ThaModulePda)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePdaMethods = mMethodsGet(pdaModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdaOverride, mMethodsGet(pdaModule), sizeof(m_ThaModulePdaOverride));

        mMethodOverride(m_ThaModulePdaOverride, PwPdaTdmLookupEnable);
        mMethodOverride(m_ThaModulePdaOverride, SAToPModeSet);
        mMethodOverride(m_ThaModulePdaOverride, CESoPModeSet);
        mMethodOverride(m_ThaModulePdaOverride, CepModeSet);
        }

    mMethodsSet(pdaModule, &m_ThaModulePdaOverride);
    }

static void OverrideTha60210011ModulePda(AtModule self)
    {
    Tha60210011ModulePda pdaModule = (Tha60210011ModulePda)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011ModulePdaMethods = mMethodsGet(pdaModule);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModulePdaOverride, m_Tha60210011ModulePdaMethods, sizeof(m_Tha60210011ModulePdaOverride));

        mMethodOverride(m_Tha60210011ModulePdaOverride, HoLoOc48IdMask);
        mMethodOverride(m_Tha60210011ModulePdaOverride, HoLoOc48IdShift);
        mMethodOverride(m_Tha60210011ModulePdaOverride, PwCEPModeMask);
        mMethodOverride(m_Tha60210011ModulePdaOverride, PwCEPModeShift);
        mMethodOverride(m_Tha60210011ModulePdaOverride, PwLowDs0ModeMask);
        mMethodOverride(m_Tha60210011ModulePdaOverride, PwLowDs0ModeShift);
        mMethodOverride(m_Tha60210011ModulePdaOverride, PwLookupPwIDMask);
        mMethodOverride(m_Tha60210011ModulePdaOverride, PwLookupPwIDShift);
        mMethodOverride(m_Tha60210011ModulePdaOverride, LoTdmSmallDS0Control);
        mMethodOverride(m_Tha60210011ModulePdaOverride, PwTdmLowRateOffset);
        }

    mMethodsSet(pdaModule, &m_Tha60210011ModulePdaOverride);
    }

static void OverrideTha60290022ModulePda(AtModule self)
    {
    Tha60290022ModulePda module = (Tha60290022ModulePda)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290022ModulePdaMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290022ModulePdaOverride, m_Tha60290022ModulePdaMethods, sizeof(m_Tha60290022ModulePdaOverride));

        mMethodOverride(m_Tha60290022ModulePdaOverride, TdmLookupOffset);
        mMethodOverride(m_Tha60290022ModulePdaOverride, NumHwPwInOc48Slice);
        mMethodOverride(m_Tha60290022ModulePdaOverride, Oc192cTdmModeCtrl);
        mMethodOverride(m_Tha60290022ModulePdaOverride, Oc192cTdmLookupCtrl);
        }

    mMethodsSet(module, &m_Tha60290022ModulePdaOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModulePda(self);
    OverrideTha60210011ModulePda(self);
    OverrideTha60290022ModulePda(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290081ModulePda);
    }

AtModule Tha60290081ModulePdaObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022ModulePdaObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60290081ModulePdaNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290081ModulePdaObjectInit(newModule, device);
    }

eAtRet Tha60290081ModulePdaEosEthFlowLookupSet(Tha60290081ModulePda self, AtEthFlow flow, AtChannel channel)
    {
    if (self)
        return PdaGfpEthFlowLookupSet(self, flow, channel);
    return cAtErrorNullPointer;
    }

eAtRet Tha60290081ModulePdaEosEthFlowLookupReset(Tha60290081ModulePda self, AtEthFlow flow, AtChannel channel)
    {
    if (self)
        return PdaGfpEthFlowLookupReset(self, flow, channel);
    return cAtErrorNullPointer;
    }

eAtRet Tha60290081ModulePdaEosEthFlowModeSet(Tha60290081ModulePda self, AtEthFlow flow, AtChannel channel)
    {
    if (self)
        return PdaEosEthFlowModeSet(self, flow, channel);
    return cAtErrorNullPointer;
    }

eAtRet Tha60290081ModulePdaEosEthFlowBandwidthUpdate(Tha60290081ModulePda self, AtEthFlow flow, AtChannel channel)
    {
    if (self)
        return PdaEosEthFlowBandwidthUpdate(self, flow, channel);
    return cAtErrorNullPointer;
    }

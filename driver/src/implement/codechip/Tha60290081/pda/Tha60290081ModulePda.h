/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDA
 * 
 * File        : Tha60290081ModulePda.h
 * 
 * Created Date: Jul 26, 2019
 *
 * Description : 60290081 Module PDA
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290081MODULEPDA_H_
#define _THA60290081MODULEPDA_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290081ModulePda *Tha60290081ModulePda;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha60290081ModulePdaEosEthFlowLookupSet(Tha60290081ModulePda self, AtEthFlow flow, AtChannel channel);
eAtRet Tha60290081ModulePdaEosEthFlowLookupReset(Tha60290081ModulePda self, AtEthFlow flow, AtChannel channel);

eAtRet Tha60290081ModulePdaEosEthFlowModeSet(Tha60290081ModulePda self, AtEthFlow flow, AtChannel channel);
eAtRet Tha60290081ModulePdaEosEthFlowBandwidthUpdate(Tha60290081ModulePda self, AtEthFlow flow, AtChannel channel);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290081MODULEPDA_H_ */


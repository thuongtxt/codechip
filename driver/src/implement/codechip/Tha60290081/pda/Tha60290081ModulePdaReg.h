/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      : PDA
 *                                                                              
 * File        : Tha60290081ModulePdaReg.h
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constant definitions of PDA block.
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _THA60290081MODULEPDAREG_H_
#define _THA60290081MODULEPDAREG_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire PDA Jitter Buffer Control
Reg Addr   : 0x00010000 - 0x000114FF #The address format for these registers is 0x00010000 + PWID
Reg Formula: 0x00010000 +  PWID
    Where  : 
           + $PWID(0-5375): Pseudowire ID
Reg Desc   : 
This register configures jitter buffer parameters per pseudo-wire
HDL_PATH: rtljitbuf.ramjitbufcfg.ram.ram[$PWID]

------------------------------------------------------------------------------*/
#define cAf6Reg_ramjitbufcfg_Base                                                                   0x00010000
#define cAf6Reg_ramjitbufcfg(PWID)                                                         (0x00010000+(PWID))
#define cAf6Reg_ramjitbufcfg_WidthVal                                                                       64

/*--------------------------------------
BitField Name: PwLowDs0Mode
BitField Type: RW
BitField Desc: Pseudo-wire CES Low DS0 or TOHPW mode
BitField Bits: [56]
--------------------------------------*/
#define cAf6_ramjitbufcfg_PwLowDs0Mode_Mask                                                             cBit24
#define cAf6_ramjitbufcfg_PwLowDs0Mode_Shift                                                                24

/*--------------------------------------
BitField Name: PwCEPMode
BitField Type: RW
BitField Desc: Pseudo-wire CEP mode indication
BitField Bits: [55]
--------------------------------------*/
#define cAf6_ramjitbufcfg_PwCEPMode_Mask                                                                cBit23
#define cAf6_ramjitbufcfg_PwCEPMode_Shift                                                                   23

/*--------------------------------------
BitField Name: PwSliceOc48Id
BitField Type: RW
BitField Desc: Indicate 4 x slice OC48 or
BitField Bits: [54:53]
--------------------------------------*/
#define cAf6_ramjitbufcfg_PwSliceOc48Id_Mask                                                         cBit22_21
#define cAf6_ramjitbufcfg_PwSliceOc48Id_Shift                                                               21

/*--------------------------------------
BitField Name: PwTdmLineId
BitField Type: RW
BitField Desc: Pseudo-wire(PW) corresponding DM line ID, it is the TDM line ID
that is using in CDR,PDH and MAP with formular
BitField Bits: [52:42]
--------------------------------------*/
#define cAf6_ramjitbufcfg_PwTdmLineId_Mask                                                           cBit20_10
#define cAf6_ramjitbufcfg_PwTdmLineId_Shift                                                                 10

/*--------------------------------------
BitField Name: PwEparEn
BitField Type: RW
BitField Desc: Pseudo-wire EPAR timing mode enable 1: Enable EPAR timing mode 0:
Disable EPAR timing mode
BitField Bits: [41]
--------------------------------------*/
#define cAf6_ramjitbufcfg_PwEparEn_Mask                                                                  cBit9
#define cAf6_ramjitbufcfg_PwEparEn_Shift                                                                     9

/*--------------------------------------
BitField Name: PwHiLoPathInd
BitField Type: RW
BitField Desc: Pseudo-wire belong to Hi-order apply for BW >= TU3 CEP 1: Pseudo-
wire belong to Hi-order path 0: Pseudo-wire belong to Lo-order path
BitField Bits: [40]
--------------------------------------*/
#define cAf6_ramjitbufcfg_PwHiLoPathInd_Mask                                                             cBit8
#define cAf6_ramjitbufcfg_PwHiLoPathInd_Shift                                                                8

/*--------------------------------------
BitField Name: PwPayloadLen
BitField Type: RW
BitField Desc: TDM Payload of pseudo-wire in byte unit
BitField Bits: [39:26]
--------------------------------------*/
#define cAf6_ramjitbufcfg_PwPayloadLen_Mask_01                                                       cBit31_26
#define cAf6_ramjitbufcfg_PwPayloadLen_Shift_01                                                             26
#define cAf6_ramjitbufcfg_PwPayloadLen_Mask_02                                                         cBit7_0
#define cAf6_ramjitbufcfg_PwPayloadLen_Shift_02                                                              0

/*--------------------------------------
BitField Name: PdvSizeInPkUnit
BitField Type: RW
BitField Desc: Pdv size in packet unit. This parameter is to prevent packet
delay variation(PDV) from PSN. PdvSizePk is packet delay variation measured in
packet unit. The formula is as below: PdvSizeInPkUnit = (PwSpeedinKbps *
PdvInUsUnit)/(8000*PwPayloadLen) + ((PwSpeedInKbps *
PdvInUsus)%(8000*PwPayloadLen) !=0)
BitField Bits: [25:13]
--------------------------------------*/
#define cAf6_ramjitbufcfg_PdvSizeInPkUnit_Mask                                                       cBit25_13
#define cAf6_ramjitbufcfg_PdvSizeInPkUnit_Shift                                                             13

/*--------------------------------------
BitField Name: JitBufSizeInPkUnit
BitField Type: RW
BitField Desc: Jitter buffer size in packet unit. This parameter is mostly
double of PdvSizeInPkUnit. The formula is as below: JitBufSizeInPkUnit =
(PwSpeedinKbps * JitBufInUsUnit)/(8000*PwPayloadLen) + ((PwSpeedInKbps *
JitBufInUsus)%(8000*PwPayloadLen) !=0)
BitField Bits: [12:0]
--------------------------------------*/
#define cAf6_ramjitbufcfg_JitBufSizeInPkUnit_Mask                                                     cBit12_0
#define cAf6_ramjitbufcfg_JitBufSizeInPkUnit_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire PDA Jitter Buffer Status
Reg Addr   : 0x00014000 - 0x00015FFF #The address format for these registers is 0x00014000 + PWID
Reg Formula: 0x00014000 +  PWID
    Where  : 
           + $PWID(0-3071): Pseudowire ID
Reg Desc   : 
This register shows jitter buffer status per pseudo-wire

------------------------------------------------------------------------------*/
#define cAf6Reg_ramjitbufsta_Base                                                                   0x00014000
#define cAf6Reg_ramjitbufsta(PWID)                                                         (0x00014000+(PWID))
#define cAf6Reg_ramjitbufsta_WidthVal                                                                       64

/*--------------------------------------
BitField Name: JitBufHwComSta
BitField Type: RO
BitField Desc: Harware debug only status
BitField Bits: [54:20]
--------------------------------------*/
#define cAf6_ramjitbufsta_JitBufHwComSta_Mask_01                                                     cBit31_20
#define cAf6_ramjitbufsta_JitBufHwComSta_Shift_01                                                           20
#define cAf6_ramjitbufsta_JitBufHwComSta_Mask_02                                                      cBit22_0
#define cAf6_ramjitbufsta_JitBufHwComSta_Shift_02                                                            0

/*--------------------------------------
BitField Name: JitBufNumPk
BitField Type: RW
BitField Desc: Current number of packet in jitter buffer
BitField Bits: [19:4]
--------------------------------------*/
#define cAf6_ramjitbufsta_JitBufNumPk_Mask                                                            cBit19_4
#define cAf6_ramjitbufsta_JitBufNumPk_Shift                                                                  4

/*--------------------------------------
BitField Name: JitBufFull
BitField Type: RW
BitField Desc: Jitter Buffer full status
BitField Bits: [3]
--------------------------------------*/
#define cAf6_ramjitbufsta_JitBufFull_Mask                                                                cBit3
#define cAf6_ramjitbufsta_JitBufFull_Shift                                                                   3

/*--------------------------------------
BitField Name: JitBufState
BitField Type: RW
BitField Desc: Jitter buffer state machine status 0: START 1: FILL 2: READY 3:
READ 4: LOST 5: NEAR_EMPTY Others: NOT VALID
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_ramjitbufsta_JitBufState_Mask                                                             cBit2_0
#define cAf6_ramjitbufsta_JitBufState_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire PDA Reorder Control
Reg Addr   : 0x00020000 - 0x000214FF #The address format for these registers is 0x00020000 + PWID
Reg Formula: 0x00020000 +  PWID
    Where  : 
           + $PWID(0-5375): Pseudowire ID HDL_PATH: ramreorcfg.ram.ram[$PWID]
Reg Desc   : 
This register configures reorder parameters per pseudo-wire

------------------------------------------------------------------------------*/
#define cAf6Reg_ramreorcfg_Base                                                                     0x00020000
#define cAf6Reg_ramreorcfg(PWID)                                                           (0x00020000+(PWID))
#define cAf6Reg_ramreorcfg_WidthVal                                                                         32

/*--------------------------------------
BitField Name: PwSetLofsInPk
BitField Type: RW
BitField Desc: Number of consecutive lost packet to declare lost of packet state
BitField Bits: [25:17]
--------------------------------------*/
#define cAf6_ramreorcfg_PwSetLofsInPk_Mask                                                           cBit25_17
#define cAf6_ramreorcfg_PwSetLofsInPk_Shift                                                                 17

/*--------------------------------------
BitField Name: PwSetLopsInMsec
BitField Type: RW
BitField Desc: Number of empty time to declare lost of packet synchronization
BitField Bits: [16:9]
--------------------------------------*/
#define cAf6_ramreorcfg_PwSetLopsInMsec_Mask                                                          cBit16_9
#define cAf6_ramreorcfg_PwSetLopsInMsec_Shift                                                                9

/*--------------------------------------
BitField Name: PwReorEn
BitField Type: RW
BitField Desc: Set 1 to enable reorder
BitField Bits: [8]
--------------------------------------*/
#define cAf6_ramreorcfg_PwReorEn_Mask                                                                    cBit8
#define cAf6_ramreorcfg_PwReorEn_Shift                                                                       8

/*--------------------------------------
BitField Name: PwReorTimeout
BitField Type: RW
BitField Desc: Reorder timeout in 512us unit to detect lost packets. The formula
is as below: ReorTimeout = ((Min(31,PdvSizeInPk) * PwPayloadLen *
16)/PwSpeedInKbps
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_ramreorcfg_PwReorTimeout_Mask                                                             cBit7_0
#define cAf6_ramreorcfg_PwReorTimeout_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire PDA TDM mode Control
Reg Addr   : 0x00030000 - 0x00037FFF
Reg Formula: 0x00030000 + $TdmId
    Where  : 
           + $TdmId(0-8928): TDM or VCAT or POS Id address (0-8191): 4 slice OC48 CES ID with fomular OC48ID*2048 + Slice Psedowire ID (8192-8319): 128 Low Order VCAT ID (8320-8655): 336 Low Order ENCAP POS ID (8656-8783): 128 High Order VCAT ID (8784-8911): 128 High Order ENCAP POS ID (8912)     : 192C CEP PW ID (8913-8928): 16 Port Eth Pass Through ID where only physical port#0 and port#8 can be 10G}
Reg Desc   : 
This register configure TDM mode for interworking between Pseudowire and TDM

------------------------------------------------------------------------------*/
#define cAf6Reg_ramtdmmodecfg_Base                                                                  0x00030000
#define cAf6Reg_ramtdmmodecfg(TdmId)                                                      (0x00030000+(TdmId))
#define cAf6Reg_ramtdmmodecfg_WidthVal                                                                      32

/*--------------------------------------
BitField Name: PDARDIOff
BitField Type: RW
BitField Desc: Set 1 to disable sending RDI from CES PW to TDM in case Mbit/Rbit
of CESoP PW
BitField Bits: [28]
--------------------------------------*/
#define cAf6_ramtdmmodecfg_PDARDIOff_Mask                                                               cBit28
#define cAf6_ramtdmmodecfg_PDARDIOff_Shift                                                                  28

/*--------------------------------------
BitField Name: PDALbitRepMode
BitField Type: RW
BitField Desc: Mode to replace Lbit packet 0: Replace by AIS (default) 1:
Replace by a configuration idle code Replace by a configuration idle code
BitField Bits: [22]
--------------------------------------*/
#define cAf6_ramtdmmodecfg_PDALbitRepMode_Mask                                                          cBit22
#define cAf6_ramtdmmodecfg_PDALbitRepMode_Shift                                                             22

/*--------------------------------------
BitField Name: PDAIdleCode_PktBW
BitField Type: RW
BitField Desc: Idle pattern to replace data in case of Lost/Lbit packet or
Bandwidth in packet mode VCAT HO/POS HO: PDAIdleCode_PktBW is number of
allocated STS for the VCID or POSID VCAT LO: PDAIdleCode_PktBW is number of
allocated STS for the POSID with bandwidth >= E3, otherwise set value 0 POS LO:
set value 1 for bandwidth DS3/E3/TU3, otherwise set zero
BitField Bits: [21:14]
--------------------------------------*/
#define cAf6_ramtdmmodecfg_PDAIdleCode_PktBW_Mask                                                    cBit21_14
#define cAf6_ramtdmmodecfg_PDAIdleCode_PktBW_Shift                                                          14

/*--------------------------------------
BitField Name: PDAAisOff
BitField Type: RW
BitField Desc: Set 1 to disable sending AIS from CES PW to TDM in case Lbit or
Lost packet replace AIS
BitField Bits: [13]
--------------------------------------*/
#define cAf6_ramtdmmodecfg_PDAAisOff_Mask                                                               cBit13
#define cAf6_ramtdmmodecfg_PDAAisOff_Shift                                                                  13

/*--------------------------------------
BitField Name: PDARepMode
BitField Type: RW
BitField Desc: Mode to replace lost packet 0: Replace by replaying previous good
packet (often for voice or video application) 1: Replace by AIS (default) 2:
Replace by a configuration idle code Replace by a configuration idle code
BitField Bits: [12:11]
--------------------------------------*/
#define cAf6_ramtdmmodecfg_PDARepMode_Mask                                                           cBit12_11
#define cAf6_ramtdmmodecfg_PDARepMode_Shift                                                                 11

/*--------------------------------------
BitField Name: PDAStsId
BitField Type: RW
BitField Desc: Used in DS3/E3 SAToP  and TU3/VC3/Vc4/VC4-4C/VC4-16C CEP basic,
per slice48 corresponding master STSID of of PW circuit
BitField Bits: [10:5]
--------------------------------------*/
#define cAf6_ramtdmmodecfg_PDAStsId_Mask                                                              cBit10_5
#define cAf6_ramtdmmodecfg_PDAStsId_Shift                                                                    5

/*--------------------------------------
BitField Name: PDASigType
BitField Type: RW
BitField Desc: TDM Payload De-Assembler signal type 0: E3,DS3,TU3,VC3 1: VC4 2:
VC4-4C 3: VC4-8C,Vc4-16C 4: VC11,VC12,E1,T1,NxDS0 Others: reserved
BitField Bits: [4:2]
--------------------------------------*/
#define cAf6_ramtdmmodecfg_PDASigType_Mask                                                             cBit4_2
#define cAf6_ramtdmmodecfg_PDASigType_Shift                                                                  2

/*--------------------------------------
BitField Name: PDAMode
BitField Type: RW
BitField Desc: TDM Payload De-Assembler modes 0: SAToP or PPP or VCAT 1: CESoP
without CAS 2: CEP 3: reserved
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_ramtdmmodecfg_PDAMode_Mask                                                                cBit1_0
#define cAf6_ramtdmmodecfg_PDAMode_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire PDA TDM Look Up Control
Reg Addr   : 0x00040000 - 0x00041FFF
Reg Formula: 0x00040000 + $TdmId
    Where  : 
           + $TdmId(0-8928): TDM or VCAT or POS Id address (0-8191): 4 slice OC48 CES ID with fomular OC48ID*2048 + Slice Psedowire ID (8192-8319): 128 Low Order VCAT ID (8320-8655): 336 Low Order ENCAP POS ID (8656-8783): 128 High Order VCAT ID (8784-8911): 128 High Order ENCAP POS ID (8912)     : 192C CEP PW ID (8913-8928): 16 Port Eth Pass Through ID where only physical port#0 and port#8 can be 10G} HDL_PATH: rtlpdatdm.rtlrdca.rampwlkcfg.array.ram[$TdmId]
Reg Desc   : 
This register configure lookup from TDM PWID to global 5376 PWID

------------------------------------------------------------------------------*/
#define cAf6Reg_ramlotdmlkupcfg_Base                                                                0x00040000
#define cAf6Reg_ramlotdmlkupcfg(TdmId)                                                    (0x00040000+(TdmId))

/*--------------------------------------
BitField Name: PwHoPathInd
BitField Type: RW
BitField Desc: PW belong to Hi-order(VC4,VC3,DS3,E3)
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_ramlotdmlkupcfg_PwHoPathInd_Mask                                                           cBit31
#define cAf6_ramlotdmlkupcfg_PwHoPathInd_Shift                                                              31

/*--------------------------------------
BitField Name: PwHomstsid
BitField Type: RW
BitField Desc: Master stsid of PW in Hi-order mode
BitField Bits: [30:25]
--------------------------------------*/
#define cAf6_ramlotdmlkupcfg_PwHomstsid_Mask                                                         cBit30_25
#define cAf6_ramlotdmlkupcfg_PwHomstsid_Shift                                                               25

/*--------------------------------------
BitField Name: MlpppBundId
BitField Type: RW
BitField Desc: MLPPP bundle ID
BitField Bits: [24:16]
--------------------------------------*/
#define cAf6_ramlotdmlkupcfg_MlpppBundId_Mask                                                        cBit24_16
#define cAf6_ramlotdmlkupcfg_MlpppBundId_Shift                                                              16

/*--------------------------------------
BitField Name: MlpppEn
BitField Type: RW
BitField Desc: MLPPP enable 0: Link run PPP mode 1: Link join in a bundle in
MLPPP mode
BitField Bits: [15]
--------------------------------------*/
#define cAf6_ramlotdmlkupcfg_MlpppEn_Mask                                                               cBit15
#define cAf6_ramlotdmlkupcfg_MlpppEn_Shift                                                                  15

/*--------------------------------------
BitField Name: PwCesEnable
BitField Type: RW
BitField Desc: CES PW Enable 0: Packet mode 1: CES PW mode
BitField Bits: [14]
--------------------------------------*/
#define cAf6_ramlotdmlkupcfg_PwCesEnable_Mask                                                           cBit14
#define cAf6_ramlotdmlkupcfg_PwCesEnable_Shift                                                              14

/*--------------------------------------
BitField Name: PwLkEnable
BitField Type: RW
BitField Desc: Enable Lookup
BitField Bits: [13]
--------------------------------------*/
#define cAf6_ramlotdmlkupcfg_PwLkEnable_Mask                                                            cBit13
#define cAf6_ramlotdmlkupcfg_PwLkEnable_Shift                                                               13

/*--------------------------------------
BitField Name: PwID
BitField Type: RW
BitField Desc: Flat 5376 CES PWID or other service ID
BitField Bits: [12:0]
--------------------------------------*/
#define cAf6_ramlotdmlkupcfg_PwID_Mask                                                                cBit12_0
#define cAf6_ramlotdmlkupcfg_PwID_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : PDA Hold Register Status
Reg Addr   : 0x000000 - 0x000002
Reg Formula: 0x000000 +  HID
    Where  : 
           + $HID(0-2): Hold ID
Reg Desc   : 
This register using for hold remain that more than 128bits

------------------------------------------------------------------------------*/
#define cAf6Reg_pda_hold_status_Base                                                                  0x000000
#define cAf6Reg_pda_hold_status(HID)                                                          (0x000000+(HID))

/*--------------------------------------
BitField Name: PdaHoldStatus
BitField Type: RW
BitField Desc: Hold 32bits
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_pda_hold_status_PdaHoldStatus_Mask                                                       cBit31_0
#define cAf6_pda_hold_status_PdaHoldStatus_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire PDA ECC CRC Parity Control
Reg Addr   : 0x00008
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures PDA ECC CRC and Parity.

------------------------------------------------------------------------------*/
#define cAf6Reg_pda_config_ecc_crc_parity_control                                                      0x00008
#define cAf6Reg_pda_config_ecc_crc_parity_control_WidthVal                                                  32

/*--------------------------------------
BitField Name: PDAForceEccCor
BitField Type: RW
BitField Desc: PDA force link list Ecc error correctable   1: Set  0: Clear
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceEccCor_Mask                                       cBit6
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceEccCor_Shift                                          6

/*--------------------------------------
BitField Name: PDAForceEccErr
BitField Type: RW
BitField Desc: PDA force link list Ecc error noncorrectable   1: Set  0: Clear
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceEccErr_Mask                                       cBit5
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceEccErr_Shift                                          5

/*--------------------------------------
BitField Name: PDAForceCrcErr
BitField Type: RW
BitField Desc: PDA force data CRC error  1: Set  0: Clear
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceCrcErr_Mask                                       cBit4
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceCrcErr_Shift                                          4

/*--------------------------------------
BitField Name: PDAForceLoTdmParErr
BitField Type: RW
BitField Desc: Pseudowire PDA Low Order TDM mode Control force parity error  1:
Set  0: Clear
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceLoTdmParErr_Mask                                   cBit3
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceLoTdmParErr_Shift                                       3

/*--------------------------------------
BitField Name: PDAForceTdmLkParErr
BitField Type: RW
BitField Desc: Pseudowire PDA Lo and Ho TDM Look Up Control force parity error
1: Set  0: Clear
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceTdmLkParErr_Mask                                   cBit2
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceTdmLkParErr_Shift                                       2

/*--------------------------------------
BitField Name: PDAForceJitBufParErr
BitField Type: RW
BitField Desc: Pseudowire PDA Jitter Buffer Control force parity error  1: Set
0: Clear
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceJitBufParErr_Mask                                   cBit1
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceJitBufParErr_Shift                                       1

/*--------------------------------------
BitField Name: PDAForceReorderParErr
BitField Type: RW
BitField Desc: Pseudowire PDA Reorder Control force parity error  1: Set  0:
Clear
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceReorderParErr_Mask                                   cBit0
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceReorderParErr_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire PDA ECC CRC Parity Disable Control
Reg Addr   : 0x00009
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures PDA ECC CRC and Parity Disable.

------------------------------------------------------------------------------*/
#define cAf6Reg_pda_config_ecc_crc_parity_disable_control                                              0x00009
#define cAf6Reg_pda_config_ecc_crc_parity_disable_control_WidthVal                                          32

/*--------------------------------------
BitField Name: PDADisableEccCor
BitField Type: RW
BitField Desc: PDA disable link list Ecc error correctable   1: Set  0: Clear
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableEccCor_Mask                                   cBit6
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableEccCor_Shift                                       6

/*--------------------------------------
BitField Name: PDADisableEccErr
BitField Type: RW
BitField Desc: PDA disable link list Ecc error noncorrectable   1: Set  0: Clear
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableEccErr_Mask                                   cBit5
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableEccErr_Shift                                       5

/*--------------------------------------
BitField Name: PDADisableCrcErr
BitField Type: RW
BitField Desc: PDA disable data CRC error  1: Set  0: Clear
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableCrcErr_Mask                                   cBit4
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableCrcErr_Shift                                       4

/*--------------------------------------
BitField Name: PDADisableLoTdmParErr
BitField Type: RW
BitField Desc: Pseudowire PDA Low Order TDM mode Control disable parity error
1: Set  0: Clear
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableLoTdmParErr_Mask                                   cBit3
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableLoTdmParErr_Shift                                       3

/*--------------------------------------
BitField Name: PDADisableTdmLkParErr
BitField Type: RW
BitField Desc: Pseudowire PDA Lo and Ho TDM Look Up Control disable parity error
1: Set  0: Clear
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableTdmLkParErr_Mask                                   cBit2
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableTdmLkParErr_Shift                                       2

/*--------------------------------------
BitField Name: PDADisableJitBufParErr
BitField Type: RW
BitField Desc: Pseudowire PDA Jitter Buffer Control disable parity error  1: Set
0: Clear
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableJitBufParErr_Mask                                   cBit1
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableJitBufParErr_Shift                                       1

/*--------------------------------------
BitField Name: PDADisableReorderParErr
BitField Type: RW
BitField Desc: Pseudowire PDA Reorder Control disable parity error  1: Set  0:
Clear
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableReorderParErr_Mask                                   cBit0
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableReorderParErr_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire PDA ECC CRC Parity Sticky
Reg Addr   : 0x0000A
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures PDA ECC CRC and Parity.

------------------------------------------------------------------------------*/
#define cAf6Reg_pda_config_ecc_crc_parity_sticky                                                       0x0000A
#define cAf6Reg_pda_config_ecc_crc_parity_sticky_WidthVal                                                   32

/*--------------------------------------
BitField Name: PDAStickyEccCor
BitField Type: W1C
BitField Desc: PDA sticky link list Ecc error correctable   1: Set  0: Clear
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyEccCor_Mask                                       cBit6
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyEccCor_Shift                                          6

/*--------------------------------------
BitField Name: PDAStickyEccErr
BitField Type: W1C
BitField Desc: PDA sticky link list Ecc error noncorrectable   1: Set  0: Clear
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyEccErr_Mask                                       cBit5
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyEccErr_Shift                                          5

/*--------------------------------------
BitField Name: PDAStickyCrcErr
BitField Type: W1C
BitField Desc: PDA sticky data CRC error  1: Set  0: Clear
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyCrcErr_Mask                                       cBit4
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyCrcErr_Shift                                          4

/*--------------------------------------
BitField Name: PDAStickyLoTdmParErr
BitField Type: W1C
BitField Desc: Pseudowire PDA Low Order TDM mode Control sticky parity error  1:
Set  0: Clear
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyLoTdmParErr_Mask                                   cBit3
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyLoTdmParErr_Shift                                       3

/*--------------------------------------
BitField Name: PDAStickyTdmLkParErr
BitField Type: W1C
BitField Desc: Pseudowire PDA Lo and Ho TDM Look Up Control sticky parity error
1: Set  0: Clear
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyTdmLkParErr_Mask                                   cBit2
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyTdmLkParErr_Shift                                       2

/*--------------------------------------
BitField Name: PDAStickyJitBufParErr
BitField Type: W1C
BitField Desc: Pseudowire PDA Jitter Buffer Control sticky parity error  1: Set
0: Clear
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyJitBufParErr_Mask                                   cBit1
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyJitBufParErr_Shift                                       1

/*--------------------------------------
BitField Name: PDAStickyReorderParErr
BitField Type: W1C
BitField Desc: Pseudowire PDA Reorder Control sticky parity error  1: Set  0:
Clear
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyReorderParErr_Mask                                   cBit0
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyReorderParErr_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit3_0 Control
Reg Addr   : 0x01000
Reg Formula: 0x01000 + HaAddr3_0
    Where  : 
           + $HaAddr3_0(0-15): HA Address Bit3_0
Reg Desc   : 
This register is used to send HA read address bit3_0 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha3_0_control_Base                                                                   0x01000
#define cAf6Reg_rdha3_0_control(HaAddr30)                                                 (0x01000+(HaAddr30))
#define cAf6Reg_rdha3_0_control_WidthVal                                                                    32

/*--------------------------------------
BitField Name: ReadAddr3_0
BitField Type: RW
BitField Desc: Read value will be 0x01000 plus HaAddr3_0
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha3_0_control_ReadAddr3_0_Mask                                                         cBit19_0
#define cAf6_rdha3_0_control_ReadAddr3_0_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit7_4 Control
Reg Addr   : 0x01010
Reg Formula: 0x01010 + HaAddr7_4
    Where  : 
           + $HaAddr7_4(0-15): HA Address Bit7_4
Reg Desc   : 
This register is used to send HA read address bit7_4 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha7_4_control_Base                                                                   0x01010
#define cAf6Reg_rdha7_4_control(HaAddr74)                                                 (0x01010+(HaAddr74))
#define cAf6Reg_rdha7_4_control_WidthVal                                                                    32

/*--------------------------------------
BitField Name: ReadAddr7_4
BitField Type: RW
BitField Desc: Read value will be 0x01000 plus HaAddr7_4
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha7_4_control_ReadAddr7_4_Mask                                                         cBit19_0
#define cAf6_rdha7_4_control_ReadAddr7_4_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit11_8 Control
Reg Addr   : 0x01020
Reg Formula: 0x01020 + HaAddr11_8
    Where  : 
           + $HaAddr11_8(0-15): HA Address Bit11_8
Reg Desc   : 
This register is used to send HA read address bit11_8 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha11_8_control_Base                                                                  0x01020
#define cAf6Reg_rdha11_8_control(HaAddr118)                                              (0x01020+(HaAddr118))
#define cAf6Reg_rdha11_8_control_WidthVal                                                                   32

/*--------------------------------------
BitField Name: ReadAddr11_8
BitField Type: RW
BitField Desc: Read value will be 0x01000 plus HaAddr11_8
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha11_8_control_ReadAddr11_8_Mask                                                       cBit19_0
#define cAf6_rdha11_8_control_ReadAddr11_8_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit15_12 Control
Reg Addr   : 0x01030
Reg Formula: 0x01030 + HaAddr15_12
    Where  : 
           + $HaAddr15_12(0-15): HA Address Bit15_12
Reg Desc   : 
This register is used to send HA read address bit15_12 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha15_12_control_Base                                                                 0x01030
#define cAf6Reg_rdha15_12_control(HaAddr1512)                                           (0x01030+(HaAddr1512))
#define cAf6Reg_rdha15_12_control_WidthVal                                                                  32

/*--------------------------------------
BitField Name: ReadAddr15_12
BitField Type: RW
BitField Desc: Read value will be 0x01000 plus HaAddr15_12
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha15_12_control_ReadAddr15_12_Mask                                                     cBit19_0
#define cAf6_rdha15_12_control_ReadAddr15_12_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit19_16 Control
Reg Addr   : 0x01040
Reg Formula: 0x01040 + HaAddr19_16
    Where  : 
           + $HaAddr19_16(0-15): HA Address Bit19_16
Reg Desc   : 
This register is used to send HA read address bit19_16 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha19_16_control_Base                                                                 0x01040
#define cAf6Reg_rdha19_16_control(HaAddr1916)                                           (0x01040+(HaAddr1916))
#define cAf6Reg_rdha19_16_control_WidthVal                                                                  32

/*--------------------------------------
BitField Name: ReadAddr19_16
BitField Type: RW
BitField Desc: Read value will be 0x01000 plus HaAddr19_16
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha19_16_control_ReadAddr19_16_Mask                                                     cBit19_0
#define cAf6_rdha19_16_control_ReadAddr19_16_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit23_20 Control
Reg Addr   : 0x01050
Reg Formula: 0x01050 + HaAddr23_20
    Where  : 
           + $HaAddr23_20(0-15): HA Address Bit23_20
Reg Desc   : 
This register is used to send HA read address bit23_20 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha23_20_control_Base                                                                 0x01050
#define cAf6Reg_rdha23_20_control(HaAddr2320)                                           (0x01050+(HaAddr2320))
#define cAf6Reg_rdha23_20_control_WidthVal                                                                  32

/*--------------------------------------
BitField Name: ReadAddr23_20
BitField Type: RW
BitField Desc: Read value will be 0x01000 plus HaAddr23_20
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha23_20_control_ReadAddr23_20_Mask                                                     cBit19_0
#define cAf6_rdha23_20_control_ReadAddr23_20_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit24 and Data Control
Reg Addr   : 0x01060
Reg Formula: 0x01060 + HaAddr24
    Where  : 
           + $HaAddr24(0-1): HA Address Bit24
Reg Desc   : 
This register is used to send HA read address bit24 to HA engine to read data

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha24data_control_Base                                                                0x01060
#define cAf6Reg_rdha24data_control(HaAddr24)                                              (0x01060+(HaAddr24))

/*--------------------------------------
BitField Name: ReadHaData31_0
BitField Type: RW
BitField Desc: HA read data bit31_0
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdha24data_control_ReadHaData31_0_Mask                                                   cBit31_0
#define cAf6_rdha24data_control_ReadHaData31_0_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Hold Data63_32
Reg Addr   : 0x01070
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to read HA dword2 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha_hold63_32                                                                         0x01070

/*--------------------------------------
BitField Name: ReadHaData63_32
BitField Type: RW
BitField Desc: HA read data bit63_32
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdha_hold63_32_ReadHaData63_32_Mask                                                      cBit31_0
#define cAf6_rdha_hold63_32_ReadHaData63_32_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Hold Data95_64
Reg Addr   : 0x01071
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to read HA dword3 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_rdindr_hold95_64                                                                       0x01071

/*--------------------------------------
BitField Name: ReadHaData95_64
BitField Type: RW
BitField Desc: HA read data bit95_64
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdindr_hold95_64_ReadHaData95_64_Mask                                                    cBit31_0
#define cAf6_rdindr_hold95_64_ReadHaData95_64_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Hold Data127_96
Reg Addr   : 0x01072
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to read HA dword4 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_rdindr_hold127_96                                                                      0x01072

/*--------------------------------------
BitField Name: ReadHaData127_96
BitField Type: RW
BitField Desc: HA read data bit127_96
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdindr_hold127_96_ReadHaData127_96_Mask                                                  cBit31_0
#define cAf6_rdindr_hold127_96_ReadHaData127_96_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : PDA Dump Input Service ID Control
Reg Addr   : 0x00C00
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to select service ID from CLA to dump data, write to this register will start dump

------------------------------------------------------------------------------*/
#define cAf6Reg_rtldumprxpwid                                                                          0x00C00
#define cAf6Reg_rtldumprxpwid_WidthVal                                                                      32

/*--------------------------------------
BitField Name: PDADumpInputSerId
BitField Type: RW
BitField Desc: PDADumpInputSerId
BitField Bits: [11:0]
--------------------------------------*/
#define cAf6_rtldumprxpwid_PDADumpInputSerId_Mask                                                     cBit11_0
#define cAf6_rtldumprxpwid_PDADumpInputSerId_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : PDA Dump Input Service Data Control
Reg Addr   : 0x00E00
Reg Formula: 0x00E00 + Entry
    Where  : 
           + $Entry(0-255): Dump Entry
Reg Desc   : 
This register is used to store dumped data

------------------------------------------------------------------------------*/
#define cAf6Reg_rtldumprxdata_Base                                                                     0x00E00
#define cAf6Reg_rtldumprxdata(Entry)                                                         (0x00E00+(Entry))
#define cAf6Reg_rtldumprxdata_WidthVal                                                                      64

/*--------------------------------------
BitField Name: PDADumpInputSop
BitField Type: RW
BitField Desc: PDADumpInputSop
BitField Bits: [35]
--------------------------------------*/
#define cAf6_rtldumprxdata_PDADumpInputSop_Mask                                                          cBit3
#define cAf6_rtldumprxdata_PDADumpInputSop_Shift                                                             3

/*--------------------------------------
BitField Name: PDADumpInputEop
BitField Type: RW
BitField Desc: PDADumpInputEop
BitField Bits: [34]
--------------------------------------*/
#define cAf6_rtldumprxdata_PDADumpInputEop_Mask                                                          cBit2
#define cAf6_rtldumprxdata_PDADumpInputEop_Shift                                                             2

/*--------------------------------------
BitField Name: PDADumpInputNob
BitField Type: RW
BitField Desc: PDADumpInputNob 0 means 1 byte
BitField Bits: [33:32]
--------------------------------------*/
#define cAf6_rtldumprxdata_PDADumpInputNob_Mask                                                        cBit1_0
#define cAf6_rtldumprxdata_PDADumpInputNob_Shift                                                             0

/*--------------------------------------
BitField Name: PDADumpInputData
BitField Type: RW
BitField Desc: PDADumpInputData MSB
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rtldumprxdata_PDADumpInputData_Mask                                                      cBit31_0
#define cAf6_rtldumprxdata_PDADumpInputData_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : PDA Dump Output Service ID Control
Reg Addr   : 0x64000
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to select service ID from PDA to dump data, write to this register will start dump

------------------------------------------------------------------------------*/
#define cAf6Reg_rtldumptxpwid                                                                          0x64000
#define cAf6Reg_rtldumptxpwid_WidthVal                                                                      32

/*--------------------------------------
BitField Name: PDADumpOutputSerId
BitField Type: RW
BitField Desc: PDADumpOutputSerId LO:          OutputSerId = slice48*0x800 +
slice24*0x400 + tdmpwid HO:          OutputSerId = 0x1100 + slice48*0x40 +
masterSTS VCAT:        OutputSerId = 0x1000 + vcgid GE bypass:   OutputSerId =
0x1180 + geid (0/1/2/3) XAUI:        OutputSerId = 0x1184 + xauiid (0/1)
BitField Bits: [11:0]
--------------------------------------*/
#define cAf6_rtldumptxpwid_PDADumpOutputSerId_Mask                                                    cBit11_0
#define cAf6_rtldumptxpwid_PDADumpOutputSerId_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : PDA Dump Output Service Data Control
Reg Addr   : 0x66000
Reg Formula: 0x66000 + Entry
    Where  : 
           + $Entry(0-2047): Dump Entry
Reg Desc   : 
This register is used to store dumped data

------------------------------------------------------------------------------*/
#define cAf6Reg_rtldumptxdata_Base                                                                     0x66000
#define cAf6Reg_rtldumptxdata(Entry)                                                         (0x66000+(Entry))
#define cAf6Reg_rtldumptxdata_WidthVal                                                                      32

/*--------------------------------------
BitField Name: PDADumpOutputSop
BitField Type: RW
BitField Desc: PDADumpOutputSop
BitField Bits: [18]
--------------------------------------*/
#define cAf6_rtldumptxdata_PDADumpOutputSop_Mask                                                        cBit18
#define cAf6_rtldumptxdata_PDADumpOutputSop_Shift                                                           18

/*--------------------------------------
BitField Name: PDADumpOutputEop
BitField Type: RW
BitField Desc: PDADumpOutputEop
BitField Bits: [17]
--------------------------------------*/
#define cAf6_rtldumptxdata_PDADumpOutputEop_Mask                                                        cBit17
#define cAf6_rtldumptxdata_PDADumpOutputEop_Shift                                                           17

/*--------------------------------------
BitField Name: PDADumpOutputNob
BitField Type: RW
BitField Desc: PDADumpOutputNob 0 means 1 byte
BitField Bits: [16]
--------------------------------------*/
#define cAf6_rtldumptxdata_PDADumpOutputNob_Mask                                                        cBit16
#define cAf6_rtldumptxdata_PDADumpOutputNob_Shift                                                           16

/*--------------------------------------
BitField Name: PDADumpOutputData
BitField Type: RW
BitField Desc: PDADumpOutputData MSB
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_rtldumptxdata_PDADumpOutputData_Mask                                                     cBit15_0
#define cAf6_rtldumptxdata_PDADumpOutputData_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire PDA per OC48 Low Order TDM CESoP Small DS0 Control
Reg Addr   : 0x00080000 - 0x0008FFFF
Reg Formula: 0x00080000 + Oc48ID*16384 + TdmOc48PwID
    Where  : 
           + $Oc48ID(0-3): OC48 ID
           + TdmOc48PwID(0-2047): TDM OC48 slice PWID
Reg Desc   : 
This register show the PRBS monitor status after PDA

------------------------------------------------------------------------------*/
#define cAf6Reg_ramlotdmsmallds0control_Base                                                        0x00080000
#define cAf6Reg_ramlotdmsmallds0control(Oc48ID, TdmOc48PwID)          (0x00080000+(Oc48ID)*16384+(TdmOc48PwID))
#define cAf6Reg_ramlotdmsmallds0control_WidthVal                                                            32

/*--------------------------------------
BitField Name: PDASmallDs0En
BitField Type: RW
BitField Desc: PDA CESoP small DS0 enable 1: CESoP Pseodo-wire with NxDS0 == 1
0: Other Pseodo-wire modes
BitField Bits: [0]
--------------------------------------*/
#define cAf6_ramlotdmsmallds0control_PDASmallDs0En_Mask                                                  cBit0
#define cAf6_ramlotdmsmallds0control_PDASmallDs0En_Shift                                                     0

#endif /* _THA60290081MODULEPDAREG_H_ */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha6A210021ModulePw.c
 *
 * Created Date: Dec 24, 2015
 *
 * Description : Pseudowrite module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210021/pw/Tha60210021ModulePwInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A210021ModulePw
    {
    tTha60210021ModulePw super;
    }tTha6A210021ModulePw;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePwMethods  m_AtModulePwOverride;

/* Save super implementation */
static const tAtModulePwMethods  *m_AtModulePwMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxPwsGet(AtModulePw self)
    {
    AtUnused(self);
    return 256;
    }

static void OverrideAtModulePw(AtModulePw self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModulePwMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePwOverride, mMethodsGet(self), sizeof(tAtModulePwMethods));
        mMethodOverride(m_AtModulePwOverride, MaxPwsGet);
        }

    mMethodsSet(self, &m_AtModulePwOverride);
    }

static void Override(AtModulePw self)
    {
    OverrideAtModulePw(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A210021ModulePw);
    }

static AtModulePw ObjectInit(AtModulePw self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210021ModulePwObjectInit((AtModulePw)self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePw Tha6A210021ModulePwNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePw newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

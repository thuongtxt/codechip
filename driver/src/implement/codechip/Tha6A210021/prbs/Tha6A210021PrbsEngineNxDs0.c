/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha6A210021PrbsEngineNxDs0.c
 *
 * Created Date: Dec 24, 2015
 *
 * Description : NxDs0 PRBS Engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6A210021PrbsEngineInternal.h"

#include "../../../default/man/ThaDevice.h"
#include "../../../default/pdh/ThaPdhDe1.h"
#include "AtPdhNxDs0.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A210021PrbsEngineNxDs0
    {
    tTha6A210021PrbsEngine super;
    }tTha6A210021PrbsEngineNxDs0;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8                      m_methodsInit = 0;

/* Override */
static tTha6A210021PrbsEngineMethods m_Tha6A210021PrbsEngineOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ErrorRate(Tha6A210021PrbsEngine self, uint32 de1ErrorRate, uint32 rateConstant)
    {
    AtPdhNxDS0 nxds0 = (AtPdhNxDS0)AtPrbsEngineChannelGet((AtPrbsEngine)self);
    uint32 de1BitRate = (AtPdhDe1IsE1(AtPdhNxDS0De1Get(nxds0)) == cAtTrue) ? 256 : 193;

    return (((AtPdhNxDs0NumTimeslotsGet(nxds0) * 8UL) * de1ErrorRate) / de1BitRate) * rateConstant;
    }

static AtPw PwCreate(Tha6A210021PrbsEngine self, uint32 pwId)
    {
    AtDevice device = AtChannelDeviceGet(AtPrbsEngineChannelGet((AtPrbsEngine)self));
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);

    return (AtPw)AtModulePwCESoPCreate(pwModule, (uint16)pwId, cAtPwCESoPModeBasic);
    }

static void OverrideTha6A210021PrbsEngine(AtPrbsEngine self)
    {
    Tha6A210021PrbsEngine engine = (Tha6A210021PrbsEngine)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6A210021PrbsEngineOverride, mMethodsGet(engine), sizeof(m_Tha6A210021PrbsEngineOverride));

        mMethodOverride(m_Tha6A210021PrbsEngineOverride, ErrorRate);
        mMethodOverride(m_Tha6A210021PrbsEngineOverride, PwCreate);
        }

    mMethodsSet(engine, &m_Tha6A210021PrbsEngineOverride);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A210021PrbsEngineNxDs0);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideTha6A210021PrbsEngine(self);
    }

static AtPrbsEngine ObjectInit(AtPrbsEngine self, AtPdhNxDS0 nxDs0)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6A210021PrbsEngineObjectInit(self, (AtChannel)nxDs0) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha6A210021PrbsEngineNxDs0New(AtPdhNxDS0 nxDs0)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newEngine, nxDs0);
    }

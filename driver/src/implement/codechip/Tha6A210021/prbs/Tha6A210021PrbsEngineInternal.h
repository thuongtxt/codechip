/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha6A210021PrbsEngineInternal.h
 * 
 * Created Date: Dec 24, 2015
 *
 * Description : PRBS Engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A210021PRBSENGINEINTERNAL_H_
#define _THA6A210021PRBSENGINEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/prbs/AtPrbsEngineInternal.h"

#include "Tha6A210021PrbsEngine.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A210021PrbsEngine *Tha6A210021PrbsEngine;
typedef struct tTha6A210021PrbsEngineMethods
    {
    AtPw (*PwCreate)(Tha6A210021PrbsEngine self, uint32 pwId);
    AtChannel (*CircuitToBind)(Tha6A210021PrbsEngine self, AtChannel channel);
    uint32 (*ErrorRate)(Tha6A210021PrbsEngine self, uint32 de1ErrorRate, uint32 rateConstant);
    }tTha6A210021PrbsEngineMethods;

typedef struct tTha6A210021PrbsEngine
    {
    tAtPrbsEngine super;
    const tTha6A210021PrbsEngineMethods *methods;

    /* Private data */
    AtPw pw;
    eAtBerRate errorRate;
    }tTha6A210021PrbsEngine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngine Tha6A210021PrbsEngineObjectInit(AtPrbsEngine self, AtChannel channel);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A210021PRBSENGINEINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha6A210021PrbsEngine.c
 *
 * Created Date: Dec 22, 2015
 *
 * Description : PRBS Module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6A210021PrbsEngineInternal.h"
#include "Tha6A210021PrbsEngineReg.h"

#include "AtPdhNxDs0.h"
#include "../../../default/man/ThaDevice.h"
#include "../../../default/pdh/ThaPdhDe1.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha6A210021PrbsEngine)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha6A210021PrbsEngineMethods m_methods;

/* Override */
static tAtPrbsEngineMethods    m_AtPrbsEngineOverride;
static tAtObjectMethods        m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet PwCircuitBind(Tha6A210021PrbsEngine self, AtPw pw, AtChannel circuit)
    {
    AtChannel realCircuit = mMethodsGet(self)->CircuitToBind(self, circuit);

    if (realCircuit == NULL)
        return cAtErrorNullPointer;

    return AtPwCircuitBind(pw, realCircuit);;
    }

static AtPw PseudowireCreate(Tha6A210021PrbsEngine self)
    {
    uint32 freePwId;
    AtPw pw = NULL;
    AtDevice device = AtChannelDeviceGet(AtPrbsEngineChannelGet((AtPrbsEngine)self));
    ThaModulePw pwModule = (ThaModulePw)AtDeviceModuleGet(device, cAtModulePw);
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    AtEthPort port = AtModuleEthPortGet(ethModule, 0);

    /* Find one free PW */
    freePwId = ThaModulePwFreePwGet(pwModule);
    if (freePwId >= AtModulePwMaxPwsGet((AtModulePw)pwModule))
        return NULL;

    /* Create PW */
    pw = mMethodsGet(self)->PwCreate(self, freePwId);
    if (pw == NULL)
        return NULL;

    AtPwEthPortSet(pw, port);
    if (PwCircuitBind(self, pw, AtPrbsEngineChannelGet((AtPrbsEngine)self)) != cAtOk)
        {
        AtModulePwDeletePw((AtModulePw)pwModule, (uint16)freePwId);
        return NULL;
        }

    return pw;
    }

static void PseudowireDelete(Tha6A210021PrbsEngine self)
    {
    AtModulePw pwModule;

    if (self == NULL)
        return;

    pwModule = (AtModulePw)AtChannelModuleGet((AtChannel)self->pw);
    AtPwCircuitUnbind((AtPw)self->pw);
    AtModulePwDeletePw(pwModule, (uint16)AtChannelIdGet((AtChannel)self->pw));
    self->pw = NULL;
    }

static uint32 Offset(AtPw pw)
    {
    return (AtChannelHwIdGet((AtChannel)pw) + 0x1A0000);
    }

static eAtModulePrbsRet HwEnable(AtPrbsEngine self, AtPw pw, eBool enable)
    {
    uint32 regAddress, regValue;

    /* Monitor */
    regAddress = cAf6Reg_bert_mon_control_Base + Offset(pw);
    regValue = AtPrbsEngineRead(self, regAddress, cThaModuleMap);
    mRegFieldSet(regValue, cAf6_mglb_pen_enable_gen_, mBoolToBin(enable));
    AtPrbsEngineWrite(self, regAddress, regValue, cThaModuleMap);

    /* Generate */
    regAddress = cAf6Reg_bert_gen_ctrl_Base + Offset(pw);
    regValue = AtPrbsEngineRead(self, regAddress, cThaModuleMap);
    mRegFieldSet(regValue, cAf6_map_channel_ctrl_enable_gen_, mBoolToBin(enable));
    AtPrbsEngineWrite(self, regAddress, regValue, cThaModuleMap);
    return cAtOk;
    }

static eBool HwIsEnabled(AtPrbsEngine self, AtPw pw)
    {
    uint32 regAddress = cAf6Reg_bert_mon_control_Base + Offset(pw);
    uint32 regValue = AtPrbsEngineRead(self, regAddress, cThaModuleMap);

    return mRegField(regValue, cAf6_mglb_pen_enable_gen_) ? cAtTrue : cAtFalse;
    }

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    /* Enable corresponding PW PRBS engine */
    if (enable)
        {
        if (mThis(self)->pw == NULL)
            mThis(self)->pw = PseudowireCreate(mThis(self));
        }

    /* In case of disable, delete PW PRBS engine to save PW resources */
    else
        PseudowireDelete(mThis(self));

    return HwEnable(self, mThis(self)->pw, enable);
    }

static eAtModulePrbsRet Invert(AtPrbsEngine self, eBool invert)
    {
    eAtModulePrbsRet ret = AtPrbsEngineTxInvert(self, invert);

    ret |= AtPrbsEngineTxInvert(self, invert);
    return ret;
    }

static eBool IsInverted(AtPrbsEngine self)
    {
    return AtPrbsEngineTxIsInverted(self);
    }

static eAtModulePrbsRet TxInvert(AtPrbsEngine self, eBool invert)
    {
    uint32 regAddress = cAf6Reg_bert_gen_ctrl_Base + Offset(mThis(self)->pw);
    uint32 regValue = AtPrbsEngineRead(self, regAddress, cThaModuleMap);

    mRegFieldSet(regValue, cAf6_bert_gen_ctrl_invert_, mBoolToBin(invert));
    AtPrbsEngineWrite(self, regAddress, regValue, cThaModuleMap);
    return cAtOk;
    }

static eBool TxIsInverted(AtPrbsEngine self)
    {
    uint32 regAddress = cAf6Reg_bert_gen_ctrl_Base + Offset(mThis(self)->pw);
    uint32 regValue = AtPrbsEngineRead(self, regAddress, cThaModuleMap);

    return mRegField(regValue, cAf6_bert_gen_ctrl_invert_) ? cAtTrue : cAtFalse;
    }

static eAtModulePrbsRet RxInvert(AtPrbsEngine self, eBool invert)
    {
    uint32 regAddress = cAf6Reg_bert_mon_control_Base + Offset(mThis(self)->pw);
    uint32 regValue = AtPrbsEngineRead(self, regAddress, cThaModuleMap);

    mRegFieldSet(regValue, cAf6_bert_mon_control_mswapmode_, mBoolToBin(invert));
    AtPrbsEngineWrite(self, regAddress, regValue, cThaModuleMap);
    return cAtOk;
    }

static eBool RxIsInverted(AtPrbsEngine self)
    {
    uint32 regAddress = cAf6Reg_bert_mon_control_Base + Offset(mThis(self)->pw);
    uint32 regValue = AtPrbsEngineRead(self, regAddress, cThaModuleMap);
    return mRegField(regValue, cAf6_bert_mon_control_mswapmode_) ? cAtTrue : cAtFalse;
    }

static eAtModulePrbsRet ErrorForce(AtPrbsEngine self, eBool force)
    {
    uint32 regAddress = cAf6Reg_bert_gen_ctrl_Base + Offset(mThis(self)->pw);
    uint32 regValue = AtPrbsEngineRead(self, regAddress, cThaModuleMap);

    mRegFieldSet(regValue, cAf6_bert_gen_ctrl_err_ins_, mBinToBool(force));
    AtPrbsEngineWrite(self, regAddress, regValue, cThaModuleMap);
    return cAtOk;
    }

static eBool ErrorIsForced(AtPrbsEngine self)
    {
    uint32 regAddress = cAf6Reg_bert_gen_ctrl_Base + Offset(mThis(self)->pw);
    uint32 regValue = AtPrbsEngineRead(self, regAddress, cThaModuleMap);

    return mBinToBool(mRegField(regValue, cAf6_bert_gen_ctrl_err_ins_)) ? cAtTrue : cAtFalse;
    }

static eAtModulePrbsRet TxErrorRateSet(AtPrbsEngine self, eAtBerRate errorRate)
    {
    uint32 regAddress, regValue;

    if (errorRate == cAtBerRate1E10)
        return cAtErrorModeNotSupport;

    regAddress = cAf6Reg_ber_gen_insert_err_Base + Offset(mThis(self)->pw);
    regValue = AtPrbsEngineRead(self, regAddress, cThaModuleMap);
    mRegFieldSet(regValue, cAf6_bert_gen_tx_ber_error_rate_, Tha6A210021PrbsEngineErrorRateSw2Hw(self, errorRate));
    AtPrbsEngineWrite(self, regAddress, regValue, cThaModuleMap);
    mThis(self)->errorRate = errorRate;

    return cAtOk;
    }

static eAtBerRate TxErrorRateGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return mThis(self)->errorRate;
    }

static eAtModulePrbsRet ModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    eAtModulePrbsRet ret = AtPrbsEngineTxModeSet(self, prbsMode);

    ret |= AtPrbsEngineRxModeSet(self, prbsMode);
    return ret;
    }

static eAtPrbsMode ModeGet(AtPrbsEngine self)
    {
    return AtPrbsEngineTxModeGet(self);
    }

static uint8 ModeSw2Hw(eAtPrbsMode prbsMode)
    {
    switch (prbsMode)
        {
        case cAtPrbsModePrbs9:             return 0x0;
        case cAtPrbsModePrbs11:            return 0x1;
        case cAtPrbsModePrbs15:            return 0x2;
        case cAtPrbsModePrbs20rStdO153:    return 0x3;
        case cAtPrbsModePrbs20StdO151:     return 0x4;
        case cAtPrbsModePrbs20QrssStdO151: return 0x5;
        case cAtPrbsModePrbs23:            return 0x6;
        case cAtPrbsModePrbsSeq:           return 0xb;
        case cAtPrbsModePrbsFixedPattern1Byte:
        case cAtPrbsModePrbsFixedPattern2Bytes:
        case cAtPrbsModePrbsFixedPattern3Bytes:
        case cAtPrbsModePrbsFixedPattern4Bytes:
            return 0x14;

        case cAtPrbsModeInvalid:
        case cAtPrbsModePrbs31 :
        case cAtPrbsModePrbs7  :
        case cAtPrbsModeNum    :
        default: return 0xFF;
        }
    }

static uint8 ModeHw2Sw(uint8 hwMode)
    {
    switch (hwMode)
        {
        case 0x0:  return cAtPrbsModePrbs9;
        case 0x1:  return cAtPrbsModePrbs11;
        case 0x2:  return cAtPrbsModePrbs15;
        case 0x3:  return cAtPrbsModePrbs20rStdO153;
        case 0x4:  return cAtPrbsModePrbs20StdO151;
        case 0x5:  return cAtPrbsModePrbs20QrssStdO151;
        case 0x6:  return cAtPrbsModePrbs23;
        case 0xb:  return cAtPrbsModePrbsSeq;
        case 0x14: return cAtPrbsModePrbsFixedPattern1Byte;
        default:   return cAtPrbsModeInvalid;
        }
    }

static eAtModulePrbsRet TxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    uint32 regAddress, regValue;

    if ((prbsMode == cAtPrbsModePrbs31) || (prbsMode == cAtPrbsModeInvalid))
        return cAtErrorModeNotSupport;

    regAddress = cAf6Reg_bert_gen_ctrl_Base + Offset(mThis(self)->pw);
    regValue = AtPrbsEngineRead(self, regAddress, cThaModuleMap);
    mRegFieldSet(regValue, cAf6_bert_gen_ctrl_mode_, ModeSw2Hw(prbsMode));
    AtPrbsEngineWrite(self, regAddress, regValue, cThaModuleMap);
    return cAtOk;
    }

static eAtPrbsMode TxModeGet(AtPrbsEngine self)
    {
    uint32 regAddress = cAf6Reg_bert_gen_ctrl_Base + Offset(mThis(self)->pw);
    uint32 regValue = AtPrbsEngineRead(self, regAddress, cThaModuleMap);
    uint8 hwMode;

    mFieldGet(regValue, cAf6_bert_gen_ctrl_mode_Mask, cAf6_bert_gen_ctrl_mode_Shift, uint8, &hwMode);
    return ModeHw2Sw(hwMode);
    }

static eAtModulePrbsRet RxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    uint32 regAddress, regValue;

    if ((prbsMode == cAtPrbsModePrbs31) || (prbsMode == cAtPrbsModeInvalid))
        return cAtErrorModeNotSupport;

    regAddress = cAf6Reg_bert_mon_control_Base + Offset(mThis(self)->pw);
    regValue = AtPrbsEngineRead(self, regAddress, cThaModuleMap);
    mRegFieldSet(regValue, cAf6_bert_mon_control_mpatt_mode_, ModeSw2Hw(prbsMode));
    AtPrbsEngineWrite(self, regAddress, regValue, cThaModuleMap);
    return cAtOk;
    }

static eAtPrbsMode RxModeGet(AtPrbsEngine self)
    {
    uint32 regAddress = cAf6Reg_bert_mon_control_Base + Offset(mThis(self)->pw);
    uint32 regValue = AtPrbsEngineRead(self, regAddress, cThaModuleMap);
    uint8 hwMode;

    mFieldGet(regValue, cAf6_bert_mon_control_mpatt_mode_Mask, cAf6_bert_mon_control_mpatt_mode_Shift, uint8, &hwMode);
    return ModeHw2Sw(hwMode);
    }

static eAtModulePrbsRet SideSet(AtPrbsEngine self, eAtPrbsSide option)
    {
    AtUnused(self);
    return (option == cAtPrbsSideTdm) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtPrbsSide SideGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsSideTdm;
    }

static eAtModulePrbsRet FixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    eAtModulePrbsRet ret = AtPrbsEngineTxFixedPatternSet(self, fixedPattern);

    ret |= AtPrbsEngineRxFixedPatternSet(self, fixedPattern);
    return ret;
    }

static uint32 FixedPatternGet(AtPrbsEngine self)
    {
    return AtPrbsEngineTxFixedPatternGet(self);
    }

static eAtModulePrbsRet TxFixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    uint32 regAddress, regValue;

    regAddress = cAf6Reg_bert_gen_fixpatt_control_Base + Offset(mThis(self)->pw);
    regValue = AtPrbsEngineRead(self, regAddress, cAtModulePrbs);
    mRegFieldSet(regValue, cAf6_bert_gen_fixpatt_control_TxFixpat_, fixedPattern);
    AtPrbsEngineWrite(self, regAddress, regValue, cAtModulePrbs);
    return cAtOk;
    }

static uint32 TxFixedPatternGet(AtPrbsEngine self)
    {
    uint32 regAddress = cAf6Reg_bert_gen_fixpatt_control_Base + Offset(mThis(self)->pw);
    uint32 regValue = AtPrbsEngineRead(self, regAddress, cAtModulePrbs);

    return mRegField(regValue, cAf6_bert_gen_fixpatt_control_TxFixpat_);
    }

static eAtModulePrbsRet RxFixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    uint32 regAddress, regValue;

    regAddress = cAf6Reg_bert_mon_fixpatt_control_Base + Offset(mThis(self)->pw);
    regValue = AtPrbsEngineRead(self, regAddress, cAtModulePrbs);
    mRegFieldSet(regValue, cAf6_bert_gen_fixpatt_control_TxFixpat_, fixedPattern);
    AtPrbsEngineWrite(self, regAddress, regValue, cAtModulePrbs);
    return cAtOk;
    }

static uint32 RxFixedPatternGet(AtPrbsEngine self)
    {
    uint32 regAddress = cAf6Reg_bert_mon_fixpatt_control_Base + Offset(mThis(self)->pw);
    uint32 regValue = AtPrbsEngineRead(self, regAddress, cAtModulePrbs);

    return mRegField(regValue, cAf6_bert_gen_fixpatt_control_TxFixpat_);
    }

static uint32 AlarmGet(AtPrbsEngine self)
    {
    uint32 regAddress = cAf6Reg_bert_status_mon_Base + Offset(mThis(self)->pw);
    uint32 regValue = AtPrbsEngineRead(self, regAddress, cAtModulePrbs);
    const uint8 cDataMonitorErr = 0x3;

    if (mRegField(regValue, cAf6_bert_status_mon_mcrr_stt_) == cDataMonitorErr)
        return cAtPrbsEngineAlarmTypeNone;
    return cAtPrbsEngineAlarmTypeError;
    }

static uint32 CounterGet(AtPrbsEngine self, uint16 counterType)
    {
    /* Hardware do not support this mode */
    AtUnused(self);
    AtUnused(counterType);
    return 0;
    }

static uint32 CounterClear(AtPrbsEngine self, uint16 counterType)
    {
    uint32 regAddress;

    switch (counterType)
        {
        case cAtPrbsEngineCounterTxBit:
            {
            regAddress = cAf6Reg_mon_goodbit_counter_gen_Base + Offset(mThis(self)->pw);
            return AtPrbsEngineRead(self, regAddress, cAtModulePrbs);
            }
        case cAtPrbsEngineCounterRxBit:
            {
            regAddress = cAf6Reg_bert_mon_good_bit_cnt_Base + Offset(mThis(self)->pw);
            return AtPrbsEngineRead(self, regAddress, cAtModulePrbs);
            }
        case cAtPrbsEngineCounterRxBitError:
            {
            regAddress = cAf6Reg_bert_mon_error_cnt_Base + Offset(mThis(self)->pw);
            return AtPrbsEngineRead(self, regAddress, cAtModulePrbs);
            }
        case cAtPrbsEngineCounterRxBitLoss:
            {
            regAddress = cAf6Reg_bert_mon_lost_bit_cnt_Base + Offset(mThis(self)->pw);
            return AtPrbsEngineRead(self, regAddress, cAtModulePrbs);
            }
        default:
            return 0;
        }
    }

static uint32 ErrorRate(Tha6A210021PrbsEngine self, uint32 de1ErrorRate, uint32 rateConstant)
    {
    AtPdhNxDS0 nxds0 = (AtPdhNxDS0)AtPrbsEngineChannelGet((AtPrbsEngine)self);
    uint32 de1BitRate = (AtPdhDe1IsE1(AtPdhNxDS0De1Get(nxds0)) == cAtTrue) ? 256 : 193;

    return (((AtPdhNxDs0NumTimeslotsGet(nxds0) * 8UL) * de1ErrorRate) / de1BitRate) * rateConstant;
    }

static AtChannel CircuitToBind(Tha6A210021PrbsEngine self, AtChannel channel)
    {
    AtUnused(self);
    return channel;
    }

static AtPw PwCreate(Tha6A210021PrbsEngine self, uint32 pwId)
    {
    AtUnused(pwId);
    AtUnused(self);
    /* Concrete class should do */
    return NULL;
    }

static void Delete(AtObject self)
    {
    PseudowireDelete(mThis(self));
    m_AtObjectMethods->Delete(self);
    }

static eBool IsEnabled(AtPrbsEngine self)
    {
    if (mThis(self)->pw == NULL)
        return cAtFalse;

    return HwIsEnabled(self, mThis(self)->pw);
    }

static eAtModulePrbsRet TxEnable(AtPrbsEngine self, eBool enable)
    {
    AtUnused(self);
    return (enable == cAtTrue) ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool TxIsEnabled(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtModulePrbsRet RxEnable(AtPrbsEngine self, eBool enable)
    {
    AtUnused(self);
    return (enable == cAtTrue) ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool RxIsEnabled(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool CounterIsSupported(AtPrbsEngine self, uint16 counterType)
    {
    AtUnused(self);
    if ((counterType == cAtPrbsEngineCounterTxBit)      ||
        (counterType == cAtPrbsEngineCounterRxBit)      ||
        (counterType == cAtPrbsEngineCounterRxBitError) ||
        (counterType == cAtPrbsEngineCounterRxBitLoss))
        return cAtTrue;
    return cAtFalse;
    }

static void MethodsInit(Tha6A210021PrbsEngine self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, ErrorRate);
        mMethodOverride(m_methods, CircuitToBind);
        mMethodOverride(m_methods, PwCreate);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtObject(AtPrbsEngine self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, mMethodsGet(self), sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Enable);
        mMethodOverride(m_AtPrbsEngineOverride, IsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmGet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, Invert);
        mMethodOverride(m_AtPrbsEngineOverride, IsInverted);
        mMethodOverride(m_AtPrbsEngineOverride, TxInvert);
        mMethodOverride(m_AtPrbsEngineOverride, TxIsInverted);
        mMethodOverride(m_AtPrbsEngineOverride, RxInvert);
        mMethodOverride(m_AtPrbsEngineOverride, RxIsInverted);
        mMethodOverride(m_AtPrbsEngineOverride, SideSet);
        mMethodOverride(m_AtPrbsEngineOverride, SideGet);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorForce);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorIsForced);
        mMethodOverride(m_AtPrbsEngineOverride, TxErrorRateSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxErrorRateGet);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, FixedPatternSet);
        mMethodOverride(m_AtPrbsEngineOverride, FixedPatternGet);
        mMethodOverride(m_AtPrbsEngineOverride, TxFixedPatternSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxFixedPatternGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxFixedPatternSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxFixedPatternGet);
        mMethodOverride(m_AtPrbsEngineOverride, CounterClear);
        mMethodOverride(m_AtPrbsEngineOverride, CounterGet);
        mMethodOverride(m_AtPrbsEngineOverride, TxEnable);
        mMethodOverride(m_AtPrbsEngineOverride, TxIsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, RxEnable);
        mMethodOverride(m_AtPrbsEngineOverride, RxIsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, CounterIsSupported);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtObject(self);
    OverrideAtPrbsEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A210021PrbsEngine);
    }

AtPrbsEngine Tha6A210021PrbsEngineObjectInit(AtPrbsEngine self, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPrbsEngineObjectInit(self, channel, 0) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

uint32 Tha6A210021PrbsEngineErrorRateSw2Hw(AtPrbsEngine self, eAtBerRate errorRate)
    {
    uint32 de1ErrorRate = 0;
    uint32 rateConstant = 1;

    switch ((uint32) errorRate)
        {
        case cAtBerRate1E3:
            de1ErrorRate = 1000;
            break;
        case cAtBerRate1E4:
            de1ErrorRate = 10000;
            break;
        case cAtBerRate1E5:
            de1ErrorRate = 100000;
            break;
        case cAtBerRate1E6:
            de1ErrorRate = 1000000;
            break;
        case cAtBerRate1E7:
            de1ErrorRate = 1000000;
            rateConstant = 10;
            break;

        case cAtBerRate1E8:
            de1ErrorRate = 1000000;
            rateConstant = 100;
            break;

        case cAtBerRate1E9:
            de1ErrorRate = 1000000;
            rateConstant = 1000;
            break;

        case cAtBerRate1E2:
        case cAtBerRate1E10:
        case cAtBerRateUnknown:
        default:
            break;
        }

    return mMethodsGet(mThis(self))->ErrorRate(mThis(self), de1ErrorRate, rateConstant);
    }

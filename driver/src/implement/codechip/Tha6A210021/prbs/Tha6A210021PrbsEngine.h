/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha6A210021PrbsEngine.h
 * 
 * Created Date: Dec 22, 2015
 *
 * Description : Engine PRBS
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A210021PRBSENGINE_H_
#define _THA6A210021PRBSENGINE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPrbsEngine.h"
#include "AtModulePrbs.h"
#include "AtPw.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef enum eThaAttPdhDe1ErrorType
    {
    cThaAttPdhDe3ErrorTypeFbit = cBit2,
    cThaAttPdhDe3ErrorTypeSEF  = cBit1,
    cThaAttPdhDe3ErrorTypeLos  = cBit0
    }eThaAttPdhDe1ErrorType;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngine Tha6A210021PrbsEngineNxDs0New(AtPdhNxDS0 nxDs0);
AtPrbsEngine Tha6A210021PrbsEngineDe1New(AtPdhDe1 de1);

eAtModulePrbsRet Tha6A210021PrbsEngineDe1ForceError(AtPdhDe1 de1, uint32 mask, eBool enable);
eBool Tha6A210021PrbsEngineDe1ForceErrorGet(AtPdhDe1 de1, uint32 mask);
uint32 Tha6A210021PrbsEngineErrorRateSw2Hw(AtPrbsEngine self, eAtBerRate errorRate);
eAtModulePrbsRet Tha6A210021PrbsEngineDe1ForceSingleErrorPerSecond(AtPdhDe1 self, eBool enable);
eAtModulePrbsRet Tha6A210021PrbsEngineDe1ForceContinuosErrorPerSecond(AtPdhDe1 self, eBool enable);
eBool Tha6A210021PrbsEngineDe1IsForcedContinuosErrorPerSecond(AtPdhDe1 self);
eAtModulePrbsRet Tha6A210021PrbsEngineDe1NumberErrorPerSecondSet(AtPdhDe1 self, uint32 numErrorPerSecond);
uint32 Tha6A210021PrbsEngineDe1NumberErrorPerSecondGet(AtPdhDe1 self);
eAtModulePrbsRet Tha6A210021PrbsEngineDe1NumberErrorSet(AtPdhDe1 self, uint32 numError);
eAtModulePrbsRet Tha6A210021PrbsEngineDe1ErrorRate(AtPdhDe1 self, eAtBerRate errorRate);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A210021PRBSENGINE_H_ */


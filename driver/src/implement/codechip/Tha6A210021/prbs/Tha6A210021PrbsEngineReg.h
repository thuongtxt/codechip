/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha6A210021PrbsEngineReg.h
 * 
 * Created Date: Dec 22, 2015
 *
 * Description : Register PRBS Engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A210021PRBSENGINEREG_H_
#define _THA6A210021PRBSENGINEREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/*------------------------------------------------------------------------------
Reg Name   : Thalassa Bert GEN Ds0 Channel Control
Reg Addr   : 0x005000 - 0x0057FF
Reg Formula: 0x005000 + pwid
    Where  :
           + $pwid(0 - 255)
Reg Desc   :
The registers specify mode of BERT

------------------------------------------------------------------------------*/
#define cAf6Reg_bert_gen_ctrl_Base                                                                    0x005000
#define cAf6Reg_bert_gen_ctrl(pwid)                                                          (0x005000+(pwid))
#define cAf6Reg_bert_gen_ctrl_WidthVal                                                                      32
#define cAf6Reg_bert_gen_ctrl_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: Enable generate
BitField Type: RW
BitField Desc: Enable generate
BitField Bits: [15]
--------------------------------------*/
#define cAf6_map_channel_ctrl_enable_gen_Bit_Start                                                                15
#define cAf6_map_channel_ctrl_enable_gen_Bit_End                                                                  15
#define cAf6_map_channel_ctrl_enable_gen_Mask                                                                 cBit15
#define cAf6_map_channel_ctrl_enable_gen_Shift                                                                    15
#define cAf6_map_channel_ctrl_enable_gen_MaxVal                                                                  0x1
#define cAf6_map_channel_ctrl_enable_gen_MinVal                                                                  0x0
#define cAf6_map_channel_ctrl_enable_gen_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: err_ins
BitField Type: RW
BitField Desc: insert one bit error
BitField Bits: [12]
--------------------------------------*/
#define cAf6_bert_gen_ctrl_err_ins_Bit_Start                                                                12
#define cAf6_bert_gen_ctrl_err_ins_Bit_End                                                                  12
#define cAf6_bert_gen_ctrl_err_ins_Mask                                                                 cBit12
#define cAf6_bert_gen_ctrl_err_ins_Shift                                                                    12
#define cAf6_bert_gen_ctrl_err_ins_MaxVal                                                                  0x1
#define cAf6_bert_gen_ctrl_err_ins_MinVal                                                                  0x0
#define cAf6_bert_gen_ctrl_err_ins_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: order
BitField Type: RW
BitField Desc: select MSB/LSB
BitField Bits: [11]
--------------------------------------*/
#define cAf6_bert_gen_ctrl_order_Bit_Start                                                                  11
#define cAf6_bert_gen_ctrl_order_Bit_End                                                                    11
#define cAf6_bert_gen_ctrl_order_Mask                                                                   cBit11
#define cAf6_bert_gen_ctrl_order_Shift                                                                      11
#define cAf6_bert_gen_ctrl_order_MaxVal                                                                    0x1
#define cAf6_bert_gen_ctrl_order_MinVal                                                                    0x0
#define cAf6_bert_gen_ctrl_order_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: invert
BitField Type: RW
BitField Desc: invert value data
BitField Bits: [10]
--------------------------------------*/
#define cAf6_bert_gen_ctrl_invert_Bit_Start                                                                 10
#define cAf6_bert_gen_ctrl_invert_Bit_End                                                                   10
#define cAf6_bert_gen_ctrl_invert_Mask                                                                  cBit10
#define cAf6_bert_gen_ctrl_invert_Shift                                                                     10
#define cAf6_bert_gen_ctrl_invert_MaxVal                                                                   0x1
#define cAf6_bert_gen_ctrl_invert_MinVal                                                                   0x0
#define cAf6_bert_gen_ctrl_invert_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: pattern_byte
BitField Type: RW
BitField Desc: number of byte user
BitField Bits: [9:5]
--------------------------------------*/
#define cAf6_bert_gen_ctrl_pattern_byte_Bit_Start                                                            5
#define cAf6_bert_gen_ctrl_pattern_byte_Bit_End                                                              9
#define cAf6_bert_gen_ctrl_pattern_byte_Mask                                                           cBit9_5
#define cAf6_bert_gen_ctrl_pattern_byte_Shift                                                                5
#define cAf6_bert_gen_ctrl_pattern_byte_MaxVal                                                            0x1f
#define cAf6_bert_gen_ctrl_pattern_byte_MinVal                                                             0x0
#define cAf6_bert_gen_ctrl_pattern_byte_RstVal                                                             0x0

/*--------------------------------------
BitField Name: mode
BitField Type: RW
BitField Desc: sel prbs mode 00000: Prbs9 00001: Prbs11 00010: Prbs15 00011:
Prbs20 (X19 + X2 + 1) 00100: Prbs20 (X19 + X16 + 1) 00101: Qrss20 (X19 + X16 +
1) 00110: Prbs23 00111: DDS1 01000: DDS2 01001: DDS3 01010: DDS4 01011: DDS5
01100: Daly55 01101: Octet55_v2 01110: Octet55_v3 01111: Fix 3 in 24 10000: Fix
1 in 8 10001: Fix 2 in 8 10010: Fix pattern n-bit 10011: Sequence 10100: Prbs7
BitField Bits: [4:0]
--------------------------------------*/
#define cAf6_bert_gen_ctrl_mode_Bit_Start                                                                    0
#define cAf6_bert_gen_ctrl_mode_Bit_End                                                                      4
#define cAf6_bert_gen_ctrl_mode_Mask                                                                   cBit4_0
#define cAf6_bert_gen_ctrl_mode_Shift                                                                        0
#define cAf6_bert_gen_ctrl_mode_MaxVal                                                                    0x1f
#define cAf6_bert_gen_ctrl_mode_MinVal                                                                     0x0
#define cAf6_bert_gen_ctrl_mode_RstVal                                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Thalassa Bert GEN Ds0 Fixpatt
Reg Addr   : 0x005800 - 0x005FFF
Reg Formula: 0x005800 + pwid
    Where  :
           + $pwid(0 - 255)
Reg Desc   :
The registers specify mode of BERT

------------------------------------------------------------------------------*/
#define cAf6Reg_bert_gen_fixpatt_control_Base                                                         0x005800
#define cAf6Reg_bert_gen_fixpatt_control(pwid)                                               (0x005800+(pwid))
#define cAf6Reg_bert_gen_fixpatt_control_WidthVal                                                           32
#define cAf6Reg_bert_gen_fixpatt_control_WriteMask                                                         0x0

/*--------------------------------------
BitField Name: TxFixpat
BitField Type: RW
BitField Desc: cfg fix pattern
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_bert_gen_fixpatt_control_TxFixpat_Bit_Start                                                     0
#define cAf6_bert_gen_fixpatt_control_TxFixpat_Bit_End                                                      31
#define cAf6_bert_gen_fixpatt_control_TxFixpat_Mask                                                   cBit31_0
#define cAf6_bert_gen_fixpatt_control_TxFixpat_Shift                                                         0
#define cAf6_bert_gen_fixpatt_control_TxFixpat_MaxVal                                               0xffffffff
#define cAf6_bert_gen_fixpatt_control_TxFixpat_MinVal                                                      0x0
#define cAf6_bert_gen_fixpatt_control_TxFixpat_RstVal                                                      0x0

/*--------------------------------------
BitField Name: DDS4Thr
BitField Type: RW
BitField Desc: Configurable the number of DDS4 pattern is repeated in DDS5
signal
BitField Bits: [29:20]
--------------------------------------*/
#define cAf6_bert_gen_fixpatt_control_DDS4Thr_Bit_Start                                                     20
#define cAf6_bert_gen_fixpatt_control_DDS4Thr_Bit_End                                                       29
#define cAf6_bert_gen_fixpatt_control_DDS4Thr_Mask                                                   cBit29_20
#define cAf6_bert_gen_fixpatt_control_DDS4Thr_Shift                                                         20
#define cAf6_bert_gen_fixpatt_control_DDS4Thr_MaxVal                                                     0x3ff
#define cAf6_bert_gen_fixpatt_control_DDS4Thr_MinVal                                                       0x0
#define cAf6_bert_gen_fixpatt_control_DDS4Thr_RstVal                                                       0x0

/*--------------------------------------
BitField Name: DDS3Thr
BitField Type: RW
BitField Desc: Configurable the number of DDS3 pattern is repeated in DDS5
signal
BitField Bits: [17:08]
--------------------------------------*/
#define cAf6_bert_gen_fixpatt_control_DDS3Thr_Bit_Start                                                      8
#define cAf6_bert_gen_fixpatt_control_DDS3Thr_Bit_End                                                       17
#define cAf6_bert_gen_fixpatt_control_DDS3Thr_Mask                                                    cBit17_8
#define cAf6_bert_gen_fixpatt_control_DDS3Thr_Shift                                                          8
#define cAf6_bert_gen_fixpatt_control_DDS3Thr_MaxVal                                                     0x3ff
#define cAf6_bert_gen_fixpatt_control_DDS3Thr_MinVal                                                       0x0
#define cAf6_bert_gen_fixpatt_control_DDS3Thr_RstVal                                                       0x0

/*--------------------------------------
BitField Name: DDS2Thr
BitField Type: RW
BitField Desc: Configurable the number of DDS2 pattern is repeated in DDS5
signal
BitField Bits: [07:04]
--------------------------------------*/
#define cAf6_bert_gen_fixpatt_control_DDS2Thr_Bit_Start                                                      4
#define cAf6_bert_gen_fixpatt_control_DDS2Thr_Bit_End                                                        7
#define cAf6_bert_gen_fixpatt_control_DDS2Thr_Mask                                                     cBit7_4
#define cAf6_bert_gen_fixpatt_control_DDS2Thr_Shift                                                          4
#define cAf6_bert_gen_fixpatt_control_DDS2Thr_MaxVal                                                       0xf
#define cAf6_bert_gen_fixpatt_control_DDS2Thr_MinVal                                                       0x0
#define cAf6_bert_gen_fixpatt_control_DDS2Thr_RstVal                                                       0x0

/*--------------------------------------
BitField Name: DDS1Thr
BitField Type: RW
BitField Desc: Configurable the number of DDS1 pattern is repeated in DDS5
signal
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_bert_gen_fixpatt_control_DDS1Thr_Bit_Start                                                      0
#define cAf6_bert_gen_fixpatt_control_DDS1Thr_Bit_End                                                        3
#define cAf6_bert_gen_fixpatt_control_DDS1Thr_Mask                                                     cBit3_0
#define cAf6_bert_gen_fixpatt_control_DDS1Thr_Shift                                                          0
#define cAf6_bert_gen_fixpatt_control_DDS1Thr_MaxVal                                                       0xf
#define cAf6_bert_gen_fixpatt_control_DDS1Thr_MinVal                                                       0x0
#define cAf6_bert_gen_fixpatt_control_DDS1Thr_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : Config Ber GEN Insert Error
Reg Addr   : 0x006000 - 0x0067FF
Reg Formula: 0x006000 + pwid
    Where  :
           + $pw (0-255)
Reg Desc   :
Mode of insert error rate

------------------------------------------------------------------------------*/
#define cAf6Reg_ber_gen_insert_err_Base                                                               0x006000
#define cAf6Reg_ber_gen_insert_err(pw)                                                       (0x006000+(pw)id)
#define cAf6Reg_ber_gen_insert_err_WidthVal                                                                 32
#define cAf6Reg_ber_gen_insert_err_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: TxBerMd
BitField Type: RW
BitField Desc: Error Rate
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_bert_gen_tx_ber_error_rate_Bit_Start                                                      0
#define cAf6_bert_gen_tx_ber_error_rate_Bit_End                                                        32
#define cAf6_bert_gen_tx_ber_error_rate_Mask                                                           cBit31_0
#define cAf6_bert_gen_tx_ber_error_rate_Shift                                                          0
#define cAf6_bert_gen_tx_ber_error_rate_MaxVal                                                         0xffff
#define cAf6_bert_gen_tx_ber_error_rate_MinVal                                                         0x0
#define cAf6_bert_gen_tx_ber_error_rate_RstVal                                                         0x0

/*------------------------------------------------------------------------------
Reg Name   : Moniter Good Bit GEN
Reg Addr   : 0x7800 - 0x7FFF
Reg Formula: 0x7800 + $pwid
    Where  :
           + $pwid(0-255)
Reg Desc   :
counter good bit

------------------------------------------------------------------------------*/
#define cAf6Reg_mon_goodbit_counter_gen_Base                                                            0x7800
#define cAf6Reg_mon_goodbit_counter_gen(pwid)                                                  (0x7800+(pwid))
#define cAf6Reg_mon_goodbit_counter_gen_WidthVal                                                            32
#define cAf6Reg_mon_goodbit_counter_gen_WriteMask                                                          0x0

/*--------------------------------------
BitField Name: goodbit_pdo
BitField Type: RW
BitField Desc: counter goodbit
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_mon_goodbit_counter_gen_goodbit_pdo_Bit_Start                                                   0
#define cAf6_mon_goodbit_counter_gen_goodbit_pdo_Bit_End                                                    23
#define cAf6_mon_goodbit_counter_gen_goodbit_pdo_Mask                                                 cBit23_0
#define cAf6_mon_goodbit_counter_gen_goodbit_pdo_Shift                                                       0
#define cAf6_mon_goodbit_counter_gen_goodbit_pdo_MaxVal                                               0xffffff
#define cAf6_mon_goodbit_counter_gen_goodbit_pdo_MinVal                                                    0x0
#define cAf6_mon_goodbit_counter_gen_goodbit_pdo_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : Bert Mon Global Register
Reg Addr   : 0x9000-0x97FF
Reg Formula: 0x9000 + $pwid
    Where  :
           + $pwid(0-255)
Reg Desc   :
global mon config

------------------------------------------------------------------------------*/
#define cAf6Reg_bert_mon_control_Base                                                                   0x9000
#define cAf6Reg_bert_mon_control(pwid)                                                         (0x9000+(pwid))
#define cAf6Reg_bert_mon_control_WidthVal                                                                   32
#define cAf6Reg_bert_mon_control_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: Enable gen
BitField Type: RW
BitField Desc: swap data
BitField Bits: [15]
--------------------------------------*/
#define cAf6_mglb_pen_enable_gen_Bit_Start                                                           15
#define cAf6_mglb_pen_enable_gen_Bit_End                                                             15
#define cAf6_mglb_pen_enable_gen_Mask                                                            cBit15
#define cAf6_mglb_pen_enable_gen_Shift                                                               15
#define cAf6_mglb_pen_enable_gen_MaxVal                                                             0x1
#define cAf6_mglb_pen_enable_gen_MinVal                                                             0x0
#define cAf6_mglb_pen_enable_gen_RstVal                                                             0x0

/*--------------------------------------
BitField Name: mswapmode
BitField Type: RW
BitField Desc: swap data
BitField Bits: [11]
--------------------------------------*/
#define cAf6_bert_mon_control_mswapmode_Bit_Start                                                           11
#define cAf6_bert_mon_control_mswapmode_Bit_End                                                             11
#define cAf6_bert_mon_control_mswapmode_Mask                                                            cBit11
#define cAf6_bert_mon_control_mswapmode_Shift                                                               11
#define cAf6_bert_mon_control_mswapmode_MaxVal                                                             0x1
#define cAf6_bert_mon_control_mswapmode_MinVal                                                             0x0
#define cAf6_bert_mon_control_mswapmode_RstVal                                                             0x0

/*--------------------------------------
BitField Name: minvmode
BitField Type: RW
BitField Desc: invert data
BitField Bits: [10]
--------------------------------------*/
#define cAf6_bert_mon_control_minvmode_Bit_Start                                                            10
#define cAf6_bert_mon_control_minvmode_Bit_End                                                              10
#define cAf6_bert_mon_control_minvmode_Mask                                                             cBit10
#define cAf6_bert_mon_control_minvmode_Shift                                                                10
#define cAf6_bert_mon_control_minvmode_MaxVal                                                              0x1
#define cAf6_bert_mon_control_minvmode_MinVal                                                              0x0
#define cAf6_bert_mon_control_minvmode_RstVal                                                              0x0

/*--------------------------------------
BitField Name: mpatbit
BitField Type: RW
BitField Desc: number of bit fix patt use
BitField Bits: [09:05]
--------------------------------------*/
#define cAf6_bert_mon_control_mpatbit_Bit_Start                                                              5
#define cAf6_bert_mon_control_mpatbit_Bit_End                                                                9
#define cAf6_bert_mon_control_mpatbit_Mask                                                             cBit9_5
#define cAf6_bert_mon_control_mpatbit_Shift                                                                  5
#define cAf6_bert_mon_control_mpatbit_MaxVal                                                              0x1f
#define cAf6_bert_mon_control_mpatbit_MinVal                                                               0x0
#define cAf6_bert_mon_control_mpatbit_RstVal                                                               0x0

/*--------------------------------------
BitField Name: mpatt_mode
BitField Type: RW
BitField Desc: sel pattern gen # 0x0 : Prbs9 # 0x1 : Prbs11 # 0x2 : Prbs15 # 0x3
: Prbs20r # 0x4 : Prbs20 # 0x5 : qrss20 # 0x6 : Prbs23 # 0x7 : dds1 # 0x8 : dds2
# 0x9 : dds3 # 0xA : dds4 # 0xB : dds5 # 0xC : daly # 0XD : octet55_v2 # 0xE :
octet55_v3 # 0xF : fix3in24 # 0x10: fix1in8 # 0x11: fix2in8 # 0x12: fixpat #
0x13: sequence # 0x14: ds0prbs # 0x15: fixnin32
BitField Bits: [04:00]
--------------------------------------*/
#define cAf6_bert_mon_control_mpatt_mode_Bit_Start                                                           0
#define cAf6_bert_mon_control_mpatt_mode_Bit_End                                                             4
#define cAf6_bert_mon_control_mpatt_mode_Mask                                                          cBit4_0
#define cAf6_bert_mon_control_mpatt_mode_Shift                                                               0
#define cAf6_bert_mon_control_mpatt_mode_MaxVal                                                           0x1f
#define cAf6_bert_mon_control_mpatt_mode_MinVal                                                            0x0
#define cAf6_bert_mon_control_mpatt_mode_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : Thalassa Bert MON Ds0 Fixpatt
Reg Addr   : 0x009800 - 0x009FFF
Reg Formula: 0x009800 + pwid
    Where  :
           + $pwid(0 - 255)
Reg Desc   :
The registers specify mode of BERT

------------------------------------------------------------------------------*/
#define cAf6Reg_bert_mon_fixpatt_control_Base                                                         0x009800
#define cAf6Reg_bert_mon_fixpatt_control(pwid)                                               (0x009800+(pwid))
#define cAf6Reg_bert_mon_fixpatt_control_WidthVal                                                           32
#define cAf6Reg_bert_mon_fixpatt_control_WriteMask                                                         0x0

/*--------------------------------------
BitField Name: TxFixpat
BitField Type: RW
BitField Desc: cfg fix pattern
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_bert_mon_fixpatt_control_TxFixpat_Bit_Start                                                     0
#define cAf6_bert_mon_fixpatt_control_TxFixpat_Bit_End                                                      31
#define cAf6_bert_mon_fixpatt_control_TxFixpat_Mask                                                   cBit31_0
#define cAf6_bert_mon_fixpatt_control_TxFixpat_Shift                                                         0
#define cAf6_bert_mon_fixpatt_control_TxFixpat_MaxVal                                               0xffffffff
#define cAf6_bert_mon_fixpatt_control_TxFixpat_MinVal                                                      0x0
#define cAf6_bert_mon_fixpatt_control_TxFixpat_RstVal                                                      0x0

/*--------------------------------------
BitField Name: mDDS4Thr
BitField Type: RW
BitField Desc: Configurable the number of DDS4 pattern is repeated in DDS5
signal
BitField Bits: [29:20]
--------------------------------------*/
#define cAf6_bert_mon_fixpatt_control_mDDS4Thr_Bit_Start                                                    20
#define cAf6_bert_mon_fixpatt_control_mDDS4Thr_Bit_End                                                      29
#define cAf6_bert_mon_fixpatt_control_mDDS4Thr_Mask                                                  cBit29_20
#define cAf6_bert_mon_fixpatt_control_mDDS4Thr_Shift                                                        20
#define cAf6_bert_mon_fixpatt_control_mDDS4Thr_MaxVal                                                    0x3ff
#define cAf6_bert_mon_fixpatt_control_mDDS4Thr_MinVal                                                      0x0
#define cAf6_bert_mon_fixpatt_control_mDDS4Thr_RstVal                                                      0x0

/*--------------------------------------
BitField Name: munused
BitField Type: RW
BitField Desc: unused
BitField Bits: [19:18]
--------------------------------------*/
#define cAf6_bert_mon_fixpatt_control_munused_Bit_Start                                                     18
#define cAf6_bert_mon_fixpatt_control_munused_Bit_End                                                       19
#define cAf6_bert_mon_fixpatt_control_munused_Mask                                                   cBit19_18
#define cAf6_bert_mon_fixpatt_control_munused_Shift                                                         18
#define cAf6_bert_mon_fixpatt_control_munused_MaxVal                                                       0x3
#define cAf6_bert_mon_fixpatt_control_munused_MinVal                                                       0x0
#define cAf6_bert_mon_fixpatt_control_munused_RstVal                                                       0x0

/*--------------------------------------
BitField Name: mDDS3Thr
BitField Type: RW
BitField Desc: Configurable the number of DDS3 pattern is repeated in DDS5
signal
BitField Bits: [17:08]
--------------------------------------*/
#define cAf6_bert_mon_fixpatt_control_mDDS3Thr_Bit_Start                                                     8
#define cAf6_bert_mon_fixpatt_control_mDDS3Thr_Bit_End                                                      17
#define cAf6_bert_mon_fixpatt_control_mDDS3Thr_Mask                                                   cBit17_8
#define cAf6_bert_mon_fixpatt_control_mDDS3Thr_Shift                                                         8
#define cAf6_bert_mon_fixpatt_control_mDDS3Thr_MaxVal                                                    0x3ff
#define cAf6_bert_mon_fixpatt_control_mDDS3Thr_MinVal                                                      0x0
#define cAf6_bert_mon_fixpatt_control_mDDS3Thr_RstVal                                                      0x0

/*--------------------------------------
BitField Name: mDDS2Thr
BitField Type: RW
BitField Desc: Configurable the number of DDS2 pattern is repeated in DDS5
signal
BitField Bits: [07:04]
--------------------------------------*/
#define cAf6_bert_mon_fixpatt_control_mDDS2Thr_Bit_Start                                                     4
#define cAf6_bert_mon_fixpatt_control_mDDS2Thr_Bit_End                                                       7
#define cAf6_bert_mon_fixpatt_control_mDDS2Thr_Mask                                                    cBit7_4
#define cAf6_bert_mon_fixpatt_control_mDDS2Thr_Shift                                                         4
#define cAf6_bert_mon_fixpatt_control_mDDS2Thr_MaxVal                                                      0xf
#define cAf6_bert_mon_fixpatt_control_mDDS2Thr_MinVal                                                      0x0
#define cAf6_bert_mon_fixpatt_control_mDDS2Thr_RstVal                                                      0x0

/*--------------------------------------
BitField Name: mDDS1Thr
BitField Type: RW
BitField Desc: Configurable the number of DDS1 pattern is repeated in DDS5
signal
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_bert_mon_fixpatt_control_mDDS1Thr_Bit_Start                                                     0
#define cAf6_bert_mon_fixpatt_control_mDDS1Thr_Bit_End                                                       3
#define cAf6_bert_mon_fixpatt_control_mDDS1Thr_Mask                                                    cBit3_0
#define cAf6_bert_mon_fixpatt_control_mDDS1Thr_Shift                                                         0
#define cAf6_bert_mon_fixpatt_control_mDDS1Thr_MaxVal                                                      0xf
#define cAf6_bert_mon_fixpatt_control_mDDS1Thr_MinVal                                                      0x0
#define cAf6_bert_mon_fixpatt_control_mDDS1Thr_RstVal                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : Status of bert
Reg Addr   : 0xA000 - 0xA7FF
Reg Formula: 0xA000 + $pwid
    Where  :
           + $pwid (0-255)
Reg Desc   :
Indicate status of bert gen

------------------------------------------------------------------------------*/
#define cAf6Reg_bert_status_mon_Base                                                                    0xA000
#define cAf6Reg_bert_status_mon(pwid)                                                          (0xA000+(pwid))
#define cAf6Reg_bert_status_mon_WidthVal                                                                   128
#define cAf6Reg_bert_status_mon_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: mcrr_stt
BitField Type: RO
BitField Desc: status value
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_bert_status_mon_mcrr_stt_Bit_Start                                                              0
#define cAf6_bert_status_mon_mcrr_stt_Bit_End                                                                1
#define cAf6_bert_status_mon_mcrr_stt_Mask                                                             cBit1_0
#define cAf6_bert_status_mon_mcrr_stt_Shift                                                                  0
#define cAf6_bert_status_mon_mcrr_stt_MaxVal                                                               0x3
#define cAf6_bert_status_mon_mcrr_stt_MinVal                                                               0x0
#define cAf6_bert_status_mon_mcrr_stt_RstVal                                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : Load BERT MON ID  counter
Reg Addr   : 0x8003
Reg Formula:
    Where  :
Reg Desc   :
Latch error counter

------------------------------------------------------------------------------*/
#define cAf6Reg_loadcnt_pen_Base                                                                        0x8003
#define cAf6Reg_loadcnt_pen                                                                             0x8003
#define cAf6Reg_loadcnt_pen_WidthVal                                                                        32
#define cAf6Reg_loadcnt_pen_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: pwid_load
BitField Type: RW
BitField Desc: pwid which want to show read cnt
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_loadcnt_pen_pwid_load_Bit_Start                                                                 0
#define cAf6_loadcnt_pen_pwid_load_Bit_End                                                                  31
#define cAf6_loadcnt_pen_pwid_load_Mask                                                               cBit31_0
#define cAf6_loadcnt_pen_pwid_load_Shift                                                                     0
#define cAf6_loadcnt_pen_pwid_load_MaxVal                                                           0xffffffff
#define cAf6_loadcnt_pen_pwid_load_MinVal                                                                  0x0
#define cAf6_loadcnt_pen_pwid_load_RstVal                                                                 0x00


/*------------------------------------------------------------------------------
Reg Name   : Error  BERT MON counter
Reg Addr   : 0x8004
Reg Formula:
    Where  :
Reg Desc   :
Latch error counter

------------------------------------------------------------------------------*/
#define cAf6Reg_errlat_pen_Base                                                                         0x8004
#define cAf6Reg_errlat_pen                                                                              0x8004
#define cAf6Reg_errlat_pen_WidthVal                                                                         32
#define cAf6Reg_errlat_pen_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: errcnt_lat
BitField Type: RW
BitField Desc: counter err
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_errlat_pen_errcnt_lat_Bit_Start                                                                 0
#define cAf6_errlat_pen_errcnt_lat_Bit_End                                                                  31
#define cAf6_errlat_pen_errcnt_lat_Mask                                                               cBit31_0
#define cAf6_errlat_pen_errcnt_lat_Shift                                                                     0
#define cAf6_errlat_pen_errcnt_lat_MaxVal                                                           0xffffffff
#define cAf6_errlat_pen_errcnt_lat_MinVal                                                                  0x0
#define cAf6_errlat_pen_errcnt_lat_RstVal                                                                 0x00


/*------------------------------------------------------------------------------
Reg Name   : Good  BERT MON counter
Reg Addr   : 0x8005
Reg Formula:
    Where  :
Reg Desc   :
Latch good counter

------------------------------------------------------------------------------*/
#define cAf6Reg_goodlat_pen_Base                                                                        0x8005
#define cAf6Reg_goodlat_pen                                                                             0x8005
#define cAf6Reg_goodlat_pen_WidthVal                                                                        32
#define cAf6Reg_goodlat_pen_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: goodcnt_lat
BitField Type: RW
BitField Desc: counter good
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_goodlat_pen_goodcnt_lat_Bit_Start                                                               0
#define cAf6_goodlat_pen_goodcnt_lat_Bit_End                                                                31
#define cAf6_goodlat_pen_goodcnt_lat_Mask                                                             cBit31_0
#define cAf6_goodlat_pen_goodcnt_lat_Shift                                                                   0
#define cAf6_goodlat_pen_goodcnt_lat_MaxVal                                                         0xffffffff
#define cAf6_goodlat_pen_goodcnt_lat_MinVal                                                                0x0
#define cAf6_goodlat_pen_goodcnt_lat_RstVal                                                               0x00


/*------------------------------------------------------------------------------
Reg Name   : Lost BERT MON counter
Reg Addr   : 0x8006
Reg Formula:
    Where  :
Reg Desc   :


------------------------------------------------------------------------------*/
#define cAf6Reg_lostlat_pen_Base                                                                        0x8006
#define cAf6Reg_lostlat_pen                                                                             0x8006
#define cAf6Reg_lostlat_pen_WidthVal                                                                        32
#define cAf6Reg_lostlat_pen_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: lostcnt_lat
BitField Type: RW
BitField Desc: counter good
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_lostlat_pen_lostcnt_lat_Bit_Start                                                               0
#define cAf6_lostlat_pen_lostcnt_lat_Bit_End                                                                31
#define cAf6_lostlat_pen_lostcnt_lat_Mask                                                             cBit31_0
#define cAf6_lostlat_pen_lostcnt_lat_Shift                                                                   0
#define cAf6_lostlat_pen_lostcnt_lat_MaxVal                                                         0xffffffff
#define cAf6_lostlat_pen_lostcnt_lat_MinVal                                                                0x0
#define cAf6_lostlat_pen_lostcnt_lat_RstVal                                                               0x00


/*------------------------------------------------------------------------------
Reg Name   : Direct Moniter BERT MON Error
Reg Addr   : 0xB000 - 0xB7FF
Reg Formula: 0xB000 + $pwid
    Where  :
           + $pwid(0-255)
Reg Desc   :
Moniter error bert counter of 16 chanel id

------------------------------------------------------------------------------*/
#define cAf6Reg_bert_mon_error_cnt_Base                                                                 0xB000
#define cAf6Reg_bert_mon_error_cnt(pwid)                                                       (0xB000+(pwid))
#define cAf6Reg_bert_mon_error_cnt_WidthVal                                                                 32
#define cAf6Reg_bert_mon_error_cnt_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: errcnt_pdo
BitField Type: R2C
BitField Desc: value of err counter
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_bert_mon_error_cnt_errcnt_pdo_Bit_Start                                                         0
#define cAf6_bert_mon_error_cnt_errcnt_pdo_Bit_End                                                          31
#define cAf6_bert_mon_error_cnt_errcnt_pdo_Mask                                                       cBit31_0
#define cAf6_bert_mon_error_cnt_errcnt_pdo_Shift                                                             0
#define cAf6_bert_mon_error_cnt_errcnt_pdo_MaxVal                                                   0xffffffff
#define cAf6_bert_mon_error_cnt_errcnt_pdo_MinVal                                                          0x0
#define cAf6_bert_mon_error_cnt_errcnt_pdo_RstVal                                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : Direct  Moniter BERT MON Good Bit
Reg Addr   : 0xB800 - 0xBFFF
Reg Formula: 0xB800 + $pwid
    Where  :
           + $pwid(0-255)
Reg Desc   :
Moniter good bert counter of 16 chanel id

------------------------------------------------------------------------------*/
#define cAf6Reg_bert_mon_good_bit_cnt_Base                                                              0xB800
#define cAf6Reg_bert_mon_good_bit_cnt(pwid)                                                    (0xB800+(pwid))
#define cAf6Reg_bert_mon_good_bit_cnt_WidthVal                                                              32
#define cAf6Reg_bert_mon_good_bit_cnt_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: goodbit_pdo
BitField Type: RW
BitField Desc: value of good cnt
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_bert_mon_good_bit_cnt_goodbit_pdo_Bit_Start                                                     0
#define cAf6_bert_mon_good_bit_cnt_goodbit_pdo_Bit_End                                                      31
#define cAf6_bert_mon_good_bit_cnt_goodbit_pdo_Mask                                                   cBit31_0
#define cAf6_bert_mon_good_bit_cnt_goodbit_pdo_Shift                                                         0
#define cAf6_bert_mon_good_bit_cnt_goodbit_pdo_MaxVal                                               0xffffffff
#define cAf6_bert_mon_good_bit_cnt_goodbit_pdo_MinVal                                                      0x0
#define cAf6_bert_mon_good_bit_cnt_goodbit_pdo_RstVal                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : Direct BERT MON Moniter Lost Bit
Reg Addr   : 0xA800 - 0xAFFF
Reg Formula: 0xA800 + $pwid
    Where  :
           + $pwid(0-255)
Reg Desc   :

------------------------------------------------------------------------------*/
#define cAf6Reg_bert_mon_lost_bit_cnt_Base                                                              0xA800
#define cAf6Reg_bert_mon_lost_bit_cnt(pwid)                                                    (0xA800+(pwid))
#define cAf6Reg_bert_mon_lost_bit_cnt_WidthVal                                                              32
#define cAf6Reg_bert_mon_lost_bit_cnt_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: lostbit_pdo
BitField Type: RW
BitField Desc: Value of lost cnt
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_bert_mon_lost_bit_cnt_lostbit_pdo_Bit_Start                                                     0
#define cAf6_bert_mon_lost_bit_cnt_lostbit_pdo_Bit_End                                                      31
#define cAf6_bert_mon_lost_bit_cnt_lostbit_pdo_Mask                                                   cBit31_0
#define cAf6_bert_mon_lost_bit_cnt_lostbit_pdo_Shift                                                         0
#define cAf6_bert_mon_lost_bit_cnt_lostbit_pdo_MaxVal                                               0xffffffff
#define cAf6_bert_mon_lost_bit_cnt_lostbit_pdo_MinVal                                                      0x0
#define cAf6_bert_mon_lost_bit_cnt_lostbit_pdo_RstVal                                                      0x0

/*------------------------------------------------------------------------------
Reg Name   : Insert error flow BER
Reg Addr   : 0x94A00-0x94BFF
Reg Formula: 0x94A00 +  $LID
    Where  :
           + $LID(0-47)
Reg Desc   :
This register is used to select alarm from pdh

------------------------------------------------------------------------------*/
#define cAf6Reg_insert_error_Base                                                                       0x94A00
#define cAf6Reg_isel_alarm2(LID)                                                               (0x94A00+(LID))
#define cAf6Reg_isel_alarm2_WidthVal                                                                        32
#define cAf6Reg_isel_alarm2_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: Hwsta
BitField Type: RO
BitField Desc: for HW only
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_isel_alarm2_Hwsta_Bit_Start                                                                    16
#define cAf6_isel_alarm2_Hwsta_Bit_End                                                                      31
#define cAf6_isel_alarm2_Hwsta_Mask                                                                  cBit31_16
#define cAf6_isel_alarm2_Hwsta_Shift                                                                        16
#define cAf6_isel_alarm2_Hwsta_MaxVal                                                                   0xffff
#define cAf6_isel_alarm2_Hwsta_MinVal                                                                      0x0
#define cAf6_isel_alarm2_Hwsta_RstVal                                                                      0xX

/*--------------------------------------
BitField Name: force_errbit_sec
BitField Type: RW
BitField Desc: set"1" to enable force single number error per second
BitField Bits: [7]
--------------------------------------*/
#define cAf6_isel_alarm2_force_single_errbit_persecond_Bit_Start                                                              7
#define cAf6_isel_alarm2_force_single_errbit_persecond_Bit_End                                                                7
#define cAf6_isel_alarm2_force_single_errbit_persecond_Mask                                                               cBit7
#define cAf6_isel_alarm2_force_single_errbit_persecond_Shift                                                                  7
#define cAf6_isel_alarm2_force_single_errbit_persecond_MaxVal                                                               0x1
#define cAf6_isel_alarm2_force_single_errbit_persecond_MinVal                                                               0x0
#define cAf6_isel_alarm2_force_single_errbit_persecond_RstVal                                                               0xX

/*--------------------------------------
BitField Name: force_errbit_sec
BitField Type: RW
BitField Desc: set"1" to enable force continuous number error per second
BitField Bits: [6]
--------------------------------------*/
#define cAf6_isel_alarm2_force_continuous_errbit_persecond_Bit_Start                                                              6
#define cAf6_isel_alarm2_force_continuous_errbit_persecond_Bit_End                                                                6
#define cAf6_isel_alarm2_force_continuous_errbit_persecond_Mask                                                               cBit6
#define cAf6_isel_alarm2_force_continuous_errbit_persecond_Shift                                                                  6
#define cAf6_isel_alarm2_force_continuous_errbit_persecond_MaxVal                                                               0x1
#define cAf6_isel_alarm2_force_continuous_errbit_persecond_MinVal                                                               0x0
#define cAf6_isel_alarm2_force_continuous_errbit_persecond_RstVal                                                               0xX

/*--------------------------------------
BitField Name: force_errbit
BitField Type: RW
BitField Desc: set"1" to enable force error bit
BitField Bits: [5]
--------------------------------------*/
#define cAf6_isel_alarm2_force_errbit_Bit_Start                                                              5
#define cAf6_isel_alarm2_force_errbit_Bit_End                                                                5
#define cAf6_isel_alarm2_force_errbit_Mask                                                               cBit5
#define cAf6_isel_alarm2_force_errbit_Shift                                                                  5
#define cAf6_isel_alarm2_force_errbit_MaxVal                                                               0x1
#define cAf6_isel_alarm2_force_errbit_MinVal                                                               0x0
#define cAf6_isel_alarm2_force_errbit_RstVal                                                               0xX

/*--------------------------------------
BitField Name: force_Fbit
BitField Type: RW
BitField Desc: set"1" to enable force error fbit
BitField Bits: [4]
--------------------------------------*/
#define cAf6_isel_alarm2_force_Fbit_Bit_Start                                                                4
#define cAf6_isel_alarm2_force_Fbit_Bit_End                                                                  4
#define cAf6_isel_alarm2_force_Fbit_Mask                                                                 cBit4
#define cAf6_isel_alarm2_force_Fbit_Shift                                                                    4
#define cAf6_isel_alarm2_force_Fbit_MaxVal                                                                 0x1
#define cAf6_isel_alarm2_force_Fbit_MinVal                                                                 0x0
#define cAf6_isel_alarm2_force_Fbit_RstVal                                                                 0xX

/*--------------------------------------
BitField Name: force_SEF
BitField Type: RW
BitField Desc: force SEF flow T1.231
BitField Bits: [3:1]
--------------------------------------*/
#define cAf6_isel_alarm2_force_SEF_Bit_Start                                                                 1
#define cAf6_isel_alarm2_force_SEF_Bit_End                                                                   3
#define cAf6_isel_alarm2_force_SEF_Mask                                                                cBit3_1
#define cAf6_isel_alarm2_force_SEF_Shift                                                                     1
#define cAf6_isel_alarm2_force_SEF_MaxVal                                                                  0x7
#define cAf6_isel_alarm2_force_SEF_MinVal                                                                  0x0
#define cAf6_isel_alarm2_force_SEF_RstVal                                                                  0xX

/*--------------------------------------
BitField Name: force_LOS
BitField Type: RW
BitField Desc: set"1" to enable force loss of signal
BitField Bits: [0]
--------------------------------------*/
#define cAf6_isel_alarm2_force_LOS_Bit_Start                                                                 0
#define cAf6_isel_alarm2_force_LOS_Bit_End                                                                   0
#define cAf6_isel_alarm2_force_LOS_Mask                                                                  cBit0
#define cAf6_isel_alarm2_force_LOS_Shift                                                                     0
#define cAf6_isel_alarm2_force_LOS_MaxVal                                                                  0x1
#define cAf6_isel_alarm2_force_LOS_MinVal                                                                  0x0
#define cAf6_isel_alarm2_force_LOS_RstVal                                                                  0xX

/*------------------------------------------------------------------------------
Reg Name   : Single bit error rate
Reg Addr   : 0x94C00-0x94DFF
Reg Formula: 0x94C00 +  $LID
    Where  :
           + $LID(0-47)
Reg Desc   :
This register is used to select alarm from pdh

------------------------------------------------------------------------------*/
#define cAf6Reg_insert_error_rate_Base                                                                       0x94C00
#define cAf6Reg_insert_error_rate(LID)                                                               (0x94C00+(LID))
#define cAf6Reg_insert_error_rate_WidthVal                                                                        32
#define cAf6Reg_insert_error_rate_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: thresh_err
BitField Type: RO
BitField Desc: Threshold insert single bit err
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_insert_error_thresh_err_Bit_Start                                                                0
#define cAf6_insert_error_thresh_err_Bit_End                                                                 31
#define cAf6_insert_error_thresh_err_Mask                                                              cBit31_0
#define cAf6_insert_error_thresh_err_Shift                                                                    0
#define cAf6_insert_error_thresh_err_MaxVal                                                          0xffffffff
#define cAf6_insert_error_thresh_err_MinVal                                                                 0x0
#define cAf6_insert_error_thresh_err_RstVal                                                                 0xX

#ifdef __cplusplus
}
#endif
#endif /* _THA6A210021PRBSENGINEREG_H_ */


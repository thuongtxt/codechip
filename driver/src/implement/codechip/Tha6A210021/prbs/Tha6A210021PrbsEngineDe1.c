/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha6A210021PrbsEngineDe1.c
 *
 * Created Date: Dec 23, 2015
 *
 * Description : DE1 PRBS Engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6A210021PrbsEngineInternal.h"
#include "Tha6A210021PrbsEngineReg.h"

#include "../../../default/man/ThaDevice.h"
#include "../../../default/pdh/ThaPdhDe1.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A210021PrbsEngineDe1
    {
    tTha6A210021PrbsEngine super;
    }tTha6A210021PrbsEngineDe1;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8                      m_methodsInit = 0;

/* Override */
static tTha6A210021PrbsEngineMethods m_Tha6A210021PrbsEngineOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ErrorRateSw2Hw(eAtBerRate errorRate)
    {
    switch ((uint32) errorRate)
        {
        case cAtBerRate1E3: return 1000;
        case cAtBerRate1E4: return 10000;
        case cAtBerRate1E5: return 100000;
        case cAtBerRate1E6: return 1000000;
        case cAtBerRate1E7: return 1000000;
        case cAtBerRate1E8: return 1000000;
        case cAtBerRate1E9: return 10000000;
        case cAtBerRate1E2:
        case cAtBerRate1E10:
        case cAtBerRateUnknown:
        default:
            return 0;
        }
    }

static eBool IsUnFrameMode(uint16 frameType)
    {
    if ((frameType == cAtPdhE1UnFrm) || (frameType == cAtPdhDs1J1UnFrm))
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsDs1FrameMode(uint16 frameValue)
    {
    if ((frameValue == cAtPdhDs1FrmSf) ||
        (frameValue == cAtPdhDs1FrmEsf) ||
        (frameValue == cAtPdhDs1FrmDDS) ||
        (frameValue == cAtPdhDs1FrmSLC))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 BaseAddress(AtPdhDe1 de1)
    {
    return ThaModulePdhBaseAddress((ThaModulePdh)AtChannelModuleGet((AtChannel)de1));
    }

static uint32 ErrorRate(Tha6A210021PrbsEngine self, uint32 de1ErrorRate, uint32 rateConstant)
    {
    AtUnused(self);
    return de1ErrorRate * rateConstant;
    }

static AtChannel CircuitToBind(Tha6A210021PrbsEngine self, AtChannel channel)
    {
    uint32 nxDs0BitMask;
    uint16 frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)channel);
    uint32 cDs0MaskOfE1ForPrbs = cBit31_1;
    uint32 cDs0MaskOfDs1ForPrbs = cBit23_1;

    if (frameType == cAtPdhDe1FrameUnknown)
        {
        mChannelLog(channel, cAtLogLevelWarning, "Frame type of circuit is unknown");
        return NULL;
        }

    if (IsUnFrameMode(frameType))
        return channel;

    nxDs0BitMask = IsDs1FrameMode(frameType) ? cDs0MaskOfDs1ForPrbs : cDs0MaskOfE1ForPrbs;
    AtPdhDe1NxDs0Create((AtPdhDe1)AtPrbsEngineChannelGet((AtPrbsEngine)self), nxDs0BitMask);
    return (AtChannel)AtPdhDe1NxDs0Get((AtPdhDe1)channel, nxDs0BitMask);
    }

static AtPw PwCreate(Tha6A210021PrbsEngine self, uint32 pwId)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    AtDevice device = AtChannelDeviceGet(channel);
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);

    if (IsUnFrameMode(AtPdhChannelFrameTypeGet((AtPdhChannel)channel)))
        return (AtPw)AtModulePwSAToPCreate(pwModule, (uint16)pwId);

    /* When E1 is in frame mode, we should create a CESoP instead of SAToP */
    return (AtPw)AtModulePwCESoPCreate(pwModule, (uint16)pwId, cAtPwCESoPModeBasic);
    }

static uint32 MaxNumberErrorSupport(AtPdhDe1 self)
    {
    uint16 frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)self);
    uint32 maxNumberErrorPerSecond = 0;

    /* All of this value is recommend by hardware */
    if ((frameType == cAtPdhE1UnFrm) || (frameType == cAtPdhE1Frm) ||
        (frameType == cAtPdhE1MFCrc))
        maxNumberErrorPerSecond = 500;

    if ((frameType == cAtPdhDs1J1UnFrm) || (frameType == cAtPdhDs1FrmSf) ||
        (frameType == cAtPdhDs1FrmEsf) || (frameType == cAtPdhDs1FrmDDS) ||
        (frameType == cAtPdhDs1FrmSLC))
        maxNumberErrorPerSecond = 333;

    return maxNumberErrorPerSecond;
    }

static uint32 NumberErrorGet(AtPdhDe1 self, uint32 numErrorPerSecond)
    {
    uint32 maxNumErrorSupport = MaxNumberErrorSupport(self);
    return (numErrorPerSecond > maxNumErrorSupport) ? maxNumErrorSupport : numErrorPerSecond;
    }

static void OverrideTha6A210021PrbsEngine(AtPrbsEngine self)
    {
    Tha6A210021PrbsEngine engine = (Tha6A210021PrbsEngine)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6A210021PrbsEngineOverride, mMethodsGet(engine), sizeof(m_Tha6A210021PrbsEngineOverride));

        mMethodOverride(m_Tha6A210021PrbsEngineOverride, CircuitToBind);
        mMethodOverride(m_Tha6A210021PrbsEngineOverride, ErrorRate);
        mMethodOverride(m_Tha6A210021PrbsEngineOverride, PwCreate);
        }

    mMethodsSet(engine, &m_Tha6A210021PrbsEngineOverride);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A210021PrbsEngineDe1);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideTha6A210021PrbsEngine(self);
    }

static AtPrbsEngine ObjectInit(AtPrbsEngine self, AtPdhDe1 de1)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6A210021PrbsEngineObjectInit(self, (AtChannel)de1) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha6A210021PrbsEngineDe1New(AtPdhDe1 de1)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newEngine, de1);
    }

eAtModulePrbsRet Tha6A210021PrbsEngineDe1ForceError(AtPdhDe1 de1, uint32 mask, eBool enable)
    {
    uint32 regAddress, regValue;

    regAddress = cAf6Reg_insert_error_Base + ThaPdhDe1DefaultOffset((ThaPdhDe1)de1)+ BaseAddress(de1);
    regValue = mChannelHwRead(de1, regAddress, cAtModulePdh);
    if (cThaAttPdhDe3ErrorTypeFbit & mask)
        mRegFieldSet(regValue, cAf6_isel_alarm2_force_Fbit_, mBoolToBin(enable));
    if (cThaAttPdhDe3ErrorTypeSEF & mask)
        mRegFieldSet(regValue, cAf6_isel_alarm2_force_SEF_, mBoolToBin(enable));
    if (cThaAttPdhDe3ErrorTypeLos & mask)
        mRegFieldSet(regValue, cAf6_isel_alarm2_force_LOS_, mBoolToBin(enable));
    mChannelHwWrite(de1, regAddress, regValue, cAtModulePdh);

    return cAtOk;
    }

eBool Tha6A210021PrbsEngineDe1ForceErrorGet(AtPdhDe1 de1, uint32 type)
    {
    uint32 regAddress = cAf6Reg_insert_error_Base + ThaPdhDe1DefaultOffset((ThaPdhDe1)de1) + BaseAddress(de1);
    uint32 regValue = mChannelHwRead(de1, regAddress, cAtModulePdh);

    if (type & cThaAttPdhDe3ErrorTypeFbit)
        return mRegField(regValue, cAf6_isel_alarm2_force_Fbit_) ? cAtTrue : cAtFalse;
    if (type & cThaAttPdhDe3ErrorTypeSEF)
        return mRegField(regValue, cAf6_isel_alarm2_force_SEF_) ? cAtTrue : cAtFalse;
    if (type & cThaAttPdhDe3ErrorTypeLos)
        return mRegField(regValue, cAf6_isel_alarm2_force_LOS_) ? cAtTrue : cAtFalse;

    return cAtFalse;
    }

eAtModulePrbsRet Tha6A210021PrbsEngineDe1ForceSingleErrorPerSecond(AtPdhDe1 self, eBool enable)
    {
    uint32 baseAddresss = ThaPdhDe1DefaultOffset((ThaPdhDe1)self) + BaseAddress(self);
    uint32 regAddress = cAf6Reg_insert_error_Base + baseAddresss;
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModulePdh);

    mRegFieldSet(regValue, cAf6_isel_alarm2_force_single_errbit_persecond_, mBoolToBin(enable));
    mChannelHwWrite(self, regAddress, regValue, cAtModulePdh);

    return cAtOk;
    }

eAtModulePrbsRet Tha6A210021PrbsEngineDe1ForceContinuosErrorPerSecond(AtPdhDe1 self, eBool enable)
    {
    uint32 baseAddresss = ThaPdhDe1DefaultOffset((ThaPdhDe1)self) + BaseAddress(self);
    uint32 regAddress = cAf6Reg_insert_error_Base + baseAddresss;
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModulePdh);

    mRegFieldSet(regValue, cAf6_isel_alarm2_force_continuous_errbit_persecond_, mBoolToBin(enable));
    mChannelHwWrite(self, regAddress, regValue, cAtModulePdh);

    return cAtOk;
    }

eBool Tha6A210021PrbsEngineDe1IsForcedContinuosErrorPerSecond(AtPdhDe1 self)
    {
    uint32 baseAddresss = ThaPdhDe1DefaultOffset((ThaPdhDe1)self) + BaseAddress(self);
    uint32 regAddress = cAf6Reg_insert_error_Base + baseAddresss;
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModulePdh);

    return mRegField(regValue, cAf6_isel_alarm2_force_continuous_errbit_persecond_) ? cAtTrue : cAtFalse;
    }

eAtModulePrbsRet Tha6A210021PrbsEngineDe1NumberErrorPerSecondSet(AtPdhDe1 self, uint32 numErrorPerSecond)
    {
    uint32 baseAddresss, regAddress, regValue;

    baseAddresss = ThaPdhDe1DefaultOffset((ThaPdhDe1)self) + BaseAddress(self);
    regAddress = cAf6Reg_insert_error_rate_Base + baseAddresss;
    regValue = mChannelHwRead(self, regAddress, cAtModulePdh);
    mRegFieldSet(regValue, cAf6_insert_error_thresh_err_, NumberErrorGet(self, numErrorPerSecond));
    mChannelHwWrite(self, regAddress, regValue, cAtModulePdh);

    return cAtOk;
    }

uint32 Tha6A210021PrbsEngineDe1NumberErrorPerSecondGet(AtPdhDe1 self)
    {
    uint32 baseAddresss = ThaPdhDe1DefaultOffset((ThaPdhDe1)self) + BaseAddress(self);
    uint32 regAddress = cAf6Reg_insert_error_rate_Base + baseAddresss;
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModulePdh);

    return mRegField(regValue, cAf6_insert_error_thresh_err_);
    }

eAtModulePrbsRet Tha6A210021PrbsEngineDe1NumberErrorSet(AtPdhDe1 self, uint32 numError)
    {
    uint32 baseAddresss, regAddress, regValue;

    if (numError > 1)
        return cAtErrorModeNotSupport;

    baseAddresss = ThaPdhDe1DefaultOffset((ThaPdhDe1)self) + BaseAddress(self);
    regAddress = cAf6Reg_insert_error_Base + baseAddresss;
    regValue = mChannelHwRead(self, regAddress, cAtModulePdh);
    mRegFieldSet(regValue, cAf6_isel_alarm2_force_errbit_, numError);
    mChannelHwWrite(self, regAddress, regValue, cAtModulePdh);

    return cAtOk;
    }

eAtModulePrbsRet Tha6A210021PrbsEngineDe1ErrorRate(AtPdhDe1 self, eAtBerRate errorRate)
    {
    uint32 baseAddresss, regAddress, regValue;

    if (errorRate == cAtBerRate1E10)
        return cAtErrorModeNotSupport;

    /* Enable force error */
    baseAddresss = ThaPdhDe1DefaultOffset((ThaPdhDe1)self) + BaseAddress(self);
    regAddress = cAf6Reg_insert_error_Base + baseAddresss;
    regValue = mChannelHwRead(self, regAddress, cAtModulePdh);
    mRegFieldSet(regValue, cAf6_isel_alarm2_force_errbit_, 1);
    mChannelHwWrite(self, regAddress, regValue, cAtModulePdh);

    /* Rate configure */
    regAddress = cAf6Reg_insert_error_rate_Base + baseAddresss;
    regValue = mChannelHwRead(self, regAddress, cAtModulePdh);
    mRegFieldSet(regValue, cAf6_insert_error_thresh_err_, ErrorRateSw2Hw(errorRate));
    mChannelHwWrite(self, regAddress, regValue, cAtModulePdh);

    return cAtOk;
    }

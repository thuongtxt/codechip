/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha6A210021ModulePrbs.h
 * 
 * Created Date: Dec 24, 2015
 *
 * Description : PRBS Module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A210021MODULEPRBS_H_
#define _THA6A210021MODULEPRBS_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModulePrbs.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePrbs Tha6A210021ModulePrbsNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A210021MODULEPRBS_H_ */


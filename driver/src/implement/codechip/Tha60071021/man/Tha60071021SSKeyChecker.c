/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60071021SSKeyChecker.c
 *
 * Created Date: Sep 20, 2013
 *
 * Description : SSKey checker
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaSSKeyCheckerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60071021SSKeyChecker
    {
    tThaSSKeyCheckerPwV2 super;
    }tTha60071021SSKeyChecker;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSSKeyCheckerMethods m_AtSSKeyCheckerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60071021SSKeyChecker);
    }

static uint32 CodeExpectedMsb(AtSSKeyChecker self)
    {
	AtUnused(self);
    return 0x02600000;
    }

static uint32 CodeExpectedLsb(AtSSKeyChecker self)
    {
	AtUnused(self);
    return 0x20441001;
    }

static void OverrideAtSSKeyChecker(AtSSKeyChecker self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSSKeyCheckerOverride, mMethodsGet(self), sizeof(m_AtSSKeyCheckerOverride));

        mMethodOverride(m_AtSSKeyCheckerOverride, CodeExpectedMsb);
        mMethodOverride(m_AtSSKeyCheckerOverride, CodeExpectedLsb);
        }

    mMethodsSet(self, &m_AtSSKeyCheckerOverride);
    }

static void Override(AtSSKeyChecker self)
    {
    OverrideAtSSKeyChecker(self);
    }

static AtSSKeyChecker ObjectInit(AtSSKeyChecker self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaSSKeyCheckerPwV2ObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSSKeyChecker Tha60071021SSKeyCheckerNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSSKeyChecker newChecker = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newChecker, device);
    }

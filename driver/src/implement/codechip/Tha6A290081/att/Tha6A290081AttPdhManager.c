/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : Tha6A290081AttPdhManager.c
 *
 * Created Date: Aug 2, 2019
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../pdh/Tha6A290081ModulePdhInternal.h"
#include "Tha6A290081AttPdhManagerInternal.h"
#include "../../../../generic/att/AtAttPdhManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha6A290081AttPdhManager)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtAttPdhManagerMethods m_AtAttPdhManagerOverride;
/* Save super implementations */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool HasRegister(AtAttPdhManager self, uint32 address)
    {
    AtUnused(self);
    if ((mInRange(address, 0x1080800, 0x1080FFF))
        )
        return cAtTrue;
    return cAtFalse;
    }

static void OverrideAtAttPdhManager(AtAttPdhManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtAttPdhManagerOverride, mMethodsGet(self), sizeof(m_AtAttPdhManagerOverride));
        mMethodOverride(m_AtAttPdhManagerOverride, HasRegister);
        }

    mMethodsSet(self, &m_AtAttPdhManagerOverride);
    }

static void Override(Tha6A290081AttPdhManager self)
    {
    OverrideAtAttPdhManager((AtAttPdhManager)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290081AttPdhManager);
    }

static AtAttPdhManager ObjectInit(AtAttPdhManager self, AtModulePdh pdh)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6A210031AttPdhManagerObjectInit(self, pdh) == NULL)
        return NULL;

    /* Setup class */
    Override(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtAttPdhManager Tha6A290081AttPdhManagerNew(AtModulePdh pdh)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtAttPdhManager newObject = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newObject == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newObject, pdh);
    }

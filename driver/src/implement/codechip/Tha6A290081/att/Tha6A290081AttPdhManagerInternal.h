/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : Tha6A210081AttPdhManagerInternal.h
 * 
 * Created Date: Aug 2, 2019
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _DRIVER_SRC_IMPLEMENT_CODECHIP_THA6A210081_ATT_THA6A210081ATTPDHMANAGERINTERNAL_H_
#define _DRIVER_SRC_IMPLEMENT_CODECHIP_THA6A210081_ATT_THA6A210081ATTPDHMANAGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha6A210031/att/Tha6A210031AttPdhManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

typedef struct tTha6A290081AttPdhManager* Tha6A290081AttPdhManager;
typedef struct tTha6A290081AttPdhManager
    {
    tTha6A210031AttPdhManager super;
    }tTha6A290081AttPdhManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtAttPdhManager Tha6A290081AttPdhManagerObjectInit(AtAttPdhManager self, AtModulePdh pdh);
AtAttPdhManager Tha6A290081AttPdhManagerNew(AtModulePdh pdh);

#ifdef __cplusplus
}
#endif
#endif /* _DRIVER_SRC_IMPLEMENT_CODECHIP_THA6A210081_ATT_THA6A210081ATTPDHMANAGERINTERNAL_H_ */


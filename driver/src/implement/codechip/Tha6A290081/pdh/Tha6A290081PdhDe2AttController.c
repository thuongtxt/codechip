/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : Tha6A290081PdhDe2AttController.c
 *
 * Created Date: Oct 14, 2019
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha6A210031/pdh/Tha6A210031PdhAttControllerInternal.h"
#include "Tha6A290081PdhAttControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A290081PdhDe2AttController
    {
    tTha6A210031PdhDe3AttController super;
    }tTha6A290081PdhDe2AttController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaAttControllerMethods    m_ThaAttControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet DefaultSet(ThaAttController self)
    {
    AtUnused(self);
    return cAtOk;
    }

static void OverrideThaAttController(ThaAttController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaAttControllerOverride, mMethodsGet(self), sizeof(m_ThaAttControllerOverride));

        mMethodOverride(m_ThaAttControllerOverride, DefaultSet);
        }

    mMethodsSet(self, &m_ThaAttControllerOverride);
    }

static void Override(AtAttController self)
    {
    OverrideThaAttController((ThaAttController)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290081PdhDe2AttController);
    }

static AtAttController ObjectInit(AtAttController self, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6A210031PdhDe3AttControllerObjectInit(self, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtAttController Tha6A290081PdhDe2AttControllerNew(AtChannel de3)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtAttController controller = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(controller, de3);
    }

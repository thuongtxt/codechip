/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : Tha6A290081ModulePdhInternal.h
 * 
 * Created Date: Oct 14, 2019
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _DRIVER_SRC_IMPLEMENT_CODECHIP_THA6A290081_PDH_THA6A290081MODULEPDHINTERNAL_H_
#define _DRIVER_SRC_IMPLEMENT_CODECHIP_THA6A290081_PDH_THA6A290081MODULEPDHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290081/pdh/Tha60290081ModulePdhInternal.h"
#include "../../../../generic/util/AtLongRegisterAccess.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A290081ModulePdh
    {
    tTha60290081ModulePdh super;
    AtLongRegisterAccess attLongRegisterAccess;
    }tTha6A290081ModulePdh;

typedef struct tTha6A290081ModulePdh *Tha6A290081ModulePdh;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePdh Tha6A290081ModulePdhNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _DRIVER_SRC_IMPLEMENT_CODECHIP_THA6A290081_PDH_THA6A290081MODULEPDHINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : Tha6A280081ModulePdh.c
 *
 * Created Date: Jun 25, 2019
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210051/pdh/Tha60210051ModulePdhInternal.h"
#include "../../Tha60210031/pdh/Tha60210031ModulePdh.h"
#include "../../../default/ocn/ThaModuleOcn.h"
#include "Tha6A290081PdhAttControllerInternal.h"
#include "../../../default/pdh/ThaModulePdh.h"
#include "Tha6A290081ModulePdhInternal.h"
#include "../att/Tha6A290081AttPdhManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((tTha6A290081ModulePdh*)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods        m_AtObjectOverride;
static tThaModulePdhMethods m_ThaModulePdhOverride;
static tThaStmModulePdhMethods m_ThaStmModulePdhOverride;
static tAtModulePdhMethods   m_AtModulePdhOverride;

/* Save super implementation */
static const tAtObjectMethods     *m_AtObjectMethods = NULL;
static const tThaModulePdhMethods    *m_ThaModulePdhMethods    = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 *AttPdhHoldRegistersGet(Tha6A290081ModulePdh self, uint16 *numberOfHoldRegisters)
    {
    static uint32 holdRegisters[] = {0x8};
    AtUnused(self);

    if (numberOfHoldRegisters)
        *numberOfHoldRegisters = 1;

    return holdRegisters;
    }

static AtLongRegisterAccess AttPdhLongRegisterAccessCreate(Tha6A290081ModulePdh self)
    {
    uint16 numHoldRegisters;
    uint32 *holdRegisters = AttPdhHoldRegistersGet(self, &numHoldRegisters);

    return AtDefaultLongRegisterAccessNew(holdRegisters, numHoldRegisters, holdRegisters, numHoldRegisters);
    }

static void SetupData(AtModulePdh self)
    {
    Tha6A290081ModulePdh module = (Tha6A290081ModulePdh)self;
    if (module->attLongRegisterAccess == NULL)
        module->attLongRegisterAccess = AttPdhLongRegisterAccessCreate(module);
    }

static eAtRet StsVtDemapVc4FractionalVc3Set(ThaStmModulePdh self, AtSdhChannel sdhVc, eBool isVc4FractionalVc3)
    {
    AtUnused(self);
    AtUnused(sdhVc);
    AtUnused(isVc4FractionalVc3);
    return cAtOk;
    }

static AtObjectAny De3AttControllerCreate(ThaModulePdh self, AtChannel channel)
    {
    AtUnused(self);
    return Tha6A290081PdhDe3AttControllerNew(channel);
    }

static AtObjectAny De1AttControllerCreate(ThaModulePdh self, AtChannel channel)
    {
    AtUnused(self);
    return Tha6A290081PdhDe1AttControllerNew(channel);
    }

static AtObjectAny De2AttControllerCreate(ThaModulePdh self, AtChannel channel)
    {
    AtUnused(self);
    return Tha6A290081PdhDe2AttControllerNew(channel);
    }

static AtAttPdhManager AttPdhManagerCreate(AtModulePdh self)
    {
    return Tha6A290081AttPdhManagerNew(self);
    }

static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject)(mThis(self)->attLongRegisterAccess));
    m_AtObjectMethods->Delete(self);
    }

static void OverrideAtObject(AtObject self)
    {
    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(self, &m_AtObjectOverride);
    }

static void OverrideAtModulePdh(AtModulePdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePdhOverride, mMethodsGet(self), sizeof(m_AtModulePdhOverride));

        mMethodOverride(m_AtModulePdhOverride, AttPdhManagerCreate);
        }

    mMethodsSet(self, &m_AtModulePdhOverride);
    }

static void OverrideThaModulePdh(AtModulePdh self)
    {
    ThaModulePdh pdhModule = (ThaModulePdh)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePdhMethods = mMethodsGet(pdhModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdhOverride, mMethodsGet(pdhModule), sizeof(m_ThaModulePdhOverride));

        mMethodOverride(m_ThaModulePdhOverride, De3AttControllerCreate);
        mMethodOverride(m_ThaModulePdhOverride, De1AttControllerCreate);
        mMethodOverride(m_ThaModulePdhOverride, De2AttControllerCreate);
        }

    mMethodsSet(pdhModule, &m_ThaModulePdhOverride);
    }

static void OverrideThaStmModulePdh(AtModulePdh self)
    {
    ThaStmModulePdh pdhModule = (ThaStmModulePdh)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaStmModulePdhOverride, mMethodsGet(pdhModule), sizeof(m_ThaStmModulePdhOverride));

        mMethodOverride(m_ThaStmModulePdhOverride, StsVtDemapVc4FractionalVc3Set);
        }

    mMethodsSet(pdhModule, &m_ThaStmModulePdhOverride);
    }

static void Override(AtModulePdh self)
    {
    OverrideAtObject((AtObject)self);
    OverrideThaModulePdh(self);
    OverrideThaStmModulePdh(self);
    OverrideAtModulePdh(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290081ModulePdh);
    }

static AtModulePdh ObjectInit(AtModulePdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290081ModulePdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    /* Setup internal data */
    SetupData(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePdh Tha6A290081ModulePdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

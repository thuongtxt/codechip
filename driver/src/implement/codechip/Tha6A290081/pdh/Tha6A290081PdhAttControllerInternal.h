/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : Tha6A290081PdhAttControllerInternal.h
 * 
 * Created Date: Oct 14, 2019
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _DRIVER_SRC_IMPLEMENT_CODECHIP_THA6A290081_PDH_THA6A290081PDHATTCONTROLLERINTERNAL_H_
#define _DRIVER_SRC_IMPLEMENT_CODECHIP_THA6A290081_PDH_THA6A290081PDHATTCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../../include/att/AtAttController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtAttController Tha6A290081PdhDe3AttControllerNew(AtChannel de3);
AtAttController Tha6A290081PdhDe1AttControllerNew(AtChannel de1);
AtAttController Tha6A290081PdhDe2AttControllerNew(AtChannel de1);

#ifdef __cplusplus
}
#endif
#endif /* _DRIVER_SRC_IMPLEMENT_CODECHIP_THA6A290081_PDH_THA6A290081PDHATTCONTROLLERINTERNAL_H_ */


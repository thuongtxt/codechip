/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : Tha6A290081ModuleDemap.c
 *
 * Created Date: Oct 17, 2019
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/map/ThaModuleMapInternal.h"
#include "../../Tha60290022/map/Tha60290022ModuleDemapInternal.h"
#include "../../Tha60290081/map/Tha60290081ModuleDemapInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A290081ModuleDemap
    {
    tTha60290081ModuleDemap super;
    }tTha6A290081ModuleDemap;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60290022ModuleDemapMethods m_Tha60290022ModuleDemapOverride;

/* Save super implementation */
static const tTha60290022ModuleDemapMethods       *m_Tha60290022ModuleDemapMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 HoDemapPWIdFieldMask(Tha60290022ModuleDemap self)
    {
    return m_Tha60290022ModuleDemapMethods->HoDemapPWIdFieldMask(self);
    }

static uint32 HoDemapPWIdFieldShift(Tha60290022ModuleDemap self)
    {
    return m_Tha60290022ModuleDemapMethods->HoDemapPWIdFieldShift(self);
    }

static void OverrideTha60290022ModuleDemap(AtModule self)
    {
    Tha60290022ModuleDemap demapModule = (Tha60290022ModuleDemap)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290022ModuleDemapMethods = mMethodsGet(demapModule);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290022ModuleDemapOverride, mMethodsGet(demapModule), sizeof(m_Tha60290022ModuleDemapOverride));

        mMethodOverride(m_Tha60290022ModuleDemapOverride, HoDemapPWIdFieldMask);
        mMethodOverride(m_Tha60290022ModuleDemapOverride, HoDemapPWIdFieldShift);
        }

    mMethodsSet(demapModule, &m_Tha60290022ModuleDemapOverride);
    }

static void Override(AtModule self)
    {
    OverrideTha60290022ModuleDemap(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290081ModuleDemap);
    }
static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290081ModuleDemapObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha6A290081ModuleDemapNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

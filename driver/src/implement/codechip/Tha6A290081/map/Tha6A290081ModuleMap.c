/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : Tha6A290081ModuleMap.c
 *
 * Created Date: Oct 17, 2019
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/map/ThaModuleMapInternal.h"
#include "../../Tha60290081/map/Tha60290081ModuleMapInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A290081ModuleMap
    {
    tTha60290081ModuleMap super;
    }tTha6A290081ModuleMap;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleAbstractMapMethods m_ThaModuleAbstractMapOverride;

/* Save super implementation */
static const tThaModuleAbstractMapMethods *m_ThaModuleAbstractMapMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet BindAuVcToPseudowire(ThaModuleAbstractMap self, AtSdhVc vc, AtPw pw)
    {
    return m_ThaModuleAbstractMapMethods->BindAuVcToPseudowire(self, vc, pw);
    }

static void OverrideThaModuleAbstractMap(AtModule self)
    {
    ThaModuleAbstractMap mapModule = (ThaModuleAbstractMap)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleAbstractMapMethods = mMethodsGet(mapModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleAbstractMapOverride, m_ThaModuleAbstractMapMethods, sizeof(m_ThaModuleAbstractMapOverride));

        mMethodOverride(m_ThaModuleAbstractMapOverride, BindAuVcToPseudowire);
        }

    mMethodsSet(mapModule, &m_ThaModuleAbstractMapOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleAbstractMap(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290081ModuleMap);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290081ModuleMapObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha6A290081ModuleMapNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

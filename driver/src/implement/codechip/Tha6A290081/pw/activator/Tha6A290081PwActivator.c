/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : Tha6A290081PwActivator.c
 *
 * Created Date: Oct 18, 2019
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../Tha60290081/pw/Tha60290081ModulePwInternal.h"
#include "../Tha6A290081PwHeaderControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A210081PwActivator
    {
    tTha60290081PwActivator super;
    }tTha6A210081PwActivator;


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPwActivatorMethods        m_ThaPwActivatorOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet PwEthPortSet(ThaPwActivator self, ThaPwAdapter adapter, AtEthPort ethPort)
    {
    AtUnused(self);
    AtUnused(adapter);
    AtUnused(ethPort);
    return cAtOk;
    }

static eAtModulePwRet PwCircuitUnbind(ThaPwActivator self, ThaPwAdapter adapter)
    {
    eAtRet ret;
    AtPw pw = (AtPw)adapter;
    AtChannel circuit;

     ret = mMethodsGet(self)->HwPwDeallocate(self, adapter);
     ThaPwAdapterHwPwSet(adapter, NULL);

     circuit = AtPwBoundCircuitGet(pw);
     ret = AtChannelBindToPseudowire(circuit, NULL);
     if (ret == cAtOk)
         ret |= ThaPwAdapterBoundCircuitSet(adapter, NULL);

    return ret;
    }

static eAtRet Activate(ThaPwActivator self, ThaPwAdapter adapter)
    {
    ThaHwPw hwPw;
    AtPw pw = (AtPw)adapter;

    hwPw = mMethodsGet(self)->HwPwAllocate(self, adapter);
    if (hwPw == NULL)
        return cAtErrorRsrcNoAvail;
    ThaPwAdapterHwPwSet(adapter, hwPw);

    return ThaPwActivatorPwCircuitConnect(self, adapter, AtPwBoundCircuitGet(pw));
    }

static eAtModulePwRet PwCircuitBind(ThaPwActivator self, ThaPwAdapter adapter, AtChannel circuit)
    {
    if (!mMethodsGet(self)->PwCanBeActivatedWithCircuit(self, adapter, circuit))
        {
        AtChannelBoundPwSet(circuit, ThaPwAdapterPwGet(adapter));
        return cAtOk;
        }

    return mMethodsGet(self)->Activate(self, adapter);
    }

static eAtModulePwRet PwCircuitConnect(ThaPwActivator self, ThaPwAdapter adapter, AtChannel circuit)
    {
    eAtRet ret = cAtOk;
    AtPw pw = (AtPw)adapter;
    AtUnused(self);

    /* Circuit will know how to bind to PW */
    ret = AtChannelBindToPseudowire(circuit, pw);
    if (ret != cAtOk)
        return ret;

    /* Set configuration on binding */
    ret |= AtChannelBoundPwSet(circuit, ThaPwAdapterPwGet(adapter));
    ret |= ThaPwAdapterBoundCircuitSet(adapter, circuit);

    return ret;
    }

static void OverrideThaPwActivator(ThaPwActivator self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwActivatorOverride, mMethodsGet(self), sizeof(m_ThaPwActivatorOverride));

        mMethodOverride(m_ThaPwActivatorOverride, PwCircuitUnbind);
        mMethodOverride(m_ThaPwActivatorOverride, Activate);
        mMethodOverride(m_ThaPwActivatorOverride, PwEthPortSet);
        mMethodOverride(m_ThaPwActivatorOverride, PwCircuitBind);
        mMethodOverride(m_ThaPwActivatorOverride, PwCircuitConnect);
        }

    mMethodsSet(self, &m_ThaPwActivatorOverride);
    }



static void Override(ThaPwActivator self)
    {
    OverrideThaPwActivator(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A210081PwActivator);
    }

static ThaPwActivator ObjectInit(ThaPwActivator self, AtModulePw pwModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290081PwActivatorObjectInit(self, pwModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwActivator Tha6A290081PwDynamicActivatorNew(AtModulePw pwModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwActivator newActivator = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newActivator == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newActivator, pwModule);
    }

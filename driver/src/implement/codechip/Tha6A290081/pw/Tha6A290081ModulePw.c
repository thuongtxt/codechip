/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : Tha6A290081ModulePw.c
 *
 * Created Date: Oct 18, 2019
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha6A000010/pw/Tha6A000010ModulePwInternal.h"
#include "../../Tha60210011/pw/activator/Tha60210011PwActivator.h"
#include "../../Tha60290081/pw/Tha60290081ModulePwInternal.h"
#include "Tha6A290081PwHeaderControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A290081ModulePw
    {
    tTha60290081ModulePw super;
    }tTha6A290081ModulePw;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePwMethods         m_ThaModulePwOverride;
static tAtModuleMethods            m_AtModuleOverride;

/* Save super implementation */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet Init(AtModule self)
    {
    AtUnused(self);
    return cAtOk;
    }

static ThaPwActivator PwActivatorCreate(ThaModulePw self)
    {
    return Tha6A290081PwDynamicActivatorNew((AtModulePw)self);
    }

static ThaPwHeaderController HeaderControllerObjectCreate(ThaModulePw self, AtPw adapter)
    {
    AtUnused(self);
    return Tha6A290081PwHeaderControllerNew(adapter);
    }

static void OverrideThaModulePw(AtModulePw self)
    {
    ThaModulePw pwModule = (ThaModulePw)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePwOverride, mMethodsGet(pwModule), sizeof(m_ThaModulePwOverride));

        mMethodOverride(m_ThaModulePwOverride, PwActivatorCreate);
        mMethodOverride(m_ThaModulePwOverride, HeaderControllerObjectCreate);
        }

    mMethodsSet(pwModule, &m_ThaModulePwOverride);
    }

static void OverrideAtModule(AtModulePw self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(module), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Init);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void Override(AtModulePw self)
    {
    OverrideAtModule(self);
    OverrideThaModulePw(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290081ModulePw);
    }

static AtModulePw ObjectInit(AtModulePw self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290081ModulePwObjectInit((AtModulePw)self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePw Tha6A290081ModulePwNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePw newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

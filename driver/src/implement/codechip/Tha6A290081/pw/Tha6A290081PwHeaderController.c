/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : Tha6A290081PwHeaderController.c
 *
 * Created Date: Oct 18, 2019
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/pw/headercontroller/Tha60210011PwHeaderControllerInternal.h"
#include "../../Tha60290081/pw/Tha60290081ModulePwInternal.h"
#include "Tha6A290081PwHeaderControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A290081PwHeaderController
    {
    tTha60210011PwHeaderController super;

    }tTha6A290081PwHeaderController;


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPwHeaderControllerMethods m_ThaPwHeaderControllerOverride;

/* Save super implementation */
static const tThaPwHeaderControllerMethods *m_ThaPwHeaderControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static tAtEthVlanTag *EthCVlanGet(ThaPwHeaderController self, tAtEthVlanTag *cVlan)
    {
    tThaPwHeaderCache *cache;
    AtOsal osal = AtSharedDriverOsalGet();

    cache = ThaPwHeaderControllerCache(self);
    if (cache == NULL)
        return NULL;

    if (cache->numVlans == 0)
        return NULL;

    mMethodsGet(osal)->MemCpy(osal, cVlan, &(cache->cVlan), sizeof(tAtEthVlanTag));

    return cVlan;
    }

static tAtEthVlanTag *EthSVlanGet(ThaPwHeaderController self, tAtEthVlanTag *sVlan)
    {
    tThaPwHeaderCache *cache;
    AtOsal osal = AtSharedDriverOsalGet();

    cache = ThaPwHeaderControllerCache(self);
    if (cache == NULL)
        return NULL;

    if (cache->numVlans <= 1)
       return NULL;

    mMethodsGet(osal)->MemCpy(osal, sVlan, &(cache->sVlan), sizeof(tAtEthVlanTag));

    return sVlan;
    }

static void OverrideThaPwHeaderController(ThaPwHeaderController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPwHeaderControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwHeaderControllerOverride, m_ThaPwHeaderControllerMethods, sizeof(m_ThaPwHeaderControllerOverride));

        mMethodOverride(m_ThaPwHeaderControllerOverride, EthCVlanGet);
        mMethodOverride(m_ThaPwHeaderControllerOverride, EthSVlanGet);
        }

    mMethodsSet(self, &m_ThaPwHeaderControllerOverride);
    }

static void Override(ThaPwHeaderController self)
    {
    OverrideThaPwHeaderController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290081PwHeaderController);
    }

static ThaPwHeaderController ObjectInit(ThaPwHeaderController self, ThaPwAdapter adapter)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290081PwHeaderControllerObjectInit(self, adapter) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwHeaderController Tha6A290081PwHeaderControllerNew(AtPw adapter)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwHeaderController newProvider = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newProvider == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newProvider, (ThaPwAdapter)adapter);
    }

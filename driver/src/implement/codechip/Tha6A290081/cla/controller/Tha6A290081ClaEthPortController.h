/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : Tha6A290081ClaEthPortController.h
 * 
 * Created Date: Oct 11, 2019
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _DRIVER_SRC_IMPLEMENT_CODECHIP_THA6A290081_CLA_CONTROLLER_THA6A290081CLAETHPORTCONTROLLER_H_
#define _DRIVER_SRC_IMPLEMENT_CODECHIP_THA6A290081_CLA_CONTROLLER_THA6A290081CLAETHPORTCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaClaEthPortController Tha6A290081ClaEthPortControllerNew(ThaModuleCla cla);

#ifdef __cplusplus
}
#endif
#endif /* _DRIVER_SRC_IMPLEMENT_CODECHIP_THA6A290081_CLA_CONTROLLER_THA6A290081CLAETHPORTCONTROLLER_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : Tha6A290081ModuleEth.c
 *
 * Created Date: Oct 15, 2019
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#include "Tha6A290081ModuleEthInternal.h"

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/
#define mThis(self)   ((Tha6A290081ModuleEth)self)

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleEthMethods m_ThaModuleEthOverride;
static tAtObjectMethods m_AtObjectOverride;
static tAtModuleMethods m_AtModuleOverride;

static const tThaModuleEthMethods *m_ThaModuleEthMethods = NULL;
static const tAtObjectMethods *m_AtObjectMethods = NULL;
static const tAtModuleMethods *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet Init(AtModule self)
    {
    m_AtModuleMethods->Init(self);
    return cAtOk;
    }

static AtSerdesController PortSerdesControllerCreate(ThaModuleEth self, AtEthPort ethPort)
    {
    AtUnused(self);
    AtUnused(ethPort);
    return NULL;
    }

static uint32 *HoldRegistersGet(AtModule self, uint16 *numberOfHoldRegisters)
    {
    static uint32 holdRegisters[] = {0x100008};
    AtUnused(self);

    if (numberOfHoldRegisters)
        *numberOfHoldRegisters = mCount(holdRegisters);

    return holdRegisters;
    }

static AtLongRegisterAccess LongRegisterAccess(AtModule self, uint32 localAddress)
    {
    AtUnused(localAddress);

    /* Or use global register access of device */
    if (mThis(self)->longRegisterAccess == NULL)
        {
        uint16 numHoldRegisters  = 0;
        uint32 *holdRegisters    = HoldRegistersGet(self, &numHoldRegisters);
        mThis(self)->longRegisterAccess = AtDefaultLongRegisterAccessNew(holdRegisters, numHoldRegisters, holdRegisters, numHoldRegisters);
        }

    return mThis(self)->longRegisterAccess;
    }

static void Delete(AtObject self)
    {
    if (mThis(self)->longRegisterAccess)
        AtObjectDelete((AtObject)mThis(self)->longRegisterAccess);
    mThis(self)->longRegisterAccess = NULL;
    m_AtObjectMethods->Delete(self);
    }

static void OverrideAtObject(AtObject self)
    {

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(self, &m_AtObjectOverride);
    }

static void OverrideThaModuleEth(ThaModuleEth self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleEthMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleEthOverride, m_ThaModuleEthMethods, sizeof(m_ThaModuleEthOverride));
        mMethodOverride(m_ThaModuleEthOverride, PortSerdesControllerCreate);
        }

    mMethodsSet(self, &m_ThaModuleEthOverride);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, HoldRegistersGet);
        mMethodOverride(m_AtModuleOverride, LongRegisterAccess);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void Override(Tha6A290081ModuleEth self)
    {
    OverrideThaModuleEth((ThaModuleEth) self);
    OverrideAtObject((AtObject) self);
    OverrideAtModule((AtModule)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290081ModuleEth);
    }

static AtModuleEth ObjectInit(AtModuleEth self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290081ModuleEthObjectInit((AtModuleEth)self, device) == NULL)
        return null;

    /* Override */
    Override((Tha6A290081ModuleEth)self);

    return self;
    }

AtModuleEth Tha6A290081ModuleEthNew(AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEth newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    return ObjectInit(newModule, device);
    }

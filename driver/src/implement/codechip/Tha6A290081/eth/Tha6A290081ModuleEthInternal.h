/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : Tha6A290081ModuleEthInternal.h
 * 
 * Created Date: Oct 15, 2019
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _DRIVER_SRC_IMPLEMENT_CODECHIP_THA6A290081_ETH_THA6A290081MODULEETHINTERNAL_H_
#define _DRIVER_SRC_IMPLEMENT_CODECHIP_THA6A290081_ETH_THA6A290081MODULEETHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/util/AtLongRegisterAccess.h"
#include "../../Tha60290081/eth/Tha60290081ModuleEthInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A290081ModuleEth
    {
    tTha60290081ModuleEth super;
    /* Private data */
    AtLongRegisterAccess  longRegisterAccess;
    }tTha6A290081ModuleEth;

typedef struct tTha6A290081ModuleEth * Tha6A290081ModuleEth;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleEth Tha6A290081ModuleEthNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _DRIVER_SRC_IMPLEMENT_CODECHIP_THA6A290081_ETH_THA6A290081MODULEETHINTERNAL_H_ */


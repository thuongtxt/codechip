/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : Tha6A290081ModulePrbs.c
 *
 * Created Date: Oct 11, 2019
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/att/ThaAttPrbsManager.h"
#include "../../../default/prbs/ThaModulePrbs.h"
#include "../../Tha6A290021/prbs/Tha6A290021ModulePrbsInternal.h"
#include "../../Tha6A000010/prbs/Tha6A000010PrbsEngineInternal.h"
#include "../../Tha6A210031/prbs/Tha6A210031ModulePrbsInternal.h"
#include "../../Tha60210011/pw/Tha60210011ModulePw.h"
#include "../../../../generic/common/AtChannelInternal.h"
#include "Tha6A290081ModulePrbsRegs.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A290081ModulePrbs
    {
    tTha6A290021ModulePrbs super;
    }tTha6A290081ModulePrbs;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePrbsMethods  m_AtModulePrbsOverride;
static tTha6A000010ModulePrbsMethods m_Tha6A000010ModulePrbsOverride;
/*static tAtPrbsEngineMethods    m_AtPrbsEngineOverride;*/

/* Save super implementation */
static const tTha6A000010ModulePrbsMethods *m_Tha6A000010ModulePrbsMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 PrbsCounterReg(Tha6A000010ModulePrbs self, eBool isHo, eBool r2c)
    {
    AtUnused(isHo);
    AtUnused(self);
    return r2c ? cReg_moncntr2clo : cReg_moncntrolo;
    }

static eBool NeedShiftUp(Tha6A000010ModulePrbs self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 PrbsGenReg(Tha6A000010ModulePrbs self, eBool isHo)
    {
    AtUnused(isHo);
    AtUnused(self);
    return cReg_prbsgenctrl4lo;
    }

static uint32 PrbsMonReg(Tha6A000010ModulePrbs self, eBool isHo)
    {
    AtUnused(self);
    AtUnused(isHo);
    return cReg_prbsmonctrl4lo;
    }

static uint32 PrbsFixPatternReg(Tha6A000010ModulePrbs self, eBool isHo, eBool isTx)
    {
    AtUnused(isHo);
    AtUnused(self);
    return isTx? cReg_prbsgenfxptdatlo : cReg_prbsmonfxptdatlo;
    }

static uint32 PrbsDelayConfigReg(Tha6A000010ModulePrbs self, eBool isHo, uint8 slice)
    {
    AtUnused(self);
    AtUnused(isHo);
    return cReg_rtmcidcfglo + (uint32) (slice*65536);
    }

static uint32 PrbsMaxDelayReg(Tha6A000010ModulePrbs self,eBool isHo, eBool r2c, uint8 slice)
    {
    uint32 address = 0;
    AtUnused(self);
    AtUnused(isHo);

    address = r2c ? cReg_mxdelayregr2clo : cReg_mxdelayregrolo;
    return address + (uint32) (slice*65536);
    }

static uint32 PrbsMinDelayReg(Tha6A000010ModulePrbs self, eBool isHo, eBool r2c, uint8 slice)
    {
    uint32 address = 0;
    AtUnused(self);
    AtUnused(isHo);

    address =  r2c ? cReg_mindelayregr2clo : cReg_mindelayregrolo;
    return address + (uint32) (slice*65536);
    }

static AtAttPrbsManager AttPrbsManagerCreate(AtModulePrbs self)
    {
    AtUnused(self);
    return ThaAttPrbsManagerNew(self);
    }

static eBool CounterIsSupported(Tha6A000010ModulePrbs self, uint16 counterType)
    {
    if ((counterType == cAtPrbsEngineCounterRxSync) ||
        (counterType == cAtPrbsEngineCounterRxErrorFrame))
        return cAtTrue;

    AtUnused(self);
    return cAtFalse;
    }

static uint32 PrbsEngineIdFromPw(Tha6A000010ModulePrbs self, AtPw pw)
    {
    AtUnused(self);
    return AtChannelIdGet((AtChannel)pw);
    }

static uint32 PrbsEngineOffset(Tha6A000010ModulePrbs self, AtPw pw)
    {
    uint8 slice = 0;
    uint32 hwIdInSlice = 0;
    AtUnused(self);

    if (Tha60210011PwCircuitSliceAndHwIdInSliceGet(pw, &slice, &hwIdInSlice) != cAtOk)
        mChannelLog(pw, cAtLogLevelCritical, "Cannot get circuit slice and HW ID in slice");

    return ((uint32)(slice * 65536) + AtChannelHwIdGet((AtChannel)pw));
    }

static uint32 PrbsEngineAlarmClear(Tha6A000010ModulePrbs self, AtPw pw)
    {
    uint32 base      = Tha6A000010ModulePrbsMonReg(self, cAtFalse);
    uint32 address   = base + PrbsEngineOffset(self, pw);
    uint32 regValue  = mChannelHwRead(pw, address, cAtModulePrbs);

    return regValue;
    }

static uint32 PrbsEngineAlarmGet(Tha6A000010ModulePrbs self, AtPw pw)
    {
    uint32 regValue  = PrbsEngineAlarmClear(self, pw);

    if (regValue & c_prbsmonctrl4lo_oerr_Mask)
        return cAtPrbsEngineAlarmTypeError;

    if ((regValue & c_prbsmonctrl4lo_osyn_Mask) == 0)
        return cAtPrbsEngineAlarmTypeLossSync;

    return 0;
    }

static uint32 EngineAlarmGet(Tha6A000010ModulePrbs self, AtPw pw)
    {
    return PrbsEngineAlarmGet(self, pw);
    }

static uint32 EngineAlarmHistoryGet(Tha6A000010ModulePrbs self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return 0;
    }

static uint8 ModeSw2Hw(uint8 mode)
    {
    if (mode == cAtPrbsModePrbsSeq)
        return  0x3;

    if (mode == cAtPrbsModePrbsFixedPattern3Bytes)
        return  0x0;

    if ((mode == cAtPrbsModePrbsFixedPattern1Byte)  ||
        (mode == cAtPrbsModePrbsFixedPattern2Bytes) ||
        (mode == cAtPrbsModePrbsFixedPattern4Bytes))
        return  0x1;

    if (mode == cAtPrbsModePrbs15)
        return 0x2;

    return cInvalidUint8;
    }

static eAtPrbsMode ModeHw2Sw(uint8 modePrbs)
    {
    if (modePrbs == 0)
        return cAtPrbsModePrbsSeq;

    if (modePrbs == 1)
        return cAtPrbsModePrbsFixedPattern1Byte;

    if (modePrbs == 2)
        return cAtPrbsModePrbs15;

    if (modePrbs == 3)
        return cAtPrbsModePrbsSeq;

    return cAtPrbsModeInvalid;
    }

static eBool ModeIsSupported(uint8 modePrbs)
    {
    if ((modePrbs == cAtPrbsModePrbsSeq) ||
        (modePrbs == cAtPrbsModePrbsFixedPattern1Byte) ||
        (modePrbs == cAtPrbsModePrbs15) ||
        (modePrbs == cAtPrbsModePrbsSeq))
        return cAtTrue;
    return cAtFalse;
    }

static eAtRet EngineModeSet(Tha6A000010ModulePrbs self, AtPw pw, uint8 mode, eBool isTxDirection)
    {
    uint32 base, address, regValue;

    if (ModeIsSupported(mode) == cAtFalse)
        return cAtErrorModeNotSupport;

    base      = isTxDirection ? Tha6A000010ModulePrbsGenReg(self, cAtFalse) :
                                Tha6A000010ModulePrbsMonReg(self, cAtFalse);
    address   = base + PrbsEngineOffset(self, pw);
    regValue  = mChannelHwRead(pw, address, cAtModulePrbs);
    mRegFieldSet(regValue, c_prbsmonctrl4lo_cfgmode3_, ModeSw2Hw(mode));
    mChannelHwWrite(pw, address, regValue, cAtModulePrbs);

    return cAtOk;
    }

static uint32 EngineModeGet(Tha6A000010ModulePrbs self, AtPw pw, eBool isTxDirection)
    {
    uint32 base      = isTxDirection ? Tha6A000010ModulePrbsGenReg(self, cAtFalse) :
                                       Tha6A000010ModulePrbsMonReg(self, cAtFalse);
    uint32 address   = base + PrbsEngineOffset(self, pw);
    uint32 regValue  = mChannelHwRead(pw, address, cAtModulePrbs);

    return ModeHw2Sw((uint8)mRegField(regValue, c_prbsmonctrl4lo_cfgmode3_));
    }

static eAtRet EngineFixPatternSet(Tha6A000010ModulePrbs self, AtPw pw, uint32 fixPattern, eBool isTxDirection)
    {
    uint32 base = Tha6A000010ModulePrbsFixPatternReg(self, cAtFalse, isTxDirection);
    uint32 address   = base + PrbsEngineOffset(self, pw);
    uint32 regValue  = mChannelHwRead(pw, address, cAtModulePrbs);

    mRegFieldSet(regValue, c_prbsgenfxptdatlo_fxptdat_, fixPattern);
    mChannelHwWrite(pw, address, regValue, cAtModulePrbs);
    return cAtOk;
    }

static uint32 EngineFixPatternGet(Tha6A000010ModulePrbs self, AtPw pw, eBool isTxDirection)
    {
    uint32 base = Tha6A000010ModulePrbsFixPatternReg(self, cAtFalse, isTxDirection);
    uint32 address   = base + PrbsEngineOffset(self, pw);
    return  mChannelHwRead(pw, address, cAtModulePrbs);
    }

static void ClearCounterDatabase(AtPrbsEngine engine)
    {
    Tha6A000010PrbsEngineCounterLatch(engine, 0);
    Tha6A000010PrbsEngineNumberCounterLatchReset(engine);
    }

static uint32 EngineCounterGet(Tha6A000010ModulePrbs self, AtPrbsEngine engine, AtPw pw, uint16 counterType, eBool r2c)
    {
    uint32 base      = Tha6A000010ModulePrbsCounterReg(self, cAtFalse, r2c);
    uint32 address   = base + PrbsEngineOffset(self, pw);
    uint32 regValue, counterValue;

    if (Tha6A000010PrbsEngineNumberCounterLatch(engine) == 0)
        Tha6A000010PrbsEngineCounterLatch(engine, mChannelHwRead(pw, address, cAtModulePrbs));

    counterValue = 0;
    if (counterType == cAtPrbsEngineCounterRxSync)
        {
        regValue = Tha6A000010PrbsEngineCounterLatchGet(engine);
        Tha6A000010PrbsEngineCounterLatchIncrease(engine);
        counterValue = mRegField(regValue, c_moncntrolo_goodcnt_);
        }

    if (counterType == cAtPrbsEngineCounterRxErrorFrame)
        {
        regValue = Tha6A000010PrbsEngineCounterLatchGet(engine);
        Tha6A000010PrbsEngineCounterLatchIncrease(engine);
        counterValue = mRegField(regValue, c_moncntrolo_badcnt_);
        }

    /* Get full counter, clear database for next time */
    if (Tha6A000010PrbsEngineNumberCounterLatch(engine) > 1)
        ClearCounterDatabase(engine);

    return counterValue;
    }

static void OverrideAtModulePrbs(AtModulePrbs self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePrbsOverride, mMethodsGet(self), sizeof(m_AtModulePrbsOverride));
        mMethodOverride(m_AtModulePrbsOverride, AttPrbsManagerCreate);
        /*mMethodOverride(m_AtModulePrbsOverride, SdhVcPrbsEngineCreate);*/
        }

    mMethodsSet(self, &m_AtModulePrbsOverride);
    }

static void OverrideTha6A000010ModulePrbs(Tha6A000010ModulePrbs self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha6A000010ModulePrbsMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6A000010ModulePrbsOverride, m_Tha6A000010ModulePrbsMethods, sizeof(m_Tha6A000010ModulePrbsOverride));
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsCounterReg);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, NeedShiftUp);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsGenReg);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsMonReg);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsFixPatternReg);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsDelayConfigReg);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsMaxDelayReg);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsMinDelayReg);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, CounterIsSupported);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsEngineIdFromPw);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, EngineCounterGet);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, EngineFixPatternGet);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, EngineFixPatternSet);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, EngineModeGet);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, EngineModeSet);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, EngineAlarmHistoryGet);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, EngineAlarmGet);
        }

    mMethodsSet(self, &m_Tha6A000010ModulePrbsOverride);
    }

static void Override(AtModulePrbs self)
    {
    OverrideAtModulePrbs(self);
    OverrideTha6A000010ModulePrbs((Tha6A000010ModulePrbs)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290081ModulePrbs);
    }

static AtModulePrbs ObjectInit(AtModulePrbs self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6A290021ModulePrbsObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePrbs Tha6A290081ModulePrbsNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePrbs newObject = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newObject == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newObject, device);
    }

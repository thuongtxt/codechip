/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : Tha6A290081ModuleSdhInternal.h
 * 
 * Created Date: Oct 8, 2019
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _DRIVER_SRC_IMPLEMENT_CODECHIP_THA6A290081_SDH_THA6A290081MODULESDHINTERNAL_H_
#define _DRIVER_SRC_IMPLEMENT_CODECHIP_THA6A290081_SDH_THA6A290081MODULESDHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290081/sdh/Tha60290081ModuleSdhInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A290081ModuleSdh
    {
    tTha60290081ModuleSdh super;
    AtLongRegisterAccess attLongRegisterAccess;
    }tTha6A290081ModuleSdh;

typedef struct tTha6A290081ModuleSdh * Tha6A290081ModuleSdh;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleSdh Tha6A290081ModuleSdhNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _DRIVER_SRC_IMPLEMENT_CODECHIP_THA6A290081_SDH_THA6A290081MODULESDHINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : Tha6A290081ModuleSdh.c
 *
 * Created Date: Oct 8, 2019
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/sdh/ThaModuleSdh.h"
#include "../../Tha60290081/sdh/Tha60290081ModuleSdhInternal.h"
#include "Tha6A290081ModuleSdhInternal.h"
#include "../../../../generic/util/AtLongRegisterAccess.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleSdhMethods  m_AtModuleSdhOverride;

/* Save super implementation */
static const tAtModuleSdhMethods    *m_AtModuleSdhMethods  = NULL;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 *AttSdhHoldRegistersGet(Tha6A290081ModuleSdh self, uint16 *numberOfHoldRegisters)
    {
    static uint32 holdRegisters[] = {0x100008};
    AtUnused(self);

    if (numberOfHoldRegisters)
        *numberOfHoldRegisters = 1;

    return holdRegisters;
    }

static AtLongRegisterAccess AttSdhLongRegisterAccessCreate(Tha6A290081ModuleSdh self)
    {
    uint16 numHoldRegisters;
    uint32 *holdRegisters = AttSdhHoldRegistersGet(self, &numHoldRegisters);

    return AtDefaultLongRegisterAccessNew(holdRegisters, numHoldRegisters, holdRegisters, numHoldRegisters);
    }

static void SetupData(AtModuleSdh self)
    {
    Tha6A290081ModuleSdh module = (Tha6A290081ModuleSdh)self;
    if (module->attLongRegisterAccess == NULL)
        module->attLongRegisterAccess = AttSdhLongRegisterAccessCreate(module);
    }

static void OverrideAtModuleSdh(AtModuleSdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleSdhMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleSdhOverride, m_AtModuleSdhMethods, sizeof(m_AtModuleSdhOverride));

      /*  mMethodOverride(m_AtModuleSdhOverride, ChannelCreate);*/
        }

    mMethodsSet(self, &m_AtModuleSdhOverride);
    }

static void Override(AtModuleSdh self)
    {
    OverrideAtModuleSdh(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290081ModuleSdh);
    }

static AtModuleSdh ObjectInit(AtModuleSdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290081ModuleSdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    /* Setup internal data */
    SetupData(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSdh Tha6A290081ModuleSdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSdh newObject = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newObject == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newObject, device);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : Tha6A210081DeviceInternal.h
 *
 * Created Date: Jan 02, 2018
 *
 * Description : 6A210012 Device internal definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A210081DEVICEINTERNAL_H_
#define _THA6A210081DEVICEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290081/man/Tha60290081DeviceInternal.h"
#include "../../../../generic/man/AtDeviceInternal.h"
#include "../sdh/Tha6A290081ModuleSdhInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A290081Device *Tha6A290081Device;

typedef struct tTha6A290081Device
    {
    tTha60290081Device super;
    }tTha6A290081Device;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDevice Tha6A290081DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A210081DEVICEINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAN
 *
 * File        : Tha6A290081Device.c
 *
 * Created Date: Dec 17, 2018
 *
 * Description : 6A290081 Device
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/man/Tha60210011Device.h"
#include "../../../default/prbs/ThaModulePrbs.h"
#include "../../../default/sdh/ThaModuleSdh.h"
#include "../../../default/encap/ThaModuleEncap.h"
#include "Tha6A290081DeviceInternal.h"
#include "Tha6A290081Device.h"
#include "../../Tha60210031/man/Tha60210031Device.h"
#include "../cla/Tha6A290081ModuleCla.h"
#include "../pdh/Tha6A290081ModulePdhInternal.h"
#include "../eth/Tha6A290081ModuleEthInternal.h"
#include "../concate/Tha6A290081ModuleConcateInternal.h"
#include "../../Tha60290021/xc/Tha60290021ModuleXc.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceMethods          m_AtDeviceOverride;
static tTha60210011DeviceMethods m_Tha60210011DeviceOverride;

/* Super implementations */
static const tAtDeviceMethods  *m_AtDeviceMethods  = NULL;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    uint32 _moduleId = (uint32)moduleId;
    eThaPhyModule phyModule = (eThaPhyModule)moduleId;

    if (_moduleId  == cAtModuleSdh)    return (AtModule)Tha6A290081ModuleSdhNew(self);
    if (_moduleId  == cAtModulePrbs)   return (AtModule)Tha6A290081ModulePrbsNew(self);
    if (moduleId  == cAtModulePdh)     return (AtModule)Tha6A290081ModulePdhNew(self);
    if (moduleId  == cAtModuleEth)     return (AtModule)Tha6A290081ModuleEthNew(self);
    if (moduleId == cAtModuleConcate)  return (AtModule)Tha6A290081ModuleConcateNew(self);
    if (moduleId == cAtModuleEncap)    return (AtModule)Tha6A290081ModuleEncapNew(self);
    if (moduleId  == cAtModulePw)     return (AtModule)Tha6A290081ModulePwNew(self);

    if (phyModule == cThaModuleCla)     return Tha6A290081ModuleClaNew(self);
    if (phyModule == cThaModulePda)     return Tha6A290081ModulePdaNew(self);
    if (phyModule == cThaModuleMap)     return Tha6A290081ModuleMapNew(self);
    if (phyModule == cThaModuleDemap)   return Tha6A290081ModuleDemapNew(self);

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static const eAtModule *AllSupportedModulesGet(AtDevice self, uint8 *numModules)
    {
    static const eAtModule supportedModules[] = {cAtModuleSdh,
                                                 cAtModulePdh,
                                                 cAtModulePrbs,
                                                 cAtModuleXc,
                                                 cAtModulePw,
                                                 cAtModuleEth,
                                                 cAtModuleEncap,
                                                 cThaModulePda,
                                                 cAtModuleConcate};

    if (numModules)
        *numModules = mCount(supportedModules);

    AtUnused(self);
    return supportedModules;
    }

static eBool ModuleIsSupported(AtDevice self, eAtModule moduleId)
    {
    uint32 _moduleId = (uint32)moduleId;

    AtUnused(self);
    switch (_moduleId)
        {
        case cThaModuleDemap: return cAtTrue;
        case cThaModuleMap:   return cAtTrue;
        case cThaModuleOcn:   return cAtTrue;
        case cThaModulePoh:   return cAtTrue;
        case cThaModulePda:   return cAtTrue;
        case cThaModuleCdr:   return cAtTrue;
        case cThaModulePwe:   return cAtTrue;
        case cAtModulePdh:   return cAtTrue;
        case cThaModuleCla:   return cAtTrue;
        case cAtModuleConcate:   return cAtTrue;
        case cAtModuleEncap:   return cAtTrue;
        case cAtModulePw:   return cAtTrue;
        case cThaModulePmc:   return cAtTrue;
        default:
            break;
        }

    switch (_moduleId)
        {
        case cAtModulePdh:    return cAtTrue;
        case cAtModuleSdh :   return cAtTrue;
        case cAtModulePw:     return cAtTrue;
        case cAtModulePrbs:   return cAtTrue;
        case cAtModuleXc:     return cAtTrue;
        case cAtModuleEth:    return cAtTrue;
        case cAtModuleBer:    return cAtTrue;

        default:
            break;
        }

    return cAtFalse;
    }


static eAtRet PweHwFlush(Tha60210011Device self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet Init(AtDevice self)
    {
    eAtRet ret = m_AtDeviceMethods->Init(self);
    ret |= Tha60290021ModuleXcHideModeSet((AtModuleXc)AtDeviceModuleGet(self, cAtModuleXc), cTha60290021XcHideModeNone);
    return ret;
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(tAtDeviceMethods));

        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, Init);
        mMethodOverride(m_AtDeviceOverride, ModuleIsSupported);
        mMethodOverride(m_AtDeviceOverride, AllSupportedModulesGet);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static void OverrideTha60210011Device(AtDevice self)
    {
    Tha60210011Device this = (Tha60210011Device)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011DeviceOverride, mMethodsGet(this), sizeof(m_Tha60210011DeviceOverride));

        mMethodOverride(m_Tha60210011DeviceOverride, PweHwFlush);
        }

    mMethodsSet(this, &m_Tha60210011DeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtDevice(self);
    OverrideTha60210011Device(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290081Device);
    }

static AtDevice ObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    if (Tha60290081DeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha6A290081DeviceNew(AtDriver driver, uint32 productCode)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDevice == NULL)
        return NULL;

    return ObjectInit(newDevice, driver, productCode);
    }

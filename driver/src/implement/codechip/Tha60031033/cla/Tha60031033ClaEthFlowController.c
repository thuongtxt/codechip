/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60031033ClaEthFlowController.c
 *
 * Created Date: Dec 10, 2013
 *
 * Description : CLA ETH Flow controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/cla/controllers/ThaClaEthFlowControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60031033ClaEthFlowController
    {
    tThaClaEthFlowController super;
    }tTha60031033ClaEthFlowController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaClaControllerMethods m_ThaClaControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool PartIsUnused(ThaClaController self, uint8 partId)
    {
    const uint8 cPppPartId = 0;
	AtUnused(self);
    return (partId != cPppPartId) ? cAtTrue : cAtFalse;
    }

static void OverrideThaClaController(ThaClaEthFlowController self)
    {
    ThaClaController controller = (ThaClaController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaControllerOverride, mMethodsGet(controller), sizeof(m_ThaClaControllerOverride));

        mMethodOverride(m_ThaClaControllerOverride, PartIsUnused);
        }

    mMethodsSet(controller, &m_ThaClaControllerOverride);
    }

static void Override(ThaClaEthFlowController self)
    {
    OverrideThaClaController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60031033ClaEthFlowController);
    }

static ThaClaEthFlowController ObjectInit(ThaClaEthFlowController self, ThaModuleCla cla)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaClaEthFlowControllerObjectInit(self, cla) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaClaEthFlowController Tha60031033ClaEthFlowControllerNew(ThaModuleCla cla)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaClaEthFlowController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, cla);
    }


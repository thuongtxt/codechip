/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : Tha60031033ModuleCla.h
 * 
 * Created Date: Nov 29, 2013
 *
 * Description : CLA Module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60031033MODULECLA_H_
#define _THA60031033MODULECLA_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/cla/pw/ThaModuleClaPwInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint8 Tha60031033ModuleClaPppEthPortId(ThaModuleCla self);
uint8 Tha60031033ModuleClaPwEthPortId(ThaModuleCla self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60031033MODULECLA_H_ */


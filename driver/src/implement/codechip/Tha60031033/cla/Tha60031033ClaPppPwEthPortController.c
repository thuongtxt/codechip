/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60031033ClaPppPwEthPortController.c
 *
 * Created Date: Nov 29, 2013
 *
 * Description : ETH Port controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/cla/controllers/ThaClaPppPwEthPortControllerInternal.h"
#include "../cla/Tha60031033ModuleCla.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60031033ClaPppPwEthPortController
    {
    tThaClaPppPwEthPortController super;
    }tTha60031033ClaPppPwEthPortController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaClaPppPwEthPortControllerMethods m_ThaClaPppPwEthPortControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60031033ClaPppPwEthPortController);
    }

static uint8 PppEthPortId(ThaClaPppPwEthPortController self)
    {
    ThaModuleCla claModule = ThaClaControllerModuleGet((ThaClaController)self);
    return Tha60031033ModuleClaPppEthPortId(claModule);
    }

static uint8 PwEthPortId(ThaClaPppPwEthPortController self)
    {
    ThaModuleCla claModule = ThaClaControllerModuleGet((ThaClaController)self);
    return Tha60031033ModuleClaPwEthPortId(claModule);
    }

static ThaClaEthPortController ControllerOfPort(ThaClaPppPwEthPortController self, AtEthPort port)
    {
    if (AtChannelIdGet((AtChannel)port) == PppEthPortId(self))
        return ThaClaPppPwEthPortControllerPppEthPortController(self);

    if (AtChannelIdGet((AtChannel)port) == PwEthPortId(self))
        return ThaClaPppPwEthPortControllerPwEthPortController(self);

    return NULL;
    }

static ThaClaEthPortController ControllerOfCounter(ThaClaPppPwEthPortController self, AtEthPort port, eThaClaEthPortCounterType counterType)
    {
	AtUnused(counterType);
    return ControllerOfPort(self, port);
    }

static ThaClaEthPortController PppEthPortControllerCreate(ThaClaPppPwEthPortController self)
    {
    return Tha60031032ClaPppEthPortControllerNew(ThaClaControllerModuleGet((ThaClaController)self));
    }

static ThaClaEthPortController PwEthPortControllerCreate(ThaClaPppPwEthPortController self)
    {
    return ThaStmPwProductClaPwEthPortControllerV2New(ThaClaControllerModuleGet((ThaClaController)self));
    }

static void OverrideThaClaPppPwEthPortController(ThaClaEthPortController self)
    {
    ThaClaPppPwEthPortController portController = (ThaClaPppPwEthPortController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaPppPwEthPortControllerOverride, mMethodsGet(portController), sizeof(m_ThaClaPppPwEthPortControllerOverride));

        mMethodOverride(m_ThaClaPppPwEthPortControllerOverride, ControllerOfCounter);
        mMethodOverride(m_ThaClaPppPwEthPortControllerOverride, ControllerOfPort);
        mMethodOverride(m_ThaClaPppPwEthPortControllerOverride, PppEthPortControllerCreate);
        mMethodOverride(m_ThaClaPppPwEthPortControllerOverride, PwEthPortControllerCreate);
        }

    mMethodsSet(portController, &m_ThaClaPppPwEthPortControllerOverride);
    }

static void Override(ThaClaEthPortController self)
    {
    OverrideThaClaPppPwEthPortController(self);
    }

static ThaClaEthPortController ObjectInit(ThaClaEthPortController self, ThaModuleCla cla)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaClaPppPwEthPortControllerObjectInit(self, cla) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaClaEthPortController Tha60031033ClaPppPwEthPortControllerNew(ThaModuleCla cla)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaClaEthPortController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newController, cla);
    }

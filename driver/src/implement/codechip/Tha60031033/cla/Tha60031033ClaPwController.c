/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60031033ClaPwController.c
 *
 * Created Date: Dec 10, 2013
 *
 * Description : PW controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/cla/controllers/ThaClaPwControllerInternal.h"
#include "../../default/ThaStmPwProduct/cla/ThaStmPwProductClaPwController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60031033ClaPwController
    {
    tThaStmPwProductClaPwController super;
    }tTha60031033ClaPwController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaClaControllerMethods   m_ThaClaControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool PartIsUnused(ThaClaController self, uint8 partId)
    {
    const uint8 cPwPartId = 1;
	AtUnused(self);
    return (partId != cPwPartId) ? cAtTrue : cAtFalse;
    }

static void OverrideThaClaController(ThaClaPwController self)
    {
    ThaClaController claController = (ThaClaController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaControllerOverride, mMethodsGet(claController), sizeof(m_ThaClaControllerOverride));

        mMethodOverride(m_ThaClaControllerOverride, PartIsUnused);
        }

    mMethodsSet(claController, &m_ThaClaControllerOverride);
    }

static void Override(ThaClaPwController self)
    {
    OverrideThaClaController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60031033ClaPwController);
    }

static ThaClaPwController ObjectInit(ThaClaPwController self, ThaModuleCla cla)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductClaPwControllerObjectInit(self, cla) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaClaPwController Tha60031033ClaPwControllerNew(ThaModuleCla cla)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaClaPwController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newController, cla);
    }

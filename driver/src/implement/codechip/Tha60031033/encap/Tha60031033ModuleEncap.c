/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Encap
 *
 * File        : Tha60031033ModuleEncap.c
 *
 * Created Date: Nov 29, 2013
 *
 * Description : ENCAP module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60031032/encap/Tha60031032ModuleEncap.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60031033ModuleEncap
    {
    tTha60031032ModuleEncap super;
    }tTha60031033ModuleEncap;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleEncapMethods  m_AtModuleEncapOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60031033ModuleEncap);
    }

static uint16 MaxChannelsGet(AtModuleEncap self)
    {
	AtUnused(self);
    return 1024 / 2;
    }

static void OverrideAtModuleEncap(AtModuleEncap self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleEncapOverride, mMethodsGet(self), sizeof(m_AtModuleEncapOverride));
        mMethodOverride(m_AtModuleEncapOverride, MaxChannelsGet);
        }

    mMethodsSet(self, &m_AtModuleEncapOverride);
    }

static void Override(AtModuleEncap self)
    {
    OverrideAtModuleEncap(self);
    }

static AtModuleEncap ObjectInit(AtModuleEncap self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60031032ModuleEncapObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleEncap Tha60031033ModuleEncapNew(AtDevice device)
    {
    /* Allocate memory for this module */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEncap newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Constructor */
    return ObjectInit(newModule, device);
    }

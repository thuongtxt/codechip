/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60031033ModulePdh.c
 *
 * Created Date: Nov 29, 2013
 *
 * Description : PDH module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60031031/pdh/Tha60031031ModulePdh.h"
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../../default/sdh/ThaModuleSdh.h"
#include "../cla/Tha60031033ModuleCla.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60031033ModulePdh
    {
    tTha60031031ModulePdh super;
    }tTha60031033ModulePdh;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods        m_AtModuleOverride;
static tThaModulePdhMethods    m_ThaModulePdhOverride;

/* Save super implementations */
static const tAtModuleMethods *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60031033ModulePdh);
    }

static eBool HasM13(ThaModulePdh self, uint8 partId)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaModuleCla claModule = (ThaModuleCla)AtDeviceModuleGet(device, cThaModuleCla);
    if (partId == Tha60031033ModuleClaPppEthPortId(claModule))
        return cAtTrue;

    return cAtFalse;
    }

static uint8 PartId(AtModule self, uint32 localAddress)
    {
	AtUnused(self);
    return (localAddress & cBit24) ? 1 : 0;
    }

static AtLongRegisterAccess LongRegisterAccess(AtModule self, uint32 localAddress)
    {
    AtDevice device = AtModuleDeviceGet(self);

    /* PPP part uses its hold registers set */
    if (PartId(self, localAddress) == 0)
        return m_AtModuleMethods->LongRegisterAccess(self, localAddress);

    return AtDeviceGlobalLongRegisterAccess(device);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, LongRegisterAccess);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void OverrideThaModulePdh(AtModule self)
    {
    ThaModulePdh pdhModule = (ThaModulePdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdhOverride, mMethodsGet(pdhModule), sizeof(m_ThaModulePdhOverride));

        mMethodOverride(m_ThaModulePdhOverride, HasM13);
        }

    mMethodsSet(pdhModule, &m_ThaModulePdhOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModulePdh(self);
    OverrideAtModule(self);
    }

static AtModulePdh ObjectInit(AtModulePdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60031031ModulePdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override((AtModule)self);
    m_methodsInit = 1;

    return self;
    }

AtModulePdh Tha60031033ModulePdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newModule, device);
    }

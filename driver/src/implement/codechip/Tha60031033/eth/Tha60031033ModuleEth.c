/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60031033ModuleEth.c
 *
 * Created Date: Nov 29, 2013
 *
 * Description : ETH module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pw/ThaModulePw.h"
#include "../../Tha60031032/eth/Tha60031032ModuleEthInternal.h"
#include "../../../default/man/ThaDeviceInternal.h"
#include "../cla/Tha60031033ModuleCla.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60031033ModuleEth
    {
    tTha60031032ModuleEth super;
    }tTha60031033ModuleEth;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleEthMethods  m_AtModuleEthOverride;
static tThaModuleEthMethods m_ThaModuleEthOverride;
static tAtModuleMethods     m_AtModuleOverride;

/* Save super implementations */
static const tAtModuleMethods     *m_AtModuleMethods     = NULL;
static const tThaModuleEthMethods *m_ThaModuleEthMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtEthPort PortCreate(AtModuleEth self, uint8 portId)
    {
    return ThaEthPortNew(portId, self);
    }

static eBool PortShouldByPassFcs(ThaModuleEth self, uint8 portId)
    {
    ThaModuleCla claModule = (ThaModuleCla)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModuleCla);

    if (portId == Tha60031033ModuleClaPppEthPortId(claModule))
        return cAtTrue;
    if (portId == Tha60031033ModuleClaPwEthPortId(claModule))
        return cAtFalse;

    return m_ThaModuleEthMethods->PortShouldByPassFcs(self, portId);
    }

static uint32 MaxNumMpFlows(ThaModuleEth self)
    {
	AtUnused(self);
    return 1024;
    }

static uint8 PartId(AtModule self, uint32 localAddress)
    {
	AtUnused(self);
    return (localAddress & cBit24) ? 1 : 0;
    }

static AtLongRegisterAccess LongRegisterAccess(AtModule self, uint32 localAddress)
    {
    AtDevice device = AtModuleDeviceGet(self);

    /* PPP part uses its hold registers set */
    if (PartId(self, localAddress) == 0)
        return m_AtModuleMethods->LongRegisterAccess(self, localAddress);

    return AtDeviceGlobalLongRegisterAccess(device);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60031033ModuleEth);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, LongRegisterAccess);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void OverrideAtModuleEth(AtModuleEth self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleEthOverride, mMethodsGet(self), sizeof(m_AtModuleEthOverride));

        mMethodOverride(m_AtModuleEthOverride, PortCreate);
        }

    mMethodsSet(self, &m_AtModuleEthOverride);
    }

static void OverrideThaModuleEth(AtModuleEth self)
    {
    ThaModuleEth ethModule = (ThaModuleEth)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleEthMethods = mMethodsGet(ethModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleEthOverride, mMethodsGet(ethModule), sizeof(m_ThaModuleEthOverride));

        mMethodOverride(m_ThaModuleEthOverride, MaxNumMpFlows);
        mMethodOverride(m_ThaModuleEthOverride, PortShouldByPassFcs);
        }

    mMethodsSet(ethModule, &m_ThaModuleEthOverride);
    }

static void Override(AtModuleEth self)
    {
    OverrideAtModuleEth(self);
    OverrideThaModuleEth(self);
    OverrideAtModule((AtModule)self);
    }

static AtModuleEth ObjectInit(AtModuleEth self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60031032ModuleEthObjectInit(self, device) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleEth Tha60031033ModuleEthNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEth newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newModule, device);
    }

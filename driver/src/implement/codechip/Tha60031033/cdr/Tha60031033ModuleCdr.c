/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : Tha60031033ModuleCdr.c
 *
 * Created Date: Dec 10, 2013
 *
 * Description : CDR module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../default/ThaStmPwProduct/cdr/ThaStmPwProductModuleCdr.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60031033ModuleCdr
    {
    tThaStmPwProductModuleCdr super;
    }tTha60031033ModuleCdr;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods m_AtModuleOverride;

/* Save super implementations */
static const tAtModuleMethods *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60031033ModuleCdr);
    }

static uint8 RegisterPartId(AtModule self, uint32 localAddress)
    {
	AtUnused(self);
    return (localAddress & cBit24) ? 1 : 0;
    }

static AtLongRegisterAccess LongRegisterAccess(AtModule self, uint32 localAddress)
    {
    AtDevice device = AtModuleDeviceGet(self);

    /* PPP part uses its hold registers set */
    if (RegisterPartId(self, localAddress) == 0)
        return m_AtModuleMethods->LongRegisterAccess(self, localAddress);

    return AtDeviceGlobalLongRegisterAccess(device);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, LongRegisterAccess);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductModuleCdrObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60031033ModuleCdrNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

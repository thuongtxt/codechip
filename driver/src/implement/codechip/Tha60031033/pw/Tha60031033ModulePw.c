/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60031033ModulePw.c
 *
 * Created Date: Dec 3, 2013
 *
 * Description : PW module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../default/ThaStmPwProduct/pw/ThaStmPwProductModulePw.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60031033ModulePw
    {
    tThaStmPwProductModulePw super;
    }tTha60031033ModulePw;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePwMethods        m_AtModulePwOverride;
static tThaModulePwMethods       m_ThaModulePwOverride;

/* Save super implementation */
static const tAtModulePwMethods *m_AtModulePwMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxPwsGet(AtModulePw self)
    {
    return m_AtModulePwMethods->MaxPwsGet(self) / 2;
    }

static uint16 NumPwsPerPart(ThaModulePw self)
    {
    return (uint16)AtModulePwMaxPwsGet((AtModulePw)self);
    }

static eBool PwMbitCounterIsSupported(ThaModulePw self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static void OverrideAtModulePw(AtModulePw self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModulePwMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePwOverride, m_AtModulePwMethods, sizeof(m_AtModulePwOverride));
        mMethodOverride(m_AtModulePwOverride, MaxPwsGet);
        }

    mMethodsSet(self, &m_AtModulePwOverride);
    }

static void OverrideThaModulePw(AtModulePw self)
    {
    ThaModulePw pwModule = (ThaModulePw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePwOverride, mMethodsGet(pwModule), sizeof(m_ThaModulePwOverride));
        mMethodOverride(m_ThaModulePwOverride, NumPwsPerPart);
        mMethodOverride(m_ThaModulePwOverride, PwMbitCounterIsSupported);
        }

    mMethodsSet(pwModule, &m_ThaModulePwOverride);
    }

static void Override(AtModulePw self)
    {
    OverrideAtModulePw(self);
    OverrideThaModulePw(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60031033ModulePw);
    }

static AtModulePw ObjectInit(AtModulePw self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductModulePwObjectInit((AtModulePw)self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePw Tha60031033ModulePwNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePw newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }


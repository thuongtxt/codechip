/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : COS
 * 
 * File        : Tha60031033ModuleCos.h
 * 
 * Created Date: Mar 1, 2015
 *
 * Description : COS module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60031033MODULECOS_H_
#define _THA60031033MODULECOS_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint8 Tha60031033ModuleCosPppEthPortId(ThaModuleCos self);
uint8 Tha60031033ModuleCosPwEthPortId(ThaModuleCos self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60031033MODULECOS_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : Tha60031033CosEthPortController.c
 *
 * Created Date: Dec 3, 2013
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/cos/controllers/ThaCosEthPortControllerInternal.h"
#include "Tha60031033ModuleCos.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60031033CosPppPwEthPortController
    {
    tThaCosPppPwEthPortController super;
    }tTha60031033CosPppPwEthPortController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaCosPppPwEthPortControllerMethods m_ThaCosPppPwEthPortControllerOverride;

/*--------------------------- Forward decosrations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60031033CosPppPwEthPortController);
    }

static uint8 PppEthPortId(ThaCosPppPwEthPortController self)
    {
    ThaModuleCos cosModule = ThaCosControllerModuleGet((ThaCosController)self);
    return Tha60031033ModuleCosPppEthPortId(cosModule);
    }

static uint8 PwEthPortId(ThaCosPppPwEthPortController self)
    {
    ThaModuleCos cosModule = ThaCosControllerModuleGet((ThaCosController)self);
    return Tha60031033ModuleCosPwEthPortId(cosModule);
    }

static ThaCosEthPortController ControllerOfPort(ThaCosPppPwEthPortController self, AtEthPort port)
    {
    if (AtChannelIdGet((AtChannel)port) == PppEthPortId(self))
        return ThaCosPppPwEthPortControllerPppEthPortController(self);

    if (AtChannelIdGet((AtChannel)port) == PwEthPortId(self))
        return ThaCosPppPwEthPortControllerPwEthPortController(self);

    return NULL;
    }

static ThaCosEthPortController ControllerOfCounter(ThaCosPppPwEthPortController self, AtEthPort port, eThaCosEthPortCounterType counterType)
    {
	AtUnused(counterType);
    return ControllerOfPort(self, port);
    }

static void OverrideThaCosPppPwEthPortController(ThaCosEthPortController self)
    {
    ThaCosPppPwEthPortController portController = (ThaCosPppPwEthPortController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaCosPppPwEthPortControllerOverride, mMethodsGet(portController), sizeof(m_ThaCosPppPwEthPortControllerOverride));

        mMethodOverride(m_ThaCosPppPwEthPortControllerOverride, ControllerOfCounter);
        mMethodOverride(m_ThaCosPppPwEthPortControllerOverride, ControllerOfPort);
        }

    mMethodsSet(portController, &m_ThaCosPppPwEthPortControllerOverride);
    }

static void Override(ThaCosEthPortController self)
    {
    OverrideThaCosPppPwEthPortController(self);
    }

static ThaCosEthPortController ObjectInit(ThaCosEthPortController self, ThaModuleCos cos)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaCosPppPwEthPortControllerObjectInit(self, cos) == NULL)
        return NULL;

    /* Setup cosss */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaCosEthPortController Tha60031033CosEthPortControllerNew(ThaModuleCos cos)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaCosEthPortController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newController, cos);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60031033Device.c
 *
 * Created Date: Nov 29, 2013
 *
 * Description : Product 60031033
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60031031/man/Tha60031031Device.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60031033Device
    {
    tTha60031031Device super;

    /* Private data */
    }tTha60031033Device;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceMethods         m_AtDeviceOverride;
static tThaDeviceMethods        m_ThaDeviceOverride;

/* Super implementation */
static tAtDeviceMethods        *m_AtDeviceMethods  = NULL;
static const tThaDeviceMethods *m_ThaDeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const eAtModule *AllSupportedModulesGet(AtDevice self, uint8 *numModules)
    {
    static const eAtModule supportedModules[] = {cAtModulePdh,
                                                 cAtModulePw,
                                                 cAtModuleEth,
                                                 cAtModuleRam,
                                                 cAtModuleSdh,
                                                 cAtModuleBer,
                                                 cAtModulePktAnalyzer,
                                                 cAtModuleClock,
                                                 cAtModuleEncap,
                                                 cAtModulePpp};
	AtUnused(self);
    if (numModules)
        *numModules = mCount(supportedModules);

    return supportedModules;
    }

static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    eThaPhyModule phyModule = moduleId;

    if (moduleId  == cAtModulePdh)         return (AtModule)Tha60031033ModulePdhNew(self);
    if (moduleId  == cAtModulePw)          return (AtModule)Tha60031033ModulePwNew(self);
    if (moduleId  == cAtModuleEth)         return (AtModule)Tha60031033ModuleEthNew(self);
    if (moduleId  == cAtModulePktAnalyzer) return (AtModule)Tha60031033ModulePktAnalyzerNew(self);
    if (moduleId  == cAtModuleClock)       return (AtModule)Tha60031033ModuleClockNew(self);
    if (moduleId  == cAtModuleEncap)       return (AtModule)Tha60031033ModuleEncapNew(self);
    if (moduleId  == cAtModulePpp)         return (AtModule)Tha60031033ModulePppNew(self);

    if (phyModule == cThaModuleCos)        return Tha60031033ModuleCosNew(self);
    if (phyModule == cThaModuleCla)        return Tha60031033ModuleClaNew(self);
    if (phyModule == cThaModuleCdr)        return Tha60031033ModuleCdrNew(self);

    /* These modules are rejected by a PW device, this device is merged MLPPP and PW,
     * so need to override here */
    if (phyModule == cThaModuleMpig)       return ThaModuleMpigNew(self);
    if (phyModule == cThaModuleMpeg)       return ThaModuleMpegNew(self);
    if (phyModule == cThaModulePmcMpeg)    return ThaModulePmcMpegNew(self);
    if (phyModule == cThaModulePmcMpig)    return ThaModulePmcMpigNew(self);

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static eBool ModuleIsSupported(AtDevice self, eAtModule moduleId)
    {
    uint32 _moduleId = (uint32)moduleId;
    switch (_moduleId)
        {
        case cThaModuleMpig:    return cAtTrue;
        case cThaModuleMpeg:    return cAtTrue;
        case cThaModulePmcMpeg: return cAtTrue;
        case cThaModulePmcMpig: return cAtTrue;

        default:
            return m_AtDeviceMethods->ModuleIsSupported(self, moduleId);
        }
    }

static uint32 ModulePartOffset(ThaDevice self, eAtModule moduleId, uint8 partId)
    {
    uint32 _moduleId = (uint32)moduleId;
    if ((_moduleId == cAtModulePw)   ||
        (_moduleId == cThaModulePda) ||
        (_moduleId == cThaModuleBmt) ||
        (_moduleId == cThaModulePwe) ||
        (_moduleId == cThaModulePwPmc))
        return ThaDevicePartOffset(self, 1);

    return ThaDevicePartOffset(self, partId);
    }

static AtLongRegisterAccess LongRegisterAccessCreate(AtDevice self, AtModule module)
    {
    uint16 numHoldRegisters;
    uint32 *holdRegisters = AtModuleHoldRegistersGet(module, &numHoldRegisters);
	AtUnused(self);
    if (holdRegisters == NULL)
        return NULL;

    return AtDefaultLongRegisterAccessNew(holdRegisters, numHoldRegisters, holdRegisters, numHoldRegisters);
    }

static uint8 NumPartsOfModule(ThaDevice self, uint32 moduleId)
    {
    uint32 moduleId_ = (uint32)moduleId; /* Just to make compiler be happy */

    if ((moduleId_ == cThaModuleMpig)    ||
        (moduleId_ == cThaModuleMpeg)    ||
        (moduleId_ == cThaModulePmcMpeg) ||
        (moduleId_ == cThaModulePmcMpig) ||
        (moduleId_ == cAtModuleEncap)    ||
        (moduleId_ == cAtModulePpp))
        return 1;

    return m_ThaDeviceMethods->NumPartsOfModule(self, moduleId_);
    }

static AtSSKeyChecker SSKeyCheckerCreate(AtDevice self)
    {
    return Tha60031032SSKeyCheckerNew(self);
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(tAtDeviceMethods));
        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, AllSupportedModulesGet);
        mMethodOverride(m_AtDeviceOverride, ModuleIsSupported);
        mMethodOverride(m_AtDeviceOverride, LongRegisterAccessCreate);
        mMethodOverride(m_AtDeviceOverride, SSKeyCheckerCreate);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static void OverrideThaDevice(AtDevice self)
    {
    ThaDevice device = (ThaDevice)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaDeviceMethods = mMethodsGet(device);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDeviceOverride, m_ThaDeviceMethods, sizeof(m_ThaDeviceOverride));
        mMethodOverride(m_ThaDeviceOverride, ModulePartOffset);
        mMethodOverride(m_ThaDeviceOverride, NumPartsOfModule);
        }

    mMethodsSet(device, &m_ThaDeviceOverride);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60031033Device);
    }

static void Override(AtDevice self)
    {
    OverrideAtDevice(self);
    OverrideThaDevice(self);
    }

static AtDevice ObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60031031DeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha60031033DeviceNew(AtDriver driver, uint32 productCode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newDevice, driver, productCode);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : Tha60031033ModulePpp.c
 *
 * Created Date: Nov 29, 2013
 *
 * Description : PPP module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60031032/ppp/Tha60031032ModulePpp.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60031033ModulePpp
    {
    tTha60031032ModulePpp super;
    }tTha60031033ModulePpp;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePppMethods  m_AtModulePppOverride;

/* Save super implementation */
static const tAtModulePppMethods *m_AtModulePppMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxNumOfMpBundleGet(AtModulePpp self)
    {
    return m_AtModulePppMethods->MaxNumOfMpBundleGet(self) / 2;
    }

static AtPidTable PidTableObjectCreate(AtModulePpp self)
    {
    return Tha60031033PidTableNew((AtModule)self);
    }

static void OverrideAtModulePpp(AtModulePpp self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModulePppMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePppOverride, m_AtModulePppMethods, sizeof(m_AtModulePppOverride));
        mMethodOverride(m_AtModulePppOverride, MaxNumOfMpBundleGet);
        mMethodOverride(m_AtModulePppOverride, PidTableObjectCreate);
        }

    mMethodsSet(self, &m_AtModulePppOverride);
    }

static void Override(AtModulePpp self)
    {
    OverrideAtModulePpp(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60031033ModulePpp);
    }

static AtModulePpp ObjectInit(AtModulePpp self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60031032ModulePppObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePpp Tha60031033ModulePppNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePpp newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newModule, device);
    }

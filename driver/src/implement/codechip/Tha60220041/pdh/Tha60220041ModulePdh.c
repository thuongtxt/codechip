/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60220041ModulePdh.c
 *
 * Created Date: Apr 3, 2015
 *
 * Description : PDH module implementations
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60220031/pdh/Tha60220031ModulePdhInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60220041ModulePdh
    {
    tTha60220031ModulePdh super;
    }tTha60220041ModulePdh;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePdhMethods  m_AtModulePdhOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60220041ModulePdh);
    }

static uint32 NumberOfDe1sGet(AtModulePdh self)
    {
    AtUnused(self);
    return 16;
    }

static void OverrideAtModulePdh(AtModulePdh self)
    {
    AtModulePdh module = (AtModulePdh)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePdhOverride, mMethodsGet(module), sizeof(m_AtModulePdhOverride));
        mMethodOverride(m_AtModulePdhOverride, NumberOfDe1sGet);
        }

    mMethodsSet(module, &m_AtModulePdhOverride);
    }

static void Override(AtModulePdh self)
    {
    OverrideAtModulePdh(self);
    }

static AtModulePdh ObjectInit(AtModulePdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60220031ModulePdhObjectInit((AtModulePdh)self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePdh Tha60220041ModulePdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

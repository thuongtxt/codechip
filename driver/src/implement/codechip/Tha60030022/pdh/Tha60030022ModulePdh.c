/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2011 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60030022ModulePdh.c
 *
 * Created Date: Jun 28, 2013
 *
 * Description : PDH module of product 60030022
 *
 * Notes       :
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pdh/ThaModulePdhReg.h"
#include "../../../default/pdh/ThaModulePdhInternal.h"
#include "../../../default/man/ThaIpCoreInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaModulePdhRxDe1IntrStatus cBit2

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60030022ModulePdh
    {
    tThaModulePdh super;
    }tTha60030022ModulePdh;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods     m_AtModuleOverride;
static tThaModulePdhMethods m_ThaModulePdhOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool HasLiuLoopback(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void RxPdhDe1InterruptProcess(AtModule self, AtHal hal)
    {
    uint8 de1Id;
    uint32 perDe1Status = AtHalNoLockRead(hal, cThaRegPdhRxFrameChannelIntrStatus);
    AtModulePdh modulePdh = (AtModulePdh)self;

    /* Find which channel cause interrupt */
    for (de1Id = 0; de1Id < AtModulePdhNumberOfDe1sGet(modulePdh); de1Id++)
        {
        if (perDe1Status & cThaRegPdhRxFrameChannelIntrStatusMask(de1Id))
            {
            /* Call all listeners register on this channel */
            AtPdhDe1 de1 = AtModulePdhDe1Get(modulePdh, de1Id);
            AtChannelAllAlarmListenersCall((AtChannel)de1,
                                           AtChannelDefectInterruptClear((AtChannel)de1),
                                           AtChannelDefectGet((AtChannel)de1));
            }
        }
    }

static void InterruptProcess(AtModule self, uint32 glbIntr, AtIpCore ipCore)
    {
    AtHal hal = AtIpCoreHalGet(ipCore);
    if (hal == NULL)
        return;

    if (!(glbIntr & cThaModulePdhRxDe1IntrStatus))
        RxPdhDe1InterruptProcess(self, hal);
    }

static eAtRet InterruptEnable(AtModule self, eBool enable)
    {
    /* Enable RX PDH Interrupt */
    uint32 regValue = mModuleHwRead(self, cThaRegPdhGlbIntrEnableCtrl);
    mRegFieldSet(regValue, cThaRegPdhRxDe1IntrEnable, enable ? 1 : 0);
    mModuleHwWrite(self, cThaRegPdhGlbIntrEnableCtrl, regValue);

    /* Enable Rx Framer Master */
    regValue = mModuleHwRead(self, cThaRegPdhRxDe1FrameMasterIntrEnableCtrl);
    mRegFieldSet(regValue, cThaRegPdhRxDe1LOSIntrEnable, enable ? 1 : 0);
    mModuleHwWrite(self, cThaRegPdhRxDe1FrameMasterIntrEnableCtrl, regValue);

    /* Store to Database */
    ((ThaModulePdh)self)->interruptIsEnabled = enable;

    return cAtOk;
    }

static eBool DetectDe1LosByAllZeroPattern(ThaModulePdh self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static void OverrideThaModulePdh(AtModulePdh self)
    {
    ThaModulePdh module = (ThaModulePdh)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdhOverride, mMethodsGet(module), sizeof(m_ThaModulePdhOverride));
        mMethodOverride(m_ThaModulePdhOverride, DetectDe1LosByAllZeroPattern);
        mMethodOverride(m_ThaModulePdhOverride, HasLiuLoopback);
        }

    mMethodsSet(module, &m_ThaModulePdhOverride);
    }

static void OverrideAtModule(AtModulePdh self)
    {
    AtModule module = (AtModule)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(module), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, InterruptProcess);
        mMethodOverride(m_AtModuleOverride, InterruptEnable);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void Override(AtModulePdh self)
    {
    OverrideAtModule(self);
    OverrideThaModulePdh(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60030022ModulePdh);
    }

/* Constructor */
static AtModulePdh ObjectInit(AtModulePdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModulePdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePdh Tha60030022ModulePdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newModule, device);
    }

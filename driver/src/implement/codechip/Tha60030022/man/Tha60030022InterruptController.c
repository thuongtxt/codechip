/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60030022InterruptController.c
 *
 * Created Date: Oct 2, 2013
 *
 * Description : Interrupt controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/intrcontroller/ThaIntrControllerInternal.h"
#include "../../../default/man/ThaDevice.h"

/*--------------------------- Define -----------------------------------------*/
#define cRegChipIntrStatus    0xF00063
#define cRegChipIntrEnable    0xF

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60030022IntrController
    {
    tThaIntrController super;
    }tTha60030022IntrController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tThaIpCoreMethods m_ThaIpCoreOverride;

/* Super implementation */
static const tThaIpCoreMethods *m_ThaIpCoreImplement;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void AllModulesInterruptEnable(ThaIpCore self, uint32 intrMask)
    {
    AtHal hal = AtIpCoreHalGet(mIpCore(self));
    if (hal == NULL)
        return;

    AtHalNoLockWrite(hal, cRegChipIntrEnable, intrMask);
    }

static void PdhHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    uint8 part_i;
    AtHal hal = AtIpCoreHalGet(mIpCore(self));
    ThaDevice device = (ThaDevice)AtIpCoreDeviceGet(mIpCore(self));

    for (part_i = 0; part_i < ThaDeviceNumPartsOfModule(device, cAtModulePdh); part_i++)
        {
        uint32 partOffset   = ThaDeviceModulePartOffset(device, cAtModulePdh, part_i);
        uint32 hwIntrEnable = AtHalNoLockRead(hal, cRegChipIntrEnable + partOffset);

        hwIntrEnable = enable ? (hwIntrEnable | cBit22) : (hwIntrEnable & ~cBit22);
        AtHalWrite(hal, cRegChipIntrEnable + partOffset, hwIntrEnable);
        }
    }

static eBool PdhCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
	AtUnused(self);
    return (intrStatus & cBit2) ? cAtFalse : cAtTrue;
    }

static uint32 InterruptStatusGet(ThaIpCore self)
    {
    AtHal hal = AtIpCoreHalGet(mIpCore(self));
    if (hal == NULL)
        return 0;

    return (AtHalNoLockRead(hal, cRegChipIntrStatus));
    }

static uint32 InterruptDisable(ThaIpCore self)
    {
    uint32 interruptEnable = 0;
    AtHal hal = AtIpCoreHalGet(mIpCore(self));
    if (hal == NULL)
        return 0;

    interruptEnable = AtHalNoLockRead(hal, cRegChipIntrEnable);
    AtHalNoLockWrite(hal, cRegChipIntrEnable, 0);

    return interruptEnable;
    }

static void OverrideThaIpCore(ThaIntrController self)
    {
    ThaIpCore thaIp = (ThaIpCore)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaIpCoreImplement = mMethodsGet(thaIp);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaIpCoreOverride, m_ThaIpCoreImplement, sizeof(m_ThaIpCoreOverride));

        mMethodOverride(m_ThaIpCoreOverride, InterruptStatusGet);
        mMethodOverride(m_ThaIpCoreOverride, InterruptDisable);
        mMethodOverride(m_ThaIpCoreOverride, PdhHwInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, PdhCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, AllModulesInterruptEnable);
        }

    mMethodsSet(thaIp, &m_ThaIpCoreOverride);
    }

static void Override(ThaIntrController self)
    {
    OverrideThaIpCore(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60030022IntrController);
    }

static ThaIntrController ObjectInit(ThaIntrController self, AtIpCore ipCore)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaIntrControllerObjectInit(self, ipCore) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaIntrController Tha60030022IntrControllerNew(AtIpCore ipCore)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaIntrController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, ipCore);
    }

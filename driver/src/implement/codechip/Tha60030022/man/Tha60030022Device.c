/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60030022Device.c
 *
 * Created Date: May 6, 2013
 *
 * Description : Product 60030022 (16 E1)
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../ThaPdhMlppp/man/ThaPdhMlpppDeviceInternal.h"
#include "Tha60030022DeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceMethods m_AtDeviceOverride;
static tThaDeviceMethods m_ThaDeviceOverride;

/* Super implementation */
static tAtDeviceMethods *m_AtDeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60030022Device);
    }

static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    eThaPhyModule phyModule = moduleId;

    if (moduleId == cAtModuleEth) return (AtModule)Tha60030022ModuleEthNew(self);
    if (moduleId == cAtModuleRam) return (AtModule)Tha60030022ModuleRamNew(self);
    if (moduleId == cAtModulePpp) return (AtModule)Tha60030022ModulePppNew(self);
    if (moduleId == cAtModulePdh) return (AtModule)Tha60030022ModulePdhNew(self);

    if (phyModule == cThaModuleCla) return Tha60030022ModuleClaPppNew(self);

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static AtSSKeyChecker SSKeyCheckerCreate(AtDevice self)
    {
    return Tha60030022SSKeyCheckerNew(self);
    }

static ThaIntrController IntrControllerCreate(ThaDevice self, AtIpCore core)
    {
	AtUnused(self);
    return Tha60030022IntrControllerNew(core);
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(m_AtDeviceOverride));

        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, SSKeyCheckerCreate);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static void OverrideThaDevice(AtDevice self)
    {
    ThaDevice device = (ThaDevice)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDeviceOverride, mMethodsGet(device), sizeof(m_ThaDeviceOverride));

        mMethodOverride(m_ThaDeviceOverride, IntrControllerCreate);
        }

    mMethodsSet(device, &m_ThaDeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtDevice(self);
    OverrideThaDevice(self);
    }

AtDevice Tha60030022DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhMlpppDeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha60030022DeviceNew(AtDriver driver, uint32 productCode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha60030022DeviceObjectInit(newDevice, driver, productCode);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : Tha60030022ModulePpp.c
 *
 * Created Date: Jun 24, 2013
 *
 * Description : PPP module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../ThaPdhMlppp/ppp/ThaPdhMlpppModulePppInternal.h"
#include "Tha60030022ModulePppInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePppMethods m_AtModulePppOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxNumOfMpBundleGet(AtModulePpp self)
    {
	AtUnused(self);
    /* This product does not support MLPPP */
    return 0;
    }

static void OverrideAtModulePpp(AtModulePpp self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePppOverride, mMethodsGet(self), sizeof(m_AtModulePppOverride));

        mMethodOverride(m_AtModulePppOverride, MaxNumOfMpBundleGet);
        }

    mMethodsSet(self, &m_AtModulePppOverride);
    }

static void Override(AtModulePpp self)
    {
    OverrideAtModulePpp(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60030022ModulePpp);
    }

AtModulePpp Tha60030022ModulePppObjectInit(AtModulePpp self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhMlpppModulePppObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePpp Tha60030022ModulePppNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePpp newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha60030022ModulePppObjectInit(newModule, device);
    }

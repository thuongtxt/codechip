/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PPP
 * 
 * File        : Tha60030022ModulePppInternal.h
 * 
 * Created Date: Jun 24, 2013
 *
 * Description : PPP module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60030022MODULEPPPINTERNAL_H_
#define _THA60030022MODULEPPPINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60030022ModulePpp
    {
    tThaPdhMlpppModulePpp super;
    }tTha60030022ModulePpp;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePpp Tha60030022ModulePppObjectInit(AtModulePpp self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60030022MODULEPPPINTERNAL_H_ */


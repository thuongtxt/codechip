/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : OCN
 *
 * File        : Tha60060011ModuleOcn.c
 *
 * Created Date: Jan 24, 2014
 *
 * Description : OCN module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/ocn/ThaModuleOcnInternal.h"
#include "../../../default/sdh/ThaModuleSdh.h"
#include "../../../../generic/sdh/AtSdhChannelInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60060011ModuleOcn
    {
    tThaModuleOcn super;
    }tTha60060011ModuleOcn;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleOcnMethods m_ThaModuleOcnOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet StsIdSw2Hw(ThaModuleOcn self, AtSdhChannel channel, eAtModule moduleId, uint8 sts1Id, uint8 *pHwSliceId, uint8 *pHwStsId)
    {
    AtSdhLine line = AtSdhChannelLineObjectGet(channel);
    
	AtUnused(self);
	AtUnused(moduleId);
    *pHwSliceId = 0;
    *pHwStsId = (uint8)((sts1Id * 4) + ThaModuleSdhLineLocalId(line));
    return cAtOk;
    }

static void OverrideThaModuleOcn(AtModule self)
    {
    ThaModuleOcn ocnModule = (ThaModuleOcn)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleOcnOverride, mMethodsGet(ocnModule), sizeof(m_ThaModuleOcnOverride));

        mMethodOverride(m_ThaModuleOcnOverride, StsIdSw2Hw);
        }

    mMethodsSet(ocnModule, &m_ThaModuleOcnOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleOcn(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60060011ModuleOcn);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleOcnObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60060011ModuleOcnNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }


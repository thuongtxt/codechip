/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60060011PdhNxDs0.c
 *
 * Created Date: Aug 23, 2013
 *
 * Description : NxDS0
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pdh/ThaPdhNxDs0.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60060011PdhNxDs0 *Tha60060011PdhNxDs0;

typedef struct tTha60060011PdhNxDs0
    {
    tThaPdhNxDs0 super;
    }tTha60060011PdhNxDs0;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods m_AtChannelOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 NumBlocksNeedToBindToEncapChannel(AtChannel self, AtEncapChannel encapChannel)
    {
    uint8 numBlocks;
    uint8 numDs0s;
	AtUnused(encapChannel);

    numDs0s = ThaPdhNumberOfDs0((AtPdhNxDS0)self);
    numBlocks = (uint8)((numDs0s % 2) ? (numDs0s / 2) + 1 : (numDs0s / 2));

    /* Limit range of number of block */
    if (numBlocks > 16)
        numBlocks = 16;

    /* Set min number of block */
    if (numBlocks == 1)
        numBlocks = 2;

    return numBlocks;
    }

static void OverrideAtChannel(Tha60060011PdhNxDs0 self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(tAtChannelMethods));
        mMethodOverride(m_AtChannelOverride, NumBlocksNeedToBindToEncapChannel);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(Tha60060011PdhNxDs0 self)
    {
    OverrideAtChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60060011PdhNxDs0);
    }

static AtPdhNxDS0 ObjectInit(AtPdhNxDS0 self, AtPdhDe1 de1, uint32 mask)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhNxDs0ObjectInit(self, de1, mask) == NULL)
        return NULL;

    /* Setup class */
    Override((Tha60060011PdhNxDs0)self);
    m_methodsInit = 1;

    return self;
    }

AtPdhNxDS0 Tha60060011PdhNxDs0New(AtPdhDe1 de1, uint32 mask)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhNxDS0 newDs0 = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDs0 == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newDs0, de1, mask);
    }

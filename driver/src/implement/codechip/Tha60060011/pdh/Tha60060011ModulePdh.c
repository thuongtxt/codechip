/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Module Pdh
 *
 * File        : Tha60060011ModulePdh.c
 *
 * Created Date: Jul 31, 2013
 *
 * Description : Module Pdh of product 60060011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pdh/ThaModulePdhInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60060011ModulePdh
    {
    tThaStmModulePdh super;
    }tTha60060011ModulePdh;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePdhMethods     m_AtModulePdhOverride;
static tThaModulePdhMethods    m_ThaModulePdhOverride;

/* Save super implementation */
static const tThaModulePdhMethods *m_ThaModulePdhMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPdhDe1 De1Create(AtModulePdh self, uint32 de1Id)
    {
	AtUnused(de1Id);
	AtUnused(self);
    return NULL;
    }

static AtPdhNxDS0 NxDs0Create(AtModulePdh self, AtPdhDe1 de1, uint32 timeslotBitmap)
    {
	AtUnused(self);
    return Tha60060011PdhNxDs0New(de1, timeslotBitmap);
    }

static eAtRet DefaultDe1Threshold(ThaModulePdh self, tThaCfgPdhDs1E1FrmRxThres *threshold)
    {
    eAtRet ret = m_ThaModulePdhMethods->DefaultDe1Threshold(self, threshold);
    if (ret == cAtOk)
        threshold->lofThres = 3;
    return ret;
    }

static eBool HasM13(ThaModulePdh self, uint8 partId)
    {
    AtUnused(self);
    AtUnused(partId);
    return cAtFalse;
    }

static void OverrideThaModulePdh(AtModulePdh self)
    {
    ThaModulePdh module = (ThaModulePdh)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePdhMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdhOverride, m_ThaModulePdhMethods, sizeof(m_ThaModulePdhOverride));
        mMethodOverride(m_ThaModulePdhOverride, DefaultDe1Threshold);
        mMethodOverride(m_ThaModulePdhOverride, HasM13);
        }

    mMethodsSet(module, &m_ThaModulePdhOverride);
    }

static void OverrideAtModulePdh(AtModulePdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePdhOverride, mMethodsGet(self), sizeof(m_AtModulePdhOverride));
        mMethodOverride(m_AtModulePdhOverride, De1Create);
        mMethodOverride(m_AtModulePdhOverride, NxDs0Create);
        }

    mMethodsSet(self, &m_AtModulePdhOverride);
    }

static void Override(AtModulePdh self)
    {
    OverrideAtModulePdh(self);
    OverrideThaModulePdh(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60060011ModulePdh);
    }

static AtModulePdh ObjectInit(AtModulePdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmModulePdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePdh Tha60060011ModulePdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newModule, device);
    }

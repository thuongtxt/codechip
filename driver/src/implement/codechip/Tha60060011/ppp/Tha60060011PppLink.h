/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PPP
 * 
 * File        : Tha60060011PppLink.h
 * 
 * Created Date: Mar 1, 2015
 *
 * Description : PPP link
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60060011PPPLINK_H_
#define _THA60060011PPPLINK_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPppLink.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha60060011PppLinkPidTableTdmToPsnPppHeaderSet(AtPppLink self, uint32 entryId, uint32 pppHeader, uint8 headerLength);
eAtRet Tha60060011PppLinkPidTableTdmToPsnEthTypeSet(AtPppLink self, uint32 entryId, uint32 ethType);
eAtRet Tha60060011PppLinkPidTablePsnToTdmPppHeaderSet(AtPppLink self, uint32 entryId, uint32 pppHeader, uint8 headerLength);
eAtRet Tha60060011PppLinkPidTablePsnToTdmEthTypeSet(AtPppLink self, uint32 entryId, uint32 ethType);
uint32 Tha60060011PppLinkPidTableTdmToPsnPppHeaderGet(AtPppLink self, uint32 entryId, uint8 *headerLength);
uint32 Tha60060011PppLinkPidTableTdmToPsnEthTypeGet(AtPppLink self, uint32 entryId);
uint32 Tha60060011PppLinkPidTablePsnToTdmPppHeaderGet(AtPppLink self, uint32 entryId, uint8 *headerLength);
uint32 Tha60060011PppLinkPidTablePsnToTdmEthTypeGet(AtPppLink self, uint32 entryId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60060011PPPLINK_H_ */


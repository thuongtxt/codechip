/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : Tha60060011PidTableEntry.c
 *
 * Created Date: Oct 16, 2013
 *
 * Description : PID table entry
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/ppp/AtPidTableInternal.h"
#include "../../../default/ppp/ThaPidTableEntryInternal.h"
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../../default/ppp/ThaPidTableInternal.h"
#include "../../../default/ppp/ThaPppLinkInternal.h"
#include "Tha60060011PidTableEntry.h"

/*--------------------------- Define -----------------------------------------*/
#define cAtLongRegMaxSize                   8

/* CLA */
#define cThaClaBcsPppHeaderControl            0x44a000

#define cThaClaBcsPppHeaderLengthMask         cBit2_0 /* Bit34_32 */
#define cThaClaBcsPppHeaderLengthShift        0
#define cThaClaBcsPppHdr1LengthDwordIndex     1

#define cThaClaBcsPppHeaderValueMask          cBit31_0
#define cThaClaBcsPppHeaderValueShift         0

#define cThaClaBcsPppEthTypeControl           0x44b000

#define cThaClaBcsPppEthTypeMask(entryId)     (cBit15_0 << (entryId * 16))
#define cThaClaBcsPppEthTypeShift(entryId)    (entryId * 16)

/* MPIG */
#define cThaMpigDatBcsHeaderControl            0x850100

#define cThaMpigBcsPppHeaderEnMask             cBit2 /* Bit34 */
#define cThaMpigBcsPppHeaderEnShift            2

#define cThaMpigBcsPppHeaderLengthMask         cBit1_0 /* Bit33_32 */
#define cThaMpigBcsPppHeaderLengthShift        0
#define cThaMpigBcsPppHeaderLengthDwordIndex   1

#define cThaMpigBcsPppHeaderValueMask          cBit31_0
#define cThaMpigBcsPppHeaderValueShift         0

#define cThaMpigBcsPppEthTypeControl           0x846000

#define cThaMpigBcsPppEthTypeMask(entryId)     (cBit15_0 << (entryId * 16))
#define cThaMpigBcsPppEthTypeShift(entryId)    (entryId * 16)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60060011PidTableEntry
    {
    tThaPidTableEntry super;
    }tTha60060011PidTableEntry;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtPidTableEntryMethods m_AtPidTableEntryOverride;

/* Save super implementation */
static const tAtPidTableEntryMethods *m_AtPidTableEntryMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 PppLinkId(AtPidTableEntry self)
    {
    ThaPidTablePppLink pidTable = (ThaPidTablePppLink)AtPidTableEntryTableGet(self);
    return ThaPppLinkDefaultOffset((ThaPppLink)ThaPidTablePppLinkGet(pidTable));
    }

static uint32 EgressDefaultOffset(AtPidTableEntry self)
    {
    return AtPidTableEntryIndexGet(self) * 0x80 + PppLinkId(self);
    }

static uint32 IngressDefaultOffset(AtPidTableEntry self)
    {
    return AtPidTableEntryIndexGet(self) * 0x100 + PppLinkId(self);
    }

static eAtRet PsnToTdmPidSet(AtPidTableEntry self, uint32 pid)
    {
    uint32 regAddr = cThaClaBcsPppHeaderControl + EgressDefaultOffset(self);
    uint32 regVal  = AtPidTableEntryRead(self, regAddr, cThaModuleCla);
    mRegFieldSet(regVal, cThaClaBcsPppHeaderValue, pid);
    AtPidTableEntryWrite(self, regAddr, regVal, cThaModuleCla);

    return cAtOk;
    }

static uint32 PsnToTdmPidGet(AtPidTableEntry self)
    {
    uint32 regAddr = cThaClaBcsPppHeaderControl + EgressDefaultOffset(self);
    uint32 regVal  = AtPidTableEntryRead(self, regAddr, cThaModuleCla);
    return mRegField(regVal, cThaClaBcsPppHeaderValue);
    }

static eAtRet PsnToTdmEthTypeSet(AtPidTableEntry self, uint32 ethType)
    {
    uint32 entryId  = AtPidTableEntryIndexGet(self);
    uint32 regAddr = cThaClaBcsPppEthTypeControl + PppLinkId(self);
    uint32 regVal  = AtPidTableEntryRead(self, regAddr, cThaModuleCla);
    mFieldIns(&regVal, cThaClaBcsPppEthTypeMask(entryId), cThaClaBcsPppEthTypeShift(entryId), ethType);
    AtPidTableEntryWrite(self, regAddr, regVal, cThaModuleCla);

    return cAtOk;
    }

static uint32 PsnToTdmEthTypeGet(AtPidTableEntry self)
    {
    uint16 ethType;
    uint32  entryId = AtPidTableEntryIndexGet(self);
    uint32 regAddr = cThaClaBcsPppEthTypeControl + PppLinkId(self);
    uint32 regVal  = AtPidTableEntryRead(self, regAddr, cThaModuleCla);
    mFieldGet(regVal, cThaClaBcsPppEthTypeMask(entryId), cThaClaBcsPppEthTypeShift(entryId), uint16, &ethType);
    return ethType;
    }

static eAtRet TdmToPsnPidSet(AtPidTableEntry self, uint32 pid)
    {
    uint32 regAddr = cThaMpigDatBcsHeaderControl + IngressDefaultOffset(self);
    uint32 regVal  = AtPidTableEntryRead(self, regAddr, cAtModulePpp);
    mRegFieldSet(regVal, cThaMpigBcsPppHeaderValue, pid);
    AtPidTableEntryWrite(self, regAddr, regVal, cAtModulePpp);

    return cAtOk;
    }

static uint32 TdmToPsnPidGet(AtPidTableEntry self)
    {
    uint32 regAddr = cThaMpigDatBcsHeaderControl + IngressDefaultOffset(self);
    uint32 regVal  = AtPidTableEntryRead(self, regAddr, cAtModulePpp);
    return mRegField(regVal, cThaMpigBcsPppHeaderValue);
    }

static eAtRet TdmToPsnEthTypeSet(AtPidTableEntry self, uint32 ethType)
    {
    uint32 entryId = AtPidTableEntryIndexGet(self);
    uint32 regAddr = cThaMpigBcsPppEthTypeControl + PppLinkId(self);
    uint32 regVal  = AtPidTableEntryRead(self, regAddr, cAtModulePpp);
    mFieldIns(&regVal, cThaMpigBcsPppEthTypeMask(entryId), cThaMpigBcsPppEthTypeShift(entryId), ethType);
    AtPidTableEntryWrite(self, regAddr, regVal, cAtModulePpp);

    return cAtOk;
    }

static uint32 TdmToPsnEthTypeGet(AtPidTableEntry self)
    {
    uint16 ethType;
    uint32 entryId = AtPidTableEntryIndexGet(self);
    uint32 regAddr = cThaMpigBcsPppEthTypeControl + PppLinkId(self);
    uint32 regVal  = AtPidTableEntryRead(self, regAddr, cAtModulePpp);
    mFieldGet(regVal, cThaMpigBcsPppEthTypeMask(entryId), cThaMpigBcsPppEthTypeShift(entryId), uint16, &ethType);
    return ethType;
    }

static eAtRet PsnToTdmEnable(AtPidTableEntry self, eBool enable)
    {
	AtUnused(self);
    return enable ? cAtOk : cAtErrorModeNotSupport;
    }

static AtModule ClaModule(AtPidTableEntry self)
    {
    AtPidTable pidTable = AtPidTableEntryTableGet(self);
    AtModule pppModule = AtPidTableModuleGet(pidTable);
    return AtDeviceModuleGet(AtModuleDeviceGet(pppModule), cThaModuleCla);
    }

static AtModule MpigModule(AtPidTableEntry self)
    {
    AtPidTable pidTable = AtPidTableEntryTableGet(self);
    AtModule pppModule = AtPidTableModuleGet(pidTable);
    return AtDeviceModuleGet(AtModuleDeviceGet(pppModule), cThaModuleMpig);
    }

static void OverrideAtPidEntryTable(AtPidTableEntry self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPidTableEntryMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPidTableEntryOverride, m_AtPidTableEntryMethods, sizeof(m_AtPidTableEntryOverride));

        mMethodOverride(m_AtPidTableEntryOverride, PsnToTdmPidSet);
        mMethodOverride(m_AtPidTableEntryOverride, PsnToTdmEthTypeSet);
        mMethodOverride(m_AtPidTableEntryOverride, PsnToTdmPidGet);
        mMethodOverride(m_AtPidTableEntryOverride, PsnToTdmEthTypeGet);
        mMethodOverride(m_AtPidTableEntryOverride, TdmToPsnPidSet);
        mMethodOverride(m_AtPidTableEntryOverride, TdmToPsnEthTypeSet);
        mMethodOverride(m_AtPidTableEntryOverride, TdmToPsnEthTypeGet);
        mMethodOverride(m_AtPidTableEntryOverride, TdmToPsnPidGet);
        mMethodOverride(m_AtPidTableEntryOverride, PsnToTdmEnable);
        }

    mMethodsSet(self, &m_AtPidTableEntryOverride);
    }

static void Override(AtPidTableEntry self)
    {
    OverrideAtPidEntryTable(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60060011PidTableEntry);
    }

static AtPidTableEntry ObjectInit(AtPidTableEntry self, uint32 entryIndex, AtPidTable table)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPidTableEntryObjectInit(self, entryIndex, table) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPidTableEntry Tha60060011PidTableEntryNew(AtPidTable self, uint32 entryIndex)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPidTableEntry entry = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (entry == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(entry, entryIndex, self);
    }

eAtRet Tha60060011PidTablePsnToTdmHeaderLengthSet(AtPidTableEntry self, uint8 headerLength)
    {
    uint32 longValue[cAtLongRegMaxSize];
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 regAddr;
    AtModule claModule;
    AtIpCore core;

    if (self == NULL)
        return cAtErrorNullPointer;

    claModule = ClaModule(self);
    core = AtDeviceIpCoreGet(AtModuleDeviceGet(claModule), AtModuleDefaultCoreGet(claModule));
    regAddr = cThaClaBcsPppHeaderControl + EgressDefaultOffset(self);
    mMethodsGet(osal)->MemInit(osal, longValue, 0, sizeof(longValue));
    mModuleHwLongRead(claModule, regAddr, longValue, cAtLongRegMaxSize, core);
    mRegFieldSet(longValue[cThaClaBcsPppHdr1LengthDwordIndex], cThaClaBcsPppHeaderLength, headerLength);
    mModuleHwLongWrite(claModule, regAddr, longValue, cAtLongRegMaxSize, core);

    return cAtOk;
    }

uint8 Tha60060011PidTablePsnToTdmHeaderLengthGet(AtPidTableEntry self)
    {
    uint32 longValue[cAtLongRegMaxSize];
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 regAddr;
    AtModule claModule;
    AtIpCore core;

    if (self == NULL)
        return 0;

    claModule = ClaModule(self);
    core = AtDeviceIpCoreGet(AtModuleDeviceGet(claModule), AtModuleDefaultCoreGet(claModule));
    regAddr = cThaClaBcsPppHeaderControl + EgressDefaultOffset(self);
    mMethodsGet(osal)->MemInit(osal, longValue, 0, sizeof(longValue));
    mModuleHwLongRead(claModule, regAddr, longValue, cAtLongRegMaxSize, core);
    return (uint8)mRegField(longValue[cThaClaBcsPppHdr1LengthDwordIndex], cThaClaBcsPppHeaderLength);
    }

eAtRet Tha60060011PidTableTdmToPsnHeaderLengthSet(AtPidTableEntry self, uint8 headerLength)
    {
    uint32 longValue[cAtLongRegMaxSize];
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 regAddr;
    AtModule mpigModule;
    AtIpCore core;

    if (self == NULL)
        return cAtErrorNullPointer;

    mpigModule = MpigModule(self);
    core = AtDeviceIpCoreGet(AtModuleDeviceGet(mpigModule), AtModuleDefaultCoreGet(mpigModule));
    regAddr = cThaMpigDatBcsHeaderControl + IngressDefaultOffset(self);
    mMethodsGet(osal)->MemInit(osal, longValue, 0, sizeof(longValue));
    mModuleHwLongRead(mpigModule, regAddr, longValue, cAtLongRegMaxSize, core);
    mRegFieldSet(longValue[cThaMpigBcsPppHeaderLengthDwordIndex], cThaMpigBcsPppHeaderLength, headerLength - 1);
    mRegFieldSet(longValue[cThaMpigBcsPppHeaderLengthDwordIndex], cThaMpigBcsPppHeaderEn, 0x1);
    mModuleHwLongWrite(mpigModule, regAddr, longValue, cAtLongRegMaxSize, core);

    return cAtOk;
    }

uint8 Tha60060011PidTableTdmToPsnHeaderLengthGet(AtPidTableEntry self)
    {
    uint32 longValue[cAtLongRegMaxSize];
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 regAddr;
    AtModule mpigModule;
    AtIpCore core;

    if (self == NULL)
        return 0;

    mpigModule = MpigModule(self);
    core = AtDeviceIpCoreGet(AtModuleDeviceGet(mpigModule), AtModuleDefaultCoreGet(mpigModule));
    regAddr = cThaMpigDatBcsHeaderControl + IngressDefaultOffset(self);
    mMethodsGet(osal)->MemInit(osal, longValue, 0, sizeof(longValue));
    mModuleHwLongRead(mpigModule, regAddr, longValue, cAtLongRegMaxSize, core);
    return (uint8)mRegField(longValue[cThaMpigBcsPppHeaderLengthDwordIndex], cThaMpigBcsPppHeaderLength);
    }

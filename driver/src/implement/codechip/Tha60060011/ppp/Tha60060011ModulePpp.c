/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Module PPP
 *
 * File        : Tha60060011ModulePpp.c
 *
 * Created Date: Jul 20, 2013
 *
 * Description : Module PPP of product 60060011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../ThaPdhMlppp/ppp/ThaPdhMlpppModulePppInternal.h"
#include "Tha60060011MpigReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60060011ModulePpp
    {
    tThaPdhMlpppModulePpp super;
    }tTha60060011ModulePpp;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePppMethods  m_AtModulePppOverride;
static tThaModulePppMethods m_ThaModulePppOverride;

/* Save super implementations */
static const tAtModulePppMethods  *m_AtModulePppMethods  = NULL;
static const tThaModulePppMethods *m_ThaModulePppMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
mDefineMaskShifts(FlwEnb)
mDefineMaskShifts(MPIGDATLookupFlwID)

static uint32 MaxNumOfMpBundleGet(AtModulePpp self)
    {
	AtUnused(self);
    return 32;
    }

static uint32 MpigMaxNumBlocks(ThaModulePpp self)
    {
	AtUnused(self);
    return 4096;
    }

static AtMpBundle MpBundleObjectCreate(AtModulePpp self, uint32 bundleId)
    {
    return Tha60060011MpBundleNew(bundleId, self);
    }

static AtPidTable PidTableObjectCreate(AtModulePpp self)
    {
	AtUnused(self);
    return NULL;
    }

static void OverrideAtModulePpp(AtModulePpp self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModulePppMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePppOverride, m_AtModulePppMethods, sizeof(m_AtModulePppOverride));
        mMethodOverride(m_AtModulePppOverride, MaxNumOfMpBundleGet);
        mMethodOverride(m_AtModulePppOverride, MpBundleObjectCreate);
        mMethodOverride(m_AtModulePppOverride, PidTableObjectCreate);
        }

    mMethodsSet(self, &m_AtModulePppOverride);
    }

static void OverrideThaModulePpp(AtModulePpp self)
    {
    ThaModulePpp thaPppModule = (ThaModulePpp)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePppMethods = mMethodsGet(thaPppModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePppOverride, m_ThaModulePppMethods, sizeof(m_ThaModulePppOverride));
        mMethodOverride(m_ThaModulePppOverride, MpigMaxNumBlocks);
        mBitFieldOverrides(m_ThaModulePppOverride, FlwEnb);
        mBitFieldOverrides(m_ThaModulePppOverride, MPIGDATLookupFlwID);
        }

    mMethodsSet(thaPppModule, &m_ThaModulePppOverride);
    }

static void Override(AtModulePpp self)
    {
    OverrideThaModulePpp(self);
    OverrideAtModulePpp(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60060011ModulePpp);
    }

static AtModulePpp ObjectInit(AtModulePpp self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhMlpppModulePppObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePpp Tha60060011ModulePppNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePpp newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newModule, device);
    }

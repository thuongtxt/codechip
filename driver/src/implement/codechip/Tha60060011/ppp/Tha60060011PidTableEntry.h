/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PPP
 * 
 * File        : Tha60060011PidTableEntry.h
 * 
 * Created Date: Mar 1, 2015
 *
 * Description : PID table entry
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60060011PIDTABLEENTRY_H_
#define _THA60060011PIDTABLEENTRY_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPidTable.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha60060011PidTablePsnToTdmHeaderLengthSet(AtPidTableEntry self, uint8 headerLength);
uint8 Tha60060011PidTablePsnToTdmHeaderLengthGet(AtPidTableEntry self);
eAtRet Tha60060011PidTableTdmToPsnHeaderLengthSet(AtPidTableEntry self, uint8 headerLength);
uint8 Tha60060011PidTableTdmToPsnHeaderLengthGet(AtPidTableEntry self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60060011PIDTABLEENTRY_H_ */


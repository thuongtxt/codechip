/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : Tha60060011PidTable.c
 *
 * Created Date: Sep 26, 2013
 *
 * Description : PID table
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/ppp/ThaPidTableInternal.h"
#include "../../../default/ppp/ThaPidTableEntryInternal.h"
#include "../../../default/man/ThaDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60060011PidTable
    {
    tThaPidTablePppLink super;
    }tTha60060011PidTable;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtPidTableMethods  m_AtPidTableOverride;

/* To save super implementation */
static const tAtPidTableMethods  *m_AtPidTableMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxNumEntries(AtPidTable self)
    {
	AtUnused(self);
    return 2;
    }

static AtPidTableEntry EntryObjectCreate(AtPidTable self, uint32 entryIndex)
    {
    return Tha60060011PidTableEntryNew(self, entryIndex);
    }

static void OverrideAtPidTable(AtPidTable self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPidTableMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPidTableOverride, m_AtPidTableMethods, sizeof(m_AtPidTableOverride));

        mMethodOverride(m_AtPidTableOverride, MaxNumEntries);
        mMethodOverride(m_AtPidTableOverride, EntryObjectCreate);
        }

    mMethodsSet(self, &m_AtPidTableOverride);
    }

static void Override(AtPidTable self)
    {
    OverrideAtPidTable(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60060011PidTable);
    }

static AtPidTable ObjectInit(AtPidTable self, AtPppLink pppLink)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPidTablePppLinkObjectInit(self, pppLink) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPidTable Tha60060011PidTableNew(AtPppLink pppLink)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPidTable table = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (table == NULL)
        return NULL;

    return ObjectInit(table, pppLink);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MPEG
 * 
 * File        : Tha60060011MpegRet.h
 * 
 * Created Date: Aug 7, 2013
 *
 * Description : MPEG registers of product 60060011
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _Tha60060011MPEGREG_H_
#define _Tha60060011MPEGREG_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/ppp/ThaMpegReg.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#undef  cThaMPEGPppLinkCtrl1BundleIDMask
#undef  cThaMPEGPppLinkCtrl1BundleIDShift
#define cThaMPEGPppLinkCtrl1BundleIDMask               cBit5_1
#define cThaMPEGPppLinkCtrl1BundleIDShift              1

#ifdef __cplusplus
}
#endif
#endif /* _Tha60060011MPEGREG_H_ */


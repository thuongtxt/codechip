/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Mlppp Bundle
 *
 * File        : Tha60060011MpBundle.c
 *
 * Created Date: Aug 6, 2013
 *
 * Description : Mlppp Bundle of product 60060011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/ppp/ThaMpBundleInternal.h"
#include "Tha60060011MpigReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60060011MpBundle
    {
    tThaMpBundle super;
    }tTha60060011MpBundle;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaMpBundleMethods m_ThaMpBundleOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 CounterAddress(ThaMpBundle self, uint32 regAddress)
    {
    uint32 noChangedField = cBit31_12 & regAddress;
    uint32 changedField   = regAddress & cBit11_0;
    uint32 newAddress     = noChangedField | (changedField << 1);
    return AtChannelIdGet((AtChannel)self) + newAddress;
    }

static void OverrideThaMpBundle(AtMpBundle self)
    {
    ThaMpBundle mpBundle = (ThaMpBundle)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaMpBundleOverride, mMethodsGet(mpBundle), sizeof(m_ThaMpBundleOverride));
        mMethodOverride(m_ThaMpBundleOverride, CounterAddress);
        }

    mMethodsSet(mpBundle, &m_ThaMpBundleOverride);
    }

static void Override(AtMpBundle self)
    {
    OverrideThaMpBundle(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60060011MpBundle);
    }

static AtMpBundle ObjectInit(AtMpBundle self, uint32 channelId, AtModulePpp module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaMpBundleObjectInit(self, channelId, (AtModule)module) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtMpBundle Tha60060011MpBundleNew(uint32 channelId, AtModulePpp module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtMpBundle newBundle = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newBundle == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newBundle, channelId, module);
    }


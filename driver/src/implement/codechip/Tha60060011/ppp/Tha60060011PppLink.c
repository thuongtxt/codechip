/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP Link
 *
 * File        : Tha60060011PppLink.c
 *
 * Created Date: Aug 5, 2013
 *
 * Description : Ppp Link of product 60060011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/ppp/ThaPppLinkInternal.h"
#include "../../../default/ppp/ThaPidTable.h"
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../../default/util/ThaUtil.h"

#include "Tha60060011MpigReg.h"
#include "Tha60060011MpegReg.h"
#include "Tha60060011PidTableEntry.h"
#include "Tha60060011PppLink.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaPppPppHeaderIpV4Packet             0xff030021
#define cThaPppPppHeaderIpV6Packet             0xff030057

#define cThaPppEthernetTypeIpV4                0x0800
#define cThaPppEthernetTypeIpV6                0x86DD

#define cInvalidValue 0xCAFECAFE

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60060011PppLink
    {
    tThaPppLink super;
    }tTha60060011PppLink;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPppLinkMethods m_ThaPppLinkOverride;
static tAtHdlcLinkMethods m_AtHdlcLinkOverride;
static tAtPppLinkMethods  m_AtPppLinkOverride;
static tAtChannelMethods  m_AtChannelOverride;

/* Save super implementation */
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet PidByPass(AtHdlcLink self, eBool pidByPass)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr;
    AtDevice device = AtChannelDeviceGet((AtChannel)AtHdlcLinkHdlcChannelGet(self));
    ThaModuleEncap encapModule = (ThaModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);

    regAddr = cThaRegMPIGDATLookupLinkCtrl + mDefaultOffset(self);
    mChannelHwLongRead(self, regAddr, longRegVal, cThaNumDwordsInLongReg, cThaModuleMpig);
    mFieldIns(&(longRegVal[cThaPidModeDwordIndex]),
              mLinkMask(encapModule, PidMode),
              mLinkShift(encapModule, PidMode),
              mBoolToBin(pidByPass));
    mChannelHwLongWrite(self, regAddr, longRegVal, cThaNumDwordsInLongReg, cThaModuleMpig);

    return cAtOk;
    }

static uint32 EntryDefaultPppHeader(uint32 entryIndex)
    {
    if (entryIndex == 0) return cThaPppPppHeaderIpV4Packet;
    if (entryIndex == 1) return cThaPppPppHeaderIpV6Packet;

    return cInvalidValue;
    }

static uint32 EntryDefaultEthType(uint32 entryIndex)
    {
    if (entryIndex == 0) return cThaPppEthernetTypeIpV4;
    if (entryIndex == 1) return cThaPppEthernetTypeIpV6;

    return cInvalidValue;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = cAtOk;
    AtHdlcChannel hdlcChannel = AtHdlcLinkHdlcChannelGet((AtHdlcLink)self);
    AtPppLink pppLink = (AtPppLink)self;
    AtPidTable table = AtPppLinkPidTableGet(pppLink);
    uint32 numEntry = AtPidTableMaxNumEntries(table);
    static const uint8 cDefaultPppHeaderLength = 4;
    uint32 entry_i;

    ret |= m_AtChannelMethods->Init(self);
    ret |= AtHdlcChannelAddressInsertionEnable(hdlcChannel, cAtFalse);
    ret |= AtHdlcChannelControlInsertionEnable(hdlcChannel, cAtFalse);

    for (entry_i = 0; entry_i < numEntry; entry_i++)
        {
        Tha60060011PppLinkPidTablePsnToTdmEthTypeSet(pppLink, entry_i, EntryDefaultEthType(entry_i));
        Tha60060011PppLinkPidTablePsnToTdmPppHeaderSet(pppLink, entry_i, EntryDefaultPppHeader(entry_i), cDefaultPppHeaderLength);
        Tha60060011PppLinkPidTableTdmToPsnEthTypeSet(pppLink, entry_i, EntryDefaultEthType(entry_i));
        Tha60060011PppLinkPidTableTdmToPsnPppHeaderSet(pppLink, entry_i, EntryDefaultPppHeader(entry_i), cDefaultPppHeaderLength);
        }

    return ret;
    }

static AtPidTable PidTableObjectCreate(ThaPppLink self)
    {
    return Tha60060011PidTableNew((AtPppLink)self);
    }

static eAtModulePppRet TxProtocolFieldCompress(AtPppLink self, eBool compress)
    {
	AtUnused(self);
    return compress ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool TxProtocolFieldIsCompressed(AtPppLink self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static void OverrideAtChannel(AtPppLink self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, Init);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtHdlcLink(AtPppLink self)
    {
    AtHdlcLink hdlcLink = (AtHdlcLink)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtHdlcLinkOverride, mMethodsGet(hdlcLink), sizeof(m_AtHdlcLinkOverride));
        mMethodOverride(m_AtHdlcLinkOverride, PidByPass);
        }

    mMethodsSet(hdlcLink, &m_AtHdlcLinkOverride);
    }

static void OverrideAtPppLink(AtPppLink self)
    {
    if (!m_methodsInit)
        {
        /* Reuse all super implementation */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPppLinkOverride, mMethodsGet(self), sizeof(m_AtPppLinkOverride));

        /* Override */
        mMethodOverride(m_AtPppLinkOverride, TxProtocolFieldCompress);
        mMethodOverride(m_AtPppLinkOverride, TxProtocolFieldIsCompressed);
        }

    mMethodsSet(self, &m_AtPppLinkOverride);
    }

static void OverrideThaPppLink(AtPppLink self)
    {
    ThaPppLink link = (ThaPppLink)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPppLinkOverride, mMethodsGet(link), sizeof(m_ThaPppLinkOverride));

        mMethodOverride(m_ThaPppLinkOverride, PidTableObjectCreate);
        }

    mMethodsSet(link, &m_ThaPppLinkOverride);
    }

static void Override(AtPppLink self)
    {
    OverrideThaPppLink(self);
    OverrideAtHdlcLink(self);
    OverrideAtPppLink(self);
    OverrideAtChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60060011PppLink);
    }

static AtPppLink ObjectInit(AtPppLink self, AtHdlcChannel hdlcChannel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPppLinkObjectInit((AtPppLink)self, hdlcChannel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPppLink Tha60060011PppLinkNew(AtHdlcChannel hdlcChannel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPppLink newLink = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newLink, hdlcChannel);
    }

static eBool HeaderLengthIsValid(uint8 headerLength)
    {
    return ((headerLength > 0) && (headerLength < 5)) ? cAtTrue : cAtFalse;
    }

static uint32 PppHeaderHwToSw(uint32 pppHeader, uint8 headerLength)
    {
    if (headerLength == 1)
        return pppHeader & cBit7_0;

    if (headerLength == 2)
        return pppHeader & cBit15_0;

    if (headerLength == 3)
        return pppHeader & cBit23_0;

    if (headerLength == 4)
        return pppHeader & cBit31_0;

    return 0;
    }

eAtRet Tha60060011PppLinkPidTableTdmToPsnPppHeaderSet(AtPppLink self, uint32 entryId, uint32 pppHeader, uint8 headerLength)
    {
    AtPidTableEntry entry;
    if (self == NULL)
        return cAtErrorNullPointer;

    if (!HeaderLengthIsValid(headerLength))
        return cAtErrorOutOfRangParm;

    entry = AtPidTableEntryGet(AtPppLinkPidTableGet(self), entryId);
    AtPidTableEntryTdmToPsnPidSet(entry, pppHeader);

    /* Configure header length */
    Tha60060011PidTableTdmToPsnHeaderLengthSet(entry, headerLength);
    return cAtOk;
    }

eAtRet Tha60060011PppLinkPidTableTdmToPsnEthTypeSet(AtPppLink self, uint32 entryId, uint32 ethType)
    {
    if (self == NULL)
        return cAtErrorNullPointer;
    return AtPidTableEntryTdmToPsnEthTypeSet(AtPidTableEntryGet(AtPppLinkPidTableGet(self), entryId), ethType);
    }

eAtRet Tha60060011PppLinkPidTablePsnToTdmPppHeaderSet(AtPppLink self, uint32 entryId, uint32 pppHeader, uint8 headerLength)
    {
    AtPidTableEntry entry;
    if (self == NULL)
        return cAtErrorNullPointer;

    if (!HeaderLengthIsValid(headerLength))
        return cAtErrorOutOfRangParm;

    entry = AtPidTableEntryGet(AtPppLinkPidTableGet(self), entryId);
    AtPidTableEntryPsnToTdmPidSet(entry, pppHeader);

    /* Configure header length */
    Tha60060011PidTablePsnToTdmHeaderLengthSet(entry, headerLength);
    return cAtOk;
    }

eAtRet Tha60060011PppLinkPidTablePsnToTdmEthTypeSet(AtPppLink self, uint32 entryId, uint32 ethType)
    {
    if (self == NULL)
        return cAtErrorNullPointer;
    return AtPidTableEntryPsnToTdmEthTypeSet(AtPidTableEntryGet(AtPppLinkPidTableGet(self), entryId), ethType);
    }

uint32 Tha60060011PppLinkPidTableTdmToPsnPppHeaderGet(AtPppLink self, uint32 entryId, uint8 *headerLength)
    {
    AtPidTableEntry entry;
    if (self == NULL)
        {
        *headerLength = 0;
        return 0;
        }

    entry = AtPidTableEntryGet(AtPppLinkPidTableGet(self), entryId);
    *headerLength = (uint8)(Tha60060011PidTableTdmToPsnHeaderLengthGet(entry) + 1);

    if (!HeaderLengthIsValid(*headerLength))
        *headerLength = 0;

    return PppHeaderHwToSw(AtPidTableEntryTdmToPsnPidGet(entry), *headerLength);
    }

uint32 Tha60060011PppLinkPidTableTdmToPsnEthTypeGet(AtPppLink self, uint32 entryId)
    {
    if (self == NULL)
        return 0;
    return AtPidTableEntryTdmToPsnEthTypeGet(AtPidTableEntryGet(AtPppLinkPidTableGet(self), entryId));
    }

uint32 Tha60060011PppLinkPidTablePsnToTdmPppHeaderGet(AtPppLink self, uint32 entryId, uint8 *headerLength)
    {
    AtPidTableEntry entry;

    if (self == NULL)
        {
        *headerLength = 0;
        return 0;
        }

    entry = AtPidTableEntryGet(AtPppLinkPidTableGet(self), entryId);
    *headerLength = Tha60060011PidTablePsnToTdmHeaderLengthGet(entry);

    if (!HeaderLengthIsValid(*headerLength))
        *headerLength = 0;

    return PppHeaderHwToSw(AtPidTableEntryPsnToTdmPidGet(entry), *headerLength);
    }

uint32 Tha60060011PppLinkPidTablePsnToTdmEthTypeGet(AtPppLink self, uint32 entryId)
    {
    if (self == NULL)
        return 0;
    return AtPidTableEntryPsnToTdmEthTypeGet(AtPidTableEntryGet(AtPppLinkPidTableGet(self), entryId));
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Encap
 *
 * File        : Tha60060011ModuleEncap.c
 *
 * Created Date: Jul 23, 2013
 *
 * Description : Encap of product 60060011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/
#include "AtHdlcChannel.h"
#include "../../../default/encap/ThaModuleEncapInternal.h"
#include "../ppp/Tha60060011MpegReg.h"
#include "../ppp/Tha60060011MpigReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
mDefineMaskShift(ThaModuleEncap, MlppHdrMode)
mDefineMaskShift(ThaModuleEncap, PidMode)
mDefineMaskShift(ThaModuleEncap, AddrCtrlMode)
mDefineMaskShift(ThaModuleEncap, AdrComp)
mDefineMaskShift(ThaModuleEncap, BlkAloc)
mDefineMaskShift(ThaModuleEncap, FcsErrDropEn)
mDefineMaskShift(ThaModuleEncap, AdrCtlErrDropEn)
mDefineMaskShift(ThaModuleEncap, NCPRule)
mDefineMaskShift(ThaModuleEncap, LCPRule)
mDefineMaskShift(ThaModuleEncap, MlpRuleRule)
mDefineMaskShift(ThaModuleEncap, PppRule)
mDefineMaskShift(ThaModuleEncap, PKtMode)
mDefineMaskShift(ThaModuleEncap, BndID)
mDefineMaskShift(ThaModuleEncap, MPIGDatEmiLbpBlkVl)
mDefineMaskShift(ThaModuleEncap, MPIGDatEmiLbpBlkID)
mDefineMaskShift(ThaModuleEncap, MPEGPppLinkCtrl1BundleID)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60060011ModuleEncap
    {
    tThaModuleEncap super;

    /* Private */
    }tTha60060011ModuleEncap;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleEncapMethods  m_AtModuleEncapOverride;
static tThaModuleEncapMethods m_ThaModuleEncapOverride;

/* Save super implementations */
static const tAtModuleEncapMethods *m_AtModuleEncapMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPppLink PppLinkObjectCreate(AtModuleEncap self, AtHdlcChannel hdlcChannel)
    {
	AtUnused(self);
    return Tha60060011PppLinkNew(hdlcChannel);
    }

static void OverrideAtModuleEncap(AtModuleEncap self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleEncapMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleEncapOverride, m_AtModuleEncapMethods, sizeof(m_AtModuleEncapOverride));
        mMethodOverride(m_AtModuleEncapOverride, PppLinkObjectCreate);
        }

    mMethodsSet(self, &m_AtModuleEncapOverride);
    }

static void OverrideThaModuleEncap(AtModuleEncap self)
    {
    ThaModuleEncap encap = (ThaModuleEncap)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleEncapOverride, mMethodsGet(encap), sizeof(m_ThaModuleEncapOverride));

        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, MlppHdrMode)
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, PidMode)
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, AddrCtrlMode)
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, AdrComp)
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, BlkAloc)
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, FcsErrDropEn)
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, AdrCtlErrDropEn)
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, NCPRule)
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, LCPRule)
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, MlpRuleRule)
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, PppRule)
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, PKtMode)
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, BndID)
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, MPIGDatEmiLbpBlkVl)
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, MPIGDatEmiLbpBlkID)
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, MPEGPppLinkCtrl1BundleID)
        }

    mMethodsSet(encap, &m_ThaModuleEncapOverride);
    }

static void Override(AtModuleEncap self)
    {
    OverrideAtModuleEncap(self);
    OverrideThaModuleEncap(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60060011ModuleEncap);
    }

static AtModuleEncap ObjectInit(AtModuleEncap self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleEncapObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleEncap Tha60060011ModuleEncapNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEncap newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newModule, device);
    }

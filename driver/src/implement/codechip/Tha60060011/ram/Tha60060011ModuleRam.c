/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM module
 *
 * File        : Tha60060011ModuleRam.c
 *
 * Created Date: Dec 18, 2013
 *
 * Description : RAM module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/ram/ThaModuleRamInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60060011ModuleRam
    {
    tThaModuleRam super;
    }tTha60060011ModuleRam;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleRamMethods m_AtModuleRamOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 NumDdrGet(AtModuleRam self)
    {
	AtUnused(self);
    return 2;
    }

static uint8 NumZbtGet(AtModuleRam self)
    {
	AtUnused(self);
    return 0;
    }

static uint32 DdrAddressBusSizeGet(AtModuleRam self, uint8 ddrId)
    {
    AtUnused(self);
    AtUnused(ddrId);
    return 24;
    }

static void OverrideAtModuleRam(AtModuleRam self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleRamOverride, mMethodsGet(self), sizeof(m_AtModuleRamOverride));

        mMethodOverride(m_AtModuleRamOverride, NumDdrGet);
        mMethodOverride(m_AtModuleRamOverride, NumZbtGet);
        mMethodOverride(m_AtModuleRamOverride, DdrAddressBusSizeGet);
        }

    mMethodsSet(self, &m_AtModuleRamOverride);
    }

static void Override(AtModuleRam self)
    {
    OverrideAtModuleRam(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60060011ModuleRam);
    }

static AtModuleRam ObjectInit(AtModuleRam self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleRamObjectInit(self, device) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleRam Tha60060011ModuleRamNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleRam newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newModule, device);
    }


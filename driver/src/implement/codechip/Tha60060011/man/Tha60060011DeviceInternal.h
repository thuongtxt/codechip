/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : Tha60060011DeviceInternal.h
 * 
 * Created Date: Jul 19, 2013
 *
 * Description : Product 60060011
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/
#ifndef _Tha60060011DEVICEINTERNAL_H_
#define _Tha60060011DEVICEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../ThaStmMlppp/man/ThaStmMlpppDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60060011Device
    {
    tThaStmMlpppDevice super;
    }tTha60060011Device;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void Tha60060011DeviceRegistersInit(AtDevice self);

#ifdef __cplusplus
}
#endif
#endif /* _Tha60060011DEVICEINTERNAL_H_ */


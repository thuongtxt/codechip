/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60060011Device.c
 *
 * Created Date: Jul 19, 2013
 *
 * Description : Product 60060011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtHdlcChannel.h"
#include "../sdh/Tha60060011ModuleSdh.h"
#include "Tha60060011DeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceMethods  m_AtDeviceOverride;
static tThaDeviceMethods m_ThaDeviceOverride;

/* Super implementation */
static tAtDeviceMethods *m_AtDeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60060011Device);
    }

static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    eThaPhyModule phyModule = moduleId;

    if (moduleId == cAtModuleSdh)   return (AtModule)Tha60060011ModuleSdhNew(self);
    if (moduleId == cAtModulePpp)   return (AtModule)Tha60060011ModulePppNew(self);
    if (moduleId == cAtModuleEth)   return (AtModule)Tha60060011ModuleEthNew(self);
    if (moduleId == cAtModuleEncap) return (AtModule)Tha60060011ModuleEncapNew(self);
    if (moduleId == cAtModulePdh)   return (AtModule)Tha60060011ModulePdhNew(self);
    if (moduleId == cAtModuleRam)   return (AtModule)Tha60060011ModuleRamNew(self);

    /* Additional physical modules */
    if (phyModule == cThaModuleCla) return (AtModule)ThaModuleClaPppNew(self);
    if (phyModule == cThaModuleOcn) return (AtModule)Tha60060011ModuleOcnNew(self);
    if (phyModule == cThaModuleCos) return (AtModule)Tha60060011ModuleCosNew(self);

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static AtSSKeyChecker SSKeyCheckerCreate(AtDevice self)
    {
    return Tha60060011SSKeyCheckerNew(self);
    }

static ThaIntrController IntrControllerCreate(ThaDevice self, AtIpCore core)
    {
	AtUnused(self);
    return Tha60060011IntrControllerNew(core);
    }

static void SetDefaultDatapath(AtDevice self)
    {
    uint16 i;
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(self, cAtModuleEncap);
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(self, cAtModuleEth);
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(self, cAtModuleSdh);
    uint8 numLines = AtModuleSdhMaxLinesGet(sdhModule);
    static const uint8 cNumTug3InSts3c = 3;
    static const uint8 cNumTug2InTug3  = 7;
    static const uint8 cNumTu1xInTug2  = 3;
    static const uint8 cNumTug2InSts3c = 21;
    static const uint8 cNumTu1xInSts3c = 63;

    Tha60060011ModuleSdhDefaultMap(sdhModule);
    for (i = 0; i < (numLines * cNumTu1xInSts3c); i++)
        {
        uint8 lineId = (uint8)((i / cNumTu1xInSts3c) % numLines);
        uint8 tug3Id = (uint8)((i / cNumTug2InSts3c) % cNumTug3InSts3c);
        uint8 tug2Id = (uint8)((i / cNumTu1xInTug2)  % cNumTug2InTug3);
        uint8 vc1xId = (uint8)( i % cNumTu1xInTug2);
        AtSdhLine line = AtModuleSdhLineGet(sdhModule, lineId);
        AtChannel pdhChannel = AtSdhChannelMapChannelGet((AtSdhChannel)AtSdhLineVc1xGet(line, 0, tug3Id, tug2Id, vc1xId));
        AtHdlcChannel hdlcChannel = AtModuleEncapHdlcPppChannelCreate(encapModule, i);
        AtHdlcLink link = AtHdlcChannelHdlcLinkGet((AtHdlcChannel)hdlcChannel);

        AtEncapChannelPhyBind((AtEncapChannel)hdlcChannel, pdhChannel);
        AtHdlcLinkTxTrafficEnable(link, cAtTrue);
        AtHdlcLinkRxTrafficEnable(link, cAtTrue);
        AtHdlcLinkFlowBind(link, AtModuleEthNopFlowCreate(ethModule, i));
        AtChannelEnable((AtChannel)AtModuleEthFlowGet(ethModule, i), cAtTrue);
        }
    }

static eAtRet Init(AtDevice self)
    {
    eAtRet ret;
    AtModuleEth ethModule;
    ThaDevice device = (ThaDevice)self;
    device->resetIsInProgress = cAtTrue;

    /* As hardware recommend, Ethernet ports should be de-activated before
     * initializing device. Device initialization process will activate these
     * ports */
    ethModule = (AtModuleEth)AtDeviceModuleGet(self, cAtModuleEth);
    ThaModuleEthAllPortMacsActivate(ethModule, cAtFalse);

    ret = AtDeviceAllCoresReset(self);

    /* Need to check if cores work */
    ret |= AtDeviceAllCoresTest(self);
    if (ret != cAtOk)
       return ret;

    /* Initialize device and set default data-path with register accessing */
    Tha60060011DeviceRegistersInit(self);

    /* Initialize device and set default data-path without register accessing */
    AtDeviceWarmRestoreStart(self);
    ret |= AtDeviceAllModulesSetup(self);
    ret |= mMethodsGet(self)->AllModulesInit(self);
    SetDefaultDatapath(self);
    AtDeviceWarmRestoreStop(self);

    /* Check SSKey, give the key engine a moment so that it can run and
     * update status */
    if (mMethodsGet(self)->NeedCheckSSKey(self))
       ret |= mMethodsGet(self)->SSKeyCheck(self);

    device->resetIsInProgress = cAtFalse;

    return ret;
    }

static eBool WarmRestoreReadOperationDisabled(AtDevice self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static void OverrideThaDevice(AtDevice self)
    {
    ThaDevice device = (ThaDevice)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDeviceOverride, mMethodsGet(device), sizeof(m_ThaDeviceOverride));

        mMethodOverride(m_ThaDeviceOverride, IntrControllerCreate);
        }

    mMethodsSet(device, &m_ThaDeviceOverride);
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(m_AtDeviceOverride));

        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, Init);
        mMethodOverride(m_AtDeviceOverride, WarmRestoreReadOperationDisabled);
        mMethodOverride(m_AtDeviceOverride, SSKeyCheckerCreate);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtDevice(self);
    OverrideThaDevice(self);
    }

static AtDevice ObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmMlpppDeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha60060011DeviceNew(AtDriver driver, uint32 productCode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newDevice, driver, productCode);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60060011EthFlowControllerFactory.c
 *
 * Created Date: Apr 4, 2015
 *
 * Description : Flow controller factory
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60060011EthFlowController.h"
#include "../../../default/eth/controller/ThaEthFlowControllerFactoryInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60060011EthFlowControllerFactory
    {
    tThaEthFlowControllerFactoryDefault super;
    }tTha60060011EthFlowControllerFactory;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaEthFlowControllerFactoryMethods m_ThaEthFlowControllerFactoryOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaEthFlowController PppFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule)
    {
    AtUnused(self);
    return Tha60060011StmPppEthFlowControllerNew(ethModule);
    }

static ThaEthFlowController MpFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule)
    {
    AtUnused(self);
    return Tha60060011StmMpEthFlowControllerNew(ethModule);
    }

static ThaEthFlowController OamFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule)
    {
    AtUnused(self);
    return Tha60060011OamFlowControllerNew(ethModule);
    }

static void OverrideThaEthFlowControllerFactory(ThaEthFlowControllerFactory self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthFlowControllerFactoryOverride, mMethodsGet(self), sizeof(m_ThaEthFlowControllerFactoryOverride));

        mMethodOverride(m_ThaEthFlowControllerFactoryOverride, PppFlowControllerCreate);
        mMethodOverride(m_ThaEthFlowControllerFactoryOverride, MpFlowControllerCreate);
        mMethodOverride(m_ThaEthFlowControllerFactoryOverride, OamFlowControllerCreate);
        }

    mMethodsSet(self, &m_ThaEthFlowControllerFactoryOverride);
    }

static void Override(ThaEthFlowControllerFactory self)
    {
    OverrideThaEthFlowControllerFactory(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60060011EthFlowControllerFactory);
    }

static ThaEthFlowControllerFactory ObjectInit(ThaEthFlowControllerFactory self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Constructor */
    if (ThaEthFlowControllerFactoryDefaultObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaEthFlowControllerFactory Tha60060011EthFlowControllerFactory(void)
    {
    static ThaEthFlowControllerFactory m_factory = NULL;
    static tTha60060011EthFlowControllerFactory factory;

    if (m_factory == NULL)
        m_factory = ObjectInit((ThaEthFlowControllerFactory)&factory);
    return m_factory;
    }

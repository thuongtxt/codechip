/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60060011EthFlowController.h
 * 
 * Created Date: Oct 5, 2013
 *
 * Description : ETH Flow controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _Tha60060011ETHFLOWCONTROLLER_H_
#define _Tha60060011ETHFLOWCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/eth/controller/ThaEthFlowControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60060011StmPppEthFlowController
    {
    tThaStmPppEthFlowController super;
    }tTha60060011StmPppEthFlowController;

typedef struct tTha60060011StmMpEthFlowController
    {
    tThaStmMpEthFlowController super;
    }tTha60060011StmMpEthFlowController;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaEthFlowController Tha60060011StmPppEthFlowControllerNew(AtModuleEth module);
ThaEthFlowController Tha60060011StmMpEthFlowControllerNew(AtModuleEth module);
ThaEthFlowController Tha60060011OamFlowControllerNew(AtModuleEth module);

uint32 Tha60060011StmPppEthFlowControllerRemoveVlanFromBuffer(ThaEthFlowController self, uint8* buffer, const tAtEthVlanDesc *egressVlan, uint32 psnLen);
uint32 Tha60060011StmPppEthFlowControllerInsertVlanToBuffer(ThaEthFlowController self, uint8* buffer, const tAtEthVlanDesc *egressVlan, uint32 psnLen);
void Tha60060011StmPppEthFlowControllerExtractVlanFromBuffer(ThaEthFlowController self, AtEthFlow flow, uint8* buffer, tAtEthVlanDesc *egressVlan);
eAtRet Tha60060011StmPppEthFlowControllerNumberEgVlanSet(ThaEthFlowController self, AtEthFlow flow, uint8 vlanTagNum);

#ifdef __cplusplus
}
#endif
#endif /* _Tha60060011ETHFLOWCONTROLLER_H_ */


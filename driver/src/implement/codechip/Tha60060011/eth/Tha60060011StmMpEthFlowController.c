/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Eth Flow
 *
 * File        : Tha60060011EthFlowController.c
 *
 * Created Date: Aug 7, 2013
 *
 * Description : Mp Flow of product 60060011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/eth/controller/ThaEthFlowControllerInternal.h"
#include "../../../default/eth/ThaEthFlowInternal.h"
#include "Tha60060011EthFlowController.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaEthFlowControllerMethods m_ThaEthFlowControllerOverride;
static tThaMpEthFlowControllerMethods m_ThaMpEthFlowControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet EthernetTypeSet(ThaEthFlowController self, AtEthFlow flow)
    {
	AtUnused(flow);
	AtUnused(self);
    return cAtOk;
    }

static uint32 AddressOfCntRegister(ThaMpEthFlowController self, AtEthFlow flow, uint32 regAddress)
    {
    uint32 noChangedField = cBit31_12 & regAddress;
    uint32 changedField   = regAddress & cBit11_0;
    uint32 newAddress     = noChangedField | (changedField << 1);
	AtUnused(self);

    return ThaEthFlowHwFlowIdGet((ThaEthFlow)flow) + newAddress;
    }

static uint32 InsertVlanToBuffer(ThaEthFlowController self, AtEthFlow ethFlow, uint8* buffer, const tAtEthVlanDesc *egressVlan, uint32 psnLen)
    {
    AtUnused(ethFlow);
    return Tha60060011StmPppEthFlowControllerInsertVlanToBuffer(self, buffer, egressVlan, psnLen);
    }

static uint32 RemoveVlanFromBuffer(ThaEthFlowController self, AtEthFlow ethFlow, uint8* buffer, const tAtEthVlanDesc *egressVlan, uint32 psnLen)
    {
    AtUnused(ethFlow);
    return Tha60060011StmPppEthFlowControllerRemoveVlanFromBuffer(self, buffer, egressVlan, psnLen);
    }

static eAtRet NumberEgVlanSet(ThaEthFlowController self, AtEthFlow flow, uint8 vlanTagNum)
    {
    return Tha60060011StmPppEthFlowControllerNumberEgVlanSet(self, flow, vlanTagNum);
    }

static void OverrideThaEthFlowController(ThaEthFlowController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthFlowControllerOverride, mMethodsGet(self), sizeof(m_ThaEthFlowControllerOverride));

        mMethodOverride(m_ThaEthFlowControllerOverride, EthernetTypeSet);
        mMethodOverride(m_ThaEthFlowControllerOverride, InsertVlanToBuffer);
        mMethodOverride(m_ThaEthFlowControllerOverride, RemoveVlanFromBuffer);
        mMethodOverride(m_ThaEthFlowControllerOverride, NumberEgVlanSet);
        }

    mMethodsSet(self, &m_ThaEthFlowControllerOverride);
    }

static void OverrideThaMpEthFlowController(ThaEthFlowController self)
    {
    ThaMpEthFlowController ethFlow = (ThaMpEthFlowController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaMpEthFlowControllerOverride, mMethodsGet(ethFlow), sizeof(m_ThaMpEthFlowControllerOverride));
        mMethodOverride(m_ThaMpEthFlowControllerOverride, AddressOfCntRegister);
        }

    mMethodsSet(ethFlow, &m_ThaMpEthFlowControllerOverride);
    }

static void Override(ThaEthFlowController self)
    {
    OverrideThaEthFlowController(self);
    OverrideThaMpEthFlowController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60060011StmMpEthFlowController);
    }

static ThaEthFlowController ObjectInit(ThaEthFlowController self, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaMpEthFlowControllerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaEthFlowController Tha60060011StmMpEthFlowControllerNew(AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaEthFlowController newFlow = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newFlow, module);
    }

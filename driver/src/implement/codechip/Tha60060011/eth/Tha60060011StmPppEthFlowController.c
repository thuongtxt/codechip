/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60060011StmPppEthFlowController.c
 *
 * Created Date: Oct 5, 2013
 *
 * Description : Flow control of ETH Flow
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtEncapChannel.h"
#include "AtEthFlowControl.h"
#include "Tha60060011EthFlowController.h"
#include "../../../default/eth/ThaEthFlowInternal.h"
#include "../../../default/eth/ThaEthFlowFlowControlInternal.h"
#include "../../../default/ppp/ThaModulePppInternal.h"
#include "../../../default/ppp/ThaMpigReg.h"
#include "../../../default/man/ThaDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaEthFlowControllerMethods m_ThaEthFlowControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet EthernetTypeSet(ThaEthFlowController self, AtEthFlow flow)
    {
	AtUnused(flow);
	AtUnused(self);
    return cAtOk;
    }

static uint8 NumBytesOfTag(ThaEthFlowController self)
    {
	AtUnused(self);
    return 4;
    }

static uint8 NumTags(ThaEthFlowController self, const tAtEthVlanDesc *egressVlan)
    {
	AtUnused(self);
    return egressVlan->numberOfVlans / 2;
    }

static eAtRet NumberEgVlanSet(ThaEthFlowController self, AtEthFlow flow, uint8 vlanTagNum)
    {
    return Tha60060011StmPppEthFlowControllerNumberEgVlanSet(self, flow, vlanTagNum);
    }

static uint32 InsertVlanToBuffer(ThaEthFlowController self, AtEthFlow flow, uint8* buffer, const tAtEthVlanDesc *egressVlan, uint32 psnLen)
    {
    AtUnused(flow);
    return Tha60060011StmPppEthFlowControllerInsertVlanToBuffer(self, buffer, egressVlan, psnLen);
    }

static uint32 RemoveVlanFromBuffer(ThaEthFlowController self, AtEthFlow flow, uint8* buffer, const tAtEthVlanDesc *egressVlan, uint32 psnLen)
    {
    AtUnused(flow);
    return Tha60060011StmPppEthFlowControllerRemoveVlanFromBuffer(self, buffer, egressVlan, psnLen);
    }

static void FlowControlThresholdInit(ThaEthFlowController self, AtEthFlow ethFlow, AtChannel channel)
    {
    AtEncapChannel	encapChannel = (AtEncapChannel)AtHdlcLinkHdlcChannelGet((AtHdlcLink)channel);
    AtChannel       phyChannel   = AtEncapChannelBoundPhyGet(encapChannel);
	AtUnused(self);
    if (phyChannel == NULL)
        return;

    ThaEthFlowFlowControlThresholdApply((ThaEthFlowFlowControl)AtEthFlowFlowControlGet(ethFlow));
    }

static eAtRet AllCountersGet(ThaEthFlowController self, AtEthFlow flow, void *pAllCounters)
    {
    return ThaPppEthFlowControllerAllCountersFromPppLinkGet(self, flow, pAllCounters);
    }

static eAtRet AllCountersClear(ThaEthFlowController self, AtEthFlow flow, void *pAllCounters)
    {
    return ThaPppEthFlowControllerAllCountersFromPppLinkClear(self, flow, pAllCounters);
    }

static void OverrideThaEthFlowController(ThaEthFlowController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();

        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthFlowControllerOverride, mMethodsGet(self), sizeof(m_ThaEthFlowControllerOverride));
        mMethodOverride(m_ThaEthFlowControllerOverride, EthernetTypeSet);
        mMethodOverride(m_ThaEthFlowControllerOverride, InsertVlanToBuffer);
        mMethodOverride(m_ThaEthFlowControllerOverride, RemoveVlanFromBuffer);
        mMethodOverride(m_ThaEthFlowControllerOverride, FlowControlThresholdInit);
        mMethodOverride(m_ThaEthFlowControllerOverride, AllCountersGet);
        mMethodOverride(m_ThaEthFlowControllerOverride, AllCountersClear);
        mMethodOverride(m_ThaEthFlowControllerOverride, NumberEgVlanSet);
        }

    mMethodsSet(self, &m_ThaEthFlowControllerOverride);
    }

static void Override(ThaEthFlowController self)
    {
    OverrideThaEthFlowController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60060011StmPppEthFlowController);
    }

uint32 Tha60060011StmPppEthFlowControllerInsertVlanToBuffer(ThaEthFlowController self, uint8* buffer, const tAtEthVlanDesc *egressVlan, uint32 psnLen)
    {
    const uint8 vlanStartIndex = 2 * cAtMacAddressLen + cNumByteOfEthernetType;
    uint8 bufferIndex = vlanStartIndex;
    uint8 numByteUsedForVlan = (uint8)(NumTags(self, egressVlan) * NumBytesOfTag(self));
    uint8 indexAfterVlan = (uint8)(vlanStartIndex + numByteUsedForVlan);
    uint32 sVlan2Tag = 0;
    uint32 cVlan2Tag = 0;

     /* Build packet tag from cVlan, sVlan
      * sVlan = cpuPktIndicator + portNumber + encapType + slotNumber */
    sVlan2Tag = ((egressVlan->vlans[1].cfi & cBit0) << 15 )      |
                ((egressVlan->vlans[1].priority & cBit3_0) << 11)|
                ((egressVlan->vlans[1].vlanId & cBit7_0) << 3);

    /* cVlan = channelNumber + packet length */
    cVlan2Tag = (((egressVlan->vlans[0].cfi & cBit7_0) << 6)|
                ((egressVlan->vlans[0].priority & cBit1_0) << 14))|
                (egressVlan->vlans[0].vlanId & cBit5_0);

    /* Shift all bytes follow Source MAC to have space for VLANs */
    while (bufferIndex < psnLen)
        {
        buffer[indexAfterVlan] = buffer[bufferIndex];
        indexAfterVlan++;
        bufferIndex++;
        }

    /* Start Buffer Index */
    bufferIndex = vlanStartIndex;

    /* Insert buffer */
    buffer[bufferIndex] = (sVlan2Tag >> 8) & cBit7_0;
    bufferIndex++;
    buffer[bufferIndex] = sVlan2Tag & cBit7_0;
    bufferIndex++;
    buffer[bufferIndex] = (cVlan2Tag >> 8) & cBit7_0;
    bufferIndex++;
    buffer[bufferIndex] = cVlan2Tag & cBit7_0;
    bufferIndex++;

    /* Start with the outer most VLAN */
    return psnLen + numByteUsedForVlan;
    }

uint32 Tha60060011StmPppEthFlowControllerRemoveVlanFromBuffer(ThaEthFlowController self, uint8* buffer, const tAtEthVlanDesc *egressVlan, uint32 psnLen)
    {
    const uint8 vlanStartIndex = 2 * cAtMacAddressLen + cNumByteOfEthernetType;
    uint8 numByteUsedForVlan   = (uint8)(NumTags(self, egressVlan) * NumBytesOfTag(self));
    uint8 indexAfterVlan       = (uint8)(vlanStartIndex + numByteUsedForVlan);
    uint8 indexUsedForVlan     = vlanStartIndex;

    if (psnLen < indexAfterVlan) /* There is no VLAN currently */
        return psnLen;

    /* Shift all bytes follow VLAN to replace bytes used for VLAN */
    while (indexAfterVlan < psnLen)
        {
        buffer[indexUsedForVlan] = buffer[indexAfterVlan];
        indexUsedForVlan++;
        indexAfterVlan++;
        }

    return psnLen - numByteUsedForVlan;
    }

eAtRet Tha60060011StmPppEthFlowControllerNumberEgVlanSet(ThaEthFlowController self, AtEthFlow flow, uint8 vlanTagNum)
    {
    uint32 offsetByFlow = mMethodsGet(self)->MpigFlowOffset(self, flow);
    uint32 regVal, address = mMethodsGet(self)->MpigDatDeQueRegisterAddress(self) + offsetByFlow;
	AtUnused(vlanTagNum);

    regVal = mChannelHwRead(self, address, cThaModuleMpig);
    mFieldIns(&regVal, cThaVlanModeMask, cThaVlanModeShift, 0);
    mChannelHwWrite(self, address, regVal, cThaModuleMpig);

    return cAtOk;
    }

void Tha60060011StmPppEthFlowControllerExtractVlanFromBuffer(ThaEthFlowController self, AtEthFlow flow, uint8* buffer, tAtEthVlanDesc *egressVlan)
    {
    uint8 bufferIndex = 2 * cAtMacAddressLen + cNumByteOfEthernetType;

    AtUnused(self);
    egressVlan->numberOfVlans = ((ThaEthFlow)flow)->flowConfig.egTrafficDesc.numberOfVlans;
    egressVlan->ethPortId = ((ThaEthFlow)flow)->flowConfig.egTrafficDesc.ethPortId;

    egressVlan->vlans[1].cfi = (buffer[bufferIndex] >> 7) & cBit0;
    egressVlan->vlans[1].priority = (buffer[bufferIndex] >> 3) & cBit3_0;
    egressVlan->vlans[1].vlanId = (uint16)(egressVlan->vlans[1].vlanId | (uint16)((buffer[bufferIndex] & cBit2_0) << 5));
    bufferIndex++;
    egressVlan->vlans[1].vlanId = (uint16)(egressVlan->vlans[1].vlanId | (buffer[bufferIndex] >> 3));
    bufferIndex++;

    egressVlan->vlans[0].priority = (buffer[bufferIndex] >> 6) & cBit1_0;
    egressVlan->vlans[0].cfi = (uint8)(egressVlan->vlans[0].cfi | (buffer[bufferIndex] & cBit5_0) << 2);
    bufferIndex++;
    egressVlan->vlans[0].cfi |= buffer[bufferIndex] >> 6;
    egressVlan->vlans[0].vlanId = buffer[bufferIndex] & cBit5_0;
    }

static ThaEthFlowController ObjectInit(ThaEthFlowController self, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPppEthFlowControllerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaEthFlowController Tha60060011StmPppEthFlowControllerNew(AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaEthFlowController newFlow = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newFlow == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newFlow, module);
    }

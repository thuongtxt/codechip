/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : Tha60060011EthFlowHeaderProvider.c
 *
 * Created Date: Dec 6, 2013
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/

#include "../../../default/eth/controller/ThaEthFlowHeaderProviderInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60060011EthFlowHeaderProvider
    {
    tThaEthFlowHeaderProvider super;
    }tTha60060011EthFlowHeaderProvider;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaEthFlowHeaderProviderMethods m_ThaEthFlowHeaderProviderOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 PsnMode(ThaEthFlowHeaderProvider self)
    {
	AtUnused(self);
    return 0xC;
    }

static void Override(ThaEthFlowHeaderProvider self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthFlowHeaderProviderOverride, mMethodsGet(self), sizeof(m_ThaEthFlowHeaderProviderOverride));

        mMethodOverride(m_ThaEthFlowHeaderProviderOverride, PsnMode);
        }

    mMethodsSet(self, &m_ThaEthFlowHeaderProviderOverride);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60060011EthFlowHeaderProvider);
    }

static ThaEthFlowHeaderProvider ObjectInit(ThaEthFlowHeaderProvider self, AtModuleEth ethModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaEthFlowHeaderProviderObjectInit(self, ethModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaEthFlowHeaderProvider Tha60060011EthFlowHeaderProviderNew(AtModuleEth ethModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaEthFlowHeaderProvider newProvider = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newProvider, ethModule);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60060011EthFlowFlowControl.c
 *
 * Created Date: Oct 3, 2013
 *
 * Description : ETH Flow control
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtEthFlow.h"
#include "AtEncapChannel.h"

#include "../../../../generic/common/AtChannelInternal.h"
#include "../../../default/eth/ThaEthFlowFlowControlInternal.h"
#include "../../../default/ppp/ThaModulePpp.h"

#include "Tha60060011EthFlowFlowControlReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60060011EthFlowFlowControl
    {
    tThaEthFlowFlowControl super;
    }tTha60060011EthFlowFlowFlowControl;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtEthFlowControlMethods      m_AtEthFlowControlOverride;
static tThaEthFlowFlowControlMethods m_ThaEthFlowFlowControlOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void ArrayUint8ToUint32(const uint8 *pIn, uint16 inNum, uint32 *pOut)
    {
    uint8  byteIndex;
    uint16 dwIndex;
    uint16 numByteRemain;
    uint8  numLoop;
    uint16 numDword = (uint16)((inNum / 4) + ((inNum % 4) ? 1 : 0));

    for (dwIndex = 0; dwIndex < numDword; dwIndex++)
        {
        numByteRemain = (uint16)(inNum - dwIndex * 4);
        numLoop       = (uint8)((numByteRemain >= 4) ? 4 : numByteRemain);
        for (byteIndex = 0; byteIndex < numLoop; byteIndex++)
            {
            pOut[dwIndex] |= pIn[byteIndex + dwIndex * 4];
            pOut[dwIndex] <<= 8;
            }
        }
    }

static void ArrayUint32ToUint8(const uint32 *pIn, uint16 inNum, uint8 *pOut)
    {
    uint8    bitIndex;
    uint8    byteIndex;
    uint16   dwIndex;
    uint16   numByteRemain;
    uint8    numLoop;
    uint16   numDword = (uint16)((inNum / 4) + ((inNum % 4) ? 1 : 0));

    for (dwIndex = 0; dwIndex < numDword; dwIndex++)
        {
        numByteRemain   = (uint16)(inNum - dwIndex * 4);
        numLoop         = (uint8)((numByteRemain >= 4) ? 4 : numByteRemain);
        for (byteIndex = 0; byteIndex < numLoop; byteIndex++)
            {
            bitIndex = (uint8)(24 - byteIndex * 8);
            pOut[byteIndex + dwIndex * 4] = (pIn[dwIndex] >> bitIndex) & cBit7_0;
            }
        }
    }

static ThaModulePpp PppModule(AtEthFlowControl self)
    {
    AtChannel channel = AtEthFlowControlChannelGet(self);
    return (ThaModulePpp)AtDeviceModuleGet(AtChannelDeviceGet(channel), cAtModulePpp);
    }

static eBool ServiceIsEstablished(AtEthFlowControl self)
	{
	AtEncapChannel encapChannel;
    AtEthFlow ethFlow = (AtEthFlow)AtEthFlowControlChannelGet(self);
    AtHdlcLink link = AtEthFlowHdlcLinkGet(ethFlow);

    if (link == NULL)
        return cAtFalse;

    encapChannel = (AtEncapChannel)AtHdlcLinkHdlcChannelGet(link);
	return (AtEncapChannelBoundPhyGet(encapChannel) == NULL) ? cAtFalse : cAtTrue;
	}

static uint32 NumBlocksInBuffer(AtEthFlowControl self)
	{
    AtEthFlow  ethFlow    = (AtEthFlow)AtEthFlowControlChannelGet(self);
    AtHdlcLink link       = AtEthFlowHdlcLinkGet(ethFlow);
    AtChannel  phyChannel = AtEncapChannelBoundPhyGet((AtEncapChannel)AtHdlcLinkHdlcChannelGet(link));
    uint8      rangeId    = AtChannelPppBlockRange(phyChannel);

    return ThaModulePppNumBlocksForRange(PppModule(self), rangeId);
	}

static uint32 ThresholdNumBlocks(AtEthFlowControl self, uint32 threshold)
	{
    return (NumBlocksInBuffer(self) * threshold) / 100;
	}

static eBool ThresholdIsValid(AtEthFlowControl self, uint32 threshold)
	{
	AtUnused(self);
	return (threshold > 100) ? cAtFalse : cAtTrue;
	}

static eAtModuleEthRet HighThresholdSet(AtEthFlowControl self, uint32 threshold)
    {
    if (!ThresholdIsValid(self, threshold))
    	return cAtErrorOutOfRangParm;

    if (ServiceIsEstablished(self))
        {
        AtEthFlow flow  = (AtEthFlow)AtEthFlowControlChannelGet(self);
        uint32 address  = cThaRegEthFlowControlPacketThreshold + AtChannelIdGet((AtChannel)AtEthFlowHdlcLinkGet(flow));
        uint32 regValue = mChannelHwRead(flow, address, cAtModuleEth);

        mRegFieldSet(regValue, cThaEthFlowControlPacketHighThreshold, (uint8)ThresholdNumBlocks(self, threshold));
        mChannelHwWrite(flow, address, regValue, cAtModuleEth);

        return cAtOk;
        }

    return ThaEthFlowFlowControlHighThresholdSet((ThaEthFlowFlowControl)self, threshold);
    }

static uint32 HighThresholdGet(AtEthFlowControl self)
    {
    if (ServiceIsEstablished(self))
        {
        AtEthFlow ethFlow = (AtEthFlow)AtEthFlowControlChannelGet(self);
        uint32 address = cThaRegEthFlowControlPacketThreshold + AtChannelIdGet((AtChannel)AtEthFlowHdlcLinkGet(ethFlow));
        uint32 regValue = mChannelHwRead(ethFlow, address, cAtModuleEth);
        return mRegField(regValue, cThaEthFlowControlPacketHighThreshold);
        }

    return ThaEthFlowFlowControlHighThresholdGet((ThaEthFlowFlowControl)self);
    }

static eAtModuleEthRet LowThresholdSet(AtEthFlowControl self, uint32 threshold)
    {
    if (!ThresholdIsValid(self, threshold))
    	return cAtErrorOutOfRangParm;

    if (ServiceIsEstablished(self))
        {
        AtEthFlow ethFlow = (AtEthFlow)AtEthFlowControlChannelGet(self);
        uint32 address  = cThaRegEthFlowControlPacketThreshold + AtChannelIdGet((AtChannel)AtEthFlowHdlcLinkGet(ethFlow));
        uint32 regValue = mChannelHwRead(ethFlow, address, cAtModuleEth);
        mRegFieldSet(regValue, cThaEthFlowControlPacketLowThreshold, (uint16)ThresholdNumBlocks(self, threshold));
        mChannelHwWrite(ethFlow, address, regValue, cAtModuleEth);
        return cAtOk;
        }

    return ThaEthFlowFlowControlLowThresholdSet((ThaEthFlowFlowControl)self, threshold);
    }

static uint32 LowThresholdGet(AtEthFlowControl self)
    {
    if (ServiceIsEstablished(self))
        {
        AtEthFlow ethFlow = (AtEthFlow)AtEthFlowControlChannelGet(self);
        uint32 address  = cThaRegEthFlowControlPacketThreshold + AtChannelIdGet((AtChannel)AtEthFlowHdlcLinkGet(ethFlow));
        uint32 regValue = mChannelHwRead(ethFlow, address, cAtModuleEth);
        return mRegField(regValue, cThaEthFlowControlPacketLowThreshold);
        }

    return ThaEthFlowFlowControlLowThresholdGet((ThaEthFlowFlowControl)self);
    }

static uint32 Read(AtEthFlowControl self, uint32 address, eAtModule moduleId)
    {
    return mChannelHwRead(AtEthFlowControlChannelGet(self), address, moduleId);
    }

static void Write(AtEthFlowControl self, uint32 address, uint32 value, eAtModule moduleId)
    {
    mChannelHwWrite(AtEthFlowControlChannelGet(self), address, value, moduleId);
    }

static eAtModuleEthRet DestMacSet(AtEthFlowControl self, uint8 *address)
    {
    uint32 regVal, regAddr;
    uint32 headMacAddr = 0;
    uint32 tailMacAddr = 0;

    ArrayUint8ToUint32(address, 4, &headMacAddr);
    ArrayUint8ToUint32(&address[4], 2, &tailMacAddr);

    regAddr = cThaRegEthFlowCtrlPsnHeader(cThaEthFlowCtrlPsnDestMacHeadDwIndex);
    regVal = Read(self, cThaRegEthFlowCtrlPsnHeader(cThaEthFlowCtrlPsnDestMacHeadDwIndex), cAtModuleEth);
    mFieldIns(&regVal,
              cThaEthFlowCtrlPsnDestMacHeadMask,
              cThaEthFlowCtrlPsnDestMacHeadShift,
              headMacAddr);
    Write(self, regAddr, regVal, cAtModuleEth);

    regAddr = cThaRegEthFlowCtrlPsnHeader(cThaEthFlowCtrlPsnDestMacTailDwIndex);
    regVal  = Read(self, regAddr, cAtModuleEth);
    mFieldIns(&regVal,
              cThaEthFlowCtrlPsnDestMacTailMask,
              cThaEthFlowCtrlPsnDestMacTailShift,
              tailMacAddr >> 16);
    Write(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eAtModuleEthRet DestMacGet(AtEthFlowControl self, uint8 *address)
    {
    uint32 regVal, regAddr;
    uint32 headMacAddr = 0;
    uint32 tailMacAddr = 0;
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, address, 0, cAtMacAddressLen);

    regAddr = cThaRegEthFlowCtrlPsnHeader(cThaEthFlowCtrlPsnDestMacTailDwIndex);
    regVal  = Read(self, regAddr, cAtModuleEth);
    mFieldGet(regVal,
              cThaEthFlowCtrlPsnDestMacTailMask,
              cThaEthFlowCtrlPsnDestMacTailShift,
              uint32,
              &tailMacAddr);

    regAddr = cThaRegEthFlowCtrlPsnHeader(cThaEthFlowCtrlPsnDestMacHeadDwIndex);
    regVal = Read(self, cThaRegEthFlowCtrlPsnHeader(cThaEthFlowCtrlPsnDestMacHeadDwIndex), cAtModuleEth);
    mFieldGet(regVal,
              cThaEthFlowCtrlPsnDestMacHeadMask,
              cThaEthFlowCtrlPsnDestMacHeadShift,
              uint32,
              &headMacAddr);

    /* Build SW MAC address */
    tailMacAddr = tailMacAddr << 16;
    ArrayUint32ToUint8(&headMacAddr, 4, address);
    ArrayUint32ToUint8(&tailMacAddr, 2, &address[4]);

    return cAtOk;
    }

static eAtModuleEthRet SourceMacSet(AtEthFlowControl self, uint8 *address)
    {
    uint32 regVal, regAddr;
    uint32 headMacAddr = 0;
    uint32 tailMacAddr = 0;
    ArrayUint8ToUint32(address, 2, &headMacAddr);
    ArrayUint8ToUint32(&address[2], 4, &tailMacAddr);

    regAddr = cThaRegEthFlowCtrlPsnHeader(cThaEthFlowCtrlPsnSrcMacHeadDwIndex);
    regVal = Read(self, cThaRegEthFlowCtrlPsnHeader(cThaEthFlowCtrlPsnSrcMacHeadDwIndex), cAtModuleEth);
    mFieldIns(&regVal,
              cThaEthFlowCtrlPsnSrcMacHeadMask,
              cThaEthFlowCtrlPsnSrcMacHeadShift,
              headMacAddr >> 16);
    Write(self, regAddr, regVal, cAtModuleEth);

    regAddr = cThaRegEthFlowCtrlPsnHeader(cThaEthFlowCtrlPsnSrcMacTailDwIndex);
    regVal  = Read(self, regAddr, cAtModuleEth);
    mFieldIns(&regVal,
              cThaEthFlowCtrlPsnSrcMacTailMask,
              cThaEthFlowCtrlPsnSrcMacTailShift,
              tailMacAddr);
    Write(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eAtModuleEthRet SourceMacGet(AtEthFlowControl self, uint8 *address)
    {
    uint32 regVal, regAddr;
    uint32 headMacAddr = 0;
    uint32 tailMacAddr = 0;
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, address, 0, cAtMacAddressLen);

    regAddr = cThaRegEthFlowCtrlPsnHeader(cThaEthFlowCtrlPsnSrcMacHeadDwIndex);
    regVal = Read(self, cThaRegEthFlowCtrlPsnHeader(cThaEthFlowCtrlPsnSrcMacHeadDwIndex), cAtModuleEth);
    mFieldGet(regVal,
              cThaEthFlowCtrlPsnSrcMacHeadMask,
              cThaEthFlowCtrlPsnSrcMacHeadShift,
              uint32,
              &headMacAddr);

    regAddr = cThaRegEthFlowCtrlPsnHeader(cThaEthFlowCtrlPsnSrcMacTailDwIndex);
    regVal  = Read(self, regAddr, cAtModuleEth);
    mFieldGet(regVal,
              cThaEthFlowCtrlPsnSrcMacTailMask,
              cThaEthFlowCtrlPsnSrcMacTailShift,
              uint32,
              &tailMacAddr);

    /* Build SW MAC address */
    headMacAddr = headMacAddr << 16;
    ArrayUint32ToUint8(&headMacAddr, 2, address);
    ArrayUint32ToUint8(&tailMacAddr, 4, &address[2]);

    return cAtOk;
    }

static eAtModuleEthRet EthTypeSet(AtEthFlowControl self, uint32 ethType)
    {
    uint32 regVal, regAddr;

    regAddr = cThaRegEthFlowCtrlPsnHeader(cThaEthFlowCtrlEthTypeDwIndex);
    regVal  = Read(self, regAddr, cAtModuleEth);
    mFieldIns(&regVal,
              cThaEthFlowCtrlEthTypeMask,
              cThaEthFlowCtrlEthTypeShift,
              ethType);
    Write(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static uint32 EthTypeGet(AtEthFlowControl self)
    {
    uint32 regVal, regAddr, ethType;

    regAddr = cThaRegEthFlowCtrlPsnHeader(cThaEthFlowCtrlEthTypeDwIndex);
    regVal  = Read(self, regAddr, cAtModuleEth);
    mFieldGet(regVal,
              cThaEthFlowCtrlEthTypeMask,
              cThaEthFlowCtrlEthTypeShift,
              uint32,
              &ethType);

    return ethType;
    }

static eAtModuleEthRet PauseFramePeriodSet(AtEthFlowControl self, uint32 timeInterval)
    {
    uint32 regValue = Read(self, cThaRegEthFlowCtrlPktInterval, cAtModuleEth);

    mRegFieldSet(regValue, cThaRegEthTimePktInterval, timeInterval);
    Write(self, cThaRegEthFlowCtrlPktInterval, regValue, cAtModuleEth);

    return cAtOk;
    }

static uint32 PauseFramePeriodGet(AtEthFlowControl self)
    {
    uint32 regValue = Read(self, cThaRegEthFlowCtrlPktInterval, cAtModuleEth);
    return mRegField(regValue, cThaRegEthTimePktInterval);
    }

static void ThresholdApply(ThaEthFlowFlowControl self)
	{
    static const uint8 cDefaultHighThreshold = 90;
    static const uint8 cDefaultLowThreshold = 5;
    AtEthFlowControl flowControl = (AtEthFlowControl)self;
    uint32 highThreshold = AtEthFlowControlHighThresholdGet(flowControl);
    uint32 lowThreshold  = AtEthFlowControlLowThresholdGet(flowControl);
    AtEthFlowControlHighThresholdSet(flowControl, highThreshold ? highThreshold : cDefaultHighThreshold);
    AtEthFlowControlLowThresholdSet(flowControl , lowThreshold  ? lowThreshold  : cDefaultLowThreshold);
	}

static void OverrideAtEthFlowControl(AtEthFlowControl self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal,
                                  &m_AtEthFlowControlOverride,
                                  mMethodsGet(self),
                                  sizeof(m_AtEthFlowControlOverride));

        mMethodOverride(m_AtEthFlowControlOverride, HighThresholdSet);
        mMethodOverride(m_AtEthFlowControlOverride, HighThresholdGet);
        mMethodOverride(m_AtEthFlowControlOverride, LowThresholdSet);
        mMethodOverride(m_AtEthFlowControlOverride, LowThresholdGet);
        mMethodOverride(m_AtEthFlowControlOverride, DestMacSet);
        mMethodOverride(m_AtEthFlowControlOverride, DestMacGet);
        mMethodOverride(m_AtEthFlowControlOverride, SourceMacSet);
        mMethodOverride(m_AtEthFlowControlOverride, SourceMacGet);
        mMethodOverride(m_AtEthFlowControlOverride, EthTypeSet);
        mMethodOverride(m_AtEthFlowControlOverride, EthTypeGet);
        mMethodOverride(m_AtEthFlowControlOverride, PauseFramePeriodSet);
        mMethodOverride(m_AtEthFlowControlOverride, PauseFramePeriodGet);
        }

    mMethodsSet(self, &m_AtEthFlowControlOverride);
    }

static void OverrideThaEthFlowFlowControl(AtEthFlowControl self)
    {
	ThaEthFlowFlowControl flowControl = (ThaEthFlowFlowControl)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal,
                                  &m_ThaEthFlowFlowControlOverride,
                                  mMethodsGet(flowControl),
                                  sizeof(m_ThaEthFlowFlowControlOverride));

        mMethodOverride(m_ThaEthFlowFlowControlOverride, ThresholdApply);
        }

    mMethodsSet(flowControl, &m_ThaEthFlowFlowControlOverride);
    }

static void Override(AtEthFlowControl self)
    {
    OverrideAtEthFlowControl(self);
    OverrideThaEthFlowFlowControl(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60060011EthFlowFlowFlowControl);
    }

static AtEthFlowControl ObjectInit(AtEthFlowControl self,
                                   AtModule module,
                                   AtEthFlow flow)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaEthFlowFlowControlObjectInit(self, module, flow) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEthFlowControl Tha60060011EthFlowFlowControlNew(AtModule module, AtEthFlow flow)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthFlowControl newFlowCtrl = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newFlowCtrl == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newFlowCtrl, module, flow);
    }

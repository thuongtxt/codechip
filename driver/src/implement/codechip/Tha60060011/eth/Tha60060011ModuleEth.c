/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Module Eth
 *
 * File        : Tha60060011ModuleEth.c
 *
 * Created Date: Jul 20, 2013
 *
 * Description : Module Eth of product 60060011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/eth/ThaModuleEthInternal.h"
#include "../../../default/eth/ThaEthFlowFlowControl.h"
#include "Tha60060011EthFlowController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60060011ModuleEth
    {
    tThaModuleEthPpp super;
    }tTha60060011ModuleEth;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleEthMethods m_AtModuleEthOverride;
static tThaModuleEthMethods m_ThaModuleEthOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static uint16 MaxFlowsGet(AtModuleEth self)
    {
	AtUnused(self);
    return 256;
    }

static AtEthFlowControl FlowControlCreate(AtModuleEth module, AtEthFlow flow)
    {
    return Tha60060011EthFlowFlowControlNew((AtModule)module, flow);
    }

static ThaEthFlowHeaderProvider FlowHeaderProviderCreate(ThaModuleEth self)
    {
    return Tha60060011EthFlowHeaderProviderNew((AtModuleEth)self);
    }

static uint32 VlanIdFromVlanDesc(ThaModuleEth self, AtEthFlow flow, const tAtEthVlanDesc *desc)
    {
    tAtEthVlanTag cVlan = desc->vlans[mMethodsGet(self)->VlanToLookUp(self)];
	AtUnused(flow);
    return (cVlan.cfi & cBit7_0) | ((cVlan.priority & cBit1_0) << 8);
    }

static eAtEthPortInterface PortDefaultInterface(AtModuleEth self, AtEthPort port)
    {
    AtUnused(self);
    AtUnused(port);
    return cAtEthPortInterfaceRgmii;
    }

static ThaEthFlowControllerFactory FlowControllerFactory(ThaModuleEth self)
    {
    AtUnused(self);
    return Tha60060011EthFlowControllerFactory();
    }

static void OverrideAtModuleEth(AtModuleEth self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleEthOverride, mMethodsGet(self), sizeof(m_AtModuleEthOverride));
        mMethodOverride(m_AtModuleEthOverride, MaxFlowsGet);
        mMethodOverride(m_AtModuleEthOverride, FlowControlCreate);
        mMethodOverride(m_AtModuleEthOverride, PortDefaultInterface);
        }

    mMethodsSet(self, &m_AtModuleEthOverride);
    }

static void OverrideThaModuleEth(AtModuleEth self)
    {
    ThaModuleEth thaModule = (ThaModuleEth)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleEthOverride, mMethodsGet(thaModule), sizeof(m_ThaModuleEthOverride));

        mMethodOverride(m_ThaModuleEthOverride, VlanIdFromVlanDesc);
        mMethodOverride(m_ThaModuleEthOverride, FlowHeaderProviderCreate);
        mMethodOverride(m_ThaModuleEthOverride, FlowControllerFactory);
        }

    mMethodsSet(thaModule, &m_ThaModuleEthOverride);
    }

static void Override(AtModuleEth self)
    {
    OverrideAtModuleEth(self);
    OverrideThaModuleEth(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60060011ModuleEth);
    }

static AtModuleEth ObjectInit(AtModuleEth self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleEthPppObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleEth Tha60060011ModuleEthNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEth newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newModule, device);
    }

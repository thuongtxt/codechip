/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : Tha60060011OamFlowController.c
 *
 * Created Date: Dec 17, 2013
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../../default/ppp/ThaPppLinkInternal.h"
#include "Tha60060011EthFlowController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60060011OamFlowController
    {
    tThaOamFlowController super;
    }tTha60060011OamFlowController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaEthFlowControllerMethods m_ThaEthFlowControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet EthernetTypeSet(ThaEthFlowController self, AtEthFlow flow)
    {
	AtUnused(flow);
	AtUnused(self);
    return cAtOk;
    }

static uint32 InsertVlanToBuffer(ThaEthFlowController self, AtEthFlow ethFlow, uint8* buffer, const tAtEthVlanDesc *egressVlan, uint32 psnLen)
    {
    AtUnused(ethFlow);
    return Tha60060011StmPppEthFlowControllerInsertVlanToBuffer(self, buffer, egressVlan, psnLen);
    }

static uint32 RemoveVlanFromBuffer(ThaEthFlowController self, AtEthFlow ethFlow, uint8* buffer, const tAtEthVlanDesc *egressVlan, uint32 psnLen)
    {
    AtUnused(ethFlow);
    return Tha60060011StmPppEthFlowControllerRemoveVlanFromBuffer(self, buffer, egressVlan, psnLen);
    }

static eAtRet NumberEgVlanSet(ThaEthFlowController self, AtEthFlow flow, uint8 vlanTagNum)
    {
    return Tha60060011StmPppEthFlowControllerNumberEgVlanSet(self, flow, vlanTagNum);
    }

static void OverrideThaEthFlowController(ThaEthFlowController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthFlowControllerOverride, mMethodsGet(self), sizeof(m_ThaEthFlowControllerOverride));

        mMethodOverride(m_ThaEthFlowControllerOverride, EthernetTypeSet);
        mMethodOverride(m_ThaEthFlowControllerOverride, InsertVlanToBuffer);
        mMethodOverride(m_ThaEthFlowControllerOverride, RemoveVlanFromBuffer);
        mMethodOverride(m_ThaEthFlowControllerOverride, NumberEgVlanSet);
        }

    mMethodsSet(self, &m_ThaEthFlowControllerOverride);
    }

static void Override(ThaEthFlowController self)
    {
    OverrideThaEthFlowController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60060011OamFlowController);
    }

static ThaEthFlowController ObjectInit(ThaEthFlowController self, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaOamFlowControllerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaEthFlowController Tha60060011OamFlowControllerNew(AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaEthFlowController newFlow = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newFlow == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newFlow, module);
    }

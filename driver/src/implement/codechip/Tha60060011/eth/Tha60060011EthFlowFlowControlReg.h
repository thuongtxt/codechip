/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : Tha60060011EthFlowFlowControlReg.h
 * 
 * Created Date: Oct 14, 2013
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _Tha60060011ETHFLOWFLOWCONTROLREG_H_
#define _Tha60060011ETHFLOWFLOWCONTROLREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/*------------------------------------------------------------------------------
Reg Name: Thalassa flow control packet interval
Address: 0x3C0000
Description: The regiseter is used to configure gap time between 2 flow controll packets. Unit is ms
------------------------------------------------------------------------------*/
#define cThaRegEthFlowCtrlPktInterval        0x3C0000

#define cThaRegEthTimePktIntervalMask        cBit31_0
#define cThaRegEthTimePktIntervalShift       0

/*------------------------------------------------------------------------------
Reg Name: Thalassa flow control PSN Header
Address: 0x3C0800-03C0803
Description: The register provides PSN header for flow controll packet
------------------------------------------------------------------------------*/
#define cThaRegEthFlowCtrlPsnHeader(dwIndex)       (0x3C0800 + dwIndex)

#define cThaEthFlowCtrlEthTypeMask                  cBit15_0
#define cThaEthFlowCtrlEthTypeShift                 0
#define cThaEthFlowCtrlEthTypeDwIndex               3

#define cThaEthFlowCtrlPsnSrcMacHeadMask            cBit15_0
#define cThaEthFlowCtrlPsnSrcMacHeadShift           0
#define cThaEthFlowCtrlPsnSrcMacHeadDwIndex         1
#define cThaEthFlowCtrlPsnSrcMacTailMask            cBit31_0
#define cThaEthFlowCtrlPsnSrcMacTailShift           0
#define cThaEthFlowCtrlPsnSrcMacTailDwIndex         2

#define cThaEthFlowCtrlPsnDestMacHeadMask           cBit31_0
#define cThaEthFlowCtrlPsnDestMacHeadShift          0
#define cThaEthFlowCtrlPsnDestMacHeadDwIndex        0
#define cThaEthFlowCtrlPsnDestMacTailMask           cBit31_16
#define cThaEthFlowCtrlPsnDestMacTailShift          16
#define cThaEthFlowCtrlPsnDestMacTailDwIndex        1

/*------------------------------------------------------------------------------
Reg Name: Thalassa flow control buffer threshold configuration
Address: 0x3C1000-0x3C17FF
The address format for these registers is 0x248000 + LID + QID
Where: LID + QID: 0x0 - 0x7FF
Description: This register configures  full/empty threshold of buffer.
------------------------------------------------------------------------------*/
#define cThaRegEthFlowControlPacketThreshold             0x3C1000

#define cThaEthFlowControlPacketHighThresholdMask        cBit31_16
#define cThaEthFlowControlPacketHighThresholdShift       16

#define cThaEthFlowControlPacketLowThresholdMask         cBit15_0
#define cThaEthFlowControlPacketLowThresholdShift        0

#ifdef __cplusplus
}
#endif
#endif /* _Tha60060011ETHFLOWFLOWCONTROLREG_H_ */


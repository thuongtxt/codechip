/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha60060011SdhLineSerdesController.c
 *
 * Created Date: Dec 17, 2013
 *
 * Description : SDH Line SERDES controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/physical/ThaSdhLineSerdesControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cSerdesCdrReg 0xf00042

#define cSerdesTxDisableMask(serdesId)       (cBit0 << (serdesId))
#define cSerdesTxDisableShift(serdesId)      (serdesId)

#define cSerdesRxEnableMask(serdesId)        ((serdesId == 0) ? cBit3 : cBit6)
#define cSerdesRxEnableShift(serdesId)       ((serdesId == 0) ? 3 : 6)

#define cSerdesStm4RateEnableMask(serdesId)  ((serdesId == 0) ? cBit2 : cBit5)
#define cSerdesStm4RateEnableShift(serdesId) ((serdesId == 0) ? 2 : 5)

#define cSerdesLockToRefMask(serdesId)       ((serdesId == 0) ? cBit4 : cBit7)
#define cSerdesLockToRefShift(serdesId)      ((serdesId == 0) ? 4 : 7)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60060011SdhLineSerdesController
    {
    tThaSdhLineSerdesController super;
    }tTha60060011SdhLineSerdesController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSerdesControllerMethods         m_AtSerdesControllerOverride;
static tThaSerdesControllerMethods        m_ThaSerdesControllerOverride;
static tThaSdhLineSerdesControllerMethods m_ThaSdhLineSerdesControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet TimingModeSet(AtSerdesController self, eAtSerdesTimingMode timingMode)
    {
    uint32 regVal  = AtSerdesControllerRead(self, cSerdesCdrReg, cAtModuleSdh);
    uint32 serdesId = AtSerdesControllerIdGet(self);

    mFieldIns(&regVal,
              cSerdesLockToRefMask(serdesId),
              cSerdesLockToRefShift(serdesId),
              (timingMode == cAtSerdesTimingModeLockToRef) ? 1 : 0);
    AtSerdesControllerWrite(self, cSerdesCdrReg, regVal, cAtModuleSdh);

    return cAtOk;
    }

static eAtSerdesTimingMode TimingModeGet(AtSerdesController self)
    {
    uint32 regVal  = AtSerdesControllerRead(self, cSerdesCdrReg, cAtModuleSdh);
    uint32 serdesId = AtSerdesControllerIdGet(self);
    return (regVal & cSerdesLockToRefMask(serdesId)) ? cAtSerdesTimingModeLockToRef : cAtSerdesTimingModeLockToData;
    }

static eAtRet Enable(AtSerdesController self, eBool enable)
    {
    uint32 regVal  = AtSerdesControllerRead(self, cSerdesCdrReg, cAtModuleSdh);
    uint32 serdesId = AtSerdesControllerIdGet(self);

    mFieldIns(&regVal, cSerdesTxDisableMask(serdesId), cSerdesTxDisableShift(serdesId), enable ? 0 : 1);
    mFieldIns(&regVal, cSerdesRxEnableMask(serdesId), cSerdesRxEnableShift(serdesId),   enable ? 1 : 0);

    AtSerdesControllerWrite(self, cSerdesCdrReg, regVal, cAtModuleSdh);

    return cAtOk;
    }

static eBool IsEnabled(AtSerdesController self)
    {
    uint32 regVal  = AtSerdesControllerRead(self, cSerdesCdrReg, cAtModuleSdh);
    uint32 serdesId = AtSerdesControllerIdGet(self);
    return (regVal & cSerdesRxEnableMask(serdesId)) ? cAtTrue : cAtFalse;
    }

static eBool CanEnable(AtSerdesController self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtTrue;
    }

static eBool RateIsSupported(eAtSdhLineRate rate)
    {
    if ((rate == cAtSdhLineRateStm1) || (rate == cAtSdhLineRateStm4))
        return cAtTrue;
    return cAtFalse;
    }

static eAtRet RateSet(ThaSdhLineSerdesController self, eAtSdhLineRate rate)
    {
    uint32 regVal;
    uint32 serdesId;

    if (!RateIsSupported(rate))
        return cAtErrorModeNotSupport;

    regVal   = AtSerdesControllerRead((AtSerdesController)self, cSerdesCdrReg, cAtModuleSdh);
    serdesId = (uint8)AtSerdesControllerIdGet((AtSerdesController)self);
    mFieldIns(&regVal,
              cSerdesStm4RateEnableMask(serdesId),
              cSerdesStm4RateEnableShift(serdesId),
              (rate == cAtSdhLineRateStm4) ? 1 : 0);
    AtSerdesControllerWrite((AtSerdesController)self, cSerdesCdrReg, regVal, cAtModuleSdh);

    return cAtOk;
    }

static eAtSdhLineRate RateGet(ThaSdhLineSerdesController self)
    {
    uint32 regVal  = AtSerdesControllerRead((AtSerdesController)self, cSerdesCdrReg, cAtModuleSdh);
    uint32 serdesId = AtSerdesControllerIdGet((AtSerdesController)self);

    return (regVal & cSerdesStm4RateEnableMask(serdesId)) ? cAtSdhLineRateStm4 : cAtSdhLineRateStm1;
    }

static eAtRet HwPhysicalParamSet(ThaSerdesController self, uint32 hwParam, uint32 value)
    {
	AtUnused(value);
	AtUnused(hwParam);
	AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static uint32 HwPhysicalParamGet(ThaSerdesController self, uint32 hwParam)
    {
	AtUnused(hwParam);
	AtUnused(self);
    return 0x0;
    }

static eBool CanControlTimingMode(AtSerdesController self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static void OverrideThaSerdesController(AtSerdesController self)
    {
    ThaSerdesController controller = (ThaSerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSerdesControllerOverride, mMethodsGet(controller), sizeof(m_ThaSerdesControllerOverride));

        mMethodOverride(m_ThaSerdesControllerOverride, HwPhysicalParamSet);
        mMethodOverride(m_ThaSerdesControllerOverride, HwPhysicalParamGet);
        }

    mMethodsSet(controller, &m_ThaSerdesControllerOverride);
    }

static void OverrideThaSdhLineSerdesController(AtSerdesController self)
    {
    ThaSdhLineSerdesController sdhLineSerdes = (ThaSdhLineSerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhLineSerdesControllerOverride, mMethodsGet(sdhLineSerdes), sizeof(m_ThaSdhLineSerdesControllerOverride));

        mMethodOverride(m_ThaSdhLineSerdesControllerOverride, RateSet);
        mMethodOverride(m_ThaSdhLineSerdesControllerOverride, RateGet);
        }

    mMethodsSet(sdhLineSerdes, &m_ThaSdhLineSerdesControllerOverride);
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, mMethodsGet(self), sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, CanControlTimingMode);
        mMethodOverride(m_AtSerdesControllerOverride, TimingModeSet);
        mMethodOverride(m_AtSerdesControllerOverride, TimingModeGet);
        mMethodOverride(m_AtSerdesControllerOverride, Enable);
        mMethodOverride(m_AtSerdesControllerOverride, IsEnabled);
        mMethodOverride(m_AtSerdesControllerOverride, CanEnable);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtSerdesController(self);
    OverrideThaSerdesController(self);
    OverrideThaSdhLineSerdesController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60060011SdhLineSerdesController);
    }

static AtSerdesController ObjectInit(AtSerdesController self, AtSdhLine sdhLine, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaSdhLineSerdesControllerObjectInit(self, sdhLine, serdesId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController Tha60060011SdhLineSerdesControllerNew(AtSdhLine sdhLine, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newController, sdhLine, serdesId);
    }


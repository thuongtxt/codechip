/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha60060011ModuleSdh.h
 * 
 * Created Date: Mar 1, 2015
 *
 * Description : SDH module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60060011MODULESDH_H_
#define _THA60060011MODULESDH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void Tha60060011ModuleSdhDefaultMap(AtModuleSdh self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60060011MODULESDH_H_ */


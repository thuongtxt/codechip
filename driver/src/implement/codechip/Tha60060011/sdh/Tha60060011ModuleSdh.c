/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60060011ModuleSdh.c
 *
 * Created Date: Jul 19, 2013
 *
 * Description : SDH module of product 60060011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../ThaStmMlppp/sdh/ThaStmMlpppModuleSdhInternal.h"
#include "../../../default/physical/ThaSerdesController.h"
#include "AtSdhAug.h"
#include "AtSdhVc.h"
#include "AtSdhTug.h"
#include "Tha60060011ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60060011ModuleSdh
    {
    tThaStmMlpppModuleSdh super;
    }tTha60060011ModuleSdh;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleSdhMethods  m_AtModuleSdhOverride;
static tThaModuleSdhMethods m_ThaModuleSdhOverride;

/* Save super implementations */
static const tAtModuleSdhMethods  *m_AtModuleSdhMethods  = NULL;
static const tThaModuleSdhMethods *m_ThaModuleSdhMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtSdhChannel ChannelCreateWithVersion(ThaModuleSdh self, uint8 lineId, AtSdhChannel parent, uint8 channelType, uint8 channelId, uint8 version)
    {
    if (channelType == cAtSdhChannelTypeLine)
        return (AtSdhChannel)Tha60060011SdhLineNew(channelId, (AtModuleSdh)self, version);

    /* Ask super for other channel types */
    return m_ThaModuleSdhMethods->ChannelCreateWithVersion(self, lineId, parent, channelType, channelId, version);
    }

static AtSerdesController SerdesControllerCreate(ThaModuleSdh self, AtSdhLine line, uint32 serdesId)
    {
	AtUnused(self);
    return Tha60060011SdhLineSerdesControllerNew(line, serdesId);
    }

static uint8 MaxLinesGet(AtModuleSdh self)
    {
	AtUnused(self);
    return 2;
    }

static eBool HasLineInterrupt(ThaModuleSdh self, uint32 glbIntr, AtIpCore ipCore)
    {
	AtUnused(ipCore);
	AtUnused(self);
    return (glbIntr & cBit2) ? cAtFalse : cAtTrue;
    }

static eAtSerdesTimingMode DefaultSerdesTimingModeForLine(ThaModuleSdh self, AtSdhLine line)
    {
	AtUnused(line);
	AtUnused(self);
    return cAtSerdesTimingModeLockToData;
    }

static eBool AlwaysDisableRdiBackwardWhenTimNotDownstreamAis(ThaModuleSdh self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60060011ModuleSdh);
    }

static void OverrideThaModuleSdh(AtModuleSdh self)
    {
    ThaModuleSdh sdhModule = (ThaModuleSdh)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleSdhMethods = mMethodsGet(sdhModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleSdhOverride, m_ThaModuleSdhMethods, sizeof(m_ThaModuleSdhOverride));
        mMethodOverride(m_ThaModuleSdhOverride, SerdesControllerCreate);
        mMethodOverride(m_ThaModuleSdhOverride, HasLineInterrupt);
        mMethodOverride(m_ThaModuleSdhOverride, DefaultSerdesTimingModeForLine);
        mMethodOverride(m_ThaModuleSdhOverride, ChannelCreateWithVersion);
        mMethodOverride(m_ThaModuleSdhOverride, AlwaysDisableRdiBackwardWhenTimNotDownstreamAis);
        }

    mMethodsSet(sdhModule, &m_ThaModuleSdhOverride);
    }

static void OverrideAtModuleSdh(AtModuleSdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleSdhMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleSdhOverride, m_AtModuleSdhMethods, sizeof(m_AtModuleSdhOverride));
        mMethodOverride(m_AtModuleSdhOverride, MaxLinesGet);
        }

    mMethodsSet(self, &m_AtModuleSdhOverride);
    }

static void Override(AtModuleSdh self)
    {
    OverrideAtModuleSdh(self);
    OverrideThaModuleSdh(self);
    }

static AtModuleSdh ObjectInit(AtModuleSdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmMlpppModuleSdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSdh Tha60060011ModuleSdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newModule, device);
    }

void Tha60060011ModuleSdhDefaultMap(AtModuleSdh self)
    {
    uint8 lineId;

    for (lineId = 0; lineId < AtModuleSdhMaxLinesGet(self); lineId++)
        {
        static const uint8 cMaxNumTug3InSts3c = 3;
        static const uint8 cMaxNumTug2InTug3  = 7;
        uint8 tug3Id, tug2Id;
        AtSdhLine line = AtModuleSdhLineGet(self, lineId);

        AtSdhLineRateSet(line, cAtSdhLineRateStm1);
        AtSdhChannelModeSet((AtSdhChannel)line, cAtSdhChannelModeSdh);

        AtSdhChannelMapTypeSet((AtSdhChannel)AtSdhLineAug1Get(line, 0), cAtSdhAugMapTypeAug1MapVc4);
        AtSdhChannelMapTypeSet((AtSdhChannel)AtSdhLineVc4Get(line, 0), cAtSdhVcMapTypeVc4Map3xTug3s);

        for (tug3Id = 0; tug3Id < cMaxNumTug3InSts3c; tug3Id++)
            {
            AtSdhChannelMapTypeSet((AtSdhChannel)AtSdhLineTug3Get(line, 0, tug3Id), cAtSdhTugMapTypeTug3Map7xTug2s);
            for (tug2Id = 0; tug2Id < cMaxNumTug2InTug3; tug2Id++)
                AtSdhChannelMapTypeSet((AtSdhChannel)AtSdhLineTug2Get(line, 0, tug3Id, tug2Id), cAtSdhTugMapTypeTug2Map3xTu12s);
            }
        }
    }

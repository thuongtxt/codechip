/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60060011SdhLine.c
 *
 * Created Date: Jul 30, 2013
 *
 * Description : SDH Line of product 60060011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../ThaStmMlppp/sdh/ThaStmMlpppModuleSdhInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60060011SdhLine
    {
    tThaStmMlpppSdhLine super;

    /* Private data */
    }tTha60060011SdhLine;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods  m_AtChannelOverride;
static tAtSdhLineMethods  m_AtSdhLineOverride;
static tThaSdhLineMethods m_ThaSdhLineOverride;

/* Super implementation */
static const tAtChannelMethods  *m_AtChannelMethods  = NULL;
static const tAtSdhLineMethods  *m_AtSdhLineMethods  = NULL;
static const tThaSdhLineMethods *m_ThaSdhLineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60060011SdhLine);
    }

static eBool RateIsSupported(AtSdhLine self, eAtSdhLineRate rate)
    {
	AtUnused(self);
    /* All ports support STM-1 */
    if (rate == cAtSdhLineRateStm1)
        return cAtTrue;

    /* Not support for other cases */
    return cAtFalse;
    }

static eAtRet SerdesLoopInEnable(ThaSdhLine self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    return cAtOk;
    }

static eBool SerdesLoopInIsEnabled(ThaSdhLine self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static eAtRet SerdesLoopoutEnable(ThaSdhLine self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    return cAtOk;
    }

static eBool SerdesLoopoutIsEnabled(ThaSdhLine self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static uint32 RxForcibleAlarmsGet(AtChannel self)
    {
    uint32 forceAbleAlarms = m_AtChannelMethods->RxForcibleAlarmsGet(self);
    return forceAbleAlarms & (uint32)(~cAtSdhLineAlarmLos);
    }

static eAtRet Enable(AtChannel self, eBool enable)
    {
    eAtRet ret = m_AtChannelMethods->Enable(self, enable);
    if (ret != cAtOk)
        return ret;

    /* Hardware for this product does not enable SERDES by default, so software
     * needs to take this job */
    ret |= AtSerdesControllerEnable(AtSdhLineSerdesController((AtSdhLine)self), enable);

    return ret;
    }

static eAtRet LoopOutEnable(ThaSdhLine self, eBool enable)
    {
	AtUnused(self);
    return enable ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool LoopOutIsEnabled(ThaSdhLine self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static void OverrideAtChannel(AtSdhLine self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, RxForcibleAlarmsGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideThaSdhLine(AtSdhLine self)
    {
    ThaSdhLine line = (ThaSdhLine)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaSdhLineMethods = mMethodsGet(line);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhLineOverride, m_ThaSdhLineMethods, sizeof(m_ThaSdhLineOverride));
        mMethodOverride(m_ThaSdhLineOverride, SerdesLoopInEnable);
        mMethodOverride(m_ThaSdhLineOverride, SerdesLoopInIsEnabled);
        mMethodOverride(m_ThaSdhLineOverride, SerdesLoopoutEnable);
        mMethodOverride(m_ThaSdhLineOverride, SerdesLoopoutIsEnabled);
        mMethodOverride(m_ThaSdhLineOverride, LoopOutEnable);
        mMethodOverride(m_ThaSdhLineOverride, LoopOutIsEnabled);
        }

    mMethodsSet(line, &m_ThaSdhLineOverride);
    }

static void OverrideAtSdhLine(AtSdhLine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhLineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhLineOverride, m_AtSdhLineMethods, sizeof(m_AtSdhLineOverride));
        mMethodOverride(m_AtSdhLineOverride, RateIsSupported);
        }

    mMethodsSet(self, &m_AtSdhLineOverride);
    }

static void Override(AtSdhLine self)
    {
    OverrideAtChannel(self);
    OverrideThaSdhLine(self);
    OverrideAtSdhLine(self);
    }

static AtSdhLine ObjectInit(AtSdhLine self, uint32 channelId, AtModuleSdh module, uint8 version)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmMlpppSdhLineObjectInit(self, channelId, module, version) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhLine Tha60060011SdhLineNew(uint32 channelId, AtModuleSdh module, uint8 version)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhLine newLine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newLine, channelId, module, version);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : ThaStmMlpppModulePpp.c
 *
 * Created Date: Jan 22, 2013
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaStmMlpppModulePppInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaModulePppMethods m_ThaModulePppOverride;
static tAtModuleMethods m_AtModuleOverride;

/* Cache super implementation */
static const tAtModuleMethods *m_AtModuleMethods = NULL;
static const tThaModulePppMethods *m_ThaModulePppMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void OamFlush(ThaModulePpp self)
    {
    uint32 regVal;

    if (ThaModulePppOamModeIsDirect((AtModulePpp)self))
        return;

    /* Flush */
    regVal = mModuleHwRead(self, cThaStmMlpppMpigOamReceiveIntrCtrl);
    mFieldIns(&regVal, cThaStmMlpppMpigOamFlushMask, cThaStmMlpppMpigOamFlushShift, 1);
    mModuleHwWrite(self, cThaStmMlpppMpigOamReceiveIntrCtrl, regVal);

    /* Disable flush */
    regVal = mModuleHwRead(self, cThaStmMlpppMpigOamReceiveIntrCtrl);
    mFieldIns(&regVal, cThaStmMlpppMpigOamFlushMask, cThaStmMlpppMpigOamFlushShift, 0);
    mModuleHwWrite(self, cThaStmMlpppMpigOamReceiveIntrCtrl, regVal);
    }

static void OamReceiveStickyClear(ThaModulePpp self, AtIpCore ipCore)
    {
    AtHal hal;

    if (ThaModulePppOamModeIsDirect((AtModulePpp)self))
    	{
        m_ThaModulePppMethods->OamReceiveStickyClear(self, ipCore);
        return;
    	}

    hal = AtIpCoreHalGet(ipCore);
    AtHalNoLockWrite(hal, cThaStmMlpppMpigOamReceiveSticky, 0xF);
    }

static void OverrideThaModulePpp(ThaStmMlpppModulePpp self)
    {
    ThaModulePpp pppModule = (ThaModulePpp)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePppMethods = mMethodsGet(pppModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePppOverride, m_ThaModulePppMethods, sizeof(m_ThaModulePppOverride));
        mMethodOverride(m_ThaModulePppOverride, OamReceiveStickyClear);
        }

    mMethodsSet(pppModule, &m_ThaModulePppOverride);
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret;

    /* Let the super do first */
    ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    OamFlush((ThaModulePpp)self);
    return cAtOk;
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static void OamDirectModeEnable(ThaModulePpp self, eBool enable)
    {
    uint32 regVal = mModuleHwRead(self, cThaStmMlpppMpigOamReceiveIntrCtrl);
    mFieldIns(&regVal, cThaStmMlpppMpigDirectOamMask, cThaStmMlpppMpigDirectOamShift, mBoolToBin(enable));
    mModuleHwWrite(self, cThaStmMlpppMpigOamReceiveIntrCtrl, regVal);

    self->oamDirectMode = enable;
    }

static eAtRet Setup(AtModule self)
    {
    eAtRet ret;
    ThaModulePpp pppModule = (ThaModulePpp)self;

    /* Super setup */
    ret = m_AtModuleMethods->Setup(self);
    if (ret != cAtOk)
        return ret;

    /* Enable OAM direct mode */
    OamDirectModeEnable(pppModule, mMethodsGet(pppModule)->DirectOam(pppModule));

    return cAtOk;
    }

static void OverrideAtModule(ThaStmMlpppModulePpp self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(module), sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, Setup);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaStmMlpppModulePpp);
    }

AtModulePpp ThaStmMlpppModulePppObjectInit(AtModulePpp self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleStmPppObjectInit(self, device) == NULL)
        return NULL;

    /* Override */
    OverrideThaModulePpp((ThaStmMlpppModulePpp)self);
    OverrideAtModule((ThaStmMlpppModulePpp)self);

    /* Only initialize method structures one time */
    m_methodsInit = 1;

    return self;
    }

AtModulePpp ThaStmMlpppModuleStmPppNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePpp newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ThaStmMlpppModulePppObjectInit(newModule, device);
    }

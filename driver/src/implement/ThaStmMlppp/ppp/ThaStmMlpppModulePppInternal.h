/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PPP
 * 
 * File        : ThaStmMlpppModulePppInternal.h
 * 
 * Created Date: Aug 14, 2013
 *
 * Description : PPP module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASTMMLPPPMODULEPPPINTERNAL_H_
#define _THASTMMLPPPMODULEPPPINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../man/ThaStmMlpppIntrReg.h"
#include "../../default/ppp/ThaModulePppInternal.h"
#include "../../default/ppp/ThaModuleStmPpp.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaStmMlpppModulePpp
    {
    tThaModuleStmPpp super;
    }tThaStmMlpppModulePpp;

typedef struct tThaStmMlpppModulePpp * ThaStmMlpppModulePpp;


/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePpp ThaStmMlpppModulePppObjectInit(AtModulePpp self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THASTMMLPPPMODULEPPPINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : ThaStmMlpppSdhLineSerdes.c
 *
 * Created Date: Nov 15, 2013
 *
 * Description : SDH Line SERDES controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../default/physical/ThaSdhLineSerdesControllerInternal.h"
#include "ThaStmMlpppSdhLineSerdesController.h"

/*--------------------------- Define -----------------------------------------*/
/*
 * Port 0-1: use one register, bit0 is for Line 0 and bit1 for line 1
 * Port 2: use bit 0 of one register
 * Port 3: use bit 0 of one register
 * ==> If port ID is 1, bit 1 is used, other ports use bit 0
 */
#define cSerdesTimingModeMask(portId)  (((portId) == 1) ? cBit1 : cBit0)
#define cSerdesTimingModeShift(portId) (((portId) == 1) ? 1 : 0)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaStmMlpppSdhLineSerdesController
    {
    tThaSdhLineSerdesController super;

    /* Private data */
    }tThaStmMlpppSdhLineSerdesController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaSdhLineSerdesControllerMethods  m_ThaSdhLineSerdesControllerOverride;
static tAtSerdesControllerMethods          m_AtSerdesControllerOverride;

/* Save super implementation */
static const tThaSdhLineSerdesControllerMethods *m_ThaSdhLineSerdesControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 PortId(ThaSdhLineSerdesController self)
    {
    AtChannel line = AtSerdesControllerPhysicalPortGet((AtSerdesController)self);
    return (uint8)AtChannelIdGet(line);
    }

static uint32 LockToDataRegAddress(ThaSdhLineSerdesController self)
    {
    uint32 portId = PortId(self);

    if (portId < 2)
        return 0xf28864;
    if (portId == 3)
        return 0xf29064;

    return 0xf29864;
    }

static uint32 LockToDataRegMask(ThaSdhLineSerdesController self)
    {
    return cSerdesTimingModeMask(PortId(self));
    }

static uint32 LockToDataRegShift(ThaSdhLineSerdesController self)
    {
    return cSerdesTimingModeShift(PortId(self));
    }

static uint32 LockToRefRegAddress(ThaSdhLineSerdesController self)
    {
    uint8 portId = (uint8)PortId(self);

    if (portId < 2)
        return 0xf28865;
    if (portId == 3)
        return 0xf29065;

    return 0xf29865;
    }

static uint32 LockToRefRegMask(ThaSdhLineSerdesController self)
    {
    return cSerdesTimingModeMask(PortId(self));
    }

static uint32 LockToRefRegShift(ThaSdhLineSerdesController self)
    {
    return cSerdesTimingModeShift(PortId(self));
    }

static eBool CanControlTimingMode(AtSerdesController self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static void OverrideThaSdhLineSerdesController(AtSerdesController self)
    {
    ThaSdhLineSerdesController sdhLineSerdes = (ThaSdhLineSerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaSdhLineSerdesControllerMethods = mMethodsGet(sdhLineSerdes);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhLineSerdesControllerOverride, m_ThaSdhLineSerdesControllerMethods, sizeof(m_ThaSdhLineSerdesControllerOverride));

        mMethodOverride(m_ThaSdhLineSerdesControllerOverride, LockToDataRegAddress);
        mMethodOverride(m_ThaSdhLineSerdesControllerOverride, LockToDataRegMask);
        mMethodOverride(m_ThaSdhLineSerdesControllerOverride, LockToDataRegShift);
        mMethodOverride(m_ThaSdhLineSerdesControllerOverride, LockToRefRegAddress);
        mMethodOverride(m_ThaSdhLineSerdesControllerOverride, LockToRefRegMask);
        mMethodOverride(m_ThaSdhLineSerdesControllerOverride, LockToRefRegShift);
        }

    mMethodsSet(sdhLineSerdes, &m_ThaSdhLineSerdesControllerOverride);
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride,  mMethodsGet(self), sizeof(m_AtSerdesControllerOverride));
        mMethodOverride(m_AtSerdesControllerOverride, CanControlTimingMode);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideThaSdhLineSerdesController(self);
    OverrideAtSerdesController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaStmMlpppSdhLineSerdesController);
    }

static AtSerdesController ObjectInit(AtSerdesController self, AtSdhLine sdhLine, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaSdhLineSerdesControllerObjectInit(self, sdhLine, serdesId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController ThaStmMlpppSdhLineSerdesControllerNew(AtSdhLine sdhLine, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, sdhLine, serdesId);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : ThaStmMlpppSdhLineSerdesController.h
 * 
 * Created Date: Nov 15, 2013
 *
 * Description : SDH Line SERDES controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/
#ifndef _THASTMMLPPPSDHLINESERDESCONTROLLER_H_
#define _THASTMMLPPPSDHLINESERDESCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../default/physical/ThaSerdesController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSerdesController ThaStmMlpppSdhLineSerdesControllerNew(AtSdhLine sdhLine, uint32 serdesId);

#ifdef __cplusplus
}
#endif
#endif /* _THASTMMLPPPSDHLINESERDESCONTROLLER_H_ */


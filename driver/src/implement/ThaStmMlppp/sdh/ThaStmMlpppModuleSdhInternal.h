/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : ThaStmMlpppSdhLineInternal.h
 * 
 * Created Date: May 8, 2013
 *
 * Description : SDH module of MLPPP product
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASTMMLPPPSDHLINEINTERNAL_H_
#define _THASTMMLPPPSDHLINEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../default/sdh/ThaSdhLineInternal.h"
#include "../../default/sdh/ThaModuleSdhInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaStmMlpppSdhLine
    {
    tThaSdhLine super;

    /* Private data */
    }tThaStmMlpppSdhLine;

typedef struct tThaStmMlpppModuleSdh * ThaStmMlpppModuleSdh;

typedef struct tThaStmMlpppModuleSdh
    {
    tThaModuleSdh super;
    }tThaStmMlpppModuleSdh;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhLine ThaStmMlpppSdhLineObjectInit(AtSdhLine self, uint32 channelId, AtModuleSdh module, uint8 version);
AtModuleSdh ThaStmMlpppModuleSdhObjectInit(AtModuleSdh self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THASTMMLPPPSDHLINEINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : ThaStmMlpppModuleSdh.c
 *
 * Created Date: Feb 19, 2013
 *
 * Description : SDH module of STM MLPPP product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaStmMlpppModuleSdhInternal.h"
#include "../physical/ThaStmMlpppSdhLineSerdesController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleSdhMethods m_ThaModuleSdhOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static AtSerdesController SerdesControllerCreate(ThaModuleSdh self, AtSdhLine line, uint32 serdesId)
    {
	AtUnused(self);
    return ThaStmMlpppSdhLineSerdesControllerNew(line, serdesId);
    }

static void OverrideThaModuleSdh(ThaStmMlpppModuleSdh self)
    {
    ThaModuleSdh sdhModule = (ThaModuleSdh)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleSdhOverride, mMethodsGet(sdhModule), sizeof(m_ThaModuleSdhOverride));
        mMethodOverride(m_ThaModuleSdhOverride, SerdesControllerCreate);
        }

    mMethodsSet(sdhModule, &m_ThaModuleSdhOverride);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaStmMlpppModuleSdh);
    }

static void Override(AtModuleSdh self)
    {
    OverrideThaModuleSdh((ThaStmMlpppModuleSdh)self);
    }

AtModuleSdh ThaStmMlpppModuleSdhObjectInit(AtModuleSdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleSdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSdh ThaStmMlpppModuleSdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ThaStmMlpppModuleSdhObjectInit(newModule, device);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : ThaStmMlpppDevice.h
 * 
 * Created Date: May 6, 2013
 *
 * Description : STM MLPPP product
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASTMMLPPPDEVICE_H_
#define _THASTMMLPPPDEVICE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../default/man/ThaDevice.h" /* Super */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaStmMlpppDevice * ThaStmMlpppDevice;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Concrete devices */
AtDevice ThaStmMlpppDeviceNew(AtDriver driver, uint32 productCode);

AtDevice Tha60070023DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60030051DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60060011DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60071032DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha65031032DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60031035DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60091132DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60091135DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60030101DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60031032DeviceNew(AtDriver driver, uint32 productCode);

#ifdef __cplusplus
}
#endif
#endif /* _THASTMMLPPPDEVICE_H_ */


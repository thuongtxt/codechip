/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : ThaStmMlpppIntrReg.h
 * 
 * Created Date: Jan 22, 2013
 *
 * Description : Interrupt registers
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASTMMLPPPINTRREG_H_
#define _THASTMMLPPPINTRREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Global interrupt status */
#define cThaStmMlpppRegChipIntrStatus  0x8

/* Global interrupt enable */
#define cThaStmMlpppRegChipIntrCtrl    0x9
#define cThaStmMlpppMpigIntrStatMask   cBit8

/* OAM interrupt control */
#define cThaStmMlpppMpigOamReceiveIntrCtrl 0x844004
#define cThaStmMlpppMpigOamReceiveIntrEnMask cBit1
#define cThaStmMlpppMpigOamReceiveIntrEnShift 1

#define cThaStmMlpppMpigOamFlushMask cBit8
#define cThaStmMlpppMpigOamFlushShift 8

#define cThaStmMlpppMpigDirectOamMask cBit2
#define cThaStmMlpppMpigDirectOamShift 2

/* OAM receive sticky */
#define cThaStmMlpppMpigOamReceiveSticky 0x844005

#ifdef __cplusplus
}
#endif
#endif /* _THASTMMLPPPINTRREG_H_ */


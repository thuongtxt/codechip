/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : ThaStmMlpppDeviceInternal.h
 * 
 * Created Date: May 6, 2013
 *
 * Description : STM MLPPP device descriptor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASTMMLPPPDEVICEINTERNAL_H_
#define _THASTMMLPPPDEVICEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../default/man/ThaDeviceInternal.h"
#include "../../default/cdr/ThaModuleCdrStm.h"
#include "../../default/pdh/ThaStmModulePdh.h"
#include "../../default/map/ThaModuleStmMap.h"
#include "../../default/ppp/ThaModulePppInternal.h"
#include "../../default/ppp/ThaModuleStmPpp.h"
#include "../../default/encap/ThaModuleStmEncap.h"
#include "../../default/eth/ThaModuleEth.h"
#include "../../default/ram/ThaModuleRam.h"
#include "../../default/sdh/ThaModuleSdh.h"

#include "ThaStmMlpppDevice.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaStmMlpppDevice
    {
    tThaDevice super;

    /* Additional methods and attributes ... */
    }tThaStmMlpppDevice;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDevice ThaStmMlpppDeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode);

#ifdef __cplusplus
}
#endif
#endif /* _THASTMMLPPPDEVICEINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : ThaStmMlpppIntrController.c
 *
 * Created Date: Aug 15, 2013
 *
 * Description : Interrupt controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../default/man/intrcontroller/ThaIntrControllerInternal.h"
#include "../../default/man/ThaDevice.h"
#include "ThaStmMlpppIntrReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaStmMlpppIntrController* ThaStmMlpppIntrController;

typedef struct tThaStmMlpppIntrController
    {
    tThaIntrController super;
    }tThaStmMlpppIntrController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tThaIpCoreMethods m_ThaIpCoreOverride;

/* Reference to super implementation (option) */
static const tThaIpCoreMethods *m_ThaIpCoreImplement;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void OamGlobalInterruptEnable(AtIpCore ipCore, eBool enable)
    {
    uint32 interruptEnReg;
    AtHal hal = AtIpCoreHalGet(ipCore);

    /* Global interrupt */
    interruptEnReg = AtHalNoLockRead(hal, cThaStmMlpppRegChipIntrCtrl);

    if (enable)
        interruptEnReg |= cThaStmMlpppMpigIntrStatMask;
    else
        interruptEnReg &= ~cThaStmMlpppMpigIntrStatMask;

    AtHalNoLockWrite(hal, cThaStmMlpppRegChipIntrCtrl, interruptEnReg);
    }

static void PppHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    uint32 interruptEnReg;
    AtHal hal = AtIpCoreHalGet(mIpCore(self));

    /* Global interrupt */
    OamGlobalInterruptEnable(mIpCore(self), enable);

    /* Enable OAM interrupt */
    interruptEnReg = AtHalNoLockRead(hal, cThaStmMlpppMpigOamReceiveIntrCtrl);
    mFieldIns(&interruptEnReg, cThaStmMlpppMpigOamReceiveIntrEnMask, cThaStmMlpppMpigOamReceiveIntrEnShift, mBoolToBin(enable));
    AtHalNoLockWrite(hal, cThaStmMlpppMpigOamReceiveIntrCtrl, interruptEnReg);
    }

static uint32 PartInterruptDisable(ThaIpCore self, uint8 partId)
    {
    uint32 interruptEnable;
    AtHal hal = AtIpCoreHalGet(mIpCore(self));
    uint32 regAddr;
    if (hal == NULL)
        return 0;

    /* Get global interrupt enable */
    regAddr = cThaStmMlpppRegChipIntrCtrl + ThaIpCorePartOffset(self, partId);
    interruptEnable = AtHalNoLockRead(hal, regAddr);

    /* Disable */
    AtHalNoLockWrite(hal, regAddr, 0);

    return interruptEnable;
    }

static uint32 InterruptDisable(ThaIpCore self)
    {
    uint8 part_i;
    uint32 interruptEnable = 0;

    for (part_i = 0; part_i < ThaIpCoreNumParts(self); part_i++)
        interruptEnable |= PartInterruptDisable(self, part_i);

    return interruptEnable;
    }

static void AllModulesInterruptEnable(ThaIpCore self, uint32 intrEnMask)
    {
    AtHal hal = AtIpCoreHalGet(mIpCore(self));
    if (hal == NULL)
        return;

    AtHalNoLockWrite(hal, cThaStmMlpppRegChipIntrCtrl, intrEnMask);
    }

static uint32 InterruptStatusGet(ThaIpCore self)
    {
    uint8 part_i;
    uint32 interruptStatus = 0;
    AtHal hal = AtIpCoreHalGet(mIpCore(self));
    if (hal == NULL)
        return 0;

    for (part_i = 0; part_i < ThaIpCoreNumParts(self); part_i++)
        interruptStatus |= AtHalNoLockRead(hal, cThaStmMlpppRegChipIntrStatus + ThaIpCorePartOffset(self, part_i));

    return interruptStatus;
    }

static eBool PppCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
	AtUnused(self);
    return ((intrStatus & cThaStmMlpppMpigIntrStatMask) != 0) ? cAtTrue : cAtFalse;
    }

static void SdhHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    uint8 part_i;
    AtHal hal = AtIpCoreHalGet(mIpCore(self));
    ThaDevice device = (ThaDevice)AtIpCoreDeviceGet(mIpCore(self));

    for (part_i = 0; part_i < ThaDeviceNumPartsOfModule(device, cAtModuleSdh); part_i++)
        {
        uint32 partOffset = ThaDeviceModulePartOffset(device, cAtModuleSdh, part_i);
        uint32 hwIntrEnable = AtHalNoLockRead(hal, cThaStmMlpppRegChipIntrCtrl + partOffset);
        uint32 hwIntrOcn = enable ? (hwIntrEnable | cBit0) : (hwIntrEnable & ~cBit0);

        AtHalWrite(hal, cThaStmMlpppRegChipIntrCtrl + partOffset, hwIntrOcn);
        }
    }

static eBool SdhCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
	AtUnused(self);
    return (intrStatus & cBit0) ? cAtTrue : cAtFalse;
    }

static void OverrideThaIpCore(ThaStmMlpppIntrController self)
    {
    ThaIpCore thaIp = (ThaIpCore)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaIpCoreImplement = mMethodsGet(thaIp);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaIpCoreOverride, m_ThaIpCoreImplement, sizeof(m_ThaIpCoreOverride));
        mMethodOverride(m_ThaIpCoreOverride, PppCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, InterruptStatusGet);
        mMethodOverride(m_ThaIpCoreOverride, InterruptDisable);
        mMethodOverride(m_ThaIpCoreOverride, PppHwInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, AllModulesInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, SdhHwInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, SdhCauseInterrupt);
        }

    mMethodsSet(thaIp, &m_ThaIpCoreOverride);
    }

static void Override(ThaStmMlpppIntrController self)
    {
    OverrideThaIpCore(self);
    }

/* Constructor of ThaStmMlpppIntrController object */
static ThaIntrController ObjectInit(ThaIntrController self, AtIpCore ipCore)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaStmMlpppIntrController));

    /* Super constructor */
    if (ThaIntrControllerObjectInit(self, ipCore) == NULL)
        return NULL;

    /* Override */
    Override((ThaStmMlpppIntrController)self);

    m_methodsInit = 1;

    return self;
    }

/* Create controller object */
ThaIntrController ThaStmMlpppIntrControllerNew(AtIpCore ipCore)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaIntrController newController = mMethodsGet(osal)->MemAlloc(osal, sizeof(tThaStmMlpppIntrController));
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, ipCore);
    }

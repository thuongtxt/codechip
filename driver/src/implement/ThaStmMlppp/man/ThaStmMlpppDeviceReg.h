/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : ThaStmMlpppDeviceReg.h
 * 
 * Created Date: Oct 11, 2013
 *
 * Description : STM MLPPP product
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASTMMLPPPDEVICEREG_H_
#define _THASTMMLPPPDEVICEREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Debug registers */
/* FPGA Top Sticky 0 */
#define cThaDebugFpgaTopSticky0                         0x00F00050
#define cThaDebugFpgaZbtPllMask                         cBit2
#define cThaDebugFpgaZbtPllShift                        2

#define cThaDebugFpgaGePllMask                          cBit1
#define cThaDebugFpgaGePllShift                         1

#define cThaDebugFpgaSysPllMask                         cBit0
#define cThaDebugFpgaSysPllShift                        0

/* FPGA Top Sticky 1 */
#define cThaDebugFpgaTopSticky1                         0x00F00051
#define cThaDebugFpgaDdrAfiResetMask(ddrId)             (cBit16 << ddrId)
#define cThaDebugFpgaDdrAfiResetShift(ddrId)            (16 + ddrId)

#define cThaDebugFpgaDdrSoftResetMask(ddrId)            (cBit12 << ddrId)
#define cThaDebugFpgaDdrSoftResetShift(ddrId)           (12 + ddrId)

#define cThaDebugFpgaDdrInitFailMask(ddrId)             (cBit8 << ddrId)
#define cThaDebugFpgaDdrInitFailShift(ddrId)            (8 + ddrId)

#define cThaDebugFpgaDdrCalibUnSuccessMask(ddrId)       (cBit4 << ddrId)
#define cThaDebugFpgaDdrCalibUnSuccessShift(ddrId)      (4 + ddrId)

#define cThaDebugFpgaDdrCalibFailMask(ddrId)            (cBit0 << ddrId)

/* FPGA Top Sticky 2 */
#define cThaDebugFpgaTopSticky2                         0x00F00052
#define cThaDebugFpgaOcnXcvrTxDigResetMask(groupId)     (cBit28 << groupId)
#define cThaDebugFpgaOcnXcvrTxDigResetShift(groupId)    (28 + groupId)

#define cThaDebugFpgaOcnXcvrRxDigResetMask(groupId)     (cBit24 << groupId)
#define cThaDebugFpgaOcnXcvrRxDigResetShift(groupId)    (24 + groupId)

#define cThaDebugFpgaOcnXcvrRxAnaResetMask(groupId)     (cBit20 << groupId)
#define cThaDebugFpgaOcnXcvrRxAnaResetShift(groupId)    (20 + groupId)

#define cThaDebugFpgaOcnXcvrPowerDownMask(groupId)      (cBit16 << groupId)
#define cThaDebugFpgaOcnXcvrPowerDownShift(groupId)     (16 + groupId)

#define cThaDebugFpgaOcnXcvrTxNotReadyMask(groupId)     (cBit8 << groupId)
#define cThaDebugFpgaOcnXcvrTxNotReadyShift(groupId)    (8 + groupId)

#define cThaDebugFpgaOcnXcvrRxNotReadyMask(groupId)     (cBit4 << groupId)
#define cThaDebugFpgaOcnXcvrRxNotReadyShift(groupId)    (4 + groupId)

#define cThaDebugFpgaOcnXcvrRxPllUnlockMask(groupId)    (cBit0 << groupId)
#define cThaDebugFpgaOcnXcvrRxPllUnlockShift(groupId)   (0 + groupId)

/* FPGA Top Status 0 */
#define cThaDebugFpgaTopStatus0                         0x00F00060
#define cThaDebugFpgaOampintrMask                       cBit18
#define cThaDebugFpgaOampintrShift                      18

#define cThaDebugFpgaCPU_INTMask                        cBit17
#define cThaDebugFpgaCPU_INTShift                       17

#define cThaDebugFpgaMpceupintrMask                     cBit16
#define cThaDebugFpgaMpceupintrShift                    16

/* FPGA Top Status 1 */
#define cThaDebugFpgaTopStatus1                         0x00F00061

/* FPGA Top Status 2 */
#define cThaDebugFpgaTopStatus2                         0x00F00062
#define cThaDebugFpgaXcvrOcnLosMask(lineId)             (cBit4 << ((lineId != 3) ? lineId : (lineId + 1)))
#define cThaDebugFpgaXcvrOcnLosShift(lineId)            (4 + ((lineId != 3) ? lineId : (lineId + 1)))

#define cThaDebugFpgaSfpOcnLosMask(lineId)              (cBit0 << lineId)
#define cThaDebugFpgaSfpOcnLosShift(lineId)             (0 + lineId)

#ifdef __cplusplus
}
#endif
#endif /* _THASTMMLPPPDEVICEREG_H_ */


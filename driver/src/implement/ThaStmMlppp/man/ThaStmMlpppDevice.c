/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : ThaStmMlpppDevice.c
 *
 * Created Date: Aug 30, 2012
 *
 * Author      : namnn
 *
 * Description : STM MLPPP image
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../default/ppp/ThaModulePpp.h"
#include "../../default/util/ThaUtil.h"
#include "../../default/binder/ThaEncapBinder.h"
#include "ThaStmMlpppDeviceInternal.h"
#include "ThaStmMlpppDeviceReg.h"


/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mPartRegisterPrint(regName, regValue, partOffset)                      \
    do                                                                         \
        {                                                                      \
        regValue = AtHalRead(hal, cThaDebug##regName + partOffset);            \
        AtPrintc(cSevNormal, "    %-20s: 0x%08X (addr = 0x%08X)\r\n", #regName, regValue, cThaDebug##regName + partOffset);\
        }while(0)

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Local variables --------------------------------*/
/* Implementation of this class */
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceMethods  m_AtDeviceOverride;
static tThaDeviceMethods m_ThaDeviceOverride;

/* Super implementation */
static tAtDeviceMethods *m_AtDeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
extern AtModuleSdh   ThaStmMlpppModuleSdhNew(AtDevice device);

/*--------------------------- Implementation ---------------------------------*/
static eBool ModuleIsSupported(AtDevice self, eAtModule moduleId)
    {
    uint32 _moduleId = (uint32)moduleId;

    /* Private modules */
    switch (_moduleId)
        {
        case cThaModuleDemap:      return cAtTrue;
        case cThaModuleMap:        return cAtTrue;
        case cThaModuleCdr:        return cAtTrue;
        case cThaModuleCla:        return cAtTrue;
        case cThaModuleMpig:       return cAtTrue;
        case cThaModuleMpeg:       return cAtTrue;
        case cThaModulePmcMpeg:    return cAtTrue;
        case cThaModulePmcMpig:    return cAtTrue;
        case cThaModuleOcn:        return cAtTrue;
        case cThaModulePoh:        return cAtTrue;
        case cThaModuleCos:        return cAtTrue;

        default:
            break;
        }

    /* Public modules */
    switch (_moduleId)
        {
        case cAtModulePdh:         return cAtTrue;
        case cAtModulePpp:         return cAtTrue;
        case cAtModuleEncap:       return cAtTrue;
        case cAtModuleEth:         return cAtTrue;
        case cAtModuleSdh:         return cAtTrue;
        case cAtModuleRam:         return cAtTrue;
        case cAtModuleClock:       return cAtTrue;
        case cAtModulePktAnalyzer: return cAtTrue;

        default:
            break;
        }

    return m_AtDeviceMethods->ModuleIsSupported(self, moduleId);
    }

static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    uint32 moduleId_ = (uint32)moduleId; /* Just to make the compiler happy */

    /* Public modules */
    if (moduleId_ == cAtModulePdh)        return (AtModule)ThaStmModulePdhNew(self);
    if (moduleId_ == cAtModulePpp)        return (AtModule)ThaStmMlpppModuleStmPppNew(self);
    if (moduleId_ == cAtModuleEncap)      return (AtModule)ThaModuleStmEncapNew(self);
    if (moduleId_ == cAtModuleEth)        return (AtModule)ThaModuleEthStmPppNew(self);
    if (moduleId_ == cAtModuleSdh)        return (AtModule)ThaStmMlpppModuleSdhNew(self);
    if (moduleId_ == cAtModuleClock)      return (AtModule)ThaStmMlpppModuleClockNew(self);
    if (moduleId == cAtModulePktAnalyzer) return (AtModule)ThaModulePktAnalyzerPppNew(self);

    /* Private modules */
    if (moduleId_ == cThaModuleCdr)     return (AtModule)ThaModuleCdrStmNew(self);
    if (moduleId_ == cThaModuleMap)     return (AtModule)ThaModuleStmMapNew(self);
    if (moduleId_ == cThaModuleDemap)   return (AtModule)ThaModuleStmDemapNew(self);
    if (moduleId_ == cThaModuleCos)     return ThaModuleCosPppNew(self);
    if (moduleId_ == cThaModuleCla)     return ThaModuleClaPppNew(self);
    if (moduleId_ == cThaModuleMpig)    return ThaModuleMpigNew(self);
    if (moduleId_ == cThaModuleMpeg)    return ThaModuleMpegNew(self);
    if (moduleId_ == cThaModulePmcMpeg) return ThaModulePmcMpegNew(self);
    if (moduleId_ == cThaModulePmcMpig) return ThaModulePmcMpigNew(self);
    
    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static const eAtModule *AllSupportedModulesGet(AtDevice self, uint8 *numModules)
    {
    static const eAtModule supportedModules[] = {cAtModulePdh,
                                                 cAtModulePpp,
                                                 cAtModuleEncap,
                                                 cAtModuleEth,
                                                 cAtModuleSdh,
                                                 cAtModuleRam,
                                                 cAtModuleClock,
                                                 cAtModulePktAnalyzer};
	AtUnused(self);
    if (numModules)
        *numModules = mCount(supportedModules);

    return supportedModules;
    }

static ThaIntrController IntrControllerCreate(ThaDevice self, AtIpCore core)
    {
    AtModule modulePpp = AtDeviceModuleGet((AtDevice)self, cAtModulePpp);

    if (ThaModulePppOamModeIsDirect((AtModulePpp)modulePpp))
        return ThaDirectPppOamIntrControllerNew(core);

    return ThaStmMlpppIntrControllerNew(core);
    }

static void StickiesClear(AtDevice self, uint8 partId)
    {
    AtHal hal = AtDeviceIpCoreHalGet(self, 0);
    static const uint32 cAllOne = 0xFFFFFFFF;
    uint32 partOffset = ThaDevicePartOffset((ThaDevice)self, partId);

    AtHalWrite(hal, cThaDebugFpgaTopSticky0 + partOffset, cAllOne);
    AtHalWrite(hal, cThaDebugFpgaTopSticky1 + partOffset, cAllOne);
    AtHalWrite(hal, cThaDebugFpgaTopSticky2 + partOffset, cAllOne);
    }

static void FpgaTop1Show(AtDevice self, uint8 partId, eBool isSticky)
    {
    AtHal  hal          = AtDeviceIpCoreHalGet(self, 0);
    uint32 partOffset   = ThaDevicePartOffset((ThaDevice)self, partId);
    uint32 regAddr      = isSticky ? (cThaDebugFpgaTopSticky1 + partOffset) : (cThaDebugFpgaTopStatus1 + partOffset);
    uint32 regVal       = AtHalRead(hal, regAddr);
    uint8  ddrId;
    AtModuleRam ramModule = (AtModuleRam)AtDeviceModuleGet(self, cAtModuleRam);

    /* No error */
    if (regVal == 0)
        {
        AtPrintc(cSevNormal, "    %s : None (addr = 0x%08x)\r\n", isSticky ? "FpgaTopSticky1" : "FpgaTopStatus1", regAddr);
        return;
        }

    if (isSticky)
        mPartRegisterPrint(FpgaTopSticky1, regVal, partOffset);
    else
        mPartRegisterPrint(FpgaTopStatus1, regVal, partOffset);

    for (ddrId = 0; ddrId < AtModuleRamNumDdrGet(ramModule); ddrId++)
        {
        uint8  hwVal;

        mFieldGet(regVal, cThaDebugFpgaDdrAfiResetMask(ddrId), cThaDebugFpgaDdrAfiResetShift(ddrId), uint8, &hwVal);
        if (hwVal)
            AtPrintc(cSevNormal, "    Ddr#%dAfiReset       : SET\r\n", ddrId);

        mFieldGet(regVal, cThaDebugFpgaDdrSoftResetMask(ddrId), cThaDebugFpgaDdrSoftResetShift(ddrId), uint8, &hwVal);
        if (hwVal)
            AtPrintc(cSevNormal, "    Ddr#%dSoftReset      : SET\r\n", ddrId);

        mFieldGet(regVal, cThaDebugFpgaDdrInitFailMask(ddrId), cThaDebugFpgaDdrInitFailShift(ddrId), uint8, &hwVal);
        if (hwVal)
            AtPrintc(cSevNormal, "    Ddr#%dInitFail       : SET\r\n", ddrId);

        mFieldGet(regVal, cThaDebugFpgaDdrCalibUnSuccessMask(ddrId), cThaDebugFpgaDdrCalibUnSuccessShift(ddrId), uint8, &hwVal);
        if (hwVal)
            AtPrintc(cSevNormal, "    Ddr#%dCalibUnsuccess : SET\r\n", ddrId);

        mFieldGet(regVal, cThaDebugFpgaDdrCalibFailMask(ddrId), cThaDebugFpgaDdrCalibFailMask(ddrId), uint8, &hwVal);
        if (hwVal)
            AtPrintc(cSevNormal, "    Ddr#%dCalibFail      : SET\r\n", ddrId);

        AtPrintc(cSevNormal, "\r\n");
        }
    }

static void FpgaTopSticky2Show(AtDevice self, uint8 partId)
    {
    AtHal  hal          = AtDeviceIpCoreHalGet(self, 0);
    uint32 partOffset   = ThaDevicePartOffset((ThaDevice)self, partId);
    uint32 regAddr      = cThaDebugFpgaTopSticky2 + partOffset;
    uint32 regVal       = AtHalRead(hal, regAddr);
    const  uint8 numOfOcnPllGrp = 3;
    uint8  ocnPllGrpId;
    uint8  hwVal;

    if (regVal == 0)
        {
        AtPrintc(cSevNormal, "    FpgaTopSticky2 : None (addr = 0x%08x)\r\n", regAddr);
        return;
        }

    mPartRegisterPrint(FpgaTopSticky2, regVal, partOffset);
    for (ocnPllGrpId = 0; ocnPllGrpId < numOfOcnPllGrp; ocnPllGrpId++)
        {
        mFieldGet(regVal, cThaDebugFpgaOcnXcvrTxDigResetMask(ocnPllGrpId), cThaDebugFpgaOcnXcvrTxDigResetShift(ocnPllGrpId), uint8, &hwVal);
        if (hwVal)
            AtPrintc(cSevNormal, "    OcnXcvrTxDigReset - group %d : SET\r\n", ocnPllGrpId);

        mFieldGet(regVal, cThaDebugFpgaOcnXcvrRxDigResetMask(ocnPllGrpId), cThaDebugFpgaOcnXcvrRxDigResetShift(ocnPllGrpId), uint8, &hwVal);
        if (hwVal)
            AtPrintc(cSevNormal, "    OcnXcvrRxDigReset - group %d : SET\r\n", ocnPllGrpId);

        mFieldGet(regVal, cThaDebugFpgaOcnXcvrRxAnaResetMask(ocnPllGrpId), cThaDebugFpgaOcnXcvrRxAnaResetShift(ocnPllGrpId), uint8, &hwVal);
        if (hwVal)
            AtPrintc(cSevNormal, "    OcnXcvrRxAnaReset - group %d : SET\r\n", ocnPllGrpId);

        mFieldGet(regVal, cThaDebugFpgaOcnXcvrPowerDownMask(ocnPllGrpId), cThaDebugFpgaOcnXcvrPowerDownShift(ocnPllGrpId), uint8, &hwVal);
        if (hwVal)
            AtPrintc(cSevNormal, "    OcnXcvrPowerDown  - group %d : SET\r\n", ocnPllGrpId);

        mFieldGet(regVal, cThaDebugFpgaOcnXcvrTxNotReadyMask(ocnPllGrpId), cThaDebugFpgaOcnXcvrTxNotReadyShift(ocnPllGrpId), uint8, &hwVal);
        if (hwVal)
            AtPrintc(cSevNormal, "    OcnXcvrTxNotReady - group %d : SET\r\n", ocnPllGrpId);

        mFieldGet(regVal, cThaDebugFpgaOcnXcvrRxNotReadyMask(ocnPllGrpId), cThaDebugFpgaOcnXcvrRxNotReadyShift(ocnPllGrpId), uint8, &hwVal);
        if (hwVal)
            AtPrintc(cSevNormal, "    OcnXcvrRxNotReady - group %d : SET\r\n", ocnPllGrpId);

        mFieldGet(regVal, cThaDebugFpgaOcnXcvrRxPllUnlockMask(ocnPllGrpId), cThaDebugFpgaOcnXcvrRxPllUnlockShift(ocnPllGrpId), uint8, &hwVal);
        if (hwVal)
            AtPrintc(cSevNormal, "    OcnXcvrPllUnlock  - group %d : SET\r\n", ocnPllGrpId);

        AtPrintc(cSevNormal, "\r\n");
        }
    }

static void FpgaTop0Show(AtDevice self, uint32 regVal)
    {
	AtUnused(self);
    if (mRegField(regVal, cThaDebugFpgaZbtPll))
        AtPrintc(cSevNormal, "    ZbtPllUnLock        : SET\r\n");
    if (mRegField(regVal, cThaDebugFpgaGePll))
        AtPrintc(cSevNormal, "    GbePllUnLock        : SET\r\n");
    if (mRegField(regVal, cThaDebugFpgaSysPll))
        AtPrintc(cSevNormal, "    SysPllUnLock        : SET\r\n");
    AtPrintc(cSevNormal, "\r\n");
    }

static void FpgaTopSticky0(AtDevice self, uint8 partId)
    {
    AtHal  hal          = AtDeviceIpCoreHalGet(self, 0);
    uint32 partOffset   = ThaDevicePartOffset((ThaDevice)self, partId);
    uint32 regAddr      = cThaDebugFpgaTopSticky0 + partOffset;
    uint32 regVal       = AtHalRead(hal, regAddr);

    if (regVal == 0)
        {
        AtPrintc(cSevNormal, "    FpgaTopSticky0 : None (addr = 0x%08x)\r\n", regAddr);
        return;
        }

    mPartRegisterPrint(FpgaTopSticky0, regVal, partOffset);
    FpgaTop0Show(self, regVal);
    }

static void FpgaTopStatus0Show(AtDevice self, uint8 partId)
    {
    AtHal  hal          = AtDeviceIpCoreHalGet(self, 0);
    uint32 partOffset   = ThaDevicePartOffset((ThaDevice)self, partId);
    uint32 regAddr      = cThaDebugFpgaTopStatus0 + partOffset;
    uint32 regVal       = AtHalRead(hal, regAddr);

    if (regVal == 0)
        {
        AtPrintc(cSevNormal, "    FpgaTopStatus0 : None (addr = 0x%08x)\r\n", regAddr);
        return;
        }

    mPartRegisterPrint(FpgaTopStatus0, regVal, partOffset);
    if (mRegField(regVal, cThaDebugFpgaOampintr))
        AtPrintc(cSevNormal, "    Oampintr            : SET\r\n");
    if (mRegField(regVal, cThaDebugFpgaCPU_INT))
        AtPrintc(cSevNormal, "    CPU_INT             : SET\r\n");
    if (mRegField(regVal, cThaDebugFpgaMpceupintr))
        AtPrintc(cSevNormal, "    Mpceupintr          : SET\r\n");
    FpgaTop0Show(self, regVal);
    }

static void FpgaTopStatus2Show(AtDevice self, uint8 partId)
    {
    AtHal  hal          = AtDeviceIpCoreHalGet(self, 0);
    uint32 partOffset   = ThaDevicePartOffset((ThaDevice)self, partId);
    uint32 regAddr      = cThaDebugFpgaTopStatus2 + partOffset;
    uint32 regVal       = AtHalRead(hal, regAddr);
    const  uint8 numOfOcnLine = 4;
    uint8  lineId;
    uint8  hwVal;

    if (regVal == 0)
        {
        AtPrintc(cSevNormal, "    FpgaTopStatus2 : None (addr = 0x%08x)\r\n", regAddr);
        return;
        }

    mPartRegisterPrint(FpgaTopStatus2, regVal, partOffset);
    for (lineId = 0; lineId < numOfOcnLine; lineId++)
        {
        mFieldGet(regVal, cThaDebugFpgaXcvrOcnLosMask(lineId), cThaDebugFpgaXcvrOcnLosShift(lineId), uint8, &hwVal);
        if (hwVal)
            AtPrintc(cSevNormal, "    XcvrOcnLos - line %d : SET\r\n", lineId + 1);

        mFieldGet(regVal, cThaDebugFpgaSfpOcnLosMask(lineId), cThaDebugFpgaSfpOcnLosShift(lineId), uint8, &hwVal);
        if (hwVal)
            AtPrintc(cSevNormal, "    SfpOcnLos  - line %d : SET\r\n", lineId + 1);
        }
    }

static uint8 NumParts(AtDevice self)
    {
    return ThaDeviceMaxNumParts((ThaDevice)self);
    }

static void Debug(AtDevice self)
    {
    uint8 part_i;

    /* Super implement */
    m_AtDeviceMethods->Debug(self);

    AtPrintc(cSevNormal, "\r\n");
    AtPrintc(cSevInfo, "FPGA TOP STATUS\r\n");
    AtPrintc(cSevInfo, "========================================================\r\n");

    for (part_i = 0; part_i < NumParts(self); part_i++)
        {
        AtPrintc(cSevInfo, "PART %d\r\n", part_i + 1);
        FpgaTopSticky0(self, part_i);
        FpgaTop1Show(self, part_i, cAtTrue);
        FpgaTopSticky2Show(self, part_i);
        StickiesClear(self, part_i);

        FpgaTopStatus0Show(self, part_i);
        FpgaTop1Show(self, part_i, cAtFalse);
        FpgaTopStatus2Show(self, part_i);
        }
    }

static AtEncapBinder EncapBinder(AtDevice self)
    {
    return ThaEncapDefaultBinder(self);
    }

static void OverrideAtDevice(ThaStmMlpppDevice self)
    {
    AtDevice device = (AtDevice)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(device);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(tAtDeviceMethods));
        mMethodOverride(m_AtDeviceOverride, AllSupportedModulesGet);
        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, ModuleIsSupported);
        mMethodOverride(m_AtDeviceOverride, Debug);
        mMethodOverride(m_AtDeviceOverride, EncapBinder);
        }

    mMethodsSet(device, &m_AtDeviceOverride);
    }

static void OverrideThaDevice(ThaStmMlpppDevice self)
    {
    ThaDevice device = (ThaDevice)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDeviceOverride, mMethodsGet(device), sizeof(m_ThaDeviceOverride));

        mMethodOverride(m_ThaDeviceOverride, IntrControllerCreate);
        }

    mMethodsSet(device, &m_ThaDeviceOverride);
    }

static void Override(ThaStmMlpppDevice self)
    {
    OverrideAtDevice(self);
    OverrideThaDevice(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaStmMlpppDevice);
    }

AtDevice ThaStmMlpppDeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Initialize its super */
    if (ThaDeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Override */
    Override((ThaStmMlpppDevice)self);

    /* Only initialize method structures one time */
    m_methodsInit = 1;

    return self;
    }

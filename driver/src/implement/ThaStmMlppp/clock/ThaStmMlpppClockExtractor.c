/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Clock
 *
 * File        : ThaStmMlpppClockExtractor.c
 *
 * Created Date: Sep 19, 2013
 *
 * Description : Clock extractor
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../default/clock/ThaClockExtractorInternal.h"
#include "../../default/sdh/ThaModuleSdhReg.h"
#include "../../default/sdh/ThaModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaStmMlpppClockExtractor
    {
    tThaClockExtractor super;
    }tThaStmMlpppClockExtractor;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtClockExtractorMethods  m_AtClockExtractorOverride;
static tThaClockExtractorMethods m_ThaClockExtractorOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool CanExtractSystemClock(ThaClockExtractor self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static eBool CanExtractSdhLineClock(ThaClockExtractor self, AtSdhLine line)
    {
	AtUnused(self);
    return line ? cAtTrue : cAtFalse;
    }

static eAtModuleClockRet HwSdhLineClockExtract(ThaClockExtractor self, AtSdhLine line)
    {
    uint32 regVal, regAddr;
    uint32 localId     = ThaClockExtractorLocalId(self);
    uint32 localLineId = ThaModuleSdhLineLocalId(line);

    regAddr = cThaRegClockOutputCtrl + ThaClockExtractorPartOffset(self);
    regVal = AtClockExtractorRead((AtClockExtractor)self, regAddr);
    mFieldIns(&regVal,
              cThaRegClockOutputCtrlRefClkOutSelMask(localId),
              cThaRegClockOutputCtrlRefClkOutSelShift(localId),
              localLineId);
    AtClockExtractorWrite((AtClockExtractor)self, regAddr, regVal);

    return cAtOk;
    }

static eAtModuleClockRet Enable(AtClockExtractor self, eBool enable)
    {
    uint32 regVal, regAddr;
    uint32 localId = ThaClockExtractorLocalId((ThaClockExtractor)self);

    regAddr = cThaRegClockOutputCtrl + ThaClockExtractorPartOffset((ThaClockExtractor)self);
    regVal = AtClockExtractorRead(self, regAddr);

    mFieldIns(&regVal,
              cThaRegClockOutputCtrlRefClkOutDisMask(localId),
              cThaRegClockOutputCtrlRefClkOutDisShift(localId),
              enable ? 0 : 1);

    AtClockExtractorWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool IsEnabled(AtClockExtractor self)
    {
    uint32 regVal, regAddr;
    uint32 localId = ThaClockExtractorLocalId((ThaClockExtractor)self);

    regAddr = cThaRegClockOutputCtrl + ThaClockExtractorPartOffset((ThaClockExtractor)self);
    regVal = AtClockExtractorRead(self, regAddr);
    return (regVal & cThaRegClockOutputCtrlRefClkOutDisMask(localId)) ? 0 : 1;
    }

static void OverrideThaClockExtractor(AtClockExtractor self)
    {
    ThaClockExtractor extractor = (ThaClockExtractor)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClockExtractorOverride, mMethodsGet(extractor), sizeof(m_ThaClockExtractorOverride));

        mMethodOverride(m_ThaClockExtractorOverride, CanExtractSystemClock);
        mMethodOverride(m_ThaClockExtractorOverride, CanExtractSdhLineClock);
        mMethodOverride(m_ThaClockExtractorOverride, HwSdhLineClockExtract);
        }

    mMethodsSet(extractor, &m_ThaClockExtractorOverride);
    }

static void OverrideAtClockExtractor(AtClockExtractor self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtClockExtractorOverride, mMethodsGet(self), sizeof(m_AtClockExtractorOverride));

        mMethodOverride(m_AtClockExtractorOverride, Enable);
        mMethodOverride(m_AtClockExtractorOverride, IsEnabled);
        }

    mMethodsSet(self, &m_AtClockExtractorOverride);
    }

static void Override(AtClockExtractor self)
    {
    OverrideAtClockExtractor(self);
    OverrideThaClockExtractor(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaStmMlpppClockExtractor);
    }

static AtClockExtractor ObjectInit(AtClockExtractor self, AtModuleClock clockModule, uint8 extractorId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaClockExtractorObjectInit(self, clockModule, extractorId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtClockExtractor ThaStmMlpppClockExtractorNew(AtModuleClock clockModule, uint8 extractorId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtClockExtractor newExtractor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newExtractor == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newExtractor, clockModule, extractorId);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Clock
 * 
 * File        : ThaStmMlpppModuleClockInternal.h
 * 
 * Created Date: Nov 23, 2013
 *
 * Description : Clock module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASTMMLPPPMODULECLOCKINTERNAL_H_
#define _THASTMMLPPPMODULECLOCKINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../default/clock/ThaModuleClockInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaStmMlpppModuleClock
    {
    tThaModuleClock super;
    }tThaStmMlpppModuleClock;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleClock ThaStmMlpppModuleClockObjectInit(AtModuleClock self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THASTMMLPPPMODULECLOCKINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDA
 *
 * File        : ThaStmPwModulePdaV2.c
 *
 * Created Date: Oct 30, 2013
 *
 * Description : PDA of STM PW product (FPGA version 2
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaStmPwModulePdaV2.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePdaV2Methods m_ThaModulePdaV2Override;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 PwAlarmCurrentStat(ThaModulePdaV2 self)
    {
	AtUnused(self);
    return 0x501800;
    }

static uint32 PwAlarmIntrStat(ThaModulePdaV2 self)
    {
	AtUnused(self);
    return 0x501400;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaStmPwModulePdaV2);
    }

static void OverrideThaModulePdaV2(AtModule self)
    {
    ThaModulePdaV2 pdaModule = (ThaModulePdaV2)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdaV2Override, mMethodsGet(pdaModule), sizeof(m_ThaModulePdaV2Override));

        mMethodOverride(m_ThaModulePdaV2Override, PwAlarmCurrentStat);
        mMethodOverride(m_ThaModulePdaV2Override, PwAlarmIntrStat);
        }

    mMethodsSet(pdaModule, &m_ThaModulePdaV2Override);
    }

static void Override(AtModule self)
    {
    OverrideThaModulePdaV2(self);
    }

AtModule ThaStmPwModulePdaV2ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModulePdaV2ObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule ThaStmPwModulePdaV2New(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ThaStmPwModulePdaV2ObjectInit(newModule, device);
    }

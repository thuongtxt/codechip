/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDA
 * 
 * File        : ThaStmPwModulePdaV2.h
 * 
 * Created Date: Oct 30, 2013
 *
 * Description : PDA of STM PW product (FPGA version 2
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASTMPWMODULEPDAV2_H_
#define _THASTMPWMODULEPDAV2_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../default/pda/ThaModulePdaInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaStmPwModulePdaV2
    {
    tThaModulePdaV2 super;
    }tThaStmPwModulePdaV2;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule ThaStmPwModulePdaV2ObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THASTMPWMODULEPDAV2_H_ */


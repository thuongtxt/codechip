/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : ThaStmPwIntrController.c
 *
 * Created Date: Aug 15, 2013
 *
 * Description : Interrupt controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../default/man/intrcontroller/ThaIntrControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaStmPwIntrPdhDe1Status cBit6
#define cThaStmPwIntrPdhDe1EnMask cBit6

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaStmPwIntrController* ThaStmPwIntrController;

typedef struct tThaStmPwIntrController
    {
    tThaDefaultIntrController super;
    }tThaStmPwIntrController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tThaDefaultIntrControllerMethods m_ThaDefaultIntrControllerOverride;

/* Reference to super implementation (option) */
static const tThaDefaultIntrControllerMethods *m_ThaDefaultIntrControllerImplement;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 PdhInterruptEnableMask(ThaDefaultIntrController self)
    {
	AtUnused(self);
    return cThaStmPwIntrPdhDe1EnMask;
    }

static uint32 PdhInterruptStatusMask(ThaDefaultIntrController self)
    {
	AtUnused(self);
    return cThaStmPwIntrPdhDe1Status;
    }

static void OverrideThaDefaultIntrController(ThaStmPwIntrController self)
    {
    ThaDefaultIntrController thaIp = (ThaDefaultIntrController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaDefaultIntrControllerImplement = mMethodsGet(thaIp);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDefaultIntrControllerOverride, m_ThaDefaultIntrControllerImplement, sizeof(m_ThaDefaultIntrControllerOverride));
        mMethodOverride(m_ThaDefaultIntrControllerOverride, PdhInterruptEnableMask);
        mMethodOverride(m_ThaDefaultIntrControllerOverride, PdhInterruptStatusMask);
        }

    mMethodsSet(thaIp, &m_ThaDefaultIntrControllerOverride);
    }

static void Override(ThaStmPwIntrController self)
    {
    OverrideThaDefaultIntrController(self);
    }

/* Constructor of ThaStmPwIntrController object */
static ThaIntrController ObjectInit(ThaIntrController self, AtIpCore ipCore)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaStmPwIntrController));

    /* Super constructor */
    if (ThaDefaultIntrControllerObjectInit(self, ipCore) == NULL)
        return NULL;

    /* Override */
    Override((ThaStmPwIntrController)self);

    m_methodsInit = 1;

    return self;
    }

/* Create controller object */
ThaIntrController ThaStmPwIntrControllerNew(AtIpCore ipCore)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaIntrController newController = mMethodsGet(osal)->MemAlloc(osal, sizeof(tThaStmPwIntrController));
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, ipCore);
    }

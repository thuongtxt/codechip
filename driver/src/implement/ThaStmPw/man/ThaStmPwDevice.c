/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : ThaStmPwDevice.c
 *
 * Created Date: Jan 18, 2013
 *
 * Description : STM PW image
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaStmPwDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Local variables --------------------------------*/
/* Implementation of this class */
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceMethods m_AtDeviceOverride;

/* Super implementation */
static tAtDeviceMethods *m_AtDeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool ModuleIsSupported(AtDevice self, eAtModule moduleId)
    {
    uint32 _moduleId = (uint32)moduleId;
    
    /* Private modules */
    switch (_moduleId)
        {
        case cThaModuleOcn:        return cAtTrue;
        case cThaModulePoh:        return cAtTrue;

        default:
            break;
        }

    /* Public modules */
    switch (_moduleId)
        {
        case cAtModuleSdh:         return cAtTrue;
        case cAtModulePktAnalyzer: return cAtTrue;

        default:
            break;
        }

    return m_AtDeviceMethods->ModuleIsSupported(self, moduleId);
    }

static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    uint32 moduleId_ = (uint32)moduleId;

    if (moduleId_ == cAtModulePdh)    return (AtModule)ThaStmModulePdhNew(self);
    if (moduleId_ == cThaModuleCdr)   return (AtModule)ThaModuleCdrStmNew(self);
    if (moduleId_ == cThaModuleMap)   return (AtModule)ThaModuleStmMapNew(self);
    if (moduleId_ == cThaModuleDemap) return (AtModule)ThaModuleStmDemapNew(self);

    /* Let super create other modules */
    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static const eAtModule *AllSupportedModulesGet(AtDevice self, uint8 *numModules)
    {
    static const eAtModule supportedModules[] = {cAtModulePdh,
                                                 cAtModulePw,
                                                 cAtModuleEth,
                                                 cAtModuleRam,
                                                 cAtModuleSdh,
                                                 cAtModuleBer,
                                                 cAtModulePktAnalyzer};
	AtUnused(self);
    if (numModules)
        *numModules = mCount(supportedModules);

    return supportedModules;
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(tAtDeviceMethods));
        mMethodOverride(m_AtDeviceOverride, AllSupportedModulesGet);
        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, ModuleIsSupported);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtDevice(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaStmPwDevice);
    }

AtDevice ThaStmPwDeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Initialize its super */
    if (ThaDevicePwObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Override */
    Override(self);

    /* Only initialize method structures one time */
    m_methodsInit = 1;

    return self;
    }


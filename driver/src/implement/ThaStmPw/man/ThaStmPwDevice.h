/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : ThaStmPwDevice.h
 * 
 * Created Date: May 6, 2013
 *
 * Description : STM PW device
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASTMPWDEVICE_H_
#define _THASTMPWDEVICE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../default/man/ThaDevice.h" /* Super */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Concrete devices */
AtDevice ThaStmPwDeviceV2New(AtDriver driver, uint32 productCode);

AtDevice Tha600a0011DeviceNew(AtDriver driver, uint32 productCode);

AtDevice Tha60031031DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60031031EpDeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60035021DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha61031031DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60030081DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60030080DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60070061DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60071021DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60150011DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60210021DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60210011DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha61210011DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60210031DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha61210031DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha6A210031DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60031131DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60200011DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60001031DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60031033DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha61150011DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60050061DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60020011DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha6A000010DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha68050011DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60210051DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60210012DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60210061DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60290011DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60290021DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60290022DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha6A290011DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha6A290021DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha6A290E21DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha6A033111DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha6A290022DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha6A291022DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha61290011DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha62290011DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60290061DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60291011DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60291022DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60290051DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha61290051DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60290081DeviceNew(AtDriver driver, uint32 productCode);

#ifdef __cplusplus
}
#endif
#endif /* _THASTMPWDEVICE_H_ */


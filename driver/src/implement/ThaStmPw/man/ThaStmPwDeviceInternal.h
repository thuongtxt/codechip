/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : ThaStmPwDeviceInternal.h
 * 
 * Created Date: May 6, 2013
 *
 * Description : STM PW device
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASTMPWDEVICEINTERNAL_H_
#define _THASTMPWDEVICEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../default/man/ThaDevicePwInternal.h"
#include "../../default/pdh/ThaStmModulePdh.h"
#include "../../default/map/ThaModuleAbstractMap.h"
#include "../../default/cdr/ThaModuleCdrStm.h"

#include "ThaStmPwDevice.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaStmPwDevice
    {
    tThaDevicePw super;

    /* Private data */
    }tThaStmPwDevice;

typedef struct tThaStmPwDeviceV2
    {
    tThaStmPwDevice super;

    /* Private */
    }tThaStmPwDeviceV2;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDevice ThaStmPwDeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode);
AtDevice ThaStmPwDeviceV2ObjectInit(AtDevice self, AtDriver driver, uint32 productCode);

#ifdef __cplusplus
}
#endif
#endif /* _THASTMPWDEVICEINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PWE
 *
 * File        : ThaStmPwModulePweV2.c
 *
 * Created Date: Sep 14, 2013
 *
 * Description : PWE module for STM product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaStmPwModulePweV2.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePweV2Methods m_ThaModulePweV2Override;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 PwTxEthHdrValCtrl(ThaModulePweV2 self)
    {
	AtUnused(self);
    return 0x308000;
    }

static void OverrideThaModulePweV2(AtModule self)
    {
    ThaModulePweV2 pweModule = (ThaModulePweV2)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePweV2Override, mMethodsGet(pweModule), sizeof(m_ThaModulePweV2Override));

        mMethodOverride(m_ThaModulePweV2Override, PwTxEthHdrValCtrl);
        }

    mMethodsSet(pweModule, &m_ThaModulePweV2Override);
    }

static void Override(AtModule self)
    {
    OverrideThaModulePweV2(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaStmPwModulePweV2);
    }

AtModule ThaStmPwModulePweV2ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModulePweV2ObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule ThaStmPwModulePweV2New(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ThaStmPwModulePweV2ObjectInit(newModule, device);
    }


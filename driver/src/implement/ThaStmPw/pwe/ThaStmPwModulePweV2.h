/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PWE
 * 
 * File        : ThaStmPwModulePweV2.h
 * 
 * Created Date: Sep 25, 2013
 *
 * Description : PWE module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASTMPWMODULEPWEV2_H_
#define _THASTMPWMODULEPWEV2_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../default/pwe/ThaModulePweInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaStmPwModulePweV2
    {
    tThaModulePweV2 super;
    }tThaStmPwModulePweV2;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule ThaStmPwModulePweV2ObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THASTMPWMODULEPWEV2_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : ThaPdhMlpppDevice.c
 *
 * Created Date: Aug 30, 2012
 *
 * Author      : namnn
 *
 * Description : 16 E1s MLPPP image
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaPdhMlpppDeviceInternal.h"
#include "../ppp/ThaPdhMlpppModulePppInternal.h"
#include "../../default/encap/ThaModuleEncap.h"
#include "../../default/binder/ThaEncapBinder.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Local variables --------------------------------*/
/* Implementation of this class */
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceMethods  m_AtDeviceOverride;
static tThaDeviceMethods m_ThaDeviceOverride;

/* Super implementation */
static tAtDeviceMethods *m_AtDeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool ModuleIsSupported(AtDevice self, eAtModule moduleId)
    {
    uint32 _moduleId = (uint32)moduleId;
    
    /* Private modules */
    switch (_moduleId)
        {
        case cThaModuleDemap:      return cAtTrue;
        case cThaModuleMap:        return cAtTrue;
        case cThaModuleCdr:        return cAtTrue;
        case cThaModuleCla:        return cAtTrue;
        case cThaModuleMpig:       return cAtTrue;
        case cThaModuleMpeg:       return cAtTrue;
        case cThaModulePmcMpeg:    return cAtTrue;
        case cThaModulePmcMpig:    return cAtTrue;
        case cThaModuleCos:        return cAtTrue;

        default:
            break;
        }

    /* Public modules */
    switch (_moduleId)
        {
        case cAtModulePdh:         return cAtTrue;
        case cAtModulePpp:         return cAtTrue;
        case cAtModuleEncap:       return cAtTrue;
        case cAtModuleEth:         return cAtTrue;
        case cAtModuleRam:         return cAtTrue;
        case cAtModuleClock:       return cAtTrue;
        case cAtModulePktAnalyzer: return cAtTrue;

        default:
            break;
        }

    return m_AtDeviceMethods->ModuleIsSupported(self, moduleId);
    }

static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    eThaPhyModule phyModule = moduleId;

    if (moduleId  == cAtModulePpp)         return (AtModule)ThaPdhMlpppModulePppNew(self);
    if (moduleId  == cAtModuleClock)       return (AtModule)ThaPdhMlpppModuleClockNew(self);
    if (moduleId  == cAtModuleEncap)       return (AtModule)ThaModuleEncapNew(self);
    if (moduleId  == cAtModuleEth)         return (AtModule)ThaModuleEthPppNew(self);
    if (moduleId  == cAtModulePktAnalyzer) return (AtModule)ThaModulePktAnalyzerPppNew(self);

    if (phyModule == cThaModuleCos)     return ThaModuleCosPppNew(self);
    if (phyModule == cThaModuleCla)     return ThaModuleClaPppNew(self);
    if (phyModule == cThaModuleMpig)    return ThaModuleMpigNew(self);
    if (phyModule == cThaModuleMpeg)    return ThaModuleMpegNew(self);
    if (phyModule == cThaModulePmcMpeg) return ThaModulePmcMpegNew(self);
    if (phyModule == cThaModulePmcMpig) return ThaModulePmcMpigNew(self);

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static const eAtModule *AllSupportedModulesGet(AtDevice self, uint8 *numModules)
    {
    static const eAtModule supportedModules[] = {cAtModulePdh,
                                                 cAtModulePpp,
                                                 cAtModuleEncap,
                                                 cAtModuleEth,
                                                 cAtModuleRam,
                                                 cAtModuleClock,
                                                 cAtModulePktAnalyzer};
    AtUnused(self);
    if (numModules)
        *numModules = mCount(supportedModules);

    return supportedModules;
    }

static ThaIntrController IntrControllerCreate(ThaDevice self, AtIpCore core)
    {
    AtModule modulePpp = AtDeviceModuleGet((AtDevice)self, cAtModulePpp);

    if (ThaModulePppOamModeIsDirect((AtModulePpp)modulePpp))
        return ThaDirectPppOamIntrControllerNew(core);

    return ThaPdhMlpppIntrControllerNew(core);
    }

static AtEncapBinder EncapBinder(AtDevice self)
    {
    return ThaEncapDefaultBinder(self);
    }

static void OverrideAtDevice(ThaPdhMlpppDevice self)
    {
    AtDevice device = (AtDevice)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(device);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(tAtDeviceMethods));
        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, AllSupportedModulesGet);
        mMethodOverride(m_AtDeviceOverride, ModuleIsSupported);
        mMethodOverride(m_AtDeviceOverride, EncapBinder);
        }

    mMethodsSet(device, &m_AtDeviceOverride);
    }

static void OverrideThaDevice(ThaPdhMlpppDevice self)
    {
    ThaDevice device = (ThaDevice)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDeviceOverride, mMethodsGet(device), sizeof(m_ThaDeviceOverride));

        mMethodOverride(m_ThaDeviceOverride, IntrControllerCreate);
        }

    mMethodsSet(device, &m_ThaDeviceOverride);
    }

static void Override(ThaPdhMlpppDevice self)
    {
    OverrideAtDevice(self);
    OverrideThaDevice(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPdhMlpppDevice);
    }

AtDevice ThaPdhMlpppDeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Initialize its super */
    if (ThaDeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override((ThaPdhMlpppDevice)self);
    m_methodsInit = 1;

    return self;
    }

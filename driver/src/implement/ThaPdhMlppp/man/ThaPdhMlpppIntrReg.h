/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device
 * 
 * File        : ThaPdhMlpppIntrReg.h
 * 
 * Created Date: Dec 26, 2012
 *
 * Description : Interrupt registers
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAE1INTRREG_H_
#define _THAE1INTRREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Global interrupt status */
#define cThaPdhMlpppRegChipIntrStatus  0xE

/* Global interrupt enable */
#define cThaPdhMlpppRegChipIntrCtrl    0xF
#define cThaPdhMlpppMpigIntrStatMask   cBit8

/* OAM interrupt control */
#define cThaPdhMlpppMpigOamReceiveIntrCtrl 0x844004
#define cThaPdhMlpppMpigOamReceiveIntrEnMask cBit10
#define cThaPdhMlpppMpigOamReceiveIntrEnShift 10

#define cThaPdhMlpppMpigOamFlushMask cBit8
#define cThaPdhMlpppMpigOamFlushShift 8

#define cThaPdhMlpppMpigDirectOamMask cBit2
#define cThaPdhMlpppMpigDirectOamShift 2

/* OAM receive sticky */
#define cThaPdhMlpppMpigOamReceiveSticky 0x844005

#ifdef __cplusplus
}
#endif
#endif /* _THAE1INTRREG_H_ */


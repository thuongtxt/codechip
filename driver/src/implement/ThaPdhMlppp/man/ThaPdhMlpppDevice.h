/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : ThaPdhMlpppDevice.h
 * 
 * Created Date: May 6, 2013
 *
 * Description : PDH MLPPP device
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPDHMLPPPDEVICE_H_
#define _THAPDHMLPPPDEVICE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../default/man/ThaDevice.h" /* Super class */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPdhMlpppDevice * ThaPdhMlpppDevice;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Concrete devices */
AtDevice ThaPdhMlpppDeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60070013DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60030022DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60030023DeviceNew(AtDriver driver, uint32 productCode);

#ifdef __cplusplus
}
#endif
#endif /* _THAPDHMLPPPDEVICE_H_ */


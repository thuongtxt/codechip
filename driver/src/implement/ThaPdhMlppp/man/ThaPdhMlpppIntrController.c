/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : ThaPdhMlpppIntrController.c
 *
 * Created Date: Aug 15, 2013
 *
 * Description : Interrupt controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../default/man/intrcontroller/ThaIntrControllerInternal.h"
#include "ThaPdhMlpppIntrReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaPdhMlpppIntrController* ThaPdhMlpppIntrController;

typedef struct tThaPdhMlpppIntrController
    {
    tThaIntrController super;
    }tThaPdhMlpppIntrController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tThaIpCoreMethods m_ThaIpCoreOverride;

/* Reference to super implementation (option) */
static const tThaIpCoreMethods *m_ThaIpCoreImplement;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 InterruptDisable(ThaIpCore self)
    {
    uint32 interruptEnable = 0;
    AtHal hal = AtIpCoreHalGet(mIpCore(self));

    if (hal == NULL)
        return 0;

    /* Get global interrupt enable */
    interruptEnable = AtHalNoLockRead(hal, cThaPdhMlpppRegChipIntrCtrl);

    /* Disable */
    AtHalNoLockWrite(hal, cThaPdhMlpppRegChipIntrCtrl, 0);

    return interruptEnable;
    }

static uint32 InterruptStatusGet(ThaIpCore self)
    {
    /* Get global interrupt status */
    AtHal hal = AtIpCoreHalGet(mIpCore(self));
    if (hal == NULL)
        return 0;

    return (AtHalNoLockRead(hal, cThaPdhMlpppRegChipIntrStatus));
    }

static void AllModulesInterruptEnable(ThaIpCore self, uint32 intrEnMask)
    {
    AtHal hal = AtIpCoreHalGet(mIpCore(self));
    if (hal == NULL)
        return;

    AtHalNoLockWrite(hal, cThaPdhMlpppRegChipIntrCtrl, intrEnMask);
    }

static eBool PppCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
	AtUnused(self);
    return ((intrStatus & cThaPdhMlpppMpigIntrStatMask) != 0) ? cAtTrue : cAtFalse;
    }

static void OamFlush(AtIpCore ipCore)
    {
    uint32 regVal;
    AtHal hal = AtIpCoreHalGet(ipCore);

    /* Flush */
    regVal = AtHalRead(hal, cThaPdhMlpppMpigOamReceiveIntrCtrl);
    mFieldIns(&regVal, cThaPdhMlpppMpigOamFlushMask, cThaPdhMlpppMpigOamFlushShift, 1);
    AtHalWrite(hal, cThaPdhMlpppMpigOamReceiveIntrCtrl, regVal);

    /* Disable flush */
    regVal = AtHalRead(hal, cThaPdhMlpppMpigOamReceiveIntrCtrl);
    mFieldIns(&regVal, cThaPdhMlpppMpigOamFlushMask, cThaPdhMlpppMpigOamFlushShift, 0);
    AtHalWrite(hal, cThaPdhMlpppMpigOamReceiveIntrCtrl, regVal);
    }

static void PppHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    uint32 interruptEnReg;
    AtHal hal = AtIpCoreHalGet(mIpCore(self));

    interruptEnReg = AtHalNoLockRead(hal, cThaPdhMlpppRegChipIntrCtrl);

    if (enable)
        interruptEnReg |= cThaPdhMlpppMpigIntrStatMask;
    else
        interruptEnReg &= ~cThaPdhMlpppMpigIntrStatMask;

    AtHalNoLockWrite(hal, cThaPdhMlpppRegChipIntrCtrl, interruptEnReg);

    /* Flush all OAMs first */
    if (enable)
        OamFlush(mIpCore(self));

    /* Enable OAM interrupt */
    interruptEnReg = AtHalNoLockRead(hal, cThaPdhMlpppMpigOamReceiveIntrCtrl);
    mFieldIns(&interruptEnReg, cThaPdhMlpppMpigOamReceiveIntrEnMask, cThaPdhMlpppMpigOamReceiveIntrEnShift, mBoolToBin(enable));
    AtHalNoLockWrite(hal, cThaPdhMlpppMpigOamReceiveIntrCtrl, interruptEnReg);
    }

static void OverrideThaIpCore(ThaPdhMlpppIntrController self)
    {
    ThaIpCore thaIp = (ThaIpCore)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaIpCoreImplement = mMethodsGet(thaIp);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaIpCoreOverride, m_ThaIpCoreImplement, sizeof(m_ThaIpCoreOverride));
        mMethodOverride(m_ThaIpCoreOverride, PppCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, PppHwInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, InterruptStatusGet);
        mMethodOverride(m_ThaIpCoreOverride, InterruptDisable);
        mMethodOverride(m_ThaIpCoreOverride, AllModulesInterruptEnable);
        }

    mMethodsSet(thaIp, &m_ThaIpCoreOverride);
    }

static void Override(ThaPdhMlpppIntrController self)
    {
    OverrideThaIpCore(self);
    }

/* Constructor of ThaPdhMlpppIntrController object */
static ThaIntrController ObjectInit(ThaIntrController self, AtIpCore ipCore)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaPdhMlpppIntrController));

    /* Super constructor */
    if (ThaIntrControllerObjectInit(self, ipCore) == NULL)
        return NULL;

    /* Override */
    Override((ThaPdhMlpppIntrController)self);

    m_methodsInit = 1;

    return self;
    }

/* Create controller object */
ThaIntrController ThaPdhMlpppIntrControllerNew(AtIpCore ipCore)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaIntrController newController = mMethodsGet(osal)->MemAlloc(osal, sizeof(tThaPdhMlpppIntrController));
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, ipCore);
    }

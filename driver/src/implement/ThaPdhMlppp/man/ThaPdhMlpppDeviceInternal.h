/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : ThaPdhMlpppDeviceInternal.h
 * 
 * Created Date: May 6, 2013
 *
 * Description : PDH MLPPP device descriptor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPDHMLPPPDEVICEINTERNAL_H_
#define _THAPDHMLPPPDEVICEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModulePpp.h"

#include "../../../generic/man/AtDeviceInternal.h"
#include "../../default/man/ThaDeviceInternal.h"
#include "../../default/ram/ThaModuleRam.h"
#include "../../default/eth/ThaModuleEth.h"

#include "ThaPdhMlpppDevice.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPdhMlpppDevice
    {
    tThaDevice super;

    /* Additional methods and attributes ... */
    }tThaPdhMlpppDevice;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDevice ThaPdhMlpppDeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode);

#ifdef __cplusplus
}
#endif
#endif /* _THAPDHMLPPPDEVICEINTERNAL_H_ */


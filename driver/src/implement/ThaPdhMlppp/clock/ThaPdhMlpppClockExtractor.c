/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Clock
 *
 * File        : ThaPdhMlpppClockExtractor.c
 *
 * Created Date: Sep 19, 2013
 *
 * Description : Clock extractor for PDH PPP/MLPPP product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../default/clock/ThaClockExtractorInternal.h"
#include "../../default/pdh/ThaModulePdhReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaPdhMlpppClockExtractor
    {
    tThaClockExtractor super;
    }tThaPdhMlpppClockExtractor;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtClockExtractorMethods  m_AtClockExtractorOverride;
static tThaClockExtractorMethods m_ThaClockExtractorOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool CanExtractSystemClock(ThaClockExtractor self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static eBool CanExtractPdhDe1Clock(ThaClockExtractor self, AtPdhDe1 de1)
    {
	AtUnused(self);
    return de1 ? cAtTrue : cAtFalse;
    }

static eAtModuleClockRet HwPdhDe1ClockExtract(ThaClockExtractor self, AtPdhDe1 de1)
    {
    uint32 address, regValue;
    uint8 localRefOutId;

    localRefOutId = ThaClockExtractorLocalId(self);
    address  = cThaRegPdhOutRefClk8k + ThaClockExtractorPartOffset(self);
    regValue = AtClockExtractorRead((AtClockExtractor)self, address);

    mFieldIns(&regValue,
              cThaRegPdhOutRefClkMask(localRefOutId),
              cThaRegPdhOutRefClkShift(localRefOutId),
              AtChannelIdGet((AtChannel)de1));

    /* Apply */
    AtClockExtractorWrite((AtClockExtractor)self, address, regValue);

    return cAtOk;
    }

static eAtModuleClockRet Enable(AtClockExtractor self, eBool enable)
    {
    uint32 address, regValue;
    uint8 localRefOutId;

    localRefOutId = ThaClockExtractorLocalId((ThaClockExtractor)self);
    address  = cThaRegPdhOutRefClk8k + ThaClockExtractorPartOffset((ThaClockExtractor)self);
    regValue = AtClockExtractorRead(self, address);

    mFieldIns(&regValue,
              cThaRegPdhOutRefClkEnableMask(localRefOutId),
              cThaRegPdhOutRefClkEnableShift(localRefOutId),
              enable);

    /* Apply */
    AtClockExtractorWrite(self, address, regValue);

    return cAtOk;
    }

static eBool IsEnabled(AtClockExtractor self)
    {
    uint32 address, regValue;
    uint8 localRefOutId;

    localRefOutId = ThaClockExtractorLocalId((ThaClockExtractor)self);
    address  = cThaRegPdhOutRefClk8k + ThaClockExtractorPartOffset((ThaClockExtractor)self);
    regValue = AtClockExtractorRead(self, address);

    return (regValue & cThaRegPdhOutRefClkEnableMask(localRefOutId)) ? cAtTrue : cAtFalse;
    }

static void OverrideThaClockExtractor(AtClockExtractor self)
    {
    ThaClockExtractor extractor = (ThaClockExtractor)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClockExtractorOverride, mMethodsGet(extractor), sizeof(m_ThaClockExtractorOverride));

        mMethodOverride(m_ThaClockExtractorOverride, CanExtractSystemClock);
        mMethodOverride(m_ThaClockExtractorOverride, CanExtractPdhDe1Clock);
        mMethodOverride(m_ThaClockExtractorOverride, HwPdhDe1ClockExtract);
        }

    mMethodsSet(extractor, &m_ThaClockExtractorOverride);
    }

static void OverrideAtClockExtractor(AtClockExtractor self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtClockExtractorOverride, mMethodsGet(self), sizeof(m_AtClockExtractorOverride));

        mMethodOverride(m_AtClockExtractorOverride, Enable);
        mMethodOverride(m_AtClockExtractorOverride, IsEnabled);
        }

    mMethodsSet(self, &m_AtClockExtractorOverride);
    }

static void Override(AtClockExtractor self)
    {
    OverrideAtClockExtractor(self);
    OverrideThaClockExtractor(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPdhMlpppClockExtractor);
    }

static AtClockExtractor ObjectInit(AtClockExtractor self, AtModuleClock clockModule, uint8 extractorId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaClockExtractorObjectInit(self, clockModule, extractorId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtClockExtractor ThaPdhMlpppClockExtractorNew(AtModuleClock clockModule, uint8 extractorId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtClockExtractor newExtractor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newExtractor == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newExtractor, clockModule, extractorId);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PPP
 * 
 * File        : ThaPdhMlpppModulePpp.h
 * 
 * Created Date: May 7, 2013
 *
 * Description : PPP module of PDH MLPPP product
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPDHMLPPPMODULEPPP_H_
#define _THAPDHMLPPPMODULEPPP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../default/ppp/ThaModulePpp.h" /* Super class */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPdhMlpppModulePpp * ThaPdhMlpppModulePpp;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THAPDHMLPPPMODULEPPP_H_ */


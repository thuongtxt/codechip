/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PPP
 * 
 * File        : ThaPdhMlpppModulePppInternal.h
 * 
 * Created Date: May 7, 2013
 *
 * Description : PPP module of PDH MLPPP product
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPDHMLPPPMODULEPPPINTERNAL_H_
#define _THAPDHMLPPPMODULEPPPINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaPdhMlpppModulePpp.h"
#include "../man/ThaPdhMlpppIntrReg.h"
#include "../../default/ppp/ThaModulePppInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPdhMlpppModulePpp
    {
    tThaModulePpp super;

    /* Private data */
    }tThaPdhMlpppModulePpp;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePpp ThaPdhMlpppModulePppObjectInit(AtModulePpp self, AtDevice device);
AtModulePpp ThaPdhMlpppModulePppNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAPDHMLPPPMODULEPPPINTERNAL_H_ */


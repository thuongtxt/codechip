/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : ThaPdhMlpppModulePpp.c
 *
 * Created Date: Dec 26, 2012
 *
 * Description : PPP module of PDH MLPPP product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaPdhMlpppModulePppInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaModulePppMethods m_ThaModulePppOverride;
static tAtModuleMethods m_AtModuleOverride;

static const tAtModuleMethods *m_AtModuleMethods = NULL;
static const tThaModulePppMethods *m_ThaModulePppMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void OamReceiveStickyClear(ThaModulePpp self, AtIpCore ipCore)
    {
    AtHal hal;

    if (ThaModulePppOamModeIsDirect((AtModulePpp)self))
    	{
        m_ThaModulePppMethods->OamReceiveStickyClear(self, ipCore);
        return;
    	}

    hal = AtIpCoreHalGet(ipCore);
    AtHalNoLockWrite(hal, cThaPdhMlpppMpigOamReceiveSticky, 0xF);
    }

static void OamDirectModeEnable(ThaModulePpp self, eBool enable)
    {
    uint32 regVal = mModuleHwRead(self, cThaPdhMlpppMpigOamReceiveIntrCtrl);
    mFieldIns(&regVal, cThaPdhMlpppMpigDirectOamMask, cThaPdhMlpppMpigDirectOamShift, mBoolToBin(enable));
    mModuleHwWrite(self, cThaPdhMlpppMpigOamReceiveIntrCtrl, regVal);

    self->oamDirectMode = enable;
    }

static eAtRet Setup(AtModule self)
    {
    eAtRet ret;
    ThaModulePpp pppModule = (ThaModulePpp)self;

    /* Super setup */
    ret = m_AtModuleMethods->Setup(self);
    if (ret != cAtOk)
        return ret;

    /* Enable OAM direct mode */
    OamDirectModeEnable(pppModule, mMethodsGet(pppModule)->DirectOam(pppModule));
    return cAtOk;
    }

static void OverrideAtModule(ThaPdhMlpppModulePpp self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(module), sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, Setup);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideThaModulePpp(ThaPdhMlpppModulePpp self)
    {
    ThaModulePpp pppModule = (ThaModulePpp)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePppMethods = mMethodsGet(pppModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePppOverride, m_ThaModulePppMethods, sizeof(m_ThaModulePppOverride));
        mMethodOverride(m_ThaModulePppOverride, OamReceiveStickyClear);
        }

    mMethodsSet(pppModule, &m_ThaModulePppOverride);
    }

static void Override(ThaPdhMlpppModulePpp self)
    {
    OverrideThaModulePpp(self);
    OverrideAtModule(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPdhMlpppModulePpp);
    }

AtModulePpp ThaPdhMlpppModulePppObjectInit(AtModulePpp self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModulePppObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override((ThaPdhMlpppModulePpp)self);
    m_methodsInit = 1;

    return self;
    }

AtModulePpp ThaPdhMlpppModulePppNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePpp newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ThaPdhMlpppModulePppObjectInit(newModule, device);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Frame-relay
 * 
 * File        : ThaFrVirtualCircuitInternal.h
 * 
 * Created Date: Jul 22, 2016
 *
 * Description : FR virtual circuitn internal definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAFRVIRTUALCIRCUITINTERNAL_H_
#define _THAFRVIRTUALCIRCUITINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/framerelay/AtFrVirtualCircuitInternal.h"
#include "ThaFrVirtualCircuit.h"
#include "hash/ThaFrHash.h"
#include "AtEthFlow.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaFrVirtualCircuit
    {
    tAtFrVirtualCircuit super;

    /* Private data */
    ThaFrHashEntry entry;
    }tThaFrVirtualCircuit;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtFrVirtualCircuit ThaFrVirtualCircuitObjectInit(AtFrVirtualCircuit self, AtChannel channel, uint32 dlci);

#ifdef __cplusplus
}
#endif
#endif /* _THAFRVIRTUALCIRCUITINTERNAL_H_ */


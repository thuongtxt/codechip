/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited 
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : ThaMfrBundle.c
 *
 * Created Date: Jul 30, 2016 
 *
 * Description : MFR Bundle implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../generic/framerelay/AtMfrBundleInternal.h"
#include "../../../generic/encap/AtHdlcLinkInternal.h"
#include "../ppp/ThaMpBundle.h"
#include "AtFrVirtualCircuit.h"
#include "ThaMfrBundleInternal.h"
#include "ThaMfrBundle.h"
#include "ThaMfrBundleVirtualCircuit.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(_self) ((ThaMfrBundle)_self)

/*--------------------------- Local Typedefs ---------------------------------*/
typedef struct tThaMfrBundle * ThaMfrBundle;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtHdlcBundleMethods     m_AtHdlcBundleOverride;
static tAtMfrBundleMethods      m_AtMfrBundleOverride;

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtFrVirtualCircuit VirtualCircuitObjectCreate(AtMfrBundle self, uint32 dlci)
    {
    return ThaMfrBundleVirtualCircuitNew(dlci, (AtChannel)self);
    }

static uint8 MaxNumLinkInBundleGet(AtHdlcBundle self)
    {
    AtUnused(self);
    return 32;
    }

static AtEthFlow OamFlowCreate(AtHdlcBundle self)
    {
    /* Just for bypass autotest, it will review and correct when updated FR path */
    return AtHdlcBundleOamFlowCreate(self);
    }

static void OamFlowDelete(AtHdlcBundle self)
    {
    /* Just for bypass autotest, it will review and correct when updated FR path */
    AtHdlcBundleOamFlowDelete(self);
    }

static eAtRet FragmentFormatSet(AtMfrBundle self, eAtMfrBundleFragmentFormat format)
    {
    /* Just for bypass autotest, it will review and correct when updated FR path */
    mThis(self)->cache.format = format;
    return cAtOk;
    }

static eAtMfrBundleFragmentFormat FragmentFormatGet(AtMfrBundle self)
    {
    return mThis(self)->cache.format;
    }

static eAtRet SchedulingModeSet(AtHdlcBundle self, eAtHdlcBundleSchedulingMode schedulingMode)
    {
    /* Just for bypass autotest, it will review and correct when updated FR path */
    mThis(self)->cache.mode = schedulingMode;
    return cAtOk;
    }

static eAtHdlcBundleSchedulingMode SchedulingModeGet(AtHdlcBundle self)
    {
    return mThis(self)->cache.mode;
    }

static void OverrideAtHdlcBundle(ThaMfrBundle self)
    {
    AtHdlcBundle bundle = (AtHdlcBundle) self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtHdlcBundleOverride, mMethodsGet(bundle), sizeof(m_AtHdlcBundleOverride));

        mMethodOverride(m_AtHdlcBundleOverride, MaxNumLinkInBundleGet);
        mMethodOverride(m_AtHdlcBundleOverride, OamFlowCreate);
        mMethodOverride(m_AtHdlcBundleOverride, OamFlowDelete);
        mMethodOverride(m_AtHdlcBundleOverride, SchedulingModeSet);
        mMethodOverride(m_AtHdlcBundleOverride, SchedulingModeGet);
        }

    mMethodsSet(bundle, &m_AtHdlcBundleOverride);
    }

static void OverrideAtMfrBundle(ThaMfrBundle self)
    {
    AtMfrBundle bundle = (AtMfrBundle)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtMfrBundleOverride, mMethodsGet(bundle), sizeof(m_AtMfrBundleOverride));

        mMethodOverride(m_AtMfrBundleOverride, VirtualCircuitObjectCreate);
        mMethodOverride(m_AtMfrBundleOverride, FragmentFormatGet);
        mMethodOverride(m_AtMfrBundleOverride, FragmentFormatSet);
        }

    mMethodsSet(bundle, &m_AtMfrBundleOverride);
    }

static void Override(ThaMfrBundle self)
    {
    OverrideAtHdlcBundle(self);
    OverrideAtMfrBundle(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaMfrBundle);
    }

static AtMfrBundle ObjectInit(AtMfrBundle self, uint32 channelId, AtModuleEncap module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtMfrBundleObjectInit((AtMfrBundle)self, channelId, module) == NULL)
        return NULL;

    /* Override */
    Override((ThaMfrBundle)self);

    m_methodsInit = 1;

    return self;
    }

AtMfrBundle ThaMfrBundleNew(uint32 channelId, AtModuleEncap module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtMfrBundle newBundle = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newBundle == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newBundle, channelId, module);
    }

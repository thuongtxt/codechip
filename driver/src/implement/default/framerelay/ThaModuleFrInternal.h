/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Frame-relay
 * 
 * File        : ThaModuleFrInternal.h
 * 
 * Created Date: Jul 24, 2016
 *
 * Description : Module frame relay internal definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEFRINTERNAL_H_
#define _THAMODULEFRINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/framerelay/AtModuleFrInternal.h"
#include "hash/ThaFrHash.h"
#include "ThaModuleFr.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleFrMethods
    {
    ThaFrHash (*HashObjectCreate)(ThaModuleFr self);
    ThaFrHash (*HashGet)(ThaModuleFr self);
    uint32    (*HashBaseAddressGet)(ThaModuleFr self);
    }tThaModuleFrMethods;

typedef struct tThaModuleFr
    {
    tAtModuleFr super;
    const tThaModuleFrMethods *methods;

    /* Private attribute */
    ThaFrHash frHash;
    } tThaModuleFr;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEFRINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : ThaMfrBundleVirtualCircuit.h
 *
 * Created Date: Aug 1, 2016 
 *
 * Description : Thalassa bundle Virtual Circuit Interface
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMFRBUNDLEVIRTUALCIRCUIT_H_
#define _THAMFRBUNDLEVIRTUALCIRCUIT_H_

/*--------------------------- Include files ----------------------------------*/


#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtFrVirtualCircuit ThaMfrBundleVirtualCircuitNew(uint32 dlci, AtChannel channel);

#ifdef __cplusplus
}
#endif

#endif /* _THAMFRBUNDLEVIRTUALCIRCUIT_H_ */

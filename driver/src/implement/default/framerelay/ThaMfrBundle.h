/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : ThaMfrBundle.h
 *
 * Created Date: Jul 30, 2016 
 *
 * Description : MFR bundle interface
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMFRBUNDLE_H_
#define _THAMFRBUNDLE_H_

/*--------------------------- Include files ----------------------------------*/


#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtMfrBundle ThaMfrBundleNew(uint32 channelId, AtModuleEncap module);

#ifdef __cplusplus

}
#endif

#endif /* _THAMFRBUNDLE_H_ */

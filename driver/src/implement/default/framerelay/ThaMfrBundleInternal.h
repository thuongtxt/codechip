/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : ThaMfrBundleInternal.h
 *
 * Created Date: Jul 30, 2016 
 *
 * Description : MFR internal definition
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMFRBUNDLEINTERNAL_H_
#define _THAMFRBUNDLEINTERNAL_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../../generic/framerelay/AtMfrBundleInternal.h"

#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* For Simulate phase - remove when HW release */
typedef struct tThaMfrBundleCache
    {
    eAtMfrBundleFragmentFormat format;
    eAtHdlcBundleSchedulingMode mode;
    }tThaMfrBundleCache;

typedef struct tThaMfrBundle
    {
    tAtMfrBundle super;

    /* Private data */
    tThaMfrBundleCache cache;
    }tThaMfrBundle;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif

#endif /* _THAMFRBUNDLEINTERNAL_H_ */

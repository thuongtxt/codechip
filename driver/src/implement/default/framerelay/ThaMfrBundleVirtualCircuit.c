/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited 
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : ThaMfrBundleVirtualCircuit.c
 *
 * Created Date: Aug 1, 2016 
 *
 * Description : MFR Virtual Circuit implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../generic/framerelay/AtFrVirtualCircuitInternal.h"
#include "ThaMfrBundleVirtualCircuit.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/
typedef struct tThaMfrBundleVirtualCircuit
    {
    tAtFrVirtualCircuit super;
    }tThaMfrBundleVirtualCircuit;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods           m_AtObjectOverride;
static tAtChannelMethods          m_AtChannelOverride;
static tAtFrVirtualCircuitMethods m_AtFrVirtualCircuitOverride;

/* Save Super Implementation */
static const tAtObjectMethods           *m_AtObjectMethods   = NULL;
static const tAtChannelMethods          *m_AtChannelMethods = NULL;

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Delete(AtObject self)
    {
    AtChannelBoundPwObjectDelete((AtChannel)self);
    m_AtObjectMethods->Delete(self);
    }

static eAtRet TxTrafficEnable(AtChannel self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtOk;
    }

static eBool TxTrafficIsEnabled(AtChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet RxTrafficEnable(AtChannel self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtOk;
    }

static eBool RxTrafficIsEnabled(AtChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static AtMfrBundle FrBundleGet(AtFrVirtualCircuit self)
    {
    return (AtMfrBundle)self->channel;
    }

static uint32 EncapHwIdAllocate(AtChannel self)
    {
    return AtChannelHwIdGet(self);
    }

static void OverrideAtObject(AtFrVirtualCircuit self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtFrVirtualCircuit self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, TxTrafficEnable);
        mMethodOverride(m_AtChannelOverride, RxTrafficEnable);
        mMethodOverride(m_AtChannelOverride, TxTrafficIsEnabled);
        mMethodOverride(m_AtChannelOverride, RxTrafficIsEnabled);
        mMethodOverride(m_AtChannelOverride, EncapHwIdAllocate);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtFrVirtualCircuit(AtFrVirtualCircuit self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtFrVirtualCircuitOverride, mMethodsGet(self), sizeof(tAtFrVirtualCircuitMethods));

        mMethodOverride(m_AtFrVirtualCircuitOverride, FrBundleGet);
        }

    mMethodsSet(self, &m_AtFrVirtualCircuitOverride);
    }

static void Override(AtFrVirtualCircuit self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtFrVirtualCircuit(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaMfrBundleVirtualCircuit);
    }

static AtFrVirtualCircuit ObjectInit(AtFrVirtualCircuit self, AtChannel channel, uint32 dlci)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Call super constructor */
    if (AtFrVirtualCircuitObjectInit(self, channel, dlci) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtFrVirtualCircuit ThaMfrBundleVirtualCircuitNew(uint32 dlci, AtChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtFrVirtualCircuit newLink = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newLink == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newLink, channel, dlci);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Frame-relay
 * 
 * File        : ThaModuleFr.h
 * 
 * Created Date: Jul 24, 2016
 *
 * Description : Module FR interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEFR_H_
#define _THAMODULEFR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "hash/ThaFrHash.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleFr * ThaModuleFr;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleFr ThaModuleFrNew(AtDevice device);
ThaFrHash ThaModuleFrHashGet(ThaModuleFr self);
uint32 ThaModuleFrHashBaseAddressGet(ThaModuleFr self);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEFR_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Frame-relay
 * 
 * File        : ThaFrVirtualCircuit.h
 * 
 * Created Date: Jul 22, 2016
 *
 * Description : FR virtual circuit interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAFRVIRTUALCIRCUIT_H_
#define _THAFRVIRTUALCIRCUIT_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtFrLink.h"
#include "hash/ThaFrHash.h"
/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaFrVirtualCircuit * ThaFrVirtualCircuit;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtFrVirtualCircuit ThaFrVirtualCircuitNew(uint32 dlci, AtChannel frLink);

uint32* ThaFrVirtualCircuitHashKeyBuild(AtFrVirtualCircuit self, uint32* keyVal, uint32 numberOfDwords);
eAtModuleFrRet ThaFrVirtualCircuitFlowLookupDeactivate(AtFrVirtualCircuit self, ThaFrHashEntry hashEntry);
eAtModuleFrRet ThaFrVirtualCircuitFlowLookupActivate(AtFrVirtualCircuit self, AtEthFlow flow, ThaFrHashEntry hashEntry);

#ifdef __cplusplus
}
#endif
#endif /* _THAFRVIRTUALCIRCUIT_H_ */


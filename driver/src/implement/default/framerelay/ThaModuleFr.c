/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : FR
 *
 * File        : ThaModuleFr.c
 *
 * Created Date: Jul 24, 2016
 *
 * Description : Module FR code
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaModuleFrInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self)  ((ThaModuleFr)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tThaModuleFrMethods m_methods;

/* Override */
static tAtObjectMethods  m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaFrHash HashObjectCreate(ThaModuleFr self)
    {
    ThaFrHash frHash = ThaFrHashNew();
    AtUnused(self);

    if (frHash == NULL)
        return NULL;

    if (ThaFrHashSetup(frHash) != cAtOk)
        {
        AtObjectDelete((AtObject)frHash);
        return NULL;
        }

    return frHash;
    }

static ThaFrHash HashGet(ThaModuleFr self)
    {
    if (mThis(self)->frHash == NULL)
        mThis(self)->frHash = mMethodsGet(self)->HashObjectCreate(self);

    return mThis(self)->frHash;
    }

static uint32 HashBaseAddressGet(ThaModuleFr self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject)mThis(self)->frHash);
    mThis(self)->frHash = NULL;
    m_AtObjectMethods->Delete(self);
    }

static void OverrideAtObject(AtModuleFr self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtModuleFr self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(ThaModuleFr self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, HashObjectCreate);
        mMethodOverride(m_methods, HashGet);
        mMethodOverride(m_methods, HashBaseAddressGet);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaModuleFr);
    }

static AtModuleFr ObjectInit(AtModuleFr self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Call super constructor to reuse all of its implementation */
    if(!AtModuleFrObjectInit(self, device))
        return NULL;

    /* Initialize Methods */
    Override(self);
    MethodsInit((ThaModuleFr) self);
    m_methodsInit = 1;

    return self;
    }

AtModuleFr ThaModuleFrNew(AtDevice device)
    {
    /* Allocate memory */
    AtModuleFr newModule = AtOsalMemAlloc(ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

ThaFrHash ThaModuleFrHashGet(ThaModuleFr self)
    {
    if (self)
        return mMethodsGet(self)->HashGet(self);
    
    return NULL;
    }

uint32 ThaModuleFrHashBaseAddressGet(ThaModuleFr self)
    {
    if (self)
        return mMethodsGet(self)->HashBaseAddressGet(self);

    return cInvalidUint32;
    }



/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Frame-relay
 * 
 * File        : ThaFrHash.h
 * 
 * Created Date: Jul 18, 2016
 *
 * Description : FR hash interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAFRHASH_H_
#define _THAFRHASH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtFrVirtualCircuit.h"
#include "AtEthFlow.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*
 * For Hask Key
 * */
#define cKeyNumberOfDwords 2  /* 2 dwords */
#define cKeyNumberOfBits   33 /* use 33bits */

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaFrHash * ThaFrHash;
typedef struct tThaFrHashTable * ThaFrHashTable;
typedef struct tThaFrHashEntry * ThaFrHashEntry;

typedef uint32 (*HashFunction)(uint32* hashKey, uint8 numKeyBits);

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* ThaFrHash */
ThaFrHash ThaFrHashNew(void);
eAtRet ThaFrHashSetup(ThaFrHash self);
uint32 ThaFrHashMaxNumTables(ThaFrHash self);
ThaFrHashTable ThaFrHashTableGet(ThaFrHash self, uint32 tableIndex);
void ThaFrHashTablesNumInUsedEntriesIncreaseSort(ThaFrHash self);

/* ThaFrHashTable */
ThaFrHashTable ThaFrHashTableNew(ThaFrHash hash, uint32 tableId, HashFunction firstHashFunc, HashFunction secondHashFunc);
eAtRet ThaFrHashTableSetup(ThaFrHashTable self);
uint32 ThaFrHashTableMaxNumEntries(ThaFrHashTable self);
ThaFrHashEntry ThaFrHashTableFreeEntryGet(ThaFrHashTable self, uint32* hashKey, uint8 numBitsOfKey);
eAtRet ThaFrHashTableEntryUse(ThaFrHashTable self, ThaFrHashEntry entry);
eAtRet ThaFrHashTableEntryUnuse(ThaFrHashTable self, ThaFrHashEntry entry);
uint32 ThaFrHashTableRegisterOffset(ThaFrHashTable self);
ThaFrHashEntry ThaFrHashTableReArrange(ThaFrHashTable self, uint32* hashKey, uint8 numBitsOfKey);

/* ThaFrHashEntry */
ThaFrHashEntry ThaFrHashEntryNew(ThaFrHashTable table, uint32 index);
eAtRet ThaFrHashEntryUse(ThaFrHashEntry self, AtFrVirtualCircuit frVc, AtEthFlow flow);
eAtRet ThaFrHashEntryUnuse(ThaFrHashEntry self);
eBool ThaFrHashEntryIsInused(ThaFrHashEntry self);
uint32 ThaFrHashEntryIdGet(ThaFrHashEntry self);
ThaFrHashTable ThaFrHashEntryHashTableGet(ThaFrHashEntry self);
AtFrVirtualCircuit ThaFrHashEntryVirtualCircuitGet(ThaFrHashEntry self);
AtEthFlow ThaFrHashEntryEthFlowGet(ThaFrHashEntry self);

#ifdef __cplusplus
}
#endif
#endif /* _THAFRHASH_H_ */


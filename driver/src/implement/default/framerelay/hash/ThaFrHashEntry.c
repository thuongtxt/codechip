/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Frame-relay
 *
 * File        : ThaFrHashEntry.c
 *
 * Created Date: Jul 19, 2016
 *
 * Description : FR hash entry
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaFrHashInternal.h"
#include "../../eth/ThaEthFlowInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtObjectMethods  m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *ToString(AtObject self)
    {
    static char string[32];
    AtSprintf(string, "ThaFrHashEntry.%u", ThaFrHashEntryIdGet((ThaFrHashEntry) self) + 1);

    return string;
    }

static void OverrideAtObject(ThaFrHashEntry self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(ThaFrHashEntry self)
    {
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaFrHashEntry);
    }

static ThaFrHashEntry ThaFrHashEntryObjectInit(ThaFrHashEntry self, ThaFrHashTable table, uint32 entryIndex)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Call super constructor to reuse all of its implementation */
    AtObjectInit((AtObject)self);

    /* Initialize Methods */
    Override(self);

    /* Only initialize method structures one time */
    m_methodsInit = 1;
    self->table = table;
    self->id = entryIndex;

    return self;
    }

ThaFrHashEntry ThaFrHashEntryNew(ThaFrHashTable table, uint32 entryIndex)
    {
    /* Allocate memory */
    ThaFrHashEntry newEntry = AtOsalMemAlloc(ObjectSize());
    if (newEntry == NULL)
        return NULL;

    /* Construct it */
    return ThaFrHashEntryObjectInit(newEntry, table, entryIndex);
    }

eAtRet ThaFrHashEntryUse(ThaFrHashEntry self, AtFrVirtualCircuit frVc, AtEthFlow flow)
    {
    eAtRet ret;
    AtUnused(flow);
    if (self == NULL)
        return cAtErrorNotExist;

    ret = ThaFrHashTableEntryUse(self->table, self);
    if (ret != cAtOk)
        return ret;

    self->flow = flow;
    self->frVc = frVc;
    return cAtOk;
    }

eAtRet ThaFrHashEntryUnuse(ThaFrHashEntry self)
    {
    eAtRet ret;
    if (self == NULL)
        return cAtErrorNotExist;

    ret = ThaFrHashTableEntryUnuse(self->table, self);
    if (ret != cAtOk)
        return ret;

    self->flow = NULL;
    self->frVc = NULL;
    return cAtOk;
    }

eBool ThaFrHashEntryIsInused(ThaFrHashEntry self)
    {
    if (self == NULL)
        return cAtFalse;

    return self->frVc ? cAtTrue : cAtFalse;
    }

uint32 ThaFrHashEntryIdGet(ThaFrHashEntry self)
    {
    if (self)
        return self->id;

    return cInvalidUint32;
    }

ThaFrHashTable ThaFrHashEntryHashTableGet(ThaFrHashEntry self)
    {
    if (self)
        return self->table;

    return NULL;
    }

AtFrVirtualCircuit ThaFrHashEntryVirtualCircuitGet(ThaFrHashEntry self)
    {
    if (self == NULL)
        return NULL;

    return self->frVc;
    }

AtEthFlow ThaFrHashEntryEthFlowGet(ThaFrHashEntry self)
    {
    if (self == NULL)
        return NULL;

    return self->flow;
    }

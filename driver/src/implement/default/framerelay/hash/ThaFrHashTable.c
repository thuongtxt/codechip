/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Frame-relay
 *
 * File        : ThaFrHashTable.c
 *
 * Created Date: Jul 18, 2016
 *
 * Description : FR hash table
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaFrHashInternal.h"
#include "ThaFrHash.h"
#include "../ThaFrVirtualCircuit.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tThaFrHashTableMethods m_methods;

/* Override */
static tAtObjectMethods  m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxNumEntries(ThaFrHashTable self)
    {
    AtUnused(self);
    return 4096;
    }

static eBool HashIdIsValid(ThaFrHashTable self, uint32 hashId)
    {
    if (hashId >= mMethodsGet(self)->MaxNumEntries(self))
        return cAtFalse;

    return cAtTrue;
    }

static ThaFrHashEntry* AllEntriedGet(ThaFrHashTable self)
    {
    return self->entries;
    }

static ThaFrHashEntry HashEntryGet(ThaFrHashTable self, uint32 hashId)
    {
    ThaFrHashEntry* entries;
    if (!HashIdIsValid(self, hashId))
        return NULL;

    entries = AllEntriedGet(self);
    if (entries == NULL)
        return NULL;

    return entries[hashId];
    }

static void AllEntriesDelete(ThaFrHashTable self)
    {
    uint32 idx, numEntries;
    if (self->entries == NULL)
        return;

    numEntries = ThaFrHashTableMaxNumEntries(self);
    for (idx = 0; idx < numEntries; idx++)
        {
        AtObjectDelete((AtObject)self->entries[idx]);
        self->entries[idx] = NULL;
        }

    AtOsalMemFree(self->entries);
    self->entries = NULL;
    }

static ThaFrHashEntry *AllEntriesAllocate(ThaFrHashTable self)
    {
    uint32 idx, numEntries = ThaFrHashTableMaxNumEntries(self);
    uint32 memSize = sizeof(ThaFrHashEntry) * numEntries;

    self->entries = AtOsalMemAlloc(memSize);
    if (self->entries == NULL)
        return NULL;

    AtOsalMemInit(self->entries, 0, memSize);
    for (idx = 0; idx < numEntries; idx++)
        {
        self->entries[idx] = ThaFrHashEntryNew(self, idx);
        if (self->entries[idx] == NULL)
            {
            AllEntriesDelete(self);
            return NULL;
            }
        }

    return self->entries;
    }

static void Delete(AtObject self)
    {
    AllEntriesDelete((ThaFrHashTable)self);
    m_AtObjectMethods->Delete(self);
    }

static void OverrideAtObject(ThaFrHashTable self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(ThaFrHashTable self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(ThaFrHashTable self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, MaxNumEntries);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaFrHashTable);
    }

static ThaFrHashTable ThaFrHashTableObjectInit(ThaFrHashTable self, ThaFrHash hash, uint32 tableId,
                                               HashFunction firstHashFunction, HashFunction secondHashFunction)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Call super constructor to reuse all of its implementation */
    AtObjectInit((AtObject)self);

    /* Initialize Methods */
    Override(self);
    MethodsInit(self);

    /* Only initialize method structures one time */
    m_methodsInit = 1;
    self->FirstHashFunction = firstHashFunction;
    self->SecondHashFunction = secondHashFunction;
    self->hash = hash;
    self->tableId = tableId;

    return self;
    }

ThaFrHashTable ThaFrHashTableNew(ThaFrHash hash, uint32 tableId, HashFunction firstHashFunction, HashFunction secondHashFunction)
    {
    ThaFrHashTable newHashTable = AtOsalMemAlloc(ObjectSize());
    if (newHashTable == NULL)
        return NULL;

    /* Construct it */
    return ThaFrHashTableObjectInit(newHashTable, hash, tableId, firstHashFunction, secondHashFunction);
    }

eAtRet ThaFrHashTableSetup(ThaFrHashTable self)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (self->entries)
        return cAtOk;

    if (AllEntriesAllocate(self) == NULL)
        return cAtErrorRsrcNoAvail;

    return cAtOk;
    }

uint32 ThaFrHashTableMaxNumEntries(ThaFrHashTable self)
    {
    if (self)
        return mMethodsGet(self)->MaxNumEntries(self);

    return 0;
    }

ThaFrHashEntry ThaFrHashTableFreeEntryGet(ThaFrHashTable self, uint32* hashKey, uint8 numBitsOfKey)
    {
    uint32 hashId;
    ThaFrHashEntry hashEntry;

    if (self == NULL)
        return NULL;

    hashId = self->FirstHashFunction(hashKey, numBitsOfKey);
    hashEntry = HashEntryGet(self, hashId);
    if (!ThaFrHashEntryIsInused(hashEntry))
        return hashEntry;

    hashId = self->SecondHashFunction(hashKey, numBitsOfKey);
    hashEntry = HashEntryGet(self, hashId);
    if (!ThaFrHashEntryIsInused(hashEntry))
        return hashEntry;

    return NULL;
    }

eAtRet ThaFrHashTableEntryUse(ThaFrHashTable self, ThaFrHashEntry entry)
    {
    if ((entry) && (self))
        {
        self->numberInUsedEntries++;
        ThaFrHashTablesNumInUsedEntriesIncreaseSort(self->hash);
        return cAtOk;
        }

    return cAtErrorNotExist;
    }

eAtRet ThaFrHashTableEntryUnuse(ThaFrHashTable self, ThaFrHashEntry entry)
    {
    if ((entry) && (self))
        {
        self->numberInUsedEntries--;
        ThaFrHashTablesNumInUsedEntriesIncreaseSort(self->hash);
        return cAtOk;
        }

    return cAtErrorNotExist;
    }

uint32 ThaFrHashTableRegisterOffset(ThaFrHashTable self)
    {
    if (self == NULL)
        return cInvalidUint32;

    return self->tableId * 0x1000;
    }

static ThaFrHashEntry FreeHashToSwap(ThaFrHashEntry self, ThaFrHashTable hashTable, uint8 numBitsOfKey, HashFunction hasFunction)
    {
    uint32 hashId;
    ThaFrHashEntry hashEntry;
    uint32 hashKey[cKeyNumberOfDwords];

    ThaFrVirtualCircuitHashKeyBuild(self->frVc, hashKey, cKeyNumberOfDwords);
    hashId = hasFunction(hashKey, numBitsOfKey);
    hashEntry = HashEntryGet(hashTable, hashId);

    return (ThaFrHashEntryIsInused(hashEntry)) ? NULL : hashEntry;
    }

static ThaFrHashEntry SwapHash(ThaFrHashEntry self, ThaFrHashEntry newHash)
    {
    AtFrVirtualCircuit vc = ThaFrHashEntryVirtualCircuitGet(self);
    AtEthFlow flow = ThaFrHashEntryEthFlowGet(self);
    if (newHash == NULL)
        return NULL;

    ThaFrHashEntryUnuse(self);
    ThaFrVirtualCircuitFlowLookupDeactivate(vc, self);
    ThaFrVirtualCircuitFlowLookupActivate(vc, flow, newHash);
    ThaFrHashEntryUse(newHash, vc, flow);
    return self;
    }

ThaFrHashEntry ThaFrHashTableReArrange(ThaFrHashTable self, uint32* hashKey, uint8 numBitsOfKey)
    {
    uint32 hashId;
    ThaFrHashEntry hashEntry;
    ThaFrHashEntry freeSwapHashEntry;

    if (self == NULL)
        return NULL;

    hashId = self->FirstHashFunction(hashKey, numBitsOfKey);
    hashEntry = HashEntryGet(self, hashId);
    freeSwapHashEntry = FreeHashToSwap(hashEntry, self, numBitsOfKey, self->SecondHashFunction);
    if (freeSwapHashEntry)
        return SwapHash(hashEntry, freeSwapHashEntry);

    hashId = self->SecondHashFunction(hashKey, numBitsOfKey);
    hashEntry = HashEntryGet(self, hashId);
    freeSwapHashEntry = FreeHashToSwap(hashEntry, self, numBitsOfKey, self->FirstHashFunction);
    if (freeSwapHashEntry)
        return SwapHash(hashEntry, freeSwapHashEntry);

    return NULL;
    }

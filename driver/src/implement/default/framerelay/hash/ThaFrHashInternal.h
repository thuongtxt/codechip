/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Frame-relay
 * 
 * File        : ThaFrHashInternal.h
 * 
 * Created Date: Jul 19, 2016
 *
 * Description : FR hash table internal definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAFRHASHINTERNAL_H_
#define _THAFRHASHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/common/AtObjectInternal.h"
#include "ThaFrHash.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaFrHashMethods
    {
    uint32 (*MaxNumTables)(ThaFrHash self);
    }tThaFrHashMethods;

typedef struct tThaFrHash
    {
    tAtObject super;
    const tThaFrHashMethods *methods;

    /* Private data */
    ThaFrHashTable* tables;
    } tThaFrHash;

typedef struct tThaFrHashTableMethods
    {
    uint32 (*MaxNumEntries)(ThaFrHashTable self);
    }tThaFrHashTableMethods;

typedef struct tThaFrHashTable
    {
    tAtObject super;
    const tThaFrHashTableMethods *methods;

    /* Private data */
    ThaFrHashEntry* entries;
    HashFunction FirstHashFunction;
    HashFunction SecondHashFunction;
    uint32 numberInUsedEntries;
    ThaFrHash hash;
    uint32 tableId;
    } tThaFrHashTable;

typedef struct tThaFrHashEntry
    {
    tAtObject super;

    /* Private data */
    ThaFrHashTable table;
    uint32 id;
    AtFrVirtualCircuit frVc;
    AtEthFlow flow;
    } tThaFrHashEntry;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THAFRHASHINTERNAL_H_ */


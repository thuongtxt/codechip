/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Frame-relay
 *
 * File        : ThaFrHash.c
 *
 * Created Date: Jul 18, 2016
 *
 * Description : Frame relay hash
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <atcrc.h>

#include "ThaFrHashInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self)  ((ThaFrHash)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tThaFrHashMethods m_methods;

/* Override */
static tAtObjectMethods  m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxNumTables(ThaFrHash self)
    {
    AtUnused(self);
    return 4;
    }

static void AllHashTablesDelete(ThaFrHash self)
    {
    uint32 idx, numbTables;

    if (self->tables == NULL)
        return;

    numbTables = ThaFrHashMaxNumTables(self);
    for (idx = 0; idx < numbTables; idx++)
        {
        AtObjectDelete((AtObject) self->tables[idx]);
        self->tables[idx] = NULL;
        }

    AtOsalMemFree(self->tables);
    self->tables = NULL;
    }

static void Delete(AtObject self)
    {
    AllHashTablesDelete((ThaFrHash)self);
    m_AtObjectMethods->Delete(self);
    }

static void OverrideAtObject(ThaFrHash self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(ThaFrHash self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(ThaFrHash self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, MaxNumTables);
        }

    mMethodsSet(self, &m_methods);
    }

static ThaFrHashTable *HashTableAllocate(ThaFrHash self)
    {
    ThaFrHashTable* tables = NULL;
    uint32 memSize = sizeof(ThaFrHashTable) * ThaFrHashMaxNumTables(self);

    tables = AtOsalMemAlloc(memSize);
    if (tables == NULL)
        return NULL;

    AtOsalMemInit(tables, 0, memSize);
    self->tables = tables;

    return tables;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaFrHash);
    }

static uint32 HashFunc1(uint32* hashKey, uint8 numKeyBits)
    {
    const uint32 cCrc_x12_x6_x4_x0 = 0x1051;
    return AtCrc(hashKey, numKeyBits, cCrc_x12_x6_x4_x0);
    }

static uint32 HashFunc2(uint32* hashKey, uint8 numKeyBits)
    {
    const uint32 cCrc_x12_x8_x4_x2_x0 = 0x1115;
    return AtCrc(hashKey, numKeyBits, cCrc_x12_x8_x4_x2_x0);
    }

static uint32 HashFunc3(uint32* hashKey, uint8 numKeyBits)
    {
    const uint32 cCrc_x12_x7_x2_x0 = 0x1085;
    return AtCrc(hashKey, numKeyBits, cCrc_x12_x7_x2_x0);
    }

static uint32 HashFunc4(uint32* hashKey, uint8 numKeyBits)
    {
    const uint32 cCrc_x12_x8_x5_x3_x2_x0 = 0x112D;
    return AtCrc(hashKey, numKeyBits, cCrc_x12_x8_x5_x3_x2_x0);
    }

static uint32 HashFunc5(uint32* hashKey, uint8 numKeyBits)
    {
    const uint32 cCrc_x12_x7_x4_x3_x2_x0 = 0x109D;
    return AtCrc(hashKey, numKeyBits, cCrc_x12_x7_x4_x3_x2_x0);
    }

static uint32 HashFunc6(uint32* hashKey, uint8 numKeyBits)
    {
    const uint32 cCrc_x12_x9_x5_x3_x0 = 0x1229;
    return AtCrc(hashKey, numKeyBits, cCrc_x12_x9_x5_x3_x0);
    }

static uint32 HashFunc7(uint32* hashKey, uint8 numKeyBits)
    {
    const uint32 cCrc_x12_x7_x5_x2_x0 = 0x10A5;
    return AtCrc(hashKey, numKeyBits, cCrc_x12_x7_x5_x2_x0);
    }

static uint32 HashFunc8(uint32* hashKey, uint8 numKeyBits)
    {
    const uint32 cCrc_x12_x9_x6_x4_x1_x0 = 0x1253;
    return AtCrc(hashKey, numKeyBits, cCrc_x12_x9_x6_x4_x1_x0);
    }

static eAtRet AllHashTablesCreate(ThaFrHash self)
    {
    uint32 table_i;
    static HashFunction Function1[] = {HashFunc1, HashFunc2, HashFunc3, HashFunc4};
    static HashFunction Function2[] = {HashFunc5, HashFunc6, HashFunc7, HashFunc8};

    for (table_i = 0; table_i < ThaFrHashMaxNumTables(self); table_i++)
        {
        self->tables[table_i] = ThaFrHashTableNew(self, table_i, Function1[table_i], Function2[table_i]);
        if ((self->tables[table_i] == NULL) || (ThaFrHashTableSetup(self->tables[table_i]) != cAtOk))
            {
            AllHashTablesDelete(self);
            return cAtErrorRsrcNoAvail;
            }
        }

    return cAtOk;
    }

static eAtRet AllHashTablesSetup(ThaFrHash self)
    {
    /* Allocate Table resource */
    if (HashTableAllocate(self) == NULL)
        return cAtErrorRsrcNoAvail;

    return AllHashTablesCreate(self);
    }

static ThaFrHash ThaFrHashObjectInit(ThaFrHash self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Call super constructor to reuse all of its implementation */
    AtObjectInit((AtObject)self);

    /* Initialize Methods */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

ThaFrHash ThaFrHashNew(void)
    {
    /* Allocate memory */
    ThaFrHash newFrHash = AtOsalMemAlloc(ObjectSize());
    if (newFrHash == NULL)
        return NULL;

    /* Construct it */
    return ThaFrHashObjectInit(newFrHash);
    }

uint32 ThaFrHashMaxNumTables(ThaFrHash self)
    {
    if (self)
        return mMethodsGet(self)->MaxNumTables(self);

    return 0;
    }

ThaFrHashTable ThaFrHashTableGet(ThaFrHash self, uint32 tableIndex)
    {
    if (self == NULL)
        return NULL;

    if (tableIndex >= ThaFrHashMaxNumTables(self))
        return NULL;

    return self->tables[tableIndex];
    }

eAtRet ThaFrHashSetup(ThaFrHash self)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    /* Already setup */
    if (self->tables)
        return cAtOk;

    return AllHashTablesSetup(self);
    }

void ThaFrHashTablesNumInUsedEntriesIncreaseSort(ThaFrHash self)
    {
    uint32 idx;
    uint32 idy;
    uint32 maxTables = ThaFrHashMaxNumTables(self);

    for (idx = 0; idx < maxTables - 1; idx++)
        {
        for (idy = 0; idy < maxTables - idx - 1; idy++)
            {
            ThaFrHashTable swap;
            ThaFrHashTable table = self->tables[idy];
            ThaFrHashTable nextTable = self->tables[idy + 1];

            if (table->numberInUsedEntries > nextTable->numberInUsedEntries)
                {
                swap = table;
                table = nextTable;
                nextTable = swap;
                }
            }
        }
    }

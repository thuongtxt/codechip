/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Frame Relay
 *
 * File        : ThaFrVirtualCircuit.c
 *
 * Created Date: Jul 22, 2016
 *
 * Description : Frame Relay Virtual Circuit Implement code
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtChannel.h"
#include "AtHdlcChannel.h"
#include "../eth/ThaEthFlowInternal.h"
#include "ThaFrVirtualCircuitInternal.h"
#include "ThaModuleFr.h"
#include "ThaFrVirtualCircuit.h"

/*--------------------------- Define -----------------------------------------*/
#define cBufferLen (4)

/*
 * HW Default Implement for  FR link.
 * FR link  : {FR_Link[11:0],FR_DLCI_Mod[1:0],FR_DLCI_ID[22:0]}
 *
 * */
#define cInputKeyFrLinkIdPart2Mask  cBit4_0
#define cInputKeyFrLinkIdPart2Shift 0
#define cInputKeyFrLinkIdPart1Mask  cBit31_25
#define cInputKeyFrLinkIdPart1Shift 25
#define cInputKeyFrDlciModMask      cBit24_23
#define cInputKeyFrDlciModShift     23
#define cInputKeyFrDlciIdMask       cBit22_0
#define cInputKeyFrDlciIdShift      0

/*
 *FR-Link-Id-Sub
 * */
#define cInputKeyFrLinkIdSub2Mask   cBit11_7
#define cInputKeyFrLinkIdSub2Shift  7
#define cInputKeyFrLinkIdSub1Mask   cBit6_0
#define cInputKeyFrLinkIdSub1Shift  0

#define cAf6_fr_hash_table_FR_Hash_Valid_Mask     cBit17
#define cAf6_fr_hash_table_FR_Hash_Valid_Shift        17
#define cAf6_fr_hash_table_FR_Link_Mask         cBit16_5
#define cAf6_fr_hash_table_FR_Link_Shift               5
#define cAf6_fr_hash_table_FR_DLCI_Mode_Mask     cBit4_3
#define cAf6_fr_hash_table_FR_DLCI_Mode_Shift          3
#define cAf6_fr_hash_table_FR_DLCI_Msb_Mask      cBit2_0
#define cAf6_fr_hash_table_FR_DLCI_Msb_Shift           0
#define cAf6_fr_hash_table_FR_DLCI_Lsb_Mask    cBit31_12
#define cAf6_fr_hash_table_FR_DLCI_Lsb_Shift          12
#define cAf6_fr_hash_table_FR_EthFlowID_Mask    cBit11_0
#define cAf6_fr_hash_table_FR_EthFlowID_Shift          0

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaFrVirtualCircuit)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods           m_AtObjectOverride;
static tAtChannelMethods          m_AtChannelOverride;
static tAtFrVirtualCircuitMethods m_AtFrVirtualCircuitOverride;

/* Save Super Implementation */
static const tAtObjectMethods           *m_AtObjectMethods = NULL;
static const tAtChannelMethods          *m_AtChannelMethods = NULL;
static const tAtFrVirtualCircuitMethods *m_AtFrVirtualCircuitMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Delete(AtObject self)
    {
    AtChannelBoundPwObjectDelete((AtChannel)self);
    m_AtObjectMethods->Delete(self);
    }

static ThaModuleFr ModuleFr(AtFrVirtualCircuit self)
    {
    return (ThaModuleFr)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cAtModuleFr);
    }

static ThaFrHash HashGet(AtFrVirtualCircuit self)
    {
    return ThaModuleFrHashGet(ModuleFr(self));
    }

static uint32 DlciMode(AtFrVirtualCircuit self)
    {
    AtFrLink fr = AtFrVirtualCircuitLinkGet(self);
    uint8 dc = AtFrLinkTxQ922AddressDCGet(fr);
    AtHdlcChannel channel = AtHdlcLinkHdlcChannelGet((AtHdlcLink)fr);
    uint32 addressSize = AtHdlcChannelAddressSizeGet(channel);

    /*
     * {0}: 10 bits address
     * {1}: 16 bits address
     * {2}: 17 bits address
     * {3}: 23 bits address
     */
    switch (addressSize)
        {
        case 2:
            return 0;
        case 3:
            return 1;
        case 4:
            return (dc == 0) ? 3 : 2;
        default:
            return 0;
        }
    }

static uint32* HashKeyBuild(AtFrVirtualCircuit self, uint32* keyVal, uint32 numberOfDwords)
    {
    AtFrLink fr;
    uint32 linkId;

    if (numberOfDwords < cKeyNumberOfDwords)
        return NULL;

    AtOsalMemInit(keyVal, 0, sizeof(uint32) * numberOfDwords);
    fr = AtFrVirtualCircuitLinkGet(self);
    linkId = AtChannelIdGet((AtChannel)fr);

    mRegFieldSet(keyVal[0], cInputKeyFrDlciId, AtChannelIdGet((AtChannel)self));
    mRegFieldSet(keyVal[0], cInputKeyFrDlciMod, DlciMode(self));
    mRegFieldSet(keyVal[0], cInputKeyFrLinkIdPart1, linkId & cInputKeyFrLinkIdSub1Mask);
    mRegFieldSet(keyVal[1], cInputKeyFrLinkIdPart2, ((linkId & cInputKeyFrLinkIdSub2Mask) >> cInputKeyFrLinkIdSub2Shift));

    return keyVal;
    }

static eBool IsBusy(AtFrVirtualCircuit self)
    {
    return (mThis(self)->entry) ? cAtTrue : cAtFalse;
    }

static ThaFrHashEntry FreeHashEntryGet(AtFrVirtualCircuit self)
    {
    ThaFrHashEntry hashEntry;
    uint32 i;
    uint32 hashKey[cKeyNumberOfDwords];
    ThaFrHash frHash = HashGet(self);
    uint32 numHashTable = ThaFrHashMaxNumTables(frHash);

    /* Build Key */
    if (ThaFrVirtualCircuitHashKeyBuild(self, hashKey, cKeyNumberOfDwords) == NULL)
        return NULL;

    for (i = 0; i < numHashTable; i++)
        {
        hashEntry = ThaFrHashTableFreeEntryGet(ThaFrHashTableGet(frHash, i), hashKey, cKeyNumberOfBits);
        if (hashEntry)
            return hashEntry;
        }

    for (i = 0; i < numHashTable; i++)
        {
        hashEntry = ThaFrHashTableReArrange(ThaFrHashTableGet(frHash, i), hashKey, cKeyNumberOfBits);
        if (hashEntry)
            return hashEntry;
        }

    return NULL;
    }

static eAtModuleFrRet FlowLookupDeactivate(AtFrVirtualCircuit self, ThaFrHashEntry hashEntry)
    {
    uint32 address;
    uint32 regVal[cBufferLen];
    uint32 location = ThaFrHashEntryIdGet(hashEntry);
    ThaFrHashTable table = ThaFrHashEntryHashTableGet(hashEntry);
    uint32 tableOffset = ThaFrHashTableRegisterOffset(table);

    address = tableOffset + location + ThaModuleFrHashBaseAddressGet(ModuleFr(self));
    mChannelHwLongRead(self, address, regVal, cBufferLen, cAtModuleFr);
    mRegFieldSet(regVal[1], cAf6_fr_hash_table_FR_Hash_Valid_, 0);
    mChannelHwLongWrite(self, address, regVal, cBufferLen, cAtModuleFr);

    return cAtOk;
    }

static eAtModuleFrRet FlowLookupActivate(AtFrVirtualCircuit self, AtEthFlow flow, ThaFrHashEntry hashEntry)
    {
    uint32 address;
    uint32 regVal[cBufferLen];
    uint32 location = ThaFrHashEntryIdGet(hashEntry);
    ThaFrHashTable table = ThaFrHashEntryHashTableGet(hashEntry);
    uint32 tableOffset = ThaFrHashTableRegisterOffset(table);
    AtFrLink fr = AtFrVirtualCircuitLinkGet(self);
    uint32 dlci = AtChannelIdGet((AtChannel)self);
    uint32 dlciMsb = (dlci & cBit23_20) >> 20;
    uint32 dlciLsb = dlci & cBit19_0;

    address = tableOffset + location + ThaModuleFrHashBaseAddressGet(ModuleFr(self));
    mChannelHwLongRead(self, address, regVal, cBufferLen, cAtModuleFr);

    mRegFieldSet(regVal[1], cAf6_fr_hash_table_FR_Hash_Valid_, 1);
    mRegFieldSet(regVal[1], cAf6_fr_hash_table_FR_Link_, AtChannelIdGet((AtChannel)fr));
    mRegFieldSet(regVal[1], cAf6_fr_hash_table_FR_DLCI_Mode_, DlciMode(self));
    mRegFieldSet(regVal[1], cAf6_fr_hash_table_FR_DLCI_Msb_, dlciMsb);

    mRegFieldSet(regVal[0], cAf6_fr_hash_table_FR_DLCI_Lsb_, dlciLsb);
    mRegFieldSet(regVal[0], cAf6_fr_hash_table_FR_EthFlowID_, AtChannelHwIdGet((AtChannel)flow));
    mChannelHwLongWrite(self, address, regVal, cBufferLen, cAtModuleFr);

    return cAtOk;
    }

static eAtModuleFrRet FlowBinding(AtFrVirtualCircuit self, AtEthFlow flow)
    {
    ThaFrHashEntry hashEntry;

    if (IsBusy(self))
        return cAtErrorRsrcNoAvail;

    /* Resource Hash Entry */
    hashEntry = FreeHashEntryGet(self);
    if (!hashEntry)
        return cAtErrorRsrcNoAvail;

    mThis(self)->entry = hashEntry;

    /* Active flow lookup */
    FlowLookupActivate(self, flow, hashEntry);

    /* Use this hash entry */
    return ThaFrHashEntryUse(hashEntry, self, flow);
    }

static eAtModuleFrRet FlowUnBind(AtFrVirtualCircuit self)
    {
    eAtRet ret;
    if (mThis(self)->entry == NULL)
        return cAtOk;

    /* Deactivate flow lookup */
    FlowLookupDeactivate(self, mThis(self)->entry);
    ret = ThaFrHashEntryUnuse(mThis(self)->entry);
    if (ret != cAtOk)
        return ret;

    mThis(self)->entry = NULL;
    return ret;
    }

static eAtModuleFrRet FlowBind(AtFrVirtualCircuit self, AtEthFlow flow)
    {
    eAtRet ret = m_AtFrVirtualCircuitMethods->FlowBind(self, flow);
    if (ret != cAtOk)
        return ret;

    if (flow)
        return FlowBinding(self, flow);

    return FlowUnBind(self);
    }

static AtFrLink FrLinkGet(AtFrVirtualCircuit self)
    {
    if (self)
        return (AtFrLink)self->channel;

    return NULL;
    }

static eAtRet TxTrafficEnable(AtChannel self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtOk;
    }

static eBool TxTrafficIsEnabled(AtChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet RxTrafficEnable(AtChannel self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtOk;
    }

static eBool RxTrafficIsEnabled(AtChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 EncapHwIdAllocate(AtChannel self)
    {
    return AtChannelHwIdGet(self);
    }

static void OverrideAtObject(AtFrVirtualCircuit self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtFrVirtualCircuit self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, TxTrafficEnable);
        mMethodOverride(m_AtChannelOverride, RxTrafficEnable);
        mMethodOverride(m_AtChannelOverride, TxTrafficIsEnabled);
        mMethodOverride(m_AtChannelOverride, RxTrafficIsEnabled);
        mMethodOverride(m_AtChannelOverride, EncapHwIdAllocate);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtFrVirtualCircuit(AtFrVirtualCircuit self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtFrVirtualCircuitMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtFrVirtualCircuitOverride, m_AtFrVirtualCircuitMethods, sizeof(m_AtFrVirtualCircuitOverride));

        mMethodOverride(m_AtFrVirtualCircuitOverride, FlowBind);
        mMethodOverride(m_AtFrVirtualCircuitOverride, FrLinkGet);
        }

    mMethodsSet(self, &m_AtFrVirtualCircuitOverride);
    }

static void Override(AtFrVirtualCircuit self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtFrVirtualCircuit(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaFrVirtualCircuit);
    }

AtFrVirtualCircuit ThaFrVirtualCircuitObjectInit(AtFrVirtualCircuit self, AtChannel channel, uint32 dlci)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Call super constructor */
    if (AtFrVirtualCircuitObjectInit(self, channel, dlci) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtFrVirtualCircuit ThaFrVirtualCircuitNew(uint32 dlci, AtChannel channel)
    {
    /* Allocate memory */
    AtFrVirtualCircuit newFrVc = AtOsalMemAlloc(ObjectSize());
    if (newFrVc == NULL)
        return NULL;

    /* Construct it */
    return ThaFrVirtualCircuitObjectInit(newFrVc, channel, dlci);
    }

uint32* ThaFrVirtualCircuitHashKeyBuild(AtFrVirtualCircuit self, uint32* keyVal, uint32 numberOfDwords)
    {
    if (self)
        return HashKeyBuild(self, keyVal, numberOfDwords);

    return NULL;
    }

eAtModuleFrRet ThaFrVirtualCircuitFlowLookupDeactivate(AtFrVirtualCircuit self, ThaFrHashEntry hashEntry)
    {
    if (!self || !hashEntry)
        return cAtErrorNotExist;

    return FlowLookupDeactivate(self, hashEntry);
    }

eAtModuleFrRet ThaFrVirtualCircuitFlowLookupActivate(AtFrVirtualCircuit self, AtEthFlow flow, ThaFrHashEntry hashEntry)
    {
    if (!self || !hashEntry || !flow)
        return cAtErrorNotExist;

    return FlowLookupActivate(self, flow, hashEntry);
    }

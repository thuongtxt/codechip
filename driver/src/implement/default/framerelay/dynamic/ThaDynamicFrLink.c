/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited 
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : ThaDynamicFrLink.c
 *
 * Created Date: Jul 30, 2016 
 *
 * Description : Thalassa Dynamic Frame Relay Link Implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/framerelay/AtFrLinkInternal.h"
#include "../../encap/dynamic/ThaDynamicHdlcLink.h"
#include "../../ppp/ThaPppLink.h"
#include "../ThaFrVirtualCircuit.h"
#include "ThaDynamicFrLink.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(_self) ((ThaDynamicFrLink)_self)

/*--------------------------- Local Typedefs ---------------------------------*/
typedef struct tThaDynamicFrLink * ThaDynamicFrLink;

/* For Simulate phase - remove when HW release */
typedef struct tThaDynamicFrLinkCache
    {
    eAtFrLinkEncapType encapType;
    eAtHdlcPduType pduType;
    eBool txEnabled;
    eBool rxEnabled;
    uint8 dc;
    uint8 cr;
    uint8 fecn;
    uint8 becn;
    uint8 de;
    uint8 dlcore;
    }tThaDynamicFrLinkCache;

typedef struct tThaDynamicFrLink
    {
    tAtFrLink super;
    tThaDynamicFrLinkCache cache;
    }tThaDynamicFrLink;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods  m_AtChannelOverride;
static tAtHdlcLinkMethods m_AtHdlcLinkOverride;
static tAtFrLinkMethods   m_AtFrLinkOverride;

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtModulePwRet PduTypeSet(AtHdlcLink self, eAtHdlcPduType pduType)
    {
    mThis(self)->cache.pduType = pduType;
    return cAtOk;
    }

static eAtHdlcPduType PduTypeGet(AtHdlcLink self)
    {
    return mThis(self)->cache.pduType;
    }

static eAtRet TxTrafficEnable(AtChannel self, eBool enable)
    {
    mThis(self)->cache.txEnabled = enable;
    return cAtOk;
    }

static eBool TxTrafficIsEnabled(AtChannel self)
    {
    return mThis(self)->cache.txEnabled;
    }

static eAtRet RxTrafficEnable(AtChannel self, eBool enable)
    {
    mThis(self)->cache.rxEnabled = enable;
    return cAtOk;
    }

static eBool RxTrafficIsEnabled(AtChannel self)
    {
    return mThis(self)->cache.rxEnabled;
    }

static eAtModuleFrRet EncapTypeSet(AtFrLink self, eAtFrLinkEncapType encapType)
    {
    mThis(self)->cache.encapType = encapType;
    return cAtOk;
    }

static eAtFrLinkEncapType EncapTypeGet(AtFrLink self)
    {
    return mThis(self)->cache.encapType;
    }

static eAtModuleFrRet TxQ922AddressDCSet(AtFrLink self, uint8 dc)
    {
    mThis(self)->cache.dc = dc;
    return cAtOk;
    }

static uint8 TxQ922AddressDCGet(AtFrLink self)
    {
    return mThis(self)->cache.dc;
    }

static eAtModuleFrRet TxQ922AddressCRSet(AtFrLink self, uint8 cr)
    {
    mThis(self)->cache.cr = cr;
    return cAtOk;
    }

static uint8 TxQ922AddressCRGet(AtFrLink self)
    {
    return mThis(self)->cache.cr;
    }

static AtFrVirtualCircuit VirtualCircuitObjectCreate(AtFrLink self, uint32 dlci)
    {
    return ThaFrVirtualCircuitNew(dlci, (AtChannel)self);
    }

static AtEthFlow OamFlowCreate(AtHdlcLink self)
    {
    /* Just for bypass autotest, it will review and correct when updated FR path */
    return AtHdlcLinkOamFlowCreate(self);
    }

static void OamFlowDelete(AtHdlcLink self)
    {
    /* Just for bypass autotest, it will review and correct when updated FR path */
    AtHdlcLinkOamFlowDelete(self);
    }

static eAtModuleFrRet TxQ922AddressFECNSet(AtFrLink self, uint8 fecn)
    {
    mThis(self)->cache.fecn = fecn;
    return cAtOk;
    }

static uint8 TxQ922AddressFECNGet(AtFrLink self)
    {
    return mThis(self)->cache.fecn;
    }

static eAtModuleFrRet TxQ922AddressBECNSet(AtFrLink self, uint8 becn)
    {
    mThis(self)->cache.becn = becn;
    return cAtOk;
    }

static uint8 TxQ922AddressBECNGet(AtFrLink self)
    {
    return mThis(self)->cache.becn;
    }

static eAtModuleFrRet TxQ922AddressDESet(AtFrLink self, uint8 de)
    {
    mThis(self)->cache.de = de;
    return cAtOk;
    }

static uint8 TxQ922AddressDEGet(AtFrLink self)
    {
    return mThis(self)->cache.de;
    }

static eAtModuleFrRet TxQ922AddressDlCoreSet(AtFrLink self, uint8 dlcore)
    {
    mThis(self)->cache.dlcore = dlcore;
    return cAtOk;
    }

static uint8 TxQ922AddressDlCoreGet(AtFrLink self)
    {
    return mThis(self)->cache.dlcore;
    }

static void OverrideAtChannel(ThaDynamicFrLink self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(m_AtChannelOverride));

        /* Override */
        mMethodOverride(m_AtChannelOverride, TxTrafficEnable);
        mMethodOverride(m_AtChannelOverride, TxTrafficIsEnabled);
        mMethodOverride(m_AtChannelOverride, RxTrafficEnable);
        mMethodOverride(m_AtChannelOverride, RxTrafficIsEnabled);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtHdlcLink(ThaDynamicFrLink self)
    {
    AtHdlcLink link = (AtHdlcLink)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtHdlcLinkOverride, mMethodsGet(link), sizeof(m_AtHdlcLinkOverride));

        /* Override */
        mMethodOverride(m_AtHdlcLinkOverride, PduTypeSet);
        mMethodOverride(m_AtHdlcLinkOverride, PduTypeGet);
        mMethodOverride(m_AtHdlcLinkOverride, OamFlowCreate);
        mMethodOverride(m_AtHdlcLinkOverride, OamFlowDelete);
        }

    mMethodsSet(link, &m_AtHdlcLinkOverride);
    }

static void OverrideAtFrLink(ThaDynamicFrLink self)
    {
    AtFrLink link = (AtFrLink)self;

    if (!m_methodsInit)
        {
        /* Reuse all super implementation */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtFrLinkOverride, mMethodsGet(link), sizeof(m_AtFrLinkOverride));

        /* Override */
        mMethodOverride(m_AtFrLinkOverride, VirtualCircuitObjectCreate);
        mMethodOverride(m_AtFrLinkOverride, EncapTypeSet);
        mMethodOverride(m_AtFrLinkOverride, EncapTypeGet);
        mMethodOverride(m_AtFrLinkOverride, TxQ922AddressDCSet);
        mMethodOverride(m_AtFrLinkOverride, TxQ922AddressDCGet);
        mMethodOverride(m_AtFrLinkOverride, TxQ922AddressCRSet);
        mMethodOverride(m_AtFrLinkOverride, TxQ922AddressCRGet);
        mMethodOverride(m_AtFrLinkOverride, TxQ922AddressFECNSet);
        mMethodOverride(m_AtFrLinkOverride, TxQ922AddressFECNGet);
        mMethodOverride(m_AtFrLinkOverride, TxQ922AddressBECNSet);
        mMethodOverride(m_AtFrLinkOverride, TxQ922AddressBECNGet);
        mMethodOverride(m_AtFrLinkOverride, TxQ922AddressDESet);
        mMethodOverride(m_AtFrLinkOverride, TxQ922AddressDEGet);
        mMethodOverride(m_AtFrLinkOverride, TxQ922AddressDlCoreSet);
        mMethodOverride(m_AtFrLinkOverride, TxQ922AddressDlCoreGet);
        }

    mMethodsSet(link, &m_AtFrLinkOverride);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaDynamicFrLink);
    }

static void Override(ThaDynamicFrLink self)
    {
    OverrideAtChannel(self);
    OverrideAtHdlcLink(self);
    OverrideAtFrLink(self);
    }

static AtFrLink ObjectInit(AtFrLink self, AtHdlcChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtFrLinkObjectInit((AtFrLink)self, channel) == NULL)
        return NULL;

    /* Setup class */
    Override((ThaDynamicFrLink)self);
    m_methodsInit = 1;

    return self;
    }

AtFrLink ThaDynamicFrLinkNew(AtHdlcChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtFrLink newLink = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newLink == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newLink, channel);
    }

eAtRet ThaDynamicFrLinkActivateConfiguration(AtHdlcLink self)
    {
    AtUnused(self);
    return cAtOk;
    }

eAtRet ThaDynamicFrLinkConfigureCache(AtHdlcLink self)
    {
    AtUnused(self);
    return cAtOk;
    }

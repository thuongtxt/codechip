/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : ThaPhysicalFrLink.h
 *
 * Created Date: Aug 10, 2016 
 *
 * Description : Physical Frame Relay Link interface
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPHYSICALFRLINK_H_
#define _THAPHYSICALFRLINK_H_

/*--------------------------- Include files ----------------------------------*/


#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtFrLink ThaPhysicalFrLinkNew(AtHdlcChannel physicalChannel);

#ifdef __cplusplus
}
#endif

#endif /* _THAPHYSICALFRLINK_H_ */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Frame-relay
 *
 * File        : ThaDynamicFrLink.h
 *
 * Created Date: Jul 30, 2016 
 *
 * Description : Dynamic Frame Relay Link class Interface
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THADYNAMICFRLINK_H_
#define _THADYNAMICFRLINK_H_

/*--------------------------- Include files ----------------------------------*/


#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtFrLink ThaDynamicFrLinkNew(AtHdlcChannel channel);

#ifdef __cplusplus
}
#endif

#endif /* _THADYNAMICFRLINK_H_ */

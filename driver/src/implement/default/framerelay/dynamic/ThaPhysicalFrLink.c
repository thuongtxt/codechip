/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited 
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Frame-relay
 *
 * File        : ThaPhysicalFrLink.c
 *
 * Created Date: Aug 10, 2016 
 *
 * Description : Physical Frame Relay Link implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/framerelay/AtFrLinkInternal.h"
#include "ThaPhysicalFrLink.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self)         ((ThaPhysicalFrLink)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/
typedef struct tThaPhysicalFrLink * ThaPhysicalFrLink;

/* For Simulate phase - remove when HW release */
typedef struct tThaPhysicalFrLinkCache
    {
    eBool queueEnabled;
    }tThaPhysicalFrLinkCache;

typedef struct tThaPhysicalFrLink
    {
    tAtFrLink super;
    tThaPhysicalFrLinkCache cache;
    /* Private data */
    }tThaPhysicalFrLink;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods m_AtChannelOverride;

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet QueueEnable(AtChannel self, eBool enable)
    {
    mThis(self)->cache.queueEnabled = enable;
    return cAtOk;
    }

static eBool QueueIsEnabled(AtChannel self)
    {
    return mThis(self)->cache.queueEnabled;
    }

static void OverrideAtChannel(AtFrLink self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(m_AtChannelOverride));

        /* Override */
        mMethodOverride(m_AtChannelOverride, QueueEnable);
        mMethodOverride(m_AtChannelOverride, QueueIsEnabled);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtFrLink self)
    {
    OverrideAtChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPhysicalFrLink);
    }

static AtFrLink ObjectInit(AtFrLink self, AtHdlcChannel physicalChannel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtFrLinkObjectInit(self, physicalChannel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtFrLink ThaPhysicalFrLinkNew(AtHdlcChannel physicalChannel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtFrLink newLink = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newLink == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newLink, physicalChannel);
    }

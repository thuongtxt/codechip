/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PMC
 *
 * File        : Tha60290021ModulePwPmc.c
 *
 * Created Date: Nov 3, 2016
 *
 * Description : PW PMC module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../man/ThaDeviceInternal.h"
#include "../ocn/ThaModuleOcn.h"
#include "../sdh/ThaModuleSdh.h"
#include "../pdh/ThaPdhDe1Internal.h"
#include "../pdh/ThaPdhVcDe1Internal.h"
#include "ThaModulePmcInternal.h"
#include "ThaModulePmcReg.h"

/*--------------------------- Define -----------------------------------------*/
/* PW TX counter offset */
#define cAf6_txpwcnt_packet_offset				2
#define cAf6_txpwcnt_pbit_packet_offset			0
#define cAf6_txpwcnt_lbit_packet_offset			1
#define cAf6_txpwcnt_rbit_packet_offset			0
#define cAf6_txpwcnt_nbit_packet_offset			1

/* PW RX counter offset */
#define cAf6_rxpwcnt_packet_offset				2
#define cAf6_rxpwcnt_lbit_packet_offset			2
#define cAf6_rxpwcnt_mbit_packet_offset			0
#define cAf6_rxpwcnt_pbit_packet_offset			1
#define cAf6_rxpwcnt_rbit_packet_offset			0
#define cAf6_rxpwcnt_nbit_packet_offset			1
#define cAf6_rxpwcnt_malformed_packet_offset	2
#define cAf6_rxpwcnt_stray_packet_offset		2
#define cAf6_rxpwcnt_mbit_packet_offset			0
#define cAf6_rxpwcnt_lost_packet_offset			0
#define cAf6_rxpwcnt_overrun_packet_offset		0
#define cAf6_rxpwcnt_underrun_packet_offset		1
#define cAf6_rxpwcnt_lopsyn_packet_offset		2
#define cAf6_rxpwcnt_lopsta_packet_offset		1
#define cAf6_rxpwcnt_early_packet_offset		0
#define cAf6_rxpwcnt_late_packet_offset		    0

/* SDH */
#define cAf6_reg_line_counter_rei_index         0
#define cAf6_reg_line_counter_rei_block_index   0
#define cAf6_reg_line_counter_b2_block_index    1
#define cAf6_reg_line_counter_b1_block_index    2
#define cAf6_reg_auvc_counter_reibip_index      0
#define cAf6_reg_auvc_counter_tx_paj_index      0
#define cAf6_reg_auvc_counter_rx_paj_index      1

/* PDH */
#define cAf6_reg_de1_counter_crc_fe_index       0
#define cAf6_reg_de1_counter_lcv_rei_index      1
#define cAf6_reg_de3_counter_rei_fe_index       0
#define cAf6_reg_de3_counter_cbit_pbit_index    1
#define cAf6_reg_de3_counter_lcv_index          2

#define cThaMoudulePmcNumStsInGroup             24

/* PMC counter index of index ID and position ID */
#define cPmcCounterIndex(cntIdx, cntPos)        cntIdx, cntPos

/* ENCAP GFP counters  */
#define cGfpEncapBytesIndex                     cPmcCounterIndex(0, 0)
#define cGfpEncapFramesIndex                    cPmcCounterIndex(0, 1)
#define cGfpEncapIdlesIndex                     cPmcCounterIndex(0, 2)

#define cGfpEncapCsfIndex                       cPmcCounterIndex(1, 1)
#define cGfpEncapCmfIndex                       cPmcCounterIndex(1, 2)

/* DECAP GFP counters */
#define cGfpDecapDropFramesIndex                cPmcCounterIndex(0, 0)
#define cGfpDecapFramesIndex                    cPmcCounterIndex(0, 1)
#define cGfpDecapIdleFramesIndex                cPmcCounterIndex(0, 2)
#define cGfpDecapBytesIndex                     cPmcCounterIndex(0, 3)

#define cGfpDecapCmfIndex                       cPmcCounterIndex(1, 1)
#define cGfpDecapTHecUCorrErrIndex              cPmcCounterIndex(1, 3)

#define cGfpDecapTypeCorrErrIndex               cPmcCounterIndex(2, 0)
#define cGfpDecapTHecCorrErrIndex               cPmcCounterIndex(2, 1)
#define cGfpDecapEHecErrIndex                   cPmcCounterIndex(2, 3)

#define cGfpDecapCHecUCorrErrIndex              cPmcCounterIndex(3, 1)
#define cGfpDecapPliCorrErrIndex                cPmcCounterIndex(3, 2)
#define cGfpDecapCHecCorrErrIndex               cPmcCounterIndex(3, 3)

#define cGfpDecapFcsErrIndex                    cPmcCounterIndex(4, 1)

#define cGfpDecapUpmIndex                       cPmcCounterIndex(5, 0)

/* Eth flow counter position ID */
#define cFlowCntBytesPosId                      0
#define cFlowCntPktsPosId                       1
#define cFlowCntDisBytesPosId                   2
#define cFlowCntDisPktsPosId                    3
#define cFlowCntFragPosId                       0
#define cFlowCntNullFragPosId                   1
#define cFlowCntBECNPosId                       2
#define cFlowCntFECNPosId                       3
#define cFlowCntDEPosId                         0

/* Eth flow counters type index*/
#define cFlowCntBytesIndex                      0
#define cFlowCntPktsIndex                       0
#define cFlowCntDisBytesIndex                   0
#define cFlowCntDisPktsIndex                    0
#define cFlowCntFragIndex                       1
#define cFlowCntNullFragIndex                   1
#define cFlowCntBECNIndex                       1
#define cFlowCntFECNIndex                       1
#define cFlowCntDEIndex                         2


/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaModulePmc)self)

#define mEthTypeRx(index) ((index) + 0x10)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaModulePmcMethods m_methods;

/* Override */
static tAtModuleMethods m_AtModuleOverride;

/* Save super implementation */
static const tAtModuleMethods *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool VcTu1xCounterGroupDivided(ThaModulePmc self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool LineSupportedBlockCounters(ThaModulePmc self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 BaseAddress(ThaModulePmc self, uint8 partId)
	{
	AtUnused(self);
	AtUnused(partId);
	return cAf6Reg_Pmc_Base_Address;
	}

static uint32 AddressWithLocalAddress(ThaModulePmc self, uint32 localAddr)
    {
    return ThaModulePmcBaseAddress(self) + localAddr;
    }

static uint32 Vc1xStsLocal(ThaModulePmc self, uint32 hwSts)
    {
    if (mMethodsGet(mThis(self))->VcTu1xCounterGroupDivided(self))
        return (hwSts % cThaMoudulePmcNumStsInGroup);

    return hwSts;
    }

static uint32 De1StsLocal(ThaModulePmc self, uint32 hwSts)
    {
    AtUnused(self);
    return (hwSts % cThaMoudulePmcNumStsInGroup);
    }

static uint32 CounterRead2Clear(ThaModulePmc self, uint32 address, eBool clear)
    {
    uint32 regVal = mModuleHwRead(self, address);

    if (clear)
        mModuleHwWrite(self, address, 0);

    return regVal;
    }

static uint32 PwCounterOffset(ThaModulePmc self, AtPw pw, uint32 cntIndex, eBool clear)
	{
	AtUnused(self);
	return AtChannelIdGet((AtChannel)pw) + (0x1500UL * cntIndex) + (0x4000UL * mRo(clear));
	}

static uint32 PwCounterAddress(ThaModulePmc self, AtPw pw, uint32 localAddress, uint32 cntIndex, eBool clear)
    {
    return AddressWithLocalAddress(self, localAddress) + mMethodsGet(self)->PwCounterOffset(self, pw, cntIndex, clear);
    }

static uint32 PwByteCounterOffset(ThaModulePmc self, AtPw pw, eBool clear)
	{
	AtUnused(self);
	return AtChannelIdGet((AtChannel)pw) + (0x2000UL * mRo(clear));
	}

static uint32 PwByteCounterAddress(ThaModulePmc self, AtPw pw, uint32 localAddress, eBool clear)
    {
    return AddressWithLocalAddress(self, localAddress) + mMethodsGet(self)->PwByteCounterOffset(self, pw, clear);
    }

static uint32 PwCounterGet(ThaModulePmc self, AtPw pw, uint32 regAddress, uint32 cntIndex, eBool clear)
	{
	uint32 address = PwCounterAddress(self, pw, regAddress, cntIndex, cAtFalse);
	return CounterRead2Clear(self, address, clear) & cAf6Reg_Counter_Field_Mask;
	}

static uint32 PwByteCounterGet(ThaModulePmc self, AtPw pw, uint32 regAddress, eBool clear)
	{
	uint32 address = PwByteCounterAddress(self, pw, regAddress, cAtFalse);
	return CounterRead2Clear(self, address, clear);
	}

static uint32 LineCounterOffset(ThaModulePmc self, ThaSdhLine line, uint32 cntIndex, eBool clear)
	{
	AtUnused(self);
	return AtChannelIdGet((AtChannel)line) + 16UL * cntIndex + 0x40UL * mRo(clear);
	}

static uint32 LineCounterAddress(ThaModulePmc self, ThaSdhLine line, uint32 localAddress, uint32 cntIndex, eBool clear)
    {
    return AddressWithLocalAddress(self, localAddress) + mMethodsGet(self)->LineCounterOffset(self, line, cntIndex, clear);
    }

static uint32 LineCounterGet(ThaModulePmc self, ThaSdhLine line, uint32 localAddress, uint32 cntIndex, eBool clear)
    {
    return CounterRead2Clear(self, LineCounterAddress(self, line, localAddress, cntIndex, cAtFalse), clear);
    }

static uint32 LineCounter1Reg(ThaModulePmc self)
    {
    AtUnused(self);
    return cAf6Reg_upen_poh_pmr_cnt0_rc;
    }

static uint32 LineCounter2Reg(ThaModulePmc self)
    {
    AtUnused(self);
    return cAf6Reg_upen_poh_pmr_cnt1_rc;
    }

static uint32 Vc1xDefaultOffset(ThaModulePmc self, AtSdhChannel sdhChannel)
    {
    AtUnused(self);
    AtUnused(sdhChannel);
    return cInvalidUint32;
    }

static uint32 AuVcDefaultOffset(ThaModulePmc self, AtSdhChannel channel)
    {
    uint8 slice, hwStsInSlice;
    AtUnused(self);

    ThaSdhChannel2HwMasterStsId(channel, cThaModuleOcn, &slice, &hwStsInSlice);
    return (uint32)(64UL * slice + hwStsInSlice);
    }

static uint32 AuVcRegOffsetCoef(ThaModulePmc self)
	{
	AtUnused(self);
	return 512UL;
	}

static uint32 AuVcCounterOffset(ThaModulePmc self, AtSdhChannel channel, uint32 cntIndex)
	{
	AtUnused(self);
	return mMethodsGet(self)->AuVcDefaultOffset(self, channel) + (mMethodsGet(self)->AuVcRegOffsetCoef(self) * cntIndex);
	}

static uint32 AuVcCounterAddress(ThaModulePmc self, AtSdhChannel channel, uint32 localAddress, uint32 cntIndex)
    {
    return AddressWithLocalAddress(self, localAddress) + AuVcCounterOffset(self, channel, cntIndex);
    }

static uint32 AuVcCounterGet(ThaModulePmc self, AtSdhChannel channel, uint32 regAddress, uint32 cntIndex, eBool clear)
    {
    return CounterRead2Clear(self, AuVcCounterAddress(self, channel, regAddress, cntIndex), clear) & cAf6Reg_Counter_Field_Mask;
    }

static uint32 Vc1xDefaultOffsetAndHwStsGet(ThaModulePmc self, AtSdhChannel sdhChannel, uint32 *hwSts)
    {
    uint8 slice, hwStsInSlice;
    uint8 vtgId = AtSdhChannelTug2Get(sdhChannel);
    uint8 vtId = AtSdhChannelTu1xGet(sdhChannel);

    if (ThaSdhChannel2HwMasterStsId(sdhChannel, cThaModuleOcn, &slice, &hwStsInSlice) == cAtOk)
        {
        *hwSts = hwStsInSlice;
        return (uint32)((672UL * slice) + (28UL * Vc1xStsLocal(self, hwStsInSlice)) + (4UL * vtgId) + vtId);
        }

    *hwSts = 0;
    return cBit31_0;
    }

static uint8 Vc1xGroupIdGet(ThaModulePmc self, uint32 hwSts)
    {
    if (mMethodsGet(self)->VcTu1xCounterGroupDivided(self))
        return (uint8)(hwSts / cThaMoudulePmcNumStsInGroup);

    return 0;
    }

static uint32 Vc1xRegOffsetCoef(ThaModulePmc self)
	{
	AtUnused(self);
	return 5376UL;
	}

static uint32 Vc1xCounterOffset(ThaModulePmc self, uint32 cntIndex, uint32 defaultOffset)
    {
    return defaultOffset + (mMethodsGet(self)->Vc1xRegOffsetCoef(self) * cntIndex);
    }

static uint32 Vc1xCounterAddress(ThaModulePmc self, uint32 localAddress, uint32 cntIndex, uint32 defaultOffset)
    {
    return AddressWithLocalAddress(self, localAddress) + Vc1xCounterOffset(self, cntIndex, defaultOffset);
    }

static uint32 Vc1xCounterGet(ThaModulePmc self, uint32 regAddress, uint32 cntIndex, uint32 defaultOffset, eBool clear)
    {
    return CounterRead2Clear(self, Vc1xCounterAddress(self, regAddress, cntIndex, defaultOffset), clear) & cAf6Reg_Counter_Field_Mask;
    }

static uint32 AuVcPohCounter1Reg(ThaModulePmc self)
    {
    AtUnused(self);
    return cAf6Reg_upen_pohpmdat_sts_ber_erdi1_rw_Base;
    }

static uint32 AuVcPohCounter2Reg(ThaModulePmc self)
    {
    AtUnused(self);
    return cAf6Reg_upen_pohpmdat_sts_ber_erdi2_rw_Base;
    }

static uint32 AuVcPointerCounter1Reg(ThaModulePmc self)
    {
    AtUnused(self);
    return cAf6Reg_upen_poh_sts_cnt_rw_Base;
    }

static uint32 AuVcPointerCounter2Reg(ThaModulePmc self)
    {
    AtUnused(self);
    return cAf6Reg_upen_poh_sts_cnt1_rw_Base;
    }

static uint32 Vc1xPohCounter1Reg(ThaModulePmc self, uint8 groupId)
    {
    AtUnused(self);
    return ((groupId == 0) ? cAf6Reg_upen_pohpmdat_vt_ber_erdi0_rw_Base : cAf6Reg_upen_pohpmdat_vt_ber_erdi1_rw_Base);
    }

static uint32 Vc1xPohCounter2Reg(ThaModulePmc self, uint8 groupId)
    {
    AtUnused(self);
    return ((groupId == 0) ? cAf6Reg_upen_pohpmdat_vt_ber_erdi01_rw_Base : cAf6Reg_upen_pohpmdat_vt_ber_erdi2_rw_Base);
    }

static uint32 Tu1xPohCounter1Reg(ThaModulePmc self, uint8 groupId)
    {
    AtUnused(self);
    return ((groupId == 0) ? cAf6Reg_upen_poh_vt_cnt0_rw_Base : cAf6Reg_upen_poh_vt_cnt1_rw_Base);
    }

static uint32 Tu1xPohCounter2Reg(ThaModulePmc self, uint8 groupId)
    {
    AtUnused(self);
    return ((groupId == 0) ? cAf6Reg_upen_poh_vt_cnt01_rw_Base : cAf6Reg_upen_poh_vt_cnt2_rw_Base);
    }

static uint32 De1Counter1Reg(ThaModulePmc self, uint8 groupId)
    {
    AtUnused(self);
    return ((groupId == 0) ? cAf6Reg_upen_pdh_de1cntval30_rw_Base : cAf6Reg_upen_pdh_de1cntval74_rw_Base);
    }

static uint32 De1Counter2Reg(ThaModulePmc self, uint8 groupId)
    {
    AtUnused(self);
    return ((groupId == 0) ? cAf6Reg_upen_pdh_de1cntval31_rw_Base : cAf6Reg_upen_pdh_de1cntval75_rw_Base);
    }

static uint8 De1GroupIdGet(ThaModulePmc self, uint32 hwSts)
    {
    AtUnused(self);
    return (uint8)(hwSts / cThaMoudulePmcNumStsInGroup);
    }

static uint32 De1DefaultOffset(ThaModulePmc self, AtPdhChannel de1)
    {
    AtUnused(self);
    AtUnused(de1);
    return cInvalidUint32;
    }

static uint32 De1DefaultOffsetAndHwStsGet(ThaModulePmc self, AtPdhChannel de1, uint32* hwSts)
    {
    uint8 slice, hwIdInSlice, vtg, vt;

    ThaPdhDe1HwIdGet((ThaPdhDe1)de1, cAtModulePdh, &slice, &hwIdInSlice, &vtg, &vt);
    *hwSts = hwIdInSlice;
    return (4096UL * slice) + (28UL * De1StsLocal(self, hwIdInSlice)) + (4UL * vtg) + vt;
    }

static uint32 De1CounterOffset(ThaModulePmc self, uint32 cntIndex, uint32 defaultOffset)
	{
	AtUnused(self);
	return defaultOffset + (672UL * cntIndex);
	}

static uint32 VcDe1CounterAddress(ThaModulePmc self, uint32 regAddress, uint32 cntIndex, uint32 defaultOffset)
    {
    return AddressWithLocalAddress(self, regAddress) + De1CounterOffset(self, cntIndex, defaultOffset);
    }

static uint32 De1CounterGet(ThaModulePmc self, uint32 regAddress, uint32 cntIndex, uint32 defaultOffset, eBool clear)
    {
    return CounterRead2Clear(self, VcDe1CounterAddress(self, regAddress, cntIndex, defaultOffset), clear) & cAf6Reg_Counter_Field_Mask;
    }

static uint32 De3DefaultOffset(ThaModulePmc self, AtPdhChannel de3)
	{
	uint8 sliceId, hwIdInSlice;
	AtUnused(self);

	ThaPdhChannelHwIdGet((AtPdhChannel)de3, cAtModulePdh, &sliceId, &hwIdInSlice);
	return (64UL * sliceId) + hwIdInSlice;
	}

static uint32 De3RegOffsetCoef(ThaModulePmc self)
	{
	AtUnused(self);
	return 512UL;
	}

static uint32 De3CounterOffset(ThaModulePmc self, AtPdhChannel de3, uint32 cntIndex)
	{
	return mMethodsGet(self)->De3DefaultOffset(self, de3) + (mMethodsGet(self)->De3RegOffsetCoef(self) * cntIndex);
	}

static uint32 De3CounterAddress(ThaModulePmc self, AtPdhChannel de3, uint32 regAddress, uint32 cntIndex)
    {
    return AddressWithLocalAddress(self, regAddress) + De3CounterOffset(self, de3, cntIndex);
    }

static uint32 De3CounterGet(ThaModulePmc self, AtPdhChannel channel, uint32 regAddress, uint32 cntIndex, eBool clear)
    {
    return CounterRead2Clear(self, De3CounterAddress(self, channel, regAddress, cntIndex), clear) & cAf6Reg_Counter_Field_Mask;
    }

static uint32 SdhLineB1CounterIndexGet(ThaModulePmc self)
    {
    if (mMethodsGet(self)->LineSupportedBlockCounters(self))
        return 2;

    return 1;
    }

static uint32 SdhLineB2CounterIndexGet(ThaModulePmc self)
    {
    if (mMethodsGet(self)->LineSupportedBlockCounters(self))
        return 1;

    return 0;
    }

static uint32 SdhLineB2CounterAddressGet(ThaModulePmc self)
    {
    if (mMethodsGet(self)->LineSupportedBlockCounters(self))
        return mMethodsGet(self)->LineCounter1Reg(self);

    return mMethodsGet(self)->LineCounter2Reg(self);
    }

static uint32 SdhLineCounterGet(ThaModulePmc self, AtSdhChannel channel, uint16 counterType, eBool clear)
    {
    ThaSdhLine line = (ThaSdhLine)channel;

    switch (counterType)
        {
        case cAtSdhLineCounterTypeB1:
            return LineCounterGet(self, line, mMethodsGet(self)->LineCounter1Reg(self), SdhLineB1CounterIndexGet(self), clear);

        case cAtSdhLineCounterTypeB2:
            return LineCounterGet(self, line, SdhLineB2CounterAddressGet(self), SdhLineB2CounterIndexGet(self), clear);

        case cAtSdhLineCounterTypeRei:
            return LineCounterGet(self, line, mMethodsGet(self)->LineCounter1Reg(self), cAf6_reg_line_counter_rei_index, clear);

        default:
            return 0;
        }

    return 0;
    }

static uint32 SdhLineBlockErrorCounterGet(ThaModulePmc self, AtSdhChannel channel, uint16 counterType, eBool clear)
    {
    ThaSdhLine line = (ThaSdhLine)channel;

    if (mMethodsGet(self)->LineSupportedBlockCounters(self) == cAtFalse)
        return 0;

    switch (counterType)
        {
        case cAtSdhLineCounterTypeB1:
            return LineCounterGet(self, line, mMethodsGet(self)->LineCounter2Reg(self), cAf6_reg_line_counter_b1_block_index, clear);

        case cAtSdhLineCounterTypeB2:
            return LineCounterGet(self, line, mMethodsGet(self)->LineCounter2Reg(self), cAf6_reg_line_counter_b2_block_index, clear);

        case cAtSdhLineCounterTypeRei:
            return LineCounterGet(self, line, mMethodsGet(self)->LineCounter2Reg(self), cAf6_reg_line_counter_rei_block_index, clear);

        default:
            return 0;
        }

    return 0;
    }

static uint32 AuVcPohCounterGet(ThaModulePmc self, AtSdhChannel channel, uint16 counterType, eBool clear)
    {
    switch (counterType)
        {
        case cAtSdhPathCounterTypeBip:
            return AuVcCounterGet(self, channel, mMethodsGet(self)->AuVcPohCounter2Reg(self), cAf6_reg_auvc_counter_reibip_index, clear);

        case cAtSdhPathCounterTypeRei:
            return AuVcCounterGet(self, channel, mMethodsGet(self)->AuVcPohCounter1Reg(self), cAf6_reg_auvc_counter_reibip_index, clear);

        default:
            return 0;
        }

    return 0;
    }

static uint32 AuVcPointerCounterGet(ThaModulePmc self, AtSdhChannel channel, uint16 counterType, eBool clear)
    {
    switch (counterType)
        {
        case cAtSdhPathCounterTypeRxPPJC:
            return AuVcCounterGet(self, channel, mMethodsGet(self)->AuVcPointerCounter2Reg(self), cAf6_reg_auvc_counter_rx_paj_index, clear);

        case cAtSdhPathCounterTypeRxNPJC:
            return AuVcCounterGet(self, channel, mMethodsGet(self)->AuVcPointerCounter1Reg(self), cAf6_reg_auvc_counter_rx_paj_index, clear);

        case cAtSdhPathCounterTypeTxPPJC:
            return AuVcCounterGet(self, channel, mMethodsGet(self)->AuVcPointerCounter2Reg(self), cAf6_reg_auvc_counter_tx_paj_index, clear);

        case cAtSdhPathCounterTypeTxNPJC:
            return AuVcCounterGet(self, channel, mMethodsGet(self)->AuVcPointerCounter1Reg(self), cAf6_reg_auvc_counter_tx_paj_index, clear);

        default:
            return 0;
        }

    return 0;
    }

static uint32 TuVc1xPointerCounterGet(ThaModulePmc self, AtSdhChannel channel, uint16 counterType, eBool clear)
    {
    uint32 hwSts;
    uint32 defaultOffset = Vc1xDefaultOffsetAndHwStsGet(self, channel, &hwSts);
    uint8  groupId = Vc1xGroupIdGet(self, hwSts);

    switch (counterType)
        {
        case cAtSdhPathCounterTypeBip:
            return Vc1xCounterGet(self, mMethodsGet(self)->Vc1xPohCounter2Reg(self, groupId), cAf6_reg_auvc_counter_reibip_index, defaultOffset, clear);

        case cAtSdhPathCounterTypeRei:
            return Vc1xCounterGet(self, mMethodsGet(self)->Vc1xPohCounter1Reg(self, groupId), cAf6_reg_auvc_counter_reibip_index, defaultOffset, clear);

        case cAtSdhPathCounterTypeRxPPJC:
            return Vc1xCounterGet(self, Tu1xPohCounter2Reg(self, groupId), cAf6_reg_auvc_counter_rx_paj_index, defaultOffset, clear);

        case cAtSdhPathCounterTypeRxNPJC:
            return Vc1xCounterGet(self, Tu1xPohCounter1Reg(self, groupId), cAf6_reg_auvc_counter_rx_paj_index, defaultOffset, clear);

        case cAtSdhPathCounterTypeTxPPJC:
            return Vc1xCounterGet(self, Tu1xPohCounter2Reg(self, groupId), cAf6_reg_auvc_counter_tx_paj_index, defaultOffset, clear);

        case cAtSdhPathCounterTypeTxNPJC:
            return Vc1xCounterGet(self, Tu1xPohCounter1Reg(self, groupId), cAf6_reg_auvc_counter_tx_paj_index, defaultOffset, clear);

        default:
            return 0;
        }

    return 0;
    }

static uint32 VcDe1CounterGet(ThaModulePmc self, AtPdhChannel channel, uint16 counterType, eBool clear)
    {
    uint32 hwSts;
    uint32 defaultOffset = De1DefaultOffsetAndHwStsGet(self, channel, &hwSts);
    uint8  groupId = De1GroupIdGet(self, hwSts);

    switch (counterType)
        {
        case cAtPdhDe1CounterBpvExz:
            return De1CounterGet(mThis(self), mMethodsGet(self)->De1Counter2Reg(self, groupId), cAf6_reg_de1_counter_lcv_rei_index, defaultOffset, clear);

        case cAtPdhDe1CounterCrc:
            return De1CounterGet(mThis(self), mMethodsGet(self)->De1Counter2Reg(self, groupId), cAf6_reg_de1_counter_crc_fe_index, defaultOffset, clear);

        case cAtPdhDe1CounterFe:
            return De1CounterGet(mThis(self), mMethodsGet(self)->De1Counter1Reg(self, groupId), cAf6_reg_de1_counter_crc_fe_index, defaultOffset, clear);

        case cAtPdhDe1CounterRei:
            return De1CounterGet(mThis(self), mMethodsGet(self)->De1Counter1Reg(self, groupId), cAf6_reg_de1_counter_lcv_rei_index, defaultOffset, clear);

        default:
            return 0;
        }

    return 0;
    }

static uint32 VcDe3CounterGet(ThaModulePmc self, AtPdhChannel channel, uint16 counterType, eBool clear)
    {
    switch (counterType)
        {
        case cAtPdhDe3CounterRei:
            return De3CounterGet(self, channel, cAf6Reg_upen_pdh_de3cnt1_rw_Base, cAf6_reg_de3_counter_rei_fe_index, clear);

        case cAtPdhDe3CounterFBit:
            return De3CounterGet(self, channel, cAf6Reg_upen_pdh_de3cnt0_rw_Base, cAf6_reg_de3_counter_rei_fe_index, clear);

        case cAtPdhDe3CounterPBit:
            return De3CounterGet(self, channel, cAf6Reg_upen_pdh_de3cnt0_rw_Base, cAf6_reg_de3_counter_cbit_pbit_index, clear);

        case cAtPdhDe3CounterCPBit:
            return De3CounterGet(self, channel, cAf6Reg_upen_pdh_de3cnt1_rw_Base, cAf6_reg_de3_counter_cbit_pbit_index, clear);

        case cAtPdhDe3CounterBpvExz:
            return De3CounterGet(self, channel, cAf6Reg_upen_pdh_de3cnt0_rw_Base, cAf6_reg_de3_counter_lcv_index, clear);

        default:
            return 0;
        }

    return 0;
    }

static uint32 PwTxPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterGet(self, pw, cAf6Reg_upen_txpwcnt1_rc, cAf6_txpwcnt_packet_offset, clear);
    }

static uint32 PwTxBytesGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwByteCounterGet(self, pw, cAf6Reg_upen_txpwcntbyte_rc, clear);
    }

static uint32 PwTxLbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterGet(self, pw, cAf6Reg_upen_txpwcnt1_rc, cAf6_txpwcnt_lbit_packet_offset, clear);
    }

static uint32 PwTxRbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterGet(self, pw, cAf6Reg_upen_txpwcnt0_rc, cAf6_txpwcnt_rbit_packet_offset, clear);
    }

static uint32 PwTxMbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterGet(self, pw, cAf6Reg_upen_txpwcnt0_rc, cAf6_txpwcnt_nbit_packet_offset, clear);
    }

static uint32 PwTxPbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterGet(self, pw, cAf6Reg_upen_txpwcnt1_rc, cAf6_txpwcnt_pbit_packet_offset, clear);
    }

static uint32 PwTxNbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterGet(self, pw, cAf6Reg_upen_txpwcnt0_rc, cAf6_txpwcnt_nbit_packet_offset, clear);
    }

static uint32 PwRxPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterGet(self, pw, cAf6Reg_upen_rxpwcnt_info0_cnt1_rc, cAf6_rxpwcnt_packet_offset, clear);
    }

static uint32 PwRxMalformedPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterGet(self, pw, cAf6Reg_upen_rxpwcnt_info2_rc, cAf6_rxpwcnt_malformed_packet_offset, clear);
    }

static uint32 PwRxStrayPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterGet(self, pw, cAf6Reg_upen_rxpwcnt_info2_cnt1_rc, cAf6_rxpwcnt_stray_packet_offset, clear);
    }

static uint32 PwRxLbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterGet(self, pw, cAf6Reg_upen_rxpwcnt_info0_rc, cAf6_rxpwcnt_lbit_packet_offset, clear);
    }

static uint32 PwRxRbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterGet(self, pw, cAf6Reg_upen_rxpwcnt_info0_cnt1_rc, cAf6_rxpwcnt_rbit_packet_offset, clear);
    }

static uint32 PwRxMbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterGet(self, pw, cAf6Reg_upen_rxpwcnt_info0_cnt1_rc, cAf6_rxpwcnt_nbit_packet_offset, clear);
    }

static uint32 PwRxPbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterGet(self, pw, cAf6Reg_upen_rxpwcnt_info0_rc, cAf6_rxpwcnt_pbit_packet_offset, clear);
    }

static uint32 PwRxNbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterGet(self, pw, cAf6Reg_upen_rxpwcnt_info0_cnt1_rc, cAf6_rxpwcnt_nbit_packet_offset, clear);
    }

static uint32 PwRxBytesGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwByteCounterGet(self, pw, cAf6Reg_upen_rxpwcntbyte_rc, clear);
    }

static uint32 PwRxLostPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterGet(self, pw, cAf6Reg_upen_rxpwcnt_info1_cnt1_rc, cAf6_rxpwcnt_lost_packet_offset, clear);
    }

static uint32 PwRxReorderedPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterGet(self, pw, cAf6Reg_upen_rxpwcnt_info1_rc, cAf6_rxpwcnt_early_packet_offset, clear);
    }

static uint32 PwRxOutOfSeqDropedPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterGet(self, pw, cAf6Reg_upen_rxpwcnt_info0_rc, cAf6_rxpwcnt_late_packet_offset, clear);
    }

static uint32 PwRxJitBufOverrunGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterGet(self, pw, cAf6Reg_upen_rxpwcnt_info2_rc, cAf6_rxpwcnt_overrun_packet_offset, clear);
    }

static uint32 PwRxJitBufUnderrunGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterGet(self, pw, cAf6Reg_upen_rxpwcnt_info2_rc, cAf6_rxpwcnt_underrun_packet_offset, clear);
    }

static uint32 PwRxLopsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    uint32 lopstaCnt = PwCounterGet(self, pw, cAf6Reg_upen_rxpwcnt_info1_rc, cAf6_rxpwcnt_lopsta_packet_offset, clear);
    return lopstaCnt + PwCounterGet(self, pw, cAf6Reg_upen_rxpwcnt_info1_rc, cAf6_rxpwcnt_lopsyn_packet_offset, clear);
    }

static eBool EthPortCounterIsSupported(ThaModulePmc self, AtEthPort port, uint8 portType, uint16 counterType)
    {
    uint32 localAddress = mMethodsGet(self)->EthPortCounterLocalAddress(self, portType, counterType, cAtFalse);

    AtUnused(port);
    if (localAddress == cInvalidUint32)
        return cAtFalse;

    return cAtTrue;
    }

static uint8 EthPortTypeSw2Hw(uint8 portType)
    {
    switch (portType)
        {
        case cThaModulePmcEthPortTypeDccPort     : return 0;
        case cThaModulePmcEthPortType10GPort     : return 1;
        case cThaModulePmcEthPortTypeDimCtrlPort : return 2;
        case cThaModulePmcEthPortType2Dot5GPort  : return 3;
        default:
            return 0;
        }
    }

static uint32 Reg_MACGE_tx_type_packet_Address(ThaModulePmc self)
    {
    AtUnused(self);
    return 0x3B380U;
    }

static uint32 EthPortCounterCommonLocalAddress(ThaModulePmc self, uint8 portType, uint8 counterIndex)
    {
    return (mMethodsGet(self)->Reg_MACGE_tx_type_packet_Address(self) + (EthPortTypeSw2Hw(portType) * 0x20U) + counterIndex);
    }

static uint32 Reg_upen_cla0_ro_Address(ThaModulePmc self)
    {
    AtUnused(self);
    return cAf6Reg_upen_cla0_ro;
    }

static uint32 EthPortCounterOthersLocalAddress(ThaModulePmc self, uint8 portType, uint8 counterIndex)
    {
    /* Just 10G Backplane port is supported */
    if (portType != cThaModulePmcEthPortType10GPort)
        return cInvalidUint32;

    return (uint32)(mMethodsGet(self)->Reg_upen_cla0_ro_Address(self) + (uint32)(counterIndex * 0x2));
    }

static uint32 Reg_upen_cnttxoam_rc_Address(ThaModulePmc self)
    {
    AtUnused(self);
    return cAf6Reg_upen_cnttxoam_rc;
    }

static uint32 EthPortCounterLocalAddress(ThaModulePmc self, uint8 portType, uint16 counterType, eBool clear)
    {
    AtUnused(clear);

    switch (counterType)
        {
        case cAtEthPortCounterTxPackets             :
            return EthPortCounterCommonLocalAddress(self, portType, cThaEthTypePacket);
        case cAtEthPortCounterTxBytes               :
            return EthPortCounterCommonLocalAddress(self, portType, cThaEthTypeByte);
        case cAtEthPortCounterTxPacketsLen0_64      :
            return EthPortCounterCommonLocalAddress(self, portType, cThaEthTypePacketLessThan64);
        case cAtEthPortCounterTxPacketsLen65_127    :
            return EthPortCounterCommonLocalAddress(self, portType, cThaEthTypePacket65To128);
        case cAtEthPortCounterTxPacketsLen128_255   :
            return EthPortCounterCommonLocalAddress(self, portType, cThaEthTypePacket129To256);
        case cAtEthPortCounterTxPacketsLen256_511   :
            return EthPortCounterCommonLocalAddress(self, portType, cThaEthTypePacket257To512);
        case cAtEthPortCounterTxPacketsLen512_1024  :
            return EthPortCounterCommonLocalAddress(self, portType, cThaEthTypePacket513To1024);
        case cAtEthPortCounterTxPacketsLen1025_1528 :
            return EthPortCounterCommonLocalAddress(self, portType, cThaEthTypePacket1025To1528);
        case cAtEthPortCounterTxPacketsJumbo        :
            return EthPortCounterCommonLocalAddress(self, portType, cThaEthTypePacketJumbo);

        case cAtEthPortCounterRxPackets             :
            return EthPortCounterCommonLocalAddress(self, portType, mEthTypeRx(cThaEthTypePacket));
        case cAtEthPortCounterRxBytes               :
            return EthPortCounterCommonLocalAddress(self, portType, mEthTypeRx(cThaEthTypeByte));
        case cAtEthPortCounterRxPacketsLen0_64      :
            return EthPortCounterCommonLocalAddress(self, portType, mEthTypeRx(cThaEthTypePacketLessThan64));
        case cAtEthPortCounterRxPacketsLen65_127    :
            return EthPortCounterCommonLocalAddress(self, portType, mEthTypeRx(cThaEthTypePacket65To128));
        case cAtEthPortCounterRxPacketsLen128_255   :
            return EthPortCounterCommonLocalAddress(self, portType, mEthTypeRx(cThaEthTypePacket129To256));
        case cAtEthPortCounterRxPacketsLen256_511   :
            return EthPortCounterCommonLocalAddress(self, portType, mEthTypeRx(cThaEthTypePacket257To512));
        case cAtEthPortCounterRxPacketsLen512_1024  :
            return EthPortCounterCommonLocalAddress(self, portType, mEthTypeRx(cThaEthTypePacket513To1024));
        case cAtEthPortCounterRxPacketsLen1025_1528 :
            return EthPortCounterCommonLocalAddress(self, portType, mEthTypeRx(cThaEthTypePacket1025To1528));
        case cAtEthPortCounterRxPacketsJumbo        :
            return EthPortCounterCommonLocalAddress(self, portType, mEthTypeRx(cThaEthTypePacketJumbo));

        case cAtEthPortCounterTxOamPackets          :
            if (portType != cThaModulePmcEthPortType10GPort)
                return cInvalidUint32;
            return mMethodsGet(self)->Reg_upen_cnttxoam_rc_Address(self);
        case cAtEthPortCounterRxErrMefPackets       :
            return EthPortCounterOthersLocalAddress(self, portType, cThaEthTypeRxErrMefPacket);
        case cAtEthPortCounterRxErrEthHdrPackets    :
            return EthPortCounterOthersLocalAddress(self, portType, cThaEthTypeRxErrEthPacket);
        case cAtEthPortCounterRxErrPsnPackets       :
            return EthPortCounterOthersLocalAddress(self, portType, cThaEthTypeRxErrPsnPacket);
        case cAtEthPortCounterRxPhysicalError       :
            return EthPortCounterOthersLocalAddress(self, portType, cThaEthTypeRxErrPhyPacket);

        default:
            return cInvalidUint32;
        }
    }

static uint32 HwCounterRead2Clear(ThaModulePmc self, uint32 address, eBool clear)
    {
    uint32 regVal = mModuleHwRead(self, address);

    if (clear)
        mModuleHwWrite(self, address, 0);

    return regVal;
    }

static uint32 EthPortCountersGet(ThaModulePmc pmcModule, AtEthPort ethPort, uint8 portType, uint16 counterType, eBool clear)
    {
    uint32 address;
    uint32 localAddress = mMethodsGet(pmcModule)->EthPortCounterLocalAddress(pmcModule, portType, counterType, clear);
    if (localAddress == cInvalidUint32)
        return 0;

    AtUnused(ethPort);
    address = AddressWithLocalAddress(pmcModule, localAddress);

    return mMethodsGet(pmcModule)->HwCounterRead2Clear(pmcModule, address, clear);
    }

static eAtRet TickModeSet(ThaModulePmc self, eThaModulePmcTickMode mode)
    {
    AtUnused(self);
    return (mode == cThaModulePmcTickModeAuto) ? cAtOk : cAtErrorModeNotSupport;
    }

static eThaModulePmcTickMode TickModeGet(ThaModulePmc self)
    {
    AtUnused(self);
    return cThaModulePmcTickModeAuto;
    }

static eAtRet Tick(ThaModulePmc self)
    {
    AtUnused(self);
    return cAtOk;
    }

static const char *TypeString(AtModule self)
    {
    AtUnused(self);
    return "pmc";
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = cAtOk;

    ret |= m_AtModuleMethods->Init(self);
    ret |= ThaModulePmcTickModeSet(mThis(self), cThaModulePmcTickModeAuto);

    return ret;
    }

static eBool HasRegister(AtModule self, uint32 localAddress)
    {
    AtUnused(self);
    return mInRange(localAddress, 0x1F00000, 0x1FFFFFF) ? cAtTrue : cAtFalse;
    }

static uint32 *HoldRegistersGet(AtModule self, uint16 *numberOfHoldRegisters)
    {
    if (mMethodsGet(mThis(self))->HasLongAccess(mThis(self)))
        {
        static uint32 holdRegisters[] = {cAf6Reg_hold_reg0 + cAf6Reg_Pmc_Base_Address};
        AtUnused(self);

        if (numberOfHoldRegisters)
            *numberOfHoldRegisters = mCount(holdRegisters);

        return holdRegisters;
        }

    return m_AtModuleMethods->HoldRegistersGet(self, numberOfHoldRegisters);
    }

static AtLongRegisterAccess LongRegisterAccessCreate(AtModule self)
    {
    if (mMethodsGet(mThis(self))->HasLongAccess(mThis(self)))
        {
        uint16 numHoldRegisters;
        uint32 *holdRegisters = AtModuleHoldRegistersGet(self, &numHoldRegisters);

        return AtDefaultLongRegisterAccessNew(holdRegisters, numHoldRegisters, holdRegisters, numHoldRegisters);
        }

    return m_AtModuleMethods->LongRegisterAccessCreate(self);
    }

static eBool HasLongAccess(ThaModulePmc self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 GfpCounterAddress(ThaModulePmc self, uint32 localAddr)
    {
    return ThaModulePmcPartBaseAddress(self, 1) + localAddr;
    }

static uint32 GfpCounterGet(ThaModulePmc self, AtGfpChannel channel, uint32 localAddress, uint32 cntIndex, uint32 cntPosId, eBool clear)
    {
    uint32 regAddr = GfpCounterAddress(self, localAddress) + mMethodsGet(self)->GfpCounterOffset(self, channel, cntIndex, cntPosId, clear);
    return mChannelHwRead(channel, regAddr, cThaModulePmc);
    }

static uint32 TxGfpCounterGet(ThaModulePmc self, AtGfpChannel channel, uint32 cntIndex, uint32 cntPosId, eBool clear)
    {
    return GfpCounterGet(self, channel, cAf6Reg_upen_enc_ho, cntIndex, cntPosId, clear);
    }

static uint32 RxGfpCounterGet(ThaModulePmc self, AtGfpChannel channel, uint32 cntIndex, uint32 cntPosId, eBool clear)
    {
    return GfpCounterGet(self, channel, cAf6Reg_upen_dec_ho, cntIndex, cntPosId, clear);
    }

static uint32 GfpTxBytesGet(ThaModulePmc self, AtGfpChannel channel, eBool clear)
    {
    return TxGfpCounterGet(self, channel, cGfpEncapBytesIndex, clear);
    }

static uint32 GfpTxIdleFramesGet(ThaModulePmc self, AtGfpChannel channel, eBool clear)
    {
    return TxGfpCounterGet(self, channel, cGfpEncapIdlesIndex, clear);
    }

static uint32 GfpTxFramesGet(ThaModulePmc self, AtGfpChannel channel, eBool clear)
    {
    return TxGfpCounterGet(self, channel, cGfpEncapFramesIndex, clear);
    }

static uint32 GfpTxCmfGet(ThaModulePmc self, AtGfpChannel channel, eBool clear)
    {
    return TxGfpCounterGet(self, channel, cGfpEncapCmfIndex, clear);
    }

static uint32 GfpTxCsfGet(ThaModulePmc self, AtGfpChannel channel, eBool clear)
    {
    return TxGfpCounterGet(self, channel, cGfpEncapCsfIndex, clear);
    }

static uint32 GfpRxBytesGet(ThaModulePmc self, AtGfpChannel channel, eBool clear)
    {
    return RxGfpCounterGet(self, channel, cGfpDecapBytesIndex, clear);
    }

static uint32 GfpRxIdleFramesGet(ThaModulePmc self, AtGfpChannel channel, eBool clear)
    {
    return RxGfpCounterGet(self, channel, cGfpDecapIdleFramesIndex, clear);
    }

static uint32 GfpRxFramesGet(ThaModulePmc self, AtGfpChannel channel, eBool clear)
    {
    return RxGfpCounterGet(self, channel, cGfpDecapFramesIndex, clear);
    }

static uint32 GfpRxCmfGet(ThaModulePmc self, AtGfpChannel channel, eBool clear)
    {
    return RxGfpCounterGet(self, channel, cGfpDecapCmfIndex, clear);
    }

static uint32 GfpCounterOffset(ThaModulePmc self, AtGfpChannel channel, uint32 cntIndex, uint32 cntPosId, eBool clear)
    {
    AtUnused(self);
    return (uint32)(0x400 * cntIndex + 0x100 * cntPosId + 0x80 * mBoolToBin(clear) + AtChannelHwIdGet((AtChannel)channel));
    }

static uint32 GfpRxCHecCorrected(ThaModulePmc self, AtGfpChannel channel, eBool clear)
    {
    /* C-HEC corrected error is sum of:
     * - 2-byte PLI corrected count and
     * - 2-byte C-HEC field corrected count
     */
    return RxGfpCounterGet(self, channel, cGfpDecapPliCorrErrIndex, clear) +
           RxGfpCounterGet(self, channel, cGfpDecapCHecCorrErrIndex, clear);
    }

static uint32 GfpRxCHecUncorrErrorGet(ThaModulePmc self, AtGfpChannel channel, eBool clear)
    {
    return RxGfpCounterGet(self, channel, cGfpDecapCHecUCorrErrIndex, clear);
    }

static uint32 GfpRxTHecCorrected(ThaModulePmc self, AtGfpChannel channel, eBool clear)
    {
    /* T-HEC corrected error is sum of:
     * - 2-byte Payload Type corrected count and
     * - 2-byte T-HEC field corrected count
     */
    return RxGfpCounterGet(self, channel, cGfpDecapTypeCorrErrIndex, clear) +
           RxGfpCounterGet(self, channel, cGfpDecapTHecCorrErrIndex, clear);
    }

static uint32 GfpRxTHecUncorrErrorGet(ThaModulePmc self, AtGfpChannel channel, eBool clear)
    {
    return RxGfpCounterGet(self, channel, cGfpDecapTHecUCorrErrIndex, clear);
    }

static uint32 GfpRxFcsError(ThaModulePmc self, AtGfpChannel channel, eBool clear)
    {
    return RxGfpCounterGet(self, channel, cGfpDecapFcsErrIndex, clear);
    }

static uint32 GfpRxDrops(ThaModulePmc self, AtGfpChannel channel, eBool clear)
    {
    return RxGfpCounterGet(self, channel, cGfpDecapDropFramesIndex, clear);
    }

static uint32 GfpRxUpmGet(ThaModulePmc self, AtGfpChannel channel, eBool clear)
    {
    return RxGfpCounterGet(self, channel, cGfpDecapUpmIndex, clear);
    }

static uint32 GfpRxEHecErrorGet(ThaModulePmc self, AtGfpChannel channel, eBool clear)
    {
    return RxGfpCounterGet(self, channel, cGfpDecapEHecErrIndex, clear);
    }

static uint32 EthFlowCounterAddress(ThaModulePmc self, uint32 localAddr)
    {
    return ThaModulePmcPartBaseAddress(self, 1) + localAddr;
    }

static uint32 EthFlowCounterGet(ThaModulePmc self, AtEthFlow channel, uint32 localAddress, uint32 cntPosId, uint32 cntIndex, eBool clear)
    {
    uint32 regAddr = EthFlowCounterAddress(self, localAddress) + mMethodsGet(self)->EthFlowCounterOffset(self, channel, cntPosId, cntIndex, clear);
    return mChannelHwRead(channel, regAddr, cThaModulePmc);
    }

static uint32 TxEthFlowCounterGet(ThaModulePmc self, AtEthFlow channel, uint32 cntPosId, uint32 cntIndex, eBool clear)
    {
    return EthFlowCounterGet(self, channel, cAf6Reg_upen_txflow, cntPosId, cntIndex, clear);
    }

static uint32 RxEthFlowCounterGet(ThaModulePmc self, AtEthFlow channel, uint32 cntPosId, uint32 cntIndex, eBool clear)
    {
    return EthFlowCounterGet(self, channel, cAf6Reg_upen_rxflow, cntPosId, cntIndex, clear);
    }

static uint32 EthFlowTxBytes(ThaModulePmc self, AtEthFlow channel, eBool clear)
    {
    return TxEthFlowCounterGet(self, channel, cFlowCntBytesPosId, cFlowCntBytesIndex, clear);
    }

static uint32 EthFlowTxPackets(ThaModulePmc self, AtEthFlow channel, eBool clear)
    {
    return TxEthFlowCounterGet(self, channel, cFlowCntPktsPosId, cFlowCntPktsIndex, clear);
    }

static uint32 EthFlowTxFragmentPackets(ThaModulePmc self, AtEthFlow channel, eBool clear)
    {
    return TxEthFlowCounterGet(self, channel, cFlowCntFragPosId, cFlowCntFragIndex, clear);
    }

static uint32 EthFlowTxNullFragmentPackets(ThaModulePmc self, AtEthFlow channel, eBool clear)
    {
    return TxEthFlowCounterGet(self, channel, cFlowCntNullFragPosId, cFlowCntNullFragIndex, clear);
    }

static uint32 EthFlowTxDiscardPackets(ThaModulePmc self, AtEthFlow channel, eBool clear)
    {
    return TxEthFlowCounterGet(self, channel, cFlowCntDisPktsPosId, cFlowCntDisPktsIndex, clear);
    }

static uint32 EthFlowTxDiscardBytes(ThaModulePmc self, AtEthFlow channel, eBool clear)
    {
    return TxEthFlowCounterGet(self, channel, cFlowCntDisBytesPosId, cFlowCntDisBytesIndex, clear);
    }

static uint32 EthFlowTxBECNPackets(ThaModulePmc self, AtEthFlow channel, eBool clear)
    {
    return TxEthFlowCounterGet(self, channel, cFlowCntBECNPosId, cFlowCntBECNIndex, clear);
    }

static uint32 EthFlowTxFECNPackets(ThaModulePmc self, AtEthFlow channel, eBool clear)
    {
    return TxEthFlowCounterGet(self, channel, cFlowCntFECNPosId, cFlowCntFECNIndex, clear);
    }

static uint32 EthFlowTxDEPackets(ThaModulePmc self, AtEthFlow channel, eBool clear)
    {
    return TxEthFlowCounterGet(self, channel, cFlowCntDEPosId, cFlowCntDEIndex, clear);
    }

static uint32 EthFlowRxBytes(ThaModulePmc self, AtEthFlow channel, eBool clear)
    {
    return RxEthFlowCounterGet(self, channel, cFlowCntBytesPosId, cFlowCntBytesIndex, clear);
    }

static uint32 EthFlowRxPackets(ThaModulePmc self, AtEthFlow channel, eBool clear)
    {
    return RxEthFlowCounterGet(self, channel, cFlowCntPktsPosId, cFlowCntPktsIndex, clear);
    }

static uint32 EthFlowRxFragmentPackets(ThaModulePmc self, AtEthFlow channel, eBool clear)
    {
    return RxEthFlowCounterGet(self, channel, cFlowCntFragPosId, cFlowCntFragIndex, clear);
    }

static uint32 EthFlowRxNullFragmentPackets(ThaModulePmc self, AtEthFlow channel, eBool clear)
    {
    return RxEthFlowCounterGet(self, channel, cFlowCntNullFragPosId, cFlowCntNullFragIndex, clear);
    }

static uint32 EthFlowRxDiscardPackets(ThaModulePmc self, AtEthFlow channel, eBool clear)
    {
    return RxEthFlowCounterGet(self, channel, cFlowCntDisPktsPosId, cFlowCntDisPktsIndex, clear);
    }

static uint32 EthFlowRxDiscardBytes(ThaModulePmc self, AtEthFlow channel, eBool clear)
    {
    return RxEthFlowCounterGet(self, channel, cFlowCntDisBytesPosId, cFlowCntDisBytesIndex, clear);
    }

static uint32 EthFlowRxBECNPackets(ThaModulePmc self, AtEthFlow channel, eBool clear)
    {
    return RxEthFlowCounterGet(self, channel, cFlowCntBECNPosId, cFlowCntBECNIndex, clear);
    }

static uint32 EthFlowRxFECNPackets(ThaModulePmc self, AtEthFlow channel, eBool clear)
    {
    return RxEthFlowCounterGet(self, channel, cFlowCntFECNPosId, cFlowCntFECNIndex, clear);
    }

static uint32 EthFlowRxDEPackets(ThaModulePmc self, AtEthFlow channel, eBool clear)
    {
    return RxEthFlowCounterGet(self, channel, cFlowCntDEPosId, cFlowCntDEIndex, clear);
    }

static uint32 EthFlowCounterOffset(ThaModulePmc self, AtEthFlow channel, uint32 cntPosId, uint32 cntIndex, eBool clear)
    {
    AtUnused(self);
    return (uint32)(65536U * cntIndex + 16384U * cntPosId + 8192U * mBoolToBin(clear) + AtChannelHwIdGet((AtChannel)channel));
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(self), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, TypeString);
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, HasRegister);
        mMethodOverride(m_AtModuleOverride, LongRegisterAccessCreate);
        mMethodOverride(m_AtModuleOverride, HoldRegistersGet);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    }

static void MethodsInit(ThaModulePmc self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, BaseAddress);
        mMethodOverride(m_methods, HasLongAccess);
    	mMethodOverride(m_methods, PwCounterOffset);
    	mMethodOverride(m_methods, PwByteCounterOffset);
    	mMethodOverride(m_methods, LineCounterOffset);
    	mMethodOverride(m_methods, AuVcRegOffsetCoef);
    	mMethodOverride(m_methods, Vc1xRegOffsetCoef);
    	mMethodOverride(m_methods, De3RegOffsetCoef);
        mMethodOverride(m_methods, AuVcPohCounter1Reg);
        mMethodOverride(m_methods, AuVcPohCounter2Reg);
        mMethodOverride(m_methods, AuVcPointerCounter1Reg);
        mMethodOverride(m_methods, AuVcPointerCounter2Reg);
        mMethodOverride(m_methods, Vc1xPohCounter1Reg);
        mMethodOverride(m_methods, Vc1xPohCounter2Reg);
        mMethodOverride(m_methods, VcTu1xCounterGroupDivided);
        mMethodOverride(m_methods, LineSupportedBlockCounters);
        mMethodOverride(m_methods, De1Counter1Reg);
        mMethodOverride(m_methods, De1Counter2Reg);
        mMethodOverride(m_methods, LineCounter1Reg);
        mMethodOverride(m_methods, LineCounter2Reg);
        mMethodOverride(m_methods, SdhLineCounterGet);
        mMethodOverride(m_methods, SdhLineBlockErrorCounterGet);
        mMethodOverride(m_methods, AuVcPointerCounterGet);
        mMethodOverride(m_methods, AuVcPohCounterGet);
        mMethodOverride(m_methods, TuVc1xPointerCounterGet);
        mMethodOverride(m_methods, VcDe1CounterGet);
        mMethodOverride(m_methods, VcDe3CounterGet);

        mMethodOverride(m_methods, PwTxPacketsGet);
        mMethodOverride(m_methods, PwTxBytesGet);
        mMethodOverride(m_methods, PwTxLbitPacketsGet);
        mMethodOverride(m_methods, PwTxRbitPacketsGet);
        mMethodOverride(m_methods, PwTxMbitPacketsGet);
        mMethodOverride(m_methods, PwTxPbitPacketsGet);
        mMethodOverride(m_methods, PwTxNbitPacketsGet);
        mMethodOverride(m_methods, PwRxPacketsGet);
        mMethodOverride(m_methods, PwRxMalformedPacketsGet);
        mMethodOverride(m_methods, PwRxStrayPacketsGet);
        mMethodOverride(m_methods, PwRxLbitPacketsGet);
        mMethodOverride(m_methods, PwRxRbitPacketsGet);
        mMethodOverride(m_methods, PwRxMbitPacketsGet);
        mMethodOverride(m_methods, PwRxPbitPacketsGet);
        mMethodOverride(m_methods, PwRxNbitPacketsGet);
        mMethodOverride(m_methods, PwRxBytesGet);
        mMethodOverride(m_methods, PwRxLostPacketsGet);
        mMethodOverride(m_methods, PwRxReorderedPacketsGet);
        mMethodOverride(m_methods, PwRxOutOfSeqDropedPacketsGet);
        mMethodOverride(m_methods, PwRxJitBufOverrunGet);
        mMethodOverride(m_methods, PwRxJitBufUnderrunGet);
        mMethodOverride(m_methods, PwRxLopsGet);

        mMethodOverride(m_methods, Reg_upen_cnttxoam_rc_Address);
        mMethodOverride(m_methods, Reg_MACGE_tx_type_packet_Address);
        mMethodOverride(m_methods, Reg_upen_cla0_ro_Address);
        mMethodOverride(m_methods, EthPortCounterLocalAddress);
        mMethodOverride(m_methods, HwCounterRead2Clear);

        mMethodOverride(m_methods, TickModeSet);
        mMethodOverride(m_methods, TickModeGet);
        mMethodOverride(m_methods, Tick);

        mMethodOverride(m_methods, AuVcDefaultOffset);
        mMethodOverride(m_methods, Vc1xDefaultOffset);
        mMethodOverride(m_methods, De3DefaultOffset);
        mMethodOverride(m_methods, De1DefaultOffset);

        mMethodOverride(m_methods, GfpTxBytesGet);
        mMethodOverride(m_methods, GfpTxIdleFramesGet);
        mMethodOverride(m_methods, GfpTxFramesGet);
        mMethodOverride(m_methods, GfpTxCmfGet);
        mMethodOverride(m_methods, GfpTxCsfGet);

        mMethodOverride(m_methods, GfpRxBytesGet);
        mMethodOverride(m_methods, GfpRxIdleFramesGet);
        mMethodOverride(m_methods, GfpRxFramesGet);
        mMethodOverride(m_methods, GfpRxCmfGet);
        mMethodOverride(m_methods, GfpCounterOffset);
        mMethodOverride(m_methods, GfpRxCHecCorrected);
        mMethodOverride(m_methods, GfpRxCHecUncorrErrorGet);
        mMethodOverride(m_methods, GfpRxTHecCorrected);
        mMethodOverride(m_methods, GfpRxTHecUncorrErrorGet);
        mMethodOverride(m_methods, GfpRxFcsError);
        mMethodOverride(m_methods, GfpRxDrops);
        mMethodOverride(m_methods, GfpRxUpmGet);
        mMethodOverride(m_methods, GfpRxEHecErrorGet);

        mMethodOverride(m_methods, EthFlowTxBytes);
        mMethodOverride(m_methods, EthFlowTxPackets);
        mMethodOverride(m_methods, EthFlowTxFragmentPackets);
        mMethodOverride(m_methods, EthFlowTxNullFragmentPackets);
        mMethodOverride(m_methods, EthFlowTxDiscardPackets);
        mMethodOverride(m_methods, EthFlowTxDiscardBytes);
        mMethodOverride(m_methods, EthFlowTxBECNPackets);
        mMethodOverride(m_methods, EthFlowTxFECNPackets);
        mMethodOverride(m_methods, EthFlowTxDEPackets);

        mMethodOverride(m_methods, EthFlowRxBytes);
        mMethodOverride(m_methods, EthFlowRxPackets);
        mMethodOverride(m_methods, EthFlowRxFragmentPackets);
        mMethodOverride(m_methods, EthFlowRxNullFragmentPackets);
        mMethodOverride(m_methods, EthFlowRxDiscardPackets);
        mMethodOverride(m_methods, EthFlowRxDiscardBytes);
        mMethodOverride(m_methods, EthFlowRxBECNPackets);
        mMethodOverride(m_methods, EthFlowRxFECNPackets);
        mMethodOverride(m_methods, EthFlowRxDEPackets);
        mMethodOverride(m_methods, EthFlowCounterOffset);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaModulePmc);
    }

AtModule ThaModulePmcObjectInit(AtModule self, AtDevice device)
	{
	/* Clear memory */
	AtOsal osal = AtSharedDriverOsalGet();
	mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

	/* Super constructor */
	if (AtModuleObjectInit(self, cThaModulePmc, device) == NULL)
		return NULL;

	/* Setup class */
	Override(self);
	MethodsInit((ThaModulePmc)self);
	m_methodsInit = 1;

	return self;
	}

AtModule ThaModulePmcNew(AtDevice device)
	{
	/* Allocate memory */
	AtOsal osal = AtSharedDriverOsalGet();
	AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
	if (newModule == NULL)
		return NULL;

	/* Construct it */
	return ThaModulePmcObjectInit(newModule, device);
	}

uint32 ThaModulePmcPwTxPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
	if (self == NULL || pw == NULL)
		return 0;
	return mMethodsGet(self)->PwTxPacketsGet(self, pw, clear);
    }

uint32 ThaModulePmcPwTxBytesGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
	if (self == NULL || pw == NULL)
		return 0;
    return mMethodsGet(self)->PwTxBytesGet(self, pw, clear);
    }

uint32 ThaModulePmcPwTxLbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
	if (self == NULL || pw == NULL)
		return 0;
	return mMethodsGet(self)->PwTxLbitPacketsGet(self, pw, clear);
    }

uint32 ThaModulePmcPwTxRbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
	if (self == NULL || pw == NULL)
		return 0;
	return mMethodsGet(self)->PwTxRbitPacketsGet(self, pw, clear);
    }

uint32 ThaModulePmcPwTxMbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
	if (self == NULL || pw == NULL)
		return 0;
	return mMethodsGet(self)->PwTxMbitPacketsGet(self, pw, clear);
    }

uint32 ThaModulePmcPwTxPbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
	if (self == NULL || pw == NULL)
		return 0;
	return mMethodsGet(self)->PwTxPbitPacketsGet(self, pw, clear);
    }

uint32 ThaModulePmcPwTxNbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
	if (self == NULL || pw == NULL)
		return 0;
	return mMethodsGet(self)->PwTxNbitPacketsGet(self, pw, clear);
    }

uint32 ThaModulePmcPwRxPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
	if (self == NULL || pw == NULL)
		return 0;
	return mMethodsGet(self)->PwRxPacketsGet(self, pw, clear);
    }

uint32 ThaModulePmcPwRxMalformedPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
	if (self == NULL || pw == NULL)
		return 0;
	return mMethodsGet(self)->PwRxMalformedPacketsGet(self, pw, clear);
    }

uint32 ThaModulePmcPwRxStrayPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
	if (self == NULL || pw == NULL)
		return 0;
	return mMethodsGet(self)->PwRxStrayPacketsGet(self, pw, clear);
    }

uint32 ThaModulePmcPwRxLbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
	if (self == NULL || pw == NULL)
		return 0;
	return mMethodsGet(self)->PwRxLbitPacketsGet(self, pw, clear);
    }

uint32 ThaModulePmcPwRxRbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
	if (self == NULL || pw == NULL)
		return 0;
	return mMethodsGet(self)->PwRxRbitPacketsGet(self, pw, clear);
    }

uint32 ThaModulePmcPwRxMbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
	if (self == NULL || pw == NULL)
		return 0;
	return mMethodsGet(self)->PwRxMbitPacketsGet(self, pw, clear);
    }

uint32 ThaModulePmcPwRxNbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
	if (self == NULL || pw == NULL)
		return 0;
	return mMethodsGet(self)->PwRxNbitPacketsGet(self, pw, clear);
    }

uint32 ThaModulePmcPwRxPbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
	if (self == NULL || pw == NULL)
		return 0;
    return mMethodsGet(self)->PwRxPbitPacketsGet(self, pw, clear);
    }

uint32 ThaModulePmcPwRxBytesGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
	if (self == NULL || pw == NULL)
		return 0;
    return mMethodsGet(self)->PwRxBytesGet(self, pw, clear);
    }

uint32 ThaModulePmcPwRxReorderedPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
	if (self == NULL || pw == NULL)
		return 0;
	return mMethodsGet(self)->PwRxReorderedPacketsGet(self, pw, clear);
    }

uint32 ThaModulePmcPwRxLostPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
	if (self == NULL || pw == NULL)
		return 0;
	return mMethodsGet(self)->PwRxLostPacketsGet(self, pw, clear);
    }

uint32 ThaModulePmcPwRxOutOfSeqDropedPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
	if (self == NULL || pw == NULL)
		return 0;
    return mMethodsGet(self)->PwRxOutOfSeqDropedPacketsGet(self, pw, clear);
    }

uint32 ThaModulePmcPwRxJitBufOverrunGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
	if (self == NULL || pw == NULL)
		return 0;
	return mMethodsGet(self)->PwRxJitBufOverrunGet(self, pw, clear);
    }

uint32 ThaModulePmcPwRxJitBufUnderrunGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
	if (self == NULL || pw == NULL)
		return 0;
	return mMethodsGet(self)->PwRxJitBufUnderrunGet(self, pw, clear);
    }

uint32 ThaModulePmcPwRxLopsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
	if (self == NULL || pw == NULL)
		return 0;
	return mMethodsGet(self)->PwRxLopsGet(self, pw, clear);
    }

eBool ThaSdhChannelShouldGetCounterFromPmc(AtChannel self)
    {
    ThaModuleSdh sdhModule = (ThaModuleSdh)AtDeviceModuleGet(AtChannelDeviceGet(self), cAtModuleSdh);

    if (ThaModuleSdhDebugCountersModuleGet(sdhModule) == cThaModulePmc)
        return cAtTrue;

    return cAtFalse;
    }

eBool ThaTdmChannelShouldGetCounterFromPmc(AtChannel self)
    {
    ThaModulePdh pdhModule = (ThaModulePdh)AtDeviceModuleGet(AtChannelDeviceGet(self), cAtModulePdh);

    if (ThaModulePdhDebugCountersModuleGet(pdhModule) == cThaModulePmc)
        return cAtTrue;

    return cAtFalse;
    }

eBool ThaEthPortShouldGetCounterFromPmc(AtChannel self)
    {
    ThaModuleEth ethModule = (ThaModuleEth)AtDeviceModuleGet(AtChannelDeviceGet(self), cAtModuleEth);

    if (ThaModuleEthDebugCountersModuleGet(ethModule) == cThaModulePmc)
        return cAtTrue;

    return cAtFalse;
    }

uint32 ThaModulePmcSdhLineCounterGet(AtModule self, AtSdhChannel channel, uint16 counterType, eBool clear)
    {
    if (self)
        return mMethodsGet(mThis(self))->SdhLineCounterGet(mThis(self), channel, counterType, clear);
    return cInvalidUint32;
    }

uint32 ThaModulePmcSdhLineBlockErrorCounterGet(AtModule self, AtSdhChannel channel, uint16 counterType, eBool clear)
    {
    if (self)
        return mMethodsGet(mThis(self))->SdhLineBlockErrorCounterGet(mThis(self), channel, counterType, clear);
    return cInvalidUint32;
    }

uint32 ThaModulePmcSdhAuVcPohCounterGet(AtModule self, AtSdhChannel channel, uint16 counterType, eBool clear)
    {
    if (self && channel)
        return mMethodsGet(mThis(self))->AuVcPohCounterGet(mThis(self), channel, counterType, clear);
    return cAtErrorNullPointer;
    }

uint32 ThaModulePmcSdhAuPointerCounterGet(AtModule self, AtSdhChannel channel, uint16 counterType, eBool clear)
    {
    if (self && channel)
        return mMethodsGet(mThis(self))->AuVcPointerCounterGet(mThis(self), channel, counterType, clear);
    return cInvalidUint32;
    }

uint32 ThaModulePmcSdhTuVc1xCounterGet(AtModule self, AtSdhChannel channel, uint16 counterType, eBool clear)
    {
    if (self && channel)
        return mMethodsGet(mThis(self))->TuVc1xPointerCounterGet(mThis(self), channel, counterType, clear);
    return cInvalidUint32;
    }

uint32 ThaModulePmcSdhVcDe1CounterGet(AtModule self, AtPdhChannel channel, uint16 counterType, eBool clear)
    {
    if (self && channel && (counterType == cAtPdhDe1CounterRxCs))
        return ThaPdhDe1RxSlipBuferCounter(channel, clear);

    if (self && channel)
        return mMethodsGet(mThis(self))->VcDe1CounterGet(mThis(self), channel, counterType, clear);

    return cInvalidUint32;
    }

uint32 ThaModulePmcSdhVcDe3CounterGet(AtModule self, AtPdhChannel channel, uint16 counterType, eBool clear)
    {
    if (self && channel)
        return mMethodsGet(mThis(self))->VcDe3CounterGet(mThis(self), channel, counterType, clear);
    return cInvalidUint32;
    }

uint32 ThaModulePmcEthPortCountersGet(ThaModulePmc self, AtEthPort port, uint8 portType, uint16 counterType, eBool clear)
    {
    if (self && port)
        return EthPortCountersGet(self, port, portType, counterType, clear);

    return 0;
    }

eBool ThaModulePmcEthPortCounterIsSupported(ThaModulePmc self, AtEthPort port, uint8 portType, uint16 counterType)
    {
    if (self && port)
        return EthPortCounterIsSupported(self, port, portType, counterType);

    return cAtFalse;
    }

eAtRet ThaModulePmcTickModeSet(ThaModulePmc self, eThaModulePmcTickMode mode)
    {
    if (self)
        return mMethodsGet(self)->TickModeSet(self, mode);
    return cAtErrorNullPointer;
    }

eThaModulePmcTickMode ThaModulePmcTickModeGet(ThaModulePmc self)
    {
    if (self)
        return mMethodsGet(self)->TickModeGet(self);
    return cThaModulePmcTickModeInvalid;
    }

eAtRet ThaModulePmcTick(ThaModulePmc self)
    {
    if (self)
        return mMethodsGet(self)->Tick(self);
    return cAtErrorNullPointer;
    }

uint32 ThaModulePmcBaseAddress(ThaModulePmc self)
	{
    static const uint8 cDefaultPart = 0;
    return ThaModulePmcPartBaseAddress(self, cDefaultPart);
	}

uint32 ThaModulePmcPartBaseAddress(ThaModulePmc self, uint8 partId)
    {
    if (self)
        return mMethodsGet(self)->BaseAddress(self, partId);
    return cInvalidUint32;
    }

uint32 ThaModulePmcGfpTxBytesGet(ThaModulePmc self, AtGfpChannel channel, eBool clear)
    {
    if (self == NULL || channel == NULL)
        return 0;
    return mMethodsGet(self)->GfpTxBytesGet(self, channel, clear);
    }

uint32 ThaModulePmcGfpTxIdleFramesGet(ThaModulePmc self, AtGfpChannel channel, eBool clear)
    {
    if (self == NULL || channel == NULL)
        return 0;
    return mMethodsGet(self)->GfpTxIdleFramesGet(self, channel, clear);
    }

uint32 ThaModulePmcGfpTxFramesGet(ThaModulePmc self, AtGfpChannel channel, eBool clear)
    {
    if (self == NULL || channel == NULL)
        return 0;
    return mMethodsGet(self)->GfpTxFramesGet(self, channel, clear);
    }

uint32 ThaModulePmcGfpTxCmfGet(ThaModulePmc self, AtGfpChannel channel, eBool clear)
    {
    if (self == NULL || channel == NULL)
        return 0;
    return mMethodsGet(self)->GfpTxCmfGet(self, channel, clear);
    }

uint32 ThaModulePmcGfpTxCsfGet(ThaModulePmc self, AtGfpChannel channel, eBool clear)
    {
    if (self == NULL || channel == NULL)
        return 0;
    return mMethodsGet(self)->GfpTxCsfGet(self, channel, clear);
    }

uint32 ThaModulePmcGfpRxBytesGet(ThaModulePmc self, AtGfpChannel channel, eBool clear)
    {
    if (self == NULL || channel == NULL)
        return 0;
    return mMethodsGet(self)->GfpRxBytesGet(self, channel, clear);
    }

uint32 ThaModulePmcGfpRxIdleFramesGet(ThaModulePmc self, AtGfpChannel channel, eBool clear)
    {
    if (self == NULL || channel == NULL)
        return 0;
    return mMethodsGet(self)->GfpRxIdleFramesGet(self, channel, clear);
    }

uint32 ThaModulePmcGfpRxFramesGet(ThaModulePmc self, AtGfpChannel channel, eBool clear)
    {
    if (self == NULL || channel == NULL)
        return 0;
    return mMethodsGet(self)->GfpRxFramesGet(self, channel, clear);
    }

uint32 ThaModulePmcGfpRxCmfGet(ThaModulePmc self, AtGfpChannel channel, eBool clear)
    {
    if (self == NULL || channel == NULL)
        return 0;
    return mMethodsGet(self)->GfpRxCmfGet(self, channel, clear);
    }

uint32 ThaModulePmcGfpRxCHecCorrected(ThaModulePmc self, AtGfpChannel channel, eBool clear)
    {
    if (self == NULL || channel == NULL)
        return 0;
    return mMethodsGet(self)->GfpRxCHecCorrected(self, channel, clear);
    }

uint32 ThaModulePmcGfpRxCHecUncorrErrorGet(ThaModulePmc self, AtGfpChannel channel, eBool clear)
    {
    if (self == NULL || channel == NULL)
        return 0;
    return mMethodsGet(self)->GfpRxCHecUncorrErrorGet(self, channel, clear);
    }

uint32 ThaModulePmcGfpRxTHecCorrected(ThaModulePmc self, AtGfpChannel channel, eBool clear)
    {
    if (self == NULL || channel == NULL)
        return 0;
    return mMethodsGet(self)->GfpRxTHecCorrected(self, channel, clear);
    }

uint32 ThaModulePmcGfpRxTHecUncorrErrorGet(ThaModulePmc self, AtGfpChannel channel, eBool clear)
    {
    if (self == NULL || channel == NULL)
        return 0;
    return mMethodsGet(self)->GfpRxTHecUncorrErrorGet(self, channel, clear);
    }

uint32 ThaModulePmcGfpRxFcsError(ThaModulePmc self, AtGfpChannel channel, eBool clear)
    {
    if (self == NULL || channel == NULL)
        return 0;
    return mMethodsGet(self)->GfpRxFcsError(self, channel, clear);
    }

uint32 ThaModulePmcGfpRxDrops(ThaModulePmc self, AtGfpChannel channel, eBool clear)
    {
    if (self == NULL || channel == NULL)
        return 0;
    return mMethodsGet(self)->GfpRxDrops(self, channel, clear);
    }

uint32 ThaModulePmcGfpRxUpmGet(ThaModulePmc self, AtGfpChannel channel, eBool clear)
    {
    if (self == NULL || channel == NULL)
        return 0;
    return mMethodsGet(self)->GfpRxUpmGet(self, channel, clear);
    }

uint32 ThaModulePmcGfpRxEHecErrorGet(ThaModulePmc self, AtGfpChannel channel, eBool clear)
    {
    if (self == NULL || channel == NULL)
        return 0;
    return mMethodsGet(self)->GfpRxEHecErrorGet(self, channel, clear);
    }

uint32 ThaModulePmcEthFlowTxBytesGet(ThaModulePmc self, AtEthFlow channel, eBool clear)
    {
    if (self == NULL || channel == NULL)
        return 0;
    return mMethodsGet(self)->EthFlowTxBytes(self, channel, clear);
    }

uint32 ThaModulePmcEthFlowTxPacketsGet(ThaModulePmc self, AtEthFlow channel, eBool clear)
    {
    if (self == NULL || channel == NULL)
        return 0;
    return mMethodsGet(self)->EthFlowTxPackets(self, channel, clear);
    }

uint32 ThaModulePmcEthFlowTxFragmentPacketsGet(ThaModulePmc self, AtEthFlow channel, eBool clear)
    {
    if (self == NULL || channel == NULL)
        return 0;
    return mMethodsGet(self)->EthFlowTxFragmentPackets(self, channel, clear);
    }

uint32 ThaModulePmcEthFlowTxNullFragmentPacketsGet(ThaModulePmc self, AtEthFlow channel, eBool clear)
    {
    if (self == NULL || channel == NULL)
        return 0;
    return mMethodsGet(self)->EthFlowTxNullFragmentPackets(self, channel, clear);
    }

uint32 ThaModulePmcEthFlowTxDiscardPacketsGet(ThaModulePmc self, AtEthFlow channel, eBool clear)
    {
    if (self == NULL || channel == NULL)
        return 0;
    return mMethodsGet(self)->EthFlowTxDiscardPackets(self, channel, clear);
    }

uint32 ThaModulePmcEthFlowTxDiscardBytesGet(ThaModulePmc self, AtEthFlow channel, eBool clear)
    {
    if (self == NULL || channel == NULL)
        return 0;
    return mMethodsGet(self)->EthFlowTxDiscardBytes(self, channel, clear);
    }

uint32 ThaModulePmcEthFlowTxBECNPacketsGet(ThaModulePmc self, AtEthFlow channel, eBool clear)
    {
    if (self == NULL || channel == NULL)
        return 0;
    return mMethodsGet(self)->EthFlowTxBECNPackets(self, channel, clear);
    }

uint32 ThaModulePmcEthFlowTxFECNPacketsGet(ThaModulePmc self, AtEthFlow channel, eBool clear)
    {
    if (self == NULL || channel == NULL)
        return 0;
    return mMethodsGet(self)->EthFlowTxFECNPackets(self, channel, clear);
    }

uint32 ThaModulePmcEthFlowTxDEPacketsGet(ThaModulePmc self, AtEthFlow channel, eBool clear)
    {
    if (self == NULL || channel == NULL)
        return 0;
    return mMethodsGet(self)->EthFlowTxDEPackets(self, channel, clear);
    }

uint32 ThaModulePmcEthFlowRxBytesGet(ThaModulePmc self, AtEthFlow channel, eBool clear)
    {
    if (self == NULL || channel == NULL)
        return 0;
    return mMethodsGet(self)->EthFlowRxBytes(self, channel, clear);
    }

uint32 ThaModulePmcEthFlowRxPacketsGet(ThaModulePmc self, AtEthFlow channel, eBool clear)
    {
    if (self == NULL || channel == NULL)
        return 0;
    return mMethodsGet(self)->EthFlowRxPackets(self, channel, clear);
    }

uint32 ThaModulePmcEthFlowRxFragmentPacketsGet(ThaModulePmc self, AtEthFlow channel, eBool clear)
    {
    if (self == NULL || channel == NULL)
        return 0;
    return mMethodsGet(self)->EthFlowRxFragmentPackets(self, channel, clear);
    }

uint32 ThaModulePmcEthFlowRxNullFragmentPacketsGet(ThaModulePmc self, AtEthFlow channel, eBool clear)
    {
    if (self == NULL || channel == NULL)
        return 0;
    return mMethodsGet(self)->EthFlowRxNullFragmentPackets(self, channel, clear);
    }

uint32 ThaModulePmcEthFlowRxDiscardPacketsGet(ThaModulePmc self, AtEthFlow channel, eBool clear)
    {
    if (self == NULL || channel == NULL)
        return 0;
    return mMethodsGet(self)->EthFlowRxDiscardPackets(self, channel, clear);
    }

uint32 ThaModulePmcEthFlowRxDiscardBytesGet(ThaModulePmc self, AtEthFlow channel, eBool clear)
    {
    if (self == NULL || channel == NULL)
        return 0;
    return mMethodsGet(self)->EthFlowRxDiscardBytes(self, channel, clear);
    }

uint32 ThaModulePmcEthFlowRxBECNPacketsGet(ThaModulePmc self, AtEthFlow channel, eBool clear)
    {
    if (self == NULL || channel == NULL)
        return 0;
    return mMethodsGet(self)->EthFlowRxBECNPackets(self, channel, clear);
    }

uint32 ThaModulePmcEthFlowRxFECNPacketsGet(ThaModulePmc self, AtEthFlow channel, eBool clear)
    {
    if (self == NULL || channel == NULL)
        return 0;
    return mMethodsGet(self)->EthFlowRxFECNPackets(self, channel, clear);
    }

uint32 ThaModulePmcEthFlowRxDEPacketsGet(ThaModulePmc self, AtEthFlow channel, eBool clear)
    {
    if (self == NULL || channel == NULL)
        return 0;
    return mMethodsGet(self)->EthFlowRxDEPackets(self, channel, clear);
    }

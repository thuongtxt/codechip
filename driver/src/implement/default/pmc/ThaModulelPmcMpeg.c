/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PMCMPEG (internal)
 *
 * File        : ThaModulePmcMpeg.c
 *
 * Created Date: Sep 10, 2012
 *
 * Description : PMCMPEG internal module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaModulePmcMpegInternal.h"
#include "../util/ThaUtil.h"

#include "../../default/man/ThaDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaModulePmcMpegOffset 0xC00000

#define cThaDebugPmcSticky0 0x00C00030
#define cThaDebugPmcSticky1 0x00C00031
#define cThaDebugPmcSticky2 0x00C00032
#define cThaDebugPmcSticky3 0x00C00033
#define cThaDebugPmcSticky4 0x00C00034
#define cThaDebugPmcSticky8 0x00C00038

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods m_AtModuleOverride;
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementations */
static const tAtModuleMethods *m_AtModuleMethods = NULL;
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet Init(AtModule self)
    {
    return m_AtModuleMethods->Init(self);
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static void Delete(AtObject self)
    {
    /* Delete private data */

    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static eBool HasRegister(AtModule self, uint32 localAddress)
    {
	AtUnused(self);
    if ((localAddress >= (0x10 + cThaModulePmcMpegOffset)) && (localAddress <= (0x103 + cThaModulePmcMpegOffset)))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 *HoldRegistersGet(AtModule self, uint16 *numberOfHoldRegisters)
    {
    static uint32 holdRegisters[] = {0x10 + cThaModulePmcMpegOffset,
                                     0x11 + cThaModulePmcMpegOffset,
                                     0x12 + cThaModulePmcMpegOffset};
	AtUnused(self);
    if (numberOfHoldRegisters)
        *numberOfHoldRegisters = 3;

    return holdRegisters;
    }

static void OverrideAtObject(AtModule self)
    {
    AtObject object = (AtObject)self;

    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void StickiesClear(AtModule self)
    {
    static const uint32 cAllOne = 0xFFFFFFFF;

    mModuleHwWrite(self, cThaDebugPmcSticky0, cAllOne);
    mModuleHwWrite(self, cThaDebugPmcSticky1, cAllOne);
    mModuleHwWrite(self, cThaDebugPmcSticky2, cAllOne);
    mModuleHwWrite(self, cThaDebugPmcSticky3, cAllOne);
    mModuleHwWrite(self, cThaDebugPmcSticky4, cAllOne);
    }

static eAtRet Debug(AtModule self)
    {
    /* Information of super */
    m_AtModuleMethods->Debug(self);

    AtPrintc(cSevNormal, "\r\n");
    mModuleDebugRegPrint(self, PmcSticky0);
    mModuleDebugRegPrint(self, PmcSticky1);
    mModuleDebugRegPrint(self, PmcSticky2);
    mModuleDebugRegPrint(self, PmcSticky3);
    mModuleDebugRegPrint(self, PmcSticky4);
    mModuleDebugRegPrint(self, PmcSticky8);

    /* Clear for next time */
    StickiesClear(self);

    return cAtOk;
    }

static AtLongRegisterAccess LongRegisterAccessCreate(AtModule self)
    {
    uint16 numHoldRegisters;
    uint32 *holdRegisters = AtModuleHoldRegistersGet(self, &numHoldRegisters);
    return AtDefaultLongRegisterAccessNew(holdRegisters, numHoldRegisters, holdRegisters, numHoldRegisters);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, HasRegister);
        mMethodOverride(m_AtModuleOverride, HoldRegistersGet);
        mMethodOverride(m_AtModuleOverride, Debug);
        mMethodOverride(m_AtModuleOverride, LongRegisterAccessCreate);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideAtObject(self);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaModulePmcMpeg));

    /* Super constructor */
    if (AtModuleObjectInit(self, cThaModulePmcMpeg, device) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule ThaModulePmcMpegNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, sizeof(tThaModulePmcMpeg));
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

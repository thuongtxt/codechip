/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PMC
 * 
 * File        : Tha60290021ModulePmcReg.h
 * 
 * Created Date: Nov 3, 2016
 *
 * Description : PMC registers declaration
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021MODULEPMCREG_H_
#define _THA60290021MODULEPMCREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#define cAf6Reg_Pmc_Base_Address											0x1F00000UL
#define cAf6Reg_Counter_Field_Mask											cBit17_0
#define cAf6Reg_Counter_Field_Shift											0

/*------------------------------------------------------------------------------
Reg Name   : CPU Reg Hold 0
Reg Addr   : 0x3_B319
Reg Formula:
    Where  :
Reg Desc   :
The register provides hold register from [63:32]

------------------------------------------------------------------------------*/
#define cAf6Reg_hold_reg0                                                                              0x3B319

/*--------------------------------------
BitField Name: hold0
BitField Type: RW
BitField Desc: Hold from  [63:32]bit
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6Reg_hold_reg0_hold0_Mask                                                                     cBit31_0
#define cAf6Reg_hold_reg0_hold0_Shift                                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Transmit Counter
Reg Addr   : 0x0_0000 - 0x3EFF(RC)
Reg Formula: 0x0_0000 + 5376*$offset + $pwid
    Where  :
           + $pwid(0-5375)
           + $offset(0-2)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txpwcnt0_rc                                                                        0x00000
#define cAf6Reg_upen_txpwcnt1_rc                                                                        0x80000

/*--------------------------------------
BitField Name: txpwcnt
BitField Type: RC
BitField Desc: in case of txpwcnt0 side: + offset = 0 : txrbit + offset = 1 :
txnbit + offset = 2 : Unused in case of txpwcnt1 side: + offset = 0 : txpbit +
offset = 1 : txlbit + offset = 2 : txpw_byte_count
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6Reg_upen_txpwcnt_rc_txpwcnt_Mask                                                             cBit17_0
#define cAf6Reg_upen_txpwcnt_rc_txpwcnt_Shift                                                                   0

/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Transmit Counter
Reg Addr   : 0x0_4000-0x0_7EFF(RO)
Reg Formula: 0x0_4000 + 5376*$offset + $pwid
    Where  :
           + $pwid(0-5375)
           + $offset(0-2)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txpwcnt_ro                                                                        0x04000

/*--------------------------------------
BitField Name: txpwcnt
BitField Type: RC
BitField Desc: in case of txpwcnt0 side: + offset = 0 : txrbit + offset = 1 :
txnbit + offset = 2 : Unused in case of txpwcnt1 side: + offset = 0 : txpbit +
offset = 1 : txlbit + offset = 2 : txpw_byte_count
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6Reg_upen_txpwcnt_ro_txpwcnt_Mask                                                             cBit17_0
#define cAf6Reg_upen_txpwcnt_ro_txpwcnt_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receiver Counter Info0
Reg Addr   : 0x0_8000 - 0x0_BEFF(RC)
Reg Formula: 0x0_8000 + 5376*$offset + $pwid
    Where  :
           + $pwid(0-5375)
           + $offset(0-2)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxpwcnt_info0_rc                                                                  0x08000
#define cAf6Reg_upen_rxpwcnt_info0_cnt1_rc                                                             0x88000

/*--------------------------------------
BitField Name: rxpwcnt_info0
BitField Type: RC
BitField Desc: in case of rxpwcnt_info0_cnt0 side: + offset = 0 : rxlate +
offset = 1 : rxpbit + offset = 2 : rxlbit in case of rxpwcnt_info0_cnt1 side: +
offset = 0 : rxrbit + offset = 1 : rxnbit + offset = 2 : rxpw_byte_count
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6Reg_upen_rxpwcnt_info0_rc_rxpwcnt_info0_Mask                                                 cBit17_0
#define cAf6Reg_upen_rxpwcnt_info0_rc_rxpwcnt_info0_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receiver Counter Info0
Reg Addr   : 0x0_C000-0x0_FEFF(RO)
Reg Formula: 0x0_C000 + 5376*$offset + $pwid
    Where  :
           + $pwid(0-5375)
           + $offset(0-2)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxpwcnt_info0_ro                                                                  0x0C000

/*--------------------------------------
BitField Name: rxpwcnt_info0
BitField Type: RC
BitField Desc: in case of rxpwcnt_info0_cnt0 side: + offset = 0 : rxlate +
offset = 1 : rxpbit + offset = 2 : rxlbit in case of rxpwcnt_info0_cnt1 side: +
offset = 0 : rxrbit + offset = 1 : rxnbit + offset = 2 : rxpw_byte_count
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6Reg_upen_rxpwcnt_info0_ro_rxpwcnt_info0_Mask                                                 cBit17_0
#define cAf6Reg_upen_rxpwcnt_info0_ro_rxpwcnt_info0_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receiver Counter Info1
Reg Addr   : 0x1_0000 - 0x1_3EFF(RC)
Reg Formula: 0x1_0000 + 5376*$offset + $pwid
    Where  :
           + $pwid(0-5375)
           + $offset(0-2)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxpwcnt_info1_rc                                                                  0x10000
#define cAf6Reg_upen_rxpwcnt_info1_cnt1_rc                                                             0x90000

/*--------------------------------------
BitField Name: rxpwcnt_info1
BitField Type: RC
BitField Desc: in case of rxpwcnt_info1_cnt0 side: + offset = 0 : rxearly +
offset = 1 : rxlopsta + offset = 2 : rxlopsyn in case of rxpwcnt_info1_cnt1
side: + offset = 0 : rxlost + offset = 1 : rxlopstaclr + offset = 2 :
rxlopsynclr
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6Reg_upen_rxpwcnt_info1_rc_rxpwcnt_info1_Mask                                                 cBit17_0
#define cAf6Reg_upen_rxpwcnt_info1_rc_rxpwcnt_info1_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receiver Counter Info1
Reg Addr   : 0x1_4000-0x1_7EFF(RO)
Reg Formula: 0x1_4000 + 5376*$offset + $pwid
    Where  :
           + $pwid(0-5375)
           + $offset(0-2)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxpwcnt_info1_ro                                                                  0x14000

/*--------------------------------------
BitField Name: rxpwcnt_info1
BitField Type: RC
BitField Desc: in case of rxpwcnt_info1_cnt0 side: + offset = 0 : rxearly +
offset = 1 : rxlopsta + offset = 2 : rxlopsyn in case of rxpwcnt_info1_cnt1
side: + offset = 0 : rxlost + offset = 1 : rxlopstaclr + offset = 2 :
rxlopsynclr
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6Reg_upen_rxpwcnt_info1_ro_rxpwcnt_info1_Mask                                                 cBit17_0
#define cAf6Reg_upen_rxpwcnt_info1_ro_rxpwcnt_info1_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receiver Counter Info2
Reg Addr   : 0x1_8000 - 0x1_BEFF(RC)
Reg Formula: 0x1_8000 + 5376*$offset + $pwid
    Where  :
           + $pwid(0-5375)
           + $offset(0-2)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxpwcnt_info2_rc                                                                  0x18000
#define cAf6Reg_upen_rxpwcnt_info2_cnt1_rc                                                             0x98000

/*--------------------------------------
BitField Name: rxpwcnt_info2
BitField Type: RC
BitField Desc: in case of rxpwcnt_info2_cnt0 side: + offset = 0 : rxoverrun +
offset = 1 : rxunderrun + offset = 2 : rxmalform in case of rxpwcnt_info2_cnt1
side: + offset = 0 : clroverrun + offset = 1 : clrunderrun + offset = 2 :
rxstray
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6Reg_upen_rxpwcnt_info2_rc_rxpwcnt_info2_Mask                                                 cBit17_0
#define cAf6Reg_upen_rxpwcnt_info2_rc_rxpwcnt_info2_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receiver Counter Info2
Reg Addr   : 0x1_C000-0x1_FEFF(RO)
Reg Formula: 0x1_C000 + 5376*$offset + $pwid
    Where  :
           + $pwid(0-5375)
           + $offset(0-2)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxpwcnt_info2_ro                                                                  0x1C000

/*--------------------------------------
BitField Name: rxpwcnt_info2
BitField Type: RC
BitField Desc: in case of rxpwcnt_info2_cnt0 side: + offset = 0 : rxoverrun +
offset = 1 : rxunderrun + offset = 2 : rxmalform in case of rxpwcnt_info2_cnt1
side: + offset = 0 : clroverrun + offset = 1 : clrunderrun + offset = 2 :
rxstray
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6Reg_upen_rxpwcnt_info2_ro_rxpwcnt_info2_Mask                                                 cBit17_0
#define cAf6Reg_upen_rxpwcnt_info2_ro_rxpwcnt_info2_Shift                                                       0

/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Byte Counter
Reg Addr   : 0x6_8000- 0x6_BEFF(RC)
Reg Formula: 0x6_8000 + 5376*$offset + $pwid
    Where  :
           + $pwid(0-5375)
           + $offset(0-2)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txpwcntbyte_rc                                                                      0x68000
#define cAf6Reg_upen_rxpwcntbyte_rc                                                                      0x70000

/*--------------------------------------
BitField Name: pwcntbyte
BitField Type: RC
BitField Desc: txpwcntbyte (rxpwcntbyte)
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_pwcntbyte_rc_pwcntbyte_Mask                                                         cBit31_0
#define cAf6_upen_pwcntbyte_rc_pwcntbyte_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : PMR Error Counter
Reg Addr   : 0x3_B000 - 0x3_B02F(RC)
Reg Formula: 0x3_B000 + 16*$offset + $stsid
    Where  :
           + $offset(0-2)
           + $stsid(0-15)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_poh_pmr_cnt0_rc                                                                   0x3B000
#define cAf6Reg_upen_poh_pmr_cnt1_rc                                                                   0xBB000

/*--------------------------------------
BitField Name: pmr_err_cnt
BitField Type: RC
BitField Desc: in case of pmr_err_cnt0 side: + offset = 0 : rei_l + offset = 1 :
b1 + offset = 2 : Unused in case of pmr_err_cnt1 side: + offset = 0 : b2 +
offset = 1 : Unused + offset = 2 : Unused
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6Reg_upen_poh_pmr_cnt0_rc_pmr_err_cnt_Mask                                                    cBit17_0
#define cAf6Reg_upen_poh_pmr_cnt0_rc_pmr_err_cnt_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : PMR Error Counter
Reg Addr   : 0x3_B040-0x3_B06F(RO)
Reg Formula: 0x3_B040 + 16*$offset + $stsid
    Where  :
           + $offset(0-2)
           + $stsid(0-15)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_poh_pmr_cnt0_ro                                                                   0x3B040

/*--------------------------------------
BitField Name: pmr_err_cnt
BitField Type: RC
BitField Desc: in case of pmr_err_cnt0 side: + offset = 0 : rei_l + offset = 1 :
b1 + offset = 2 : Unused in case of pmr_err_cnt1 side: + offset = 0 : b2 +
offset = 1 : Unused + offset = 2 : Unused
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6Reg_upen_poh_pmr_cnt0_ro_pmr_err_cnt_Mask                                                    cBit17_0
#define cAf6Reg_upen_poh_pmr_cnt0_ro_pmr_err_cnt_Shift                                                          0

/*------------------------------------------------------------------------------
Reg Name   : POH path sts ERDI Counter1
Reg Addr   : 0x3_D800-0x3_DDFF (RW)
Reg Formula: 0x3_D800 + 512*$offset +  64*$slcid + $stsid
    Where  :
           + $stsid(0-63)
           + $offset(0-2)
           + $slcid(0-7)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_pohpmdat_sts_ber_erdi1_rw_Base                                                    0x3D800

/*--------------------------------------
BitField Name: pohpath_ber_erdi
BitField Type: RW
BitField Desc: in case of pohpath_ber_erdi_cnt2 side: + offset = 0 : sts_rei +
offset = 1 : Unused + offset = 2 : Unused in case of pohpath_ber_erdi_cnt3 side:
+ offset = 0 : sts_bip(B3) + offset = 1 : Unused + offset = 2 : Unused
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_pohpmdat_sts_ber_erdi1_rw_pohpath_ber_erdi_Mask                                     cBit17_0
#define cAf6_upen_pohpmdat_sts_ber_erdi1_rw_pohpath_ber_erdi_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : POH path sts ERDI Counter1
Reg Addr   : 0xB_D800-0xB_DDFF (RW)
Reg Formula: 0xB_D800 + 512*$offset +  64*$slcid + $stsid
    Where  :
           + $stsid(0-63)
           + $offset(0-2)
           + $slcid(0-7)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_pohpmdat_sts_ber_erdi2_rw_Base                                                    0xBD800

/*------------------------------------------------------------------------------
Reg Name   : POH path vt ERDI Counter0
Reg Addr   : 0x6_4000 - 0x6_7FFF(RW)
Reg Formula: 0x6_4000 + 5376*$offset + 672*$slcid +  28*stsid + 4*vtgid + vtid
    Where  :
           + $offset(0-2)
           + $slcid(0-7)
           + $stsid(0-23)
           + $vtgid(0-6)
           + $vtid(0-3)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_pohpmdat_vt_ber_erdi0_rw_Base                                                     0x64000

/*--------------------------------------
BitField Name: pohpath_ber_erdi
BitField Type: RW
BitField Desc: in case of pohpath_ber_erdi_cnt2 side: + offset = 0 : vt_rei +
offset = 1 : Unused + offset = 2 : Unused in case of pohpath_ber_erdi_cnt3 side:
+ offset = 0 : vt_bip + offset = 1 : Unused + offset = 2 : Unused
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_pohpmdat_vt_ber_erdi0_rw_pohpath_ber_erdi_Mask                                      cBit17_0
#define cAf6_upen_pohpmdat_vt_ber_erdi0_rw_pohpath_ber_erdi_Shift                                            0


/*------------------------------------------------------------------------------
Reg Name   : POH path vt ERDI Counter0
Reg Addr   : 0xE_4000 - 0xE_7FFF(RW)
Reg Formula: 0xE_4000 + 5376*$offset + 672*$slcid +  28*stsid + 4*vtgid + vtid
    Where  :
           + $offset(0-2)
           + $slcid(0-7)
           + $stsid(0-23)
           + $vtgid(0-6)
           + $vtid(0-3)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_pohpmdat_vt_ber_erdi01_rw_Base                                                     0xE4000


/*------------------------------------------------------------------------------
Reg Name   : POH path vt ERDI Counter1
Reg Addr   : 0x4_4000 - 0x4_7FFF(RW)
Reg Formula: 0x4_4000 + 5376*$offset + 672*$slcid +  28*stsid + 4*vtgid + vtid
    Where  :
           + $offset(0-2)
           + $slcid(0-7)
           + $stsid(0-23)
           + $vtgid(0-6)
           + $vtid(0-3)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_pohpmdat_vt_ber_erdi1_rw_Base                                                     0x44000

/*--------------------------------------
BitField Name: pohpath_ber_erdi
BitField Type: RW
BitField Desc: in case of pohpath_ber_erdi_cnt2 side: + offset = 0 : vt_rei +
offset = 1 : Unused + offset = 2 : Unused in case of pohpath_ber_erdi_cnt3 side:
+ offset = 0 : vt_bip + offset = 1 : Unused + offset = 2 : Unused
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_pohpmdat_vt_ber_erdi1_rw_pohpath_ber_erdi_Mask                                      cBit17_0
#define cAf6_upen_pohpmdat_vt_ber_erdi1_rw_pohpath_ber_erdi_Shift                                            0


/*------------------------------------------------------------------------------
Reg Name   : POH path vt ERDI Counter1
Reg Addr   : 0xC_4000 - 0xC_7FFF(RW)
Reg Formula: 0xC_4000 + 5376*$offset + 672*$slcid +  28*stsid + 4*vtgid + vtid
    Where  :
           + $offset(0-2)
           + $slcid(0-7)
           + $stsid(0-23)
           + $vtgid(0-6)
           + $vtid(0-3)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_pohpmdat_vt_ber_erdi2_rw_Base                                                     0xC4000


/*------------------------------------------------------------------------------
Reg Name   : POH vt pointer Counter 0
Reg Addr   : 0x5_4000-0x5_7EFF(RW)
Reg Formula: 0x5_4000 + 5376*$offset + 672*$slcid +  28*stsid + 4*vtgid + vtid
    Where  :
           + $offset(0-2)
           + $slcid(0-7)
           + $stsid(0-23)
           + $vtgid(0-6)
           + $vtid(0-3)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_poh_vt_cnt0_rw_Base                                                               0x54000

/*--------------------------------------
BitField Name: poh_vt_cnt
BitField Type: RW
BitField Desc: in case of poh_vt_cnt0 side: + offset = 0 : pohtx_vt_dec + offset
= 1 : pohrx_vt_dec + offset = 2 : Unused in case of poh_vt_cnt1 side: + offset =
0 : pohtx_vt_inc + offset = 1 : pohrx_vt_inc + offset = 2 : Unused
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_poh_vt_cnt0_rw_poh_vt_cnt_Mask                                                      cBit17_0
#define cAf6_upen_poh_vt_cnt0_rw_poh_vt_cnt_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : POH vt pointer Counter 0
Reg Addr   : 0xD_4000-0xD_7EFF(RW)
Reg Formula: 0xD_4000 + 5376*$offset + 672*$slcid +  28*stsid + 4*vtgid + vtid
    Where  :
           + $offset(0-2)
           + $slcid(0-7)
           + $stsid(0-23)
           + $vtgid(0-6)
           + $vtid(0-3)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_poh_vt_cnt01_rw_Base                                                               0xD4000


/*------------------------------------------------------------------------------
Reg Name   : POH vt pointer Counter 1
Reg Addr   : 0x4_C000-0x4_FEFF(RW)
Reg Formula: 0x4_C000 + 5376*$offset + 672*$slcid +  28*stsid + 4*vtgid + vtid
    Where  :
           + $offset(0-2)
           + $slcid(0-7)
           + $stsid(0-23)
           + $vtgid(0-6)
           + $vtid(0-3)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_poh_vt_cnt1_rw_Base                                                               0x4C000

/*--------------------------------------
BitField Name: poh_vt_cnt
BitField Type: RW
BitField Desc: in case of poh_vt_cnt0 side: + offset = 0 : pohtx_vt_dec + offset
= 1 : pohrx_vt_dec + offset = 2 : Unused in case of poh_vt_cnt1 side: + offset =
0 : pohtx_vt_inc + offset = 1 : pohrx_vt_inc + offset = 2 : Unused
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_poh_vt_cnt1_rw_poh_vt_cnt_Mask                                                      cBit17_0
#define cAf6_upen_poh_vt_cnt1_rw_poh_vt_cnt_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : POH vt pointer Counter 1
Reg Addr   : 0xC_C000-0xC_FEFF(RW)
Reg Formula: 0xC_C000 + 5376*$offset + 672*$slcid +  28*stsid + 4*vtgid + vtid
    Where  :
           + $offset(0-2)
           + $slcid(0-7)
           + $stsid(0-23)
           + $vtgid(0-6)
           + $vtid(0-3)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_poh_vt_cnt2_rw_Base                                                               0xCC000

/*------------------------------------------------------------------------------
Reg Name   : POH sts pointer Counter
Reg Addr   : 0x3_F800-0x3_FDFF(RW)
Reg Formula: 0x3_F800 + 512*$offset +  64*$slcid + $stsid
    Where  :
           + $stsid(0-63)
           + $offset(0-2)
           + $slcid(0-7)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_poh_sts_cnt_rw_Base                                                               0x3F800

/*--------------------------------------
BitField Name: poh_sts_cnt
BitField Type: RW
BitField Desc: in case of poh_sts_cnt0 side: + offset = 0 :  pohtx_sts_dec +
offset = 1 :  pohrx_sts_dec + offset = 2 :  Unused in case of poh_sts_cnt1 side:
+ offset = 0 :  pohtx_sts_inc + offset = 1 :  pohrx_sts_inc + offset = 2 :
Unused
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_poh_sts_cnt_rw_poh_sts_cnt_Mask                                                     cBit17_0
#define cAf6_upen_poh_sts_cnt_rw_poh_sts_cnt_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : POH sts pointer Counter
Reg Addr   : 0xB_F800-0xB_FDFF(RW)
Reg Formula: 0xB_F800 + 512*$offset +  64*$slcid + $stsid
    Where  :
           + $stsid(0-63)
           + $offset(0-2)
           + $slcid(0-7)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_poh_sts_cnt1_rw_Base                                                               0xBF800


/*------------------------------------------------------------------------------
Reg Name   : PDH ds1 cntval Counter Bus1
Reg Addr   : 0x3_0800-0x3_0FDF(RW)
Reg Formula: 0x3_0800 + 4096*$slcid + 672*$offset + 28*stsid + 4*vtgid + vtid
    Where  :
           + $slcid(0-3)
           + $offset(0-2)
           + $stsid(0-23)
           + $vtgid(0-6)
           + $vtid(0-3)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_pdh_de1cntval30_rw_Base                                                           0x30800
#define cAf6Reg_upen_pdh_de1cntval31_rw_Base                                                           0xB0800

/*--------------------------------------
BitField Name: ds1_cntval_bus1_cnt
BitField Type: RW
BitField Desc: in case of ds1_cntval_bus1_cnt0 side: + offset = 0 : fe + offset
= 1 : rei + offset = 2 : unused in case of ds1_cntval_bus1_cnt1 side: + offset =
0 : crc + offset = 1 : lcv + offset = 2 : unused
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_pdh_de1cntval30_rw_ds1_cntval_bus1_cnt_Mask                                         cBit17_0
#define cAf6_upen_pdh_de1cntval30_rw_ds1_cntval_bus1_cnt_Shift                                               0


/*------------------------------------------------------------------------------
Reg Name   : PDH ds1 cntval Counter Bus2
Reg Addr   : 0x3_4800-0x3_4FDF(RW)
Reg Formula: 0x3_4800 + 4096*$slcid + 672*$offset + 28*stsid + 4*vtgid + vtid
    Where  :
           + $slcid(0-3)
           + $offset(0-2)
           + $stsid(0-23)
           + $vtgid(0-6)
           + $vtid(0-3)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_pdh_de1cntval74_rw_Base                                                           0x34800
#define cAf6Reg_upen_pdh_de1cntval75_rw_Base                                                           0xB4800

/*--------------------------------------
BitField Name: ds1_cntval_bus2_cnt
BitField Type: RW
BitField Desc: in case of ds1_cntval_bus2_cnt0 side: + offset = 0 : fe + offset
= 1 : rei + offset = 2 : unused in case of ds1_cntval_bus2_cnt1 side: + offset =
0 : crc + offset = 1 : lcv + offset = 2 : unused
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_pdh_de1cntval74_rw_ds1_cntval_bus2_cnt_Mask                                         cBit17_0
#define cAf6_upen_pdh_de1cntval74_rw_ds1_cntval_bus2_cnt_Shift                                               0


/*------------------------------------------------------------------------------
Reg Name   : PDH ds3 cntval Counter
Reg Addr   : 0x3_8800-0x3_8DFF(RW)
Reg Formula: 0x3_8800 + 512*$offset +  64*$slcid + $stsid
    Where  :
           + $stsid(0-63)
           + $offset(0-2)
           + $slcid(0-7)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_pdh_de3cnt0_rw_Base                                                               0x38800
#define cAf6Reg_upen_pdh_de3cnt1_rw_Base                                                               0xB8800

/*--------------------------------------
BitField Name: ds3_cntval_cnt
BitField Type: RW
BitField Desc: in case of ds3_cntval_cnt0 side: + offset = 0 : fe + offset = 1 :
pb + offset = 2 : lcv in case of ds3_cntval_cnt1 side: + offset = 0 : rei +
offset = 1 : cb + offset = 2 : unused
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_pdh_de3cnt0_rw_ds3_cntval_cnt_Mask                                                  cBit17_0
#define cAf6_upen_pdh_de3cnt0_rw_ds3_cntval_cnt_Shift                                                        0

/*------------------------------------------------------------------------------
Reg Name   : CLA Counter00
Reg Addr   : 0x3_B320(RC)
Reg Formula:
    Where  :
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cla0_rc                                                                           0x3B320

/*--------------------------------------
BitField Name: cla_err
BitField Type: RC
BitField Desc: cla_err
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_cla0_rc_cla_err_Mask                                                                cBit31_0
#define cAf6_upen_cla0_rc_cla_err_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : CLA Counter00
Reg Addr   : 0x3_B321(RO)
Reg Formula:
    Where  :
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cla0_ro                                                                           0x3B321

/*--------------------------------------
BitField Name: cla_err
BitField Type: RC
BitField Desc: cla_err
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_cla0_ro_cla_err_Mask                                                                cBit31_0
#define cAf6_upen_cla0_ro_cla_err_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : ETH Counter00
Reg Addr   : 0x3_B328(RC)
Reg Formula:
    Where  :
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxeth00_rc                                                                        0x3B328

/*--------------------------------------
BitField Name: rxpkt
BitField Type: RC
BitField Desc: rxpkt
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxeth00_rc_rxpkt_Mask                                                               cBit31_0
#define cAf6_upen_rxeth00_rc_rxpkt_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : ETH Counter00
Reg Addr   : 0x3_B329(RO)
Reg Formula:
    Where  :
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxeth00_ro                                                                        0x3B329

/*--------------------------------------
BitField Name: rxpkt
BitField Type: RC
BitField Desc: rxpkt
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxeth00_ro_rxpkt_Mask                                                               cBit31_0
#define cAf6_upen_rxeth00_ro_rxpkt_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : ETH Packet RX Counter
Reg Addr   : 0x3_B370(RC)
Reg Formula:
    Where  :
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxeth1_pkt_rc                                                                     0x3B370

/*--------------------------------------
BitField Name: cntrxpkt
BitField Type: RC
BitField Desc: ETH rx Packet Counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxeth1_pkt_rc_cntrxpkt_Mask                                                         cBit31_0
#define cAf6_upen_rxeth1_pkt_rc_cntrxpkt_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : ETH Packet RX Counter
Reg Addr   : 0x3_B371(RO)
Reg Formula:
    Where  :
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxeth1_pkt_ro                                                                     0x3B371

/*--------------------------------------
BitField Name: cntrxpkt
BitField Type: RC
BitField Desc: ETH rx Packet Counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxeth1_pkt_ro_cntrxpkt_Mask                                                         cBit31_0
#define cAf6_upen_rxeth1_pkt_ro_cntrxpkt_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : ETH Byte Counter
Reg Addr   : 0x3_B372(RC)
Reg Formula:
    Where  :
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxeth1_byte_rc                                                                    0x3B372

/*--------------------------------------
BitField Name: cntrxbyte
BitField Type: RC
BitField Desc: ETH rx Byte Counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxeth1_byte_rc_cntrxbyte_Mask                                                       cBit31_0
#define cAf6_upen_rxeth1_byte_rc_cntrxbyte_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : ETH Byte Counter
Reg Addr   : 0x3_B373(RO)
Reg Formula:
    Where  :
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxeth1_byte_ro                                                                    0x3B373

/*--------------------------------------
BitField Name: cntrxbyte
BitField Type: RC
BitField Desc: ETH rx Byte Counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxeth1_byte_ro_cntrxbyte_Mask                                                       cBit31_0
#define cAf6_upen_rxeth1_byte_ro_cntrxbyte_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : ETH Counter00
Reg Addr   : 0x3_B338(RC)
Reg Formula:
    Where  :
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txeth00_rc                                                                        0x3B338
#define cAf6Reg_upen_txeth_len257_512_rc                                                               0x3B368
#define cAf6Reg_upen_txeth_len512_1024_rc                                                              0x3B36A
#define cAf6Reg_upen_txeth_len1025_1528_rc                                                             0x3B36C
#define cAf6Reg_upen_txeth_len257_512_ro                                                               0x3B369
#define cAf6Reg_upen_txeth_len512_1024_ro                                                              0x3B36B
#define cAf6Reg_upen_txeth_len1025_1528_ro                                                             0x3B36D

/*--------------------------------------
BitField Name: txpkt
BitField Type: RC
BitField Desc: txpkt
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txeth00_rc_txpkt_Mask                                                               cBit31_0
#define cAf6_upen_txeth00_rc_txpkt_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : ETH Counter00
Reg Addr   : 0x3_B339(RO)
Reg Formula:
    Where  :
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txeth00_ro                                                                        0x3B339

/*--------------------------------------
BitField Name: txpkt
BitField Type: RC
BitField Desc: txpkt
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txeth00_ro_txpkt_Mask                                                               cBit31_0
#define cAf6_upen_txeth00_ro_txpkt_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : ETH Packet TX Counter
Reg Addr   : 0x3_B374(RC)
Reg Formula:
    Where  :
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txeth1_pkt_rc                                                                     0x3B374

/*--------------------------------------
BitField Name: cnttxpkt
BitField Type: RC
BitField Desc: ETH tx Packet Counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txeth1_pkt_rc_cnttxpkt_Mask                                                         cBit31_0
#define cAf6_upen_txeth1_pkt_rc_cnttxpkt_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : ETH Packet TX Counter
Reg Addr   : 0x3_B375(RO)
Reg Formula:
    Where  :
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txeth1_pkt_ro                                                                     0x3B375

/*--------------------------------------
BitField Name: cnttxpkt
BitField Type: RC
BitField Desc: ETH tx Packet Counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txeth1_pkt_ro_cnttxpkt_Mask                                                         cBit31_0
#define cAf6_upen_txeth1_pkt_ro_cnttxpkt_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : ETH Byte TX Counter
Reg Addr   : 0x3_B376(RC)
Reg Formula:
    Where  :
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txeth1_byte_rc                                                                    0x3B376

/*--------------------------------------
BitField Name: cnttxbyte
BitField Type: RC
BitField Desc: ETH tx Byte Counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txeth1_byte_rc_cnttxbyte_Mask                                                       cBit31_0
#define cAf6_upen_txeth1_byte_rc_cnttxbyte_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : ETH Byte TX Counter
Reg Addr   : 0x3_B377(RO)
Reg Formula:
    Where  :
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txeth1_byte_ro                                                                    0x3B377

/*--------------------------------------
BitField Name: cnttxbyte
BitField Type: RC
BitField Desc: ETH tx Byte Counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txeth1_byte_ro_cnttxbyte_Mask                                                       cBit31_0
#define cAf6_upen_txeth1_byte_ro_cnttxbyte_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : ETH TX Packet OAM CNT
Reg Addr   : 0x3_B336(RC)
Reg Formula:
    Where  :
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cnttxoam_rc                                                                       0x3B337

/*--------------------------------------
BitField Name: cnttxoamt
BitField Type: RC
BitField Desc: TX Packet OAM CNT
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_cnttxoam_rc_cnttxoamt_Mask                                                          cBit31_0
#define cAf6_upen_cnttxoam_rc_cnttxoamt_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : ETH TX Packet OAM CNT
Reg Addr   : 0x3_B337(RO)
Reg Formula:
    Where  :
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cnttxoam_ro                                                                       0x3B337

/*--------------------------------------
BitField Name: cnttxoamt
BitField Type: RC
BitField Desc: TX Packet OAM CNT
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_cnttxoam_ro_cnttxoamt_Mask                                                          cBit31_0
#define cAf6_upen_cnttxoam_ro_cnttxoamt_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : Classifier Bytes Counter vld
Reg Addr   : 0x3_B308-0x3_B30B(RC)
Reg Formula: 0x3_B308 + $clsid
    Where  :
           + $clsid(0-3)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cls_bcnt_rc                                                                       0x3B308

/*--------------------------------------
BitField Name: bytecnt
BitField Type: RC
BitField Desc: Classifier bytecnt
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_cls_bcnt_rc_bytecnt_Mask                                                            cBit31_0
#define cAf6_upen_cls_bcnt_rc_bytecnt_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Classifier Bytes Counter vld
Reg Addr   : 0x3_B30C-0x3_B30F(RO)
Reg Formula: 0x3_B30C + $clsid
    Where  :
           + $clsid(0-3)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cls_bcnt_ro                                                                       0x3B30C

/*--------------------------------------
BitField Name: bytecnt
BitField Type: RC
BitField Desc: Classifier bytecnt
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_cls_bcnt_ro_bytecnt_Mask                                                            cBit31_0
#define cAf6_upen_cls_bcnt_ro_bytecnt_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Classifier output counter pkt0
Reg Addr   : 0x3_B348-0x3_B34B(RC)
Reg Formula: 0x3_B348 + $offset
    Where  :
           + $offset(0-2)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cls_opcnt0_rc                                                                     0x3B348
#define cAf6Reg_upen_cls_opcnt0_opt2_rc                                                                0xBB348

/*--------------------------------------
BitField Name: opktcnt0(opktcnt1)
BitField Type: RC
BitField Desc: in case of opktcnt0 + offset = 0 : DS1/E1 good pkt counter +
offset = 1 : Control pkt good counter + offset = 2 : FCs err cnt in case of
opktcnt1 + offset = 0 : DS3/E3/EC1 good pkt counter + offset = 1 : Sequence err
counter + offset = 2 : len pkt err cnt
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_cls_opcnt0_rc_opktcnt0_Mask                                               cBit17_0
#define cAf6_upen_cls_opcnt0_rc_opktcnt0_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Classifier output counter pkt0
Reg Addr   : 0x3_B34C-0x3_B34F(RO)
Reg Formula: 0x3_B34C + $offset
    Where  :
           + $offset(0-2)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cls_opcnt0_ro                                                                     0x3B34C
#define cAf6Reg_upen_cls_opcnt0_opt2_ro                                                                0xBB34C

/*--------------------------------------
BitField Name: opktcnt0(opktcnt1)
BitField Type: RC
BitField Desc: in case of opktcnt0 + offset = 0 : DS1/E1 good pkt counter +
offset = 1 : Control pkt good counter + offset = 2 : FCs err cnt in case of
opktcnt1 + offset = 0 : DS3/E3/EC1 good pkt counter + offset = 1 : Sequence err
counter + offset = 2 : len pkt err cnt
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_cls_opcnt0_ro_opktcnt0_Mask                                               cBit17_0
#define cAf6_upen_cls_opcnt0_ro_opktcnt0_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Classifier output counter pkt1
Reg Addr   : 0x3_B350-0x3_B353(RC)
Reg Formula: 0x3_B350 + $offset
    Where  :
           + $offset(0-2)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cls_opcnt1_rc                                                                     0x3B350
#define cAf6Reg_upen_cls_opcnt1_opt2_rc                                                                0xBB350

/*--------------------------------------
BitField Name: opktcnt0(opktcnt1)
BitField Type: RC
BitField Desc: in case of opktcnt0 + offset = 0 : DA error pkt counter + offset
= 1 : E-type err pkt counter + offset = 2 : Unused in case of opktcnt1 + offset
= 0 : SA error pkt counter + offset = 1 : Length field err counter + offset = 2
: Unused
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_cls_opcnt1_rc_opktcnt0_Mask                                               cBit17_0
#define cAf6_upen_cls_opcnt1_rc_opktcnt0_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Classifier output counter pkt1
Reg Addr   : 0x3_B354-0x3_B357(RO)
Reg Formula: 0x3_B354 + $offset
    Where  :
           + $offset(0-2)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cls_opcnt1_ro                                                                     0x3B354
#define cAf6Reg_upen_cls_opcnt1_opt2_ro                                                                0xBB354

/*--------------------------------------
BitField Name: opktcnt0(opktcnt1)
BitField Type: RC
BitField Desc: in case of opktcnt0 + offset = 0 : DA error pkt counter + offset
= 1 : E-type err pkt counter + offset = 2 : Unused in case of opktcnt1 + offset
= 0 : SA error pkt counter + offset = 1 : Length field err counter + offset = 2
: Unused
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_cls_opcnt1_ro_opktcnt0_Mask                                               cBit17_0
#define cAf6_upen_cls_opcnt1_ro_opktcnt0_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Classifier input counter pkt
Reg Addr   : 0x3_B306(RC)
Reg Formula:
    Where  :
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cls_ipcnt_rc                                                                      0x3B306

/*--------------------------------------
BitField Name: ipktcnt
BitField Type: RC
BitField Desc: input packet cnt
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_cls_ipcnt_rc_ipktcnt_Mask                                                           cBit31_0
#define cAf6_upen_cls_ipcnt_rc_ipktcnt_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Classifier input counter pkt
Reg Addr   : 0x3_B307(RO)
Reg Formula:
    Where  :
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cls_ipcnt_ro                                                                      0x3B307

/*--------------------------------------
BitField Name: ipktcnt
BitField Type: RC
BitField Desc: input packet cnt
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_cls_ipcnt_ro_ipktcnt_Mask                                                           cBit31_0
#define cAf6_upen_cls_ipcnt_ro_ipktcnt_Shift                                                                 0



/*------------------------------------------------------------------------------
Reg Name   : PDH Mux Ethernet Byte Counter
Reg Addr   : 0x3_B300(RC)
Reg Formula:
    Where  :
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_eth_bytecnt_rc                                                                    0x3B300

/*--------------------------------------
BitField Name: eth_bytecnt
BitField Type: RC
BitField Desc: ETH Byte Counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_eth_bytecnt_rc_eth_bytecnt_Mask                                                     cBit31_0
#define cAf6_upen_eth_bytecnt_rc_eth_bytecnt_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : PDH Mux Ethernet Byte Counter
Reg Addr   : 0x3_B301(RO)
Reg Formula:
    Where  :
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_eth_bytecnt_ro                                                                    0x3B301

/*--------------------------------------
BitField Name: eth_bytecnt
BitField Type: RC
BitField Desc: ETH Byte Counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_eth_bytecnt_ro_eth_bytecnt_Mask                                                     cBit31_0
#define cAf6_upen_eth_bytecnt_ro_eth_bytecnt_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : PDH Mux Ethernet Byte Packet Counter
Reg Addr   : 0x3_B340(RC)
Reg Formula:
    Where  :
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_eth_pktcnt_rc                                                                    0x3B340

/*--------------------------------------
BitField Name: eth_pktcnt
BitField Type: RC
BitField Desc: ETH Packet Counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_eth_bytecnt_rc_eth_pktcnt_Mask                                                      cBit31_0
#define cAf6_upen_eth_bytecnt_rc_eth_pktcnt_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : PDH Mux Ethernet Byte Packet Counter
Reg Addr   : 0x3_B341(RO)
Reg Formula:
    Where  :
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_eth_pktcnt_ro                                                                    0x3B341

/*--------------------------------------
BitField Name: eth_pktcnt
BitField Type: RC
BitField Desc: ETH Packet Counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_eth_bytecnt_ro_eth_pktcnt_Mask                                                      cBit31_0
#define cAf6_upen_eth_bytecnt_ro_eth_pktcnt_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : DE1 TDM bit counter
Reg Addr   : 0x3_B200-0x3_B27F(RC)
Reg Formula: 0x3_B200 + $lid
    Where  :
           + $lid(0-127)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_de1_tdmvld_cnt_rc                                                                 0x3B200

/*--------------------------------------
BitField Name: de1_bit_cnt
BitField Type: RC
BitField Desc: bit_cnt
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6Reg_upen_de1_tdmvld_cnt_rc_de1_bit_cnt_Mask                                                  cBit17_0
#define cAf6Reg_upen_de1_tdmvld_cnt_rc_de1_bit_cnt_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : DE1 TDM bit counter
Reg Addr   : 0x3_B280-0x3_B2FF(RO)
Reg Formula: 0x3_B280 + $lid
    Where  :
           + $lid(0-127)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_de1_tdmvld_cnt_ro                                                                 0x3B280

/*--------------------------------------
BitField Name: de1_bit_cnt
BitField Type: RC
BitField Desc: bit_cnt
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6Reg_upen_de1_tdmvld_cnt_ro_de1_bit_cnt_Mask                                                  cBit17_0
#define cAf6Reg_upen_de1_tdmvld_cnt_ro_de1_bit_cnt_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : DE3 TDM bit counter
Reg Addr   : 0x3_B180-0x3_B19F(RC)
Reg Formula: 0x3_B180 + $lid
    Where  :
           + $lid(0-31)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_de3_tdmvld_cnt_rc                                                                 0x3B180

/*--------------------------------------
BitField Name: de3_bit_cnt
BitField Type: RC
BitField Desc: bit_cnt
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6Reg_upen_de3_tdmvld_cnt_rc_de3_bit_cnt_Mask                                                  cBit17_0
#define cAf6Reg_upen_de3_tdmvld_cnt_rc_de3_bit_cnt_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : DE3 TDM bit counter
Reg Addr   : 0x3_B1A0-0x3_B1BF(RO)
Reg Formula: 0x3_B1A0 + $lid
    Where  :
           + $lid(0-31)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_de3_tdmvld_cnt_ro                                                                 0x3B1A0

/*--------------------------------------
BitField Name: de3_bit_cnt
BitField Type: RC
BitField Desc: bit_cnt
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6Reg_upen_de3_tdmvld_cnt_ro_de3_bit_cnt_Mask                                                  cBit17_0
#define cAf6Reg_upen_de3_tdmvld_cnt_ro_de3_bit_cnt_Shift                                                        0

/*------------------------------------------------------------------------------
Reg Name   : PMC GLB INF Counter GFP ENC HO
Reg Addr   : 0x0B_4000
Reg Formula: 0x0B_4000 +  $idx_id*0x400 + $pos_id*0x100 + $r2c*0x80 + $encho_id
    Where  :
           + $idx_id(0-1) : index
           + $pos_id(0-3) : Position ID
           + $r2c(0-1) : r2c bit
           + $encho_id(0-127) : Counter ID
Reg Desc   :
This register is used to read some packet counters , support  both mode r2c and ro
- in sace of idx_id = 0
+ pos_id = 0 : byte
+ pos_id = 1 : pkt
+ pos_id = 2 : idle
+ pos_id = 3 : unused
- in sace of idx_id = 1
+ pos_id = 0 : GFPF-SO-DATA-FRM-CNT
+ pos_id = 1 : GFPF-SO-CMF-CSF-FRM-CNT
+ pos_id = 2 : GFPF-SO-CMF-FRM-CNT
+ pos_id = 3 : GFPF-SO-MCF-FRM-CNT
- r2c       : enable mean read to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_enc_ho                                                                           0x0B4000

/*--------------------------------------
BitField Name: cnt_encho
BitField Type: R/W
BitField Desc: Counter enc high order
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_enc_ho_cnt_encho_Mask                                                               cBit31_0
#define cAf6_upen_enc_ho_cnt_encho_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : PMC GLB INF Counter GFP DEC HO
Reg Addr   : 0x0B_5000
Reg Formula: 0x0B_5000 +  $idx_id*0x400 + $pos_id*0x100 + $r2c*0x80 + $decho_id
    Where  :
           + $idx_id(0-5)
           + $pos_id(0-3) : Position ID
           + $r2c(0-1) : r2c bit
           + $decho_id(0-127) : Counter ID
Reg Desc   :
This register is used to read some packet counters , support  both mode r2c and ro
- in sace of idx_id = 0
+ pos_id = 0 : err
+ pos_id = 1 : pkt
+ pos_id = 2 : idle
+ pos_id = 3 : byte
- in sace of idx_id = 1
+ pos_id = 0 : GFPF-SK-DATA-FRM-CNT
+ pos_id = 1 : GFPF-SK-CMF-FRM-CNT
+ pos_id = 2 : GFPF-SK-THEC-HIT-CNT
+ pos_id = 3 : GFPF-SK-THEC-ERR-CNT
- in sace of idx_id = 2
+ pos_id = 0 : GFPF-SK-THEC-TYPE-CORR-CNT
+ pos_id = 1 : GFPF-SK-THEC-CRC-COR-CNT
+ pos_id = 2 : GFPF-SK-EHEC-HIT-CNT
+ pos_id = 3 : GFPF-SK-EHEC-ERR-CNT
- in sace of idx_id = 3
+ pos_id = 0 : GFPF-SK-CHEC-HIT-CNT
+ pos_id = 1 : GFPF-SK-CHEC-ERR-CNT
+ pos_id = 2 : GFPF-SK-CHEC-PLI-COR-CNT
+ pos_id = 3 : GFPF-SK-CHEC-CRC-COR-CNT
- in sace of idx_id = 4
+ pos_id = 0 : GFPF-SK-PFCS-HIT-CNT
+ pos_id = 1 : GFPF-SK-PFCS-ERR-CNT
+ pos_id = 2 : GFPF-SK-MAX-LEN-ERR-CNT
+ pos_id = 3 : GFPF-SK-MIN-LEN-ERR-CNT
- in sace of idx_id = 5
+ pos_id = 0 : GFPF-SK-PTI-UPI-DISCARDED-CNT
+ pos_id = 1 : unused
+ pos_id = 2 : unused
+ pos_id = 3 : unused
- r2c       : enable mean read to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dec_ho                                                                           0x0B5000

/*--------------------------------------
BitField Name: cnt_decho
BitField Type: R/W
BitField Desc: Counter dec high order
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dec_ho_cnt_decho_Mask                                                               cBit31_0
#define cAf6_upen_dec_ho_cnt_decho_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : PMC GLB INF Counter GFP ENC LO
Reg Addr   : 0x0B_7000
Reg Formula: 0x0B_7000 +  $idx_id*0x400 + $pos_id*0x100 + $r2c*0x80 + $encho_id
    Where  :
           + $idx_id(0-1) : index
           + $pos_id(0-3) : Position ID
           + $r2c(0-1) : r2c bit
           + $encho_id(0-127) : Counter ID
Reg Desc   :
This register is used to read some packet counters , support  both mode r2c and ro
- in sace of idx_id = 0
+ pos_id = 0 : byte
+ pos_id = 1 : pkt
+ pos_id = 2 : idle
+ pos_id = 3 : unused
- in sace of idx_id = 1
+ pos_id = 0 : GFPF-SO-DATA-FRM-CNT
+ pos_id = 1 : GFPF-SO-CMF-CSF-FRM-CNT
+ pos_id = 2 : GFPF-SO-CMF-FRM-CNT
+ pos_id = 3 : GFPF-SO-MCF-FRM-CNT
- r2c       : enable mean read to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_enc_lo                                                                           0x0B7000

/*--------------------------------------
BitField Name: cnt_enclo
BitField Type: R/W
BitField Desc: Counter enc low order
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_enc_lo_cnt_enclo_Mask                                                               cBit31_0
#define cAf6_upen_enc_lo_cnt_enclo_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : PMC GLB INF Counter GFP DEC LO
Reg Addr   : 0x0B_8000
Reg Formula: 0x0B_8000 +  $idx_id*0x400 + $pos_id*0x100 + $r2c*0x80 + $decho_id
    Where  :
           + $idx_id(0-5)
           + $pos_id(0-3) : Position ID
           + $r2c(0-1) : r2c bit
           + $decho_id(0-127) : Counter ID
Reg Desc   :
This register is used to read some packet counters , support  both mode r2c and ro
- in sace of idx_id = 0
+ pos_id = 0 : err
+ pos_id = 1 : pkt
+ pos_id = 2 : idle
+ pos_id = 3 : byte
- in sace of idx_id = 1
+ pos_id = 0 : GFPF-SK-DATA-FRM-CNT
+ pos_id = 1 : GFPF-SK-CMF-FRM-CNT
+ pos_id = 2 : GFPF-SK-THEC-HIT-CNT
+ pos_id = 3 : GFPF-SK-THEC-ERR-CNT
- in sace of idx_id = 2
+ pos_id = 0 : GFPF-SK-THEC-TYPE-CORR-CNT
+ pos_id = 1 : GFPF-SK-THEC-CRC-COR-CNT
+ pos_id = 2 : GFPF-SK-EHEC-HIT-CNT
+ pos_id = 3 : GFPF-SK-EHEC-ERR-CNT
- in sace of idx_id = 3
+ pos_id = 0 : GFPF-SK-CHEC-HIT-CNT
+ pos_id = 1 : GFPF-SK-CHEC-ERR-CNT
+ pos_id = 2 : GFPF-SK-CHEC-PLI-COR-CNT
+ pos_id = 3 : GFPF-SK-CHEC-CRC-COR-CNT
- in sace of idx_id = 4
+ pos_id = 0 : GFPF-SK-PFCS-HIT-CNT
+ pos_id = 1 : GFPF-SK-PFCS-ERR-CNT
+ pos_id = 2 : GFPF-SK-MAX-LEN-ERR-CNT
+ pos_id = 3 : GFPF-SK-MIN-LEN-ERR-CNT
- in sace of idx_id = 5
+ pos_id = 0 : GFPF-SK-PTI-UPI-DISCARDED-CNT
+ pos_id = 1 : unused
+ pos_id = 2 : unused
+ pos_id = 3 : unused
- r2c       : enable mean read to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dec_lo                                                                           0x0B8000

/*--------------------------------------
BitField Name: cnt_declo
BitField Type: R/W
BitField Desc: Counter dec low order
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dec_lo_cnt_declo_Mask                                                               cBit31_0
#define cAf6_upen_dec_lo_cnt_declo_Shift                                                                     0

/*------------------------------------------------------------------------------
Reg Name   : PMC GLB INF Counter rxflow
Reg Addr   : 0x05_0000
Reg Formula: 0x05_0000 + $idx_id*0x1_0000 +  $pos_id*0x4000 + $r2c*0x2000 + $rxflow_id
    Where  :
           + $idx_id(0-2)
           + $pos_id(0-3) : Position ID
           + $r2c(0-1) : r2c bit
           + $rxflow_id(0-5375) : Counter ID
Reg Desc   :
This register is used to read some packet counters , support  both mode r2c and ro
- in sace of idx_id = 0
+ pos_id = 0 : rxbytecnt
+ pos_id = 1 : rxpktcnt
+ pos_id = 2 : rxdisbyte
+ pos_id = 3 : rxdispkt
- in sace of idx_id = 1
+ pos_id = 0 : rxfrg
+ pos_id = 1 : rxnullfrg
+ pos_id = 2 : rxBECN
+ pos_id = 3 : rxFECN
- in sace of idx_id = 2
+ pos_id = 0 : rxDE
+ pos_id = 1 : unused
+ pos_id = 2 : unused
+ pos_id = 3 : unused
- r2c       : enable mean read to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxflow                                                                           0x050000

/*--------------------------------------
BitField Name: cnt_rxflow
BitField Type: R/W
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_rxflow_cnt_rxflow_Mask                                                              cBit31_0
#define cAf6_upen_rxflow_cnt_rxflow_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : PMC GLB INF Counter txflow
Reg Addr   : 0x08_0000
Reg Formula: 0x08_0000 + $idx_id*0x1_0000 +  $pos_id*0x4000 + $r2c*0x2000 + $txflow_id
    Where  :
           + $idx_id(0-2)
           + $pos_id(0-3) : Position ID
           + $r2c(0-1) : r2c bit
           + $txflow_id(0-5375) : Counter ID
Reg Desc   :
This register is used to read some packet counters , support  both mode r2c and ro
- in sace of idx_id = 0
+ pos_id = 0 : txbytecnt
+ pos_id = 1 : txpktcnt
+ pos_id = 2 : txdisbyte
+ pos_id = 3 : txdispkt
- in sace of idx_id = 1
+ pos_id = 0 : txfrg
+ pos_id = 1 : txnullfrg
+ pos_id = 2 : txBECN
+ pos_id = 3 : txFECN
- in sace of idx_id = 2
+ pos_id = 0 : txDE
+ pos_id = 1 : unused
+ pos_id = 2 : unused
+ pos_id = 3 : unused
- r2c       : enable mean read to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txflow                                                                           0x080000

/*--------------------------------------
BitField Name: cnt_txflow
BitField Type: R/W
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_txflow_cnt_txflow_Mask                                                              cBit31_0
#define cAf6_upen_txflow_cnt_txflow_Shift                                                                    0

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021MODULEPMCREG_H_ */

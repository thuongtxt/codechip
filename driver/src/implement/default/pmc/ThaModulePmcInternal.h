/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PMC
 * 
 * File        : ThaModulePmcInternal.h
 * 
 * Created Date: Dec 24, 2012
 *
 * Description : Performance counters module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEPMCINTERNAL_H_
#define _THAMODULEPMCINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/man/AtModuleInternal.h"
#include "../sdh/ThaSdhLineInternal.h"
#include "ThaModulePmc.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/* Ethernet port */
#define cThaEthTypePacketJumbo                  0
#define cThaEthTypePacketLessThan64             1
#define cThaEthTypePacket65To128                2
#define cThaEthTypePacket129To256               3
#define cThaEthTypePacket257To512               4
#define cThaEthTypePacket513To1024              5
#define cThaEthTypePacket1025To1528             6
#define cThaEthTypePacket                       7
#define cThaEthTypeByte                         8

#define cThaEthTypeRxErrPhyPacket               0
#define cThaEthTypeRxErrPsnPacket               1
#define cThaEthTypeRxErrEthPacket               2
#define cThaEthTypeRxErrMefPacket               3

/*--------------------------- Macros -----------------------------------------*/
#define mRo(clear) ((clear) ? 0 : 1)

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModulePmcMethods
    {
    uint32 (*BaseAddress)(ThaModulePmc self, uint8 partId);
    uint32 (*PwCounterOffset)(ThaModulePmc self, AtPw pw, uint32 cntIndex, eBool clear);
    uint32 (*PwByteCounterOffset)(ThaModulePmc self, AtPw pw, eBool clear);
    uint32 (*LineCounterOffset)(ThaModulePmc self, ThaSdhLine line, uint32 cntIndex, eBool clear);
    uint32 (*AuVcDefaultOffset)(ThaModulePmc self, AtSdhChannel sdhChannel);
    uint32 (*Vc1xDefaultOffset)(ThaModulePmc self, AtSdhChannel sdhChannel);
    uint32 (*De1DefaultOffset)(ThaModulePmc self, AtPdhChannel de1);
    uint32 (*De3DefaultOffset)(ThaModulePmc self, AtPdhChannel de3);
    eBool (*HasLongAccess)(ThaModulePmc self);

    uint32 (*AuVcRegOffsetCoef)(ThaModulePmc self);
    uint32 (*Vc1xRegOffsetCoef)(ThaModulePmc self);
    uint32 (*De3RegOffsetCoef)(ThaModulePmc self);

    uint32 (*AuVcPohCounter1Reg)(ThaModulePmc self);
    uint32 (*AuVcPohCounter2Reg)(ThaModulePmc self);
    uint32 (*AuVcPointerCounter1Reg)(ThaModulePmc self);
    uint32 (*AuVcPointerCounter2Reg)(ThaModulePmc self);
    uint32 (*Vc1xPohCounter1Reg)(ThaModulePmc self, uint8 groupId);
    uint32 (*Vc1xPohCounter2Reg)(ThaModulePmc self, uint8 groupId);

    eBool  (*VcTu1xCounterGroupDivided)(ThaModulePmc self);
    eBool  (*LineSupportedBlockCounters)(ThaModulePmc self);

    uint32 (*De1Counter1Reg)(ThaModulePmc self, uint8 groupId);
    uint32 (*De1Counter2Reg)(ThaModulePmc self, uint8 groupId);

    uint32 (*LineCounter1Reg)(ThaModulePmc self);
    uint32 (*LineCounter2Reg)(ThaModulePmc self);

    uint32 (*SdhLineCounterGet)(ThaModulePmc self, AtSdhChannel channel, uint16 counterType, eBool clear);
    uint32 (*SdhLineBlockErrorCounterGet)(ThaModulePmc self, AtSdhChannel channel, uint16 counterType, eBool clear);
    uint32 (*AuVcPointerCounterGet)(ThaModulePmc self, AtSdhChannel channel, uint16 counterType, eBool clear);
    uint32 (*AuVcPohCounterGet)(ThaModulePmc self, AtSdhChannel channel, uint16 counterType, eBool clear);
    uint32 (*TuVc1xPointerCounterGet)(ThaModulePmc self, AtSdhChannel channel, uint16 counterType, eBool clear);
    uint32 (*VcDe1CounterGet)(ThaModulePmc self, AtPdhChannel channel, uint16 counterType, eBool clear);
    uint32 (*VcDe3CounterGet)(ThaModulePmc self, AtPdhChannel channel, uint16 counterType, eBool clear);

    uint32 (*PwTxPacketsGet)(ThaModulePmc self, AtPw pw, eBool clear);
    uint32 (*PwTxBytesGet)(ThaModulePmc self, AtPw pw, eBool clear);
    uint32 (*PwTxLbitPacketsGet)(ThaModulePmc self, AtPw pw, eBool clear);
    uint32 (*PwTxRbitPacketsGet)(ThaModulePmc self, AtPw pw, eBool clear);
    uint32 (*PwTxMbitPacketsGet)(ThaModulePmc self, AtPw pw, eBool clear);
    uint32 (*PwTxPbitPacketsGet)(ThaModulePmc self, AtPw pw, eBool clear);
    uint32 (*PwTxNbitPacketsGet)(ThaModulePmc self, AtPw pw, eBool clear);
    uint32 (*PwRxPacketsGet)(ThaModulePmc self, AtPw pw, eBool clear);
    uint32 (*PwRxMalformedPacketsGet)(ThaModulePmc self, AtPw pw, eBool clear);
    uint32 (*PwRxStrayPacketsGet)(ThaModulePmc self, AtPw pw, eBool clear);
    uint32 (*PwRxLbitPacketsGet)(ThaModulePmc self, AtPw pw, eBool clear);
    uint32 (*PwRxRbitPacketsGet)(ThaModulePmc self, AtPw pw, eBool clear);
    uint32 (*PwRxMbitPacketsGet)(ThaModulePmc self, AtPw pw, eBool clear);
    uint32 (*PwRxPbitPacketsGet)(ThaModulePmc self, AtPw pw, eBool clear);
    uint32 (*PwRxNbitPacketsGet)(ThaModulePmc self, AtPw pw, eBool clear);
    uint32 (*PwRxBytesGet)(ThaModulePmc self, AtPw pw, eBool clear);
    uint32 (*PwRxLostPacketsGet)(ThaModulePmc self, AtPw pw, eBool clear);
    uint32 (*PwRxReorderedPacketsGet)(ThaModulePmc self, AtPw pw, eBool clear);
    uint32 (*PwRxOutOfSeqDropedPacketsGet)(ThaModulePmc self, AtPw pw, eBool clear);
    uint32 (*PwRxJitBufOverrunGet)(ThaModulePmc self, AtPw pw, eBool clear);
    uint32 (*PwRxJitBufUnderrunGet)(ThaModulePmc self, AtPw pw, eBool clear);
    uint32 (*PwRxLopsGet)(ThaModulePmc self, AtPw pw, eBool clear);

    /* Register */
    uint32 (*Reg_upen_cnttxoam_rc_Address)(ThaModulePmc self);
    uint32 (*Reg_MACGE_tx_type_packet_Address)(ThaModulePmc self);
    uint32 (*Reg_upen_cla0_ro_Address)(ThaModulePmc self);
    uint32 (*EthPortCounterLocalAddress)(ThaModulePmc self, uint8 portType, uint16 counterType, eBool clear);
    uint32 (*HwCounterRead2Clear)(ThaModulePmc self, uint32 address, eBool clear);

    /* To control PM tick */
    eAtRet (*TickModeSet)(ThaModulePmc self, eThaModulePmcTickMode mode);
    eThaModulePmcTickMode (*TickModeGet)(ThaModulePmc self);
    eAtRet (*Tick)(ThaModulePmc self);

    /* GFP counter offset */
    uint32 (*GfpCounterOffset)(ThaModulePmc self, AtGfpChannel channel, uint32 cntIndex, uint32 cntPosId, eBool clear);

    /* TX GFP */
    uint32 (*GfpTxBytesGet)(ThaModulePmc self, AtGfpChannel channel, eBool clear);
    uint32 (*GfpTxIdleFramesGet)(ThaModulePmc self, AtGfpChannel channel, eBool clear);
    uint32 (*GfpTxFramesGet)(ThaModulePmc self, AtGfpChannel channel, eBool clear);
    uint32 (*GfpTxCmfGet)(ThaModulePmc self, AtGfpChannel channel, eBool clear);
    uint32 (*GfpTxCsfGet)(ThaModulePmc self, AtGfpChannel channel, eBool clear);

    /* RX GFP */
    uint32 (*GfpRxBytesGet)(ThaModulePmc self, AtGfpChannel channel, eBool clear);
    uint32 (*GfpRxIdleFramesGet)(ThaModulePmc self, AtGfpChannel channel, eBool clear);
    uint32 (*GfpRxFramesGet)(ThaModulePmc self, AtGfpChannel channel, eBool clear);
    uint32 (*GfpRxCmfGet)(ThaModulePmc self, AtGfpChannel channel, eBool clear);
    uint32 (*GfpRxCHecCorrected)(ThaModulePmc self, AtGfpChannel channel, eBool clear);
    uint32 (*GfpRxCHecUncorrErrorGet)(ThaModulePmc self, AtGfpChannel channel, eBool clear);
    uint32 (*GfpRxTHecCorrected)(ThaModulePmc self, AtGfpChannel channel, eBool clear);
    uint32 (*GfpRxTHecUncorrErrorGet)(ThaModulePmc self, AtGfpChannel channel, eBool clear);
    uint32 (*GfpRxFcsError)(ThaModulePmc self, AtGfpChannel channel, eBool clear);
    uint32 (*GfpRxDrops)(ThaModulePmc self, AtGfpChannel channel, eBool clear);
    uint32 (*GfpRxUpmGet)(ThaModulePmc self, AtGfpChannel channel, eBool clear);
    uint32 (*GfpRxEHecErrorGet)(ThaModulePmc self, AtGfpChannel channel, eBool clear);

    /* TX eth flow */
    uint32 (*EthFlowTxBytes)(ThaModulePmc self, AtEthFlow channel, eBool clear);
    uint32 (*EthFlowTxPackets)(ThaModulePmc self, AtEthFlow channel, eBool clear);
    uint32 (*EthFlowTxFragmentPackets)(ThaModulePmc self, AtEthFlow channel, eBool clear);
    uint32 (*EthFlowTxNullFragmentPackets)(ThaModulePmc self, AtEthFlow channel, eBool clear);
    uint32 (*EthFlowTxDiscardPackets)(ThaModulePmc self, AtEthFlow channel, eBool clear);
    uint32 (*EthFlowTxDiscardBytes)(ThaModulePmc self, AtEthFlow channel, eBool clear);
    uint32 (*EthFlowTxBECNPackets)(ThaModulePmc self, AtEthFlow channel, eBool clear);
    uint32 (*EthFlowTxFECNPackets)(ThaModulePmc self, AtEthFlow channel, eBool clear);
    uint32 (*EthFlowTxDEPackets)(ThaModulePmc self, AtEthFlow channel, eBool clear);

    /* RX eth flow */
    uint32 (*EthFlowRxBytes)(ThaModulePmc self, AtEthFlow channel, eBool clear);
    uint32 (*EthFlowRxPackets)(ThaModulePmc self, AtEthFlow channel, eBool clear);
    uint32 (*EthFlowRxFragmentPackets)(ThaModulePmc self, AtEthFlow channel, eBool clear);
    uint32 (*EthFlowRxNullFragmentPackets)(ThaModulePmc self, AtEthFlow channel, eBool clear);
    uint32 (*EthFlowRxDiscardPackets)(ThaModulePmc self, AtEthFlow channel, eBool clear);
    uint32 (*EthFlowRxDiscardBytes)(ThaModulePmc self, AtEthFlow channel, eBool clear);
    uint32 (*EthFlowRxBECNPackets)(ThaModulePmc self, AtEthFlow channel, eBool clear);
    uint32 (*EthFlowRxFECNPackets)(ThaModulePmc self, AtEthFlow channel, eBool clear);
    uint32 (*EthFlowRxDEPackets)(ThaModulePmc self, AtEthFlow channel, eBool clear);
    uint32 (*EthFlowCounterOffset)(ThaModulePmc self, AtEthFlow channel, uint32 cntPosId, uint32 cntIndex, eBool clear);

	}tThaModulePmcMethods;

typedef struct tThaModulePmc
    {
    tAtModule super;
    const tThaModulePmcMethods *methods;
    }tThaModulePmc;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtIterator ThaModulePmcMpegRegisterIteratorCreate(AtModule module);
AtIterator ThaModulePmcMpigRegisterIteratorCreate(AtModule module);
AtModule ThaModuleStmPmcMpigObjectInit(AtModule self, AtDevice device);
AtModule ThaModulePmcObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEPMCINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PMC
 * 
 * File        : ThaModulePmc.h
 * 
 * Created Date: Dec 24, 2012
 *
 * Description : Module PMC
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEPMC_H_
#define _THAMODULEPMC_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPw.h"
#include "../poh/ThaPohProcessor.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModulePmc * ThaModulePmc;

typedef enum eThaModulePmcEthPortType
    {
    cThaModulePmcEthPortType10GPort,
    cThaModulePmcEthPortType2Dot5GPort,
    cThaModulePmcEthPortTypeDimCtrlPort,
    cThaModulePmcEthPortTypeDccPort
    }eThaModulePmcEthPortType;

typedef enum eThaModulePmcTickMode
    {
    cThaModulePmcTickModeInvalid,
    cThaModulePmcTickModeAuto,
    cThaModulePmcTickModeManual
    }eThaModulePmcTickMode;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule ThaModulePmcMpigNew(AtDevice device);
AtModule ThaModuleStmPmcMpigNew(AtDevice device);
AtModule ThaModulePmcNew(AtDevice device);

/* PW TX counters */
uint32 ThaModulePmcPwTxPacketsGet(ThaModulePmc self, AtPw pw, eBool clear);
uint32 ThaModulePmcPwTxBytesGet(ThaModulePmc self, AtPw pw, eBool clear);
uint32 ThaModulePmcPwTxLbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear);
uint32 ThaModulePmcPwTxRbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear);
uint32 ThaModulePmcPwTxMbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear);
uint32 ThaModulePmcPwTxPbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear);
uint32 ThaModulePmcPwTxNbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear);

/* PW RX counters */
uint32 ThaModulePmcPwRxPacketsGet(ThaModulePmc self, AtPw pw, eBool clear);
uint32 ThaModulePmcPwRxMalformedPacketsGet(ThaModulePmc self, AtPw pw, eBool clear);
uint32 ThaModulePmcPwRxStrayPacketsGet(ThaModulePmc self, AtPw pw, eBool clear);
uint32 ThaModulePmcPwRxLbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear);
uint32 ThaModulePmcPwRxRbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear);
uint32 ThaModulePmcPwRxMbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear);
uint32 ThaModulePmcPwRxNbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear);
uint32 ThaModulePmcPwRxPbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear);
uint32 ThaModulePmcPwRxBytesGet(ThaModulePmc self, AtPw pw, eBool clear);
uint32 ThaModulePmcPwRxReorderedPacketsGet(ThaModulePmc self, AtPw pw, eBool clear);
uint32 ThaModulePmcPwRxLostPacketsGet(ThaModulePmc self, AtPw pw, eBool clear);
uint32 ThaModulePmcPwRxOutOfSeqDropedPacketsGet(ThaModulePmc self, AtPw pw, eBool clear);
uint32 ThaModulePmcPwRxJitBufOverrunGet(ThaModulePmc self, AtPw pw, eBool clear);
uint32 ThaModulePmcPwRxJitBufUnderrunGet(ThaModulePmc self, AtPw pw, eBool clear);
uint32 ThaModulePmcPwRxLopsGet(ThaModulePmc self, AtPw pw, eBool clear);

/* SDH counters */
uint32 ThaModulePmcSdhLineCounterGet(AtModule self, AtSdhChannel channel, uint16 counterType, eBool clear);
uint32 ThaModulePmcSdhLineBlockErrorCounterGet(AtModule self, AtSdhChannel channel, uint16 counterType, eBool clear);
uint32 ThaModulePmcSdhAuPointerCounterGet(AtModule self, AtSdhChannel channel, uint16 counterType, eBool clear);
uint32 ThaModulePmcSdhAuVcPohCounterGet(AtModule self, AtSdhChannel channel, uint16 counterType, eBool clear);
uint32 ThaModulePmcSdhTuVc1xCounterGet(AtModule self, AtSdhChannel channel, uint16 counterType, eBool clear);

/* PDH counters */
uint32 ThaModulePmcSdhVcDe1CounterGet(AtModule self, AtPdhChannel channel, uint16 counterType, eBool clear);
uint32 ThaModulePmcSdhVcDe3CounterGet(AtModule self, AtPdhChannel channel, uint16 counterType, eBool clear);

/* Ethernet port counters */
uint32 ThaModulePmcEthPortCountersGet(ThaModulePmc self, AtEthPort port, uint8 portType, uint16 counterType,  eBool clear);
eBool ThaModulePmcEthPortCounterIsSupported(ThaModulePmc self, AtEthPort port, uint8 portType, uint16 counterType);

/* Tick */
eAtRet ThaModulePmcTickModeSet(ThaModulePmc self, eThaModulePmcTickMode mode);
eThaModulePmcTickMode ThaModulePmcTickModeGet(ThaModulePmc self);
eAtRet ThaModulePmcTick(ThaModulePmc self);

/* Utils */
eBool ThaTdmChannelShouldGetCounterFromPmc(AtChannel self);
eBool ThaSdhChannelShouldGetCounterFromPmc(AtChannel self);
eBool ThaEthPortShouldGetCounterFromPmc(AtChannel self);
uint32 ThaModulePmcBaseAddress(ThaModulePmc self);
uint32 ThaModulePmcPartBaseAddress(ThaModulePmc self, uint8 partId);

uint32 ThaModulePmcGfpTxBytesGet(ThaModulePmc self, AtGfpChannel channel, eBool clear);
uint32 ThaModulePmcGfpTxIdleFramesGet(ThaModulePmc self, AtGfpChannel channel, eBool clear);
uint32 ThaModulePmcGfpTxFramesGet(ThaModulePmc self, AtGfpChannel channel, eBool clear);
uint32 ThaModulePmcGfpTxCmfGet(ThaModulePmc self, AtGfpChannel channel, eBool clear);
uint32 ThaModulePmcGfpTxCsfGet(ThaModulePmc self, AtGfpChannel channel, eBool clear);

uint32 ThaModulePmcGfpRxBytesGet(ThaModulePmc self, AtGfpChannel channel, eBool clear);
uint32 ThaModulePmcGfpRxIdleFramesGet(ThaModulePmc self, AtGfpChannel channel, eBool clear);
uint32 ThaModulePmcGfpRxFramesGet(ThaModulePmc self, AtGfpChannel channel, eBool clear);
uint32 ThaModulePmcGfpRxCmfGet(ThaModulePmc self, AtGfpChannel channel, eBool clear);
uint32 ThaModulePmcGfpRxCHecCorrected(ThaModulePmc self, AtGfpChannel channel, eBool clear);
uint32 ThaModulePmcGfpRxCHecUncorrErrorGet(ThaModulePmc self, AtGfpChannel channel, eBool clear);
uint32 ThaModulePmcGfpRxTHecCorrected(ThaModulePmc self, AtGfpChannel channel, eBool clear);
uint32 ThaModulePmcGfpRxTHecUncorrErrorGet(ThaModulePmc self, AtGfpChannel channel, eBool clear);
uint32 ThaModulePmcGfpRxFcsError(ThaModulePmc self, AtGfpChannel channel, eBool clear);
uint32 ThaModulePmcGfpRxDrops(ThaModulePmc self, AtGfpChannel channel, eBool clear);
uint32 ThaModulePmcGfpRxUpmGet(ThaModulePmc self, AtGfpChannel channel, eBool clear);
uint32 ThaModulePmcGfpRxEHecErrorGet(ThaModulePmc self, AtGfpChannel channel, eBool clear);

uint32 ThaModulePmcEthFlowTxBytesGet(ThaModulePmc self, AtEthFlow channel, eBool clear);
uint32 ThaModulePmcEthFlowTxPacketsGet(ThaModulePmc self, AtEthFlow channel, eBool clear);
uint32 ThaModulePmcEthFlowTxFragmentPacketsGet(ThaModulePmc self, AtEthFlow channel, eBool clear);
uint32 ThaModulePmcEthFlowTxNullFragmentPacketsGet(ThaModulePmc self, AtEthFlow channel, eBool clear);
uint32 ThaModulePmcEthFlowTxDiscardPacketsGet(ThaModulePmc self, AtEthFlow channel, eBool clear);
uint32 ThaModulePmcEthFlowTxDiscardBytesGet(ThaModulePmc self, AtEthFlow channel, eBool clear);
uint32 ThaModulePmcEthFlowTxBECNPacketsGet(ThaModulePmc self, AtEthFlow channel, eBool clear);
uint32 ThaModulePmcEthFlowTxFECNPacketsGet(ThaModulePmc self, AtEthFlow channel, eBool clear);
uint32 ThaModulePmcEthFlowTxDEPacketsGet(ThaModulePmc self, AtEthFlow channel, eBool clear);

uint32 ThaModulePmcEthFlowRxBytesGet(ThaModulePmc self, AtEthFlow channel, eBool clear);
uint32 ThaModulePmcEthFlowRxPacketsGet(ThaModulePmc self, AtEthFlow channel, eBool clear);
uint32 ThaModulePmcEthFlowRxFragmentPacketsGet(ThaModulePmc self, AtEthFlow channel, eBool clear);
uint32 ThaModulePmcEthFlowRxNullFragmentPacketsGet(ThaModulePmc self, AtEthFlow channel, eBool clear);
uint32 ThaModulePmcEthFlowRxDiscardPacketsGet(ThaModulePmc self, AtEthFlow channel, eBool clear);
uint32 ThaModulePmcEthFlowRxDiscardBytesGet(ThaModulePmc self, AtEthFlow channel, eBool clear);
uint32 ThaModulePmcEthFlowRxBECNPacketsGet(ThaModulePmc self, AtEthFlow channel, eBool clear);
uint32 ThaModulePmcEthFlowRxFECNPacketsGet(ThaModulePmc self, AtEthFlow channel, eBool clear);
uint32 ThaModulePmcEthFlowRxDEPacketsGet(ThaModulePmc self, AtEthFlow channel, eBool clear);

/* Product concretes */
AtModule Tha60290021ModulePmcNew(AtDevice self);
AtModule Tha60290022ModulePmcNew(AtDevice self);
AtModule Tha60290022ModulePmcV2New(AtDevice self);
AtModule Tha60290081ModulePmcNew(AtDevice self);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEPMC_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PmcMpig (internal)
 *
 * File        : ThaModulePmcMpig.c
 *
 * Created Date: Sep 10, 2012
 *
 * Description : PMCMPIG internal module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaModulePmcMpigInternal.h"

#include "../../default/man/ThaDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
static uint8 m_methodsInit = 0;
static tThaModulePmcMpigMethods m_methods;

/* Override */
static tAtModuleMethods m_AtModuleOverride;
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementations */
static const tAtModuleMethods *m_AtModuleMethods = NULL;
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet Init(AtModule self)
    {
    return m_AtModuleMethods->Init(self);
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static void Delete(AtObject self)
    {
    /* Delete private data */

    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static eBool HasRegister(AtModule self, uint32 localAddress)
    {
    ThaModulePmcMpig mpigModule = (ThaModulePmcMpig)self;
    uint32 baseAddress = mMethodsGet(mpigModule)->BaseAddress(mpigModule);

    /* increase range counter */
    if ((localAddress >= (0x0 + baseAddress)) && (localAddress <= (0xFFFFF + baseAddress)))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 *HoldRegistersGet(AtModule self, uint16 *numberOfHoldRegisters)
    {
    ThaModulePmcMpig mpigModule = (ThaModulePmcMpig)self;
    uint32 baseAddress = mMethodsGet(mpigModule)->BaseAddress(mpigModule);
    static eBool initialized = cAtFalse;
    static uint32 holdRegisters[3];
    static const uint8 numHoldRegs = 3;
    uint8 i;

    /* Initialize hold registers */
    if (!initialized)
        {
        initialized = cAtTrue;
        for (i = 0; i < numHoldRegs; i++)
            holdRegisters[i] = 0x10 + baseAddress + i;

        initialized = cAtTrue;
        }

    /* Number of hold registers */
    if (numberOfHoldRegisters)
        *numberOfHoldRegisters = numHoldRegs;

    return holdRegisters;
    }

static uint32 BaseAddress(ThaModulePmcMpig self)
    {
	AtUnused(self);
    return 0xC00000;
    }

static AtLongRegisterAccess LongRegisterAccessCreate(AtModule self)
    {
    uint16 numHoldRegisters;
    uint32 *holdRegisters = AtModuleHoldRegistersGet(self, &numHoldRegisters);
    return AtDefaultLongRegisterAccessNew(holdRegisters, numHoldRegisters, holdRegisters, numHoldRegisters);
    }

static void OverrideAtObject(AtModule self)
    {
    AtObject object = (AtObject)self;

    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, HasRegister);
        mMethodOverride(m_AtModuleOverride, HoldRegistersGet);
        mMethodOverride(m_AtModuleOverride, LongRegisterAccessCreate);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void MethodsInit(ThaModulePmcMpig self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));
        mMethodOverride(m_methods, BaseAddress);
        }

    mMethodsSet(self, &m_methods);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideAtObject(self);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaModulePmcMpig));

    /* Super constructor */
    if (AtModuleObjectInit(self, cThaModulePmcMpig, device) == NULL)
        return NULL;

    /* Override */
    Override(self);
    MethodsInit((ThaModulePmcMpig)self);

    /* Initialize one time */
    m_methodsInit = 1;

    return self;
    }

AtModule ThaModulePmcMpigNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, sizeof(tThaModulePmcMpig));
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PMCMPIG internal module
 * 
 * File        : ThaModulePmcMpigInternal.h
 * 
 * Created Date: Sep 10, 2012
 *
 * Description :
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEPMCMPIGINTERNAL_H_
#define _THAMODULEPMCMPIGINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/man/AtModuleInternal.h"
#include "../man/ThaModuleClasses.h"
#include "ThaModulePmcMpig.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModulePmcMpigMethods
    {
    uint32 (*BaseAddress)(ThaModulePmcMpig self);
    }tThaModulePmcMpigMethods;

typedef struct tThaModulePmcMpig
    {
    tAtModule super;
    const tThaModulePmcMpigMethods *methods;

    }tThaModulePmcMpig;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEPMCMPIGINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PMCMPEG (internal module)
 * 
 * File        : ThaModulePmcMpeg.h
 * 
 * Created Date: Sep 10, 2012
 *
 * Description :
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEPMCMPEG_H_
#define _THAMODULEPMCMPEG_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaModulePmcMpegInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule ThaModulePmcMpegNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEPMCMPEG_H_ */


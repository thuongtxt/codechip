/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PMCMPIG (internal module)
 * 
 * File        : ThaModulePmcMpig.h
 * 
 * Created Date: Sep 10, 2012
 *
 * Description :
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEPMCMPIG_H_
#define _THAMODULEPMCMPIG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule ThaModulePmcMpigNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEPMCMPIG_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : ThaPwActivatorInternal.h
 * 
 * Created Date: Jul 29, 2013
 *
 * Description : PW Activator
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPWACTIVATORINTERNAL_H_
#define _THAPWACTIVATORINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h"
#include "AtList.h"
#include "ThaPwActivator.h"
#include "../../../../generic/man/AtDriverInternal.h"
#include "../../util/ThaBitMask.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPwActivatorMethods
    {
    /* Binding */
    eBool (*PwBindingInformationIsValid)(ThaPwActivator self, ThaPwAdapter adapter, AtChannel circuit, AtEthPort ethPort);
    eBool (*PwCanBeActivatedWithCircuit)(ThaPwActivator self, ThaPwAdapter adapter, AtChannel circuit);
    eAtModulePwRet (*PwCircuitBind)(ThaPwActivator self, ThaPwAdapter adapter, AtChannel circuit);
    eAtModulePwRet (*PwCircuitUnbind)(ThaPwActivator self, ThaPwAdapter adapter);
    eBool (*PwCanBeActivatedWithEthPort)(ThaPwActivator self, ThaPwAdapter adapter, AtEthPort ethPort);
    eAtRet (*PwEthPortSet)(ThaPwActivator self, ThaPwAdapter adapter, AtEthPort ethPort);
    eAtRet (*PwEthFlowBind)(ThaPwActivator self, ThaPwAdapter adapter, AtEthFlow ethFlow);
    eAtRet (*PwEthFlowUnbind)(ThaPwActivator self, ThaPwAdapter adapter);
    eBool (*PsnIsUsed)(ThaPwActivator self, ThaPwAdapter adapter, AtPwPsn psn);
    eBool (*VlanTagsAreUsed)(ThaPwActivator self, ThaPwAdapter adapter, tAtEthVlanTag *expectedCVlan, tAtEthVlanTag *expectedSVlan);

    /* Hardware resource management */
    ThaHwPw (*HwPwAllocate)(ThaPwActivator self, ThaPwAdapter adapter);
    eAtRet (*HwPwDeallocate)(ThaPwActivator self, ThaPwAdapter adapter);
    eAtRet (*PwDelete)(ThaPwActivator self, ThaPwAdapter adapter);

    eAtRet (*Activate)(ThaPwActivator self, ThaPwAdapter adapter);
    eAtRet (*Deactivate)(ThaPwActivator self, ThaPwAdapter adapter);
    eAtModulePwRet (*PwCircuitConnect)(ThaPwActivator self, ThaPwAdapter adapter, AtChannel circuit);
    eAtRet (*PwPayloadSizeReset)(ThaPwActivator self, ThaPwAdapter adapter);
    eAtRet (*HbceActivate)(ThaPwActivator self, ThaPwAdapter adapter);

    eBool (*ShouldControlPwBandwidth)(ThaPwActivator self);
    eBool (*ShouldControlJitterBuffer)(ThaPwActivator self);

    /* Debug */
    void (*Debug)(ThaPwActivator self);
    }tThaPwActivatorMethods;

typedef struct tThaPwActivator
    {
    tAtObject super;
    const tThaPwActivatorMethods *methods;

    /* Private data */
    AtModulePw pwModule;
    }tThaPwActivator;

typedef struct tThaPwDynamicActivator * ThaPwDynamicActivator;

typedef struct tThaPwDynamicActivatorMethods
    {
    eAtRet (*ActivateConfiguration)(ThaPwDynamicActivator self, ThaPwAdapter adapter);
    eAtRet (*DeactivateConfiguration)(ThaPwDynamicActivator self, ThaPwAdapter adapter);
    }tThaPwDynamicActivatorMethods;

typedef struct tThaPwDynamicActivator
    {
    tThaPwActivator super;
    const tThaPwDynamicActivatorMethods *methods;

    /* Private data */
    ThaBitMask *partMasks;
    AtList deactivatedPws;
    }tThaPwDynamicActivator;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPwActivator ThaPwActivatorObjectInit(ThaPwActivator self, AtModulePw pwModule);
ThaModulePw ThaPwActivatorModuleGet(ThaPwActivator self);
eAtModulePwRet ThaPwActivatorPwCircuitConnect(ThaPwActivator self, ThaPwAdapter adapter, AtChannel circuit);
eAtModulePwRet ThaPwActivatorPwCircuitDisconnect(ThaPwActivator self, ThaPwAdapter adapter);
ThaPwActivator ThaPwDynamicActivatorObjectInit(ThaPwActivator self, AtModulePw pwModule);
eAtRet ThaPwDynamicActivatorPwDeactivate(ThaPwActivator self, ThaPwAdapter adapter);
eAtRet ThaPwDynamicActivatorResourceDeallocate(ThaPwActivator self, ThaPwAdapter adapter);

#ifdef __cplusplus
}
#endif
#endif /* _THAPWACTIVATORINTERNAL_H_ */


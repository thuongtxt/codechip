/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : ThaPwDynamicActivator.c
 *
 * Created Date: Jul 29, 2013
 *
 * Description : Dynamic activator
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/util/AtUtil.h"
#include "../../eth/ThaModuleEth.h"
#include "../../eth/controller/ThaEthFlowController.h"
#include "../../cos/ThaModuleCos.h"
#include "../../cla/pw/ThaModuleClaPw.h"
#include "../../cla/hbce/ThaHbce.h"
#include "../../pwe/ThaModulePwe.h"
#include "../../man/ThaDeviceInternal.h"
#include "../../pw/adapters/ThaPwAdapter.h"
#include "../defectcontrollers/ThaPwDefectController.h"
#include "../headercontrollers/ThaPwHeaderControllerInternal.h"
#include "ThaPwActivatorInternal.h"
#include "AtPwCep.h"

/*--------------------------- Define -----------------------------------------*/
#define cInvalidValue cBit31_0

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaPwDynamicActivator)self)
#define mApiCall(apiCall)          \
    do                             \
        {                          \
        ret = apiCall;             \
        if (ret != cAtOk)          \
            return ret;            \
        } while(0)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaPwDynamicActivatorMethods m_methods;

/* Override */
static tThaPwActivatorMethods m_ThaPwActivatorOverride;
static tAtObjectMethods       m_AtObjectOverride;

/* Save super implementation */
static const tThaPwActivatorMethods *m_ThaPwActivatorMethods = NULL;
static const tAtObjectMethods       *m_AtObjectMethods       = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaPwDynamicActivator);
    }

static AtList DeactivatedPws(ThaPwDynamicActivator self)
    {
    AtModulePw pwModule = (AtModulePw)ThaPwActivatorModuleGet((ThaPwActivator)self);
    if (self->deactivatedPws == NULL)
        self->deactivatedPws = AtListCreate(AtModulePwMaxPwsGet(pwModule));
    return self->deactivatedPws;
    }

static uint8 NumParts(ThaPwActivator self)
    {
    ThaModulePw pwModule = ThaPwActivatorModuleGet(self);
    return ThaModulePwNumParts(pwModule);
    }

static uint32 NumPwsPerPart(ThaPwActivator self)
    {
    ThaModulePw pwModule = ThaPwActivatorModuleGet(self);
    return ThaModulePwNumPwsPerPart(pwModule);
    }

static void DeleteMasks(ThaPwActivator self)
    {
    uint32 part_i;
    AtOsal osal;

    if (mThis(self)->partMasks == NULL)
        return;

    /* Delete mask for each part */
    for (part_i = 0; part_i < NumParts(self); part_i++)
        AtObjectDelete((AtObject)mThis(self)->partMasks[part_i]);

    /* And the memory that hodl all masks of all part */
    osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemFree(osal, mThis(self)->partMasks);
    mThis(self)->partMasks = NULL;
    }

static ThaBitMask *CreateMasks(ThaPwActivator self)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 memorySize;
    ThaBitMask *allPartMasks;
    uint32 part_i;

    /* Create masks for all parts */
    memorySize = sizeof(ThaBitMask) * NumParts(self);
    allPartMasks = mMethodsGet(osal)->MemAlloc(osal, memorySize);
    if (allPartMasks == NULL)
        return NULL;
    mMethodsGet(osal)->MemInit(osal, allPartMasks, 0, memorySize);

    /* Create mask for each parts */
    for (part_i = 0; part_i < NumParts(self); part_i++)
        {
        allPartMasks[part_i] = ThaBitMaskNew(NumPwsPerPart(self));
        if (allPartMasks[part_i] == NULL)
            {
            uint32 existingPart_i;

            for (existingPart_i = 0; existingPart_i < part_i; existingPart_i++)
                AtObjectDelete((AtObject)allPartMasks[existingPart_i]);
            mMethodsGet(osal)->MemFree(osal, allPartMasks);

            return NULL;
            }
        }

    return allPartMasks;
    }

static eBool PwCanBeActivatedWithCircuit(ThaPwActivator self, ThaPwAdapter adapter, AtChannel circuit)
    {
    if ((AtPwEthPortGet((AtPw)adapter) == NULL) || (circuit == NULL))
        return cAtFalse;

    return m_ThaPwActivatorMethods->PwCanBeActivatedWithCircuit(self, adapter, circuit);
    }

static eBool PwCanBeActivatedWithEthPort(ThaPwActivator self, ThaPwAdapter adapter, AtEthPort ethPort)
    {
    if (((AtPwBoundCircuitGet((AtPw)adapter)) == NULL) || (ethPort == NULL))
        return cAtFalse;

    return m_ThaPwActivatorMethods->PwCanBeActivatedWithEthPort(self, adapter, ethPort);
    }

static eAtRet ActivateConfiguration(ThaPwDynamicActivator self, ThaPwAdapter adapter)
    {
    tThaPwConfigCache *configureCache = ThaPwAdapterCache(adapter);
    ThaPwHeaderController controller = ThaPwAdapterHeaderController(adapter);
    ThaPwHeaderController backupController = ThaPwAdapterBackupHeaderController(adapter);
    tThaPwHeaderCache *headerCache = ThaPwHeaderControllerCache(controller);
    tThaPwHeaderCache *backupheaderCache = ThaPwHeaderControllerCache(backupController);
    eAtRet ret;
    AtPw pw;
    tAtEthVlanTag *cVlan, *sVlan;
    ThaModulePw pwModule;

	AtUnused(self);

    if (configureCache == NULL)
        return cAtErrorNullPointer;

    pw = (AtPw)adapter;
    if (ThaPwTypeIsCesCep(pw))
        {
        mApiCall(AtPwSuppressEnable(pw, configureCache->suppressed));
        mApiCall(AtPwReorderingEnable(pw, configureCache->reorderingEnabled));

        /* Control word default configuration */
        mApiCall(AtPwCwEnable(pw, configureCache->cwEnabled));
        mApiCall(AtPwCwAutoRBitEnable(pw, configureCache->cwAutoRBitEnabled));
        mApiCall(AtPwCwAutoTxLBitEnable(pw, configureCache->cwAutoTxLBitEnabled));
        mApiCall(AtPwCwAutoRxLBitEnable(pw, configureCache->cwAutoRxLBitEnabled));
        mApiCall(AtPwCwSequenceModeSet(pw, configureCache->cwSequenceMode));
        mApiCall(AtPwCwPktReplaceModeSet(pw, configureCache->cwPktReplaceMode));
        mApiCall(AtPwCwLengthModeSet(pw, configureCache->cwLengthMode));

        if (mMethodsGet(pw)->CanControlLopsPktReplaceMode(pw))
            mApiCall(AtPwLopsPktReplaceModeSet(pw, configureCache->lopsPktReplaceMode));

        if (AtPwTypeGet(pw) == cAtPwTypeCESoP)
            {
            AtPwCESoP cesop = (AtPwCESoP)ThaPwAdapterPwGet(adapter);
            mApiCall(AtPwCESoPCwAutoTxMBitEnable(cesop, configureCache->cwAutoTxMBitEnabled));
            if (AtPwCESoPCwAutoRxMBitIsConfigurable(cesop))
                mApiCall(AtPwCESoPCwAutoRxMBitEnable(cesop, configureCache->cwAutoRxMBitEnabled));
            if (AtModulePwCasIdleCodeIsSupported((AtModulePw)AtChannelModuleGet((AtChannel)pw)))
                {
                mApiCall(AtPwCESoPCasIdleCode1Set(cesop, configureCache->casIdleCode1));
                mApiCall(AtPwCESoPCasIdleCode2Set(cesop, configureCache->casIdleCode2));
                mApiCall(AtPwCESoPCasAutoIdleEnable(cesop, configureCache->casAutoIdleEnabled));
                }
            }

        if (AtPwTypeGet(pw) == cAtPwTypeCEP)
            mApiCall(AtPwCepEparEnable((AtPwCep)ThaPwAdapterPwGet(adapter), ThaPwCepAdapterDbEparIsEnabled((ThaPwCepAdapter)adapter)));

        /* Default threshold */
        mApiCall(ThaPwDefectControllerAllThresholdsApply(ThaPwAdapterPwDefectControllerGet(adapter)));

        /* And RTP */
        mApiCall(AtPwRtpEnable(pw, configureCache->rtpEnabled));
        mApiCall(AtPwRtpTxPayloadTypeSet(pw, headerCache->txRtpPayloadType));
        mApiCall(AtPwRtpExpectedPayloadTypeSet(pw, configureCache->expectedRtpPayloadType));
        mApiCall(AtPwRtpPayloadTypeCompare(pw, configureCache->payloadTypeIsCompared));
        mApiCall(AtPwRtpTxSsrcSet(pw, headerCache->txRtpSsrc));
        mApiCall(AtPwRtpExpectedSsrcSet(pw, configureCache->expectedRtpSsrc));
        mApiCall(AtPwRtpSsrcCompare(pw, configureCache->ssrcIsCompared));
        if (configureCache->rtpTimeStampMode != cAtPwRtpTimeStampModeInvalid)
            mApiCall(AtPwRtpTimeStampModeSet(pw, configureCache->rtpTimeStampMode));

        /* IDLE code */
        mApiCall(AtPwIdleCodeSet(pw, configureCache->idleCode));

        /* Payload */
        mApiCall(AtPwPayloadSizeSet(pw, ThaPwAdapterPayloadSizeGet(adapter)));
        mApiCall(AtPwPrioritySet(pw, configureCache->priority));

        /* Jitter */
        mApiCall(AtPwJitterBufferSizeSet(pw, AtPwJitterBufferSizeGet(pw)));
        mApiCall(AtPwJitterBufferDelaySet(pw, AtPwJitterBufferDelayGet(pw)));

        /* Product specific */
        pwModule = (ThaModulePw)AtChannelModuleGet((AtChannel)pw);
        if (ThaModulePwSubportVlansSupported(pwModule))
            mApiCall(AtPwSubPortVlanIndexSet(pw, headerCache->subportVlanIndex));
        if (ThaModulePwTxActiveForceSupported(pwModule))
            mApiCall(AtPwTxActiveForce(pw, configureCache->txActiveForced));
        }

    /* Ethernet header */
    cVlan = (headerCache->numVlans == 0) ? NULL : &(headerCache->cVlan);
    sVlan = (headerCache->numVlans <= 1) ? NULL : &(headerCache->sVlan);
    mApiCall(AtPwEthHeaderSet(pw, headerCache->destMac, cVlan, sVlan));

    /* Expect VLANs */
    if (configureCache->useExpectedCVlan)
        mApiCall(AtPwEthExpectedCVlanSet(pw, &(configureCache->expectedCVlan)));
    if (configureCache->useExpectedSVlan)
        mApiCall(AtPwEthExpectedSVlanSet(pw, &(configureCache->expectedSVlan)));

    /* PSN */
    if (headerCache->psn)
        mApiCall(AtPwPsnSet(pw, headerCache->psn));

    if (backupController)
        {
        cVlan = (backupheaderCache->numVlans == 0) ? NULL : &(backupheaderCache->cVlan);
        sVlan = (backupheaderCache->numVlans <= 1) ? NULL : &(backupheaderCache->sVlan);
        mApiCall(AtPwBackupEthHeaderSet(pw, backupheaderCache->destMac, cVlan, sVlan));

        if (backupheaderCache->psn)
            mApiCall(AtPwBackupPsnSet(pw, backupheaderCache->psn));
        }

    mApiCall(AtChannelTxAlarmUnForce((AtChannel)pw, AtChannelTxForcableAlarmsGet((AtChannel)pw)));
    mApiCall(AtChannelEnable((AtChannel)pw, ThaPwAdapterIsEnabled(adapter)));

    /* It's time to free cache to save memory */
    ThaPwAdapterCacheDelete(adapter);
    ThaPwHeaderControllerCacheDelete(controller);
    ThaPwHeaderControllerCacheDelete(backupController);

    return cAtOk;
    }

static eAtRet DeactivateConfiguration(ThaPwDynamicActivator self, ThaPwAdapter adapter)
    {
    tThaPwConfigCache *cache = ThaPwAdapterCache(adapter);
    ThaPwHeaderController controller = ThaPwAdapterHeaderController(adapter);
    ThaPwHeaderController backupController = ThaPwAdapterBackupHeaderController(adapter);
    tThaPwHeaderCache *headerCache = ThaPwHeaderControllerCache(controller);
    tThaPwHeaderCache *backupheaderCache = ThaPwHeaderControllerCache(backupController);
    AtPw pw = ThaPwAdapterPwGet(adapter);
    AtOsal osal;

    AtUnused(self);

    if (cache == NULL)
        return cAtErrorNullPointer;

    /* Clear cache */
    osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, cache, 0, sizeof(tThaPwConfigCache));

    /* General configuration */
    cache->suppressed        = AtPwSuppressIsEnabled(pw);
    cache->reorderingEnabled = AtPwReorderingIsEnabled(pw);

    /* Control word default configuration */
    cache->cwEnabled           = AtPwCwIsEnabled(pw);
    cache->cwAutoRBitEnabled   = AtPwCwAutoRBitIsEnabled(pw);
    cache->cwAutoTxLBitEnabled = AtPwCwAutoTxLBitIsEnabled(pw);
    cache->cwAutoRxLBitEnabled = AtPwCwAutoRxLBitIsEnabled(pw);
    cache->cwSequenceMode      = AtPwCwSequenceModeGet(pw);
    cache->cwPktReplaceMode    = AtPwCwPktReplaceModeGet(pw);
    cache->cwLengthMode        = AtPwCwLengthModeGet(pw);
    cache->lopsPktReplaceMode  = AtPwLopsPktReplaceModeGet(pw);

    if (AtPwTypeGet(pw) == cAtPwTypeCESoP)
        {
        cache->cwAutoTxMBitEnabled = AtPwCESoPCwAutoTxMBitIsEnabled((AtPwCESoP)pw);
        cache->cwAutoRxMBitEnabled = AtPwCESoPCwAutoRxMBitIsEnabled((AtPwCESoP)pw);
        if (AtModulePwCasIdleCodeIsSupported((AtModulePw)AtChannelModuleGet((AtChannel)pw)))
            {
            cache->casIdleCode1 = AtPwCESoPCasIdleCode1Get((AtPwCESoP)pw);
            cache->casIdleCode2 = AtPwCESoPCasIdleCode2Get((AtPwCESoP)pw);
            cache->casAutoIdleEnabled = AtPwCESoPCasAutoIdleIsEnabled((AtPwCESoP)pw);
            }
        }

    /* Default threshold */
    cache->lopsClearThreshold  = AtPwLopsClearThresholdGet(pw);
    cache->lopsSetThreshold    = AtPwLopsSetThresholdGet(pw);

    /* And RTP */
    cache->rtpEnabled               = AtPwRtpIsEnabled(pw);
    headerCache->txRtpPayloadType   = AtPwRtpTxPayloadTypeGet(pw);
    cache->expectedRtpPayloadType   = AtPwRtpExpectedPayloadTypeGet(pw);
    cache->payloadTypeIsCompared    = AtPwRtpPayloadTypeIsCompared(pw);
    headerCache->txRtpSsrc          = AtPwRtpTxSsrcGet(pw);
    cache->expectedRtpSsrc          = AtPwRtpExpectedSsrcGet(pw);
    cache->ssrcIsCompared           = AtPwRtpSsrcIsCompared(pw);
    cache->rtpTimeStampMode         = AtPwRtpTimeStampModeGet(pw);

    /* IDLE code */
    cache->idleCode = AtPwIdleCodeGet(pw);

    /* Payload */
    cache->priority = AtPwPriorityGet(pw);

    /* Ethernet header */
    if (AtPwEthCVlanGet(pw, &(headerCache->cVlan)))
        headerCache->numVlans = (uint8)(headerCache->numVlans + 1);
    if (AtPwEthSVlanGet(pw, &(headerCache->sVlan)))
        headerCache->numVlans = (uint8)(headerCache->numVlans + 1);
    AtPwEthDestMacGet(pw, headerCache->destMac);

    /* Expect VLANs */
    if (AtPwEthExpectedCVlanGet(pw, &(cache->expectedCVlan)))
        cache->useExpectedCVlan = cAtTrue;
    if (AtPwEthExpectedSVlanGet(pw, &(cache->expectedSVlan)))
        cache->useExpectedSVlan = cAtTrue;

    /* PSN */
    if (AtPwPsnGet(pw))
        headerCache->psn = (AtPwPsn)AtObjectClone((AtObject)AtPwPsnGet(pw));

    if (backupController)
        {
        if (AtPwBackupEthCVlanGet(pw, &(backupheaderCache->cVlan)))
            backupheaderCache->numVlans = (uint8)(headerCache->numVlans + 1);
        if (AtPwBackupEthSVlanGet(pw, &(backupheaderCache->sVlan)))
            backupheaderCache->numVlans = (uint8)(backupheaderCache->numVlans + 1);
        AtPwBackupEthDestMacGet(pw, backupheaderCache->destMac);

        if (AtPwBackupPsnGet(pw))
            backupheaderCache->psn = (AtPwPsn)AtObjectClone((AtObject)AtPwBackupPsnGet(pw));
        }

    if (ThaModulePwNumSubPortVlans((ThaModulePw)AtChannelModuleGet((AtChannel)pw)) > 0)
        headerCache->subportVlanIndex = AtPwSubPortVlanIndexGet(pw);

    return cAtOk;
    }

static eAtRet ResourceAllocate(ThaPwActivator self, ThaPwAdapter adapter)
    {
    ThaHwPw hwPw;

    /* Resource was allocated */
    if (ThaPwAdapterHwPwGet(adapter))
        return cAtOk;

    hwPw = mMethodsGet(self)->HwPwAllocate(self, adapter);
    if (hwPw == NULL)
        return cAtErrorRsrcNoAvail;
    ThaPwAdapterHwPwSet(adapter, hwPw);

    return cAtOk;
    }

static eAtRet ResourceDeallocate(ThaPwActivator self, ThaPwAdapter adapter)
    {
    eAtRet ret;

    ret = mMethodsGet(self)->HwPwDeallocate(self, adapter);
    ThaPwAdapterHwPwSet(adapter, NULL);

    return ret;
    }

static eBool DeactivatedPwNeedCache(ThaPwActivator self, ThaPwAdapter adapter)
    {
    AtPw pwAdapter = (AtPw)adapter;
    tAtEthVlanTag cVlan, sVlan;
	AtUnused(self);

    if (AtPwPsnGet(pwAdapter) ||
        AtPwEthExpectedCVlanGet(pwAdapter, &cVlan) ||
        AtPwEthExpectedSVlanGet(pwAdapter, &sVlan))
        return cAtTrue;

    return cAtFalse;
    }

static ThaModuleClaPw ClaModule(ThaPwActivator self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)ThaPwActivatorModuleGet(self));
    return (ThaModuleClaPw)AtDeviceModuleGet(device, cThaModuleCla);
    }

static eBool IsBindingTimeOptimization(ThaPwAdapter adapter)
    {
    ThaModulePw modulePw = (ThaModulePw)AtChannelModuleGet((AtChannel)adapter);
    return ThaModulePwIsPwBindingTimeOptimization(modulePw);
    }

static eAtRet AllConfigurationActivate(ThaPwActivator self, ThaPwAdapter adapter)
    {
    eAtRet ret;
    AtPw pw = (AtPw)adapter;
    AtChannel acrDcrRefCircuit;
    eBool needOptimalBinding = IsBindingTimeOptimization(adapter);

    ret = ResourceAllocate(self, adapter);
    if (ret != cAtOk)
        return ret;

    if (needOptimalBinding)
        {
        ThaPwAdapterPwBindingInProgressSet((AtPw)adapter, cAtTrue);
        ret |= ThaPwAdapterStartRemovingForBinding((AtPw)adapter);
        }

    ret |= m_ThaPwActivatorMethods->PwEthPortSet(self, adapter,  AtPwEthPortGet(pw));
    ret |= ThaPwActivatorPwCircuitConnect(self, adapter, AtPwBoundCircuitGet(pw));

    /* If any error, circuit may not been bound successfully should return here after doing anything else */
    if (ret != cAtOk)
        {
        ret |= ResourceDeallocate(self, adapter);
        if (needOptimalBinding)
            {
            ret |= ThaPwAdapterFinishRemovingForBinding((AtPw)adapter);
            ThaPwAdapterPwBindingInProgressSet((AtPw)adapter, cAtFalse);
            }
        return ret;
        }

    ret |= mMethodsGet(mThis(self))->ActivateConfiguration(mThis(self), adapter);

    /* Application may configure ACR/DCR that ref to this PW before it is
     * activated. Now, PW comes to work, need to apply that configuration */
    acrDcrRefCircuit = ThaPwAdapterAcrDcrRefCircuitGet(adapter);
    if (acrDcrRefCircuit)
        ret |= AtChannelTimingSet(acrDcrRefCircuit,
                                  AtChannelTimingModeGet(acrDcrRefCircuit),
                                  AtChannelTimingSourceGet(acrDcrRefCircuit));

    /* It is activate, uncache it */
    if (DeactivatedPwNeedCache(self, adapter))
        AtListObjectRemove(DeactivatedPws(mThis(self)), (AtObject)ThaPwAdapterPwGet(adapter));

    if (needOptimalBinding)
        {
        ret |= ThaPwAdapterFinishRemovingForBinding((AtPw)adapter);
        ThaPwAdapterPwBindingInProgressSet((AtPw)adapter, cAtFalse);
        }

    return ret;
    }

static eAtRet Activate(ThaPwActivator self, ThaPwAdapter adapter)
    {
    eAtRet ret = cAtOk;

    /* Need to disable reconfiguring checking when activating so that both HW
     * and SW database will be sync */
    ret |= ThaPwAdapterReconfigureCheckingEnable(adapter, cAtFalse);
    ret |= AllConfigurationActivate(self, adapter);
    ret |= ThaPwAdapterReconfigureCheckingEnable(adapter, cAtTrue);

    return ret;
    }

eAtRet ThaPwDynamicActivatorPwDeactivate(ThaPwActivator self, ThaPwAdapter adapter)
    {
    eAtRet ret = cAtOk;
    AtPw pwAdapter = (AtPw)adapter;
    AtDevice device;
    ThaModulePda pdaModule;

    /* Already deactivate */
    if (ThaPwAdapterIsLogicPw(adapter))
        return cAtOk;

    /* Backup configuration */
    ret |= mMethodsGet(mThis(self))->DeactivateConfiguration(mThis(self), adapter);

    /* Reset PW */
    device = AtChannelDeviceGet((AtChannel)adapter);
    ret |= ThaPwAdapterTxHeaderReset(adapter);
    ret |= ThaPwAdapterHwPayloadSizeSet(adapter, 0);
    ret |= ThaClaPwControllerPwLookupEnable(ThaPwAdapterClaPwController(adapter), pwAdapter, cAtFalse);

    /* Both PWE and CLA are deactivated, it's time to reset PDA lookup */
    pdaModule = (ThaModulePda)AtDeviceModuleGet(device, cThaModulePda);
    ret |= ThaModulePdaPwCircuitIdReset(pdaModule, pwAdapter);

    /* Reset EPAR enable at PDA */
    if (AtPwTypeGet((AtPw)adapter) == cAtPwTypeCEP)
        ret |= ThaModulePdaPwEparEnable(pdaModule, (AtPw)adapter, cAtFalse);

    /* Although PW that is deactivated but its PSN information is still remained.
     * It needs to be cached to database for PSN conflict checking */
    if (DeactivatedPwNeedCache(self, adapter))
        AtListObjectAdd(DeactivatedPws(mThis(self)), (AtObject)ThaPwAdapterPwGet(adapter));

    return ret;
    }

static eAtModulePwRet PwCircuitBind(ThaPwActivator self, ThaPwAdapter adapter, AtChannel circuit)
    {
    if (!mMethodsGet(self)->PwCanBeActivatedWithCircuit(self, adapter, circuit))
        {
        AtChannelBoundPwSet(circuit, ThaPwAdapterPwGet(adapter));
        return cAtOk;
        }

    return Activate(self, adapter);
    }

static eAtModulePwRet PwCircuitUnbind(ThaPwActivator self, ThaPwAdapter adapter)
    {
    eAtRet ret = cAtOk;

    ret |= m_ThaPwActivatorMethods->PwCircuitUnbind(self, adapter);
    ret |= ThaPwDynamicActivatorPwDeactivate(self, adapter);
    ret |= ThaPwDynamicActivatorResourceDeallocate(self, adapter);

    return ret;
    }

static ThaBitMask MaskForPart(ThaPwActivator self, uint8 partId)
    {
    if (mThis(self)->partMasks == NULL)
        mThis(self)->partMasks = CreateMasks(self);

    if (mThis(self)->partMasks == NULL)
        return NULL;

    return mThis(self)->partMasks[partId];
    }

static uint32 FreePwForPart(ThaPwActivator self, uint32 partId)
    {
    ThaBitMask mask = MaskForPart(self, (uint8)partId);
    uint32 freeBit = ThaBitMaskFirstZeroBit(mask);
    return ThaBitMaskBitPositionIsValid(mask, freeBit) ? freeBit : cInvalidValue;
    }

static ThaHwPw HwPwAllocate(ThaPwActivator self, ThaPwAdapter adapter)
    {
    uint8 part = ThaPwAdapterPartOfCircuit(adapter, AtPwBoundCircuitGet((AtPw)adapter));
    uint32 partFreeHwId, hwPwId;
    ThaBitMask bitMask = MaskForPart(self, part);

        partFreeHwId = AtChannelIdGet((AtChannel)adapter) % NumPwsPerPart(self);
        if (ThaBitMaskBitVal(bitMask, partFreeHwId))
            partFreeHwId = FreePwForPart(self, part);

    /* No one left */
    if (partFreeHwId == cInvalidValue)
        return NULL;

    /* Have one, just use it */
    hwPwId = (part * NumPwsPerPart(self)) + partFreeHwId;
    ThaBitMaskSetBit(bitMask, partFreeHwId);

    return ThaHwPwNew(adapter, hwPwId);
    }

static eAtRet HwPwDeallocate(ThaPwActivator self, ThaPwAdapter adapter)
    {
    uint32 part;
    uint32 localPwId;

    ThaHwPw hwPw = ThaPwAdapterHwPwGet(adapter);
    if (hwPw == NULL)
        return cAtOk;

    part      = ThaHwPwIdGet(hwPw) / NumPwsPerPart(self);
    localPwId = ThaHwPwIdGet(hwPw) % NumPwsPerPart(self);
    ThaBitMaskClearBit(MaskForPart(self, (uint8)part), localPwId);
    AtObjectDelete((AtObject)hwPw);

    return cAtOk;
    }

static eAtRet PwEthPortBind(ThaPwActivator self, ThaPwAdapter adapter, AtEthPort ethPort)
    {
    if (!mMethodsGet(self)->PwCanBeActivatedWithEthPort(self, adapter, ethPort))
        return cAtOk;

    return Activate(self, adapter);
    }

static eAtRet PwEthPortUnBind(ThaPwActivator self, ThaPwAdapter adapter)
    {
    eAtRet ret = cAtOk;

    ret |= m_ThaPwActivatorMethods->PwEthPortSet(self, adapter, NULL);
    ret |= ThaPwDynamicActivatorPwDeactivate(self, adapter);
    ret |= ThaPwDynamicActivatorResourceDeallocate(self, adapter);

    return ret;
    }

static eAtRet PwEthPortSet(ThaPwActivator self, ThaPwAdapter adapter, AtEthPort ethPort)
    {
    if (ethPort)
        return PwEthPortBind(self, adapter, ethPort);

    return PwEthPortUnBind(self, adapter);
    }

static ThaEthFlowController EthFlowController(ThaPwAdapter adapter)
    {
    ThaModuleEth ethModule = (ThaModuleEth)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)adapter), cAtModuleEth);
    return ThaModuleEthPwFlowControllerGet(ethModule);
    }

static eAtRet PwEthHeaderCopy(ThaPwAdapter adapter, AtEthFlow flow)
    {
    eAtRet ret = cAtOk;
    AtPw pw = (AtPw)adapter;
    uint8 macAddr[cAtMacAddressLen];
    tAtEthVlanTag cVlan, sVlan;
    tAtEthVlanDesc vlanDesc;

    /* Egress Ethernet headers */
    ret |= AtPwEthDestMacGet(pw, macAddr);
    ret |= AtEthFlowEgressDestMacSet(flow, macAddr);

    if (AtPwEthCVlanGet(pw, &cVlan))
        {
        AtOsalMemInit(&vlanDesc, 0, sizeof(vlanDesc));

        vlanDesc.numberOfVlans = 1;
        vlanDesc.vlans[0] = cVlan;

        if (AtPwEthSVlanGet(pw, &sVlan))
            {
            vlanDesc.numberOfVlans = 2;
            vlanDesc.vlans[1] = sVlan;
            }

        ret |= AtEthFlowEgressVlanAdd(flow, &vlanDesc);
        }

    /* Ingress Ethernet headers */
    if (AtPwEthExpectedCVlanGet(pw, &cVlan))
        {
        AtOsalMemInit(&vlanDesc, 0, sizeof(vlanDesc));

        vlanDesc.numberOfVlans = 1;
        vlanDesc.vlans[0] = cVlan;

        if (AtPwEthExpectedSVlanGet(pw, &sVlan))
            {
            vlanDesc.numberOfVlans = 2;
            vlanDesc.vlans[1] = sVlan;
            }

        ret |= AtEthFlowIngressVlanAdd(flow, &vlanDesc);
        }

    return ret;
    }

static eAtRet PwEthFlowActivate(ThaPwActivator self, ThaPwAdapter adapter, AtEthFlow flow)
    {
    ThaEthFlowController flowController = EthFlowController(adapter);
    AtUnused(self);
    if (flowController)
        return ThaEthFlowControllerFlowActivate(flowController, flow, (AtChannel)adapter, 0);
    return cAtOk;
    }

static eAtRet PwEthFlowBind(ThaPwActivator self, ThaPwAdapter adapter, AtEthFlow ethFlow)
    {
    eAtRet ret = m_ThaPwActivatorMethods->PwEthFlowBind(self, adapter, ethFlow);
    if (ret != cAtOk)
        return ret;

    ret  = PwEthHeaderCopy(adapter, ethFlow);
    ret |= PwEthFlowActivate(self, adapter, ethFlow);
    if (ret != cAtOk)
        ret |= m_ThaPwActivatorMethods->PwEthFlowUnbind(self, adapter);

    return ret;
    }

static eAtRet PwEthFlowDeactivate(ThaPwActivator self, ThaPwAdapter adapter)
    {
    ThaEthFlowController flowController = EthFlowController(adapter);
    AtEthFlow currentFlow = ThaPwAdapterEthFlowGet(adapter);
    AtUnused(self);

    if (currentFlow && flowController)
        return ThaEthFlowControllerFlowDeActivate(flowController, currentFlow);

    return cAtOk;
    }

static eAtRet PwEthFlowUnbind(ThaPwActivator self, ThaPwAdapter adapter)
    {
    eAtRet ret = PwEthFlowDeactivate(self, adapter);
    if (ret != cAtOk)
        return ret;

    return m_ThaPwActivatorMethods->PwEthFlowUnbind(self, adapter);
    }

static eBool PwBindingInformationIsValid(ThaPwActivator self, ThaPwAdapter adapter, AtChannel circuit, AtEthPort ethPort)
    {
    ThaModuleEth ethModule;
	AtUnused(self);

    /* PW does not need to be activated yet */
    if ((circuit == NULL) || (ethPort == NULL))
        return cAtTrue;

    /* Circuit and ETH port must be on the same part */
    ethModule = (ThaModuleEth)AtChannelModuleGet((AtChannel)ethPort);
    if (ThaPwAdapterPartOfCircuit(adapter, circuit) == ThaModuleEthPartOfPort(ethModule, ethPort))
        return cAtTrue;

    return cAtFalse;
    }

static void Delete(AtObject self)
    {
    DeleteMasks((ThaPwActivator)self);
    AtObjectDelete((AtObject)mThis(self)->deactivatedPws);
    mThis(self)->deactivatedPws = NULL;
    m_AtObjectMethods->Delete(self);
    }

static void Debug(ThaPwActivator self)
    {
    uint8 part_i;

    AtPrintc(cSevInfo, "PW resource usages:\r\n");
    if (NumParts(self) == 1)
        {
        ThaBitMaskDebug(MaskForPart(self, 0));
        return;
        }

    for (part_i = 0; part_i < NumParts(self); part_i++)
        {
        AtPrintc(cSevInfo, "* Part %u:\r\n", part_i + 1);
        ThaBitMaskDebug(MaskForPart(self, part_i));
        }
    }

static ThaClaPwController ClaPwController(ThaPwActivator self)
    {
    return ThaModuleClaPwControllerGet((ThaModuleCla)ClaModule(self));
    }

static ThaHbce Hbce(ThaPwActivator self, ThaPwAdapter pwAdapter)
    {
    return ThaClaPwControllerHbceGet(ClaPwController(self), (AtPw)pwAdapter);
    }

static eBool PsnIsUsed(ThaPwActivator self, ThaPwAdapter adapter, AtPwPsn psn)
    {
    uint32 i;
    AtList deactivatedPws = DeactivatedPws(mThis(self));
    ThaHbce hbce = Hbce(self, adapter);
    uint32 labelForHashing = ThaHbcePsnLabelForHashing(hbce, psn);

    for (i = 0; i < AtListLengthGet(deactivatedPws); i++)
        {
        AtPw pw = (AtPw)AtListObjectGet(deactivatedPws, i);
        AtPwPsn pwPsn = AtPwPsnGet(pw);

        /* Ignore itself */
        if (ThaPwAdapterGet(pw) == adapter)
            continue;

        if (labelForHashing == ThaHbcePsnLabelForHashing(hbce, pwPsn))
            return cAtTrue;
        }

    return cAtFalse;
    }

static eBool VlanTagsAreUsed(ThaPwActivator self, ThaPwAdapter adapter, tAtEthVlanTag *expectedCVlan, tAtEthVlanTag *expectedSVlan)
    {
    uint32 i;
    AtList deactivatedPws = DeactivatedPws(mThis(self));

    for (i = 0; i < AtListLengthGet(deactivatedPws); i++)
        {
        tAtEthVlanTag cVlan, sVlan;
        AtPw pw = (AtPw)AtListObjectGet(deactivatedPws, i);

        /* Ignore itself */
        if (pw == ThaPwAdapterPwGet(adapter))
            continue;

        /* Make sure that other PWs are not using these tags */
        if (ThaClaPwControllerExpectedVlanTagsConflict(ClaPwController(self),
                                                       expectedCVlan, expectedSVlan,
                                                       AtPwEthExpectedCVlanGet(pw, &cVlan),
                                                       AtPwEthExpectedSVlanGet(pw, &sVlan)))
            return cAtTrue;
        }

    return cAtFalse;
    }

static eAtRet PwDelete(ThaPwActivator self, ThaPwAdapter adapter)
    {
    eBool deviceDeleting = AtDeviceIsDeleting(AtChannelDeviceGet((AtChannel)adapter));
    if (!deviceDeleting)
    AtListObjectRemove(DeactivatedPws(mThis(self)), (AtObject)ThaPwAdapterPwGet(adapter));
    return m_ThaPwActivatorMethods->PwDelete(self, adapter);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaPwDynamicActivator object = (ThaPwDynamicActivator)self;
    AtChannel *deactivatedPws = AtUtilSortedChannelsCreate(object->deactivatedPws);

    m_AtObjectMethods->Serialize(self, encoder);

    AtCoderEncodeObjectDescriptionInArray(encoder, (AtObject *)deactivatedPws, AtListLengthGet(object->deactivatedPws), "deactivatedPws");
    AtOsalMemFree(deactivatedPws);
    mEncodeObjects(partMasks, NumParts((ThaPwActivator)self));
    }

static eAtRet PwPayloadSizeReset(ThaPwActivator self, ThaPwAdapter adapter)
    {
    AtUnused(self);
    AtUnused(adapter);
    return cAtOk;
    }

static eAtRet HbceActivate(ThaPwActivator self, ThaPwAdapter adapter)
    {
    AtUnused(self);
    AtUnused(adapter);
    return cAtOk;
    }

static void OverrideAtObject(ThaPwActivator self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideThaPwActivator(ThaPwActivator self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPwActivatorMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwActivatorOverride, m_ThaPwActivatorMethods, sizeof(m_ThaPwActivatorOverride));

        mMethodOverride(m_ThaPwActivatorOverride, PwCanBeActivatedWithCircuit);
        mMethodOverride(m_ThaPwActivatorOverride, PwCircuitBind);
        mMethodOverride(m_ThaPwActivatorOverride, PwCircuitUnbind);
        mMethodOverride(m_ThaPwActivatorOverride, HwPwAllocate);
        mMethodOverride(m_ThaPwActivatorOverride, HwPwDeallocate);
        mMethodOverride(m_ThaPwActivatorOverride, PwDelete);
        mMethodOverride(m_ThaPwActivatorOverride, PwBindingInformationIsValid);
        mMethodOverride(m_ThaPwActivatorOverride, PwEthPortSet);
        mMethodOverride(m_ThaPwActivatorOverride, PwCanBeActivatedWithEthPort);
        mMethodOverride(m_ThaPwActivatorOverride, PwEthFlowBind);
        mMethodOverride(m_ThaPwActivatorOverride, PwEthFlowUnbind);
        mMethodOverride(m_ThaPwActivatorOverride, PsnIsUsed);
        mMethodOverride(m_ThaPwActivatorOverride, VlanTagsAreUsed);
        mMethodOverride(m_ThaPwActivatorOverride, Debug);
        mMethodOverride(m_ThaPwActivatorOverride, PwPayloadSizeReset);
        mMethodOverride(m_ThaPwActivatorOverride, HbceActivate);
        }

    mMethodsSet(self, &m_ThaPwActivatorOverride);
    }

static void Override(ThaPwActivator self)
    {
    OverrideAtObject(self);
    OverrideThaPwActivator(self);
    }

static void MethodsInit(ThaPwActivator self)
    {
    ThaPwDynamicActivator activator = (ThaPwDynamicActivator)self;
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, ActivateConfiguration);
        mMethodOverride(m_methods, DeactivateConfiguration);
        }

    mMethodsSet(activator, &m_methods);
    }

ThaPwActivator ThaPwDynamicActivatorObjectInit(ThaPwActivator self, AtModulePw pwModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPwActivatorObjectInit(self, pwModule) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->deactivatedPws = AtListCreate(AtModulePwMaxPwsGet(pwModule));

    return self;
    }

ThaPwActivator ThaPwDynamicActivatorNew(AtModulePw pwModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwActivator newActivator = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newActivator == NULL)
        return NULL;

    /* Construct it */
    return ThaPwDynamicActivatorObjectInit(newActivator, pwModule);
    }

eAtRet ThaPwDynamicActivatorResourceDeallocate(ThaPwActivator self, ThaPwAdapter adapter)
    {
    if (self)
        return ResourceDeallocate(self, adapter);

    return cAtErrorNullPointer;
    }

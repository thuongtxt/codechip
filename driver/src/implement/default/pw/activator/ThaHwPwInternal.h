/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : ThaHwPwInternal.h
 * 
 * Created Date: May 16, 2015
 *
 * Description : HW PW
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAHWPWINTERNAL_H_
#define _THAHWPWINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaHwPw.h"
#include "../../../../generic/man/AtDriverInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaHwPwMethods
    {
    void (*Debug)(ThaHwPw self);
    void (*DebuggerEntriesFill)(ThaHwPw self, AtDebugger debugger);
    }tThaHwPwMethods;

typedef struct tThaHwPw
    {
    tAtObject super;
    const tThaHwPwMethods *methods;

    /* Private data */
    ThaPwAdapter adapter;
    uint32 hwPwId;
    }tThaHwPw;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaHwPw ThaHwPwObjectInit(ThaHwPw self, ThaPwAdapter adapter, uint32 hwPwId);

#ifdef __cplusplus
}
#endif
#endif /* _THAHWPWINTERNAL_H_ */


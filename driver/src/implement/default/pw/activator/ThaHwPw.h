/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : ThaHwPw.h
 * 
 * Created Date: Jul 25, 2013
 *
 * Description : Hardware PW
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAHWPW_H_
#define _THAHWPW_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"
#include "AtObject.h"
#include "AtDebugger.h"
#include "../ThaModulePw.h"
#include "../adapters/ThaPwAdapter.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaHwPw ThaHwPwNew(ThaPwAdapter adapter, uint32 hwPwId);
uint32 ThaHwPwIdGet(ThaHwPw self);
void ThaHwPwDebug(ThaHwPw self);
ThaPwAdapter ThaHwPwAdapterGet(ThaHwPw self);
void ThaHwPwDebuggerEntriesFill(ThaHwPw self, AtDebugger debugger);

#ifdef __cplusplus
}
#endif
#endif /* _THAHWPW_H_ */


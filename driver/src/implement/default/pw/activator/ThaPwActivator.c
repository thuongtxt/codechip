/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : ThaPwActivator.c
 *
 * Created Date: Jul 29, 2013
 *
 * Description : PW Activator
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/common/AtChannelInternal.h"
#include "../../../../generic/pw/AtPwInternal.h"
#include "../../../../generic/util/AtUtil.h"
#include "../../../../generic/eth/AtEthFlowInternal.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../bmt/ThaModuleBmtInternal.h"
#include "../../man/ThaDeviceInternal.h"
#include "../adapters/ThaPwAdapter.h"
#include "ThaPwActivatorInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaPwActivatorMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool PwCanBeActivatedWithCircuit(ThaPwActivator self, ThaPwAdapter adapter, AtChannel circuit)
    {
	AtUnused(circuit);
	AtUnused(adapter);
	AtUnused(self);
    return cAtTrue;
    }

static eBool PwBindingInformationIsValid(ThaPwActivator self, ThaPwAdapter adapter, AtChannel circuit, AtEthPort ethPort)
    {
	AtUnused(ethPort);
	AtUnused(circuit);
	AtUnused(adapter);
	AtUnused(self);
    return cAtTrue;
    }

static ThaHwPw HwPwAllocate(ThaPwActivator self, ThaPwAdapter adapter)
    {
	AtUnused(adapter);
	AtUnused(self);
    /* This class is static PW allocation, so PW allocation is not used */
    return NULL;
    }

static eAtRet HwPwDeallocate(ThaPwActivator self, ThaPwAdapter adapter)
    {
	AtUnused(adapter);
	AtUnused(self);
    /* This class is static PW allocation, so PW allocation is not used */
    return cAtOk;
    }

static eAtModulePwRet PwCircuitBind(ThaPwActivator self, ThaPwAdapter adapter, AtChannel circuit)
    {
    eAtRet ret = ThaPwActivatorPwCircuitConnect(self, adapter, circuit);

    if (AtChannelDeviceInWarmRestore((AtChannel)adapter))
        return ret;

    /* Enable pw if it is enabled before */
    ret |= AtChannelEnable((AtChannel)adapter, ThaPwAdapterIsEnabled(adapter));

    return ret;
    }

static eAtModulePwRet PwCircuitUnbind(ThaPwActivator self, ThaPwAdapter adapter)
    {
    return ThaPwActivatorPwCircuitDisconnect(self, adapter);
    }

static ThaModuleBmt ModuleBmt(ThaPwAdapter adapter)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)adapter);
    return (ThaModuleBmt)AtDeviceModuleGet(device, cThaModuleBmt);
    }

static ThaModulePwe ModulePwe(ThaPwAdapter adapter)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)adapter);
    return (ThaModulePwe)AtDeviceModuleGet(device, cThaModulePwe);
    }

static eAtRet HbceActivate(ThaPwActivator self, ThaPwAdapter adapter)
    {
    AtUnused(self);
    return ThaPwAdapterHbceActivate(adapter);
    }

static eAtRet PwEthPortSet(ThaPwActivator self, ThaPwAdapter adapter, AtEthPort ethPort)
    {
    ThaModuleBmt moduleBmt = ModuleBmt(adapter);
	AtUnused(self);

    /* Re-configure priority */
    if (moduleBmt && ethPort)
        mMethodsGet(moduleBmt)->PwPrioritySet(moduleBmt, (AtPw)adapter, AtPwPriorityGet((AtPw)adapter));

    /* Deactivate PW if ETH port is not configured, otherwise, activate it */
    if (ethPort == NULL)
        return ThaPwAdapterHbceDeactivate(adapter);
    else
        return mMethodsGet(self)->HbceActivate(self, adapter);
    }

static eAtRet PwEthFlowBind(ThaPwActivator self, ThaPwAdapter pw, AtEthFlow ethFlow)
    {
    AtUnused(self);

    if (AtEthFlowIsBusy(ethFlow))
        return cAtErrorChannelBusy;

    AtEthFlowPwSet(ethFlow, ThaPwAdapterPwGet(pw));
    return ThaPwAdapterEthFlowSet(pw, ethFlow);
    }

static eAtRet PwEthFlowUnbind(ThaPwActivator self, ThaPwAdapter pw)
    {
    AtEthFlow currentFlow = ThaPwAdapterEthFlowGet(pw);
    AtUnused(self);

    if (currentFlow)
        AtEthFlowPwSet(currentFlow, NULL);

    return ThaPwAdapterEthFlowSet(pw, NULL);
    }

static eBool PwCanBeActivatedWithEthPort(ThaPwActivator self, ThaPwAdapter adapter, AtEthPort ethPort)
    {
	AtUnused(ethPort);
	AtUnused(adapter);
	AtUnused(self);
    return cAtTrue;
    }

static void Debug(ThaPwActivator self)
    {
	AtUnused(self);
    }

static eBool PsnIsUsed(ThaPwActivator self, ThaPwAdapter adapter, AtPwPsn psn)
    {
	AtUnused(psn);
	AtUnused(adapter);
	AtUnused(self);
    /* This static activator does not how to check, it assumes all of PSN can be used */
    return cAtFalse;
    }

static eBool VlanTagsAreUsed(ThaPwActivator self, ThaPwAdapter adapter, tAtEthVlanTag *expectedCVlan, tAtEthVlanTag *expectedSVlan)
    {
	AtUnused(expectedSVlan);
	AtUnused(expectedCVlan);
	AtUnused(adapter);
	AtUnused(self);
    return cAtFalse;
    }

static eAtRet PwDelete(ThaPwActivator self, ThaPwAdapter adapter)
    {
    eAtRet ret = cAtOk;
    AtEthFlow currentFlow = ThaPwAdapterEthFlowGet(adapter);

    if (currentFlow)
        {
        ret = ThaPwActivatorPwEthFlowUnbind(self, adapter);
        AtEthFlowPwSet(currentFlow, NULL);
        }

    return ret;
    }

static eAtModulePwRet PwCircuitConnect(ThaPwActivator self, ThaPwAdapter adapter, AtChannel circuit)
    {
    eAtRet ret = cAtOk;
    AtPw pw = (AtPw)adapter;
    AtChannel redirectCircuit;
    ThaModulePw pwModule;

    AtUnused(self);

    /* Circuit will know how to bind to PW */
    ret = AtChannelBindToPseudowire(circuit, pw);
    if (ret != cAtOk)
        return ret;

    /* Set configuration on binding */
    ret |= AtChannelBoundPwSet(circuit, ThaPwAdapterPwGet(adapter));
    ret |= ThaPwAdapterBoundCircuitSet(adapter, circuit);

    if (AtChannelDeviceInWarmRestore((AtChannel)adapter))
        return ret;

    pwModule = (ThaModulePw)AtChannelModuleGet((AtChannel)adapter);
    redirectCircuit = ThaModulePwReferenceCircuitToConfigureHardware(pwModule, adapter, circuit);

    ret |= ThaPwAdapterSetDefaultOnBinding(adapter, redirectCircuit);
    if (ret != cAtOk)
        return ret;

    ret |= ThaPwAdapterAcrDcrConfigure(adapter, redirectCircuit);

    /* Try to activate */
    ret |= mMethodsGet(self)->HbceActivate(self, adapter);
    ret |= ThaPwAdapterJitterBufferDefaultSet(adapter);

    return ret;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaPwActivator object = (ThaPwActivator)self;
    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObjectDescription(pwModule);
    }

static eAtRet PwPayloadSizeReset(ThaPwActivator self, ThaPwAdapter adapter)
    {
    AtUnused(self);
    return ThaModulePwePayloadSizeSet(ModulePwe(adapter), (AtPw)adapter, 0);
    }

static eBool ShouldControlPwBandwidth(ThaPwActivator self)
    {
    /* Some product need disable this feature will control at concrete */
    AtUnused(self);
    return cAtTrue;
    }

static eBool ShouldControlJitterBuffer(ThaPwActivator self)
    {
    /* Some product need disable this feature will control at concrete */
    AtUnused(self);
    return cAtTrue;
    }

static void MethodsInit(ThaPwActivator self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, PwBindingInformationIsValid);
        mMethodOverride(m_methods, PwCanBeActivatedWithCircuit);
        mMethodOverride(m_methods, PwCircuitBind);
        mMethodOverride(m_methods, PwCircuitUnbind);
        mMethodOverride(m_methods, PsnIsUsed);
        mMethodOverride(m_methods, VlanTagsAreUsed);
        mMethodOverride(m_methods, HwPwAllocate);
        mMethodOverride(m_methods, HwPwDeallocate);
        mMethodOverride(m_methods, PwDelete);
        mMethodOverride(m_methods, PwEthPortSet);
        mMethodOverride(m_methods, PwEthFlowBind);
        mMethodOverride(m_methods, PwEthFlowUnbind);
        mMethodOverride(m_methods, PwCanBeActivatedWithEthPort);
        mMethodOverride(m_methods, Debug);
        mMethodOverride(m_methods, PwCircuitConnect);
        mMethodOverride(m_methods, PwPayloadSizeReset);
        mMethodOverride(m_methods, HbceActivate);
        mMethodOverride(m_methods, ShouldControlPwBandwidth);
        mMethodOverride(m_methods, ShouldControlJitterBuffer);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtObject(ThaPwActivator self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(AtSharedDriverOsalGet(), &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(ThaPwActivator self)
    {
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPwActivator);
    }

ThaPwActivator ThaPwActivatorObjectInit(ThaPwActivator self, AtModulePw pwModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Private data */
    self->pwModule = pwModule;

    return self;
    }

ThaPwActivator ThaPwActivatorNew(AtModulePw pwModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwActivator newActivator = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newActivator == NULL)
        return NULL;

    /* Construct it */
    return ThaPwActivatorObjectInit(newActivator, pwModule);
    }

eAtModulePwRet ThaPwActivatorPwCircuitConnect(ThaPwActivator self, ThaPwAdapter adapter, AtChannel circuit)
    {
    if (self)
        return mMethodsGet(self)->PwCircuitConnect(self, adapter, circuit);
    return cAtErrorNotImplemented;
    }

eAtModulePwRet ThaPwActivatorPwCircuitDisconnect(ThaPwActivator self, ThaPwAdapter adapter)
    {
    AtPw pw = (AtPw)adapter;
    AtChannel circuit = AtPwBoundCircuitGet(pw);
    eAtRet ret = cAtOk;
    AtUnused(self);

    if (pw)
        ret |= AtChannelHardwareCleanup((AtChannel)pw);

    ret = AtChannelBindToPseudowire(circuit, NULL);
    ret |= mMethodsGet(self)->PwPayloadSizeReset(self, adapter);
    if (ret == cAtOk)
        {
        ret |= ThaPwAdapterBoundCircuitSet(adapter, NULL);
        ret |= ThaPwAdapterHbceDeactivate(adapter);
        }

    return ret;
    }

eBool ThaPwActivatorPwBindingInformationIsValid(ThaPwActivator self, ThaPwAdapter adapter, AtChannel circuit, AtEthPort ethPort)
    {
    if (self)
        return mMethodsGet(self)->PwBindingInformationIsValid(self, adapter, circuit, ethPort);
    return cAtFalse;
    }

eAtModulePwRet ThaPwActivatorPwCircuitBind(ThaPwActivator self, ThaPwAdapter adapter, AtChannel circuit)
    {
    if (self)
        return mMethodsGet(self)->PwCircuitBind(self, adapter, circuit);
    return cAtError;
    }

eAtModulePwRet ThaPwActivatorPwCircuitUnbind(ThaPwActivator self, ThaPwAdapter adapter)
    {
    if (self)
        return mMethodsGet(self)->PwCircuitUnbind(self, adapter);
    return cAtError;
    }

eAtRet ThaPwActivatorHwPwDeallocate(ThaPwActivator self, ThaPwAdapter adapter)
    {
    if (self)
        return mMethodsGet(self)->HwPwDeallocate(self, adapter);

    return cAtOk;
    }

eAtRet ThaPwActivatorPwEthPortSet(ThaPwActivator self, ThaPwAdapter adapter, AtEthPort ethPort)
    {
    if (self == NULL)
        return cAtError;

    return mMethodsGet(self)->PwEthPortSet(self, adapter, ethPort);
    }

eAtRet ThaPwActivatorPwEthFlowBind(ThaPwActivator self, ThaPwAdapter adapter, AtEthFlow ethFlow)
    {
    if (self)
        return mMethodsGet(self)->PwEthFlowBind(self, adapter, ethFlow);
    return cAtErrorNullPointer;
    }

eAtRet ThaPwActivatorPwEthFlowUnbind(ThaPwActivator self, ThaPwAdapter adapter)
    {
    if (self)
        return mMethodsGet(self)->PwEthFlowUnbind(self, adapter);
    return cAtErrorNullPointer;
    }

eBool ThaPwActivatorPsnIsUsed(ThaPwActivator self, ThaPwAdapter adapter, AtPwPsn psn)
    {
    if (self)
        return mMethodsGet(self)->PsnIsUsed(self, adapter, psn);
    return cAtFalse;
    }

eBool ThaPwActivatorVlanTagsAreUsed(ThaPwActivator self, ThaPwAdapter adapter, tAtEthVlanTag *expectedCVlan, tAtEthVlanTag *expectedSVlan)
    {
    if (self)
        return mMethodsGet(self)->VlanTagsAreUsed(self, adapter, expectedCVlan, expectedSVlan);
    return cAtFalse;
    }

ThaModulePw ThaPwActivatorModuleGet(ThaPwActivator self)
    {
    if (self)
        return (ThaModulePw)(self->pwModule);
    return NULL;
    }

void ThaPwActivatorDebug(ThaPwActivator self)
    {
    if (self)
        mMethodsGet(self)->Debug(self);
    }

eAtRet ThaPwActivatorPwDelete(ThaPwActivator self, ThaPwAdapter adapter)
    {
    if (self)
        return mMethodsGet(self)->PwDelete(self, adapter);
    return cAtError;
    }

eBool ThaPwActivatorShouldControlPwBandwidth(ThaPwActivator self)
    {
    if (self)
        return mMethodsGet(self)->ShouldControlPwBandwidth(self);
    return cAtFalse;
    }

eBool ThaPwActivatorShouldControlJitterBuffer(ThaPwActivator self)
    {
    if (self)
        return mMethodsGet(self)->ShouldControlJitterBuffer(self);
    return cAtFalse;
    }

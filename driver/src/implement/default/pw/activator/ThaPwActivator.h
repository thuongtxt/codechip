/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : ThaPwActivator.h
 * 
 * Created Date: Jul 29, 2013
 *
 * Description : PW Activator
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPWACTIVATOR_H_
#define _THAPWACTIVATOR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h" /* Super class */
#include "../adapters/ThaPwAdapter.h"
#include "../ThaModulePw.h"
#include "ThaHwPw.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Concrete classes */
ThaPwActivator ThaPwActivatorNew(AtModulePw pwModule);
ThaPwActivator ThaPwDynamicActivatorNew(AtModulePw pwModule);

/* Binding */
eBool ThaPwActivatorPwBindingInformationIsValid(ThaPwActivator self, ThaPwAdapter adapter, AtChannel circuit, AtEthPort ethPort);
eAtModulePwRet ThaPwActivatorPwCircuitBind(ThaPwActivator self, ThaPwAdapter adapter, AtChannel circuit);
eAtModulePwRet ThaPwActivatorPwCircuitUnbind(ThaPwActivator self, ThaPwAdapter adapter);
eAtRet ThaPwActivatorPwEthPortSet(ThaPwActivator self, ThaPwAdapter adapter, AtEthPort ethPort);
eAtRet ThaPwActivatorPwEthFlowBind(ThaPwActivator self, ThaPwAdapter adapter, AtEthFlow ethFlow);
eAtRet ThaPwActivatorPwEthFlowUnbind(ThaPwActivator self, ThaPwAdapter adapter);
eBool ThaPwActivatorPsnIsUsed(ThaPwActivator self, ThaPwAdapter adapter, AtPwPsn psn);
eBool ThaPwActivatorVlanTagsAreUsed(ThaPwActivator self, ThaPwAdapter adapter, tAtEthVlanTag *expectedCVlan, tAtEthVlanTag *expectedSVlan);

/* Hardware resource management */
eAtRet ThaPwActivatorHwPwDeallocate(ThaPwActivator self, ThaPwAdapter adapter);
eAtRet ThaPwActivatorPwDelete(ThaPwActivator self, ThaPwAdapter adapter);

eBool ThaPwActivatorShouldControlPwBandwidth(ThaPwActivator self);
eBool ThaPwActivatorShouldControlJitterBuffer(ThaPwActivator self);

/* Debug */
void ThaPwActivatorDebug(ThaPwActivator self);

/* Concrete products activator */
ThaPwActivator Tha60210011PwDynamicActivatorNew(AtModulePw pwModule);
ThaPwActivator Tha60210012PwDynamicActivatorNew(AtModulePw pwModule);
ThaPwActivator Tha60210061PwActivatorNew(AtModulePw pwModule);
ThaPwActivator Tha60290021PwActivatorNew(AtModulePw pwModule);
ThaPwActivator Tha60290081PwActivatorNew(AtModulePw pwModule);

ThaPwActivator Tha6A000010PwDynamicActivatorNew(AtModulePw pwModule);
ThaPwActivator Tha6A033111PwDynamicActivatorNew(AtModulePw pwModule);
ThaPwActivator Tha6A290021PwDynamicActivatorNew(AtModulePw pwModule);

#ifdef __cplusplus
}
#endif
#endif /* _THAPWACTIVATOR_H_ */


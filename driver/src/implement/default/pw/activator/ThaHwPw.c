/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : ThaHwPw.c
 *
 * Created Date: Jul 29, 2013
 *
 * Description : Hardware PW
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaHwPwInternal.h"
#include "../../../../generic/man/AtDriverInternal.h"
#include "../../../../generic/common/AtChannelInternal.h"
#include "../../../../util/coder/AtCoderUtil.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaHwPwMethods m_methods;

/* Override */
static tAtObjectMethods  m_AtObjectOverride;

/* Save super's implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaHwPw object = (ThaHwPw)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeChannelIdString(adapter);
    mEncodeUInt(hwPwId);
    }

static void Debug(ThaHwPw self)
    {
    AtPrintc(cSevNormal, "* hwPwId: %d\r\n", self->hwPwId);
    }

static void DebuggerEntriesFill(ThaHwPw self, AtDebugger debugger)
    {
    AtUnused(self);
    AtUnused(debugger);
    }

static void MethodsInit(ThaHwPw self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, Debug);
        mMethodOverride(m_methods, DebuggerEntriesFill);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtObject(ThaHwPw self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(ThaHwPw self)
    {
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaHwPw);
    }

ThaHwPw ThaHwPwObjectInit(ThaHwPw self, ThaPwAdapter adapter, uint32 hwPwId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Nothing to setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Private data */
    self->adapter = adapter;
    self->hwPwId  = hwPwId;

    return self;
    }

ThaHwPw ThaHwPwNew(ThaPwAdapter adapter, uint32 hwPwId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaHwPw newHwPw = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newHwPw == NULL)
        return NULL;

    /* Construct it */
    return ThaHwPwObjectInit(newHwPw, adapter, hwPwId);
    }

uint32 ThaHwPwIdGet(ThaHwPw self)
    {
    return self->hwPwId;
    }

ThaPwAdapter ThaHwPwAdapterGet(ThaHwPw self)
    {
    if (self)
        return self->adapter;
    return NULL;
    }

void ThaHwPwDebug(ThaHwPw self)
    {
    if (self)
        mMethodsGet(self)->Debug(self);
    }

void ThaHwPwDebuggerEntriesFill(ThaHwPw self, AtDebugger debugger)
    {
    if (self)
        mMethodsGet(self)->DebuggerEntriesFill(self, debugger);
    }

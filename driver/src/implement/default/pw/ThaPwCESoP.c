/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : ThaPwCESoP.c
 *
 * Created Date: Nov 17, 2012
 *
 * Description : Thalassa CESoP implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaModulePw.h"
#include "ThaPwInternal.h"
#include "../../default/man/ThaDeviceInternal.h"
#include "../../../util/coder/AtCoderUtil.h"
#include "adapters/ThaPwAdapterInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mCESoPAdapter(self) ((ThaPwCESoPAdapter)Adapter((AtPw)self))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtObjectMethods  m_AtObjectOverride;
static tAtPwMethods      m_AtPwOverride;
static tAtChannelMethods m_AtChannelOverride;
static tAtPwCESoPMethods m_AtPwCESoPOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaPwCESoP);
    }

static AtPw Adapter(AtPw pw)
    {
    return (AtPw)((ThaPwCESoP)pw)->adapter;
    }

static eAtModulePwRet CwAutoTxMBitEnable(AtPwCESoP self, eBool enable)
    {
    return mMethodsGet(mCESoPAdapter(self))->CwAutoTxMBitEnable(mCESoPAdapter(self), enable);
    }

static eBool CwAutoTxMBitIsEnabled(AtPwCESoP self)
    {
    return mMethodsGet(mCESoPAdapter(self))->CwAutoTxMBitIsEnabled(mCESoPAdapter(self));
    }

static eAtModulePwRet CwAutoRxMBitEnable(AtPwCESoP self, eBool enable)
    {
    return mMethodsGet(mCESoPAdapter(self))->CwAutoRxMBitEnable(mCESoPAdapter(self), enable);
    }

static eBool CwAutoRxMBitIsEnabled(AtPwCESoP self)
    {
    return mMethodsGet(mCESoPAdapter(self))->CwAutoRxMBitIsEnabled(mCESoPAdapter(self));
    }

static eBool CwAutoRxMBitIsConfigurable(AtPwCESoP self)
    {
    return mMethodsGet(mCESoPAdapter(self))->CwAutoRxMBitIsConfigurable(mCESoPAdapter(self));
    }

static eAtModulePwRet CasIdleCode1Set(AtPwCESoP self, uint8 abcd1)
    {
    return mMethodsGet(mCESoPAdapter(self))->CasIdleCode1Set(mCESoPAdapter(self), abcd1);
    }

static uint8 CasIdleCode1Get(AtPwCESoP self)
    {
    return mMethodsGet(mCESoPAdapter(self))->CasIdleCode1Get(mCESoPAdapter(self));
    }

static eAtModulePwRet CasIdleCode2Set(AtPwCESoP self, uint8 abcd2)
    {
    return mMethodsGet(mCESoPAdapter(self))->CasIdleCode2Set(mCESoPAdapter(self), abcd2);
    }

static uint8 CasIdleCode2Get(AtPwCESoP self)
    {
    return mMethodsGet(mCESoPAdapter(self))->CasIdleCode2Get(mCESoPAdapter(self));
    }

static eAtModulePwRet CasAutoIdleEnable(AtPwCESoP self, eBool enable)
    {
    return mMethodsGet(mCESoPAdapter(self))->CasAutoIdleEnable(mCESoPAdapter(self), enable);
    }

static eBool CasAutoIdleIsEnabled(AtPwCESoP self)
    {
    return mMethodsGet(mCESoPAdapter(self))->CasAutoIdleIsEnabled(mCESoPAdapter(self));
    }

static eAtRet RxCasModeSet(AtPwCESoP self, eAtPwCESoPCasMode mode)
    {
    return mMethodsGet(mCESoPAdapter(self))->RxCasModeSet(mCESoPAdapter(self), mode);
    }

static eAtPwCESoPCasMode RxCasModeGet(AtPwCESoP self)
    {
    return mMethodsGet(mCESoPAdapter(self))->RxCasModeGet(mCESoPAdapter(self));
    }

#include "ThaPwCommonSetup.h"

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaPwCESoP object = (ThaPwCESoP)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObject(adapter);
    }

static void OverrideAtObject(AtPw self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtPwCESoP(AtPw self)
    {
    AtPwCESoP pw = (AtPwCESoP)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPwCESoPOverride, mMethodsGet(pw), sizeof(m_AtPwCESoPOverride));

        mMethodOverride(m_AtPwCESoPOverride, CwAutoTxMBitEnable);
        mMethodOverride(m_AtPwCESoPOverride, CwAutoRxMBitEnable);
        mMethodOverride(m_AtPwCESoPOverride, CwAutoTxMBitIsEnabled);
        mMethodOverride(m_AtPwCESoPOverride, CwAutoRxMBitIsEnabled);
        mMethodOverride(m_AtPwCESoPOverride, CwAutoRxMBitIsConfigurable);
        mMethodOverride(m_AtPwCESoPOverride, CasIdleCode1Set);
        mMethodOverride(m_AtPwCESoPOverride, CasIdleCode1Get);
        mMethodOverride(m_AtPwCESoPOverride, CasIdleCode2Set);
        mMethodOverride(m_AtPwCESoPOverride, CasIdleCode2Get);
        mMethodOverride(m_AtPwCESoPOverride, CasAutoIdleEnable);
        mMethodOverride(m_AtPwCESoPOverride, CasAutoIdleIsEnabled);
        mMethodOverride(m_AtPwCESoPOverride, RxCasModeSet);
        mMethodOverride(m_AtPwCESoPOverride, RxCasModeGet);
        }

    mMethodsSet(pw, &m_AtPwCESoPOverride);
    }

static void Override(AtPw self)
    {
    OverrideAtObject(self);
    OverrideAtPw(self);
    OverrideAtChannel(self);
    OverrideAtPwCESoP(self);
    }

static ThaPwAdapter ThaPwCESoPAdapterCreate(AtPw self)
    {
    AtModulePw module = (AtModulePw)AtChannelModuleGet((AtChannel)self);

    if (AtModulePwCesopPayloadSizeInByteIsEnabled(module))
        return ThaPwCESoPAdapterInBytesNew(self);

    return ThaPwCESoPAdapterNew(self);
    }

/* To initialize object */
static AtPw ObjectInit(AtPw self, uint32 pwId, AtModulePw module, eAtPwCESoPMode mode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (AtPwCESoPObjectInit((AtPwCESoP)self, pwId, module, mode) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    /* PW adapter */
    ((ThaPwCESoP)self)->adapter = ThaPwCESoPAdapterCreate(self);

    return self;
    }

AtPw ThaPwCESoPNew(AtModulePw self, uint32 pwId, eAtPwCESoPMode mode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPw newPw = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPw == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPw, pwId, self, mode);
    }

ThaPwAdapter ThaPwCESoPAdapterGet(AtPw self)
    {
    return (ThaPwAdapter)Adapter(self);
    }

void ThaPwCESoPAdapterDelete(AtPw self)
    {
    AtObjectDelete((AtObject)((ThaPwCESoP)self)->adapter);
    ((ThaPwCESoP)self)->adapter = NULL;
    }

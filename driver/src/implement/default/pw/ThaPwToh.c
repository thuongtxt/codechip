/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * extoht by express written agreement with Arrive Technologies.
 *
 * Module      : Pseudowire
 *
 * File        : ThaPwToh.c
 *
 * Created Date: Mar 3, 2014
 *
 * Description : TOH pseudowire
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../generic/common/AtChannelInternal.h"
#include "../../../generic/pw/AtModulePwInternal.h"
#include "../../../util/coder/AtCoderUtil.h"
#include "../pw/ThaPwInternal.h"
#include "adapters/ThaPwAdapterInternal.h"
#include "ThaModulePw.h"
#include "ThaPwToh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mTohAdapter(self) (ThaPwTohAdapter)Adapter((AtPw)self)
#define mRs(name) cAtSdhLineRsOverheadByte##name
#define mMs(name) cAtSdhLineMsOverheadByte##name
#define mThis(self) ((ThaPwToh)self)

#define cNumOhRow 9
#define cNumOhColForOneSts 3
#define cNumOhBytePerSts (cNumOhRow * cNumOhColForOneSts)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtPwMethods      m_AtPwOverride;
static tAtChannelMethods m_AtChannelOverride;
static tAtPwTohMethods   m_AtPwTohOverride;
static tAtObjectMethods  m_AtObjectOverride;

static const tAtChannelMethods *m_AtChannelMethods = NULL;
static const tAtPwTohMethods   *m_AtPwTohMethods   = NULL;
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;

static const eAtSdhLineOverheadByte m_TohTable[9][3] = {{mRs(A1) , mRs(A2) , mRs(J0)},
                                                        {mRs(B1) , mRs(E1) , mRs(F1)},
                                                        {mRs(D1) , mRs(D2) , mRs(D3)},
                                                        {mRs(H1) , mRs(H2) , mRs(H3)},
                                                        {mMs(B2) , mMs(K1) , mMs(K2)},
                                                        {mMs(D4) , mMs(D5) , mMs(D6)},
                                                        {mMs(D7) , mMs(D8) , mMs(D9)},
                                                        {mMs(D10), mMs(D11), mMs(D12)},
                                                        {mMs(S1) , mMs(M1) , mMs(E2)}};

static const char *m_TohString[9][3] = {{"A1" , "A2" , "J0"},
                                        {"B1" , "E1" , "F1"},
                                        {"D1" , "D2" , "D3"},
                                        {"H1" , "H2" , "H3"},
                                        {"B2" , "K1" , "K2"},
                                        {"D4" , "D5" , "D6"},
                                        {"D7" , "D8" , "D9"},
                                        {"D10", "D11", "D12"},
                                        {"S1" , "M1" , "E2"}};

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPw Adapter(AtPw pw)
    {
    return (AtPw)((ThaPwToh)pw)->adapter;
    }

static uint32 NumTohByteInLine(AtSdhLine line)
    {
    eAtSdhLineRate rate = AtSdhLineRateGet(line);

    if (rate == cAtSdhLineRateStm1)
        return cNumOhBytePerSts * 3;

    if (rate == cAtSdhLineRateStm4)
        return cNumOhBytePerSts * 12;

    return 0;
    }

eAtRet ThaTohByteRowColGet(const tThaTohByte *tohByte, uint8 *row, uint8 *col)
    {
    uint8 row_i, col_i;

    for (row_i = 0; row_i < 9; row_i++)
        {
        for (col_i = 0; col_i < 3; col_i++)
            {
            if (tohByte->ohByte != m_TohTable[row_i][col_i])
                continue;

            *row = row_i;
            *col = col_i;
            return cAtOk;
            }
        }

    *row = 0xFF;
    *col = 0xFF;

    return cAtError;
    }

eAtSdhLineOverheadByte ThaTohByteFromRowColGet(uint8 row, uint8 col)
    {
    if ((row >= 9) || (col >= 3))
        return cThaInvalidTohByte;

    return m_TohTable[row][col];
    }

/*
 * This function is use to calculate corresponding bit position of
 * OH byte on bit mask
 *
 * This position is also used to "compare" 2 OH bytes
 * if OH byte A has position less than OH byte B, we consider byte A stands before
 * byte B, this concept is used to update first and last OH byte in HW
 *
 * Table of TOH byte position for STM-1
 * STS:  0   1   2   0   1   2   0   1   2
 *     +---+---+---+---+---+---+---+---+---+
 *     |A1 |A1 |A1 |A2 |A2 |A2 |J0 |J0 |J0 |
 *     +---+---+---+---+---+---+---+---+---+
 *     |B1 |B1 |B1 |E1 |E1 |E1 |F1 |F1 |F1 |
 *     +---+---+---+---+---+---+---+---+---+
 *     |D1 |D1 |D1 |D2 |D2 |D2 |D3 |D3 |D3 |
 *     +---+---+---+---+---+---+---+---+---+
 *     |B2 |B2 |B2 |K1 |K1 |K1 |K2 |K2 |K2 |
 *     +---+---+---+---+---+---+---+---+---+
 *     |D4 |D4 |D4 |D5 |D5 |D5 |D6 |D6 |D6 |
 *     +---+---+---+---+---+---+---+---+---+
 *     |D7 |D7 |D7 |D8 |D8 |D8 |D9 |D9 |D9 |
 *     +---+---+---+---+---+---+---+---+---+
 *     |S1 |S1 |S1 |M1 |M1 |M1 |E2 |E2 |E2 |
 *     +---+---+---+---+---+---+---+---+---+
 *     |D10|D10|D10|D11|D11|D11|D12|D12|D12|
 *     +---+---+---+---+---+---+---+---+---+
 *
 * * Table of TOH byte position for STM-4
 * STS:  0   1   2          13  14          25
 *     +---+---+---+...+---+---+---+...+---+---+---+...+---+---+
 *     |A1 |A1 |A1 |...|A1 |A2 |A2 |...|A2 |J0 |J0 |...|J0 |J0 |
 *     +---+---+---+...+---+---+---+...+---+---+---+...+---+---+
 *     |B1 |B1 |B1 |...|B1 |E1 |E1 |...|E1 |F1 |F1 |...|F1 |F1 |
 *     +---+---+---+...+---+---+---+...+---+---+---+...+---+---+
 *     |D1 |D1 |D1 |...|D1 |D2 |D2 |...|D2 |D3 |D3 |...|D3 |D3 |
 *     +---+---+---+...+---+---+---+...+---+---+---+...+---+---+
 *     |B2 |B2 |B2 |...|B2 |K1 |K1 |...|K1 |K2 |K2 |...|K2 |K2 |
 *     +---+---+---+...+---+---+---+...+---+---+---+...+---+---+
 *     |D4 |D4 |D4 |...|D4 |D5 |D5 |...|D5 |D6 |D6 |...|D6 |D6 |
 *     +---+---+---+...+---+---+---+...+---+---+---+...+---+---+
 *     |D7 |D7 |D7 |...|D7 |D8 |D8 |...|D8 |D9 |D9 |...|D9 |D9 |
 *     +---+---+---+...+---+---+---+...+---+---+---+...+---+---+
 *     |S1 |S1 |S1 |...|S1 |M1 |M1 |...|M1 |E2 |E2 |...|E2 |E2 |
 *     +---+---+---+...+---+---+---+...+---+---+---+...+---+---+
 *     |D10|D10|D10|...|D10|D11|D11|...|D11|D12|D12|...|D12|D12|
 *     +---+---+---+...+---+---+---+...+---+---+---+...+---+---+
 */

static uint8 NumStsInLine(AtSdhLine line)
    {
    return AtSdhChannelNumSts((AtSdhChannel)line);
    }

static uint16 TohByteBitPosition(ThaPwToh self, const tThaTohByte *tohByte)
    {
    uint8 row, col;
    uint8 numSts = NumStsInLine((AtSdhLine)AtPwBoundCircuitGet((AtPw)self));
    ThaTohByteRowColGet(tohByte, &row, &col);
    return (uint16)((row * (cNumOhColForOneSts * numSts)) + (col * numSts) + tohByte->sts1);
    }

static void BitPositionToTohByte(ThaPwToh self, uint32 position, eAtSdhLineOverheadByte *ohByte, uint8 *sts1)
    {
    uint8 row, col;
    uint8 numSts = NumStsInLine((AtSdhLine)AtPwBoundCircuitGet((AtPw)self));
    uint32 numColumns = (uint32)(numSts * cNumOhColForOneSts);

    row = (uint8)(position / numColumns);
    col = (uint8)((position % numColumns) / numSts);

    if (sts1)
        *sts1 = (uint8)((position % numColumns) % numSts);

    if (ohByte)
        *ohByte = m_TohTable[row][col];
    }

static eAtRet TohByteBitMaskSet(ThaPwToh self, const tThaTohByte *tohByte)
    {
    if (self->tohBytesMask == NULL)
        self->tohBytesMask = ThaBitMaskNew(NumTohByteInLine((AtSdhLine)AtPwBoundCircuitGet((AtPw)self)));

    if (self->tohBytesMask == NULL)
        return cAtError;

    ThaBitMaskSetBit(self->tohBytesMask, TohByteBitPosition(self, tohByte));
    return cAtOk;
    }

static eAtRet TohByteBitMaskClear(ThaPwToh self, const tThaTohByte *tohByte)
    {
    if (self->tohBytesMask != NULL)
        ThaBitMaskClearBit(self->tohBytesMask, TohByteBitPosition(self, tohByte));

    return cAtOk;
    }

static eBool NeedToConfigure(AtPwToh self, uint8 sts1, eAtSdhLineOverheadByte ohByte, eBool enable)
    {
    return (enable != AtPwTohByteIsEnabled(self, sts1, ohByte)) ? cAtTrue : cAtFalse;
    }

static void FirstLastBytesUpdate(ThaPwToh self)
    {
    uint32 firstBit  = 0;
    uint32 lastBit   = 0;
    uint32 position  = 0;
    uint8 firstFound = 0;

    /* Find the first and last set bit in mask */
    while(ThaBitMaskBitPositionIsValid(self->tohBytesMask, position))
        {
        if (ThaBitMaskBitVal(self->tohBytesMask, position))
            {
            /* Reach set bit, if first has not been found, set it */
            if (firstFound == 0)
                {
                firstBit = position;
                firstFound = 1;
                }

            /* Update last bit */
            lastBit = position;
            }

        position = position + 1;
        }

    if (!firstFound)
        {
        self->hasFirstLast = 0;
        return;
        }

    self->hasFirstLast = 1;
    BitPositionToTohByte(self, firstBit, &self->firstByte.ohByte, &self->firstByte.sts1);
    BitPositionToTohByte(self, lastBit, &self->lastByte.ohByte, &self->lastByte.sts1);
    }

static eBool OhByteIsSupported(uint32 ohByte)
    {
    if ((ohByte == cAtSdhLineRsOverheadByteA1) ||
        (ohByte == cAtSdhLineRsOverheadByteA2) ||
        (ohByte == cAtSdhLineRsOverheadByteB1) ||
        (ohByte == cAtSdhLineMsOverheadByteB2) ||
        (ohByte == cAtSdhLineRsOverheadByteH1) ||
        (ohByte == cAtSdhLineRsOverheadByteH2) ||
        (ohByte == cAtSdhLineRsOverheadByteH3))
        return cAtFalse;

    return cAtTrue;
    }

static eBool TohByteIsValid(AtPwToh self, uint8 sts1, uint32 ohByte)
    {
    AtSdhLine line = (AtSdhLine)AtPwBoundCircuitGet((AtPw)self);
    eAtSdhLineRate rate = AtSdhLineRateGet(line);
    uint8 numStsInLine = (rate == cAtSdhLineRateStm1) ? 3 : 12;

    if (rate == cAtSdhLineRateStm1)
        numStsInLine = 3;

    if (rate == cAtSdhLineRateStm4)
        numStsInLine = 12;

    if (sts1 >= numStsInLine)
        return cAtFalse;

    return OhByteIsSupported(ohByte);
    }

static eBool K1Exist(uint32 ohByteBitmap)
    {
    return ((ohByteBitmap & cAtSdhLineMsOverheadByteK1) ? cAtTrue : cAtFalse);
    }

static eBool K2Exist(uint32 ohByteBitmap)
    {
    return ((ohByteBitmap & cAtSdhLineMsOverheadByteK2) ? cAtTrue : cAtFalse);
    }

static eBool TohByteBitmapIsValid(AtPwToh self, uint8 sts1, uint32 ohByteBitmap, eBool enable)
    {
    uint8 bit_i;
	AtUnused(enable);

    for (bit_i = 0; bit_i < cNumOhBytePerSts; bit_i++)
        {
        if ((ohByteBitmap & (cBit0 << bit_i)) == 0)
            continue;

        if (!TohByteIsValid(self, sts1, cBit0 << bit_i))
            return cAtFalse;
        }

    /* K1, K2 must come together in STS #0 */
    if (sts1 != 0)
        return cAtTrue;

    if (K1Exist(ohByteBitmap) != K2Exist(ohByteBitmap))
        return cAtFalse;

    return cAtTrue;
    }

static tThaTohByte *TohByteConstruct(eAtSdhLineOverheadByte ohByte, uint8 sts1, tThaTohByte *tohByte)
    {
    if (tohByte == NULL)
        return NULL;

    tohByte->ohByte = ohByte;
    tohByte->sts1   = sts1;
    
    return tohByte;
    }

static eAtRet TohOneByteEnable(AtPwToh self, uint8 sts1, eAtSdhLineOverheadByte ohByte, eBool enable)
    {
    ThaPwToh pwToh = (ThaPwToh)self;
    eAtRet ret;
    tThaTohByte tohByte;

    if (!NeedToConfigure(self, sts1, ohByte, enable))
        return cAtOk;

    TohByteConstruct(ohByte, sts1, &tohByte);
    if (enable)
        {
        ret = mMethodsGet(mTohAdapter(self))->Allow(mTohAdapter(self), &tohByte);
        if (ret != cAtOk)
            return ret;

        ret = TohByteBitMaskSet(pwToh, &tohByte);
        }

    /* Search and reject */
    else
        {
        TohByteBitMaskClear(pwToh, &tohByte);
        FirstLastBytesUpdate(pwToh);
        ret = mMethodsGet(mTohAdapter(self))->Reject(mTohAdapter(self), &tohByte);
        }

    return ret;
    }

static eAtModulePwRet TohByteEnable(AtPwToh self, uint8 sts1, uint32 tohByteBitmap, eBool enable)
    {
    eAtRet ret = cAtOk;
    uint8 bit_i;
    eBool isEnabled;

    /* Pw just can carry any TOH byte if it is bound to a line */
    AtChannel circuit = AtPwBoundCircuitGet((AtPw)self);
    if (circuit == NULL)
        return cAtError;

    if (!TohByteBitmapIsValid(self, sts1, tohByteBitmap, enable))
        return cAtErrorInvlParm;

    isEnabled = AtChannelIsEnabled((AtChannel)self);
    ret = AtChannelEnable((AtChannel)self, cAtFalse);
    if (ret != cAtOk)
        return (eAtModulePwRet)ret;

    for (bit_i = 0; bit_i < cNumOhBytePerSts; bit_i++)
        {
        if ((tohByteBitmap & (cBit0 << bit_i)) == 0)
            continue;

        ret |= TohOneByteEnable(self, sts1, (eAtSdhLineOverheadByte)(cBit0 << bit_i), enable);
        }

    /* Re-configure payload size to apply new bytes */
    if (ret == cAtOk)
        {
        eBool needUpdatePayloadSize = (AtPwTohNumEnabledBytesGet(self) > 0) &&
                                      (AtPwPayloadSizeGet((AtPw)self) > 0);
        if (needUpdatePayloadSize)
            ret = AtPwPayloadSizeSet((AtPw)self, AtPwPayloadSizeGet((AtPw)self));
        }

    ret |= AtChannelEnable((AtChannel)self, isEnabled);
    return (eAtModulePwRet)ret;
    }

static eBool TohByteIsEnabled(AtPwToh self, uint8 sts1, eAtSdhLineOverheadByte ohByte)
    {
    ThaPwToh pwToh = (ThaPwToh)self;
    tThaTohByte tohByte;
    uint32 bitPosition;

    if (!TohByteIsValid(self, sts1, (uint32)ohByte))
        return cAtFalse;

    if (pwToh->tohBytesMask == NULL)
        return cAtFalse;

    bitPosition = TohByteBitPosition((ThaPwToh)self, TohByteConstruct(ohByte, sts1, &tohByte));
    return (ThaBitMaskBitVal(pwToh->tohBytesMask, bitPosition) ? cAtTrue : cAtFalse);
    }

static uint16 AllEnabledBytesGet(AtPwToh self, uint32* tohByteBitmaps, uint8 numBitmap)
    {
    uint32 position = 0;
    uint16 byteCount = 0;
    uint8 sts1;
    eAtSdhLineOverheadByte ohByte;

    if (mThis(self)->tohBytesMask == NULL)
        return 0;

    while(ThaBitMaskBitPositionIsValid(mThis(self)->tohBytesMask, position))
        {
        if (ThaBitMaskBitVal(mThis(self)->tohBytesMask, position))
            {
            if(tohByteBitmaps)
                {
                BitPositionToTohByte((ThaPwToh)self, position, &ohByte, &sts1);
                if (sts1 >= numBitmap)
                    continue;

                tohByteBitmaps[sts1] |= ohByte;
                }

            byteCount = (uint16)(byteCount + 1);
            }

        position = position + 1;
        }

    return byteCount;
    }

static uint8 EnabledBytesByStsGet(AtPwToh self, uint8 sts1, uint32* tohByteBitmap)
    {
    uint32 position;
    uint8 byteCount = 0;
    eAtSdhLineOverheadByte ohByte;
    uint8 step;
    AtSdhLine line = (AtSdhLine)AtPwBoundCircuitGet((AtPw)self);

    if ((mThis(self)->tohBytesMask == NULL) || (line == NULL))
        return 0;

    if (tohByteBitmap)
        *tohByteBitmap = 0;

    step = (AtSdhLineRateGet(line) == cAtSdhLineRateStm1) ? 3 : 12;
    position = sts1;
    while(ThaBitMaskBitPositionIsValid(mThis(self)->tohBytesMask, position))
        {
        if (ThaBitMaskBitVal(mThis(self)->tohBytesMask, position))
            {
            if(tohByteBitmap)
                {
                BitPositionToTohByte((ThaPwToh)self, position, &ohByte, NULL);
                *tohByteBitmap |= ohByte;
                }

            byteCount = (uint8)(byteCount + 1);
            }

        position = position + step;
        }

    return (uint8)byteCount;
    }

static void Delete(AtObject self)
    {
    ThaPwToh pwToh = mThis(self);

    if (pwToh->tohBytesMask)
        {
        AtObjectDelete((AtObject)(pwToh->tohBytesMask));
        pwToh->tohBytesMask = NULL;
        }

    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

char* ThaPwTohByteToString(const tThaTohByte* ohByte)
    {
    static char tohString[64];
    uint8 col, row;

    if (ohByte == NULL)
        {
        AtSprintf(tohString, "none");
        return tohString;
        }

    ThaTohByteRowColGet(ohByte, &row, &col);
    AtSprintf(tohString, "STS %d - Byte %s [%d : %d]", ohByte->sts1 + 1, m_TohString[row][col], row, col);
    return tohString;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaPwToh object = (ThaPwToh)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObject(adapter);

    mEncodeObject(tohBytesMask);
    mEncodeUInt(hasFirstLast);

    AtCoderEncodeUInt(encoder, object->firstByte.ohByte, "firstByte.ohByte");
    AtCoderEncodeUInt(encoder, object->firstByte.sts1, "firstByte.sts1");
    AtCoderEncodeUInt(encoder, object->lastByte.ohByte, "lastByte.ohByte");
    AtCoderEncodeUInt(encoder, object->lastByte.sts1, "lastByte.sts1");
    }

static void OverrideAtObject(AtObject self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(self);

        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(self, &m_AtObjectOverride);
    }

#include "ThaPwCommonSetup.h"

static void OverrideAtPwToh(ThaPwToh self)
    {
    AtPwToh tohPw = (AtPwToh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPwTohMethods = mMethodsGet(tohPw);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPwTohOverride, m_AtPwTohMethods, sizeof(m_AtPwTohOverride));

        mMethodOverride(m_AtPwTohOverride, TohByteEnable);
        mMethodOverride(m_AtPwTohOverride, TohByteIsEnabled);
        mMethodOverride(m_AtPwTohOverride, AllEnabledBytesGet);
        mMethodOverride(m_AtPwTohOverride, EnabledBytesByStsGet);
        }

    mMethodsSet(tohPw, &m_AtPwTohOverride);
    }

static void Override(ThaPwToh self)
    {
    OverrideAtPw((AtPw)self);
    OverrideAtChannel((AtPw)self);
    OverrideAtPwToh(self);
    OverrideAtObject((AtObject)self);
    }

/* To initialize object */
static AtPw ObjectInit(AtPw self, uint32 pwId, AtModulePw module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaPwToh));

    /* Super constructor should be called first */
    if (AtPwTohObjectInit((AtPwToh)self, pwId, module) == NULL)
        return NULL;

    /* Override */
    Override((ThaPwToh)self);
    m_methodsInit = 1;

    /* PW adapter */
    ((ThaPwToh)self)->adapter = ThaPwTohAdapterNew(self);

    return self;
    }

AtPw ThaPwTohNew(AtModulePw self, uint32 pwId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPw newPw = mMethodsGet(osal)->MemAlloc(osal, sizeof(tThaPwToh));
    if (newPw == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPw, pwId, self);
    }

tThaTohByte* ThaPwTohFirstByteSet(ThaPwToh self, const tThaTohByte* firstByte)
    {
    if (self == NULL)
        return NULL;

    self->firstByte = *firstByte;
    self->hasFirstLast = 1;
    return &self->firstByte;
    }

tThaTohByte* ThaPwTohFirstByteGet(ThaPwToh self)
    {
    if ((self == NULL) || (!self->hasFirstLast))
        return NULL;

    return &self->firstByte;
    }

tThaTohByte* ThaPwTohLastByteSet(ThaPwToh self, const tThaTohByte* lastByte)
    {
    if (self == NULL)
        return NULL;

    self->lastByte = *lastByte;
    self->hasFirstLast = 1;
    return &self->lastByte;
    }

tThaTohByte* ThaPwTohLastByteGet(ThaPwToh self)
    {
    if ((self == NULL) || (!self->hasFirstLast))
        return NULL;

    return &self->lastByte;
    }

eBool ThaTohByteIsPrevious(ThaPwToh self, const tThaTohByte* byte1, const tThaTohByte* byte2)
    {
    return (TohByteBitPosition(self, byte1) < TohByteBitPosition(self, byte2));
    }

eBool ThaTohByteIsAfter(ThaPwToh self, const tThaTohByte* byte1, const tThaTohByte* byte2)
    {
    return (TohByteBitPosition(self, byte1) > TohByteBitPosition(self, byte2));
    }

eAtRet ThaPwTohAllBytesAllow(ThaPwToh self)
    {
    uint32 position = 0;
    eAtRet ret = cAtOk;
    eAtSdhLineOverheadByte ohByte;
    uint8 sts1;
    tThaTohByte tohByte;

    if (self == NULL)
        return cAtErrorNullPointer;

    if (self->tohBytesMask == NULL)
        return cAtOk;

    while(ThaBitMaskBitPositionIsValid(self->tohBytesMask, position))
        {
        if (ThaBitMaskBitVal(self->tohBytesMask, position))
            {
            BitPositionToTohByte(self, position, &ohByte, &sts1);
            ret |= mMethodsGet(mTohAdapter(self))->Allow(mTohAdapter(self), TohByteConstruct(ohByte, sts1, &tohByte));
            }

        position = position + 1;
        }

    return ret;
    }

ThaPwAdapter ThaPwTohAdapterGet(AtPw self)
    {
    return (ThaPwAdapter)Adapter(self);
    }

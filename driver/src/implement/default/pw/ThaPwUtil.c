/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : ThaPwUtil.c
 *
 * Created Date: Aug 19, 2013
 *
 * Description : PW util
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaPwUtil.h"
#include "AtPdhNxDs0.h"
#include "AtPwCep.h"
#include "adapters/ThaPwAdapter.h"
#include "AtPwToh.h"
#include "AtModulePw.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 TimeInUsForNumBytes(uint32 numBytes, uint32 rateInBytePerMs)
    {
    uint32 us;
    uint32 timeInMs;
    uint32 remainBytes, remainTimeInUs;
    uint32 temp;

    if (rateInBytePerMs == 0)
        return 0;

    timeInMs = (numBytes / rateInBytePerMs);
    remainBytes = numBytes % rateInBytePerMs;

    temp = remainBytes * 1000;
    remainTimeInUs = temp / rateInBytePerMs;
    if (temp % rateInBytePerMs)
        remainTimeInUs += 1;

    us = timeInMs * 1000U + remainTimeInUs;
    return us;
    }

static uint16 NumPacketsFromUs(uint32 microsecond, uint32 rateInBytePerMs, uint32 payloadSize, eBool roundUp)
    {
    uint32 numBytes       = ThaPwUtilNumBytesForTimeInUs(microsecond, rateInBytePerMs);
    uint16 numPackets     = (uint16)(numBytes / payloadSize);
    uint32 remainingBytes = numBytes % payloadSize;

    if (remainingBytes != 0)
        {
        /* Just for safe, plus one packet if remaining bytes are greater
         * than 30% of payload size. */
        if (roundUp || (remainingBytes >= (payloadSize / 3)))
            numPackets = (uint16)(numPackets + 1);
        }

    return numPackets;
    }

uint32 ThaPwUtilNumBytesForTimeInUs(uint32 microsecond, uint32 rateInBytePerMs)
    {
    uint32 rateInKbps = rateInBytePerMs * 8U;
    uint32 rateIn64Kbps = rateInKbps / 64U;
    uint32 remainRateInKbps = rateInKbps % 64U;
    uint32 numBytes = (rateIn64Kbps * microsecond) / 125U;
    uint32 remainBytes;
    uint32 remainKbits;

    if (remainRateInKbps == 0)
        return numBytes;

    remainKbits = (remainRateInKbps * microsecond);
    remainBytes = remainKbits / 8000U;

    /* Round up one byte if remain bits is more than 30% */
    if ((remainKbits % 8000U) > 2400)
        remainBytes += 1;

    return numBytes + remainBytes;
    }

uint32 ThaPwUtilTimeInUsForNumPackets(uint32 numPackets, uint32 rateInBytePerMs, uint32 payloadSize)
    {
    uint32 numBytes = numPackets * payloadSize;
    return TimeInUsForNumBytes(numBytes, rateInBytePerMs);
    }

uint16 ThaPwUtilNumPacketsForTimeInUs(uint32 microseconds, uint32 rateInBytePerMs, uint32 payloadSize)
    {
    if (payloadSize == 0)
        return 0;

    return (uint16)NumPacketsFromUs(microseconds, rateInBytePerMs, payloadSize, cAtFalse);
    }

uint16 ThaPwUtilNumPacketsForTimeInUsRoundUp(uint32 microseconds, uint32 rateInBytePerMs, uint32 payloadSize)
    {
    if (payloadSize == 0)
        return 0;

    return (uint16)NumPacketsFromUs(microseconds, rateInBytePerMs, payloadSize, cAtTrue);
    }

static eBool PwIsCepFractional(AtPw pwAdapter)
    {
    AtPwCep pwCep;

    if (AtPwTypeGet(pwAdapter) != cAtPwTypeCEP)
        return cAtFalse;

    pwCep = (AtPwCep)ThaPwAdapterPwGet((ThaPwAdapter)pwAdapter);
    if (AtPwCepModeGet(pwCep) == cAtPwCepModeFractional)
        return cAtTrue;

    return cAtFalse;
    }

static uint32 CesopPayloadSizeInBytes(AtPw pwAdapter, AtChannel circuit, uint32 payloadSize)
    {
    AtModulePw modulePw = (AtModulePw)AtChannelModuleGet((AtChannel)pwAdapter);

    if (AtModulePwCesopPayloadSizeInByteIsEnabled(modulePw))
        return payloadSize;

    return (payloadSize * AtPdhNxDs0NumTimeslotsGet((AtPdhNxDS0)circuit));
    }

static eBool PayloadSizeInByteIsRoundDownEnabled(void)
    {
    return cAtTrue;
    }

uint32 ThaPwUtilPayloadSizeInBytes(AtPw pwAdapter, AtChannel circuit, uint32 payloadSize)
    {
    /* In case of CESoP, payload size is number of frames. But
     * the following formula expects payload size in bytes */
    if (AtPwTypeGet(pwAdapter) == cAtPwTypeCESoP)
        return CesopPayloadSizeInBytes(pwAdapter, circuit, payloadSize);

    /* In case of CEP fractional need to calculate real payload size */
    if (PwIsCepFractional(pwAdapter))
        return ThaPwCepFractionalSpePayloadSizeCalculate(pwAdapter, payloadSize);

    /* In case of TOH, payload is number of STM frames, payload in byte is number of frame * number of equipped byte */
    if (AtPwTypeGet(pwAdapter) == cAtPwTypeToh)
        {
        AtPwToh pwToh = (AtPwToh)ThaPwAdapterPwGet((ThaPwAdapter)pwAdapter);
        uint16 numByte = AtPwTohNumEnabledBytesGet(pwToh);
        if (numByte == 0)
            numByte = 1;

        return payloadSize * numByte;
        }

    return payloadSize;
    }

uint32 ThaPwUtilCesopPayloadSizeInByteToInFrames(AtPw pwAdapter, AtChannel circuit, uint32 numberOfBytes)
    {
    uint8 numberOfDs0 = AtPdhNxDs0NumTimeslotsGet((AtPdhNxDS0)circuit);
    uint32 numberOfFrames = 0;

    AtUnused(pwAdapter);
    if (numberOfDs0 == 0)
        return 0;

    numberOfFrames = (uint32)(numberOfBytes / numberOfDs0);

    if (PayloadSizeInByteIsRoundDownEnabled())
        return numberOfFrames;

    if ((numberOfFrames * numberOfDs0) != numberOfBytes)
        numberOfFrames += 1;

    return numberOfFrames;
    }


uint32 ThaPwUtilPayloadSizeInByteToHwUnit(AtPw pw, AtChannel circuit, uint32 numberOfBytes)
    {
    AtModulePw modulePw = (AtModulePw)AtChannelModuleGet((AtChannel)pw);

    if (AtModulePwCesopPayloadSizeInByteIsEnabled(modulePw))
        return numberOfBytes;

    if (AtPwTypeGet(pw) == cAtPwTypeCESoP)
        return ThaPwUtilCesopPayloadSizeInByteToInFrames(pw, circuit, numberOfBytes);

    /* In case of TOH, payload is number of STM frames, payload in byte is number of frame * number of equipped byte */
    if (AtPwTypeGet(pw) == cAtPwTypeToh)
        {
        uint32 numFrame = 0;
        AtPwToh pwToh = (AtPwToh)pw;
        uint16 numBytePerFrame = AtPwTohNumEnabledBytesGet(pwToh);
        if (numBytePerFrame == 0)
            numBytePerFrame = 1;

        numFrame = numberOfBytes / numBytePerFrame;
        if (numberOfBytes % numBytePerFrame)
            numFrame++;

        return numFrame;
        }

    return numberOfBytes;
    }

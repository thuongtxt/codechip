/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : ThaPwAtm.c
 *
 * Created Date: Nov 19, 2012
 *
 * Description : ATM PW
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../generic/common/AtChannelInternal.h"
#include "../../../generic/pw/AtModulePwInternal.h"
#include "../pw/ThaPwInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/* To initialize object */
static AtPw ObjectInit(AtPw self, uint32 pwId, AtModulePw module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaPwAtm));

    /* Super constructor should be called first */
    if (AtPwAtmObjectInit((AtPwAtm)self, pwId, module) == NULL)
        return NULL;

    /* Only initialize method structures one time */
    m_methodsInit = 1;

    return self;
    }

AtPw ThaPwAtmNew(AtModulePw self, uint32 pwId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPw newPw = mMethodsGet(osal)->MemAlloc(osal, sizeof(tThaPwAtm));
    if (newPw == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPw, pwId, self);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : ThaPwSAToP.c
 *
 * Created Date: Nov 17, 2012
 *
 * Description : Thalassa SAToP implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPwCounters.h"
#include "ThaPwInternal.h"
#include "adapters/ThaPwAdapter.h"
#include "../../../util/coder/AtCoderUtil.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtObjectMethods  m_AtObjectOverride;
static tAtPwMethods      m_AtPwOverride;
static tAtChannelMethods m_AtChannelOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaPwSAToP);
    }

static AtPw Adapter(AtPw pw)
    {
    return (AtPw)((ThaPwSAToP)pw)->adapter;
    }

#include "ThaPwCommonSetup.h"

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaPwSAToP object = (ThaPwSAToP)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObject(adapter);
    }

static void OverrideAtObject(ThaPwSAToP self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(ThaPwSAToP self)
    {
    OverrideAtObject(self);
    OverrideAtPw((AtPw)self);
    OverrideAtChannel((AtPw)self);
    }

/* To initialize object */
AtPw ThaPwSAToPObjectInit(AtPw self, uint32 pwId, AtModulePw module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (AtPwSAToPObjectInit((AtPwSAToP)self, pwId, module) == NULL)
        return NULL;

    /* Override */
    Override((ThaPwSAToP)self);
    m_methodsInit = 1;

    /* PW adapter */
    ((ThaPwSAToP)self)->adapter = ThaPwSAToPAdapterNew(self);

    return self;
    }

AtPw ThaPwSAToPNew(AtModulePw self, uint32 pwId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPw newPw = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPw == NULL)
        return NULL;

    /* Construct it */
    return ThaPwSAToPObjectInit(newPw, pwId, self);
    }

ThaPwAdapter ThaPwSAToPAdapterGet(AtPw self)
    {
    return (ThaPwAdapter)Adapter(self);
    }

void ThaPwSAToPAdapterDelete(AtPw self)
    {
    AtObjectDelete((AtObject)((ThaPwSAToP)self)->adapter);
    ((ThaPwSAToP)self)->adapter = NULL;
    }

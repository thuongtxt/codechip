/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : ThaPwUtil.h
 * 
 * Created Date: Aug 19, 2013
 *
 * Description : PW util
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPWUTIL_H_
#define _THAPWUTIL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPw.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define mSpeedIn64Kbps(rateInBytePerMs_) (((rateInBytePerMs_) * 8) / 64)
#define mSpeedInKbps(rateInBytePerMs_)   (((rateInBytePerMs_) * 8))

/*--------------------------- Typedefs ---------------------------------------*/
typedef enum eAtCircuitRateId
    {
    cAtVC3RateId       = 0,
    cAtVC4RateId       ,
    cAtVC4_4cRateId    ,
    cAtVC4_16cRateId   ,
    cAtVC4_64cId       ,
    cAtVC11RateId      ,
    cAtVC12RateId      ,
    cAtDS1_SAToPRateId ,
    cAtE1_SAToPRateId  ,
    cAtDS3_SAToPRateId ,
    cAtE3_SAToPRateId  ,
    cAtDS0_CESoPRateId ,
    cAtDS1_CASRateId   ,
    cAtE1_CASRateId    ,
    cAtNumRateId
    } eAtCircuitRateId;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 ThaPwUtilPayloadSizeInBytes(AtPw pw, AtChannel circuit, uint32 payloadSize);
uint32 ThaPwUtilNumBytesForTimeInUs(uint32 microsecond, uint32 rateInBytePerMs);
uint32 ThaPwUtilTimeInUsForNumPackets(uint32 numPackets, uint32 rateInBytePerMs, uint32 payloadSize);
uint16 ThaPwUtilNumPacketsForTimeInUs(uint32 microseconds, uint32 rateInBytePerMs, uint32 payloadSize);
uint16 ThaPwUtilNumPacketsForTimeInUsRoundUp(uint32 microseconds, uint32 rateInBytePerMs, uint32 payloadSize);
uint32 ThaPwUtilCesopPayloadSizeInByteToInFrames(AtPw pwAdapter, AtChannel circuit, uint32 numberOfBytes);
uint32 ThaPwUtilPayloadSizeInByteToHwUnit(AtPw pw, AtChannel circuit, uint32 numberOfBytes);

#ifdef __cplusplus
}
#endif
#endif /* _THAPWUTIL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : PW Module
 *
 * File        : ThaModulePw.h
 *
 * Created Date: Jan 09, 2013
 *
 * Description : Functions to work with other modules
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEPW_H_
#define _THAMODULEPW_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModulePw.h"
#include "AtPdhDe1.h"
#include "debugger/ThaPwDebugger.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cThaModulePwInvalidPwId cBit15_0
#define cThaPwInvalidEthPortId  cBit7_0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModulePw * ThaModulePw;
typedef struct tThaHwPw * ThaHwPw;
typedef struct tThaPwActivator * ThaPwActivator;
typedef struct tThaPwHeaderProvider * ThaPwHeaderProvider;
typedef struct tThaPwDefectController * ThaPwDefectController;
typedef struct tThaPwHeaderController * ThaPwHeaderController;
typedef struct tThaPwEthPortBinder * ThaPwEthPortBinder;
typedef struct tThaPwInterruptProcessor * ThaPwInterruptProcessor;
typedef struct tThaPwAdapter      * ThaPwAdapter;

typedef enum eThaPwLookupMode
    {
    cThaPwLookupModeInvalid, /* Invalid lookup mode */
    cThaPwLookupModePsn,     /* PSN is used to lookup (default mode) */
    cThaPwLookupMode2Vlans   /* 2 VLANs are used to lookup (customer specific) */
    }eThaPwLookupMode;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePw ThaModulePwNew(AtDevice device);

uint8 ThaModulePwNumParts(ThaModulePw self);
uint16 ThaModulePwNumPwsPerPart(ThaModulePw self);
uint8 ThaModulePwPartOfPw(ThaModulePw self, AtPw pw);
uint32 ThaModulePwLocalPwId(ThaModulePw self, AtPw pw);

eThaPwLookupMode ThaModulePwLookupModeGet(ThaModulePw self, AtPw pw);
eAtRet ThaModulePwLookupModeSet(ThaModulePw self, AtPw pw, eThaPwLookupMode lookupMode);
eBool ThaModulePwCasIsSupported(ThaModulePw self);
eBool ThaModulePwHspwIsSupported(ThaModulePw self);
eBool ThaModulePwEnableErrorCheckingByDefault(ThaModulePw self);
eBool ThaModulePwIdleCodeIsSupported(ThaModulePw self);
eBool ThaModulePwMbitCounterIsSupported(ThaModulePw self);
eBool ThaModulePwCanCheckPwPayloadSize(ThaModulePw self);
eBool ThaModulePwTimingModeIsApplicableForEPAR(ThaModulePw self, eAtTimingMode timingMode);
eBool ThaModulePwShouldHandleCircuitAisForcingOnEnabling(ThaModulePw self);
eBool ThaModulePwShouldMaskDefectAndFailureOnPwDisabling(ThaModulePw self);

void ThaModulePwPwEnablePrepare(ThaModulePw self, AtPw adapter);
void ThaModulePwPwEnableFinish(ThaModulePw self, AtPw adapter);
void ThaModulePwPwDisablePrepare(ThaModulePw self, AtPw adapter);
void ThaModulePwPwDisableFinish(ThaModulePw self, AtPw adapter);
eAtRet ThaModulePwPwFinishRemoving(ThaModulePw self, AtPw adapter);

/* Pw binding time optimization */
eAtRet ThaModulePwPwFinishRemovingForBinding(ThaModulePw self, AtPw adapter);
eAtRet ThaModulePwIsPwBindingTimeOptimization(ThaModulePw self);

/* Shared buffers */
uint8 *ThaModulePwSharedHeaderBuffer(ThaModulePw self, uint32 *bufferSize);
uint8 *ThaModulePwSharedPsnBuffer(ThaModulePw self, uint32 *bufferSize);

/* PW activator */
eBool ThaModulePwDynamicPwAllocation(ThaModulePw self);
ThaPwActivator ThaModulePwActivator(ThaModulePw self);

/* Concrete PWs */
AtPw ThaPwCepNew(AtModulePw self, uint32 pwId, eAtPwCepMode mode);
AtPw ThaPwSAToPNew(AtModulePw self, uint32 pwId);
AtPw ThaPwCESoPNew(AtModulePw self, uint32 pwId, eAtPwCESoPMode mode);
AtPw ThaPwHdlcNew(AtModulePw self, AtHdlcChannel hdlcChannel);
AtPw ThaPwAtmNew(AtModulePw self, uint32 pwId);
AtPw ThaPwTohNew(AtModulePw self, uint32 pwId);
AtPw ThaPwPppNew(AtModulePw self, AtPppLink pppLink);
AtPw ThaPwMlpppNew(AtModulePw self, AtMpBundle mpBundle);
AtPw ThaPwFrNew(AtModulePw self, AtFrVirtualCircuit frVc);

uint32 ThaModulePwFreePwGet(ThaModulePw self);
uint32 ThaPwPartOffset(AtPw pw, eAtModule moduleId);

ThaPwHeaderProvider ThaModulePwHeaderProviderGet(ThaModulePw self);

ThaPwDefectController ThaModulePwDefectControllerCreate(ThaModulePw self, AtPw pw);
AtPwCep ThaModulePwCepObjectCreate(AtModulePw self, uint32 pwId, eAtPwCepMode mode);

/* HW storage for warm restoring purpose */
uint32 ThaModulePwLabelGetFromHwStorage(ThaModulePw self, AtPw pw);
eAtRet ThaModulePwLabelSaveToHwStorage(ThaModulePw self, AtPw pw, uint32 pwLabel);

eAtRet ThaModulePwEthPortIdSaveToHwStorage(ThaModulePw self, AtPw pw, uint8 ethPortId);
uint8 ThaModulePwEthPortIdGetFromHwStorage(ThaModulePw self, AtPw pw);
eAtRet ThaModulePwEthPortIdHwStorageClear(ThaModulePw self, AtPw pw);

uint16 ThaModulePwDe1BoundPwIdGetFromHwStorage(ThaModulePw self, AtPdhDe1 de1);
eAtRet ThaModulePwDe1BoundPwIdSaveToHwStorage(ThaModulePw self, AtPdhDe1 de1, uint16 pwHwId);
eAtRet ThaModulePwDe1BoundPwIdHwStorageClear(ThaModulePw self, AtPdhDe1 de1);

eBool ThaModulePwPwEnableGetFromHwStorage(ThaModulePw self, AtPw pw);
eAtRet ThaModulePwPwEnableSaveToHwStorage(ThaModulePw self, AtPw pw, eBool enable);

eBool ThaModulePwMefOverMplsIsSupported(ThaModulePw self);

eAtRet ThaModulePwBufferSizeDisparitySaveToHwStorage(ThaModulePw self, AtPw pw, int disparityInUs);
int ThaModulePwBufferSizeDisparityGetFromHwStorage(ThaModulePw self, AtPw pw);

eAtRet ThaModulePwBufferDelayDisparitySaveToHwStorage(ThaModulePw self, AtPw pw, int disparityInUs);
int ThaModulePwBufferDelayDisparityGetFromHwStorage(ThaModulePw self, AtPw pw);

eAtRet ThaModulePwSendLbitPacketsToCdrEnable(AtModulePw self, eBool enable);
eBool ThaModulePwSendLbitPacketsToCdrIsEnabled(AtModulePw self);

eBool ThaModulePwCanBindEthPort(ThaModulePw self, AtPw pw, AtEthPort port);
eBool ThaModulePwCanBindEthFlow(ThaModulePw self, AtPw pw, AtEthFlow flow);

ThaPwHeaderController ThaModulePwHeaderControllerObjectCreate(ThaModulePw self, AtPw adapter);
ThaPwHeaderController ThaModulePwBackupHeaderControllerObjectCreate(ThaModulePw self, AtPw adapter);

/* Work with SUR module */
eBool ThaModulePwShouldReadAllCountersFromSurModule(ThaModulePw self);

/* For debugging, used to specify what modules used to read PW counters */
eAtRet ThaModulePwDebugCountersModuleSet(ThaModulePw self, uint32 module);
uint32 ThaModulePwDebugCountersModuleGet(ThaModulePw self);
ThaPwDebugger ThaModulePwPwDebuggerGet(ThaModulePw self);
eAtRet ThaModulePwJitterEmptyTimeoutSet(ThaModulePw self, uint32 timeoutMs);
uint32 ThaModulePwJitterEmptyTimeoutGet(ThaModulePw self);
eAtRet ThaModulePwDebugAlarmModuleSet(ThaModulePw self, uint32 module);
uint32 ThaModulePwDebugAlarmModuleGet(ThaModulePw self);
ThaPwDefectController ThaModulePwDebugDefectControllerCreate(ThaModulePw self, AtPw pw);
eAtRet ThaModulePwHitlessHeaderChangeEnable(ThaModulePw self, eBool enabled);
eBool ThaModulePwHitlessHeaderChangeIsEnabled(ThaModulePw self);
AtChannel ThaModulePwReferenceCircuitToConfigureHardware(ThaModulePw self, ThaPwAdapter adapter, AtChannel circuit);

/* For BW handling */
ThaPwEthPortBinder ThaModulePwEthPortBinder(ThaModulePw self);
eBool ThaModulePwShouldStrictlyLimitResources(ThaModulePw self);
eBool ThaModulePwResourcesLimitationIsDisabled(ThaModulePw self);
void ThaModulePwResourcesLimitationDisable(ThaModulePw self, eBool isDisabled);

/* For interrupt processor */
ThaPwInterruptProcessor ThaModulePwInterruptProcessor(ThaModulePw self);
uint32 ThaModulePwDefectBaseAddress(ThaModulePw self);
uint32 ThaModulePwDefectSliceInterruptEnable(ThaModulePw self, eBool enable);
uint32 ThaModulePwDefectCurrentStatusRegister(ThaModulePw self);
uint32 ThaModulePwDefectInterruptStatusRegister(ThaModulePw self);
uint32 ThaModulePwDefectInterruptMaskRegister(ThaModulePw self);
eAtRet ThaModulePwInterruptEnable(ThaModulePw self, eBool enable);

/* For standby driver */
uint32 ThaModulePwPwAdapterHwId(ThaModulePw self, AtPw adapter);
ThaPwAdapter ThaModulePwHwPwId2Adapter(ThaModulePw self, uint32 hwId);

/* Utils */
uint32 ThaModulePwMinRtpPayloadType(void);
uint32 ThaModulePwMaxRtpPayloadType(void);
eBool ThaModulePwRtpPayloadTypeIsInRange(uint32 payloadType);

/* Product concretes */
AtModulePw Tha60000031ModulePwNew(AtDevice device);
AtModulePw Tha60031021ModulePwNew(AtDevice device);
AtModulePw Tha60070041ModulePwNew(AtDevice device);
AtModulePw Tha60091023ModulePwNew(AtDevice device);
AtModulePw Tha60001031ModulePwNew(AtDevice device);
AtModulePw Tha60030080ModulePwNew(AtDevice device);
AtModulePw Tha60030081ModulePwNew(AtDevice device);
AtModulePw Tha60031031EpModulePwNew(AtDevice device);
AtModulePw Tha60031031ModulePwNew(AtDevice device);
AtModulePw Tha60031033ModulePwNew(AtDevice device);
AtModulePw Tha60031131ModulePwNew(AtDevice device);
AtModulePw Tha60070061ModulePwNew(AtDevice device);
AtModulePw Tha60150011ModulePwNew(AtDevice device);
AtModulePw Tha60200011ModulePwNew(AtDevice device);
AtModulePw Tha60035021ModulePwNew(AtDevice device);
AtModulePw Tha60071021ModulePwNew(AtDevice device);
AtModulePw Tha600a0011ModulePwNew(AtDevice device);
AtModulePw Tha60210031ModulePwNew(AtDevice device);
AtModulePw Tha60210021ModulePwNew(AtDevice device);
AtModulePw Tha60210011ModulePwNew(AtDevice device);
AtModulePw Tha61210011ModulePwNew(AtDevice device);
AtModulePw Tha60050061ModulePwNew(AtDevice device);
AtModulePw Tha60240021ModulePwNew(AtDevice device);
AtModulePw Tha60020011ModulePwNew(AtDevice device);
AtModulePw ThaStmPwProductModulePwNew(AtDevice device);
AtModulePw ThaPdhPwProductModulePwNew(AtDevice device);
AtModulePw Tha60210051ModulePwNew(AtDevice device);
AtModulePw Tha6A000010ModulePwNew(AtDevice device);
AtModulePw Tha6A033111ModulePwNew(AtDevice device);
AtModulePw Tha6A210021ModulePwNew(AtDevice device);
AtModulePw Tha6A210031ModulePwNew(AtDevice device);
AtModulePw Tha60210012ModulePwNew(AtDevice device);
AtModulePw Tha60290021ModulePwNew(AtDevice device);
AtModulePw Tha60290022ModulePwNew(AtDevice device);
AtModulePw Tha60210061ModulePwNew(AtDevice device);
AtModulePw Tha60290011ModulePwNew(AtDevice device);
AtModulePw Tha6A290011ModulePwNew(AtDevice device);
AtModulePw Tha6A290021ModulePwNew(AtDevice device);
AtModulePw Tha60290022ModulePwNew(AtDevice device);
AtModulePw Tha6A290022ModulePwNew(AtDevice device);
AtModulePw Tha60291011ModulePwNew(AtDevice device);
AtModulePw Tha60291022ModulePwNew(AtDevice device);
AtModulePw Tha60290081ModulePwNew(AtDevice device);
AtModulePw Tha6A290081ModulePwNew(AtDevice device);

/* Product PW concretes */
AtPw Tha60150011PwCepNew(AtModulePw self, uint32 pwId, eAtPwCepMode mode);
AtPw Tha60150011PwSAToPNew(AtModulePw self, uint32 pwId);

/* Subport VLANs */
eBool ThaModulePwSubportVlansSupported(ThaModulePw self);
eAtRet ThaModulePwTxSubportVlanSet(ThaModulePw self, uint32 subPortVlanIndex, const tAtVlan *vlan);
eAtRet ThaModulePwTxSubportVlanGet(ThaModulePw self, uint32 subPortVlanIndex, tAtVlan *vlan);
eAtRet ThaModulePwTxPwSubPortVlanIndexSet(ThaModulePw self, AtPw adapter, uint32 subPortVlanIndex);
uint32 ThaModulePwTxPwSubPortVlanIndexGet(ThaModulePw self, AtPw adapter);
eAtRet ThaModulePwExpectedSubportVlanSet(ThaModulePw self, uint32 subPortVlanIndex, const tAtVlan *expectedVlan);
eAtRet ThaModulePwExpectedSubportVlanGet(ThaModulePw self, uint32 subPortVlanIndex, tAtVlan *expectedVlan);
uint32 ThaModulePwNumSubPortVlans(ThaModulePw self);
eAtRet ThaModulePwSubPortVlanInsertionEnable(ThaModulePw self, eBool enabled);
eBool ThaModulePwSubPortVlanInsertionIsEnabled(ThaModulePw self);
eAtRet ThaModulePwSubPortVlanCheckingEnable(ThaModulePw self, eBool enabled);
eBool ThaModulePwSubPortVlanCheckingIsEnabled(ThaModulePw self);
eAtRet ThaModulePwSubPortVlanDebug(ThaModulePw self);

eBool ThaModulePwTxActiveForceSupported(ThaModulePw self);

/* To Get Kbyte PW */
AtPw ThaModulePwKBytePwGet(ThaModulePw self);
eBool ThaModulePwNeedDoReConfig(ThaModulePw self);
eBool ThaModulePwNeedClearDebug(ThaModulePw self);
#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEPW_H_ */

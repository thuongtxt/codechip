/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : ThaPwCep.c
 *
 * Created Date: Nov 17, 2012
 *
 * Description : Thalassa CEP implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaModulePw.h"
#include "ThaPwInternal.h"
#include "../../default/man/ThaDeviceInternal.h"
#include "adapters/ThaPwAdapterInternal.h"
#include "../../../generic/pw/AtPwInternal.h"
#include "../../../util/coder/AtCoderUtil.h"
#include "AtPwCep.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mCepAdapter(self) ((ThaPwCepAdapter)Adapter((AtPw)self))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtObjectMethods  m_AtObjectOverride;
static tAtPwMethods      m_AtPwOverride;
static tAtChannelMethods m_AtChannelOverride;
static tAtPwCepMethods   m_AtPwCepOverride;

static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;
static const tAtPwCepMethods   *m_AtPwCepMethods   = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaPwCep);
    }

static AtPw Adapter(AtPw pw)
    {
    return (AtPw)((ThaPwCep)pw)->adapter;
    }

static eAtModulePwRet CwEbmEnable(AtPwCep self, eBool enable)
    {
    return mMethodsGet(mCepAdapter(self))->EbmEnable(mCepAdapter(self), enable);
    }

static eBool CwEbmIsEnabled(AtPwCep self)
    {
    return mMethodsGet(mCepAdapter(self))->EbmIsEnabled(mCepAdapter(self));
    }

static eAtModulePwRet EparEnable(AtPwCep self, eBool enable)
    {
    return mMethodsGet(mCepAdapter(self))->EparEnable(mCepAdapter(self), enable);
    }

static eBool EparIsEnabled(AtPwCep self)
    {
    return mMethodsGet(mCepAdapter(self))->EparIsEnabled(mCepAdapter(self));
    }

static eAtModulePwRet Equip(AtPwCep self, AtChannel channel, eBool equip)
    {
    eAtRet ret;

    ret = m_AtPwCepMethods->Equip(self, channel, equip);
    if (ret != cAtOk)
        return ret;

    if (AtChannelDeviceInWarmRestore((AtChannel)self))
        return cAtOk;

    ret = mMethodsGet(mCepAdapter(self))->Equip(mCepAdapter(self), channel, equip);

    /* If not success, un-equip at super */
    if ((ret != cAtOk) && (equip == cAtTrue))
        m_AtPwCepMethods->Equip(self, channel, cAtFalse);

    return ret;
    }

static eAtModulePwRet CwAutoNBitEnable(AtPwCep self, eBool enable)
    {
    return mMethodsGet(mCepAdapter(self))->CwAutoNBitEnable(mCepAdapter(self), enable);
    }

static eAtModulePwRet CwAutoPBitEnable(AtPwCep self, eBool enable)
    {
    return mMethodsGet(mCepAdapter(self))->CwAutoPBitEnable(mCepAdapter(self), enable);
    }

static eBool CwAutoNBitIsEnabled(AtPwCep self)
    {
    return mMethodsGet(mCepAdapter(self))->CwAutoNBitIsEnabled(mCepAdapter(self));
    }

static eBool CwAutoPBitIsEnabled(AtPwCep self)
    {
    return mMethodsGet(mCepAdapter(self))->CwAutoPBitIsEnabled(mCepAdapter(self));
    }

#include "ThaPwCommonSetup.h"

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaPwCep object = (ThaPwCep)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObject(adapter);
    }

static void OverrideAtObject(ThaPwCep self)
    {
    AtObject object = (AtObject)self;

    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtPwCep(ThaPwCep self)
    {
    AtPwCep cepPw = (AtPwCep)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPwCepMethods = mMethodsGet(cepPw);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPwCepOverride, m_AtPwCepMethods, sizeof(m_AtPwCepOverride));

        mMethodOverride(m_AtPwCepOverride, Equip);
        mMethodOverride(m_AtPwCepOverride, CwEbmEnable);
        mMethodOverride(m_AtPwCepOverride, CwEbmIsEnabled);
        mMethodOverride(m_AtPwCepOverride, EparEnable);
        mMethodOverride(m_AtPwCepOverride, EparIsEnabled);
        mMethodOverride(m_AtPwCepOverride, CwAutoNBitEnable);
        mMethodOverride(m_AtPwCepOverride, CwAutoPBitEnable);
        mMethodOverride(m_AtPwCepOverride, CwAutoNBitIsEnabled);
        mMethodOverride(m_AtPwCepOverride, CwAutoPBitIsEnabled);
        }

    mMethodsSet(cepPw, &m_AtPwCepOverride);
    }

static void Override(ThaPwCep self)
    {
    OverrideAtObject(self);
    OverrideAtPw((AtPw)self);
    OverrideAtChannel((AtPw)self);
    OverrideAtPwCep(self);
    }

/* To initialize object */
AtPw ThaPwCepObjectInit(AtPw self, uint32 pwId, AtModulePw module, eAtPwCepMode mode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (AtPwCepObjectInit((AtPwCep)self, pwId, module, mode) == NULL)
        return NULL;

    /* Override */
    Override((ThaPwCep)self);
    m_methodsInit = 1;

    /* PW adapter */
    if (mode != cAtPwCepModeFractional)
        ((ThaPwCep)self)->adapter = ThaPwCepAdapterNew(self);
    else
        ((ThaPwCep)self)->adapter = ThaPwCepFractionalAdapterNew(self);

    return self;
    }

AtPw ThaPwCepNew(AtModulePw self, uint32 pwId, eAtPwCepMode mode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPw newPw = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPw == NULL)
        return NULL;

    /* Construct it */
    return ThaPwCepObjectInit(newPw, pwId, self, mode);
    }

ThaPwAdapter ThaPwCepAdapterGet(AtPw self)
    {
    return (ThaPwAdapter)Adapter(self);
    }

void ThaPwCepAdapterDelete(AtPw self)
    {
    AtObjectDelete((AtObject)((ThaPwCep)self)->adapter);
    ((ThaPwCep)self)->adapter = NULL;
    }

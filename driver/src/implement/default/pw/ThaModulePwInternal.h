/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : ThaModulePwInternal.h
 * 
 * Created Date: Jun 1, 2013
 *
 * Description : PW module class
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEPWINTERNAL_H_
#define _THAMODULEPWINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtCommon.h"
#include "commacro.h"

#include "../../../generic/pw/AtModulePwInternal.h"
#include "../../../generic/man/AtDeviceInternal.h"
#include "../man/ThaDeviceInternal.h"
#include "../pda/ThaModulePdaInternal.h"
#include "../pda/ThaModulePda.h"
#include "../cla/pw/ThaModuleClaPw.h"
#include "../cdr/ThaModuleCdrStm.h"

#include "ThaModulePw.h"
#include "ThaPwInternal.h"
#include "activator/ThaPwActivator.h"
#include "headerprovider/ThaPwHeaderProvider.h"
#include "defectcontrollers/ThaPwDefectController.h"
#include "headerprovider/ThaPwHeaderProvider.h"
#include "ethportbinder/ThaPwEthPortBinder.h"
#include "interruptprocessor/ThaPwInterruptProcessor.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModulePwMethods
    {
    eBool (*DynamicPwAllocation)(ThaModulePw self);
    ThaPwActivator (*PwActivatorCreate)(ThaModulePw self);
    eBool (*CasIsSupported)(ThaModulePw self);
    eBool (*EnableErrorCheckingByDefault)(ThaModulePw self);
    uint16 (*NumPwsPerPart)(ThaModulePw self);
    uint32 (*LocalPwId)(ThaModulePw self, AtPw adapter);
    eBool (*PwIdleCodeIsSupported)(ThaModulePw self);
    eBool (*PwMbitCounterIsSupported)(ThaModulePw self);
    eBool (*UseLbitPacketsForAcrDcr)(ThaModulePw self);
    ThaPwHeaderProvider (*PwHeaderProviderCreate)(ThaModulePw self);
    eAtRet (*DidChangeCVlanOnPw)(ThaModulePw self, AtPw pwAdapter, const tAtEthVlanTag *cVlan);
    ThaPwDefectController (*DefectControllerCreate)(ThaModulePw self, AtPw pw);
    ThaPwDefectController (*DebugDefectControllerCreate)(ThaModulePw self, AtPw pw);
    eAtRet (*ResetPw)(ThaModulePw self, AtPw pw);
    eBool (*TimingModeIsApplicableForEPAR)(ThaModulePw self, eAtTimingMode timingMode);
    eBool (*CanBindEthPort)(ThaModulePw self, AtPw pw, AtEthPort port);
    eBool (*CanBindEthFlow)(ThaModulePw self, AtPw pw, AtEthFlow flow);
    eAtRet (*TdmPwInit)(ThaModulePw self, AtPw pw);
    eBool (*HitlessHeaderChangeShouldBeEnabledByDefault)(ThaModulePw self);
    eBool (*ShouldHandleCircuitAisForcingOnEnabling)(ThaModulePw self);
    eBool (*ShouldMaskDefectAndFailureOnPwDisabling)(ThaModulePw self);

    /* For PW adding/removing process */
    void (*PwEnablePrepare)(ThaModulePw self, AtPw adapter);
    void (*PwEnableFinish)(ThaModulePw self, AtPw adapter);
    void (*PwDisablePrepare)(ThaModulePw self, AtPw adapter);
    void (*PwDisableFinish)(ThaModulePw self, AtPw adapter);
    eAtRet (*PwFinishRemoving)(ThaModulePw self, AtPw adapter);
    eAtRet (*PwFinishRemovingForBinding)(ThaModulePw self, AtPw adapter);
    eBool (*IsPwBindingTimeOptimization)(ThaModulePw self);

    /* For warm restore */
    uint32 (*PwLabelGetFromHwStorage)(ThaModulePw self, AtPw pw);
    eAtRet (*PwLabelSaveToHwStorage)(ThaModulePw self, AtPw pw, uint32 pwLabel);
    uint8  (*PwEthPortIdGetFromHwStorage)(ThaModulePw self, AtPw pw);
    eAtRet (*PwEthPortIdSaveToHwStorage)(ThaModulePw self, AtPw pw, uint8 ethPortId);
    eAtRet (*PwEthPortIdHwStorageClear)(ThaModulePw self, AtPw pw);
    eBool  (*PwEnableGetFromHwStorage)(ThaModulePw self, AtPw pw);
    eAtRet (*PwEnableSaveToHwStorage)(ThaModulePw self, AtPw pw, eBool enable);
    uint16 (*De1BoundPwIdGetFromHwStorage)(ThaModulePw self, AtPdhDe1 de1);
    eAtRet (*De1BoundPwIdSaveToHwStorage)(ThaModulePw self, AtPdhDe1 de1, uint16 pwHwId);
    eAtRet (*De1BoundPwIdHwStorageClear)(ThaModulePw self, AtPdhDe1 de1);
    eBool  (*MefOverMplsIsSupported)(ThaModulePw self);
    eAtRet (*PwBufferSizeDisparitySaveToHwStorage)(ThaModulePw self, AtPw pw, int disparityInUs);
    int    (*PwBufferSizeDisparityGetFromHwStorage)(ThaModulePw self, AtPw pw);
    eAtRet (*PwBufferDelayDisparitySaveToHwStorage)(ThaModulePw self, AtPw pw, int disparityInUs);
    int    (*PwBufferDelayDisparityGetFromHwStorage)(ThaModulePw self, AtPw pw);

    /* PW header handler */
    ThaPwHeaderController (*HeaderControllerObjectCreate)(ThaModulePw self, AtPw adapter);
    ThaPwHeaderController (*BackupHeaderControllerObjectCreate)(ThaModulePw self, AtPw adapter);

    eBool (*CanCheckPwPayloadSize)(ThaModulePw self);
    ThaPwDebugger (*PwDebuggerCreate)(ThaModulePw self);

    /* Work with Surveillance */
    uint32 (*DefaultCounterModule)(ThaModulePw self);
    uint32 (*DefaultDefectModule)(ThaModulePw self);
    eBool (*DebugCountersModuleIsSupported)(ThaModulePw self, uint32 module);

    /* For standby driver */
    uint32 (*PwAdapterHwId)(ThaModulePw self, ThaPwAdapter adapter);
    ThaPwAdapter (*HwPwId2Adapter)(ThaModulePw self, uint32 hwId);

    /* For Bandwidth handle */
    eBool (*ShouldStrictlyLimitResources)(ThaModulePw self);
    ThaPwEthPortBinder (*PwEthPortBinderCreate)(ThaModulePw self);

    /* For PWGroup */
    uint32 (*StartVersionSupportHspw)(ThaModulePw self);
    
    /* For PW counters */
    uint32 (*NumPwsSupportCounters)(ThaModulePw self);

    /* Interrupt processor helper. */
    ThaPwInterruptProcessor (*InterruptProcessorCreate)(ThaModulePw self);
    ThaPwInterruptProcessor (*DebugIntrProcessorCreate)(ThaModulePw self);

    /* Specific for KByte PW */
    eBool (*NeedDoReConfig)(ThaModulePw self);
    AtPw  (*PwKBytePwGet)(ThaModulePw self);

    /* Subport VLANs (just some products have this) */
    eBool (*SubportVlansSupported)(ThaModulePw self);
    eAtRet (*TxSubportVlanSet)(ThaModulePw self, uint32 subPortVlanIndex, const tAtVlan *vlan);
    eAtRet (*TxSubportVlanGet)(ThaModulePw self, uint32 subPortVlanIndex, tAtVlan *vlan);
    eAtRet (*TxPwSubPortVlanIndexSet)(ThaModulePw self, AtPw adapter, uint32 subPortVlanIndex);
    uint32 (*TxPwSubPortVlanIndexGet)(ThaModulePw self, AtPw adapter);
    eAtRet (*ExpectedSubportVlanSet)(ThaModulePw self, uint32 subPortVlanIndex, const tAtVlan *expectedVlan);
    eAtRet (*ExpectedSubportVlanGet)(ThaModulePw self, uint32 subPortVlanIndex, tAtVlan *expectedVlan);
    eAtRet (*SubPortVlanInsertionEnable)(ThaModulePw self, eBool enabled);
    eBool (*SubPortVlanInsertionIsEnabled)(ThaModulePw self);
    eAtRet (*SubPortVlanCheckingEnable)(ThaModulePw self, eBool enabled);
    eBool (*SubPortVlanCheckingIsEnabled)(ThaModulePw self);
    uint32 (*NumSubPortVlans)(ThaModulePw self);

    /* PW forced active (just some products have this) */
    eBool (*TxActiveForceSupported)(ThaModulePw self);

    AtChannel (*ReferenceCircuitToConfigureHardware)(ThaModulePw self, ThaPwAdapter adapter, AtChannel circuit);
    eBool (*NeedClearDebug)(ThaModulePw self);
    }tThaModulePwMethods;

typedef struct tThaModulePw
    {
    tAtModulePw super;
    const tThaModulePwMethods *methods;

    /* Private data */
    uint8 headerBuffer[cThaMaxPwPsnHdrLen];
    uint8 psnBuffer[cThaMaxPwPsnHdrLen];
    ThaPwActivator pwActivator;
    ThaPwHeaderProvider pwHeaderProvider;
    eBool interruptIsEnabled;
    ThaPwDebugger pwDebugger;
    ThaPwEthPortBinder pwEthPortBinder;
    ThaPwInterruptProcessor intrProcessor;
    ThaPwInterruptProcessor debugIntrProcessor;

    /* For debugging. */
    uint32 countersModule; /* This is to specify what modules used to read counters for each direction */
    uint32 alarmModule;   /* This is to specify what modules used to get pw defect */
    uint32 jitterEmptyTimeout;
    eBool disableResourcesLimitation;
    eBool hitlessHeaderChangeEnabled;
    }tThaModulePw;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePw ThaModulePwObjectInit(AtModulePw self, AtDevice device);

eAtRet ThaModulePwDidChangeCVlanOnPw(ThaModulePw self, AtPw pw, const tAtEthVlanTag *cVlan);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEPWINTERNAL_H_ */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : ThaPwEthPortBinder.h
 * 
 * Created Date: Oct 19, 2015
 *
 * Description : PW bandwidth controller interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPWETHPORTBINDER_H_
#define _THAPWETHPORTBINDER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPw.h"
#include "../ThaModulePw.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet ThaPwEthPortBinderPwEnable(ThaPwEthPortBinder self, AtPw pw, eBool enable);
eBool ThaPwEthPortBinderPwIsEnabled(ThaPwEthPortBinder self, AtPw adapter);
eBool ThaPwEthPortBinderPwBandwidthIsAcceptable(ThaPwEthPortBinder self, AtPw pw, uint64 newPwBandwidth);
eBool ThaPwEthPortBinderPwCanBindEthPort(ThaPwEthPortBinder self, AtPw pw, AtEthPort ethPort, uint64 bwWithPort);
eAtRet ThaPwEthPortBinderPwBandwidthUpdate(ThaPwEthPortBinder self, AtPw pw, uint64 newPwBandwidth);
eAtRet ThaPwEthPortBinderPwEthPortBind(ThaPwEthPortBinder self, AtPw pw, AtEthPort ethPort);
eAtRet ThaPwEthPortBinderPwEthPortUnBind(ThaPwEthPortBinder self, AtPw pw, AtEthPort ethPort);
uint64 ThaPwEthPortBinderPwRemainingBw(ThaPwEthPortBinder self, AtPw pw);
eBool ThaPwEthPortBinderNeedToManageEthPortBandwidth(ThaPwEthPortBinder self);

/* Object create */
ThaPwEthPortBinder ThaPwEthPortBinderNew(ThaModulePw module);
ThaPwEthPortBinder Tha60210031PwEthPortBinderNew(ThaModulePw module);
ThaPwEthPortBinder Tha60210051PwEthPortBinderNew(ThaModulePw module);
ThaPwEthPortBinder Tha60210021PwEthPortBinderNew(ThaModulePw module);

#ifdef __cplusplus
}
#endif
#endif /* _THAPWETHPORTBINDER_H_ */


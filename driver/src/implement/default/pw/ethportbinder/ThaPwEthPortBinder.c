/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : ThaPwEthPortBinder.c
 *
 * Created Date: Oct 19, 2015
 *
 * Description : PW Ethernet port binder
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../headercontrollers/ThaPwHeaderController.h"
#include "../ThaModulePw.h"
#include "../activator/ThaPwActivator.h"
#include "../../pwe/ThaModulePweInternal.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "ThaPwEthPortBinderInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaPwEthPortBinderMethods m_methods;

/* Override */
static tAtObjectMethods         m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool PwBandwidthIsAcceptable(ThaPwEthPortBinder self, AtPw pw, uint64 newPwBwInBps)
    {
    ThaEthPort ethPort;
    uint64 pwCurrentBw;

    AtUnused(self);

    ethPort = (ThaEthPort)AtPwEthPortGet(pw);
    if (ethPort == NULL)
        return cAtTrue;

    pwCurrentBw = ThaPwAdapterCurrentPwBandwidthInBpsGet(pw);
    return ThaEthPortProvisionBandwidthCanAdjust(ethPort, pwCurrentBw, newPwBwInBps);
    }

static eAtRet PwBandwidthUpdate(ThaPwEthPortBinder self, AtPw pw, uint64 newPwBwInBps)
    {
    eAtRet ret;
    ThaEthPort ethPort = (ThaEthPort)AtPwEthPortGet(pw);
    uint64 curBw = ThaPwAdapterCurrentPwBandwidthInBpsGet(pw);
    AtUnused(self);

    if (curBw == newPwBwInBps)
        return cAtOk;

    ThaPwAdapterCurrentPwBandwidthInBpsSet(pw, newPwBwInBps);
    if (ethPort == NULL)
        return cAtOk;

    ret = ThaEthPortProvisionedBandwidthAdjust(ethPort, curBw, newPwBwInBps);
    if ((newPwBwInBps) && ThaPwAdapterHwIsEnabled((ThaPwAdapter)pw))
        ret |= ThaEthPortRunningBandwidthAdjust(ethPort, curBw, newPwBwInBps);

    return ret;
    }

static eBool PwCanBindEthPort(ThaPwEthPortBinder self, AtPw pw, AtEthPort ethPort, uint64 bwWithPort)
    {
    AtUnused(self);
    AtUnused(pw);
    return ThaEthPortProvisionBandwidthCanAdjust((ThaEthPort)ethPort, 0, bwWithPort);
    }

static ThaModulePw ModulePw(AtPw pw)
    {
    return (ThaModulePw)AtChannelModuleGet((AtChannel)pw);
    }

static ThaPwActivator PwActivator(AtPw pw)
    {
    return ThaModulePwActivator(ModulePw(pw));
    }

static eAtRet PwEthPortBind(ThaPwEthPortBinder self, AtPw pw, AtEthPort ethPort)
    {
    eAtRet ret;

    /* Check if Ethernet port is available to add pw bandwidth */
    if (ThaPwActivatorShouldControlPwBandwidth(PwActivator(pw)))
        {
        uint64 pwBandwidth = ThaPwAdapterBandwidthCalculateWithEthPort(pw, ethPort);
        if (!ThaPwEthPortBinderPwCanBindEthPort(self, pw, ethPort, pwBandwidth))
            return cAtErrorBandwidthExceeded;

        /* Add pw bandwidth to Ethernet port first, so in binding process, PW bandwidth can be changed,
         * if binding fail, port will be unbound, and BW will be removed */
        ret = ThaEthPortProvisionedBandwidthAdjust((ThaEthPort)ethPort, 0, pwBandwidth);
        ThaPwAdapterCurrentPwBandwidthInBpsSet(pw, pwBandwidth);
        if (ret != cAtOk)
            return ret;
        }

    ThaPwAdapterEthPortSet((ThaPwAdapter)pw, ethPort);
    ret = ThaPwActivatorPwEthPortSet(PwActivator(pw), (ThaPwAdapter)pw, ethPort);
    if (ret != cAtOk)
        return ret;

    ThaModulePwEthPortIdSaveToHwStorage(ModulePw(pw), pw, (uint8)AtChannelIdGet((AtChannel)ethPort));

    if (!ThaPwEthPortBinderNeedToManageEthPortBandwidth(self))
        return ret;

    return AtChannelEnable((AtChannel)pw, ThaPwAdapterIsEnabled((ThaPwAdapter)pw));
    }

static eAtRet PwEthPortUnBind(ThaPwEthPortBinder self, AtPw pw, AtEthPort ethPort)
    {
    ThaPwAdapterEthPortSet((ThaPwAdapter)pw, NULL);
    return ThaPwEthPortBinderEthPortDisconnect(self, pw, ethPort);
    }

static eAtRet PwEnable(ThaPwEthPortBinder self, AtPw pw, eBool enable)
    {
    eAtRet ret;
    ThaEthPort ethPort = (ThaEthPort)AtPwEthPortGet(pw);
    AtUnused(self);

    /* When enable bandwidth management, PW should only be enabled when it has been bound Ethernet port successfully */
    if (ThaPwEthPortBinderNeedToManageEthPortBandwidth(self) && (ethPort == NULL))
        return cAtOk;

    /* PW is only enabled when it is bound to circuit */
    if (AtPwBoundCircuitGet(pw) == NULL)
        return cAtOk;

    ret = ThaPwAdapterHwEnable((ThaPwAdapter)pw, enable);
    if ((ret != cAtOk) || (ethPort == NULL))
        return ret;

    if (enable)
        return ThaEthPortRunningBandwidthAdjust(ethPort, 0, ThaPwAdapterCurrentPwBandwidthInBpsGet(pw));

    return ThaEthPortRunningBandwidthAdjust(ethPort, ThaPwAdapterCurrentPwBandwidthInBpsGet(pw), 0);
    }

static eBool NeedToManageEthPortBandwidth(ThaPwEthPortBinder self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint64 PwRemainingBw(ThaPwEthPortBinder self, AtPw pw)
    {
    ThaEthPort ethPort = (ThaEthPort)AtPwEthPortGet(pw);

    AtUnused(self);

    if (ethPort == NULL)
        return 0;

    return ThaEthPortPwRemainingBandwidthInBpsGet(ethPort, ThaPwAdapterCurrentPwBandwidthInBpsGet(pw));
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaPwEthPortBinder object = (ThaPwEthPortBinder)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObjectDescription(modulePw);
    }

static eBool PwIsEnabled(ThaPwEthPortBinder self, AtPw adapter)
    {
    ThaModulePwe pweModule;

    AtUnused(self);

    /* If PW don't bound to ethernet port, just get from database */
    if (AtPwEthPortGet(adapter) == NULL)
        return ThaPwAdapterIsEnabled((ThaPwAdapter)adapter);

    pweModule = (ThaModulePwe)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)adapter), cThaModulePwe);
    return ThaModulePwePwIsEnabled(pweModule, adapter);
    }

static void Override(ThaPwEthPortBinder self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void MethodsInit(ThaPwEthPortBinder self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, PwBandwidthIsAcceptable);
        mMethodOverride(m_methods, PwBandwidthUpdate);
        mMethodOverride(m_methods, PwCanBindEthPort);
        mMethodOverride(m_methods, PwEthPortBind);
        mMethodOverride(m_methods, PwEthPortUnBind);
        mMethodOverride(m_methods, PwEnable);
        mMethodOverride(m_methods, NeedToManageEthPortBandwidth);
        mMethodOverride(m_methods, PwRemainingBw);
        mMethodOverride(m_methods, PwIsEnabled);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPwEthPortBinder);
    }

ThaPwEthPortBinder ThaPwEthPortBinderObjectInit(ThaPwEthPortBinder self, ThaModulePw module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;
    self->modulePw = module;

    return self;
    }

ThaPwEthPortBinder ThaPwEthPortBinderNew(ThaModulePw module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwEthPortBinder newActivator = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newActivator == NULL)
        return NULL;

    /* Construct it */
    return ThaPwEthPortBinderObjectInit(newActivator, module);
    }

eBool ThaPwEthPortBinderPwBandwidthIsAcceptable(ThaPwEthPortBinder self, AtPw pw, uint64 newPwBandwidth)
    {
    if (self == NULL)
        return cAtFalse;

    if (!ThaPwEthPortBinderNeedToManageEthPortBandwidth(self))
        return cAtTrue;

    return mMethodsGet(self)->PwBandwidthIsAcceptable(self, pw, newPwBandwidth);
    }

eAtRet ThaPwEthPortBinderPwBandwidthUpdate(ThaPwEthPortBinder self, AtPw pw, uint64 newPwBandwidth)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    return mMethodsGet(self)->PwBandwidthUpdate(self, pw, newPwBandwidth);
    }

eBool ThaPwEthPortBinderPwCanBindEthPort(ThaPwEthPortBinder self, AtPw pw, AtEthPort ethPort, uint64 bwWithPort)
    {
    if (self == NULL)
        return cAtFalse;

    if (!ThaPwEthPortBinderNeedToManageEthPortBandwidth(self))
        return cAtTrue;

    return mMethodsGet(self)->PwCanBindEthPort(self, pw, ethPort, bwWithPort);
    }

eAtRet ThaPwEthPortBinderPwEthPortBind(ThaPwEthPortBinder self, AtPw pw, AtEthPort ethPort)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    return mMethodsGet(self)->PwEthPortBind(self, pw, ethPort);
    }

eAtRet ThaPwEthPortBinderPwEthPortUnBind(ThaPwEthPortBinder self, AtPw pw, AtEthPort currentEthPort)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    /* PW has not been bound yet */
    if (currentEthPort == NULL)
        return cAtOk;

    return mMethodsGet(self)->PwEthPortUnBind(self, pw, currentEthPort);
    }

eAtRet ThaPwEthPortBinderPwEnable(ThaPwEthPortBinder self, AtPw pw, eBool enable)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    return mMethodsGet(self)->PwEnable(self, pw, enable);
    }

uint64 ThaPwEthPortBinderPwRemainingBw(ThaPwEthPortBinder self, AtPw pw)
    {
    if (self == NULL)
        return 0;

    if (!ThaPwEthPortBinderNeedToManageEthPortBandwidth(self))
        return 0;

    return mMethodsGet(self)->PwRemainingBw(self, pw);
    }

eBool ThaPwEthPortBinderNeedToManageEthPortBandwidth(ThaPwEthPortBinder self)
    {
    if (self == NULL)
        return cAtFalse;

    if (ThaModulePwResourcesLimitationIsDisabled(self->modulePw))
        return cAtFalse;

    return mMethodsGet(self)->NeedToManageEthPortBandwidth(self);
    }

eAtRet ThaPwEthPortBinderEthPortDisconnect(ThaPwEthPortBinder self, AtPw pw, AtEthPort ethPort)
    {
    eAtRet ret;
    uint64 pwBandwidth = ThaPwAdapterCurrentPwBandwidthInBpsGet(pw);
    eBool pwIsEnabled = ThaPwAdapterHwIsEnabled((ThaPwAdapter)pw);
    AtUnused(self);

    ret = ThaPwAdapterHwEnable((ThaPwAdapter)pw, cAtFalse);
    ret |= ThaPwActivatorPwEthPortSet(PwActivator(pw), (ThaPwAdapter)pw, NULL);
    if (ret != cAtOk)
        return ret;

    ThaModulePwEthPortIdHwStorageClear(ModulePw(pw), pw);

    if (pwIsEnabled)
        ThaEthPortRunningBandwidthAdjust((ThaEthPort)ethPort, pwBandwidth, 0);

    return ThaEthPortProvisionedBandwidthAdjust((ThaEthPort)ethPort, pwBandwidth, 0);
    }

eBool ThaPwEthPortBinderPwIsEnabled(ThaPwEthPortBinder self, AtPw adapter)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    return mMethodsGet(self)->PwIsEnabled(self, adapter);
    }

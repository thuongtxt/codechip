/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : ThaPwEthPortBinderInternal.h
 * 
 * Created Date: Oct 19, 2015
 *
 * Description : PW Ethernet port binder
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPWETHPORTBINDERINTERNAL_H_
#define _THAPWETHPORTBINDERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/common/AtObjectInternal.h"
#include "ThaPwEthPortBinder.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPwEthPortBinderMethods
    {
    eBool  (*NeedToManageEthPortBandwidth)(ThaPwEthPortBinder self);
    eBool  (*PwBandwidthIsAcceptable)(ThaPwEthPortBinder self, AtPw pw, uint64 newPwBandwidth);
    eAtRet (*PwBandwidthUpdate)(ThaPwEthPortBinder self, AtPw pw, uint64 newPwBandwidth);
    eBool  (*PwCanBindEthPort)(ThaPwEthPortBinder self, AtPw pw, AtEthPort ethPort, uint64 bwWithPort);
    eAtRet (*PwEthPortBind)(ThaPwEthPortBinder self, AtPw pw, AtEthPort ethPort);
    eAtRet (*PwEthPortUnBind)(ThaPwEthPortBinder self, AtPw pw, AtEthPort ethPort);
    eAtRet (*PwEnable)(ThaPwEthPortBinder self, AtPw pw, eBool enable);
    uint64 (*PwRemainingBw)(ThaPwEthPortBinder self, AtPw pw);
    eBool (*PwIsEnabled)(ThaPwEthPortBinder self, AtPw adapter);
    }tThaPwEthPortBinderMethods;

typedef struct tThaPwEthPortBinder
    {
    tAtObject super;
    const tThaPwEthPortBinderMethods *methods;

    ThaModulePw modulePw;
    }tThaPwEthPortBinder;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPwEthPortBinder ThaPwEthPortBinderObjectInit(ThaPwEthPortBinder self, ThaModulePw module);
eAtRet ThaPwEthPortBinderEthPortDisconnect(ThaPwEthPortBinder self, AtPw pw, AtEthPort ethPort);

#ifdef __cplusplus
}
#endif
#endif /* _THAPWETHPORTBINDERINTERNAL_H_ */


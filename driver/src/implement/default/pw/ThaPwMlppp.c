/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited 
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : ThaPwMp.c
 *
 * Created Date: Oct 11, 2016 
 *
 * Description : MP PW
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "adapters/ThaPwAdapterInternal.h"
#include "ThaPwInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaPwMlppp)self)
#define mHdlcPwAdapter(self) ((ThaPwHdlcAdapter)Adapter((AtPw)self))

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtObjectMethods  m_AtObjectOverride;
static tAtChannelMethods m_AtChannelOverride;
static tAtPwMethods      m_AtPwOverride;
static tAtPwHdlcMethods  m_AtPwHdlcOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;
static const tAtPwHdlcMethods  *m_AtPwHdlcMethods  = NULL;

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPw Adapter(AtPw pw)
    {
    return (AtPw)((ThaPwMlppp)pw)->adapter;
    }

#include "ThaPwCommonSetup.h"

static eAtModulePwRet PayloadTypeSet(AtPwHdlc self, eAtPwHdlcPayloadType payloadType)
    {
    eAtRet ret;

    if (payloadType == cAtPwHdlcPayloadTypeInvalid)
        return cAtErrorModeNotSupport;

    ret = mMethodsGet(mHdlcPwAdapter(self))->PayloadTypeSet(mHdlcPwAdapter(self), payloadType);
    if (ret == cAtOk)
        mThis(self)->payloadType = payloadType;

    return ret;
    }

static eAtPwHdlcPayloadType PayloadTypeGet(AtPwHdlc self)
    {
    return mThis(self)->payloadType;
    }

static void Delete(AtObject self)
    {
    ThaPwAdapterDelete((AtPw)self);

    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static void OverrideAtObject(AtPw self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtPwHdlc(AtPw self)
    {
    AtPwHdlc hdlc = (AtPwHdlc)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPwHdlcMethods = mMethodsGet(hdlc);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPwHdlcOverride, m_AtPwHdlcMethods, sizeof(m_AtPwHdlcOverride));

        mMethodOverride(m_AtPwHdlcOverride, PayloadTypeSet);
        mMethodOverride(m_AtPwHdlcOverride, PayloadTypeGet);
        }

    mMethodsSet(hdlc, &m_AtPwHdlcOverride);
    }

static void Override(AtPw self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtPw(self);
    OverrideAtPwHdlc(self);
    }

/* To initialize object */
static AtPw ObjectInit(AtPw self, AtMpBundle MpBundle, AtModulePw module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaPwPpp));

    /* Super constructor should be called first */
    if (AtPwPppObjectInit((AtPwPpp)self, AtChannelIdGet((AtChannel)MpBundle), cAtPwTypeMlppp, module) == NULL)
        return NULL;

    /* Only initialize method structures one time */
    Override(self);
    m_methodsInit = 1;

    /* PW adapter */
    ((ThaPwMlppp)self)->adapter = ThaPwMlpppAdapterNew(self);

    return self;
    }

AtPw ThaPwMlpppNew(AtModulePw self, AtMpBundle mlpppBundle)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPw newPw = mMethodsGet(osal)->MemAlloc(osal, sizeof(tThaPwPpp));
    if (newPw == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPw, mlpppBundle, self);
    }

ThaPwAdapter ThaPwMlpppAdapterGet(AtPw self)
    {
    return (ThaPwAdapter)Adapter(self);
    }

void ThaPwMlpppAdapterDelete(AtPw self)
    {
    AtObjectDelete((AtObject)((ThaPwMlppp)self)->adapter);
    ((ThaPwMlppp)self)->adapter = NULL;
    }

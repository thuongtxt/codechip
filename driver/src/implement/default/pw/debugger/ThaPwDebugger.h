/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : ThaPwDebugger.h
 * 
 * Created Date: Sep 26, 2015
 *
 * Description : PW debugger public utility
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPWDEBUGGER_H_
#define _THAPWDEBUGGER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"
#include "AtPw.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPwDebugger * ThaPwDebugger;

typedef enum eThaPwPrbsError
    {
    cThaPwPrbsPlaNotSync = cBit0,
    cThaPwPrbsPlaError   = cBit1,

    cThaPwPrbsPweNotSync = cBit2,
    cThaPwPrbsPweError   = cBit3,

    cThaPwPrbsClaNotSync = cBit4,
    cThaPwPrbsClaError   = cBit5,

    cThaPwPrbsPdaNotSync = cBit6,
    cThaPwPrbsPdaError   = cBit7,

    cThaPwPrbsEncNotSync = cBit8,
    cThaPwPrbsEncError   = cBit9
    }eThaPwPrbsError;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPwDebugger ThaPwDebuggerNew(void);
ThaPwDebugger Tha60210031PwDebuggerNew(void);
ThaPwDebugger Tha60210021PwDebuggerNew(void);

uint32 ThaPwDebuggerPwPrbsCheck(ThaPwDebugger self, AtPw pw);
void ThaPwDebuggerPwSpeedShow(ThaPwDebugger self, AtPw pw);
void ThaPwDebuggerPwCepDebug(ThaPwDebugger self, AtPw pw);

#ifdef __cplusplus
}
#endif
#endif /* _THAPWDEBUGGER_H_ */


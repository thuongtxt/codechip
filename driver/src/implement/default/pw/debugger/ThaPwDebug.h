/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : ThaPwDebug.h
 * 
 * Created Date: Jul 29, 2016
 *
 * Description : ThaPwDebug declarations
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPWDEBUG_H_
#define _THAPWDEBUG_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModulePw.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void ThaPwPktAnalyzerCla2CdrDataFlush(AtPw self);
eAtRet ThaPwPktAnalyzerCla2CdrTrigger(AtPw self);
eBool ThaPwPktAnalyzerCla2CdrHoIsEnabled(AtPw self, uint16 packetId);
eBool ThaPwPktAnalyzerCla2CdrPacketIsError(AtPw self, uint16 packetId);
uint32 ThaPwPktAnalyzerCla2CdrRtpTimeStampGet(AtPw self, uint16 packetId);
uint32 ThaPwPktAnalyzerCla2CdrRtpTimeStampOffset(AtPw self, uint16 previousPktId, uint16 nextPktId);
uint32 ThaPwPktAnalyzerCla2CdrRtpSequenceNumberGet(AtPw self, uint16 packetId);
uint32 ThaPwPktAnalyzerCla2CdrControlWordSequenceNumberGet(AtPw self, uint16 packetId);

#ifdef __cplusplus
}
#endif
#endif /* _THAPWDEBUG_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : ThaPwDebuggerInternal.c
 * 
 * Created Date: Sep 26, 2015
 *
 * Description : PW debugger
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPWDEBUGGERINTERNAL_H_
#define _THAPWDEBUGGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/common/AtObjectInternal.h"
#include "ThaPwDebugger.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPwDebuggerMethods
    {
    uint32 (*PwPrbsCheck)(ThaPwDebugger self, AtPw pw);
    void  (*PwSpeedShow)(ThaPwDebugger self, AtPw pw);
    uint32 (*MaxNumStsPerCircuitToHavePrbsCheckFeature)(ThaPwDebugger self);

    uint32 (*PrbsCheckAtPwe)(ThaPwDebugger self, AtPw pwAdapter);
    uint32 (*PrbsCheckAtCla)(ThaPwDebugger self, AtPw pwAdapter);
    uint32 (*PrbsCheckAtPda)(ThaPwDebugger self, AtPw pwAdapter);
    uint32 (*PrbsCheckAtEnc)(ThaPwDebugger self, AtPw pwAdapter);

    void  (*PwCepDebug)(ThaPwDebugger self, AtPw pw);
    uint32 (*PwPrbsCheckDefaultOffset)(ThaPwDebugger self, AtPw pwAdapter);

    uint32 (*RegFieldIsRunningMask)(ThaPwDebugger self);
    uint32 (*RegFieldIsRunningShift)(ThaPwDebugger self);
    uint32 (*RegFieldSyncMask)(ThaPwDebugger self);
    uint32 (*RegFieldSyncShift)(ThaPwDebugger self);
    uint32 (*RegFieldErrorMask)(ThaPwDebugger self);
    uint32 (*RegFieldErrorShift)(ThaPwDebugger self);

    uint32 (*PlaSpeedBase)(ThaPwDebugger self, AtPw pw);
    uint32 (*PdaSpeedBase)(ThaPwDebugger self, AtPw pw);
    eAtRet (*PwSelect)(ThaPwDebugger self, AtPw pw);
    uint32 (*PlaPrbsCheck)(ThaPwDebugger self, AtPw pw, uint32 regVal);
    uint32 (*PdaPrbsCheck)(ThaPwDebugger self, AtPw pw, uint32 regVal);
    }tThaPwDebuggerMethods;

typedef struct tThaPwDebugger
    {
    tAtObject super;
    const tThaPwDebuggerMethods* methods;
    }tThaPwDebugger;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPwDebugger ThaPwDebuggerObjectInit(ThaPwDebugger self);

#ifdef __cplusplus
}
#endif
#endif /* _THAPWDEBUGGERINTERNAL_H_ */


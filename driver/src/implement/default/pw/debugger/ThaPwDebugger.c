/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : ThaPwDebugger.c
 *
 * Created Date: Sep 26, 2015
 *
 * Description : PW debugger
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../adapters/ThaPwAdapter.h"
#include "ThaPwDebuggerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaPrbsIsRunningMask  cBit15_0
#define cThaPrbsIsRunningShift 0
#define cThaPrbsSyncMask       cBit16
#define cThaPrbsSyncShift      16
#define cThaPrbsErrorMask      cBit17
#define cThaPrbsErrorShift     17

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaPwDebuggerMethods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 PwPrbsCheck(ThaPwDebugger self, AtPw pw)
    {
    uint32 result = 0;
    AtPw pwAdapter = (AtPw)ThaPwAdapterGet(pw);

    result |= mMethodsGet(self)->PrbsCheckAtPwe(self, pwAdapter);
    result |= mMethodsGet(self)->PrbsCheckAtCla(self, pwAdapter);
    result |= mMethodsGet(self)->PrbsCheckAtPda(self, pwAdapter);

    return result;
    }

static void PwSpeedShow(ThaPwDebugger self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    }

static uint32 PrbsCheckAtPwe(ThaPwDebugger self, AtPw pwAdapter)
    {
    AtUnused(self);
    AtUnused(pwAdapter);
    return 0;
    }

static uint32 PrbsCheckAtCla(ThaPwDebugger self, AtPw pwAdapter)
    {
    AtUnused(self);
    AtUnused(pwAdapter);
    return 0;
    }

static uint32 PrbsCheckAtPda(ThaPwDebugger self, AtPw pwAdapter)
    {
    AtUnused(self);
    AtUnused(pwAdapter);
    return 0;
    }

static uint32 PrbsCheckAtEnc(ThaPwDebugger self, AtPw pwAdapter)
    {
    AtUnused(self);
    AtUnused(pwAdapter);
    return 0;
    }

static void PwCepDebug(ThaPwDebugger self, AtPw pw)
    {
    AtUnused(self);
    ThaPwCepAdapterDebug((AtChannel)pw);
    }

static uint32 PwPrbsCheckDefaultOffset(ThaPwDebugger self, AtPw pwAdapter)
    {
    AtUnused(self);
    AtUnused(pwAdapter);
    return 0;
    }

static uint32 RegFieldIsRunningMask(ThaPwDebugger self)
    {
    AtUnused(self);
    return cThaPrbsIsRunningMask;
    }

static uint32 RegFieldIsRunningShift(ThaPwDebugger self)
    {
    AtUnused(self);
    return cThaPrbsIsRunningShift;
    }

static uint32 RegFieldSyncMask(ThaPwDebugger self)
    {
    AtUnused(self);
    return cThaPrbsSyncMask;
    }

static uint32 RegFieldSyncShift(ThaPwDebugger self)
    {
    AtUnused(self);
    return cThaPrbsSyncShift;
    }

static uint32 RegFieldErrorMask(ThaPwDebugger self)
    {
    AtUnused(self);
    return cThaPrbsErrorMask;
    }

static uint32 RegFieldErrorShift(ThaPwDebugger self)
    {
    AtUnused(self);
    return cThaPrbsErrorShift;
    }

static eBool PwPrbsCheckIsSupported(ThaPwDebugger self, AtPw pw)
    {
    if (AtPwTypeGet(pw) == cAtPwTypeCEP)
        {
        AtSdhChannel sdhChannel = (AtSdhChannel)AtPwBoundCircuitGet(pw);
        uint8 numSts = AtSdhChannelNumSts(sdhChannel);
        if (numSts > mMethodsGet(self)->MaxNumStsPerCircuitToHavePrbsCheckFeature(self))
           return cAtFalse;
        }

    return cAtTrue;
    }

static uint32 MaxNumStsPerCircuitToHavePrbsCheckFeature(ThaPwDebugger self)
    {
    /* Return max value to open this feature for all of products and let concrete
     * products determine. */
    AtUnused(self);
    return cBit31_0;
    }

static eAtRet PwSelect(ThaPwDebugger self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cAtOk;
    }

static uint32 PlaPrbsCheck(ThaPwDebugger self, AtPw pw, uint32 regVal)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(regVal);
    return 0;
    }

static uint32 PdaPrbsCheck(ThaPwDebugger self, AtPw pw, uint32 regVal)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(regVal);
    return 0;
    }

static uint32 PlaSpeedBase(ThaPwDebugger self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cInvalidUint32;
    }

static uint32 PdaSpeedBase(ThaPwDebugger self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cInvalidUint32;
    }

static void MethodsInit(ThaPwDebugger self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, PrbsCheckAtPda);
        mMethodOverride(m_methods, PrbsCheckAtCla);
        mMethodOverride(m_methods, PrbsCheckAtPwe);
        mMethodOverride(m_methods, PrbsCheckAtEnc);
        mMethodOverride(m_methods, PwPrbsCheck);
        mMethodOverride(m_methods, PwSpeedShow);
        mMethodOverride(m_methods, PwCepDebug);
        mMethodOverride(m_methods, PwPrbsCheckDefaultOffset);
        mMethodOverride(m_methods, RegFieldIsRunningMask);
        mMethodOverride(m_methods, RegFieldIsRunningShift);
        mMethodOverride(m_methods, RegFieldSyncMask);
        mMethodOverride(m_methods, RegFieldSyncShift);
        mMethodOverride(m_methods, RegFieldErrorMask);
        mMethodOverride(m_methods, RegFieldErrorShift);
        mMethodOverride(m_methods, MaxNumStsPerCircuitToHavePrbsCheckFeature);
        mMethodOverride(m_methods, PwSelect);
        mMethodOverride(m_methods, PlaPrbsCheck);
        mMethodOverride(m_methods, PdaPrbsCheck);
        mMethodOverride(m_methods, PlaSpeedBase);
        mMethodOverride(m_methods, PdaSpeedBase);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPwDebugger);
    }

ThaPwDebugger ThaPwDebuggerObjectInit(ThaPwDebugger self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwDebugger ThaPwDebuggerNew(void)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwDebugger newChecker = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newChecker == NULL)
        return NULL;

    /* Construct it */
    return ThaPwDebuggerObjectInit(newChecker);
    }

uint32 ThaPwDebuggerPwPrbsCheck(ThaPwDebugger self, AtPw pw)
    {
    if (self == NULL)
        return 0;

    if (PwPrbsCheckIsSupported(self, pw))
        return mMethodsGet(self)->PwPrbsCheck(self, pw);

    return cInvalidUint32;
    }

void ThaPwDebuggerPwSpeedShow(ThaPwDebugger self, AtPw pw)
    {
    if (self)
        mMethodsGet(self)->PwSpeedShow(self, pw);
    }

void ThaPwDebuggerPwCepDebug(ThaPwDebugger self, AtPw pw)
    {
    if (self)
        mMethodsGet(self)->PwCepDebug(self, pw);
    }

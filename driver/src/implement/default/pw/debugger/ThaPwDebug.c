/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : ThaPwDebug.c
 *
 * Created Date: Jul 29, 2016
 *
 * Description : ThaPwDebug implementations
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaPwDebug.h"
#include "AtDevice.h"
#include "../../cla/ThaModuleCla.h"
#include "../../cla/controllers/ThaClaPwController.h"
#include "../../man/ThaDevice.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleCla ClaModule(AtPw self)
    {
    return (ThaModuleCla)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cThaModuleCla);
    }

static ThaClaPwController ClaPwController(AtPw self)
    {
    return ThaModuleClaPwControllerGet(ClaModule(self));
    }

void ThaPwPktAnalyzerCla2CdrDataFlush(AtPw self)
    {
    ThaClaPwControllerPktAnalyzerCla2CdrDataFlush(ClaPwController(self), self);
    }

eAtRet ThaPwPktAnalyzerCla2CdrTrigger(AtPw self)
    {
    return ThaClaPwControllerPktAnalyzerCla2CdrTrigger(ClaPwController(self), self);
    }

eBool ThaPwPktAnalyzerCla2CdrHoIsEnabled(AtPw self, uint16 packetId)
    {
    return ThaClaPwControllerPktAnalyzerCla2CdrHoIsEnabled(ClaPwController(self), self, packetId);
    }

eBool ThaPwPktAnalyzerCla2CdrPacketIsError(AtPw self, uint16 packetId)
    {
    return ThaClaPwControllerPktAnalyzerCla2CdrPacketIsError(ClaPwController(self), self, packetId);
    }

uint32 ThaPwPktAnalyzerCla2CdrRtpTimeStampGet(AtPw self, uint16 packetId)
    {
    return ThaClaPwControllerPktAnalyzerCla2CdrRtpTimeStampGet(ClaPwController(self), self, packetId);
    }

uint32 ThaPwPktAnalyzerCla2CdrRtpTimeStampOffset(AtPw self, uint16 previousPktId, uint16 nextPktId)
    {
    return ThaClaPwControllerPktAnalyzerCla2CdrRtpTimeStampOffset(ClaPwController(self), self, previousPktId, nextPktId);
    }

uint32 ThaPwPktAnalyzerCla2CdrRtpSequenceNumberGet(AtPw self, uint16 packetId)
    {
    return ThaClaPwControllerPktAnalyzerCla2CdrRtpSequenceNumberGet(ClaPwController(self), self, packetId);
    }

uint32 ThaPwPktAnalyzerCla2CdrControlWordSequenceNumberGet(AtPw self, uint16 packetId)
    {
    return ThaClaPwControllerPktAnalyzerCla2CdrControlWordSequenceNumberGet(ClaPwController(self), self, packetId);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ThaPwHsGroup Module Name
 *
 * File        : ThaPwHsGroup.c
 *
 * Created Date: Oct 23, 2015
 *
 * Description : default implementation for PW HS group
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/pw/AtModulePwInternal.h"
#include "../../../../generic/pw/AtPwGroupInternal.h"
#include "../../cla/ThaModuleCla.h"
#include "../../cla/pw/ThaModuleClaPw.h"
#include "../../cla/hbce/ThaHbce.h"
#include "../../pwe/ThaModulePwe.h"
#include "../../man/ThaDevice.h"
#include "ThaPwHsGroup.h"
#include "ThaPwHsGroupInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPwGroupMethods m_AtPwGroupOverride;

/* Save super implementation */
static const tAtPwGroupMethods *m_AtPwGroupMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePweV2 ModulePwe(AtPwGroup self)
    {
    AtModule pwModule = (AtModule)AtPwGroupModuleGet(self);
    return (ThaModulePweV2)AtDeviceModuleGet(AtModuleDeviceGet(pwModule), cThaModulePwe);
    }

static ThaModuleClaPwV2 ModuleCla(AtPwGroup self)
    {
    AtModule pwModule = (AtModule)AtPwGroupModuleGet(self);
    return (ThaModuleClaPwV2)AtDeviceModuleGet(AtModuleDeviceGet(pwModule), cThaModuleCla);
    }

static eAtModulePwRet Enable(AtPwGroup self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorModeNotSupport;
    }

static eBool IsEnabled(AtPwGroup self)
    {
    return ThaModuleClaPwV2HsGroupIsEnabled(ModuleCla(self), self);
    }

static eAtRet HwPwAdd(AtPwGroup self, AtPw pw)
    {
    eAtRet ret = cAtOk;
    ThaPwAdapter adapter = ThaPwAdapterGet(pw);

    ret |= ThaModulePweV2HsGroupPwAdd(ModulePwe(self), self, (AtPw)ThaPwAdapterGet(pw));
    ret |= ThaModuleClaPwV2HsGroupPwAdd(ModuleCla(self), self, (AtPw)ThaPwAdapterGet(pw));

    if (ret != cAtOk)
        return ret;

    /* When add PW to HS group, need to enable backup PSN if pw is enabled */
    ret = ThaHbcePwHeaderControllerEnable(ThaPwAdapterHbceGet((AtPw)adapter), ThaPwAdapterBackupHeaderController(adapter), AtChannelIsEnabled((AtChannel)pw));

    /* If back up header controller is not configured for HBCE yet,
     * Above function will return error and it is normal in this case */
    if (ret == cAtErrorNullPointer)
        return cAtOk;

    return ret;
    }

static eAtModulePwRet PwAdd(AtPwGroup self, AtPw pw)
    {
    eAtRet ret = m_AtPwGroupMethods->PwAdd(self, pw);
    if (ret != cAtOk)
        return ret;

    return HwPwAdd(self, pw);
    }

static eAtRet HwPwRemove(AtPwGroup self, AtPw pw)
    {
    eAtRet ret = cAtOk;
    ThaPwAdapter adapter = ThaPwAdapterGet(pw);

    ret |= ThaModulePweV2HsGroupPwRemove(ModulePwe(self), self, (AtPw)ThaPwAdapterGet(pw));
    ret |= ThaModuleClaPwV2HsGroupPwRemove(ModuleCla(self), self, (AtPw)ThaPwAdapterGet(pw));

    if (ret != cAtOk)
        return ret;

    /* When remove PW out of HS group, need to disable backup PSN */
    ret = ThaHbcePwHeaderControllerEnable(ThaPwAdapterHbceGet((AtPw)adapter), ThaPwAdapterBackupHeaderController(adapter), cAtFalse);

    /* If back up header controller is not configured for HBCE yet,
     * Above function will return error and it is normal in this case */
    if (ret == cAtErrorNullPointer)
        return cAtOk;

    return ret;
    }

static eAtModulePwRet PwRemove(AtPwGroup self, AtPw pw)
    {
    eAtRet ret = m_AtPwGroupMethods->PwRemove(self, pw);
    if (ret != cAtOk)
        return ret;

    if (pw == NULL)
        return cAtOk;

    return HwPwRemove(self, pw);
    }

static eBool LabelSetIsValid(eAtPwGroupLabelSet labelSet)
    {
    if (labelSet == cAtPwGroupLabelSetPrimary)
        return cAtTrue;
    if (labelSet == cAtPwGroupLabelSetBackup)
        return cAtTrue;
    return cAtFalse;
    }

static eAtModulePwRet TxLabelSetSelect(AtPwGroup self, eAtPwGroupLabelSet labelSet)
    {
    if (!LabelSetIsValid(labelSet))
        return cAtErrorInvlParm;
    return ThaModulePweV2HsGroupLabelSetSelect(ModulePwe(self), self, labelSet);
    }

static eAtPwGroupLabelSet TxSelectedLabelGet(AtPwGroup self)
    {
    return ThaModulePweV2HsGroupSelectedLabelSetGet(ModulePwe(self), self);
    }

static eAtModulePwRet RxLabelSetSelect(AtPwGroup self, eAtPwGroupLabelSet labelSet)
    {
    if (!LabelSetIsValid(labelSet))
        return cAtErrorInvlParm;
    return ThaModuleClaPwV2HsGroupLabelSetSelect(ModuleCla(self), self, labelSet);
    }

static eAtPwGroupLabelSet RxSelectedLabelGet(AtPwGroup self)
    {
    return ThaModuleClaPwV2HsGroupSelectedLabelSetGet(ModuleCla(self), self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPwHsGroup);
    }

static void OverrideAtPwGroup(AtPwGroup self)
    {
    AtOsal osal = AtSharedDriverOsalGet();

    if (!m_methodsInit)
        {
        m_AtPwGroupMethods = mMethodsGet(self);

        mMethodsGet(osal)->MemCpy(osal, &m_AtPwGroupOverride, m_AtPwGroupMethods, sizeof(m_AtPwGroupOverride));
        mMethodOverride(m_AtPwGroupOverride, Enable);
        mMethodOverride(m_AtPwGroupOverride, IsEnabled);
        mMethodOverride(m_AtPwGroupOverride, PwAdd);
        mMethodOverride(m_AtPwGroupOverride, PwRemove);
        mMethodOverride(m_AtPwGroupOverride, TxLabelSetSelect);
        mMethodOverride(m_AtPwGroupOverride, TxSelectedLabelGet);
        mMethodOverride(m_AtPwGroupOverride, RxLabelSetSelect);
        mMethodOverride(m_AtPwGroupOverride, RxSelectedLabelGet);
        }

    mMethodsSet(self, &m_AtPwGroupOverride);
    }

static void Override(AtPwGroup self)
    {
    OverrideAtPwGroup(self);
    }

static AtPwGroup ObjectInit(AtPwGroup self, uint32 groupId, AtModulePw module)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    if (AtPwHsGroupObjectInit(self, groupId, module) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPwGroup ThaPwHsGroupNew(uint32 groupId, AtModulePw module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPwGroup newGroup = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newGroup == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newGroup, groupId, module);
    }


eAtRet ThaPwHsGroupHwPwAdd(AtPwGroup self, AtPw pw)
    {
    return (self) ? HwPwAdd(self, pw) : cAtErrorNullPointer;
    }

eAtRet ThaPwHsGroupHwPwRemove(AtPwGroup self, AtPw pw)
    {
    return (self) ? HwPwRemove(self, pw) : cAtErrorNullPointer;
    }

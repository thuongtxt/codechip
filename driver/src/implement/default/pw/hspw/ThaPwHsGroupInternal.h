/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ThaPwHsGroup module name
 * 
 * File        : ThaPwHsGroupInternal.h
 * 
 * Created Date: Oct 23, 2015
 *
 * Description : Internal structure
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _HSPW_THAPWHSGROUPINTERNAL_H_
#define _HSPW_THAPWHSGROUPINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPwHsGroup
    {
    tAtPwHsGroup super;
    }tThaPwHsGroup;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _HSPW_THAPWHSGROUPINTERNAL_H_ */


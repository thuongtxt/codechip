/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ThaPwHsGroup module name
 * 
 * File        : ThaPwHsGroup.h
 * 
 * Created Date: Oct 23, 2015
 *
 * Description : Header file for PW HW group implementation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _HSPW_THAPWHSGROUP_H_
#define _HSPW_THAPWHSGROUP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPwGroup.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPwGroup ThaPwHsGroupNew(uint32 groupId, AtModulePw module);
eAtRet ThaPwHsGroupHwPwAdd(AtPwGroup self, AtPw pw);
eAtRet ThaPwHsGroupHwPwRemove(AtPwGroup self, AtPw pw);

#ifdef __cplusplus
}
#endif
#endif /* _HSPW_THAPWHSGROUP_H_ */


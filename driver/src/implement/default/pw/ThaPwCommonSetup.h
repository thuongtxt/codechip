/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : ThaPwCommonSetup.h
 * 
 * Created Date: Mar 14, 2013
 *
 * Description : PW common class setup
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Includes ---------------------------------------*/
#include "../pda/ThaModulePda.h"
#include "../man/ThaDevice.h"
#include "../pw/ThaModulePwInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
static uint32 LopsThresholdMax(AtPw self)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->LopsThresholdMax(adapter);
    }

static uint32 LopsThresholdMin(AtPw self)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->LopsThresholdMin(adapter);
    }

static eAtModulePwRet LopsSetThresholdSet(AtPw self, uint32 numPackets)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->LopsSetThresholdSet(adapter, numPackets);
    }

static uint32 LopsSetThresholdGet(AtPw self)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->LopsSetThresholdGet(adapter);
    }

static eAtModulePwRet LopsClearThresholdSet(AtPw self, uint32 numPackets)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->LopsClearThresholdSet(adapter, numPackets);
    }

static uint32 LopsClearThresholdGet(AtPw self)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->LopsClearThresholdGet(adapter);
    }

static uint16 MinPayloadSize(AtPw self, AtChannel circuit, uint32 jitterBufferSize)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->MinPayloadSize(adapter, circuit, jitterBufferSize);
    }

static uint16 MaxPayloadSize(AtPw self, AtChannel circuit, uint32 jitterBufferSize)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->MaxPayloadSize(adapter, circuit, jitterBufferSize);
    }

static eAtModulePwRet PayloadSizeSet(AtPw self, uint16 payloadSize)
    {
    AtPw adapter = Adapter(self);

    /* Payload size not change */
    if (AtChannelShouldPreventReconfigure((AtChannel)self) && (AtPwPayloadSizeGet(self) == payloadSize))
        return cAtOk;

    return mMethodsGet(adapter)->PayloadSizeSet(adapter, payloadSize);
    }

static uint16 PayloadSizeGet(AtPw self)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->PayloadSizeGet(adapter);
    }

static eAtModulePwRet PrioritySet(AtPw self, uint8 priority)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->PrioritySet(adapter, priority);
    }

static uint8 PriorityGet(AtPw self)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->PriorityGet(adapter);
    }

static eAtModulePwRet JitterBufferSizeSet(AtPw self, uint32 microseconds)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->JitterBufferSizeSet(adapter, microseconds);
    }

static uint32 JitterBufferSizeGet(AtPw self)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->JitterBufferSizeGet(adapter);
    }

static eAtModulePwRet JitterBufferDelaySet(AtPw self, uint32 microseconds)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->JitterBufferDelaySet(adapter, microseconds);
    }

static uint32 JitterBufferDelayGet(AtPw self)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->JitterBufferDelayGet(adapter);
    }

static uint32 MinJitterBufferSize(AtPw self, AtChannel circuit, uint16 payloadSize)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->MinJitterBufferSize(adapter, circuit, payloadSize);
    }

static uint32 MaxJitterBufferSize(AtPw self, AtChannel circuit, uint16 payloadSize)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->MaxJitterBufferSize(adapter, circuit, payloadSize);
    }

static uint32 MinJitterDelay(AtPw self, AtChannel circuit, uint16 payloadSize)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->MinJitterDelay(adapter, circuit, payloadSize);
    }

static uint32 MaxJitterDelay(AtPw self, AtChannel circuit, uint16 payloadSize)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->MaxJitterDelay(adapter, circuit, payloadSize);
    }

static uint32 MaxJitterDelayForJitterBufferSize(AtPw self, uint32 jitterBufferSize_us)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->MaxJitterDelayForJitterBufferSize(adapter, jitterBufferSize_us);
    }

static eAtModulePwRet JitterBufferSizeInPacketSet(AtPw self, uint16 numPackets)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->JitterBufferSizeInPacketSet(adapter, numPackets);
    }

static uint16 JitterBufferSizeInPacketGet(AtPw self)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->JitterBufferSizeInPacketGet(adapter);
    }

static eAtModulePwRet JitterBufferDelayInPacketSet(AtPw self, uint16 numPackets)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->JitterBufferDelayInPacketSet(adapter, numPackets);
    }

static uint16 JitterBufferDelayInPacketGet(AtPw self)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->JitterBufferDelayInPacketGet(adapter);
    }

static eAtModulePwRet JitterBufferAndPayloadSizeSet(AtPw self, uint32 jitterBufferSize_us, uint32 jitterDelay_us, uint16 payloadSize)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->JitterBufferAndPayloadSizeSet(adapter, jitterBufferSize_us, jitterDelay_us, payloadSize);
    }

static eAtModulePwRet JitterBufferInPacketAndPayloadSizeSet(AtPw self, uint32 jitterBufferSize_pkt, uint32 jitterDelay_pkt, uint16 payloadSize)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->JitterBufferInPacketAndPayloadSizeSet(adapter, jitterBufferSize_pkt, jitterDelay_pkt, payloadSize);
    }

static uint32 NumCurrentPacketsInJitterBuffer(AtPw self)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->NumCurrentPacketsInJitterBuffer(adapter);
    }

static uint32 NumCurrentAdditionalBytesInJitterBuffer(AtPw self)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->NumCurrentAdditionalBytesInJitterBuffer(adapter);
    }

static eAtRet JitterBufferCenter(AtPw self)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->JitterBufferCenter(adapter);
    }

static uint32 JitterBufferWatermarkMinPackets(AtPw self)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->JitterBufferWatermarkMinPackets(adapter);
    }

static uint32 JitterBufferWatermarkMaxPackets(AtPw self)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->JitterBufferWatermarkMaxPackets(adapter);
    }

static eBool JitterBufferWatermarkIsSupported(AtPw self)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->JitterBufferWatermarkIsSupported(adapter);
    }

static eAtRet JitterBufferWatermarkReset(AtPw self)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->JitterBufferWatermarkReset(adapter);
    }

static uint16 MinJitterBufferDelayInPacketGet(AtPw self, AtChannel circuit, uint16 payloadSize)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->MinJitterBufferDelayInPacketGet(adapter, circuit, payloadSize);
    }

static uint16 MinJitterBufferSizeInPacketGet(AtPw self, AtChannel circuit, uint16 payloadSize)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->MinJitterBufferSizeInPacketGet(adapter, circuit, payloadSize);
    }

static eAtModulePwRet RtpEnable(AtPw self, eBool enable)
    {
    AtPw adapter = Adapter(self);
    /* RTP enable can not check here */
    return mMethodsGet(adapter)->RtpEnable(adapter, enable);
    }

static eBool RtpIsEnabled(AtPw self)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->RtpIsEnabled(adapter);
    }

static eAtModulePwRet RtpTxPayloadTypeSet(AtPw self, uint8 payloadType)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->RtpTxPayloadTypeSet(adapter, payloadType);
    }

static uint8 RtpTxPayloadTypeGet(AtPw self)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->RtpTxPayloadTypeGet(adapter);
    }

static eAtModulePwRet RtpExpectedPayloadTypeSet(AtPw self, uint8 payloadType)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->RtpExpectedPayloadTypeSet(adapter, payloadType);
    }

static uint8 RtpExpectedPayloadTypeGet(AtPw self)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->RtpExpectedPayloadTypeGet(adapter);
    }

static eAtModulePwRet RtpPayloadTypeCompare(AtPw self, eBool enableCompare)
	{
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->RtpPayloadTypeCompare(adapter, enableCompare);
	}

static eBool RtpPayloadTypeIsCompared(AtPw self)
	{
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->RtpPayloadTypeIsCompared(adapter);
    }

static eAtModulePwRet RtpTxSsrcSet(AtPw self, uint32 ssrc)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->RtpTxSsrcSet(adapter, ssrc);
    }

static uint32 RtpTxSsrcGet(AtPw self)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->RtpTxSsrcGet(adapter);
    }

static eAtModulePwRet RtpExpectedSsrcSet(AtPw self, uint32 ssrc)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->RtpExpectedSsrcSet(adapter, ssrc);
    }

static uint32 RtpExpectedSsrcGet(AtPw self)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->RtpExpectedSsrcGet(adapter);
    }

static eAtModulePwRet RtpSsrcCompare(AtPw self, eBool enableCompare)
	{
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->RtpSsrcCompare(adapter, enableCompare);
	}

static eBool RtpSsrcIsCompared(AtPw self)
	{
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->RtpSsrcIsCompared(adapter);
	}

static eAtModulePwRet RtpTimeStampModeSet(AtPw self, eAtPwRtpTimeStampMode timeStampMode)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->RtpTimeStampModeSet(adapter, timeStampMode);
    }

static eAtPwRtpTimeStampMode RtpTimeStampModeGet(AtPw self)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->RtpTimeStampModeGet(adapter);
    }

static eBool RtpTimeStampModeIsSupported(AtPw self, eAtPwRtpTimeStampMode timeStampMode)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->RtpTimeStampModeIsSupported(adapter, timeStampMode);
    }

static eAtModulePwRet ReorderingEnable(AtPw self, eBool enable)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->ReorderingEnable(adapter, enable);
    }

static eBool ReorderingIsEnabled(AtPw self)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->ReorderingIsEnabled(adapter);
    }

static eAtModulePwRet SuppressEnable(AtPw self, eBool enable)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->SuppressEnable(adapter, enable);
    }

static eBool SuppressIsEnabled(AtPw self)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->SuppressIsEnabled(adapter);
    }

static eAtModulePwRet CwEnable(AtPw self, eBool enable)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->CwEnable(adapter, enable);
    }

static eBool CwIsEnabled(AtPw self)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->CwIsEnabled(adapter);
    }

static eAtModulePwRet CwAutoTxLBitEnable(AtPw self, eBool enable)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->CwAutoTxLBitEnable(adapter, enable);
    }

static eBool CwAutoTxLBitIsEnabled(AtPw self)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->CwAutoTxLBitIsEnabled(adapter);
    }

static eAtModulePwRet CwAutoRxLBitEnable(AtPw self, eBool enable)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->CwAutoRxLBitEnable(adapter, enable);
    }

static eBool CwAutoRxLBitIsEnabled(AtPw self)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->CwAutoRxLBitIsEnabled(adapter);
    }

static eBool CwAutoRxLBitCanEnable(AtPw self, eBool enable)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->CwAutoRxLBitCanEnable(adapter, enable);
    }

static eAtModulePwRet CwAutoRBitEnable(AtPw self, eBool enable)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->CwAutoRBitEnable(adapter, enable);
    }

static eBool CwAutoRBitIsEnabled(AtPw self)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->CwAutoRBitIsEnabled(adapter);
    }

static eAtModulePwRet CwSequenceModeSet(AtPw self, eAtPwCwSequenceMode sequenceMode)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->CwSequenceModeSet(adapter, sequenceMode);
    }

static eAtPwCwSequenceMode CwSequenceModeGet(AtPw self)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->CwSequenceModeGet(adapter);
    }

static eBool CwSequenceModeIsSupported(AtPw self, eAtPwCwSequenceMode sequenceMode)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->CwSequenceModeIsSupported(adapter, sequenceMode);
    }

static eAtModulePwRet CwLengthModeSet(AtPw self, eAtPwCwLengthMode lengthMode)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->CwLengthModeSet(adapter, lengthMode);
    }

static eAtPwCwLengthMode CwLengthModeGet(AtPw self)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->CwLengthModeGet(adapter);
    }

static eBool CwLengthModeIsSupported(AtPw self, eAtPwCwLengthMode lengthMode)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->CwLengthModeIsSupported(adapter, lengthMode);
    }

static eAtModulePwRet CwPktReplaceModeSet(AtPw self, eAtPwPktReplaceMode pktReplaceMode)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->CwPktReplaceModeSet(adapter, pktReplaceMode);
    }

static eAtPwPktReplaceMode CwPktReplaceModeGet(AtPw self)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->CwPktReplaceModeGet(adapter);
    }

static eAtModulePwRet LopsPktReplaceModeSet(AtPw self, eAtPwPktReplaceMode pktReplaceMode)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->LopsPktReplaceModeSet(adapter, pktReplaceMode);
    }

static eAtPwPktReplaceMode LopsPktReplaceModeGet(AtPw self)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->LopsPktReplaceModeGet(adapter);
    }

static eBool CanControlLopsPktReplaceMode(AtPw self)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->CanControlLopsPktReplaceMode(adapter);
    }

static eBool PktReplaceModeIsSupported(AtPw self, eAtPwPktReplaceMode pktReplaceMode)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->PktReplaceModeIsSupported(adapter, pktReplaceMode);
    }

static eAtModulePwRet PsnSet(AtPw self, AtPwPsn psn)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->PsnSet(adapter, psn);
    }

static AtPwPsn PsnGet(AtPw self)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->PsnGet(adapter);
    }

static eAtModulePwRet BackupPsnSet(AtPw self, AtPwPsn psn)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->BackupPsnSet(adapter, psn);
    }

static AtPwPsn BackupPsnGet(AtPw self)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->BackupPsnGet(adapter);
    }

static eAtModulePwRet EthPortSet(AtPw self, AtEthPort ethPort)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->EthPortSet(adapter, ethPort);
    }

static AtEthPort EthPortGet(AtPw self)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->EthPortGet(adapter);
    }

static eAtModulePwRet EthFlowSet(AtPw self, AtEthFlow ethFlow)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->EthFlowSet(adapter, ethFlow);
    }

static AtEthFlow EthFlowGet(AtPw self)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->EthFlowGet(adapter);
    }

static eAtModulePwRet EthHeaderSet(AtPw self, uint8 *destMac, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->EthHeaderSet(adapter, destMac, cVlan, sVlan);
    }

static eAtModulePwRet BackupEthHeaderSet(AtPw self, uint8 *destMac, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->BackupEthHeaderSet(adapter, destMac, cVlan, sVlan);
    }

static eAtModulePwRet EthDestMacSet(AtPw self, uint8 *destMac)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->EthDestMacSet(adapter, destMac);
    }

static eAtModulePwRet BackupEthDestMacSet(AtPw self, uint8 *destMac)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->BackupEthDestMacSet(adapter, destMac);
    }

static eAtModulePwRet EthSrcMacSet(AtPw self, uint8 *srcMac)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->EthSrcMacSet(adapter, srcMac);
    }

static eAtModulePwRet EthCVlanTpidSet(AtPw self, uint16 tpid)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->EthCVlanTpidSet(adapter, tpid);
    }

static eAtModulePwRet EthSVlanTpidSet(AtPw self, uint16 tpid)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->EthSVlanTpidSet(adapter, tpid);
    }

static eAtModulePwRet BackupEthSrcMacSet(AtPw self, uint8 *srcMac)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->BackupEthSrcMacSet(adapter, srcMac);
    }

static eAtModulePwRet EthSrcMacGet(AtPw self, uint8 *srcMac)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->EthSrcMacGet(adapter, srcMac);
    }

static eAtModulePwRet BackupEthSrcMacGet(AtPw self, uint8 *srcMac)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->BackupEthSrcMacGet(adapter, srcMac);
    }

static eAtModulePwRet EthDestMacGet(AtPw self, uint8 *destMac)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->EthDestMacGet(adapter, destMac);
    }

static eAtModulePwRet BackupEthDestMacGet(AtPw self, uint8 *destMac)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->BackupEthDestMacGet(adapter, destMac);
    }

static eAtModulePwRet EthVlanSet(AtPw self, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->EthVlanSet(adapter, cVlan, sVlan);
    }

static eAtModulePwRet BackupEthVlanSet(AtPw self, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->BackupEthVlanSet(adapter, cVlan, sVlan);
    }

static tAtEthVlanTag* EthCVlanGet(AtPw self, tAtEthVlanTag *cVlan)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->EthCVlanGet(adapter, cVlan);
    }

static uint16 EthCVlanTpidGet(AtPw self)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->EthCVlanTpidGet(adapter);
    }

static uint16 EthSVlanTpidGet(AtPw self)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->EthSVlanTpidGet(adapter);
    }

static tAtEthVlanTag* BackupEthCVlanGet(AtPw self, tAtEthVlanTag *cVlan)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->BackupEthCVlanGet(adapter, cVlan);
    }

static tAtEthVlanTag* EthSVlanGet(AtPw self, tAtEthVlanTag *sVlan)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->EthSVlanGet(adapter, sVlan);
    }

static tAtEthVlanTag* BackupEthSVlanGet(AtPw self, tAtEthVlanTag *sVlan)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->BackupEthSVlanGet(adapter, sVlan);
    }

static eAtModulePwRet EthExpectedCVlanSet(AtPw self, tAtEthVlanTag *cVlan)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->EthExpectedCVlanSet(adapter, cVlan);
    }

static tAtEthVlanTag *EthExpectedCVlanGet(AtPw self, tAtEthVlanTag *cVlan)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->EthExpectedCVlanGet(adapter, cVlan);
    }

static eAtModulePwRet EthExpectedSVlanSet(AtPw self, tAtEthVlanTag *sVlan)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->EthExpectedSVlanSet(adapter, sVlan);
    }

static tAtEthVlanTag *EthExpectedSVlanGet(AtPw self, tAtEthVlanTag *sVlan)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->EthExpectedSVlanGet(adapter, sVlan);
    }

static eAtModulePwRet CircuitBind(AtPw self, AtChannel circuit)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->CircuitBind(adapter, circuit);
    }

static AtChannel BoundCircuitGet(AtPw self)
    {
    AtPw adapter = Adapter(self);
    if (adapter)
        return mMethodsGet(adapter)->BoundCircuitGet(adapter);

    return NULL;
    }

static eAtModulePwRet CircuitUnbind(AtPw self)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->CircuitUnbind(adapter);
    }

static void InterruptProcess(AtPw self, uint32 hwPw, AtHal hal)
    {
    AtPw adapter = Adapter(self);
    mMethodsGet(adapter)->InterruptProcess(adapter, hwPw, hal);
    }

static eAtRet Debug(AtChannel self)
    {

    if (ThaModulePwNeedClearDebug((ThaModulePw)AtChannelModuleGet(self)))
        AtChannelDebug((AtChannel)Adapter((AtPw)self));
    return cAtOk;
    }

static eAtRet Enable(AtChannel self, eBool enable)
    {
    return AtChannelEnable((AtChannel)Adapter((AtPw)self), enable);
    }

static eBool IsEnabled(AtChannel self)
    {
    return AtChannelIsEnabled((AtChannel)Adapter((AtPw)self));
    }

static eBool HasCounters(AtChannel self)
    {
    ThaModulePw modulePw = (ThaModulePw)AtChannelModuleGet(self);
    uint32 numPwsSupportCounters = mMethodsGet(modulePw)->NumPwsSupportCounters(modulePw);
    return (eBool)((AtChannelIdGet(self) < numPwsSupportCounters) ? cAtTrue : cAtFalse);
    }

static eAtRet AllCountersGet(AtChannel self, void *pAllCounters)
    {
    AtPw adapter = Adapter((AtPw)self);

    if (HasCounters(self))
        return mMethodsGet((AtChannel)adapter)->AllCountersGet((AtChannel)adapter, pAllCounters);

    return cAtErrorModeNotSupport;
    }

static eBool CounterIsSupported(AtChannel self, uint16 counterType)
    {
    if (counterType == cAtPwTdmCounterTypeRxPacketsSentToTdm)
        {
        AtDevice device = AtChannelDeviceGet((AtChannel)self);
        ThaModulePda pdaModule = (ThaModulePda)AtDeviceModuleGet(device, cThaModulePda);
        return ThaModulePdaPwRxPacketsSentToTdmCounterIsSupported(pdaModule);
        }

    if (counterType == cAtPwCounterTypeRxDuplicatedPackets)
        {
        AtDevice device = AtChannelDeviceGet((AtChannel)self);
        ThaModuleCla claModule = (ThaModuleCla)AtDeviceModuleGet(device, cThaModuleCla);
        return ThaModuleClaPwRxDuplicatedPacketsIsSupported(claModule);
        }

    return cAtTrue;
    }

static eAtRet AllCountersClear(AtChannel self, void *pAllCounters)
    {
    AtPw adapter = Adapter((AtPw)self);

    if (HasCounters(self))
        return mMethodsGet((AtChannel)adapter)->AllCountersClear((AtChannel)adapter, pAllCounters);

    return cAtErrorModeNotSupport;
    }

static uint32 DefectGet(AtChannel self)
    {
    AtPw adapter = Adapter((AtPw)self);
    return mMethodsGet((AtChannel)adapter)->DefectGet((AtChannel)adapter);
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    AtPw adapter = Adapter((AtPw)self);
    return mMethodsGet((AtChannel)adapter)->DefectHistoryGet((AtChannel)adapter);
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    AtPw adapter = Adapter((AtPw)self);
    return mMethodsGet((AtChannel)adapter)->DefectHistoryClear((AtChannel)adapter);
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
    AtPw adapter = Adapter((AtPw)self);
    return mMethodsGet((AtChannel)adapter)->SupportedInterruptMasks((AtChannel)adapter);
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    AtPw adapter = Adapter((AtPw)self);
    return mMethodsGet((AtChannel)adapter)->InterruptMaskSet((AtChannel)adapter, defectMask, enableMask);
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    AtPw adapter = Adapter((AtPw)self);
    return mMethodsGet((AtChannel)adapter)->InterruptMaskGet((AtChannel)adapter);
    }

static eAtRet InterruptEnable(AtChannel self)
    {
    AtPw adapter = Adapter((AtPw)self);
    return mMethodsGet((AtChannel)adapter)->InterruptEnable((AtChannel)adapter);
    }

static eAtRet InterruptDisable(AtChannel self)
    {
    AtPw adapter = Adapter((AtPw)self);
    return mMethodsGet((AtChannel)adapter)->InterruptDisable((AtChannel)adapter);
    }

static eBool InterruptIsEnabled(AtChannel self)
    {
    AtPw adapter = Adapter((AtPw)self);
    return mMethodsGet((AtChannel)adapter)->InterruptIsEnabled((AtChannel)adapter);
    }

static eBool AlarmIsSupported(AtChannel self, uint32 alarmType)
    {
    AtPw adapter = Adapter((AtPw)self);
    return mMethodsGet((AtChannel)adapter)->AlarmIsSupported((AtChannel)adapter, alarmType);
    }

static uint32 HwIdGet(AtChannel self)
    {
    AtPw adapter = Adapter((AtPw)self);
    return mMethodsGet((AtChannel)adapter)->HwIdGet((AtChannel)adapter);
    }

static eAtRet TxAlarmForce(AtChannel self, uint32 alarmType)
    {
    AtPw adapter = Adapter((AtPw)self);
    return mMethodsGet((AtChannel)adapter)->TxAlarmForce((AtChannel)adapter, alarmType);
    }

static eAtRet TxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    AtPw adapter = Adapter((AtPw)self);
    return mMethodsGet((AtChannel)adapter)->TxAlarmUnForce((AtChannel)adapter, alarmType);
    }

static uint32 TxForcedAlarmGet(AtChannel self)
    {
    AtPw adapter = Adapter((AtPw)self);
    return mMethodsGet((AtChannel)adapter)->TxForcedAlarmGet((AtChannel)adapter);
    }

static eAtModulePwRet IdleCodeSet(AtPw self, uint8 idleCode)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->IdleCodeSet(adapter, idleCode);
    }

static uint8  IdleCodeGet(AtPw self)
    {
    AtPw adapter = Adapter(self);
    return mMethodsGet(adapter)->IdleCodeGet(adapter);
    }

static uint32 TxForcibleAlarmsGet(AtChannel self)
    {
    AtPw adapter = Adapter((AtPw)self);
    return AtChannelTxForcableAlarmsGet((AtChannel)adapter);
    }

static eAtModulePwRet MissingPacketDefectThresholdSet(AtPw self, uint32 numPackets)
    {
    return AtPwMissingPacketDefectThresholdSet(Adapter(self), numPackets);
    }

static uint32 MissingPacketDefectThresholdGet(AtPw self)
    {
    return AtPwMissingPacketDefectThresholdGet(Adapter(self));
    }

static eAtModulePwRet ExcessivePacketLossRateDefectThresholdSet(AtPw self, uint32 numPackets, uint32 timeInSecond)
    {
    return AtPwExcessivePacketLossRateDefectThresholdSet(Adapter(self), numPackets, timeInSecond);
    }

static uint32 ExcessivePacketLossRateDefectThresholdGet(AtPw self, uint32* timeInSecond)
    {
    return AtPwExcessivePacketLossRateDefectThresholdGet(Adapter(self), timeInSecond);
    }

static eAtModulePwRet StrayPacketDefectThresholdSet(AtPw self, uint32 numPackets)
    {
    return AtPwStrayPacketDefectThresholdSet(Adapter(self), numPackets);
    }

static uint32 StrayPacketDefectThresholdGet(AtPw self)
    {
    return AtPwStrayPacketDefectThresholdGet(Adapter(self));
    }

static eAtModulePwRet MalformedPacketDefectThresholdSet(AtPw self, uint32 numPackets)
    {
    return AtPwMalformedPacketDefectThresholdSet(Adapter(self), numPackets);
    }

static uint32 MalformedPacketDefectThresholdGet(AtPw self)
    {
    return AtPwMalformedPacketDefectThresholdGet(Adapter(self));
    }

static eAtModulePwRet RemotePacketLossDefectThresholdSet(AtPw self, uint32 numPackets)
    {
    return AtPwRemotePacketLossDefectThresholdSet(Adapter(self), numPackets);
    }

static uint32 RemotePacketLossDefectThresholdGet(AtPw self)
    {
    return AtPwRemotePacketLossDefectThresholdGet(Adapter(self));
    }

static eAtModulePwRet JitterBufferOverrunDefectThresholdSet(AtPw self, uint32 numOverrunEvent)
    {
    return AtPwJitterBufferOverrunDefectThresholdSet(Adapter(self), numOverrunEvent);
    }

static uint32 JitterBufferOverrunDefectThresholdGet(AtPw self)
    {
    return AtPwJitterBufferOverrunDefectThresholdGet(Adapter(self));
    }

static eAtModulePwRet JitterBufferUnderrunDefectThresholdSet(AtPw self, uint32 numUnderrunEvent)
    {
    return AtPwJitterBufferUnderrunDefectThresholdSet(Adapter(self), numUnderrunEvent);
    }

static uint32 JitterBufferUnderrunDefectThresholdGet(AtPw self)
    {
    return AtPwJitterBufferUnderrunDefectThresholdGet(Adapter(self));
    }

static eAtModulePwRet MisConnectionDefectThresholdSet(AtPw self, uint32 numPackets)
    {
    return AtPwMisConnectionDefectThresholdSet(Adapter(self), numPackets);
    }

static uint32 MisConnectionDefectThresholdGet(AtPw self)
    {
    return AtPwMisConnectionDefectThresholdGet(Adapter(self));
    }

static eAtRet WarmRestore(AtChannel self)
    {
    return AtChannelWarmRestore((AtChannel)Adapter((AtPw)self));
    }

static AtSurEngine SurEngineGet(AtChannel self)
    {
    AtPw adapter = Adapter((AtPw)self);
    return mMethodsGet((AtChannel)adapter)->SurEngineGet((AtChannel)adapter);
    }

static eBool LopsClearThresholdIsSupported(AtPw self)
    {
    return AtPwLopsClearThresholdIsSupported(Adapter(self));
    }

static void CachePayloadSizeSet(AtPw self, uint16 sizeInBytes)
    {
    AtPw adapter = Adapter((AtPw)self);
    mMethodsGet(adapter)->CachePayloadSizeSet(adapter, sizeInBytes);
    }

static uint16 CachePayloadSizeGet(AtPw self)
    {
    AtPw adapter = Adapter((AtPw)self);
    return mMethodsGet(adapter)->CachePayloadSizeGet(adapter);
    }

static eAtRet EthPortQueueSet(AtPw self, uint8 queueId)
    {
    AtPw adapter = Adapter((AtPw)self);
    return mMethodsGet(adapter)->EthPortQueueSet(adapter, queueId);
    }

static uint8 EthPortQueueGet(AtPw self)
    {
    AtPw adapter = Adapter((AtPw)self);
    return mMethodsGet(adapter)->EthPortQueueGet(adapter);
    }

static eAtRet PtchInsertionModeSet(AtPw self, eAtPtchMode insertMode)
    {
    AtPw adapter = Adapter((AtPw)self);
    return mMethodsGet(adapter)->PtchInsertionModeSet(adapter, insertMode);
    }

static eAtPtchMode PtchInsertionModeGet(AtPw self)
    {
    AtPw adapter = Adapter((AtPw)self);
    return mMethodsGet(adapter)->PtchInsertionModeGet(adapter);
    }

static eAtRet PtchServiceEnable(AtPw self, eBool enable)
    {
    AtPw adapter = Adapter((AtPw)self);
    return mMethodsGet(adapter)->PtchServiceEnable(adapter, enable);
    }

static eBool PtchServiceIsEnabled(AtPw self)
    {
    AtPw adapter = Adapter((AtPw)self);
    return mMethodsGet(adapter)->PtchServiceIsEnabled(adapter);
    }

static eAtModulePwRet TxActiveForce(AtPw self, eBool active)
    {
    AtPw adapter = Adapter((AtPw)self);
    return mMethodsGet(adapter)->TxActiveForce(adapter, active);
    }

static eBool TxActiveIsForced(AtPw self)
    {
    AtPw adapter = Adapter((AtPw)self);
    return mMethodsGet(adapter)->TxActiveIsForced(adapter);
    }

static eAtRet SubPortVlanIndexSet(AtPw self, uint32 subPortVlanIndex)
    {
    AtPw adapter = Adapter((AtPw)self);
    return mMethodsGet(adapter)->SubPortVlanIndexSet(adapter, subPortVlanIndex);
    }

static uint32 SubPortVlanIndexGet(AtPw self)
    {
    AtPw adapter = Adapter((AtPw)self);
    return mMethodsGet(adapter)->SubPortVlanIndexGet(adapter);
    }

static uint8 EthPortMaxNumQueues(AtPw self)
    {
    AtPw adapter = Adapter((AtPw)self);
    return mMethodsGet(adapter)->EthPortMaxNumQueues(adapter);
    }

static void DebuggerEntriesFill(AtChannel self, AtDebugger debugger)
    {
    AtPw adapter = Adapter((AtPw)self);
    m_AtChannelMethods->DebuggerEntriesFill(self, debugger);
    AtChannelDebuggerEntriesFill((AtChannel)adapter, debugger);
    }

static uint32 JitterBufferNumBlocksGet(AtPw self)
    {
    AtPw adapter = Adapter((AtPw)self);
    return mMethodsGet(adapter)->JitterBufferNumBlocksGet(adapter);
    }

static void OverrideAtChannel(AtPw self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, Debug);
        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, IsEnabled);
        mMethodOverride(m_AtChannelOverride, AllCountersGet);
        mMethodOverride(m_AtChannelOverride, CounterIsSupported);
        mMethodOverride(m_AtChannelOverride, AllCountersClear);
        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, HwIdGet);
        mMethodOverride(m_AtChannelOverride, TxAlarmForce);
        mMethodOverride(m_AtChannelOverride, TxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcedAlarmGet);
        mMethodOverride(m_AtChannelOverride, SurEngineGet);
        mMethodOverride(m_AtChannelOverride, TxForcibleAlarmsGet);
        mMethodOverride(m_AtChannelOverride, WarmRestore);
        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, InterruptEnable);
        mMethodOverride(m_AtChannelOverride, InterruptDisable);
        mMethodOverride(m_AtChannelOverride, InterruptIsEnabled);
        mMethodOverride(m_AtChannelOverride, AlarmIsSupported);
        mMethodOverride(m_AtChannelOverride, DebuggerEntriesFill);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtPw(AtPw self)
    {
    AtPw pw = (AtPw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPwOverride, mMethodsGet(pw), sizeof(m_AtPwOverride));

        mMethodOverride(m_AtPwOverride, MinPayloadSize);
        mMethodOverride(m_AtPwOverride, MaxPayloadSize);
        mMethodOverride(m_AtPwOverride, PayloadSizeGet);
        mMethodOverride(m_AtPwOverride, PayloadSizeSet);
        mMethodOverride(m_AtPwOverride, LopsThresholdMax);
        mMethodOverride(m_AtPwOverride, LopsThresholdMin);
        mMethodOverride(m_AtPwOverride, LopsSetThresholdSet);
        mMethodOverride(m_AtPwOverride, LopsSetThresholdGet);
        mMethodOverride(m_AtPwOverride, LopsClearThresholdSet);
        mMethodOverride(m_AtPwOverride, LopsClearThresholdGet);
        mMethodOverride(m_AtPwOverride, PrioritySet);
        mMethodOverride(m_AtPwOverride, PriorityGet);
        mMethodOverride(m_AtPwOverride, ReorderingEnable);
        mMethodOverride(m_AtPwOverride, ReorderingIsEnabled);
        mMethodOverride(m_AtPwOverride, JitterBufferSizeSet);
        mMethodOverride(m_AtPwOverride, JitterBufferSizeGet);
        mMethodOverride(m_AtPwOverride, MinJitterBufferSize);
        mMethodOverride(m_AtPwOverride, MaxJitterBufferSize);
        mMethodOverride(m_AtPwOverride, JitterBufferDelaySet);
        mMethodOverride(m_AtPwOverride, JitterBufferDelayGet);
        mMethodOverride(m_AtPwOverride, MinJitterDelay);
        mMethodOverride(m_AtPwOverride, MaxJitterDelay);
        mMethodOverride(m_AtPwOverride, MaxJitterDelayForJitterBufferSize);
        mMethodOverride(m_AtPwOverride, NumCurrentPacketsInJitterBuffer);
        mMethodOverride(m_AtPwOverride, NumCurrentAdditionalBytesInJitterBuffer);
        mMethodOverride(m_AtPwOverride, JitterBufferCenter);
        mMethodOverride(m_AtPwOverride, JitterBufferWatermarkMinPackets);
        mMethodOverride(m_AtPwOverride, JitterBufferWatermarkMaxPackets);
        mMethodOverride(m_AtPwOverride, JitterBufferWatermarkIsSupported);
        mMethodOverride(m_AtPwOverride, JitterBufferWatermarkReset);
        mMethodOverride(m_AtPwOverride, RtpEnable);
        mMethodOverride(m_AtPwOverride, RtpIsEnabled);
        mMethodOverride(m_AtPwOverride, RtpTxSsrcSet);
        mMethodOverride(m_AtPwOverride, RtpTxSsrcGet);
        mMethodOverride(m_AtPwOverride, RtpExpectedSsrcSet);
        mMethodOverride(m_AtPwOverride, RtpExpectedSsrcGet);
        mMethodOverride(m_AtPwOverride, RtpSsrcCompare);
        mMethodOverride(m_AtPwOverride, RtpSsrcIsCompared);
        mMethodOverride(m_AtPwOverride, RtpTxPayloadTypeSet);
        mMethodOverride(m_AtPwOverride, RtpTxPayloadTypeGet);
        mMethodOverride(m_AtPwOverride, RtpExpectedPayloadTypeSet);
        mMethodOverride(m_AtPwOverride, RtpExpectedPayloadTypeGet);
        mMethodOverride(m_AtPwOverride, RtpPayloadTypeCompare);
        mMethodOverride(m_AtPwOverride, RtpPayloadTypeIsCompared);
        mMethodOverride(m_AtPwOverride, RtpTimeStampModeSet);
        mMethodOverride(m_AtPwOverride, RtpTimeStampModeGet);
        mMethodOverride(m_AtPwOverride, RtpTimeStampModeIsSupported);
        mMethodOverride(m_AtPwOverride, CwEnable);
        mMethodOverride(m_AtPwOverride, CwIsEnabled);
        mMethodOverride(m_AtPwOverride, CwAutoTxLBitEnable);
        mMethodOverride(m_AtPwOverride, CwAutoTxLBitIsEnabled);
        mMethodOverride(m_AtPwOverride, CwAutoRxLBitEnable);
        mMethodOverride(m_AtPwOverride, CwAutoRxLBitIsEnabled);
        mMethodOverride(m_AtPwOverride, CwAutoRxLBitCanEnable);
        mMethodOverride(m_AtPwOverride, CwAutoRBitEnable);
        mMethodOverride(m_AtPwOverride, CwAutoRBitIsEnabled);
        mMethodOverride(m_AtPwOverride, CwSequenceModeSet);
        mMethodOverride(m_AtPwOverride, CwSequenceModeGet);
        mMethodOverride(m_AtPwOverride, CwSequenceModeIsSupported);
        mMethodOverride(m_AtPwOverride, CwLengthModeSet);
        mMethodOverride(m_AtPwOverride, CwLengthModeGet);
        mMethodOverride(m_AtPwOverride, CwLengthModeIsSupported);
        mMethodOverride(m_AtPwOverride, CwPktReplaceModeSet);
        mMethodOverride(m_AtPwOverride, CwPktReplaceModeGet);
        mMethodOverride(m_AtPwOverride, LopsPktReplaceModeSet);
        mMethodOverride(m_AtPwOverride, LopsPktReplaceModeGet);
        mMethodOverride(m_AtPwOverride, CanControlLopsPktReplaceMode);
        mMethodOverride(m_AtPwOverride, PktReplaceModeIsSupported);
        mMethodOverride(m_AtPwOverride, PsnSet);
        mMethodOverride(m_AtPwOverride, PsnGet);
        mMethodOverride(m_AtPwOverride, EthPortSet);
        mMethodOverride(m_AtPwOverride, EthPortGet);
        mMethodOverride(m_AtPwOverride, EthFlowSet);
        mMethodOverride(m_AtPwOverride, EthFlowGet);
        mMethodOverride(m_AtPwOverride, EthHeaderSet);
        mMethodOverride(m_AtPwOverride, EthDestMacSet);
        mMethodOverride(m_AtPwOverride, EthSrcMacSet);
        mMethodOverride(m_AtPwOverride, EthDestMacGet);
        mMethodOverride(m_AtPwOverride, EthSrcMacGet);
        mMethodOverride(m_AtPwOverride, EthVlanSet);
        mMethodOverride(m_AtPwOverride, EthCVlanGet);
        mMethodOverride(m_AtPwOverride, EthSVlanGet);
        mMethodOverride(m_AtPwOverride, EthExpectedCVlanSet);
        mMethodOverride(m_AtPwOverride, EthExpectedCVlanGet);
        mMethodOverride(m_AtPwOverride, EthExpectedSVlanSet);
        mMethodOverride(m_AtPwOverride, EthExpectedSVlanGet);
        mMethodOverride(m_AtPwOverride, CircuitBind);
        mMethodOverride(m_AtPwOverride, CircuitUnbind);
        mMethodOverride(m_AtPwOverride, BoundCircuitGet);
        mMethodOverride(m_AtPwOverride, SuppressEnable);
        mMethodOverride(m_AtPwOverride, SuppressIsEnabled);
        mMethodOverride(m_AtPwOverride, JitterBufferSizeInPacketSet);
        mMethodOverride(m_AtPwOverride, JitterBufferSizeInPacketGet);
        mMethodOverride(m_AtPwOverride, JitterBufferDelayInPacketSet);
        mMethodOverride(m_AtPwOverride, JitterBufferDelayInPacketGet);
        mMethodOverride(m_AtPwOverride, JitterBufferAndPayloadSizeSet);
        mMethodOverride(m_AtPwOverride, JitterBufferInPacketAndPayloadSizeSet);
        mMethodOverride(m_AtPwOverride, MinJitterBufferDelayInPacketGet);
        mMethodOverride(m_AtPwOverride, MinJitterBufferSizeInPacketGet);
        mMethodOverride(m_AtPwOverride, IdleCodeSet);
        mMethodOverride(m_AtPwOverride, IdleCodeGet);
        mMethodOverride(m_AtPwOverride, MissingPacketDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, MissingPacketDefectThresholdGet);
        mMethodOverride(m_AtPwOverride, ExcessivePacketLossRateDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, ExcessivePacketLossRateDefectThresholdGet);
        mMethodOverride(m_AtPwOverride, StrayPacketDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, StrayPacketDefectThresholdGet);
        mMethodOverride(m_AtPwOverride, MalformedPacketDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, MalformedPacketDefectThresholdGet);
        mMethodOverride(m_AtPwOverride, RemotePacketLossDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, RemotePacketLossDefectThresholdGet);
        mMethodOverride(m_AtPwOverride, JitterBufferOverrunDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, JitterBufferOverrunDefectThresholdGet);
        mMethodOverride(m_AtPwOverride, JitterBufferUnderrunDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, JitterBufferUnderrunDefectThresholdGet);
        mMethodOverride(m_AtPwOverride, MisConnectionDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, MisConnectionDefectThresholdGet);
        mMethodOverride(m_AtPwOverride, BackupPsnSet);
        mMethodOverride(m_AtPwOverride, BackupPsnGet);
        mMethodOverride(m_AtPwOverride, BackupEthHeaderSet);
        mMethodOverride(m_AtPwOverride, BackupEthDestMacSet);
        mMethodOverride(m_AtPwOverride, BackupEthDestMacGet);
        mMethodOverride(m_AtPwOverride, BackupEthSrcMacSet);
        mMethodOverride(m_AtPwOverride, BackupEthSrcMacGet);
        mMethodOverride(m_AtPwOverride, BackupEthVlanSet);
        mMethodOverride(m_AtPwOverride, BackupEthCVlanGet);
        mMethodOverride(m_AtPwOverride, BackupEthSVlanGet);
        mMethodOverride(m_AtPwOverride, InterruptProcess);
        mMethodOverride(m_AtPwOverride, LopsClearThresholdIsSupported);
        mMethodOverride(m_AtPwOverride, EthCVlanTpidSet);
        mMethodOverride(m_AtPwOverride, EthSVlanTpidSet);
        mMethodOverride(m_AtPwOverride, EthCVlanTpidGet);
        mMethodOverride(m_AtPwOverride, EthSVlanTpidGet);
        mMethodOverride(m_AtPwOverride, EthPortMaxNumQueues);
        mMethodOverride(m_AtPwOverride, EthCVlanTpidSet);
        mMethodOverride(m_AtPwOverride, EthSVlanTpidSet);
        mMethodOverride(m_AtPwOverride, EthCVlanTpidGet);
        mMethodOverride(m_AtPwOverride, EthSVlanTpidGet);

        /* Work with standby driver */
        mMethodOverride(m_AtPwOverride, CachePayloadSizeSet);
        mMethodOverride(m_AtPwOverride, CachePayloadSizeGet);

        mMethodOverride(m_AtPwOverride, EthPortQueueSet);
        mMethodOverride(m_AtPwOverride, EthPortQueueGet);

        /* PTCH */
        mMethodOverride(m_AtPwOverride, PtchInsertionModeSet);
        mMethodOverride(m_AtPwOverride, PtchInsertionModeGet);
        mMethodOverride(m_AtPwOverride, PtchServiceEnable);
        mMethodOverride(m_AtPwOverride, PtchServiceIsEnabled);

        mMethodOverride(m_AtPwOverride, JitterBufferNumBlocksGet);

        mMethodOverride(m_AtPwOverride, TxActiveForce);
        mMethodOverride(m_AtPwOverride, TxActiveIsForced);

        mMethodOverride(m_AtPwOverride, SubPortVlanIndexSet);
        mMethodOverride(m_AtPwOverride, SubPortVlanIndexGet);
        }

    mMethodsSet(pw, &m_AtPwOverride);
    }

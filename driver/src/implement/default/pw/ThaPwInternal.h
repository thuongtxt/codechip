/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : ThaPwInternal.h
 * 
 * Created Date: Nov 17, 2012
 *
 * Description : Thalassa PW
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPWINTERNAL_H_
#define _THAPWINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/pw/AtPwInternal.h"
#include "../cla/hbce/ThaHbceMemoryCellContent.h"
#include "ThaModulePw.h"
#include "ThaPwToh.h"
#include "adapters/ThaPwAdapter.h"
#include "../util/ThaBitMask.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
#define cThaMaxPwPsnHdrLen   128

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* SAToP */
typedef struct tThaPwSAToP * ThaPwSAToP;

typedef struct tThaPwSAToP
    {
    tAtPwSAToP super;

    /* Private data */
    ThaPwAdapter adapter;
    }tThaPwSAToP;

/* CEP */
typedef struct tThaPwCep * ThaPwCep;

typedef struct tThaPwCep
    {
    tAtPwCep super;

    /* Private data */
    ThaPwAdapter adapter;
    }tThaPwCep;

/* CESoP */
typedef struct tThaPwCESoP * ThaPwCESoP;

typedef struct tThaPwCESoP
    {
    tAtPwCESoP super;

    /* Private data */
    ThaPwAdapter adapter;
    }tThaPwCESoP;

/* HDLC PW */
typedef struct tThaPwHdlc * ThaPwHdlc;

typedef struct tThaPwHdlc
    {
    tAtPwHdlc super;

    /* Private data */
    ThaPwAdapter adapter;
    eAtPwHdlcPayloadType payloadType;
    }tThaPwHdlc;

/* ATM PW */
typedef struct tThaPwAtm
    {
    tAtPwAtm super;
    }tThaPwAtm;

/* TOH PW */
typedef struct tThaPwToh
    {
    tAtPwToh super;

    /* Private data */
    ThaPwAdapter adapter;
    ThaBitMask tohBytesMask;

    tThaTohByte firstByte;
    tThaTohByte lastByte;
    uint8 hasFirstLast; /* First byte, last byte will be valid when at least one byte is equipped */

    }tThaPwToh;

/* PPP PW */
typedef struct tThaPwPpp *ThaPwPpp;
typedef struct tThaPwPpp
    {
    tAtPwPpp super;

    /* Private data */
    ThaPwAdapter adapter;
    eAtPwHdlcPayloadType payloadType;
    }tThaPwPpp;

/* FR Virtual Circuit PW */
typedef struct tThaPwFr *ThaPwFr;
typedef struct tThaPwFr
    {
    tAtPwFr super;

    /* Private data */
    ThaPwAdapter adapter;
    eAtPwHdlcPayloadType payloadType;
    }tThaPwFr;

typedef struct tThaPwMlppp *ThaPwMlppp;
typedef struct tThaPwMlppp
    {
    tAtPwPpp super;

    /* Private data */
    ThaPwAdapter adapter;
    eAtPwHdlcPayloadType payloadType;
    }tThaPwMlppp;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void ThaPwOverrideAtChannel(AtPw self, tAtChannelMethods *methods);
void ThaPwOverrideAtPw(AtPw self, tAtPwMethods *methods);

/* Helpers */
ThaPwAdapter ThaPwSAToPAdapterGet(AtPw self);
ThaPwAdapter ThaPwCepAdapterGet(AtPw self);
ThaPwAdapter ThaPwCESoPAdapterGet(AtPw self);
ThaPwAdapter ThaPwTohAdapterGet(AtPw self);
ThaPwAdapter ThaPwHdlcAdapterGet(AtPw self);
ThaPwAdapter ThaPwPppAdapterGet(AtPw self);
ThaPwAdapter ThaPwMlpppAdapterGet(AtPw self);

void ThaPwCepAdapterDelete(AtPw self);
void ThaPwSAToPAdapterDelete(AtPw self);
void ThaPwCESoPAdapterDelete(AtPw self);
void ThaPwTohAdapterDelete(AtPw self);
void ThaPwHdlcAdapterDelete(AtPw self);
void ThaPwPppAdapterDelete(AtPw self);
void ThaPwMlpppAdapterDelete(AtPw self);
void ThaPwFrVcAdapterDelete(AtPw self);
void ThaPwMlpppAdapterDelete(AtPw self);

/* Object init */
AtPw ThaPwSAToPObjectInit(AtPw self, uint32 pwId, AtModulePw module);
AtPw ThaPwCepObjectInit(AtPw self, uint32 pwId, AtModulePw module, eAtPwCepMode mode);

#ifdef __cplusplus
}
#endif
#endif /* _THAPWINTERNAL_H_ */


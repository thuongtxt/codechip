/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : ThaPwAdapterBw.c
 *
 * Created Date: Oct 19, 2015
 *
 * Description : PW adapter bandwidth calculation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPwCep.h"
#include "../ThaPwUtil.h"
#include "../headercontrollers/ThaPwHeaderController.h"
#include "ThaPwAdapterInternal.h"
#include "AtNumber.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 PwEthPortGapPreembleFcsLengthGet(ThaPwAdapter self, AtEthPort ethPort)
    {
    const uint8 cEthPreembleLength = 8;
    const uint8 cNumByteFcs        = 4;
    uint8 portTxGap = 0;

    AtUnused(self);
    if (ethPort)
        portTxGap = AtEthPortTxIpgGet(ethPort);

    return (uint32)(portTxGap + cEthPreembleLength + cNumByteFcs);
    }

static uint32 PwCwLengthGet(ThaPwAdapter self, eBool rtpIsEnabled)
    {
    AtPw pw = ThaPwAdapterPwGet(self);
    uint32 cwLength = 0;

    if (AtPwCwIsEnabled(pw))
        cwLength += 4;

    if (rtpIsEnabled)
        cwLength += 12;

    if (AtPwTypeGet(pw) == cAtPwTypeCEP)
        cwLength += 4;

    return cwLength;
    }

static eBool PwIsCepFractional(AtPw pw)
    {
    if (AtPwTypeGet(pw) != cAtPwTypeCEP)
        return cAtFalse;

    if (AtPwCepModeGet((AtPwCep)pw) == cAtPwCepModeFractional)
        return cAtTrue;

    return cAtFalse;
    }

static uint32 RealPayloadSizeInByte(ThaPwAdapter self, uint16 payloadSize)
    {
    AtPw realPw = ThaPwAdapterPwGet(self);
    AtPw pwAdapter = (AtPw)self;

    if (PwIsCepFractional(realPw))
        return ThaPwCepFractionalRealPayloadSizeCalculate(pwAdapter, payloadSize);

    return ThaPwUtilPayloadSizeInBytes(pwAdapter, AtPwBoundCircuitGet(pwAdapter), payloadSize);
    }

static uint32 PwPaddingLengthGet(ThaPwAdapter self, uint32 controlWordLength, uint16 payloadSize)
    {
    const uint8 cMinimumPayloadSize = 64;
    uint32 pwPldSizeInByte = RealPayloadSizeInByte(self, payloadSize) + controlWordLength;
    return ((pwPldSizeInByte >= cMinimumPayloadSize) ? 0 : (cMinimumPayloadSize - pwPldSizeInByte));
    }

static uint32 PwPsnLengthInByte(AtPwPsn psn)
    {
    return AtPwPsnLengthInBytes(psn);
    }

static uint32 PwHeaderLength(eBool hasCVlan, eBool hasSVlan)
    {
    const uint8 cNumByteMacAddress = 6;
    const uint8 cNumByteVlanTag    = 4;
    const uint8 cNumByteEthType    = 2;
    uint32 pwHeaderLength = (uint32)(2 * cNumByteMacAddress + cNumByteEthType + ((hasCVlan) ? cNumByteVlanTag : 0) + ((hasSVlan) ? cNumByteVlanTag : 0));

    return pwHeaderLength;
    }

static uint64 BandwidthCalculate(uint32 ethAdditionalBytes, uint64 circuitDataRateInbps, uint32 pwPldSizeInByte)
    {
    return (((uint64)ethAdditionalBytes * (circuitDataRateInbps / pwPldSizeInByte)) + circuitDataRateInbps);
    }

static uint32 TotalHeaderLengthInBytes(ThaPwAdapter self, AtPwPsn psn, eBool hasCVlan, eBool hasSVlan, eBool rtpIsEnabled)
    {
    uint32 ethHeaderLength      = PwHeaderLength(hasCVlan, hasSVlan);
    uint32 psnLength            = PwPsnLengthInByte(psn);
    uint32 cwLength             = PwCwLengthGet(self, rtpIsEnabled);

    return (uint32)(ethHeaderLength + psnLength + cwLength);
    }

static uint32 EthAdditionalBytes(ThaPwAdapter self,
                                 uint16 payloadSize,
                                 AtPwPsn psn,
                                 AtEthPort ethPort,
                                 eBool hasCVlan,
                                 eBool hasSVlan,
                                 eBool rtpIsEnabled)
    {
    uint32 totalHeaderLength    = TotalHeaderLengthInBytes(self, psn, hasCVlan, hasSVlan, rtpIsEnabled);
    uint32 gapAndPreemble       = PwEthPortGapPreembleFcsLengthGet(self, ethPort);
    uint32 cwLength             = PwCwLengthGet(self, rtpIsEnabled);
    uint32 paddingLength        = PwPaddingLengthGet(self, cwLength, payloadSize);

    return (uint32)(gapAndPreemble + totalHeaderLength + paddingLength);
    }

static uint64 BandwidthInbpsCalculate(ThaPwAdapter self,
                                      AtChannel circuit,
                                      uint16 payloadSize,
                                      AtPwPsn psn,
                                      AtEthPort ethPort,
                                      eBool hasCVlan,
                                      eBool hasSVlan,
                                      eBool rtpIsEnabled)
    {
    uint64 circuitDataRateInbps = (uint64)ThaPwAdapterDataRateInBytesPerMs(self, circuit) * 8000;
    uint32 ethAdditionalBytes   = EthAdditionalBytes(self, payloadSize, psn, ethPort, hasCVlan, hasSVlan, rtpIsEnabled);
    uint32 pwPldSizeInByte      = RealPayloadSizeInByte(self, payloadSize);

    if (pwPldSizeInByte == 0)
        return 0;

    return BandwidthCalculate(ethAdditionalBytes, circuitDataRateInbps, pwPldSizeInByte);
    }

static eBool PwHasCvlan(AtPw self)
    {
    tAtEthVlanTag cVlan;
    return (AtPwEthCVlanGet(self, &cVlan) != NULL) ? cAtTrue : cAtFalse;
    }

static eBool PwHasSvlan(AtPw self)
    {
    tAtEthVlanTag sVlan;
    return (AtPwEthSVlanGet(self, &sVlan) != NULL) ? cAtTrue : cAtFalse;
    }

uint64 ThaPwAdapterCurrentPwBandwidthInBpsGet(AtPw self)
    {
    uint64 bandwidthInBps;
    bandwidthInBps  = (uint64)mAdapter(self)->currentBandwidthInMbps * 1000000;
    bandwidthInBps += (uint64)mAdapter(self)->currentBandwidthInBps;
    return bandwidthInBps;
    }

void ThaPwAdapterCurrentPwBandwidthInBpsSet(AtPw self, uint64 bandwidthInBps)
    {
    mAdapter(self)->currentBandwidthInBps  = (uint32)(bandwidthInBps % 1000000);
    mAdapter(self)->currentBandwidthInMbps = (uint32)(bandwidthInBps / 1000000);
    }

uint64 ThaPwAdapterCurrentPwBandwidthInBpsCalculate(AtPw self)
    {
    return BandwidthInbpsCalculate(mAdapter(self),
                                   AtPwBoundCircuitGet(self),
                                   AtPwPayloadSizeGet(self),
                                   AtPwPsnGet(self),
                                   AtPwEthPortGet(self),
                                   PwHasCvlan(self),
                                   PwHasSvlan(self),
                                   AtPwRtpIsEnabled(self));
    }

uint64 ThaPwAdapterBandwidthCalculateWithPayload(AtPw self, uint16 payloadSize)
    {
    return BandwidthInbpsCalculate(mAdapter(self),
                                   AtPwBoundCircuitGet(self),
                                   payloadSize,
                                   AtPwPsnGet(self),
                                   AtPwEthPortGet(self),
                                   PwHasCvlan(self),
                                   PwHasSvlan(self),
                                   AtPwRtpIsEnabled(self));
    }

uint64 ThaPwAdapterBandwidthCalculateWithRtpEnable(AtPw self, eBool enable)
    {
    return BandwidthInbpsCalculate(mAdapter(self),
                                   AtPwBoundCircuitGet(self),
                                   AtPwPayloadSizeGet(self),
                                   AtPwPsnGet(self),
                                   AtPwEthPortGet(self),
                                   PwHasCvlan(self),
                                   PwHasSvlan(self),
                                   enable);
    }

uint64 ThaPwAdapterBandwidthCalculateWithPsn(AtPw self, AtPwPsn psn)
    {
    return BandwidthInbpsCalculate(mAdapter(self),
                                   AtPwBoundCircuitGet(self),
                                   AtPwPayloadSizeGet(self),
                                   psn,
                                   AtPwEthPortGet(self),
                                   PwHasCvlan(self),
                                   PwHasSvlan(self),
                                   AtPwRtpIsEnabled(self));
    }

uint64 ThaPwAdapterBandwidthCalculateWithEthHeader(AtPw self, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan)
    {
    return BandwidthInbpsCalculate(mAdapter(self),
                                   AtPwBoundCircuitGet(self),
                                   AtPwPayloadSizeGet(self),
                                   AtPwPsnGet(self),
                                   AtPwEthPortGet(self),
                                   (cVlan) ? cAtTrue : cAtFalse,
                                   (sVlan) ? cAtTrue : cAtFalse,
                                   AtPwRtpIsEnabled(self));
    }

uint64 ThaPwAdapterBandwidthCalculateWithCircuit(AtPw self, AtChannel circuit)
    {
    return BandwidthInbpsCalculate(mAdapter(self),
                                   circuit,
                                   AtPwPayloadSizeGet(self),
                                   AtPwPsnGet(self),
                                   AtPwEthPortGet(self),
                                   PwHasCvlan(self),
                                   PwHasSvlan(self),
                                   AtPwRtpIsEnabled(self));
    }

uint64 ThaPwAdapterBandwidthCalculateWithEthPort(AtPw self, AtEthPort ethPort)
    {
    return BandwidthInbpsCalculate(mAdapter(self),
                                   AtPwBoundCircuitGet(self),
                                   AtPwPayloadSizeGet(self),
                                   AtPwPsnGet(self),
                                   ethPort,
                                   PwHasCvlan(self),
                                   PwHasSvlan(self),
                                   AtPwRtpIsEnabled(self));
    }

uint32 ThaPwAdapterPayloadSizeInByteFromBwCalculate(AtPw self, uint64 bandwidth)
    {
    uint32 gapAndPreemble;
    uint32 paddingLength;
    uint32 ethAdditionalBytes;
    uint32 totalHeaderLength;
    ThaPwAdapter adapter = mAdapter(self);
    uint64 circuitDataRateInbps = (uint64)ThaPwAdapterDataRateInBytesPerMs(adapter, AtPwBoundCircuitGet(self)) * 8000;

    if (bandwidth <= circuitDataRateInbps)
        return 0;

    totalHeaderLength  = TotalHeaderLengthInBytes(adapter, AtPwPsnGet(self), PwHasCvlan(self), PwHasSvlan(self), AtPwRtpIsEnabled(self));
    gapAndPreemble     = PwEthPortGapPreembleFcsLengthGet(adapter, AtPwEthPortGet(self));
    paddingLength      = 0;
    ethAdditionalBytes = gapAndPreemble + totalHeaderLength + paddingLength;

    return (uint32)((ethAdditionalBytes * circuitDataRateInbps) / (bandwidth - circuitDataRateInbps)) + 1;
    }

void ThaPwAdapterCurrentPwBandwidthDebug(AtPw self)
    {
    ThaPwAdapter adapter = (ThaPwAdapter)self;
    uint64 circuitDataRateInbps = (uint64)ThaPwAdapterDataRateInBytesPerMs(adapter, AtPwBoundCircuitGet(self)) * 8000;
    uint32 ethHeaderLength      = PwHeaderLength(PwHasCvlan(self), PwHasSvlan(self));
    uint32 psnLength            = PwPsnLengthInByte(AtPwPsnGet(self));
    uint32 gapPreembleFcs       = PwEthPortGapPreembleFcsLengthGet(adapter, AtPwEthPortGet(self));
    uint32 cwLength             = PwCwLengthGet(adapter, AtPwRtpIsEnabled(self));
    uint32 paddingLength        = PwPaddingLengthGet(adapter, cwLength, AtPwPayloadSizeGet(self));
    uint32 ethAdditionalBytes   = gapPreembleFcs + ethHeaderLength + cwLength + paddingLength + psnLength;
    uint32 pwPldSizeInByte      = RealPayloadSizeInByte(adapter, AtPwPayloadSizeGet(self));

    uint64 bandwidth = (pwPldSizeInByte == 0) ? 0 : BandwidthCalculate(ethAdditionalBytes, circuitDataRateInbps, pwPldSizeInByte);

    AtPrintc(cSevNormal, "* PW bandwidth                : "); AtPrintc(cSevNormal, "%s bps\r\n", AtNumberUInt64DigitGroupingString(bandwidth));
    AtPrintc(cSevNormal, "    - Circuit Data Rate       : "); AtPrintc(cSevNormal, "%s bps\r\n", AtNumberUInt64DigitGroupingString(circuitDataRateInbps));
    AtPrintc(cSevNormal, "    - Ethernet header         : "); AtPrintc(cSevNormal, "%u bytes\r\n", ethHeaderLength);
    AtPrintc(cSevNormal, "    - PSN header              : "); AtPrintc(cSevNormal, "%u bytes\r\n", psnLength);
    AtPrintc(cSevNormal, "    - Gap, preemble and FCS   : "); AtPrintc(cSevNormal, "%u bytes\r\n", gapPreembleFcs);
    AtPrintc(cSevNormal, "    - CW and RTP              : "); AtPrintc(cSevNormal, "%u bytes\r\n", cwLength);
    AtPrintc(cSevNormal, "    - Padding                 : "); AtPrintc(cSevNormal, "%u bytes\r\n", paddingLength);
    AtPrintc(cSevNormal, "    - Payload size in bytes   : "); AtPrintc(cSevNormal, "%u bytes\r\n", pwPldSizeInByte);
    }

uint32 ThaPwAdapterTotalHeaderLengthGet(AtPw self)
    {
    return TotalHeaderLengthInBytes((ThaPwAdapter)self, AtPwPsnGet(self), PwHasCvlan(self), PwHasSvlan(self), AtPwRtpIsEnabled(self));
    }

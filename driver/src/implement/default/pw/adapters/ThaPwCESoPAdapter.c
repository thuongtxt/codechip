/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : ThaPwCESoPAdapter.c
 *
 * Created Date: Apr 02, 2013
 *
 * Description : Thalassa PW implementation
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaPwAdapterInternal.h"
#include "../../pwe/ThaModulePwe.h"
#include "../../sdh/ThaModuleSdh.h"
#include "../../sur/hard/ThaModuleHardSur.h"
#include "../ethportbinder/ThaPwEthPortBinder.h"
#include "../../cla/ThaModuleCla.h"
#include "../../pdh/ThaPdhNxDs0.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mInAccessible(self) AtChannelInAccessible((AtChannel)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaPwCESoPAdapterMethods m_methods;

/* Override */
static tAtChannelMethods    m_AtChannelOverride;
static tThaPwAdapterMethods m_ThaPwAdapterOverride;
static tAtPwMethods         m_AtPwOverride;

/* Save super's implementation */
static const tAtPwMethods         *m_AtPwMethods         = NULL;
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;
static const tThaPwAdapterMethods *m_ThaPwAdapterMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaPwCESoPAdapter);
    }

static ThaModulePwe ModulePwe(AtPw self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaModulePwe)AtDeviceModuleGet(device, cThaModulePwe);
    }

static ThaModulePda ModulePda(AtPw self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaModulePda)AtDeviceModuleGet(device, cThaModulePda);
    }

static ThaModuleCla ModuleCla(AtPw self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaModuleCla)AtDeviceModuleGet(device, cThaModuleCla);
    }

static ThaModulePw ModulePw(AtPw self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaModulePw)AtDeviceModuleGet(device, cAtModulePw);
    }

static ThaModuleHardSur ModuleSur(AtPw self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaModuleHardSur)AtDeviceModuleGet(device, cAtModuleSur);
    }

static const char *TypeString(AtChannel self)
    {
	AtUnused(self);
    return "pw_cesop_adapter";
    }

static AtPwCounters CountersCreate(ThaPwAdapter self)
    {
	AtUnused(self);
    return (AtPwCounters)AtPwCESoPCountersNew();
    }

static eAtRet AllCountersReadToClear(AtChannel self, void *pAllCounters, eBool clear)
    {
    eAtRet ret;
    AtPw pw = (AtPw)self;
    AtPwCESoPCounters counters = (AtPwCESoPCounters)ThaPwAdapterCountersGet((ThaPwAdapter)self);

	AtUnused(pAllCounters);

    /* Read TDM counters */
    ret = ThaTdmPwCountersReadToClear(pw, clear);
    if (ret != cAtOk)
        return ret;

    /* TX additional counter */
    if (ThaPwAdapterShouldReadCountersFromSurModule(self))
        {
        ThaModuleHardSur surModule = ModuleSur(pw);
        AtPwCESoPCountersTxMbitPacketsSet(counters, ThaModuleHardSurPwTxMbitPacketsGet(surModule, pw, clear));
        AtPwCESoPCountersRxMbitPacketsSet(counters, ThaModuleHardSurPwRxMbitPacketsGet(surModule, pw, clear));
        }
    else
        {
        ThaModulePwe pweModule = ModulePwe(pw);
        AtPwCESoPCountersTxMbitPacketsSet(counters, ThaModulePwePwTxMbitPacketsGet(pweModule, pw, clear));
        AtPwCESoPCountersRxMbitPacketsSet(counters, ThaClaPwControllerRxMbitPacketsGet(ThaPwAdapterClaPwController((ThaPwAdapter)self), pw, clear));
        }

    return cAtOk;
    }

static eAtRet AllCountersGet(AtChannel self, void *pAllCounters)
    {
    eAtRet ret;

    /* Let the super do first */
    ret = m_AtChannelMethods->AllCountersGet(self, pAllCounters);
    if (ret != cAtOk)
        return ret;

    /* Add read additional counters */
    return AllCountersReadToClear(self, pAllCounters, cAtFalse);
    }

static eAtRet AllCountersClear(AtChannel self, void *pAllCounters)
    {
    eAtRet ret;

    /* Let the super do first */
    ret = m_AtChannelMethods->AllCountersClear(self, pAllCounters);
    if (ret != cAtOk)
        return ret;

    /* And clear additional counters */
    return AllCountersReadToClear(self, pAllCounters, cAtTrue);
    }

static uint32 DefaultPayloadSize(ThaPwAdapter self, AtChannel circuit)
    {
	AtUnused(circuit);
	AtUnused(self);
    return 8;   /* It means 8 frames */
    }

static uint8 PartOfCircuit(ThaPwAdapter self, AtChannel circuit)
    {
    AtPdhDe1 de1 = AtPdhNxDS0De1Get((AtPdhNxDS0)circuit);
    AtSdhChannel vc = (AtSdhChannel)AtPdhChannelVcInternalGet((AtPdhChannel)de1);
	AtUnused(self);
    return (uint8)(vc ? ThaModuleSdhPartOfChannel(vc) : 0);
    }

static eAtRet SetDefaultOnBinding(ThaPwAdapter self, AtChannel circuit)
    {
    AtPw pw = (AtPw)self;
    eAtRet ret = cAtOk;
    ThaModulePda pdaModule = ModulePda(pw);
    ThaModulePwe pweModule = ModulePwe(pw);
    AtModulePw pwModule = (AtModulePw)ModulePw(pw);
    AtPwCESoP cesop = (AtPwCESoP)ThaPwAdapterPwGet(self);
    uint8 numSlots;
    eBool autoInsertRdi;

    ret |= m_ThaPwAdapterMethods->SetDefaultOnBinding(self, circuit);
    if (ret != cAtOk)
        return ret;

    numSlots = mMethodsGet(mAdapter(self))->NumDs0Slots(mAdapter(self));
    ret |= ThaModulePwePwNumDs0TimeslotsSet(pweModule, pw, numSlots);
    ret |= ThaModulePdaPwNumDs0TimeslotsSet(pdaModule, pw, numSlots);
    ret |= ThaModulePdaCESoPModeSet(pdaModule, self);
    ret |= ThaModulePdaPwCircuitTypeSet(pdaModule, pw, circuit);

    /* Some products may not support disabling auto insert RDI when PW receive
     * R-Bit or M-Bit packets. Just ignore error code */
    autoInsertRdi = ThaModulePdaShouldAutoInsertRdi(pdaModule);
    ThaModulePdaAutoInsertRdiEnable(pdaModule, pw, autoInsertRdi);

    /* CESOP with CAS */
    if (AtModulePwCasIdleCodeIsSupported(pwModule))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(self);
        ret |= AtPwCESoPCasIdleCode1Set(cesop, cache->casIdleCode1);
        ret |= AtPwCESoPCasIdleCode2Set(cesop, cache->casIdleCode2);
        ret |= AtPwCESoPCasAutoIdleEnable(cesop, cache->casAutoIdleEnabled);
        }

    return ret;
    }

static ThaPdhNxDs0 NxDS0Get(ThaPwAdapter self)
    {
    return (ThaPdhNxDs0)AtPwBoundCircuitGet((AtPw)self);
    }

static eAtRet PwCasIdleCode1Set(ThaPwAdapter self, uint8 abcd1)
    {
    return ThaPdhNxDs0PwCasIdleCode1Set(NxDS0Get(self), (AtPw)self, abcd1);
    }

static uint8 PwCasIdleCode1Get(ThaPwAdapter self)
    {
    return ThaPdhNxDs0PwCasIdleCode1Get(NxDS0Get(self), (AtPw)self);
    }

static eAtRet PwCasIdleCode2Set(ThaPwAdapter self, uint8 abcd2)
    {
    return ThaPdhNxDs0PwCasIdleCode2Set(NxDS0Get(self), (AtPw)self, abcd2);
    }

static uint8 PwCasIdleCode2Get(ThaPwAdapter self)
    {
    return ThaPdhNxDs0PwCasIdleCode2Get(NxDS0Get(self), (AtPw)self);
    }

static eAtModulePwRet CasIdleCode1Set(ThaPwCESoPAdapter self, uint8 abcd1)
    {
    eAtRet ret = cAtOk;
    ThaPdhNxDs0 nxds0 = NxDS0Get(mAdapter(self));
    tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(self));

    if (!nxds0 || ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        if (cache)
            cache->casIdleCode1 = abcd1;
        return cAtOk;
        }

    ret = ThaModulePwePwCesopCasIdleCode1Set(ModulePwe((AtPw)self), mAdapter(self), abcd1);
    ret |= PwCasIdleCode1Set(mAdapter(self), abcd1);
    if (ret == cAtOk)
        {
        if (cache)
            cache->casIdleCode1 = abcd1;
        }

    return ret;
    }

static uint8 CasIdleCode1Get(ThaPwCESoPAdapter self)
    {
    ThaPdhNxDs0 nxds0 = NxDS0Get(mAdapter(self));
    tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(self));

    if (!nxds0 || ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        return (uint8)(cache ? cache->casIdleCode1 : 0);
        }

    return PwCasIdleCode1Get(mAdapter(self));
    }

static eAtModulePwRet CasIdleCode2Set(ThaPwCESoPAdapter self, uint8 abcd2)
    {
    eAtRet ret = cAtOk;
    ThaPdhNxDs0 nxds0 = NxDS0Get(mAdapter(self));
    tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(self));

    if (!nxds0 || ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        if (cache)
            cache->casIdleCode2 = abcd2;
        return cAtOk;
        }

    ret = PwCasIdleCode2Set(mAdapter(self), abcd2);
    if (ret == cAtOk)
        {
        if (cache)
            cache->casIdleCode2 = abcd2;
        }

    return ret;
    }

static uint8 CasIdleCode2Get(ThaPwCESoPAdapter self)
    {
    ThaPdhNxDs0 nxds0 = NxDS0Get(mAdapter(self));
    tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(self));

    if (!nxds0 || ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        return (uint8)(cache ? cache->casIdleCode2 : 0);
        }

    return PwCasIdleCode2Get(mAdapter(self));
    }

static eAtRet PwCasAutoIdleEnable(ThaPwAdapter self, eBool enable)
    {
    return ThaPdhNxDs0PwCasAutoIdleEnable(NxDS0Get(self), (AtPw)self, enable);
    }

static eBool PwCasAutoIdleIsEnabled(ThaPwAdapter self)
    {
    return ThaPdhNxDs0PwCasAutoIdleIsEnabled(NxDS0Get(self), (AtPw)self);
    }

static eAtModulePwRet CasAutoIdleEnable(ThaPwCESoPAdapter self, eBool enable)
    {
    eAtRet ret = cAtOk;
    ThaPdhNxDs0 nxds0 = NxDS0Get(mAdapter(self));
    tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(self));

    if (!nxds0 || ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        if (cache)
            cache->casAutoIdleEnabled = enable;
        return cAtOk;
        }

    ret = PwCasAutoIdleEnable(mAdapter(self), enable);
    if (ret == cAtOk)
        {
        if (cache)
            cache->casAutoIdleEnabled = enable;
        }

    return ret;
    }

static eBool CasAutoIdleIsEnabled(ThaPwCESoPAdapter self)
    {
    ThaPdhNxDs0 nxds0 = NxDS0Get(mAdapter(self));
    tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(self));

    if (!nxds0 || ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        return (eBool)(cache ? cache->casAutoIdleEnabled : cAtFalse);
        }

    return PwCasAutoIdleIsEnabled(mAdapter(self));
    }

static eAtModulePwRet RxCasModeSet(ThaPwCESoPAdapter self, eAtPwCESoPCasMode mode)
    {
    tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(self));

    if (!ThaPwCESoPAdapterWithCas(self))
        return cAtErrorModeNotSupport;

    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        if (cache)
            cache->casMode = mode;
        return cAtOk;
        }

    return ThaModulePdaCESoPCasModeSet(ModulePda((AtPw)self), mAdapter(self), mode);
    }

static eAtPwCESoPCasMode RxCasModeGet(ThaPwCESoPAdapter self)
    {
    tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(self));

    if (!ThaPwCESoPAdapterWithCas(self))
        return cAtPwCESoPCasModeUnknown;

    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        return (cache ? cache->casMode : cAtPwCESoPCasModeUnknown);
        }

    return ThaModulePdaCESoPCasModeGet(ModulePda((AtPw)self), mAdapter(self));
    }

static eAtModulePwRet CwAutoTxMBitEnable(ThaPwCESoPAdapter self, eBool enable)
    {
    AtPw pw = (AtPw)self;

    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(self));
        if (cache)
            cache->cwAutoTxMBitEnabled = enable;
        return cAtOk;
        }

    if (mMethodsGet(self)->CwAutoTxMBitIsEnabled(self) == enable)
        return cAtOk;

    return ThaModulePwePwCwAutoTxMBitEnable(ModulePwe(pw), pw, enable);
    }

static eBool CwAutoTxMBitIsEnabled(ThaPwCESoPAdapter self)
    {
    AtPw pw = (AtPw)self;

    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(self));
        return (eBool)(cache ? cache->cwAutoTxMBitEnabled : cAtFalse);
        }

    return ThaModulePwePwCwAutoTxMBitIsEnabled(ModulePwe(pw), pw);
    }

static eAtModulePwRet CwAutoRxMBitEnable(ThaPwCESoPAdapter self, eBool enable)
    {
    AtPw pw = (AtPw)self;

    if (!mMethodsGet(self)->CwAutoRxMBitIsConfigurable(self))
        return enable ? cAtOk : cAtErrorModeNotSupport;

    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(self));
        if (cache)
            cache->cwAutoRxMBitEnabled = enable;
        return cAtOk;
        }

    if (mMethodsGet(self)->CwAutoRxMBitIsEnabled(self) == enable)
        return cAtOk;

    return ThaModulePdaPwCwAutoRxMBitEnable(ModulePda(pw), pw, enable);
    }

static eBool CwAutoRxMBitIsEnabled(ThaPwCESoPAdapter self)
    {
    AtPw pw = (AtPw)self;

    if (!mMethodsGet(self)->CwAutoRxMBitIsConfigurable(self))
        return cAtTrue;

    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(self));
        return (eBool)(cache ? cache->cwAutoRxMBitEnabled : cAtFalse);
        }

    return ThaModulePdaPwCwAutoRxMBitIsEnabled(ModulePda(pw), pw);
    }

static eBool CwAutoRxMBitIsConfigurable(ThaPwCESoPAdapter self)
    {
    return ThaModulePdaPwCwAutoRxMBitIsConfigurable(ModulePda((AtPw)self));
    }

static eAtPwCESoPMode ModeGet(ThaPwCESoPAdapter self)
    {
	AtUnused(self);
    return cAtPwCESoPModeUnknown;
    }

static uint8 NumDs0Slots(ThaPwAdapter self)
    {
    AtPdhNxDS0 circuit = (AtPdhNxDS0)AtPwBoundCircuitGet((AtPw)self);
    return AtPdhNxDs0NumTimeslotsGet(circuit);
    }

static uint16 JitterPayloadLengthCalculate(ThaPwAdapter self)
    {
    return (uint16)(mMethodsGet(self)->NumDs0Slots(self) * AtPwPayloadSizeGet((AtPw)self));
    }

static eAtRet JitterPayloadLengthSet(ThaPwAdapter self)
    {
    AtPw pw = (AtPw)self;
    ThaModulePda pdaModule = ModulePda(pw);
    ThaModulePdaPwJitterPayloadLengthSet(pdaModule, pw, JitterPayloadLengthCalculate(self));
    return cAtOk;
    }

static ThaCdrController CdrController(ThaPwAdapter self)
    {
    AtPdhNxDS0 nxDs0 = (AtPdhNxDS0)AtPwBoundCircuitGet((AtPw)self);
    ThaPdhDe1 circuit = (ThaPdhDe1)AtPdhNxDS0De1Get(nxDs0);
    if (circuit)
        return ThaPdhDe1CdrControllerGet(circuit);
    return NULL;
    }

static uint32 DbPayloadSizeInBytes(ThaPwAdapter self)
    {
    uint32 numFrames = AtPwPayloadSizeGet((AtPw)self);
    AtPdhNxDS0 nxDs0 = (AtPdhNxDS0)AtPwBoundCircuitGet((AtPw)self);
    return numFrames * AtPdhNxDs0NumTimeslotsGet(nxDs0);
    }

static uint32 PayloadSizeInBytes(ThaPwAdapter self)
    {
    AtPw pw = (AtPw)self;
    ThaModulePwe pweModule = (ThaModulePwe)ModulePwe(pw);

    if (mInAccessible(self) || ThaPwAdapterIsLogicPw(self))
        return DbPayloadSizeInBytes(self);

    return (uint32)ThaModulePwePwNumDs0TimeslotsGet(pweModule, pw) *
                   ThaModulePweNumFramesInPacketGet(pweModule, pw);
    }

static uint16 DCRRxNcoConfPackLen(ThaPwAdapter self)
    {
    return (uint16)(AtPwPayloadSizeGet((AtPw)self) * 32);
    }

static uint16 CDRTimingPktLenInBits(ThaPwAdapter self)
    {
    uint16 numFramesInPacket = AtPwPayloadSizeGet((AtPw)self);
    return (uint16)((AtPdhDe1IsE1(AtPdhNxDS0De1Get((AtPdhNxDS0)AtPwBoundCircuitGet((AtPw)self))) ? 256 : 193) * numFramesInPacket);
    }

static uint16 PdaMinNumFrame(AtPw self, AtChannel circuit)
    {
    uint32 minNumBytes = ThaModulePdaCESoPMinPayloadSize(ModulePda(self), self);
    uint8  numSlots = AtPdhNxDs0NumTimeslotsGet((AtPdhNxDS0)circuit);
    uint16 pdaMinNumFrames = ThaModulePdaCESoPMinNumFrames(ModulePda(self), self);
    uint16 minNumFrames;

    if (numSlots == 0)
        minNumFrames = 0;
    else
        {
        minNumFrames = (uint16)(minNumBytes / numSlots);
        if (minNumBytes % numSlots)
            minNumFrames++;
        }

    return (uint16)mMax(pdaMinNumFrames, minNumFrames);
    }

static uint16 DefaultMinPayloadSize(AtChannel circuit)
    {
    return (AtPdhNxDs0NumTimeslotsGet((AtPdhNxDS0)circuit) >= 2) ? 1 : 2; /* Frames */
    }

static uint16 AdapterMinPayloadSize(ThaPwAdapter self, AtChannel circuit, uint32 jitterBufferSize)
    {
	AtUnused(jitterBufferSize);
	return (uint16)mMax(PdaMinNumFrame((AtPw)self, circuit), DefaultMinPayloadSize(circuit));
    }

static uint16 PdaMaxNumFrame(AtPw self, AtChannel circuit, uint32 jitterBufferSize)
    {
    /* Super return maximum num byte */
    uint32 numBytes = m_AtPwMethods->MaxPayloadSize(self, circuit, jitterBufferSize);
    uint8  numSlots = AtPdhNxDs0NumTimeslotsGet((AtPdhNxDS0)circuit);
    uint16 pdaMaxNumFrames = ThaModulePdaCESoPMaxNumFrames(ModulePda(self), self);
    uint16 numFrames;

    if (numSlots == 0)
        numFrames = (uint16)DefaultPayloadSize((ThaPwAdapter)self, circuit);
    else
        {
        numFrames = (uint16)(numBytes / numSlots);
        if (numFrames == 0)
            numFrames = 1;
        }

    return (uint16)mMin(numFrames, pdaMaxNumFrames);
    }

static ThaPwEthPortBinder EthPortBinder(AtPw self)
    {
    return ThaModulePwEthPortBinder(ModulePw(self));
    }

static uint16 MinPayloadSize(AtPw self, AtChannel circuit, uint32 jitterBufferSize)
    {
    uint32 minNumFrameByJitterBuffer = mMethodsGet(mAdapter(self))->AdapterMinPayloadSize(mAdapter(self), circuit, jitterBufferSize);
    uint64 remainingBwForPw = ThaPwEthPortBinderPwRemainingBw(EthPortBinder(self), self);
    uint32 minPayloadSizeByBwController = ThaPwAdapterPayloadSizeInByteFromBwCalculate(self, remainingBwForPw);
    uint8  numSlots = AtPdhNxDs0NumTimeslotsGet((AtPdhNxDS0)circuit);
    uint32 minNumFrameByBw = (numSlots > 0) ? minPayloadSizeByBwController / numSlots : 1;

    return (uint16)mMax(minNumFrameByJitterBuffer, minNumFrameByBw);
    }

/* Maximum number of frame */
static uint16 MaxPayloadSize(AtPw self, AtChannel circuit, uint32 jitterBufferSize)
    {
    uint16 pdaMaxNumFrame = PdaMaxNumFrame(self, circuit, jitterBufferSize);
    uint16 pweMaxNumFrame = ThaModulePwePwCesMaxNumFramesInPacket(ModulePwe(self));

    return mMin(pdaMaxNumFrame, pweMaxNumFrame);
    }

static eAtRet HwPayloadSizeSet(ThaPwAdapter self, uint16 numFramesPerPacket)
    {
    eAtRet ret = cAtOk;
    AtPw pw = (AtPw)self;
    ThaModulePwe pweModule = ModulePwe(pw);

    uint32 lopsThreshold = AtPwLopsSetThresholdGet(pw);
    ret |= ThaModulePwePwNumFramesInPacketSet(pweModule, pw, numFramesPerPacket);
    ret |= ThaModulePwePwNumDs0TimeslotsSet(pweModule, pw, mMethodsGet(self)->NumDs0Slots(self));
    ret |= ThaClaPwControllerPayloadSizeSet(ThaPwAdapterClaPwController(self), pw, (uint16)PayloadSizeInBytes(self));
    ret |= mMethodsGet(self)->CdrPayloadUpdate(self);

    if ((ret != cAtOk) || (lopsThreshold == 0))
        return ret;

    /* Reconfigure LOPS value to update HW field */
    return AtPwLopsSetThresholdSet((AtPw)ThaPwAdapterPwDefectControllerGet(self), lopsThreshold);
    }

static eBool LimitResourceIsEnabled(AtPw self)
    {
    ThaModulePw  pwModule = (ThaModulePw)AtChannelModuleGet((AtChannel)self);
    return ThaModulePwResourcesLimitationIsDisabled(pwModule) ? cAtFalse : cAtTrue;
    }

static eBool PayloadSizeIsValid(ThaPwAdapter self, uint16 payloadSize)
    {
    ThaPwCESoPAdapter pw = (ThaPwCESoPAdapter)self;
    AtChannel circuit = AtPwBoundCircuitGet((AtPw)self);
    uint16 numFramesPerPacket = payloadSize;

    if (!circuit)
        return cAtTrue;

    return ThaPwCESoPAdapterPayloadSizeInFrameIsValid(pw, circuit, numFramesPerPacket);
    }

static eAtModulePwRet PayloadSizeSet(AtPw self, uint16 numFramesPerPacket)
    {
    eBool claEnabled;
    ThaPwAdapter adapter = (ThaPwAdapter)self;
    eAtRet ret = cAtOk;
    uint8 numSlots;
    ThaModulePda pdaModule = ModulePda(self);
    static const uint8 cMinPayloadSize = 2;
    AtChannel circuit = AtPwBoundCircuitGet(self);
    uint32 payloadSizeInByte;
    uint64 newBw;
    uint32 newOccupiedBlock;

    /* Need to check if payload size is in range if there is already a circuit
     * bound to it */

    if (!ThaPwAdatperPayloadSizeIsValid(adapter, numFramesPerPacket))
        return cAtErrorInvlParm;

    if (LimitResourceIsEnabled(self) && (circuit != NULL))
        {
        uint8 numTimeSlot = AtPdhNxDs0NumTimeslotsGet((AtPdhNxDS0)circuit);
        uint32 payloadSizeInBytes = (uint32)(numTimeSlot * numFramesPerPacket);
        if (payloadSizeInBytes > mMethodsGet(adapter)->PdaMaxPayloadSize(adapter, pdaModule))
            return cAtErrorOutOfRangParm;

        if (!ThaPwCESoPAdapterPayloadSizeInFrameIsValid((ThaPwCESoPAdapter)self, circuit, numFramesPerPacket))
            return cAtErrorOutOfRangParm;
        }

    /* Just cache when PW has not been activated */
    if (ThaPwAdapterIsLogicPw(mAdapter(self)) || (circuit == NULL))
        {
        mAdapter(self)->payloadSize = numFramesPerPacket;
        return m_AtPwMethods->PayloadSizeSet(self, numFramesPerPacket);
        }

    /* Check if payload size is in range */
    if (LimitResourceIsEnabled(self) && !ThaPwAdapterPayloadSizeIsInRange(self, numFramesPerPacket))
        return cAtErrorOutOfRangParm;

    numSlots = mMethodsGet(mAdapter(self))->NumDs0Slots(mAdapter(self));
    payloadSizeInByte = (uint32)(numSlots * numFramesPerPacket);
    if (LimitResourceIsEnabled(self) && payloadSizeInByte < cMinPayloadSize)
        return cAtErrorInvlParm;

    /* Check if bandwidth is acceptable */
    newBw = ThaPwAdapterBandwidthCalculateWithPayload(self, numFramesPerPacket);
    if (LimitResourceIsEnabled(self) && !ThaPwEthPortBinderPwBandwidthIsAcceptable(EthPortBinder(self), self, newBw))
        return cAtErrorBandwidthExceeded;

    /* Check if PDA block is available for new payload size */
    newOccupiedBlock = ThaModulePdaPwNumBlockByBufferInUs(pdaModule, self, AtPwJitterBufferSizeGet(self), payloadSizeInByte);
    if (LimitResourceIsEnabled(self) && !ThaModulePdaResourceIsAvailable(ModulePda(self), self, newOccupiedBlock))
        return cAtErrorRsrcNoAvail;

    ThaPwAdapterStartRemoving(self);

    /* Disable CLA to stop packets come to FPGA */
    claEnabled = ThaPwAdapterClaIsEnabled(self);
    ThaModulePdaHotConfigureStart(pdaModule, self);

    if (mInAccessible(self))
        mAdapter(self)->payloadSize = numFramesPerPacket;

    ret |= mMethodsGet(adapter)->HwPayloadSizeSet(adapter, numFramesPerPacket);
    ret |= ThaPwAdapterJitterBufferDefaultSet(adapter);

    /* Enable PW */
    if (claEnabled)
        ret |= ThaPwAdapterClaEnable(self, cAtTrue);

    ThaModulePdaHotConfigureStop(pdaModule, self);
    if (ret == cAtOk)
        mAdapter(self)->payloadSize = numFramesPerPacket;

    ThaPwAdapterFinishRemoving(self);

    if (ret != cAtOk)
        return ret;

    ret = ThaPwEthPortBinderPwBandwidthUpdate(EthPortBinder(self), self, newBw);
    ret |= ThaModulePdaTotalInUsedRamBlockUpdate(pdaModule, self, newOccupiedBlock);

    return ret;
    }

static uint16 HwPayloadSizeGet(ThaPwAdapter self)
    {
    AtPw pw = (AtPw)self;
    return ThaModulePweNumFramesInPacketGet(ModulePwe(pw), pw);
    }

static uint8 CASLength(uint8 numSlot)
    {
    return (uint8)((numSlot + 1) / 2);
    }

static eAtModulePwRet CircuitBind(AtPw self, AtChannel circuit)
    {
    eAtRet ret = cAtOk;
    ThaModuleCla claModule = ModuleCla(self);

    if (circuit == NULL)
        return m_AtPwMethods->CircuitBind(self, circuit);

    ret = m_AtPwMethods->CircuitBind(self, circuit);
    if (ret != cAtOk)
        return ret;

    if (!ThaModulePwCasIsSupported(ModulePw(self)))
        return ret;

    ret |= ThaModulePwePwNxDs0Set(ModulePwe(self), self, (AtPdhNxDS0)circuit);

    if (ThaPwCESoPAdapterWithCas((ThaPwCESoPAdapter)self))
        {
        uint8 numTimeslots = AtPdhNxDs0NumTimeslotsGet((AtPdhNxDS0)circuit);
        ret |= ThaModuleClaCESoPCASLengthSet(claModule, self, CASLength(numTimeslots));
        }

    return ret;
    }

static eAtModulePwRet CircuitUnbind(AtPw self)
    {
    AtChannel refCircuit;
    eAtRet ret = cAtOk;

    /* For channel that is using this PW for ACR/DCR for timing, need to change
     * its timing to system because this PW is no longer work */
    refCircuit = ThaPwAdapterAcrDcrRefCircuitGet(mAdapter(self));
    if (refCircuit)
        ret |= AtChannelTimingSet(refCircuit, cAtTimingModeSys, NULL);

    if (ret != cAtOk)
        return ret;

    if (ThaPwCESoPAdapterWithCas((ThaPwCESoPAdapter)self))
        ret |= ThaModuleClaCESoPCASLengthSet(ModuleCla(self), self, 0);

    /* Super */
    return m_AtPwMethods->CircuitUnbind(self);
    }

static uint16 PdaMaxPayloadSize(ThaPwAdapter self, ThaModulePda pdaModule)
    {
    return ThaModulePdaCESoPMaxPayloadSize(pdaModule, (AtPw)self);
    }

static uint16 PdaMinPayloadSize(ThaPwAdapter self, ThaModulePda pdaModule)
    {
    return ThaModulePdaCESoPMinPayloadSize(pdaModule, (AtPw)self);
    }

static uint32 TxForcibleAlarmsGet(AtChannel self)
    {
	AtUnused(self);
    return cAtPwAlarmTypeLBit | cAtPwAlarmTypeRBit | cAtPwAlarmTypeMBit;
    }

static uint8 CircuitSliceByModule(ThaPwAdapter self, AtChannel circuit, eAtModule module)
    {
    uint8 sliceId, hwIdInSlice;
    AtUnused(self);
    ThaPdhChannelHwIdGet((AtPdhChannel)AtPdhNxDS0De1Get((AtPdhNxDS0)circuit), module, &sliceId, &hwIdInSlice);
    return sliceId;
    }

static uint8 PdaCircuitSlice(ThaPwAdapter self, AtChannel circuit)
    {
    return CircuitSliceByModule(self, circuit, cThaModulePda);
    }

static uint8 CdrCircuitSlice(ThaPwAdapter self, AtChannel circuit)
    {
    return CircuitSliceByModule(self, circuit, cThaModuleCdr);
    }

static uint8 PweCircuitSlice(ThaPwAdapter self, AtChannel circuit)
    {
    return CircuitSliceByModule(self, circuit, cThaModulePwe);
    }

static uint32 MinJitterDelay(AtPw self, AtChannel circuit, uint16 payloadSize)
    {
    return ThaModulePdaCESoPMinJitterDelay(ModulePda(self), self, circuit, payloadSize);
    }

static uint32 MinJitterBufferSize(AtPw self, AtChannel circuit, uint16 payloadSize)
    {
    return ThaModulePdaCESoPMinJitterBufferSize(ModulePda(self), self, circuit, payloadSize);
    }

static eAtRet TxAlarmForce(AtChannel self, uint32 alarmType)
    {
    uint32 currentForcedAlarm = AtChannelTxForcedAlarmGet(self);

    if (((currentForcedAlarm & cAtPwAlarmTypeLBit) && (alarmType & cAtPwAlarmTypeMBit)) ||
        ((currentForcedAlarm & cAtPwAlarmTypeMBit) && (alarmType & cAtPwAlarmTypeLBit)))
        {
        uint32 unforcedAlarms = cAtPwAlarmTypeLBit | cAtPwAlarmTypeMBit;
        AtChannelTxAlarmUnForce(self, unforcedAlarms);
        return m_AtChannelMethods->TxAlarmForce(self, alarmType);
        }

    return m_AtChannelMethods->TxAlarmForce(self, alarmType);
    }

static uint32 CASExtraJitterBufferGet(ThaPwAdapter self)
    {
    AtPdhNxDS0 nxds0 = (AtPdhNxDS0)AtPwBoundCircuitGet((AtPw)self);
    AtPdhDe1 de1;

    /* In case PW unbound circuit, let return the maximum extra jitter buffer
     * to satisfy all circuit bounding later. */
    if (nxds0 == NULL)
        return 3000U;

    de1 = AtPdhNxDS0De1Get(nxds0);
    if (AtPdhDe1IsE1(de1))
        return 2000U; /* 16x 125us */;

    return 3000U; /* 24x 125us */
    }

static uint32 ExtraJitterBufferGet(ThaPwAdapter self)
    {
    if (ThaPwCESoPAdapterWithCas((ThaPwCESoPAdapter)self))
        return CASExtraJitterBufferGet(self);

    return 125U;
    }

static void MethodsInit(ThaPwCESoPAdapter self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, CwAutoTxMBitEnable);
        mMethodOverride(m_methods, CwAutoTxMBitIsEnabled);
        mMethodOverride(m_methods, CwAutoRxMBitEnable);
        mMethodOverride(m_methods, CwAutoRxMBitIsEnabled);
        mMethodOverride(m_methods, CwAutoRxMBitIsConfigurable);
        mMethodOverride(m_methods, ModeGet);
        mMethodOverride(m_methods, CasIdleCode1Set);
        mMethodOverride(m_methods, CasIdleCode1Get);
        mMethodOverride(m_methods, CasIdleCode2Set);
        mMethodOverride(m_methods, CasIdleCode2Get);
        mMethodOverride(m_methods, CasAutoIdleEnable);
        mMethodOverride(m_methods, CasAutoIdleIsEnabled);
        mMethodOverride(m_methods, RxCasModeSet);
        mMethodOverride(m_methods, RxCasModeGet);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtChannel(ThaPwCESoPAdapter self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, TypeString);
        mMethodOverride(m_AtChannelOverride, AllCountersGet);
        mMethodOverride(m_AtChannelOverride, AllCountersClear);
        mMethodOverride(m_AtChannelOverride, TxForcibleAlarmsGet);
        mMethodOverride(m_AtChannelOverride, TxAlarmForce);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideThaPwAdapter(ThaPwCESoPAdapter self)
    {
    ThaPwAdapter adapter = (ThaPwAdapter)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPwAdapterMethods = mMethodsGet(adapter);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwAdapterOverride, m_ThaPwAdapterMethods, sizeof(m_ThaPwAdapterOverride));

        mMethodOverride(m_ThaPwAdapterOverride, CountersCreate);
        mMethodOverride(m_ThaPwAdapterOverride, SetDefaultOnBinding);
        mMethodOverride(m_ThaPwAdapterOverride, JitterPayloadLengthSet);
        mMethodOverride(m_ThaPwAdapterOverride, NumDs0Slots);
        mMethodOverride(m_ThaPwAdapterOverride, CdrController);
        mMethodOverride(m_ThaPwAdapterOverride, PayloadSizeInBytes);
        mMethodOverride(m_ThaPwAdapterOverride, DCRRxNcoConfPackLen);
        mMethodOverride(m_ThaPwAdapterOverride, CDRTimingPktLenInBits);
        mMethodOverride(m_ThaPwAdapterOverride, DefaultPayloadSize);
        mMethodOverride(m_ThaPwAdapterOverride, PartOfCircuit);
        mMethodOverride(m_ThaPwAdapterOverride, HwPayloadSizeSet);
        mMethodOverride(m_ThaPwAdapterOverride, HwPayloadSizeGet);
        mMethodOverride(m_ThaPwAdapterOverride, PdaMaxPayloadSize);
        mMethodOverride(m_ThaPwAdapterOverride, PdaMinPayloadSize);
        mMethodOverride(m_ThaPwAdapterOverride, PdaCircuitSlice);
        mMethodOverride(m_ThaPwAdapterOverride, PweCircuitSlice);
        mMethodOverride(m_ThaPwAdapterOverride, CdrCircuitSlice);
        mMethodOverride(m_ThaPwAdapterOverride, AdapterMinPayloadSize);
        mMethodOverride(m_ThaPwAdapterOverride, PayloadSizeIsValid);
        mMethodOverride(m_ThaPwAdapterOverride, ExtraJitterBufferGet);
        }

    mMethodsSet(adapter, &m_ThaPwAdapterOverride);
    }

static void OverrideAtPw(ThaPwCESoPAdapter self)
    {
    AtPw pw = (AtPw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPwMethods = mMethodsGet(pw);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPwOverride, m_AtPwMethods, sizeof(m_AtPwOverride));

        mMethodOverride(m_AtPwOverride, MinPayloadSize);
        mMethodOverride(m_AtPwOverride, MaxPayloadSize);
        mMethodOverride(m_AtPwOverride, PayloadSizeSet);
        mMethodOverride(m_AtPwOverride, CircuitBind);
        mMethodOverride(m_AtPwOverride, CircuitUnbind);
        mMethodOverride(m_AtPwOverride, MinJitterDelay);
        mMethodOverride(m_AtPwOverride, MinJitterBufferSize);
        }

    mMethodsSet(pw, &m_AtPwOverride);
    }

static void Override(ThaPwCESoPAdapter self)
    {
    OverrideAtChannel(self);
    OverrideThaPwAdapter(self);
    OverrideAtPw(self);
    }

ThaPwCESoPAdapter ThaPwCESoPAdapterObjectInit(ThaPwCESoPAdapter self, AtPw pw)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPwAdapterObjectInit((ThaPwAdapter)self, pw) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwAdapter ThaPwCESoPAdapterNew(AtPw pw)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwCESoPAdapter newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return (ThaPwAdapter)ThaPwCESoPAdapterObjectInit(newController, pw);
    }

eBool ThaPwCESoPAdapterWithCas(ThaPwCESoPAdapter self)
    {
    AtPw pw = ThaPwAdapterPwGet((ThaPwAdapter)self);

    if (AtPwCESoPModeGet((AtPwCESoP)pw) == cAtPwCESoPModeWithCas)
        return cAtTrue;

    return cAtFalse;
    }

static eBool CASPayloadSizeInFrameIsValid(ThaPwCESoPAdapter self, AtChannel circuit, uint16 numFramesPerPacket)
    {
    AtPdhNxDS0 nxds0 = (AtPdhNxDS0)circuit;
    AtPdhDe1 de1 = AtPdhNxDS0De1Get(nxds0);
    static uint8 casCesopNumE1Frames[]  = {4, 8, 16};
    static uint8 casCesopNumDs1Frames[] = {4, 8, 12, 24};
    uint8 i;
    AtUnused(self);

    if (AtPdhDe1IsE1(de1))
        {
        for (i = 0; i < mCount(casCesopNumE1Frames); i++)
            {
            if (numFramesPerPacket == casCesopNumE1Frames[i])
                return cAtTrue;
            }
        return cAtFalse;
        }

    for (i = 0; i < mCount(casCesopNumDs1Frames); i++)
        {
        if (numFramesPerPacket == casCesopNumDs1Frames[i])
            return cAtTrue;
        }

    return cAtFalse;
    }

eBool ThaPwCESoPAdapterPayloadSizeInFrameIsValid(ThaPwCESoPAdapter self, AtChannel circuit, uint16 numFramesPerPacket)
    {
    AtPdhNxDS0 nxds0;
    uint8 numTimeSlot;
    AtUnused(self);

    if (ThaPwCESoPAdapterWithCas(self))
        return CASPayloadSizeInFrameIsValid(self, circuit, numFramesPerPacket);

    nxds0 = (AtPdhNxDS0)circuit;
    numTimeSlot = AtPdhNxDs0NumTimeslotsGet(nxds0);
    if ((numFramesPerPacket * numTimeSlot) >= 8)
        return cAtTrue;

    return cAtFalse;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : ThaPw.c
 *
 * Created Date: Nov 20, 2012
 *
 * Description : Thalassa PW implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPwCep.h"
#include "AtNumber.h"
#include "../../../../generic/sur/AtModuleSurInternal.h"
#include "ThaPwAdapterInternal.h"
#include "../../pwe/ThaModulePweInternal.h"
#include "../../cdr/ThaModuleCdrStm.h"
#include "../../sdh/ThaSdhVcInternal.h"
#include "../../sdh/ThaModuleSdh.h"
#include "../../cdr/controllers/ThaCdrController.h"
#include "../../cla/hbce/ThaHbce.h"
#include "../../sur/hard/ThaModuleHardSur.h"
#include "../activator/ThaPwActivator.h"
#include "../ThaPwUtil.h"
#include "../headercontrollers/ThaPwHeaderControllerInternal.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../prbs/ThaPrbsEngineInternal.h"
#include "../ethportbinder/ThaPwEthPortBinder.h"
#include "../defectcontrollers/ThaPwDefectController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mDevice(self) AtChannelDeviceGet((AtChannel)self)
#define mInAccessible(self) AtChannelInAccessible((AtChannel)self)
#define mShouldPreventReconfigure(self) AtChannelShouldPreventReconfigure((AtChannel)self)
#define mPrintField(self, fieldName) AtPrintf("* %s: %d\r\n", #fieldName, self->fieldName);
#define mPrintFieldUint64(self, fieldName) AtPrintf("* %s: %lld\r\n", #fieldName, self->fieldName);
#define mThis(self) ((ThaPwAdapter)self)

#define mEncodeVlan(fieldName)                                                 \
    do                                                                         \
        {                                                                      \
        AtCoderEncodeUInt(encoder, object->fieldName.priority, #fieldName".priority");  \
        AtCoderEncodeUInt(encoder, object->fieldName.cfi,      #fieldName".cfi");       \
        AtCoderEncodeUInt(encoder, object->fieldName.vlanId,   #fieldName".vlanId");    \
        }while(0)

#define mEncodeVlanCache(fieldName)                                            \
    do                                                                         \
        {                                                                      \
        AtCoderEncodeUInt(encoder, cache->fieldName.priority, "cache."#fieldName".priority");\
        AtCoderEncodeUInt(encoder, cache->fieldName.cfi,      "cache."#fieldName".cfi");     \
        AtCoderEncodeUInt(encoder, cache->fieldName.vlanId,   "cache."#fieldName".vlanId");  \
        }while(0)

#define mCacheEncodeUInt(fieldName) AtCoderEncodeUInt(encoder, cache->fieldName, "cache."#fieldName)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaPwAdapterMethods m_methods;

/* Override */
static tAtChannelMethods m_AtChannelOverride;
static tAtPwMethods      m_AtPwOverride;
static tAtObjectMethods  m_AtObjectOverride;

/* Save super's implementation */
static const tAtChannelMethods *m_AtChannelMethods = NULL;
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
static const tAtPwMethods      *m_AtPwMethods      = NULL;

/*--------------------------- Forward declarations ---------------------------*/
static eAtRet JitterBufferSizeDefaultSet(ThaPwAdapter self);
static eAtRet JitterBufferDelayDefaultSet(ThaPwAdapter self);

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaPwAdapter);
    }

static tThaPwConfigCache *CacheCreate(ThaPwAdapter self)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 memorySize = sizeof(tThaPwConfigCache);
    tThaPwConfigCache *newCache = mMethodsGet(osal)->MemAlloc(osal, memorySize);
	AtUnused(self);
    if (newCache == NULL)
        return NULL;

    /* Clear memory */
    mMethodsGet(osal)->MemInit(osal, newCache, 0, memorySize);

    return newCache;
    }

static ThaModulePda ModulePda(AtPw self)
    {
    return (ThaModulePda)AtDeviceModuleGet(mDevice(self), cThaModulePda);
    }

static ThaModuleCos ModuleCos(AtPw self)
    {
    return (ThaModuleCos)AtDeviceModuleGet(mDevice(self), cThaModuleCos);
    }

static ThaModuleClaPw ModuleCla(AtPw self)
    {
    return (ThaModuleClaPw)AtDeviceModuleGet(mDevice(self), cThaModuleCla);
    }

static ThaModulePwe ModulePwe(AtPw self)
    {
    return (ThaModulePwe)AtDeviceModuleGet(mDevice(self), cThaModulePwe);
    }

static ThaModulePw ModulePw(AtPw self)
    {
    return (ThaModulePw)AtDeviceModuleGet(mDevice(self), cAtModulePw);
    }

static ThaModuleEth ModuleEth(AtPw self)
    {
    return (ThaModuleEth)AtDeviceModuleGet(mDevice(self), cAtModuleEth);
    }

static ThaModuleHardSur ModuleSur(AtPw self)
    {
    return (ThaModuleHardSur)AtDeviceModuleGet(mDevice(self), cAtModuleSur);
    }

static const char *TypeString(AtChannel self)
    {
	AtUnused(self);
    return "pw_adapter";
    }

static uint32 IdGet(AtChannel self)
    {
    ThaModulePw pwModule = ModulePw((AtPw)self);
    if ((!ThaModulePwDynamicPwAllocation(pwModule)) ||
        (mAdapter(self)->hwPw == NULL))
        return m_AtChannelMethods->IdGet(self);

    return ThaHwPwIdGet(mAdapter(self)->hwPw);
    }

static uint32 HwIdGet(AtChannel self)
    {
    return ThaModulePwLocalPwId(ModulePw((AtPw)self), (AtPw)self);
    }

static uint32 DefaultOffset(ThaPwAdapter self)
    {
    return AtChannelIdGet((AtChannel)self);
    }

static ThaHbce Hbce(AtPw self)
    {
    AtChannel circuit;
    AtEthPort ethPort;
    ThaPwAdapter adapter = mAdapter(self);
    ThaClaPwController claController = ThaPwAdapterClaPwController(adapter);

    if ((claController == NULL) || (adapter == NULL))
        return NULL;

    if (!ThaPwAdapterIsLogicPw(adapter))
        return ThaClaPwControllerHbceGet(claController, self);

    /* Try determining by circuit */
    circuit = AtPwBoundCircuitGet(self);
    if (circuit)
        return ThaClaPwControllerHbceForPart(claController, ThaPwAdapterPartOfCircuit(adapter, circuit));

    /* Try determining by ETH port */
    ethPort = AtPwEthPortGet(self);
    if (ethPort)
        return ThaClaPwControllerHbceForPart(claController, ThaModuleEthPartOfPort(ModuleEth(self), ethPort));

    /* Cannot determine PW part, just ask CLA controller for default operation */
    return ThaClaPwControllerHbceGet(claController, self);
    }

static AtPwCounters CountersCreate(ThaPwAdapter self)
    {
	AtUnused(self);
    return AtPwCountersNew();
    }

static eAtRet HwPwTypeSet(ThaPwAdapter self)
    {
    eAtRet ret = cAtOk;
    AtPw pw = (AtPw)self;
    ThaModulePwe pweModule = ModulePwe(pw);
    eAtPwType pwType = AtPwTypeGet(pw);

    ret |= ThaModulePwePwTypeSet(pweModule, pw, pwType);
    ret |= ThaClaPwControllerPwTypeSet(ThaPwAdapterClaPwController(self), pw, pwType);
    ret |= ThaPwDefectControllerPwTypeSet(ThaPwAdapterPwDefectControllerGet(self), pwType);

    return ret;
    }

static uint32 LopsThresholdMin(AtPw self)
    {
    return ThaModulePdaPwLopsThresholdMin(ModulePda(self), self);
    }

static uint32 LopsThresholdMax(AtPw self)
    {
    return ThaModulePdaPwLopsThresholdMax(ModulePda(self), self);
    }

static uint32 DefaultJitterDelay(ThaPwAdapter self)
    {
    AtPw pw = (AtPw)self;
    return AtPwMinJitterDelay(pw, AtPwBoundCircuitGet(pw), AtPwPayloadSizeGet(pw));
    }

static uint32 DefaultJitterBufferSize(ThaPwAdapter self)
    {
    AtPw pw = (AtPw)self;
    return ThaModulePdaDefaultJitterBufferSize(ModulePda(pw), pw, mMethodsGet(self)->DefaultJitterDelay(self));
    }

static const char *IdString(AtChannel self)
    {
    static char idBuf[64];
    AtPw pw;

    if (!ThaModulePwDynamicPwAllocation(ModulePw((AtPw)self)))
        return m_AtChannelMethods->IdString(self);

    pw = ThaPwAdapterPwGet(mAdapter(self));
    AtSprintf(idBuf, "%s", m_AtChannelMethods->IdString(self));
    AtSprintf(idBuf, "%s(%s.%s)",
              idBuf,
              AtChannelTypeString((AtChannel)pw),
              AtChannelIdString((AtChannel)pw));
    return idBuf;
    }

static eAtRet JitterBufferSizeInPacketDefaultSet(ThaPwAdapter self)
    {
    eAtRet ret = cAtOk;
    uint16 jitterBuffer = self->jitterBufferSizeInPkt;

    /* Can not configure when number of packets is 0, switch to configure by us */
    if (jitterBuffer == 0)
        {
        AtChannelLog((AtChannel)self,
                     cAtLogLevelCritical, AtSourceLocation,
                     "Cannot configure jitter buffer size 0 packet, try default configuration in microsecond\r\n");
        self->jitterSizeIsInPkt = cAtFalse;
        return JitterBufferSizeDefaultSet(self);
        }

    /* If existing configuration cannot be applied, try the default configuration in microsecond */
    ret |= AtPwJitterBufferSizeInPacketSet((AtPw)self, jitterBuffer);
    if (ret != cAtOk)
        {
        AtChannelLog((AtChannel)self, cAtLogLevelCritical, AtSourceLocation,
                     "Cannot applied jitter buffer size (%u pkt), try default configuration in microsecond, ret = %s\r\n",
                     jitterBuffer, AtRet2String(ret));

        self->jitterSizeIsInPkt = cAtFalse;
        return JitterBufferSizeDefaultSet(self);
        }

    return cAtOk;
    }

static eAtRet JitterBufferDelayInPacketDefaultSet(ThaPwAdapter self)
    {
    eAtRet ret = cAtOk;
    uint16 jitterDelay = self->jitterBufferDelayInPkt;

    /* Can not configure when number of packets is 0, switch to configure by us */
    if (jitterDelay == 0)
        {
        AtChannelLog((AtChannel)self, cAtLogLevelCritical, AtSourceLocation,
                    "Cannot configure jitter buffer delay 0 packet, try default configuration in microsecond\r\n");
        self->jitterDelayIsInPkt = cAtFalse;
        return JitterBufferDelayDefaultSet(self);
        }

    /* If existing configuration cannot be applied, try the default configuration */
    ret |= AtPwJitterBufferDelayInPacketSet((AtPw)self, jitterDelay);
    if (ret != cAtOk)
        {
        AtChannelLog((AtChannel)self, cAtLogLevelCritical, AtSourceLocation,
                     "Cannot applied jitter buffer delay (%d pkt), try default configuration in microsecond, ret = %s\r\n",
                     jitterDelay, AtRet2String(ret));
        self->jitterDelayIsInPkt = cAtFalse;
        return JitterBufferDelayDefaultSet(self);
        }

    return ret;
    }

static eAtRet JitterBufferSizeInUsDefaultSet(ThaPwAdapter self)
    {
    eAtRet ret = cAtOk;
    uint32 jitterBuffer = self->jitterBufferSizeInUs;

    /* Make default configuration */
    if (jitterBuffer == 0)
        jitterBuffer = mMethodsGet(self)->DefaultJitterBufferSize(self);

    /* If existing configuration cannot be applied, try the default configuration */
    ret |= AtPwJitterBufferSizeSet((AtPw)self, jitterBuffer);
    if (ret != cAtOk)
        {
        AtChannelLog((AtChannel)self,
                     cAtLogLevelCritical, AtSourceLocation,
                     "Cannot applied jitter buffer size (%d us), try default configuration, ret = %s\r\n",
                     jitterBuffer, AtRet2String(ret));
        ret = AtPwJitterBufferSizeSet((AtPw)self, mMethodsGet(self)->DefaultJitterBufferSize(self));
        }

    return ret;
    }

static eAtRet JitterBufferDelayInUsDefaultSet(ThaPwAdapter self)
    {
    eAtRet ret = cAtOk;
    uint32 jitterDelay;

    jitterDelay = self->jitterBufferDelayInUs;

    /* Make default configuration */
    if (jitterDelay == 0)
        jitterDelay = mMethodsGet(self)->DefaultJitterDelay(self);

    /* If existing configuration cannot be applied, try the default configuration */
    ret |= AtPwJitterBufferDelaySet((AtPw)self, jitterDelay);

    if (ret != cAtOk)
        {
        AtChannelLog((AtChannel)self, cAtLogLevelCritical, AtSourceLocation,
                     "Cannot applied jitter buffer delay (%d us), try default configuration, ret = %s\r\n",
                     jitterDelay, AtRet2String(ret));
        ret  = AtPwJitterBufferDelaySet((AtPw)self, mMethodsGet(self)->DefaultJitterDelay(self));
        }

    return ret;
    }

static eAtRet JitterBufferSizeDefaultSet(ThaPwAdapter self)
    {
    if (self->jitterSizeIsInPkt)
        return JitterBufferSizeInPacketDefaultSet(self);

    return JitterBufferSizeInUsDefaultSet(self);
    }

static eAtRet JitterBufferDelayDefaultSet(ThaPwAdapter self)
    {
    if (self->jitterDelayIsInPkt)
        return JitterBufferDelayInPacketDefaultSet(self);
    return JitterBufferDelayInUsDefaultSet(self);
    }

static eBool BufferSizeIsInRange(AtPw self, uint32 bufferSizeUs, uint16 payloadSize)
    {
    uint32 minBufferSizeUs = AtPwMinJitterBufferSize(self, AtPwBoundCircuitGet(self), payloadSize);
    uint32 maxBufferSizeUs = AtPwMaxJitterBufferSize(self, AtPwBoundCircuitGet(self), payloadSize);

    return (((bufferSizeUs < minBufferSizeUs) || (bufferSizeUs > maxBufferSizeUs)) ? cAtFalse : cAtTrue);
    }

static eBool JitterBufferIsEnough(AtPw self, uint16 payloadSizeInBytes)
    {
    uint32 jitterBufferUs = AtPwJitterBufferSizeGet(self);

    if (jitterBufferUs == 0)
        return cAtTrue;

    if ((jitterBufferUs >= AtPwMinJitterBufferSize(self, AtPwBoundCircuitGet(self), payloadSizeInBytes)) &&
        (jitterBufferUs <= AtPwMaxJitterBufferSize(self, AtPwBoundCircuitGet(self), payloadSizeInBytes)))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet HwPayloadSizeSet(ThaPwAdapter self, uint16 payloadSizeInBytes)
    {
    uint32 lopsThreshold = 0;
    eAtRet ret = cAtOk;
    AtPw pw = (AtPw)self;
    ThaModulePwe pweModule = ModulePwe(pw);

    lopsThreshold = AtPwLopsSetThresholdGet(pw);
    ret |= ThaModulePwePayloadSizeSet(pweModule, pw, payloadSizeInBytes);
    ret |= ThaClaPwControllerPayloadSizeSet(ThaPwAdapterClaPwController(self), pw, payloadSizeInBytes);
    ret |= mMethodsGet(self)->CdrPayloadUpdate(self);

    if ((ret != cAtOk) || (lopsThreshold == 0))
        return ret;

    /* Reconfigure LOPS value to update HW field */
    return AtPwLopsSetThresholdSet((AtPw)ThaPwAdapterPwDefectControllerGet(self), lopsThreshold);
    }

static ThaPwEthPortBinder EthPortBinder(AtPw self)
    {
    return ThaModulePwEthPortBinder(ModulePw(self));
    }

static eAtRet JitterBufferInUsAndPayloadSizeCache(ThaPwAdapter adapter, uint32 jitterBufferSize_us, uint32 jitterDelay_us, uint16 payloadSize)
    {
    adapter->jitterBufferSizeInUs  = jitterBufferSize_us;
    adapter->jitterBufferDelayInUs = jitterDelay_us;
    adapter->payloadSize           = payloadSize;

    return cAtOk;
    }

static eBool LimitResourceIsEnabled(AtPw self)
    {
    ThaModulePw  pwModule = (ThaModulePw)AtChannelModuleGet((AtChannel)self);
    return ThaModulePwResourcesLimitationIsDisabled(pwModule) ? cAtFalse : cAtTrue;
    }

static eAtModulePwRet JitterBufferAndPayloadSizeSet(AtPw self, uint32 jitterBufferSize_us, uint32 jitterDelay_us, uint16 payloadSize)
    {
    eAtRet ret = cAtOk;
    ThaPwAdapter adapter = mAdapter(self);
    ThaModulePda pdaModule = ModulePda(self);
    uint32 pdaMaxPayloadSize, pdaMinPayloadSize, payloadSizeInByte = 0;
    uint32 newBufferSizeInPkt, newJitterDelayInPkt;
    eBool sizeInPktIsSame = cAtFalse;
    eBool delayInPktIsSame = cAtFalse;
    uint64 newBw;
    uint32 newOccupiedBlock;
    eBool limitResourceEnabled;

    if ((jitterBufferSize_us == 0) || (jitterDelay_us == 0) || (payloadSize == 0))
        return cAtErrorInvlParm;

    if (!ThaModulePdaPwJitterBufferAndJitterDelayAreValid(ModulePda(self), self, jitterBufferSize_us, jitterDelay_us))
        return cAtErrorInvlParm;

    if (!ThaPwAdatperPayloadSizeIsValid(adapter, payloadSize))
        return cAtErrorInvlParm;

    /* Application want to configure jitter buffer in microsecond unit */
    adapter->jitterSizeIsInPkt  = cAtFalse;
    adapter->jitterDelayIsInPkt = cAtFalse;

    /* PW is still logical, just save database */
    if (ThaPwAdapterIsLogicPw(adapter) || (AtPwBoundCircuitGet(self) == NULL))
        return JitterBufferInUsAndPayloadSizeCache(adapter, jitterBufferSize_us, jitterDelay_us, payloadSize);

    /* Always do constrains checking if any parameter is changed. */
    if (mShouldPreventReconfigure(self) &&
        (AtPwPayloadSizeGet(self) == payloadSize) &&
        (AtPwJitterBufferSizeGet(self) == jitterBufferSize_us) &&
        (AtPwJitterBufferDelayGet(self) == jitterDelay_us))
        return JitterBufferInUsAndPayloadSizeCache(adapter, jitterBufferSize_us, jitterDelay_us, payloadSize);

    /* Check all of constrains before applying */
    limitResourceEnabled = LimitResourceIsEnabled(self);
    if (limitResourceEnabled && !BufferSizeIsInRange(self, jitterBufferSize_us, payloadSize))
        return cAtErrorOutOfRangParm;

    newBufferSizeInPkt = ThaModulePdaCalculateNumPacketsByUs(pdaModule, self, jitterBufferSize_us);
    if (newBufferSizeInPkt == ThaModulePdaPwJitterBufferSizeInPacketGet(pdaModule, self))
        sizeInPktIsSame = cAtTrue;

    newJitterDelayInPkt = ThaModulePdaCalculateNumPacketsByUs(pdaModule, self, jitterDelay_us);
    if (newJitterDelayInPkt == ThaModulePdaPwJitterBufferDelayInPacketGet(pdaModule, self))
        delayInPktIsSame = cAtTrue;

    if (mShouldPreventReconfigure(self) && (AtPwPayloadSizeGet(self) == payloadSize) && sizeInPktIsSame && delayInPktIsSame)
        return JitterBufferInUsAndPayloadSizeCache(adapter, jitterBufferSize_us, jitterDelay_us, payloadSize);

    pdaMaxPayloadSize = AtPwMaxPayloadSize(self, AtPwBoundCircuitGet(self), jitterBufferSize_us);
    pdaMinPayloadSize = AtPwMinPayloadSize(self, AtPwBoundCircuitGet(self), jitterBufferSize_us);
    if (limitResourceEnabled && ((payloadSize > pdaMaxPayloadSize) || (payloadSize < pdaMinPayloadSize)))
        return cAtErrorOutOfRangParm;

    /* Check if bandwidth is acceptable */
    newBw = ThaPwAdapterBandwidthCalculateWithPayload(self, payloadSize);
    if (limitResourceEnabled && !ThaPwEthPortBinderPwBandwidthIsAcceptable(EthPortBinder(self), self, newBw))
        return cAtErrorBandwidthExceeded;

    /* Check if PDA block is available for new payload size */
    payloadSizeInByte = ThaPwUtilPayloadSizeInBytes(self, AtPwBoundCircuitGet(self), payloadSize);
    newOccupiedBlock = ThaModulePdaPwNumBlockByBufferInUs(pdaModule, self, jitterBufferSize_us, payloadSizeInByte);
    if (limitResourceEnabled && !ThaModulePdaResourceIsAvailable(ModulePda(self), self, newOccupiedBlock))
        return cAtErrorRsrcNoAvail;

    /* Apply */
    ThaPwAdapterStartRemoving(self);
    ret |= ThaPwAdapterHwPayloadSizeSet(adapter, payloadSize);
    ret |= mMethodsGet(adapter)->JitterPayloadLengthSet(adapter);
    ret |= ThaModulePdaPwJitterBufferSizeSet(pdaModule, self, jitterBufferSize_us);
    ret |= ThaModulePdaPwJitterBufferDelaySet(pdaModule, self, jitterDelay_us);
    ThaPwAdapterFinishRemoving(self);
    mChannelSuccessAssert(self, ret);

    /* Save database and also need to know jitter buffer information in packet unit */
    JitterBufferInUsAndPayloadSizeCache(adapter, jitterBufferSize_us, jitterDelay_us, payloadSize);
    adapter->jitterBufferSizeInPkt  = (uint16)newBufferSizeInPkt;
    adapter->jitterBufferDelayInPkt = (uint16)newJitterDelayInPkt;

    ret  = ThaPwEthPortBinderPwBandwidthUpdate(EthPortBinder(self), self, newBw);
    ret |= ThaModulePdaTotalInUsedRamBlockUpdate(pdaModule, self, newOccupiedBlock);
    return ret;
    }

static eBool JitterBufferSizeInPacketIsInRange(AtPw self, uint32 numPackets, uint32 payloadSize)
    {
    uint32 jbInNumUs;
    ThaModulePda pdaModule = ModulePda(self);
    uint32 min = ThaModulePdaMinNumPacketsForJitterBufferSize(pdaModule, self, payloadSize);
    uint32 max = ThaModulePdaMaxNumPacketsForJitterBufferSize(pdaModule, self, payloadSize);

    if ((numPackets < min) || (numPackets > max))
        return cAtFalse;

    jbInNumUs = ThaModulePdaCalculateUsByNumberOfPacket(pdaModule, self, numPackets);
    min = ThaModulePdaMinNumMicrosecondsForJitterBufferSize(pdaModule, self, payloadSize);
    max = ThaModulePdaMaxNumMicrosecondsForJitterBufferSize(pdaModule, self, payloadSize);
    
    return ((jbInNumUs < min) || (jbInNumUs > max)) ? cAtFalse : cAtTrue;
    }

static eAtRet JitterBufferInPacketAndPayloadSizeCache(ThaPwAdapter adapter, uint32 jitterBufferSize_pkt, uint32 jitterDelay_pkt, uint16 payloadSize)
    {
    adapter->jitterBufferSizeInPkt  = (uint16)jitterBufferSize_pkt;
    adapter->jitterBufferDelayInPkt = (uint16)jitterDelay_pkt;
    adapter->payloadSize            = payloadSize;
    return cAtOk;
    }

static eAtModulePwRet JitterBufferInPacketAndPayloadSizeSet(AtPw self, uint32 jitterBufferSize_pkt, uint32 jitterDelay_pkt, uint16 payloadSize)
    {
    eAtRet ret = cAtOk;
    ThaPwAdapter adapter = mAdapter(self);
    ThaModulePda pdaModule = ModulePda(self);
    uint64 newBw;
    uint32 newOccupiedBlock, payloadSizeInByte = 0;
    eBool limitResourceEnabled;

    if ((jitterBufferSize_pkt == 0) || (jitterDelay_pkt == 0) || (payloadSize == 0))
        return cAtErrorInvlParm;

    if (jitterBufferSize_pkt < jitterDelay_pkt)
        return cAtErrorInvlParm;

    if (!ThaPwAdatperPayloadSizeIsValid(adapter, payloadSize))
        return cAtErrorInvlParm;

    /* Application want to configure jitter buffer in packet unit */
    adapter->jitterSizeIsInPkt  = cAtTrue;
    adapter->jitterDelayIsInPkt = cAtTrue;

    /* PW is still logical, just save database */
    if (ThaPwAdapterIsLogicPw(adapter) || (AtPwBoundCircuitGet(self) == NULL))
        return JitterBufferInPacketAndPayloadSizeCache(adapter, jitterBufferSize_pkt, jitterDelay_pkt, payloadSize);

    if (mShouldPreventReconfigure(self) &&
        (AtPwJitterBufferSizeInPacketGet(self)  == jitterBufferSize_pkt) &&
        (AtPwJitterBufferDelayInPacketGet(self) == jitterDelay_pkt)      &&
        (AtPwPayloadSizeGet(self)               == payloadSize))
        return JitterBufferInPacketAndPayloadSizeCache(adapter, jitterBufferSize_pkt, jitterDelay_pkt, payloadSize);

    /* Check all of constrains before applying */
    limitResourceEnabled = LimitResourceIsEnabled(self);
    if (limitResourceEnabled && !JitterBufferSizeInPacketIsInRange(self, jitterBufferSize_pkt, payloadSize))
        return cAtErrorOutOfRangParm;

    /* Check if bandwidth is acceptable */
    newBw = ThaPwAdapterBandwidthCalculateWithPayload(self, payloadSize);
    if (limitResourceEnabled && !ThaPwEthPortBinderPwBandwidthIsAcceptable(EthPortBinder(self), self, newBw))
        return cAtErrorBandwidthExceeded;

    /* Check if PDA block is available for new payload size */
    payloadSizeInByte = ThaPwUtilPayloadSizeInBytes(self, AtPwBoundCircuitGet(self), payloadSize);
    newOccupiedBlock = ThaModulePdaPwNumBlockByBufferInPkt(pdaModule, self, jitterBufferSize_pkt, payloadSizeInByte);
    if (limitResourceEnabled && !ThaModulePdaResourceIsAvailable(ModulePda(self), self, newOccupiedBlock))
        return cAtErrorRsrcNoAvail;

    /* Apply */
    ThaPwAdapterStartCenterJitterBuffer(self);
    ret |= ThaPwAdapterHwPayloadSizeSet(adapter, payloadSize);
    ret |= mMethodsGet(adapter)->JitterPayloadLengthSet(adapter);
    ret |= ThaModulePdaPwJitterBufferSizeInNumOfPacketSet(pdaModule, self, jitterBufferSize_pkt);
    ret |= ThaModulePdaPwJitterBufferDelayInNumOfPacketSet(pdaModule, self, jitterDelay_pkt);
    ThaPwAdapterStopCenterJitterBuffer(self);
    mChannelSuccessAssert(self, ret);

    /* Save database and also need to know jitter buffer information in microsecond unit */
    JitterBufferInPacketAndPayloadSizeCache(adapter, jitterBufferSize_pkt, jitterDelay_pkt, payloadSize);
    adapter->jitterBufferSizeInUs   = ThaModulePdaCalculateUsByNumberOfPacket(pdaModule, self, jitterBufferSize_pkt);
    adapter->jitterBufferDelayInUs  = ThaModulePdaCalculateUsByNumberOfPacket(pdaModule, self, jitterDelay_pkt);

    ret = ThaPwEthPortBinderPwBandwidthUpdate(EthPortBinder(self), self, newBw);
    ret |= ThaModulePdaTotalInUsedRamBlockUpdate(pdaModule, self, newOccupiedBlock);
    return ret;
    }

static eAtModulePwRet PayloadSizeSet(AtPw self, uint16 payloadSizeInBytes)
    {
    eAtRet ret = cAtOk;
    ThaModulePda pdaModule = ModulePda(self);
    uint64 newBw;
    uint32 jitterBufferInUs, newOccupiedBlock;
    eBool limitResourceEnabled;

    /* Payload size not change */
    if (mShouldPreventReconfigure(self) && ((AtPwPayloadSizeGet(self) == payloadSizeInBytes)))
        {
        /* Just for standby driver, without this, if hardware configuration is
         * same as the input value, the value in database will be always 0 */
        mAdapter(self)->payloadSize = payloadSizeInBytes;
        return cAtOk;
        }

    /* Just cache when PW has not been activated */
    limitResourceEnabled = LimitResourceIsEnabled(self);
    if (ThaPwAdapterIsLogicPw(mAdapter(self)) || (AtPwBoundCircuitGet(self) == NULL))
        {
        if (limitResourceEnabled && (payloadSizeInBytes > mMethodsGet(mAdapter(self))->PdaMaxPayloadSize(mAdapter(self), pdaModule)))
            return cAtErrorOutOfRangParm;

        /* Check with minimum payload size in case of logical pw (Circuit NULL and don't care jitter buffer) */
        if (limitResourceEnabled && (payloadSizeInBytes < AtPwMinPayloadSize(self, NULL, 0)))
            return cAtErrorOutOfRangParm;

        mAdapter(self)->payloadSize = payloadSizeInBytes;
        return cAtOk;
        }

    /* Check if it is in range */
    if (limitResourceEnabled && !ThaPwAdapterPayloadSizeIsInRange(self, payloadSizeInBytes))
        return cAtErrorOutOfRangParm;

    if (!ThaPwAdatperPayloadSizeIsValid(mAdapter(self), payloadSizeInBytes))
        return cAtErrorInvlParm;

    /* Check if current jitter buffer size is enough for this payload size */
    jitterBufferInUs = AtPwJitterBufferSizeGet(self);
    if (limitResourceEnabled && !JitterBufferIsEnough(self, payloadSizeInBytes))
        {
        AtChannelLog((AtChannel)self,
                     cAtLogLevelCritical, AtSourceLocation,
                     "The current jitter buffer size %u(us) is not enough for payload size %u bytes",
                     jitterBufferInUs,
                     payloadSizeInBytes);
        return cAtErrorOutOfRangParm;
        }

    /* Check if bandwidth is acceptable */
    newBw = ThaPwAdapterBandwidthCalculateWithPayload(self, payloadSizeInBytes);
    if (limitResourceEnabled && !ThaPwEthPortBinderPwBandwidthIsAcceptable(EthPortBinder(self), self, newBw))
        return cAtErrorBandwidthExceeded;

    /* Check if PDA block is available for new payload size */
    newOccupiedBlock = ThaModulePdaPwNumBlockByBufferInUs(pdaModule, self, jitterBufferInUs, payloadSizeInBytes);
    if (limitResourceEnabled && !ThaModulePdaResourceIsAvailable(ModulePda(self), self, newOccupiedBlock))
        return cAtErrorRsrcNoAvail;
    
    ThaPwAdapterStartRemoving(self);
    ThaModulePdaPwCenterJitterEnable(pdaModule, self, cAtFalse); /* Avoid nested center process */

    mAdapter(self)->payloadSize = payloadSizeInBytes;
    ret |= ThaPwAdapterHwPayloadSizeSet(mAdapter(self), payloadSizeInBytes);
    ret |= ThaPwAdapterJitterBufferDefaultSet(mAdapter(self));

    ThaModulePdaPwCenterJitterEnable(pdaModule, self, cAtTrue);

    ThaPwAdapterFinishRemoving(self);

    if (ret != cAtOk)
        return ret;

    return ThaPwEthPortBinderPwBandwidthUpdate(EthPortBinder(self), self, newBw);
    }

static eBool ShouldGetFromDatabase(AtPw self)
    {
    if (mInAccessible(self))
        return cAtTrue;
    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        return cAtTrue;
    if (AtPwBoundCircuitGet(self) == NULL)
        return cAtTrue;

    return cAtFalse;
    }

static uint16 PayloadSizeGet(AtPw self)
    {
    ThaPwAdapter pw = mAdapter(self);

    if (ShouldGetFromDatabase(self))
        return pw->payloadSize;

    return mMethodsGet(pw)->HwPayloadSizeGet(pw);
    }

static uint16 HwPayloadSizeGet(ThaPwAdapter self)
    {
    AtPw pw = (AtPw)self;
    return ThaModulePwePayloadSizeGet(ModulePwe(pw), pw);
    }

static eAtModulePwRet PrioritySet(AtPw self, uint8 priority)
    {
	if (AtPwPriorityGet(self) == priority)
	    return cAtOk;

    /* Hardware not support */
    return cAtErrorModeNotSupport;
    }

static uint8 PriorityGet(AtPw self)
    {
	AtUnused(self);
    /* Hardware not support */
    return 0;
    }

static eAtModulePwRet ReorderingEnable(AtPw self, eBool enable)
    {
    eAtRet ret = cAtOk;
    ThaModulePda modulePda;

    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(self));
        if (cache)
            cache->reorderingEnabled = enable;
        return cAtOk;
        }

    mAdapter(self)->reoderEnabled = enable;
    if (AtPwBoundCircuitGet(self) == NULL)
        return cAtOk;

    modulePda = ModulePda(self);
    if (mShouldPreventReconfigure(self) && (ThaModulePdaPwReorderingIsEnable(modulePda, self) == enable))
        return cAtOk;

    ThaPwAdapterStartCenterJitterBuffer(self);
    ret = ThaModulePdaPwReoderingEnable(modulePda, self, enable);
    ThaPwAdapterStopCenterJitterBuffer(self);

    return ret;
    }

static eBool ReorderingIsEnabled(AtPw self)
    {
    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(self));
        return (eBool)(cache ? cache->reorderingEnabled : cAtFalse);
        }

    return mAdapter(self)->reoderEnabled;
    }

static eBool BufferSizeIsValid(uint32 microseconds)
    {
    if (microseconds == 0)
        return cAtFalse;

    return cAtTrue;
    }

static uint32 JitterBufferDelayIsInRange(AtPw self, uint32 delayUs)
    {
    uint32 minDelayUs = AtPwMinJitterDelay(self, AtPwBoundCircuitGet(self), AtPwPayloadSizeGet(self));

    return ((delayUs < minDelayUs) ? cAtFalse : cAtTrue);
    }

static eAtModulePwRet JitterBufferSizeSet(AtPw self, uint32 microseconds)
    {
    eAtRet ret = cAtOk;
    ThaModulePda pdaModule = ModulePda(self);
    eBool sizeInPktIsSame = cAtFalse;
    uint32 newSizeInPkt, newOccupiedBlock;

    if (!ThaModulePdaPwJitterBufferSizeIsValid(pdaModule, self, microseconds))
        mChannelError(self, cAtErrorInvlParm);

    /* Check if buffer size is valid */
    if (!BufferSizeIsValid(microseconds))
        mChannelError(self, cAtErrorInvlParm);

    /* Turn the flag off,
     * When the pw is activated, base on this flag to configure to hardware by packet unit or us unit */
    mAdapter(self)->jitterSizeIsInPkt = cAtFalse;

    /* Just cache configuration if PW cannot be run yet */
    if (ThaPwAdapterIsLogicPw(mAdapter(self)) || (AtPwBoundCircuitGet(self) == NULL))
        {
        mAdapter(self)->jitterBufferSizeInUs = microseconds;
        return cAtOk;
        }

    /* Must check when pw has been activated */
    newSizeInPkt = ThaModulePdaCalculateNumPacketsByUs(pdaModule, self, microseconds);
    if (newSizeInPkt == ThaModulePdaPwJitterBufferSizeInPacketGet(pdaModule, self))
        sizeInPktIsSame = cAtTrue;

    if (mShouldPreventReconfigure(self) && (AtPwJitterBufferSizeGet(self) == microseconds) && sizeInPktIsSame)
        return cAtOk;

    /* Check if buffer size is in range */
    if (!BufferSizeIsInRange(self, microseconds, AtPwPayloadSizeGet(self)))
        mChannelError(self, cAtErrorOutOfRangParm);

    /* Check if buffer resource is available */
    newOccupiedBlock = ThaModulePdaPwNumBlockByBufferInUs(pdaModule, self, microseconds, ThaPwAdapterPayloadSizeInBytes(mAdapter(self)));
    if (!ThaModulePdaResourceIsAvailable(pdaModule, self, newOccupiedBlock))
        mChannelError(self, cAtErrorRsrcNoAvail);

    /* Enough information, apply to hardware */
    ThaPwAdapterStartCenterJitterBuffer(self);
    ret = ThaModulePdaPwJitterBufferSizeSet(pdaModule, self, microseconds);
    if (ret == cAtOk)
        {
        mAdapter(self)->jitterBufferSizeInUs  = microseconds;
        mAdapter(self)->jitterBufferSizeInPkt = (uint16)ThaModulePdaCalculateNumPacketsByUs(pdaModule, self, microseconds);
        }
    ThaPwAdapterStopCenterJitterBuffer(self);

    if (ret != cAtOk)
        return ret;

    return ThaModulePdaTotalInUsedRamBlockUpdate(pdaModule, self, newOccupiedBlock);
    }

static uint32 JitterBufferSizeGet(AtPw self)
    {
    return mAdapter(self)->jitterBufferSizeInUs;
    }

static eAtRet DefaultJitterBufferSet(AtPw self, uint32 jitterDelayUs)
    {
    uint32 defaultJitterBuffer = ThaModulePdaDefaultJitterBufferSize(ModulePda(self), self, jitterDelayUs);

    /* Only set default jitter buffer if the jitter buffer size is currently
     * less than the minimum jitter buffer required for user's jitter delay. */
    if (defaultJitterBuffer > AtPwJitterBufferSizeGet(self))
        return AtPwJitterBufferSizeSet(self, defaultJitterBuffer);

    return cAtOk;
    }

static eAtModulePwRet JitterBufferDelaySet(AtPw self, uint32 microseconds)
    {
    eAtRet ret = cAtOk;
    ThaModulePda modulePda = ModulePda(self);
    eBool delayInPktIsSame = cAtFalse;

    /* Check if input delay is valid */
    if (microseconds == 0)
        mChannelError(self, cAtErrorInvlParm);
    if (!ThaModulePdaPwJitterDelayIsValid(modulePda, self, microseconds))
        mChannelError(self, cAtErrorInvlParm);

    /* Turn the flag off,
     * When the pw is activated, base on this flag to configure to hardware by packet unit or us unit */
    mAdapter(self)->jitterDelayIsInPkt = cAtFalse;

    /* Just cache if PW is not activated */
    if (ThaPwAdapterIsLogicPw(mAdapter(self)) || (AtPwBoundCircuitGet(self) == NULL))
        {
        mAdapter(self)->jitterBufferDelayInUs = microseconds;

        if (ThaModulePdaShouldSetDefaultJitterBufferSizeOnJitterDelaySet(modulePda, self))
            ret |= DefaultJitterBufferSet(self, microseconds);

        return ret;
        }

    /* Must check when pw has been activated */
    if (ThaModulePdaCalculateNumPacketsByUs(modulePda, self, microseconds) == ThaModulePdaPwJitterBufferDelayInPacketGet(modulePda, self))
        delayInPktIsSame = cAtTrue;

    if (mShouldPreventReconfigure(self) && (AtPwJitterBufferDelayGet(self) == microseconds) && delayInPktIsSame)
        return cAtOk;

    /* Check if jitter delay in range */
    if (!JitterBufferDelayIsInRange(self, microseconds))
        mChannelError(self, cAtErrorOutOfRangParm);

    /* Enough information, apply to hardware */
    ThaPwAdapterStartCenterJitterBuffer(self);
    ret = ThaModulePdaPwJitterBufferDelaySet(modulePda, self, microseconds);
    if (ret == cAtOk)
        {
        mAdapter(self)->jitterBufferDelayInUs = microseconds;
        mAdapter(self)->jitterBufferDelayInPkt = (uint16)ThaModulePdaCalculateNumPacketsByUs(modulePda, self, microseconds);
        }
    ThaPwAdapterStopCenterJitterBuffer(self);

    if (ThaModulePdaShouldSetDefaultJitterBufferSizeOnJitterDelaySet(modulePda, self))
        ret |= DefaultJitterBufferSet(self, microseconds);

    return ret;
    }

static uint32 JitterBufferDelayGet(AtPw self)
    {
    return mAdapter(self)->jitterBufferDelayInUs;
    }

static uint32 PayloadSizeInBytes(ThaPwAdapter self)
    {
    return AtPwPayloadSizeGet((AtPw)self);
    }

static uint32 NumCurrentPacketsInJitterBuffer(AtPw self)
    {
    ThaModulePda modulePda;

    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        return 0;

    modulePda = ModulePda(self);
    return ThaModulePdaNumCurrentPacketsInJitterBuffer(modulePda, self);
    }

static uint32 NumCurrentAdditionalBytesInJitterBuffer(AtPw self)
    {
    ThaModulePda modulePda;

    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        return 0;

    modulePda = ModulePda(self);
    return ThaModulePdaNumCurrentAdditionalBytesInJitterBuffer(modulePda, self);
    }

static eAtRet JitterBufferCenter(AtPw self)
    {
    eAtRet ret = cAtOk;

    ret |= ThaPwAdapterStartCenterJitterBuffer(self);
    ret |= ThaPwAdapterStopCenterJitterBuffer(self);

    return ret;
    }

static uint32 JitterBufferWatermarkMinPackets(AtPw self)
    {
    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        return 0;

    if (AtPwJitterBufferWatermarkIsSupported(self))
        return ThaModulePdaJitterBufferWatermarkMinPackets(ModulePda(self), self);

    return 0;
    }

static uint32 JitterBufferWatermarkMaxPackets(AtPw self)
    {
    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        return 0;

    if (AtPwJitterBufferWatermarkIsSupported(self))
        return ThaModulePdaJitterBufferWatermarkMaxPackets(ModulePda(self), self);

    return 0;
    }

static eBool JitterBufferWatermarkIsSupported(AtPw self)
    {
    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        return 0;

    return ThaModulePdaJitterBufferWatermarkIsSupported(ModulePda(self), self);
    }

static eAtRet JitterBufferWatermarkReset(AtPw self)
    {
    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        return cAtOk;

    if (AtPwJitterBufferWatermarkIsSupported(self))
        return ThaModulePdaJitterBufferWatermarkReset(ModulePda(self), self);

    return m_AtPwMethods->JitterBufferWatermarkReset(self);
    }

static eAtModulePwRet RtpEnable(AtPw self, eBool enable)
    {
    eAtRet ret;
    uint64 newBw;

    /* Check if bandwidth is acceptable */
    newBw = ThaPwAdapterBandwidthCalculateWithRtpEnable(self, enable);
    if (enable && !ThaPwEthPortBinderPwBandwidthIsAcceptable(EthPortBinder(self), self, newBw))
        return cAtErrorBandwidthExceeded;

    ret = ThaPwHeaderControllerRtpEnable(ThaPwAdapterHeaderController(mAdapter(self)), enable);
    if (ret != cAtOk)
        return ret;

    ThaPwEthPortBinderPwBandwidthUpdate(EthPortBinder(self), self, newBw);
    return cAtOk;
    }

static eBool RtpIsEnabled(AtPw self)
    {
    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(self));
        return (eBool)(cache ? cache->rtpEnabled : cAtFalse);
        }

    return ThaClaPwControllerPwRtpIsEnabled(ThaPwAdapterClaPwController(mAdapter(self)), self);
    }

static eAtModulePwRet RtpTxPayloadTypeSet(AtPw self, uint8 payloadType)
    {
    eAtRet ret = ThaPwHeaderControllerRtpTxPayloadTypeSet(ThaPwAdapterHeaderController(mAdapter(self)), payloadType);
    ThaPwHeaderController backupController = ThaPwAdapterBackupHeaderController(mAdapter(self));
    if (backupController)
        ret |= ThaPwHeaderControllerRtpTxPayloadTypeSet(backupController, payloadType);

    return ret;
    }

static uint8 RtpTxPayloadTypeGet(AtPw self)
    {
    return ThaPwHeaderControllerRtpTxPayloadTypeGet(ThaPwAdapterHeaderController(mAdapter(self)));
    }

static eAtModulePwRet RtpExpectedPayloadTypeSet(AtPw self, uint8 payloadType)
    {
    eAtRet ret = cAtOk;

    if (!ThaModulePwRtpPayloadTypeIsInRange(payloadType))
        return cAtErrorOutOfRangParm;

    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(self));
        if (cache)
            cache->expectedRtpPayloadType = payloadType;

        return cAtOk;
        }

    if (mShouldPreventReconfigure(self) && (AtPwRtpExpectedPayloadTypeGet(self) == payloadType))
        return cAtOk;

    ThaPwAdapterStartCenterJitterBuffer(self);
    ret = ThaClaPwControllerPwRtpPayloadTypeSet(ThaPwAdapterClaPwController(mAdapter(self)), self, payloadType);
    ThaPwAdapterStopCenterJitterBuffer(self);

    return ret;
    }

static uint8 RtpExpectedPayloadTypeGet(AtPw self)
    {
    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(self));
        return (uint8)(cache ? cache->expectedRtpPayloadType : 0);
        }

    return ThaClaPwControllerPwRtpPayloadTypeGet(ThaPwAdapterClaPwController(mAdapter(self)), self);
    }

static eAtModulePwRet RtpPayloadTypeCompare(AtPw self, eBool enableCompare)
	{
    return ThaPwHeaderControllerRtpPayloadTypeCompare(ThaPwAdapterHeaderController(mAdapter(self)), enableCompare);
	}

static eBool RtpPayloadTypeIsCompared(AtPw self)
	{
    return ThaPwHeaderControllerRtpPayloadTypeIsCompared(ThaPwAdapterHeaderController(mAdapter(self)));
	}

static eAtModulePwRet RtpTxSsrcSet(AtPw self, uint32 ssrc)
    {
    eAtRet ret = ThaPwHeaderControllerRtpTxSsrcSet(ThaPwAdapterHeaderController(mAdapter(self)), ssrc);
    ThaPwHeaderController backupController = ThaPwAdapterBackupHeaderController(mAdapter(self));
    if (backupController)
        ret |= ThaPwHeaderControllerRtpTxSsrcSet(backupController, ssrc);

    return ret;
    }

static uint32 RtpTxSsrcGet(AtPw self)
    {
    return ThaPwHeaderControllerRtpTxSsrcGet(ThaPwAdapterHeaderController(mAdapter(self)));
    }

static eAtModulePwRet RtpExpectedSsrcSet(AtPw self, uint32 ssrc)
    {
    eAtRet ret = cAtOk;

    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
    {
    tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(self));
    if (cache)
        cache->expectedRtpSsrc = ssrc;
    return cAtOk;
    }

    if (mShouldPreventReconfigure(self) && (AtPwRtpExpectedSsrcGet(self) == ssrc))
        return cAtOk;

    ThaPwAdapterStartCenterJitterBuffer(self);
    ret = ThaClaPwControllerPwRtpSsrcSet(ThaPwAdapterClaPwController(mAdapter(self)), self, ssrc);
    ThaPwAdapterStopCenterJitterBuffer(self);

    return ret;
    }

static uint32 RtpExpectedSsrcGet(AtPw self)
    {
    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(self));
        return cache ? cache->expectedRtpSsrc : 0x0;
        }

    return ThaClaPwControllerPwRtpSsrcGet(ThaPwAdapterClaPwController(mAdapter(self)), self);
    }

static eAtModulePwRet RtpSsrcCompare(AtPw self, eBool enableCompare)
	{
    return ThaPwHeaderControllerRtpSsrcCompare(ThaPwAdapterHeaderController(mAdapter(self)), enableCompare);
	}

static eBool RtpSsrcIsCompared(AtPw self)
	{
    return ThaPwHeaderControllerRtpSsrcIsCompared(ThaPwAdapterHeaderController(mAdapter(self)));
	}

static eAtModulePwRet RtpTimeStampModeSet(AtPw self, eAtPwRtpTimeStampMode timeStampMode)
    {
    AtUnused(self);
    if (timeStampMode == cAtPwRtpTimeStampModeDifferential)
        return cAtOk;

    return cAtErrorModeNotSupport;
    }

static eAtPwRtpTimeStampMode RtpTimeStampModeGet(AtPw self)
    {
	AtUnused(self);
    return cAtPwRtpTimeStampModeDifferential;
    }

static eBool RtpTimeStampModeIsSupported(AtPw self, eAtPwRtpTimeStampMode timeStampMode)
    {
    AtUnused(self);
    return (timeStampMode == cAtPwRtpTimeStampModeDifferential) ? cAtTrue : cAtFalse;
    }

static eAtModulePwRet CwEnable(AtPw self, eBool enable)
    {
    eAtRet  ret = cAtOk;
    ThaModulePwe modulePwe;
    eBool cwDisable = mMethodsGet(mAdapter(self))->CanDisableCw(mAdapter(self));

    /* Cannot disable control word */
    if (!enable && !cwDisable)
        return cAtErrorModeNotSupport;

    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(self));
        if (cache)
            cache->cwEnabled = enable;
        return cAtOk;
        }

    modulePwe = ModulePwe(self);
    mAdapter(self)->cwIsEnabled = enable;

    if (mShouldPreventReconfigure(self) && (AtPwCwIsEnabled(self) == enable))
        return cAtOk;

    ret |= ThaModulePwePwCwEnable(modulePwe, self, enable);
    ret |= ThaClaPwControllerPwCwEnable(ThaPwAdapterClaPwController(mAdapter(self)), self, enable);

    return ret;
    }

static eBool CwIsEnabled(AtPw self)
    {
    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(self));
        return (eBool)(cache ? cache->cwEnabled : cAtFalse);
        }

    return ThaClaPwControllerPwCwIsEnabled(ThaPwAdapterClaPwController(mAdapter(self)), self);
    }

static eAtModulePwRet CwAutoTxLBitEnable(AtPw self, eBool enable)
    {
    ThaModulePwe modulePwe;
    eAtRet ret = cAtOk;

    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(self));
        if (cache)
            cache->cwAutoTxLBitEnabled = enable;
        return cAtOk;
        }

    if (mShouldPreventReconfigure(self) && (AtPwCwAutoTxLBitIsEnabled(self) == enable))
        return cAtOk;

    modulePwe = ModulePwe(self);
    ret = ThaModulePwePwCwAutoTxLBitEnable(modulePwe, self, enable);
    ret |= ThaModulePwePwIdleCodeInsertionEnable(modulePwe, mAdapter(self), !enable);
    return ret;
    }

static eBool CwAutoTxLBitIsEnabled(AtPw self)
    {
    ThaModulePwe modulePwe;

    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(self));
        return (eBool)(cache ? cache->cwAutoTxLBitEnabled : cAtFalse);
        }

    modulePwe = ModulePwe(self);
    return ThaModulePwePwCwAutoTxLBitIsEnabled(modulePwe, self);
    }

static eAtModulePwRet CwAutoRxLBitEnable(AtPw self, eBool enable)
    {
    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(self));
        if (cache)
            cache->cwAutoRxLBitEnabled = enable;
        return cAtOk;
        }

	if (mShouldPreventReconfigure(self) && (AtPwCwAutoRxLBitIsEnabled(self) == enable))
	    return cAtOk;

	/* It is always enabled by default */
    if (!enable)
        return cAtErrorModeNotSupport;

    return cAtOk;
    }

static eBool CwAutoRxLBitCanEnable(AtPw self, eBool enable)
    {
    /* It is always enabled by default */
    AtUnused(self);
    return enable;
    }

static eBool CwAutoRxLBitIsEnabled(AtPw self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static eAtModulePwRet CwAutoRBitEnable(AtPw self, eBool enable)
    {
    ThaModulePwe modulePwe;

    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(self));
        if (cache)
            cache->cwAutoRBitEnabled = enable;
        return cAtOk;
        }

    if (mShouldPreventReconfigure(self) && (AtPwCwAutoRBitIsEnabled(self) == enable))
        return cAtOk;

    modulePwe = ModulePwe(self);
    return ThaModulePwePwCwAutoTxRBitEnable(modulePwe, self, enable);
    }

static eBool CwAutoRBitIsEnabled(AtPw self)
    {
    ThaModulePwe modulePwe;

    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(self));
        return (eBool)(cache ? cache->cwAutoRBitEnabled : cAtFalse);
        }

    modulePwe = ModulePwe(self);
    return ThaModulePwePwCwAutoTxRBitIsEnabled(modulePwe, self);
    }

static eAtModulePwRet CwSequenceModeSet(AtPw self, eAtPwCwSequenceMode sequenceMode)
    {
    eAtRet ret = cAtOk;
    ThaCdrController cdrController;
    ThaModuleCos moduleCos;
    ThaModulePda modulePda;

    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(self));
        if (cache)
            cache->cwSequenceMode = (uint8)sequenceMode;
        return cAtOk;
        }

    if (mShouldPreventReconfigure(self) && (AtPwCwSequenceModeGet(self) == sequenceMode))
        return cAtOk;

    moduleCos = ModuleCos(self);
    modulePda = ModulePda(self);

    if ((moduleCos == NULL) || (modulePda == NULL))
        return cAtErrorNullPointer;

    ret |= mMethodsGet(moduleCos)->PwCwSequenceModeSet(moduleCos, self, sequenceMode);
    ret |= ThaModulePdaPwCwSequenceModeSet(modulePda, self, sequenceMode);

    cdrController =  mMethodsGet(mAdapter(self))->CdrController(mAdapter(self));
    if (cdrController)
        ret |= ThaCdrControllerCwSequenceModeSet(cdrController, sequenceMode);

    return ret;
    }

static eAtPwCwSequenceMode CwSequenceModeGet(AtPw self)
    {
    ThaModuleCos moduleCos;
    eAtPwCwSequenceMode sequenceMode = cAtPwCwSequenceModeInvalid;
    ThaCdrController cdrController;

    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(self));
        return cache ? cache->cwSequenceMode : cAtPwCwSequenceModeInvalid;
        }

    moduleCos = ModuleCos(self);
    if (moduleCos)
        sequenceMode = mMethodsGet(moduleCos)->PwCwSequenceModeGet(moduleCos, self);

    if (sequenceMode != cAtPwCwSequenceModeInvalid)
        return sequenceMode;

    cdrController = mMethodsGet(mAdapter(self))->CdrController(mAdapter(self));
    return ThaCdrControllerCwSequenceModeGet(cdrController);
    }

static eBool CwSequenceModeIsSupported(AtPw self, eAtPwCwSequenceMode sequenceMode)
    {
    return ThaModuleCosPwCwSequenceModeIsSupported(ModuleCos(self), sequenceMode);
    }

static eAtModulePwRet CwLengthModeSet(AtPw self, eAtPwCwLengthMode lengthMode)
    {
    ThaModuleCos moduleCos;
    eAtRet ret = cAtOk;

    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(self));
        if (cache)
            cache->cwLengthMode = (uint8)lengthMode;
        return cAtOk;
        }

    if (mShouldPreventReconfigure(self) && (AtPwCwLengthModeGet(self) == lengthMode))
        return cAtOk;

    ret = ThaClaPwControllerPwCwLengthModeSet(ThaPwAdapterClaPwController(mAdapter(self)), self, lengthMode);
    if (ret != cAtOk)
        return ret;

    moduleCos = ModuleCos(self);
    if (moduleCos)
        return mMethodsGet(moduleCos)->PwCwLengthModeSet(moduleCos, self, lengthMode);

    return cAtErrorNullPointer;
    }

static eBool CwLengthModeIsSupported(AtPw self, eAtPwCwLengthMode lengthMode)
    {
    return ThaModuleCosPwCwLengthModeIsSupported(ModuleCos(self), lengthMode);
    }

static eAtPwCwLengthMode CwLengthModeGet(AtPw self)
    {
    ThaModuleCos moduleCos;

    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(self));
        return cache ? cache->cwLengthMode : cAtPwCwLengthModeInvalid;
        }

    moduleCos = ModuleCos(self);
    if (moduleCos)
        return mMethodsGet(moduleCos)->PwCwLengthModeGet(moduleCos, self);

    return cAtErrorNullPointer;
    }

static eAtModulePwRet CwPktReplaceModeSet(AtPw self, eAtPwPktReplaceMode pktReplaceMode)
    {
    ThaModulePda modulePda;

    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(self));
        if (cache)
            cache->cwPktReplaceMode = (uint8)pktReplaceMode;
        return cAtOk;
        }

    if (mShouldPreventReconfigure(self) && (AtPwCwPktReplaceModeGet(self) == pktReplaceMode))
        return cAtOk;

    modulePda = ModulePda(self);
    return ThaModulePdaPwCwPktReplaceModeSet(modulePda, self, pktReplaceMode);
    }

static eAtPwPktReplaceMode CwPktReplaceModeGet(AtPw self)
    {
    ThaModulePda modulePda;

    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(self));
        return cache ? cache->cwPktReplaceMode : cAtPwPktReplaceModeInvalid;
        }

    modulePda = ModulePda(self);
    return ThaModulePdaPwCwPktReplaceModeGet(modulePda, self);
    }

static eAtModulePwRet LopsPktReplaceModeSet(AtPw self, eAtPwPktReplaceMode pktReplaceMode)
    {
    ThaModulePda modulePda;

    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(self));
        if (cache)
            cache->lopsPktReplaceMode = (uint8)pktReplaceMode;
        return cAtOk;
        }

    if (mShouldPreventReconfigure(self) && (AtPwLopsPktReplaceModeGet(self) == pktReplaceMode))
        return cAtOk;

    modulePda = ModulePda(self);
    return ThaModulePdaPwLopsPktReplaceModeSet(modulePda, self, pktReplaceMode);
    }

static eAtPwPktReplaceMode LopsPktReplaceModeGet(AtPw self)
    {
    ThaModulePda modulePda;

    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(self));
        return cache ? cache->lopsPktReplaceMode : cAtPwPktReplaceModeInvalid;
        }

    modulePda = ModulePda(self);
    return ThaModulePdaPwLopsPktReplaceModeGet(modulePda, self);
    }

static eBool CanControlLopsPktReplaceMode(AtPw self)
    {
    return ThaModulePdaPwCanControlLopsPktReplaceMode(ModulePda(self));
    }

static eBool PktReplaceModeIsSupported(AtPw self, eAtPwPktReplaceMode pktReplaceMode)
    {
    return ThaModulePdaPwPktReplaceModeIsSupported(ModulePda(self), pktReplaceMode);
    }

static ThaPwActivator PwActivator(AtPw self)
    {
    return ThaModulePwActivator(ModulePw(self));
    }

static ThaPwHeaderController BackupHeaderController(ThaPwAdapter adapter)
    {
    if (adapter->backupHeaderController == NULL)
        adapter->backupHeaderController = ThaModulePwBackupHeaderControllerObjectCreate(ModulePw((AtPw)adapter), (AtPw)adapter);
    return adapter->backupHeaderController;
    }

static eAtModulePwRet PsnSet(AtPw self, AtPwPsn psn)
    {
    eAtRet ret;
    uint64 newBw;

    if ((psn != NULL) && !ThaHbcePwPsnIsSupported(psn))
        return cAtErrorModeNotSupport;

    newBw = ThaPwAdapterBandwidthCalculateWithPsn(self, psn);
    if (!ThaPwEthPortBinderPwBandwidthIsAcceptable(EthPortBinder(self), self, newBw))
        return cAtErrorBandwidthExceeded;

    ret = ThaPwHeaderControllerPsnSet(ThaPwAdapterHeaderController(mAdapter(self)), psn);
    if (ret != cAtOk)
        return ret;

    ThaPwEthPortBinderPwBandwidthUpdate(EthPortBinder(self), self, newBw);
    return cAtOk;
    }

static eAtModulePwRet BackupPsnSet(AtPw self, AtPwPsn psn)
    {
    return ThaPwHeaderControllerPsnSet(BackupHeaderController(mAdapter(self)), psn);
    }

static AtPwPsn PsnGet(AtPw self)
    {
    return ThaPwHeaderControllerPsnGet(ThaPwAdapterHeaderController(mAdapter(self)));
    }

static AtPwPsn BackupPsnGet(AtPw self)
    {
    return ThaPwHeaderControllerPsnGet(BackupHeaderController(mAdapter(self)));
    }

static eBool ShouldHandleCircuitAisForcingOnEnabling(ThaPwAdapter self)
    {
    ThaModulePw pwModule = ModulePw((AtPw)self);
    return ThaModulePwShouldHandleCircuitAisForcingOnEnabling(pwModule);
    }

static eBool IsPrbsEngineExisted(AtChannel self)
    {

    return AtChannelPrbsEngineIsCreated(self);
    }

static eAtRet CircuitTxSquelch(ThaPwAdapter self, eBool squelched)
    {
    AtChannel circuit = AtPwBoundCircuitGet((AtPw)self);
    if (circuit)
        {
        if (IsPrbsEngineExisted(circuit))
            return cAtOk;

        return AtChannelTxSquelch(circuit, squelched);
        }
    return cAtOk;
    }

static eAtModulePwRet EthPortSet(AtPw self, AtEthPort ethPort)
    {
    AtEthPort currentPort;
    eAtRet ret;
    ThaPwAdapter adapter = mAdapter(self);
    ThaPwEthPortBinder ethPortBinder = EthPortBinder(self);

    if (!ThaModulePwCanBindEthPort(ModulePw(self), self, ethPort))
        return cAtErrorModeNotSupport;

    if (!ThaPwActivatorPwBindingInformationIsValid(PwActivator(self), adapter, AtPwBoundCircuitGet(self), ethPort))
        return cAtErrorModeNotSupport;

    currentPort = AtPwEthPortGet(self);

    /* Already activate and now the same port is assigned. Just do nothing */
    if ((currentPort == ethPort) && !ThaPwAdapterIsLogicPw(adapter))
        return cAtOk;

    /* PW already bound to another Ethernet port */
    if (currentPort && ethPort && (currentPort != ethPort))
        return cAtErrorChannelBusy;

    if (ethPort)
        ret = ThaPwEthPortBinderPwEthPortBind(ethPortBinder, self, ethPort);
    else
        ret = ThaPwEthPortBinderPwEthPortUnBind(ethPortBinder, self, currentPort);
    
    if (ret != cAtOk)
        {
        eAtRet restoreRet = AtPwEthPortSet(self, currentPort);
        if (restoreRet != cAtOk)
            AtChannelLog((AtChannel)self, cAtLogLevelCritical, AtSourceLocation,
                         "Restore ethernet port fail, error code = %s\r\n", AtRet2String(restoreRet));
        }

    if ((ret == cAtOk) && ShouldHandleCircuitAisForcingOnEnabling(mAdapter(self)))
        ret |= CircuitTxSquelch(mAdapter(self), ethPort ? cAtFalse : cAtTrue);

    return ret;
    }

static AtEthPort EthPortGet(AtPw self)
    {
    return mAdapter(self)->ethPort;
    }

static eAtModulePwRet EthFlowSet(AtPw self, AtEthFlow ethFlow)
    {
    AtEthFlow currentFlow;
    ThaPwAdapter adapter = mAdapter(self);
    ThaPwActivator activator = PwActivator(self);

    if (!ThaModulePwCanBindEthFlow(ModulePw(self), self, ethFlow))
        return cAtErrorModeNotSupport;

    currentFlow = AtPwEthFlowGet(self);

    /* The same flow is assigned. Just do nothing */
    if (ethFlow && (currentFlow == ethFlow) && !ThaPwAdapterIsLogicPw(adapter))
        return cAtOk;

    /* PW already bound to another Ethernet flow */
    if (currentFlow && ethFlow && (currentFlow != ethFlow))
        return cAtErrorChannelBusy;

    /* PW is activated, cannot unbind Ethernet flow */
    if (AtPwEthPortGet(self) && (ethFlow == NULL))
        return cAtErrorChannelBusy;

    if (ethFlow)
        return ThaPwActivatorPwEthFlowBind(activator, adapter, ethFlow);

    return ThaPwActivatorPwEthFlowUnbind(activator, adapter);
    }

static AtEthFlow EthFlowGet(AtPw self)
    {
    return mAdapter(self)->ethFlow;
    }

static eBool VlanTagsMakePwBandwidthChange(AtPw self, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan)
    {
    tAtEthVlanTag curCVlan, curSVlan;
    tAtEthVlanTag *pCurCVlan = AtPwEthCVlanGet(self, &curCVlan);
    tAtEthVlanTag *pCurSVlan = AtPwEthSVlanGet(self, &curSVlan);

    if (((pCurCVlan == NULL) && (cVlan != NULL)) ||
        ((pCurCVlan != NULL) && (cVlan == NULL)) ||
        ((pCurSVlan == NULL) && (sVlan != NULL)) ||
        ((pCurSVlan != NULL) && (sVlan == NULL)))
        return cAtTrue;

    return cAtFalse;
    }

static eAtModulePwRet EthHeaderSet(AtPw self, uint8 *destMac, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan)
    {
    ThaPwHeaderController headerController;
    eAtRet ret;
    uint64 newBw = 0;
    eBool vlanBwChange = VlanTagsMakePwBandwidthChange(self, cVlan, sVlan);

    if (vlanBwChange)
        {
        newBw = ThaPwAdapterBandwidthCalculateWithEthHeader(self, cVlan, sVlan);
        if (!ThaPwEthPortBinderPwBandwidthIsAcceptable(EthPortBinder(self), self, newBw))
            return cAtErrorBandwidthExceeded;
        }

    headerController = ThaPwAdapterHeaderController(mAdapter(self));
    ret = ThaPwHeaderControllerEthHeaderSet(headerController, destMac, cVlan, sVlan);
    if (ret != cAtOk)
        return ret;

    if (vlanBwChange)
        ThaPwEthPortBinderPwBandwidthUpdate(EthPortBinder(self), self, newBw);

    return cAtOk;
    }

static eAtModulePwRet BackupEthHeaderSet(AtPw self, uint8 *destMac, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan)
    {
    ThaPwHeaderController controller = BackupHeaderController(mAdapter(self));
    return ThaPwHeaderControllerEthHeaderSet(controller, destMac, cVlan, sVlan);
    }

static eAtModulePwRet EthDestMacSet(AtPw self, uint8 *destMac)
    {
    ThaPwHeaderController controller = ThaPwAdapterHeaderController(mAdapter(self));
    return ThaPwHeaderControllerEthDestMacSet(controller, destMac);
    }

static eAtModulePwRet BackupEthDestMacSet(AtPw self, uint8 *destMac)
    {
    ThaPwHeaderController controller = BackupHeaderController(mAdapter(self));
    return ThaPwHeaderControllerEthDestMacSet(controller, destMac);
    }

static eAtModulePwRet EthDestMacGet(AtPw self, uint8 *destMac)
    {
    ThaPwHeaderController controller = ThaPwAdapterHeaderController(mAdapter(self));
    return ThaPwHeaderControllerEthDestMacGet(controller, destMac);
    }

static eAtModulePwRet BackupEthDestMacGet(AtPw self, uint8 *destMac)
    {
    ThaPwHeaderController controller = BackupHeaderController(mAdapter(self));
    return ThaPwHeaderControllerEthDestMacGet(controller, destMac);
    }

static eAtModulePwRet EthSrcMacSet(AtPw self, uint8 *srcMac)
    {
    ThaPwHeaderController controller = ThaPwAdapterHeaderController(mAdapter(self));
    return ThaPwHeaderControllerEthSourceMacSet(controller, srcMac);
    }

static eAtModulePwRet EthCVlanTpidSet(AtPw self, uint16 tpid)
    {
    ThaPwHeaderController controller = ThaPwAdapterHeaderController(mAdapter(self));
    return ThaPwHeaderControllerEthCVlanTpidSet(controller, tpid);
    }

static eAtModulePwRet EthSVlanTpidSet(AtPw self, uint16 tpid)
    {
    ThaPwHeaderController controller = ThaPwAdapterHeaderController(mAdapter(self));
    return ThaPwHeaderControllerEthSVlanTpidSet(controller, tpid);
    }

static eAtModulePwRet BackupEthSrcMacSet(AtPw self, uint8 *srcMac)
    {
    ThaPwHeaderController controller = BackupHeaderController(mAdapter(self));
    return ThaPwHeaderControllerEthSourceMacSet(controller, srcMac);
    }

static eAtModulePwRet EthSrcMacGet(AtPw self, uint8 *srcMac)
    {
    ThaPwHeaderController controller = ThaPwAdapterHeaderController(mAdapter(self));
    return ThaPwHeaderControllerEthSourceMacGet(controller, srcMac);
    }

static eAtModulePwRet BackupEthSrcMacGet(AtPw self, uint8 *srcMac)
    {
    ThaPwHeaderController controller = BackupHeaderController(mAdapter(self));
    return ThaPwHeaderControllerEthSourceMacGet(controller, srcMac);
    }

static eAtModulePwRet EthVlanSet(AtPw self, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan)
    {
    ThaPwHeaderController headerController;
    eAtRet ret;
    uint64 newBw = 0;
    eBool vlanBwChange = VlanTagsMakePwBandwidthChange(self, cVlan, sVlan);

    if (vlanBwChange)
        {
        newBw = ThaPwAdapterBandwidthCalculateWithEthHeader(self, cVlan, sVlan);
        if (!ThaPwEthPortBinderPwBandwidthIsAcceptable(EthPortBinder(self), self, newBw))
            return cAtErrorBandwidthExceeded;
        }

    headerController = ThaPwAdapterHeaderController(mAdapter(self));
    ret = ThaPwHeaderControllerEthVlanSet(headerController, cVlan, sVlan);
    if (ret != cAtOk)
            return ret;

    if (vlanBwChange)
        ThaPwEthPortBinderPwBandwidthUpdate(EthPortBinder(self), self, newBw);

    return cAtOk;
    }

static eAtModulePwRet BackupEthVlanSet(AtPw self, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan)
    {
    ThaPwHeaderController controller = BackupHeaderController(mAdapter(self));
    return ThaPwHeaderControllerEthVlanSet(controller, cVlan, sVlan);
    }

static tAtEthVlanTag *EthCVlanGet(AtPw self, tAtEthVlanTag *cVlan)
    {
    ThaPwHeaderController controller = ThaPwAdapterHeaderController(mAdapter(self));
    return ThaPwHeaderControllerEthCVlanGet(controller, cVlan);
    }

static uint16 EthCVlanTpidGet(AtPw self)
    {
    ThaPwHeaderController controller = ThaPwAdapterHeaderController(mAdapter(self));
    return ThaPwHeaderControllerEthCVlanTpidGet(controller);
    }

static uint16 EthSVlanTpidGet(AtPw self)
    {
    ThaPwHeaderController controller = ThaPwAdapterHeaderController(mAdapter(self));
    return ThaPwHeaderControllerEthSVlanTpidGet(controller);
    }

static tAtEthVlanTag *BackupEthCVlanGet(AtPw self, tAtEthVlanTag *cVlan)
    {
    ThaPwHeaderController controller = BackupHeaderController(mAdapter(self));
    return ThaPwHeaderControllerEthCVlanGet(controller, cVlan);
    }

static tAtEthVlanTag *EthSVlanGet(AtPw self, tAtEthVlanTag *sVlan)
    {
    ThaPwHeaderController controller = ThaPwAdapterHeaderController(mAdapter(self));
    return ThaPwHeaderControllerEthSVlanGet(controller, sVlan);
    }

static tAtEthVlanTag *BackupEthSVlanGet(AtPw self, tAtEthVlanTag *sVlan)
    {
    ThaPwHeaderController controller = BackupHeaderController(mAdapter(self));
    return ThaPwHeaderControllerEthSVlanGet(controller, sVlan);
    }

static eBool ShouldEnableLookupOnBinding(AtPw self, AtChannel circuit)
    {
    AtSdhChannel vc = (AtSdhChannel)circuit;

    if (AtPwTypeGet(self) != cAtPwTypeCEP)
        return cAtTrue;

    if (AtSdhChannelTypeGet(vc) == cAtSdhChannelTypeVc4_64c)
        return cAtFalse;

    return cAtTrue;
    }

static eAtRet BufferOverrunForce(ThaPwAdapter self, eBool force)
    {
    AtPw adapter = (AtPw)self;
    AtChannel circuit = AtPwBoundCircuitGet(adapter);
    eAtRet ret = cAtOk;

    if (AtDriverIsStandby() || ThaPwTypeIsHdlcPppFr(adapter))
        return cAtErrorModeNotSupport;

    if (force)
        {
        ret |= ThaPwAdapterStartCenterJitterBuffer(adapter);
        if (circuit)
            ret |= AtChannelTxTrafficEnable(circuit, cAtFalse);

        ret |= ThaPwAdapterStopCenterJitterBuffer(adapter);
        }
    else
        {
        ret |= ThaPwAdapterStartCenterJitterBuffer(adapter);
        if (circuit)
            ret |= AtChannelTxTrafficEnable(circuit, cAtTrue);

        ret |= ThaPwAdapterStopCenterJitterBuffer(adapter);
        }

    return ret;
    }

static eAtRet HwEnable(ThaPwAdapter self, eBool enable)
    {
    eAtRet ret;
    AtPw pw;
    AtChannel circuit;
    ThaModulePwe pweModule;
    ThaModulePda pdaModule;
    /* Initialize */
    ret = cAtOk;
    pw  = (AtPw)self;
    circuit = AtPwBoundCircuitGet(pw);
    pweModule = ModulePwe(pw);
    pdaModule= ModulePda(pw);

    /* Enable */
    if (enable)
        {
        ThaModulePwPwEnablePrepare(ModulePw(pw), pw);

        /* Tx PW, should enable PWE then DEMAP */
        ret |= ThaModulePwePwEnable(pweModule, pw, enable);
        if (circuit)
            ret |= AtChannelRxTrafficEnable(circuit, enable);

        /* Rx PW, should enable MAP then CLA */
        if (circuit)
            ret |= AtChannelTxTrafficEnable(circuit, enable);

        ret |= ThaPwAdapterClaEnable(pw, enable);
        ret |= ThaModulePdaPwPdaTdmLookupEnable(ModulePda(pw), pw, cAtTrue);

        ThaModulePwPwEnableFinish(ModulePw(pw), pw);
        }

    /* Disable */
    else
        {
        ThaModulePwPwDisablePrepare(ModulePw(pw), pw);

        /* Tx PW, should disable DEMAP then PWE */
        if (circuit)
            ret |= AtChannelRxTrafficEnable(circuit, enable);
        ret |= ThaModulePwePwEnable(pweModule, pw, enable);

        /* Rx PW, should disable CLA then MAP */
        if (ThaPwActivatorShouldControlJitterBuffer(PwActivator(pw)))
            {
            ret |= ThaPwAdapterClaEnable(pw, enable);
            if (pdaModule)
            	ThaModulePdaJitterBufferEmptyWait(pdaModule, pw);
            if (circuit)
                {
                ret |= AtChannelTxTrafficEnable(circuit, enable);

                /* If we do not need to enable PW lookup on binding, PW lookup shall
                 * be disabled by PW disabling. */
                if (!ShouldEnableLookupOnBinding(pw, circuit))
                	{
                	if (pdaModule)
                		ret |= ThaModulePdaPwPdaTdmLookupEnable(pdaModule, pw, cAtFalse);
                	}
                }

            ret |= ThaModulePwePwRamStatusClear(pweModule, pw);
            }

        ThaModulePwPwDisableFinish(ModulePw(pw), pw);
        }

    return ret;
    }

static eBool HwIsEnabled(ThaPwAdapter self)
    {
    AtPw pw = (AtPw)self;
    return ThaModulePwePwIsEnabled(ModulePwe(pw), pw);
    }

static eBool NeedDoReConfig(AtPw pw)
    {
    ThaModulePw pwModule = (ThaModulePw)AtChannelModuleGet((AtChannel) pw);
    return ThaModulePwNeedDoReConfig(pwModule);
    }

static eAtRet Enable(AtChannel self, eBool enable)
    {
    AtPw pw = (AtPw)self;
    AtEthPort port = AtPwEthPortGet(pw);
    eAtRet ret;

    if (NeedDoReConfig(pw))
        {
        if (mShouldPreventReconfigure(self) && (AtChannelIsEnabled(self) == enable))
            return cAtOk;

        if (port != NULL)
            {
            AtSerdesController controller = AtEthPortSerdesController(port);
            if (AtSerdesControllerPowerIsDown(controller) && (enable == cAtTrue))
                {
                AtChannelLog(self, cAtLogLevelCritical, AtSourceLocation,
                             "Cannot enable PW when Ethernet port serdes is power down\r\n");
                return cAtErrorNotApplicable;
                }
            }
        }

    mAdapter(self)->enabled = enable;
    ThaModulePwPwEnableSaveToHwStorage(ModulePw(pw), pw, enable);

    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        return cAtOk;

    ret = ThaPwEthPortBinderPwEnable(EthPortBinder(pw), pw, enable);
    if ((ret == cAtOk) && ShouldHandleCircuitAisForcingOnEnabling(mAdapter(self)))
        ret |= CircuitTxSquelch(mAdapter(self), enable ? cAtFalse : cAtTrue);

    return ret;
    }

static eBool IsEnabled(AtChannel self)
    {
    AtPw pw;

    if (ThaPwAdapterIsLogicPw(mAdapter(self)) || mInAccessible(self))
        return ThaPwAdapterIsEnabled(mAdapter(self));

    pw = (AtPw)self;
    return ThaPwEthPortBinderPwIsEnabled(EthPortBinder(pw), pw);
    }

static eAtRet AllCountersReadToClear(AtChannel self, void *pAllCounters, eBool clear)
    {
    AtPwCounters counters;
    AtPw pw;

    counters = ThaPwAdapterCountersGet((mAdapter(self)));
    if (counters == NULL)
        return cAtOk;

    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        if (clear)
            AtPwCountersInit(counters);
        if (pAllCounters)
            *((AtPwCounters *)pAllCounters) = counters;
        return cAtOk;
        }

    /* Initialize */
    pw = (AtPw)self;

    if (ThaPwAdapterShouldReadCountersFromSurModule(self))
        {
        ThaModuleHardSur surModule = ModuleSur(pw);
        eAtRet ret;

        /* In case of latching fail, just return previous counters value, do not
         * need to exit */
        ret = ThaModuleHardSurPwCounterLatch(surModule, pw, clear);
        if (ret != cAtOk)
            {
            AtChannelLog(self, cAtLogLevelCritical, AtSourceLocation,
                         "SUR PW counter latching fail with ret = %s\r\n",
                         AtRet2String(ret));
            }

        AtPwCountersTxPacketsSet(counters, ThaModuleHardSurPwTxPacketsGet(surModule, pw, clear));
        AtPwCountersTxPayloadBytesSet(counters, ThaModuleHardSurPwTxBytesGet(surModule, pw, clear));
        AtPwCountersRxPacketsSet(counters, ThaModuleHardSurPwRxPacketsGet(surModule, pw, clear));
        AtPwCountersRxMalformedPacketsSet(counters, ThaModuleHardSurPwRxMalformedPacketsGet(surModule, pw, clear));
        AtPwCountersRxStrayPacketsSet(counters, ThaModuleHardSurPwRxStrayPacketsGet(surModule, pw, clear));
        AtPwCountersRxOamPacketsSet(counters, ThaModuleHardSurPwRxOamPacketsGet(surModule, pw, clear));
        AtPwCountersRxPayloadBytesSet(counters, ThaModuleHardSurPwRxBytesGet(surModule, pw, clear));
        AtPwCountersRxReorderedPacketsSet(counters, ThaModuleHardSurPwRxReorderedPacketsGet(surModule, pw, clear));
        AtPwCountersRxLostPacketsSet(counters, ThaModuleHardSurPwRxLostPacketsGet(surModule, pw, clear));
        AtPwCountersRxOutOfSeqDropPacketsSet(counters, ThaModuleHardSurPwRxOutOfSeqDropedPacketsGet(surModule, pw, clear));
        }
    else
        {
        ThaClaPwController claController = ThaPwAdapterClaPwController(mAdapter(self));
        ThaModulePwe pweModule = ModulePwe(pw);
        ThaModulePda pdaModule = ModulePda(pw);

        AtPwCountersTxPacketsSet(counters, ThaModulePwePwTxPacketsGet(pweModule, pw, clear));
        AtPwCountersTxPayloadBytesSet(counters, ThaModulePwePwTxBytesGet(pweModule, pw, clear));
        AtPwCountersRxPacketsSet(counters, ThaClaPwControllerRxPacketsGet(claController, pw, clear));
        AtPwCountersRxMalformedPacketsSet(counters, ThaClaPwControllerRxMalformedPacketsGet(claController, pw, clear));
        AtPwCountersRxStrayPacketsSet(counters, ThaClaPwControllerRxStrayPacketsGet(claController, pw, clear));
        AtPwCountersRxDuplicatedPacketsSet(counters, ThaClaPwControllerRxDuplicatedPacketsGet(claController, pw, clear));
        AtPwCountersRxOamPacketsSet(counters, ThaClaPwControllerRxOamPacketsGet(claController, pw, clear));
        AtPwCountersRxPayloadBytesSet(counters, ThaModulePdaPwRxBytesGet(pdaModule, pw, clear));
        AtPwCountersRxReorderedPacketsSet(counters, ThaModulePdaPwRxReorderedPacketsGet(pdaModule, pw, clear));
        AtPwCountersRxLostPacketsSet(counters, ThaModulePdaPwRxLostPacketsGet(pdaModule, pw, clear));
        AtPwCountersRxOutOfSeqDropPacketsSet(counters, ThaModulePdaPwRxOutOfSeqDropedPacketsGet(pdaModule, pw, clear));
        }

    AtPwCountersRxDiscardedPacketsSet(counters, AtPwCountersRxOutOfSeqDropPacketsGet(counters));

    if (pAllCounters)
        *((AtPwCounters *)pAllCounters) = counters;

    return cAtOk;
    }

static eAtRet AllCountersGet(AtChannel self, void *pAllCounters)
    {
    return AllCountersReadToClear(self, pAllCounters, cAtFalse);
    }

static eAtRet AllCountersClear(AtChannel self, void *pAllCounters)
    {
    return AllCountersReadToClear(self, pAllCounters, cAtTrue);
    }

static void DeleteSurEngine(AtChannel self)
    {
    if (mAdapter(self)->surEngine)
        {
        AtModuleSur surModule = (AtModuleSur)AtDeviceModuleGet((AtChannelDeviceGet(self)), cAtModuleSur);
        AtModuleSurEnginePwDelete(surModule,mAdapter(self)->surEngine);
        mAdapter(self)->surEngine = NULL;
        }
    }

static void DeleteController(AtObject *controller)
    {
    AtObjectDelete(*controller);
    *controller = NULL;
    }

static void Delete(AtObject self)
    {
    /* Delete internal objects */
    AtObjectDelete((AtObject)(mAdapter(self)->counters));
    mAdapter(self)->counters = NULL;

    ThaPwAdapterCacheDelete(mAdapter(self));
	
    /* Delete header controller */
    DeleteController((AtObject*)&mAdapter(self)->headerController);
    DeleteController((AtObject*)&mAdapter(self)->backupHeaderController);

    /* Delete hardware PW */
    ThaPwActivatorHwPwDeallocate(ThaModulePwActivator(ModulePw((AtPw)self)), mAdapter(self));

    DeleteSurEngine((AtChannel)self);

    /* Delete defect controller */
    DeleteController((AtObject*)&mAdapter(self)->defaultDefectController);
    DeleteController((AtObject*)&mAdapter(self)->debugDefectController);

    /* Fully delete itself */
    m_AtObjectMethods->Delete(self);
    }

static eAtModulePwRet SuppressEnable(AtPw self, eBool enable)
    {
    eAtRet ret = cAtOk;
    ThaModulePwe pweModule;
    ThaModuleClaPw claModule;
    eBool pweSuppressIsEnabled;
    eBool claSuppressIsEnabled;

    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(self));
        if (cache)
            cache->suppressed = enable;
        return cAtOk;
        }

    pweModule = ModulePwe(self);
    claModule = ModuleCla(self);

    pweSuppressIsEnabled = ThaModulePwePwSuppressIsEnabled(pweModule, self);
    if (pweSuppressIsEnabled != enable)
        ret |= ThaModulePwePwSuppressEnable(pweModule, self, enable);

    if (!ThaModuleClaPwCanControlPwSuppression(claModule))
        return ret;

    claSuppressIsEnabled = ThaModuleClaPwSuppressIsEnabled(claModule, self);
    if (claSuppressIsEnabled != enable)
        ret |= ThaModuleClaPwSuppressEnable(claModule, self, enable);

    return ret;
    }

static eBool SuppressIsEnabled(AtPw self)
    {
    ThaModulePwe pweModule;

    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(self));
        return (eBool)(cache ? cache->suppressed : cAtFalse);
        }

    pweModule = ModulePwe(self);
    return ThaModulePwePwSuppressIsEnabled(pweModule, self);
    }

static eThaPwErrorCheckingMode DefaultErrorCheckingMode(ThaPwAdapter self)
    {
    if (ThaModulePwEnableErrorCheckingByDefault(ModulePw((AtPw)self)))
        return cThaPwErrorCheckingModeCheckCountAndDiscard;
    return cThaPwErrorCheckingModeNoCheck;
    }

static eAtRet DefaultErrorCheckingSet(ThaPwAdapter self)
    {
    AtPw pw = (AtPw)self;
    ThaClaPwController claController = ThaPwAdapterClaPwController(self);
    eThaPwErrorCheckingMode defaultErrorCheckMode = DefaultErrorCheckingMode(self);

    /* Not all of product can support all error checking, just ignore error code */
    ThaClaPwControllerRtpSequenceMistmatchCheckModeSet(claController, pw, defaultErrorCheckMode);
    ThaClaPwControllerMalformCheckModeSet(claController, pw, defaultErrorCheckMode);
    ThaClaPwControllerRealLenCheckModeSet(claController, pw, defaultErrorCheckMode);
    ThaClaPwControllerLenFieldCheckModeSet(claController, pw, defaultErrorCheckMode);

    return cAtOk;
    }

static uint32 DefaultPayloadSize(ThaPwAdapter self, AtChannel circuit)
    {
	AtUnused(circuit);
	AtUnused(self);
    return 64;
    }

static eBool NeedToControlCdr(ThaPwAdapter self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static eAtRet SetDefaultOnBinding(ThaPwAdapter self, AtChannel circuit)
    {
    AtPw adapter = (AtPw)self;
    uint16 payloadSize;
    eAtRet ret = cAtOk;
    ThaModulePda pdaModule = ModulePda(adapter);

    /* Need to use application configuration first. If it has not configured,
     * use a default one just for safe */
    payloadSize = self->payloadSize;
    if (payloadSize == 0)
        payloadSize = (uint16)mMethodsGet(self)->DefaultPayloadSize(self, circuit);

    ret |= AtPwPayloadSizeSet(adapter, payloadSize);
    if (ret != cAtOk)
        return ret;

    ret |= mMethodsGet(self)->JitterPayloadLengthSet(self);

    ret |= ThaPwAdapterHwPwTypeSet(self);
    ret |= AtPwReorderingEnable(adapter, self->reoderEnabled);
    ret |= DefaultErrorCheckingSet(self);

    if (mMethodsGet(self)->NeedToControlCdr(self))
        {
        ThaClaPwController claController = ThaPwAdapterClaPwController(self);
        ret |= ThaClaPwControllerCdrEnable(claController, adapter, 0, cAtFalse);
        ret |= ThaClaPwControllerCdrCircuitSliceSet(claController, adapter, ThaPwAdapterCdrCircuitSlice(self, circuit));
        }

    ret |= ThaModulePdaPwCircuitSliceSet(pdaModule, adapter, ThaPwAdapterPdaCircuitSlice(self, circuit));
    ret |= ThaPwDefectControllerDefaultSet(ThaPwAdapterPwDefectControllerGet(self));
    ret |= ThaModulePdaPwCircuitIdSet(pdaModule, adapter);
    ret |= ThaModulePdaPwPdaTdmLookupEnable(pdaModule, adapter, ShouldEnableLookupOnBinding(adapter, circuit));
    ret |= ThaModulePdaCepIndicate(pdaModule, adapter, cAtFalse);
    ret |= ThaModulePwePwJitterAttenuatorEnable(ModulePwe(adapter), adapter, cAtFalse);

    return ret;
    }

static AtChannel BoundCircuitGet(AtPw self)
    {
    return mAdapter(self)->circuit;
    }

static eAtModulePwRet CircuitBind(AtPw self, AtChannel circuit)
    {
    eAtRet ret;
    uint64 newBw;
    uint32 newOccupiedBlock;
    ThaPwAdapter adapter = mAdapter(self);
    uint32 payloadSizeInByte;
    ThaModulePda pdaModule = ModulePda(self);
    AtChannel oldCircuit = AtPwBoundCircuitGet(self);

    if (oldCircuit == circuit)
        return cAtOk;

    if (circuit == NULL)
        return AtPwCircuitUnbind(self);

    /* PW is being bound to other circuit */
    if ((oldCircuit != NULL) && (oldCircuit != circuit))
        return cAtErrorChannelBusy;

    ret = AtChannelCanBindToPseudowire(circuit, ThaPwAdapterPwGet(adapter));
    if (ret != cAtOk)
        return ret;

    if (!ThaPwActivatorPwBindingInformationIsValid(PwActivator(self), adapter, circuit, AtPwEthPortGet(self)))
        return cAtErrorModeNotSupport;

    mChannelSuccessAssert(self, AtPwCircuitBindingCheck(self, circuit));

    adapter->circuit = circuit;
    ret = ThaPwActivatorPwCircuitBind(PwActivator(self), adapter, circuit);
    if (ret != cAtOk)
        {
        AtChannelBoundPwSet(adapter->circuit, NULL);
        adapter->circuit = NULL;
        return ret;
        }

    if ((!AtChannelIsEnabled((AtChannel)self)) && ShouldHandleCircuitAisForcingOnEnabling(adapter))
        CircuitTxSquelch(adapter, cAtTrue);

    if (!ThaPwActivatorShouldControlPwBandwidth(PwActivator(self)))
        return cAtOk;

    /* Bandwidth may be changed after binding, re-calculate */
    newBw = ThaPwAdapterCurrentPwBandwidthInBpsCalculate(self);
    ret   = ThaPwEthPortBinderPwBandwidthUpdate(EthPortBinder(self), self, newBw);

    /* Update in used ram block */
    payloadSizeInByte = ThaPwAdapterPayloadSizeInBytes(adapter);
    newOccupiedBlock = ThaModulePdaPwNumBlockByBufferInUs(pdaModule, self, AtPwJitterBufferSizeGet(self), payloadSizeInByte);
    ret |= ThaModulePdaTotalInUsedRamBlockUpdate(pdaModule, self, newOccupiedBlock);

    return ret;
    }

static eAtModulePwRet CircuitUnbind(AtPw self)
    {
    AtChannel circuit;
    eAtRet ret = cAtOk;
    AtPrbsEngine prbsEngine;
    ThaPwAdapter adapter = mAdapter(self);

    /* Already unbind */
    circuit = AtPwBoundCircuitGet(self);
    if (circuit == NULL)
        return cAtOk;

    if ((!AtChannelIsEnabled((AtChannel)self)) && ShouldHandleCircuitAisForcingOnEnabling(adapter))
        CircuitTxSquelch(adapter, cAtFalse);

    /* Should change monitoring point to TDM if circuit is bound */
    prbsEngine = AtChannelPrbsEngineGet(circuit);
    if (prbsEngine && (AtPrbsEngineMonitoringSideGet(prbsEngine) == cAtPrbsSidePsn))
        {
        if (!ThaPrbsEngineFlexiblePsnSide((ThaPrbsEngine)prbsEngine))
        AtPrbsEngineMonitoringSideSet(prbsEngine, cAtPrbsSideTdm);
        }

    ret |= ThaPwEthPortBinderPwEnable(EthPortBinder(self), self, cAtFalse);
    ret |= ThaPwActivatorPwCircuitUnbind(ThaModulePwActivator(ModulePw(self)), mAdapter(self));
    if (ret != cAtOk)
        return ret;

    ret = ThaPwEthPortBinderPwBandwidthUpdate(EthPortBinder(self), self, 0);
    ret |= ThaModulePdaTotalInUsedRamBlockUpdate(ModulePda(self), self, 0);

    /* Some services needs different jitter buffer size between bound and unbound state */
    if (ThaModulePdaShouldSetDefaultJitterBufferSizeOnJitterDelaySet(ModulePda(self), self))
        ret |= DefaultJitterBufferSet(self, AtPwJitterBufferDelayGet(self));

    return ret;
    }

static void InterruptProcess(AtPw self, uint32 hwPw, AtHal hal)
    {
    AtPwInterruptProcess((AtPw)ThaPwAdapterPwDefectControllerGet((ThaPwAdapter)self), hwPw, hal);
    }

static eBool ShouldMaskAllAlarms(AtChannel self)
    {
    if (ThaModulePwShouldMaskDefectAndFailureOnPwDisabling(ModulePw((AtPw)self)))
        return AtChannelIsEnabled(self) ? cAtFalse : cAtTrue;
    return cAtFalse;
    }

static uint32 DefectGet(AtChannel self)
    {
    if (ShouldMaskAllAlarms(self))
        return 0;
    return AtChannelDefectGet((AtChannel)ThaPwAdapterPwDefectControllerGet((ThaPwAdapter)self));
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    return AtChannelDefectInterruptGet((AtChannel)ThaPwAdapterPwDefectControllerGet((ThaPwAdapter)self));
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    return AtChannelDefectInterruptClear((AtChannel)ThaPwAdapterPwDefectControllerGet((ThaPwAdapter)self));
    }

static uint32 AlarmGet(AtChannel self)
    {
    /* Some applications require when PW is disabled, need to mask all of alarms */
    if (ShouldMaskAllAlarms(self))
        return 0;

    return AtChannelAlarmGet((AtChannel)ThaPwAdapterPwDefectControllerGet((ThaPwAdapter)self));
    }

static uint32 AlarmHistoryGet(AtChannel self)
    {
    return AtChannelAlarmInterruptGet((AtChannel)ThaPwAdapterPwDefectControllerGet((ThaPwAdapter)self));
    }

static uint32 AlarmHistoryClear(AtChannel self)
    {
    return AtChannelAlarmInterruptClear((AtChannel)ThaPwAdapterPwDefectControllerGet((ThaPwAdapter)self));
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
    return AtChannelSupportedInterruptMasks((AtChannel)ThaPwAdapterPwDefectControllerGet((ThaPwAdapter)self));
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    return AtChannelInterruptMaskSet((AtChannel)ThaPwAdapterPwDefectControllerGet((ThaPwAdapter)self), defectMask, enableMask);
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    return AtChannelInterruptMaskGet((AtChannel)ThaPwAdapterPwDefectControllerGet((ThaPwAdapter)self));
    }

static eAtRet InterruptEnable(AtChannel self)
    {
    return AtChannelInterruptEnable((AtChannel)ThaPwAdapterPwDefectControllerGet((ThaPwAdapter)self));
    }

static eAtRet InterruptDisable(AtChannel self)
    {
    return AtChannelInterruptDisable((AtChannel)ThaPwAdapterPwDefectControllerGet((ThaPwAdapter)self));
    }

static eBool InterruptIsEnabled(AtChannel self)
    {
    return AtChannelInterruptIsEnabled((AtChannel)ThaPwAdapterPwDefectControllerGet((ThaPwAdapter)self));
    }

static eBool AlarmIsSupported(AtChannel self, uint32 alarmType)
    {
    return AtChannelAlarmIsSupported((AtChannel)ThaPwAdapterPwDefectControllerGet((ThaPwAdapter)self), alarmType);
    }

static void BandwidthPrint(ThaPwAdapter self)
    {
    uint64 currentBandwidthInBps = ThaPwAdapterCurrentPwBandwidthInBpsGet((AtPw)self);
    AtPrintf("* currentBandwidthInBps: %s\r\n", AtNumberUInt64DigitGroupingString(currentBandwidthInBps));
    }

static eAtRet DatabaseDebug(ThaPwAdapter self)
    {
    mPrintField(self, jitterBufferSizeInUs);

    mPrintField(self, jitterBufferDelayInUs);
    mPrintField(self, jitterBufferSizeInPkt);
    mPrintField(self, jitterBufferDelayInPkt);
    mPrintField(self, jitterSizeIsInPkt);
    mPrintField(self, jitterDelayIsInPkt);
    mPrintField(self, numRamBlockForBuffer);
    BandwidthPrint(self);

    return cAtOk;
    }

static eAtRet Debug(AtChannel self)
    {
    ThaPwAdapter adapter = (ThaPwAdapter)self;
    AtPw pw = (AtPw)self;
    ThaModulePda modulePda = ModulePda(pw);

    if (ThaPwAdapterIsLogicPw(adapter))
        AtPrintc(cSevInfo, "PW is logical\r\n");

    else
        {
        AtPrintc(cSevNormal, "=================================================\r\n");
        AtPrintc(cSevNormal, "= %s.%s\r\n",
                 AtChannelTypeString(self),
                 AtChannelIdString(self));
        AtPrintc(cSevNormal, "=================================================\r\n");
        if (adapter->hwPw)
            {
            AtPrintc(cSevInfo, "HwPw: \r\n");
            ThaHwPwDebug(adapter->hwPw);
            }

        AtPrintc(cSevInfo, "CLA information\r\n");
        ThaPwHeaderControllerDebug(ThaPwAdapterHeaderController(adapter));
        ThaPwHeaderControllerDebug(BackupHeaderController(adapter));

        AtPrintc(cSevInfo, "PDA information\r\n");
        ThaModulePdaPwDebug(modulePda, pw);
        if (ThaModulePwShouldStrictlyLimitResources((ThaModulePw)AtChannelModuleGet(self)))
            AtPrintc(cSevNormal, "* Number of jitter buffer ram block: %u\r\n", AtPwJitterBufferNumBlocksGet(pw));
        }

    AtPrintc(cSevInfo, "Bandwidth information\r\n");
    ThaPwAdapterCurrentPwBandwidthDebug(pw);

    AtPrintc(cSevInfo, "Database information\r\n");
    DatabaseDebug(adapter);

    return cAtOk;
    }

static void DebuggerEntriesFill(AtChannel self, AtDebugger debugger)
    {
    ThaHwPwDebuggerEntriesFill(mAdapter(self)->hwPw, debugger);
    AtDebuggerEntryAdd(debugger, AtDebugEntryNew(cSevNormal, "CLA enabled", ThaPwAdapterClaIsEnabled((AtPw)self) ? "en" : "dis"));
    }

static eAtRet JitterPayloadLengthSet(ThaPwAdapter self)
    {
    AtPw pw = (AtPw)self;
    ThaModulePda pdaModule = ModulePda(pw);
    ThaModulePdaPwJitterPayloadLengthSet(pdaModule, pw, AtPwPayloadSizeGet(pw));

    return cAtOk;
    }

static uint8 NumDs0Slots(ThaPwAdapter self)
    {
	AtUnused(self);
    return 0;
    }

static ThaCdrController CdrController(ThaPwAdapter self)
    {
	AtUnused(self);
    return NULL;
    }

static uint16 DCRRxNcoConfPackLen(ThaPwAdapter self)
    {
    return AtPwPayloadSizeGet((AtPw)self);
    }

static uint16 CDRTimingPktLenInBits(ThaPwAdapter self)
    {
    return (uint16)(AtPwPayloadSizeGet((AtPw)self) * 8);
    }

static eBool NeedUpdateCdrPayload(ThaPwAdapter self)
    {
    ThaCdrController cdrController = mMethodsGet(self)->CdrController(self);
    AtChannel channel = ThaCdrControllerChannelGet(cdrController);
    eAtTimingMode timingMode = AtChannelTimingModeGet(channel);

    /* Not ACR/DCR timing, do not need to update */
    if ((timingMode != cAtTimingModeAcr) && (timingMode != cAtTimingModeDcr))
        return cAtFalse;

    /* Need to update when CDR controller has reference to this PW */
    if ((AtPw)ThaCdrControllerTimingSourceGet(cdrController) == ThaPwAdapterPwGet(self))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet CdrPayloadUpdate(ThaPwAdapter self)
    {
    ThaPwAdapter adapter = (ThaPwAdapter)self;
    ThaCdrController cdrController = mMethodsGet(adapter)->CdrController(adapter);
    eAtRet ret = cAtOk;
    uint32 packetLen = mMethodsGet(adapter)->CDRTimingPktLenInBits(adapter);

    if (cdrController == NULL)
        return cAtOk;

    ret |= ThaCdrControllerRxNcoPackLenSet(cdrController, mMethodsGet(adapter)->DCRRxNcoConfPackLen(adapter));
    if (NeedUpdateCdrPayload(self))
        ret |= ThaCdrControllerTimingPacketLenSet(cdrController, packetLen);

    return ret;
    }

static eAtModulePwRet EthExpectedCVlanSet(AtPw self, tAtEthVlanTag *cVlan)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    eAtRet ret;
    tAtEthVlanTag sVlan, *pSVlan;
    ThaClaPwController claController = ThaPwAdapterClaPwController(mAdapter(self));

    if (!ThaClaPwControllerLookupModeIsSupported(claController, self, cThaPwLookupMode2Vlans))
        return cAtErrorModeNotSupport;

    pSVlan = AtPwEthExpectedSVlanGet(self, &sVlan);
    if (ThaPwActivatorVlanTagsAreUsed(PwActivator(self), mAdapter(self), cVlan, pSVlan))
        return cAtErrorResourceBusy;

    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(self));
        if (cache == NULL)
            return cAtErrorNullPointer;

        mMethodsGet(osal)->MemCpy(osal, &(cache->expectedCVlan), cVlan, sizeof(tAtEthVlanTag));
        cache->useExpectedCVlan = cAtTrue;
        return cAtOk;
        }

    /* Ask CLA module for this */
    ret = ThaClaPwControllerExpectedCVlanSet(ThaPwAdapterClaPwController(mAdapter(self)), self, cVlan);
    if (ret != cAtOk)
        return ret;

    /* Save it */
    if (cVlan != NULL)
		{
		mMethodsGet(osal)->MemCpy(osal, &(mAdapter(self)->expectedCVlan), cVlan, sizeof(tAtEthVlanTag));
		mAdapter(self)->expectedCVlanIsValid = cAtTrue;
		}
    else
		mAdapter(self)->expectedCVlanIsValid = cAtFalse;

    return cAtOk;
    }

static tAtEthVlanTag *EthExpectedCVlanGet(AtPw self, tAtEthVlanTag *cVlan)
    {
    AtOsal osal = AtSharedDriverOsalGet();

    if (!ThaClaPwControllerLookupModeIsSupported(ThaPwAdapterClaPwController(mAdapter(self)), self, cThaPwLookupMode2Vlans))
        return NULL;

    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(self));
        if (cache == NULL)
            return NULL;

        if (cache->useExpectedCVlan)
            {
            mMethodsGet(osal)->MemCpy(osal, cVlan, &(cache->expectedCVlan), sizeof(tAtEthVlanTag));
            return cVlan;
            }

        return NULL;
        }

    if (cVlan == NULL)
        return NULL;

    if (!mAdapter(self)->expectedCVlanIsValid)
        return NULL;

    mMethodsGet(osal)->MemCpy(osal, cVlan, &(mAdapter(self)->expectedCVlan), sizeof(tAtEthVlanTag));

    return cVlan;
    }

static eAtModulePwRet EthExpectedSVlanSet(AtPw self, tAtEthVlanTag *sVlan)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    eAtRet ret;
    ThaClaPwController claController = ThaPwAdapterClaPwController(mAdapter(self));

    if (!ThaClaPwControllerLookupModeIsSupported(claController, self, cThaPwLookupMode2Vlans))
        return cAtErrorModeNotSupport;

    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(self));
        if (cache == NULL)
            return cAtErrorNullPointer;

        if (sVlan)
            {
            mMethodsGet(osal)->MemCpy(osal, &(cache->expectedSVlan), sVlan, sizeof(tAtEthVlanTag));
            cache->useExpectedSVlan = cAtTrue;
            }
        else
            cache->useExpectedSVlan = cAtFalse;

        return cAtOk;
        }

    ret = ThaClaPwControllerExpectedSVlanSet(ThaPwAdapterClaPwController(mAdapter(self)), self, sVlan);
    if (ret != cAtOk)
        return ret;

    if (sVlan)
        {
        mMethodsGet(osal)->MemCpy(osal, &(mAdapter(self)->expectedSVlan), sVlan, sizeof(tAtEthVlanTag));
        mAdapter(self)->expectedSVlanIsValid = cAtTrue;
        }
    else
        mAdapter(self)->expectedSVlanIsValid = cAtFalse;

    return cAtOk;
    }

static tAtEthVlanTag *EthExpectedSVlanGet(AtPw self, tAtEthVlanTag *sVlan)
    {
    AtOsal osal = AtSharedDriverOsalGet();

    if (!ThaClaPwControllerLookupModeIsSupported(ThaPwAdapterClaPwController(mAdapter(self)), self, cThaPwLookupMode2Vlans))
        return NULL;

    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(self));
        if (cache == NULL)
            return NULL;

        if (cache->useExpectedSVlan)
            {
            mMethodsGet(osal)->MemCpy(osal, sVlan, &(cache->expectedSVlan), sizeof(tAtEthVlanTag));
            return sVlan;
            }

        return NULL;
        }

    if (sVlan == NULL)
        return NULL;

    if (!mAdapter(self)->expectedSVlanIsValid)
        return NULL;

    mMethodsGet(osal)->MemCpy(osal, sVlan, &(mAdapter(self)->expectedSVlan), sizeof(tAtEthVlanTag));

    return sVlan;
    }

static uint8 PartOfCircuit(ThaPwAdapter self, AtChannel circuit)
    {
	AtUnused(circuit);
	AtUnused(self);
    return 0;
    }

static uint16 MinPayloadSize(AtPw self, AtChannel circuit, uint32 jitterBufferSize)
    {
    uint32 minPayloadSizeByJitterBuffer = mMethodsGet(mAdapter(self))->AdapterMinPayloadSize(mAdapter(self), circuit, jitterBufferSize);
    uint64 remainingBwForPw = ThaPwEthPortBinderPwRemainingBw(EthPortBinder(self), self);
    uint32 minPayloadSizeByBandwidth = ThaPwAdapterPayloadSizeInByteFromBwCalculate(self, remainingBwForPw);

    return (uint16)mMax(minPayloadSizeByJitterBuffer, minPayloadSizeByBandwidth);
    }

static uint16 AdapterMinPayloadSize(ThaPwAdapter self, AtChannel circuit, uint32 jitterBufferSize)
    {
    ThaModulePda modulePda = ModulePda((AtPw)self);
    uint16 pdaMinPldSizeByBuffer = ThaModulePdaPwMinPayloadSize(modulePda, (AtPw)self, circuit, jitterBufferSize);
    uint16 pdaMinAcceptedPldSize = mMethodsGet(self)->PdaMinPayloadSize(self, modulePda);

    return (uint16)mMax(pdaMinAcceptedPldSize, pdaMinPldSizeByBuffer);
    }

/* Maximum payload size in byte unit that module PDA can accept */
static uint16 PdaMaxPayloadSize(ThaPwAdapter self, ThaModulePda pdaModule)
    {
	AtUnused(pdaModule);
	AtUnused(self);
    return 0x0;
    }

/* Minimum payload size in byte unit that module PDA can accept */
static uint16 PdaMinPayloadSize(ThaPwAdapter self, ThaModulePda pdaModule)
    {
    AtUnused(pdaModule);
    AtUnused(self);
    return 0x0;
    }

static uint16 MaxPayloadSize(AtPw self, AtChannel circuit, uint32 jitterBufferSize)
    {
    uint32 bufferMaxPayloadSizeInByte = ThaModulePdaPwMaxPayloadSize(ModulePda(self), self, circuit, jitterBufferSize);
    uint16 pdaMaxPayloadSizeInByte = mMethodsGet(mAdapter(self))->PdaMaxPayloadSize(mAdapter(self), ModulePda(self));

    return (uint16)(mMin(bufferMaxPayloadSizeInByte, pdaMaxPayloadSizeInByte));
    }

static uint32 MinJitterBufferSize(AtPw self, AtChannel circuit, uint16 payloadSize)
    {
    return ThaModulePdaMinJitterBufferSize(ModulePda(self), self, circuit, payloadSize);
    }

static uint32 MaxJitterBufferSize(AtPw self, AtChannel circuit, uint16 payloadSize)
    {
    return ThaModulePdaMaxJitterBufferSize(ModulePda(self), self, circuit, payloadSize);
    }

static uint32 MinJitterDelay(AtPw self, AtChannel circuit, uint16 payloadSize)
    {
    return ThaModulePdaMinJitterDelay(ModulePda(self), self, circuit, payloadSize);
    }

static uint32 MaxJitterDelay(AtPw self, AtChannel circuit, uint16 payloadSize)
    {
    return ThaModulePdaMaxJitterDelay(ModulePda(self), self, circuit, payloadSize);
    }

static uint32 MaxJitterDelayForJitterBufferSize(AtPw self, uint32 jitterBufferSize_us)
    {
    return ThaModulePdaMaxJitterDelayForJitterBuffer(ModulePda(self), self, jitterBufferSize_us);
    }

static eBool JitterBufferSizeInPacketIsValid(uint16 numPackets)
    {
    if (numPackets == 0)
        return cAtFalse;

    return cAtTrue;
    }

static eAtModulePwRet JitterBufferSizeInPacketSet(AtPw self, uint16 numPackets)
    {
    eAtRet ret = cAtOk;
    uint32 newOccupiedBlock;
    ThaModulePda pdaModule = ModulePda(self);

    if (numPackets < mAdapter(self)->jitterBufferDelayInPkt)
        mChannelError(self, cAtErrorInvlParm);

    /* Check if buffer size is valid */
    if (!JitterBufferSizeInPacketIsValid(numPackets))
        mChannelError(self, cAtErrorInvlParm);

    /* Check if buffer size is in range */
    if (!JitterBufferSizeInPacketIsInRange(self, numPackets, AtPwPayloadSizeGet(self)))
        mChannelError(self, cAtErrorOutOfRangParm);

    /* Turn the flag on,
     * When the pw is activated, base on this flag to configure to hardware by packet unit or us unit */
    mAdapter(self)->jitterSizeIsInPkt = cAtTrue;

    /* Just cache configuration if PW cannot be run yet */
    if (ThaPwAdapterIsLogicPw(mAdapter(self)) || (AtPwBoundCircuitGet(self) == NULL))
        {
        mAdapter(self)->jitterBufferSizeInPkt = numPackets;
        return cAtOk;
        }

    /* Buffer size not change */
    if (AtPwJitterBufferSizeInPacketGet(self) == numPackets)
        return cAtOk;

    /* Check if buffer resource is available */
    newOccupiedBlock = ThaModulePdaPwNumBlockByBufferInPkt(pdaModule, self, numPackets, ThaPwAdapterPayloadSizeInBytes(mAdapter(self)));
    if (!ThaModulePdaResourceIsAvailable(pdaModule, self, newOccupiedBlock))
        mChannelError(self, cAtErrorRsrcNoAvail);

    /* Enough information, apply to hardware */
    ThaPwAdapterStartCenterJitterBuffer(self);
    ret = ThaModulePdaPwJitterBufferSizeInNumOfPacketSet(pdaModule, self, numPackets);
    if (ret == cAtOk)
        {
        mAdapter(self)->jitterBufferSizeInPkt = numPackets;
        mAdapter(self)->jitterBufferSizeInUs = ThaModulePdaCalculateUsByNumberOfPacket(pdaModule, self, numPackets);
        }
    ThaPwAdapterStopCenterJitterBuffer(self);
    return ThaModulePdaTotalInUsedRamBlockUpdate(pdaModule, self, newOccupiedBlock);
    }

static uint16 JitterBufferSizeInPacketGet(AtPw self)
    {
    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        return mAdapter(self)->jitterBufferSizeInPkt;

    /* Should get from HW to reflect real status */
    return ThaModulePdaPwJitterBufferSizeInPacketGet(ModulePda(self), self);
    }

static uint32 JitterBufferDelayInNumOfPktIsInRange(AtPw self, uint32 delayPkt)
    {
    uint32 jbInNumUs, payloadSize;
    ThaModulePda pdaModule = ModulePda(self);
    uint32 min = ThaModulePdaMinNumPacketsForJitterDelay(ModulePda(self), self);
    uint32 max = ThaModulePdaMaxNumPacketsForJitterDelay(ModulePda(self), self);

    if ((delayPkt < min) || (delayPkt > max))
        return cAtFalse;

    payloadSize = AtPwPayloadSizeGet(self);
    jbInNumUs = ThaModulePdaCalculateUsByNumberOfPacket(pdaModule, self, delayPkt);
    min = ThaModulePdaMinNumMicrosecondsForJitterBufferSize(pdaModule, self, payloadSize) / 2;
    max = ThaModulePdaMaxNumMicrosecondsForJitterBufferSize(pdaModule, self, payloadSize);

    return ((jbInNumUs < min) || (jbInNumUs > max)) ? cAtFalse : cAtTrue;
    }

static eAtModulePwRet JitterBufferDelayInPacketSet(AtPw self, uint16 numPackets)
    {
    eAtRet ret = cAtOk;
    ThaModulePda modulePda = ModulePda(self);

    /* Check if input delay is valid */
    if (numPackets == 0)
        mChannelError(self, cAtErrorInvlParm);
    if (numPackets > mAdapter(self)->jitterBufferSizeInPkt)
        mChannelError(self, cAtErrorInvlParm);

    /* Check if jitter delay in range */
    if (!JitterBufferDelayInNumOfPktIsInRange(self, numPackets))
        mChannelError(self, cAtErrorOutOfRangParm);

    /* Turn the flag on,
     * When the pw is activated, base on this flag to configure to hardware by packet unit or us unit */
    mAdapter(self)->jitterDelayIsInPkt = cAtTrue;

    /* Just cache if PW is not activated */
    if (ThaPwAdapterIsLogicPw(mAdapter(self)) || (AtPwBoundCircuitGet(self) == NULL))
        {
        mAdapter(self)->jitterBufferDelayInPkt = numPackets;
        return cAtOk;
        }

    if (AtPwJitterBufferDelayInPacketGet(self) == numPackets)
        return cAtOk;

    /* Enough information, apply to hardware */
    ThaPwAdapterStartCenterJitterBuffer(self);
    ret = ThaModulePdaPwJitterBufferDelayInNumOfPacketSet(modulePda, self, numPackets);
    if (ret == cAtOk)
        {
        mAdapter(self)->jitterBufferDelayInPkt = numPackets;
        mAdapter(self)->jitterBufferDelayInUs = ThaModulePdaCalculateUsByNumberOfPacket(modulePda, self, numPackets);
        }
    ThaPwAdapterStopCenterJitterBuffer(self);

    return ret;
    }

static uint16 JitterBufferDelayInPacketGet(AtPw self)
    {
    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        return mAdapter(self)->jitterBufferDelayInPkt;

    /* Should get from HW to reflect real status */
    return ThaModulePdaPwJitterBufferDelayInPacketGet(ModulePda(self), self);
    }

static eAtRet TxAlarmForce(AtChannel self, uint32 alarmType)
    {
    AtPw pw = (AtPw)self;

    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        return cAtErrorNotActivate;

    return ThaModulePwePwTxAlarmForce(ModulePwe(pw), pw, (eAtPwAlarmType)alarmType);
    }

static eAtRet TxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    AtPw pw = (AtPw)self;

    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        return cAtOk;

    return ThaModulePwePwTxAlarmUnForce(ModulePwe(pw), pw, (eAtPwAlarmType)alarmType);
    }

static eAtModulePwRet IdleCodeSet(AtPw self, uint8 idleCode)
    {
    eAtRet ret = cAtOk;

    if (!ThaModulePwIdleCodeIsSupported(ModulePw(self)))
        return cAtErrorModeNotSupport;

    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(self));
        if (cache)
            cache->idleCode = idleCode;
        return cAtOk;
        }

    if (AtPwIdleCodeGet(self) == idleCode)
        return cAtOk;

    ret |= ThaModulePdaPwIdleCodeSet(ModulePda(self), mAdapter(self), idleCode);
    ret |= ThaModulePwePwIdleCodeSet(ModulePwe(self), mAdapter(self), idleCode);
    return ret;
    }

static uint8 IdleCodeGet(AtPw self)
    {
    if (!ThaModulePwIdleCodeIsSupported(ModulePw(self)))
        return 0x0;

    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(self));
        return (uint8)(cache ? cache->idleCode : 0);
        }

    return ThaModulePdaPwIdleCodeGet(ModulePda(self), mAdapter(self));
    }

static uint32 TxForcedAlarmGet(AtChannel self)
    {
    AtPw pw = (AtPw)self;

    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        return 0x0;

    return ThaModulePwePwTxForcedAlarmGet(ModulePwe(pw), pw);
    }

static uint32 DataRateInBytesPerMs(ThaPwAdapter self, AtChannel circuit)
    {
    AtUnused(self);
    return AtChannelDataRateInBytesPerMs(circuit);
    }

static uint32 TxForcibleAlarmsGet(AtChannel self)
    {
    AtUnused(self);
    return cAtPwAlarmTypeLBit | cAtPwAlarmTypeRBit;
    }

static uint8 PdaCircuitSlice(ThaPwAdapter self, AtChannel circuit)
    {
    AtUnused(self);
    AtUnused(circuit);
    return 0x0;
    }

static uint8 CdrCircuitSlice(ThaPwAdapter self, AtChannel circuit)
    {
    AtUnused(self);
    AtUnused(circuit);
    return 0x0;
    }

static eAtModulePwRet MissingPacketDefectThresholdSet(AtPw self, uint32 numPackets)
    {
    return AtPwMissingPacketDefectThresholdSet((AtPw)ThaPwAdapterPwDefectControllerGet((ThaPwAdapter)self), numPackets);
    }

static uint32 MissingPacketDefectThresholdGet(AtPw self)
    {
    return AtPwMissingPacketDefectThresholdGet((AtPw)ThaPwAdapterPwDefectControllerGet((ThaPwAdapter)self));
    }

static eAtModulePwRet ExcessivePacketLossRateDefectThresholdSet(AtPw self, uint32 numPackets, uint32 timeInSecond)
    {
    return AtPwExcessivePacketLossRateDefectThresholdSet((AtPw)ThaPwAdapterPwDefectControllerGet((ThaPwAdapter)self), numPackets, timeInSecond);
    }

static uint32 ExcessivePacketLossRateDefectThresholdGet(AtPw self, uint32* timeInSecond)
    {
    return AtPwExcessivePacketLossRateDefectThresholdGet((AtPw)ThaPwAdapterPwDefectControllerGet((ThaPwAdapter)self), timeInSecond);
    }

static eAtModulePwRet StrayPacketDefectThresholdSet(AtPw self, uint32 numPackets)
    {
    return AtPwStrayPacketDefectThresholdSet((AtPw)ThaPwAdapterPwDefectControllerGet((ThaPwAdapter)self), numPackets);
    }

static uint32 StrayPacketDefectThresholdGet(AtPw self)
    {
    return AtPwStrayPacketDefectThresholdGet((AtPw)ThaPwAdapterPwDefectControllerGet((ThaPwAdapter)self));
    }

static eAtModulePwRet MalformedPacketDefectThresholdSet(AtPw self, uint32 numPackets)
    {
    return AtPwMalformedPacketDefectThresholdSet((AtPw)ThaPwAdapterPwDefectControllerGet((ThaPwAdapter)self), numPackets);
    }

static uint32 MalformedPacketDefectThresholdGet(AtPw self)
    {
    return AtPwMalformedPacketDefectThresholdGet((AtPw)ThaPwAdapterPwDefectControllerGet((ThaPwAdapter)self));
    }

static eAtModulePwRet RemotePacketLossDefectThresholdSet(AtPw self, uint32 numPackets)
    {
    return AtPwRemotePacketLossDefectThresholdSet((AtPw)ThaPwAdapterPwDefectControllerGet((ThaPwAdapter)self), numPackets);
    }

static uint32 RemotePacketLossDefectThresholdGet(AtPw self)
    {
    return AtPwRemotePacketLossDefectThresholdGet((AtPw)ThaPwAdapterPwDefectControllerGet((ThaPwAdapter)self));
    }

static uint32 LopsSetThresholdGet(AtPw self)
    {
    return AtPwLopsSetThresholdGet((AtPw)ThaPwAdapterPwDefectControllerGet((ThaPwAdapter)self));
    }

static eAtModulePwRet LopsSetThresholdSet(AtPw self, uint32 numPackets)
    {
    return AtPwLopsSetThresholdSet((AtPw)ThaPwAdapterPwDefectControllerGet((ThaPwAdapter)self), numPackets);
    }

static eBool LopsClearThresholdIsSupported(AtPw self)
    {
    return AtPwLopsClearThresholdIsSupported((AtPw)ThaPwAdapterPwDefectControllerGet((ThaPwAdapter)self));
    }

static eAtModulePwRet LopsClearThresholdSet(AtPw self, uint32 numPackets)
    {
    return AtPwLopsClearThresholdSet((AtPw)ThaPwAdapterPwDefectControllerGet((ThaPwAdapter)self), numPackets);
    }

static uint32 LopsClearThresholdGet(AtPw self)
    {
    return AtPwLopsClearThresholdGet((AtPw)ThaPwAdapterPwDefectControllerGet((ThaPwAdapter)self));
    }

static eAtModulePwRet JitterBufferOverrunDefectThresholdSet(AtPw self, uint32 numOverrunEvent)
    {
    return AtPwJitterBufferOverrunDefectThresholdSet((AtPw)ThaPwAdapterPwDefectControllerGet((ThaPwAdapter)self), numOverrunEvent);
    }

static uint32 JitterBufferOverrunDefectThresholdGet(AtPw self)
    {
	return AtPwJitterBufferOverrunDefectThresholdGet((AtPw)ThaPwAdapterPwDefectControllerGet((ThaPwAdapter)self));
    }

static eAtModulePwRet JitterBufferUnderrunDefectThresholdSet(AtPw self, uint32 numUnderrunEvent)
    {
	return AtPwJitterBufferUnderrunDefectThresholdSet((AtPw)ThaPwAdapterPwDefectControllerGet((ThaPwAdapter)self), numUnderrunEvent);
    }

static uint32 JitterBufferUnderrunDefectThresholdGet(AtPw self)
    {
	return AtPwJitterBufferUnderrunDefectThresholdGet((AtPw)ThaPwAdapterPwDefectControllerGet((ThaPwAdapter)self));
    }

static eAtModulePwRet MisConnectionDefectThresholdSet(AtPw self, uint32 numPackets)
    {
    return AtPwMisConnectionDefectThresholdSet((AtPw)ThaPwAdapterPwDefectControllerGet((ThaPwAdapter)self), numPackets);
    }

static uint32 MisConnectionDefectThresholdGet(AtPw self)
    {
    return AtPwMisConnectionDefectThresholdGet((AtPw)ThaPwAdapterPwDefectControllerGet((ThaPwAdapter)self));
    }

static eAtRet RestoreEthHeader(AtPw self)
    {
    eAtRet ret;
    uint8 destMac[cAtMacAddressLen];
    tAtEthVlanTag cVlan, sVlan;

    ret  = AtPwEthDestMacGet(self, destMac);
    ret |= AtPwEthHeaderSet(self, destMac, AtPwEthCVlanGet(self, &cVlan), AtPwEthSVlanGet(self, &sVlan));

    return ret;
    }

static eAtRet WarmRestore(AtChannel self)
    {
    uint8 ethPortId;
    AtPw pw = (AtPw)self;
    ThaModulePda modulePda;
    eAtRet ret;

    /* Restore Eth port */
    ethPortId = ThaModulePwEthPortIdGetFromHwStorage(ModulePw(pw), pw);
    if (ethPortId != cThaPwInvalidEthPortId)
        AtPwEthPortSet(pw, AtModuleEthPortGet((AtModuleEth)ModuleEth(pw), ethPortId));

    ThaPwHeaderControllerWarmRestore(ThaPwAdapterHeaderController(mAdapter(self)));

    mAdapter(self)->enabled = ThaModulePwPwEnableGetFromHwStorage(ModulePw(pw), pw);
    mAdapter(self)->rtpEnableIsSet = cAtTrue; /* RTP has been set before warm restore takes place */

    modulePda = ModulePda(pw);
    mAdapter(self)->reoderEnabled = ThaModulePdaPwReorderingIsEnable(modulePda, pw);

    /* Get jitter buffer size and delay to restore pw database */
    mAdapter(self)->jitterBufferSizeInPkt = ThaModulePdaPwJitterBufferSizeInPacketGet(modulePda, pw);
    mAdapter(self)->jitterBufferDelayInPkt = ThaModulePdaPwJitterBufferDelayInPacketGet(modulePda, pw);
    
    mAdapter(self)->jitterBufferSizeInUs = ThaModulePdaPwJitterBufferSizeGet(modulePda, pw);
    mAdapter(self)->jitterBufferDelayInUs = ThaModulePdaPwJitterBufferDelayGet(modulePda, pw);

    mAdapter(self)->payloadSize  = AtPwPayloadSizeGet(pw);
    mAdapter(self)->rtpIsEnabled = AtPwRtpIsEnabled(pw);
    mAdapter(self)->cwIsEnabled  = AtPwCwIsEnabled(pw);

    /* Restore ethernet header */
    ret = RestoreEthHeader(pw);
    ret |= AtChannelWarmRestore((AtChannel)ThaPwAdapterPwDefectControllerGet((ThaPwAdapter)self));

    return ret;
    }

static AtSurEngine *SurEngineAddress(AtChannel self)
    {
    return &(mAdapter(self)->surEngine);
    }

static void **CacheMemoryAddress(AtChannel self)
    {
    AtChannel realPw = (AtChannel)ThaPwAdapterPwGet((ThaPwAdapter)self);
    return mMethodsGet(realPw)->CacheMemoryAddress(realPw);
    }

static eBool CacheIsEnabled(AtChannel self)
    {
    return mAdapter(self)->enabled;
    }

static tThaPwAdapterStandbyCache *StandbyCache(AtPw self)
    {
    if (mInAccessible(self))
        return &(mAdapter(self)->haCache);
    return NULL;
    }

static void CacheHbceEnable(AtPw self, eBool enable)
    {
    tThaPwAdapterStandbyCache *cache = StandbyCache(self);
    if (cache)
        cache->hbceEnabled = enable;
    }

static eBool CacheHbceIsEnabled(AtPw self)
    {
    tThaPwAdapterStandbyCache *cache = StandbyCache(self);
    return (eBool)(cache ? cache->hbceEnabled : cAtFalse);
    }

static void CachePayloadSizeSet(AtPw self, uint16 sizeInBytes)
    {
    AtUnused(self);
    AtUnused(sizeInBytes);

    /* Payload size is always cached to database in method PayloadSizeSet,
     * Do not need to cache here */
    }

static uint16 CachePayloadSizeGet(AtPw self)
    {
    return mAdapter(self)->payloadSize;
    }

static void CacheClaEnable(AtPw self, eBool enabled)
    {
    tThaPwAdapterStandbyCache *cache = StandbyCache(self);
    if (cache)
        cache->claEnabled = enabled;
    }

static eBool CacheClaIsEnabled(AtPw self)
    {
    tThaPwAdapterStandbyCache *cache = StandbyCache(self);
    return (eBool)(cache ? cache->claEnabled : cAtFalse);
    }

static eAtRet HbceEnable(AtPw self, eBool enable)
    {
    return ThaHbcePwEnable(Hbce(self), mAdapter(self), enable);
    }

static void SerializeCache(AtObject self, AtCoder encoder)
    {
    ThaPwAdapter object = (ThaPwAdapter)self;
    tThaPwConfigCache *cache = object->cache;

    if (cache == NULL)
        return;

    mCacheEncodeUInt(lopsSetThreshold);
    mCacheEncodeUInt(lopsClearThreshold);

    /* Payload */
    mCacheEncodeUInt(priority);
    mCacheEncodeUInt(reorderingEnabled);
    mCacheEncodeUInt(suppressed);

    /* RTP */
    mCacheEncodeUInt(rtpEnabled);
    mCacheEncodeUInt(rtpTimeStampMode);
    mCacheEncodeUInt(expectedRtpSsrc);
    mCacheEncodeUInt(ssrcIsCompared);
    mCacheEncodeUInt(expectedRtpPayloadType);
    mCacheEncodeUInt(payloadTypeIsCompared);

    mCacheEncodeUInt(idleCode);

    /* Control word */
    mCacheEncodeUInt(cwEnabled);
    mCacheEncodeUInt(cwAutoTxLBitEnabled);
    mCacheEncodeUInt(cwAutoRxLBitEnabled);
    mCacheEncodeUInt(cwAutoRBitEnabled);
    mCacheEncodeUInt(cwSequenceMode);
    mCacheEncodeUInt(cwLengthMode);
    mCacheEncodeUInt(cwPktReplaceMode);
    mCacheEncodeUInt(cwAutoTxMBitEnabled);
    mCacheEncodeUInt(cwAutoRxMBitEnabled);

    /* For PW that needs to lookup by VLANs */
    mEncodeVlanCache(expectedCVlan);
    mEncodeVlanCache(expectedSVlan);
    mCacheEncodeUInt(useExpectedCVlan);
    mCacheEncodeUInt(useExpectedSVlan);

    /* CAS IDLE */
    mCacheEncodeUInt(casIdleCode1);
    mCacheEncodeUInt(casIdleCode2);
    mCacheEncodeUInt(casAutoIdleEnabled);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaPwAdapter object = (ThaPwAdapter)self;
    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(enabled);
    mEncodeChannelIdString(pw);

    /* Jitter configuration */
    mEncodeUInt(jitterBufferSizeInUs);
    mEncodeUInt(jitterBufferDelayInUs);
    mEncodeUInt(jitterBufferSizeInPkt);
    mEncodeUInt(jitterBufferDelayInPkt);
    mEncodeUInt(jitterSizeIsInPkt);
    mEncodeUInt(jitterDelayIsInPkt);

    /* Payload */
    mEncodeUInt(payloadSize);
    mEncodeUInt(rtpIsEnabled);
    mEncodeUInt(cwIsEnabled);
    mEncodeUInt(reoderEnabled);
    mEncodeUInt(rtpEnableIsSet);

    /* Binding information */
    mEncodeChannelIdString(ethPort);
    mEncodeChannelIdString(circuit);

    /* Basically, if CESoP does not involve, the reference timing circuit will
     * be the same as the attachment circuit. So, do not need to serialize to
     * not cause mismatch in warm restore testing */
    if (object->acrDcrRefCircuit == object->circuit)
        AtCoderEncodeString(encoder, "none", "acrDcrRefCircuit");
    else
        mEncodeChannelIdString(acrDcrRefCircuit);

    mEncodeVlan(expectedCVlan);
    mEncodeVlan(expectedSVlan);
    mEncodeUInt(expectedCVlanIsValid);
    mEncodeUInt(expectedSVlanIsValid);

    SerializeCache(self, encoder);
    mEncodeObject(hwPw);
    mEncodeObject(defaultDefectController);
    mEncodeObject(debugDefectController);
    mEncodeObject(headerController);
    mEncodeObject(backupHeaderController);

    mEncodeUInt(numRamBlockForBuffer);
    mEncodeUInt64(currentBandwidthInBps);
    }

static eAtRet EthPortQueueSet(AtPw self, uint8 queueId)
    {
    return ThaPwHeaderControllerEthPortQueueSet(ThaPwAdapterHeaderController(mAdapter(self)), queueId);
    }

static uint8 EthPortQueueGet(AtPw self)
    {
    return ThaPwHeaderControllerEthPortQueueGet(ThaPwAdapterHeaderController(mAdapter(self)));
    }

static uint8 EthPortMaxNumQueues(AtPw self)
    {
    return ThaPwHeaderControllerEthPortMaxNumQueues(ThaPwAdapterHeaderController(mAdapter(self)));
    }

static eAtRet ClaEnable(AtPw self, eBool enabled)
    {
    ThaClaController claController = (ThaClaController)ThaPwAdapterClaPwController(mAdapter(self));
    return ThaClaControllerChannelEnable(claController, (AtChannel)self, enabled);
    }

static eBool ClaIsEnabled(AtPw self)
    {
    ThaClaController claController = (ThaClaController)ThaPwAdapterClaPwController(mAdapter(self));
    return ThaClaControllerChannelIsEnabled(claController, (AtChannel)self);
    }

static ThaClaPwController ClaPwController(ThaPwAdapter self)
    {
    if (self->claController)
        return self->claController;

    return ThaModuleClaPwControllerGet((ThaModuleCla)ModuleCla((AtPw)self));
    }

static ThaPwDefectController DebugDefectControllerGet(ThaPwAdapter self)
    {
    if (self->debugDefectController == NULL)
        self->debugDefectController = ThaModulePwDebugDefectControllerCreate((ThaModulePw)AtChannelModuleGet((AtChannel)self), (AtPw)self);
    return self->debugDefectController;
    }

static eBool ShouldClearPmCountersWhenEnabling(void)
    {
    /* Note, although this is necessary but doing so will increase setup time when
     * PW is enabled. Beside this, application is now handling clearing PM
     * counters at first period. So, let's deactivate this logic till we see it
     * is necessary. */
    return cAtFalse;
    }

static eAtRet PmCountersClear(ThaPwAdapter self)
    {
    tAtSurEnginePwPmCounters pwPmCounters;
    AtSurEngine surPwEngine;

    surPwEngine = AtChannelSurEngineGet((AtChannel)self);
    if (surPwEngine == NULL)
        return cAtOk;

    AtSurEngineAllCountersClear(surPwEngine, cAtPmCurrentPeriodRegister, &pwPmCounters);
    AtSurEngineAllCountersClear(surPwEngine, cAtPmPreviousPeriodRegister, &pwPmCounters);

    return cAtOk;
    }

static eAtRet JitterBufferDefaultSet(ThaPwAdapter self)
    {
    eAtRet ret = cAtOk;

    ret |= mMethodsGet(self)->JitterPayloadLengthSet(self);
    ret |= JitterBufferSizeDefaultSet(self);
    ret |= JitterBufferDelayDefaultSet(self);

    return ret;
    }

static eAtRet PtchInsertionModeSet(AtPw self, eAtPtchMode insertMode)
    {
    return ThaModulePwePwPtchInsertionModeSet(ModulePwe(self), self, insertMode);
    }

static eAtPtchMode PtchInsertionModeGet(AtPw self)
    {
    return ThaModulePwePwPtchInsertionModeGet(ModulePwe(self), self);
    }

static eAtModulePwRet TxActiveForce(AtPw self, eBool enable)
    {
    ThaPwAdapter adapter = mAdapter(self);

    if (ThaPwAdapterIsLogicPw(adapter))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(adapter);
        if (cache)
            cache->txActiveForced = enable;
        return cAtOk;
        }

    return ThaModulePweTxPwActiveForce(ModulePwe(self), self, enable);
    }

static eBool TxActiveIsForced(AtPw self)
    {
    ThaPwAdapter adapter = mAdapter(self);

    if (ThaPwAdapterIsLogicPw(adapter))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(adapter);
        if (cache)
            return cache->txActiveForced;
        return cAtFalse;
        }

	return ThaModulePweTxPwActiveIsForced(ModulePwe(self), self);
    }

static eAtRet PtchServiceEnable(AtPw self, eBool enable)
    {
    return ThaModulePwePwPtchServiceEnable(ModulePwe(self), self, enable);
    }

static eBool PtchServiceIsEnabled(AtPw self)
    {
    return ThaModulePwePwPtchServiceIsEnabled(ModulePwe(self), self);
    }

static eBool CanDisableCw(ThaPwAdapter self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet SubPortVlanIndexSet(AtPw self, uint32 subPortVlanIndex)
    {
    ThaPwHeaderController controller = ThaPwAdapterHeaderController(mAdapter(self));
    return ThaPwHeaderControllerSubPortVlanIndexSet(controller, subPortVlanIndex);
    }

static uint32 SubPortVlanIndexGet(AtPw self)
    {
    ThaPwHeaderController controller = ThaPwAdapterHeaderController(mAdapter(self));
    return ThaPwHeaderControllerSubPortVlanIndexGet(controller);
    }

static eAtRet TxHeaderReset(ThaPwAdapter self)
    {
    eAtRet ret;

    ret = ThaPwAdapterTxHeaderLengthReset(self);
    if (ret != cAtOk)
        return ret;

    self->rtpEnableIsSet = cAtFalse;
    return cAtOk;
    }

static uint32 JitterBufferNumBlocksGet(AtPw self)
    {
    return ((ThaPwAdapter)self)->numRamBlockForBuffer;
    }

static eAtRet ReconfigureCheckingEnable(ThaPwAdapter self, eBool enabled)
    {
    self->notCheckReconfigured = enabled ? cAtFalse : cAtTrue;
    return cAtOk;
    }

static eBool ShouldPreventReconfigure(AtChannel self)
    {
    /* Reconfigure checking may be disabled during PW is activated. In this case,
     * will not need to check reconfiguring, so that configuration will always
     * apply to both SW and HW to make both sync */
    if (mThis(self)->notCheckReconfigured)
        return cAtFalse;

    /* Otherwise, let super determine */
    return m_AtChannelMethods->ShouldPreventReconfigure(self);
    }

static eAtRet PwAdapterPweStartRemovingForBinding(AtPw adapter)
    {
    eAtRet ret = cAtOk;
    ThaModulePwe pweModule = ModulePwe(adapter);

    if (!ThaModulePwePwRemovingShouldStart(pweModule, adapter) || ThaPwTypeIsHdlcPppFr(adapter))
        return cAtOk;

    ret = ThaModulePwePwRemovingStart(pweModule, adapter);

    return ret;
    }

static eBool PayloadSizeIsValid(ThaPwAdapter self, uint16 payloadSize)
    {
    AtUnused(self);
    AtUnused(payloadSize);
    return cAtTrue;
    }

static eAtRet PwAdapterStartCenterJitterBufferForBinding(AtPw adapter)
    {
    eAtRet ret = cAtOk;

    if (mInAccessible(adapter) || ThaPwTypeIsHdlcPppFr(adapter))
        return cAtOk;

    /* First time enter removing process */
    ret = ThaModulePdaPwCenterJitterStart(ModulePda(adapter), adapter);

    return ret;
    }

static uint16 MinJitterBufferDelayInPacketGet(AtPw self, AtChannel circuit, uint16 payloadSize)
    {
    AtUnused(circuit);
    AtUnused(payloadSize);
    return (uint16)ThaModulePdaMinNumPacketsForJitterDelay(ModulePda(self), self);
    }

static uint16 MinJitterBufferSizeInPacketGet(AtPw self, AtChannel circuit, uint16 payloadSize)
    {
    uint32 microseconds = AtPwMinJitterBufferSize(self, circuit, payloadSize);
    return ThaModulePdaCalculateNumPacketsByUs(ModulePda(self), self, microseconds);
    }

static uint32 ExtraJitterBufferGet(ThaPwAdapter self)
    {
    AtUnused(self);
    return 0;
    }

static void OverrideAtObject(ThaPwAdapter self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(ThaPwAdapter self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, TypeString);
        mMethodOverride(m_AtChannelOverride, IdString);
        mMethodOverride(m_AtChannelOverride, IdGet);
        mMethodOverride(m_AtChannelOverride, HwIdGet);
        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, IsEnabled);
        mMethodOverride(m_AtChannelOverride, AllCountersGet);
        mMethodOverride(m_AtChannelOverride, AllCountersClear);
        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, TxAlarmForce);
        mMethodOverride(m_AtChannelOverride, TxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcedAlarmGet);
        mMethodOverride(m_AtChannelOverride, TxForcibleAlarmsGet);
        mMethodOverride(m_AtChannelOverride, Debug);
        mMethodOverride(m_AtChannelOverride, AlarmGet);
        mMethodOverride(m_AtChannelOverride, AlarmHistoryClear);
        mMethodOverride(m_AtChannelOverride, AlarmHistoryGet);
        mMethodOverride(m_AtChannelOverride, WarmRestore);
        mMethodOverride(m_AtChannelOverride, CacheMemoryAddress);
        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, InterruptEnable);
        mMethodOverride(m_AtChannelOverride, InterruptDisable);
        mMethodOverride(m_AtChannelOverride, InterruptIsEnabled);
        mMethodOverride(m_AtChannelOverride, AlarmIsSupported);
        mMethodOverride(m_AtChannelOverride, SurEngineAddress);
        mMethodOverride(m_AtChannelOverride, CacheIsEnabled);
        mMethodOverride(m_AtChannelOverride, DebuggerEntriesFill);
        mMethodOverride(m_AtChannelOverride, ShouldPreventReconfigure);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtPw(ThaPwAdapter self)
    {
    AtPw pw = (AtPw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPwMethods = mMethodsGet(pw);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPwOverride, m_AtPwMethods, sizeof(m_AtPwOverride));

        mMethodOverride(m_AtPwOverride, PayloadSizeGet);
        mMethodOverride(m_AtPwOverride, PayloadSizeSet);
        mMethodOverride(m_AtPwOverride, MinPayloadSize);
        mMethodOverride(m_AtPwOverride, MaxPayloadSize);
        mMethodOverride(m_AtPwOverride, LopsThresholdMin);
        mMethodOverride(m_AtPwOverride, LopsThresholdMax);
        mMethodOverride(m_AtPwOverride, LopsSetThresholdSet);
        mMethodOverride(m_AtPwOverride, LopsSetThresholdGet);
        mMethodOverride(m_AtPwOverride, LopsThresholdMax);
        mMethodOverride(m_AtPwOverride, LopsThresholdMin);
        mMethodOverride(m_AtPwOverride, LopsClearThresholdSet);
        mMethodOverride(m_AtPwOverride, LopsClearThresholdGet);
        mMethodOverride(m_AtPwOverride, PrioritySet);
        mMethodOverride(m_AtPwOverride, PriorityGet);
        mMethodOverride(m_AtPwOverride, ReorderingEnable);
        mMethodOverride(m_AtPwOverride, ReorderingIsEnabled);
        mMethodOverride(m_AtPwOverride, JitterBufferSizeSet);
        mMethodOverride(m_AtPwOverride, JitterBufferSizeGet);
        mMethodOverride(m_AtPwOverride, MinJitterBufferSize);
        mMethodOverride(m_AtPwOverride, MaxJitterBufferSize);
        mMethodOverride(m_AtPwOverride, JitterBufferDelaySet);
        mMethodOverride(m_AtPwOverride, JitterBufferDelayGet);
        mMethodOverride(m_AtPwOverride, JitterBufferAndPayloadSizeSet);
        mMethodOverride(m_AtPwOverride, JitterBufferInPacketAndPayloadSizeSet);
        mMethodOverride(m_AtPwOverride, MinJitterDelay);
        mMethodOverride(m_AtPwOverride, MaxJitterDelay);
        mMethodOverride(m_AtPwOverride, MaxJitterDelayForJitterBufferSize);
        mMethodOverride(m_AtPwOverride, NumCurrentPacketsInJitterBuffer);
        mMethodOverride(m_AtPwOverride, NumCurrentAdditionalBytesInJitterBuffer);
        mMethodOverride(m_AtPwOverride, JitterBufferCenter);
        mMethodOverride(m_AtPwOverride, JitterBufferWatermarkMinPackets);
        mMethodOverride(m_AtPwOverride, JitterBufferWatermarkMaxPackets);
        mMethodOverride(m_AtPwOverride, JitterBufferWatermarkIsSupported);
        mMethodOverride(m_AtPwOverride, JitterBufferWatermarkReset);
        mMethodOverride(m_AtPwOverride, MinJitterBufferDelayInPacketGet);
        mMethodOverride(m_AtPwOverride, MinJitterBufferSizeInPacketGet);
        mMethodOverride(m_AtPwOverride, RtpEnable);
        mMethodOverride(m_AtPwOverride, RtpIsEnabled);
        mMethodOverride(m_AtPwOverride, RtpTxPayloadTypeSet);
        mMethodOverride(m_AtPwOverride, RtpTxPayloadTypeGet);
        mMethodOverride(m_AtPwOverride, RtpExpectedPayloadTypeSet);
        mMethodOverride(m_AtPwOverride, RtpExpectedPayloadTypeGet);
        mMethodOverride(m_AtPwOverride, RtpPayloadTypeCompare);
        mMethodOverride(m_AtPwOverride, RtpPayloadTypeIsCompared);
        mMethodOverride(m_AtPwOverride, RtpTxSsrcSet);
        mMethodOverride(m_AtPwOverride, RtpTxSsrcGet);
        mMethodOverride(m_AtPwOverride, RtpExpectedSsrcSet);
        mMethodOverride(m_AtPwOverride, RtpExpectedSsrcGet);
        mMethodOverride(m_AtPwOverride, RtpSsrcCompare);
        mMethodOverride(m_AtPwOverride, RtpSsrcIsCompared);
        mMethodOverride(m_AtPwOverride, RtpTimeStampModeSet);
        mMethodOverride(m_AtPwOverride, RtpTimeStampModeGet);
        mMethodOverride(m_AtPwOverride, RtpTimeStampModeIsSupported);
        mMethodOverride(m_AtPwOverride, CwEnable);
        mMethodOverride(m_AtPwOverride, CwIsEnabled);
        mMethodOverride(m_AtPwOverride, CwAutoTxLBitEnable);
        mMethodOverride(m_AtPwOverride, CwAutoTxLBitIsEnabled);
        mMethodOverride(m_AtPwOverride, CwAutoRxLBitEnable);
        mMethodOverride(m_AtPwOverride, CwAutoRxLBitIsEnabled);
        mMethodOverride(m_AtPwOverride, CwAutoRxLBitCanEnable);
        mMethodOverride(m_AtPwOverride, CwAutoRBitEnable);
        mMethodOverride(m_AtPwOverride, CwAutoRBitIsEnabled);
        mMethodOverride(m_AtPwOverride, CwSequenceModeSet);
        mMethodOverride(m_AtPwOverride, CwSequenceModeGet);
        mMethodOverride(m_AtPwOverride, CwSequenceModeIsSupported);
        mMethodOverride(m_AtPwOverride, CwLengthModeSet);
        mMethodOverride(m_AtPwOverride, CwLengthModeGet);
        mMethodOverride(m_AtPwOverride, CwLengthModeIsSupported);
        mMethodOverride(m_AtPwOverride, CwPktReplaceModeSet);
        mMethodOverride(m_AtPwOverride, CwPktReplaceModeGet);
        mMethodOverride(m_AtPwOverride, LopsPktReplaceModeSet);
        mMethodOverride(m_AtPwOverride, LopsPktReplaceModeGet);
        mMethodOverride(m_AtPwOverride, CanControlLopsPktReplaceMode);
        mMethodOverride(m_AtPwOverride, PktReplaceModeIsSupported);
        mMethodOverride(m_AtPwOverride, PsnSet);
        mMethodOverride(m_AtPwOverride, PsnGet);
        mMethodOverride(m_AtPwOverride, EthPortSet);
        mMethodOverride(m_AtPwOverride, EthPortGet);
        mMethodOverride(m_AtPwOverride, EthFlowSet);
        mMethodOverride(m_AtPwOverride, EthFlowGet);
        mMethodOverride(m_AtPwOverride, EthHeaderSet);
        mMethodOverride(m_AtPwOverride, EthDestMacSet);
        mMethodOverride(m_AtPwOverride, EthDestMacGet);
        mMethodOverride(m_AtPwOverride, EthSrcMacSet);
        mMethodOverride(m_AtPwOverride, EthSrcMacGet);
        mMethodOverride(m_AtPwOverride, EthVlanSet);
        mMethodOverride(m_AtPwOverride, EthCVlanGet);
        mMethodOverride(m_AtPwOverride, EthSVlanGet);
        mMethodOverride(m_AtPwOverride, EthExpectedCVlanSet);
        mMethodOverride(m_AtPwOverride, EthExpectedCVlanGet);
        mMethodOverride(m_AtPwOverride, EthExpectedSVlanSet);
        mMethodOverride(m_AtPwOverride, EthExpectedSVlanGet);
        mMethodOverride(m_AtPwOverride, SuppressEnable);
        mMethodOverride(m_AtPwOverride, SuppressIsEnabled);
        mMethodOverride(m_AtPwOverride, CircuitBind);
        mMethodOverride(m_AtPwOverride, CircuitUnbind);
        mMethodOverride(m_AtPwOverride, InterruptProcess);
        mMethodOverride(m_AtPwOverride, BoundCircuitGet);
        mMethodOverride(m_AtPwOverride, JitterBufferSizeInPacketSet);
        mMethodOverride(m_AtPwOverride, JitterBufferSizeInPacketGet);
        mMethodOverride(m_AtPwOverride, JitterBufferDelayInPacketSet);
        mMethodOverride(m_AtPwOverride, JitterBufferDelayInPacketGet);
        mMethodOverride(m_AtPwOverride, IdleCodeSet);
        mMethodOverride(m_AtPwOverride, IdleCodeGet);
        mMethodOverride(m_AtPwOverride, MissingPacketDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, MissingPacketDefectThresholdGet);
        mMethodOverride(m_AtPwOverride, ExcessivePacketLossRateDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, ExcessivePacketLossRateDefectThresholdGet);
        mMethodOverride(m_AtPwOverride, StrayPacketDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, StrayPacketDefectThresholdGet);
        mMethodOverride(m_AtPwOverride, MalformedPacketDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, MalformedPacketDefectThresholdGet);
        mMethodOverride(m_AtPwOverride, RemotePacketLossDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, RemotePacketLossDefectThresholdGet);
        mMethodOverride(m_AtPwOverride, JitterBufferOverrunDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, JitterBufferOverrunDefectThresholdGet);
        mMethodOverride(m_AtPwOverride, JitterBufferUnderrunDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, JitterBufferUnderrunDefectThresholdGet);
        mMethodOverride(m_AtPwOverride, MisConnectionDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, MisConnectionDefectThresholdGet);
        mMethodOverride(m_AtPwOverride, BackupPsnSet);
        mMethodOverride(m_AtPwOverride, BackupPsnGet);
        mMethodOverride(m_AtPwOverride, BackupEthHeaderSet);
        mMethodOverride(m_AtPwOverride, BackupEthDestMacSet);
        mMethodOverride(m_AtPwOverride, BackupEthDestMacGet);
        mMethodOverride(m_AtPwOverride, BackupEthSrcMacSet);
        mMethodOverride(m_AtPwOverride, BackupEthSrcMacGet);
        mMethodOverride(m_AtPwOverride, BackupEthVlanSet);
        mMethodOverride(m_AtPwOverride, BackupEthCVlanGet);
        mMethodOverride(m_AtPwOverride, BackupEthSVlanGet);
        mMethodOverride(m_AtPwOverride, LopsClearThresholdIsSupported);
        mMethodOverride(m_AtPwOverride, EthPortQueueSet);
        mMethodOverride(m_AtPwOverride, EthPortQueueGet);
        mMethodOverride(m_AtPwOverride, EthPortMaxNumQueues);

		mMethodOverride(m_AtPwOverride, EthCVlanTpidGet);
        mMethodOverride(m_AtPwOverride, EthSVlanTpidGet);
        mMethodOverride(m_AtPwOverride, EthCVlanTpidSet);
        mMethodOverride(m_AtPwOverride, EthSVlanTpidSet);
        
        /* Work with standby driver */
        mMethodOverride(m_AtPwOverride, CachePayloadSizeSet);
        mMethodOverride(m_AtPwOverride, CachePayloadSizeGet);

        /* PTCH */
        mMethodOverride(m_AtPwOverride, PtchInsertionModeSet);
        mMethodOverride(m_AtPwOverride, PtchInsertionModeGet);
        mMethodOverride(m_AtPwOverride, PtchServiceEnable);
        mMethodOverride(m_AtPwOverride, PtchServiceIsEnabled);
        mMethodOverride(m_AtPwOverride, JitterBufferNumBlocksGet);
        
        /* Product specific */
        mMethodOverride(m_AtPwOverride, TxActiveForce);
        mMethodOverride(m_AtPwOverride, TxActiveIsForced);
        mMethodOverride(m_AtPwOverride, SubPortVlanIndexSet);
        mMethodOverride(m_AtPwOverride, SubPortVlanIndexGet);
        }

    mMethodsSet(pw, &m_AtPwOverride);
    }

static void Override(ThaPwAdapter self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtPw(self);
    }

static void MethodsInit(ThaPwAdapter self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, DefaultOffset);
        mMethodOverride(m_methods, CountersCreate);
        mMethodOverride(m_methods, SetDefaultOnBinding);
        mMethodOverride(m_methods, JitterPayloadLengthSet);
        mMethodOverride(m_methods, NumDs0Slots);
        mMethodOverride(m_methods, CdrController);
        mMethodOverride(m_methods, CdrPayloadUpdate);
        mMethodOverride(m_methods, DCRRxNcoConfPackLen);
        mMethodOverride(m_methods, CDRTimingPktLenInBits);
        mMethodOverride(m_methods, PayloadSizeInBytes);
        mMethodOverride(m_methods, DefaultPayloadSize);
        mMethodOverride(m_methods, DefaultJitterBufferSize);
        mMethodOverride(m_methods, DefaultJitterDelay);
        mMethodOverride(m_methods, PartOfCircuit);
        mMethodOverride(m_methods, HwPayloadSizeSet);
        mMethodOverride(m_methods, HwPayloadSizeGet);
        mMethodOverride(m_methods, PdaMaxPayloadSize);
        mMethodOverride(m_methods, PdaMinPayloadSize);
        mMethodOverride(m_methods, NeedToControlCdr);
        mMethodOverride(m_methods, HwEnable);
        mMethodOverride(m_methods, HwIsEnabled);
        mMethodOverride(m_methods, DataRateInBytesPerMs);
        mMethodOverride(m_methods, PdaCircuitSlice);
        mMethodOverride(m_methods, CdrCircuitSlice);
        mMethodOverride(m_methods, AdapterMinPayloadSize);
        mMethodOverride(m_methods, JitterBufferDefaultSet);
        mMethodOverride(m_methods, HwPwTypeSet);
        mMethodOverride(m_methods, CanDisableCw);
        mMethodOverride(m_methods, PayloadSizeIsValid);
        mMethodOverride(m_methods, ExtraJitterBufferGet);
        }

    mMethodsSet(self, &m_methods);
    }

ThaPwAdapter ThaPwAdapterObjectInit(ThaPwAdapter self, AtPw pw)
    {
    AtChannel pwChannel = (AtChannel)pw;

    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPwObjectInit((AtPw)self,
                       (uint16)AtChannelIdGet(pwChannel),
                       (AtModulePw)AtChannelModuleGet(pwChannel),
                       AtPwTypeGet(pw)) == NULL)
        return NULL;

    /* Override */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Private data */
    mAdapter(self)->pw = pw;
    mAdapter(self)->defaultDefectController = ThaModulePwDefectControllerCreate(ModulePw((AtPw)self), (AtPw)self);
    mAdapter(self)->activeDefectController  = mAdapter(self)->defaultDefectController;

    return self;
    }

AtPwCounters ThaPwAdapterCountersGet(ThaPwAdapter self)
    {
    if (self == NULL)
        return NULL;

    /* Create counters object if it has not */
    if (self->counters == NULL)
        self->counters = mMethodsGet(self)->CountersCreate(self);

    return self->counters;
    }

uint32 ThaPwAdapterPayloadSizeInBytes(ThaPwAdapter self)
    {
    if (self)
        return mMethodsGet(self)->PayloadSizeInBytes(self);
    return 0;
    }

ThaPwAdapter ThaPwAdapterGet(AtPw pw)
    {
    eAtPwType pwType;

    if (pw == NULL)
        return NULL;

    pwType = AtPwTypeGet(pw);
    if (pwType == cAtPwTypeCEP)   return ThaPwCepAdapterGet(pw);
    if (pwType == cAtPwTypeSAToP) return ThaPwSAToPAdapterGet(pw);
    if (pwType == cAtPwTypeCESoP) return ThaPwCESoPAdapterGet(pw);
    if (pwType == cAtPwTypeToh)   return ThaPwTohAdapterGet(pw);
    if (pwType == cAtPwTypeHdlc)  return ThaPwHdlcAdapterGet(pw);
    if (pwType == cAtPwTypePpp)   return ThaPwPppAdapterGet(pw);
    if (pwType == cAtPwTypeMlppp) return ThaPwMlpppAdapterGet(pw);

    return NULL;
    }

void ThaPwAdapterDelete(AtPw pw)
    {
    eAtPwType pwType;

    if (pw == NULL)
        return;

    /* The activate may cache this PW before, uncache this */
    ThaPwActivatorPwDelete(PwActivator(pw), ThaPwAdapterGet(pw));

    /* Delete its adapter */
    pwType = AtPwTypeGet(pw);
    if (pwType == cAtPwTypeCEP)   ThaPwCepAdapterDelete(pw);
    if (pwType == cAtPwTypeSAToP) ThaPwSAToPAdapterDelete(pw);
    if (pwType == cAtPwTypeCESoP) ThaPwCESoPAdapterDelete(pw);
    if (pwType == cAtPwTypeToh)   ThaPwTohAdapterDelete(pw);
    if (pwType == cAtPwTypeHdlc)  ThaPwHdlcAdapterDelete(pw);
    if (pwType == cAtPwTypePpp)   ThaPwPppAdapterDelete(pw);
    if (pwType == cAtPwTypeMlppp) ThaPwPppAdapterDelete(pw);

    return;
    }

void ThaPwAdapterAcrDcrRefCircuitSet(ThaPwAdapter self, AtChannel circuit)
    {
    if (self)
        self->acrDcrRefCircuit = circuit;
    }

AtChannel ThaPwAdapterAcrDcrRefCircuitGet(ThaPwAdapter self)
    {
    if (self)
        return self->acrDcrRefCircuit;
    return NULL;
    }

uint16 ThaPwAdapterCDRTimingPktLenInBits(ThaPwAdapter self)
    {
    if (self)
        return mMethodsGet(self)->CDRTimingPktLenInBits(self);
    return 0;
    }

tThaPwConfigCache *ThaPwAdapterCache(ThaPwAdapter self)
    {
    if (self == NULL)
        return NULL;

    if (self->cache == NULL)
        self->cache = CacheCreate(self);

    return self->cache;
    }

eAtRet ThaPwAdapterSetDefaultOnBinding(ThaPwAdapter self, AtChannel circuit)
    {
    if (self)
        return mMethodsGet(self)->SetDefaultOnBinding(self, circuit);
    return cAtError;
    }

eAtRet ThaPwAdapterAcrDcrConfigure(ThaPwAdapter self, AtChannel circuit)
    {
    eAtTimingMode timingMd;

    if (self == NULL)
        return cAtErrorNullPointer;

    timingMd = AtChannelTimingModeGet(circuit);
    if ((timingMd == cAtTimingModeAcr) || (timingMd == cAtTimingModeDcr))
        return AtChannelTimingSet(circuit, timingMd, NULL);

    return cAtOk;
    }

/* Call this function to activate all PSNs of a PW */
eAtRet ThaPwAdapterHbceActivate(ThaPwAdapter self)
    {
    eAtRet ret = cAtOk;

    ret |= ThaPwHeaderControllerHbceActivate(ThaPwAdapterHeaderController(self));
    ret |= ThaPwHeaderControllerHbceActivate(BackupHeaderController(self));

    return ret;
    }

/* Call this function to deactivate all PSNs of a PW */
eAtRet ThaPwAdapterHbceDeactivate(ThaPwAdapter self)
    {
    eAtRet ret = cAtOk;

    ret |= ThaPwHeaderControllerHbceDeactivate(ThaPwAdapterHeaderController(self));
    ret |= ThaPwHeaderControllerHbceDeactivate(BackupHeaderController(self));

    return ret;
    }

AtPw ThaPwAdapterPwGet(ThaPwAdapter self)
    {
    return self ? self->pw : NULL;
    }

ThaHwPw ThaPwAdapterHwPwGet(ThaPwAdapter self)
    {
    return self->hwPw;
    }

void ThaPwAdapterHwPwSet(ThaPwAdapter self, ThaHwPw hwPw)
    {
    self->hwPw = hwPw;
    }

eAtRet ThaPwAdapterEthPortSet(ThaPwAdapter self, AtEthPort ethPort)
    {
    self->ethPort = ethPort;
    return cAtOk;
    }

AtEthPort ThaPwAdapterEthPortGet(ThaPwAdapter self)
    {
    return self ? self->ethPort : NULL;
    }

eAtRet ThaPwAdapterEthFlowSet(ThaPwAdapter self, AtEthFlow ethFlow)
    {
    self->ethFlow = ethFlow;
    return cAtOk;
    }

AtEthFlow ThaPwAdapterEthFlowGet(ThaPwAdapter self)
    {
    return self ? self->ethFlow : NULL;
    }

uint8 ThaPwAdapterPartOfCircuit(ThaPwAdapter self, AtChannel circuit)
    {
    if (self)
        return mMethodsGet(self)->PartOfCircuit(self, circuit);
    return 0;
    }

void ThaPwAdapterCacheDelete(ThaPwAdapter self)
    {
    AtOsal osal;

    if (self->cache == NULL)
        return;

    osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemFree(osal, self->cache);
    self->cache = NULL;
    }

eAtRet ThaPwAdapterBoundCircuitSet(ThaPwAdapter self, AtChannel circuit)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    self->circuit = circuit;
    return cAtOk;
    }

uint16 ThaPwAdapterPayloadSizeGet(ThaPwAdapter self)
    {
    return self->payloadSize;
    }

eBool ThaPwAdapterPayloadSizeIsInRange(AtPw self, uint16 payloadSize)
    {
    AtChannel circuit = AtPwBoundCircuitGet(self);
    uint32 jitterBufferSize = AtPwJitterBufferSizeGet(self);
    ThaModulePw pwModule = (ThaModulePw)AtChannelModuleGet((AtChannel)self);

    if (!ThaModulePwCanCheckPwPayloadSize(pwModule))
        return cAtTrue;

    /* If jitter buffer size has not been configured, just assume the minimum one */
    if (jitterBufferSize == 0)
        jitterBufferSize = AtPwMinJitterBufferSize(self, circuit, payloadSize);

    if ((payloadSize >= AtPwMinPayloadSize(self, circuit, jitterBufferSize)) &&
        (payloadSize <= AtPwMaxPayloadSize(self, circuit, jitterBufferSize)))
        return cAtTrue;

    return cAtFalse;
    }

eAtRet ThaPwAdapterJitterBufferDefaultSet(ThaPwAdapter self)
    {
    if (self)
        return mMethodsGet(self)->JitterBufferDefaultSet(self);
    return cAtErrorModeNotSupport;
    }

eAtRet ThaPwAdapterHbceEnable(AtPw self, eBool enable)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    CacheHbceEnable(self, enable);
    return HbceEnable(self, enable);
    }

eBool ThaPwAdapterHbceIsEnabled(AtPw self)
    {
    if (mInAccessible(self))
        return CacheHbceIsEnabled(self);
    return ThaHbcePwIsEnabled(Hbce(self), mAdapter(self));
    }

eBool ThaPwAdapterIsLogicPw(ThaPwAdapter self)
    {
    if (ThaModulePwDynamicPwAllocation((ThaModulePw)AtChannelModuleGet((AtChannel)self)))
        return ThaPwAdapterHwPwGet(self) ? cAtFalse : cAtTrue;

    return cAtFalse;
    }

eAtRet ThaPwAdapterPweStartRemoving(AtPw adapter)
    {
    eAtRet ret = cAtOk;
    ThaModulePwe pweModule = ModulePwe(adapter);

    if (!ThaModulePwePwRemovingShouldStart(pweModule, adapter) || ThaPwTypeIsHdlcPppFr(adapter))
        return cAtOk;

    if (ThaPwAdapterPwBindingIsInProgress(adapter))
        return cAtOk;

    /* First time enter removing process */
    if (mAdapter(adapter)->pweEnterRemovingProcessTime == 0)
        ret = ThaModulePwePwRemovingStart(pweModule, adapter);

    mAdapter(adapter)->pweEnterRemovingProcessTime++;
    return ret;
    }

eAtRet ThaPwAdapterStartCenterJitterBuffer(AtPw adapter)
    {
    eAtRet ret = cAtOk;

    if (mInAccessible(adapter) || ThaPwTypeIsHdlcPppFr(adapter))
        return cAtOk;

    if (ThaPwAdapterPwBindingIsInProgress(adapter))
        return cAtOk;

    /* First time enter removing process */
    if (mAdapter(adapter)->pdaEnterCenteringProcessTime == 0)
        ret = ThaModulePdaPwCenterJitterStart(ModulePda(adapter), adapter);

    mAdapter(adapter)->pdaEnterCenteringProcessTime++;
    return ret;
    }

eAtRet ThaPwAdapterStartRemoving(AtPw adapter)
    {
    eAtRet ret = cAtOk;

    if (mInAccessible(adapter) || ThaPwTypeIsHdlcPppFr(adapter))
        return cAtOk;

    ret |= ThaPwAdapterPweStartRemoving(adapter);

    if (ThaPwActivatorShouldControlJitterBuffer(PwActivator(adapter)))
        ret |= ThaPwAdapterStartCenterJitterBuffer(adapter);

    return ret;
    }

eAtRet ThaPwAdapterStartRemovingForBinding(AtPw adapter)
    {
    eAtRet ret = cAtOk;

    if (mInAccessible(adapter) || ThaPwTypeIsHdlcPppFr(adapter))
        return cAtOk;

    ret |= PwAdapterPweStartRemovingForBinding(adapter);
    if (ThaPwActivatorShouldControlJitterBuffer(PwActivator(adapter)))
        ret |= PwAdapterStartCenterJitterBufferForBinding(adapter);

    return ret;
    }

eAtRet ThaPwAdapterPweFinishRemovingForBinding(AtPw adapter)
    {
    eAtRet ret = cAtOk;

    if (mInAccessible(adapter) || ThaPwTypeIsHdlcPppFr(adapter))
        return cAtOk;

    ret = ThaModulePwePwRemovingFinish(ModulePwe(adapter), adapter);

    return ret;
    }

eAtRet ThaPwAdapterStopCenterJitterBufferForBinding(AtPw adapter)
    {
    eAtRet ret = cAtOk;

    if (mInAccessible(adapter) || ThaPwTypeIsHdlcPppFr(adapter))
        return cAtOk;

    /* Last time exit removing process */
    ret |= ThaModulePdaPwCenterJitterStop(ModulePda(adapter), adapter);

    return ret;
    }

eAtRet ThaPwAdapterFinishRemovingForBinding(AtPw adapter)
    {
    return ThaModulePwPwFinishRemovingForBinding(ModulePw(adapter), adapter);
    }

eBool ThaPwAdapterPwBindingIsInProgress(AtPw adapter)
    {
    return mAdapter(adapter)->isBindingInProgress;
    }

void ThaPwAdapterPwBindingInProgressSet(AtPw adapter, eBool enable)
    {
    mAdapter(adapter)->isBindingInProgress = enable;
    }

eAtRet ThaPwAdapterPweFinishRemoving(AtPw adapter)
    {
    eAtRet ret = cAtOk;

    if (mInAccessible(adapter) || ThaPwTypeIsHdlcPppFr(adapter))
        return cAtOk;

    if (ThaPwAdapterPwBindingIsInProgress(adapter))
        return cAtOk;

    /* Last time exit removing process */
    if (mAdapter(adapter)->pweEnterRemovingProcessTime == 1)
        ret = ThaModulePwePwRemovingFinish(ModulePwe(adapter), adapter);

    mAdapter(adapter)->pweEnterRemovingProcessTime--;
    return ret;
    }

eAtRet ThaPwAdapterStopCenterJitterBuffer(AtPw adapter)
    {
    eAtRet ret = cAtOk;

    if (mInAccessible(adapter) || ThaPwTypeIsHdlcPppFr(adapter))
        return cAtOk;

    if (ThaPwAdapterPwBindingIsInProgress(adapter))
        return cAtOk;

    /* Last time exit removing process */
    if (mAdapter(adapter)->pdaEnterCenteringProcessTime == 1)
        ret |= ThaModulePdaPwCenterJitterStop(ModulePda(adapter), adapter);

    mAdapter(adapter)->pdaEnterCenteringProcessTime--;
    return ret;
    }

eAtRet ThaPwAdapterFinishRemoving(AtPw adapter)
    {
    return ThaModulePwPwFinishRemoving(ModulePw(adapter), adapter);
    }

eAtRet ThaPwAdapterHwEnable(ThaPwAdapter self, eBool enable)
    {
    eAtRet ret;

    if (self == NULL)
        return cAtErrorNullPointer;

    ret = mMethodsGet(self)->HwEnable(self, enable);
    if (ret != cAtOk)
        return ret;

    if (enable)
        {
        if (ShouldClearPmCountersWhenEnabling())
            PmCountersClear(self);

        ThaPwDefectControllerCurrentStatusReset(ThaPwAdapterPwDefectControllerGet(mAdapter(self)));
        }

    return cAtOk;
    }

eBool ThaPwAdapterHwIsEnabled(ThaPwAdapter self)
    {
    if(self)
        return mMethodsGet(self)->HwIsEnabled(self);
    return cAtErrorNullPointer;
    }

eBool ThaPwAdapterIsEnabled(ThaPwAdapter self)
    {
    return (eBool)(self ? self->enabled : cAtFalse);
    }

ThaClaPwController ThaPwAdapterClaPwController(ThaPwAdapter self)
    {
    if (self)
        return ClaPwController(self);
    return NULL;
    }

void ThaPwAdapterClaPwControllerSet(ThaPwAdapter self, ThaClaPwController controller)
    {
    if (self)
        self->claController = controller;
    }

eAtRet ThaPwAdapterTxHeaderLengthReset(ThaPwAdapter self)
    {
    eAtRet ret = cAtOk;

    ret |= ThaPwHeaderControllerTxHeaderLengthReset(ThaPwAdapterHeaderController(self));
    ret |= ThaPwHeaderControllerTxHeaderLengthReset(BackupHeaderController(self));

    return ret;
    }

uint32 ThaPwAdapterDataRateInBytesPerMs(ThaPwAdapter self, AtChannel circuit)
    {
    if (self)
        return mMethodsGet(self)->DataRateInBytesPerMs(self, circuit);
    return 0;
    }

eAtRet ThaPwAdapterJitterPayloadLengthSet(ThaPwAdapter self)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    return mMethodsGet(self)->JitterPayloadLengthSet(self);
    }

eAtRet ThaPwAdapterStmPortSet(AtPw self, uint8 stmPortId)
    {
    ThaModulePwe pweModule  = ModulePwe(self);

    if (!ThaModuleEthPort10GbSupported(ModuleEth(self)))
        return cAtOk;

    if (pweModule)
        return mMethodsGet(pweModule)->PwStmPortSet(pweModule, self, stmPortId);

    return cAtErrorNullPointer;
    }

uint8 ThaPwAdapterPdaCircuitSlice(ThaPwAdapter self, AtChannel circuit)
    {
    if (self)
        return mMethodsGet(self)->PdaCircuitSlice(self, circuit);
    return 0;
    }

uint8 ThaPwAdapterCdrCircuitSlice(ThaPwAdapter self, AtChannel circuit)
    {
    if (self)
        return mMethodsGet(self)->CdrCircuitSlice(self, circuit);
    return 0;
    }

uint8 ThaPwAdapterPweCircuitSlice(ThaPwAdapter self, AtChannel circuit)
    {
    if (self)
        return mMethodsGet(self)->PweCircuitSlice(self, circuit);
    return 0;
    }

ThaPwDefectController ThaPwAdapterPwDefectControllerGet(ThaPwAdapter self)
    {
    ThaPwAdapter adapter = (ThaPwAdapter)self;
    return adapter ? adapter->activeDefectController : NULL;
    }

ThaHbce ThaPwAdapterHbceGet(AtPw self)
    {
    return Hbce(self);
    }

ThaPwHeaderController ThaPwAdapterHeaderController(ThaPwAdapter self)
    {
    if (self == NULL)
        return NULL;

    if (self->headerController == NULL)
        self->headerController = ThaModulePwHeaderControllerObjectCreate(ModulePw((AtPw)self), (AtPw)self);

    return self->headerController;
    }

ThaPwHeaderController ThaPwAdapterBackupHeaderController(ThaPwAdapter self)
    {
    return (self) ? BackupHeaderController(self) : NULL;
    }

AtSurEngine ThaPwAdapterSurEngine(ThaPwAdapter self)
    {
    return mAdapter(self)->surEngine;
    }

eBool ThaPwAdapterShouldReadCountersFromSurModule(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    ThaModulePw pwModule = (ThaModulePw)AtDeviceModuleGet(device, cAtModulePw);
    return ThaModulePwShouldReadAllCountersFromSurModule(pwModule);
    }

eAtRet ThaPwAdapterNumRamBlockForBufferSet(ThaPwAdapter self, uint32 numRamBlockForBuffer)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    self->numRamBlockForBuffer = numRamBlockForBuffer;
    return cAtOk;
    }

eAtRet ThaPwAdapterClaEnable(AtPw self, eBool enabled)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    CacheClaEnable(self, enabled);
    return ClaEnable(self, enabled);
    }

eBool ThaPwAdapterClaIsEnabled(AtPw self)
    {
    if (self == NULL)
        return cAtFalse;

    if (mInAccessible(self))
        return CacheClaIsEnabled(self);

    return ClaIsEnabled(self);
    }

void ThaPwAdapterSwitchToDefaultDefectController(ThaPwAdapter self)
    {
    if (self)
        self->activeDefectController = self->defaultDefectController;
    }

void ThaPwAdapterSwitchToDebugDefectController(ThaPwAdapter self)
    {
    if (self)
        self->activeDefectController = DebugDefectControllerGet(self);
    }

eAtRet ThaPwAdapterHwPwTypeSet(ThaPwAdapter self)
    {
    if (self)
        return mMethodsGet(self)->HwPwTypeSet(self);
    return cAtErrorNullPointer;
    }

eAtRet ThaPwAdapterHwPayloadSizeSet(ThaPwAdapter self, uint16 payloadSizeInBytes)
    {
    if (self)
        {
        if (mInAccessible(self))
            self->payloadSize = payloadSizeInBytes;
        return mMethodsGet(self)->HwPayloadSizeSet(self, payloadSizeInBytes);
        }
    return cAtErrorNullPointer;
    }

eBool ThaPwTypeIsHdlcPppFr(AtPw self)
    {
    uint8 type = AtPwTypeGet(self);
    if ((type == cAtPwTypeHdlc) ||
        (type == cAtPwTypePpp)  ||
        (type == cAtPwTypeFr)   ||
        (type == cAtPwTypeMlppp))
        return cAtTrue;

    return cAtFalse;
    }

eBool ThaPwTypeIsCesCep(AtPw self)
    {
    eAtPwType pwType = AtPwTypeGet(self);
    if ((pwType == cAtPwTypeCEP)   ||
        (pwType == cAtPwTypeSAToP) ||
        (pwType == cAtPwTypeCESoP))
        return cAtTrue;

    return cAtFalse;
    }

eAtRet ThaPwAdapterTxHeaderReset(ThaPwAdapter self)
    {
    if (self)
        return TxHeaderReset(self);
    return cAtErrorObjectNotExist;
    }

void ThaPwAdapterHeaderControllerSet(ThaPwAdapter self, ThaPwHeaderController controller)
    {
    if (self)
        self->headerController = controller;
    }

void ThaPwAdapterHeaderControllerDelete(ThaPwAdapter self)
    {
    if (self == NULL)
        return;

    AtObjectDelete((AtObject)self->headerController);
    self->headerController = NULL;
    }

eAtRet ThaPwAdapterReconfigureCheckingEnable(ThaPwAdapter self, eBool enabled)
    {
    if (self)
        return ReconfigureCheckingEnable(self, enabled);
    return cAtErrorObjectNotExist;
    }

eAtRet ThaPwAdapterBufferOverrunForce(ThaPwAdapter self, eBool force)
    {
    if (self)
        return BufferOverrunForce(self, force);
    return cAtErrorNullPointer;
    }

uint16 ThaPwAdapterDefaultClassMaxPayloadSize(AtPw self, AtChannel circuit, uint32 jitterBufferSize)
    {
    return MaxPayloadSize(self, circuit, jitterBufferSize);
    }

uint16 ThaPwAdapterDefaultClassPayloadSizeGet(AtPw self)
    {
    return PayloadSizeGet(self);
    }

uint32 ThaPwAdapterDefaultClassMinJitterDelay(AtPw self, AtChannel circuit, uint16 payloadSize)
    {
    return MinJitterDelay(self, circuit, payloadSize);
    }

uint32 ThaPwAdapterDefaultClassMinJitterBufferSize(AtPw self, AtChannel circuit, uint16 payloadSize)
    {
    return MinJitterBufferSize(self, circuit, payloadSize);
    }

eAtModulePwRet ThaPwAdapterDefaultClassPayloadSizeSet(AtPw self, uint16 payloadSizeInBytes)
    {
    return PayloadSizeSet(self, payloadSizeInBytes);
    }

eBool ThaPwAdatperPayloadSizeIsValid(ThaPwAdapter self, uint16 payloadSize)
    {
    if (self)
        return mMethodsGet(self)->PayloadSizeIsValid(self, payloadSize);
    return cAtFalse;
    }

uint32 ThaPwAdapterExtraJitterBufferGet(ThaPwAdapter self)
    {
    if (self)
        return mMethodsGet(self)->ExtraJitterBufferGet(self);
    return 0;
    }


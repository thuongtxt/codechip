/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : ThaPwAdapter.h
 * 
 * Created Date: Mar 14, 2013
 *
 * Description : PW adapter
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPWADAPTER_H_
#define _THAPWADAPTER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPw.h" /* Its super class */
#include "AtPwCounters.h"
#include "../../cla/hbce/ThaHbceClasses.h"
#include "../../cla/controllers/ThaClaPwController.h"
#include "../ThaModulePw.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

typedef enum eThaPwAdapterHeaderWriteMode
    {
    cThaPwAdapterHeaderWriteModeRaw,     /* Will completely take raw buffer input. Used for debug only. */
    cThaPwAdapterHeaderWriteModeDefault  /* Will buffer input but some additional fields may
                                            be changed according to each concrete product.
                                            Currently used for all of products so far */
    }eThaPwAdapterHeaderWriteMode;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPwCESoPAdapter * ThaPwCESoPAdapter;
typedef struct tThaPwCepAdapter   * ThaPwCepAdapter;
typedef struct tThaPwTohAdapter   * ThaPwTohAdapter;
typedef struct tThaPwHdlcAdapter  * ThaPwHdlcAdapter;
typedef struct tThaPwMlpppAdapter * ThaPwMlpppAdapter;

typedef struct tThaPwConfigCache
    {
    /* Threshold */
    uint32 lopsSetThreshold;
    uint32 lopsClearThreshold;
    uint32 missingPacketThreshold;

    /* Payload */
    uint8 priority;
    eBool reorderingEnabled;
    eBool suppressed;

    /* RTP */
    eBool rtpEnabled;
    uint8 rtpTimeStampMode; /* eAtPwRtpTimeStampMode */
    uint32 expectedRtpSsrc;
    eBool ssrcIsCompared;
    uint8 expectedRtpPayloadType;
    eBool payloadTypeIsCompared;

    uint8 idleCode;

    /* Control word */
    eBool cwEnabled;
    eBool cwAutoTxLBitEnabled;
    eBool cwAutoRxLBitEnabled;
    eBool cwAutoRBitEnabled;
    uint8 cwSequenceMode;   /* eAtPwCwSequenceMode */
    uint8 cwLengthMode;     /* eAtPwCwLengthMode */
    uint8 cwPktReplaceMode; /* eAtPwPktReplaceMode */
    uint8 lopsPktReplaceMode; /* eAtPwPktReplaceMode */
    eBool cwAutoTxMBitEnabled;
    eBool cwAutoRxMBitEnabled;

    /* For PW that needs to lookup by VLANs */
    tAtEthVlanTag expectedCVlan;
    tAtEthVlanTag expectedSVlan;
    eBool useExpectedCVlan;
    eBool useExpectedSVlan;

    /* Some products specific */
    eBool txActiveForced;

    /* CAS IDLE code */
    uint8 casIdleCode1;
    uint8 casIdleCode2;
    eBool casAutoIdleEnabled;

    uint8 casMode;
    }tThaPwConfigCache;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Concrete adapter */
ThaPwAdapter ThaPwCepAdapterNew(AtPw pw);
ThaPwAdapter ThaPwCESoPAdapterNew(AtPw pw);
ThaPwAdapter ThaPwCESoPAdapterInBytesNew(AtPw pw);
ThaPwAdapter ThaPwSAToPAdapterNew(AtPw pw);
ThaPwAdapter ThaPwCepFractionalAdapterNew(AtPw pw);
ThaPwAdapter ThaPwTohAdapterNew(AtPw pw);
ThaPwAdapter ThaPwHdlcAdapterNew(AtPw pw);
ThaPwAdapter ThaPwMlpppAdapterNew(AtPw pw);

eAtRet ThaPwAdapterHwEnable(ThaPwAdapter self, eBool enable);
eBool ThaPwAdapterHwIsEnabled(ThaPwAdapter self);
eBool ThaPwAdapterIsEnabled(ThaPwAdapter self);
eAtRet ThaPwAdapterTxHeaderLengthReset(ThaPwAdapter self);
eAtRet ThaPwAdapterTxHeaderReset(ThaPwAdapter self);

/* Internal properties */
AtPwCounters ThaPwAdapterCountersGet(ThaPwAdapter self);
eBool ThaPwAdapterShouldReadCountersFromSurModule(AtChannel adapter);
void ThaPwAdapterAcrDcrRefCircuitSet(ThaPwAdapter self, AtChannel circuit);
AtChannel ThaPwAdapterAcrDcrRefCircuitGet(ThaPwAdapter self);
uint16 ThaPwAdapterCDRTimingPktLenInBits(ThaPwAdapter self);

/* CLA controlling */
eAtRet ThaPwAdapterHbceEnable(AtPw self, eBool enable);
eBool ThaPwAdapterHbceIsEnabled(AtPw self);

uint32 ThaPwAdapterPayloadSizeInBytes(ThaPwAdapter self);
eAtRet ThaPwAdapterJitterBufferDefaultSet(ThaPwAdapter self);
uint32 ThaPwAdapterExtraJitterBufferGet(ThaPwAdapter self);

ThaPwAdapter ThaPwAdapterGet(AtPw pw);
void ThaPwAdapterDelete(AtPw pw);
eBool ThaPwAdapterIsLogicPw(ThaPwAdapter self);

eAtRet ThaPwAdapterSetDefaultOnBinding(ThaPwAdapter self, AtChannel circuit);
eAtRet ThaPwAdapterAcrDcrConfigure(ThaPwAdapter self, AtChannel circuit);

eAtRet ThaPwAdapterHbceActivate(ThaPwAdapter self);
eAtRet ThaPwAdapterHbceDeactivate(ThaPwAdapter self);

eAtRet ThaPwAdapterBoundCircuitSet(ThaPwAdapter self, AtChannel circuit);
AtPw ThaPwAdapterPwGet(ThaPwAdapter self);

tThaPwConfigCache *ThaPwAdapterCache(ThaPwAdapter self);
void ThaPwAdapterCacheDelete(ThaPwAdapter self);

ThaHwPw ThaPwAdapterHwPwGet(ThaPwAdapter self);
void ThaPwAdapterHwPwSet(ThaPwAdapter self, ThaHwPw hwPw);

eAtRet ThaPwAdapterEthPortSet(ThaPwAdapter self, AtEthPort ethPort);
AtEthPort ThaPwAdapterEthPortGet(ThaPwAdapter self);
eAtRet ThaPwAdapterEthFlowSet(ThaPwAdapter self, AtEthFlow ethFlow);
AtEthFlow ThaPwAdapterEthFlowGet(ThaPwAdapter self);

uint8 ThaPwAdapterPartOfCircuit(ThaPwAdapter self, AtChannel circuit);
uint16 ThaPwAdapterPayloadSizeGet(ThaPwAdapter self);
eBool ThaPwAdapterPayloadSizeIsInRange(AtPw self, uint16 payloadSize);
eBool ThaPwAdatperPayloadSizeIsValid(ThaPwAdapter self, uint16 payloadSize);

eAtRet ThaPwAdapterStartRemoving(AtPw adapter);
eAtRet ThaPwAdapterPweStartRemoving(AtPw adapter);
eAtRet ThaPwAdapterStartCenterJitterBuffer(AtPw adapter);
eAtRet ThaPwAdapterFinishRemoving(AtPw adapter);
eAtRet ThaPwAdapterPweFinishRemoving(AtPw adapter);
eAtRet ThaPwAdapterStopCenterJitterBuffer(AtPw adapter);

/* Binding time optimization*/
eAtRet ThaPwAdapterStartRemovingForBinding(AtPw adapter);
eAtRet ThaPwAdapterFinishRemovingForBinding(AtPw adapter);
eBool ThaPwAdapterPwBindingIsInProgress(AtPw adapter);
void ThaPwAdapterPwBindingInProgressSet(AtPw adapter, eBool enable);
eAtRet ThaPwAdapterPweFinishRemovingForBinding(AtPw adapter);
eAtRet ThaPwAdapterStopCenterJitterBufferForBinding(AtPw adapter);

ThaClaPwController ThaPwAdapterClaPwController(ThaPwAdapter self);
void ThaPwAdapterClaPwControllerSet(ThaPwAdapter self, ThaClaPwController controller);

eAtRet ThaPwAdapterClaEnable(AtPw self, eBool enabled);
eBool ThaPwAdapterClaIsEnabled(AtPw self);
uint32 ThaPwAdapterDataRateInBytesPerMs(ThaPwAdapter self, AtChannel circuit);

eAtModulePwRet ThaPwCepAdapterEquip(ThaPwCepAdapter self, AtChannel channel, eBool equip);
uint32 ThaPwCepFractionalRealPayloadSizeCalculate(AtPw pwAdapter, uint32 numRow);
uint32 ThaPwCepFractionalJ1ReturnCalculate(AtPw pwAdapter);
uint32 ThaPwCepFractionalSpePayloadSizeCalculate(AtPw pwAdapter, uint32 numRow);
eAtModulePwRet ThaPwCepEbmEnable(ThaPwCepAdapter self, eBool enable);
eBool ThaPwCepEbmIsEnabled(ThaPwCepAdapter self);
eAtRet ThaPwCepAdapterDebug(AtChannel self);
eAtRet ThaPwAdapterJitterPayloadLengthSet(ThaPwAdapter self);
eBool ThaPwCepAdapterDbEparIsEnabled(ThaPwCepAdapter self);

uint8 ThaPwAdapterPdaCircuitSlice(ThaPwAdapter self, AtChannel circuit);
uint8 ThaPwAdapterPweCircuitSlice(ThaPwAdapter self, AtChannel circuit);
uint8 ThaPwAdapterCdrCircuitSlice(ThaPwAdapter self, AtChannel circuit);
ThaPwDefectController ThaPwAdapterPwDefectControllerGet(ThaPwAdapter self);
eAtRet ThaPwAdapterStmPortSet(AtPw self, uint8 stmPortId);

ThaHbce ThaPwAdapterHbceGet(AtPw self);
ThaPwHeaderController ThaPwAdapterHeaderController(ThaPwAdapter self);
ThaPwHeaderController ThaPwAdapterBackupHeaderController(ThaPwAdapter self);
AtSurEngine ThaPwAdapterSurEngine(ThaPwAdapter self);
eAtRet ThaPwAdapterNumRamBlockForBufferSet(ThaPwAdapter self, uint32 numRamBlockForBuffer);
uint32 ThaPwAdapterTotalHeaderLengthGet(AtPw self);

/* Bandwidth */
uint64 ThaPwAdapterCurrentPwBandwidthInBpsGet(AtPw self);
void ThaPwAdapterCurrentPwBandwidthInBpsSet(AtPw self, uint64 bandwidth);
uint64 ThaPwAdapterBandwidthCalculateWithPayload(AtPw self, uint16 payloadSize);
uint64 ThaPwAdapterBandwidthCalculateWithRtpEnable(AtPw self, eBool enable);
uint64 ThaPwAdapterBandwidthCalculateWithPsn(AtPw self, AtPwPsn psn);
uint64 ThaPwAdapterBandwidthCalculateWithEthHeader(AtPw self, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan);
uint64 ThaPwAdapterBandwidthCalculateWithCircuit(AtPw self, AtChannel circuit);
uint64 ThaPwAdapterBandwidthCalculateWithEthPort(AtPw self, AtEthPort ethPort);
uint64 ThaPwAdapterCurrentPwBandwidthInBpsCalculate(AtPw self);
uint32 ThaPwAdapterPayloadSizeInByteFromBwCalculate(AtPw self, uint64 bandwidth);

/* For debugging */
void ThaPwAdapterCurrentPwBandwidthDebug(AtPw self);
void ThaPwAdapterSwitchToDefaultDefectController(ThaPwAdapter self);
void ThaPwAdapterSwitchToDebugDefectController(ThaPwAdapter self);

eAtRet ThaPwAdapterHwPwTypeSet(ThaPwAdapter self);
eAtRet ThaPwAdapterHwPayloadSizeSet(ThaPwAdapter self, uint16 payloadSizeInBytes);
eBool ThaPwTypeIsHdlcPppFr(AtPw self);
eBool ThaPwTypeIsCesCep(AtPw self);
eAtRet ThaHdlcPwAdapterCircuitUnbind(AtPw self);
void ThaPwAdapterHeaderControllerSet(ThaPwAdapter self, ThaPwHeaderController controller);
void ThaPwAdapterHeaderControllerDelete(ThaPwAdapter self);
eAtRet ThaPwAdapterBufferOverrunForce(ThaPwAdapter self, eBool force);

eAtRet ThaPwAdapterReconfigureCheckingEnable(ThaPwAdapter self, eBool enabled);

/* Default class function */
uint16 ThaPwAdapterDefaultClassMaxPayloadSize(AtPw self, AtChannel circuit, uint32 jitterBufferSize);
uint16 ThaPwAdapterDefaultClassPayloadSizeGet(AtPw self);
uint32 ThaPwAdapterDefaultClassMinJitterDelay(AtPw self, AtChannel circuit, uint16 payloadSize);
uint32 ThaPwAdapterDefaultClassMinJitterBufferSize(AtPw self, AtChannel circuit, uint16 payloadSize);
eAtModulePwRet ThaPwAdapterDefaultClassPayloadSizeSet(AtPw self, uint16 payloadSizeInBytes);

/* CESoP utility */
eBool ThaPwCESoPAdapterWithCas(ThaPwCESoPAdapter self);
eBool ThaPwCESoPAdapterPayloadSizeInFrameIsValid(ThaPwCESoPAdapter self, AtChannel circuit, uint16 numFramesPerPacket);

const uint8* ThaHdlcPwAdapterCircuitEncapHeader(ThaPwHdlcAdapter self, uint8* numBytes);

#ifdef __cplusplus
}
#endif
#endif /* _THAPWADAPTER_H_ */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : ThaPwTohAdapter.c
 *
 * Created Date: Mar 3, 2014
 *
 * Description : TOH PW adapter
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaPwAdapterInternal.h"
#include "../ThaPwToh.h"
#include "../../ocn/ThaModuleOcnInternal.h"
#include "../../sdh/ThaModuleSdh.h"
#include "../activator/ThaPwActivator.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaPwTohAdapterMethods m_methods;

/* Override */
static tThaPwAdapterMethods m_ThaPwAdapterOverride;
static tAtPwMethods         m_AtPwOverride;
static tAtChannelMethods    m_AtChannelOverride;

/* Save super's implementation */
static const tThaPwAdapterMethods *m_ThaPwAdapterMethods = NULL;
static const tAtPwMethods         *m_AtPwMethods         = NULL;
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaPwTohAdapter);
    }

static ThaModulePda ModulePda(AtPw self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaModulePda)AtDeviceModuleGet(device, cThaModulePda);
    }

static ThaModulePwe ModulePwe(AtPw self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaModulePwe)AtDeviceModuleGet(device, cThaModulePwe);
    }

static ThaModuleOcn ModuleOcn(AtPw self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaModuleOcn)AtDeviceModuleGet(device, cThaModuleOcn);
    }

static eAtRet TohByteEnable(ThaPwTohAdapter self, const tThaTohByte *tohByte, eBool enable)
    {
    AtPw pw = (AtPw)self;
    eAtRet ret = ThaModulePwePwTohByteEnable(ModulePwe(pw), pw, tohByte, enable);
    ret |= ThaModulePdaPwTohByteEnable(ModulePda(pw), pw, tohByte, enable);
    ret |= ThaModuleOcnPwTohByteEnable(ModuleOcn(pw), pw, tohByte, enable);

    return ret;
    }

static eAtRet FirstTohByteEnable(ThaPwTohAdapter self, const tThaTohByte *tohByte, eBool enable)
    {
    AtPw pw = (AtPw)self;
    eAtRet ret;

    if (tohByte == NULL)
        return cAtOk;

    ret = ThaModulePwePwTohFirstByteEnable(ModulePwe(pw), pw, tohByte, enable);
    ret |= ThaModulePdaPwTohFirstByteEnable(ModulePda(pw), pw, tohByte, enable);

    return ret;
    }

static eAtRet LastTohByteEnable(ThaPwTohAdapter self, const tThaTohByte *tohByte, eBool enable)
    {
    if (tohByte == NULL)
        return cAtOk;

    return ThaModulePwePwTohLastByteEnable(ModulePwe((AtPw)self), (AtPw)self, tohByte, enable);
    }

static eAtRet Allow(ThaPwTohAdapter self, const tThaTohByte *tohByte)
    {
    ThaPwToh pwToh = (ThaPwToh)ThaPwAdapterPwGet(mAdapter(self));
    tThaTohByte *currentFirst, *currentLast;
    eBool willBeFirst, willBeLast;

    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        return cAtOk;

    currentFirst = ThaPwTohFirstByteGet(pwToh);
    currentLast = ThaPwTohLastByteGet(pwToh);

    willBeFirst = (eBool)((currentFirst == NULL) ? 1 : ThaTohByteIsPrevious(pwToh, tohByte, currentFirst));
    willBeLast  = (eBool)((currentLast  == NULL) ? 1 : ThaTohByteIsAfter(pwToh, tohByte, currentLast));

    /* Allow this TOH byte to be carried */
    TohByteEnable(self, tohByte, cAtTrue);

    /* New TOH byte becomes the first one */
    if (willBeFirst)
        {
        /* Disable current first byte */
        FirstTohByteEnable(self, currentFirst, cAtFalse);

        /* Enable new first byte */
        FirstTohByteEnable(self, tohByte, cAtTrue);
        ThaPwTohFirstByteSet(pwToh, tohByte);
        }

    /* New TOH byte becomes the last one */
    if (willBeLast)
        {
        /* Disable current last byte */
        LastTohByteEnable(self, currentLast, cAtFalse);

        /* Enable new last byte */
        LastTohByteEnable(self, tohByte, cAtTrue);
        ThaPwTohLastByteSet(pwToh, tohByte);
        }

    return cAtOk;
    }

static eAtRet Reject(ThaPwTohAdapter self, const tThaTohByte *tohByte)
    {
    ThaPwToh pwToh = (ThaPwToh)ThaPwAdapterPwGet(mAdapter(self));
    tThaTohByte *currentFirst, *currentLast;
    eBool wasFirst, wasLast;

    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        return cAtOk;

    currentFirst = ThaPwTohFirstByteGet(pwToh);
    currentLast = ThaPwTohLastByteGet(pwToh);

    wasFirst = (eBool)((currentFirst == NULL) ? 1 : ThaTohByteIsPrevious(pwToh, tohByte, currentFirst));
    wasLast  = (eBool)((currentLast  == NULL) ? 1 : ThaTohByteIsAfter(pwToh, tohByte, currentLast));

    /* Reject this TOH byte */
    TohByteEnable(self, tohByte, cAtFalse);

    /* Rejected TOH was the first byte */
    if (wasFirst)
        {
        FirstTohByteEnable(self, tohByte, cAtFalse);
        FirstTohByteEnable(self, currentFirst, cAtTrue);
        }

    /* Rejected TOH was the last byte */
    if (wasLast)
        {
        LastTohByteEnable(self, tohByte, cAtFalse);
        LastTohByteEnable(self, currentLast, cAtTrue);
        }

    return cAtOk;
    }

static eAtRet HwEnable(ThaPwAdapter self, eBool enable)
    {
    eAtRet ret;
    AtPw pw;
    ThaModulePwe pweModule;

    /* Initialize */
    ret = cAtOk;
    pw  = (AtPw)self;
    pweModule = ModulePwe(pw);

    /* Enable */
    if (enable)
        {
        /* Prepare */
        ThaPwAdapterStartRemoving(pw);

        ret |= ThaModulePdaPwTohEnable(ModulePda(pw), pw, cAtTrue);
        ret |= ThaPwAdapterClaEnable(pw, enable);
        ret |= ThaModulePwePwTohEnable(pweModule, pw, enable);

        /* Enable BMT */
        ret |= ThaModulePwePwEnable(pweModule, pw, enable);

        /* Start PW adding */
        ThaPwAdapterFinishRemoving(pw);
        }

    /* Disable */
    else
        {
        ThaPwAdapterStartRemoving(pw);
        ret |= ThaModulePwePwEnable(pweModule, pw, enable);
        ret |= ThaModulePwePwTohEnable(pweModule, pw, enable);
        ret |= ThaModulePdaPwTohEnable(ModulePda(pw), pw, enable);
        ret |= ThaPwAdapterClaEnable(pw, enable);

        ThaModulePdaJitterBufferEmptyWait(ModulePda(pw), pw);

        ret |= ThaModulePwePwRamStatusClear(pweModule, pw);
        ThaPwAdapterFinishRemoving(pw);
        }

    return ret;
    }

/* This function is to get state of CESoP channel */
static eBool IsEnabled(AtChannel self)
    {
    AtPw pw;

    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        return ThaPwAdapterIsEnabled(mAdapter(self));

    pw = (AtPw)self;
    return ThaModulePwePwTohIsEnabled(ModulePwe(pw), pw);
    }

static void MethodsInit(ThaPwTohAdapter self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, Allow);
        mMethodOverride(m_methods, Reject);
        }

    mMethodsSet(self, &m_methods);
    }

static uint16 PdaMaxPayloadSize(ThaPwAdapter self, ThaModulePda pdaModule)
    {
    return ThaModulePdaSAToPMaxPayloadSize(pdaModule, (AtPw)self);
    }

static uint16 PdaMinPayloadSize(ThaPwAdapter self, ThaModulePda pdaModule)
    {
    return ThaModulePdaSAToPMinPayloadSize(pdaModule, (AtPw)self);
    }

static uint16 AdapterMinPayloadSize(ThaPwAdapter self, AtChannel circuit, uint32 jitterBufferSize)
    {
	AtUnused(jitterBufferSize);
	AtUnused(circuit);
	AtUnused(self);
    return 4;
    }

static uint32 DefaultPayloadSize(ThaPwAdapter self, AtChannel circuit)
    {
	AtUnused(circuit);
	AtUnused(self);
    return 8;
    }

static uint16 PayloadSizeGet(AtPw self)
    {
    if (ThaPwAdapterIsLogicPw(mAdapter(self)) || (AtPwBoundCircuitGet(self) == NULL))
        return mAdapter(self)->payloadSize;

    return ThaModulePweTohPayloadSizeGet(ModulePwe(self), self);
    }

static uint16 PdaMaxNumFrame(AtPw self, AtChannel circuit, uint32 jitterBufferSize)
    {
    /* Super return maximum num byte */
    uint32 numBytes = m_AtPwMethods->MaxPayloadSize(self, circuit, jitterBufferSize);
    AtPwToh pwToh = (AtPwToh)ThaPwAdapterPwGet((ThaPwAdapter)self);
    uint16 numBytePerFrame = AtPwTohNumEnabledBytesGet(pwToh);
    uint16 numFrames;

    if (numBytePerFrame == 0)
        return (uint16)DefaultPayloadSize((ThaPwAdapter)self, circuit);

    numFrames = (uint16)(numBytes / numBytePerFrame);
    return (uint16)((numFrames == 0) ? 1 : numFrames);
    }

static uint16 MaxPayloadSize(AtPw self, AtChannel circuit, uint32 jitterBufferSize)
    {
    uint16 pdaMaxNumFrame = PdaMaxNumFrame(self, circuit, jitterBufferSize);
    uint16 pweMaxNumFrame = ThaModulePwePwTohMaxNumFrames(ModulePwe(self));

    return mMin(pdaMaxNumFrame, pweMaxNumFrame);
    }

static eAtRet HwPayloadSizeSet(ThaPwAdapter self, uint16 numFramesPerPacket)
    {
    eAtRet ret = cAtOk;
    ThaModulePwe pweModule;
    AtPw pw = (AtPw)self;
    uint16 payloadSizeInBytes;
    AtPwToh pwToh = (AtPwToh)ThaPwAdapterPwGet(self);

    pweModule = ModulePwe((AtPw)self);
    payloadSizeInBytes = (uint16)(numFramesPerPacket * AtPwTohNumEnabledBytesGet(pwToh));

    /* IMPORTANT: Payload size at PWE is not used for TOH PW and it must be set to 0 */
    ret |= ThaModulePwePayloadSizeSet(pweModule, pw, 0);
    ret |= ThaClaPwControllerPayloadSizeSet(ThaPwAdapterClaPwController(self), pw, (uint16)payloadSizeInBytes);

    /* Special for TOH payload size */
    ret |= ThaModulePweTohPayloadSizeSet(pweModule, pw, numFramesPerPacket);

    return ret;
    }

static eAtRet SetDefaultOnBinding(ThaPwAdapter self, AtChannel circuit)
    {
    AtPw pw = (AtPw)self;
    eAtRet ret = cAtOk;
    ThaModulePda pdaModule = ModulePda(pw);
    ThaModuleOcn ocnModule = ModuleOcn(pw);

    if ((pdaModule == NULL) || (ocnModule == NULL))
        return cAtErrorNullPointer;

    ret |= m_ThaPwAdapterMethods->SetDefaultOnBinding(self, circuit);
    if (ret != cAtOk)
        return ret;

    ret |= ThaModulePdaPwTohModeSet(pdaModule, self);
    ret |= mMethodsGet(ocnModule)->PwTohSourceSet(ocnModule, pw, (AtSdhLine)circuit);

    /* Actually allow all added bytes */
    ret |= ThaPwTohAllBytesAllow((ThaPwToh)ThaPwAdapterPwGet(self));
    return ret;
    }

static eBool NeedToControlCdr(ThaPwAdapter self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static uint32 DataRateInBytesPerMs(ThaPwAdapter self, AtChannel circuit)
    {
    static const uint16 cNumFramePerMs = 8; /* 125 microsecond has 1 STM frame */
    AtPwToh pwToh = (AtPwToh)ThaPwAdapterPwGet(self);
    uint16 numByte = AtPwTohNumEnabledBytesGet(pwToh);
	AtUnused(circuit);

    if (numByte == 0)
        numByte = 1;

    /* One millisecond has 8 frames, PW equips N bytes/frame --> one millisecond has 8*N bytes */
    return (uint32)(cNumFramePerMs * numByte);
    }

static uint16 JitterPayloadLengthCalculate(ThaPwAdapter self)
    {
    AtPwToh pwToh = (AtPwToh)ThaPwAdapterPwGet(self);
    return (uint16)(AtPwTohNumEnabledBytesGet(pwToh) * AtPwPayloadSizeGet((AtPw)self));
    }

static eAtRet JitterPayloadLengthSet(ThaPwAdapter self)
    {
    AtPw pw = (AtPw)self;
    ThaModulePda pdaModule = ModulePda(pw);
    ThaModulePdaPwJitterPayloadLengthSet(pdaModule, pw, JitterPayloadLengthCalculate(self));

    return cAtOk;
    }

static uint32 PayloadSizeInBytes(ThaPwAdapter self)
    {
    AtPwToh pwToh = (AtPwToh)ThaPwAdapterPwGet(self);
    AtPw pwAdapter = (AtPw)self;
    uint16 numByte = AtPwTohNumEnabledBytesGet(pwToh);

    if (numByte == 0)
        numByte = 1;

    return (uint32)(numByte * ThaModulePweTohPayloadSizeGet(ModulePwe(pwAdapter), pwAdapter));
    }

static char* BitmapToString(uint32 bitmap)
    {
    uint8 bit_i;
    static char bitmapStr[256];
    char tempStr[10];

    if (bitmap == 0)
        {
        AtSprintf(bitmapStr, "none");
        return bitmapStr;
        }

    AtOsalMemInit(bitmapStr, 0, sizeof(char) * 150);
    for(bit_i = 0; bit_i < 32; bit_i++)
        {
        if ((bitmap & (cBit0 << bit_i)) == 0)
            continue;

        AtSprintf(tempStr, "[%d:%d]", (uint8)(bit_i / 3), (uint8)(bit_i % 3));
        AtStrcat(bitmapStr, tempStr);
        }

    return bitmapStr;
    }

static void PrintByteString(const tThaTohByte* ohByte)
    {
    eAtSevLevel sevLevel;
    char* byteString = ThaPwTohByteToString(ohByte);
    sevLevel = (AtStrcmp(byteString, "none") == 0) ? cSevCritical  : cSevInfo;

    AtPrintc(sevLevel, "%s\r\n", byteString);
    }

static eAtRet Debug(AtChannel self)
    {
    ThaPwToh pwToh = (ThaPwToh)ThaPwAdapterPwGet(mAdapter(self));
    tThaTohByte tohByte;
    AtPw pw = (AtPw)self;
    uint32 byteBitmaps[12];
    uint8 numSts, sts_i;

    m_AtChannelMethods->Debug(self);

    AtPrintc(cSevInfo, "TOH information\r\n");
    AtPrintc(cSevNormal, "* First byte   : "); PrintByteString(ThaPwTohFirstByteGet(pwToh));
    AtPrintc(cSevNormal, "* Last byte    : "); PrintByteString(ThaPwTohLastByteGet(pwToh));
    AtPrintc(cSevDebug,  "* PWE\r\n");
    AtPrintc(cSevNormal, "    - First byte       : "); PrintByteString(ThaModulePwePwTohFirstByteGet(ModulePwe(pw), pw, &tohByte));
    AtPrintc(cSevNormal, "    - Last byte        : "); PrintByteString(ThaModulePwePwTohLastByteGet(ModulePwe(pw), pw, &tohByte));

    numSts = ThaModulePwePwTohBytesBitmapGet(ModulePwe(pw), pw, byteBitmaps);
    numSts = (uint8)((numSts > 12) ? 12 : numSts);

    for (sts_i = 0; sts_i < numSts; sts_i++)
        AtPrintc(cSevNormal, "    - STS %d bitmaps  : 0x%08x (%s)\r\n", sts_i + 1, byteBitmaps[sts_i], BitmapToString(byteBitmaps[sts_i]));

    AtPrintc(cSevDebug,  "* PDA\r\n");
    AtPrintc(cSevNormal, "    - First byte       : "); PrintByteString(ThaModulePdaPwTohFirstByteGet(ModulePda(pw), pw, &tohByte));
    numSts = ThaModulePdaPwTohBytesBitmapGet(ModulePda(pw), pw, byteBitmaps, 12);
    AtPrintc((numSts > 12) ? cSevWarning : cSevInfo, "    - Number of bitmap : %u\r\n", numSts);

    for (sts_i = 0; sts_i < ((numSts > 12) ? 12 : numSts); sts_i++)
        AtPrintc(cSevNormal, "    - STS %u bitmaps  : 0x%08x (%s)\r\n", sts_i + 1, byteBitmaps[sts_i], BitmapToString(byteBitmaps[sts_i]));

    return cAtOk;
    }

static AtPwCounters CountersCreate(ThaPwAdapter self)
    {
	AtUnused(self);
    return (AtPwCounters)AtPwSAToPCountersNew();
    }

static eAtRet AllCountersReadToClear(AtChannel self, void *pAllCounters, eBool clear)
    {
    eAtRet ret;
	AtUnused(pAllCounters);

    /* Read TDM counters */
    ret = ThaTdmPwCountersReadToClear((AtPw)self, clear);
    if (ret != cAtOk)
        return ret;

    /* More counters may be added in the future */

    return cAtOk;
    }

static eAtRet AllCountersGet(AtChannel self, void *pAllCounters)
    {
    eAtRet ret;

    /* Let the super do first */
    ret = m_AtChannelMethods->AllCountersGet(self, pAllCounters);
    if (ret != cAtOk)
        return ret;

    /* Then read additional counters */
    return AllCountersReadToClear(self, pAllCounters, cAtFalse);
    }

static eAtRet AllCountersClear(AtChannel self, void *pAllCounters)
    {
    eAtRet ret;

    /* Let the super do first */
    ret = m_AtChannelMethods->AllCountersClear(self, pAllCounters);
    if (ret != cAtOk)
        return ret;

    /* Then clear additional counters */
    return AllCountersReadToClear(self, pAllCounters, cAtTrue);
    }

static uint8 PartOfCircuit(ThaPwAdapter self, AtChannel circuit)
    {
	AtUnused(self);
    return ThaModuleSdhPartOfChannel((AtSdhChannel)circuit);
    }

static ThaModulePw ModulePw(AtPw self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaModulePw)AtDeviceModuleGet(device, cAtModulePw);
    }

static eAtModulePwRet CircuitUnbind(AtPw self)
    {
    AtChannel circuit;
    eAtRet ret = cAtOk;
    AtPwToh pw = (AtPwToh)ThaPwAdapterPwGet(mAdapter(self));
    uint8 sts_i, numSts;
    uint32 bitMap;

    /* Already unbind */
    circuit = AtPwBoundCircuitGet(self);
    if (circuit == NULL)
        return cAtOk;

    ThaPwAdapterStartRemoving(self);
    ThaModulePwePwTohEnable(ModulePwe(self), self, cAtFalse);

    AtOsalUSleep(2000);

    numSts = (AtSdhLineRateGet((AtSdhLine)circuit) == cAtSdhLineRateStm1) ? 3 : 12;
    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        if (AtPwTohEnabledBytesByStsGet(pw, sts_i, &bitMap) > 0)
            ret |= AtPwTohByteEnable(pw, sts_i, bitMap, cAtFalse);
        }
        
    /* Disable pw first */
    ret |= AtChannelEnable((AtChannel)self, cAtFalse);

    ret |= ThaPwActivatorPwCircuitUnbind(ThaModulePwActivator(ModulePw(self)), mAdapter(self));
    ThaPwAdapterFinishRemoving(self);

    return (eAtModulePwRet)ret;

    }

static void OverrideAtPw(ThaPwTohAdapter self)
    {
    AtPw pw = (AtPw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPwMethods = mMethodsGet(pw);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPwOverride, m_AtPwMethods, sizeof(m_AtPwOverride));

        mMethodOverride(m_AtPwOverride, MaxPayloadSize);
        mMethodOverride(m_AtPwOverride, PayloadSizeGet);
        mMethodOverride(m_AtPwOverride, CircuitUnbind);
        }

    mMethodsSet(pw, &m_AtPwOverride);
    }

static void OverrideThaPwAdapter(ThaPwTohAdapter self)
    {
    ThaPwAdapter adapter = (ThaPwAdapter)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPwAdapterMethods = mMethodsGet(adapter);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwAdapterOverride, m_ThaPwAdapterMethods, sizeof(m_ThaPwAdapterOverride));

        mMethodOverride(m_ThaPwAdapterOverride, PdaMaxPayloadSize);
        mMethodOverride(m_ThaPwAdapterOverride, PdaMinPayloadSize);
        mMethodOverride(m_ThaPwAdapterOverride, HwPayloadSizeSet);
        mMethodOverride(m_ThaPwAdapterOverride, SetDefaultOnBinding);
        mMethodOverride(m_ThaPwAdapterOverride, DefaultPayloadSize);
        mMethodOverride(m_ThaPwAdapterOverride, JitterPayloadLengthSet);
        mMethodOverride(m_ThaPwAdapterOverride, PayloadSizeInBytes);
        mMethodOverride(m_ThaPwAdapterOverride, NeedToControlCdr);
        mMethodOverride(m_ThaPwAdapterOverride, HwEnable);
        mMethodOverride(m_ThaPwAdapterOverride, CountersCreate);
        mMethodOverride(m_ThaPwAdapterOverride, PartOfCircuit);
        mMethodOverride(m_ThaPwAdapterOverride, DataRateInBytesPerMs);
        mMethodOverride(m_ThaPwAdapterOverride, AdapterMinPayloadSize);
        }

    mMethodsSet(adapter, &m_ThaPwAdapterOverride);
    }

static void OverrideAtChannel(ThaPwTohAdapter self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, IsEnabled);
        mMethodOverride(m_AtChannelOverride, Debug);
        mMethodOverride(m_AtChannelOverride, AllCountersGet);
        mMethodOverride(m_AtChannelOverride, AllCountersClear);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(ThaPwTohAdapter self)
    {
    OverrideThaPwAdapter(self);
    OverrideAtPw(self);
    OverrideAtChannel(self);
    }

static ThaPwTohAdapter ObjectInit(ThaPwTohAdapter self, AtPw pw)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPwAdapterObjectInit((ThaPwAdapter)self, pw) == NULL)
        return NULL;

    /* Override */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwAdapter ThaPwTohAdapterNew(AtPw pw)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwTohAdapter newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return (ThaPwAdapter)ObjectInit(newController, pw);
    }

void ThaPwTohAdapterDelete(AtPw self)
    {
    AtObjectDelete((AtObject)(((ThaPwToh)self)->adapter));
    ((ThaPwToh)self)->adapter = NULL;
    }

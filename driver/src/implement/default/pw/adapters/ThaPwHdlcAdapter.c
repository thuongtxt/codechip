/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : ThaPwHdlcAdapter.c
 *
 * Created Date: Apr 5, 2016
 *
 * Description : Thalassa PW implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../pwe/ThaModulePweInternal.h"
#include "../../cla/hbce/ThaHbce.h"
#include "../../prbs/ThaPrbsEngineInternal.h"
#include "../../encap/dynamic/ThaPhysicalHdlcChannel.h"
#include "../../sur/hard/ThaModuleHardSur.h"
#include "../activator/ThaPwActivator.h"
#include "../headercontrollers/ThaPwHeaderControllerInternal.h"
#include "../ethportbinder/ThaPwEthPortBinder.h"
#include "../ThaPwUtil.h"
#include "ThaPwAdapterInternal.h"
#include "AtHdlcChannel.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mDevice(self) AtChannelDeviceGet((AtChannel)self)
#define mThis(self) ((ThaPwHdlcAdapter)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaPwHdlcAdapterMethods m_methods;

/* Override */
static tAtChannelMethods    m_AtChannelOverride;
static tThaPwAdapterMethods m_ThaPwAdapterOverride;
static tAtPwMethods         m_AtPwOverride;

/* Save super's implementation */
static const tAtPwMethods         *m_AtPwMethods         = NULL;
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;
static const tThaPwAdapterMethods *m_ThaPwAdapterMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtDevice DeviceGet(AtPw self)
    {
    return AtChannelDeviceGet((AtChannel)self);
    }

static ThaModulePw ModulePw(AtPw self)
    {
    return (ThaModulePw)AtDeviceModuleGet(DeviceGet(self), cAtModulePw);
    }

static ThaPwActivator PwActivator(AtPw self)
    {
    return ThaModulePwActivator(ModulePw(self));
    }

static const char *TypeString(AtChannel self)
    {
    AtUnused(self);
    return "pw_hdlc_adapter";
    }

static eAtRet HwPayloadSizeSet(ThaPwAdapter self, uint16 payloadSizeInBytes)
    {
    AtUnused(self);
    AtUnused(payloadSizeInBytes);
    return cAtOk;
    }

static eAtModulePwRet PayloadSizeSet(AtPw self, uint16 payloadSize)
    {
    AtUnused(self);
    AtUnused(payloadSize);
    return cAtErrorModeNotSupport;
    }

static uint16 PayloadSizeGet(AtPw self)
    {
    AtUnused(self);
    return 0;
    }

static uint16 MinPayloadSize(AtPw self, AtChannel circuit, uint32 jitterBufferSize)
    {
    AtUnused(self);
    AtUnused(circuit);
    AtUnused(jitterBufferSize);
    return 0;
    }

static uint16 MaxPayloadSize(AtPw self, AtChannel circuit, uint32 jitterBufferSize)
    {
    AtUnused(self);
    AtUnused(circuit);
    AtUnused(jitterBufferSize);
    return 0;
    }

static uint32 LopsThresholdMax(AtPw self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 LopsThresholdMin(AtPw self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModulePwRet LopsSetThresholdSet(AtPw self, uint32 numPackets)
    {
    AtUnused(self);
    AtUnused(numPackets);
    return cAtErrorModeNotSupport;
    }

static uint32 LopsSetThresholdGet(AtPw self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModulePwRet LopsClearThresholdSet(AtPw self, uint32 numPackets)
    {
    AtUnused(self);
    AtUnused(numPackets);
    return cAtErrorModeNotSupport;
    }

static uint32 LopsClearThresholdGet(AtPw self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModulePwRet ReorderingEnable(AtPw self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorModeNotSupport;
    }

static eBool ReorderingIsEnabled(AtPw self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModulePwRet JitterBufferSizeSet(AtPw self, uint32 microseconds)
    {
    AtUnused(self);
    AtUnused(microseconds);
    return cAtErrorModeNotSupport;
    }

static uint32 JitterBufferSizeGet(AtPw self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 MinJitterBufferSize(AtPw self, AtChannel circuit, uint16 payloadSize)
    {
    AtUnused(self);
    AtUnused(circuit);
    AtUnused(payloadSize);
    return 0;
    }

static uint32 MaxJitterBufferSize(AtPw self, AtChannel circuit, uint16 payloadSize)
    {
    AtUnused(self);
    AtUnused(circuit);
    AtUnused(payloadSize);
    return 0;
    }

static eAtModulePwRet JitterBufferDelaySet(AtPw self, uint32 microseconds)
    {
    AtUnused(self);
    AtUnused(microseconds);
    return cAtErrorModeNotSupport;
    }

static uint32 JitterBufferDelayGet(AtPw self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 MinJitterDelay(AtPw self, AtChannel circuit, uint16 payloadSize)
    {
    AtUnused(self);
    AtUnused(circuit);
    AtUnused(payloadSize);
    return 0;
    }

static uint32 MaxJitterDelay(AtPw self, AtChannel circuit, uint16 payloadSize)
    {
    AtUnused(self);
    AtUnused(circuit);
    AtUnused(payloadSize);
    return 0;
    }

static uint32 NumCurrentPacketsInJitterBuffer(AtPw self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 NumCurrentAdditionalBytesInJitterBuffer(AtPw self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet JitterBufferCenter(AtPw self)
    {
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eAtModulePwRet JitterBufferSizeInPacketSet(AtPw self, uint16 numPackets)
    {
    AtUnused(self);
    AtUnused(numPackets);
    return cAtErrorModeNotSupport;
    }

static uint16 JitterBufferSizeInPacketGet(AtPw self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModulePwRet JitterBufferDelayInPacketSet(AtPw self, uint16 numPackets)
    {
    AtUnused(self);
    AtUnused(numPackets);
    return cAtErrorModeNotSupport;
    }

static uint16 JitterBufferDelayInPacketGet(AtPw self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModulePwRet JitterBufferAndPayloadSizeSet(AtPw self, uint32 jitterBufferSize_us, uint32 jitterDelay_us, uint16 payloadSize)
    {
    AtUnused(self);
    AtUnused(jitterBufferSize_us);
    AtUnused(jitterDelay_us);
    AtUnused(payloadSize);
    return cAtErrorModeNotSupport;
    }

static eAtModulePwRet JitterBufferInPacketAndPayloadSizeSet(AtPw self, uint32 jitterBufferSize_pkt, uint32 jitterDelay_pkt, uint16 payloadSize)
    {
    AtUnused(self);
    AtUnused(jitterBufferSize_pkt);
    AtUnused(jitterDelay_pkt);
    AtUnused(payloadSize);
    return cAtErrorModeNotSupport;
    }

static eAtModulePwRet MissingPacketDefectThresholdSet(AtPw self, uint32 numPackets)
    {
    AtUnused(self);
    AtUnused(numPackets);
    return cAtErrorModeNotSupport;
    }

static uint32 MissingPacketDefectThresholdGet(AtPw self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModulePwRet ExcessivePacketLossRateDefectThresholdSet(AtPw self, uint32 numPackets, uint32 timeInSecond)
    {
    AtUnused(self);
    AtUnused(numPackets);
    AtUnused(timeInSecond);
    return cAtErrorModeNotSupport;
    }

static uint32 ExcessivePacketLossRateDefectThresholdGet(AtPw self, uint32* timeInSecond)
    {
    AtUnused(self);
    if (timeInSecond)
        *timeInSecond = 0;

    return 0;
    }

static eAtModulePwRet StrayPacketDefectThresholdSet(AtPw self, uint32 numPackets)
    {
    AtUnused(self);
    AtUnused(numPackets);
    return cAtErrorModeNotSupport;
    }

static uint32 StrayPacketDefectThresholdGet(AtPw self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModulePwRet MalformedPacketDefectThresholdSet(AtPw self, uint32 numPackets)
    {
    AtUnused(self);
    AtUnused(numPackets);
    return cAtErrorModeNotSupport;
    }

static uint32 MalformedPacketDefectThresholdGet(AtPw self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModulePwRet RemotePacketLossDefectThresholdSet(AtPw self, uint32 numPackets)
    {
    AtUnused(self);
    AtUnused(numPackets);
    return cAtErrorModeNotSupport;
    }

static uint32 RemotePacketLossDefectThresholdGet(AtPw self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModulePwRet JitterBufferUnderrunDefectThresholdSet(AtPw self, uint32 numUnderrunEvent)
    {
    AtUnused(self);
    AtUnused(numUnderrunEvent);
    return cAtErrorModeNotSupport;
    }

static uint32 JitterBufferUnderrunDefectThresholdGet(AtPw self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModulePwRet JitterBufferOverrunDefectThresholdSet(AtPw self, uint32 numOverrunEvent)
    {
    AtUnused(self);
    AtUnused(numOverrunEvent);
    return cAtErrorModeNotSupport;
    }

static uint32 JitterBufferOverrunDefectThresholdGet(AtPw self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModulePwRet MisConnectionDefectThresholdSet(AtPw self, uint32 numPackets)
    {
    AtUnused(self);
    AtUnused(numPackets);
    return cAtErrorModeNotSupport;
    }

static uint32 MisConnectionDefectThresholdGet(AtPw self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModulePwRet CwAutoTxLBitEnable(AtPw self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorModeNotSupport;
    }

static eBool CwAutoTxLBitIsEnabled(AtPw self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModulePwRet CwAutoRxLBitEnable(AtPw self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorModeNotSupport;
    }

static eBool CwAutoRxLBitCanEnable(AtPw self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtFalse;
    }

static eBool CwAutoRxLBitIsEnabled(AtPw self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModulePwRet CwAutoRBitEnable(AtPw self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorModeNotSupport;
    }

static eBool CwAutoRBitIsEnabled(AtPw self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModulePwRet CwSequenceModeSet(AtPw self, eAtPwCwSequenceMode sequenceMode)
    {
    AtUnused(self);
    AtUnused(sequenceMode);
    return cAtErrorModeNotSupport;
    }

static eAtPwCwSequenceMode CwSequenceModeGet(AtPw self)
    {
    AtUnused(self);
    return cAtPwCwSequenceModeInvalid;
    }

static eBool CwSequenceModeIsSupported(AtPw self, eAtPwCwSequenceMode sequenceMode)
    {
    AtUnused(self);
    AtUnused(sequenceMode);
    return cAtFalse;
    }

static eAtModulePwRet CwPktReplaceModeSet(AtPw self, eAtPwPktReplaceMode pktReplaceMode)
    {
    AtUnused(self);
    AtUnused(pktReplaceMode);
    return cAtErrorModeNotSupport;
    }

static eAtPwPktReplaceMode CwPktReplaceModeGet(AtPw self)
    {
    AtUnused(self);
    return cAtPwPktReplaceModeInvalid;
    }

static eAtModulePwRet RtpEnable(AtPw self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorModeNotSupport;
    }

static eBool RtpIsEnabled(AtPw self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModulePwRet RtpTimeStampModeSet(AtPw self, eAtPwRtpTimeStampMode timeStampMode)
    {
    AtUnused(self);
    AtUnused(timeStampMode);
    return cAtErrorModeNotSupport;
    }

static eAtPwRtpTimeStampMode RtpTimeStampModeGet(AtPw self)
    {
    AtUnused(self);
    return cAtPwRtpTimeStampModeInvalid;
    }

static eBool RtpTimeStampModeIsSupported(AtPw self, eAtPwRtpTimeStampMode timeStampMode)
    {
    AtUnused(self);
    AtUnused(timeStampMode);
    return cAtFalse;
    }

static eAtModulePwRet RtpTxSsrcSet(AtPw self, uint32 ssrc)
    {
    AtUnused(self);
    AtUnused(ssrc);
    return cAtErrorModeNotSupport;
    }

static uint32 RtpTxSsrcGet(AtPw self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModulePwRet RtpExpectedSsrcSet(AtPw self, uint32 ssrc)
    {
    AtUnused(self);
    AtUnused(ssrc);
    return cAtErrorModeNotSupport;
    }

static uint32 RtpExpectedSsrcGet(AtPw self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModulePwRet RtpSsrcCompare(AtPw self, eBool enableCompare)
    {
    AtUnused(self);
    AtUnused(enableCompare);
    return cAtErrorModeNotSupport;
    }

static eBool RtpSsrcIsCompared(AtPw self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModulePwRet RtpTxPayloadTypeSet(AtPw self, uint8 payloadType)
    {
    AtUnused(self);
    AtUnused(payloadType);
    return cAtErrorModeNotSupport;
    }

static uint8 RtpTxPayloadTypeGet(AtPw self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModulePwRet RtpExpectedPayloadTypeSet(AtPw self, uint8 payloadType)
    {
    AtUnused(self);
    AtUnused(payloadType);
    return cAtErrorModeNotSupport;
    }

static uint8 RtpExpectedPayloadTypeGet(AtPw self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModulePwRet RtpPayloadTypeCompare(AtPw self, eBool enableCompare)
    {
    AtUnused(self);
    AtUnused(enableCompare);
    return cAtErrorModeNotSupport;
    }

static eBool RtpPayloadTypeIsCompared(AtPw self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModulePwRet IdleCodeSet(AtPw self, uint8 idleCode)
    {
    AtUnused(self);
    AtUnused(idleCode);
    return cAtErrorModeNotSupport;
    }

static uint8 IdleCodeGet(AtPw self)
    {
    AtUnused(self);
    return 0;
    }

static ThaPwEthPortBinder EthPortBinder(AtPw self)
    {
    return ThaModulePwEthPortBinder(ModulePw(self));
    }

static ThaModulePda ModulePda(AtPw self)
    {
    return (ThaModulePda)AtDeviceModuleGet(DeviceGet(self), cThaModulePda);
    }

static eAtModulePwRet EthHeaderSet(AtPw self, uint8 *destMac, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan)
    {
    ThaPwHeaderController headerController = ThaPwAdapterHeaderController(mAdapter(self));
    eAtRet ret;

    ret = ThaPwHeaderControllerEthHeaderSet(headerController, destMac, cVlan, sVlan);
    if (ret != cAtOk)
        return ret;

    return ret;
    }

static eAtModulePwRet CircuitBind(AtPw self, AtChannel circuit)
    {
    eAtRet ret;
    ThaPwAdapter adapter = (ThaPwAdapter)self;
    AtChannel oldCircuit = AtPwBoundCircuitGet(self);

    if (oldCircuit == circuit)
        return cAtOk;

    if (circuit == NULL)
        return cAtErrorModeNotSupport;

    /* PW is being bound to other circuit */
    if ((oldCircuit != NULL) && (oldCircuit != circuit))
        return cAtErrorChannelBusy;

    ret = AtChannelCanBindToPseudowire(circuit, ThaPwAdapterPwGet(adapter));
    if (ret != cAtOk)
        return ret;

    if (!ThaPwActivatorPwBindingInformationIsValid(PwActivator(self), adapter, circuit, AtPwEthPortGet(self)))
        return cAtErrorModeNotSupport;

    mChannelSuccessAssert(self, AtPwCircuitBindingCheck(self, circuit));
    ThaPwAdapterBoundCircuitSet(adapter, circuit);
    ret = ThaPwActivatorPwCircuitBind(PwActivator(self), adapter, circuit);
    if (ret != cAtOk)
        {
        AtChannelBoundPwSet(adapter->circuit, NULL);
        adapter->circuit = NULL;
        return ret;
        }

    return ret;
    }

static eAtModulePwRet CircuitUnbind(AtPw self)
    {
    /* This PW was managed by HDLC channel, application can not un-bind */
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static ThaClaPwController ClaPwController(ThaPwHdlcAdapter self)
    {
    return ThaPwAdapterClaPwController((ThaPwAdapter)self);
    }

static eAtRet PayloadTypeDefaultSet(ThaPwHdlcAdapter self)
    {
    if (self->payloadType == cAtPwHdlcPayloadTypeInvalid)
        self->payloadType = ThaClaPwControllerDefaultPayloadType(ClaPwController(self));

    return AtPwHdlcPayloadTypeSet((AtPwHdlc)ThaPwAdapterPwGet(mAdapter(self)), self->payloadType);
    }

static eAtRet SetDefaultOnBinding(ThaPwAdapter self, AtChannel circuit)
    {
    eAtRet ret;

    AtUnused(circuit);
    ret = ThaPwAdapterHwPwTypeSet(self);
    ret |= PayloadTypeDefaultSet((ThaPwHdlcAdapter)self);
    ret |= AtPwCwEnable((AtPw)self, ThaPwHeaderControllerCwShouldEnableByDefault(ThaPwAdapterHeaderController(self)));

    return ret;
    }

static eAtRet JitterBufferDefaultSet(ThaPwAdapter self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtModulePwRet PsnSet(AtPw self, AtPwPsn psn)
    {
    return ThaPwHeaderControllerPsnSet(ThaPwAdapterHeaderController(mAdapter(self)), psn);
    }

static eAtRet HwPwTypeSet(ThaPwAdapter self)
    {
    AtPw pw = (AtPw)self;
    eAtPwType pwType = AtPwTypeGet(pw);

    return ThaClaPwControllerPwTypeSet(ThaPwAdapterClaPwController(self), pw, pwType);
    }

static AtPwCounters CountersCreate(ThaPwAdapter self)
    {
    AtUnused(self);
    return (AtPwCounters)AtPwHdlcCountersNew();
    }

static eAtRet HwEnable(ThaPwAdapter self, eBool enable)
    {
    eAtRet ret = cAtOk;
    AtPw pw  = (AtPw)self;
    AtChannel circuit = AtPwBoundCircuitGet(pw);

    /* Enable */
    if (enable)
        {
        if (circuit)
            ret |= AtChannelRxTrafficEnable(circuit, enable);

        /* Rx PW, should enable MAP then CLA */
        if (circuit)
            ret |= AtChannelTxTrafficEnable(circuit, enable);

        /* It is for MeqQue */
        if (circuit)
            AtChannelQueueEnable(circuit, cAtTrue);

        ret |= ThaPwAdapterClaEnable(pw, enable);
        }

    /* Disable */
    else
        {
        if (circuit)
            ret |= AtChannelRxTrafficEnable(circuit, enable);

        ret |= ThaPwAdapterClaEnable(pw, enable);

        /* It is for MeqQue */
        if (circuit)
            AtChannelQueueEnable(circuit, cAtFalse);

        if (circuit)
            ret |= AtChannelTxTrafficEnable(circuit, enable);
        }

    return ret;
    }

static ThaModulePwe ModulePwe(AtPw self)
    {
    return (ThaModulePwe)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cThaModulePwe);
    }

static ThaModuleHardSur ModuleSur(AtPw self)
    {
    return (ThaModuleHardSur)AtDeviceModuleGet(mDevice(self), cAtModuleSur);
    }

static eAtRet AllCountersReadToClear(AtChannel self, void *pAllCounters, eBool clear)
    {
    AtPwCounters counters;
    ThaModulePda pdaModule;
    ThaModulePwe pweModule;
    ThaClaPwController claController;
    AtPw pw = (AtPw)self;

    counters = ThaPwAdapterCountersGet((mAdapter(self)));
    if (counters == NULL)
        return cAtOk;

    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        if (clear)
            AtPwCountersInit(counters);
        if (pAllCounters)
            *((AtPwCounters *)pAllCounters) = counters;
        return cAtOk;
        }

    if (ThaPwAdapterShouldReadCountersFromSurModule(self))
        {
        ThaModuleHardSur surModule = ModuleSur(pw);
        eAtRet ret = ThaModuleHardSurHdlcPwCounterLatch(surModule, pw, clear);
        if (ret != cAtOk)
            AtChannelLog(self, cAtLogLevelCritical, AtSourceLocation, "SUR PW counter latching fail with ret = %s\r\n", AtRet2String(ret));

        AtPwCountersTxPacketsSet(counters, ThaModuleHardSurHdlcPwTxGoodPacketGet(surModule, pw));
        AtPwCountersTxPayloadBytesSet(counters, ThaModuleHardSurHdlcPwTxTotalByteGet(surModule, pw));
        AtPwCountersRxPacketsSet(counters, ThaModuleHardSurHdlcPwRxGoodPacketGet(surModule, pw));
        AtPwCountersRxPayloadBytesSet(counters, ThaModuleHardSurHdlcPwRxTotalByteGet(surModule, pw));
        AtPwCountersRxDiscardedPacketsSet(counters, ThaModuleHardSurHdlcPwRxDiscardPktGet(surModule, pw));
        }
    else
        {
        pweModule = ModulePwe(pw);
        pdaModule = ModulePda(pw);
        claController = ThaPwAdapterClaPwController(mAdapter(self));

        AtPwCountersTxPacketsSet(counters, ThaModulePwePwTxPacketsGet(pweModule, pw, clear));
        AtPwCountersTxPayloadBytesSet(counters, ThaModulePwePwTxBytesGet(pweModule, pw, clear));
        AtPwCountersRxPacketsSet(counters, ThaClaPwControllerRxPacketsGet(claController, pw, clear));
        AtPwCountersRxPayloadBytesSet(counters, ThaModulePdaPwRxBytesGet(pdaModule, pw, clear));
        AtPwCountersRxDiscardedPacketsSet(counters, ThaClaPwControllerRxDiscardedPacketsGet(claController, pw, clear));
        }

    if (pAllCounters)
        *((AtPwCounters *)pAllCounters) = counters;

    return cAtOk;
    }

static eAtRet AllCountersGet(AtChannel self, void *pAllCounters)
    {
    return AllCountersReadToClear(self, pAllCounters, cAtFalse);
    }

static eAtRet AllCountersClear(AtChannel self, void *pAllCounters)
    {
    return AllCountersReadToClear(self, pAllCounters, cAtTrue);
    }

static eAtRet PayloadTypeSet(ThaPwHdlcAdapter self, eAtPwHdlcPayloadType payloadType)
    {
    eAtRet ret;
    AtPw pw  = (AtPw)self;

    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        self->payloadType = payloadType;
        return cAtOk;
        }

    ret = ThaModulePdaHdlcPwPayloadTypeSet(ModulePda(pw), pw, payloadType);
    ret |= ThaModulePweHdlcPwPayloadTypeSet(ModulePwe(pw), self, payloadType);
    ret |= ThaClaPwControllerHdlcPwPayloadTypeSet(ClaPwController(mThis(pw)), pw, payloadType);

    return ret;
    }

static eBool CwIsEnabled(AtPw self)
    {
    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(self));
        return (eBool)(cache ? cache->cwEnabled : cAtFalse);
        }

    return ThaModulePwePwCwIsEnabled(ModulePwe(self), self);
    }

static eBool CanDisableCw(ThaPwAdapter self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 TxForcibleAlarmsGet(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet TxAlarmForce(AtChannel self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);
    return cAtErrorModeNotSupport;
    }

static eAtRet TxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);
    return cAtOk;
    }

static eAtModulePwRet SuppressEnable(AtPw self, eBool enable)
    {
    AtUnused(self);
    return (enable) ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool SuppressIsEnabled(AtPw self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void InterruptProcess(AtPw self, uint32 hwPw, AtHal hal)
    {
    AtUnused(self);
    AtUnused(hwPw);
    AtUnused(hal);
    }

/* Defect */
static uint32 DefectGet(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 AlarmGet(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 AlarmHistoryGet(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 AlarmHistoryClear(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    AtUnused(self);
    AtUnused(defectMask);
    AtUnused(enableMask);
    return cAtErrorModeNotSupport;
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet InterruptEnable(AtChannel self)
    {
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eAtRet InterruptDisable(AtChannel self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eBool InterruptIsEnabled(AtChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool AlarmIsSupported(AtChannel self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);
    return cAtFalse;
    }

static const uint8* cHdlcEncapHeader(AtHdlcChannel hdlcChannel, uint8* numBytes)
    {
    AtHdlcLink hdlcLink = AtHdlcChannelHdlcLinkGet(hdlcChannel);
    eAtHdlcPduType pduType = AtHdlcLinkPduTypeGet(hdlcLink);

    *numBytes = 4;
    if (pduType == cAtHdlcPduTypeIPv4)
        {
        static const uint8 cHdlcIpv4[] = {0x0F, 0x00, 0x08, 0x00};
        return cHdlcIpv4;
        }

    if (pduType == cAtHdlcPduTypeIPv6)
        {
        static const uint8 cHdlcIpv6[] = {0x0F, 0x00, 0x86, 0xDD};
        return cHdlcIpv6;
        }

    if (pduType == cAtHdlcPduTypeEthernet)
        {
        static const uint8 cHdlcEth[] = {0x0F, 0x00, 0x65, 0x58};
        return cHdlcEth;
        }

    if (pduType == cAtHdlcPduTypeAny)
        {
        static const uint8 cHdlcAny[] = {0x0F, 0x00, 0xFF, 0xFF};
        return cHdlcAny;
        }

    *numBytes = 0;
    return NULL;
    }

static const uint8* CircuitEncapHeader(ThaPwHdlcAdapter self, uint8* numBytes)
    {
    AtHdlcChannel hdlcChannel = (AtHdlcChannel)AtPwBoundCircuitGet((AtPw)self);
    eAtHdlcFrameType frameType;

    *numBytes = 0;
    if (hdlcChannel == NULL)
        return NULL;

    frameType = AtHdlcChannelFrameTypeGet(hdlcChannel);
    if (frameType == cAtHdlcFrmCiscoHdlc)
        return cHdlcEncapHeader(hdlcChannel, numBytes);

    return NULL;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPwHdlcAdapter);
    }

static void OverrideAtChannel(ThaPwHdlcAdapter self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, TypeString);
        mMethodOverride(m_AtChannelOverride, AllCountersGet);
        mMethodOverride(m_AtChannelOverride, AllCountersClear);
        mMethodOverride(m_AtChannelOverride, TxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, TxAlarmForce);
        mMethodOverride(m_AtChannelOverride, TxForcibleAlarmsGet);
        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, AlarmGet);
        mMethodOverride(m_AtChannelOverride, AlarmHistoryGet);
        mMethodOverride(m_AtChannelOverride, AlarmHistoryClear);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, InterruptEnable);
        mMethodOverride(m_AtChannelOverride, InterruptDisable);
        mMethodOverride(m_AtChannelOverride, InterruptIsEnabled);
        mMethodOverride(m_AtChannelOverride, AlarmIsSupported);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtPw(ThaPwHdlcAdapter self)
    {
    AtPw pw = (AtPw)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPwMethods = mMethodsGet(pw);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPwOverride, m_AtPwMethods, sizeof(m_AtPwOverride));

        mMethodOverride(m_AtPwOverride, PayloadSizeGet);
        mMethodOverride(m_AtPwOverride, PayloadSizeSet);
        mMethodOverride(m_AtPwOverride, MinPayloadSize);
        mMethodOverride(m_AtPwOverride, MaxPayloadSize);
        mMethodOverride(m_AtPwOverride, LopsThresholdMin);
        mMethodOverride(m_AtPwOverride, LopsThresholdMax);
        mMethodOverride(m_AtPwOverride, LopsSetThresholdSet);
        mMethodOverride(m_AtPwOverride, LopsSetThresholdGet);
        mMethodOverride(m_AtPwOverride, LopsThresholdMax);
        mMethodOverride(m_AtPwOverride, LopsThresholdMin);
        mMethodOverride(m_AtPwOverride, LopsClearThresholdSet);
        mMethodOverride(m_AtPwOverride, LopsClearThresholdGet);
        mMethodOverride(m_AtPwOverride, ReorderingEnable);
        mMethodOverride(m_AtPwOverride, ReorderingIsEnabled);
        mMethodOverride(m_AtPwOverride, JitterBufferSizeSet);
        mMethodOverride(m_AtPwOverride, JitterBufferSizeGet);
        mMethodOverride(m_AtPwOverride, MinJitterBufferSize);
        mMethodOverride(m_AtPwOverride, MaxJitterBufferSize);
        mMethodOverride(m_AtPwOverride, JitterBufferDelaySet);
        mMethodOverride(m_AtPwOverride, JitterBufferDelayGet);
        mMethodOverride(m_AtPwOverride, JitterBufferAndPayloadSizeSet);
        mMethodOverride(m_AtPwOverride, JitterBufferInPacketAndPayloadSizeSet);
        mMethodOverride(m_AtPwOverride, MinJitterDelay);
        mMethodOverride(m_AtPwOverride, MaxJitterDelay);
        mMethodOverride(m_AtPwOverride, NumCurrentPacketsInJitterBuffer);
        mMethodOverride(m_AtPwOverride, NumCurrentAdditionalBytesInJitterBuffer);
        mMethodOverride(m_AtPwOverride, JitterBufferCenter);
        mMethodOverride(m_AtPwOverride, PsnSet);
        mMethodOverride(m_AtPwOverride, EthHeaderSet);
        mMethodOverride(m_AtPwOverride, CircuitBind);
        mMethodOverride(m_AtPwOverride, CircuitUnbind);
        mMethodOverride(m_AtPwOverride, JitterBufferSizeInPacketSet);
        mMethodOverride(m_AtPwOverride, JitterBufferSizeInPacketGet);
        mMethodOverride(m_AtPwOverride, JitterBufferDelayInPacketSet);
        mMethodOverride(m_AtPwOverride, JitterBufferDelayInPacketGet);
        mMethodOverride(m_AtPwOverride, MissingPacketDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, MissingPacketDefectThresholdGet);
        mMethodOverride(m_AtPwOverride, ExcessivePacketLossRateDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, ExcessivePacketLossRateDefectThresholdGet);
        mMethodOverride(m_AtPwOverride, StrayPacketDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, StrayPacketDefectThresholdGet);
        mMethodOverride(m_AtPwOverride, MalformedPacketDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, MalformedPacketDefectThresholdGet);
        mMethodOverride(m_AtPwOverride, RemotePacketLossDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, RemotePacketLossDefectThresholdGet);
        mMethodOverride(m_AtPwOverride, JitterBufferOverrunDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, JitterBufferOverrunDefectThresholdGet);
        mMethodOverride(m_AtPwOverride, JitterBufferUnderrunDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, JitterBufferUnderrunDefectThresholdGet);
        mMethodOverride(m_AtPwOverride, MisConnectionDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, MisConnectionDefectThresholdGet);
        mMethodOverride(m_AtPwOverride, CwAutoTxLBitEnable);
        mMethodOverride(m_AtPwOverride, CwAutoTxLBitIsEnabled);
        mMethodOverride(m_AtPwOverride, CwAutoRxLBitEnable);
        mMethodOverride(m_AtPwOverride, CwAutoRxLBitIsEnabled);
        mMethodOverride(m_AtPwOverride, CwAutoRxLBitCanEnable);
        mMethodOverride(m_AtPwOverride, CwAutoRBitEnable);
        mMethodOverride(m_AtPwOverride, CwAutoRBitIsEnabled);
        mMethodOverride(m_AtPwOverride, CwSequenceModeSet);
        mMethodOverride(m_AtPwOverride, CwSequenceModeGet);
        mMethodOverride(m_AtPwOverride, CwSequenceModeIsSupported);
        mMethodOverride(m_AtPwOverride, CwPktReplaceModeSet);
        mMethodOverride(m_AtPwOverride, CwPktReplaceModeGet);
        mMethodOverride(m_AtPwOverride, RtpEnable);
        mMethodOverride(m_AtPwOverride, RtpIsEnabled);
        mMethodOverride(m_AtPwOverride, RtpTxPayloadTypeSet);
        mMethodOverride(m_AtPwOverride, RtpTxPayloadTypeGet);
        mMethodOverride(m_AtPwOverride, RtpExpectedPayloadTypeSet);
        mMethodOverride(m_AtPwOverride, RtpExpectedPayloadTypeGet);
        mMethodOverride(m_AtPwOverride, RtpPayloadTypeCompare);
        mMethodOverride(m_AtPwOverride, RtpPayloadTypeIsCompared);
        mMethodOverride(m_AtPwOverride, RtpTxSsrcSet);
        mMethodOverride(m_AtPwOverride, RtpTxSsrcGet);
        mMethodOverride(m_AtPwOverride, RtpExpectedSsrcSet);
        mMethodOverride(m_AtPwOverride, RtpExpectedSsrcGet);
        mMethodOverride(m_AtPwOverride, RtpSsrcCompare);
        mMethodOverride(m_AtPwOverride, RtpSsrcIsCompared);
        mMethodOverride(m_AtPwOverride, RtpTimeStampModeSet);
        mMethodOverride(m_AtPwOverride, RtpTimeStampModeGet);
        mMethodOverride(m_AtPwOverride, RtpTimeStampModeIsSupported);
        mMethodOverride(m_AtPwOverride, IdleCodeSet);
        mMethodOverride(m_AtPwOverride, IdleCodeGet);
        mMethodOverride(m_AtPwOverride, CwIsEnabled);
        mMethodOverride(m_AtPwOverride, SuppressEnable);
        mMethodOverride(m_AtPwOverride, SuppressIsEnabled);
        mMethodOverride(m_AtPwOverride, InterruptProcess);
        }

    mMethodsSet(pw, &m_AtPwOverride);
    }

static void OverrideThaPwAdapter(ThaPwHdlcAdapter self)
    {
    ThaPwAdapter adapter = (ThaPwAdapter)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPwAdapterMethods = mMethodsGet(adapter);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwAdapterOverride, m_ThaPwAdapterMethods, sizeof(m_ThaPwAdapterOverride));

        mMethodOverride(m_ThaPwAdapterOverride, JitterBufferDefaultSet);
        mMethodOverride(m_ThaPwAdapterOverride, SetDefaultOnBinding);
        mMethodOverride(m_ThaPwAdapterOverride, HwPayloadSizeSet);
        mMethodOverride(m_ThaPwAdapterOverride, HwPwTypeSet);
        mMethodOverride(m_ThaPwAdapterOverride, CountersCreate);
        mMethodOverride(m_ThaPwAdapterOverride, HwEnable);
        mMethodOverride(m_ThaPwAdapterOverride, CanDisableCw);
        }

    mMethodsSet(adapter, &m_ThaPwAdapterOverride);
    }

static void MethodsInit(ThaPwHdlcAdapter self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, PayloadTypeSet);
        mMethodOverride(m_methods, CircuitEncapHeader);
        }

    mMethodsSet(self, &m_methods);
    }

static void Override(ThaPwHdlcAdapter self)
    {
    OverrideAtChannel(self);
    OverrideAtPw(self);
    OverrideThaPwAdapter(self);
    }

ThaPwHdlcAdapter ThaPwHdlcAdapterObjectInit(ThaPwHdlcAdapter self, AtPw pw)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPwAdapterObjectInit((ThaPwAdapter)self, pw) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwAdapter ThaPwHdlcAdapterNew(AtPw pw)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwHdlcAdapter newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return (ThaPwAdapter)ThaPwHdlcAdapterObjectInit(newController, pw);
    }
    
eAtRet ThaHdlcPwAdapterCircuitUnbind(AtPw adapter)
    {
    AtChannel circuit;
    eAtRet ret = cAtOk;
    AtPrbsEngine prbsEngine;

    /* Already unbind */
    circuit = AtPwBoundCircuitGet(adapter);
    if (circuit == NULL)
        return cAtOk;

    /* Disable pw first */
    ret |= ThaPwEthPortBinderPwEnable(EthPortBinder(adapter), adapter, cAtFalse);

    /* Reset to default */
    if (ThaPwTypeIsHdlcPppFr(adapter))
        ret |= AtPwHdlcPayloadTypeSet((AtPwHdlc)ThaPwAdapterPwGet((ThaPwAdapter)adapter), cAtPwHdlcPayloadTypeFull);

    ret |= ThaPwActivatorPwCircuitUnbind(ThaModulePwActivator(ModulePw(adapter)), mAdapter(adapter));

    if (ret != cAtOk)
        return ret;

    prbsEngine = AtChannelPrbsEngineGet(circuit);
    if ((prbsEngine != NULL) && (AtPrbsEngineMonitoringSideGet(prbsEngine) == cAtPrbsSidePsn))
        AtPrbsEngineRxEnable(prbsEngine, cAtFalse);

    return ret;
    }

const uint8* ThaHdlcPwAdapterCircuitEncapHeader(ThaPwHdlcAdapter self, uint8* numBytes)
    {
    *numBytes = 0;
    if (self)
        return mMethodsGet(self)->CircuitEncapHeader(self, numBytes);
    return NULL;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : ThaPwCepAdapter.c
 *
 * Created Date: Apr 02, 2013
 *
 * Description : Thalassa PW implementation
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPwCep.h"
#include "ThaPwAdapterInternal.h"
#include "../../sdh/ThaModuleSdh.h"
#include "../../sdh/ThaSdhVc.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../sur/hard/ThaModuleHardSur.h"
#include "../debugger/ThaPwDebugger.h"

/*--------------------------- Define -----------------------------------------*/
#define cMinCepJitterDelayInUs 125

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaPwCepAdapter)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaPwCepAdapterMethods m_methods;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtChannelMethods    m_AtChannelOverride;
static tAtPwMethods         m_AtPwOverride;
static tThaPwAdapterMethods m_ThaPwAdapterOverride;

/* Save super's implementation */
static const tAtObjectMethods     *m_AtObjectMethods     = NULL;
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;
static const tThaPwAdapterMethods *m_ThaPwAdapterMethods = NULL;
static const tAtPwMethods         *m_AtPwMethods         = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet Debug(AtChannel self)
    {
    eAtRet ret;

    /* Let the super do first */
    ret = m_AtChannelMethods->Debug(self);
    if (ret != cAtOk)
        return ret;

    ThaPwDebuggerPwCepDebug(ThaModulePwPwDebuggerGet((ThaModulePw)AtChannelModuleGet(self)), (AtPw)self);
    return cAtOk;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPwCepAdapter);
    }

static ThaModulePda ModulePda(AtPw self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaModulePda)AtDeviceModuleGet(device, cThaModulePda);
    }

static ThaModulePwe ModulePwe(AtPw self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaModulePwe)AtDeviceModuleGet(device, cThaModulePwe);
    }

static ThaModuleHardSur ModuleSur(AtPw self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaModuleHardSur)AtDeviceModuleGet(device, cAtModuleSur);
    }

static const char *TypeString(AtChannel self)
    {
	AtUnused(self);
    return "pw_cep_adapter";
    }

static AtPwCounters CountersCreate(ThaPwAdapter self)
    {
	AtUnused(self);
    return (AtPwCounters)AtPwCepCountersNew();
    }

static uint32 DefaultOffset(AtPw self)
    {
    return AtChannelIdGet((AtChannel)self);
    }

static uint8 PartOfCircuit(ThaPwAdapter self, AtChannel circuit)
    {
	AtUnused(self);
    return ThaModuleSdhPartOfChannel((AtSdhChannel)circuit);
    }

static eAtRet AllCountersReadToClear(AtChannel self, void *pAllCounters, eBool clear)
    {
    eAtRet ret;
    AtPw pw = (AtPw)self;
    AtPwCepCounters counters = (AtPwCepCounters)ThaPwAdapterCountersGet(mAdapter(self));

    AtUnused(pAllCounters);

    /* Read TDM counter */
    ret = ThaTdmPwCountersReadToClear(pw, clear);
    if (ret != cAtOk)
        return ret;

    if (ThaPwAdapterShouldReadCountersFromSurModule(self))
        {
        ThaModuleHardSur surModule = ModuleSur(pw);
        AtPwCepCountersTxNbitPacketsSet(counters, ThaModuleHardSurPwTxNbitPacketsGet(surModule, pw, clear));
        AtPwCepCountersTxPbitPacketsSet(counters, ThaModuleHardSurPwTxPbitPacketsGet(surModule, pw, clear));
        AtPwCepCountersRxNbitPacketsSet(counters, ThaModuleHardSurPwRxNbitPacketsGet(surModule, pw, clear));
        AtPwCepCountersRxPbitPacketsSet(counters, ThaModuleHardSurPwRxPbitPacketsGet(surModule, pw, clear));
        }

    else
        {
        ThaModulePwe pweModule = ModulePwe(pw);
        ThaClaPwController claController = ThaPwAdapterClaPwController(mAdapter(self));
        AtPwCepCountersTxNbitPacketsSet(counters, ThaModulePwePwTxNbitPacketsGet(pweModule, pw, clear));
        AtPwCepCountersTxPbitPacketsSet(counters, ThaModulePwePwTxPbitPacketsGet(pweModule, pw, clear));
        AtPwCepCountersRxNbitPacketsSet(counters, ThaClaPwControllerRxNbitPacketsGet(claController, pw, clear));
        AtPwCepCountersRxPbitPacketsSet(counters, ThaClaPwControllerRxPbitPacketsGet(claController, pw, clear));
        }

    return cAtOk;
    }

static eAtRet AllCountersGet(AtChannel self, void *pAllCounters)
    {
    eAtRet ret;

    /* Let the super do first */
    ret = m_AtChannelMethods->AllCountersGet(self, pAllCounters);
    if (ret != cAtOk)
        return ret;

    /* Then read additional counters */
    return AllCountersReadToClear(self, pAllCounters, cAtFalse);
    }

static eAtRet AllCountersClear(AtChannel self, void *pAllCounters)
    {
    eAtRet ret;

    /* Let the super do first */
    ret = m_AtChannelMethods->AllCountersClear(self, pAllCounters);
    if (ret != cAtOk)
        return ret;

    /* Then clear additional counters */
    return AllCountersReadToClear(self, pAllCounters, cAtTrue);
    }

static uint32 DefaultPayloadSize(ThaPwAdapter self, AtChannel circuit)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)circuit;
    uint8 numSts = AtSdhChannelNumSts(sdhChannel);
    eAtSdhChannelType vcType = AtSdhChannelTypeGet(sdhChannel);
	AtUnused(self);

    if ((vcType == cAtSdhChannelTypeVc4) || (vcType == cAtSdhChannelTypeVc3))
        {
        if ((AtSdhChannelMapTypeGet(sdhChannel) == cAtSdhVcMapTypeVc4MapC4) ||
            (AtSdhChannelMapTypeGet(sdhChannel) == cAtSdhVcMapTypeVc3MapC3))
            return 783;

        if ((AtSdhChannelMapTypeGet(sdhChannel) == cAtSdhVcMapTypeVc3MapDe3) &&
            ThaSdhAuVcMapDe3LiuIsEnabled((ThaSdhAuVc)circuit))
            return 783;

        else
            return 3; /* Fractional 3 rows */
        }

    if (vcType == cAtSdhChannelTypeVc12)
    	return 140;
    	
    if (vcType == cAtSdhChannelTypeVc11)    
    	return 104;
    	
    if ((vcType == cAtSdhChannelTypeVc4_4c) || (numSts == 12)) 
    	return 783;
    	
    if ((vcType == cAtSdhChannelTypeVc4_16c) || (numSts == 48)) 
    	return 783;
    	
    if ((vcType == cAtSdhChannelTypeVc4_64c) || (numSts == 192))
        return 783;

    if (vcType == cAtSdhChannelTypeVc4_nc)
        return 783;

    return 0;
    }

static eAtRet EparDefaultSet(ThaPwAdapter self, AtChannel circuit)
    {
    eAtTimingMode timingMode;
    AtPw adapter = (AtPw)self;
    ThaModulePw pwModule = (ThaModulePw)AtChannelModuleGet((AtChannel)self);
    eAtRet ret = cAtOk;
    AtSdhChannel sdhVc = (AtSdhChannel)circuit;
    AtPwCep realPw = (AtPwCep)ThaPwAdapterPwGet(self);
    eBool eparEnabled;

    eparEnabled = mThis(self)->eparIsEnabled;
    timingMode = AtChannelTimingModeGet((AtChannel)sdhVc);
    if (!ThaModulePwTimingModeIsApplicableForEPAR(pwModule, timingMode))
        eparEnabled = cAtFalse;

    /*Should toggle EPAR to overcome the case epar enabled pw unbind then bind to another circuit*/
    if (eparEnabled)
        {
        ret |= AtPwCepEparEnable(realPw, cAtFalse);
        ret |= AtPwCepEparEnable(realPw, cAtTrue);
        }
    else
        ret |= AtPwCepEparEnable(realPw, eparEnabled);

    if (ThaModulePwePwCwAutoTxNPBitIsConfigurable(ModulePwe(adapter)))
       ret |= ThaModulePwePwCwAutoTxNPBitEnable(ModulePwe(adapter), adapter, eparEnabled);

    return ret;
    }

static eAtRet SetDefaultOnBinding(ThaPwAdapter self, AtChannel circuit)
    {
    AtPw adapter = (AtPw)self;
    ThaModulePda pdaModule = ModulePda(adapter);
    eAtRet ret = cAtOk;
    AtSdhChannel sdhVc = (AtSdhChannel)circuit;

    /* Let super do first */
    ret = m_ThaPwAdapterMethods->SetDefaultOnBinding(self, circuit);
    if (ret != cAtOk)
        return ret;

    ret |= ThaModulePdaCepModeSet(pdaModule, self, sdhVc);
    ret |= ThaModulePdaPwCircuitTypeSet(pdaModule, adapter, circuit);
    ret |= ThaPwCepAdapterEbmHwEnable(self, cAtFalse);
    ret |= EparDefaultSet(self, circuit);
    ret |= ThaModulePdaCepIndicate(pdaModule, adapter, cAtTrue);

    return ret;
    }

static ThaCdrController CdrController(ThaPwAdapter self)
    {
    AtSdhVc circuit = (AtSdhVc)AtPwBoundCircuitGet((AtPw)self);
    if (circuit)
        return ThaSdhVcCdrControllerGet(circuit);

    return NULL;
    }

static eAtModulePwRet EparEnable(ThaPwCepAdapter self, eBool enable)
    {
    eAtRet ret;
    AtChannel circuit = AtPwBoundCircuitGet((AtPw)self);
    ThaModulePda pdaModule = ModulePda((AtPw)self);
    ThaModulePw pwModule = (ThaModulePw)AtChannelModuleGet((AtChannel)self);
    ThaSdhVc vc = (ThaSdhVc)circuit;
    eAtTimingMode timingMode = circuit ? AtChannelTimingModeGet(circuit) : cAtTimingModeUnknown;

    /* Can only enable EPAR when timing of circuit is system clock */
    if (!ThaModulePwTimingModeIsApplicableForEPAR(pwModule, timingMode) && (enable == cAtTrue))
        return cAtErrorModeNotSupport;

    /* Just cache configuration if PW cannot be run yet */
    if (ThaPwAdapterIsLogicPw(mAdapter(self)) || (circuit == NULL))
        {
        self->eparIsEnabled = enable;
        return cAtOk;
        }

    if (mMethodsGet(self)->EparIsEnabled(self) == enable)
        return cAtOk;

    /* Enable RX */
    ret  = ThaSdhVcEparEnable(vc, enable);
    ret |= ThaModulePdaPwEparEnable(pdaModule, (AtPw)self, enable);

    /* Enable/disable TX N,P bit in control word */
    if (ThaModulePwePwCwAutoTxNPBitIsConfigurable(ModulePwe((AtPw)self)))
        ret |= ThaModulePwePwCwAutoTxNPBitEnable(ModulePwe((AtPw)self), (AtPw)self, enable);

    if (ret == cAtOk)
        self->eparIsEnabled = enable;

    return ret;
    }

static eBool EparIsEnabled(ThaPwCepAdapter self)
    {
    AtChannel circuit = AtPwBoundCircuitGet((AtPw)self);

    if (ThaPwAdapterIsLogicPw(mAdapter(self)) || (circuit == NULL))
        return self->eparIsEnabled;

    return ThaModulePdaPwEparIsEnabled(ModulePda((AtPw)self), (AtPw)self);
    }

static eAtModulePwRet CwAutoNBitEnable(ThaPwCepAdapter self, eBool enable)
    {
    AtPw pw = (AtPw)self;
    return ThaModulePwePwCwAutoTxNPBitEnable(ModulePwe(pw), pw, enable);
    }

static eAtModulePwRet CwAutoPBitEnable(ThaPwCepAdapter self, eBool enable)
    {
    AtPw pw = (AtPw)self;
    return ThaModulePwePwCwAutoTxNPBitEnable(ModulePwe(pw), pw, enable);
    }

static eBool CwAutoNBitIsEnabled(ThaPwCepAdapter self)
    {
    AtPw pw = (AtPw)self;
    return ThaModulePwePwCwAutoTxNPBitIsEnabled(ModulePwe(pw), pw);
    }

static eBool CwAutoPBitIsEnabled(ThaPwCepAdapter self)
    {
    AtPw pw = (AtPw)self;
    return ThaModulePwePwCwAutoTxNPBitIsEnabled(ModulePwe(pw), pw);
    }

static uint16 PdaMaxPayloadSize(ThaPwAdapter self, ThaModulePda pdaModule)
    {
    return ThaModulePdaCEPMaxPayloadSize(pdaModule, (AtPw)self);
    }

static uint16 PdaMinPayloadSize(ThaPwAdapter self, ThaModulePda pdaModule)
    {
    return ThaModulePdaCEPMinPayloadSize(pdaModule, (AtPw)self);
    }

static eAtModulePwRet Equip(ThaPwCepAdapter self, AtChannel channel, eBool equip)
    {
	AtUnused(equip);
	AtUnused(channel);
	AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eAtModulePwRet EbmEnable(ThaPwCepAdapter self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eBool EbmIsEnabled(ThaPwCepAdapter self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static uint32 PdaPayloadSizeCorrect(ThaPwAdapter self, uint32 payloadSizeInByte)
    {
    if (ThaModulePdaCepPayloadSizeShouldExcludeHeader(ModulePda((AtPw)self)))
        return payloadSizeInByte;
    return payloadSizeInByte + 4;
    }

static eAtRet JitterPayloadLengthSet(ThaPwAdapter self)
    {
    AtPw pw = (AtPw)self;
    ThaModulePda pdaModule = ModulePda(pw);
    uint32 payloadSize = AtPwPayloadSizeGet(pw);

    payloadSize = PdaPayloadSizeCorrect(self, payloadSize);
    ThaModulePdaPwJitterPayloadLengthSet(pdaModule, pw, payloadSize);

    return cAtOk;
    }

static uint8 CircuitSliceByModule(ThaPwAdapter self, AtChannel circuit, eAtModule module)
    {
    uint8 hwSts, slice;
    AtUnused(self);
    ThaSdhChannel2HwMasterStsId((AtSdhChannel)circuit, module, &slice, &hwSts);

    return slice;
    }

static uint8 PdaCircuitSlice(ThaPwAdapter self, AtChannel circuit)
    {
    return CircuitSliceByModule(self, circuit, cThaModulePda);
    }

static uint8 PweCircuitSlice(ThaPwAdapter self, AtChannel circuit)
    {
    return CircuitSliceByModule(self, circuit, cThaModulePwe);
    }

static uint8 CdrCircuitSlice(ThaPwAdapter self, AtChannel circuit)
    {
    return CircuitSliceByModule(self, circuit, cThaModuleCdr);
    }

static uint32 MinJitterDelay(AtPw self, AtChannel circuit, uint16 payloadSize)
    {
    return ThaModulePdaCEPMinJitterDelay(ModulePda(self), self, circuit, payloadSize);
    }

static uint32 MinJitterBufferSize(AtPw self, AtChannel circuit, uint16 payloadSize)
    {
    return ThaModulePdaCEPMinJitterBufferSize(ModulePda(self), self, circuit, payloadSize);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaPwCepAdapter object = (ThaPwCepAdapter)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeUInt(eparIsEnabled);
    }

static uint16 CDRTimingPktLenInBits(ThaPwAdapter self)
    {
    AtSdhChannel circuit = (AtSdhChannel)AtPwBoundCircuitGet((AtPw)self);
    uint8 numSts;

    if (circuit == NULL)
        return m_ThaPwAdapterMethods->CDRTimingPktLenInBits(self);

    numSts = AtSdhChannelNumSts(circuit);

    if (numSts == 192)
        {
        uint32 payloadSizeInBytes = AtPwPayloadSizeGet((AtPw)self);
        /*
         * How to manage CDR payload length in bit for VC-4-64c
         *   - LineType: must be set to 5 (DS3/VC4_16c)
         *   - PayloadSize in bit: (payloadSizeInBytes * 8) / 4
         */
        return (uint16)(payloadSizeInBytes * 2);
        }

    /*
     * How to manage CDR payload length in bit for VC-4-nc (n = 4, 8, 16)
     *   - VC4_4c: must be set to 1 to run in VC4-4c basic rate
     *   - PayloadSize in bit: (payloadSizeInBytes * 8) / (numSts / 12) # 12 is number of STSs of VC-4-4c
     */
    if (numSts > 3)
        {
        uint32 payloadSizeInBytes = AtPwPayloadSizeGet((AtPw)self);

        /* Log warnings first (if configuration is not safe) */
        if ((numSts % 12) != 0)
            {
            AtChannelLog((AtChannel)self, cAtLogLevelWarning, AtSourceLocation,
                         "Circuit %s.%s has number of STS = %d not divisible by 12. Configuration will still be applied but ACR/DCR may not lock properly\r\n",
                         AtChannelTypeString((AtChannel)circuit), AtChannelIdString((AtChannel)circuit),
                         numSts);
            }

        if (((payloadSizeInBytes * 8) % (numSts / 12)) != 0)
            {
            AtChannelLog((AtChannel)self, cAtLogLevelWarning, AtSourceLocation,
                         "Payload size in bit (%u) is not divisible to %d. Configuration will still be applied but ACR/DCR may not lock properly\r\n",
                         payloadSizeInBytes * 8,
                         numSts / 12);
            }

        return (uint16)((payloadSizeInBytes * 8UL) / (numSts / 12UL));
        }

    return m_ThaPwAdapterMethods->CDRTimingPktLenInBits(self);
    }

static uint16 MinJitterBufferDelayInPacketGet(AtPw self, AtChannel circuit, uint16 payloadSize)
    {
    AtUnused(circuit);
    AtUnused(payloadSize);
    return (uint16)ThaModulePdaCEPMinNumPacketsForJitterDelay(ModulePda(self), self);
    }

static uint32 ExtraJitterBufferGet(ThaPwAdapter self)
    {
    AtSdhChannel vc = (AtSdhChannel)AtPwBoundCircuitGet((AtPw)self);
    eAtSdhChannelType vcType;

    /* In case PW unbound circuit, let return the maximum extra jitter buffer
     * to satisfy all circuit bounding later. */
    if (vc == NULL)
        return 500U;

    vcType = AtSdhChannelTypeGet(vc);
    if ((vcType == cAtSdhChannelTypeVc11) ||
        (vcType == cAtSdhChannelTypeVc12))
        return 500U;

    return 125U;
    }

static uint32 DefaultJitterDelay(ThaPwAdapter self)
    {
    uint32 defaultMinJitterDelay = m_ThaPwAdapterMethods->DefaultJitterDelay(self);
    if (defaultMinJitterDelay < cMinCepJitterDelayInUs)
        return cMinCepJitterDelayInUs;

    return defaultMinJitterDelay;
    }

static eBool PayloadSizeIsValid(ThaPwAdapter self, uint16 payloadSize)
    {
    if (ThaPwAdapterIsLogicPw(self))
        return cAtTrue;

    return ThaModulePdaCEPPayloadSizeIsValid(ModulePda((AtPw)self), (AtPw)self, payloadSize);
    }

static void OverrideAtObject(ThaPwCepAdapter self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtPw(ThaPwCepAdapter self)
    {
    AtPw pw = (AtPw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPwMethods = mMethodsGet(pw);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPwOverride, m_AtPwMethods, sizeof(m_AtPwOverride));

        mMethodOverride(m_AtPwOverride, MinJitterDelay);
        mMethodOverride(m_AtPwOverride, MinJitterBufferSize);
        mMethodOverride(m_AtPwOverride, MinJitterBufferDelayInPacketGet);
        }

    mMethodsSet(pw, &m_AtPwOverride);
    }

static void OverrideAtChannel(ThaPwCepAdapter self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, TypeString);
        mMethodOverride(m_AtChannelOverride, AllCountersGet);
        mMethodOverride(m_AtChannelOverride, AllCountersClear);
        mMethodOverride(m_AtChannelOverride, Debug);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideThaPwAdapter(ThaPwCepAdapter self)
    {
    ThaPwAdapter adapter = (ThaPwAdapter)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPwAdapterMethods = mMethodsGet(adapter);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwAdapterOverride, m_ThaPwAdapterMethods, sizeof(m_ThaPwAdapterOverride));

        mMethodOverride(m_ThaPwAdapterOverride, CountersCreate);
        mMethodOverride(m_ThaPwAdapterOverride, SetDefaultOnBinding);
        mMethodOverride(m_ThaPwAdapterOverride, CdrController);
        mMethodOverride(m_ThaPwAdapterOverride, DefaultPayloadSize);
        mMethodOverride(m_ThaPwAdapterOverride, PartOfCircuit);
        mMethodOverride(m_ThaPwAdapterOverride, PdaMaxPayloadSize);
        mMethodOverride(m_ThaPwAdapterOverride, PdaMinPayloadSize);
        mMethodOverride(m_ThaPwAdapterOverride, JitterPayloadLengthSet);
        mMethodOverride(m_ThaPwAdapterOverride, PdaCircuitSlice);
        mMethodOverride(m_ThaPwAdapterOverride, PweCircuitSlice);
        mMethodOverride(m_ThaPwAdapterOverride, CdrCircuitSlice);
        mMethodOverride(m_ThaPwAdapterOverride, CDRTimingPktLenInBits);
        mMethodOverride(m_ThaPwAdapterOverride, ExtraJitterBufferGet);
        mMethodOverride(m_ThaPwAdapterOverride, DefaultJitterDelay);
        mMethodOverride(m_ThaPwAdapterOverride, PayloadSizeIsValid);
        }

    mMethodsSet(adapter, &m_ThaPwAdapterOverride);
    }

static void Override(ThaPwCepAdapter self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtPw(self);
    OverrideThaPwAdapter(self);
    }

static void MethodsInit(ThaPwCepAdapter self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, DefaultOffset);
        mMethodOverride(m_methods, EparEnable);
        mMethodOverride(m_methods, EparIsEnabled);
        mMethodOverride(m_methods, Equip);
        mMethodOverride(m_methods, EbmEnable);
        mMethodOverride(m_methods, EbmIsEnabled);
        mMethodOverride(m_methods, CwAutoNBitEnable);
        mMethodOverride(m_methods, CwAutoPBitEnable);
        mMethodOverride(m_methods, CwAutoNBitIsEnabled);
        mMethodOverride(m_methods, CwAutoPBitIsEnabled);
        }

    mMethodsSet(self, &m_methods);
    }

ThaPwCepAdapter ThaPwCepAdapterObjectInit(ThaPwCepAdapter self, AtPw pw)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPwAdapterObjectInit((ThaPwAdapter)self, pw) == NULL)
        return NULL;

    /* Override */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwAdapter ThaPwCepAdapterNew(AtPw pw)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwCepAdapter newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return (ThaPwAdapter)ThaPwCepAdapterObjectInit(newController, pw);
    }

eAtRet ThaTdmPwCountersReadToClear(AtPw self, eBool clear)
    {
    AtPwTdmCounters counters = (AtPwTdmCounters)ThaPwAdapterCountersGet(mAdapter(self));

    if (ThaPwAdapterShouldReadCountersFromSurModule((AtChannel)self))
        {
        ThaModuleHardSur surModule = ModuleSur(self);

        AtPwTdmCountersTxLbitPacketsSet(counters, ThaModuleHardSurPwTxLbitPacketsGet(surModule, self, clear));
        AtPwTdmCountersTxRbitPacketsSet(counters, ThaModuleHardSurPwTxRbitPacketsGet(surModule, self, clear));
        AtPwTdmCountersRxLbitPacketsSet(counters, ThaModuleHardSurPwRxLbitPacketsGet(surModule, self, clear));
        AtPwTdmCountersRxRbitPacketsSet(counters, ThaModuleHardSurPwRxRbitPacketsGet(surModule, self, clear));
        AtPwTdmCountersRxJitBufOverrunSet(counters, ThaModuleHardSurPwRxJitBufOverrunGet(surModule, self, clear));
        AtPwTdmCountersRxJitBufUnderrunSet(counters, ThaModuleHardSurPwRxJitBufUnderrunGet(surModule, self, clear));
        AtPwTdmCountersRxLopsSet(counters, ThaModuleHardSurPwRxLopsGet(surModule, self, clear));
        AtPwTdmCountersRxPacketsSentToTdmSet(counters, ThaModuleHardSurPwRxPacketsSentToTdmGet(surModule, self, clear));
        }
    else
        {
        ThaModulePwe pweModule = ModulePwe(self);
        ThaModulePda pdaModule = ModulePda(self);
        ThaClaPwController claController = ThaPwAdapterClaPwController(mAdapter(self));

        AtPwTdmCountersTxLbitPacketsSet(counters, ThaModulePwePwTxLbitPacketsGet(pweModule, self, clear));
        AtPwTdmCountersTxRbitPacketsSet(counters, ThaModulePwePwTxRbitPacketsGet(pweModule, self, clear));
        AtPwTdmCountersRxLbitPacketsSet(counters, ThaClaPwControllerRxLbitPacketsGet(claController, self, clear));
        AtPwTdmCountersRxRbitPacketsSet(counters, ThaClaPwControllerRxRbitPacketsGet(claController, self, clear));
        AtPwTdmCountersRxJitBufOverrunSet(counters, ThaModulePdaPwRxJitBufOverrunGet(pdaModule, self, clear));
        AtPwTdmCountersRxJitBufUnderrunSet(counters, ThaModulePdaPwRxJitBufUnderrunGet(pdaModule, self, clear));
        AtPwTdmCountersRxLopsSet(counters, ThaModulePdaPwRxLopsGet(pdaModule, self, clear));
        AtPwTdmCountersRxPacketsSentToTdmSet(counters, ThaModulePdaPwRxPacketsSentToTdmGet(pdaModule, self, clear));
        }

    return cAtOk;
    }

eAtModulePwRet ThaPwCepAdapterEquip(ThaPwCepAdapter self, AtChannel channel, eBool equip)
    {
    return mMethodsGet(self)->Equip(self, channel, equip);
    }

eAtModulePwRet ThaPwCepEbmEnable(ThaPwCepAdapter self, eBool enable)
    {
    return mMethodsGet(self)->EbmEnable(self, enable);
    }

eBool ThaPwCepEbmIsEnabled(ThaPwCepAdapter self)
    {
    return mMethodsGet(self)->EbmIsEnabled(self);
    }

eBool ThaPwCepAdapterDbEparIsEnabled(ThaPwCepAdapter self)
    {
    return self->eparIsEnabled;
    }

eAtModulePwRet ThaPwCepAdapterEbmHwEnable(ThaPwAdapter self, eBool enable)
    {
    AtPw pw = (AtPw)self;
    eAtRet ret = ThaClaPwControllerPwCepEbmEnable(ThaPwAdapterClaPwController(self), pw, enable);
    ret |= ThaModulePdaPwCepEbmEnable(ModulePda(pw), pw, enable);
    ret |= ThaModulePwePwHeaderCepEbmEnable(ModulePwe(pw), pw, enable);
    return ret;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : ThaPwAdapterInternal.h
 * 
 * Created Date: Mar 13, 2013
 *
 * Description : PW adapter
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPWADAPTERINTERNAL_H_
#define _THAPWADAPTERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPwCounters.h"
#include "AtSurEngine.h"

#include "../../../../generic/man/AtDeviceInternal.h"
#include "../../../../generic/pw/AtPwInternal.h"
#include "../../../../generic/pw/psn/AtPwPsnInternal.h"
#include "../../../../generic/pw/counters/AtPwCountersInternal.h"
#include "../../../../generic/sdh/AtSdhChannelInternal.h"

#include "../../man/ThaDeviceInternal.h"
#include "../../pda/ThaModulePda.h"
#include "../../bmt/ThaModuleBmtInternal.h"
#include "../../cos/ThaModuleCosInternal.h"
#include "../../cla/pw/ThaModuleClaPwInternal.h"
#include "../../cla/hbce/ThaHbceMemoryCellContent.h"
#include "../../cla/hbce/ThaHbceMemoryCell.h"
#include "../../pwe/ThaModulePwe.h"
#include "../ThaPwReg.h"
#include "../ThaPwInternal.h"
#include "../../util/ThaUtil.h"

#include "ThaPwAdapter.h"
#include "../defectcontrollers/ThaPwDefectController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define mAdapter(self) (AtCast(ThaPwAdapter, self))

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPwAdapterStandbyCache
    {
    uint8 hbceEnabled;
    uint8 claEnabled;
    }tThaPwAdapterStandbyCache;

typedef struct tThaPwAdapterMethods
    {
    uint32 (*DefaultOffset)(ThaPwAdapter self);
    AtPwCounters (*CountersCreate)(ThaPwAdapter self);
    eAtRet (*SetDefaultOnBinding)(ThaPwAdapter self, AtChannel circuit);
    uint32 (*DefaultPayloadSize)(ThaPwAdapter self, AtChannel circuit);
    uint8 (*PartOfCircuit)(ThaPwAdapter self, AtChannel circuit);
    uint32 (*DataRateInBytesPerMs)(ThaPwAdapter self, AtChannel circuit);

    ThaCdrController (*CdrController)(ThaPwAdapter self);
    uint16 (*DCRRxNcoConfPackLen)(ThaPwAdapter self);
    uint16 (*CDRTimingPktLenInBits)(ThaPwAdapter self);
    eAtRet (*CdrPayloadUpdate)(ThaPwAdapter self);

    /* To configure internal modules */
    uint8 (*NumDs0Slots)(ThaPwAdapter self);

    eAtRet (*JitterPayloadLengthSet)(ThaPwAdapter self);
    uint32 (*PayloadSizeInBytes)(ThaPwAdapter self);
    uint32 (*DefaultJitterBufferSize)(ThaPwAdapter self);
    uint32 (*DefaultJitterDelay)(ThaPwAdapter self);
    eAtRet (*CircuitTxAisForce)(ThaPwAdapter self, eBool force);
    eAtRet (*HwPayloadSizeSet)(ThaPwAdapter self, uint16 payloadSize);
    uint16 (*HwPayloadSizeGet)(ThaPwAdapter self);

    /* Some PW types in some specific devices require an extra jitter buffer
     * to be added into actual jitter buffer which be calculated from jitter delay. */
    uint32 (*ExtraJitterBufferGet)(ThaPwAdapter self);

    /* Maximum payload size in byte unit that module PDA can accept */
    uint16 (*PdaMaxPayloadSize)(ThaPwAdapter self, ThaModulePda pdaModule);
    uint16 (*PdaMinPayloadSize)(ThaPwAdapter self, ThaModulePda pdaModule);

    /* Adapter minimum payload size */
    uint16 (*AdapterMinPayloadSize)(ThaPwAdapter self, AtChannel circuit, uint32 jitterBufferSize);

    eBool  (*NeedToControlCdr)(ThaPwAdapter self);
    eAtRet (*HwEnable)(ThaPwAdapter self, eBool enable);
    eBool  (*HwIsEnabled)(ThaPwAdapter self);

    /* Circuit slice */
    uint8 (*PdaCircuitSlice)(ThaPwAdapter self, AtChannel circuit);
    uint8 (*CdrCircuitSlice)(ThaPwAdapter self, AtChannel circuit);
    uint8 (*PweCircuitSlice)(ThaPwAdapter self, AtChannel circuit);

    eAtRet (*JitterBufferDefaultSet)(ThaPwAdapter self);
    eAtRet (*HwPwTypeSet)(ThaPwAdapter self);
    eBool (*CanDisableCw)(ThaPwAdapter self);

    /* Constrain checking */
    eBool (*PayloadSizeIsValid)(ThaPwAdapter self, uint16 payloadSize);
    }tThaPwAdapterMethods;

typedef struct tThaPwAdapter
    {
    tAtPw super; /* To have the same generic interface */
    const tThaPwAdapterMethods *methods;

    /* Private data */
    eBool enabled;
    AtPw pw; /* The PW that this adapter needs to ask for information */
    AtPwCounters counters;
    eBool notCheckReconfigured;

    /* Jitter configuration */
    uint32 jitterBufferSizeInUs;
    uint32 jitterBufferDelayInUs;
    uint16 jitterBufferSizeInPkt;
    uint16 jitterBufferDelayInPkt;
    eBool  jitterSizeIsInPkt;
    eBool  jitterDelayIsInPkt;

    /* Payload */
    uint16 payloadSize;
    uint8 rtpIsEnabled;
    uint8 cwIsEnabled;
    eBool reoderEnabled;
    uint8 rtpEnableIsSet;

    /* Binding information */
    AtEthPort ethPort;
    AtEthFlow ethFlow;
    AtChannel circuit;
    AtChannel acrDcrRefCircuit; /* The circuit that is selecting this PW for ACR/DCR processing */

    /* For VLAN lookup */
    tAtEthVlanTag expectedCVlan;
    eBool expectedCVlanIsValid;
    tAtEthVlanTag expectedSVlan;
    eBool expectedSVlanIsValid;

    /* To save configuration of application when no real HW PW is assigned */
    tThaPwConfigCache *cache;
    ThaHwPw hwPw; /* Hardware PW controlled by this adapter */
    tThaPwAdapterStandbyCache haCache;

    /* To prevent nested removing */
    uint8 pweEnterRemovingProcessTime;
    uint8 pdaEnterCenteringProcessTime;
    eBool isBindingInProgress;

    /* Defect */
    ThaPwDefectController defaultDefectController;
    ThaPwDefectController debugDefectController;
    ThaPwDefectController activeDefectController;

    /* Header controller */
    ThaPwHeaderController headerController;
    ThaPwHeaderController backupHeaderController;

    /* PW may have it's own cla controller */
    ThaClaPwController claController;

    AtSurEngine surEngine;
    uint32 numRamBlockForBuffer;
    uint32 currentBandwidthInBps; /* Remainder by dividing 10^6 */
    uint32 currentBandwidthInMbps;
    }tThaPwAdapter;

typedef struct tThaPwCESoPAdapterMethods
    {
    eAtModulePwRet (*CwAutoRxMBitEnable)(ThaPwCESoPAdapter self, eBool enable);
    eBool  (*CwAutoRxMBitIsEnabled)(ThaPwCESoPAdapter self);
    eAtModulePwRet (*CwAutoTxMBitEnable)(ThaPwCESoPAdapter self, eBool enable);
    eBool  (*CwAutoTxMBitIsEnabled)(ThaPwCESoPAdapter self);
    eAtPwCESoPMode (*ModeGet)(ThaPwCESoPAdapter self);
    eBool (*CwAutoRxMBitIsConfigurable)(ThaPwCESoPAdapter self);

    /* CAS IDLE code */
    eAtModulePwRet (*CasIdleCode1Set)(ThaPwCESoPAdapter self, uint8 abcd1);
    uint8 (*CasIdleCode1Get)(ThaPwCESoPAdapter self);
    eAtModulePwRet (*CasIdleCode2Set)(ThaPwCESoPAdapter self, uint8 abcd2);
    uint8 (*CasIdleCode2Get)(ThaPwCESoPAdapter self);
    eAtModulePwRet (*CasAutoIdleEnable)(ThaPwCESoPAdapter self, eBool enable);
    eBool (*CasAutoIdleIsEnabled)(ThaPwCESoPAdapter self);

    /* CAS mode */
    eAtRet (*RxCasModeSet)(ThaPwCESoPAdapter self, eAtPwCESoPCasMode mode);
    eAtPwCESoPCasMode (*RxCasModeGet)(ThaPwCESoPAdapter self);
    }tThaPwCESoPAdapterMethods;

typedef struct tThaPwCESoPAdapter
    {
    tThaPwAdapter  super;
    const tThaPwCESoPAdapterMethods *methods;

    /* Private data */
    }tThaPwCESoPAdapter;

typedef struct tThaPwCESoPAdapterInBytes
    {
    tThaPwCESoPAdapter  super;

    }tThaPwCESoPAdapterInBytes;

typedef struct tThaPwCepAdapterMethods
    {
    uint32 (*DefaultOffset)(AtPw self);
    eAtModulePwRet (*Equip)(ThaPwCepAdapter self, AtChannel channel, eBool equip);
    eAtModulePwRet (*EparEnable)(ThaPwCepAdapter self, eBool enable);
    eBool  (*EparIsEnabled)(ThaPwCepAdapter self);

    /* On-the-fly mode */
    eAtModulePwRet (*EbmEnable)(ThaPwCepAdapter self, eBool enable);
    eBool  (*EbmIsEnabled)(ThaPwCepAdapter self);
    eAtPwCepMode (*ModeGet)(ThaPwCepAdapter self);

    /* Auto N-Bit, P-Bit */
    eAtModulePwRet (*CwAutoNBitEnable)(ThaPwCepAdapter self, eBool enable);
    eAtModulePwRet (*CwAutoPBitEnable)(ThaPwCepAdapter self, eBool enable);
    eBool (*CwAutoNBitIsEnabled)(ThaPwCepAdapter self);
    eBool (*CwAutoPBitIsEnabled)(ThaPwCepAdapter self);
    }tThaPwCepAdapterMethods;

typedef struct tThaPwCepAdapter
    {
    tThaPwAdapter  super;
    const tThaPwCepAdapterMethods *methods;

    /* Private data */
    uint8 eparIsEnabled;
    }tThaPwCepAdapter;

typedef struct tThaPwCepFractionalAdapter
    {
    tThaPwCepAdapter super;

    /* Private data */
    uint8 ebmIsEnabled;
    }tThaPwCepFractionalAdapter;

typedef struct tThaPwTohAdapterMethods
    {
    eAtRet (*Allow)(ThaPwTohAdapter self, const tThaTohByte *tohByte);
    eAtRet (*Reject)(ThaPwTohAdapter self, const tThaTohByte *tohByte);
    }tThaPwTohAdapterMethods;

typedef struct tThaPwTohAdapter
    {
    tThaPwAdapter  super;
    const tThaPwTohAdapterMethods *methods;

    }tThaPwTohAdapter;

typedef struct tThaPwHdlcAdapterMethods
    {
    eAtRet (*PayloadTypeSet)(ThaPwHdlcAdapter self, eAtPwHdlcPayloadType payloadType);
    const uint8* (*CircuitEncapHeader)(ThaPwHdlcAdapter self, uint8* numBytes);
    }tThaPwHdlcAdapterMethods;

typedef struct tThaPwHdlcAdapter
    {
    tThaPwAdapter  super;
    const tThaPwHdlcAdapterMethods *methods;

    /* Private data */
    uint8 payloadType;
    }tThaPwHdlcAdapter;

typedef struct tThaPwMlpppAdapter
    {
    tThaPwHdlcAdapter  super;
    }tThaPwMlpppAdapter;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPwAdapter ThaPwAdapterObjectInit(ThaPwAdapter self, AtPw pw);
ThaPwCepAdapter ThaPwCepAdapterObjectInit(ThaPwCepAdapter self, AtPw pw);
ThaPwHdlcAdapter ThaPwHdlcAdapterObjectInit(ThaPwHdlcAdapter self, AtPw pw);
ThaPwCESoPAdapter ThaPwCESoPAdapterObjectInit(ThaPwCESoPAdapter self, AtPw pw);

/* Utils */
eAtRet ThaTdmPwCountersReadToClear(AtPw self, eBool clear);
eAtModulePwRet ThaPwCepAdapterEbmHwEnable(ThaPwAdapter self, eBool enable);

#ifdef __cplusplus
}
#endif
#endif /* _THAPWADAPTERINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : ThaPwCepAdapterDebug.c
 *
 * Created Date: Jan 3, 2014
 *
 * Description : CEP debug
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaPwAdapterInternal.h"
#include "../../sdh/ThaModuleSdh.h"
#include "../../sdh/ThaSdhVc.h"
#include "AtPwCep.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaRegHwMapToPlaB3V5CheckControl           0x000106

#define cThaRegMap2PlaB3V5CheckFracMask             cBit25
#define cThaRegMap2PlaB3V5CheckFracShift            25

#define cThaRegMap2PlaB3V5CheckVtBasicMask          cBit24
#define cThaRegMap2PlaB3V5CheckVtBasicShift         24

#define cThaRegMap2PlaB3V5CheckPosMask              cBit23_12
#define cThaRegMap2PlaB3V5CheckPosShift             12

#define cThaRegMap2PlaB3V5CheckPwidMask             cBit11_0
#define cThaRegMap2PlaB3V5CheckPwidShift            0

#define cThaRegHwMapToPlaB3V5CheckSticky            0x000110

#define cThaRegMap2PlaB3V5CheckErrorMask            cBit0
#define cThaRegMap2PlaB3V5CheckErrorShift           0


#define cThaRegHwRxPwB3V5CheckControl               0x247001

#define cThaRegRxPwB3V5CheckFracVtBasicMask         cBit28
#define cThaRegRxPwB3V5CheckFracVtBasicShift        28

#define cThaRegRxPwB3V5CheckCepModeMask             cBit27_26
#define cThaRegRxPwB3V5CheckCepModeShift            26

#define cThaRegRxPwB3V5CheckFracVt15Mask            cBit25
#define cThaRegRxPwB3V5CheckFracVt15Shift           25

#define cThaRegRxPwB3V5CheckVC3VC4FracVtMask        cBit24
#define cThaRegRxPwB3V5CheckVC3VC4FracVtShift       24

#define cThaRegRxPwB3V5CheckPosMask                 cBit23_12
#define cThaRegRxPwB3V5CheckPosShift                12

#define cThaRegRxPwB3V5CheckSticky                  0x247000

#define cThaRegRxPwB3V5CheckCep2MapErrMask          cBit2
#define cThaRegRxPwB3V5CheckCep2MapErrShift         2

#define cThaRegRxPwB3V5CheckPda2CepErrMask          cBit1
#define cThaRegRxPwB3V5CheckPda2CepErrShift         1

#define cThaRegRxPwB3V5CheckCla2PdaErrMask          cBit0
#define cThaRegRxPwB3V5CheckCla2PdaErrShift         0


#define cThaRegOcn2PdhB3V5CheckControl              0x700004

#define cThaRegOcn2PdhB3V5CheckPosMask              cBit27_16
#define cThaRegOcn2PdhB3V5CheckPosShift             16

#define cThaRegOcn2PdhB3V5CheckVc4_4cModeMask       cBit15_14
#define cThaRegOcn2PdhB3V5CheckVc4_4cModeShift      14

#define cThaRegOcn2PdhB3V5CheckVc4ModeMask          cBit13
#define cThaRegOcn2PdhB3V5CheckVc4ModeShift         13

#define cThaRegOcn2PdhB3V5CheckVtBasicMask          cBit12
#define cThaRegOcn2PdhB3V5CheckVtBasicShift         12

#define cThaRegOcn2PdhB3V5CheckLineIdMask           cBit11_0
#define cThaRegOcn2PdhB3V5CheckLineIdShift          0


#define cThaRegPdh2DemapB3V5CheckControl            0x700005

#define cThaRegPdh2DemapB3V5CheckPosMask            cBit27_16
#define cThaRegPdh2DemapB3V5CheckPosShift           16

#define cThaRegPdh2DemapB3V5CheckVc4_4cModeMask     cBit15_14
#define cThaRegPdh2DemapB3V5CheckVc4_4cModeShift    14

#define cThaRegPdh2DemapB3V5CheckVc4ModeMask        cBit13
#define cThaRegPdh2DemapB3V5CheckVc4ModeShift       13

#define cThaRegPdh2DemapB3V5CheckVtBasicMask        cBit12
#define cThaRegPdh2DemapB3V5CheckVtBasicShift       12

#define cThaRegPdh2DemapB3V5CheckLineIdMask         cBit11_0
#define cThaRegPdh2DemapB3V5CheckLineIdShift        0


#define cThaRegPwe2BmtB3V5CheckControl              0x3000f0

#define cThaRegPwe2BmtB3V5CheckVtBasicMask          cBit28
#define cThaRegPwe2BmtB3V5CheckVtBasicShift         28

#define cThaRegPwe2BmtB3V5CheckPosMask              cBit27_16
#define cThaRegPwe2BmtB3V5CheckPosShift             16

#define cThaRegPwe2BmtB3V5CheckFracVt15Mask         cBit15
#define cThaRegPwe2BmtB3V5CheckFracVt15Shift        15

#define cThaRegPwe2BmtB3V5CheckFracMask             cBit12
#define cThaRegPwe2BmtB3V5CheckFracShift            12

#define cThaRegPwe2BmtB3V5CheckPwIdMask             cBit11_0
#define cThaRegPwe2BmtB3V5CheckPwIdShift            0

#define cThaRegOcn2Pdh2DemapB3V5CheckSticky         0x700006

#define cThaRegPdh2DemapB3V5CheckMask               cBit1
#define cThaRegPdh2DemapB3V5CheckShift              1

#define cThaRegOcn2PdhB3V5CheckMask                 cBit0
#define cThaRegOcn2PdhB3V5CheckShift                0

#define cThaRegPwe2BmtB3V5CheckSticky               0x3000F1

#define cThaRegPwe2BmtB3V5CheckMask                 cBit22
#define cThaRegPwe2BmtB3V5CheckShift                22

/*--------------------------- Macros -----------------------------------------*/
#define mInAccessible(self) AtChannelInAccessible((AtChannel)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPw PwGet(AtChannel self)
    {
    ThaPwAdapter adapter = (ThaPwAdapter)self;
    return ThaPwAdapterPwGet(adapter);
    }

static AtSdhChannel CircuitGet(AtChannel self)
    {
    return (AtSdhChannel)AtPwBoundCircuitGet(PwGet(self));
    }

static uint32 PartOffset(AtChannel self)
    {
    ThaPwAdapter adapter = (ThaPwAdapter)self;
    AtChannel    circuit = (AtChannel)CircuitGet(self);
    ThaDevice    device  = (ThaDevice)AtChannelDeviceGet(circuit);
    return ThaDevicePartOffset(device, ThaPwAdapterPartOfCircuit(adapter, circuit));
    }

static eBool IsCepBasic(AtChannel self)
    {
    return (AtPwCepModeGet((AtPwCep)PwGet(self)) == cAtPwCepModeBasic) ? cAtTrue : cAtFalse;
    }

static eBool IsHighSpeedCep(AtChannel self)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(CircuitGet(self));

    if ((channelType == cAtSdhChannelTypeVc4) ||
        (channelType == cAtSdhChannelTypeVc3) ||
        (channelType == cAtSdhChannelTypeVc4_4c))
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsCepVc4_4c(AtChannel self)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(CircuitGet(self));
    return (channelType == cAtSdhChannelTypeVc4_4c) ? cAtTrue : cAtFalse;
    }

static eBool IsCepVc4(AtChannel self)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(CircuitGet(self));
    return (channelType == cAtSdhChannelTypeVc4) ? cAtTrue : cAtFalse;
    }

static eBool IsCepVc3(AtChannel self)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(CircuitGet(self));
    return (channelType == cAtSdhChannelTypeVc3) ? cAtTrue : cAtFalse;
    }

static uint32 B3V5CheckPos(AtChannel self)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(CircuitGet(self));

    if (channelType == cAtSdhChannelTypeVc3)
        {
        eAtSdhChannelType parentChannelType = AtSdhChannelTypeGet(AtSdhChannelParentChannelGet(CircuitGet(self)));
        return (parentChannelType == cAtSdhChannelTypeAu3) ? 88 : 86;
        }

    if (channelType == cAtSdhChannelTypeVc4)
        return 262;

    if (channelType == cAtSdhChannelTypeVc4_4c)
        return 1045;

    return 0;
    }

static ThaModulePw ModulePw(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaModulePw)AtDeviceModuleGet(device, cAtModulePw);
    }

static eBool BoundCircuitIsVc1x(AtChannel self)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(CircuitGet(self));
    return ((channelType == cAtSdhChannelTypeVc12) || (channelType == cAtSdhChannelTypeVc11)) ? cAtTrue : cAtFalse;
    }

static void HwMapToPlaB3V5Control(AtChannel self)
    {
    uint32 regVal     = 0;
    uint32 regAddr    = cThaRegHwMapToPlaB3V5CheckControl + PartOffset(self);
    eBool  isVc1x     = BoundCircuitIsVc1x(self);
    uint8  fractional = IsCepBasic(self) ? 0 : 1;

    mRegFieldSet(regVal, cThaRegMap2PlaB3V5CheckVtBasic, mBoolToBin(isVc1x));
    mRegFieldSet(regVal, cThaRegMap2PlaB3V5CheckPwid, ThaModulePwLocalPwId(ModulePw(self), (AtPw)self));
    mRegFieldSet(regVal, cThaRegMap2PlaB3V5CheckFrac, fractional);
    mRegFieldSet(regVal, cThaRegMap2PlaB3V5CheckPos, B3V5CheckPos(self));
    mChannelHwWrite(self, regAddr, regVal, cAtModulePw);
    }

static eBool IsCepFractional(AtChannel self)
    {
    return (AtPwCepModeGet((AtPwCep)PwGet(self)) == cAtPwCepModeFractional) ? cAtTrue : cAtFalse;
    }

static eAtSdhChannelType CepFractionalChannelType(AtChannel self)
    {
    AtIterator iterator;
    eAtSdhChannelType channelType;

    if (!IsCepFractional(self))
        return cAtSdhChannelTypeUnknown;

    iterator = AtPwCepEquippedChannelsIteratorCreate((AtPwCep)PwGet(self));
    if (AtIteratorCount(iterator) == 0)
        {
        AtObjectDelete((AtObject)iterator);
        return cAtSdhChannelTypeUnknown;
        }

    channelType = AtSdhChannelTypeGet((AtSdhChannel)AtIteratorNext(iterator));
    AtObjectDelete((AtObject)iterator);

    return channelType;
    }

static eBool IsCepVc3FractionalVc1x(AtChannel self)
    {
    if (!IsCepVc3(self))
        return cAtFalse;

    if (CepFractionalChannelType(self) == cAtSdhChannelTypeVc12)
        return cAtTrue;

    if (CepFractionalChannelType(self) == cAtSdhChannelTypeVc11)
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsCepVc4FractionalVcx(AtChannel self)
    {
    if (!IsCepVc4(self))
        return cAtFalse;

    if (CepFractionalChannelType(self) == cAtSdhChannelTypeVc3)
        return cAtTrue;

    if (CepFractionalChannelType(self) == cAtSdhChannelTypeVc11)
        return cAtTrue;

    if (CepFractionalChannelType(self) == cAtSdhChannelTypeVc12)
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsCepVcxFractionalVc11(AtChannel self)
    {
    if (!IsCepFractional(self))
        return cAtFalse;

    return (CepFractionalChannelType(self) == cAtSdhChannelTypeVc11) ? cAtTrue : cAtFalse;
    }

static eBool IsCepVcxFractionalVc12(AtChannel self)
    {
    if (!IsCepFractional(self))
        return cAtFalse;

    return (CepFractionalChannelType(self) == cAtSdhChannelTypeVc12) ? cAtTrue : cAtFalse;
    }

static eBool IsCepVcxFractionalVc1x(AtChannel self)
    {
    if (IsCepVcxFractionalVc11(self))
        return cAtTrue;

    if (IsCepVcxFractionalVc12(self))
        return cAtTrue;

    return cAtFalse;
    }

static uint8 B3V5CheckCepMode(AtChannel self)
    {
    if (IsCepBasic(self))
        return 0;

    if (IsCepVc3FractionalVc1x(self))
        return 1;

    if (IsCepVc4FractionalVcx(self))
        return 3;

    return 0;
    }

static uint8 B3V5CheckFracVt15(AtChannel self)
    {
    if (IsCepVcxFractionalVc11(self))
        return 1;

    if (IsCepVcxFractionalVc12(self))
        return 0;

    return 0;
    }

static uint8 B3V5CheckVC4VC3FracVt(AtChannel self)
    {
    if (IsCepVcxFractionalVc1x(self))
        return 1;

    return 0;
    }

static void HwRxPwB3V5Control(AtChannel self)
    {
    uint32 regVal  = 0;
    uint32 regAddr = cThaRegHwRxPwB3V5CheckControl + PartOffset(self);
    eBool  isVc1x  = BoundCircuitIsVc1x(self);
    mRegFieldSet(regVal, cThaRegRxPwB3V5CheckFracVtBasic, mBoolToBin(isVc1x));
    mRegFieldSet(regVal, cThaRegRxPwB3V5CheckCepMode, B3V5CheckCepMode(self));
    mRegFieldSet(regVal, cThaRegRxPwB3V5CheckVC3VC4FracVt, B3V5CheckVC4VC3FracVt(self));
    mRegFieldSet(regVal, cThaRegMap2PlaB3V5CheckPwid, ThaModulePwLocalPwId(ModulePw(self), (AtPw)self));
    mRegFieldSet(regVal, cThaRegRxPwB3V5CheckPos, B3V5CheckPos(self));

    if (!IsCepBasic(self))
        mRegFieldSet(regVal, cThaRegRxPwB3V5CheckFracVt15, B3V5CheckFracVt15(self));

    mChannelHwWrite(self, regAddr, regVal, cAtModulePw);
    }

static uint32 SdhChannelFlatIdGet(AtChannel self, eAtModule module)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)CircuitGet(self);
    uint8 tug2Id = 0;
    uint8 vtId   = 0;
    uint8 stsId;
    uint8 sliceId;

    ThaSdhChannel2HwMasterStsId(sdhChannel, module, &sliceId, &stsId);

    if (IsHighSpeedCep(self) == cAtFalse)
        {
        tug2Id = AtSdhChannelTug2Get(sdhChannel);
        vtId   = AtSdhChannelTu1xGet(sdhChannel);
        }

    return (uint32)(32 * stsId + 4 * tug2Id + vtId);
    }

static void HwOcnToPdhB3V5Control(AtChannel self)
    {
    uint32 regVal  = 0;
    uint32 regAddr = cThaRegOcn2PdhB3V5CheckControl + PartOffset(self);
    eBool  isVc1x  = BoundCircuitIsVc1x(self);

    mRegFieldSet(regVal, cThaRegOcn2PdhB3V5CheckPos, B3V5CheckPos(self));
    mRegFieldSet(regVal, cThaRegOcn2PdhB3V5CheckVc4_4cMode, mBoolToBin(IsCepVc4_4c(self)));
    mRegFieldSet(regVal, cThaRegOcn2PdhB3V5CheckVc4Mode, mBoolToBin(IsCepVc4(self)));
    mRegFieldSet(regVal, cThaRegOcn2PdhB3V5CheckVtBasic, mBoolToBin(isVc1x));
    mRegFieldSet(regVal, cThaRegOcn2PdhB3V5CheckLineId, SdhChannelFlatIdGet(self, cThaModuleOcn));
    mChannelHwWrite(self, regAddr, regVal, cAtModulePw);
    }

static void HwPdhToDemapB3V5Control(AtChannel self)
    {
    uint32 regVal  = 0;
    uint32 regAddr = cThaRegPdh2DemapB3V5CheckControl + PartOffset(self);
    eBool  isVc1x  = BoundCircuitIsVc1x(self);

    mRegFieldSet(regVal, cThaRegPdh2DemapB3V5CheckPos, B3V5CheckPos(self));
    mRegFieldSet(regVal, cThaRegPdh2DemapB3V5CheckVc4_4cMode, mBoolToBin(IsCepVc4_4c(self)));
    mRegFieldSet(regVal, cThaRegPdh2DemapB3V5CheckVc4Mode, mBoolToBin(IsCepVc4(self)));
    mRegFieldSet(regVal, cThaRegPdh2DemapB3V5CheckVtBasic, mBoolToBin(isVc1x));
    mRegFieldSet(regVal, cThaRegPdh2DemapB3V5CheckLineId, SdhChannelFlatIdGet(self, cAtModulePdh));
    mChannelHwWrite(self, regAddr, regVal, cAtModulePw);
    }

static void HwPweToBmtB3V5Control(AtChannel self)
    {
    uint32 regVal  = 0;
    uint32 regAddr = cThaRegPwe2BmtB3V5CheckControl + PartOffset(self);
    eBool isVc1x  = BoundCircuitIsVc1x(self);
    uint8 fractional = IsCepBasic(self) ? 0 : 1;

    mRegFieldSet(regVal, cThaRegPwe2BmtB3V5CheckVtBasic, mBoolToBin(isVc1x));
    mRegFieldSet(regVal, cThaRegPwe2BmtB3V5CheckPos, B3V5CheckPos(self));

    if (fractional)
        mRegFieldSet(regVal, cThaRegPwe2BmtB3V5CheckFracVt15, B3V5CheckFracVt15(self));

    mRegFieldSet(regVal, cThaRegPwe2BmtB3V5CheckFrac, fractional);
    mRegFieldSet(regVal,  cThaRegPwe2BmtB3V5CheckPwId, ThaModulePwLocalPwId(ModulePw(self), (AtPw)self));
    mChannelHwWrite(self, regAddr, regVal, cAtModulePw);
    }

static void Map2PlaErrorGet(AtChannel self, eBool *b3v5IsError)
    {
    uint32 regAddr = cThaRegHwMapToPlaB3V5CheckSticky + PartOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModulePw);
    *b3v5IsError   = mRegField(regVal, cThaRegMap2PlaB3V5CheckError) ? cAtTrue : cAtFalse;
    mChannelHwWrite(self, regAddr, 0xffffffff, cAtModulePw);
    }

static void RxPwCheckErrorGet(AtChannel self, eBool *cep2MapIsError, eBool *pda2CepIsError, eBool *cla2PdaIsError)
    {
    uint32 regAddr   = cThaRegRxPwB3V5CheckSticky + PartOffset(self);
    uint32 regVal    = mChannelHwRead(self, regAddr, cAtModulePw);
    *cep2MapIsError  = mRegField(regVal, cThaRegRxPwB3V5CheckCep2MapErr) ? cAtTrue : cAtFalse;
    *pda2CepIsError  = mRegField(regVal, cThaRegRxPwB3V5CheckPda2CepErr) ? cAtTrue : cAtFalse;
    *cla2PdaIsError  = mRegField(regVal, cThaRegRxPwB3V5CheckCla2PdaErr) ? cAtTrue : cAtFalse;
    }

static void Ocn2Pdh2DemapErrorGet(AtChannel self, eBool *ocn2PdhIsError, eBool *pdh2DemapIsError)
    {
    uint32 regAddr    = cThaRegOcn2Pdh2DemapB3V5CheckSticky + PartOffset(self);
    uint32 regVal     = mChannelHwRead(self, regAddr, cAtModulePw);
    *pdh2DemapIsError = mRegField(regVal, cThaRegPdh2DemapB3V5Check) ? cAtTrue : cAtFalse;
    *ocn2PdhIsError   = mRegField(regVal, cThaRegOcn2PdhB3V5Check) ? cAtTrue : cAtFalse;
    mChannelHwWrite(self, regAddr, 0xffffffff, cAtModulePw);
    }

static void Pwe2BmtErrorGet(AtChannel self, eBool *b3v5IsError)
    {
    uint32 regAddr = cThaRegPwe2BmtB3V5CheckSticky + PartOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModulePw);
    *b3v5IsError   = mRegField(regVal, cThaRegPwe2BmtB3V5Check) ? cAtTrue : cAtFalse;
    mChannelHwWrite(self, regAddr, 0xffffffff, cAtModulePw);
    }

static eAtRet DebugPrint(AtChannel self,
                         eBool vtBasicIsError,
                         eBool cep2MapIsError,
                         eBool pda2CepIsError,
                         eBool cla2PdaIsError,
                         eBool ocn2PdhIsError,
                         eBool pdh2DemapIsError,
                         eBool pwe2BmtIsError)
    {
    eAtSevLevel sevlevel;
	AtUnused(self);

    AtPrintc(cSevNormal, "=================================================\r\n");
    AtPrintc(cSevNormal, "= B3 or V5 debug\r\n");
    AtPrintc(cSevNormal, "=================================================\r\n");

    sevlevel = ocn2PdhIsError ? cSevCritical : cSevInfo;
    AtPrintc(sevlevel, "OCN to PDH     :   %s\r\n", ocn2PdhIsError ? "error" : "good");

    sevlevel = pdh2DemapIsError ? cSevCritical : cSevInfo;
    AtPrintc(sevlevel, "PDH to Demap   :   %s\r\n", pdh2DemapIsError ? "error" : "good");

    sevlevel = vtBasicIsError ? cSevCritical : cSevInfo;
    AtPrintc(sevlevel, "Map to Pla     :   %s\r\n", vtBasicIsError ? "error" : "good");

    sevlevel = cep2MapIsError ? cSevCritical : cSevInfo;
    AtPrintc(sevlevel, "Cep to Map     :   %s\r\n", cep2MapIsError ? "error" : "good");

    sevlevel = pda2CepIsError ? cSevCritical : cSevInfo;
    AtPrintc(sevlevel, "Pda to Cep     :   %s\r\n", pda2CepIsError ? "error" : "good");

    sevlevel = cla2PdaIsError ? cSevCritical : cSevInfo;
    AtPrintc(sevlevel, "Cla to Pda     :   %s\r\n", cla2PdaIsError ? "error" : "good");

    sevlevel = pwe2BmtIsError ? cSevCritical : cSevInfo;
    AtPrintc(sevlevel, "Pwe to Bmt     :   %s\r\n", pwe2BmtIsError ? "error" : "good");

    return cAtOk;
    }

eAtRet ThaPwCepAdapterDebug(AtChannel self)
    {
    eBool vtBasicIsError;
    eBool cep2MapIsError;
    eBool pda2CepIsError;
    eBool cla2PdaIsError;
    eBool ocn2PdhIsError;
    eBool pdh2DemapIsError;
    eBool pwe2BmtIsError;
    AtOsal osal = AtSharedDriverOsalGet();
    eBool isSimulated;

    if (CircuitGet(self) == NULL)
        return cAtOk;

    if (mInAccessible(self))
        return cAtOk;

    /* Configure information to control B3/V5 debug engine */
    HwMapToPlaB3V5Control(self);
    HwRxPwB3V5Control(self);
    HwOcnToPdhB3V5Control(self);
    HwPdhToDemapB3V5Control(self);
    HwPweToBmtB3V5Control(self);

    isSimulated = AtDeviceIsSimulated(AtChannelDeviceGet(self));
    if (!isSimulated)
        mMethodsGet(osal)->USleep(osal, 50000);
    Map2PlaErrorGet(self, &vtBasicIsError);
    RxPwCheckErrorGet(self, &cep2MapIsError, &pda2CepIsError, &cla2PdaIsError);
    Ocn2Pdh2DemapErrorGet(self, &ocn2PdhIsError, &pdh2DemapIsError);
    Pwe2BmtErrorGet(self, &pwe2BmtIsError);

    /* If no error, check result again after one second */
    if ((cep2MapIsError == cAtFalse) && (pda2CepIsError == cAtFalse) &&
        (vtBasicIsError == cAtFalse) && (cla2PdaIsError == cAtFalse))
        {
        if (!isSimulated)
            mMethodsGet(osal)->Sleep(osal, 1);
        Map2PlaErrorGet(self, &vtBasicIsError);
        RxPwCheckErrorGet(self, &cep2MapIsError, &pda2CepIsError, &cla2PdaIsError);
        Ocn2Pdh2DemapErrorGet(self, &ocn2PdhIsError, &pdh2DemapIsError);
        Pwe2BmtErrorGet(self, &pwe2BmtIsError);
        }

    return DebugPrint(self, vtBasicIsError, cep2MapIsError, pda2CepIsError, cla2PdaIsError, ocn2PdhIsError, pdh2DemapIsError, pwe2BmtIsError);
    }

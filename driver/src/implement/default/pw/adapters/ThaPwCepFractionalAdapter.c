/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : ThaPwCepFractionalAdapter.c
 *
 * Created Date: Jan 6, 2014
 *
 * Description : CEP fractional adapter
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "ThaPwAdapterInternal.h"
#include "AtPwCep.h"

/*--------------------------- Define -----------------------------------------*/
typedef struct tThaPwCepFractionalAdapter   * ThaPwCepFractionalAdapter;

#define mThis(self) ((ThaPwCepFractionalAdapter)self)
#define cSpeNumRow 9

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods        m_AtObjectOverride;
static tAtChannelMethods       m_AtChannelOverride;
static tAtPwMethods            m_AtPwOverride;
static tThaPwAdapterMethods    m_ThaPwAdapterOverride;
static tThaPwCepAdapterMethods m_ThaPwCepAdapterOverride;

/* Save super's implementation */
static const tAtObjectMethods        *m_AtObjectMethods        = NULL;
static const tAtChannelMethods       *m_AtChannelMethods       = NULL;
static const tThaPwAdapterMethods    *m_ThaPwAdapterMethods    = NULL;
static const tAtPwMethods            *m_AtPwMethods            = NULL;
static const tThaPwCepAdapterMethods *m_ThaPwCepAdapterMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePda ModulePda(AtPw self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaModulePda)AtDeviceModuleGet(device, cThaModulePda);
    }

static ThaModulePwe ModulePwe(AtPw self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaModulePwe)AtDeviceModuleGet(device, cThaModulePwe);
    }

static eAtRet PwCepSubChannelHwEquip(ThaPwAdapter adapter)
    {
    AtPwCep pwCep = (AtPwCep)ThaPwAdapterPwGet(adapter);
    AtIterator iterator = AtPwCepEquippedChannelsIteratorCreate(pwCep);
    AtChannel equipedChannel;
    eAtRet ret = cAtOk;

    while((equipedChannel = (AtChannel)AtIteratorNext(iterator)) != NULL)
        {
        ret |= ThaPwCepAdapterEquip((ThaPwCepAdapter)adapter, equipedChannel, cAtTrue);
        }

    AtObjectDelete((AtObject)iterator);
    return ret;
    }

static uint16 AdapterMinPayloadSize(ThaPwAdapter self, AtChannel circuit, uint32 jitterBufferSize)
    {
	AtUnused(jitterBufferSize);
	AtUnused(circuit);
	AtUnused(self);
    return 3;
    }

static uint16 CDRTimingPktLenInBits(ThaPwAdapter self)
    {
    uint16 numRows = AtPwPayloadSizeGet((AtPw)self);
    return (uint16)(ThaPwCepFractionalSpePayloadSizeCalculate((AtPw)self, numRows) * 8);
    }

static uint32 VcRealNumContributeBytesGet(AtChannel sdhVc)
    {
    eAtSdhChannelType vcType = AtSdhChannelTypeGet((AtSdhChannel)sdhVc);

    if (vcType == cAtSdhChannelTypeVc11)
        return 27;

    if (vcType == cAtSdhChannelTypeVc12)
        return 36;

    if (vcType == cAtSdhChannelTypeVc3)
        return 774;

    return 0;
    }

static uint32 PdhRealNumContributeBytesGet(AtPdhChannel pdhChannel)
    {
    eAtPdhDe3FrameType de3FrameType = AtPdhChannelFrameTypeGet(pdhChannel);
    if ((de3FrameType == cAtPdhE3FrmG751) ||
        (de3FrameType == cAtPdhE3Frmg832) ||
        (de3FrameType == cAtPdhE3Unfrm))
        return 567;

    /* Assume that it is DS3 */
    return 792;
    }

static uint32 AllSubVCsRealNumContributeBytesGet(AtPw pwAdapter)
    {
    AtPw realPw = ThaPwAdapterPwGet((ThaPwAdapter)pwAdapter);
    AtIterator iterator = AtPwCepEquippedChannelsIteratorCreate((AtPwCep)realPw);
    const uint8 cVcNumPohBytes = 9;
    AtChannel channel;
    uint16 totalBytes = 0;

    while ((channel = (AtChannel)AtIteratorNext(iterator)) != NULL)
        totalBytes = (uint16)(totalBytes + VcRealNumContributeBytesGet(channel));

    AtObjectDelete((AtObject)iterator);
    totalBytes = (uint16)(totalBytes + cVcNumPohBytes);

    return totalBytes;
    }

static uint32 TotalRealBytesInOneSpe(AtPw pwAdapter)
    {
    AtSdhChannel sdhVc = (AtSdhChannel)AtPwBoundCircuitGet(pwAdapter);
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(sdhVc);

    if (channelType == cAtSdhChannelTypeVc4)
        return AllSubVCsRealNumContributeBytesGet(pwAdapter);

    if (channelType == cAtSdhChannelTypeVc3)
        {
        if (AtSdhChannelMapTypeGet(sdhVc) == cAtSdhVcMapTypeVc3MapDe3)
            return PdhRealNumContributeBytesGet((AtPdhChannel)AtSdhChannelMapChannelGet(sdhVc));

        if (AtSdhChannelMapTypeGet(sdhVc) == cAtSdhVcMapTypeVc3Map7xTug2s)
            return AllSubVCsRealNumContributeBytesGet(pwAdapter);

        return 0; /* Something wrong here */
        }

    return 0;
    }

static uint32 RealPayloadSizeCalculate(AtPw pwAdapter, uint32 numRow)
    {
    /* Invalid value */
    if (numRow == 0)
        return cBit31_0;

    return TotalRealBytesInOneSpe(pwAdapter) / numRow;
    }

static eAtRet HwPayloadSizeSet(ThaPwAdapter self, uint16 numRow)
    {
    ThaModulePwe pweModule;
    eAtRet ret = cAtOk;
    AtPw pw = (AtPw)self;
    uint32 payloadSizeInBytes = RealPayloadSizeCalculate((AtPw)self, numRow);

    pweModule = ModulePwe(pw);
    ret |= ThaModulePwePayloadSizeSet(pweModule, pw, numRow);
    ret |= ThaClaPwControllerPayloadSizeSet(ThaPwAdapterClaPwController(self), pw, (uint16)payloadSizeInBytes);
    ret |= mMethodsGet(self)->CdrPayloadUpdate(self);

    return ret;
    }

static eAtRet UpdateClaPayloadSize(ThaPwAdapter self)
    {
    AtPw pw = (AtPw)self;
    uint32 numRow = AtPwPayloadSizeGet(pw);
    uint32 payloadSizeInBytes = RealPayloadSizeCalculate(pw, numRow);
    return ThaClaPwControllerPayloadSizeSet(ThaPwAdapterClaPwController(self), pw, (uint16)payloadSizeInBytes);
    }

static eAtModulePwRet Equip(ThaPwCepAdapter self, AtChannel channel, eBool equip)
    {
    eAtRet ret = cAtOk;
    AtPw pwAdapter = (AtPw)self;
    AtOsal osal = AtSharedDriverOsalGet();
    static const uint32 cDelayTimeInUs = 1000;

    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        return cAtOk;

    ret |= ThaModulePdaPwCepEquip(ModulePda(pwAdapter), pwAdapter, channel, equip);
    ret |= UpdateClaPayloadSize(mAdapter(self));
    ret |= ThaModulePwePwCepEquip(ModulePwe(pwAdapter), pwAdapter, channel, equip);
    ret |= ThaPwAdapterJitterPayloadLengthSet(mAdapter(self));

    /* Hw recommend, delay 10ms for HW have time to acknowledge the change */
    mMethodsGet(osal)->USleep(osal, cDelayTimeInUs);

    return (eAtModulePwRet)ret;
    }

static eAtRet Debug(AtChannel self)
    {
    AtPw adapter = (AtPw)self;
    uint32 numRow;

    m_AtChannelMethods->Debug(self);

    numRow = AtPwPayloadSizeGet(adapter);
    AtPrintc(cSevInfo, "CEP fractional\r\n");
    AtPrintc(cSevNormal, "* Real payload size  : %u bytes\r\n", RealPayloadSizeCalculate(adapter, numRow));
    AtPrintc(cSevNormal, "* SPE payload size   : %u bytes\r\n", ThaPwCepFractionalSpePayloadSizeCalculate(adapter, numRow));
    AtPrintc(cSevNormal, "\r\n");

    return cAtOk;
    }

static uint32 PayloadSizeInBytes(ThaPwAdapter self)
    {
    uint32 payloadSize = AtPwPayloadSizeGet((AtPw)self);
    return ThaPwCepFractionalSpePayloadSizeCalculate((AtPw)self, payloadSize);
    }

static eAtModulePwRet EbmEnable(ThaPwCepAdapter self, eBool enable)
    {
    AtChannel circuit = AtPwBoundCircuitGet((AtPw)self);

    mThis(self)->ebmIsEnabled = enable;

    /* Just cache configuration if PW cannot be run yet */
    if (ThaPwAdapterIsLogicPw(mAdapter(self)) || (circuit == NULL))
        return cAtOk;

    return ThaPwCepAdapterEbmHwEnable(mAdapter(self), enable);
    }

static eBool EbmIsEnabled(ThaPwCepAdapter self)
    {
    return mThis(self)->ebmIsEnabled;
    }

static const char *TypeString(AtChannel self)
    {
	AtUnused(self);
    return "pw_cep_fractional_adapter";
    }

static eAtRet SetDefaultOnBinding(ThaPwAdapter self, AtChannel circuit)
    {
    AtPw pw = (AtPw)self;
    eAtRet ret = cAtOk;

    /* Let super do first */
    ret = m_ThaPwAdapterMethods->SetDefaultOnBinding(self, circuit);
    if (ret != cAtOk)
        return ret;

    ret |= ThaModulePweVcFragmentTypeSet(ModulePwe(pw), (AtSdhChannel)circuit);
    ret |= PwCepSubChannelHwEquip((ThaPwAdapter)self);

    /* Enable EBM if it is enabled before */
    ret |= ThaPwCepEbmEnable((ThaPwCepAdapter)self, ThaPwCepEbmIsEnabled((ThaPwCepAdapter)self));

    return ret;
    }

static eAtModulePwRet CircuitUnbind(AtPw self)
    {
    AtChannel circuit;
    eAtRet ret = cAtOk;
    AtPwCep pw = (AtPwCep)ThaPwAdapterPwGet(mAdapter(self));

    /* Already unbind */
    circuit = AtPwBoundCircuitGet(self);
    if (circuit == NULL)
        return cAtOk;

    /* Unequip channels first */
    while (AtPwCepNumEquippedChannelsGet(pw) > 0)
        {
        AtChannel subChannel = AtPwCepEquippedChannelAtIndexGet(pw, 0);
        ret |= AtPwCepEquip(pw, subChannel, cAtFalse);
        }

    if (ret != cAtOk)
        return ret;

    return m_AtPwMethods->CircuitUnbind(self);
    }

static uint32 PdaPayloadSizeCorrect(ThaPwAdapter pwAdapter, uint32 payloadSizeInByte)
    {
    AtSdhChannel circuit = (AtSdhChannel)AtPwBoundCircuitGet((AtPw)pwAdapter);
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(circuit);

    if (ThaModulePdaCepPayloadSizeShouldExcludeHeader(ModulePda((AtPw)pwAdapter)))
        return payloadSizeInByte;

    if (channelType == cAtSdhChannelTypeVc4)
        return payloadSizeInByte + 16;

    if (channelType == cAtSdhChannelTypeVc3)
        return payloadSizeInByte + 8;

    return payloadSizeInByte;
    }

static eAtRet JitterPayloadLengthSet(ThaPwAdapter self)
    {
    AtPw pw = (AtPw)self;
    ThaModulePda pdaModule = ModulePda(pw);
    uint32 numRow = AtPwPayloadSizeGet((AtPw)self);
    uint32 realPayloadSize = ThaPwCepFractionalRealPayloadSizeCalculate((AtPw)self, numRow);

    realPayloadSize = PdaPayloadSizeCorrect(self, realPayloadSize);
    ThaModulePdaPwJitterPayloadLengthSet(pdaModule, pw, realPayloadSize);

    return cAtOk;
    }

static uint16 PdaMaxNumRow(AtPw self, AtChannel circuit, uint32 jitterBufferSize)
    {
    /* Super return maximum num byte */
    uint32 maxNumBytes = m_AtPwMethods->MaxPayloadSize(self, circuit, jitterBufferSize);
    uint32 totalRealByte = TotalRealBytesInOneSpe(self);

    if (totalRealByte == 0)
        return cSpeNumRow;

    return (uint16)(maxNumBytes * cSpeNumRow / totalRealByte);
    }

static uint16 MaxPayloadSize(AtPw self, AtChannel circuit, uint32 jitterBufferSize)
    {
    uint16 pdaMaxNumRow = PdaMaxNumRow(self, circuit, jitterBufferSize);
    return (uint16)mMin(pdaMaxNumRow, cSpeNumRow);
    }

static eAtRet WarmRestore(AtChannel self)
    {
    AtList equippedChannels;

    eAtRet ret = m_AtChannelMethods->WarmRestore(self);
    if (ret != cAtOk)
        return ret;

    /* Restore equipped channels */
    equippedChannels = AtListCreate(0);
    ThaModulePwePwCepFractionalEquippedChannelsGet(ModulePwe((AtPw)self), (AtPw)self, equippedChannels);
    if (AtListLengthGet(equippedChannels) == 0)
        {
        AtObjectDelete((AtObject)equippedChannels);
        return cAtOk;
        }

    while(AtListLengthGet(equippedChannels) > 0)
        {
        AtChannel channel = (AtChannel)AtListObjectRemoveAtIndex(equippedChannels, 0);
        ret |= AtPwCepEquip((AtPwCep)ThaPwAdapterPwGet((ThaPwAdapter)self), channel, cAtTrue);
        }

    AtObjectDelete((AtObject)equippedChannels);
    return ret;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaPwCepFractionalAdapter object = (ThaPwCepFractionalAdapter)self;
    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(ebmIsEnabled);
    }

static void OverrideAtObject(ThaPwCepAdapter self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtPw(ThaPwCepAdapter self)
    {
    AtPw pw = (AtPw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPwMethods = mMethodsGet(pw);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPwOverride, m_AtPwMethods, sizeof(m_AtPwOverride));
        mMethodOverride(m_AtPwOverride, CircuitUnbind);
        mMethodOverride(m_AtPwOverride, MaxPayloadSize);
        }

    mMethodsSet(pw, &m_AtPwOverride);
    }

static void OverrideAtChannel(ThaPwCepAdapter self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, Debug);
        mMethodOverride(m_AtChannelOverride, TypeString);
        mMethodOverride(m_AtChannelOverride, WarmRestore);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideThaPwAdapter(ThaPwCepAdapter self)
    {
    ThaPwAdapter adapter = (ThaPwAdapter)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPwAdapterMethods = mMethodsGet(adapter);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwAdapterOverride, m_ThaPwAdapterMethods, sizeof(m_ThaPwAdapterOverride));

        mMethodOverride(m_ThaPwAdapterOverride, PayloadSizeInBytes);
        mMethodOverride(m_ThaPwAdapterOverride, HwPayloadSizeSet);
        mMethodOverride(m_ThaPwAdapterOverride, CDRTimingPktLenInBits);
        mMethodOverride(m_ThaPwAdapterOverride, SetDefaultOnBinding);
        mMethodOverride(m_ThaPwAdapterOverride, JitterPayloadLengthSet);
        mMethodOverride(m_ThaPwAdapterOverride, AdapterMinPayloadSize);
        }

    mMethodsSet(adapter, &m_ThaPwAdapterOverride);
    }

static void OverrideThaPwCepAdapter(ThaPwCepAdapter self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPwCepAdapterMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwCepAdapterOverride, m_ThaPwCepAdapterMethods, sizeof(m_ThaPwCepAdapterOverride));

        mMethodOverride(m_ThaPwCepAdapterOverride, Equip);
        mMethodOverride(m_ThaPwCepAdapterOverride, EbmEnable);
        mMethodOverride(m_ThaPwCepAdapterOverride, EbmIsEnabled);
        }

    mMethodsSet(self, &m_ThaPwCepAdapterOverride);
    }

static void Override(ThaPwCepAdapter self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtPw(self);
    OverrideThaPwAdapter(self);
    OverrideThaPwCepAdapter(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPwCepFractionalAdapter);
    }

static ThaPwCepAdapter ObjectInit(ThaPwCepAdapter self, AtPw pw)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPwCepAdapterObjectInit((ThaPwCepAdapter)self, pw) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwAdapter ThaPwCepFractionalAdapterNew(AtPw pw)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwCepAdapter newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return (ThaPwAdapter)ObjectInit(newController, pw);
    }

uint32 ThaPwCepFractionalRealPayloadSizeCalculate(AtPw pwAdapter, uint32 numRow)
    {
    return (TotalRealBytesInOneSpe(pwAdapter) * numRow / 9);
    }

uint32 ThaPwCepFractionalJ1ReturnCalculate(AtPw pwAdapter)
    {
    return TotalRealBytesInOneSpe(pwAdapter) / cSpeNumRow;
    }

uint32 ThaPwCepFractionalSpePayloadSizeCalculate(AtPw pwAdapter, uint32 numRows)
    {
    static const uint32 cVc4FrameSizeInByte = 783 * 3;
    static const uint32 cVc3FrameSizeInByte = 783;

    AtSdhChannel circuit = (AtSdhChannel)AtPwBoundCircuitGet((AtPw)pwAdapter);
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(circuit);

    /* The formular is: (vc frame in bit * numRow) / 9 */
    if (channelType == cAtSdhChannelTypeVc4)
        return (uint16)((cVc4FrameSizeInByte * numRows) / 9);

    if (channelType == cAtSdhChannelTypeVc3)
        return (uint16)((cVc3FrameSizeInByte * numRows) / 9);

    return 0;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : ThaPwMlpppAdapter.c
 *
 * Created Date: May 8, 2017
 *
 * Description : Mlppp Adapter
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtMpBundle.h"
#include "../../pwe/ThaModulePweInternal.h"
#include "../../cla/hbce/ThaHbce.h"
#include "../../prbs/ThaPrbsEngineInternal.h"
#include "../../encap/dynamic/ThaPhysicalHdlcChannel.h"
#include "../../sur/hard/ThaModuleHardSur.h"
#include "../activator/ThaPwActivator.h"
#include "../headercontrollers/ThaPwHeaderControllerInternal.h"
#include "../ethportbinder/ThaPwEthPortBinder.h"
#include "../ThaPwUtil.h"
#include "ThaPwAdapterInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods        m_AtChannelOverride;
static tThaPwHdlcAdapterMethods m_ThaPwHdlcAdapterOverride;

/* Save super's implementation */
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *TypeString(AtChannel self)
    {
    AtUnused(self);
    return "pw_mlppp_adapter";
    }

static ThaModulePda ModulePda(AtPw self)
    {
    return (ThaModulePda)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cThaModulePda);
    }

static ThaModulePwe ModulePwe(AtPw self)
    {
    return (ThaModulePwe)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cThaModulePwe);
    }

static ThaClaPwController ClaPwController(AtPw self)
    {
    ThaModuleCla claModule = (ThaModuleCla)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cThaModuleCla);
    return ThaModuleClaPwControllerGet(claModule);
    }

static eAtRet PayloadTypeSet(ThaPwHdlcAdapter self, eAtPwHdlcPayloadType payloadType)
    {
    eAtRet ret;
    AtPw pw = (AtPw)self;

    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        self->payloadType = payloadType;
        return cAtOk;
        }

    ret = ThaModulePdaMlpppPwPayloadTypeSet(ModulePda(pw), pw, payloadType);
    ret |= ThaModulePweHdlcPwPayloadTypeSet(ModulePwe(pw), self, payloadType);
    ret |= ThaClaPwControllerMlpppPwPayloadTypeSet(ClaPwController(pw), pw, payloadType);

    return ret;
    }

static const uint8* CircuitEncapHeader(ThaPwHdlcAdapter self, uint8* numBytes)
    {
    static uint8 mlpppHeader[16];
    uint8 mlpppHeaderLen = 0;
    AtMpBundle bundle = (AtMpBundle)AtPwBoundCircuitGet((AtPw)self);
    eAtHdlcPduType pduType = AtHdlcBundlePduTypeGet((AtHdlcBundle)bundle);

    /* Address/Control/PID */
    mlpppHeader[mlpppHeaderLen++] = 0xFF;
    mlpppHeader[mlpppHeaderLen++] = 0x03;
    mlpppHeader[mlpppHeaderLen++] = 0x00;
    mlpppHeader[mlpppHeaderLen++] = 0x3D;

    /* MP Sequence */
    mlpppHeader[mlpppHeaderLen++] = 0xC0;
    mlpppHeader[mlpppHeaderLen++] = 0x00;

    if (AtMpBundleRxSequenceModeGet(bundle) == cAtMpSequenceModeLong)
        {
        mlpppHeader[mlpppHeaderLen++] = 0x00;
        mlpppHeader[mlpppHeaderLen++] = 0x00;
        }

    /* PDU */
    if (pduType == cAtHdlcPduTypeIPv4)
        {
        mlpppHeader[mlpppHeaderLen++] = 0x00;
        mlpppHeader[mlpppHeaderLen++] = 0x21;
        }

    else if (pduType == cAtHdlcPduTypeIPv6)
        {
        mlpppHeader[mlpppHeaderLen++] = 0x00;
        mlpppHeader[mlpppHeaderLen++] = 0x57;
        }

    else if (pduType == cAtHdlcPduTypeEthernet)
        {
        mlpppHeader[mlpppHeaderLen++] = 0x00;
        mlpppHeader[mlpppHeaderLen++] = 0x31;
        }

    else if (pduType == cAtHdlcPduTypeAny)
        {
        mlpppHeader[mlpppHeaderLen++] = 0xFF;
        mlpppHeader[mlpppHeaderLen++] = 0xFF;
        }

    else
        {
        mlpppHeader[mlpppHeaderLen++] = 0x0;
        mlpppHeader[mlpppHeaderLen++] = 0x0;
        }

    *numBytes = mlpppHeaderLen;
    mlpppHeader[mlpppHeaderLen] = '\0';
    return mlpppHeader;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPwMlpppAdapter);
    }

static void OverrideAtChannel(ThaPwAdapter self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, TypeString);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideThaPwHdlcAdapter(ThaPwAdapter self)
    {
    ThaPwHdlcAdapter adapter = (ThaPwHdlcAdapter)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwHdlcAdapterOverride, mMethodsGet(adapter), sizeof(m_ThaPwHdlcAdapterOverride));

        mMethodOverride(m_ThaPwHdlcAdapterOverride, PayloadTypeSet);
        mMethodOverride(m_ThaPwHdlcAdapterOverride, CircuitEncapHeader);
        }

    mMethodsSet(adapter, &m_ThaPwHdlcAdapterOverride);
    }

static void Override(ThaPwAdapter self)
    {
    OverrideAtChannel(self);
    OverrideThaPwHdlcAdapter(self);
    }

static ThaPwMlpppAdapter ObjectInit(ThaPwMlpppAdapter self, AtPw pw)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPwHdlcAdapterObjectInit((ThaPwHdlcAdapter)self, pw) == NULL)
        return NULL;

    /* Setup class */
    Override((ThaPwAdapter)self);
    m_methodsInit = 1;

    return self;
    }

ThaPwAdapter ThaPwMlpppAdapterNew(AtPw pw)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwMlpppAdapter newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return (ThaPwAdapter)ObjectInit(newController, pw);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CESoPAdatper for payloadsize in bytes
 *
 * File        : ThaPwCESoPAdapterInBytes.c
 *
 * Created Date: Jun 4, 2018
 *
 * Description : Implement payloadsize in bytes for CESOP adapterl
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaPwAdapterInternal.h"
#include "../../pwe/ThaModulePwe.h"
#include "../../sdh/ThaModuleSdh.h"
#include "../../sur/hard/ThaModuleHardSur.h"
#include "../ethportbinder/ThaPwEthPortBinder.h"
#include "../ThaPwUtil.h"

/*--------------------------- Define -----------------------------------------*/
#define cMinCESoPJitterDelayInUs 125

/*--------------------------- Macros -----------------------------------------*/
#define mInAccessible(self) AtChannelInAccessible((AtChannel)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods    m_AtChannelOverride;
static tThaPwAdapterMethods m_ThaPwAdapterOverride;
static tAtPwMethods         m_AtPwOverride;

/* Save super's implementation */
static const tAtPwMethods         *m_AtPwMethods         = NULL;
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;
static const tThaPwAdapterMethods *m_ThaPwAdapterMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaPwCESoPAdapterInBytes);
    }

static ThaModulePwe ModulePwe(AtPw self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaModulePwe)AtDeviceModuleGet(device, cThaModulePwe);
    }

static ThaModulePda ModulePda(AtPw self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaModulePda)AtDeviceModuleGet(device, cThaModulePda);
    }

static ThaModulePw ModulePw(AtPw self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaModulePw)AtDeviceModuleGet(device, cAtModulePw);
    }

static const char *TypeString(AtChannel self)
    {
    AtUnused(self);
    return "pw_cesop_ib_adapter";
    }

static uint16 JitterPayloadLengthCalculate(ThaPwAdapter self)
    {
    return AtPwPayloadSizeGet((AtPw)self);
    }

static eAtRet JitterPayloadLengthSet(ThaPwAdapter self)
    {
    AtPw pw = (AtPw)self;
    ThaModulePda pdaModule = ModulePda(pw);
    ThaModulePdaPwJitterPayloadLengthSet(pdaModule, pw, JitterPayloadLengthCalculate(self));
    return cAtOk;
    }

static uint32 DbPayloadSizeInBytes(ThaPwAdapter self)
    {
    return AtPwPayloadSizeGet((AtPw)self);
    }

static uint32 PayloadSizeInBytes(ThaPwAdapter self)
    {
    AtPw pw = (AtPw)self;
    ThaModulePwe pweModule = (ThaModulePwe)ModulePwe(pw);

    if (mInAccessible(self) || ThaPwAdapterIsLogicPw(self))
        return DbPayloadSizeInBytes(self);

    return (uint32)ThaModulePwePwNumDs0TimeslotsGet(pweModule, pw) *
                   ThaModulePweNumFramesInPacketGet(pweModule, pw);
    }

static uint16 DCRRxNcoConfPackLen(ThaPwAdapter self)
    {
    uint32 numberOfBytes = AtPwPayloadSizeGet((AtPw)self);
    AtChannel circuit = AtPwBoundCircuitGet((AtPw)self);
    uint32 numberOfFrames = ThaPwUtilCesopPayloadSizeInByteToInFrames((AtPw)self, circuit, numberOfBytes);
    return (uint16)(numberOfFrames * 32);
    }

static uint16 CDRTimingPktLenInBits(ThaPwAdapter self)
    {
    uint32 numberOfBytes = AtPwPayloadSizeGet((AtPw)self);
    AtChannel circuit = AtPwBoundCircuitGet((AtPw)self);
    uint32 numFramesInPacket = ThaPwUtilCesopPayloadSizeInByteToInFrames((AtPw)self, circuit, numberOfBytes);
    return (uint16)((AtPdhDe1IsE1(AtPdhNxDS0De1Get((AtPdhNxDS0)circuit)) ? 256 : 193) * numFramesInPacket);
    }

static uint32 DefaultPayloadSize(ThaPwAdapter self, AtChannel circuit)
    {
    static const uint32 cDefaultPayloadSize = 8;
    uint8  numSlots = 0;
    AtUnused(self);

    if (!circuit)
        return cDefaultPayloadSize;

    numSlots = AtPdhNxDs0NumTimeslotsGet((AtPdhNxDS0)circuit);
    if (numSlots == 0)
        return cDefaultPayloadSize;

    return (uint32)((uint32)numSlots * cDefaultPayloadSize);
    }

static uint16 PdaMaxNumFrame(AtPw self, AtChannel circuit, uint32 jitterBufferSize)
    {
    /* Super return maximum num byte */
    uint32 numBytes = ThaPwAdapterDefaultClassMaxPayloadSize(self, circuit, jitterBufferSize);
    uint8  numSlots = AtPdhNxDs0NumTimeslotsGet((AtPdhNxDS0)circuit);
    uint16 pdaMaxNumFrames = ThaModulePdaCESoPMaxNumFrames(ModulePda(self), self);
    uint16 numFrames;

    if (numSlots == 0)
        numFrames = (uint16)DefaultPayloadSize((ThaPwAdapter)self, circuit);
    else
        {
        numFrames = (uint16)(numBytes / numSlots);
        if (numFrames == 0)
            numFrames = 1;
        }

    return (uint16)mMin(numFrames, pdaMaxNumFrames);
    }

static ThaPwEthPortBinder EthPortBinder(AtPw self)
    {
    return ThaModulePwEthPortBinder(ModulePw(self));
    }

/*
M (number of frames)   number of frame could be 1 to 64 frames
N (number of timeslot)  The number of timeslot in each frame could be 1 to 31 for E1 or 1 to 24 for DS1
Constrain   Value M and N must be met with the constraint M*N >= 8
*/
static uint16 MinPayloadSize(AtPw self, AtChannel circuit, uint32 jitterBufferSize)
    {
    uint32 minNumFrameByJitterBuffer = mMethodsGet(mAdapter(self))->AdapterMinPayloadSize(mAdapter(self), circuit, jitterBufferSize);
    uint64 remainingBwForPw = ThaPwEthPortBinderPwRemainingBw(EthPortBinder(self), self);
    uint32 minPayloadSizeByBwController = ThaPwAdapterPayloadSizeInByteFromBwCalculate(self, remainingBwForPw);
    uint8  numSlots = AtPdhNxDs0NumTimeslotsGet((AtPdhNxDS0)circuit);
    uint32 minNumFrameByBw = (numSlots > 0) ? minPayloadSizeByBwController / numSlots : 1;
    uint32 minNumFrames = mMax(minNumFrameByJitterBuffer, minNumFrameByBw);
    uint16 minNumBytes = (uint16)(numSlots * minNumFrames);
    uint16 pdaMinNumBytes = ThaModulePdaCESoPMinPayloadSize(ModulePda(self), self);

    return (uint16)mMax(minNumBytes, pdaMinNumBytes);
    }

/* Maximum number of bytes */
static uint16 MaxPayloadSize(AtPw self, AtChannel circuit, uint32 jitterBufferSize)
    {
    uint16 pdaMaxNumFrame = PdaMaxNumFrame(self, circuit, jitterBufferSize);
    uint16 pweMaxNumFrame = ThaModulePwePwCesMaxNumFramesInPacket(ModulePwe(self));
    uint32 maxNumFrames = mMin(pdaMaxNumFrame, pweMaxNumFrame);
    uint8  numSlots = AtPdhNxDs0NumTimeslotsGet((AtPdhNxDS0)circuit);
    const uint8 cMaxNumSlots = 31;

    if (numSlots == 0)
        numSlots = cMaxNumSlots;

    return (uint16)(numSlots * maxNumFrames);
    }

static eAtRet HwPayloadSizeSet(ThaPwAdapter self, uint16 numBytesPerPacket)
    {
    eAtRet ret = cAtOk;
    AtPw pw = (AtPw)self;
    ThaModulePwe pweModule = ModulePwe(pw);
    uint16 numDs0Slots = mMethodsGet(self)->NumDs0Slots(self);
    uint16 numFramesPerPacket = (uint16)(numBytesPerPacket / (numDs0Slots ? numDs0Slots : (uint16)1));
    uint32 lopsThreshold = AtPwLopsSetThresholdGet(pw);

    ret |= ThaModulePwePwNumFramesInPacketSet(pweModule, pw, numFramesPerPacket);
    ret |= ThaModulePwePwNumDs0TimeslotsSet(pweModule, pw, numDs0Slots);
    ret |= ThaClaPwControllerPayloadSizeSet(ThaPwAdapterClaPwController(self), pw, (uint16)PayloadSizeInBytes(self));
    ret |= mMethodsGet(self)->CdrPayloadUpdate(self);

    if ((ret != cAtOk) || (lopsThreshold == 0))
        return ret;

    /* Reconfigure LOPS value to update HW field */
    return AtPwLopsSetThresholdSet((AtPw)ThaPwAdapterPwDefectControllerGet(self), lopsThreshold);
    }

static eBool LimitResourceIsEnabled(AtPw self)
    {
    ThaModulePw  pwModule = (ThaModulePw)AtChannelModuleGet((AtChannel)self);
    return ThaModulePwResourcesLimitationIsDisabled(pwModule) ? cAtFalse : cAtTrue;
    }

static uint16 PayloadSizeRoundDown(AtChannel circuit, uint16 userPayloadSizeInBytes)
    {
    uint8 numberOfDs0 = 0;
    uint16 numFrames = 0;
    uint16 roundedPayloadSize = 0;

    if (circuit == NULL)
        return userPayloadSizeInBytes;

    numberOfDs0 = AtPdhNxDs0NumTimeslotsGet((AtPdhNxDS0)circuit);
    numFrames = (uint16)(userPayloadSizeInBytes / numberOfDs0);
    roundedPayloadSize = (uint16)(numFrames * numberOfDs0);
    return roundedPayloadSize;
    }

static eBool PayloadSizeIsValid(ThaPwAdapter self, uint16 roundedPayloadSizeInBytes)
    {
    AtChannel circuit = AtPwBoundCircuitGet((AtPw)self);
    uint8 numberOfDs0 = 0;
    uint16 numFramesPerPacket = 0;

    if (!circuit)
        return cAtTrue;

    numberOfDs0 = AtPdhNxDs0NumTimeslotsGet((AtPdhNxDS0)circuit);
    numFramesPerPacket = (uint16)(roundedPayloadSizeInBytes / numberOfDs0);

    return ThaPwCESoPAdapterPayloadSizeInFrameIsValid((ThaPwCESoPAdapter)self, circuit, numFramesPerPacket);
    }

static eAtModulePwRet PayloadSizeSet(AtPw self, uint16 userPayloadSizeInBytes)
    {
    eBool claEnabled;
    ThaPwAdapter adapter = (ThaPwAdapter)self;
    eAtRet ret = cAtOk;
    ThaModulePda pdaModule = ModulePda(self);
    static const uint8 cMinPayloadSize = 2;
    AtChannel circuit = AtPwBoundCircuitGet(self);
    uint64 newBw;
    uint32 newOccupiedBlock;
    uint16 payloadSizeInBytes = PayloadSizeRoundDown(circuit, userPayloadSizeInBytes);

    /* Need to check if payload size is in range if there is already a circuit
     * bound to it */

    if (!ThaPwAdatperPayloadSizeIsValid(adapter, payloadSizeInBytes))
        return cAtErrorInvlParm;

    if (LimitResourceIsEnabled(self) && (circuit != NULL))
        {
        if (payloadSizeInBytes > mMethodsGet(adapter)->PdaMaxPayloadSize(adapter, pdaModule))
            return cAtErrorOutOfRangParm;

        /* Check if payload size is in range */
        if (!ThaPwAdapterPayloadSizeIsInRange(self, payloadSizeInBytes))
            return cAtErrorOutOfRangParm;
        }

    /* Just cache when PW has not been activated */
    if (ThaPwAdapterIsLogicPw(mAdapter(self)) || (circuit == NULL))
        {
        mAdapter(self)->payloadSize = payloadSizeInBytes;
        return ThaPwAdapterDefaultClassPayloadSizeSet(self, payloadSizeInBytes);
        }

    /* Check if payload size is in range */
    if (LimitResourceIsEnabled(self) && !ThaPwAdapterPayloadSizeIsInRange(self, payloadSizeInBytes))
        return cAtErrorOutOfRangParm;

    if (LimitResourceIsEnabled(self) && payloadSizeInBytes < cMinPayloadSize)
        return cAtErrorInvlParm;

    /* Check if bandwidth is acceptable */
    newBw = ThaPwAdapterBandwidthCalculateWithPayload(self, payloadSizeInBytes);
    if (LimitResourceIsEnabled(self) && !ThaPwEthPortBinderPwBandwidthIsAcceptable(EthPortBinder(self), self, newBw))
        return cAtErrorBandwidthExceeded;

    /* Check if PDA block is available for new payload size */
    newOccupiedBlock = ThaModulePdaPwNumBlockByBufferInUs(pdaModule, self, AtPwJitterBufferSizeGet(self), payloadSizeInBytes);
    if (LimitResourceIsEnabled(self) && !ThaModulePdaResourceIsAvailable(ModulePda(self), self, newOccupiedBlock))
        return cAtErrorRsrcNoAvail;

    ThaPwAdapterStartRemoving(self);

    /* Disable CLA to stop packets come to FPGA */
    claEnabled = ThaPwAdapterClaIsEnabled(self);
    ThaModulePdaHotConfigureStart(pdaModule, self);

    if (mInAccessible(self))
        mAdapter(self)->payloadSize = payloadSizeInBytes;

    ret |= mMethodsGet(adapter)->HwPayloadSizeSet(adapter, payloadSizeInBytes);
    ret |= ThaPwAdapterJitterBufferDefaultSet(adapter);

    /* Enable PW */
    if (claEnabled)
        ret |= ThaPwAdapterClaEnable(self, cAtTrue);

    ThaModulePdaHotConfigureStop(pdaModule, self);
    if (ret == cAtOk)
        mAdapter(self)->payloadSize = payloadSizeInBytes;

    ThaPwAdapterFinishRemoving(self);

    if (ret != cAtOk)
        return ret;

    ret = ThaPwEthPortBinderPwBandwidthUpdate(EthPortBinder(self), self, newBw);
    ret |= ThaModulePdaTotalInUsedRamBlockUpdate(pdaModule, self, newOccupiedBlock);

    return ret;
    }

static uint16 HwPayloadSizeGet(ThaPwAdapter self)
    {
    AtPw pw = (AtPw)self;
    uint8 numDs0 = mMethodsGet(self)->NumDs0Slots(self);
    return (uint16)(numDs0 * ThaModulePweNumFramesInPacketGet(ModulePwe(pw), pw));
    }

static void OverrideAtChannel(ThaPwCESoPAdapter self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, TypeString);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideThaPwAdapter(ThaPwCESoPAdapter self)
    {
    ThaPwAdapter adapter = (ThaPwAdapter)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPwAdapterMethods = mMethodsGet(adapter);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwAdapterOverride, m_ThaPwAdapterMethods, sizeof(m_ThaPwAdapterOverride));

        mMethodOverride(m_ThaPwAdapterOverride, JitterPayloadLengthSet);
        mMethodOverride(m_ThaPwAdapterOverride, PayloadSizeInBytes);
        mMethodOverride(m_ThaPwAdapterOverride, DCRRxNcoConfPackLen);
        mMethodOverride(m_ThaPwAdapterOverride, CDRTimingPktLenInBits);
        mMethodOverride(m_ThaPwAdapterOverride, HwPayloadSizeSet);
        mMethodOverride(m_ThaPwAdapterOverride, HwPayloadSizeGet);
        mMethodOverride(m_ThaPwAdapterOverride, DefaultPayloadSize);
        mMethodOverride(m_ThaPwAdapterOverride, PayloadSizeIsValid);
        }

    mMethodsSet(adapter, &m_ThaPwAdapterOverride);
    }

static void OverrideAtPw(ThaPwCESoPAdapter self)
    {
    AtPw pw = (AtPw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPwMethods = mMethodsGet(pw);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPwOverride, m_AtPwMethods, sizeof(m_AtPwOverride));

        mMethodOverride(m_AtPwOverride, MinPayloadSize);
        mMethodOverride(m_AtPwOverride, MaxPayloadSize);
        mMethodOverride(m_AtPwOverride, PayloadSizeSet);
        }

    mMethodsSet(pw, &m_AtPwOverride);
    }

static void Override(ThaPwCESoPAdapter self)
    {
    OverrideAtChannel(self);
    OverrideThaPwAdapter(self);
    OverrideAtPw(self);
    }

static ThaPwCESoPAdapter ObjectInit(ThaPwCESoPAdapter self, AtPw pw)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPwCESoPAdapterObjectInit(self, pw) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwAdapter ThaPwCESoPAdapterInBytesNew(AtPw pw)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwCESoPAdapter newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return (ThaPwAdapter)ObjectInit(newController, pw);
    }

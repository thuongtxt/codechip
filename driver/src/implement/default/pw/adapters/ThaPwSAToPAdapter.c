/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : ThaPwSAToPAdapter.c
 *
 * Created Date: Apr 02, 2013
 *
 * Description : Thalassa PW implementation
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaPwAdapterInternal.h"
#include "../../sdh/ThaModuleSdh.h"
#include "../../../../generic/pdh/AtPdhDe1Internal.h"
#include "../../../../generic/pdh/AtPdhDe3Internal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaPwSAToPAdapter * ThaPwSAToPAdapter;

typedef struct tThaPwSAToPAdapterMethods
    {
    uint32 (*DefaultOffset)(AtPw self);
    }tThaPwSAToPAdapterMethods;

typedef struct tThaPwSAToPAdapter
    {
    tThaPwAdapter  super;
    const tThaPwSAToPAdapterMethods *methods;
    }tThaPwSAToPAdapter;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaPwSAToPAdapterMethods m_methods;

/* Override */
static tAtChannelMethods    m_AtChannelOverride;
static tThaPwAdapterMethods m_ThaPwAdapterOverride;

/* Save super's implementation */
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;
static const tThaPwAdapterMethods *m_ThaPwAdapterMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePda ModulePda(AtPw self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaModulePda)AtDeviceModuleGet(device, cThaModulePda);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPwSAToPAdapter);
    }

static const char *TypeString(AtChannel self)
    {
	AtUnused(self);
    return "pw_satop_adapter";
    }

static AtPwCounters CountersCreate(ThaPwAdapter self)
    {
	AtUnused(self);
    return (AtPwCounters)AtPwSAToPCountersNew();
    }

static uint32 DefaultOffset(AtPw self)
    {
    return AtChannelIdGet((AtChannel)self);
    }

static eAtRet AllCountersReadToClear(AtChannel self, void *pAllCounters, eBool clear)
    {
    eAtRet ret;
	AtUnused(pAllCounters);

    /* Read TDM counters */
    ret = ThaTdmPwCountersReadToClear((AtPw)self, clear);
    if (ret != cAtOk)
        return ret;

    /* More counters may be added in the future */

    return cAtOk;
    }

static eAtRet AllCountersGet(AtChannel self, void *pAllCounters)
    {
    eAtRet ret;

    /* Let the super do first */
    ret = m_AtChannelMethods->AllCountersGet(self, pAllCounters);
    if (ret != cAtOk)
        return ret;

    /* Then read additional counters */
    return AllCountersReadToClear(self, pAllCounters, cAtFalse);
    }

static eAtRet AllCountersClear(AtChannel self, void *pAllCounters)
    {
    eAtRet ret;

    /* Let the super do first */
    ret = m_AtChannelMethods->AllCountersClear(self, pAllCounters);
    if (ret != cAtOk)
        return ret;

    /* Then clear additional counters */
    return AllCountersReadToClear(self, pAllCounters, cAtTrue);
    }

static eBool CircuitIsDe1(ThaPwAdapter self, AtChannel circuit)
    {
    uint32 dataRateInByte = AtChannelDataRateInBytesPer125Us(circuit);
    AtUnused(self);
    if ((dataRateInByte == 32) || (dataRateInByte == 24))
        return cAtTrue;
    return cAtFalse;
    }

static uint32 DefaultPayloadSize(ThaPwAdapter self, AtChannel circuit)
    {
    if (CircuitIsDe1(self, circuit))
        return AtPdhDe1IsE1((AtPdhDe1)circuit) ? 256 : 193;

	/* Default for DS3,E3 as suggested by rfc 4553 */
    return 1024;
    }

static uint8 PartOfCircuit(ThaPwAdapter self, AtChannel circuit)
    {
    AtSdhChannel vc = (AtSdhChannel)AtPdhChannelVcInternalGet((AtPdhChannel)circuit);
	AtUnused(self);
    return (uint8)(vc ? ThaModuleSdhPartOfChannel(vc) : 0);
    }

static eAtRet SetDefaultOnBinding(ThaPwAdapter self, AtChannel circuit)
    {
    AtPw pw = (AtPw)self;
    ThaModulePda pdaModule = ModulePda(pw);

    eAtRet ret = m_ThaPwAdapterMethods->SetDefaultOnBinding(self, circuit);

    /* As hardware recommend with DS3/E3 need configure TDM Payload mode = 0xf,
     * other mode keep as current */
    ret |= ThaModulePdaSAToPModeSet(pdaModule, self, circuit);
    ret |= ThaModulePdaPwCircuitTypeSet(pdaModule, pw, circuit);

    return ret;
    }

static ThaCdrController CdrController(ThaPwAdapter self)
    {
    AtChannel circuit = AtPwBoundCircuitGet((AtPw)self);

    if (circuit == NULL)
        return NULL;

    if (CircuitIsDe1(self, circuit) == cAtTrue)
        return ThaPdhDe1CdrControllerGet((ThaPdhDe1)circuit);

    return ThaPdhDe3CdrControllerGet((ThaPdhDe3)circuit);
    }

static uint16 PdaMaxPayloadSize(ThaPwAdapter self, ThaModulePda pdaModule)
    {
    return ThaModulePdaSAToPMaxPayloadSize(pdaModule, (AtPw)self);
    }

static uint16 PdaMinPayloadSize(ThaPwAdapter self, ThaModulePda pdaModule)
    {
    return ThaModulePdaSAToPMinPayloadSize(pdaModule, (AtPw)self);
    }

static uint8 CircuitSliceByModule(ThaPwAdapter self, AtChannel circuit, eAtModule module)
    {
    uint8 sliceId, hwIdInSlice;
    AtUnused(self);
    ThaPdhChannelHwIdGet((AtPdhChannel)circuit, module, &sliceId, &hwIdInSlice);
    return sliceId;
    }

static uint8 PdaCircuitSlice(ThaPwAdapter self, AtChannel circuit)
    {
    return CircuitSliceByModule(self, circuit, cThaModulePda);
    }

static uint8 CdrCircuitSlice(ThaPwAdapter self, AtChannel circuit)
    {
    return CircuitSliceByModule(self, circuit, cThaModuleCdr);
    }

static uint8 PweCircuitSlice(ThaPwAdapter self, AtChannel circuit)
    {
    return CircuitSliceByModule(self, circuit, cThaModulePwe);
    }

static void OverrideAtChannel(ThaPwSAToPAdapter self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, TypeString);
        mMethodOverride(m_AtChannelOverride, AllCountersGet);
        mMethodOverride(m_AtChannelOverride, AllCountersClear);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideThaPwAdapter(ThaPwSAToPAdapter self)
    {
    ThaPwAdapter adapter = (ThaPwAdapter)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPwAdapterMethods = mMethodsGet(adapter);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwAdapterOverride, m_ThaPwAdapterMethods, sizeof(m_ThaPwAdapterOverride));

        mMethodOverride(m_ThaPwAdapterOverride, CountersCreate);
        mMethodOverride(m_ThaPwAdapterOverride, SetDefaultOnBinding);
        mMethodOverride(m_ThaPwAdapterOverride, CdrController);
        mMethodOverride(m_ThaPwAdapterOverride, DefaultPayloadSize);
        mMethodOverride(m_ThaPwAdapterOverride, PartOfCircuit);
        mMethodOverride(m_ThaPwAdapterOverride, PdaMaxPayloadSize);
        mMethodOverride(m_ThaPwAdapterOverride, PdaMinPayloadSize);
        mMethodOverride(m_ThaPwAdapterOverride, PdaCircuitSlice);
        mMethodOverride(m_ThaPwAdapterOverride, PweCircuitSlice);
        mMethodOverride(m_ThaPwAdapterOverride, CdrCircuitSlice);
        }

    mMethodsSet(adapter, &m_ThaPwAdapterOverride);
    }

static void Override(ThaPwSAToPAdapter self)
    {
    OverrideAtChannel(self);
    OverrideThaPwAdapter(self);
    }

static void MethodsInit(ThaPwSAToPAdapter self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, DefaultOffset);
        }

    mMethodsSet(self, &m_methods);
    }

static ThaPwSAToPAdapter ObjectInit(ThaPwSAToPAdapter self, AtPw pw)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPwAdapterObjectInit((ThaPwAdapter)self, pw) == NULL)
        return NULL;

    /* Override */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwAdapter ThaPwSAToPAdapterNew(AtPw pw)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwSAToPAdapter newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return (ThaPwAdapter)ObjectInit(newController, pw);
    }


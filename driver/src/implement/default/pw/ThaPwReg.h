/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : ThaPwReg.h
 *
 * Created Date: Nov 19, 2012
 *
 * Description : Thalassa Pseudowire register
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _THAPWREG_H_
#define _THAPWREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/*------------------------------------------------------------------------------
Reg Name: PWPDH Payload Assemble Payload Size Control
Reg Addr: 0x2C2000-0x2C23FF
          The address format for these registers is 0x2C2000 + pwid
          Where: pwid (0 � 1023) Pseudowire Identification
Reg Desc: These registers are used to configure payload size in each PW
          channel
------------------------------------------------------------------------------*/
#define cThaRegPWPdhPlaPldSizeCtrl   0x2C2000

/*--------------------------------------
BitField Name: PWPDHPlaAsmPldTypeCtrl
BitField Type: R/W
BitField Desc: Payload type in the PW channel
               - 0: SAToP
               - 1: AAL1 with raw mode
               - 2: AAL1 with CAS
               - 3: AAL1 without CAS
               - 4: CES with CAS
               - 5: CES without CAS
               - 6: CEP
--------------------------------------*/
#define cThaPWPdhPlaAsmPldTypeCtrlMask                 cBit18_15
#define cThaPWPdhPlaAsmPldTypeCtrlShift                15

/*--------------------------------------
BitField Name: PWPDHPlaAsmPldSizeCtrl
BitField Type: R/W
BitField Desc: Payload size in the PW channel SAToP mode: [11:0] payload size
               in a packet AAL1 mode
--------------------------------------*/
#define cThaPWPdhPlaAsmPldSizeCtrlMask                 cBit14_0
#define cThaPWPdhPlaAsmPldSizeCtrlShift                0

#define cThaPWPdhPlaAsmCesNumDs0PldSizeCtrlMask       cBit5_0
#define cThaPWPdhPlaAsmCesNumDs0PldSizeCtrlShift      0
#define cThaPWPdhPlaAsmCesNumFrmPldSizeCtrlMask       cBit14_6
#define cThaPWPdhPlaAsmCesNumFrmPldSizeCtrlShift      6


/*------------------------------------------------------------------------------
Reg Name: Thalassa PDHPW Payload Assemble Signaling Inverse Lookup Control
Reg Addr: 0x2C2800-0x2C28FF
          The address format for these registers is 0x2C2800 + pwid
Reg Desc: These registers are used to configure which timeslots are mapped into
          a Pseudowire channels. So this Pseudowire channel can read signaling
          byte in the signaling buffer to append into Pseudowire packet.
------------------------------------------------------------------------------*/
#define cThaRegPDHPWPlaSignalingInverseLookupControl 0x2C2800

#define cThaPDHPWPlaAsmSigInvLkIdCtrlMask        cBit8_0
#define cThaPDHPWPlaAsmSigInvLkIdCtrlShift       0
#define cThaPDHPWPlaAsmSigInvLkIdCtrlDwIndex     1

#define cThaPDHPWPlaAsmSigInvLkBitMapCtrlMask    cBit31_0
#define cThaPDHPWPlaAsmSigInvLkBitMapCtrlShift   0
#define cThaPDHPWPlaAsmSigInvLkBitMapCtrlDwIndex 0
#ifdef __cplusplus
}
#endif
#endif /* _THAPWREG_H_ */

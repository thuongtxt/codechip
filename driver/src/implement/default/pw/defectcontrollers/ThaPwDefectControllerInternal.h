/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Module PW
 * 
 * File        : ThaPwDefectControllerInternal.h
 * 
 * Created Date: Sep 3, 2014
 *
 * Description : Control PW defect
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPWDEFECTCONTROLLERINTERNAL_H_
#define _THAPWDEFECTCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/man/AtDeviceInternal.h"
#include "../../../../generic/pw/AtPwInternal.h"
#include "../../../../generic/pw/AtModulePwInternal.h"
#include "ThaPwDefectController.h"
#include "../adapters/ThaPwAdapter.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mAdapter(self) ThaPwDefectControllerAdapter((AtPw)self)

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPwDefectControllerMethods
    {
    eAtRet (*PwTypeSet)(ThaPwDefectController self, eAtPwType pwType);
    eAtRet (*AllThresholdsApply)(ThaPwDefectController self);
    eAtRet (*DefaultSet)(ThaPwDefectController self);
    uint32 (*PwDefaultOffset)(ThaPwDefectController self);

    /* Generic defects such as LOPS, LOPSTA, Lbit, Rbit, Overrun and Underrun
     * must be supported in all products.
     * Moreover, there are several extension defects such as Mbit, Stray,
     * Malformed, etc. that are optionally supported. Therefore, they should be
     * dynamically controlled for ease. */
    uint32 (*LopsMask)(ThaPwDefectController self);
    uint32 (*MissingMask)(ThaPwDefectController self);
    uint32 (*MbitMask)(ThaPwDefectController self);
    uint32 (*StrayMask)(ThaPwDefectController self);
    uint32 (*MalformedMask)(ThaPwDefectController self);

    eAtRet (*MissingPacketDefectThresholdSave)(ThaPwDefectController self, uint32 numPackets);
    uint32 (*MissingPacketDefectThresholdDbGet)(ThaPwDefectController self);
    }tThaPwDefectControllerMethods;

typedef struct tThaPwDefectController
    {
    tAtPw super;
    const tThaPwDefectControllerMethods *methods;

    ThaPwAdapter pwAdapter;
    }tThaPwDefectController;

/*--------------------------- Forward declarations ---------------------------*/
ThaPwDefectController ThaPwDefectControllerObjectInit(ThaPwDefectController self, AtPw pw);
ThaPwAdapter ThaPwDefectControllerAdapter(AtPw self);

/*--------------------------- Entries ----------------------------------------*/

#endif /* _THAPWDEFECTCONTROLLERINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Module PW
 *
 * File        : ThaPwDefectController.c
 *
 * Created Date: Sep 3, 2014
 *
 * Description : Control PW defect
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../man/ThaDeviceInternal.h"
#include "../../pda/ThaModulePda.h"
#include "../../cla/controllers/ThaClaPwController.h"
#include "../adapters/ThaPwAdapter.h"
#include "ThaPwDefectControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaPwDefectController)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaPwDefectControllerMethods m_methods;

/* Override */
static tAtObjectMethods  m_AtObjectOverride;
static tAtPwMethods      m_AtPwOverride;
static tAtChannelMethods m_AtChannelOverride;

/* Save super's implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
static const tAtPwMethods      *m_AtPwMethods      = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePda PdaModule(AtPw self)
    {
    return (ThaModulePda)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cThaModulePda);
    }

static eBool LopsIsSupported(ThaPwAdapter adapter, uint32 numPackets)
    {
    ThaModulePda pdaModule = PdaModule((AtPw)adapter);
    return ThaModulePdaPwLopsThresholdIsValid(pdaModule, (AtPw)adapter, numPackets);
    }

static eAtModulePwRet LopsSetThresholdSet(AtPw self, uint32 numPackets)
    {
    ThaPwAdapter adapter = mAdapter(self);

    if (!LopsIsSupported(adapter, numPackets))
        return cAtErrorModeNotSupport;

    if (ThaPwAdapterIsLogicPw(adapter))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(adapter);
        if (cache)
            cache->lopsSetThreshold = numPackets;
        return cAtOk;
        }

    if (ThaModulePdaNeedPreventReconfigure(PdaModule(self)) && (AtPwLopsSetThresholdGet(self) == numPackets))
        return cAtOk;

    if (numPackets == 0)
        return cAtErrorInvlParm;

    if (AtPwTypeGet(self) == cAtPwTypeCEP)
        return ThaModulePdaPwLopsSetThresholdSet(PdaModule(self), (AtPw)adapter, numPackets);

    return ThaModulePdaPwLofsSetThresholdSet(PdaModule(self), (AtPw)adapter, numPackets);
    }

static uint32 LopsSetThresholdGet(AtPw self)
    {
    ThaPwAdapter adapter = mAdapter(self);

    if (ThaPwAdapterIsLogicPw(adapter))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(adapter);
        if (cache)
            return cache->lopsSetThreshold;

        return cBit7_0;
        }

    if (AtPwTypeGet(self) == cAtPwTypeCEP)
        return ThaModulePdaPwLopsSetThresholdGet(PdaModule(self), (AtPw)adapter);

    return ThaModulePdaPwLofsSetThresholdGet(PdaModule(self), (AtPw)adapter);
    }

static eAtModulePwRet LopsClearThresholdSet(AtPw self, uint32 numPackets)
    {
    ThaPwAdapter adapter = mAdapter(self);

    if (!LopsIsSupported(adapter, numPackets))
        return cAtErrorModeNotSupport;

    if (ThaPwAdapterIsLogicPw(adapter))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(adapter);
        if (cache)
            cache->lopsClearThreshold = numPackets;
        return cAtOk;
        }

    /* LOPS clear threshold not change */
    if (AtPwLopsClearThresholdGet(self) == numPackets)
        return cAtOk;

    if (numPackets > 0)
        return ThaModulePdaPwLopsClearThresholdSet(PdaModule(self), (AtPw)adapter, numPackets);

    return cAtErrorInvlParm;
    }

static uint32 LopsClearThresholdGet(AtPw self)
    {
    ThaPwAdapter adapter = mAdapter(self);

    if (ThaPwAdapterIsLogicPw(adapter))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(adapter);
        return cache ? cache->lopsClearThreshold : 0;
        }

    return ThaModulePdaPwLopsClearThresholdGet(PdaModule(self), (AtPw)adapter);
    }

static eAtRet PwTypeSet(ThaPwDefectController self, eAtPwType pwType)
    {
    AtUnused(self);
    AtUnused(pwType);
    return cAtOk;
    }

static uint32 DefectGet(AtChannel self)
    {
    AtPw pw;
    ThaPwAdapter adapter = mAdapter((AtPw)self);

    if (ThaPwAdapterIsLogicPw(adapter))
        return 0;

    pw = (AtPw)adapter;
    return ThaClaPwControllerPwAlarmGet(ThaPwAdapterClaPwController(adapter), pw) |
           ThaModulePdaPwAlarmGet(PdaModule(pw), pw);
    }

static uint32 DefectHistoryReadToClear(AtChannel self, eBool read2Clear)
    {
    AtPw pw;
    ThaPwAdapter adapter = mAdapter((AtPw)self);

    if (ThaPwAdapterIsLogicPw(adapter))
        return 0;

    pw = (AtPw)adapter;
    return ThaClaPwControllerPwDefectHistoryGet(ThaPwAdapterClaPwController(adapter), pw, read2Clear) |
           ThaModulePdaPwDefectHistoryGet(PdaModule(pw), pw, read2Clear);
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    return DefectHistoryReadToClear(self, cAtFalse);
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    return DefectHistoryReadToClear(self, cAtTrue);
    }

static eAtRet AllThresholdsApply(ThaPwDefectController self)
    {
    AtPw pw;
    tThaPwConfigCache *adapterCache = ThaPwAdapterCache(mAdapter(self));
    if (adapterCache == NULL)
        return cAtErrorNullPointer;

    pw = (AtPw)self;
    if (adapterCache->lopsClearThreshold)
        AtPwLopsClearThresholdSet(pw, adapterCache->lopsClearThreshold);

    if (adapterCache->lopsSetThreshold)
        AtPwLopsSetThresholdSet(pw, adapterCache->lopsSetThreshold);

    return cAtOk;
    }

static eAtRet DefaultSet(ThaPwDefectController self)
    {
    AtPw pw = (AtPw)self;
    AtModulePw modulePw = (AtModulePw)AtChannelModuleGet((AtChannel)self);
    uint32 currentThreshold;

    if (AtPwTypeGet((AtPw)self) != cAtPwTypeCEP)
        return cAtOk;

    if (!AtPwMissingPacketDefectThresholdIsSupported(pw))
        return cAtOk;

    /* Missing packet threshold */
    currentThreshold = AtPwMissingPacketDefectThresholdGet(pw);
    if (currentThreshold == 0)
        currentThreshold = AtModulePwDefaultLopsSetThreshold(modulePw);

    return AtPwMissingPacketDefectThresholdSet(pw, currentThreshold);
    }

static const char *TypeString(AtChannel self)
    {
    AtUnused(self);
    return "pw_defect_controller";
    }

static eBool AlarmIsSupported(AtChannel self, uint32 alarmType)
    {
    /* Mbit just is supported with CESoP  */
    eBool isPwTypeCESoP = (AtPwTypeGet((AtPw)self) == cAtPwTypeCESoP) ? cAtTrue : cAtFalse;
    uint32 alarmMask = isPwTypeCESoP ? cAtPwAlarmTypeAll : (cAtPwAlarmTypeAll & (~cAtPwAlarmTypeMBit));
    return (alarmType & alarmMask) ? cAtTrue : cAtFalse;
    }

static uint32 PwDefaultOffset(ThaPwDefectController self)
    {
    return AtChannelIdGet((AtChannel)mAdapter(self));
    }

static uint32 LopsMask(ThaPwDefectController self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 MissingMask(ThaPwDefectController self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 MbitMask(ThaPwDefectController self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 StrayMask(ThaPwDefectController self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 MalformedMask(ThaPwDefectController self)
    {
    AtUnused(self);
    return 0;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaPwDefectController object = (ThaPwDefectController)self;
    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeChannelIdString(pwAdapter);
    }

static eBool MissingPacketDefectThresholdIsSupported(AtPw self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool MissingPacketDefectThresholdSupported(ThaPwDefectController self, ThaPwAdapter adapter, uint32 numPackets)
    {
    /* The number of bit is same (9 bits), so can use same PDA checking function */
    ThaModulePda pdaModule = PdaModule((AtPw)self);

    if (AtPwMissingPacketDefectThresholdIsSupported((AtPw)self) == cAtFalse)
        return cAtFalse;

    return ThaModulePdaPwLopsThresholdIsValid(pdaModule, (AtPw)adapter, numPackets);
    }

static eAtRet MissingPacketDefectThresholdSave(ThaPwDefectController self, uint32 numPackets)
    {
    ThaPwAdapter adapter = mAdapter(self);
    tThaPwConfigCache *cache = ThaPwAdapterCache(adapter);
    if (cache)
        cache->lopsClearThreshold = numPackets;
    return cAtOk;
    }

static eAtModulePwRet MissingPacketDefectThresholdSet(AtPw self, uint32 numPackets)
    {
    ThaPwAdapter adapter = mAdapter(self);

    if (AtPwTypeGet(self) != cAtPwTypeCEP)
        return cAtErrorModeNotSupport;

    if (!MissingPacketDefectThresholdSupported(mThis(self), adapter, numPackets))
        return cAtErrorModeNotSupport;

    if (ThaPwAdapterIsLogicPw(adapter))
        return mMethodsGet(mThis(self))->MissingPacketDefectThresholdSave(mThis(self), numPackets);

    if (AtPwMissingPacketDefectThresholdGet(self) == numPackets)
        return cAtOk;

    /* CEP use field Lost of frame state for this purpose */
    return ThaModulePdaPwLofsSetThresholdSet(PdaModule(self), (AtPw)adapter, numPackets);
    }

static uint32 MissingPacketDefectThresholdDbGet(ThaPwDefectController self)
    {
    ThaPwAdapter adapter = mAdapter(self);
    tThaPwConfigCache *cache = ThaPwAdapterCache(adapter);
    return cache ? cache->lopsClearThreshold : 0;
    }

static uint32 MissingPacketDefectThresholdGet(AtPw self)
    {
    ThaPwAdapter adapter = mAdapter(self);
    if (AtPwTypeGet(self) != cAtPwTypeCEP)
        return 0;

    if (AtPwMissingPacketDefectThresholdIsSupported((AtPw)self) == cAtFalse)
        return 0;

    if (ThaPwAdapterIsLogicPw(adapter))
        return mMethodsGet(mThis(self))->MissingPacketDefectThresholdDbGet(mThis(self));

    /* CEP use field Lost of frame state for this purpose */
    return ThaModulePdaPwLofsSetThresholdGet(PdaModule(self), (AtPw)adapter);
    }

static void CurrentStatusReset(ThaPwDefectController self)
    {
    uint32 address = ThaModulePwDefectCurrentStatusRegister((ThaModulePw)AtChannelModuleGet((AtChannel)self)) +
                     ThaPwDefectControllerPwDefaultOffset(self);
    mChannelHwWrite(self, address, 0, cAtModulePw);
    }

static void OverrideAtObject(ThaPwDefectController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtPw(ThaPwDefectController self)
    {
    AtPw pw = (AtPw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPwMethods = mMethodsGet(pw);

        mMethodsGet(osal)->MemCpy(osal, &m_AtPwOverride, m_AtPwMethods, sizeof(m_AtPwOverride));
        mMethodOverride(m_AtPwOverride, LopsSetThresholdSet);
        mMethodOverride(m_AtPwOverride, LopsSetThresholdGet);
        mMethodOverride(m_AtPwOverride, LopsClearThresholdSet);
        mMethodOverride(m_AtPwOverride, LopsClearThresholdGet);
        mMethodOverride(m_AtPwOverride, MissingPacketDefectThresholdIsSupported);
        mMethodOverride(m_AtPwOverride, MissingPacketDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, MissingPacketDefectThresholdGet);
        }

    mMethodsSet(pw, &m_AtPwOverride);
    }

static void OverrideAtChannel(ThaPwDefectController self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, TypeString);
        mMethodOverride(m_AtChannelOverride, AlarmIsSupported);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void MethodsInit(ThaPwDefectController self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, PwTypeSet);
        mMethodOverride(m_methods, AllThresholdsApply);
        mMethodOverride(m_methods, DefaultSet);
        mMethodOverride(m_methods, PwDefaultOffset);
        mMethodOverride(m_methods, LopsMask);
        mMethodOverride(m_methods, MissingMask);
        mMethodOverride(m_methods, MbitMask);
        mMethodOverride(m_methods, StrayMask);
        mMethodOverride(m_methods, MalformedMask);
        mMethodOverride(m_methods, MissingPacketDefectThresholdSave);
        mMethodOverride(m_methods, MissingPacketDefectThresholdDbGet);
        }

    mMethodsSet(self, &m_methods);
    }

static void Override(ThaPwDefectController self)
    {
    OverrideAtObject(self);
    OverrideAtPw(self);
    OverrideAtChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPwDefectController);
    }

ThaPwDefectController ThaPwDefectControllerObjectInit(ThaPwDefectController self, AtPw pw)
    {
    AtChannel pwChannel = (AtChannel)pw;

    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPwObjectInit((AtPw)self,
                       (uint16)AtChannelIdGet(pwChannel),
                       (AtModulePw)AtChannelModuleGet(pwChannel),
                       AtPwTypeGet(pw)) == NULL)
        return NULL;

    /* Override */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    self->pwAdapter = (ThaPwAdapter)pw;

    return self;
    }

ThaPwDefectController ThaPwDefectControllerNew(AtPw pw)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwDefectController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ThaPwDefectControllerObjectInit(newController, pw);
    }

eAtRet ThaPwDefectControllerPwTypeSet(ThaPwDefectController self, eAtPwType pwType)
    {
    if (self)
        return mMethodsGet(self)->PwTypeSet(self, pwType);
    return cAtError;
    }

eAtRet ThaPwDefectControllerAllThresholdsApply(ThaPwDefectController self)
    {
    if (self)
        return mMethodsGet(self)->AllThresholdsApply(self);
    return cAtError;
    }

ThaPwAdapter ThaPwDefectControllerAdapter(AtPw self)
    {
    ThaPwDefectController controller = (ThaPwDefectController)self;
    return controller->pwAdapter;
    }

eAtRet ThaPwDefectControllerDefaultSet(ThaPwDefectController self)
    {
    if (self)
        return mMethodsGet(self)->DefaultSet(self);

    return cAtError;
    }

uint32 ThaPwDefectControllerPwDefaultOffset(ThaPwDefectController self)
    {
    if (self)
        return mMethodsGet(self)->PwDefaultOffset(self);
    return cBit31_0;
    }

void ThaPwDefectControllerCurrentStatusReset(ThaPwDefectController self)
    {
    if (self)
        CurrentStatusReset(self);
    }

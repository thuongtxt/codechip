/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : ThaPwDefectController.h
 * 
 * Created Date: Sep 3, 2014
 *
 * Description : Defect controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPWDEFECTCONTROLLER_H_
#define _THAPWDEFECTCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../ThaModulePw.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Default concretes */
ThaPwDefectController ThaPwDefectControllerNew(AtPw pw);

eAtRet ThaPwDefectControllerPwTypeSet(ThaPwDefectController self, eAtPwType pwType);
eAtRet ThaPwDefectControllerAllThresholdsApply(ThaPwDefectController defectController);
eAtRet ThaPwDefectControllerDefaultSet(ThaPwDefectController self);
uint32 ThaPwDefectControllerPwDefaultOffset(ThaPwDefectController self);
void ThaPwDefectControllerCurrentStatusReset(ThaPwDefectController self);

/* Product concretes */
ThaPwDefectController Tha60150011PwDefectControllerNew(AtPw pw);

#endif /* _THAPWDEFECTCONTROLLER_H_ */


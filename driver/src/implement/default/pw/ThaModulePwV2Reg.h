/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDA
 * 
 * File        : ThaModulePdaV1Reg.h
 * 
 * Created Date: May 13, 2013
 *
 * Description : PDA registers
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEPWV2REG_H_
#define _THAMODULEPWV2REG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*------------------------------------------------------------------------------
Reg Name: Pseudowire PDA Jitter Buffer Control
Reg Addr: 0x243100 - 0x24317F
          The address format for these registers is 0x243100 + PwId
          Where: PwId(0 � 127) Pseudowire Identification
Description: This register sets jitter buffer parameters of AF6FH1 pseudowires.
------------------------------------------------------------------------------*/
#define cThaRegPWPdaJitBufCtrl                   0x243000

/*--------------------------------------
BitField Name: PwPayloadLen
BitField Type: R/W
BitField Desc: Payload length iof receive pseudowire packet
--------------------------------------*/
#define cThaRegPWPdaJitBufCtrlPldLenMask         cBit15_2 /* cBit34_47 */
#define cThaRegPWPdaJitBufCtrlPldLenShift        2
#define cThaRegPWPdaJitBufCtrlPldLenDwIndex      1

/*--------------------------------------
BitField Name: PwSetLofs
BitField Type: R/W
BitField Desc: Number of consecutive lost packets to enter lost of frame state
--------------------------------------*/
#define cThaRegPWPdaJitBufCtrlSetLofsHeadMask         cBit1_0 /* cBit33_32 */
#define cThaRegPWPdaJitBufCtrlSetLofsHeadShift        0
#define cThaRegPWPdaJitBufCtrlSetLofsTailMask         cBit31_29
#define cThaRegPWPdaJitBufCtrlSetLofsTailShift        29

/*--------------------------------------
BitField Name: PwClearLofs
BitField Type: R/W
BitField Desc: Number of consecutive good packets to exit lost of frame state
--------------------------------------*/
#define cThaRegPWPdaJitBufCtrlClearLofsMask         cBit28_24
#define cThaRegPWPdaJitBufCtrlClearLofsShift        24

/*--------------------------------------
BitField Name: PdvSizePk
BitField Type: R/W
BitField Desc:
--------------------------------------*/
#define cThaRegPWPdaJitBufCtrlPdvSizePkMask         cBit23_12
#define cThaRegPWPdaJitBufCtrlPdvSizePkShift        12
#define cThaRegPWPdaJitBufCtrlPdvSizePkDwIndex      0

/*--------------------------------------
BitField Name: JitBufSizePk
BitField Type: R/W
BitField Desc:
--------------------------------------*/
#define cThaRegPWPdaJitBufCtrlJitBufSizePkMask      cBit11_0
#define cThaRegPWPdaJitBufCtrlJitBufSizePkShift     0
#define cThaRegPWPdaJitBufCtrlJitBufSizePkDwIndex   0

/*------------------------------------------------------------------------------
Reg Name: Pseudowire PDA Mode Control
Reg Addr: 0x245000 - 0x24507F
The address format for these registers is 0x245000 + PwId
Where: PwId(0 � 127) Pseudowire Identification
Description:  The register configures modes of TDM Payload De-Assembler function
------------------------------------------------------------------------------*/
#define cThaRegPWPdaMdCtrl              0x245000

#define cThaRegPWPdaMdCtrlIdleCodeMask   cBit25_18
#define cThaRegPWPdaMdCtrlIdleCodeShift  18

/*--------------------------------------
BitField Name: PDARDIOff
BitField Type: R/W
BitField Desc:
--------------------------------------*/
#define cThaRegPWPdaRDIOffMask           cBit17
#define cThaRegPWPdaRDIOffShift          17

/*--------------------------------------
BitField Name: PDARepMode
BitField Type: R/W
BitField Desc:
--------------------------------------*/
#define cThaRegPWPdaMdCtrlRepMdMask      cBit16_15
#define cThaRegPWPdaMdCtrlRepMdShift     15

/*--------------------------------------
BitField Name: PDANxDS0
BitField Type: R/W
BitField Desc:
--------------------------------------*/
#define cThaRegPWPdaMdCtrlNxDs0Mask      cBit14_5
#define cThaRegPWPdaMdCtrlNxDs0Shift     5

/* In CEP mode */
#define cThaRegPWPdaFirstStsIdMask  cBit14_5
#define cThaRegPWPdaFirstStsIdShift 5

/*--------------------------------------
BitField Name: PDAMode
BitField Type: R/W
BitField Desc:
--------------------------------------*/
#define cThaRegPWPdaMdCtrlMdMask      cBit4_1
#define cThaRegPWPdaMdCtrlMdShift     1

/*--------------------------------------
BitField Name: PDAE1orT1
BitField Type: R/W
BitField Desc:
--------------------------------------*/
#define cThaRegPWPdaMdCtrlT1E1Mask      cBit0
#define cThaRegPWPdaMdCtrlT1E1Shift     0

/* In CEP product */
#define cThaRegPWPdaMdEbmEnMask      cBit0
#define cThaRegPWPdaMdEbmEnShift     0

/*------------------------------------------------------------------------------
Reg Name: Pseudowire PDA Reorder Control
Reg Addr: 0x244000-0x24407F
The address format for these registers is 0x244000 + PwId
Where: PwId(0 � 127) Pseudowire Identification
Description:  This register sets reorder parameters of AF6FH1 pseudowires.
------------------------------------------------------------------------------*/
#define cThaRegPWPdaReorderCtrl               0x244000

/*--------------------------------------
BitField Name: PwReorEn
BitField Type: R/W
BitField Desc:
--------------------------------------*/
#define cThaRegPWPdaReorderCtrlReorEnMask      cBit11
#define cThaRegPWPdaReorderCtrlReorEnShift     11

/*--------------------------------------
BitField Name: PwReorTimeOut
BitField Type: R/W
BitField Desc:
--------------------------------------*/
#define cThaRegPWPdaReorderCtrlReorTimeoutMask      cBit10_0
#define cThaRegPWPdaReorderCtrlReorTimeoutShift     0

/*==============================================================================
Reg Name: PDA TDM Jitter Buffer States
Reg Addr: 0x243200-0x24327F
Format  : 0x243200 + pwid, where pwid=[0,127]
Reg Desc: This register stores PDA TDM jitter buffer states
===============================================================================*/
#define  cThaRegPDATdmJitBufStat    0x243400

#define  cThaRxBufNumAdditionalBytesMask   cBit23_16
#define  cThaRxBufNumAdditionalBytesShift  16

#define  cThaRxBufNumPktMask               cBit15_4
#define  cThaRxBufNumPktShift              4

#define  cThaRxJitBufFullMask              cBit3
#define  cThaRxJitBufFullShift             3

#define  cThaJitBufStateMask               cBit2_0
#define  cThaJitBufStateShift              0

/*------------------------------------------------------------------------------
Reg Name: Pseudowire Transmit Ethernet Header Length Control
Reg Addr: 0x301000-0x30107F
The address format for these registers is 0x301000 + PwId
Where: PwId(0 � 127) Pseudowire Identification
Description: This register configures length in number of bytes of AF6F1
             transmit Ethernet header.
------------------------------------------------------------------------------*/
#define cThaRegPwTxEthHdrLengthCtrl                     0x301000

/*--------------------------------------
BitField Name: TxEthPwStmLineId
BitField Type: R/W
BitField Desc: Used for TDM PW, this is the corresponding STM lineid of PW
--------------------------------------*/
#define cThaRegPwTxEthPwStmLineIdMask                  cBit26_24
#define cThaRegPwTxEthPwStmLineIdShift                 24

#define cThaRegPwTxForceControlWordErrorMask           cBit23
#define cThaRegPwTxForceControlWordErrorShift          23
/*--------------------------------------
BitField Name: TxEthPwRtpPtValue
BitField Type: R/W
BitField Desc: Used for TDM PW, this is the PT value of RTP header.
--------------------------------------*/
#define cThaRegPwTxEthPwRtpPtValueMask                  cBit22_16
#define cThaRegPwTxEthPwRtpPtValueShift                 16

/*--------------------------------------
BitField Name: TxEthPwRtpEn
BitField Type: R/W
BitField Desc: 1: Enable RTP field in PSN header (used for TDM PW with
                  DCR timing)
               0: Disable RTP field in PSN header (used for ATM PW or
                  TDM PW without DCR timing)
--------------------------------------*/
#define cThaRegPwTxEthPwRtpEnMask                       cBit15
#define cThaRegPwTxEthPwRtpEnShift                      15

/*--------------------------------------
BitField Name: TxEthPwPsnType
BitField Type: R/W
BitField Desc: Control word length
               - 1: PW PSN header is UDP/IPv4
               - 2: PW PSN header is UDP/IPv6
               - Others: for other PW PSN header type
--------------------------------------*/
#define cThaRegPwTxEthHdrLengthCtrlTxEthPwPsnTypeMask      cBit14_12
#define cThaRegPwTxEthHdrLengthCtrlTxEthPwPsnTypeShift     12

/*--------------------------------------
BitField Name: TxEthPwCwType
BitField Type: R/W
BitField Desc: Control word length
               - 0: Control word 4-byte (used for PDH PW or PW ATM N to 1)
               - 1: Control word 3-byte (used for PW ATM 1 to 1)
--------------------------------------*/
#define cThaRegPwTxEthHdrLengthCtrlTxEthPwCwTypeMask      cBit11
#define cThaRegPwTxEthHdrLengthCtrlTxEthPwCwTypeShift     11

/*--------------------------------------
BitField Name: TxEthPwNumVlan
BitField Type: R/W
BitField Desc:
--------------------------------------*/
#define cThaRegPwTxEthHdrLengthCtrlTxNumVlanMask      cBit10_9
#define cThaRegPwTxEthHdrLengthCtrlTxNumVlanShift     9

/*--------------------------------------
BitField Name: TxEthPwNumMplsOutLb
BitField Type: R/W
BitField Desc:
--------------------------------------*/
#define cThaRegPwTxEthHdrLengthCtrlTxNumMplsOutLbMask      cBit8_7
#define cThaRegPwTxEthHdrLengthCtrlTxNumMplsOutLbShift     7

/*--------------------------------------
BitField Name: TxEthPwHeadLen
BitField Type: R/W
BitField Desc:
--------------------------------------*/
#define cThaRegPwTxEthHdrLengthCtrlTxHdrLenMask      cBit6_0
#define cThaRegPwTxEthHdrLengthCtrlTxHdrLenShift     0

/*------------------------------------------------------------------------------
Reg Name: Pseudowire Transmit Header RTP SSRC Value Control
Reg Addr: 0x303000-0x30307F
Format  : 0x303000 + pwid, where pwid=[0,127]
Reg Desc: This register configures transmitted RTP SSRC value.
------------------------------------------------------------------------------*/
#define cThaRegPwTxRtpSsrcValue             0x303000

/*------------------------------------------------------------------------------
Reg Name: Pseudowire Transmit Ethernet Header Value Control
Reg Addr: 0x300000-0x300FFF
The address format for these registers is 0x300000 + PwId*16 + Entry
Where: PwId(0 � 127) Pseudowire Identification
    Entry(0 � 15) Entries to get header values.
Description: This register configures value of AF6F1 transmit Ethernet
             pseudowire header. Each pseudowire has 16 entries (1 entry contains
             32 bits) for header value configuration
------------------------------------------------------------------------------*/
#define cThaRegPwTxEthHdrValCtrl    0x300000

/*------------------------------------------------------------------------------
Reg Name: Pseudowire Transmit Good Packet Counter
Address : 0x511800 � 0x51187F(R_O), 0x511000 � 0x51107F(R2C)
The address format for these registers is 0x510100 + PwId (R_O), 0x511100 + PwId ((r2c))
Where: PwId (0 � 127) Pseudowire IDs
Description: Count number of CESoETH packets transmitted to Ethernet side.
------------------------------------------------------------------------------*/
#define cThaRegPmcPwTxPktCnt(ro)  (0x511000UL + (ro) * 0x800UL)

/*------------------------------------------------------------------------------
Reg Name: Pseudowire Receive Payload Octet Counter
Address : 0x512800 � 0x51287F(R_O), 0x512000 � 0x51207F(R2C),
The address format for these registers is 0x510200 + PwId (R_O), 0x511200 + PwId (R2C)
Where: PwId (0 � 127) Pseudowire IDs
Description: Count number of pseudowire payload octet received
------------------------------------------------------------------------------*/
#define cThaRegPmcPwRxPldOctCnt(ro)  (0x512000UL + (ro) * 0x800UL)

/*------------------------------------------------------------------------------
Reg Name: Pseudowire Receive Good Packet Counter
Address:  0x513800 � 0x51387F(R_O), 0x513000 � 0x51307F(R2C),
The address format for these registers is 0x510300 + PwId (R_O), 0x511300 + PwId (R2C)
Where: PwId (0 � 127) Pseudowire IDs
Description: Count number of CESoETH packets received
------------------------------------------------------------------------------*/
#define cThaRegPmcPwRxPktCnt(ro)  (0x513000UL + (ro) * 0x800UL)

/*------------------------------------------------------------------------------
Reg Name: Pseudowire Receive Reorder Drop Packet Counter
Address:  0x514800 � 0x51487F(R_O), 0x514000 � 0x51407F(R2C),
The address format for these registers is 0x510400 + PwId (R_O), 0x511400 + PwId (R2C)
Where: PwId (0 � 127) Pseudowire IDs
Description: Count number of dropped packet (too late or too soon) by reorder function
------------------------------------------------------------------------------*/
#define cThaRegPmcPwRxReorderDropPktCnt(ro)  (0x514000UL + (ro) * 0x800UL)

/*------------------------------------------------------------------------------
Reg Name: Pseudowire Receive Reorder Out Of Sequence  Packet Counter
Address: 0x515800 � 0x51587F(R_O), 0x515000 � 0x51507F(R2C),
The address format for these registers is 0x510500 + PwId (R_O), 0x511500 + PwId (R2C)
Where: PwId (0 � 127) Pseudowire IDs
Description: Count number of frames received that are out-of-sequence, but successfully re-ordered
------------------------------------------------------------------------------*/
#define cThaRegPmcPwRxReorderedPktCnt(ro)  (0x515000UL + (ro) * 0x800UL)

/*------------------------------------------------------------------------------
Reg Name: Pseudowire Receive LOFS Transition Counter
Address:  0x516800 � 0x51687F(R_O), 0x516000 � 0x51607F(R2C),
The address format for these registers is 0x510600 + PwId (R_O), 0x511600 + PwId (R2C)
Where: PwId (0 � 127) Pseudowire IDs
Description: Count number of transitions from the normal to the loss of frames state (LOFS)
------------------------------------------------------------------------------*/
#define cThaRegPmcPwRxLofsTransCnt(ro)  (0x516000UL + (ro) * 0x800UL)

/*------------------------------------------------------------------------------
Reg Name: Pseudowire Receive Jitter Buffer Overrun Packet Counter
Address: 0x517800 � 0x51787F(R_O), 0x517000 � 0x51707F(R2C),
The address format for these registers is 0x510700 + PwId (R_O), 0x511700 + PwId (R2C)
Where: PwId (0 � 127) Pseudowire IDs
Description: Count number of arrived packets when jitter buffer overrun
------------------------------------------------------------------------------*/
#define cThaRegPmcPwRxJitBufOverrunPktCnt(ro)  (0x517000UL + (ro) * 0x800UL)

/*------------------------------------------------------------------------------
Reg Name: Pseudowire Jitter Buffer Underrrun Event Counter
Address: 0x51F800 � 0x51F87F(R_O), 0x51F000 � 0x51F07F(R2C),
The address format for these registers is 0x51F800 + PwId (R_O), 0x51F000 + PwId (R2C)
Where: PwId (0 � 127) Pseudowire IDs
Description: Count number of jitter buffer transition events from normal to underrun status
------------------------------------------------------------------------------*/
#define cThaRegPmcPwJitBufUnderrunEvtCnt(ro)  (0x51F000UL + (ro) * 0x800UL)

/*------------------------------------------------------------------------------
Reg Name: Pseudowire Receive Reorder Lost Packet Counter
Address: 0x518800 � 0x51887F(R_O), 0x518000 � 0x51807F(R2C),
The address format for these registers is 0x510800 + PwId (R_O), 0x511800 + PwId (R2C)
Where: PwId (0 � 127) Pseudowire IDs
Description: Count number of lost packets detected
------------------------------------------------------------------------------*/
#define cThaRegPmcPwRxReorderLostPktCnt(ro)  (0x518000UL + (ro) * 0x800UL)

/*------------------------------------------------------------------------------
Reg Name: Pseudowire Receive Malform Packet Counter
Address: 0x519800 � 0x51987F(R_O), 0x519000 � 0x51907F(R2C),
The address format for these registers is 0x510900 + PwId (R_O), 0x511900 + PwId (R2C)
Where: PwId (0 � 127) Pseudowire IDs
Description: Count number of malformed packets received
------------------------------------------------------------------------------*/
#define cThaRegPmcPwRxMalformPktCnt(ro)  (0x519000UL + (ro) * 0x800UL)

/*------------------------------------------------------------------------------
Reg Name: Pseudowire Receive Stray Packet Counter
Address: 0x510800 � 0x51087F(R_O), 0x510000 � 0x51007F(R2C)
The address format for these registers is 0x510800 + PwId (R_O), 0x510000 + PwId (R2C)
Where: PwId (0 � 127) Pseudowire IDs
Description: Count number of stray packets received
------------------------------------------------------------------------------*/
#define cThaRegPmcPwRxStrayPktCnt(ro)  (0x510000UL + (ro) * 0x800UL)

/*------------------------------------------------------------------------------
Reg Name: Pseudowire Transmit L Bit Packet Counter
Address:  0x51A800 � 0x51A87F(R_O), 0x51A000 � 0x51A07F(R2C),
The address format for these registers is 0x510A00 + PwId (R_O), 0x511A00 + PwId (R2C)
Where: PwId (0 � 127) Pseudowire IDs
Description: Count number of good pseudowire packets transmitted with L bit set
------------------------------------------------------------------------------*/
#define cThaRegPmcPwTxLbitPktCnt(ro)  (0x51A000UL + (ro) * 0x800UL)

/*------------------------------------------------------------------------------
Reg Name: Pseudowire Receive L Bit Packet Counter
Address: 0x51B800 � 0x51B87F(R_O), 0x51B000 � 0x51B07F(R2C),
The address format for these registers is 0x510B00 + PwId (R_O), 0x511B00 + PwId (R2C)
Where: PwId (0 � 127) Pseudowire IDs
Description: Count number of good pseudowire packets received with L bit set
------------------------------------------------------------------------------*/
#define cThaRegPmcPwRxLbitPktCnt(ro)  (0x51B000UL + (ro) * 0x800UL)

/*------------------------------------------------------------------------------
Reg Name: Pseudowire Transmit R Bit Packet Counter
Address: 0x51C800 � 0x51C87F(R_O), 0x515000 � 0x51507F(R2C),
The address format for these registers is 0x510500 + PwId (R_O), 0x511500 + PwId (R2C)
Where: PwId (0 � 127) Pseudowire IDs
Description: Count number of good pseudowire packets transmitted with R bit set
------------------------------------------------------------------------------*/
#define cThaRegPmcPwTxRbitPktCnt(ro)  (0x51C000UL + (ro) * 0x800UL)

/*------------------------------------------------------------------------------
Reg Name: Pseudowire Receive R Bit Packet Counter
Address: 0x51D800 � 0x51D87F(R_O), 0x51D000 � 0x51D07F(R2C),
The address format for these registers is 0x510D00 + PwId (R_O), 0x511D00 + PwId (R2C)
Where: PwId (0 � 127) Pseudowire IDs
Description: Count number of good pseudowire packets received with R bit set
------------------------------------------------------------------------------*/
#define cThaRegPmcPwRxRbitPktCnt(ro)  (0x51D000UL + (ro) * 0x800UL)

/*------------------------------------------------------------------------------
Reg Name: Pseudowire Transmit Payload Octet Counter
Address: 0x51E800 � 0x51E87F(R_O), 0x51E000 � 0x51E07F(R2C),
The address format for these registers is 0x510E00 + PwId (R_O), 0x511E00 + PwId (R2C)
Where: PwId (0 � 127) Pseudowire IDs
Description: Count number of pseudowire payload octet transmitted
------------------------------------------------------------------------------*/
#define cThaRegPmcPwTxPldOctCnt(ro)  (0x51E000UL + (ro) * 0x800UL)

/*------------------------------------------------------------------------------
Reg Name: Pseudowire Receive N Bit Packet Counter
Address:  0x51A800 � 0x51A87F(R_O), 0x51A000 � 0x51A07F(R2C),
The address format for these registers is 0x51A800 + PwId (R_O), 0x51A000 + PwId (R2C)
Where: PwId (0 � 127) Pseudowire IDs
Description: Count number of received pseudowire Nbit packets
------------------------------------------------------------------------------*/
#define cThaRegPmcPwRxNbitPktCnt(ro)  (0x51A000UL + (ro) * 0x800UL)

/*------------------------------------------------------------------------------
Reg Name: Pseudowire Receive P Bit Packet Counter
Address: 0x51C800 � 0x51C87F(R_O), 0x51C000 � 0x51C07F(R2C),
The address format for these registers is 0x51C800 + PwId (R_O), 0x51C000 + PwId (R2C)
Where: PwId (0 � 127) Pseudowire IDs
Description: Count number of received pseudowire Pbit packets
------------------------------------------------------------------------------*/
#define cThaRegPmcPwRxPbitPktCnt(ro)  (0x51C000UL + (ro) * 0x800UL)

/*------------------------------------------------------------------------------
Reg Name: Pseudowire Receive Mbit Counter
Address: 0x521800 – 0x5219FF(R_O), 0x521000 – 0x5213FF(R2C),
The address format for these registers is 0x521800 + PwId (R_O), 0x521000 + PwId (R2C)
Where: PwId (0 – 1023) Pseudowire IDs
Description: Count number of pseudowire Mbit packet transmitted
------------------------------------------------------------------------------*/
#define cThaRegPmcPwRxMbitCnt(ro)  (0x521000UL + (ro) * 0x800UL)

/*------------------------------------------------------------------------------
Reg Name: Pseudowire Transmit Mbit Counter
Address: 0x520800 – 0x5209FF(R_O), 0x520000 – 0x5203FF(R2C),
The address format for these registers is 0x520800 + PwId (R_O), 0x520000 + PwId (R2C)
Where: PwId (0 – 1023) Pseudowire IDs
Description: Count number of pseudowire Mbit packet transmitted
------------------------------------------------------------------------------*/
#define cThaRegPmcPwTxMbitCnt(ro)  (0x520000UL + (ro) * 0x800UL)

/*------------------------------------------------------------------------------
Reg Name: PwPda Idle Code Control
Reg Addr: 0x24AC00
Reg Desc: This register configures idle code for TDM packet replacement
------------------------------------------------------------------------------*/
#define cThaRegPwPdaIdleCodeCtrl 0x245C00

/*------------------------------------------------------------------------------
Reg Name: Counter Per Alarm Current Status
Reg Addr: 0x501200 – 0x50127F
Reg Desc: This is the per Alarm current status of pseudowires. Each register is
used to store 4 current status of 4 alarms in pseudowires
------------------------------------------------------------------------------*/
#define cThaRegPmcPwAlarmCurrentStat                     0x501200

#define cThaPmcPwRbitCurStatMask                         cBit5
#define cThaPmcPwRbitCurStatShift                        5

#define cThaPmcPwUnderrunCurStatMask                     cBit3
#define cThaPmcPwOverrunCurStatMask                      cBit2
#define cThaPmcPwLofsCurStatMask                         cBit1
#define cThaPmcPwLbitCurStatMask                         cBit0

/*------------------------------------------------------------------------------
Reg Name: Counter Per Alarm Interrupt Status
Reg Addr: 0x501100 – 0x50117F
Reg Desc: This is the per Alarm interrupt status of pseudowires. Each register is
used to store 5 sticky bits for 5 alarms in pseudowires
------------------------------------------------------------------------------*/
#define cThaRegPmcPwAlarmIntrStat                         0x501100

/*------------------------------------------------------------------------------
Reg Name: Thalassa PDHPW Removing Protocol
Reg Addr:
Reg Desc:
------------------------------------------------------------------------------*/
#define cThaRegPDHPWRemovingProtocol                      0x324000

#define cThaRemovePwIdMask                                cBit31_2
#define cThaRemovePwIdShift                               2

#define cThaRemovePwStateMask                             cBit1_0
#define cThaRemovePwStateShift                            0

#define cThaRemovePwStateStop                             0
#define cThaRemovePwStateStart                            1
#define cThaRemovePwStateReady                            2

/*------------------------------------------------------------------------------
Reg Name: Thalassa PDA CEP De-Assembler EBM Mode Control
Reg Addr: 0x2461FF
Reg Desc: This is the configuration for EBM mode at the Payload De-Assemble from Packet side
------------------------------------------------------------------------------*/
#define cThaRegPDACEPDeAssemblerEBMModeControl 0x2461FF

#define cThaRegPDAEBMMdMask(stsId)  (cBit0 << (stsId))
#define cThaRegPDAEBMMdShift(stsId) (stsId)

/*------------------------------------------------------------------------------
Reg Name: Thalassa PDA EPAR Line ID Lookup Control
------------------------------------------------------------------------------*/
#define cThaRegPDAEPARLineIDLookupControl 0x249000

/*------------------------------------------------------------------------------
Reg Name: Thalassa PDA CEP De-Assembler Control
Reg Addr: 0x246000 - 0x24600B (0x246000 + stsId)
Reg Desc: The CEP De-Asemble Control is use to configure for per STS1/VC3 CEP
          operation.
------------------------------------------------------------------------------*/
#define cThaRegPDACEPDeAssemblerControl 0x246000

#define cThaRegPDASTSVCCcatEnMask     cBit18
#define cThaRegPDASTSVCCcatEnShift    18
#define cThaRegPDASTSVCCcatSlave      1
#define cThaRegPDASTSVCCcatMaster     0

#define cThaRegPDASTSVCCcatMstIDMask  cBit17_14
#define cThaRegPDASTSVCCcatMstIDShift 14

#define cThaRegPDAVTTypeMask(vtgId)  (cBit1_0 << (2 * (vtgId)))
#define cThaRegPDAVTTypeShift(vtgId) (2 * (vtgId))

/*------------------------------------------------------------------------------
Reg Name: Thalassa PDA CEP De-Assembler VC4 fractional VC3 Control
Reg Addr: 0x002461FC
Reg Desc: The CEP De-Asemble VC4 fractional VC3 Control is use to configure for per
          STS1/VC3 Concatenation operation.
------------------------------------------------------------------------------*/
#define cThaRegPDACepDeAssemblerVc4FractionalVc3Control 0x2461FC

#define cThaRegPDACepVC4FractionalVC3Mask(hwStsId) (cBit0 << (hwStsId))
#define cThaRegPDACepVC4FractionalVC3Shift(hwStsId) hwStsId

/*------------------------------------------------------------------------------
Reg Name: Thalassa PDA CEP De-Assembler EBM Bit Store/Configuration
Reg Name: 0x2460C0 � 0x2460CB (for 12 STS1/VC3)
The address format for these registers is 0x002460C0 + stsid
Where: stsid: 0 - 11
Reg Desc: These registers are used to store the EBM bits received from packet side
 in automatic EBM mode. In SW controlled mode, these registers are used for SW to
 write the EBM value to generate Equipped VT/TU in STS/VC factional CEP carrying VT/TU
 ------------------------------------------------------------------------------*/
#define cThaRegPDACepDeAssemblerEbmConfig 0x2460C0

#define cThaRegPDACepDeAssemblerEbmBitMask cBit27_0
#define cThaRegPDACepDeAssemblerEbmBitShift 0

#define cThaRegPDACepDeAssemblerTBitMask cBit28
#define cThaRegPDACepDeAssemblerTBitShift 28

/*------------------------------------------------------------------------------
Reg Name: Thalassa PDA TOH Pseudowire Byte Mapping Control
Reg Addr: 0x24C000-0x24C00B
          For OC-3
          The address format for these registers is 0x24C000 + stsid*4 +
          lineid
          Where: stsid (0 - 2) sts identificaition per OC3
          lineid (0 - 3) line identificaition
          For OC-12
          The address format for these registers is 0x24C000 + stsid
          Where: stsid (0 - 11) sts identificaition
Reg Desc: These registers are used to indicate which byte of TOH transported
          through PW
------------------------------------------------------------------------------*/
#define cThaRegThalassaPDATohPseudowireByteMappingCtrl 0x24C000

/*--------------------------------------
BitField Name: PdaTohPwFirstByteEnCtrl
BitField Type: R/W
BitField Desc: the first TOH byte in the STM-1 frame is present
               - 1: enable
               - 0: disable
BitField Bits: 33
--------------------------------------*/
#define cThaPdaTohPwFirstByteEnCtrlMask                cBit1
#define cThaPdaTohPwFirstByteEnCtrlShift               1
#define cThaPdaTohPwFirstByteEnCtrlDwIndex             1

/*--------------------------------------
BitField Name: PdaTohPwFirstByteRowCtrl
BitField Type: R/W
BitField Desc: row of the first TOH byte in the STM-1 frame is transported
               through PW
BitField Bits: 32_29
--------------------------------------*/
#define cThaPdaTohPwFirstByteRowCtrlHeadMask         cBit0
#define cThaPdaTohPwFirstByteRowCtrlHeadShift        0
#define cThaPdaTohPwFirstByteRowCtrlHeadDwIndex      1

#define cThaPdaTohPwFirstByteRowCtrlTailMask         cBit31_29
#define cThaPdaTohPwFirstByteRowCtrlTailShift        29
#define cThaPdaTohPwFirstByteRowCtrlTailDwIndex      0

/*--------------------------------------
BitField Name: PdaTohPwFirstByteColCtrl
BitField Type: R/W
BitField Desc: column of the first TOH byte in the STM-1 frame is transported
               through PW
BitField Bits: 28_27
--------------------------------------*/
#define cThaPdaTohPwFirstByteColCtrlMask               cBit28_27
#define cThaPdaTohPwFirstByteColCtrlShift              27
#define cThaPdaTohPwFirstByteColCtrlDwIndex            0

/*--------------------------------------
BitField Name: PdaTohPwByteEnCtrl
BitField Type: R/W
BitField Desc: TOH bytes bit map. There are 9 group for 9 row, and each row has
               3 collumns. Group bit[2:0] for TOH bytes in row#0 with bit[0]
               for col#0,bit[1] for col#1,bit[2] for col#2, and other groups
               are similar.
               - 0: disable
               - 1: enable
BitField Bits: 26_0
--------------------------------------*/
#define cThaPdaTohPwByteEnCtrlMask(row, col)  (cBit0 << (((row) * 3) + (col)))
#define cThaPdaTohPwByteEnCtrlShift(row, col) (((row) * 3) + (col))
#define cThaPdaTohPwByteEnCtrlDwIndex                  0
#define cThaPdaTohPwByteBitmapMask cBit26_0
#define cThaPdaTohPwByteBitmapShift 0

/*------------------------------------------------------------------------------
Reg Name: Thalassa PDA TOH Pseudowire Line ID Control
Reg Addr: 0x24C010
Reg Desc: The register configures TOH Pseudowire ID of  4 OCN line
------------------------------------------------------------------------------*/
#define cThaRegThalassaPDATohPseudowireLineIdCtrl   0x24C010

/*--------------------------------------
BitField Name: PdaTohPwIdLine
BitField Type: R/W
BitField Desc: TOH Pseudowire ID assigned to line#0
BitField Bits: 8_0
--------------------------------------*/
#define cThaPdaTohPwIdLineMask                        cBit8_0
#define cThaPdaTohPwIdLineShift                       0

/*------------------------------------------------------------------------------
Reg Name: Thalassa PDA TOH Pseudowire Line Enable Control
Reg Addr: 0x24C014
Reg Desc: The register enable TOH Pseudowires of corresponding lines
------------------------------------------------------------------------------*/
#define cThaRegThalassaPDATohPseudowireLineEnCtrl 0x24C014

/*--------------------------------------
BitField Name: PdaTohOcnLineGroup
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
               - 0: Select group line #0,1,2,3 from OCN for corresponding TOH
                 PW line #0,1,2,3
               - 1: Select group line #4,5,6,7 from OCN for corresponding TOH
                 PW line #0,1,2,3
BitField Bits: 20
--------------------------------------*/
#define cThaPdaTohOcnLineGrpMask                       cBit20
#define cThaPdaTohOcnLineGrpShift                      20

/*--------------------------------------
BitField Name: PdaTohOcnLineRate
BitField Type: R/W
BitField Desc: 8 bit represent for 8 OCN lines. Bit 0 is for line#0 and so on
               - 0: OC3
               - 1: OC12
BitField Bits: 19_12
--------------------------------------*/
#define cThaPdaTohOcnLineRateMask(lineId)              (cBit12 << (lineId))
#define cThaPdaTohOcnLineRateShift(lineId)             (12 + (lineId))

/*--------------------------------------
BitField Name: PdaTohOcnLineEn
BitField Type: R/W
BitField Desc: 8 bit represent for 8 OCN lines. Bit 0 is for line#0 and so on
               - 0: The OCN line is disabled
               - 1: The OCN line is enabled
BitField Bits: 11_4
--------------------------------------*/
#define cThaPdaTohOcnLineEnMask(lineId)                (cBit4 << (lineId))
#define cThaPdaTohOcnLineEnShift(lineId)               (4 + (lineId))

/*--------------------------------------
BitField Name: PdaTohPwLine0En
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
               - 1: TOH PW of line #0 is enabled
               - 0: TOH PW of line #0 is disabled
BitField Bits: 0
--------------------------------------*/
#define cThaPdaTohPwLineEnMask(lineId)                (cBit0 << lineId)
#define cThaPdaTohPwLineEnShift(lineId)               (lineId)

#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwUpsrUseValue_Mask                                  cBit15
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwUpsrUseValue_Shift                                      15
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwHspwUseValue_Mask                                  cBit14
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwHspwUseValue_Shift                                      14
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwUpsrGrpValue_Mask                                cBit13_6
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwUpsrGrpValue_Shift                                       6
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwHspwGrpValue_Mask                                 cBit5_0
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwHspwGrpValue_Shift                                       0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEPWV2REG_H_ */


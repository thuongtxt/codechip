/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : ThaPwToh.h
 * 
 * Created Date: Mar 6, 2014
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPWTOH_H_
#define _THAPWTOH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPwToh.h"
#include "adapters/ThaPwAdapter.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
#define cThaInvalidTohByte cBit31_0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPwToh * ThaPwToh;

typedef struct tThaTohByte
    {
    eAtSdhLineOverheadByte ohByte;  /* SDH line TOH byte */
    uint8 sts1;                     /* STS ID, [0-2] in STM-1, [0-11] in STM-4 */
    }tThaTohByte;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
tThaTohByte* ThaPwTohFirstByteSet(ThaPwToh self, const tThaTohByte* firstByte);
tThaTohByte* ThaPwTohFirstByteGet(ThaPwToh self);
tThaTohByte* ThaPwTohLastByteSet(ThaPwToh self, const tThaTohByte* lastByte);
tThaTohByte* ThaPwTohLastByteGet(ThaPwToh self);
eBool ThaTohByteIsPrevious(ThaPwToh self, const tThaTohByte* byte1, const tThaTohByte* byte2);
eBool ThaTohByteIsAfter(ThaPwToh self, const tThaTohByte* byte1, const tThaTohByte* byte2);
eAtRet ThaTohByteRowColGet(const tThaTohByte *tohByte, uint8 *row, uint8 *col);
eAtRet ThaPwTohAllBytesAllow(ThaPwToh self);
char* ThaPwTohByteToString(const tThaTohByte* ohByte);
eAtSdhLineOverheadByte ThaTohByteFromRowColGet(uint8 row, uint8 col);

#ifdef __cplusplus
}
#endif
#endif /* _THAPWTOH_H_ */


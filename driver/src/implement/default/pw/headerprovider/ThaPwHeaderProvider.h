/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : ThaPwHeaderProvider.h
 * 
 * Created Date: Jul 9, 2014
 *
 * Description : PW header provider
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPWHEADERPROVIDER_H_
#define _THAPWHEADERPROVIDER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtEthFlow.h"
#include "../adapters/ThaPwAdapter.h"
#include "../ThaModulePw.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPwHeaderProvider ThaPwHeaderProviderNew(void);

tAtEthVlanTag* ThaPwHeaderProviderCVlanGet(ThaPwHeaderProvider self, ThaPwHeaderController controller, tAtEthVlanTag *cVlan);
tAtEthVlanTag* ThaPwHeaderProviderSVlanGet(ThaPwHeaderProvider self, ThaPwHeaderController controller, tAtEthVlanTag *sVlan);
uint8 ThaPwHeaderProviderPutCVlanToBuffer(ThaPwHeaderProvider self, uint8 *buffer, uint8 currentIndex, uint16 vlanType, const tAtEthVlanTag *vlanTag);
uint8 ThaPwHeaderProviderPutSVlanToBuffer(ThaPwHeaderProvider self, uint8 *buffer, uint8 currentIndex, uint16 vlanType, const tAtEthVlanTag *vlanTag);
tAtEthVlanTag* ThaPwHeaderProviderSVlanGetFromBuffer(ThaPwHeaderProvider self, ThaPwAdapter pw, tAtEthVlanTag *sVlan, uint8* header, uint8 headerLength);
tAtEthVlanTag* ThaPwHeaderProviderCVlanGetFromBuffer(ThaPwHeaderProvider self, ThaPwAdapter pw, tAtEthVlanTag *cVlan, uint8* header, uint8 headerLength);
uint16 ThaPwHeaderProviderCVlanTpidGet(ThaPwHeaderProvider self, ThaPwHeaderController controller);
uint16 ThaPwHeaderProviderSVlanTpidGet(ThaPwHeaderProvider self, ThaPwHeaderController controller);

/* Product concretes */
ThaPwHeaderProvider Tha60000031PwHeaderProviderNew(void);
ThaPwHeaderProvider Tha60210012PwHeaderProviderNew(void);

#ifdef __cplusplus
}
#endif
#endif /* _THAPWHEADERPROVIDER_H_ */


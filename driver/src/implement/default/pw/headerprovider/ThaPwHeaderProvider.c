/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : ThaPwHeaderProvider.c
 *
 * Created Date: Jul 9, 2014
 *
 * Description : PW header provider
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaPwHeaderProviderInternal.h"
#include "AtEthPort.h"
#include "../../eth/ThaModuleEth.h"
#include "../../util/ThaUtil.h"
#include "../headercontrollers/ThaPwHeaderController.h"
#include "../headercontrollers/ThaPwHeaderControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaPwHeaderProviderMethods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePw ModulePw(ThaPwAdapter pw)
    {
    return (ThaModulePw)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)pw), cAtModulePw);
    }

static tAtEthVlanTag* CVlanGetFromBuffer(ThaPwHeaderProvider self, ThaPwAdapter pw, tAtEthVlanTag *cVlan, uint8* header, uint8 headerLength)
    {
    uint8* headerBuffer = header;
    static const uint8 cSVlanSizeInBytes = 4;
    ThaPwHeaderController controller = ThaPwAdapterHeaderController(pw);
    uint16 cVlanTpidIndex = controller->cVlanTpidIndex;
    uint16 sVlanTpidIndex = controller->sVlanTpidIndex;

	AtUnused(headerLength);
	AtUnused(self);

    if (cVlanTpidIndex == 0)
        return NULL;

    headerBuffer = headerBuffer + cMacSizeInBytes;

    if (ThaModulePwLookupModeGet(ModulePw(pw), (AtPw)pw) == cThaPwLookupMode2Vlans)
        {
        headerBuffer += cSVlanSizeInBytes;
        headerBuffer += cVlanTypeSizeInBytes;
        return ThaPktUtilVlanTagFromBuffer(headerBuffer, cVlan);
        }

    /* If next two byte is cThaEthTypeSvlan */
    if (sVlanTpidIndex)
        headerBuffer = headerBuffer + cSVlanSizeInBytes + cVlanTypeSizeInBytes;
    else
        headerBuffer = headerBuffer + cVlanTypeSizeInBytes;

    return ThaPktUtilVlanTagFromBuffer(headerBuffer, cVlan);
    }

static uint16 CVlanTpidGetFromBuffer(ThaPwHeaderProvider self, ThaPwAdapter pw, uint8* header, uint8 headerLength)
    {
    uint8* headerBuffer = header;
    ThaPwHeaderController controller = ThaPwAdapterHeaderController(pw);
    uint16 cVlanTpidIndex = controller->cVlanTpidIndex;
    AtUnused(self);

    if ((cVlanTpidIndex == 0) || (cVlanTpidIndex > headerLength))
        return 0;

    headerBuffer = headerBuffer + cVlanTpidIndex;
    return ThaPktUtilVlanTypeFromBuffer(headerBuffer);
    }

static uint16 SVlanTpidGetFromBuffer(ThaPwHeaderProvider self, ThaPwAdapter pw, uint8* header, uint8 headerLength)
    {
    uint8* headerBuffer = header;
    ThaPwHeaderController controller = ThaPwAdapterHeaderController(pw);
    uint16 sVlanTpidIndex = controller->sVlanTpidIndex;
    AtUnused(self);

    if ((sVlanTpidIndex == 0) || (sVlanTpidIndex > headerLength))
        return 0;

    headerBuffer = headerBuffer + sVlanTpidIndex;
    return ThaPktUtilVlanTypeFromBuffer(headerBuffer);
    }

static tAtEthVlanTag* CVlanGet(ThaPwHeaderProvider self, ThaPwHeaderController controller, tAtEthVlanTag *cVlan)
    {
    /* Get current header buffer */
    uint8 currentHeaderLength;
    uint8 *headerBuffer = ThaPwHeaderControllerHwHeaderArrayGet(controller, &currentHeaderLength);

    return mMethodsGet(self)->CVlanGetFromBuffer(self, ThaPwHeaderControllerAdapterGet(controller), cVlan, headerBuffer, currentHeaderLength);
    }

static uint16 CVlanTpidGet(ThaPwHeaderProvider self, ThaPwHeaderController controller)
    {
    /* Get current header buffer */
    uint8 currentHeaderLength;
    uint8 *headerBuffer = ThaPwHeaderControllerHwHeaderArrayGet(controller, &currentHeaderLength);

    return mMethodsGet(self)->CVlanTpidGetFromBuffer(self, ThaPwHeaderControllerAdapterGet(controller), headerBuffer, currentHeaderLength);
    }

static uint16 SVlanTpidGet(ThaPwHeaderProvider self, ThaPwHeaderController controller)
    {
    /* Get current header buffer */
    uint8 currentHeaderLength;
    uint8 *headerBuffer = ThaPwHeaderControllerHwHeaderArrayGet(controller, &currentHeaderLength);

    return mMethodsGet(self)->SVlanTpidGetFromBuffer(self, ThaPwHeaderControllerAdapterGet(controller), headerBuffer, currentHeaderLength);
    }

static tAtEthVlanTag* SVlanGetFromBuffer(ThaPwHeaderProvider self, ThaPwAdapter pw, tAtEthVlanTag *sVlan, uint8* header, uint8 headerLength)
    {
    uint8 *headerBuffer = header;
    ThaPwHeaderController controller = ThaPwAdapterHeaderController(pw);
    uint16 sVlanTpidIndex = controller->sVlanTpidIndex;

    AtUnused(headerLength);
    AtUnused(self);

    if (sVlanTpidIndex == 0)
        return NULL;

    headerBuffer = headerBuffer + cMacSizeInBytes + cVlanTypeSizeInBytes;
    return ThaPktUtilVlanTagFromBuffer(headerBuffer, sVlan);
    }

static tAtEthVlanTag* SVlanGet(ThaPwHeaderProvider self, ThaPwHeaderController controller, tAtEthVlanTag *sVlan)
    {
    /* Get current header buffer */
    uint8 currentHeaderLength;
    uint8 *headerBuffer = ThaPwHeaderControllerHwHeaderArrayGet(controller, &currentHeaderLength);

    return mMethodsGet(self)->SVlanGetFromBuffer(self, ThaPwHeaderControllerAdapterGet(controller), sVlan, headerBuffer, currentHeaderLength);
    }

static uint8 PutCVlanToBuffer(ThaPwHeaderProvider self, uint8 *buffer, uint8 currentIndex, uint16 vlanType, const tAtEthVlanTag *vlanTag)
    {
	AtUnused(self);
    return ThaPktUtilPutVlanToBuffer(buffer, currentIndex, vlanType, vlanTag);
    }

static uint8 PutSVlanToBuffer(ThaPwHeaderProvider self, uint8 *buffer, uint8 currentIndex, uint16 vlanType, const tAtEthVlanTag *vlanTag)
    {
	AtUnused(self);
    return ThaPktUtilPutVlanToBuffer(buffer, currentIndex, vlanType, vlanTag);
    }

static void MethodsInit(ThaPwHeaderProvider self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, CVlanGet);
        mMethodOverride(m_methods, SVlanGet);
        mMethodOverride(m_methods, SVlanGetFromBuffer);
        mMethodOverride(m_methods, CVlanGetFromBuffer);
        mMethodOverride(m_methods, PutCVlanToBuffer);
        mMethodOverride(m_methods, PutSVlanToBuffer);
        mMethodOverride(m_methods, CVlanTpidGet);
        mMethodOverride(m_methods, SVlanTpidGet);
        mMethodOverride(m_methods, SVlanTpidGetFromBuffer);
        mMethodOverride(m_methods, CVlanTpidGetFromBuffer);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPwHeaderProvider);
    }

ThaPwHeaderProvider ThaPwHeaderProviderObjectInit(ThaPwHeaderProvider self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Override */
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwHeaderProvider ThaPwHeaderProviderNew(void)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwHeaderProvider newProvider = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newProvider == NULL)
        return NULL;

    /* Construct it */
    return ThaPwHeaderProviderObjectInit(newProvider);
    }

tAtEthVlanTag* ThaPwHeaderProviderCVlanGet(ThaPwHeaderProvider self, ThaPwHeaderController controller, tAtEthVlanTag *cVlan)
    {
    if (self)
        return mMethodsGet(self)->CVlanGet(self, controller, cVlan);

    return NULL;
    }

uint16 ThaPwHeaderProviderCVlanTpidGet(ThaPwHeaderProvider self, ThaPwHeaderController controller)
    {
    if (self)
        return mMethodsGet(self)->CVlanTpidGet(self, controller);

    return 0;
    }

uint16 ThaPwHeaderProviderSVlanTpidGet(ThaPwHeaderProvider self, ThaPwHeaderController controller)
    {
    if (self)
        return mMethodsGet(self)->SVlanTpidGet(self, controller);

    return 0;
    }

tAtEthVlanTag* ThaPwHeaderProviderSVlanGet(ThaPwHeaderProvider self, ThaPwHeaderController controller, tAtEthVlanTag *sVlan)
    {
    if (self)
        return mMethodsGet(self)->SVlanGet(self, controller, sVlan);

    return NULL;
    }

uint8 ThaPwHeaderProviderPutCVlanToBuffer(ThaPwHeaderProvider self, uint8 *buffer, uint8 currentIndex, uint16 vlanType, const tAtEthVlanTag *vlanTag)
    {
    if (self == NULL)
        return currentIndex;

    return mMethodsGet(self)->PutCVlanToBuffer(self, buffer, currentIndex, vlanType, vlanTag);
    }

uint8 ThaPwHeaderProviderPutSVlanToBuffer(ThaPwHeaderProvider self, uint8 *buffer, uint8 currentIndex, uint16 vlanType, const tAtEthVlanTag *vlanTag)
    {
    if (self == NULL)
        return currentIndex;

    return mMethodsGet(self)->PutSVlanToBuffer(self, buffer, currentIndex, vlanType, vlanTag);
    }

tAtEthVlanTag* ThaPwHeaderProviderSVlanGetFromBuffer(ThaPwHeaderProvider self, ThaPwAdapter pw, tAtEthVlanTag *sVlan, uint8* header, uint8 headerLength)
    {
    if (self)
        return mMethodsGet(self)->SVlanGetFromBuffer(self, pw, sVlan, header, headerLength);
    return NULL;
    }

tAtEthVlanTag* ThaPwHeaderProviderCVlanGetFromBuffer(ThaPwHeaderProvider self, ThaPwAdapter pw, tAtEthVlanTag *cVlan, uint8* header, uint8 headerLength)
    {
    if (self)
        return mMethodsGet(self)->CVlanGetFromBuffer(self, pw, cVlan, header, headerLength);
    return NULL;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : ThaPwHeaderProviderInternal.h
 * 
 * Created Date: Jul 9, 2014
 *
 * Description : Header provider
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPWHEADERPROVIDERINTERNAL_H_
#define _THAPWHEADERPROVIDERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaPwHeaderProvider.h"
#include "../../man/ThaDeviceInternal.h"
#include "../adapters/ThaPwAdapter.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
#define cMacSizeInBytes (2 * cAtMacAddressLen)
#define cEthTypeSizeInBytes  2

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPwHeaderProviderMethods
    {
    tAtEthVlanTag* (*CVlanGet)(ThaPwHeaderProvider self, ThaPwHeaderController controller, tAtEthVlanTag *cVlan);
    tAtEthVlanTag* (*SVlanGet)(ThaPwHeaderProvider self, ThaPwHeaderController controller, tAtEthVlanTag *sVlan);
    uint8 (*PutSVlanToBuffer)(ThaPwHeaderProvider self, uint8 *buffer, uint8 currentIndex, uint16 vlanType, const tAtEthVlanTag *vlanTag);
    uint8 (*PutCVlanToBuffer)(ThaPwHeaderProvider self, uint8 *buffer, uint8 currentIndex, uint16 vlanType, const tAtEthVlanTag *vlanTag);
    tAtEthVlanTag* (*SVlanGetFromBuffer)(ThaPwHeaderProvider self, ThaPwAdapter pw, tAtEthVlanTag *sVlan, uint8* header, uint8 headerLength);
    tAtEthVlanTag* (*CVlanGetFromBuffer)(ThaPwHeaderProvider self, ThaPwAdapter pw, tAtEthVlanTag *cVlan, uint8* header, uint8 headerLength);
    uint16 (*CVlanTpidGet)(ThaPwHeaderProvider self, ThaPwHeaderController controller);
    uint16 (*SVlanTpidGet)(ThaPwHeaderProvider self, ThaPwHeaderController controller);
    uint16 (*CVlanTpidGetFromBuffer)(ThaPwHeaderProvider self, ThaPwAdapter pw, uint8* header, uint8 headerLength);
    uint16 (*SVlanTpidGetFromBuffer)(ThaPwHeaderProvider self, ThaPwAdapter pw, uint8* header, uint8 headerLength);
    }tThaPwHeaderProviderMethods;

typedef struct tThaPwHeaderProvider
    {
    tAtObject super;
    const tThaPwHeaderProviderMethods* methods;
    
    }tThaPwHeaderProvider;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPwHeaderProvider ThaPwHeaderProviderObjectInit(ThaPwHeaderProvider self);

#ifdef __cplusplus
}
#endif
#endif /* _THAPWHEADERPROVIDERINTERNAL_H_ */


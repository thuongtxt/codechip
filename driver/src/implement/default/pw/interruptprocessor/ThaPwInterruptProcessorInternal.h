/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : ThaPwInterruptProcessorInternal.h
 * 
 * Created Date: Oct 29, 2015
 *
 * Description : Default representation of PW interrupt processor.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPWINTERRUPTPROCESSORINTERNAL_H_
#define _THAPWINTERRUPTPROCESSORINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h" /* Superclass. */
#include "ThaPwInterruptProcessor.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPwInterruptProcessorMethods
    {
    uint32 (*BaseAddress)(ThaPwInterruptProcessor self);
    void (*Process)(ThaPwInterruptProcessor self, uint32 glbIntr, AtHal hal);
    eAtRet (*Enable)(ThaPwInterruptProcessor self, eBool enable);
    uint32 (*CurrentStatusRegister)(ThaPwInterruptProcessor self);
    uint32 (*InterruptStatusRegister)(ThaPwInterruptProcessor self);
    uint32 (*InterruptMaskRegister)(ThaPwInterruptProcessor self);
    }tThaPwInterruptProcessorMethods;

typedef struct tThaPwInterruptProcessor
    {
    tAtObject super;

    const tThaPwInterruptProcessorMethods* methods;
    ThaModulePw module;
    }tThaPwInterruptProcessor;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPwInterruptProcessor ThaPwInterruptProcessorObjectInit(ThaPwInterruptProcessor self, ThaModulePw module);

#ifdef __cplusplus
}
#endif
#endif /* _THAPWINTERRUPTPROCESSORINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : ThaPwInterruptProcessor.c
 *
 * Created Date: Oct 29, 2015
 *
 * Description : Default implementation of PW interrupt processor.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/man/AtDriverInternal.h"
#include "../ThaModulePw.h"
#include "ThaPwInterruptProcessor.h"
#include "ThaPwInterruptProcessorInternal.h"
#include "../../../../util/coder/AtCoderUtil.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaPwInterruptProcessorMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(ThaPwInterruptProcessor self)
    {
    AtUnused(self);
    return 0x0;
    }

static void Process(ThaPwInterruptProcessor self, uint32 glbIntr, AtHal hal)
    {
    AtUnused(self);
    AtUnused(glbIntr);
    AtUnused(hal);
    }

static eAtRet Enable(ThaPwInterruptProcessor self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtOk;
    }

static uint32 CurrentStatusRegister(ThaPwInterruptProcessor self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 InterruptStatusRegister(ThaPwInterruptProcessor self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 InterruptMaskRegister(ThaPwInterruptProcessor self)
    {
    AtUnused(self);
    return 0;
    }

static const char *ToString(AtObject self)
    {
    AtUnused(self);
    return "ThaPwInterruptProcessor";
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaPwInterruptProcessor object = (ThaPwInterruptProcessor)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(module);
    }

static void OverrideAtObject(ThaPwInterruptProcessor self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(ThaPwInterruptProcessor self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(ThaPwInterruptProcessor self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, BaseAddress);
        mMethodOverride(m_methods, Process);
        mMethodOverride(m_methods, Enable);
        mMethodOverride(m_methods, CurrentStatusRegister);
        mMethodOverride(m_methods, InterruptStatusRegister);
        mMethodOverride(m_methods, InterruptMaskRegister);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPwInterruptProcessor);
    }

ThaPwInterruptProcessor ThaPwInterruptProcessorObjectInit(ThaPwInterruptProcessor self, ThaModulePw module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Override */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Private data */
    self->module = module;

    return self;
    }

ThaPwInterruptProcessor ThaPwInterruptProcessorNew(ThaModulePw module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwInterruptProcessor newProcessor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newProcessor == NULL)
        return NULL;

    /* Construct it */
    return ThaPwInterruptProcessorObjectInit(newProcessor, module);
    }

uint32 ThaPwInterruptProcessorBaseAddress(ThaPwInterruptProcessor self)
    {
    if (self)
        return mMethodsGet(self)->BaseAddress(self);
    return 0x0;
    }

void ThaPwInterruptProcessorProcess(ThaPwInterruptProcessor self, uint32 glbIntr, AtHal hal)
    {
    if (self)
        mMethodsGet(self)->Process(self, glbIntr, hal);
    }

eAtRet ThaPwInterruptProcessorEnable(ThaPwInterruptProcessor self, eBool enable)
    {
    if (self)
        mMethodsGet(self)->Enable(self, enable);
    return cAtErrorNullPointer;
    }

ThaModulePw ThaPwInterruptProcessorModuleGet(ThaPwInterruptProcessor self)
    {
    if (self)
        return self->module;
    return NULL;
    }

uint32 ThaPwInterruptProcessorCurrentStatusRegister(ThaPwInterruptProcessor self)
    {
    if (self)
        return mMethodsGet(self)->CurrentStatusRegister(self);
    return 0;
    }

uint32 ThaPwInterruptProcessorInterruptStatusRegister(ThaPwInterruptProcessor self)
    {
    if (self)
        return mMethodsGet(self)->InterruptStatusRegister(self);
    return 0;
    }

uint32 ThaPwInterruptProcessorInterruptMaskRegister(ThaPwInterruptProcessor self)
    {
    if (self)
        return mMethodsGet(self)->InterruptMaskRegister(self);
    return 0;
    }

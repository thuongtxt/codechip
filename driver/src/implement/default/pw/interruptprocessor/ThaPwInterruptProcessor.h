/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : ThaPwInterruptProcessor.h
 * 
 * Created Date: Oct 29, 2015
 *
 * Description : Default interface of PW interrupt processor.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPWINTERRUPTPROCESSOR_H_
#define _THAPWINTERRUPTPROCESSOR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../ThaModulePw.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPwInterruptProcessor ThaPwInterruptProcessorNew(ThaModulePw module);

uint32 ThaPwInterruptProcessorBaseAddress(ThaPwInterruptProcessor self);
void ThaPwInterruptProcessorProcess(ThaPwInterruptProcessor self, uint32 glbIntr, AtHal hal);
eAtRet ThaPwInterruptProcessorEnable(ThaPwInterruptProcessor self, eBool enable);

ThaModulePw ThaPwInterruptProcessorModuleGet(ThaPwInterruptProcessor self);

uint32 ThaPwInterruptProcessorCurrentStatusRegister(ThaPwInterruptProcessor self);
uint32 ThaPwInterruptProcessorInterruptStatusRegister(ThaPwInterruptProcessor self);
uint32 ThaPwInterruptProcessorInterruptMaskRegister(ThaPwInterruptProcessor self);

/* Concrete classes */
ThaPwInterruptProcessor Tha60290022PwInterruptProcessorNew(ThaModulePw module);

#ifdef __cplusplus
}
#endif
#endif /* _THAPWINTERRUPTPROCESSOR_H_ */


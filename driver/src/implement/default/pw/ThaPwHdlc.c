/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : ThaPwHdlcPpp.c
 *
 * Created Date: Nov 19, 2012
 *
 * Description : HDLC PW
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../generic/common/AtChannelInternal.h"
#include "../../../generic/pw/AtModulePwInternal.h"
#include "../../../util/coder/AtCoderUtil.h"
#include "../encap/dynamic/ThaDynamicHdlcChannel.h"
#include "../encap/dynamic/ThaModuleDynamicEncap.h"
#include "../encap/profile/profilepool/ThaProfile.h"
#include "../ppp/dynamic/ThaPhysicalPppLink.h"
#include "adapters/ThaPwAdapterInternal.h"
#include "ThaPwInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaPwHdlc)self)
#define mHdlcPwAdapter(self) ((ThaPwHdlcAdapter)Adapter((AtPw)self))

/*--------------------------- Local typedefs ---------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtObjectMethods  m_AtObjectOverride;
static tAtPwMethods      m_AtPwOverride;
static tAtChannelMethods m_AtChannelOverride;
static tAtPwHdlcMethods  m_AtPwHdlcOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;
static const tAtPwHdlcMethods  *m_AtPwHdlcMethods  = NULL;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPw Adapter(AtPw pw)
    {
    return (AtPw)(mThis(pw))->adapter;
    }
    
#include "ThaPwCommonSetup.h"

static eAtModulePwRet PayloadTypeSet(AtPwHdlc self, eAtPwHdlcPayloadType payloadType)
    {
    eAtRet ret;

    if (payloadType == cAtPwHdlcPayloadTypeInvalid)
        return cAtErrorModeNotSupport;

    ret = mMethodsGet(mHdlcPwAdapter(self))->PayloadTypeSet(mHdlcPwAdapter(self), payloadType);
    if (ret == cAtOk)
        mThis(self)->payloadType = payloadType;

    return ret;
    }

static eAtPwHdlcPayloadType PayloadTypeGet(AtPwHdlc self)
    {
    return mThis(self)->payloadType;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaPwHdlc object = (ThaPwHdlc)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObject(adapter);
    }

static void Delete(AtObject self)
    {
    ThaPwAdapterDelete((AtPw)self);

    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static void OverrideAtObject(AtPw self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtPwHdlc(AtPw self)
    {
    AtPwHdlc hdlc = (AtPwHdlc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPwHdlcMethods = mMethodsGet(hdlc);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPwHdlcOverride, m_AtPwHdlcMethods, sizeof(m_AtPwHdlcOverride));

        mMethodOverride(m_AtPwHdlcOverride, PayloadTypeSet);
        mMethodOverride(m_AtPwHdlcOverride, PayloadTypeGet);
        }

    mMethodsSet(hdlc, &m_AtPwHdlcOverride);
    }

static void Override(AtPw self)
    {
    OverrideAtObject(self);
    OverrideAtPw(self);
    OverrideAtChannel(self);
    OverrideAtPwHdlc(self);
    }

/* To initialize object */
static AtPw ObjectInit(AtPw self, AtHdlcChannel hdlcChannel, AtModulePw module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaPwHdlc));

    /* Super constructor should be called first */
    if (AtPwHdlcObjectInit((AtPwHdlc)self, AtChannelIdGet((AtChannel)hdlcChannel), module) == NULL)
        return NULL;

    /* Only initialize method structures one time */
    Override(self);
    m_methodsInit = 1;

    /* PW adapter */
    ((ThaPwHdlc)self)->adapter = ThaPwHdlcAdapterNew(self);

    return self;
    }

AtPw ThaPwHdlcNew(AtModulePw self, AtHdlcChannel hdlcChannel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPw newPw = mMethodsGet(osal)->MemAlloc(osal, sizeof(tThaPwHdlc));
    if (newPw == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPw, hdlcChannel, self);
    }

ThaPwAdapter ThaPwHdlcAdapterGet(AtPw self)
    {
    return (ThaPwAdapter)Adapter(self);
    }

void ThaPwHdlcAdapterDelete(AtPw self)
    {
    AtObjectDelete((AtObject)((ThaPwHdlc)self)->adapter);
    ((ThaPwHdlc)self)->adapter = NULL;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : ThaPwModule.c
 *
 * Created Date: Nov 17, 2012
 *
 * Description : Psuedowire module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../util/coder/AtCoderUtil.h"
#include "../pwe/ThaModulePwe.h"
#include "../cla/ThaModuleCla.h"
#include "../man/ThaIpCoreInternal.h"
#include "ThaModulePwInternal.h"
#include "headercontrollers/ThaPwHeaderController.h"
#include "hspw/ThaPwHsGroup.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self_) ((ThaModulePw)self_)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tThaModulePwMethods m_methods;

/* Override */
static tAtObjectMethods   m_AtObjectOverride;
static tAtModulePwMethods m_AtModulePwOverride;
static tAtModuleMethods   m_AtModuleOverride;

/* Save super implementation */
static const tAtModulePwMethods *m_AtModulePwMethods = NULL;
static const tAtObjectMethods   *m_AtObjectMethods   = NULL;
static const tAtModuleMethods   *m_AtModuleMethods   = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaModulePw);
    }

static uint32 MaxPwsGet(AtModulePw self)
    {
	AtUnused(self);
    return 336;
    }

static eAtModulePwRet IdleCodeSet(AtModulePw self, uint8 idleCode)
    {
    ThaModulePda modulePda = (ThaModulePda)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModulePda);
    if (modulePda)
        return mMethodsGet(modulePda)->ModuleIdleCodeSet(modulePda, idleCode);

    return cAtError;
    }

static uint8 IdleCodeGet(AtModulePw self)
    {
    ThaModulePda modulePda = (ThaModulePda)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModulePda);
    if (modulePda)
        return mMethodsGet(modulePda)->ModuleIdleCodeGet(modulePda);

    return 0;
    }

static eBool IdleCodeIsSupported(AtModulePw self)
    {
    ThaModulePda modulePda = (ThaModulePda)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModulePda);
    if (modulePda)
        return mMethodsGet(modulePda)->ModuleIdleCodeIsSupported(modulePda);

    return 0;
    }

static eBool NeedToInitPw(AtPw pw)
    {
    if (AtChannelDeviceInWarmRestore((AtChannel)pw))
        return cAtFalse;

    return cAtTrue;
    }

static AtPwHdlc HdlcCreate(AtModulePw self, AtHdlcChannel hdlcChannel)
    {
    eAtRet ret;
    AtPwHdlc pw = m_AtModulePwMethods->HdlcCreate(self, hdlcChannel);

    if (pw == NULL)
        return NULL;

    ret = AtPwCircuitBind((AtPw)pw, (AtChannel)hdlcChannel);
    if (ret != cAtOk)
        {
        mMethodsGet(self)->DeletePwObject(self, (AtPw)pw);
        AtModuleLog((AtModule)self, cAtLogLevelCritical, AtSourceLocation,
                    "Cannot bind HDLC PW to %s.%s\r\n",
                    AtChannelTypeString((AtChannel)hdlcChannel), AtChannelIdString((AtChannel)hdlcChannel));
        return NULL;
        }

    /* Init PW after created */
    ret = AtChannelInit((AtChannel) pw);
    if (ret != cAtOk)
        {
        AtModuleLog((AtModule)self, cAtLogLevelCritical, AtSourceLocation,
                    "Cannot Init HDLC PW to %s.%s\r\n",
                    AtChannelTypeString((AtChannel)hdlcChannel), AtChannelIdString((AtChannel)hdlcChannel));
        }

    return pw;
    }

static AtPwPpp PppCreate(AtModulePw self, AtPppLink pppLink)
    {
    eAtRet ret;
    AtPwPpp pw = m_AtModulePwMethods->PppCreate(self, pppLink);

    if (pw == NULL)
        return NULL;

    ret = AtPwCircuitBind((AtPw)pw, (AtChannel)pppLink);
    if (ret != cAtOk)
        {
        mMethodsGet(self)->DeletePwObject(self, (AtPw)pw);
        AtModuleLog((AtModule)self, cAtLogLevelCritical, AtSourceLocation,
                    "Cannot bind PPP PW to %s.%s\r\n",
                    AtChannelTypeString((AtChannel)pppLink), AtChannelIdString((AtChannel)pppLink));
        return NULL;
        }

    return pw;
    }

static AtPwPpp MlpppCreate(AtModulePw self, AtMpBundle mpBundle)
    {
    eAtRet ret;
    AtPwPpp pw = m_AtModulePwMethods->MlpppCreate(self, mpBundle);

    if (pw == NULL)
        return NULL;

    ret = AtPwCircuitBind((AtPw)pw, (AtChannel)mpBundle);
    if (ret != cAtOk)
        {
        mMethodsGet(self)->DeletePwObject(self, (AtPw)pw);
        AtModuleLog((AtModule)self, cAtLogLevelCritical, AtSourceLocation,
                    "Cannot bind Multilink PPP PW to %s.%s\r\n",
                    AtChannelTypeString((AtChannel)mpBundle), AtChannelIdString((AtChannel)mpBundle));
        return NULL;
        }

    return pw;
    }

static AtPwFr FrVcCreate(AtModulePw self, AtFrVirtualCircuit frVc)
    {
    eAtRet ret;
    AtPwFr pw = m_AtModulePwMethods->FrVcCreate(self, frVc);

    if (pw == NULL)
        return NULL;

    ret = AtPwCircuitBind((AtPw)pw, (AtChannel)frVc);
    if (ret != cAtOk)
        {
        mMethodsGet(self)->DeletePwObject(self, (AtPw)pw);
        AtModuleLog((AtModule)self, cAtLogLevelCritical, AtSourceLocation,
                    "Cannot bind FR Virtual Circuit PW to %s.%s\r\n",
                    AtChannelTypeString((AtChannel)frVc), AtChannelIdString((AtChannel)frVc));
        return NULL;
        }

    return pw;
    }

static AtPwCep CepObjectCreate(AtModulePw self, uint32 pwId, eAtPwCepMode mode)
    {
    AtPwCep pwCep = (AtPwCep)ThaPwCepNew(self, pwId, mode);
    if (NeedToInitPw((AtPw)pwCep) && ThaModulePwNeedDoReConfig((ThaModulePw)self))
        mMethodsGet(mThis(self))->TdmPwInit(mThis(self), (AtPw)pwCep);

    return pwCep;
    }

static AtPwSAToP SAToPObjectCreate(AtModulePw self, uint32 pwId)
    {
    AtPwSAToP pwSatop = (AtPwSAToP)ThaPwSAToPNew(self, pwId);

    if (NeedToInitPw((AtPw)pwSatop) && ThaModulePwNeedDoReConfig((ThaModulePw)self))
        mMethodsGet(mThis(self))->TdmPwInit(mThis(self), (AtPw)pwSatop);

    return pwSatop;
    }

static AtPwCESoP CESoPObjectCreate(AtModulePw self, uint32 pwId, eAtPwCESoPMode mode)
    {
    AtPwCESoP pwCESoP = (AtPwCESoP)ThaPwCESoPNew(self, pwId, mode);

    if (NeedToInitPw((AtPw)pwCESoP) && ThaModulePwNeedDoReConfig((ThaModulePw)self))
        mMethodsGet(mThis(self))->TdmPwInit(mThis(self), (AtPw)pwCESoP);

    return pwCESoP;
    }

static AtPwHdlc HdlcPwObjectCreate(AtModulePw self, AtHdlcChannel hdlcChannel)
    {
    return (AtPwHdlc)ThaPwHdlcNew(self, hdlcChannel);
    }

static AtPwAtm AtmPwObjectCreate(AtModulePw self, uint32 pwId)
    {
    return (AtPwAtm)ThaPwAtmNew(self, pwId);
    }

static AtPwToh TohPwObjectCreate(AtModulePw self, uint32 pwId)
    {
    return (AtPwToh)ThaPwTohNew(self, pwId);
    }

static eAtRet Debug(AtModule self)
    {
    AtDevice device = AtModuleDeviceGet(self);
    AtModule counterModule;

    m_AtModuleMethods->Debug(self);

    /* PW module */
    AtPrintc(cSevInfo,   "PW information\r\n");
    AtPrintc(cSevInfo,   "========================================================\r\n");
    AtPrintc(cSevNormal, "* PW timeout: %d(ms)\r\n", mThis(self)->jitterEmptyTimeout);

    ThaClaControllerDebug((ThaClaController)ThaModuleClaPwControllerGet((ThaModuleCla)AtDeviceModuleGet(device, cThaModuleCla)));

    /* Debug all physical modules */
    AtModuleDebug(AtDeviceModuleGet(device, cThaModuleCos)); AtPrintc(cSevNormal, "\r\n");
    AtModuleDebug(AtDeviceModuleGet(device, cThaModuleBmt)); AtPrintc(cSevNormal, "\r\n");

    /* PW activator */
    ThaPwActivatorDebug(ThaModulePwActivator((ThaModulePw)self));

    AtPrintc(cSevNormal, "* Resources limitation: %s\r\n", mThis(self)->disableResourcesLimitation ? "Disabled" : "Enabled");
    AtPrintc(cSevNormal, "* Hitless header changing: %s\r\n", mThis(self)->hitlessHeaderChangeEnabled ? "Enabled" : "Disabled");

    /* Display counter module */
    counterModule = AtDeviceModuleGet(AtModuleDeviceGet(self), mThis(self)->countersModule);
    AtPrintc(cSevNormal, "* Counter module: %s\r\n", AtModuleTypeString(counterModule));

    return cAtOk;
    }

static void InterruptHwEnable(AtIpCore ipCore, eBool enable)
    {
    ThaIpCorePwHwInterruptEnable((ThaIpCore)ipCore, enable);
    }

static eAtRet InterruptEnable(AtModule self, eBool enable)
    {
    uint8 i, numCore;
    AtIpCore ipCore;
    AtDevice device = AtModuleDeviceGet(self);

    numCore = AtDeviceNumIpCoresGet(device);

    for (i = 0; i < numCore; i++)
        {
        ipCore = AtDeviceIpCoreGet(device, i);

        /* If a product supports interrupt pin enable register, just simple
         * enable module interrupt on IPcore. Otherwise, we need to check if
         * IPcore interrupt was enabled before enable module interrupt. */
        if (ThaIpCoreCanEnableHwInterruptPin((ThaIpCore)ipCore) || AtIpCoreInterruptIsEnabled(ipCore))
            InterruptHwEnable(ipCore, enable);

        /* Store to DB */
        ((ThaModulePw)self)->interruptIsEnabled = enable;
        }

    return m_AtModuleMethods->InterruptEnable(self, enable);
    }

static eBool InterruptIsEnabled(AtModule self)
    {
    return ((ThaModulePw)self)->interruptIsEnabled;
    }

/* Enable interrupt on one core */
static void InterruptOnIpCoreEnable(AtModule self, eBool enable, AtIpCore ipCore)
    {
    if(InterruptIsEnabled(self))
        InterruptHwEnable(ipCore, enable);
    }

static void InterruptProcess(AtModule self, uint32 glbIntr, AtIpCore ipCore)
    {
    AtHal hal = AtIpCoreHalGet(ipCore);
    if (hal)
        ThaPwInterruptProcessorProcess(ThaModulePwInterruptProcessor(mThis(self)), glbIntr, hal);

    m_AtModuleMethods->InterruptProcess(self, glbIntr, ipCore);
    }

static ThaModuleCla ClaModule(ThaModulePw self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return (ThaModuleCla)AtDeviceModuleGet(device, cThaModuleCla);
    }

static ThaClaPwController ClaPwController(ThaModulePw self)
    {
    return ThaModuleClaPwControllerGet(ClaModule(self));
    }

static eAtRet DeletePwAdapter(AtModulePw self, AtPw pw, eBool applyHardware)
    {
    ThaPwAdapter adapter = ThaPwAdapterGet(pw);
    ThaModuleCla claModule = ClaModule(mThis(self));
    eAtRet ret = cAtOk;

    if (!ThaModulePwDynamicPwAllocation(mThis(self)))
        {
        eBool deviceIsDeleting = AtDeviceIsDeleting(AtModuleDeviceGet((AtModule)self));
        if (!deviceIsDeleting)
            ret = ThaClaPwControllerLookupRemove(ThaModuleClaPwControllerGet(claModule), (AtPw)adapter, applyHardware);
        }

    ThaPwAdapterDelete(pw);
    return ret;
    }

static eAtRet ResetPw(ThaModulePw self, AtPw pw)
    {
    AtPw adapter;
    AtDevice device;
    ThaModuleCos cosModule;
    ThaModulePwe pweModule;

    if (ThaModulePwDynamicPwAllocation(mThis(self)))
        return cAtOk;

    device = AtModuleDeviceGet((AtModule)self);
    cosModule = (ThaModuleCos)AtDeviceModuleGet(device, cThaModuleCos);
    pweModule = (ThaModulePwe)AtDeviceModuleGet(device, cThaModulePwe);
    adapter = (AtPw)ThaPwAdapterGet(pw);

    ThaModuleCosPwHeaderLengthSet(cosModule, adapter, 0);
    ThaModulePwePayloadSizeSet(pweModule, adapter, 0);

    AtChannelHardwareCleanup((AtChannel)pw);

    return cAtOk;
    }

static eAtRet PwReset(AtModulePw self, AtPw pw)
    {
    if (AtPwBoundCircuitGet(pw) || AtPwHsGroupGet(pw) || AtPwApsGroupGet(pw))
        return cAtErrorChannelBusy;

    /* Reset this PW */
    mMethodsGet(mThis(self))->ResetPw(mThis(self), pw);

    DeletePwAdapter(self, pw, cAtTrue);
    ThaClaPwControllerLookupRemove(ClaPwController(mThis(self)), (AtPw)ThaPwAdapterGet(pw), cAtTrue);

    return cAtOk;
    }
    
static eAtRet InterruptMaskDisable(AtPw pw)
    {
    eAtRet ret = cAtOk;

    ret |= AtChannelInterruptMaskSet((AtChannel)pw, AtChannelInterruptMaskGet((AtChannel)pw), 0);
    ret |= AtChannelSurEngineInterruptDisable((AtChannel)pw);

    return ret;
    }

static eAtRet DeletePw(AtModulePw self, uint32 pwId)
    {
    eAtRet ret = cAtOk;
    AtPw pw = AtModulePwGetPw(self, pwId);
    if (pw == NULL)
        return cAtOk;

    ret |= InterruptMaskDisable(pw);
    ret |= PwReset(self, pw);
    if (ret != cAtOk)
        return ret;

    /* Let super do other things */
    return m_AtModulePwMethods->DeletePw(self, pwId);
    }

static ThaModuleCdr CdrModule(AtModulePw self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return (ThaModuleCdr)AtDeviceModuleGet(device, cThaModuleCdr);
    }

static eAtModulePwRet DcrClockSourceSet(AtModulePw self, eAtPwDcrClockSource clockSource)
    {
    return ThaModuleCdrDcrClockSourceSet(CdrModule(self), clockSource);
    }

static eAtPwDcrClockSource DcrClockSourceGet(AtModulePw self)
    {
    return ThaModuleCdrDcrClockSourceGet(CdrModule(self));
    }

static eAtModulePwRet DcrClockFrequencySet(AtModulePw self, uint32 khz)
    {
    if (AtModulePwDcrClockSourceGet(self) == cAtPwDcrClockSourceSystem)
        return cAtErrorModeNotSupport;

    return ThaModuleCdrDcrClockFrequencySet(CdrModule(self), khz);
    }

static uint32 DcrClockFrequencyGet(AtModulePw self)
    {
    return ThaModuleCdrDcrClockFrequencyGet(CdrModule(self));
    }

static eBool TimestampFrequencyIsValid(AtModulePw self, uint32 khz)
    {
    AtUnused(self);

    /* Frequency must be multiple of 8Khz */
    if ((khz % 8) != 0)
        {
        AtModuleLog((AtModule)self, cAtLogLevelWarning, AtSourceLocation, "Frequency must be multiple of 8Khz\r\n");
        return cAtFalse;
        }

    return cAtTrue;
    }

static eBool RtpTimestampFrequencyIsSupported(AtModulePw self, uint32 khz)
    {
    AtUnused(self);

    /* As default, only support 16 bits in register */
    if (khz > cBit15_0)
        {
        AtModuleLog((AtModule)self, cAtLogLevelWarning, AtSourceLocation, "Frequency is too large, maximum is %lu\r\n", cBit15_0);
        return cAtFalse;
        }

    return TimestampFrequencyIsValid(self, khz);
    }

static eAtModulePwRet RtpTimestampFrequencySet(AtModulePw self, uint32 khz)
    {
    if (!TimestampFrequencyIsValid(self, khz))
        return cAtErrorModeNotSupport;

    return ThaModuleCdrDcrRtpTimestampFrequencySet(CdrModule(self), khz);
    }

static uint32 RtpTimestampFrequencyGet(AtModulePw self)
    {
    return ThaModuleCdrDcrRtpTimestampFrequencyGet(CdrModule(self));
    }

static uint32 CepRtpTimestampFrequencyGet(AtModulePw self)
    {
    /* Default implement not support different CES and CEP time stamp */
    return AtModulePwRtpTimestampFrequencyGet(self);
    }

static uint32 CesRtpTimestampFrequencyGet(AtModulePw self)
    {
    /* Default implement not support different CES and CEP time stamp */
    return AtModulePwRtpTimestampFrequencyGet(self);
    }

static void DeleteAllPwAdapters(AtModulePw self)
    {
    uint16 i;
    eBool isDeleting = AtDeviceIsDeleting(AtModuleDeviceGet((AtModule)self));

    for (i = 0; i< AtModulePwMaxPwsGet(self); i++)
        {
        AtPw pw = AtModulePwGetPw(self, i);
        if (pw == NULL)
            continue;

        if (!isDeleting)
            ThaClaPwControllerLookupRemove(ClaPwController(mThis(self)), (AtPw)ThaPwAdapterGet(pw), cAtFalse);
        DeletePwAdapter(self, pw, cAtFalse);
        }
    }

static void DestroyResources(AtModulePw self)
    {
    DeleteAllPwAdapters(self);
    AtObjectDelete((AtObject)mThis(self)->pwActivator);
    mThis(self)->pwActivator = NULL;
    AtObjectDelete((AtObject)mThis(self)->pwHeaderProvider);
    mThis(self)->pwHeaderProvider = NULL;
    AtObjectDelete((AtObject)(mThis(self)->pwDebugger));
    mThis(self)->pwDebugger = NULL;
    AtObjectDelete((AtObject)mThis(self)->pwEthPortBinder);
    mThis(self)->pwEthPortBinder = NULL;
    AtObjectDelete((AtObject)mThis(self)->intrProcessor);
    mThis(self)->intrProcessor = NULL;
    AtObjectDelete((AtObject)mThis(self)->debugIntrProcessor);
    mThis(self)->debugIntrProcessor = NULL;
    }

static void Delete(AtObject self)
    {
    DestroyResources((AtModulePw)self);
    m_AtObjectMethods->Delete(self);
    }

static uint32 DefaultCounterModule(ThaModulePw self)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    return ThaDeviceDefaultCounterModule(device, AtModuleTypeGet((AtModule)self));
    }

static eAtRet Setup(AtModule self)
    {
    DestroyResources((AtModulePw)self);

    /* Reset debug configuration */
    mThis(self)->countersModule = mMethodsGet(mThis(self))->DefaultCounterModule(mThis(self));
    mThis(self)->alarmModule    = mMethodsGet(mThis(self))->DefaultDefectModule(mThis(self));

    return m_AtModuleMethods->Setup(self);
    }

static eBool DynamicPwAllocation(ThaModulePw self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static ThaPwActivator PwActivatorCreate(ThaModulePw self)
    {
    if (ThaModulePwDynamicPwAllocation(self))
        return ThaPwDynamicActivatorNew((AtModulePw)self);
    else
        return ThaPwActivatorNew((AtModulePw)self);
    }

static eBool CasIsSupported(ThaModulePw self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static uint32 StartVersionSupportHspw(ThaModulePw self)
    {
    AtUnused(self);
    return cBit31_0;
    }

static eBool EnableErrorCheckingByDefault(ThaModulePw self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static uint16 NumPwsPerPart(ThaModulePw self)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    return (uint16)(AtModulePwMaxPwsGet((AtModulePw)self) / ThaDeviceNumPartsOfModule(device, cAtModulePw));
    }

static uint32 LocalPwId(ThaModulePw self, AtPw pw)
    {
    uint16 numPwPerPart = ThaModulePwNumPwsPerPart(self);
    uint32 pwId = AtChannelIdGet((AtChannel)pw);
    return (numPwPerPart > 0) ? (pwId % numPwPerPart) : pwId;
    }

static ThaPwAdapter HwPwId2Adapter(ThaModulePw self, uint32 hwId)
    {
    /* Note, the following code will not work for products that have two separated
     * PW modules. Fortunately, they do not require to use this function to
     * convert from HW ID back to SW ID */
    AtPw pw = AtModulePwGetPw((AtModulePw)self, hwId);
    return ThaPwAdapterGet(pw);
    }

static eBool PwIdleCodeIsSupported(ThaModulePw self)
    {
	AtUnused(self);
    /* Not all of products can support IDLE code for each PW instance. So let
     * concrete products determine whether this feature is supported or not */
    return cAtFalse;
    }

static eBool PwMbitCounterIsSupported(ThaModulePw self)
    {
	AtUnused(self);
    /* Not all of products can support MBit counter for each PW instance. So let
     * concrete products determine whether this feature is supported or not */
    return cAtFalse;
    }

static eBool UseLbitPacketsForAcrDcr(ThaModulePw self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static eAtRet Init(AtModule self)
    {
    ThaModulePw pwModule = mThis(self);
    eAtRet ret = cAtOk;
    eBool useLbitPktForAcr = mMethodsGet(pwModule)->UseLbitPacketsForAcrDcr(pwModule);

    ret |= m_AtModuleMethods->Init(self);
    ret |= ThaClaPwControllerSendLbitPacketToCdrEnable(ClaPwController(pwModule), useLbitPktForAcr);

    if (mMethodsGet(pwModule)->HitlessHeaderChangeShouldBeEnabledByDefault(pwModule))
        ThaModulePwHitlessHeaderChangeEnable(pwModule, cAtTrue);

    return ret;
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static ThaPwHeaderProvider PwHeaderProviderCreate(ThaModulePw self)
    {
	AtUnused(self);
    return ThaPwHeaderProviderNew();
    }

static eAtRet DidChangeCVlanOnPw(ThaModulePw self, AtPw pwAdapter, const tAtEthVlanTag *cVlan)
    {
	AtUnused(cVlan);
	AtUnused(pwAdapter);
	AtUnused(self);
    /* Let concrete class do it */
    return cAtOk;
    }

static ThaPwDefectController DefectControllerCreate(ThaModulePw self, AtPw pw)
    {
    AtUnused(self);
    return ThaPwDefectControllerNew(pw);
    }

static uint32 PwLabelGetFromHwStorage(ThaModulePw self, AtPw pw)
    {
    /* Default implement not support */
    AtUnused(self);
    AtUnused(pw);
    return 0;
    }

static eAtRet PwLabelSaveToHwStorage(ThaModulePw self, AtPw pw, uint32 pwLabel)
    {
    /* Default implement not support */
    AtUnused(self);
    AtUnused(pw);
    AtUnused(pwLabel);
    return cAtOk;
    }

static uint8 PwEthPortIdGetFromHwStorage(ThaModulePw self, AtPw pw)
    {
    /* Default implement not support */
    AtUnused(self);
    AtUnused(pw);
    return 0;
    }

static eAtRet PwEthPortIdSaveToHwStorage(ThaModulePw self, AtPw pw, uint8 ethPortId)
    {
    /* Default implement not support */
    AtUnused(self);
    AtUnused(pw);
    AtUnused(ethPortId);
    return cAtOk;
    }

static eAtRet PwEthPortIdHwStorageClear(ThaModulePw self, AtPw pw)
    {
    /* Default implement not support */
    AtUnused(self);
    AtUnused(pw);
    return cAtOk;
    }

static uint16 De1BoundPwIdGetFromHwStorage(ThaModulePw self, AtPdhDe1 de1)
    {
    /* Default implement not support */
    AtUnused(self);
    AtUnused(de1);
    return 0;
    }

static eAtRet De1BoundPwIdSaveToHwStorage(ThaModulePw self, AtPdhDe1 de1, uint16 pwHwId)
    {
    /* Default implement not support */
    AtUnused(self);
    AtUnused(de1);
    AtUnused(pwHwId);
    return cAtOk;
    }

static eAtRet De1BoundPwIdHwStorageClear(ThaModulePw self, AtPdhDe1 de1)
    {
    /* Default implement not support */
    AtUnused(self);
    AtUnused(de1);
    return cAtOk;
    }

static eBool PwEnableGetFromHwStorage(ThaModulePw self, AtPw pw)
    {
    /* Default implement not support */
    AtUnused(self);
    AtUnused(pw);
    return cAtFalse;
    }

static eAtRet PwEnableSaveToHwStorage(ThaModulePw self, AtPw pw, eBool enable)
    {
    /* Default implement not support */
    AtUnused(self);
    AtUnused(pw);
    AtUnused(enable);
    return cAtOk;
    }

static eBool MefOverMplsIsSupported(ThaModulePw self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet PwBufferSizeDisparitySaveToHwStorage(ThaModulePw self, AtPw pw, int disparityInUs)
    {
    /* Default implement not support */
    AtUnused(self);
    AtUnused(pw);
    AtUnused(disparityInUs);
    return cAtOk;
    }

static int PwBufferSizeDisparityGetFromHwStorage(ThaModulePw self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return 0;
    }

static eAtRet PwBufferDelayDisparitySaveToHwStorage(ThaModulePw self, AtPw pw, int disparityInUs)
    {
    /* Default implement not support */
    AtUnused(self);
    AtUnused(pw);
    AtUnused(disparityInUs);
    return cAtOk;
    }

static int PwBufferDelayDisparityGetFromHwStorage(ThaModulePw self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return 0;
    }

static ThaPwHeaderController HeaderControllerObjectCreate(ThaModulePw self, AtPw adapter)
    {
    AtUnused(self);
    return ThaPwHeaderControllerNew(adapter);
    }

static ThaPwHeaderController BackupHeaderControllerObjectCreate(ThaModulePw self, AtPw adapter)
    {
    AtUnused(self);
    AtUnused(adapter);
    return NULL;
    }

static eBool CanCheckPwPayloadSize(ThaModulePw self)
    {
    /* Most of products now can check payload size constrain, just some old
     * products cannot. Let them determine */
    AtUnused(self);
    return cAtTrue;
    }

static uint32 PwAdapterHwId(ThaModulePw self, ThaPwAdapter adapter)
    {
    ThaHwPw hwPw = ThaPwAdapterHwPwGet(adapter);
    AtUnused(self);
    if (hwPw)
        return ThaHwPwIdGet(hwPw);

    return AtChannelIdGet((AtChannel)adapter);
    }

static ThaPwDebugger PwDebuggerCreate(ThaModulePw self)
    {
    AtUnused(self);
    return ThaPwDebuggerNew();
    }

static uint32 DefectBaseAddress(ThaModulePw self)
    {
    return ThaPwInterruptProcessorBaseAddress(ThaModulePwInterruptProcessor(self));
    }

static uint32 NumPwsSupportCounters(ThaModulePw self)
    {
    return AtModulePwMaxPwsGet((AtModulePw)self);
    }

static eBool DebugCountersModuleIsSupported(ThaModulePw self, uint32 module)
    {
    AtUnused(self);
    if ((module == cAtModulePw) || (module == cAtModuleSur) || (module == cThaModulePmc))
        return cAtTrue;
    return cAtFalse;
    }

static eAtRet DebugCountersModuleSet(ThaModulePw self, uint32 module)
    {
    if (DebugCountersModuleIsSupported(self, module))
        {
        self->countersModule = module;
        return cAtOk;
        }

    return cAtErrorModeNotSupport;
    }

static ThaPwEthPortBinder PwEthPortBinderCreate(ThaModulePw self)
    {
    return ThaPwEthPortBinderNew(self);
    }

static AtPwGroup HsGroupObjectCreate(AtModulePw self, uint32 groupId)
    {
    ThaModuleClaPwV2 claModule = (ThaModuleClaPwV2)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModuleCla);
    AtPwGroup newGroup = ThaPwHsGroupNew(groupId, self);

    ThaModuleClaPwV2HsGroupEnable(claModule, newGroup, cAtTrue);

    return newGroup;
    }

static ThaPwInterruptProcessor InterruptProcessorCreate(ThaModulePw self)
    {
    return ThaPwInterruptProcessorNew(self);
    }

static ThaModulePwe ModulePwe(ThaModulePw self)
    {
    return (ThaModulePwe)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModulePwe);
    }

static void PwDisablePrepare(ThaModulePw self, AtPw adapter)
    {
    ThaPwAdapterStartRemoving(adapter);
    ThaModulePwePwRemovingPrepare(ModulePwe(self), adapter);
    }

static void PwDisableFinish(ThaModulePw self, AtPw adapter)
    {
    AtUnused(self);
    ThaPwAdapterFinishRemoving(adapter);
    }

static void PwEnablePrepare(ThaModulePw self, AtPw adapter)
    {
    /* Prepare enable */
    ThaPwAdapterStartRemoving(adapter);
    ThaModulePwePwAddingPrepare(ModulePwe(self), adapter);
    }

static void PwEnableFinish(ThaModulePw self, AtPw adapter)
    {
    /* Start PWE adding */
    ThaModulePwePwAddingStart(ModulePwe(self), adapter);
    ThaPwAdapterFinishRemoving(adapter);
    }

static eBool TimingModeIsApplicableForEPAR(ThaModulePw self, eAtTimingMode timingMode)
    {
    AtUnused(self);
    return (timingMode == cAtTimingModeSys) ? cAtTrue : cAtFalse;
    }

static eAtRet PwFinishRemoving(ThaModulePw self, AtPw adapter)
    {
    eAtRet ret = cAtOk;

    AtUnused(self);

    if (AtModuleInAccessible((AtModule)self))
        return cAtOk;

    ret |= ThaPwAdapterPweFinishRemoving(adapter);
    ret |= ThaPwAdapterStopCenterJitterBuffer(adapter);

    return ret;
    }

static eAtRet PwFinishRemovingForBinding(ThaModulePw self, AtPw adapter)
    {
    eAtRet ret = cAtOk;

    AtUnused(self);

    if (AtModuleInAccessible((AtModule)self))
        return cAtOk;

    ret |= ThaPwAdapterPweFinishRemovingForBinding(adapter);
    ret |= ThaPwAdapterStopCenterJitterBufferForBinding(adapter);

    return ret;
    }

static eBool IsPwBindingTimeOptimization(ThaModulePw self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet JitterEmptyTimeoutSet(ThaModulePw self, uint32 timeoutMs)
    {
    self->jitterEmptyTimeout = timeoutMs;
    return cAtOk;
    }

static void SwitchAllPwToDefaultDefectController(AtModulePw self)
    {
    /* Bad performance but this code is run when debugging only */
    uint32 pwId;

    for (pwId = 0; pwId < AtModulePwMaxPwsGet(self); pwId++)
        {
        AtPw pw = AtModulePwGetPw(self, pwId);
        if (pw == NULL)
            continue;

        ThaPwAdapterSwitchToDefaultDefectController(ThaPwAdapterGet(pw));
        }
    }

static void SwitchAllPwToDebugDefectController(AtModulePw self)
    {
    /* Bad performance but this code is run when debugging only */
    uint32 pwId;

    for (pwId = 0; pwId < AtModulePwMaxPwsGet(self); pwId++)
        {
        AtPw pw = AtModulePwGetPw(self, pwId);
        if (pw == NULL)
            continue;

        ThaPwAdapterSwitchToDebugDefectController(ThaPwAdapterGet(pw));
        }
    }

static ThaPwInterruptProcessor InterruptProcessor(ThaModulePw self)
    {
    if (self->intrProcessor == NULL)
        self->intrProcessor = mMethodsGet(self)->InterruptProcessorCreate(self);
    return self->intrProcessor;
    }

static ThaPwInterruptProcessor DebugInterruptProcessor(ThaModulePw self)
    {
    if (self->debugIntrProcessor == NULL)
        self->debugIntrProcessor = mMethodsGet(self)->DebugIntrProcessorCreate(self);
    return self->debugIntrProcessor;
    }

static uint32 DefaultDefectModule(ThaModulePw self)
    {
    return AtModuleTypeGet((AtModule)self);
    }

static ThaPwInterruptProcessor DebugIntrProcessorCreate(ThaModulePw self)
    {
    AtUnused(self);
    return NULL;
    }

static ThaPwDefectController DebugDefectControllerCreate(ThaModulePw self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return NULL;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaModulePw object = (ThaModulePw)self;

    m_AtObjectMethods->Serialize(self, encoder);

    /* For lazy object at standby */
    (void)ThaModulePwInterruptProcessor(object);

    mEncodeUInt(interruptIsEnabled);
    mEncodeObject(pwActivator);
    mEncodeObject(pwHeaderProvider);
    mEncodeObject(pwDebugger);
    mEncodeObject(pwEthPortBinder);
    mEncodeObject(intrProcessor);
    mEncodeObject(debugIntrProcessor);
    mEncodeNone(headerBuffer);
    mEncodeNone(psnBuffer);

    mEncodeUInt(countersModule);
    mEncodeUInt(jitterEmptyTimeout);
    mEncodeUInt(disableResourcesLimitation);
    mEncodeUInt(alarmModule);
    mEncodeUInt(hitlessHeaderChangeEnabled);
    }

static AtPwPpp PppPwObjectCreate(AtModulePw self, AtPppLink pppLink)
    {
    return (AtPwPpp)ThaPwPppNew(self, pppLink);
    }

static AtPwPpp MlpppPwObjectCreate(AtModulePw self, AtMpBundle mpBundle)
    {
    return (AtPwPpp)ThaPwMlpppNew(self, mpBundle);
    }

static AtPwFr FrPwObjectCreate(AtModulePw self, AtFrVirtualCircuit frVc)
    {
    return (AtPwFr)ThaPwFrNew(self, frVc);
    }

static uint32 DefaultDcrClockFrequency(AtModulePw self)
    {
    return ThaModuleCdrDefaultDcrClockFrequency(CdrModule(self));
    }

static eAtRet DeleteHdlcPwObject(AtModulePw self, AtPw pw)
    {
    AtChannel circuit;
    if (pw == NULL)
        return cAtOk;

    circuit = AtPwBoundCircuitGet(pw);
    ThaHdlcPwAdapterCircuitUnbind((AtPw)ThaPwAdapterGet(pw));
    AtChannelBoundPwSet(circuit, NULL);
    PwReset(self, pw);
    AtObjectDelete((AtObject)pw);

    return cAtOk;
    }

static eBool CanBindEthPort(ThaModulePw self, AtPw pw, AtEthPort port)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(port);
    return cAtTrue;
    }

static eBool CanBindEthFlow(ThaModulePw self, AtPw pw, AtEthFlow flow)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(flow);
    return cAtFalse;
    }

static AtPw PwKBytePwGet(ThaModulePw self)
    {
    AtUnused(self);
    return NULL;
    }

static eBool NeedDoReConfig(ThaModulePw self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool VlanIsValid(ThaModulePw self, const tAtVlan *vlan)
    {
    AtUnused(self);

    if (vlan == NULL)
        return cAtFalse;

    if (vlan->vlanId > 4095)
        return cAtFalse;

    if (vlan->priority > 7)
        return cAtFalse;

    if (vlan->cfi > 1)
        return cAtFalse;

    return cAtTrue;
    }

static eAtRet SubportVlanSet(ThaModulePw self, uint32 subPortVlanIndex, const tAtVlan *vlan)
    {
    AtUnused(self);
    AtUnused(subPortVlanIndex);
    AtUnused(vlan);
    return cAtErrorNotImplemented;
    }

static eAtRet SubportVlanGet(ThaModulePw self, uint32 subPortVlanIndex, tAtVlan *vlan)
    {
    AtUnused(self);
    AtUnused(subPortVlanIndex);
    AtUnused(vlan);
    return cAtErrorNotImplemented;
    }

static eAtRet TxSubportVlanSet(ThaModulePw self, uint32 subPortVlanIndex, const tAtVlan *vlan)
    {
    return SubportVlanSet(self, subPortVlanIndex, vlan);
    }

static eAtRet TxSubportVlanGet(ThaModulePw self, uint32 subPortVlanIndex, tAtVlan *vlan)
    {
    return SubportVlanGet(self, subPortVlanIndex, vlan);
    }

static eAtRet ExpectedSubportVlanSet(ThaModulePw self, uint32 subPortVlanIndex, const tAtVlan *expectedVlan)
    {
    return SubportVlanSet(self, subPortVlanIndex, expectedVlan);
    }

static eAtRet ExpectedSubportVlanGet(ThaModulePw self, uint32 subPortVlanIndex, tAtVlan *expectedVlan)
    {
    return SubportVlanGet(self, subPortVlanIndex, expectedVlan);
    }

static uint32 NumSubPortVlans(ThaModulePw self)
    {
    AtUnused(self);
    return 0;
    }

static eBool SubPortVlanIndexIsInRange(ThaModulePw self, uint32 subPortVlanIndex)
    {
    if (subPortVlanIndex < ThaModulePwNumSubPortVlans(self))
        return cAtTrue;
    return cAtFalse;
    }

static eAtRet SubPortVlanInsertionEnable(ThaModulePw self, eBool enabled)
    {
    eBool bypass = enabled ? cAtFalse : cAtTrue;
    return ThaModulePweSubPortVlanBypass(ModulePwe(self), bypass);
    }

static eBool SubPortVlanInsertionIsEnabled(ThaModulePw self)
    {
    return ThaModulePweSubPortVlanIsBypassed(ModulePwe(self)) ? cAtFalse : cAtTrue;
    }

static ThaModuleCla ModuleCla(ThaModulePw self)
    {
    return (ThaModuleCla)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModuleCla);
    }

static eAtRet SubPortVlanCheckingEnable(ThaModulePw self, eBool enabled)
    {
    return ThaModuleClaSubPortVlanCheckingEnable(ModuleCla(self), enabled);
    }

static eBool SubPortVlanCheckingIsEnabled(ThaModulePw self)
    {
    return ThaModuleClaSubPortVlanCheckingIsEnabled(ModuleCla(self));
    }

static eAtRet SubPortVlanDebug(ThaModulePw self)
    {
    AtPrintc(cSevInfo,   "* SubPort VLAN:\r\n");
    AtPrintc(cSevNormal, "  - Module insertion: %s\r\n", ThaModulePwSubPortVlanInsertionIsEnabled(self) ? "enabled" : "disabled");
    AtPrintc(cSevNormal, "  - Checking        : %s\r\n", ThaModulePwSubPortVlanCheckingIsEnabled(self) ? "enabled" : "disabled");
    return cAtOk;
    }

static eAtRet TxPwSubPortVlanIndexSet(ThaModulePw self, AtPw adapter, uint32 subPortVlanIndex)
    {
    AtUnused(self);
    AtUnused(adapter);
    AtUnused(subPortVlanIndex);
    return cAtErrorModeNotSupport;
    }

static uint32 TxPwSubPortVlanIndexGet(ThaModulePw self, AtPw adapter)
    {
    AtUnused(self);
    AtUnused(adapter);
    return cInvalidUint32;
    }

static eAtRet TdmPwInit(ThaModulePw self, AtPw pw)
    {
    AtUnused(self);
    return AtChannelInit((AtChannel)pw);
    }

static eAtRet HitlessHeaderChangeEnable(ThaModulePw self, eBool enabled)
    {
    self->hitlessHeaderChangeEnabled = enabled;
    return cAtOk;
    }

static eBool HitlessHeaderChangeIsEnabled(ThaModulePw self)
    {
    return self->hitlessHeaderChangeEnabled;
    }

static eBool HitlessHeaderChangeShouldBeEnabledByDefault(ThaModulePw self)
    {
    /* Turn this on on required products */
    AtUnused(self);
    return cAtFalse;
    }

static ThaModulePda PdaModule(ThaModulePw self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return (ThaModulePda)AtDeviceModuleGet(device, cThaModulePda);
    }

static uint32 JitterBufferMaxBlocksGet(AtModulePw self)
    {
    return ThaModulePdaMaxNumJitterBufferBlocks(PdaModule(mThis(self)));
    }

static uint32 JitterBufferBlockSizeInBytesGet(AtModulePw self)
    {
    return ThaModulePdaJitterBufferBlockSizeInByte(PdaModule(mThis(self)));
    }

static uint32 JitterBufferFreeBlocksGet(AtModulePw self)
    {
    return ThaModulePdaNumUnUsedBlocks(PdaModule(mThis(self)));
    }

static AtChannel ReferenceCircuitToConfigureHardware(ThaModulePw self, ThaPwAdapter adapter, AtChannel circuit)
    {
    AtUnused(self);
    AtUnused(adapter);
    return circuit;
    }

static eBool SubportVlansSupported(ThaModulePw self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool TxActiveForceSupported(ThaModulePw self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool ShouldHandleCircuitAisForcingOnEnabling(ThaModulePw self)
    {
    /* Just some products require this, so let them determine */
    AtUnused(self);
    return cAtFalse;
    }

static eBool ShouldMaskDefectAndFailureOnPwDisabling(ThaModulePw self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool NeedClearDebug(ThaModulePw self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void StatusClear(AtModule self)
    {
    AtChannel pw;

    m_AtModuleMethods->StatusClear(self);

    if ((pw = (AtChannel)ThaModulePwKBytePwGet(mThis(self))) != NULL)
        AtChannelStatusClear(pw);
    }

static void OverrideAtObject(ThaModulePw self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModulePw(ThaModulePw self)
    {
    AtModulePw module = (AtModulePw)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModulePwMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePwOverride, m_AtModulePwMethods, sizeof(tAtModulePwMethods));
        mMethodOverride(m_AtModulePwOverride, DeletePw);
        mMethodOverride(m_AtModulePwOverride, MaxPwsGet);
        mMethodOverride(m_AtModulePwOverride, HdlcCreate);
        mMethodOverride(m_AtModulePwOverride, PppCreate);
        mMethodOverride(m_AtModulePwOverride, MlpppCreate);
        mMethodOverride(m_AtModulePwOverride, FrVcCreate);
        mMethodOverride(m_AtModulePwOverride, CepObjectCreate);
        mMethodOverride(m_AtModulePwOverride, SAToPObjectCreate);
        mMethodOverride(m_AtModulePwOverride, CESoPObjectCreate);
        mMethodOverride(m_AtModulePwOverride, HdlcPwObjectCreate);
        mMethodOverride(m_AtModulePwOverride, AtmPwObjectCreate);
        mMethodOverride(m_AtModulePwOverride, TohPwObjectCreate);
        mMethodOverride(m_AtModulePwOverride, IdleCodeSet);
        mMethodOverride(m_AtModulePwOverride, IdleCodeGet);
        mMethodOverride(m_AtModulePwOverride, IdleCodeIsSupported);

        mMethodOverride(m_AtModulePwOverride, DcrClockSourceSet);
        mMethodOverride(m_AtModulePwOverride, DcrClockSourceGet);
        mMethodOverride(m_AtModulePwOverride, DcrClockFrequencySet);
        mMethodOverride(m_AtModulePwOverride, DcrClockFrequencyGet);
        mMethodOverride(m_AtModulePwOverride, RtpTimestampFrequencySet);
        mMethodOverride(m_AtModulePwOverride, RtpTimestampFrequencyGet);
        mMethodOverride(m_AtModulePwOverride, RtpTimestampFrequencyIsSupported);
        mMethodOverride(m_AtModulePwOverride, CepRtpTimestampFrequencyGet);
        mMethodOverride(m_AtModulePwOverride, CesRtpTimestampFrequencyGet);
        mMethodOverride(m_AtModulePwOverride, HsGroupObjectCreate);
        mMethodOverride(m_AtModulePwOverride, PppPwObjectCreate);
        mMethodOverride(m_AtModulePwOverride, MlpppPwObjectCreate);
        mMethodOverride(m_AtModulePwOverride, FrPwObjectCreate);
        mMethodOverride(m_AtModulePwOverride, DefaultDcrClockFrequency);
        mMethodOverride(m_AtModulePwOverride, DeleteHdlcPwObject);
        mMethodOverride(m_AtModulePwOverride, JitterBufferMaxBlocksGet);
        mMethodOverride(m_AtModulePwOverride, JitterBufferBlockSizeInBytesGet);
        mMethodOverride(m_AtModulePwOverride, JitterBufferFreeBlocksGet);
        }

    mMethodsSet(module, &m_AtModulePwOverride);
    }

static void OverrideAtModule(ThaModulePw self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Setup);
        mMethodOverride(m_AtModuleOverride, Debug);
        mMethodOverride(m_AtModuleOverride, InterruptEnable);
        mMethodOverride(m_AtModuleOverride, InterruptIsEnabled);
        mMethodOverride(m_AtModuleOverride, InterruptOnIpCoreEnable);
        mMethodOverride(m_AtModuleOverride, InterruptProcess);
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, StatusClear);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void Override(ThaModulePw self)
    {
    OverrideAtObject(self);
    OverrideAtModulePw(self);
    OverrideAtModule(self);
    }

static void MethodsInit(ThaModulePw self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, DynamicPwAllocation);
        mMethodOverride(m_methods, PwActivatorCreate);
        mMethodOverride(m_methods, CasIsSupported);
        mMethodOverride(m_methods, StartVersionSupportHspw);
        mMethodOverride(m_methods, EnableErrorCheckingByDefault);
        mMethodOverride(m_methods, NumPwsPerPart);
        mMethodOverride(m_methods, LocalPwId);
        mMethodOverride(m_methods, PwIdleCodeIsSupported);
        mMethodOverride(m_methods, PwMbitCounterIsSupported);
        mMethodOverride(m_methods, UseLbitPacketsForAcrDcr);
        mMethodOverride(m_methods, PwHeaderProviderCreate);
        mMethodOverride(m_methods, DefectControllerCreate);
        mMethodOverride(m_methods, DidChangeCVlanOnPw);
        mMethodOverride(m_methods, TimingModeIsApplicableForEPAR);
        mMethodOverride(m_methods, CanBindEthPort);
        mMethodOverride(m_methods, CanBindEthFlow);
        mMethodOverride(m_methods, TdmPwInit);
        mMethodOverride(m_methods, HitlessHeaderChangeShouldBeEnabledByDefault);
        mMethodOverride(m_methods, ShouldHandleCircuitAisForcingOnEnabling);
        mMethodOverride(m_methods, ShouldMaskDefectAndFailureOnPwDisabling);

        mMethodOverride(m_methods, PwEnablePrepare);
        mMethodOverride(m_methods, PwEnableFinish);
        mMethodOverride(m_methods, PwDisablePrepare);
        mMethodOverride(m_methods, PwDisableFinish);
        mMethodOverride(m_methods, PwFinishRemoving);
        mMethodOverride(m_methods, PwFinishRemovingForBinding);
        mMethodOverride(m_methods, IsPwBindingTimeOptimization);

        /* Warm restore */
        mMethodOverride(m_methods, PwLabelGetFromHwStorage);
        mMethodOverride(m_methods, PwLabelSaveToHwStorage);
        mMethodOverride(m_methods, PwEthPortIdGetFromHwStorage);
        mMethodOverride(m_methods, PwEthPortIdSaveToHwStorage);
        mMethodOverride(m_methods, PwEthPortIdHwStorageClear);
        mMethodOverride(m_methods, De1BoundPwIdGetFromHwStorage);
        mMethodOverride(m_methods, De1BoundPwIdSaveToHwStorage);
        mMethodOverride(m_methods, De1BoundPwIdHwStorageClear);
        mMethodOverride(m_methods, PwEnableSaveToHwStorage);
        mMethodOverride(m_methods, PwEnableGetFromHwStorage);
        mMethodOverride(m_methods, MefOverMplsIsSupported);
        mMethodOverride(m_methods, PwBufferSizeDisparitySaveToHwStorage);
        mMethodOverride(m_methods, PwBufferSizeDisparityGetFromHwStorage);
        mMethodOverride(m_methods, PwBufferDelayDisparitySaveToHwStorage);
        mMethodOverride(m_methods, PwBufferDelayDisparityGetFromHwStorage);
        mMethodOverride(m_methods, HeaderControllerObjectCreate);
        mMethodOverride(m_methods, BackupHeaderControllerObjectCreate);
        mMethodOverride(m_methods, CanCheckPwPayloadSize);
        mMethodOverride(m_methods, ResetPw);
        mMethodOverride(m_methods, PwAdapterHwId);
        mMethodOverride(m_methods, HwPwId2Adapter);
        mMethodOverride(m_methods, PwDebuggerCreate);
        mMethodOverride(m_methods, DefaultCounterModule);
        mMethodOverride(m_methods, PwEthPortBinderCreate);
        mMethodOverride(m_methods, NumPwsSupportCounters);
        mMethodOverride(m_methods, InterruptProcessorCreate);
        mMethodOverride(m_methods, DefaultDefectModule);
        mMethodOverride(m_methods, DebugDefectControllerCreate);
        mMethodOverride(m_methods, DebugIntrProcessorCreate);
        mMethodOverride(m_methods, PwKBytePwGet);
        mMethodOverride(m_methods, NeedDoReConfig);
        mMethodOverride(m_methods, TxSubportVlanSet);
        mMethodOverride(m_methods, TxSubportVlanGet);
        mMethodOverride(m_methods, ExpectedSubportVlanSet);
        mMethodOverride(m_methods, ExpectedSubportVlanGet);
        mMethodOverride(m_methods, NumSubPortVlans);
        mMethodOverride(m_methods, SubPortVlanInsertionEnable);
        mMethodOverride(m_methods, SubPortVlanInsertionIsEnabled);
        mMethodOverride(m_methods, SubPortVlanCheckingEnable);
        mMethodOverride(m_methods, SubPortVlanCheckingIsEnabled);
        mMethodOverride(m_methods, TxPwSubPortVlanIndexSet);
        mMethodOverride(m_methods, TxPwSubPortVlanIndexGet);
        mMethodOverride(m_methods, DebugCountersModuleIsSupported);
        mMethodOverride(m_methods, ReferenceCircuitToConfigureHardware);
        mMethodOverride(m_methods, SubportVlansSupported);
        mMethodOverride(m_methods, TxActiveForceSupported);
        mMethodOverride(m_methods, NeedClearDebug);
        }

    mMethodsSet(self, &m_methods);
    }

uint8 *ThaModulePwSharedHeaderBuffer(ThaModulePw self, uint32 *bufferSize)
    {
    if (self == NULL)
        return NULL;

    if (bufferSize)
        *bufferSize = cThaMaxPwPsnHdrLen;

    return self->headerBuffer;
    }

uint8 *ThaModulePwSharedPsnBuffer(ThaModulePw self, uint32 *bufferSize)
    {
    if (self == NULL)
        return NULL;

    if (bufferSize)
        *bufferSize = cThaMaxPwPsnHdrLen;

    return self->psnBuffer;
    }

AtModulePw ThaModulePwObjectInit(AtModulePw self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtModulePwObjectInit((AtModulePw)self, device) == NULL)
        return NULL;

    /* Setup class */
    Override((ThaModulePw)self);
    MethodsInit((ThaModulePw)self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->countersModule = cAtModulePw;

    return self;
    }

AtModulePw ThaModulePwNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePw newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ThaModulePwObjectInit(newModule, device);
    }

uint8 ThaModulePwNumParts(ThaModulePw self)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    return ThaDeviceNumPartsOfModule(device, cAtModulePw);
    }

uint16 ThaModulePwNumPwsPerPart(ThaModulePw self)
    {
    if (self)
        return mMethodsGet(self)->NumPwsPerPart(self);
    return 0;
    }

uint8 ThaModulePwPartOfPw(ThaModulePw self, AtPw adapter)
    {
    uint32 numPwPerPart = ThaModulePwNumPwsPerPart(self);
    uint32 pwId = AtChannelIdGet((AtChannel)adapter);

    if (numPwPerPart == 0)
        return 0;

    return (uint8)(pwId / numPwPerPart);
    }

uint32 ThaModulePwLocalPwId(ThaModulePw self, AtPw adapter)
    {
    if (self)
        return mMethodsGet(self)->LocalPwId(self, adapter);

    return 0;
    }

eThaPwLookupMode ThaModulePwLookupModeGet(ThaModulePw self, AtPw pw)
    {
    return ThaClaPwControllerLookupModeGet(ClaPwController(self), pw);
    }

eAtRet ThaModulePwLookupModeSet(ThaModulePw self, AtPw pw, eThaPwLookupMode lookupMode)
    {
    return ThaClaPwControllerLookupModeSet(ClaPwController(self), pw, lookupMode);
    }

eBool ThaModulePwDynamicPwAllocation(ThaModulePw self)
    {
    if (self)
        return mMethodsGet(self)->DynamicPwAllocation(self);
    return cAtFalse;
    }

ThaPwActivator ThaModulePwActivator(ThaModulePw self)
    {
    if (self == NULL)
        return NULL;

    if (self->pwActivator == NULL)
        self->pwActivator = mMethodsGet(self)->PwActivatorCreate(self);

    return self->pwActivator;
    }

/* The following function should be faster. */
uint32 ThaModulePwFreePwGet(ThaModulePw self)
    {
    uint32 i;
    AtModulePw pwModule = (AtModulePw)self;
    uint32 maxNumPws = AtModulePwMaxPwsGet((AtModulePw)self);

    for (i = 0; i < maxNumPws; i++)
        {
        if (AtModulePwGetPw(pwModule, i) == NULL)
            return i;
        }

    return maxNumPws;
    }

eBool ThaModulePwCasIsSupported(ThaModulePw self)
    {
    if (self)
        return mMethodsGet(self)->CasIsSupported(self);
    return cAtFalse;
    }

eBool ThaModulePwHspwIsSupported(ThaModulePw self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    uint32 startSupportedVersion;

    if (AtDeviceAllFeaturesAvailableInSimulation(device))
        return cAtTrue;

    startSupportedVersion = mMethodsGet(self)->StartVersionSupportHspw(self);
    return (AtDeviceVersionNumber(device) >= startSupportedVersion) ? cAtTrue : cAtFalse;
    }

eBool ThaModulePwEnableErrorCheckingByDefault(ThaModulePw self)
    {
    if (self)
        return mMethodsGet(self)->EnableErrorCheckingByDefault(self);
    return cAtFalse;
    }

uint32 ThaPwPartOffset(AtPw pw, eAtModule moduleId)
    {
    ThaDevice device     = (ThaDevice)AtChannelDeviceGet((AtChannel)pw);
    ThaModulePw pwModule = (ThaModulePw)AtChannelModuleGet((AtChannel)pw);
    return ThaDeviceModulePartOffset(device, moduleId, ThaModulePwPartOfPw(pwModule, pw));
    }

eBool ThaModulePwIdleCodeIsSupported(ThaModulePw self)
    {
    if (self)
        return mMethodsGet(self)->PwIdleCodeIsSupported(self);
    return cAtFalse;
    }

eBool ThaModulePwMbitCounterIsSupported(ThaModulePw self)
    {
    if (self)
        return mMethodsGet(self)->PwMbitCounterIsSupported(self);
    return cAtFalse;
    }

ThaPwHeaderProvider ThaModulePwHeaderProviderGet(ThaModulePw self)
    {
    if (self == NULL)
        return NULL;

    if (self->pwHeaderProvider == NULL)
        self->pwHeaderProvider = mMethodsGet(self)->PwHeaderProviderCreate(self);

    return self->pwHeaderProvider;
    }

eAtRet ThaModulePwDidChangeCVlanOnPw(ThaModulePw self, AtPw pwAdapter, const tAtEthVlanTag *cVlan)
    {
    if (self)
        return mMethodsGet(self)->DidChangeCVlanOnPw(self, pwAdapter, cVlan);

    return cAtError;
    }

ThaPwDefectController ThaModulePwDefectControllerCreate(ThaModulePw self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->DefectControllerCreate(self, pw);

    return NULL;
    }

AtPwCep ThaModulePwCepObjectCreate(AtModulePw self, uint32 pwId, eAtPwCepMode mode)
    {
    return CepObjectCreate(self, pwId, mode);
    }

uint32 ThaModulePwLabelGetFromHwStorage(ThaModulePw self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwLabelGetFromHwStorage(self, pw);
    return 0;
    }

eAtRet ThaModulePwLabelSaveToHwStorage(ThaModulePw self, AtPw pw, uint32 pwLabel)
    {
    if (self)
        return mMethodsGet(self)->PwLabelSaveToHwStorage(self, pw, pwLabel);
    return cAtErrorNullPointer;
    }

uint8 ThaModulePwEthPortIdGetFromHwStorage(ThaModulePw self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwEthPortIdGetFromHwStorage(self, pw);
    return 0;
    }

eAtRet ThaModulePwEthPortIdSaveToHwStorage(ThaModulePw self, AtPw pw, uint8 ethPortId)
    {
    if (self)
        return mMethodsGet(self)->PwEthPortIdSaveToHwStorage(self, pw, ethPortId);
    return cAtErrorNullPointer;
    }

eAtRet ThaModulePwEthPortIdHwStorageClear(ThaModulePw self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwEthPortIdHwStorageClear(self, pw);
    return cAtErrorNullPointer;
    }

uint16 ThaModulePwDe1BoundPwIdGetFromHwStorage(ThaModulePw self, AtPdhDe1 de1)
    {
    if (self)
        return mMethodsGet(self)->De1BoundPwIdGetFromHwStorage(self, de1);
    return 0;
    }

eAtRet ThaModulePwDe1BoundPwIdSaveToHwStorage(ThaModulePw self, AtPdhDe1 de1, uint16 pwHwId)
    {
    if (self)
        return mMethodsGet(self)->De1BoundPwIdSaveToHwStorage(self, de1, pwHwId);
    return cAtErrorNullPointer;
    }

eAtRet ThaModulePwDe1BoundPwIdHwStorageClear(ThaModulePw self, AtPdhDe1 de1)
    {
    if (self)
        return mMethodsGet(self)->De1BoundPwIdHwStorageClear(self, de1);
    return cAtErrorNullPointer;
    }

eBool ThaModulePwMefOverMplsIsSupported(ThaModulePw self)
    {
    if (self)
        return mMethodsGet(self)->MefOverMplsIsSupported(self);
    return cAtFalse;
    }

eBool ThaModulePwPwEnableGetFromHwStorage(ThaModulePw self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwEnableGetFromHwStorage(self, pw);
    return cAtFalse;
    }

eAtRet ThaModulePwPwEnableSaveToHwStorage(ThaModulePw self, AtPw pw, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->PwEnableSaveToHwStorage(self, pw, enable);
    return cAtErrorNullPointer;
    }

eAtRet ThaModulePwSendLbitPacketsToCdrEnable(AtModulePw self, eBool enable)
    {
    return ThaClaPwControllerSendLbitPacketToCdrEnable(ClaPwController(mThis(self)), enable);
    }

eBool ThaModulePwSendLbitPacketsToCdrIsEnabled(AtModulePw self)
    {
    return ThaClaPwControllerSendLbitPacketToCdrIsEnabled(ClaPwController(mThis(self)));
    }

eAtRet ThaModulePwBufferSizeDisparitySaveToHwStorage(ThaModulePw self, AtPw pw, int disparityInUs)
    {
    if (self)
        return mMethodsGet(self)->PwBufferSizeDisparitySaveToHwStorage(self, pw, disparityInUs);
    return cAtErrorNullPointer;
    }

int ThaModulePwBufferSizeDisparityGetFromHwStorage(ThaModulePw self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwBufferSizeDisparityGetFromHwStorage(self, pw);
    return 0;
    }

eAtRet ThaModulePwBufferDelayDisparitySaveToHwStorage(ThaModulePw self, AtPw pw, int disparityInUs)
    {
    if (self)
        return mMethodsGet(self)->PwBufferDelayDisparitySaveToHwStorage(self, pw, disparityInUs);
    return cAtErrorNullPointer;
    }

int ThaModulePwBufferDelayDisparityGetFromHwStorage(ThaModulePw self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwBufferDelayDisparityGetFromHwStorage(self, pw);
    return 0;
    }

ThaPwHeaderController ThaModulePwHeaderControllerObjectCreate(ThaModulePw self, AtPw adapter)
    {
    if (self)
        return mMethodsGet(self)->HeaderControllerObjectCreate(self, adapter);
    return NULL;
    }

ThaPwHeaderController ThaModulePwBackupHeaderControllerObjectCreate(ThaModulePw self, AtPw adapter)
    {
    if (self)
        return mMethodsGet(self)->BackupHeaderControllerObjectCreate(self, adapter);
    return NULL;
    }

eBool ThaModulePwCanCheckPwPayloadSize(ThaModulePw self)
    {
    if (self)
        return mMethodsGet(self)->CanCheckPwPayloadSize(self);
    return cAtFalse;
    }

uint32 ThaModulePwDefectBaseAddress(ThaModulePw self)
    {
    if (self)
        return DefectBaseAddress(self);
    return 0x0;
    }

uint32 ThaModulePwDefectSliceInterruptEnable(ThaModulePw self, eBool enable)
    {
    return ThaPwInterruptProcessorEnable(ThaModulePwInterruptProcessor(self), enable);
    }

uint32 ThaModulePwDefectCurrentStatusRegister(ThaModulePw self)
    {
    ThaPwInterruptProcessor processor = ThaModulePwInterruptProcessor(self);
    return ThaPwInterruptProcessorBaseAddress(processor) + ThaPwInterruptProcessorCurrentStatusRegister(processor);
    }

uint32 ThaModulePwDefectInterruptStatusRegister(ThaModulePw self)
    {
    ThaPwInterruptProcessor processor = ThaModulePwInterruptProcessor(self);
    return ThaPwInterruptProcessorBaseAddress(processor) + ThaPwInterruptProcessorInterruptStatusRegister(processor);
    }

uint32 ThaModulePwDefectInterruptMaskRegister(ThaModulePw self)
    {
    ThaPwInterruptProcessor processor = ThaModulePwInterruptProcessor(self);
    return ThaPwInterruptProcessorBaseAddress(processor) + ThaPwInterruptProcessorInterruptMaskRegister(processor);
    }

eAtRet ThaModulePwDebugCountersModuleSet(ThaModulePw self, uint32 module)
    {
    if (self)
        return DebugCountersModuleSet(self, module);
    return cAtErrorNullPointer;
    }

uint32 ThaModulePwDebugCountersModuleGet(ThaModulePw self)
    {
    return self ? self->countersModule : cAtModuleStart;
    }

eBool ThaModulePwShouldReadAllCountersFromSurModule(ThaModulePw self)
    {
    if (self == NULL)
        return cAtFalse;

    if (ThaModulePwDebugCountersModuleGet(self) == cAtModuleSur)
        return cAtTrue;

    return cAtFalse;
    }

ThaPwDebugger ThaModulePwPwDebuggerGet(ThaModulePw self)
    {
    if (self->pwDebugger)
        return self->pwDebugger;

    self->pwDebugger = mMethodsGet(self)->PwDebuggerCreate(self);
    return self->pwDebugger;
    }

ThaPwEthPortBinder ThaModulePwEthPortBinder(ThaModulePw self)
    {
    if (self == NULL)
        return NULL;

    if (self->pwEthPortBinder == NULL)
        self->pwEthPortBinder = mMethodsGet(self)->PwEthPortBinderCreate(self);

    return self->pwEthPortBinder;
    }

eAtRet ThaModulePwJitterEmptyTimeoutSet(ThaModulePw self, uint32 timeoutMs)
    {
    if (self)
        return JitterEmptyTimeoutSet(self, timeoutMs);
    return cAtErrorNullPointer;
    }

uint32 ThaModulePwJitterEmptyTimeoutGet(ThaModulePw self)
    {
    return self ? self->jitterEmptyTimeout : 0;
    }

ThaPwInterruptProcessor ThaModulePwInterruptProcessor(ThaModulePw self)
    {
    if (self == NULL)
        return NULL;

    if (mMethodsGet(self)->DefaultDefectModule(self) != self->alarmModule)
        return DebugInterruptProcessor(self);

    return InterruptProcessor(self);
    }

eBool ThaModulePwShouldStrictlyLimitResources(ThaModulePw self)
    {
    return ThaPwEthPortBinderNeedToManageEthPortBandwidth(ThaModulePwEthPortBinder(self));
    }

void ThaModulePwPwEnablePrepare(ThaModulePw self, AtPw adapter)
    {
    if (self)
        mMethodsGet(self)->PwEnablePrepare(self, adapter);
    }

void ThaModulePwPwEnableFinish(ThaModulePw self, AtPw adapter)
    {
    if (self)
        mMethodsGet(self)->PwEnableFinish(self, adapter);
    }

void ThaModulePwPwDisablePrepare(ThaModulePw self, AtPw adapter)
    {
    if (self == NULL)
        return;

    if (AtModuleInAccessible((AtModule)self))
        return;

    mMethodsGet(self)->PwDisablePrepare(self, adapter);
    }

void ThaModulePwPwDisableFinish(ThaModulePw self, AtPw adapter)
    {
    if (self == NULL)
        return;

    if (AtModuleInAccessible((AtModule)self))
        return;

    mMethodsGet(self)->PwDisableFinish(self, adapter);
    }

eAtRet ThaModulePwPwFinishRemoving(ThaModulePw self, AtPw adapter)
    {
    if (self)
        return mMethodsGet(self)->PwFinishRemoving(self, adapter);
    return cAtErrorNullPointer;
    }

eAtRet ThaModulePwPwFinishRemovingForBinding(ThaModulePw self, AtPw adapter)
    {
    if (self)
        return mMethodsGet(self)->PwFinishRemovingForBinding(self, adapter);
    return cAtErrorNullPointer;
    }

eAtRet ThaModulePwIsPwBindingTimeOptimization(ThaModulePw self)
    {
    if (self)
        return mMethodsGet(self)->IsPwBindingTimeOptimization(self);
    return cAtErrorNullPointer;
    }

void ThaModulePwResourcesLimitationDisable(ThaModulePw self, eBool isDisabled)
    {
    if (self)
        self->disableResourcesLimitation = isDisabled;
    }

eBool ThaModulePwResourcesLimitationIsDisabled(ThaModulePw self)
    {
    if (self)
        return self->disableResourcesLimitation;

    return cAtFalse;
    }

eBool ThaModulePwTimingModeIsApplicableForEPAR(ThaModulePw self, eAtTimingMode timingMode)
    {
    if (self)
        return mMethodsGet(self)->TimingModeIsApplicableForEPAR(self, timingMode);
    return cAtFalse;
    }

ThaPwDefectController ThaModulePwDebugDefectControllerCreate(ThaModulePw self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->DebugDefectControllerCreate(self, pw);

    return NULL;
    }

uint32 ThaModulePwDebugAlarmModuleGet(ThaModulePw self)
    {
    return self ? self->alarmModule : cAtModuleStart;
    }

eAtRet ThaModulePwDebugAlarmModuleSet(ThaModulePw self, uint32 module)
    {
    if ((module != cAtModulePw) && (module != cAtModuleSur))
        return cAtErrorModeNotSupport;

    if (self->alarmModule == module)
        return cAtOk;

    if (module == cAtModuleSur)
        {
        if (!ThaDeviceSurveillanceSupported((ThaDevice)AtModuleDeviceGet((AtModule)self)))
            return cAtErrorModeNotSupport;
        }

    if (module == mMethodsGet(self)->DefaultDefectModule(self))
        SwitchAllPwToDefaultDefectController((AtModulePw)self);
    else
        SwitchAllPwToDebugDefectController((AtModulePw)self);

    self->alarmModule = module;
    return cAtOk;
    }

uint32 ThaModulePwPwAdapterHwId(ThaModulePw self, AtPw adapter)
    {
    if (self)
        return mMethodsGet(self)->PwAdapterHwId(self, (ThaPwAdapter)adapter);

    return cInvalidUint32;
    }

ThaPwAdapter ThaModulePwHwPwId2Adapter(ThaModulePw self, uint32 hwId)
    {
    if (self)
        return mMethodsGet(self)->HwPwId2Adapter(self, hwId);
    return NULL;
    }

eBool ThaModulePwCanBindEthPort(ThaModulePw self, AtPw pw, AtEthPort port)
    {
    if (self)
        return mMethodsGet(self)->CanBindEthPort(self, pw, port);
    return cAtFalse;
    }

eBool ThaModulePwNeedDoReConfig(ThaModulePw self)
    {
    if (self)
        return mMethodsGet(self)->NeedDoReConfig(self);
    return cAtFalse;
    }

eBool ThaModulePwCanBindEthFlow(ThaModulePw self, AtPw pw, AtEthFlow flow)
    {
    if (self)
        return mMethodsGet(self)->CanBindEthFlow(self, pw, flow);
    return cAtFalse;
    }

AtPw ThaModulePwKBytePwGet(ThaModulePw self)
    {
    if (self)
        return mMethodsGet(self)->PwKBytePwGet(self);
    return NULL;
    }

eAtRet ThaModulePwTxSubportVlanSet(ThaModulePw self, uint32 subPortVlanIndex, const tAtVlan *vlan)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (!SubPortVlanIndexIsInRange(self, subPortVlanIndex))
        return cAtErrorOutOfRangParm;

    if (VlanIsValid(self, vlan))
        return mMethodsGet(self)->TxSubportVlanSet(self, subPortVlanIndex, vlan);

    return cAtErrorInvlParm;
    }

eAtRet ThaModulePwTxSubportVlanGet(ThaModulePw self, uint32 subPortVlanIndex, tAtVlan *vlan)
    {
    if (self == NULL)
        return cAtErrorNullPointer;
    if (vlan == NULL)
        return cAtErrorNullPointer;

    if (SubPortVlanIndexIsInRange(self, subPortVlanIndex))
        return mMethodsGet(self)->TxSubportVlanGet(self, subPortVlanIndex, vlan);

    return cAtErrorOutOfRangParm;
    }

eAtRet ThaModulePwExpectedSubportVlanSet(ThaModulePw self, uint32 subPortVlanIndex, const tAtVlan *expectedVlan)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (!SubPortVlanIndexIsInRange(self, subPortVlanIndex))
        return cAtErrorOutOfRangParm;

    if (VlanIsValid(self, expectedVlan))
        return mMethodsGet(self)->ExpectedSubportVlanSet(self, subPortVlanIndex, expectedVlan);

    return cAtErrorInvlParm;
    }

eAtRet ThaModulePwExpectedSubportVlanGet(ThaModulePw self, uint32 subPortVlanIndex, tAtVlan *expectedVlan)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (expectedVlan == NULL)
        return cAtErrorNullPointer;

    if (SubPortVlanIndexIsInRange(self, subPortVlanIndex))
        return mMethodsGet(self)->ExpectedSubportVlanGet(self, subPortVlanIndex, expectedVlan);

    return cAtErrorOutOfRangParm;
    }

uint32 ThaModulePwNumSubPortVlans(ThaModulePw self)
    {
    if (self)
        return mMethodsGet(self)->NumSubPortVlans(self);
    return 0;
    }

eAtRet ThaModulePwSubPortVlanInsertionEnable(ThaModulePw self, eBool enabled)
    {
    if (self)
        return mMethodsGet(mThis(self))->SubPortVlanInsertionEnable(self, enabled);
    return cAtErrorNullPointer;
    }

eBool ThaModulePwSubPortVlanInsertionIsEnabled(ThaModulePw self)
    {
    if (self)
        return mMethodsGet(mThis(self))->SubPortVlanInsertionIsEnabled(self);
    return cAtFalse;
    }

eAtRet ThaModulePwSubPortVlanCheckingEnable(ThaModulePw self, eBool enabled)
    {
    if (self)
        return mMethodsGet(mThis(self))->SubPortVlanCheckingEnable(self, enabled);
    return cAtErrorNullPointer;
    }

eBool ThaModulePwSubPortVlanCheckingIsEnabled(ThaModulePw self)
    {
    if (self)
        return mMethodsGet(mThis(self))->SubPortVlanCheckingIsEnabled(self);
    return cAtFalse;
    }

eAtRet ThaModulePwSubPortVlanDebug(ThaModulePw self)
    {
    if (self)
        return SubPortVlanDebug(self);
    return cAtErrorNullPointer;
    }

eAtRet ThaModulePwTxPwSubPortVlanIndexSet(ThaModulePw self, AtPw adapter, uint32 subPortVlanIndex)
    {
    if (self == NULL)
        return cAtErrorObjectNotExist;

    if (subPortVlanIndex < ThaModulePwNumSubPortVlans(self))
        return mMethodsGet(self)->TxPwSubPortVlanIndexSet(self, adapter, subPortVlanIndex);

    return cAtErrorInvlParm;
    }

uint32 ThaModulePwTxPwSubPortVlanIndexGet(ThaModulePw self, AtPw adapter)
    {
    if (self)
        return mMethodsGet(mThis(self))->TxPwSubPortVlanIndexGet(self, adapter);
    return cInvalidUint32;
    }

eAtRet ThaModulePwHitlessHeaderChangeEnable(ThaModulePw self, eBool enabled)
    {
    if (self)
        return HitlessHeaderChangeEnable(self, enabled);
    return cAtErrorNullPointer;
    }

eBool ThaModulePwHitlessHeaderChangeIsEnabled(ThaModulePw self)
    {
    if (self)
        return HitlessHeaderChangeIsEnabled(self);
    return cAtFalse;
    }

AtChannel ThaModulePwReferenceCircuitToConfigureHardware(ThaModulePw self, ThaPwAdapter adapter, AtChannel circuit)
    {
    if (self)
        return mMethodsGet(self)->ReferenceCircuitToConfigureHardware(self, adapter, circuit);
    return NULL;
    }

eBool ThaModulePwSubportVlansSupported(ThaModulePw self)
    {
    if (self)
        return mMethodsGet(self)->SubportVlansSupported(self);
    return cAtFalse;
    }

eBool ThaModulePwTxActiveForceSupported(ThaModulePw self)
    {
    if (self)
        return mMethodsGet(self)->TxActiveForceSupported(self);
    return cAtFalse;
    }

uint32 ThaModulePwMinRtpPayloadType(void)
    {
    return 96;
    }

uint32 ThaModulePwMaxRtpPayloadType(void)
    {
    return 127;
    }

eBool ThaModulePwRtpPayloadTypeIsInRange(uint32 payloadType)
    {
    if (mOutOfRange(payloadType, ThaModulePwMinRtpPayloadType(), ThaModulePwMaxRtpPayloadType()))
        return cAtFalse;
    return cAtTrue;
    }

eBool ThaModulePwShouldHandleCircuitAisForcingOnEnabling(ThaModulePw self)
    {
    if (self)
        return mMethodsGet(self)->ShouldHandleCircuitAisForcingOnEnabling(self);
    return cAtFalse;
    }

eBool ThaModulePwShouldMaskDefectAndFailureOnPwDisabling(ThaModulePw self)
    {
    if (self)
        return mMethodsGet(self)->ShouldMaskDefectAndFailureOnPwDisabling(self);
    return cAtFalse;
    }

eBool ThaModulePwNeedClearDebug(ThaModulePw self)
    {
    if (self)
        return mMethodsGet(self)->NeedClearDebug(self);
    return cAtFalse;
    }

eAtRet ThaModulePwInterruptEnable(ThaModulePw self, eBool enable)
    {
    ThaPwInterruptProcessor processor = ThaModulePwInterruptProcessor(self);
    if (processor)
        return ThaPwInterruptProcessorEnable(processor, enable);
    return cAtOk;
    }

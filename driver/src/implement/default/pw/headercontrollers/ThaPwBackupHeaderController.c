/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : ThaPwBackupHeaderController.c
 *
 * Created Date: Oct 13, 2015
 *
 * Description : Backup header controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPwGroup.h"
#include "../../../../generic/man/AtDriverInternal.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../cla/hbce/ThaHbceMemoryCell.h"
#include "../../cla/hbce/ThaHbceMemoryCellContent.h"
#include "ThaPwHeaderControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods              m_AtObjectOverride;
static tThaPwHeaderControllerMethods m_ThaPwHeaderControllerOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPwGroup HsPwGroup(ThaPwHeaderController self)
    {
    ThaPwAdapter adapter = ThaPwHeaderControllerAdapterGet(self);
    AtPw pw = ThaPwAdapterPwGet(adapter);
    return AtPwHsGroupGet(pw);
    }

static eBool NeedProtectOnTxDirection(ThaPwHeaderController self)
    {
    AtPwGroup group = HsPwGroup(self);
    eAtPwGroupLabelSet labelSet;

    /* If PW does not join to HS group, configure primary PSN need to be protected */
    if (group == NULL)
        return cAtTrue;

    labelSet = AtPwGroupTxSelectedLabelGet(group);
    return (labelSet == cAtPwGroupLabelSetPrimary) ? cAtFalse : cAtTrue;
    }

static eBool NeedProtectOnRxDirection(ThaPwHeaderController self)
    {
    AtPwGroup group = HsPwGroup(self);
    eAtPwGroupLabelSet labelSet;

    /* If PW does not join to HS group, configure backup PSN does not need to be protected */
    if (group == NULL)
        return cAtFalse;

    labelSet = AtPwGroupRxSelectedLabelGet(group);
    return (labelSet == cAtPwGroupLabelSetPrimary) ? cAtFalse : cAtTrue;
    }

static void Debug(ThaPwHeaderController self)
    {
    ThaHbceMemoryCell cell = ThaHbceMemoryCellContentCellGet(self->hbceMemoryCellContent);

    AtPrintc(cSevNormal, "* Backup HBCE Activated: %s\r\n", self->hbceActivated ? "YES" : "NO");
    AtPrintc(cSevNormal, "* Backup HBCE memory cell %u: %s\r\n",
             ThaHbceMemoryCellIndexGet(cell),
             AtObjectToString((AtObject)cell));
    }

static eBool IsPrimary(ThaPwHeaderController self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModulePwRet PsnTypeSet(ThaPwHeaderController self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtModulePwRet HeaderLengthSet(ThaPwHeaderController self, uint8 hdrLenInByte)
    {
    AtUnused(self);
    AtUnused(hdrLenInByte);

    /* HW provides only one field for both primary and backup header length.
     * Backup header length needs to be configured as SW data-base
     * to not depend on HW field when primary header length changed first */
    ((tThaPwBackupHeaderController *)self)->headerLength = hdrLenInByte;
    return cAtOk;
    }

static uint8 HeaderLengthGet(ThaPwHeaderController self)
    {
    return ((tThaPwBackupHeaderController *)self)->headerLength;
    }

static tAtEthVlanTag* StandbyEthCVlanGet(ThaPwHeaderController self, tAtEthVlanTag *cVlan)
    {
    AtPw pw = (AtPw)ThaPwHeaderControllerAdapterGet(self);
    return AtPwBackupEthCVlanGet(pw, cVlan);
    }

static tAtEthVlanTag* StandbyEthSVlanGet(ThaPwHeaderController self, tAtEthVlanTag *sVlan)
    {
    AtPw pw = (AtPw)ThaPwHeaderControllerAdapterGet(self);
    return AtPwBackupEthSVlanGet(pw, sVlan);
    }

static eAtRet NumVlansSet(ThaPwHeaderController self, uint8 numVlans)
    {
    AtUnused(self);
    AtUnused(numVlans);
    return cAtOk;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tThaPwBackupHeaderController *object = (tThaPwBackupHeaderController *)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeUInt(headerLength);
    }

static void OverrideAtObject(ThaPwHeaderController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideThaPwHeaderController(ThaPwHeaderController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwHeaderControllerOverride, mMethodsGet(self), sizeof(m_ThaPwHeaderControllerOverride));

        mMethodOverride(m_ThaPwHeaderControllerOverride, NeedProtectOnTxDirection);
        mMethodOverride(m_ThaPwHeaderControllerOverride, NeedProtectOnRxDirection);
        mMethodOverride(m_ThaPwHeaderControllerOverride, Debug);
        mMethodOverride(m_ThaPwHeaderControllerOverride, IsPrimary);
        mMethodOverride(m_ThaPwHeaderControllerOverride, PsnTypeSet);
        mMethodOverride(m_ThaPwHeaderControllerOverride, HeaderLengthSet);
        mMethodOverride(m_ThaPwHeaderControllerOverride, HeaderLengthGet);
        mMethodOverride(m_ThaPwHeaderControllerOverride, StandbyEthCVlanGet);
        mMethodOverride(m_ThaPwHeaderControllerOverride, StandbyEthSVlanGet);
        mMethodOverride(m_ThaPwHeaderControllerOverride, NumVlansSet);
        }

    mMethodsSet(self, &m_ThaPwHeaderControllerOverride);
    }

static void Override(ThaPwHeaderController self)
    {
    OverrideAtObject(self);
    OverrideThaPwHeaderController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPwBackupHeaderController);
    }

ThaPwHeaderController ThaPwBackupHeaderControllerObjectInit(ThaPwHeaderController self, ThaPwAdapter adapter)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPwHeaderControllerObjectInit(self, adapter) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

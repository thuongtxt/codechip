/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : ThaPwPsnController.c
 *
 * Created Date: May 9, 2015
 *
 * Description : PW header controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/pw/psn/AtPwPsnInternal.h"
#include "../../cos/ThaModuleCosInternal.h"
#include "../../eth/ThaModuleEth.h"
#include "../ThaPwInternal.h"
#include "../adapters/ThaPwAdapterInternal.h"
#include "../headerprovider/ThaPwHeaderProviderInternal.h"
#include "../ThaModulePwInternal.h"
#include "../../util/ThaUtil.h"
#include "ThaPwHeaderControllerInternal.h"
#include "../../../../util/coder/AtCoderUtil.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((ThaPwHeaderController)self)
#define cEthTypeSizeInBytes  2

/*--------------------------- Macros -----------------------------------------*/
#define mDevice(self) AtChannelDeviceGet((AtChannel)self)

#define mCacheEncodeUInt(fieldName) AtCoderEncodeUInt(encoder, cache->fieldName, "cache."#fieldName)

#define mEncodeVlanCache(fieldName)                                            \
    do                                                                         \
        {                                                                      \
        AtCoderEncodeUInt(encoder, cache->fieldName.priority, "cache."#fieldName".priority");\
        AtCoderEncodeUInt(encoder, cache->fieldName.cfi,      "cache."#fieldName".cfi");     \
        AtCoderEncodeUInt(encoder, cache->fieldName.vlanId,   "cache."#fieldName".vlanId");  \
        }while(0)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaPwHeaderControllerMethods m_methods;

/* Override */
static tAtObjectMethods          m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods    *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static tThaPwHeaderControllerSimulation *SimulationDatabaseCreate(ThaPwHeaderController self)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 memorySize = sizeof(tThaPwHeaderControllerSimulation);
    tThaPwHeaderControllerSimulation *database = mMethodsGet(osal)->MemAlloc(osal, memorySize);
    AtUnused(self);
    if (database)
        mMethodsGet(osal)->MemInit(osal, database, 0, memorySize);
    return database;
    }

static void SimulationDatabaseDelete(ThaPwHeaderController self)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemFree(osal, self->simulation);
    self->simulation = NULL;
    }

static tThaPwHeaderControllerSimulation *SimulationDatabase(ThaPwHeaderController self)
    {
    if (self->simulation == NULL)
        self->simulation = SimulationDatabaseCreate(self);
    return self->simulation;
    }

static eBool IsSimulated(ThaPwHeaderController self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)ThaPwHeaderControllerAdapterGet(self));
    return AtDeviceIsSimulated(device);
    }

static ThaModulePda ModulePda(ThaPwHeaderController self)
    {
    ThaPwAdapter adapter = ThaPwHeaderControllerAdapterGet(self);
    return (ThaModulePda)AtDeviceModuleGet(mDevice(adapter), cThaModulePda);
    }

static ThaModulePw ModulePw(ThaPwHeaderController self)
    {
    ThaPwAdapter adapter = ThaPwHeaderControllerAdapterGet(self);
    return (ThaModulePw)AtDeviceModuleGet(mDevice(adapter), cAtModulePw);
    }

static ThaModuleCos ModuleCos(ThaPwHeaderController self)
    {
    return (ThaModuleCos)AtDeviceModuleGet(mDevice(ThaPwHeaderControllerAdapterGet(self)), cThaModuleCos);
    }

static ThaModulePwe ModulePwe(ThaPwHeaderController self)
    {
    return (ThaModulePwe)AtDeviceModuleGet(mDevice(ThaPwHeaderControllerAdapterGet(self)), cThaModulePwe);
    }

static ThaModuleCla ModuleCla(ThaPwHeaderController self)
    {
    return (ThaModuleCla)AtDeviceModuleGet(mDevice(ThaPwHeaderControllerAdapterGet(self)), cThaModuleCla);
    }

static eBool IsMefOverMpls(AtPwPsn psn)
    {
    if (AtPwPsnTypeGet(psn) != cAtPwPsnTypeMef)
        return cAtFalse;

    return (AtPwPsnTypeGet(AtPwPsnLowerPsnGet(psn)) == cAtPwPsnTypeMpls) ? cAtTrue : cAtFalse;
    }

static eBool NeedProtectOnTxDirection(ThaPwHeaderController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool NeedProtectOnRxDirection(ThaPwHeaderController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtModulePwRet LogicPwPsnSet(ThaPwHeaderController self, AtPwPsn psn)
    {
    AtPwPsn newPsn = psn;
    tThaPwHeaderCache *cache = ThaPwHeaderControllerCache(self);

    if (cache->psn == NULL)
        newPsn = (AtPwPsn)AtObjectClone((AtObject)psn);
    else if (cache->psn != psn)
        {
        AtObjectDelete((AtObject)(cache->psn));
        newPsn = (AtPwPsn)AtObjectClone((AtObject)psn);
        }

    cache->psn = newPsn;
    return cAtOk;
    }

static eAtModulePwRet RemovePsn(ThaPwHeaderController self)
    {
    eAtRet ret = ThaPwHeaderControllerHbceDeactivate(self);
    if (ret != cAtOk)
        return ret;

    /* Reset cache */
    AtObjectDelete((AtObject)self->newPsn);
    self->newPsn     = NULL;
    self->currentPsn = NULL;
    return cAtOk;
    }

static eBool IsLogicPw(ThaPwHeaderController self)
    {
    return ThaPwAdapterIsLogicPw(ThaPwHeaderControllerAdapterGet(self));
    }

static ThaPwActivator PwActivator(ThaPwHeaderController self)
    {
    return ThaModulePwActivator(ModulePw(self));
    }

static eBool HbceActivated(ThaPwHeaderController self)
    {
    return self->hbceActivated;
    }

static uint8 PwEthHeaderLength(ThaPwHeaderController self)
    {
    /* Always have room for DA and SA */
    if (self->ethHdrLength == 0)
        self->ethHdrLength = cAtMacAddressLen * 2;

    return self->ethHdrLength;
    }

static uint8 *SharedHeaderBuffer(ThaPwHeaderController self, uint32 *bufferSize)
    {
    return ThaModulePwSharedHeaderBuffer(ModulePw(self), bufferSize);
    }

static uint8 *SharedPsnBuffer(ThaPwHeaderController self, uint32 *bufferSize)
    {
    return ThaModulePwSharedPsnBuffer(ModulePw(self), bufferSize);
    }

static eBool HeaderLengthIsChanged(ThaPwHeaderController self, uint8 hdrLenInByte)
    {
    uint8 currentHeaderLength = mMethodsGet(self)->HeaderLengthGet(self);
    return (hdrLenInByte == currentHeaderLength) ? cAtFalse : cAtTrue;
    }

static eBool ShouldAlwaysStartHwProtocol(ThaPwHeaderController self)
    {
    if (ThaModulePwHitlessHeaderChangeIsEnabled(ModulePw(self)))
        return cAtFalse;

    return cAtTrue;
    }

static eBool ShouldStartHwProtocolWithNewHeaderLength(ThaPwHeaderController self, uint8 newHdrLenInByte)
    {
    if (ShouldAlwaysStartHwProtocol(self))
        return cAtTrue;

    return HeaderLengthIsChanged(self, newHdrLenInByte) ? cAtTrue : cAtFalse;
    }

static uint8 HeaderLengthWithPsnLength(ThaPwHeaderController self, uint8 psnHdrLenInByte)
    {
    return (uint8)(psnHdrLenInByte + PwEthHeaderLength(self) + cEthTypeSizeInBytes);
    }

static eAtRet HwHeaderArraySet(ThaPwHeaderController self, uint8 *headerBuffer, uint8 hdrLenInByte, AtPwPsn psn, eThaPwAdapterHeaderWriteMode writeMode)
    {
    eAtRet ret = cAtOk;
    eBool needProtectOnTxDirection = mMethodsGet(self)->NeedProtectOnTxDirection(self);
    eBool needProtectOnRxDirection = mMethodsGet(self)->NeedProtectOnRxDirection(self);
    AtPw adapter = (AtPw)ThaPwHeaderControllerAdapterGet(self);

    if (!ShouldStartHwProtocolWithNewHeaderLength(self, hdrLenInByte))
        {
        needProtectOnTxDirection = cAtFalse;
        needProtectOnRxDirection = cAtFalse;
        }

    if (needProtectOnTxDirection)
        ThaPwAdapterPweStartRemoving(adapter);

    if (needProtectOnRxDirection)
        ThaPwAdapterStartCenterJitterBuffer(adapter);

    ret |= mMethodsGet(self)->HeaderLengthSet(self, hdrLenInByte);
    if (headerBuffer == NULL)
        headerBuffer = SharedHeaderBuffer(self, NULL);
    ret |= mMethodsGet(self)->HeaderWrite(self, headerBuffer, hdrLenInByte, psn, writeMode);

    if (needProtectOnTxDirection)
        ThaPwAdapterPweFinishRemoving(adapter);

    if (needProtectOnRxDirection)
        ThaPwAdapterStopCenterJitterBuffer(adapter);

    return ret;
    }

static eBool HeaderLengthIsValid(uint8 headerLength)
    {
    if (headerLength > cThaMaxPwPsnHdrLen)
        return cAtFalse;

    return cAtTrue;
    }

static eAtRet PwPsnHeaderArraySet(ThaPwHeaderController self, uint8* buffer, uint8 psnHdrLenInByte, AtPwPsn psn)
    {
    uint8 ethHdrLen = PwEthHeaderLength(self);
    uint8 newHdrLen = HeaderLengthWithPsnLength(self, psnHdrLenInByte);
    uint32 i;
    uint8 *headerBuffer;
    uint8 headerLenInByte;
    uint16 ethernetType;

    if (!HeaderLengthIsValid(newHdrLen))
        return cAtError;

    headerBuffer = ThaPwHeaderControllerHwHeaderArrayGet(self, &headerLenInByte);
    if (headerBuffer == NULL)
        return cAtError;

    ethernetType = ThaPktUtilEthTypeFromPsn(psn);
    headerBuffer[ethHdrLen] = (uint8)(ethernetType >> 8);
    headerBuffer[ethHdrLen + 1] = (uint8)(ethernetType & cBit7_0);

    for (i = ethHdrLen + (uint32)cEthTypeSizeInBytes; (i < newHdrLen) && (i < cThaMaxPwPsnHdrLen); i++)
        {
        headerBuffer[i] = *buffer;
        buffer++;
        }

    return HwHeaderArraySet(self, NULL, newHdrLen, psn, cThaPwAdapterHeaderWriteModeDefault);
    }

static eAtModulePwRet PsnHeaderSet(ThaPwHeaderController self, AtPwPsn psn)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    uint8 headerLen;
    eAtRet ret = cAtOk;
    uint32 bufferSize;
    uint8 *psnHeader = SharedPsnBuffer(self, &bufferSize);

    /* Build PW header Array */
    mMethodsGet(osal)->MemInit(osal, psnHeader, 0, sizeof(uint8) * bufferSize);
    headerLen = AtPwPsnHeaderBuild(psn, (AtPw)self, psnHeader);

    /* Configure to HW */
    ret |= PwPsnHeaderArraySet(self, psnHeader, headerLen, psn);

    return ret;
    }

static eBool PsnCanBeUsed(ThaPwHeaderController self, AtPwPsn psn)
    {
    ThaPwAdapter adapter = ThaPwHeaderControllerAdapterGet(self);

    /* This cannot be determined on standby driver. Application has to make sure
     * this */
    if (ThaPwHeaderControllerInAccessible(self))
        return cAtTrue;

    if (!ThaHbcePsnCanBeUsed(ThaPwAdapterHbceGet((AtPw)adapter), self, psn))
        return cAtFalse;

    /* There may be one deactivated PW is using this PSN label */
    if (ThaPwActivatorPsnIsUsed(PwActivator(self), adapter, psn))
        return cAtFalse;

    return cAtTrue;
    }

static eBool RxIsEnable(ThaPwHeaderController self)
    {
    ThaPwAdapter adapter = ThaPwHeaderControllerAdapterGet(self);
    return ThaPwAdapterClaIsEnabled((AtPw)adapter);
    }

static eAtRet RxEnable(ThaPwHeaderController self, eBool enable)
    {
    ThaPwAdapter adapter = ThaPwHeaderControllerAdapterGet(self);
    return ThaPwAdapterClaEnable((AtPw)adapter, enable);
    }

static eBool NeedDisableTxWhenChangePsn(ThaPwHeaderController self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool IsPsnTypeIdentical(AtPwPsn psn1, AtPwPsn psn2)
    {
    if ((psn1 == NULL) && (psn2 == NULL))
        return cAtTrue;

    if ((psn1 == NULL) || (psn2 == NULL))
        return cAtFalse;

    if (AtPwPsnTypeGet(psn1) != AtPwPsnTypeGet(psn2))
        return cAtFalse;

    return IsPsnTypeIdentical(AtPwPsnLowerPsnGet(psn1), AtPwPsnLowerPsnGet(psn2));
    }

static eBool SamePsnAndSameLabel(AtPwPsn psn1, AtPwPsn psn2)
    {
    if ((IsPsnTypeIdentical(psn1, psn2)) &&
        (AtPwPsnExpectedLabelGet(psn1) == AtPwPsnExpectedLabelGet(psn2)))
        return cAtTrue;

    return cAtFalse;
    }

static eAtModulePwRet PsnSet(ThaPwHeaderController self, AtPwPsn psn)
    {
    eAtRet ret;
    AtPwPsn newPsn = psn;
    eBool claEnabled;
    eBool pweEnabled = cAtFalse;
    ThaModulePda pdaModule = ModulePda(self);
    ThaModulePw pwModule = ModulePw(self);
    ThaModulePwe pweModule = ModulePwe(self);
    ThaClaController claController;
    eBool mefOverMpls = IsMefOverMpls(newPsn);
    AtPw adapter = (AtPw)ThaPwHeaderControllerAdapterGet(self);
    eBool needProtectOnTxDirection = mMethodsGet(self)->NeedProtectOnTxDirection(self);
    eBool needProtectOnRxDirection = mMethodsGet(self)->NeedProtectOnRxDirection(self);
    eBool needDisableTxWhenChangePsn = mMethodsGet(self)->NeedDisableTxWhenChangePsn(self);
    eBool shouldReapplyClaEnabling = cAtTrue;

    /* A none NULL PSN must be valid */
    if (psn && !AtPwPsnIsValid(psn))
        mChannelError(adapter, cAtErrorInvlParm);

    if (!PsnCanBeUsed(self, psn))
        return cAtErrorResourceBusy;

    if (mefOverMpls && (ThaModulePwMefOverMplsIsSupported(pwModule) == cAtFalse))
        return cAtErrorModeNotSupport;

    if (IsLogicPw(self))
        return LogicPwPsnSet(self, psn);

    /* Need to clone this PSN for internal usage */
    if (psn)
        newPsn = (AtPwPsn)AtObjectClone((AtObject)psn);

    /* Need to deactivate this PW if PSN is set to NULL */
    if (newPsn == NULL)
        {
        ret = RemovePsn(self);
        ret |= ThaModuleClaPwPsnHeaderUpdate(ModuleCla(self), adapter);
        return ret;
        }

    /*
     * Need to use cloned psn to compare, because there is case when higher apps get current PSN, then
     * modify something and set back to pw, call AtPwPsnGet again will overwrite what user have modified
     * at that time and "psn" and AtPwPsnGet(self) are always identical.
     */
    if ((self->pwPsnHeaderIsSet) && AtPwPsnIsIdentical(newPsn, ThaPwHeaderControllerPsnGet(self)))
        {
        AtObjectDelete((AtObject)newPsn);
        return cAtOk;
        }

    claEnabled = mMethodsGet(self)->RxIsEnable(self);

    if (!ShouldStartHwProtocolWithNewHeaderLength(self, HeaderLengthWithPsnLength(self, (uint8)AtPwPsnLengthInBytes(psn))))
        {
        needProtectOnTxDirection = cAtFalse;
        needProtectOnRxDirection = cAtFalse;
        needDisableTxWhenChangePsn = cAtFalse;

        if (SamePsnAndSameLabel(psn, ThaPwHeaderControllerPsnGet(self)))
            shouldReapplyClaEnabling = cAtFalse;
        }

    if (needDisableTxWhenChangePsn)
        pweEnabled = ThaModulePwePwIsEnabled(pweModule, adapter);

    if (needProtectOnTxDirection)
        ThaPwAdapterPweStartRemoving(adapter);

    if (needProtectOnRxDirection)
        {
        ThaPwAdapterStartCenterJitterBuffer(adapter);
        ThaModulePdaHotConfigureStart(pdaModule, adapter);
        }

    if (needDisableTxWhenChangePsn)
        ThaModulePwePwEnable(pweModule, adapter, cAtFalse);

    /* Try to apply new PSN */
    ret = PsnHeaderSet(self, newPsn);
    if (ret != cAtOk)
        {
        ret |= mMethodsGet(self)->RxEnable(self, claEnabled);

        if (needDisableTxWhenChangePsn)
            ret |= ThaModulePwePwEnable(pweModule, adapter, pweEnabled);

        AtObjectDelete((AtObject)newPsn);

        if (needProtectOnRxDirection)
            ThaModulePdaHotConfigureStop(pdaModule, adapter);

        mChannelLog(adapter, cAtLogLevelCritical, "Cannot apply new PSN header");
        return ret;
        }

    self->pwPsnHeaderIsSet = cAtTrue;

    /* Update database and activate PW */
    if (!HbceActivated(self) && (self->newPsn != newPsn))
        AtObjectDelete((AtObject)self->newPsn);
    self->newPsn = newPsn;

    ret |= ThaPwHeaderControllerHbceActivate(self);
    if (shouldReapplyClaEnabling)
        {
        claController = (ThaClaController)ThaPwAdapterClaPwController(mAdapter(adapter));
        ret |= ThaClaControllerMefOverMplsEnable(claController, adapter, mefOverMpls);
        ret |= mMethodsGet(self)->RxEnable(self, claEnabled);
        }

    if (needDisableTxWhenChangePsn)
        ret |= ThaModulePwePwEnable(pweModule, adapter, pweEnabled);

    if (needProtectOnRxDirection)
        {
        ThaModulePdaHotConfigureStop(pdaModule, adapter);
        ThaPwAdapterStopCenterJitterBuffer(adapter);
        }

    if (needProtectOnTxDirection)
        ThaPwAdapterPweFinishRemoving(adapter);

    /* Update CLA PSN header if needed */
    ret |= ThaModuleClaPwPsnHeaderUpdate(ModuleCla(self), adapter);

    mChannelReturn(adapter, ret);
    }

static AtPwPsn ReplicatePwPsn(AtPwPsn replicaPsn, AtPwPsn psn)
    {
    if ((psn == NULL) && (replicaPsn == NULL))
        return NULL;

    if (AtPwPsnTypeGet(replicaPsn) == AtPwPsnTypeGet(psn))
        AtPwPsnCopy(psn, replicaPsn);
    else
        {
        AtObjectDelete((AtObject)replicaPsn);
        replicaPsn = (AtPwPsn)AtObjectClone((AtObject)psn);
        }

    /* Replicate lower psn */
    AtPwPsnLowerPsnSet(replicaPsn, ReplicatePwPsn(AtPwPsnLowerPsnGet(replicaPsn), AtPwPsnLowerPsnGet(psn)));

    return replicaPsn;
    }

static AtPwPsn PsnGet(ThaPwHeaderController self)
    {
    AtPwPsn psnToReturn;
    if (IsLogicPw(self))
        psnToReturn = ThaPwHeaderControllerCache(self)->psn;
    else
        {
        if (HbceActivated(self))
            psnToReturn = self->currentPsn;
        else
            psnToReturn = self->newPsn;
        }

    /* Return a copy of current PSN in order to protect current PSN
     * from being set invalid value by higher application
     */
    self->replicaPsn = ReplicatePwPsn(self->replicaPsn, psnToReturn);
    return self->replicaPsn;
    }

static eBool HbceCanActivate(ThaPwHeaderController self)
    {
    AtPw pw = (AtPw)ThaPwHeaderControllerAdapterGet(self);

    if (self == NULL)
        return cAtFalse;

    if ((ThaPwHeaderControllerPsnGet(self) == NULL) ||
        (AtPwEthPortGet(pw)                == NULL) ||
        (AtPwBoundCircuitGet(pw)           == NULL))
        return cAtFalse;

    return cAtTrue;
    }

static eAtRet HbceWarmRestore(ThaPwHeaderController self)
    {
    eAtRet ret = cAtOk;
    AtPw pw = (AtPw)ThaPwHeaderControllerAdapterGet(self);
    ThaHbce hbce = ThaPwAdapterHbceGet(pw);

    if (!HbceCanActivate(self))
        return cAtOk;

    self->currentPsn = self->newPsn;

    if (hbce)
        ret |= ThaHbcePwWarmRestore(hbce, self);

    /* Mark it as activated */
    self->newPsn        = NULL;
    self->hbceActivated = cAtTrue;

    return ret;
    }

static eAtModulePwRet PsnWarmRestore(ThaPwHeaderController self, AtPwPsn psn)
    {
    eAtRet ret = cAtOk;
    ThaPwAdapter adapter = ThaPwHeaderControllerAdapterGet(self);
    AtPwPsn newPsn = psn;

    /* A none NULL PSN must be valid */
    if (psn && !AtPwPsnIsValid(psn))
        mChannelError(adapter, cAtErrorInvlParm);

    if (!PsnCanBeUsed(self, psn))
        return cAtErrorResourceBusy;

    if (IsLogicPw(self))
        {
        tThaPwHeaderCache *cache = ThaPwHeaderControllerCache(self);
        if (cache == NULL)
            return cAtErrorNullPointer;

        if (cache->psn == NULL)
            newPsn = (AtPwPsn)AtObjectClone((AtObject)psn);
        else if (cache->psn != psn)
            {
            AtObjectDelete((AtObject)(cache->psn));
            newPsn = (AtPwPsn)AtObjectClone((AtObject)psn);
            }

        cache->psn = newPsn;
        return cAtOk;
        }

    /* Need to clone this PSN for internal usage */
    if (psn)
        newPsn = (AtPwPsn)AtObjectClone((AtObject)psn);

    /* Update database and activate PW */
    self->newPsn = newPsn;
    ret |= HbceWarmRestore(self);

    return ret;
    }

static eBool MplsLabelFromBufferIsInner(uint8* psnBuffer, tAtPwMplsLabel *label)
    {
    label->label = ((uint32)(psnBuffer[0]) << 12) | ((uint32)(psnBuffer[1]) << 4) | ((uint32)(psnBuffer[2]) >> 4);
    label->experimental = (uint8)((psnBuffer[2] & cBit3_1) >> 1);
    label->timeToLive = (psnBuffer[3]);
    if (psnBuffer[2] & cBit0)
        return cAtTrue;

    return cAtFalse;
    }

static AtPwPsn NormalMplsPsnFromBufferBuild(uint8* psnBuffer, uint8 bufferLength)
    {
    uint32 byte_i = 0;
    AtPwMplsPsn mplsPsn;
    tAtPwMplsLabel label;

    if ((bufferLength % 4) != 0)
        return NULL;

    mplsPsn = AtPwMplsPsnNew();
    while (byte_i < bufferLength)
        {
        if (MplsLabelFromBufferIsInner(&psnBuffer[byte_i], &label))
            AtPwMplsPsnInnerLabelSet(mplsPsn, &label);
        else
            AtPwMplsPsnOuterLabelAdd(mplsPsn, &label);

        byte_i = byte_i + 4;
        }

    return (AtPwPsn)mplsPsn;
    }

static AtPwPsn InvalidMefMplsPsn(AtPwMplsPsn mplsPsn, AtPwMefPsn mefPsn)
    {
    AtObjectDelete((AtObject)mplsPsn);
    AtObjectDelete((AtObject)mefPsn);
    return NULL;
    }

static AtPwPsn MefOverMplsTpPsnFromBufferBuild(AtPw self, uint8* psnBuffer, uint8 bufferLength)
    {
    uint32 byte_i = 0;
    AtPwMplsPsn mplsPsn;
    tAtPwMplsLabel label;
    AtPwMefPsn mefPsn;
    uint16 ethType;
    uint32 txEcid;
    ThaClaController claController;
    uint8 expectedMac[cAtMacAddressLen];

    mplsPsn = AtPwMplsPsnNew();
    mefPsn = AtPwMefPsnNew();

    MplsLabelFromBufferIsInner(&psnBuffer[byte_i], &label);
    AtPwMplsPsnInnerLabelSet(mplsPsn, &label);
    byte_i = byte_i + 4;

    if (bufferLength < byte_i + 6)
        return InvalidMefMplsPsn(mplsPsn, mefPsn);

    AtPwMefPsnDestMacSet(mefPsn, psnBuffer + byte_i);
    byte_i = byte_i + 6;

    if (bufferLength < byte_i + 6)
        return InvalidMefMplsPsn(mplsPsn, mefPsn);

    AtPwMefPsnSourceMacSet(mefPsn, psnBuffer + byte_i);
    byte_i = byte_i + 6;

    /* Check MEF ethernet type */
    ethType = (uint16)((psnBuffer[byte_i] << 8) | psnBuffer[byte_i + 1]);
    if (ethType != cThaEthTypeMef)
        return InvalidMefMplsPsn(mplsPsn, mefPsn);
    byte_i = byte_i + 2;

    if (bufferLength < byte_i + 4)
        return InvalidMefMplsPsn(mplsPsn, mefPsn);

    txEcid = (uint32)(psnBuffer[byte_i] << 12) | (uint32)(psnBuffer[byte_i + 1] << 4) | (uint32)(psnBuffer[byte_i + 2] >> 4);
    AtPwMefPsnTxEcIdSet(mefPsn, txEcid);

    claController = (ThaClaController)ThaPwAdapterClaPwController(mAdapter(self));
    AtPwMefPsnExpectedEcIdSet(mefPsn, ThaClaControllerMefOverMplsExpectedEcidGet(claController, self));

    if (ThaClaControllerMefOverMplsExpectedMacGet(claController, self, expectedMac) == cAtOk)
        AtPwMefPsnExpectedMacSet(mefPsn, expectedMac);

    AtPwPsnLowerPsnSet((AtPwPsn)mefPsn, (AtPwPsn)mplsPsn);

    return (AtPwPsn)mefPsn;
    }

static AtPwPsn MplsPsnFromBufferBuild(ThaPwAdapter self, uint8* psnBuffer, uint8 bufferLength)
    {
    AtPwPsn psn;
    ThaModulePw modulePw = (ThaModulePw)AtChannelModuleGet((AtChannel)self);
    uint8 maxNumBytesOfMplsPsn = (cAtMplsPsnMaxNumOuterLabels + 1 /* Inner */) * 4;

    /* Normal MPLS */
    if (bufferLength <= maxNumBytesOfMplsPsn)
        {
        psn = NormalMplsPsnFromBufferBuild(psnBuffer, bufferLength);
        AtPwMplsPsnExpectedLabelSet((AtPwMplsPsn)psn, ThaModulePwLabelGetFromHwStorage(modulePw, (AtPw)self));
        }

    /* MEF over MPLS-TP */
    else
        {
        psn = MefOverMplsTpPsnFromBufferBuild((AtPw)self, psnBuffer, bufferLength);
        AtPwMplsPsnExpectedLabelSet((AtPwMplsPsn)AtPwPsnLowerPsnGet(psn), ThaModulePwLabelGetFromHwStorage(modulePw, (AtPw)self));
        }

    return psn;
    }

static AtPwPsn MefPsnFromBufferBuild(ThaPwAdapter self, uint8* psnBuffer, uint8 bufferLength)
    {
    AtPwMefPsn mefPsn;
    uint32 txEcid;
    ThaModulePw modulePw = (ThaModulePw)AtChannelModuleGet((AtChannel)self);

    if (bufferLength != 4)
        return NULL;

    mefPsn = AtPwMefPsnNew();
    txEcid = (uint32)(psnBuffer[0] << 12) | (uint32)(psnBuffer[1] << 4) | (uint32)(psnBuffer[2] >> 4);
    AtPwMefPsnTxEcIdSet(mefPsn, txEcid);
    AtPwMefPsnExpectedEcIdSet(mefPsn, ThaModulePwLabelGetFromHwStorage(modulePw, (AtPw)self));

    return (AtPwPsn)mefPsn;
    }

static AtPwPsn Ipv4PsnFromBufferBuild(ThaPwAdapter self, uint8* psnBuffer, uint8 bufferLength)
    {
    AtUnused(self);
    AtUnused(psnBuffer);
    AtUnused(bufferLength);
    return NULL;
    }

static AtPwPsn Ipv6PsnFromBufferBuild(ThaPwAdapter self, uint8* psnBuffer, uint8 bufferLength)
    {
    AtUnused(self);
    AtUnused(psnBuffer);
    AtUnused(bufferLength);
    return NULL;
    }

static AtPwPsn PsnFromBufferBuild(ThaPwAdapter self, uint8* psnBuffer, uint8 bufferLength)
    {
    uint16 ethType;
    uint8 length;

    if (bufferLength <= cEthTypeSizeInBytes)
        return NULL;

    ethType = (uint16)((psnBuffer[0] << 8) | psnBuffer[1]);
    length = (uint8)(bufferLength - cEthTypeSizeInBytes);
    switch (ethType)
        {
        case cThaEthTypeMplsUcast:
            return MplsPsnFromBufferBuild(self, &psnBuffer[cEthTypeSizeInBytes], length);
        case cThaEthTypeMef:
            return MefPsnFromBufferBuild(self, &psnBuffer[cEthTypeSizeInBytes], length);
        case cThaEthTypeIpv4:
            return Ipv4PsnFromBufferBuild(self, &psnBuffer[cEthTypeSizeInBytes], length);
        case cThaEthTypeIpv6:
            return Ipv6PsnFromBufferBuild(self, &psnBuffer[cEthTypeSizeInBytes], length);
        default:
            return NULL;
        }

    return NULL;
    }

static tAtEthVlanTag* SVlanGetFromBuffer(ThaPwHeaderController self, tAtEthVlanTag *sVlan, uint8* header, uint8 headerLength)
    {
    ThaPwHeaderProvider headerProvider = ThaModulePwHeaderProviderGet(ModulePw(self));
    ThaPwAdapter adapter = ThaPwHeaderControllerAdapterGet(self);
    return ThaPwHeaderProviderSVlanGetFromBuffer(headerProvider, adapter, sVlan, header, headerLength);
    }

static tAtEthVlanTag* CVlanGetFromBuffer(ThaPwHeaderController self, tAtEthVlanTag *cVlan, uint8* header, uint8 headerLength)
    {
    ThaPwHeaderProvider headerProvider = ThaModulePwHeaderProviderGet(ModulePw(self));
    ThaPwAdapter adapter = ThaPwHeaderControllerAdapterGet(self);
    return ThaPwHeaderProviderCVlanGetFromBuffer(headerProvider, adapter, cVlan, header, headerLength);
    }

static tAtEthVlanTag* StandbyEthCVlanGet(ThaPwHeaderController self, tAtEthVlanTag *cVlan)
    {
    AtPw pw = (AtPw)ThaPwHeaderControllerAdapterGet(self);
    return AtPwEthCVlanGet(pw, cVlan);
    }

static uint16 StandbyEthCVlanTpidGet(ThaPwHeaderController self)
    {
    AtPw pw = (AtPw)ThaPwHeaderControllerAdapterGet(self);
    return AtPwEthCVlanTpidGet(pw);
    }

static uint16 StandbyEthSVlanTpidGet(ThaPwHeaderController self)
    {
    AtPw pw = (AtPw)ThaPwHeaderControllerAdapterGet(self);
    return AtPwEthSVlanTpidGet(pw);
    }

static tAtEthVlanTag* StandbyEthSVlanGet(ThaPwHeaderController self, tAtEthVlanTag *sVlan)
    {
    AtPw pw = (AtPw)ThaPwHeaderControllerAdapterGet(self);
    return AtPwEthSVlanGet(pw, sVlan);
    }

/* Helper function to get SVLAN information of a Pseudowire */
static tAtEthVlanTag* EthHeaderSVlanGet(ThaPwHeaderController self, tAtEthVlanTag *sVlan)
    {
    ThaPwHeaderProvider headerProvider;
    if (ThaPwHeaderControllerInAccessible(self))
        return mMethodsGet(self)->StandbyEthSVlanGet(self, sVlan);

    headerProvider = ThaModulePwHeaderProviderGet(ModulePw(self));
    return ThaPwHeaderProviderSVlanGet(headerProvider, self, sVlan);
    }

/* Helper function to get CVLAN information of a Pseudowire */
static tAtEthVlanTag* EthHeaderCVlanGet(ThaPwHeaderController self, tAtEthVlanTag *cVlan)
    {
    ThaPwHeaderProvider headerProvider;
    if (ThaPwHeaderControllerInAccessible(self))
        return mMethodsGet(self)->StandbyEthCVlanGet(self, cVlan);

    headerProvider = ThaModulePwHeaderProviderGet(ModulePw(self));
    return ThaPwHeaderProviderCVlanGet(headerProvider, self, cVlan);
    }

static uint16 EthHeaderCVlanTpidGet(ThaPwHeaderController self)
    {
    ThaPwHeaderProvider headerProvider;
    if (AtDriverIsStandby())
        return mMethodsGet(self)->StandbyEthCVlanTpidGet(self);

    headerProvider = ThaModulePwHeaderProviderGet(ModulePw(self));
    return ThaPwHeaderProviderCVlanTpidGet(headerProvider, self);
    }

static uint16 EthHeaderSVlanTpidGet(ThaPwHeaderController self)
    {
    ThaPwHeaderProvider headerProvider;
    if (AtDriverIsStandby())
        return mMethodsGet(self)->StandbyEthSVlanTpidGet(self);

    headerProvider = ThaModulePwHeaderProviderGet(ModulePw(self));
    return ThaPwHeaderProviderSVlanTpidGet(headerProvider, self);
    }

static AtPwPsn PsnGetFromHardware(ThaPwHeaderController self)
    {
    uint8 totalHeaderLength, ethHeaderLength;
    tAtEthVlanTag vlan;
    uint8* headerBuffer = ThaPwHeaderControllerHwHeaderArrayGet(self, &totalHeaderLength);

    if (totalHeaderLength <= cAtMacAddressLen * 2)
        return NULL;

    /* Jump over MACs */
    ethHeaderLength = cAtMacAddressLen * 2;

    /* Jump over SVLAN */
    if (SVlanGetFromBuffer(self, &vlan, headerBuffer, totalHeaderLength))
        ethHeaderLength = (uint8)(ethHeaderLength + 4);

    /* Jump over CVLAN */
    if (CVlanGetFromBuffer(self, &vlan, headerBuffer, totalHeaderLength))
        ethHeaderLength = (uint8)(ethHeaderLength + 4);

    /* Psn has not been configured, do nothing */
    if (totalHeaderLength < ethHeaderLength + cEthTypeSizeInBytes)
        return NULL;

    /* TODO: need to update the following function to support warm restore for both primary and backup PSN */
    return PsnFromBufferBuild(ThaPwHeaderControllerAdapterGet(self), &headerBuffer[ethHeaderLength], (uint8)(totalHeaderLength - ethHeaderLength));
    }

static void DeletePsn(AtPwPsn *psn)
    {
    AtObjectDelete((AtObject)*psn);
    *psn = NULL;
    }

static void Delete(AtObject self)
    {
    ThaPwHeaderController controller = (ThaPwHeaderController)self;

    /* Delete cached PSNs */
    ThaPwHeaderControllerCacheDelete(controller);

    DeletePsn(&controller->newPsn);

    if (controller->currentPsn != controller->newPsn)
        DeletePsn(&controller->currentPsn);

    DeletePsn(&controller->replicaPsn);
    SimulationDatabaseDelete(mThis(self));
    ThaHbceMemoryCellContentPwHeaderControllerSet(controller->hbceMemoryCellContent, NULL);

    /* Fully delete itself */
    m_AtObjectMethods->Delete(self);
    }

static uint8 ActivePageGet(ThaPwHeaderController self)
    {
    AtPw pw = (AtPw)ThaPwHeaderControllerAdapterGet(self);
    AtDevice device = AtChannelDeviceGet((AtChannel)pw);
    ThaModuleCla moduleCla = (ThaModuleCla)AtDeviceModuleGet(device, cThaModuleCla);
    ThaClaPwController claPwController = ThaModuleClaPwControllerGet(moduleCla);
    ThaHbce hbce = ThaClaPwControllerHbceGet(claPwController, pw);
    return ThaHbceActivePage(hbce);
    }

static void Debug(ThaPwHeaderController self)
    {
    ThaHbceMemoryCell cell = ThaHbceMemoryCellContentCellGet(self->hbceMemoryCellContent);

    AtPrintc(cSevNormal, "CLA Active page: %d\n", ActivePageGet(self));
    AtPrintc(cSevNormal, "* HBCE Activated: %s\r\n", self->hbceActivated ? "YES" : "NO");
    AtPrintc(cSevNormal, "* HBCE memory cell %u: %s\r\n",
             ThaHbceMemoryCellIndexGet(cell),
             AtObjectToString((AtObject)cell));
    }

static void PwEthHeaderLengthSet(ThaPwHeaderController self, uint8 headerLength)
    {
    self->ethHdrLength = headerLength;
    }

static uint8 EthHeaderArrayBuild(ThaPwHeaderController self,
                                 ThaPwAdapter adapter,
                                 uint8 *srcMac,
                                 uint8 *destMac,
                                 const tAtEthVlanTag *cVlan,
                                 const tAtEthVlanTag *sVlan,
                                 uint8* buffer)
    {
    uint8 byte_i = 0;
    AtDevice device = AtChannelDeviceGet((AtChannel)adapter);
    ThaModuleEth ethModule = (ThaModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    ThaModulePw modulePw = (ThaModulePw)AtDeviceModuleGet(mDevice(adapter), cAtModulePw);
    ThaPwHeaderProvider headerProvider = ThaModulePwHeaderProviderGet(modulePw);

    /* SMAC and DMAC */
    byte_i = ThaPktUtilPutMacToBuffer(buffer, byte_i, destMac);
    byte_i = ThaPktUtilPutMacToBuffer(buffer, byte_i, srcMac);

    /* SVLAN */
    if (sVlan)
        {
        self->sVlanTpidIndex = byte_i;
        byte_i = ThaPwHeaderProviderPutSVlanToBuffer(headerProvider, buffer, byte_i, ThaModuleEthSVlanTpid(ethModule, ThaPwAdapterEthPortGet(adapter)), sVlan);
        }
    else
        self->sVlanTpidIndex = 0;

    /* CVLAN */
    if (cVlan)
        {
        self->cVlanTpidIndex = byte_i;
        byte_i = ThaPwHeaderProviderPutCVlanToBuffer(headerProvider, buffer, byte_i, ThaModuleEthCVlanTpid(ethModule), cVlan);
        }
    else
        self->cVlanTpidIndex = 0;

    return byte_i;
    }

/* Set Ethernet header array */
static eAtRet PwEthHeaderArraySet(ThaPwHeaderController self, uint8 *buffer, uint8 lengthInByte)
    {
    uint8 i, newLength;
    uint8 hwHeaderLength;
    uint8 *hwHeaderBuffer       = ThaPwHeaderControllerHwHeaderArrayGet(self, &hwHeaderLength);
    uint8 currentSwHeaderLength = PwEthHeaderLength(self);
    ThaPwAdapter adapter        = ThaPwHeaderControllerAdapterGet(self);

    if (hwHeaderBuffer == NULL)
        return cAtErrorNullPointer;

    /*
     * In case psn is set before setting ethernet header for pw, 12 bytes for DA, SA has been added to header buffer at
     * the time PSN is set, so hwHeaderLength must >= currentSwHeaderLength.
     *
     * This is case that pw has not been set psn and ethernet header is set at first time, hwHeaderLength should be 0
     * and currentSwHeaderLength is 12 returned by function PwEthHeaderLength
     */
    if (hwHeaderLength < currentSwHeaderLength)
        hwHeaderLength = currentSwHeaderLength;

    newLength = (uint8)((hwHeaderLength - currentSwHeaderLength) + lengthInByte);
    if (!HeaderLengthIsValid(newLength))
        {
        AtChannelLog((AtChannel)adapter, cAtLogLevelCritical, AtSourceLocation, "Header length %d (bytes) is invalid\r\n", newLength);
        return cAtErrorInvlParm;
        }

    /* Shift PSN up */
    if (currentSwHeaderLength >= lengthInByte)
        {
        for (i = 0; i < (hwHeaderLength - currentSwHeaderLength); i++)
            hwHeaderBuffer[lengthInByte + i] = hwHeaderBuffer[currentSwHeaderLength + i];
        }

    /* Shift PSN down */
    else
        {
        for (i = 1; i <= (hwHeaderLength - currentSwHeaderLength); i++)
            hwHeaderBuffer[newLength - i] = hwHeaderBuffer[hwHeaderLength - i];
        }

    /* Put new Ethernet header */
    for (i = 0; i < lengthInByte; i++)
        hwHeaderBuffer[i] = buffer[i];

    /* Cache new Ethernet header length */
    PwEthHeaderLengthSet(self, lengthInByte);

    /* Set to HW */
    return HwHeaderArraySet(self, NULL, newLength, AtPwPsnGet((AtPw)adapter), cThaPwAdapterHeaderWriteModeDefault);
    }

static eAtModulePwRet HwEthHeaderDestMacGet(ThaPwHeaderController self, uint8 *destMac)
    {
    /* Get current header buffer */
    uint8 curHdrLength;
    uint8 *buffer = ThaPwHeaderControllerHwHeaderArrayGet(self, &curHdrLength);
    AtOsal osal = AtSharedDriverOsalGet();

    if (curHdrLength < cAtMacAddressLen)
        {
        mMethodsGet(osal)->MemInit(osal, destMac, cAtMacAddressLen, 0);
        return cAtOk;
        }

    mMethodsGet(osal)->MemCpy(osal, destMac, buffer, cAtMacAddressLen);

    return cAtOk;
    }

static eAtModulePwRet HwEthHeaderSrcMacGet(ThaPwHeaderController self, uint8 *srcMac)
    {
    /* Get current header buffer */
    uint8 curHdrLength;
    uint8 *buffer = ThaPwHeaderControllerHwHeaderArrayGet(self, &curHdrLength);
    AtOsal osal = AtSharedDriverOsalGet();

    if (curHdrLength < cAtMacAddressLen * 2)
        {
        mMethodsGet(osal)->MemInit(osal, srcMac, cAtMacAddressLen, 0);
        return cAtOk;
        }

    mMethodsGet(osal)->MemCpy(osal, srcMac, buffer + cAtMacAddressLen, cAtMacAddressLen);
    return cAtOk;
    }

static eBool PwEthHeaderNeedToBeUpdated(ThaPwHeaderController self, uint8 *newHeader, uint8 newHeaderLength)
    {
    uint8 totalHeaderLength;
    uint8 *headerBuffer = ThaPwHeaderControllerHwHeaderArrayGet(self, &totalHeaderLength);
    uint8 byte_i;
    uint8 currentEthHdrLength = PwEthHeaderLength(self);

    /* If length of ethernet header change */
    if ((headerBuffer == NULL) || (currentEthHdrLength != newHeaderLength))
        return cAtTrue;

    /* If psn has not been configured yet */
    if (totalHeaderLength < newHeaderLength)
        return cAtTrue;

    /* Compare each byte of ethernet header */
    for (byte_i = 0; byte_i < newHeaderLength; byte_i++)
        {
        if (headerBuffer[byte_i] != newHeader[byte_i])
            return cAtTrue;
        }

    return cAtFalse;
    }

/* Helper function to update PW Ethernet header */
static eAtModulePwRet HwEthHeaderSet(ThaPwHeaderController self, uint8 *destMac, uint8 *srcMac, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan)
    {
    uint8 ethHdrLength;
    uint8 numVlans = 0;
    eAtRet ret = cAtOk;
    ThaPwAdapter adapter = ThaPwHeaderControllerAdapterGet(self);
    uint8 *psnBuffer = SharedPsnBuffer(self, NULL);

    if ((destMac == NULL) || (srcMac == NULL))
        return cAtErrorInvlParm;

    /* Build Ethernet header */
    ethHdrLength = EthHeaderArrayBuild(self, adapter, srcMac, destMac, cVlan, sVlan, psnBuffer);

    if (!PwEthHeaderNeedToBeUpdated(self, psnBuffer, ethHdrLength))
        {
        /* It looks not necessary to save this length to database,
         * but it is useful when pw is in warm restore process */
        PwEthHeaderLengthSet(self, ethHdrLength);
        return cAtOk;
        }

    /* Set number of VLANs */
    if (cVlan && sVlan)
        numVlans = 2;
    else if (cVlan)
        numVlans = 1;
    mMethodsGet(self)->NumVlansSet(self, numVlans);

    /* Set self header to HW */
    ret  = PwEthHeaderArraySet(self, psnBuffer, ethHdrLength);
    ret |= ThaModulePwDidChangeCVlanOnPw(ModulePw(self), (AtPw)adapter, cVlan);

    return ret;
    }

static eAtModulePwRet CacheVlans(ThaPwHeaderController self, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan)
    {
    AtOsal osal;
    tThaPwHeaderCache *cache = ThaPwHeaderControllerCache(self);
    if (cache == NULL)
        return cAtErrorNullPointer;

    osal = AtSharedDriverOsalGet();
    cache->numVlans = 0;

    if (cVlan)
        {
        mMethodsGet(osal)->MemCpy(osal, &(cache->cVlan), cVlan, sizeof(tAtEthVlanTag));
        cache->numVlans = (uint8)(cache->numVlans + 1);
        }

    if (sVlan)
        {
        mMethodsGet(osal)->MemCpy(osal, &(cache->sVlan), sVlan, sizeof(tAtEthVlanTag));
        cache->numVlans = (uint8)(cache->numVlans + 1);
        }

    return cAtOk;
    }

static eAtModulePwRet EthHeaderSet(ThaPwHeaderController self, uint8 *destMac, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan)
    {
    tThaPwHeaderCache *cache;
    AtOsal osal;

    if (destMac == NULL)
        return cAtErrorNullPointer;

    cache = ThaPwHeaderControllerCache(self);
    if (cache == NULL)
        return cAtErrorNullPointer;

    if (!IsLogicPw(self))
        {
        uint8 srcMac[cAtMacAddressLen];
        ThaPwHeaderControllerEthSourceMacGetFromDb(self, srcMac);
        return HwEthHeaderSet(self, destMac, srcMac, cVlan, sVlan);
        }

    /* Cache MAC */
    osal  = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemCpy(osal, cache->destMac, destMac, sizeof(cache->destMac));

    return CacheVlans(self, cVlan, sVlan);
    }

static eAtModulePwRet EthDestMacSet(ThaPwHeaderController self, uint8 *destMac)
    {
    tAtEthVlanTag cVlan, sVlan;
    tAtEthVlanTag* pCVlan;
    tAtEthVlanTag* pSVlan;
    uint8 srcMac[cAtMacAddressLen];

    if (destMac == NULL)
        return cAtErrorNullPointer;

    if (IsLogicPw(self))
        {
        AtOsal osal = AtSharedDriverOsalGet();
        tThaPwHeaderCache *cache = ThaPwHeaderControllerCache(self);
        if (cache)
            mMethodsGet(osal)->MemCpy(osal, cache->destMac, destMac, sizeof(cache->destMac));

        return cAtOk;
        }

    pCVlan = EthHeaderCVlanGet(self, &cVlan);
    pSVlan = EthHeaderSVlanGet(self, &sVlan);
    HwEthHeaderSrcMacGet(self, srcMac);
    return HwEthHeaderSet(self, destMac, srcMac, pCVlan, pSVlan);
    }

static eAtModulePwRet EthDestMacGet(ThaPwHeaderController self, uint8 *destMac)
    {
    tThaPwHeaderCache *cache;
    AtOsal osal;

    if (destMac == NULL)
        return cAtErrorNullPointer;

    if (!IsLogicPw(self))
        return HwEthHeaderDestMacGet(self, destMac);

    cache = ThaPwHeaderControllerCache(self);
    if (cache == NULL)
        return cAtErrorNullPointer;

    osal  = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemCpy(osal, destMac, cache->destMac, sizeof(cache->destMac));

    return cAtOk;
    }

static eAtRet CacheSrcMac(ThaPwHeaderController self, uint8 *srcMac)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemCpy(osal, self->srcMac, srcMac, sizeof(self->srcMac));
    return cAtOk;
    }

static eAtModulePwRet EthSrcMacSetHelper(ThaPwHeaderController self, uint8 *srcMac)
    {
    tAtEthVlanTag cVlan, sVlan;
    tAtEthVlanTag* pCVlan;
    tAtEthVlanTag* pSVlan;
    uint8 destMac[cAtMacAddressLen];

    if (srcMac == NULL)
        return cAtErrorNullPointer;

    if (IsLogicPw(self))
        return cAtOk;

    pCVlan = EthHeaderCVlanGet(self, &cVlan);
    pSVlan = EthHeaderSVlanGet(self, &sVlan);
    HwEthHeaderDestMacGet(self, destMac);
    return HwEthHeaderSet(self, destMac, srcMac, pCVlan, pSVlan);
    }

static eAtModulePwRet EthSrcMacSet(ThaPwHeaderController self, uint8 *srcMac)
    {
    eAtRet ret = EthSrcMacSetHelper(self, srcMac);
    if (ret != cAtOk)
        return ret;

    self->srcMacIsSetByUser = cAtTrue;
    CacheSrcMac(self, srcMac);

    return cAtOk;
    }

static eAtModulePwRet EthVlanTpidSet(ThaPwHeaderController self, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan, uint16 tpId)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    eAtRet ret = cAtOk;
    ThaPwAdapter adapter = ThaPwHeaderControllerAdapterGet(self);
    ThaModulePw modulePw = (ThaModulePw)AtDeviceModuleGet(mDevice(adapter), cAtModulePw);
    ThaPwHeaderProvider headerProvider = ThaModulePwHeaderProviderGet(modulePw);
    uint8 *headerBuffer = SharedHeaderBuffer(self, NULL);
    uint8 *psnBuffer = SharedPsnBuffer(self, NULL);

    if (sVlan)
        ThaPwHeaderProviderPutSVlanToBuffer(headerProvider, headerBuffer, self->sVlanTpidIndex, tpId, sVlan);

    if (cVlan)
        ThaPwHeaderProviderPutCVlanToBuffer(headerProvider, headerBuffer, self->cVlanTpidIndex, tpId, cVlan);

    /* Set self header to HW */
    mMethodsGet(osal)->MemCpy(osal, psnBuffer, headerBuffer, PwEthHeaderLength(self));
    ret = PwEthHeaderArraySet(self, psnBuffer, PwEthHeaderLength(self));

    return ret;
    }

static eAtModulePwRet EthCVlanTpidSet(ThaPwHeaderController self, uint16 tpid)
    {
    tAtEthVlanTag cVlan;
    tAtEthVlanTag* pCVlan = EthHeaderCVlanGet(self, &cVlan);

    if (pCVlan == NULL)
        return cAtErrorNotApplicable;

    if (IsLogicPw(self))
        {
        tThaPwHeaderCache *cache = ThaPwHeaderControllerCache(self);
        if (cache == NULL)
            return cAtErrorNotApplicable;
        cache->tpidCVlan = tpid;
        }

    return EthVlanTpidSet(self, pCVlan, NULL, tpid);
    }

static eAtModulePwRet EthSVlanTpidSet(ThaPwHeaderController self, uint16 tpid)
    {
    tAtEthVlanTag sVlan;
    tAtEthVlanTag* pSVlan = EthHeaderSVlanGet(self, &sVlan);

    if (pSVlan == NULL)
        return cAtErrorNotApplicable;

    if (IsLogicPw(self))
        {
        tThaPwHeaderCache *cache = ThaPwHeaderControllerCache(self);
        if (cache == NULL)
            return cAtErrorNullPointer;
        cache->tpidSVlan = tpid;
        }

    return EthVlanTpidSet(self, NULL, pSVlan, tpid);
    }

static eAtModulePwRet EthSrcMacGet(ThaPwHeaderController self, uint8 *srcMac)
    {
    AtOsal osal = AtSharedDriverOsalGet();

    if (srcMac == NULL)
        return cAtErrorNullPointer;

    if (!IsLogicPw(self))
        return HwEthHeaderSrcMacGet(self, srcMac);

    mMethodsGet(osal)->MemCpy(osal, srcMac, self->srcMac, sizeof(self->srcMac));

    return cAtOk;
    }

static eAtModulePwRet EthVlanSet(ThaPwHeaderController self, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan)
    {
    uint8 destMac[cAtMacAddressLen];
    uint8 srcMac[cAtMacAddressLen];

    if (IsLogicPw(self))
        return CacheVlans(self, cVlan, sVlan);

    HwEthHeaderDestMacGet(self, destMac);
    HwEthHeaderSrcMacGet(self, srcMac);
    return HwEthHeaderSet(self, destMac, srcMac, cVlan, sVlan);
    }

static eAtRet NumVlansSet(ThaPwHeaderController self, uint8 numVlans)
    {
    AtPw pw = (AtPw)ThaPwHeaderControllerAdapterGet(self);
    return ThaModulePweNumVlansSet(ModulePwe(self), pw, numVlans);
    }

static tAtEthVlanTag *EthCVlanGet(ThaPwHeaderController self, tAtEthVlanTag *cVlan)
    {
    tThaPwHeaderCache *cache;
    AtOsal osal = AtSharedDriverOsalGet();

    if (!IsLogicPw(self))
        return EthHeaderCVlanGet(self, cVlan);

    cache = ThaPwHeaderControllerCache(self);
    if (cache == NULL)
        return NULL;

    if (cache->numVlans == 0)
        return NULL;

    mMethodsGet(osal)->MemCpy(osal, cVlan, &(cache->cVlan), sizeof(tAtEthVlanTag));

    return cVlan;
    }

static uint16 EthCVlanTpidGet(ThaPwHeaderController self)
    {
    tThaPwHeaderCache *cache;

    if (!IsLogicPw(self))
        return EthHeaderCVlanTpidGet(self);

    cache = ThaPwHeaderControllerCache(self);
    if (cache == NULL)
        return 0;

    return cache->tpidCVlan;
    }

static uint16 EthSVlanTpidGet(ThaPwHeaderController self)
    {
    tThaPwHeaderCache *cache;

    if (!IsLogicPw(self))
        return EthHeaderSVlanTpidGet(self);

    cache = ThaPwHeaderControllerCache(self);
    if (cache == NULL)
        return 0;
    return cache->tpidSVlan;
    }

static tAtEthVlanTag *EthSVlanGet(ThaPwHeaderController self, tAtEthVlanTag *sVlan)
    {
    tThaPwHeaderCache *cache;
    AtOsal osal = AtSharedDriverOsalGet();

    if (!IsLogicPw(self))
        return EthHeaderSVlanGet(self, sVlan);

    cache = ThaPwHeaderControllerCache(self);
    if (cache == NULL)
        return NULL;

    if (cache->numVlans <= 1)
       return NULL;

    mMethodsGet(osal)->MemCpy(osal, sVlan, &(cache->sVlan), sizeof(tAtEthVlanTag));

    return sVlan;
    }

static eAtModulePwRet HeaderLengthSet(ThaPwHeaderController self, uint8 hdrLenInByte)
    {
    AtPw pw = (AtPw)ThaPwHeaderControllerAdapterGet(self);
    eAtRet ret = ThaModuleCosPwHeaderLengthSet(ModuleCos(self), pw, hdrLenInByte);

    if (IsSimulated(self))
        SimulationDatabase(self)->psnHeaderLength = hdrLenInByte;

    return ret;
    }

static uint8 HeaderLengthGet(ThaPwHeaderController self)
    {
    AtPw pw = (AtPw)ThaPwHeaderControllerAdapterGet(self);
    ThaModuleCos moduleCos = ModuleCos(self);
    uint8 simLength = SimulationDatabase(self)->psnHeaderLength;

    if (moduleCos)
        {
        uint8 length = ThaModuleCosPwHeaderLengthGet(moduleCos, pw);
        return (uint8)(IsSimulated(self) ? simLength : length);
        }

    if (IsSimulated(self))
        return simLength;

    return 0;
    }

static eAtModulePwRet HeaderRead(ThaPwHeaderController self, uint8 *buffer, uint8 headerLengthInByte)
    {
    ThaModulePwe modulePwe = ModulePwe(self);
    AtPw pw = (AtPw)ThaPwHeaderControllerAdapterGet(self);
    eAtRet ret = ThaModulePweHeaderRead(modulePwe, pw, buffer, headerLengthInByte);

    if (IsSimulated(self))
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, buffer, SimulationDatabase(self)->psnHeader, headerLengthInByte);
        return cAtOk;
        }

    return ret;
    }

static eAtModulePwRet HeaderWrite(ThaPwHeaderController self, uint8 *buffer, uint8 headerLenInByte, AtPwPsn psn, eThaPwAdapterHeaderWriteMode writeMode)
    {
    ThaModulePwe modulePwe = ModulePwe(self);
    AtPw pw = (AtPw)ThaPwHeaderControllerAdapterGet(self);
    eAtRet ret = ThaModulePweHeaderWrite(modulePwe, pw, buffer, headerLenInByte, psn);

    AtUnused(writeMode);

    if (IsSimulated(self))
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, SimulationDatabase(self)->psnHeader, buffer, headerLenInByte);
        }

    return ret;
    }

static eAtModulePwRet PsnTypeSet(ThaPwHeaderController self)
    {
    return ThaModulePwePwPsnTypeSet(ModulePwe(self), (AtPw)ThaPwHeaderControllerAdapterGet(self), ThaPwHeaderControllerPsnGet(self));
    }

static eAtModulePwRet RtpTxSsrcSet(ThaPwHeaderController self, uint32 ssrc)
    {
    ThaModuleCos moduleCos;
    AtPw pwAdapter = (AtPw)ThaPwHeaderControllerAdapterGet(self);

    if (IsLogicPw(self))
        {
        tThaPwHeaderCache *cache = ThaPwHeaderControllerCache(self);
        if (cache)
            cache->txRtpSsrc = ssrc;

        return cAtOk;
        }

    if (AtPwRtpTxSsrcGet(pwAdapter) == ssrc)
        return cAtOk;

    moduleCos = ModuleCos(self);
    if (moduleCos)
        return mMethodsGet(moduleCos)->PwRtpSsrcSet(moduleCos, pwAdapter, ssrc);

    return cAtError;
    }

static uint32 RtpTxSsrcGet(ThaPwHeaderController self)
    {
    ThaModuleCos moduleCos;
    AtPw pwAdapter = (AtPw)ThaPwHeaderControllerAdapterGet(self);

    if (IsLogicPw(self))
        {
        tThaPwHeaderCache *cache = ThaPwHeaderControllerCache(self);
        return cache ? cache->txRtpSsrc : 0x0;
        }

    moduleCos = ModuleCos(self);
    if (moduleCos)
        return mMethodsGet(moduleCos)->PwRtpSsrcGet(moduleCos, pwAdapter);

    return cAtError;
    }

static eAtModulePwRet RtpTxPayloadTypeSet(ThaPwHeaderController self, uint8 payloadType)
    {
    ThaModuleCos moduleCos;
    AtPw pwAdapter = (AtPw)ThaPwHeaderControllerAdapterGet(self);

    if (!ThaModulePwRtpPayloadTypeIsInRange(payloadType))
        return cAtErrorOutOfRangParm;

    if (IsLogicPw(self))
        {
        tThaPwHeaderCache *cache = ThaPwHeaderControllerCache(self);
        if (cache)
            cache->txRtpPayloadType = payloadType;

        return cAtOk;
        }

    if (AtPwRtpTxPayloadTypeGet(pwAdapter) == payloadType)
        return cAtOk;

    moduleCos = ModuleCos(self);
    if (moduleCos)
        return mMethodsGet(moduleCos)->PwRtpPayloadTypeSet(moduleCos, pwAdapter, payloadType);

    return cAtError;
    }

static uint8 RtpTxPayloadTypeGet(ThaPwHeaderController self)
    {
    ThaModuleCos moduleCos;
    AtPw pwAdapter = (AtPw)ThaPwHeaderControllerAdapterGet(self);

    if (IsLogicPw(self))
        {
        tThaPwHeaderCache *cache = ThaPwHeaderControllerCache(self);
        return (uint8)(cache ? cache->txRtpPayloadType : 0);
        }

    moduleCos = ModuleCos(self);
    if (moduleCos)
        return mMethodsGet(moduleCos)->PwRtpPayloadTypeGet(moduleCos, pwAdapter);

    return cAtError;
    }

static eBool IsPrimary(ThaPwHeaderController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet ApplyCellToHw(ThaPwHeaderController self)
    {
    AtUnused(self);

    /* Product applies directly cell to HW will override this method */
    return cAtOk;
    }

static void SerializeCache(tThaPwHeaderCache * cache, AtCoder encoder)
    {
    if (cache == NULL)
        return;

    /* RTP */
    mCacheEncodeUInt(txRtpSsrc);
    mCacheEncodeUInt(txRtpPayloadType);

    AtCoderEncodeObject(encoder, (AtObject)cache->psn, "cache.psn");

    /* ETH header */
    mEncodeVlanCache(cVlan);
    mEncodeVlanCache(sVlan);
    mCacheEncodeUInt(numVlans);

    AtCoderEncodeUInt8Array(encoder, cache->destMac, sizeof(cache->destMac), "cache.destMac");

    /* Subport VLAN */
    mCacheEncodeUInt(subportVlanIndex);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaPwHeaderController object = (ThaPwHeaderController)self;
    m_AtObjectMethods->Serialize(self, encoder);

    (void)ThaPwHeaderControllerCache(object); /* For lazy objects */
    SerializeCache(object->cache, encoder);
    (void)ThaPwHeaderControllerPsnGet(object); /* For lazy objects */

    /* ETH */
    mEncodeUInt(ethHdrLength);

    /* PSN */
    mEncodeObject(currentPsn);
    mEncodeObject(newPsn);
    mEncodeUInt(hbceActivated);
    mEncodeObjectDescription(hbceMemoryCellContent);
    mEncodeObject(replicaPsn);
    mEncodeUInt(pwPsnHeaderIsSet);
    mEncodeUInt(srcMacIsSetByUser);
    mEncodeUInt8Array(srcMac, sizeof(object->srcMac));
    mEncodeObjectDescription(adapter);
    mEncodeNone(simulation);
    mEncodeUInt(cVlanTpidIndex);
    mEncodeUInt(sVlanTpidIndex);
    }

static const char *ToString(AtObject self)
    {
    static char description[128];
    AtChannel adapter = (AtChannel)mThis(self)->adapter;
    AtDevice dev = AtChannelDeviceGet(adapter);

    AtSnprintf(description, sizeof(description) - 1,
               "%s%s.%s_header_controller",
               AtDeviceIdToString(dev),
               AtChannelTypeString(adapter),
               AtChannelIdString(adapter));
    return description;
    }

static eAtModulePwRet RtpEnable(ThaPwHeaderController self, eBool enable)
    {
    ThaModulePwe modulePwe = ModulePwe(self);
    AtPw pwAdapter = (AtPw)ThaPwHeaderControllerAdapterGet(self);
    eAtRet ret = cAtOk;

    if (IsLogicPw(self))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(pwAdapter));
        if (cache)
            cache->rtpEnabled = enable;
        return cAtOk;
        }

    /* To make sure that RTP enable/disable is set at least once */
    if (ThaPwHeaderControllerShouldPreventReconfig(self) && (mAdapter(pwAdapter)->rtpEnableIsSet) && (AtPwRtpIsEnabled(pwAdapter) == enable))
        return cAtOk;

    ThaPwAdapterStartRemoving(pwAdapter);
    ret |= ThaModulePwePwRtpEnable(modulePwe, pwAdapter, enable);
    ret |= ThaClaPwControllerPwRtpEnable(ThaPwAdapterClaPwController(mAdapter(pwAdapter)), pwAdapter, enable);

    mAdapter(pwAdapter)->rtpIsEnabled = enable;

    /* Need to set default RTP */
    if (enable)
        {
        if (AtPwRtpTimeStampModeGet(pwAdapter) == cAtPwRtpTimeStampModeInvalid)
            AtPwRtpTimeStampModeSet(pwAdapter, cAtPwRtpTimeStampModeDifferential);
        }

    ThaPwAdapterFinishRemoving(pwAdapter);

    mAdapter(pwAdapter)->rtpEnableIsSet = cAtTrue;
    return ret;
    }

static eAtModulePwRet RtpSsrcCompare(ThaPwHeaderController self, eBool enableCompare)
    {
    eThaPwErrorCheckingMode checkingMode;
    AtPw pwAdapter = (AtPw)ThaPwHeaderControllerAdapterGet(self);

    if (IsLogicPw(self))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(pwAdapter));
        if (cache)
            cache->ssrcIsCompared = enableCompare;
        return cAtOk;
        }

    if (AtPwRtpSsrcIsCompared(pwAdapter) == enableCompare)
        return cAtOk;

    checkingMode = enableCompare ? cThaPwErrorCheckingModeCheckCountAndDiscard : cThaPwErrorCheckingModeNoCheck;
    return ThaClaPwControllerRtpSsrcMismatchCheckModeSet(ThaPwAdapterClaPwController(mAdapter(pwAdapter)), pwAdapter, checkingMode);
    }

static eBool RtpSsrcIsCompared(ThaPwHeaderController self)
    {
    eThaPwErrorCheckingMode checkingMode;
    AtPw pwAdapter = (AtPw)ThaPwHeaderControllerAdapterGet(self);

    if (IsLogicPw(self))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(pwAdapter));
        return (eBool)(cache ? cache->ssrcIsCompared : cAtFalse);
        }

    checkingMode = ThaClaPwControllerRtpSsrcMismatchCheckModeGet(ThaPwAdapterClaPwController(mAdapter(pwAdapter)), pwAdapter);
    return (checkingMode == cThaPwErrorCheckingModeCheckCountAndDiscard) ? cAtTrue : cAtFalse;
    }

static eAtModulePwRet RtpPayloadTypeCompare(ThaPwHeaderController self, eBool enableCompare)
    {
    eThaPwErrorCheckingMode checkingMode;
    AtPw pwAdapter = (AtPw)ThaPwHeaderControllerAdapterGet(self);

    if (IsLogicPw(self))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(pwAdapter));
        if (cache)
            cache->payloadTypeIsCompared = enableCompare;

        return cAtOk;
        }

    if (AtPwRtpPayloadTypeIsCompared(pwAdapter) == enableCompare)
        return cAtOk;

    checkingMode = enableCompare ? cThaPwErrorCheckingModeCheckCountAndDiscard : cThaPwErrorCheckingModeNoCheck;
    return ThaClaPwControllerRtpPldTypeMismatchCheckModeSet(ThaPwAdapterClaPwController(mAdapter(pwAdapter)), pwAdapter, checkingMode);
    }

static eBool RtpPayloadTypeIsCompared(ThaPwHeaderController self)
    {
    eThaPwErrorCheckingMode checkingMode;
    AtPw pwAdapter = (AtPw)ThaPwHeaderControllerAdapterGet(self);

    if (IsLogicPw(self))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache(mAdapter(pwAdapter));
        return (eBool)(cache ? cache->payloadTypeIsCompared : cAtFalse);
        }

    checkingMode = ThaClaPwControllerRtpPldTypeMismatchCheckModeGet(ThaPwAdapterClaPwController(mAdapter(pwAdapter)), pwAdapter);
    return (checkingMode == cThaPwErrorCheckingModeCheckCountAndDiscard) ? cAtTrue : cAtFalse;
    }

static eAtRet EthPortQueueSet(ThaPwHeaderController self, uint8 queueId)
    {
    AtUnused(self);
    AtUnused(queueId);
    return cAtErrorModeNotSupport;
    }

static uint8 EthPortQueueGet(ThaPwHeaderController self)
    {
    AtUnused(self);
    return cAtInvalidPwEthPortQueueId;
    }

static uint8 EthPortMaxNumQueues(ThaPwHeaderController self)
    {
    AtUnused(self);
    return 0;
    }

static ThaModulePw PwModule(ThaPwHeaderController self)
    {
    ThaPwAdapter adapter = ThaPwHeaderControllerAdapterGet(self);
    return (ThaModulePw)AtChannelModuleGet((AtChannel)adapter);
    }

static eBool SubPortVlanIndexIsInRange(ThaPwHeaderController self, uint32 subPortVlanIndex)
    {
    if (subPortVlanIndex < ThaModulePwNumSubPortVlans(PwModule(self)))
        return cAtTrue;
    return cAtFalse;
    }

static eAtRet SubPortVlanIndexSet(ThaPwHeaderController self, uint32 subPortVlanIndex)
    {
    if (IsLogicPw(self))
        {
        tThaPwHeaderCache *cache = ThaPwHeaderControllerCache(self);
        if (cache)
            cache->subportVlanIndex = subPortVlanIndex;
        return cAtOk;
        }

    return ThaModulePwTxPwSubPortVlanIndexSet(ModulePw(self), (AtPw)ThaPwHeaderControllerAdapterGet(self), subPortVlanIndex);
    }

static uint32 SubPortVlanIndexGet(ThaPwHeaderController self)
    {
    if (IsLogicPw(self))
        {
        tThaPwHeaderCache *cache = ThaPwHeaderControllerCache(self);
        if (cache)
            return cache->subportVlanIndex;
        return cInvalidUint32;
        }

    return ThaModulePwTxPwSubPortVlanIndexGet(ModulePw(self), (AtPw)ThaPwHeaderControllerAdapterGet(self));
    }

static eBool ShouldEnableCwByDefault(ThaPwHeaderController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void MethodsInit(ThaPwHeaderController self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, PsnSet);
        mMethodOverride(m_methods, PsnGet);
        mMethodOverride(m_methods, NeedProtectOnTxDirection);
        mMethodOverride(m_methods, NeedProtectOnRxDirection);
        mMethodOverride(m_methods, PsnWarmRestore);
        mMethodOverride(m_methods, Debug);
        mMethodOverride(m_methods, PsnGetFromHardware);
        mMethodOverride(m_methods, PsnTypeSet);
        mMethodOverride(m_methods, HeaderLengthGet);
        mMethodOverride(m_methods, HeaderRead);
        mMethodOverride(m_methods, HeaderLengthSet);
        mMethodOverride(m_methods, HeaderWrite);
        mMethodOverride(m_methods, EthHeaderSet);
        mMethodOverride(m_methods, EthDestMacSet);
        mMethodOverride(m_methods, EthDestMacGet);
        mMethodOverride(m_methods, EthSrcMacSet);
        mMethodOverride(m_methods, EthSrcMacGet);
        mMethodOverride(m_methods, EthCVlanTpidSet);
        mMethodOverride(m_methods, EthSVlanTpidSet);
        mMethodOverride(m_methods, EthVlanSet);
        mMethodOverride(m_methods, EthCVlanGet);
        mMethodOverride(m_methods, EthSVlanGet);
        mMethodOverride(m_methods, EthCVlanTpidGet);
        mMethodOverride(m_methods, EthSVlanTpidGet);
        mMethodOverride(m_methods, NumVlansSet);
        mMethodOverride(m_methods, RtpTxSsrcSet);
        mMethodOverride(m_methods, RtpTxSsrcGet);
        mMethodOverride(m_methods, RtpTxPayloadTypeSet);
        mMethodOverride(m_methods, RtpTxPayloadTypeGet);
        mMethodOverride(m_methods, IsPrimary);
        mMethodOverride(m_methods, RxEnable);
        mMethodOverride(m_methods, RxIsEnable);
        mMethodOverride(m_methods, ApplyCellToHw);
        mMethodOverride(m_methods, RtpEnable);
        mMethodOverride(m_methods, RtpPayloadTypeCompare);
        mMethodOverride(m_methods, RtpPayloadTypeIsCompared);
        mMethodOverride(m_methods, RtpSsrcCompare);
        mMethodOverride(m_methods, RtpSsrcIsCompared);
        mMethodOverride(m_methods, EthPortQueueSet);
        mMethodOverride(m_methods, EthPortQueueGet);
        mMethodOverride(m_methods, EthPortMaxNumQueues);
        mMethodOverride(m_methods, NeedDisableTxWhenChangePsn);
        mMethodOverride(m_methods, StandbyEthCVlanGet);
        mMethodOverride(m_methods, StandbyEthSVlanGet);
        mMethodOverride(m_methods, SubPortVlanIndexSet);
        mMethodOverride(m_methods, SubPortVlanIndexGet);
        mMethodOverride(m_methods, StandbyEthCVlanTpidGet);
        mMethodOverride(m_methods, StandbyEthSVlanTpidGet);
        mMethodOverride(m_methods, ShouldEnableCwByDefault);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtObject(ThaPwHeaderController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(ThaPwHeaderController self)
    {
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPwHeaderController);
    }

ThaPwHeaderController ThaPwHeaderControllerObjectInit(ThaPwHeaderController self, ThaPwAdapter adapter)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Override */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;
    self->adapter = adapter;

    return self;
    }

ThaPwHeaderController ThaPwHeaderControllerNew(AtPw adapter)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwHeaderController newProvider = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newProvider == NULL)
        return NULL;

    /* Construct it */
    return ThaPwHeaderControllerObjectInit(newProvider, (ThaPwAdapter)adapter);
    }

ThaPwAdapter ThaPwHeaderControllerAdapterGet(ThaPwHeaderController self)
    {
    return (self) ? self->adapter : NULL;
    }

eAtModulePwRet ThaPwHeaderControllerPsnSet(ThaPwHeaderController self, AtPwPsn psn)
    {
    if (self)
        return mMethodsGet(self)->PsnSet(self, psn);
    return cAtErrorNullPointer;
    }

AtPwPsn ThaPwHeaderControllerPsnGet(ThaPwHeaderController self)
    {
    if (self)
        return mMethodsGet(self)->PsnGet(self);
    return NULL;
    }

eBool ThaPwHeaderControllerHbceIsActivated(ThaPwHeaderController self)
    {
    if (self)
        return self->hbceActivated;
    return cAtFalse;
    }

void ThaPwHeaderControllerDebug(ThaPwHeaderController self)
    {
    if (self)
        mMethodsGet(self)->Debug(self);
    }

eAtModulePwRet ThaPwHeaderControllerWarmRestore(ThaPwHeaderController self)
    {
    AtPwPsn pwPsn;

    if (self == NULL)
        return cAtErrorNullPointer;

    pwPsn = PsnGetFromHardware(self);
    if (pwPsn)
        {
        if (PsnWarmRestore(self, pwPsn) == cAtOk)
            self->pwPsnHeaderIsSet = cAtTrue;

        AtObjectDelete((AtObject)pwPsn);
        }

    return cAtOk;
    }

eAtModulePwRet ThaPwHeaderControllerEthHeaderSet(ThaPwHeaderController self, uint8 *destMac, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan)
    {
    if (self)
        return mMethodsGet(self)->EthHeaderSet(self, destMac, cVlan, sVlan);
    return cAtErrorNullPointer;
    }

eAtModulePwRet ThaPwHeaderControllerEthSourceMacSet(ThaPwHeaderController self, uint8 *srcMac)
    {
    if (self)
        return mMethodsGet(self)->EthSrcMacSet(self, srcMac);
    return cAtErrorNullPointer;
    }

eAtModulePwRet ThaPwHeaderControllerEthSVlanTpidSet(ThaPwHeaderController self, uint16 tpid)
    {
    if (self)
        return mMethodsGet(self)->EthSVlanTpidSet(self, tpid);
    return cAtErrorNullPointer;
    }

eAtModulePwRet ThaPwHeaderControllerEthCVlanTpidSet(ThaPwHeaderController self, uint16 tpid)
    {
    if (self)
        return mMethodsGet(self)->EthCVlanTpidSet(self, tpid);
    return cAtErrorNullPointer;
    }

uint16 ThaPwHeaderControllerEthCVlanTpidGet(ThaPwHeaderController self)
    {
    if (self)
        return mMethodsGet(self)->EthCVlanTpidGet(self);
    return 0;
    }

uint16 ThaPwHeaderControllerEthSVlanTpidGet(ThaPwHeaderController self)
    {
    if (self)
        return mMethodsGet(self)->EthSVlanTpidGet(self);
    return 0;
    }

eAtModulePwRet ThaPwHeaderControllerEthDestMacGet(ThaPwHeaderController self, uint8 *destMac)
    {
    if (self)
        return mMethodsGet(self)->EthDestMacGet(self, destMac);
    return cAtErrorNullPointer;
    }

eAtModulePwRet ThaPwHeaderControllerEthSourceMacGet(ThaPwHeaderController self, uint8 *srcMac)
    {
    if (self)
        return mMethodsGet(self)->EthSrcMacGet(self, srcMac);
    return cAtErrorNullPointer;
    }

eAtModulePwRet ThaPwHeaderControllerEthDestMacSet(ThaPwHeaderController self, uint8 *destMac)
    {
    if (self)
        return mMethodsGet(self)->EthDestMacSet(self, destMac);
    return cAtErrorNullPointer;
    }

tAtEthVlanTag* ThaPwHeaderControllerEthCVlanGet(ThaPwHeaderController self, tAtEthVlanTag *cVlan)
    {
    if (self)
        return mMethodsGet(self)->EthCVlanGet(self, cVlan);
    return NULL;
    }

eAtModulePwRet ThaPwHeaderControllerEthVlanSet(ThaPwHeaderController self, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan)
    {
    if (self)
        return mMethodsGet(self)->EthVlanSet(self, cVlan, sVlan);
    return cAtErrorNullPointer;
    }

tAtEthVlanTag* ThaPwHeaderControllerEthSVlanGet(ThaPwHeaderController self, tAtEthVlanTag *sVlan)
    {
    if (self)
        return mMethodsGet(self)->EthSVlanGet(self, sVlan);
    return NULL;
    }

static tThaPwHeaderCache *CacheCreate(ThaPwHeaderController self)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 memorySize = sizeof(tThaPwHeaderCache);
    tThaPwHeaderCache *newCache = mMethodsGet(osal)->MemAlloc(osal, memorySize);
    AtUnused(self);
    if (newCache == NULL)
        return NULL;

    /* Clear memory */
    mMethodsGet(osal)->MemInit(osal, newCache, 0, memorySize);

    return newCache;
    }

tThaPwHeaderCache *ThaPwHeaderControllerCache(ThaPwHeaderController self)
    {
    if (self == NULL)
        return NULL;

    if (self->cache == NULL)
        self->cache = CacheCreate(self);

    return self->cache;
    }

void ThaPwHeaderControllerCacheDelete(ThaPwHeaderController self)
    {
    AtOsal osal;

    if (self == NULL)
        return;

    if (self->cache == NULL)
        return;

    AtObjectDelete((AtObject)self->cache->psn);

    osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemFree(osal, self->cache);
    self->cache = NULL;
    }

eAtRet ThaPwHeaderControllerHbceActivate(ThaPwHeaderController self)
    {
    AtPw pw;
    ThaHbce hbce;
    eAtRet ret = cAtOk;
    eBool shouldReactivateLookup = cAtTrue;

    if (self == NULL)
        return cAtOk;

    pw = (AtPw)ThaPwHeaderControllerAdapterGet(self);
    hbce = ThaPwAdapterHbceGet(pw);

    if (!HbceCanActivate(self))
        return cAtOk;

    if (ThaModulePwHitlessHeaderChangeIsEnabled(ModulePw(self)) && SamePsnAndSameLabel(self->newPsn, self->currentPsn))
        shouldReactivateLookup = cAtFalse;

    /* PW is activated before, just change in PSN */
    if (HbceActivated(self))
        {
        AtPwPsn oldPsn = self->currentPsn;

        /* New PSN must be valid and different with the current one */
        if (self->newPsn == NULL)
            return cAtOk;

        /* If HBCE exist, just remove and add it again to have new configuration */
        if (hbce && shouldReactivateLookup)
            {
            ret |= ThaHbcePwRemove(hbce, self);
            self->currentPsn = self->newPsn;
            ret |= ThaHbcePwAdd(hbce, self);
            }

        /* Otherwise, just update database for new PSn */
        else
            self->currentPsn = self->newPsn;

        /* Done, also delete old one */
        if (oldPsn != self->newPsn)
            AtObjectDelete((AtObject)oldPsn);
        self->newPsn = NULL;
        }

    /* PW has not been activated yet, pretty sure that it is not in HBCE engine,
     * just add it to HBCE */
    else
        {
        self->currentPsn = self->newPsn;

        if (hbce)
            ret |= ThaHbcePwAdd(hbce, self);

        /* Mark it as activated */
        self->newPsn        = NULL;
        self->hbceActivated = cAtTrue;
        }

    if (!shouldReactivateLookup)
        return ret;

    if ((ret == cAtOk) && hbce)
        {
        ret |= ThaHbceApply2Hardware(hbce);
        ret |= mMethodsGet(self)->ApplyCellToHw(self);
        }

    /* Update PWE */
    ret |= mMethodsGet(self)->PsnTypeSet(self);

    /* Product uses HBCE will do not care the content of this function */
    ret |= ThaClaPwControllerPwLabelSet(ThaPwAdapterClaPwController(mAdapter(pw)), pw, AtPwPsnGet(pw));
    if (ret == cAtOk)
        ThaModulePwLabelSaveToHwStorage(ModulePw(self), pw, ThaHbcePsnLabelForHashing(hbce, self->currentPsn)); /* TODO: need to consider this feature */

    return ret;
    }

eAtRet ThaPwHeaderControllerHbceDeactivate(ThaPwHeaderController self)
    {
    eAtRet ret = cAtOk;
    AtPw pw;
    ThaHbce hbce;

    if (self == NULL)
        return cAtOk;

    if (!HbceActivated(self))
        return cAtOk;

    pw = (AtPw)ThaPwHeaderControllerAdapterGet(self);

    /* Remove it out of HBCE engine and mark this PW as not activated */
    hbce = ThaPwAdapterHbceGet(pw);
    if (hbce)
        {
        ret |= ThaHbcePwRemove(hbce, self);
        if (ret == cAtOk)
            {
            ret |= ThaHbceApply2Hardware(hbce);
            ret |= mMethodsGet(self)->ApplyCellToHw(self);
            }
        }

    /* For next activation */
    self->newPsn        = self->currentPsn;
    self->currentPsn    = NULL;
    self->hbceActivated = cAtFalse;

    ThaClaPwControllerPwLabelSet(ThaPwAdapterClaPwController(mAdapter(pw)), pw, NULL);

    return ret;
    }

eAtRet ThaPwHeaderControllerTxHeaderLengthReset(ThaPwHeaderController self)
    {
    eAtRet ret;

    if (self == NULL)
        return cAtOk;

    ret = ThaModuleCosPwHeaderLengthSet(ModuleCos(self), (AtPw)ThaPwHeaderControllerAdapterGet(self), 0);
    self->ethHdrLength = 0;
    self->pwPsnHeaderIsSet = cAtFalse;

    return ret;
    }

eAtRet ThaPwHeaderControllerHbceMemoryCellContentSet(ThaPwHeaderController self, ThaHbceMemoryCellContent cellContent)
    {
    if (self == NULL)
        mChannelError(ThaPwHeaderControllerAdapterGet(self), cAtErrorNullPointer);

    self->hbceMemoryCellContent = cellContent;
    return cAtOk;
    }

ThaHbceMemoryCellContent ThaPwHeaderControllerHbceMemoryCellContentGet(ThaPwHeaderController self)
    {
    if (self)
        return self->hbceMemoryCellContent;
    return NULL;
    }

eBool ThaPwHeaderControllerIsPrimary(ThaPwHeaderController self)
    {
    if (self)
        return mMethodsGet(self)->IsPrimary(self);
    return cAtFalse;
    }

eAtModulePwRet ThaPwHeaderControllerRtpTxSsrcSet(ThaPwHeaderController self, uint32 ssrc)
    {
    if (self)
        return mMethodsGet(self)->RtpTxSsrcSet(self, ssrc);
    return cAtErrorNullPointer;
    }

uint32 ThaPwHeaderControllerRtpTxSsrcGet(ThaPwHeaderController self)
    {
    if (self)
        return mMethodsGet(self)->RtpTxSsrcGet(self);
    return 0;
    }

eAtModulePwRet ThaPwHeaderControllerRtpTxPayloadTypeSet(ThaPwHeaderController self, uint8 payloadType)
    {
    if (self)
        return mMethodsGet(self)->RtpTxPayloadTypeSet(self, payloadType);
    return cAtErrorNullPointer;
    }

uint8 ThaPwHeaderControllerRtpTxPayloadTypeGet(ThaPwHeaderController self)
    {
    if (self)
        return mMethodsGet(self)->RtpTxPayloadTypeGet(self);
    return 0;
    }

uint8* ThaPwHeaderControllerHwHeaderArrayGet(ThaPwHeaderController self, uint8 *headerLenInByte)
    {
    uint8 *sharedBuffer;
    uint32 sharedBufferSize;

    /* Get shared buffer */
    sharedBuffer = SharedHeaderBuffer(self, &sharedBufferSize);

    /* Adjust buffer size */
    *headerLenInByte = mMethodsGet(self)->HeaderLengthGet(self);
    if (*headerLenInByte > sharedBufferSize)
        *headerLenInByte = (uint8)sharedBufferSize;

    if (mMethodsGet(self)->HeaderRead(self, sharedBuffer, *headerLenInByte) != cAtOk)
        *headerLenInByte = 0;

    return sharedBuffer;
    }

eBool ThaPwHeaderControllerIsSimulated(ThaPwHeaderController self)
    {
    if (self)
        return IsSimulated(self);
    return cAtFalse;
    }

tThaPwHeaderControllerSimulation *ThaPwHeaderControllerSimulationDatabase(ThaPwHeaderController self)
    {
    return (self) ? SimulationDatabase(self) : NULL;
    }

eAtModulePwRet ThaPwHeaderControllerRtpEnable(ThaPwHeaderController self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->RtpEnable(self, enable);
    return cAtErrorNullPointer;
    }

eAtModulePwRet ThaPwHeaderControllerRtpSsrcCompare(ThaPwHeaderController self, eBool enableCompare)
    {
    if (self)
        return mMethodsGet(self)->RtpSsrcCompare(self, enableCompare);
    return cAtErrorNullPointer;
    }

eBool ThaPwHeaderControllerRtpSsrcIsCompared(ThaPwHeaderController self)
    {
    if (self)
        return mMethodsGet(self)->RtpSsrcIsCompared(self);
    return cAtFalse;
    }

eAtModulePwRet ThaPwHeaderControllerRtpPayloadTypeCompare(ThaPwHeaderController self, eBool enableCompare)
    {
    if (self)
        return mMethodsGet(self)->RtpPayloadTypeCompare(self, enableCompare);
    return cAtErrorNullPointer;
    }

eBool ThaPwHeaderControllerRtpPayloadTypeIsCompared(ThaPwHeaderController self)
    {
    if (self)
        return mMethodsGet(self)->RtpPayloadTypeIsCompared(self);
    return cAtFalse;
    }

eAtRet ThaPwHeaderControllerEthPortQueueSet(ThaPwHeaderController self, uint8 queueId)
    {
    if (self)
        return mMethodsGet(self)->EthPortQueueSet(self, queueId);
    return cAtErrorNullPointer;
    }

uint8 ThaPwHeaderControllerEthPortQueueGet(ThaPwHeaderController self)
    {
    if (self)
        return mMethodsGet(self)->EthPortQueueGet(self);
    return 0;
    }

uint8 ThaPwHeaderControllerEthPortMaxNumQueues(ThaPwHeaderController self)
    {
    if (self)
        return mMethodsGet(self)->EthPortMaxNumQueues(self);
    return 0;
    }

uint8 ThaPwHeaderControllerHeaderLengthGet(ThaPwHeaderController self)
    {
    if (self)
        return mMethodsGet(self)->HeaderLengthGet(self);
    return 0;
    }

/* Use this function to set source MAC with address got from Ethernet port */
void ThaPwHeaderControllerEthPortSrcMacSet(ThaPwHeaderController self, uint8 *srcMac)
    {
    if (self)
        EthSrcMacSetHelper(self, srcMac);
    }

eBool ThaPwHeaderControllerSrcMacIsSetByUser(ThaPwHeaderController self)
    {
    if (self)
        return self->srcMacIsSetByUser;
    return cAtFalse;
    }

eAtModulePwRet ThaPwHeaderControllerEthSourceMacGetFromDb(ThaPwHeaderController self, uint8 *srcMac)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwAdapter adapter = ThaPwHeaderControllerAdapterGet(self);

    if (self == NULL)
        return cAtErrorNullPointer;

    /* If source MAC is not set before, just simply return Ethernet port source MAC */
    if (self->srcMacIsSetByUser == cAtFalse)
        {
        AtEthPort port = AtPwEthPortGet((AtPw)adapter);
        if (port != NULL)
            AtEthPortSourceMacAddressGet(port, srcMac);

        return cAtOk;
        }

    mMethodsGet(osal)->MemCpy(osal, srcMac, self->srcMac, sizeof(self->srcMac));

    return cAtOk;
    }

eAtRet ThaPwHeaderControllerSubPortVlanIndexSet(ThaPwHeaderController self, uint32 subPortVlanIndex)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (SubPortVlanIndexIsInRange(self, subPortVlanIndex))
        return mMethodsGet(self)->SubPortVlanIndexSet(self, subPortVlanIndex);

    return cAtErrorInvlParm;
    }

uint32 ThaPwHeaderControllerSubPortVlanIndexGet(ThaPwHeaderController self)
    {
    if (self)
        return mMethodsGet(self)->SubPortVlanIndexGet(self);
    return cInvalidUint32;
    }

eAtRet ThaPwHeaderControllerHwRawHeaderSet(ThaPwHeaderController self, uint8 *headerBuffer, uint8 length)
    {
    if (self)
        return HwHeaderArraySet(self, headerBuffer, length, NULL, cThaPwAdapterHeaderWriteModeRaw);
    return cAtOk;
    }

eBool ThaPwHeaderControllerInAccessible(ThaPwHeaderController self)
    {
    if (self == NULL)
        return cAtFalse;

    return AtChannelInAccessible((AtChannel)ThaPwHeaderControllerAdapterGet(self));
    }

eBool ThaPwHeaderControllerShouldPreventReconfig(ThaPwHeaderController self)
    {
    if (self)
        return AtChannelShouldPreventReconfigure((AtChannel)ThaPwHeaderControllerAdapterGet(self));
    return cAtTrue;
    }

eBool ThaPwHeaderControllerCwShouldEnableByDefault(ThaPwHeaderController self)
    {
    if (self)
        return mMethodsGet(self)->ShouldEnableCwByDefault(self);
    return cAtTrue;
    }

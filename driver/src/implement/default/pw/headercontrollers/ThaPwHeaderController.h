/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : ThaPwHeaderController.h
 * 
 * Created Date: May 12, 2015
 *
 * Description : Header controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPWHEADERCONTROLLER_H_
#define _THAPWHEADERCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../man/ThaDeviceInternal.h"
#include "../adapters/ThaPwAdapter.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPwHeaderControllerSimulation
    {
    uint8 psnHeader[128];
    uint8 psnHeaderLength;
    }tThaPwHeaderControllerSimulation;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPwHeaderController ThaPwHeaderControllerNew(AtPw adapter);
ThaPwHeaderController ThaPwBackupHeaderControllerNew(ThaPwAdapter adapter);
ThaPwAdapter ThaPwHeaderControllerAdapterGet(ThaPwHeaderController self);
eAtModulePwRet ThaPwHeaderControllerPsnSet(ThaPwHeaderController self, AtPwPsn psn);
AtPwPsn ThaPwHeaderControllerPsnGet(ThaPwHeaderController self);
eBool ThaPwHeaderControllerHbceIsActivated(ThaPwHeaderController self);
eAtModulePwRet ThaPwHeaderControllerPsnSet(ThaPwHeaderController self, AtPwPsn psn);
AtPwPsn ThaPwHeaderControllerPsnGet(ThaPwHeaderController self);
eBool ThaPwHeaderControllerHbceIsActivated(ThaPwHeaderController self);
eAtModulePwRet ThaPwHeaderControllerEthHeaderSet(ThaPwHeaderController self, uint8 *destMac, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan);
eAtModulePwRet ThaPwHeaderControllerEthSourceMacSet(ThaPwHeaderController self, uint8 *srcMac);
eAtModulePwRet ThaPwHeaderControllerEthSourceMacGet(ThaPwHeaderController self, uint8 *srcMac);
eAtModulePwRet ThaPwHeaderControllerEthDestMacSet(ThaPwHeaderController self, uint8 *destMac);
eAtModulePwRet ThaPwHeaderControllerEthDestMacGet(ThaPwHeaderController self, uint8 *destMac);
eAtModulePwRet ThaPwHeaderControllerEthVlanSet(ThaPwHeaderController self, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan);
tAtEthVlanTag* ThaPwHeaderControllerEthCVlanGet(ThaPwHeaderController self, tAtEthVlanTag *cVlan);
tAtEthVlanTag* ThaPwHeaderControllerEthSVlanGet(ThaPwHeaderController self, tAtEthVlanTag *sVlan);
eAtModulePwRet ThaPwHeaderControllerEthSourceMacSet(ThaPwHeaderController self, uint8 *srcMac);
eAtModulePwRet ThaPwHeaderControllerEthSVlanTpidSet(ThaPwHeaderController self, uint16 tpid);
eAtModulePwRet ThaPwHeaderControllerEthCVlanTpidSet(ThaPwHeaderController self, uint16 tpid);
uint16 ThaPwHeaderControllerEthCVlanTpidGet(ThaPwHeaderController self);
uint16 ThaPwHeaderControllerEthSVlanTpidGet(ThaPwHeaderController self);

eAtRet ThaPwHeaderControllerSubPortVlanIndexSet(ThaPwHeaderController self, uint32 subPortVlanIndex);
uint32 ThaPwHeaderControllerSubPortVlanIndexGet(ThaPwHeaderController self);

eAtRet ThaPwHeaderControllerHbceActivate(ThaPwHeaderController self);
eAtRet ThaPwHeaderControllerHbceDeactivate(ThaPwHeaderController self);
eAtRet ThaPwHeaderControllerHbceMemoryCellContentSet(ThaPwHeaderController self, ThaHbceMemoryCellContent cellContent);
ThaHbceMemoryCellContent ThaPwHeaderControllerHbceMemoryCellContentGet(ThaPwHeaderController self);

eAtRet ThaPwHeaderControllerTxHeaderLengthReset(ThaPwHeaderController self);
uint8 *ThaPwHeaderControllerHwHeaderArrayGet(ThaPwHeaderController self, uint8 *headerLenInByte);
eBool ThaPwHeaderControllerIsPrimary(ThaPwHeaderController self);
eAtRet ThaPwHeaderControllerHwRawHeaderSet(ThaPwHeaderController self, uint8 *headerBuffer, uint8 length);
eBool ThaPwHeaderControllerInAccessible(ThaPwHeaderController self);
eBool ThaPwHeaderControllerShouldPreventReconfig(ThaPwHeaderController self);

void ThaPwHeaderControllerDebug(ThaPwHeaderController self);
eAtModulePwRet ThaPwHeaderControllerWarmRestore(ThaPwHeaderController self);

/* RTP */
eAtModulePwRet ThaPwHeaderControllerRtpEnable(ThaPwHeaderController self, eBool enable);
eAtModulePwRet ThaPwHeaderControllerRtpTxSsrcSet(ThaPwHeaderController self, uint32 ssrc);
uint32 ThaPwHeaderControllerRtpTxSsrcGet(ThaPwHeaderController self);
eAtModulePwRet ThaPwHeaderControllerRtpTxPayloadTypeSet(ThaPwHeaderController self, uint8 payloadType);
uint8 ThaPwHeaderControllerRtpTxPayloadTypeGet(ThaPwHeaderController self);
eAtModulePwRet ThaPwHeaderControllerRtpSsrcCompare(ThaPwHeaderController self, eBool enableCompare);
eBool ThaPwHeaderControllerRtpSsrcIsCompared(ThaPwHeaderController self);
eAtModulePwRet ThaPwHeaderControllerRtpPayloadTypeCompare(ThaPwHeaderController self, eBool enableCompare);
eBool ThaPwHeaderControllerRtpPayloadTypeIsCompared(ThaPwHeaderController self);
uint8 ThaPwHeaderControllerHeaderLengthGet(ThaPwHeaderController self);

eBool ThaPwHeaderControllerIsSimulated(ThaPwHeaderController self);
tThaPwHeaderControllerSimulation *ThaPwHeaderControllerSimulationDatabase(ThaPwHeaderController self);
eAtRet ThaPwHeaderControllerEthPortQueueSet(ThaPwHeaderController self, uint8 queueId);
uint8 ThaPwHeaderControllerEthPortQueueGet(ThaPwHeaderController self);
uint8 ThaPwHeaderControllerEthPortMaxNumQueues(ThaPwHeaderController self);

void ThaPwHeaderControllerEthPortSrcMacSet(ThaPwHeaderController self, uint8 *srcMac);
eBool ThaPwHeaderControllerSrcMacIsSetByUser(ThaPwHeaderController self);
eAtModulePwRet ThaPwHeaderControllerEthSourceMacGetFromDb(ThaPwHeaderController self, uint8 *srcMac);
eBool ThaPwHeaderControllerCwShouldEnableByDefault(ThaPwHeaderController self);

/* Concrete products controllers */
ThaPwHeaderController Tha60210061DccPwHeaderControllerNew(ThaPwAdapter adapter);
ThaPwHeaderController Tha60290081PwHeaderControllerNew(ThaPwAdapter adapter);

#endif /* _THAPWHEADERCONTROLLER_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : ThaPwHeaderControllerInternal.h
 * 
 * Created Date: May 9, 2015
 *
 * Description : PW header controller definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPWHEADERCONTROLLERINTERNAL_H_
#define _THAPWHEADERCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/man/AtDriverInternal.h" /* For OSAL macros */
#include "ThaPwHeaderController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPwHeaderControllerMethods
    {
    eAtModulePwRet (*PsnSet)(ThaPwHeaderController self, AtPwPsn psn);
    AtPwPsn (*PsnGet)(ThaPwHeaderController self);
    eBool (*NeedProtectOnTxDirection)(ThaPwHeaderController self);
    eBool (*NeedProtectOnRxDirection)(ThaPwHeaderController self);
    eAtModulePwRet (*PsnWarmRestore)(ThaPwHeaderController self, AtPwPsn psn);
    void (*Debug)(ThaPwHeaderController self);
    AtPwPsn (*PsnGetFromHardware)(ThaPwHeaderController self);
    eAtModulePwRet (*PsnTypeSet)(ThaPwHeaderController self);

    eAtModulePwRet (*HeaderLengthSet)(ThaPwHeaderController self, uint8 hdrLenInByte);
    uint8 (*HeaderLengthGet)(ThaPwHeaderController self);
    eAtModulePwRet (*HeaderRead)(ThaPwHeaderController self, uint8 *buffer, uint8 headerLenInByte);
    eAtModulePwRet (*HeaderWrite)(ThaPwHeaderController self, uint8 *buffer, uint8 headerLenInByte, AtPwPsn psn, eThaPwAdapterHeaderWriteMode writeMode);

    eAtRet (*NumVlansSet)(ThaPwHeaderController pw, uint8 numVlans);

    eAtModulePwRet (*EthHeaderSet)(ThaPwHeaderController self, uint8 *destMac, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan);
    eAtModulePwRet (*EthDestMacSet)(ThaPwHeaderController self, uint8 *destMac);
    eAtModulePwRet (*EthDestMacGet)(ThaPwHeaderController self, uint8 *destMac);
    eAtModulePwRet (*EthSrcMacSet)(ThaPwHeaderController self, uint8 *srcMac);
    eAtModulePwRet (*EthSrcMacGet)(ThaPwHeaderController self, uint8 *srcMac);
    eAtModulePwRet (*EthVlanSet)(ThaPwHeaderController self, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan);
    tAtEthVlanTag* (*EthCVlanGet)(ThaPwHeaderController self, tAtEthVlanTag *cVlan);
    tAtEthVlanTag* (*EthSVlanGet)(ThaPwHeaderController self, tAtEthVlanTag *sVlan);
    tAtEthVlanTag* (*StandbyEthCVlanGet)(ThaPwHeaderController self, tAtEthVlanTag *cVlan);
    tAtEthVlanTag* (*StandbyEthSVlanGet)(ThaPwHeaderController self, tAtEthVlanTag *sVlan);

    /* Subport VLAN */
    eAtRet (*SubPortVlanIndexSet)(ThaPwHeaderController self, uint32 subPortVlanIndex);
    uint32 (*SubPortVlanIndexGet)(ThaPwHeaderController self);

    eAtModulePwRet (*RtpEnable)(ThaPwHeaderController self, eBool enable);
    eAtModulePwRet (*RtpTxSsrcSet)(ThaPwHeaderController self, uint32 ssrc);
    uint32 (*RtpTxSsrcGet)(ThaPwHeaderController self);
    eAtModulePwRet (*RtpTxPayloadTypeSet)(ThaPwHeaderController self, uint8 payloadType);
    uint8 (*RtpTxPayloadTypeGet)(ThaPwHeaderController self);
    eBool (*IsPrimary)(ThaPwHeaderController self);
    eBool (*RxIsEnable)(ThaPwHeaderController self);
    eAtRet (*RxEnable)(ThaPwHeaderController self, eBool enable);
    eAtRet (*ApplyCellToHw)(ThaPwHeaderController self);
    eAtModulePwRet (*RtpSsrcCompare)(ThaPwHeaderController self, eBool enableCompare);
    eBool (*RtpSsrcIsCompared)(ThaPwHeaderController self);
    eAtModulePwRet (*RtpPayloadTypeCompare)(ThaPwHeaderController self, eBool enableCompare);
    eBool (*RtpPayloadTypeIsCompared)(ThaPwHeaderController self);
    eAtRet (*EthPortQueueSet)(ThaPwHeaderController self, uint8 queueId);
    uint8 (*EthPortQueueGet)(ThaPwHeaderController self);
    uint8 (*EthPortMaxNumQueues)(ThaPwHeaderController self);
    eBool (*NeedDisableTxWhenChangePsn)(ThaPwHeaderController self);
    uint16 (*StandbyEthCVlanTpidGet)(ThaPwHeaderController self);
    uint16 (*StandbyEthSVlanTpidGet)(ThaPwHeaderController self);
    eAtModulePwRet (*EthCVlanTpidSet)(ThaPwHeaderController self, uint16 tpid);
    eAtModulePwRet (*EthSVlanTpidSet)(ThaPwHeaderController self, uint16 tpid);
    uint16 (*EthCVlanTpidGet)(ThaPwHeaderController self);
    uint16 (*EthSVlanTpidGet)(ThaPwHeaderController self);
    eBool (*ShouldEnableCwByDefault)(ThaPwHeaderController self);
    }tThaPwHeaderControllerMethods;

typedef struct tThaPwHeaderCache
    {
    AtPwPsn psn;

    /* ETH header */
    tAtEthVlanTag cVlan;
    tAtEthVlanTag sVlan;
    uint8 numVlans;
    uint8 destMac[6];
    uint16 tpidSVlan;
    uint16 tpidCVlan;

    /* TX RTP */
    uint32 txRtpSsrc;
    uint8 txRtpPayloadType;

    /* Subport VLAN */
    uint32 subportVlanIndex;
    }tThaPwHeaderCache;

typedef struct tThaPwHeaderController
    {
    tAtObject super;
    const tThaPwHeaderControllerMethods* methods;

    ThaPwAdapter adapter;
    
    /* To save configuration of application when no real HW PW is assigned */
    tThaPwHeaderCache *cache;

    /* PSN */
    AtPwPsn currentPsn;
    AtPwPsn newPsn;
    eBool hbceActivated;
    ThaHbceMemoryCellContent hbceMemoryCellContent;
    AtPwPsn replicaPsn;
    eBool pwPsnHeaderIsSet;

    /* SMAC */
    eBool srcMacIsSetByUser;
    uint8 srcMac[6];

    uint8 ethHdrLength;

    /* To cache byte index of VLANs TPID in pw header */
    uint8 cVlanTpidIndex;
    uint8 sVlanTpidIndex;

    /* For simulation */
    tThaPwHeaderControllerSimulation *simulation;
    }tThaPwHeaderController;

typedef struct tThaPwBackupHeaderController
    {
    tThaPwHeaderController super;

    /* Private data */
    uint8 headerLength;
    }tThaPwBackupHeaderController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPwHeaderController ThaPwHeaderControllerObjectInit(ThaPwHeaderController self, ThaPwAdapter adapter);
ThaPwHeaderController ThaPwBackupHeaderControllerObjectInit(ThaPwHeaderController self, ThaPwAdapter adapter);

tThaPwHeaderCache *ThaPwHeaderControllerCache(ThaPwHeaderController self);
void ThaPwHeaderControllerCacheDelete(ThaPwHeaderController self);

#endif /* _THAPWHEADERCONTROLLERINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited 
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : ThaPwFrVcVc.c
 *
 * Created Date: Oct 12, 2016 
 *
 * Description : Frame Relay Virtual Circuit PW
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "adapters/ThaPwAdapterInternal.h"
#include "ThaPwInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mHdlcPwAdapter(self) ((ThaPwHdlcAdapter)Adapter((AtPw)self))
/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtObjectMethods  m_AtObjectOverride;
static tAtChannelMethods m_AtChannelOverride;
static tAtPwMethods      m_AtPwOverride;
static tAtPwHdlcMethods  m_AtPwHdlcOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;
static const tAtPwHdlcMethods  *m_AtPwHdlcMethods  = NULL;

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPw Adapter(AtPw pw)
    {
    return (AtPw)((ThaPwFr)pw)->adapter;
    }

#include "ThaPwCommonSetup.h"

static void Delete(AtObject self)
    {
    ThaPwAdapterDelete((AtPw)self);

    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static eAtPwHdlcPayloadType PayloadTypeGet(AtPwHdlc self)
    {
    return ((ThaPwFr)self)->payloadType;
    }

static eAtModulePwRet PayloadTypeSet(AtPwHdlc self, eAtPwHdlcPayloadType payloadType)
    {
    if (payloadType == cAtPwHdlcPayloadTypeInvalid)
        return cAtErrorModeNotSupport;

    ((ThaPwFr)self)->payloadType = payloadType;

    return cAtOk;
    }

static void OverrideAtObject(AtPw self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtPwHdlc(AtPw self)
    {
    AtPwHdlc hdlc = (AtPwHdlc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPwHdlcMethods = mMethodsGet(hdlc);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPwHdlcOverride, m_AtPwHdlcMethods, sizeof(m_AtPwHdlcOverride));

        mMethodOverride(m_AtPwHdlcOverride, PayloadTypeSet);
        mMethodOverride(m_AtPwHdlcOverride, PayloadTypeGet);
        }

    mMethodsSet(hdlc, &m_AtPwHdlcOverride);
    }

static void Override(AtPw self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtPw(self);
    OverrideAtPwHdlc(self);
    }

/* To initialize object */
static AtPw ObjectInit(AtPw self, AtFrVirtualCircuit frVc, AtModulePw module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaPwFr));

    /* Super constructor should be called first */
    if (AtPwFrObjectInit((AtPwFr)self, AtChannelIdGet((AtChannel)frVc), module) == NULL)
        return NULL;

    /* Only initialize method structures one time */
    Override(self);
    m_methodsInit = 1;

    /* PW adapter */
    ((ThaPwFr)self)->adapter = ThaPwHdlcAdapterNew(self);

    return self;
    }

AtPw ThaPwFrNew(AtModulePw self, AtFrVirtualCircuit frVc)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPw newPw = mMethodsGet(osal)->MemAlloc(osal, sizeof(tThaPwFr));
    if (newPw == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPw, frVc, self);
    }

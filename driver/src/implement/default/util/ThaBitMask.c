/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Util
 *
 * File        : ThaBitMask.c
 *
 * Created Date: Jul 30, 2013
 *
 * Description : Bit mask
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaBitMask.h"
#include "../../../generic/man/AtDriverInternal.h"
#include "../../../util/coder/AtCoderUtil.h"

/*--------------------------- Define -----------------------------------------*/
#define cNumBitsInDword 32

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaBitMask)self)

#define mDwordIndex(bitPosition_)  ((bitPosition_) / cNumBitsInDword)
#define mLocalBit(bitPosition_)    ((bitPosition_) % cNumBitsInDword)
#define mPositionMask(bitPosition) (cBit0 << mLocalBit(bitPosition))

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaBitMask
    {
    tAtObject super;

    /* Private data */
    uint32 *masks;
    uint32 numBits;
    uint32 numDwords;
    }tThaBitMask;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaBitMask);
    }

static void Delete(AtObject self)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemFree(osal, mThis(self)->masks);
    mThis(self)->masks = NULL;

    m_AtObjectMethods->Delete(self);
    }

static uint32 NumDwords(ThaBitMask self)
    {
    if (self->numDwords == 0)
        self->numDwords = (self->numBits / cNumBitsInDword) + (((self->numBits % cNumBitsInDword) == 0) ? 0 : 1);
    return self->numDwords;
    }

static uint32 *MasksCreate(ThaBitMask self)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 memorySize;
    uint32 *masks;

    memorySize = sizeof(uint32) * NumDwords(self);
    masks = mMethodsGet(osal)->MemAlloc(osal, memorySize);
    if (masks == NULL)
        return NULL;
    mMethodsGet(osal)->MemInit(osal, masks, 0, memorySize);

    return masks;
    }

static uint32 *Masks(ThaBitMask self)
    {
    if (self == NULL)
        return NULL;

    if (self->masks == NULL)
        self->masks = MasksCreate(self);
    return self->masks;
    }

static uint8 FirstZeroBit(uint32 value)
    {
    uint8 bit_i = 0;

    while (bit_i < 32)
        {
        if ((value & (cBit0 << bit_i)) == 0)
            return bit_i;

        bit_i = (uint8)(bit_i + 1);
        }

    /* Never happen */
    return 0;
    }

static uint8 LastZeroBit(ThaBitMask self, uint32 dword_i, uint32 value)
    {
    uint8 bit_i = 0;

    while (bit_i < 32)
        {
        uint32 bitPosition = (dword_i * cNumBitsInDword) + (uint32)(31 - bit_i);

        if (ThaBitMaskBitPositionIsValid(self, bitPosition))
            {
            if ((value & (cBit31 >> bit_i)) == 0)
                return (uint8)(31 - bit_i);
            }

        bit_i = (uint8)(bit_i + 1);
        }

    /* Never happen */
    return 0;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaBitMask object = (ThaBitMask)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt32Array(masks, object->numDwords);
    mEncodeUInt(numBits);
    mEncodeUInt(numDwords);
    }

static void Init(ThaBitMask self)
    {
    uint32 memorySize = sizeof(uint32) * self->numDwords;
    if (self->masks && (memorySize > 0))
        AtOsalMemInit(self->masks, 0, memorySize);
    }

static eBool DwordIsFull(ThaBitMask self, uint32* masks, int32 dword_i)
    {
    if ((dword_i < (int32)NumDwords(self) - 1) || ((self->numBits % cNumBitsInDword) == 0))
        return (masks[dword_i] == cBit31_0) ? cAtTrue : cAtFalse;

    if (FirstZeroBit(masks[dword_i]) >= (self->numBits % cNumBitsInDword))
        return cAtTrue;

    return cAtFalse;
    }

static void OverrideAtObject(ThaBitMask self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(ThaBitMask self)
    {
    OverrideAtObject(self);
    }

static ThaBitMask ObjectInit(ThaBitMask self, uint32 numBits)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    self->numBits = numBits;

    return self;
    }

ThaBitMask ThaBitMaskNew(uint32 numBits)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaBitMask newMask = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newMask == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newMask, numBits);
    }

uint32 ThaBitMaskFirstZeroBitFromPosition(ThaBitMask self, uint32 bitPosition)
    {
    uint32 startDword = bitPosition / cNumBitsInDword;
    uint32 dword_i;
    uint32 *masks = Masks(self);

    if (masks == NULL)
        return cBit31_0;

    for (dword_i = startDword; dword_i < NumDwords(self); dword_i++)
        {
        /* Dword is full */
        if (masks[dword_i] == cBit31_0)
            continue;
        return (dword_i * cNumBitsInDword) + FirstZeroBit(masks[dword_i]);
        }

    /* No bit */
    return cBit31_0;
    }

uint32 ThaBitMaskFirstZeroBit(ThaBitMask self)
    {
    return ThaBitMaskFirstZeroBitFromPosition(self, 0);
    }

uint32 ThaBitMaskLastZeroBit(ThaBitMask self)
    {
    int32 dword_i;
    uint32 *masks = Masks(self);

    if (masks == NULL)
        return cBit31_0;

    for (dword_i = (int32)NumDwords(self) - 1; dword_i >= 0; dword_i--)
        {
        /* Dword is full */
        if (DwordIsFull(self, masks, dword_i))
            continue;

        return ((uint32)dword_i * cNumBitsInDword) + LastZeroBit(self, (uint32)dword_i, masks[dword_i]);
        }

    /* No bit */
    return cBit31_0;
    }

eBool ThaBitMaskBitPositionIsValid(ThaBitMask self, uint32 bitPosition)
    {
    if (self)
        return ((bitPosition < self->numBits) ? cAtTrue : cAtFalse);

    return cAtFalse;
    }

uint8 ThaBitMaskBitVal(ThaBitMask self, uint32 bitPosition)
    {
    uint32 dwordIndex;
    uint32 *masks;

    if (!ThaBitMaskBitPositionIsValid(self, bitPosition))
        return 0xFF;

    masks      = Masks(self);
    if (masks == NULL)
        return 0xFF;

    dwordIndex = mDwordIndex(bitPosition);
    return (masks[dwordIndex] & mPositionMask(bitPosition)) ? 1 : 0;
    }

void ThaBitMaskClearBit(ThaBitMask self, uint32 bitPosition)
    {
    uint32 dwordIndex;
    uint32 *masks;

    if (!ThaBitMaskBitPositionIsValid(self, bitPosition))
        return;

    masks = Masks(self);
    if (masks == NULL)
        return;

    dwordIndex = mDwordIndex(bitPosition);
    masks[dwordIndex] = masks[dwordIndex] & (~mPositionMask(bitPosition));
    }

void ThaBitMaskSetBit(ThaBitMask self, uint32 bitPosition)
    {
    uint32 dwordIndex;
    uint32 *masks;

    if (!ThaBitMaskBitPositionIsValid(self, bitPosition))
        return;

    masks      = Masks(self);
    if (masks == NULL)
        return;

    dwordIndex = mDwordIndex(bitPosition);
    masks[dwordIndex] = masks[dwordIndex] | mPositionMask(bitPosition);
    }

void ThaBitMaskDebug(ThaBitMask self)
    {
    uint32 dword_i;
    uint32 *masks;

    if ((self == NULL) || (self->masks == NULL))
        return;

    masks = Masks(self);
    if (masks == NULL)
        return;

    for (dword_i = 0; dword_i < NumDwords(self); dword_i++)
        {
        AtPrintc(cSevNormal, "- [%u]: ", dword_i);
        if (masks[dword_i] == cBit31_0)
            AtPrintc(cSevCritical, "Full");
        else if (masks[dword_i] == 0)
            AtPrintc(cSevNormal, "Free");
        else
            AtPrintc(cSevInfo, "0x%08x", masks[dword_i]);
        AtPrintc(cSevNormal, "\r\n");
        }
    }

void ThaBitMaskInit(ThaBitMask self)
    {
    if (self)
        Init(self);
    }

eBool ThaBitMaskIsAllZeroBits(ThaBitMask self)
    {
    uint32 dword_i;
    uint32 *masks = Masks(self);

    if (self == NULL || masks == NULL)
        return cAtFalse;

    for (dword_i = 0; dword_i < NumDwords(self); dword_i++)
        {
        /* Any bit differs from zero */
        if (masks[dword_i])
            return cAtFalse;
        }

    return cAtTrue;
    }

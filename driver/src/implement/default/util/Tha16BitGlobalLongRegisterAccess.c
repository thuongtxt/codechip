/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : UTIL
 *
 * File        : Tha16BitGlobalLongRegisterAccess.c
 *
 * Created Date: May 10, 2013
 *
 * Description : Long register access of product 2400
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../generic/util/AtLongRegisterAccessInternal.h"
#include "ThaUtil.h"

/*--------------------------- Define -----------------------------------------*/
/* Hold registers for long read operations */
#define cReadHoldField31_16Adr    0x000021
#define cReadHoldField47_32Adr    0x000022
#define cReadHoldField63_48Adr    0x000023
#define cReadHoldField79_64Adr    0x000024
#define cReadHoldField95_80Adr    0x000025
#define cReadHoldField111_96Adr   0x000026
#define cReadHoldField127_112Adr  0x000027
#define cReadHoldField143_128Adr  0x000028
#define cReadHoldField159_144Adr  0x000029

/* Hold registers for long write operations */
#define cWriteHoldField31_16Adr   0x000011
#define cWriteHoldField47_32Adr   0x000012
#define cWriteHoldField63_48Adr   0x000013
#define cWriteHoldField79_64Adr   0x000014
#define cWriteHoldField95_80Adr   0x000015
#define cWriteHoldField111_96Adr  0x000016
#define cWriteHoldField127_112Adr 0x000017
#define cWriteHoldField143_128Adr 0x000018
#define cWriteHoldField159_144Adr 0x000019

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha16BitGlobalLongRegisterAccess
    {
    tAtLongRegisterAccess super;

    /* Private data */
    }tTha16BitGlobalLongRegisterAccess;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtLongRegisterAccessMethods m_AtLongRegisterAccessOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint16 Read(AtLongRegisterAccess self, AtHal hal, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen)
    {
    uint32 holdRegs[] = {0                       , cReadHoldField31_16Adr,
                         cReadHoldField47_32Adr  , cReadHoldField63_48Adr,
                         cReadHoldField79_64Adr  , cReadHoldField95_80Adr,
                         cReadHoldField111_96Adr , cReadHoldField127_112Adr,
                         cReadHoldField143_128Adr, cReadHoldField159_144Adr};
    uint32 numRegs = mCount(holdRegs) / 2;
    uint16 reg_i;

    AtUnused(self);

    AtHalLock(hal);

    holdRegs[0] = localAddress;
    for (reg_i = 0; reg_i < numRegs; reg_i++)
        {
        uint16 offset = (uint16)(reg_i * 2);

        if (reg_i < bufferLen)
            dataBuffer[reg_i] = (AtHalNoLockRead16(hal, holdRegs[offset]) & cBit15_0) | ((AtHalNoLockRead16(hal, holdRegs[offset + 1]) & cBit15_0) << 16);
        else
            {
            AtPrintc(cSevWarning,
                     "WARNING: (%s, %d): register buffer is not enough "
                     "space to hold all values, MSB values will be stripped\r\n",
                     AtSourceLocation);
            break;
            }
        }

    AtHalUnLock(hal);

    return reg_i;
    }

static uint16 Write(AtLongRegisterAccess self, AtHal hal, uint32 localAddress, const uint32 *dataBuffer, uint16 bufferLen)                                               \
    {
    uint32 holdRegs[] = {0                        , cWriteHoldField31_16Adr,
                         cWriteHoldField47_32Adr  , cWriteHoldField63_48Adr,
                         cWriteHoldField79_64Adr  , cWriteHoldField95_80Adr,
                         cWriteHoldField111_96Adr , cWriteHoldField127_112Adr,
                         cWriteHoldField143_128Adr, cWriteHoldField159_144Adr};
    uint32 numRegs = mCount(holdRegs) / 2;
    uint16 reg_i;
	AtUnused(self);

    AtHalLock(hal);
    holdRegs[0] = localAddress;

    /* Write hold registers except the first one */
    for (reg_i = 1; reg_i < numRegs; reg_i++)
        {
        uint8 offset = (uint8)(reg_i * 2);

        if (reg_i < bufferLen)
            {
            AtHalNoLockWrite16(hal, holdRegs[offset]    , (uint16) dataBuffer[reg_i]);
            AtHalNoLockWrite16(hal, holdRegs[offset + 1], (uint16)(dataBuffer[reg_i] >> 16));
            }
        else
            {
            AtPrintc(cSevWarning,
                     "WARNING: (%s, %d): register buffer does not hold enough values "
                     "of long register. MSB values may be undetermined\r\n",
                     AtSourceLocation);
            break;
            }
        }

    /* Then write data to this register to activate hardware writing sequence */
    AtHalNoLockWrite16(hal, holdRegs[1], (uint16)(dataBuffer[0] >> 16));
    AtHalNoLockWrite16(hal, holdRegs[0], (uint16)(dataBuffer[0]));

    AtHalUnLock(hal);

    return reg_i;
    }

static void OverrideAtLongRegisterAccess(AtLongRegisterAccess self)
    {
    AtLongRegisterAccess registerAccess = (AtLongRegisterAccess)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtLongRegisterAccessOverride, mMethodsGet(registerAccess), sizeof(m_AtLongRegisterAccessOverride));

        mMethodOverride(m_AtLongRegisterAccessOverride, Read);
        mMethodOverride(m_AtLongRegisterAccessOverride, Write);
        }

    mMethodsSet(registerAccess, &m_AtLongRegisterAccessOverride);
    }

static void Override(AtLongRegisterAccess self)
    {
    OverrideAtLongRegisterAccess(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha16BitGlobalLongRegisterAccess);
    }

static AtLongRegisterAccess ObjectInit(AtLongRegisterAccess self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtLongRegisterAccessObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtLongRegisterAccess Tha16BitGlobalLongRegisterAccessNew(void)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtLongRegisterAccess newAccess = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newAccess == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newAccess);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Util
 *
 * File        : ThaCrcHash.c
 *
 * Created Date: Sep 21, 2017
 *
 * Description : Hash using CRC functions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <atcrc.h>
#include "../../../../util/AtIteratorInternal.h"
#include "ThaCrcHashInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self)  ((ThaCrcHash)self)
#define mThisIterator(self) ((tConflictedEntriesIterator*)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tConflictedEntriesIterator
    {
    tAtIterator super;

    ThaCrcHash hash;
    int currentTable;
    uint32* key;
    uint32 numKeyBits;
    }tConflictedEntriesIterator;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static char m_iteratorMethodsInit = 0;

/* Override */
static tAtObjectMethods  m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;

static tAtIteratorMethods m_AtIteratorOverride;
static tAtObjectMethods  m_AtIteratorObjectOverride;
static const tAtObjectMethods  *m_AtIteratorObjectMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void AllHashTablesDelete(ThaCrcHash self)
    {
    uint32 idx, numbTables;

    if (self->tables == NULL)
        return;

    numbTables = self->numTables;
    for (idx = 0; idx < numbTables; idx++)
        {
        AtObjectDelete((AtObject) self->tables[idx]);
        self->tables[idx] = NULL;
        }

    AtOsalMemFree(self->tables);
    }

static void ListenerDelete(AtObject listener)
    {
    AtOsalMemFree(listener);
    }

static void ListenerListDelete(ThaCrcHash self)
    {
    AtListDeleteWithObjectHandler(self->listeners, ListenerDelete);
    }

static void Delete(AtObject self)
    {
    AllHashTablesDelete(mThis(self));
    ListenerListDelete(mThis(self));

    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaCrcHash object = mThis(self);
    m_AtObjectMethods->Serialize(self, encoder);

    if (object->tables)
        {
        uint32 table_i;
        for (table_i = 0; table_i < object->numTables; table_i++)
            AtCoderEncodeObject(encoder, (AtObject)ThaCrcHashTableGet(object, table_i), "table");
        }

    mEncodeUInt(numTables);
    mEncodeUInt(numEntriesPerTable);
    mEncodeNone(listeners);
    }

static const char *DescriptionBuild(AtObject self, char *buffer, uint32 bufferSize)
    {
    AtSnprintf(buffer, bufferSize, "crc_hash: %p", (void *)self);
    return buffer;
    }

static void OverrideAtObject(ThaCrcHash self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, DescriptionBuild);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(ThaCrcHash self)
    {
    OverrideAtObject(self);
    }

static ThaCrcHashTable *HashTableAllocate(ThaCrcHash self)
    {
    ThaCrcHashTable* tables = NULL;
    uint32 memSize = sizeof(ThaCrcHashTable) * self->numTables;

    tables = AtOsalMemAlloc(memSize);
    if (tables == NULL)
        return NULL;

    AtOsalMemInit(tables, 0, memSize);
    self->tables = tables;

    return tables;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaCrcHash);
    }

static eAtRet AllHashTablesCreate(ThaCrcHash self)
    {
    uint32 table_i;
    for (table_i = 0; table_i < self->numTables; table_i++)
        {
        self->tables[table_i] = ThaCrcHashTableNew(self, table_i);
        if ((self->tables[table_i] == NULL) || (ThaCrcHashTableSetup(self->tables[table_i]) != cAtOk))
            {
            AllHashTablesDelete(self);
            return cAtErrorRsrcNoAvail;
            }
        }

    return cAtOk;
    }

static eAtRet AllHashTablesSetup(ThaCrcHash self)
    {
    /* Allocate Table resource */
    if (HashTableAllocate(self) == NULL)
        return cAtErrorRsrcNoAvail;

    return AllHashTablesCreate(self);
    }

static uint32 IndexOfTable(ThaCrcHash self, uint32 tableId)
    {
    uint32 idx;
    for (idx = 0; idx < self->numTables; idx++)
        {
        if (ThaCrcHashTableId(self->tables[idx]) == tableId)
            return idx;
        }

    return cInvalidUint32;
    }

/* Get next element, return null if there is no next element */
static AtObject NextGet(AtIterator iterator)
    {
    ThaCrcHash hash = mThisIterator(iterator)->hash;
    ThaCrcHashEntry entry;
    ThaCrcHashTable table;

    if (mThisIterator(iterator)->currentTable == -1)
        {
        mThisIterator(iterator)->currentTable++;
        table = ThaCrcHashTableGet(hash, (uint32)(mThisIterator(iterator)->currentTable));
        return (AtObject)ThaCrcHashTableFirstConflictedEntry(table, mThisIterator(iterator)->key, mThisIterator(iterator)->numKeyBits);
        }

    if (mThisIterator(iterator)->currentTable >= (int)hash->numTables)
        return NULL;

    table = ThaCrcHashTableGet(hash, (uint32)(mThisIterator(iterator)->currentTable));
    entry = ThaCrcHashTableNextConflictedEntry(table, mThisIterator(iterator)->key, mThisIterator(iterator)->numKeyBits);
    if (entry)
        return (AtObject)entry;

    /* Next table */
    mThisIterator(iterator)->currentTable++;
    if (mThisIterator(iterator)->currentTable >= (int)hash->numTables)
        return NULL;

    table = ThaCrcHashTableGet(hash, (uint32)(mThisIterator(iterator)->currentTable));
    return (AtObject)ThaCrcHashTableFirstConflictedEntry(table, mThisIterator(iterator)->key, mThisIterator(iterator)->numKeyBits);
    }

static void OverrideAtIterator(AtIterator iterator)
    {
    if (!m_iteratorMethodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtIteratorOverride, mMethodsGet(iterator), sizeof(m_AtIteratorOverride));
        mMethodOverride(m_AtIteratorOverride, NextGet);
        }

    mMethodsSet(iterator, &m_AtIteratorOverride);
    }

static void KeyValueCache(AtIterator newIterator, uint32* hashKey, uint32 numKeyBits)
    {
    uint32 numDword = numKeyBits / cNumBitsPerDword + ((numKeyBits % cNumBitsPerDword) ? 1 : 0);

    mThisIterator(newIterator)->key = AtOsalMemAlloc(numDword * 4);
    AtOsalMemCpy(mThisIterator(newIterator)->key, hashKey, numDword * 4);
    mThisIterator(newIterator)->numKeyBits = numKeyBits;
    }

static void AtIteratorDelete(AtObject iterator)
    {
    AtOsalMemFree(mThisIterator(iterator)->key);
    m_AtIteratorObjectMethods->Delete(iterator);
    }

static void IteratorOverrideAtObject(AtIterator iterator)
    {
    AtObject object = (AtObject)iterator;

    if (!m_iteratorMethodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtIteratorObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtIteratorObjectOverride, m_AtIteratorObjectMethods, sizeof(m_AtIteratorObjectOverride));

        (m_AtIteratorObjectOverride).Delete = AtIteratorDelete;
        }

    mMethodsSet(object, &m_AtIteratorObjectOverride);
    }

static AtIterator IteratorObjectInit(AtIterator newIterator, ThaCrcHash self, uint32* hashKey, uint32 numKeyBits)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, newIterator, 0, sizeof(tConflictedEntriesIterator));

    /* Super constructor */
    if (AtIteratorObjectInit(newIterator) == NULL)
        return NULL;

    /* Override */
    OverrideAtIterator(newIterator);
    IteratorOverrideAtObject(newIterator);

    m_iteratorMethodsInit = 1;
    mThisIterator(newIterator)->hash = self;
    mThisIterator(newIterator)->currentTable = -1;
    KeyValueCache(newIterator, hashKey, numKeyBits);

    return newIterator;
    }

static AtIterator ConflictedEntriesIteratorCreate(ThaCrcHash self, uint32* hashKey, uint32 numKeyBits)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtIterator newIterator = mMethodsGet(osal)->MemAlloc(osal, sizeof(tConflictedEntriesIterator));

    /* Construct it */
    if (newIterator)
        IteratorObjectInit(newIterator, self, hashKey, numKeyBits);

    return newIterator;
    }

static eAtRet EntryWillMoveNotify(ThaCrcHash self, ThaCrcHashEntry fromEntry, ThaCrcHashEntry toEntry)
    {
    tListenerWraper* wrapper;
    AtIterator iterator;
    eAtRet ret = cAtOk;

    if (self->listeners == NULL)
        return ret;

    iterator = AtListIteratorCreate(mThis(self)->listeners);
    while ((wrapper = (tListenerWraper*)AtIteratorNext(iterator)) != NULL)
        {
        if (wrapper->listener.EntriesWillMove)
            ret |= wrapper->listener.EntriesWillMove(self, fromEntry, toEntry, wrapper->userData);
        }

    AtObjectDelete((AtObject)iterator);
    return ret;
    }

static eAtRet EntrysDidMoveNotify(ThaCrcHash self, ThaCrcHashEntry fromEntry, ThaCrcHashEntry toEntry)
    {
    tListenerWraper* wrapper;
    AtIterator iterator;
    eAtRet ret = cAtOk;

    if (self->listeners == NULL)
        return ret;

    iterator = AtListIteratorCreate(mThis(self)->listeners);
    while ((wrapper = (tListenerWraper*)AtIteratorNext(iterator)) != NULL)
        {
        if (wrapper->listener.EntriesDidMove)
            ret |= wrapper->listener.EntriesDidMove(self, fromEntry, toEntry, wrapper->userData);
        }

    AtObjectDelete((AtObject)iterator);
    return ret;
    }

static ThaCrcHashEntry MoveEntry(ThaCrcHash self, ThaCrcHashEntry fromEntry, ThaCrcHashEntry toEntry)
    {
    eAtRet ret = EntryWillMoveNotify(self, fromEntry, toEntry);
    if (ret != cAtOk)
        return NULL;

    EntrysDidMoveNotify(self, fromEntry, toEntry);

    return fromEntry;
    }

static ThaCrcHashEntry AllTablesFreeEntrySearch(ThaCrcHash self, uint32* hashKey, uint32 numKeyBits)
    {
    ThaCrcHashEntry hashEntry;
    uint32 i;

    for (i = 0; i < self->numTables; i++)
        {
        hashEntry = ThaCrcHashTableFreeEntryGet(self->tables[i], hashKey, numKeyBits);
        if (hashEntry)
            return hashEntry;
        }

    return NULL;
    }

static ThaCrcHashEntry MoveEntryToMakeFreeEntry(ThaCrcHash self, uint32* hashKey, uint32 numKeyBits)
    {
    ThaCrcHashEntry conflictedEntry, freeEntry = NULL;
    AtIterator conflictedEntriesIterator = ConflictedEntriesIteratorCreate(self, hashKey, numKeyBits);

    while ((conflictedEntry = (ThaCrcHashEntry)AtIteratorNext(conflictedEntriesIterator)) != NULL)
        {
        uint32 entryNumKeyBits;
        uint32* entryKey = ThaCrcHashEntryKeyGet(conflictedEntry, &entryNumKeyBits);
        ThaCrcHashEntry tmpEntry;
        uint32 toEntryId;
        uint32 fromEntryId = ThaCrcHashEntryFlatIdGet(conflictedEntry);

        /* Find a free entry for this conflicted entry */
        tmpEntry = AllTablesFreeEntrySearch(self, entryKey, entryNumKeyBits);
        if (tmpEntry == NULL) /* Can not find, try next entry */
            continue;

        /* Try moving, free entry return on success */
        toEntryId = ThaCrcHashEntryFlatIdGet(tmpEntry);
        tmpEntry = MoveEntry(self, conflictedEntry, tmpEntry);
        if (tmpEntry == NULL)
            continue;

        AtDriverLogLock();
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelWarning,
                                AtSourceLocation, "CRC hash moves entry ID %5u (key:0x%06x) to entry ID %5u (key:0x%06x)\r\n",
                                fromEntryId, *hashKey, toEntryId, *entryKey);
        AtDriverLogUnLock();

        freeEntry = conflictedEntry;
        break;
        }

    AtObjectDelete((AtObject)conflictedEntriesIterator);
    return freeEntry;
    }

static AtList ListenerList(ThaCrcHash self)
    {
    if (self->listeners == NULL)
        self->listeners = AtListCreate(0);

    return mThis(self)->listeners;
    }

static tListenerWraper* NewListenerWrapper(void* userData, const tThaCrcHashEntryMovingListener* listener)
    {
    tListenerWraper* newWrapper = AtOsalMemAlloc(sizeof(tListenerWraper));
    if (newWrapper == NULL)
        return NULL;

    newWrapper->userData = userData;
    newWrapper->listener.EntriesWillMove = listener->EntriesWillMove;
    newWrapper->listener.EntriesDidMove = listener->EntriesDidMove;
    return newWrapper;
    }

static tListenerWraper* ListenerWrapperSearch(ThaCrcHash self, void* userData, const tThaCrcHashEntryMovingListener* listener)
    {
    AtIterator iterator;
    tListenerWraper* wrapper = NULL;

    if (mThis(self)->listeners == NULL)
        return NULL;

    iterator = AtListIteratorCreate(self->listeners);
    while ((wrapper = (tListenerWraper*)AtIteratorNext(iterator)) != NULL)
        {
        if ((wrapper->userData == userData) &&
            (wrapper->listener.EntriesWillMove == listener->EntriesWillMove) &&
            (wrapper->listener.EntriesDidMove == listener->EntriesDidMove))
            break;
        }

    AtObjectDelete((AtObject)iterator);
    return wrapper;
    }

static ThaCrcHash ObjectInit(ThaCrcHash self, uint32 numTables, uint32 numEntriesPerTable, ThaHbce hbce)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Call super constructor to reuse all of its implementation */
    AtObjectInit((AtObject)self);

    /* Initialize Methods */
    Override(self);
    m_methodsInit = 1;

    self->numTables = numTables;
    self->numEntriesPerTable = numEntriesPerTable;
    self->hbce = hbce;

    if (AllHashTablesSetup(self) != cAtOk)
        {
        AtObjectDelete((AtObject)self);
        return NULL;
        }

    return self;
    }

ThaCrcHash ThaCrcHashNew(uint32 numTables, uint32 numEntriesInTable, ThaHbce hbce)
    {
    /* Allocate memory */
    ThaCrcHash newHash = AtOsalMemAlloc(ObjectSize());
    if (newHash == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newHash, numTables, numEntriesInTable, hbce);
    }

ThaCrcHashTable ThaCrcHashTableGet(ThaCrcHash self, uint32 tableId)
    {
    uint32 tableIndex;

    if (self == NULL)
        return NULL;

    /* Can not use table ID as array index directly as array is always resorted */
    tableIndex = IndexOfTable(self, tableId);
    if (tableIndex >= self->numTables)
        return NULL;

    return self->tables[tableIndex];
    }

uint32 ThaCrcHashNumEntriesPerTable(ThaCrcHash self)
    {
    if (self)
        return self->numEntriesPerTable;

    return 0;
    }

void ThaCrcHashTableInUsedIncreaseRearrange(ThaCrcHash self, ThaCrcHashTable table)
    {
    uint32 idx;
    uint32 tableIndex = IndexOfTable(self, ThaCrcHashTableId(table));

    if (self == NULL)
        return;

    for (idx = tableIndex + 1; idx < self->numTables; idx++)
        {
        ThaCrcHashTable nextTable = self->tables[idx];
        if (table->numberInUsedEntries > nextTable->numberInUsedEntries)
            {
            self->tables[tableIndex] = nextTable;
            self->tables[idx] = table;
            }

        tableIndex++;
        }
    }

void ThaCrcHashTableInUsedDecreaseRearrange(ThaCrcHash self, ThaCrcHashTable table)
    {
    uint32 idx;
    uint32 tableId = IndexOfTable(self, ThaCrcHashTableId(table));

    if (self == NULL)
        return;

    for (idx = tableId; idx > 0; idx--)
        {
        ThaCrcHashTable nextTable = self->tables[idx - 1];

        if (table->numberInUsedEntries < nextTable->numberInUsedEntries)
            {
            self->tables[tableId] = nextTable;
            self->tables[idx - 1] = table;
            }

        tableId--;
        }
    }

ThaCrcHashEntry ThaCrcHashFreeEntryGet(ThaCrcHash self, uint32* hashKey, uint32 numKeyBits)
    {
    ThaCrcHashEntry hashEntry;
    if (self == NULL)
        return NULL;

    hashEntry = AllTablesFreeEntrySearch(self, hashKey, numKeyBits);
    if (hashEntry)
        return hashEntry;

    /* Can not find free entry, try moving existing entries to create a free one */
    return MoveEntryToMakeFreeEntry(self, hashKey, numKeyBits);
    }

ThaCrcHashEntry ThaCrcHashEntryGetByKey(ThaCrcHash self, uint32* hashKey, uint32 numKeyBits)
    {
    ThaCrcHashEntry hashEntry;
    uint32 i;

    if (self == NULL)
        return NULL;

    for (i = 0; i < self->numTables; i++)
        {
        hashEntry = ThaCrcHashTableEntryGetByKey(ThaCrcHashTableGet(self, i), hashKey, numKeyBits);
        if (hashEntry)
            return hashEntry;
        }

    return NULL;
    }

eAtRet ThaCrcHashUseEntry(ThaCrcHash self, uint32 entryId, uint32* key, uint32 numKeyBits)
    {
    ThaCrcHashTable table = ThaCrcHashTableGet(self, entryId / self->numEntriesPerTable);
    if (ThaCrcHashTableEntryUse(table, entryId % self->numEntriesPerTable) == cAtOk)
        {
        ThaCrcHashEntryKeySet(ThaCrcHashTableEntryAtIndex(table, entryId % self->numEntriesPerTable), key, numKeyBits);
        return cAtOk;
        }

    return cAtErrorNotExist;
    }

eAtRet ThaCrcHashUnuseEntry(ThaCrcHash self, uint32 entryId)
    {
    ThaCrcHashTable table = ThaCrcHashTableGet(self, entryId / self->numEntriesPerTable);
    return ThaCrcHashTableEntryUnuse(table, entryId % self->numEntriesPerTable);
    }

ThaCrcHashEntry ThaCrcHashEntryGetByIndex(ThaCrcHash self, uint32 entryId)
    {
    ThaCrcHashTable table = ThaCrcHashTableGet(self, entryId / self->numEntriesPerTable);
    return ThaCrcHashTableEntryAtIndex(table, entryId % self->numEntriesPerTable);
    }

void ThaCrcHashDebug(ThaCrcHash self)
    {
    uint32 table_i;

    AtPrintc(cSevInfo, "\r\nCRC hash table information:\r\n");
    AtPrintc(cSevInfo, "========================================================\r\n");

    if (self == NULL)
        {
        AtPrintc(cSevWarning, "* Hash handler is NULL.\r\n");
        return;
        }

    AtPrintc(cSevNormal, "* Number of tables: %u\r\n", self->numTables);
    AtPrintc(cSevNormal, "* Number of entry per table: %u\r\n", self->numEntriesPerTable);

    for (table_i = 0; table_i < self->numTables; table_i++)
        {
        AtPrintc(cSevInfo, "\r\n* Table index %u:\r\n", table_i);
        ThaCrcHashTableDebug(self->tables[table_i]);
        }
    }

eAtRet ThaCrcHashEntryMovingListenerAdd(ThaCrcHash self, const tThaCrcHashEntryMovingListener* listener, void* userData)
    {
    /* Identical listener is existing */
    if (ListenerWrapperSearch(self, userData, listener))
        return cAtOk;

    return AtListObjectAdd(ListenerList(self), (AtObject)NewListenerWrapper(userData, listener));
    }

eAtRet ThaCrcHashEntryMovingListenerRemove(ThaCrcHash self, const tThaCrcHashEntryMovingListener* listener, void* userData)
    {
    eAtRet ret;
    tListenerWraper* wrapper = ListenerWrapperSearch(self, userData, listener);
    if (wrapper == NULL)
        return cAtOk;

    ret = AtListObjectRemove(mThis(self)->listeners, (AtObject)wrapper);
    AtOsalMemFree(wrapper);
    return ret;
    }

AtIterator ThaCrcHashConflictedEntriesIteratorCreate(ThaCrcHash self, uint32* hashKey, uint32 numKeyBits)
    {
    if (self)
        return ConflictedEntriesIteratorCreate(self, hashKey, numKeyBits);
    return NULL;
    }

ThaHbce ThaCrcHashHbceGet(ThaCrcHash self)
    {
    if (self)
        return self->hbce;
    return NULL;
    }

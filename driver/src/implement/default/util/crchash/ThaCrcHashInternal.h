/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Util
 * 
 * File        : ThaCrcHashInternal.h
 * 
 * Created Date: Sep 21, 2017
 *
 * Description : FR hash table internal definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THACRCHASHINTERNAL_H_
#define _THACRCHASHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtList.h"
#include "../../../../generic/common/AtObjectInternal.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "ThaCrcHash.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cNumBitsPerDword 32
/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaCrcHash
    {
    tAtObject super;

    /* Private data */
    ThaCrcHashTable* tables;
    uint32 numTables;
    uint32 numEntriesPerTable;
    AtList listeners;
    ThaHbce hbce;
    } tThaCrcHash;

typedef struct tThaCrcHashTable
    {
    tAtObject super;

    /* Private data */
    ThaCrcHashEntry* entries;
    AtList crcPolyList;
    uint32 numberInUsedEntries;
    ThaCrcHash hash;
    uint32 tableId;
    AtIterator iterator;
    } tThaCrcHashTable;

typedef struct tThaCrcHashEntry
    {
    tAtObject super;

    /* Private data */
    ThaCrcHashTable table;
    uint32 id;
    eBool isInUsed;
    uint32* key;
    uint32 numKeyBits;
    uint32 byCrc;
    } tThaCrcHashEntry;

typedef struct tListenerWraper
    {
    void* userData;
    tThaCrcHashEntryMovingListener listener;
    }tListenerWraper;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void ThaCrcHashTableInUsedIncreaseRearrange(ThaCrcHash self, ThaCrcHashTable table);
void ThaCrcHashTableInUsedDecreaseRearrange(ThaCrcHash self, ThaCrcHashTable table);
uint32 ThaCrcHashNumEntriesPerTable(ThaCrcHash self);

eAtRet ThaCrcHashTableSetup(ThaCrcHashTable self);
ThaCrcHashTable ThaCrcHashTableNew(ThaCrcHash hash, uint32 tableId);
eAtRet ThaCrcHashTableEntryUse(ThaCrcHashTable self, uint32 entryIdInTable);
eAtRet ThaCrcHashTableEntryUnuse(ThaCrcHashTable self, uint32 entryIdInTable);
uint32 ThaCrcHashTableId(ThaCrcHashTable self);
ThaCrcHashEntry ThaCrcHashTableFreeEntryGet(ThaCrcHashTable self, uint32* hashKey, uint32 numBitsOfKey);
ThaCrcHashEntry ThaCrcHashTableEntryGetByKey(ThaCrcHashTable self, uint32* hashKey, uint32 numBitsOfKey);
ThaCrcHashEntry ThaCrcHashTableFirstConflictedEntry(ThaCrcHashTable self, uint32* hashKey, uint32 numBitsOfKey);
ThaCrcHashEntry ThaCrcHashTableNextConflictedEntry(ThaCrcHashTable self, uint32* hashKey, uint32 numBitsOfKey);
ThaCrcHashEntry ThaCrcHashTableEntryAtIndex(ThaCrcHashTable self, uint32 entryIdInTable);
void ThaCrcHashTableDebug(ThaCrcHashTable self);

ThaCrcHashEntry ThaCrcHashEntryNew(ThaCrcHashTable table, uint32 index);
eBool ThaCrcHashEntryIsInused(ThaCrcHashEntry self);
void ThaCrcHashEntryInUsedSet(ThaCrcHashEntry self, eBool isInUsed);
eBool ThaCrcHashEntryHasKey(ThaCrcHashEntry self, uint32* key, uint32 numKeyBits);
ThaCrcHashTable ThaCrcHashEntryTableGet(ThaCrcHashEntry self);
void ThaCrcHashEntryDebug(ThaCrcHashEntry self);
eAtRet ThaCrcHashEntryKeySet(ThaCrcHashEntry self, uint32* key, uint32 numKeyBits);
uint32* ThaCrcHashEntryKeyGet(ThaCrcHashEntry self, uint32* numKeyBits);
uint32 ThaCrcHashEntryIdInTable(ThaCrcHashEntry self);

#ifdef __cplusplus
}
#endif
#endif /* _THACRCHASHINTERNAL_H_ */


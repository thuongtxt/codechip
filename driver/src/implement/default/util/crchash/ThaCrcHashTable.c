/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Util
 *
 * File        : ThaCrcHashTable.c
 *
 * Created Date: Sep 21, 2017
 *
 * Description : Crc hash table
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaCrcHash.h"
#include "ThaCrcHashInternal.h"
#include "atcrc.h"
#include "../../cla/hbce/ThaHbceEntity.h"
#include "../../cla/ThaModuleCla.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaCrcHashTable)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tNumber * Number;
typedef struct tNumber
    {
    uint32 number;
    }tNumber;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtObjectMethods  m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxNumEntries(ThaCrcHashTable self)
    {
    return ThaCrcHashNumEntriesPerTable(self->hash);
    }

static eBool HashIdIsValid(ThaCrcHashTable self, uint32 hashId)
    {
    if (hashId >= MaxNumEntries(self))
        return cAtFalse;

    return cAtTrue;
    }

static ThaCrcHashEntry* AllEntriedGet(ThaCrcHashTable self)
    {
    return self->entries;
    }

static ThaCrcHashEntry HashEntryGet(ThaCrcHashTable self, uint32 hashId)
    {
    ThaCrcHashEntry* entries;
    if (!HashIdIsValid(self, hashId))
        return NULL;

    entries = AllEntriedGet(self);
    if (entries == NULL)
        return NULL;

    return entries[hashId];
    }

static void AllEntriesDelete(ThaCrcHashTable self)
    {
    uint32 idx, numEntries;
    if (self->entries == NULL)
        return;

    numEntries = MaxNumEntries(self);
    for (idx = 0; idx < numEntries; idx++)
        {
        AtObjectDelete((AtObject)self->entries[idx]);
        self->entries[idx] = NULL;
        }

    AtOsalMemFree(self->entries);
    }

static ThaCrcHashEntry *AllEntriesAllocate(ThaCrcHashTable self)
    {
    uint32 idx, numEntries = MaxNumEntries(self);
    uint32 memSize = sizeof(ThaCrcHashEntry) * numEntries;

    self->entries = AtOsalMemAlloc(memSize);
    if (self->entries == NULL)
        return NULL;

    AtOsalMemInit(self->entries, 0, memSize);
    for (idx = 0; idx < numEntries; idx++)
        {
        self->entries[idx] = ThaCrcHashEntryNew(self, idx);
        if (self->entries[idx] == NULL)
            {
            AllEntriesDelete(self);
            return NULL;
            }
        }

    return self->entries;
    }

static void CrcPolyListDelete(ThaCrcHashTable self)
    {
    Number crcPoly;
    if (self->crcPolyList == NULL)
        return;

    while ((crcPoly = (Number)AtListObjectRemoveAtIndex(self->crcPolyList, 0)) != NULL)
        AtOsalMemFree(crcPoly);

    AtObjectDelete((AtObject)self->crcPolyList);
    }

static void Delete(AtObject self)
    {
    AllEntriesDelete((ThaCrcHashTable)self);
    CrcPolyListDelete((ThaCrcHashTable)self);
    AtObjectDelete((AtObject)mThis(self)->iterator);
    mThis(self)->iterator = NULL;
    m_AtObjectMethods->Delete(self);
    }

static Number NumberObjectNew(uint32 number)
    {
    Number newNumber = AtOsalMemAlloc(sizeof(tNumber));
    if (newNumber == NULL)
        return NULL;

    newNumber->number = number;
    return newNumber;
    }

static void CrcPolyListPrint(ThaCrcHashTable self)
    {
    Number crcPoly;
    AtIterator iterator = AtListIteratorGet(self->crcPolyList);
    uint32 index = 1;

    AtIteratorRestart(iterator);
    while ((crcPoly = (Number)AtIteratorNext(iterator)) != NULL)
        {
        AtPrintc(cSevNormal, "        - CRC %u: 0x%X\r\n", index, crcPoly->number);
        index++;
        }
    }

static void AllInusedEntriesPrint(ThaCrcHashTable self)
    {
    uint32 entry_i;
    for (entry_i = 0; entry_i < MaxNumEntries(self); entry_i++)
        {
        if (ThaCrcHashEntryIsInused(self->entries[entry_i]))
            ThaCrcHashEntryDebug(self->entries[entry_i]);
        }
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaCrcHashTable object = mThis(self);
    m_AtObjectMethods->Serialize(self, encoder);

    if (object->entries)
        mEncodeObjects(entries, MaxNumEntries(object));

    mEncodeObject(crcPolyList);
    mEncodeUInt(numberInUsedEntries);
    mEncodeUInt(tableId);
    }

static const char *DescriptionBuild(AtObject self, char *buffer, uint32 bufferSize)
    {
    AtSnprintf(buffer, bufferSize, "crc_hash_table: %u (%p)", ThaCrcHashTableId(mThis(self)), (void *)self);
    return buffer;
    }

static eBool ShouldBalanceCrcPolynomialsUsage(ThaCrcHashTable self)
    {
    ThaModuleCla claModule = (ThaModuleCla)ThaHbceEntityClaModuleGet((ThaHbceEntity)ThaCrcHashHbceGet(self->hash));
    return ThaModuleClaShouldBalanceCrcPolynomialsUsage(claModule);
    }

static void OverrideAtObject(ThaCrcHashTable self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, DescriptionBuild);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(ThaCrcHashTable self)
    {
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaCrcHashTable);
    }

static ThaCrcHashTable ThaCrcHashTableObjectInit(ThaCrcHashTable self, ThaCrcHash hash, uint32 tableId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Call super constructor to reuse all of its implementation */
    AtObjectInit((AtObject)self);

    /* Initialize Methods */
    Override(self);

    /* Only initialize method structures one time */
    m_methodsInit = 1;
    self->hash = hash;
    self->tableId = tableId;

    return self;
    }

ThaCrcHashTable ThaCrcHashTableNew(ThaCrcHash hash, uint32 tableId)
    {
    ThaCrcHashTable newHashTable = AtOsalMemAlloc(ObjectSize());
    if (newHashTable == NULL)
        return NULL;

    /* Construct it */
    return ThaCrcHashTableObjectInit(newHashTable, hash, tableId);
    }

eAtRet ThaCrcHashTableSetup(ThaCrcHashTable self)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (self->entries)
        return cAtOk;

    if (AllEntriesAllocate(self) == NULL)
        return cAtErrorRsrcNoAvail;

    return cAtOk;
    }

static AtIterator CrcPolyListIteratorGet(ThaCrcHashTable self)
    {
    if (self->iterator == NULL)
        self->iterator = AtListIteratorCreate(self->crcPolyList);
    return self->iterator;
    }

static ThaCrcHashEntry FreeEntryGetWithCrcUsageBalancing(ThaCrcHashTable self, uint32* hashKey, uint32 numBitsOfKey)
    {
    Number crcPoly;
    AtIterator iterator;
    ThaCrcHashEntry hashEntry = NULL;
    uint32 numTriedCrc = 0;

    if (self == NULL)
        return NULL;

    iterator = CrcPolyListIteratorGet(self);
    while (numTriedCrc < AtListLengthGet(self->crcPolyList))
        {
        uint32 hashId;
        ThaCrcHashEntry entry;

        crcPoly = (Number)AtIteratorNext(iterator);
        if (crcPoly == NULL) /* Iterator reached last item, restart from first item */
            {
            AtIteratorRestart(iterator);
            crcPoly = (Number)AtIteratorNext(iterator);
            }

        hashId = AtCrc(hashKey, numBitsOfKey, crcPoly->number);
        entry = HashEntryGet(self, hashId);
        if ((entry) && !ThaCrcHashEntryIsInused(entry))
            {
            hashEntry = entry;
            hashEntry->byCrc = crcPoly->number;
            break;
            }

        numTriedCrc++;
        }

    return hashEntry;
    }

ThaCrcHashEntry ThaCrcHashTableFreeEntryGet(ThaCrcHashTable self, uint32* hashKey, uint32 numBitsOfKey)
    {
    Number crcPoly;
    AtIterator iterator;
    ThaCrcHashEntry hashEntry = NULL;

    if (self == NULL)
        return NULL;

    if (ShouldBalanceCrcPolynomialsUsage(self))
        return FreeEntryGetWithCrcUsageBalancing(self, hashKey, numBitsOfKey);

    iterator = AtListIteratorCreate(self->crcPolyList);
    while ((crcPoly = (Number)AtIteratorNext(iterator)) != NULL)
        {
        uint32 hashId = AtCrc(hashKey, numBitsOfKey, crcPoly->number);
        ThaCrcHashEntry entry = HashEntryGet(self, hashId);

        if ((entry) && !ThaCrcHashEntryIsInused(entry))
            {
            hashEntry = entry;
            hashEntry->byCrc = crcPoly->number;
            break;
            }
        }

    AtObjectDelete((AtObject)iterator);
    return hashEntry;
    }

ThaCrcHashEntry ThaCrcHashTableNextConflictedEntry(ThaCrcHashTable self, uint32* hashKey, uint32 numBitsOfKey)
    {
    Number crcPoly;
    AtIterator iterator;

    if (self == NULL)
        return NULL;

    iterator = AtListIteratorGet(self->crcPolyList);
    while ((crcPoly = (Number)AtIteratorNext(iterator)) != NULL)
        {
        uint32 hashId = AtCrc(hashKey, numBitsOfKey, crcPoly->number);
        return HashEntryGet(self, hashId);
        }

    return NULL;
    }

ThaCrcHashEntry ThaCrcHashTableFirstConflictedEntry(ThaCrcHashTable self, uint32* hashKey, uint32 numBitsOfKey)
    {
    Number crcPoly;
    AtIterator iterator;

    if (self == NULL)
        return NULL;

    iterator = AtListIteratorGet(self->crcPolyList);
    AtIteratorRestart(iterator);

    while ((crcPoly = (Number)AtIteratorNext(iterator)) != NULL)
        {
        uint32 hashId = AtCrc(hashKey, numBitsOfKey, crcPoly->number);
        return HashEntryGet(self, hashId);
        }

    return NULL;
    }

ThaCrcHashEntry ThaCrcHashTableEntryGetByKey(ThaCrcHashTable self, uint32* hashKey, uint32 numBitsOfKey)
    {
    Number crcPoly;
    AtIterator iterator;

    if (self == NULL)
        return NULL;

    iterator = AtListIteratorGet(self->crcPolyList);
    AtIteratorRestart(iterator);

    while ((crcPoly = (Number)AtIteratorNext(iterator)) != NULL)
        {
        uint32 hashId = AtCrc(hashKey, numBitsOfKey, crcPoly->number);
        ThaCrcHashEntry hashEntry = HashEntryGet(self, hashId);

        if (!ThaCrcHashEntryIsInused(hashEntry))
            continue;

        if (ThaCrcHashEntryHasKey(hashEntry, hashKey, numBitsOfKey))
            return hashEntry;
        }

    return NULL;
    }

eAtRet ThaCrcHashTableEntryUse(ThaCrcHashTable self, uint32 entryIdInTable)
    {
    if ((self) && HashIdIsValid(self, entryIdInTable))
        {
        if (ThaCrcHashEntryIsInused(self->entries[entryIdInTable]))
            AtDriverLog(AtDriverSharedDriverGet(), cAtLogLevelCritical,
                        "%s, %u. Entry %u is in-used on table %s\r\n",
                        AtSourceLocation, entryIdInTable, AtObjectToString((AtObject)self));

        self->numberInUsedEntries++;
        ThaCrcHashEntryInUsedSet(self->entries[entryIdInTable], cAtTrue);
        ThaCrcHashTableInUsedIncreaseRearrange(self->hash, self);

        return cAtOk;
        }

    return cAtErrorNotExist;
    }

eAtRet ThaCrcHashTableEntryUnuse(ThaCrcHashTable self, uint32 entryIdInTable)
    {
    if ((self) && HashIdIsValid(self, entryIdInTable))
        {
        if (!ThaCrcHashEntryIsInused(self->entries[entryIdInTable]))
            AtDriverLog(AtDriverSharedDriverGet(), cAtLogLevelCritical,
                        "%s, %u. Entry %u is un-used on table %s\r\n",
                        AtSourceLocation, entryIdInTable, AtObjectToString((AtObject)self));

        self->numberInUsedEntries--;
        ThaCrcHashEntryInUsedSet(self->entries[entryIdInTable], cAtFalse);
        ThaCrcHashEntryKeySet(self->entries[entryIdInTable], NULL, cInvalidUint32);
        ThaCrcHashTableInUsedDecreaseRearrange(self->hash, self);
        return cAtOk;
        }

    return cAtErrorNotExist;
    }

ThaCrcHashEntry ThaCrcHashTableEntryAtIndex(ThaCrcHashTable self, uint32 entryIdInTable)
    {
    if ((self) && HashIdIsValid(self, entryIdInTable))
        return self->entries[entryIdInTable];

    return NULL;
    }

uint32 ThaCrcHashTableId(ThaCrcHashTable self)
    {
    return (self) ? self->tableId : cInvalidUint32;
    }

eAtRet ThaCrcHashTableCrcPolyAdd(ThaCrcHashTable self, uint32 crcPoly)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (self->crcPolyList == NULL)
        self->crcPolyList = AtListCreate(0);

    if (self->crcPolyList == NULL)
        return cAtErrorRsrcNoAvail;

    return AtListObjectAdd(self->crcPolyList, (AtObject)NumberObjectNew(crcPoly));
    }

void ThaCrcHashTableDebug(ThaCrcHashTable self)
    {
    if (self == NULL)
        AtPrintc(cSevWarning, " * Table is NULL.\r\n");

    AtPrintc(cSevNormal, "    * Table ID: %u\r\n", self->tableId);
    AtPrintc(cSevNormal, "    * Crc functions:\r\n");
    CrcPolyListPrint(self);

    if (self->numberInUsedEntries)
        AtPrintc(cSevNormal, "    * %u in-used entries:\r\n", self->numberInUsedEntries);
    else
        AtPrintc(cSevNormal, "    * None in-used entry.\r\n");

    /* Look stupid!, but this is to double check information in database */
    AllInusedEntriesPrint(self);
    }

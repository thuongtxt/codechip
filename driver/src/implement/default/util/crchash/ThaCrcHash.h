/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Util
 * 
 * File        : ThaCrcHash.h
 * 
 * Created Date: Sep 21, 2017
 *
 * Description : CRC hash interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THACRCHASH_H_
#define _THACRCHASH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"
#include "AtRet.h"
#include "AtIterator.h"
#include "../../cla/hbce/ThaHbceClasses.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaCrcHash * ThaCrcHash;
typedef struct tThaCrcHashTable * ThaCrcHashTable;
typedef struct tThaCrcHashEntry * ThaCrcHashEntry;

typedef struct tThaCrcHashEntryMovingListener
    {
    eAtRet (*EntriesDidMove)(ThaCrcHash self, ThaCrcHashEntry fromEntry, ThaCrcHashEntry toEntry, void* userData);
    eAtRet (*EntriesWillMove)(ThaCrcHash self, ThaCrcHashEntry fromEntry, ThaCrcHashEntry toEntry, void* userData);
    }tThaCrcHashEntryMovingListener;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* ThaCrcHash */
ThaCrcHash ThaCrcHashNew(uint32 numTables, uint32 numEntriesInTable, ThaHbce hbce);
ThaCrcHashEntry ThaCrcHashFreeEntryGet(ThaCrcHash self, uint32* hashKey, uint32 numKeyBits);
ThaCrcHashEntry ThaCrcHashEntryGetByKey(ThaCrcHash self, uint32* hashKey, uint32 numKeyBits);
ThaCrcHashEntry ThaCrcHashEntryGetByIndex(ThaCrcHash self, uint32 entryId);
eAtRet ThaCrcHashUseEntry(ThaCrcHash self, uint32 entryId, uint32* key, uint32 numKeyBits);
eAtRet ThaCrcHashUnuseEntry(ThaCrcHash self, uint32 entryId);
ThaCrcHashTable ThaCrcHashTableGet(ThaCrcHash self, uint32 tableIndex);
void ThaCrcHashDebug(ThaCrcHash self);
ThaHbce ThaCrcHashHbceGet(ThaCrcHash self);

/* ThaCrcHashEntry */
uint32 ThaCrcHashEntryFlatIdGet(ThaCrcHashEntry self);

/* Hash Table */
eAtRet ThaCrcHashTableCrcPolyAdd(ThaCrcHashTable self, uint32 crcPoly);

/* Entries moving callback */
eAtRet ThaCrcHashEntryMovingListenerAdd(ThaCrcHash self, const tThaCrcHashEntryMovingListener* listener, void* userData);
eAtRet ThaCrcHashEntryMovingListenerRemove(ThaCrcHash self, const tThaCrcHashEntryMovingListener* listener, void* userData);

/* To iterate on all entries from all CRC functions */
AtIterator ThaCrcHashConflictedEntriesIteratorCreate(ThaCrcHash self, uint32* hashKey, uint32 numKeyBits);

#ifdef __cplusplus
}
#endif
#endif /* _THACRCHASH_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Frame-relay
 *
 * File        : ThaCrcHashEntry.c
 *
 * Created Date: Jul 19, 2016
 *
 * Description : FR hash entry
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../eth/ThaEthFlowInternal.h"
#include "ThaCrcHashInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaCrcHashEntry)self)

/*--------------------------- Local typedefs ---------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtObjectMethods  m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *ToString(AtObject self)
    {
    static char string[32];
    AtSprintf(string, "ThaCrcHashEntry.%u", ThaCrcHashEntryFlatIdGet((ThaCrcHashEntry) self) + 1);

    return string;
    }

static void Delete(AtObject self)
    {
    AtOsalMemFree(mThis(self)->key);
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaCrcHashEntry object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    if (object->key)
        {
        uint32 numDword = object->numKeyBits / cNumBitsPerDword + ((object->numKeyBits % cNumBitsPerDword) ? 1 : 0);
        mEncodeUInt32Array(key, numDword);
        }

    mEncodeUInt(id);
    mEncodeUInt(numKeyBits);
    mEncodeUInt(isInUsed);
    }

static void OverrideAtObject(ThaCrcHashEntry self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(ThaCrcHashEntry self)
    {
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaCrcHashEntry);
    }

static ThaCrcHashEntry ObjectInit(ThaCrcHashEntry self, ThaCrcHashTable table, uint32 entryIndex)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Call super constructor to reuse all of its implementation */
    AtObjectInit((AtObject)self);

    /* Initialize Methods */
    Override(self);

    /* Only initialize method structures one time */
    m_methodsInit = 1;
    self->table = table;
    self->id = entryIndex;

    return self;
    }

ThaCrcHashEntry ThaCrcHashEntryNew(ThaCrcHashTable table, uint32 entryIndex)
    {
    /* Allocate memory */
    ThaCrcHashEntry newEntry = AtOsalMemAlloc(ObjectSize());
    if (newEntry == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newEntry, table, entryIndex);
    }

uint32 ThaCrcHashEntryFlatIdGet(ThaCrcHashEntry self)
    {
    ThaCrcHashTable table;
    if (self == NULL)
        return cInvalidUint32;

    table = self->table;
    return (self->id + ThaCrcHashNumEntriesPerTable(table->hash) * ThaCrcHashTableId(table));
    }

ThaCrcHashTable ThaCrcHashEntryTableGet(ThaCrcHashEntry self)
    {
    if (self)
        return self->table;

    return NULL;
    }

void ThaCrcHashEntryInUsedSet(ThaCrcHashEntry self, eBool isInUsed)
    {
    if (self)
        self->isInUsed = isInUsed;
    }

eBool ThaCrcHashEntryIsInused(ThaCrcHashEntry self)
    {
    if (self)
        return self->isInUsed;
    return cAtFalse;
    }

eAtRet ThaCrcHashEntryKeySet(ThaCrcHashEntry self, uint32* key, uint32 numKeyBits)
    {
    uint32 numDword;
    if (self == NULL)
        return cAtErrorNullPointer;

    /* Case removing key out of entry */
    if (key == NULL)
        {
        AtOsalMemFree(self->key);
        self->key = NULL;
        self->numKeyBits = 0;
        return cAtOk;
        }

    numDword = numKeyBits / cNumBitsPerDword + ((numKeyBits % cNumBitsPerDword) ? 1 : 0);
    if ((self->key) && (numKeyBits <= self->numKeyBits))
        {
        AtOsalMemCpy(self->key, key, numDword * 4);
        self->numKeyBits = numKeyBits;
        return cAtOk;
        }

    if (self->key)
       AtOsalMemFree(self->key);

    self->key = AtOsalMemAlloc(numDword * 4);
    AtOsalMemCpy(self->key, key, numDword * 4);
    self->numKeyBits = numKeyBits;

    return cAtOk;
    }

eBool ThaCrcHashEntryHasKey(ThaCrcHashEntry self, uint32* key, uint32 numKeyBits)
    {
    uint32 numDword = numKeyBits / cNumBitsPerDword + ((numKeyBits % cNumBitsPerDword) ? 1 : 0);
    if ((self == NULL) || (self->key == NULL))
        return cAtFalse;

    return (AtOsalMemCmp(self->key, key, numDword * 4) == 0) ? cAtTrue : cAtFalse;
    }

void ThaCrcHashEntryDebug(ThaCrcHashEntry self)
    {
    uint32 numDword, dword_i;
    if (self == NULL)
        return;

    numDword = self->numKeyBits / cNumBitsPerDword + ((self->numKeyBits % cNumBitsPerDword) ? 1 : 0);
    AtPrintc(cSevNormal, "        - Entry %u: Flat ID: %u, %s, Key value: ",
             self->id, ThaCrcHashEntryFlatIdGet(self), (self->isInUsed) ? "In-used" : "Un-used");
    for (dword_i = 0; dword_i < numDword; dword_i++)
        {
        AtPrintc(cSevNormal, "0x%08X", self->key[dword_i]);
        if (dword_i < numDword - 1)
            AtPrintc(cSevNormal, ", ");
        }

    AtPrintc(cSevNormal, ", By CRC: 0x%X\r\n", self->byCrc);
    }

uint32* ThaCrcHashEntryKeyGet(ThaCrcHashEntry self, uint32* numKeyBits)
    {
    if (self == NULL)
        return 0;

    if (numKeyBits)
        *numKeyBits = self->numKeyBits;

    return self->key;
    }

uint32 ThaCrcHashEntryIdInTable(ThaCrcHashEntry self)
    {
    if (self)
        return self->id;

    return cInvalidUint32;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : UTIL
 *
 * File        : Tha32BitGlobalLongRegisterAccess.c
 *
 * Created Date: May 10, 2013
 *
 * Description : Long register access of product 2400
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaUtil.h"
#include "Tha32BitGlobalLongRegisterAccessInternal.h"

/*--------------------------- Define -----------------------------------------*/
/* Hold registers for long read operations */
#define cReadHoldField63_32Adr   0x000021
#define cReadHoldField95_64Adr   0x000022
#define cReadHoldField127_96Adr  0x000023

/* Hold registers for long write operations */
#define cWriteHoldField63_32Adr   0x000011
#define cWriteHoldField95_64Adr   0x000012
#define cWriteHoldField127_96Adr  0x000013

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha32BitGlobalLongRegisterAccess)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha32BitGlobalLongRegisterAccessMethods m_methods;

/* Override */
static tAtLongRegisterAccessMethods m_AtLongRegisterAccessOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint16 Read(AtLongRegisterAccess self, AtHal hal, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen)
    {
    uint32 numRegs;
    uint32 *holdRegs = mMethodsGet(mThis(self))->ReadHoldRegs(mThis(self), &numRegs);
    return Tha32BitGlobalLongRegisterReadByHandleAndHoldRegisters(self,
                                        hal,
                                        localAddress,
                                        holdRegs,
                                        numRegs,
                                        dataBuffer,
                                        bufferLen,
                                        AtHalNoLockRead,
                                        AtHalNoLockRead);
    }

static uint16 Write(AtLongRegisterAccess self, AtHal hal, uint32 localAddress, const uint32 *dataBuffer, uint16 bufferLen)                                               \
    {
    uint32 offset = localAddress & cBit24;
    uint32 numRegs;
    uint32 *holdRegs = mMethodsGet(mThis(self))->WriteHoldRegs(mThis(self), &numRegs);
    uint16 reg_i;
	AtUnused(self);

    AtHalLock(hal);
    holdRegs[0] = localAddress & cBit23_0;

    /* Write hold registers except the first one */
    for (reg_i = 1; reg_i < numRegs; reg_i++)
        {
        if (reg_i < bufferLen)
            AtHalNoLockWrite(hal, holdRegs[reg_i] + offset, dataBuffer[reg_i]);
        else
            {
            AtPrintc(cSevWarning,
                     "WARNING: (%s, %d): register buffer does not hold enough values "
                     "of long register. MSB values may be undetermined\r\n",
                     AtSourceLocation);
            break;
            }
        }

    /* Then write data to this register to activate hardware writing sequence */
    AtHalNoLockWrite(hal, holdRegs[0] + offset, dataBuffer[0]);

    AtHalUnLock(hal);

    return reg_i;
    }

static uint32 *ReadHoldRegs(Tha32BitGlobalLongRegisterAccess self, uint32 *numRegs)
    {
    static uint32 holdRegs[] = {0,
                                cReadHoldField63_32Adr,
                                cReadHoldField95_64Adr,
                                cReadHoldField127_96Adr};
    AtUnused(self);
    if (numRegs)
        *numRegs = 4;
    return holdRegs;
    }

static uint32 *WriteHoldRegs(Tha32BitGlobalLongRegisterAccess self, uint32 *numRegs)
    {
    static uint32 holdRegs[] = {0,
                                cWriteHoldField63_32Adr,
                                cWriteHoldField95_64Adr,
                                cWriteHoldField127_96Adr};
    AtUnused(self);
    if (numRegs)
        *numRegs = 4;
    return holdRegs;
    }

static void MethodsInit(AtLongRegisterAccess self)
    {
    Tha32BitGlobalLongRegisterAccess regAccess = (Tha32BitGlobalLongRegisterAccess)self;
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, ReadHoldRegs);
        mMethodOverride(m_methods, WriteHoldRegs);
        }

    mMethodsSet(regAccess, &m_methods);
    }

static void OverrideAtLongRegisterAccess(AtLongRegisterAccess self)
    {
    AtLongRegisterAccess registerAccess = (AtLongRegisterAccess)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtLongRegisterAccessOverride, mMethodsGet(registerAccess), sizeof(m_AtLongRegisterAccessOverride));

        mMethodOverride(m_AtLongRegisterAccessOverride, Read);
        mMethodOverride(m_AtLongRegisterAccessOverride, Write);
        }

    mMethodsSet(registerAccess, &m_AtLongRegisterAccessOverride);
    }

static void Override(AtLongRegisterAccess self)
    {
    OverrideAtLongRegisterAccess(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha32BitGlobalLongRegisterAccess);
    }

AtLongRegisterAccess Tha32BitGlobalLongRegisterAccessObjectInit(AtLongRegisterAccess self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtLongRegisterAccessObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtLongRegisterAccess Tha32BitGlobalLongRegisterAccessNew(void)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtLongRegisterAccess newAccess = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newAccess == NULL)
        return NULL;

    /* Construct it */
    return Tha32BitGlobalLongRegisterAccessObjectInit(newAccess);
    }

uint16 Tha32BitGlobalLongRegisterReadByHandleAndHoldRegisters(AtLongRegisterAccess self,
                                           AtHal hal,
                                           uint32 localAddress,
                                           uint32 *holdRegisters,
                                           uint32 numHoldRegisters,
                                           uint32 *dataBuffer,
                                           uint16 bufferLen,
                                           uint32 (*NoLockReadFunc)(AtHal self, uint32 address),
                                           uint32 (*HoldRegReadFunc)(AtHal self, uint32 address))
    {
    uint32 offset = localAddress & cBit24;
    uint16 reg_i;
    uint32 holdAddress;
    AtUnused(self);

    AtHalLock(hal);

    dataBuffer[0] = NoLockReadFunc(hal, localAddress & cBit23_0);

    for (reg_i = 1; reg_i < numHoldRegisters; reg_i++)
        {
        if (reg_i < bufferLen)
            {
            holdAddress = holdRegisters[reg_i] + offset;
            dataBuffer[reg_i] = HoldRegReadFunc(hal, holdAddress);
            }
        else
            {
            AtPrintc(cSevWarning,
                     "WARNING: (%s, %d): register buffer is not enough "
                     "space to hold all values, MSB values will be stripped\r\n",
                     AtSourceLocation);
            break;
            }
        }

    AtHalUnLock(hal);

    return reg_i;
    }

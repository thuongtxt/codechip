/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Util
 * 
 * File        : ThaBitMask.h
 * 
 * Created Date: Jul 30, 2013
 *
 * Description : Bit mask
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THABITMASK_H_
#define _THABITMASK_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"
#include "AtObject.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaBitMask * ThaBitMask;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaBitMask ThaBitMaskNew(uint32 numBits);

void ThaBitMaskInit(ThaBitMask self);
uint32 ThaBitMaskFirstZeroBit(ThaBitMask self);
uint32 ThaBitMaskLastZeroBit(ThaBitMask self);
uint32 ThaBitMaskFirstZeroBitFromPosition(ThaBitMask self, uint32 bitPosition);
eBool ThaBitMaskBitPositionIsValid(ThaBitMask self, uint32 bitPosition);
uint8 ThaBitMaskBitVal(ThaBitMask self, uint32 bitPosition);
void ThaBitMaskClearBit(ThaBitMask self, uint32 bitPosition);
void ThaBitMaskSetBit(ThaBitMask self, uint32 bitPosition);
void ThaBitMaskDebug(ThaBitMask self);
eBool ThaBitMaskIsAllZeroBits(ThaBitMask self);

#ifdef __cplusplus
}
#endif
#endif /* _THABITMASK_H_ */


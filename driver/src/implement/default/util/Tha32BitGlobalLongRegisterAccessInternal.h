/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Util
 * 
 * File        : Tha32BitGlobalLongRegisterAccessInternal.h
 * 
 * Created Date: Jun 24, 2015
 *
 * Description : Long register access
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA32BITGLOBALLONGREGISTERACCESSINTERNAL_H_
#define _THA32BITGLOBALLONGREGISTERACCESSINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/util/AtLongRegisterAccessInternal.h"
#include "ThaUtil.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha32BitGlobalLongRegisterAccess *Tha32BitGlobalLongRegisterAccess;

typedef struct tTha32BitGlobalLongRegisterAccessMethods
    {
    uint32 *(*ReadHoldRegs)(Tha32BitGlobalLongRegisterAccess self, uint32 *numRegs);
    uint32 *(*WriteHoldRegs)(Tha32BitGlobalLongRegisterAccess self, uint32 *numRegs);
    }tTha32BitGlobalLongRegisterAccessMethods;

typedef struct tTha32BitGlobalLongRegisterAccess
    {
    tAtLongRegisterAccess super;
    const tTha32BitGlobalLongRegisterAccessMethods *methods;
    }tTha32BitGlobalLongRegisterAccess;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtLongRegisterAccess Tha32BitGlobalLongRegisterAccessObjectInit(AtLongRegisterAccess self);
uint16 Tha32BitGlobalLongRegisterReadByHandleAndHoldRegisters(AtLongRegisterAccess self,
                                           AtHal hal,
                                           uint32 localAddress,
                                           uint32 *holdRegisters,
                                           uint32 numHoldRegisters,
                                           uint32 *dataBuffer,
                                           uint16 bufferLen,
                                           uint32 (*NoLockReadFunc)(AtHal self, uint32 address),
                                           uint32 (*HoldRegReadFunc)(AtHal self, uint32 address));

#endif /* _THA32BITGLOBALLONGREGISTERACCESSINTERNAL_H_ */


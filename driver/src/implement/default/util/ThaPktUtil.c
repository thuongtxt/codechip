/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Util
 *
 * File        : ThaPktUtil.c
 *
 * Created Date: Dec 12, 2014
 *
 * Description : Packet util
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtEthPort.h"
#include "AtPwPsn.h"
#include "ThaUtil.h"
#include "../../../generic/man/AtDriverInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
uint8 ThaPktUtilPutMacToBuffer(uint8 *buffer, uint8 currentIndex, uint8 *mac)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemCpy(osal, (void*)(buffer + currentIndex), mac, cAtMacAddressLen);
    currentIndex = (uint8)(currentIndex + cAtMacAddressLen);

    return currentIndex;
    }

uint8 ThaPktUtilPutEthTypeToBuffer(uint8 *buffer, uint8 currentIndex, uint16 ethType)
    {
    buffer[currentIndex] = (uint8)((ethType >> 8) & cBit7_0);
    currentIndex = (uint8)(currentIndex + 1);
    buffer[currentIndex] = ethType & cBit7_0;
    currentIndex = (uint8)(currentIndex + 1);

    return currentIndex;
    }

uint8 ThaPktUtilPutVlanTagToBuffer(uint8 *buffer, uint8 currentIndex, const tAtEthVlanTag *vlanTag)
    {
    buffer[currentIndex] = (uint8)(((vlanTag->priority & cBit2_0) << 5) |
                                   ((vlanTag->cfi & cBit0) << 4) |
                                   ((vlanTag->vlanId >> 8) & cBit3_0));
    currentIndex = (uint8)(currentIndex + 1);

    buffer[currentIndex] = vlanTag->vlanId & cBit7_0;
    currentIndex = (uint8)(currentIndex + 1);

    return currentIndex;
    }

uint8 ThaPktUtilPutVlanTypeToBuffer(uint8 *buffer, uint8 currentIndex, uint16 vlanType)
    {
    buffer[currentIndex] = (uint8)(vlanType >> 8);
    currentIndex = (uint8)(currentIndex + 1);
    buffer[currentIndex] = vlanType & cBit7_0;
    currentIndex = (uint8)(currentIndex + 1);

    return currentIndex;
    }

uint8 ThaPktUtilPutVlanToBuffer(uint8 *buffer, uint8 currentIndex, uint16 vlanType, const tAtEthVlanTag *vlanTag)
    {
    currentIndex = ThaPktUtilPutVlanTypeToBuffer(buffer, currentIndex, vlanType);
    currentIndex = ThaPktUtilPutVlanTagToBuffer(buffer, currentIndex, vlanTag);

    return currentIndex;
    }

uint32 ThaPktUtilPutDataToRegister(uint8 *buffer, uint32 currentIndex)
    {
    uint32 regVal;

    regVal  =   (uint32)buffer[currentIndex*4 + 3];
    regVal |= (((uint32)buffer[currentIndex*4 + 2]) << 8)  & cBit15_8;
    regVal |= (((uint32)buffer[currentIndex*4 + 1]) << 16) & cBit23_16;
    regVal |= (((uint32)buffer[currentIndex*4 + 0]) << 24) & cBit31_24;

    return regVal;
    }

uint16 ThaPktUtilEthTypeFromPsn(AtPwPsn psn)
    {
    eAtPwPsnType psnType;
    AtPwPsn lowerPsn = psn;

    /* Get lowest PSN */
    while (AtPwPsnLowerPsnGet(lowerPsn) != NULL)
        lowerPsn = AtPwPsnLowerPsnGet(lowerPsn);

    psnType = AtPwPsnTypeGet(lowerPsn);
    if (psnType == cAtPwPsnTypeMpls) return cThaEthTypeMplsUcast;
    if (psnType == cAtPwPsnTypeMef)  return cThaEthTypeMef;
    if (psnType == cAtPwPsnTypeIPv4) return cThaEthTypeIpv4;
    if (psnType == cAtPwPsnTypeIPv6) return cThaEthTypeIpv6;

    return 0;
    }

tAtEthVlanTag *ThaPktUtilVlanTagFromBuffer(uint8 *buffer, tAtEthVlanTag *vlanTag)
    {
    uint16 vlanId;

    vlanTag->priority = *buffer >> 5;
    vlanTag->cfi      = (*buffer >> 4) & cBit0;

    vlanId = *(buffer);
    vlanId = (uint16)(vlanId << 8);
    vlanId = (uint16)(vlanId | *(buffer + 1));

    vlanTag->vlanId = vlanId & cBit11_0;

    return vlanTag;
    }

uint16 ThaPktUtilVlanTypeFromBuffer(uint8 *buffer)
    {
    uint16 vlanType;

    vlanType = *(buffer);
    vlanType = (uint16)(vlanType << 8);
    vlanType = (uint16)(vlanType | *(buffer + 1));

    return vlanType;
    }

uint32 ThaPktUtilVlanToUint32(const tAtVlan *vlan)
    {
    uint32 value;
    if (vlan == NULL)
        return 0;

    mFieldIns(&value, cBit31_16, 16, vlan->tpid);
    mFieldIns(&value, cBit15_13, 13, vlan->priority);
    mFieldIns(&value, cBit12,    12, vlan->cfi);
    mFieldIns(&value, cBit11_0,  0,  vlan->vlanId);
    return value;
    }

void ThaPktUtilRegUint32ToVlan(uint32 value, tAtVlan *vlan)
    {
    if (vlan)
        {
        mFieldGet(value, cBit31_16, 16, uint16, &(vlan->tpid));
        mFieldGet(value, cBit15_13, 13, uint8,  &(vlan->priority));
        mFieldGet(value, cBit12,    12, uint8,  &(vlan->cfi));
        mFieldGet(value, cBit11_0,  0,  uint16, &(vlan->vlanId));
        }
    }

uint16 ThaPktUtilVlanTagToUint16(const tAtEthVlanTag *vlanTag)
    {
    uint16 buffer = (uint16)(((vlanTag->priority & cBit2_0) << 13) |
                             ((vlanTag->cfi & cBit0) << 12) |
                              (vlanTag->vlanId & cBit11_0));
    return buffer;
    }

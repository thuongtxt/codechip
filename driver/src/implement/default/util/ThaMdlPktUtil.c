/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Util
 *
 * File        : ThaPktUtil.c
 *
 * Created Date: Dec 12, 2014
 *
 * Description : Packet util
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaUtil.h"
#include "../../../generic/man/AtDriverInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
uint8 ThaMdlPktUtilPutZeroToBuffer(uint8 *buffer, uint8 currentIndex, uint8 length)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal,
                               (void*) (buffer + currentIndex),
                               0,
                               length);
    currentIndex = (uint8) (currentIndex + length);
    return currentIndex;
    }

uint8 ThaMdlPktUtilPutDataArrayToBuffer(uint8 *buffer, uint8 currentIndex, const uint8 *dataArray, uint8 length)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemCpy(osal,
                              (void*) (buffer + currentIndex),
                              dataArray,
                              length);
    currentIndex = (uint8) (currentIndex + length);
    return currentIndex;
    }

uint8 ThaMdlPktUtilPutLapdHeaderToBuffer(uint8 *buffer, uint8 currentIndex, const tAtLapdHeader *lapdHeader)
    {
    buffer[currentIndex] = (uint8)(((lapdHeader->sapi & cBit5_0) << 2) |
                                   ((lapdHeader->cr & cBit0) << 1) |
                                   ((lapdHeader->iea & cBit0)));
    currentIndex = (uint8) (currentIndex + 1);
    buffer[currentIndex] = (uint8)(((lapdHeader->tei & cBit6_0) << 1) | (lapdHeader->fea & cBit0));
    currentIndex = (uint8) (currentIndex + 1);
    buffer[currentIndex] = lapdHeader->control;
    currentIndex = (uint8) (currentIndex + 1);
    return currentIndex;
    }

uint8 ThaMdlPktUtilPutLapdMessageTypeToBuffer(uint8 *buffer, uint8 currentIndex, uint8 messageType)
    {
    buffer[currentIndex] = messageType;
    currentIndex = (uint8) (currentIndex + 1);
    return currentIndex;
    }

uint8 ThaMdlPktUtilLapdMessageTypeFromBuffer(uint8 *buffer)
    {
    return buffer[0];
    }

tAtLapdHeader * ThaMdlPktUtilLapdHeaderFromBuffer(uint8 *buffer, tAtLapdHeader *lapdHeader)
    {
    lapdHeader->sapi     = (*buffer >> 2) & cBit5_0;
    lapdHeader->cr       = (*buffer >> 1) & cBit0;
    lapdHeader->iea      = (*buffer) & cBit0;
    lapdHeader->tei      = ((*(buffer + 1)) >> 1) & cBit6_0;
    lapdHeader->fea      = (*(buffer + 1)) & cBit0;
    lapdHeader->control  = *(buffer + 2);
    return lapdHeader;
    }

uint8 * ThaMdlPktUtilPutDataArrayFromBuffer(uint8 *buffer, uint8 *dataArray, uint8 length)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemCpy(osal, (void*) dataArray, (void*) buffer, length);
    return dataArray;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : None
 * 
 * File        : ThaUtil.h
 * 
 * Created Date: Oct 9, 2012
 *
 * Description : Utility functions and macros
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAUTIL_H_
#define _THAUTIL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/util/AtLongRegisterAccess.h"
#include "../../../generic/util/AtUtil.h"
#include "AtPwPsn.h"
#include "AtEthVlanTag.h"
#include "AtPdhMdlController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
#define cThaNumDwordsInLongReg   4
#define cThaWrIndrTimeOut        300  /* ms */

#define cThaEthTypeMef       0x88D8
#define cThaEthTypeMplsUcast 0x8847
#define cThaEthTypeIpv4      0x0800
#define cThaEthTypeIpv6      0x86DD

#define cVlanTypeSizeInBytes 2

/**
 * @brief LAPD Header
 */
typedef struct tAtLapdHeader
    {
    uint8 sapi;    /**<   6-bit field SAPI or Service Access Point Identifier */
    uint8 cr;      /**<   [0..1]    Command/Response bit field */
    uint8 iea;     /**<   [0..1]    Initial Address Extension bit field. */
    uint8 tei;     /**<   [0..128]  Terminal End-point Identifier */
    uint8 fea;     /**<   [0..1]    Final Address Extension bit field. */
    uint8 control; /**<   [0..255]  Terminal End-point Identifier */
    } tAtLapdHeader;

/*--------------------------- Macros -----------------------------------------*/
#define mDebugRegPrint(RegName, regValue) AtPrintc(cSevNormal, "    %-20s: 0x%08X (addr = 0x%08X)\r\n", #RegName, regValue, cThaDebug##RegName)
#define mModuleDebugRegPrint(self, regName) mDebugRegPrint(regName, mModuleHwRead(self, cThaDebug##regName))

#define mRoundUpDivide(dividend, divisor) (((dividend) / (divisor)) + ((((dividend) % (divisor)) == 0) ? 0 : 1))

#define mDefineMaskShift(moduleClass, field)                                   \
    static uint32 field##Mask(moduleClass self)                                \
        {                                                                      \
        AtUnused(self);                                                        \
        return cTha##field##Mask;                                              \
        }                                                                      \
                                                                               \
    static uint8 field##Shift(moduleClass self)                                \
        {                                                                      \
        AtUnused(self);                                                        \
        return cTha##field##Shift;                                             \
        }

#define mDefineMaskShiftField(moduleClass, field)                              \
     uint32 (*field##Mask)(moduleClass self);                                  \
     uint8  (*field##Shift)(moduleClass self);

#define mBitFieldOverride(moduleClass, methods, field)                         \
    mMethodOverride(methods, field##Mask);                                     \
    mMethodOverride(methods, field##Shift);

#define mDefineRegAdress(moduleClass, regName)                                 \
    static uint32 regName(moduleClass self)                                    \
        {                                                                      \
        AtUnused(self);                                                        \
        return cThaReg##regName;                                               \
        }

#define mLongFieldSet(longReg, startDword, field, value)                       \
    do                                                                         \
        {                                                                      \
        AtUtilLongFieldSet(longReg, startDword, value,                         \
                           field##01_Mask, field##01_Shift,                    \
                           field##02_Mask, field##02_Shift);                   \
        }while(0)

#define mLongField(longReg, startDword, field)                                 \
    AtUtilLongFieldGet(longReg, startDword,                                    \
                       field##01_Mask, field##01_Shift,                        \
                       field##02_Mask, field##02_Shift)

#define mOsalNetSwap16(value) (ThaBigEndianCheck() ? (value) : ((((value) >> 8) & 0x00FF) | (((value) << 8) & 0xFF00)))
#define mOsalNetSwap32(value) (ThaBigEndianCheck() ? (value) : ((((value) >> 24) & 0x000000FF) | (((value) >> 8 ) & 0x0000FF00) | (((value) << 8 ) & 0x00FF0000) | (((value) << 24) & 0xFF000000)))
#define mOsalHostSwap16(value) (!ThaBigEndianCheck() ? (value) : ((((value) >> 8) & 0x00FF) | (((value) << 8) & 0xFF00)))
#define mOsalHostSwap32(value) (!ThaBigEndianCheck() ? (value) : ((((value) >> 24) & 0x000000FF) | (((value) >> 8 ) & 0x0000FF00) | (((value) << 8 ) & 0x00FF0000) | (((value) << 24) & 0xFF000000)))

#define AtNtoHs(value)  mOsalNetSwap16(value)
#define AtNtoHl(value)  mOsalNetSwap32(value)
#define AtHtoNs(value)  mOsalNetSwap16(value)
#define AtHtoNl(value)  mOsalNetSwap32(value)

#define mVersionReset(self)                                                    \
    do                                                                         \
        {                                                                      \
        AtUnused(self);                                                        \
        return 0;                                                              \
        }while(0)

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtLongRegisterAccess Tha16BitGlobalLongRegisterAccessNew(void);
AtLongRegisterAccess Tha32BitGlobalLongRegisterAccessNew(void);
AtLongRegisterAccess Tha60210011LongRegisterAccessNew(void);
AtLongRegisterAccess Tha60210051LongRegisterAccessNew(void);

/* Packet util */
uint8 ThaPktUtilPutMacToBuffer(uint8 *buffer, uint8 currentIndex, uint8 *mac);
uint8 ThaPktUtilPutEthTypeToBuffer(uint8 *buffer, uint8 currentIndex, uint16 ethType);
uint8 ThaPktUtilPutVlanTagToBuffer(uint8 *buffer, uint8 currentIndex, const tAtEthVlanTag *vlanTag);
uint8 ThaPktUtilPutVlanTypeToBuffer(uint8 *buffer, uint8 currentIndex, uint16 vlanType);
uint8 ThaPktUtilPutVlanToBuffer(uint8 *buffer, uint8 currentIndex, uint16 vlanType, const tAtEthVlanTag *vlanTag);
uint32 ThaPktUtilPutDataToRegister(uint8 *buffer, uint32 currentIndex);
uint16 ThaPktUtilEthTypeFromPsn(AtPwPsn psn);
tAtEthVlanTag *ThaPktUtilVlanTagFromBuffer(uint8 *buffer, tAtEthVlanTag *vlanTag);
uint16 ThaPktUtilVlanTypeFromBuffer(uint8 *buffer);

/* MDL Packet util */
uint8 ThaMdlPktUtilPutZeroToBuffer(uint8 *buffer, uint8 currentIndex, uint8 length);
uint8 ThaMdlPktUtilPutDataArrayToBuffer(uint8 *buffer, uint8 currentIndex, const uint8 *dataArray, uint8 length);
uint8 ThaMdlPktUtilPutLapdHeaderToBuffer(uint8 *buffer, uint8 currentIndex, const tAtLapdHeader *lapdHeader);
uint8 ThaMdlPktUtilPutLapdMessageTypeToBuffer(uint8 *buffer, uint8 currentIndex, uint8 messageType);
uint8 ThaMdlPktUtilLapdMessageTypeFromBuffer(uint8 *buffer);
tAtLapdHeader * ThaMdlPktUtilLapdHeaderFromBuffer(uint8 *buffer, tAtLapdHeader *lapdHeader);
uint8 * ThaMdlPktUtilPutDataArrayFromBuffer(uint8 *buffer, uint8 *dataArray, uint8 length);

/* Vlan Util */
uint32 ThaPktUtilVlanToUint32(const tAtVlan *vlan);
void ThaPktUtilRegUint32ToVlan(uint32 value, tAtVlan *vlan);
uint16 ThaPktUtilVlanTagToUint16(const tAtEthVlanTag *vlanTag);

#ifdef __cplusplus
}
#endif
#endif /* _THAUTIL_H_ */


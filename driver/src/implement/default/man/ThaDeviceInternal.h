/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : ThaDeviceInternal.h
 * 
 * Created Date: Aug 27, 2012
 *
 * Description : Thalassa device
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THADEVICEINTERNAL_H_
#define _THADEVICEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/man/AtDeviceInternal.h"

#include "../bmt/ThaModuleBmt.h"
#include "../cdr/ThaModuleCdr.h"
#include "../cos/ThaModuleCos.h"
#include "../map/ThaModuleMap.h"
#include "../pda/ThaModulePda.h"
#include "../pwe/ThaModulePwe.h"
#include "../cla/ThaModuleCla.h"
#include "../mpig/ThaModuleMpig.h"
#include "../mpeg/ThaModuleMpeg.h"
#include "../pmc/ThaModulePmcMpig.h"
#include "../pmc/ThaModulePmcMpeg.h"
#include "../ocn/ThaModuleOcn.h"
#include "../poh/ThaModulePoh.h"
#include "../pktanalyzer/ThaModulePktAnalyzer.h"
#include "../pmc/ThaModulePmc.h"
#include "../encap/resequence/ThaResequenceManager.h"

#include "ThaDevice.h"
#include "ThaModuleClasses.h"
#include "versionreader/ThaVersionReader.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cThaModulePwPmc cThaModulePmc

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaDeviceMethods
    {
    ThaIntrController (*IntrControllerCreate)(ThaDevice self, AtIpCore core);
    ThaVersionReader (*VersionReaderCreate)(ThaDevice self);
    eBool (*UseIsrInterrupt)(ThaDevice self);
    eBool (*ShouldRetryReset)(ThaDevice self);
    eBool (*ShouldDeprovisionHwOnDeleting)(ThaDevice self);
    uint32 (*ResetRetryTime)(ThaDevice self);
    void (*ClockDebug)(ThaDevice self);
    uint32 (*DefaultCounterModule)(ThaDevice self, uint32 moduleId);

    /* Module partitions */
    uint8 (*MaxNumParts)(ThaDevice self);
    uint32 (*PartOffset)(ThaDevice self, uint8 partId);
    uint8 (*NumPartsOfModule)(ThaDevice self, uint32 moduleId);
    uint32 (*ModulePartOffset)(ThaDevice self, eAtModule moduleId, uint8 partId);

    /* Registers */
    eBool (*CanCheckPll)(ThaDevice self);
    uint32 (*PllClkSysStatReg)(ThaDevice self);
    uint32 (*PllClkSysStatBitMask)(ThaDevice self);
    uint32 (*ModuleActiveMask)(ThaDevice self, uint32 moduleId);
    uint32 (*ModuleActiveShift)(ThaDevice self, uint32 moduleId);
    eBool (*ShouldFlushRange)(ThaDevice self, uint32 startAddress, uint32 stopAddress);

    /* For version controlling */
    eBool (*CanHaveNewOcn)(ThaDevice self);
    uint32 (*StartVersionWithNewOcn)(ThaDevice self);
    uint32 (*StartVersionSupportSurveillance)(ThaDevice self);

    /* To work on Evaluation Platform */
    eBool (*ShouldEnforceEpLogic)(ThaDevice self);

    /* Resequence manager */
    AtResequenceManager (*ResequenceManagerObjectCreate)(ThaDevice self);
    eBool (*ResequenceManagerIsSupported)(ThaDevice self);

    /* Error Generator*/
    eBool (*ErrorGeneratorIsSupported)(ThaDevice self);

    eBool (*SerdesPowerControlIsSupported)(ThaDevice self);

    /* Clock state value */
    eBool (*ClockStateValueV2IsSupported)(ThaDevice self);
    eBool (*ShouldSwitchToPmc)(ThaDevice self);
    eAtRet (*HwFlushAtt)(ThaDevice self);
    eBool (*ClockDebugIsSupported)(ThaDevice self);
    eBool (*StickyDebugIsSupported)(ThaDevice self);

    /* version reader */
    ThaVersionReader (*DeviceVersionReader)(ThaDevice self);
    }tThaDeviceMethods;

/* Thalassa default device */
typedef struct tThaDevice
    {
    tAtDevice super;
    const tThaDeviceMethods *methods;

    /* Local physical modules */
    ThaModuleBmt         bmtModule;
    ThaModuleCdr         cdrModule;
    ThaModuleCos         cosModule;
    ThaModuleAbstractMap mapModule;
    ThaModuleAbstractMap demapModule;
    ThaModulePda         pdaModule;
    ThaModulePwe         pweModule;
    ThaModuleCla         claModule;
    ThaModuleMpig        mpigModule;
    ThaModuleMpeg        mpegModule;
    ThaModulePmcMpeg     pmcMpegModule;
    ThaModulePmcMpig     pmcMpigModule;
    ThaModuleOcn         ocnModule;
    ThaModulePoh         pohModule;
    ThaModulePmc         pmcModule;

    /* OAM polling */
    eBool oamPollingEnabled;

    /* Other purposes */
    ThaVersionReader versionReader;
    eBool resetIsInProgress;
    uint32 resetElapseTimeMs;
    uint32 resetWaitTimeMs;
    uint32 resetTime;
    tAtOsalCurTime startResetTime;
    AtResequenceManager resequenceManager;

    /* Async init */
    uint32 asyncInitState;
    uint32 asyncModulePosition;
    uint32 asyncAllModuleInitState;
    uint8 resetingCore;

    /* For async checking sticky */
    eBool checkingIsRunning;
    tAtOsalCurTime startCheckingTime;
    uint32 goodTime;
    }tThaDevice;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDevice ThaDeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode);
AtDevice ThaDeviceNew(AtDriver driver);

uint8 ThaDeviceChipVersionMajorGet(ThaDevice self);

#ifdef __cplusplus
}
#endif
#endif /* _THADEVICEINTERNAL_H_ */


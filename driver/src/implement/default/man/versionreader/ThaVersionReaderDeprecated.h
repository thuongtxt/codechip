/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : ThaVersionReaderDeprecated.h
 * 
 * Created Date: Aug 10, 2016
 *
 * Description : Version reader deprecated APIs
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAVERSIONREADERDEPRECATED_H_
#define _THAVERSIONREADERDEPRECATED_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 ThaVersionReaderVersionNumber(ThaVersionReader self);
uint32 ThaVersionReaderVersionBuild(uint8 _major, uint8 _minor, uint8 _build);
uint32 ThaVersionReaderPwVersionBuild(ThaVersionReader self, uint8 _year, uint8 _month, uint8 _day, uint8 _hourOrMajorNumber);
uint32 ThaVersionReaderVersionNumberFromHwGet(ThaVersionReader self);
uint32 ThaVersionReaderBuiltNumberFromHwGet(ThaVersionReader self);

/* Some products support hardware major version and built number */
uint32 ThaVersionReaderHardwareVersion(ThaVersionReader self);
uint32 ThaVersionReaderHardwareBuiltNumber(ThaVersionReader self);
uint32 ThaVersionReaderHardwareBuiltNumberBuild(uint8 firstNible, uint8 secondNible, uint8 minor);

/* To manage products that have version and released date. A lot of products so
 * far just manage released date */
uint32 ThaVersionReaderReleasedDate(ThaVersionReader self);
uint32 ThaVersionReaderReleasedDateBuild(uint32 yy, uint32 mm, uint32 dd);

#ifdef __cplusplus
}
#endif
#endif /* _THAVERSIONREADERDEPRECATED_H_ */

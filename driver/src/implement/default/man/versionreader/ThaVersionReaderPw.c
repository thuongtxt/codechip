/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : ThaVersionReaderPwV2.c
 *
 * Created Date: May 12, 2014
 *
 * Description : Device version reader
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaVersionReaderInternal.h"
#include "../ThaDevicePwInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((tThaVersionReaderPw *)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaVersionReaderPwMethods m_methods;

/* Override */
static tThaVersionReaderMethods m_ThaVersionReaderOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 VersionNumberRead(ThaVersionReader self)
    {
    static const uint32 cVersionRegister = 0;
    uint32 longRegVal[cThaLongRegMaxSize];
    uint16 numRegs;
    AtDevice device = ThaVersionReaderDeviceGet(self);

    numRegs = AtDeviceLongReadOnCore(device, cVersionRegister, longRegVal, cThaLongRegMaxSize, 0);
    if (numRegs < 2)
        return 0;

    return longRegVal[1];
    }

static char *VersionDescription(ThaVersionReader self, char *buffer, uint32 bufferLen)
    {
    uint32 versionValue;
    AtOsal osal = AtSharedDriverOsalGet();
    uint8 hour, day, month, year;

    /* Get version value from hardware */
    versionValue = ThaVersionReaderVersionNumber(self);

    /* Make its description */
    if (bufferLen < 20)
        return NULL;

    mMethodsGet(osal)->MemInit(osal, buffer, 0, bufferLen);
    hour  = (uint8)mRegField(versionValue, cVersionHH);
    day   = (uint8)mRegField(versionValue, cVersionDD);
    month = (uint8)mRegField(versionValue, cVersionMM);
    year  = (uint8)mRegField(versionValue, cVersionYY);

    AtSprintf(buffer, "20%d%d/%d%d/%d%d (%d%dh)",
              mLastNible(year) , mFirstNible(year),
              mLastNible(month), mFirstNible(month),
              mLastNible(day)  , mFirstNible(day),
              mLastNible(hour) , mFirstNible(hour));

    return buffer;
    }

static uint32 ReleasedDate(ThaVersionReader self)
    {
    uint32 versionNumber = ThaVersionReaderVersionNumber(self);
    return mRegField(versionNumber, cVersionDate);
    }

static uint32 BuiltNumberRead(ThaVersionReader self)
    {
    uint32 versionNumber = ThaVersionReaderVersionNumber(self);
    return mRegField(versionNumber, cVersionHH);
    }

static uint32 VersionBuild(ThaVersionReader self, uint8 _year, uint8 _month, uint8 _day, uint8 _hour)
    {
    AtUnused(self);
    return mVersionBuild(_year, _month, _day, _hour);
    }

static void MethodsInit(ThaVersionReader self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, VersionBuild);
        }

    mMethodsSet((ThaVersionReaderPw)self, &m_methods);
    }

static void OverrideThaVersionReader(ThaVersionReader self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaVersionReaderOverride, mMethodsGet(self), sizeof(m_ThaVersionReaderOverride));

        mMethodOverride(m_ThaVersionReaderOverride, VersionNumberRead);
        mMethodOverride(m_ThaVersionReaderOverride, VersionDescription);
        mMethodOverride(m_ThaVersionReaderOverride, ReleasedDate);
        mMethodOverride(m_ThaVersionReaderOverride, BuiltNumberRead);
        }

    mMethodsSet(self, &m_ThaVersionReaderOverride);
    }

static void Override(ThaVersionReader self)
    {
    OverrideThaVersionReader(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaVersionReaderPw);
    }

ThaVersionReader ThaVersionReaderPwObjectInit(ThaVersionReader self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    if (device == NULL)
        return NULL;

    /* Super constructor */
    if (ThaVersionReaderObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaVersionReader ThaVersionReaderPwNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaVersionReader newReader = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newReader == NULL)
        return NULL;

    /* Construct it */
    return ThaVersionReaderPwObjectInit(newReader, device);
    }

uint32 ThaVersionReaderPwVersionBuild(ThaVersionReader self, uint8 _year, uint8 _month, uint8 _day, uint8 _hourOrMajorNumber)
    {
    ThaVersionReaderPw pwVersionReader = (ThaVersionReaderPw)self;
    if (self)
        return mMethodsGet(pwVersionReader)->VersionBuild(self, _year, _month, _day, _hourOrMajorNumber);

    return 0;
    }

void ThaVersionReaderPwDayTimeGet(uint32 version, uint8 *_year, uint8 *_month, uint8 *_day, uint8 *_hour)
    {
    *_hour = (uint8)(version & cBit7_0);
    *_day = (uint8)(version >> 8 & cBit7_0);
    *_month = (uint8)(version >> 16 & cBit7_0);
    *_year = (uint8)(version >> 24 & cBit7_0);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : ThaVersionReaderInternal.h
 * 
 * Created Date: May 12, 2014
 *
 * Description : Device version reader
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAVERSIONREADERINTERNAL_H_
#define _THAVERSIONREADERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaVersionReader.h"
#include "AtDevice.h"
#include "../../../../generic/man/AtDriverInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define mFirstNible(uint8Value) ((uint8Value) & 0x0F)
#define mLastNible(uint8Value)  (((uint8Value) & 0xF0) >> 4)

/* Version description */
#define cVersionHHMask    cBit7_0
#define cVersionHHShift   0
#define cVersionDDMask    cBit15_8
#define cVersionDDShift   8
#define cVersionMMMask    cBit23_16
#define cVersionMMShift   16
#define cVersionYYMask    cBit31_24
#define cVersionYYShift   24
#define cVersionDateMask  cBit31_8
#define cVersionDateShift 8
#define mVersionBuild(yy, mm, dd, hh)               \
    ((((uint32)(dd) << cVersionDDShift) & cVersionDDMask) | \
     (((uint32)(mm) << cVersionMMShift) & cVersionMMMask) | \
     (((uint32)(yy) << cVersionYYShift) & cVersionYYMask) | \
     (((uint32)(hh) << cVersionHHShift) & cVersionHHMask))

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaVersionReaderMethods
    {
    uint32 (*BuiltNumberRead)(ThaVersionReader self);
    uint32 (*VersionNumberRead)(ThaVersionReader self);
    char*  (*VersionDescription)(ThaVersionReader self, char *buffer, uint32 bufferLen);
    eBool  (*HasInternalBuiltNumber)(ThaVersionReader self);
    uint32 (*ReleasedDate)(ThaVersionReader self);
    uint32 (*HardwareVersion)(ThaVersionReader self);
    uint32 (*VersionNumber)(ThaVersionReader self);
    eAtRet (*VersionNumberSet)(ThaVersionReader self, uint32 versionNumber);
    eAtRet (*BuiltNumberSet)(ThaVersionReader self, uint32 builtNumber);
    /*purely hw version*/
    uint32 (*VersionNumberFromHwGet)(ThaVersionReader self);
    uint32 (*BuiltNumberFromHwGet)(ThaVersionReader self);

    /* Registers */
    uint32 (*ReleaseDateRegister)(ThaVersionReader self);
    uint32 (*VersionRegister)(ThaVersionReader self);
    }tThaVersionReaderMethods;

typedef struct tThaVersionReader
    {
    tAtObject super;
    const tThaVersionReaderMethods *methods;

    AtDevice device;

    /* Cache version information for backward compatible performance */
    uint32 versionNumber;
    uint32 builtNumber;
    }tThaVersionReader;

typedef struct tThaVersionReaderPwMethods
    {
    uint32 (*VersionBuild)(ThaVersionReader self, uint8 _year, uint8 _month, uint8 _day, uint8 _hour);
    }tThaVersionReaderPwMethods;

typedef struct tThaVersionReaderPw
    {
    tThaVersionReader super;
    const tThaVersionReaderPwMethods *methods;
    }tThaVersionReaderPw;


/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDevice ThaVersionReaderDeviceGet(ThaVersionReader self);
ThaVersionReader ThaVersionReaderObjectInit(ThaVersionReader self, AtDevice device);
ThaVersionReader ThaVersionReaderPwObjectInit(ThaVersionReader self, AtDevice device);

/* Concrete products */
ThaVersionReader Tha60150011VersionReaderObjectInit(ThaVersionReader self, AtDevice device);
ThaVersionReader Tha60210011VersionReaderObjectInit(ThaVersionReader self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAVERSIONREADERINTERNAL_H_ */


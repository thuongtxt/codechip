/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : ThaVersionReader.c
 *
 * Created Date: May 12, 2014
 *
 * Description : Device version reader
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/man/AtDeviceInternal.h"
#include "../ThaDeviceReg.h"
#include "ThaVersionReaderInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tThaVersionReaderMethods m_methods;

static tAtObjectMethods         m_AtObjectOverride;

static const tAtObjectMethods  *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 VersionRegister(ThaVersionReader self)
    {
	AtUnused(self);
    return cThaRegChipVersion;
    }

static uint32 ReleaseDateRegister(ThaVersionReader self)
    {
	AtUnused(self);
    return cThaRegChipReleaseDate;
    }

static uint32 VersionNumberRead(ThaVersionReader self)
    {
    AtDevice device = ThaVersionReaderDeviceGet(self);
    uint32 regAddr = mMethodsGet(self)->VersionRegister(self);
    return AtHalRead(AtDeviceIpCoreHalGet(device, 0), regAddr);
    }

static uint32 BuiltNumberRead(ThaVersionReader self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 BuiltNumber(ThaVersionReader self)
    {
    if (self->builtNumber == 0)
        self->builtNumber = mMethodsGet(self)->BuiltNumberRead(self);
    return self->builtNumber;
    }

static uint32 IsValidValue(uint32 value)
    {
    switch (value)
        {
        case 0xCAFECAFE: return cAtFalse;
        case 0xC0CAC0CA: return cAtFalse;
        case 0xCAC0CAC0: return cAtFalse;
        case 0x0       : return cAtFalse;
        default:
            return cAtTrue;
        }
    }

static eBool VersionIsStable(ThaVersionReader self, uint32 expectedVersionNumber)
    {
    static const uint32 cTimeoutMs = 100;
    tAtOsalCurTime startTime, curTime;
    uint32 elapseTimeMs = 0;

    AtOsalCurTimeGet(&startTime);
    while (elapseTimeMs < cTimeoutMs)
        {
        if (mMethodsGet(self)->VersionNumberRead(self) != expectedVersionNumber)
            return cAtFalse;

        AtOsalCurTimeGet(&curTime);
        elapseTimeMs = mTimeIntervalInMsGet(startTime, curTime);
        }

    return cAtTrue;
    }

static uint32 StableVersionNumberRead(ThaVersionReader self)
    {
    static const uint32 cTimeoutMs = 1000;
    tAtOsalCurTime startTime, curTime;
    uint32 elapseTimeMs = 0;

    /* Cannot determine version when there is no HAL. Try next time. */
    if (AtDeviceIpCoreHalGet(ThaVersionReaderDeviceGet(self), 0) == NULL)
        return 0;

    if (AtDeviceIsSimulated(self->device) || AtDeviceInAccessible(self->device))
        return mMethodsGet(self)->VersionNumberRead(self);

    AtOsalCurTimeGet(&startTime);
    while (elapseTimeMs < cTimeoutMs)
        {
        uint32 version = mMethodsGet(self)->VersionNumberRead(self);

        /* Just for simulation purpose */
        if (AtDeviceIsSimulated(self->device) || AtDeviceInAccessible(self->device))
            return version;

        if (IsValidValue(version) && VersionIsStable(self, version))
            return version;

        /* Release CPU and retry */
        AtOsalUSleep(1000);
        AtOsalCurTimeGet(&curTime);
        elapseTimeMs = mTimeIntervalInMsGet(startTime, curTime);
        }

    /* Cannot find a stable version number, return invalid value for next catching */
    AtDeviceLog(self->device, cAtLogLevelWarning, AtSourceLocation, "Cannot determine stable device version, will try next time\r\n");
    return 0;
    }

static uint32 VersionNumber(ThaVersionReader self)
    {
    if (self->versionNumber == 0)
        self->versionNumber = StableVersionNumberRead(self);

    return self->versionNumber;
    }

static void PutFpgaVersionDescription(ThaVersionReader self, char *buffer)
    {
    uint16 buildNumber;
    uint8 _major, _minor;
    char versionDescription[16];
    uint32 versionNumber = ThaVersionReaderVersionNumber(self);

    mFieldGet(versionNumber, cThaRegChipVersionMajorMask, cThaRegChipVersionMajorShift, uint8, &_major);
    mFieldGet(versionNumber, cThaRegChipVersionMinorMask, cThaRegChipVersionMinorShift, uint8, &_minor);
    mFieldGet(versionNumber, cThaRegChipVersionBuildMask, cThaRegChipVersionBuildShift, uint16, &buildNumber);

    AtSprintf(versionDescription, "%u.%u.%u", _major, _minor, buildNumber);
    AtStrcat(buffer, versionDescription);
    }

static uint32 ReleaseDateRead(ThaVersionReader self)
    {
    AtDevice device = ThaVersionReaderDeviceGet(self);
    AtHal hal = AtDeviceIpCoreHalGet(device, 0);
    return AtHalRead(hal, mMethodsGet(self)->ReleaseDateRegister(self));
    }

static void PutReleaseDateDescription(ThaVersionReader self, char *buffer)
    {
    uint8 day, month, year, releaseNumber;
    uint32 regValue = ReleaseDateRead(self);
    char releaseDateDescription[32];

    mFieldGet(regValue, cThaRegChipReleaseDateYearMask, cThaRegChipReleaseDateYearShift, uint8, &year);
    mFieldGet(regValue, cThaRegChipReleaseDateMonthMask, cThaRegChipReleaseDateMonthShift, uint8, &month);
    mFieldGet(regValue, cThaRegChipReleaseDateDayMask, cThaRegChipReleaseDateDayShift, uint8, &day);
    mFieldGet(regValue, cThaRegChipReleaseDateInternalReleaseMask, cThaRegChipReleaseDateInternalReleaseShift, uint8, &releaseNumber);

    AtSprintf(releaseDateDescription, "20%u%u-%u%u-%u%u(%u)",
              (uint32)mLastNible(year),  (uint32)mFirstNible(year),
              (uint32)mLastNible(month), (uint32)mFirstNible(month),
              (uint32)mLastNible(day),   (uint32)mFirstNible(day),
              releaseNumber);

    AtStrcat(buffer, releaseDateDescription);
    }

static char *VersionDescription(ThaVersionReader self, char *buffer, uint32 bufferLen)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, buffer, 0, bufferLen);

    PutFpgaVersionDescription(self, buffer);
    AtStrcat(buffer, " on ");
    PutReleaseDateDescription(self, buffer);

    return buffer;
    }

static eBool HasInternalBuiltNumber(ThaVersionReader self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 ReleasedDate(ThaVersionReader self)
    {
    uint32 releasedDate = ReleaseDateRead(self);
    return mRegField(releasedDate, cThaRegChipReleaseDate);
    }

static uint32 HardwareVersion(ThaVersionReader self)
    {
    return ThaVersionReaderVersionNumber(self);
    }

static eAtRet VersionNumberSet(ThaVersionReader self, uint32 versionNumber)
    {
    self->versionNumber = versionNumber;
    return cAtOk;
    }

static eAtRet BuiltNumberSet(ThaVersionReader self, uint32 builtNumber)
    {
    self->builtNumber = builtNumber;
    return cAtOk;
    }

static uint32 VersionNumberFromHwGet(ThaVersionReader self)
    {
    return ThaVersionReaderVersionNumber(self);
    }

static uint32 BuiltNumberFromHwGet(ThaVersionReader self)
    {
    return ThaVersionReaderHardwareBuiltNumber(self);
    }

static void MethodsInit(ThaVersionReader self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, BuiltNumberRead);
        mMethodOverride(m_methods, VersionNumberRead);
        mMethodOverride(m_methods, VersionDescription);
        mMethodOverride(m_methods, ReleaseDateRegister);
        mMethodOverride(m_methods, VersionRegister);
        mMethodOverride(m_methods, HasInternalBuiltNumber);
        mMethodOverride(m_methods, ReleasedDate);
        mMethodOverride(m_methods, HardwareVersion);
        mMethodOverride(m_methods, VersionNumber);
        mMethodOverride(m_methods, VersionNumberSet);
        mMethodOverride(m_methods, BuiltNumberSet);
        mMethodOverride(m_methods, VersionNumberFromHwGet);
        mMethodOverride(m_methods, BuiltNumberFromHwGet);
        }

    mMethodsSet(self, &m_methods);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaVersionReader object = (ThaVersionReader)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(device);
    mEncodeUInt(versionNumber);
    mEncodeUInt(builtNumber);
    }

static void OverrideAtObject(ThaVersionReader self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(ThaVersionReader self)
    {
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaVersionReader);
    }

ThaVersionReader ThaVersionReaderObjectInit(ThaVersionReader self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    if (device == NULL)
        return NULL;

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Private data */
    self->device = device;

    return self;
    }

ThaVersionReader ThaVersionReaderNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaVersionReader newReader = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newReader == NULL)
        return NULL;

    /* Construct it */
    return ThaVersionReaderObjectInit(newReader, device);
    }

uint32 ThaVersionReaderVersionNumberFromHwGet(ThaVersionReader self)
    {
    if (self)
        return mMethodsGet(self)->VersionNumberFromHwGet(self);

    return 0;
    }

uint32 ThaVersionReaderBuiltNumberFromHwGet(ThaVersionReader self)
    {
    if (self)
        return mMethodsGet(self)->BuiltNumberFromHwGet(self);

    return 0;
    }

uint32 ThaVersionReaderVersionNumber(ThaVersionReader self)
    {
    if (self)
        return mMethodsGet(self)->VersionNumber(self);

    return 0;
    }

char* ThaVersionReaderVersionDescription(ThaVersionReader self, char *buffer, uint32 bufferLen)
    {
    if (self)
        return mMethodsGet(self)->VersionDescription(self, buffer, bufferLen);

    AtSprintf(buffer, "Unknown");
    return buffer;
    }

AtDevice ThaVersionReaderDeviceGet(ThaVersionReader self)
    {
    if (self)
        return self->device;
    return NULL;
    }

uint32 ThaVersionReaderVersionBuild(uint8 _major, uint8 _minor, uint8 _build)
    {
    return mThaVersionBuild(_major, _minor, _build);
    }

uint32 ThaVersionReaderHardwareVersion(ThaVersionReader self)
    {
    if (self)
        return mMethodsGet(self)->HardwareVersion(self);
    return 0x0;
    }

uint32 ThaVersionReaderHardwareVersionWithBuiltNumberBuild(uint32 major, uint32 minor, uint32 betaBuild)
    {
    uint32 version = 0;

    mRegFieldSet(version, cThaRegChipVersionMajor, major);
    mRegFieldSet(version, cThaRegChipVersionMinor, minor);
    mRegFieldSet(version, cThaRegChipVersionBuild, betaBuild);

    return version;
    }

uint32 ThaVersionReaderHardwareBuiltNumber(ThaVersionReader self)
    {
    if (self)
        return BuiltNumber(self);

    return 0;
    }

uint32 ThaVersionReaderHardwareBuiltNumberBuild(uint8 firstNible, uint8 secondNible, uint8 minor)
    {
    return mThaHwBuiltNumberBuild(firstNible, secondNible, minor);
    }

uint32 ThaVersionReaderReleasedDate(ThaVersionReader self)
    {
    if (self)
        return mMethodsGet(self)->ReleasedDate(self);
    return 0x0;
    }

uint32 ThaVersionReaderReleasedDateBuild(uint32 yy, uint32 mm, uint32 dd)
    {
    return (dd & cBit7_0) | ((mm & cBit7_0) << 8) | ((yy & cBit7_0) << 16);
    }

eAtRet ThaVersionReaderVersionNumberSet(ThaVersionReader self, uint32 versionNumber)
    {
    if (self)
        return mMethodsGet(self)->VersionNumberSet(self, versionNumber);
    return cAtErrorNullPointer;
    }

eAtRet ThaVersionReaderBuiltNumberSet(ThaVersionReader self, uint32 builtNumber)
    {
    if (self)
        return mMethodsGet(self)->BuiltNumberSet(self, builtNumber);
    return cAtErrorNullPointer;
    }

uint32 ThaVersionReaderHardwareVersionAndBuiltNumber(ThaVersionReader self)
    {
    uint32 hwVersion = ThaVersionReaderHardwareVersion(self);
    uint32 hwBuiltNumber = ThaVersionReaderHardwareBuiltNumber(self);
    return hwVersion | hwBuiltNumber;
    }

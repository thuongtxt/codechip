/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : ThaVersionReader.h
 * 
 * Created Date: May 12, 2014
 *
 * Description : Device version reader
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAVERSIONREADER_H_
#define _THAVERSIONREADER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h" /* Super class */
#include "AtDevice.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaVersionReader * ThaVersionReader;
typedef struct tThaVersionReaderPw * ThaVersionReaderPw;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet ThaVersionReaderVersionNumberSet(ThaVersionReader self, uint32 versionNumber);
eAtRet ThaVersionReaderBuiltNumberSet(ThaVersionReader self, uint32 builtNumber);
uint32 ThaVersionReaderHardwareVersionWithBuiltNumberBuild(uint32 major, uint32 minor, uint32 betaBuild);
uint32 ThaVersionReaderHardwareVersionAndBuiltNumber(ThaVersionReader self);

/* Utils */
char* ThaVersionReaderVersionDescription(ThaVersionReader self, char *buffer, uint32 bufferLen);
void ThaVersionReaderPwDayTimeGet(uint32 version, uint8 *_year, uint8 *_month, uint8 *_day, uint8 *_hour);

/* Default concretes */
ThaVersionReader ThaVersionReaderNew(AtDevice device);
ThaVersionReader ThaVersionReaderPwNew(AtDevice device);

/* Product concretes */
ThaVersionReader Tha60091023VersionReaderNew(AtDevice device);
ThaVersionReader Tha60150011VersionReaderNew(AtDevice device);
ThaVersionReader Tha60210011VersionReaderNew(AtDevice device);
ThaVersionReader Tha60210011VersionReaderNew(AtDevice device);
ThaVersionReader Tha60210051VersionReaderNew(AtDevice device);
ThaVersionReader Tha60210031VersionReaderNew(AtDevice device);
ThaVersionReader Tha60210021VersionReaderNew(AtDevice device);
ThaVersionReader Tha60210012VersionReaderNew(AtDevice device);
ThaVersionReader Tha60290021VersionReaderNew(AtDevice device);
ThaVersionReader Tha60290011VersionReaderNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAVERSIONREADER_H_ */

#include "ThaVersionReaderDeprecated.h"


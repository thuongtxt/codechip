/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Management
 *
 * File        : AtDevice.c
 *
 * Created Date: Aug 27, 2012
 *
 * Author      : namnn
 *
 * Description : This source file is default implementation of Thalassa device.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "ThaDeviceInternal.h"
#include "ThaIpCoreInternal.h"
#include "AtModulePdh.h"
#include "AtModuleSdh.h"
#include "AtModuleEth.h"
#include "AtModulePw.h"
#include "ThaDeviceReg.h"
#include "AtModuleBer.h"
#include "AtRam.h"
#include "../eth/ThaModuleEth.h"
#include "../pdh/ThaModulePdh.h"
#include "../sdh/ThaModuleSdh.h"
#include "../cla/ThaModuleCla.h"
#include "../pktanalyzer/ThaModulePktAnalyzer.h"
#include "../pw/ThaModulePw.h"
#include "../ram/ThaModuleRam.h"
#include "../prbs/ThaModulePrbs.h"
#include "../sur/hard/ThaModuleHardSur.h"
#include "../physical/ThaThermalSensor.h"
#include "../framerelay/ThaModuleFr.h"
#include "../diag/querier/ThaQueriers.h"

/* Concrete devices */
#include "../../ThaPdhMlppp/man/ThaPdhMlpppDevice.h"
#include "../../ThaStmMlppp/man/ThaStmMlpppDevice.h"
#include "../../ThaPdhPw/man/ThaPdhPwDevice.h"
#include "../../ThaStmPw/man/ThaStmPwDevice.h"
#include "../util/ThaUtil.h"
#include "../../../util/coder/AtCoderUtil.h"

#include "../../../generic/man/AtDriverLogMacro.h"
#include "../../codechip/Tha6A290081/man/Tha6A290081Device.h"

#include "ThaDeviceAsyncInit.h"

/*--------------------------- Define -----------------------------------------*/
#define cInvalidValue 0xDEADCAFE

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaDevice)self)

#define mDeviceCreate(driver, productCode, realProductCode, supportedCode)     \
    case 0x##supportedCode: return Tha##supportedCode##DeviceNew(driver, productCode)
#define mEpDeviceCreate(driver, productCode, realProductCode, supportedCode)   \
    if ((realProductCode) == 0x##supportedCode) return Tha##supportedCode##EpDeviceNew(driver, productCode);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tThaDeviceMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;
static tAtDeviceMethods m_AtDeviceOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;
static const tAtDeviceMethods *m_AtDeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    eThaPhyModule phyModule = moduleId;

    /* Concrete logical modules */
    if (moduleId == cAtModulePw)          return (AtModule)ThaModulePwNew(self);
    if (moduleId == cAtModulePdh)         return (AtModule)ThaModulePdhNew(self);
    if (moduleId == cAtModuleEth)         return (AtModule)ThaModuleEthNew(self);
    if (moduleId == cAtModuleSdh)         return (AtModule)ThaModuleSdhNew(self);
    if (moduleId == cAtModulePktAnalyzer) return (AtModule)ThaModulePktAnalyzerNew(self);
    if (moduleId == cAtModuleRam)         return (AtModule)ThaModuleRamNew(self);
    if (moduleId == cAtModulePrbs)        return (AtModule)ThaModulePrbsNew(self);
    if (moduleId == cAtModuleSur)         return (AtModule)ThaModuleHardSurNew(self);
    if (moduleId == cAtModuleFr)          return (AtModule)ThaModuleFrNew(self);

    /* Additional physical modules */
    if (phyModule == cThaModuleMap)     return ThaModuleMapNew(self);
    if (phyModule == cThaModuleDemap)   return ThaModuleDemapNew(self);
    if (phyModule == cThaModuleCdr)     return ThaModuleCdrNew(self);
    if (phyModule == cThaModulePwe)     return ThaModulePweNew(self);
    if (phyModule == cThaModulePda)     return ThaModulePdaNew(self);
    if (phyModule == cThaModuleCos)     return ThaModuleCosNew(self);
    if (phyModule == cThaModuleBmt)     return ThaModuleBmtNew(self);
    if (phyModule == cThaModuleOcn)     return ThaModuleOcnNew(self);
    if (phyModule == cThaModulePoh)     return ThaModulePohV2New(self);
    if (phyModule == cThaModulePmc)     return ThaModulePmcNew(self);

    return NULL;
    }

static AtModule ModuleGet(AtDevice self, eAtModule moduleId)
    {
    ThaDevice device = (ThaDevice)self;
    eThaPhyModule phyModuleId = moduleId;

    if (!AtDeviceModuleIsSupported(self, moduleId))
        return NULL;

    /* Try getting physical module */
    if (phyModuleId == cThaModuleMap)     return (AtModule)(device->mapModule);
    if (phyModuleId == cThaModuleDemap)   return (AtModule)(device->demapModule);
    if (phyModuleId == cThaModuleCdr)     return (AtModule)(device->cdrModule);
    if (phyModuleId == cThaModuleCla)     return (AtModule)(device->claModule);
    if (phyModuleId == cThaModulePwe)     return (AtModule)(device->pweModule);
    if (phyModuleId == cThaModulePda)     return (AtModule)(device->pdaModule);
    if (phyModuleId == cThaModuleCos)     return (AtModule)(device->cosModule);
    if (phyModuleId == cThaModuleBmt)     return (AtModule)(device->bmtModule);
    if (phyModuleId == cThaModuleMpig)    return (AtModule)(device->mpigModule);
    if (phyModuleId == cThaModuleMpeg)    return (AtModule)(device->mpegModule);
    if (phyModuleId == cThaModulePmcMpeg) return (AtModule)(device->pmcMpegModule);
    if (phyModuleId == cThaModulePmcMpig) return (AtModule)(device->pmcMpigModule);
    if (phyModuleId == cThaModuleOcn)     return (AtModule)(device->ocnModule);
    if (phyModuleId == cThaModulePoh)     return (AtModule)(device->pohModule);
    if (phyModuleId == cThaModulePmc)     return (AtModule)(device->pmcModule);

    /* If not success, let super class determine */
    return m_AtDeviceMethods->ModuleGet(self, moduleId);
    }

static void AllModulesCreate(AtDevice self)
    {
    ThaDevice device = (ThaDevice)self;

    /* Super implement */
    m_AtDeviceMethods->AllModulesCreate(self);

    /* Additional modules */
    device->mapModule     = (ThaModuleAbstractMap)mMethodsGet(self)->ModuleCreate(self, cThaModuleMap);
    device->demapModule   = (ThaModuleAbstractMap)mMethodsGet(self)->ModuleCreate(self, cThaModuleDemap);
    device->cdrModule     = (ThaModuleCdr)mMethodsGet(self)->ModuleCreate(self, cThaModuleCdr);
    device->claModule     = (ThaModuleCla)mMethodsGet(self)->ModuleCreate(self, cThaModuleCla);
    device->pweModule     = (ThaModulePwe)mMethodsGet(self)->ModuleCreate(self, cThaModulePwe);
    device->pdaModule     = (ThaModulePda)mMethodsGet(self)->ModuleCreate(self, cThaModulePda);
    device->cosModule     = (ThaModuleCos)mMethodsGet(self)->ModuleCreate(self, cThaModuleCos);
    device->bmtModule     = (ThaModuleBmt)mMethodsGet(self)->ModuleCreate(self, cThaModuleBmt);
    device->mpigModule    = (ThaModuleMpig)mMethodsGet(self)->ModuleCreate(self, cThaModuleMpig);
    device->mpegModule    = (ThaModuleMpeg)mMethodsGet(self)->ModuleCreate(self, cThaModuleMpeg);
    device->pmcMpegModule = (ThaModulePmcMpeg)mMethodsGet(self)->ModuleCreate(self, cThaModulePmcMpeg);
    device->pmcMpigModule = (ThaModulePmcMpig)mMethodsGet(self)->ModuleCreate(self, cThaModulePmcMpig);
    device->ocnModule     = (ThaModuleOcn)mMethodsGet(self)->ModuleCreate(self, cThaModuleOcn);
    device->pohModule     = (ThaModulePoh)mMethodsGet(self)->ModuleCreate(self, cThaModulePoh);
    device->pmcModule     = (ThaModulePmc)mMethodsGet(self)->ModuleCreate(self, cThaModulePmc);
    }

static void AllModulesDelete(AtDevice self)
    {
    ThaDevice device = (ThaDevice)self;

    /* Super implement */
    m_AtDeviceMethods->AllModulesDelete(self);

    /* Delete additional modules */
    AtObjectDelete((AtObject)(device->mapModule));
    AtObjectDelete((AtObject)(device->demapModule));
    AtObjectDelete((AtObject)(device->cdrModule));
    AtObjectDelete((AtObject)(device->claModule));
    AtObjectDelete((AtObject)(device->pweModule));
    AtObjectDelete((AtObject)(device->pdaModule));
    AtObjectDelete((AtObject)(device->cosModule));
    AtObjectDelete((AtObject)(device->bmtModule));
    AtObjectDelete((AtObject)(device->mpigModule));
    AtObjectDelete((AtObject)(device->mpegModule));
    AtObjectDelete((AtObject)(device->pmcMpigModule));
    AtObjectDelete((AtObject)(device->pmcMpegModule));
    AtObjectDelete((AtObject)(device->ocnModule));
    AtObjectDelete((AtObject)(device->pohModule));
    AtObjectDelete((AtObject)(device->pmcModule));
    }

static eAtRet AllModulesInit(AtDevice self)
    {
    eAtRet ret = cAtOk;
    uint8 i, numCore = AtDeviceNumIpCoresGet(self);

    /* Disable interrupt mask for all modules in all cores. */
    for (i = 0; i < numCore; i++)
        {
        ThaIpCore ipCore = (ThaIpCore)AtDeviceIpCoreGet(self, i);
        if (ipCore)
            mMethodsGet(ipCore)->InterruptDisable(ipCore);
        }

    /* Initialize all physical modules */
    ret |= AtDeviceModuleInit(self, cThaModuleMap);
    ret |= AtDeviceModuleInit(self, cThaModuleDemap);
    ret |= AtDeviceModuleInit(self, cThaModuleCdr);
    ret |= AtDeviceModuleInit(self, cThaModuleCla);
    ret |= AtDeviceModuleInit(self, cThaModulePwe);
    ret |= AtDeviceModuleInit(self, cThaModulePda);
    ret |= AtDeviceModuleInit(self, cThaModuleCos);
    ret |= AtDeviceModuleInit(self, cThaModuleBmt);
    ret |= AtDeviceModuleInit(self, cThaModuleMpig);
    ret |= AtDeviceModuleInit(self, cThaModuleMpeg);
    ret |= AtDeviceModuleInit(self, cThaModulePmcMpig);
    ret |= AtDeviceModuleInit(self, cThaModulePmcMpeg);
    ret |= AtDeviceModuleInit(self, cThaModuleOcn);
    ret |= AtDeviceModuleInit(self, cThaModulePoh);
    ret |= AtDeviceModuleInit(self, cThaModulePmc);

    /* Let's super do remaining thing */
    ret |= m_AtDeviceMethods->AllModulesInit(self);

    return ret;
    }

static AtIpCore IpCoreCreate(AtDevice self, uint8 coreId)
    {
    return (AtIpCore)ThaIpCoreNew(coreId, self);
    }

static uint32 SimulationCode(AtDevice self)
    {
    AtUnused(self);
    return 0xF;
    }

static void ResequenceManagerDelete(ThaDevice self)
    {
    if (self->resequenceManager == NULL)
        return;

    AtObjectDelete((AtObject)self->resequenceManager);
    self->resequenceManager = NULL;
    }

static void VersionNumberCache(AtDevice self)
    {
    (void)AtDeviceVersionNumber(self);
    (void)AtDeviceBuiltNumber(self);
    }

static void EntranceToInit(AtDevice self)
    {
    AtModuleEth ethModule;

    /* Have version number cache in database */
    VersionNumberCache(self);

    mThis(self)->resetIsInProgress = cAtTrue;

    /* As hardware recommend, Ethernet ports should be de-activated before
     * initializing device. Device initialization process will activate these
     * ports */
    ethModule = (AtModuleEth)AtDeviceModuleGet(self, cAtModuleEth);
    ThaModuleEthAllPortMacsActivate(ethModule, cAtFalse);
    ThaModuleClaAllTrafficsDeactivate((ThaModuleCla)AtDeviceModuleGet(self, cThaModuleCla));
    }

static void PostInit(AtDevice self)
    {
    mThis(self)->resetIsInProgress = cAtFalse;
    }

static eAtRet Init(AtDevice self)
    {
    eAtRet ret = cAtOk;

    EntranceToInit(self);
    ret = m_AtDeviceMethods->Init(self);
    PostInit(self);

    return ret;
    }

static void EnterIsrContext(AtDevice self)
    {
    uint8 i, numCore;
    AtHal hal;

    numCore = AtDeviceNumIpCoresGet(self);
    for (i = 0; i < numCore; i++)
        {
        hal = AtIpCoreHalGet(AtDeviceIpCoreGet(self, i));
        AtHalIsrContextEnter(hal);
        AtHalStateSave(hal);
        }
    }

static void ExitIsrContext(AtDevice self)
    {
    uint8 i, numCore;
    AtHal hal;

    numCore = AtDeviceNumIpCoresGet(self);
    for (i = 0; i < numCore; i++)
        {
        hal = AtIpCoreHalGet(AtDeviceIpCoreGet(self, i));
        AtHalStateRestore(hal);
        AtHalIsrContextExit(hal);
        }
    }

static void InterruptProcess(AtDevice self)
    {
    uint8 i, numCore;
    eBool useIsrInterrupt = mMethodsGet(mThis(self))->UseIsrInterrupt(mThis(self));

    /* Suspend interrupt process during device initialization. */
    if (mThis(self)->resetIsInProgress)
        return;

    numCore = AtDeviceNumIpCoresGet(self);

    if (useIsrInterrupt)
        EnterIsrContext(self);

    /* process interrupt alarm at the device top layer such as role switch done */
    mMethodsGet(self)->DevAlarmInterruptProcess(self);

    /* Process interrupt */
    for (i = 0; i < numCore; i++)
        {
        ThaIpCore core = (ThaIpCore)AtDeviceIpCoreGet(self, i);
        ThaIpCoreInterruptProcess(core);
        }

    if (useIsrInterrupt)
        ExitIsrContext(self);
    }

static eAtRet InterruptRestore(AtDevice self)
    {
    uint8 i, numCore = AtDeviceNumIpCoresGet(self);
    eAtRet ret = cAtOk;

    for (i = 0; i < numCore; i++)
        {
        ThaIpCore ipCore = (ThaIpCore)AtDeviceIpCoreGet(self, i);
        ret |= ThaIpCoreInterruptRestore(ipCore);
        }

    return ret;
    }

static void OamPollingOnCore(AtDevice self, uint8 coreId)
    {
    static const uint16 cNumServedOamsAtOneTime = 64;
    AtModule pppModule = AtDeviceModuleGet(self, cAtModulePpp);

    if (pppModule == NULL)
        return;

    /* At this moment, just need to poll OAM for PPP module where LCP/NCP
     * control messages are exchanged */
    mMethodsGet(pppModule)->OamProcessOnCore(pppModule, AtDeviceIpCoreGet(self, coreId), cNumServedOamsAtOneTime);

    /* Polling OAM for other modules may be added soon */
    }

static void OamPolling(AtDevice self)
    {
    uint8 i;

    for (i = 0; i < AtDeviceNumIpCoresGet(self); i++)
        OamPollingOnCore(self, i);
    }

static void PeriodicProcess(AtDevice self, uint32 periodInMs)
    {
    ThaDevice thaDevice = (ThaDevice)self;

    /* Let the super do first */
    m_AtDeviceMethods->PeriodicProcess(self, periodInMs);

    /*  Additional polling */
    if (thaDevice->oamPollingEnabled)
        OamPolling(self);

    /* BER software processing */
    AtModuleBerPeriodicProcess((AtModuleBer)AtDeviceModuleGet(self, cAtModuleBer), periodInMs);
    }

static AtModule ModuleOfRegisterGet(AtDevice self, uint32 localAddress)
    {
    AtIterator moduleIterator = ThaDeviceModuleIteratorCreate(self);
    AtModule module;
    uint32 moduleLocalAddress = localAddress /* & cBit23_0  FIXME: need to review this for products that have more than one parts */;

    /* Iterate all modules including physical modules */
    while ((module = (AtModule)AtIteratorNext(moduleIterator)) != NULL)
        {
        if (mMethodsGet(module)->HasRegister(module, moduleLocalAddress))
            break;
        }

    AtObjectDelete((AtObject)moduleIterator);

    return module;
    }

static uint16 LongReadOnCore(AtDevice self, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen, uint8 coreId)
    {
    AtModule module;
    uint16 numRegs = 0;

    /* Let module that contains this register do first */
    module = ModuleOfRegisterGet(self, localAddress);
    if (module && (AtModuleTypeGet(module) == cAtModulePdh))
		{
    	AtAttPdhManager mng = AtModulePdhAttManager((AtModulePdh)module);
    	if (mng)
    		{
			eBool has = AtModulePdhAttManagerHasRegister(mng, localAddress);
			if (has)
				{
				numRegs = AtModulePdhAttManagerLongRead(mng, localAddress, dataBuffer, bufferLen, AtDeviceIpCoreGet(self, coreId));
				return numRegs;
				}
    		}
		}
    if (module)
        numRegs = mMethodsGet(module)->HwLongReadOnCore(module, localAddress, dataBuffer, bufferLen, AtDeviceIpCoreGet(self, coreId));

    /* For register that has not been handled by all modules, use global long
     * register access to access this register */
    if (numRegs == 0)
        {
        AtLongRegisterAccess longRegAccess = AtDeviceGlobalLongRegisterAccess(self);
        AtHal hal = AtDeviceIpCoreHalGet(self, coreId);
        if (hal)
            numRegs = AtLongRegisterAccessRead(longRegAccess, hal, localAddress, dataBuffer, bufferLen);
        else
            AtDeviceLog(self, cAtLogLevelWarning, AtSourceLocation, "Access registers 0x%08x when no HAL\r\n", localAddress);
        }

    return numRegs;
    }

static uint16 LongWriteOnCore(AtDevice self, uint32 localAddress, const uint32 *dataBuffer, uint16 bufferLen, uint8 coreId)
    {
    AtModule module = ModuleOfRegisterGet(self, localAddress);
    uint16 numRegs = 0;

    if (module && (AtModuleTypeGet(module) == cAtModulePdh))
		{
		AtAttPdhManager mng = AtModulePdhAttManager((AtModulePdh)module);
		if (mng)
			{
			eBool has = AtModulePdhAttManagerHasRegister(mng, localAddress);
			if (has)
				{
				numRegs = AtModulePdhAttManagerLongWrite(mng, localAddress, dataBuffer, bufferLen, AtDeviceIpCoreGet(self, coreId));
				return numRegs;
				}
			}
		}
    /* Write this register if it is a long one */
    if (module)
        numRegs = mMethodsGet(module)->HwLongWriteOnCore(module, localAddress, dataBuffer, bufferLen, AtDeviceIpCoreGet(self, coreId));

    /* For register that has not been handled by all modules, use global long
     * register access to access this register */
    if (numRegs == 0)
        {
        AtLongRegisterAccess longRegAccess = AtDeviceGlobalLongRegisterAccess(self);
        AtHal hal = AtDeviceIpCoreHalGet(self, coreId);
        if (hal)
            numRegs = AtLongRegisterAccessWrite(longRegAccess, hal, localAddress, dataBuffer, bufferLen);
        else
            AtDeviceLog(self, cAtLogLevelWarning, AtSourceLocation, "Access registers 0x%08x when no HAL\r\n", localAddress);
        }

    return numRegs;
    }

static void DeactivateAllModulesOnPart(AtDevice self, uint8 partId)
    {
    AtIterator coreIterator = AtDeviceCoreIteratorCreate(self);
    AtIpCore core;
    uint32 regAddress = 0x1 + ThaDevicePartOffset((ThaDevice)self, partId);

    while ((core = (AtIpCore)AtIteratorNext(coreIterator)) != NULL)
        AtIpCoreWrite(core, regAddress, 0);

    AtObjectDelete((AtObject)coreIterator);
    }

static void DeactivateAllModules(AtDevice self)
    {
    uint8 partId;

    for (partId = 0; partId < ThaDeviceMaxNumParts((ThaDevice)self); partId++)
        DeactivateAllModulesOnPart(self, partId);
    }

static eAtRet MemoryTest(AtDevice self)
    {
    AtIterator moduleIterator = ThaDeviceModuleIteratorCreate(self);
    eAtRet ret;

    DeactivateAllModules(self);
    ret = AtModulesMemoryTest(moduleIterator);
    AtObjectDelete((AtObject)moduleIterator);

    return ret;
    }

static ThaVersionReader VersionReaderCreate(ThaDevice self)
    {
    return ThaVersionReaderNew((AtDevice)self);
    }

static ThaVersionReader DeviceVersionReader(ThaDevice self)
    {
    if (self->versionReader == NULL)
        self->versionReader = mMethodsGet(self)->VersionReaderCreate(self);
    return self->versionReader;
    }

static ThaVersionReader VersionReader(AtDevice self)
    {
    return mMethodsGet(mThis(self))->DeviceVersionReader(mThis(self));
    }

static uint32 VersionNumber(AtDevice self)
    {
    return ThaVersionReaderVersionNumber(VersionReader(self));
    }

static uint32 BuiltNumber(AtDevice self)
    {
    return ThaVersionReaderHardwareBuiltNumber(VersionReader(self));
    }

static uint32 VersionNumberFromHwGet(AtDevice self)
    {
    return ThaVersionReaderVersionNumberFromHwGet(VersionReader(self));
    }

static uint32 BuiltNumberFromHwGet(AtDevice self)
    {
    return ThaVersionReaderBuiltNumberFromHwGet(VersionReader(self));
    }

static char *VersionRead(AtDevice self, char *buffer, uint32 bufferLen)
    {
    return ThaVersionReaderVersionDescription(VersionReader(self), buffer, bufferLen);
    }

static eAtRet VersionNumberSet(AtDevice self, uint32 versionNumber)
    {
    return ThaVersionReaderVersionNumberSet(VersionReader(self), versionNumber);
    }

static eAtRet BuiltNumberSet(AtDevice self, uint32 builtNumber)
    {
    return ThaVersionReaderBuiltNumberSet(VersionReader(self), builtNumber);
    }

static uint32 ProductCodeRead(AtDevice self)
    {
    AtHal hal = AtDeviceIpCoreHalGet(self, 0);
    return AtHalRead(hal, 0x0);
    }
    
static void Debug(AtDevice self)
    {
    AtDebugger debugger = AtDeviceDebuggerGet(self);

    /* Super implement */
    m_AtDeviceMethods->Debug(self);

    if (debugger == NULL)
        {
        AtPrintc(cSevNormal, "\r\n");
        if (mThis(self)->resetIsInProgress)
            AtPrintc(cSevWarning, "Reset is in progress\r\n");
        }

    else
        {
        AtDebuggerEntryAdd(debugger, AtDebugEntryNew(mThis(self)->resetIsInProgress ?
                           cSevInfo : cSevNormal, "Reset is in progress",
                           mThis(self)->resetIsInProgress ? "yes" : "no"));
        }
    }

static AtLongRegisterAccess GlobalLongRegisterAccessCreate(AtDevice self)
    {
	AtUnused(self);
    return Tha32BitGlobalLongRegisterAccessNew();
    }

static uint8 MaxNumParts(ThaDevice self)
    {
	AtUnused(self);
    /* Device just has only part as default */
    return 1;
    }

static uint32 PartOffset(ThaDevice self, uint8 partId)
    {
    /* Note, there may be programming mistake and partId is input > 0 although
     * the device has only one part. It is better to have the following
     * condition before examining offset for input part */
	if (ThaDeviceMaxNumParts(self) < 2)
	    return 0;
	return (partId == 0) ? 0x0 : cBit24;
    }

static eBool IsEjected(AtDevice self)
    {
    if (AtDeviceIsSimulated(self))
        return cAtFalse;

    if (mThis(self)->resetIsInProgress)
        return cAtFalse;

    return m_AtDeviceMethods->IsEjected(self);
    }

static uint8 NumPartsOfModule(ThaDevice self, uint32 moduleId)
    {
	AtUnused(moduleId);
    return mMethodsGet(self)->MaxNumParts(self);
    }

static uint32 ModulePartOffset(ThaDevice self, eAtModule moduleId, uint8 partId)
    {
	AtUnused(moduleId);
    return mMethodsGet(self)->PartOffset(self, partId);
    }

static eBool NeedCheckSSKey(AtDevice self)
    {
    if (ThaDeviceIsEp((ThaDevice)self))
        return cAtFalse;

    return cAtTrue;
    }

static void DidDeleteAllManagedObjects(AtDevice self)
    {
    m_AtDeviceMethods->DidDeleteAllManagedObjects(self);
    AtObjectDelete((AtObject)mThis(self)->versionReader);
    }

static uint32 PllClkSysStatReg(ThaDevice self)
    {
	AtUnused(self);
    return cThaPllClkSysStatReg;
    }

static uint32 PllClkSysStatBitMask(ThaDevice self)
    {
	AtUnused(self);
    return cThaPllClkSysStatBitMask;
    }

static eBool CanCheckPll(ThaDevice self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static ThaIntrController IntrControllerCreate(ThaDevice self, AtIpCore core)
    {
	AtUnused(self);
    return ThaDefaultIntrControllerNew(core);
    }

static eBool CanHaveNewOcn(ThaDevice self)
    {
	AtUnused(self);
    /* If hardware use new OCN version that fix interrupt issue, this function
     * must return cAtTrue and the StartVersionWithNewOcn must be overridden correctly */
    return cAtFalse;
    }

static uint32 StartVersionWithNewOcn(ThaDevice self)
    {
	AtUnused(self);
    /* Need to override correctly if CanHaveNewOcn function return cAtTrue */
    return 0x0;
    }

static uint8 ModuleVersion(AtDevice self, eAtModule moduleId)
    {
    uint32 productVersion = AtDeviceVersionNumber(self);
    uint8 version = m_AtDeviceMethods->ModuleVersion(self, moduleId);

    if ((uint32)moduleId != cThaModuleOcn)
        return version;

    if (!mMethodsGet(mThis(self))->CanHaveNewOcn(mThis(self)))
        return version;

    if (productVersion < mMethodsGet(mThis(self))->StartVersionWithNewOcn(mThis(self)))
        return version;

    return (uint8)(version + 1);
    }

static eBool UseIsrInterrupt(ThaDevice self)
    {
	AtUnused(self);
    /* Just a few number of products use this mode. So it is better to let
     * concrete determine when to use in ISR */
    return cAtFalse;
    }

static eBool ShouldEnforceEpLogic(ThaDevice self)
    {
    /* NOTE: At customer site, this product is being used for practice purpose and
     * EP is used. Just because AtHalTcp is used, so that there is no way to
     * determine if EP related logic need to run by using product code which is
     * an official one. */
    AtUnused(self);
    return cAtFalse;
    }

static eBool IsEpCode(uint32 productCode)
    {
    static const uint8 cThalassaEpFamily = 8;
    uint8 platform = (uint8)((productCode & cBit31_28) >> 28);
    return (platform == cThalassaEpFamily) ? cAtTrue : cAtFalse;
    }

static eBool IsEp(ThaDevice self)
    {
    if (mMethodsGet(self)->ShouldEnforceEpLogic(self))
        return cAtTrue;
    return IsEpCode(AtDeviceProductCodeGet((AtDevice)self));
    }

static uint32 RealProductCode(uint32 productCode)
    {
    uint32 realProductCode;
    static const uint8 cThalassaFamily = 6;

    realProductCode = productCode;
    mFieldIns(&realProductCode, cBit31_28, 28, cThalassaFamily);

    return realProductCode;
    }

static const char *LongRegValue2String(uint32 *regValues, uint16 numDwords)
    {
    static char longRegValueString[150];
    static char dwordString[16];
    int32 i;

    if (numDwords == 0)
        return "None";

    if (numDwords > cThaLongRegMaxSize)
        numDwords = cThaLongRegMaxSize;

    AtSprintf(longRegValueString, "0x");
    for (i = (int16)(numDwords - 1); i >= 0; i--)
        {
        AtSprintf(dwordString, "%08x.", regValues[i]);
        AtStrcat(longRegValueString, dwordString);
        }

    /* Remove the last '.' */
    longRegValueString[AtStrlen(longRegValueString) - 1] = '\0';

    return longRegValueString;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaDevice object = (ThaDevice)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObject(bmtModule);
    mEncodeObject(cdrModule);
    mEncodeObject(cosModule);
    mEncodeObject(mapModule);
    mEncodeObject(demapModule);
    mEncodeObject(pdaModule);
    mEncodeObject(pweModule);
    mEncodeObject(claModule);
    mEncodeObject(mpigModule);
    mEncodeObject(mpegModule);
    mEncodeObject(pmcMpegModule);
    mEncodeObject(pmcMpigModule);
    mEncodeObject(ocnModule);
    mEncodeObject(pohModule);
    mEncodeObject(pmcModule);
    mEncodeObject(versionReader);
    mEncodeObject(resequenceManager);

    mEncodeUInt(oamPollingEnabled);
    mEncodeUInt(resetIsInProgress);
    mEncodeNone(resetElapseTimeMs);
    mEncodeNone(resetWaitTimeMs);
    mEncodeNone(resetTime);
    mEncodeNone(startResetTime);
    mEncodeNone(asyncInitState);
    mEncodeNone(asyncModulePosition);
    mEncodeNone(asyncAllModuleInitState);
    mEncodeNone(resetingCore);
    mEncodeNone(checkingIsRunning);
    mEncodeNone(startCheckingTime);
    mEncodeNone(goodTime);
    }

static uint32 StartVersionSupportSurveillance(ThaDevice self)
    {
	AtUnused(self);
	return cBit31_0;
    }

static eBool SurveillanceSupported(ThaDevice self)
    {
    uint32 version = AtDeviceVersionNumber((AtDevice)self);
    uint32 startSupportedVersion = mMethodsGet(self)->StartVersionSupportSurveillance(self) ;

    return (version >= startSupportedVersion) ? cAtTrue : cAtFalse;
    }

static eBool ShouldRetryReset(ThaDevice self)
    {
    /* Let concrete determine */
    AtUnused(self);
    return cAtFalse;
    }

static AtThermalSensor ThermalSensorObjectCreate(AtDevice self)
    {
    return ThaThermalSensorNew(self);
    }

static void RamResetWaitTimeMsSet(AtDevice self, uint32 timesInMs)
    {
    uint8 core_i;

    for (core_i = 0; core_i < AtDeviceNumIpCoresGet(self); core_i++)
        {
        AtIpCore core = AtDeviceIpCoreGet(self, core_i);
        ThaIpCoreRamSoftResetWaitTimeMsSet((ThaIpCore)core, timesInMs);
        }
    }

static uint32 ResetRetryTime(ThaDevice self)
    {
    /* Default so far */
    AtUnused(self);
    return 20;
    }

static eBool ShouldDelay(AtDevice self)
    {
    if (AtDeviceIsSimulated(self) ||
        AtDeviceWarmRestoreIsStarted(self))
        return cAtFalse;
    return cAtTrue;
    }

static eAtRet Reset(AtDevice self)
    {
    ThaDevice device = mThis(self);
    uint32 retry_i;
    eAtRet ret = cAtOk;
    tAtOsalCurTime startTime, curTime;
    uint32 resetWaitTimeMs = 1000;
    uint32 retryTimes;
    eBool shouldDelay;

    if (!mMethodsGet(device)->ShouldRetryReset(device))
        return m_AtDeviceMethods->Reset(self);

    device->resetElapseTimeMs = 0;
    device->resetWaitTimeMs  = 0;
    AtOsalCurTimeGet(&startTime);

    retryTimes = mMethodsGet(device)->ResetRetryTime(device);
    shouldDelay = ShouldDelay(self);
    for (retry_i = 0; retry_i < retryTimes; retry_i++)
        {
        /* When reset fail, just give hardware a moment the retry */
        RamResetWaitTimeMsSet(self, resetWaitTimeMs);
        ret = mMethodsGet(self)->HwReset(self);
        if (ret != cAtOk)
            {
            resetWaitTimeMs = resetWaitTimeMs + 1000;

            if (!shouldDelay)
                AtOsalUSleep(100000);
            continue;
            }

        /* For debugging purpose, need to profile */
        AtOsalCurTimeGet(&curTime);
        device->resetElapseTimeMs = mTimeIntervalInMsGet(startTime, curTime);
        device->resetWaitTimeMs = resetWaitTimeMs;

        break;
        }

    /* Back to default setting */
    RamResetWaitTimeMsSet(self, 0);

    return ret;
    }

static eAtRet AllCoresAsyncReset(AtDevice self)
    {
    ThaDevice device = mThis(self);
    eAtRet ret = cAtOk;
    AtIpCore core;

    if (device->resetingCore >= AtDeviceNumIpCoresGet(self))
        {
        device->resetingCore = 0;
        return cAtOk;
        }

    core = AtDeviceIpCoreGet(self, device->resetingCore);
    ret = AtIpCoreAsyncReset(core);
    if (ret == cAtOk)
        {
        device->resetingCore++;
        return cAtErrorAgain;
        }

    return ret;
    }

static eAtRet AsyncHwReset(AtDevice self)
    {
    return AllCoresAsyncReset(self);
    }

static void RamResetInit(AtDevice self)
    {
    mThis(self)->resetTime = 0;
    RamResetWaitTimeMsSet(self, 0);
    }

static eAtRet AsyncReset(AtDevice self)
    {
    ThaDevice device = mThis(self);
    eAtRet ret = cAtOk;
    tAtOsalCurTime curTime;
    uint32 resetWaitTimeMs = 1000;
    uint32 retryTimes;

    if (!mMethodsGet(device)->ShouldRetryReset(device))
        return mMethodsGet(self)->AsyncHwReset(self);

    if (device->resetTime == 0)
        {
        AtOsalCurTimeGet(&device->startResetTime);
        RamResetWaitTimeMsSet(self, resetWaitTimeMs);
        device->resetTime = device->resetTime + 1;
        }

    ret = mMethodsGet(self)->AsyncHwReset(self);
    /* HW reset has not finished yet */
    if (AtDeviceAsyncRetValIsInState(ret))
        return ret;

    /* HW reset finished OK */
    if (ret == cAtOk)
        {
        /* For debugging purpose, need to profile */
        AtOsalCurTimeGet(&curTime);
        device->resetElapseTimeMs = mTimeIntervalInMsGet(device->startResetTime, curTime);
        device->resetWaitTimeMs = resetWaitTimeMs;

        RamResetInit(self);
        return ret;
        }

    /* Try next time */
    device->resetTime = device->resetTime + 1;

    /* Reach max trying time but not successful, return error */
    retryTimes = mMethodsGet(device)->ResetRetryTime(device);
    if (device->resetTime == retryTimes)
        {
        RamResetInit(self);
        return ret;
        }

    RamResetWaitTimeMsSet(self, resetWaitTimeMs * device->resetTime);
    return cAtErrorAgain;
    }

static void ShowResetInfo(AtDevice self)
    {
    m_AtDeviceMethods->ShowResetInfo(self);
    AtPrintc(cSevNormal, "* Reset takes %d (ms)\r\n", mThis(self)->resetElapseTimeMs);
    AtPrintc(cSevNormal, "* RAM reset wait time %d (ms)\r\n", mThis(self)->resetWaitTimeMs);
    }

static uint32 ModuleActiveMask(ThaDevice self, uint32 moduleId)
    {
    /* Concrete should know, return zero so that it will not affect anything */
    AtUnused(self);
    AtUnused(moduleId);
    return 0;
    }

static uint32 ModuleActiveShift(ThaDevice self, uint32 moduleId)
    {
    /* Concrete should know, return zero so that it will not affect anything */
    AtUnused(self);
    AtUnused(moduleId);
    return 0;
    }

static void Delete(AtObject self)
    {
    ResequenceManagerDelete(mThis(self));
    m_AtObjectMethods->Delete(self);
    }

static AtResequenceManager ResequenceManagerObjectCreate(ThaDevice self)
    {
    return ThaResequenceManagerNew((AtDevice)self);
    }

static eBool ErrorGeneratorIsSupported(ThaDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool SerdesPowerControlIsSupported(ThaDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static AtIterator RestoredModulesIteratorCreate(AtDevice self)
    {
    return ThaDeviceModuleIteratorCreate(self);
    }

static eBool ShouldDeprovisionHwOnDeleting(ThaDevice self)
    {
    /* Basically, when device is deleted, the hardware should not be touched.
     * Let concrete products determine */
    AtUnused(self);
    return cAtFalse;
    }

static eBool ResequenceManagerIsSupported(ThaDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool ClockStateValueV2IsSupported(ThaDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet AsyncInit(AtDevice self)
    {
    return ThaDeviceAsyncInit(self);
    }

static eAtRet AllModulesAsyncInit(AtDevice self)
    {
    return ThaDeviceAllModulesAsyncInit(self);
    }

static void ClockDebug(ThaDevice self)
    {
    AtUnused(self);
    }

static void AsycnStickyCheckingInfoReset(ThaDevice self)
    {
    self->checkingIsRunning = cAtFalse;
    self->goodTime = 0;
    }

static AtIterator AllModulesIteratorCreate(AtDevice self)
    {
    return ThaDeviceModuleIteratorCreate(self);
    }

static uint32 DefaultCounterModule(ThaDevice self, uint32 moduleId)
    {
    AtUnused(self);
    return moduleId;
    }

static eBool ClockDebugIsSupported(ThaDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }
static eBool StickyDebugIsSupported(ThaDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool ShouldFlushRange(ThaDevice self, uint32 startAddress, uint32 stopAddress)
    {
    AtUnused(self);
    AtUnused(startAddress);
    AtUnused(stopAddress);
    return cAtTrue;
    }

static void MemoryFlush(ThaDevice self, uint32 start, uint32 stop, uint32 value)
    {
    uint32 address;

    for (address = start; address <= stop; address++)
        AtDeviceWriteOnCore((AtDevice)self, address, value, 0);
    }

static void MemoryLongFlush(ThaDevice self, uint32 start, uint32 stop)
    {
    uint32 longReg[8];
    uint32 address;

    AtOsalMemInit(longReg, 0, sizeof(longReg));
    for (address = start; address <= stop; address++)
        AtDeviceLongWriteOnCore((AtDevice)self, address, longReg, mCount(longReg), 0);
    }

static eAtRet AllModulesHwResourceCheck(AtDevice self)
    {
    AtIterator moduleIterator = ThaDeviceModuleIteratorCreate(self);
    AtModule module;
    eAtRet ret = cAtOk;

    while ((module = (AtModule)AtIteratorNext(moduleIterator)) != NULL)
        ret |= AtModuleHwResourceCheck(module);

    AtObjectDelete((AtObject)moduleIterator);
    return ret;
    }

static AtQuerier QuerierGet(AtDevice self)
    {
    AtUnused(self);
    return ThaDeviceSharedQuerier();
    }

static eBool ShouldSwitchToPmc(ThaDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet HwFlushAtt(ThaDevice self)
    {
    AtUnused(self);
    return cAtOk;
    }

static void OverrideAtObject(ThaDevice self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtDevice(ThaDevice self)
    {
    AtDevice device = (AtDevice)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(device);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(tAtDeviceMethods));
        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, AllModulesCreate);
        mMethodOverride(m_AtDeviceOverride, AllModulesDelete);
        mMethodOverride(m_AtDeviceOverride, Init);
        mMethodOverride(m_AtDeviceOverride, ModuleGet);
        mMethodOverride(m_AtDeviceOverride, AllModulesInit);
        mMethodOverride(m_AtDeviceOverride, InterruptProcess);
        mMethodOverride(m_AtDeviceOverride, InterruptRestore);
        mMethodOverride(m_AtDeviceOverride, IpCoreCreate);
        mMethodOverride(m_AtDeviceOverride, LongReadOnCore);
        mMethodOverride(m_AtDeviceOverride, LongWriteOnCore);
        mMethodOverride(m_AtDeviceOverride, PeriodicProcess);
        mMethodOverride(m_AtDeviceOverride, MemoryTest);
        mMethodOverride(m_AtDeviceOverride, VersionRead);
        mMethodOverride(m_AtDeviceOverride, VersionNumber);
        mMethodOverride(m_AtDeviceOverride, BuiltNumber);
        mMethodOverride(m_AtDeviceOverride, Debug);
        mMethodOverride(m_AtDeviceOverride, ProductCodeRead);
        mMethodOverride(m_AtDeviceOverride, IsEjected);
        mMethodOverride(m_AtDeviceOverride, GlobalLongRegisterAccessCreate);
        mMethodOverride(m_AtDeviceOverride, NeedCheckSSKey);
        mMethodOverride(m_AtDeviceOverride, ModuleVersion);
        mMethodOverride(m_AtDeviceOverride, DidDeleteAllManagedObjects);
        mMethodOverride(m_AtDeviceOverride, RestoredModulesIteratorCreate);
        mMethodOverride(m_AtDeviceOverride, ShowResetInfo);
        mMethodOverride(m_AtDeviceOverride, Reset);
        mMethodOverride(m_AtDeviceOverride, ThermalSensorObjectCreate);
        mMethodOverride(m_AtDeviceOverride, SimulationCode);
        mMethodOverride(m_AtDeviceOverride, VersionNumberSet);
        mMethodOverride(m_AtDeviceOverride, BuiltNumberSet);
        mMethodOverride(m_AtDeviceOverride, VersionNumberFromHwGet);
        mMethodOverride(m_AtDeviceOverride, BuiltNumberFromHwGet);
        mMethodOverride(m_AtDeviceOverride, AsyncInit);
        mMethodOverride(m_AtDeviceOverride, AllModulesAsyncInit);
        mMethodOverride(m_AtDeviceOverride, AsyncReset);
        mMethodOverride(m_AtDeviceOverride, AsyncHwReset);
        mMethodOverride(m_AtDeviceOverride, AllModulesIteratorCreate);
        mMethodOverride(m_AtDeviceOverride, AllModulesHwResourceCheck);
        mMethodOverride(m_AtDeviceOverride, QuerierGet);
        }

    mMethodsSet(device, &m_AtDeviceOverride);
    }

static void Override(ThaDevice self)
    {
    OverrideAtObject(self);
    OverrideAtDevice(self);
    }

static void MethodsInit(ThaDevice self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, MaxNumParts);
        mMethodOverride(m_methods, PartOffset);
        mMethodOverride(m_methods, NumPartsOfModule);
        mMethodOverride(m_methods, ModulePartOffset);
        mMethodOverride(m_methods, PllClkSysStatReg);
        mMethodOverride(m_methods, PllClkSysStatBitMask);
        mMethodOverride(m_methods, CanCheckPll);
        mMethodOverride(m_methods, IntrControllerCreate);
        mMethodOverride(m_methods, CanHaveNewOcn);
        mMethodOverride(m_methods, StartVersionWithNewOcn);
        mMethodOverride(m_methods, VersionReaderCreate);
        mMethodOverride(m_methods, UseIsrInterrupt);
        mMethodOverride(m_methods, ShouldEnforceEpLogic);
        mMethodOverride(m_methods, StartVersionSupportSurveillance);
        mMethodOverride(m_methods, ShouldRetryReset);
        mMethodOverride(m_methods, ModuleActiveMask);
        mMethodOverride(m_methods, ModuleActiveShift);
        mMethodOverride(m_methods, ResequenceManagerObjectCreate);
        mMethodOverride(m_methods, ResequenceManagerIsSupported);
        mMethodOverride(m_methods, ErrorGeneratorIsSupported);
        mMethodOverride(m_methods, SerdesPowerControlIsSupported);
        mMethodOverride(m_methods, ShouldDeprovisionHwOnDeleting);
        mMethodOverride(m_methods, ResetRetryTime);
        mMethodOverride(m_methods, ClockStateValueV2IsSupported);
        mMethodOverride(m_methods, ClockDebug);
        mMethodOverride(m_methods, DefaultCounterModule);
        mMethodOverride(m_methods, ShouldFlushRange);
        mMethodOverride(m_methods, ShouldSwitchToPmc);
        mMethodOverride(m_methods, HwFlushAtt);
        mMethodOverride(m_methods, ClockDebugIsSupported);
        mMethodOverride(m_methods, StickyDebugIsSupported);
        mMethodOverride(m_methods, DeviceVersionReader);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaDevice);
    }

AtDevice ThaDeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Initialize memory */
    ThaDevice device = (ThaDevice)self;
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, device, 0, ObjectSize());

    /* Call super constructor */
    if (AtDeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(device);
    MethodsInit(device);
    m_methodsInit = 1;

    return (AtDevice)device;
    }

eBool ThaProductCodeIsValid(AtDriver driver, uint32 productCode)
    {
	AtUnused(driver);
    if ((productCode == 0x60070013) ||
        (productCode == 0x60070023) ||
        (productCode == 0x60071011) ||
        (productCode == 0x60071021) ||
        (productCode == 0x60030022) ||
        (productCode == 0x60030051) ||
        (productCode == 0x60030023) ||
        (productCode == 0x600a0011) ||
        (productCode == 0x60030011) ||
        (productCode == 0x60031031) ||
        (productCode == 0x60031021) ||
        (productCode == 0x60031033) ||
        (productCode == 0x60060011) ||
        (productCode == 0x60031035) ||
        (productCode == 0x60080011) ||
        (productCode == 0x60035011) ||
        (productCode == 0x60035021) ||
        (productCode == 0x61031031) ||
        (productCode == 0x60031032) ||
        (productCode == 0x60031023) ||
        (productCode == 0x60030080) ||
        (productCode == 0x60030081) ||
        (productCode == 0x60091023) ||
        (productCode == 0x60071032) ||
        (productCode == 0x60030111) ||
        (productCode == 0x65031032) ||
        (productCode == 0x60091132) ||
        (productCode == 0x60070041) ||
        (productCode == 0x60031131) ||
        (productCode == 0x60000031) ||
        (productCode == 0x60091135) ||
        (productCode == 0x60070061) ||
        (productCode == 0x60150011) ||
        (productCode == 0x60001031) ||
        (productCode == 0x60031071) ||
        (productCode == 0x60200011) ||
        (productCode == 0x61150011) ||
        (productCode == 0x60210021) ||
        (productCode == 0x60030101) ||
        (productCode == 0x60210031) ||
        (productCode == 0x61150011) ||
        (productCode == 0x60220031) ||
        (productCode == 0x60220041) ||
        (productCode == 0x60210011) ||
        (productCode == 0x61210011) ||
        (productCode == 0x60050061) ||
        (productCode == 0x60070051) ||
        (productCode == 0x60020011) ||
        (productCode == 0x60240021) ||
        (productCode == 0x60210012) ||
        (productCode == 0x60210051) ||
        (productCode == 0x6A000010) ||
        (productCode == 0x68050011) ||
        (productCode == 0x60210061) ||
        (productCode == 0x6A210021) ||
        (productCode == 0x60290011) ||
        (productCode == 0x6A290011) ||
        (productCode == 0x60290021) ||
		(productCode == 0x6A290021) ||
        (productCode == 0x6A290E21) ||
        (productCode == 0x60290022) ||
        (productCode == 0x6A033111) ||
        (productCode == 0x61290011) ||
        (productCode == 0x62290011) ||
        (productCode == 0x60290061) ||
        (productCode == 0x60291011) ||
        (productCode == 0x60291022) ||
        (productCode == 0x60290051) ||
        (productCode == 0x60290081) ||
        (productCode == 0x6A290081) ||
        (productCode == 0x61290051))
        return cAtTrue;

    return cAtFalse;
    }

AtDevice ThaDeviceCreate(AtDriver driver, uint32 productCode)
    {
    uint32 realProductCode = RealProductCode(productCode);

    /* For products that do not have corresponding EP products */
    switch (realProductCode)
        {
        mDeviceCreate(driver, productCode, realProductCode, 60070013);
        mDeviceCreate(driver, productCode, realProductCode, 60070023);
        mDeviceCreate(driver, productCode, realProductCode, 60071011);
        mDeviceCreate(driver, productCode, realProductCode, 60030022);
        mDeviceCreate(driver, productCode, realProductCode, 60030051);
        mDeviceCreate(driver, productCode, realProductCode, 60030023);
        mDeviceCreate(driver, productCode, realProductCode, 600a0011);
        mDeviceCreate(driver, productCode, realProductCode, 60030011);
        mDeviceCreate(driver, productCode, realProductCode, 60071021);
        mDeviceCreate(driver, productCode, realProductCode, 60031032);
        mDeviceCreate(driver, productCode, realProductCode, 60060011);
        mDeviceCreate(driver, productCode, realProductCode, 60035011);
        mDeviceCreate(driver, productCode, realProductCode, 60035021);
        mDeviceCreate(driver, productCode, realProductCode, 60091023);
        mDeviceCreate(driver, productCode, realProductCode, 61031031);
        mDeviceCreate(driver, productCode, realProductCode, 60030080);
        mDeviceCreate(driver, productCode, realProductCode, 60030081);
        mDeviceCreate(driver, productCode, realProductCode, 60071032);
        mDeviceCreate(driver, productCode, realProductCode, 60030111);
        mDeviceCreate(driver, productCode, realProductCode, 65031032);
        mDeviceCreate(driver, productCode, realProductCode, 60031033);
        mDeviceCreate(driver, productCode, realProductCode, 60031035);
        mDeviceCreate(driver, productCode, realProductCode, 60091132);
        mDeviceCreate(driver, productCode, realProductCode, 60070041);
        mDeviceCreate(driver, productCode, realProductCode, 60031131);
        mDeviceCreate(driver, productCode, realProductCode, 60150011);
        mDeviceCreate(driver, productCode, realProductCode, 60000031);
        mDeviceCreate(driver, productCode, realProductCode, 60031031);
        mDeviceCreate(driver, productCode, realProductCode, 60031021);
        mDeviceCreate(driver, productCode, realProductCode, 60091135);
        mDeviceCreate(driver, productCode, realProductCode, 60070061);
        mDeviceCreate(driver, productCode, realProductCode, 60001031);
        mDeviceCreate(driver, productCode, realProductCode, 60031071);
        mDeviceCreate(driver, productCode, realProductCode, 60200011);
        mDeviceCreate(driver, productCode, realProductCode, 60030101);
        mDeviceCreate(driver, productCode, realProductCode, 60210031);
        mDeviceCreate(driver, productCode, realProductCode, 61210031);
        mDeviceCreate(driver, productCode, realProductCode, 6A210031);
        mDeviceCreate(driver, productCode, realProductCode, 61150011);
        mDeviceCreate(driver, productCode, realProductCode, 60210021);
        mDeviceCreate(driver, productCode, realProductCode, 60220031);
        mDeviceCreate(driver, productCode, realProductCode, 60220041);
        mDeviceCreate(driver, productCode, realProductCode, 60210011);
        mDeviceCreate(driver, productCode, realProductCode, 61210011);
        mDeviceCreate(driver, productCode, realProductCode, 60050061);
        mDeviceCreate(driver, productCode, realProductCode, 60070051);
        mDeviceCreate(driver, productCode, realProductCode, 60020011);
        mDeviceCreate(driver, productCode, realProductCode, 60240021);
        mDeviceCreate(driver, productCode, realProductCode, 6A000010);
        mDeviceCreate(driver, productCode, realProductCode, 60210051);
        mDeviceCreate(driver, productCode, realProductCode, 6A210021);
        mDeviceCreate(driver, productCode, realProductCode, 60210012);
        mDeviceCreate(driver, productCode, realProductCode, 60210061);
        mDeviceCreate(driver, productCode, realProductCode, 60290011);
        mDeviceCreate(driver, productCode, realProductCode, 6A290011);
        mDeviceCreate(driver, productCode, realProductCode, 60290021);
        mDeviceCreate(driver, productCode, realProductCode, 6A033111);
        mDeviceCreate(driver, productCode, realProductCode, 60290022);
        mDeviceCreate(driver, productCode, realProductCode, 61290011);
        mDeviceCreate(driver, productCode, realProductCode, 62290011);
        mDeviceCreate(driver, productCode, realProductCode, 60290061);
        mDeviceCreate(driver, productCode, realProductCode, 60291011);
        mDeviceCreate(driver, productCode, realProductCode, 60291022);
        mDeviceCreate(driver, productCode, realProductCode, 60290051);
        mDeviceCreate(driver, productCode, realProductCode, 61290051);
        mDeviceCreate(driver, productCode, realProductCode, 60290081);

        /* Test only */
        case 0x60031023:
        case 0x60071023:
            return Tha60091023DeviceNew(driver, productCode);
        case 0x60080011:
            return Tha60060011DeviceNew(driver, productCode);

        case 0x68050011:
            return Tha6A000010DeviceNew(driver, productCode);

        case 0x6A290021:
            return Tha6A290021DeviceNew(driver, productCode);

        case 0x6A290E21:
            return Tha6A290E21DeviceNew(driver, productCode);

        case 0x6A290022:
            return Tha6A290022DeviceNew(driver, productCode);

        case 0x6A291022:
            return Tha6A291022DeviceNew(driver, productCode);

        case 0x6A290081:
            return Tha6A290081DeviceNew(driver, productCode);

        default:
            break;
        }

    return NULL;
    }

eBool ThaDeviceIsEp(ThaDevice self)
    {
    if (self)
        return IsEp(self);
    return cAtFalse;
    }

uint8 ThaDeviceMaxNumParts(ThaDevice self)
    {
    if (self)
        return mMethodsGet(self)->MaxNumParts(self);

    return 0;
    }

uint32 ThaDevicePartOffset(ThaDevice self, uint8 partId)
    {
    if (self)
        return mMethodsGet(self)->PartOffset(self, partId);

    return 0;
    }

uint8 ThaDeviceNumPartsOfModule(ThaDevice self, eAtModule moduleId)
    {
    if (self)
        return mMethodsGet(self)->NumPartsOfModule(self, (uint32)moduleId);

    return cBit7_0;
    }

uint32 ThaDeviceModulePartOffset(ThaDevice self, eAtModule moduleId, uint8 partId)
    {
    if (self)
        return mMethodsGet(self)->ModulePartOffset(self, moduleId, partId);

    return 0;
    }

uint32 ThaDevicePllClkSysStatReg(ThaDevice self)
    {
    if (self)
        return mMethodsGet(self)->PllClkSysStatReg(self);
    return cInvalidValue;
    }

uint32 ThaDevicePllClkSysStatBitMask(ThaDevice self)
    {
    if (self)
        return mMethodsGet(self)->PllClkSysStatBitMask(self);
    return cInvalidValue;
    }

eBool ThaDeviceCanCheckPll(ThaDevice self)
    {
    if (self)
        return mMethodsGet(self)->CanCheckPll(self);
    return cAtFalse;
    }

ThaIntrController ThaDeviceIntrControllerCreate(ThaDevice self, AtIpCore core)
    {
    if (self)
        return mMethodsGet(self)->IntrControllerCreate(self, core);
    return NULL;
    }

uint8 ThaDeviceChipVersionMajorGet(ThaDevice self)
    {
    uint32 versionNumber = AtDeviceVersionNumber((AtDevice)self);
    return (uint8)mRegField(versionNumber, cThaRegChipVersionMajor);
    }

uint32 ThaDeviceExpectedProductCode(ThaDevice self)
    {
    uint32 productCode = AtDeviceProductCodeGet((AtDevice)self);

    if (ThaDeviceIsEp(self))
        return RealProductCode(productCode);

    return productCode;
    }

void ThaDeviceRegNameDisplay(const char* regName)
    {
    AtPrintc(cSevInfo, "   - %s:\r\n", regName);
    }

void ThaDeviceChannelRegValueDisplay(AtChannel channel, uint32 address, eAtModule moduleId)
    {
    AtPrintc(cSevNormal, "        0x%08x: 0x%08x\r\n", address, mChannelHwRead(channel, address, moduleId));
    }

void ThaDeviceChannelRegLongValueDisplay(AtChannel channel, uint32 address, eAtModule moduleId)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint16 numDwords = mChannelHwLongRead(channel, address, longRegVal, cThaLongRegMaxSize, moduleId);
    AtPrintc(cSevNormal, "        0x%08x: %s\r\n", address, LongRegValue2String(longRegVal, numDwords));
    }

void ThaDeviceModuleRegValueDisplay(AtModule module, uint32 address)
    {
    AtPrintc(cSevNormal, "        0x%08x: 0x%08x\r\n", address, mModuleHwRead(module, address));
    }

void ThaDeviceModuleRegLongValueDisplay(AtModule module, uint32 address, uint8 coreId)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    AtIpCore core = AtDeviceIpCoreGet(AtModuleDeviceGet(module), coreId);
    uint16 numDwords = mModuleHwLongRead(module, address, longRegVal, cThaLongRegMaxSize, core);

    AtPrintc(cSevNormal, "        0x%08x: %s\r\n", address, LongRegValue2String(longRegVal, numDwords));
    }

void ThaDeviceRegLongValueDisplayOnly(const char* regName, uint32 address, uint32 *longRegVal, uint32 numOfDword)
    {
    AtPrintc(cSevNormal, "%s, address@0x%08x: %s\r\n", regName, address, LongRegValue2String(longRegVal, (uint16)numOfDword));
    }

static char *Buffer(AtDebugger debugger, uint32 *bufferSize)
    {
    if (debugger)
        return AtDebuggerCharBuffer(debugger, bufferSize);
    return AtSharedDriverSharedBuffer(bufferSize);
    }

void ThaDeviceClockResultPrint(AtDevice self, AtDebugger debugger, const char *clockString, uint32 regAddr, uint32 expectedClockInHz, uint32 ppmSwingRange)
    {
    AtHal hal = AtDeviceIpCoreHalGet(self, 0);
    uint32 clockInHz = AtHalRead(hal, regAddr);
    char clockInKhzStr[32] = {0};
    char expectedClockInKhzStr[32] = {0};
    char clockInPpmStr[32] = {0};
    int32 clockDiffInHz = (int)(clockInHz - expectedClockInHz);
    double expectedClockInMhz = (double)expectedClockInHz / 1000000;
    double clockDiffInPpm = ((double)clockDiffInHz / expectedClockInMhz);
    eBool overRange = cAtTrue;
    uint32 bufferSize;
    eAtSevLevel color;
    char *buffer = Buffer(debugger, &bufferSize);

    if (((int32)clockDiffInPpm <= (int32)ppmSwingRange) &&
        ((int32)clockDiffInPpm >= (int32)(-ppmSwingRange)))
        overRange = cAtFalse;

    color = overRange ? cSevCritical : cSevInfo;

    AtSprintf(clockInKhzStr, "%7.3f", (double)clockInHz / 1000);
    AtSprintf(expectedClockInKhzStr, "%7.3f", (double)expectedClockInHz / 1000);
    AtSprintf(clockInPpmStr, "%7.3f", clockDiffInPpm);

    AtSnprintf(buffer, bufferSize,
               "%11s KHz | Expected: %11s KHz | PPM: %s -> %s\r\n",
               clockInKhzStr,
               expectedClockInKhzStr,
               clockInPpmStr,
               overRange ? "OUT OF RANGE" : "OK");
    if (debugger)
        {
        AtDebuggerEntryAdd(debugger, AtDebugEntryNew(color, clockString, buffer));
        return;
        }
    AtPrintc(cSevNormal, "%-25s: ", clockString);
    AtPrintc(color, "%s", buffer);
    }

eBool ThaDeviceStickyIsGood(AtDevice self, uint32 regAddr, uint32 bitMask, uint32 timeoutMs)
    {
    uint32 regVal;
    AtOsal osal = AtSharedDriverOsalGet();
    tAtOsalCurTime curTime, startTime;
    uint32 elapse, goodCount;
    AtIpCore core = AtDeviceIpCoreGet(self, 0);
    static const uint32 cStickyWaitTimeToRecheckUs = 1000; /* 1ms */
    static const uint8  cStickyCheckMaxCount       = 10;
    eBool success = cAtFalse;
    eBool isTestbench = AtDeviceTestbenchIsEnabled(self);
    eBool isSimulated = AtDeviceIsSimulated(self);

    if (AtDeviceWarmRestoreIsStarted(self))
        return cAtTrue;

    /* Clear sticky */
    AtIpCoreWrite(core, regAddr, bitMask);

    elapse    = 0;
    goodCount = 0;
    mMethodsGet(osal)->CurTimeGet(osal, &startTime);
    while (elapse < timeoutMs)
        {
        regVal = AtIpCoreRead(core, regAddr);

        /* Good, increase good counter
         * Consider good if simulate, do not just return here, it will help to
         * verify asynchronous sequence */
        if (AtDeviceIsSimulated(self) || ((regVal & bitMask) == 0))
            goodCount++;

        /* Unstable, reset lock counter */
        else
            {
            AtIpCoreWrite(core, regAddr, bitMask);
            goodCount = 0;
            }

        /* It is stable, exit */
        if (goodCount == cStickyCheckMaxCount)
            {
            success = cAtTrue;
            break;
            }

        /* Testbench environment is slow, cannot expect many good times */
        if (isTestbench && (goodCount > 0))
            {
            success = cAtTrue;
            break;
            }

        /* Calculate elapse time and retry */
        mMethodsGet(osal)->CurTimeGet(osal, &curTime);
        elapse = mTimeIntervalInMsGet(startTime, curTime);

        /* Sleep 1 ms before re-check */
        if (!isSimulated)
            AtOsalUSleep(cStickyWaitTimeToRecheckUs);
        }

    if (success)
        {
        AtDeviceLog(self, cAtLogLevelInfo, AtSourceLocation, "%s, Checking sticky mask 0x%08X pass, elapse time: %ums\r\n",
                        AtFunction, bitMask, elapse);
        }
    else
        {
        AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "%s, checking sticky mask 0x%08X fail, pass %u/%u\r\n",
                    AtFunction, bitMask, goodCount, cStickyCheckMaxCount);
        }

    return success;
    }

/* Return cAtOk if sticky is good, cAtErrorAgain if need to continue checking, otherwise error happened */
eAtRet ThaDeviceStickyAsyncCheck(AtDevice self, uint32 regAddr, uint32 bitMask, uint32 timeoutMs)
    {
    uint32 regVal, elapse;
    AtOsal osal = AtSharedDriverOsalGet();
    tAtOsalCurTime curTime;
    AtIpCore core = AtDeviceIpCoreGet(self, 0);
    static const uint32 cStickyWaitTimeToRecheckUs = 1000; /* 1ms */
    static const uint8  cStickyCheckMaxCount       = 10;

    if (!mThis(self)->checkingIsRunning)
        {
        mMethodsGet(osal)->CurTimeGet(osal, &mThis(self)->startCheckingTime);
        mThis(self)->checkingIsRunning = cAtTrue;

        /* Always write to clear at first time checking */
    AtIpCoreWrite(core, regAddr, bitMask);
        }

    /* Read to check */
    regVal = AtIpCoreRead(core, regAddr);

    mMethodsGet(osal)->CurTimeGet(osal, &curTime);
    elapse = mTimeIntervalInMsGet(mThis(self)->startCheckingTime, curTime);

    /* Good, increase good counter */
    if (AtDeviceIsSimulated(self) || ((regVal & bitMask) == 0))
        {
        mThis(self)->goodTime++;
        if (mThis(self)->goodTime == cStickyCheckMaxCount)
            {
            AtDeviceLog(self, cAtLogLevelInfo, AtSourceLocation, "%s, Checking sticky mask 0x%08X pass, elapse time: %ums\r\n",
                        AtFunction, bitMask, elapse);

            AsycnStickyCheckingInfoReset(mThis(self));
            return cAtOk;
            }
        }

    /* Unstable, reset lock counter */
    else
        {
        AtIpCoreWrite(core, regAddr, bitMask);
        mThis(self)->goodTime = 0;
        }

    /* Retry if not timeout */
    if (elapse < timeoutMs)
        {
        /* Sleep 1 ms before next time */
        AtOsalUSleep(cStickyWaitTimeToRecheckUs);
        return cAtErrorAgain;
        }

    AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "%s, checking sticky mask 0x%08X fail, pass %u/%u\r\n",
                AtFunction, bitMask, mThis(self)->goodTime, cStickyCheckMaxCount);
    AsycnStickyCheckingInfoReset(mThis(self));

    return cAtError;
    }

eBool ThaDeviceSurveillanceSupported(ThaDevice self)
    {
    if (self == NULL)
        return cAtFalse;

    /* In EP, need to check if SUR module version is valid */
    if (ThaDeviceIsEp(self))
        {
        ThaModuleHardSur surModule = (ThaModuleHardSur)AtDeviceModuleGet((AtDevice)self, cAtModuleSur);
        if (ThaModuleHardSurVersionIsValid(surModule))
            return cAtTrue;
        }

    /* This is also supported in testbench */
    if (AtDeviceTestbenchIsEnabled((AtDevice)self))
        return cAtTrue;

    /* Otherwise, fully base on device version number */
    return SurveillanceSupported(self);
    }

ThaVersionReader ThaDeviceVersionReader(AtDevice self)
    {
    if (self)
        return VersionReader(self);
    return NULL;
    }

uint32 ThaDeviceModuleActiveMask(ThaDevice self, uint32 moduleId)
    {
    if (self)
        return mMethodsGet(self)->ModuleActiveMask(self, moduleId);
    return 0;
    }

uint32 ThaDeviceModuleActiveShift(ThaDevice self, uint32 moduleId)
    {
    if (self)
        return mMethodsGet(self)->ModuleActiveShift(self, moduleId);
    return 0;
    }

void ThaDeviceMemoryLongFlush(ThaDevice self, uint32 start, uint32 stop)
    {
    if (mMethodsGet(self)->ShouldFlushRange(self, start, stop))
        MemoryLongFlush(self, start, stop);
    }

void ThaDeviceMemoryFlush(ThaDevice self, uint32 start, uint32 stop, uint32 value)
    {
    if (mMethodsGet(self)->ShouldFlushRange(self, start, stop))
        MemoryFlush(self, start, stop, value);
    }

AtResequenceManager ThaDeviceResequenceManagerGet(ThaDevice self)
    {
    if (self)
        return self->resequenceManager;
    return NULL;
    }

eBool ThaDeviceErrorGeneratorIsSupported(ThaDevice self)
    {
    mAttributeGet(ErrorGeneratorIsSupported, eBool, cAtFalse);
    }

eBool ThaDeviceSerdesPowerControlIsSupported(ThaDevice self)
    {
    mAttributeGet(SerdesPowerControlIsSupported, eBool, cAtFalse);
    }

eBool ThaDeviceShouldDeprovisionHwOnDeleting(ThaDevice self)
    {
    if (self)
        return mMethodsGet(self)->ShouldDeprovisionHwOnDeleting(self);
    return cAtFalse;
    }

AtResequenceManager ThaDeviceResequenceManagerCreate(ThaDevice self)
    {
    if (self->resequenceManager != NULL)
        return self->resequenceManager;

    self->resequenceManager = mMethodsGet(self)->ResequenceManagerObjectCreate(self);
    if (AtResequenceManagerInit(self->resequenceManager) == cAtOk)
        return self->resequenceManager;

    AtObjectDelete((AtObject)self->resequenceManager);
    self->resequenceManager = NULL;
    return NULL;
    }

eBool ThaDeviceNeedProfileInterrupt(ThaDevice self)
    {
    /* Open this when need to profile interrupt */
    AtUnused(self);
    return cAtFalse;
    }

eAtRet ThaDeviceSupperAsyncInit(AtDevice self)
    {
    return m_AtDeviceMethods->AsyncInit(self);
    }

void ThaDeviceEntranceToInit(AtDevice self)
    {
    EntranceToInit(self);
    }

void ThaDevicePostInit(AtDevice self)
    {
    PostInit(self);
    }

eAtRet ThaDeviceSuperAllModulesAsyncInit(AtDevice self)
    {
    return m_AtDeviceMethods->AllModulesAsyncInit(self);
    }

eBool ThaDeviceHwClockStateShouldUseNewTransition(ThaDevice self)
    {
    if (self)
        return mMethodsGet(self)->ClockStateValueV2IsSupported(self);

    return cAtFalse;
    }

void ThaDeviceClockDebug(ThaDevice self)
    {
    if (self)
        mMethodsGet(self)->ClockDebug(self);
    }

uint32 ThaDeviceDefaultCounterModule(ThaDevice self, uint32 moduleId)
    {
    if (self)
        return mMethodsGet(self)->DefaultCounterModule(self, moduleId);
    return cInvalidUint32;
    }

eBool ThaDeviceSupportFeatureFromVersion(AtDevice self, uint32 major, uint32 minor, uint32 betaBuild)
    {
    uint32 startVerion, currentVersion;

    if (AtDeviceAllFeaturesAvailableInSimulation(self))
        return cAtTrue;

    startVerion = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(major, minor, betaBuild);
    currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(ThaDeviceVersionReader(self));
    return (currentVersion >= startVerion) ? cAtTrue : cAtFalse;
    }

const char *ThaDeviceUtilLongRegValue2String(uint32 *regValues, uint16 numDwords)
    {
    return LongRegValue2String(regValues, numDwords);
    }

eBool ThaDeviceClockDebugIsSupported(ThaDevice self)
    {
    if (self)
        return mMethodsGet(self)->ClockDebugIsSupported(self);
    return cAtFalse;
    }
eBool ThaDeviceStickyDebugIsSupported(ThaDevice self)
    {
    if (self)
        return mMethodsGet(self)->StickyDebugIsSupported(self);
    return cAtFalse;
    }

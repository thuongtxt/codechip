/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : ThaSSKeyCheckerPwV2.c
 *
 * Created Date: Sep 20, 2013
 *
 * Description : SSKey checker used for PW products (with FPGA version 2)
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaSSKeyCheckerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSSKeyCheckerMethods m_AtSSKeyCheckerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaSSKeyCheckerPwV2);
    }

static uint32 StickyAddress(AtSSKeyChecker self)
    {
	AtUnused(self);
    return 0x340006;
    }

static uint32 CodeMsbAddress(AtSSKeyChecker self)
    {
	AtUnused(self);
    return 0x340001;
    }

static uint32 CodeLsbAddress(AtSSKeyChecker self)
    {
	AtUnused(self);
    return 0x340002;
    }

static void OverrideAtSSKeyChecker(AtSSKeyChecker self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSSKeyCheckerOverride, mMethodsGet(self), sizeof(m_AtSSKeyCheckerOverride));

        mMethodOverride(m_AtSSKeyCheckerOverride, StickyAddress);
        mMethodOverride(m_AtSSKeyCheckerOverride, CodeMsbAddress);
        mMethodOverride(m_AtSSKeyCheckerOverride, CodeLsbAddress);
        }

    mMethodsSet(self, &m_AtSSKeyCheckerOverride);
    }

static void Override(AtSSKeyChecker self)
    {
    OverrideAtSSKeyChecker(self);
    }

AtSSKeyChecker ThaSSKeyCheckerPwV2ObjectInit(AtSSKeyChecker self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtSSKeyCheckerObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

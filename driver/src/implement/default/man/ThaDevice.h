/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Management
 * 
 * File        : ThaDevice.h
 * 
 * Created Date: Apr 23, 2013
 *
 * Description : Thalassa device
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THADEVICE_H_
#define _THADEVICE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtDevice.h" /* Super class */
#include "AtIpCore.h"
#include "../../../generic/util/AtLongRegisterAccess.h"
#include "intrcontroller/ThaIntrController.h"
#include "versionreader/ThaVersionReader.h"

/* All modules */
#include "../eth/ThaModuleEth.h"
#include "../pdh/ThaModulePdh.h"
#include "../encap/ThaModuleEncap.h"
#include "../ppp/ThaModulePpp.h"
#include "../sdh/ThaModuleSdh.h"
#include "../cla/ThaModuleCla.h"
#include "../pktanalyzer/ThaModulePktAnalyzer.h"
#include "../pw/ThaModulePw.h"
#include "../ram/ThaModuleRam.h"
#include "../clock/ThaModuleClock.h"
#include "../pdh/ThaModulePdh.h"
#include "../concate/ThaModuleConcate.h"
#include "../prbs/ThaModulePrbs.h"
#include "../xc/ThaModuleXc.h"
#include "../encap/resequence/ThaResequenceManager.h"
#include "../sur/hard/ThaModuleHardSur.h"
#include "../ber/ThaModuleBer.h"
#include "../ptp/ThaModulePtp.h"

#include "ThaSSKeyChecker.h"
#include "ThaIpCore.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define cThaModulePwPmc    cThaModulePmc
#define cThaLongRegMaxSize 8

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaDevice * ThaDevice;

typedef enum eThaPhyModule
    {
    cThaModuleStart = cAtModuleInvalid,
    cThaModuleMap   = cThaModuleStart,
    cThaModuleDemap,
    cThaModuleCdr,
    cThaModuleCla,
    cThaModulePwe,
    cThaModulePda,
    cThaModuleCos,
    cThaModuleBmt,
    cThaModuleMpig,
    cThaModuleMpeg,
    cThaModulePmcMpeg,
    cThaModulePmcMpig,
	cThaModulePmc,
    cThaModuleOcn,
    cThaModulePoh,
    cThaModuleEnd
    }eThaPhyModule;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eBool ThaProductCodeIsValid(AtDriver driver, uint32 productCode);
AtDevice ThaDeviceCreate(AtDriver driver, uint32 productCode);
eBool ThaDeviceShouldDeprovisionHwOnDeleting(ThaDevice self);
void ThaDeviceClockDebug(ThaDevice self);
uint32 ThaDeviceDefaultCounterModule(ThaDevice self, uint32 moduleId);

/* Iterator to iterate all of modules (logical and physical modules) */
AtIterator ThaDeviceModuleIteratorCreate(AtDevice self);

/* Version handling */
ThaVersionReader ThaDeviceVersionReader(AtDevice self);
eBool ThaDeviceSurveillanceSupported(ThaDevice self);

/* Interrupt controller factory */
ThaIntrController ThaDeviceIntrControllerCreate(ThaDevice self, AtIpCore core);
eBool ThaDeviceNeedProfileInterrupt(ThaDevice self);

/* Concrete devices */
AtDevice ThaStmPwDeviceNew(AtDriver driver, uint32 productCode);
AtDevice ThaPdhPwDeviceNew(AtDriver driver, uint32 productCode);

/* Part managements */
uint8 ThaDeviceMaxNumParts(ThaDevice self);
uint32 ThaDevicePartOffset(ThaDevice self, uint8 partId);
uint8 ThaDeviceNumPartsOfModule(ThaDevice self, eAtModule moduleId);
uint32 ThaDeviceModulePartOffset(ThaDevice self, eAtModule moduleId, uint8 partId);

/* Registers */
eBool ThaDeviceCanCheckPll(ThaDevice self);
uint32 ThaDevicePllClkSysStatReg(ThaDevice self);
uint32 ThaDevicePllClkSysStatBitMask(ThaDevice self);
uint32 ThaDeviceModuleActiveMask(ThaDevice self, uint32 moduleId);
uint32 ThaDeviceModuleActiveShift(ThaDevice self, uint32 moduleId);

/* For testing */
eBool ThaDeviceIsEp(ThaDevice self);
uint32 ThaDeviceExpectedProductCode(ThaDevice self);

void ThaDeviceClockResultPrint(AtDevice self, AtDebugger debugger, const char *clockString, uint32 regAddr, uint32 expectedClockInHz, uint32 ppmSwingRange);

/* For backward compatible */
eBool ThaDeviceSupportFeatureFromVersion(AtDevice self, uint32 major, uint32 minor, uint32 betaBuild);

/* Concrete products */
AtDevice ThaPdhPwProductDeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60031032CommonDeviceNew(AtDriver driver, uint32 productCode);

/* For debugging */
void ThaDeviceRegNameDisplay(const char* regName);
void ThaDeviceChannelRegValueDisplay(AtChannel channel, uint32 address, eAtModule moduleId);
void ThaDeviceChannelRegLongValueDisplay(AtChannel channel, uint32 address, eAtModule moduleId);
void ThaDeviceModuleRegValueDisplay(AtModule module, uint32 address);
void ThaDeviceModuleRegLongValueDisplay(AtModule module, uint32 address, uint8 coreId);
void ThaDeviceRegLongValueDisplayOnly(const char* regName, uint32 address, uint32 *longRegVal, uint32 numOfDword);
const char *ThaDeviceUtilLongRegValue2String(uint32 *regValues, uint16 numDwords);

/* Diagnostic */
eBool ThaDeviceStickyIsGood(AtDevice self, uint32 regAddr, uint32 bitMask, uint32 timeoutMs);
eBool ThaDeviceErrorGeneratorIsSupported(ThaDevice self);
eBool ThaDeviceSerdesPowerControlIsSupported(ThaDevice self);
eAtRet ThaDeviceStickyAsyncCheck(AtDevice self, uint32 regAddr, uint32 bitMask, uint32 timeoutMs);

/* Memory Flush */
void ThaDeviceMemoryLongFlush(ThaDevice self, uint32 start, uint32 stop);
void ThaDeviceMemoryFlush(ThaDevice self, uint32 start, uint32 stop, uint32 value);

AtResequenceManager ThaDeviceResequenceManagerGet(ThaDevice self);
AtResequenceManager ThaDeviceResequenceManagerCreate(ThaDevice self);

/* Clock state value change */
eBool ThaDeviceHwClockStateShouldUseNewTransition(ThaDevice self);
eBool ThaDeviceClockDebugIsSupported(ThaDevice self);
eBool ThaDeviceStickyDebugIsSupported(ThaDevice self);
#ifdef __cplusplus
}
#endif
#endif /* _THADEVICE_H_ */



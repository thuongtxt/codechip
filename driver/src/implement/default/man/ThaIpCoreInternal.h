/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : Device Management
 *
 * File        : ThaIpCoreInternal.h
 *
 * Created Date: Oct 16, 2012
 *
 * Description : Thalassa IP Core
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _THAIPCOREINTERNAL_H_
#define _THAIPCOREINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/man/AtIpCoreInternal.h"
#include "ThaDevice.h"
#include "ThaIpCore.h"
#include "intrcontroller/ThaIntrController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaIpCoreMethods
    {
    void (*InterruptProcess)(ThaIpCore self);
    eAtRet (*InterruptRestore)(ThaIpCore self);
    eBool (*SdhCauseInterrupt)(ThaIpCore self, uint32 intrStatus);
    eBool (*PdhCauseInterrupt)(ThaIpCore self, uint32 intrStatus);
    eBool (*PppCauseInterrupt)(ThaIpCore self, uint32 intrStatus);
    eBool (*PwCauseInterrupt)(ThaIpCore self, uint32 intrStatus);
    eBool (*SurCauseInterrupt)(ThaIpCore self, uint32 intrStatus);
    eBool (*EthCauseInterrupt)(ThaIpCore self, uint32 intrStatus);
    eBool (*RamCauseInterrupt)(ThaIpCore self, uint32 intrStatus);
    eBool (*PtpCauseInterrupt)(ThaIpCore self, uint32 intrStatus);
    eBool (*ConcateCauseInterrupt)(ThaIpCore self, uint32 intrStatus);
    eBool (*EncapCauseInterrupt)(ThaIpCore self, uint32 intrStatus);
    eBool (*ThermalCauseInterrupt)(ThaIpCore self, uint32 intrStatus);
    eBool (*SemCauseInterrupt)(ThaIpCore self, uint32 intrStatus);
    eBool (*ClockCauseInterrupt)(ThaIpCore self, uint32 intrStatus);
    eBool (*ApsCauseInterrupt)(ThaIpCore self, uint32 intrStatus);
    uint32 (*InterruptStatusGet)(ThaIpCore self);
    uint32 (*InterruptDisable)(ThaIpCore self);

    /* Thalassa does not support a global bit to enable interrupt. To do so,
     * modules APIs may be called. But, module interrupt APIs should be called
     * only one time to enable/disable interrupt, when a module enables interrupt,
     * it can flush its internal state. Calling them in interrupt processing may
     * cause module internal state be flushed consequently.
     *
     * This is why this internal function comes, it just enable/disable hardware
     * interrupt of modules which are previous enabled at one time */
    void (*AllModulesInterruptEnable)(ThaIpCore self, uint32 intrEnMask);

    void (*PdhHwInterruptEnable)(ThaIpCore self, eBool enable);
    void (*SdhHwInterruptEnable)(ThaIpCore self, eBool enable);
    void (*PppHwInterruptEnable)(ThaIpCore self, eBool enable);
    void (*PwHwInterruptEnable)(ThaIpCore self, eBool enable);
    void (*SurHwInterruptEnable)(ThaIpCore self, eBool enable);
    void (*EthHwInterruptEnable)(ThaIpCore self, eBool enable);
    void (*RamHwInterruptEnable)(ThaIpCore self, eBool enable);
    void (*PtpHwInterruptEnable)(ThaIpCore self, eBool enable);
    void (*ConcateHwInterruptEnable)(ThaIpCore self, eBool enable);
    void (*EncapHwInterruptEnable)(ThaIpCore self, eBool enable);
    void (*PhysicalHwInterruptEnable)(ThaIpCore self, eBool enable); /* All physical monitoring enable. */
    void (*ClockHwInterruptEnable)(ThaIpCore self, eBool enable);
    void (*ApsHwInterruptEnable)(ThaIpCore self, eBool enable);
    void (*HwInterruptEnable)(ThaIpCore self, uint32 intrEnMask, eBool enable);

    eBool (*FastRamTestIsEnabled)(ThaIpCore self);
    eAtRet (*SerdesSoftReset)(ThaIpCore core);
    eAtRet (*RamSoftReset)(ThaIpCore core);
    eAtRet (*RamSoftResetWait)(ThaIpCore core);
    uint32 (*RamStableWaitInMs)(ThaIpCore core);
    uint32 (*RamSoftResetRegister)(ThaIpCore self);
    eAtRet (*AsyncSerdesSoftReset)(ThaIpCore core);
    eAtRet (*AsyncRamSoftReset)(ThaIpCore core);
    eAtRet (*AsyncRamSoftResetWait)(ThaIpCore core);

    /* Some FPGA has two parts */
    uint8 (*NumParts)(ThaIpCore self);
    uint32 (*PartOffset)(ThaIpCore self, uint8 partId);

    /* Some product support disable interrupt pin by a distinct register. */
    eBool (*CanEnableHwInterruptPin)(ThaIpCore self);
    eBool (*InterruptRestoreIsSupported)(ThaIpCore self);
    }tThaIpCoreMethods;

typedef struct tThaIpCore
    {
    tAtIpCore super;
    const tThaIpCoreMethods* methods;

    /* Private data */
    eBool interruptIsEnabled;
    ThaIntrController intrController;
    uint32 ramResetWaitTimeMs;

    /* To handle interrupt flooding */
    eBool interruptHappened;
    uint32 interruptCount;
    uint32 interruptContiguousCount;
    uint32 maxInterruptProcessingTimeUs;
    uint32 lastInterruptProcessingTimeUs;

    /* Async init */
    uint32 asyncResetState;
    uint32 asyncRamResetState;
    uint32 asyncSerdesResetState;
    tAtOsalCurTime asyncStartTime;
    uint32 asyncRamResetWaitState;
    }tThaIpCore;

typedef struct tThaIpCorePwV2 *ThaIpCorePwV2;

typedef struct tThaIpCorePwV2
    {
    tThaIpCore super;

    /* Private data */
    }tThaIpCorePwV2;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtIpCore ThaIpCoreObjectInit(AtIpCore self, uint8 coreId, AtDevice device);
AtIpCore ThaIpCorePwV2ObjectInit(AtIpCore self, uint8 coreId, AtDevice device);

uint32 ThaIpCoreInterruptStatusGet(ThaIpCore self);
uint32 ThaIpCoreInterruptDisable(ThaIpCore self);
eBool ThaIpCoreSdhCauseInterrupt(ThaIpCore self, uint32 intrStatus);
eBool ThaIpCorePdhCauseInterrupt(ThaIpCore self, uint32 intrStatus);
eBool ThaIpCoreSurCauseInterrupt(ThaIpCore self, uint32 intrStatus);
eBool ThaIpCoreEthCauseInterrupt(ThaIpCore self, uint32 intrStatus);
eBool ThaIpCorePwCauseInterrupt(ThaIpCore self, uint32 intrStatus);
eBool ThaIpCoreRamCauseInterrupt(ThaIpCore self, uint32 intrStatus);
eBool ThaIpCorePtpCauseInterrupt(ThaIpCore self, uint32 intrStatus);
eBool ThaIpCoreThermalCauseInterrupt(ThaIpCore self, uint32 intrStatus);
eBool ThaIpCoreSemCauseInterrupt(ThaIpCore self, uint32 intrStatus);
eBool ThaIpCoreClockCauseInterrupt(ThaIpCore self, uint32 intrStatus);
eBool ThaIpCoreApsCauseInterrupt(ThaIpCore self, uint32 intrStatus);
eBool ThaIpCoreConcateCauseInterrupt(ThaIpCore self, uint32 intrStatus);
eBool ThaIpCoreEncapCauseInterrupt(ThaIpCore self, uint32 intrStatus);
void ThaIpCoreAllModulesInterruptEnable(ThaIpCore self, uint32 intrEnMask);
void ThaIpCoreInterruptProcess(ThaIpCore self);
eAtRet ThaIpCoreInterruptRestore(ThaIpCore self);
eBool ThaIpCoreCanEnableHwInterruptPin(ThaIpCore self);
eBool ThaIpCoreInterruptRestoreIsSupported(ThaIpCore self);

void ThaIpCorePdhHwInterruptEnable(ThaIpCore self, eBool enable);
void ThaIpCoreSdhHwInterruptEnable(ThaIpCore self, eBool enable);
void ThaIpCorePppHwInterruptEnable(ThaIpCore self, eBool enable);
void ThaIpCorePwHwInterruptEnable(ThaIpCore self, eBool enable);
void ThaIpCoreSurHwInterruptEnable(ThaIpCore self, eBool enable);
void ThaIpCoreEthHwInterruptEnable(ThaIpCore self, eBool enable);
void ThaIpCoreRamHwInterruptEnable(ThaIpCore self, eBool enable);
void ThaIpCorePtpHwInterruptEnable(ThaIpCore self, eBool enable);
void ThaIpCorePhysicalHwInterruptEnable(ThaIpCore self, eBool enable);
void ThaIpCoreClockHwInterruptEnable(ThaIpCore self, eBool enable);
void ThaIpCoreApsHwInterruptEnable(ThaIpCore self, eBool enable);
void ThaIpCoreConcateHwInterruptEnable(ThaIpCore self, eBool enable);
void ThaIpCoreEncapHwInterruptEnable(ThaIpCore self, eBool enable);
void ThaIpCoreHwInterruptEnable(ThaIpCore self, uint32 intrEnMask, eBool enable);

/* Part management */
uint8 ThaIpCoreNumParts(ThaIpCore self);
uint32 ThaIpCorePartOffset(ThaIpCore self, uint8 partId);

eBool ThaIpCoreIsRamResetDone(ThaIpCore self);
#ifdef __cplusplus
}
#endif
#endif /* _THAIPCOREINTERNAL_H_ */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : ThaDeviceModuleIterator.c
 *
 * Created Date: Nov 9, 2012
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../util/AtIteratorInternal.h"
#include "ThaDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaDeviceModuleIterator * ThaDeviceModuleIterator;

/* To iterate all modules */
typedef struct tThaDeviceModuleIterator
    {
    tAtIterator super;

    /* Private data */
    ThaDevice device;
    AtIterator standardModuleIterator;
    int8 currentModuleId;
    }tThaDeviceModuleIterator;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* To override */
static tAtIteratorMethods m_AtIteratorOverride;
static tAtObjectMethods m_AtObjectOverride;

/* To save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaDeviceModuleIterator);
    }

static AtObject NextGet(AtIterator self)
    {
    ThaDeviceModuleIterator iterator = (ThaDeviceModuleIterator)self;
    AtObject nextModule;

    /* Iterate logical module */
    nextModule = AtIteratorNext(iterator->standardModuleIterator);
    if (nextModule != NULL)
        return nextModule;

    /* Iterator physical module till a valid one is found */
    while (iterator->currentModuleId != cThaModuleEnd)
        {
        nextModule = (AtObject)AtDeviceModuleGet((AtDevice)iterator->device, (eAtModule)iterator->currentModuleId);
        iterator->currentModuleId = (int8)(iterator->currentModuleId + 1);
        if (nextModule)
            return nextModule;
        }

    return NULL;
    }

static uint32 Count(AtIterator self)
    {
    ThaDeviceModuleIterator iterator = (ThaDeviceModuleIterator)self;

    return AtIteratorCount(iterator->standardModuleIterator) + (cThaModuleEnd - cThaModuleStart);
    }

static void Delete(AtObject self)
    {
    ThaDeviceModuleIterator iterator = (ThaDeviceModuleIterator)self;
    AtObjectDelete((AtObject)(iterator->standardModuleIterator));
    iterator->standardModuleIterator = NULL;

    m_AtObjectMethods->Delete(self);
    }

static void OverrideAtIterator(AtIterator self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtIteratorOverride, mMethodsGet(self), sizeof(m_AtIteratorOverride));
        mMethodOverride(m_AtIteratorOverride, NextGet);
        mMethodOverride(m_AtIteratorOverride, Count);
        }

    mMethodsSet(self, &m_AtIteratorOverride);
    }

static void OverrideAtObject(AtIterator self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtIterator self)
    {
    OverrideAtIterator(self);
    OverrideAtObject(self);
    }

static AtIterator ThaDeviceModuleIteratorObjectInit(AtIterator self, ThaDevice device)
    {
    ThaDeviceModuleIterator iterator = (ThaDeviceModuleIterator)self;

    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtIteratorObjectInit(self) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    /* Setup private data */
    iterator->device = device;
    iterator->standardModuleIterator = AtDeviceModuleIteratorCreate((AtDevice)device);
    iterator->currentModuleId = cThaModuleStart;

    return self;
    }

static AtIterator ThaDeviceModuleIteratorNew(ThaDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtIterator newIterator = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newIterator == NULL)
        return NULL;

    /* Construct it */
    return ThaDeviceModuleIteratorObjectInit(newIterator, device);
    }

/*
 * Create iterator that iterate all of modules including physical modules
 *
 * @param self This device
 * @return Module iterator
 */
AtIterator ThaDeviceModuleIteratorCreate(AtDevice self)
    {
    return ThaDeviceModuleIteratorNew((ThaDevice)self);
    }

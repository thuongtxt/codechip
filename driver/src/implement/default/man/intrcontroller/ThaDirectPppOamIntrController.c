/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : ThaPppDirectOamIntrController.c
 *
 * Created Date: Aug 15, 2013
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../ThaIpCoreInternal.h"
#include "../../ppp/ThaModulePppInternal.h"
#include "../../ppp/ThaMpigReg.h"
#include "ThaIntrControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
/* This controller is used when PPP OAM interrupt connect directly to CPU,
 * Other modules do not support interrupt
 */
typedef struct tThaDirectPppOamIntrController* ThaDirectPppOamIntrController;

typedef struct tThaDirectPppOamIntrController
    {
    tThaIntrController super;
    }tThaDirectPppOamIntrController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tThaIpCoreMethods m_ThaIpCoreOverride;

/* Reference to super implementation (option) */
static const tThaIpCoreMethods *m_ThaIpCoreImplement;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void PppHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    uint32 interruptEnReg, address;
    AtHal hal = AtIpCoreHalGet(mIpCore(self));

    if (hal == NULL)
        return;

    /* Enable OAM interrupt */
    address = cThaOamDirectIntrControl + mMethodsGet(hal)->DirectOamOffsetGet(hal);
    interruptEnReg = AtHalNoLockRead(hal, address);

    mFieldIns(&interruptEnReg, cThaOamDirectIntrMask, cThaOamDirectIntrShift, mBoolToBin(enable));
    AtHalNoLockWrite(hal, address, interruptEnReg);
    }

static uint32 InterruptDisable(ThaIpCore self)
    {
    /* Only module PPP support interrupt */
    PppHwInterruptEnable(self, cAtFalse);
    return 0;
    }

static uint32 InterruptStatusGet(ThaIpCore self)
    {
	AtUnused(self);
    /* This interrupt controller assumes that whenever an interrupt happens at CPU,
     * It always causes by the interrupt of incoming PPP OAM packet, so nothing return here
     */
    return 0;
    }

static void AllModulesInterruptEnable(ThaIpCore self, uint32 intrMask)
    {
	AtUnused(intrMask);
    /* Only module PPP support interrupt */
    PppHwInterruptEnable(self, cAtTrue);
    }

static eBool PppCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
	AtUnused(intrStatus);
	AtUnused(self);
    /* Whenever interrupt occurs, it always causes by PPP OAM */
    return cAtTrue;
    }

static void OverrideThaIpCore(ThaDirectPppOamIntrController self)
    {
    ThaIpCore thaIp = (ThaIpCore)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaIpCoreImplement = mMethodsGet(thaIp);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaIpCoreOverride, m_ThaIpCoreImplement, sizeof(m_ThaIpCoreOverride));
        mMethodOverride(m_ThaIpCoreOverride, PppCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, InterruptStatusGet);
        mMethodOverride(m_ThaIpCoreOverride, InterruptDisable);
        mMethodOverride(m_ThaIpCoreOverride, AllModulesInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, PppHwInterruptEnable);
        }

    mMethodsSet(thaIp, &m_ThaIpCoreOverride);
    }

static void Override(ThaDirectPppOamIntrController self)
    {
    OverrideThaIpCore(self);
    }

/* Constructor of ThaDirectPppOamIntrController object */
static ThaIntrController ObjectInit(ThaIntrController self, AtIpCore ipCore)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaDirectPppOamIntrController));

    /* Super constructor */
    if (ThaIntrControllerObjectInit(self, ipCore) == NULL)
        return NULL;

    /* Override */
    Override((ThaDirectPppOamIntrController)self);

    m_methodsInit = 1;

    return self;
    }

/* Create controller object */
ThaIntrController ThaDirectPppOamIntrControllerNew(AtIpCore ipCore)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaIntrController newController = mMethodsGet(osal)->MemAlloc(osal, sizeof(tThaDirectPppOamIntrController));
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, ipCore);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : ThaIntrControllerInternal.h
 *
 * Created Date: Aug 15, 2013
 *
 * Description : Interrupt controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAINTRCONTROLLERINTERNAL_H_
#define _THAINTRCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"
#include "AtObject.h"
#include "../../../../generic/man/AtIpCoreInternal.h"
#include "../ThaIpCoreInternal.h"
#include "ThaIntrController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define mIpCore(controller) (ThaIntrControllerIpCoreGet((ThaIntrController)controller))

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaIntrController
    {
    tThaIpCore super;

    AtIpCore ipCore;
    }tThaIntrController;

typedef struct tThaDefaultIntrController* ThaDefaultIntrController;

typedef struct tThaDefaultIntrControllerMethods
    {
    uint32 (*PdhInterruptEnableMask)(ThaDefaultIntrController self);
    uint32 (*PdhInterruptStatusMask)(ThaDefaultIntrController self);
    }tThaDefaultIntrControllerMethods;

typedef struct tThaDefaultIntrController
    {
    tThaIntrController super;

    const tThaDefaultIntrControllerMethods *methods;
    }tThaDefaultIntrController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Default controller internal functions */
ThaIntrController ThaIntrControllerObjectInit(ThaIntrController self, AtIpCore ipCore);
ThaIntrController ThaDefaultIntrControllerObjectInit(ThaIntrController self, AtIpCore ipCore);

#ifdef __cplusplus
}
#endif
#endif /* _THAINTRCONTROLLERINTERNAL_H_ */


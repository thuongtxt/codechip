/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : INTERRUPT
 *
 * File        : ThaDefaultIntrController.c
 *
 * Created Date: Aug 15, 2013
 *
 * Description : Default device interrupt implementation.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../ThaIpCoreInternal.h"
#include "../../../../generic/man/AtDriverInternal.h"
#include "../ThaDeviceReg.h"
#include "../ThaDevice.h"
#include "ThaIntrControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tThaIpCoreMethods m_ThaIpCoreOverride;
static tThaDefaultIntrControllerMethods m_methods;

/* Reference to super implementation (option) */
static const tThaIpCoreMethods *m_ThaIpCoreImplement;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 PartInterruptDisable(ThaIpCore self, uint8 partId)
    {
    uint32 interruptEnable = 0;
    AtHal hal = AtIpCoreHalGet(mIpCore(self));
    uint32 regAddr;

    if (hal == NULL)
        return 0;

    /* Get global interrupt enable */
    regAddr = cThaRegChipIntrCtrl + ThaIpCorePartOffset(self, partId);
    interruptEnable = AtHalNoLockRead(hal, regAddr);

    /* Disable */
    AtHalNoLockWrite(hal, regAddr, 0);

    return interruptEnable;
    }

static uint32 InterruptDisable(ThaIpCore self)
    {
    uint8 part_i;
    uint32 interruptEnable = 0;

    for (part_i = 0; part_i < ThaIpCoreNumParts(self); part_i++)
        interruptEnable |= PartInterruptDisable(self, part_i);

    return interruptEnable;
    }

static uint32 InterruptStatusGet(ThaIpCore self)
    {
    uint32 interruptStatus = 0;
    uint8 part_i;

    /* Get global interrupt status */
    AtHal hal = AtIpCoreHalGet(mIpCore(self));
    if (hal == NULL)
        return 0;

    for (part_i = 0; part_i < ThaIpCoreNumParts(self); part_i++)
        interruptStatus |= AtHalNoLockRead(hal, cThaRegChipIntrSta + ThaIpCorePartOffset(self, part_i));

    return interruptStatus;
    }

static eAtRet InterruptRestore(ThaIpCore self)
    {
    uint32 interruptEnable = 0;
    AtHal hal = AtIpCoreHalGet(mIpCore(self));

    if (hal == NULL)
        return cAtOk;

    /* Get global interrupt enable */
    interruptEnable = AtHalNoLockRead(hal, cThaRegChipIntrCtrl);

    /* Restore interrupt for relevant modules. */
    AtHalNoLockWrite(hal, cThaRegChipIntrRestore, interruptEnable);

    return cAtOk;
    }

static void AllModulesInterruptEnable(ThaIpCore self, uint32 intrMask)
    {
    uint8 part_i;
    AtHal hal = AtIpCoreHalGet(mIpCore(self));
    if (hal == NULL)
        return;

    for (part_i = 0; part_i < ThaIpCoreNumParts(self); part_i++)
        AtHalNoLockWrite(hal, cThaRegChipIntrCtrl + ThaIpCorePartOffset(self, part_i), intrMask);
    }

static eBool SdhCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
	AtUnused(self);
    return (intrStatus & cThaRegChipIntrStaSdhModuleAlarm) ? cAtTrue : cAtFalse;
    }

static eBool PdhCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    ThaDefaultIntrController controller = (ThaDefaultIntrController)self;
    uint32 statusMask = mMethodsGet(controller)->PdhInterruptStatusMask(controller);

    return (intrStatus & statusMask) ? cAtTrue : cAtFalse;
    }

static void InterruptHwEnableOnPart(ThaIpCore self, eAtModule module, uint32 mask, eBool enable, uint8 partId)
    {
    AtHal hal = AtIpCoreHalGet(mIpCore(self));
    ThaDevice device = (ThaDevice)AtIpCoreDeviceGet(mIpCore(self));
    uint32 partOffset = ThaDeviceModulePartOffset(device, module, partId);
    uint32 interruptEnReg = AtHalNoLockRead(hal, cThaRegChipIntrCtrl + partOffset);

    if (enable)
        interruptEnReg |= mask;
    else
        interruptEnReg &= ~mask;

    AtHalWrite(hal, cThaRegChipIntrCtrl + partOffset, interruptEnReg);
    }

static void PdhHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    uint8 part_i;
    ThaDevice device = (ThaDevice)AtIpCoreDeviceGet(mIpCore(self));
    ThaDefaultIntrController controller = (ThaDefaultIntrController)self;
    uint32 enMask = mMethodsGet(controller)->PdhInterruptEnableMask(controller);

    for (part_i = 0; part_i < ThaDeviceNumPartsOfModule(device, cAtModulePdh); part_i++)
        InterruptHwEnableOnPart(self, cAtModulePdh, enMask, enable, part_i);
    }

static void SdhHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    uint8 part_i;
    ThaDevice device = (ThaDevice)AtIpCoreDeviceGet(mIpCore(self));

    for (part_i = 0; part_i < ThaDeviceNumPartsOfModule(device, cAtModuleSdh); part_i++)
        InterruptHwEnableOnPart(self, cAtModuleSdh, cThaRegChipIntrStaSdhModuleAlarm, enable, part_i);
    }

static void OverrideThaIpCore(ThaIntrController self)
    {
    ThaIpCore thaIp = (ThaIpCore)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaIpCoreImplement = mMethodsGet(thaIp);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaIpCoreOverride, m_ThaIpCoreImplement, sizeof(m_ThaIpCoreOverride));
        mMethodOverride(m_ThaIpCoreOverride, SdhCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, PdhCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, InterruptStatusGet);
        mMethodOverride(m_ThaIpCoreOverride, InterruptDisable);
        mMethodOverride(m_ThaIpCoreOverride, AllModulesInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, PdhHwInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, SdhHwInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, InterruptRestore);
        }

    mMethodsSet(thaIp, &m_ThaIpCoreOverride);
    }

static void Override(ThaIntrController self)
    {
    OverrideThaIpCore(self);
    }

static uint32 PdhInterruptEnableMask(ThaDefaultIntrController self)
    {
	AtUnused(self);
    return cThaRegChipIntrStaPdhModuleAlarm;
    }

static uint32 PdhInterruptStatusMask(ThaDefaultIntrController self)
    {
	AtUnused(self);
    return cThaRegChipIntrStaPdhModuleAlarm;
    }

static void MethodsInit(ThaDefaultIntrController self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, PdhInterruptEnableMask);
        mMethodOverride(m_methods, PdhInterruptStatusMask);
        }
    mMethodsSet(self, &m_methods);
    }

/* Constructor of ThaIntrController object */
ThaIntrController ThaDefaultIntrControllerObjectInit(ThaIntrController self, AtIpCore ipCore)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaDefaultIntrController));

    /* Super constructor */
    if (ThaIntrControllerObjectInit(self, ipCore) == NULL)
        return NULL;

    /* Override */
    Override(self);
    MethodsInit((ThaDefaultIntrController)self);

    m_methodsInit = 1;
    return self;
    }

/* Create controller object */
ThaIntrController ThaDefaultIntrControllerNew(AtIpCore core)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaIntrController newController = mMethodsGet(osal)->MemAlloc(osal, sizeof(tThaDefaultIntrController));
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ThaDefaultIntrControllerObjectInit(newController, core);
    }


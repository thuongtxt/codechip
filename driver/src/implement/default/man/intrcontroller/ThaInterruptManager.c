/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : INTERRUPT
 *
 * File        : ThaInterruptManager.c
 *
 * Created Date: Dec 12, 2015
 *
 * Description : Default module interrupt manager implementation.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/man/AtDriverInternal.h"
#include "../../../../generic/man/AtModuleInternal.h"
#include "../ThaIpCoreInternal.h"
#include "ThaInterruptManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaInterruptManager)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

static tThaInterruptManagerMethods m_methods;

/* Override */
static tAtObjectMethods           m_AtObjectOverride;
static tAtInterruptManagerMethods m_AtInterruptManagerOverride;

/* Save super implementation */
static const tAtObjectMethods    *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtDevice DeviceGet(AtInterruptManager self)
    {
    return AtModuleDeviceGet(AtInterruptManagerModuleGet(self));
    }

static eAtRet InterruptEnable(AtInterruptManager self, eBool enable)
    {
    uint8 i, numCore;
    AtIpCore ipCore;
    AtDevice device = DeviceGet(self);

    numCore = AtDeviceNumIpCoresGet(device);

    for (i = 0; i < numCore; i++)
        {
        ipCore = AtDeviceIpCoreGet(device, i);

        /* If a product supports interrupt pin enable register, just simple
         * enable module interrupt on IPcore. Otherwise, we need to check if
         * IPcore interrupt was enabled before enable module interrupt. */
        if (ThaIpCoreCanEnableHwInterruptPin((ThaIpCore)ipCore) || AtIpCoreInterruptIsEnabled(ipCore))
            mMethodsGet(mThis(self))->InterruptHwEnable(mThis(self), enable, ipCore);

        /* Store to DB */
        mThis(self)->interruptEnabled = enable;
        }

    return cAtOk;
    }

static eBool InterruptIsEnabled(AtInterruptManager self)
    {
    return mThis(self)->interruptEnabled;
    }

static void InterruptOnIpCoreEnable(AtInterruptManager self, eBool enable, AtIpCore ipCore)
    {
    if (mMethodsGet(self)->InterruptIsEnabled(self))
        mMethodsGet(mThis(self))->InterruptHwEnable(mThis(self), enable, ipCore);
    }

static uint32 BaseAddress(ThaInterruptManager self)
    {
    AtUnused(self);
    return 0x0;
    }

static uint32 CurrentStatusRegister(ThaInterruptManager self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 InterruptStatusRegister(ThaInterruptManager self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 InterruptEnableRegister(ThaInterruptManager self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 SliceOffset(ThaInterruptManager self, uint8 sliceId)
    {
    AtUnused(self);
    AtUnused(sliceId);
    return 0;
    }

static uint32 NumSlices(ThaInterruptManager self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 SlicesInterruptGet(ThaInterruptManager self, uint32 glbIntr, AtHal hal)
    {
    AtUnused(self);
    AtUnused(glbIntr);
    AtUnused(hal);
    return 0;
    }

static void InterruptHwEnable(ThaInterruptManager self, eBool enable, AtIpCore ipCore)
    {
    AtUnused(self);
    AtUnused(enable);
    AtUnused(ipCore);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaInterruptManager object = (ThaInterruptManager)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(interruptEnabled);
    }

static uint32 NumStsInSlice(ThaInterruptManager self)
    {
    AtUnused(self);
    return 0;
    }

static void OverrideAtObject(AtInterruptManager self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtInterruptManager(AtInterruptManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtInterruptManagerOverride, mMethodsGet(self), sizeof(m_AtInterruptManagerOverride));

        mMethodOverride(m_AtInterruptManagerOverride, InterruptEnable);
        mMethodOverride(m_AtInterruptManagerOverride, InterruptIsEnabled);
        mMethodOverride(m_AtInterruptManagerOverride, InterruptOnIpCoreEnable);
        }

    mMethodsSet(self, &m_AtInterruptManagerOverride);
    }

static void Override(AtInterruptManager self)
    {
    OverrideAtObject(self);
    OverrideAtInterruptManager(self);
    }

static void MethodsInit(ThaInterruptManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, BaseAddress);
        mMethodOverride(m_methods, CurrentStatusRegister);
        mMethodOverride(m_methods, InterruptStatusRegister);
        mMethodOverride(m_methods, InterruptEnableRegister);
        mMethodOverride(m_methods, InterruptHwEnable);
        mMethodOverride(m_methods, SliceOffset);
        mMethodOverride(m_methods, NumSlices);
        mMethodOverride(m_methods, SlicesInterruptGet);
        mMethodOverride(m_methods, NumStsInSlice);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaInterruptManager);
    }

AtInterruptManager ThaInterruptManagerObjectInit(AtInterruptManager self, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtInterruptManagerObjectInit(self, module) == NULL)
        return NULL;

    /* Override */
    Override(self);
    MethodsInit((ThaInterruptManager)self);
    m_methodsInit = 1;

    return self;
    }

AtInterruptManager ThaInterruptManagerNew(AtModule module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInterruptManager newManager = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newManager == NULL)
        return NULL;

    /* Construct it */
    return ThaInterruptManagerObjectInit(newManager, module);
    }

uint32 ThaInterruptManagerBaseAddress(ThaInterruptManager self)
    {
    if (self)
        return mMethodsGet(self)->BaseAddress(self);
    return 0x0;
    }

uint32 ThaInterruptManagerCurrentStatusRegister(ThaInterruptManager self)
    {
    if (self)
        return mMethodsGet(self)->CurrentStatusRegister(self);
    return 0;
    }

uint32 ThaInterruptManagerInterruptStatusRegister(ThaInterruptManager self)
    {
    if (self)
        return mMethodsGet(self)->InterruptStatusRegister(self);
    return 0;
    }

uint32 ThaInterruptManagerInterruptEnableRegister(ThaInterruptManager self)
    {
    if (self)
        return mMethodsGet(self)->InterruptEnableRegister(self);
    return 0;
    }

uint32 ThaInterruptManagerNumSlices(ThaInterruptManager self)
    {
    if (self)
        return mMethodsGet(self)->NumSlices(self);
    return 0;
    }

uint32 ThaInterruptManagerNumStsInSlices(ThaInterruptManager self)
    {
    if (self)
        return mMethodsGet(self)->NumStsInSlice(self);
    return 0;
    }

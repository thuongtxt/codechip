/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : ThaIntrController.h
 * 
 * Created Date: Aug 27, 2013
 *
 * Description : Interrupt controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAINTRCONTROLLER_H_
#define _THAINTRCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaIntrController* ThaIntrController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Default concretes */
ThaIntrController ThaDirectPppOamIntrControllerNew(AtIpCore ipCore);
ThaIntrController ThaPdhMlpppIntrControllerNew(AtIpCore ipCore);
ThaIntrController ThaStmMlpppIntrControllerNew(AtIpCore ipCore);
ThaIntrController ThaDefaultIntrControllerNew(AtIpCore core);
ThaIntrController ThaStmPwIntrControllerNew(AtIpCore ipCore);

/* Access private date */
AtIpCore ThaIntrControllerIpCoreGet(ThaIntrController self);

/* Product concretes */
ThaIntrController Tha60030022IntrControllerNew(AtIpCore ipCore);
ThaIntrController Tha60030051IntrControllerNew(AtIpCore ipCore);
ThaIntrController Tha60060011IntrControllerNew(AtIpCore ipCore);
ThaIntrController Tha60210021IntrControllerNew(AtIpCore ipCore);
ThaIntrController Tha61210011IntrControllerNew(AtIpCore ipCore);
ThaIntrController Tha60290021IntrControllerNew(AtIpCore ipCore);
ThaIntrController Tha60290011IntrControllerNew(AtIpCore ipCore);

#ifdef __cplusplus
}
#endif
#endif /* _THAINTRCONTROLLER_H_ */


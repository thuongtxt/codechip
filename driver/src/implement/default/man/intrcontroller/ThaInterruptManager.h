/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : INTERRUPT
 * 
 * File        : ThaInterruptManager.h
 * 
 * Created Date: Dec 12, 2015
 *
 * Description : Default module interrupt manager interface.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAINTERRUPTMANAGER_H_
#define _THAINTERRUPTMANAGER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/man/interrupt/AtInterruptManager.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaInterruptManager * ThaInterruptManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 ThaInterruptManagerBaseAddress(ThaInterruptManager self);
uint32 ThaInterruptManagerCurrentStatusRegister(ThaInterruptManager self);
uint32 ThaInterruptManagerInterruptStatusRegister(ThaInterruptManager self);
uint32 ThaInterruptManagerInterruptEnableRegister(ThaInterruptManager self);

uint32 ThaInterruptManagerNumSlices(ThaInterruptManager self);
uint32 ThaInterruptManagerNumStsInSlices(ThaInterruptManager self);

/* Product concretes */
AtInterruptManager Tha60290022CdrInterruptManagerNew(AtModule module);
AtInterruptManager Tha60290022CdrInterruptManagerV3New(AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _THAINTERRUPTMANAGER_H_ */


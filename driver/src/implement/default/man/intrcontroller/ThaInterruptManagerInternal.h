/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : INTERRUPT
 * 
 * File        : ThaInterruptManagerInternal.h
 * 
 * Created Date: Dec 12, 2015
 *
 * Description : Default module interrupt manager representation.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAINTERRUPTMANAGERINTERNAL_H_
#define _THAINTERRUPTMANAGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/man/interrupt/AtInterruptManagerInternal.h" /* Superclass. */
#include "ThaInterruptManager.h"
#include "AtHal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define cIteratorMask(iter)     (cBit0 << (iter))
#define mIntrManager(self)      ((ThaInterruptManager)self)

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaInterruptManagerMethods
    {
    uint32 (*BaseAddress)(ThaInterruptManager self);
    uint32 (*CurrentStatusRegister)(ThaInterruptManager self);
    uint32 (*InterruptStatusRegister)(ThaInterruptManager self);
    uint32 (*InterruptEnableRegister)(ThaInterruptManager self);
    void (*InterruptHwEnable)(ThaInterruptManager self, eBool enable, AtIpCore ipCore);

    /* Some RTL modules are designed as instantiations (a.k.a. slices).
     * So following methods of slices will be helpful. */
    uint32 (*SliceOffset)(ThaInterruptManager self, uint8 sliceId);
    uint32 (*NumSlices)(ThaInterruptManager self);
    uint32 (*SlicesInterruptGet)(ThaInterruptManager self, uint32 glbIntr, AtHal hal);
    uint32 (*NumStsInSlice)(ThaInterruptManager self);
    }tThaInterruptManagerMethods;

typedef struct tThaInterruptManager
    {
    tAtInterruptManager super;
    const tThaInterruptManagerMethods * methods;

    /* Private data. */
    eBool interruptEnabled;
    }tThaInterruptManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtInterruptManager ThaInterruptManagerObjectInit(AtInterruptManager self, AtModule module);
AtInterruptManager ThaInterruptManagerNew(AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _THAINTERRUPTMANAGERINTERNAL_H_ */


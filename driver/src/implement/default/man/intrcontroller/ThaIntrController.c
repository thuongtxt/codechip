/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : ThaIntrController.c
 *
 * Created Date: Aug 15, 2013
 *
 * Description : Interrupt controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/man/AtDriverInternal.h"
#include "../ThaDevice.h"
#include "../ThaIpCoreInternal.h"
#include "ThaIntrControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtObjectMethods  m_AtObjectOverride;
static tAtIpCoreMethods  m_AtIpCoreOverride;
static tThaIpCoreMethods m_ThaIpCoreOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet InterruptEnable(AtIpCore self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    /* Let sub-class do */
    return cAtErrorNotImplemented;
    }

static uint32 InterruptDisable(ThaIpCore self)
    {
	AtUnused(self);
    /* Let sub-class do */
    return 0;
    }

static uint32 InterruptStatusGet(ThaIpCore self)
    {
	AtUnused(self);
    /* Let sub-class do */
    return 0;
    }

static void AllModulesInterruptEnable(ThaIpCore self, uint32 intrMask)
    {
	AtUnused(intrMask);
	AtUnused(self);
    /* Let sub-class do */
    return;
    }

static eBool PppCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
	AtUnused(intrStatus);
	AtUnused(self);
    /* Let sub-class do */
    return cAtFalse;
    }

static eBool SdhCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
	AtUnused(intrStatus);
	AtUnused(self);
    /* Let sub-class do */
    return cAtFalse;
    }

static eBool PdhCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
	AtUnused(intrStatus);
	AtUnused(self);
    /* Let sub-class do */
    return cAtFalse;
    }

static eBool PwCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    AtUnused(intrStatus);
    AtUnused(self);
    /* Let sub-class do */
    return cAtFalse;
    }

static eBool SurCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    AtUnused(intrStatus);
    AtUnused(self);
    /* Let sub-class do */
    return cAtFalse;
    }

static eBool EthCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    AtUnused(intrStatus);
    AtUnused(self);
    /* Let sub-class do */
    return cAtFalse;
    }

static eBool RamCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    AtUnused(intrStatus);
    AtUnused(self);
    /* Let sub-class do */
    return cAtFalse;
    }

static eBool PtpCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    AtUnused(intrStatus);
    AtUnused(self);
    /* Let sub-class do */
    return cAtFalse;
    }

static eBool ThermalCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    AtUnused(intrStatus);
    AtUnused(self);
    /* Let sub-class do */
    return cAtFalse;
    }

static eBool SemCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    AtUnused(intrStatus);
    AtUnused(self);
    /* Let sub-class do */
    return cAtFalse;
    }

static eBool ClockCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    AtUnused(intrStatus);
    AtUnused(self);
    /* Let sub-class do */
    return cAtFalse;
    }

static eBool ApsCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    AtUnused(intrStatus);
    AtUnused(self);
    /* Let sub-class do */
    return cAtFalse;
    }

static eBool ConcateCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    AtUnused(intrStatus);
    AtUnused(self);
    /* Let sub-class do */
    return cAtFalse;
    }

static eBool EncapCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    AtUnused(intrStatus);
    AtUnused(self);
    /* Let sub-class do */
    return cAtFalse;
    }

static void PdhHwInterruptEnable(ThaIpCore self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    /* Let sub-class do */
    return;
    }

static void SdhHwInterruptEnable(ThaIpCore self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    /* Let sub-class do */
    return;
    }

static void PppHwInterruptEnable(ThaIpCore self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    /* Let sub-class do */
    return;
    }

static void PwHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    AtUnused(enable);
    AtUnused(self);
    /* Let sub-class do */
    return;
    }

static void SurHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    AtUnused(enable);
    AtUnused(self);
    /* Let sub-class do */
    return;
    }

static void EthHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    AtUnused(enable);
    AtUnused(self);
    /* Let sub-class do */
    return;
    }

static void RamHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    AtUnused(enable);
    AtUnused(self);
    /* Let sub-class do */
    return;
    }

static void PtpHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    AtUnused(enable);
    AtUnused(self);
    /* Let sub-class do */
    return;
    }

static void PhysicalHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    AtUnused(enable);
    AtUnused(self);
    /* Let sub-class do */
    return;
    }

static void ClockHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    AtUnused(enable);
    AtUnused(self);
    /* Let sub-class do */
    return;
    }

static void ApsHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    AtUnused(enable);
    AtUnused(self);
    /* Let sub-class do */
    return;
    }

static void ConcateHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    AtUnused(enable);
    AtUnused(self);
    /* Let sub-class do */
    return;
    }

static void EncapHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    AtUnused(enable);
    AtUnused(self);
    /* Let sub-class do */
    return;
    }

static void HwInterruptEnable(ThaIpCore self, uint32 intrEnMask, eBool enable)
    {
    AtUnused(self);
    AtUnused(intrEnMask);
    AtUnused(enable);
    }

static eAtRet InterruptRestore(ThaIpCore self)
    {
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eBool CanEnableHwInterruptPin(ThaIpCore self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool InterruptRestoreIsSupported(ThaIpCore self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static const char *ToString(AtObject self)
    {
    AtUnused(self);
    return "interrupt_controller";
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaIntrController object = (ThaIntrController)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(ipCore);
    }

static void OverrideAtObject(ThaIntrController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideThaIpCore(ThaIntrController self)
    {
    ThaIpCore thaIp = (ThaIpCore)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaIpCoreOverride, mMethodsGet(thaIp), sizeof(m_ThaIpCoreOverride));

        mMethodOverride(m_ThaIpCoreOverride, InterruptRestore);
        mMethodOverride(m_ThaIpCoreOverride, PppCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, SdhCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, PdhCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, PwCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, SurCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, EthCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, RamCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, PtpCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, ConcateCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, EncapCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, ThermalCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, SemCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, ClockCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, ApsCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, InterruptStatusGet);
        mMethodOverride(m_ThaIpCoreOverride, InterruptDisable);
        mMethodOverride(m_ThaIpCoreOverride, AllModulesInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, PdhHwInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, SdhHwInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, PppHwInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, PwHwInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, SurHwInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, EthHwInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, RamHwInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, PtpHwInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, PhysicalHwInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, ClockHwInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, ApsHwInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, ConcateHwInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, EncapHwInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, HwInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, CanEnableHwInterruptPin);
        mMethodOverride(m_ThaIpCoreOverride, InterruptRestoreIsSupported);
        }

    mMethodsSet(thaIp, &m_ThaIpCoreOverride);
    }

static void OverrideAtIpCore(ThaIntrController self)
    {
    AtIpCore ipCore = (AtIpCore)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtIpCoreOverride, mMethodsGet(ipCore), sizeof(m_AtIpCoreOverride));

        mMethodOverride(m_AtIpCoreOverride, InterruptEnable);
        }

    mMethodsSet(ipCore, &m_AtIpCoreOverride);
    }

static void Override(ThaIntrController self)
    {
    OverrideAtObject(self);
    OverrideThaIpCore(self);
    OverrideAtIpCore(self);
    }

/* Constructor of ThaIntrController object */
ThaIntrController ThaIntrControllerObjectInit(ThaIntrController self, AtIpCore ipCore)
    {
    uint8 coreId = AtIpCoreIdGet(ipCore);
    AtDevice device = AtIpCoreDeviceGet(ipCore);

    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaIntrController));

    /* Super constructor */
    if (ThaIpCoreObjectInit((AtIpCore)self, coreId, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Cache IP core */
    self->ipCore = ipCore;

    return self;
    }

AtIpCore ThaIntrControllerIpCoreGet(ThaIntrController self)
    {
    if (self)
        return self->ipCore;

    return NULL;
    }

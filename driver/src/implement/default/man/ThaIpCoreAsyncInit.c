/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAN
 *
 * File        : ThaIpCoreAsyncInit.c
 *
 * Created Date: Oct 27, 2016
 *
 * Description : Implement Async Init for IP core
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../util/coder/AtCoderUtil.h"
#include "ThaIpCoreInternal.h"
#include "ThaDeviceReg.h"
#include "ThaDeviceInternal.h"
#include "ThaDeviceReg.h"
#include "AtModule.h"
#include "../../../generic/ram/AtRamInternal.h"
#include "../../../generic/ram/AtModuleRamInternal.h"
#include "ThaIpCoreAsyncInit.h"

/*--------------------------- Define -----------------------------------------*/
#define cSerdesResetStateWaitTimeInMs 500
#define cRamResetWaitStateWaitInMs    1000
#define cNumUsPerMs                   1000

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaIpCore)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef enum eThaIpCoreAsyncResetState
    {
    cThaIpCoreAsyncResetStateRam,
    cThaIpCoreAsyncResetStateSerdes
    }eThaIpCoreAsyncResetState;

typedef enum eThaIpCoreRamResetState
    {
    cThaIpCoreRamResetStateOn,
    cThaIpCoreRamResetStateWait,
    cThaIpCoreRamResetStateOff,
    cThaIpCoreRamResetStateStableWait
    }eThaIpCoreRamResetState;

typedef enum eThaIpCoreRamResetWaitState
    {
    cThaIpCoreRamResetWaitStateStart,
    cThaIpCoreRamResetWaitStateWait
    }eThaIpCoreRamResetWaitState;

typedef enum eThaIpCoreSerdesResetState
    {
    cThaIpCoreSerdesResetStateOn,
    cThaIpCoreSerdesResetStateWait,
    cThaIpCoreSerdesResetStateOff,
    cThaIpCoreSerdesResetStateStableWait
    }eThaIpCoreSerdesResetState;
/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet RamResetTriggerOn(ThaIpCore self)
    {
    AtIpCore core = (AtIpCore)self;
    uint32 softResetRegister = mMethodsGet(self)->RamSoftResetRegister(self);

    /* Make reset */
    AtIpCoreWrite(core, softResetRegister, 0x1);

    return cAtOk;
    }

static eAtRet RamResetTriggerOff(ThaIpCore self)
    {
    AtIpCore core = (AtIpCore)self;
    uint32 softResetRegister = mMethodsGet(self)->RamSoftResetRegister(self);

    AtIpCoreWrite(core, softResetRegister, 0x0);

    return cAtOk;
    }

static uint32 IpCoreAsyncRamResetWaitStateGet(ThaIpCore self)
    {
    return self->asyncRamResetWaitState;
    }

static uint32 IpCoreAsyncRamResetWaitStateIncrease(ThaIpCore self)
    {
    return self->asyncRamResetWaitState++;
    }

static uint32 IpCoreAsyncRamResetWaitStateReset(ThaIpCore self)
    {
    return self->asyncRamResetWaitState = 0;
    }

static eAtRet RamResetWaitStateStart(ThaIpCore self)
    {
    AtOsalCurTimeGet(&self->asyncStartTime);
    AtDeviceAsyncInitNeedFixDelayTimeSet(AtIpCoreDeviceGet((AtIpCore)self), cRamResetWaitStateWaitInMs * cNumUsPerMs);
    IpCoreAsyncRamResetWaitStateIncrease(self);
    return cAtRetNeedDelay;
    }

static eAtRet RamResetWaitStateWait(ThaIpCore self)
    {
    eAtRet ret = cAtOk;
    tAtOsalCurTime curTime;
    uint32 diffTimeInMs;

    AtOsalCurTimeGet(&curTime);
    diffTimeInMs = AtOsalDifferenceTimeInMs(&curTime, &self->asyncStartTime);
    if (ThaIpCoreIsRamResetDone(self))
        {
	    AtDeviceAsyncInitNeedFixDelayTimeSet(AtIpCoreDeviceGet((AtIpCore)self), 0);
	    IpCoreAsyncRamResetWaitStateReset(self);
	    ret = cAtOk;
        }
    else if (diffTimeInMs >= cRamResetWaitStateWaitInMs)
        {
	    AtDeviceAsyncInitNeedFixDelayTimeSet(AtIpCoreDeviceGet((AtIpCore)self), 0);
        AtDriverLog(AtDriverSharedDriverGet(), cAtLogLevelCritical, "(%s, %d): RAM soft-reset waiting fail\r\n", AtSourceLocation);
        IpCoreAsyncRamResetWaitStateReset(self);
        ret = cAtErrorDevFail;
        }
    else
	    {
	    AtDeviceAsyncInitNeedFixDelayTimeSet(AtIpCoreDeviceGet((AtIpCore)self), (cRamResetWaitStateWaitInMs - diffTimeInMs) * cNumUsPerMs);
	    ret = cAtRetNeedDelay;
	    }
    return ret;
    }

static eAtRet AsyncRamSoftResetWait(ThaIpCore self)
    {
    uint32 state = IpCoreAsyncRamResetWaitStateGet(self);
    eAtRet ret = cAtOk;

    if (AtDeviceWarmRestoreIsStarted(AtIpCoreDeviceGet((AtIpCore)self)))
        return cAtOk;

    switch(state)
        {
        case cThaIpCoreRamResetWaitStateStart:
            ret = RamResetWaitStateStart(self);
            break;
        case cThaIpCoreRamResetWaitStateWait:
            ret = RamResetWaitStateWait(self);
            break;
        default:
            ret = cAtErrorDevFail;
            IpCoreAsyncRamResetWaitStateReset(self);
        }

    return ret;
    }

static eAtRet RamResetWaitStateWaitV2(ThaIpCore self)
    {
    eAtRet ret = cAtOk;
    tAtOsalCurTime curTime;
    uint32 diffTimeInMs;

    AtOsalCurTimeGet(&curTime);
    diffTimeInMs = AtOsalDifferenceTimeInMs(&curTime, &self->asyncStartTime);
    if (diffTimeInMs >= cRamResetWaitStateWaitInMs)
        {
	    AtDeviceAsyncInitNeedFixDelayTimeSet(AtIpCoreDeviceGet((AtIpCore)self), 0);
        IpCoreAsyncRamResetWaitStateReset(self);
        ret = cAtOk;
        }
    else
	    {
	    AtDeviceAsyncInitNeedFixDelayTimeSet(AtIpCoreDeviceGet((AtIpCore)self), (cRamResetWaitStateWaitInMs - diffTimeInMs) * cNumUsPerMs);
        ret = cAtRetNeedDelay;
	    }

    return ret;
    }

static eAtRet AsyncRamSoftResetWaitV2(ThaIpCore self)
    {
    uint32 state = IpCoreAsyncRamResetWaitStateGet(self);
    eAtRet ret = cAtOk;

    switch(state)
        {
        case cThaIpCoreRamResetWaitStateStart:
            ret = RamResetWaitStateStart(self);
            break;
        case cThaIpCoreRamResetWaitStateWait:
            ret = RamResetWaitStateWaitV2(self);
            break;
        default:
            ret = cAtErrorDevFail;
            IpCoreAsyncRamResetWaitStateReset(self);
        }

    return ret;
    }

static uint32 IpCoreSerdesAsyncResetStateGet(ThaIpCore self)
    {
    return self->asyncSerdesResetState;
    }

static uint32 IpCoreSerdesAsyncResetStateIncrease(ThaIpCore self)
    {
    return self->asyncSerdesResetState++;
    }

static uint32 IpCoreSerdesAsyncResetStateReset(ThaIpCore self)
    {
    return self->asyncSerdesResetState = 0;
    }

static eAtRet SerdesResetStateOn(ThaIpCore self)
    {
    AtIpCore core = (AtIpCore)self;

    AtIpCoreWrite(core, cThaSoftResetReg2, 0x7);
    IpCoreSerdesAsyncResetStateIncrease(self);
    AtOsalCurTimeGet(&self->asyncStartTime);
    AtDeviceAsyncInitNeedFixDelayTimeSet(AtIpCoreDeviceGet((AtIpCore)self), cSerdesResetStateWaitTimeInMs * cNumUsPerMs);
    return cAtRetNeedDelay;
    }

static eAtRet SerdesResetStateWait(ThaIpCore self)
    {
    eAtRet ret = cAtOk;
    tAtOsalCurTime curTime;
    uint32 diffTimeInMs;

    AtOsalCurTimeGet(&curTime);
    diffTimeInMs = AtOsalDifferenceTimeInMs(&curTime, &self->asyncStartTime);
    if (diffTimeInMs >= cSerdesResetStateWaitTimeInMs)
        {
        IpCoreSerdesAsyncResetStateIncrease(self);
        AtDeviceAsyncInitNeedFixDelayTimeSet(AtIpCoreDeviceGet((AtIpCore)self), 0);
        ret = cAtErrorAgain;
        }
    else
        {
        AtDeviceAsyncInitNeedFixDelayTimeSet(AtIpCoreDeviceGet((AtIpCore)self), (cSerdesResetStateWaitTimeInMs - diffTimeInMs) * cNumUsPerMs);
        ret = cAtRetNeedDelay;
        }

    return ret;
    }

static eAtRet SerdesResetStateOff(ThaIpCore self)
    {
    AtIpCore core = (AtIpCore)self;

    AtIpCoreWrite(core, cThaSoftResetReg2, 0);
    IpCoreSerdesAsyncResetStateIncrease(self);
    AtOsalCurTimeGet(&self->asyncStartTime);
    AtDeviceAsyncInitNeedFixDelayTimeSet(AtIpCoreDeviceGet((AtIpCore)self), cSerdesResetStateWaitTimeInMs * cNumUsPerMs);
    return cAtRetNeedDelay;
    }

static eAtRet SerdesResetStateStableWait(ThaIpCore self)
    {
    eAtRet ret = cAtOk;
    tAtOsalCurTime curTime;
    uint32 diffTimeInMs;

    AtOsalCurTimeGet(&curTime);
    diffTimeInMs = AtOsalDifferenceTimeInMs(&curTime, &self->asyncStartTime);
    if (diffTimeInMs >= cSerdesResetStateWaitTimeInMs)
        {
        IpCoreSerdesAsyncResetStateReset(self);
        AtDeviceAsyncInitNeedFixDelayTimeSet(AtIpCoreDeviceGet((AtIpCore)self), 0);
        ret = cAtOk;
        }
    else
        {
        AtDeviceAsyncInitNeedFixDelayTimeSet(AtIpCoreDeviceGet((AtIpCore)self), (cSerdesResetStateWaitTimeInMs - diffTimeInMs) * cNumUsPerMs);
        ret = cAtRetNeedDelay;
        }
    return ret;
    }

static eAtRet AsyncSerdesSoftReset(ThaIpCore self)
    {
    uint32 state = IpCoreSerdesAsyncResetStateGet(self);
    eAtRet ret = cAtOk;

    switch(state)
        {
        case cThaIpCoreSerdesResetStateOn:
            ret = SerdesResetStateOn(self);
            break;
        case cThaIpCoreSerdesResetStateWait:
            ret = SerdesResetStateWait(self);
            break;
        case cThaIpCoreSerdesResetStateOff:
            ret = SerdesResetStateOff(self);
            break;
        case cThaIpCoreSerdesResetStateStableWait:
            ret = SerdesResetStateStableWait(self);
            break;
        default:
            ret = cAtErrorDevFail;
            IpCoreSerdesAsyncResetStateReset(self);
        }

    return ret;
    }

static uint32 IpCoreRamAsyncResetStateGet(ThaIpCore self)
    {
    return self->asyncRamResetState;
    }

static uint32 IpCoreRamAsyncResetStateIncrease(ThaIpCore self)
    {
    return self->asyncRamResetState++;
    }

static uint32 IpCoreRamAsyncResetStateReset(ThaIpCore self)
    {
    return self->asyncRamResetState = 0;
    }

static eAtRet RamResetStateOn(ThaIpCore self)
    {
    eAtRet ret = RamResetTriggerOn(self);
    if (ret == cAtOk)
        {
        IpCoreRamAsyncResetStateIncrease(self);
        ret = cAtErrorAgain;
        }
    else if (!AtDeviceAsyncRetValIsInState(ret))
        IpCoreRamAsyncResetStateReset(self);

    return ret;
    }

static eAtRet RamResetStateWait(ThaIpCore self)
    {
    eAtRet ret = mMethodsGet(self)->AsyncRamSoftResetWait(self);
    if (ret == cAtOk)
        {
        IpCoreRamAsyncResetStateIncrease(self);
        ret = cAtErrorAgain;
        }
    else if (!AtDeviceAsyncRetValIsInState(ret))
        IpCoreRamAsyncResetStateReset(self);

    return ret;
    }

static eAtRet RamResetStateOff(ThaIpCore self)
    {
    eAtRet ret = RamResetTriggerOff(self);
    if (ret == cAtOk)
        {
        IpCoreRamAsyncResetStateIncrease(self);
        AtOsalCurTimeGet(&self->asyncStartTime);
        AtDeviceAsyncInitNeedFixDelayTimeSet(AtIpCoreDeviceGet((AtIpCore)self), mMethodsGet(mThis(self))->RamStableWaitInMs(mThis(self)) * cNumUsPerMs);
        ret = cAtRetNeedDelay;
        }
    else if (!AtDeviceAsyncRetValIsInState(ret))
        IpCoreRamAsyncResetStateReset(self);

    return ret;
    }

static eAtRet RamResetStateStableWait(ThaIpCore self)
    {
    uint32 stableWaitTimeInUs = mMethodsGet(mThis(self))->RamStableWaitInMs(mThis(self));
    eAtRet ret = cAtOk;
    tAtOsalCurTime curTime;
    uint32 diffTimeInMs;

    AtOsalCurTimeGet(&curTime);
    diffTimeInMs = AtOsalDifferenceTimeInMs(&curTime, &self->asyncStartTime);
    if (diffTimeInMs >= stableWaitTimeInUs)
        {
        IpCoreRamAsyncResetStateReset(self);
        AtDeviceAsyncInitNeedFixDelayTimeSet(AtIpCoreDeviceGet((AtIpCore)self), 0);
        ret = cAtOk;
        }
    else
        {
        AtDeviceAsyncInitNeedFixDelayTimeSet(AtIpCoreDeviceGet((AtIpCore)self), (stableWaitTimeInUs - diffTimeInMs) * cNumUsPerMs);
            ret = cAtRetNeedDelay;
        }

    return ret;
    }

eAtRet ThaIpCoreAsyncRamSoftReset(ThaIpCore self)
    {
    uint32 state = IpCoreRamAsyncResetStateGet(self);
    eAtRet ret = cAtOk;

    switch(state)
        {
        case cThaIpCoreRamResetStateOn:
            ret = RamResetStateOn(self);
            break;
        case cThaIpCoreRamResetStateWait:
            ret = RamResetStateWait(self);
            break;
        case cThaIpCoreRamResetStateOff:
            ret = RamResetStateOff(self);
            break;
        case cThaIpCoreRamResetStateStableWait:
            ret = RamResetStateStableWait(self);
            break;
        default:
            ret = cAtErrorDevFail;
            IpCoreRamAsyncResetStateReset(self);
        }

    return ret;
    }

static uint32 IpCoreAsyncResetStateGet(ThaIpCore self)
    {
    return self->asyncResetState;
    }

static uint32 IpCoreAsyncResetStateIncrease(ThaIpCore self)
    {
    return self->asyncResetState++;
    }

static uint32 IpCoreAsyncResetStateReset(ThaIpCore self)
    {
    return self->asyncResetState = 0;
    }

eAtRet ThaIpCoreAsyncReset(AtIpCore self)
    {
    eAtRet ret;
    uint32 state;
    ThaIpCore thaCore = (ThaIpCore)self;
    AtDevice device = AtIpCoreDeviceGet(self);
    tAtOsalCurTime profileTime;
    char stateString[32];

    state = IpCoreAsyncResetStateGet(thaCore);

    AtOsalCurTimeGet(&profileTime);
    AtSprintf(stateString, "state: %d", state);

    switch (state)
        {
        case cThaIpCoreAsyncResetStateRam:
            ret  = mMethodsGet(mThis(self))->AsyncRamSoftReset(mThis(self));
            AtDeviceAccessTimeCountSinceLastProfileCheck(device, cAtTrue, AtSourceLocation, stateString);
            AtDeviceAccessTimeInMsSinceLastProfileCheck(device, cAtTrue, &profileTime, AtSourceLocation, stateString);
            if (ret == cAtOk)
                {
                IpCoreAsyncResetStateIncrease(thaCore);
                ret = cAtErrorAgain;
                }
            else if (!AtDeviceAsyncRetValIsInState(ret))
                IpCoreAsyncResetStateReset(thaCore);
            break;
        case cThaIpCoreAsyncResetStateSerdes:
            ret = mMethodsGet(mThis(self))->AsyncSerdesSoftReset(mThis(self));
            AtDeviceAccessTimeCountSinceLastProfileCheck(device, cAtTrue, AtSourceLocation, stateString);
            AtDeviceAccessTimeInMsSinceLastProfileCheck(device, cAtTrue, &profileTime, AtSourceLocation, stateString);
            if ((ret == cAtOk) || (!AtDeviceAsyncRetValIsInState(ret)))
                IpCoreAsyncResetStateReset(thaCore);
            break;
        default:
            ret = cAtErrorDevFail;
            IpCoreAsyncResetStateReset(thaCore);
        }

    return ret;
    }

eAtRet ThaIpCoreAsyncRamSoftResetWait(ThaIpCore self)
    {
    return AsyncRamSoftResetWait(self);
    }

eAtRet ThaIpCoreAsyncSerdesSoftReset(ThaIpCore self)
    {
    return AsyncSerdesSoftReset(self);
    }

eAtRet ThaIpCoreAsyncRamSoftResetWaitV2(ThaIpCore self)
    {
    return AsyncRamSoftResetWaitV2(self);
    }

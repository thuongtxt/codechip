/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : ThaDeviceAsyncInit.h
 * 
 * Created Date: Aug 16, 2016
 *
 * Description : To handle async init
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THADEVICEASYNCINIT_H_
#define _THADEVICEASYNCINIT_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet ThaDeviceAsyncInit(AtDevice self);
eAtRet ThaDeviceSupperAsyncInit(AtDevice self);
void ThaDeviceEntranceToInit(AtDevice self);
void ThaDevicePostInit(AtDevice self);
eAtRet ThaDeviceSuperAllModulesAsyncInit(AtDevice self);
eAtRet ThaDeviceAllModulesAsyncInit(AtDevice self);

#ifdef __cplusplus
}
#endif
#endif /* _THADEVICEASYNCINIT_H_ */


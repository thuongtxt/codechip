/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : ThaDeviceReg.h
 * 
 * Created Date: Oct 3, 2012
 *
 * Author      : nguyennt
 * 
 * Description : Device registers
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THADEVICEREG_H_
#define _THADEVICEREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
#define cThaRegOcnNumberSlice  2

/* The VT maximum value in one STS. */
#define cThaOcnNumVtInSts      28

/* The number of STS in OC12 line */
#define cThaOcnNumStsInOc12    12

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#define cThaSoftResetReg2 0xF00040

/*------------------------------------------------------------------------------
Reg Name: Version
Reg Addr: 0x2
Reg Desc: FPGA version
------------------------------------------------------------------------------*/
#define cThaRegChipVersion 0x2

#define cThaRegChipVersionMajorMask  cBit31_24
#define cThaRegChipVersionMajorShift 24
#define cThaRegChipVersionMinorMask  cBit23_16
#define cThaRegChipVersionMinorShift 16
#define cThaRegChipVersionBuildMask  cBit15_0
#define cThaRegChipVersionBuildShift 0

#define mThaVersionBuild(major, minor, build)                                   \
    ((((uint32)(major) << cThaRegChipVersionMajorShift) & cThaRegChipVersionMajorMask) | \
     (((uint32)(minor) << cThaRegChipVersionMinorShift) & cThaRegChipVersionMinorMask) | \
     (((uint32)(build) << cThaRegChipVersionBuildShift) & cThaRegChipVersionBuildMask))

#define cThaFirstNibleMask      cBit15_12
#define cThaFirstNibleShift     12
#define cThaSecondNibleMask     cBit11_8
#define cThaSecondNibleShift    8
#define cThaMinorMask           cBit7_0
#define cThaMinorShift          0

#define mThaHwBuiltNumberBuild(firstNible, secondNible, minor)                          \
    ((((uint32)(firstNible) << cThaFirstNibleShift) & cThaFirstNibleMask) |             \
     (((uint32)(secondNible) << cThaSecondNibleShift) & cThaSecondNibleMask) |          \
     (((uint32)(minor) << cThaMinorShift) & cThaMinorMask))

/*------------------------------------------------------------------------------
Reg Name: Release date
Reg Addr: 0x3
Reg Desc: FPGA release date
------------------------------------------------------------------------------*/
#define cThaRegChipReleaseDate 0x3

#define cThaRegChipReleaseDateYearMask             cBit31_24
#define cThaRegChipReleaseDateYearShift            24
#define cThaRegChipReleaseDateMonthMask            cBit23_16
#define cThaRegChipReleaseDateMonthShift           16
#define cThaRegChipReleaseDateDayMask              cBit15_8
#define cThaRegChipReleaseDateDayShift             8
#define cThaRegChipReleaseDateMask                 cBit31_8
#define cThaRegChipReleaseDateShift                8
#define cThaRegChipReleaseDateInternalReleaseMask  cBit7_0
#define cThaRegChipReleaseDateInternalReleaseShift 0

/*------------------------------------------------------------------------------
Reg Name: Chip Active Control
Reg Addr: 0x000001
Reg Desc: This register is used to configure global signal for CHIP
------------------------------------------------------------------------------*/
#define cThaRegChipActiveControl 0x000001

#define cThaGlbRegBmtPactMask  cBit4
#define cThaGlbRegBmtPactShift 4

#define cThaGlbRegPohPactMask  cBit0
#define cThaGlbRegPohPactShift 0

/* GLOBAL */
#define cThaRegChipIntrSta                              0x000002
#define cThaRegChipIntrCtrl                             0x000003
#define cThaRegChipIntrRestore                          0x000004

#define cThaRegChipIntrStaRxLineOcnIntAlarm             cBit0
#define cThaRegChipIntrStaRxSTSOcnIntAlarm              cBit1
#define cThaRegChipIntrStaRxVTOcnIntAlarm               cBit2
#define cThaRegChipIntrStaTxSTSVTOcnIntAlarm            cBit3
#define cThaRegChipIntrStaPDHFullIntAlarm               cBit18
#define cThaRegChipIntrStaPDHSTSVTIntAlarm              cBit19
#define cThaRegChipIntrStaPDHDS3E3IntAlarm              cBit20
#define cThaRegChipIntrStaPDHDS2E2IntAlarm              cBit21
#define cThaRegChipIntrStaPDHDS1E1IntAlarm              cBit22

#define cThaRegChipIntrStaSdhModuleAlarm (cThaRegChipIntrStaRxLineOcnIntAlarm |  \
                                          cThaRegChipIntrStaRxSTSOcnIntAlarm  |  \
                                          cThaRegChipIntrStaRxVTOcnIntAlarm   |  \
                                          cThaRegChipIntrStaTxSTSVTOcnIntAlarm)

#define cThaRegChipIntrStaPdhModuleAlarm (cThaRegChipIntrStaPDHFullIntAlarm |    \
                                          cThaRegChipIntrStaPDHSTSVTIntAlarm |   \
                                          cThaRegChipIntrStaPDHDS3E3IntAlarm |   \
                                          cThaRegChipIntrStaPDHDS2E2IntAlarm |   \
                                          cThaRegChipIntrStaPDHDS1E1IntAlarm)

/*------------------------------------------------------------------------------
Reg Name: OCN Rx STS/VC Per Slice Interrupt OR Status
Reg Addr: 0x00050002
Reg Desc: The register consists of 2 bits for 2 STS-12 slice in the STS/VC
          Pointer interpreter block. Each bit is used to store Interrupt OR
          status of the related slice.
------------------------------------------------------------------------------*/
#define cThaRegOcnRxStsVCPerSliceIntrOrStat         (0x050002)
#define cThaRegOcnRxStsVcPerSliceIntrMask(sliceId)   (cBit0 << sliceId)

/*------------------------------------------------------------------------------
Reg Name: OCN Rx VT/TU Per Slice Interrupt OR Status
Reg Addr: 0x00050003
Reg Desc: The register consists of 2 bits for 2 STS-12 slice in the VT/TU
          Pointer interpreter block. Each bit is used to store Interrupt OR
          status of the related slice.
------------------------------------------------------------------------------*/
#define cThaRegOcnRxVtTUPerSliceIntrOrStat          (0x050003)
#define cThaRegOcnRxVtTUPerSliceIntrMask(sliceId)   (cBit0 << sliceId)

/* Clock checking registers */
#define cThaPllClkSysStatReg      0xF0000B
#define cThaPllClkSysStatBitMask  cBit1

#ifdef __cplusplus
}
#endif
#endif /* _THADEVICEREG_H_ */


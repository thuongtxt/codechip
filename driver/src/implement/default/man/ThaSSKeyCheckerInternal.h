/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : ThaSSKeyCheckerInternal.h
 * 
 * Created Date: Sep 18, 2013
 *
 * Description : SSKey checker
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASSKEYCHECKERINTERNAL_H_
#define _THASSKEYCHECKERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/man/AtSSKeyCheckerInternal.h"
#include "ThaSSKeyChecker.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaSSKeyCheckerPpp
    {
    tAtSSKeyChecker super;
    }tThaSSKeyCheckerPpp;

typedef struct tThaSSKeyCheckerPwV2
    {
    tAtSSKeyChecker super;
    }tThaSSKeyCheckerPwV2;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSSKeyChecker ThaSSKeyCheckerPppObjectInit(AtSSKeyChecker self, AtDevice device);
AtSSKeyChecker ThaSSKeyCheckerPwV2ObjectInit(AtSSKeyChecker self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THASSKEYCHECKERINTERNAL_H_ */


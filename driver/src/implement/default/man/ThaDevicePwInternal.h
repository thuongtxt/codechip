/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : ThaDevicePwInternal.h
 * 
 * Created Date: Apr 2, 2013
 *
 * Description : PW common device
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THADEVICEPWINTERNAL_H_
#define _THADEVICEPWINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaDeviceInternal.h"
#include "ThaDevice.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaDevicePw
    {
    tThaDevice super;

    /* Private data */
    }tThaDevicePw;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDevice ThaDevicePwObjectInit(AtDevice self, AtDriver driver, uint32 productCode);

#ifdef __cplusplus
}
#endif
#endif /* _THADEVICEPWINTERNAL_H_ */


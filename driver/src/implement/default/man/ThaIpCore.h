/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : ThaIpCore.h
 * 
 * Created Date: Dec 3, 2012
 *
 * Description : Thalassa IP core
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAIPCORE_H_
#define _THAIPCORE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtIpCore.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaIpCore *ThaIpCore;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Default concretes */
AtIpCore ThaIpCoreNew(uint8 coreId, AtDevice device);
AtIpCore ThaIpCorePwNew(uint8 coreId, AtDevice device);
AtIpCore ThaIpCorePwV2New(uint8 coreId, AtDevice device);

eBool ThaIpCorePllIsLocked(AtIpCore core);
eAtRet ThaIpCorePohDeactivate(ThaIpCore self);

void ThaIpCoreRamSoftResetWaitTimeMsSet(ThaIpCore self, uint32 waitTimeMs);
uint32 ThaIpCoreRamSoftResetWaitTimeMsGet(ThaIpCore self);

/* Product concretes */
AtIpCore Tha60030111IpCoreNew(uint8 coreId, AtDevice device);
AtIpCore Tha60070013IpCoreNew(uint8 coreId, AtDevice device);
AtIpCore Tha60070023IpCoreNew(uint8 coreId, AtDevice device);
AtIpCore Tha60071011IpCoreNew(uint8 coreId, AtDevice device);
AtIpCore Tha60290051IpCoreNew(uint8 coreId, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAIPCORE_H_ */


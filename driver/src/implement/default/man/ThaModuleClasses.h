/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : ThaModuleClasses.h
 * 
 * Created Date: Apr 6, 2015
 *
 * Description : Private module classes
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULECLASSES_H_
#define _THAMODULECLASSES_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleMpig    * ThaModuleMpig;
typedef struct tThaModuleMpeg    * ThaModuleMpeg;
typedef struct tThaModulePmcMpeg * ThaModulePmcMpeg;
typedef struct tThaModulePmcMpig * ThaModulePmcMpig;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THAMODULECLASSES_H_ */


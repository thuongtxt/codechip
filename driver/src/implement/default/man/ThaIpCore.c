/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : ThaIpCore.c
 *
 * Created Date: Oct 16, 2012
 *
 * Description : IP Core of Thalassa product
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../util/coder/AtCoderUtil.h"
#include "../../../generic/physical/AtThermalSensorInternal.h"
#include "../../../generic/physical/AtSemControllerInternal.h"
#include "ThaIpCoreInternal.h"
#include "ThaDeviceReg.h"
#include "ThaDeviceInternal.h"
#include "AtModule.h"
#include "../../../generic/ram/AtRamInternal.h"
#include "../../../generic/ram/AtModuleRamInternal.h"
#include "ThaIpCoreAsyncInit.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaIpCore)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

static tThaIpCoreMethods m_methods;

/* To override super implementation (require if need to override some methods) */
static tAtIpCoreMethods m_AtIpCoreOverride;
static tAtObjectMethods m_AtObjectOverride;

/* Reference to super implementation (option) */
static const tAtIpCoreMethods *m_AtIpCoreImplement;
static const tAtObjectMethods *m_AtObjectMethods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool IsSimulated(AtIpCore core)
    {
    return AtDeviceIsSimulated(AtIpCoreDeviceGet(core));
    }

/* Delete */
static void Delete(AtObject self)
    {
    ThaIpCore ipCore = (ThaIpCore)self;

    AtObjectDelete((AtObject)(ipCore->intrController));
    ipCore->intrController = NULL;

    /* Call super function to fully delete */
    m_AtObjectMethods->Delete((AtObject)self);
    }

static eAtRet RamSoftResetWait(ThaIpCore self)
    {
    uint32 elapseTime;
    tAtOsalCurTime curTime;
    tAtOsalCurTime startTime;
    AtOsal osal = AtSharedDriverOsalGet();
    static const uint32 timeoutMs = 1000; /* 1s just for the worst case,
                                             hardware just needs about 60ms to reset */

    if (AtDeviceWarmRestoreIsStarted(AtIpCoreDeviceGet((AtIpCore)self)))
        return cAtOk;

    /* Timeout wait for hardware */
    elapseTime = 0;
    mMethodsGet(osal)->CurTimeGet(osal, &startTime);
    while (elapseTime < timeoutMs)
        {
        if (ThaIpCoreIsRamResetDone(self))
            return cAtOk;

        /* Get current time */
        mMethodsGet(osal)->CurTimeGet(osal, &curTime);

        /* Calculate elapse time */
        elapseTime = mTimeIntervalInMsGet(startTime, curTime);
        }

    AtDriverLog(AtDriverSharedDriverGet(), cAtLogLevelCritical, "(%s, %d): RAM soft-reset waiting fail\r\n", AtSourceLocation);

    return cAtErrorDevFail;
    }

static eAtRet AsyncRamSoftResetWait(ThaIpCore self)
    {
    return ThaIpCoreAsyncRamSoftResetWait(self);
    }

static uint32 RamStableWaitInMs(ThaIpCore self)
    {
	AtUnused(self);
    return 20;
    }

static uint32 RamSoftResetRegister(ThaIpCore self)
    {
	AtUnused(self);
    return 0xF00004;
    }

static eAtRet RamSoftReset(ThaIpCore self)
    {
    AtIpCore core = (AtIpCore)self;
    eAtRet ret = cAtOk;
    AtOsal osal;
    uint32 stableWaitTimeInUs;
    uint32 softResetRegister = mMethodsGet(self)->RamSoftResetRegister(self);

    /* Make reset */
    AtIpCoreWrite(core, softResetRegister, 0x1);
    ret = mMethodsGet(self)->RamSoftResetWait(self);
    AtIpCoreWrite(core, softResetRegister, 0x0);

    /* Give hardware a rest */
    osal = AtSharedDriverOsalGet();
    stableWaitTimeInUs = mMethodsGet(mThis(self))->RamStableWaitInMs(mThis(self)) * 1000;

    if (!IsSimulated(core))
        mMethodsGet(osal)->USleep(osal, stableWaitTimeInUs);

    return ret;
    }

static eAtRet AsyncRamSoftReset(ThaIpCore self)
    {
    return ThaIpCoreAsyncRamSoftReset(self);
    }

static eAtRet SerdesSoftReset(ThaIpCore self)
    {
    AtIpCore core = (AtIpCore)self;

    AtIpCoreWrite(core, cThaSoftResetReg2, 0x7);
    AtOsalUSleep(500000);
    AtIpCoreWrite(core, cThaSoftResetReg2, 0);
    AtOsalUSleep(500000);

    return cAtOk;
    }

static eAtRet AsyncSerdesSoftReset(ThaIpCore self)
    {
    return ThaIpCoreAsyncSerdesSoftReset(self);
    }

static eAtRet SoftReset(AtIpCore self)
    {
    eAtRet ret = cAtOk;

    if (AtDeviceWarmRestoreIsStarted(AtIpCoreDeviceGet(self)))
        return cAtOk;

    ret |= mMethodsGet(mThis(self))->RamSoftReset(mThis(self));
    ret |= mMethodsGet(mThis(self))->SerdesSoftReset(mThis(self));

    return ret;
    }

static eAtRet AsyncReset(AtIpCore self)
    {
    return ThaIpCoreAsyncReset(self);
    }

static eAtRet AsyncSoftReset(AtIpCore self)
    {
    return AsyncReset(self);
    }

static eBool FastRamTestIsEnabled(ThaIpCore self)
    {
    AtUnused(self);
    /* Do no testing as default, concrete product will decide to test by it self */
    return cAtFalse;
    }

static eAtRet FastRamTest(ThaIpCore self)
    {
    AtDevice device = AtIpCoreDeviceGet((AtIpCore)self);
    AtModuleRam moduleRam = (AtModuleRam)AtDeviceModuleGet(device, cAtModuleRam);
    uint8       numDdr = AtModuleRamNumDdrGet(moduleRam);
    uint8       ddrIndex;
    eAtRet      ret;
    AtRam       ram;
    uint32      firstErrorAddress;
    static const uint32 startAddr = 0x12345;
    static const uint32 numTestAddr = 100;

    if (IsSimulated((AtIpCore)self))
        return cAtOk;

    for (ddrIndex = 0; ddrIndex < numDdr; ddrIndex++)
        {
        ram = AtModuleRamDdrGet(moduleRam, ddrIndex);

        /* If this ram belong to this core, test it */
        if (AtRamIpCoreGet(ram) != (AtIpCore)self)
            continue;

        ret = AtRamInitStatusGet(ram);
        if (ret != cAtOk)
            return ret;

        ret = AtRamMemoryTest(ram, startAddr, startAddr + numTestAddr, &firstErrorAddress);
        if (ret != cAtOk)
            return ret;

        /* Fill all memory to 0 */
        ret = AtRamMemoryFill(ram, 0x0);
        if (ret != cAtOk)
            return ret;
        }

    return cAtOk;
    }

static eAtRet Test(AtIpCore self)
    {
    eAtRet ret = cAtOk;

    if (AtDeviceWarmRestoreIsStarted(AtIpCoreDeviceGet(self)))
        return cAtOk;

    if (!ThaIpCorePllIsLocked(self))
        {
        AtDevice device = AtIpCoreDeviceGet(self);
        mDeviceError(device, cAtErrorDevicePllNotLocked);
        }

    if (!mMethodsGet(mThis(self))->FastRamTestIsEnabled(mThis(self)))
        return cAtOk;

    ret |= FastRamTest(mThis(self));
    ret |= AtIpCoreSoftReset(self);

    return ret;
    }

static ThaIntrController IntrControllerGet(ThaIpCore self)
    {
    AtIpCore core = (AtIpCore)self;

    if (self->intrController == NULL)
        self->intrController = ThaDeviceIntrControllerCreate((ThaDevice)AtIpCoreDeviceGet(core), core);;

    return self->intrController;
    }

static uint32 InterruptDisable(ThaIpCore self)
    {
    return ThaIpCoreInterruptDisable((ThaIpCore)IntrControllerGet(self));
    }

static eBool SdhCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    return ThaIpCoreSdhCauseInterrupt((ThaIpCore)IntrControllerGet(self), intrStatus);
    }

static eBool PdhCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    return ThaIpCorePdhCauseInterrupt((ThaIpCore)IntrControllerGet(self), intrStatus);
    }

static eBool SurCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    return ThaIpCoreSurCauseInterrupt((ThaIpCore)IntrControllerGet(self), intrStatus);
    }

static eBool EthCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    return ThaIpCoreEthCauseInterrupt((ThaIpCore)IntrControllerGet(self), intrStatus);
    }

static eBool ThaIpCorePppCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    if (self)
        return mMethodsGet(self)->PppCauseInterrupt(self, intrStatus);

    return cAtFalse;
    }

static eBool PppCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    return ThaIpCorePppCauseInterrupt((ThaIpCore)IntrControllerGet(self), intrStatus);
    }

static eBool PwCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    return ThaIpCorePwCauseInterrupt((ThaIpCore)IntrControllerGet(self), intrStatus);
    }

static eBool RamCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    return ThaIpCoreRamCauseInterrupt((ThaIpCore)IntrControllerGet(self), intrStatus);
    }

static eBool PtpCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    return ThaIpCorePtpCauseInterrupt((ThaIpCore)IntrControllerGet(self), intrStatus);
    }

static eBool ConcateCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    return ThaIpCoreConcateCauseInterrupt((ThaIpCore)IntrControllerGet(self), intrStatus);
    }

static eBool EncapCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    return ThaIpCoreEncapCauseInterrupt((ThaIpCore)IntrControllerGet(self), intrStatus);
    }

static eBool ThermalCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    return ThaIpCoreThermalCauseInterrupt((ThaIpCore)IntrControllerGet(self), intrStatus);
    }

static eBool SemCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    return ThaIpCoreSemCauseInterrupt((ThaIpCore)IntrControllerGet(self), intrStatus);
    }

static eBool ClockCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    return ThaIpCoreClockCauseInterrupt((ThaIpCore)IntrControllerGet(self), intrStatus);
    }

static eBool ApsCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    return ThaIpCoreApsCauseInterrupt((ThaIpCore)IntrControllerGet(self), intrStatus);
    }

static uint32 InterruptStatusGet(ThaIpCore self)
    {
    return ThaIpCoreInterruptStatusGet((ThaIpCore)IntrControllerGet(self));
    }

static void AllModulesInterruptEnable(ThaIpCore self, uint32 intrEnMask)
    {
    ThaIpCoreAllModulesInterruptEnable((ThaIpCore)IntrControllerGet(self), intrEnMask);
    }

static void PdhHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    ThaIpCorePdhHwInterruptEnable((ThaIpCore)IntrControllerGet(self), enable);
    }

static void PhysicalHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    ThaIpCorePhysicalHwInterruptEnable((ThaIpCore)IntrControllerGet(self), enable);
    }

static void SdhHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    ThaIpCoreSdhHwInterruptEnable((ThaIpCore)IntrControllerGet(self), enable);
    }

static void PppHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    ThaIpCorePppHwInterruptEnable((ThaIpCore)IntrControllerGet(self), enable);
    }

static void PwHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    ThaIpCorePwHwInterruptEnable((ThaIpCore)IntrControllerGet(self), enable);
    }

static void SurHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    ThaIpCoreSurHwInterruptEnable((ThaIpCore)IntrControllerGet(self), enable);
    }

static void RamHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    ThaIpCoreRamHwInterruptEnable((ThaIpCore)IntrControllerGet(self), enable);
    }

static void PtpHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    ThaIpCorePtpHwInterruptEnable((ThaIpCore)IntrControllerGet(self), enable);
    }

static void ClockHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    ThaIpCoreClockHwInterruptEnable((ThaIpCore)IntrControllerGet(self), enable);
    }

static void ApsHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    ThaIpCoreApsHwInterruptEnable((ThaIpCore)IntrControllerGet(self), enable);
    }

static void ConcateHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    ThaIpCoreConcateHwInterruptEnable((ThaIpCore)IntrControllerGet(self), enable);
    }

static void EncapHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    ThaIpCoreEncapHwInterruptEnable((ThaIpCore)IntrControllerGet(self), enable);
    }

static void HwInterruptEnable(ThaIpCore self, uint32 intrEnMask, eBool enable)
    {
    ThaIpCoreHwInterruptEnable((ThaIpCore)IntrControllerGet(self), intrEnMask, enable);
    }

static eAtRet InterruptEnableWithCache(AtIpCore self, eBool enable)
    {
    /* Core has a global register, each module has one bit to enable/disable interrupt on this register,
     * Modules control these bits, so let each module enables by itself
     */
    if (enable)
        {
        AtDevice device = AtIpCoreDeviceGet((AtIpCore)self);
        AtModule module;
        uint8 i, numModule;
        const eAtModule *allModules = AtDeviceAllSupportedModulesGet(device, &numModule);

        if (allModules == NULL)
            return cAtErrorNullPointer;

        for (i = 0; i < numModule; i++)
            {
            module = AtDeviceModuleGet(device, allModules[i]);
            if (module)
                mMethodsGet(module)->InterruptOnIpCoreEnable(module, enable, self);
            }
        }

    else /* When disable, just simply turn off (write 0) all bits */
        ThaIpCoreInterruptDisable((ThaIpCore)self);

    /* Cache it */
    ((ThaIpCore)self)->interruptIsEnabled = enable;

    return cAtOk;
    }

static eAtRet InterruptEnable(AtIpCore self, eBool enable)
    {
    if (mMethodsGet(mThis(self))->CanEnableHwInterruptPin(mThis(self)))
        return AtIpCoreInterruptEnable((AtIpCore)IntrControllerGet((ThaIpCore)self), enable);
    else
        return InterruptEnableWithCache(self, enable);
    }

static eBool InterruptIsEnabled(AtIpCore self)
    {
    if (mMethodsGet(mThis(self))->CanEnableHwInterruptPin(mThis(self)))
        return AtIpCoreInterruptIsEnabled((AtIpCore)IntrControllerGet((ThaIpCore)self));
    else
        return ((ThaIpCore)self)->interruptIsEnabled;
    }

static void InterruptProfile(ThaIpCore self, eBool interruptHappened)
    {
    AtIpCore core = (AtIpCore)self;

    if (interruptHappened)
        {
        self->interruptCount = self->interruptCount + 1;
        self->interruptContiguousCount = self->interruptContiguousCount + 1;

        AtIpCoreInterruptHappenedNotify(core);
        }

    else
        {
        /* It has just been resolved */
        if (self->interruptHappened)
            AtIpCoreInterruptResolvedNotify(core);
        self->interruptContiguousCount = 0;
        }

    self->interruptHappened = interruptHappened;
    }

static eAtRet SemInterruptEnable(AtIpCore self, eBool enable)
    {
    return AtIpCoreSemInterruptEnable((AtIpCore)IntrControllerGet((ThaIpCore)self), enable);
    }

static eBool SemInterruptIsEnabled(AtIpCore self)
    {
    return AtIpCoreSemInterruptIsEnabled((AtIpCore)IntrControllerGet((ThaIpCore)self));
    }

static eAtRet SysmonInterruptEnable(AtIpCore self, eBool enable)
    {
    return AtIpCoreSysmonInterruptEnable((AtIpCore)IntrControllerGet((ThaIpCore)self), enable);
    }

static eBool SysmonInterruptIsEnabled(AtIpCore self)
    {
    return AtIpCoreSysmonInterruptIsEnabled((AtIpCore)IntrControllerGet((ThaIpCore)self));
    }

static void InterruptProcess(ThaIpCore self)
    {
    uint32 intrStatus;
    AtModule module;
    AtIpCore ipCore = (AtIpCore)self;
    AtDevice device = AtIpCoreDeviceGet(ipCore);
    uint32 interruptEnMask;
    eBool interruptHappened = cAtFalse;

    /* Get global interrupt status */
    intrStatus = ThaIpCoreInterruptStatusGet(self);

    /* Disable global interrupt */
    interruptEnMask = ThaIpCoreInterruptDisable(self);

    if (intrStatus)
        self->interruptCount++;

    /* Find which module cause interrupt and let that module process */
    /* SDH module */
    module = AtDeviceModuleGet(device, cAtModuleSdh);
    if ((module) && AtModuleInterruptIsEnabled(module) && ThaIpCoreSdhCauseInterrupt(self, intrStatus))
        {
        AtModuleInterruptProcess(module, intrStatus, ipCore);
        interruptHappened = cAtTrue;
        }

    /* PDH module */
    module = AtDeviceModuleGet(device, cAtModulePdh);
    if ((module) && AtModuleInterruptIsEnabled(module) && ThaIpCorePdhCauseInterrupt(self, intrStatus))
        {
        AtModuleInterruptProcess(module, intrStatus, ipCore);
        interruptHappened = cAtTrue;
        }

    /* PPP module */
    module = AtDeviceModuleGet(device, cAtModulePpp);
    if ((module) && AtModuleInterruptIsEnabled(module) && ThaIpCorePppCauseInterrupt(self, intrStatus))
        {
        AtModuleInterruptProcess(module, intrStatus, ipCore);
        interruptHappened = cAtTrue;
        }

    /* PW module */
    module = AtDeviceModuleGet(device, cAtModulePw);
    if ((module) && AtModuleInterruptIsEnabled(module) && ThaIpCorePwCauseInterrupt(self, intrStatus))
        {
        AtModuleInterruptProcess(module, intrStatus, ipCore);
        interruptHappened = cAtTrue;
        }

    /* ETH module */
    module = AtDeviceModuleGet(device, cAtModuleEth);
    if ((module) && AtModuleInterruptIsEnabled(module) && ThaIpCoreEthCauseInterrupt(self, intrStatus))
        {
        AtModuleInterruptProcess(module, intrStatus, ipCore);
        interruptHappened = cAtTrue;
        }

    /* SUR module. */
    module = AtDeviceModuleGet(device, cAtModuleSur);
    if ((module) && AtModuleInterruptIsEnabled(module) && ThaIpCoreSurCauseInterrupt(self, intrStatus))
        {
        AtModuleInterruptProcess(module, intrStatus, ipCore);
        interruptHappened = cAtTrue;
        }

    /* RAM module. */
    module = AtDeviceModuleGet(device, cAtModuleRam);
    if ((module) && AtModuleInterruptIsEnabled(module) && ThaIpCoreRamCauseInterrupt(self, intrStatus))
        {
        AtModuleInterruptProcess(module, intrStatus, ipCore);
        interruptHappened = cAtTrue;
        }

    /* Clock module. */
    module = AtDeviceModuleGet(device, cAtModuleClock);
    if ((module) && AtModuleInterruptIsEnabled(module) && ThaIpCoreClockCauseInterrupt(self, intrStatus))
        {
        AtModuleInterruptProcess(module, intrStatus, ipCore);
        interruptHappened = cAtTrue;
        }

    /* APS module. */
    module = AtDeviceModuleGet(device, cAtModuleAps);
    if ((module) && AtModuleInterruptIsEnabled(module) && ThaIpCoreApsCauseInterrupt(self, intrStatus))
        {
        AtModuleInterruptProcess(module, intrStatus, ipCore);
        interruptHappened = cAtTrue;
        }

    /* PTP module. */
    module = AtDeviceModuleGet(device, cAtModulePtp);
    if ((module) && AtModuleInterruptIsEnabled(module) && ThaIpCorePtpCauseInterrupt(self, intrStatus))
        AtModuleInterruptProcess(module, intrStatus, ipCore);

    /* Concate module. */
    module = AtDeviceModuleGet(device, cAtModuleConcate);
    if ((module) && AtModuleInterruptIsEnabled(module) && ThaIpCoreConcateCauseInterrupt(self, intrStatus))
        AtModuleInterruptProcess(module, intrStatus, ipCore);

    /* Encap module. */
    module = AtDeviceModuleGet(device, cAtModuleEncap);
    if ((module) && AtModuleInterruptIsEnabled(module) && ThaIpCoreEncapCauseInterrupt(self, intrStatus))
        AtModuleInterruptProcess(module, intrStatus, ipCore);

    /* Some physical sensors and IP utilities on FPGA device. */
    if (AtIpCoreSysmonInterruptIsEnabled((AtIpCore)self) && ThaIpCoreThermalCauseInterrupt(self, intrStatus))
        {
        AtSensorInterruptProcess((AtSensor)AtDeviceThermalSensorGet(device), ipCore);
        interruptHappened = cAtTrue;
        }

    if (AtIpCoreSemInterruptIsEnabled((AtIpCore)self) && ThaIpCoreSemCauseInterrupt(self, intrStatus))
        {
        uint8 semIndex, numSemControllers = AtDeviceNumSemControllersGet(device);
        for (semIndex = 0; semIndex < numSemControllers; semIndex++)
            AtSemControllerInterruptProcess(AtDeviceSemControllerGetByIndex(device, semIndex), ipCore);
        interruptHappened = cAtTrue;
        }

    /* Enable Interrupt */
    if (AtDeviceShouldEnableInterruptAfterProcessing(device))
        ThaIpCoreAllModulesInterruptEnable(self, interruptEnMask);

    InterruptProfile(self, interruptHappened);
    }

static eAtRet InterruptRestore(ThaIpCore self)
    {
    return ThaIpCoreInterruptRestore((ThaIpCore)IntrControllerGet(self));
    }

static uint32 ActiveRegister(ThaIpCore self)
    {
    AtUnused(self);
    return cThaRegChipActiveControl;
    }

static eAtRet AllModulesActivateOnPart(AtIpCore self, uint8 partId, eBool activate)
    {
    ThaDevice device = (ThaDevice)AtIpCoreDeviceGet(self);

    AtIpCoreWrite(self, ActiveRegister(mThis(self)) + ThaDevicePartOffset(device, partId), activate ? 0xFFFFFFFF : 0x0);

    return cAtOk;
    }

static eAtRet ModuleReactivate(AtIpCore self, uint32 moduleId)
    {
    uint32 regAddr, regVal;
    ThaDevice device = (ThaDevice)AtIpCoreDeviceGet(self);
    uint32 activeMask = ThaDeviceModuleActiveMask(device, moduleId);
    uint32 activeShift = ThaDeviceModuleActiveShift(device, moduleId);

    if (activeMask == 0)
        return cAtOk;

    regAddr = ActiveRegister(mThis(self));
    regVal  = AtIpCoreRead(self, regAddr);

    /* Just do nothing if module has not been activated */
    if ((regVal & activeMask) == 0)
        return cAtOk;

    mRegFieldSet(regVal, active, 0);
    AtIpCoreWrite(self, regAddr, regVal);
    mRegFieldSet(regVal, active, 1);
    AtIpCoreWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool ModuleIsActive(AtIpCore self, uint32 moduleId)
    {
    uint32 regAddr, regVal;
    ThaDevice device = (ThaDevice)AtIpCoreDeviceGet(self);
    uint32 activeMask = ThaDeviceModuleActiveMask(device, moduleId);

    if (activeMask == 0)
        return m_AtIpCoreImplement->ModuleIsActive(self, moduleId);

    regAddr = ActiveRegister(mThis(self));
    regVal  = AtIpCoreRead(self, regAddr);

    return (regVal & activeMask) ? cAtTrue : cAtFalse;
    }

static eAtRet PohDeactivate(ThaIpCore self)
    {
    AtIpCore core = (AtIpCore)self;
    uint32 regVal = AtIpCoreRead(core, ActiveRegister(self));
    mRegFieldSet(regVal, cThaGlbRegPohPact, 0);
    AtIpCoreWrite(core, ActiveRegister(self), regVal);
    return cAtOk;
    }

static eAtRet Activate(AtIpCore self, eBool activate)
    {
    uint8 partId;
    ThaDevice device = (ThaDevice)AtIpCoreDeviceGet(self);

    for (partId = 0; partId < ThaDeviceMaxNumParts(device); partId++)
        AllModulesActivateOnPart(self, partId, activate);

    return cAtOk;
    }

static eBool AllModulesIsActivatedOnPart(AtIpCore self, uint8 partId)
    {
    ThaDevice device = (ThaDevice)AtIpCoreDeviceGet(self);

    if (AtIpCoreRead(self, ActiveRegister(mThis(self)) + ThaDevicePartOffset(device, partId)) > 0)
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsActivated(AtIpCore self)
    {
    uint8 partId;
    ThaDevice device = (ThaDevice)AtIpCoreDeviceGet(self);

    for (partId = 0; partId < ThaDeviceMaxNumParts(device); partId++)
        {
        if (!AllModulesIsActivatedOnPart(self, partId))
            return cAtFalse;
        }

    return cAtTrue;
    }

static uint8 NumParts(ThaIpCore self)
    {
    return ThaDeviceMaxNumParts((ThaDevice)AtIpCoreDeviceGet((AtIpCore)self));
    }

static uint32 PartOffset(ThaIpCore self, uint8 partId)
    {
    return ThaDevicePartOffset((ThaDevice)AtIpCoreDeviceGet((AtIpCore)self), partId);
    }

static eBool CanEnableHwInterruptPin(ThaIpCore self)
    {
    return ThaIpCoreCanEnableHwInterruptPin((ThaIpCore)IntrControllerGet(self));
    }

static eBool InterruptRestoreIsSupported(ThaIpCore self)
    {
    return ThaIpCoreInterruptRestoreIsSupported((ThaIpCore)IntrControllerGet(self));
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaIpCore object = (ThaIpCore)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeUInt(interruptIsEnabled);
    mEncodeObject(intrController);
    mEncodeNone(ramResetWaitTimeMs);
    mEncodeUInt(asyncResetState);
    mEncodeUInt(asyncRamResetState);
    mEncodeUInt(asyncSerdesResetState);
    mEncodeNone(asyncStartTime);
    mEncodeUInt(asyncRamResetWaitState);

    mEncodeNone(interruptHappened);
    mEncodeNone(interruptCount);
    mEncodeNone(interruptContiguousCount);
    mEncodeNone(maxInterruptProcessingTimeUs);
    mEncodeNone(lastInterruptProcessingTimeUs);
    }

static void EthHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    ThaIpCoreEthHwInterruptEnable((ThaIpCore)IntrControllerGet(self), enable);
    }

static uint32 InterruptCountRead2Clear(AtIpCore self, eBool clear)
    {
    uint32 count = mThis(self)->interruptCount;
    if (clear)
        mThis(self)->interruptCount = 0;
    return count;
    }

static uint32 InterruptContiguousCountRead2Clear(AtIpCore self, eBool clear)
    {
    uint32 count = mThis(self)->interruptContiguousCount;
    if (clear)
        mThis(self)->interruptContiguousCount = 0;
    return count;
    }

static uint32 InterruptMaxProcessingTimeInUsRead2Clear(AtIpCore self, eBool clear)
    {
    uint32 count = mThis(self)->maxInterruptProcessingTimeUs;
    if (clear)
        mThis(self)->maxInterruptProcessingTimeUs = 0;
    return count;
    }

static uint32 InterruptLastProcessingTimeInUsRead2Clear(AtIpCore self, eBool clear)
    {
    uint32 count = mThis(self)->lastInterruptProcessingTimeUs;
    if (clear)
        mThis(self)->lastInterruptProcessingTimeUs = 0;
    return count;
    }

static void ShowInterruptCount(const char *title, uint32 count)
    {
    AtPrintc(cSevNormal, "%s: ", title);
    AtPrintc(count ? cSevCritical : cSevInfo, "%d\r\n", count);
    }

static void Debug(AtIpCore self)
    {
    ThaIpCore core = mThis(self);

    ShowInterruptCount("* Total interrupt count", core->interruptCount);
    ShowInterruptCount("* Interrupt contiguous count", core->interruptContiguousCount);
    }

static uint32 TimeStampInUs(tAtOsalCurTime *time)
    {
    return (time->sec * 1000000 + time->usec);
    }

static void InterruptProcessProfile(ThaIpCore self)
    {
    tAtOsalCurTime curTime, thisTime;

    AtOsalCurTimeGet(&thisTime);
    mMethodsGet(self)->InterruptProcess(self);
    AtOsalCurTimeGet(&curTime);

    self->lastInterruptProcessingTimeUs = TimeStampInUs(&curTime) - TimeStampInUs(&thisTime);
    if (self->lastInterruptProcessingTimeUs > self->maxInterruptProcessingTimeUs)
        self->maxInterruptProcessingTimeUs = self->lastInterruptProcessingTimeUs;
    }

static eBool ShouldProfile(ThaIpCore self)
    {
    ThaDevice device = (ThaDevice)AtIpCoreDeviceGet((AtIpCore)self);
    return ThaDeviceNeedProfileInterrupt(device);
    }

static void OverrideAtObject(AtIpCore self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();

        /* Save reference to super implementation */
        m_AtObjectMethods = mMethodsGet(object);

        /* Copy to reuse implementation of super class. But override some methods */
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    /* Change behavior of super class level 1 */
    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtIpCore(AtIpCore self)
    {
    AtOsal osal = AtSharedDriverOsalGet();

    if (!m_methodsInit)
        {
        m_AtIpCoreImplement = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtIpCoreOverride, m_AtIpCoreImplement, sizeof(m_AtIpCoreOverride));
        mMethodOverride(m_AtIpCoreOverride, InterruptEnable);
        mMethodOverride(m_AtIpCoreOverride, InterruptIsEnabled);
        mMethodOverride(m_AtIpCoreOverride, SemInterruptEnable);
        mMethodOverride(m_AtIpCoreOverride, SemInterruptIsEnabled);
        mMethodOverride(m_AtIpCoreOverride, SysmonInterruptEnable);
        mMethodOverride(m_AtIpCoreOverride, SysmonInterruptIsEnabled);
        mMethodOverride(m_AtIpCoreOverride, Activate);
        mMethodOverride(m_AtIpCoreOverride, IsActivated);
        mMethodOverride(m_AtIpCoreOverride, SoftReset);
        mMethodOverride(m_AtIpCoreOverride, AsyncSoftReset);
        mMethodOverride(m_AtIpCoreOverride, Test);
        mMethodOverride(m_AtIpCoreOverride, ModuleReactivate);
        mMethodOverride(m_AtIpCoreOverride, ModuleIsActive);
        mMethodOverride(m_AtIpCoreOverride, InterruptCountRead2Clear);
        mMethodOverride(m_AtIpCoreOverride, InterruptContiguousCountRead2Clear);
        mMethodOverride(m_AtIpCoreOverride, InterruptMaxProcessingTimeInUsRead2Clear);
        mMethodOverride(m_AtIpCoreOverride, InterruptLastProcessingTimeInUsRead2Clear);
        mMethodOverride(m_AtIpCoreOverride, Debug);
        mMethodOverride(m_AtIpCoreOverride, AsyncReset);
        }

    mMethodsSet(self, &m_AtIpCoreOverride);
    }

static void Override(AtIpCore self)
    {
    OverrideAtIpCore(self);
    OverrideAtObject(self);
    }

static void MethodsInit(ThaIpCore self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, InterruptProcess);
        mMethodOverride(m_methods, InterruptRestore);
        mMethodOverride(m_methods, FastRamTestIsEnabled);
        mMethodOverride(m_methods, SerdesSoftReset);
        mMethodOverride(m_methods, RamSoftReset);
        mMethodOverride(m_methods, AsyncSerdesSoftReset);
        mMethodOverride(m_methods, AsyncRamSoftReset);
        mMethodOverride(m_methods, RamSoftResetWait);
        mMethodOverride(m_methods, AsyncRamSoftResetWait);
        mMethodOverride(m_methods, SdhCauseInterrupt);
        mMethodOverride(m_methods, PdhCauseInterrupt);
        mMethodOverride(m_methods, PppCauseInterrupt);
        mMethodOverride(m_methods, SurCauseInterrupt);
        mMethodOverride(m_methods, PwCauseInterrupt);
        mMethodOverride(m_methods, EthCauseInterrupt);
        mMethodOverride(m_methods, RamCauseInterrupt);
        mMethodOverride(m_methods, PtpCauseInterrupt);
        mMethodOverride(m_methods, ConcateCauseInterrupt);
        mMethodOverride(m_methods, EncapCauseInterrupt);
        mMethodOverride(m_methods, ThermalCauseInterrupt);
        mMethodOverride(m_methods, SemCauseInterrupt);
        mMethodOverride(m_methods, ClockCauseInterrupt);
        mMethodOverride(m_methods, ApsCauseInterrupt);
        mMethodOverride(m_methods, InterruptStatusGet);
        mMethodOverride(m_methods, AllModulesInterruptEnable);
        mMethodOverride(m_methods, PdhHwInterruptEnable);
        mMethodOverride(m_methods, SdhHwInterruptEnable);
        mMethodOverride(m_methods, PppHwInterruptEnable);
        mMethodOverride(m_methods, PwHwInterruptEnable);
        mMethodOverride(m_methods, SurHwInterruptEnable);
        mMethodOverride(m_methods, EthHwInterruptEnable);
        mMethodOverride(m_methods, RamHwInterruptEnable);
        mMethodOverride(m_methods, PtpHwInterruptEnable);
        mMethodOverride(m_methods, ConcateHwInterruptEnable);
        mMethodOverride(m_methods, EncapHwInterruptEnable);
        mMethodOverride(m_methods, PhysicalHwInterruptEnable);
        mMethodOverride(m_methods, ClockHwInterruptEnable);
        mMethodOverride(m_methods, ApsHwInterruptEnable);
        mMethodOverride(m_methods, HwInterruptEnable);
        mMethodOverride(m_methods, InterruptDisable);
        mMethodOverride(m_methods, RamStableWaitInMs);
        mMethodOverride(m_methods, RamSoftResetRegister);
        mMethodOverride(m_methods, NumParts);
        mMethodOverride(m_methods, PartOffset);
        mMethodOverride(m_methods, CanEnableHwInterruptPin);
        mMethodOverride(m_methods, InterruptRestoreIsSupported);
        }

    mMethodsSet(self, &m_methods);
    }

AtIpCore ThaIpCoreObjectInit(AtIpCore self, uint8 coreId, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaIpCore));

    /* Super constructor */
    if (AtIpCoreObjectInit((AtIpCore)self, coreId, device) == NULL)
        return NULL;

    /* Initialize implementation */
    MethodsInit((ThaIpCore)self);

    /* Override */
    Override(self);

    m_methodsInit = 1;

    return self;
    }

AtIpCore ThaIpCoreNew(uint8 coreId, AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtIpCore newCore = mMethodsGet(osal)->MemAlloc(osal, sizeof(tThaIpCore));
    if (newCore == NULL)
        return NULL;

    /* Construct it */
    return ThaIpCoreObjectInit(newCore, coreId, device);
    }

eBool ThaIpCorePllIsLocked(AtIpCore core)
    {
    uint32 regVal;
    tAtOsalCurTime curTime, startTime;
    uint32 elapse, lockedCount;
    ThaDevice device = (ThaDevice)AtIpCoreDeviceGet(core);
    uint32 regAddr = ThaDevicePllClkSysStatReg(device);
    uint32 bitMask = ThaDevicePllClkSysStatBitMask(device);
    eBool simulated = IsSimulated(core);
    static const uint32 cThaPllClkWaitTimeToRecheckUs = 50000; /* 50ms */
    static const uint32 cThaPllClkCheckTimeoutMs      = 20000; /* Some products require long time for DDR calib done */
    static const uint8  cThaPllClkCheckMaxCount       = 10;

    if (!ThaDeviceCanCheckPll(device))
        return cAtTrue;

    /* Clear sticky */
    AtIpCoreWrite(core, regAddr, bitMask);

    elapse      = 0;
    lockedCount = 0;
    AtOsalCurTimeGet(&startTime);
    while (elapse < cThaPllClkCheckTimeoutMs)
        {
        /* Lock, increase lock counter */
        regVal = AtIpCoreRead(core, regAddr);

        if (IsSimulated(core))
            return cAtTrue;

        if ((regVal & bitMask) == 0)
            lockedCount++;

        /* Unstable, reset lock counter */
        else
            {
            AtIpCoreWrite(core, regAddr, bitMask);
            lockedCount = 0;
            }

        /* It is stable, exit */
        if (lockedCount == cThaPllClkCheckMaxCount)
            return cAtTrue;

        /* Calculate elapse time and retry */
        AtOsalCurTimeGet(&curTime);
        elapse = mTimeIntervalInMsGet(startTime, curTime);

        /* Sleep 50 ms before re-check */
        if (!simulated)
            AtOsalUSleep(cThaPllClkWaitTimeToRecheckUs);
        }

    /* PLL is unstable */
    AtDeviceLog((AtDevice)device, cAtLogLevelWarning, AtSourceLocation, "PLL is unstable, value of last checking time: 0x%x\r\n", regVal);
    return cAtFalse;
    }

uint8 ThaIpCoreNumParts(ThaIpCore self)
    {
    if (self)
        return mMethodsGet(self)->NumParts(self);
    return 0;
    }

uint32 ThaIpCorePartOffset(ThaIpCore self, uint8 partId)
    {
    if (self)
        return mMethodsGet(self)->PartOffset(self, partId);
    return 0x0;
    }

void ThaIpCoreEthHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    if (self)
       mMethodsGet(self)->EthHwInterruptEnable(self, enable);
    }

eAtRet ThaIpCorePohDeactivate(ThaIpCore self)
    {
    if (self)
        return PohDeactivate(self);
    return cAtErrorNullPointer;
    }

eBool ThaIpCorePwCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    if (self)
        return mMethodsGet(self)->PwCauseInterrupt(self, intrStatus);

    return cAtFalse;
    }

uint32 ThaIpCoreInterruptStatusGet(ThaIpCore self)
    {
    if (self)
        return mMethodsGet(self)->InterruptStatusGet(self);

    return 0;
    }

void ThaIpCoreAllModulesInterruptEnable(ThaIpCore self, uint32 intrEnMask)
    {
    if (self)
        mMethodsGet(self)->AllModulesInterruptEnable(self, intrEnMask);
    }

void ThaIpCorePdhHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    if (self)
        mMethodsGet(self)->PdhHwInterruptEnable(self, enable);
    }

void ThaIpCoreSdhHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    if (self)
        mMethodsGet(self)->SdhHwInterruptEnable(self, enable);
    }

void ThaIpCorePppHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    if (self)
       mMethodsGet(self)->PppHwInterruptEnable(self, enable);
    }

void ThaIpCorePwHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    if (self)
       mMethodsGet(self)->PwHwInterruptEnable(self, enable);
    }

void ThaIpCoreSurHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    if (self)
       mMethodsGet(self)->SurHwInterruptEnable(self, enable);
    }

void ThaIpCoreRamHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    if (self)
       mMethodsGet(self)->RamHwInterruptEnable(self, enable);
    }

void ThaIpCorePtpHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    if (self)
       mMethodsGet(self)->PtpHwInterruptEnable(self, enable);
    }

void ThaIpCorePhysicalHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    if (self)
       mMethodsGet(self)->PhysicalHwInterruptEnable(self, enable);
    }

void ThaIpCoreClockHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    if (self)
       mMethodsGet(self)->ClockHwInterruptEnable(self, enable);
    }

void ThaIpCoreApsHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    if (self)
       mMethodsGet(self)->ApsHwInterruptEnable(self, enable);
    }

void ThaIpCoreConcateHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    if (self)
       mMethodsGet(self)->ConcateHwInterruptEnable(self, enable);
    }

void ThaIpCoreEncapHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    if (self)
       mMethodsGet(self)->EncapHwInterruptEnable(self, enable);
    }

void ThaIpCoreHwInterruptEnable(ThaIpCore self, uint32 intrEnMask, eBool enable)
    {
    if (self)
       mMethodsGet(self)->HwInterruptEnable(self, intrEnMask, enable);
    }

void ThaIpCoreInterruptProcess(ThaIpCore self)
    {
    if (self == NULL)
        return;

    if (ShouldProfile(self))
        InterruptProcessProfile(self);
    else
        mMethodsGet(self)->InterruptProcess(self);
    }

eAtRet ThaIpCoreInterruptRestore(ThaIpCore self)
    {
    if (self)
        return mMethodsGet(self)->InterruptRestore(self);
    return cAtErrorNullPointer;
    }

uint32 ThaIpCoreInterruptDisable(ThaIpCore self)
    {
    if (self)
        return mMethodsGet(self)->InterruptDisable(self);

    return 0;
    }

eBool ThaIpCoreSdhCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    if (self)
        return mMethodsGet(self)->SdhCauseInterrupt(self, intrStatus);

    return cAtFalse;
    }

eBool ThaIpCorePdhCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    if (self)
        return mMethodsGet(self)->PdhCauseInterrupt(self, intrStatus);

    return cAtFalse;
    }

eBool ThaIpCoreSurCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    if (self)
        return mMethodsGet(self)->SurCauseInterrupt(self, intrStatus);

    return cAtFalse;
    }

eBool ThaIpCoreEthCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    if (self)
        return mMethodsGet(self)->EthCauseInterrupt(self, intrStatus);

    return cAtFalse;
    }

eBool ThaIpCoreRamCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    if (self)
        return mMethodsGet(self)->RamCauseInterrupt(self, intrStatus);

    return cAtFalse;
    }

eBool ThaIpCorePtpCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    if (self)
        return mMethodsGet(self)->PtpCauseInterrupt(self, intrStatus);

    return cAtFalse;
    }

eBool ThaIpCoreThermalCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    if (self)
        return mMethodsGet(self)->ThermalCauseInterrupt(self, intrStatus);
    return cAtFalse;
    }

eBool ThaIpCoreSemCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    if (self)
        return mMethodsGet(self)->SemCauseInterrupt(self, intrStatus);
    return cAtFalse;
    }

eBool ThaIpCoreClockCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    if (self)
        return mMethodsGet(self)->ClockCauseInterrupt(self, intrStatus);

    return cAtFalse;
    }

eBool ThaIpCoreApsCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    if (self)
        return mMethodsGet(self)->ApsCauseInterrupt(self, intrStatus);
    return cAtFalse;
    }

eBool ThaIpCoreConcateCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    if (self)
        return mMethodsGet(self)->ConcateCauseInterrupt(self, intrStatus);
    return cAtFalse;
    }

eBool ThaIpCoreEncapCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    if (self)
        return mMethodsGet(self)->EncapCauseInterrupt(self, intrStatus);
    return cAtFalse;
    }

void ThaIpCoreRamSoftResetWaitTimeMsSet(ThaIpCore self, uint32 waitTimeMs)
    {
    if (self)
        self->ramResetWaitTimeMs = waitTimeMs;
    }

uint32 ThaIpCoreRamSoftResetWaitTimeMsGet(ThaIpCore self)
    {
    return self ? self->ramResetWaitTimeMs : 0;
    }

eBool ThaIpCoreCanEnableHwInterruptPin(ThaIpCore self)
    {
    if (self)
        return mMethodsGet(self)->CanEnableHwInterruptPin(self);
    return cAtFalse;
    }

eBool ThaIpCoreInterruptRestoreIsSupported(ThaIpCore self)
    {
    if (self)
        return mMethodsGet(self)->InterruptRestoreIsSupported(self);
    return cAtFalse;
    }

eBool ThaIpCoreIsRamResetDone(ThaIpCore self)
    {
    AtIpCore core = (AtIpCore)self;
    uint32 softResetRegister;

    if (AtDeviceWarmRestoreIsStarted(AtIpCoreDeviceGet((AtIpCore)self)))
        return cAtTrue;

    softResetRegister = mMethodsGet(self)->RamSoftResetRegister(self);
    if (AtIpCoreRead(core, softResetRegister) == 0)
        return cAtTrue;

    return cAtFalse;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : ThaDevicePw.c
 *
 * Created Date: Apr 2, 2013
 *
 * Description : PW common device
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaDevicePwInternal.h"

#include "ThaIpCore.h"
#include "../sdh/ThaModuleSdh.h"
#include "../pdh/ThaModulePdh.h"
#include "../cdr/ThaModuleCdr.h"
#include "../map/ThaModuleAbstractMap.h"
#include "../eth/ThaModuleEth.h"
#include "../cla/ThaModuleCla.h"
#include "../ram/ThaModuleRam.h"
#include "../pmc/ThaModulePmc.h"
#include "versionreader/ThaVersionReader.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
/* Implementation of this class */
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceMethods  m_AtDeviceOverride;
static tThaDeviceMethods m_ThaDeviceOverride;

/* Super implementation */
static tAtDeviceMethods *m_AtDeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaDevicePw);
    }

static eBool ModuleIsSupported(AtDevice self, eAtModule moduleId)
    {
    /* Just to make compiler happy */
    uint32 _moduleId = (uint32)moduleId;
    switch (_moduleId)
        {
        case cThaModuleDemap: return cAtTrue;
        case cThaModuleMap:   return cAtTrue;
        case cThaModuleCdr:   return cAtTrue;
        case cThaModuleCla:   return cAtTrue;
        case cThaModulePwe:   return cAtTrue;
        case cThaModulePda:   return cAtTrue;
        case cThaModuleCos:   return cAtTrue;
        case cThaModuleBmt:   return cAtTrue;
        case cThaModulePmc:   return cAtTrue;
        default:
            break;
        }

    switch (_moduleId)
        {
        case cAtModulePdh:    return cAtTrue;
        case cAtModulePw :    return cAtTrue;
        case cAtModuleRam:    return cAtTrue;
        case cAtModuleEth:    return cAtTrue;

        default:
            break;
        }

    return m_AtDeviceMethods->ModuleIsSupported(self, moduleId);
    }

static const eAtModule *AllSupportedModulesGet(AtDevice self, uint8 *numModules)
    {
    static const eAtModule supportedModules[] = {cAtModulePdh,
                                                 cAtModulePw,
                                                 cAtModuleRam,
                                                 cAtModuleEth};
	AtUnused(self);
    if (numModules)
        *numModules = mCount(supportedModules);

    return supportedModules;
    }

static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    uint32 moduleId_ = (uint32)moduleId;

    if (moduleId_ == cAtModuleSdh)    return (AtModule)ThaModuleSdhV1New(self);
    if (moduleId_ == cAtModulePdh)    return (AtModule)ThaModulePdhNew(self);
    if (moduleId_ == cThaModuleCdr)   return (AtModule)ThaModuleCdrNew(self);
    if (moduleId_ == cThaModuleMap)   return (AtModule)ThaModuleMapNew(self);
    if (moduleId_ == cThaModuleDemap) return (AtModule)ThaModuleDemapNew(self);
    if (moduleId_ == cAtModuleEth)    return (AtModule)ThaModuleEthPwNew(self);
    if (moduleId_ == cThaModuleCla)   return (AtModule)ThaModuleClaPwNew(self);
    if (moduleId_ == cAtModuleRam)    return (AtModule)ThaModuleDdr2New(self);
    if (moduleId_ == cThaModulePmc)   return (AtModule)ThaModulePmcNew(self);

    /* Not supported modules */
    if ((moduleId_ == cAtModuleEncap)    ||
        (moduleId_ == cAtModulePpp)      ||
        (moduleId_ == cAtModuleAtm)      ||
        (moduleId_ == cAtModuleIma)      ||
        (moduleId_ == cThaModuleMpig)    ||
        (moduleId_ == cThaModuleMpeg)    ||
        (moduleId_ == cThaModulePmcMpeg) ||
        (moduleId_ == cThaModulePmcMpig))
        return NULL;

    /* Let super create other modules */
    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static eAtRet DefaultIpCoreSetup(AtDevice self)
    {
    static const uint8 secondCore = 1;

    if (AtDeviceNumIpCoresGet(self) == 1)
        return cAtOk;

    /* Just set default core for modules that belong to the second core */
    AtModuleDefaultCoreSet(AtDeviceModuleGet(self, cAtModulePdh)   , secondCore);
    AtModuleDefaultCoreSet(AtDeviceModuleGet(self, cAtModuleEncap) , secondCore);
    AtModuleDefaultCoreSet(AtDeviceModuleGet(self, cThaModulePwe)  , secondCore);
    AtModuleDefaultCoreSet(AtDeviceModuleGet(self, cThaModuleBmt)  , secondCore);
    AtModuleDefaultCoreSet(AtDeviceModuleGet(self, cThaModuleCos)  , secondCore);
    AtModuleDefaultCoreSet(AtDeviceModuleGet(self, cAtModuleEth)   , secondCore);
    AtModuleDefaultCoreSet(AtDeviceModuleGet(self, cThaModuleCla)  , secondCore);
    AtModuleDefaultCoreSet(AtDeviceModuleGet(self, cThaModuleCdr)  , secondCore);
    AtModuleDefaultCoreSet(AtDeviceModuleGet(self, cThaModulePwPmc), secondCore);
    AtModuleDefaultCoreSet(AtDeviceModuleGet(self, cThaModuleDemap), secondCore);
    AtModuleDefaultCoreSet(AtDeviceModuleGet(self, cAtModuleConcate), secondCore);

    return cAtOk;
    }

static uint8 NumIpCoresGet(AtDevice self)
    {
	AtUnused(self);
    return 2;
    }

static AtIpCore IpCoreCreate(AtDevice self, uint8 coreId)
    {
    return ThaIpCorePwNew(coreId, self);
    }

static ThaVersionReader VersionReaderCreate(ThaDevice self)
    {
    return ThaVersionReaderPwNew((AtDevice)self);
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(tAtDeviceMethods));
        mMethodOverride(m_AtDeviceOverride, AllSupportedModulesGet);
        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, ModuleIsSupported);
        mMethodOverride(m_AtDeviceOverride, DefaultIpCoreSetup);
        mMethodOverride(m_AtDeviceOverride, NumIpCoresGet);
        mMethodOverride(m_AtDeviceOverride, IpCoreCreate);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static void OverrideThaDevice(AtDevice self)
    {
    ThaDevice device = (ThaDevice)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDeviceOverride, mMethodsGet(device), sizeof(m_ThaDeviceOverride));
        mMethodOverride(m_ThaDeviceOverride, VersionReaderCreate);
        }

    mMethodsSet(device, &m_ThaDeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtDevice(self);
    OverrideThaDevice(self);
    }

AtDevice ThaDevicePwObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaDeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

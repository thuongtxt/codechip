/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : AtSSKeyChecker.h
 * 
 * Created Date: Sep 18, 2013
 *
 * Description : SS Key checker
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASSKEYCHECKER_H_
#define _THASSKEYCHECKER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h" /* Super class */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Product concretes */
AtSSKeyChecker Tha60030022SSKeyCheckerNew(AtDevice device);
AtSSKeyChecker Tha60060011SSKeyCheckerNew(AtDevice device);
AtSSKeyChecker Tha60031032SSKeyCheckerNew(AtDevice device);
AtSSKeyChecker Tha60030023SSKeyCheckerNew(AtDevice device);
AtSSKeyChecker Tha60030051SSKeyCheckerNew(AtDevice device);
AtSSKeyChecker Tha60070013SSKeyCheckerNew(AtDevice device);
AtSSKeyChecker Tha60070023SSKeyCheckerNew(AtDevice device);
AtSSKeyChecker Tha60031021SSKeyCheckerNew(AtDevice device);
AtSSKeyChecker Tha60070041SSKeyCheckerNew(AtDevice device);
AtSSKeyChecker Tha60030080SSKeyCheckerNew(AtDevice device);
AtSSKeyChecker Tha60030081SSKeyCheckerNew(AtDevice device);
AtSSKeyChecker Tha60031031SSKeyCheckerNew(AtDevice device);
AtSSKeyChecker Tha60200011SSKeyCheckerNew(AtDevice device);
AtSSKeyChecker Tha60071011SSKeyCheckerNew(AtDevice device);
AtSSKeyChecker Tha60071021SSKeyCheckerNew(AtDevice device);
AtSSKeyChecker Tha60150011SSKeyCheckerNew(AtDevice device);
AtSSKeyChecker Tha60070061SSKeyCheckerNew(AtDevice device);
AtSSKeyChecker Tha60030101SSKeyCheckerNew(AtDevice device);
AtSSKeyChecker Tha60210011SSKeyCheckerNew(AtDevice device);
AtSSKeyChecker Tha60070051SSKeyCheckerNew(AtDevice device);
AtSSKeyChecker Tha60240021SSKeyCheckerNew(AtDevice device);
AtSSKeyChecker Tha60220031SSKeyCheckerNew(AtDevice device);
AtSSKeyChecker ThaPdhPwProductSSKeyCheckerNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THASSKEYCHECKER_H_ */


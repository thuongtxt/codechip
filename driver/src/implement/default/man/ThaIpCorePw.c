/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : ThaIpCorePw.c
 *
 * Created Date: Mar 26, 2013
 *
 * Description : IP Core of PW product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../generic/man/AtDeviceInternal.h"
#include "../../default/man/ThaIpCoreInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaSoftResetReg 0xF00004

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaIpCorePw *ThaIpCorePw;

typedef struct tThaIpCorePw
    {
    tThaIpCore super;

    /* Private data */
    }tThaIpCorePw;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
/* Implementation of this class */
static uint8 m_methodsInit = 0;

/* Override */
static tAtIpCoreMethods  m_AtIpCoreOverride;

/* Save super implementation */
static const tAtIpCoreMethods  *m_AtIpCoreImplement  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool IsSimulated(AtIpCore self)
    {
    AtDevice device = AtIpCoreDeviceGet(self);
    return AtDeviceIsSimulated(device);
    }

static eAtRet SoftReset(AtIpCore core)
    {
    AtOsal osal = AtSharedDriverOsalGet();

    if (IsSimulated(core))
        return cAtOk;

    AtIpCoreWrite(core, cThaSoftResetReg, 0x1);
    mMethodsGet(osal)->Sleep(osal, 1);
    AtIpCoreWrite(core, cThaSoftResetReg, 0x0);
    mMethodsGet(osal)->Sleep(osal, 1);

    return cAtOk;
    }

static void OverrideAtIpCore(ThaIpCorePw self)
    {
    AtIpCore ipCore = (AtIpCore)self;
    AtOsal osal = AtSharedDriverOsalGet();

    if (!m_methodsInit)
        {
        m_AtIpCoreImplement = mMethodsGet(ipCore);
        mMethodsGet(osal)->MemCpy(osal, &m_AtIpCoreOverride, m_AtIpCoreImplement, sizeof(m_AtIpCoreOverride));
        mMethodOverride(m_AtIpCoreOverride, SoftReset);
        }

    mMethodsSet(ipCore, &m_AtIpCoreOverride);
    }

static void Override(ThaIpCorePw self)
    {
    OverrideAtIpCore(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaIpCorePw);
    }

/* Constructor of ThaIpCorePw object */
static AtIpCore ObjectInit(AtIpCore self, uint8 coreId, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaIpCoreObjectInit(self, coreId, device) == NULL)
        return NULL;

    /* Override */
    Override((ThaIpCorePw)self);
    m_methodsInit = 1;

    return self;
    }

/* Create new IP core object */
AtIpCore ThaIpCorePwNew(uint8 coreId, AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtIpCore newCore = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newCore == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newCore, coreId, device);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAN
 * 
 * File        : ThaIpCoreAsyncInit.h
 * 
 * Created Date: Oct 27, 2016
 *
 * Description : Interface of IP CORE async init
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAIPCOREASYNCINIT_H_
#define _THAIPCOREASYNCINIT_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet ThaIpCoreAsyncReset(AtIpCore self);
eAtRet ThaIpCoreAsyncSerdesSoftReset(ThaIpCore self);
eAtRet ThaIpCoreAsyncRamSoftReset(ThaIpCore self);
eAtRet ThaIpCoreAsyncRamSoftResetWait(ThaIpCore self);
eAtRet ThaIpCoreAsyncRamSoftResetWaitV2(ThaIpCore self);

#ifdef __cplusplus
}
#endif
#endif /* _THAIPCOREASYNCINIT_H_ */


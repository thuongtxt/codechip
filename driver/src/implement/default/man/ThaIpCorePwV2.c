/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : ThaIpCorePwV2V2.c
 *
 * Created Date: May 23, 2013
 *
 * Description : IP Core of PW product (version 2)
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../generic/man/AtDeviceInternal.h"
#include "../../default/man/ThaIpCoreInternal.h"
#include "ThaIpCoreAsyncInit.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
/* Implementation of this class */
static uint8 m_methodsInit = 0;

/* Override */
static tThaIpCoreMethods m_ThaIpCoreOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet SerdesSoftReset(ThaIpCore self)
    {
	AtUnused(self);
    return cAtOk;
    }

static eAtRet AsyncSerdesSoftReset(ThaIpCore self)
    {
    return SerdesSoftReset(self);
    }

static eBool IsSimulated(ThaIpCore self)
    {
    return AtDeviceIsSimulated(AtIpCoreDeviceGet((AtIpCore)self));
    }

static eAtRet RamSoftResetWait(ThaIpCore self)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 waitTimeMs = ThaIpCoreRamSoftResetWaitTimeMsGet(self);
    if (waitTimeMs == 0)
        waitTimeMs = 1000;

    if (!IsSimulated(self))
        mMethodsGet(osal)->USleep(osal, waitTimeMs * 1000);

    return cAtOk;
    }

static eAtRet AsyncRamSoftResetWait(ThaIpCore self)
    {
    return ThaIpCoreAsyncRamSoftResetWaitV2(self);
    }

static uint32 RamStableWaitInMs(ThaIpCore self)
    {
    AtUnused(self);
    return 1000; /* It is so large */
    }

static void OverrideThaIpCore(ThaIpCorePwV2 self)
    {
    ThaIpCore core = (ThaIpCore)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaIpCoreOverride, mMethodsGet(core), sizeof(m_ThaIpCoreOverride));

        mMethodOverride(m_ThaIpCoreOverride, SerdesSoftReset);
        mMethodOverride(m_ThaIpCoreOverride, AsyncSerdesSoftReset);
        mMethodOverride(m_ThaIpCoreOverride, RamSoftResetWait);
        mMethodOverride(m_ThaIpCoreOverride, AsyncRamSoftResetWait);
        mMethodOverride(m_ThaIpCoreOverride, RamStableWaitInMs);
        }

    mMethodsSet(core, &m_ThaIpCoreOverride);
    }

static void Override(ThaIpCorePwV2 self)
    {
    OverrideThaIpCore(self);
    }

/* Constructor of ThaIpCorePwV2 object */
AtIpCore ThaIpCorePwV2ObjectInit(AtIpCore self, uint8 coreId, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaIpCorePwV2));

    /* Super constructor */
    if (ThaIpCoreObjectInit(self, coreId, device) == NULL)
        return NULL;

    /* Override */
    Override((ThaIpCorePwV2)self);
    m_methodsInit = 1;

    return self;
    }

/* Create new IP core object */
AtIpCore ThaIpCorePwV2New(uint8 coreId, AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtIpCore newCore = mMethodsGet(osal)->MemAlloc(osal, sizeof(tThaIpCorePwV2));
    if (newCore == NULL)
        return NULL;

    /* Construct it */
    return ThaIpCorePwV2ObjectInit(newCore, coreId, device);
    }


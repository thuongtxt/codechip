/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : ThaDeviceAsyncInit.c
 *
 * Created Date: Aug 16, 2016
 *
 * Description :
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaDeviceInternal.h"
#include "ThaIpCoreInternal.h"
#include "ThaDeviceAsyncInit.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaDevice)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef enum eTheDeviceAsynInitState
    {
    cThaDeviceAsyncInitStateEntrance = 0,
    cThaDeviceAsyncInitStateSupperInit,
    cThaDeviceAsyncInitStatePostInit
    } eTheDeviceAsynInitState;

typedef enum eThaDeviceAllModuleAsyncInitState
    {
    cThaDeviceAllModuleAsyncInitStateInterruptDisable = 0,
    cThaDeviceAllModuleAsyncInitStateInit,
    cThaDeviceAllModuleAsyncInitStateSupperInit
    } eThaDeviceAllModuleAsyncInitState;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void AllInterruptDisable(AtDevice self)
    {
    uint8 i, numCore = AtDeviceNumIpCoresGet(self);

    /* Disable interrupt mask for all modules in all cores. */
    for (i = 0; i < numCore; i++)
        {
        ThaIpCore ipCore = (ThaIpCore)AtDeviceIpCoreGet(self, i);
        if (ipCore)
            mMethodsGet(ipCore)->InterruptDisable(ipCore);
        }
    }

static void AsyncInitStateSet(AtDevice self, uint32 state)
    {
    mThis(self)->asyncInitState = state;
    }

static uint32 AsyncInitStateGet(AtDevice self)
    {
    return mThis(self)->asyncInitState;
    }

eAtRet ThaDeviceAsyncInit(AtDevice self)
    {
    eAtRet ret = cAtOk;
    uint32 state = AsyncInitStateGet(self);
    tAtOsalCurTime prevTime;

    AtOsalCurTimeGet(&prevTime);
    switch(state)
        {
        case cThaDeviceAsyncInitStateEntrance:
            ThaDeviceEntranceToInit(self);
            AtDeviceAccessTimeCountSinceLastProfileCheck(self, cAtTrue, AtSourceLocation, "EntranceToInit");
            AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &prevTime, AtSourceLocation, "EntranToInit");
            state = cThaDeviceAsyncInitStateSupperInit;
            ret = cAtErrorAgain;
            break;
        case cThaDeviceAsyncInitStateSupperInit:
            ret = ThaDeviceSupperAsyncInit(self);
            AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &prevTime, AtSourceLocation, "ThaDeviceSupperAsyncInit");
            if (ret == cAtOk)
                {
                state = cThaDeviceAsyncInitStatePostInit;
                ret = cAtErrorAgain;
                }
            else if (!AtDeviceAsyncRetValIsInState(ret))
                state = cThaDeviceAsyncInitStateEntrance;
            break;
        case cThaDeviceAsyncInitStatePostInit:
            ThaDevicePostInit(self);
            AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &prevTime, AtSourceLocation, "ThaDevicePostInit");
            state = cThaDeviceAsyncInitStateEntrance;
            ret = cAtOk;
            break;
        default:
            state = cThaDeviceAsyncInitStateEntrance;
            ret = cAtErrorDevFail;
        }

    AsyncInitStateSet(self, state);
    return ret;
    }

static void AsyncModulePositionSet(AtDevice self, uint32 position)
    {
    mThis(self)->asyncModulePosition = position;
    }

static uint32 AsyncModulePositionGet(AtDevice self)
    {
    return mThis(self)->asyncModulePosition;
    }

static eAtRet ThaModulesAsyncInit(AtDevice self)
    {
    uint32 nextModulePos = AsyncModulePositionGet(self);
    static uint32 allModules[] = {cThaModuleMap,
                                  cThaModuleDemap,
                                  cThaModuleCdr,
                                  cThaModuleCla,
                                  cThaModulePwe,
                                  cThaModulePda,
                                  cThaModuleCos,
                                  cThaModuleBmt,
                                  cThaModuleMpig,
                                  cThaModuleMpeg,
                                  cThaModulePmcMpig,
                                  cThaModulePmcMpeg,
                                  cThaModuleOcn,
                                  cThaModulePoh};
    eAtRet ret = cAtOk;
    tAtOsalCurTime profileTime;
    char stateString[32];

    AtOsalCurTimeGet(&profileTime);
    AtSprintf(stateString, "nextModulePos: %d", nextModulePos);
    ret = AtDeviceModuleAsyncInit(self, allModules[nextModulePos]);
    AtDeviceAccessTimeCountSinceLastProfileCheck(self, cAtTrue, AtSourceLocation, stateString);
    AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &profileTime, AtSourceLocation, stateString);
    if (ret == cAtOk)
        {
        nextModulePos += 1;
        ret = cAtErrorAgain;
        }
    else if (!AtDeviceAsyncRetValIsInState(ret))
        nextModulePos = 0;

    if (nextModulePos == mCount(allModules))/*last module*/
        {
        nextModulePos = 0;
        ret = cAtOk;
        }

    AsyncModulePositionSet(self, nextModulePos);
    return ret;
    }

static void AsyncAllModuleInitStateSet(AtDevice self, uint32 state)
    {
    mThis(self)->asyncAllModuleInitState = state;
    }

static uint32 AsyncAllModuleInitStateGet(AtDevice self)
    {
    return mThis(self)->asyncAllModuleInitState;
    }

eAtRet ThaDeviceAllModulesAsyncInit(AtDevice self)
    {
    uint32 state = AsyncAllModuleInitStateGet(self);
    eAtRet ret = cAtOk;
    tAtOsalCurTime profileTime;

    AtOsalCurTimeGet(&profileTime);

    switch (state)
        {
        case cThaDeviceAllModuleAsyncInitStateInterruptDisable:
            AllInterruptDisable(self);
            AtDeviceAccessTimeCountSinceLastProfileCheck(self, cAtTrue, AtSourceLocation, "AllInterruptDisable");
            AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &profileTime, AtSourceLocation, "AllInterruptDisable");
            state = cThaDeviceAllModuleAsyncInitStateInit;
            ret = cAtErrorAgain;
            break;
        case cThaDeviceAllModuleAsyncInitStateInit:
            ret = ThaModulesAsyncInit(self);
            AtDeviceAccessTimeCountSinceLastProfileCheck(self, cAtTrue, AtSourceLocation, "ThaModulesAsyncInit");
            AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &profileTime, AtSourceLocation, "ThaModulesAsyncInit");
            if (ret == cAtOk)
                {
                state = cThaDeviceAllModuleAsyncInitStateSupperInit;
                ret = cAtErrorAgain;
                }
            else if (!AtDeviceAsyncRetValIsInState(ret))
                state = cThaDeviceAllModuleAsyncInitStateInterruptDisable;
            break;
        case cThaDeviceAllModuleAsyncInitStateSupperInit:
            /* Let's super do remaining thing */
            ret = ThaDeviceSuperAllModulesAsyncInit(self);
            AtDeviceAccessTimeCountSinceLastProfileCheck(self, cAtTrue, AtSourceLocation, "ThaDeviceSuperAllModulesAsyncInit");
            AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &profileTime, AtSourceLocation, "ThaDeviceSuperAllModulesAsyncInit");
            if (!AtDeviceAsyncRetValIsInState(ret))
                state = cThaDeviceAllModuleAsyncInitStateInterruptDisable;
            break;
        default:
            state = cThaDeviceAllModuleAsyncInitStateInterruptDisable;
            ret = cAtErrorDevFail;
        }

    AsyncAllModuleInitStateSet(self, state);
    return ret;
    }

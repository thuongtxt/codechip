/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : ThaModuleRamInternal.h
 * 
 * Created Date: Feb 1, 2013
 *
 * Description : Thalassa RAM module representation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULERAMINTERNAL_H_
#define _THAMODULERAMINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/ram/AtModuleRamInternal.h"
#include "ThaModuleRam.h"
#include "AtErrorGenerator.h"
#include "ThaModuleRam.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleRamMethods
    {
    uint32 (*DDRUserClockValueStatusRegister)(ThaModuleRam self, uint8 ddrId);
    uint32 (*QDRUserClockValueStatusRegister)(ThaModuleRam self, uint8 qdrId);
    const char *(*QDRUserClockName)(ThaModuleRam self, uint8 qdrId);
    eBool  (*BurstTestIsSupported)(ThaModuleRam self);
    eBool  (*NeedClearWholeMemoryBeforeTesting)(ThaModuleRam self);
    uint32 (*DdrCalibStatusMask)(ThaModuleRam self, uint8 ddrId);
    uint32 (*ClockDdrSwingRangeInPpm)(ThaModuleRam self);
    uint32 (*ClockQdrSwingRangeInPpm)(ThaModuleRam self);
    eBool  (*DdrHasSpecificValue)(ThaModuleRam self);
    void (*StickyDebug)(ThaModuleRam self);

    /* Internal RAM. */
    eBool  (*InternalRamMonitoringIsSupported)(ThaModuleRam self);
    uint32 (*InterruptStatusGet)(ThaModuleRam self);
    eBool  (*PhyModuleHasRamInterrupt)(ThaModuleRam self, uint32 moduleId, uint32 ramIntr);
    const char **(*PhyModuleRamDescription)(ThaModuleRam self, uint32 moduleId, uint32 *numRams);
    uint32 (*PhyModuleRamLocalCellAddress)(ThaModuleRam self, uint32 moduleId, uint32 localRamId);
    const tThaInternalRamEntry* (*AllInternalRamEntry)(ThaModuleRam self, uint32 *numRams);

    /* Error-Generator for Internal RAM. */
    eBool  (*CrcCounterIsSupported)(ThaModuleRam self);
    eBool  (*EccCounterIsSupported)(ThaModuleRam self);
    AtErrorGenerator (*ErrorGeneratorObjectCreate)(ThaModuleRam self, AtInternalRam ram);
    }tThaModuleRamMethods;

typedef struct tThaModuleRam
    {
    tAtModuleRam super;
    const tThaModuleRamMethods *methods;

    /* Private data */
    eBool interruptIsEnabled;
    }tThaModuleRam;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleRam ThaModuleRamObjectInit(AtModuleRam self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULERAMINTERNAL_H_ */


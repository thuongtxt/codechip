/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : ThaModuleDdr2.c
 *
 * Created Date: Mar 30, 2013
 *
 * Description : RAM module that manage DDR2s
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaModuleDdr2Internal.h"
#include "../ThaDdr.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaNumDdrOfEachFpga                           3 /* The number of DDR for each FPGA device */

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

static tAtModuleMethods m_AtModuleOverride;
static tAtModuleRamMethods m_AtModuleRamOverride;

/* Save super implementations */
static const tAtModuleMethods *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaModuleDdr2);
    }

static uint8 NumDdrGet(AtModuleRam self)
    {
	AtUnused(self);
    return 6;
    }

static AtIpCore CoreOfDdr(AtModuleRam self, uint8 ddrId)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return AtDeviceIpCoreGet(device, ddrId / cThaNumDdrOfEachFpga);
    }

static AtRam DdrCreate(AtModuleRam self, AtIpCore core, uint8 ddrId)
    {
    return ThaDdr2New(self, core, ddrId);
    }

static eBool DdrIsUsed(AtModule self, uint8 ddrId)
    {
	AtUnused(self);
    if ((ddrId == 2) || (ddrId == 3) || (ddrId == 4))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet InitAllRams(AtModule self)
    {
    uint8 i;
    AtModuleRam ramModule = (AtModuleRam)self;
    eAtRet ret = cAtOk;

    for (i = 0; i < AtModuleRamNumDdrGet(ramModule); i++)
        {
        if (DdrIsUsed(self, i))
            ret |= AtRamInit(AtModuleRamDdrGet(ramModule, i));
        }

    return ret;
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return InitAllRams(self);
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void OverrideAtModuleRam(AtModuleRam self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleRamOverride, mMethodsGet(self), sizeof(m_AtModuleRamOverride));
        mMethodOverride(m_AtModuleRamOverride, NumDdrGet);
        mMethodOverride(m_AtModuleRamOverride, CoreOfDdr);
        mMethodOverride(m_AtModuleRamOverride, DdrCreate);
        }

    mMethodsSet(self, &m_AtModuleRamOverride);
    }

static void Override(AtModuleRam self)
    {
    OverrideAtModule((AtModule)self);
    OverrideAtModuleRam(self);
    }

static AtModuleRam ObjectInit(AtModuleRam self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleRamObjectInit(self, device) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleRam ThaModuleDdr2New(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleRam newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newModule, device);
    }


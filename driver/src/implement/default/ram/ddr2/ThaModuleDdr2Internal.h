/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : ThaModuleDdr2Internal.h
 * 
 * Created Date: May 8, 2013
 *
 * Description : DDR2 module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEDDR2INTERNAL_H_
#define _THAMODULEDDR2INTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../ThaModuleRamInternal.h"

#include "ThaModuleDdr2Reg.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleDdr2
    {
    tThaModuleRam super;

    /* Private data */
    }tThaModuleDdr2;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEDDR2INTERNAL_H_ */


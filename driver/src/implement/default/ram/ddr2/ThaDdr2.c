/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : ThaDdr2.c
 *
 * Created Date: Mar 30, 2013
 *
 * Description : DDR of STM PW product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/man/AtDeviceInternal.h"
#include "../ThaDdrInternal.h"
#include "ThaModuleDdr2Reg.h"

/*--------------------------- Define -----------------------------------------*/
#define cTestAddress  1
#define cTestData     2
#define cPrbs23       0
#define cWaitTimeToClearStatus 500000

/* Error status */
#define cErrorPrbsEngineNotRun cBit0
#define cErrorPrbsError        cBit1

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaDdr2)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaDdr2 * ThaDdr2;

typedef struct tThaDdr2
    {
    tThaDdr super;

    /* Private data */
    uint32 testDurationInMs;
    uint32 errorStatus;
    }tThaDdr2;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtRamMethods m_AtRamOverride;

/* Save super's implementation */
static const tAtRamMethods *m_AtRamMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 LocalId(ThaDdr2 self)
    {
    static const uint8 cNumRamsForOneCore = 3;
    return AtRamIdGet((AtRam)self) % cNumRamsForOneCore;
    }

static uint32 BaseAddress(ThaDdr2 self)
    {
    uint8 ddrLocalId = LocalId(self);

    if (ddrLocalId == 0) return 0x00f20000;
    if (ddrLocalId == 1) return 0x00f22000;
    if (ddrLocalId == 2) return 0x00f24000;

    return 0;
    }

static uint32 CpuRead(AtRam self, uint32 localAddress)
    {
    AtHal hal = AtIpCoreHalGet(AtRamIpCoreGet(self));
    return AtHalRead(hal, localAddress + BaseAddress(mThis(self)));
    }

static void CpuWrite(AtRam self, uint32 localAddress, uint32 value)
    {
    AtHal hal = AtIpCoreHalGet(AtRamIpCoreGet(self));
    AtHalWrite(hal, localAddress + BaseAddress(mThis(self)), value);
    }

static void CalibrationEnable(AtRam self, eBool enable)
    {
    CpuWrite(self, cThaDdrRegInitCtrl, (uint32)(enable ? 1 : 0));
    }

static void PrbsEnable(AtRam self, eBool enable)
    {
    uint32 regVal = CpuRead(self, cThaDdrRegPrbsCtrl);
    mFieldIns(&regVal, cThaDdrPrbsTestEnMask, cThaDdrPrbsTestEnShift, enable ? 1 : 0);
    CpuWrite(self, cThaDdrRegPrbsCtrl, regVal);
    }

static void ClearPrbsErrorStatus(AtRam self)
    {
    CpuWrite(self, cThaDdrRegPrbsErrStat, 0xF);
    }

static eAtModuleRamRet HwTestEnable(AtRam self, eBool enable)
    {
    uint32 regVal;

    regVal = CpuRead(self, cThaDdrRegPrbsCtrl);
    mFieldIns(&regVal, cThaDdrUserEnMask, cThaDdrUserEnShift, (enable == cAtTrue)? 0 : 1);
    mFieldIns(&regVal, cThaDdrPrbsTestEnMask, cThaDdrPrbsTestEnShift, enable ? 1 : 0);
    CpuWrite(self, cThaDdrRegPrbsCtrl, regVal);

    /* Clear PRBS error status */
    if (enable)
        ClearPrbsErrorStatus(self);

    return cAtOk;
    }

static eAtModuleRamRet TestEnable(AtRam self, eBool enable)
    {
    eAtRet ret = cAtOk;
    eBool a = enable ? cAtFalse : cAtTrue;

    /* Need to toggle configuration */
    ret |= HwTestEnable(self, a);
    ret |= HwTestEnable(self, enable);

    return ret;
    }

static eAtModuleRamRet Init(AtRam self)
    {
    /* Super initialization */
    eAtRet ret = m_AtRamMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    /* Additional setup */
    CalibrationEnable(self, cAtTrue);
    PrbsEnable(self, cAtTrue);
    TestEnable(self, cAtFalse);

    return cAtOk;
    }

static eAtModuleRamRet TestDurationSet(AtRam self, uint32 durationInMs)
    {
    mThis(self)->testDurationInMs = durationInMs;
    return cAtOk;
    }

static uint32 TestDurationGet(AtRam self)
    {
    return mThis(self)->testDurationInMs;
    }

static uint32 CellSizeGet(AtRam self)
    {
	AtUnused(self);
    return 2;
    }

static AtDevice Device(AtRam self)
    {
    return AtModuleDeviceGet((AtModule)AtRamModuleGet(self));
    }

static uint32 TimeoutMs(AtRam self)
    {
    if (AtDeviceTestbenchIsEnabled(Device(self)))
        return AtDeviceTestbenchDefaultTimeoutMs();

    AtUnused(self);
    return 100;
    }

static eBool HwDone(AtRam self)
    {
    uint32 elapseTime;
    tAtOsalCurTime startTime, curTime;
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 regVal;
    uint32 cThaDrrRdWrTimeOutInMs = TimeoutMs(self);

    elapseTime = 0;
    mMethodsGet(osal)->CurTimeGet(osal, &startTime);
    while(elapseTime < cThaDrrRdWrTimeOutInMs)
        {
        regVal = CpuRead(self, cThaDdrRegReadWriteRequest);
        if (regVal & cThaDdrReadyMask)
            return cAtTrue;

        /* Calculate elapse time */
        mMethodsGet(osal)->CurTimeGet(osal, &curTime);
        elapseTime = mMethodsGet(osal)->DifferenceTimeInMs(osal, &curTime, &startTime);
        }

    /* TODO: Should be replaced by logger */
    AtPrintc(cSevCritical, "ERROR: (%s, %d) Waiting for DDR fail\r\n", AtSourceLocation);

    return cAtFalse;
    }

static void InvalidValueMake(AtRam self, uint32 *value)
    {
    uint8 cell_i;
    for (cell_i = 0; cell_i < AtRamCellSizeGet(self); cell_i++)
        value[cell_i] = 0xCAFECAFE;
    }

static eAtModuleRamRet AccessEnable(AtRam self, eBool enable)
    {
    uint32 regVal;

    regVal = CpuRead(self, cThaDdrRegPrbsCtrl);

    mFieldIns(&regVal, cThaDdrPrbsTestEnMask, cThaDdrPrbsTestEnShift, 1);

    mFieldIns(&regVal, cThaDdrUserEnMask, cThaDdrUserEnShift, enable ? 1 : 0);
    CpuWrite(self, cThaDdrRegPrbsCtrl, cThaDdrRegPrbsCtrl);

    return cAtOk;
    }

static eBool AccessIsEnabled(AtRam self)
    {
    return (CpuRead(self, cThaDdrRegPrbsCtrl) & cThaDdrUserEnMask) ? cAtTrue : cAtFalse;
    }

static eAtRet CellRead(AtRam self, uint32 address, uint32 *value)
    {
    uint32 regVal;

    /* Check engine is busy or not */
    if (!HwDone(self))
        {
        InvalidValueMake(self, value);
        return cAtErrorIndrAcsTimeOut;
        }

    /* Read DDR */
    regVal = 0;
    mFieldIns(&regVal, cBit30,    30, 1); /* Read */
    mFieldIns(&regVal, cBit22_10, 10, AtRamCellRowGet(self, address));
    mFieldIns(&regVal, cBit9_8,   8, AtRamCellBankGet(self, address));
    mFieldIns(&regVal, cBit7_0,   0, AtRamCellColumnGet(self, address));
    CpuWrite(self, cThaDdrRegReadWriteRequest, regVal);

    /* Waiting for hardware done */
    if (!HwDone(self))
        {
        InvalidValueMake(self, value);
        return cAtErrorIndrAcsTimeOut;
        }

    value[0] = CpuRead(self, cThaDdrRegPrbsDataStat);
    value[1] = CpuRead(self, cThaDdrRegPrbsDataStat + 1);
    return cAtOk;
    }

static eAtRet CellWrite(AtRam self, uint32 address, const uint32 *value)
    {
    uint32 regVal;

    /* Write values */
    CpuWrite(self, 0x20, value[0]);
    CpuWrite(self, 0x21, value[1]);
    if (!HwDone(self))
        return cAtErrorIndrAcsTimeOut;

    /* Write DDR */
    regVal = 0;
    mFieldIns(&regVal, cBit30,    30, 0); /* Write */
    mFieldIns(&regVal, cBit22_10, 10, AtRamCellRowGet(self, address));
    mFieldIns(&regVal, cBit9_8,   8, AtRamCellBankGet(self, address));
    mFieldIns(&regVal, cBit7_0,   0, AtRamCellColumnGet(self, address));
    CpuWrite(self, cThaDdrRegReadWriteRequest, regVal);
    if (!HwDone(self))
        return cAtErrorIndrAcsTimeOut;

    return cAtOk;
    }

static eBool CalibrationSuccess(AtRam self)
    {
    uint32 regVal = CpuRead(self, cThaDdrRegCalibStat);
    return ((regVal & cThaDdrCalibStatMask) == 1) ? cAtTrue : cAtFalse;
    }

static eAtModuleRamRet InitStatusGet(AtRam self)
    {
    if (!CalibrationSuccess(self))
        return cAtErrorDdrCalibFail;

    return cAtOk;
    }

static eBool PrbsEngineIsRunning(AtRam self)
    {
    uint32 prbsFirstVal;
    uint32 idx;
    AtOsal osal = AtSharedDriverOsalGet();
    static const uint8 cPrbsEngCheckTime = 50;

    prbsFirstVal = CpuRead(self, cThaDdrRegPrbsDataStat);
    for (idx = 0; idx < cPrbsEngCheckTime; idx++)
        {
        if (prbsFirstVal != CpuRead(self, cThaDdrRegPrbsDataStat))
            return cAtTrue;

        /* Give hardware a moment */
        mMethodsGet(osal)->USleep(osal, 10000);
        }

    /* It is really not running */
    return cAtFalse;
    }

static eBool PrbsError(AtRam self)
    {
    uint32 dRegVal;
    uint8 prbsErr;

    prbsErr = 0;
    dRegVal = CpuRead(self, cThaDdrRegPrbsErrStat);
    mFieldGet(dRegVal, cThaDdrPrbsErrStatMask, cThaDdrPrbsErrStatShift, uint8, &prbsErr);

    return (prbsErr > 0) ? cAtTrue : cAtFalse;
    }

static void DataModeSet(AtRam self)
    {
    uint32 regVal;
    AtOsal osal = AtSharedDriverOsalGet();

    regVal = CpuRead(self, cThaDdrRegPrbsCtrl);
    mFieldIns(&regVal, cThaDdrTestMdMask, cThaDdrTestMdShift, cTestAddress);
    CpuWrite(self, cThaDdrRegPrbsCtrl, regVal);
    mFieldIns(&regVal, cThaDdrTestMdMask, cThaDdrTestMdShift, cTestData);
    mFieldIns(&regVal, cThaDdrPrbsMdMask, cThaDdrPrbsMdShift, cPrbs23);
    CpuWrite(self, cThaDdrRegPrbsCtrl, regVal);

    if (!AtDeviceWarmRestoreIsStarted(AtModuleDeviceGet((AtModule)AtRamModuleGet(self))))
        mMethodsGet(osal)->USleep(osal, cWaitTimeToClearStatus);
    }

static eAtModuleRamRet MemoryTest(AtRam self, uint32 startAddress, uint32 endAddress, uint32 *firstErrorAddress, uint32 *errorCount)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 testingDurationMs = AtRamTestDurationGet(self);
    tAtOsalCurTime startTime, currentTime, cpuRest;
    uint32 elapseTime;
    eAtModuleRamRet ret;

    AtUnused(endAddress);
    AtUnused(startAddress);

    /* About first error address and error count, this DDR does not support */
    if (firstErrorAddress) *firstErrorAddress = 0;
    if (errorCount) *errorCount = 0;

    /* Only test when DDR is initialized properly */
    ret = AtRamInitStatusGet(self);
    if (ret != cAtOk)
        return ret;

    /* Change test mode to data (note, need to toggle these modes) */
    DataModeSet(self);

    /* Enable testing and make sure that PRBS engine is running */
    TestEnable(self, cAtTrue);
    if (!PrbsEngineIsRunning(self))
        {
        mThis(self)->errorStatus |= cErrorPrbsEngineNotRun;
        return cAtErrorMemoryTestFail;
        }

    /* Give hardware a moment then clear PRBS error status */
    mMethodsGet(osal)->USleep(osal, cWaitTimeToClearStatus);
    ClearPrbsErrorStatus(self);

    /* Calculate testing duration */
    if (testingDurationMs == 0)
        testingDurationMs = 30000;

    /* Save start testing time */
    mMethodsGet(osal)->CurTimeGet(osal, &startTime);
    mMethodsGet(osal)->CurTimeGet(osal, &cpuRest);

    /* Wait for diagnosing */
    elapseTime = 0;
    while (elapseTime < testingDurationMs)
        {
        /* PRBS error, need to exit */
        if (PrbsError(self))
            {
            TestEnable(self, cAtFalse);
            mThis(self)->errorStatus |= cErrorPrbsError;
            return cAtErrorMemoryTestFail;
            }

        /* Calculate elapse time */
        mMethodsGet(osal)->CurTimeGet(osal, &currentTime);
        elapseTime = mMethodsGet(osal)->DifferenceTimeInMs(osal, &currentTime, &startTime);

        /* Give CPU a rest */
        AtRamCpuRest(osal, &cpuRest);
        }

    /* Everything runs well, disable testing */
    TestEnable(self, cAtFalse);
    return cAtOk;
    }

static void Debug(AtRam self)
    {
    uint32 errorStatus = mThis(self)->errorStatus;

    m_AtRamMethods->Debug(self);

    AtPrintc(cSevNormal, "DDR %d rrror status:", AtRamIdGet(self) + 1);

    /* No error */
    if (errorStatus == 0)
        {
        AtPrintc(cSevInfo, " None\r\n");
        return;
        }

    /* Print error detail */
    if (errorStatus & cErrorPrbsEngineNotRun) AtPrintc(cSevInfo, " EngineNotRun");
    if (errorStatus & cErrorPrbsError)        AtPrintc(cSevInfo, " PrbsError");
    AtPrintc(cSevNormal, "\r\n");

    /* Clear status */
    mThis(self)->errorStatus = 0;
    }

static void OverrideAtRam(AtRam self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtRamMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtRamOverride, m_AtRamMethods, sizeof(m_AtRamOverride));

        mMethodOverride(m_AtRamOverride, Init);
        mMethodOverride(m_AtRamOverride, TestDurationSet);
        mMethodOverride(m_AtRamOverride, TestDurationGet);
        mMethodOverride(m_AtRamOverride, CellSizeGet);
        mMethodOverride(m_AtRamOverride, CellRead);
        mMethodOverride(m_AtRamOverride, CellWrite);
        mMethodOverride(m_AtRamOverride, AccessEnable);
        mMethodOverride(m_AtRamOverride, AccessIsEnabled);
        mMethodOverride(m_AtRamOverride, MemoryTest);
        mMethodOverride(m_AtRamOverride, InitStatusGet);
        mMethodOverride(m_AtRamOverride, Debug);
        }

    mMethodsSet(self, &m_AtRamOverride);
    }

static void Override(AtRam self)
    {
    OverrideAtRam(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaDdr2);
    }

static AtRam ObjectInit(AtRam self, AtModuleRam ramModule, AtIpCore core, uint8 ramId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaDdrObjectInit(self, ramModule, core, ramId) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtRam ThaDdr2New(AtModuleRam ramModule, AtIpCore core, uint8 ramId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtRam newDdr = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDdr == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newDdr, ramModule, core, ramId);
    }

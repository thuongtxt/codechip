/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : ThaModuleRamDdr2Reg.h
 *
 * Created Date: Mar 30, 2013
 *
 * Description : RAM register
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _THAPWMODULERAMPWDDRREG_H_
#define _THAPWMODULERAMPWDDRREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
/*------------------------------------------------------------------------------
Reg Name: DDR initialization control register
Reg Addr:
Reg Desc: This register is used to initialize DDR
------------------------------------------------------------------------------*/
#define cThaDdrRegInitCtrl              0xA

/*------------------------------------------------------------------------------
Reg Name: DDR Calib status
Reg Addr:
Reg Desc: The registers provide the ALTMEMPHY calibration process status
------------------------------------------------------------------------------*/
#define cThaDdrRegCalibStat                                 0x7

/*--------------------------------------
BitField Name: DDRCalibStat
BitField Type: R/W
BitField Desc: ALTMEMPHY calibration status
               0x1: Calibration OK
               other value: Calibration fail
--------------------------------------*/
#define cThaDdrCalibStatMask                                cBit2_0

/*------------------------------------------------------------------------------
Reg Name: DDR PRBS generate control register
Reg Addr:
Reg Desc: This register is used to enable generate PRBS to DDR for diagnostic
------------------------------------------------------------------------------*/
#define cThaDdrRegPrbsCtrl                               0xE

/*--------------------------------------
BitField Name: DdrTestMd
BitField Type: R/W
BitField Desc: Select testing mode
               - 1: Address mode
               - 2: Data mode
--------------------------------------*/
#define cThaDdrTestMdMask                                cBit5_4
#define cThaDdrTestMdShift                               4

/*--------------------------------------
BitField Name: DdrPrbsMd
BitField Type: R/W
BitField Desc: Select PRBS mode for DDR testing
               - 0: PRBS 23
               - 1: PRBS 9
--------------------------------------*/
#define cThaDdrPrbsMdMask                                cBit3_2
#define cThaDdrPrbsMdShift                               2

/*--------------------------------------
BitField Name: DdrPrbsTestEn
BitField Type: R/W
BitField Desc: Enable PRBS test for DDR
               - 1: Enable
               - 0: Disable
--------------------------------------*/
#define cThaDdrPrbsTestEnMask                                cBit1
#define cThaDdrPrbsTestEnShift                               1

/*--------------------------------------
BitField Name: DdrUserEn
BitField Type: R/W
BitField Desc: Enable user mode for DDR2
               - 1: Enable
               - 0: Disable
--------------------------------------*/
#define cThaDdrUserEnMask                                cBit0
#define cThaDdrUserEnShift                               0

/*------------------------------------------------------------------------------
Reg Name: DDR PRBS error status
Reg Addr:
Reg Desc: The registers provide the error report of PRBS test.
------------------------------------------------------------------------------*/
#define cThaDdrRegPrbsErrStat                            0x3F

/*--------------------------------------
BitField Name: DdrPrbsErrStat
BitField Type: R/W
BitField Desc: PRBS Error Report. For 16-bit DDR, the DdrPrbsErrStat[3:2] is unused.
               - 0x0: No error
               - otherwise: DDR error
--------------------------------------*/
#define cThaDdrPrbsErrStatMask                                cBit3_0
#define cThaDdrPrbsErrStatShift                               0

/*------------------------------------------------------------------------------
Reg Name: DDR PRBS data status
Reg Addr:
Reg Desc: The registers provide the current data of PRBS
------------------------------------------------------------------------------*/
#define cThaDdrRegPrbsDataStat                                0x29

/*------------------------------------------------------------------------------
Reg Name: DDR ready status
Reg Addr:
Reg Desc:
------------------------------------------------------------------------------*/
#define cThaDdrRegReadWriteRequest                            0x12

#define cThaDdrReadyMask                                      cBit31

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THAPWMODULERAMPWDDRREG_H_ */


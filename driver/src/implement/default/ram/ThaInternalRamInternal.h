/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : ThaInternalRamInternal.h
 * 
 * Created Date: Dec 4, 2015
 *
 * Description : Default internal RAM representation.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAINTERNALRAMINTERNAL_H_
#define _THAINTERNALRAMINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/ram/AtInternalRamInternal.h"
#include "../man/ThaDeviceInternal.h"
#include "ThaInternalRam.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* This default class only supports either Parity or CRC error. */
typedef struct tThaInternalRamMethods
    {
    /* Register */
    uint32 (*DisableCheckRegister)(ThaInternalRam self);
    uint32 (*ErrorForceRegister)(ThaInternalRam self);
    uint32 (*ErrorStickyRegister)(ThaInternalRam self);
    uint32 (*CounterTypeRegister)(ThaInternalRam self, uint16 counterType, eBool r2c);

    /* Hardware access */
    eAtRet (*ErrorMonitorEnable)(ThaInternalRam self, eBool enable);
    eBool (*ErrorMonitorIsEnabled)(ThaInternalRam self);
    uint32 (*ErrorRead2Clear)(ThaInternalRam self, eBool clear);
    eAtRet (*HwForcedErrorSet)(ThaInternalRam self, uint32 error, eBool enable);
    uint32 (*HwForcedErrorGet)(ThaInternalRam self);

    /* This default implement only supports either Parity or CRC error. */
    uint32 (*ErrorMask)(ThaInternalRam self);
    uint32 (*LocalIdMask)(ThaInternalRam self);

    eBool (*ErrorGeneratorIsSupported)(ThaInternalRam self);
    AtErrorGenerator (*ErrorGeneratorObjectCreate)(ThaInternalRam self);
    uint32 (*FirstCellAddress)(ThaInternalRam self); /* Address of the first RAM cell to force parity error on it. */
    eAtRet (*ParityVerifyingTrigger)(ThaInternalRam self);
    }tThaInternalRamMethods;

typedef struct tThaInternalRam
    {
    tAtInternalRam super;
    const tThaInternalRamMethods* methods;

    /* Private data */
    AtErrorGenerator errorGenerator;
    }tThaInternalRam;

/* This class supports ECC only. */
typedef struct tThaInternalRamEccMethods
    {
    uint32 (*CorrectableMask)(ThaInternalRamEcc self);
    uint32 (*UncorrectableMask)(ThaInternalRamEcc self);
    }tThaInternalRamEccMethods;

typedef struct tThaInternalRamEcc
    {
    tThaInternalRam super;
    const tThaInternalRamEccMethods* methods;
    }tThaInternalRamEcc;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtInternalRam ThaInternalRamObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId);
AtInternalRam ThaInternalRamEccObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId);

#ifdef __cplusplus
}
#endif
#endif /* _THAINTERNALRAMINTERNAL_H_ */


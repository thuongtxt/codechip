/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ZBT
 * 
 * File        : ThaZbt.h
 * 
 * Created Date: Feb 1, 2013
 *
 * Description : Thalassa default ZBT
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAZBT_H_
#define _THAZBT_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtZbt.h" /* Super class */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtRam ThaZbtNew(AtModuleRam ramModule, AtIpCore core, uint8 ramId);

#ifdef __cplusplus
}
#endif
#endif /* _THAZBT_H_ */


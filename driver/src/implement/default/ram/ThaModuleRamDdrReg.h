/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : ThaModuleRamReg.h
 * 
 * Created Date: Feb 1, 2013
 *
 * Description : RAM register
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULERAMDDRREG_H_
#define _THAMODULERAMDDRREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
/*==============================================================================
Reg Name:
Address :
Description:
==============================================================================*/
#define cThaRegDdrTestEnable 0x980001

/*--------------------------------------
BitField Name:
BitField Type:
BitField Desc:
--------------------------------------*/
#define cThaRegDdrTestEnableMask  cBit12
#define cThaRegDdrTestEnableShift 12

/*==============================================================================
Reg Name:
Address :
Description:
==============================================================================*/
#define cThaRegDdrClockCounterReadOnly 0x980040
#define cThaRegDdrIdleCounterReadOnly  0x980041
#define cThaRegDdrWriteCounterReadOnly 0x980042
#define cThaRegDdrReadCounterReadOnly  0x980043
#define cThaRegDdrErrorCounterReadOnly 0x980241

#define cThaRegDdrClockCounterRead2Clear 0x980050
#define cThaRegDdrIdleCounterRead2Clear  0x980051
#define cThaRegDdrWriteCounterRead2Clear 0x980052
#define cThaRegDdrReadCounterRead2Clear  0x980053
#define cThaRegDdrErrorCounterRead2Clear 0x980251

/*==============================================================================
Reg Name:
Address :
Description:
==============================================================================*/
#define cThaRegDdrErrorStatus 0x980201

#define cThaRegDdrErrorStatusDataErrorMask  cBit1
#define cThaRegDdrErrorStatusDataErrorShift 1

/*==============================================================================
Reg Name:
Address :
Description:
==============================================================================*/
#define cThaRegDdrForceError 0x980200

#define cThaRegDdrForceErrorMask  cBit1
#define cThaRegDdrForceErrorShift 1

/*==============================================================================
Reg Name:
Address :
Description:
==============================================================================*/
#define cThaRegDdrDiagEnable             0x0
#define cThaDdrDiagEnableMask            cBit0
#define cThaDdrDiagEnableShift           0
#define cThaDdrDiagHwTestEnableMask      cBit1
#define cThaDdrDiagHwTestEnableShift     1
#define cThaDdrDiagHwTestForceErrorMask  cBit2
#define cThaDdrDiagHwTestForceErrorShift 2

#define cThaRegDdrDiagStatus            0x1
#define cThaDdrDiagHwTestErrorMask      cBit0
#define cThaDdrDiagHwTestErrorShift     0
#define cThaDdrDiagCalibStateMask       cBit2_1
#define cThaDdrDiagCalibStateShift      1
#define cThaDdrDiagInitFailMask         cBit3
#define cThaDdrDiagInitFailShift        3

#define cThaRegDdrDiagErrDataMsb        0x2
#define cThaRegDdrDiagErrDataLsb        0x3
#define cThaRegDdrDiagErrAddr           0x4

#define cThaRegDdrBurst                 0x5
#define cThaDdrBurstMask                cBit3_0
#define cThaDdrBurstShift               0

#define cThaRegDdrDiagControl0          0x10
#define cThaRegDdrDiagControl1          0x11
#define cThaRegDdrDiagWriteDataMsb      0x12
#define cThaRegDdrDiagWriteDataLsb      0x13
#define cThaRegDdrDiagExpReadDataMsb    0x14
#define cThaRegDdrDiagExpReadDataLsb    0x15

#define cThaRegDdrDiagReadDataMsb       0x20
#define cThaRegDdrDiagReadDataLsb       0x21

#define cThaRegDdrDiagErrCounter        0x40
#define cThaRegDdrDiagReadCounter       0x41
#define cThaRegDdrDiagWriteCounter      0x42

#define cThaDebugDdr1Sticky0      0x980034
#define cThaDebugDdr2Sticky0      0x990034

#define cThaDebugDdr1Sticky1      0x980035
#define cThaDebugDdr2Sticky1      0x990035

#define cThaDebugDdr1Status0      0x980038
#define cThaDebugDdr2Status0      0x990038

#define cThaDebugDdr1Status1      0x980039
#define cThaDebugDdr2Status1      0x990039

/*------------------------------------------------------------------------------
Reg Name   : DDR Error Data Latch
Reg Addr   : 0x80 - 0x8F
Reg Formula: 0x80 + word_id
    Where  :
           + $word_id(0-15)
Reg Desc   :
Fist error pattern latch register, includes maximum 16x32bit words for 64b DDR interface. Write to clear the latched value, and start the next latched pattern

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_err_dat_Base                                                                         0x80

/*--------------------------------------
BitField Name: err_data
BitField Type: RWC
BitField Desc: DDR Error Data  Latch
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_err_dat_err_data_Mask                                                               cBit31_0
#define cAf6_upen_err_dat_err_data_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : DDR Expected Data Latch
Reg Addr   : 0x90 - 0x9F
Reg Formula: 0x90 + word_id
    Where  :
           + $word_id(0-15)
Reg Desc   :
Expected pattern latch register, latched with the first error occur, includes maximumx 16x32bit words for 64b DDR interface. This register is cleared and latched for the next error/expected value, when the Error Data Latch register is cleared.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_exp_dat_Base                                                                         0x90

/*--------------------------------------
BitField Name: exp_data
BitField Type: RW
BitField Desc: DDR Expected Data Latched
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_exp_dat_exp_data_Mask                                                               cBit31_0
#define cAf6_upen_exp_dat_exp_data_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : DDR Error Data Bit
Reg Addr   : 0xA0 - 0xAF
Reg Formula: 0xA0 + word_id
    Where  :
           + $word_id(0-15)
Reg Desc   :
maximum 16x32bit words for 64b DDR interface. Used to accumulated indicate the error position. When Error occur, the correlative bit of this register is set when the read data and the expected data is unmatch. Write 0 to clear the whole pattern.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_err_pat_Base                                                                         0xA0

/*--------------------------------------
BitField Name: err_pat
BitField Type: RW
BitField Desc: DDR Error Data Bit
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_err_pat_err_pat_Mask                                                                cBit31_0
#define cAf6_upen_err_pat_err_pat_Shift                                                                      0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULERAMDDRREG_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : ThaInternalRam.c
 *
 * Created Date: Dec 4, 2015
 *
 * Description : Default internal RAM implementation to support either parity or
 *               CRC error.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../util/coder/AtCoderUtil.h"
#include "../man/ThaDevice.h"
#include "ThaInternalRamInternal.h"
#include "ThaModuleRamInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaInternalRam)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaInternalRamMethods m_methods;

/* Override. */
static tAtObjectMethods m_AtObjectOverride;
static tAtInternalRamMethods m_AtInternalRamOverride;

/* To save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet ParityMonitorEnable(AtInternalRam self, eBool enable)
    {
    if (AtInternalRamIsReserved(self))
        return cAtErrorModeNotSupport;

    if (!AtInternalRamParityMonitorIsSupported(self))
        return cAtErrorModeNotSupport;

    return mMethodsGet(mThis(self))->ErrorMonitorEnable(mThis(self), enable);
    }

static eBool ParityMonitorIsEnabled(AtInternalRam self)
    {
    if (AtInternalRamIsReserved(self))
        return cAtFalse;

    if (!AtInternalRamParityMonitorIsSupported(self))
        return cAtFalse;

    return mMethodsGet(mThis(self))->ErrorMonitorIsEnabled(mThis(self));
    }

static eBool ParityMonitorIsSupported(AtInternalRam self)
    {
    AtUnused(self);
    return (AtInternalRamIsReserved(self) ? cAtFalse : cAtTrue);
    }

static eAtRet CrcMonitorEnable(AtInternalRam self, eBool enable)
    {
    if (AtInternalRamIsReserved(self))
        return cAtErrorModeNotSupport;

    if (!AtInternalRamCrcMonitorIsSupported(self))
        return cAtErrorModeNotSupport;

    return mMethodsGet(mThis(self))->ErrorMonitorEnable(mThis(self), enable);
    }

static eBool CrcMonitorIsEnabled(AtInternalRam self)
    {
    if (AtInternalRamIsReserved(self))
        return cAtFalse;

    if (!AtInternalRamCrcMonitorIsSupported(self))
        return cAtFalse;

    return mMethodsGet(mThis(self))->ErrorMonitorIsEnabled(mThis(self));
    }

static eBool CrcMonitorIsSupported(AtInternalRam self)
    {
    if (AtInternalRamIsReserved(self))
        return cAtFalse;
        
    return mMethodsGet(self)->ParityMonitorIsSupported(self) ? cAtFalse : cAtTrue;
    }

static eAtRet ParityCalculatingTrigger(AtInternalRam self)
    {
    AtModule module = AtInternalRamPhyModuleGet(self);
    uint32 regAddr, regVal;

    /* Make a write action to trigger HW update the parity on RAM */
    regAddr = mMethodsGet(mThis(self))->FirstCellAddress(mThis(self));
    if (regAddr == cInvalidUint32)
        return cAtErrorInvalidAddress;

    regVal = mModuleHwRead(module, regAddr);
    mModuleHwWrite(module, regAddr, regVal);

    return cAtOk;
    }

static eAtRet ParityVerifyingTrigger(ThaInternalRam self)
    {
    AtModule module = AtInternalRamPhyModuleGet((AtInternalRam)self);
    uint32 regAddr;

    /* Make a read action to trigger HW verify the parity on RAM */
    regAddr = mMethodsGet(self)->FirstCellAddress(self);
    if (regAddr == cInvalidUint32)
        return cAtErrorInvalidAddress;

    mModuleHwRead(module, regAddr);
    return cAtOk;
    }

static eAtRet ForceErrorToRam(AtInternalRam self, uint32 errors, eBool force)
    {
    eAtRet ret;

    if (AtInternalRamIsReserved(self))
        return cAtErrorModeNotSupport;

    if (errors & ~AtInternalRamForcableErrorsGet(self))
        return cAtErrorModeNotSupport;

    ret = mMethodsGet(mThis(self))->HwForcedErrorSet(mThis(self), errors, force);

    /* Only support parity error */
    if (errors & cAtRamAlarmParityError)
        ret |= ParityCalculatingTrigger(self);

    return ret;
    }

static uint32 RamError2GeneratorError(uint32 errors)
    {
    if (errors & cAtRamAlarmEccCorrectable)     return cAtRamGeneratorErrorTypeEccCorrectable;
    if (errors & cAtRamAlarmEccUnnorrectable)   return cAtRamGeneratorErrorTypeEccUncorrectable;
    if (errors & cAtRamAlarmCrcError)           return cAtRamGeneratorErrorTypeCrcError;
    return cAtRamGeneratorErrorTypeInvalid;
    }

static eAtRet ErrorForceByGenerator(AtInternalRam self, uint32 errors, eBool force)
    {
    eAtRet ret = cAtOk;
    AtErrorGenerator errorGenerator = NULL;

    if (AtInternalRamIsReserved(self))
        return cAtErrorModeNotSupport;

    if (errors & ~AtInternalRamForcableErrorsGet(self))
        return cAtErrorModeNotSupport;

    errorGenerator = AtInternalRamErrorGeneratorGet(self);

    if (errorGenerator == NULL)
        return ForceErrorToRam(self, errors, force);

    /* In case error generator supported, we should redirectly control via it. */
    ret |= AtErrorGeneratorErrorNumSet(errorGenerator, 1);
    ret |= AtErrorGeneratorModeSet(errorGenerator, cAtErrorGeneratorModeOneshot);
    ret |= AtErrorGeneratorErrorTypeSet(errorGenerator, RamError2GeneratorError(errors));
    if (force)
        ret |= AtErrorGeneratorStart(errorGenerator);
    else
        ret |= AtErrorGeneratorStop(errorGenerator);

    return ret;
    }

static eAtRet HelperErrorForce(AtInternalRam self, uint32 errors, eBool force)
    {
    if (mMethodsGet(mThis(self))->ErrorGeneratorIsSupported(mThis(self)))
        return ErrorForceByGenerator(self, errors, force);
    return ForceErrorToRam(self, errors, force);
    }

static eAtRet ErrorForce(AtInternalRam self, uint32 errors)
    {
    return HelperErrorForce(self, errors, cAtTrue);
    }

static eAtRet ErrorUnForce(AtInternalRam self, uint32 errors)
    {
    return HelperErrorForce(self, errors, cAtFalse);
    }

static uint32 ForcedErrorsGet(AtInternalRam self)
    {
    if (AtInternalRamIsReserved(self))
        return 0;

    return mMethodsGet(mThis(self))->HwForcedErrorGet(mThis(self));
    }

static uint32 ForcableErrorsGet(AtInternalRam self)
    {
    return AtInternalRamIsReserved(self) ? 0 : cAtRamAlarmParityError;
    }

static uint32 ErrorHistoryGet(AtInternalRam self)
    {
    if (AtInternalRamIsReserved(self))
        return 0;
        
    return mMethodsGet(mThis(self))->ErrorRead2Clear(mThis(self), cAtFalse);
    }

static uint32 ErrorHistoryClear(AtInternalRam self)
    {
    if (AtInternalRamIsReserved(self))
        return 0;
        
    return mMethodsGet(mThis(self))->ErrorRead2Clear(mThis(self), cAtTrue);
    }

static ThaModuleRam RamModule(AtInternalRam self)
    {
    return (ThaModuleRam)AtInternalRamModuleRamGet(self);
    }

static const char* Name(AtInternalRam self)
    {
    uint32 moduleId = AtModuleTypeGet(AtInternalRamPhyModuleGet(self));
    uint32 numRams;
    const char ** description = ThaModuleRamPhyModuleRamDescription(RamModule(self), moduleId, &numRams);
    uint32 localRamId = AtInternalRamLocalIdGet(self);
    return (localRamId < numRams) ? description[localRamId] : NULL;
    }

static const char* Description(AtInternalRam self)
    {
    if (AtInternalRamIsReserved(self))
        return "Reserved";

    return AtInternalRamName(self);
    }

static uint32 ErrorForceRegister(ThaInternalRam self)
    {
    AtUnused(self);
    /* Default common RTL logic contains the set of three registers:
     * Force Error      : X
     * Disable Check    : X + 1
     * Sticky Error     : X + 2
     * Some logic can not follow this rule.
     */
    return 0x107;
    }

static uint32 CounterTypeRegister(ThaInternalRam self, uint16 counterType, eBool r2c)
    {
    AtUnused(self);
    AtUnused(counterType);
    AtUnused(r2c);
    return 0;
    }

static uint32 DisableCheckRegister(ThaInternalRam self)
    {
    return mMethodsGet(self)->ErrorForceRegister(self) + 1;
    }

static uint32 ErrorStickyRegister(ThaInternalRam self)
    {
    return mMethodsGet(self)->ErrorForceRegister(self) + 2;
    }

static eAtRet ErrorMonitorEnable(ThaInternalRam self, eBool enable)
    {
    AtModule module = AtInternalRamPhyModuleGet((AtInternalRam)self);
    uint32 regAddr = mMethodsGet(self)->DisableCheckRegister(self);
    uint32 regVal  = mModuleHwRead(module, regAddr);
    uint32 regMask = mMethodsGet(self)->LocalIdMask(self);

    regVal = (enable) ? (regVal & ~regMask) : (regVal | regMask);
    mModuleHwWrite(module, regAddr, regVal);

    return cAtOk;
    }

static eBool ErrorMonitorIsEnabled(ThaInternalRam self)
    {
    AtModule module = AtInternalRamPhyModuleGet((AtInternalRam)self);
    uint32 regAddr = mMethodsGet(mThis(self))->DisableCheckRegister(mThis(self));
    uint32 regVal  = mModuleHwRead(module, regAddr);
    uint32 regMask = mMethodsGet(self)->LocalIdMask(self);
    return (regMask & regVal) ? cAtFalse : cAtTrue;
    }

static uint32 ErrorRead2Clear(ThaInternalRam self, eBool clear)
    {
    AtModule module = AtInternalRamPhyModuleGet((AtInternalRam)self);
    uint32 regAddr, regVal;
    uint32 regMask = mMethodsGet(self)->LocalIdMask(self);

    if (AtInternalRamForcableErrorsGet((AtInternalRam)self) & cAtRamAlarmParityError)
        mMethodsGet(self)->ParityVerifyingTrigger(self);

    regAddr = mMethodsGet(self)->ErrorStickyRegister(self);
    regVal  = mModuleHwRead(module, regAddr);

    if (clear)
        mModuleHwWrite(module, regAddr, regMask);

    if (regVal & regMask)
        return mMethodsGet(self)->ErrorMask(self);

    return 0;
    }

static eAtRet HwForcedErrorSet(ThaInternalRam self, uint32 errors, eBool enable)
    {
    AtModule module = AtInternalRamPhyModuleGet((AtInternalRam)self);
    uint32 regAddr = mMethodsGet(mThis(self))->ErrorForceRegister(mThis(self));
    uint32 regVal  = mModuleHwRead(module, regAddr);
    uint32 regMask = mMethodsGet(self)->LocalIdMask(mThis(self));

    AtUnused(errors);

    regVal = (enable) ? (regVal | regMask) : (regVal & ~regMask);
    mModuleHwWrite(module, regAddr, regVal);

    return cAtOk;
    }

static uint32 HwForcedErrorGet(ThaInternalRam self)
    {
    AtModule module = AtInternalRamPhyModuleGet((AtInternalRam)self);
    uint32 regAddr = mMethodsGet(mThis(self))->ErrorForceRegister(mThis(self));
    uint32 regVal  = mModuleHwRead(module, regAddr);
    uint32 regMask = mMethodsGet(self)->LocalIdMask(mThis(self));
    if (regVal & regMask)
        return mMethodsGet(mThis(self))->ErrorMask(mThis(self));
    return 0;
    }

static uint32 ErrorMask(ThaInternalRam self)
    {
    AtUnused(self);
    return cAtRamAlarmParityError;
    }

static uint32 LocalIdMask(ThaInternalRam self)
    {
    return cBit0 << (AtInternalRamLocalIdGet((AtInternalRam)self) & 0x1F);
    }

static AtErrorGenerator ErrorGeneratorObjectCreate(ThaInternalRam self)
    {
    ThaModuleRam moduleRam = (ThaModuleRam)AtInternalRamModuleRamGet((AtInternalRam)self);
    return ThaModuleRamErrorGeneratorObjectCreate(moduleRam, (AtInternalRam)self);
    }

static eBool ErrorGeneratorIsSupported(ThaInternalRam self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static AtErrorGenerator ErrorGeneratorGet(AtInternalRam self)
    {
    if (mThis(self)->errorGenerator)
        return mThis(self)->errorGenerator;

    if (!mMethodsGet(mThis(self))->ErrorGeneratorIsSupported(mThis(self)))
        return NULL;

    mThis(self)->errorGenerator = mMethodsGet(mThis(self))->ErrorGeneratorObjectCreate(mThis(self));
    return mThis(self)->errorGenerator;
    }

static uint32 FirstCellAddress(ThaInternalRam self)
    {
    uint32 moduleId = AtModuleTypeGet(AtInternalRamPhyModuleGet((AtInternalRam)self));
    uint32 localRamId = AtInternalRamLocalIdGet((AtInternalRam)self);
    return ThaModuleRamPhyModuleRamLocalCellAddress(RamModule((AtInternalRam)self), moduleId, localRamId);
    }

static void Delete(AtObject self)
    {
    if (mThis(self)->errorGenerator)
        {
        AtObjectDelete((AtObject)mThis(self)->errorGenerator);
        mThis(self)->errorGenerator = NULL;
        }

    /* Call super function to fully delete */
    m_AtObjectMethods->Delete((AtObject)self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaInternalRam object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObject(errorGenerator);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaInternalRam);
    }

static void OverrideAtInternalRam(AtInternalRam self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtInternalRamOverride, mMethodsGet(self), sizeof(m_AtInternalRamOverride));

        /* Common */
        mMethodOverride(m_AtInternalRamOverride, ParityMonitorEnable);
        mMethodOverride(m_AtInternalRamOverride, ParityMonitorIsEnabled);
        mMethodOverride(m_AtInternalRamOverride, ParityMonitorIsSupported);
        mMethodOverride(m_AtInternalRamOverride, CrcMonitorEnable);
        mMethodOverride(m_AtInternalRamOverride, CrcMonitorIsEnabled);
        mMethodOverride(m_AtInternalRamOverride, CrcMonitorIsSupported);
        mMethodOverride(m_AtInternalRamOverride, ErrorForce);
        mMethodOverride(m_AtInternalRamOverride, ErrorUnForce);
        mMethodOverride(m_AtInternalRamOverride, ForcedErrorsGet);
        mMethodOverride(m_AtInternalRamOverride, ForcableErrorsGet);
        mMethodOverride(m_AtInternalRamOverride, ErrorHistoryClear);
        mMethodOverride(m_AtInternalRamOverride, ErrorHistoryGet);
        mMethodOverride(m_AtInternalRamOverride, Description);
        mMethodOverride(m_AtInternalRamOverride, ErrorGeneratorGet);
        mMethodOverride(m_AtInternalRamOverride, Name);
        }

    mMethodsSet(self, &m_AtInternalRamOverride);
    }

static void OverrideAtObject(AtInternalRam self)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtObject ramObject = (AtObject)self;

    if (!m_methodsInit)
        {
        m_AtObjectMethods = mMethodsGet(ramObject);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(ramObject, &m_AtObjectOverride);
    }

static void Override(AtInternalRam self)
    {
    OverrideAtInternalRam(self);
    OverrideAtObject(self);
    }

static void MethodsInit(ThaInternalRam self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Common */
        mMethodOverride(m_methods, CounterTypeRegister);
        mMethodOverride(m_methods, DisableCheckRegister);
        mMethodOverride(m_methods, ErrorForceRegister);
        mMethodOverride(m_methods, ErrorStickyRegister);
        mMethodOverride(m_methods, ErrorMonitorEnable);
        mMethodOverride(m_methods, ErrorMonitorIsEnabled);
        mMethodOverride(m_methods, ErrorRead2Clear);
        mMethodOverride(m_methods, HwForcedErrorSet);
        mMethodOverride(m_methods, HwForcedErrorGet);
        mMethodOverride(m_methods, ErrorMask);
        mMethodOverride(m_methods, LocalIdMask);
        mMethodOverride(m_methods, FirstCellAddress);
        mMethodOverride(m_methods, ErrorGeneratorObjectCreate);
        mMethodOverride(m_methods, ErrorGeneratorIsSupported);
        mMethodOverride(m_methods, ParityVerifyingTrigger);
        }

    mMethodsSet(self, &m_methods);
    }

AtInternalRam ThaInternalRamObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtInternalRamObjectInit(self, phyModule, ramId, localId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtInternalRam ThaInternalRamNew(AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInternalRam newRam = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newRam == NULL)
        return NULL;

    return ThaInternalRamObjectInit(newRam, phyModule, ramId, localId);
    }

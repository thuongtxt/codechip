/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : ThaModuleRamZbtReg.h
 * 
 * Created Date: Feb 21, 2013
 *
 * Description : ZBT register
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULERAMZBTREG_H_
#define _THAMODULERAMZBTREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
/*==============================================================================
Reg Name:
Address :
Description:
==============================================================================*/
#define cThaRegZbtTestEnable 0xf30000

/*==============================================================================
Reg Name:
Address :
Description:
==============================================================================*/
#define cThaRegZbtTestClockSetup 0xf30004

/*==============================================================================
Reg Name:
Address :
Description:
==============================================================================*/
#define cThaRegZbtTestSetup 0xf30001

/*==============================================================================
Reg Name:
Address :
Description:
==============================================================================*/
#define cThaRegZbtTestNumRegsToTest 0xf30002

/*==============================================================================
Reg Name:
Address :
Description:
==============================================================================*/
#define cThaRegZbtTestSetupStatus 0xf30006

/*==============================================================================
Reg Name:
Address :
Description:
==============================================================================*/
#define cThaRegZbtTestErrorCounters 0xf30005

#define cThaRegZbtTestErrorCountersMask  cBit15_0
#define cThaRegZbtTestErrorCountersShift 0

/*==============================================================================
Reg Name:
Address :
Description:
==============================================================================*/
#define cThaRegZbtTestForceError 0xf30008

/*==============================================================================
Reg Name:
Address :
Description:
==============================================================================*/
#define cThaRegZbtBase 0xFC0000

/*==============================================================================
Reg Name:
Address :
Description:
==============================================================================*/
#define cThaRegZbtRangeSelect 0xF80009

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULERAMZBTREG_H_ */


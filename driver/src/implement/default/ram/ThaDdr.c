/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : DDR
 *
 * File        : ThaDdr.c
 *
 * Created Date: Jan 31, 2013
 *
 * Description : Thalassa DDR implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../generic/man/AtDeviceInternal.h"
#include "../../../generic/ram/AtModuleRamInternal.h"
#include "../../../util/coder/AtCoderUtil.h"
#include "ThaModuleRamDdrReg.h"
#include "ThaDdrInternal.h"
#include "ThaModuleRam.h"

/*--------------------------- Define -----------------------------------------*/
#define cRdWrModeReadOnly     0
#define cRdWrModeWriteOnly    1
#define cRdWrModeReadAndWrite 2

#define cDdrRdWrTimeout 60000 /* 60 seconds */

/*--------------------------- Macros -----------------------------------------*/
#define mDdrAddressOffset(self) (mMethodsGet((ThaDdr)self)->DefaultOffset((ThaDdr)self) + mMethodsGet((ThaDdr)self)->BaseAddress((ThaDdr)self))
#define mThis(self) ((ThaDdr)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaDdrMethods m_methods;

/* Override */
static tAtRamMethods m_AtRamOverride;
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtRamMethods *m_AtRamMethods = NULL;
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaDdr);
    }

static void DeviceWrite(AtRam self, uint32 address, uint32 value)
    {
    AtHal hal = AtIpCoreHalGet(AtRamIpCoreGet(self));
    AtHalWrite(hal, address, value);
    }

static uint32 DeviceRead(AtRam self, uint32 address)
    {
    AtHal hal = AtIpCoreHalGet(AtRamIpCoreGet(self));
    uint32 value = AtHalRead(hal, address);
    return value;
    }

static uint32 DefaultOffset(ThaDdr self)
    {
    return (0x2000UL * AtRamIdGet((AtRam)self));
    }

static uint32 BaseAddress(ThaDdr self)
    {
	AtUnused(self);
    return 0xF20000;
    }

static uint32 CellSizeGet(AtRam self)
    {
	AtUnused(self);
    return 2;
    }

static eBool IsSimulated(AtRam self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)AtRamModuleGet(self));
    return AtDeviceIsSimulated(device);
    }

static AtDevice Device(AtRam self)
    {
    return AtModuleDeviceGet((AtModule)AtRamModuleGet(self));
    }

static uint32 TimeoutMs(AtRam self)
    {
    if (AtDeviceTestbenchIsEnabled(Device(self)))
        return AtDeviceTestbenchDefaultTimeoutMs();

    AtUnused(self);
    return cDdrRdWrTimeout;
    }

static eBool ReadWriteRequest(ThaDdr self, uint32 startAddress, uint32 step, uint32 count, eBool isIncreased, uint8 mode)
    {
    uint32 regValue;
    uint32 elapsedTime;
    tAtOsalCurTime startTime, currentTime, previousTime;
    uint32 ddrCtrlAddr;
    AtOsal osal;
    uint8 decrease = isIncreased ? 0 : 1;
    uint32 timeoutMs = TimeoutMs((AtRam)self);

    regValue = (startAddress & cBit24_0) | (step << 28);
    DeviceWrite((AtRam)self, cThaRegDdrDiagControl1 + mDdrAddressOffset(self), regValue);

    regValue = (uint32)(mode & cBit2_0) | (uint32)(decrease << 3) | (uint32)((count << 4) & cBit28_4) | cBit31;
    ddrCtrlAddr = cThaRegDdrDiagControl0 + mDdrAddressOffset(self);
    DeviceWrite((AtRam)self, ddrCtrlAddr, regValue);

    /* Do not need to always wait for a long time, but on testbench we should */
    if ((count <= 1) && (!AtDeviceTestbenchIsEnabled(Device((AtRam)self))))
        timeoutMs = 100;

    /* Check if hardware done */
    elapsedTime = 0;
    osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->CurTimeGet(osal, &startTime);
    mMethodsGet(osal)->MemCpy(osal, &previousTime, &startTime, sizeof(tAtOsalCurTime));
    while (elapsedTime < timeoutMs)
        {
        regValue = DeviceRead((AtRam)self, ddrCtrlAddr);
        if ((regValue & cBit31) == 0) /* Operation is done */
            return cAtTrue;

        if (IsSimulated((AtRam)self))
            return cAtTrue;

        /* Or retry */
        mMethodsGet(osal)->CurTimeGet(osal, &currentTime);
        elapsedTime = mTimeIntervalInMsGet(startTime, currentTime);

        /* But try to give CPU a rest */
        AtRamCpuRest(osal, &previousTime);
        }

    return cAtFalse;
    }

static eAtRet CellRead(AtRam self, uint32 address, uint32 *value)
    {
    uint32 i;

    if (ReadWriteRequest((ThaDdr)self, address, 1, 0, cAtTrue, cRdWrModeReadOnly) == cAtTrue)
        {
        uint32 addrOffset = mDdrAddressOffset(self);

        value[0] = DeviceRead(self, cThaRegDdrDiagReadDataLsb + addrOffset);
        value[1] = DeviceRead(self, cThaRegDdrDiagReadDataMsb + addrOffset);

        return cAtOk;
        }

    for (i = 0; i < mMethodsGet(self)->CellSizeGet(self); i++)
        value[i] = 0xCAFECAFE;

    return cAtErrorIndrAcsTimeOut;
    }

static eAtRet CellWrite(AtRam self, uint32 address, const uint32 *value)
    {
    uint32 addrOffset = mDdrAddressOffset(self);

    DeviceWrite(self, cThaRegDdrDiagWriteDataLsb + addrOffset, value[0]);
    DeviceWrite(self, cThaRegDdrDiagWriteDataMsb + addrOffset, value[1]);

    if (!ReadWriteRequest((ThaDdr)self, address, 1, 0, cAtTrue, cRdWrModeWriteOnly))
        return cAtErrorIndrAcsTimeOut;

    return cAtOk;
    }

static eAtModuleRamRet MemoryFill(AtRam self, eBool addressIsIncreased, uint32 data, uint32 startAddress, uint32 endAddress)
    {
    uint32 count;
    uint32 start;
    uint32 addrOffset;

    if (!AtRamTestHwAssistIsEnabled(self))
        return m_AtRamMethods->MemoryFill(self, addressIsIncreased, data, startAddress, endAddress);

    addrOffset = mDdrAddressOffset(self);
    count = endAddress - startAddress;
    start = (addressIsIncreased)? startAddress : endAddress;

    DeviceWrite(self, cThaRegDdrDiagWriteDataLsb + addrOffset, data);
    DeviceWrite(self, cThaRegDdrDiagWriteDataMsb + addrOffset, data);

    if (ReadWriteRequest((ThaDdr)self, start, 1, count, addressIsIncreased, cRdWrModeWriteOnly) == cAtFalse)
        return cAtErrorDevBusy;

    return cAtOk;
    }

static eAtModuleRamRet MemoryReadCheck(AtRam self,
									   eBool addressIsIncreased,
									   uint32 expectedData,
									   uint32 startAddress,
									   uint32 endAddress,
									   uint32 *firstErrorAddress,
									   uint32 *errorCount)
    {
    uint32 count;
    uint32 start;
    uint32 addrOffset;

    if (!AtRamTestHwAssistIsEnabled(self))
        return m_AtRamMethods->MemoryReadCheck(self,
                                               addressIsIncreased,
                                               expectedData,
                                               startAddress,
                                               endAddress,
                                               firstErrorAddress,
                                               errorCount);

    addrOffset = mDdrAddressOffset(self);
    count = endAddress - startAddress;
    start = (addressIsIncreased)? startAddress : endAddress;

    /* Clear counter */
    DeviceWrite(self, cThaRegDdrDiagErrCounter + addrOffset, 0);

    /* Test execute */
    DeviceWrite(self, cThaRegDdrDiagExpReadDataMsb + addrOffset, expectedData);
    DeviceWrite(self, cThaRegDdrDiagExpReadDataLsb + addrOffset, expectedData);

    if (ReadWriteRequest((ThaDdr)self, start, 1, count, addressIsIncreased, cRdWrModeReadOnly) == cAtFalse)
        return cAtErrorDevBusy;

    *errorCount = DeviceRead((AtRam)self, cThaRegDdrDiagErrCounter + addrOffset);
    if (*errorCount != 0)
        *firstErrorAddress = DeviceRead((AtRam)self, cThaRegDdrDiagErrAddr + addrOffset);

    return cAtOk;
    }

static eAtModuleRamRet MemoryReadWriteCheck(AtRam self,
										    eBool addressIsIncreased,
										    uint32 expectedReadData,
										    uint32 writeData,
										    uint32 startAddress,
										    uint32 endAddress,
										    uint32 *firstErrorAddress,
										    uint32 *errorCount)
    {
    uint32 count;
    uint32 addrOffset;
    uint32 correctStartAddress;

    if (!AtRamTestHwAssistIsEnabled(self))
        return m_AtRamMethods->MemoryReadWriteCheck(self,
                                                    addressIsIncreased,
                                                    expectedReadData,
                                                    writeData,
                                                    startAddress,
                                                    endAddress,
                                                    firstErrorAddress,
                                                    errorCount);

    addrOffset = mDdrAddressOffset(self);
    count = endAddress - startAddress;

    /* Clear counter */
    DeviceWrite(self, cThaRegDdrDiagErrCounter + addrOffset, 0);

    /* Test execute */
    DeviceWrite(self, cThaRegDdrDiagExpReadDataMsb + addrOffset, expectedReadData);
    DeviceWrite(self, cThaRegDdrDiagExpReadDataLsb + addrOffset, expectedReadData);

    DeviceWrite(self, cThaRegDdrDiagWriteDataMsb + addrOffset, writeData);
    DeviceWrite(self, cThaRegDdrDiagWriteDataLsb + addrOffset, writeData);

    correctStartAddress = addressIsIncreased ? startAddress : endAddress;
    if (ReadWriteRequest((ThaDdr)self, correctStartAddress, 1, count, addressIsIncreased, cRdWrModeReadAndWrite) == cAtFalse)
        return cAtErrorDevBusy;

    *errorCount = DeviceRead((AtRam)self, cThaRegDdrDiagErrCounter + addrOffset);
    if (*errorCount != 0)
        *firstErrorAddress = DeviceRead((AtRam)self, cThaRegDdrDiagErrAddr + addrOffset);

    return cAtOk;
    }

/*
 * Address rule:
 * - address [7:0]   -> column
 * - address [10:8]  -> bank
 * - address [24:11] -> row
*/
static uint32 CellAddressGet(AtRam self, uint32 bank, uint32 row, uint32 column)
    {
	AtUnused(self);
    return ((row << 11) & cBit24_11) | ((bank << 8) & cBit10_8) | (column & cBit7_0);
    }

static uint32 CellBankGet(AtRam self, uint32 address)
    {
	AtUnused(self);
    return ((address & cBit10_8) >> 8);
    }

static uint32 CellColumnGet(AtRam self, uint32 address)
    {
	AtUnused(self);
    return (address & cBit7_0);
    }

static uint32 CellRowGet(AtRam self, uint32 address)
    {
	AtUnused(self);
    return ((address & cBit24_11) >> 11);
    }

static uint32 DiagEnableRegister(AtRam self)
    {
    return cThaRegDdrDiagEnable + mDdrAddressOffset(self);
    }

static eAtModuleRamRet AccessEnable(AtRam self, eBool enable)
    {
    uint32 regAddr = DiagEnableRegister(self);
    uint32 regVal  = DeviceRead(self, regAddr);
    mRegFieldSet(regVal, cThaDdrDiagEnable, enable ? 1 : 0);
    DeviceWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eBool AccessIsEnabled(AtRam self)
    {
    uint32 regAddr = DiagEnableRegister(self);
    uint32 regVal = DeviceRead((AtRam)self, regAddr);
    return (regVal & cThaDdrDiagEnableMask) ? cAtTrue : cAtFalse;
    }

static eAtModuleRamRet InitStatusGet(AtRam self)
    {
    uint32 value;
    uint32 ddrOffset = mDdrAddressOffset(self);

    /* Clear history */
    DeviceWrite(self, cThaRegDdrDiagStatus + ddrOffset, 0xFFFFFFFF);

    value = DeviceRead((AtRam)self, cThaRegDdrDiagStatus + ddrOffset);

    if (AtRamIsSimulated(self))
        return cAtOk;

    if ((value & cThaDdrDiagCalibStateMask) != 0)
        return cAtErrorDdrCalibFail;

    if ((value & cThaDdrDiagInitFailMask) != 0)
        return cAtErrorDdrInitFail;

    return cAtOk;
    }

static uint32 NumBanks(AtRam self)
    {
	AtUnused(self);
    return 8;
    }

static uint32 NumColumns(AtRam self)
    {
	AtUnused(self);
    return 256;
    }

static uint32 NumRows(AtRam self)
    {
	AtUnused(self);
    return 16384;
    }

static const char *TypeString(AtRam self)
    {
    AtUnused(self);
    return "DDR";
    }

static eBool BurstIsSupported(AtRam self)
    {
    return ThaModuleRamBurstTestIsSupported((ThaModuleRam)AtRamModuleGet(self));
    }

static uint32 MaxBurst(AtRam self)
    {
    if (AtRamTestBurstIsSupported(self))
        return 16;
    return m_AtRamMethods->MaxBurst(self);
    }

static uint32 BurstRegister(AtRam self)
    {
    return cThaRegDdrBurst + mDdrAddressOffset(self);
    }

static eAtRet HwBurstSet(AtRam self, uint32 burst)
    {
    uint32 regAddr = BurstRegister(self);
    uint32 regVal = AtRamRead(self, regAddr);
    mRegFieldSet(regVal, cThaDdrBurst, burst - 1);
    AtRamWrite(self, regAddr, regVal);
    return cAtOk;
    }

static uint32 HwBurstGet(AtRam self)
    {
    uint32 regAddr = BurstRegister(self);
    uint32 regVal = AtRamRead(self, regAddr);
    return mRegField(regVal, cThaDdrBurst) + 1;
    }

static eAtRet BurstSet(AtRam self, uint32 burst)
    {
    if (AtRamTestBurstIsSupported(self))
        return HwBurstSet(self, burst);
    return m_AtRamMethods->BurstSet(self, burst);
    }

static uint32 BurstGet(AtRam self)
    {
    if (AtRamTestBurstIsSupported(self))
        return HwBurstGet(self);
    return m_AtRamMethods->BurstGet(self);
    }

static eBool NeedClearWholeMemoryBeforeTesting(AtRam self)
    {
    return ThaModuleRamNeedClearWholeMemoryBeforeTesting((ThaModuleRam)AtRamModuleGet(self));
    }

static uint32 AddressBusSizeGet(AtRam self)
    {
    return AtModuleRamDdrAddressBusSizeGet(AtRamModuleGet(self), AtRamIdGet(self));
    }

static uint32 DataBusSizeGet(AtRam self)
    {
    return AtModuleRamDdrDataBusSizeGet(AtRamModuleGet(self), AtRamIdGet(self));
    }

static eAtRet HelperHwTestStart(AtRam self, eBool start)
    {
    uint32 regAddr = DiagEnableRegister(self);
    uint32 regVal = DeviceRead(self, regAddr);
    mRegFieldSet(regVal, cThaDdrDiagHwTestEnable, start ? 1 : 0);
    DeviceWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool HwTestIsStarted(AtRam self)
    {
    uint32 regAddr = DiagEnableRegister(self);
    uint32 regVal = DeviceRead(self, regAddr);
    return (regVal & cThaDdrDiagHwTestEnableMask) ? cAtTrue : cAtFalse;
    }

static eAtRet HwTestStop(AtRam self)
    {
    eAtRet ret = cAtOk;

    ret |= HelperHwTestStart(self, cAtFalse);
    ret |= AtRamAccessEnable(self, cAtFalse);

    return ret;
    }

static eBool HwTestIsGood(AtRam self)
    {
    uint32 regAddr = cThaRegDdrDiagStatus + mDdrAddressOffset(self);
    uint32 regVal  = DeviceRead(self, regAddr);
    uint32 prbsError = regVal & cThaDdrDiagHwTestErrorMask;
    if (prbsError)
        DeviceWrite(self, regAddr, prbsError);
    return prbsError ? cAtFalse : cAtTrue;
    }

static eBool IsGood(AtRam self)
    {
    if (AtRamHwTestIsStarted(self))
        return HwTestIsGood(self);

    return m_AtRamMethods->IsGood(self);
    }

static eAtModuleRamRet Init(AtRam self)
    {
    /* Super initialization */
    eAtRet ret = m_AtRamMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    AtRamHwTestStop(self);

    return cAtOk;
    }

static uint32 CounterAddress(AtRam self, uint32 counterType)
    {
    switch (counterType)
        {
        case cAtRamCounterTypeRead  : return cThaRegDdrDiagReadCounter  + mDdrAddressOffset(self);
        case cAtRamCounterTypeWrite : return cThaRegDdrDiagWriteCounter + mDdrAddressOffset(self);
        case cAtRamCounterTypeErrors: return cThaRegDdrDiagErrCounter   + mDdrAddressOffset(self);
        default:
            return cInvalidUint32;
        }
    }

static uint32 CounterRead2Clear(AtRam self, uint32 counterType, eBool clear)
    {
    uint32 regAddr = CounterAddress(self, counterType);
    uint32 regVal;

    if (regAddr == cInvalidUint32)
        return 0;

    regVal = DeviceRead(self, regAddr);
    if (clear)
        DeviceWrite(self, regAddr, 0);

    return regVal;
    }

static uint32 CounterGet(AtRam self, uint32 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtFalse);
    }

static uint32 CounterClear(AtRam self, uint32 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtTrue);
    }

static eBool CounterIsSupported(AtRam self, uint32 counterType)
    {
    if (CounterAddress(self, counterType) == cInvalidUint32)
        return cAtFalse;

    return cAtTrue;
    }

static eAtRet HwTestErrorForce(AtRam self, eBool forced)
    {
    uint32 regAddr = DiagEnableRegister(self);
    uint32 regVal = DeviceRead(self, regAddr);
    mRegFieldSet(regVal, cThaDdrDiagHwTestForceError, forced ? 1 : 0);
    DeviceWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eBool HwTestErrorIsForced(AtRam self)
    {
    uint32 regAddr = DiagEnableRegister(self);
    uint32 regVal = DeviceRead(self, regAddr);
    return (regVal & cThaDdrDiagHwTestForceErrorMask) ? cAtTrue : cAtFalse;
    }

static const uint32 *LatchedErrorDataRead(AtRam self, uint32 *numDwords,
                                          uint32 *latchedErrorData,
                                          uint32 regBaseAddress)
    {
    uint32 dword_i;
    uint32 baseAddr = regBaseAddress + mDdrAddressOffset(self);

    for (dword_i = 0; dword_i < cThaDdrLatchedDataNumDwords; dword_i++)
        {
        uint32 regAddr = baseAddr + dword_i;
        latchedErrorData[dword_i] = DeviceRead(self, regAddr);
        }

    if (numDwords)
        *numDwords = cThaDdrLatchedDataNumDwords;

    return latchedErrorData;
    }

static void LatchedErrorDataClear(AtRam self, uint32 *latchedErrorData, uint32 regBaseAddress)
    {
    uint32 dword_i;
    uint32 baseAddr = regBaseAddress + mDdrAddressOffset(self);

    for (dword_i = 0; dword_i < cThaDdrLatchedDataNumDwords; dword_i++)
        {
        uint32 regAddr = baseAddr + dword_i;
        DeviceWrite(self, regAddr, 0);
        latchedErrorData[dword_i] = 0;
        }
    }

static const uint32 *LatchedErrorData(AtRam self, uint32 *numDwords)
    {
    return LatchedErrorDataRead(self, numDwords, mThis(self)->latchedErrorData, cAf6Reg_upen_err_dat_Base);
    }

static const uint32 *LatchedExpectedData(AtRam self, uint32 *numDwords)
    {
    return LatchedErrorDataRead(self, numDwords, mThis(self)->latchedExpectedData, cAf6Reg_upen_exp_dat_Base);
    }

static const uint32 *LatchedErrorDataBits(AtRam self, uint32 *numDwords)
    {
    return LatchedErrorDataRead(self, numDwords, mThis(self)->latchedErrorDataBits, cAf6Reg_upen_err_pat_Base);
    }

static void AllLatchErrorsClear(AtRam self)
    {
    LatchedErrorDataClear(self, mThis(self)->latchedErrorData, cAf6Reg_upen_err_dat_Base);
    LatchedErrorDataClear(self, mThis(self)->latchedExpectedData, cAf6Reg_upen_exp_dat_Base);
    LatchedErrorDataClear(self, mThis(self)->latchedErrorDataBits, cAf6Reg_upen_err_pat_Base);
    }

static uint32 LatchedErrorAddress(AtRam self)
    {
    uint32 regAddr = cThaRegDdrDiagErrAddr + mDdrAddressOffset(self);
    return DeviceRead(self, regAddr);
    }

static eAtRet HwTestStart(AtRam self)
    {
    eAtRet ret = cAtOk;

    ret |= AtRamAccessEnable(self, cAtTrue);
    ret |= HelperHwTestStart(self, cAtTrue);

    if (AtRamErrorLatchingSupported(self))
        {
        /* Just give hardware a moment */
        AtOsalUSleep(100000);
        AllLatchErrorsClear(self);
        }

    return ret;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeNone(latchedErrorData);
    mEncodeNone(latchedExpectedData);
    mEncodeNone(latchedErrorDataBits);
    }

static void OverrideAtObject(AtRam self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtRam(AtRam self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtRamMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtRamOverride, mMethodsGet(self), sizeof(m_AtRamOverride));

        mMethodOverride(m_AtRamOverride, CellSizeGet);
        mMethodOverride(m_AtRamOverride, CellRead);
        mMethodOverride(m_AtRamOverride, CellWrite);

        mMethodOverride(m_AtRamOverride, AddressBusSizeGet);
        mMethodOverride(m_AtRamOverride, DataBusSizeGet);

        mMethodOverride(m_AtRamOverride, AccessEnable);
        mMethodOverride(m_AtRamOverride, AccessIsEnabled);

        mMethodOverride(m_AtRamOverride, MemoryFill);
        mMethodOverride(m_AtRamOverride, MemoryReadWriteCheck);
        mMethodOverride(m_AtRamOverride, MemoryReadCheck);
        mMethodOverride(m_AtRamOverride, NeedClearWholeMemoryBeforeTesting);

        mMethodOverride(m_AtRamOverride, CellAddressGet);
        mMethodOverride(m_AtRamOverride, CellBankGet);
        mMethodOverride(m_AtRamOverride, CellColumnGet);
        mMethodOverride(m_AtRamOverride, CellRowGet);

        mMethodOverride(m_AtRamOverride, InitStatusGet);
        mMethodOverride(m_AtRamOverride, NumBanks);
        mMethodOverride(m_AtRamOverride, NumColumns);
        mMethodOverride(m_AtRamOverride, NumRows);
        mMethodOverride(m_AtRamOverride, TypeString);

        mMethodOverride(m_AtRamOverride, BurstSet);
        mMethodOverride(m_AtRamOverride, BurstGet);
        mMethodOverride(m_AtRamOverride, MaxBurst);
        mMethodOverride(m_AtRamOverride, BurstIsSupported);

        mMethodOverride(m_AtRamOverride, HwTestStart);
        mMethodOverride(m_AtRamOverride, HwTestStop);
        mMethodOverride(m_AtRamOverride, HwTestIsStarted);
        mMethodOverride(m_AtRamOverride, HwTestErrorForce);
        mMethodOverride(m_AtRamOverride, HwTestErrorIsForced);
        mMethodOverride(m_AtRamOverride, IsGood);
        mMethodOverride(m_AtRamOverride, Init);
        mMethodOverride(m_AtRamOverride, LatchedErrorData);
        mMethodOverride(m_AtRamOverride, LatchedExpectedData);
        mMethodOverride(m_AtRamOverride, LatchedErrorDataBits);
        mMethodOverride(m_AtRamOverride, LatchedErrorAddress);

        mMethodOverride(m_AtRamOverride, CounterIsSupported);
        mMethodOverride(m_AtRamOverride, CounterGet);
        mMethodOverride(m_AtRamOverride, CounterClear);
        }

    mMethodsSet(self, &m_AtRamOverride);
    }

static void Override(AtRam self)
    {
    OverrideAtObject(self);
    OverrideAtRam(self);
    }

static void MethodsInit(AtRam self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, DefaultOffset);
        mMethodOverride(m_methods, BaseAddress);
        }

    mMethodsSet((ThaDdr)self, &m_methods);
    }

AtRam ThaDdrObjectInit(AtRam self, AtModuleRam ramModule, AtIpCore core, uint8 ramId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtDdrObjectInit(self, ramModule, core, ramId) == NULL)
        return NULL;

    /* Override */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtRam ThaDdrNew(AtModuleRam ramModule, AtIpCore core, uint8 ramId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtRam newDdr = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDdr == NULL)
        return NULL;

    /* Construct it */
    return ThaDdrObjectInit(newDdr, ramModule, core, ramId);
    }

uint32 ThaDdrDefaultOffset(ThaDdr self)
    {
    if (self)
        return mMethodsGet(self)->DefaultOffset(self);
    return 0x0;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ZBT
 *
 * File        : ThaZbt.c
 *
 * Created Date: Jan 31, 2013
 *
 * Description : Thalassa ZBT implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../generic/ram/AtZbtInternal.h"
#include "ThaZbt.h"
#include "ThaDdrInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaZbt
    {
    tThaDdr super;
    }tThaZbt;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtRamMethods m_AtRamOverride;
static tThaDdrMethods m_ThaDdrOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet TestHwAssistEnable(AtRam self, eBool enable)
    {
    AtUnused(self);
    return (enable ? cAtErrorModeNotSupport : cAtOk);
    }

static eBool TestHwAssistIsEnabled(AtRam self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 AddressBusSizeGet(AtRam self)
    {
	AtUnused(self);
    return 20;
    }

static uint32 DataBusSizeGet(AtRam self)
    {
	AtUnused(self);
    return 36;
    }

static uint32 CellSizeGet(AtRam self)
    {
	AtUnused(self);
    return 2;
    }

static const char *TypeString(AtRam self)
    {
    AtUnused(self);
    return "ZBT";
    }

static void OverrideAtRam(AtRam self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtRamOverride, mMethodsGet(self), sizeof(m_AtRamOverride));
        mMethodOverride(m_AtRamOverride, CellSizeGet);
        mMethodOverride(m_AtRamOverride, AddressBusSizeGet);
        mMethodOverride(m_AtRamOverride, DataBusSizeGet);
        mMethodOverride(m_AtRamOverride, TypeString);
        mMethodOverride(m_AtRamOverride, TestHwAssistEnable);
        mMethodOverride(m_AtRamOverride, TestHwAssistIsEnabled);
        }

    mMethodsSet(self, &m_AtRamOverride);
    }

static uint32 DefaultOffset(ThaDdr self)
    {
	AtUnused(self);
    return 0x4000;
    }

static void OverrideThaDdr(ThaDdr self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDdrOverride, mMethodsGet(self), sizeof(m_ThaDdrOverride));

        mMethodOverride(m_ThaDdrOverride, DefaultOffset);
        }

    mMethodsSet(self, &m_ThaDdrOverride);
    }

static void Override(AtRam self)
    {
    OverrideAtRam(self);
    OverrideThaDdr((ThaDdr)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaZbt);
    }

static AtRam ObjectInit(AtRam self, AtModuleRam ramModule, AtIpCore core, uint8 ramId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaDdrObjectInit(self, ramModule, core, ramId) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtRam ThaZbtNew(AtModuleRam ramModule, AtIpCore core, uint8 ramId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtRam newZbt = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newZbt == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newZbt, ramModule, core, ramId);
    }

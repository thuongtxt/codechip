/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : DDR
 * 
 * File        : ThaDdr.h
 * 
 * Created Date: Feb 1, 2013
 *
 * Description : Thalassa default DDR
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THADDR_H_
#define _THADDR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtDdr.h" /* Super class */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaDdr * ThaDdr;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Utils */
uint32 ThaDdrDefaultOffset(ThaDdr self);

/* Default concretes */
AtRam ThaDdrNew(AtModuleRam ramModule, AtIpCore core, uint8 ramId);
AtRam ThaDdr2New(AtModuleRam ramModule, AtIpCore core, uint8 ramId);

/* Product concretes */
AtRam Tha60030022DdrNew(AtModuleRam ramModule, AtIpCore core, uint8 ramId);
AtRam Tha60031031DdrNew(AtModuleRam ramModule, AtIpCore core, uint8 ramId);
AtRam Tha60031031Ddr32BitNew(AtModuleRam ramModule, AtIpCore core, uint8 ramId);
AtRam Tha60031031DdrNew(AtModuleRam ramModule, AtIpCore core, uint8 ramId);
AtRam Tha60031031Ddr32BitNew(AtModuleRam ramModule, AtIpCore core, uint8 ramId);
AtRam Tha60031031EpDdrNew(AtModuleRam ramModule, AtIpCore core, uint8 ramId);
AtRam Tha60035021DdrNew(AtModuleRam ramModule, AtIpCore core, uint8 ramId);
AtRam Tha60060011DdrNew(AtModuleRam ramModule, AtIpCore core, uint8 ramId);
AtRam Tha60150011DdrNew(AtModuleRam ramModule, AtIpCore core, uint8 ramId);
AtRam Tha60070061DdrNew(AtModuleRam ramModule, AtIpCore core, uint8 ramId);

#ifdef __cplusplus
}
#endif
#endif /* _THADDR_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : ThaModuleRam.h
 * 
 * Created Date: Feb 1, 2013
 *
 * Description : Thalassa RAM module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULERAM_H_
#define _THAMODULERAM_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleRam.h"
#include "AtDebugger.h"
#include "ThaDdr.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleRam * ThaModuleRam;
typedef struct tThaInternalRamEntry
    {
    char name[128];
    uint32 address;
    } tThaInternalRamEntry;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Concrete RAMs */
AtModuleRam ThaModuleRamNew(AtDevice device);
AtModuleRam ThaPdhMlpppModuleRamNew(AtDevice device);
AtModuleRam ThaModuleDdr2New(AtDevice device);

void ThaModuleRamDdrUserClockDisplay(ThaModuleRam self, AtDebugger debugger);
void ThaModuleRamQdrUserClockDisplay(ThaModuleRam self, AtDebugger debugger);
eBool ThaModuleRamBurstTestIsSupported(ThaModuleRam self);
eBool ThaModuleRamNeedClearWholeMemoryBeforeTesting(ThaModuleRam self);

eBool ThaModuleRamPhyModuleHasRamInterrupt(ThaModuleRam self, uint32 moduleId, uint32 ramIntr);
const char ** ThaModuleRamPhyModuleRamDescription(ThaModuleRam self, uint32 moduleId, uint32 *numRams);
uint32 ThaModuleRamPhyModuleRamLocalCellAddress(ThaModuleRam self, uint32 moduleId, uint32 localRamId);

/* Error-Generator */
AtErrorGenerator ThaModuleRamErrorGeneratorObjectCreate(ThaModuleRam self, AtInternalRam ram);

/* Backward compatible. */
eBool ThaModuleRamInternalRamMonitoringIsSupported(ThaModuleRam self);
eBool ThaModuleRamCrcCounterIsSupported(ThaModuleRam self);
eBool ThaModuleRamEccCounterIsSupported(ThaModuleRam self);

/* Registers */
uint32 ThaModuleRamDdrCalibStatusMask(ThaModuleRam self, uint8 ddrId);
const tThaInternalRamEntry* ThaModuleRamAllInternalRamEntry(ThaModuleRam self, uint32 *numRams);

/* Product concretes */
AtModuleRam Tha60030022ModuleRamNew(AtDevice device);
AtModuleRam Tha60001031ModuleRamNew(AtDevice device);
AtModuleRam Tha60000031ModuleRamNew(AtDevice device);
AtModuleRam Tha60031021ModuleRamNew(AtDevice device);
AtModuleRam Tha60031071ModuleRamNew(AtDevice device);
AtModuleRam Tha60031021EpModuleRamNew(AtDevice device);
AtModuleRam Tha60031031EpModuleRamNew(AtDevice device);
AtModuleRam Tha60031031ModuleRamNew(AtDevice device);
AtModuleRam Tha60200011ModuleRamNew(AtDevice device);
AtModuleRam Tha60035021ModuleRamNew(AtDevice device, uint8 numDdrs);
AtModuleRam Tha60060011ModuleRamNew(AtDevice device);
AtModuleRam Tha60070013ModuleRamNew(AtDevice device);
AtModuleRam Tha60070023ModuleRamNew(AtDevice device);
AtModuleRam Tha60150011ModuleRamNew(AtDevice device);
AtModuleRam Tha60070061ModuleRamNew(AtDevice device);
AtModuleRam Tha60210031ModuleRamNew(AtDevice device);
AtModuleRam Tha60210021ModuleRamNew(AtDevice device);
AtModuleRam Tha60210011ModuleRamNew(AtDevice device);
AtModuleRam Tha60290022ModuleRamNew(AtDevice device);
AtModuleRam Tha60290051ModuleRamNew(AtDevice device);
AtModuleRam ThaStmPwProductModuleRamNew(AtDevice device);
AtModuleRam ThaPdhPwProductModuleRamNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULERAM_H_ */


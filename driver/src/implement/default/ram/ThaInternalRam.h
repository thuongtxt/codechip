/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : ThaInternalRam.h
 * 
 * Created Date: Dec 4, 2015
 *
 * Description : Default internal RAM interface.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAINTERNALRAM_H_
#define _THAINTERNALRAM_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtInternalRam.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaInternalRam * ThaInternalRam;
typedef struct tThaInternalRamEcc * ThaInternalRamEcc;

/*--------------------------- Forward declarations ---------------------------*/
AtInternalRam ThaInternalRamNew(AtModule phyModule, uint32 ramId, uint32 localId);
AtInternalRam ThaInternalRamEccNew(AtModule phyModule, uint32 ramId, uint32 localId);

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THAINTERNALRAM_H_ */

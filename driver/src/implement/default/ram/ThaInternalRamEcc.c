/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : ThaInternalRamEccEcc.c
 *
 * Created Date: Dec 8, 2015
 *
 * Description : Default internal RAM ECC implementation.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../man/ThaDevice.h"
#include "ThaInternalRamInternal.h"
#include "ThaModuleRamInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaInternalRamEcc)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaInternalRamEccMethods m_methods;

/* Override */
static tAtInternalRamMethods  m_AtInternalRamOverride;
static tThaInternalRamMethods m_ThaInternalRamOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool ParityMonitorIsSupported(AtInternalRam self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool CrcMonitorIsSupported(AtInternalRam self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet EccMonitorEnable(AtInternalRam self, eBool enable)
    {
    ThaInternalRam ram = (ThaInternalRam)self;
    if (AtInternalRamIsReserved(self))
        return cAtErrorModeNotSupport;

    return mMethodsGet(ram)->ErrorMonitorEnable(ram, enable);
    }

static eBool EccMonitorIsEnabled(AtInternalRam self)
    {
    ThaInternalRam ram = (ThaInternalRam)self;
    if (AtInternalRamIsReserved(self))
        return cAtFalse;

    return mMethodsGet(ram)->ErrorMonitorIsEnabled(ram);
    }

static eBool EccMonitorIsSupported(AtInternalRam self)
    {
    AtUnused(self);
    return AtInternalRamIsReserved(self) ? cAtFalse : cAtTrue;
    }

static uint32 ForcableErrorsGet(AtInternalRam self)
    {
    return AtInternalRamIsReserved(self) ? 0 : (cAtRamAlarmEccCorrectable | cAtRamAlarmEccUnnorrectable);
    }

static uint32 CorrectableMask(ThaInternalRamEcc self)
    {
    AtUnused(self);
    /* Let sub-class do */
    return 0;
    }

static uint32 UncorrectableMask(ThaInternalRamEcc self)
    {
    AtUnused(self);
    /* Let sub-class do */
    return 0;
    }

static eAtRet ErrorMonitorEnable(ThaInternalRam self, eBool enable)
    {
    AtModule module = AtInternalRamPhyModuleGet((AtInternalRam)self);
    ThaInternalRam ram = (ThaInternalRam)self;
    uint32 regAddr = mMethodsGet(ram)->DisableCheckRegister(ram);
    uint32 regVal;
    uint32 regCorrMask = mMethodsGet(mThis(self))->CorrectableMask(mThis(self));
    uint32 regUncorrMask = mMethodsGet(mThis(self))->UncorrectableMask(mThis(self));

    regVal = mModuleHwRead(module, regAddr);
    regVal = (enable) ? (regVal & ~(regCorrMask | regUncorrMask)) : (regVal | (regCorrMask | regUncorrMask));
    mModuleHwWrite(module, regAddr, regVal);

    return cAtOk;
    }

static eBool ErrorMonitorIsEnabled(ThaInternalRam self)
    {
    AtModule module = AtInternalRamPhyModuleGet((AtInternalRam)self);
    ThaInternalRam ram = (ThaInternalRam)self;
    uint32 regAddr = mMethodsGet(ram)->DisableCheckRegister(ram);
    uint32 regVal;
    uint32 regCorrMask = mMethodsGet(mThis(self))->CorrectableMask(mThis(self));
    uint32 regUncorrMask = mMethodsGet(mThis(self))->UncorrectableMask(mThis(self));

    regVal = mModuleHwRead(module, regAddr);
    return ((regCorrMask | regUncorrMask) & regVal) ? cAtFalse : cAtTrue;
    }

static uint32 ErrorRead2Clear(ThaInternalRam self, eBool clear)
    {
    AtModule module = AtInternalRamPhyModuleGet((AtInternalRam)self);
    uint32 regAddr = mMethodsGet(self)->ErrorStickyRegister(self);
    uint32 regVal  = mModuleHwRead(module, regAddr);
    uint32 errors = 0;
    uint32 regCorrMask = mMethodsGet(mThis(self))->CorrectableMask(mThis(self));
    uint32 regUncorrMask = mMethodsGet(mThis(self))->UncorrectableMask(mThis(self));

    if (clear)
        mModuleHwWrite(module, regAddr, regCorrMask|regUncorrMask);

    if (regVal & regCorrMask)
        errors |= cAtRamAlarmEccCorrectable;

    if (regVal & regUncorrMask)
        errors |= cAtRamAlarmEccUnnorrectable;

    return errors;
    }

static eAtRet HwForcedErrorSet(ThaInternalRam self, uint32 error, eBool enable)
    {
    AtModule module = AtInternalRamPhyModuleGet((AtInternalRam)self);
    uint32 regAddr = mMethodsGet(self)->ErrorForceRegister(self);
    uint32 regVal  = mModuleHwRead(module, regAddr);
    uint32 regMask = 0;

    if (error & cAtRamAlarmEccCorrectable)
        regMask |= mMethodsGet(mThis(self))->CorrectableMask(mThis(self));

    if (error & cAtRamAlarmEccUnnorrectable)
        regMask |= mMethodsGet(mThis(self))->UncorrectableMask(mThis(self));

    regVal = (enable) ? (regVal | regMask) : (regVal & ~regMask);
    mModuleHwWrite(module, regAddr, regVal);

    return cAtOk;
    }

static uint32 HwForcedErrorGet(ThaInternalRam self)
    {
    AtModule module = AtInternalRamPhyModuleGet((AtInternalRam)self);
    ThaInternalRam ram = (ThaInternalRam)self;
    uint32 regAddr = mMethodsGet(ram)->ErrorForceRegister(ram);
    uint32 regVal  = mModuleHwRead(module, regAddr);
    uint32 errors = 0;

    if (regVal & mMethodsGet(mThis(self))->CorrectableMask(mThis(self)))
        errors |= cAtRamAlarmEccCorrectable;

    if (regVal & mMethodsGet(mThis(self))->UncorrectableMask(mThis(self)))
        errors |= cAtRamAlarmEccUnnorrectable;

    return errors;
    }

static eBool ErrorGeneratorIsSupported(ThaInternalRam self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideAtInternalRam(AtInternalRam self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtInternalRamOverride, mMethodsGet(self), sizeof(m_AtInternalRamOverride));

        mMethodOverride(m_AtInternalRamOverride, ParityMonitorIsSupported);
        mMethodOverride(m_AtInternalRamOverride, CrcMonitorIsSupported);
        mMethodOverride(m_AtInternalRamOverride, EccMonitorEnable);
        mMethodOverride(m_AtInternalRamOverride, EccMonitorIsEnabled);
        mMethodOverride(m_AtInternalRamOverride, EccMonitorIsSupported);
        mMethodOverride(m_AtInternalRamOverride, ForcableErrorsGet);
        }

    mMethodsSet(self, &m_AtInternalRamOverride);
    }

static void OverrideThaInternalRam(AtInternalRam self)
    {
    ThaInternalRam ram = (ThaInternalRam)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInternalRamOverride, mMethodsGet(ram), sizeof(m_ThaInternalRamOverride));

        mMethodOverride(m_ThaInternalRamOverride, ErrorRead2Clear);
        mMethodOverride(m_ThaInternalRamOverride, ErrorMonitorEnable);
        mMethodOverride(m_ThaInternalRamOverride, ErrorMonitorIsEnabled);
        mMethodOverride(m_ThaInternalRamOverride, HwForcedErrorSet);
        mMethodOverride(m_ThaInternalRamOverride, HwForcedErrorGet);
        mMethodOverride(m_ThaInternalRamOverride, ErrorGeneratorIsSupported);
        }

    mMethodsSet(ram, &m_ThaInternalRamOverride);
    }

static void Override(AtInternalRam self)
    {
    OverrideThaInternalRam(self);
    OverrideAtInternalRam(self);
    }

static void MethodsInit(ThaInternalRamEcc self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, CorrectableMask);
        mMethodOverride(m_methods, UncorrectableMask);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaInternalRamEcc);
    }

AtInternalRam ThaInternalRamEccObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaInternalRamObjectInit(self, phyModule, ramId, localId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtInternalRam ThaInternalRamEccNew(AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInternalRam newRam = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newRam == NULL)
        return NULL;

    return ThaInternalRamEccObjectInit(newRam, phyModule, ramId, localId);
    }

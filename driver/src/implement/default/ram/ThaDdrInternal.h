/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : ThaDdrInternal.h
 *
 * Created Date: Mar 1, 2013
 *
 * Description : Thalassa DDR class representation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THADDRINTERNAL_H_
#define _THADDRINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/ram/AtDdrInternal.h"
#include "ThaDdr.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cThaDdrLatchedDataNumDwords 16

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaDdrMethods
    {
    uint32 (*DefaultOffset)(ThaDdr self);
    uint32 (*BaseAddress)(ThaDdr self);
    }tThaDdrMethods;

typedef struct tThaDdr
    {
    tAtDdr super;
    const tThaDdrMethods *methods;

    /* Latched error data, used for hardware PRBS testing */
    uint32 latchedErrorData[cThaDdrLatchedDataNumDwords];
    uint32 latchedExpectedData[cThaDdrLatchedDataNumDwords];
    uint32 latchedErrorDataBits[cThaDdrLatchedDataNumDwords];
    }tThaDdr;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtRam ThaDdrObjectInit(AtRam self, AtModuleRam ramModule, AtIpCore core, uint8 ramId);

#ifdef __cplusplus
}
#endif
#endif /* _THADDRINTERNAL_H_ */


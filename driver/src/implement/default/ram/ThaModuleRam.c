/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : ThaModuleRam.c
 *
 * Created Date: Jan 31, 2013
 *
 * Description : Thalassa RAM module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../util/coder/AtCoderUtil.h"
#include "../../../generic/ram/AtModuleRamInternal.h"
#include "../man/ThaDevice.h"
#include "../man/ThaIpCoreInternal.h"
#include "../man/ThaDeviceInternal.h"
#include "../util/ThaUtil.h"
#include "ThaDdr.h"
#include "ThaZbt.h"
#include "ThaModuleRamDdrReg.h"
#include "ThaModuleRamInternal.h"
#include "ThaInternalRam.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaModuleRam)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaModuleRamMethods m_methods;

static tAtObjectMethods    m_AtObjectOverride;
static tAtModuleMethods    m_AtModuleOverride;
static tAtModuleRamMethods m_AtModuleRamOverride;

static const tAtObjectMethods  *m_AtObjectMethods = NULL;
static const tAtModuleMethods  *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtRam DdrCreate(AtModuleRam self, AtIpCore core, uint8 ramId)
    {
    return ThaDdrNew(self, core, ramId);
    }

static AtRam ZbtCreate(AtModuleRam self, AtIpCore core, uint8 ramId)
    {
    return ThaZbtNew(self, core, ramId);
    }

static AtInternalRam InternalRamGet(AtModuleRam self, uint32 ramId)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    AtModule module;
    AtIterator iterator;
    uint32 upperRamId = 0, lowerRamId = 0;
    AtInternalRam ram = NULL;

    if (!ThaModuleRamInternalRamMonitoringIsSupported(mThis(self)))
        return NULL;

    iterator = ThaDeviceModuleIteratorCreate(device);
    while ((module = (AtModule)AtIteratorNext(iterator)))
        {
        upperRamId = AtModuleNumInternalRamsGet(module) + lowerRamId;
        if (ramId < upperRamId)
            {
            ram = AtModuleInternalRamGet(module, ramId, ramId - lowerRamId);
            break;
            }

        lowerRamId = upperRamId;
        }
        
    AtObjectDelete((AtObject)iterator);

    return ram;
    }

static uint32 NumInternalRams(AtModuleRam self)
    {
    AtModule module;
    AtIterator iterator = ThaDeviceModuleIteratorCreate(AtModuleDeviceGet((AtModule)self));
    uint32 numRam = 0;

    while ((module = (AtModule)AtIteratorNext(iterator)))
        numRam += AtModuleNumInternalRamsGet(module);

    AtObjectDelete((AtObject)iterator);
    return numRam;
    }

static uint32 StartInternalRamIdOfModuleGet(AtModuleRam self, uint32 moduleId)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    AtModule module;
    AtIterator iterator;
    uint32 minRamId = 0;

    if (!ThaModuleRamInternalRamMonitoringIsSupported(mThis(self)))
        return cBit31_0;

    iterator = ThaDeviceModuleIteratorCreate(device);
    while ((module = (AtModule)AtIteratorNext(iterator)))
        {
        if (moduleId == AtModuleTypeGet(module))
            break;
        minRamId += AtModuleNumInternalRamsGet(module);
        }

    AtObjectDelete((AtObject)iterator);
    return minRamId;
    }

static void StickiesClear(ThaModuleRam self)
    {
    static const uint32 cAllOne = 0xFFFFFFFF;

    mModuleHwWrite(self, cThaDebugDdr1Sticky0, cAllOne);
    mModuleHwWrite(self, cThaDebugDdr2Sticky0, cAllOne);
    }

static void StickyDebug(ThaModuleRam self)
    {
    AtPrintc(cSevNormal, "\r\n");
    AtPrintc(cSevInfo, "* Sticky:\r\n");
    mModuleDebugRegPrint(self, Ddr1Sticky0);
    mModuleDebugRegPrint(self, Ddr1Sticky1);
    mModuleDebugRegPrint(self, Ddr1Status0);
    mModuleDebugRegPrint(self, Ddr1Status1);

    mModuleDebugRegPrint(self, Ddr2Sticky0);
    mModuleDebugRegPrint(self, Ddr2Sticky1);
    mModuleDebugRegPrint(self, Ddr2Status0);
    mModuleDebugRegPrint(self, Ddr2Status1);

    /* Clear for next time */
    StickiesClear(self);
    }

static eAtRet Debug(AtModule self)
    {
    m_AtModuleMethods->Debug(self);
    mMethodsGet(mThis(self))->StickyDebug(mThis(self));
    return cAtOk;
    }

static void InterruptHwEnable(AtIpCore ipCore, eBool enable)
    {
    ThaIpCoreRamHwInterruptEnable((ThaIpCore)ipCore, enable);
    }

static eAtRet InterruptEnable(AtModule self, eBool enable)
    {
    uint8 i, numCore;
    AtIpCore ipCore;
    AtDevice device = AtModuleDeviceGet(self);

    numCore = AtDeviceNumIpCoresGet(device);

    for (i = 0; i < numCore; i++)
        {
        ipCore = AtDeviceIpCoreGet(device, i);

        /* If a product supports interrupt pin enable register, just simple
         * enable module interrupt on IPcore. Otherwise, we need to check if
         * IPcore interrupt was enabled before enable module interrupt. */
        if (ThaIpCoreCanEnableHwInterruptPin((ThaIpCore)ipCore) || AtIpCoreInterruptIsEnabled(ipCore))
            InterruptHwEnable(ipCore, enable);

        /* Store to DB */
        ((ThaModuleRam)self)->interruptIsEnabled = enable;
        }

    return cAtOk;
    }

static eBool InterruptIsEnabled(AtModule self)
    {
    return ((ThaModuleRam)self)->interruptIsEnabled;
    }

/* Enable interrupt on one core */
static void InterruptOnIpCoreEnable(AtModule self, eBool enable, AtIpCore ipCore)
    {
    if(InterruptIsEnabled(self))
        InterruptHwEnable(ipCore, enable);
    }

static void InterruptProcess(AtModule self, uint32 glbIntr, AtIpCore ipCore)
    {
    AtModule module;
    AtIterator iterator = ThaDeviceModuleIteratorCreate(AtModuleDeviceGet(self));
    uint32 ramIntr = mMethodsGet(mThis(self))->InterruptStatusGet(mThis(self));
    uint32 ramId = 0;

    AtUnused(glbIntr);
    AtUnused(ipCore);

    while ((module = (AtModule)AtIteratorNext(iterator)) != NULL)
        {
        uint32 numRams = AtModuleNumInternalRamsGet(module);
        uint32 localRam;

        if (!ThaModuleRamPhyModuleHasRamInterrupt(mThis(self), AtModuleTypeGet(module), ramIntr))
            {
            ramId += numRams;
            continue;
            }

        for (localRam = 0; localRam < numRams; localRam++)
            {
            AtInternalRam ram = AtModuleInternalRamGet(module, ramId, localRam);
            uint32 errors = AtInternalRamErrorHistoryClear(ram);
            if (errors)
                AtModuleRamAllEventListenersCall((AtModuleRam)self, ram, errors);
            ramId = ramId + 1;
            }
        }

    AtObjectDelete((AtObject)iterator);
    }

static AtInternalRam InternalRamCreate(AtModule self, uint32 ramId, uint32 localRamId)
    {
    return ThaInternalRamNew(self, ramId, localRamId);
    }

static uint32 DDRUserClockValueStatusRegister(ThaModuleRam self, uint8 ddrId)
    {
    AtUnused(self);
    AtUnused(ddrId);
    return 0xFFFFFF;
    }

static uint32 QDRUserClockValueStatusRegister(ThaModuleRam self, uint8 qdrId)
    {
    AtUnused(self);
    AtUnused(qdrId);
    return 0xFFFFFF;
    }

static const char *QDRUserClockName(ThaModuleRam self, uint8 qdrId)
    {
    AtUnused(self);
    AtUnused(qdrId);
    return "User";
    }

static eBool BurstTestIsSupported(ThaModuleRam self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool NeedClearWholeMemoryBeforeTesting(ThaModuleRam self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 DdrAddressBusSizeGet(AtModuleRam self, uint8 ddrId)
    {
    AtUnused(self);
    AtUnused(ddrId);
    return 25;
    }

static uint32 DdrDataBusSizeGet(AtModuleRam self, uint8 ddrId)
    {
    AtUnused(self);
    AtUnused(ddrId);
    return 16;
    }

static uint32 DdrCalibStatusMask(ThaModuleRam self, uint8 ddrId)
    {
    AtUnused(self);
    AtUnused(ddrId);
    return cBit31_0;
    }

static uint32 ClockDdrSwingRangeInPpm(ThaModuleRam self)
    {
    AtUnused(self);
    return 5;
    }

static uint32 ClockQdrSwingRangeInPpm(ThaModuleRam self)
    {
    AtUnused(self);
    return 5;
    }

static eBool DdrHasSpecificValue(ThaModuleRam self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool InternalRamMonitoringIsSupported(ThaModuleRam self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 InterruptStatusGet(ThaModuleRam self)
    {
    /* Some products supports a intermediate OR interrupt status. So this method
     * must return this OR status. Otherwise, simple return 32-bits all 1s. */
    AtUnused(self);
    return cBit31_0;
    }

static eBool PhyModuleHasRamInterrupt(ThaModuleRam self, uint32 moduleId, uint32 ramIntr)
    {
    AtUnused(self);
    AtUnused(ramIntr);
    if (moduleId == cAtModuleRam)
        return cAtTrue;
    return cAtFalse;
    }

static const char ** PhyModuleRamDescription(ThaModuleRam self, uint32 moduleId, uint32 *numRams)
    {
    AtUnused(moduleId);
    return AtModuleAllInternalRamsDescription((AtModule)self, numRams);
    }

static uint32 PhyModuleRamLocalCellAddress(ThaModuleRam self, uint32 moduleId, uint32 localRamId)
    {
    AtUnused(moduleId);
    AtUnused(self);
    AtUnused(localRamId);
    return cInvalidUint32;
    }

static const char **AllInternalRamsDescription(AtModule self, uint32 *numRams)
    {
    static const char * description[] = {
        "PDH DS1/E1/J1 Rx Framer Control RAM",
        "PDH DS1/E1/J1 Tx Framer Control RAM",
        "PDH DS1/E1/J1 Tx Framer Signaling Insertion Control RAM",
        "Demap Channel Control RAM",
        "Map Channel Control RAM",
        "Map Timing Reference Control RAM",
        "CDR Engine Timing control RAM",
        "PLA PDHPW Payload Assemble Payload Size Control RAM",
        "PDA Pseudowire Reorder Control RAM",
        "PDA Pseudowire Jitter Buffer Control RAM",
        "PDA Pseudowire TDM Mode Control RAM",
        "PWE Pseudowire Transmit Ethernet Header Value Control RAM",
        "PWE Pseudowire Transmit Ethernet Header Length Control RAM",
        "PWE Pseudowire Transmit Header RTP SSRC Value Control RAM",
        "PWE Pseudowire Transmit HSPW Label Control RAM",
        "PWE Pseudowire Transmit HSPW Group Enable Control RAM",
        "PWE Pseudowire Transmit UPSR Group Enable Control RAM",
        "PWE Pseudowire Transmit HSPW Group Protection Control RAM",
        "CLA Classify VLAN TAG Lookup Control RAM ",
        "CLA Classify HBCE Hashing Table page0 Control RAM ",
        "CLA Classify HBCE Hashing Table page1 Control RAM ",
        "CLA Classify HBCE Looking Up Information Control RAM ",
        "CLA Classify Pseudowire Type Control RAM ",
        "CLA Classify Per Group Enable Control RAM ",
        "DDR "
        };
    AtUnused(self);

    if (numRams)
        *numRams = mCount(description);

    return description;
    }

static const tThaInternalRamEntry* AllInternalRamEntry(ThaModuleRam self, uint32 *numRams)
    {
    AtUnused(self);
    AtUnused(numRams);
    return NULL;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaModuleRam object = (ThaModuleRam)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(interruptIsEnabled);
    }

static AtErrorGenerator ErrorGeneratorObjectCreate(ThaModuleRam self, AtInternalRam ram)
    {
    AtUnused(self);
    AtUnused(ram);
    return NULL;
    }

static eBool CrcCounterIsSupported(ThaModuleRam self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool EccCounterIsSupported(ThaModuleRam self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void MethodsInit(AtModuleRam self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, DDRUserClockValueStatusRegister);
        mMethodOverride(m_methods, QDRUserClockValueStatusRegister);
        mMethodOverride(m_methods, QDRUserClockName);
        mMethodOverride(m_methods, BurstTestIsSupported);
        mMethodOverride(m_methods, NeedClearWholeMemoryBeforeTesting);
        mMethodOverride(m_methods, DdrCalibStatusMask);
        mMethodOverride(m_methods, ClockDdrSwingRangeInPpm);
        mMethodOverride(m_methods, ClockQdrSwingRangeInPpm);
        mMethodOverride(m_methods, DdrHasSpecificValue);
        mMethodOverride(m_methods, InternalRamMonitoringIsSupported);
        mMethodOverride(m_methods, InterruptStatusGet);
        mMethodOverride(m_methods, PhyModuleHasRamInterrupt);
        mMethodOverride(m_methods, PhyModuleRamDescription);
        mMethodOverride(m_methods, ErrorGeneratorObjectCreate);
        mMethodOverride(m_methods, CrcCounterIsSupported);
        mMethodOverride(m_methods, EccCounterIsSupported);
        mMethodOverride(m_methods, PhyModuleRamLocalCellAddress);
        mMethodOverride(m_methods, StickyDebug);
        mMethodOverride(m_methods, AllInternalRamEntry);
        }

    mMethodsSet(mThis(self), &m_methods);
    }

static void OverrideAtObject(AtModuleRam self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModuleRam self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, Debug);
        mMethodOverride(m_AtModuleOverride, InterruptProcess);
        mMethodOverride(m_AtModuleOverride, InterruptEnable);
        mMethodOverride(m_AtModuleOverride, InterruptIsEnabled);
        mMethodOverride(m_AtModuleOverride, InterruptOnIpCoreEnable);
        mMethodOverride(m_AtModuleOverride, InternalRamCreate);
        mMethodOverride(m_AtModuleOverride, AllInternalRamsDescription);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtModuleRam(AtModuleRam self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleRamOverride, mMethodsGet(self), sizeof(m_AtModuleRamOverride));

        mMethodOverride(m_AtModuleRamOverride, DdrCreate);
        mMethodOverride(m_AtModuleRamOverride, ZbtCreate);
        mMethodOverride(m_AtModuleRamOverride, DdrAddressBusSizeGet);
        mMethodOverride(m_AtModuleRamOverride, DdrDataBusSizeGet);
        mMethodOverride(m_AtModuleRamOverride, InternalRamGet);
        mMethodOverride(m_AtModuleRamOverride, NumInternalRams);
        mMethodOverride(m_AtModuleRamOverride, StartInternalRamIdOfModuleGet);
        }

    mMethodsSet(self, &m_AtModuleRamOverride);
    }

static void Override(AtModuleRam self)
    {
    OverrideAtModuleRam(self);
    OverrideAtModule(self);
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaModuleRam);
    }

AtModuleRam ThaModuleRamObjectInit(AtModuleRam self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleRamObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleRam ThaModuleRamNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleRam newRam = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newRam == NULL)
        return NULL;

    /* Construct it */
    return ThaModuleRamObjectInit(newRam, device);
    }

void ThaModuleRamDdrUserClockDisplay(ThaModuleRam self, AtDebugger debugger)
    {
    AtModuleRam ramModule = (AtModuleRam)self;
    uint8 numDdrs = AtModuleRamNumDdrGet(ramModule);
    uint8 ddr_i;
    uint32 ppmSwingRange = mMethodsGet(self)->ClockDdrSwingRangeInPpm(self);

    for (ddr_i = 0; ddr_i < numDdrs; ddr_i++)
        {
        char buffer[64];
        uint32 regAddr = mMethodsGet(mThis(self))->DDRUserClockValueStatusRegister(mThis(self), ddr_i);
        uint32 expectedClock = AtModuleRamDdrUserClock(ramModule, ddr_i);

        if (mMethodsGet(self)->DdrHasSpecificValue(self))
            {
            AtSnprintf(buffer,
                       sizeof(buffer) - 1,
                       "%s#%d User Clock %uMhz Value Status",
                       AtModuleRamDdrTypeString(ramModule, ddr_i),
                       ddr_i + 1,
                       AtModuleRamDdrUserClock(ramModule, ddr_i) / 1000000);
            ThaDeviceClockResultPrint(AtModuleDeviceGet((AtModule)self), debugger, buffer, regAddr, expectedClock, ppmSwingRange);
            }
        else
            {
            uint32 realClock = mModuleHwRead(self, regAddr) / 1000;
            AtSnprintf(buffer,
                       sizeof(buffer) - 1,
                       "%s#%d User Clock Value Status",
                       AtModuleRamDdrTypeString(ramModule, ddr_i),
                       ddr_i + 1);
            AtPrintc(cSevNormal, "%-50s: ", buffer);
            if (debugger)
                {
                uint32 bufferSize;
                char *debuggerBuf = AtDebuggerCharBuffer(debugger, &bufferSize);
                AtDebuggerEntryAdd(debugger, AtDebugEntryStringFormatNew(cSevNormal, debuggerBuf, bufferSize, buffer,
                                   "%-7d KHz | Expected: %-7s     | PPM: %-7s  -> N/A", realClock, "N/A", "N/A"));
                return;
                }
            AtPrintc(cSevInfo, "%-7d KHz | Expected: %-7s     | PPM: %-7s  -> N/A\r\n", realClock, "N/A", "N/A");
            }
        }
    }

void ThaModuleRamQdrUserClockDisplay(ThaModuleRam self, AtDebugger debugger)
    {
    AtModuleRam ramModule = (AtModuleRam)self;
    uint8 numQdrs = AtModuleRamNumQdrGet(ramModule);
    uint8 qdr_i;
    uint32 ppmSwingRange;

    if (self == NULL)
        return;

    ppmSwingRange = mMethodsGet(self)->ClockQdrSwingRangeInPpm(self);

    for (qdr_i = 0; qdr_i < numQdrs; qdr_i++)
        {
        char buffer[64];
        uint32 regAddr = mMethodsGet(mThis(self))->QDRUserClockValueStatusRegister(mThis(self), qdr_i);
        uint32 expectedClock = AtModuleRamQdrUserClock(ramModule, qdr_i);
        uint32 userClock = AtModuleRamQdrUserClock(ramModule, qdr_i);
        AtSnprintf(buffer,
                   sizeof(buffer) - 1,
                   "FPGA QDRNo%u %s Clock %.2fMhz Value Status",
                   qdr_i + 1,
                   mMethodsGet(self)->QDRUserClockName(self, qdr_i),
                   (float)userClock / 1000000.0f);
        ThaDeviceClockResultPrint(AtModuleDeviceGet((AtModule)self), debugger, buffer, regAddr, expectedClock, ppmSwingRange);
        }
    }

eBool ThaModuleRamBurstTestIsSupported(ThaModuleRam self)
    {
    if (self)
        return mMethodsGet(self)->BurstTestIsSupported(self);
    return cAtFalse;
    }

eBool ThaModuleRamNeedClearWholeMemoryBeforeTesting(ThaModuleRam self)
    {
    if (self)
        return mMethodsGet(self)->NeedClearWholeMemoryBeforeTesting(self);
    return cAtFalse;
    }

uint32 ThaModuleRamDdrCalibStatusMask(ThaModuleRam self, uint8 ddrId)
    {
    if (self)
        return mMethodsGet(self)->DdrCalibStatusMask(self, ddrId);
    return cBit31_0;
    }

eBool ThaModuleRamInternalRamMonitoringIsSupported(ThaModuleRam self)
    {
    if (self)
        return mMethodsGet(self)->InternalRamMonitoringIsSupported(self);

    return cAtFalse;
    }

eBool ThaModuleRamPhyModuleHasRamInterrupt(ThaModuleRam self, uint32 moduleId, uint32 ramIntr)
    {
    if (self)
        return mMethodsGet(self)->PhyModuleHasRamInterrupt(self, moduleId, ramIntr);
    return cAtFalse;
    }

const char ** ThaModuleRamPhyModuleRamDescription(ThaModuleRam self, uint32 moduleId, uint32 *numRams)
    {
    if (self)
        return mMethodsGet(self)->PhyModuleRamDescription(self, moduleId, numRams);
    return NULL;
    }

uint32 ThaModuleRamPhyModuleRamLocalCellAddress(ThaModuleRam self, uint32 moduleId, uint32 localRamId)
    {
    if (self)
        return mMethodsGet(self)->PhyModuleRamLocalCellAddress(self, moduleId, localRamId);
    return cInvalidUint32;
    }

AtErrorGenerator ThaModuleRamErrorGeneratorObjectCreate(ThaModuleRam self, AtInternalRam ram)
    {
    if (self == NULL)
        return NULL;

    if (!ThaDeviceErrorGeneratorIsSupported((ThaDevice)AtModuleDeviceGet((AtModule)self)))
        return NULL;

    return mMethodsGet(self)->ErrorGeneratorObjectCreate(self, ram);
    }

eBool ThaModuleRamCrcCounterIsSupported(ThaModuleRam self)
    {
    if (self)
        return mMethodsGet(self)->CrcCounterIsSupported(self);
    return cAtFalse;
    }

eBool ThaModuleRamEccCounterIsSupported(ThaModuleRam self)
    {
    if (self)
        return mMethodsGet(self)->EccCounterIsSupported(self);
    return cAtFalse;
    }

const tThaInternalRamEntry* ThaModuleRamAllInternalRamEntry(ThaModuleRam self, uint32 *numRams)
    {
    if (self)
        return mMethodsGet(self)->AllInternalRamEntry(self, numRams);
    return NULL;
    }

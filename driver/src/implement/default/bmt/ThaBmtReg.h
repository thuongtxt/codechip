/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : BMT
 * 
 * File        : ThaBmtReg.h
 * 
 * Created Date: Nov 21, 2012
 *
 * Description : BMT physical module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THABMTREG_H_
#define _THABMTREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/*------------------------------------------------------------------------------
Reg Name: BM CPU Write Cache Data Control
Reg Addr: 0x340080-0x340087
          The address format for these registers is 0x340080 + DwId
          Where: DwId (0 - 7) double word identification number
Reg Desc: Contain the data of the cache that CPU wants to write
------------------------------------------------------------------------------*/
#define cThaRegBmCpuWriteCacheDataCtrl(DwId)           (uint32)(0x340080 + (DwId))

/*------------------------------------------------------------------------------
Reg Name: BM DDR CPU Access Cache Info Control
Reg Addr: 0x340064
Reg Desc: Contain the information of cache that CPU wants to access.
------------------------------------------------------------------------------*/
#define cThaRegBmDdrCpuAcsCacheInfoCtrl             (0x340064)

/*--------------------------------------
BitField Name: BmDdrCpuRnW
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
               - 1: CPU want to Read DDR
               - 0: CPU want to Write DDR
--------------------------------------*/
#define cThaBmDdrCpuRnWMask                            cBit18
#define cThaBmDdrCpuRnWShift                           18

/*--------------------------------------
BitField Name: BmDdrCpuAdr[18:0]
BitField Type:
BitField Desc: Contain the DDR address CPU want to access
--------------------------------------*/
#define cThaBmDdrCpuAdrMask                            cBit17_0
#define cThaBmDdrCpuAdrShift                           0

#define cThaBmDdrCpuAdrConstMask                       cBit17_11
#define cThaBmDdrCpuAdrConstShift                      11
#define cThaBmDdrCpuAdrConstRstVal                     0x7E
#define cThaBmDdrCpuAdrPwIdMask                        cBit10_2
#define cThaBmDdrCpuAdrPwIdShift                       2
#define cThaBmDdrCpuAdrSegIdMask                       cBit1_0
#define cThaBmDdrCpuAdrSegIdShift                      0

/*------------------------------------------------------------------------------
Reg Name: BM DDR CPU Access Cache Request Control
Reg Addr: 0x340065
Reg Desc: When CPU wants to access a cache, CPU will set this bit to "1". When
          the access is done, this bit will be automatically clear to "O".
------------------------------------------------------------------------------*/
#define cThaRegBmDdrCpuAcsCacheRequestCtrl          (0x340065)

/*--------------------------------------
BitField Name: BmDdrCpuReq
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
               - 1: CPU request access
               - 0: Access is done
--------------------------------------*/
#define cThaBmDdrCpuReqMask                            cBit0

/*------------------------------------------------------------------------------
Reg Name: BM CPU Read Cache Data Status
Reg Addr: 0x3400A0-0x3400A7
          The address format for these registers is 0x3400A0 + DwId
          Where: DwId (0 - 7) double word identification number
Reg Desc: Contain the data of the cache that CPU wants to read
------------------------------------------------------------------------------*/
#define cThaRegBmCpuReadCacheDataStat(wDwId)        (0x3400A0 + (wDwId))

/*------------------------------------------------------------------------------
Reg Name: BM PW Disable Control
Reg Addr: 0x344800-0x344BFF
          The address format for these registers is 0x344800 + pwid
          Where: pwid (0 - 1023) Pseudo wire identification number
Reg Desc: Configure to disable for specific pwid
------------------------------------------------------------------------------*/
#define cThaRegBmPWEnableCtrl              (0x344800)

/*--------------------------------------
BitField Name: BmPWDis
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
               - 1: PW is disable
               - 0: PW is enable
--------------------------------------*/
#define cThaBmPWEnMask                                cBit0
#define cThaBmPWEnShift                               0

/*------------------------------------------------------------------------------
Reg Name: BM CPU Pseduowires Buffer control
Reg Addr: 0x35E000 - 0x35E1FF
          The address format of these registers is 0x35E000+ Pwid
          Where: Pwid: (0 -335) Pseudo wires ID
Reg Desc: The register provides the configuration of Pseudo wires to control
          incoming data stored DRAM buffer.
------------------------------------------------------------------------------*/
#define cThaRegBmCpuPseduowiresBufCtrl              (0x35E000UL)

/*--------------------------------------
BitField Name: BmMaxBlk[3:0]
BitField Type: R/W
BitField Desc: This is the maximum Big-blocks that Pseudo wire permit to use to
               store incoming data.
BitField Bits: 15_12
--------------------------------------*/
#define cThaBmMaxBlkMask                               cBit15_12
#define cThaBmMaxBlkShift                              12

/*--------------------------------------
BitField Name: BmStartBlk[11:0]
BitField Type: R/W
BitField Desc: This is the first Big-block that Pseudo wire begin using to
               store incoming data.
BitField Bits: 11_0
--------------------------------------*/
#define cThaBmStartBlkMask                             cBit11_0
#define cThaBmStartBlkShift                            0

/*------------------------------------------------------------------------------
Reg Name: BM CPU  Link List Table Big-block Control
Reg Addr: 0x35C000 - 0x35DFFF
          The address format of these registers is 0x35C000+ llistid
          Where: llistid: (0 - 8127) Linked-list IDs
Reg Desc: The register provides the Linked-list table Big-block controller.
          Every Big-block is created by 32 blocks that used to store incoming
          cells. There's up to 256K blocks DRAM buffer for data storing with
          32 bytes so that there are up to 8128 Big-blocks. After power up, SW
          must initialize the linked-list table Big-block through this
          register to indicate the Big-blocks are used and linked together.
          After initialization, the Big-block is removed/ added from/to
          linked-list table Big-block through the Add/Get Big-block control
          register and linked-list table Big-block will be updated by HW
------------------------------------------------------------------------------*/
#define cThaRegBmCpuLinkListTabBigblockCtrl         (0x35C000)

/*--------------------------------------
BitField Name: BmLinkListTable
BitField Type: R/W
BitField Desc: This is the next block ID linked with it
BitField Bits: 11_0
--------------------------------------*/
#define cThaBmLinkListTabMask                          cBit11_0
#define cThaBmLinkListTabShift                         0

/*------------------------------------------------------------------------------
Reg Name: BM CPU Ingress PW Dynamic Cache Initial Request Control
Reg Addr: 0x340026
Reg Desc: When CPU wants to initialize dynamic cache, CPU will set this bit to
          "1". When the initializing is complete, this bit will be
          automatically clear to "O".
------------------------------------------------------------------------------*/
#define cThaRegBmCpuIngPWDynamicCacheInitialRequestCtrl (0x340026)

/*--------------------------------------
BitField Name: BmCpuPWCacheInitReq
BitField Type: R/W
BitField Desc: initialize dynamic cache
               - 1: CPU Initial request
               - 0:  Initial done
--------------------------------------*/
#define cThaBmCpuPWCacheInitReqMask                    cBit0
#define cThaBmCpuPWCacheInitReqShift                   0
#define cThaBmCpuPWCacheInitReqMaxVal                  0x1
#define cThaBmCpuPWCacheInitReqMinVal                  0x0
#define cThaBmCpuPWCacheInitReqRstVal                  0x0


/*------------------------------------------------------------------------------
Reg Name: BM CPU Ingress PW Dynamic Cache Size Control
Reg Addr: 0x340027
Reg Desc: Contain the size of dynamic cache.
------------------------------------------------------------------------------*/
#define cThaRegBmCpuIngPWDynamicCacheSizeCtrl       (0x340027)

#define cThaRegBmCpuIngPWDynamicCacheSizeCtrlRstVal 0x00000000
#define cThaRegBmCpuIngPWDynamicCacheSizeCtrlRwMsk  0x000003FF
#define cThaRegBmCpuIngPWDynamicCacheSizeCtrlRwcMsk 0x00000000
/*
#define cThaUnusedMask                                 cBit31_11
#define cThaUnusedShift                                11
#define cThaUnusedRstVal                               0x0
*/

/*--------------------------------------
BitField Name: BmCpuPWCacheSize[10:0]
BitField Type: R/W
BitField Desc: Value of the cache size
--------------------------------------*/
#define cThaBmCpuPWCacheSizeMask                       cBit9_0
#define cThaBmCpuPWCacheSizeShift                      0
#define cThaBmCpuPWCacheSizeMaxVal                     0x3FF
#define cThaBmCpuPWCacheSizeMinVal                     0x0
#define cThaBmCpuPWCacheSizeRstVal                     0x0

/*------------------------------------------------------------------------------
Reg Name: BM Sticky Status
Reg Addr: 0x34000A
Reg Desc: When CPU wants to initialize dynamic cache, CPU will set this bit to
          "1". When the initializing is complete, this bit will be
          automatically clear to "O".
------------------------------------------------------------------------------*/
#define cThaRegBMStickyStatus                       0x34000A

#define cThaBmDiscardCacheError      cBit15
#define cThaBmWriteCacheEror         cBit14
#define cThaBmReadCacheError         cBit13
#define cThaBmReadEmpty              cBit12
#define cThaBmLinkListFull           cBit11
#define cThaBmMissSopEop             cBit10
#define cThaBmOutqueRequestFull      cBit9
#define cThaBmOutqueBelowThreshold   cBit8
#define cThaBmEnqueMissSopEop        cBit7
#define cThaBmOverlen                cBit6
#define cThaBmZeroLen                cBit5
#define cThaBmMaxLenViolate          cBit4
#define cThaBmEnQueueFail            cBit3
#define cThaBmEnQueueFifoFull        cBit2
#define cThaBmFreeSegmentDiscardFull cBit1
#define cThaBmEmptyFreeSegment       cBit0

/*------------------------------------------------------------------------------
Reg Name: BM CRC-8 Sticky
Reg Addr: 0x3CC009
Reg Desc: Check CRC-8 for BM buffer
------------------------------------------------------------------------------*/
#define cThaRegBMCRC8Sticky                     0x3CC009

#ifdef __cplusplus
}
#endif
#endif /* _THABMTREG_H_ */

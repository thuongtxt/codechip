/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : BMT internal module
 * 
 * File        : ThaModuleBmtInternal.h
 * 
 * Created Date: Sep 10, 2012
 *
 * Description :
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEBMTINTERNAL_H_
#define _THAMODULEBMTINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPw.h"

#include "../../../generic/man/AtModuleInternal.h"
#include "../../default/man/ThaDeviceInternal.h"
#include "ThaModuleBmt.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
#define cModuleBmtSharedBufferLen 256

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleBmtMethods
    {
    uint32 (*BaseAddress)(ThaModuleBmt self);
    eAtRet (*DefaultSet)(ThaModuleBmt self);
    uint32 (*PwDefaultOffset)(ThaModuleBmt self, AtPw pw);
    uint8 (*PwPart)(ThaModuleBmt self, AtPw pw);

    eAtRet (*PwPrioritySet)(ThaModuleBmt self, AtPw pw, uint8 priority);
    uint8 (*PwPriorityGet)(ThaModuleBmt self, AtPw pw);

    eAtRet (*Enable)(ThaModuleBmt self, AtPw pw, eBool enable);
    eBool  (*IsEnabled)(ThaModuleBmt self, AtPw pw);

    eAtRet (*PwHeaderArraySet)(ThaModuleBmt self, AtPw pw, uint8 *buffer, uint8 headerLenInByte);
    eAtRet (*PwHeaderArrayGet)(ThaModuleBmt self, AtPw pw, uint8 *buffer, uint8 headerLenInByte);

    eBool (*NeedInitCache)(ThaModuleBmt self);
    }tThaModuleBmtMethods;

typedef struct tThaModuleBmt
    {
    tAtModule super;
    const tThaModuleBmtMethods *methods;

    /* Private data */
    uint8 sharedBuffer[cModuleBmtSharedBufferLen];

    /* Debug */
    uint32 psnRetryTimes;
    }tThaModuleBmt;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule ThaModuleBmtObjectInit(AtModule module, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEBMTINTERNAL_H_ */


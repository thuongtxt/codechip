/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : BMT (internal module)
 * 
 * File        : ThaModuleBmt.h
 * 
 * Created Date: Sep 10, 2012
 *
 * Description :
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEBMT_H_
#define _THAMODULEBMT_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleBmt * ThaModuleBmt;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule ThaModuleBmtNew(AtDevice device);
AtModule ThaModuleBmtV2New(AtDevice device);

AtModule ThaDeviceBmtModule(AtDevice device);
uint8 ThaModuleBmtDdrRead(AtModule self, uint32 address, uint32 *values);
uint8 ThaModuleBmtDdrWrite(AtModule self, uint32 address, const uint32 *values);

/* Product concretes */
AtModule Tha60030080ModuleBmtNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEBMT_H_ */


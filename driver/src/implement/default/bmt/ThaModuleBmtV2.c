/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BMT
 *
 * File        : ThaModuleBmtV2.c
 *
 * Created Date: May 28, 2013
 *
 * Description : BMT module for PW product version 2
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaModuleBmtInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaModuleBmtV2
    {
    tThaModuleBmt super;
    }tThaModuleBmtV2;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleBmtMethods m_ThaModuleBmtOverride;
static tAtModuleMethods     m_AtModuleOverride;

/* Save super implementations */
static const tAtModuleMethods *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaModuleBmtV2);
    }

static eAtRet PwHeaderArraySet(ThaModuleBmt self, AtPw pw, uint8 *buffer, uint8 hdrLenInByte)
    {
	AtUnused(hdrLenInByte);
	AtUnused(buffer);
	AtUnused(pw);
	AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eAtRet PwHeaderArrayGet(ThaModuleBmt self, AtPw pw, uint8 *buffer, uint8 hdrLenInByte)
    {
	AtUnused(hdrLenInByte);
	AtUnused(buffer);
	AtUnused(pw);
	AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eAtRet Enable(ThaModuleBmt self, AtPw pw, eBool enable)
    {
	AtUnused(enable);
	AtUnused(pw);
	AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eBool IsEnabled(ThaModuleBmt self, AtPw pw)
    {
	AtUnused(pw);
	AtUnused(self);
    return cAtFalse;
    }

static eAtRet Debug(AtModule self)
    {
	AtUnused(self);
    return cAtOk;
    }

static eAtRet DefaultSet(ThaModuleBmt self)
    {
	AtUnused(self);
    /* This module is almost unused for this product. Additional initialization
     * source code may be added in the future */
    return cAtOk;
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, Debug);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void OverrideThaModuleBmt(AtModule self)
    {
    ThaModuleBmt bmtModule = (ThaModuleBmt)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleBmtOverride, mMethodsGet(bmtModule), sizeof(m_ThaModuleBmtOverride));

        mMethodOverride(m_ThaModuleBmtOverride, PwHeaderArraySet);
        mMethodOverride(m_ThaModuleBmtOverride, PwHeaderArrayGet);
        mMethodOverride(m_ThaModuleBmtOverride, Enable);
        mMethodOverride(m_ThaModuleBmtOverride, IsEnabled);
        mMethodOverride(m_ThaModuleBmtOverride, DefaultSet);
        }

    mMethodsSet(bmtModule, &m_ThaModuleBmtOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideThaModuleBmt(self);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleBmtObjectInit(self, device) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule ThaModuleBmtV2New(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

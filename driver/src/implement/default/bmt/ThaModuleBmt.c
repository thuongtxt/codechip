/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BMT (internal)
 *
 * File        : ThaModuleBmt.c
 *
 * Created Date: Sep 10, 2012
 *
 * Description : BMT internal module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModulePw.h"

#include "../../default/man/ThaDeviceInternal.h"
#include "../man/ThaDeviceReg.h"
#include "../pw/ThaModulePw.h"
#include "../man/ThaDeviceReg.h"
#include "../pw/ThaModulePw.h"

#include "ThaModuleBmtInternal.h"
#include "ThaBmtReg.h"

/*--------------------------- Define -----------------------------------------*/
/* Block sizes */
#define cBlockSizeInBytes 32
#define cBlockSizeInDwords 8
#define cNumBytesInDword   4

#define cThaBmBlkNumPerPw               7UL
#define cThaBmPwCatchSizeVal            2040

#define cThaWrIndrTimeOut               10000 /* 10000 ms*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) (ThaModuleBmt)self

/*--------------------------- Local typedefs ---------------------------------*/
typedef enum eHwRequest
    {
    cHwRequestWrite,
    cHwRequestRead
    }eHwRequest;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaModuleBmtMethods m_methods;

/* Override */
static tAtModuleMethods m_AtModuleOverride;
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementations */
static const tAtModuleMethods *m_AtModuleMethods = NULL;
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ByteShift(uint8 byteId)
	{
	return (uint32)((3 - ((byteId) % 4)) * 8);
	}

static uint32 ByteMask(uint8 byteId)
	{
	return cBit7_0 << ByteShift(byteId);
	}

static uint32 BaseAddress(ThaModuleBmt self)
    {
	AtUnused(self);
    return 0x340000;
    }

static eBool HasRegister(AtModule self, uint32 localAddress)
    {
	AtUnused(self);
    if ((localAddress >= 0x340000) && (localAddress <= 0x35FFFF))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 *HoldRegistersGet(AtModule self, uint16 *numberOfHoldRegisters)
    {
    ThaModuleBmt bmtModule = mThis(self);
    uint32 baseAddress = mMethodsGet(bmtModule)->BaseAddress(bmtModule);
    static eBool initialized = cAtFalse;
    static uint32 holdRegisters[3];
    static const uint8 numHoldRegs = 3;
    uint8 i;

    /* Initialize hold registers */
    if (!initialized)
        {
        initialized = cAtTrue;
        for (i = 0; i < numHoldRegs; i++)
            holdRegisters[i] = 0x10000 + baseAddress + i;

        initialized = cAtTrue;
        }

    /* Number of hold registers */
    if (numberOfHoldRegisters)
        *numberOfHoldRegisters = numHoldRegs;

    return holdRegisters;
    }

static uint8 PwPart(ThaModuleBmt self, AtPw pw)
    {
    ThaModulePw modulePw = (ThaModulePw)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModulePw);
    return ThaModulePwPartOfPw(modulePw, pw);
    }

static ThaDevice Device(ThaModuleBmt self)
    {
    return (ThaDevice)AtModuleDeviceGet((AtModule)self);
    }

static uint32 PwGlobalOffset(ThaModuleBmt self, AtPw pw)
    {
    return ThaDeviceModulePartOffset(Device(self), cThaModuleBmt, (uint8)mMethodsGet(self)->PwPart(self, pw));
    }

static uint32 DdrCpuAcsCacheRequestCtrl(ThaModuleBmt self, AtPw pw)
    {
    return cThaRegBmDdrCpuAcsCacheRequestCtrl + PwGlobalOffset(self, pw);
    }

static eBool IsSimulated(ThaModuleBmt self)
    {
    return AtDeviceIsSimulated(AtModuleDeviceGet((AtModule)self));
    }

static eBool HwDone(ThaModuleBmt self, AtPw pw)
    {
    uint32 elapseTime = 0;
    AtOsal osal = AtSharedDriverOsalGet();
    tAtOsalCurTime startTime, curTime;
    uint32 regVal;

    if (AtDeviceWarmRestoreIsStarted(AtModuleDeviceGet((AtModule)self)))
        return cAtTrue;

    /* Timing waiting for hardware finish */
    mMethodsGet(osal)->CurTimeGet(osal, &startTime);
    while (elapseTime < cThaWrIndrTimeOut)
        {
        /* Done */
        regVal = mChannelHwRead(pw, DdrCpuAcsCacheRequestCtrl(self, pw), cThaModuleBmt);

        if (IsSimulated(self))
            return cAtTrue;

        if ((regVal & cThaBmDdrCpuReqMask) == 0)
            return cAtTrue;

        /* Calculate elapse time */
        mMethodsGet(osal)->CurTimeGet(osal, &curTime);
        elapseTime = mTimeIntervalInMsGet(startTime, curTime);
        }

    return cAtFalse;
    }

static void BlockWrite(ThaModuleBmt self, AtPw pw, uint8 *buffer, uint8 blkId, uint8 hdrLenInByte)
    {
    uint8 dword_i, byte_i;
    uint32 regVal, regAddr;
    uint8 byteIndex;

    for (dword_i = 0; dword_i < cBlockSizeInDwords; dword_i++)
        {
        regVal = 0;
        for (byte_i = 0; byte_i < cNumBytesInDword; byte_i++)
            {
            uint8 value;
            byteIndex = (uint8)((blkId * cBlockSizeInBytes) + (dword_i * cNumBytesInDword) + byte_i);

            value = (uint8)((byteIndex < hdrLenInByte) ? buffer[byteIndex] : 0);
            mFieldIns(&regVal, ByteMask(byte_i), ByteShift(byte_i), value);
            }

        regAddr = cThaRegBmCpuWriteCacheDataCtrl(dword_i) + PwGlobalOffset(self, pw);
        mModuleHwWrite(self, regAddr, regVal);
        }
    }

static uint32 DdrCpuAcsCacheInfoCtrl(ThaModuleBmt self, AtPw pw)
    {
    return cThaRegBmDdrCpuAcsCacheInfoCtrl + PwGlobalOffset(self, pw);
    }

static eAtRet HwRequest(ThaModuleBmt self, AtPw pw, uint32 blkId, eHwRequest requestType)
    {
    uint32 regVal;
    ThaModulePw modulePw = (ThaModulePw)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModulePw);
    uint32 pwId = ThaModulePwLocalPwId(modulePw, pw);

    regVal = 0;
    mFieldIns(&regVal, cThaBmDdrCpuRnWMask, cThaBmDdrCpuRnWShift, requestType);

    /* PW and block ID need to access */
    mFieldIns(&regVal, cThaBmDdrCpuAdrConstMask, cThaBmDdrCpuAdrConstShift, cThaBmDdrCpuAdrConstRstVal);
    mFieldIns(&regVal, cThaBmDdrCpuAdrPwIdMask , cThaBmDdrCpuAdrPwIdShift, pwId);
    mFieldIns(&regVal, cThaBmDdrCpuAdrSegIdMask, cThaBmDdrCpuAdrSegIdShift, blkId);

    /* And request */
    mModuleHwWrite(self, DdrCpuAcsCacheInfoCtrl(self, pw), regVal);
    mModuleHwWrite(self, DdrCpuAcsCacheRequestCtrl(self, pw), 1);

    if (!HwDone(self, pw))
        return cAtErrorDevFail;

    return cAtOk;
    }

static eAtRet _PwHeaderArraySet(ThaModuleBmt self, AtPw pw, uint8 *buffer, uint8 hdrLenInByte)
    {
    uint8 blkId = 0;
    eAtRet ret;
    uint8 curHeaderLen = hdrLenInByte;

    /* Configure header array */
    while (curHeaderLen > 0)
        {
        BlockWrite(self, pw, buffer, blkId, hdrLenInByte);
        ret = HwRequest(self, pw, blkId, cHwRequestWrite);
        if (ret != cAtOk)
            return ret;

        /* Next block */
        blkId = (uint8)(blkId + 1);
        if (curHeaderLen > cBlockSizeInBytes)
            curHeaderLen = (uint8)(curHeaderLen - cBlockSizeInBytes);
        else
            curHeaderLen = 0;
        }

    return cAtOk;
    }

static eAtRet PwHeaderArraySet(ThaModuleBmt self, AtPw pw, uint8 *buffer, uint8 hdrLenInByte)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    const uint8 cRetryTimes = 10;
    uint8 retry_i;

    for (retry_i = 0; retry_i < cRetryTimes; retry_i++)
        {
        /* Retry on fail */
        if (_PwHeaderArraySet(self, pw, buffer, hdrLenInByte) != cAtOk)
            continue;

        if (IsSimulated(self))
            return cAtOk;

        /* Give hardware a moment and read back to check, also retry on fail */
        mMethodsGet(osal)->USleep(osal, 1000);
        mMethodsGet(self)->PwHeaderArrayGet(self, pw, self->sharedBuffer, hdrLenInByte);
        if (mMethodsGet(osal)->MemCmp(osal, self->sharedBuffer, buffer, hdrLenInByte) == 0)
            return cAtOk;
        }

    AtChannelLog((AtChannel)pw, cAtLogLevelWarning, AtSourceLocation, "Read PSN does not match the written buffer\r\n");
    return cAtErrorDevBusy;
    }

static uint8 NumberOfBlocks(uint8 numBytes)
    {
    uint8 blkNum;

    blkNum = numBytes / cBlockSizeInBytes;
    if ((numBytes % cBlockSizeInBytes) != 0)
        blkNum = (uint8)(blkNum + 1);

    return blkNum;
    }

static uint32 CpuReadCacheDataStat(ThaModuleBmt self, AtPw pw, uint32 dwordIndex)
    {
    return cThaRegBmCpuReadCacheDataStat(dwordIndex) + PwGlobalOffset(self, pw);
    }

static void BlockRead(ThaModuleBmt self, AtPw pw, uint8 blkId, uint8 *buffer)
    {
    uint8 dword_i, byte_i;
    uint32 regVal;

    for (dword_i = 0; dword_i < cBlockSizeInDwords; dword_i++)
        {
        regVal = mModuleHwRead(self, CpuReadCacheDataStat(self, pw, dword_i));
        for (byte_i = 0; byte_i < cNumBytesInDword; byte_i++)
            {
            uint16 byteIndex = (uint16)((blkId * cBlockSizeInBytes) + (dword_i * cNumBytesInDword) + byte_i);
            mFieldGet(regVal, ByteMask(byte_i), ByteShift(byte_i), uint8, &(buffer[byteIndex]));
            }
        }
    }

static eAtRet PwHeaderArrayGet(ThaModuleBmt self, AtPw pw, uint8 *buffer, uint8 hdrLenInByte)
    {
    uint8 blkId;

    for (blkId = 0; blkId < NumberOfBlocks(hdrLenInByte); blkId ++)
        {
        mChannelSuccessAssert(pw, HwRequest(self, pw, blkId, cHwRequestRead));

        BlockRead(self, pw, blkId, buffer);
        }

    return cAtOk;
    }

static uint32 PwDefaultOffset(ThaModuleBmt self, AtPw pw)
    {
    ThaModulePw modulePw = (ThaModulePw)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModulePw);
    return ThaModulePwLocalPwId(modulePw, pw) + PwGlobalOffset(self, pw);
    }

static eAtRet PwPrioritySet(ThaModuleBmt self, AtPw pw, uint8 priority)
    {
	AtUnused(priority);
	AtUnused(pw);
	AtUnused(self);
    /* Hardware not support */
    return cAtErrorModeNotSupport;
    }

static uint8 PwPriorityGet(ThaModuleBmt self, AtPw pw)
    {
	AtUnused(pw);
	AtUnused(self);
    /* Hardware not support */
    return 0;
    }

static eAtRet Enable(ThaModuleBmt self, AtPw pw, eBool enable)
    {
    uint32 address, regVal;

    /* Read from hardware */
    address = cThaRegBmPWEnableCtrl + mMethodsGet(self)->PwDefaultOffset(self, pw);
    regVal = mChannelHwRead(pw, address, cThaModuleBmt);
    mFieldIns(&regVal, cThaBmPWEnMask, cThaBmPWEnShift, (mBoolToBin(enable)));

    /* Write to hardware */
    mChannelHwWrite(pw, address, regVal, cThaModuleBmt);

    return cAtOk;
    }

static eBool IsEnabled(ThaModuleBmt self, AtPw pw)
    {
    eBool isEnabled;
    uint32 address, regVal;

    /* Read from hardware */
    address = cThaRegBmPWEnableCtrl + mMethodsGet(self)->PwDefaultOffset(self, pw);
    regVal  = mChannelHwRead(pw, address, cThaModuleBmt);
    mFieldGet(regVal, cThaBmPWEnMask, cThaBmPWEnShift, uint8, &isEnabled);

    return (isEnabled);
    }

static eAtRet Debug(AtModule self)
    {
    uint32 regVal    = mModuleHwRead(self, cThaRegBMStickyStatus);
    uint32 crcStatus = mModuleHwRead(self, cThaRegBMCRC8Sticky);
    ThaModuleBmt bmtModule = mThis(self);

    AtPrintc(cSevInfo, "\r\n");
    AtPrintc(cSevInfo, "BMT information\r\n");
    AtPrintc(cSevInfo, "========================================================\r\n");

    AtPrintc(cSevNormal, "- PSN writing retry: %u times\r\n", bmtModule->psnRetryTimes);
    bmtModule->psnRetryTimes = 0;

    AtPrintc(cSevNormal, "- Error: ");
    if ((regVal == 0) && (crcStatus == 0))
        {
        AtPrintc(cSevInfo, "none\r\n");
        return cAtOk;
        }

    /* Show sticky status */
    if (regVal & cThaBmDiscardCacheError      )    AtPrintc(cSevCritical, " DiscardCacheError");
    if (regVal & cThaBmWriteCacheEror         )    AtPrintc(cSevCritical, " WriteCacheEror");
    if (regVal & cThaBmReadCacheError         )    AtPrintc(cSevCritical, " ReadCacheError");
    if (regVal & cThaBmReadEmpty              )    AtPrintc(cSevCritical, " ReadEmpty");
    if (regVal & cThaBmLinkListFull           )    AtPrintc(cSevCritical, " LinkListFull");
    if (regVal & cThaBmMissSopEop             )    AtPrintc(cSevCritical, " MissSopEop");
    if (regVal & cThaBmOutqueRequestFull      )    AtPrintc(cSevCritical, " OutqueRequestFull");
    if (regVal & cThaBmOutqueBelowThreshold   )    AtPrintc(cSevCritical, " OutqueBelowThreshold");
    if (regVal & cThaBmEnqueMissSopEop        )    AtPrintc(cSevCritical, " EnqueMissSopEop");
    if (regVal & cThaBmOverlen                )    AtPrintc(cSevCritical, " Overlen");
    if (regVal & cThaBmZeroLen                )    AtPrintc(cSevCritical, " ZeroLen");
    if (regVal & cThaBmMaxLenViolate          )    AtPrintc(cSevCritical, " MaxLenViolate");
    if (regVal & cThaBmEnQueueFail            )    AtPrintc(cSevCritical, " EnQueueFail");
    if (regVal & cThaBmEnQueueFifoFull        )    AtPrintc(cSevCritical, " EnQueueFifoFull");
    if (regVal & cThaBmFreeSegmentDiscardFull )    AtPrintc(cSevCritical, " FreeSegmentDiscardFull");
    if (regVal & cThaBmEmptyFreeSegment       )    AtPrintc(cSevCritical, " EmptyFreeSegment");
    mModuleHwWrite(self, cThaRegBMStickyStatus, regVal);

    /* Show CRC status */
    if (crcStatus & cBit0) AtPrintc(cSevCritical, " BM_DDR_Fail");
    mModuleHwWrite(self, cThaRegBMCRC8Sticky, crcStatus);

    AtPrintc(cSevNormal, "\r\n");

    return cAtOk;
    }

static eAtRet PartCacheInit(ThaModuleBmt self, uint8 partId, uint32 size)
    {
    uint32 regVal, address;
    tAtOsalCurTime startTime, curTime;
    uint32 elapseTime;
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 partOffset = ThaDeviceModulePartOffset(Device(self), cThaModuleBmt, partId);

    /* Config cache size */
    regVal = 0;
    mFieldIns(&regVal, cThaBmCpuPWCacheSizeMask, cThaBmCpuPWCacheSizeShift, size);
    address = cThaRegBmCpuIngPWDynamicCacheSizeCtrl + partOffset;
    mModuleHwWrite(self, address, regVal);

    /* Write request */
    regVal = 0;
    mFieldIns(&regVal, cThaBmCpuPWCacheInitReqMask, cThaBmCpuPWCacheInitReqShift, 1);
    address = cThaRegBmCpuIngPWDynamicCacheInitialRequestCtrl + partOffset;
    mModuleHwWrite(self, address, regVal);

    /* Check  Init process is done or not  */
    elapseTime = 0;
    mMethodsGet(osal)->CurTimeGet(osal, &startTime);
    while(elapseTime < cThaWrIndrTimeOut)
        {
        regVal = mModuleHwRead(self, address);

        if (IsSimulated(self))
            return cAtOk;

        if ((regVal & cThaBmCpuPWCacheInitReqMask) == 0)
            return cAtOk;

        /* Calculate elapse time */
        mMethodsGet(osal)->CurTimeGet(osal, &curTime);
        elapseTime = mTimeIntervalInMsGet(startTime, curTime);
        }

    /* Hardware has not finished yet */
    return cAtErrorDevBusy;
    }

static eBool NeedInitCache(ThaModuleBmt self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static eAtRet CacheInit(ThaModuleBmt self, uint32 size)
    {
    eAtRet ret = cAtOk;
    uint8 part_i;

    if (!mMethodsGet(self)->NeedInitCache(self))
        return cAtOk;

    for (part_i = 0; part_i < ThaDeviceNumPartsOfModule(Device(self), cThaModuleBmt); part_i++)
        ret |= PartCacheInit(self, part_i, size);

    return ret;
    }

static uint32 NumPwsOnPart(ThaModuleBmt self, uint8 partId)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaModulePw pwModule = (ThaModulePw) AtDeviceModuleGet(device, cAtModulePw);
	AtUnused(partId);
    return ThaModulePwNumPwsPerPart(pwModule);
    }

static eAtRet AllBlocksInitOnPart(ThaModuleBmt self, uint8 partId)
    {
    uint16 pw_i;
    uint16 block_i;
    uint32 blockValue;
    uint32 linkListId;
    uint32 regVal, address;
    uint32 startBlock;
    uint32 partOffset = ThaDeviceModulePartOffset(Device(self), cThaModuleBmt, partId);

    for (pw_i = 0; pw_i < NumPwsOnPart(self, partId); pw_i ++)
        {
        for (block_i = 0; block_i < cThaBmBlkNumPerPw; block_i ++)
            {
            if (block_i == (cThaBmBlkNumPerPw - 1))
                blockValue = (uint16)(pw_i * cThaBmBlkNumPerPw);
            else
                blockValue = (uint16)((pw_i * cThaBmBlkNumPerPw) + block_i + 1);

            regVal = 0;
            linkListId = (uint16)((pw_i * cThaBmBlkNumPerPw) + block_i);
            address = cThaRegBmCpuLinkListTabBigblockCtrl + linkListId + partOffset;
            mFieldIns(&regVal, cThaBmLinkListTabMask, cThaBmLinkListTabShift, blockValue);
            mModuleHwWrite(self, address, regVal);
            }

        /* Each PW 10 block */
        regVal = 0;
        mFieldIns(&regVal, cThaBmMaxBlkMask, cThaBmMaxBlkShift, cThaBmBlkNumPerPw - 1);

        startBlock = pw_i * cThaBmBlkNumPerPw;
        mFieldIns(&regVal, cThaBmStartBlkMask, cThaBmStartBlkShift, startBlock);

        address = cThaRegBmCpuPseduowiresBufCtrl + pw_i + partOffset;
        mModuleHwWrite(self, address, regVal);

        startBlock = startBlock + cThaBmBlkNumPerPw;
        }

    return cAtOk;
    }

static eAtRet AllBlocksInit(ThaModuleBmt self)
    {
    uint8 i;
    eAtRet ret = cAtOk;

    for (i = 0; i < ThaDeviceNumPartsOfModule(Device(self), cThaModuleBmt); i++)
        ret |= AllBlocksInitOnPart(self, i);

    return ret;
    }

static eAtRet AllBlocksDefaultSet(ThaModuleBmt self, uint32 cacheSize)
    {
    eAtRet ret = cAtOk;

    ret |= AllBlocksInit(self);
    ret |= CacheInit(self, cacheSize);

    return ret;
    }

static uint8 NumParts(ThaModuleBmt self)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    return ThaDeviceNumPartsOfModule(device, cThaModuleBmt);
    }

static uint32 PartOffset(ThaModuleBmt self, uint8 partId)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    return ThaDeviceModulePartOffset(device, cThaModuleBmt, partId);
    }

static void Reset(ThaModuleBmt self)
    {
    uint32  address, regVal;
    uint8 part_i;

    for (part_i = 0; part_i < NumParts(self); part_i++)
        {
        address = cThaRegChipActiveControl + PartOffset(self, part_i);

        regVal = mModuleHwRead(self, address);
        mFieldIns(&regVal, cThaGlbRegBmtPactMask, cThaGlbRegBmtPactShift, 0);
        mModuleHwWrite(self, address, regVal);
        mFieldIns(&regVal, cThaGlbRegBmtPactMask, cThaGlbRegBmtPactShift, 1);
        mModuleHwWrite(self, address, regVal);

        AtOsalUSleep(1000);
        }
    }

static eAtRet DefaultSet(ThaModuleBmt self)
    {
    Reset(self);
    return AllBlocksDefaultSet(mThis(self), 1023);
    }

static eAtRet Init(AtModule self)
    {
    eAtRet  ret;

    /* Super initialization */
    ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return mMethodsGet(mThis(self))->DefaultSet(mThis(self));
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static void Delete(AtObject self)
    {
    /* Delete private data */

    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static const char *TypeString(AtModule self)
    {
    AtUnused(self);
    return "bmt";
    }

static void MethodsInit(ThaModuleBmt self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));
        mMethodOverride(m_methods, PwHeaderArraySet);
        mMethodOverride(m_methods, PwHeaderArrayGet);
        mMethodOverride(m_methods, PwDefaultOffset);
        mMethodOverride(m_methods, PwPrioritySet);
        mMethodOverride(m_methods, PwPriorityGet);
        mMethodOverride(m_methods, Enable);
        mMethodOverride(m_methods, IsEnabled);
        mMethodOverride(m_methods, BaseAddress);
        mMethodOverride(m_methods, PwPart);
        mMethodOverride(m_methods, DefaultSet);
        mMethodOverride(m_methods, NeedInitCache);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtObject(AtModule self)
    {
    AtObject object = (AtObject)self;

    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, HasRegister);
        mMethodOverride(m_AtModuleOverride, HoldRegistersGet);
        mMethodOverride(m_AtModuleOverride, Debug);
        mMethodOverride(m_AtModuleOverride, TypeString);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaModuleBmt);
    }

AtModule ThaModuleBmtObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleObjectInit(self, cThaModuleBmt, device) == NULL)
        return NULL;

    /* Override */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtModule ThaModuleBmtNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ThaModuleBmtObjectInit(newModule, device);
    }

AtModule ThaDeviceBmtModule(AtDevice device)
    {
    return AtDeviceModuleGet(device, cThaModuleBmt);
    }

uint8 ThaModuleBmtDdrRead(AtModule self, uint32 address, uint32 *values)
    {
    uint8 dwId;
    uint32 regVal = 0;

    /* Make request information */
    mFieldIns(&regVal, cThaBmDdrCpuRnWMask, cThaBmDdrCpuRnWShift, 1);
    mFieldIns(&regVal, cThaBmDdrCpuAdrMask, cThaBmDdrCpuAdrShift, address);
    mModuleHwWrite(self, DdrCpuAcsCacheInfoCtrl(mThis(self), NULL), regVal);

    /* Request hardware */
    mModuleHwWrite(self, DdrCpuAcsCacheRequestCtrl(mThis(self), NULL), 1);
    if (!HwDone(mThis(self), NULL))
        return 0;

    /* Return correct values */
    for (dwId = 0; dwId < cBlockSizeInDwords; dwId++)
        values[dwId] = mModuleHwRead(self, CpuReadCacheDataStat(mThis(self), NULL, dwId));

    return cBlockSizeInDwords;
    }

uint8 ThaModuleBmtDdrWrite(AtModule self, uint32 address, const uint32 *values)
    {
    uint8 dwId;
    uint32 regVal = 0;

    /* Make request information */
    mFieldIns(&regVal, cThaBmDdrCpuRnWMask, cThaBmDdrCpuRnWShift, 0);
    mFieldIns(&regVal, cThaBmDdrCpuAdrMask, cThaBmDdrCpuAdrShift, address);
    mModuleHwWrite(self, DdrCpuAcsCacheInfoCtrl(mThis(self), NULL), regVal);

    /* Write data */
    for (dwId = 0; dwId < cBlockSizeInDwords; dwId++)
        mModuleHwWrite(self, cThaRegBmCpuWriteCacheDataCtrl(dwId), values[dwId]);

    /* Request hardware */
    mModuleHwWrite(self, DdrCpuAcsCacheRequestCtrl(mThis(self), NULL), 1);
    if (!HwDone(mThis(self), NULL))
        return 0;

    return cBlockSizeInDwords;
    }

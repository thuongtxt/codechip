/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : ThaSoftSurEngineSdhPath.c
 *
 * Created Date: Mar 21, 2015
 *
 * Description : SDH Path Surveillance soft engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtChannel.h"
#include "AtSdhChannel.h"
#include "../../../../generic/man/AtDriverInternal.h"
#include "ThaSoftSurEngineInternal.h"
#include "ThaSoftPmParamInternal.h"
#include "../../../../../../components/include/fmpm/atfmpm.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtSurEngineMethods m_AtSurEngineOverride;
static tAtObjectMethods     m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet Init(AtSurEngine self)
    {
    return AtSurEngineProvision(self, cAtTrue);
    }

static eAtRet Enable(AtSurEngine self, eBool enable)
    {
    AtSdhChannel channel = (AtSdhChannel)AtSurEngineChannelGet(self);
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(channel);
    AtSdhChannel parentChannel = AtSdhChannelParentChannelGet(channel);
    eAtSdhChannelType parentChannelType = AtSdhChannelTypeGet(parentChannel);

    if ((channelType == cAtSdhChannelTypeVc4) ||
        (channelType == cAtSdhChannelTypeVc4_4c) ||
        ((channelType == cAtSdhChannelTypeVc3) && (parentChannelType == cAtSdhChannelTypeAu3)))
        return AtSurAdaptHoVcSurEngineEnable(channel, enable);

    if (channelType == cAtSdhChannelTypeVc3)
        return AtSurAdaptTu3SurEngineEnable(channel, enable);

    if (channelType == cAtSdhChannelTypeVc12)
        return AtSurAdaptTuSurEngineEnable(channel, enable);

    return cAtErrorModeNotSupport;
    }

static eBool IsEnabled(AtSurEngine self)
    {
    AtSdhChannel channel = (AtSdhChannel)AtSurEngineChannelGet(self);
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(channel);
    AtSdhChannel parentChannel = AtSdhChannelParentChannelGet(channel);
    eAtSdhChannelType parentChannelType = AtSdhChannelTypeGet(parentChannel);

    if ((channelType == cAtSdhChannelTypeVc4) ||
        (channelType == cAtSdhChannelTypeVc4_4c) ||
        ((channelType == cAtSdhChannelTypeVc3) && (parentChannelType == cAtSdhChannelTypeAu3)))
        return AtSurAdaptHoVcSurEngineIsEnabled(channel);

    if (channelType == cAtSdhChannelTypeVc3)
        return AtSurAdaptTu3SurEngineIsEnabled(channel);

    if (channelType == cAtSdhChannelTypeVc12)
        return AtSurAdaptTuSurEngineIsEnabled(channel);

    return cAtFalse;
    }

static uint32 CurrentFailuresGet(AtSurEngine self)
    {
    AtSdhChannel channel = (AtSdhChannel)AtSurEngineChannelGet(self);
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(channel);
    AtSdhChannel parentChannel = AtSdhChannelParentChannelGet(channel);
    eAtSdhChannelType parentChannelType = AtSdhChannelTypeGet(parentChannel);

    if ((channelType == cAtSdhChannelTypeVc4) ||
        (channelType == cAtSdhChannelTypeVc4_4c) ||
        ((channelType == cAtSdhChannelTypeVc3) && (parentChannelType == cAtSdhChannelTypeAu3)))
        return AtSurAdaptHoVcFailureGet(channel);

    if (channelType == cAtSdhChannelTypeVc3)
        return AtSurAdaptTu3FailureGet(channel);

    if (channelType == cAtSdhChannelTypeVc12)
        return AtSurAdaptTuFailureGet(channel);

    return 0;
    }

static uint32 FailureChangedHistoryGet(AtSurEngine self)
    {
    AtSdhChannel channel = (AtSdhChannel)AtSurEngineChannelGet(self);
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(channel);
    AtSdhChannel parentChannel = AtSdhChannelParentChannelGet(channel);
    eAtSdhChannelType parentChannelType = AtSdhChannelTypeGet(parentChannel);

    if ((channelType == cAtSdhChannelTypeVc4) ||
        (channelType == cAtSdhChannelTypeVc4_4c) ||
        ((channelType == cAtSdhChannelTypeVc3) && (parentChannelType == cAtSdhChannelTypeAu3)))
        return AtSurAdaptHoVcFailureHistoryGet(channel);

    if (channelType == cAtSdhChannelTypeVc3)
        return AtSurAdaptTu3FailureHistoryGet(channel);

    if (channelType == cAtSdhChannelTypeVc12)
        return AtSurAdaptTuFailureHistoryGet(channel);

    return 0;
    }

static uint32 FailureChangedHistoryClear(AtSurEngine self)
    {
    AtSdhChannel channel = (AtSdhChannel)AtSurEngineChannelGet(self);
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(channel);
    AtSdhChannel parentChannel = AtSdhChannelParentChannelGet(channel);
    eAtSdhChannelType parentChannelType = AtSdhChannelTypeGet(parentChannel);

    if ((channelType == cAtSdhChannelTypeVc4) ||
        (channelType == cAtSdhChannelTypeVc4_4c) ||
        ((channelType == cAtSdhChannelTypeVc3) && (parentChannelType == cAtSdhChannelTypeAu3)))
        return AtSurAdaptHoVcFailureHistoryClear(channel);

    if (channelType == cAtSdhChannelTypeVc3)
        return AtSurAdaptTu3FailureHistoryClear(channel);

    if (channelType == cAtSdhChannelTypeVc12)
        return AtSurAdaptTuFailureHistoryClear(channel);

    return 0;
    }

static void Delete(AtObject self)
    {
    AtSurEngineProvision((AtSurEngine)self, cAtFalse);

    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static eAtRet Provision(AtSurEngine self, eBool provision)
    {
    AtSdhChannel channel = (AtSdhChannel)AtSurEngineChannelGet((AtSurEngine)self);
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(channel);
    AtSdhChannel parentChannel = AtSdhChannelParentChannelGet(channel);
    eAtSdhChannelType parentChannelType = AtSdhChannelTypeGet(parentChannel);

    self->isProvisioned = provision;

    if ((channelType == cAtSdhChannelTypeVc4) ||
        (channelType == cAtSdhChannelTypeVc4_4c) ||
        ((channelType == cAtSdhChannelTypeVc3) && (parentChannelType == cAtSdhChannelTypeAu3)))
        return AtSurAdaptHoVcSurEngineProvision(channel, provision);

    if (channelType == cAtSdhChannelTypeVc3)
        return AtSurAdaptTu3SurEngineProvision(channel, provision);

    if (channelType == cAtSdhChannelTypeVc12)
        return AtSurAdaptTuSurEngineProvision(channel, provision);

    return cAtErrorModeNotSupport;
    }

static void OverrideAtObject(AtSurEngine self)
    {
    AtObject object = (AtObject)self;

    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtSurEngine(AtSurEngine self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSurEngineOverride, mMethodsGet(self), sizeof(m_AtSurEngineOverride));

        mMethodOverride(m_AtSurEngineOverride, Init);
        mMethodOverride(m_AtSurEngineOverride, Enable);
        mMethodOverride(m_AtSurEngineOverride, IsEnabled);
        mMethodOverride(m_AtSurEngineOverride, CurrentFailuresGet);
        mMethodOverride(m_AtSurEngineOverride, FailureChangedHistoryGet);
        mMethodOverride(m_AtSurEngineOverride, FailureChangedHistoryClear);
        mMethodOverride(m_AtSurEngineOverride, Provision);
        }

    mMethodsSet(self, &m_AtSurEngineOverride);
    }

static void Override(AtSurEngine self)
    {
    OverrideAtObject(self);
    OverrideAtSurEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaSoftSurEngineSdhPath);
    }

AtSurEngine ThaSoftSurEngineSdhPathObjectInit(AtSurEngine self, AtModuleSur module, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (AtSurEngineSdhPathObjectInit(self, module, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSurEngine ThaSoftSurEngineSdhPathNew(AtModuleSur module, AtChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSurEngine newSurEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newSurEngine == NULL)
        return NULL;

    /* Construct it */
    return ThaSoftSurEngineSdhPathObjectInit(newSurEngine, module, channel);
    }

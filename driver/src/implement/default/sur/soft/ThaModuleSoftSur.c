/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : ThaModuleSoftSur.c
 *
 * Created Date: Mar 19, 2015
 *
 * Description : Surveillance Soft module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaModuleSoftSurInternal.h"
#include "ThaSoftSurEngineInternal.h"
#include "ThaSoftPmParamInternal.h"
#include "ThaSoftPmRegisterInternal.h"
#include "fmpm/atfmpm.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtObjectMethods    m_AtObjectOverride;
static tAtModuleMethods    m_AtModuleOverride;
static tAtModuleSurMethods m_AtModuleSurOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtSurEngine SdhLineEngineCreate(AtModuleSur self, AtSdhLine line)
    {
    return ThaSoftSurEngineSdhLineNew(self, (AtChannel)line);
    }

static AtSurEngine SdhHoPathEngineCreate(AtModuleSur self, AtSdhPath path)
    {
    return ThaSoftSurEngineSdhPathNew(self, (AtChannel)path);
    }

static AtSurEngine SdhLoPathEngineCreate(AtModuleSur self, AtSdhPath path)
    {
    return ThaSoftSurEngineSdhPathNew(self, (AtChannel)path);
    }

static AtSurEngine PdhDe1EngineCreate(AtModuleSur self, AtPdhDe1 de1)
    {
    return ThaSoftSurEnginePdhDe1New(self, (AtChannel)de1);
    }

static AtSurEngine PdhDe3EngineCreate(AtModuleSur self, AtPdhDe3 de3)
    {
    return ThaSoftSurEnginePdhDe3New(self, (AtChannel)de3);
    }

static AtSurEngine PwEngineCreate(AtModuleSur self, AtPw pw)
    {
    return ThaSoftSurEnginePwNew(self, (AtChannel)pw);
    }

static eAtRet Init(AtModule self)
    {
    return AtSurAdaptInit((AtModuleDeviceGet(self)));
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static void Delete(AtObject self)
    {
    AtSurAdaptDelete((AtModuleDeviceGet((AtModule)self)));

    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static uint32 PeriodicProcess(AtModule self, uint32 periodInMs)
    {
    tAtOsalCurTime startTime, stopTime;
    AtUnused(self);
    AtUnused(periodInMs);

    AtOsalCurTimeGet(&startTime);
    AtSurAdaptEngPoll();
    AtOsalCurTimeGet(&stopTime);

    return mTimeIntervalInMsGet(startTime, stopTime);
    }

static AtPmRegister CurrentSecondRegisterCreate(AtModuleSur self, AtPmParam param)
    {
    AtUnused(self);
    return ThaSoftPmRegisterCurrentSecondNew(param);
    }

static AtPmRegister CurrentPeriodRegisterCreate(AtModuleSur self, AtPmParam param)
    {
    AtUnused(self);
    return ThaSoftPmRegisterCurrentPeriodNew(param);
    }

static AtPmRegister PreviousPeriodRegisterCreate(AtModuleSur self, AtPmParam param)
    {
    AtUnused(self);
    return ThaSoftPmRegisterPreviousPeriodNew(param);
    }

static AtPmRegister CurrentDayRegisterCreate(AtModuleSur self, AtPmParam param)
    {
    AtUnused(self);
    return ThaSoftPmRegisterCurrentDayNew(param);
    }

static AtPmRegister PreviousDayRegisterCreate(AtModuleSur self, AtPmParam param)
    {
    AtUnused(self);
    return  ThaSoftPmRegisterPreviousDayNew(param);
    }

static AtPmRegister RecentRegisterCreate(AtModuleSur self, AtPmParam param, uint8 recentPeriod)
    {
    AtUnused(self);
    return ThaSoftPmRegisterRecentNew(param, recentPeriod);
    }

static AtPmParam SdhLineParamCreate(AtModuleSur self, AtSurEngine engine, const char *name, uint32 paramType)
    {
    AtUnused(self);
    return ThaSoftPmParamSdhLineNew(engine, name, paramType);
    }

static AtPmParam SdhPathParamCreate(AtModuleSur self, AtSurEngine engine, const char *name, uint32 paramType)
    {
    AtUnused(self);
    return ThaSoftPmParamSdhPathNew(engine, name, paramType);
    }

static AtPmParam PdhDe3ParamCreate(AtModuleSur self, AtSurEngine engine, const char *name, uint32 paramType)
    {
    AtUnused(self);
    return ThaSoftPmParamPdhDe3New(engine, name, paramType);
    }

static AtPmParam PdhDe1ParamCreate(AtModuleSur self, AtSurEngine engine, const char *name, uint32 paramType)
    {
    AtUnused(self);
    return ThaSoftPmParamPdhDe1New(engine, name, paramType);
    }

static AtPmParam PwParamCreate(AtModuleSur self, AtSurEngine engine, const char *name, uint32 paramType)
    {
    AtUnused(self);
    return ThaSoftPmParamPwNew(engine, name, paramType);
    }

static void OverrideAtObject(AtModuleSur self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModuleSur self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(module), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, PeriodicProcess);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtModuleSur(AtModuleSur self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleSurOverride, mMethodsGet(self), sizeof(m_AtModuleSurOverride));

        mMethodOverride(m_AtModuleSurOverride, SdhLineEngineCreate);
        mMethodOverride(m_AtModuleSurOverride, SdhHoPathEngineCreate);
        mMethodOverride(m_AtModuleSurOverride, SdhLoPathEngineCreate);
        mMethodOverride(m_AtModuleSurOverride, PdhDe1EngineCreate);
        mMethodOverride(m_AtModuleSurOverride, PdhDe3EngineCreate);
        mMethodOverride(m_AtModuleSurOverride, PwEngineCreate);

        mMethodOverride(m_AtModuleSurOverride, CurrentSecondRegisterCreate);
        mMethodOverride(m_AtModuleSurOverride, CurrentPeriodRegisterCreate);
        mMethodOverride(m_AtModuleSurOverride, PreviousPeriodRegisterCreate);
        mMethodOverride(m_AtModuleSurOverride, CurrentDayRegisterCreate);
        mMethodOverride(m_AtModuleSurOverride, PreviousDayRegisterCreate);
        mMethodOverride(m_AtModuleSurOverride, RecentRegisterCreate);

        mMethodOverride(m_AtModuleSurOverride, SdhLineParamCreate);
        mMethodOverride(m_AtModuleSurOverride, SdhPathParamCreate);
        mMethodOverride(m_AtModuleSurOverride, PdhDe3ParamCreate);
        mMethodOverride(m_AtModuleSurOverride, PdhDe1ParamCreate);
        mMethodOverride(m_AtModuleSurOverride, PwParamCreate);
        }

    mMethodsSet(self, &m_AtModuleSurOverride);
    }

static void Override(AtModuleSur self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    OverrideAtModuleSur(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaModuleSoftSur);
    }

static AtModuleSur ObjectInit(AtModuleSur self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (AtModuleSurObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSur ThaModuleSoftSurNew(AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSur newSurModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newSurModule == NULL)
        return NULL;

    return ObjectInit(newSurModule, device);
    }

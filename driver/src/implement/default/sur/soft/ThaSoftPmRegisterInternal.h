/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : ThaSoftPmRegisterInternal.h
 * 
 * Created Date: Mar 19, 2015
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASOFTPMREGISTERINTERNAL_H_
#define _THASOFTPMREGISTERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/sur/AtPmRegisterInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaSoftPmRegisterCurrentSecond
    {
    tAtPmRegister super;
    }tThaSoftPmRegisterCurrentSecond;

typedef struct tThaSoftPmRegisterCurrentPeriod
    {
    tAtPmRegister super;
    }tThaSoftPmRegisterCurrentPeriod;

typedef struct tThaSoftPmRegisterPreviousPeriod
    {
    tAtPmRegister super;
    }tThaSoftPmRegisterPreviousPeriod;

typedef struct tThaSoftPmRegisterCurrentDay
    {
    tAtPmRegister super;
    }tThaSoftPmRegisterCurrentDay;

typedef struct tThaSoftPmRegisterPreviousDay
    {
    tAtPmRegister super;
    }tThaSoftPmRegisterPreviousDay;

typedef struct tThaSoftPmRegisterRecent
    {
    tAtPmRegister super;

    /* Private data */
    uint8 recentPeriod;
    }tThaSoftPmRegisterRecent;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPmRegister ThaSoftPmRegisterCurrentSecondNew(AtPmParam param);
AtPmRegister ThaSoftPmRegisterCurrentPeriodNew(AtPmParam param);
AtPmRegister ThaSoftPmRegisterPreviousPeriodNew(AtPmParam param);
AtPmRegister ThaSoftPmRegisterCurrentDayNew(AtPmParam param);
AtPmRegister ThaSoftPmRegisterPreviousDayNew(AtPmParam param);
AtPmRegister ThaSoftPmRegisterRecentNew(AtPmParam param, uint8 recentPeriod);

uint8 ThaSoftPmRegisterRecentPeriodGet(AtPmRegister self);

#endif /* _THASOFTPMREGISTERINTERNAL_H_ */


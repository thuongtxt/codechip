/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : ThaSoftSurEngineSdhLine.c
 *
 * Created Date: Mar 19, 2015
 *
 * Description : SDH Line Surveillance soft engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtChannel.h"
#include "ThaSoftSurEngineInternal.h"
#include "ThaSoftPmParamInternal.h"
#include "../../../../../../components/include/fmpm/atfmpm.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtSurEngineMethods m_AtSurEngineOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/
AtSurEngine ThaSoftSurEngineSdhLineObjectInit(AtSurEngine self, AtModuleSur module, AtChannel channel);

/*--------------------------- Implementation ---------------------------------*/
static eAtRet Init(AtSurEngine self)
    {
    return AtSurEngineProvision(self, cAtTrue);
    }

static eAtRet Enable(AtSurEngine self, eBool enable)
    {
    AtSdhLine line = (AtSdhLine)AtSurEngineChannelGet(self);
    return AtSurAdaptLineSurEngineEnable(line, enable);
    }

static eBool IsEnabled(AtSurEngine self)
    {
    AtSdhLine line = (AtSdhLine)AtSurEngineChannelGet(self);
    return AtSurAdaptLineSurEngineIsEnabled(line);
    }

static uint32 CurrentFailuresGet(AtSurEngine self)
    {
    AtSdhLine line = (AtSdhLine)AtSurEngineChannelGet(self);
    return AtSurAdaptLineFailureGet(line);
    }

static uint32 FailureChangedHistoryGet(AtSurEngine self)
    {
    AtSdhLine line = (AtSdhLine)AtSurEngineChannelGet(self);
    return AtSurAdaptLineFailureHistoryGet(line);
    }

static uint32 FailureChangedHistoryClear(AtSurEngine self)
    {
    AtSdhLine line = (AtSdhLine)AtSurEngineChannelGet(self);
    return AtSurAdaptLineFailureHistoryClear(line);
    }

static void Delete(AtObject self)
    {
    AtSurEngineProvision((AtSurEngine)self, cAtFalse);

    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static eAtRet Provision(AtSurEngine self, eBool provision)
    {
    self->isProvisioned = provision;
    return AtSurAdaptLineSurEngineProvision((AtSdhLine)AtSurEngineChannelGet(self), provision);
    }

static void OverrideAtObject(AtSurEngine self)
    {
    AtObject object = (AtObject)self;

    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtSurEngine(AtSurEngine self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSurEngineOverride, mMethodsGet(self), sizeof(m_AtSurEngineOverride));

        mMethodOverride(m_AtSurEngineOverride, Init);
        mMethodOverride(m_AtSurEngineOverride, Enable);
        mMethodOverride(m_AtSurEngineOverride, IsEnabled);
        mMethodOverride(m_AtSurEngineOverride, CurrentFailuresGet);
        mMethodOverride(m_AtSurEngineOverride, FailureChangedHistoryGet);
        mMethodOverride(m_AtSurEngineOverride, FailureChangedHistoryClear);
        mMethodOverride(m_AtSurEngineOverride, Provision);
        }

    mMethodsSet(self, &m_AtSurEngineOverride);
    }

static void Override(AtSurEngine self)
    {
    OverrideAtObject(self);
    OverrideAtSurEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaSoftSurEngineSdhLine);
    }

AtSurEngine ThaSoftSurEngineSdhLineObjectInit(AtSurEngine self, AtModuleSur module, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (AtSurEngineSdhLineObjectInit(self, module, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSurEngine ThaSoftSurEngineSdhLineNew(AtModuleSur module, AtChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSurEngine newSurEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newSurEngine == NULL)
        return NULL;

    /* Construct it */
    return ThaSoftSurEngineSdhLineObjectInit(newSurEngine, module, channel);
    }

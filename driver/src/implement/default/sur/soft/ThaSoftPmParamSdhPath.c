/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : ThaSoftPmParamSdhPath.c
 *
 * Created Date: Mar 21, 2015
 *
 * Description : SDH Surveillance soft engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtChannel.h"
#include "AtSdhChannel.h"
#include "../../../../generic/sur/AtSurEngineInternal.h"
#include "../../../../generic/man/AtDriverInternal.h"
#include "ThaSoftPmParamInternal.h"
#include "ThaSoftPmRegisterInternal.h"
#include "../../../../../../components/include/fmpm/atfmpm.h"


/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtPmParamMethods m_AtPmParamOverride;
static tThaSoftPmParamMethods m_ThaSoftPmParamOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 NumRecentPeriodRegisters(AtPmParam self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet ThresholdSet(AtPmParam self, uint32 threshold, eAtSurAdaptPmRegType pmRegType)
    {
    AtSdhChannel channel = (AtSdhChannel)AtSurEngineChannelGet(AtPmParamEngineGet(self));
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(channel);
    AtSdhChannel parentChannel = AtSdhChannelParentChannelGet(channel);
    eAtSdhChannelType parentChannelType = AtSdhChannelTypeGet(parentChannel);

    if ((channelType == cAtSdhChannelTypeVc4) ||
        (channelType == cAtSdhChannelTypeVc4_4c) ||
        ((channelType == cAtSdhChannelTypeVc3) && (parentChannelType == cAtSdhChannelTypeAu3)))
        return AtSurAdaptHoVcPmThresholdSet(channel, pmRegType, AtPmParamTypeGet(self), threshold);

    if (channelType == cAtSdhChannelTypeVc3)
        return AtSurAdaptTu3PmThresholdSet(channel, pmRegType, AtPmParamTypeGet(self), threshold);

    if (channelType == cAtSdhChannelTypeVc12)
        return AtSurAdaptTuPmThresholdSet(channel, pmRegType, AtPmParamTypeGet(self), threshold);

    return cAtErrorModeNotSupport;
    }

static eAtRet PeriodThresholdSet(AtPmParam self, uint32 threshold)
    {
    return ThresholdSet(self, cAtSurAdaptPmCurPer, threshold);
    }

static uint32 ThresholdGet(AtPmParam self, eAtSurAdaptPmRegType pmRegType)
    {
    AtSdhChannel channel = (AtSdhChannel)AtSurEngineChannelGet(AtPmParamEngineGet(self));
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(channel);
    AtSdhChannel parentChannel = AtSdhChannelParentChannelGet(channel);
    eAtSdhChannelType parentChannelType = AtSdhChannelTypeGet(parentChannel);

    if ((channelType == cAtSdhChannelTypeVc4) ||
        (channelType == cAtSdhChannelTypeVc4_4c) ||
        ((channelType == cAtSdhChannelTypeVc3) && (parentChannelType == cAtSdhChannelTypeAu3)))
        return AtSurAdaptHoVcPmThresholdGet(channel, pmRegType, AtPmParamTypeGet(self));

    if (channelType == cAtSdhChannelTypeVc3)
        return AtSurAdaptTu3PmThresholdGet(channel, pmRegType, AtPmParamTypeGet(self));

    if (channelType == cAtSdhChannelTypeVc12)
        return AtSurAdaptTuPmThresholdGet(channel, pmRegType, AtPmParamTypeGet(self));

    return 0;
    }

static uint32 PeriodThresholdGet(AtPmParam self)
    {
    return ThresholdGet(self, cAtSurAdaptPmCurPer);
    }

static eAtRet DayThresholdSet(AtPmParam self, uint32 threshold)
    {
    return ThresholdSet(self, cAtSurAdaptPmCurDay, threshold);
    }

static uint32 DayThresholdGet(AtPmParam self)
    {
    return ThresholdGet(self, cAtSurAdaptPmCurDay);
    }

static int32 RegisterValue(ThaSoftPmParam self, eAtSurAdaptPmRegType pmRegType)
    {
    AtSdhChannel channel = (AtSdhChannel)AtSurEngineChannelGet(AtPmParamEngineGet((AtPmParam)self));
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(channel);
    AtSdhChannel parentChannel = AtSdhChannelParentChannelGet(channel);
    eAtSdhChannelType parentChannelType = AtSdhChannelTypeGet(parentChannel);

    if ((channelType == cAtSdhChannelTypeVc4) ||
        (channelType == cAtSdhChannelTypeVc4_4c) ||
        ((channelType == cAtSdhChannelTypeVc3) && (parentChannelType == cAtSdhChannelTypeAu3)))
        return (int32)AtSurAdaptHoVcPmValueGet(channel, pmRegType, AtPmParamTypeGet((AtPmParam)self));

    if (channelType == cAtSdhChannelTypeVc3)
        return (int32)AtSurAdaptTu3PmValueGet(channel, pmRegType, AtPmParamTypeGet((AtPmParam)self));

    if (channelType == cAtSdhChannelTypeVc12)
        return (int32)AtSurAdaptTuPmValueGet(channel, pmRegType, AtPmParamTypeGet((AtPmParam)self));

    return 0;
    }

static int32 CurrentSecondRegisterValue(ThaSoftPmParam self)
    {
    /* TODO: old module surveillance does not support it */
    AtUnused(self);
    return 0;
    }

static int32 CurrentPeriodRegisterValue(ThaSoftPmParam self)
    {
    return RegisterValue(self, cAtSurAdaptPmCurPer);
    }

static int32 PreviousPeriodRegisterValue(ThaSoftPmParam self)
    {
    return RegisterValue(self, cAtSurAdaptPmPrePer);
    }

static int32 CurrentDayRegisterValue(ThaSoftPmParam self)
    {
    return RegisterValue(self, cAtSurAdaptPmCurDay);
    }

static int32 PreviousDayRegisterValue(ThaSoftPmParam self)
    {
    return RegisterValue(self, cAtSurAdaptPmPreDay);
    }

static int32 RecentRegisterValue(ThaSoftPmParam self, uint8 recentPeriod)
    {
    AtSdhChannel channel = (AtSdhChannel)AtSurEngineChannelGet(AtPmParamEngineGet((AtPmParam)self));
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(channel);
    AtSdhChannel parentChannel = AtSdhChannelParentChannelGet(channel);
    eAtSdhChannelType parentChannelType = AtSdhChannelTypeGet(parentChannel);

    if ((channelType == cAtSdhChannelTypeVc4) ||
        (channelType == cAtSdhChannelTypeVc4_4c) ||
        ((channelType == cAtSdhChannelTypeVc3) && (parentChannelType == cAtSdhChannelTypeAu3)))
        return (int32)AtSurAdaptHoVcPmValueGet(channel, ThaSoftPmParamRecentPeriodIndex(self, recentPeriod), AtPmParamTypeGet((AtPmParam)self));

    if (channelType == cAtSdhChannelTypeVc3)
        return (int32)AtSurAdaptTu3PmValueGet(channel, ThaSoftPmParamRecentPeriodIndex(self, recentPeriod), AtPmParamTypeGet((AtPmParam)self));

    if (channelType == cAtSdhChannelTypeVc12)
        return (int32)AtSurAdaptTuPmValueGet(channel, ThaSoftPmParamRecentPeriodIndex(self, recentPeriod), AtPmParamTypeGet((AtPmParam)self));

    return 0;
    }

static int32 RegisterReset(ThaSoftPmParam self, eAtSurAdaptPmRegType pmRegType)
    {
    AtSdhChannel channel = (AtSdhChannel)AtSurEngineChannelGet(AtPmParamEngineGet((AtPmParam)self));
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(channel);
    AtSdhChannel parentChannel = AtSdhChannelParentChannelGet(channel);
    eAtSdhChannelType parentChannelType = AtSdhChannelTypeGet(parentChannel);

    if ((channelType == cAtSdhChannelTypeVc4) ||
        (channelType == cAtSdhChannelTypeVc4_4c) ||
        ((channelType == cAtSdhChannelTypeVc3) && (parentChannelType == cAtSdhChannelTypeAu3)))
        AtSurAdaptHoVcPmReset(channel, pmRegType, AtPmParamTypeGet((AtPmParam)self));

    if (channelType == cAtSdhChannelTypeVc3)
        AtSurAdaptTu3PmReset(channel, pmRegType, AtPmParamTypeGet((AtPmParam)self));

    if (channelType == cAtSdhChannelTypeVc12)
        AtSurAdaptTuPmReset(channel, pmRegType, AtPmParamTypeGet((AtPmParam)self));

    return 0;
    }

static int32 CurrentSecondRegisterReset(ThaSoftPmParam self)
    {
    AtUnused(self);
    return 0;
    }

static int32 CurrentPeriodRegisterReset(ThaSoftPmParam self)
    {
    return RegisterReset(self, cAtSurAdaptPmCurPer);
    }

static int32 PreviousPeriodRegisterReset(ThaSoftPmParam self)
    {
    return RegisterReset(self, cAtSurAdaptPmPrePer);
    }

static int32 CurrentDayRegisterReset(ThaSoftPmParam self)
    {
    return RegisterReset(self, cAtSurAdaptPmCurDay);
    }

static int32 PreviousDayRegisterReset(ThaSoftPmParam self)
    {
    return RegisterReset(self, cAtSurAdaptPmPreDay);
    }

static int32 RecentRegisterReset(ThaSoftPmParam self, uint8 recentPeriod)
    {
    AtSdhChannel channel = (AtSdhChannel)AtSurEngineChannelGet(AtPmParamEngineGet((AtPmParam)self));
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(channel);
    AtSdhChannel parentChannel = AtSdhChannelParentChannelGet(channel);
    eAtSdhChannelType parentChannelType = AtSdhChannelTypeGet(parentChannel);

    if ((channelType == cAtSdhChannelTypeVc4) ||
        (channelType == cAtSdhChannelTypeVc4_4c) ||
        ((channelType == cAtSdhChannelTypeVc3) && (parentChannelType == cAtSdhChannelTypeAu3)))
        AtSurAdaptHoVcPmReset(channel, ThaSoftPmParamRecentPeriodIndex(self, recentPeriod), AtPmParamTypeGet((AtPmParam)self));

    if (channelType == cAtSdhChannelTypeVc3)
        AtSurAdaptTu3PmReset(channel, ThaSoftPmParamRecentPeriodIndex(self, recentPeriod), AtPmParamTypeGet((AtPmParam)self));

    if (channelType == cAtSdhChannelTypeVc12)
        AtSurAdaptTuPmReset(channel, ThaSoftPmParamRecentPeriodIndex(self, recentPeriod), AtPmParamTypeGet((AtPmParam)self));

    return 0;
    }

static eBool RegisterIsValid(ThaSoftPmParam self, eAtSurAdaptPmRegType pmRegType)
    {
    AtSdhChannel channel = (AtSdhChannel)AtSurEngineChannelGet(AtPmParamEngineGet((AtPmParam)self));
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(channel);
    AtSdhChannel parentChannel = AtSdhChannelParentChannelGet(channel);
    eAtSdhChannelType parentChannelType = AtSdhChannelTypeGet(parentChannel);

    if ((channelType == cAtSdhChannelTypeVc4) ||
        (channelType == cAtSdhChannelTypeVc4_4c) ||
        ((channelType == cAtSdhChannelTypeVc3) && (parentChannelType == cAtSdhChannelTypeAu3)))
        return AtSurAdaptHoVcPmIsValid(channel, pmRegType, AtPmParamTypeGet((AtPmParam)self));

    if (channelType == cAtSdhChannelTypeVc3)
        return AtSurAdaptTu3PmIsValid(channel, pmRegType, AtPmParamTypeGet((AtPmParam)self));

    if (channelType == cAtSdhChannelTypeVc12)
        return AtSurAdaptTuPmIsValid(channel, pmRegType, AtPmParamTypeGet((AtPmParam)self));

    return cAtFalse;
    }

static eBool CurrentSecondRegisterIsValid(ThaSoftPmParam self)
    {
    /* TODO: old module surveillance does not support it */
    AtUnused(self);
    return 0;
    }

static eBool CurrentPeriodRegisterIsValid(ThaSoftPmParam self)
    {
    return RegisterIsValid(self, cAtSurAdaptPmCurPer);
    }

static eBool PreviousPeriodRegisterIsValid(ThaSoftPmParam self)
    {
    return RegisterIsValid(self, cAtSurAdaptPmPrePer);
    }

static eBool CurrentDayRegisterIsValid(ThaSoftPmParam self)
    {
    return RegisterIsValid(self, cAtSurAdaptPmCurDay);
    }

static eBool PreviousDayRegisterIsValid(ThaSoftPmParam self)
    {
    return RegisterIsValid(self, cAtSurAdaptPmPreDay);
    }

static eBool RecentRegisterIsValid(ThaSoftPmParam self, uint8 recentPeriod)
    {
    AtSdhChannel channel = (AtSdhChannel)AtSurEngineChannelGet(AtPmParamEngineGet((AtPmParam)self));
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(channel);
    AtSdhChannel parentChannel = AtSdhChannelParentChannelGet(channel);
    eAtSdhChannelType parentChannelType = AtSdhChannelTypeGet(parentChannel);

    if ((channelType == cAtSdhChannelTypeVc4) ||
        (channelType == cAtSdhChannelTypeVc4_4c) ||
        ((channelType == cAtSdhChannelTypeVc3) && (parentChannelType == cAtSdhChannelTypeAu3)))
        return AtSurAdaptHoVcPmIsValid(channel, ThaSoftPmParamRecentPeriodIndex(self, recentPeriod), AtPmParamTypeGet((AtPmParam)self));

    if (channelType == cAtSdhChannelTypeVc3)
        return AtSurAdaptTu3PmIsValid(channel, ThaSoftPmParamRecentPeriodIndex(self, recentPeriod), AtPmParamTypeGet((AtPmParam)self));

    if (channelType == cAtSdhChannelTypeVc12)
        return AtSurAdaptTuPmIsValid(channel, ThaSoftPmParamRecentPeriodIndex(self, recentPeriod), AtPmParamTypeGet((AtPmParam)self));

    return cAtFalse;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaSoftPmParamSdhPath);
    }

static void OverrideAtPmParam(AtPmParam self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPmParamOverride, mMethodsGet(self), sizeof(m_AtPmParamOverride));

        mMethodOverride(m_AtPmParamOverride, NumRecentPeriodRegisters);
        mMethodOverride(m_AtPmParamOverride, PeriodThresholdSet);
        mMethodOverride(m_AtPmParamOverride, PeriodThresholdGet);
        mMethodOverride(m_AtPmParamOverride, DayThresholdSet);
        mMethodOverride(m_AtPmParamOverride, DayThresholdGet);
        }

    mMethodsSet(self, &m_AtPmParamOverride);
    }

static void OverrideThaSoftPmParam(AtPmParam self)
    {
    ThaSoftPmParam param = (ThaSoftPmParam)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSoftPmParamOverride, mMethodsGet(param), sizeof(m_ThaSoftPmParamOverride));

        mMethodOverride(m_ThaSoftPmParamOverride, CurrentSecondRegisterValue);
        mMethodOverride(m_ThaSoftPmParamOverride, CurrentPeriodRegisterValue);
        mMethodOverride(m_ThaSoftPmParamOverride, PreviousPeriodRegisterValue);
        mMethodOverride(m_ThaSoftPmParamOverride, CurrentDayRegisterValue);
        mMethodOverride(m_ThaSoftPmParamOverride, PreviousDayRegisterValue);
        mMethodOverride(m_ThaSoftPmParamOverride, RecentRegisterValue);

        mMethodOverride(m_ThaSoftPmParamOverride, CurrentSecondRegisterIsValid);
        mMethodOverride(m_ThaSoftPmParamOverride, CurrentPeriodRegisterIsValid);
        mMethodOverride(m_ThaSoftPmParamOverride, PreviousPeriodRegisterIsValid);
        mMethodOverride(m_ThaSoftPmParamOverride, CurrentDayRegisterIsValid);
        mMethodOverride(m_ThaSoftPmParamOverride, PreviousDayRegisterIsValid);
        mMethodOverride(m_ThaSoftPmParamOverride, RecentRegisterIsValid);

        mMethodOverride(m_ThaSoftPmParamOverride, CurrentSecondRegisterReset);
        mMethodOverride(m_ThaSoftPmParamOverride, CurrentPeriodRegisterReset);
        mMethodOverride(m_ThaSoftPmParamOverride, PreviousPeriodRegisterReset);
        mMethodOverride(m_ThaSoftPmParamOverride, CurrentDayRegisterReset);
        mMethodOverride(m_ThaSoftPmParamOverride, PreviousDayRegisterReset);
        mMethodOverride(m_ThaSoftPmParamOverride, RecentRegisterReset);
        }

    mMethodsSet(param, &m_ThaSoftPmParamOverride);
    }


static void Override(AtPmParam self)
    {
    OverrideAtPmParam(self);
    OverrideThaSoftPmParam(self);
    }

AtPmParam ThaSoftPmParamSdhPathObjectInit(AtPmParam self, AtSurEngine engine, const char *name, uint32 pmType)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (ThaSoftPmParamObjectInit(self, engine, name, pmType) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPmParam ThaSoftPmParamSdhPathNew(AtSurEngine engine, const char *name, uint32 pmType)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPmParam newPmParam = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPmParam == NULL)
        return NULL;

    /* Construct it */
    return ThaSoftPmParamSdhPathObjectInit(newPmParam, engine, name, pmType);
    }

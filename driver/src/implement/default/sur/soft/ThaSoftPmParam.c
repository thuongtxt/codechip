/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : ThaSoftPmParam.c
 *
 * Created Date: Mar 21, 2015
 *
 * Description : PM parameter
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/man/AtDriverInternal.h"
#include "ThaSoftPmParamInternal.h"
#include "ThaSoftPmRegisterInternal.h"
#include "fmpm/atfmpm.h"

/*--------------------------- Define -----------------------------------------*/
#define cSurInvalidValue (int32)cInvalidUint32

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tThaSoftPmParamMethods m_methods;

/* Override */
static tAtPmParamMethods m_AtPmParamOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 NumRecentPeriodRegisters(AtPmParam self)
    {
    /* This number can be adjusted on demand */
    AtUnused(self);
    return 16;
    }

static int32 CurrentSecondRegisterValue(ThaSoftPmParam self)
    {
    AtUnused(self);
    return 0;
    }

static int32 CurrentPeriodRegisterValue(ThaSoftPmParam self)
    {
    AtUnused(self);
    return 0;
    }

static int32 PreviousPeriodRegisterValue(ThaSoftPmParam self)
    {
    AtUnused(self);
    return 0;
    }

static int32 CurrentDayRegisterValue(ThaSoftPmParam self)
    {
    AtUnused(self);
    return 0;
    }

static int32 PreviousDayRegisterValue(ThaSoftPmParam self)
    {
    AtUnused(self);
    return 0;
    }

static int32 RecentRegisterValue(ThaSoftPmParam self, uint8 recentPeriod)
    {
    AtUnused(self);
    AtUnused(recentPeriod);
    return 0;
    }

static int32 CurrentSecondRegisterReset(ThaSoftPmParam self)
    {
    AtUnused(self);
    return 0;
    }

static int32 CurrentPeriodRegisterReset(ThaSoftPmParam self)
    {
    AtUnused(self);
    return 0;
    }

static int32 PreviousPeriodRegisterReset(ThaSoftPmParam self)
    {
    AtUnused(self);
    return 0;
    }

static int32 CurrentDayRegisterReset(ThaSoftPmParam self)
    {
    AtUnused(self);
    return 0;
    }

static int32 PreviousDayRegisterReset(ThaSoftPmParam self)
    {
    AtUnused(self);
    return 0;
    }

static int32 RecentRegisterReset(ThaSoftPmParam self, uint8 recentPeriod)
    {
    AtUnused(self);
    AtUnused(recentPeriod);
    return 0;
    }

static eBool CurrentSecondRegisterIsValid(ThaSoftPmParam self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool CurrentPeriodRegisterIsValid(ThaSoftPmParam self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool PreviousPeriodRegisterIsValid(ThaSoftPmParam self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool CurrentDayRegisterIsValid(ThaSoftPmParam self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool PreviousDayRegisterIsValid(ThaSoftPmParam self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool RecentRegisterIsValid(ThaSoftPmParam self, uint8 recentPeriod)
    {
    AtUnused(self);
    AtUnused(recentPeriod);
    return cAtFalse;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaSoftPmParam);
    }

static void MethodsInit(AtPmParam self)
    {
    ThaSoftPmParam param = (ThaSoftPmParam)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, CurrentSecondRegisterValue);
        mMethodOverride(m_methods, CurrentPeriodRegisterValue);
        mMethodOverride(m_methods, PreviousPeriodRegisterValue);
        mMethodOverride(m_methods, CurrentDayRegisterValue);
        mMethodOverride(m_methods, PreviousDayRegisterValue);
        mMethodOverride(m_methods, RecentRegisterValue);

        mMethodOverride(m_methods, CurrentSecondRegisterIsValid);
        mMethodOverride(m_methods, CurrentPeriodRegisterIsValid);
        mMethodOverride(m_methods, PreviousPeriodRegisterIsValid);
        mMethodOverride(m_methods, CurrentDayRegisterIsValid);
        mMethodOverride(m_methods, PreviousDayRegisterIsValid);
        mMethodOverride(m_methods, RecentRegisterIsValid);

        mMethodOverride(m_methods, CurrentSecondRegisterReset);
        mMethodOverride(m_methods, CurrentPeriodRegisterReset);
        mMethodOverride(m_methods, PreviousPeriodRegisterReset);
        mMethodOverride(m_methods, CurrentDayRegisterReset);
        mMethodOverride(m_methods, PreviousDayRegisterReset);
        mMethodOverride(m_methods, RecentRegisterReset);
        }

    mMethodsSet(param, &m_methods);
    }

static void OverrideAtPmParam(AtPmParam self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPmParamOverride, mMethodsGet(self), sizeof(m_AtPmParamOverride));

        mMethodOverride(m_AtPmParamOverride, NumRecentPeriodRegisters);
        }

    mMethodsSet(self, &m_AtPmParamOverride);
    }

static void Override(AtPmParam self)
    {
    OverrideAtPmParam(self);
    }

AtPmParam ThaSoftPmParamObjectInit(AtPmParam self, AtSurEngine engine, const char *name, uint32 pmType)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (AtPmParamObjectInit(self, engine, name, pmType) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

eBool ThaSoftPmParamCurrentSecondRegisterIsValid(ThaSoftPmParam self)
    {
    return (eBool)(self ? mMethodsGet(self)->CurrentSecondRegisterIsValid(self) : cAtFalse);
    }

eBool ThaSoftPmParamCurrentPeriodRegisterIsValid(ThaSoftPmParam self)
    {
    return (eBool)(self ? mMethodsGet(self)->CurrentPeriodRegisterIsValid(self) : cAtFalse);
    }

eBool ThaSoftPmParamPreviousPeriodRegisterIsValid(ThaSoftPmParam self)
    {
    return (eBool)(self ? mMethodsGet(self)->PreviousPeriodRegisterIsValid(self) : cAtFalse);
    }

eBool ThaSoftPmParamCurrentDayRegisterIsValid(ThaSoftPmParam self)
    {
    return (eBool)(self ? mMethodsGet(self)->CurrentDayRegisterIsValid(self) : cAtFalse);
    }

eBool ThaSoftPmParamPreviousDayRegisterIsValid(ThaSoftPmParam self)
    {
    return (eBool)(self ? mMethodsGet(self)->PreviousDayRegisterIsValid(self) : cAtFalse);
    }

eBool ThaSoftPmParamRecentRegisterIsValid(ThaSoftPmParam self, uint8 recentPeriod)
    {
    return (eBool)(self ? mMethodsGet(self)->RecentRegisterIsValid(self, recentPeriod) : cAtFalse);
    }

int32 ThaSoftPmParamCurrentSecondRegisterValue(ThaSoftPmParam self)
    {
    return self ? mMethodsGet(self)->CurrentSecondRegisterValue(self) : cSurInvalidValue;
    }

int32 ThaSoftPmParamCurrentPeriodRegisterValue(ThaSoftPmParam self)
    {
    return self ? mMethodsGet(self)->CurrentPeriodRegisterValue(self) : cSurInvalidValue;
    }

int32 ThaSoftPmParamPreviousPeriodRegisterValue(ThaSoftPmParam self)
    {
    return self ? mMethodsGet(self)->PreviousPeriodRegisterValue(self) : cSurInvalidValue;
    }

int32 ThaSoftPmParamCurrentDayRegisterValue(ThaSoftPmParam self)
    {
    return self ? mMethodsGet(self)->CurrentDayRegisterValue(self) : cSurInvalidValue;
    }

int32 ThaSoftPmParamPreviousDayRegisterValue(ThaSoftPmParam self)
    {
    return self ? mMethodsGet(self)->PreviousDayRegisterValue(self) : cSurInvalidValue;
    }

int32 ThaSoftPmParamRecentRegisterValue(ThaSoftPmParam self, uint8 recentPeriod)
    {
    return self ? mMethodsGet(self)->RecentRegisterValue(self, recentPeriod) : cSurInvalidValue;
    }

int32 ThaSoftPmParamCurrentSecondRegisterReset(ThaSoftPmParam self)
    {
    if (self)
        return mMethodsGet(self)->CurrentSecondRegisterReset(self);
    return 0;
    }

int32 ThaSoftPmParamCurrentPeriodRegisterReset(ThaSoftPmParam self)
    {
    if (self)
        return mMethodsGet(self)->CurrentPeriodRegisterReset(self);
    return 0;
    }

int32 ThaSoftPmParamPreviousPeriodRegisterReset(ThaSoftPmParam self)
    {
    if (self)
        return mMethodsGet(self)->PreviousPeriodRegisterReset(self);
    return 0;
    }

int32 ThaSoftPmParamCurrentDayRegisterReset(ThaSoftPmParam self)
    {
    if (self)
        return mMethodsGet(self)->CurrentDayRegisterReset(self);
    return 0;
    }

int32 ThaSoftPmParamPreviousDayRegisterReset(ThaSoftPmParam self)
    {
    if (self)
        return mMethodsGet(self)->PreviousDayRegisterReset(self);
    return 0;
    }

int32 ThaSoftPmParamRecentRegisterReset(ThaSoftPmParam self, uint8 recentPeriod)
    {
    AtUnused(recentPeriod);
    if (self)
        return mMethodsGet(self)->RecentRegisterReset(self, recentPeriod);
    return 0;
    }

uint32 ThaSoftPmParamRecentPeriodIndex(ThaSoftPmParam self, uint8 recentPeriod)
    {
    AtUnused(self);
    return (uint32)(cAtSurAdaptPmRecPer + recentPeriod - 1);
    }

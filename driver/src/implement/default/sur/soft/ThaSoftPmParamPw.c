/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : ThaSoftPmParamPw.c
 *
 * Created Date: Mar 21, 2015
 *
 * Description : PW Surveillance soft engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/man/AtDriverInternal.h"
#include "../../../../generic/sur/AtSurEngineInternal.h"
#include "AtPw.h"
#include "ThaSoftPmParamInternal.h"
#include "ThaSoftPmRegisterInternal.h"
#include "../../../../../../components/include/fmpm/atfmpm.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtPmParamMethods m_AtPmParamOverride;
static tThaSoftPmParamMethods m_ThaSoftPmParamOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet ThresholdSet(AtPmParam self, uint32 threshold, eAtSurAdaptPmRegType pmRegType)
    {
    AtPw pw = (AtPw)AtSurEngineChannelGet(AtPmParamEngineGet(self));
    return AtSurAdaptPwPmThresholdSet(pw, pmRegType, AtPmParamTypeGet(self), threshold);
    }

static uint32 ThresholdGet(AtPmParam self, eAtSurAdaptPmRegType pmRegType)
    {
    AtPw pw = (AtPw)AtSurEngineChannelGet(AtPmParamEngineGet(self));
    return AtSurAdaptPwPmThresholdGet(pw, pmRegType, AtPmParamTypeGet(self));
    }

static eAtRet PeriodThresholdSet(AtPmParam self, uint32 threshold)
    {
    return ThresholdSet(self, threshold, cAtSurAdaptPmCurPer);
    }

static uint32 PeriodThresholdGet(AtPmParam self)
    {
    return ThresholdGet(self, cAtSurAdaptPmCurPer);
    }

static eAtRet DayThresholdSet(AtPmParam self, uint32 threshold)
    {
    return ThresholdSet(self, threshold, cAtSurAdaptPmCurDay);
    }

static uint32 DayThresholdGet(AtPmParam self)
    {
    return ThresholdGet(self, cAtSurAdaptPmCurDay);
    }

static int32 RegisterValue(ThaSoftPmParam self, eAtSurAdaptPmRegType pmRegType)
    {
    AtPw pw = (AtPw)AtSurEngineChannelGet(AtPmParamEngineGet((AtPmParam)self));
    return (int32)AtSurAdaptPwPmValueGet(pw, pmRegType, AtPmParamTypeGet((AtPmParam)self));
    }

static int32 CurrentSecondRegisterValue(ThaSoftPmParam self)
    {
    /* TODO: old module surveillance does not support it */
    AtUnused(self);
    return 0;
    }

static int32 CurrentPeriodRegisterValue(ThaSoftPmParam self)
    {
    return RegisterValue(self, cAtSurAdaptPmCurPer);
    }

static int32 PreviousPeriodRegisterValue(ThaSoftPmParam self)
    {
    return RegisterValue(self, cAtSurAdaptPmPrePer);
    }

static int32 CurrentDayRegisterValue(ThaSoftPmParam self)
    {
    return RegisterValue(self, cAtSurAdaptPmCurDay);
    }

static int32 PreviousDayRegisterValue(ThaSoftPmParam self)
    {
    return RegisterValue(self, cAtSurAdaptPmPreDay);
    }

static int32 RecentRegisterValue(ThaSoftPmParam self, uint8 recentPeriod)
    {
    AtPw pw = (AtPw)AtSurEngineChannelGet(AtPmParamEngineGet((AtPmParam)self));
    return (int32)AtSurAdaptPwPmValueGet(pw, ThaSoftPmParamRecentPeriodIndex(self, recentPeriod), AtPmParamTypeGet((AtPmParam)self));
    }

static int32 RegisterReset(ThaSoftPmParam self, eAtSurAdaptPmRegType pmRegType)
    {
    AtPw pw = (AtPw)AtSurEngineChannelGet(AtPmParamEngineGet((AtPmParam)self));
    AtSurAdaptPwPmReset(pw, pmRegType, AtPmParamTypeGet((AtPmParam)self));
    return 0;
    }

static int32 CurrentSecondRegisterReset(ThaSoftPmParam self)
    {
    AtUnused(self);
    return 0;
    }

static int32 CurrentPeriodRegisterReset(ThaSoftPmParam self)
    {
    return RegisterReset(self, cAtSurAdaptPmCurPer);
    }

static int32 PreviousPeriodRegisterReset(ThaSoftPmParam self)
    {
    return RegisterReset(self, cAtSurAdaptPmPrePer);
    }

static int32 CurrentDayRegisterReset(ThaSoftPmParam self)
    {
    return RegisterReset(self, cAtSurAdaptPmCurDay);
    }

static int32 PreviousDayRegisterReset(ThaSoftPmParam self)
    {
    return RegisterReset(self, cAtSurAdaptPmPreDay);
    }

static int32 RecentRegisterReset(ThaSoftPmParam self, uint8 recentPeriod)
    {
    AtPw pw = (AtPw)AtSurEngineChannelGet(AtPmParamEngineGet((AtPmParam)self));
    AtSurAdaptPwPmReset(pw, ThaSoftPmParamRecentPeriodIndex(self, recentPeriod), AtPmParamTypeGet((AtPmParam)self));
    return 0;
    }

static eBool RegisterIsValid(ThaSoftPmParam self, eAtSurAdaptPmRegType pmRegType)
    {
    AtPw pw = (AtPw)AtSurEngineChannelGet(AtPmParamEngineGet((AtPmParam)self));
    return AtSurAdaptPwPmIsValid(pw, pmRegType, AtPmParamTypeGet((AtPmParam)self));
    }

static eBool CurrentSecondRegisterIsValid(ThaSoftPmParam self)
    {
    /* TODO: old module surveillance does not support it */
    AtUnused(self);
    return 0;
    }

static eBool CurrentPeriodRegisterIsValid(ThaSoftPmParam self)
    {
    return RegisterIsValid(self, cAtSurAdaptPmCurPer);
    }

static eBool PreviousPeriodRegisterIsValid(ThaSoftPmParam self)
    {
    return RegisterIsValid(self, cAtSurAdaptPmPrePer);
    }

static eBool CurrentDayRegisterIsValid(ThaSoftPmParam self)
    {
    return RegisterIsValid(self, cAtSurAdaptPmCurDay);
    }

static eBool PreviousDayRegisterIsValid(ThaSoftPmParam self)
    {
    return RegisterIsValid(self, cAtSurAdaptPmPreDay);
    }

static eBool RecentRegisterIsValid(ThaSoftPmParam self, uint8 recentPeriod)
    {
    AtPw pw = (AtPw)AtSurEngineChannelGet(AtPmParamEngineGet((AtPmParam)self));
    return AtSurAdaptPwPmIsValid(pw, ThaSoftPmParamRecentPeriodIndex(self, recentPeriod), AtPmParamTypeGet((AtPmParam)self));
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaSoftPmParamPw);
    }

static void OverrideAtPmParam(AtPmParam self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPmParamOverride, mMethodsGet(self), sizeof(m_AtPmParamOverride));

        mMethodOverride(m_AtPmParamOverride, PeriodThresholdSet);
        mMethodOverride(m_AtPmParamOverride, PeriodThresholdGet);
        mMethodOverride(m_AtPmParamOverride, DayThresholdSet);
        mMethodOverride(m_AtPmParamOverride, DayThresholdGet);
        }

    mMethodsSet(self, &m_AtPmParamOverride);
    }

static void OverrideThaSoftPmParam(AtPmParam self)
    {
    ThaSoftPmParam param = (ThaSoftPmParam)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSoftPmParamOverride, mMethodsGet(param), sizeof(m_ThaSoftPmParamOverride));

        mMethodOverride(m_ThaSoftPmParamOverride, CurrentSecondRegisterValue);
        mMethodOverride(m_ThaSoftPmParamOverride, CurrentPeriodRegisterValue);
        mMethodOverride(m_ThaSoftPmParamOverride, PreviousPeriodRegisterValue);
        mMethodOverride(m_ThaSoftPmParamOverride, CurrentDayRegisterValue);
        mMethodOverride(m_ThaSoftPmParamOverride, PreviousDayRegisterValue);
        mMethodOverride(m_ThaSoftPmParamOverride, RecentRegisterValue);

        mMethodOverride(m_ThaSoftPmParamOverride, CurrentSecondRegisterIsValid);
        mMethodOverride(m_ThaSoftPmParamOverride, CurrentPeriodRegisterIsValid);
        mMethodOverride(m_ThaSoftPmParamOverride, PreviousPeriodRegisterIsValid);
        mMethodOverride(m_ThaSoftPmParamOverride, CurrentDayRegisterIsValid);
        mMethodOverride(m_ThaSoftPmParamOverride, PreviousDayRegisterIsValid);
        mMethodOverride(m_ThaSoftPmParamOverride, RecentRegisterIsValid);

        mMethodOverride(m_ThaSoftPmParamOverride, CurrentSecondRegisterReset);
        mMethodOverride(m_ThaSoftPmParamOverride, CurrentPeriodRegisterReset);
        mMethodOverride(m_ThaSoftPmParamOverride, PreviousPeriodRegisterReset);
        mMethodOverride(m_ThaSoftPmParamOverride, CurrentDayRegisterReset);
        mMethodOverride(m_ThaSoftPmParamOverride, PreviousDayRegisterReset);
        mMethodOverride(m_ThaSoftPmParamOverride, RecentRegisterReset);
        }

    mMethodsSet(param, &m_ThaSoftPmParamOverride);
    }

static void Override(AtPmParam self)
    {
    OverrideAtPmParam(self);
    OverrideThaSoftPmParam(self);
    }

AtPmParam ThaSoftPmParamPwObjectInit(AtPmParam self, AtSurEngine engine, const char *name, uint32 pmType)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (ThaSoftPmParamObjectInit(self, engine, name, pmType) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPmParam ThaSoftPmParamPwNew(AtSurEngine engine, const char *name, uint32 pmType)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPmParam newPmParam = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPmParam == NULL)
        return NULL;

    /* Construct it */
    return ThaSoftPmParamPwObjectInit(newPmParam, engine, name, pmType);
    }

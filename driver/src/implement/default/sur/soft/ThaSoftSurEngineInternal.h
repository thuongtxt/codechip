/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : ThaSoftSurEngineInternal.h
 * 
 * Created Date: Mar 19, 2015
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASOFTSURENGINEINTERNAL_H_
#define _THASOFTSURENGINEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/sur/AtSurEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaSoftSurEngineSdhLine
    {
    tAtSurEngineSdhLine super;
    }tThaSoftSurEngineSdhLine;

typedef struct tThaSoftSurEngineSdhPath
    {
    tAtSurEngineSdhPath super;
    }tThaSoftSurEngineSdhPath;

typedef struct tThaSoftSurEnginePdhDe1
    {
    tAtSurEnginePdhDe1 super;
    }tThaSoftSurEnginePdhDe1;

typedef struct tThaSoftSurEnginePdhDe3
    {
    tAtSurEnginePdhDe3 super;
    }tThaSoftSurEnginePdhDe3;

typedef struct tThaSoftSurEnginePw
    {
    tAtSurEnginePw super;
    }tThaSoftSurEnginePw;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSurEngine ThaSoftSurEngineSdhLineNew(AtModuleSur module, AtChannel channel);
AtSurEngine ThaSoftSurEngineSdhPathNew(AtModuleSur module, AtChannel channel);
AtSurEngine ThaSoftSurEnginePdhDe1New(AtModuleSur module, AtChannel channel);
AtSurEngine ThaSoftSurEnginePdhDe3New(AtModuleSur module, AtChannel channel);
AtSurEngine ThaSoftSurEnginePwNew(AtModuleSur module, AtChannel channel);

AtSurEngine ThaSoftSurEngineSdhPathObjectInit(AtSurEngine self, AtModuleSur module, AtChannel channel);

#endif /* _THASOFTSURENGINEINTERNAL_H_ */


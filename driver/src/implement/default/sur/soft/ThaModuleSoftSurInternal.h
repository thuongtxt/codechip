/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SUR
 * 
 * File        : ThaModuleSoftSurInternal.h
 * 
 * Created Date: Mar 19, 2015
 *
 * Description : Surveillance module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULESOFTSURINTERNAL_H_
#define _THAMODULESOFTSURINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/sur/AtModuleSurInternal.h"
#include "ThaModuleSoftSur.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleSoftSur
    {
    tAtModuleSur super;
    }tThaModuleSoftSur;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#endif /* _THAMODULESOFTSURINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : ThaSoftPmParamInternal.h
 * 
 * Created Date: Mar 19, 2015
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASOFTPMPARAMINTERNAL_H_
#define _THASOFTPMPARAMINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/sur/AtPmParamInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaSoftPmParam *ThaSoftPmParam;

typedef struct tThaSoftPmParamMethods
    {
    int32 (*CurrentSecondRegisterValue)(ThaSoftPmParam self);
    int32 (*CurrentPeriodRegisterValue)(ThaSoftPmParam self);
    int32 (*PreviousPeriodRegisterValue)(ThaSoftPmParam self);
    int32 (*CurrentDayRegisterValue)(ThaSoftPmParam self);
    int32 (*PreviousDayRegisterValue)(ThaSoftPmParam self);
    int32 (*RecentRegisterValue)(ThaSoftPmParam self, uint8 recentPeriod);

    int32 (*CurrentSecondRegisterReset)(ThaSoftPmParam self);
    int32 (*CurrentPeriodRegisterReset)(ThaSoftPmParam self);
    int32 (*PreviousPeriodRegisterReset)(ThaSoftPmParam self);
    int32 (*CurrentDayRegisterReset)(ThaSoftPmParam self);
    int32 (*PreviousDayRegisterReset)(ThaSoftPmParam self);
    int32 (*RecentRegisterReset)(ThaSoftPmParam self, uint8 recentPeriod);

    eBool (*CurrentSecondRegisterIsValid)(ThaSoftPmParam self);
    eBool (*CurrentPeriodRegisterIsValid)(ThaSoftPmParam self);
    eBool (*PreviousPeriodRegisterIsValid)(ThaSoftPmParam self);
    eBool (*CurrentDayRegisterIsValid)(ThaSoftPmParam self);
    eBool (*PreviousDayRegisterIsValid)(ThaSoftPmParam self);
    eBool (*RecentRegisterIsValid)(ThaSoftPmParam self, uint8 recentPeriod);
    }tThaSoftPmParamMethods;

typedef struct tThaSoftPmParam
    {
    tAtPmParam super;
    const tThaSoftPmParamMethods *methods;
    }tThaSoftPmParam;

typedef struct tThaSoftPmParamSdhLine
    {
    tThaSoftPmParam super;
    }tThaSoftPmParamSdhLine;

typedef struct tThaSoftPmParamSdhPath
    {
    tThaSoftPmParam super;
    }tThaSoftPmParamSdhPath;

typedef struct tThaSoftPmParamPdhDe1
    {
    tThaSoftPmParam super;
    }tThaSoftPmParamPdhDe1;

typedef struct tThaSoftPmParamPdhDe3
    {
    tThaSoftPmParam super;
    }tThaSoftPmParamPdhDe3;

typedef struct tThaSoftPmParamPw
    {
    tThaSoftPmParam super;
    }tThaSoftPmParamPw;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPmParam ThaSoftPmParamObjectInit(AtPmParam self, AtSurEngine engine, const char *name, uint32 pmType);
AtPmParam ThaSoftPmParamSdhLineNew(AtSurEngine engine, const char *name, uint32 pmType);
AtPmParam ThaSoftPmParamSdhPathNew(AtSurEngine engine, const char *name, uint32 pmType);
AtPmParam ThaSoftPmParamPdhDe3New(AtSurEngine engine, const char *name, uint32 pmType);
AtPmParam ThaSoftPmParamPdhDe1New(AtSurEngine engine, const char *name, uint32 pmType);
AtPmParam ThaSoftPmParamPwNew(AtSurEngine engine, const char *name, uint32 pmType);

eBool ThaSoftPmParamCurrentSecondRegisterIsValid(ThaSoftPmParam self);
eBool ThaSoftPmParamCurrentPeriodRegisterIsValid(ThaSoftPmParam self);
eBool ThaSoftPmParamPreviousPeriodRegisterIsValid(ThaSoftPmParam self);
eBool ThaSoftPmParamCurrentDayRegisterIsValid(ThaSoftPmParam self);
eBool ThaSoftPmParamPreviousDayRegisterIsValid(ThaSoftPmParam self);
eBool ThaSoftPmParamRecentRegisterIsValid(ThaSoftPmParam self, uint8 recentPeriod);

int32 ThaSoftPmParamCurrentSecondRegisterReset(ThaSoftPmParam self);
int32 ThaSoftPmParamCurrentPeriodRegisterReset(ThaSoftPmParam self);
int32 ThaSoftPmParamPreviousPeriodRegisterReset(ThaSoftPmParam self);
int32 ThaSoftPmParamCurrentDayRegisterReset(ThaSoftPmParam self);
int32 ThaSoftPmParamPreviousDayRegisterReset(ThaSoftPmParam self);
int32 ThaSoftPmParamRecentRegisterReset(ThaSoftPmParam self, uint8 recentPeriod);

int32 ThaSoftPmParamCurrentSecondRegisterValue(ThaSoftPmParam self);
int32 ThaSoftPmParamCurrentPeriodRegisterValue(ThaSoftPmParam self);
int32 ThaSoftPmParamPreviousPeriodRegisterValue(ThaSoftPmParam self);
int32 ThaSoftPmParamCurrentDayRegisterValue(ThaSoftPmParam self);
int32 ThaSoftPmParamPreviousDayRegisterValue(ThaSoftPmParam self);
int32 ThaSoftPmParamRecentRegisterValue(ThaSoftPmParam self, uint8 recentPeriod);

uint32 ThaSoftPmParamRecentPeriodIndex(ThaSoftPmParam self, uint8 recentPeriod);
AtPmParam ThaSoftPmParamPdhDe3ObjectInit(AtPmParam self, AtSurEngine engine, const char *name, uint32 pmType);
AtPmParam ThaSoftPmParamPdhDe1ObjectInit(AtPmParam self, AtSurEngine engine, const char *name, uint32 pmType);
AtPmParam ThaSoftPmParamPwObjectInit(AtPmParam self, AtSurEngine engine, const char *name, uint32 pmType);
AtPmParam ThaSoftPmParamSdhPathObjectInit(AtPmParam self, AtSurEngine engine, const char *name, uint32 pmType);
AtPmParam ThaSoftPmParamSdhLineObjectInit(AtPmParam self, AtSurEngine engine, const char *name, uint32 pmType);

#endif /* _THASOFTPMPARAMINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : ThaSoftPmRegisterRecent.c
 *
 * Created Date: Mar 21, 2015
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtChannel.h"
#include "../../../../generic/sur/AtSurEngineInternal.h"
#include "../../../../generic/man/AtDriverInternal.h"
#include "ThaSoftPmParamInternal.h"
#include "ThaSoftPmRegisterInternal.h"
#include "../../../../../../components/include/fmpm/atfmpm.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((tThaSoftPmRegisterRecent*)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tAtPmRegisterMethods m_AtPmRegisterOverride;

/*--------------------------- Forward declarations ---------------------------*/
AtPmRegister ThaSoftPmRegisterRecentObjectInit(AtPmRegister self, AtPmParam param, uint8 recentPeriod);

/*--------------------------- Implementation ---------------------------------*/
static int32 Reset(AtPmRegister self)
    {
    return ThaSoftPmParamRecentRegisterReset((ThaSoftPmParam)AtPmRegisterParamGet(self), mThis(self)->recentPeriod);
    }

static int32 Value(AtPmRegister self)
    {
    return ThaSoftPmParamRecentRegisterValue((ThaSoftPmParam)AtPmRegisterParamGet(self), mThis(self)->recentPeriod);
    }

static eBool IsValid(AtPmRegister self)
    {
    return ThaSoftPmParamRecentRegisterIsValid((ThaSoftPmParam)AtPmRegisterParamGet(self), mThis(self)->recentPeriod);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaSoftPmRegisterRecent);
    }

static void OverrideAtPmRegister(AtPmRegister self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPmRegisterOverride, mMethodsGet(self), sizeof(m_AtPmRegisterOverride));

        mMethodOverride(m_AtPmRegisterOverride, Reset);
        mMethodOverride(m_AtPmRegisterOverride, Value);
        mMethodOverride(m_AtPmRegisterOverride, IsValid);
        }

    mMethodsSet(self, &m_AtPmRegisterOverride);
    }

static void Override(AtPmRegister self)
    {
    OverrideAtPmRegister(self);
    }

AtPmRegister ThaSoftPmRegisterRecentObjectInit(AtPmRegister self, AtPmParam param, uint8 recentPeriod)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (AtPmRegisterObjectInit(self, param) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;
    mThis(self)->recentPeriod = recentPeriod;

    return self;
    }

AtPmRegister ThaSoftPmRegisterRecentNew(AtPmParam param, uint8 recentPeriod)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPmRegister newPmRegister = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPmRegister == NULL)
        return NULL;

    /* Construct it */
    return ThaSoftPmRegisterRecentObjectInit(newPmRegister, param, recentPeriod);
    }

uint8 ThaSoftPmRegisterRecentPeriodGet(AtPmRegister self)
    {
    return (uint8)(self ? mThis(self)->recentPeriod : 0);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SUR
 * 
 * File        : ThaSurInterruptManagerInternal.h
 * 
 * Created Date: Dec 30, 2015
 *
 * Description : Default Surveillance interrupt manager.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASURINTERRUPTMANAGERINTERNAL_H_
#define _THASURINTERRUPTMANAGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/sur/AtSurEngineInternal.h"
#include "../../man/intrcontroller/ThaInterruptManagerInternal.h"
#include "ThaSurInterruptManager.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define mThrowNullEngine(self, _type, _offset)                                  \
    { do {                                                                      \
    ThaModuleHardSur _module = SurModule(self);                                 \
    uint32 _address = ThaModuleHardSur##_type##FailuresInterruptMaskAddr(_module);\
    mModuleHwWrite(_module, _address + (_offset), 0x0);                         \
                                                                                \
    _address = ThaModuleHardSur##_type##TcaInterruptMaskAddr(_module);          \
    mModuleHwWrite(_module, _address + (_offset), 0x0);                         \
                                                                                \
    AtModuleLog((AtModule)_module, cAtLogLevelCritical, AtSourceLocation,       \
                "NULL %s surveillance engine trapped at address 0x%08x, offset 0x%08x\n", #_type, _address, (_offset)); \
    } while(0); continue; }

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaSurInterruptManagerMethods
    {
    uint32 (*InterruptMask)(ThaSurInterruptManager self); /* Global interrupt mask of RTL module. */

    void (*LineInterruptProcess)(ThaSurInterruptManager self, AtHal hal);
    void (*StsInterruptProcess)(ThaSurInterruptManager self, AtHal hal);
    void (*VtInterruptProcess)(ThaSurInterruptManager self, AtHal hal);
    void (*De3InterruptProcess)(ThaSurInterruptManager self, AtHal hal);
    void (*De1InterruptProcess)(ThaSurInterruptManager self, AtHal hal);
    void (*PwInterruptProcess)(ThaSurInterruptManager self, AtHal hal);

    eBool (*LineHasInterrupt)(ThaSurInterruptManager self, uint32 glbIntr);
    eBool (*StsHasInterrupt)(ThaSurInterruptManager self, uint32 glbIntr);
    eBool (*VtHasInterrupt)(ThaSurInterruptManager self, uint32 glbIntr);
    eBool (*De3HasInterrupt)(ThaSurInterruptManager self, uint32 glbIntr);
    eBool (*De1HasInterrupt)(ThaSurInterruptManager self, uint32 glbIntr);
    eBool (*PwHasInterrupt)(ThaSurInterruptManager self, uint32 glbIntr);

    /* Please note that Surveillance module has two possible and identical interrupt
     * trees in which one tree is Failure interrupt and other is TCA interrupt.
     * They are only different in address offset. Therefore, this class will be used
     * for both trees as well (with correct offset control). */
    uint32 (*Offset)(ThaSurInterruptManager self);
    void (*EngineNotify)(ThaSurInterruptManager self, AtSurEngine engine, uint32 engineOffset);
    } tThaSurInterruptManagerMethods;

typedef struct tThaSurInterruptManager
    {
    tThaInterruptManager super;
    const tThaSurInterruptManagerMethods* methods;
    } tThaSurInterruptManager;

typedef struct tThaSurTcaInterruptManager
    {
    tThaSurInterruptManager super;
    } tThaSurTcaInterruptManager;

/*--------------------------- Forward declarations ---------------------------*/
AtInterruptManager ThaSurInterruptManagerObjectInit(AtInterruptManager self, AtModule module);
AtInterruptManager ThaSurTcaInterruptManagerObjectInit(AtInterruptManager self, AtModule module);

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THASURINTERRUPTMANAGERINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : ThaHardPmThresholdProfile.c
 *
 * Created Date: Sep 10, 2016
 *
 * Description : Tha PM Threshold Profile
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../../generic/man/AtModuleInternal.h"
#include "../ThaModuleHardSurReg.h"
#include "../ThaModuleHardSur.h"
#include "ThaHardPmThresholdProfileInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((tThaHardPmThresholdProfile*)self)

/*--------------------------- Macros -----------------------------------------*/
#define mWriteThreshold(paramType, levelType, levelRegType, maskName, offset, threshold)                         \
    {                                                                                                            \
    case cAtSurEngine##levelType##PmParam##paramType:                                                            \
        {                                                                                                        \
        uint32 regAddr = cAf6Reg_FMPM_TCA_threshold_of_##levelRegType##_Base + offset;                           \
        uint32 regVal = Read(self, regAddr);                                                                     \
        mRegFieldSet(regVal, cAf6_FMPM_TCA_threshold_of_##levelRegType##_tca_thres_##maskName##_, threshold);    \
        Write(self, regAddr, regVal);                                                                            \
        return cAtOk;                                                                                            \
        }                                                                                                        \
    }

#define mReadThreshold(paramType, levelType, levelRegType, maskName, offset)                           \
    {                                                                                                  \
    case cAtSurEngine##levelType##PmParam##paramType:                                                  \
        {                                                                                              \
        uint32 regAddr = cAf6Reg_FMPM_TCA_threshold_of_##levelRegType##_Base + offset;                 \
        uint32 regVal = Read(self, regAddr);                                                           \
        return mRegField(regVal, cAf6_FMPM_TCA_threshold_of_##levelRegType##_tca_thres_##maskName##_); \
        }                                                                                              \
    }

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtPmThresholdProfileMethods m_AtPmThresholdProfileOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleHardSur SurModule(AtPmThresholdProfile self)
    {
    return (ThaModuleHardSur)AtPmThresholdProfileModuleGet(self);
    }

static uint32 BaseAddress(AtPmThresholdProfile self)
    {
    return ThaModuleHardSurBaseAddress((ThaModuleHardSur)AtPmThresholdProfileModuleGet(self));
    }

static uint32 DefaultOffset(AtPmThresholdProfile self)
    {
    return BaseAddress(self) + AtPmThresholdProfileIdGet(self) * 2;
    }

static uint32 Read(AtPmThresholdProfile self, uint32 address)
    {
    return mModuleHwRead(SurModule(self), address);
    }

static void Write(AtPmThresholdProfile self, uint32 address, uint32 value)
    {
    mModuleHwWrite(SurModule(self), address, value);
    }

static eAtRet SdhLinePmParamCheck(eAtSurEngineSdhLinePmParam param, uint32 threshold)
    {
    if (mOutOfRange(param, cAtSurEngineSdhLinePmParamSefsS, cAtSurEngineSdhLinePmParamFcLfe))
        return cAtErrorOutOfRangParm;

    switch ((uint32)param)
        {
        case cAtSurEngineSdhLinePmParamSefsS:
        case cAtSurEngineSdhLinePmParamEsS:
        case cAtSurEngineSdhLinePmParamSesS:
        case cAtSurEngineSdhLinePmParamEsL:
        case cAtSurEngineSdhLinePmParamSesL:
        case cAtSurEngineSdhLinePmParamUasL:
        case cAtSurEngineSdhLinePmParamFcL:
        case cAtSurEngineSdhLinePmParamEsLfe:
        case cAtSurEngineSdhLinePmParamSesLfe:
        case cAtSurEngineSdhLinePmParamUasLfe:
        case cAtSurEngineSdhLinePmParamFcLfe:
            return mInRange(threshold, 0, cBit9_0) ? cAtOk : cAtErrorOutOfRangParm;

        case cAtSurEngineSdhLinePmParamCvS:
        case cAtSurEngineSdhLinePmParamCvL:
        case cAtSurEngineSdhLinePmParamCvLfe:
            return mInRange(threshold, 0, cBit23_0) ? cAtOk : cAtErrorOutOfRangParm;
        default:
            return cAtErrorModeNotSupport;
        }
    }

static eAtRet SdhLinePeriodTcaThresholdSet(AtPmThresholdProfile self, eAtSurEngineSdhLinePmParam param, uint32 threshold)
    {
    uint32 offset = DefaultOffset(self);
    eAtRet ret = SdhLinePmParamCheck(param, threshold);
    if (ret != cAtOk)
        return ret;

    switch ((uint32)param)
        {
        mWriteThreshold(CvS,    SdhLine, Section_Part0, sec_cv_s,    offset, threshold);
        mWriteThreshold(SefsS,  SdhLine, Section_Part1, sec_sefs_s,  offset, threshold);
        mWriteThreshold(EsS,    SdhLine, Section_Part1, sec_es_s,    offset, threshold);
        mWriteThreshold(SesS,   SdhLine, Section_Part1, sec_ses_s,   offset, threshold);
        mWriteThreshold(CvL,    SdhLine, Section_Part2, sec_cv_l,    offset, threshold);
        mWriteThreshold(EsL,    SdhLine, Section_Part3, sec_es_l,    offset, threshold);
        mWriteThreshold(SesL,   SdhLine, Section_Part3, sec_ses_l,   offset, threshold);
        mWriteThreshold(UasL,   SdhLine, Section_Part3, sec_uas_l,   offset, threshold);
        mWriteThreshold(CvLfe,  SdhLine, Section_Part4, sec_cv_lfe,  offset, threshold);
        mWriteThreshold(FcL,    SdhLine, Section_Part5, sec_fc_l,    offset, threshold);
        mWriteThreshold(EsLfe,  SdhLine, Section_Part5, sec_es_lfe,  offset, threshold);
        mWriteThreshold(SesLfe, SdhLine, Section_Part5, sec_ses_lfe, offset, threshold);
        mWriteThreshold(UasLfe, SdhLine, Section_Part6, sec_uas_lfe, offset, threshold);
        mWriteThreshold(FcLfe,  SdhLine, Section_Part6, sec_fc_lfe,  offset, threshold);
        default:
            return cAtErrorInvlParm;
        }

    return cAtErrorInvlParm;
    }

static uint32 SdhLinePeriodTcaThresholdGet(AtPmThresholdProfile self, eAtSurEngineSdhLinePmParam param)
    {
    uint32 offset = DefaultOffset(self);

    switch ((uint32)param)
        {
        mReadThreshold(CvS,    SdhLine, Section_Part0, sec_cv_s,    offset);
        mReadThreshold(SefsS,  SdhLine, Section_Part1, sec_sefs_s,  offset);
        mReadThreshold(EsS,    SdhLine, Section_Part1, sec_es_s,    offset);
        mReadThreshold(SesS,   SdhLine, Section_Part1, sec_ses_s,   offset);
        mReadThreshold(CvL,    SdhLine, Section_Part2, sec_cv_l,    offset);
        mReadThreshold(EsL,    SdhLine, Section_Part3, sec_es_l,    offset);
        mReadThreshold(SesL,   SdhLine, Section_Part3, sec_ses_l,   offset);
        mReadThreshold(UasL,   SdhLine, Section_Part3, sec_uas_l,   offset);
        mReadThreshold(CvLfe,  SdhLine, Section_Part4, sec_cv_lfe,  offset);
        mReadThreshold(FcL,    SdhLine, Section_Part5, sec_fc_l,    offset);
        mReadThreshold(EsLfe,  SdhLine, Section_Part5, sec_es_lfe,  offset);
        mReadThreshold(SesLfe, SdhLine, Section_Part5, sec_ses_lfe, offset);
        mReadThreshold(UasLfe, SdhLine, Section_Part6, sec_uas_lfe, offset);
        mReadThreshold(FcLfe,  SdhLine, Section_Part6, sec_fc_lfe,  offset);
        default:
            return 0x0;
        }

    return 0x0;
    }

static eBool SdhPathPmParamCheck(eAtSurEngineSdhPathPmParam param, uint32 threshold)
    {
    if (mOutOfRange(param, cAtSurEngineSdhPathPmParamPpjcPdet, cAtSurEngineSdhPathPmParamBberP))
        return cAtErrorOutOfRangParm;

    switch ((uint32)param)
        {
        case cAtSurEngineSdhPathPmParamPpjcPdet:
        case cAtSurEngineSdhPathPmParamNpjcPdet:
        case cAtSurEngineSdhPathPmParamPpjcPgen:
        case cAtSurEngineSdhPathPmParamNpjcPgen:
        case cAtSurEngineSdhPathPmParamPjcDiff:
            return mInRange(threshold, 0, cBit19_0) ? cAtOk : cAtErrorOutOfRangParm;

        case cAtSurEngineSdhPathPmParamPjcsPdet:
        case cAtSurEngineSdhPathPmParamPjcsPgen:
        case cAtSurEngineSdhPathPmParamEsP:
        case cAtSurEngineSdhPathPmParamSesP:
        case cAtSurEngineSdhPathPmParamUasP:
        case cAtSurEngineSdhPathPmParamFcP:
        case cAtSurEngineSdhPathPmParamEsPfe:
        case cAtSurEngineSdhPathPmParamSesPfe:
        case cAtSurEngineSdhPathPmParamUasPfe:
        case cAtSurEngineSdhPathPmParamFcPfe:
            return mInRange(threshold, 0, cBit9_0) ? cAtOk : cAtErrorOutOfRangParm;

        case cAtSurEngineSdhPathPmParamCvP:
        case cAtSurEngineSdhPathPmParamCvPfe:
            return mInRange(threshold, 0, cBit23_0) ? cAtOk : cAtErrorOutOfRangParm;

        default:
            return cAtErrorModeNotSupport;
        }
    }

static eAtRet SdhHoPathPeriodTcaThresholdSet(AtPmThresholdProfile self, eAtSurEngineSdhPathPmParam param, uint32 threshold)
    {
    uint32 offset = DefaultOffset(self);
    eAtRet ret = SdhPathPmParamCheck(param, threshold);
    if (ret != cAtOk)
        return ret;

    switch ((uint32)param)
        {
        mWriteThreshold(CvP,      SdhPath, HO_Part0,  sts_cv_p,      offset, threshold);
        mWriteThreshold(EsP,      SdhPath, HO_Part1,  sts_es_p,      offset, threshold);
        mWriteThreshold(SesP,     SdhPath, HO_Part1,  sts_ses_p,     offset, threshold);
        mWriteThreshold(UasP,     SdhPath, HO_Part1,  sts_uas_p,     offset, threshold);
        mWriteThreshold(PpjcPdet, SdhPath, HO_Part2,  sts_ppjc_pdet, offset, threshold);
        mWriteThreshold(NpjcPdet, SdhPath, HO_Part3,  sts_npjc_pdet, offset, threshold);
        mWriteThreshold(PpjcPgen, SdhPath, HO_Part4,  sts_ppjc_pgen, offset, threshold);
        mWriteThreshold(NpjcPgen, SdhPath, HO_Part5,  sts_npjc_pgen, offset, threshold);
        mWriteThreshold(PjcDiff,  SdhPath, HO_Part6,  sts_pjcdiff_p, offset, threshold);
        mWriteThreshold(PjcsPdet, SdhPath, HO_Part7,  sts_pjcs_pdet, offset, threshold);
        mWriteThreshold(PjcsPgen, SdhPath, HO_Part7,  sts_pjcs_pgen, offset, threshold);
        mWriteThreshold(CvPfe,    SdhPath, HO_Part8,  sts_cv_pfe,    offset, threshold);
        mWriteThreshold(EsPfe,    SdhPath, HO_Part9,  sts_es_pfe,    offset, threshold);
        mWriteThreshold(SesPfe,   SdhPath, HO_Part9,  sts_ses_pfe,   offset, threshold);
        mWriteThreshold(UasPfe,   SdhPath, HO_Part9,  sts_uas_pfe,   offset, threshold);
        mWriteThreshold(FcP,      SdhPath, HO_Part10, sts_fc_p,      offset, threshold);
        mWriteThreshold(FcPfe,    SdhPath, HO_Part10, sts_fc_pfe,    offset, threshold);
        default:
            return cAtErrorInvlParm;
        }

    return cAtErrorInvlParm;
    }

static uint32 SdhHoPathPeriodTcaThresholdGet(AtPmThresholdProfile self, eAtSurEngineSdhPathPmParam param)
    {
    uint32 offset = DefaultOffset(self);

    switch ((uint32)param)
        {
        mReadThreshold(CvP,      SdhPath, HO_Part0,  sts_cv_p,      offset);
        mReadThreshold(EsP,      SdhPath, HO_Part1,  sts_es_p,      offset);
        mReadThreshold(SesP,     SdhPath, HO_Part1,  sts_ses_p,     offset);
        mReadThreshold(UasP,     SdhPath, HO_Part1,  sts_uas_p,     offset);
        mReadThreshold(PpjcPdet, SdhPath, HO_Part2,  sts_ppjc_pdet, offset);
        mReadThreshold(NpjcPdet, SdhPath, HO_Part3,  sts_npjc_pdet, offset);
        mReadThreshold(PpjcPgen, SdhPath, HO_Part4,  sts_ppjc_pgen, offset);
        mReadThreshold(NpjcPgen, SdhPath, HO_Part5,  sts_npjc_pgen, offset);
        mReadThreshold(PjcDiff,  SdhPath, HO_Part6,  sts_pjcdiff_p, offset);
        mReadThreshold(PjcsPdet, SdhPath, HO_Part7,  sts_pjcs_pdet, offset);
        mReadThreshold(PjcsPgen, SdhPath, HO_Part7,  sts_pjcs_pgen, offset);
        mReadThreshold(CvPfe,    SdhPath, HO_Part8,  sts_cv_pfe,    offset);
        mReadThreshold(EsPfe,    SdhPath, HO_Part9,  sts_es_pfe,    offset);
        mReadThreshold(SesPfe,   SdhPath, HO_Part9,  sts_ses_pfe,   offset);
        mReadThreshold(UasPfe,   SdhPath, HO_Part9,  sts_uas_pfe,   offset);
        mReadThreshold(FcP,      SdhPath, HO_Part10, sts_fc_p,      offset);
        mReadThreshold(FcPfe,    SdhPath, HO_Part10, sts_fc_pfe,    offset);
        default:
            return 0x0;
        }

    return 0x0;
    }

static eAtRet SdhLoPathPeriodTcaThresholdSet(AtPmThresholdProfile self, eAtSurEngineSdhPathPmParam param, uint32 threshold)
    {
    uint32 offset = DefaultOffset(self);
    eAtRet ret = SdhPathPmParamCheck(param, threshold);
    if (ret != cAtOk)
        return ret;

    switch ((uint32)param)
        {
        mWriteThreshold(CvP,      SdhPath, LO_Part0,  vt_cv_v,      offset, threshold);
        mWriteThreshold(EsP,      SdhPath, LO_Part1,  vt_es_v,      offset, threshold);
        mWriteThreshold(SesP,     SdhPath, LO_Part1,  vt_ses_v,     offset, threshold);
        mWriteThreshold(UasP,     SdhPath, LO_Part1,  vt_uas_v,     offset, threshold);
        mWriteThreshold(PpjcPdet, SdhPath, LO_Part2,  vt_ppjc_vdet, offset, threshold);
        mWriteThreshold(NpjcPdet, SdhPath, LO_Part3,  vt_npjc_vdet, offset, threshold);
        mWriteThreshold(PpjcPgen, SdhPath, LO_Part4,  vt_ppjc_vgen, offset, threshold);
        mWriteThreshold(NpjcPgen, SdhPath, LO_Part5,  vt_npjc_vgen, offset, threshold);
        mWriteThreshold(PjcDiff,  SdhPath, LO_Part6,  vt_pjcdiff_v, offset, threshold);
        mWriteThreshold(PjcsPdet, SdhPath, LO_Part7,  vt_pjcs_vdet, offset, threshold);
        mWriteThreshold(PjcsPgen, SdhPath, LO_Part7,  vt_pjcs_vgen, offset, threshold);
        mWriteThreshold(CvPfe,    SdhPath, LO_Part8,  vt_cv_pfe,    offset, threshold);
        mWriteThreshold(EsPfe,    SdhPath, LO_Part9,  vt_es_vfe,    offset, threshold);
        mWriteThreshold(SesPfe,   SdhPath, LO_Part9,  vt_ses_vfe,   offset, threshold);
        mWriteThreshold(UasPfe,   SdhPath, LO_Part9,  vt_uas_vfe,   offset, threshold);
        mWriteThreshold(FcP,      SdhPath, LO_Part10, vt_fc_v,      offset, threshold);
        mWriteThreshold(FcPfe,    SdhPath, LO_Part10, vt_fc_vfe,    offset, threshold);
        default:
            return cAtErrorInvlParm;
        }

    return cAtErrorInvlParm;
    }

static uint32 SdhLoPathPeriodTcaThresholdGet(AtPmThresholdProfile self, eAtSurEngineSdhPathPmParam param)
    {
    uint32 offset = DefaultOffset(self);

    switch ((uint32)param)
        {
        mReadThreshold(CvP,      SdhPath, LO_Part0,  vt_cv_v,      offset);
        mReadThreshold(EsP,      SdhPath, LO_Part1,  vt_es_v,      offset);
        mReadThreshold(SesP,     SdhPath, LO_Part1,  vt_ses_v,     offset);
        mReadThreshold(UasP,     SdhPath, LO_Part1,  vt_uas_v,     offset);
        mReadThreshold(PpjcPdet, SdhPath, LO_Part2,  vt_ppjc_vdet, offset);
        mReadThreshold(NpjcPdet, SdhPath, LO_Part3,  vt_npjc_vdet, offset);
        mReadThreshold(PpjcPgen, SdhPath, LO_Part4,  vt_ppjc_vgen, offset);
        mReadThreshold(NpjcPgen, SdhPath, LO_Part5,  vt_npjc_vgen, offset);
        mReadThreshold(PjcDiff,  SdhPath, LO_Part6,  vt_pjcdiff_v, offset);
        mReadThreshold(PjcsPdet, SdhPath, LO_Part7,  vt_pjcs_vdet, offset);
        mReadThreshold(PjcsPgen, SdhPath, LO_Part7,  vt_pjcs_vgen, offset);
        mReadThreshold(CvPfe,    SdhPath, LO_Part8,  vt_cv_pfe,    offset);
        mReadThreshold(EsPfe,    SdhPath, LO_Part9,  vt_es_vfe,    offset);
        mReadThreshold(SesPfe,   SdhPath, LO_Part9,  vt_ses_vfe,   offset);
        mReadThreshold(UasPfe,   SdhPath, LO_Part9,  vt_uas_vfe,   offset);
        mReadThreshold(FcP,      SdhPath, LO_Part10, vt_fc_v,      offset);
        mReadThreshold(FcPfe,    SdhPath, LO_Part10, vt_fc_vfe,    offset);
        default:
            return 0x0;
        }
    return 0x0;
    }

static eAtRet PdhDe3PmParamCheck(eAtSurEnginePdhDe3PmParam param, uint32 threshold)
    {
    if (mOutOfRange(param, cAtSurEnginePdhDe3PmParamCvL, cAtSurEnginePdhDe3PmParamFccpPfe))
        return cAtErrorOutOfRangParm;

    switch ((uint32)param)
        {
        case cAtSurEnginePdhDe3PmParamEsL:
        case cAtSurEnginePdhDe3PmParamSesL:
        case cAtSurEnginePdhDe3PmParamLossL:
        case cAtSurEnginePdhDe3PmParamSespP:
        case cAtSurEnginePdhDe3PmParamSescpP:
        case cAtSurEnginePdhDe3PmParamSasP:
        case cAtSurEnginePdhDe3PmParamAissP:
        case cAtSurEnginePdhDe3PmParamUaspP:
        case cAtSurEnginePdhDe3PmParamUascpP:
        case cAtSurEnginePdhDe3PmParamEscpPfe:
        case cAtSurEnginePdhDe3PmParamSescpPfe:
        case cAtSurEnginePdhDe3PmParamSascpPfe:
        case cAtSurEnginePdhDe3PmParamUascpPfe:
        case cAtSurEnginePdhDe3PmParamEsbcpPfe:
        case cAtSurEnginePdhDe3PmParamEsaL:
        case cAtSurEnginePdhDe3PmParamEsbL:
        case cAtSurEnginePdhDe3PmParamEsacpP:
        case cAtSurEnginePdhDe3PmParamEsbpP:
        case cAtSurEnginePdhDe3PmParamEsbcpP:
        case cAtSurEnginePdhDe3PmParamFcP:
        case cAtSurEnginePdhDe3PmParamEsacpPfe:
        case cAtSurEnginePdhDe3PmParamFccpPfe:
        case cAtSurEnginePdhDe3PmParamEspP:
        case cAtSurEnginePdhDe3PmParamEscpP:
        case cAtSurEnginePdhDe3PmParamEsapP:
            return mInRange(threshold, 0, cBit9_0) ? cAtOk : cAtErrorOutOfRangParm;

        case cAtSurEnginePdhDe3PmParamCvL:
        case cAtSurEnginePdhDe3PmParamCvpP:
        case cAtSurEnginePdhDe3PmParamCvcpP:
        case cAtSurEnginePdhDe3PmParamCvcpPfe:
            return mInRange(threshold, 0, cBit23_0) ? cAtOk : cAtErrorOutOfRangParm;

        default:
            return cAtErrorModeNotSupport;
        }
    }

static eAtRet PdhDe3PeriodTcaThresholdSet(AtPmThresholdProfile self, eAtSurEnginePdhDe3PmParam param, uint32 threshold)
    {
    uint32 offset = DefaultOffset(self);
    eAtRet ret = PdhDe3PmParamCheck(param, threshold);
    if (ret != cAtOk)
        return ret;

    switch ((uint32)param)
        {
        mWriteThreshold(CvL,      PdhDe3, DS3_Part0,  ds3_cv_l,      offset, threshold);
        mWriteThreshold(EsL,      PdhDe3, DS3_Part1,  ds3_es_l,      offset, threshold);
        mWriteThreshold(EsaL,     PdhDe3, DS3_Part1,  ds3_esa_l,     offset, threshold);
        mWriteThreshold(EsbL,     PdhDe3, DS3_Part1,  ds3_esb_l,     offset, threshold);
        mWriteThreshold(SesL,     PdhDe3, DS3_Part2,  ds3_ses_l,     offset, threshold);
        mWriteThreshold(LossL,    PdhDe3, DS3_Part2,  ds3_loss_l,    offset, threshold);
        mWriteThreshold(CvpP,     PdhDe3, DS3_Part3,  ds3_cvp_p,     offset, threshold);
        mWriteThreshold(CvcpP,    PdhDe3, DS3_Part4,  ds3_cvcp_p,    offset, threshold);
        mWriteThreshold(EspP,     PdhDe3, DS3_Part5,  ds3_esp_p,     offset, threshold);
        mWriteThreshold(EscpP,    PdhDe3, DS3_Part5,  ds3_escp_p,    offset, threshold);
        mWriteThreshold(EsapP,    PdhDe3, DS3_Part5,  ds3_esacp_p,   offset, threshold);
        mWriteThreshold(EsacpP,   PdhDe3, DS3_Part6,  ds3_esacp_p,   offset, threshold);
        mWriteThreshold(EsbpP,    PdhDe3, DS3_Part6,  ds3_esbp_p,     offset, threshold);
        mWriteThreshold(EsbcpP,   PdhDe3, DS3_Part6,  ds3_esbcp_p,   offset, threshold);
        mWriteThreshold(SespP,    PdhDe3, DS3_Part7,  ds3_sesp_p,    offset, threshold);
        mWriteThreshold(SescpP,   PdhDe3, DS3_Part7,  ds3_sescp_p,   offset, threshold);
        mWriteThreshold(SasP,     PdhDe3, DS3_Part7,  ds3_sas_p,     offset, threshold);
        mWriteThreshold(AissP,    PdhDe3, DS3_Part8,  ds3_aiss_p,    offset, threshold);
        mWriteThreshold(UaspP,    PdhDe3, DS3_Part8,  ds3_uasp_p,    offset, threshold);
        mWriteThreshold(UascpP,   PdhDe3, DS3_Part8,  ds3_uascp_p,   offset, threshold);
        mWriteThreshold(CvcpPfe,  PdhDe3, DS3_Part9,  ds3_cvcp_pfe,  offset, threshold);
        mWriteThreshold(EscpPfe,  PdhDe3, DS3_Part10, ds3_escp_pfe,  offset, threshold);
        mWriteThreshold(EsacpPfe, PdhDe3, DS3_Part10, ds3_esacp_pfe, offset, threshold);
        mWriteThreshold(EsbcpPfe, PdhDe3, DS3_Part10, ds3_esbcp_pfe, offset, threshold);
        mWriteThreshold(SescpPfe, PdhDe3, DS3_Part11, ds3_sescp_pfe, offset, threshold);
        mWriteThreshold(SascpPfe, PdhDe3, DS3_Part11, ds3_sascp_pfe, offset, threshold);
        mWriteThreshold(UascpPfe, PdhDe3, DS3_Part11, ds3_uascp_pfe, offset, threshold);
        mWriteThreshold(FcP,      PdhDe3, DS3_Part12, ds3_fc_p,      offset, threshold);
        mWriteThreshold(FccpPfe,  PdhDe3, DS3_Part12, ds3_fccp_pfe,  offset, threshold);
        default:
            return cAtErrorInvlParm;
        }

    return cAtErrorInvlParm;
    }

static uint32 PdhDe3PeriodTcaThresholdGet(AtPmThresholdProfile self, eAtSurEnginePdhDe3PmParam param)
    {
    uint32 offset = DefaultOffset(self);

    switch ((uint32)param)
        {
        mReadThreshold(CvL,      PdhDe3, DS3_Part0,  ds3_cv_l,      offset);
        mReadThreshold(EsL,      PdhDe3, DS3_Part1,  ds3_es_l,      offset);
        mReadThreshold(EsaL,     PdhDe3, DS3_Part1,  ds3_esa_l,     offset);
        mReadThreshold(EsbL,     PdhDe3, DS3_Part1,  ds3_esb_l,     offset);
        mReadThreshold(SesL,     PdhDe3, DS3_Part2,  ds3_ses_l,     offset);
        mReadThreshold(LossL,    PdhDe3, DS3_Part2,  ds3_loss_l,    offset);
        mReadThreshold(CvpP,     PdhDe3, DS3_Part3,  ds3_cvp_p,     offset);
        mReadThreshold(CvcpP,    PdhDe3, DS3_Part4,  ds3_cvcp_p,    offset);
        mReadThreshold(EspP,     PdhDe3, DS3_Part5,  ds3_esp_p,     offset);
        mReadThreshold(EscpP,    PdhDe3, DS3_Part5,  ds3_escp_p,    offset);
        mReadThreshold(EsapP,    PdhDe3, DS3_Part5,  ds3_esacp_p,   offset);
        mReadThreshold(EsacpP,   PdhDe3, DS3_Part6,  ds3_esacp_p,   offset);
        mReadThreshold(EsbpP,    PdhDe3, DS3_Part6,  ds3_esbp_p,     offset);
        mReadThreshold(EsbcpP,   PdhDe3, DS3_Part6,  ds3_esbcp_p,   offset);
        mReadThreshold(SespP,    PdhDe3, DS3_Part7,  ds3_sesp_p,    offset);
        mReadThreshold(SescpP,   PdhDe3, DS3_Part7,  ds3_sescp_p,   offset);
        mReadThreshold(SasP,     PdhDe3, DS3_Part7,  ds3_sas_p,     offset);
        mReadThreshold(AissP,    PdhDe3, DS3_Part8,  ds3_aiss_p,    offset);
        mReadThreshold(UaspP,    PdhDe3, DS3_Part8,  ds3_uasp_p,    offset);
        mReadThreshold(UascpP,   PdhDe3, DS3_Part8,  ds3_uascp_p,   offset);
        mReadThreshold(CvcpPfe,  PdhDe3, DS3_Part9,  ds3_cvcp_pfe,  offset);
        mReadThreshold(EscpPfe,  PdhDe3, DS3_Part10, ds3_escp_pfe,  offset);
        mReadThreshold(EsacpPfe, PdhDe3, DS3_Part10, ds3_esacp_pfe, offset);
        mReadThreshold(EsbcpPfe, PdhDe3, DS3_Part10, ds3_esbcp_pfe, offset);
        mReadThreshold(SescpPfe, PdhDe3, DS3_Part11, ds3_sescp_pfe, offset);
        mReadThreshold(SascpPfe, PdhDe3, DS3_Part11, ds3_sascp_pfe, offset);
        mReadThreshold(UascpPfe, PdhDe3, DS3_Part11, ds3_uascp_pfe, offset);
        mReadThreshold(FcP,      PdhDe3, DS3_Part12, ds3_fc_p,      offset);
        mReadThreshold(FccpPfe,  PdhDe3, DS3_Part12, ds3_fccp_pfe,  offset);
        default:
            return 0x0;
        }

    return 0x0;
    }

static eAtRet PdhDe1PmParamCheck(eAtSurEnginePdhDe1PmParam param, uint32 threshold)
    {
    if (mOutOfRange(param, cAtSurEnginePdhDe1PmParamCvL, cAtSurEnginePdhDe1PmParamFcP))
        return cAtErrorOutOfRangParm;

    switch ((uint32)param)
        {
        case cAtSurEnginePdhDe1PmParamEsL:
        case cAtSurEnginePdhDe1PmParamSesL:
        case cAtSurEnginePdhDe1PmParamLossL:
        case cAtSurEnginePdhDe1PmParamEsLfe:
        case cAtSurEnginePdhDe1PmParamEsP:
        case cAtSurEnginePdhDe1PmParamSesP:
        case cAtSurEnginePdhDe1PmParamAissP:
        case cAtSurEnginePdhDe1PmParamSasP:
        case cAtSurEnginePdhDe1PmParamCssP:
        case cAtSurEnginePdhDe1PmParamUasP:
        case cAtSurEnginePdhDe1PmParamSefsPfe:
        case cAtSurEnginePdhDe1PmParamEsPfe:
        case cAtSurEnginePdhDe1PmParamSesPfe:
        case cAtSurEnginePdhDe1PmParamCssPfe:
        case cAtSurEnginePdhDe1PmParamFcP:
        case cAtSurEnginePdhDe1PmParamUasPfe:
        case cAtSurEnginePdhDe1PmParamFcPfe:
            return mInRange(threshold, 0, cBit9_0) ? cAtOk : cAtErrorOutOfRangParm;

        case cAtSurEnginePdhDe1PmParamCvL:
        case cAtSurEnginePdhDe1PmParamCvP:
            return mInRange(threshold, 0, cBit23_0) ? cAtOk : cAtErrorOutOfRangParm;

        default:
            return cAtErrorModeNotSupport;
        }
    }

static eAtRet PdhDe1PeriodTcaThresholdSet(AtPmThresholdProfile self, eAtSurEnginePdhDe1PmParam param, uint32 threshold)
    {
    uint32 offset = DefaultOffset(self);
    eAtRet ret = PdhDe1PmParamCheck(param, threshold);
    if (ret != cAtOk)
        return ret;

    switch ((uint32)param)
        {
        mWriteThreshold(CvL,     PdhDe1, DS1_Part0, ds1_cv_l,     offset, threshold);
        mWriteThreshold(EsL,     PdhDe1, DS1_Part1, ds1es_l,      offset, threshold);
        mWriteThreshold(SesL,    PdhDe1, DS1_Part1, ds1ses_l,     offset, threshold);
        mWriteThreshold(LossL,   PdhDe1, DS1_Part1, ds1loss_l,    offset, threshold);
        mWriteThreshold(CvP,     PdhDe1, DS1_Part2, ds1_cv_p,     offset, threshold);
        mWriteThreshold(EsP,     PdhDe1, DS1_Part3, ds1_es_p,     offset, threshold);
        mWriteThreshold(SesP,    PdhDe1, DS1_Part3, ds1_ses_p,    offset, threshold);
        mWriteThreshold(AissP,   PdhDe1, DS1_Part3, ds1_aiss_p,   offset, threshold);
        mWriteThreshold(SasP,    PdhDe1, DS1_Part4, ds1_sas_p,    offset, threshold);
        mWriteThreshold(CssP,    PdhDe1, DS1_Part4, ds1_css_p,    offset, threshold);
        mWriteThreshold(UasP,    PdhDe1, DS1_Part4, ds1_uas_p,    offset, threshold);
        mWriteThreshold(FcP,     PdhDe1, DS1_Part5, ds1_fc_p,     offset, threshold);
        mWriteThreshold(EsLfe,   PdhDe1, DS1_Part5, ds1_es_lfe,   offset, threshold);
        mWriteThreshold(SefsPfe, PdhDe1, DS1_Part5, ds1_sefs_pfe, offset, threshold);
        mWriteThreshold(EsPfe,   PdhDe1, DS1_Part6, ds1_es_pfe,   offset, threshold);
        mWriteThreshold(SesPfe,  PdhDe1, DS1_Part6, ds1_ses_pfe,  offset, threshold);
        mWriteThreshold(CssPfe,  PdhDe1, DS1_Part6, ds1_css_pfe,  offset, threshold);
        mWriteThreshold(UasPfe,  PdhDe1, DS1_Part7, ds1_uas_pfe,  offset, threshold);
        mWriteThreshold(FcPfe,   PdhDe1, DS1_Part7, ds1_fc_pfe,   offset, threshold);
        default:
            return cAtErrorInvlParm;
        }

    return cAtErrorInvlParm;
    }

static uint32 PdhDe1PeriodTcaThresholdGet(AtPmThresholdProfile self, eAtSurEnginePdhDe1PmParam param)
    {
    uint32 offset = DefaultOffset(self);

    switch ((uint32)param)
        {
        mReadThreshold(CvL,     PdhDe1, DS1_Part0, ds1_cv_l,     offset);
        mReadThreshold(EsL,     PdhDe1, DS1_Part1, ds1es_l,      offset);
        mReadThreshold(SesL,    PdhDe1, DS1_Part1, ds1ses_l,     offset);
        mReadThreshold(LossL,   PdhDe1, DS1_Part1, ds1loss_l,    offset);
        mReadThreshold(CvP,     PdhDe1, DS1_Part2, ds1_cv_p,     offset);
        mReadThreshold(EsP,     PdhDe1, DS1_Part3, ds1_es_p,     offset);
        mReadThreshold(SesP,    PdhDe1, DS1_Part3, ds1_ses_p,    offset);
        mReadThreshold(AissP,   PdhDe1, DS1_Part3, ds1_aiss_p,   offset);
        mReadThreshold(SasP,    PdhDe1, DS1_Part4, ds1_sas_p,    offset);
        mReadThreshold(CssP,    PdhDe1, DS1_Part4, ds1_css_p,    offset);
        mReadThreshold(UasP,    PdhDe1, DS1_Part4, ds1_uas_p,    offset);
        mReadThreshold(FcP,     PdhDe1, DS1_Part5, ds1_fc_p,     offset);
        mReadThreshold(EsLfe,   PdhDe1, DS1_Part5, ds1_es_lfe,   offset);
        mReadThreshold(SefsPfe, PdhDe1, DS1_Part5, ds1_sefs_pfe, offset);
        mReadThreshold(EsPfe,   PdhDe1, DS1_Part6, ds1_es_pfe,   offset);
        mReadThreshold(SesPfe,  PdhDe1, DS1_Part6, ds1_ses_pfe,  offset);
        mReadThreshold(CssPfe,  PdhDe1, DS1_Part6, ds1_css_pfe,  offset);
        mReadThreshold(UasPfe,  PdhDe1, DS1_Part7, ds1_uas_pfe,  offset);
        mReadThreshold(FcPfe,   PdhDe1, DS1_Part7, ds1_fc_pfe,   offset);
        default:
            return 0x0;
        }

    return 0x0;
    }

static eAtRet PwPmParamCheck(eAtSurEnginePwPmParam param, uint32 threshold)
    {
    if (mOutOfRange(param, cAtSurEnginePwPmParamEs, cAtSurEnginePwPmParamFc))
        return cAtErrorOutOfRangParm;

    return mInRange(threshold, 0, cBit9_0) ? cAtOk  : cAtErrorOutOfRangParm;
    }

static eAtRet PwPeriodTcaThresholdSet(AtPmThresholdProfile self, eAtSurEnginePwPmParam param, uint32 threshold)
    {
    uint32 offset = DefaultOffset(self);
    eAtRet ret = PwPmParamCheck(param, threshold);
    if (ret != cAtOk)
        return ret;

    switch ((uint32)param)
        {
        mWriteThreshold(Es,    Pw, PW_Part0, pw_es,     offset, threshold);
        mWriteThreshold(Ses,   Pw, PW_Part0, pw_ses,    offset, threshold);
        mWriteThreshold(Uas,   Pw, PW_Part0, pw_uas,    offset, threshold);
        mWriteThreshold(FeEs,  Pw, PW_Part1, pw_es_fe,  offset, threshold);
        mWriteThreshold(FeSes, Pw, PW_Part1, pw_ses_fe, offset, threshold);
        mWriteThreshold(FeUas, Pw, PW_Part1, pw_uas_fe, offset, threshold);
        mWriteThreshold(Fc,    Pw, PW_Part2, pw_fc,     offset, threshold);
        default:
            return cAtErrorInvlParm;
        }

    return cAtErrorInvlParm;
    }

static uint32 PwPeriodTcaThresholdGet(AtPmThresholdProfile self, eAtSurEnginePwPmParam param)
    {
    uint32 offset = DefaultOffset(self);

    switch ((uint32)param)
        {
        mReadThreshold(Es,    Pw, PW_Part0, pw_es,     offset);
        mReadThreshold(Ses,   Pw, PW_Part0, pw_ses,    offset);
        mReadThreshold(Uas,   Pw, PW_Part0, pw_uas,    offset);
        mReadThreshold(FeEs,  Pw, PW_Part1, pw_es_fe,  offset);
        mReadThreshold(FeSes, Pw, PW_Part1, pw_ses_fe, offset);
        mReadThreshold(FeUas, Pw, PW_Part1, pw_uas_fe, offset);
        mReadThreshold(Fc,    Pw, PW_Part2, pw_fc,     offset);
        default:
            return 0x0;
        }

    return 0x0;
    }

static eAtRet Init(AtPmThresholdProfile self)
    {
    eAtRet ret = cAtOk;
    ret |= AtPmThresholdProfileSdhLinePeriodTcaThresholdSet(self, cAtSurEngineSdhLinePmParamSefsS , cBit9_0);
    ret |= AtPmThresholdProfileSdhLinePeriodTcaThresholdSet(self, cAtSurEngineSdhLinePmParamCvS   , cBit23_0);
    ret |= AtPmThresholdProfileSdhLinePeriodTcaThresholdSet(self, cAtSurEngineSdhLinePmParamEsS   , cBit9_0);
    ret |= AtPmThresholdProfileSdhLinePeriodTcaThresholdSet(self, cAtSurEngineSdhLinePmParamSesS  , cBit9_0);
    ret |= AtPmThresholdProfileSdhLinePeriodTcaThresholdSet(self, cAtSurEngineSdhLinePmParamCvL   , cBit23_0);
    ret |= AtPmThresholdProfileSdhLinePeriodTcaThresholdSet(self, cAtSurEngineSdhLinePmParamEsL   , cBit9_0);
    ret |= AtPmThresholdProfileSdhLinePeriodTcaThresholdSet(self, cAtSurEngineSdhLinePmParamSesL  , cBit9_0);
    ret |= AtPmThresholdProfileSdhLinePeriodTcaThresholdSet(self, cAtSurEngineSdhLinePmParamUasL  , cBit9_0);
    ret |= AtPmThresholdProfileSdhLinePeriodTcaThresholdSet(self, cAtSurEngineSdhLinePmParamFcL   , cBit9_0);
    ret |= AtPmThresholdProfileSdhLinePeriodTcaThresholdSet(self, cAtSurEngineSdhLinePmParamCvLfe , cBit23_0);
    ret |= AtPmThresholdProfileSdhLinePeriodTcaThresholdSet(self, cAtSurEngineSdhLinePmParamEsLfe , cBit9_0);
    ret |= AtPmThresholdProfileSdhLinePeriodTcaThresholdSet(self, cAtSurEngineSdhLinePmParamSesLfe, cBit9_0);
    ret |= AtPmThresholdProfileSdhLinePeriodTcaThresholdSet(self, cAtSurEngineSdhLinePmParamUasLfe, cBit9_0);
    ret |= AtPmThresholdProfileSdhLinePeriodTcaThresholdSet(self, cAtSurEngineSdhLinePmParamFcLfe , cBit9_0);

    ret |= AtPmThresholdProfileSdhHoPathPeriodTcaThresholdSet(self, cAtSurEngineSdhPathPmParamPpjcPdet, cBit19_0);
    ret |= AtPmThresholdProfileSdhHoPathPeriodTcaThresholdSet(self, cAtSurEngineSdhPathPmParamNpjcPdet, cBit19_0);
    ret |= AtPmThresholdProfileSdhHoPathPeriodTcaThresholdSet(self, cAtSurEngineSdhPathPmParamPpjcPgen, cBit19_0);
    ret |= AtPmThresholdProfileSdhHoPathPeriodTcaThresholdSet(self, cAtSurEngineSdhPathPmParamNpjcPgen, cBit19_0);
    ret |= AtPmThresholdProfileSdhHoPathPeriodTcaThresholdSet(self, cAtSurEngineSdhPathPmParamPjcDiff , cBit19_0);
    ret |= AtPmThresholdProfileSdhHoPathPeriodTcaThresholdSet(self, cAtSurEngineSdhPathPmParamPjcsPdet, cBit9_0);
    ret |= AtPmThresholdProfileSdhHoPathPeriodTcaThresholdSet(self, cAtSurEngineSdhPathPmParamPjcsPgen, cBit9_0);
    ret |= AtPmThresholdProfileSdhHoPathPeriodTcaThresholdSet(self, cAtSurEngineSdhPathPmParamCvP     , cBit23_0);
    ret |= AtPmThresholdProfileSdhHoPathPeriodTcaThresholdSet(self, cAtSurEngineSdhPathPmParamEsP     , cBit9_0);
    ret |= AtPmThresholdProfileSdhHoPathPeriodTcaThresholdSet(self, cAtSurEngineSdhPathPmParamSesP    , cBit9_0);
    ret |= AtPmThresholdProfileSdhHoPathPeriodTcaThresholdSet(self, cAtSurEngineSdhPathPmParamUasP    , cBit9_0);
    ret |= AtPmThresholdProfileSdhHoPathPeriodTcaThresholdSet(self, cAtSurEngineSdhPathPmParamFcP     , cBit9_0);
    ret |= AtPmThresholdProfileSdhHoPathPeriodTcaThresholdSet(self, cAtSurEngineSdhPathPmParamCvPfe   , cBit23_0);
    ret |= AtPmThresholdProfileSdhHoPathPeriodTcaThresholdSet(self, cAtSurEngineSdhPathPmParamEsPfe   , cBit9_0);
    ret |= AtPmThresholdProfileSdhHoPathPeriodTcaThresholdSet(self, cAtSurEngineSdhPathPmParamSesPfe  , cBit9_0);
    ret |= AtPmThresholdProfileSdhHoPathPeriodTcaThresholdSet(self, cAtSurEngineSdhPathPmParamUasPfe  , cBit9_0);
    ret |= AtPmThresholdProfileSdhHoPathPeriodTcaThresholdSet(self, cAtSurEngineSdhPathPmParamFcPfe   , cBit9_0);

    ret |= AtPmThresholdProfileSdhLoPathPeriodTcaThresholdSet(self, cAtSurEngineSdhPathPmParamPpjcPdet, cBit19_0);
    ret |= AtPmThresholdProfileSdhLoPathPeriodTcaThresholdSet(self, cAtSurEngineSdhPathPmParamNpjcPdet, cBit19_0);
    ret |= AtPmThresholdProfileSdhLoPathPeriodTcaThresholdSet(self, cAtSurEngineSdhPathPmParamPpjcPgen, cBit19_0);
    ret |= AtPmThresholdProfileSdhLoPathPeriodTcaThresholdSet(self, cAtSurEngineSdhPathPmParamNpjcPgen, cBit19_0);
    ret |= AtPmThresholdProfileSdhLoPathPeriodTcaThresholdSet(self, cAtSurEngineSdhPathPmParamPjcDiff , cBit19_0);
    ret |= AtPmThresholdProfileSdhLoPathPeriodTcaThresholdSet(self, cAtSurEngineSdhPathPmParamPjcsPdet, cBit9_0);
    ret |= AtPmThresholdProfileSdhLoPathPeriodTcaThresholdSet(self, cAtSurEngineSdhPathPmParamPjcsPgen, cBit9_0);
    ret |= AtPmThresholdProfileSdhLoPathPeriodTcaThresholdSet(self, cAtSurEngineSdhPathPmParamCvP     , cBit23_0);
    ret |= AtPmThresholdProfileSdhLoPathPeriodTcaThresholdSet(self, cAtSurEngineSdhPathPmParamEsP     , cBit9_0);
    ret |= AtPmThresholdProfileSdhLoPathPeriodTcaThresholdSet(self, cAtSurEngineSdhPathPmParamSesP    , cBit9_0);
    ret |= AtPmThresholdProfileSdhLoPathPeriodTcaThresholdSet(self, cAtSurEngineSdhPathPmParamUasP    , cBit9_0);
    ret |= AtPmThresholdProfileSdhLoPathPeriodTcaThresholdSet(self, cAtSurEngineSdhPathPmParamFcP     , cBit9_0);
    ret |= AtPmThresholdProfileSdhLoPathPeriodTcaThresholdSet(self, cAtSurEngineSdhPathPmParamCvPfe   , cBit23_0);
    ret |= AtPmThresholdProfileSdhLoPathPeriodTcaThresholdSet(self, cAtSurEngineSdhPathPmParamEsPfe   , cBit9_0);
    ret |= AtPmThresholdProfileSdhLoPathPeriodTcaThresholdSet(self, cAtSurEngineSdhPathPmParamSesPfe  , cBit9_0);
    ret |= AtPmThresholdProfileSdhLoPathPeriodTcaThresholdSet(self, cAtSurEngineSdhPathPmParamUasPfe  , cBit9_0);
    ret |= AtPmThresholdProfileSdhLoPathPeriodTcaThresholdSet(self, cAtSurEngineSdhPathPmParamFcPfe   , cBit9_0);

    ret |= AtPmThresholdProfilePdhDe3PeriodTcaThresholdSet(self, cAtSurEnginePdhDe3PmParamCvL     , cBit23_0);
    ret |= AtPmThresholdProfilePdhDe3PeriodTcaThresholdSet(self, cAtSurEnginePdhDe3PmParamEsL     , cBit9_0);
    ret |= AtPmThresholdProfilePdhDe3PeriodTcaThresholdSet(self, cAtSurEnginePdhDe3PmParamSesL    , cBit9_0);
    ret |= AtPmThresholdProfilePdhDe3PeriodTcaThresholdSet(self, cAtSurEnginePdhDe3PmParamLossL   , cBit9_0);
    ret |= AtPmThresholdProfilePdhDe3PeriodTcaThresholdSet(self, cAtSurEnginePdhDe3PmParamCvpP    , cBit23_0);
    ret |= AtPmThresholdProfilePdhDe3PeriodTcaThresholdSet(self, cAtSurEnginePdhDe3PmParamCvcpP   , cBit23_0);
    ret |= AtPmThresholdProfilePdhDe3PeriodTcaThresholdSet(self, cAtSurEnginePdhDe3PmParamEspP    , cBit9_0);
    ret |= AtPmThresholdProfilePdhDe3PeriodTcaThresholdSet(self, cAtSurEnginePdhDe3PmParamEscpP   , cBit9_0);
    ret |= AtPmThresholdProfilePdhDe3PeriodTcaThresholdSet(self, cAtSurEnginePdhDe3PmParamSespP   , cBit9_0);
    ret |= AtPmThresholdProfilePdhDe3PeriodTcaThresholdSet(self, cAtSurEnginePdhDe3PmParamSescpP  , cBit9_0);
    ret |= AtPmThresholdProfilePdhDe3PeriodTcaThresholdSet(self, cAtSurEnginePdhDe3PmParamSasP    , cBit9_0);
    ret |= AtPmThresholdProfilePdhDe3PeriodTcaThresholdSet(self, cAtSurEnginePdhDe3PmParamAissP   , cBit9_0);
    ret |= AtPmThresholdProfilePdhDe3PeriodTcaThresholdSet(self, cAtSurEnginePdhDe3PmParamUaspP   , cBit9_0);
    ret |= AtPmThresholdProfilePdhDe3PeriodTcaThresholdSet(self, cAtSurEnginePdhDe3PmParamUascpP  , cBit9_0);
    ret |= AtPmThresholdProfilePdhDe3PeriodTcaThresholdSet(self, cAtSurEnginePdhDe3PmParamCvcpPfe , cBit23_0);
    ret |= AtPmThresholdProfilePdhDe3PeriodTcaThresholdSet(self, cAtSurEnginePdhDe3PmParamEscpPfe , cBit9_0);
    ret |= AtPmThresholdProfilePdhDe3PeriodTcaThresholdSet(self, cAtSurEnginePdhDe3PmParamSescpPfe, cBit9_0);
    ret |= AtPmThresholdProfilePdhDe3PeriodTcaThresholdSet(self, cAtSurEnginePdhDe3PmParamSascpPfe, cBit9_0);
    ret |= AtPmThresholdProfilePdhDe3PeriodTcaThresholdSet(self, cAtSurEnginePdhDe3PmParamUascpPfe, cBit9_0);
    ret |= AtPmThresholdProfilePdhDe3PeriodTcaThresholdSet(self, cAtSurEnginePdhDe3PmParamEsbcpPfe, cBit9_0);
    ret |= AtPmThresholdProfilePdhDe3PeriodTcaThresholdSet(self, cAtSurEnginePdhDe3PmParamEsaL    , cBit9_0);
    ret |= AtPmThresholdProfilePdhDe3PeriodTcaThresholdSet(self, cAtSurEnginePdhDe3PmParamEsbL    , cBit9_0);
    ret |= AtPmThresholdProfilePdhDe3PeriodTcaThresholdSet(self, cAtSurEnginePdhDe3PmParamEsapP   , cBit9_0);
    ret |= AtPmThresholdProfilePdhDe3PeriodTcaThresholdSet(self, cAtSurEnginePdhDe3PmParamEsacpP  , cBit9_0);
    ret |= AtPmThresholdProfilePdhDe3PeriodTcaThresholdSet(self, cAtSurEnginePdhDe3PmParamEsbpP   , cBit9_0);
    ret |= AtPmThresholdProfilePdhDe3PeriodTcaThresholdSet(self, cAtSurEnginePdhDe3PmParamEsbcpP  , cBit9_0);
    ret |= AtPmThresholdProfilePdhDe3PeriodTcaThresholdSet(self, cAtSurEnginePdhDe3PmParamFcP     , cBit9_0);
    ret |= AtPmThresholdProfilePdhDe3PeriodTcaThresholdSet(self, cAtSurEnginePdhDe3PmParamEsacpPfe, cBit9_0);
    ret |= AtPmThresholdProfilePdhDe3PeriodTcaThresholdSet(self, cAtSurEnginePdhDe3PmParamFccpPfe , cBit9_0);

    ret |= AtPmThresholdProfilePdhDe1PeriodTcaThresholdSet(self, cAtSurEnginePdhDe1PmParamCvL    , cBit23_0);
    ret |= AtPmThresholdProfilePdhDe1PeriodTcaThresholdSet(self, cAtSurEnginePdhDe1PmParamEsL    , cBit9_0);
    ret |= AtPmThresholdProfilePdhDe1PeriodTcaThresholdSet(self, cAtSurEnginePdhDe1PmParamSesL   , cBit9_0);
    ret |= AtPmThresholdProfilePdhDe1PeriodTcaThresholdSet(self, cAtSurEnginePdhDe1PmParamLossL  , cBit9_0);
    ret |= AtPmThresholdProfilePdhDe1PeriodTcaThresholdSet(self, cAtSurEnginePdhDe1PmParamEsLfe  , cBit9_0);
    ret |= AtPmThresholdProfilePdhDe1PeriodTcaThresholdSet(self, cAtSurEnginePdhDe1PmParamCvP    , cBit23_0);
    ret |= AtPmThresholdProfilePdhDe1PeriodTcaThresholdSet(self, cAtSurEnginePdhDe1PmParamEsP    , cBit9_0);
    ret |= AtPmThresholdProfilePdhDe1PeriodTcaThresholdSet(self, cAtSurEnginePdhDe1PmParamSesP   , cBit9_0);
    ret |= AtPmThresholdProfilePdhDe1PeriodTcaThresholdSet(self, cAtSurEnginePdhDe1PmParamAissP  , cBit9_0);
    ret |= AtPmThresholdProfilePdhDe1PeriodTcaThresholdSet(self, cAtSurEnginePdhDe1PmParamSasP   , cBit9_0);
    ret |= AtPmThresholdProfilePdhDe1PeriodTcaThresholdSet(self, cAtSurEnginePdhDe1PmParamCssP   , cBit9_0);
    ret |= AtPmThresholdProfilePdhDe1PeriodTcaThresholdSet(self, cAtSurEnginePdhDe1PmParamUasP   , cBit9_0);
    ret |= AtPmThresholdProfilePdhDe1PeriodTcaThresholdSet(self, cAtSurEnginePdhDe1PmParamSefsPfe, cBit9_0);
    ret |= AtPmThresholdProfilePdhDe1PeriodTcaThresholdSet(self, cAtSurEnginePdhDe1PmParamEsPfe  , cBit9_0);
    ret |= AtPmThresholdProfilePdhDe1PeriodTcaThresholdSet(self, cAtSurEnginePdhDe1PmParamSesPfe , cBit9_0);
    ret |= AtPmThresholdProfilePdhDe1PeriodTcaThresholdSet(self, cAtSurEnginePdhDe1PmParamCssPfe , cBit9_0);
    ret |= AtPmThresholdProfilePdhDe1PeriodTcaThresholdSet(self, cAtSurEnginePdhDe1PmParamFcP    , cBit9_0);

    ret |= AtPmThresholdProfilePwPeriodTcaThresholdSet(self, cAtSurEnginePwPmParamEs   , cBit9_0);
    ret |= AtPmThresholdProfilePwPeriodTcaThresholdSet(self, cAtSurEnginePwPmParamSes  , cBit9_0);
    ret |= AtPmThresholdProfilePwPeriodTcaThresholdSet(self, cAtSurEnginePwPmParamUas  , cBit9_0);
    ret |= AtPmThresholdProfilePwPeriodTcaThresholdSet(self, cAtSurEnginePwPmParamFeEs , cBit9_0);
    ret |= AtPmThresholdProfilePwPeriodTcaThresholdSet(self, cAtSurEnginePwPmParamFeSes, cBit9_0);
    ret |= AtPmThresholdProfilePwPeriodTcaThresholdSet(self, cAtSurEnginePwPmParamFeUas, cBit9_0);
    ret |= AtPmThresholdProfilePwPeriodTcaThresholdSet(self, cAtSurEnginePwPmParamFc   , cBit9_0);

    return ret;
    }

static void OverrideAtPmThresholdProfile(AtPmThresholdProfile self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPmThresholdProfileOverride, mMethodsGet(self), sizeof(m_AtPmThresholdProfileOverride));

        mMethodOverride(m_AtPmThresholdProfileOverride, SdhLinePeriodTcaThresholdSet);
        mMethodOverride(m_AtPmThresholdProfileOverride, SdhLinePeriodTcaThresholdGet);
        mMethodOverride(m_AtPmThresholdProfileOverride, SdhHoPathPeriodTcaThresholdSet);
        mMethodOverride(m_AtPmThresholdProfileOverride, SdhHoPathPeriodTcaThresholdGet);
        mMethodOverride(m_AtPmThresholdProfileOverride, SdhLoPathPeriodTcaThresholdSet);
        mMethodOverride(m_AtPmThresholdProfileOverride, SdhLoPathPeriodTcaThresholdGet);
        mMethodOverride(m_AtPmThresholdProfileOverride, PdhDe3PeriodTcaThresholdSet);
        mMethodOverride(m_AtPmThresholdProfileOverride, PdhDe3PeriodTcaThresholdGet);
        mMethodOverride(m_AtPmThresholdProfileOverride, PdhDe1PeriodTcaThresholdSet);
        mMethodOverride(m_AtPmThresholdProfileOverride, PdhDe1PeriodTcaThresholdGet);
        mMethodOverride(m_AtPmThresholdProfileOverride, PwPeriodTcaThresholdSet);
        mMethodOverride(m_AtPmThresholdProfileOverride, PwPeriodTcaThresholdGet);
        mMethodOverride(m_AtPmThresholdProfileOverride, Init);
        }

    mMethodsSet(self, &m_AtPmThresholdProfileOverride);
    }

static void Override(AtPmThresholdProfile self)
    {
    OverrideAtPmThresholdProfile(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaHardPmThresholdProfile);
    }

static AtPmThresholdProfile ObjectInit(AtPmThresholdProfile self, AtModuleSur module, uint32 profileId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (AtPmThresholdProfileObjectInit(self, module, profileId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPmThresholdProfile ThaHardPmThresholdProfileNew(AtModuleSur module, uint32 profileId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPmThresholdProfile newProfile = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newProfile == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newProfile, module, profileId);
    }

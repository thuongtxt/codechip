/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Surveillance
 * 
 * File        : ThaHardPmSesThresholdProfileInternal.h
 * 
 * Created Date: Mar 19, 2015
 *
 * Description : Hardware PM SES threshold internal header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAHARDPMSESTHRESHOLDPROFILEINTERNAL_H_
#define _THAHARDPMSESTHRESHOLDPROFILEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../../generic/sur/AtPmSesThresholdProfileInternal.h"
#include "ThaHardPmSesThresholdProfile.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaHardPmSesThresholdProfile
    {
    tAtPmSesThresholdProfile super;
    }tThaHardPmSesThresholdProfile;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#endif /* _THAHARDPMSESTHRESHOLDPROFILEINTERNAL_H_ */


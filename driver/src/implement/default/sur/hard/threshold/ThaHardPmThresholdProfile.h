/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SUR
 * 
 * File        : ThaHardPmThresholdProfile.h
 * 
 * Created Date: Sep 10, 2016
 *
 * Description : Tha PM Threshold Profile
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAHARDPMTHRESHOLDPROFILE_H_
#define _THAHARDPMTHRESHOLDPROFILE_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPmThresholdProfile ThaHardPmThresholdProfileNew(AtModuleSur module, uint32 profileId);

#ifdef __cplusplus
}
#endif
#endif /* _THAHARDPMTHRESHOLDPROFILE_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SUR
 * 
 * File        : ThaHardPmThresholdProfileInternal.h
 * 
 * Created Date: Sep 10, 2016
 *
 * Description : Tha PM Threshold Profile
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAHARDPMTHRESHOLDPROFILEINTERNAL_H_
#define _THAHARDPMTHRESHOLDPROFILEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../../generic/sur/AtPmThresholdProfileInternal.h"
#include "ThaHardPmThresholdProfile.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

typedef struct tThaHardPmThresholdProfile
    {
    tAtPmThresholdProfile super;
    }tThaHardPmThresholdProfile;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THAHARDPMTHRESHOLDPROFILEINTERNAL_H_ */


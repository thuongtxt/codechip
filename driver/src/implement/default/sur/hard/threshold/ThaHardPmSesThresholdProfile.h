/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Surveillance
 * 
 * File        : ThaHardPmSesThresholdProfile.h
 * 
 * Created Date: Mar 19, 2015
 *
 * Description : Hardware PM SES threshold header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAHARDPMSESTHRESHOLDPROFILE_H_
#define _THAHARDPMSESTHRESHOLDPROFILE_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPmSesThresholdProfile ThaHardPmSesThresholdProfileNew(AtModuleSur module, uint32 profileId);

#endif /* _THAHARDPMSESTHRESHOLDPROFILE_H_ */


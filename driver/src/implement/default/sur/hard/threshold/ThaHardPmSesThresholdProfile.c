/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Surveillance
 *
 * File        : ThaHardPmSesThresholdProfile.c
 *
 * Created Date: Mar 21, 2015
 *
 * Description : Hardware PM SES Threshold Profile
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../man/ThaDevice.h"
#include "../ThaModuleHardSurInternal.h"
#include "../ThaModuleHardSurReg.h"
#include "../param/ThaHardPmRegisterInternal.h"
#include "ThaHardPmSesThresholdProfileInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtPmSesThresholdProfileMethods m_AtPmSesThresholdProfileOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleHardSur SurModule(AtPmSesThresholdProfile self)
    {
    return (ThaModuleHardSur)AtPmSesThresholdProfileModuleGet(self);
    }

static uint32 BaseAddress(AtPmSesThresholdProfile self)
    {
    return ThaModuleHardSurBaseAddress((ThaModuleHardSur)AtPmSesThresholdProfileModuleGet(self));
    }

static uint32 DefaultOffset(AtPmSesThresholdProfile self)
    {
    return BaseAddress(self) + AtPmSesThresholdProfileIdGet(self);
    }

static uint32 Read(AtPmSesThresholdProfile self, uint32 address)
    {
    return mModuleHwRead(SurModule(self), address);
    }

static void Write(AtPmSesThresholdProfile self, uint32 address, uint32 value)
    {
    mModuleHwWrite(SurModule(self), address, value);
    }

static eAtRet ThresholdValueCheck(uint32 threshold)
    {
    return mInRange(threshold, 0, cBit15_0) ? cAtOk : cAtErrorOutOfRangParm;
    }

static eAtRet SdhSectionThresholdSet(AtPmSesThresholdProfile self, uint32 threshold)
    {
    eAtRet ret;
    uint32 offset = DefaultOffset(self);
    uint32 regAddr, regVal;

    ret = ThresholdValueCheck(threshold);
    if (ret != cAtOk)
        return ret;

    regAddr = cAf6Reg_FMPM_K_threshold_of_Section_Base + offset;
    regVal  = Read(self, regAddr);
    mRegFieldSet(regVal, cAf6_FMPM_K_threshold_of_Section_section_kval_, threshold);
    Write(self, regAddr, regVal);

    return cAtOk;
    }

static uint32 SdhSectionThresholdGet(AtPmSesThresholdProfile self)
    {
    uint32 offset = DefaultOffset(self);
    uint32 regAddr, regVal;

    regAddr = cAf6Reg_FMPM_K_threshold_of_Section_Base + offset;
    regVal  = Read(self, regAddr);
    return mRegField(regVal, cAf6_FMPM_K_threshold_of_Section_section_kval_);
    }

static eAtRet SdhLineThresholdSet(AtPmSesThresholdProfile self, uint32 threshold)
    {
    eAtRet ret;
    uint32 offset = DefaultOffset(self);
    uint32 regAddr, regVal;

    ret = ThresholdValueCheck(threshold);
    if (ret != cAtOk)
        return ret;

    regAddr = cAf6Reg_FMPM_K_threshold_of_Section_Base + offset;
    regVal  = Read(self, regAddr);
    mRegFieldSet(regVal, cAf6_FMPM_K_threshold_of_Section_line_kval_, threshold);
    Write(self, regAddr, regVal);

    return cAtOk;
    }

static uint32 SdhLineThresholdGet(AtPmSesThresholdProfile self)
    {
    uint32 offset = DefaultOffset(self);
    uint32 regAddr, regVal;

    regAddr = cAf6Reg_FMPM_K_threshold_of_Section_Base + offset;
    regVal  = Read(self, regAddr);
    return mRegField(regVal, cAf6_FMPM_K_threshold_of_Section_line_kval_);
    }

static eAtRet SdhHoPathThresholdSet(AtPmSesThresholdProfile self, uint32 threshold)
    {
    eAtRet ret;
    uint32 offset = DefaultOffset(self);
    uint32 regAddr, regVal;

    ret = ThresholdValueCheck(threshold);
    if (ret != cAtOk)
        return ret;

    regAddr = cAf6Reg_FMPM_K_threshold_of_STS_Base + offset;
    regVal  = Read(self, regAddr);
    mRegFieldSet(regVal, cAf6_FMPM_K_threshold_of_STS_kval_, threshold);
    Write(self, regAddr, regVal);

    return cAtOk;
    }

static uint32 SdhHoPathThresholdGet(AtPmSesThresholdProfile self)
    {
    uint32 offset = DefaultOffset(self);
    uint32 regAddr, regVal;

    regAddr = cAf6Reg_FMPM_K_threshold_of_STS_Base + offset;
    regVal  = Read(self, regAddr);
    return mRegField(regVal, cAf6_FMPM_K_threshold_of_STS_kval_);
    }

static eAtRet SdhLoPathThresholdSet(AtPmSesThresholdProfile self, uint32 threshold)
    {
    eAtRet ret;
    uint32 offset = DefaultOffset(self);
    uint32 regAddr, regVal;

    ret = ThresholdValueCheck(threshold);
    if (ret != cAtOk)
        return ret;

    regAddr = cAf6Reg_FMPM_K_Threshold_of_VT_Base + offset;
    regVal  = Read(self, regAddr);
    mRegFieldSet(regVal, cAf6_FMPM_K_Threshold_of_VT_kval_, threshold);
    Write(self, regAddr, regVal);

    return cAtOk;
    }

static uint32 SdhLoPathThresholdGet(AtPmSesThresholdProfile self)
    {
    uint32 offset = DefaultOffset(self);
    uint32 regAddr, regVal;

    regAddr = cAf6Reg_FMPM_K_Threshold_of_VT_Base + offset;
    regVal  = Read(self, regAddr);
    return mRegField(regVal, cAf6_FMPM_K_Threshold_of_VT_kval_);
    }

static eAtRet PdhDe3LineThresholdSet(AtPmSesThresholdProfile self, uint32 threshold)
    {
    eAtRet ret;
    uint32 offset = DefaultOffset(self);
    uint32 regAddr, regVal;

    ret = ThresholdValueCheck(threshold);
    if (ret != cAtOk)
        return ret;

    regAddr = cAf6Reg_FMPM_K_Threshold_of_DS3_Base + offset;
    regVal  = Read(self, regAddr);
    mRegFieldSet(regVal, cAf6_FMPM_K_Threshold_of_DS3_line_kval_, threshold);
    Write(self, regAddr, regVal);

    return cAtOk;
    }

static uint32 PdhDe3LineThresholdGet(AtPmSesThresholdProfile self)
    {
    uint32 offset = DefaultOffset(self);
    uint32 regAddr, regVal;

    regAddr = cAf6Reg_FMPM_K_Threshold_of_DS3_Base + offset;
    regVal  = Read(self, regAddr);
    return mRegField(regVal, cAf6_FMPM_K_Threshold_of_DS3_line_kval_);
    }

static eAtRet PdhDe3PathThresholdSet(AtPmSesThresholdProfile self, uint32 threshold)
    {
    eAtRet ret;
    uint32 offset = DefaultOffset(self);
    uint32 regAddr, regVal;

    ret = ThresholdValueCheck(threshold);
    if (ret != cAtOk)
        return ret;

    regAddr = cAf6Reg_FMPM_K_Threshold_of_DS3_Base + offset;
    regVal  = Read(self, regAddr);
    mRegFieldSet(regVal, cAf6_FMPM_K_Threshold_of_DS3_path_kval_, threshold);
    Write(self, regAddr, regVal);

    return cAtOk;
    }

static uint32 PdhDe3PathThresholdGet(AtPmSesThresholdProfile self)
    {
    uint32 offset = DefaultOffset(self);
    uint32 regAddr, regVal;

    regAddr = cAf6Reg_FMPM_K_Threshold_of_DS3_Base + offset;
    regVal  = Read(self, regAddr);
    return mRegField(regVal, cAf6_FMPM_K_Threshold_of_DS3_path_kval_);
    }

static eAtRet PdhDe1LineThresholdSet(AtPmSesThresholdProfile self, uint32 threshold)
    {
    eAtRet ret;
    uint32 offset = DefaultOffset(self);
    uint32 regAddr, regVal;

    ret = ThresholdValueCheck(threshold);
    if (ret != cAtOk)
        return ret;

    regAddr = cAf6Reg_FMPM_K_Threshold_of_DS1_Base + offset;
    regVal  = Read(self, regAddr);
    mRegFieldSet(regVal, cAf6_FMPM_K_Threshold_of_DS1_line_kval_, threshold);
    Write(self, regAddr, regVal);

    return cAtOk;
    }

static uint32 PdhDe1LineThresholdGet(AtPmSesThresholdProfile self)
    {
    uint32 offset = DefaultOffset(self);
    uint32 regAddr, regVal;

    regAddr = cAf6Reg_FMPM_K_Threshold_of_DS1_Base + offset;
    regVal  = Read(self, regAddr);
    return mRegField(regVal, cAf6_FMPM_K_Threshold_of_DS1_line_kval_);
    }

static eAtRet PdhDe1PathThresholdSet(AtPmSesThresholdProfile self, uint32 threshold)
    {
    eAtRet ret;
    uint32 offset = DefaultOffset(self);
    uint32 regAddr, regVal;

    ret = ThresholdValueCheck(threshold);
    if (ret != cAtOk)
        return ret;

    regAddr = cAf6Reg_FMPM_K_Threshold_of_DS1_Base + offset;
    regVal  = Read(self, regAddr);
    mRegFieldSet(regVal, cAf6_FMPM_K_Threshold_of_DS1_path_kval_, threshold);
    Write(self, regAddr, regVal);

    return cAtOk;
    }

static uint32 PdhDe1PathThresholdGet(AtPmSesThresholdProfile self)
    {
    uint32 offset = DefaultOffset(self);
    uint32 regAddr, regVal;

    regAddr = cAf6Reg_FMPM_K_Threshold_of_DS1_Base + offset;
    regVal  = Read(self, regAddr);
    return mRegField(regVal, cAf6_FMPM_K_Threshold_of_DS1_path_kval_);
    }

static eAtRet PwThresholdSet(AtPmSesThresholdProfile self, uint32 threshold)
    {
    eAtRet ret;
    uint32 offset = DefaultOffset(self);
    uint32 regAddr, regVal;

    ret = ThresholdValueCheck(threshold);
    if (ret != cAtOk)
        return ret;

    regAddr = cAf6Reg_FMPM_K_Threshold_of_PW_Base + offset;
    regVal  = Read(self, regAddr);
    mRegFieldSet(regVal, cAf6_FMPM_K_Threshold_of_PW_kval_, threshold);
    Write(self, regAddr, regVal);

    return cAtOk;
    }

static uint32 PwThresholdGet(AtPmSesThresholdProfile self)
    {
    uint32 offset = DefaultOffset(self);
    uint32 regAddr, regVal;

    regAddr = cAf6Reg_FMPM_K_Threshold_of_PW_Base + offset;
    regVal  = Read(self, regAddr);
    return mRegField(regVal, cAf6_FMPM_K_Threshold_of_PW_kval_);
    }

static uint32 SdhSectionSesThresholdDefaultValue(AtPmSesThresholdProfile self)
    {
    static const uint32 sections[] = {52, 155, 616, 2392, 8554, 22778}; /* See GR-253 issue4, Table 6-9 */
    uint32 profileId = AtPmSesThresholdProfileIdGet(self);

    if (profileId < mCount(sections))
        return sections[profileId];
    return 0;
    }

static uint32 SdhLineSesThresholdDefaultValue(AtPmSesThresholdProfile self)
    {
    static const uint32 lines[] = {51, 154, 615, 2459, 9835, 39339}; /* See GR-253 issue4, Table 6-10 */
    uint32 profileId = AtPmSesThresholdProfileIdGet(self);

    if (profileId < mCount(lines))
        return lines[profileId];
    return 0;
    }

static uint32 SdhHoPathSesThresholdDefaultValue(AtPmSesThresholdProfile self)
    {
    static const uint32 thresholds = 2400; /* See GR-253 issue4, Table 6-11 */
    AtUnused(self);
    return thresholds;
    }

static uint32 SdhLoPathSesThresholdDefaultValue(AtPmSesThresholdProfile self)
    {
    static const uint32 vc1xThresholds = 600; /* See GR-253 issue4, Table 6-12 */
    static const uint32 vc3Thresholds = 2400;
    uint32 profileId = AtPmSesThresholdProfileIdGet(self);
    if (profileId == 0)
        return vc3Thresholds; /* For TU3/VC3 */
    return vc1xThresholds;
    }

static uint32 PdhDe3SesThresholdDefaultValue(AtPmSesThresholdProfile self)
    {
    /* See GR-820, page 96 */
    static const uint32 thresholds = 44;
    AtUnused(self);
    return thresholds;
    }

static uint32 PdhDe1LineSesThresholdDefaultValue(AtPmSesThresholdProfile self)
    {
    /* See GR-820, page 71 */
    static uint32 thresholds = 1544;
    AtUnused(self);
    return thresholds;
    }

static uint32 PdhDe1PathSesThresholdDefaultValue(AtPmSesThresholdProfile self)
    {
    /* See GR-820, page 71 */
    static const uint32 thresholds = 320;
    AtUnused(self);
    return thresholds;
    }

static uint32 PwSesThresholdDefaultValue(AtPmSesThresholdProfile self)
    {
    /* As specified in RFC-4842, "Each second that contains at least one type 2
     * defect SHALL be declared as SES-CEP". So not sure while K is used. Just
     * initialize to all 1 */
    static const uint32 thresholds = 1;
    AtUnused(self);
    return thresholds;
    }

static eAtRet Init(AtPmSesThresholdProfile self)
    {
    eAtRet ret = cAtOk;
    ret |= AtPmSesThresholdProfileSdhSectionThresholdSet(self, SdhSectionSesThresholdDefaultValue(self));
    ret |= AtPmSesThresholdProfileSdhLineThresholdSet(self, SdhLineSesThresholdDefaultValue(self));
    ret |= AtPmSesThresholdProfileSdhHoPathThresholdSet(self, SdhHoPathSesThresholdDefaultValue(self));
    ret |= AtPmSesThresholdProfileSdhLoPathThresholdSet(self, SdhLoPathSesThresholdDefaultValue(self));
    ret |= AtPmSesThresholdProfilePdhDe3LineThresholdSet(self, PdhDe3SesThresholdDefaultValue(self));
    ret |= AtPmSesThresholdProfilePdhDe3PathThresholdSet(self, PdhDe3SesThresholdDefaultValue(self));
    ret |= AtPmSesThresholdProfilePdhDe1LineThresholdSet(self, PdhDe1LineSesThresholdDefaultValue(self));
    ret |= AtPmSesThresholdProfilePdhDe1PathThresholdSet(self, PdhDe1PathSesThresholdDefaultValue(self));
    ret |= AtPmSesThresholdProfilePwThresholdSet(self, PwSesThresholdDefaultValue(self));

    return ret;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaHardPmSesThresholdProfile);
    }

static void OverrideAtPmSesThresholdProfile(AtPmSesThresholdProfile self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPmSesThresholdProfileOverride, mMethodsGet(self), sizeof(m_AtPmSesThresholdProfileOverride));

        mMethodOverride(m_AtPmSesThresholdProfileOverride, SdhSectionThresholdSet);
        mMethodOverride(m_AtPmSesThresholdProfileOverride, SdhSectionThresholdGet);
        mMethodOverride(m_AtPmSesThresholdProfileOverride, SdhLineThresholdSet);
        mMethodOverride(m_AtPmSesThresholdProfileOverride, SdhLineThresholdGet);
        mMethodOverride(m_AtPmSesThresholdProfileOverride, SdhHoPathThresholdSet);
        mMethodOverride(m_AtPmSesThresholdProfileOverride, SdhHoPathThresholdGet);
        mMethodOverride(m_AtPmSesThresholdProfileOverride, SdhLoPathThresholdSet);
        mMethodOverride(m_AtPmSesThresholdProfileOverride, SdhLoPathThresholdGet);
        mMethodOverride(m_AtPmSesThresholdProfileOverride, PdhDe3LineThresholdSet);
        mMethodOverride(m_AtPmSesThresholdProfileOverride, PdhDe3LineThresholdGet);
        mMethodOverride(m_AtPmSesThresholdProfileOverride, PdhDe3PathThresholdSet);
        mMethodOverride(m_AtPmSesThresholdProfileOverride, PdhDe3PathThresholdGet);
        mMethodOverride(m_AtPmSesThresholdProfileOverride, PdhDe1LineThresholdSet);
        mMethodOverride(m_AtPmSesThresholdProfileOverride, PdhDe1LineThresholdGet);
        mMethodOverride(m_AtPmSesThresholdProfileOverride, PdhDe1PathThresholdSet);
        mMethodOverride(m_AtPmSesThresholdProfileOverride, PdhDe1PathThresholdGet);
        mMethodOverride(m_AtPmSesThresholdProfileOverride, PwThresholdSet);
        mMethodOverride(m_AtPmSesThresholdProfileOverride, PwThresholdGet);
        mMethodOverride(m_AtPmSesThresholdProfileOverride, Init);
        }

    mMethodsSet(self, &m_AtPmSesThresholdProfileOverride);
    }

static void Override(AtPmSesThresholdProfile self)
    {
    OverrideAtPmSesThresholdProfile(self);
    }

static AtPmSesThresholdProfile ObjectInit(AtPmSesThresholdProfile self, AtModuleSur module, uint32 profileId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (AtPmSesThresholdProfileObjectInit(self, module, profileId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPmSesThresholdProfile ThaHardPmSesThresholdProfileNew(AtModuleSur module, uint32 profileId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPmSesThresholdProfile newPmThreshold = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPmThreshold == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPmThreshold, module, profileId);
    }

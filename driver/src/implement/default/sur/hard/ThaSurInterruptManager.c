/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : ThaSurInterruptManager.c
 *
 * Created Date: Dec 30, 2015
 *
 * Description : Default Surveillance interrupt manager.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../man/ThaDeviceInternal.h"
#include "../../man/ThaIpCoreInternal.h"
#include "../../../../generic/sur/AtModuleSurInternal.h"
#include "../../../../generic/sur/AtSurEngineInternal.h"
#include "ThaSurInterruptManagerInternal.h"
#include "ThaModuleHardSurReg.h"
#include "ThaModuleHardSur.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)         ((ThaSurInterruptManager)self)
#define mSurOffset(self)    mMethodsGet(self)->Offset(self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static struct tThaSurInterruptManagerMethods m_methods;

/* Override */
static tAtInterruptManagerMethods   m_AtInterruptManagerOverride;
static tThaInterruptManagerMethods  m_ThaInterruptManagerOverride;

/* Save superclass implementation. */
static const tAtInterruptManagerMethods  *m_AtInterruptManagerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static AtDevice DeviceGet(ThaSurInterruptManager self)
    {
    return AtModuleDeviceGet(AtInterruptManagerModuleGet((AtInterruptManager)self));
    }

static void InterruptProcess(AtInterruptManager self, uint32 glbIntr, AtIpCore ipCore)
    {
    AtHal hal = AtIpCoreHalGet(ipCore);
    uint32 intrReg = ThaInterruptManagerBaseAddress((ThaInterruptManager)self) + cAf6Reg_FMPM_FM_Interrupt + mSurOffset(mThis(self));
    uint32 intrVal;

    AtUnused(glbIntr);

    if (hal == NULL)
        return;

    intrVal = AtHalRead(hal, intrReg);

    if (mMethodsGet(mThis(self))->LineHasInterrupt(mThis(self), intrVal))
        mMethodsGet(mThis(self))->LineInterruptProcess(mThis(self), hal);

    if (mMethodsGet(mThis(self))->StsHasInterrupt(mThis(self), intrVal))
        mMethodsGet(mThis(self))->StsInterruptProcess(mThis(self), hal);

    if (mMethodsGet(mThis(self))->VtHasInterrupt(mThis(self), intrVal))
        mMethodsGet(mThis(self))->VtInterruptProcess(mThis(self), hal);

    if (mMethodsGet(mThis(self))->De3HasInterrupt(mThis(self), intrVal))
        mMethodsGet(mThis(self))->De3InterruptProcess(mThis(self), hal);

    if (mMethodsGet(mThis(self))->De1HasInterrupt(mThis(self), intrVal))
        mMethodsGet(mThis(self))->De1InterruptProcess(mThis(self), hal);

    if (mMethodsGet(mThis(self))->PwHasInterrupt(mThis(self), intrVal))
        mMethodsGet(mThis(self))->PwInterruptProcess(mThis(self), hal);
    }

static void InterruptHwEnable(ThaInterruptManager self, eBool enable, AtIpCore ipCore)
    {
    uint32 interruptReg = ThaInterruptManagerBaseAddress(self) + cAf6Reg_FMPM_FM_Interrupt_Mask + mSurOffset(mThis(self));
    uint32 interruptEn = (enable) ? mMethodsGet(mThis(self))->InterruptMask(mThis(self)) : 0;
    AtHal hal = AtIpCoreHalGet(ipCore);
    AtModule module = AtInterruptManagerModuleGet((AtInterruptManager)self);

    ThaIpCoreSurHwInterruptEnable((ThaIpCore)ipCore, enable);
    AtModuleHwWrite(module, interruptReg, interruptEn, hal);
    }

static eBool HasInterrupt(AtInterruptManager self, uint32 glbIntr, AtHal hal)
    {
    AtUnused(self);
    AtUnused(hal);
    return (glbIntr & cBit6) ? cAtTrue : cAtFalse;
    }

static ThaModuleHardSur SurModule(ThaSurInterruptManager self)
    {
    return (ThaModuleHardSur)AtInterruptManagerModuleGet((AtInterruptManager)self);
    }

static uint32 BaseAddress(ThaInterruptManager self)
    {
    return ThaModuleHardSurBaseAddress(SurModule((ThaSurInterruptManager)self));
    }

static uint32 NumSlices(ThaInterruptManager self)
    {
    AtUnused(self);
    return 8;
    }

static ThaModuleSdh SdhModule(ThaSurInterruptManager self)
    {
    return (ThaModuleSdh)AtDeviceModuleGet(DeviceGet(self), cAtModuleSdh);
    }

static ThaModulePdh PdhModule(ThaSurInterruptManager self)
    {
    return (ThaModulePdh)AtDeviceModuleGet(DeviceGet(self), cAtModulePdh);
    }

static AtModulePw PwModule(ThaSurInterruptManager self)
    {
    return (AtModulePw)AtDeviceModuleGet(DeviceGet(self), cAtModulePw);
    }

static void LineInterruptProcess(ThaSurInterruptManager self, AtHal hal)
    {
    const uint32 baseAddress = ThaInterruptManagerBaseAddress(mIntrManager(self)) + mSurOffset(self);
    uint32 intrOR = AtHalRead(hal, baseAddress + cAf6Reg_FMPM_FM_EC1_Interrupt_OR);
    AtModuleSdh sdhModule = (AtModuleSdh)SdhModule(self);
    uint32 numLines = AtModuleSdhMaxLinesGet(sdhModule);
    uint8 line_i;
    uint32 intrSts24Val;

    if (intrOR == 0)
        {
        AtModuleLog((AtModule)SurModule(self), cAtLogLevelCritical, AtSourceLocation,
                    "Line interrupt routine is accidentally terminated at EC1_Interrupt_OR(0x%x)\n",
                    baseAddress + cAf6Reg_FMPM_FM_EC1_Interrupt_OR);
        return;
        }

    /* Default implementation is applicable for projects which have less than 32 lines */
    intrSts24Val = AtHalRead(hal, baseAddress + cAf6Reg_FMPM_FM_EC1_Interrupt_OR_AND_MASK_Per_STS1_Base);
    for (line_i = 0; line_i < numLines; line_i++)
        {
        if (intrSts24Val & cIteratorMask(line_i))
            {
            AtSdhLine sdhLine = AtModuleSdhLineGet(sdhModule, line_i);
            uint32 offset = ThaModuleHardSurLineOffset(SurModule(self), 0, line_i);
            AtSurEngine engine;

            /* Exception */
            if (sdhLine == NULL)
                mThrowNullEngine(self, Line, offset)

            engine = AtChannelSurEngineGet((AtChannel)sdhLine);

            /* Execute line interrupt process */
            ThaSurInterruptManagerEngineNotify(self, engine, offset);
            }
        }
    }

static void StsInterruptProcess(ThaSurInterruptManager self, AtHal hal)
    {
    const uint32 baseAddress = ThaInterruptManagerBaseAddress(mIntrManager(self)) + mSurOffset(self);
    uint32 intrOR = AtHalRead(hal, baseAddress + cAf6Reg_FMPM_FM_STS24_Interrupt_OR);
    ThaModuleSdh sdhModule = SdhModule(self);
    uint32 numSlices = ThaInterruptManagerNumSlices(mIntrManager(self));
    uint32 numStsInSlice = ThaInterruptManagerNumStsInSlices(mIntrManager(self));
    uint32 slice, sts1;

    for (slice = 0; slice < numSlices; slice++)
        {
        if (intrOR & cIteratorMask(slice))
            {
            /* Get STS24 OR interrupt status */
            uint32 intrSts24Val = AtHalRead(hal, baseAddress + cAf6Reg_FMPM_FM_STS24_Interrupt_OR_AND_MASK_Per_STS1_Base + slice);

            for (sts1 = 0; sts1 < numStsInSlice; sts1++)
                {
                if (intrSts24Val & cIteratorMask(sts1))
                    {
                    AtSdhPath auPath;
                    AtChannel   vcPath;
                    uint32 offset = ThaModuleHardSurStsOffset(SurModule(self), (uint8)slice, (uint8)sts1);

                    /* Get AU path from hardware ID */
                    auPath = ThaModuleSdhAuPathFromHwIdGet(sdhModule, cAtModuleSur, (uint8)slice, (uint8)sts1);

                    /* Exception */
                    if (auPath == NULL)
                        mThrowNullEngine(self, Sts, offset)

                    vcPath = (AtChannel)AtSdhChannelSubChannelGet((AtSdhChannel)auPath, 0);

                    /* Execute path interrupt process */
                    ThaSurInterruptManagerEngineNotify(self, AtChannelSurEngineGet(vcPath), offset);
                    }
                }
            }
        }
    }

static void VtInterruptProcess(ThaSurInterruptManager self, AtHal hal)
    {
    const uint32 baseAddress = ThaInterruptManagerBaseAddress(mIntrManager(self)) + mSurOffset(self);
    uint32 intrOR = AtHalRead(hal, baseAddress + cAf6Reg_FMPM_FM_STS24_LO_Interrupt_OR);
    ThaModuleSdh sdhModule = SdhModule(self);
    uint32 numSlices = ThaInterruptManagerNumSlices(mIntrManager(self));
    uint32 numStsInSlice = ThaInterruptManagerNumStsInSlices(mIntrManager(self));
    uint32 slice, sts1;

    for (slice = 0; slice < numSlices; slice++)
        {
        if (intrOR & cIteratorMask(slice))
            {
            /* Get STS24 OR interrupt status */
            uint32 intrSts24Val = AtHalRead(hal, baseAddress + cAf6Reg_FMPM_FM_STS1_LO_Interrupt_OR(slice));

            for (sts1 = 0; sts1 < numStsInSlice; sts1++)
                {
                if (intrSts24Val & cIteratorMask(sts1))
                    {
                    uint8 vtg, vt, vt28 = 0;
                    uint32 intrVt28Val = AtHalRead(hal, baseAddress + cAf6Reg_FMPM_FM_VTTU_Interrupt_OR_AND_MASK_Per_VTTU(slice, sts1));

                    for (vtg = 0; vtg < 7; vtg++)
                        {
                        for (vt = 0; vt < 4; vt++)
                            {
                            if (intrVt28Val & cIteratorMask(vt28))
                                {
                                AtSdhPath tuPath;
                                AtChannel   vcPath;
                                uint32 offset = ThaModuleHardSurVtOffset(SurModule(self), (uint8)slice, (uint8)sts1, vtg, vt);

                                /* Get TU path from hardware ID */
                                tuPath = ThaModuleSdhTuPathFromHwIdGet(sdhModule, cAtModuleSur, (uint8)slice, (uint8)sts1, vtg, vt);

                                /* Exception */
                                if (tuPath == NULL)
                                    mThrowNullEngine(self, Vt, offset)

                                vcPath = (AtChannel)AtSdhChannelSubChannelGet((AtSdhChannel)tuPath, 0);

                                /* Execute path interrupt process */
                                ThaSurInterruptManagerEngineNotify(self, AtChannelSurEngineGet(vcPath), offset);
                                }
                            vt28++;
                            }
                        }
                    }
                }
            }
        }
    }

static void De3InterruptProcess(ThaSurInterruptManager self, AtHal hal)
    {
    const uint32 baseAddress = ThaInterruptManagerBaseAddress(mIntrManager(self)) + mSurOffset(self);
    uint32 intrOR = AtHalRead(hal, baseAddress + cAf6Reg_FMPM_FM_DS3E3_Group_Interrupt_OR);
    ThaModulePdh pdhModule = PdhModule(self);
    uint32 numSlices = ThaInterruptManagerNumSlices(mIntrManager(self));
    uint32 numStsInSlice = ThaInterruptManagerNumStsInSlices(mIntrManager(self));
    uint32 slice;

    for (slice = 0; slice < numSlices; slice++)
        {
        if (intrOR & cIteratorMask(slice))
            {
            uint32 intrSts24Val = AtHalRead(hal, baseAddress + cAf6Reg_FMPM_FM_DS3E3_Framer_Interrupt_OR_AND_MASK_Per_DS3E3(slice));
            uint8 de3;

            for (de3 = 0; de3 < numStsInSlice; de3++)
                {
                if (intrSts24Val & cIteratorMask(de3))
                    {
                    AtPdhDe3 de3Channel = ThaModulePdhDe3ChannelFromHwIdGet(pdhModule, cAtModuleSur, (uint8)slice, de3);
                    uint32 offset = ThaModuleHardSurDe3Offset(SurModule(self), (uint8)slice, de3);

                    /* Exception */
                    if (de3Channel == NULL)
                        mThrowNullEngine(self, De3, offset)

                    /* Execute channel interrupt. */
                    ThaSurInterruptManagerEngineNotify(self, AtChannelSurEngineGet((AtChannel)de3Channel), offset);
                    }
                }
            }
        }
    }

static void De1InterruptProcess(ThaSurInterruptManager self, AtHal hal)
    {
    const uint32 baseAddress = ThaInterruptManagerBaseAddress(mIntrManager(self)) + mSurOffset(self);
    uint32 intrOR = AtHalRead(hal, baseAddress + cAf6Reg_FMPM_FM_DS1E1_Level1_Group_Interrupt_OR);
    ThaModulePdh pdhModule = PdhModule(self);
    uint32 numSlices = ThaInterruptManagerNumSlices(mIntrManager(self));
    uint32 numStsInSlice = ThaInterruptManagerNumStsInSlices(mIntrManager(self));
    uint32 slice;

    for (slice = 0; slice < numSlices; slice++)
        {
        if (intrOR & cIteratorMask(slice))
            {
            /* Get STS24 OR interrupt status */
            uint32 intrSts24Val = AtHalRead(hal, baseAddress + cAf6Reg_FMPM_FM_DS1E1_Level2_Group_Interrupt_OR(slice));
            uint8 de3;

            for (de3 = 0; de3 < numStsInSlice; de3++)
                {
                if (intrSts24Val & cIteratorMask(de3))
                    {
                    uint8 de2, de1, vt28 = 0;
                    uint32 intrVt28Val = AtHalRead(hal, baseAddress + cAf6Reg_FMPM_FM_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1(slice, de3));

                    for (de2 = 0; de2 < 7; de2++)
                        {
                        for (de1 = 0; de1 < 4; de1++)
                            {
                            if (intrVt28Val & cIteratorMask(vt28))
                                {
                                AtPdhDe1 de1Channel = ThaModulePdhDe1ChannelFromHwIdGet(pdhModule, cAtModuleSur, (uint8)slice, de3, de2, de1);
                                uint32 offset = ThaModuleHardSurDe1Offset(SurModule(self), (uint8)slice, de3, de2, de1);

                                /* Exception */
                                if (de1Channel == NULL)
                                    mThrowNullEngine(self, De1, offset)

                                /* Execute path interrupt process */
                                ThaSurInterruptManagerEngineNotify(self, AtChannelSurEngineGet((AtChannel)de1Channel), offset);
                                }
                            vt28++;
                            }
                        }
                    }
                }
            }
        }
    }

static void PwInterruptProcess(ThaSurInterruptManager self, AtHal hal)
    {
    const uint32 baseAddress = ThaInterruptManagerBaseAddress(mIntrManager(self)) + mSurOffset(self);
    uint32 intrOR = AtHalRead(hal, baseAddress + cAf6Reg_FMPM_FM_PW_Level1_Group_Interrupt_OR);
    AtModulePw pwModule = PwModule(self);
    uint32 numSlices = ThaInterruptManagerNumSlices(mIntrManager(self));
    uint32 slice;

    for (slice = 0; slice < numSlices; slice++)
        {
        if (intrOR & cIteratorMask(slice))
            {
            uint32 intrStat = AtHalRead(hal, baseAddress + cAf6Reg_FMPM_FM_PW_Level2_Group_Interrupt_OR(slice));
            uint32 grpid;

            for (grpid = 0; grpid < 32; grpid++)
                {
                if (intrStat & cIteratorMask(grpid))
                    {
                    uint32 intrStat32 = AtHalRead(hal, baseAddress + cAf6Reg_FMPM_FM_PW_Framer_Interrupt_OR_AND_MASK_Per_PW(slice, grpid));
                    uint32 subid;

                    for (subid = 0; subid < 32; subid++)
                        {
                        if (intrStat32 & cIteratorMask(subid))
                            {
                            uint32 pwid = (slice << 10) + (grpid << 5) + subid;

                            /* Get PW channel */
                            AtPw pwChannel = AtModulePwGetPw(pwModule, pwid);

                            /* Exception */
                            if (pwChannel == NULL)
                                mThrowNullEngine(self, Pw, pwid)

                            /* Process channel interrupt. */
                            ThaSurInterruptManagerEngineNotify(self, AtChannelSurEngineGet((AtChannel)pwChannel), pwid);
                            }
                        }
                    }
                }
            }
        }
    }

static eBool LineHasInterrupt(ThaSurInterruptManager self, uint32 glbIntr)
    {
    if (AtModuleSurLineIsSupported((AtModuleSur)SurModule(self)))
        return (glbIntr & cAf6_FMPM_FM_Interrupt_Mask_FMPM_FM_Intr_MSK_ECSTM_Mask) ? cAtTrue : cAtFalse;
    return cAtFalse;
    }

static eBool StsHasInterrupt(ThaSurInterruptManager self, uint32 glbIntr)
    {
    AtUnused(self);
    return (glbIntr & cAf6_FMPM_FM_Interrupt_Mask_FMPM_FM_Intr_MSK_STSAU_Mask) ? cAtTrue : cAtFalse;
    }

static eBool VtHasInterrupt(ThaSurInterruptManager self, uint32 glbIntr)
    {
    AtUnused(self);
    return (glbIntr & cAf6_FMPM_FM_Interrupt_Mask_FMPM_FM_Intr_MSK_VTGTUG_Mask) ? cAtTrue : cAtFalse;
    }

static eBool De3HasInterrupt(ThaSurInterruptManager self, uint32 glbIntr)
    {
    AtUnused(self);
    return (glbIntr & cAf6_FMPM_FM_Interrupt_Mask_FMPM_FM_Intr_MSK_DS3E3_Mask) ? cAtTrue : cAtFalse;
    }

static eBool De1HasInterrupt(ThaSurInterruptManager self, uint32 glbIntr)
    {
    AtUnused(self);
    return (glbIntr & cAf6_FMPM_FM_Interrupt_Mask_FMPM_FM_Intr_MSK_DS1E1_Mask) ? cAtTrue : cAtFalse;
    }

static eBool PwHasInterrupt(ThaSurInterruptManager self, uint32 glbIntr)
    {
    AtUnused(self);
    return (glbIntr & cAf6_FMPM_FM_Interrupt_Mask_FMPM_FM_Intr_MSK_PW_Mask) ? cAtTrue : cAtFalse;
    }

static uint32 InterruptMask(ThaSurInterruptManager self)
    {
    return (((AtModuleSurLineIsSupported((AtModuleSur)SurModule(self))) ?
             cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_ECSTM_Mask : 0) |
            cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_VTGTUG_Mask |
            cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_STSAU_Mask  |
            cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_DS3E3_Mask  |
            cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_DS1E1_Mask  |
            cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_PW_Mask);
    }

static uint32 Offset(ThaSurInterruptManager self)
    {
    AtUnused(self);
    return 0;
    }

static void EngineNotify(ThaSurInterruptManager self, AtSurEngine engine, uint32 engineOffset)
    {
    AtUnused(self);
    AtSurEngineNotificationProcess(engine, engineOffset);
    }

static uint32 NumStsInSlice(ThaInterruptManager self)
    {
    AtUnused(self);
    return 24;
    }

static void OverrideThaInterruptManager(ThaSurInterruptManager self)
    {
    ThaInterruptManager manager = (ThaInterruptManager)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInterruptManagerOverride, mMethodsGet(manager), sizeof(m_ThaInterruptManagerOverride));

        mMethodOverride(m_ThaInterruptManagerOverride, BaseAddress);
        mMethodOverride(m_ThaInterruptManagerOverride, NumSlices);
        mMethodOverride(m_ThaInterruptManagerOverride, InterruptHwEnable);
        mMethodOverride(m_ThaInterruptManagerOverride, NumStsInSlice);
        }

    mMethodsSet(manager, &m_ThaInterruptManagerOverride);
    }

static void OverrideAtInterruptManager(ThaSurInterruptManager self)
    {
    AtInterruptManager manager = (AtInterruptManager)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtInterruptManagerMethods = mMethodsGet(manager);
        mMethodsGet(osal)->MemCpy(osal, &m_AtInterruptManagerOverride, mMethodsGet(manager), sizeof(m_AtInterruptManagerOverride));

        mMethodOverride(m_AtInterruptManagerOverride, HasInterrupt);
        mMethodOverride(m_AtInterruptManagerOverride, InterruptProcess);
        }

    mMethodsSet(manager, &m_AtInterruptManagerOverride);
    }

static void Override(ThaSurInterruptManager self)
    {
    OverrideAtInterruptManager(self);
    OverrideThaInterruptManager(self);
    }

static void MethodsInit(ThaSurInterruptManager self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, LineInterruptProcess);
        mMethodOverride(m_methods, StsInterruptProcess);
        mMethodOverride(m_methods, VtInterruptProcess);
        mMethodOverride(m_methods, De3InterruptProcess);
        mMethodOverride(m_methods, De1InterruptProcess);
        mMethodOverride(m_methods, PwInterruptProcess);
        mMethodOverride(m_methods, LineHasInterrupt);
        mMethodOverride(m_methods, StsHasInterrupt);
        mMethodOverride(m_methods, VtHasInterrupt);
        mMethodOverride(m_methods, De3HasInterrupt);
        mMethodOverride(m_methods, De1HasInterrupt);
        mMethodOverride(m_methods, PwHasInterrupt);
        mMethodOverride(m_methods, InterruptMask);
        mMethodOverride(m_methods, Offset);
        mMethodOverride(m_methods, EngineNotify);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaSurInterruptManager);
    }

AtInterruptManager ThaSurInterruptManagerObjectInit(AtInterruptManager self, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaInterruptManagerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override((ThaSurInterruptManager)self);
    MethodsInit((ThaSurInterruptManager)self);
    m_methodsInit = 1;

    return self;
    }

AtInterruptManager ThaSurInterruptManagerNew(AtModule module)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtInterruptManager newInterruptManager = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newInterruptManager == NULL)
        return NULL;

    return ThaSurInterruptManagerObjectInit(newInterruptManager, module);
    }

void ThaSurInterruptManagerEngineNotify(ThaSurInterruptManager self, AtSurEngine engine, uint32 engineOffset)
    {
    if (self)
        mMethodsGet(self)->EngineNotify(self, engine, engineOffset);
    }

void ThaSurInterruptManagerNullEngineProcess(ThaSurInterruptManager self, uint32 engineAddress, uint32 engineOffset)
    {
    mModuleHwWrite(SurModule(self), engineAddress + engineOffset, 0x0);

    AtModuleLog((AtModule)SurModule(self), cAtLogLevelCritical, AtSourceLocation,
                "NULL surveillance engine trapped at address 0x%08x, offset 0x%08x\n", engineAddress, engineOffset);
    }

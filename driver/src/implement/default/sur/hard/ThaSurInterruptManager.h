/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SUR
 * 
 * File        : ThaSurInterruptManager.h
 * 
 * Created Date: Dec 30, 2015
 *
 * Description : Default Surveillance interrupt manager.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASURINTERRUPTMANAGER_H_
#define _THASURINTERRUPTMANAGER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSurEngine.h"
#include "../../../../generic/man/interrupt/AtInterruptManager.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaSurInterruptManager * ThaSurInterruptManager;

/*--------------------------- Forward declarations ---------------------------*/
AtInterruptManager ThaSurInterruptManagerNew(AtModule module);
AtInterruptManager ThaSurTcaInterruptManagerNew(AtModule module);

void ThaSurInterruptManagerEngineNotify(ThaSurInterruptManager self, AtSurEngine engine, uint32 engineOffset);
void ThaSurInterruptManagerNullEngineProcess(ThaSurInterruptManager self, uint32 engineAddress, uint32 engineOffset);

/* Concrete */
AtInterruptManager Tha60210061SurInterruptManagerNew(AtModule module);
AtInterruptManager Tha60290022SurInterruptManagerNew(AtModule module);
AtInterruptManager Tha60290022SurTcaInterruptManagerNew(AtModule module);

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THASURINTERRUPTMANAGER_H_ */


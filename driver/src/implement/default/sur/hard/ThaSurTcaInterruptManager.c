/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : ThaSurTcaInterruptManager.c
 *
 * Created Date: Nov 11, 2016
 *
 * Description : Default Surveillance TCA interrupt manager.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../man/ThaDeviceInternal.h"
#include "../../../../generic/sur/AtSurEngineInternal.h"
#include "ThaSurInterruptManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtInterruptManagerMethods   m_AtInterruptManagerOverride;
static tThaSurInterruptManagerMethods m_ThaSurInterruptManagerOverride;

/* Save superclass implementation. */
static const tAtInterruptManagerMethods  *m_AtInterruptManagerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static eBool HasInterrupt(AtInterruptManager self, uint32 glbIntr, AtHal hal)
    {
    AtUnused(self);
    AtUnused(hal);
    return (glbIntr & cBit7) ? cAtTrue : cAtFalse;
    }

static uint32 Offset(ThaSurInterruptManager self)
    {
    AtUnused(self);
    /* Offset of TCA interrupt tree from Failure interrupt tree. */
    return 0x40000;
    }

static void EngineNotify(ThaSurInterruptManager self, AtSurEngine engine, uint32 engineOffset)
    {
    AtUnused(self);
    AtSurEngineTcaNotificationProcess(engine, engineOffset);
    }

static void OverrideAtInterruptManager(ThaSurInterruptManager self)
    {
    AtInterruptManager manager = (AtInterruptManager)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtInterruptManagerMethods = mMethodsGet(manager);
        mMethodsGet(osal)->MemCpy(osal, &m_AtInterruptManagerOverride, mMethodsGet(manager), sizeof(m_AtInterruptManagerOverride));

        mMethodOverride(m_AtInterruptManagerOverride, HasInterrupt);
        }

    mMethodsSet(manager, &m_AtInterruptManagerOverride);
    }

static void OverrideThaSurInterruptManager(ThaSurInterruptManager self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSurInterruptManagerOverride, mMethodsGet(self), sizeof(m_ThaSurInterruptManagerOverride));

        mMethodOverride(m_ThaSurInterruptManagerOverride, Offset);
        mMethodOverride(m_ThaSurInterruptManagerOverride, EngineNotify);
        }

    mMethodsSet(self, &m_ThaSurInterruptManagerOverride);
    }

static void Override(ThaSurInterruptManager self)
    {
    OverrideAtInterruptManager(self);
    OverrideThaSurInterruptManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaSurTcaInterruptManager);
    }

AtInterruptManager ThaSurTcaInterruptManagerObjectInit(AtInterruptManager self, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaSurInterruptManagerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override((ThaSurInterruptManager)self);
    m_methodsInit = 1;

    return self;
    }

AtInterruptManager ThaSurTcaInterruptManagerNew(AtModule module)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtInterruptManager newInterruptManager = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newInterruptManager == NULL)
        return NULL;

    return ThaSurTcaInterruptManagerObjectInit(newInterruptManager, module);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Surveillance
 * 
 * File        : ThaModuleSoftSur.h
 * 
 * Created Date: Mar 19, 2015
 *
 * Description : Hardware Surveillance Module header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEHARDSUR_H_
#define _THAMODULEHARDSUR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleSur.h"
#include "AtPw.h"

/*--------------------------- Define -----------------------------------------*/
typedef struct tThaModuleHardSur * ThaModuleHardSur;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef enum eThaModuleSurCounterType
    {
    cThaModuleSurCounterPwParams         = 0,
    cThaModuleSurCounterPwStatistic      = 1,
    cThaModuleSurCounterVtParams         = 2,
    cThaModuleSurCounterDe1Params        = 3,
    cThaModuleSurCounterStsParams        = 4,
    cThaModuleSurCounterDe3Params        = 5,
    cThaModuleSurCounterLineParams       = 6,
    cThaModuleSurCounterEncDecStatistic  = 7,
    cThaModuleSurCounterInvalid
    } eThaModuleSurCounterType;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleSur ThaModuleHardSurObjectInit(AtModuleSur self, AtDevice device);
AtModuleSur ThaModuleHardSurNew(AtDevice device);
AtModuleSur Tha60210011ModuleSurNew(AtDevice device);
AtModuleSur Tha60210021ModuleSurNew(AtDevice device);
AtModuleSur Tha60210031ModuleSurNew(AtDevice device);
AtModuleSur Tha60210012ModuleSurNew(AtDevice device);
AtModuleSur Tha60210061ModuleSurNew(AtDevice device);
AtModuleSur Tha60290021ModuleSurNew(AtDevice device);
AtModuleSur Tha60290011ModuleSurNew(AtDevice device);
AtModuleSur Tha60290022ModuleSurNew(AtDevice device);
AtModuleSur Tha6A290011ModuleSurNew(AtDevice device);
AtModuleSur Tha6A290021ModuleSurNew(AtDevice device);
AtModuleSur Tha6A290022ModuleSurNew(AtDevice device);
AtModuleSur Tha60290061ModuleSurNew(AtDevice device);
AtModuleSur Tha60291011ModuleSurNew(AtDevice device);
AtModuleSur Tha60290081ModuleSurNew(AtDevice device);

/* Version controlling */
uint32 ThaModuleHardSurVersionNumber(ThaModuleHardSur self);
eBool ThaModuleHardSurVersionIsValid(ThaModuleHardSur self);

/* Work with DDR */
eAtRet ThaModuleHardSurDdrRead2Clear(ThaModuleHardSur self, uint32 page, uint8 type, uint32 address, uint32 *value, eBool read2Clear);
uint32 ThaModuleHardSurCurrentPage(ThaModuleHardSur self);
eAtRet ThaModuleHardSurDe1DdrCountersR2c(AtSurEngine engine, uint8 period, uint32 *regVal, eBool r2c);
eAtRet ThaModuleHardSurDe3DdrCountersR2c(AtSurEngine engine, uint8 period, uint32 *regVal, eBool r2c);
eAtRet ThaModuleHardSurPathDdrCountersR2c(AtSurEngine engine, uint8 period, uint32 *regVal, eBool r2c);
eAtRet ThaModuleHardSurLineDdrCountersR2c(AtSurEngine engine, uint8 period, uint32 *regVal, eBool r2c);
eAtRet ThaModuleHardSurPwDdrCountersR2c(AtSurEngine engine, uint8 period, uint32 *regVal, eBool r2c);
uint32 ThaModuleHardSurNumberDdrHoldRegisters(ThaModuleHardSur self);
uint8 ThaModuleSurSdhPathHwChannelType(AtSurEngine engine);

eAtRet ThaModuleHardSurPwCounterLatch(ThaModuleHardSur self, AtPw pw, eBool clear);
eAtRet ThaModuleHardSurHdlcPwCounterLatch(ThaModuleHardSur self, AtPw pw, eBool clear);
uint32 ThaModuleHardSurPwTxPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear);
uint32 ThaModuleHardSurPwTxBytesGet(ThaModuleHardSur self, AtPw pw, eBool clear);
uint32 ThaModuleHardSurPwTxRbitPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear);
uint32 ThaModuleHardSurPwTxMbitPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear);
uint32 ThaModuleHardSurPwTxLbitPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear);
uint32 ThaModuleHardSurPwTxNbitPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear);
uint32 ThaModuleHardSurPwTxPbitPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear);

uint32 ThaModuleHardSurPwRxPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear);
uint32 ThaModuleHardSurPwRxMalformedPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear);
uint32 ThaModuleHardSurPwRxStrayPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear);
uint32 ThaModuleHardSurPwRxOamPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear);
uint32 ThaModuleHardSurPwRxLbitPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear);
uint32 ThaModuleHardSurPwRxRbitPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear);
uint32 ThaModuleHardSurPwRxMbitPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear);
uint32 ThaModuleHardSurPwRxNbitPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear);
uint32 ThaModuleHardSurPwRxPbitPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear);

uint32 ThaModuleHardSurPwRxJitBufOverrunGet(ThaModuleHardSur self, AtPw pw, eBool clear);
uint32 ThaModuleHardSurPwRxJitBufUnderrunGet(ThaModuleHardSur self, AtPw pw, eBool clear);
uint32 ThaModuleHardSurPwRxLopsGet(ThaModuleHardSur self, AtPw pw, eBool clear);
uint32 ThaModuleHardSurPwRxPacketsSentToTdmGet(ThaModuleHardSur self, AtPw pw, eBool clear);
uint32 ThaModuleHardSurPwRxBytesGet(ThaModuleHardSur self, AtPw pw, eBool clear);
uint32 ThaModuleHardSurPwRxReorderedPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear);
uint32 ThaModuleHardSurPwRxLostPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear);
uint32 ThaModuleHardSurPwRxOutOfSeqDropedPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear);

/* Get Encap/Decap counter from DDR */
uint32 ThaModuleHardSurHdlcChannelTxGoodPacketsGet(ThaModuleHardSur self, AtChannel channel, eBool r2c);
uint32 ThaModuleHardSurHdlcChannelTxGoodBytesGet(ThaModuleHardSur self, AtChannel channel, eBool r2c);
uint32 ThaModuleHardSurHdlcChannelTxFragmentsGet(ThaModuleHardSur self, AtChannel channel, eBool r2c);
uint32 ThaModuleHardSurHdlcChannelTxPlainGet(ThaModuleHardSur self, AtChannel channel, eBool r2c);
uint32 ThaModuleHardSurHdlcChannelTxOamGet(ThaModuleHardSur self, AtChannel channel, eBool r2c);
uint32 ThaModuleHardSurHdlcChannelTxIdleBytesGet(ThaModuleHardSur self, AtChannel channel, eBool r2c);
uint32 ThaModuleHardSurHdlcChannelTxAbortsGet(ThaModuleHardSur self, AtChannel channel, eBool r2c);

uint32 ThaModuleHardSurHdlcChannelRxGoodPacketsGet(ThaModuleHardSur self, AtChannel channel, eBool r2c);
uint32 ThaModuleHardSurHdlcChannelRxBytesGet(ThaModuleHardSur self, AtChannel channel, eBool r2c);
uint32 ThaModuleHardSurHdlcChannelRxGoodBytesGet(ThaModuleHardSur self, AtChannel channel, eBool r2c);
uint32 ThaModuleHardSurHdlcChannelRxFragmentsGet(ThaModuleHardSur self, AtChannel channel, eBool r2c);
uint32 ThaModuleHardSurHdlcChannelRxPlainGet(ThaModuleHardSur self, AtChannel channel, eBool r2c);
uint32 ThaModuleHardSurHdlcChannelRxOamGet(ThaModuleHardSur self, AtChannel channel, eBool r2c);
uint32 ThaModuleHardSurHdlcChannelRxAbortsGet(ThaModuleHardSur self, AtChannel channel, eBool r2c);
uint32 ThaModuleHardSurHdlcChannelRxIdleBytesGet(ThaModuleHardSur self, AtChannel channel, eBool r2c);
uint32 ThaModuleHardSurHdlcChannelRxMRUPacketsGet(ThaModuleHardSur self, AtChannel channel, eBool r2c);
uint32 ThaModuleHardSurHdlcChannelRxFcsErrorGet(ThaModuleHardSur self, AtChannel channel, eBool r2c);
uint32 ThaModuleHardSurHdlcChannelRxDiscardFramesGet(ThaModuleHardSur self, AtChannel channel, eBool r2c);
eAtRet ThaModuleHardSurHdlcChannelCounterLatch(ThaModuleHardSur self, AtChannel channel, eBool r2c);

/* Getting counters for PPP link */
uint32 ThaModuleHardSurPppLinkTxByteGet(ThaModuleHardSur self, AtChannel channel, eBool r2c);
uint32 ThaModuleHardSurPppLinkTxFragmentGet(ThaModuleHardSur self, AtChannel channel, eBool r2c);
uint32 ThaModuleHardSurPppLinkTxGoodPktGet(ThaModuleHardSur self, AtChannel channel, eBool r2c);
uint32 ThaModuleHardSurPppLinkTxDiscardPktGet(ThaModuleHardSur self, AtChannel channel, eBool r2c);
uint32 ThaModuleHardSurPppLinkTxDiscardByteGet(ThaModuleHardSur self, AtChannel channel, eBool r2c);
uint32 ThaModuleHardSurPppLinkRxFragmentGet(ThaModuleHardSur self, AtChannel channel, eBool r2c);
uint32 ThaModuleHardSurPppLinkRxByteGet(ThaModuleHardSur self, AtChannel channel, eBool r2c);
uint32 ThaModuleHardSurPppLinkRxGoodPktGet(ThaModuleHardSur self, AtChannel channel, eBool r2c);
uint32 ThaModuleHardSurPppLinkRxDiscardPktGet(ThaModuleHardSur self, AtChannel channel, eBool r2c);
uint32 ThaModuleHardSurPppLinkRxDiscardByteGet(ThaModuleHardSur self, AtChannel channel, eBool r2c);

/* Getting MP bundle counters */
uint32 ThaModuleHardSurMpBundleRxGoodByteGet(ThaModuleHardSur self, AtChannel channel, eBool r2c);
uint32 ThaModuleHardSurMpBundleRxGoodPktGet(ThaModuleHardSur self, AtChannel channel, eBool r2c);
uint32 ThaModuleHardSurMpBundleRxDiscardPktGet(ThaModuleHardSur self, AtChannel channel, eBool r2c);
uint32 ThaModuleHardSurMpBundleRxFragmentPktGet(ThaModuleHardSur self, AtChannel channel, eBool r2c);
uint32 ThaModuleHardSurMpBundleTxGoodByteGet(ThaModuleHardSur self, AtChannel channel, eBool r2c);
uint32 ThaModuleHardSurMpBundleTxGoodPktGet(ThaModuleHardSur self, AtChannel channel, eBool r2c);
uint32 ThaModuleHardSurMpBundleTxDiscardPktGet(ThaModuleHardSur self, AtChannel channel, eBool r2c);
uint32 ThaModuleHardSurMpBundleTxFragmentPktGet(ThaModuleHardSur self, AtChannel channel, eBool r2c);

/* Base address. */
uint32 ThaModuleHardSurBaseAddress(ThaModuleHardSur self);

/* OC/STM addresses. */
uint32 ThaModuleHardSurLineCurrentFailuresAddr(ThaModuleHardSur self);
uint32 ThaModuleHardSurLineFailuresInterruptAddr(ThaModuleHardSur self);
uint32 ThaModuleHardSurLineFailuresInterruptMaskAddr(ThaModuleHardSur self);
uint32 ThaModuleHardSurLineTcaInterruptAddr(ThaModuleHardSur self);
uint32 ThaModuleHardSurLineTcaInterruptMaskAddr(ThaModuleHardSur self);
uint32 ThaModuleHardSurLineThresholdRegister(ThaModuleHardSur self);
uint32 ThaModuleHardSurLineOffset(ThaModuleHardSur self, uint8 slice, uint8 lineInSlice);

/* STS/AU addresses. */
uint32 ThaModuleHardSurStsCurrentFailuresAddr(ThaModuleHardSur self);
uint32 ThaModuleHardSurStsFailuresInterruptAddr(ThaModuleHardSur self);
uint32 ThaModuleHardSurStsFailuresInterruptMaskAddr(ThaModuleHardSur self);
uint32 ThaModuleHardSurStsTcaInterruptAddr(ThaModuleHardSur self);
uint32 ThaModuleHardSurStsTcaInterruptMaskAddr(ThaModuleHardSur self);
uint32 ThaModuleHardSurStsThresholdRegister(ThaModuleHardSur self);
uint32 ThaModuleHardSurStsOffset(ThaModuleHardSur self, uint8 slice, uint8 sts);

/* VT/TU addresses. */
uint32 ThaModuleHardSurVtCurrentFailuresAddr(ThaModuleHardSur self);
uint32 ThaModuleHardSurVtFailuresInterruptAddr(ThaModuleHardSur self);
uint32 ThaModuleHardSurVtFailuresInterruptMaskAddr(ThaModuleHardSur self);
uint32 ThaModuleHardSurVtTcaInterruptAddr(ThaModuleHardSur self);
uint32 ThaModuleHardSurVtTcaInterruptMaskAddr(ThaModuleHardSur self);
uint32 ThaModuleHardSurVtThresholdRegister(ThaModuleHardSur self);
uint32 ThaModuleHardSurVtOffset(ThaModuleHardSur self, uint8 slice, uint8 sts, uint8 vtg, uint8 vt);

/* DS3/E3 addresses. */
uint32 ThaModuleHardSurDe3CurrentFailuresAddr(ThaModuleHardSur self);
uint32 ThaModuleHardSurDe3FailuresInterruptAddr(ThaModuleHardSur self);
uint32 ThaModuleHardSurDe3FailuresInterruptMaskAddr(ThaModuleHardSur self);
uint32 ThaModuleHardSurDe3TcaInterruptAddr(ThaModuleHardSur self);
uint32 ThaModuleHardSurDe3TcaInterruptMaskAddr(ThaModuleHardSur self);
uint32 ThaModuleHardSurDe3ThresholdRegister(ThaModuleHardSur self);
uint32 ThaModuleHardSurDe3Offset(ThaModuleHardSur self, uint8 slice, uint8 de3);

/* DS1/E1 addresses. */
uint32 ThaModuleHardSurDe1CurrentFailuresAddr(ThaModuleHardSur self);
uint32 ThaModuleHardSurDe1FailuresInterruptAddr(ThaModuleHardSur self);
uint32 ThaModuleHardSurDe1FailuresInterruptMaskAddr(ThaModuleHardSur self);
uint32 ThaModuleHardSurDe1TcaInterruptAddr(ThaModuleHardSur self);
uint32 ThaModuleHardSurDe1TcaInterruptMaskAddr(ThaModuleHardSur self);
uint32 ThaModuleHardSurDe1ThresholdRegister(ThaModuleHardSur self);
uint32 ThaModuleHardSurDe1Offset(ThaModuleHardSur self, uint8 slice, uint8 sts, uint8 vtg, uint8 vt);

/* PW addresses. */
uint32 ThaModuleHardSurPwCurrentFailuresAddr(ThaModuleHardSur self);
uint32 ThaModuleHardSurPwFailuresInterruptAddr(ThaModuleHardSur self);
uint32 ThaModuleHardSurPwFailuresInterruptMaskAddr(ThaModuleHardSur self);
uint32 ThaModuleHardSurPwTcaInterruptAddr(ThaModuleHardSur self);
uint32 ThaModuleHardSurPwTcaInterruptMaskAddr(ThaModuleHardSur self);
uint32 ThaModuleHardSurPwThresholdRegister(ThaModuleHardSur self);
uint32 ThaModuleHardSurPWFramerInterruptMASKPerPWBase(ThaModuleHardSur self);
uint32 ThaModuleHardSurFmPWInterruptMASKPerTypePerPWBase(ThaModuleHardSur self);

/* PM counter indirect address. */
uint32 ThaModuleHardSurLineDdrCountersAddress(AtSurEngine engine);
uint32 ThaModuleHardSurPathDdrCountersAddress(AtSurEngine engine);
uint32 ThaModuleHardSurDe3DdrCountersAddress(AtSurEngine engine);
uint32 ThaModuleHardSurDe1DdrCountersAddress(AtSurEngine engine);
uint32 ThaModuleHardSurPwDdrCountersAddress(AtSurEngine engine);

/* Enable/disable for debugging */
eBool ThaModuleHardSurIsOperating(ThaModuleHardSur self);

uint32 ThaModuleHardSurHdlcPwRxTotalByteGet(ThaModuleHardSur self, AtPw pw);
uint32 ThaModuleHardSurHdlcPwRxGoodPacketGet(ThaModuleHardSur self, AtPw pw);
uint32 ThaModuleHardSurHdlcPwRxDiscardPktGet(ThaModuleHardSur self, AtPw pw);
uint32 ThaModuleHardSurHdlcPwTxTotalByteGet(ThaModuleHardSur self, AtPw pw);
uint32 ThaModuleHardSurHdlcPwTxGoodPacketGet(ThaModuleHardSur self, AtPw pw);
eAtRet ThaModuleHardSurHdlcPwCounterLatch(ThaModuleHardSur self, AtPw pw, eBool clear);

uint32 ThaModuleHardSurEthFlowTxGoodPacketGet(ThaModuleHardSur self, AtEthFlow flow);
uint32 ThaModuleHardSurEthFlowTxTotalByteGet(ThaModuleHardSur self, AtEthFlow flow);
uint32 ThaModuleHardSurEthFlowRxDiscardPktGet(ThaModuleHardSur self, AtEthFlow flow);
uint32 ThaModuleHardSurEthFlowRxGoodPacketGet(ThaModuleHardSur self, AtEthFlow flow);
uint32 ThaModuleHardSurEthFlowRxTotalByteGet(ThaModuleHardSur self, AtEthFlow flow);
eAtRet ThaModuleHardSurEthFlowCounterLatch(ThaModuleHardSur self, AtEthFlow flow, eBool clear);

/* Supported features. */
eBool ThaModuleHardSurCanEnableHwPmEngine(ThaModuleHardSur self);
eBool ThaModuleHardSurFailureForwardingStatusIsSupported(ThaModuleHardSur self);
eBool ThaModuleHardSurFailureIsSupported(ThaModuleHardSur self);
eBool ThaModuleHardSurShouldUpdatePmRegisterWhenPotentialUasChange(ThaModuleHardSur self);

/* For HW cleanup */
void ThaModuleHardSurHwFlush(ThaModuleHardSur self);

/* Reset trigger address */
eAtRet ThaModuleHarSurResetForDdrOffLoad(ThaModuleHardSur self);

#endif /* _THAMODULEHARDSUR_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Surveillance
 *
 * File        : ThaHardPmRegister.c
 *
 * Created Date: Mar 19, 2015
 *
 * Description : Hardware PM Register Current Second implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaHardPmRegisterInternal.h"
#include "ThaHardPmParamInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaHardPmRegisterCurrentSecond
    {
    tThaHardPmRegister super;
    }tThaHardPmRegisterCurrentSecond;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtPmRegisterMethods m_AtPmRegisterOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool IsValid(AtPmRegister self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaHardPmRegisterCurrentSecond);
    }

static void OverrideAtPmRegister(AtPmRegister self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPmRegisterOverride, mMethodsGet(self), sizeof(m_AtPmRegisterOverride));

        mMethodOverride(m_AtPmRegisterOverride, IsValid);
        }

    mMethodsSet(self, &m_AtPmRegisterOverride);
    }

static void Override(AtPmRegister self)
    {
    OverrideAtPmRegister(self);
    }

static AtPmRegister ObjectInit(AtPmRegister self, AtPmParam param)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (ThaHardPmRegisterObjectInit(self, param, cAtPmCurrentSecondRegister) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPmRegister ThaHardPmRegisterCurrentSecondNew(AtPmParam param)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPmRegister newPmRegister = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPmRegister == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPmRegister, param);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Module Surveillance
 *
 * File        : ThaHardPmParam.c
 *
 * Created Date: Mar 21, 2015
 *
 * Description : Hardware PM param
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../../generic/sur/AtModuleSurInternal.h"
#include "../../../../../generic/sur/AtSurEngineInternal.h"
#include "ThaHardPmParamInternal.h"
#include "ThaHardPmRegisterInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cNumHoursInOneDay    24
#define cNumPeriodsInOneHour 4
#define cNumPeriodsInOneDay  cNumHoursInOneDay * cNumPeriodsInOneHour

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaHardPmParam)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtPmParamMethods m_AtPmParamOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void UpdatePreviousDay(AtPmParam self)
    {
    /* Update to previous day when reach full time of a day
     * Reset param for current day */
    if (mThis(self)->periodCounter >= cNumPeriodsInOneDay)
        {
        AtPmRegister dayReg    = AtPmParamCurrentDayRegister(self);
        AtPmRegister preDayReg = AtPmParamPreviousDayRegister(self);

        AtPmRegisterValueSet(preDayReg, AtPmRegisterValue(dayReg));
        AtPmRegisterValueSet(dayReg, 0);
        mThis(self)->periodCounter = 0;
        }
    }

static void UpdateCurrentDay(AtPmParam self)
    {
    uint32 numRecent = AtPmParamNumRecentPeriodRegisters(self);
    AtPmRegister dayRegister = AtPmParamCurrentDayRegister(self);
    AtPmRegister lastRecent = AtPmParamRecentRegister(self, (uint8)(numRecent - 1));

    AtPmRegisterValueSet(dayRegister, AtPmRegisterValue(dayRegister) + AtPmRegisterValue(lastRecent));
    mThis(self)->periodCounter++;
    UpdatePreviousDay(self);
    }

static uint8 NumRecentPeriodRegisters(AtPmParam self)
    {
    AtUnused(self);
    return 10;
    }

static eAtRet PeriodThresholdSet(AtPmParam self, uint32 threshold)
    {
    if (threshold == AtPmParamPeriodThresholdGet(self))
        return cAtOk;

    return cAtErrorModeNotSupport;
    }

static uint32 DayThresholdGet(AtPmParam self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet DayThresholdSet(AtPmParam self, uint32 threshold)
    {
    AtUnused(self);
    return (threshold == 0) ? cAtOk : cAtErrorModeNotSupport;
    }

static void RecentPeriodShiftLeft(AtPmParam self)
    {
    uint32 i;
    uint32 numRecentRegisters = AtPmParamNumRecentPeriodRegisters(self);

    UpdateCurrentDay(self);

    for (i = 0; i < numRecentRegisters; i++)
        {
        uint32 period_i = numRecentRegisters - i - 1;
        AtPmRegister reg1 = AtPmParamRecentRegister(self, (uint8)period_i);
        AtPmRegister reg2;

        if (period_i == 0)
            reg2 = AtPmParamPreviousPeriodRegister(self);
        else
            reg2 = AtPmParamRecentRegister(self, (uint8)(period_i - 1));

        AtPmRegisterValueSet(reg1, AtPmRegisterValue(reg2));
        }
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaHardPmParam);
    }

static void OverrideAtPmParam(AtPmParam self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPmParamOverride, mMethodsGet(self), sizeof(m_AtPmParamOverride));

        mMethodOverride(m_AtPmParamOverride, NumRecentPeriodRegisters);
        mMethodOverride(m_AtPmParamOverride, PeriodThresholdSet);
        mMethodOverride(m_AtPmParamOverride, DayThresholdSet);
        mMethodOverride(m_AtPmParamOverride, DayThresholdGet);
        mMethodOverride(m_AtPmParamOverride, RecentPeriodShiftLeft);
        }

    mMethodsSet(self, &m_AtPmParamOverride);
    }

static void Override(AtPmParam self)
    {
    OverrideAtPmParam(self);
    }

AtPmParam ThaHardPmParamObjectInit(AtPmParam self, AtSurEngine engine, const char *name, uint32 pmType)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (AtPmParamObjectInit(self, engine, name, pmType) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPmParam ThaHardPmParamNew(AtSurEngine engine, const char *name, uint32 pmType)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPmParam newPmParam = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPmParam == NULL)
        return NULL;

    /* Construct it */
    return ThaHardPmParamObjectInit(newPmParam, engine, name, pmType);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : ThaHardPmRegister.c
 *
 * Created Date: Sep 12, 2015
 *
 * Description : Hardware PM register
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaHardPmRegisterInternal.h"
#include "ThaHardPmParamInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)         ((tThaHardPmRegister * )self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtPmRegisterMethods m_AtPmRegisterOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool IsValid(AtPmRegister self)
    {
    return mThis(self)->isValid;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaHardPmRegister);
    }

static void OverrideAtPmRegister(AtPmRegister self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPmRegisterOverride, mMethodsGet(self), sizeof(m_AtPmRegisterOverride));

        mMethodOverride(m_AtPmRegisterOverride, IsValid);
        }

    mMethodsSet(self, &m_AtPmRegisterOverride);
    }

static void Override(AtPmRegister self)
    {
    OverrideAtPmRegister(self);
    }

AtPmRegister ThaHardPmRegisterObjectInit(AtPmRegister self, AtPmParam param, eAtPmRegisterType type)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (AtPmRegisterObjectInit(self, param) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Initialization. */
    self->type = type;
    mThis(self)->isValid = cAtTrue;

    return self;
    }

AtPmRegister ThaHardPmRegisterNew(AtPmParam param, eAtPmRegisterType type)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPmRegister newPmRegister = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPmRegister == NULL)
        return NULL;

    /* Construct it */
    return ThaHardPmRegisterObjectInit(newPmRegister, param, type);
    }

void ThaHardPmRegisterValidSet(ThaHardPmRegister self, eBool valid)
    {
    if (self)
        self->isValid = valid;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Surveillance
 * 
 * File        : ThaHardPmParamInternal.h
 * 
 * Created Date: Mar 19, 2015
 *
 * Description : Hardware PM PM param internal header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAHARDPMPARAMINTERNAL_H_
#define _THAHARDPMPARAMINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../../generic/sur/AtPmParamInternal.h"
#include "ThaHardPmParam.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaHardPmParam *ThaHardPmParam;

typedef struct tThaHardPmParam
    {
    tAtPmParam super;

    /* Private data */
    uint32 periodCounter;
    }tThaHardPmParam;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPmParam ThaHardPmParamObjectInit(AtPmParam self, AtSurEngine engine, const char *name, uint32 pmType);

#endif /* _THAHARDPMPARAMINTERNAL_H_ */


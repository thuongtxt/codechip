/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Surveillance
 * 
 * File        : ThaHardPmRegisterInternal.h
 * 
 * Created Date: Mar 19, 2015
 *
 * Description : Hardware PM Register internal header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAHARDPMREGISTERINTERNAL_H_
#define _THAHARDPMREGISTERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../../generic/sur/AtPmRegisterInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaHardPmRegister * ThaHardPmRegister;

typedef struct tThaHardPmRegister
    {
    tAtPmRegister super;

    /* Private data */
    eBool isValid;
    }tThaHardPmRegister;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPmRegister ThaHardPmRegisterObjectInit(AtPmRegister self, AtPmParam param, eAtPmRegisterType type);

/* Concretes */
AtPmRegister ThaHardPmRegisterNew(AtPmParam param, eAtPmRegisterType type);
AtPmRegister ThaHardPmRegisterCurrentSecondNew(AtPmParam param);

void ThaHardPmRegisterValidSet(ThaHardPmRegister self, eBool valid);

#endif /* _THAHARDPMREGISTERINTERNAL_H_ */


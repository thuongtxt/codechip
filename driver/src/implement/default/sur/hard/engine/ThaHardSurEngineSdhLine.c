/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : ThaHardSurEngineSdhLine.c
 *
 * Created Date: Aug 13, 2015
 *
 * Description : Line surveillance engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSdhLine.h"
#include "AtPmThresholdProfile.h"
#include "../../../../../generic/sdh/AtSdhChannelInternal.h"
#include "../../../ocn/ThaModuleOcn.h"
#include "../ThaModuleHardSurReg.h"
#include "../ThaModuleHardSur.h"
#include "../param/ThaHardPmRegisterInternal.h"
#include "ThaHardSurEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6_UAS_LNE_Index        6
#define cAf6_UAS_LFE_Index        7

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)     ((ThaHardSurEngineSdhLine)self)

#define mSuccessAssert(calling)                                                \
    do                                                                         \
        {                                                                      \
        eAtRet ret_ = calling;                                                 \
        if (ret_ != cAtOk)                                                     \
            return ret_;                                                       \
        }while(0)

#define mGetIntrBitMask(regVal_, alarmType_, enableMask_, type_, field_)       \
    (enableMask_) |= ((regVal_) & cAf6_FMPM_##type_##_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_##type_##_EC1STM0_STK_##field_##_Mask) ? alarmType_ : 0

#define mSetIntrBitMask(alarmType_, defectMask_, enableMask_, type_, field_)   \
    do                                                                         \
        {                                                                      \
        if (defectMask_ & alarmType_)                                          \
            regVal = (enableMask_ & alarmType_) ? \
                    (regVal | cAf6_FMPM_##type_##_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_##type_##_EC1STM0_##field_##_Mask) :\
                    (regVal & (~cAf6_FMPM_##type_##_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_##type_##_EC1STM0_##field_##_Mask)); \
        }while(0)

#define mGetFmIntrStatus(alarmType_, field_, status_, event_)                 \
    do {                                                                       \
        if (regVal & cAf6_FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_AMSK_##field_##_Mask) \
            {                                                                  \
            event_ |= alarmType_;                                              \
            if (regVal & cAf6_FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_CUR_##field_##_Mask) \
                status_ |= alarmType_;                                         \
            }                                                                  \
       } while (0)

#define mGetFmStatus(regVal_, field_)                                          \
    mRegField(regVal_, cAf6_FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_CUR_##field_##_)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSurEngineMethods       m_AtSurEngineOverride;

/* Save super implementation */
static const tAtSurEngineMethods *m_AtSurEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleHardSur Module(AtSurEngine self)
    {
    return (ThaModuleHardSur)AtSurEngineModuleGet(self);
    }

static eBool FailureIsSupported(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    if (module)
        return ThaModuleHardSurFailureIsSupported(module);
    return cAtFalse;
    }

static uint32 DefaultSesProfileId(AtSdhLine line)
    {
    switch (AtSdhLineRateGet(line))
        {
        case cAtSdhLineRateStm0:    return 0;
        case cAtSdhLineRateStm1:    return 1;
        case cAtSdhLineRateStm4:    return 2;
        case cAtSdhLineRateStm16:   return 3;
        case cAtSdhLineRateStm64:   return 4;
        case cAtSdhLineRateInvalid: return 0;
        default:                    return 0;
        }
    }

static AtPmSesThresholdProfile DefaultSesProfile(AtSurEngine self)
    {
    AtSdhLine line = (AtSdhLine)AtSurEngineChannelGet(self);
    return AtModuleSurSesThresholdProfileGet((AtModuleSur)Module(self), DefaultSesProfileId(line));
    }

static eAtRet Init(AtSurEngine self)
    {
    eAtRet ret = m_AtSurEngineMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    ret = AtSurEngineSesThresholdProfileSet(self, DefaultSesProfile(self));
    return ret;
    }

static uint32 HwPmEnableRegister(ThaModuleHardSur module)
    {
    return (uint32)(ThaModuleHardSurBaseAddress(module) + cAf6Reg_FMPM_Enable_PM_Collection_Section_Base);
    }

static eAtRet HwPmEnable(AtSurEngine self, eBool enable)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr = HwPmEnableRegister(module) + AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    mRegFieldSet(regVal, cAf6_FMPM_Enable_PM_Collection_Section_enb_col_sec_, mBoolToBin(enable));
    mModuleHwWrite(module, regAddr, regVal);
    return cAtOk;
    }

static eBool HwPmIsEnabled(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr = HwPmEnableRegister(module) + AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    return (regVal & cAf6_FMPM_Enable_PM_Collection_Section_enb_col_sec_Mask) ? cAtTrue : cAtFalse;
    }

static eAtRet PmEnable(AtSurEngine self, eBool enable)
    {
    if (ThaModuleHardSurCanEnableHwPmEngine(Module(self)))
        return HwPmEnable(self, enable);

    return enable ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool PmIsEnabled(AtSurEngine self)
    {
    if (ThaModuleHardSurCanEnableHwPmEngine(Module(self)))
        return HwPmIsEnabled(self);

    return cAtTrue;
    }

static eBool PmCanEnable(AtSurEngine self, eBool enable)
    {
    if (ThaModuleHardSurCanEnableHwPmEngine((Module(self))))
        return cAtTrue;

    return m_AtSurEngineMethods->PmCanEnable(self, enable);
    }

static eAtRet SesThresholdPoolEntrySet(AtSurEngine self, uint32 entryIndex)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr = ThaModuleHardSurLineThresholdRegister(module) +
                     AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    mRegFieldSet(regVal, cAf6_FMPM_Threshold_Type_of_Section_K_TYPE_, entryIndex);
    mModuleHwWrite(module, regAddr, regVal);

    return cAtOk;
    }

static uint32 SesThresholdPoolEntryGet(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr = ThaModuleHardSurLineThresholdRegister(module) +
                     AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    return mRegField(regVal, cAf6_FMPM_Threshold_Type_of_Section_K_TYPE_);
    }

static eAtRet TcaThresholdPoolEntrySet(AtSurEngine self, uint32 entryIndex)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr = ThaModuleHardSurLineThresholdRegister(module) +
                     AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    uint32 tcaTypeMask = mMethodsGet(module)->LineTcaTypeMask(module);
    uint32 tcaTypeShift = mMethodsGet(module)->LineTcaTypeShift(module);
    mRegFieldSet(regVal, tcaType, entryIndex);
    mModuleHwWrite(module, regAddr, regVal);

    return cAtOk;
    }

static uint32 TcaThresholdPoolEntryGet(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr = ThaModuleHardSurLineThresholdRegister(module) +
                     AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    uint32 tcaTypeMask = mMethodsGet(module)->LineTcaTypeMask(module);
    uint32 tcaTypeShift = mMethodsGet(module)->LineTcaTypeShift(module);
    return mRegField(regVal, tcaType);
    }

static uint32 TcaPeriodThresholdValueGet(AtSurEngine self, uint32 paramType)
    {
    AtPmThresholdProfile profile = AtSurEngineThresholdProfileGet(self);
    if (profile)
        return AtPmThresholdProfileSdhLinePeriodTcaThresholdGet(profile, paramType);
    return 0;
    }

static uint32 FailureMaskGet(uint32 regVal)
    {
    uint32 failures = 0;

    mGetIntrBitMask(regVal, cAtSdhLineAlarmLos,   failures, FM,   LOS_S);
    mGetIntrBitMask(regVal, cAtSdhLineAlarmLof,   failures, FM,   LOF_S);
    mGetIntrBitMask(regVal, cAtSdhLineAlarmTim,   failures, FM,   TIM_S);
    mGetIntrBitMask(regVal, cAtSdhLineAlarmAis,   failures, FM,   AIS_L);
    mGetIntrBitMask(regVal, cAtSdhLineAlarmRfi,   failures, FM,   RFI_L);
    mGetIntrBitMask(regVal, cAtSdhLineAlarmBerSd, failures, FM,   BERSD_L);
    mGetIntrBitMask(regVal, cAtSdhLineAlarmBerSf, failures, FM,   BERSF_L);

    return failures;
    }

static uint32 AutonomousFailuresGet(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    if (module && FailureIsSupported(self))
		{
    uint32 regAddr = ThaModuleHardSurLineCurrentFailuresAddr(module) + AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);

    return FailureMaskGet(regVal);
    }
    return 0;
    }

static uint32 NonAutonomousFailuresGet(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    if (module && FailureIsSupported(self))
		{
    uint32 regAddr;
    uint32 regVal;

    if (!ThaModuleHardSurFailureForwardingStatusIsSupported(module))
        return 0;

    regAddr = ThaModuleHardSurLineCurrentFailuresAddr(module) + AtSurEngineDefaultOffset(self);
    regVal = mModuleHwRead(module, regAddr);

    if (mGetFmStatus(regVal, HI_AIS_L))
        return cAtSdhLineAlarmAis;
		}
    return 0;
    }

static uint32 FailureRead2Clear(AtSurEngine self, eBool read2Clear)
    {
    ThaModuleHardSur module = Module(self);
    if (module && FailureIsSupported(self))
		{
    uint32 regAddr = ThaModuleHardSurLineFailuresInterruptAddr(module) + AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    uint32 failures = FailureMaskGet(regVal);

    if (read2Clear)
        mModuleHwWrite(module, regAddr, regVal);

    return failures;
    }
    return 0;
    }

static uint32 FailureChangedHistoryGet(AtSurEngine self)
    {
    return FailureRead2Clear(self, cAtFalse);
    }

static uint32 FailureChangedHistoryClear(AtSurEngine self)
    {
    return FailureRead2Clear(self, cAtTrue);
    }

static eAtRet FailureNotificationMaskSet(AtSurEngine self, uint32 failureMask, uint32 enableMask)
    {
    ThaModuleHardSur module = Module(self);
    if (module && FailureIsSupported(self))
		{
    uint32 regAddr = ThaModuleHardSurLineFailuresInterruptMaskAddr(module) + AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);

    mSetIntrBitMask(cAtSdhLineAlarmLos,    failureMask, enableMask, FM, LOS_S);
    mSetIntrBitMask(cAtSdhLineAlarmLof,    failureMask, enableMask, FM, LOF_S);
    mSetIntrBitMask(cAtSdhLineAlarmTim,    failureMask, enableMask, FM, TIM_S);
    mSetIntrBitMask(cAtSdhLineAlarmAis,    failureMask, enableMask, FM, AIS_L);
    mSetIntrBitMask(cAtSdhLineAlarmRfi,    failureMask, enableMask, FM, RFI_L);
    mSetIntrBitMask(cAtSdhLineAlarmBerSd,  failureMask, enableMask, FM, BERSD_L);
    mSetIntrBitMask(cAtSdhLineAlarmBerSf,  failureMask, enableMask, FM, BERSF_L);
    mModuleHwWrite(module, regAddr, regVal);

    if (regVal)
        return AtSurEngineFailureNotificationEnable(self, cAtTrue);

    return AtSurEngineFailureNotificationEnable(self, cAtFalse);
    }
    return cAtOk;
    }

static uint32 FailureNotificationMaskGet(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    if (module && FailureIsSupported(self))
		{
    uint32 regAddr = ThaModuleHardSurLineFailuresInterruptMaskAddr(module) + AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);

    return FailureMaskGet(regVal);
    }
    return 0;
    }

static uint32 TcaMaskGet(uint32 regVal)
    {
    uint32 tcaMask = 0;

    mGetIntrBitMask(regVal, cAtSurEngineSdhLinePmParamSefsS,  tcaMask, TCA, SEFS_S);
    mGetIntrBitMask(regVal, cAtSurEngineSdhLinePmParamCvS,    tcaMask, TCA, CV_S);
    mGetIntrBitMask(regVal, cAtSurEngineSdhLinePmParamEsS,    tcaMask, TCA, ES_S);
    mGetIntrBitMask(regVal, cAtSurEngineSdhLinePmParamSesS,   tcaMask, TCA, SES_S);
    mGetIntrBitMask(regVal, cAtSurEngineSdhLinePmParamCvL,    tcaMask, TCA, CV_L);
    mGetIntrBitMask(regVal, cAtSurEngineSdhLinePmParamEsL,    tcaMask, TCA, ES_L);
    mGetIntrBitMask(regVal, cAtSurEngineSdhLinePmParamSesL,   tcaMask, TCA, SES_L);
    mGetIntrBitMask(regVal, cAtSurEngineSdhLinePmParamUasL,   tcaMask, TCA, UAS_L);
    mGetIntrBitMask(regVal, cAtSurEngineSdhLinePmParamFcL,    tcaMask, TCA, FC_L);
    mGetIntrBitMask(regVal, cAtSurEngineSdhLinePmParamCvLfe,  tcaMask, TCA, CV_LFE);
    mGetIntrBitMask(regVal, cAtSurEngineSdhLinePmParamEsLfe,  tcaMask, TCA, ES_LFE);
    mGetIntrBitMask(regVal, cAtSurEngineSdhLinePmParamSesLfe, tcaMask, TCA, SES_LFE);
    mGetIntrBitMask(regVal, cAtSurEngineSdhLinePmParamUasLfe, tcaMask, TCA, UAS_LFE);
    mGetIntrBitMask(regVal, cAtSurEngineSdhLinePmParamFcLfe,  tcaMask, TCA, FC_LFE);

    return tcaMask;
    }

static eAtRet TcaNotificationMaskSet(AtSurEngine self, uint32 paramMask, uint32 enableMask)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr;
    uint32 regVal;

    if (!AtModuleSurTcaIsSupported((AtModuleSur)module))
        return cAtErrorModeNotSupport;

    regAddr = ThaModuleHardSurLineTcaInterruptMaskAddr(module) + AtSurEngineDefaultOffset(self);
    regVal = mModuleHwRead(module, regAddr);

    mSetIntrBitMask(cAtSurEngineSdhLinePmParamSefsS,  paramMask, enableMask, TCA, MSK_SEFS_S);
    mSetIntrBitMask(cAtSurEngineSdhLinePmParamCvS,    paramMask, enableMask, TCA, MSK_CV_S);
    mSetIntrBitMask(cAtSurEngineSdhLinePmParamEsS,    paramMask, enableMask, TCA, MSK_ES_S);
    mSetIntrBitMask(cAtSurEngineSdhLinePmParamSesS,   paramMask, enableMask, TCA, MSK_SES_S);
    mSetIntrBitMask(cAtSurEngineSdhLinePmParamCvL,    paramMask, enableMask, TCA, MSK_CV_L);
    mSetIntrBitMask(cAtSurEngineSdhLinePmParamEsL,    paramMask, enableMask, TCA, MSK_ES_L);
    mSetIntrBitMask(cAtSurEngineSdhLinePmParamSesL,   paramMask, enableMask, TCA, MSK_SES_L);
    mSetIntrBitMask(cAtSurEngineSdhLinePmParamUasL,   paramMask, enableMask, TCA, MSK_UAS_L);
    mSetIntrBitMask(cAtSurEngineSdhLinePmParamFcL,    paramMask, enableMask, TCA, MSK_FC_L);
    mSetIntrBitMask(cAtSurEngineSdhLinePmParamCvLfe,  paramMask, enableMask, TCA, MSK_CV_LFE);
    mSetIntrBitMask(cAtSurEngineSdhLinePmParamEsLfe,  paramMask, enableMask, TCA, MSK_ES_LFE);
    mSetIntrBitMask(cAtSurEngineSdhLinePmParamSesLfe, paramMask, enableMask, TCA, MSK_SES_LFE);
    mSetIntrBitMask(cAtSurEngineSdhLinePmParamUasLfe, paramMask, enableMask, TCA, MSK_UAS_LFE);
    mSetIntrBitMask(cAtSurEngineSdhLinePmParamFcLfe,  paramMask, enableMask, TCA, MSK_FC_LFE);

    mModuleHwWrite(module, regAddr, regVal);

    return AtSurEngineTcaNotificationEnable(self, regVal ? cAtTrue : cAtFalse);
    }

static uint32 TcaNotificationMaskGet(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr;
    uint32 regVal;

    if (!AtModuleSurTcaIsSupported((AtModuleSur)module))
        return 0;

    regAddr = ThaModuleHardSurLineTcaInterruptMaskAddr(module) + AtSurEngineDefaultOffset(self);
    regVal = mModuleHwRead(module, regAddr);
    return TcaMaskGet(regVal);
    }

static uint32 TcaChangedHistoryRead2Clear(AtSurEngine self, eBool r2c)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr;
    uint32 regVal;
    uint32 changedHistoryMask  = 0;

    if (!AtModuleSurTcaIsSupported((AtModuleSur)module))
        return 0;

    regAddr = ThaModuleHardSurLineTcaInterruptAddr(module) + AtSurEngineDefaultOffset(self);
    regVal = mModuleHwRead(module, regAddr);
    changedHistoryMask = TcaMaskGet(regVal);

    if (r2c)
        mModuleHwWrite(module, regAddr, regVal);

    return changedHistoryMask;
    }

static uint32 TcaChangedHistoryGet(AtSurEngine self)
    {
    return TcaChangedHistoryRead2Clear(self, cAtFalse);
    }

static uint32 TcaChangedHistoryClear(AtSurEngine self)
    {
    return TcaChangedHistoryRead2Clear(self, cAtTrue);
    }

#include "ThaHardSurEngineCommon.h"

static eAtRet SaveValid(AtSurEngine self, uint32 *regVal, AtPmRegister (*Register)(AtPmParam))
    {
    mRegistersSave(self, cAtSurEngineSdhLinePmParamCvLfe,    cAf6_Ec1_LFE_Invalid, LatchHwValid, Register);
    mRegistersSave(self, cAtSurEngineSdhLinePmParamEsLfe,    cAf6_Ec1_LFE_Invalid, LatchHwValid, Register);
    mRegistersSave(self, cAtSurEngineSdhLinePmParamSesLfe,   cAf6_Ec1_LFE_Invalid, LatchHwValid, Register);
    mRegistersSave(self, cAtSurEngineSdhLinePmParamUasLfe,   cAf6_Ec1_LFE_Invalid, LatchHwValid, Register);
    mRegistersSave(self, cAtSurEngineSdhLinePmParamFcLfe,    cAf6_Ec1_LFE_Invalid, LatchHwValid, Register);

    return cAtOk;
    }

/*
 * Latch PM counters from hardware to PM register.
 *
 * @param self          This engine
 * @param regVal        Buffers read from hardware
 * @param Register      PM register to latch to
 * @param LatchFunc     Latch function for normal PM parameters
 * @param NE_LatchFunc  Latch function for near-end PM CV/ES/SES/UAS parameters
 * @param FE_LatchFunc  Latch function for far-end PM CV/ES/SES/UAS parameters
 * @return AT return code
 */
static eAtRet RegistersLatch(AtSurEngine self, uint32 *regVal,
                             AtPmRegister (*Register)(AtPmParam),
                             AtPmRegisterHandler LatchFunc,
                             AtPmRegisterHandler NE_LatchFunc,
                             AtPmRegisterHandler FE_LatchFunc)
    {
    mRegistersSave(self, cAtSurEngineSdhLinePmParamCvS,      cAf6_Ec1_CV_S,    LatchFunc,    Register);
    mRegistersSave(self, cAtSurEngineSdhLinePmParamEsS,      cAf6_Ec1_ES_S,    LatchFunc,    Register);
    mRegistersSave(self, cAtSurEngineSdhLinePmParamSesS,     cAf6_Ec1_SES_S,   LatchFunc,    Register);
    mRegistersSave(self, cAtSurEngineSdhLinePmParamSefsS,    cAf6_Ec1_SEFS_S,  LatchFunc,    Register);
    mRegistersSave(self, cAtSurEngineSdhLinePmParamCvL,      cAf6_Ec1_CV_L,    NE_LatchFunc, Register);
    mRegistersSave(self, cAtSurEngineSdhLinePmParamEsL,      cAf6_Ec1_ES_L,    NE_LatchFunc, Register);
    mRegistersSave(self, cAtSurEngineSdhLinePmParamSesL,     cAf6_Ec1_SES_L,   NE_LatchFunc, Register);
    mRegistersSave(self, cAtSurEngineSdhLinePmParamUasL,     cAf6_Ec1_UAS_L,   NE_LatchFunc, Register);
    mRegistersSave(self, cAtSurEngineSdhLinePmParamFcL,      cAf6_Ec1_FC_L,    LatchFunc,    Register);
    mRegistersSave(self, cAtSurEngineSdhLinePmParamCvLfe,    cAf6_Ec1_CV_LFE,  FE_LatchFunc, Register);
    mRegistersSave(self, cAtSurEngineSdhLinePmParamEsLfe,    cAf6_Ec1_ES_LFE,  FE_LatchFunc, Register);
    mRegistersSave(self, cAtSurEngineSdhLinePmParamSesLfe,   cAf6_Ec1_SES_LFE, FE_LatchFunc, Register);
    mRegistersSave(self, cAtSurEngineSdhLinePmParamUasLfe,   cAf6_Ec1_UAS_LFE, FE_LatchFunc, Register);
    mRegistersSave(self, cAtSurEngineSdhLinePmParamFcLfe,    cAf6_Ec1_FC_LFE,  LatchFunc,    Register);

    /* Save valid indication. */
    SaveValid(self, regVal, Register);

    return cAtOk;
    }

/*
 * Latch PM counters from hardware to PM current register.
 *
 * @param self              This engine
 * @param regVal            Buffers read from hardware
 * @param Register          PM register to latch to
 * @param LatchFunc         Latch function for PM parameters
 * @return AT return code
 */
static eAtRet RegistersSave(AtSurEngine self, uint32 *regVal,
                            AtPmRegister (*Register)(AtPmParam),
                            AtPmRegisterHandler LatchFunc)
    {
    AtPmRegisterHandler NE_LatchFunc = LatchFunc;
    AtPmRegisterHandler FE_LatchFunc = LatchFunc;

    /* Some projects need to not latch CV/ES/SES/UAS value during the duration of
     * a potential UAS change. It is to avoid decremented PM values seen at the
     * end of potential UAS duration when application keeps polling PM value. */
    if (!ThaModuleHardSurShouldUpdatePmRegisterWhenPotentialUasChange(Module(self)))
        {
        if (IsPotentialUas(self, regVal[cAf6_UAS_LNE_Index]))
            NE_LatchFunc = DonotLatchHwValue;
        if (IsPotentialUas(self, regVal[cAf6_UAS_LFE_Index]))
            FE_LatchFunc = DonotLatchHwValue;
        }

    return RegistersLatch(self, regVal, Register, LatchFunc, NE_LatchFunc, FE_LatchFunc);
    }

static eAtRet NearEndParamsAdjust(AtSurEngine self, uint32 *regVal,
                                  AtPmRegister (*Register)(AtPmParam),
                                  AtPmRegisterHandler UasParamFunc,
                                  AtPmRegisterHandler SesParamFunc,
                                  AtPmRegisterHandler OtherParamsFunc)
    {
    mRegistersSave(self, cAtSurEngineSdhLinePmParamCvL, cAf6_Ec1_CV_L_Adj, OtherParamsFunc, Register);
    mRegistersSave(self, cAtSurEngineSdhLinePmParamEsL, cAf6_Ec1_ES_L_Adj, OtherParamsFunc, Register);
    mRegistersSave(self, cAtSurEngineSdhLinePmParamSesL, cAf6_Ec1_UAS_L_Adj, SesParamFunc, Register);
    mRegistersSave(self, cAtSurEngineSdhLinePmParamUasL, cAf6_Ec1_UAS_L_Adj, UasParamFunc, Register);

    return cAtOk;
    }

static eAtRet FarEndParamsAdjust(AtSurEngine self, uint32 *regVal,
                                 AtPmRegister (*Register)(AtPmParam),
                                 AtPmRegisterHandler UasParamFunc,
                                 AtPmRegisterHandler SesParamFunc,
                                 AtPmRegisterHandler OtherParamsFunc)
    {
    mRegistersSave(self, cAtSurEngineSdhLinePmParamCvLfe, cAf6_Ec1_CV_LFE_Adj, OtherParamsFunc, Register);
    mRegistersSave(self, cAtSurEngineSdhLinePmParamEsLfe, cAf6_Ec1_ES_LFE_Adj, OtherParamsFunc, Register);
    mRegistersSave(self, cAtSurEngineSdhLinePmParamSesLfe, cAf6_Ec1_UAS_LFE_Adj, SesParamFunc, Register);
    mRegistersSave(self, cAtSurEngineSdhLinePmParamUasLfe, cAf6_Ec1_UAS_LFE_Adj, UasParamFunc, Register);

    return cAtOk;
    }

static uint32 UasParamStateGet(AtSurEngine self, uint32 prevRegVal, uint32 currRegVal)
    {
    uint32 stkUas = mRegField(currRegVal, cAf6_UAS_Sticky_);
    uint32 curSes = mRegField(currRegVal, cAf6_UAS_Ses_T2_);
    uint32 prevUas = mRegField(prevRegVal, cAf6_UAS_State_);
    uint32 prevSes = mRegField(prevRegVal, cAf6_UAS_Ses_T1_);
    AtUnused(self);

    if (!stkUas && ((curSes + prevSes) == 10) && (prevSes != 0))
        return (prevUas) ? cThaHardSurUasParamExitting : cThaHardSurUasParamEntering;

    return cThaHardSurUasParamUnknown;
    }

/*
 * Latch & adjust PM counters from hardware to PM previous register.
 *
 * @param self              This engine
 * @param regVal            Buffers read from hardware
 * @param Register          PM register to latch to
 * @param LatchFunc         Latch function for PM parameters
 * @return AT return code
 */
static eAtRet RegistersAdjust(AtSurEngine self, uint32 *regVal,
                              AtPmRegister (*Register)(AtPmParam),
                              AtPmRegisterHandler LatchFunc)
    {
    uint32 latchedSesp = mRegField(regVal[cAf6_UAS_LNE_Index], cAf6_UAS_Ses_T1_);
    uint32 latchedSespfe = mRegField(regVal[cAf6_UAS_LFE_Index], cAf6_UAS_Ses_T1_);
    uint32 regCurrent[cNumDdrHoldRegisters];
    eAtRet ret;
    uint32 uasNeState, uasFeState;

    RegistersLatch(self, regVal, Register, LatchFunc, LatchFunc, LatchFunc);
    if (!latchedSesp && !latchedSespfe)
        return cAtOk;

    ret = ThaModuleHardSurLineDdrCountersR2c(self, cAtPmCurrentPeriodRegister, regCurrent, cAtFalse);
    if (ret != cAtOk)
        return ret;

    uasNeState = UasParamStateGet(self, regVal[cAf6_UAS_LNE_Index], regCurrent[cAf6_UAS_LNE_Index]);
    uasFeState = UasParamStateGet(self, regVal[cAf6_UAS_LFE_Index], regCurrent[cAf6_UAS_LFE_Index]);

    if (uasNeState == cThaHardSurUasParamEntering)
        NearEndParamsAdjust(self, regVal, Register, AddHwValue, SubstractHwValue, SubstractHwValue);
    else if (uasNeState == cThaHardSurUasParamExitting)
        NearEndParamsAdjust(self, regVal, Register, SubstractHwValue, KeepHwValue, AddHwValue);

    if (uasFeState == cThaHardSurUasParamEntering)
        FarEndParamsAdjust(self, regVal, Register, AddHwValue, SubstractHwValue, SubstractHwValue);
    else if (uasFeState == cThaHardSurUasParamExitting)
        FarEndParamsAdjust(self, regVal, Register, SubstractHwValue, KeepHwValue, AddHwValue);

    return cAtOk;
    }

static eAtRet PeriodExpire(AtSurEngine self, void *data)
    {
    uint32 *regVal = (uint32*)data;

    m_AtSurEngineMethods->PeriodExpire(self, data);

    return RegistersSave(self, regVal, AtPmParamPreviousPeriodRegister, PeriodShift);
    }

static eAtRet AllCountersLatchAndClear(AtSurEngine self, eBool clear)
    {
    uint32 regVal[cNumDdrHoldRegisters];
    eAtRet ret;

    ret = ThaModuleHardSurLineDdrCountersR2c(self, cAtPmCurrentPeriodRegister, regVal, clear);
    if (ret != cAtOk)
        return ret;

    return RegistersSave(self, regVal, AtPmParamCurrentPeriodRegister, LatchHwValue);
    }

static eAtRet RegistersLatchAndClear(AtSurEngine self, eAtPmRegisterType type, eBool clear)
    {
    uint32 regVal[cNumDdrHoldRegisters];
    eAtRet ret;

    ret = ThaModuleHardSurLineDdrCountersR2c(self, type, regVal, clear);
    if (ret != cAtOk)
        return ret;

    if (type == cAtPmCurrentPeriodRegister)
        return RegistersSave(self, regVal, AtPmParamCurrentPeriodRegister, LatchHwValue);

    if (type == cAtPmPreviousPeriodRegister)
        return RegistersAdjust(self, regVal, AtPmParamPreviousPeriodRegister, LatchHwValue);

    return cAtErrorModeNotSupport;
    }

static uint32 SavedValid(AtSurEngine self, uint32 paramType, AtPmRegister (*Register)(AtPmParam))
    {
    AtPmParam param = AtSurEnginePmParamGet(self, paramType);
    return AtPmRegisterIsValid(Register(param)) ? 0 : paramType;
    }

static eAtRet RegistersInvalidFlagsGet(AtSurEngine self, AtPmRegister (*Register)(AtPmParam))
    {
    uint32 flags = 0;
    flags |= SavedValid(self, cAtSurEngineSdhLinePmParamCvLfe, Register);
    flags |= SavedValid(self, cAtSurEngineSdhLinePmParamEsLfe, Register);
    flags |= SavedValid(self, cAtSurEngineSdhLinePmParamSesLfe, Register);
    flags |= SavedValid(self, cAtSurEngineSdhLinePmParamUasLfe, Register);
    flags |= SavedValid(self, cAtSurEngineSdhLinePmParamFcLfe, Register);

    return flags;
    }

static uint32 PmInvalidFlagsGet(AtSurEngine self, eAtPmRegisterType type)
    {
    if (type == cAtPmCurrentPeriodRegister)
        return RegistersInvalidFlagsGet(self, AtPmParamCurrentPeriodRegister);

    if (type == cAtPmPreviousPeriodRegister)
        return RegistersInvalidFlagsGet(self, AtPmParamPreviousPeriodRegister);

    return 0;
    }

static AtSdhLine LineGet(AtSurEngine self)
    {
    return (AtSdhLine)AtSurEngineChannelGet(self);
    }

static uint32 DefaultOffset(AtSurEngine self)
    {
    uint8 sliceId, hwLineInSlice;

    if (ThaSdhLineHwIdGet(LineGet(self), cAtModuleSur, &sliceId, &hwLineInSlice) != cAtOk)
        return 0;

    return ThaModuleHardSurLineOffset(Module(self), sliceId, hwLineInSlice);
    }

static eAtRet HwNotificationEnable(AtSurEngine self, uint32 baseAddr, eBool enable)
    {
    ThaModuleHardSur module = Module(self);

    if (module && FailureIsSupported(self))
        {
        uint8 sliceId, hwLineInSlice;
        uint32 bitMask;
        uint32 regVal;
        uint32 regAddr;
        eAtRet ret = ThaSdhLineHwIdGet(LineGet(self), cAtModuleSur, &sliceId, &hwLineInSlice);
        if (ret != cAtOk)
            return ret;

        regAddr = ThaModuleHardSurBaseAddress(module) + (uint32)(baseAddr + sliceId);
        bitMask = cBit0 << hwLineInSlice;

        regVal = mModuleHwRead(module, regAddr);
        regVal = (enable) ? (regVal | bitMask) : (regVal & ~bitMask);
        mModuleHwWrite(module, regAddr, regVal);
        }

    return cAtOk;
    }

static eBool HwNotificationIsEnabled(AtSurEngine self, uint32 baseAddr)
    {
    ThaModuleHardSur module = Module(self);
    if (module && FailureIsSupported(self))
		{
        uint8 sliceId, hwLineInSlice;
        uint32 bitMask;
        uint32 regVal;
        uint32 regAddr;

        if (ThaSdhLineHwIdGet(LineGet(self), cAtModuleSur, &sliceId, &hwLineInSlice) != cAtOk)
            return cAtFalse;

        regAddr = ThaModuleHardSurBaseAddress(module) + (uint32)(baseAddr + sliceId);
        bitMask = cBit0 << hwLineInSlice;

        regVal = mModuleHwRead(module, regAddr);
        return (regVal & bitMask) ? cAtTrue : cAtFalse;
        }
    return cAtFalse;
    }

static eAtRet FailureNotificationEnable(AtSurEngine self, eBool enable)
    {
    return HwNotificationEnable(self, cAf6Reg_FMPM_FM_EC1_Interrupt_MASK_Per_STS1_Base, enable);
    }

static eBool FailureNotificationIsEnabled(AtSurEngine self)
    {
    return HwNotificationIsEnabled(self, cAf6Reg_FMPM_FM_EC1_Interrupt_MASK_Per_STS1_Base);
    }

static eAtRet TcaNotificationEnable(AtSurEngine self, eBool enable)
    {
    return HwNotificationEnable(self, cAf6Reg_FMPM_TCA_EC1_Interrupt_MASK_Per_STS1_Base, enable);
    }

static eBool TcaNotificationIsEnabled(AtSurEngine self)
    {
    return HwNotificationIsEnabled(self, cAf6Reg_FMPM_TCA_EC1_Interrupt_MASK_Per_STS1_Base);
    }

static void FailureNotificationProcess(AtSurEngine self, uint32 offset)
    {
    ThaModuleHardSur module = Module(self);
    if (module && FailureIsSupported(self))
		{
    uint32 regAddr = ThaModuleHardSurLineCurrentFailuresAddr(module) + offset;
    uint32 regVal = mModuleHwRead(module, regAddr);
    uint32 events = 0;
    uint32 failures = 0;
    uint32 currentFailures = FailureMaskGet(regVal);

    mGetFmIntrStatus(cAtSdhLineAlarmLos,    LOS_S,  failures, events);
    mGetFmIntrStatus(cAtSdhLineAlarmLof,    LOF_S,  failures, events);
    mGetFmIntrStatus(cAtSdhLineAlarmTim,    TIM_S,  failures, events);
    mGetFmIntrStatus(cAtSdhLineAlarmAis,    AIS_L,  failures, events);
    mGetFmIntrStatus(cAtSdhLineAlarmRfi,    RFI_L,  failures, events);
    mGetFmIntrStatus(cAtSdhLineAlarmBerSd,  BERSD_L,failures, events);
    mGetFmIntrStatus(cAtSdhLineAlarmBerSf,  BERSF_L,failures, events);

    /* Only clear masked interrupt. */
    regVal = (regVal >> 16) & cBit15_0;

    /* Clear interrupt */
    mModuleHwWrite(module, ThaModuleHardSurLineFailuresInterruptAddr(module) + offset, regVal);

    AtSurEngineFailureNotify(self, events, failures);

    AtSdhChannelFailureForwardingClearanceNotifyWithSubChannelOnly((AtSdhChannel)AtSurEngineChannelGet(self), currentFailures, cAtTrue);
    }
    }

static void TcaNotificationProcess(AtSurEngine self, uint32 offset)
    {
    ThaModuleHardSur module = Module(self);
    uint32 intrAddr = ThaModuleHardSurLineTcaInterruptAddr(module) + offset;
    uint32 maskAddr = ThaModuleHardSurLineTcaInterruptMaskAddr(module) + offset;
    uint32 intrVal = mModuleHwRead(module, intrAddr);
    uint32 maskVal = mModuleHwRead(module, maskAddr);
    uint32 params = 0;

    /* Filter unmasked stickies. */
    intrVal &= maskVal;
    params = TcaMaskGet(intrVal);

    /* Clear interrupt */
    mModuleHwWrite(module, intrAddr, intrVal);

    AtSurEngineAllParamsPeriodTcaNotify(self, params);
    }

static void OverrideAtSurEngine(AtSurEngine self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSurEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSurEngineOverride, m_AtSurEngineMethods, sizeof(m_AtSurEngineOverride));

        mMethodOverride(m_AtSurEngineOverride, Init);
        mMethodOverride(m_AtSurEngineOverride, PmEnable);
        mMethodOverride(m_AtSurEngineOverride, PmIsEnabled);
        mMethodOverride(m_AtSurEngineOverride, PmCanEnable);
        mMethodOverride(m_AtSurEngineOverride, AutonomousFailuresGet);
        mMethodOverride(m_AtSurEngineOverride, NonAutonomousFailuresGet);
        mMethodOverride(m_AtSurEngineOverride, FailureChangedHistoryGet);
        mMethodOverride(m_AtSurEngineOverride, FailureChangedHistoryClear);
        mMethodOverride(m_AtSurEngineOverride, FailureNotificationMaskSet);
        mMethodOverride(m_AtSurEngineOverride, FailureNotificationMaskGet);
        mMethodOverride(m_AtSurEngineOverride, TcaChangedHistoryGet);
        mMethodOverride(m_AtSurEngineOverride, TcaChangedHistoryClear);
        mMethodOverride(m_AtSurEngineOverride, TcaNotificationMaskSet);
        mMethodOverride(m_AtSurEngineOverride, TcaNotificationMaskGet);
        mMethodOverride(m_AtSurEngineOverride, SesThresholdPoolEntrySet);
        mMethodOverride(m_AtSurEngineOverride, SesThresholdPoolEntryGet);
        mMethodOverride(m_AtSurEngineOverride, TcaThresholdPoolEntrySet);
        mMethodOverride(m_AtSurEngineOverride, TcaThresholdPoolEntryGet);
        mMethodOverride(m_AtSurEngineOverride, TcaPeriodThresholdValueGet);
        mMethodOverride(m_AtSurEngineOverride, AllCountersLatchAndClear);
        mMethodOverride(m_AtSurEngineOverride, RegistersLatchAndClear);
        mMethodOverride(m_AtSurEngineOverride, PeriodExpire);
        mMethodOverride(m_AtSurEngineOverride, DefaultOffset);
        mMethodOverride(m_AtSurEngineOverride, FailureNotificationEnable);
        mMethodOverride(m_AtSurEngineOverride, FailureNotificationIsEnabled);
        mMethodOverride(m_AtSurEngineOverride, FailureNotificationProcess);
        mMethodOverride(m_AtSurEngineOverride, TcaNotificationEnable);
        mMethodOverride(m_AtSurEngineOverride, TcaNotificationIsEnabled);
        mMethodOverride(m_AtSurEngineOverride, TcaNotificationProcess);
        mMethodOverride(m_AtSurEngineOverride, PmInvalidFlagsGet);
        mMethodOverride(m_AtSurEngineOverride, SesThresholdProfileGet);
        }

    mMethodsSet(self, &m_AtSurEngineOverride);
    }

static void Override(AtSurEngine self)
    {
    OverrideAtSurEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaHardSurEngineSdhLine);
    }

AtSurEngine ThaHardSurEngineSdhLineObjectInit(AtSurEngine self, AtModuleSur module, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (AtSurEngineSdhLineObjectInit(self, module, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSurEngine ThaHardSurEngineSdhLineNew(AtModuleSur module, AtChannel channel)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtSurEngine newSurEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newSurEngine == NULL)
        return NULL;
    return ThaHardSurEngineSdhLineObjectInit(newSurEngine, module, channel);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : ThaHardSurEnginePdhDe1.c
 *
 * Created Date: Mar 21, 2015
 *
 * Description : DS1/E1 Surveillance engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPdhDe1.h"
#include "../../../../../generic/pdh/AtPdhChannelInternal.h"
#include "../../../pdh/ThaModulePdh.h"
#include "../ThaModuleHardSurReg.h"
#include "../param/ThaHardPmRegisterInternal.h"
#include "AtPmThresholdProfile.h"
#include "ThaHardSurEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6_UAS_PNE_Index       11
#define cAf6_UAS_PFE_Index       12

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaHardSurEnginePdhDe1)self)

#define mGetIntrBitMask(regVal_, alarmType_, enableMask_, type_, field_)       \
    (enableMask_) |= ((regVal_) & cAf6_FMPM_##type_##_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_##type_##_DS1E1_STK_##field_##_Mask) ? alarmType_ : 0

#define mSetIntrBitMask(alarmType_, defectMask_, enableMask_, type_, field_)   \
    do                                                                         \
        {                                                                      \
        if (defectMask_ & alarmType_)                                          \
            regVal = (enableMask_ & alarmType_) ? \
                    (regVal | cAf6_FMPM_##type_##_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_##type_##_DS1E1_MSK_##field_##_Mask) :\
                    (regVal & (~cAf6_FMPM_##type_##_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_##type_##_DS1E1_MSK_##field_##_Mask)); \
        }while(0)

#define mGetFmIntrStatus(alarmType_, field_, status_, event_)                  \
    do {                                                                       \
        if (regVal & cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_##field_##_Mask) \
            {                                                                  \
            event_ |= alarmType_;                                              \
            if (regVal & cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_##field_##_Mask) \
                status_ |= alarmType_;                                         \
            }                                                                  \
       } while (0)

#define mGetFmStatus(regVal_, field_)                                          \
    mRegField(regVal_, cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_##field_##_)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSurEngineMethods m_AtSurEngineOverride;

/* Save super implementation */
static const tAtSurEngineMethods *m_AtSurEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleHardSur Module(AtSurEngine self)
    {
    return (ThaModuleHardSur)AtSurEngineModuleGet(self);
    }

static eBool FailureIsSupported(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    return ThaModuleHardSurFailureIsSupported(module);
    }

static uint32 HwPmEnableRegister(ThaModuleHardSur module)
    {
    return (uint32)(ThaModuleHardSurBaseAddress(module) + cAf6Reg_FMPM_Enable_PM_Collection_DS1_Base);
    }

static eAtRet HwPmEnable(AtSurEngine self, eBool enable)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr = HwPmEnableRegister(module) + AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    mRegFieldSet(regVal, cAf6_FMPM_Enable_PM_Collection_DS1_enb_col_ds1_, mBoolToBin(enable));
    mModuleHwWrite(module, regAddr, regVal);
    return cAtOk;
    }

static eBool HwPmIsEnabled(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr = HwPmEnableRegister(module) + AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    return (regVal & cAf6_FMPM_Enable_PM_Collection_DS1_enb_col_ds1_Mask) ? cAtTrue : cAtFalse;
    }

static eAtRet PmEnable(AtSurEngine self, eBool enable)
    {
    if (ThaModuleHardSurCanEnableHwPmEngine(Module(self)))
        return HwPmEnable(self, enable);

    return enable ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool PmIsEnabled(AtSurEngine self)
    {
    if (ThaModuleHardSurCanEnableHwPmEngine(Module(self)))
        return HwPmIsEnabled(self);

    return cAtTrue;
    }

static eBool PmCanEnable(AtSurEngine self, eBool enable)
    {
    if (ThaModuleHardSurCanEnableHwPmEngine((Module(self))))
        return cAtTrue;

    return m_AtSurEngineMethods->PmCanEnable(self, enable);
    }

static uint32 ThresholdRegister(AtSurEngine self)
    {
    return ThaModuleHardSurDe1ThresholdRegister(Module(self)) + AtSurEngineDefaultOffset(self);
    }

static eAtRet SesThresholdPoolEntrySet(AtSurEngine self, uint32 entryIndex)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr = ThresholdRegister(self);
    uint32 regVal;

    regVal = mModuleHwRead(module, regAddr);
    mRegFieldSet(regVal, cAf6_FMPM_Threshold_Type_of_DS1_K_TYPE_, entryIndex);
    mModuleHwWrite(module, regAddr, regVal);

    return cAtOk;
    }

static uint32 SesThresholdPoolEntryGet(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr = ThresholdRegister(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    return mRegField(regVal, cAf6_FMPM_Threshold_Type_of_DS1_K_TYPE_);
    }

static eAtRet TcaThresholdPoolEntrySet(AtSurEngine self, uint32 entryIndex)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr = ThresholdRegister(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    uint32 tcaTypeMask = mMethodsGet(module)->De1TcaTypeMask(module);
    uint32 tcaTypeShift = mMethodsGet(module)->De1TcaTypeShift(module);
    mRegFieldSet(regVal, tcaType, entryIndex);
    mModuleHwWrite(module, regAddr, regVal);

    return cAtOk;
    }

static uint32 TcaThresholdPoolEntryGet(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr = ThresholdRegister(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    uint32 tcaTypeMask = mMethodsGet(module)->De1TcaTypeMask(module);
    uint32 tcaTypeShift = mMethodsGet(module)->De1TcaTypeShift(module);
    return mRegField(regVal, tcaType);
    }

static uint32 TcaPeriodThresholdValueGet(AtSurEngine self, uint32 paramType)
    {
    AtPmThresholdProfile profile = AtSurEngineThresholdProfileGet(self);
    if (profile)
        return AtPmThresholdProfilePdhDe1PeriodTcaThresholdGet(profile, paramType);
    return 0;
    }

static uint32 FailureMaskGet(uint32 regVal)
    {
    uint32 failures = 0;

    mGetIntrBitMask(regVal, cAtPdhDe1AlarmAisCi, failures, FM, AIS_CI);
    mGetIntrBitMask(regVal, cAtPdhDe1AlarmRaiCi, failures, FM, RAI_CI);
    mGetIntrBitMask(regVal, cAtPdhDe1AlarmAis,   failures, FM, AIS);
    mGetIntrBitMask(regVal, cAtPdhDe1AlarmLos,   failures, FM, LOS);
    mGetIntrBitMask(regVal, cAtPdhDe1AlarmLof,   failures, FM, LOF);
    mGetIntrBitMask(regVal, cAtPdhDe1AlarmRai,   failures, FM, RAI);

    return failures;
    }

static uint32 AutonomousFailuresGet(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
	if (module && FailureIsSupported(self))
		{
		uint32 regAddr = ThaModuleHardSurDe1CurrentFailuresAddr(module) + AtSurEngineDefaultOffset(self);
		uint32 regVal = mModuleHwRead(module, regAddr);
		return FailureMaskGet(regVal);
		}
	return 0;
    }

static uint32 NonAutonomousFailuresGet(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    if (module && FailureIsSupported(self))
		{
		uint32 regAddr, regVal;

		if (!ThaModuleHardSurFailureForwardingStatusIsSupported(module))
			return 0;

		regAddr = ThaModuleHardSurDe1CurrentFailuresAddr(module) + AtSurEngineDefaultOffset(self);
		regVal = mModuleHwRead(module, regAddr);
		if (mGetFmStatus(regVal, HI_AIS))
			return cAtPdhDe1AlarmAis;
		}
    return 0;
    }

static uint32 FailureRead2Clear(AtSurEngine self, eBool read2Clear)
    {
    ThaModuleHardSur module = Module(self);
    if (module && FailureIsSupported(self))
		{
		uint32 regAddr = ThaModuleHardSurDe1FailuresInterruptAddr(module) + AtSurEngineDefaultOffset(self);
		uint32 regVal = mModuleHwRead(module, regAddr);
		uint32 failures = FailureMaskGet(regVal);

		if (read2Clear)
			mModuleHwWrite(module, regAddr, regVal);

    	return failures;
		}
    return 0;
    }

static uint32 FailureChangedHistoryGet(AtSurEngine self)
    {
    return FailureRead2Clear(self, cAtFalse);
    }

static uint32 FailureChangedHistoryClear(AtSurEngine self)
    {
    return FailureRead2Clear(self, cAtTrue);
    }

static eAtRet FailureNotificationMaskSet(AtSurEngine self, uint32 failureMask, uint32 enableMask)
    {
    ThaModuleHardSur module = Module(self);
    if (module && FailureIsSupported(self))
		{
		uint32 regAddr = ThaModuleHardSurDe1FailuresInterruptMaskAddr(module) + AtSurEngineDefaultOffset(self);
		uint32 regVal = mModuleHwRead(module, regAddr);

		mSetIntrBitMask(cAtPdhDe1AlarmAis,   failureMask, enableMask, FM, AIS);
		mSetIntrBitMask(cAtPdhDe1AlarmLos,   failureMask, enableMask, FM, LOS);
		mSetIntrBitMask(cAtPdhDe1AlarmLof,   failureMask, enableMask, FM, LOF);
		mSetIntrBitMask(cAtPdhDe1AlarmRai,   failureMask, enableMask, FM, RAI);
		mSetIntrBitMask(cAtPdhDe1AlarmRaiCi, failureMask, enableMask, FM, RAI_CI);
		mSetIntrBitMask(cAtPdhDe1AlarmAisCi, failureMask, enableMask, FM, AIS_CI);
		mModuleHwWrite(module, regAddr, regVal);

		if (regVal)
			return AtSurEngineFailureNotificationEnable(self, cAtTrue);
		else
			return AtSurEngineFailureNotificationEnable(self, cAtFalse);
		}
    return cAtOk;
    }

static uint32 FailureNotificationMaskGet(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    if (module && FailureIsSupported(self))
		{
		uint32 regAddr = ThaModuleHardSurDe1FailuresInterruptMaskAddr(module) + AtSurEngineDefaultOffset(self);
		uint32 regVal = mModuleHwRead(module, regAddr);
		return FailureMaskGet(regVal);
		}
    return 0;
    }

static uint32 TcaMaskGet(uint32 regVal)
    {
    uint32 tcaMask = 0;

    mGetIntrBitMask(regVal, cAtSurEnginePdhDe1PmParamCvL,     tcaMask, TCA, CV_L);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe1PmParamEsL,     tcaMask, TCA, ES_L);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe1PmParamSesL,    tcaMask, TCA, SES_L);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe1PmParamLossL,   tcaMask, TCA, LOSS_L);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe1PmParamEsLfe,   tcaMask, TCA, ES_LFE);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe1PmParamCvP,     tcaMask, TCA, CV_P);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe1PmParamEsP,     tcaMask, TCA, ES_P);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe1PmParamSesP,    tcaMask, TCA, SES_P);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe1PmParamAissP,   tcaMask, TCA, AISS_P);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe1PmParamSasP,    tcaMask, TCA, SAS_P);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe1PmParamCssP,    tcaMask, TCA, CSS_P);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe1PmParamUasP,    tcaMask, TCA, UAS_P);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe1PmParamSefsPfe, tcaMask, TCA, SEFS_PFE);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe1PmParamEsPfe,   tcaMask, TCA, ES_PFE);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe1PmParamSesPfe,  tcaMask, TCA, SES_PFE);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe1PmParamCssPfe,  tcaMask, TCA, CSS_PFE);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe1PmParamFcPfe,   tcaMask, TCA, FC_PFE);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe1PmParamUasPfe,  tcaMask, TCA, UAS_PFE);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe1PmParamFcP,     tcaMask, TCA, FC_P);

    return tcaMask;
    }

static eBool HasFarEndPerformance(AtSurEngine self)
    {
    AtPdhDe1 de1 = (AtPdhDe1)AtSurEngineChannelGet(self);
    return AtPdhDe1HasFarEndPerformance(de1, AtPdhChannelFrameTypeGet((AtPdhChannel)de1));
    }

static uint32 TcaNotApplicableMaskFilterOut(AtSurEngine self, uint32 tcaMask)
    {
    if (HasFarEndPerformance(self))
        return tcaMask;

    return (tcaMask & (~AtSurEngineDe1FarEndParams()));
    }

static uint32 TcaInterruptMaskAddress(AtSurEngine self)
    {
    return ThaModuleHardSurDe1TcaInterruptMaskAddr(Module(self)) + AtSurEngineDefaultOffset(self);
    }

static eAtRet TcaNotificationMaskReset(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    mModuleHwWrite(module, TcaInterruptMaskAddress(self), 0);
    return cAtOk;
    }

static eAtRet TcaNotificationMaskSet(AtSurEngine self, uint32 paramMask, uint32 enableMask)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr;
    uint32 regVal;

    if (!AtModuleSurTcaIsSupported((AtModuleSur)module))
        return cAtErrorModeNotSupport;

    paramMask = TcaNotApplicableMaskFilterOut(self, paramMask);
    regAddr = TcaInterruptMaskAddress(self);
    regVal = mModuleHwRead(module, regAddr);

    mSetIntrBitMask(cAtSurEnginePdhDe1PmParamCvL,     paramMask, enableMask, TCA, CV_L);
    mSetIntrBitMask(cAtSurEnginePdhDe1PmParamEsL,     paramMask, enableMask, TCA, ES_L);
    mSetIntrBitMask(cAtSurEnginePdhDe1PmParamSesL,    paramMask, enableMask, TCA, SES_L);
    mSetIntrBitMask(cAtSurEnginePdhDe1PmParamLossL,   paramMask, enableMask, TCA, LOSS_L);
    mSetIntrBitMask(cAtSurEnginePdhDe1PmParamEsLfe,   paramMask, enableMask, TCA, ES_LFE);
    mSetIntrBitMask(cAtSurEnginePdhDe1PmParamCvP,     paramMask, enableMask, TCA, CV_P);
    mSetIntrBitMask(cAtSurEnginePdhDe1PmParamEsP,     paramMask, enableMask, TCA, ES_P);
    mSetIntrBitMask(cAtSurEnginePdhDe1PmParamSesP,    paramMask, enableMask, TCA, SES_P);
    mSetIntrBitMask(cAtSurEnginePdhDe1PmParamAissP,   paramMask, enableMask, TCA, AISS_P);
    mSetIntrBitMask(cAtSurEnginePdhDe1PmParamSasP,    paramMask, enableMask, TCA, SAS_P);
    mSetIntrBitMask(cAtSurEnginePdhDe1PmParamCssP,    paramMask, enableMask, TCA, CSS_P);
    mSetIntrBitMask(cAtSurEnginePdhDe1PmParamUasP,    paramMask, enableMask, TCA, UAS_P);
    mSetIntrBitMask(cAtSurEnginePdhDe1PmParamSefsPfe, paramMask, enableMask, TCA, SEFS_PFE);
    mSetIntrBitMask(cAtSurEnginePdhDe1PmParamEsPfe,   paramMask, enableMask, TCA, ES_PFE);
    mSetIntrBitMask(cAtSurEnginePdhDe1PmParamSesPfe,  paramMask, enableMask, TCA, SES_PFE);
    mSetIntrBitMask(cAtSurEnginePdhDe1PmParamCssPfe,  paramMask, enableMask, TCA, CSS_PFE);
    mSetIntrBitMask(cAtSurEnginePdhDe1PmParamFcPfe,   paramMask, enableMask, TCA, FC_PFE);
    mSetIntrBitMask(cAtSurEnginePdhDe1PmParamUasPfe,  paramMask, enableMask, TCA, UAS_PFE);
    mSetIntrBitMask(cAtSurEnginePdhDe1PmParamFcP,     paramMask, enableMask, TCA, FC_P);
    mModuleHwWrite(module, regAddr, regVal);

    return AtSurEngineTcaNotificationEnable(self, regVal ? cAtTrue : cAtFalse);
    }

static uint32 TcaNotificationMaskGet(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr;
    uint32 regVal;

    if (!AtModuleSurTcaIsSupported((AtModuleSur)module))
        return 0;

    regAddr = ThaModuleHardSurDe1TcaInterruptMaskAddr(module) + AtSurEngineDefaultOffset(self);
    regVal = mModuleHwRead(module, regAddr);
    return TcaMaskGet(regVal);
    }

static uint32 TcaChangedHistoryRead2Clear(AtSurEngine self, eBool r2c)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr;
    uint32 regVal;
    uint32 changedHistoryMask  = 0;

    if (!AtModuleSurTcaIsSupported((AtModuleSur)module))
        return 0;

    regAddr = ThaModuleHardSurDe1TcaInterruptAddr(module) + AtSurEngineDefaultOffset(self);
    regVal = mModuleHwRead(module, regAddr);
    changedHistoryMask = TcaNotApplicableMaskFilterOut(self, TcaMaskGet(regVal));

    if (r2c)
        mModuleHwWrite(module, regAddr, regVal);

    return changedHistoryMask;
    }

static uint32 TcaChangedHistoryGet(AtSurEngine self)
    {
    return TcaChangedHistoryRead2Clear(self, cAtFalse);
    }

static uint32 TcaChangedHistoryClear(AtSurEngine self)
    {
    return TcaChangedHistoryRead2Clear(self, cAtTrue);
    }

#include "ThaHardSurEngineCommon.h"

static eAtRet SaveValid(AtSurEngine self, uint32 *regVal, AtPmRegister (*Register)(AtPmParam))
    {
    mRegistersSave(self, cAtSurEnginePdhDe1PmParamEsLfe,    cAf6_Ds1_LFE_Invalid, LatchHwValid, Register);
    mRegistersSave(self, cAtSurEnginePdhDe1PmParamSefsPfe,    cAf6_Ds1_PFE_Invalid, LatchHwValid, Register);
    mRegistersSave(self, cAtSurEnginePdhDe1PmParamEsPfe,   cAf6_Ds1_PFE_Invalid, LatchHwValid, Register);
    mRegistersSave(self, cAtSurEnginePdhDe1PmParamSesPfe,   cAf6_Ds1_PFE_Invalid, LatchHwValid, Register);
    mRegistersSave(self, cAtSurEnginePdhDe1PmParamCssPfe,    cAf6_Ds1_PFE_Invalid, LatchHwValid, Register);
    mRegistersSave(self, cAtSurEnginePdhDe1PmParamFcPfe,    cAf6_Ds1_PFE_Invalid, LatchHwValid, Register);
    mRegistersSave(self, cAtSurEnginePdhDe1PmParamUasPfe,    cAf6_Ds1_PFE_Invalid, LatchHwValid, Register);

    return cAtOk;
    }

/*
 * Latch PM counters from hardware to PM register.
 *
 * @param self          This engine
 * @param regVal        Buffers read from hardware
 * @param Register      PM register to latch to
 * @param LatchFunc     Latch function for normal PM parameters
 * @param NE_LatchFunc  Latch function for near-end PM CV/ES/SES/UAS parameters
 * @param FE_LatchFunc  Latch function for far-end PM CV/ES/SES/UAS parameters
 * @return AT return code
 */
static eAtRet RegistersLatch(AtSurEngine self, uint32 *regVal,
                             AtPmRegister (*Register)(AtPmParam),
                             AtPmRegisterHandler LatchFunc,
                             AtPmRegisterHandler NE_LatchFunc,
                             AtPmRegisterHandler FE_LatchFunc)
    {
    mRegistersSave(self, cAtSurEnginePdhDe1PmParamCvL,   cAf6_Ds1_CV_L,   LatchFunc,    Register);
    mRegistersSave(self, cAtSurEnginePdhDe1PmParamEsL,   cAf6_Ds1_ES_L,   LatchFunc,    Register);
    mRegistersSave(self, cAtSurEnginePdhDe1PmParamSesL,  cAf6_Ds1_SES_L,  LatchFunc,    Register);
    mRegistersSave(self, cAtSurEnginePdhDe1PmParamLossL, cAf6_Ds1_LOSS_L, LatchFunc,    Register);
    mRegistersSave(self, cAtSurEnginePdhDe1PmParamEsLfe, cAf6_Ds1_ES_LFE, LatchFunc,    Register);
    mRegistersSave(self, cAtSurEnginePdhDe1PmParamCvP,   cAf6_Ds1_CV_P,   NE_LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe1PmParamEsP,   cAf6_Ds1_ES_P,   NE_LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe1PmParamSesP,  cAf6_Ds1_SES_P,  NE_LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe1PmParamAissP, cAf6_Ds1_AISS_P, NE_LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe1PmParamSasP,  cAf6_Ds1_SAS_P,  NE_LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe1PmParamCssP,  cAf6_Ds1_CSS_P,  NE_LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe1PmParamUasP,  cAf6_Ds1_UAS_P,  NE_LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe1PmParamSefsPfe, cAf6_Ds1_SEFS_PFE, FE_LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe1PmParamEsPfe,   cAf6_Ds1_ES_PFE,   FE_LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe1PmParamSesPfe,  cAf6_Ds1_SES_PFE,  FE_LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe1PmParamCssPfe,  cAf6_Ds1_CSS_PFE,  FE_LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe1PmParamUasPfe,  cAf6_Ds1_UAS_PFE,  FE_LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe1PmParamFcPfe,   cAf6_Ds1_FC_PFE,   LatchFunc,    Register);
    mRegistersSave(self, cAtSurEnginePdhDe1PmParamFcP,     cAf6_Ds1_FC_P,     LatchFunc,    Register);

    /* Save valid indication. */
    SaveValid(self, regVal, Register);

    return cAtOk;
    }

/*
 * Latch PM counters from hardware to PM current register.
 *
 * @param self              This engine
 * @param regVal            Buffers read from hardware
 * @param Register          PM register to latch to
 * @param LatchFunc         Latch function for PM parameters
 * @return AT return code
 */
static eAtRet RegistersSave(AtSurEngine self, uint32 *regVal,
                            AtPmRegister (*Register)(AtPmParam),
                            AtPmRegisterHandler LatchFunc)
    {
    AtPmRegisterHandler NE_LatchFunc = LatchFunc;
    AtPmRegisterHandler FE_LatchFunc = LatchFunc;

    /* Some projects need to not latch CV/ES/SES/UAS value during the duration of
     * a potential UAS change. It is to avoid decremented PM values seen at the
     * end of potential UAS duration when application keeps polling PM value. */
    if (!ThaModuleHardSurShouldUpdatePmRegisterWhenPotentialUasChange(Module(self)))
        {
        if (IsPotentialUas(self, regVal[cAf6_UAS_PNE_Index]))
            NE_LatchFunc = DonotLatchHwValue;
        if (IsPotentialUas(self, regVal[cAf6_UAS_PFE_Index]))
            FE_LatchFunc = DonotLatchHwValue;
        }

    return RegistersLatch(self, regVal, Register, LatchFunc, NE_LatchFunc, FE_LatchFunc);
    }

static eAtRet NearEndParamsAdjust(AtSurEngine self, uint32 *regVal,
                                  AtPmRegister (*Register)(AtPmParam),
                                  AtPmRegisterHandler UasParamFunc,
                                  AtPmRegisterHandler SesParamFunc,
                                  AtPmRegisterHandler OtherParamsFunc)
    {
    mRegistersSave(self, cAtSurEnginePdhDe1PmParamCvP, cAf6_Ds1_CV_P_Adj, OtherParamsFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe1PmParamEsP, cAf6_Ds1_ES_P_Adj, OtherParamsFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe1PmParamAissP, cAf6_Ds1_AISS_P_Adj, OtherParamsFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe1PmParamSasP, cAf6_Ds1_SAS_P_Adj, OtherParamsFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe1PmParamCssP, cAf6_Ds1_CSS_P_Adj, OtherParamsFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe1PmParamSesP, cAf6_Ds1_UAS_NE_Adj, SesParamFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe1PmParamUasP, cAf6_Ds1_UAS_NE_Adj, UasParamFunc, Register);

    return cAtOk;
    }

static eAtRet FarEndParamsAdjust(AtSurEngine self, uint32 *regVal,
                                 AtPmRegister (*Register)(AtPmParam),
                                 AtPmRegisterHandler UasParamFunc,
                                 AtPmRegisterHandler SesParamFunc,
                                 AtPmRegisterHandler OtherParamsFunc)
    {
    mRegistersSave(self, cAtSurEnginePdhDe1PmParamSefsPfe, cAf6_Ds1_SEFS_PFE_Adj, OtherParamsFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe1PmParamEsPfe, cAf6_Ds1_ES_PFE_Adj, OtherParamsFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe1PmParamCssPfe, cAf6_Ds1_CSS_PFE_Adj, OtherParamsFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe1PmParamSesPfe, cAf6_Ds1_UAS_FE_Adj, SesParamFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe1PmParamUasPfe, cAf6_Ds1_UAS_FE_Adj, UasParamFunc, Register);

    return cAtOk;
    }

static uint32 UasParamStateGet(AtSurEngine self, uint32 prevRegVal, uint32 currRegVal)
    {
    uint32 stkUas = mRegField(currRegVal, cAf6_UAS_Sticky_);
    uint32 curSes = mRegField(currRegVal, cAf6_UAS_Ses_T2_);
    uint32 prevUas = mRegField(prevRegVal, cAf6_UAS_State_);
    uint32 prevSes = mRegField(prevRegVal, cAf6_UAS_Ses_T1_);
    AtUnused(self);

    if (!stkUas && ((curSes + prevSes) == 10) && (prevSes != 0))
        return (prevUas) ? cThaHardSurUasParamExitting : cThaHardSurUasParamEntering;

    return cThaHardSurUasParamUnknown;
    }

/*
 * Latch & adjust PM counters from hardware to PM previous register.
 *
 * @param self              This engine
 * @param regVal            Buffers read from hardware
 * @param Register          PM register to latch to
 * @param LatchFunc         Latch function for PM parameters
 * @return AT return code
 */
static eAtRet RegistersAdjust(AtSurEngine self, uint32 *regVal,
                              AtPmRegister (*Register)(AtPmParam),
                              AtPmRegisterHandler LatchFunc)
    {
    uint32 latchedSesp = mRegField(regVal[cAf6_UAS_PNE_Index], cAf6_UAS_Ses_T1_);
    uint32 latchedSespfe = mRegField(regVal[cAf6_UAS_PFE_Index], cAf6_UAS_Ses_T1_);
    uint32 regCurrent[cNumDdrHoldRegisters];
    eAtRet ret;
    uint32 uasNeState, uasFeState;

    RegistersLatch(self, regVal, Register, LatchFunc, LatchFunc, LatchFunc);
    if (!latchedSesp && !latchedSespfe)
        return cAtOk;

    ret = ThaModuleHardSurDe1DdrCountersR2c(self, cAtPmCurrentPeriodRegister, regCurrent, cAtFalse);
    if (ret != cAtOk)
        return ret;

    uasNeState = UasParamStateGet(self, regVal[cAf6_UAS_PNE_Index], regCurrent[cAf6_UAS_PNE_Index]);
    uasFeState = UasParamStateGet(self, regVal[cAf6_UAS_PFE_Index], regCurrent[cAf6_UAS_PFE_Index]);

    if (uasNeState == cThaHardSurUasParamEntering)
        NearEndParamsAdjust(self, regVal, Register, AddHwValue, SubstractHwValue, SubstractHwValue);
    else if (uasNeState == cThaHardSurUasParamExitting)
        NearEndParamsAdjust(self, regVal, Register, SubstractHwValue, KeepHwValue, AddHwValue);

    if (uasFeState == cThaHardSurUasParamEntering)
        FarEndParamsAdjust(self, regVal, Register, AddHwValue, SubstractHwValue, SubstractHwValue);
    else if (uasFeState == cThaHardSurUasParamExitting)
        FarEndParamsAdjust(self, regVal, Register, SubstractHwValue, KeepHwValue, AddHwValue);

    return cAtOk;
    }

static eAtRet PeriodExpire(AtSurEngine self, void *data)
    {
    uint32 *regVal = (uint32*)data;

    m_AtSurEngineMethods->PeriodExpire(self, data);

    return RegistersSave(self, regVal, AtPmParamPreviousPeriodRegister, PeriodShift);
    }

static eAtRet AllCountersLatchAndClear(AtSurEngine self, eBool clear)
    {
    uint32 regVal[cNumDdrHoldRegisters];
    eAtRet ret;

    ret = ThaModuleHardSurDe1DdrCountersR2c(self, cAtPmCurrentPeriodRegister, regVal, clear);
    if (ret != cAtOk)
        return ret;

    return RegistersSave(self, regVal, AtPmParamCurrentPeriodRegister, LatchHwValue);
    }

static eAtRet RegistersLatchAndClear(AtSurEngine self, eAtPmRegisterType type, eBool clear)
    {
    uint32 regVal[cNumDdrHoldRegisters];
    eAtRet ret;

    ret = ThaModuleHardSurDe1DdrCountersR2c(self, type, regVal, clear);
    if (ret != cAtOk)
        return ret;

    if (type == cAtPmCurrentPeriodRegister)
        return RegistersSave(self, regVal, AtPmParamCurrentPeriodRegister, LatchHwValue);
    if (type == cAtPmPreviousPeriodRegister)
        return RegistersAdjust(self, regVal, AtPmParamPreviousPeriodRegister, LatchHwValue);
    return cAtErrorModeNotSupport;
    }

static uint32 SavedValid(AtSurEngine self, uint32 paramType, AtPmRegister (*Register)(AtPmParam))
    {
    AtPmParam param = AtSurEnginePmParamGet(self, paramType);
    return AtPmRegisterIsValid(Register(param)) ? 0 : paramType;
    }

static uint32 RegistersInvalidFlagsGet(AtSurEngine self, AtPmRegister (*Register)(AtPmParam))
    {
    uint32 flags = 0;
    flags |= SavedValid(self, cAtSurEnginePdhDe1PmParamEsLfe, Register);
    flags |= SavedValid(self, cAtSurEnginePdhDe1PmParamSefsPfe, Register);
    flags |= SavedValid(self, cAtSurEnginePdhDe1PmParamEsPfe, Register);
    flags |= SavedValid(self, cAtSurEnginePdhDe1PmParamSesPfe, Register);
    flags |= SavedValid(self, cAtSurEnginePdhDe1PmParamCssPfe, Register);
    flags |= SavedValid(self, cAtSurEnginePdhDe1PmParamFcPfe, Register);
    flags |= SavedValid(self, cAtSurEnginePdhDe1PmParamUasPfe, Register);

    return flags;
    }

static uint32 PmInvalidFlagsGet(AtSurEngine self, eAtPmRegisterType type)
    {
    if (type == cAtPmCurrentPeriodRegister)
        return RegistersInvalidFlagsGet(self, AtPmParamCurrentPeriodRegister);

    if (type == cAtPmPreviousPeriodRegister)
        return RegistersInvalidFlagsGet(self, AtPmParamPreviousPeriodRegister);

    return 0;
    }

static uint32 DefaultOffset(AtSurEngine self)
    {
    uint8 sliceId, hwStsInSlice;
    AtPdhChannel de1 = (AtPdhChannel)AtSurEngineChannelGet(self);
    uint32 flatIdInSlice = ThaPdhDe1FlatIdInPhyModule((ThaPdhDe1)de1, cAtModuleSur);

    if (ThaPdhChannelHwIdGet(de1, cAtModuleSur, &sliceId, &hwStsInSlice) != cAtOk)
        return cInvalidUint32;

    return ((uint32)(sliceId << 10UL) | flatIdInSlice);
    }

static uint32 De1ChannelBitMask(ThaPdhDe1 de1)
    {
    uint32 de1InSlice = ThaPdhDe1FlatIdInPhyModule(de1, cAtModuleSur);
    return cBit0 << (de1InSlice & 0x1FU);
    }

static eAtRet HwNotificationEnable(AtSurEngine self, uint32 baseAddr, eBool enable)
    {
    eAtRet ret;
    ThaModuleHardSur module = Module(self);
    if (module && FailureIsSupported(self))
		{
		AtPdhChannel de1 = (AtPdhChannel)AtSurEngineChannelGet(self);
		uint8 sliceId, hwStsInSlice;
		uint32 bitMask;
		uint32 regVal;
		uint32 regAddr;

		ret = ThaPdhChannelHwIdGet(de1, cAtModuleSur, &sliceId, &hwStsInSlice);
		if (ret != cAtOk)
			return ret;

		regAddr = ThaModuleHardSurBaseAddress(module) + baseAddr + (uint32)((sliceId << 5) + hwStsInSlice);
		bitMask = De1ChannelBitMask((ThaPdhDe1)de1);

		regVal = mModuleHwRead(module, regAddr);
		regVal = (enable) ? (regVal | bitMask) : (regVal & ~bitMask);
		mModuleHwWrite(module, regAddr, regVal);
		}
    return cAtOk;
    }

static eBool HwNotificationIsEnabled(AtSurEngine self, uint32 baseAddr)
    {
    ThaModuleHardSur module = Module(self);
    if (module && FailureIsSupported(self))
		{
		AtPdhChannel de1 = (AtPdhChannel)AtSurEngineChannelGet(self);
		uint8 sliceId, hwStsInSlice;
		uint32 bitMask;
		uint32 regVal;
		uint32 regAddr;

		if (ThaPdhChannelHwIdGet(de1, cAtModuleSur, &sliceId, &hwStsInSlice) != cAtOk)
			return cAtFalse;

		regAddr = ThaModuleHardSurBaseAddress(module) + baseAddr + (uint32)((sliceId << 5) + hwStsInSlice);
		bitMask = De1ChannelBitMask((ThaPdhDe1)de1);

		regVal = mModuleHwRead(module, regAddr);
		return (regVal & bitMask) ? cAtTrue : cAtFalse;
		}
    return cAtFalse;
    }

static eAtRet FailureNotificationEnable(AtSurEngine self, eBool enable)
    {
    return HwNotificationEnable(self, cAf6Reg_FMPM_FM_DS1E1_Framer_Interrupt_MASK_Per_DS1E1_Base, enable);
    }

static eBool FailureNotificationIsEnabled(AtSurEngine self)
    {
    return HwNotificationIsEnabled(self, cAf6Reg_FMPM_FM_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1_Base);
    }

static eAtRet TcaNotificationEnable(AtSurEngine self, eBool enable)
    {
    return HwNotificationEnable(self, cAf6Reg_FMPM_TCA_DS1E1_Framer_Interrupt_MASK_Per_DS1E1_Base, enable);
    }

static eBool TcaNotificationIsEnabled(AtSurEngine self)
    {
    return HwNotificationIsEnabled(self, cAf6Reg_FMPM_TCA_DS1E1_Framer_Interrupt_MASK_Per_DS1E1_Base);
    }

static void FailureNotificationProcess(AtSurEngine self, uint32 offset)
    {
    ThaModuleHardSur module = Module((AtSurEngine)self);
    if (module && FailureIsSupported(self))
		{
		uint32 regVal = mModuleHwRead(module, ThaModuleHardSurDe1CurrentFailuresAddr(module) + offset);
		uint32 events = 0;
		uint32 failures = 0;

		mGetFmIntrStatus(cAtPdhDe1AlarmAis,    AIS,    failures, events);
		mGetFmIntrStatus(cAtPdhDe1AlarmLos,    LOS,    failures, events);
		mGetFmIntrStatus(cAtPdhDe1AlarmLof,    LOF,    failures, events);
		mGetFmIntrStatus(cAtPdhDe1AlarmRai,    RAI,    failures, events);
		mGetFmIntrStatus(cAtPdhDe1AlarmRaiCi,  RAI_CI, failures, events);
		mGetFmIntrStatus(cAtPdhDe1AlarmAisCi,  AIS_CI, failures, events);

		/* Only clear masked interrupt. */
		regVal = (regVal >> 16) & cBit15_0;

		/* Clear interrupt */
		mModuleHwWrite(module, ThaModuleHardSurDe1FailuresInterruptAddr(module) + offset, regVal);

		AtSurEngineFailureNotify(self, events, failures);
		}
    }

static void TcaNotificationProcess(AtSurEngine self, uint32 offset)
    {
    ThaModuleHardSur module = Module(self);
    uint32 intrAddr = ThaModuleHardSurDe1TcaInterruptAddr(module) + offset;
    uint32 maskAddr = ThaModuleHardSurDe1TcaInterruptMaskAddr(module) + offset;
    uint32 intrVal = mModuleHwRead(module, intrAddr);
    uint32 maskVal = mModuleHwRead(module, maskAddr);
    uint32 params = 0;

    /* Filter unmasked stickies. */
    intrVal &= maskVal;
    params = TcaMaskGet(intrVal);

    /* Clear interrupt */
    mModuleHwWrite(module, intrAddr, intrVal);

    AtSurEngineAllParamsPeriodTcaNotify(self, params);
    }

static eAtRet Init(AtSurEngine self)
    {
    eAtRet ret = m_AtSurEngineMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    ret |= TcaNotificationMaskReset(self);

    return ret;
    }

static void OverrideAtSurEngine(AtSurEngine self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSurEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSurEngineOverride, m_AtSurEngineMethods, sizeof(m_AtSurEngineOverride));

        mMethodOverride(m_AtSurEngineOverride, Init);
        mMethodOverride(m_AtSurEngineOverride, PmEnable);
        mMethodOverride(m_AtSurEngineOverride, PmIsEnabled);
        mMethodOverride(m_AtSurEngineOverride, PmCanEnable);
        mMethodOverride(m_AtSurEngineOverride, PeriodExpire);
        mMethodOverride(m_AtSurEngineOverride, AllCountersLatchAndClear);
        mMethodOverride(m_AtSurEngineOverride, RegistersLatchAndClear);
        mMethodOverride(m_AtSurEngineOverride, AutonomousFailuresGet);
        mMethodOverride(m_AtSurEngineOverride, NonAutonomousFailuresGet);
        mMethodOverride(m_AtSurEngineOverride, FailureChangedHistoryGet);
        mMethodOverride(m_AtSurEngineOverride, FailureChangedHistoryClear);
        mMethodOverride(m_AtSurEngineOverride, FailureNotificationMaskSet);
        mMethodOverride(m_AtSurEngineOverride, FailureNotificationMaskGet);
        mMethodOverride(m_AtSurEngineOverride, TcaChangedHistoryGet);
        mMethodOverride(m_AtSurEngineOverride, TcaChangedHistoryClear);
        mMethodOverride(m_AtSurEngineOverride, TcaNotificationMaskSet);
        mMethodOverride(m_AtSurEngineOverride, TcaNotificationMaskGet);
        mMethodOverride(m_AtSurEngineOverride, SesThresholdPoolEntrySet);
        mMethodOverride(m_AtSurEngineOverride, SesThresholdPoolEntryGet);
        mMethodOverride(m_AtSurEngineOverride, TcaThresholdPoolEntrySet);
        mMethodOverride(m_AtSurEngineOverride, TcaThresholdPoolEntryGet);
        mMethodOverride(m_AtSurEngineOverride, TcaPeriodThresholdValueGet);
        mMethodOverride(m_AtSurEngineOverride, DefaultOffset);
        mMethodOverride(m_AtSurEngineOverride, FailureNotificationEnable);
        mMethodOverride(m_AtSurEngineOverride, FailureNotificationIsEnabled);
        mMethodOverride(m_AtSurEngineOverride, FailureNotificationProcess);
        mMethodOverride(m_AtSurEngineOverride, TcaNotificationEnable);
        mMethodOverride(m_AtSurEngineOverride, TcaNotificationIsEnabled);
        mMethodOverride(m_AtSurEngineOverride, TcaNotificationProcess);
        mMethodOverride(m_AtSurEngineOverride, PmInvalidFlagsGet);
        mMethodOverride(m_AtSurEngineOverride, SesThresholdProfileGet);
        }

    mMethodsSet(self, &m_AtSurEngineOverride);
    }

static void Override(AtSurEngine self)
    {
    OverrideAtSurEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaHardSurEnginePdhDe1);
    }

AtSurEngine ThaHardSurEnginePdhDe1ObjectInit(AtSurEngine self, AtModuleSur module, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (AtSurEnginePdhDe1ObjectInit(self, module, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSurEngine ThaHardSurEnginePdhDe1New(AtModuleSur module, AtChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSurEngine newSurEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newSurEngine == NULL)
        return NULL;

    /* Construct it */
    return ThaHardSurEnginePdhDe1ObjectInit(newSurEngine, module, channel);
    }

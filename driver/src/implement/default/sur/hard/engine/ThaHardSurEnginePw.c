/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : ThaHardSurEnginePw.c
 *
 * Created Date: Mar 21, 2015
 *
 * Description : PW Surveillance engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../pw/adapters/ThaPwAdapter.h"
#include "../ThaModuleHardSurReg.h"
#include "ThaHardSurEngineInternal.h"
#include "AtPmThresholdProfile.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6_UAS_PNE_Index        3
#define cAf6_UAS_PFE_Index        4

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)     ((ThaHardSurEnginePw)self)

#define mGetIntrBitMask(regVal_, alarmType_, enableMask_, type_, field_)       \
    (enableMask_) |= ((regVal_) & cAf6_FMPM_##type_##_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_##type_##_PW_STK_##field_##_Mask) ? alarmType_ : 0

#define mSetIntrBitMask(alarmType_, defectMask_, enableMask_, type_, field_)   \
    do                                                                         \
        {                                                                      \
        if (defectMask_ & alarmType_)                                          \
            regVal = (enableMask_ & alarmType_) ? \
                    (regVal | cAf6_FMPM_##type_##_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_##type_##_PW_MSK_##field_##_Mask) :\
                    (regVal & (~cAf6_FMPM_##type_##_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_##type_##_PW_MSK_##field_##_Mask)); \
        }while(0)

#define mGetFmIntrStatus(alarmType_, field_, status_, event_)                 \
    do {                                                                       \
        if (regVal & cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_##field_##_Mask) \
            {                                                                  \
            event_ |= alarmType_;                                              \
            if (regVal & cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_##field_##_Mask) \
                status_ |= alarmType_;                                         \
            }                                                                  \
       } while (0)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSurEngineMethods       m_AtSurEngineOverride;

/* Save super implementation */
static const tAtSurEngineMethods *m_AtSurEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleHardSur Module(AtSurEngine self)
    {
    return (ThaModuleHardSur)AtSurEngineModuleGet(self);
    }

static uint32 HwPmEnableRegister(ThaModuleHardSur module)
    {
    return (uint32)(ThaModuleHardSurBaseAddress(module) + cAf6Reg_FMPM_Enable_PM_Collection_PW_Base);
    }

static eAtRet HwPmEnable(AtSurEngine self, eBool enable)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr = HwPmEnableRegister(module) + AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    mRegFieldSet(regVal, cAf6_FMPM_Enable_PM_Collection_PW_enb_col_pw_, mBoolToBin(enable));
    mModuleHwWrite(module, regAddr, regVal);
    return cAtOk;
    }

static eBool HwPmIsEnabled(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr = HwPmEnableRegister(module) + AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    return (regVal & cAf6_FMPM_Enable_PM_Collection_PW_enb_col_pw_Mask) ? cAtTrue : cAtFalse;
    }

static eAtRet PmEnable(AtSurEngine self, eBool enable)
    {
    if (ThaModuleHardSurCanEnableHwPmEngine(Module(self)))
        return HwPmEnable(self, enable);

    return enable ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool PmIsEnabled(AtSurEngine self)
    {
    if (ThaModuleHardSurCanEnableHwPmEngine(Module(self)))
        return HwPmIsEnabled(self);

    return cAtTrue;
    }

static eBool PmCanEnable(AtSurEngine self, eBool enable)
    {
    if (ThaModuleHardSurCanEnableHwPmEngine((Module(self))))
        return cAtTrue;

    return m_AtSurEngineMethods->PmCanEnable(self, enable);
    }

static eAtRet SesThresholdPoolEntrySet(AtSurEngine self, uint32 entryIndex)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr = ThaModuleHardSurPwThresholdRegister(module) +
                     AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    mRegFieldSet(regVal, cAf6_FMPM_Threshold_Type_of_PW_K_TYPE_, entryIndex);
    mModuleHwWrite(module, regAddr, regVal);
    return cAtOk;
    }

static uint32 SesThresholdPoolEntryGet(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr = ThaModuleHardSurPwThresholdRegister(module) +
                     AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    return mRegField(regVal, cAf6_FMPM_Threshold_Type_of_PW_K_TYPE_);
    }

static eAtRet TcaThresholdPoolEntrySet(AtSurEngine self, uint32 entryIndex)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr = ThaModuleHardSurPwThresholdRegister(module) +
                     AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    uint32 tcaTypeMask = mMethodsGet(module)->PwTcaTypeMask(module);
    uint32 tcaTypeShift = mMethodsGet(module)->PwTcaTypeShift(module);
    mRegFieldSet(regVal, tcaType, entryIndex);
    mModuleHwWrite(module, regAddr, regVal);
    return cAtOk;
    }

static uint32 TcaThresholdPoolEntryGet(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr = ThaModuleHardSurPwThresholdRegister(module) +
                     AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    uint32 tcaTypeMask = mMethodsGet(module)->PwTcaTypeMask(module);
    uint32 tcaTypeShift = mMethodsGet(module)->PwTcaTypeShift(module);
    return mRegField(regVal, tcaType);
    }

static uint32 TcaPeriodThresholdValueGet(AtSurEngine self, uint32 paramType)
    {
    AtPmThresholdProfile profile = AtSurEngineThresholdProfileGet(self);
    if (profile)
        return AtPmThresholdProfilePwPeriodTcaThresholdGet(profile, paramType);
    return 0;
    }

static uint32 FailuresCorrect(AtSurEngine self, uint32 failureMask)
    {
    AtChannel pw = AtSurEngineChannelGet(self);

    if ((failureMask & cAtPwAlarmTypeMissingPacket) && !AtChannelAlarmIsSupported(pw, cAtPwAlarmTypeMissingPacket))
        {
        /* For CES PW, RTL missing packets means for LOPSTA, and it should be LOPS. */
        failureMask &= (uint32)~cAtPwAlarmTypeMissingPacket;
        failureMask |= cAtPwAlarmTypeLops;
        }

    return failureMask;
    }

static uint32 FailureMaskGet(uint32 regVal)
    {
    uint32 failures = 0;

    mGetIntrBitMask(regVal, cAtPwAlarmTypeLBit,                 failures, FM,   LBIT);
    mGetIntrBitMask(regVal, cAtPwAlarmTypeRBit,                 failures, FM,   RBIT);
    mGetIntrBitMask(regVal, cAtPwAlarmTypeJitterBufferUnderrun, failures, FM,   BufUder);
    mGetIntrBitMask(regVal, cAtPwAlarmTypeJitterBufferOverrun,  failures, FM,   BufOver);
    mGetIntrBitMask(regVal, cAtPwAlarmTypeLops,                 failures, FM,   LOPS);
    mGetIntrBitMask(regVal, cAtPwAlarmTypeMissingPacket,        failures, FM,   LOPSTA);
    mGetIntrBitMask(regVal, cAtPwAlarmTypeStrayPacket,          failures, FM,   STRAY);
    mGetIntrBitMask(regVal, cAtPwAlarmTypeMalformedPacket,      failures, FM,   MALF);

    return failures;
    }

static uint32 CurrentHwFailuresGet(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr = ThaModuleHardSurPwCurrentFailuresAddr(module) +
                     AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    return FailuresCorrect(self, FailureMaskGet(regVal));
    }

static eBool ShouldMaskAllFailures(AtSurEngine self)
    {
    AtChannel pw = AtSurEngineChannelGet(self);
    ThaModulePw pwModule = (ThaModulePw)AtChannelModuleGet(pw);
    if (ThaModulePwShouldMaskDefectAndFailureOnPwDisabling(pwModule))
        return AtChannelIsEnabled(pw) ? cAtFalse : cAtTrue;
    return cAtFalse;
    }

static uint32 CurrentFailuresGet(AtSurEngine self)
    {
    /* Some applications require when PW is disabled, need to mask all of failures */
    if (ShouldMaskAllFailures(self))
        return 0;

    return CurrentHwFailuresGet(self);
    }

static uint32 FailureRead2Clear(AtSurEngine self, eBool read2Clear)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr = ThaModuleHardSurPwFailuresInterruptAddr(module) +
                     AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    uint32 failures = FailuresCorrect(self, FailureMaskGet(regVal));

    if (read2Clear)
        mModuleHwWrite(module, regAddr, regVal);

    return failures;
    }

static uint32 FailureChangedHistoryGet(AtSurEngine self)
    {
    return FailureRead2Clear(self, cAtFalse);
    }

static uint32 FailureChangedHistoryClear(AtSurEngine self)
    {
    return FailureRead2Clear(self, cAtTrue);
    }

static eBool ContainNoneFailureMask(uint32 failureMask)
    {
    uint32 allFailureMask =
            cAtPwAlarmTypeLBit                 |
            cAtPwAlarmTypeRBit                 |
            cAtPwAlarmTypeJitterBufferUnderrun |
            cAtPwAlarmTypeJitterBufferOverrun  |
            cAtPwAlarmTypeLops                 |
            cAtPwAlarmTypeMissingPacket        |
            cAtPwAlarmTypeStrayPacket          |
            cAtPwAlarmTypeMalformedPacket;
    uint32 noneFailureMask = ~allFailureMask;

    if (failureMask & noneFailureMask)
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet FailureNotificationMaskSet(AtSurEngine self, uint32 failureMask, uint32 enableMask)
    {
    ThaModuleHardSur module;
    uint32 regAddr, regVal;

    if (ContainNoneFailureMask(failureMask))
        return cAtErrorModeNotSupport;

    module = Module(self);
    regAddr = ThaModuleHardSurPwFailuresInterruptMaskAddr(module) + AtSurEngineDefaultOffset(self);
    regVal = mModuleHwRead(module, regAddr);
    failureMask = FailuresCorrect(self, failureMask);

    mSetIntrBitMask(cAtPwAlarmTypeLBit,                 failureMask, enableMask, FM, LBIT);
    mSetIntrBitMask(cAtPwAlarmTypeRBit,                 failureMask, enableMask, FM, RBIT);
    mSetIntrBitMask(cAtPwAlarmTypeJitterBufferUnderrun, failureMask, enableMask, FM, BufUder);
    mSetIntrBitMask(cAtPwAlarmTypeJitterBufferOverrun,  failureMask, enableMask, FM, BufOver);
    mSetIntrBitMask(cAtPwAlarmTypeLops,                 failureMask, enableMask, FM, LOPS);
    mSetIntrBitMask(cAtPwAlarmTypeMissingPacket,        failureMask, enableMask, FM, LOPSTA);
    mSetIntrBitMask(cAtPwAlarmTypeStrayPacket,          failureMask, enableMask, FM, STRAY);
    mSetIntrBitMask(cAtPwAlarmTypeMalformedPacket,      failureMask, enableMask, FM, MALF);
    mModuleHwWrite(module, regAddr, regVal);

    if (regVal)
        return AtSurEngineFailureNotificationEnable(self, cAtTrue);
    else
        return AtSurEngineFailureNotificationEnable(self, cAtFalse);
    }

static uint32 FailureNotificationMaskGet(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr = ThaModuleHardSurPwFailuresInterruptMaskAddr(module) +
                     AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    return FailuresCorrect(self, FailureMaskGet(regVal));
    }

static uint32 TcaMaskGet(uint32 regVal)
    {
    uint32 tcaMask = 0;

    mGetIntrBitMask(regVal, cAtSurEnginePwPmParamEs,    tcaMask, TCA, ES);
    mGetIntrBitMask(regVal, cAtSurEnginePwPmParamSes,   tcaMask, TCA, SES);
    mGetIntrBitMask(regVal, cAtSurEnginePwPmParamUas,   tcaMask, TCA, UAS);
    mGetIntrBitMask(regVal, cAtSurEnginePwPmParamFeEs,  tcaMask, TCA, ES_FE);
    mGetIntrBitMask(regVal, cAtSurEnginePwPmParamFeSes, tcaMask, TCA, SES_FE);
    mGetIntrBitMask(regVal, cAtSurEnginePwPmParamFeUas, tcaMask, TCA, UAS_FE);
    mGetIntrBitMask(regVal, cAtSurEnginePwPmParamFc,    tcaMask, TCA, FC);

    return tcaMask;
    }

static eAtRet TcaNotificationMaskSet(AtSurEngine self, uint32 paramMask, uint32 enableMask)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr;
    uint32 regVal;

    if (!AtModuleSurTcaIsSupported((AtModuleSur)module))
        return cAtErrorModeNotSupport;

    regAddr = ThaModuleHardSurPwTcaInterruptMaskAddr(module) + AtSurEngineDefaultOffset(self);
    regVal = mModuleHwRead(module, regAddr);

    mSetIntrBitMask(cAtSurEnginePwPmParamEs,    paramMask, enableMask, TCA, ES);
    mSetIntrBitMask(cAtSurEnginePwPmParamSes,   paramMask, enableMask, TCA, SES);
    mSetIntrBitMask(cAtSurEnginePwPmParamUas,   paramMask, enableMask, TCA, UAS);
    mSetIntrBitMask(cAtSurEnginePwPmParamFeEs,  paramMask, enableMask, TCA, ES_FE);
    mSetIntrBitMask(cAtSurEnginePwPmParamFeSes, paramMask, enableMask, TCA, SES_FE);
    mSetIntrBitMask(cAtSurEnginePwPmParamFeUas, paramMask, enableMask, TCA, UAS_FE);
    mSetIntrBitMask(cAtSurEnginePwPmParamFc,    paramMask, enableMask, TCA, FC);

    mModuleHwWrite(module, regAddr, regVal);

    return AtSurEngineTcaNotificationEnable(self, regVal ? cAtTrue : cAtFalse);
    }

static uint32 TcaNotificationMaskGet(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr;
    uint32 regVal;

    if (!AtModuleSurTcaIsSupported((AtModuleSur)module))
        return 0;

    regAddr = ThaModuleHardSurPwTcaInterruptMaskAddr(module) + AtSurEngineDefaultOffset(self);
    regVal = mModuleHwRead(module, regAddr);
    return TcaMaskGet(regVal);
    }

static uint32 TcaChangedHistoryRead2Clear(AtSurEngine self, eBool r2c)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr;
    uint32 regVal;
    uint32 changedHistoryMask  = 0;

    if (!AtModuleSurTcaIsSupported((AtModuleSur)module))
        return 0;

    regAddr = ThaModuleHardSurPwTcaInterruptAddr(module) + AtSurEngineDefaultOffset(self);
    regVal = mModuleHwRead(module, regAddr);
    changedHistoryMask = TcaMaskGet(regVal);

    if (r2c)
        mModuleHwWrite(module, regAddr, regVal);

    return changedHistoryMask;
    }

static uint32 TcaChangedHistoryGet(AtSurEngine self)
    {
    return TcaChangedHistoryRead2Clear(self, cAtFalse);
    }

static uint32 TcaChangedHistoryClear(AtSurEngine self)
    {
    return TcaChangedHistoryRead2Clear(self, cAtTrue);
    }

static void PeriodShift(AtSurEngine self, uint32 paramType, uint32 regval, uint32 mask, uint32 shift, AtPmRegister (*Register)(AtPmParam))
    {
    int32 value;
    AtPmParam param = AtSurEnginePmParamGet(self, paramType);
    AtPmRegister reg = Register(param);
    AtPmParamRecentPeriodShiftLeft(param);
    mFieldGet(regval, mask, shift, int32, &value);
    AtPmRegisterValueSet(reg, value);
    }

static void LatchHwValue(AtSurEngine self, uint32 paramType, uint32 regval, uint32 mask, uint32 shift, AtPmRegister (*Register)(AtPmParam))
    {
    int32 value;
    AtPmParam param = AtSurEnginePmParamGet(self, paramType);
    AtPmRegister reg = Register(param);
    mFieldGet(regval, mask, shift, int32, &value);
    AtPmRegisterValueSet(reg, value);
    }

static void KeepHwValue(AtSurEngine self, uint32 paramType, uint32 regval, uint32 mask, uint32 shift, AtPmRegister (*Register)(AtPmParam))
    {
    AtUnused(self);
    AtUnused(paramType);
    AtUnused(regval);
    AtUnused(mask);
    AtUnused(shift);
    AtUnused(Register);
    }

static void AddHwValue(AtSurEngine self, uint32 paramType, uint32 regval, uint32 mask, uint32 shift, AtPmRegister (*Register)(AtPmParam))
    {
    int32 value;
    AtPmParam param = AtSurEnginePmParamGet(self, paramType);
    AtPmRegister reg = Register(param);
    mFieldGet(regval, mask, shift, int32, &value);
    AtPmRegisterValueSet(reg, AtPmRegisterValue(reg) + value);
    }

static void SubstractHwValue(AtSurEngine self, uint32 paramType, uint32 regval, uint32 mask, uint32 shift, AtPmRegister (*Register)(AtPmParam))
    {
    int32 value;
    AtPmParam param = AtSurEnginePmParamGet(self, paramType);
    AtPmRegister reg = Register(param);
    mFieldGet(regval, mask, shift, int32, &value);
    AtPmRegisterValueSet(reg, AtPmRegisterValue(reg) - value);
    }

static void DonotLatchHwValue(AtSurEngine self, uint32 paramType, uint32 regval, uint32 mask, uint32 shift, AtPmRegister (*Register)(AtPmParam))
    {
    AtUnused(self);
    AtUnused(paramType);
    AtUnused(regval);
    AtUnused(mask);
    AtUnused(shift);
    AtUnused(Register);
    }

static eBool IsPotentialUas(AtSurEngine self, uint32 regVal)
    {
    AtUnused(self);
    return (mRegField(regVal, cAf6_UAS_Ses_T1_) > 0) ? cAtTrue : cAtFalse;
    }

/*
 * Latch PM counters from hardware to PM register.
 *
 * @param self          This engine
 * @param regVal        Buffers read from hardware
 * @param Register      PM register to latch to
 * @param LatchFunc     Latch function for normal PM parameters
 * @param NE_LatchFunc  Latch function for near-end PM CV/ES/SES/UAS parameters
 * @param FE_LatchFunc  Latch function for far-end PM CV/ES/SES/UAS parameters
 * @return AT return code
 */
static eAtRet RegistersLatch(AtSurEngine self, uint32 *regVal,
                             AtPmRegister (*Register)(AtPmParam),
                             AtPmRegisterHandler LatchFunc,
                             AtPmRegisterHandler NE_LatchFunc,
                             AtPmRegisterHandler FE_LatchFunc)
    {
    mRegistersSave(self, cAtSurEnginePwPmParamEs,    cAf6_Pw_NE_ES,  NE_LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePwPmParamSes,   cAf6_Pw_NE_SES, NE_LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePwPmParamUas,   cAf6_Pw_NE_UAS, NE_LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePwPmParamFeEs,  cAf6_Pw_FE_ES,  FE_LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePwPmParamFeSes, cAf6_Pw_FE_SES, FE_LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePwPmParamFeUas, cAf6_Pw_FE_UAS, FE_LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePwPmParamFc,    cAf6_PW_FC_P,   LatchFunc,    Register);

    return cAtOk;
    }

/*
 * Latch PM counters from hardware to PM current register.
 *
 * @param self              This engine
 * @param regVal            Buffers read from hardware
 * @param Register          PM register to latch to
 * @param LatchFunc         Latch function for PM parameters
 * @return AT return code
 */
static eAtRet RegistersSave(AtSurEngine self, uint32 *regVal,
                            AtPmRegister (*Register)(AtPmParam),
                            AtPmRegisterHandler LatchFunc)
    {
    AtPmRegisterHandler NE_LatchFunc = LatchFunc;
    AtPmRegisterHandler FE_LatchFunc = LatchFunc;

    /* Some projects need to not latch CV/ES/SES/UAS value during the duration of
     * a potential UAS change. It is to avoid decremented PM values seen at the
     * end of potential UAS duration when application keeps polling PM value. */
    if (!ThaModuleHardSurShouldUpdatePmRegisterWhenPotentialUasChange(Module(self)))
        {
        if (IsPotentialUas(self, regVal[cAf6_UAS_PNE_Index]))
            NE_LatchFunc = DonotLatchHwValue;
        if (IsPotentialUas(self, regVal[cAf6_UAS_PFE_Index]))
            FE_LatchFunc = DonotLatchHwValue;
        }

    return RegistersLatch(self, regVal, Register, LatchFunc, NE_LatchFunc, FE_LatchFunc);
    }

static eAtRet NearEndParamsAdjust(AtSurEngine self, uint32 *regVal,
                                  AtPmRegister (*Register)(AtPmParam),
                                  AtPmRegisterHandler UasParamFunc,
                                  AtPmRegisterHandler SesParamFunc,
                                  AtPmRegisterHandler OtherParamsFunc)
    {
    mRegistersSave(self, cAtSurEnginePwPmParamEs, cAf6_PW_ES_P_Adj, OtherParamsFunc, Register);
    mRegistersSave(self, cAtSurEnginePwPmParamSes, cAf6_PW_UAS_NE_Adj, SesParamFunc, Register);
    mRegistersSave(self, cAtSurEnginePwPmParamUas, cAf6_PW_UAS_NE_Adj, UasParamFunc, Register);

    return cAtOk;
    }

static eAtRet FarEndParamsAdjust(AtSurEngine self, uint32 *regVal,
                                 AtPmRegister (*Register)(AtPmParam),
                                 AtPmRegisterHandler UasParamFunc,
                                 AtPmRegisterHandler SesParamFunc,
                                 AtPmRegisterHandler OtherParamsFunc)
    {
    mRegistersSave(self, cAtSurEnginePwPmParamFeEs, cAf6_PW_ES_PFE_Adj, OtherParamsFunc, Register);
    mRegistersSave(self, cAtSurEnginePwPmParamFeSes, cAf6_PW_UAS_FE_Adj, SesParamFunc, Register);
    mRegistersSave(self, cAtSurEnginePwPmParamFeUas, cAf6_PW_UAS_FE_Adj, UasParamFunc, Register);

    return cAtOk;
    }

static uint32 UasParamStateGet(AtSurEngine self, uint32 prevRegVal, uint32 currRegVal)
    {
    uint32 stkUas = mRegField(currRegVal, cAf6_UAS_Sticky_);
    uint32 curSes = mRegField(currRegVal, cAf6_UAS_Ses_T2_);
    uint32 prevUas = mRegField(prevRegVal, cAf6_UAS_State_);
    uint32 prevSes = mRegField(prevRegVal, cAf6_UAS_Ses_T1_);
    AtUnused(self);

    if (!stkUas && ((curSes + prevSes) == 10) && (prevSes != 0))
        return (prevUas) ? cThaHardSurUasParamExitting : cThaHardSurUasParamEntering;

    return cThaHardSurUasParamUnknown;
    }

static eAtRet RegistersAdjust(AtSurEngine self, uint32 *regVal,
                              AtPmRegister (*Register)(AtPmParam),
                              AtPmRegisterHandler LatchFunc)
    {
    uint32 latchedSesp = mRegField(regVal[cAf6_UAS_PNE_Index], cAf6_UAS_Ses_T1_);
    uint32 latchedSespfe = mRegField(regVal[cAf6_UAS_PFE_Index], cAf6_UAS_Ses_T1_);
    uint32 regCurrent[cNumDdrHoldRegisters];
    eAtRet ret;
    uint32 uasNeState, uasFeState;

    RegistersLatch(self, regVal, Register, LatchFunc, LatchFunc, LatchFunc);
    if (!latchedSesp && !latchedSespfe)
        return cAtOk;

    ret = ThaModuleHardSurPwDdrCountersR2c(self, cAtPmCurrentPeriodRegister, regCurrent, cAtFalse);
    if (ret != cAtOk)
        return ret;

    uasNeState = UasParamStateGet(self, regVal[cAf6_UAS_PNE_Index], regCurrent[cAf6_UAS_PNE_Index]);
    uasFeState = UasParamStateGet(self, regVal[cAf6_UAS_PFE_Index], regCurrent[cAf6_UAS_PFE_Index]);

    if (uasNeState == cThaHardSurUasParamEntering)
        NearEndParamsAdjust(self, regVal, Register, AddHwValue, SubstractHwValue, SubstractHwValue);
    else if (uasNeState == cThaHardSurUasParamExitting)
        NearEndParamsAdjust(self, regVal, Register, SubstractHwValue, KeepHwValue, AddHwValue);

    if (uasFeState == cThaHardSurUasParamEntering)
        FarEndParamsAdjust(self, regVal, Register, AddHwValue, SubstractHwValue, SubstractHwValue);
    else if (uasFeState == cThaHardSurUasParamExitting)
        FarEndParamsAdjust(self, regVal, Register, SubstractHwValue, KeepHwValue, AddHwValue);

    return cAtOk;
    }

static eAtRet PeriodExpire(AtSurEngine self, void *data)
    {
    uint32 *regVal = (uint32*)data;

    m_AtSurEngineMethods->PeriodExpire(self, data);

    return RegistersSave(self, regVal, AtPmParamPreviousPeriodRegister, PeriodShift);
    }

static eAtRet AllCountersLatchAndClear(AtSurEngine self, eBool clear)
    {
    uint32 regVal[cNumDdrHoldRegisters];
    eAtRet ret;

    ret = ThaModuleHardSurPwDdrCountersR2c(self, cAtPmCurrentPeriodRegister, regVal, clear);
    if (ret != cAtOk)
        return ret;

    return RegistersSave(self, regVal, AtPmParamCurrentPeriodRegister, LatchHwValue);
    }

static eAtRet RegistersLatchAndClear(AtSurEngine self, eAtPmRegisterType type, eBool clear)
    {
    uint32 regVal[cNumDdrHoldRegisters];
    eAtRet ret;

    ret = ThaModuleHardSurPwDdrCountersR2c(self, type, regVal, clear);
    if (ret != cAtOk)
        return ret;

    if (type == cAtPmCurrentPeriodRegister)
        return RegistersSave(self, regVal, AtPmParamCurrentPeriodRegister, LatchHwValue);
    if (type == cAtPmPreviousPeriodRegister)
        return RegistersAdjust(self, regVal, AtPmParamPreviousPeriodRegister, LatchHwValue);
    return cAtErrorModeNotSupport;
    }

static uint32 PmInvalidFlagsGet(AtSurEngine self, eAtPmRegisterType type)
    {
    AtUnused(self);
    AtUnused(type);
    /* Just all counters are valid. */
    return 0;
    }

static uint32 DefaultOffset(AtSurEngine self)
    {
    return AtChannelIdGet(AtSurEngineChannelGet(self));
    }

static eAtRet HwNotificationEnable(AtSurEngine self, uint32 baseAddr, eBool enable)
    {
    ThaModuleHardSur module = Module(self);
    uint32 pwId = AtChannelIdGet(AtSurEngineChannelGet(self));
    uint32 grpId = (pwId >> 5);
    uint32 regAddr = ThaModuleHardSurBaseAddress(module) + (uint32)(baseAddr + grpId);
    uint32 bitMask = cBit0 << (pwId & 0x1F);
    uint32 regVal = mModuleHwRead(module, regAddr);

    regVal = (enable) ? (regVal | bitMask) : (regVal & ~bitMask);
    mModuleHwWrite(module, regAddr, regVal);

    return cAtOk;
    }

static eBool HwNotificationIsEnabled(AtSurEngine self, uint32 baseAddr)
    {
    ThaModuleHardSur module = Module(self);
    uint32 pwId = AtChannelIdGet(AtSurEngineChannelGet(self));
    uint32 grpId = (pwId >> 5);
    uint32 regAddr = ThaModuleHardSurBaseAddress(module) + (uint32)(baseAddr + grpId);
    uint32 bitMask = cBit0 << (pwId & 0x1F);
    uint32 regVal = mModuleHwRead(module, regAddr);
    return (regVal & bitMask) ? cAtTrue : cAtFalse;
    }

static uint32 PWFramerInterruptMASKPerPWBase(AtSurEngine self)
    {
    ThaModuleHardSur module = (ThaModuleHardSur)AtSurEngineModuleGet(self);
    return ThaModuleHardSurPWFramerInterruptMASKPerPWBase(module);
    }

static eAtRet FailureNotificationEnable(AtSurEngine self, eBool enable)
    {
    return ThaHardSurEnginePwNotificationEnable(self, PWFramerInterruptMASKPerPWBase(self), enable);
    }

static eBool FailureNotificationIsEnabled(AtSurEngine self)
    {
    return ThaHardSurEnginePwNotificationIsEnabled(self, PWFramerInterruptMASKPerPWBase(self));
    }

static eAtRet TcaNotificationEnable(AtSurEngine self, eBool enable)
    {
    return ThaHardSurEnginePwNotificationEnable(self, cAf6Reg_FMPM_TCA_PW_Framer_Interrupt_MASK_Per_PW_Base, enable);
    }

static eBool TcaNotificationIsEnabled(AtSurEngine self)
    {
    return ThaHardSurEnginePwNotificationIsEnabled(self, cAf6Reg_FMPM_TCA_PW_Framer_Interrupt_MASK_Per_PW_Base);
    }

static void FailureNotificationProcess(AtSurEngine self, uint32 offset)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regVal = mModuleHwRead(module, ThaModuleHardSurPwCurrentFailuresAddr(module) + offset);
    uint32 events = 0;
    uint32 failures = 0;

    mGetFmIntrStatus(cAtPwAlarmTypeLBit,                LBIT,   failures, events);
    mGetFmIntrStatus(cAtPwAlarmTypeRBit,                RBIT,   failures, events);
    mGetFmIntrStatus(cAtPwAlarmTypeJitterBufferUnderrun,BufUder, failures, events);
    mGetFmIntrStatus(cAtPwAlarmTypeJitterBufferOverrun, BufOver, failures, events);
    mGetFmIntrStatus(cAtPwAlarmTypeLops,                LOPS,   failures, events);
    mGetFmIntrStatus(cAtPwAlarmTypeMissingPacket,       LOPSTA, failures, events);
    mGetFmIntrStatus(cAtPwAlarmTypeStrayPacket,         STRAY,  failures, events);
    mGetFmIntrStatus(cAtPwAlarmTypeMalformedPacket,     MALF,   failures, events);

    /* Only clear masked interrupt. */
    regVal = (regVal >> 16) & cBit15_0;

    /* Clear interrupt */
    mModuleHwWrite(module, ThaModuleHardSurPwFailuresInterruptAddr(module) + offset, regVal);

    AtSurEngineFailureNotify(self, events, failures);
    }

static void TcaNotificationProcess(AtSurEngine self, uint32 offset)
    {
    ThaModuleHardSur module = Module(self);
    uint32 intrAddr = ThaModuleHardSurPwTcaInterruptAddr(module) + offset;
    uint32 maskAddr = ThaModuleHardSurPwTcaInterruptMaskAddr(module) + offset;
    uint32 intrVal = mModuleHwRead(module, intrAddr);
    uint32 maskVal = mModuleHwRead(module, maskAddr);
    uint32 params = 0;

    /* Filter unmasked stickies. */
    intrVal &= maskVal;
    params = TcaMaskGet(intrVal);

    /* Clear interrupt */
    mModuleHwWrite(module, intrAddr, intrVal);

    AtSurEngineAllParamsPeriodTcaNotify(self, params);
    }

static AtChannel ChannelGet(AtSurEngine self)
    {
    return (AtChannel)ThaPwAdapterPwGet((ThaPwAdapter)self->channel);
    }

static AtPmSesThresholdProfile SesThresholdProfileGet(AtSurEngine self)
    {
    return AtModuleSurSesThresholdProfileGet(AtSurEngineModuleGet(self), AtSurEngineSesThresholdPoolEntryGet(self));
    }

static void OverrideAtSurEngine(AtSurEngine self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSurEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSurEngineOverride, m_AtSurEngineMethods, sizeof(m_AtSurEngineOverride));

        mMethodOverride(m_AtSurEngineOverride, PmEnable);
        mMethodOverride(m_AtSurEngineOverride, PmIsEnabled);
        mMethodOverride(m_AtSurEngineOverride, PmCanEnable);
        mMethodOverride(m_AtSurEngineOverride, ChannelGet);
        mMethodOverride(m_AtSurEngineOverride, CurrentFailuresGet);
        mMethodOverride(m_AtSurEngineOverride, FailureChangedHistoryGet);
        mMethodOverride(m_AtSurEngineOverride, FailureChangedHistoryClear);
        mMethodOverride(m_AtSurEngineOverride, FailureNotificationMaskSet);
        mMethodOverride(m_AtSurEngineOverride, FailureNotificationMaskGet);
        mMethodOverride(m_AtSurEngineOverride, TcaChangedHistoryGet);
        mMethodOverride(m_AtSurEngineOverride, TcaChangedHistoryClear);
        mMethodOverride(m_AtSurEngineOverride, TcaNotificationMaskSet);
        mMethodOverride(m_AtSurEngineOverride, TcaNotificationMaskGet);
        mMethodOverride(m_AtSurEngineOverride, SesThresholdPoolEntrySet);
        mMethodOverride(m_AtSurEngineOverride, SesThresholdPoolEntryGet);
        mMethodOverride(m_AtSurEngineOverride, TcaThresholdPoolEntrySet);
        mMethodOverride(m_AtSurEngineOverride, TcaThresholdPoolEntryGet);
        mMethodOverride(m_AtSurEngineOverride, TcaPeriodThresholdValueGet);
        mMethodOverride(m_AtSurEngineOverride, AllCountersLatchAndClear);
        mMethodOverride(m_AtSurEngineOverride, RegistersLatchAndClear);
        mMethodOverride(m_AtSurEngineOverride, PeriodExpire);
        mMethodOverride(m_AtSurEngineOverride, DefaultOffset);
        mMethodOverride(m_AtSurEngineOverride, FailureNotificationEnable);
        mMethodOverride(m_AtSurEngineOverride, FailureNotificationIsEnabled);
        mMethodOverride(m_AtSurEngineOverride, FailureNotificationProcess);
        mMethodOverride(m_AtSurEngineOverride, TcaNotificationEnable);
        mMethodOverride(m_AtSurEngineOverride, TcaNotificationIsEnabled);
        mMethodOverride(m_AtSurEngineOverride, TcaNotificationProcess);
        mMethodOverride(m_AtSurEngineOverride, PmInvalidFlagsGet);
        mMethodOverride(m_AtSurEngineOverride, SesThresholdProfileGet);
        }

    mMethodsSet(self, &m_AtSurEngineOverride);
    }

static void Override(AtSurEngine self)
    {
    OverrideAtSurEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaHardSurEnginePw);
    }

AtSurEngine ThaHardSurEnginePwObjectInit(AtSurEngine self, AtModuleSur module, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (AtSurEnginePwObjectInit(self, module, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSurEngine ThaHardSurEnginePwNew(AtModuleSur module, AtChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSurEngine newSurEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newSurEngine == NULL)
        return NULL;

    /* Construct it */
    return ThaHardSurEnginePwObjectInit(newSurEngine, module, channel);
    }

eAtRet ThaHardSurEnginePwNotificationEnable(AtSurEngine self, uint32 baseAddr, eBool enable)
    {
    if (self)
        return HwNotificationEnable(self, baseAddr, enable);
    return cAtErrorNullPointer;
    }

eBool ThaHardSurEnginePwNotificationIsEnabled(AtSurEngine self, uint32 baseAddr)
    {
    if (self)
        return HwNotificationIsEnabled(self, baseAddr);
    return cAtFalse;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : ThaHardSurEngineSdhPath.c
 *
 * Created Date: Mar 21, 2015
 *
 * Description : Path surveillance engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../../generic/sdh/AtSdhChannelInternal.h"
#include "../../../ocn/ThaModuleOcn.h"
#include "../../../man/ThaDevice.h"
#include "../ThaModuleHardSurReg.h"
#include "../param/ThaHardPmRegisterInternal.h"
#include "ThaHardSurEngineInternal.h"
#include "AtPmThresholdProfile.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6_UAS_PNE_Index        9
#define cAf6_UAS_PFE_Index       10

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)     ((ThaHardSurEngineSdhPath)self)

#define mGetIntrBitMask(regVal_, alarmType_, enableMask_, type_, field_)       \
    (enableMask_) |= ((regVal_) & cAf6_FMPM_##type_##_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_##type_##_STS1_STK_##field_##_Mask) ? alarmType_ : 0

#define mSetIntrBitMask(alarmType_, defectMask_, enableMask_, type_, field_)   \
    do                                                                         \
        {                                                                      \
        if (defectMask_ & alarmType_)                                          \
            regVal = (enableMask_ & alarmType_) ? \
                    (regVal | cAf6_FMPM_##type_##_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_##type_##_STS1_MSK_##field_##_Mask) :\
                    (regVal & (~cAf6_FMPM_##type_##_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_##type_##_STS1_MSK_##field_##_Mask)); \
        }while(0)

#define mGetFmIntrStatus(alarmType_, field_, status_, event_)                 \
    do {                                                                       \
        if (regVal & cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_AMSK_##field_##_Mask) \
            {                                                                  \
            event_ |= alarmType_;                                              \
            if (regVal & cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_CUR_##field_##_Mask) \
                status_ |= alarmType_;                                         \
            }                                                                  \
       } while (0)

#define mGetFmStatus(regVal_, field_)                                          \
    mRegField(regVal_, cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_CUR_##field_##_)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSurEngineMethods       m_AtSurEngineOverride;

/* Save super implementation */
static const tAtSurEngineMethods *m_AtSurEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleHardSur Module(AtSurEngine self)
    {
    return (ThaModuleHardSur)AtSurEngineModuleGet(self);
    }

static eBool FailureIsSupported(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    if (module)
        return ThaModuleHardSurFailureIsSupported(module);
    return cAtFalse;
    }

static uint32 HwPmEnableRegister(ThaModuleHardSur module)
    {
    return (uint32)(ThaModuleHardSurBaseAddress(module) + cAf6Reg_FMPM_Enable_PM_Collection_STS_Base);
    }

static eAtRet HwPmEnable(AtSurEngine self, eBool enable)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr = HwPmEnableRegister(module) + AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    mRegFieldSet(regVal, cAf6_FMPM_Enable_PM_Collection_STS_enb_col_sts_, mBoolToBin(enable));
    mModuleHwWrite(module, regAddr, regVal);
    return cAtOk;
    }

static eBool HwPmIsEnabled(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr = HwPmEnableRegister(module) + AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    return (regVal & cAf6_FMPM_Enable_PM_Collection_STS_enb_col_sts_Mask) ? cAtTrue : cAtFalse;
    }

static eAtRet PmEnable(AtSurEngine self, eBool enable)
    {
    if (ThaModuleHardSurCanEnableHwPmEngine(Module(self)))
        return HwPmEnable(self, enable);

    return enable ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool PmIsEnabled(AtSurEngine self)
    {
    if (ThaModuleHardSurCanEnableHwPmEngine(Module(self)))
        return HwPmIsEnabled(self);

    return cAtTrue;
    }

static eBool PmCanEnable(AtSurEngine self, eBool enable)
    {
    if (ThaModuleHardSurCanEnableHwPmEngine((Module(self))))
        return cAtTrue;

    return m_AtSurEngineMethods->PmCanEnable(self, enable);
    }

static eAtRet SesThresholdPoolEntrySet(AtSurEngine self, uint32 entryIndex)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr = ThaModuleHardSurStsThresholdRegister(module) +
                     AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    mRegFieldSet(regVal, cAf6_FMPM_Threshold_Type_of_STS_K_TYPE_, entryIndex);
    mModuleHwWrite(module, regAddr, regVal);

    return cAtOk;
    }

static uint32 SesThresholdPoolEntryGet(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr = ThaModuleHardSurStsThresholdRegister(module) +
                     AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    return mRegField(regVal, cAf6_FMPM_Threshold_Type_of_STS_K_TYPE_);
    }

static eAtRet TcaThresholdPoolEntrySet(AtSurEngine self, uint32 entryIndex)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr = ThaModuleHardSurStsThresholdRegister(module) +
                     AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    uint32 tcaTypeMask = mMethodsGet(module)->StsTcaTypeMask(module);
    uint32 tcaTypeShift = mMethodsGet(module)->StsTcaTypeShift(module);
    mRegFieldSet(regVal, tcaType, entryIndex);
    mModuleHwWrite(module, regAddr, regVal);

    return cAtOk;
    }

static uint32 TcaThresholdPoolEntryGet(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr = ThaModuleHardSurStsThresholdRegister(module) +
                     AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    uint32 tcaTypeMask = mMethodsGet(module)->StsTcaTypeMask(module);
    uint32 tcaTypeShift = mMethodsGet(module)->StsTcaTypeShift(module);
    return mRegField(regVal, tcaType);
    }

static uint32 TcaPeriodThresholdValueGet(AtSurEngine self, uint32 paramType)
    {
    AtPmThresholdProfile profile = AtSurEngineThresholdProfileGet(self);
    if (profile)
        return AtPmThresholdProfileSdhHoPathPeriodTcaThresholdGet(profile, paramType);
    return 0;
    }

static uint32 FailureMaskGet(uint32 regVal)
    {
    uint32 failures = 0;

    mGetIntrBitMask(regVal, cAtSdhPathAlarmAis,   failures, FM,   AIS);
    mGetIntrBitMask(regVal, cAtSdhPathAlarmLop,   failures, FM,   LOP);
    mGetIntrBitMask(regVal, cAtSdhPathAlarmLom,   failures, FM,   LOM);
    mGetIntrBitMask(regVal, cAtSdhPathAlarmRfi,   failures, FM,   RFI);
    mGetIntrBitMask(regVal, cAtSdhPathAlarmRfiS,  failures, FM,   RFI_SER);
    mGetIntrBitMask(regVal, cAtSdhPathAlarmRfiC,  failures, FM,   RFI_CON);
    mGetIntrBitMask(regVal, cAtSdhPathAlarmRfiP,  failures, FM,   RFI_PAY);
    mGetIntrBitMask(regVal, cAtSdhPathAlarmTim,   failures, FM,   TIM);
    mGetIntrBitMask(regVal, cAtSdhPathAlarmUneq,  failures, FM,  UNEQ);
    mGetIntrBitMask(regVal, cAtSdhPathAlarmPlm,   failures, FM,   PLM);
    mGetIntrBitMask(regVal, cAtSdhPathAlarmBerSd, failures, FM, BERSD);
    mGetIntrBitMask(regVal, cAtSdhPathAlarmBerSf, failures, FM, BERSF);

    return failures;
    }

static uint32 AutonomousFailuresGet(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    if (module && FailureIsSupported(self))
		{
    uint32 regAddr = ThaModuleHardSurStsCurrentFailuresAddr(module) +
                     AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    return FailureMaskGet(regVal);
    }
    return 0;
    }

static uint32 NonAutonomousFailuresGet(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    if (module && FailureIsSupported(self))
		{
    uint32 regAddr;
    uint32 regVal;

    if (!ThaModuleHardSurFailureForwardingStatusIsSupported(module))
        return 0;

    regAddr = ThaModuleHardSurStsCurrentFailuresAddr(module) + AtSurEngineDefaultOffset(self);
    regVal = mModuleHwRead(module, regAddr);

    if (mGetFmStatus(regVal, HI_AIS))
        return cAtSdhPathAlarmAis;
		}
    return 0;
    }

static uint32 FailureRead2Clear(AtSurEngine self, eBool read2Clear)
    {
    ThaModuleHardSur module = Module(self);
    if (module && FailureIsSupported(self))
		{
    uint32 regAddr = ThaModuleHardSurStsFailuresInterruptAddr(module) +
                     AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    uint32 failures = FailureMaskGet(regVal);

    if (read2Clear)
        mModuleHwWrite(module, regAddr, regVal);

    return failures;
    }
    return 0;
    }

static uint32 FailureChangedHistoryGet(AtSurEngine self)
    {
    return FailureRead2Clear(self, cAtFalse);
    }

static uint32 FailureChangedHistoryClear(AtSurEngine self)
    {
    return FailureRead2Clear(self, cAtTrue);
    }

static eAtRet FailureNotificationMaskSet(AtSurEngine self, uint32 failureMask, uint32 enableMask)
    {
    ThaModuleHardSur module = Module(self);
    if (module && FailureIsSupported(self))
		{
    uint32 regAddr = ThaModuleHardSurStsFailuresInterruptMaskAddr(module) +
                     AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);

    mSetIntrBitMask(cAtSdhPathAlarmLop,    failureMask, enableMask, FM, LOP);
    mSetIntrBitMask(cAtSdhPathAlarmAis,    failureMask, enableMask, FM, AIS);
    mSetIntrBitMask(cAtSdhPathAlarmUneq,   failureMask, enableMask, FM, UNEQ);
    mSetIntrBitMask(cAtSdhPathAlarmTim,    failureMask, enableMask, FM, TIM);
    mSetIntrBitMask(cAtSdhPathAlarmPlm,    failureMask, enableMask, FM, PLM);
    mSetIntrBitMask(cAtSdhPathAlarmLom,    failureMask, enableMask, FM, LOM);
    mSetIntrBitMask(cAtSdhPathAlarmRfi,    failureMask, enableMask, FM, RFI);
    mSetIntrBitMask(cAtSdhPathAlarmRfiS,   failureMask, enableMask, FM, RFI_SER);
    mSetIntrBitMask(cAtSdhPathAlarmRfiC,   failureMask, enableMask, FM, RFI_CON);
    mSetIntrBitMask(cAtSdhPathAlarmRfiP,   failureMask, enableMask, FM, RFI_PAY);
    mSetIntrBitMask(cAtSdhPathAlarmBerSd,  failureMask, enableMask, FM, BERSD);
    mSetIntrBitMask(cAtSdhPathAlarmBerSf,  failureMask, enableMask, FM, BERSF);
    mModuleHwWrite(module, regAddr, regVal);

    if (regVal)
        return AtSurEngineFailureNotificationEnable(self, cAtTrue);
    else
        return AtSurEngineFailureNotificationEnable(self, cAtFalse);
    }
    return cAtOk;
    }

static uint32 FailureNotificationMaskGet(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    if (module && FailureIsSupported(self))
		{
    uint32 regAddr = ThaModuleHardSurStsFailuresInterruptMaskAddr(module) +
                     AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    return FailureMaskGet(regVal);
    }
    return 0;
    }

static uint32 TcaMaskGet(uint32 regVal)
    {
    uint32 tcaMask = 0;

    mGetIntrBitMask(regVal, cAtSurEngineSdhPathPmParamPpjcPdet, tcaMask, TCA, PPJC_PDet);
    mGetIntrBitMask(regVal, cAtSurEngineSdhPathPmParamNpjcPdet, tcaMask, TCA, NPJC_PDet);
    mGetIntrBitMask(regVal, cAtSurEngineSdhPathPmParamPpjcPgen, tcaMask, TCA, PPJC_PGen);
    mGetIntrBitMask(regVal, cAtSurEngineSdhPathPmParamNpjcPgen, tcaMask, TCA, NPJC_PGen);
    mGetIntrBitMask(regVal, cAtSurEngineSdhPathPmParamPjcDiff,  tcaMask, TCA, PJCDiff_P);
    mGetIntrBitMask(regVal, cAtSurEngineSdhPathPmParamPjcsPdet, tcaMask, TCA, PJCS_Det);
    mGetIntrBitMask(regVal, cAtSurEngineSdhPathPmParamPjcsPgen, tcaMask, TCA, PJCS_Gen);
    mGetIntrBitMask(regVal, cAtSurEngineSdhPathPmParamCvP,      tcaMask, TCA, CV_P);
    mGetIntrBitMask(regVal, cAtSurEngineSdhPathPmParamEsP,      tcaMask, TCA, ES_P);
    mGetIntrBitMask(regVal, cAtSurEngineSdhPathPmParamSesP,     tcaMask, TCA, SES_P);
    mGetIntrBitMask(regVal, cAtSurEngineSdhPathPmParamUasP,     tcaMask, TCA, UAS_P);
    mGetIntrBitMask(regVal, cAtSurEngineSdhPathPmParamFcP,      tcaMask, TCA, FC_P);
    mGetIntrBitMask(regVal, cAtSurEngineSdhPathPmParamCvPfe,    tcaMask, TCA, CV_PFE);
    mGetIntrBitMask(regVal, cAtSurEngineSdhPathPmParamEsPfe,    tcaMask, TCA, ES_PFE);
    mGetIntrBitMask(regVal, cAtSurEngineSdhPathPmParamSesPfe,   tcaMask, TCA, SES_PFE);
    mGetIntrBitMask(regVal, cAtSurEngineSdhPathPmParamUasPfe,   tcaMask, TCA, UAS_PFE);
    mGetIntrBitMask(regVal, cAtSurEngineSdhPathPmParamFcPfe,    tcaMask, TCA, FC_PFE);

    return tcaMask;
    }

static eAtRet TcaNotificationMaskSet(AtSurEngine self, uint32 paramMask, uint32 enableMask)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr;
    uint32 regVal;

    if (!AtModuleSurTcaIsSupported((AtModuleSur)module))
        return cAtErrorModeNotSupport;

    regAddr = ThaModuleHardSurStsTcaInterruptMaskAddr(module) + AtSurEngineDefaultOffset(self);
    regVal = mModuleHwRead(module, regAddr);

    mSetIntrBitMask(cAtSurEngineSdhPathPmParamPpjcPdet, paramMask, enableMask, TCA, PPJC_PDet);
    mSetIntrBitMask(cAtSurEngineSdhPathPmParamNpjcPdet, paramMask, enableMask, TCA, NPJC_PDet);
    mSetIntrBitMask(cAtSurEngineSdhPathPmParamPpjcPgen, paramMask, enableMask, TCA, PPJC_PGen);
    mSetIntrBitMask(cAtSurEngineSdhPathPmParamNpjcPgen, paramMask, enableMask, TCA, NPJC_PGen);
    mSetIntrBitMask(cAtSurEngineSdhPathPmParamPjcDiff,  paramMask, enableMask, TCA, PJCDiff_P);
    mSetIntrBitMask(cAtSurEngineSdhPathPmParamPjcsPdet, paramMask, enableMask, TCA, PJCS_Det);
    mSetIntrBitMask(cAtSurEngineSdhPathPmParamPjcsPgen, paramMask, enableMask, TCA, PJCS_Gen);
    mSetIntrBitMask(cAtSurEngineSdhPathPmParamCvP,      paramMask, enableMask, TCA, CV_P);
    mSetIntrBitMask(cAtSurEngineSdhPathPmParamEsP,      paramMask, enableMask, TCA, ES_P);
    mSetIntrBitMask(cAtSurEngineSdhPathPmParamSesP,     paramMask, enableMask, TCA, SES_P);
    mSetIntrBitMask(cAtSurEngineSdhPathPmParamUasP,     paramMask, enableMask, TCA, UAS_P);
    mSetIntrBitMask(cAtSurEngineSdhPathPmParamFcP,      paramMask, enableMask, TCA, FC_P);
    mSetIntrBitMask(cAtSurEngineSdhPathPmParamCvPfe,    paramMask, enableMask, TCA, CV_PFE);
    mSetIntrBitMask(cAtSurEngineSdhPathPmParamEsPfe,    paramMask, enableMask, TCA, ES_PFE);
    mSetIntrBitMask(cAtSurEngineSdhPathPmParamSesPfe,   paramMask, enableMask, TCA, SES_PFE);
    mSetIntrBitMask(cAtSurEngineSdhPathPmParamUasPfe,   paramMask, enableMask, TCA, UAS_PFE);
    mSetIntrBitMask(cAtSurEngineSdhPathPmParamFcPfe,    paramMask, enableMask, TCA, FC_PFE);

    mModuleHwWrite(module, regAddr, regVal);

    return AtSurEngineTcaNotificationEnable(self, regVal ? cAtTrue : cAtFalse);
    }

static uint32 TcaNotificationMaskGet(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr;
    uint32 regVal;

    if (!AtModuleSurTcaIsSupported((AtModuleSur)module))
        return 0;

    regAddr = ThaModuleHardSurStsTcaInterruptMaskAddr(module) + AtSurEngineDefaultOffset(self);
    regVal = mModuleHwRead(module, regAddr);
    return TcaMaskGet(regVal);
    }

static uint32 TcaChangedHistoryRead2Clear(AtSurEngine self, eBool r2c)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr;
    uint32 regVal;
    uint32 changedHistoryMask  = 0;

    if (!AtModuleSurTcaIsSupported((AtModuleSur)module))
        return 0;

    regAddr = ThaModuleHardSurStsTcaInterruptAddr(module) + AtSurEngineDefaultOffset(self);
    regVal = mModuleHwRead(module, regAddr);
    changedHistoryMask = TcaMaskGet(regVal);

    if (r2c)
        mModuleHwWrite(module, regAddr, regVal);

    return changedHistoryMask;
    }

static uint32 TcaChangedHistoryGet(AtSurEngine self)
    {
    return TcaChangedHistoryRead2Clear(self, cAtFalse);
    }

static uint32 TcaChangedHistoryClear(AtSurEngine self)
    {
    return TcaChangedHistoryRead2Clear(self, cAtTrue);
    }

#include "ThaHardSurEngineCommon.h"

static eAtRet SaveValid(AtSurEngine self, uint32 *regVal, AtPmRegister (*Register)(AtPmParam))
    {
    mRegistersSave(self, cAtSurEngineSdhPathPmParamCvPfe,    cAf6_Sts_PFE_Invalid, LatchHwValid, Register);
    mRegistersSave(self, cAtSurEngineSdhPathPmParamEsPfe,    cAf6_Sts_PFE_Invalid, LatchHwValid, Register);
    mRegistersSave(self, cAtSurEngineSdhPathPmParamSesPfe,   cAf6_Sts_PFE_Invalid, LatchHwValid, Register);
    mRegistersSave(self, cAtSurEngineSdhPathPmParamUasPfe,   cAf6_Sts_PFE_Invalid, LatchHwValid, Register);
    mRegistersSave(self, cAtSurEngineSdhPathPmParamFcPfe,    cAf6_Sts_PFE_Invalid, LatchHwValid, Register);

    return cAtOk;
    }

/*
 * Latch PM counters from hardware to PM register.
 *
 * @param self          This engine
 * @param regVal        Buffers read from hardware
 * @param Register      PM register to latch to
 * @param LatchFunc     Latch function for normal PM parameters
 * @param NE_LatchFunc  Latch function for near-end PM CV/ES/SES/UAS parameters
 * @param FE_LatchFunc  Latch function for far-end PM CV/ES/SES/UAS parameters
 * @return AT return code
 */
static eAtRet RegistersLatch(AtSurEngine self, uint32 *regVal,
                             AtPmRegister (*Register)(AtPmParam),
                             AtPmRegisterHandler LatchFunc,
                             AtPmRegisterHandler NE_LatchFunc,
                             AtPmRegisterHandler FE_LatchFunc)
    {
    mRegistersSave(self, cAtSurEngineSdhPathPmParamPpjcPdet, cAf6_Sts_PPJC_PDet, LatchFunc, Register);
    mRegistersSave(self, cAtSurEngineSdhPathPmParamPpjcPgen, cAf6_Sts_PPJC_PGEN, LatchFunc, Register);
    mRegistersSave(self, cAtSurEngineSdhPathPmParamNpjcPdet, cAf6_Sts_NPJC_PDet, LatchFunc, Register);
    mRegistersSave(self, cAtSurEngineSdhPathPmParamNpjcPgen, cAf6_Sts_NPJC_PGEN, LatchFunc, Register);
    mRegistersSave(self, cAtSurEngineSdhPathPmParamPjcDiff,  cAf6_Sts_PJCDIFF_P, LatchFunc, Register);
    mRegistersSave(self, cAtSurEngineSdhPathPmParamPjcsPdet, cAf6_Sts_PJCS_PDet, LatchFunc, Register);
    mRegistersSave(self, cAtSurEngineSdhPathPmParamPjcsPgen, cAf6_Sts_PJCS_PGEN, LatchFunc, Register);
    mRegistersSave(self, cAtSurEngineSdhPathPmParamCvP,      cAf6_Sts_CV_P,      NE_LatchFunc, Register);
    mRegistersSave(self, cAtSurEngineSdhPathPmParamEsP,      cAf6_Sts_ES_P,      NE_LatchFunc, Register);
    mRegistersSave(self, cAtSurEngineSdhPathPmParamSesP,     cAf6_Sts_SES_P,     NE_LatchFunc, Register);
    mRegistersSave(self, cAtSurEngineSdhPathPmParamUasP,     cAf6_Sts_UAS_P,     NE_LatchFunc, Register);
    mRegistersSave(self, cAtSurEngineSdhPathPmParamFcP,      cAf6_Sts_FC_P,      LatchFunc, Register);
    mRegistersSave(self, cAtSurEngineSdhPathPmParamCvPfe,    cAf6_Sts_CV_PFE,    FE_LatchFunc, Register);
    mRegistersSave(self, cAtSurEngineSdhPathPmParamEsPfe,    cAf6_Sts_ES_PFE,    FE_LatchFunc, Register);
    mRegistersSave(self, cAtSurEngineSdhPathPmParamSesPfe,   cAf6_Sts_SES_PFE,   FE_LatchFunc, Register);
    mRegistersSave(self, cAtSurEngineSdhPathPmParamUasPfe,   cAf6_Sts_UAS_PFE,   FE_LatchFunc, Register);
    mRegistersSave(self, cAtSurEngineSdhPathPmParamFcPfe,    cAf6_Sts_FC_PFE,    LatchFunc, Register);

    /* Save valid indication. */
    SaveValid(self, regVal, Register);

    return cAtOk;
    }

/*
 * Latch PM counters from hardware to PM current register.
 *
 * @param self              This engine
 * @param regVal            Buffers read from hardware
 * @param Register          PM register to latch to
 * @param LatchFunc         Latch function for PM parameters
 * @return AT return code
 */
static eAtRet RegistersSave(AtSurEngine self, uint32 *regVal,
                            AtPmRegister (*Register)(AtPmParam),
                            AtPmRegisterHandler LatchFunc)
    {
    AtPmRegisterHandler NE_LatchFunc = LatchFunc;
    AtPmRegisterHandler FE_LatchFunc = LatchFunc;

    /* Some projects need to not latch CV/ES/SES/UAS value during the duration of
     * a potential UAS change. It is to avoid decremented PM values seen at the
     * end of potential UAS duration when application keeps polling PM value. */
    if (!ThaModuleHardSurShouldUpdatePmRegisterWhenPotentialUasChange(Module(self)))
        {
        if (IsPotentialUas(self, regVal[cAf6_UAS_PNE_Index]))
            NE_LatchFunc = DonotLatchHwValue;
        if (IsPotentialUas(self, regVal[cAf6_UAS_PFE_Index]))
            FE_LatchFunc = DonotLatchHwValue;
        }

    return RegistersLatch(self, regVal, Register, LatchFunc, NE_LatchFunc, FE_LatchFunc);
    }

static eAtRet NearEndParamsAdjust(AtSurEngine self, uint32 *regVal,
                                  AtPmRegister (*Register)(AtPmParam),
                                  AtPmRegisterHandler UasParamFunc,
                                  AtPmRegisterHandler SesParamFunc,
                                  AtPmRegisterHandler OtherParamsFunc)
	{
    mRegistersSave(self, cAtSurEngineSdhPathPmParamCvP, cAf6_Sts_CV_P_Adj, OtherParamsFunc, Register);
    mRegistersSave(self, cAtSurEngineSdhPathPmParamEsP, cAf6_Sts_ES_P_Adj, OtherParamsFunc, Register);
    mRegistersSave(self, cAtSurEngineSdhPathPmParamSesP, cAf6_Sts_UAS_NE_Adj, SesParamFunc, Register);
    mRegistersSave(self, cAtSurEngineSdhPathPmParamUasP, cAf6_Sts_UAS_NE_Adj, UasParamFunc, Register);

    return cAtOk;
	}

static eAtRet FarEndParamsAdjust(AtSurEngine self, uint32 *regVal,
                                 AtPmRegister (*Register)(AtPmParam),
                                 AtPmRegisterHandler UasParamFunc,
                                 AtPmRegisterHandler SesParamFunc,
                                 AtPmRegisterHandler OtherParamsFunc)
    {
    mRegistersSave(self, cAtSurEngineSdhPathPmParamCvPfe, cAf6_Sts_CV_PFE_Adj, OtherParamsFunc, Register);
    mRegistersSave(self, cAtSurEngineSdhPathPmParamEsPfe, cAf6_Sts_ES_PFE_Adj, OtherParamsFunc, Register);
    mRegistersSave(self, cAtSurEngineSdhPathPmParamSesPfe, cAf6_Sts_UAS_FE_Adj, SesParamFunc, Register);
    mRegistersSave(self, cAtSurEngineSdhPathPmParamUasPfe, cAf6_Sts_UAS_FE_Adj, UasParamFunc, Register);

    return cAtOk;
    }

static uint32 UasParamStateGet(AtSurEngine self, uint32 prevRegVal, uint32 currRegVal)
    {
    uint32 stkUas = mRegField(currRegVal, cAf6_UAS_Sticky_);
    uint32 curSes = mRegField(currRegVal, cAf6_UAS_Ses_T2_);
    uint32 prevUas = mRegField(prevRegVal, cAf6_UAS_State_);
    uint32 prevSes = mRegField(prevRegVal, cAf6_UAS_Ses_T1_);
    AtUnused(self);

    if (!stkUas && ((curSes + prevSes) == 10) && (prevSes != 0))
        return (prevUas) ? cThaHardSurUasParamExitting : cThaHardSurUasParamEntering;

    return cThaHardSurUasParamUnknown;
    }

/*
 * Latch & adjust PM counters from hardware to PM previous register.
 *
 * @param self              This engine
 * @param regVal            Buffers read from hardware
 * @param Register          PM register to latch to
 * @param LatchFunc         Latch function for PM parameters
 * @return AT return code
 */
static eAtRet RegistersAdjust(AtSurEngine self, uint32 *regVal,
                              AtPmRegister (*Register)(AtPmParam),
                              AtPmRegisterHandler LatchFunc)
    {
    uint32 latchedSesp = mRegField(regVal[cAf6_UAS_PNE_Index], cAf6_UAS_Ses_T1_);
    uint32 latchedSespfe = mRegField(regVal[cAf6_UAS_PFE_Index], cAf6_UAS_Ses_T1_);
    uint32 regCurrent[cNumDdrHoldRegisters];
    eAtRet ret;
    uint32 uasNeState, uasFeState;

    RegistersLatch(self, regVal, Register, LatchFunc, LatchFunc, LatchFunc);
    if (!latchedSesp && !latchedSespfe)
        return cAtOk;

    ret = ThaModuleHardSurPathDdrCountersR2c(self, cAtPmCurrentPeriodRegister, regCurrent, cAtFalse);
    if (ret != cAtOk)
        return ret;

    uasNeState = UasParamStateGet(self, regVal[cAf6_UAS_PNE_Index], regCurrent[cAf6_UAS_PNE_Index]);
    uasFeState = UasParamStateGet(self, regVal[cAf6_UAS_PFE_Index], regCurrent[cAf6_UAS_PFE_Index]);

    if (uasNeState == cThaHardSurUasParamEntering)
        NearEndParamsAdjust(self, regVal, Register, AddHwValue, SubstractHwValue, SubstractHwValue);
    else if (uasNeState == cThaHardSurUasParamExitting)
        NearEndParamsAdjust(self, regVal, Register, SubstractHwValue, KeepHwValue, AddHwValue);

    if (uasFeState == cThaHardSurUasParamEntering)
        FarEndParamsAdjust(self, regVal, Register, AddHwValue, SubstractHwValue, SubstractHwValue);
    else if (uasFeState == cThaHardSurUasParamExitting)
        FarEndParamsAdjust(self, regVal, Register, SubstractHwValue, KeepHwValue, AddHwValue);

    return cAtOk;
    }

static eAtRet PeriodExpire(AtSurEngine self, void *data)
    {
    uint32 *regVal = (uint32*)data;

    m_AtSurEngineMethods->PeriodExpire(self, data);

    return RegistersSave(self, regVal, AtPmParamPreviousPeriodRegister, PeriodShift);
    }

static eAtRet AllCountersLatchAndClear(AtSurEngine self, eBool clear)
    {
    uint32 regVal[cNumDdrHoldRegisters];
    eAtRet ret;

    ret = ThaModuleHardSurPathDdrCountersR2c(self, cAtPmCurrentPeriodRegister, regVal, clear);
    if (ret != cAtOk)
        return ret;

    return RegistersSave(self, regVal, AtPmParamCurrentPeriodRegister, LatchHwValue);
    }

static eAtRet RegistersLatchAndClear(AtSurEngine self, eAtPmRegisterType type, eBool clear)
    {
    uint32 regVal[cNumDdrHoldRegisters];
    eAtRet ret;

    ret = ThaModuleHardSurPathDdrCountersR2c(self, type, regVal, clear);
    if (ret != cAtOk)
        return ret;

    if (type == cAtPmCurrentPeriodRegister)
        return RegistersSave(self, regVal, AtPmParamCurrentPeriodRegister, LatchHwValue);
    if (type == cAtPmPreviousPeriodRegister)
        return RegistersAdjust(self, regVal, AtPmParamPreviousPeriodRegister, LatchHwValue);
    return cAtErrorModeNotSupport;
    }

static uint32 SavedValid(AtSurEngine self, uint32 paramType, AtPmRegister (*Register)(AtPmParam))
    {
    AtPmParam param = AtSurEnginePmParamGet(self, paramType);
    return AtPmRegisterIsValid(Register(param)) ? 0 : paramType;
    }

static uint32 RegistersInvalidFlagsGet(AtSurEngine self, AtPmRegister (*Register)(AtPmParam))
    {
    uint32 flags = 0;
    flags |= SavedValid(self, cAtSurEngineSdhPathPmParamCvPfe, Register);
    flags |= SavedValid(self, cAtSurEngineSdhPathPmParamEsPfe, Register);
    flags |= SavedValid(self, cAtSurEngineSdhPathPmParamSesPfe, Register);
    flags |= SavedValid(self, cAtSurEngineSdhPathPmParamUasPfe, Register);
    flags |= SavedValid(self, cAtSurEngineSdhPathPmParamFcPfe, Register);

    return flags;
    }

static uint32 PmInvalidFlagsGet(AtSurEngine self, eAtPmRegisterType type)
    {
    if (type == cAtPmCurrentPeriodRegister)
        return RegistersInvalidFlagsGet(self, AtPmParamCurrentPeriodRegister);

    if (type == cAtPmPreviousPeriodRegister)
        return RegistersInvalidFlagsGet(self, AtPmParamPreviousPeriodRegister);

    return 0;
    }

static uint32 DefaultOffset(AtSurEngine self)
    {
    uint8 sliceId, hwStsInSlice;
    AtSdhChannel au = AtSdhChannelParentChannelGet((AtSdhChannel)AtSurEngineChannelGet((AtSurEngine)self));

    if (ThaSdhChannel2HwMasterStsId(au, cAtModuleSur, &sliceId, &hwStsInSlice) != cAtOk)
        return cInvalidUint32;

    return ThaModuleHardSurStsOffset(Module(self), sliceId, hwStsInSlice);
    }

static eAtRet HwNotificationEnable(AtSurEngine self, uint32 baseAddr, eBool enable)
    {
    ThaModuleHardSur module = Module(self);
    if (module && FailureIsSupported(self))
		{
    	eAtRet ret;
    uint8 sliceId, hwStsInSlice;
    uint32 bitMask;
    uint32 regVal;
    uint32 regAddr;
    AtSdhChannel au = AtSdhChannelParentChannelGet((AtSdhChannel)AtSurEngineChannelGet((AtSurEngine)self));

    ret = ThaSdhChannel2HwMasterStsId(au, cAtModuleSur, &sliceId, &hwStsInSlice);
    if (ret != cAtOk)
        return ret;

    regAddr = ThaModuleHardSurBaseAddress(module) + (uint32)(baseAddr + sliceId);
    bitMask = cBit0 << hwStsInSlice;

    regVal = mModuleHwRead(module, regAddr);
    regVal = (enable) ? (regVal | bitMask) : (regVal & ~bitMask);
    mModuleHwWrite(module, regAddr, regVal);
		}
    return cAtOk;
    }

static eBool HwNotificationIsEnabled(AtSurEngine self, uint32 baseAddr)
    {
    ThaModuleHardSur module = Module(self);
    if (module && FailureIsSupported(self))
		{
    	eAtRet ret;
    uint8 sliceId, hwStsInSlice;
    uint32 bitMask;
    uint32 regVal;
    uint32 regAddr;
    AtSdhChannel au = AtSdhChannelParentChannelGet((AtSdhChannel)AtSurEngineChannelGet((AtSurEngine)self));

    ret = ThaSdhChannel2HwMasterStsId(au, cAtModuleSur, &sliceId, &hwStsInSlice);
    if (ret != cAtOk)
        return cAtFalse;

    regAddr = ThaModuleHardSurBaseAddress(module) + (uint32)(baseAddr + sliceId);
    bitMask = cBit0 << hwStsInSlice;

    regVal = mModuleHwRead(module, regAddr);
    return (regVal & bitMask) ? cAtTrue : cAtFalse;
    }
    return cAtFalse;
    }

static eAtRet FailureNotificationEnable(AtSurEngine self, eBool enable)
    {
    return HwNotificationEnable(self, cAf6Reg_FMPM_FM_STS24_Interrupt_MASK_Per_STS1_Base, enable);
    }

static eBool FailureNotificationIsEnabled(AtSurEngine self)
    {
    return HwNotificationIsEnabled(self, cAf6Reg_FMPM_FM_STS24_Interrupt_MASK_Per_STS1_Base);
    }

static eAtRet TcaNotificationEnable(AtSurEngine self, eBool enable)
    {
    return HwNotificationEnable(self, cAf6Reg_FMPM_TCA_STS24_Interrupt_MASK_Per_STS1_Base, enable);
    }

static eBool TcaNotificationIsEnabled(AtSurEngine self)
    {
    return HwNotificationIsEnabled(self, cAf6Reg_FMPM_TCA_STS24_Interrupt_MASK_Per_STS1_Base);
    }

static void FailureNotificationProcess(AtSurEngine self, uint32 offset)
    {
    ThaModuleHardSur module = Module(self);
    if (module && FailureIsSupported(self))
		{
    uint32 regAddr = ThaModuleHardSurStsCurrentFailuresAddr(module) + offset;
    uint32 regVal = mModuleHwRead(module, regAddr);
    uint32 events = 0;
    uint32 failures = 0;
    uint32 currentFailures = FailureMaskGet(regVal);

    mGetFmIntrStatus(cAtSdhPathAlarmAis,    AIS,    failures, events);
    mGetFmIntrStatus(cAtSdhPathAlarmLop,    LOP,    failures, events);
    mGetFmIntrStatus(cAtSdhPathAlarmTim,    TIM,    failures, events);
    mGetFmIntrStatus(cAtSdhPathAlarmRfi,    RFI,    failures, events);
    mGetFmIntrStatus(cAtSdhPathAlarmRfiS,   RFI_SER,failures, events);
    mGetFmIntrStatus(cAtSdhPathAlarmRfiC,   RFI_CON,failures, events);
    mGetFmIntrStatus(cAtSdhPathAlarmRfiP,   RFI_PAY,failures, events);
    mGetFmIntrStatus(cAtSdhPathAlarmUneq,   UNEQ,   failures, events);
    mGetFmIntrStatus(cAtSdhPathAlarmPlm,    PLM,    failures, events);
    mGetFmIntrStatus(cAtSdhPathAlarmLom,    LOM,    failures, events);
    mGetFmIntrStatus(cAtSdhPathAlarmBerSd,  BERSD,  failures, events);
    mGetFmIntrStatus(cAtSdhPathAlarmBerSf,  BERSF,  failures, events);

    /* Only clear masked interrupt. */
    regVal = (regVal >> 16) & cBit15_0;

    /* Clear interrupt */
    mModuleHwWrite(module, ThaModuleHardSurStsFailuresInterruptAddr(module) + offset, regVal);

    AtSurEngineFailureNotify(self, events, failures);

    AtSdhChannelFailureForwardingClearanceNotifyWithSubChannelOnly((AtSdhChannel)AtSurEngineChannelGet(self), currentFailures, cAtTrue);
    }
    }

static void TcaNotificationProcess(AtSurEngine self, uint32 offset)
    {
    ThaModuleHardSur module = Module(self);
    uint32 intrAddr = ThaModuleHardSurStsTcaInterruptAddr(module) + offset;
    uint32 maskAddr = ThaModuleHardSurStsTcaInterruptMaskAddr(module) + offset;
    uint32 intrVal = mModuleHwRead(module, intrAddr);
    uint32 maskVal = mModuleHwRead(module, maskAddr);
    uint32 params = 0;

    /* Filter unmasked stickies. */
    intrVal &= maskVal;
    params = TcaMaskGet(intrVal);

    /* Clear interrupt */
    mModuleHwWrite(module, intrAddr, intrVal);

    AtSurEngineAllParamsPeriodTcaNotify(self, params);
    }

static eBool PmParamIsSupported(AtSurEngine self, uint32 paramType)
    {
    switch (paramType)
        {
        /* Hardware engine would not support following parameters as they can be
         * calculated from current supported parameters. */
        case cAtSurEngineSdhPathPmParamEfsP:   return cAtFalse;
        case cAtSurEngineSdhPathPmParamEfsPfe: return cAtFalse;
        case cAtSurEngineSdhPathPmParamAsP:    return cAtFalse;
        case cAtSurEngineSdhPathPmParamAsPfe:  return cAtFalse;
        case cAtSurEngineSdhPathPmParamEbP:    return cAtFalse;
        case cAtSurEngineSdhPathPmParamBbeP:   return cAtFalse;
        case cAtSurEngineSdhPathPmParamEsrP:   return cAtFalse;
        case cAtSurEngineSdhPathPmParamSesrP:  return cAtFalse;
        case cAtSurEngineSdhPathPmParamBberP:  return cAtFalse;

        default:
            return m_AtSurEngineMethods->PmParamIsSupported(self, paramType);
        }
    }

static void OverrideAtSurEngine(AtSurEngine self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSurEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSurEngineOverride, m_AtSurEngineMethods, sizeof(m_AtSurEngineOverride));

        mMethodOverride(m_AtSurEngineOverride, PmEnable);
        mMethodOverride(m_AtSurEngineOverride, PmIsEnabled);
        mMethodOverride(m_AtSurEngineOverride, AutonomousFailuresGet);
        mMethodOverride(m_AtSurEngineOverride, NonAutonomousFailuresGet);
        mMethodOverride(m_AtSurEngineOverride, PmCanEnable);
        mMethodOverride(m_AtSurEngineOverride, FailureChangedHistoryGet);
        mMethodOverride(m_AtSurEngineOverride, FailureChangedHistoryClear);
        mMethodOverride(m_AtSurEngineOverride, FailureNotificationMaskSet);
        mMethodOverride(m_AtSurEngineOverride, FailureNotificationMaskGet);
        mMethodOverride(m_AtSurEngineOverride, TcaChangedHistoryGet);
        mMethodOverride(m_AtSurEngineOverride, TcaChangedHistoryClear);
        mMethodOverride(m_AtSurEngineOverride, TcaNotificationMaskSet);
        mMethodOverride(m_AtSurEngineOverride, TcaNotificationMaskGet);
        mMethodOverride(m_AtSurEngineOverride, SesThresholdPoolEntrySet);
        mMethodOverride(m_AtSurEngineOverride, SesThresholdPoolEntryGet);
        mMethodOverride(m_AtSurEngineOverride, TcaThresholdPoolEntrySet);
        mMethodOverride(m_AtSurEngineOverride, TcaThresholdPoolEntryGet);
        mMethodOverride(m_AtSurEngineOverride, TcaPeriodThresholdValueGet);
        mMethodOverride(m_AtSurEngineOverride, AllCountersLatchAndClear);
        mMethodOverride(m_AtSurEngineOverride, RegistersLatchAndClear);
        mMethodOverride(m_AtSurEngineOverride, PeriodExpire);
        mMethodOverride(m_AtSurEngineOverride, DefaultOffset);
        mMethodOverride(m_AtSurEngineOverride, FailureNotificationEnable);
        mMethodOverride(m_AtSurEngineOverride, FailureNotificationIsEnabled);
        mMethodOverride(m_AtSurEngineOverride, FailureNotificationProcess);
        mMethodOverride(m_AtSurEngineOverride, PmParamIsSupported);
        mMethodOverride(m_AtSurEngineOverride, TcaNotificationEnable);
        mMethodOverride(m_AtSurEngineOverride, TcaNotificationIsEnabled);
        mMethodOverride(m_AtSurEngineOverride, TcaNotificationProcess);
        mMethodOverride(m_AtSurEngineOverride, PmInvalidFlagsGet);
        mMethodOverride(m_AtSurEngineOverride, SesThresholdProfileGet);
        }

    mMethodsSet(self, &m_AtSurEngineOverride);
    }

static void Override(AtSurEngine self)
    {
    OverrideAtSurEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaHardSurEngineSdhPath);
    }

AtSurEngine ThaHardSurEngineSdhPathObjectInit(AtSurEngine self, AtModuleSur module, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (AtSurEngineSdhPathObjectInit(self, module, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSurEngine ThaHardSurEngineSdhPathNew(AtModuleSur module, AtChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSurEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return ThaHardSurEngineSdhPathObjectInit(newEngine, module, channel);
    }

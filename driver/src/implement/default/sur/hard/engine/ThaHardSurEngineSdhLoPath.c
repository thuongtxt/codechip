/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : ThaHardSurEngineSdhLoPath.c
 *
 * Created Date: Jun 10, 2015
 *
 * Description : SDH LO path Surveillance engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../../generic/sdh/AtSdhChannelInternal.h"
#include "../../../ocn/ThaModuleOcn.h"
#include "../ThaModuleHardSurReg.h"
#include "ThaHardSurEngineInternal.h"
#include "AtPmThresholdProfile.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

#define mGetIntrBitMask(regVal_, alarmType_, enableMask_, type_, field_)       \
    (enableMask_) |= ((regVal_) & cAf6_FMPM_##type_##_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_##type_##_VTTU_STK_##field_##_Mask) ? alarmType_ : 0

#define mSetIntrBitMask(alarmType_, defectMask_, enableMask_, type_, field_)   \
    do                                                                         \
        {                                                                      \
        if (defectMask_ & alarmType_)                                          \
            regVal = (enableMask_ & alarmType_) ? \
                    (regVal | cAf6_FMPM_##type_##_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_##type_##_VTTU_MSK_##field_##_Mask) :\
                    (regVal & (~cAf6_FMPM_##type_##_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_##type_##_VTTU_MSK_##field_##_Mask)); \
        }while(0)

#define mGetFmIntrStatus(alarmType_, field_, status_, event_)                  \
    do {                                                                       \
        if (regVal & cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_AMSK_##field_##_Mask) \
            {                                                                  \
            event_ |= alarmType_;                                              \
            if (regVal & cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_CUR_##field_##_Mask) \
                status_ |= alarmType_;                                         \
            }                                                                  \
       } while (0)

#define mGetFmStatus(regVal_, field_)                                          \
    mRegField(regVal_, cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_CUR_##field_##_)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSurEngineMethods       m_AtSurEngineOverride;

/* Save super implementation */
static const tAtSurEngineMethods *m_AtSurEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleHardSur Module(AtSurEngine self)
    {
    return (ThaModuleHardSur)AtSurEngineModuleGet(self);
    }

static eBool FailureIsSupported(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    if (module)
        return ThaModuleHardSurFailureIsSupported(module);
    return cAtFalse;
    }

static uint32 DefaultSesProfileId(AtSdhVc vc)
    {
    if (AtSdhChannelTypeGet((AtSdhChannel)vc) == cAtSdhChannelTypeVc3) /* TU-3 VC */
        return 0;

    return 1;
    }

static AtPmSesThresholdProfile DefaultSesProfile(AtSurEngine self)
    {
    AtSdhVc vc = (AtSdhVc)AtSurEngineChannelGet(self);
    return AtModuleSurSesThresholdProfileGet((AtModuleSur)Module(self), DefaultSesProfileId(vc));
    }

static eAtRet Init(AtSurEngine self)
    {
    eAtRet ret = m_AtSurEngineMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    ret = AtSurEngineSesThresholdProfileSet(self, DefaultSesProfile(self));
    return ret;
    }

static uint32 HwPmEnableRegister(ThaModuleHardSur module)
    {
    return (uint32)(ThaModuleHardSurBaseAddress(module) + cAf6Reg_FMPM_Enable_PM_Collection_VT_Base);
    }

static eAtRet HwPmEnable(AtSurEngine self, eBool enable)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr = HwPmEnableRegister(module) + AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    mRegFieldSet(regVal, cAf6_FMPM_Enable_PM_Collection_VT_enb_col_vt_, mBoolToBin(enable));
    mModuleHwWrite(module, regAddr, regVal);
    return cAtOk;
    }

static eBool HwPmIsEnabled(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr = HwPmEnableRegister(module) + AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    return (regVal & cAf6_FMPM_Enable_PM_Collection_VT_enb_col_vt_Mask) ? cAtTrue : cAtFalse;
    }

static eAtRet PmEnable(AtSurEngine self, eBool enable)
    {
    if (ThaModuleHardSurCanEnableHwPmEngine(Module(self)))
        return HwPmEnable(self, enable);

    return enable ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool PmIsEnabled(AtSurEngine self)
    {
    if (ThaModuleHardSurCanEnableHwPmEngine(Module(self)))
        return HwPmIsEnabled(self);

    return cAtTrue;
    }

static eAtRet SesThresholdPoolEntrySet(AtSurEngine self, uint32 entryIndex)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr = ThaModuleHardSurVtThresholdRegister(module) +
                     AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    mRegFieldSet(regVal, cAf6_FMPM_Threshold_Type_of_VT_K_TYPE_, entryIndex);
    mModuleHwWrite(module, regAddr, regVal);

    return cAtOk;
    }

static uint32 SesThresholdPoolEntryGet(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr = ThaModuleHardSurVtThresholdRegister(module) +
                     AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    return mRegField(regVal, cAf6_FMPM_Threshold_Type_of_VT_K_TYPE_);
    }

static eAtRet TcaThresholdPoolEntrySet(AtSurEngine self, uint32 entryIndex)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr = ThaModuleHardSurVtThresholdRegister(module) +
                     AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    uint32 tcaTypeMask = mMethodsGet(module)->VtTcaTypeMask(module);
    uint32 tcaTypeShift = mMethodsGet(module)->VtTcaTypeShift(module);
    mRegFieldSet(regVal, tcaType, entryIndex);
    mModuleHwWrite(module, regAddr, regVal);

    return cAtOk;
    }

static uint32 TcaThresholdPoolEntryGet(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr = ThaModuleHardSurVtThresholdRegister(module) +
                     AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    uint32 tcaTypeMask = mMethodsGet(module)->VtTcaTypeMask(module);
    uint32 tcaTypeShift = mMethodsGet(module)->VtTcaTypeShift(module);
    return mRegField(regVal, tcaType);
    }

static uint32 TcaPeriodThresholdValueGet(AtSurEngine self, uint32 paramType)
    {
    AtPmThresholdProfile profile = AtSurEngineThresholdProfileGet(self);
    if (profile)
        return AtPmThresholdProfileSdhLoPathPeriodTcaThresholdGet(profile, paramType);
    return 0;
    }

static uint32 FailureMaskGet(uint32 regVal)
    {
    uint32 failures = 0;

    mGetIntrBitMask(regVal, cAtSdhPathAlarmAis,   failures, FM,   AIS);
    mGetIntrBitMask(regVal, cAtSdhPathAlarmLop,   failures, FM,   LOP);
    mGetIntrBitMask(regVal, cAtSdhPathAlarmRfi,   failures, FM,   RFI);
    mGetIntrBitMask(regVal, cAtSdhPathAlarmRfiS,  failures, FM,   RFI_SER);
    mGetIntrBitMask(regVal, cAtSdhPathAlarmRfiC,  failures, FM,   RFI_CON);
    mGetIntrBitMask(regVal, cAtSdhPathAlarmRfiP,  failures, FM,   RFI_PAY);
    mGetIntrBitMask(regVal, cAtSdhPathAlarmTim,   failures, FM,   TIM);
    mGetIntrBitMask(regVal, cAtSdhPathAlarmUneq,  failures, FM,  UNEQ);
    mGetIntrBitMask(regVal, cAtSdhPathAlarmPlm,   failures, FM,   PLM);
    mGetIntrBitMask(regVal, cAtSdhPathAlarmBerSd, failures, FM, BERSD);
    mGetIntrBitMask(regVal, cAtSdhPathAlarmBerSf, failures, FM, BERSF);

    return failures;
    }

static uint32 AutonomousFailuresGet(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    if (module && FailureIsSupported(self))
		{
    uint32 regAddr = ThaModuleHardSurVtCurrentFailuresAddr(module) +
                     AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    return FailureMaskGet(regVal);
    }
    return 0;
    }

static uint32 NonAutonomousFailuresGet(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    if (module && FailureIsSupported(self))
		{
    uint32 regAddr;
    uint32 regVal;

    if (!ThaModuleHardSurFailureForwardingStatusIsSupported(module))
        return 0;

    regAddr = ThaModuleHardSurVtCurrentFailuresAddr(module) + AtSurEngineDefaultOffset(self);
    regVal = mModuleHwRead(module, regAddr);
    if (mGetFmStatus(regVal, HI_AIS))
        return cAtSdhPathAlarmAis;
		}
    return 0;
    }

static uint32 FailureRead2Clear(AtSurEngine self, eBool read2Clear)
    {
    ThaModuleHardSur module = Module(self);
    if (module && FailureIsSupported(self))
		{
    uint32 regAddr = ThaModuleHardSurVtFailuresInterruptAddr(module) +
                     AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    uint32 failures = FailureMaskGet(regVal);

    if (read2Clear)
        mModuleHwWrite(module, regAddr, regVal);

    return failures;
    }
    return 0;
    }

static uint32 FailureChangedHistoryGet(AtSurEngine self)
    {
    return FailureRead2Clear(self, cAtFalse);
    }

static uint32 FailureChangedHistoryClear(AtSurEngine self)
    {
    return FailureRead2Clear(self, cAtTrue);
    }

static eAtRet FailureNotificationMaskSet(AtSurEngine self, uint32 failureMask, uint32 enableMask)
    {
    ThaModuleHardSur module = Module(self);
    if (module && FailureIsSupported(self))
		{
    uint32 regAddr = ThaModuleHardSurVtFailuresInterruptMaskAddr(module) +
                     AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);

    mSetIntrBitMask(cAtSdhPathAlarmLop,    failureMask, enableMask, FM,   LOP);
    mSetIntrBitMask(cAtSdhPathAlarmAis,    failureMask, enableMask, FM,   AIS);
    mSetIntrBitMask(cAtSdhPathAlarmUneq,   failureMask, enableMask, FM,  UNEQ);
    mSetIntrBitMask(cAtSdhPathAlarmTim,    failureMask, enableMask, FM,   TIM);
    mSetIntrBitMask(cAtSdhPathAlarmPlm,    failureMask, enableMask, FM,   PLM);
    mSetIntrBitMask(cAtSdhPathAlarmRfi,    failureMask, enableMask, FM,   RFI);
    mSetIntrBitMask(cAtSdhPathAlarmRfiS,   failureMask, enableMask, FM,   RFI_SER);
    mSetIntrBitMask(cAtSdhPathAlarmRfiC,   failureMask, enableMask, FM,   RFI_CON);
    mSetIntrBitMask(cAtSdhPathAlarmRfiP,   failureMask, enableMask, FM,   RFI_PAY);
    mSetIntrBitMask(cAtSdhPathAlarmBerSd,  failureMask, enableMask, FM, BERSD);
    mSetIntrBitMask(cAtSdhPathAlarmBerSf,  failureMask, enableMask, FM, BERSF);
    mModuleHwWrite(module, regAddr, regVal);

    if (regVal)
        return AtSurEngineFailureNotificationEnable(self, cAtTrue);
    else
        return AtSurEngineFailureNotificationEnable(self, cAtFalse);
		}
    return cAtOk;
    }

static uint32 FailureNotificationMaskGet(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    if (module && FailureIsSupported(self))
		{
    uint32 regAddr = ThaModuleHardSurVtFailuresInterruptMaskAddr(module) +
                     AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    return FailureMaskGet(regVal);
    }
    return 0;
    }

static uint32 TcaMaskGet(uint32 regVal)
    {
    uint32 tcaMask = 0;

    mGetIntrBitMask(regVal, cAtSurEngineSdhPathPmParamPpjcPdet, tcaMask, TCA, PPJC_VDet);
    mGetIntrBitMask(regVal, cAtSurEngineSdhPathPmParamNpjcPdet, tcaMask, TCA, NPJC_VDet);
    mGetIntrBitMask(regVal, cAtSurEngineSdhPathPmParamPpjcPgen, tcaMask, TCA, PPJC_VGen);
    mGetIntrBitMask(regVal, cAtSurEngineSdhPathPmParamNpjcPgen, tcaMask, TCA, NPJC_VGen);
    mGetIntrBitMask(regVal, cAtSurEngineSdhPathPmParamPjcDiff,  tcaMask, TCA, PJCDiff_V);
    mGetIntrBitMask(regVal, cAtSurEngineSdhPathPmParamPjcsPdet, tcaMask, TCA, PJCS_VDet);
    mGetIntrBitMask(regVal, cAtSurEngineSdhPathPmParamPjcsPgen, tcaMask, TCA, PJCS_VGen);
    mGetIntrBitMask(regVal, cAtSurEngineSdhPathPmParamCvP,      tcaMask, TCA, CV_V);
    mGetIntrBitMask(regVal, cAtSurEngineSdhPathPmParamEsP,      tcaMask, TCA, ES_V);
    mGetIntrBitMask(regVal, cAtSurEngineSdhPathPmParamSesP,     tcaMask, TCA, SES_V);
    mGetIntrBitMask(regVal, cAtSurEngineSdhPathPmParamUasP,     tcaMask, TCA, UAS_V);
    mGetIntrBitMask(regVal, cAtSurEngineSdhPathPmParamFcP,      tcaMask, TCA, FC_V);
    mGetIntrBitMask(regVal, cAtSurEngineSdhPathPmParamCvPfe,    tcaMask, TCA, CV_VFE);
    mGetIntrBitMask(regVal, cAtSurEngineSdhPathPmParamEsPfe,    tcaMask, TCA, ES_VFE);
    mGetIntrBitMask(regVal, cAtSurEngineSdhPathPmParamSesPfe,   tcaMask, TCA, SES_VFE);
    mGetIntrBitMask(regVal, cAtSurEngineSdhPathPmParamUasPfe,   tcaMask, TCA, UAS_VFE);
    mGetIntrBitMask(regVal, cAtSurEngineSdhPathPmParamFcPfe,    tcaMask, TCA, FC_VFE);

    return tcaMask;
    }

static eAtRet TcaNotificationMaskSet(AtSurEngine self, uint32 paramMask, uint32 enableMask)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr;
    uint32 regVal;

    if (!AtModuleSurTcaIsSupported((AtModuleSur)module))
        return cAtErrorModeNotSupport;

    regAddr = ThaModuleHardSurVtTcaInterruptMaskAddr(module) + AtSurEngineDefaultOffset(self);
    regVal = mModuleHwRead(module, regAddr);

    mSetIntrBitMask(cAtSurEngineSdhPathPmParamPpjcPdet, paramMask, enableMask, TCA, PPJC_VDet);
    mSetIntrBitMask(cAtSurEngineSdhPathPmParamNpjcPdet, paramMask, enableMask, TCA, NPJC_VDet);
    mSetIntrBitMask(cAtSurEngineSdhPathPmParamPpjcPgen, paramMask, enableMask, TCA, PPJC_VGen);
    mSetIntrBitMask(cAtSurEngineSdhPathPmParamNpjcPgen, paramMask, enableMask, TCA, NPJC_VGen);
    mSetIntrBitMask(cAtSurEngineSdhPathPmParamPjcDiff,  paramMask, enableMask, TCA, PJCDiff_V);
    mSetIntrBitMask(cAtSurEngineSdhPathPmParamPjcsPdet, paramMask, enableMask, TCA, PJCS_VDet);
    mSetIntrBitMask(cAtSurEngineSdhPathPmParamPjcsPgen, paramMask, enableMask, TCA, PJCS_VGen);
    mSetIntrBitMask(cAtSurEngineSdhPathPmParamCvP,      paramMask, enableMask, TCA, CV_V);
    mSetIntrBitMask(cAtSurEngineSdhPathPmParamEsP,      paramMask, enableMask, TCA, ES_V);
    mSetIntrBitMask(cAtSurEngineSdhPathPmParamSesP,     paramMask, enableMask, TCA, SES_V);
    mSetIntrBitMask(cAtSurEngineSdhPathPmParamUasP,     paramMask, enableMask, TCA, UAS_V);
    mSetIntrBitMask(cAtSurEngineSdhPathPmParamFcP,      paramMask, enableMask, TCA, FC_V);
    mSetIntrBitMask(cAtSurEngineSdhPathPmParamCvPfe,    paramMask, enableMask, TCA, CV_VFE);
    mSetIntrBitMask(cAtSurEngineSdhPathPmParamEsPfe,    paramMask, enableMask, TCA, ES_VFE);
    mSetIntrBitMask(cAtSurEngineSdhPathPmParamSesPfe,   paramMask, enableMask, TCA, SES_VFE);
    mSetIntrBitMask(cAtSurEngineSdhPathPmParamUasPfe,   paramMask, enableMask, TCA, UAS_VFE);
    mSetIntrBitMask(cAtSurEngineSdhPathPmParamFcPfe,    paramMask, enableMask, TCA, FC_VFE);

    mModuleHwWrite(module, regAddr, regVal);

    return AtSurEngineTcaNotificationEnable(self, regVal ? cAtTrue : cAtFalse);
    }

static uint32 TcaNotificationMaskGet(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr;
    uint32 regVal;

    if (!AtModuleSurTcaIsSupported((AtModuleSur)module))
        return 0;

    regAddr = ThaModuleHardSurVtTcaInterruptMaskAddr(module) + AtSurEngineDefaultOffset(self);
    regVal = mModuleHwRead(module, regAddr);
    return TcaMaskGet(regVal);
    }

static uint32 TcaChangedHistoryRead2Clear(AtSurEngine self, eBool r2c)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr;
    uint32 regVal;
    uint32 changedHistoryMask  = 0;

    if (!AtModuleSurTcaIsSupported((AtModuleSur)module))
        return 0;

    regAddr = ThaModuleHardSurVtTcaInterruptAddr(module) + AtSurEngineDefaultOffset(self);
    regVal = mModuleHwRead(module, regAddr);
    changedHistoryMask = TcaMaskGet(regVal);

    if (r2c)
        mModuleHwWrite(module, regAddr, regVal);

    return changedHistoryMask;
    }

static uint32 TcaChangedHistoryGet(AtSurEngine self)
    {
    return TcaChangedHistoryRead2Clear(self, cAtFalse);
    }

static uint32 TcaChangedHistoryClear(AtSurEngine self)
    {
    return TcaChangedHistoryRead2Clear(self, cAtTrue);
    }

static uint32 DefaultOffset(AtSurEngine self)
    {
    uint8 sliceId, hwStsInSlice;
    AtSdhChannel tu = AtSdhChannelParentChannelGet((AtSdhChannel)AtSurEngineChannelGet(self));

    if (ThaSdhChannelHwStsGet(tu, cAtModuleSur, AtSdhChannelSts1Get(tu), &sliceId, &hwStsInSlice) != cAtOk)
        return cInvalidUint32;

    return ThaModuleHardSurVtOffset(Module(self), sliceId, hwStsInSlice, AtSdhChannelTug2Get(tu), AtSdhChannelTu1xGet(tu));
    }

static eAtRet HwNotificationEnable(AtSurEngine self, uint32 baseAddr, eBool enable)
    {
    ThaModuleHardSur module = Module(self);
    if (module && FailureIsSupported(self))
		{
    	eAtRet ret=cAtOk;
    AtSdhChannel tu = AtSdhChannelParentChannelGet((AtSdhChannel)AtSurEngineChannelGet(self));
    uint8 sliceId, hwStsInSlice;
    uint32 bitMask;
    uint32 regVal;
    uint32 regAddr;

    ret = ThaSdhChannelHwStsGet(tu, cAtModuleSur, AtSdhChannelSts1Get(tu), &sliceId, &hwStsInSlice);
    if (ret != cAtOk)
        return ret;

    regAddr = ThaModuleHardSurBaseAddress(module) + baseAddr + (uint32)((sliceId << 5) + hwStsInSlice);
    bitMask = cBit0 << ((AtSdhChannelTug2Get(tu) << 2) | AtSdhChannelTu1xGet(tu));

    regVal = mModuleHwRead(module, regAddr);
    regVal = (enable) ? (regVal | bitMask) : (regVal & ~bitMask);
    mModuleHwWrite(module, regAddr, regVal);
		}
    return cAtOk;
    }

static eBool HwNotificationIsEnabled(AtSurEngine self, uint32 baseAddr)
    {
    ThaModuleHardSur module = Module(self);
    if (module && FailureIsSupported(self))
		{
    	eAtRet ret;
    AtSdhChannel tu = AtSdhChannelParentChannelGet((AtSdhChannel)AtSurEngineChannelGet(self));
    uint8 sliceId, hwStsInSlice;
    uint32 bitMask;
    uint32 regVal;
    uint32 regAddr;

    ret = ThaSdhChannelHwStsGet(tu, cAtModuleSur, AtSdhChannelSts1Get(tu), &sliceId, &hwStsInSlice);
    if (ret != cAtOk)
        return cAtFalse;

    regAddr = ThaModuleHardSurBaseAddress(module) + baseAddr + (uint32)((sliceId << 5) + hwStsInSlice);
    bitMask = cBit0 << ((AtSdhChannelTug2Get(tu) << 2) | AtSdhChannelTu1xGet(tu));

    regVal = mModuleHwRead(module, regAddr);
    return (regVal & bitMask) ? cAtTrue : cAtFalse;
    }
    return cAtFalse;
    }

static eAtRet FailureNotificationEnable(AtSurEngine self, eBool enable)
    {
    return HwNotificationEnable(self, cAf6Reg_FMPM_FM_VTTU_Interrupt_MASK_Per_VTTU_Base, enable);
    }

static eBool FailureNotificationIsEnabled(AtSurEngine self)
    {
    return HwNotificationIsEnabled(self, cAf6Reg_FMPM_FM_VTTU_Interrupt_MASK_Per_VTTU_Base);
    }

static eAtRet TcaNotificationEnable(AtSurEngine self, eBool enable)
    {
    return HwNotificationEnable(self, cAf6Reg_FMPM_TCA_VTTU_Interrupt_MASK_Per_VTTU_Base, enable);
    }

static eBool TcaNotificationIsEnabled(AtSurEngine self)
    {
    return HwNotificationIsEnabled(self, cAf6Reg_FMPM_TCA_VTTU_Interrupt_MASK_Per_VTTU_Base);
    }

static void FailureNotificationProcess(AtSurEngine self, uint32 offset)
    {
    ThaModuleHardSur module = Module(self);
    if (module && FailureIsSupported(self))
		{
    uint32 regAddr = ThaModuleHardSurVtCurrentFailuresAddr(module) + offset;
    uint32 regVal = mModuleHwRead(module, regAddr);
    uint32 events = 0;
    uint32 failures = 0;
    uint32 currentFailures = FailureMaskGet(regVal);

    mGetFmIntrStatus(cAtSdhPathAlarmAis,    AIS,    failures, events);
    mGetFmIntrStatus(cAtSdhPathAlarmLop,    LOP,    failures, events);
    mGetFmIntrStatus(cAtSdhPathAlarmTim,    TIM,    failures, events);
    mGetFmIntrStatus(cAtSdhPathAlarmRfi,    RFI,    failures, events);
    mGetFmIntrStatus(cAtSdhPathAlarmRfiS,   RFI_SER,failures, events);
    mGetFmIntrStatus(cAtSdhPathAlarmRfiC,   RFI_CON,failures, events);
    mGetFmIntrStatus(cAtSdhPathAlarmRfiP,   RFI_PAY,failures, events);
    mGetFmIntrStatus(cAtSdhPathAlarmUneq,   UNEQ,   failures, events);
    mGetFmIntrStatus(cAtSdhPathAlarmPlm,    PLM,    failures, events);
    mGetFmIntrStatus(cAtSdhPathAlarmBerSd,  BERSD,  failures, events);
    mGetFmIntrStatus(cAtSdhPathAlarmBerSf,  BERSF,  failures, events);

    /* Only clear masked interrupt. */
    regVal = (regVal >> 16) & cBit15_0;

    /* Clear interrupt */
    mModuleHwWrite(module, ThaModuleHardSurVtFailuresInterruptAddr(module) + offset, regVal);

    AtSurEngineFailureNotify(self, events, failures);

    AtSdhChannelFailureForwardingClearanceNotifyWithSubChannelOnly((AtSdhChannel)AtSurEngineChannelGet(self), currentFailures, cAtTrue);
    }
    }

static void TcaNotificationProcess(AtSurEngine self, uint32 offset)
    {
    ThaModuleHardSur module = Module(self);
    uint32 intrAddr = ThaModuleHardSurVtTcaInterruptAddr(module) + offset;
    uint32 maskAddr = ThaModuleHardSurVtTcaInterruptMaskAddr(module) + offset;
    uint32 intrVal = mModuleHwRead(module, intrAddr);
    uint32 maskVal = mModuleHwRead(module, maskAddr);
    uint32 params = 0;

    /* Filter unmasked stickies. */
    intrVal &= maskVal;
    params = TcaMaskGet(intrVal);

    /* Clear interrupt */
    mModuleHwWrite(module, intrAddr, intrVal);

    AtSurEngineAllParamsPeriodTcaNotify(self, params);
    }

static void OverrideAtSurEngine(AtSurEngine self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSurEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSurEngineOverride, m_AtSurEngineMethods, sizeof(m_AtSurEngineOverride));

        mMethodOverride(m_AtSurEngineOverride, Init);
        mMethodOverride(m_AtSurEngineOverride, PmEnable);
        mMethodOverride(m_AtSurEngineOverride, PmIsEnabled);
        mMethodOverride(m_AtSurEngineOverride, AutonomousFailuresGet);
        mMethodOverride(m_AtSurEngineOverride, NonAutonomousFailuresGet);
        mMethodOverride(m_AtSurEngineOverride, FailureChangedHistoryGet);
        mMethodOverride(m_AtSurEngineOverride, FailureChangedHistoryClear);
        mMethodOverride(m_AtSurEngineOverride, FailureNotificationMaskSet);
        mMethodOverride(m_AtSurEngineOverride, FailureNotificationMaskGet);
        mMethodOverride(m_AtSurEngineOverride, TcaChangedHistoryGet);
        mMethodOverride(m_AtSurEngineOverride, TcaChangedHistoryClear);
        mMethodOverride(m_AtSurEngineOverride, TcaNotificationMaskSet);
        mMethodOverride(m_AtSurEngineOverride, TcaNotificationMaskGet);
        mMethodOverride(m_AtSurEngineOverride, SesThresholdPoolEntrySet);
        mMethodOverride(m_AtSurEngineOverride, SesThresholdPoolEntryGet);
        mMethodOverride(m_AtSurEngineOverride, TcaThresholdPoolEntrySet);
        mMethodOverride(m_AtSurEngineOverride, TcaThresholdPoolEntryGet);
        mMethodOverride(m_AtSurEngineOverride, TcaPeriodThresholdValueGet);
        mMethodOverride(m_AtSurEngineOverride, DefaultOffset);
        mMethodOverride(m_AtSurEngineOverride, FailureNotificationEnable);
        mMethodOverride(m_AtSurEngineOverride, FailureNotificationIsEnabled);
        mMethodOverride(m_AtSurEngineOverride, FailureNotificationProcess);
        mMethodOverride(m_AtSurEngineOverride, TcaNotificationEnable);
        mMethodOverride(m_AtSurEngineOverride, TcaNotificationIsEnabled);
        mMethodOverride(m_AtSurEngineOverride, TcaNotificationProcess);
        }

    mMethodsSet(self, &m_AtSurEngineOverride);
    }

static void Override(AtSurEngine self)
    {
    OverrideAtSurEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaHardSurEngineSdhLoPath);
    }

AtSurEngine ThaHardSurEngineSdhLoPathObjectInit(AtSurEngine self, AtModuleSur module, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (ThaHardSurEngineSdhPathObjectInit(self, module, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSurEngine ThaHardSurEngineSdhLoPathNew(AtModuleSur module, AtChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSurEngine newSurEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newSurEngine == NULL)
        return NULL;

    /* Construct it */
    return ThaHardSurEngineSdhLoPathObjectInit(newSurEngine, module, channel);
    }

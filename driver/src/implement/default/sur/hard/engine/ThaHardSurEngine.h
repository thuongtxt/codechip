/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SUR
 * 
 * File        : ThaHardSurEngine.h
 * 
 * Created Date: Sep 11, 2015
 *
 * Description : Hard SUR engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAHARDSURENGINE_H_
#define _THAHARDSURENGINE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleSur.h"
#include "AtSurEngine.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Concretes */
AtSurEngine ThaHardSurEngineSdhLineNew(AtModuleSur module, AtChannel channel);
AtSurEngine ThaHardSurEngineSdhPathNew(AtModuleSur module, AtChannel channel);
AtSurEngine ThaHardSurEngineSdhLoPathNew(AtModuleSur module, AtChannel channel);
AtSurEngine ThaHardSurEnginePdhDe1New(AtModuleSur module, AtChannel channel);
AtSurEngine ThaHardSurEnginePdhDe3New(AtModuleSur module, AtChannel channel);
AtSurEngine ThaHardSurEnginePwNew(AtModuleSur module, AtChannel channel);

/* Helper functions */
eAtRet ThaHardSurEnginePwNotificationEnable(AtSurEngine self, uint32 baseAddr, eBool enable);
eBool ThaHardSurEnginePwNotificationIsEnabled(AtSurEngine self, uint32 baseAddr);

/* Product concretes */
AtSurEngine Tha60290022SurEnginePwNew(AtModuleSur module, AtChannel channel);

#ifdef __cplusplus
}
#endif
#endif /* _THAHARDSURENGINE_H_ */


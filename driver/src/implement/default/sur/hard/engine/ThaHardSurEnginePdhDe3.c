/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : ThaHardSurEnginePdhDe3.c
 *
 * Created Date: Mar 21, 2015
 *
 * Description : DS3/E3 Surveillance engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPdhDe3.h"
#include "../../../../../generic/pdh/AtPdhChannelInternal.h"
#include "../../../pdh/ThaModulePdh.h"
#include "../ThaModuleHardSurReg.h"
#include "../param/ThaHardPmRegisterInternal.h"
#include "AtPmThresholdProfile.h"
#include "ThaHardSurEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6_UAS_PNE_Index       14
#define cAf6_UAS_CPNE_Index      15
#define cAf6_UAS_PFE_Index       16

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)     ((ThaHardSurEnginePdhDe3)self)

#define mGetIntrBitMask(regVal_, alarmType_, enableMask_, type_, field_)       \
    (enableMask_) |= ((regVal_) & cAf6_FMPM_##type_##_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_##type_##_DS3E3_STK_##field_##_Mask) ? alarmType_ : 0

#define mSetIntrBitMask(alarmType_, defectMask_, enableMask_, type_, field_)   \
    do                                                                         \
        {                                                                      \
        if (defectMask_ & alarmType_)                                          \
            regVal = (enableMask_ & alarmType_) ? \
                    (regVal | cAf6_FMPM_##type_##_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_##type_##_DS3E3_MSK_##field_##_Mask) :\
                    (regVal & (~cAf6_FMPM_##type_##_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_##type_##_DS3E3_MSK_##field_##_Mask)); \
        }while(0)

#define mGetFmIntrStatus(alarmType_, field_, status_, event_)                 \
    do {                                                                       \
        if (regVal & cAf6_FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_AMSK_##field_##_Mask) \
            {                                                                  \
            event_ |= alarmType_;                                              \
            if (regVal & cAf6_FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_CUR_##field_##_Mask) \
                status_ |= alarmType_;                                         \
            }                                                                  \
       } while (0)

#define mGetFmStatus(regVal_, field_)                                          \
    mRegField(regVal_, cAf6_FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_CUR_##field_##_)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSurEngineMethods       m_AtSurEngineOverride;

/* Save super implementation */
static const tAtSurEngineMethods *m_AtSurEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleHardSur Module(AtSurEngine self)
    {
    return (ThaModuleHardSur)AtSurEngineModuleGet(self);
    }

static eBool FailureIsSupported(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    return ThaModuleHardSurFailureIsSupported(module);
    }
    
static uint32 HwPmEnableRegister(ThaModuleHardSur module)
    {
    return (uint32)(ThaModuleHardSurBaseAddress(module) + cAf6Reg_FMPM_Enable_PM_Collection_DS3_Base);
    }

static eAtRet HwPmEnable(AtSurEngine self, eBool enable)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr = HwPmEnableRegister(module) + AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    mRegFieldSet(regVal, cAf6_FMPM_Enable_PM_Collection_DS3_enb_col_ds3_, mBoolToBin(enable));
    mModuleHwWrite(module, regAddr, regVal);
    return cAtOk;
    }

static eBool HwPmIsEnabled(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr = HwPmEnableRegister(module) + AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    return (regVal & cAf6_FMPM_Enable_PM_Collection_DS3_enb_col_ds3_Mask) ? cAtTrue : cAtFalse;
    }

static eAtRet PmEnable(AtSurEngine self, eBool enable)
    {
    if (ThaModuleHardSurCanEnableHwPmEngine(Module(self)))
        return HwPmEnable(self, enable);

    return enable ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool PmIsEnabled(AtSurEngine self)
    {
    if (ThaModuleHardSurCanEnableHwPmEngine(Module(self)))
        return HwPmIsEnabled(self);

    return cAtTrue;
    }

static eBool PmCanEnable(AtSurEngine self, eBool enable)
    {
    if (ThaModuleHardSurCanEnableHwPmEngine((Module(self))))
        return cAtTrue;

    return m_AtSurEngineMethods->PmCanEnable(self, enable);
    }

static eAtRet SesThresholdPoolEntrySet(AtSurEngine self, uint32 entryIndex)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr = ThaModuleHardSurDe3ThresholdRegister(module) +
                     AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    mRegFieldSet(regVal, cAf6_FMPM_Threshold_Type_of_DS3_K_TYPE_, entryIndex);
    mModuleHwWrite(module, regAddr, regVal);

    return cAtOk;
    }

static uint32 SesThresholdPoolEntryGet(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr = ThaModuleHardSurDe3ThresholdRegister(module) +
                     AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    return mRegField(regVal, cAf6_FMPM_Threshold_Type_of_DS3_K_TYPE_);
    }

static eAtRet TcaThresholdPoolEntrySet(AtSurEngine self, uint32 entryIndex)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr = ThaModuleHardSurDe3ThresholdRegister(module) +
                     AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    uint32 tcaTypeMask = mMethodsGet(module)->De3TcaTypeMask(module);
    uint32 tcaTypeShift = mMethodsGet(module)->De3TcaTypeShift(module);
    mRegFieldSet(regVal, tcaType, entryIndex);
    mModuleHwWrite(module, regAddr, regVal);

    return cAtOk;
    }

static uint32 TcaThresholdPoolEntryGet(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr = ThaModuleHardSurDe3ThresholdRegister(module) +
                     AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    uint32 tcaTypeMask = mMethodsGet(module)->De3TcaTypeMask(module);
    uint32 tcaTypeShift = mMethodsGet(module)->De3TcaTypeShift(module);
    return mRegField(regVal, tcaType);
    }

static uint32 TcaPeriodThresholdValueGet(AtSurEngine self, uint32 paramType)
    {
    AtPmThresholdProfile profile = AtSurEngineThresholdProfileGet(self);
    if (profile)
        return AtPmThresholdProfilePdhDe3PeriodTcaThresholdGet(profile, paramType);
    return 0;
    }

static uint32 FailureMaskGet(uint32 regVal)
    {
    uint32 failures = 0;

    mGetIntrBitMask(regVal, cAtPdhDe3AlarmAis, failures, FM, AIS);
    mGetIntrBitMask(regVal, cAtPdhDe3AlarmLos, failures, FM, LOS);
    mGetIntrBitMask(regVal, cAtPdhDe3AlarmLof, failures, FM, LOF);
    mGetIntrBitMask(regVal, cAtPdhDe3AlarmRai, failures, FM, RAI);

    return failures;
    }

static uint32 AutonomousFailuresGet(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    if (module && FailureIsSupported(self))
		{
    uint32 regAddr = ThaModuleHardSurDe3CurrentFailuresAddr(module) +
                     AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    return FailureMaskGet(regVal);
    }
    return 0;
    }

static uint32 NonAutonomousFailuresGet(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    if (module && FailureIsSupported(self))
		{
    uint32 regAddr, regVal;

    if (!ThaModuleHardSurFailureForwardingStatusIsSupported(module))
        return 0;

    regAddr = ThaModuleHardSurDe3CurrentFailuresAddr(module) + AtSurEngineDefaultOffset(self);
    regVal  = mModuleHwRead(module, regAddr);
    if (mGetFmStatus(regVal, HI_AIS))
        return cAtPdhDe3AlarmAis;
		}
    return 0;
    }

static uint32 FailureRead2Clear(AtSurEngine self, eBool read2Clear)
    {
    ThaModuleHardSur module = Module(self);
    if (module && FailureIsSupported(self))
		{
    uint32 regAddr = ThaModuleHardSurDe3FailuresInterruptAddr(module) +
                     AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    uint32 failures = FailureMaskGet(regVal);

    if (read2Clear)
        mModuleHwWrite(module, regAddr, regVal);

    return failures;
    }
    return 0;
    }

static uint32 FailureChangedHistoryGet(AtSurEngine self)
    {
    return FailureRead2Clear(self, cAtFalse);
    }

static uint32 FailureChangedHistoryClear(AtSurEngine self)
    {
    return FailureRead2Clear(self, cAtTrue);
    }

static eAtRet FailureNotificationMaskSet(AtSurEngine self, uint32 failureMask, uint32 enableMask)
    {
    ThaModuleHardSur module = Module(self);
    if (module && FailureIsSupported(self))
		{
    uint32 regAddr = ThaModuleHardSurDe3FailuresInterruptMaskAddr(module) +
                     AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);

    mSetIntrBitMask(cAtPdhDe3AlarmAis, failureMask, enableMask, FM, AIS);
    mSetIntrBitMask(cAtPdhDe3AlarmLos, failureMask, enableMask, FM, LOS);
    mSetIntrBitMask(cAtPdhDe3AlarmLof, failureMask, enableMask, FM, LOF);
    mSetIntrBitMask(cAtPdhDe3AlarmRai, failureMask, enableMask, FM, RAI);
    mModuleHwWrite(module, regAddr, regVal);

    if (regVal)
        return AtSurEngineFailureNotificationEnable(self, cAtTrue);
    else
        return AtSurEngineFailureNotificationEnable(self, cAtFalse);
    }
    return cAtOk;
    }

static uint32 FailureNotificationMaskGet(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    if (module && FailureIsSupported(self))
		{
    uint32 regAddr = ThaModuleHardSurDe3FailuresInterruptMaskAddr(module) +
                     AtSurEngineDefaultOffset(self);
    uint32 regVal = mModuleHwRead(module, regAddr);
    return FailureMaskGet(regVal);
    }
    return 0;
    }

static uint32 TcaMaskGet(uint32 regVal)
    {
    uint32 tcaMask = 0;

    mGetIntrBitMask(regVal, cAtSurEnginePdhDe3PmParamCvL,      tcaMask, TCA, CV_L);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe3PmParamEsL,      tcaMask, TCA, ES_L);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe3PmParamSesL,     tcaMask, TCA, SES_L);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe3PmParamLossL,    tcaMask, TCA, LOSS_L);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe3PmParamCvpP,     tcaMask, TCA, CVP_P);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe3PmParamCvcpP,    tcaMask, TCA, CVCP_P);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe3PmParamEspP,     tcaMask, TCA, ESP_P);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe3PmParamEscpP,    tcaMask, TCA, ESCP_P);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe3PmParamSespP,    tcaMask, TCA, SESP_P);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe3PmParamSescpP,   tcaMask, TCA, SESCP_P);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe3PmParamSasP,     tcaMask, TCA, SAS_P);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe3PmParamAissP,    tcaMask, TCA, AISS_P);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe3PmParamUaspP,    tcaMask, TCA, UAS_P);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe3PmParamUascpP,   tcaMask, TCA, UASCP_P);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe3PmParamCvcpPfe,  tcaMask, TCA, CVCP_PFE);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe3PmParamEscpPfe,  tcaMask, TCA, ESCP_PFE);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe3PmParamSescpPfe, tcaMask, TCA, SESCP_PFE);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe3PmParamSascpPfe, tcaMask, TCA, SASCP_PFE);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe3PmParamUascpPfe, tcaMask, TCA, UASCP_PFE);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe3PmParamEsbcpPfe, tcaMask, TCA, ESBCP_PFE);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe3PmParamEsaL,     tcaMask, TCA, ESA_L);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe3PmParamEsbL,     tcaMask, TCA, ESB_L);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe3PmParamEsapP,    tcaMask, TCA, ESAP_P);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe3PmParamEsacpP,   tcaMask, TCA, ESACP_P);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe3PmParamEsbpP,    tcaMask, TCA, ESBP_P);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe3PmParamEsbcpP,   tcaMask, TCA, ESBCP_P);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe3PmParamFcP,      tcaMask, TCA, FC_P);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe3PmParamEsacpPfe, tcaMask, TCA, ESACP_PFE);
    mGetIntrBitMask(regVal, cAtSurEnginePdhDe3PmParamFccpPfe,  tcaMask, TCA, FCCP_PFE);

    return tcaMask;
    }

static eBool IsDs3Cbit(AtSurEngine self)
    {
    AtPdhDe3 de3 = (AtPdhDe3)AtSurEngineChannelGet(self);
    return AtPdhDe3IsDs3CbitFrameType(AtPdhChannelFrameTypeGet((AtPdhChannel)de3));
    }

static uint32 TcaNotApplicableMaskFilterOut(AtSurEngine self, uint32 tcaMask, uint32 enableMask)
    {
    uint32 ignoredParams;

    if (IsDs3Cbit(self))
        return tcaMask;

    /* Only filter out enabled parameters, when disabling, still allow all masks
     * to be input for cleanup purpose */
    ignoredParams = AtSurEngineDs3CbitParams() & enableMask;
    return (tcaMask & (~ignoredParams));
    }

static eAtRet TcaNotificationMaskSet(AtSurEngine self, uint32 paramMask, uint32 enableMask)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr;
    uint32 regVal;

    if (!AtModuleSurTcaIsSupported((AtModuleSur)module))
        return cAtErrorModeNotSupport;

    paramMask = TcaNotApplicableMaskFilterOut(self, paramMask, enableMask);
    regAddr = ThaModuleHardSurDe3TcaInterruptMaskAddr(module) + AtSurEngineDefaultOffset(self);
    regVal = mModuleHwRead(module, regAddr);

    mSetIntrBitMask(cAtSurEnginePdhDe3PmParamCvL,      paramMask, enableMask, TCA, CV_L);
    mSetIntrBitMask(cAtSurEnginePdhDe3PmParamEsL,      paramMask, enableMask, TCA, ES_L);
    mSetIntrBitMask(cAtSurEnginePdhDe3PmParamSesL,     paramMask, enableMask, TCA, SES_L);
    mSetIntrBitMask(cAtSurEnginePdhDe3PmParamLossL,    paramMask, enableMask, TCA, LOSS_L);
    mSetIntrBitMask(cAtSurEnginePdhDe3PmParamCvpP,     paramMask, enableMask, TCA, CVP_P);
    mSetIntrBitMask(cAtSurEnginePdhDe3PmParamCvcpP,    paramMask, enableMask, TCA, CVCP_P);
    mSetIntrBitMask(cAtSurEnginePdhDe3PmParamEspP,     paramMask, enableMask, TCA, ESP_P);
    mSetIntrBitMask(cAtSurEnginePdhDe3PmParamEscpP,    paramMask, enableMask, TCA, ESCP_P);
    mSetIntrBitMask(cAtSurEnginePdhDe3PmParamSespP,    paramMask, enableMask, TCA, SESP_P);
    mSetIntrBitMask(cAtSurEnginePdhDe3PmParamSescpP,   paramMask, enableMask, TCA, SESCP_P);
    mSetIntrBitMask(cAtSurEnginePdhDe3PmParamSasP,     paramMask, enableMask, TCA, SAS_P);
    mSetIntrBitMask(cAtSurEnginePdhDe3PmParamAissP,    paramMask, enableMask, TCA, AISS_P);
    mSetIntrBitMask(cAtSurEnginePdhDe3PmParamUaspP,    paramMask, enableMask, TCA, UAS_P);
    mSetIntrBitMask(cAtSurEnginePdhDe3PmParamUascpP,   paramMask, enableMask, TCA, UASCP_P);
    mSetIntrBitMask(cAtSurEnginePdhDe3PmParamCvcpPfe,  paramMask, enableMask, TCA, CVCP_PFE);
    mSetIntrBitMask(cAtSurEnginePdhDe3PmParamEscpPfe,  paramMask, enableMask, TCA, ESCP_PFE);
    mSetIntrBitMask(cAtSurEnginePdhDe3PmParamSescpPfe, paramMask, enableMask, TCA, SESCP_PFE);
    mSetIntrBitMask(cAtSurEnginePdhDe3PmParamSascpPfe, paramMask, enableMask, TCA, SASCP_PFE);
    mSetIntrBitMask(cAtSurEnginePdhDe3PmParamUascpPfe, paramMask, enableMask, TCA, UASCP_PFE);
    mSetIntrBitMask(cAtSurEnginePdhDe3PmParamEsbcpPfe, paramMask, enableMask, TCA, ESBCP_PFE);
    mSetIntrBitMask(cAtSurEnginePdhDe3PmParamEsaL,     paramMask, enableMask, TCA, ESA_L);
    mSetIntrBitMask(cAtSurEnginePdhDe3PmParamEsbL,     paramMask, enableMask, TCA, ESB_L);
    mSetIntrBitMask(cAtSurEnginePdhDe3PmParamEsapP,    paramMask, enableMask, TCA, ESAP_P);
    mSetIntrBitMask(cAtSurEnginePdhDe3PmParamEsacpP,   paramMask, enableMask, TCA, ESACP_P);
    mSetIntrBitMask(cAtSurEnginePdhDe3PmParamEsbpP,    paramMask, enableMask, TCA, ESBP_P);
    mSetIntrBitMask(cAtSurEnginePdhDe3PmParamEsbcpP,   paramMask, enableMask, TCA, ESBCP_P);
    mSetIntrBitMask(cAtSurEnginePdhDe3PmParamFcP,      paramMask, enableMask, TCA, FC_P);
    mSetIntrBitMask(cAtSurEnginePdhDe3PmParamEsacpPfe, paramMask, enableMask, TCA, ESACP_PFE);
    mSetIntrBitMask(cAtSurEnginePdhDe3PmParamFccpPfe,  paramMask, enableMask, TCA, FCCP_PFE);
    mModuleHwWrite(module, regAddr, regVal);

    return AtSurEngineTcaNotificationEnable(self, regVal ? cAtTrue : cAtFalse);
    }

static uint32 TcaNotificationMaskGet(AtSurEngine self)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr;
    uint32 regVal;

    if (!AtModuleSurTcaIsSupported((AtModuleSur)module))
        return 0;

    regAddr = ThaModuleHardSurDe3TcaInterruptMaskAddr(module) + AtSurEngineDefaultOffset(self);
    regVal = mModuleHwRead(module, regAddr);
    return TcaMaskGet(regVal);
    }

static uint32 TcaChangedHistoryRead2Clear(AtSurEngine self, eBool r2c)
    {
    ThaModuleHardSur module = Module(self);
    uint32 regAddr;
    uint32 regVal;
    uint32 changedHistoryMask  = 0;

    if (!AtModuleSurTcaIsSupported((AtModuleSur)module))
        return 0;

    regAddr = ThaModuleHardSurDe3TcaInterruptAddr(module) + AtSurEngineDefaultOffset(self);
    regVal = mModuleHwRead(module, regAddr);
    changedHistoryMask = TcaNotApplicableMaskFilterOut(self, TcaMaskGet(regVal), 0);

    if (r2c)
        mModuleHwWrite(module, regAddr, regVal);

    return changedHistoryMask;
    }

static uint32 TcaChangedHistoryGet(AtSurEngine self)
    {
    return TcaChangedHistoryRead2Clear(self, cAtFalse);
    }

static uint32 TcaChangedHistoryClear(AtSurEngine self)
    {
    return TcaChangedHistoryRead2Clear(self, cAtTrue);
    }

#include "ThaHardSurEngineCommon.h"

static eAtRet SaveValid(AtSurEngine self, uint32 *regVal, AtPmRegister (*Register)(AtPmParam))
    {
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamCvcpPfe,    cAf6_Ds3_PFE_Invalid, LatchHwValid, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamEscpPfe,    cAf6_Ds3_PFE_Invalid, LatchHwValid, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamEsacpPfe,   cAf6_Ds3_PFE_Invalid, LatchHwValid, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamEsbcpPfe,   cAf6_Ds3_PFE_Invalid, LatchHwValid, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamSescpPfe,   cAf6_Ds3_PFE_Invalid, LatchHwValid, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamSascpPfe,   cAf6_Ds3_PFE_Invalid, LatchHwValid, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamUascpPfe,   cAf6_Ds3_PFE_Invalid, LatchHwValid, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamFccpPfe,    cAf6_Ds3_PFE_Invalid, LatchHwValid, Register);

    return cAtOk;
    }

/*
 * Latch PM counters from hardware to PM register.
 *
 * @param self          This engine
 * @param regVal        Buffers read from hardware
 * @param Register      PM register to latch to
 * @param LatchFunc     Latch function for normal PM parameters
 * @param NE_LatchFunc  Latch function for near-end PM CV/ES/SES/UAS parameters
 * @param NECP_LatchFunc  Latch function for near-end PM CV/ES/SES/UAS-CP parameters
 * @param FECP_LatchFunc  Latch function for far-end PM CV/ES/SES/UAS-CP parameters
 * @return AT return code
 */
static eAtRet RegistersLatch(AtSurEngine self, uint32 *regVal,
                             AtPmRegister (*Register)(AtPmParam),
                             AtPmRegisterHandler LatchFunc,
                             AtPmRegisterHandler NE_LatchFunc,
                             AtPmRegisterHandler NECP_LatchFunc,
                             AtPmRegisterHandler FECP_LatchFunc)
    {
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamCvL,   cAf6_Ds3_CV_L,   LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamEsL,   cAf6_Ds3_ES_L,   LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamEsaL,  cAf6_Ds3_ESA_L,  LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamEsbL,  cAf6_Ds3_ESB_L,  LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamSesL,  cAf6_Ds3_SES_L,  LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamLossL, cAf6_Ds3_LOSS_L, LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamCvpP,  cAf6_Ds3_CVP_P,  NE_LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamEspP,  cAf6_Ds3_ESP_P,  NE_LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamEsapP, cAf6_Ds3_ESAP_P, NE_LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamEsbpP, cAf6_Ds3_ESBP_P, NE_LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamSespP, cAf6_Ds3_SESP_P, NE_LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamSasP,  cAf6_Ds3_SAS_P,  NE_LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamAissP, cAf6_Ds3_AISS_P, NE_LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamUaspP, cAf6_Ds3_UAS_P,  NE_LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamCvcpP,  cAf6_Ds3_CVCP_P,  NECP_LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamEscpP,  cAf6_Ds3_ESCP_P,  NECP_LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamEsacpP, cAf6_Ds3_ESACP_P, NECP_LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamEsbcpP, cAf6_Ds3_ESBCP_P, NECP_LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamSescpP, cAf6_Ds3_SESCP_P, NECP_LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamUascpP, cAf6_Ds3_UASCP_P, NECP_LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamCvcpPfe,   cAf6_Ds3_CVCP_PFE,  FECP_LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamEscpPfe,   cAf6_Ds3_ESCP_PFE,  FECP_LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamEsacpPfe,  cAf6_Ds3_ESACP_PFE, FECP_LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamEsbcpPfe,  cAf6_Ds3_ESBCP_PFE, FECP_LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamSescpPfe,  cAf6_Ds3_SESCP_PFE, FECP_LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamSascpPfe,  cAf6_Ds3_SASCP_PFE, FECP_LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamUascpPfe,  cAf6_Ds3_UASCP_PFE, FECP_LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamFcP,       cAf6_Ds3_FC_P,      LatchFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamFccpPfe,   cAf6_Ds3_FCCP_PFE,  LatchFunc, Register);

    /* Save valid indication. */
    SaveValid(self, regVal, Register);

    return cAtOk;
    }

/*
 * Latch PM counters from hardware to PM current register.
 *
 * @param self              This engine
 * @param regVal            Buffers read from hardware
 * @param Register          PM register to latch to
 * @param LatchFunc         Latch function for PM parameters
 * @return AT return code
 */
static eAtRet RegistersSave(AtSurEngine self, uint32 *regVal,
                            AtPmRegister (*Register)(AtPmParam),
                            AtPmRegisterHandler LatchFunc)
    {
    AtPmRegisterHandler NE_LatchFunc = LatchFunc;
    AtPmRegisterHandler NECP_LatchFunc = LatchFunc;
    AtPmRegisterHandler FECP_LatchFunc = LatchFunc;

    /* Some projects need to not latch CV/ES/SES/UAS value during the duration of
     * a potential UAS change. It is to avoid decremented PM values seen at the
     * end of potential UAS duration when application keeps polling PM value. */
    if (!ThaModuleHardSurShouldUpdatePmRegisterWhenPotentialUasChange(Module(self)))
        {
        if (IsPotentialUas(self, regVal[cAf6_UAS_PNE_Index]))
            NE_LatchFunc = DonotLatchHwValue;
        if (IsPotentialUas(self, regVal[cAf6_UAS_CPNE_Index]))
            NECP_LatchFunc = DonotLatchHwValue;
        if (IsPotentialUas(self, regVal[cAf6_UAS_PFE_Index]))
            FECP_LatchFunc = DonotLatchHwValue;
        }

    return RegistersLatch(self, regVal, Register, LatchFunc, NE_LatchFunc, NECP_LatchFunc, FECP_LatchFunc);
    }

static eAtRet NearEndParamsAdjust(AtSurEngine self, uint32 *regVal,
                                  AtPmRegister (*Register)(AtPmParam),
                                  AtPmRegisterHandler UasParamFunc,
                                  AtPmRegisterHandler SesParamFunc,
                                  AtPmRegisterHandler OtherParamsFunc)
    {
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamCvpP, cAf6_Ds3_CV_P_Adj, OtherParamsFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamEspP, cAf6_Ds3_ESP_P_Adj, OtherParamsFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamEsapP, cAf6_Ds3_ESAP_P_Adj, OtherParamsFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamEsbpP, cAf6_Ds3_ESBP_P_Adj, OtherParamsFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamSespP, cAf6_Ds3_UAS_NE_Adj, SesParamFunc, Register); /* near-end UASP positive/negative and SESP negative adjustment same register and bit field */
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamSasP, cAf6_Ds3_SAS_P_Adj, OtherParamsFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamAissP, cAf6_Ds3_AISS_P_Adj, OtherParamsFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamUaspP, cAf6_Ds3_UAS_NE_Adj, UasParamFunc, Register);

    return cAtOk;
    }

static eAtRet NearEndCpParamsAdjust(AtSurEngine self, uint32 *regVal,
                                  AtPmRegister (*Register)(AtPmParam),
                                  AtPmRegisterHandler UasParamFunc,
                                  AtPmRegisterHandler SesParamFunc,
                                  AtPmRegisterHandler OtherParamsFunc)
    {
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamCvcpP, cAf6_Ds3_CVCP_P_Adj, OtherParamsFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamEscpP, cAf6_Ds3_ESCP_P_Adj, OtherParamsFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamEsacpP, cAf6_Ds3_ESACP_P_Adj, OtherParamsFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamEsbcpP, cAf6_Ds3_ESBCP_P_Adj, OtherParamsFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamSescpP, cAf6_Ds3_UASCP_P_Adj, SesParamFunc, Register); /* near-end UASCP positive/negative and SESCP negative adjustment same register and bit field */
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamUascpP, cAf6_Ds3_UASCP_P_Adj, UasParamFunc, Register);

    return cAtOk;
    }

static eAtRet FarEndParamsAdjust(AtSurEngine self, uint32 *regVal,
                                 AtPmRegister (*Register)(AtPmParam),
                                 AtPmRegisterHandler UasParamFunc,
                                 AtPmRegisterHandler SesParamFunc,
                                 AtPmRegisterHandler OtherParamsFunc)
    {
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamCvcpPfe, cAf6_Ds3_CVCP_PFE_Adj, OtherParamsFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamEscpPfe, cAf6_Ds3_ESCP_PFE_Adj, OtherParamsFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamEsacpPfe, cAf6_Ds3_ESACP_PFE_Adj, OtherParamsFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamEsbcpPfe, cAf6_Ds3_ESBCP_PFE_Adj, OtherParamsFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamSascpPfe, cAf6_Ds3_SASCP_PFE_Adj, OtherParamsFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamSescpPfe, cAf6_Ds3_UASCP_PFE_Adj, SesParamFunc, Register);
    mRegistersSave(self, cAtSurEnginePdhDe3PmParamUascpPfe, cAf6_Ds3_UASCP_PFE_Adj, UasParamFunc, Register);

    return cAtOk;
    }

static uint32 UasParamStateGet(AtSurEngine self, uint32 prevRegVal, uint32 currRegVal)
    {
    uint32 stkUas = mRegField(currRegVal, cAf6_UAS_Sticky_);
    uint32 curSes = mRegField(currRegVal, cAf6_UAS_Ses_T2_);
    uint32 prevUas = mRegField(prevRegVal, cAf6_UAS_State_);
    uint32 prevSes = mRegField(prevRegVal, cAf6_UAS_Ses_T1_);
    AtUnused(self);

    if (!stkUas && ((curSes + prevSes) == 10) && (prevSes != 0))
        return (prevUas) ? cThaHardSurUasParamExitting : cThaHardSurUasParamEntering;

    return cThaHardSurUasParamUnknown;
    }

/*
 * Latch & adjust PM counters from hardware to PM previous register.
 *
 * @param self              This engine
 * @param regVal            Buffers read from hardware
 * @param Register          PM register to latch to
 * @param LatchFunc         Latch function for PM parameters
 * @return AT return code
 */
static eAtRet RegistersAdjust(AtSurEngine self, uint32 *regVal,
                              AtPmRegister (*Register)(AtPmParam),
                              AtPmRegisterHandler LatchFunc)
    {
    uint32 latchedSesp = mRegField(regVal[cAf6_UAS_PNE_Index], cAf6_UAS_Ses_T1_);
    uint32 latchedSescp = mRegField(regVal[cAf6_UAS_CPNE_Index], cAf6_UAS_Ses_T1_);
    uint32 latchedSescpfe = mRegField(regVal[cAf6_UAS_PFE_Index], cAf6_UAS_Ses_T1_);
    uint32 regCurrent[cNumDdrHoldRegisters];
    eAtRet ret;
    uint32 uasNeState, uascpNeState, uascpFeState;

    RegistersLatch(self, regVal, Register, LatchFunc, LatchFunc, LatchFunc, LatchFunc);
    if (!latchedSesp && !latchedSescpfe && !latchedSescp)
        return cAtOk;

    ret = ThaModuleHardSurDe3DdrCountersR2c(self, cAtPmCurrentPeriodRegister, regCurrent, cAtFalse);
    if (ret != cAtOk)
        return ret;

    uasNeState = UasParamStateGet(self, regVal[cAf6_UAS_PNE_Index], regCurrent[cAf6_UAS_PNE_Index]);
    uascpNeState = UasParamStateGet(self, regVal[cAf6_UAS_CPNE_Index], regCurrent[cAf6_UAS_CPNE_Index]);
    uascpFeState = UasParamStateGet(self, regVal[cAf6_UAS_PFE_Index], regCurrent[cAf6_UAS_PFE_Index]);

    if (uasNeState == cThaHardSurUasParamEntering)
        NearEndParamsAdjust(self, regVal, Register, AddHwValue, SubstractHwValue, SubstractHwValue);
    else if (uasNeState == cThaHardSurUasParamExitting)
        NearEndParamsAdjust(self, regVal, Register, SubstractHwValue, KeepHwValue, AddHwValue);

    if (uascpNeState == cThaHardSurUasParamEntering)
        NearEndCpParamsAdjust(self, regVal, Register, AddHwValue, SubstractHwValue, SubstractHwValue);
    else if (uascpNeState == cThaHardSurUasParamExitting)
        NearEndCpParamsAdjust(self, regVal, Register, SubstractHwValue, KeepHwValue, AddHwValue);

    if (uascpFeState == cThaHardSurUasParamEntering)
        FarEndParamsAdjust(self, regVal, Register, AddHwValue, SubstractHwValue, SubstractHwValue);
    else if (uascpFeState == cThaHardSurUasParamExitting)
        FarEndParamsAdjust(self, regVal, Register, SubstractHwValue, KeepHwValue, AddHwValue);

    return cAtOk;
    }

static eAtRet PeriodExpire(AtSurEngine self, void *data)
    {
    uint32 *regVal = (uint32*)data;

    m_AtSurEngineMethods->PeriodExpire(self, data);

    return RegistersSave(self, regVal, AtPmParamPreviousPeriodRegister, PeriodShift);
    }

static eAtRet AllCountersLatchAndClear(AtSurEngine self, eBool clear)
    {
    uint32 regVal[cNumDdrHoldRegisters];
    eAtRet ret;

    ret = ThaModuleHardSurDe3DdrCountersR2c(self, cAtPmCurrentPeriodRegister, regVal, clear);
    if (ret != cAtOk)
        return ret;

    return RegistersSave(self, regVal, AtPmParamCurrentPeriodRegister, LatchHwValue);
    }

static eAtRet RegistersLatchAndClear(AtSurEngine self, eAtPmRegisterType type, eBool clear)
    {
    uint32 regVal[cNumDdrHoldRegisters];
    eAtRet ret;

    ret = ThaModuleHardSurDe3DdrCountersR2c(self, type, regVal, clear);
    if (ret != cAtOk)
        return ret;

    if (type == cAtPmCurrentPeriodRegister)
        return RegistersSave(self, regVal, AtPmParamCurrentPeriodRegister, LatchHwValue);
    if (type == cAtPmPreviousPeriodRegister)
        return RegistersAdjust(self, regVal, AtPmParamPreviousPeriodRegister, LatchHwValue);
    return cAtErrorModeNotSupport;
    }

static uint32 SavedValid(AtSurEngine self, uint32 paramType, AtPmRegister (*Register)(AtPmParam))
    {
    AtPmParam param = AtSurEnginePmParamGet(self, paramType);
    return AtPmRegisterIsValid(Register(param)) ? 0 : paramType;
    }

static uint32 RegistersInvalidFlagsGet(AtSurEngine self, AtPmRegister (*Register)(AtPmParam))
    {
    uint32 flags = 0;
    flags |= SavedValid(self, cAtSurEnginePdhDe3PmParamCvcpPfe, Register);
    flags |= SavedValid(self, cAtSurEnginePdhDe3PmParamEscpPfe, Register);
    flags |= SavedValid(self, cAtSurEnginePdhDe3PmParamEsacpPfe, Register);
    flags |= SavedValid(self, cAtSurEnginePdhDe3PmParamEsbcpPfe, Register);
    flags |= SavedValid(self, cAtSurEnginePdhDe3PmParamSescpPfe, Register);
    flags |= SavedValid(self, cAtSurEnginePdhDe3PmParamSascpPfe, Register);
    flags |= SavedValid(self, cAtSurEnginePdhDe3PmParamUascpPfe, Register);
    flags |= SavedValid(self, cAtSurEnginePdhDe3PmParamFccpPfe, Register);

    return flags;
    }

static uint32 PmInvalidFlagsGet(AtSurEngine self, eAtPmRegisterType type)
    {
    if (type == cAtPmCurrentPeriodRegister)
        return RegistersInvalidFlagsGet(self, AtPmParamCurrentPeriodRegister);

    if (type == cAtPmPreviousPeriodRegister)
        return RegistersInvalidFlagsGet(self, AtPmParamPreviousPeriodRegister);

    return 0;
    }

static uint32 DefaultOffset(AtSurEngine self)
    {
    uint8 sliceId, hwStsInSlice;

    if (ThaPdhChannelHwIdGet((AtPdhChannel)AtSurEngineChannelGet(self), cAtModuleSur, &sliceId, &hwStsInSlice) != cAtOk)
        return cInvalidUint32;

    return ThaModuleHardSurDe3Offset(Module(self), sliceId, hwStsInSlice);
    }

static eAtRet HwNotificationEnable(AtSurEngine self, uint32 baseAddr, eBool enable)
    {
    ThaModuleHardSur module = Module(self);
    if (module && FailureIsSupported(self))
        {
    uint8 sliceId, hwStsInSlice;
    uint32 bitMask;
    uint32 regVal;
    uint32 regAddr;

    if (ThaPdhChannelHwIdGet((AtPdhChannel)AtSurEngineChannelGet(self), cAtModuleSur, &sliceId, &hwStsInSlice)!= cAtOk)
        return cInvalidUint32;

    regAddr = ThaModuleHardSurBaseAddress(module) + (uint32)(baseAddr + sliceId);
    bitMask = cBit0 << hwStsInSlice;

    regVal = mModuleHwRead(module, regAddr);
    regVal = (enable) ? (regVal | bitMask) : (regVal & ~bitMask);
    mModuleHwWrite(module, regAddr, regVal);
        }
    return cAtOk;
    }

static eBool HwNotificationIsEnabled(AtSurEngine self, uint32 baseAddr)
    {
    ThaModuleHardSur module = Module(self);
    if (module && FailureIsSupported(self))
		{
    uint8 sliceId, hwStsInSlice;
    uint32 bitMask;
    uint32 regVal;
    uint32 regAddr;

    if (ThaPdhChannelHwIdGet((AtPdhChannel)AtSurEngineChannelGet(self), cAtModuleSur, &sliceId, &hwStsInSlice)!= cAtOk)
        return cAtFalse;

    regAddr = ThaModuleHardSurBaseAddress(module) + (uint32)(baseAddr + sliceId);
    bitMask = cBit0 << hwStsInSlice;

    regVal = mModuleHwRead(module, regAddr);
    return (regVal & bitMask) ? cAtTrue : cAtFalse;
    }
    return cAtFalse;		
    }

static eAtRet FailureNotificationEnable(AtSurEngine self, eBool enable)
    {
    return HwNotificationEnable(self, cAf6Reg_FMPM_FM_DS3E3_Framer_Interrupt_MASK_Per_DS3E3_Base, enable);
    }

static eBool FailureNotificationIsEnabled(AtSurEngine self)
    {
    return HwNotificationIsEnabled(self, cAf6Reg_FMPM_FM_DS3E3_Framer_Interrupt_MASK_Per_DS3E3_Base);
    }

static eAtRet TcaNotificationEnable(AtSurEngine self, eBool enable)
    {
    return HwNotificationEnable(self, cAf6Reg_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_DS3E3_Base, enable);
    }

static eBool TcaNotificationIsEnabled(AtSurEngine self)
    {
    return HwNotificationIsEnabled(self, cAf6Reg_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_DS3E3_Base);
    }

static void FailureNotificationProcess(AtSurEngine self, uint32 offset)
    {
    ThaModuleHardSur module = Module(self);
    if (module && FailureIsSupported(self))
        {
    uint32 regVal = mModuleHwRead(module, ThaModuleHardSurDe3CurrentFailuresAddr(module) + offset);
    uint32 events = 0;
    uint32 failures = 0;

    mGetFmIntrStatus(cAtPdhDe3AlarmAis,     AIS,    failures, events);
    mGetFmIntrStatus(cAtPdhDe3AlarmLos,     LOS,    failures, events);
    mGetFmIntrStatus(cAtPdhDe3AlarmLof,     LOF,    failures, events);
    mGetFmIntrStatus(cAtPdhDe3AlarmRai,     RAI,    failures, events);

    /* Only clear masked interrupt. */
    regVal = (regVal >> 16) & cBit15_0;

    /* Clear interrupt */
    mModuleHwWrite(module, ThaModuleHardSurDe3FailuresInterruptAddr(module) + offset, regVal);

    AtSurEngineFailureNotify(self, events, failures);

    AtPdhChannelFailureForwardingClearanceNotifyWithSubChannelOnly((AtPdhChannel)AtSurEngineChannelGet(self), cAtTrue);
    }
    }

static void TcaNotificationProcess(AtSurEngine self, uint32 offset)
    {
    ThaModuleHardSur module = Module(self);
    uint32 intrAddr = ThaModuleHardSurDe3TcaInterruptAddr(module) + offset;
    uint32 maskAddr = ThaModuleHardSurDe3TcaInterruptMaskAddr(module) + offset;
    uint32 intrVal = mModuleHwRead(module, intrAddr);
    uint32 maskVal = mModuleHwRead(module, maskAddr);
    uint32 params = 0;

    /* Filter unmasked stickies. */
    intrVal &= maskVal;
    params = TcaMaskGet(intrVal);

    /* Clear interrupt */
    mModuleHwWrite(module, intrAddr, intrVal);

    AtSurEngineAllParamsPeriodTcaNotify(self, params);
    }

static void OverrideAtSurEngine(AtSurEngine self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSurEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSurEngineOverride, m_AtSurEngineMethods, sizeof(m_AtSurEngineOverride));

        mMethodOverride(m_AtSurEngineOverride, PmEnable);
        mMethodOverride(m_AtSurEngineOverride, PmIsEnabled);
        mMethodOverride(m_AtSurEngineOverride, AutonomousFailuresGet);
        mMethodOverride(m_AtSurEngineOverride, NonAutonomousFailuresGet);
        mMethodOverride(m_AtSurEngineOverride, PmCanEnable);
        mMethodOverride(m_AtSurEngineOverride, FailureChangedHistoryGet);
        mMethodOverride(m_AtSurEngineOverride, FailureChangedHistoryClear);
        mMethodOverride(m_AtSurEngineOverride, FailureNotificationMaskSet);
        mMethodOverride(m_AtSurEngineOverride, FailureNotificationMaskGet);
        mMethodOverride(m_AtSurEngineOverride, TcaChangedHistoryGet);
        mMethodOverride(m_AtSurEngineOverride, TcaChangedHistoryClear);
        mMethodOverride(m_AtSurEngineOverride, TcaNotificationMaskSet);
        mMethodOverride(m_AtSurEngineOverride, TcaNotificationMaskGet);
        mMethodOverride(m_AtSurEngineOverride, SesThresholdPoolEntrySet);
        mMethodOverride(m_AtSurEngineOverride, SesThresholdPoolEntryGet);
        mMethodOverride(m_AtSurEngineOverride, TcaThresholdPoolEntrySet);
        mMethodOverride(m_AtSurEngineOverride, TcaThresholdPoolEntryGet);
        mMethodOverride(m_AtSurEngineOverride, TcaPeriodThresholdValueGet);
        mMethodOverride(m_AtSurEngineOverride, AllCountersLatchAndClear);
        mMethodOverride(m_AtSurEngineOverride, RegistersLatchAndClear);
        mMethodOverride(m_AtSurEngineOverride, PeriodExpire);
        mMethodOverride(m_AtSurEngineOverride, DefaultOffset);
        mMethodOverride(m_AtSurEngineOverride, FailureNotificationEnable);
        mMethodOverride(m_AtSurEngineOverride, FailureNotificationIsEnabled);
        mMethodOverride(m_AtSurEngineOverride, FailureNotificationProcess);
        mMethodOverride(m_AtSurEngineOverride, TcaNotificationEnable);
        mMethodOverride(m_AtSurEngineOverride, TcaNotificationIsEnabled);
        mMethodOverride(m_AtSurEngineOverride, TcaNotificationProcess);
        mMethodOverride(m_AtSurEngineOverride, PmInvalidFlagsGet);
        mMethodOverride(m_AtSurEngineOverride, SesThresholdProfileGet);
        }

    mMethodsSet(self, &m_AtSurEngineOverride);
    }

static void Override(AtSurEngine self)
    {
    OverrideAtSurEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaHardSurEnginePdhDe3);
    }

AtSurEngine ThaHardSurEnginePdhDe3ObjectInit(AtSurEngine self, AtModuleSur module, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (AtSurEnginePdhDe3ObjectInit(self, module, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSurEngine ThaHardSurEnginePdhDe3New(AtModuleSur module, AtChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSurEngine newSurEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newSurEngine == NULL)
        return NULL;

    /* Construct it */
    return ThaHardSurEnginePdhDe3ObjectInit(newSurEngine, module, channel);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Surveillance
 * 
 * File        : ThaHardSurEngineInternal.h
 * 
 * Created Date: Mar 19, 2015
 *
 * Description : Hardware Surveillance Engine internal header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAHARDSURENGINEINTERNAL_H_
#define _THAHARDSURENGINEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../../generic/sur/AtSurEngineInternal.h"
#include "../../../../../generic/sur/AtPmRegisterInternal.h"
#include "../../../../../generic/sur/AtPmParamInternal.h"
#include "../ThaModuleHardSurInternal.h"
#include "ThaHardSurEngine.h"

/*--------------------------- Define -----------------------------------------*/

#define cAf6_UAS_State_Mask     cBit31
#define cAf6_UAS_State_Shift        31
#define cAf6_UAS_Sticky_Mask    cBit30
#define cAf6_UAS_Sticky_Shift       30
#define cAf6_UAS_Ses_T1_Mask cBit19_16
#define cAf6_UAS_Ses_T1_Shift       16
#define cAf6_UAS_Ses_T2_Mask  cBit11_8
#define cAf6_UAS_Ses_T2_Shift        8

/*--------------------------- Macros -----------------------------------------*/
#define mRegistersSave(self, type, fieldName, functionHandler, registerHandler) functionHandler(self, type, regVal[fieldName##_Index], fieldName##_Mask, fieldName##_Shift, registerHandler);

#define mParamType(field, period, day)                                     \
    if (engine->field == NULL)                                             \
        engine->field = ThaHardPmParamNew(self, #field, paramType);        \
    return engine->field;

#define mThresholdUpdate(field)                                                \
    do                                                                         \
        {                                                                      \
        AtPmParamPeriodThresholdSet(engine->field, period);                    \
        AtPmParamDayThresholdSet(engine->field, period * 24* 4);               \
        }while(0)

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaHardSurEngineSdhLine* ThaHardSurEngineSdhLine;
typedef struct tThaHardSurEngineSdhPath* ThaHardSurEngineSdhPath;
typedef struct tThaHardSurEnginePdhDe3* ThaHardSurEnginePdhDe3;
typedef struct tThaHardSurEnginePdhDe1* ThaHardSurEnginePdhDe1;
typedef struct tThaHardSurEnginePw* ThaHardSurEnginePw;

typedef struct tThaHardSurEngineSdhLine
    {
    tAtSurEngineSdhLine super;
    }tThaHardSurEngineSdhLine;

typedef struct tThaHardSurEngineSdhPath
    {
    tAtSurEngineSdhPath super;
    }tThaHardSurEngineSdhPath;

typedef struct tThaHardSurEngineSdhLoPath
    {
    tThaHardSurEngineSdhPath super;
    }tThaHardSurEngineSdhLoPath;

typedef struct tThaHardSurEnginePdhDe1
    {
    tAtSurEnginePdhDe1 super;
    }tThaHardSurEnginePdhDe1;

typedef struct tThaHardSurEnginePdhDe3
    {
    tAtSurEnginePdhDe3 super;
    }tThaHardSurEnginePdhDe3;

typedef struct tThaHardSurEnginePw
    {
    tAtSurEnginePw super;
    }tThaHardSurEnginePw;

typedef enum eThaHardSurUasParamState
    {
    cThaHardSurUasParamUnknown,
    cThaHardSurUasParamEntering,
    cThaHardSurUasParamExitting
    }eThaHardSurUasParamState;

typedef void (*AtPmRegisterHandler)(AtSurEngine self, uint32 paramType, uint32 regval, uint32 mask, uint32 shift, AtPmRegister (*Register)(AtPmParam));

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Constructor */
AtSurEngine ThaHardSurEngineSdhLineObjectInit(AtSurEngine self, AtModuleSur module, AtChannel channel);
AtSurEngine ThaHardSurEngineSdhPathObjectInit(AtSurEngine self, AtModuleSur module, AtChannel channel);
AtSurEngine ThaHardSurEngineSdhLoPathObjectInit(AtSurEngine self, AtModuleSur module, AtChannel channel);
AtSurEngine ThaHardSurEnginePdhDe3ObjectInit(AtSurEngine self, AtModuleSur module, AtChannel channel);
AtSurEngine ThaHardSurEnginePdhDe1ObjectInit(AtSurEngine self, AtModuleSur module, AtChannel channel);
AtSurEngine ThaHardSurEnginePwObjectInit(AtSurEngine self, AtModuleSur module, AtChannel channel);

#endif /* _THAHARDSURENGINEINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SUR
 * 
 * File        : ThaHardSurEngineCommon.h
 * 
 * Created Date: Jan 31, 2018
 *
 * Description : Default common Surveillance implementation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAHARDSURENGINECOMMON_H_
#define _THAHARDSURENGINECOMMON_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
static void PeriodShift(AtSurEngine self, uint32 paramType, uint32 regval, uint32 mask, uint32 shift, AtPmRegister (*Register)(AtPmParam))
    {
    int32 value = 0;
    AtPmParam param = AtSurEnginePmParamGet(self, paramType);
    AtPmRegister reg = Register(param);
    AtPmParamRecentPeriodShiftLeft(param);
    if (AtSurEnginePmParamIsDefined(self, paramType))
        mFieldGet(regval, mask, shift, int32, &value);
    AtPmRegisterValueSet(reg, value);
    }

static void LatchHwValue(AtSurEngine self, uint32 paramType, uint32 regval, uint32 mask, uint32 shift, AtPmRegister (*Register)(AtPmParam))
    {
    int32 value = 0;
    AtPmParam param = AtSurEnginePmParamGet(self, paramType);
    AtPmRegister reg = Register(param);
    if (AtSurEnginePmParamIsDefined(self, paramType))
        mFieldGet(regval, mask, shift, int32, &value);
    AtPmRegisterValueSet(reg, value);
    }

static void KeepHwValue(AtSurEngine self, uint32 paramType, uint32 regval, uint32 mask, uint32 shift, AtPmRegister (*Register)(AtPmParam))
    {
    AtUnused(self);
    AtUnused(paramType);
    AtUnused(regval);
    AtUnused(mask);
    AtUnused(shift);
    AtUnused(Register);
    }

static void AddHwValue(AtSurEngine self, uint32 paramType, uint32 regval, uint32 mask, uint32 shift, AtPmRegister (*Register)(AtPmParam))
    {
    int32 value = 0;
    AtPmParam param = AtSurEnginePmParamGet(self, paramType);
    AtPmRegister reg = Register(param);
    if (AtSurEnginePmParamIsDefined(self, paramType))
        mFieldGet(regval, mask, shift, int32, &value);
    AtPmRegisterValueSet(reg, AtPmRegisterValue(reg) + value);
    }

static void SubstractHwValue(AtSurEngine self, uint32 paramType, uint32 regval, uint32 mask, uint32 shift, AtPmRegister (*Register)(AtPmParam))
    {
    int32 value = 0;
    AtPmParam param = AtSurEnginePmParamGet(self, paramType);
    AtPmRegister reg = Register(param);
    if (AtSurEnginePmParamIsDefined(self, paramType))
        mFieldGet(regval, mask, shift, int32, &value);
    AtPmRegisterValueSet(reg, AtPmRegisterValue(reg) - value);
    }

static void DonotLatchHwValue(AtSurEngine self, uint32 paramType, uint32 regval, uint32 mask, uint32 shift, AtPmRegister (*Register)(AtPmParam))
    {
    AtUnused(self);
    AtUnused(paramType);
    AtUnused(regval);
    AtUnused(mask);
    AtUnused(shift);
    AtUnused(Register);
    }

static void LatchHwValid(AtSurEngine self, uint32 paramType, uint32 regval, uint32 mask, uint32 shift, AtPmRegister (*Register)(AtPmParam))
    {
    AtPmParam param = AtSurEnginePmParamGet(self, paramType);
    AtPmRegister reg = Register(param);
    AtUnused(shift);
    ThaHardPmRegisterValidSet((ThaHardPmRegister)reg, (regval & mask) ? cAtFalse : cAtTrue);
    }

static eBool IsPotentialUas(AtSurEngine self, uint32 regVal)
    {
    AtUnused(self);
    return (mRegField(regVal, cAf6_UAS_Ses_T1_) > 0) ? cAtTrue : cAtFalse;
    }

static AtPmSesThresholdProfile SesThresholdProfileGet(AtSurEngine self)
    {
    return AtModuleSurSesThresholdProfileGet(AtSurEngineModuleGet(self), AtSurEngineSesThresholdPoolEntryGet(self));
    }

#ifdef __cplusplus
}
#endif
#endif /* _THAHARDSURENGINECOMMON_H_ */


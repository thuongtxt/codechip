/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Surveillance
 *
 * File        : ThaModuleHardSur.c
 *
 * Created Date: Mar 19, 2015
 *
 * Description : Hardware Surveillance Module implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../ocn/ThaModuleOcn.h"
#include "../../man/ThaDevice.h"
#include "../../man/versionreader/ThaVersionReader.h"
#include "../../encap/ThaHdlcChannel.h"
#include "engine/ThaHardSurEngineInternal.h"
#include "param/ThaHardPmRegisterInternal.h"
#include "param/ThaHardPmParam.h"
#include "threshold/ThaHardPmSesThresholdProfile.h"
#include "threshold/ThaHardPmThresholdProfile.h"
#include "ThaModuleHardSurInternal.h"
#include "ThaModuleHardSurReg.h"
#include "ThaSurInterruptManager.h"

/*--------------------------- Define -----------------------------------------*/
#define cInvalidCounterValue 0xCAFECAFE

#define cThaLineTcaTypeMask   cAf6_FMPM_Threshold_Type_of_Section_TCA_TYPE_Mask
#define cThaLineTcaTypeShift  cAf6_FMPM_Threshold_Type_of_Section_TCA_TYPE_Shift

#define cThaStsTcaTypeMask    cAf6_FMPM_Threshold_Type_of_STS_TCA_TYPE_Mask
#define cThaStsTcaTypeShift   cAf6_FMPM_Threshold_Type_of_STS_TCA_TYPE_Shift

#define cThaDe3TcaTypeMask    cAf6_FMPM_Threshold_Type_of_DS3_TCA_TYPE_Mask
#define cThaDe3TcaTypeShift   cAf6_FMPM_Threshold_Type_of_DS3_TCA_TYPE_Shift

#define cThaVtTcaTypeMask     cAf6_FMPM_Threshold_Type_of_VT_TCA_TYPE_Mask
#define cThaVtTcaTypeShift    cAf6_FMPM_Threshold_Type_of_VT_TCA_TYPE_Shift

#define cThaDe1TcaTypeMask    cAf6_FMPM_Threshold_Type_of_DS1_TCA_TYPE_Mask
#define cThaDe1TcaTypeShift   cAf6_FMPM_Threshold_Type_of_DS1_TCA_TYPE_Shift

#define cThaPwTcaTypeMask     cAf6_FMPM_Threshold_Type_of_PW_TCA_TYPE_Mask
#define cThaPwTcaTypeShift    cAf6_FMPM_Threshold_Type_of_PW_TCA_TYPE_Shift

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaModuleHardSur)self)
#define mValidPage(page) ((page == 0) || (page == 1))
#define mInvalidateRequestPage(self) mThis(self)->currentRequestedPage = cInvalidUint32

#define mSuccessAssert(calling)                                                \
    do                                                                         \
        {                                                                      \
        eAtRet ret_ = calling;                                                 \
        if (ret_ != cAtOk) return ret_;                                        \
        }while(0)

#define mModuleSur(self) ((ThaModuleHardSur)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cAtModuleSur))
#define mIsNullPointer(self) ((self) == NULL)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tThaModuleHardSurMethods m_methods;

/* Override */
static tAtObjectMethods    m_AtObjectOverride;
static tAtModuleMethods    m_AtModuleOverride;
static tAtModuleSurMethods m_AtModuleSurOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
static const tAtModuleMethods  *m_AtModuleMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
mDefineMaskShift(ThaModuleHardSur, LineTcaType)
mDefineMaskShift(ThaModuleHardSur, StsTcaType)
mDefineMaskShift(ThaModuleHardSur, De3TcaType)
mDefineMaskShift(ThaModuleHardSur, VtTcaType)
mDefineMaskShift(ThaModuleHardSur, De1TcaType)
mDefineMaskShift(ThaModuleHardSur, PwTcaType)

static eBool  FailureIsSupported(AtModuleSur self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool HwIsReady(AtModule self)
    {
    return ThaDeviceSurveillanceSupported((ThaDevice)AtModuleDeviceGet(self));
    }

static const char *CapacityDescription(AtModule self)
    {
    AtUnused(self);
    return "sdh_line, sdh_path, pdh_de1, pdh_de3, pw";
    }

static AtInterruptManager InterruptManagerCreate(AtModule self, uint32 managerIndex)
    {
    if (managerIndex == 0)
    return ThaSurInterruptManagerNew(self);

    if (managerIndex == 1)
        return ThaSurTcaInterruptManagerNew(self);

    return NULL;
    }

static uint32 NumInterruptManagers(AtModule self)
    {
    AtUnused(self);
    return 1;
    }

static AtSurEngine SdhLineSurEngineObjectCreate(ThaModuleHardSur self, AtChannel line)
    {
    return ThaHardSurEngineSdhLineNew((AtModuleSur)self, line);
    }

static AtSurEngine SdhLineEngineCreate(AtModuleSur self, AtSdhLine line)
    {
    if (!HwIsReady((AtModule)self))
        return NULL;

    return mMethodsGet(mThis(self))->SdhLineSurEngineObjectCreate(mThis(self), (AtChannel)line);
    }

static AtSurEngine SdhHoPathEngineObjectCreate(ThaModuleHardSur self, AtChannel path)
    {
    return ThaHardSurEngineSdhPathNew((AtModuleSur)self, path);
    }

static AtSurEngine SdhHoPathEngineCreate(AtModuleSur self, AtSdhPath path)
    {
    if (!HwIsReady((AtModule)self))
        return NULL;

    return mMethodsGet(mThis(self))->SdhHoPathEngineObjectCreate(mThis(self), (AtChannel)path);
    }

static AtSurEngine SdhLoPathEngineObjectCreate(ThaModuleHardSur self, AtChannel path)
    {
    return ThaHardSurEngineSdhLoPathNew((AtModuleSur)self, path);
    }

static AtSurEngine SdhLoPathEngineCreate(AtModuleSur self, AtSdhPath path)
    {
    if (!HwIsReady((AtModule)self))
        return NULL;

    return mMethodsGet(mThis(self))->SdhLoPathEngineObjectCreate(mThis(self), (AtChannel)path);
    }

static AtSurEngine PdhDe1EngineObjectCreate(ThaModuleHardSur self, AtChannel de1)
    {
    return ThaHardSurEnginePdhDe1New((AtModuleSur)self, de1);
    }

static AtSurEngine PdhDe1EngineCreate(AtModuleSur self, AtPdhDe1 de1)
    {
    if (!HwIsReady((AtModule)self))
        return NULL;

    return mMethodsGet(mThis(self))->PdhDe1EngineObjectCreate(mThis(self), (AtChannel)de1);
    }

static AtSurEngine PdhDe3EngineObjectCreate(ThaModuleHardSur self, AtChannel de3)
    {
    return ThaHardSurEnginePdhDe3New((AtModuleSur)self, de3);
    }

static AtSurEngine PdhDe3EngineCreate(AtModuleSur self, AtPdhDe3 de3)
    {
    if (!HwIsReady((AtModule)self))
        return NULL;

    return  mMethodsGet(mThis(self))->PdhDe3EngineObjectCreate(mThis(self), (AtChannel)de3);
    }

static AtSurEngine PwEngineObjectCreate(ThaModuleHardSur self, AtChannel pw)
    {
    return ThaHardSurEnginePwNew((AtModuleSur)self, pw);
    }

static AtSurEngine PwEngineCreate(AtModuleSur self, AtPw pw)
    {
    if (!HwIsReady((AtModule)self))
        return NULL;

    return mMethodsGet(mThis(self))->PwEngineObjectCreate(mThis(self), (AtChannel)pw);
    }

static eAtRet SdhLineEngineDelete(AtModuleSur self, AtSurEngine line)
    {
    AtUnused(self);
    AtObjectDelete((AtObject)line);
    return cAtOk;
    }

static eAtRet SdhHoPathEngineDelete(AtModuleSur self, AtSurEngine path)
    {
    AtUnused(self);
    AtObjectDelete((AtObject)path);
    return cAtOk;
    }

static eAtRet SdhLoPathEngineDelete(AtModuleSur self, AtSurEngine path)
    {
    AtUnused(self);
    AtObjectDelete((AtObject)path);
    return cAtOk;
    }

static eAtRet PdhDe1EngineDelete(AtModuleSur self, AtSurEngine de1)
    {
    AtUnused(self);
    AtObjectDelete((AtObject)de1);
    return cAtOk;
    }

static eAtRet PdhDe3EngineDelete(AtModuleSur self, AtSurEngine de3)
    {
    AtUnused(self);
    AtObjectDelete((AtObject)de3);
    return cAtOk;
    }

static eAtRet PwEngineDelete(AtModuleSur self, AtSurEngine pw)
    {
    AtUnused(self);
    AtObjectDelete((AtObject)pw);
    return cAtOk;
    }

static uint32 ResetRegister(ThaModuleHardSur self)
    {
    return cAf6Reg_FMPM_Force_Reset_Core + ThaModuleHardSurBaseAddress(self);
    }

static uint32 NumSesThresholdProfiles(AtModuleSur self)
    {
    AtUnused(self);
    return 8;
    }

static AtPmSesThresholdProfile SesThresholdProfileCreate(AtModuleSur self, uint32 profileId)
    {
    return ThaHardPmSesThresholdProfileNew(self, profileId);
    }

static eAtRet Setup(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Setup(self);
    mSuccessAssert(ret);

    if (mThis(self)->ddrMutex == NULL)
        mThis(self)->ddrMutex = AtOsalMutexCreate();

    return cAtOk;
    }

static eBool IsSimulated(AtModule self)
    {
    AtDevice device = AtModuleDeviceGet(self);
    return AtDeviceIsSimulated(device);
    }

static eAtRet ResetForDdrOffLoad(ThaModuleHardSur self)
    {
    uint32 regAddr = ResetRegister(mThis(self));
    uint32 regVal = mModuleHwRead(self, regAddr);

    /* Trigger reset */
    mModuleHwWrite(self, regAddr, (regVal & ~cAf6_FMPM_Force_Reset_Core_frst_Mask));
    mModuleHwWrite(self, regAddr, (regVal | cAf6_FMPM_Force_Reset_Core_frst_Mask));
    mModuleHwWrite(self, regAddr, (regVal & ~cAf6_FMPM_Force_Reset_Core_frst_Mask));

    return cAtOk;
    }

static eAtRet Reset(ThaModuleHardSur self)
    {
    tAtOsalCurTime curTime, startTime;
    uint32 elapse = 0;
    static const uint32 cTimeoutMs = 1000;
    uint32 regAddr = ResetRegister(mThis(self));
    uint32 regVal = mModuleHwRead(self, regAddr);

    /* Trigger reset */
    mModuleHwWrite(self, regAddr, (regVal & ~cAf6_FMPM_Force_Reset_Core_frst_Mask));
    mModuleHwWrite(self, regAddr, (regVal | cAf6_FMPM_Force_Reset_Core_frst_Mask));

    /* Wait for hardware */
    AtOsalCurTimeGet(&startTime);
    while (elapse < cTimeoutMs)
        {
        regVal = mModuleHwRead(self, regAddr);
        if (regVal & cAf6_FMPM_Force_Reset_Core_drst_Mask)
            return cAtOk;

        if (IsSimulated((AtModule)self) || (AtDeviceWarmRestoreIsStarted(AtModuleDeviceGet((AtModule)self))))
            return cAtOk;

        /* Retry */
        AtOsalCurTimeGet(&curTime);
        elapse = mTimeIntervalInMsGet(startTime, curTime);
        }

    AtModuleLog((AtModule)self, cAtLogLevelCritical, AtSourceLocation, "Surveillance reset fail\r\n");
    return cAtErrorRamAccessTimeout;
    }

static eBool ShouldReset(AtModule self)
    {
    if (AtModuleInAccessible(self))
        return cAtFalse;

    return HwIsReady(self);
    }

static uint32 Clock125usRegister(AtModule self)
    {
    return cAf6Reg_FMPM_Config_Number_Clock_Of_125us_Base + ThaModuleHardSurBaseAddress(mThis(self));
    }

static ThaVersionReader VersionReader(AtModule self)
    {
    return ThaDeviceVersionReader(AtModuleDeviceGet(self));
    }

static uint32 StartVersionSupportReset(AtModule self)
    {
    return ThaVersionReaderPwVersionBuild(VersionReader(self), 0x15, 0x10, 0x13, 0x0);
    }

static eBool ResetIsSupported(AtModule self)
    {
    AtDevice device = AtModuleDeviceGet(self);
    if (AtDeviceVersionNumber(device) >= StartVersionSupportReset(self))
        return cAtTrue;
    return cAtFalse;
    }

static void Clock125usSet(ThaModuleHardSur self, uint32 value)
    {
    uint32 regAddr = Clock125usRegister((AtModule)self);
    uint32 regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_FMPM_Config_Number_Clock_Of_125us_config125us_, value);
    mModuleHwWrite(self, regAddr, regVal);
    }

static uint32 Clock125usGet(ThaModuleHardSur self)
    {
    uint32 regAddr = Clock125usRegister((AtModule)self);
    uint32 regVal = mModuleHwRead(self, regAddr);
    return mRegField(regVal, cAf6_FMPM_Config_Number_Clock_Of_125us_config125us_);
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = cAtOk;
    ThaModuleHardSur surModule = mThis(self);

    mSuccessAssert(m_AtModuleMethods->Init(self));

    if (!ResetIsSupported(self))
        return cAtOk;

    /* Default setting */
    Clock125usSet(mThis(self), mMethodsGet(mThis(self))->Clock125usDefaultValue(mThis(self)));

    /* Activate module. */
    ret |= AtModuleActivate(self);

    /* Do not move below before activate the module, doing so will not have any
     * affect */
    ret |= AtModuleSurExpireMethodSet((AtModuleSur)self, mMethodsGet(surModule)->DefaultExpireMethod(surModule));
    ret |= AtModuleSurTickSourceSet((AtModuleSur)self, mMethodsGet(surModule)->DefaultTickSourceGet(surModule));

    return ret;
    }

static void DebugClear(AtModule self)
    {
    mThis(self)->maxChangePageInMs = 0;
    mThis(self)->maxReadingInUs = 0;
    mThis(self)->minReadingInUs = 0;
    mThis(self)->numLongReadings = 0;
    mThis(self)->maxWaitingDdrInUs = 0;
    mThis(self)->maxHoldRegReadingInUs = 0;
    mThis(self)->maxHoldRegAddress = 0;
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static const char* CurrentTimeInUsGet(ThaModuleHardSur self)
    {
    uint32 regAddr = ThaModuleHardSurBaseAddress(self) + cAf6Reg_FMPM_Monitor_Timer;
    uint32 currentTimeIn125usUnit = mModuleHwRead(self, regAddr);
    static char timeInString[32];

    AtSprintf(timeInString, "%010d.%06d", currentTimeIn125usUnit / 8000, (currentTimeIn125usUnit % 8000) * 125);
    return timeInString;
    }

static void MonitorTimerShow(AtModule self)
    {
    ThaModuleHardSur surModule = (ThaModuleHardSur)self;
    tAtOsalCurTime currentTime;

    AtOsalCurTimeGet(&currentTime);
    AtPrintc(cSevNormal, "* Timer: \r\n");
    AtPrintc(cSevNormal, "  * System OS time: %d.%06d\r\n", currentTime.sec, currentTime.usec);
    AtPrintc(cSevNormal, "  * Hardware time : %s\r\n", CurrentTimeInUsGet(surModule));
    }

static eAtRet Debug(AtModule self)
    {
    ThaModuleHardSur module = mThis(self);
    uint32 version = ThaModuleHardSurVersionNumber(module);

    AtPrintc(cSevNormal, "Surveillance debug information:\r\n");
    AtPrintc(cSevNormal, "* **********************************\r\n");
    AtPrintc(cSevNormal, "* Product          : %d\r\n", mRegField(version, cAf6_FMPM_Block_Version_project_));
    AtPrintc(cSevNormal, "* Date version     : %x/%x/%x\r\n", mRegField(version, cAf6_FMPM_Block_Version_day_),
                                                          mRegField(version, cAf6_FMPM_Block_Version_month_),
                                                          mRegField(version, cAf6_FMPM_Block_Version_year_));
    AtPrintc(cSevNormal, "* Built version    : %d\r\n", mRegField(version, cAf6_FMPM_Block_Version_number_));
    AtPrintc(cSevNormal, "* Active page      : %d\r\n", ThaModuleHardSurCurrentPage(module));
    AtPrintc(cSevNormal, "* PActive          : %s\r\n", AtModuleIsActive(self) ? "Enabled" : "Disabled");
    AtPrintc(cSevNormal, "* OperationState   : %s\r\n", !AtModuleIsActive(self) ? "N/A" : ThaModuleHardSurIsOperating(mThis(self)) ? "Operational" : "Reset");
    AtPrintc(cSevNormal, "* Clock 125us      : 0x%x\r\n", Clock125usGet(mThis(self)));
    AtPrintc(cSevNormal, "\r\n* **********************************\r\n");
    AtPrintc(cSevNormal, "* Max switching    : %d (ms)\r\n", module->maxChangePageInMs);
    AtPrintc(cSevNormal, "* Max hold reading : %d (us) (whole 25 hold registers)\r\n", module->maxReadingInUs);
    AtPrintc(cSevNormal, "* Min hold reading : %d (us) (whole 25 hold registers)\r\n", module->minReadingInUs);
    AtPrintc(cSevNormal, "* Max REG reading  : %d (us) at address 0x%x\r\n", module->maxHoldRegReadingInUs, module->maxHoldRegAddress);
    AtPrintc(cSevNormal, "* Long readings    : %d (i.e. number of readings that takes time > 400us)\r\n", module->numLongReadings);
    AtPrintc(cSevNormal, "* Max DDR waiting  : %d (us)\r\n", module->maxWaitingDdrInUs);

    /* Asynchronous switching page */
    AtPrintc(cSevNormal, "* Requesting switching page: ");
    if (mValidPage(module->currentRequestedPage))
        AtPrintc(cSevWarning, "%d\r\n", module->currentRequestedPage);
    else
        AtPrintc(cSevInfo, "None\r\n");

    AtPrintc(cSevNormal, "* Timeout settings: switch page (%d ms), DDR (%d us)\r\n",
                         mMethodsGet(mThis(self))->ChangePageTimeoutInMs(mThis(self)),
                         mMethodsGet(mThis(self))->DdrAccessTimeoutInUs(mThis(self)));

    if (mMethodsGet(mThis(self))->HasMonitorTimer(mThis(self)))
        MonitorTimerShow(self);

    AtPrintc(cSevNormal, "* **********************************\r\n");

    DebugClear(self);
    return cAtOk;
    }

static void Delete(AtObject self)
    {
    AtOsalMutexDestroy(mThis(self)->ddrMutex);
    mThis(self)->ddrMutex = NULL;

    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static uint32 ChangePageRegister(ThaModuleHardSur self)
    {
    return cAf6Reg_FMPM_Change_Page_DDR + ThaModuleHardSurBaseAddress(self);
    }

static eBool PageChanged(ThaModuleHardSur self)
    {
    uint32 regAddr = ChangePageRegister(mThis(self));
    uint32 regVal = mModuleHwRead(self, regAddr);
    return (regVal & cAf6_FMPM_Change_Page_DDR_CHG_DONE_Mask) ? cAtTrue : cAtFalse;
    }

static eAtRet ChangePageRequest(ThaModuleHardSur self, uint32 newPage)
    {
    uint32 regVal, regAddr = ChangePageRegister(mThis(self));

    /* Specify new page */
    regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_FMPM_Change_Page_DDR_HW_Page_, newPage);
    mRegFieldSet(regVal, cAf6_FMPM_Change_Page_DDR_CHG_DONE_, 1); /* Clear sticky */
    mModuleHwWrite(self, regAddr, regVal);

    /* Trigger page change. */
    mModuleHwWrite(self, regAddr, (regVal & ~cAf6_FMPM_Change_Page_DDR_CHG_START_Mask));
    mModuleHwWrite(self, regAddr, (regVal | cAf6_FMPM_Change_Page_DDR_CHG_START_Mask));

    return cAtOk;
    }

static uint32 ChangePageTimeoutInMs(ThaModuleHardSur self)
    {
    AtUnused(self);
    return 3000; /* All of implementations so far */
    }

static eAtRet ChangePage(ThaModuleHardSur self, uint32 newPage)
    {
    uint32 elapse = 0;
    tAtOsalCurTime curTime, startTime;
    uint32 timeout = mMethodsGet(self)->ChangePageTimeoutInMs(self);
    static const uint32 cSleepStep = 100000; /* 100ms. */

    /* Make request and wait till hardware finishes its job */
    ChangePageRequest(self, newPage);
    AtOsalCurTimeGet(&startTime);
    while (elapse < timeout)
        {
        /* This action can take few hundred milliseconds. */
        AtOsalUSleep(cSleepStep);

        /* Done */
        if (PageChanged(self))
            {
            if ((elapse += 100) > mThis(self)->maxChangePageInMs)
                mThis(self)->maxChangePageInMs = elapse;
            return cAtOk;
            }

        if (IsSimulated((AtModule)self))
            return cAtOk;

        /* Retry */
        AtOsalCurTimeGet(&curTime);
        elapse = mTimeIntervalInMsGet(startTime, curTime);
        }

    return cAtErrorRamAccessTimeout;
    }

static uint32 CurrentPage(ThaModuleHardSur self)
    {
    uint32 regVal = mModuleHwRead(self, ChangePageRegister(mThis(self)));
    return mRegField(regVal, cAf6_FMPM_Change_Page_DDR_HW_Page_);
    }

static eBool ChangePageRequestMade(ThaModuleHardSur self, uint32 currentPage)
    {
    if (!mValidPage(self->currentRequestedPage))
        return cAtFalse;

    if (self->currentRequestedPage != currentPage)
        return cAtFalse;

    return cAtTrue;
    }

static eAtRet PageSwitchBlocking(ThaModuleHardSur self)
    {
    uint32 currentPage = CurrentPage(self);
    uint32 newPage = (currentPage == 0) ? 1 : 0;
    eAtRet ret;

    /* Just for sure, if none-blocking call is being served, do not allow this */
    if (ChangePageRequestMade(self, currentPage))
        return cAtErrorDevBusy;

    ret = ChangePage(self, newPage);
    mSuccessAssert(ret);

    if (ThaModuleHardSurCurrentPage(self) != newPage)
        return cAtErrorDevFail;

    return ret;
    }

static eAtRet PageSwitchNoneBlocking(ThaModuleHardSur self)
    {
    uint32 currentHwPage = CurrentPage(self);
    uint32 newPage = (currentHwPage == 0) ? 1 : 0;

    /* If request has been made, check if hardware finish its job */
    if (ChangePageRequestMade(self, currentHwPage))
        {
        if (PageChanged(self))
            {
            mInvalidateRequestPage(self);
            return cAtOk;
            }

        return cAtErrorAgain;
        }

    /* This is actually new request */
    ChangePageRequest(self, newPage);
    self->currentRequestedPage = newPage;
    if (PageChanged(self))
        {
        mInvalidateRequestPage(self);
        return cAtOk;
        }

    return cAtErrorAgain;
    }

static uint32 ReadDdrControlRegister(ThaModuleHardSur self)
    {
    return cAf6Reg_FMPM_Read_DDR_Control + ThaModuleHardSurBaseAddress(self);
    }

static uint32 DdrAccessTimeoutInUs(ThaModuleHardSur self)
    {
    AtUnused(self);
    return 100000;
    }

static eAtRet DdrReadDone(ThaModuleHardSur self, uint32 page, uint8 type, uint32 address, eBool read2Clear)
    {
    uint32 elapse = 0;
    tAtOsalCurTime curTime, startTime;
    static const uint32 cExpectedTimeInUs = 400;
    uint32 cTimeoutUs = mMethodsGet(self)->DdrAccessTimeoutInUs(self);
    uint32 regAddr = ReadDdrControlRegister(self);
    uint32 regVal = mModuleHwRead(self, regAddr);
    eBool isIncreased = cAtFalse;

    if (type >= cThaModuleSurCounterInvalid)
        return cAtErrorInvlParm;

    /* Set page, address, counter type. */
    mRegFieldSet(regVal, cAf6_FMPM_Read_DDR_Control_Page_, page);
    mRegFieldSet(regVal, cAf6_FMPM_Read_DDR_Control_Type_, type);
    mRegFieldSet(regVal, cAf6_FMPM_Read_DDR_Control_ADR_, address);
    mRegFieldSet(regVal, cAf6_FMPM_Read_DDR_Control_R2C_, read2Clear ? 1 : 0);
    mModuleHwWrite(self, regAddr, regVal);

    /* Request read. */
    mModuleHwWrite(self, regAddr, (regVal & ~cAf6_FMPM_Read_DDR_Control_Start_Mask));
    mModuleHwWrite(self, regAddr, (regVal | cAf6_FMPM_Read_DDR_Control_Start_Mask));

    /* Wait for HW */
    AtOsalCurTimeGet(&startTime);
    while (elapse < cTimeoutUs)
        {
        if (!isIncreased && (elapse > cExpectedTimeInUs))
            {
            self->numLongReadings += 1;
            isIncreased = cAtTrue;
            }

        regVal = mModuleHwRead(self, regAddr);
        if (regVal & cAf6_FMPM_Read_DDR_Control_Done_Mask)
            return cAtOk;

        if (IsSimulated((AtModule)self))
            return cAtOk;

        /* Retry */
        AtOsalCurTimeGet(&curTime);
        elapse = AtOsalDifferenceTimeInUs(&curTime, &startTime);
        }

    return cAtErrorRamAccessTimeout;
    }

static eAtRet PeriodExpireWithBlocking(AtModuleSur self, eBool blocking)
    {
    eAtRet ret = cAtOk;
    AtModule module = (AtModule)self;
    eAtSurPeriodExpireMethod currentMethod;

    if (!HwIsReady(module))
        return cAtErrorModeNotSupport;

    AtModuleLock(module);

    /* Change page can only be done in manual expiring */
    currentMethod = AtModuleSurExpireMethodGet(self);
    if (currentMethod != cAtSurPeriodExpireMethodManual)
        AtModuleSurExpireMethodSet(self, cAtSurPeriodExpireMethodManual);

    if (blocking)
        ret = PageSwitchBlocking(mThis(self));
    else
        ret = PageSwitchNoneBlocking(mThis(self));

    /* Revert current mode */
    if (currentMethod != cAtSurPeriodExpireMethodManual)
        AtModuleSurExpireMethodSet(self, currentMethod);

    AtModuleUnLock(module);

    return ret;
    }

static eAtRet PeriodExpire(AtModuleSur self)
    {
    return PeriodExpireWithBlocking(self, cAtTrue);
    }

static eAtRet PeriodAsyncExpire(AtModuleSur self)
    {
    return PeriodExpireWithBlocking(self, cAtFalse);
    }

static uint32 TimeTriggerRegister(AtModuleSur self)
    {
    return cAf6Reg_FMPM_Syn_Get_time + ThaModuleHardSurBaseAddress(mThis(self));
    }

static eAtRet TimeTrigger(AtModuleSur self)
    {
    uint32 regAddr = TimeTriggerRegister(self);
    uint32 regVal = mModuleHwRead(self, regAddr);

    mRegFieldSet(regVal, cAf6_FMPM_Syn_Get_time_gettime_, 1);
    mModuleHwWrite(self, regAddr, regVal);
    mRegFieldSet(regVal, cAf6_FMPM_Syn_Get_time_gettime_, 0);
    mModuleHwWrite(self, regAddr, regVal);

    return 0;
    }

static uint32 TimeValueRegister(AtModuleSur self)
    {
    return cAf6Reg_FMPM_Time_Value + ThaModuleHardSurBaseAddress(mThis(self));
    }

static uint32 TriggerElapsedSecondsGet(AtModuleSur self)
    {
    uint32 regVal;

    if (TimeTrigger(self) != cAtOk)
        return 0;

    regVal = mModuleHwRead(self, TimeValueRegister(self));
    return mRegField(regVal, cAf6_FMPM_Time_Value_cnt1s_);
    }

static uint32 PmTickCounterGet(AtModuleSur self)
    {
    uint32 regVal = mModuleHwRead(self, ChangePageRegister(mThis(self)));
    return mRegField(regVal, cAf6_FMPM_Change_Page_DDR_pmtick_cnt_);
    }

static eBool HasPmTickCounter(ThaModuleHardSur self)
    {
    /* This feature is provided together with ability of PM enable as default */
    return mMethodsGet(self)->CanEnableHwPmEngine(self);
    }

static uint32 PeriodElapsedSecondsGet(AtModuleSur self)
    {
    if (HasPmTickCounter(mThis(self)))
        return PmTickCounterGet(self);

    return TriggerElapsedSecondsGet(self);
    }

static uint32 PeriodicProcess(AtModule self, uint32 periodInMs)
    {
    static const uint32 cPeriodMs = 15 * 60 * 1000;
    tAtOsalCurTime startTime, stopTime;

    /* Not recommend */
    if (periodInMs != cPeriodMs)
        return 0;

    AtOsalCurTimeGet(&startTime);
    AtModuleSurPeriodExpire((AtModuleSur)self);
    AtOsalCurTimeGet(&stopTime);

    return mTimeIntervalInMsGet(startTime, stopTime);
    }

static uint32 RecommendedPeriodInMs(AtModule self)
    {
    static const uint32 c15Minutes = 900000;
    AtUnused(self);
    return c15Minutes;
    }

static AtPmRegister CurrentSecondRegisterCreate(AtModuleSur self, AtPmParam param)
    {
    AtUnused(self);
    return ThaHardPmRegisterCurrentSecondNew(param);
    }

static AtPmRegister CurrentPeriodRegisterCreate(AtModuleSur self, AtPmParam param)
    {
    AtUnused(self);
    return ThaHardPmRegisterNew(param, cAtPmCurrentPeriodRegister);
    }

static AtPmRegister PreviousPeriodRegisterCreate(AtModuleSur self, AtPmParam param)
    {
    AtUnused(self);
    return ThaHardPmRegisterNew(param, cAtPmPreviousPeriodRegister);
    }

static AtPmRegister CurrentDayRegisterCreate(AtModuleSur self, AtPmParam param)
    {
    AtUnused(self);
    return ThaHardPmRegisterNew(param, cAtPmCurrentDayRegister);
    }

static AtPmRegister PreviousDayRegisterCreate(AtModuleSur self, AtPmParam param)
    {
    AtUnused(self);
    return ThaHardPmRegisterNew(param, cAtPmPreviousDayRegister);
    }

static AtPmRegister RecentRegisterCreate(AtModuleSur self, AtPmParam param, uint8 recentPeriod)
    {
    AtUnused(self);
    AtUnused(recentPeriod);
    return ThaHardPmRegisterNew(param, cAtPmRecentPeriodRegister);
    }

static eAtRet SdhChannelHwMasterStsId(AtPw pw, uint8 *line, uint8* slice, uint8 *hwSts)
    {
    AtChannel channel = AtPwBoundCircuitGet(pw);

    *line = AtSdhChannelLineGet((AtSdhChannel)channel);
    return ThaSdhChannel2HwMasterStsId((AtSdhChannel)channel, cThaModuleOcn, slice, hwSts);
    }

static uint32 BaseAddress(ThaModuleHardSur self)
    {
    AtUnused(self);
    return cBit31_0;
    }

static uint32 IndirectDe1AddressShift(ThaModuleHardSur self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 LineCurrentFailuresAddr(ThaModuleHardSur self)
    {
    return mMethodsGet(self)->BaseAddress(self) + cAf6Reg_FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1_Base;
    }

static uint32 LineFailuresInterruptAddr(ThaModuleHardSur self)
    {
    return mMethodsGet(self)->BaseAddress(self) + cAf6Reg_FMPM_FM_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_Base;
    }

static uint32 LineFailuresInterruptMaskAddr(ThaModuleHardSur self)
    {
    return mMethodsGet(self)->BaseAddress(self) + cAf6Reg_FMPM_FM_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_Base;
    }

static uint32 LineTcaInterruptAddr(ThaModuleHardSur self)
    {
    return mMethodsGet(self)->BaseAddress(self) + cAf6Reg_FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_Base;
    }

static uint32 LineTcaInterruptMaskAddr(ThaModuleHardSur self)
    {
    return mMethodsGet(self)->BaseAddress(self) + cAf6Reg_FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_Base;
    }

static uint32 StsCurrentFailuresAddr(ThaModuleHardSur self)
    {
    return mMethodsGet(self)->BaseAddress(self) + cAf6Reg_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_Base;
    }

static uint32 StsFailuresInterruptAddr(ThaModuleHardSur self)
    {
    return mMethodsGet(self)->BaseAddress(self) + cAf6Reg_FMPM_FM_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_Base;
    }

static uint32 StsFailuresInterruptMaskAddr(ThaModuleHardSur self)
    {
    return mMethodsGet(self)->BaseAddress(self) + cAf6Reg_FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_Base;
    }

static uint32 StsTcaInterruptAddr(ThaModuleHardSur self)
    {
    return mMethodsGet(self)->BaseAddress(self) + cAf6Reg_FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_Base;
    }

static uint32 StsTcaInterruptMaskAddr(ThaModuleHardSur self)
    {
    return mMethodsGet(self)->BaseAddress(self) + cAf6Reg_FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_Base;
    }

static uint32 VtCurrentFailuresAddr(ThaModuleHardSur self)
    {
    return mMethodsGet(self)->BaseAddress(self) + cAf6Reg_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_Base;
    }

static uint32 VtFailuresInterruptAddr(ThaModuleHardSur self)
    {
    return mMethodsGet(self)->BaseAddress(self) + cAf6Reg_FMPM_FM_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_Base;
    }

static uint32 VtFailuresInterruptMaskAddr(ThaModuleHardSur self)
    {
    return mMethodsGet(self)->BaseAddress(self) + cAf6Reg_FMPM_FM_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_Base;
    }

static uint32 VtTcaInterruptAddr(ThaModuleHardSur self)
    {
    return mMethodsGet(self)->BaseAddress(self) + cAf6Reg_FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_Base;
    }

static uint32 VtTcaInterruptMaskAddr(ThaModuleHardSur self)
    {
    return mMethodsGet(self)->BaseAddress(self) + cAf6Reg_FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_Base;
    }

static uint32 De3CurrentFailuresAddr(ThaModuleHardSur self)
    {
    return mMethodsGet(self)->BaseAddress(self) + cAf6Reg_FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3_Base;
    }

static uint32 De3FailuresInterruptAddr(ThaModuleHardSur self)
    {
    return mMethodsGet(self)->BaseAddress(self) + cAf6Reg_FMPM_FM_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_Base;
    }

static uint32 De3FailuresInterruptMaskAddr(ThaModuleHardSur self)
    {
    return mMethodsGet(self)->BaseAddress(self) + cAf6Reg_FMPM_FM_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_Base;
    }

static uint32 De3TcaInterruptAddr(ThaModuleHardSur self)
    {
    return mMethodsGet(self)->BaseAddress(self) + cAf6Reg_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_Base;
    }

static uint32 De3TcaInterruptMaskAddr(ThaModuleHardSur self)
    {
    return mMethodsGet(self)->BaseAddress(self) + cAf6Reg_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_Base;
    }

static uint32 De1CurrentFailuresAddr(ThaModuleHardSur self)
    {
    return mMethodsGet(self)->BaseAddress(self) + cAf6Reg_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_Base;
    }

static uint32 De1FailuresInterruptAddr(ThaModuleHardSur self)
    {
    return mMethodsGet(self)->BaseAddress(self) + cAf6Reg_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_Base;
    }

static uint32 De1FailuresInterruptMaskAddr(ThaModuleHardSur self)
    {
    return mMethodsGet(self)->BaseAddress(self) + cAf6Reg_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_Base;
    }

static uint32 De1TcaInterruptAddr(ThaModuleHardSur self)
    {
    return mMethodsGet(self)->BaseAddress(self) + cAf6Reg_FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_Base;
    }

static uint32 De1TcaInterruptMaskAddr(ThaModuleHardSur self)
    {
    return mMethodsGet(self)->BaseAddress(self) + cAf6Reg_FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_Base;
    }

static uint32 PwCurrentFailuresAddr(ThaModuleHardSur self)
    {
    return mMethodsGet(self)->BaseAddress(self) + cAf6Reg_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_Base;
    }

static uint32 PwFailuresInterruptAddr(ThaModuleHardSur self)
    {
    return mMethodsGet(self)->BaseAddress(self) + cAf6Reg_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_Base;
    }

static uint32 PwFailuresInterruptMaskAddr(ThaModuleHardSur self)
    {
    return mMethodsGet(self)->BaseAddress(self) + mMethodsGet(self)->FmPWInterruptMASKPerTypePerPWBase(self);
    }

static uint32 PwTcaInterruptAddr(ThaModuleHardSur self)
    {
    return mMethodsGet(self)->BaseAddress(self) + cAf6Reg_FMPM_TCA_PW_Interrupt_Sticky_Per_Type_Per_PW_Base;
    }

static uint32 PwTcaInterruptMaskAddr(ThaModuleHardSur self)
    {
    return mMethodsGet(self)->BaseAddress(self) + cAf6Reg_FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW_Base;
    }

static uint32 LineThresholdRegister(ThaModuleHardSur self)
    {
    return mMethodsGet(self)->BaseAddress(self) + cAf6Reg_FMPM_Threshold_Type_of_Section_Base;
    }

static uint32 StsThresholdRegister(ThaModuleHardSur self)
    {
    return mMethodsGet(self)->BaseAddress(self) + cAf6Reg_FMPM_Threshold_Type_of_STS_Base;
    }

static uint32 VtThresholdRegister(ThaModuleHardSur self)
    {
    return mMethodsGet(self)->BaseAddress(self) + cAf6Reg_FMPM_Threshold_Type_of_VT_Base;
    }

static uint32 De3ThresholdRegister(ThaModuleHardSur self)
    {
    return mMethodsGet(self)->BaseAddress(self) + cAf6Reg_FMPM_Threshold_Type_of_DS3_Base;
    }

static uint32 De1ThresholdRegister(ThaModuleHardSur self)
    {
    return mMethodsGet(self)->BaseAddress(self) + cAf6Reg_FMPM_Threshold_Type_of_DS1_Base;
    }

static uint32 PwThresholdRegister(ThaModuleHardSur self)
    {
    return mMethodsGet(self)->BaseAddress(self) + cAf6Reg_FMPM_Threshold_Type_of_PW_Base;
    }

static eBool IsHoPw(ThaModuleHardSur self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cAtFalse;
    }

static eBool NeedLatchHoPw(ThaModuleHardSur self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool IsLoPw(ThaModuleHardSur self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cAtFalse;
    }

static eAtRet LoPwCounterLatch(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    uint32 page = ThaModuleHardSurCurrentPage(self);
    uint32 pwId = AtChannelIdGet((AtChannel)pw);
    return ThaModuleHardSurDdrRead2Clear(self, (uint8)page, cThaModuleSurCounterPwStatistic, pwId, self->loPwCounters, clear);
    }

static eAtRet HoCounterLatch(ThaModuleHardSur self, eBool clear)
    {
    uint32 regAddr = cAf6Reg_FMPM_HO_Counter_Mode + ThaModuleHardSurBaseAddress(self);
    uint32 regVal;

    regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_FMPM_HO_Counter_Mode_ho_mod_, clear ? 1 : 0);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet PwCounterLatch(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    if (mMethodsGet(mThis(self))->IsLoPw(mThis(self), pw))
        return LoPwCounterLatch(self, pw, clear);

    if (mMethodsGet(mThis(self))->IsHoPw(mThis(self), pw) && mMethodsGet(mThis(self))->NeedLatchHoPw(mThis(self)))
        return HoCounterLatch(self, clear);

    return cAtOk;
    }

static uint32 LoPwCounterGet(ThaModuleHardSur self, AtPw pw, eBool clear, uint32 counterIndex)
    {
    AtUnused(clear);
    AtUnused(pw);
    return self->loPwCounters[counterIndex];
    }

static uint32 CounterCommonOffset(AtPw pw)
    {
    uint8 slice = 0, hwLine = 0, hwSts = 0;
    
    SdhChannelHwMasterStsId(pw, &hwLine, &slice, &hwSts);
    slice = hwSts % 2;
    hwSts = hwSts >> 1;
    return (hwLine * 0x400UL) + (slice * 0x200UL) + (hwSts * 0x10UL);
    }

static uint32 HoTxCounterRegister(AtPw pw, uint32 counterType)
    {
    uint32 offset = CounterCommonOffset(pw) + (counterType * 0x2);
    uint32 baseAddress = ThaModuleHardSurBaseAddress(mModuleSur(pw));
    return baseAddress + cAf6Reg_FMPM_HO_Counter_Of_TX_PW_Base + offset;
    }

static uint32 HoRxCounterRegister(AtPw pw, uint32 counterType)
    {
    uint32 offset = CounterCommonOffset(pw) + counterType;
    uint32 baseAddress = ThaModuleHardSurBaseAddress(mModuleSur(pw));
    return baseAddress + cAf6Reg_FMPM_HO_Counter_Of_RX_PW_Base + offset;
    }

static uint32 PwTxPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    if (mMethodsGet(self)->IsHoPw(self, pw) && mMethodsGet(self)->NeedLatchHoPw(self))
        return mModuleHwRead(self, HoTxCounterRegister(pw, cAf6Reg_tho_pen_cnt_Packets));

    if (mMethodsGet(self)->IsLoPw(self, pw))
        return LoPwCounterGet(self, pw, clear, cAf6_PwCnt_TX_Packet_Index);

    return 0;
    }

static uint32 PwTxBytesGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    if (mMethodsGet(self)->IsHoPw(self, pw) && mMethodsGet(self)->NeedLatchHoPw(self))
        return mModuleHwRead(self, HoTxCounterRegister(pw, cAf6Reg_tho_pen_cnt_Bytes));

    if (mMethodsGet(self)->IsLoPw(self, pw))
        return LoPwCounterGet(self, pw, clear, cAf6_PwCnt_TX_NOB_Index);

    return 0;
    }

static uint32 PwTxLbitPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    if (mMethodsGet(self)->IsHoPw(self, pw) && mMethodsGet(self)->NeedLatchHoPw(self))
        return mModuleHwRead(self, HoTxCounterRegister(pw, cAf6Reg_tho_pen_cnt_Lbit));

    if (mMethodsGet(self)->IsLoPw(self, pw))
        return LoPwCounterGet(self, pw, clear, cAf6_PwCnt_TX_LBIT_Index);

    return 0;
    }

static uint32 PwTxRbitPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    if (mMethodsGet(self)->IsHoPw(self, pw) && mMethodsGet(self)->NeedLatchHoPw(self))
        return mModuleHwRead(self, HoTxCounterRegister(pw, cAf6Reg_tho_pen_cnt_Rbit));

    if (mMethodsGet(self)->IsLoPw(self, pw))
        return LoPwCounterGet(self, pw, clear, cAf6_PwCnt_TX_RBIT_Index);

    return 0;
    }

static uint32 PwTxNbitPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    if (mMethodsGet(self)->IsHoPw(self, pw) && mMethodsGet(self)->NeedLatchHoPw(self))
        return mModuleHwRead(self, HoTxCounterRegister(pw, cAf6Reg_tho_pen_cnt_Nbit));

    if (mMethodsGet(self)->IsLoPw(self, pw))
        return LoPwCounterGet(self, pw, clear, cAf6_PwCnt_TX_NBIT_Index);

    return 0;
    }

static uint32 PwTxMbitPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    /* For CES, hardware uses NBit counter */
    return PwTxNbitPacketsGet(self, pw, clear);
    }

static uint32 PwTxPbitPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    if (mMethodsGet(self)->IsHoPw(self, pw) && mMethodsGet(self)->NeedLatchHoPw(self))
        return mModuleHwRead(self, HoTxCounterRegister(pw, cAf6Reg_tho_pen_cnt_Pbit));

    if (mMethodsGet(self)->IsLoPw(self, pw))
        return LoPwCounterGet(self, pw, clear, cAf6_PwCnt_TX_PBIT_Index);

    return 0;
    }

static uint32 PwRxPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    if (mMethodsGet(self)->IsHoPw(self, pw) && mMethodsGet(self)->NeedLatchHoPw(self))
        return mModuleHwRead(self, HoRxCounterRegister(pw, cAf6Reg_rho_pen_cnt_Packet));

    if (mMethodsGet(self)->IsLoPw(self, pw))
        return LoPwCounterGet(self, pw, clear, cAf6_PwCnt_RX_Packet_Index);

    return 0;
    }

static uint32 PwRxMalformedPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    if (mMethodsGet(self)->IsHoPw(self, pw) && mMethodsGet(self)->NeedLatchHoPw(self))
        return mModuleHwRead(self, HoRxCounterRegister(pw, cAf6Reg_rho_pen_cnt_Malf));

    if (mMethodsGet(self)->IsLoPw(self, pw))
        return LoPwCounterGet(self, pw, clear, cAf6_PwCnt_RX_MALF_Index);
    return 0;
    }

static uint32 PwRxStrayPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    if (mMethodsGet(self)->IsHoPw(self, pw) && mMethodsGet(self)->NeedLatchHoPw(self))
        return mModuleHwRead(self, HoRxCounterRegister(pw, cAf6Reg_rho_pen_cnt_Stray));

    if (mMethodsGet(self)->IsLoPw(self, pw))
        return LoPwCounterGet(self, pw, clear, cAf6_PwCnt_RX_STRAY_Index);
    return 0;
    }

static uint32 PwRxOamPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(clear);
    return 0;
    }

static uint32 PwRxLbitPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    if (mMethodsGet(self)->IsHoPw(self, pw) && mMethodsGet(self)->NeedLatchHoPw(self))
        return mModuleHwRead(self, HoRxCounterRegister(pw, cAf6Reg_rho_pen_cnt_Lbit));

    if (mMethodsGet(self)->IsLoPw(self, pw) && mMethodsGet(self)->NeedLatchHoPw(self))
        return LoPwCounterGet(self, pw, clear, cAf6_PwCnt_RX_LBIT_Index);

    return 0;
    }

static uint32 PwRxRbitPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    if (mMethodsGet(self)->IsHoPw(self, pw) && mMethodsGet(self)->NeedLatchHoPw(self))
        return mModuleHwRead(self, HoRxCounterRegister(pw, cAf6Reg_rho_pen_cnt_Rbit));

    if (mMethodsGet(self)->IsLoPw(self, pw))
        return LoPwCounterGet(self, pw, clear, cAf6_PwCnt_RX_RBIT_Index);
    return 0;
    }

static uint32 PwRxNbitPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    if (mMethodsGet(self)->IsHoPw(self, pw) && mMethodsGet(self)->NeedLatchHoPw(self))
        return mModuleHwRead(self, HoRxCounterRegister(pw, cAf6Reg_rho_pen_cnt_Nbit));

    if (mMethodsGet(self)->IsLoPw(self, pw))
        return LoPwCounterGet(self, pw, clear, cAf6_PwCnt_RX_NBIT_Index);
    return 0;
    }

static uint32 PwRxMbitPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    /* For CES, hardware uses NBit counter */
    return PwRxNbitPacketsGet(self, pw, clear);
    }

static uint32 PwRxPbitPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    if (mMethodsGet(self)->IsHoPw(self, pw) && mMethodsGet(self)->NeedLatchHoPw(self))
        return mModuleHwRead(self, HoRxCounterRegister(pw, cAf6Reg_rho_pen_cnt_Pbit));

    if (mMethodsGet(self)->IsLoPw(self, pw))
        return LoPwCounterGet(self, pw, clear, cAf6_PwCnt_RX_PBIT_Index);
    return 0;
    }

static uint32 PwRxBytesGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    if (mMethodsGet(self)->IsHoPw(self, pw) && mMethodsGet(self)->NeedLatchHoPw(self))
        return mModuleHwRead(self, HoRxCounterRegister(pw, cAf6Reg_rho_pen_cnt_Bytes));

    if (mMethodsGet(self)->IsLoPw(self, pw))
        return LoPwCounterGet(self, pw, clear, cAf6_PwCnt_RX_NOB_Index);
    return 0;
    }

static uint32 PwRxReorderedPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    /* Hardware use Early counter for this kind of packets */
    if (mMethodsGet(self)->IsHoPw(self, pw) && mMethodsGet(self)->NeedLatchHoPw(self))
        return mModuleHwRead(self, HoRxCounterRegister(pw, cAf6Reg_rho_pen_cnt_Early));

    if (mMethodsGet(self)->IsLoPw(self, pw))
        return LoPwCounterGet(self, pw, clear, cAf6_PwCnt_RX_EARLY_Index);
    return 0;
    }

static uint32 PwRxLostPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    if (mMethodsGet(self)->IsHoPw(self, pw) && mMethodsGet(self)->NeedLatchHoPw(self))
        return mModuleHwRead(self, HoRxCounterRegister(pw, cAf6Reg_rho_pen_cnt_Lost));

    if (mMethodsGet(self)->IsLoPw(self, pw))
        return LoPwCounterGet(self, pw, clear, cAf6_PwCnt_RX_LOST_Index);
    return 0;
    }

static uint32 PwRxOutOfSeqDropedPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    /* Hardware use Late counter for this kind of error */
    if (mMethodsGet(self)->IsHoPw(self, pw) && mMethodsGet(self)->NeedLatchHoPw(self))
        return mModuleHwRead(self, HoRxCounterRegister(pw, cAf6Reg_rho_pen_cnt_Late));

    if (mMethodsGet(self)->IsLoPw(self, pw))
        return LoPwCounterGet(self, pw, clear, cAf6_PwCnt_RX_LATE_Index);

    return 0;
    }

static uint32 PwRxJitBufOverrunGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    if (mMethodsGet(self)->IsHoPw(self, pw) && mMethodsGet(self)->NeedLatchHoPw(self))
        return mModuleHwRead(self, HoRxCounterRegister(pw, cAf6Reg_rho_pen_cnt_Overrun));

    if (mMethodsGet(self)->IsLoPw(self, pw))
        return LoPwCounterGet(self, pw, clear, cAf6_PwCnt_RX_OVERUN_Index);

    return 0;
    }

static uint32 PwRxJitBufUnderrunGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    if (mMethodsGet(self)->IsHoPw(self, pw) && mMethodsGet(self)->NeedLatchHoPw(self))
        return mModuleHwRead(self, HoRxCounterRegister(pw, cAf6Reg_rho_pen_cnt_Underrun));

    if (mMethodsGet(self)->IsLoPw(self, pw))
        return LoPwCounterGet(self, pw, clear, cAf6_PwCnt_RX_UNDERUN_Index);

    return 0;
    }

static uint32 LoRxLopsCounterGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    return (LoPwCounterGet(self, pw, clear, cAf6_PwCnt_RX_LOPSTA_Index) + LoPwCounterGet(self, pw, clear, cAf6_PwCnt_RX_LOPSYN_Index));
    }

static uint32 PwRxLopsGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    if (mMethodsGet(self)->IsHoPw(self, pw) && mMethodsGet(self)->NeedLatchHoPw(self))
        return mModuleHwRead(self, HoRxCounterRegister(pw, cAf6Reg_rho_pen_cnt_Lops));

    if (mMethodsGet(self)->IsLoPw(self, pw))
        return LoRxLopsCounterGet(self, pw, clear);

    return 0;
    }

static uint32 PwRxPacketsSentToTdmGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(clear);
    return 0;
    }

static AtPmParam SdhLineParamCreate(AtModuleSur self, AtSurEngine engine, const char *name, uint32 paramType)
    {
    AtUnused(self);
    return ThaHardPmParamNew(engine, name, paramType);
    }

static AtPmParam SdhPathParamCreate(AtModuleSur self, AtSurEngine engine, const char *name, uint32 paramType)
    {
    AtUnused(self);
    return ThaHardPmParamNew(engine, name, paramType);
    }

static AtPmParam PdhDe3ParamCreate(AtModuleSur self, AtSurEngine engine, const char *name, uint32 paramType)
    {
    AtUnused(self);
    return ThaHardPmParamNew(engine, name, paramType);
    }

static AtPmParam PdhDe1ParamCreate(AtModuleSur self, AtSurEngine engine, const char *name, uint32 paramType)
    {
    AtUnused(self);
    return ThaHardPmParamNew(engine, name, paramType);
    }

static AtPmParam PwParamCreate(AtModuleSur self, AtSurEngine engine, const char *name, uint32 paramType)
    {
    AtUnused(self);
    return ThaHardPmParamNew(engine, name, paramType);
    }

static eAtRet DdrCountersR2c(AtSurEngine engine, uint8 type, uint32 offset, uint32 *regVal, eBool r2c)
    {
    ThaModuleHardSur self = (ThaModuleHardSur)AtSurEngineModuleGet(engine);
    uint32 page = ThaModuleHardSurCurrentPage(self);

    return ThaModuleHardSurDdrRead2Clear(self, page, type, offset, regVal, r2c);
    }

static eAtRet DdrHistoryCountersR2c(AtSurEngine engine, uint8 type, uint32 offset, uint32 *regVal, eBool r2c)
    {
    ThaModuleHardSur self = (ThaModuleHardSur)AtSurEngineModuleGet(engine);
    uint32 page = ThaModuleHardSurCurrentPage(self) ? 0 : 1;

    return ThaModuleHardSurDdrRead2Clear(self, page, type, offset, regVal, r2c);
    }

static void DdrLock(ThaModuleHardSur self)
    {
    AtOsalMutexLock(mThis(self)->ddrMutex);
    }

static void DdrUnLock(ThaModuleHardSur self)
    {
    AtOsalMutexUnLock(mThis(self)->ddrMutex);
    }

static uint32 VersionNumberRead(ThaModuleHardSur self)
    {
    uint32 regAddr = cAf6Reg_FMPM_Block_Version + ThaModuleHardSurBaseAddress(self);
    return mModuleHwRead(self, regAddr);
    }

static uint32 VersionNumber(ThaModuleHardSur self)
    {
    if (self->versionNumber == 0)
        self->versionNumber = VersionNumberRead(self);

    if (self->versionNumber == 0xCAFECAFE)
        return 0;

    return self->versionNumber;
    }

static eBool ShouldEnablePwCounters(ThaModuleHardSur self)
    {
    /* Concrete should know */
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet HwEnable(ThaModuleHardSur self, eBool enable)
    {
    uint32 regAddr;
    uint32 regVal;

    regAddr = ResetRegister(self);
    regVal = mModuleHwRead(self, regAddr);
    regVal = (enable) ? (regVal | cAf6_FMPM_Force_Reset_Core_drst_enb_Mask) : (regVal & ~cAf6_FMPM_Force_Reset_Core_drst_enb_Mask);

    /* Only explicitly touch this field when concretes need */
    if (mMethodsGet(self)->ShouldEnablePwCounters(self))
        mRegFieldSet(regVal, cAf6_FMPM_Force_Reset_Core_lopwcnt_enb_, 1);

    mModuleHwWrite(self, regAddr, regVal);

    if (ShouldReset((AtModule)self))
        return mMethodsGet(mThis(self))->Reset(self);

    return cAtOk;
    }

static eAtRet Activate(AtModule self)
    {
    return HwEnable(mThis(self), cAtTrue);
    }

static eAtRet Deactivate(AtModule self)
    {
    return HwEnable(mThis(self), cAtFalse);
    }

static eBool IsActive(AtModule self)
    {
    uint32 regVal = mModuleHwRead(self, ResetRegister(mThis(self)));
    return (regVal & cAf6_FMPM_Force_Reset_Core_drst_enb_Mask) ? cAtTrue : cAtFalse;
    }

static uint32 Clock125usDefaultValue(ThaModuleHardSur self)
    {
    AtUnused(self);
    return 0x4BEE;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaModuleHardSur object = (ThaModuleHardSur)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(versionNumber);
    mEncodeUInt(currentRequestedPage);

    mEncodeNone(ddrMutex);
    mEncodeNone(lines);
    mEncodeNone(hoPaths);
    mEncodeNone(loPaths);
    mEncodeNone(de3s);
    mEncodeNone(de1s);
    mEncodeNone(pws);
    mEncodeNone(loPwCounters);
    mEncodeNone(maxChangePageInMs);
    mEncodeNone(maxReadingInUs);
    mEncodeNone(minReadingInUs);
    mEncodeNone(numLongReadings);
    mEncodeNone(maxChangePageTimeval);
    mEncodeNone(maxReadingTimeval);
    mEncodeNone(minReadingTimeval);
    mEncodeNone(averReadingTimeval);
    mEncodeNone(loHdlcChannelCounters);
    mEncodeNone(maxHoldRegReadingInUs);
    mEncodeNone(maxHoldRegAddress);
    mEncodeNone(maxWaitingDdrInUs);
    }

static eBool LineIsSupported(AtModuleSur self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool IsHoHdlcChannel(AtChannel self)
    {

    return ThaHdlcChannelCircuitIsHo((ThaHdlcChannel)self) ? cAtTrue : cAtFalse;
    }

static uint32 HoHdlcChannelDefaultOffset(AtChannel self)
    {
    uint8 hwLineId, hwStsId, oddSliceId, localStsId;
    AtSdhChannel sdhChannel = (AtSdhChannel)AtEncapChannelBoundPhyGet((AtEncapChannel)self);

    ThaSdhChannel2HwMasterStsId(sdhChannel, cThaModuleOcn, &hwLineId, &hwStsId);
    oddSliceId = hwStsId & cBit0;
    localStsId = (hwStsId >> 1);

    return (hwLineId * 0x400UL) + (oddSliceId * 0x200UL) + (localStsId * 0x10UL);
    }

static uint32 TxHoHdlcChannelRegister(AtChannel self, uint32 counterType, eBool r2c)
    {
    uint32 baseAddress, address;
    AtUnused(r2c);

    if (mIsNullPointer(self))
        return cBit31_0;

    baseAddress = ThaModuleHardSurBaseAddress(mModuleSur(self));
    address = cAf6_FMPM_HO_Counter_Of_ENC_Base + counterType * 2;

    return baseAddress + HoHdlcChannelDefaultOffset(self) + address;
    }

static uint32 RxHoHdlcChannelRegister(AtChannel self, uint32 counterType, eBool r2c)
    {
    uint32 baseAddress, address;
    AtUnused(r2c);

    if (mIsNullPointer(self))
        return cBit31_0;

    baseAddress = ThaModuleHardSurBaseAddress(mModuleSur(self));
    address = cAf6_FMPM_HO_Counter_Of_DEC_Base + counterType;

    return baseAddress + HoHdlcChannelDefaultOffset(self) + address;
    }

static eAtRet LoHdlcChannelCounterLatch(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    uint32 page = ThaModuleHardSurCurrentPage(self);
    uint32 channelId = ThaHdlcChannelSurCounterId((ThaHdlcChannel)channel);

    return ThaModuleHardSurDdrRead2Clear(self, (uint8)page, cThaModuleSurCounterEncDecStatistic, channelId, self->loHdlcChannelCounters, r2c);
    }

static eAtRet HoChannelCounterModeSet(ThaModuleHardSur self, eBool r2c)
    {
    uint32 regAddr = ThaModuleHardSurBaseAddress(self) + cAf6Reg_FMPM_HO_Counter_Mode;
    uint32 regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_FMPM_HO_Counter_Mode_ho_mod_, r2c ? 0x1 : 0x0);
    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eAtRet HdlcChannelCounterLatch(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    return IsHoHdlcChannel(channel) ? HoChannelCounterModeSet(self, r2c) : LoHdlcChannelCounterLatch(self, channel, r2c);
    }

static uint32 LoHdlcChannelCounterGet(ThaModuleHardSur self, AtChannel channel, eBool r2c, uint32 counterIndex)
    {
    AtUnused(r2c);
    AtUnused(channel);

    return self->loHdlcChannelCounters[counterIndex];
    }

static uint32 HdlcChannelTxGoodPacketsGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    if (IsHoHdlcChannel(channel))
        return mModuleHwRead(self, TxHoHdlcChannelRegister(channel, cAf6_FMPM_HO_Counter_Of_ENC_type_GoodPkt, r2c));

    return LoHdlcChannelCounterGet(self, channel, r2c, cAf6_EncCnt_GoodPkt_Index);
    }

static uint32 HdlcChannelTxGoodBytesGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    if (IsHoHdlcChannel(channel))
        return mModuleHwRead(self, TxHoHdlcChannelRegister(channel, cAf6_FMPM_HO_Counter_Of_ENC_type_Byte, r2c));

    return LoHdlcChannelCounterGet(self, channel, r2c, cAf6_EncCnt_GoodBytes_Index);
    }

static uint32 HdlcChannelTxFragmentsGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    if (IsHoHdlcChannel(channel))
        return mModuleHwRead(self, TxHoHdlcChannelRegister(channel, cAf6_FMPM_HO_Counter_Of_ENC_type_Fragment, r2c));

    return LoHdlcChannelCounterGet(self, channel, r2c, cAf6_EncCnt_Fragment_Index);
    }

static uint32 HdlcChannelTxPlainGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    if (IsHoHdlcChannel(channel))
        return mModuleHwRead(self, TxHoHdlcChannelRegister(channel, cAf6_FMPM_HO_Counter_Of_ENC_type_Plain, r2c));

    return LoHdlcChannelCounterGet(self, channel, r2c, cAf6_EncCnt_Plain_Index);
    }

static uint32 HdlcChannelTxOamGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    if (IsHoHdlcChannel(channel))
        return mModuleHwRead(self, TxHoHdlcChannelRegister(channel, cAf6_FMPM_HO_Counter_Of_ENC_type_Oam, r2c));

    return LoHdlcChannelCounterGet(self, channel, r2c, cAf6_EncCnt_Oam_Index);
    }

static uint32 HdlcChannelTxIdleBytesGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    if (IsHoHdlcChannel(channel))
        return mModuleHwRead(self, TxHoHdlcChannelRegister(channel, cAf6_FMPM_HO_Counter_Of_ENC_type_Idle, r2c));

    return LoHdlcChannelCounterGet(self, channel, r2c, cAf6_EncCnt_Idle_Index);
    }

static uint32 HdlcChannelTxAbortsGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    if (IsHoHdlcChannel(channel))
        return mModuleHwRead(self, TxHoHdlcChannelRegister(channel, cAf6_FMPM_HO_Counter_Of_ENC_type_Abort, r2c));

    return LoHdlcChannelCounterGet(self, channel, r2c, cAf6_EncCnt_Abort_Index);
    }

static uint32 HdlcChannelRxGoodPacketsGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    if (IsHoHdlcChannel(channel))
        return mModuleHwRead(self, RxHoHdlcChannelRegister(channel, cAf6_FMPM_HO_Counter_Of_DEC_type_GoodPkt, r2c));

    return LoHdlcChannelCounterGet(self, channel, r2c, cAf6_DecCnt_GoodPackets_Index);
    }

static uint32 HdlcChannelRxBytesGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    if (IsHoHdlcChannel(channel))
        return mModuleHwRead(self, RxHoHdlcChannelRegister(channel, cAf6_FMPM_HO_Counter_Of_DEC_type_TotalByte, r2c));

    return LoHdlcChannelCounterGet(self, channel, r2c, cAf6_DecCnt_Bytes_Index);
    }

static uint32 HdlcChannelRxGoodBytesGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    if (IsHoHdlcChannel(channel))
        return mModuleHwRead(self, RxHoHdlcChannelRegister(channel, cAf6_FMPM_HO_Counter_Of_DEC_type_GoodByte, r2c));

    return LoHdlcChannelCounterGet(self, channel, r2c, cAf6_DecCnt_GoodBytes_Index);
    }

static uint32 HdlcChannelRxFragmentsGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    if (IsHoHdlcChannel(channel))
        return mModuleHwRead(self, RxHoHdlcChannelRegister(channel, cAf6_FMPM_HO_Counter_Of_DEC_type_Fragment, r2c));

    return LoHdlcChannelCounterGet(self, channel, r2c, cAf6_DecCnt_Fragment_Index);
    }

static uint32 HdlcChannelRxPlainGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    if (IsHoHdlcChannel(channel))
        return mModuleHwRead(self, RxHoHdlcChannelRegister(channel, cAf6_FMPM_HO_Counter_Of_DEC_type_Plain, r2c));

    return LoHdlcChannelCounterGet(self, channel, r2c, cAf6_DecCnt_Plain_Index);
    }

static uint32 HdlcChannelRxOamGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    if (IsHoHdlcChannel(channel))
        return mModuleHwRead(self, RxHoHdlcChannelRegister(channel, cAf6_FMPM_HO_Counter_Of_DEC_type_Oam, r2c));

    return LoHdlcChannelCounterGet(self, channel, r2c, cAf6_DecCnt_Oam_Index);
    }

static uint32 HdlcChannelRxAbortsGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    if (IsHoHdlcChannel(channel))
        return mModuleHwRead(self, RxHoHdlcChannelRegister(channel, cAf6_FMPM_HO_Counter_Of_DEC_type_Abort, r2c));

    return LoHdlcChannelCounterGet(self, channel, r2c, cAf6_DecCnt_Abort_Index);
    }

static uint32 HdlcChannelRxIdleBytesGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    if (IsHoHdlcChannel(channel))
        return mModuleHwRead(self, RxHoHdlcChannelRegister(channel, cAf6_FMPM_HO_Counter_Of_DEC_type_Idle, r2c));

    return LoHdlcChannelCounterGet(self, channel, r2c, cAf6_DecCnt_Idle_Index);
    }

static uint32 HdlcChannelRxMRUPacketsGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    if (IsHoHdlcChannel(channel))
        return mModuleHwRead(self, RxHoHdlcChannelRegister(channel, cAf6_FMPM_HO_Counter_Of_DEC_type_Mru, r2c));

    return LoHdlcChannelCounterGet(self, channel, r2c, cAf6_DecCnt_Mru_Index);
    }

static uint32 HdlcChannelRxFcsErrorGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    if (IsHoHdlcChannel(channel))
        return mModuleHwRead(self, RxHoHdlcChannelRegister(channel, cAf6_FMPM_HO_Counter_Of_DEC_type_FcsError, r2c));

    return LoHdlcChannelCounterGet(self, channel, r2c, cAf6_DecCnt_FcsError_Index);
    }

static uint32 HdlcChannelRxDiscardFramesGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    if (IsHoHdlcChannel(channel))
        return mModuleHwRead(self, RxHoHdlcChannelRegister(channel, cAf6_FMPM_HO_Counter_Of_DEC_type_Discard, r2c));

    return LoHdlcChannelCounterGet(self, channel, r2c, cAf6_DecCnt_DiscardPkt_Index);
    }

static uint32 NumThresholdProfiles(AtModuleSur self)
    {
    AtUnused(self);
    return 0;
    }

static AtPmThresholdProfile ThresholdProfileCreate(AtModuleSur self, uint32 profileId)
    {
    return ThaHardPmThresholdProfileNew(self, profileId);
    }

static eBool HasExpireTimeout(ThaModuleHardSur self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool CanEnableHwPmEngine(ThaModuleHardSur self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool HasMonitorTimer(ThaModuleHardSur self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 IdOfHdlcPw(ThaModuleHardSur self, AtPw pw)
    {
    AtUnused(self);
    return AtChannelHwIdGet((AtChannel)pw);
    }

static eBool FailureForwardingStatusIsSupported(ThaModuleHardSur self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool ExpireInterruptIsSupported(ThaModuleHardSur self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 FailureTimerSw2Hw(uint32 timerInMs)
    {
    /*
     * ((timerInMs - 400ms) / 100ms) - 1 = HwValue
     * ==> (timerInMs / 100) - 5 = HwValue
     * ==> unit - 5 = HwValue
     */
    uint32 unit = timerInMs / 100;
    uint32 hwValue;

    if (unit <= 5)
        return cInvalidUint32;

    hwValue = unit - 5;
    if (hwValue > (cAf6_FMPM_Config_Number_Clock_Of_125us_time2p5s_Mask >> cAf6_FMPM_Config_Number_Clock_Of_125us_time2p5s_Shift))
        return cInvalidUint32;

    return hwValue;
    }

static uint32 FailureTimerHw2Sw(uint32 hwValue)
    {
    uint32 unit = hwValue + 5; /* See explanation at FailureTimerSw2Hw() */
    return unit * 100;
    }

static eAtRet FailureTimerSet(AtModuleSur self, uint32 timerInMs, uint32 fieldMask, uint32 fieldShift)
    {
    uint32 regAddr, regVal;
    uint32 hwValue = FailureTimerSw2Hw(timerInMs);

    if (hwValue == cInvalidUint32)
        return cAtErrorOutOfRangParm;

    regAddr = Clock125usRegister((AtModule)self);
    regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, field, hwValue);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static uint32 FailureTimerGet(AtModuleSur self, uint32 fieldMask, uint32 fieldShift)
    {
    uint32 regAddr = Clock125usRegister((AtModule)self);
    uint32 regVal = mModuleHwRead(self, regAddr);
    return FailureTimerHw2Sw(mRegField(regVal, field));
    }

static eAtRet FailureHoldOffTimerSet(AtModuleSur self, uint32 timerInMs)
    {
    return FailureTimerSet(self, timerInMs, cAf6_FMPM_Config_Number_Clock_Of_125us_time2p5s_Mask, cAf6_FMPM_Config_Number_Clock_Of_125us_time2p5s_Shift);
    }

static uint32 FailureHoldOffTimerGet(AtModuleSur self)
    {
    return FailureTimerGet(self, cAf6_FMPM_Config_Number_Clock_Of_125us_time2p5s_Mask, cAf6_FMPM_Config_Number_Clock_Of_125us_time2p5s_Shift);
    }

static eAtRet FailureHoldOnTimerSet(AtModuleSur self, uint32 timerInMs)
    {
    return FailureTimerSet(self, timerInMs, cAf6_FMPM_Config_Number_Clock_Of_125us_time10s_Mask, cAf6_FMPM_Config_Number_Clock_Of_125us_time10s_Shift);
    }

static uint32 FailureHoldOnTimerGet(AtModuleSur self)
    {
    return FailureTimerGet(self, cAf6_FMPM_Config_Number_Clock_Of_125us_time10s_Mask, cAf6_FMPM_Config_Number_Clock_Of_125us_time10s_Shift);
    }

static eAtSurTickSource DefaultTickSourceGet(ThaModuleHardSur self)
    {
    AtUnused(self);
    return cAtSurTickSourceInternal;
    }

static eAtSurPeriodExpireMethod DefaultExpireMethod(ThaModuleHardSur self)
    {
    AtUnused(self);
    return cAtSurPeriodExpireMethodManual;
    }

static uint32 ExpireMethodSw2Hw(eAtSurPeriodExpireMethod method)
    {
    switch (method)
        {
        case cAtSurPeriodExpireMethodManual: return 0;
        case cAtSurPeriodExpireMethodAuto  : return 1;
        case cAtSurPeriodExpireMethodUnknown:
        default:
            return cInvalidUint32;
        }
    }

static eAtSurPeriodExpireMethod ExpireMethodHw2Sw(uint32 method)
    {
    switch (method)
        {
        case 0: return cAtSurPeriodExpireMethodManual;
        case 1: return cAtSurPeriodExpireMethodAuto;
        default:
            return cAtSurPeriodExpireMethodUnknown;
        }
    }

static eAtRet ExpireMethodHwSet(AtModuleSur self, uint32 hwMethod)
    {
    uint32 regAddr = ChangePageRegister(mThis(self));
    uint32 regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_FMPM_Change_Page_DDR_SEL_CHG_PAGE_, hwMethod);
    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static uint32 ExpireMethodHwGet(AtModuleSur self)
    {
    uint32 regAddr = ChangePageRegister(mThis(self));
    uint32 regVal = mModuleHwRead(self, regAddr);
    return mRegField(regVal, cAf6_FMPM_Change_Page_DDR_SEL_CHG_PAGE_);
    }

static eAtRet ExpireMethodSet(AtModuleSur self, eAtSurPeriodExpireMethod method)
    {
    uint32 hwMethod = ExpireMethodSw2Hw(method);

    if (hwMethod == cInvalidUint32)
        return cAtErrorInvlParm;

    return ExpireMethodHwSet(self, hwMethod);
    }

static eAtSurPeriodExpireMethod ExpireMethodGet(AtModuleSur self)
    {
    uint32 hwMethod = ExpireMethodHwGet(self);
    return ExpireMethodHw2Sw(hwMethod);
    }

static uint32 TickSourceSw2Hw(eAtSurTickSource source)
    {
    switch (source)
        {
        case cAtSurTickSourceInternal: return 0;
        case cAtSurTickSourceExternal: return 1;
        case cAtSurTickSourceUnknown:
        default:
            return cInvalidUint32;
        }
    }

static eAtSurTickSource TickSourceHw2Sw(uint32 hwSource)
    {
    switch (hwSource)
        {
        case 0: return cAtSurTickSourceInternal;
        case 1: return cAtSurTickSourceExternal;
        default:
            return cAtSurTickSourceUnknown;
        }
    }

static eAtRet TickSourceHwSet(AtModuleSur self, uint32 hwSource)
    {
    uint32 regAddr = ResetRegister(mThis(self));
    uint32 regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_FMPM_Force_Reset_Core_sel_1s_mode_, hwSource);
    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static uint32 TickSourceHwGet(AtModuleSur self)
    {
    uint32 regAddr = ResetRegister(mThis(self));
    uint32 regVal = mModuleHwRead(self, regAddr);
    return mRegField(regVal, cAf6_FMPM_Force_Reset_Core_sel_1s_mode_);
    }

static eAtRet TickSourceSet(AtModuleSur self, eAtSurTickSource source)
    {
    uint32 hwTickSource = TickSourceSw2Hw(source);

    if (hwTickSource == cInvalidUint32)
        return cAtErrorModeNotSupport;

    return TickSourceHwSet(self, hwTickSource);
    }

static eAtSurTickSource TickSourceGet(AtModuleSur self)
    {
    return TickSourceHw2Sw(TickSourceHwGet(self));
    }

static eAtRet ExpireNotificationEnable(AtModuleSur self, eBool enable)
    {
    uint32 regAddr, regVal;

    if (!mMethodsGet(mThis(self))->ExpireInterruptIsSupported(mThis(self)))
        return enable ? cAtErrorModeNotSupport : cAtOk;

    regAddr = ThaModuleHardSurBaseAddress((ThaModuleHardSur)self) + cAf6Reg_FMPM_FM_Interrupt_Mask;
    regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_FMPM_FM_Interrupt_Mask_PM_SWPAGE_Intr_MSK_, enable ? 1 : 0);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool ExpireNotificationIsEnabled(AtModuleSur self)
    {
    uint32 regAddr, regVal;

    if (!mMethodsGet(mThis(self))->ExpireInterruptIsSupported(mThis(self)))
        return cAtFalse;

    regAddr = ThaModuleHardSurBaseAddress((ThaModuleHardSur)self) + cAf6Reg_FMPM_FM_Interrupt_Mask;
    regVal = mModuleHwRead(self, regAddr);
    return (regVal & cAf6_FMPM_FM_Interrupt_Mask_PM_SWPAGE_Intr_MSK_Mask) ? cAtTrue : cAtFalse;
    }

static void ExpireNotificationProcess(ThaModuleHardSur self)
    {
    uint32 baseAddr = ThaModuleHardSurBaseAddress(self);
    uint32 regAddr = baseAddr + cAf6Reg_FMPM_FM_Interrupt;
    uint32 regVal = mModuleHwRead(self, regAddr);

    if (regVal & cAf6_FMPM_FM_Interrupt_PM_SWPAGE_Intr_Mask)
        {
        /* Clear interrupt status */
        regAddr = baseAddr + cAf6Reg_FMPM_Change_Page_DDR;
        regVal = mModuleHwRead(self, regAddr);
        regVal |= cAf6_FMPM_Change_Page_DDR_CHG_DONE_Mask;
        mModuleHwWrite(self, regAddr, regVal);

        AtModuleSurAllEventListenersCall((AtModuleSur)self);
        }
    }

static void InterruptProcess(AtModule self, uint32 glbIntrStatus, AtIpCore ipCore)
    {
    /* Period expiration should be priorly notified */
    if (mMethodsGet(mThis(self))->ExpireInterruptIsSupported(mThis(self)))
        ExpireNotificationProcess(mThis(self));

    m_AtModuleMethods->InterruptProcess(self, glbIntrStatus, ipCore);
    }

static ThaModuleHardSur SurModule(AtSurEngine engine)
    {
    return (ThaModuleHardSur)AtSurEngineModuleGet(engine);
    }

static uint32 PWFramerInterruptMASKPerPWBase(ThaModuleHardSur self)
    {
    AtUnused(self);
    return cAf6Reg_FMPM_FM_PW_Framer_Interrupt_MASK_Per_PW_Base;
    }

static uint32 FmPWInterruptMASKPerTypePerPWBase(ThaModuleHardSur self)
    {
    AtUnused(self);
    return cAf6Reg_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_Base;
    }

static void HwFlush(ThaModuleHardSur self)
    {
    uint32 baseAddress = ThaModuleHardSurBaseAddress((ThaModuleHardSur)self);
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);

    /* Line */
    ThaDeviceMemoryFlush(device,
                         baseAddress + cAf6Reg_FMPM_FM_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_Base,
                         baseAddress + cAf6Reg_FMPM_FM_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_Base + 0x1FF,
                         0);
    ThaDeviceMemoryFlush(device,
                         baseAddress + cAf6Reg_FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_Base,
                         baseAddress + cAf6Reg_FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_Base + 0x1FF,
                         0);

    /* HO Path */
    ThaDeviceMemoryFlush(device,
                         baseAddress + cAf6Reg_FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_Base,
                         baseAddress + cAf6Reg_FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_Base + 0x1FF,
                         0);
    ThaDeviceMemoryFlush(device,
                         baseAddress + cAf6Reg_FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_Base,
                         baseAddress + cAf6Reg_FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_Base + 0x1FF,
                         0);

    /* LO Path */
    ThaDeviceMemoryFlush(device,
                         baseAddress + cAf6Reg_FMPM_FM_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_Base,
                         baseAddress + cAf6Reg_FMPM_FM_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_Base + 0x3FFF,
                         0);
    ThaDeviceMemoryFlush(device,
                         baseAddress + cAf6Reg_FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_Base,
                         baseAddress + cAf6Reg_FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_Base + 0x3FFF,
                         0);

    /* DE3 */
    ThaDeviceMemoryFlush(device,
                         baseAddress + cAf6Reg_FMPM_FM_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_Base,
                         baseAddress + cAf6Reg_FMPM_FM_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_Base + 0x1FF,
                         0);
    ThaDeviceMemoryFlush(device,
                         baseAddress + cAf6Reg_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_Base,
                         baseAddress + cAf6Reg_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_Base + 0x1FF,
                         0);

    /* DE1 */
    ThaDeviceMemoryFlush(device,
                         baseAddress + cAf6Reg_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_Base,
                         baseAddress + cAf6Reg_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_Base + 0x3FFF,
                         0);
    ThaDeviceMemoryFlush(device,
                         baseAddress + cAf6Reg_FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_Base,
                         baseAddress + cAf6Reg_FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_Base + 0x3FFF,
                         0);
    }

static uint32 LineOffset(ThaModuleHardSur self, uint8 slice, uint8 lineInSlice)
    {
    AtUnused(self);
    return (uint32)((slice << 5) | lineInSlice);
    }

static uint32 StsOffset(ThaModuleHardSur self, uint8 sliceId, uint8 hwStsInSlice)
    {
    AtUnused(self);
    return (uint32)((sliceId << 5) | hwStsInSlice);
    }

static uint32 VtOffset(ThaModuleHardSur self, uint8 sliceId, uint8 hwStsInSlice, uint8 vtgInSts, uint8 vtInVtg)
    {
    AtUnused(self);
    return (uint32)((sliceId << 10) | (hwStsInSlice << 5) | (vtgInSts << 2) | vtInVtg);
    }

static uint32 De3Offset(ThaModuleHardSur self, uint8 sliceId, uint8 hwStsInSlice)
    {
    return StsOffset(self, sliceId, hwStsInSlice);
    }

static uint32 De1Offset(ThaModuleHardSur self, uint8 sliceId, uint8 hwStsInSlice, uint8 vtgInSts, uint8 vtInVtg)
    {
    return VtOffset(self, sliceId, hwStsInSlice, vtgInSts, vtInVtg);
    }

static eBool ShouldUpdatePmRegisterWhenPotentialUasChange(ThaModuleHardSur self)
    {
    AtUnused(self);
    /* Default always update values from hardware to software PM registers */
    return cAtTrue;
    }

static void OverrideAtObject(AtModuleSur self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModuleSur self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Setup);
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, Debug);
        mMethodOverride(m_AtModuleOverride, CapacityDescription);
        mMethodOverride(m_AtModuleOverride, InterruptManagerCreate);
        mMethodOverride(m_AtModuleOverride, NumInterruptManagers);
        mMethodOverride(m_AtModuleOverride, PeriodicProcess);
        mMethodOverride(m_AtModuleOverride, RecommendedPeriodInMs);
        mMethodOverride(m_AtModuleOverride, InterruptProcess);
        mMethodOverride(m_AtModuleOverride, Activate);
        mMethodOverride(m_AtModuleOverride, Deactivate);
        mMethodOverride(m_AtModuleOverride, IsActive);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtModuleSur(AtModuleSur self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleSurOverride, mMethodsGet(self), sizeof(m_AtModuleSurOverride));

        mMethodOverride(m_AtModuleSurOverride, PeriodExpire);
        mMethodOverride(m_AtModuleSurOverride, PeriodAsyncExpire);
        mMethodOverride(m_AtModuleSurOverride, PeriodElapsedSecondsGet);
        mMethodOverride(m_AtModuleSurOverride, ExpireMethodSet);
        mMethodOverride(m_AtModuleSurOverride, ExpireMethodGet);
        mMethodOverride(m_AtModuleSurOverride, TickSourceSet);
        mMethodOverride(m_AtModuleSurOverride, TickSourceGet);
        mMethodOverride(m_AtModuleSurOverride, ExpireNotificationEnable);
        mMethodOverride(m_AtModuleSurOverride, ExpireNotificationIsEnabled);

        mMethodOverride(m_AtModuleSurOverride, SdhLineEngineCreate);
        mMethodOverride(m_AtModuleSurOverride, SdhHoPathEngineCreate);
        mMethodOverride(m_AtModuleSurOverride, SdhLoPathEngineCreate);
        mMethodOverride(m_AtModuleSurOverride, PdhDe1EngineCreate);
        mMethodOverride(m_AtModuleSurOverride, PdhDe3EngineCreate);
        mMethodOverride(m_AtModuleSurOverride, PwEngineCreate);

        mMethodOverride(m_AtModuleSurOverride, SdhLineEngineDelete);
        mMethodOverride(m_AtModuleSurOverride, SdhHoPathEngineDelete);
        mMethodOverride(m_AtModuleSurOverride, SdhLoPathEngineDelete);
        mMethodOverride(m_AtModuleSurOverride, PdhDe1EngineDelete);
        mMethodOverride(m_AtModuleSurOverride, PdhDe3EngineDelete);
        mMethodOverride(m_AtModuleSurOverride, PwEngineDelete);
        mMethodOverride(m_AtModuleSurOverride, FailureIsSupported);

        mMethodOverride(m_AtModuleSurOverride, CurrentSecondRegisterCreate);
        mMethodOverride(m_AtModuleSurOverride, CurrentPeriodRegisterCreate);
        mMethodOverride(m_AtModuleSurOverride, PreviousPeriodRegisterCreate);
        mMethodOverride(m_AtModuleSurOverride, CurrentDayRegisterCreate);
        mMethodOverride(m_AtModuleSurOverride, PreviousDayRegisterCreate);
        mMethodOverride(m_AtModuleSurOverride, RecentRegisterCreate);

        mMethodOverride(m_AtModuleSurOverride, SdhLineParamCreate);
        mMethodOverride(m_AtModuleSurOverride, SdhPathParamCreate);
        mMethodOverride(m_AtModuleSurOverride, PdhDe3ParamCreate);
        mMethodOverride(m_AtModuleSurOverride, PdhDe1ParamCreate);
        mMethodOverride(m_AtModuleSurOverride, PwParamCreate);

        mMethodOverride(m_AtModuleSurOverride, SesThresholdProfileCreate);
        mMethodOverride(m_AtModuleSurOverride, NumSesThresholdProfiles);
        mMethodOverride(m_AtModuleSurOverride, LineIsSupported);
        mMethodOverride(m_AtModuleSurOverride, NumThresholdProfiles);
        mMethodOverride(m_AtModuleSurOverride, ThresholdProfileCreate);

        /* Timers */
        mMethodOverride(m_AtModuleSurOverride, FailureHoldOffTimerSet);
        mMethodOverride(m_AtModuleSurOverride, FailureHoldOffTimerGet);
        mMethodOverride(m_AtModuleSurOverride, FailureHoldOnTimerSet);
        mMethodOverride(m_AtModuleSurOverride, FailureHoldOnTimerGet);
        }

    mMethodsSet(self, &m_AtModuleSurOverride);
    }

static void Override(AtModuleSur self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    OverrideAtModuleSur(self);
    }

static void MethodsInit(ThaModuleHardSur self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, BaseAddress);
        mMethodOverride(m_methods, IndirectDe1AddressShift);
        mMethodOverride(m_methods, DefaultExpireMethod);
        mMethodOverride(m_methods, DefaultTickSourceGet);

        mMethodOverride(m_methods, SdhLineSurEngineObjectCreate);
        mMethodOverride(m_methods, SdhHoPathEngineObjectCreate);
        mMethodOverride(m_methods, SdhLoPathEngineObjectCreate);
        mMethodOverride(m_methods, PdhDe1EngineObjectCreate);
        mMethodOverride(m_methods, PdhDe3EngineObjectCreate);
        mMethodOverride(m_methods, PwEngineObjectCreate);

        mMethodOverride(m_methods, LineCurrentFailuresAddr);
        mMethodOverride(m_methods, LineFailuresInterruptAddr);
        mMethodOverride(m_methods, LineFailuresInterruptMaskAddr);
        mMethodOverride(m_methods, StsCurrentFailuresAddr);
        mMethodOverride(m_methods, StsFailuresInterruptAddr);
        mMethodOverride(m_methods, StsFailuresInterruptMaskAddr);
        mMethodOverride(m_methods, VtCurrentFailuresAddr);
        mMethodOverride(m_methods, VtFailuresInterruptAddr);
        mMethodOverride(m_methods, VtFailuresInterruptMaskAddr);
        mMethodOverride(m_methods, De3CurrentFailuresAddr);
        mMethodOverride(m_methods, De3FailuresInterruptAddr);
        mMethodOverride(m_methods, De3FailuresInterruptMaskAddr);
        mMethodOverride(m_methods, De1CurrentFailuresAddr);
        mMethodOverride(m_methods, De1FailuresInterruptAddr);
        mMethodOverride(m_methods, De1FailuresInterruptMaskAddr);
        mMethodOverride(m_methods, PwCurrentFailuresAddr);
        mMethodOverride(m_methods, PwFailuresInterruptAddr);
        mMethodOverride(m_methods, PwFailuresInterruptMaskAddr);
        mMethodOverride(m_methods, PwTcaInterruptAddr);
        mMethodOverride(m_methods, PwTcaInterruptMaskAddr);
        mMethodOverride(m_methods, PWFramerInterruptMASKPerPWBase);
        mMethodOverride(m_methods, FmPWInterruptMASKPerTypePerPWBase);
        mMethodOverride(m_methods, LineOffset);

        mMethodOverride(m_methods, IsHoPw);
        mMethodOverride(m_methods, IsLoPw);
        mMethodOverride(m_methods, NeedLatchHoPw);

        mMethodOverride(m_methods, PwCounterLatch);

        mMethodOverride(m_methods, Clock125usDefaultValue);
        mMethodOverride(m_methods, ShouldEnablePwCounters);
        mMethodOverride(m_methods, DdrAccessTimeoutInUs);
        mMethodOverride(m_methods, ChangePageTimeoutInMs);
        mMethodOverride(m_methods, IdOfHdlcPw);
        mMethodOverride(m_methods, HasExpireTimeout);
        mMethodOverride(m_methods, CanEnableHwPmEngine);
        mMethodOverride(m_methods, HasMonitorTimer);
        mMethodOverride(m_methods, FailureForwardingStatusIsSupported);
        mMethodOverride(m_methods, ExpireInterruptIsSupported);
        mMethodOverride(m_methods, HwFlush);
        mMethodOverride(m_methods, Reset);
        mMethodOverride(m_methods, ShouldUpdatePmRegisterWhenPotentialUasChange);

        mBitFieldOverride(ThaModuleHardSur, m_methods, LineTcaType)
        mBitFieldOverride(ThaModuleHardSur, m_methods, StsTcaType)
        mBitFieldOverride(ThaModuleHardSur, m_methods, De3TcaType)
        mBitFieldOverride(ThaModuleHardSur, m_methods, VtTcaType)
        mBitFieldOverride(ThaModuleHardSur, m_methods, De1TcaType)
        mBitFieldOverride(ThaModuleHardSur, m_methods, PwTcaType)
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaModuleHardSur);
    }

AtModuleSur ThaModuleHardSurObjectInit(AtModuleSur self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (AtModuleSurObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit((ThaModuleHardSur)self);
    m_methodsInit = 1;

    /* Private data */
    mInvalidateRequestPage(self);

    return self;
    }

AtModuleSur ThaModuleHardSurNew(AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSur newSurModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newSurModule == NULL)
        return NULL;

    return ThaModuleHardSurObjectInit(newSurModule, device);
    }

eAtRet ThaModuleHardSurDdrRead2Clear(ThaModuleHardSur self, uint32 page, uint8 type, uint32 address, uint32 *value, eBool read2Clear)
    {
    uint8 dword_i;
    uint32 regAddr = ThaModuleHardSurBaseAddress(self) + cAf6Reg_FMPM_Hold00_Data_Of_Read_DDR;
    eAtRet ret;
    tAtOsalCurTime t1, t2, t3;
    uint32 elapsedTime;

    if (self == NULL)
        return cAtErrorNullPointer;

    DdrLock(self);

    AtOsalCurTimeGet(&t1);
    ret = DdrReadDone(self, page, type, address, read2Clear);
    if (ret != cAtOk)
        {
        DdrUnLock(self);
        return ret;
        }

    AtOsalCurTimeGet(&t2);

    for (dword_i = 0; dword_i < cNumDdrHoldRegisters; dword_i++)
        {
        tAtOsalCurTime tm1, tm2;

        AtOsalCurTimeGet(&tm1);
        value[dword_i] = mModuleHwRead(self, regAddr + dword_i);
        AtOsalCurTimeGet(&tm2);

        elapsedTime = AtOsalDifferenceTimeInUs(&tm2, &tm1);
        if (elapsedTime > self->maxHoldRegReadingInUs)
            {
            self->maxHoldRegReadingInUs = elapsedTime;
            self->maxHoldRegAddress = (regAddr + dword_i);
            }
        }

    AtOsalCurTimeGet(&t3);

    elapsedTime = AtOsalDifferenceTimeInUs(&t2, &t1);
    if (self->maxWaitingDdrInUs < elapsedTime)
        self->maxWaitingDdrInUs = elapsedTime;

    elapsedTime = AtOsalDifferenceTimeInUs(&t3, &t2);
    if (elapsedTime > self->maxReadingInUs)
        self->maxReadingInUs = elapsedTime;

    if (self->minReadingInUs > elapsedTime || self->minReadingInUs == 0)
        self->minReadingInUs = elapsedTime;


    DdrUnLock(self);

    return cAtOk;
    }

uint32 ThaModuleHardSurCurrentPage(ThaModuleHardSur self)
    {
    if (self)
        return CurrentPage(self);
    return cBit31_0;
    }

uint32 ThaModuleHardSurPwTxPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    if (self)
        return PwTxPacketsGet(self, pw, clear);
    return 0;
    }

uint32 ThaModuleHardSurPwTxBytesGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    if (self)
        return PwTxBytesGet(self, pw, clear);
    return 0;
    }

uint32 ThaModuleHardSurPwTxLbitPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    if (self)
        return PwTxLbitPacketsGet(self, pw, clear);

    return 0;
    }

uint32 ThaModuleHardSurPwTxRbitPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    if (self)
        return PwTxRbitPacketsGet(self, pw, clear);

    return 0;
    }

uint32 ThaModuleHardSurPwTxMbitPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    if (self)
        return PwTxMbitPacketsGet(self, pw, clear);

    return 0;
    }

uint32 ThaModuleHardSurPwTxNbitPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    if (self)
        return PwTxNbitPacketsGet(self, pw, clear);

    return 0;
    }

uint32 ThaModuleHardSurPwTxPbitPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    if (self)
        return PwTxPbitPacketsGet(self, pw, clear);

    return 0;
    }

uint32 ThaModuleHardSurPwRxPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    if (self)
        return PwRxPacketsGet(self, pw, clear);
    return 0;
    }

uint32 ThaModuleHardSurPwRxMalformedPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    if (self)
        return PwRxMalformedPacketsGet(self, pw, clear);
    return 0;
    }

uint32 ThaModuleHardSurPwRxStrayPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    if (self)
        return PwRxStrayPacketsGet(self, pw, clear);
    return 0;
    }

uint32 ThaModuleHardSurPwRxOamPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    if (self)
        return PwRxOamPacketsGet(self, pw, clear);
    return 0;
    }

uint32 ThaModuleHardSurPwRxLbitPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    if (self)
        return PwRxLbitPacketsGet(self, pw, clear);
    return 0;
    }

uint32 ThaModuleHardSurPwRxRbitPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    if (self)
        return PwRxRbitPacketsGet(self, pw, clear);
    return 0;
    }

uint32 ThaModuleHardSurPwRxMbitPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    if (self)
        return PwRxMbitPacketsGet(self, pw, clear);
    return 0;
    }

uint32 ThaModuleHardSurPwRxNbitPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    if (self)
        return PwRxNbitPacketsGet(self, pw, clear);
    return 0;
    }

uint32 ThaModuleHardSurPwRxPbitPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    if (self)
        return PwRxPbitPacketsGet(self, pw, clear);
    return 0;
    }

uint32 ThaModuleHardSurPwRxBytesGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    if (self)
        return PwRxBytesGet(self, pw, clear);

    return 0;
    }

uint32 ThaModuleHardSurPwRxReorderedPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    if (self)
        return PwRxReorderedPacketsGet(self, pw, clear);

    return 0;
    }

uint32 ThaModuleHardSurPwRxLostPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    if (self)
        return PwRxLostPacketsGet(self, pw, clear);

    return 0;
    }

uint32 ThaModuleHardSurPwRxOutOfSeqDropedPacketsGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    if (self)
        return PwRxOutOfSeqDropedPacketsGet(self, pw, clear);

    return 0;
    }

uint32 ThaModuleHardSurPwRxJitBufOverrunGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    if (self)
        return PwRxJitBufOverrunGet(self, pw, clear);

    return 0;
    }

uint32 ThaModuleHardSurPwRxJitBufUnderrunGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    if (self)
        return PwRxJitBufUnderrunGet(self, pw, clear);

    return 0;
    }

uint32 ThaModuleHardSurPwRxLopsGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    if (self)
        return PwRxLopsGet(self, pw, clear);

    return 0;
    }

uint32 ThaModuleHardSurPwRxPacketsSentToTdmGet(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    if (self)
        return PwRxPacketsSentToTdmGet(self, pw, clear);

    return 0;
    }

uint32 ThaModuleHardSurBaseAddress(ThaModuleHardSur self)
    {
    if (self)
        return mMethodsGet(self)->BaseAddress(self);
    return 0x0;
    }

uint32 ThaModuleHardSurLineCurrentFailuresAddr(ThaModuleHardSur self)
    {
    if (self)
        return mMethodsGet(self)->LineCurrentFailuresAddr(self);
    return 0x0;
    }

uint32 ThaModuleHardSurLineFailuresInterruptAddr(ThaModuleHardSur self)
    {
    if (self)
        return mMethodsGet(self)->LineFailuresInterruptAddr(self);
    return 0x0;
    }

uint32 ThaModuleHardSurLineFailuresInterruptMaskAddr(ThaModuleHardSur self)
    {
    if (self)
        return mMethodsGet(self)->LineFailuresInterruptMaskAddr(self);
    return 0x0;
    }

uint32 ThaModuleHardSurLineTcaInterruptAddr(ThaModuleHardSur self)
    {
    if (self)
        return LineTcaInterruptAddr(self);
    return 0x0;
    }

uint32 ThaModuleHardSurLineTcaInterruptMaskAddr(ThaModuleHardSur self)
    {
    if (self)
        return LineTcaInterruptMaskAddr(self);
    return 0x0;
    }

uint32 ThaModuleHardSurStsCurrentFailuresAddr(ThaModuleHardSur self)
    {
    if (self)
        return mMethodsGet(self)->StsCurrentFailuresAddr(self);
    return 0x0;
    }

uint32 ThaModuleHardSurStsFailuresInterruptAddr(ThaModuleHardSur self)
    {
    if (self)
        return mMethodsGet(self)->StsFailuresInterruptAddr(self);
    return 0x0;
    }

uint32 ThaModuleHardSurStsFailuresInterruptMaskAddr(ThaModuleHardSur self)
    {
    if (self)
        return mMethodsGet(self)->StsFailuresInterruptMaskAddr(self);
    return 0x0;
    }

uint32 ThaModuleHardSurStsTcaInterruptAddr(ThaModuleHardSur self)
    {
    if (self)
        return StsTcaInterruptAddr(self);
    return 0x0;
    }

uint32 ThaModuleHardSurStsTcaInterruptMaskAddr(ThaModuleHardSur self)
    {
    if (self)
        return StsTcaInterruptMaskAddr(self);
    return 0x0;
    }

uint32 ThaModuleHardSurVtCurrentFailuresAddr(ThaModuleHardSur self)
    {
    if (self)
        return mMethodsGet(self)->VtCurrentFailuresAddr(self);
    return 0x0;
    }

uint32 ThaModuleHardSurVtFailuresInterruptAddr(ThaModuleHardSur self)
    {
    if (self)
        return mMethodsGet(self)->VtFailuresInterruptAddr(self);
    return 0x0;
    }

uint32 ThaModuleHardSurVtFailuresInterruptMaskAddr(ThaModuleHardSur self)
    {
    if (self)
        return mMethodsGet(self)->VtFailuresInterruptMaskAddr(self);
    return 0x0;
    }

uint32 ThaModuleHardSurVtTcaInterruptAddr(ThaModuleHardSur self)
    {
    if (self)
        return VtTcaInterruptAddr(self);
    return 0x0;
    }

uint32 ThaModuleHardSurVtTcaInterruptMaskAddr(ThaModuleHardSur self)
    {
    if (self)
        return VtTcaInterruptMaskAddr(self);
    return 0x0;
    }

uint32 ThaModuleHardSurDe3CurrentFailuresAddr(ThaModuleHardSur self)
    {
    if (self)
        return mMethodsGet(self)->De3CurrentFailuresAddr(self);
    return 0x0;
    }

uint32 ThaModuleHardSurDe3FailuresInterruptAddr(ThaModuleHardSur self)
    {
    if (self)
        return mMethodsGet(self)->De3FailuresInterruptAddr(self);
    return 0x0;
    }

uint32 ThaModuleHardSurDe3FailuresInterruptMaskAddr(ThaModuleHardSur self)
    {
    if (self)
        return mMethodsGet(self)->De3FailuresInterruptMaskAddr(self);
    return 0x0;
    }

uint32 ThaModuleHardSurDe3TcaInterruptAddr(ThaModuleHardSur self)
    {
    if (self)
        return De3TcaInterruptAddr(self);
    return 0x0;
    }

uint32 ThaModuleHardSurDe3TcaInterruptMaskAddr(ThaModuleHardSur self)
    {
    if (self)
        return De3TcaInterruptMaskAddr(self);
    return 0x0;
    }

uint32 ThaModuleHardSurDe1CurrentFailuresAddr(ThaModuleHardSur self)
    {
    if (self)
        return mMethodsGet(self)->De1CurrentFailuresAddr(self);
    return 0x0;
    }

uint32 ThaModuleHardSurDe1FailuresInterruptAddr(ThaModuleHardSur self)
    {
    if (self)
        return mMethodsGet(self)->De1FailuresInterruptAddr(self);
    return 0x0;
    }

uint32 ThaModuleHardSurDe1FailuresInterruptMaskAddr(ThaModuleHardSur self)
    {
    if (self)
        return mMethodsGet(self)->De1FailuresInterruptMaskAddr(self);
    return 0x0;
    }

uint32 ThaModuleHardSurDe1TcaInterruptAddr(ThaModuleHardSur self)
    {
    if (self)
        return De1TcaInterruptAddr(self);
    return 0x0;
    }

uint32 ThaModuleHardSurDe1TcaInterruptMaskAddr(ThaModuleHardSur self)
    {
    if (self)
        return De1TcaInterruptMaskAddr(self);
    return 0x0;
    }

uint32 ThaModuleHardSurPwCurrentFailuresAddr(ThaModuleHardSur self)
    {
    if (self)
        return mMethodsGet(self)->PwCurrentFailuresAddr(self);
    return 0x0;
    }

uint32 ThaModuleHardSurPwFailuresInterruptAddr(ThaModuleHardSur self)
    {
    if (self)
        return mMethodsGet(self)->PwFailuresInterruptAddr(self);
    return 0x0;
    }

uint32 ThaModuleHardSurPwFailuresInterruptMaskAddr(ThaModuleHardSur self)
    {
    if (self)
        return mMethodsGet(self)->PwFailuresInterruptMaskAddr(self);
    return 0x0;
    }

uint32 ThaModuleHardSurPwTcaInterruptAddr(ThaModuleHardSur self)
    {
    if (self)
        return mMethodsGet(self)->PwTcaInterruptAddr(self);
    return 0x0;
    }

uint32 ThaModuleHardSurPwTcaInterruptMaskAddr(ThaModuleHardSur self)
    {
    if (self)
        return mMethodsGet(self)->PwTcaInterruptMaskAddr(self);
    return 0x0;
    }

uint32 ThaModuleHardSurLineThresholdRegister(ThaModuleHardSur self)
    {
    if (self)
        return LineThresholdRegister(self);
    return 0x0;
    }

uint32 ThaModuleHardSurStsThresholdRegister(ThaModuleHardSur self)
    {
    if (self)
        return StsThresholdRegister(self);
    return 0x0;
    }

uint32 ThaModuleHardSurVtThresholdRegister(ThaModuleHardSur self)
    {
    if (self)
        return VtThresholdRegister(self);

    return 0x0;
    }

uint32 ThaModuleHardSurDe3ThresholdRegister(ThaModuleHardSur self)
    {
    if (self)
        return De3ThresholdRegister(self);
    return 0x0;
    }

uint32 ThaModuleHardSurDe1ThresholdRegister(ThaModuleHardSur self)
    {
    if (self)
        return De1ThresholdRegister(self);
    return 0x0;
    }

uint32 ThaModuleHardSurPwThresholdRegister(ThaModuleHardSur self)
    {
    if (self)
        return PwThresholdRegister(self);
    return 0x0;
    }

uint32 ThaModuleHardSurPWFramerInterruptMASKPerPWBase(ThaModuleHardSur self)
    {
    if (self)
        return mMethodsGet(self)->PWFramerInterruptMASKPerPWBase(self);
    return cInvalidUint32;
    }

uint32 ThaModuleHardSurFmPWInterruptMASKPerTypePerPWBase(ThaModuleHardSur self)
    {
    if (self)
        return mMethodsGet(self)->FmPWInterruptMASKPerTypePerPWBase(self);
    return cInvalidUint32;
    }

uint32 ThaModuleHardSurLineDdrCountersAddress(AtSurEngine engine)
    {
    return AtSurEngineDefaultOffset(engine) << 5;
    }

uint32 ThaModuleHardSurPathDdrCountersAddress(AtSurEngine engine)
    {
    uint32 shift = (ThaModuleSurSdhPathHwChannelType(engine) == cThaModuleSurCounterStsParams) ? 5 : 0;
    return AtSurEngineDefaultOffset(engine) << shift;
    }

uint32 ThaModuleHardSurDe3DdrCountersAddress(AtSurEngine engine)
    {
    return AtSurEngineDefaultOffset(engine) << 5;
    }

uint32 ThaModuleHardSurDe1DdrCountersAddress(AtSurEngine engine)
    {
    ThaModuleHardSur module = SurModule(engine);
    uint32 shift = mMethodsGet(module)->IndirectDe1AddressShift(module);
    return AtSurEngineDefaultOffset(engine) << shift;
    }

uint32 ThaModuleHardSurPwDdrCountersAddress(AtSurEngine engine)
    {
    return AtSurEngineDefaultOffset(engine);
    }

eAtRet ThaModuleHardSurDe1DdrCountersR2c(AtSurEngine engine, uint8 period, uint32 *regVal, eBool r2c)
    {
    uint32 address = ThaModuleHardSurDe1DdrCountersAddress(engine);
    if (period == cAtPmCurrentPeriodRegister)
        return DdrCountersR2c(engine, cThaModuleSurCounterDe1Params, address, regVal, r2c);
    if (period == cAtPmPreviousPeriodRegister)
        return DdrHistoryCountersR2c(engine, cThaModuleSurCounterDe1Params, address, regVal, r2c);
    return cAtErrorModeNotSupport;
    }

eAtRet ThaModuleHardSurDe3DdrCountersR2c(AtSurEngine engine, uint8 period, uint32 *regVal, eBool r2c)
    {
    uint32 address = ThaModuleHardSurDe3DdrCountersAddress(engine);
    if (period == cAtPmCurrentPeriodRegister)
        return DdrCountersR2c(engine, cThaModuleSurCounterDe3Params, address, regVal, r2c);
    if (period == cAtPmPreviousPeriodRegister)
        return DdrHistoryCountersR2c(engine, cThaModuleSurCounterDe3Params, address, regVal, r2c);
    return cAtErrorModeNotSupport;
    }

eAtRet ThaModuleHardSurPathDdrCountersR2c(AtSurEngine engine, uint8 period, uint32 *regVal, eBool r2c)
    {
    uint32 address = ThaModuleHardSurPathDdrCountersAddress(engine);
    if (period == cAtPmCurrentPeriodRegister)
        return DdrCountersR2c(engine, ThaModuleSurSdhPathHwChannelType(engine), address, regVal, r2c);
    if (period == cAtPmPreviousPeriodRegister)
        return DdrHistoryCountersR2c(engine, ThaModuleSurSdhPathHwChannelType(engine), address, regVal, r2c);
    return cAtErrorModeNotSupport;
    }

eAtRet ThaModuleHardSurLineDdrCountersR2c(AtSurEngine engine, uint8 period, uint32 *regVal, eBool r2c)
    {
    uint32 address = ThaModuleHardSurLineDdrCountersAddress(engine);
    if (period == cAtPmCurrentPeriodRegister)
        return DdrCountersR2c(engine, cThaModuleSurCounterLineParams, address, regVal, r2c);
    if (period == cAtPmPreviousPeriodRegister)
        return DdrHistoryCountersR2c(engine, cThaModuleSurCounterLineParams, address, regVal, r2c);
    return cAtErrorModeNotSupport;
    }

eAtRet ThaModuleHardSurPwDdrCountersR2c(AtSurEngine engine, uint8 period, uint32 *regVal, eBool r2c)
    {
    uint32 address = ThaModuleHardSurPwDdrCountersAddress(engine);
    if (period == cAtPmCurrentPeriodRegister)
        return DdrCountersR2c(engine, cThaModuleSurCounterPwParams, address, regVal, r2c);
    if (period == cAtPmPreviousPeriodRegister)
        return DdrHistoryCountersR2c(engine, cThaModuleSurCounterPwParams, address, regVal, r2c);
    return cAtErrorModeNotSupport;
    }

uint32 ThaModuleHardSurNumberDdrHoldRegisters(ThaModuleHardSur self)
    {
    AtUnused(self);
    return cNumDdrHoldRegisters;
    }

eAtRet ThaModuleHardSurPwCounterLatch(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->PwCounterLatch(self, pw, clear);
    return cAtErrorNullPointer;
    }

uint32 ThaModuleHardSurVersionNumber(ThaModuleHardSur self)
    {
    if (self)
        return VersionNumber(self);
    return 0;
    }

eBool ThaModuleHardSurVersionIsValid(ThaModuleHardSur self)
    {
    uint32 version = ThaModuleHardSurVersionNumber(self);
    return (version > 0) ? cAtTrue : cAtFalse;
    }

uint8 ThaModuleSurSdhPathHwChannelType(AtSurEngine engine)
    {
    AtSdhChannel vc = (AtSdhChannel)AtSurEngineChannelGet(engine);
    uint32 channelType = AtSdhChannelTypeGet(vc);

    switch (channelType)
        {
        case cAtSdhChannelTypeVc12:
        case cAtSdhChannelTypeVc11:
            return cThaModuleSurCounterVtParams;

        case cAtSdhChannelTypeVc4_64c:
        case cAtSdhChannelTypeVc4_16c:
        case cAtSdhChannelTypeVc4_4c:
        case cAtSdhChannelTypeVc4:
        case cAtSdhChannelTypeVc4_nc:
            return cThaModuleSurCounterStsParams;

        case cAtSdhChannelTypeVc3:
            if (AtSdhChannelTypeGet(AtSdhChannelParentChannelGet(vc)) == cAtSdhChannelTypeAu3)
                return cThaModuleSurCounterStsParams;
            else
                return cThaModuleSurCounterVtParams;
        default:
            return cBit7_0;
        }
    }

eBool ThaModuleHardSurIsOperating(ThaModuleHardSur self)
    {
    uint32 regVal;

    if (self == NULL)
        return cAtFalse;

    regVal = mModuleHwRead(self, ResetRegister(self));
    return (regVal & cAf6_FMPM_Force_Reset_Core_drst_Mask) ? cAtTrue : cAtFalse;
    }

uint32 ThaModuleHardSurHdlcChannelTxGoodPacketsGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    return HdlcChannelTxGoodPacketsGet(self, channel, r2c);
    }

uint32 ThaModuleHardSurHdlcChannelTxGoodBytesGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    return HdlcChannelTxGoodBytesGet(self, channel, r2c);
    }

uint32 ThaModuleHardSurHdlcChannelTxFragmentsGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    return HdlcChannelTxFragmentsGet(self, channel, r2c);
    }

uint32 ThaModuleHardSurHdlcChannelTxPlainGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    return HdlcChannelTxPlainGet(self, channel, r2c);
    }

uint32 ThaModuleHardSurHdlcChannelTxOamGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    return HdlcChannelTxOamGet(self, channel, r2c);
    }

uint32 ThaModuleHardSurHdlcChannelTxIdleBytesGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    return HdlcChannelTxIdleBytesGet(self, channel, r2c);
    }

uint32 ThaModuleHardSurHdlcChannelTxAbortsGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    return HdlcChannelTxAbortsGet(self, channel, r2c);
    }

uint32 ThaModuleHardSurHdlcChannelRxGoodPacketsGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    return HdlcChannelRxGoodPacketsGet(self, channel, r2c);
    }

uint32 ThaModuleHardSurHdlcChannelRxBytesGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    return HdlcChannelRxBytesGet(self, channel, r2c);
    }

uint32 ThaModuleHardSurHdlcChannelRxGoodBytesGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    return HdlcChannelRxGoodBytesGet(self, channel, r2c);
    }

uint32 ThaModuleHardSurHdlcChannelRxFragmentsGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    return HdlcChannelRxFragmentsGet(self, channel, r2c);
    }

uint32 ThaModuleHardSurHdlcChannelRxPlainGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    return HdlcChannelRxPlainGet(self, channel, r2c);
    }

uint32 ThaModuleHardSurHdlcChannelRxOamGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    return HdlcChannelRxOamGet(self, channel, r2c);
    }

uint32 ThaModuleHardSurHdlcChannelRxAbortsGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    return HdlcChannelRxAbortsGet(self, channel, r2c);
    }

uint32 ThaModuleHardSurHdlcChannelRxIdleBytesGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    return HdlcChannelRxIdleBytesGet(self, channel, r2c);
    }

uint32 ThaModuleHardSurHdlcChannelRxMRUPacketsGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    return HdlcChannelRxMRUPacketsGet(self, channel, r2c);
    }

uint32 ThaModuleHardSurHdlcChannelRxFcsErrorGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    return HdlcChannelRxFcsErrorGet(self, channel, r2c);
    }

uint32 ThaModuleHardSurHdlcChannelRxDiscardFramesGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    return HdlcChannelRxDiscardFramesGet(self, channel, r2c);
    }

eAtRet ThaModuleHardSurHdlcChannelCounterLatch(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    if (self)
        return HdlcChannelCounterLatch(self, channel, r2c);

    return cAtErrorNullPointer;
    }

static uint32 PppLinkTxCountersGet(ThaModuleHardSur self, AtChannel channel, uint32 counterType, eBool r2c)
    {
    uint32 address;
    AtUnused(r2c);

    if (mIsNullPointer(self) || mIsNullPointer(channel))
        return 0;

    address = cAf6_FMPM_MPEG_Counter_Base + ThaModuleHardSurBaseAddress(self) + (AtChannelHwIdGet(channel) << 4) + counterType;
    return mModuleHwRead(self, address);
    }

static uint32 PppLinkRxCountersGet(ThaModuleHardSur self, AtChannel channel, uint32 counterType, eBool r2c)
    {
    uint32 address;
    AtUnused(r2c);

    if (mIsNullPointer(self) || mIsNullPointer(channel))
        return 0;

    address = cAf6_FMPM_MPIG_Counter_Base + ThaModuleHardSurBaseAddress(self) + (AtChannelHwIdGet(channel) << 4) + counterType;
    return mModuleHwRead(self, address);
    }

static uint32 MpBundleRxCounterGet(ThaModuleHardSur self, AtChannel channel, uint32 counterType, eBool r2c)
    {
    uint32 address;
    AtUnused(r2c);

    if (mIsNullPointer(self) || mIsNullPointer(channel))
        return 0;

    address = cAf6_FMPM_RX_Bundle_Counter_Base + ThaModuleHardSurBaseAddress(self) + (AtChannelIdGet(channel) << 2) + counterType;
    return mModuleHwRead(self, address);
    }

static uint32 MpBundleTxCounterGet(ThaModuleHardSur self, AtChannel channel, uint32 counterType, eBool r2c)
    {
    uint32 address;
    AtUnused(r2c);

    if (mIsNullPointer(self) || mIsNullPointer(channel))
        return 0;

    address = cAf6_FMPM_TX_Bundle_Counter_Base + ThaModuleHardSurBaseAddress(self) + (AtChannelIdGet(channel) << 2) + counterType;
    return mModuleHwRead(self, address);
    }

static uint32 MpigCounterGet(ThaModuleHardSur self, uint32 chnId, uint32 counterType)
    {
    uint32 address = cAf6_FMPM_MPIG_Counter_Base + ThaModuleHardSurBaseAddress(self) + chnId * 0x10 + counterType;
    return mModuleHwRead(self, address);
    }

static uint32 MpegCounterGet(ThaModuleHardSur self, uint32 chnId, uint32 counterType)
    {
    uint32 address = cAf6_FMPM_MPEG_Counter_Base + ThaModuleHardSurBaseAddress(self) + chnId * 0x10 + counterType;
    return mModuleHwRead(self, address);
    }

uint32 ThaModuleHardSurPppLinkTxByteGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    if (self)
        return PppLinkTxCountersGet(self, channel, cAf6_FMPM_PPP_CounterType_TotalByte, r2c);
    return 0;
    }

uint32 ThaModuleHardSurPppLinkTxFragmentGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    if (self)
        return PppLinkTxCountersGet(self, channel, cAf6_FMPM_PPP_CounterType_Fragment, r2c);
    return 0;
    }

uint32 ThaModuleHardSurPppLinkTxGoodPktGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    if (self)
        return PppLinkTxCountersGet(self, channel, cAf6_FMPM_PPP_CounterType_GoodPacket, r2c);
    return 0;
    }

uint32 ThaModuleHardSurPppLinkTxDiscardPktGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    if (self)
        return PppLinkTxCountersGet(self, channel, cAf6_FMPM_PPP_CounterType_DiscardPacket, r2c);
    return 0;
    }

uint32 ThaModuleHardSurPppLinkTxDiscardByteGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    if (self)
        return PppLinkTxCountersGet(self, channel, cAf6_FMPM_PPP_CounterType_DiscardByte, r2c);
    return 0;
    }

uint32 ThaModuleHardSurPppLinkRxFragmentGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    return PppLinkRxCountersGet(self, channel, cAf6_FMPM_PPP_CounterType_Fragment, r2c);
    }

uint32 ThaModuleHardSurPppLinkRxByteGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    if (self)
        return PppLinkRxCountersGet(self, channel, cAf6_FMPM_PPP_CounterType_TotalByte, r2c);
    return 0;
    }

uint32 ThaModuleHardSurPppLinkRxGoodPktGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    if (self)
        return PppLinkRxCountersGet(self, channel, cAf6_FMPM_PPP_CounterType_GoodPacket, r2c);
    return 0;
    }

uint32 ThaModuleHardSurPppLinkRxDiscardPktGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    if (self)
        return PppLinkRxCountersGet(self, channel, cAf6_FMPM_PPP_CounterType_DiscardPacket, r2c);
    return 0;
    }

uint32 ThaModuleHardSurPppLinkRxDiscardByteGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    if (self)
        return PppLinkRxCountersGet(self, channel, cAf6_FMPM_PPP_CounterType_DiscardByte, r2c);
    return 0;
    }

uint32 ThaModuleHardSurMpBundleRxGoodByteGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    if (self)
        return MpBundleRxCounterGet(self, channel, r2c, cAf6_FMPM_Bundle_Counter_Type_GoodByte);
    return 0;
    }

uint32 ThaModuleHardSurMpBundleRxGoodPktGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    if (self)
        return MpBundleRxCounterGet(self, channel, r2c, cAf6_FMPM_Bundle_Counter_Type_GoodPkt);
    return 0;
    }

uint32 ThaModuleHardSurMpBundleRxDiscardPktGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    if (self)
        return MpBundleRxCounterGet(self, channel, r2c, cAf6_FMPM_Bundle_Counter_Type_DiscardPkt);
    return 0;
    }

uint32 ThaModuleHardSurMpBundleRxFragmentPktGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    if (self)
        return MpBundleRxCounterGet(self, channel, r2c, cAf6_FMPM_Bundle_Counter_Type_FragmentPkt);
    return 0;
    }

uint32 ThaModuleHardSurMpBundleTxGoodByteGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    if (self)
        return MpBundleTxCounterGet(self, channel, r2c, cAf6_FMPM_Bundle_Counter_Type_GoodByte);
    return 0;
    }

uint32 ThaModuleHardSurMpBundleTxGoodPktGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    if (self)
        return MpBundleTxCounterGet(self, channel, r2c, cAf6_FMPM_Bundle_Counter_Type_GoodPkt);
    return 0;
    }

uint32 ThaModuleHardSurMpBundleTxDiscardPktGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    if (self)
        return MpBundleTxCounterGet(self, channel, r2c, cAf6_FMPM_Bundle_Counter_Type_DiscardPkt);
    return 0;
    }

uint32 ThaModuleHardSurMpBundleTxFragmentPktGet(ThaModuleHardSur self, AtChannel channel, eBool r2c)
    {
    if (self)
        return MpBundleTxCounterGet(self, channel, r2c, cAf6_FMPM_Bundle_Counter_Type_FragmentPkt);
    return 0;
    }

eBool ThaModuleHardSurCanEnableHwPmEngine(ThaModuleHardSur self)
    {
    if (self)
        return mMethodsGet(self)->CanEnableHwPmEngine(self);
    return cAtFalse;
    }

uint32 ThaModuleHardSurHdlcPwRxTotalByteGet(ThaModuleHardSur self, AtPw pw)
    {
    if (self)
        {
        uint32 chnId = mMethodsGet(self)->IdOfHdlcPw(self, pw);
        return MpegCounterGet(self, chnId, cAf6_FMPM_MPIG_MPEG_CounterType_TotalByte);
        }

    return 0;
    }

uint32 ThaModuleHardSurHdlcPwRxGoodPacketGet(ThaModuleHardSur self, AtPw pw)
    {
    if (self)
        {
        uint32 chnId = mMethodsGet(self)->IdOfHdlcPw(self, pw);
        return MpegCounterGet(self, chnId, cAf6_FMPM_MPIG_MPEG_CounterType_GoodPacket);
        }

    return 0;
    }

uint32 ThaModuleHardSurHdlcPwRxDiscardPktGet(ThaModuleHardSur self, AtPw pw)
    {
    if (self)
        {
        uint32 chnId = mMethodsGet(self)->IdOfHdlcPw(self, pw);
        return MpegCounterGet(self, chnId, cAf6_FMPM_MPIG_MPEG_CounterType_DiscardPkt);
        }

    return 0;
    }

uint32 ThaModuleHardSurHdlcPwTxTotalByteGet(ThaModuleHardSur self, AtPw pw)
    {
    if (self)
        {
        uint32 chnId = mMethodsGet(self)->IdOfHdlcPw(self, pw);
        return MpigCounterGet(self, chnId, cAf6_FMPM_MPIG_MPEG_CounterType_TotalByte);
        }

    return 0;
    }

uint32 ThaModuleHardSurHdlcPwTxGoodPacketGet(ThaModuleHardSur self, AtPw pw)
    {
    if (self)
        {
        uint32 chnId = mMethodsGet(self)->IdOfHdlcPw(self, pw);
        return MpigCounterGet(self, chnId, cAf6_FMPM_MPIG_MPEG_CounterType_GoodPacket);
        }

    return 0;
    }

eAtRet ThaModuleHardSurHdlcPwCounterLatch(ThaModuleHardSur self, AtPw pw, eBool clear)
    {
    AtUnused(pw);
    return HoCounterLatch(self, clear);
    }

eBool ThaModuleHardSurFailureForwardingStatusIsSupported(ThaModuleHardSur self)
    {
    if (self)
        return mMethodsGet(self)->FailureForwardingStatusIsSupported(self);
    return cAtFalse;
    }

eAtRet ThaModuleHardSurEthFlowCounterLatch(ThaModuleHardSur self, AtEthFlow flow, eBool clear)
    {
    AtUnused(flow);
    return HoCounterLatch(self, clear);
    }

uint32 ThaModuleHardSurEthFlowRxTotalByteGet(ThaModuleHardSur self, AtEthFlow flow)
    {
    if (self)
        {
        uint32 channelId = ThaEthFlowHwFlowIdGet((ThaEthFlow)flow);
        return MpegCounterGet(self, channelId, cAf6_FMPM_MPIG_MPEG_CounterType_TotalByte);
        }

    return 0;
    }

uint32 ThaModuleHardSurEthFlowRxGoodPacketGet(ThaModuleHardSur self, AtEthFlow flow)
    {
    if (self)
        {
        uint32 channelId = ThaEthFlowHwFlowIdGet((ThaEthFlow)flow);
        return MpegCounterGet(self, channelId, cAf6_FMPM_MPIG_MPEG_CounterType_GoodPacket);
        }

    return 0;
    }

uint32 ThaModuleHardSurEthFlowRxDiscardPktGet(ThaModuleHardSur self, AtEthFlow flow)
    {
    if (self)
        {
        uint32 channelId = ThaEthFlowHwFlowIdGet((ThaEthFlow)flow);
        return MpegCounterGet(self, channelId, cAf6_FMPM_MPIG_MPEG_CounterType_DiscardPkt);
        }

    return 0;
    }

uint32 ThaModuleHardSurEthFlowTxTotalByteGet(ThaModuleHardSur self, AtEthFlow flow)
    {
    if (self)
        {
        uint32 channelId = ThaEthFlowHwFlowIdGet((ThaEthFlow)flow);
        return MpigCounterGet(self, channelId, cAf6_FMPM_MPIG_MPEG_CounterType_TotalByte);
        }

    return 0;
    }

uint32 ThaModuleHardSurEthFlowTxGoodPacketGet(ThaModuleHardSur self, AtEthFlow flow)
    {
    if (self)
        {
        uint32 channelId = ThaEthFlowHwFlowIdGet((ThaEthFlow)flow);
        return MpigCounterGet(self, channelId, cAf6_FMPM_MPIG_MPEG_CounterType_GoodPacket);
        }

    return 0;
    }

eBool ThaModuleHardSurFailureIsSupported(ThaModuleHardSur self)
    {
    return AtModuleSurFailureIsSupported((AtModuleSur)self);
    }

void ThaModuleHardSurHwFlush(ThaModuleHardSur self)
    {
    if (self)
        mMethodsGet(self)->HwFlush(self);
    }

uint32 ThaModuleHardSurLineOffset(ThaModuleHardSur self, uint8 slice, uint8 lineInSlice)
    {
    if (self)
        return mMethodsGet(self)->LineOffset(self, slice, lineInSlice);
    return 0x0;
    }

uint32 ThaModuleHardSurStsOffset(ThaModuleHardSur self, uint8 slice, uint8 sts)
    {
    if (self)
        return StsOffset(self, slice, sts);
    return 0x0;
    }

uint32 ThaModuleHardSurVtOffset(ThaModuleHardSur self, uint8 slice, uint8 sts, uint8 vtg, uint8 vt)
    {
    if (self)
        return VtOffset(self, slice, sts, vtg, vt);
    return 0x0;
    }

uint32 ThaModuleHardSurDe3Offset(ThaModuleHardSur self, uint8 slice, uint8 sts)
    {
    if (self)
        return De3Offset(self, slice, sts);
    return 0x0;
    }

uint32 ThaModuleHardSurDe1Offset(ThaModuleHardSur self, uint8 slice, uint8 sts, uint8 vtg, uint8 vt)
    {
    if (self)
        return De1Offset(self, slice, sts, vtg, vt);
    return 0x0;
    }

eAtRet ThaModuleHarSurResetForDdrOffLoad(ThaModuleHardSur self)
    {
    return ResetForDdrOffLoad(self);
    }

eBool ThaModuleHardSurShouldUpdatePmRegisterWhenPotentialUasChange(ThaModuleHardSur self)
    {
    if (self)
        return mMethodsGet(self)->ShouldUpdatePmRegisterWhenPotentialUasChange(self);
    return cAtFalse;
    }

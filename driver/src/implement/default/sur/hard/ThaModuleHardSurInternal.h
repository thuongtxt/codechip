/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Surveillance
 * 
 * File        : ThaModuleSoftSurInternal.h
 * 
 * Created Date: Mar 19, 2015
 *
 * Description : Module hardware Surveillance internal header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEHARDSURINTERNAL_H_
#define _THAMODULEHARDSURINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../util/ThaUtil.h"
#include "../../../../generic/sur/AtModuleSurInternal.h"
#include "ThaModuleHardSur.h"
#include "AtList.h"

/*--------------------------- Define -----------------------------------------*/
#define cNumDdrHoldRegisters 25

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleHardSurMethods
    {
    uint32 (*BaseAddress)(ThaModuleHardSur self);
    uint32 (*IndirectDe1AddressShift)(ThaModuleHardSur self);
    eBool (*ShouldEnablePwCounters)(ThaModuleHardSur self);
    uint32 (*ChangePageTimeoutInMs)(ThaModuleHardSur self);
    uint32 (*DdrAccessTimeoutInUs)(ThaModuleHardSur self);
    eBool (*HasExpireTimeout)(ThaModuleHardSur self);
    eAtSurPeriodExpireMethod (*DefaultExpireMethod)(ThaModuleHardSur self);
    eAtSurTickSource (*DefaultTickSourceGet)(ThaModuleHardSur self);

    /* Factory */
    AtSurEngine (*SdhLineSurEngineObjectCreate)(ThaModuleHardSur self, AtChannel line);
    AtSurEngine (*SdhHoPathEngineObjectCreate)(ThaModuleHardSur self, AtChannel path);
    AtSurEngine (*SdhLoPathEngineObjectCreate)(ThaModuleHardSur self, AtChannel path);
    AtSurEngine (*PdhDe1EngineObjectCreate)(ThaModuleHardSur self, AtChannel de1);
    AtSurEngine (*PdhDe3EngineObjectCreate)(ThaModuleHardSur self, AtChannel de3);
    AtSurEngine (*PwEngineObjectCreate)(ThaModuleHardSur self, AtChannel pw);

    /* OC/STM addresses. */
    uint32 (*LineCurrentFailuresAddr)(ThaModuleHardSur self);
    uint32 (*LineFailuresInterruptAddr)(ThaModuleHardSur self);
    uint32 (*LineFailuresInterruptMaskAddr)(ThaModuleHardSur self);
    uint32 (*LineOffset)(ThaModuleHardSur self, uint8 slice, uint8 lineInSlice);

    /* STS/AU addresses. */
    uint32 (*StsCurrentFailuresAddr)(ThaModuleHardSur self);
    uint32 (*StsFailuresInterruptAddr)(ThaModuleHardSur self);
    uint32 (*StsFailuresInterruptMaskAddr)(ThaModuleHardSur self);

    /* VT/TU addresses. */
    uint32 (*VtCurrentFailuresAddr)(ThaModuleHardSur self);
    uint32 (*VtFailuresInterruptAddr)(ThaModuleHardSur self);
    uint32 (*VtFailuresInterruptMaskAddr)(ThaModuleHardSur self);

    /* DS3/E3 addresses. */
    uint32 (*De3CurrentFailuresAddr)(ThaModuleHardSur self);
    uint32 (*De3FailuresInterruptAddr)(ThaModuleHardSur self);
    uint32 (*De3FailuresInterruptMaskAddr)(ThaModuleHardSur self);

    /* DS1/E1 addresses. */
    uint32 (*De1CurrentFailuresAddr)(ThaModuleHardSur self);
    uint32 (*De1FailuresInterruptAddr)(ThaModuleHardSur self);
    uint32 (*De1FailuresInterruptMaskAddr)(ThaModuleHardSur self);

    /* PW addresses. */
    uint32 (*PwCurrentFailuresAddr)(ThaModuleHardSur self);
    uint32 (*PwFailuresInterruptAddr)(ThaModuleHardSur self);
    uint32 (*PwFailuresInterruptMaskAddr)(ThaModuleHardSur self);
    uint32 (*PwTcaInterruptAddr)(ThaModuleHardSur self);
    uint32 (*PwTcaInterruptMaskAddr)(ThaModuleHardSur self);

    /* Bit mask fields */
    mDefineMaskShiftField(ThaModuleHardSur, LineTcaType)
    mDefineMaskShiftField(ThaModuleHardSur, StsTcaType)
    mDefineMaskShiftField(ThaModuleHardSur, De3TcaType)
    mDefineMaskShiftField(ThaModuleHardSur, VtTcaType)
    mDefineMaskShiftField(ThaModuleHardSur, De1TcaType)
    mDefineMaskShiftField(ThaModuleHardSur, PwTcaType)

    /* Working with PW counters */
    eAtRet (*PwCounterLatch)(ThaModuleHardSur self, AtPw pw, eBool clear);
    eBool (*IsHoPw)(ThaModuleHardSur self, AtPw pw);
    eBool (*IsLoPw)(ThaModuleHardSur self, AtPw pw);
    eBool (*NeedLatchHoPw)(ThaModuleHardSur self);

    /* Supported feature. */
    eBool (*CanEnableHwPmEngine)(ThaModuleHardSur self);
    eBool (*ExpireInterruptIsSupported)(ThaModuleHardSur self);
    eBool (*FailureForwardingStatusIsSupported)(ThaModuleHardSur self);
    eBool (*ShouldUpdatePmRegisterWhenPotentialUasChange)(ThaModuleHardSur self);

    /* Default setting */
    uint32 (*Clock125usDefaultValue)(ThaModuleHardSur self);
    eBool (*HasMonitorTimer)(ThaModuleHardSur self);
    
    uint32 (*IdOfHdlcPw)(ThaModuleHardSur self, AtPw pw);

    /* Registers */
    uint32 (*PWFramerInterruptMASKPerPWBase)(ThaModuleHardSur self);
    uint32 (*FmPWInterruptMASKPerTypePerPWBase)(ThaModuleHardSur self);
    void (*HwFlush)(ThaModuleHardSur self);

    /* Reset */
    eAtRet (*Reset)(ThaModuleHardSur self);
    }tThaModuleHardSurMethods;

typedef struct tThaModuleHardSur
    {
    tAtModuleSur super;
    const tThaModuleHardSurMethods* methods;

    /* Version management */
    uint32 versionNumber;

    /* Lockers */
    AtOsalMutex  ddrMutex;

    /* For none-blocking calls */
    uint32 currentRequestedPage;

    /* For latching LO PW counters */
    uint32 loPwCounters[cNumDdrHoldRegisters];

    /* For latching LO HDLC channel counters */
    uint32 loHdlcChannelCounters[cNumDdrHoldRegisters];

    /* Debugging purpose. */
    uint32 maxChangePageInMs; /* Maximum switching time. */
    uint32 maxReadingInUs;
    uint32 minReadingInUs;
    uint32 maxHoldRegReadingInUs;
    uint32 maxHoldRegAddress; /* The hold address that got maximum reading. */
    uint32 maxWaitingDdrInUs;
    uint32 numLongReadings; /* Number of long readings, more than 400us */
    }tThaModuleHardSur;

/*--------------------------- Forward declarations ---------------------------*/
AtList SdhLineSurEngineList(AtModuleSur self);
AtList PdhDe1SurEngineList(AtModuleSur self);
AtList PdhDe3SurEngineList(AtModuleSur self);
AtList SdhHoPathSurEngineList(AtModuleSur self);
AtList SdhLoPathSurEngineList(AtModuleSur self);
AtList PwSurEngineList(AtModuleSur self);

/*--------------------------- Entries ----------------------------------------*/

#endif /* _THAMODULEHARDSURINTERNAL_H_ */


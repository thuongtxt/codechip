/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      : SUR                                                            
 *                                                                              
 * File        : ThaModuleHardSurReg.h
 *                                                                              
 * Created Date: Aug-26-2015                                                    
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _THMODULEHARDSURREG_H_
#define _THMODULEHARDSURREG_H_

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*------------------------------------------------------------------------------
Reg Name   : FMPM Block Version
Reg Addr   : 0x00000
Reg Formula: 
    Where  : 
Reg Desc   : 
PM Block Version

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Block_Version                                                                     0x00000

/*--------------------------------------
BitField Name: day
BitField Type: R_O
BitField Desc: day, hexdecimal format
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_FMPM_Block_Version_day_Mask                                                             cBit31_24
#define cAf6_FMPM_Block_Version_day_Shift                                                                   24

/*--------------------------------------
BitField Name: month
BitField Type: R_O
BitField Desc: month, hexdecimal format
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_FMPM_Block_Version_month_Mask                                                           cBit23_16
#define cAf6_FMPM_Block_Version_month_Shift                                                                 16

/*--------------------------------------
BitField Name: year
BitField Type: R_O
BitField Desc: year, hexdecimal format
BitField Bits: [15:08]
--------------------------------------*/
#define cAf6_FMPM_Block_Version_year_Mask                                                             cBit15_8
#define cAf6_FMPM_Block_Version_year_Shift                                                                   8

/*--------------------------------------
BitField Name: project
BitField Type: R_O
BitField Desc: Project ID, hexdecimal format
BitField Bits: [07:04]
--------------------------------------*/
#define cAf6_FMPM_Block_Version_project_Mask                                                           cBit7_4
#define cAf6_FMPM_Block_Version_project_Shift                                                                4

/*--------------------------------------
BitField Name: number
BitField Type: R_O
BitField Desc: number of synthesis FPGA each day, hexdecimal format
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_FMPM_Block_Version_number_Mask                                                            cBit3_0
#define cAf6_FMPM_Block_Version_number_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Read DDR Control
Reg Addr   : 0x00001
Reg Formula: 
    Where  : 
Reg Desc   : 
Read Parameters or Statistic counters of DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Read_DDR_Control                                                                  0x00001

/*--------------------------------------
BitField Name: Done
BitField Type: R/W/C
BitField Desc: Access DDR Is Done
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_FMPM_Read_DDR_Control_Done_Mask                                                            cBit31
#define cAf6_FMPM_Read_DDR_Control_Done_Shift                                                               31

/*--------------------------------------
BitField Name: Start
BitField Type: R/W
BitField Desc: Trigger 0->1 to start access DDR
BitField Bits: [30:30]
--------------------------------------*/
#define cAf6_FMPM_Read_DDR_Control_Start_Mask                                                           cBit30
#define cAf6_FMPM_Read_DDR_Control_Start_Shift                                                              30

/*--------------------------------------
BitField Name: Page
BitField Type: R/W
BitField Desc: Page access
BitField Bits: [29:29]
--------------------------------------*/
#define cAf6_FMPM_Read_DDR_Control_Page_Mask                                                            cBit29
#define cAf6_FMPM_Read_DDR_Control_Page_Shift                                                               29

/*--------------------------------------
BitField Name: R2C
BitField Type: R/W
BitField Desc: Read mode
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_FMPM_Read_DDR_Control_R2C_Mask                                                             cBit28
#define cAf6_FMPM_Read_DDR_Control_R2C_Shift                                                                28

/*--------------------------------------
BitField Name: Type
BitField Type: R/W
BitField Desc: Counter Type
BitField Bits: [27:20]
--------------------------------------*/
#define cAf6_FMPM_Read_DDR_Control_Type_Mask                                                         cBit27_20
#define cAf6_FMPM_Read_DDR_Control_Type_Shift                                                               20

/*--------------------------------------
BitField Name: ADR
BitField Type: R/W
BitField Desc: Address of Counter
BitField Bits: [13:00]
--------------------------------------*/
#define cAf6_FMPM_Read_DDR_Control_ADR_Mask                                                           cBit13_0
#define cAf6_FMPM_Read_DDR_Control_ADR_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Change Page DDR
Reg Addr   : 0x00002
Reg Formula: 
    Where  : 
Reg Desc   : 
Change Page for DDR when SW want to get Current Period Parameters

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Change_Page_DDR                                                                   0x00002

/*--------------------------------------
BitField Name: pmtick_cnt
BitField Type: R_O
BitField Desc: Count PM tick_1s of Current page
BitField Bits: [31:20]
--------------------------------------*/
#define cAf6_FMPM_Change_Page_DDR_pmtick_cnt_Mask                                                    cBit31_20
#define cAf6_FMPM_Change_Page_DDR_pmtick_cnt_Shift                                                          20

/*--------------------------------------
BitField Name: CHG_Page_enb
BitField Type: R_O
BitField Desc: Enable change page
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_FMPM_Change_Page_DDR_CHG_Page_enb_Mask                                                      cBit9
#define cAf6_FMPM_Change_Page_DDR_CHG_Page_enb_Shift                                                         9

/*--------------------------------------
BitField Name: HW_Page
BitField Type: R_O
BitField Desc: Get Current Page of DDR
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_FMPM_Change_Page_DDR_HW_Page_Mask                                                           cBit8
#define cAf6_FMPM_Change_Page_DDR_HW_Page_Shift                                                              8

/*--------------------------------------
BitField Name: CHG_DONE
BitField Type: R/W/C
BitField Desc: Change page is done
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_Change_Page_DDR_CHG_DONE_Mask                                                          cBit4
#define cAf6_FMPM_Change_Page_DDR_CHG_DONE_Shift                                                             4

/*--------------------------------------
BitField Name: SEL_CHG_PAGE
BitField Type: R/W
BitField Desc: Select change page mode
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_Change_Page_DDR_SEL_CHG_PAGE_Mask                                                      cBit1
#define cAf6_FMPM_Change_Page_DDR_SEL_CHG_PAGE_Shift                                                         1

/*--------------------------------------
BitField Name: CHG_START
BitField Type: R/W
BitField Desc: Trigger 0->1 to request change page, this bit is available when
bit SEL_CHG_PAGE is 0
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_Change_Page_DDR_CHG_START_Mask                                                         cBit0
#define cAf6_FMPM_Change_Page_DDR_CHG_START_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Parity Force
Reg Addr   : 0x00060
Reg Formula: 
    Where  : 
Reg Desc   : 
Force parity for RAM configuration

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Pariry_Force                                                                      0x00060

/*--------------------------------------
BitField Name: Par_For_pw_def_msk_chn
BitField Type: R/W
BitField Desc: FMPM PW Def Framer Interrupt MASK Per PW
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Force_Par_For_pw_def_msk_chn_Mask                                              cBit21
#define cAf6_FMPM_Pariry_Force_Par_For_pw_def_msk_chn_Shift                                                 21

/*--------------------------------------
BitField Name: Par_For_pw_def_msk_typ
BitField Type: R/W
BitField Desc: FMPM PW Def Interrupt MASK Per Type Per PW
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Force_Par_For_pw_def_msk_typ_Mask                                              cBit20
#define cAf6_FMPM_Pariry_Force_Par_For_pw_def_msk_typ_Shift                                                 20

/*--------------------------------------
BitField Name: Par_For_sts_msk_chn
BitField Type: R/W
BitField Desc: FMPM FM STS24 Interrupt MASK Per STS1
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Force_Par_For_sts_msk_chn_Mask                                                 cBit19
#define cAf6_FMPM_Pariry_Force_Par_For_sts_msk_chn_Shift                                                    19

/*--------------------------------------
BitField Name: Par_For_sts_msk_typ
BitField Type: R/W
BitField Desc: FMPM FM STS24 Interrupt MASK Per Type Of Per STS1
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Force_Par_For_sts_msk_typ_Mask                                                 cBit18
#define cAf6_FMPM_Pariry_Force_Par_For_sts_msk_typ_Shift                                                    18

/*--------------------------------------
BitField Name: Par_For_vt_msk_chn
BitField Type: R/W
BitField Desc: FMPM FM VTTU Interrupt MASK Per VTTU
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Force_Par_For_vt_msk_chn_Mask                                                  cBit17
#define cAf6_FMPM_Pariry_Force_Par_For_vt_msk_chn_Shift                                                     17

/*--------------------------------------
BitField Name: Par_For_vt_msk_typ
BitField Type: R/W
BitField Desc: FMPM FM VTTU Interrupt MASK Per Type Per VTTU
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Force_Par_For_vt_msk_typ_Mask                                                  cBit16
#define cAf6_FMPM_Pariry_Force_Par_For_vt_msk_typ_Shift                                                     16

/*--------------------------------------
BitField Name: Par_For_ds3_msk_chn
BitField Type: R/W
BitField Desc: FMPM FM DS3E3 Framer Interrupt MASK Per DS3E3
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Force_Par_For_ds3_msk_chn_Mask                                                 cBit15
#define cAf6_FMPM_Pariry_Force_Par_For_ds3_msk_chn_Shift                                                    15

/*--------------------------------------
BitField Name: Par_For_ds3_msk_typ
BitField Type: R/W
BitField Desc: FMPM FM DS3E3 Framer Interrupt MASK Per Type of Per DS3E3
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Force_Par_For_ds3_msk_typ_Mask                                                 cBit14
#define cAf6_FMPM_Pariry_Force_Par_For_ds3_msk_typ_Shift                                                    14

/*--------------------------------------
BitField Name: Par_For_ds1_msk_chn
BitField Type: R/W
BitField Desc: FMPM FM DS1E1 Interrupt MASK Per Type Per DS1E1
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Force_Par_For_ds1_msk_chn_Mask                                                 cBit13
#define cAf6_FMPM_Pariry_Force_Par_For_ds1_msk_chn_Shift                                                    13

/*--------------------------------------
BitField Name: Par_For_ds1_msk_typ
BitField Type: R/W
BitField Desc: FMPM FM DS1E1 Interrupt MASK Per Type Per DS1E1
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Force_Par_For_ds1_msk_typ_Mask                                                 cBit12
#define cAf6_FMPM_Pariry_Force_Par_For_ds1_msk_typ_Shift                                                    12

/*--------------------------------------
BitField Name: Par_For_pw_msk_chn
BitField Type: R/W
BitField Desc: FMPM FM PW Framer Interrupt MASK Per PW
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Force_Par_For_pw_msk_chn_Mask                                                  cBit11
#define cAf6_FMPM_Pariry_Force_Par_For_pw_msk_chn_Shift                                                     11

/*--------------------------------------
BitField Name: Par_For_pw_msk_typ
BitField Type: R/W
BitField Desc: FMPM FM PW Interrupt MASK Per Type Per PW
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Force_Par_For_pw_msk_typ_Mask                                                  cBit10
#define cAf6_FMPM_Pariry_Force_Par_For_pw_msk_typ_Shift                                                     10

/*--------------------------------------
BitField Name: Par_For_config_sts_typ
BitField Type: R/W
BitField Desc: FMPM Threshold Type of STS
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Force_Par_For_config_sts_typ_Mask                                               cBit9
#define cAf6_FMPM_Pariry_Force_Par_For_config_sts_typ_Shift                                                  9

/*--------------------------------------
BitField Name: Par_For_config_vt_typ
BitField Type: R/W
BitField Desc: FMPM Threshold Type of VT
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Force_Par_For_config_vt_typ_Mask                                                cBit8
#define cAf6_FMPM_Pariry_Force_Par_For_config_vt_typ_Shift                                                   8

/*--------------------------------------
BitField Name: Par_For_config_ds3_typ
BitField Type: R/W
BitField Desc: FMPM Threshold Type of DS3
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Force_Par_For_config_ds3_typ_Mask                                               cBit7
#define cAf6_FMPM_Pariry_Force_Par_For_config_ds3_typ_Shift                                                  7

/*--------------------------------------
BitField Name: Par_For_config_ds1_typ
BitField Type: R/W
BitField Desc: FMPM Threshold Type of DS1
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Force_Par_For_config_ds1_typ_Mask                                               cBit6
#define cAf6_FMPM_Pariry_Force_Par_For_config_ds1_typ_Shift                                                  6

/*--------------------------------------
BitField Name: Par_For_config_pw_typ
BitField Type: R/W
BitField Desc: FMPM Threshold Type of PW
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Force_Par_For_config_pw_typ_Mask                                                cBit5
#define cAf6_FMPM_Pariry_Force_Par_For_config_pw_typ_Shift                                                   5

/*--------------------------------------
BitField Name: Par_For_config_sts_k
BitField Type: R/W
BitField Desc: FMPM K threshold of STS
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Force_Par_For_config_sts_k_Mask                                                 cBit4
#define cAf6_FMPM_Pariry_Force_Par_For_config_sts_k_Shift                                                    4

/*--------------------------------------
BitField Name: Par_For_config_vt_k
BitField Type: R/W
BitField Desc: FMPM K Threshold of VT
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Force_Par_For_config_vt_k_Mask                                                  cBit3
#define cAf6_FMPM_Pariry_Force_Par_For_config_vt_k_Shift                                                     3

/*--------------------------------------
BitField Name: Par_For_config_ds3_k
BitField Type: R/W
BitField Desc: FMPM K Threshold of DS3
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Force_Par_For_config_ds3_k_Mask                                                 cBit2
#define cAf6_FMPM_Pariry_Force_Par_For_config_ds3_k_Shift                                                    2

/*--------------------------------------
BitField Name: Par_For_config_ds1_k
BitField Type: R/W
BitField Desc: FMPM K Threshold of DS1
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Force_Par_For_config_ds1_k_Mask                                                 cBit1
#define cAf6_FMPM_Pariry_Force_Par_For_config_ds1_k_Shift                                                    1

/*--------------------------------------
BitField Name: Par_For_config_pw_k
BitField Type: R/W
BitField Desc: FMPM K Threshold of PW
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Force_Par_For_config_pw_k_Mask                                                  cBit0
#define cAf6_FMPM_Pariry_Force_Par_For_config_pw_k_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Parity Disable
Reg Addr   : 0x00061
Reg Formula: 
    Where  : 
Reg Desc   : 
Force parity for RAM configuration

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Pariry_Disable                                                                    0x00061

/*--------------------------------------
BitField Name: Par_For_pw_def_msk_chn
BitField Type: R/W
BitField Desc: FMPM PW Def Framer Interrupt MASK Per PW
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Disable_Par_For_pw_def_msk_chn_Mask                                            cBit21
#define cAf6_FMPM_Pariry_Disable_Par_For_pw_def_msk_chn_Shift                                               21

/*--------------------------------------
BitField Name: Par_For_pw_def_msk_typ
BitField Type: R/W
BitField Desc: FMPM PW Def Interrupt MASK Per Type Per PW
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Disable_Par_For_pw_def_msk_typ_Mask                                            cBit20
#define cAf6_FMPM_Pariry_Disable_Par_For_pw_def_msk_typ_Shift                                               20

/*--------------------------------------
BitField Name: Par_For_sts_msk_chn
BitField Type: R/W
BitField Desc: FMPM FM STS24 Interrupt MASK Per STS1
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Disable_Par_For_sts_msk_chn_Mask                                               cBit19
#define cAf6_FMPM_Pariry_Disable_Par_For_sts_msk_chn_Shift                                                  19

/*--------------------------------------
BitField Name: Par_For_sts_msk_typ
BitField Type: R/W
BitField Desc: FMPM FM STS24 Interrupt MASK Per Type Of Per STS1
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Disable_Par_For_sts_msk_typ_Mask                                               cBit18
#define cAf6_FMPM_Pariry_Disable_Par_For_sts_msk_typ_Shift                                                  18

/*--------------------------------------
BitField Name: Par_For_vt_msk_chn
BitField Type: R/W
BitField Desc: FMPM FM VTTU Interrupt MASK Per VTTU
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Disable_Par_For_vt_msk_chn_Mask                                                cBit17
#define cAf6_FMPM_Pariry_Disable_Par_For_vt_msk_chn_Shift                                                   17

/*--------------------------------------
BitField Name: Par_For_vt_msk_typ
BitField Type: R/W
BitField Desc: FMPM FM VTTU Interrupt MASK Per Type Per VTTU
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Disable_Par_For_vt_msk_typ_Mask                                                cBit16
#define cAf6_FMPM_Pariry_Disable_Par_For_vt_msk_typ_Shift                                                   16

/*--------------------------------------
BitField Name: Par_For_ds3_msk_chn
BitField Type: R/W
BitField Desc: FMPM FM DS3E3 Framer Interrupt MASK Per DS3E3
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Disable_Par_For_ds3_msk_chn_Mask                                               cBit15
#define cAf6_FMPM_Pariry_Disable_Par_For_ds3_msk_chn_Shift                                                  15

/*--------------------------------------
BitField Name: Par_For_ds3_msk_typ
BitField Type: R/W
BitField Desc: FMPM FM DS3E3 Framer Interrupt MASK Per Type of Per DS3E3
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Disable_Par_For_ds3_msk_typ_Mask                                               cBit14
#define cAf6_FMPM_Pariry_Disable_Par_For_ds3_msk_typ_Shift                                                  14

/*--------------------------------------
BitField Name: Par_For_ds1_msk_chn
BitField Type: R/W
BitField Desc: FMPM FM DS1E1 Interrupt MASK Per Type Per DS1E1
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Disable_Par_For_ds1_msk_chn_Mask                                               cBit13
#define cAf6_FMPM_Pariry_Disable_Par_For_ds1_msk_chn_Shift                                                  13

/*--------------------------------------
BitField Name: Par_For_ds1_msk_typ
BitField Type: R/W
BitField Desc: FMPM FM DS1E1 Interrupt MASK Per Type Per DS1E1
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Disable_Par_For_ds1_msk_typ_Mask                                               cBit12
#define cAf6_FMPM_Pariry_Disable_Par_For_ds1_msk_typ_Shift                                                  12

/*--------------------------------------
BitField Name: Par_For_pw_msk_chn
BitField Type: R/W
BitField Desc: FMPM FM PW Framer Interrupt MASK Per PW
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Disable_Par_For_pw_msk_chn_Mask                                                cBit11
#define cAf6_FMPM_Pariry_Disable_Par_For_pw_msk_chn_Shift                                                   11

/*--------------------------------------
BitField Name: Par_For_pw_msk_typ
BitField Type: R/W
BitField Desc: FMPM FM PW Interrupt MASK Per Type Per PW
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Disable_Par_For_pw_msk_typ_Mask                                                cBit10
#define cAf6_FMPM_Pariry_Disable_Par_For_pw_msk_typ_Shift                                                   10

/*--------------------------------------
BitField Name: Par_For_config_sts_typ
BitField Type: R/W
BitField Desc: FMPM Threshold Type of STS
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Disable_Par_For_config_sts_typ_Mask                                             cBit9
#define cAf6_FMPM_Pariry_Disable_Par_For_config_sts_typ_Shift                                                9

/*--------------------------------------
BitField Name: Par_For_config_vt_typ
BitField Type: R/W
BitField Desc: FMPM Threshold Type of VT
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Disable_Par_For_config_vt_typ_Mask                                              cBit8
#define cAf6_FMPM_Pariry_Disable_Par_For_config_vt_typ_Shift                                                 8

/*--------------------------------------
BitField Name: Par_For_config_ds3_typ
BitField Type: R/W
BitField Desc: FMPM Threshold Type of DS3
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Disable_Par_For_config_ds3_typ_Mask                                             cBit7
#define cAf6_FMPM_Pariry_Disable_Par_For_config_ds3_typ_Shift                                                7

/*--------------------------------------
BitField Name: Par_For_config_ds1_typ
BitField Type: R/W
BitField Desc: FMPM Threshold Type of DS1
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Disable_Par_For_config_ds1_typ_Mask                                             cBit6
#define cAf6_FMPM_Pariry_Disable_Par_For_config_ds1_typ_Shift                                                6

/*--------------------------------------
BitField Name: Par_For_config_pw_typ
BitField Type: R/W
BitField Desc: FMPM Threshold Type of PW
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Disable_Par_For_config_pw_typ_Mask                                              cBit5
#define cAf6_FMPM_Pariry_Disable_Par_For_config_pw_typ_Shift                                                 5

/*--------------------------------------
BitField Name: Par_For_config_sts_k
BitField Type: R/W
BitField Desc: FMPM K threshold of STS
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Disable_Par_For_config_sts_k_Mask                                               cBit4
#define cAf6_FMPM_Pariry_Disable_Par_For_config_sts_k_Shift                                                  4

/*--------------------------------------
BitField Name: Par_For_config_vt_k
BitField Type: R/W
BitField Desc: FMPM K Threshold of VT
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Disable_Par_For_config_vt_k_Mask                                                cBit3
#define cAf6_FMPM_Pariry_Disable_Par_For_config_vt_k_Shift                                                   3

/*--------------------------------------
BitField Name: Par_For_config_ds3_k
BitField Type: R/W
BitField Desc: FMPM K Threshold of DS3
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Disable_Par_For_config_ds3_k_Mask                                               cBit2
#define cAf6_FMPM_Pariry_Disable_Par_For_config_ds3_k_Shift                                                  2

/*--------------------------------------
BitField Name: Par_For_config_ds1_k
BitField Type: R/W
BitField Desc: FMPM K Threshold of DS1
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Disable_Par_For_config_ds1_k_Mask                                               cBit1
#define cAf6_FMPM_Pariry_Disable_Par_For_config_ds1_k_Shift                                                  1

/*--------------------------------------
BitField Name: Par_For_config_pw_k
BitField Type: R/W
BitField Desc: FMPM K Threshold of PW
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Disable_Par_For_config_pw_k_Mask                                                cBit0
#define cAf6_FMPM_Pariry_Disable_Par_For_config_pw_k_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Parity Sticky
Reg Addr   : 0x00062
Reg Formula: 
    Where  : 
Reg Desc   : 
Change Page for DDR when SW want to get Current Period Parameters

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Pariry_Sticky                                                                     0x00062

/*--------------------------------------
BitField Name: Par_Stk_pw_def_msk_chn
BitField Type: W1C
BitField Desc: FMPM PW Def Framer Interrupt MASK Per PW
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pw_def_msk_chn_Mask                                             cBit21
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pw_def_msk_chn_Shift                                                21

/*--------------------------------------
BitField Name: Par_Stk_pw_def_msk_typ
BitField Type: W1C
BitField Desc: FMPM PW Def Interrupt MASK Per Type Per PW
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pw_def_msk_typ_Mask                                             cBit20
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pw_def_msk_typ_Shift                                                20

/*--------------------------------------
BitField Name: Par_Stk_sts_msk_chn
BitField Type: W1C
BitField Desc: FMPM FM STS24 Interrupt MASK Per STS1
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_sts_msk_chn_Mask                                                cBit19
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_sts_msk_chn_Shift                                                   19

/*--------------------------------------
BitField Name: Par_Stk_sts_msk_typ
BitField Type: W1C
BitField Desc: FMPM FM STS24 Interrupt MASK Per Type Of Per STS1
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_sts_msk_typ_Mask                                                cBit18
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_sts_msk_typ_Shift                                                   18

/*--------------------------------------
BitField Name: Par_Stk_vt_msk_chn
BitField Type: W1C
BitField Desc: FMPM FM VTTU Interrupt MASK Per VTTU
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_vt_msk_chn_Mask                                                 cBit17
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_vt_msk_chn_Shift                                                    17

/*--------------------------------------
BitField Name: Par_Stk_vt_msk_typ
BitField Type: W1C
BitField Desc: FMPM FM VTTU Interrupt MASK Per Type Per VTTU
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_vt_msk_typ_Mask                                                 cBit16
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_vt_msk_typ_Shift                                                    16

/*--------------------------------------
BitField Name: Par_Stk_ds3_msk_chn
BitField Type: W1C
BitField Desc: FMPM FM DS3E3 Framer Interrupt MASK Per DS3E3
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_ds3_msk_chn_Mask                                                cBit15
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_ds3_msk_chn_Shift                                                   15

/*--------------------------------------
BitField Name: Par_Stk_ds3_msk_typ
BitField Type: W1C
BitField Desc: FMPM FM DS3E3 Framer Interrupt MASK Per Type of Per DS3E3
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_ds3_msk_typ_Mask                                                cBit14
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_ds3_msk_typ_Shift                                                   14

/*--------------------------------------
BitField Name: Par_Stk_ds1_msk_chn
BitField Type: W1C
BitField Desc: FMPM FM DS1E1 Interrupt MASK Per Type Per DS1E1
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_ds1_msk_chn_Mask                                                cBit13
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_ds1_msk_chn_Shift                                                   13

/*--------------------------------------
BitField Name: Par_Stk_ds1_msk_typ
BitField Type: W1C
BitField Desc: FMPM FM DS1E1 Interrupt MASK Per Type Per DS1E1
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_ds1_msk_typ_Mask                                                cBit12
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_ds1_msk_typ_Shift                                                   12

/*--------------------------------------
BitField Name: Par_Stk_pw_msk_chn
BitField Type: W1C
BitField Desc: FMPM FM PW Framer Interrupt MASK Per PW
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pw_msk_chn_Mask                                                 cBit11
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pw_msk_chn_Shift                                                    11

/*--------------------------------------
BitField Name: Par_Stk_pw_msk_typ
BitField Type: W1C
BitField Desc: FMPM FM PW Interrupt MASK Per Type Per PW
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pw_msk_typ_Mask                                                 cBit10
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pw_msk_typ_Shift                                                    10

/*--------------------------------------
BitField Name: Par_Stk_config_sts_typ
BitField Type: W1C
BitField Desc: FMPM Threshold Type of STS
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_sts_typ_Mask                                              cBit9
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_sts_typ_Shift                                                 9

/*--------------------------------------
BitField Name: Par_Stk_config_vt_typ
BitField Type: W1C
BitField Desc: FMPM Threshold Type of VT
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_vt_typ_Mask                                               cBit8
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_vt_typ_Shift                                                  8

/*--------------------------------------
BitField Name: Par_Stk_config_ds3_typ
BitField Type: W1C
BitField Desc: FMPM Threshold Type of DS3
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_ds3_typ_Mask                                              cBit7
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_ds3_typ_Shift                                                 7

/*--------------------------------------
BitField Name: Par_Stk_config_ds1_typ
BitField Type: W1C
BitField Desc: FMPM Threshold Type of DS1
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_ds1_typ_Mask                                              cBit6
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_ds1_typ_Shift                                                 6

/*--------------------------------------
BitField Name: Par_Stk_config_pw_typ
BitField Type: W1C
BitField Desc: FMPM Threshold Type of PW
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_pw_typ_Mask                                               cBit5
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_pw_typ_Shift                                                  5

/*--------------------------------------
BitField Name: Par_Stk_config_sts_k
BitField Type: W1C
BitField Desc: FMPM K threshold of STS
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_sts_k_Mask                                                cBit4
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_sts_k_Shift                                                   4

/*--------------------------------------
BitField Name: Par_Stk_config_vt_k
BitField Type: W1C
BitField Desc: FMPM K Threshold of VT
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_vt_k_Mask                                                 cBit3
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_vt_k_Shift                                                    3

/*--------------------------------------
BitField Name: Par_Stk_config_ds3_k
BitField Type: W1C
BitField Desc: FMPM K Threshold of DS3
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_ds3_k_Mask                                                cBit2
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_ds3_k_Shift                                                   2

/*--------------------------------------
BitField Name: Par_Stk_config_ds1_k
BitField Type: W1C
BitField Desc: FMPM K Threshold of DS1
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_ds1_k_Mask                                                cBit1
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_ds1_k_Shift                                                   1

/*--------------------------------------
BitField Name: Par_Stk_config_pw_k
BitField Type: W1C
BitField Desc: FMPM K Threshold of PW
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_pw_k_Mask                                                 cBit0
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_pw_k_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Force Reset Core
Reg Addr   : 0x00005
Reg Formula: 
    Where  : 
Reg Desc   : 
Force Reset PM Core

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Force_Reset_Core                                                                  0x00005

/*--------------------------------------
BitField Name: sel_1s_mode
BitField Type: R/W
BitField Desc: Select source of 1s for PM function
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_FMPM_Force_Reset_Core_sel_1s_mode_Mask                                                     cBit18
#define cAf6_FMPM_Force_Reset_Core_sel_1s_mode_Shift                                                        18

/*--------------------------------------
BitField Name: param_enb
BitField Type: R/W
BitField Desc: Enable Parameter counters
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_FMPM_Force_Reset_Core_param_enb_Mask                                                       cBit17
#define cAf6_FMPM_Force_Reset_Core_param_enb_Shift                                                          17

/*--------------------------------------
BitField Name: lopwcnt_enb
BitField Type: R/W
BitField Desc: Enable PW counters
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_FMPM_Force_Reset_Core_lopwcnt_enb_Mask                                                     cBit16
#define cAf6_FMPM_Force_Reset_Core_lopwcnt_enb_Shift                                                        16

/*--------------------------------------
BitField Name: drst_enb
BitField Type: R/W
BitField Desc: Enable PM block, SW has to do reset PM block when this bit is
transition from 0/1 to 1/0
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_FMPM_Force_Reset_Core_drst_enb_Mask                                                         cBit8
#define cAf6_FMPM_Force_Reset_Core_drst_enb_Shift                                                            8

/*--------------------------------------
BitField Name: drdy_chg
BitField Type: W1C
BitField Desc: Ready signal has changed 0/1
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_Force_Reset_Core_drdy_chg_Mask                                                         cBit5
#define cAf6_FMPM_Force_Reset_Core_drdy_chg_Shift                                                            5

/*--------------------------------------
BitField Name: drst
BitField Type: R_O
BitField Desc: PM Core reset done
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_Force_Reset_Core_drst_Mask                                                             cBit4
#define cAf6_FMPM_Force_Reset_Core_drst_Shift                                                                4

/*--------------------------------------
BitField Name: frst
BitField Type: R/W
BitField Desc: Trigger 0->1 to reset PM Core
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_Force_Reset_Core_frst_Mask                                                             cBit0
#define cAf6_FMPM_Force_Reset_Core_frst_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Config Number Clock Of 125us
Reg Addr   : 0x00007
Reg Formula: 
    Where  : 
Reg Desc   : 
Configure number clock of 125us. This register is used by HW only.

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Config_Number_Clock_Of_125us_Base                                                 0x00007

/*--------------------------------------
BitField Name: time2p5s
BitField Type: R/W
BitField Desc: Threshold for Failure set  (Ref 2.5s), step 100ms, SW has to
minus 1 before configuring to HW
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_FMPM_Config_Number_Clock_Of_125us_time2p5s_Mask                                         cBit31_24
#define cAf6_FMPM_Config_Number_Clock_Of_125us_time2p5s_Shift                                               24

/*--------------------------------------
BitField Name: time10s
BitField Type: R/W
BitField Desc: Threshold for Failure clear(Ref  10s), step 100ms, SW has to
minus 1 before configuring to HW
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_FMPM_Config_Number_Clock_Of_125us_time10s_Mask                                          cBit23_16
#define cAf6_FMPM_Config_Number_Clock_Of_125us_time10s_Shift                                                16

/*--------------------------------------
BitField Name: config125us
BitField Type: R/W
BitField Desc: Number of clocks for one 125us unit. N(mhz) is operation clock of
PM block then the configuration value is (N*125-2)
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_FMPM_Config_Number_Clock_Of_125us_config125us_Mask                                       cBit15_0
#define cAf6_FMPM_Config_Number_Clock_Of_125us_config125us_Shift                                             0

/*------------------------------------------------------------------------------
Reg Name   : FMPM Syn Get Time
Reg Addr   : 0x00008
Reg Formula: 
    Where  : 
Reg Desc   : 
SW request HW synchronize timing.

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Syn_Get_time                                                                      0x00008

/*--------------------------------------
BitField Name: syntime
BitField Type: R/W
BitField Desc: Trigger 0->1 to synchronize timing
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_FMPM_Syn_Get_time_syntime_Mask                                                             cBit20
#define cAf6_FMPM_Syn_Get_time_syntime_Shift                                                                20

/*--------------------------------------
BitField Name: gettime
BitField Type: R/W
BitField Desc: Trigger 0->1 to get time value
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_FMPM_Syn_Get_time_gettime_Mask                                                             cBit16
#define cAf6_FMPM_Syn_Get_time_gettime_Shift                                                                16

/*--------------------------------------
BitField Name: numclk
BitField Type: R_O
BitField Desc: number of clocks for one 125us unit
BitField Bits: [14:00]
--------------------------------------*/
#define cAf6_FMPM_Syn_Get_time_numclk_Mask                                                            cBit14_0
#define cAf6_FMPM_Syn_Get_time_numclk_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Time Value
Reg Addr   : 0x00009
Reg Formula: 
    Where  : 
Reg Desc   : 
Time value after SW request to get time

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Time_Value                                                                        0x00009

/*--------------------------------------
BitField Name: cnt1s
BitField Type: R_O
BitField Desc: counter 1s
BitField Bits: [26:16]
--------------------------------------*/
#define cAf6_FMPM_Time_Value_cnt1s_Mask                                                              cBit26_16
#define cAf6_FMPM_Time_Value_cnt1s_Shift                                                                    16

/*--------------------------------------
BitField Name: cnt1ms
BitField Type: R_O
BitField Desc: counter 1ms
BitField Bits: [12:04]
--------------------------------------*/
#define cAf6_FMPM_Time_Value_cnt1ms_Mask                                                              cBit12_4
#define cAf6_FMPM_Time_Value_cnt1ms_Shift                                                                    4

/*--------------------------------------
BitField Name: cnt125us
BitField Type: R_O
BitField Desc: counter 125us
BitField Bits: [02:00]
--------------------------------------*/
#define cAf6_FMPM_Time_Value_cnt125us_Mask                                                             cBit2_0
#define cAf6_FMPM_Time_Value_cnt125us_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Monitor Timer
Reg Addr   : 0x0001C
Reg Formula:
    Where  :
Reg Desc   :
Time value after SW request to get time

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Monitor_Timer                                                                     0x0001C

/*--------------------------------------
BitField Name: mon125us
BitField Type: R_O
BitField Desc: monitor timer of HW, resolution is 125us, counter will be roll-
over when it get maximum value
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_Monitor_Timer_mon125us_Mask                                                         cBit31_0
#define cAf6_FMPM_Monitor_Timer_mon125us_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Hold00 Data Of Read DDR
Reg Addr   : 0x00020
Reg Formula: 
    Where  : 
Reg Desc   : 
Hold00 Data Of Read DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Hold00_Data_Of_Read_DDR                                                           0x00020

#define cAf6_Sts_CV_P_Index             0
#define cAf6_Sts_CV_P_Mask       cBit23_0
#define cAf6_Sts_CV_P_Shift             0

#define cAf6_Vt_CV_V_Index              0
#define cAf6_Vt_CV_V_Mask        cBit23_0
#define cAf6_Vt_CV_V_Shift              0

#define cAf6_Ds3_CV_L_Index             0
#define cAf6_Ds3_CV_L_Mask       cBit23_0
#define cAf6_Ds3_CV_L_Shift             0

#define cAf6_Ds1_CV_L_Index             0
#define cAf6_Ds1_CV_L_Mask       cBit23_0
#define cAf6_Ds1_CV_L_Shift             0

#define cAf6_Pw_NE_ES_Index             0
#define cAf6_Pw_NE_ES_Mask      cBit25_16
#define cAf6_Pw_NE_ES_Shift            16
#define cAf6_Pw_FE_ES_Index             0
#define cAf6_Pw_FE_ES_Mask        cBit9_0
#define cAf6_Pw_FE_ES_Shift             0

#define cAf6_PwCnt_TX_NOB_Index         0
#define cAf6_PwCnt_TX_NOB_Mask   cBit31_0
#define cAf6_PwCnt_TX_NOB_Shift         0

#define cAf6_Ec1_CV_S_Index             0
#define cAf6_Ec1_CV_S_Mask       cBit23_0
#define cAf6_Ec1_CV_S_Shift             0

#define cAf6_EncCnt_Idle_Index          0
#define cAf6_EncCnt_Idle_Mask    cBit31_0
#define cAf6_EncCnt_Idle_Shift          0

/*--------------------------------------
BitField Name: Hold00
BitField Type: R_O
BitField Desc: Missing bit is unused
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_Hold00_Data_Of_Read_DDR_Hold00_Mask                                                 cBit31_0
#define cAf6_FMPM_Hold00_Data_Of_Read_DDR_Hold00_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Hold01 Data Of Read DDR
Reg Addr   : 0x00021
Reg Formula: 
    Where  : 
Reg Desc   : 
Hold01 Data Of Read DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Hold01_Data_Of_Read_DDR                                                           0x00021

#define cAf6_Sts_ES_P_Index             1
#define cAf6_Sts_ES_P_Mask      cBit29_20
#define cAf6_Sts_ES_P_Shift            20
#define cAf6_Sts_PPJC_PDet_Index        1
#define cAf6_Sts_PPJC_PDet_Mask  cBit19_0
#define cAf6_Sts_PPJC_PDet_Shift        0
#define cAf6_Vt_ES_V_Index              1
#define cAf6_Vt_ES_V_Mask       cBit29_20
#define cAf6_Vt_ES_V_Shift             20
#define cAf6_Vt_PPJC_VDet_Index         1
#define cAf6_Vt_PPJC_VDet_Mask   cBit19_0
#define cAf6_Vt_PPJC_VDet_Shift         0
#define cAf6_Ds3_ES_L_Index             1
#define cAf6_Ds3_ES_L_Mask      cBit29_20
#define cAf6_Ds3_ES_L_Shift            20
#define cAf6_Ds3_SES_L_Index            1
#define cAf6_Ds3_SES_L_Mask     cBit19_10
#define cAf6_Ds3_SES_L_Shift           10
#define cAf6_Ds3_LOSS_L_Index           1
#define cAf6_Ds3_LOSS_L_Mask      cBit9_0
#define cAf6_Ds3_LOSS_L_Shift           0
#define cAf6_Ds1_ES_L_Index             1
#define cAf6_Ds1_ES_L_Mask      cBit19_10
#define cAf6_Ds1_ES_L_Shift            10
#define cAf6_Ds1_SES_L_Index            1
#define cAf6_Ds1_SES_L_Mask       cBit9_0
#define cAf6_Ds1_SES_L_Shift            0
#define cAf6_Pw_NE_SES_Index            1
#define cAf6_Pw_NE_SES_Mask     cBit25_16
#define cAf6_Pw_NE_SES_Shift           16
#define cAf6_Pw_FE_SES_Index            1
#define cAf6_Pw_FE_SES_Mask       cBit9_0
#define cAf6_Pw_FE_SES_Shift            0

#define cAf6_PwCnt_TX_LBIT_Index        1
#define cAf6_PwCnt_TX_LBIT_Mask  cBit31_0
#define cAf6_PwCnt_TX_LBIT_Shift        0

#define cAf6_Ec1_SEFS_S_Index           1
#define cAf6_Ec1_SEFS_S_Mask    cBit29_20
#define cAf6_Ec1_SEFS_S_Shift          20
#define cAf6_Ec1_ES_S_Index             1
#define cAf6_Ec1_ES_S_Mask      cBit19_10
#define cAf6_Ec1_ES_S_Shift            10
#define cAf6_Ec1_SES_S_Index            1
#define cAf6_Ec1_SES_S_Mask       cBit9_0
#define cAf6_Ec1_SES_S_Shift            0

#define cAf6_EncCnt_GoodPkt_Index          1
#define cAf6_EncCnt_GoodPkt_Mask    cBit31_0
#define cAf6_EncCnt_GoodPkt_Shift          0


/*--------------------------------------
BitField Name: Hold01
BitField Type: R_O
BitField Desc: Missing bit is unused
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_Hold01_Data_Of_Read_DDR_Hold01_Mask                                                 cBit31_0
#define cAf6_FMPM_Hold01_Data_Of_Read_DDR_Hold01_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Hold02 Data Of Read DDR
Reg Addr   : 0x00022
Reg Formula: 
    Where  : 
Reg Desc   : 
Hold02 Data Of Read DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Hold02_Data_Of_Read_DDR                                                           0x00022

#define cAf6_Sts_SES_P_Index            2
#define cAf6_Sts_SES_P_Mask     cBit29_20
#define cAf6_Sts_SES_P_Shift           20
#define cAf6_Sts_NPJC_PDet_Index        2
#define cAf6_Sts_NPJC_PDet_Mask  cBit19_0
#define cAf6_Sts_NPJC_PDet_Shift        0
#define cAf6_Vt_SES_V_Index             2
#define cAf6_Vt_SES_V_Mask      cBit29_20
#define cAf6_Vt_SES_V_Shift            20
#define cAf6_Vt_NPJC_VDet_Index         2
#define cAf6_Vt_NPJC_VDet_Mask   cBit14_0
#define cAf6_Vt_NPJC_VDet_Shift         0
#define cAf6_Ds3_ESA_L_Index            2
#define cAf6_Ds3_ESA_L_Mask     cBit19_10
#define cAf6_Ds3_ESA_L_Shift           10
#define cAf6_Ds3_ESB_L_Index            2
#define cAf6_Ds3_ESB_L_Mask       cBit9_0
#define cAf6_Ds3_ESB_L_Shift            0

#define cAf6_Ds1_LOSS_L_Index           2
#define cAf6_Ds1_LOSS_L_Mask    cBit19_10
#define cAf6_Ds1_LOSS_L_Shift          10
#define cAf6_Ds1_ES_LFE_Index           2
#define cAf6_Ds1_ES_LFE_Mask      cBit9_0
#define cAf6_Ds1_ES_LFE_Shift           0

#define cAf6_Pw_NE_UAS_Index            2
#define cAf6_Pw_NE_UAS_Mask     cBit25_16
#define cAf6_Pw_NE_UAS_Shift           16
#define cAf6_Pw_FE_UAS_Index            2
#define cAf6_Pw_FE_UAS_Mask       cBit9_0
#define cAf6_Pw_FE_UAS_Shift            0

#define cAf6_PwCnt_TX_NBIT_Index        2
#define cAf6_PwCnt_TX_NBIT_Mask  cBit31_0
#define cAf6_PwCnt_TX_NBIT_Shift        0

#define cAf6_Ec1_CV_L_Index             2
#define cAf6_Ec1_CV_L_Mask       cBit23_0
#define cAf6_Ec1_CV_L_Shift             0

#define cAf6_EncCnt_GoodBytes_Index          2
#define cAf6_EncCnt_GoodBytes_Mask    cBit31_0
#define cAf6_EncCnt_GoodBytes_Shift          0

/*--------------------------------------
BitField Name: Hold02
BitField Type: R_O
BitField Desc: Missing bit is unused
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_Hold02_Data_Of_Read_DDR_Hold02_Mask                                                 cBit31_0
#define cAf6_FMPM_Hold02_Data_Of_Read_DDR_Hold02_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Hold03 Data Of Read DDR
Reg Addr   : 0x00023
Reg Formula: 
    Where  : 
Reg Desc   : 
Hold03 Data Of Read DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Hold03_Data_Of_Read_DDR                                                           0x00023


#define cAf6_Sts_UAS_P_Index             3
#define cAf6_Sts_UAS_P_Mask      cBit29_20
#define cAf6_Sts_UAS_P_Shift            20
#define cAf6_Sts_PPJC_PGEN_Index         3
#define cAf6_Sts_PPJC_PGEN_Mask   cBit19_0
#define cAf6_Sts_PPJC_PGEN_Shift         0

#define cAf6_Vt_UAS_V_Index              3
#define cAf6_Vt_UAS_V_Mask       cBit29_20
#define cAf6_Vt_UAS_V_Shift             20
#define cAf6_Vt_PPJC_VGEN_Index          3
#define cAf6_Vt_PPJC_VGEN_Mask    cBit19_0
#define cAf6_Vt_PPJC_VGEN_Shift          0

#define cAf6_Ds3_CVP_P_Index             3
#define cAf6_Ds3_CVP_P_Mask       cBit23_0
#define cAf6_Ds3_CVP_P_Shift             0

#define cAf6_Ds1_CV_P_Index              3
#define cAf6_Ds1_CV_P_Mask        cBit23_0
#define cAf6_Ds1_CV_P_Shift              0

#define cAf6_PW_UAS_NE_Sta_Index         3
#define cAf6_PW_UAS_NE_Sta_Mask     cBit31
#define cAf6_PW_UAS_NE_Sta_Shift        31
#define cAf6_PW_UAS_NE_Swp_Index         3
#define cAf6_PW_UAS_NE_Swp_Mask     cBit30
#define cAf6_PW_UAS_NE_Swp_Shift        30
#define cAf6_PW_UAS_NE_Adj_Index         3
#define cAf6_PW_UAS_NE_Adj_Mask  cBit19_16
#define cAf6_PW_UAS_NE_Adj_Shift        16
#define cAf6_PW_UAS_NE_Cur_Index         3
#define cAf6_PW_UAS_NE_Cur_Mask   cBit11_8
#define cAf6_PW_UAS_NE_Cur_Shift         8

#define cAf6_PwCnt_TX_PBIT_Index         3
#define cAf6_PwCnt_TX_PBIT_Mask   cBit31_0
#define cAf6_PwCnt_TX_PBIT_Shift         0

#define cAf6_Ec1_ES_L_Index              3
#define cAf6_Ec1_ES_L_Mask       cBit29_20
#define cAf6_Ec1_ES_L_Shift             20
#define cAf6_Ec1_SES_L_Index             3
#define cAf6_Ec1_SES_L_Mask      cBit19_10
#define cAf6_Ec1_SES_L_Shift            10
#define cAf6_Ec1_FC_L_Index              3
#define cAf6_Ec1_FC_L_Mask         cBit9_0
#define cAf6_Ec1_FC_L_Shift              0

#define cAf6_EncCnt_Fragment_Index          3
#define cAf6_EncCnt_Fragment_Mask    cBit31_0
#define cAf6_EncCnt_Fragment_Shift          0

/*--------------------------------------
BitField Name: Hold03
BitField Type: R_O
BitField Desc: Missing bit is unused
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_Hold03_Data_Of_Read_DDR_Hold03_Mask                                                 cBit31_0
#define cAf6_FMPM_Hold03_Data_Of_Read_DDR_Hold03_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Hold04 Data Of Read DDR
Reg Addr   : 0x00024
Reg Formula: 
    Where  : 
Reg Desc   : 
Hold04 Data Of Read DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Hold04_Data_Of_Read_DDR                                                           0x00024

#define cAf6_Sts_FC_P_Index              4
#define cAf6_Sts_FC_P_Mask       cBit22_16
#define cAf6_Sts_FC_P_Shift             16
#define cAf6_Sts_FC_PFE_Index            4
#define cAf6_Sts_FC_PFE_Mask       cBit9_0
#define cAf6_Sts_FC_PFE_Shift            0

#define cAf6_Vt_FC_V_Index               4
#define cAf6_Vt_FC_V_Mask        cBit22_16
#define cAf6_Vt_FC_V_Shift              16
#define cAf6_Vt_FC_VFE_Index             4
#define cAf6_Vt_FC_VFE_Mask        cBit9_0
#define cAf6_Vt_FC_VFE_Shift             0

#define cAf6_Ds3_ESP_P_Index             4
#define cAf6_Ds3_ESP_P_Mask      cBit29_20
#define cAf6_Ds3_ESP_P_Shift            20
#define cAf6_Ds3_ESAP_P_Index            4
#define cAf6_Ds3_ESAP_P_Mask     cBit19_10
#define cAf6_Ds3_ESAP_P_Shift           10
#define cAf6_Ds3_ESBP_P_Index            4
#define cAf6_Ds3_ESBP_P_Mask       cBit9_0
#define cAf6_Ds3_ESBP_P_Shift            0

#define cAf6_Ds1_ES_P_Index              4
#define cAf6_Ds1_ES_P_Mask       cBit29_20
#define cAf6_Ds1_ES_P_Shift             20
#define cAf6_Ds1_SES_P_Index             4
#define cAf6_Ds1_SES_P_Mask      cBit19_10
#define cAf6_Ds1_SES_P_Shift            10
#define cAf6_Ds1_AISS_P_Index            4
#define cAf6_Ds1_AISS_P_Mask       cBit9_0
#define cAf6_Ds1_AISS_P_Shift            0

#define cAf6_PW_UAS_FE_Sta_Index         4
#define cAf6_PW_UAS_FE_Sta_Mask     cBit31
#define cAf6_PW_UAS_FE_Sta_Shift        31
#define cAf6_PW_UAS_FE_Swp_Index         4
#define cAf6_PW_UAS_FE_Swp_Mask     cBit30
#define cAf6_PW_UAS_FE_Swp_Shift        30
#define cAf6_PW_UAS_FE_Adj_Index         4
#define cAf6_PW_UAS_FE_Adj_Mask  cBit19_16
#define cAf6_PW_UAS_FE_Adj_Shift        16
#define cAf6_PW_UAS_FE_Cur_Index         4
#define cAf6_PW_UAS_FE_Cur_Mask   cBit11_8
#define cAf6_PW_UAS_FE_Cur_Shift         8

#define cAf6_PwCnt_TX_RBIT_Index         4
#define cAf6_PwCnt_TX_RBIT_Mask   cBit31_0
#define cAf6_PwCnt_TX_RBIT_Shift         0

#define cAf6_Ec1_CV_LFE_Index            4
#define cAf6_Ec1_CV_LFE_Mask      cBit23_0
#define cAf6_Ec1_CV_LFE_Shift            0

#define cAf6_EncCnt_Plain_Index          4
#define cAf6_EncCnt_Plain_Mask    cBit31_0
#define cAf6_EncCnt_Plain_Shift          0

/*--------------------------------------
BitField Name: Hold04
BitField Type: R_O
BitField Desc: Missing bit is unused
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_Hold04_Data_Of_Read_DDR_Hold04_Mask                                                 cBit31_0
#define cAf6_FMPM_Hold04_Data_Of_Read_DDR_Hold04_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Hold05 Data Of Read DDR
Reg Addr   : 0x00025
Reg Formula: 
    Where  : 
Reg Desc   : 
Hold05 Data Of Read DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Hold05_Data_Of_Read_DDR                                                           0x00025

#define cAf6_Sts_PJCS_PDet_Index          5
#define cAf6_Sts_PJCS_PDet_Mask   cBit29_20
#define cAf6_Sts_PJCS_PDet_Shift         20
#define cAf6_Sts_NPJC_PGEN_Index          5
#define cAf6_Sts_NPJC_PGEN_Mask    cBit19_0
#define cAf6_Sts_NPJC_PGEN_Shift          0

#define cAf6_Vt_PJCS_VDET_Index           5
#define cAf6_Vt_PJCS_VDET_Mask    cBit29_20
#define cAf6_Vt_PJCS_VDET_Shift          20
#define cAf6_Vt_NPJC_VGEN_Index           5
#define cAf6_Vt_NPJC_VGEN_Mask     cBit14_0
#define cAf6_Vt_NPJC_VGEN_Shift           0

#define cAf6_Ds3_SESP_P_Index             5
#define cAf6_Ds3_SESP_P_Mask      cBit29_20
#define cAf6_Ds3_SESP_P_Shift            20
#define cAf6_Ds3_SAS_P_Index              5
#define cAf6_Ds3_SAS_P_Mask       cBit19_10
#define cAf6_Ds3_SAS_P_Shift             10
#define cAf6_Ds3_AISS_P_Index             5
#define cAf6_Ds3_AISS_P_Mask        cBit9_0
#define cAf6_Ds3_AISS_P_Shift             0

#define cAf6_Ds1_SAS_P_Index              5
#define cAf6_Ds1_SAS_P_Mask       cBit29_20
#define cAf6_Ds1_SAS_P_Shift             20
#define cAf6_Ds1_CSS_P_Index              5
#define cAf6_Ds1_CSS_P_Mask       cBit19_10
#define cAf6_Ds1_CSS_P_Shift             10
#define cAf6_Ds1_UAS_P_Index              5
#define cAf6_Ds1_UAS_P_Mask         cBit9_0
#define cAf6_Ds1_UAS_P_Shift              0

#define cAf6_PW_ES_P_Adj_Index            5
#define cAf6_PW_ES_P_Adj_Mask     cBit27_24
#define cAf6_PW_ES_P_Adj_Shift           24
#define cAf6_PW_ES_PFE_Adj_Index          5
#define cAf6_PW_ES_PFE_Adj_Mask   cBit19_16
#define cAf6_PW_ES_PFE_Adj_Shift         16

#define cAf6_PwCnt_RX_NOB_Index           5
#define cAf6_PwCnt_RX_NOB_Mask     cBit31_0
#define cAf6_PwCnt_RX_NOB_Shift           0

#define cAf6_Ec1_ES_LFE_Index             5
#define cAf6_Ec1_ES_LFE_Mask      cBit29_20
#define cAf6_Ec1_ES_LFE_Shift            20
#define cAf6_Ec1_SES_LFE_Index            5
#define cAf6_Ec1_SES_LFE_Mask     cBit19_10
#define cAf6_Ec1_SES_LFE_Shift           10
#define cAf6_Ec1_FC_LFE_Index             5
#define cAf6_Ec1_FC_LFE_Mask        cBit9_0
#define cAf6_Ec1_FC_LFE_Shift             0

#define cAf6_EncCnt_Oam_Index          5
#define cAf6_EncCnt_Oam_Mask    cBit31_0
#define cAf6_EncCnt_Oam_Shift          0

/*--------------------------------------
BitField Name: Hold05
BitField Type: R_O
BitField Desc: Missing bit is unused
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_Hold05_Data_Of_Read_DDR_Hold05_Mask                                                 cBit31_0
#define cAf6_FMPM_Hold05_Data_Of_Read_DDR_Hold05_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Hold06 Data Of Read DDR
Reg Addr   : 0x00026
Reg Formula: 
    Where  : 
Reg Desc   : 
Hold06 Data Of Read DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Hold06_Data_Of_Read_DDR                                                           0x00026

#define cAf6_Sts_PJCDIFF_P_Index          6
#define cAf6_Sts_PJCDIFF_P_Mask   cBit31_12
#define cAf6_Sts_PJCDIFF_P_Shift         12
#define cAf6_Sts_PJCS_PGEN_Index          6
#define cAf6_Sts_PJCS_PGEN_Mask     cBit9_0
#define cAf6_Sts_PJCS_PGEN_Shift          0

#define cAf6_Vt_PJCDIFF_V_Index           6
#define cAf6_Vt_PJCDIFF_V_Mask    cBit26_12
#define cAf6_Vt_PJCDIFF_V_Shift          12
#define cAf6_Vt_PJCS_VGen_Index           6
#define cAf6_Vt_PJCS_VGen_Mask      cBit9_0
#define cAf6_Vt_PJCS_VGen_Shift           0

#define cAf6_Ds3_UAS_P_Index              6
#define cAf6_Ds3_UAS_P_Mask       cBit29_20
#define cAf6_Ds3_UAS_P_Shift             20
#define cAf6_Ds3_FC_P_Index               6
#define cAf6_Ds3_FC_P_Mask        cBit19_10
#define cAf6_Ds3_FC_P_Shift              10
#define cAf6_Ds3_FCCP_PFE_Index           6
#define cAf6_Ds3_FCCP_PFE_Mask      cBit9_0
#define cAf6_Ds3_FCCP_PFE_Shift           0

#define cAf6_Ds1_FC_PFE_Index             6
#define cAf6_Ds1_FC_PFE_Mask      cBit19_10
#define cAf6_Ds1_FC_PFE_Shift            10
#define cAf6_Ds1_FC_P_Index               6
#define cAf6_Ds1_FC_P_Mask          cBit9_0
#define cAf6_Ds1_FC_P_Shift               0

#define cAf6_PW_FC_P_Index                6
#define cAf6_PW_FC_P_Mask           cBit9_0
#define cAf6_PW_FC_P_Shift                0

#define cAf6_PwCnt_RX_LBIT_Index          6
#define cAf6_PwCnt_RX_LBIT_Mask    cBit31_0
#define cAf6_PwCnt_RX_LBIT_Shift          0

#define cAf6_Ec1_UAS_L_Sta_Index          6
#define cAf6_Ec1_UAS_L_Sta_Mask      cBit31
#define cAf6_Ec1_UAS_L_Sta_Shift         31
#define cAf6_Ec1_UAS_L_Swp_Index          6
#define cAf6_Ec1_UAS_L_Swp_Mask      cBit30
#define cAf6_Ec1_UAS_L_Swp_Shift         30
#define cAf6_Ec1_UAS_L_Adj_Index          6
#define cAf6_Ec1_UAS_L_Adj_Mask   cBit19_16
#define cAf6_Ec1_UAS_L_Adj_Shift         16
#define cAf6_Ec1_UAS_L_Cur_Index          6
#define cAf6_Ec1_UAS_L_Cur_Mask    cBit11_8
#define cAf6_Ec1_UAS_L_Cur_Shift          8

#define cAf6_EncCnt_Abort_Index          6
#define cAf6_EncCnt_Abort_Mask    cBit31_0
#define cAf6_EncCnt_Abort_Shift          0

/*--------------------------------------
BitField Name: Hold06
BitField Type: R_O
BitField Desc: Missing bit is unused
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_Hold06_Data_Of_Read_DDR_Hold06_Mask                                                 cBit31_0
#define cAf6_FMPM_Hold06_Data_Of_Read_DDR_Hold06_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Hold07 Data Of Read DDR
Reg Addr   : 0x00027
Reg Formula: 
    Where  : 
Reg Desc   : 
Hold07 Data Of Read DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Hold07_Data_Of_Read_DDR                                                           0x00027

#define cAf6_Sts_CV_PFE_Index           7
#define cAf6_Sts_CV_PFE_Mask     cBit23_0
#define cAf6_Sts_CV_PFE_Shift           0

#define cAf6_Vt_CV_VFE_Index            7
#define cAf6_Vt_CV_VFE_Mask      cBit23_0
#define cAf6_Vt_CV_VFE_Shift            0

#define cAf6_Ds3_CVCP_P_Index           7
#define cAf6_Ds3_CVCP_P_Mask     cBit23_0
#define cAf6_Ds3_CVCP_P_Shift           0

#define cAf6_Ds1_SEFS_PFE_Index         7
#define cAf6_Ds1_SEFS_PFE_Mask  cBit19_10
#define cAf6_Ds1_SEFS_PFE_Shift        10
#define cAf6_Ds1_ES_PFE_Index           7
#define cAf6_Ds1_ES_PFE_Mask      cBit9_0
#define cAf6_Ds1_ES_PFE_Shift           0

#define cAf6_PwCnt_RX_STRAY_Index       7
#define cAf6_PwCnt_RX_STRAY_Mask cBit31_0
#define cAf6_PwCnt_RX_STRAY_Shift       0

#define cAf6_Ec1_UAS_LFE_Sta_Index      7
#define cAf6_Ec1_UAS_LFE_Sta_Mask  cBit31
#define cAf6_Ec1_UAS_LFE_Sta_Shift     31
#define cAf6_Ec1_UAS_LFE_Swp_Index      7
#define cAf6_Ec1_UAS_LFE_Swp_Mask  cBit30
#define cAf6_Ec1_UAS_LFE_Swp_Shift     30
#define cAf6_Ec1_UAS_LFE_Adj_Index      7
#define cAf6_Ec1_UAS_LFE_Adj_Mask   cBit19_16
#define cAf6_Ec1_UAS_LFE_Adj_Shift     16
#define cAf6_Ec1_UAS_LFE_Cur_Index      7
#define cAf6_Ec1_UAS_LFE_Cur_Mask   cBit11_8
#define cAf6_Ec1_UAS_LFE_Cur_Shift      8

#define cAf6_DecCnt_Bytes_Index          7
#define cAf6_DecCnt_Bytes_Mask    cBit31_0
#define cAf6_DecCnt_Bytes_Shift          0

/*--------------------------------------
BitField Name: Hold07
BitField Type: R_O
BitField Desc: Missing bit is unused
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_Hold07_Data_Of_Read_DDR_Hold07_Mask                                                 cBit31_0
#define cAf6_FMPM_Hold07_Data_Of_Read_DDR_Hold07_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Hold08 Data Of Read DDR
Reg Addr   : 0x00028
Reg Formula: 
    Where  : 
Reg Desc   : 
Hold08 Data Of Read DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Hold08_Data_Of_Read_DDR                                                           0x00028

#define cAf6_Sts_ES_PFE_Index           8
#define cAf6_Sts_ES_PFE_Mask    cBit29_20
#define cAf6_Sts_ES_PFE_Shift          20
#define cAf6_Sts_SES_PFE_Index          8
#define cAf6_Sts_SES_PFE_Mask   cBit19_10
#define cAf6_Sts_SES_PFE_Shift         10
#define cAf6_Sts_UAS_PFE_Index          8
#define cAf6_Sts_UAS_PFE_Mask     cBit9_0
#define cAf6_Sts_UAS_PFE_Shift          0

#define cAf6_Vt_ES_VFE_Index            8
#define cAf6_Vt_ES_VFE_Mask     cBit29_20
#define cAf6_Vt_ES_VFE_Shift           20
#define cAf6_Vt_SES_VFE_Index           8
#define cAf6_Vt_SES_VFE_Mask    cBit19_10
#define cAf6_Vt_SES_VFE_Shift          10
#define cAf6_Vt_UAS_VFE_Index           8
#define cAf6_Vt_UAS_VFE_Mask      cBit9_0
#define cAf6_Vt_UAS_VFE_Shift           0

#define cAf6_Ds3_ESCP_P_Index           8
#define cAf6_Ds3_ESCP_P_Mask    cBit29_20
#define cAf6_Ds3_ESCP_P_Shift          20
#define cAf6_Ds3_ESACP_P_Index          8
#define cAf6_Ds3_ESACP_P_Mask   cBit19_10
#define cAf6_Ds3_ESACP_P_Shift         10
#define cAf6_Ds3_ESBCP_P_Index          8
#define cAf6_Ds3_ESBCP_P_Mask     cBit9_0
#define cAf6_Ds3_ESBCP_P_Shift          0

#define cAf6_Ds1_SES_PFE_Index          8
#define cAf6_Ds1_SES_PFE_Mask   cBit29_20
#define cAf6_Ds1_SES_PFE_Shift         20
#define cAf6_Ds1_CSS_PFE_Index          8
#define cAf6_Ds1_CSS_PFE_Mask   cBit19_10
#define cAf6_Ds1_CSS_PFE_Shift         10
#define cAf6_Ds1_UAS_PFE_Index          8
#define cAf6_Ds1_UAS_PFE_Mask     cBit9_0
#define cAf6_Ds1_UAS_PFE_Shift          0

#define cAf6_PwCnt_RX_MALF_Index        8
#define cAf6_PwCnt_RX_MALF_Mask  cBit31_0
#define cAf6_PwCnt_RX_MALF_Shift        0

#define cAf6_Ec1_CV_L_Adj_Index          8
#define cAf6_Ec1_CV_L_Adj_Mask   cBit27_24
#define cAf6_Ec1_CV_L_Adj_Shift         24
#define cAf6_Ec1_ES_L_Adj_Index          8
#define cAf6_Ec1_ES_L_Adj_Mask   cBit19_16
#define cAf6_Ec1_ES_L_Adj_Shift         16
#define cAf6_Ec1_CV_LFE_Adj_Index        8
#define cAf6_Ec1_CV_LFE_Adj_Mask  cBit11_8
#define cAf6_Ec1_CV_LFE_Adj_Shift        8
#define cAf6_Ec1_ES_LFE_Adj_Index        8
#define cAf6_Ec1_ES_LFE_Adj_Mask   cBit3_0
#define cAf6_Ec1_ES_LFE_Adj_Shift        0

#define cAf6_DecCnt_GoodBytes_Index          8
#define cAf6_DecCnt_GoodBytes_Mask    cBit31_0
#define cAf6_DecCnt_GoodBytes_Shift          0

/*--------------------------------------
BitField Name: Hold08
BitField Type: R_O
BitField Desc: Missing bit is unused
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_Hold08_Data_Of_Read_DDR_Hold08_Mask                                                 cBit31_0
#define cAf6_FMPM_Hold08_Data_Of_Read_DDR_Hold08_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Hold09 Data Of Read DDR
Reg Addr   : 0x00029
Reg Formula: 
    Where  : 
Reg Desc   : 
Hold09 Data Of Read DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Hold09_Data_Of_Read_DDR                                                           0x00029

#define cAf6_Sts_UAS_NE_Sta_Index            9
#define cAf6_Sts_UAS_NE_Sta_Mask        cBit31
#define cAf6_Sts_UAS_NE_Sta_Shift           31
#define cAf6_Sts_UAS_NE_Swp_Index            9
#define cAf6_Sts_UAS_NE_Swp_Mask        cBit30
#define cAf6_Sts_UAS_NE_Swp_Shift           30
#define cAf6_Sts_UAS_NE_Adj_Index            9
#define cAf6_Sts_UAS_NE_Adj_Mask     cBit19_16
#define cAf6_Sts_UAS_NE_Adj_Shift           16
#define cAf6_Sts_UAS_NE_Cur_Index            9
#define cAf6_Sts_UAS_NE_Cur_Mask      cBit11_8
#define cAf6_Sts_UAS_NE_Cur_Shift            8

#define cAf6_Vt_UAS_NE_Sta_Index             9
#define cAf6_Vt_UAS_NE_Sta_Mask         cBit31
#define cAf6_Vt_UAS_NE_Sta_Shift            31
#define cAf6_Vt_UAS_NE_Swp_Index             9
#define cAf6_Vt_UAS_NE_Swp_Mask         cBit30
#define cAf6_Vt_UAS_NE_Swp_Shift            30
#define cAf6_Vt_UAS_NE_Adj_Index             9
#define cAf6_Vt_UAS_NE_Adj_Mask      cBit19_16
#define cAf6_Vt_UAS_NE_Adj_Shift            16
#define cAf6_Vt_UAS_NE_Cur_Index             9
#define cAf6_Vt_UAS_NE_Cur_Mask       cBit11_8
#define cAf6_Vt_UAS_NE_Cur_Shift             8

#define cAf6_Ds3_SESCP_P_Index               9
#define cAf6_Ds3_SESCP_P_Mask        cBit19_10
#define cAf6_Ds3_SESCP_P_Shift              10
#define cAf6_Ds3_UASCP_P_Index               9
#define cAf6_Ds3_UASCP_P_Mask          cBit9_0
#define cAf6_Ds3_UASCP_P_Shift               0

#define cAf6_Ds1_FECNT_Index                 9
#define cAf6_Ds1_FECNT_Mask           cBit31_0
#define cAf6_Ds1_FECNT_Shift                 0

#define cAf6_PwCnt_RX_RBIT_Index             9
#define cAf6_PwCnt_RX_RBIT_Mask       cBit31_0
#define cAf6_PwCnt_RX_RBIT_Shift             0

#define cAf6_Ec1_LFE_Invalid_Index           9
#define cAf6_Ec1_LFE_Invalid_Mask        cBit4
#define cAf6_Ec1_LFE_Invalid_Shift           4

#define cAf6_DecCnt_GoodPackets_Index          9
#define cAf6_DecCnt_GoodPackets_Mask    cBit31_0
#define cAf6_DecCnt_GoodPackets_Shift          0

/*--------------------------------------
BitField Name: Hold09
BitField Type: R_O
BitField Desc: Missing bit is unused
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_Hold09_Data_Of_Read_DDR_Hold09_Mask                                                 cBit31_0
#define cAf6_FMPM_Hold09_Data_Of_Read_DDR_Hold09_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Hold10 Data Of Read DDR
Reg Addr   : 0x0002A
Reg Formula: 
    Where  : 
Reg Desc   : 
Hold10 Data Of Read DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Hold10_Data_Of_Read_DDR                                                           0x0002A

#define cAf6_Sts_UAS_FE_Sta_Index           10
#define cAf6_Sts_UAS_FE_Sta_Mask        cBit31
#define cAf6_Sts_UAS_FE_Sta_Shift           31
#define cAf6_Sts_UAS_FE_Swp_Index           10
#define cAf6_Sts_UAS_FE_Swp_Mask        cBit30
#define cAf6_Sts_UAS_FE_Swp_Shift           30
#define cAf6_Sts_UAS_FE_Adj_Index           10
#define cAf6_Sts_UAS_FE_Adj_Mask     cBit19_16
#define cAf6_Sts_UAS_FE_Adj_Shift           16
#define cAf6_Sts_UAS_FE_Cur_Index           10
#define cAf6_Sts_UAS_FE_Cur_Mask      cBit11_8
#define cAf6_Sts_UAS_FE_Cur_Shift            8

#define cAf6_Vt_UAS_FE_Sta_Index            10
#define cAf6_Vt_UAS_FE_Sta_Mask         cBit31
#define cAf6_Vt_UAS_FE_Sta_Shift            31
#define cAf6_Vt_UAS_FE_Swp_Index            10
#define cAf6_Vt_UAS_FE_Swp_Mask         cBit30
#define cAf6_Vt_UAS_FE_Swp_Shift            30
#define cAf6_Vt_UAS_FE_Adj_Index            10
#define cAf6_Vt_UAS_FE_Adj_Mask      cBit19_16
#define cAf6_Vt_UAS_FE_Adj_Shift            16
#define cAf6_Vt_UAS_FE_Cur_Index            10
#define cAf6_Vt_UAS_FE_Cur_Mask       cBit11_8
#define cAf6_Vt_UAS_FE_Cur_Shift             8

#define cAf6_Ds3_CVCP_PFE_Index             10
#define cAf6_Ds3_CVCP_PFE_Mask        cBit23_0
#define cAf6_Ds3_CVCP_PFE_Shift              0

#define cAf6_Ds1_REICNT_Index               10
#define cAf6_Ds1_REICNT_Mask          cBit23_0
#define cAf6_Ds1_REICNT_Shift                0

#define cAf6_PwCnt_RX_NBIT_Index            10
#define cAf6_PwCnt_RX_NBIT_Mask       cBit31_0
#define cAf6_PwCnt_RX_NBIT_Shift             0

#define cAf6_Ec1_UAS_L_Index                10
#define cAf6_Ec1_UAS_L_Mask          cBit25_16
#define cAf6_Ec1_UAS_L_Shift                16
#define cAf6_Ec1_UAS_LFE_Index              10
#define cAf6_Ec1_UAS_LFE_Mask          cBit9_0
#define cAf6_Ec1_UAS_LFE_Shift               0

#define cAf6_DecCnt_Fragment_Index          10
#define cAf6_DecCnt_Fragment_Mask    cBit31_0
#define cAf6_DecCnt_Fragment_Shift          0

/*--------------------------------------
BitField Name: Hold10
BitField Type: R_O
BitField Desc: Missing bit is unused
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_Hold10_Data_Of_Read_DDR_Hold10_Mask                                                 cBit31_0
#define cAf6_FMPM_Hold10_Data_Of_Read_DDR_Hold10_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Hold11 Data Of Read DDR
Reg Addr   : 0x0002B
Reg Formula: 
    Where  : 
Reg Desc   : 
Hold11 Data Of Read DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Hold11_Data_Of_Read_DDR                                                           0x0002B

#define cAf6_Sts_CV_P_Adj_Index         11
#define cAf6_Sts_CV_P_Adj_Mask   cBit27_24
#define cAf6_Sts_CV_P_Adj_Shift         24
#define cAf6_Sts_ES_P_Adj_Index         11
#define cAf6_Sts_ES_P_Adj_Mask   cBit19_16
#define cAf6_Sts_ES_P_Adj_Shift         16
#define cAf6_Sts_CV_PFE_Adj_Index       11
#define cAf6_Sts_CV_PFE_Adj_Mask  cBit11_8
#define cAf6_Sts_CV_PFE_Adj_Shift        8
#define cAf6_Sts_ES_PFE_Adj_Index       11
#define cAf6_Sts_ES_PFE_Adj_Mask   cBit3_0
#define cAf6_Sts_ES_PFE_Adj_Shift        0

#define cAf6_Vt_CV_P_Adj_Index          11
#define cAf6_Vt_CV_P_Adj_Mask    cBit27_24
#define cAf6_Vt_CV_P_Adj_Shift          24
#define cAf6_Vt_ES_P_Adj_Index          11
#define cAf6_Vt_ES_P_Adj_Mask    cBit19_16
#define cAf6_Vt_ES_P_Adj_Shift          16
#define cAf6_Vt_CV_PFE_Adj_Index        11
#define cAf6_Vt_CV_PFE_Adj_Mask   cBit11_8
#define cAf6_Vt_CV_PFE_Adj_Shift         8
#define cAf6_Vt_ES_PFE_Adj_Index        11
#define cAf6_Vt_ES_PFE_Adj_Mask    cBit3_0
#define cAf6_Vt_ES_PFE_Adj_Shift         0

#define cAf6_Ds3_ESCP_PFE_Index         11
#define cAf6_Ds3_ESCP_PFE_Mask   cBit29_20
#define cAf6_Ds3_ESCP_PFE_Shift         20
#define cAf6_Ds3_ESACP_PFE_Index        11
#define cAf6_Ds3_ESACP_PFE_Mask  cBit19_10
#define cAf6_Ds3_ESACP_PFE_Shift        10
#define cAf6_Ds3_ESBCP_PFE_Index        11
#define cAf6_Ds3_ESBCP_PFE_Mask    cBit9_0
#define cAf6_Ds3_ESBCP_PFE_Shift         0

#define cAf6_Ds1_UAS_NE_Sta_Index       11
#define cAf6_Ds1_UAS_NE_Sta_Mask    cBit31
#define cAf6_Ds1_UAS_NE_Sta_Shift       31
#define cAf6_Ds1_UAS_NE_Swp_Index       11
#define cAf6_Ds1_UAS_NE_Swp_Mask    cBit30
#define cAf6_Ds1_UAS_NE_Swp_Shift       30
#define cAf6_Ds1_UAS_NE_Adj_Index       11
#define cAf6_Ds1_UAS_NE_Adj_Mask cBit19_16
#define cAf6_Ds1_UAS_NE_Adj_Shift       16
#define cAf6_Ds1_UAS_NE_Cur_Index       11
#define cAf6_Ds1_UAS_NE_Cur_Mask  cBit11_8
#define cAf6_Ds1_UAS_NE_Cur_Shift        8

#define cAf6_PwCnt_RX_PBIT_Index        11
#define cAf6_PwCnt_RX_PBIT_Mask   cBit31_0
#define cAf6_PwCnt_RX_PBIT_Shift         0

#define cAf6_DecCnt_Plain_Index          11
#define cAf6_DecCnt_Plain_Mask    cBit31_0
#define cAf6_DecCnt_Plain_Shift          0

/*--------------------------------------
BitField Name: Hold11
BitField Type: R_O
BitField Desc: Missing bit is unused
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_Hold11_Data_Of_Read_DDR_Hold11_Mask                                                 cBit31_0
#define cAf6_FMPM_Hold11_Data_Of_Read_DDR_Hold11_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Hold12 Data Of Read DDR
Reg Addr   : 0x0002C
Reg Formula: 
    Where  : 
Reg Desc   : 
Hold12 Data Of Read DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Hold12_Data_Of_Read_DDR                                                           0x0002C

#define cAf6_Sts_PFE_Invalid_Index          12
#define cAf6_Sts_PFE_Invalid_Mask        cBit4
#define cAf6_Sts_PFE_Invalid_Shift           4

#define cAf6_Ds3_SESCP_PFE_Index            12
#define cAf6_Ds3_SESCP_PFE_Mask      cBit29_20
#define cAf6_Ds3_SESCP_PFE_Shift            20
#define cAf6_Ds3_SASCP_PFE_Index            12
#define cAf6_Ds3_SASCP_PFE_Mask      cBit19_10
#define cAf6_Ds3_SASCP_PFE_Shift            10
#define cAf6_Ds3_UASCP_PFE_Index            12
#define cAf6_Ds3_UASCP_PFE_Mask        cBit9_0
#define cAf6_Ds3_UASCP_PFE_Shift             0

#define cAf6_Ds1_UAS_FE_Sta_Index           12
#define cAf6_Ds1_UAS_FE_Sta_Mask        cBit31
#define cAf6_Ds1_UAS_FE_Sta_Shift           31
#define cAf6_Ds1_UAS_FE_Swp_Index           12
#define cAf6_Ds1_UAS_FE_Swp_Mask        cBit30
#define cAf6_Ds1_UAS_FE_Swp_Shift           30
#define cAf6_Ds1_UAS_FE_Adj_Index           12
#define cAf6_Ds1_UAS_FE_Adj_Mask     cBit19_16
#define cAf6_Ds1_UAS_FE_Adj_Shift           16
#define cAf6_Ds1_UAS_FE_Cur_Index           12
#define cAf6_Ds1_UAS_FE_Cur_Mask      cBit11_8
#define cAf6_Ds1_UAS_FE_Cur_Shift            8

#define cAf6_PwCnt_RX_LATE_Index            12
#define cAf6_PwCnt_RX_LATE_Mask       cBit31_0
#define cAf6_PwCnt_RX_LATE_Shift             0

#define cAf6_DecCnt_Oam_Index          12
#define cAf6_DecCnt_Oam_Mask    cBit31_0
#define cAf6_DecCnt_Oam_Shift          0

/*--------------------------------------
BitField Name: Hold12
BitField Type: R_O
BitField Desc: Missing bit is unused
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_Hold12_Data_Of_Read_DDR_Hold12_Mask                                                 cBit31_0
#define cAf6_FMPM_Hold12_Data_Of_Read_DDR_Hold12_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Hold13 Data Of Read DDR
Reg Addr   : 0x0002D
Reg Formula: 
    Where  : 
Reg Desc   : 
Hold13 Data Of Read DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Hold13_Data_Of_Read_DDR                                                           0x0002D

#define cAf6_Ds3_FECNT_Index              13
#define cAf6_Ds3_FECNT_Mask         cBit23_0
#define cAf6_Ds3_FECNT_Shift               0

#define cAf6_Ds1_CV_P_Adj_Index           13
#define cAf6_Ds1_CV_P_Adj_Mask      cBit15_0
#define cAf6_Ds1_CV_P_Adj_Shift            0

#define cAf6_PwCnt_RX_EARLY_Index         13
#define cAf6_PwCnt_RX_EARLY_Mask    cBit31_0
#define cAf6_PwCnt_RX_EARLY_Shift          0

#define cAf6_DecCnt_Abort_Index          13
#define cAf6_DecCnt_Abort_Mask    cBit31_0
#define cAf6_DecCnt_Abort_Shift          0

/*--------------------------------------
BitField Name: Hold13
BitField Type: R_O
BitField Desc: Missing bit is unused
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_Hold13_Data_Of_Read_DDR_Hold13_Mask                                                 cBit31_0
#define cAf6_FMPM_Hold13_Data_Of_Read_DDR_Hold13_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Hold14 Data Of Read DDR
Reg Addr   : 0x0002E
Reg Formula: 
    Where  : 
Reg Desc   : 
Hold14 Data Of Read DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Hold14_Data_Of_Read_DDR                                                           0x0002E

#define cAf6_Ds3_UAS_NE_Sta_Index           14
#define cAf6_Ds3_UAS_NE_Sta_Mask        cBit31
#define cAf6_Ds3_UAS_NE_Sta_Shift           31
#define cAf6_Ds3_UAS_NE_Swp_Index           14
#define cAf6_Ds3_UAS_NE_Swp_Mask        cBit30
#define cAf6_Ds3_UAS_NE_Swp_Shift           30
#define cAf6_Ds3_UAS_NE_Adj_Index           14
#define cAf6_Ds3_UAS_NE_Adj_Mask     cBit19_16
#define cAf6_Ds3_UAS_NE_Adj_Shift           16
#define cAf6_Ds3_UAS_NE_Cur_Index           14
#define cAf6_Ds3_UAS_NE_Cur_Mask      cBit11_8
#define cAf6_Ds3_UAS_NE_Cur_Shift            8

#define cAf6_Ds1_ES_P_Adj_Index             14
#define cAf6_Ds1_ES_P_Adj_Mask       cBit27_24
#define cAf6_Ds1_ES_P_Adj_Shift             24
#define cAf6_Ds1_AISS_P_Adj_Index           14
#define cAf6_Ds1_AISS_P_Adj_Mask     cBit19_16
#define cAf6_Ds1_AISS_P_Adj_Shift           16
#define cAf6_Ds1_SAS_P_Adj_Index            14
#define cAf6_Ds1_SAS_P_Adj_Mask       cBit11_8
#define cAf6_Ds1_SAS_P_Adj_Shift             8
#define cAf6_Ds1_CSS_P_Adj_Index            14
#define cAf6_Ds1_CSS_P_Adj_Mask        cBit3_0
#define cAf6_Ds1_CSS_P_Adj_Shift             0

#define cAf6_PwCnt_RX_LOST_Index            14
#define cAf6_PwCnt_RX_LOST_Mask       cBit31_0
#define cAf6_PwCnt_RX_LOST_Shift             0

#define cAf6_DecCnt_Idle_Index          14
#define cAf6_DecCnt_Idle_Mask    cBit31_0
#define cAf6_DecCnt_Idle_Shift          0

/*--------------------------------------
BitField Name: Hold14
BitField Type: R_O
BitField Desc: Missing bit is unused
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_Hold14_Data_Of_Read_DDR_Hold14_Mask                                                 cBit31_0
#define cAf6_FMPM_Hold14_Data_Of_Read_DDR_Hold14_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Hold15 Data Of Read DDR
Reg Addr   : 0x0002F
Reg Formula: 
    Where  : 
Reg Desc   : 
Hold15 Data Of Read DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Hold15_Data_Of_Read_DDR                                                           0x0002F

#define cAf6_Ds3_UASCP_P_Sta_Index          15
#define cAf6_Ds3_UASCP_P_Sta_Mask       cBit31
#define cAf6_Ds3_UASCP_P_Sta_Shift          31
#define cAf6_Ds3_UASCP_P_Swp_Index          15
#define cAf6_Ds3_UASCP_P_Swp_Mask       cBit30
#define cAf6_Ds3_UASCP_P_Swp_Shift          30
#define cAf6_Ds3_UASCP_P_Adj_Index          15
#define cAf6_Ds3_UASCP_P_Adj_Mask    cBit19_16
#define cAf6_Ds3_UASCP_P_Adj_Shift          16
#define cAf6_Ds3_UASCP_P_Cur_Index          15
#define cAf6_Ds3_UASCP_P_Cur_Mask     cBit11_8
#define cAf6_Ds3_UASCP_P_Cur_Shift           8

#define cAf6_Ds1_SEFS_PFE_Adj_Index         15
#define cAf6_Ds1_SEFS_PFE_Adj_Mask   cBit19_16
#define cAf6_Ds1_SEFS_PFE_Adj_Shift         16
#define cAf6_Ds1_ES_PFE_Adj_Index           15
#define cAf6_Ds1_ES_PFE_Adj_Mask      cBit11_8
#define cAf6_Ds1_ES_PFE_Adj_Shift            8
#define cAf6_Ds1_CSS_PFE_Adj_Index          15
#define cAf6_Ds1_CSS_PFE_Adj_Mask      cBit3_0
#define cAf6_Ds1_CSS_PFE_Adj_Shift           0

#define cAf6_PwCnt_RX_LOPSYN_Index          15
#define cAf6_PwCnt_RX_LOPSYN_Mask     cBit31_0
#define cAf6_PwCnt_RX_LOPSYN_Shift           0

#define cAf6_DecCnt_FcsError_Index          15
#define cAf6_DecCnt_FcsError_Mask    cBit31_0
#define cAf6_DecCnt_FcsError_Shift          0

/*--------------------------------------
BitField Name: Hold15
BitField Type: R_O
BitField Desc: Missing bit is unused
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_Hold15_Data_Of_Read_DDR_Hold15_Mask                                                 cBit31_0
#define cAf6_FMPM_Hold15_Data_Of_Read_DDR_Hold15_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Hold16 Data Of Read DDR
Reg Addr   : 0x00030
Reg Formula: 
    Where  : 
Reg Desc   : 
Hold16 Data Of Read DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Hold16_Data_Of_Read_DDR                                                           0x00030

#define cAf6_Ds3_UASCP_PFE_Sta_Index           16
#define cAf6_Ds3_UASCP_PFE_Sta_Mask        cBit31
#define cAf6_Ds3_UASCP_PFE_Sta_Shift           31
#define cAf6_Ds3_UASCP_PFE_Swp_Index           16
#define cAf6_Ds3_UASCP_PFE_Swp_Mask        cBit30
#define cAf6_Ds3_UASCP_PFE_Swp_Shift           30
#define cAf6_Ds3_UASCP_PFE_Adj_Index           16
#define cAf6_Ds3_UASCP_PFE_Adj_Mask     cBit19_16
#define cAf6_Ds3_UASCP_PFE_Adj_Shift           16
#define cAf6_Ds3_UASCP_PFE_Cur_Index           16
#define cAf6_Ds3_UASCP_PFE_Cur_Mask      cBit11_8
#define cAf6_Ds3_UASCP_PFE_Cur_Shift            8

#define cAf6_Ds1_LFE_Invalid_Index             16
#define cAf6_Ds1_LFE_Invalid_Mask           cBit4
#define cAf6_Ds1_LFE_Invalid_Shift              4
#define cAf6_Ds1_PFE_Invalid_Index             16
#define cAf6_Ds1_PFE_Invalid_Mask           cBit3
#define cAf6_Ds1_PFE_Invalid_Shift              3

#define cAf6_PwCnt_RX_LOPSTA_Index             16
#define cAf6_PwCnt_RX_LOPSTA_Mask        cBit31_0
#define cAf6_PwCnt_RX_LOPSTA_Shift              0

#define cAf6_DecCnt_DiscardPkt_Index          16
#define cAf6_DecCnt_DiscardPkt_Mask    cBit31_0
#define cAf6_DecCnt_DiscardPkt_Shift          0

/*--------------------------------------
BitField Name: Hold16
BitField Type: R_O
BitField Desc: Missing bit is unused
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_Hold16_Data_Of_Read_DDR_Hold16_Mask                                                 cBit31_0
#define cAf6_FMPM_Hold16_Data_Of_Read_DDR_Hold16_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Hold17 Data Of Read DDR
Reg Addr   : 0x00031
Reg Formula: 
    Where  : 
Reg Desc   : 
Hold17 Data Of Read DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Hold17_Data_Of_Read_DDR                                                           0x00031

#define cAf6_Ds3_CV_P_Adj_Index         17
#define cAf6_Ds3_CV_P_Adj_Mask    cBit19_0
#define cAf6_Ds3_CV_P_Adj_Shift          0

#define cAf6_PwCnt_RX_OVERUN_Index      17
#define cAf6_PwCnt_RX_OVERUN_Mask cBit31_0
#define cAf6_PwCnt_RX_OVERUN_Shift       0

#define cAf6_DecCnt_Mru_Index          17
#define cAf6_DecCnt_Mru_Mask    cBit31_0
#define cAf6_DecCnt_Mru_Shift          0

/*--------------------------------------
BitField Name: Hold17
BitField Type: R_O
BitField Desc: Missing bit is unused
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_Hold17_Data_Of_Read_DDR_Hold17_Mask                                                 cBit31_0
#define cAf6_FMPM_Hold17_Data_Of_Read_DDR_Hold17_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Hold18 Data Of Read DDR
Reg Addr   : 0x00032
Reg Formula: 
    Where  : 
Reg Desc   : 
Hold18 Data Of Read DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Hold18_Data_Of_Read_DDR                                                           0x00032

#define cAf6_Ds3_CVCP_P_Adj_Index         18
#define cAf6_Ds3_CVCP_P_Adj_Mask    cBit19_0
#define cAf6_Ds3_CVCP_P_Adj_Shift          0

#define cAf6_PwCnt_RX_UNDERUN_Index       18
#define cAf6_PwCnt_RX_UNDERUN_Mask  cBit31_0
#define cAf6_PwCnt_RX_UNDERUN_Shift        0

/*--------------------------------------
BitField Name: Hold18
BitField Type: R_O
BitField Desc: Missing bit is unused
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_Hold18_Data_Of_Read_DDR_Hold18_Mask                                                 cBit31_0
#define cAf6_FMPM_Hold18_Data_Of_Read_DDR_Hold18_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Hold19 Data Of Read DDR
Reg Addr   : 0x00033
Reg Formula: 
    Where  : 
Reg Desc   : 
Hold19 Data Of Read DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Hold19_Data_Of_Read_DDR                                                           0x00033

#define cAf6_Ds3_ESP_P_Adj_Index          19
#define cAf6_Ds3_ESP_P_Adj_Mask    cBit27_24
#define cAf6_Ds3_ESP_P_Adj_Shift          24
#define cAf6_Ds3_ESCP_P_Adj_Index         19
#define cAf6_Ds3_ESCP_P_Adj_Mask   cBit19_16
#define cAf6_Ds3_ESCP_P_Adj_Shift         16
#define cAf6_Ds3_ESAP_P_Adj_Index         19
#define cAf6_Ds3_ESAP_P_Adj_Mask    cBit11_8
#define cAf6_Ds3_ESAP_P_Adj_Shift          8
#define cAf6_Ds3_ESACP_P_Adj_Index        19
#define cAf6_Ds3_ESACP_P_Adj_Mask    cBit3_0
#define cAf6_Ds3_ESACP_P_Adj_Shift         0

#define cAf6_PwCnt_TX_Packet_Index        19
#define cAf6_PwCnt_TX_Packet_Mask   cBit31_0
#define cAf6_PwCnt_TX_Packet_Shift         0

/*--------------------------------------
BitField Name: Hold19
BitField Type: R_O
BitField Desc: Missing bit is unused
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_Hold19_Data_Of_Read_DDR_Hold19_Mask                                                 cBit31_0
#define cAf6_FMPM_Hold19_Data_Of_Read_DDR_Hold19_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Hold20 Data Of Read DDR
Reg Addr   : 0x00034
Reg Formula: 
    Where  : 
Reg Desc   : 
Hold20 Data Of Read DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Hold20_Data_Of_Read_DDR                                                           0x00034

#define cAf6_Ds3_ESBP_P_Adj_Index         20
#define cAf6_Ds3_ESBP_P_Adj_Mask   cBit19_16
#define cAf6_Ds3_ESBP_P_Adj_Shift         16
#define cAf6_Ds3_ESBCP_P_Adj_Index        20
#define cAf6_Ds3_ESBCP_P_Adj_Mask   cBit11_8
#define cAf6_Ds3_ESBCP_P_Adj_Shift         8
#define cAf6_Ds3_SESP_P_Adj_Index         20
#define cAf6_Ds3_SESP_P_Adj_Mask     cBit3_0
#define cAf6_Ds3_SESP_P_Adj_Shift          0

#define cAf6_PwCnt_RX_Packet_Index        20
#define cAf6_PwCnt_RX_Packet_Mask   cBit31_0
#define cAf6_PwCnt_RX_Packet_Shift         0

/*--------------------------------------
BitField Name: Hold20
BitField Type: R_O
BitField Desc: Missing bit is unused
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_Hold20_Data_Of_Read_DDR_Hold20_Mask                                                 cBit31_0
#define cAf6_FMPM_Hold20_Data_Of_Read_DDR_Hold20_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Hold21 Data Of Read DDR
Reg Addr   : 0x00035
Reg Formula: 
    Where  : 
Reg Desc   : 
Hold21 Data Of Read DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Hold21_Data_Of_Read_DDR                                                           0x00035

#define cAf6_Ds3_SESCP_P_Adj_Index        21
#define cAf6_Ds3_SESCP_P_Adj_Mask  cBit27_24
#define cAf6_Ds3_SESCP_P_Adj_Shift        24
#define cAf6_Ds3_SAS_P_Adj_Index          21
#define cAf6_Ds3_SAS_P_Adj_Mask    cBit19_16
#define cAf6_Ds3_SAS_P_Adj_Shift          16
#define cAf6_Ds3_AISS_P_Adj_Index         21
#define cAf6_Ds3_AISS_P_Adj_Mask    cBit11_8
#define cAf6_Ds3_AISS_P_Adj_Shift          8

/*--------------------------------------
BitField Name: Hold21
BitField Type: R_O
BitField Desc: Missing bit is unused
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_Hold21_Data_Of_Read_DDR_Hold21_Mask                                                 cBit31_0
#define cAf6_FMPM_Hold21_Data_Of_Read_DDR_Hold21_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Hold22 Data Of Read DDR
Reg Addr   : 0x00036
Reg Formula: 
    Where  : 
Reg Desc   : 
Hold22 Data Of Read DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Hold22_Data_Of_Read_DDR                                                           0x00036

#define cAf6_Ds3_CVCP_PFE_Adj_Index        22
#define cAf6_Ds3_CVCP_PFE_Adj_Mask   cBit19_0
#define cAf6_Ds3_CVCP_PFE_Adj_Shift         0

/*--------------------------------------
BitField Name: Hold22
BitField Type: R_O
BitField Desc: Missing bit is unused
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_Hold22_Data_Of_Read_DDR_Hold22_Mask                                                 cBit31_0
#define cAf6_FMPM_Hold22_Data_Of_Read_DDR_Hold22_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Hold23 Data Of Read DDR
Reg Addr   : 0x00037
Reg Formula: 
    Where  : 
Reg Desc   : 
Hold23 Data Of Read DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Hold23_Data_Of_Read_DDR                                                           0x00037

#define cAf6_Ds3_ESCP_PFE_Adj_Index          23
#define cAf6_Ds3_ESCP_PFE_Adj_Mask    cBit27_24
#define cAf6_Ds3_ESCP_PFE_Adj_Shift          24
#define cAf6_Ds3_ESACP_PFE_Adj_Index         23
#define cAf6_Ds3_ESACP_PFE_Adj_Mask   cBit19_16
#define cAf6_Ds3_ESACP_PFE_Adj_Shift         16
#define cAf6_Ds3_ESBCP_PFE_Adj_Index         23
#define cAf6_Ds3_ESBCP_PFE_Adj_Mask    cBit11_8
#define cAf6_Ds3_ESBCP_PFE_Adj_Shift          8
#define cAf6_Ds3_SASCP_PFE_Adj_Index         23
#define cAf6_Ds3_SASCP_PFE_Adj_Mask     cBit3_0
#define cAf6_Ds3_SASCP_PFE_Adj_Shift          0

/*--------------------------------------
BitField Name: Hold23
BitField Type: R_O
BitField Desc: Missing bit is unused
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_Hold23_Data_Of_Read_DDR_Hold23_Mask                                                 cBit31_0
#define cAf6_FMPM_Hold23_Data_Of_Read_DDR_Hold23_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Hold24 Data Of Read DDR
Reg Addr   : 0x00038
Reg Formula: 
    Where  : 
Reg Desc   : 
Hold24 Data Of Read DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Hold24_Data_Of_Read_DDR                                                           0x00038

#define cAf6_Ds3_PFE_Invalid_Index             24
#define cAf6_Ds3_PFE_Invalid_Mask           cBit4
#define cAf6_Ds3_PFE_Invalid_Shift              4

/*--------------------------------------
BitField Name: Hold24
BitField Type: R_O
BitField Desc: Missing bit is unused
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_Hold24_Data_Of_Read_DDR_Hold24_Mask                                                 cBit31_0
#define cAf6_FMPM_Hold24_Data_Of_Read_DDR_Hold24_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM K threshold of Section
Reg Addr   : 0x000D0-0x000D7
Reg Formula: 0x000D0+$id
    Where  : 
           + $id(0-7) : Type ID
Reg Desc   : 
K threshold of Section and Line

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_K_threshold_of_Section_Base                                                       0x000D0
#define cAf6Reg_FMPM_K_threshold_of_Section(id)                                                 (0x000D0+(id))

/*--------------------------------------
BitField Name: line_kval
BitField Type: R/W
BitField Desc: K threshold of Line
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_FMPM_K_threshold_of_Section_line_kval_Mask                                              cBit31_16
#define cAf6_FMPM_K_threshold_of_Section_line_kval_Shift                                                    16

/*--------------------------------------
BitField Name: section_kval
BitField Type: R/W
BitField Desc: K threshold of Section
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_FMPM_K_threshold_of_Section_section_kval_Mask                                            cBit15_0
#define cAf6_FMPM_K_threshold_of_Section_section_kval_Shift                                                  0


/*------------------------------------------------------------------------------
Reg Name   : FMPM K threshold of STS
Reg Addr   : 0x00080-0x00087
Reg Formula: 0x00080+$id
    Where  : 
           + $id(0-7) : Type ID
Reg Desc   : 
K threshold of STS

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_K_threshold_of_STS_Base                                                           0x00080
#define cAf6Reg_FMPM_K_threshold_of_STS(id)                                                     (0x00080+(id))

/*--------------------------------------
BitField Name: kval
BitField Type: R/W
BitField Desc: K threshold of STS
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_FMPM_K_threshold_of_STS_kval_Mask                                                        cBit15_0
#define cAf6_FMPM_K_threshold_of_STS_kval_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : FMPM K Threshold of VT
Reg Addr   : 0x00090-0x00097
Reg Formula: 0x00090+$id
    Where  : 
           + $id(0-7) : Type ID
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_K_Threshold_of_VT_Base                                                            0x00090
#define cAf6Reg_FMPM_K_Threshold_of_VT(id)                                                      (0x00090+(id))

/*--------------------------------------
BitField Name: kval
BitField Type: R/W
BitField Desc: K threshold of VT
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_FMPM_K_Threshold_of_VT_kval_Mask                                                         cBit15_0
#define cAf6_FMPM_K_Threshold_of_VT_kval_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : FMPM K Threshold of DS3
Reg Addr   : 0x000A0-0x000A7
Reg Formula: 0x000A0+$id
    Where  : 
           + $id(0-7) : Type ID
Reg Desc   : 
K threshold of DS3

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_K_Threshold_of_DS3_Base                                                           0x000A0
#define cAf6Reg_FMPM_K_Threshold_of_DS3(id)                                                     (0x000A0+(id))

/*--------------------------------------
BitField Name: line_kval
BitField Type: R/W
BitField Desc: K threshold of Line DS3
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_FMPM_K_Threshold_of_DS3_line_kval_Mask                                                  cBit31_16
#define cAf6_FMPM_K_Threshold_of_DS3_line_kval_Shift                                                        16

/*--------------------------------------
BitField Name: path_kval
BitField Type: R/W
BitField Desc: K threshold of Path DS3
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_FMPM_K_Threshold_of_DS3_path_kval_Mask                                                   cBit15_0
#define cAf6_FMPM_K_Threshold_of_DS3_path_kval_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : FMPM K Threshold of DS1
Reg Addr   : 0x000B0-0x000B7
Reg Formula: 0x000B0+$id
    Where  : 
           + $id(0-7) : Type ID
Reg Desc   : 
K threshold of DS1

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_K_Threshold_of_DS1_Base                                                           0x000B0
#define cAf6Reg_FMPM_K_Threshold_of_DS1(id)                                                     (0x000B0+(id))

/*--------------------------------------
BitField Name: line_kval
BitField Type: R/W
BitField Desc: K threshold of Line DS1
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_FMPM_K_Threshold_of_DS1_line_kval_Mask                                                  cBit31_16
#define cAf6_FMPM_K_Threshold_of_DS1_line_kval_Shift                                                        16

/*--------------------------------------
BitField Name: path_kval
BitField Type: R/W
BitField Desc: K threshold of Path DS1
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_FMPM_K_Threshold_of_DS1_path_kval_Mask                                                   cBit15_0
#define cAf6_FMPM_K_Threshold_of_DS1_path_kval_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : FMPM K Threshold of PW
Reg Addr   : 0x000C0-0x000C7
Reg Formula: 0x000C0+$id
    Where  : 
           + $id(0-7) : Type ID
Reg Desc   : 
K threshold of PW

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_K_Threshold_of_PW_Base                                                            0x000C0
#define cAf6Reg_FMPM_K_Threshold_of_PW(id)                                                      (0x000C0+(id))

/*--------------------------------------
BitField Name: kval
BitField Type: R/W
BitField Desc: K threshold of PW
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_FMPM_K_Threshold_of_PW_kval_Mask                                                         cBit15_0
#define cAf6_FMPM_K_Threshold_of_PW_kval_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : FMPM HO Counter Mode
Reg Addr   : 0x00006
Reg Formula: 
    Where  : 
Reg Desc   : 
HO Counter Mode, using for HO TX & RX

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_HO_Counter_Mode                                                                   0x00006

/*--------------------------------------
BitField Name: ho_mod
BitField Type: R/W
BitField Desc: HO counter mode
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_HO_Counter_Mode_ho_mod_Mask                                                            cBit0
#define cAf6_FMPM_HO_Counter_Mode_ho_mod_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : FMPM HO Counter Of RX PW
Reg Addr   : 0x0E000-0x0FFFF
Reg Formula: 0x0E000+$L*0x400+$M*0x200+$N*0x10+$rtype
    Where  : 
           + $L(0-7) : is the TFI-5 line identifier
           + $M(0-1) : is 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s  (belong to that TFI-5 line)
           + $N(0-23) : STS-1s in a STS-24 group
           + $rtype (0-14) : Counter Type
Reg Desc   : 
HO Counter Of RX PW, counter mode is depended on FMPM HO Counter Mode register

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_HO_Counter_Of_RX_PW_Base                                                          0x0E000
#define cAf6Reg_FMPM_HO_Counter_Of_RX_PW(L, M, N, rtype)              (0x0E000+(L)*0x400+(M)*0x200+(N)*0x10+(rtype))

#define cAf6Reg_rho_pen_cnt_Malf        0x0
#define cAf6Reg_rho_pen_cnt_Stray       0x1
#define cAf6Reg_rho_pen_cnt_Lbit        0x2
#define cAf6Reg_rho_pen_cnt_Underrun    0x3
#define cAf6Reg_rho_pen_cnt_Pbit        0x4
#define cAf6Reg_rho_pen_cnt_Nbit        0x5
#define cAf6Reg_rho_pen_cnt_Rbit        0x6
#define cAf6Reg_rho_pen_cnt_Bytes       0x7
#define cAf6Reg_rho_pen_cnt_Lost        0x8
#define cAf6Reg_rho_pen_cnt_Early       0x9
#define cAf6Reg_rho_pen_cnt_Late        0xA
#define cAf6Reg_rho_pen_cnt_Packet      0xB
#define cAf6Reg_rho_pen_cnt_Overrun     0xC
#define cAf6Reg_rho_pen_cnt_Lopsta      0xD
#define cAf6Reg_rho_pen_cnt_Lops        0xE

/*--------------------------------------
BitField Name: rcnt_type
BitField Type: RO
BitField Desc: Missing value is unused Type counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_HO_Counter_Of_RX_PW_rcnt_type_Mask                                                  cBit31_0
#define cAf6_FMPM_HO_Counter_Of_RX_PW_rcnt_type_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : FMPM HO Counter Of TX PW
Reg Addr   : 0x10000-0x11FFE
Reg Formula: 0x10000+$L*0x400+$M*0x200+$N*0x10+$ttype*0x2
    Where  : 
           + $L(0-7) : is the TFI-5 line identifier
           + $M(0-1) : is 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s  (belong to that TFI-5 line)
           + $N(0-23) : STS-1s in a STS-24 group
           + $ttype (0-4) : Counter Type
Reg Desc   : 
HO Counter Of TX PW, counter mode is depended on FMPM HO Counter Mode register

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_HO_Counter_Of_TX_PW_Base                                                          0x10000
#define cAf6Reg_FMPM_HO_Counter_Of_TX_PW(L, M, N, ttype)              (0x10000+(L)*0x400+(M)*0x200+(N)*0x10+(ttype)*0x2)

#define cAf6Reg_tho_pen_cnt_Lbit        0x0UL
#define cAf6Reg_tho_pen_cnt_Nbit        0x1UL
#define cAf6Reg_tho_pen_cnt_Pbit        0x2UL
#define cAf6Reg_tho_pen_cnt_Rbit        0x3UL
#define cAf6Reg_tho_pen_cnt_Bytes       0x4UL
#define cAf6Reg_tho_pen_cnt_Packets     0x5UL

/*--------------------------------------
BitField Name: tcnt_type
BitField Type: RO
BitField Desc: Missing value is unused Type counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_HO_Counter_Of_TX_PW_tcnt_type_Mask                                                  cBit31_0
#define cAf6_FMPM_HO_Counter_Of_TX_PW_tcnt_type_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : FMPM HO Counter Of ENC
Reg Addr   : 0x13000-0x137FE
Reg Formula: 0x13000+$L*0x400+$M*0x200+$N*0x10+$enctype*0x2
    Where  :
           + $L(0-1) : is the TFI-5 line identifier
           + $M(0-1) : is 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s  (belong to that TFI-5 line)
           + $N(0-23) : STS-1s in a STS-24 group
           + $enctype (0-6) : Counter Type
Reg Desc   :
HO Counter Of ENC, counter mode is depended on FMPM HO Counter Mode register

------------------------------------------------------------------------------*/
#define cAf6_FMPM_HO_Counter_Of_ENC_Base                                                            0x13000
#define cAf6_FMPM_HO_Counter_Of_ENC(L, M, N, enctype)              (0x13000UL+(L)*0x400UL+(M)*0x2UL00UL+(N)*0x10UL+(enctype)*0x2UL)

/* Encap counter type */
#define cAf6_FMPM_HO_Counter_Of_ENC_type_Abort      0x0UL
#define cAf6_FMPM_HO_Counter_Of_ENC_type_Oam        0x1UL
#define cAf6_FMPM_HO_Counter_Of_ENC_type_Plain      0x2UL
#define cAf6_FMPM_HO_Counter_Of_ENC_type_Fragment   0x3UL
#define cAf6_FMPM_HO_Counter_Of_ENC_type_Idle       0x4UL
#define cAf6_FMPM_HO_Counter_Of_ENC_type_GoodPkt    0x5UL
#define cAf6_FMPM_HO_Counter_Of_ENC_type_Byte       0x6UL

/*--------------------------------------
BitField Name: enctype
BitField Type: RO
BitField Desc: Missing value is unused Counter types
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_HO_Counter_Of_ENC_enctype_Mask                                                      cBit31_0
#define cAf6_FMPM_HO_Counter_Of_ENC_enctype_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : FMPM HO Counter Of DEC
Reg Addr   : 0x13800-0x13FFF
Reg Formula: 0x13800+$L*0x400+$M*0x200+$N*0x10+$dectype
    Where  :
           + $L(0-1) : is the TFI-5 line identifier
           + $M(0-1) : is 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s  (belong to that TFI-5 line)
           + $N(0-23) : STS-1s in a STS-24 group
           + $dectype (0-10) : Counter Type
Reg Desc   :
HO Counter Of RX PW, counter mode is depended on FMPM HO Counter Mode register

------------------------------------------------------------------------------*/
#define cAf6_FMPM_HO_Counter_Of_DEC_Base                                                            0x13800
#define cAf6_FMPM_HO_Counter_Of_DEC(L, M, N, dectype)              (0x13800UL+(L)*0x400UL+(M)*0x200UL+(N)*0x10UL+(dectype))

/* Decap counter type */
#define cAf6_FMPM_HO_Counter_Of_DEC_type_TotalByte      0x0UL
#define cAf6_FMPM_HO_Counter_Of_DEC_type_GoodByte       0x1UL
#define cAf6_FMPM_HO_Counter_Of_DEC_type_GoodPkt        0x2UL
#define cAf6_FMPM_HO_Counter_Of_DEC_type_Fragment       0x3UL
#define cAf6_FMPM_HO_Counter_Of_DEC_type_Plain          0x4UL
#define cAf6_FMPM_HO_Counter_Of_DEC_type_Oam            0x5UL
#define cAf6_FMPM_HO_Counter_Of_DEC_type_Abort          0x6UL
#define cAf6_FMPM_HO_Counter_Of_DEC_type_Idle           0x7UL
#define cAf6_FMPM_HO_Counter_Of_DEC_type_FcsError       0x8UL
#define cAf6_FMPM_HO_Counter_Of_DEC_type_Discard        0x9UL
#define cAf6_FMPM_HO_Counter_Of_DEC_type_Mru            0xAUL

/*--------------------------------------
BitField Name: dectype
BitField Type: RO
BitField Desc: Missing value is unused Counter types
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_HO_Counter_Of_DEC_dectype_Mask                                                      cBit31_0
#define cAf6_FMPM_HO_Counter_Of_DEC_dectype_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : FMPM MPEG Counter
Reg Addr   : 0xC0000-0xCFFFF
Reg Formula: 0xC0000+$chid*0x10+$mpeg_typ
    Where  :
           + $chid(0-4095) : channel id
           + $mpeg_typ(0-8) : counter types
Reg Desc   :
MPEG counter, counter mode is depended on FMPM HO Counter Mode register

------------------------------------------------------------------------------*/
#define cAf6_FMPM_MPEG_Counter_Base                                                                 0xC0000
#define cAf6_FMPM_MPEG_Counter(chid, mpegtyp)                           (0xC0000UL+(chid)*0x10UL+(mpegtyp))

/*--------------------------------------
BitField Name: mpegcnt
BitField Type: RO
BitField Desc: MPEG counters Counter types
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_MPEG_Counter_mpegcnt_Mask                                                           cBit31_0
#define cAf6_FMPM_MPEG_Counter_mpegcnt_Shift                                                                 0

#define cAf6_FMPM_PPP_CounterType_Fragment        0x0UL
#define cAf6_FMPM_PPP_CounterType_DiscardPacket   0x1UL
#define cAf6_FMPM_PPP_CounterType_Fecn            0x2UL
#define cAf6_FMPM_PPP_CounterType_Becn            0x3UL
#define cAf6_FMPM_PPP_CounterType_De              0x4UL
#define cAf6_FMPM_PPP_CounterType_Mtu             0x5UL
#define cAf6_FMPM_PPP_CounterType_TotalByte       0x6UL
#define cAf6_FMPM_PPP_CounterType_GoodPacket      0x7UL
#define cAf6_FMPM_PPP_CounterType_DiscardByte     0x8UL


/*------------------------------------------------------------------------------
Reg Name   : FMPM MPIG Counter
Reg Addr   : 0xD0000-0xDFFFF
Reg Formula: 0xD0000+$chid*0x10+$mpeg_typ
    Where  :
           + $chid(0-4095) : channel id
           + $mpig_typ(0-8) : counter types
Reg Desc   :
MPIG counter, counter mode is depended on FMPM HO Counter Mode register

------------------------------------------------------------------------------*/
#define cAf6_FMPM_MPIG_Counter_Base                                                                 0xD0000
#define cAf6_FMPM_MPIG_Counter(chid, mpigtyp)                             (0xD0000UL+(chid)*0x10UL+mpegtyp)

/*--------------------------------------
BitField Name: mpigcnt
BitField Type: RO
BitField Desc: MPIG counters Counter types
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_MPIG_Counter_mpigcnt_Mask                                                           cBit31_0
#define cAf6_FMPM_MPIG_Counter_mpigcnt_Shift                                                                 0

#define cAf6_FMPM_MPIG_MPEG_CounterType_TotalByte       0x6UL
#define cAf6_FMPM_MPIG_MPEG_CounterType_GoodPacket      0x7UL
#define cAf6_FMPM_MPIG_MPEG_CounterType_DiscardPkt      0x1UL

/*------------------------------------------------------------------------------
Reg Name   : FMPM TX-Bundle Counter
Reg Addr   : 0x01000-0x017FF
Reg Formula: 0x01000+$bndid*0x4+$txbndtype
    Where  :
           + $bndid(0-511) : is bundle id
           + $txbndtype(0-2) : is Counter Type
Reg Desc   :
TX-Bundle Counter, counter mode is depended on FMPM HO Counter Mode register

------------------------------------------------------------------------------*/
#define cAf6_FMPM_TX_Bundle_Counter_Base                                                            0x01000
#define cAf6_FMPM_TX_Bundle_Counter(bndid, txbndtype)                 (0x01000UL+(bndid)*0x4UL+(txbndtype))

/*--------------------------------------
BitField Name: txbndtype
BitField Type: RO
BitField Desc: Missing value is unused Counter types
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_TX_Bundle_Counter_txbndtype_Mask                                                    cBit31_0
#define cAf6_FMPM_TX_Bundle_Counter_txbndtype_Shift                                                          0

#define cAf6_FMPM_Bundle_Counter_Type_GoodByte    0x0UL
#define cAf6_FMPM_Bundle_Counter_Type_GoodPkt     0x1UL
#define cAf6_FMPM_Bundle_Counter_Type_DiscardPkt  0x2UL
#define cAf6_FMPM_Bundle_Counter_Type_FragmentPkt 0x3UL

/*------------------------------------------------------------------------------
Reg Name   : FMPM RX-Bundle Counter
Reg Addr   : 0x01800-0x01FFF
Reg Formula: 0x01800+$bndid*0x4+$rxbndtype
    Where  :
           + $bndid(0-511) : is bundle id
           + $rxbndtype(0-2) : is Counter Type
Reg Desc   :
RX-Bundle Counter, counter mode is depended on FMPM HO Counter Mode register

------------------------------------------------------------------------------*/
#define cAf6_FMPM_RX_Bundle_Counter_Base                                                            0x01800
#define cAf6_FMPM_RX_Bundle_Counter(bndid, rxbndtype)                 (0x01800UL+(bndid)*0x4UL+(rxbndtype))

/*--------------------------------------
BitField Name: rxbndtype
BitField Type: RO
BitField Desc: Missing value is unused Counter types
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_RX_Bundle_Counter_rxbndtype_Mask                                                    cBit31_0
#define cAf6_FMPM_RX_Bundle_Counter_rxbndtype_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of Section Part0
Reg Addr   : 0xB0200-0xB020E
Reg Formula: 0xB0200+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part0 of Section and Line

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_Section_Part0_Base                                               0xB0200
#define cAf6Reg_FMPM_TCA_threshold_of_Section_Part0(id)                                     (0xB0200+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_sec_cv_s
BitField Type: R/W
BitField Desc: TCA threshold of CV_S
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_Section_Part0_tca_thres_sec_cv_s_Mask                                cBit23_0
#define cAf6_FMPM_TCA_threshold_of_Section_Part0_tca_thres_sec_cv_s_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of Section Part1
Reg Addr   : 0xB0210-0xB021E
Reg Formula: 0xB0210+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part1 of Section and Line

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_Section_Part1_Base                                               0xB0210
#define cAf6Reg_FMPM_TCA_threshold_of_Section_Part1(id)                                     (0xB0210+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_sec_sefs_s
BitField Type: R/W
BitField Desc: TCA threshold of SEFS_S
BitField Bits: [29:20]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_Section_Part1_tca_thres_sec_sefs_s_Mask                               cBit29_20
#define cAf6_FMPM_TCA_threshold_of_Section_Part1_tca_thres_sec_sefs_s_Shift                                      20

/*--------------------------------------
BitField Name: tca_thres_sec_es_s
BitField Type: R/W
BitField Desc: TCA threshold of ES_S
BitField Bits: [19:10]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_Section_Part1_tca_thres_sec_es_s_Mask                               cBit19_10
#define cAf6_FMPM_TCA_threshold_of_Section_Part1_tca_thres_sec_es_s_Shift                                      10

/*--------------------------------------
BitField Name: tca_thres_sec_ses_s
BitField Type: R/W
BitField Desc: TCA threshold of SES_S
BitField Bits: [09:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_Section_Part1_tca_thres_sec_ses_s_Mask                                 cBit9_0
#define cAf6_FMPM_TCA_threshold_of_Section_Part1_tca_thres_sec_ses_s_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of Section Part2
Reg Addr   : 0xB0220-0xB022E
Reg Formula: 0xB0220+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part2 of Section and Line

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_Section_Part2_Base                                               0xB0220
#define cAf6Reg_FMPM_TCA_threshold_of_Section_Part2(id)                                     (0xB0220+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_sec_cv_l
BitField Type: R/W
BitField Desc: TCA threshold of CV_L
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_Section_Part2_tca_thres_sec_cv_l_Mask                                cBit23_0
#define cAf6_FMPM_TCA_threshold_of_Section_Part2_tca_thres_sec_cv_l_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of Section Part3
Reg Addr   : 0xB0230-0xB023E
Reg Formula: 0xB0230+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part3 of Section and Line

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_Section_Part3_Base                                               0xB0230
#define cAf6Reg_FMPM_TCA_threshold_of_Section_Part3(id)                                     (0xB0230+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_sec_es_l
BitField Type: R/W
BitField Desc: TCA threshold of ES_L
BitField Bits: [29:20]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_Section_Part3_tca_thres_sec_es_l_Mask                               cBit29_20
#define cAf6_FMPM_TCA_threshold_of_Section_Part3_tca_thres_sec_es_l_Shift                                      20

/*--------------------------------------
BitField Name: tca_thres_sec_ses_l
BitField Type: R/W
BitField Desc: TCA threshold of SES_L
BitField Bits: [19:10]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_Section_Part3_tca_thres_sec_ses_l_Mask                               cBit19_10
#define cAf6_FMPM_TCA_threshold_of_Section_Part3_tca_thres_sec_ses_l_Shift                                      10

/*--------------------------------------
BitField Name: tca_thres_sec_uas_l
BitField Type: R/W
BitField Desc: TCA threshold of UAS_L
BitField Bits: [09:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_Section_Part3_tca_thres_sec_uas_l_Mask                                 cBit9_0
#define cAf6_FMPM_TCA_threshold_of_Section_Part3_tca_thres_sec_uas_l_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of Section Part4
Reg Addr   : 0xB0240-0xB024E
Reg Formula: 0xB0240+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part4 of Section and Line

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_Section_Part4_Base                                               0xB0240
#define cAf6Reg_FMPM_TCA_threshold_of_Section_Part4(id)                                     (0xB0240+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_sec_cv_lfe
BitField Type: R/W
BitField Desc: TCA threshold of CV_LFE
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_Section_Part4_tca_thres_sec_cv_lfe_Mask                                cBit23_0
#define cAf6_FMPM_TCA_threshold_of_Section_Part4_tca_thres_sec_cv_lfe_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of Section Part5
Reg Addr   : 0xB0250-0xB025E
Reg Formula: 0xB0250+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part5 of Section and Line

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_Section_Part5_Base                                               0xB0250
#define cAf6Reg_FMPM_TCA_threshold_of_Section_Part5(id)                                     (0xB0250+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_sec_fc_l
BitField Type: R/W
BitField Desc: TCA threshold of FC_L
BitField Bits: [29:20]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_Section_Part5_tca_thres_sec_fc_l_Mask                               cBit29_20
#define cAf6_FMPM_TCA_threshold_of_Section_Part5_tca_thres_sec_fc_l_Shift                                      20

/*--------------------------------------
BitField Name: tca_thres_sec_es_lfe
BitField Type: R/W
BitField Desc: TCA threshold of ES_LFE
BitField Bits: [19:10]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_Section_Part5_tca_thres_sec_es_lfe_Mask                               cBit19_10
#define cAf6_FMPM_TCA_threshold_of_Section_Part5_tca_thres_sec_es_lfe_Shift                                      10

/*--------------------------------------
BitField Name: tca_thres_sec_ses_lfe
BitField Type: R/W
BitField Desc: TCA threshold of SES_LFE
BitField Bits: [09:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_Section_Part5_tca_thres_sec_ses_lfe_Mask                                 cBit9_0
#define cAf6_FMPM_TCA_threshold_of_Section_Part5_tca_thres_sec_ses_lfe_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of Section Part6
Reg Addr   : 0xB0260-0xB026E
Reg Formula: 0xB0260+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part6 of Section and Line

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_Section_Part6_Base                                               0xB0260
#define cAf6Reg_FMPM_TCA_threshold_of_Section_Part6(id)                                     (0xB0260+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_sec_uas_lfe
BitField Type: R/W
BitField Desc: TCA threshold of UAS_LFE
BitField Bits: [19:10]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_Section_Part6_tca_thres_sec_uas_lfe_Mask                               cBit19_10
#define cAf6_FMPM_TCA_threshold_of_Section_Part6_tca_thres_sec_uas_lfe_Shift                                      10

/*--------------------------------------
BitField Name: tca_thres_sec_fc_lfe
BitField Type: R/W
BitField Desc: TCA threshold of FC_LFE
BitField Bits: [09:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_Section_Part6_tca_thres_sec_fc_lfe_Mask                                 cBit9_0
#define cAf6_FMPM_TCA_threshold_of_Section_Part6_tca_thres_sec_fc_lfe_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of HO Part0
Reg Addr   : 0xB0000-0xB000E
Reg Formula: 0xB0000+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part0 of HO

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_HO_Part0_Base                                                    0xB0000
#define cAf6Reg_FMPM_TCA_threshold_of_HO_Part0(id)                                          (0xB0000+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_sts_cv_p
BitField Type: R/W
BitField Desc: TCA threshold of CV_P
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_HO_Part0_tca_thres_sts_cv_p_Mask                                   cBit23_0
#define cAf6_FMPM_TCA_threshold_of_HO_Part0_tca_thres_sts_cv_p_Shift                                         0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of HO Part1
Reg Addr   : 0xB0010-0xB001E
Reg Formula: 0xB0010+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part1 of HO

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_HO_Part1_Base                                                    0xB0010
#define cAf6Reg_FMPM_TCA_threshold_of_HO_Part1(id)                                          (0xB0010+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_sts_es_p
BitField Type: R/W
BitField Desc: TCA threshold of ES_P
BitField Bits: [29:20]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_HO_Part1_tca_thres_sts_es_p_Mask                                  cBit29_20
#define cAf6_FMPM_TCA_threshold_of_HO_Part1_tca_thres_sts_es_p_Shift                                        20

/*--------------------------------------
BitField Name: tca_thres_sts_ses_p
BitField Type: R/W
BitField Desc: TCA threshold of SES_P
BitField Bits: [19:10]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_HO_Part1_tca_thres_sts_ses_p_Mask                                 cBit19_10
#define cAf6_FMPM_TCA_threshold_of_HO_Part1_tca_thres_sts_ses_p_Shift                                       10

/*--------------------------------------
BitField Name: tca_thres_sts_uas_p
BitField Type: R/W
BitField Desc: TCA threshold of UAS_P
BitField Bits: [09:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_HO_Part1_tca_thres_sts_uas_p_Mask                                   cBit9_0
#define cAf6_FMPM_TCA_threshold_of_HO_Part1_tca_thres_sts_uas_p_Shift                                        0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of HO Part2
Reg Addr   : 0xB0020-0xB002E
Reg Formula: 0xB0020+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part2 of HO

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_HO_Part2_Base                                                    0xB0020
#define cAf6Reg_FMPM_TCA_threshold_of_HO_Part2(id)                                          (0xB0020+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_sts_ppjc_pdet
BitField Type: R/W
BitField Desc: TCA threshold of PPJC_PDet
BitField Bits: [19:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_HO_Part2_tca_thres_sts_ppjc_pdet_Mask                                cBit19_0
#define cAf6_FMPM_TCA_threshold_of_HO_Part2_tca_thres_sts_ppjc_pdet_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of HO Part3
Reg Addr   : 0xB0030-0xB003E
Reg Formula: 0xB0030+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part3 of HO

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_HO_Part3_Base                                                    0xB0030
#define cAf6Reg_FMPM_TCA_threshold_of_HO_Part3(id)                                          (0xB0030+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_sts_npjc_pdet
BitField Type: R/W
BitField Desc: TCA threshold of NPJC_PDet
BitField Bits: [19:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_HO_Part3_tca_thres_sts_npjc_pdet_Mask                                cBit19_0
#define cAf6_FMPM_TCA_threshold_of_HO_Part3_tca_thres_sts_npjc_pdet_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of HO Part4
Reg Addr   : 0xB0040-0xB004E
Reg Formula: 0xB0040+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part4 of HO

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_HO_Part4_Base                                                    0xB0040
#define cAf6Reg_FMPM_TCA_threshold_of_HO_Part4(id)                                          (0xB0040+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_sts_ppjc_pgen
BitField Type: R/W
BitField Desc: TCA threshold of PPJC_PGen
BitField Bits: [19:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_HO_Part4_tca_thres_sts_ppjc_pgen_Mask                                cBit19_0
#define cAf6_FMPM_TCA_threshold_of_HO_Part4_tca_thres_sts_ppjc_pgen_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of HO Part5
Reg Addr   : 0xB0050-0xB005E
Reg Formula: 0xB0050+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part5 of HO

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_HO_Part5_Base                                                    0xB0050
#define cAf6Reg_FMPM_TCA_threshold_of_HO_Part5(id)                                          (0xB0050+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_sts_npjc_pgen
BitField Type: R/W
BitField Desc: TCA threshold of NPJC_PGen
BitField Bits: [19:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_HO_Part5_tca_thres_sts_npjc_pgen_Mask                                cBit19_0
#define cAf6_FMPM_TCA_threshold_of_HO_Part5_tca_thres_sts_npjc_pgen_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of HO Part6
Reg Addr   : 0xB0060-0xB006E
Reg Formula: 0xB0060+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part6 of HO

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_HO_Part6_Base                                                    0xB0060
#define cAf6Reg_FMPM_TCA_threshold_of_HO_Part6(id)                                          (0xB0060+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_sts_pjcdiff_p
BitField Type: R/W
BitField Desc: TCA threshold of PJCDiff_P
BitField Bits: [19:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_HO_Part6_tca_thres_sts_pjcdiff_p_Mask                                cBit19_0
#define cAf6_FMPM_TCA_threshold_of_HO_Part6_tca_thres_sts_pjcdiff_p_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of HO Part7
Reg Addr   : 0xB0070-0xB007E
Reg Formula: 0xB0070+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part7 of HO

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_HO_Part7_Base                                                    0xB0070
#define cAf6Reg_FMPM_TCA_threshold_of_HO_Part7(id)                                          (0xB0070+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_sts_pjcs_pdet
BitField Type: R/W
BitField Desc: TCA threshold of PJCS_PDet
BitField Bits: [19:10]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_HO_Part7_tca_thres_sts_pjcs_pdet_Mask                               cBit19_10
#define cAf6_FMPM_TCA_threshold_of_HO_Part7_tca_thres_sts_pjcs_pdet_Shift                                      10

/*--------------------------------------
BitField Name: tca_thres_sts_pjcs_pgen
BitField Type: R/W
BitField Desc: TCA threshold of PJCS_PGen
BitField Bits: [09:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_HO_Part7_tca_thres_sts_pjcs_pgen_Mask                                 cBit9_0
#define cAf6_FMPM_TCA_threshold_of_HO_Part7_tca_thres_sts_pjcs_pgen_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of HO Part8
Reg Addr   : 0xB0080-0xB008E
Reg Formula: 0xB0080+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part8 of HO

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_HO_Part8_Base                                                    0xB0080
#define cAf6Reg_FMPM_TCA_threshold_of_HO_Part8(id)                                          (0xB0080+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_sts_cv_pfe
BitField Type: R/W
BitField Desc: TCA threshold of CV_PFE
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_HO_Part8_tca_thres_sts_cv_pfe_Mask                                 cBit23_0
#define cAf6_FMPM_TCA_threshold_of_HO_Part8_tca_thres_sts_cv_pfe_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of HO Part9
Reg Addr   : 0xB0090-0xB009E
Reg Formula: 0xB0090+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part9 of HO

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_HO_Part9_Base                                                    0xB0090
#define cAf6Reg_FMPM_TCA_threshold_of_HO_Part9(id)                                          (0xB0090+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_sts_es_pfe
BitField Type: R/W
BitField Desc: TCA threshold of ES_PFE
BitField Bits: [29:20]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_HO_Part9_tca_thres_sts_es_pfe_Mask                                cBit29_20
#define cAf6_FMPM_TCA_threshold_of_HO_Part9_tca_thres_sts_es_pfe_Shift                                      20

/*--------------------------------------
BitField Name: tca_thres_sts_ses_pfe
BitField Type: R/W
BitField Desc: TCA threshold of SES_PFE
BitField Bits: [19:10]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_HO_Part9_tca_thres_sts_ses_pfe_Mask                               cBit19_10
#define cAf6_FMPM_TCA_threshold_of_HO_Part9_tca_thres_sts_ses_pfe_Shift                                      10

/*--------------------------------------
BitField Name: tca_thres_sts_uas_pfe
BitField Type: R/W
BitField Desc: TCA threshold of USE_PFE
BitField Bits: [09:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_HO_Part9_tca_thres_sts_uas_pfe_Mask                                 cBit9_0
#define cAf6_FMPM_TCA_threshold_of_HO_Part9_tca_thres_sts_uas_pfe_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of HO Part10
Reg Addr   : 0xB00A0-0xB00AE
Reg Formula: 0xB00A0+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part10 of HO

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_HO_Part10_Base                                                   0xB00A0
#define cAf6Reg_FMPM_TCA_threshold_of_HO_Part10(id)                                         (0xB00A0+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_sts_fc_p
BitField Type: R/W
BitField Desc: TCA threshold of FC_P
BitField Bits: [19:10]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_HO_Part10_tca_thres_sts_fc_p_Mask                                 cBit19_10
#define cAf6_FMPM_TCA_threshold_of_HO_Part10_tca_thres_sts_fc_p_Shift                                       10

/*--------------------------------------
BitField Name: tca_thres_sts_fc_pfe
BitField Type: R/W
BitField Desc: TCA threshold of FC_PFE
BitField Bits: [09:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_HO_Part10_tca_thres_sts_fc_pfe_Mask                                 cBit9_0
#define cAf6_FMPM_TCA_threshold_of_HO_Part10_tca_thres_sts_fc_pfe_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of LO Part0
Reg Addr   : 0xB0300-0xB030E
Reg Formula: 0xB0300+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part0 of LO

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_LO_Part0_Base                                                    0xB0300
#define cAf6Reg_FMPM_TCA_threshold_of_LO_Part0(id)                                          (0xB0300+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_vt_cv_v
BitField Type: R/W
BitField Desc: TCA threshold of CV_V
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_LO_Part0_tca_thres_vt_cv_v_Mask                                    cBit23_0
#define cAf6_FMPM_TCA_threshold_of_LO_Part0_tca_thres_vt_cv_v_Shift                                          0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of LO Part1
Reg Addr   : 0xB0310-0xB031E
Reg Formula: 0xB0310+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part1 of LO

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_LO_Part1_Base                                                    0xB0310
#define cAf6Reg_FMPM_TCA_threshold_of_LO_Part1(id)                                          (0xB0310+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_vt_es_v
BitField Type: R/W
BitField Desc: TCA threshold of ES_V
BitField Bits: [29:20]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_LO_Part1_tca_thres_vt_es_v_Mask                                   cBit29_20
#define cAf6_FMPM_TCA_threshold_of_LO_Part1_tca_thres_vt_es_v_Shift                                         20

/*--------------------------------------
BitField Name: tca_thres_vt_ses_v
BitField Type: R/W
BitField Desc: TCA threshold of SES_V
BitField Bits: [19:10]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_LO_Part1_tca_thres_vt_ses_v_Mask                                  cBit19_10
#define cAf6_FMPM_TCA_threshold_of_LO_Part1_tca_thres_vt_ses_v_Shift                                        10

/*--------------------------------------
BitField Name: tca_thres_vt_uas_v
BitField Type: R/W
BitField Desc: TCA threshold of UAS_V
BitField Bits: [09:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_LO_Part1_tca_thres_vt_uas_v_Mask                                    cBit9_0
#define cAf6_FMPM_TCA_threshold_of_LO_Part1_tca_thres_vt_uas_v_Shift                                         0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of LO Part2
Reg Addr   : 0xB0320-0xB032E
Reg Formula: 0xB0320+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part2 of LO

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_LO_Part2_Base                                                    0xB0320
#define cAf6Reg_FMPM_TCA_threshold_of_LO_Part2(id)                                          (0xB0320+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_vt_ppjc_vdet
BitField Type: R/W
BitField Desc: TCA threshold of PPJC_VDet
BitField Bits: [14:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_LO_Part2_tca_thres_vt_ppjc_vdet_Mask                                cBit14_0
#define cAf6_FMPM_TCA_threshold_of_LO_Part2_tca_thres_vt_ppjc_vdet_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of LO Part3
Reg Addr   : 0xB0330-0xB033E
Reg Formula: 0xB0330+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part3 of LO

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_LO_Part3_Base                                                    0xB0330
#define cAf6Reg_FMPM_TCA_threshold_of_LO_Part3(id)                                          (0xB0330+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_vt_npjc_vdet
BitField Type: R/W
BitField Desc: TCA threshold of NPJC_VDet
BitField Bits: [14:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_LO_Part3_tca_thres_vt_npjc_vdet_Mask                                cBit14_0
#define cAf6_FMPM_TCA_threshold_of_LO_Part3_tca_thres_vt_npjc_vdet_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of LO Part4
Reg Addr   : 0xB0340-0xB034E
Reg Formula: 0xB0340+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part4 of LO

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_LO_Part4_Base                                                    0xB0340
#define cAf6Reg_FMPM_TCA_threshold_of_LO_Part4(id)                                          (0xB0340+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_vt_ppjc_vgen
BitField Type: R/W
BitField Desc: TCA threshold of PPJC_VGen
BitField Bits: [14:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_LO_Part4_tca_thres_vt_ppjc_vgen_Mask                                cBit14_0
#define cAf6_FMPM_TCA_threshold_of_LO_Part4_tca_thres_vt_ppjc_vgen_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of LO Part5
Reg Addr   : 0xB0350-0xB035E
Reg Formula: 0xB0350+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part5 of LO

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_LO_Part5_Base                                                    0xB0350
#define cAf6Reg_FMPM_TCA_threshold_of_LO_Part5(id)                                          (0xB0350+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_vt_npjc_vgen
BitField Type: R/W
BitField Desc: TCA threshold of NPJC_VGen
BitField Bits: [14:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_LO_Part5_tca_thres_vt_npjc_vgen_Mask                                cBit14_0
#define cAf6_FMPM_TCA_threshold_of_LO_Part5_tca_thres_vt_npjc_vgen_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of LO Part6
Reg Addr   : 0xB0360-0xB036E
Reg Formula: 0xB0360+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part6 of LO

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_LO_Part6_Base                                                    0xB0360
#define cAf6Reg_FMPM_TCA_threshold_of_LO_Part6(id)                                          (0xB0360+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_vt_pjcdiff_v
BitField Type: R/W
BitField Desc: TCA threshold of PJCDiff_V
BitField Bits: [14:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_LO_Part6_tca_thres_vt_pjcdiff_v_Mask                                cBit14_0
#define cAf6_FMPM_TCA_threshold_of_LO_Part6_tca_thres_vt_pjcdiff_v_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of LO Part7
Reg Addr   : 0xB0370-0xB037E
Reg Formula: 0xB0370+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part7 of LO

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_LO_Part7_Base                                                    0xB0370
#define cAf6Reg_FMPM_TCA_threshold_of_LO_Part7(id)                                          (0xB0370+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_vt_pjcs_vdet
BitField Type: R/W
BitField Desc: TCA threshold of PJCS_VDET
BitField Bits: [19:10]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_LO_Part7_tca_thres_vt_pjcs_vdet_Mask                               cBit19_10
#define cAf6_FMPM_TCA_threshold_of_LO_Part7_tca_thres_vt_pjcs_vdet_Shift                                      10

/*--------------------------------------
BitField Name: tca_thres_vt_pjcs_vgen
BitField Type: R/W
BitField Desc: TCA threshold of PJCS_VGEN
BitField Bits: [09:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_LO_Part7_tca_thres_vt_pjcs_vgen_Mask                                 cBit9_0
#define cAf6_FMPM_TCA_threshold_of_LO_Part7_tca_thres_vt_pjcs_vgen_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of LO Part8
Reg Addr   : 0xB0380-0xB038E
Reg Formula: 0xB0380+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part8 of LO

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_LO_Part8_Base                                                    0xB0380
#define cAf6Reg_FMPM_TCA_threshold_of_LO_Part8(id)                                          (0xB0380+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_vt_cv_pfe
BitField Type: R/W
BitField Desc: TCA threshold of CV_VFE
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_LO_Part8_tca_thres_vt_cv_pfe_Mask                                  cBit23_0
#define cAf6_FMPM_TCA_threshold_of_LO_Part8_tca_thres_vt_cv_pfe_Shift                                        0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of LO Part9
Reg Addr   : 0xB0390-0xB039E
Reg Formula: 0xB0390+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part9 of LO

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_LO_Part9_Base                                                    0xB0390
#define cAf6Reg_FMPM_TCA_threshold_of_LO_Part9(id)                                          (0xB0390+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_vt_es_vfe
BitField Type: R/W
BitField Desc: TCA threshold of ES_VFE
BitField Bits: [29:20]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_LO_Part9_tca_thres_vt_es_vfe_Mask                                 cBit29_20
#define cAf6_FMPM_TCA_threshold_of_LO_Part9_tca_thres_vt_es_vfe_Shift                                       20

/*--------------------------------------
BitField Name: tca_thres_vt_ses_vfe
BitField Type: R/W
BitField Desc: TCA threshold of SES_VFE
BitField Bits: [19:10]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_LO_Part9_tca_thres_vt_ses_vfe_Mask                                cBit19_10
#define cAf6_FMPM_TCA_threshold_of_LO_Part9_tca_thres_vt_ses_vfe_Shift                                      10

/*--------------------------------------
BitField Name: tca_thres_vt_uas_vfe
BitField Type: R/W
BitField Desc: TCA threshold of USE_VFE
BitField Bits: [09:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_LO_Part9_tca_thres_vt_uas_vfe_Mask                                  cBit9_0
#define cAf6_FMPM_TCA_threshold_of_LO_Part9_tca_thres_vt_uas_vfe_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of LO Part10
Reg Addr   : 0xB03A0-0xB03AE
Reg Formula: 0xB03A0+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part10 of LO

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_LO_Part10_Base                                                   0xB03A0
#define cAf6Reg_FMPM_TCA_threshold_of_LO_Part10(id)                                         (0xB03A0+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_vt_fc_v
BitField Type: R/W
BitField Desc: TCA threshold of FC_V
BitField Bits: [19:10]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_LO_Part10_tca_thres_vt_fc_v_Mask                                  cBit19_10
#define cAf6_FMPM_TCA_threshold_of_LO_Part10_tca_thres_vt_fc_v_Shift                                        10

/*--------------------------------------
BitField Name: tca_thres_vt_fc_vfe
BitField Type: R/W
BitField Desc: TCA threshold of FC_VFE
BitField Bits: [09:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_LO_Part10_tca_thres_vt_fc_vfe_Mask                                  cBit9_0
#define cAf6_FMPM_TCA_threshold_of_LO_Part10_tca_thres_vt_fc_vfe_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of DS3 Part0
Reg Addr   : 0xB0100-0xB010E
Reg Formula: 0xB0100+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part0 of DS3

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_DS3_Part0_Base                                                   0xB0100
#define cAf6Reg_FMPM_TCA_threshold_of_DS3_Part0(id)                                         (0xB0100+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_ds3_cv_l
BitField Type: R/W
BitField Desc: TCA threshold of CV_L
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS3_Part0_tca_thres_ds3_cv_l_Mask                                  cBit23_0
#define cAf6_FMPM_TCA_threshold_of_DS3_Part0_tca_thres_ds3_cv_l_Shift                                        0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of DS3 Part1
Reg Addr   : 0xB0110-0xB011E
Reg Formula: 0xB0110+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part1 of DS3

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_DS3_Part1_Base                                                   0xB0110
#define cAf6Reg_FMPM_TCA_threshold_of_DS3_Part1(id)                                         (0xB0110+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_ds3_es_l
BitField Type: R/W
BitField Desc: TCA threshold of ES_L
BitField Bits: [29:20]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS3_Part1_tca_thres_ds3_es_l_Mask                                 cBit29_20
#define cAf6_FMPM_TCA_threshold_of_DS3_Part1_tca_thres_ds3_es_l_Shift                                       20

/*--------------------------------------
BitField Name: tca_thres_ds3_esa_l
BitField Type: R/W
BitField Desc: TCA threshold of ESA_L
BitField Bits: [19:10]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS3_Part1_tca_thres_ds3_esa_l_Mask                                cBit19_10
#define cAf6_FMPM_TCA_threshold_of_DS3_Part1_tca_thres_ds3_esa_l_Shift                                      10

/*--------------------------------------
BitField Name: tca_thres_ds3_esb_l
BitField Type: R/W
BitField Desc: TCA threshold of ESB_L
BitField Bits: [09:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS3_Part1_tca_thres_ds3_esb_l_Mask                                  cBit9_0
#define cAf6_FMPM_TCA_threshold_of_DS3_Part1_tca_thres_ds3_esb_l_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of DS3 Part2
Reg Addr   : 0xB0120-0xB012E
Reg Formula: 0xB0120+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part2 of DS3

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_DS3_Part2_Base                                                   0xB0120
#define cAf6Reg_FMPM_TCA_threshold_of_DS3_Part2(id)                                         (0xB0120+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_ds3_ses_l
BitField Type: R/W
BitField Desc: TCA threshold of SES_L
BitField Bits: [19:10]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS3_Part2_tca_thres_ds3_ses_l_Mask                                cBit19_10
#define cAf6_FMPM_TCA_threshold_of_DS3_Part2_tca_thres_ds3_ses_l_Shift                                      10

/*--------------------------------------
BitField Name: tca_thres_ds3_loss_l
BitField Type: R/W
BitField Desc: TCA threshold of LOSS_L
BitField Bits: [09:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS3_Part2_tca_thres_ds3_loss_l_Mask                                 cBit9_0
#define cAf6_FMPM_TCA_threshold_of_DS3_Part2_tca_thres_ds3_loss_l_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of DS3 Part3
Reg Addr   : 0xB0130-0xB013E
Reg Formula: 0xB0130+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part3 of DS3

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_DS3_Part3_Base                                                   0xB0130
#define cAf6Reg_FMPM_TCA_threshold_of_DS3_Part3(id)                                         (0xB0130+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_ds3_cvp_p
BitField Type: R/W
BitField Desc: TCA threshold of CVP_P
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS3_Part3_tca_thres_ds3_cvp_p_Mask                                 cBit23_0
#define cAf6_FMPM_TCA_threshold_of_DS3_Part3_tca_thres_ds3_cvp_p_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of DS3 Part4
Reg Addr   : 0xB0140-0xB014E
Reg Formula: 0xB0140+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part4 of DS3

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_DS3_Part4_Base                                                   0xB0140
#define cAf6Reg_FMPM_TCA_threshold_of_DS3_Part4(id)                                         (0xB0140+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_ds3_cvcp_p
BitField Type: R/W
BitField Desc: TCA threshold of CVCP_P
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS3_Part4_tca_thres_ds3_cvcp_p_Mask                                cBit23_0
#define cAf6_FMPM_TCA_threshold_of_DS3_Part4_tca_thres_ds3_cvcp_p_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of DS3 Part5
Reg Addr   : 0xB0150-0xB015E
Reg Formula: 0xB0150+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part5 of DS3

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_DS3_Part5_Base                                                   0xB0150
#define cAf6Reg_FMPM_TCA_threshold_of_DS3_Part5(id)                                         (0xB0150+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_ds3_esp_p
BitField Type: R/W
BitField Desc: TCA threshold of ESP_P
BitField Bits: [29:20]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS3_Part5_tca_thres_ds3_esp_p_Mask                                cBit29_20
#define cAf6_FMPM_TCA_threshold_of_DS3_Part5_tca_thres_ds3_esp_p_Shift                                      20

/*--------------------------------------
BitField Name: tca_thres_ds3_escp_p
BitField Type: R/W
BitField Desc: TCA threshold of ESCP_P
BitField Bits: [19:10]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS3_Part5_tca_thres_ds3_escp_p_Mask                               cBit19_10
#define cAf6_FMPM_TCA_threshold_of_DS3_Part5_tca_thres_ds3_escp_p_Shift                                      10

/*--------------------------------------
BitField Name: tca_thres_ds3_esacp_p
BitField Type: R/W
BitField Desc: TCA threshold of ESACP_P
BitField Bits: [09:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS3_Part5_tca_thres_ds3_esacp_p_Mask                                 cBit9_0
#define cAf6_FMPM_TCA_threshold_of_DS3_Part5_tca_thres_ds3_esacp_p_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of DS3 Part6
Reg Addr   : 0xB0160-0xB016E
Reg Formula: 0xB0160+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part6 of DS3

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_DS3_Part6_Base                                                   0xB0160
#define cAf6Reg_FMPM_TCA_threshold_of_DS3_Part6(id)                                         (0xB0160+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_ds3_esacp_p
BitField Type: R/W
BitField Desc: TCA threshold of ESACP_P
BitField Bits: [29:20]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS3_Part6_tca_thres_ds3_esacp_p_Mask                               cBit29_20
#define cAf6_FMPM_TCA_threshold_of_DS3_Part6_tca_thres_ds3_esacp_p_Shift                                      20

/*--------------------------------------
BitField Name: tca_thres_ds3_esbp_p
BitField Type: R/W
BitField Desc: TCA threshold of ESBP_P
BitField Bits: [19:10]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS3_Part6_tca_thres_ds3_esbp_p_Mask                               cBit19_10
#define cAf6_FMPM_TCA_threshold_of_DS3_Part6_tca_thres_ds3_esbp_p_Shift                                      10

/*--------------------------------------
BitField Name: tca_thres_ds3_esbcp_p
BitField Type: R/W
BitField Desc: TCA threshold of ESBCP_P
BitField Bits: [09:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS3_Part6_tca_thres_ds3_esbcp_p_Mask                                 cBit9_0
#define cAf6_FMPM_TCA_threshold_of_DS3_Part6_tca_thres_ds3_esbcp_p_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of DS3 Part7
Reg Addr   : 0xB0170-0xB017E
Reg Formula: 0xB0170+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part7 of DS3

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_DS3_Part7_Base                                                   0xB0170
#define cAf6Reg_FMPM_TCA_threshold_of_DS3_Part7(id)                                         (0xB0170+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_ds3_sesp_p
BitField Type: R/W
BitField Desc: TCA threshold of SESP_P
BitField Bits: [29:20]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS3_Part7_tca_thres_ds3_sesp_p_Mask                               cBit29_20
#define cAf6_FMPM_TCA_threshold_of_DS3_Part7_tca_thres_ds3_sesp_p_Shift                                      20

/*--------------------------------------
BitField Name: tca_thres_ds3_sescp_p
BitField Type: R/W
BitField Desc: TCA threshold of SESCP_P
BitField Bits: [19:10]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS3_Part7_tca_thres_ds3_sescp_p_Mask                               cBit19_10
#define cAf6_FMPM_TCA_threshold_of_DS3_Part7_tca_thres_ds3_sescp_p_Shift                                      10

/*--------------------------------------
BitField Name: tca_thres_ds3_sas_p
BitField Type: R/W
BitField Desc: TCA threshold of SAS_P
BitField Bits: [09:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS3_Part7_tca_thres_ds3_sas_p_Mask                                  cBit9_0
#define cAf6_FMPM_TCA_threshold_of_DS3_Part7_tca_thres_ds3_sas_p_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of DS3 Part8
Reg Addr   : 0xB0180-0xB018E
Reg Formula: 0xB0180+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part8 of DS3

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_DS3_Part8_Base                                                   0xB0180
#define cAf6Reg_FMPM_TCA_threshold_of_DS3_Part8(id)                                         (0xB0180+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_ds3_aiss_p
BitField Type: R/W
BitField Desc: TCA threshold of AISS_P
BitField Bits: [29:20]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS3_Part8_tca_thres_ds3_aiss_p_Mask                               cBit29_20
#define cAf6_FMPM_TCA_threshold_of_DS3_Part8_tca_thres_ds3_aiss_p_Shift                                      20

/*--------------------------------------
BitField Name: tca_thres_ds3_uasp_p
BitField Type: R/W
BitField Desc: TCA threshold of UASP_P
BitField Bits: [19:10]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS3_Part8_tca_thres_ds3_uasp_p_Mask                               cBit19_10
#define cAf6_FMPM_TCA_threshold_of_DS3_Part8_tca_thres_ds3_uasp_p_Shift                                      10

/*--------------------------------------
BitField Name: tca_thres_ds3_uascp_p
BitField Type: R/W
BitField Desc: TCA threshold of UASCP_P
BitField Bits: [09:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS3_Part8_tca_thres_ds3_uascp_p_Mask                                 cBit9_0
#define cAf6_FMPM_TCA_threshold_of_DS3_Part8_tca_thres_ds3_uascp_p_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of DS3 Part9
Reg Addr   : 0xB0190-0xB019E
Reg Formula: 0xB0190+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part9 of DS3

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_DS3_Part9_Base                                                   0xB0190
#define cAf6Reg_FMPM_TCA_threshold_of_DS3_Part9(id)                                         (0xB0190+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_ds3_cvcp_pfe
BitField Type: R/W
BitField Desc: TCA threshold of CVCP_PFE
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS3_Part9_tca_thres_ds3_cvcp_pfe_Mask                                cBit23_0
#define cAf6_FMPM_TCA_threshold_of_DS3_Part9_tca_thres_ds3_cvcp_pfe_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of DS3 Part10
Reg Addr   : 0xB01A0-0xB01AE
Reg Formula: 0xB01A0+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part10 of DS3

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_DS3_Part10_Base                                                  0xB01A0
#define cAf6Reg_FMPM_TCA_threshold_of_DS3_Part10(id)                                        (0xB01A0+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_ds3_escp_pfe
BitField Type: R/W
BitField Desc: TCA threshold of ESCP_PFE
BitField Bits: [29:20]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS3_Part10_tca_thres_ds3_escp_pfe_Mask                               cBit29_20
#define cAf6_FMPM_TCA_threshold_of_DS3_Part10_tca_thres_ds3_escp_pfe_Shift                                      20

/*--------------------------------------
BitField Name: tca_thres_ds3_esacp_pfe
BitField Type: R/W
BitField Desc: TCA threshold of ESACP_PFE
BitField Bits: [19:10]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS3_Part10_tca_thres_ds3_esacp_pfe_Mask                               cBit19_10
#define cAf6_FMPM_TCA_threshold_of_DS3_Part10_tca_thres_ds3_esacp_pfe_Shift                                      10

/*--------------------------------------
BitField Name: tca_thres_ds3_esbcp_pfe
BitField Type: R/W
BitField Desc: TCA threshold of ESBCP_PFE
BitField Bits: [09:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS3_Part10_tca_thres_ds3_esbcp_pfe_Mask                                 cBit9_0
#define cAf6_FMPM_TCA_threshold_of_DS3_Part10_tca_thres_ds3_esbcp_pfe_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of DS3 Part11
Reg Addr   : 0xB01B0-0xB01BE
Reg Formula: 0xB01B0+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part11 of DS3

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_DS3_Part11_Base                                                  0xB01B0
#define cAf6Reg_FMPM_TCA_threshold_of_DS3_Part11(id)                                        (0xB01B0+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_ds3_sescp_pfe
BitField Type: R/W
BitField Desc: TCA threshold of SESCP_PFE
BitField Bits: [29:20]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS3_Part11_tca_thres_ds3_sescp_pfe_Mask                               cBit29_20
#define cAf6_FMPM_TCA_threshold_of_DS3_Part11_tca_thres_ds3_sescp_pfe_Shift                                      20

/*--------------------------------------
BitField Name: tca_thres_ds3_sascp_pfe
BitField Type: R/W
BitField Desc: TCA threshold of SASCP_PFE
BitField Bits: [19:10]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS3_Part11_tca_thres_ds3_sascp_pfe_Mask                               cBit19_10
#define cAf6_FMPM_TCA_threshold_of_DS3_Part11_tca_thres_ds3_sascp_pfe_Shift                                      10

/*--------------------------------------
BitField Name: tca_thres_ds3_uascp_pfe
BitField Type: R/W
BitField Desc: TCA threshold of UASCP_PFE
BitField Bits: [09:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS3_Part11_tca_thres_ds3_uascp_pfe_Mask                                 cBit9_0
#define cAf6_FMPM_TCA_threshold_of_DS3_Part11_tca_thres_ds3_uascp_pfe_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of DS3 Part12
Reg Addr   : 0xB01C0-0xB01CE
Reg Formula: 0xB01C0+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part12 of DS3

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_DS3_Part12_Base                                                  0xB01C0
#define cAf6Reg_FMPM_TCA_threshold_of_DS3_Part12(id)                                        (0xB01C0+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_ds3_fc_p
BitField Type: R/W
BitField Desc: TCA threshold of FC_P
BitField Bits: [19:10]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS3_Part12_tca_thres_ds3_fc_p_Mask                                cBit19_10
#define cAf6_FMPM_TCA_threshold_of_DS3_Part12_tca_thres_ds3_fc_p_Shift                                      10

/*--------------------------------------
BitField Name: tca_thres_ds3_fccp_pfe
BitField Type: R/W
BitField Desc: TCA threshold of FCCP_PFE
BitField Bits: [09:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS3_Part12_tca_thres_ds3_fccp_pfe_Mask                                 cBit9_0
#define cAf6_FMPM_TCA_threshold_of_DS3_Part12_tca_thres_ds3_fccp_pfe_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of DS1 Part0
Reg Addr   : 0xB0400-0xB040E
Reg Formula: 0xB0400+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part0 of DS1

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_DS1_Part0_Base                                                   0xB0400
#define cAf6Reg_FMPM_TCA_threshold_of_DS1_Part0(id)                                         (0xB0400+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_ds1_cv_l
BitField Type: R/W
BitField Desc: TCA threshold of CV_L
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS1_Part0_tca_thres_ds1_cv_l_Mask                                  cBit23_0
#define cAf6_FMPM_TCA_threshold_of_DS1_Part0_tca_thres_ds1_cv_l_Shift                                        0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of DS1 Part1
Reg Addr   : 0xB0410-0xB041E
Reg Formula: 0xB0410+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part1 of DS1

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_DS1_Part1_Base                                                   0xB0410
#define cAf6Reg_FMPM_TCA_threshold_of_DS1_Part1(id)                                         (0xB0410+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_ds1es_l
BitField Type: R/W
BitField Desc: TCA threshold of ES_L
BitField Bits: [29:20]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS1_Part1_tca_thres_ds1es_l_Mask                                  cBit29_20
#define cAf6_FMPM_TCA_threshold_of_DS1_Part1_tca_thres_ds1es_l_Shift                                        20

/*--------------------------------------
BitField Name: tca_thres_ds1ses_l
BitField Type: R/W
BitField Desc: TCA threshold of SES_L
BitField Bits: [19:10]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS1_Part1_tca_thres_ds1ses_l_Mask                                 cBit19_10
#define cAf6_FMPM_TCA_threshold_of_DS1_Part1_tca_thres_ds1ses_l_Shift                                       10

/*--------------------------------------
BitField Name: tca_thres_ds1loss_l
BitField Type: R/W
BitField Desc: TCA threshold of LOSS_L
BitField Bits: [09:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS1_Part1_tca_thres_ds1loss_l_Mask                                  cBit9_0
#define cAf6_FMPM_TCA_threshold_of_DS1_Part1_tca_thres_ds1loss_l_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of DS1 Part2
Reg Addr   : 0xB0420-0xB042E
Reg Formula: 0xB0420+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part2 of DS1

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_DS1_Part2_Base                                                   0xB0420
#define cAf6Reg_FMPM_TCA_threshold_of_DS1_Part2(id)                                         (0xB0420+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_ds1_cv_p
BitField Type: R/W
BitField Desc: TCA threshold of CV_P
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS1_Part2_tca_thres_ds1_cv_p_Mask                                  cBit23_0
#define cAf6_FMPM_TCA_threshold_of_DS1_Part2_tca_thres_ds1_cv_p_Shift                                        0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of DS1 Part3
Reg Addr   : 0xB0430-0xB043E
Reg Formula: 0xB0430+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part3 of DS1

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_DS1_Part3_Base                                                   0xB0430
#define cAf6Reg_FMPM_TCA_threshold_of_DS1_Part3(id)                                         (0xB0430+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_ds1_es_p
BitField Type: R/W
BitField Desc: TCA threshold of ES_P
BitField Bits: [29:20]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS1_Part3_tca_thres_ds1_es_p_Mask                                 cBit29_20
#define cAf6_FMPM_TCA_threshold_of_DS1_Part3_tca_thres_ds1_es_p_Shift                                       20

/*--------------------------------------
BitField Name: tca_thres_ds1_ses_p
BitField Type: R/W
BitField Desc: TCA threshold of SES_P
BitField Bits: [19:10]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS1_Part3_tca_thres_ds1_ses_p_Mask                                cBit19_10
#define cAf6_FMPM_TCA_threshold_of_DS1_Part3_tca_thres_ds1_ses_p_Shift                                      10

/*--------------------------------------
BitField Name: tca_thres_ds1_aiss_p
BitField Type: R/W
BitField Desc: TCA threshold of AISS_P
BitField Bits: [09:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS1_Part3_tca_thres_ds1_aiss_p_Mask                                 cBit9_0
#define cAf6_FMPM_TCA_threshold_of_DS1_Part3_tca_thres_ds1_aiss_p_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of DS1 Part4
Reg Addr   : 0xB0440-0xB044E
Reg Formula: 0xB0440+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part4 of DS1

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_DS1_Part4_Base                                                   0xB0440
#define cAf6Reg_FMPM_TCA_threshold_of_DS1_Part4(id)                                         (0xB0440+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_ds1_sas_p
BitField Type: R/W
BitField Desc: TCA threshold of SAS_P
BitField Bits: [29:20]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS1_Part4_tca_thres_ds1_sas_p_Mask                                cBit29_20
#define cAf6_FMPM_TCA_threshold_of_DS1_Part4_tca_thres_ds1_sas_p_Shift                                      20

/*--------------------------------------
BitField Name: tca_thres_ds1_css_p
BitField Type: R/W
BitField Desc: TCA threshold of CSS_P
BitField Bits: [19:10]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS1_Part4_tca_thres_ds1_css_p_Mask                                cBit19_10
#define cAf6_FMPM_TCA_threshold_of_DS1_Part4_tca_thres_ds1_css_p_Shift                                      10

/*--------------------------------------
BitField Name: tca_thres_ds1_uas_p
BitField Type: R/W
BitField Desc: TCA threshold of UAS_P
BitField Bits: [09:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS1_Part4_tca_thres_ds1_uas_p_Mask                                  cBit9_0
#define cAf6_FMPM_TCA_threshold_of_DS1_Part4_tca_thres_ds1_uas_p_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of DS1 Part5
Reg Addr   : 0xB0450-0xB045E
Reg Formula: 0xB0450+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part5 of DS1

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_DS1_Part5_Base                                                   0xB0450
#define cAf6Reg_FMPM_TCA_threshold_of_DS1_Part5(id)                                         (0xB0450+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_ds1_fc_p
BitField Type: R/W
BitField Desc: TCA threshold of FC_P
BitField Bits: [29:20]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS1_Part5_tca_thres_ds1_fc_p_Mask                                 cBit29_20
#define cAf6_FMPM_TCA_threshold_of_DS1_Part5_tca_thres_ds1_fc_p_Shift                                       20

/*--------------------------------------
BitField Name: tca_thres_ds1_es_lfe
BitField Type: R/W
BitField Desc: TCA threshold of ES_LFE
BitField Bits: [19:10]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS1_Part5_tca_thres_ds1_es_lfe_Mask                               cBit19_10
#define cAf6_FMPM_TCA_threshold_of_DS1_Part5_tca_thres_ds1_es_lfe_Shift                                      10

/*--------------------------------------
BitField Name: tca_thres_ds1_sefs_pfe
BitField Type: R/W
BitField Desc: TCA threshold of SEFS_PFE
BitField Bits: [09:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS1_Part5_tca_thres_ds1_sefs_pfe_Mask                                 cBit9_0
#define cAf6_FMPM_TCA_threshold_of_DS1_Part5_tca_thres_ds1_sefs_pfe_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of DS1 Part6
Reg Addr   : 0xB0460-0xB046E
Reg Formula: 0xB0460+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part6 of DS1

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_DS1_Part6_Base                                                   0xB0460
#define cAf6Reg_FMPM_TCA_threshold_of_DS1_Part6(id)                                         (0xB0460+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_ds1_es_pfe
BitField Type: R/W
BitField Desc: TCA threshold of ES_PFE
BitField Bits: [29:20]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS1_Part6_tca_thres_ds1_es_pfe_Mask                               cBit29_20
#define cAf6_FMPM_TCA_threshold_of_DS1_Part6_tca_thres_ds1_es_pfe_Shift                                      20

/*--------------------------------------
BitField Name: tca_thres_ds1_ses_pfe
BitField Type: R/W
BitField Desc: TCA threshold of SES_PFE
BitField Bits: [19:10]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS1_Part6_tca_thres_ds1_ses_pfe_Mask                               cBit19_10
#define cAf6_FMPM_TCA_threshold_of_DS1_Part6_tca_thres_ds1_ses_pfe_Shift                                      10

/*--------------------------------------
BitField Name: tca_thres_ds1_css_pfe
BitField Type: R/W
BitField Desc: TCA threshold of CSS_PFE
BitField Bits: [09:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS1_Part6_tca_thres_ds1_css_pfe_Mask                                 cBit9_0
#define cAf6_FMPM_TCA_threshold_of_DS1_Part6_tca_thres_ds1_css_pfe_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of DS1 Part7
Reg Addr   : 0xB0470-0xB047E
Reg Formula: 0xB0470+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part6 of DS1

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_DS1_Part7_Base                                                   0xB0470
#define cAf6Reg_FMPM_TCA_threshold_of_DS1_Part7(id)                                         (0xB0470+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_ds1_fc_pfe
BitField Type: R/W
BitField Desc: TCA threshold of FC-PFE
BitField Bits: [19:10]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS1_Part7_tca_thres_ds1_fc_pfe_Mask                               cBit19_10
#define cAf6_FMPM_TCA_threshold_of_DS1_Part7_tca_thres_ds1_fc_pfe_Shift                                      10

/*--------------------------------------
BitField Name: tca_thres_ds1_uas_pfe
BitField Type: R/W
BitField Desc: TCA threshold of UAS_PFE
BitField Bits: [09:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_DS1_Part7_tca_thres_ds1_uas_pfe_Mask                                 cBit9_0
#define cAf6_FMPM_TCA_threshold_of_DS1_Part7_tca_thres_ds1_uas_pfe_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of PW Part0
Reg Addr   : 0xB0500-0xB050E
Reg Formula: 0xB0500+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part0 of PW

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_PW_Part0_Base                                                    0xB0500
#define cAf6Reg_FMPM_TCA_threshold_of_PW_Part0(id)                                          (0xB0500+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_pw_es
BitField Type: R/W
BitField Desc: TCA threshold of ES
BitField Bits: [29:20]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_PW_Part0_tca_thres_pw_es_Mask                                     cBit29_20
#define cAf6_FMPM_TCA_threshold_of_PW_Part0_tca_thres_pw_es_Shift                                           20

/*--------------------------------------
BitField Name: tca_thres_pw_ses
BitField Type: R/W
BitField Desc: TCA threshold of SES
BitField Bits: [19:10]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_PW_Part0_tca_thres_pw_ses_Mask                                    cBit19_10
#define cAf6_FMPM_TCA_threshold_of_PW_Part0_tca_thres_pw_ses_Shift                                          10

/*--------------------------------------
BitField Name: tca_thres_pw_uas
BitField Type: R/W
BitField Desc: TCA threshold of UAS
BitField Bits: [09:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_PW_Part0_tca_thres_pw_uas_Mask                                      cBit9_0
#define cAf6_FMPM_TCA_threshold_of_PW_Part0_tca_thres_pw_uas_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of PW Part1
Reg Addr   : 0xB0510-0xB051E
Reg Formula: 0xB0510+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part1 of PW

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_PW_Part1_Base                                                    0xB0510
#define cAf6Reg_FMPM_TCA_threshold_of_PW_Part1(id)                                          (0xB0510+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_pw_es_fe
BitField Type: R/W
BitField Desc: TCA threshold of ES_FE
BitField Bits: [29:20]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_PW_Part1_tca_thres_pw_es_fe_Mask                                  cBit29_20
#define cAf6_FMPM_TCA_threshold_of_PW_Part1_tca_thres_pw_es_fe_Shift                                        20

/*--------------------------------------
BitField Name: tca_thres_pw_ses_fe
BitField Type: R/W
BitField Desc: TCA threshold of SES_FE
BitField Bits: [19:10]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_PW_Part1_tca_thres_pw_ses_fe_Mask                                 cBit19_10
#define cAf6_FMPM_TCA_threshold_of_PW_Part1_tca_thres_pw_ses_fe_Shift                                       10

/*--------------------------------------
BitField Name: tca_thres_pw_uas_fe
BitField Type: R/W
BitField Desc: TCA threshold of UAS_CNT
BitField Bits: [09:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_PW_Part1_tca_thres_pw_uas_fe_Mask                                   cBit9_0
#define cAf6_FMPM_TCA_threshold_of_PW_Part1_tca_thres_pw_uas_fe_Shift                                        0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA threshold of PW Part2
Reg Addr   : 0xB0520-0xB052E
Reg Formula: 0xB0520+$id*0x2
    Where  :
           + $id(0-7) : Type ID
Reg Desc   :
TCA threshold part2 of PW

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_threshold_of_PW_Part2_Base                                                    0xB0520
#define cAf6Reg_FMPM_TCA_threshold_of_PW_Part2(id)                                          (0xB0520+(id)*0x2)

/*--------------------------------------
BitField Name: tca_thres_pw_fc
BitField Type: R/W
BitField Desc: TCA threshold of FC
BitField Bits: [09:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_threshold_of_PW_Part2_tca_thres_pw_fc_Mask                                       cBit9_0
#define cAf6_FMPM_TCA_threshold_of_PW_Part2_tca_thres_pw_fc_Shift                                            0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Enable PM Collection Section
Reg Addr   : 0xB1800-0xB19FF
Reg Formula: 0xB1800+$L*0x40+$M*0x20+$N
    Where  :
           + $L(0-0) : is the TFI-5 line identifier
           + $M(0-0) : is 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s  (belong to that TFI-5 line)
           + $N(0-15) : is STS-1s in a STS-24 group
Reg Desc   :
Enable PM Collection Section

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Enable_PM_Collection_Section_Base                                                 0xB1800
#define cAf6Reg_FMPM_Enable_PM_Collection_Section(L, M, N)                     (0xB1800+(L)*0x40+(M)*0x20+(N))

/*--------------------------------------
BitField Name: enb_col_sec
BitField Type: R/W
BitField Desc: Enable PM Collection Section
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_Enable_PM_Collection_Section_enb_col_sec_Mask                                          cBit0
#define cAf6_FMPM_Enable_PM_Collection_Section_enb_col_sec_Shift                                             0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Enable PM Collection STS
Reg Addr   : 0xB1000-0xB11FF
Reg Formula: 0xB1000+$L*0x40+$M*0x20+$N
    Where  :
           + $L(0-3) : is the TFI-5 line identifier
           + $M(0-1) : is 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s  (belong to that TFI-5 line)
           + $N(0-23) : is STS-1s in a STS-24 group
Reg Desc   :
Enable PM STS

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Enable_PM_Collection_STS_Base                                                     0xB1000
#define cAf6Reg_FMPM_Enable_PM_Collection_STS(L, M, N)                         (0xB1000+(L)*0x40+(M)*0x20+(N))

/*--------------------------------------
BitField Name: enb_col_sts
BitField Type: R/W
BitField Desc: Enable PM Collection STS
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_Enable_PM_Collection_STS_enb_col_sts_Mask                                              cBit0
#define cAf6_FMPM_Enable_PM_Collection_STS_enb_col_sts_Shift                                                 0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Enable PM Collection DS3
Reg Addr   : 0xB1400-0xB15FF
Reg Formula: 0xB1400+$G*0x20+$H
    Where  :
           + $G(0-7) : DS3/E3 Group
           + $H(0-23) : DS3/E3 Framer
Reg Desc   :
Enbale PM DS3

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Enable_PM_Collection_DS3_Base                                                     0xB1400
#define cAf6Reg_FMPM_Enable_PM_Collection_DS3(G, H)                                     (0xB1400+(G)*0x20+(H))

/*--------------------------------------
BitField Name: enb_col_ds3
BitField Type: R/W
BitField Desc: Enable PM Collection DS3
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_Enable_PM_Collection_DS3_enb_col_ds3_Mask                                              cBit0
#define cAf6_FMPM_Enable_PM_Collection_DS3_enb_col_ds3_Shift                                                 0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Enable PM Collection VT
Reg Addr   : 0xB4000-0xB7FFF
Reg Formula: 0xB4000+$G*0x400+$H*0x20+$I
    Where  :
           + $G(0-7) : STS-24 LO group
           + $H(0-23) : STS-1 LO group
           + $I(0-27) : VT/TU ID
Reg Desc   :
Enable PM VT

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Enable_PM_Collection_VT_Base                                                      0xB4000
#define cAf6Reg_FMPM_Enable_PM_Collection_VT(G, H, I)                         (0xB4000+(G)*0x400+(H)*0x20+(I))

/*--------------------------------------
BitField Name: enb_col_vt
BitField Type: R/W
BitField Desc: Enable PM Collection VT
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_Enable_PM_Collection_VT_enb_col_vt_Mask                                                cBit0
#define cAf6_FMPM_Enable_PM_Collection_VT_enb_col_vt_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Enable PM Collection DS1
Reg Addr   : 0xB8000-0xBBFFF
Reg Formula: 0xB8000+$G*0x400+$H*0x20+$I
    Where  :
           + $G(0-7) : DS1/E1 Level-1 Group
           + $H(0-23) : DS1/E1 Level-2 Group
           + $I(0-27) : DS1/E1 Framer
Reg Desc   :
Enable PM DS1

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Enable_PM_Collection_DS1_Base                                                     0xB8000
#define cAf6Reg_FMPM_Enable_PM_Collection_DS1(G, H, I)                        (0xB8000+(G)*0x400+(H)*0x20+(I))

/*--------------------------------------
BitField Name: enb_col_ds1
BitField Type: R/W
BitField Desc: Enable PM Collection DS1
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_Enable_PM_Collection_DS1_enb_col_ds1_Mask                                              cBit0
#define cAf6_FMPM_Enable_PM_Collection_DS1_enb_col_ds1_Shift                                                 0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Enable PM Collection PW
Reg Addr   : 0xBC000-0xBFFFF
Reg Formula: 0xBC000+$id
    Where  :
           + $id(0-5736) : PW ID
Reg Desc   :
Enable PM PW

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Enable_PM_Collection_PW_Base                                                      0xBC000
#define cAf6Reg_FMPM_Enable_PM_Collection_PW(id)                                                (0xBC000+(id))

/*--------------------------------------
BitField Name: enb_col_pw
BitField Type: R/W
BitField Desc: Enable PM Collection PW
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_Enable_PM_Collection_PW_enb_col_pw_Mask                                                cBit0
#define cAf6_FMPM_Enable_PM_Collection_PW_enb_col_pw_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Threshold Type of Section
Reg Addr   : 0x00600-0x006FF
Reg Formula: 0x00600+$L*0x40+$M*0x20+$N
    Where  :
           + $L(0-0) : is the TFI-5 line identifier
           + $M(0-0) : is 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s  (belong to that TFI-5 line)
           + $N(0-15) : is STS-1s in a STS-24 group
Reg Desc   :
Threshold Type of Section

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Threshold_Type_of_Section_Base                                                    0x00600
#define cAf6Reg_FMPM_Threshold_Type_of_Section(L, M, N)                        (0x00600+(L)*0x40+(M)*0x20+(N))

/*--------------------------------------
BitField Name: K_TYPE
BitField Type: R/W
BitField Desc: Use to read K threshold table
BitField Bits: [10:08]
--------------------------------------*/
#define cAf6_FMPM_Threshold_Type_of_Section_K_TYPE_Mask                                               cBit10_8
#define cAf6_FMPM_Threshold_Type_of_Section_K_TYPE_Shift                                                     8

/*--------------------------------------
BitField Name: TCA_TYPE
BitField Type: R/W
BitField Desc: Use to read TCA threshold table
BitField Bits: [02:00]
--------------------------------------*/
#define cAf6_FMPM_Threshold_Type_of_Section_TCA_TYPE_Mask                                              cBit2_0
#define cAf6_FMPM_Threshold_Type_of_Section_TCA_TYPE_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Threshold Type of STS
Reg Addr   : 0x00200-0x003FF
Reg Formula: 0x00200+$L*0x40+$M*0x20+$N
    Where  :
           + $L(0-3) : is the TFI-5 line identifier
           + $M(0-1) : is 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s  (belong to that TFI-5 line)
           + $N(0-23) : is STS-1s in a STS-24 group
Reg Desc   :
Threshold Type of STS

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Threshold_Type_of_STS_Base                                                        0x00200
#define cAf6Reg_FMPM_Threshold_Type_of_STS(L, M, N)                            (0x00200+(L)*0x40+(M)*0x20+(N))

/*--------------------------------------
BitField Name: K_TYPE
BitField Type: R/W
BitField Desc: Use to read K threshold table
BitField Bits: [10:08]
--------------------------------------*/
#define cAf6_FMPM_Threshold_Type_of_STS_K_TYPE_Mask                                                   cBit10_8
#define cAf6_FMPM_Threshold_Type_of_STS_K_TYPE_Shift                                                         8

/*--------------------------------------
BitField Name: TCA_TYPE
BitField Type: R/W
BitField Desc: Use to read TCA threshold table
BitField Bits: [02:00]
--------------------------------------*/
#define cAf6_FMPM_Threshold_Type_of_STS_TCA_TYPE_Mask                                                  cBit2_0
#define cAf6_FMPM_Threshold_Type_of_STS_TCA_TYPE_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Threshold Type of DS3
Reg Addr   : 0x00400-0x00520
Reg Formula: 0x00400+$G*0x20+$H
    Where  :
           + $G(0-7) : DS3/E3 Group
           + $H(0-23) : DS3/E3 Framer
Reg Desc   :
Threshold Type of DS3

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Threshold_Type_of_DS3_Base                                                        0x00400
#define cAf6Reg_FMPM_Threshold_Type_of_DS3(G, H)                                        (0x00400+(G)*0x20+(H))

/*--------------------------------------
BitField Name: K_TYPE
BitField Type: R/W
BitField Desc: Use to read K threshold table
BitField Bits: [10:08]
--------------------------------------*/
#define cAf6_FMPM_Threshold_Type_of_DS3_K_TYPE_Mask                                                   cBit10_8
#define cAf6_FMPM_Threshold_Type_of_DS3_K_TYPE_Shift                                                         8

/*--------------------------------------
BitField Name: TCA_TYPE
BitField Type: R/W
BitField Desc: Use to read TCA threshold table
BitField Bits: [02:00]
--------------------------------------*/
#define cAf6_FMPM_Threshold_Type_of_DS3_TCA_TYPE_Mask                                                  cBit2_0
#define cAf6_FMPM_Threshold_Type_of_DS3_TCA_TYPE_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Threshold Type of VT
Reg Addr   : 0x04000-0x07FFF
Reg Formula: 0x04000+$G*0x400+$H*0x20+$I
    Where  :
           + $G(0-7) : STS-24 LO group
           + $H(0-23) : STS-1 LO group
           + $I(0-27) : VT/TU ID
Reg Desc   :
Threshold Type of VT

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Threshold_Type_of_VT_Base                                                         0x04000
#define cAf6Reg_FMPM_Threshold_Type_of_VT(G, H, I)                            (0x04000+(G)*0x400+(H)*0x20+(I))

/*--------------------------------------
BitField Name: K_TYPE
BitField Type: R/W
BitField Desc: Use to read K threshold table
BitField Bits: [10:08]
--------------------------------------*/
#define cAf6_FMPM_Threshold_Type_of_VT_K_TYPE_Mask                                                    cBit10_8
#define cAf6_FMPM_Threshold_Type_of_VT_K_TYPE_Shift                                                          8

/*--------------------------------------
BitField Name: TCA_TYPE
BitField Type: R/W
BitField Desc: Use to read TCA threshold table
BitField Bits: [02:00]
--------------------------------------*/
#define cAf6_FMPM_Threshold_Type_of_VT_TCA_TYPE_Mask                                                   cBit2_0
#define cAf6_FMPM_Threshold_Type_of_VT_TCA_TYPE_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Threshold Type of DS1
Reg Addr   : 0x08000-0x0BFFF
Reg Formula: 0x08000+$G*0x400+$H*0x20+$I
    Where  :
           + $G(0-7) : DS1/E1 Level-1 Group
           + $H(0-23) : DS1/E1 Level-2 Group
           + $I(0-27) : DS1/E1 Framer
Reg Desc   :
Threshold Type of DS1

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Threshold_Type_of_DS1_Base                                                        0x08000
#define cAf6Reg_FMPM_Threshold_Type_of_DS1(G, H, I)                           (0x08000+(G)*0x400+(H)*0x20+(I))

/*--------------------------------------
BitField Name: K_TYPE
BitField Type: R/W
BitField Desc: Use to read K threshold table
BitField Bits: [10:08]
--------------------------------------*/
#define cAf6_FMPM_Threshold_Type_of_DS1_K_TYPE_Mask                                                   cBit10_8
#define cAf6_FMPM_Threshold_Type_of_DS1_K_TYPE_Shift                                                         8

/*--------------------------------------
BitField Name: TCA_TYPE
BitField Type: R/W
BitField Desc: Use to read TCA threshold table
BitField Bits: [02:00]
--------------------------------------*/
#define cAf6_FMPM_Threshold_Type_of_DS1_TCA_TYPE_Mask                                                  cBit2_0
#define cAf6_FMPM_Threshold_Type_of_DS1_TCA_TYPE_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Threshold Type of PW
Reg Addr   : 0x0C000-0x0DFFF
Reg Formula: 0x0C000+$id
    Where  :
           + $id(0-1343) : PW ID
Reg Desc   :
Threshold Type of PW

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Threshold_Type_of_PW_Base                                                         0x0C000
#define cAf6Reg_FMPM_Threshold_Type_of_PW(id)                                                   (0x0C000+(id))

/*--------------------------------------
BitField Name: K_TYPE
BitField Type: R/W
BitField Desc: Use to read K threshold table
BitField Bits: [10:08]
--------------------------------------*/
#define cAf6_FMPM_Threshold_Type_of_PW_K_TYPE_Mask                                                    cBit10_8
#define cAf6_FMPM_Threshold_Type_of_PW_K_TYPE_Shift                                                          8

/*--------------------------------------
BitField Name: TCA_TYPE
BitField Type: R/W
BitField Desc: Use to read TCA threshold table
BitField Bits: [02:00]
--------------------------------------*/
#define cAf6_FMPM_Threshold_Type_of_PW_TCA_TYPE_Mask                                                   cBit2_0
#define cAf6_FMPM_Threshold_Type_of_PW_TCA_TYPE_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM Interrupt
Reg Addr   : 0x14000
Reg Formula: 
    Where  : 
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_Interrupt                                                                      0x14000

/*--------------------------------------
BitField Name: PM_SWPAGE_Intr
BitField Type: R_O
BitField Desc: PM switch page status
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_FMPM_FM_Interrupt_PM_SWPAGE_Intr_Mask                                                       cBit8
#define cAf6_FMPM_FM_Interrupt_PM_SWPAGE_Intr_Shift                                                          8

/*--------------------------------------
BitField Name: FMPM_FM_Intr_PW
BitField Type: R_O
BitField Desc: PW    Interrupt
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_PW_Mask                                                      cBit5
#define cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_PW_Shift                                                         5

/*--------------------------------------
BitField Name: FMPM_FM_Intr_DS1E1
BitField Type: R_O
BitField Desc: DS1/E1 interrupt
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_DS1E1_Mask                                                   cBit4
#define cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_DS1E1_Shift                                                      4

/*--------------------------------------
BitField Name: FMPM_FM_Intr_DS3E3
BitField Type: R_O
BitField Desc: DS3/E3 interrupt
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_DS3E3_Mask                                                   cBit3
#define cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_DS3E3_Shift                                                      3

/*--------------------------------------
BitField Name: FMPM_FM_Intr_VTGTUG
BitField Type: R_O
BitField Desc: VTG/TUG interrupt
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_VTGTUG_Mask                                                  cBit2
#define cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_VTGTUG_Shift                                                     2

/*--------------------------------------
BitField Name: FMPM_FM_Intr_STSAU
BitField Type: R_O
BitField Desc: STS/AU interrupt
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_STSAU_Mask                                                   cBit1
#define cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_STSAU_Shift                                                      1

/*--------------------------------------
BitField Name: FMPM_FM_Intr_ECSTM
BitField Type: R_O
BitField Desc: ECSTM interrupt
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_ECSTM_Mask                                                   cBit0
#define cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_ECSTM_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM Interrupt Mask
Reg Addr   : 0x14001
Reg Formula: 
    Where  : 
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_Interrupt_Mask                                                                 0x14001

/*--------------------------------------
BitField Name: PM_SWPAGE_Intr_MSK
BitField Type: R/W
BitField Desc: PM switch page mask
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_FMPM_FM_Interrupt_Mask_PM_SWPAGE_Intr_MSK_Mask                                              cBit8
#define cAf6_FMPM_FM_Interrupt_Mask_PM_SWPAGE_Intr_MSK_Shift                                                 8

/*--------------------------------------
BitField Name: FMPM_FM_Intr_MSK_PW
BitField Type: R/W
BitField Desc: PW interrupt
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_FM_Interrupt_Mask_FMPM_FM_Intr_MSK_PW_Mask                                             cBit5
#define cAf6_FMPM_FM_Interrupt_Mask_FMPM_FM_Intr_MSK_PW_Shift                                                5

/*--------------------------------------
BitField Name: FMPM_FM_Intr_MSK_DS1E1
BitField Type: R/W
BitField Desc: DS1/E1 interrupt
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_FM_Interrupt_Mask_FMPM_FM_Intr_MSK_DS1E1_Mask                                          cBit4
#define cAf6_FMPM_FM_Interrupt_Mask_FMPM_FM_Intr_MSK_DS1E1_Shift                                             4

/*--------------------------------------
BitField Name: FMPM_FM_Intr_MSK_DS3E3
BitField Type: R/W
BitField Desc: DS3/E3 interrupt
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_FM_Interrupt_Mask_FMPM_FM_Intr_MSK_DS3E3_Mask                                          cBit3
#define cAf6_FMPM_FM_Interrupt_Mask_FMPM_FM_Intr_MSK_DS3E3_Shift                                             3

/*--------------------------------------
BitField Name: FMPM_FM_Intr_MSK_VTGTUG
BitField Type: R/W
BitField Desc: VTG/TUG interrupt
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_FM_Interrupt_Mask_FMPM_FM_Intr_MSK_VTGTUG_Mask                                         cBit2
#define cAf6_FMPM_FM_Interrupt_Mask_FMPM_FM_Intr_MSK_VTGTUG_Shift                                            2

/*--------------------------------------
BitField Name: FMPM_FM_Intr_MSK_STSAU
BitField Type: R/W
BitField Desc: STS/AU interrupt
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_FM_Interrupt_Mask_FMPM_FM_Intr_MSK_STSAU_Mask                                          cBit1
#define cAf6_FMPM_FM_Interrupt_Mask_FMPM_FM_Intr_MSK_STSAU_Shift                                             1

/*--------------------------------------
BitField Name: FMPM_FM_Intr_MSK_ECSTM
BitField Type: R/W
BitField Desc: ECSTM interrupt
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_FM_Interrupt_Mask_FMPM_FM_Intr_MSK_ECSTM_Mask                                          cBit0
#define cAf6_FMPM_FM_Interrupt_Mask_FMPM_FM_Intr_MSK_ECSTM_Shift                                             0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM EC1 Interrupt OR
Reg Addr   : 0x18005
Reg Formula: 
    Where  : 
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_EC1_Interrupt_OR                                                               0x18005

/*--------------------------------------
BitField Name: FMPM_FM_EC1_OR
BitField Type: R_O
BitField Desc: Interrupt EC1 OR
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_FMPM_FM_EC1_Interrupt_OR_FMPM_FM_EC1_OR_Mask                                              cBit1_0
#define cAf6_FMPM_FM_EC1_Interrupt_OR_FMPM_FM_EC1_OR_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM EC1 Interrupt OR AND MASK Per STS1
Reg Addr   : 0x18100-0x1810F
Reg Formula: 0x18100+$L*0x2+$M
    Where  : 
           + $L(0-0) : TFI-5 line identifier
           + $M(0-1) : 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_EC1_Interrupt_OR_AND_MASK_Per_STS1_Base                                        0x18100
#define cAf6Reg_FMPM_FM_EC1_Interrupt_OR_AND_MASK_Per_STS1(L, M)                         (0x18100+(L)*0x2+(M))

/*--------------------------------------
BitField Name: FMPM_FM_EC1_OAMSK
BitField Type: R_O
BitField Desc: Interrupt STS1 OR AND MASK
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_FMPM_FM_EC1_Interrupt_OR_AND_MASK_Per_STS1_FMPM_FM_EC1_OAMSK_Mask                                cBit23_0
#define cAf6_FMPM_FM_EC1_Interrupt_OR_AND_MASK_Per_STS1_FMPM_FM_EC1_OAMSK_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM EC1 Interrupt MASK Per STS1
Reg Addr   : 0x18110-0x1811F
Reg Formula: 0x18110+$L*0x2+$M
    Where  : 
           + $L(0-0) : TFI-5 line identifier
           + $M(0-1) : 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_EC1_Interrupt_MASK_Per_STS1_Base                                               0x18110
#define cAf6Reg_FMPM_FM_EC1_Interrupt_MASK_Per_STS1(L, M)                                (0x18110+(L)*0x2+(M))

/*--------------------------------------
BitField Name: FMPM_FM_EC1_MSK
BitField Type: R/W
BitField Desc: Interrupt MASK per STS1
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_FMPM_FM_EC1_Interrupt_MASK_Per_STS1_FMPM_FM_EC1_MSK_Mask                                 cBit23_0
#define cAf6_FMPM_FM_EC1_Interrupt_MASK_Per_STS1_FMPM_FM_EC1_MSK_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM EC1STM0 Interrupt Sticky Per Type Of Per STS1
Reg Addr   : 0x1B600-0x1B7FF
Reg Formula: 0x1B600+$L*0x40+$M*0x20+$N
    Where  : 
           + $L(0-0) : TFI-5 line identifier
           + $M(0-1) : 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s
           + $N(0-23) : STS-1s in a STS-24 group will be numbered from 0 to 23
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_Base                                 0x1B600
#define cAf6Reg_FMPM_FM_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1(L, M, N)         (0x1B600+(L)*0x40+(M)*0x20+(N))

/*--------------------------------------
BitField Name: FMPM_FM_EC1STM0_STK_LOS_S
BitField Type: W1C
BitField Desc: LOS_S
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_STK_LOS_S_Mask                                   cBit6
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_STK_LOS_S_Shift                                       6

/*--------------------------------------
BitField Name: FMPM_FM_EC1STM0_STK_LOF_S
BitField Type: W1C
BitField Desc: LOF_S
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_STK_LOF_S_Mask                                   cBit5
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_STK_LOF_S_Shift                                       5

/*--------------------------------------
BitField Name: FMPM_FM_EC1STM0_STK_AIS_L
BitField Type: W1C
BitField Desc: AIS_L
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_STK_AIS_L_Mask                                   cBit4
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_STK_AIS_L_Shift                                       4

/*--------------------------------------
BitField Name: FMPM_FM_EC1STM0_STK_RFI_L
BitField Type: W1C
BitField Desc: RFI_L
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_STK_RFI_L_Mask                                   cBit3
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_STK_RFI_L_Shift                                       3

/*--------------------------------------
BitField Name: FMPM_FM_EC1STM0_STK_TIM_L
BitField Type: W1C
BitField Desc: TIM_L
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_STK_TIM_S_Mask                                   cBit2
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_STK_TIM_S_Shift                                       2

/*--------------------------------------
BitField Name: FMPM_FM_EC1STM0_STK_BERSD_L
BitField Type: W1C
BitField Desc: BERSD_L
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_STK_BERSD_L_Mask                                   cBit1
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_STK_BERSD_L_Shift                                       1

/*--------------------------------------
BitField Name: FMPM_FM_EC1STM0_STK_BERSF_L
BitField Type: W1C
BitField Desc: BERSF_L
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_STK_BERSF_L_Mask                                   cBit0
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_STK_BERSF_L_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM EC1STM0 Interrupt MASK Per Type Of Per STS1
Reg Addr   : 0x1B800-0x1B9FF
Reg Formula: 0x1B800+$L*0x40+$M*0x20+$N
    Where  : 
           + $L(0-0) : TFI-5 line identifier
           + $M(0-1) : 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s
           + $N(0-23) : STS-1s in a STS-24 group will be numbered from 0 to 23
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_Base                                 0x1B800
#define cAf6Reg_FMPM_FM_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1(L, M, N)         (0x1B800+(L)*0x40+(M)*0x20+(N))

/*--------------------------------------
BitField Name: FMPM_FM_EC1STM0_HI_AIS_L
BitField Type: R/W
BitField Desc: High level AIS-L
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_FMPM_FM_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_HI_AIS_L_Mask                                   cBit7
#define cAf6_FMPM_FM_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_HI_AIS_L_Shift                                       7

/*--------------------------------------
BitField Name: FMPM_FM_EC1STM0_LOS_S
BitField Type: R/W
BitField Desc: LOS_S
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_FMPM_FM_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_LOS_S_Mask                                   cBit6
#define cAf6_FMPM_FM_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_LOS_S_Shift                                       6

/*--------------------------------------
BitField Name: FMPM_FM_EC1STM0_LOF_S
BitField Type: R/W
BitField Desc: LOF_S
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_FM_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_LOF_S_Mask                                   cBit5
#define cAf6_FMPM_FM_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_LOF_S_Shift                                       5

/*--------------------------------------
BitField Name: FMPM_FM_EC1STM0_AIS_L
BitField Type: R/W
BitField Desc: AIS_L
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_FM_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_AIS_L_Mask                                   cBit4
#define cAf6_FMPM_FM_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_AIS_L_Shift                                       4

/*--------------------------------------
BitField Name: FMPM_FM_EC1STM0_RFI_L
BitField Type: R/W
BitField Desc: RFI_L
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_FM_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_RFI_L_Mask                                   cBit3
#define cAf6_FMPM_FM_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_RFI_L_Shift                                       3

/*--------------------------------------
BitField Name: FMPM_FM_EC1STM0_TIM_L
BitField Type: R/W
BitField Desc: TIM_L
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_FM_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_TIM_S_Mask                                   cBit2
#define cAf6_FMPM_FM_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_TIM_S_Shift                                       2

/*--------------------------------------
BitField Name: FMPM_FM_EC1STM0_BERSD_L
BitField Type: R/W
BitField Desc: BERSD_L
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_FM_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_BERSD_L_Mask                                   cBit1
#define cAf6_FMPM_FM_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_BERSD_L_Shift                                       1

/*--------------------------------------
BitField Name: FMPM_FM_EC1STM0_BERSF_L
BitField Type: R/W
BitField Desc: BERSF_L
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_FM_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_BERSF_L_Mask                                   cBit0
#define cAf6_FMPM_FM_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_BERSF_L_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM EC1STM0 Interrupt Current Status Per Type Of Per STS1
Reg Addr   : 0x1BA00-0x1BBFF
Reg Formula: 0x1BA00+$L*0x40+$M*0x20+$N
    Where  : 
           + $L(0-0) : TFI-5 line identifier
           + $M(0-1) : 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s
           + $N(0-23) : STS-1s in a STS-24 group will be numbered from 0 to 23
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1_Base                                 0x1BA00
#define cAf6Reg_FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1(L, M, N)         (0x1BA00+(L)*0x40+(M)*0x20+(N))

/*--------------------------------------
BitField Name: FMPM_FM_EC1STM0_AMSK_HI_AIS_L
BitField Type: R_O
BitField Desc: High level AIS-L
BitField Bits: [23:23]
--------------------------------------*/
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_AMSK_HI_AIS_L_Mask                                  cBit23
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_AMSK_HI_AIS_L_Shift                                      23

/*--------------------------------------
BitField Name: FMPM_FM_EC1STM0_AMSK_LOS_S
BitField Type: R_O
BitField Desc: LOS_S
BitField Bits: [22:22]
--------------------------------------*/
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_AMSK_LOS_S_Mask                                  cBit22
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_AMSK_LOS_S_Shift                                      22

/*--------------------------------------
BitField Name: FMPM_FM_EC1STM0_AMSK_LOF_S
BitField Type: R_O
BitField Desc: LOF_S
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_AMSK_LOF_S_Mask                                  cBit21
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_AMSK_LOF_S_Shift                                      21

/*--------------------------------------
BitField Name: FMPM_FM_EC1STM0_AMSK_AIS_L
BitField Type: R_O
BitField Desc: AIS_L
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_AMSK_AIS_L_Mask                                  cBit20
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_AMSK_AIS_L_Shift                                      20

/*--------------------------------------
BitField Name: FMPM_FM_EC1STM0_AMSK_RFI_L
BitField Type: R_O
BitField Desc: RFI_L
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_AMSK_RFI_L_Mask                                  cBit19
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_AMSK_RFI_L_Shift                                      19

/*--------------------------------------
BitField Name: FMPM_FM_EC1STM0_AMSK_TIM_L
BitField Type: R_O
BitField Desc: TIM_L
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_AMSK_TIM_S_Mask                                  cBit18
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_AMSK_TIM_S_Shift                                      18

/*--------------------------------------
BitField Name: FMPM_FM_EC1STM0_AMSK_BERSD_L
BitField Type: R_O
BitField Desc: BERSD_L
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_AMSK_BERSD_L_Mask                                  cBit17
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_AMSK_BERSD_L_Shift                                      17

/*--------------------------------------
BitField Name: FMPM_FM_EC1STM0_AMSK_BERSF_L
BitField Type: R_O
BitField Desc: BERSF_L
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_AMSK_BERSF_L_Mask                                  cBit16
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_AMSK_BERSF_L_Shift                                      16

/*--------------------------------------
BitField Name: FMPM_FM_EC1STM0_CUR_HI_AIS_L
BitField Type: R_O
BitField Desc: High level AIS_L
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_CUR_HI_AIS_L_Mask                                   cBit7
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_CUR_HI_AIS_L_Shift                                       7

/*--------------------------------------
BitField Name: FMPM_FM_EC1STM0_CUR_LOS_S
BitField Type: R_O
BitField Desc: LOS_S
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_CUR_LOS_S_Mask                                   cBit6
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_CUR_LOS_S_Shift                                       6

/*--------------------------------------
BitField Name: FMPM_FM_EC1STM0_CUR_LOF_S
BitField Type: R_O
BitField Desc: LOF_S
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_CUR_LOF_S_Mask                                   cBit5
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_CUR_LOF_S_Shift                                       5

/*--------------------------------------
BitField Name: FMPM_FM_EC1STM0_CUR_AIS_L
BitField Type: R_O
BitField Desc: AIS_L
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_CUR_AIS_L_Mask                                   cBit4
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_CUR_AIS_L_Shift                                       4

/*--------------------------------------
BitField Name: FMPM_FM_EC1STM0_CUR_RFI_L
BitField Type: R_O
BitField Desc: RFI_L
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_CUR_RFI_L_Mask                                   cBit3
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_CUR_RFI_L_Shift                                       3

/*--------------------------------------
BitField Name: FMPM_FM_EC1STM0_CUR_TIM_L
BitField Type: R_O
BitField Desc: TIM_L
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_CUR_TIM_S_Mask                                   cBit2
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_CUR_TIM_S_Shift                                       2

/*--------------------------------------
BitField Name: FMPM_FM_EC1STM0_CUR_BERSD_L
BitField Type: R_O
BitField Desc: BERSD_L
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_CUR_BERSD_L_Mask                                   cBit1
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_CUR_BERSD_L_Shift                                       1

/*--------------------------------------
BitField Name: FMPM_FM_EC1STM0_CUR_BERSF_L
BitField Type: R_O
BitField Desc: BERSF_L
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_CUR_BERSF_L_Mask                                   cBit0
#define cAf6_FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_EC1STM0_CUR_BERSF_L_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM STS24 Interrupt OR
Reg Addr   : 0x18000
Reg Formula: 
    Where  : 
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_STS24_Interrupt_OR                                                             0x18000

/*--------------------------------------
BitField Name: FMPM_FM_STS24_OR
BitField Type: R_O
BitField Desc: Interrupt STS24 OR
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_OR_FMPM_FM_STS24_OR_Mask                                         cBit15_0
#define cAf6_FMPM_FM_STS24_Interrupt_OR_FMPM_FM_STS24_OR_Shift                                               0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM STS24 Interrupt OR AND MASK Per STS1
Reg Addr   : 0x18080-0x1808F
Reg Formula: 0x18080+$L*0x2+$M
    Where  : 
           + $L(0-7) : TFI-5 line identifier
           + $M(0-1) : 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_STS24_Interrupt_OR_AND_MASK_Per_STS1_Base                                      0x18080
#define cAf6Reg_FMPM_FM_STS24_Interrupt_OR_AND_MASK_Per_STS1(L, M)                       (0x18080+(L)*0x2+(M))

/*--------------------------------------
BitField Name: FMPM_FM_STS1_OAMSK
BitField Type: R_O
BitField Desc: Interrupt STS1 OR AND MASK
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_OR_AND_MASK_Per_STS1_FMPM_FM_STS1_OAMSK_Mask                                cBit23_0
#define cAf6_FMPM_FM_STS24_Interrupt_OR_AND_MASK_Per_STS1_FMPM_FM_STS1_OAMSK_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM STS24 Interrupt MASK Per STS1
Reg Addr   : 0x18090-0x1809F
Reg Formula: 0x18090+$L*0x2+$M
    Where  : 
           + $L(0-7) : TFI-5 line identifier
           + $M(0-1) : 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_STS24_Interrupt_MASK_Per_STS1_Base                                             0x18090
#define cAf6Reg_FMPM_FM_STS24_Interrupt_MASK_Per_STS1(L, M)                              (0x18090+(L)*0x2+(M))

/*--------------------------------------
BitField Name: FMPM_FM_STS1_MSK
BitField Type: R/W
BitField Desc: Interrupt MASK per STS1
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_MASK_Per_STS1_FMPM_FM_STS1_MSK_Mask                                cBit23_0
#define cAf6_FMPM_FM_STS24_Interrupt_MASK_Per_STS1_FMPM_FM_STS1_MSK_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM STS24 Interrupt Sticky Per Type Of Per STS1
Reg Addr   : 0x1A000-0x1A1FF
Reg Formula: 0x1A000+$L*0x40+$M*0x20+$N
    Where  : 
           + $L(0-7) : TFI-5 line identifier
           + $M(0-1) : 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s
           + $N(0-23) : STS-1s in a STS-24 group will be numbered from 0 to 23
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_Base                                 0x1A000
#define cAf6Reg_FMPM_FM_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1(L, M, N)         (0x1A000+(L)*0x40+(M)*0x20+(N))

/*--------------------------------------
BitField Name: FMPM_FM_STS1_STK_RFI_SER
BitField Type: W1C
BitField Desc: RFI server
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_FM_STS1_STK_RFI_SER_Mask                                  cBit11
#define cAf6_FMPM_FM_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_FM_STS1_STK_RFI_SER_Shift                                      11

/*--------------------------------------
BitField Name: FMPM_FM_STS1_STK_RFI_CON
BitField Type: W1C
BitField Desc: RFI connectivity
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_FM_STS1_STK_RFI_CON_Mask                                  cBit10
#define cAf6_FMPM_FM_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_FM_STS1_STK_RFI_CON_Shift                                      10

/*--------------------------------------
BitField Name: FMPM_FM_STS1_STK_RFI_PAY
BitField Type: W1C
BitField Desc: RFI payload
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_FM_STS1_STK_RFI_PAY_Mask                                   cBit9
#define cAf6_FMPM_FM_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_FM_STS1_STK_RFI_PAY_Shift                                       9

/*--------------------------------------
BitField Name: FMPM_FM_STS1_STK_LOM
BitField Type: W1C
BitField Desc: LOM
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_FM_STS1_STK_LOM_Mask                                   cBit8
#define cAf6_FMPM_FM_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_FM_STS1_STK_LOM_Shift                                       8

/*--------------------------------------
BitField Name: FMPM_FM_STS1_STK_RFI
BitField Type: W1C
BitField Desc: one-bit RFI
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_FM_STS1_STK_RFI_Mask                                   cBit7
#define cAf6_FMPM_FM_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_FM_STS1_STK_RFI_Shift                                       7

/*--------------------------------------
BitField Name: FMPM_FM_STS1_STK_AIS
BitField Type: W1C
BitField Desc: AIS
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_FM_STS1_STK_AIS_Mask                                   cBit6
#define cAf6_FMPM_FM_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_FM_STS1_STK_AIS_Shift                                       6

/*--------------------------------------
BitField Name: FMPM_FM_STS1_STK_TIM
BitField Type: W1C
BitField Desc: TIM
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_FM_STS1_STK_TIM_Mask                                   cBit5
#define cAf6_FMPM_FM_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_FM_STS1_STK_TIM_Shift                                       5

/*--------------------------------------
BitField Name: FMPM_FM_STS1_STK_UNEQ
BitField Type: W1C
BitField Desc: UNEQ
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_FM_STS1_STK_UNEQ_Mask                                   cBit4
#define cAf6_FMPM_FM_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_FM_STS1_STK_UNEQ_Shift                                       4

/*--------------------------------------
BitField Name: FMPM_FM_STS1_STK_PLM
BitField Type: W1C
BitField Desc: PLM
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_FM_STS1_STK_PLM_Mask                                   cBit3
#define cAf6_FMPM_FM_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_FM_STS1_STK_PLM_Shift                                       3

/*--------------------------------------
BitField Name: FMPM_FM_STS1_STK_LOP
BitField Type: W1C
BitField Desc: LOP
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_FM_STS1_STK_LOP_Mask                                   cBit2
#define cAf6_FMPM_FM_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_FM_STS1_STK_LOP_Shift                                       2

/*--------------------------------------
BitField Name: FMPM_FM_STS1_STK_BERSD
BitField Type: W1C
BitField Desc: BERSD
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_FM_STS1_STK_BERSD_Mask                                   cBit1
#define cAf6_FMPM_FM_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_FM_STS1_STK_BERSD_Shift                                       1

/*--------------------------------------
BitField Name: FMPM_FM_STS1_STK_BERSF
BitField Type: W1C
BitField Desc: BERSF
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_FM_STS1_STK_BERSF_Mask                                   cBit0
#define cAf6_FMPM_FM_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_FM_STS1_STK_BERSF_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM STS24 Interrupt MASK Per Type Of Per STS1
Reg Addr   : 0x1A200-0x1A3FF
Reg Formula: 0x1A200+$L*0x40+$M*0x20+$N
    Where  : 
           + $L(0-7) : TFI-5 line identifier
           + $M(0-1) : 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s
           + $N(0-23) : STS-1s in a STS-24 group will be numbered from 0 to 23
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_Base                                 0x1A200
#define cAf6Reg_FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1(L, M, N)         (0x1A200+(L)*0x40+(M)*0x20+(N))

/*--------------------------------------
BitField Name: FMPM_FM_STS1_MSK_HI_AIS
BitField Type: R/W
BitField Desc: High level AIS
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_FM_STS1_MSK_HI_AIS_Mask                                  cBit12
#define cAf6_FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_FM_STS1_MSK_HI_AIS_Shift

/*--------------------------------------
BitField Name: FMPM_FM_STS1_MSK_RFI_SER
BitField Type: R/W
BitField Desc: RFI server
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_FM_STS1_MSK_RFI_SER_Mask                                  cBit11
#define cAf6_FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_FM_STS1_MSK_RFI_SER_Shift                                      11

/*--------------------------------------
BitField Name: FMPM_FM_STS1_MSK_RFI_CON
BitField Type: R/W
BitField Desc: RFI connectivity
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_FM_STS1_MSK_RFI_CON_Mask                                  cBit10
#define cAf6_FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_FM_STS1_MSK_RFI_CON_Shift                                      10

/*--------------------------------------
BitField Name: FMPM_FM_STS1_MSK_RFI_PAY
BitField Type: R/W
BitField Desc: RFI payload
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_FM_STS1_MSK_RFI_PAY_Mask                                   cBit9
#define cAf6_FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_FM_STS1_MSK_RFI_PAY_Shift                                       9

/*--------------------------------------
BitField Name: FMPM_FM_STS1_MSK_LOM
BitField Type: R/W
BitField Desc: LOM
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_FM_STS1_MSK_LOM_Mask                                   cBit8
#define cAf6_FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_FM_STS1_MSK_LOM_Shift                                       8

/*--------------------------------------
BitField Name: FMPM_FM_STS1_MSK_RFI
BitField Type: R/W
BitField Desc: one-bit RFI
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_FM_STS1_MSK_RFI_Mask                                   cBit7
#define cAf6_FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_FM_STS1_MSK_RFI_Shift                                       7

/*--------------------------------------
BitField Name: FMPM_FM_STS1_MSK_AIS
BitField Type: R/W
BitField Desc: AIS
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_FM_STS1_MSK_AIS_Mask                                   cBit6
#define cAf6_FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_FM_STS1_MSK_AIS_Shift                                       6

/*--------------------------------------
BitField Name: FMPM_FM_STS1_MSK_TIM
BitField Type: R/W
BitField Desc: TIM
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_FM_STS1_MSK_TIM_Mask                                   cBit5
#define cAf6_FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_FM_STS1_MSK_TIM_Shift                                       5

/*--------------------------------------
BitField Name: FMPM_FM_STS1_MSK_UNEQ
BitField Type: R/W
BitField Desc: UNEQ
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_FM_STS1_MSK_UNEQ_Mask                                   cBit4
#define cAf6_FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_FM_STS1_MSK_UNEQ_Shift                                       4

/*--------------------------------------
BitField Name: FMPM_FM_STS1_MSK_PLM
BitField Type: R/W
BitField Desc: PLM
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_FM_STS1_MSK_PLM_Mask                                   cBit3
#define cAf6_FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_FM_STS1_MSK_PLM_Shift                                       3

/*--------------------------------------
BitField Name: FMPM_FM_STS1_MSK_LOP
BitField Type: R/W
BitField Desc: LOP
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_FM_STS1_MSK_LOP_Mask                                   cBit2
#define cAf6_FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_FM_STS1_MSK_LOP_Shift                                       2

/*--------------------------------------
BitField Name: FMPM_FM_STS1_MSK_BERSD
BitField Type: R/W
BitField Desc: BERSD
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_FM_STS1_MSK_BERSD_Mask                                   cBit1
#define cAf6_FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_FM_STS1_MSK_BERSD_Shift                                       1

/*--------------------------------------
BitField Name: FMPM_FM_STS1_MSK_BERSF
BitField Type: R/W
BitField Desc: BERSF
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_FM_STS1_MSK_BERSF_Mask                                   cBit0
#define cAf6_FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_FM_STS1_MSK_BERSF_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM STS24 Interrupt Current Status Per Type Of Per STS1
Reg Addr   : 0x1A400-0x1A5FF
Reg Formula: 0x1A400+$L*0x40+$M*0x20+$N
    Where  : 
           + $L(0-7) : TFI-5 line identifier
           + $M(0-1) : 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s
           + $N(0-23) : STS-1s in a STS-24 group will be numbered from 0 to 23
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_Base                                 0x1A400
#define cAf6Reg_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1(L, M, N)         (0x1A400+(L)*0x40+(M)*0x20+(N))

/*--------------------------------------
BitField Name: FMPM_FM_STS1_AMSK_HI_AIS
BitField Type: R_O
BitField Desc: High level AIS
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_AMSK_HI_AIS_Mask                                  cBit28
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_AMSK_HI_AIS_Shift                                      28

/*--------------------------------------
BitField Name: FMPM_FM_STS1_AMSK_RFI_SER
BitField Type: R_O
BitField Desc: RFI server
BitField Bits: [27:27]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_AMSK_RFI_SER_Mask                                  cBit27
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_AMSK_RFI_SER_Shift                                      27

/*--------------------------------------
BitField Name: FMPM_FM_STS1_AMSK_RFI_CON
BitField Type: R_O
BitField Desc: RFI connectivity
BitField Bits: [26:26]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_AMSK_RFI_CON_Mask                                  cBit26
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_AMSK_RFI_CON_Shift                                      26

/*--------------------------------------
BitField Name: FMPM_FM_STS1_AMSK_RFI_PAY
BitField Type: R_O
BitField Desc: RFI payload
BitField Bits: [25:25]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_AMSK_RFI_PAY_Mask                                  cBit25
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_AMSK_RFI_PAY_Shift                                      25

/*--------------------------------------
BitField Name: FMPM_FM_STS1_AMSK_LOM
BitField Type: R_O
BitField Desc: LOM
BitField Bits: [24:24]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_AMSK_LOM_Mask                                  cBit24
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_AMSK_LOM_Shift                                      24

/*--------------------------------------
BitField Name: FMPM_FM_STS1_AMSK_RFI
BitField Type: R_O
BitField Desc: one-bit RFI
BitField Bits: [23:23]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_AMSK_RFI_Mask                                  cBit23
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_AMSK_RFI_Shift                                      23

/*--------------------------------------
BitField Name: FMPM_FM_STS1_AMSK_AIS
BitField Type: R_O
BitField Desc: AIS
BitField Bits: [22:22]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_AMSK_AIS_Mask                                  cBit22
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_AMSK_AIS_Shift                                      22

/*--------------------------------------
BitField Name: FMPM_FM_STS1_AMSK_TIM
BitField Type: R_O
BitField Desc: TIM
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_AMSK_TIM_Mask                                  cBit21
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_AMSK_TIM_Shift                                      21

/*--------------------------------------
BitField Name: FMPM_FM_STS1_AMSK_UNEQ
BitField Type: R_O
BitField Desc: UNEQ
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_AMSK_UNEQ_Mask                                  cBit20
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_AMSK_UNEQ_Shift                                      20

/*--------------------------------------
BitField Name: FMPM_FM_STS1_AMSK_PLM
BitField Type: R_O
BitField Desc: PLM
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_AMSK_PLM_Mask                                  cBit19
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_AMSK_PLM_Shift                                      19

/*--------------------------------------
BitField Name: FMPM_FM_STS1_AMSK_LOP
BitField Type: R_O
BitField Desc: LOP
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_AMSK_LOP_Mask                                  cBit18
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_AMSK_LOP_Shift                                      18

/*--------------------------------------
BitField Name: FMPM_FM_STS1_AMSK_BERSD
BitField Type: R_O
BitField Desc: BERSD
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_AMSK_BERSD_Mask                                  cBit17
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_AMSK_BERSD_Shift                                      17

/*--------------------------------------
BitField Name: FMPM_FM_STS1_AMSK_BERSF
BitField Type: R_O
BitField Desc: BERSF
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_AMSK_BERSF_Mask                                  cBit16
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_AMSK_BERSF_Shift                                      16

/*--------------------------------------
BitField Name: FMPM_FM_STS1_CUR_HI_AIS
BitField Type: R_O
BitField Desc: High level AIS
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_CUR_HI_AIS_Mask                                  cBit12
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_CUR_HI_AIS_Shift                                      12

/*--------------------------------------
BitField Name: FMPM_FM_STS1_CUR_RFI_SER
BitField Type: R_O
BitField Desc: RFI server
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_CUR_RFI_SER_Mask                                  cBit11
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_CUR_RFI_SER_Shift                                      11

/*--------------------------------------
BitField Name: FMPM_FM_STS1_CUR_RFI_CON
BitField Type: R_O
BitField Desc: RFI connectivity
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_CUR_RFI_CON_Mask                                  cBit10
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_CUR_RFI_CON_Shift                                      10

/*--------------------------------------
BitField Name: FMPM_FM_STS1_CUR_RFI_PAY
BitField Type: R_O
BitField Desc: RFI payload
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_CUR_RFI_PAY_Mask                                   cBit9
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_CUR_RFI_PAY_Shift                                       9

/*--------------------------------------
BitField Name: FMPM_FM_STS1_CUR_LOM
BitField Type: R_O
BitField Desc: LOM
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_CUR_LOM_Mask                                   cBit8
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_CUR_LOM_Shift                                       8

/*--------------------------------------
BitField Name: FMPM_FM_STS1_CUR_RFI
BitField Type: R_O
BitField Desc: one-bit RFI
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_CUR_RFI_Mask                                   cBit7
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_CUR_RFI_Shift                                       7

/*--------------------------------------
BitField Name: FMPM_FM_STS1_CUR_AIS
BitField Type: R_O
BitField Desc: AIS
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_CUR_AIS_Mask                                   cBit6
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_CUR_AIS_Shift                                       6

/*--------------------------------------
BitField Name: FMPM_FM_STS1_CUR_TIM
BitField Type: R_O
BitField Desc: TIM
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_CUR_TIM_Mask                                   cBit5
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_CUR_TIM_Shift                                       5

/*--------------------------------------
BitField Name: FMPM_FM_STS1_CUR_UNEQ
BitField Type: R_O
BitField Desc: UNEQ
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_CUR_UNEQ_Mask                                   cBit4
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_CUR_UNEQ_Shift                                       4

/*--------------------------------------
BitField Name: FMPM_FM_STS1_CUR_PLM
BitField Type: R_O
BitField Desc: PLM
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_CUR_PLM_Mask                                   cBit3
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_CUR_PLM_Shift                                       3

/*--------------------------------------
BitField Name: FMPM_FM_STS1_CUR_LOP
BitField Type: R_O
BitField Desc: LOP
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_CUR_LOP_Mask                                   cBit2
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_CUR_LOP_Shift                                       2

/*--------------------------------------
BitField Name: FMPM_FM_STS1_CUR_BERSD
BitField Type: R_O
BitField Desc: BERSD
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_CUR_BERSD_Mask                                   cBit1
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_CUR_BERSD_Shift                                       1

/*--------------------------------------
BitField Name: FMPM_FM_STS1_CUR_BERSF
BitField Type: R_O
BitField Desc: BERSF
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_CUR_BERSF_Mask                                   cBit0
#define cAf6_FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1_FMPM_FM_STS1_CUR_BERSF_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM DS3E3 Group Interrupt OR
Reg Addr   : 0x18001
Reg Formula: 
    Where  : 
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_DS3E3_Group_Interrupt_OR                                                       0x18001

/*--------------------------------------
BitField Name: FMPM_FM_DS3E3_OR
BitField Type: R_O
BitField Desc: Interrupt DS3/E3 OR
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_FMPM_FM_DS3E3_Group_Interrupt_OR_FMPM_FM_DS3E3_OR_Mask                                   cBit11_0
#define cAf6_FMPM_FM_DS3E3_Group_Interrupt_OR_FMPM_FM_DS3E3_OR_Shift                                         0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM DS3E3 Framer Interrupt OR AND MASK Per DS3E3
Reg Addr   : 0x180A0-0x180AF
Reg Formula: 0x180A0+$G
    Where  : 
           + $G(0-11) : DS3/E3 Group  Interrupt
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_DS3E3_Framer_Interrupt_OR_AND_MASK_Per_DS3E3_Base                                 0x180A0
#define cAf6Reg_FMPM_FM_DS3E3_Framer_Interrupt_OR_AND_MASK_Per_DS3E3(G)                           (0x180A0+(G))

/*--------------------------------------
BitField Name: FMPM_FM_DS3E3_OAMSK
BitField Type: R_O
BitField Desc: Interrupt DS3/E3 Group
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_OR_AND_MASK_Per_DS3E3_FMPM_FM_DS3E3_OAMSK_Mask                                cBit23_0
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_OR_AND_MASK_Per_DS3E3_FMPM_FM_DS3E3_OAMSK_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM DS3E3 Framer Interrupt MASK Per DS3E3
Reg Addr   : 0x180B0-0x180BF
Reg Formula: 0x180B0+$G
    Where  : 
           + $G(0-11) : DS3/E3 Group  Interrupt
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_DS3E3_Framer_Interrupt_MASK_Per_DS3E3_Base                                     0x180B0
#define cAf6Reg_FMPM_FM_DS3E3_Framer_Interrupt_MASK_Per_DS3E3(G)                                 (0x180B0+(G))

/*--------------------------------------
BitField Name: FMPM_FM_DS3E3_MSK
BitField Type: R/W
BitField Desc: MASK per DS3/E3 Framer
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_MASK_Per_DS3E3_FMPM_FM_DS3E3_MSK_Mask                                cBit23_0
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_MASK_Per_DS3E3_FMPM_FM_DS3E3_MSK_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM DS3E3 Framer Interrupt Sticky Per Type of Per DS3E3
Reg Addr   : 0x1A600-0x1A7FF
Reg Formula: 0x1A600+$G*0x20+$H
    Where  : 
           + $G(0-11) : DS3/E3 Group  Interrupt
           + $H(0-23) : DS3/E3 Group  Interrupt
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_Base                                 0x1A600
#define cAf6Reg_FMPM_FM_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3(G, H)                  (0x1A600+(G)*0x20+(H))

/*--------------------------------------
BitField Name: FMPM_FM_DS3E3_STK_RAI
BitField Type: W1C
BitField Desc: RAI
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_STK_RAI_Mask                                   cBit4
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_STK_RAI_Shift                                       4

/*--------------------------------------
BitField Name: FMPM_FM_DS3E3_STK_AIS
BitField Type: W1C
BitField Desc: AIS
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_STK_AIS_Mask                                   cBit3
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_STK_AIS_Shift                                       3

/*--------------------------------------
BitField Name: FMPM_FM_DS3E3_STK_SEF
BitField Type: W1C
BitField Desc: SEF
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_STK_SEF_Mask                                   cBit2
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_STK_SEF_Shift                                       2

/*--------------------------------------
BitField Name: FMPM_FM_DS3E3_STK_LOF
BitField Type: W1C
BitField Desc: LOF
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_STK_LOF_Mask                                   cBit1
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_STK_LOF_Shift                                       1

/*--------------------------------------
BitField Name: FMPM_FM_DS3E3_STK_LOS
BitField Type: W1C
BitField Desc: LOS
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_STK_LOS_Mask                                   cBit0
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_STK_LOS_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM DS3E3 Framer Interrupt MASK Per Type of Per DS3E3
Reg Addr   : 0x1A800-0x1A9FF
Reg Formula: 0x1A800+$G*0x20+$H
    Where  : 
           + $G(0-11) : DS3/E3 Group  Interrupt
           + $H(0-23) : DS3/E3 Group  Interrupt
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_Base                                 0x1A800
#define cAf6Reg_FMPM_FM_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3(G, H)                  (0x1A800+(G)*0x20+(H))

/*--------------------------------------
BitField Name: FMPM_FM_DS3E3_MSK_HI_AIS
BitField Type: R/W
BitField Desc: High level AIS
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_MSK_HI_AIS_Mask                                   cBit5
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_MSK_HI_AIS_Shift                                       5

/*--------------------------------------
BitField Name: FMPM_FM_DS3E3_MSK_RAI
BitField Type: R/W
BitField Desc: RAI
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_MSK_RAI_Mask                                   cBit4
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_MSK_RAI_Shift                                       4

/*--------------------------------------
BitField Name: FMPM_FM_DS3E3_MSK_AIS
BitField Type: R/W
BitField Desc: AIS
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_MSK_AIS_Mask                                   cBit3
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_MSK_AIS_Shift                                       3

/*--------------------------------------
BitField Name: FMPM_FM_DS3E3_MSK_SEF
BitField Type: R/W
BitField Desc: SEF
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_MSK_SEF_Mask                                   cBit2
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_MSK_SEF_Shift                                       2

/*--------------------------------------
BitField Name: FMPM_FM_DS3E3_MSK_LOF
BitField Type: R/W
BitField Desc: LOF
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_MSK_LOF_Mask                                   cBit1
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_MSK_LOF_Shift                                       1

/*--------------------------------------
BitField Name: FMPM_FM_DS3E3_MSK_LOS
BitField Type: R/W
BitField Desc: LOS
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_MSK_LOS_Mask                                   cBit0
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_MSK_LOS_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM DS3E3 Framer Interrupt Current Status Per Type of Per DS3E3
Reg Addr   : 0x1AA00-0x1ABFF
Reg Formula: 0x1AA00+$G*0x20+$H
    Where  : 
           + $G(0-11) : DS3/E3 Group  Interrupt
           + $H(0-23) : DS3/E3 Group  Interrupt
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3_Base                                 0x1AA00
#define cAf6Reg_FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3(G, H)                  (0x1AA00+(G)*0x20+(H))

/*--------------------------------------
BitField Name: FMPM_FM_DS3E3_AMSK_HI_AIS
BitField Type: R_O
BitField Desc: High level AIS
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_AMSK_HI_AIS_Mask                                  cBit21
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_AMSK_HI_AIS_Shift                                      21

/*--------------------------------------
BitField Name: FMPM_FM_DS3E3_AMSK_RAI
BitField Type: R_O
BitField Desc: RAI
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_AMSK_RAI_Mask                                  cBit20
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_AMSK_RAI_Shift                                      20

/*--------------------------------------
BitField Name: FMPM_FM_DS3E3_AMSK_AIS
BitField Type: R_O
BitField Desc: AIS
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_AMSK_AIS_Mask                                  cBit19
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_AMSK_AIS_Shift                                      19

/*--------------------------------------
BitField Name: FMPM_FM_DS3E3_AMSK_SEF
BitField Type: R_O
BitField Desc: SEF
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_AMSK_SEF_Mask                                  cBit18
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_AMSK_SEF_Shift                                      18

/*--------------------------------------
BitField Name: FMPM_FM_DS3E3_AMSK_LOF
BitField Type: R_O
BitField Desc: LOF
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_AMSK_LOF_Mask                                  cBit17
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_AMSK_LOF_Shift                                      17

/*--------------------------------------
BitField Name: FMPM_FM_DS3E3_AMSK_LOS
BitField Type: R_O
BitField Desc: LOS
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_AMSK_LOS_Mask                                  cBit16
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_AMSK_LOS_Shift                                      16


/*--------------------------------------
BitField Name: FMPM_FM_DS3E3_CUR_HI_AIS
BitField Type: R_O
BitField Desc: High level AIS
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_CUR_HI_AIS_Mask                                cBit5
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_CUR_HI_AIS_Shift                                    5

/*--------------------------------------
BitField Name: FMPM_FM_DS3E3_CUR_RAI
BitField Type: R_O
BitField Desc: RAI
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_CUR_RAI_Mask                                   cBit4
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_CUR_RAI_Shift                                       4

/*--------------------------------------
BitField Name: FMPM_FM_DS3E3_CUR_AIS
BitField Type: R_O
BitField Desc: AIS
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_CUR_AIS_Mask                                   cBit3
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_CUR_AIS_Shift                                       3

/*--------------------------------------
BitField Name: FMPM_FM_DS3E3_CUR_SEF
BitField Type: R_O
BitField Desc: SEF
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_CUR_SEF_Mask                                   cBit2
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_CUR_SEF_Shift                                       2

/*--------------------------------------
BitField Name: FMPM_FM_DS3E3_CUR_LOF
BitField Type: R_O
BitField Desc: LOF
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_CUR_LOF_Mask                                   cBit1
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_CUR_LOF_Shift                                       1

/*--------------------------------------
BitField Name: FMPM_FM_DS3E3_CUR_LOS
BitField Type: R_O
BitField Desc: LOS
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_CUR_LOS_Mask                                   cBit0
#define cAf6_FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3_FMPM_FM_DS3E3_CUR_LOS_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM STS24 LO Interrupt OR
Reg Addr   : 0x18002
Reg Formula: 
    Where  : 
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_STS24_LO_Interrupt_OR                                                          0x18002

/*--------------------------------------
BitField Name: FMPM_FM_STS24LO_OR
BitField Type: R_O
BitField Desc: Interrupt STS24 LO OR
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_FMPM_FM_STS24_LO_Interrupt_OR_FMPM_FM_STS24LO_OR_Mask                                    cBit11_0
#define cAf6_FMPM_FM_STS24_LO_Interrupt_OR_FMPM_FM_STS24LO_OR_Shift                                          0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM STS1 LO Interrupt OR
Reg Addr   : 0x180C0-0x180CF
Reg Formula: 0x180C0+$G
    Where  : 
           + $G(0-11) : STS-24 LO group
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_STS1_LO_Interrupt_OR_Base                                                      0x180C0
#define cAf6Reg_FMPM_FM_STS1_LO_Interrupt_OR(G)                                                  (0x180C0+(G))

/*--------------------------------------
BitField Name: FMPM_FM_STS1LO_OR
BitField Type: R_O
BitField Desc: Interrupt STS1 LO OR
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_FMPM_FM_STS1_LO_Interrupt_OR_FMPM_FM_STS1LO_OR_Mask                                      cBit23_0
#define cAf6_FMPM_FM_STS1_LO_Interrupt_OR_FMPM_FM_STS1LO_OR_Shift                                            0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM VTTU Interrupt OR AND MASK Per VTTU
Reg Addr   : 0x1AC00-0x1ADFF
Reg Formula: 0x1AC00+$G*0x20+$H
    Where  : 
           + $G(0-11) : STS-24 LO group
           + $H(0-23) : STS-1 LO group
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_VTTU_Interrupt_OR_AND_MASK_Per_VTTU_Base                                       0x1AC00
#define cAf6Reg_FMPM_FM_VTTU_Interrupt_OR_AND_MASK_Per_VTTU(G, H)                       (0x1AC00+(G)*0x20+(H))

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_OAMSK
BitField Type: R_O
BitField Desc: VT/TU Interrupt or TU-3 interrupt
BitField Bits: [27:00]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_OR_AND_MASK_Per_VTTU_FMPM_FM_VTTU_OAMSK_Mask                                cBit27_0
#define cAf6_FMPM_FM_VTTU_Interrupt_OR_AND_MASK_Per_VTTU_FMPM_FM_VTTU_OAMSK_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM VTTU Interrupt MASK Per VTTU
Reg Addr   : 0x1AE00-0x1AFFF
Reg Formula: 0x1AE00+$G*0x20+$H
    Where  : 
           + $G(0-11) : STS-24 LO group
           + $H(0-23) : STS-1 LO group
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_VTTU_Interrupt_MASK_Per_VTTU_Base                                              0x1AE00
#define cAf6Reg_FMPM_FM_VTTU_Interrupt_MASK_Per_VTTU(G, H)                              (0x1AE00+(G)*0x20+(H))

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_MSK
BitField Type: R/W
BitField Desc: VT/TU Interrupt or TU-3 interrupt
BitField Bits: [27:00]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_MASK_Per_VTTU_FMPM_FM_VTTU_MSK_Mask                                cBit27_0
#define cAf6_FMPM_FM_VTTU_Interrupt_MASK_Per_VTTU_FMPM_FM_VTTU_MSK_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM VTTU Interrupt Sticky Per Type Per VTTU
Reg Addr   : 0x24000-0x27FFF
Reg Formula: 0x24000+$G*0x400+$H*0x20+$I
    Where  : 
           + $G(0-11) : STS-24 LO group
           + $H(0-23) : STS-1 LO group
           + $I(0-27) : VT/TU
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_Base                                   0x24000
#define cAf6Reg_FMPM_FM_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU(G, H, I)        (0x24000+(G)*0x400+(H)*0x20+(I))

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_STK_RFI_SER
BitField Type: W1C
BitField Desc: RFI server
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_FM_VTTU_STK_RFI_SER_Mask                                  cBit10
#define cAf6_FMPM_FM_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_FM_VTTU_STK_RFI_SER_Shift                                      10

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_STK_RFI_CON
BitField Type: W1C
BitField Desc: RFI connectivity
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_FM_VTTU_STK_RFI_CON_Mask                                   cBit9
#define cAf6_FMPM_FM_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_FM_VTTU_STK_RFI_CON_Shift                                       9

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_STK_RFI_PAY
BitField Type: W1C
BitField Desc: RFI payload
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_FM_VTTU_STK_RFI_PAY_Mask                                   cBit8
#define cAf6_FMPM_FM_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_FM_VTTU_STK_RFI_PAY_Shift                                       8

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_STK_RFI
BitField Type: W1C
BitField Desc: one-bit RFI
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_FM_VTTU_STK_RFI_Mask                                   cBit7
#define cAf6_FMPM_FM_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_FM_VTTU_STK_RFI_Shift                                       7

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_STK_AIS
BitField Type: W1C
BitField Desc: AIS
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_FM_VTTU_STK_AIS_Mask                                   cBit6
#define cAf6_FMPM_FM_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_FM_VTTU_STK_AIS_Shift                                       6

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_STK_TIM
BitField Type: W1C
BitField Desc: TIM
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_FM_VTTU_STK_TIM_Mask                                   cBit5
#define cAf6_FMPM_FM_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_FM_VTTU_STK_TIM_Shift                                       5

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_STK_UNEQ
BitField Type: W1C
BitField Desc: UNEQ
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_FM_VTTU_STK_UNEQ_Mask                                   cBit4
#define cAf6_FMPM_FM_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_FM_VTTU_STK_UNEQ_Shift                                       4

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_STK_PLM
BitField Type: W1C
BitField Desc: PLM
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_FM_VTTU_STK_PLM_Mask                                   cBit3
#define cAf6_FMPM_FM_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_FM_VTTU_STK_PLM_Shift                                       3

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_STK_LOP
BitField Type: W1C
BitField Desc: LOP
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_FM_VTTU_STK_LOP_Mask                                   cBit2
#define cAf6_FMPM_FM_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_FM_VTTU_STK_LOP_Shift                                       2

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_STK_BERSD
BitField Type: W1C
BitField Desc: BERSD
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_FM_VTTU_STK_BERSD_Mask                                   cBit1
#define cAf6_FMPM_FM_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_FM_VTTU_STK_BERSD_Shift                                       1

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_STK_BERSF
BitField Type: W1C
BitField Desc: BERSF
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_FM_VTTU_STK_BERSF_Mask                                   cBit0
#define cAf6_FMPM_FM_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_FM_VTTU_STK_BERSF_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM VTTU Interrupt MASK Per Type Per VTTU
Reg Addr   : 0x28000-0x2BFFF
Reg Formula: 0x28000+$G*0x400+$H*0x20+$I
    Where  : 
           + $G(0-11) : STS-24 LO group
           + $H(0-23) : STS-1 LO group
           + $I(0-27) : VT/TU
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_Base                                     0x28000
#define cAf6Reg_FMPM_FM_VTTU_Interrupt_MASK_Per_Type_Per_VTTU(G, H, I)        (0x28000+(G)*0x400+(H)*0x20+(I))

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_MSK_HI_AIS
BitField Type: R/W
BitField Desc: High level AIS
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_FM_VTTU_MSK_HI_AIS_Mask                                  cBit11
#define cAf6_FMPM_FM_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_FM_VTTU_MSK_HI_AIS_Shift                                      11

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_MSK_RFI_SER
BitField Type: R/W
BitField Desc: RFI server
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_FM_VTTU_MSK_RFI_SER_Mask                                  cBit10
#define cAf6_FMPM_FM_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_FM_VTTU_MSK_RFI_SER_Shift                                      10

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_MSK_RFI_CON
BitField Type: R/W
BitField Desc: RFI connectivity
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_FM_VTTU_MSK_RFI_CON_Mask                                   cBit9
#define cAf6_FMPM_FM_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_FM_VTTU_MSK_RFI_CON_Shift                                       9

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_MSK_RFI_PAY
BitField Type: R/W
BitField Desc: RFI payload
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_FM_VTTU_MSK_RFI_PAY_Mask                                   cBit8
#define cAf6_FMPM_FM_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_FM_VTTU_MSK_RFI_PAY_Shift                                       8

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_MSK_RFI
BitField Type: R/W
BitField Desc: one-bit RFI
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_FM_VTTU_MSK_RFI_Mask                                   cBit7
#define cAf6_FMPM_FM_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_FM_VTTU_MSK_RFI_Shift                                       7

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_MSK_AIS
BitField Type: R/W
BitField Desc: AIS
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_FM_VTTU_MSK_AIS_Mask                                   cBit6
#define cAf6_FMPM_FM_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_FM_VTTU_MSK_AIS_Shift                                       6

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_MSK_TIM
BitField Type: R/W
BitField Desc: TIM
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_FM_VTTU_MSK_TIM_Mask                                   cBit5
#define cAf6_FMPM_FM_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_FM_VTTU_MSK_TIM_Shift                                       5

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_MSK_UNEQ
BitField Type: R/W
BitField Desc: UNEQ
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_FM_VTTU_MSK_UNEQ_Mask                                   cBit4
#define cAf6_FMPM_FM_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_FM_VTTU_MSK_UNEQ_Shift                                       4

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_MSK_PLM
BitField Type: R/W
BitField Desc: PLM
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_FM_VTTU_MSK_PLM_Mask                                   cBit3
#define cAf6_FMPM_FM_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_FM_VTTU_MSK_PLM_Shift                                       3

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_MSK_LOP
BitField Type: R/W
BitField Desc: LOP
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_FM_VTTU_MSK_LOP_Mask                                   cBit2
#define cAf6_FMPM_FM_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_FM_VTTU_MSK_LOP_Shift                                       2

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_MSK_BERSD
BitField Type: R/W
BitField Desc: BERSD
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_FM_VTTU_MSK_BERSD_Mask                                   cBit1
#define cAf6_FMPM_FM_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_FM_VTTU_MSK_BERSD_Shift                                       1

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_MSK_BERSF
BitField Type: R/W
BitField Desc: BERSF
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_FM_VTTU_MSK_BERSF_Mask                                   cBit0
#define cAf6_FMPM_FM_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_FM_VTTU_MSK_BERSF_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM VTTU Interrupt Current Status Per Type Per VTTU
Reg Addr   : 0x2C000-0x2FFFF
Reg Formula: 0x2C000+$G*0x400+$H*0x20+$I
    Where  : 
           + $G(0-11) : STS-24 LO group
           + $H(0-23) : STS-1 LO group
           + $I(0-27) : VT/TU
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_Base                                 0x2C000
#define cAf6Reg_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU(G, H, I)        (0x2C000+(G)*0x400+(H)*0x20+(I))

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_AMSK_HI_AIS
BitField Type: R_O
BitField Desc: High level AIS
BitField Bits: [27:27]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_AMSK_HI_AIS_Mask                                  cBit27
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_AMSK_HI_AIS_Shift                                      27

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_AMSK_RFI_SER
BitField Type: R_O
BitField Desc: RFI server
BitField Bits: [26:26]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_AMSK_RFI_SER_Mask                                  cBit26
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_AMSK_RFI_SER_Shift                                      26

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_AMSK_RFI_CON
BitField Type: R_O
BitField Desc: RFI connectivity
BitField Bits: [25:25]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_AMSK_RFI_CON_Mask                                  cBit25
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_AMSK_RFI_CON_Shift                                      25

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_AMSK_RFI_PAY
BitField Type: R_O
BitField Desc: RFI payload
BitField Bits: [24:24]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_AMSK_RFI_PAY_Mask                                  cBit24
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_AMSK_RFI_PAY_Shift                                      24

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_AMSK_RFI
BitField Type: R_O
BitField Desc: one-bit
BitField Bits: [23:23]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_AMSK_RFI_Mask                                  cBit23
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_AMSK_RFI_Shift                                      23

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_AMSK_AIS
BitField Type: R_O
BitField Desc: AIS
BitField Bits: [22:22]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_AMSK_AIS_Mask                                  cBit22
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_AMSK_AIS_Shift                                      22

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_AMSK_TIM
BitField Type: R_O
BitField Desc: TIM
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_AMSK_TIM_Mask                                  cBit21
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_AMSK_TIM_Shift                                      21

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_AMSK_UNEQ
BitField Type: R_O
BitField Desc: UNEQ
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_AMSK_UNEQ_Mask                                  cBit20
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_AMSK_UNEQ_Shift                                      20

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_AMSK_PLM
BitField Type: R_O
BitField Desc: PLM
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_AMSK_PLM_Mask                                  cBit19
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_AMSK_PLM_Shift                                      19

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_AMSK_LOP
BitField Type: R_O
BitField Desc: LOP
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_AMSK_LOP_Mask                                  cBit18
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_AMSK_LOP_Shift                                      18

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_AMSK_BERSD
BitField Type: R_O
BitField Desc: BERSD
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_AMSK_BERSD_Mask                                  cBit17
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_AMSK_BERSD_Shift                                      17

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_AMSK_BERSF
BitField Type: R_O
BitField Desc: BERSF
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_AMSK_BERSF_Mask                                  cBit16
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_AMSK_BERSF_Shift                                      16

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_CUR_HI_AIS
BitField Type: R_O
BitField Desc: High level
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_CUR_HI_AIS_Mask                                  cBit11
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_CUR_HI_AIS_Shift                                      11

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_CUR_RFI_SER
BitField Type: R_O
BitField Desc: RFI server
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_CUR_RFI_SER_Mask                                  cBit10
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_CUR_RFI_SER_Shift                                      10

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_CUR_RFI_CON
BitField Type: R_O
BitField Desc: RFI connectivity
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_CUR_RFI_CON_Mask                                   cBit9
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_CUR_RFI_CON_Shift                                       9

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_CUR_RFI_PAY
BitField Type: R_O
BitField Desc: RFI payload
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_CUR_RFI_PAY_Mask                                   cBit8
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_CUR_RFI_PAY_Shift                                       8

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_CUR_RFI
BitField Type: R_O
BitField Desc: one-bit
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_CUR_RFI_Mask                                   cBit7
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_CUR_RFI_Shift                                       7

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_CUR_AIS
BitField Type: R_O
BitField Desc: AIS
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_CUR_AIS_Mask                                   cBit6
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_CUR_AIS_Shift                                       6

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_CUR_TIM
BitField Type: R_O
BitField Desc: TIM
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_CUR_TIM_Mask                                   cBit5
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_CUR_TIM_Shift                                       5

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_CUR_UNEQ
BitField Type: R_O
BitField Desc: UNEQ
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_CUR_UNEQ_Mask                                   cBit4
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_CUR_UNEQ_Shift                                       4

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_CUR_PLM
BitField Type: R_O
BitField Desc: PLM
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_CUR_PLM_Mask                                   cBit3
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_CUR_PLM_Shift                                       3

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_CUR_LOP
BitField Type: R_O
BitField Desc: LOP
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_CUR_LOP_Mask                                   cBit2
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_CUR_LOP_Shift                                       2

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_CUR_BERSD
BitField Type: R_O
BitField Desc: BERSD
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_CUR_BERSD_Mask                                   cBit1
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_CUR_BERSD_Shift                                       1

/*--------------------------------------
BitField Name: FMPM_FM_VTTU_CUR_BERSF
BitField Type: R_O
BitField Desc: BERSF
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_CUR_BERSF_Mask                                   cBit0
#define cAf6_FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU_FMPM_FM_VTTU_CUR_BERSF_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM DS1E1 Level1 Group Interrupt OR
Reg Addr   : 0x18003
Reg Formula: 
    Where  : 
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_DS1E1_Level1_Group_Interrupt_OR                                                0x18003

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_LEV1_OR
BitField Type: R_O
BitField Desc: Interrupt DS1E1 level1 group OR
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Level1_Group_Interrupt_OR_FMPM_FM_DS1E1_LEV1_OR_Mask                                cBit11_0
#define cAf6_FMPM_FM_DS1E1_Level1_Group_Interrupt_OR_FMPM_FM_DS1E1_LEV1_OR_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM DS1E1 Level2 Group Interrupt OR
Reg Addr   : 0x180D0-0x180DF
Reg Formula: 0x180D0+$G
    Where  : 
           + $G(0-11) : DS1/E1 Level-1 Group
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_DS1E1_Level2_Group_Interrupt_OR_Base                                           0x180D0
#define cAf6Reg_FMPM_FM_DS1E1_Level2_Group_Interrupt_OR(G)                                       (0x180D0+(G))

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_LEV2_OR
BitField Type: R_O
BitField Desc: Level2 Group Interrupt OR
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Level2_Group_Interrupt_OR_FMPM_FM_DS1E1_LEV2_OR_Mask                                cBit23_0
#define cAf6_FMPM_FM_DS1E1_Level2_Group_Interrupt_OR_FMPM_FM_DS1E1_LEV2_OR_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM DS1E1 Framer Interrupt OR AND MASK Per DS1E1
Reg Addr   : 0x1B000-0x1B1FF
Reg Formula: 0x1B000+$G*0x20+$H
    Where  : 
           + $G(0-11) : DS1/E1 Level-1 Group
           + $H(0-23) : DS1/E1 Level-2 Group
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1_Base                                 0x1B000
#define cAf6Reg_FMPM_FM_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1(G, H)                  (0x1B000+(G)*0x20+(H))

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_OAMSK
BitField Type: R_O
BitField Desc: DS1/E1 Framer Interrupt
BitField Bits: [27:00]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1_FMPM_FM_DS1E1_OAMSK_Mask                                cBit27_0
#define cAf6_FMPM_FM_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1_FMPM_FM_DS1E1_OAMSK_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM DS1E1 Framer Interrupt MASK Per DS1E1
Reg Addr   : 0x1B200-0x1B3FF
Reg Formula: 0x1B200+$G*0x20+$H
    Where  : 
           + $G(0-11) : DS1/E1 Level-1 Group
           + $H(0-23) : DS1/E1 Level-2 Group
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_DS1E1_Framer_Interrupt_MASK_Per_DS1E1_Base                                     0x1B200
#define cAf6Reg_FMPM_FM_DS1E1_Framer_Interrupt_MASK_Per_DS1E1(G, H)                     (0x1B200+(G)*0x20+(H))

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_MSK
BitField Type: R/W
BitField Desc: DS1/E1 Framer Interrupt
BitField Bits: [27:00]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Framer_Interrupt_MASK_Per_DS1E1_FMPM_FM_DS1E1_MSK_Mask                                cBit27_0
#define cAf6_FMPM_FM_DS1E1_Framer_Interrupt_MASK_Per_DS1E1_FMPM_FM_DS1E1_MSK_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM DS1E1 Interrupt Sticky Per Type Per DS1E1
Reg Addr   : 0x30000-0x33FFF
Reg Formula: 0x30000+$G*0x400+$H*0x20+$I
    Where  : 
           + $G(0-11) : DS1/E1 Level-1 Group
           + $H(0-23) : DS1/E1 Level-2 Group
           + $I(0-27) : DS1/E1
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_Base                                 0x30000
#define cAf6Reg_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1(G, H, I)        (0x30000+(G)*0x400+(H)*0x20+(I))

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_STK_RAI_CI
BitField Type: W1C
BitField Desc: RAI_CI
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_RAI_CI_Mask                                   cBit5
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_RAI_CI_Shift                                       5

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_STK_RAI
BitField Type: W1C
BitField Desc: RAI
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_RAI_Mask                                   cBit4
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_RAI_Shift                                       4

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_STK_LOF
BitField Type: W1C
BitField Desc: LOF
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_LOF_Mask                                   cBit3
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_LOF_Shift                                       3

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_STK_AIS_CI
BitField Type: W1C
BitField Desc: AIS_CI
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_AIS_CI_Mask                                   cBit2
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_AIS_CI_Shift                                       2

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_STK_AIS
BitField Type: W1C
BitField Desc: AIS
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_AIS_Mask                                   cBit1
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_AIS_Shift                                       1

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_STK_LOS
BitField Type: W1C
BitField Desc: LOS
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_LOS_Mask                                   cBit0
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_LOS_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM DS1E1 Interrupt MASK Per Type Per DS1E1
Reg Addr   : 0x34000-0x37FFF
Reg Formula: 0x34000+$G*0x400+$H*0x20+$I
    Where  : 
           + $G(0-11) : DS1/E1 Level-1 Group
           + $H(0-23) : DS1/E1 Level-2 Group
           + $I(0-27) : DS1/E1
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_Base                                   0x34000
#define cAf6Reg_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1(G, H, I)        (0x34000+(G)*0x400+(H)*0x20+(I))

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_MSK_HI_AIS
BitField Type: R/W
BitField Desc: High level AIS
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_HI_AIS_Mask                                   cBit6
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_HI_AIS_Shift                                       6

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_MSK_RAI_CI
BitField Type: R/W
BitField Desc: RAI_CI
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_RAI_CI_Mask                                   cBit5
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_RAI_CI_Shift                                       5

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_MSK_RAI
BitField Type: R/W
BitField Desc: RAI
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_RAI_Mask                                   cBit4
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_RAI_Shift                                       4

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_MSK_LOF
BitField Type: R/W
BitField Desc: LOF
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_LOF_Mask                                   cBit3
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_LOF_Shift                                       3

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_MSK_AIS_CI
BitField Type: R/W
BitField Desc: AIS_CI
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_AIS_CI_Mask                                   cBit2
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_AIS_CI_Shift                                       2

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_MSK_AIS
BitField Type: R/W
BitField Desc: AIS
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_AIS_Mask                                   cBit1
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_AIS_Shift                                       1

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_MSK_LOS
BitField Type: R/W
BitField Desc: LOS
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_LOS_Mask                                   cBit0
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_LOS_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM DS1E1 Interrupt Current Status Per Type Per DS1E1
Reg Addr   : 0x38000-0x3BFFF
Reg Formula: 0x38000+$G*0x400+$H*0x20+$I
    Where  : 
           + $G(0-11) : DS1/E1 Level-1 Group
           + $H(0-23) : DS1/E1 Level-2 Group
           + $I(0-27) : DS1/E1
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_Base                                 0x38000
#define cAf6Reg_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1(G, H, I)        (0x38000+(G)*0x400+(H)*0x20+(I))

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_AMSK_HI_AIS
BitField Type: R_O
BitField Desc: High level AIS
BitField Bits: [22:22]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_HI_AIS_Mask                                  cBit22
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_HI_AIS_Shift                                      22

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_AMSK_RAI_CI
BitField Type: R_O
BitField Desc: RAI_CI
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_RAI_CI_Mask                                  cBit21
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_RAI_CI_Shift                                      21

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_AMSK_RAI
BitField Type: R_O
BitField Desc: RAI
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_RAI_Mask                                  cBit20
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_RAI_Shift                                      20

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_AMSK_LOF
BitField Type: R_O
BitField Desc: LOF
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_LOF_Mask                                  cBit19
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_LOF_Shift                                      19

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_AMSK_AIS_CI
BitField Type: R_O
BitField Desc: AIS_CI
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_AIS_CI_Mask                                  cBit18
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_AIS_CI_Shift                                      18

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_AMSK_AIS
BitField Type: R_O
BitField Desc: AIS
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_AIS_Mask                                  cBit17
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_AIS_Shift                                      17

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_AMSK_LOS
BitField Type: R_O
BitField Desc: LOS
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_LOS_Mask                                  cBit16
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_LOS_Shift                                      16

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_CUR_HI_AIS
BitField Type: R_O
BitField Desc: High level AIS
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_HI_AIS_Mask                                   cBit6
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_HI_AIS_Shift                                       6

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_CUR_RAI_CI
BitField Type: R_O
BitField Desc: RAI_CI
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_RAI_CI_Mask                                   cBit5
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_RAI_CI_Shift                                       5

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_CUR_RAI
BitField Type: R_O
BitField Desc: RAI
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_RAI_Mask                                   cBit4
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_RAI_Shift                                       4

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_CUR_LOF
BitField Type: R_O
BitField Desc: LOF
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_LOF_Mask                                   cBit3
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_LOF_Shift                                       3

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_CUR_AIS_CI
BitField Type: R_O
BitField Desc: AIS_CI
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_AIS_CI_Mask                                   cBit2
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_AIS_CI_Shift                                       2

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_CUR_AIS
BitField Type: R_O
BitField Desc: AIS
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_AIS_Mask                                   cBit1
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_AIS_Shift                                       1

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_CUR_LOS
BitField Type: R_O
BitField Desc: LOS
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_LOS_Mask                                   cBit0
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_LOS_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM PW Level1 Group Interrupt OR
Reg Addr   : 0x18004
Reg Formula: 
    Where  : 
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_PW_Level1_Group_Interrupt_OR                                                   0x18004

/*--------------------------------------
BitField Name: FMPM_FM_PW_LEV1_OR
BitField Type: R_O
BitField Desc: Interrupt PW level1 group OR
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Level1_Group_Interrupt_OR_FMPM_FM_PW_LEV1_OR_Mask                                 cBit7_0
#define cAf6_FMPM_FM_PW_Level1_Group_Interrupt_OR_FMPM_FM_PW_LEV1_OR_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM PW Level2 Group Interrupt OR
Reg Addr   : 0x180E0-0x180E7
Reg Formula: 0x180E0+$D
    Where  : 
           + $D(0-7) : PW  Level-1 Group
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_PW_Level2_Group_Interrupt_OR_Base                                              0x180E0
#define cAf6Reg_FMPM_FM_PW_Level2_Group_Interrupt_OR(D)                                          (0x180E0+(D))

/*--------------------------------------
BitField Name: FMPM_FM_PW_LEV2_OR
BitField Type: R_O
BitField Desc: PW Level2 Group Interrupt OR, bit per group
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Level2_Group_Interrupt_OR_FMPM_FM_PW_LEV2_OR_Mask                                cBit31_0
#define cAf6_FMPM_FM_PW_Level2_Group_Interrupt_OR_FMPM_FM_PW_LEV2_OR_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM PW Framer Interrupt OR AND MASK Per PW
Reg Addr   : 0x1B400-0x1B4FF
Reg Formula: 0x1B400+$D*0x20+$E
    Where  : 
           + $D(0-7) : PW  Level-1 Group
           + $E(0-31) : PW  Level-2 Group
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_PW_Framer_Interrupt_OR_AND_MASK_Per_PW_Base                                    0x1B400
#define cAf6Reg_FMPM_FM_PW_Framer_Interrupt_OR_AND_MASK_Per_PW(D, E)                    (0x1B400+(D)*0x20+(E))

/*--------------------------------------
BitField Name: FMPM_FM_PW_OAMSK
BitField Type: R_O
BitField Desc: Interrupt OR AND MASK per PW, bit per PW
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Framer_Interrupt_OR_AND_MASK_Per_PW_FMPM_FM_PW_OAMSK_Mask                                cBit31_0
#define cAf6_FMPM_FM_PW_Framer_Interrupt_OR_AND_MASK_Per_PW_FMPM_FM_PW_OAMSK_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM PW Framer Interrupt MASK Per PW
Reg Addr   : 0x1B500-0x1B5FF
Reg Formula: 0x1B500+$D*0x20+$E
    Where  : 
           + $D(0-7) : PW  Level-1 Group
           + $E(0-31) : PW  Level-2 Group
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_PW_Framer_Interrupt_MASK_Per_PW_Base                                           0x1B500
#define cAf6Reg_FMPM_FM_PW_Framer_Interrupt_MASK_Per_PW(D, E)                           (0x1B500+(D)*0x20+(E))

/*--------------------------------------
BitField Name: FMPM_FM_PW_MSK
BitField Type: R/W
BitField Desc: Interrupt OR AND MASK per PW, bit per PW
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Framer_Interrupt_MASK_Per_PW_FMPM_FM_PW_MSK_Mask                                cBit31_0
#define cAf6_FMPM_FM_PW_Framer_Interrupt_MASK_Per_PW_FMPM_FM_PW_MSK_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM PW Interrupt Sticky Per Type Per PW
Reg Addr   : 0x1C000-0x1DFFF
Reg Formula: 0x1C000+$D*0x400+$E*0x20+$F
    Where  : 
           + $D(0-7) : PW  Level-1 Group
           + $E(0-31) : PW  Level-2 Group
           + $F(0-31) : PW-ID
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_Base                                       0x1C000
#define cAf6Reg_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW(D, E, F)          (0x1C000+(D)*0x400+(E)*0x20+(F))

/*--------------------------------------
BitField Name: FMPM_FM_PW_STK_RBIT
BitField Type: W1C
BitField Desc: RBIT
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_RBIT_Mask                                   cBit7
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_RBIT_Shift                                       7

/*--------------------------------------
BitField Name: FMPM_FM_PW_STK_STRAY
BitField Type: W1C
BitField Desc: STRAY
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_STRAY_Mask                                   cBit6
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_STRAY_Shift                                       6

/*--------------------------------------
BitField Name: FMPM_FM_PW_STK_MALF
BitField Type: W1C
BitField Desc: MALF
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_MALF_Mask                                   cBit5
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_MALF_Shift                                       5

/*--------------------------------------
BitField Name: FMPM_FM_PW_STK_BufUder
BitField Type: W1C
BitField Desc: BufUder
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_BufUder_Mask                                   cBit4
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_BufUder_Shift                                       4

/*--------------------------------------
BitField Name: FMPM_FM_PW_STK_BufOver
BitField Type: W1C
BitField Desc: BufOver
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_BufOver_Mask                                   cBit3
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_BufOver_Shift                                       3

/*--------------------------------------
BitField Name: FMPM_FM_PW_STK_LBIT
BitField Type: W1C
BitField Desc: LBIT
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_LBIT_Mask                                   cBit2
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_LBIT_Shift                                       2

/*--------------------------------------
BitField Name: FMPM_FM_PW_STK_LOPS
BitField Type: W1C
BitField Desc: LOPS
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_LOPS_Mask                                   cBit1
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_LOPS_Shift                                       1

/*--------------------------------------
BitField Name: FMPM_FM_PW_STK_LOPSTA
BitField Type: W1C
BitField Desc: LOPSTA
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_LOPSTA_Mask                                   cBit0
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_LOPSTA_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM PW Interrupt MASK Per Type Per PW
Reg Addr   : 0x1E000-0x1FFFF
Reg Formula: 0x1E000+$D*0x400+$E*0x20+$F
    Where  : 
           + $D(0-7) : PW  Level-1 Group
           + $E(0-31) : PW  Level-2 Group
           + $F(0-31) : PW-ID
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_Base                                         0x1E000
#define cAf6Reg_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW(D, E, F)          (0x1(E)000+(D)*0x400+(E)*0x20+(F))

/*--------------------------------------
BitField Name: FMPM_FM_PW_MSK_RBIT
BitField Type: R/W
BitField Desc: RBIT
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_RBIT_Mask                                   cBit7
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_RBIT_Shift                                       7

/*--------------------------------------
BitField Name: FMPM_FM_PW_MSK_STRAY
BitField Type: R/W
BitField Desc: STRAY
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_STRAY_Mask                                   cBit6
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_STRAY_Shift                                       6

/*--------------------------------------
BitField Name: FMPM_FM_PW_MSK_MALF
BitField Type: R/W
BitField Desc: MALF
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_MALF_Mask                                   cBit5
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_MALF_Shift                                       5

/*--------------------------------------
BitField Name: FMPM_FM_PW_MSK_BufUder
BitField Type: R/W
BitField Desc: BufUder
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_BufUder_Mask                                   cBit4
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_BufUder_Shift                                       4

/*--------------------------------------
BitField Name: FMPM_FM_PW_MSK_BufOver
BitField Type: R/W
BitField Desc: BufOver
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_BufOver_Mask                                   cBit3
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_BufOver_Shift                                       3

/*--------------------------------------
BitField Name: FMPM_FM_PW_MSK_LBIT
BitField Type: R/W
BitField Desc: LBIT
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_LBIT_Mask                                   cBit2
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_LBIT_Shift                                       2

/*--------------------------------------
BitField Name: FMPM_FM_PW_MSK_LOPS
BitField Type: R/W
BitField Desc: LOPS
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_LOPS_Mask                                   cBit1
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_LOPS_Shift                                       1

/*--------------------------------------
BitField Name: FMPM_FM_PW_MSK_LOPSTA
BitField Type: R/W
BitField Desc: LOPSTA
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_LOPSTA_Mask                                   cBit0
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_LOPSTA_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM PW Interrupt Current Status Per Type Per PW
Reg Addr   : 0x20000-0x21FFF
Reg Formula: 0x20000+$D*0x400+$E*0x20+$F
    Where  : 
           + $D(0-7) : PW  Level-1 Group
           + $E(0-31) : PW  Level-2 Group
           + $F(0-31) : PW-ID
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_Base                                 0x20000
#define cAf6Reg_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW(D, E, F)        (0x20000+(D)*0x400+(E)*0x20+(F))

/*--------------------------------------
BitField Name: FMPM_FM_PW_AMSK_RBIT
BitField Type: R_O
BitField Desc: RBIT
BitField Bits: [23:23]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_RBIT_Mask                                  cBit23
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_RBIT_Shift                                      23

/*--------------------------------------
BitField Name: FMPM_FM_PW_AMSK_STRAY
BitField Type: R_O
BitField Desc: STRAY
BitField Bits: [22:22]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_STRAY_Mask                                  cBit22
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_STRAY_Shift                                      22

/*--------------------------------------
BitField Name: FMPM_FM_PW_AMSK_MALF
BitField Type: R_O
BitField Desc: MALF
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_MALF_Mask                                  cBit21
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_MALF_Shift                                      21

/*--------------------------------------
BitField Name: FMPM_FM_PW_AMSK_BufUder
BitField Type: R_O
BitField Desc: BufUder
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_BufUder_Mask                                  cBit20
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_BufUder_Shift                                      20

/*--------------------------------------
BitField Name: FMPM_FM_PW_AMSK_BufOver
BitField Type: R_O
BitField Desc: BufOver
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_BufOver_Mask                                  cBit19
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_BufOver_Shift                                      19

/*--------------------------------------
BitField Name: FMPM_FM_PW_AMSK_LBIT
BitField Type: R_O
BitField Desc: LBIT
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LBIT_Mask                                  cBit18
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LBIT_Shift                                      18

/*--------------------------------------
BitField Name: FMPM_FM_PW_AMSK_LOPS
BitField Type: R_O
BitField Desc: LOPS
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LOPS_Mask                                  cBit17
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LOPS_Shift                                      17

/*--------------------------------------
BitField Name: FMPM_FM_PW_AMSK_LOPSTA
BitField Type: R_O
BitField Desc: LOPSTA
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LOPSTA_Mask                                  cBit16
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LOPSTA_Shift                                      16

/*--------------------------------------
BitField Name: FMPM_FM_PW_CUR_RBIT
BitField Type: R_O
BitField Desc: RBIT
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_RBIT_Mask                                   cBit7
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_RBIT_Shift                                       7

/*--------------------------------------
BitField Name: FMPM_FM_PW_CUR_STRAY
BitField Type: R_O
BitField Desc: STRAY
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_STRAY_Mask                                   cBit6
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_STRAY_Shift                                       6

/*--------------------------------------
BitField Name: FMPM_FM_PW_CUR_MALF
BitField Type: R_O
BitField Desc: MALF
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_MALF_Mask                                   cBit5
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_MALF_Shift                                       5

/*--------------------------------------
BitField Name: FMPM_FM_PW_CUR_BufUder
BitField Type: R_O
BitField Desc: BufUder
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_BufUder_Mask                                   cBit4
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_BufUder_Shift                                       4

/*--------------------------------------
BitField Name: FMPM_FM_PW_CUR_BufOver
BitField Type: R_O
BitField Desc: BufOver
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_BufOver_Mask                                   cBit3
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_BufOver_Shift                                       3

/*--------------------------------------
BitField Name: FMPM_FM_PW_CUR_LBIT
BitField Type: R_O
BitField Desc: LBIT
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_LBIT_Mask                                   cBit2
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_LBIT_Shift                                       2

/*--------------------------------------
BitField Name: FMPM_FM_PW_CUR_LOPS
BitField Type: R_O
BitField Desc: LOPS
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_LOPS_Mask                                   cBit1
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_LOPS_Shift                                       1

/*--------------------------------------
BitField Name: FMPM_FM_PW_CUR_LOPSTA
BitField Type: R_O
BitField Desc: LOPSTA
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_LOPSTA_Mask                                   cBit0
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_LOPSTA_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM PW Def Interrupt
Reg Addr   : 0x94000
Reg Formula: 
    Where  : 
Reg Desc   : 
FMPM PW defect Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_PW_Def_Interrupt                                                                  0x94000

/*--------------------------------------
BitField Name: FMPM_PW_Def_Intr_PW
BitField Type: R_O
BitField Desc: PW defect interrupt
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_FMPM_PW_Def_Intr_PW_Mask                                              cBit5
#define cAf6_FMPM_PW_Def_Interrupt_FMPM_PW_Def_Intr_PW_Shift                                                 5


/*------------------------------------------------------------------------------
Reg Name   : FMPM PW Def Interrupt Mask
Reg Addr   : 0x94001
Reg Formula: 
    Where  : 
Reg Desc   : 
FMPM PW defect Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_PW_Def_Interrupt_Mask                                                             0x94001

/*--------------------------------------
BitField Name: FMPM_PW_Def_Intr_MSK_PW
BitField Type: R/W
BitField Desc: PW defect interrupt
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_Mask_FMPM_PW_Def_Intr_MSK_PW_Mask                                     cBit5
#define cAf6_FMPM_PW_Def_Interrupt_Mask_FMPM_PW_Def_Intr_MSK_PW_Shift                                        5


/*------------------------------------------------------------------------------
Reg Name   : FMPM PW Def Level1 Group Interrupt OR
Reg Addr   : 0x98004
Reg Formula: 
    Where  : 
Reg Desc   : 
FMPM PW defect Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_PW_Def_Level1_Group_Interrupt_OR                                                  0x98004

/*--------------------------------------
BitField Name: FMPM_PW_Def_LEV1_OR
BitField Type: R_O
BitField Desc: PW defect interrupt level1 group OR
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Level1_Group_Interrupt_OR_FMPM_PW_Def_LEV1_OR_Mask                                 cBit7_0
#define cAf6_FMPM_PW_Def_Level1_Group_Interrupt_OR_FMPM_PW_Def_LEV1_OR_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM PW Def Level2 Group Interrupt OR
Reg Addr   : 0x980E0-0x980E7
Reg Formula: 0x980E0+$D
    Where  : 
           + $D(0-7) : PW  Level-1 Group
Reg Desc   : 
FMPM PW defect Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_PW_Def_Level2_Group_Interrupt_OR_Base                                             0x980E0
#define cAf6Reg_FMPM_PW_Def_Level2_Group_Interrupt_OR(D)                                         (0x980E0+(D))

/*--------------------------------------
BitField Name: FMPM_PW_Def_LEV2_OR
BitField Type: R_O
BitField Desc: PW defect Level2 Group Interrupt OR, bit per group
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Level2_Group_Interrupt_OR_FMPM_PW_Def_LEV2_OR_Mask                                cBit31_0
#define cAf6_FMPM_PW_Def_Level2_Group_Interrupt_OR_FMPM_PW_Def_LEV2_OR_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM PW Def Framer Interrupt OR AND MASK Per PW
Reg Addr   : 0x9B400-0x9B4FF
Reg Formula: 0x9B400+$D*0x20+$E
    Where  : 
           + $D(0-7) : PW  Level-1 Group
           + $E(0-31) : PW  Level-2 Group
Reg Desc   : 
FMPM PW defect Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_PW_Def_Framer_Interrupt_OR_AND_MASK_Per_PW_Base                                   0x9B400
#define cAf6Reg_FMPM_PW_Def_Framer_Interrupt_OR_AND_MASK_Per_PW(D, E)                   (0x9B400+(D)*0x20+(E))

/*--------------------------------------
BitField Name: FMPM_PW_Def_OAMSK
BitField Type: R_O
BitField Desc: Interrupt OR AND MASK per PW, bit per PW
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Framer_Interrupt_OR_AND_MASK_Per_PW_FMPM_PW_Def_OAMSK_Mask                                cBit31_0
#define cAf6_FMPM_PW_Def_Framer_Interrupt_OR_AND_MASK_Per_PW_FMPM_PW_Def_OAMSK_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM PW Def Framer Interrupt MASK Per PW
Reg Addr   : 0x9B500-0x9B5FF
Reg Formula: 0x9B500+$D*0x20+$E
    Where  : 
           + $D(0-7) : PW  Level-1 Group
           + $E(0-31) : PW  Level-2 Group
Reg Desc   : 
FMPM PW defect Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_PW_Def_Framer_Interrupt_MASK_Per_PW_Base                                          0x9B500
#define cAf6Reg_FMPM_PW_Def_Framer_Interrupt_MASK_Per_PW(D, E)                          (0x9B500+(D)*0x20+(E))

/*--------------------------------------
BitField Name: FMPM_PW_Def_MSK
BitField Type: R/W
BitField Desc: Interrupt OR AND MASK per PW, bit per PW
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Framer_Interrupt_MASK_Per_PW_FMPM_PW_Def_MSK_Mask                                cBit31_0
#define cAf6_FMPM_PW_Def_Framer_Interrupt_MASK_Per_PW_FMPM_PW_Def_MSK_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM PW Def Interrupt Sticky Per Type Per PW
Reg Addr   : 0x9C000-0x9DFFF
Reg Formula: 0x9C000+$D*0x400+$E*0x20+$F
    Where  : 
           + $D(0-7) : PW  Level-1 Group
           + $E(0-31) : PW  Level-2 Group
           + $F(0-31) : PW-ID
Reg Desc   : 
FMPM PW defect Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_Base                                      0x9C000
#define cAf6Reg_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW(D, E, F)         (0x9C000+(D)*0x400+(E)*0x20+(F))

/*--------------------------------------
BitField Name: FMPM_PW_Def_STK_RBIT
BitField Type: W1C
BitField Desc: RBIT
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_STK_RBIT_Mask                                   cBit7
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_STK_RBIT_Shift                                       7

/*--------------------------------------
BitField Name: FMPM_PW_Def_STK_STRAY
BitField Type: W1C
BitField Desc: STRAY
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_STK_STRAY_Mask                                   cBit6
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_STK_STRAY_Shift                                       6

/*--------------------------------------
BitField Name: FMPM_PW_Def_STK_MALF
BitField Type: W1C
BitField Desc: MALF
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_STK_MALF_Mask                                   cBit5
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_STK_MALF_Shift                                       5

/*--------------------------------------
BitField Name: FMPM_PW_Def_STK_BufUder
BitField Type: W1C
BitField Desc: BufUder
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_STK_BufUder_Mask                                   cBit4
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_STK_BufUder_Shift                                       4

/*--------------------------------------
BitField Name: FMPM_PW_Def_STK_BufOver
BitField Type: W1C
BitField Desc: BufOver
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_STK_BufOver_Mask                                   cBit3
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_STK_BufOver_Shift                                       3

/*--------------------------------------
BitField Name: FMPM_PW_Def_STK_LBIT
BitField Type: W1C
BitField Desc: LBIT
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_STK_LBIT_Mask                                   cBit2
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_STK_LBIT_Shift                                       2

/*--------------------------------------
BitField Name: FMPM_PW_Def_STK_LOPS
BitField Type: W1C
BitField Desc: LOPS
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_STK_LOPS_Mask                                   cBit1
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_STK_LOPS_Shift                                       1

/*--------------------------------------
BitField Name: FMPM_PW_Def_STK_LOPSTA
BitField Type: W1C
BitField Desc: LOPSTA
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_STK_LOPSTA_Mask                                   cBit0
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_STK_LOPSTA_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM PW Def Interrupt MASK Per Type Per PW
Reg Addr   : 0x9E000-0x9FFFF
Reg Formula: 0x9E000+$D*0x400+$E*0x20+$F
    Where  : 
           + $D(0-7) : PW  Level-1 Group
           + $E(0-31) : PW  Level-2 Group
           + $F(0-31) : PW-ID
Reg Desc   : 
FMPM PW defect Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_Base                                        0x9E000
#define cAf6Reg_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW(D, E, F)         (0x9(E)000+(D)*0x400+(E)*0x20+(F))

/*--------------------------------------
BitField Name: FMPM_PW_Def_MSK_RBIT
BitField Type: R/W
BitField Desc: RBIT
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_RBIT_Mask                                   cBit7
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_RBIT_Shift                                       7

/*--------------------------------------
BitField Name: FMPM_PW_Def_MSK_STRAY
BitField Type: R/W
BitField Desc: STRAY
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_STRAY_Mask                                   cBit6
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_STRAY_Shift                                       6

/*--------------------------------------
BitField Name: FMPM_PW_Def_MSK_MALF
BitField Type: R/W
BitField Desc: MALF
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_MALF_Mask                                   cBit5
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_MALF_Shift                                       5

/*--------------------------------------
BitField Name: FMPM_PW_Def_MSK_BufUder
BitField Type: R/W
BitField Desc: BufUder
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_BufUder_Mask                                   cBit4
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_BufUder_Shift                                       4

/*--------------------------------------
BitField Name: FMPM_PW_Def_MSK_BufOver
BitField Type: R/W
BitField Desc: BufOver
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_BufOver_Mask                                   cBit3
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_BufOver_Shift                                       3

/*--------------------------------------
BitField Name: FMPM_PW_Def_MSK_LBIT
BitField Type: R/W
BitField Desc: LBIT
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_LBIT_Mask                                   cBit2
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_LBIT_Shift                                       2

/*--------------------------------------
BitField Name: FMPM_PW_Def_MSK_LOPS
BitField Type: R/W
BitField Desc: LOPS
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_LOPS_Mask                                   cBit1
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_LOPS_Shift                                       1

/*--------------------------------------
BitField Name: FMPM_PW_Def_MSK_LOPSTA
BitField Type: R/W
BitField Desc: LOPSTA
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_LOPSTA_Mask                                   cBit0
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_LOPSTA_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM PW Def Interrupt Current Status Per Type Per PW
Reg Addr   : 0xA0000-0xA1FFF
Reg Formula: 0xA0000+$D*0x400+$E*0x20+$F
    Where  : 
           + $D(0-7) : PW  Level-1 Group
           + $E(0-31) : PW  Level-2 Group
           + $F(0-31) : PW-ID
Reg Desc   : 
FMPM PW defect Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_Base                                 0xA0000
#define cAf6Reg_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW(D, E, F)        (0xA0000+(D)*0x400+(E)*0x20+(F))

/*--------------------------------------
BitField Name: FMPM_FM_PW_AMSK_RBIT
BitField Type: R_O
BitField Desc: RBIT
BitField Bits: [23:23]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_RBIT_Mask                                  cBit23
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_RBIT_Shift                                      23

/*--------------------------------------
BitField Name: FMPM_FM_PW_AMSK_STRAY
BitField Type: R_O
BitField Desc: STRAY
BitField Bits: [22:22]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_STRAY_Mask                                  cBit22
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_STRAY_Shift                                      22

/*--------------------------------------
BitField Name: FMPM_FM_PW_AMSK_MALF
BitField Type: R_O
BitField Desc: MALF
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_MALF_Mask                                  cBit21
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_MALF_Shift                                      21

/*--------------------------------------
BitField Name: FMPM_FM_PW_AMSK_BufUder
BitField Type: R_O
BitField Desc: BufUder
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_BufUder_Mask                                  cBit20
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_BufUder_Shift                                      20

/*--------------------------------------
BitField Name: FMPM_FM_PW_AMSK_BufOver
BitField Type: R_O
BitField Desc: BufOver
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_BufOver_Mask                                  cBit19
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_BufOver_Shift                                      19

/*--------------------------------------
BitField Name: FMPM_FM_PW_AMSK_LBIT
BitField Type: R_O
BitField Desc: LBIT
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LBIT_Mask                                  cBit18
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LBIT_Shift                                      18

/*--------------------------------------
BitField Name: FMPM_FM_PW_AMSK_LOPS
BitField Type: R_O
BitField Desc: LOPS
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LOPS_Mask                                  cBit17
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LOPS_Shift                                      17

/*--------------------------------------
BitField Name: FMPM_FM_PW_AMSK_LOPSTA
BitField Type: R_O
BitField Desc: LOPSTA
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LOPSTA_Mask                                  cBit16
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LOPSTA_Shift                                      16

/*--------------------------------------
BitField Name: FMPM_FM_PW_CUR_RBIT
BitField Type: R_O
BitField Desc: RBIT
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_RBIT_Mask                                   cBit7
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_RBIT_Shift                                       7

/*--------------------------------------
BitField Name: FMPM_FM_PW_CUR_STRAY
BitField Type: R_O
BitField Desc: STRAY
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_STRAY_Mask                                   cBit6
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_STRAY_Shift                                       6

/*--------------------------------------
BitField Name: FMPM_FM_PW_CUR_MALF
BitField Type: R_O
BitField Desc: MALF
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_MALF_Mask                                   cBit5
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_MALF_Shift                                       5

/*--------------------------------------
BitField Name: FMPM_FM_PW_CUR_BufUder
BitField Type: R_O
BitField Desc: BufUder
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_BufUder_Mask                                   cBit4
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_BufUder_Shift                                       4

/*--------------------------------------
BitField Name: FMPM_FM_PW_CUR_BufOver
BitField Type: R_O
BitField Desc: BufOver
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_BufOver_Mask                                   cBit3
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_BufOver_Shift                                       3

/*--------------------------------------
BitField Name: FMPM_FM_PW_CUR_LBIT
BitField Type: R_O
BitField Desc: LBIT
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_LBIT_Mask                                   cBit2
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_LBIT_Shift                                       2

/*--------------------------------------
BitField Name: FMPM_FM_PW_CUR_LOPS
BitField Type: R_O
BitField Desc: LOPS
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_LOPS_Mask                                   cBit1
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_LOPS_Shift                                       1

/*--------------------------------------
BitField Name: FMPM_FM_PW_CUR_LOPSTA
BitField Type: R_O
BitField Desc: LOPSTA
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_LOPSTA_Mask                                   cBit0
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_LOPSTA_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM PW HO Counter Mode
Reg Addr   : 0x12000
------------------------------------------------------------------------------*/
#define cAf6_FMPM_PW_HO_Counter_Mode                         0x12000
#define cAf6_FMPM_PW_HO_Counter_Mode_Mask                    cBit12_0

#define cAf6_FMPM_PW_HO_Counter_Mode_Pwho_Enb_Mask           cBit12
#define cAf6_FMPM_PW_HO_Counter_Mode_Pwho_Enb_Shift          12

#define cAf6_FMPM_PW_HO_Counter_Mode_Pwho_Line_Mask           cBit11_9
#define cAf6_FMPM_PW_HO_Counter_Mode_Pwho_Line_Shift          9

#define cAf6_FMPM_PW_HO_Counter_Mode_Pwho_Slice_Mask         cBit8
#define cAf6_FMPM_PW_HO_Counter_Mode_Pwho_Slice_Shift        8

#define cAf6_FMPM_PW_HO_Counter_Mode_Pwho_Sts_Mask           cBit7_0
#define cAf6_FMPM_PW_HO_Counter_Mode_Pwho_Sts_Shift          0

/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA Interrupt
Reg Addr   : 0x54000
Reg Formula:
    Where  :
Reg Desc   :
FMPM TCA Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_Interrupt                                                                     0x54000

/*--------------------------------------
BitField Name: FMPM_TCA_Intr_PW
BitField Type: R_O
BitField Desc: PW     interrupt
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_TCA_Interrupt_FMPM_TCA_Intr_PW_Mask                                                    cBit5
#define cAf6_FMPM_TCA_Interrupt_FMPM_TCA_Intr_PW_Shift                                                       5

/*--------------------------------------
BitField Name: FMPM_TCA_Intr_DS1E1
BitField Type: R_O
BitField Desc: DS1E1  interrupt
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_TCA_Interrupt_FMPM_TCA_Intr_DS1E1_Mask                                                 cBit4
#define cAf6_FMPM_TCA_Interrupt_FMPM_TCA_Intr_DS1E1_Shift                                                    4

/*--------------------------------------
BitField Name: FMPM_TCA_Intr_DS3E3
BitField Type: R_O
BitField Desc: DS3E3  interrupt
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_TCA_Interrupt_FMPM_TCA_Intr_DS3E3_Mask                                                 cBit3
#define cAf6_FMPM_TCA_Interrupt_FMPM_TCA_Intr_DS3E3_Shift                                                    3

/*--------------------------------------
BitField Name: FMPM_TCA_Intr_VTGTUG
BitField Type: R_O
BitField Desc: VTGTUG interrupt
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_TCA_Interrupt_FMPM_TCA_Intr_VTGTUG_Mask                                                cBit2
#define cAf6_FMPM_TCA_Interrupt_FMPM_TCA_Intr_VTGTUG_Shift                                                   2

/*--------------------------------------
BitField Name: FMPM_TCA_Intr_STSAU
BitField Type: R_O
BitField Desc: STSAU  interrupt
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_TCA_Interrupt_FMPM_TCA_Intr_STSAU_Mask                                                 cBit1
#define cAf6_FMPM_TCA_Interrupt_FMPM_TCA_Intr_STSAU_Shift                                                    1

/*--------------------------------------
BitField Name: FMPM_TCA_Intr_ECSTM
BitField Type: R_O
BitField Desc: ECSTM  interrupt
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_Interrupt_FMPM_TCA_Intr_ECSTM_Mask                                                 cBit0
#define cAf6_FMPM_TCA_Interrupt_FMPM_TCA_Intr_ECSTM_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA Interrupt Mask
Reg Addr   : 0x54001
Reg Formula:
    Where  :
Reg Desc   :
FMPM TCA Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_Interrupt_Mask                                                                0x54001

/*--------------------------------------
BitField Name: FMPM_TCA_Intr_MSK_PW
BitField Type: R/W
BitField Desc: PW    Interrupt
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_TCA_Interrupt_Mask_FMPM_TCA_Intr_MSK_PW_Mask                                           cBit5
#define cAf6_FMPM_TCA_Interrupt_Mask_FMPM_TCA_Intr_MSK_PW_Shift                                              5

/*--------------------------------------
BitField Name: FMPM_TCA_Intr_MSK_DS1E1
BitField Type: R/W
BitField Desc: DS1E1 interrupt
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_TCA_Interrupt_Mask_FMPM_TCA_Intr_MSK_DS1E1_Mask                                        cBit4
#define cAf6_FMPM_TCA_Interrupt_Mask_FMPM_TCA_Intr_MSK_DS1E1_Shift                                           4

/*--------------------------------------
BitField Name: FMPM_TCA_Intr_MSK_DS3E3
BitField Type: R/W
BitField Desc: DS3E3  interrupt
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_TCA_Interrupt_Mask_FMPM_TCA_Intr_MSK_DS3E3_Mask                                        cBit3
#define cAf6_FMPM_TCA_Interrupt_Mask_FMPM_TCA_Intr_MSK_DS3E3_Shift                                           3

/*--------------------------------------
BitField Name: FMPM_TCA_Intr_MSK_VTGTUG
BitField Type: R/W
BitField Desc: VTGTUG interrupt
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_TCA_Interrupt_Mask_FMPM_TCA_Intr_MSK_VTGTUG_Mask                                       cBit2
#define cAf6_FMPM_TCA_Interrupt_Mask_FMPM_TCA_Intr_MSK_VTGTUG_Shift                                          2

/*--------------------------------------
BitField Name: FMPM_TCA_Intr_MSK_STSAU
BitField Type: R/W
BitField Desc: STSAU interrupt
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_TCA_Interrupt_Mask_FMPM_TCA_Intr_MSK_STSAU_Mask                                        cBit1
#define cAf6_FMPM_TCA_Interrupt_Mask_FMPM_TCA_Intr_MSK_STSAU_Shift                                           1

/*--------------------------------------
BitField Name: FMPM_TCA_Intr_MSK_ECSTM
BitField Type: R/W
BitField Desc: ECSTM interrupt
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_Interrupt_Mask_FMPM_TCA_Intr_MSK_ECSTM_Mask                                        cBit0
#define cAf6_FMPM_TCA_Interrupt_Mask_FMPM_TCA_Intr_MSK_ECSTM_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA EC1 Interrupt OR
Reg Addr   : 0x58005
Reg Formula:
    Where  :
Reg Desc   :
FMPM TCA Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_EC1_Interrupt_OR                                                              0x58005

/*--------------------------------------
BitField Name: FMPM_TCA_EC1_OR
BitField Type: R_O
BitField Desc: Interrupt EC1 OR
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_EC1_Interrupt_OR_FMPM_TCA_EC1_OR_Mask                                            cBit1_0
#define cAf6_FMPM_TCA_EC1_Interrupt_OR_FMPM_TCA_EC1_OR_Shift                                                 0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA EC1 Interrupt OR AND MASK Per STS1
Reg Addr   : 0x58100-0x5810F
Reg Formula: 0x58100+$L*0x2+$M
    Where  :
           + $L(0-0) : TFI-5 line identifier
           + $M(0-0) : 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s
Reg Desc   :
FMPM TCA Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_EC1_Interrupt_OR_AND_MASK_Per_STS1_Base                                       0x58100
#define cAf6Reg_FMPM_TCA_EC1_Interrupt_OR_AND_MASK_Per_STS1(L, M)                        (0x58100+(L)*0x2+(M))

/*--------------------------------------
BitField Name: FMPM_TCA_EC1_OAMSK
BitField Type: R_O
BitField Desc: Interrupt STS1 OR AND MASK
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_EC1_Interrupt_OR_AND_MASK_Per_STS1_FMPM_TCA_EC1_OAMSK_Mask                                cBit15_0
#define cAf6_FMPM_TCA_EC1_Interrupt_OR_AND_MASK_Per_STS1_FMPM_TCA_EC1_OAMSK_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA EC1 Interrupt MASK Per STS1
Reg Addr   : 0x58110-0x5811F
Reg Formula: 0x58110+$L*0x2+$M
    Where  :
           + $L(0-0) : TFI-5 line identifier
           + $M(0-0) : 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s
Reg Desc   :
FMPM TCA Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_EC1_Interrupt_MASK_Per_STS1_Base                                              0x58110
#define cAf6Reg_FMPM_TCA_EC1_Interrupt_MASK_Per_STS1(L, M)                               (0x58110+(L)*0x2+(M))

/*--------------------------------------
BitField Name: FMPM_TCA_EC1_MSK
BitField Type: R/W
BitField Desc: Interrupt MASK per STS1
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_EC1_Interrupt_MASK_Per_STS1_FMPM_TCA_EC1_MSK_Mask                                cBit15_0
#define cAf6_FMPM_TCA_EC1_Interrupt_MASK_Per_STS1_FMPM_TCA_EC1_MSK_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA EC1STM0 Interrupt Sticky Per Type Of Per STS1
Reg Addr   : 0x5B600-0x5B7FF
Reg Formula: 0x5B600+$L*0x40+$M*0x20+$N
    Where  :
           + $L(0-0) : TFI-5 line identifier
           + $M(0-0) : 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s
           + $N(0-15) : STS-1s in a STS-24 group will be numbered from 0 to 23
Reg Desc   :
FMPM TCA Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_Base                                 0x5B600
#define cAf6Reg_FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1(L, M, N)         (0x5B600+(L)*0x40+(M)*0x20+(N))

/*--------------------------------------
BitField Name: FMPM_TCA_EC1STM0_STK_CV_S
BitField Type: W1C
BitField Desc: CV_S
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_STK_CV_S_Mask                                  cBit13
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_STK_CV_S_Shift                                      13

/*--------------------------------------
BitField Name: FMPM_TCA_EC1STM0_STK_SEFS_S
BitField Type: W1C
BitField Desc: SEFS_S
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_STK_SEFS_S_Mask                                  cBit12
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_STK_SEFS_S_Shift                                      12

/*--------------------------------------
BitField Name: FMPM_TCA_EC1STM0_STK_ES_S
BitField Type: W1C
BitField Desc: ES_S
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_STK_ES_S_Mask                                  cBit11
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_STK_ES_S_Shift                                      11

/*--------------------------------------
BitField Name: FMPM_TCA_EC1STM0_STK_SES_S
BitField Type: W1C
BitField Desc: SES_S
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_STK_SES_S_Mask                                  cBit10
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_STK_SES_S_Shift                                      10

/*--------------------------------------
BitField Name: FMPM_TCA_EC1STM0_STK_CV_L
BitField Type: W1C
BitField Desc: CV_L
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_STK_CV_L_Mask                                   cBit9
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_STK_CV_L_Shift                                       9

/*--------------------------------------
BitField Name: FMPM_TCA_EC1STM0_STK_ES_L
BitField Type: W1C
BitField Desc: ES_L
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_STK_ES_L_Mask                                   cBit8
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_STK_ES_L_Shift                                       8

/*--------------------------------------
BitField Name: FMPM_TCA_EC1STM0_STK_SES_L
BitField Type: W1C
BitField Desc: SES_L
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_STK_SES_L_Mask                                   cBit7
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_STK_SES_L_Shift                                       7

/*--------------------------------------
BitField Name: FMPM_TCA_EC1STM0_STK_UAS_L
BitField Type: W1C
BitField Desc: UAS_L
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_STK_UAS_L_Mask                                   cBit6
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_STK_UAS_L_Shift                                       6

/*--------------------------------------
BitField Name: FMPM_TCA_EC1STM0_STK_CV_LFE
BitField Type: W1C
BitField Desc: CV_LFE
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_STK_CV_LFE_Mask                                   cBit5
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_STK_CV_LFE_Shift                                       5

/*--------------------------------------
BitField Name: FMPM_TCA_EC1STM0_STK_FC_L
BitField Type: W1C
BitField Desc: FC_L
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_STK_FC_L_Mask                                   cBit4
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_STK_FC_L_Shift                                       4

/*--------------------------------------
BitField Name: FMPM_TCA_EC1STM0_STK_ES_LFE
BitField Type: W1C
BitField Desc: ES_LFE
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_STK_ES_LFE_Mask                                   cBit3
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_STK_ES_LFE_Shift                                       3

/*--------------------------------------
BitField Name: FMPM_TCA_EC1STM0_STK_SES_LFE
BitField Type: W1C
BitField Desc: SES_LFE
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_STK_SES_LFE_Mask                                   cBit2
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_STK_SES_LFE_Shift                                       2

/*--------------------------------------
BitField Name: FMPM_TCA_EC1STM0_STK_UAS_LFE
BitField Type: W1C
BitField Desc: UAS_LFE
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_STK_UAS_LFE_Mask                                   cBit1
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_STK_UAS_LFE_Shift                                       1

/*--------------------------------------
BitField Name: FMPM_TCA_EC1STM0_STK_FC_LFE
BitField Type: W1C
BitField Desc: FC_LFE
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_STK_FC_LFE_Mask                                   cBit0
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_STK_FC_LFE_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA EC1STM0 Interrupt MASK Per Type Of Per STS1
Reg Addr   : 0x5B800-0x5B9FF
Reg Formula: 0x5B800+$L*0x40+$M*0x20+$N
    Where  :
           + $L(0-0) : TFI-5 line identifier
           + $M(0-0) : 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s
           + $N(0-15) : STS-1s in a STS-24 group will be numbered from 0 to 23
Reg Desc   :
FMPM TCA Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_Base                                 0x5B800
#define cAf6Reg_FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1(L, M, N)         (0x5B800+(L)*0x40+(M)*0x20+(N))

/*--------------------------------------
BitField Name: FMPM_TCA_EC1STM0_MSK_CV_S
BitField Type: R/W
BitField Desc: CV_S
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_MSK_CV_S_Mask                                  cBit13
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_MSK_CV_S_Shift                                      13

/*--------------------------------------
BitField Name: FMPM_TCA_EC1STM0_MSK_SEFS_S
BitField Type: R/W
BitField Desc: SEFS_S
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_MSK_SEFS_S_Mask                                  cBit12
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_MSK_SEFS_S_Shift                                      12

/*--------------------------------------
BitField Name: FMPM_TCA_EC1STM0_MSK_ES_S
BitField Type: R/W
BitField Desc: ES_S
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_MSK_ES_S_Mask                                  cBit11
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_MSK_ES_S_Shift                                      11

/*--------------------------------------
BitField Name: FMPM_TCA_EC1STM0_MSK_SES_S
BitField Type: R/W
BitField Desc: SES_S
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_MSK_SES_S_Mask                                  cBit10
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_MSK_SES_S_Shift                                      10

/*--------------------------------------
BitField Name: FMPM_TCA_EC1STM0_MSK_CV_L
BitField Type: R/W
BitField Desc: CV_L
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_MSK_CV_L_Mask                                   cBit9
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_MSK_CV_L_Shift                                       9

/*--------------------------------------
BitField Name: FMPM_TCA_EC1STM0_MSK_ES_L
BitField Type: R/W
BitField Desc: ES_L
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_MSK_ES_L_Mask                                   cBit8
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_MSK_ES_L_Shift                                       8

/*--------------------------------------
BitField Name: FMPM_TCA_EC1STM0_MSK_SES_L
BitField Type: R/W
BitField Desc: SES_L
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_MSK_SES_L_Mask                                   cBit7
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_MSK_SES_L_Shift                                       7

/*--------------------------------------
BitField Name: FMPM_TCA_EC1STM0_MSK_UAS_L
BitField Type: R/W
BitField Desc: UAS_L
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_MSK_UAS_L_Mask                                   cBit6
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_MSK_UAS_L_Shift                                       6

/*--------------------------------------
BitField Name: FMPM_TCA_EC1STM0_MSK_CV_LFE
BitField Type: R/W
BitField Desc: CV_LFE
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_MSK_CV_LFE_Mask                                   cBit5
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_MSK_CV_LFE_Shift                                       5

/*--------------------------------------
BitField Name: FMPM_TCA_EC1STM0_MSK_FC_L
BitField Type: R/W
BitField Desc: FC_L
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_MSK_FC_L_Mask                                   cBit4
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_MSK_FC_L_Shift                                       4

/*--------------------------------------
BitField Name: FMPM_TCA_EC1STM0_MSK_ES_LFE
BitField Type: R/W
BitField Desc: ES_LFE
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_MSK_ES_LFE_Mask                                   cBit3
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_MSK_ES_LFE_Shift                                       3

/*--------------------------------------
BitField Name: FMPM_TCA_EC1STM0_MSK_SES_LFE
BitField Type: R/W
BitField Desc: SES_LFE
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_MSK_SES_LFE_Mask                                   cBit2
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_MSK_SES_LFE_Shift                                       2

/*--------------------------------------
BitField Name: FMPM_TCA_EC1STM0_MSK_UAS_LFE
BitField Type: R/W
BitField Desc: UAS_LFE
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_MSK_UAS_LFE_Mask                                   cBit1
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_MSK_UAS_LFE_Shift                                       1

/*--------------------------------------
BitField Name: FMPM_TCA_EC1STM0_MSK_FC_LFE
BitField Type: R/W
BitField Desc: FC_LFE
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_MSK_FC_LFE_Mask                                   cBit0
#define cAf6_FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_EC1STM0_MSK_FC_LFE_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA STS24 Interrupt OR
Reg Addr   : 0x58000
Reg Formula:
    Where  :
Reg Desc   :
FMPM TCA Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_STS24_Interrupt_OR                                                            0x58000

/*--------------------------------------
BitField Name: FMPM_TCA_STS24_OR
BitField Type: R_O
BitField Desc: Interrupt STS24 OR
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_STS24_Interrupt_OR_FMPM_TCA_STS24_OR_Mask                                        cBit1_0
#define cAf6_FMPM_TCA_STS24_Interrupt_OR_FMPM_TCA_STS24_OR_Shift                                             0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA STS24 Interrupt OR AND MASK Per STS1
Reg Addr   : 0x58080-0x5808F
Reg Formula: 0x58080+$L*0x2+$M
    Where  :
           + $L(0-3) : TFI-5 line identifier
           + $M(0-1) : 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s
Reg Desc   :
FMPM TCA Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_STS24_Interrupt_OR_AND_MASK_Per_STS1_Base                                     0x58080
#define cAf6Reg_FMPM_TCA_STS24_Interrupt_OR_AND_MASK_Per_STS1(L, M)                      (0x58080+(L)*0x2+(M))

/*--------------------------------------
BitField Name: FMPM_TCA_STS1_OAMSK
BitField Type: R_O
BitField Desc: Interrupt STS1 OR AND MASK
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_STS24_Interrupt_OR_AND_MASK_Per_STS1_FMPM_TCA_STS1_OAMSK_Mask                                cBit23_0
#define cAf6_FMPM_TCA_STS24_Interrupt_OR_AND_MASK_Per_STS1_FMPM_TCA_STS1_OAMSK_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA STS24 Interrupt MASK Per STS1
Reg Addr   : 0x58090-0x5809F
Reg Formula: 0x58090+$L*0x2+$M
    Where  :
           + $L(0-3) : TFI-5 line identifier
           + $M(0-1) : 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s
Reg Desc   :
FMPM TCA Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_STS24_Interrupt_MASK_Per_STS1_Base                                            0x58090
#define cAf6Reg_FMPM_TCA_STS24_Interrupt_MASK_Per_STS1(L, M)                             (0x58090+(L)*0x2+(M))

/*--------------------------------------
BitField Name: FMPM_TCA_STS1_MSK
BitField Type: R/W
BitField Desc: Interrupt MASK per STS1
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_STS24_Interrupt_MASK_Per_STS1_FMPM_TCA_STS1_MSK_Mask                                cBit23_0
#define cAf6_FMPM_TCA_STS24_Interrupt_MASK_Per_STS1_FMPM_TCA_STS1_MSK_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA STS24 Interrupt Sticky Per Type Of Per STS1
Reg Addr   : 0x5A000-0x5A1FF
Reg Formula: 0x5A000+$L*0x40+$M*0x20+$N
    Where  :
           + $L(0-3) : TFI-5 line identifier
           + $M(0-1) : 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s
           + $N(0-23) : STS-1s in a STS-24 group will be numbered from 0 to 23
Reg Desc   :
FMPM TCA Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_Base                                 0x5A000
#define cAf6Reg_FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1(L, M, N)         (0x5A000+(L)*0x40+(M)*0x20+(N))

/*--------------------------------------
BitField Name: FMPM_TCA_STS1_STK_CV_P
BitField Type: W1C
BitField Desc: CV_P
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_STK_CV_P_Mask                                  cBit16
#define cAf6_FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_STK_CV_P_Shift                                      16

/*--------------------------------------
BitField Name: FMPM_TCA_STS1_STK_ES_P
BitField Type: W1C
BitField Desc: ES_P
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_STK_ES_P_Mask                                  cBit15
#define cAf6_FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_STK_ES_P_Shift                                      15

/*--------------------------------------
BitField Name: FMPM_TCA_STS1_STK_SES_P
BitField Type: W1C
BitField Desc: SES_P
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_STK_SES_P_Mask                                  cBit14
#define cAf6_FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_STK_SES_P_Shift                                      14

/*--------------------------------------
BitField Name: FMPM_TCA_STS1_STK_UAS_P
BitField Type: W1C
BitField Desc: UAS_P
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_STK_UAS_P_Mask                                  cBit13
#define cAf6_FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_STK_UAS_P_Shift                                      13

/*--------------------------------------
BitField Name: FMPM_TCA_STS1_STK_PPJC_PDet
BitField Type: W1C
BitField Desc: PPJC_PDet
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_STK_PPJC_PDet_Mask                                  cBit12
#define cAf6_FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_STK_PPJC_PDet_Shift                                      12

/*--------------------------------------
BitField Name: FMPM_TCA_STS1_STK_NPJC_PDet
BitField Type: W1C
BitField Desc: NPJC_PDet
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_STK_NPJC_PDet_Mask                                  cBit11
#define cAf6_FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_STK_NPJC_PDet_Shift                                      11

/*--------------------------------------
BitField Name: FMPM_TCA_STS1_STK_PPJC_PGen
BitField Type: W1C
BitField Desc: PPJC_PGen
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_STK_PPJC_PGen_Mask                                  cBit10
#define cAf6_FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_STK_PPJC_PGen_Shift                                      10

/*--------------------------------------
BitField Name: FMPM_TCA_STS1_STK_NPJC_PGen
BitField Type: W1C
BitField Desc: NPJC_PGen
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_STK_NPJC_PGen_Mask                                   cBit9
#define cAf6_FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_STK_NPJC_PGen_Shift                                       9

/*--------------------------------------
BitField Name: FMPM_TCA_STS1_STK_PJCDiff_P
BitField Type: W1C
BitField Desc: PJCDiff_P
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_STK_PJCDiff_P_Mask                                   cBit8
#define cAf6_FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_STK_PJCDiff_P_Shift                                       8

/*--------------------------------------
BitField Name: FMPM_TCA_STS1_STK_PJCS_Det
BitField Type: W1C
BitField Desc: PJCS_Det
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_STK_PJCS_Det_Mask                                   cBit7
#define cAf6_FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_STK_PJCS_Det_Shift                                       7

/*--------------------------------------
BitField Name: FMPM_TCA_STS1_STK_PJCS_Gen
BitField Type: W1C
BitField Desc: PJCS_Gen
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_STK_PJCS_Gen_Mask                                   cBit6
#define cAf6_FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_STK_PJCS_Gen_Shift                                       6

/*--------------------------------------
BitField Name: FMPM_TCA_STS1_STK_CV_PFE
BitField Type: W1C
BitField Desc: CV_PFE
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_STK_CV_PFE_Mask                                   cBit5
#define cAf6_FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_STK_CV_PFE_Shift                                       5

/*--------------------------------------
BitField Name: FMPM_TCA_STS1_STK_ES_PFE
BitField Type: W1C
BitField Desc: ES_PFE
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_STK_ES_PFE_Mask                                   cBit4
#define cAf6_FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_STK_ES_PFE_Shift                                       4

/*--------------------------------------
BitField Name: FMPM_TCA_STS1_STK_SES_PFE
BitField Type: W1C
BitField Desc: SES_PFE
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_STK_SES_PFE_Mask                                   cBit3
#define cAf6_FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_STK_SES_PFE_Shift                                       3

/*--------------------------------------
BitField Name: FMPM_TCA_STS1_STK_UAS_PFE
BitField Type: W1C
BitField Desc: UAS_PFE
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_STK_UAS_PFE_Mask                                   cBit2
#define cAf6_FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_STK_UAS_PFE_Shift                                       2

/*--------------------------------------
BitField Name: FMPM_TCA_STS1_STK_FC_P
BitField Type: W1C
BitField Desc: FC_P
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_STK_FC_P_Mask                                   cBit1
#define cAf6_FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_STK_FC_P_Shift                                       1

/*--------------------------------------
BitField Name: FMPM_TCA_STS1_STK_FC_PFE
BitField Type: W1C
BitField Desc: FC_PFE
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_STK_FC_PFE_Mask                                   cBit0
#define cAf6_FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_STK_FC_PFE_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA STS24 Interrupt MASK Per Type Of Per STS1
Reg Addr   : 0x5A200-0x5A3FF
Reg Formula: 0x5A200+$L*0x40+$M*0x20+$N
    Where  :
           + $L(0-3) : TFI-5 line identifier
           + $M(0-1) : 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s
           + $N(0-23) : STS-1s in a STS-24 group will be numbered from 0 to 23
Reg Desc   :
FMPM TCA Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_Base                                 0x5A200
#define cAf6Reg_FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1(L, M, N)         (0x5A200+(L)*0x40+(M)*0x20+(N))

/*--------------------------------------
BitField Name: FMPM_TCA_STS1_MSK_CV_P
BitField Type: R/W
BitField Desc: CV_P
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_MSK_CV_P_Mask                                  cBit16
#define cAf6_FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_MSK_CV_P_Shift                                      16

/*--------------------------------------
BitField Name: FMPM_TCA_STS1_MSK_ES_P
BitField Type: R/W
BitField Desc: ES_P
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_MSK_ES_P_Mask                                  cBit15
#define cAf6_FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_MSK_ES_P_Shift                                      15

/*--------------------------------------
BitField Name: FMPM_TCA_STS1_MSK_SES_P
BitField Type: R/W
BitField Desc: SES_P
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_MSK_SES_P_Mask                                  cBit14
#define cAf6_FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_MSK_SES_P_Shift                                      14

/*--------------------------------------
BitField Name: FMPM_TCA_STS1_MSK_UAS_P
BitField Type: R/W
BitField Desc: UAS_P
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_MSK_UAS_P_Mask                                  cBit13
#define cAf6_FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_MSK_UAS_P_Shift                                      13

/*--------------------------------------
BitField Name: FMPM_TCA_STS1_MSK_PPJC_PDet
BitField Type: R/W
BitField Desc: PPJC_PDet
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_MSK_PPJC_PDet_Mask                                  cBit12
#define cAf6_FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_MSK_PPJC_PDet_Shift                                      12

/*--------------------------------------
BitField Name: FMPM_TCA_STS1_MSK_NPJC_PDet
BitField Type: R/W
BitField Desc: NPJC_PDet
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_MSK_NPJC_PDet_Mask                                  cBit11
#define cAf6_FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_MSK_NPJC_PDet_Shift                                      11

/*--------------------------------------
BitField Name: FMPM_TCA_STS1_MSK_PPJC_PGen
BitField Type: R/W
BitField Desc: PPJC_PGen
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_MSK_PPJC_PGen_Mask                                  cBit10
#define cAf6_FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_MSK_PPJC_PGen_Shift                                      10

/*--------------------------------------
BitField Name: FMPM_TCA_STS1_MSK_NPJC_PGen
BitField Type: R/W
BitField Desc: NPJC_PGen
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_MSK_NPJC_PGen_Mask                                   cBit9
#define cAf6_FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_MSK_NPJC_PGen_Shift                                       9

/*--------------------------------------
BitField Name: FMPM_TCA_STS1_MSK_PJCDiff_P
BitField Type: R/W
BitField Desc: PJCDiff_P
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_MSK_PJCDiff_P_Mask                                   cBit8
#define cAf6_FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_MSK_PJCDiff_P_Shift                                       8

/*--------------------------------------
BitField Name: FMPM_TCA_STS1_MSK_PJCS_Det
BitField Type: R/W
BitField Desc: PJCS_Det
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_MSK_PJCS_Det_Mask                                   cBit7
#define cAf6_FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_MSK_PJCS_Det_Shift                                       7

/*--------------------------------------
BitField Name: FMPM_TCA_STS1_MSK_PJCS_Gen
BitField Type: R/W
BitField Desc: PJCS_Gen
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_MSK_PJCS_Gen_Mask                                   cBit6
#define cAf6_FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_MSK_PJCS_Gen_Shift                                       6

/*--------------------------------------
BitField Name: FMPM_TCA_STS1_MSK_CV_PFE
BitField Type: R/W
BitField Desc: CV_PFE
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_MSK_CV_PFE_Mask                                   cBit5
#define cAf6_FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_MSK_CV_PFE_Shift                                       5

/*--------------------------------------
BitField Name: FMPM_TCA_STS1_MSK_ES_PFE
BitField Type: R/W
BitField Desc: ES_PFE
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_MSK_ES_PFE_Mask                                   cBit4
#define cAf6_FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_MSK_ES_PFE_Shift                                       4

/*--------------------------------------
BitField Name: FMPM_TCA_STS1_MSK_SES_PFE
BitField Type: R/W
BitField Desc: SES_PFE
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_MSK_SES_PFE_Mask                                   cBit3
#define cAf6_FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_MSK_SES_PFE_Shift                                       3

/*--------------------------------------
BitField Name: FMPM_TCA_STS1_MSK_UAS_PFE
BitField Type: R/W
BitField Desc: UAS_PFE
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_MSK_UAS_PFE_Mask                                   cBit2
#define cAf6_FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_MSK_UAS_PFE_Shift                                       2

/*--------------------------------------
BitField Name: FMPM_TCA_STS1_MSK_FC_P
BitField Type: R/W
BitField Desc: FC_P
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_MSK_FC_P_Mask                                   cBit1
#define cAf6_FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_MSK_FC_P_Shift                                       1

/*--------------------------------------
BitField Name: FMPM_TCA_STS1_MSK_FC_PFE
BitField Type: R/W
BitField Desc: FC_PFE
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_MSK_FC_PFE_Mask                                   cBit0
#define cAf6_FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1_FMPM_TCA_STS1_MSK_FC_PFE_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA DS3E3 Group Interrupt OR
Reg Addr   : 0x58001
Reg Formula:
    Where  :
Reg Desc   :
FMPM TCA Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_DS3E3_Group_Interrupt_OR                                                      0x58001

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_OR
BitField Type: R_O
BitField Desc: Interrupt DS3/E3 OR
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Group_Interrupt_OR_FMPM_TCA_DS3E3_OR_Mask                                    cBit0
#define cAf6_FMPM_TCA_DS3E3_Group_Interrupt_OR_FMPM_TCA_DS3E3_OR_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA DS3E3 Framer Interrupt OR AND MASK Per DS3E3
Reg Addr   : 0x580A0-0x580AF
Reg Formula: 0x580A0+$G
    Where  :
           + $G(0-7) : DS3/E3 Group  Interrupt
Reg Desc   :
FMPM TCA Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_DS3E3_Framer_Interrupt_OR_AND_MASK_Per_DS3E3_Base                                 0x580A0
#define cAf6Reg_FMPM_TCA_DS3E3_Framer_Interrupt_OR_AND_MASK_Per_DS3E3(G)                           (0x580A0+(G))

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_OAMSK
BitField Type: R_O
BitField Desc: Interrupt DS3/E3 Group
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_OR_AND_MASK_Per_DS3E3_FMPM_TCA_DS3E3_OAMSK_Mask                                cBit23_0
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_OR_AND_MASK_Per_DS3E3_FMPM_TCA_DS3E3_OAMSK_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA DS3E3 Framer Interrupt MASK Per DS3E3
Reg Addr   : 0x580B0-0x580BF
Reg Formula: 0x580B0+$G
    Where  :
           + $G(0-7) : DS3/E3 Group  Interrupt
Reg Desc   :
FMPM TCA Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_DS3E3_Base                                    0x580B0
#define cAf6Reg_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_DS3E3(G)                                (0x580B0+(G))

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_MSK
BitField Type: R/W
BitField Desc: MASK per DS3/E3 Framer
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_DS3E3_FMPM_TCA_DS3E3_MSK_Mask                                cBit23_0
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_DS3E3_FMPM_TCA_DS3E3_MSK_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA DS3E3 Framer Interrupt Sticky Per Type of Per DS3E3
Reg Addr   : 0x5A600-0x5A7FF
Reg Formula: 0x5A600+$G*0x20+$H
    Where  :
           + $G(0-7) : DS3/E3 Group  Interrupt
           + $H(0-23) : DS3/E3 Group  Interrupt
Reg Desc   :
FMPM TCA Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_Base                                 0x5A600
#define cAf6Reg_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3(G, H)                  (0x5A600+(G)*0x20+(H))

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_STK_CV_L
BitField Type: W1C
BitField Desc: CV_L
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_CV_L_Mask                                  cBit28
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_CV_L_Shift                                      28

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_STK_ES_L
BitField Type: W1C
BitField Desc: ES_L
BitField Bits: [27:27]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_ES_L_Mask                                  cBit27
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_ES_L_Shift                                      27

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_STK_ESA_L
BitField Type: W1C
BitField Desc: ESA_L
BitField Bits: [26:26]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_ESA_L_Mask                                  cBit26
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_ESA_L_Shift                                      26

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_STK_ESB_L
BitField Type: W1C
BitField Desc: ESB_L
BitField Bits: [25:25]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_ESB_L_Mask                                  cBit25
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_ESB_L_Shift                                      25

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_STK_SES_L
BitField Type: W1C
BitField Desc: SES_L
BitField Bits: [24:24]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_SES_L_Mask                                  cBit24
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_SES_L_Shift                                      24

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_STK_LOSS_L
BitField Type: W1C
BitField Desc: LOSS_L
BitField Bits: [23:23]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_LOSS_L_Mask                                  cBit23
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_LOSS_L_Shift                                      23

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_STK_CVP_P
BitField Type: W1C
BitField Desc: CVP_P
BitField Bits: [22:22]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_CVP_P_Mask                                  cBit22
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_CVP_P_Shift                                      22

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_STK_CVCP_P
BitField Type: W1C
BitField Desc: CVCP_P
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_CVCP_P_Mask                                  cBit21
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_CVCP_P_Shift                                      21

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_STK_ESP_P
BitField Type: W1C
BitField Desc: ESP_P
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_ESP_P_Mask                                  cBit20
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_ESP_P_Shift                                      20

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_STK_ESCP_P
BitField Type: W1C
BitField Desc: ESCP_P
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_ESCP_P_Mask                                  cBit19
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_ESCP_P_Shift                                      19

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_STK_ESAP_P
BitField Type: W1C
BitField Desc: ESAP_P
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_ESAP_P_Mask                                  cBit18
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_ESAP_P_Shift                                      18

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_STK_ESACP_P
BitField Type: W1C
BitField Desc: ESACP_P
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_ESACP_P_Mask                                  cBit17
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_ESACP_P_Shift                                      17

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_STK_ESBP_P
BitField Type: W1C
BitField Desc: ESBP_P
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_ESBP_P_Mask                                  cBit16
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_ESBP_P_Shift                                      16

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_STK_ESBCP_P
BitField Type: W1C
BitField Desc: ESBCP_P
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_ESBCP_P_Mask                                  cBit15
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_ESBCP_P_Shift                                      15

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_STK_SESP_P
BitField Type: W1C
BitField Desc: SESP_P
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_SESP_P_Mask                                  cBit14
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_SESP_P_Shift                                      14

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_STK_SESCP_P
BitField Type: W1C
BitField Desc: SESCP_P
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_SESCP_P_Mask                                  cBit13
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_SESCP_P_Shift                                      13

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_STK_SAS_P
BitField Type: W1C
BitField Desc: SAS_P
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_SAS_P_Mask                                  cBit12
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_SAS_P_Shift                                      12

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_STK_AISS_P
BitField Type: W1C
BitField Desc: AISS_P
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_AISS_P_Mask                                  cBit11
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_AISS_P_Shift                                      11

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_STK_UAS_P
BitField Type: W1C
BitField Desc: UAS_P
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_UAS_P_Mask                                  cBit10
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_UAS_P_Shift                                      10

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_STK_UASCP_P
BitField Type: W1C
BitField Desc: UASCP_P
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_UASCP_P_Mask                                   cBit9
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_UASCP_P_Shift                                       9

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_STK_CVCP_PFE
BitField Type: W1C
BitField Desc: CVCP_PFE
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_CVCP_PFE_Mask                                   cBit8
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_CVCP_PFE_Shift                                       8

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_STK_ESCP_PFE
BitField Type: W1C
BitField Desc: ESCP_PFE
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_ESCP_PFE_Mask                                   cBit7
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_ESCP_PFE_Shift                                       7

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_STK_ESACP_PFE
BitField Type: W1C
BitField Desc: ESACP_PFE
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_ESACP_PFE_Mask                                   cBit6
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_ESACP_PFE_Shift                                       6

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_STK_ESBCP_PFE
BitField Type: W1C
BitField Desc: ESBCP_PFE
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_ESBCP_PFE_Mask                                   cBit5
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_ESBCP_PFE_Shift                                       5

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_STK_SESCP_PFE
BitField Type: W1C
BitField Desc: SESCP_PFE
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_SESCP_PFE_Mask                                   cBit4
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_SESCP_PFE_Shift                                       4

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_STK_SASCP_PFE
BitField Type: W1C
BitField Desc: SASCP_PFE
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_SASCP_PFE_Mask                                   cBit3
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_SASCP_PFE_Shift                                       3

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_STK_UASCP_PFE
BitField Type: W1C
BitField Desc: UASCP_PFE
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_UASCP_PFE_Mask                                   cBit2
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_UASCP_PFE_Shift                                       2

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_STK_FC_P
BitField Type: W1C
BitField Desc: FC_P
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_FC_P_Mask                                   cBit1
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_FC_P_Shift                                       1

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_STK_FCCP_PFE
BitField Type: W1C
BitField Desc: FCCP_PFE
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_FCCP_PFE_Mask                                   cBit0
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_STK_FCCP_PFE_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA DS3E3 Framer Interrupt MASK Per Type of Per DS3E3
Reg Addr   : 0x5A800-0x5A9FF
Reg Formula: 0x5A800+$G*0x20+$H
    Where  :
           + $G(0-7) : DS3/E3 Group  Interrupt
           + $H(0-23) : DS3/E3 Group  Interrupt
Reg Desc   :
FMPM TCA Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_Base                                 0x5A800
#define cAf6Reg_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3(G, H)                  (0x5A800+(G)*0x20+(H))

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_MSK_CV_L
BitField Type: R/W
BitField Desc: CV_L
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_CV_L_Mask                                  cBit28
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_CV_L_Shift                                      28

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_MSK_ES_L
BitField Type: R/W
BitField Desc: ES_L
BitField Bits: [27:27]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_ES_L_Mask                                  cBit27
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_ES_L_Shift                                      27

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_MSK_ESA_L
BitField Type: R/W
BitField Desc: ESA_L
BitField Bits: [26:26]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_ESA_L_Mask                                  cBit26
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_ESA_L_Shift                                      26

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_MSK_ESB_L
BitField Type: R/W
BitField Desc: ESB_L
BitField Bits: [25:25]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_ESB_L_Mask                                  cBit25
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_ESB_L_Shift                                      25

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_MSK_SES_L
BitField Type: R/W
BitField Desc: SES_L
BitField Bits: [24:24]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_SES_L_Mask                                  cBit24
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_SES_L_Shift                                      24

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_MSK_LOSS_L
BitField Type: R/W
BitField Desc: LOSS_L
BitField Bits: [23:23]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_LOSS_L_Mask                                  cBit23
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_LOSS_L_Shift                                      23

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_MSK_CVP_P
BitField Type: R/W
BitField Desc: CVP_P
BitField Bits: [22:22]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_CVP_P_Mask                                  cBit22
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_CVP_P_Shift                                      22

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_MSK_CVCP_P
BitField Type: R/W
BitField Desc: CVCP_P
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_CVCP_P_Mask                                  cBit21
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_CVCP_P_Shift                                      21

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_MSK_ESP_P
BitField Type: R/W
BitField Desc: ESP_P
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_ESP_P_Mask                                  cBit20
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_ESP_P_Shift                                      20

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_MSK_ESCP_P
BitField Type: R/W
BitField Desc: ESCP_P
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_ESCP_P_Mask                                  cBit19
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_ESCP_P_Shift                                      19

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_MSK_ESAP_P
BitField Type: R/W
BitField Desc: ESAP_P
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_ESAP_P_Mask                                  cBit18
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_ESAP_P_Shift                                      18

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_MSK_ESACP_P
BitField Type: R/W
BitField Desc: ESACP_P
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_ESACP_P_Mask                                  cBit17
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_ESACP_P_Shift                                      17

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_MSK_ESBP_P
BitField Type: R/W
BitField Desc: ESBP_P
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_ESBP_P_Mask                                  cBit16
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_ESBP_P_Shift                                      16

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_MSK_ESBCP_P
BitField Type: R/W
BitField Desc: ESBCP_P
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_ESBCP_P_Mask                                  cBit15
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_ESBCP_P_Shift                                      15

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_MSK_SESP_P
BitField Type: R/W
BitField Desc: SESP_P
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_SESP_P_Mask                                  cBit14
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_SESP_P_Shift                                      14

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_MSK_SESCP_P
BitField Type: R/W
BitField Desc: SESCP_P
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_SESCP_P_Mask                                  cBit13
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_SESCP_P_Shift                                      13

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_MSK_SAS_P
BitField Type: R/W
BitField Desc: SAS_P
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_SAS_P_Mask                                  cBit12
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_SAS_P_Shift                                      12

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_MSK_AISS_P
BitField Type: R/W
BitField Desc: AISS_P
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_AISS_P_Mask                                  cBit11
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_AISS_P_Shift                                      11

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_MSK_UAS_P
BitField Type: R/W
BitField Desc: UAS_P
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_UAS_P_Mask                                  cBit10
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_UAS_P_Shift                                      10

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_MSK_UASCP_P
BitField Type: R/W
BitField Desc: UASCP_P
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_UASCP_P_Mask                                   cBit9
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_UASCP_P_Shift                                       9

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_MSK_CVCP_PFE
BitField Type: R/W
BitField Desc: CVCP_PFE
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_CVCP_PFE_Mask                                   cBit8
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_CVCP_PFE_Shift                                       8

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_MSK_ESCP_PFE
BitField Type: R/W
BitField Desc: ESCP_PFE
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_ESCP_PFE_Mask                                   cBit7
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_ESCP_PFE_Shift                                       7

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_MSK_ESACP_PFE
BitField Type: R/W
BitField Desc: ESACP_PFE
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_ESACP_PFE_Mask                                   cBit6
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_ESACP_PFE_Shift                                       6

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_MSK_ESBCP_PFE
BitField Type: R/W
BitField Desc: ESBCP_PFE
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_ESBCP_PFE_Mask                                   cBit5
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_ESBCP_PFE_Shift                                       5

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_MSK_SESCP_PFE
BitField Type: R/W
BitField Desc: SESCP_PFE
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_SESCP_PFE_Mask                                   cBit4
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_SESCP_PFE_Shift                                       4

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_MSK_SASCP_PFE
BitField Type: R/W
BitField Desc: SASCP_PFE
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_SASCP_PFE_Mask                                   cBit3
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_SASCP_PFE_Shift                                       3

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_MSK_UASCP_PFE
BitField Type: R/W
BitField Desc: UASCP_PFE
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_UASCP_PFE_Mask                                   cBit2
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_UASCP_PFE_Shift                                       2

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_MSK_FC_P
BitField Type: R/W
BitField Desc: FC_P
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_FC_P_Mask                                   cBit1
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_FC_P_Shift                                       1

/*--------------------------------------
BitField Name: FMPM_TCA_DS3E3_MSK_FCCP_PFE
BitField Type: R/W
BitField Desc: FCCP_PFE
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_FCCP_PFE_Mask                                   cBit0
#define cAf6_FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3_FMPM_TCA_DS3E3_MSK_FCCP_PFE_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA STS24 LO Interrupt OR
Reg Addr   : 0x58002
Reg Formula:
    Where  :
Reg Desc   :
FMPM TCA Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_STS24_LO_Interrupt_OR                                                         0x58002

/*--------------------------------------
BitField Name: FMPM_TCA_STS24LO_OR
BitField Type: R_O
BitField Desc: Interrupt STS24 LO OR
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_STS24_LO_Interrupt_OR_FMPM_TCA_STS24LO_OR_Mask                                     cBit0
#define cAf6_FMPM_TCA_STS24_LO_Interrupt_OR_FMPM_TCA_STS24LO_OR_Shift                                        0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA STS1 LO Interrupt OR
Reg Addr   : 0x580C0-0x580CF
Reg Formula: 0x580C0+$G
    Where  :
           + $G(0-7) : STS-24 LO group
Reg Desc   :
FMPM TCA Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_STS1_LO_Interrupt_OR_Base                                                     0x580C0
#define cAf6Reg_FMPM_TCA_STS1_LO_Interrupt_OR(G)                                                 (0x580C0+(G))

/*--------------------------------------
BitField Name: FMPM_TCA_STS1LO_OR
BitField Type: R_O
BitField Desc: Interrupt STS1 LO OR
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_STS1_LO_Interrupt_OR_FMPM_TCA_STS1LO_OR_Mask                                    cBit23_0
#define cAf6_FMPM_TCA_STS1_LO_Interrupt_OR_FMPM_TCA_STS1LO_OR_Shift                                          0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA VTTU Interrupt OR AND MASK Per VTTU
Reg Addr   : 0x5AC00-0x5ADFF
Reg Formula: 0x5AC00+$G*0x20+$H
    Where  :
           + $G(0-7) : STS-24 LO group
           + $H(0-23) : STS-1 LO group
Reg Desc   :
FMPM TCA Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_VTTU_Interrupt_OR_AND_MASK_Per_VTTU_Base                                      0x5AC00
#define cAf6Reg_FMPM_TCA_VTTU_Interrupt_OR_AND_MASK_Per_VTTU(G, H)                      (0x5AC00+(G)*0x20+(H))

/*--------------------------------------
BitField Name: FMPM_TCA_VTTU_OAMSK
BitField Type: R_O
BitField Desc: VT/TU Interrupt
BitField Bits: [27:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_VTTU_Interrupt_OR_AND_MASK_Per_VTTU_FMPM_TCA_VTTU_OAMSK_Mask                                cBit27_0
#define cAf6_FMPM_TCA_VTTU_Interrupt_OR_AND_MASK_Per_VTTU_FMPM_TCA_VTTU_OAMSK_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA VTTU Interrupt MASK Per VTTU
Reg Addr   : 0x5AE00-0x5AFFF
Reg Formula: 0x5AE00+$G*0x20+$H
    Where  :
           + $G(0-7) : STS-24 LO group
           + $H(0-23) : STS-1 LO group
Reg Desc   :
FMPM TCA Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_VTTU_Interrupt_MASK_Per_VTTU_Base                                             0x5AE00
#define cAf6Reg_FMPM_TCA_VTTU_Interrupt_MASK_Per_VTTU(G, H)                             (0x5AE00+(G)*0x20+(H))

/*--------------------------------------
BitField Name: FMPM_TCA_VTTU_MSK
BitField Type: R/W
BitField Desc: VT/TU Interrupt
BitField Bits: [27:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_VTTU_Interrupt_MASK_Per_VTTU_FMPM_TCA_VTTU_MSK_Mask                                cBit27_0
#define cAf6_FMPM_TCA_VTTU_Interrupt_MASK_Per_VTTU_FMPM_TCA_VTTU_MSK_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA VTTU Interrupt Sticky Per Type Per VTTU
Reg Addr   : 0x64000-0x67FFF
Reg Formula: 0x64000+$G*0x400+$H*0x20+$I
    Where  :
           + $G(0-7) : STS-24 LO group
           + $H(0-23) : STS-1 LO group
           + $I(0-27) : VT/TU
Reg Desc   :
FMPM TCA Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_Base                                  0x64000
#define cAf6Reg_FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU(G, H, I)        (0x64000+(G)*0x400+(H)*0x20+(I))

/*--------------------------------------
BitField Name: FMPM_TCA_VTTU_STK_CV_V
BitField Type: W1C
BitField Desc: CV_V
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_TCA_VTTU_STK_CV_V_Mask                                  cBit16
#define cAf6_FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_TCA_VTTU_STK_CV_V_Shift                                      16

/*--------------------------------------
BitField Name: FMPM_TCA_VTTU_STK_ES_V
BitField Type: W1C
BitField Desc: ES_V
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_TCA_VTTU_STK_ES_V_Mask                                  cBit15
#define cAf6_FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_TCA_VTTU_STK_ES_V_Shift                                      15

/*--------------------------------------
BitField Name: FMPM_TCA_VTTU_STK_SES_V
BitField Type: W1C
BitField Desc: SES_V
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_TCA_VTTU_STK_SES_V_Mask                                  cBit14
#define cAf6_FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_TCA_VTTU_STK_SES_V_Shift                                      14

/*--------------------------------------
BitField Name: FMPM_TCA_VTTU_STK_UAS_V
BitField Type: W1C
BitField Desc: UAS_V
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_TCA_VTTU_STK_UAS_V_Mask                                  cBit13
#define cAf6_FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_TCA_VTTU_STK_UAS_V_Shift                                      13

/*--------------------------------------
BitField Name: FMPM_TCA_VTTU_STK_PPJC_VDet
BitField Type: W1C
BitField Desc: PPJC_VDet
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_TCA_VTTU_STK_PPJC_VDet_Mask                                  cBit12
#define cAf6_FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_TCA_VTTU_STK_PPJC_VDet_Shift                                      12

/*--------------------------------------
BitField Name: FMPM_TCA_VTTU_STK_NPJC_VDet
BitField Type: W1C
BitField Desc: NPJC_VDet
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_TCA_VTTU_STK_NPJC_VDet_Mask                                  cBit11
#define cAf6_FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_TCA_VTTU_STK_NPJC_VDet_Shift                                      11

/*--------------------------------------
BitField Name: FMPM_TCA_VTTU_STK_PPJC_VGen
BitField Type: W1C
BitField Desc: PPJC_VGen
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_TCA_VTTU_STK_PPJC_VGen_Mask                                  cBit10
#define cAf6_FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_TCA_VTTU_STK_PPJC_VGen_Shift                                      10

/*--------------------------------------
BitField Name: FMPM_TCA_VTTU_STK_NPJC_VGen
BitField Type: W1C
BitField Desc: NPJC_VGen
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_TCA_VTTU_STK_NPJC_VGen_Mask                                   cBit9
#define cAf6_FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_TCA_VTTU_STK_NPJC_VGen_Shift                                       9

/*--------------------------------------
BitField Name: FMPM_TCA_VTTU_STK_PJCDiff_V
BitField Type: W1C
BitField Desc: PJCDiff_V
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_TCA_VTTU_STK_PJCDiff_V_Mask                                   cBit8
#define cAf6_FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_TCA_VTTU_STK_PJCDiff_V_Shift                                       8

/*--------------------------------------
BitField Name: FMPM_TCA_VTTU_STK_PJCS_VDet
BitField Type: W1C
BitField Desc: PJCS_VDet
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_TCA_VTTU_STK_PJCS_VDet_Mask                                   cBit7
#define cAf6_FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_TCA_VTTU_STK_PJCS_VDet_Shift                                       7

/*--------------------------------------
BitField Name: FMPM_TCA_VTTU_STK_PJCS_VGen
BitField Type: W1C
BitField Desc: PJCS_VGen
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_TCA_VTTU_STK_PJCS_VGen_Mask                                   cBit6
#define cAf6_FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_TCA_VTTU_STK_PJCS_VGen_Shift                                       6

/*--------------------------------------
BitField Name: FMPM_TCA_VTTU_STK_CV_VFE
BitField Type: W1C
BitField Desc: CV_VFE
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_TCA_VTTU_STK_CV_VFE_Mask                                   cBit5
#define cAf6_FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_TCA_VTTU_STK_CV_VFE_Shift                                       5

/*--------------------------------------
BitField Name: FMPM_TCA_VTTU_STK_ES_VFE
BitField Type: W1C
BitField Desc: ES_VFE
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_TCA_VTTU_STK_ES_VFE_Mask                                   cBit4
#define cAf6_FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_TCA_VTTU_STK_ES_VFE_Shift                                       4

/*--------------------------------------
BitField Name: FMPM_TCA_VTTU_STK_SES_VFE
BitField Type: W1C
BitField Desc: SES_VFE
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_TCA_VTTU_STK_SES_VFE_Mask                                   cBit3
#define cAf6_FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_TCA_VTTU_STK_SES_VFE_Shift                                       3

/*--------------------------------------
BitField Name: FMPM_TCA_VTTU_STK_UAS_VFE
BitField Type: W1C
BitField Desc: UAS_VFE
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_TCA_VTTU_STK_UAS_VFE_Mask                                   cBit2
#define cAf6_FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_TCA_VTTU_STK_UAS_VFE_Shift                                       2

/*--------------------------------------
BitField Name: FMPM_TCA_VTTU_STK_FC_V
BitField Type: W1C
BitField Desc: FC_V
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_TCA_VTTU_STK_FC_V_Mask                                   cBit1
#define cAf6_FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_TCA_VTTU_STK_FC_V_Shift                                       1

/*--------------------------------------
BitField Name: FMPM_TCA_VTTU_STK_FC_VFE
BitField Type: W1C
BitField Desc: FC_VFE
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_TCA_VTTU_STK_FC_VFE_Mask                                   cBit0
#define cAf6_FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU_FMPM_TCA_VTTU_STK_FC_VFE_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA VTTU Interrupt MASK Per Type Per VTTU
Reg Addr   : 0x68000-0x6BFFF
Reg Formula: 0x68000+$G*0x400+$H*0x20+$I
    Where  :
           + $G(0-7) : STS-24 LO group
           + $H(0-23) : STS-1 LO group
           + $I(0-27) : VT/TU
Reg Desc   :
FMPM TCA Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_Base                                    0x68000
#define cAf6Reg_FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU(G, H, I)        (0x68000+(G)*0x400+(H)*0x20+(I))

/*--------------------------------------
BitField Name: FMPM_TCA_VTTU_MSK_CV_V
BitField Type: R/W
BitField Desc: CV_V
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_TCA_VTTU_MSK_CV_V_Mask                                  cBit16
#define cAf6_FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_TCA_VTTU_MSK_CV_V_Shift                                      16

/*--------------------------------------
BitField Name: FMPM_TCA_VTTU_MSK_ES_V
BitField Type: R/W
BitField Desc: ES_V
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_TCA_VTTU_MSK_ES_V_Mask                                  cBit15
#define cAf6_FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_TCA_VTTU_MSK_ES_V_Shift                                      15

/*--------------------------------------
BitField Name: FMPM_TCA_VTTU_MSK_SES_V
BitField Type: R/W
BitField Desc: SES_V
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_TCA_VTTU_MSK_SES_V_Mask                                  cBit14
#define cAf6_FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_TCA_VTTU_MSK_SES_V_Shift                                      14

/*--------------------------------------
BitField Name: FMPM_TCA_VTTU_MSK_UAS_V
BitField Type: R/W
BitField Desc: UAS_V
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_TCA_VTTU_MSK_UAS_V_Mask                                  cBit13
#define cAf6_FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_TCA_VTTU_MSK_UAS_V_Shift                                      13

/*--------------------------------------
BitField Name: FMPM_TCA_VTTU_MSK_PPJC_VDet
BitField Type: R/W
BitField Desc: PPJC_VDet
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_TCA_VTTU_MSK_PPJC_VDet_Mask                                  cBit12
#define cAf6_FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_TCA_VTTU_MSK_PPJC_VDet_Shift                                      12

/*--------------------------------------
BitField Name: FMPM_TCA_VTTU_MSK_NPJC_VDet
BitField Type: R/W
BitField Desc: NPJC_VDet
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_TCA_VTTU_MSK_NPJC_VDet_Mask                                  cBit11
#define cAf6_FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_TCA_VTTU_MSK_NPJC_VDet_Shift                                      11

/*--------------------------------------
BitField Name: FMPM_TCA_VTTU_MSK_PPJC_VGen
BitField Type: R/W
BitField Desc: PPJC_VGen
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_TCA_VTTU_MSK_PPJC_VGen_Mask                                  cBit10
#define cAf6_FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_TCA_VTTU_MSK_PPJC_VGen_Shift                                      10

/*--------------------------------------
BitField Name: FMPM_TCA_VTTU_MSK_NPJC_VGen
BitField Type: R/W
BitField Desc: NPJC_VGen
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_TCA_VTTU_MSK_NPJC_VGen_Mask                                   cBit9
#define cAf6_FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_TCA_VTTU_MSK_NPJC_VGen_Shift                                       9

/*--------------------------------------
BitField Name: FMPM_TCA_VTTU_MSK_PJCDiff_V
BitField Type: R/W
BitField Desc: PJCDiff_V
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_TCA_VTTU_MSK_PJCDiff_V_Mask                                   cBit8
#define cAf6_FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_TCA_VTTU_MSK_PJCDiff_V_Shift                                       8

/*--------------------------------------
BitField Name: FMPM_TCA_VTTU_MSK_PJCS_VDet
BitField Type: R/W
BitField Desc: PJCS_VDet
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_TCA_VTTU_MSK_PJCS_VDet_Mask                                   cBit7
#define cAf6_FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_TCA_VTTU_MSK_PJCS_VDet_Shift                                       7

/*--------------------------------------
BitField Name: FMPM_TCA_VTTU_MSK_PJCS_VGen
BitField Type: R/W
BitField Desc: PJCS_VGen
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_TCA_VTTU_MSK_PJCS_VGen_Mask                                   cBit6
#define cAf6_FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_TCA_VTTU_MSK_PJCS_VGen_Shift                                       6

/*--------------------------------------
BitField Name: FMPM_TCA_VTTU_MSK_CV_VFE
BitField Type: R/W
BitField Desc: CV_VFE
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_TCA_VTTU_MSK_CV_VFE_Mask                                   cBit5
#define cAf6_FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_TCA_VTTU_MSK_CV_VFE_Shift                                       5

/*--------------------------------------
BitField Name: FMPM_TCA_VTTU_MSK_ES_VFE
BitField Type: R/W
BitField Desc: ES_VFE
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_TCA_VTTU_MSK_ES_VFE_Mask                                   cBit4
#define cAf6_FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_TCA_VTTU_MSK_ES_VFE_Shift                                       4

/*--------------------------------------
BitField Name: FMPM_TCA_VTTU_MSK_SES_VFE
BitField Type: R/W
BitField Desc: SES_VFE
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_TCA_VTTU_MSK_SES_VFE_Mask                                   cBit3
#define cAf6_FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_TCA_VTTU_MSK_SES_VFE_Shift                                       3

/*--------------------------------------
BitField Name: FMPM_TCA_VTTU_MSK_UAS_VFE
BitField Type: R/W
BitField Desc: UAS_VFE
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_TCA_VTTU_MSK_UAS_VFE_Mask                                   cBit2
#define cAf6_FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_TCA_VTTU_MSK_UAS_VFE_Shift                                       2

/*--------------------------------------
BitField Name: FMPM_TCA_VTTU_MSK_FC_V
BitField Type: R/W
BitField Desc: FC_V
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_TCA_VTTU_MSK_FC_V_Mask                                   cBit1
#define cAf6_FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_TCA_VTTU_MSK_FC_V_Shift                                       1

/*--------------------------------------
BitField Name: FMPM_TCA_VTTU_MSK_FC_VFE
BitField Type: R/W
BitField Desc: FC_VFE
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_TCA_VTTU_MSK_FC_VFE_Mask                                   cBit0
#define cAf6_FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU_FMPM_TCA_VTTU_MSK_FC_VFE_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA DS1E1 Level1 Group Interrupt OR
Reg Addr   : 0x58003
Reg Formula:
    Where  :
Reg Desc   :
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_DS1E1_Level1_Group_Interrupt_OR                                               0x58003

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_LEV1_OR
BitField Type: R_O
BitField Desc: Interrupt DS1E1 level1 group OR
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS1E1_Level1_Group_Interrupt_OR_FMPM_FM_DS1E1_LEV1_OR_Mask                                   cBit0
#define cAf6_FMPM_TCA_DS1E1_Level1_Group_Interrupt_OR_FMPM_FM_DS1E1_LEV1_OR_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA DS1E1 Level2 Group Interrupt OR
Reg Addr   : 0x580D0-0x580DF
Reg Formula: 0x580D0+$G
    Where  :
           + $G(0-7) : DS1/E1 Level-1 Group
Reg Desc   :
FMPM TCA Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_DS1E1_Level2_Group_Interrupt_OR_Base                                          0x580D0
#define cAf6Reg_FMPM_TCA_DS1E1_Level2_Group_Interrupt_OR(G)                                      (0x580D0+(G))

/*--------------------------------------
BitField Name: FMPM_TCA_DS1E1_LEV2_OR
BitField Type: R_O
BitField Desc: Level2 Group Interrupt OR
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS1E1_Level2_Group_Interrupt_OR_FMPM_TCA_DS1E1_LEV2_OR_Mask                                cBit23_0
#define cAf6_FMPM_TCA_DS1E1_Level2_Group_Interrupt_OR_FMPM_TCA_DS1E1_LEV2_OR_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA DS1E1 Framer Interrupt OR AND MASK Per DS1E1
Reg Addr   : 0x5B000-0x5B1FF
Reg Formula: 0x5B000+$G*0x20+$H
    Where  :
           + $G(0-7) : DS1/E1 Level-1 Group
           + $H(0-23) : DS1/E1 Level-2 Group
Reg Desc   :
FMPM TCA Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1_Base                                 0x5B000
#define cAf6Reg_FMPM_TCA_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1(G, H)                  (0x5B000+(G)*0x20+(H))

/*--------------------------------------
BitField Name: FMPM_TCA_DS1E1_OAMSK
BitField Type: R_O
BitField Desc: DS1/E1 Framer Interrupt
BitField Bits: [27:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1_FMPM_TCA_DS1E1_OAMSK_Mask                                cBit27_0
#define cAf6_FMPM_TCA_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1_FMPM_TCA_DS1E1_OAMSK_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA DS1E1 Framer Interrupt MASK Per DS1E1
Reg Addr   : 0x5B200-0x5B3FF
Reg Formula: 0x5B200+$G*0x20+$H
    Where  :
           + $G(0-7) : DS1/E1 Level-1 Group
           + $H(0-23) : DS1/E1 Level-2 Group
Reg Desc   :
FMPM TCA Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_DS1E1_Framer_Interrupt_MASK_Per_DS1E1_Base                                    0x5B200
#define cAf6Reg_FMPM_TCA_DS1E1_Framer_Interrupt_MASK_Per_DS1E1(G, H)                    (0x5B200+(G)*0x20+(H))

/*--------------------------------------
BitField Name: FMPM_TCA_DS1E1_MSK
BitField Type: R/W
BitField Desc: DS1/E1 Framer Interrupt
BitField Bits: [27:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS1E1_Framer_Interrupt_MASK_Per_DS1E1_FMPM_TCA_DS1E1_MSK_Mask                                cBit27_0
#define cAf6_FMPM_TCA_DS1E1_Framer_Interrupt_MASK_Per_DS1E1_FMPM_TCA_DS1E1_MSK_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA DS1E1 Interrupt Sticky Per Type Per DS1E1
Reg Addr   : 0x70000-0x73FFF
Reg Formula: 0x70000+$G*0x400+$H*0x20+$I
    Where  :
           + $G(0-7) : DS1/E1 Level-1 Group
           + $H(0-23) : DS1/E1 Level-2 Group
           + $I(0-27) : DS1/E1
Reg Desc   :
FMPM TCA Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_Base                                 0x70000
#define cAf6Reg_FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1(G, H, I)        (0x70000+(G)*0x400+(H)*0x20+(I))

/*--------------------------------------
BitField Name: FMPM_TCA_DS1E1_STK_CV_L
BitField Type: W1C
BitField Desc: CV_L
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_STK_CV_L_Mask                                  cBit18
#define cAf6_FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_STK_CV_L_Shift                                      18

/*--------------------------------------
BitField Name: FMPM_TCA_DS1E1_STK_ES_L
BitField Type: W1C
BitField Desc: ES_L
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_STK_ES_L_Mask                                  cBit17
#define cAf6_FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_STK_ES_L_Shift                                      17

/*--------------------------------------
BitField Name: FMPM_TCA_DS1E1_STK_SES_L
BitField Type: W1C
BitField Desc: SES_L
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_STK_SES_L_Mask                                  cBit16
#define cAf6_FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_STK_SES_L_Shift                                      16

/*--------------------------------------
BitField Name: FMPM_TCA_DS1E1_STK_LOSS_L
BitField Type: W1C
BitField Desc: LOSS_L
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_STK_LOSS_L_Mask                                  cBit15
#define cAf6_FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_STK_LOSS_L_Shift                                      15

/*--------------------------------------
BitField Name: FMPM_TCA_DS1E1_STK_CV_P
BitField Type: W1C
BitField Desc: CV_P
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_STK_CV_P_Mask                                  cBit14
#define cAf6_FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_STK_CV_P_Shift                                      14

/*--------------------------------------
BitField Name: FMPM_TCA_DS1E1_STK_ES_P
BitField Type: W1C
BitField Desc: ES_P
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_STK_ES_P_Mask                                  cBit13
#define cAf6_FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_STK_ES_P_Shift                                      13

/*--------------------------------------
BitField Name: FMPM_TCA_DS1E1_STK_SES_P
BitField Type: W1C
BitField Desc: SES_P
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_STK_SES_P_Mask                                  cBit12
#define cAf6_FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_STK_SES_P_Shift                                      12

/*--------------------------------------
BitField Name: FMPM_TCA_DS1E1_STK_AISS_P
BitField Type: W1C
BitField Desc: AISS_P
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_STK_AISS_P_Mask                                  cBit11
#define cAf6_FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_STK_AISS_P_Shift                                      11

/*--------------------------------------
BitField Name: FMPM_TCA_DS1E1_STK_SAS_P
BitField Type: W1C
BitField Desc: SAS_P
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_STK_SAS_P_Mask                                  cBit10
#define cAf6_FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_STK_SAS_P_Shift                                      10

/*--------------------------------------
BitField Name: FMPM_TCA_DS1E1_STK_CSS_P
BitField Type: W1C
BitField Desc: CSS_P
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_STK_CSS_P_Mask                                   cBit9
#define cAf6_FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_STK_CSS_P_Shift                                       9

/*--------------------------------------
BitField Name: FMPM_TCA_DS1E1_STK_UAS_P
BitField Type: W1C
BitField Desc: UAS_P
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_STK_UAS_P_Mask                                   cBit8
#define cAf6_FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_STK_UAS_P_Shift                                       8

/*--------------------------------------
BitField Name: FMPM_TCA_DS1E1_STK_FC_P
BitField Type: W1C
BitField Desc: FC_P
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_STK_FC_P_Mask                                   cBit7
#define cAf6_FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_STK_FC_P_Shift                                       7

/*--------------------------------------
BitField Name: FMPM_TCA_DS1E1_STK_ES_LFE
BitField Type: W1C
BitField Desc: ES_LFE
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_STK_ES_LFE_Mask                                   cBit6
#define cAf6_FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_STK_ES_LFE_Shift                                       6

/*--------------------------------------
BitField Name: FMPM_TCA_DS1E1_STK_SEFS_PFE
BitField Type: W1C
BitField Desc: SEFS_PFE
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_STK_SEFS_PFE_Mask                                   cBit5
#define cAf6_FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_STK_SEFS_PFE_Shift                                       5

/*--------------------------------------
BitField Name: FMPM_TCA_DS1E1_STK_ES_PFE
BitField Type: W1C
BitField Desc: ES_PFE
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_STK_ES_PFE_Mask                                   cBit4
#define cAf6_FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_STK_ES_PFE_Shift                                       4

/*--------------------------------------
BitField Name: FMPM_TCA_DS1E1_STK_SES_PFE
BitField Type: W1C
BitField Desc: SES_PFE
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_STK_SES_PFE_Mask                                   cBit3
#define cAf6_FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_STK_SES_PFE_Shift                                       3

/*--------------------------------------
BitField Name: FMPM_TCA_DS1E1_STK_CSS_PFE
BitField Type: W1C
BitField Desc: CSS_PFE
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_STK_CSS_PFE_Mask                                   cBit2
#define cAf6_FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_STK_CSS_PFE_Shift                                       2

/*--------------------------------------
BitField Name: FMPM_TCA_DS1E1_STK_FC_PFE
BitField Type: W1C
BitField Desc: FC_PFE
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_STK_FC_PFE_Mask                                   cBit1
#define cAf6_FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_STK_FC_PFE_Shift                                       1

/*--------------------------------------
BitField Name: FMPM_TCA_DS1E1_STK_UAS_PFE
BitField Type: W1C
BitField Desc: UAS_PFE
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_STK_UAS_PFE_Mask                                   cBit0
#define cAf6_FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_STK_UAS_PFE_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA DS1E1 Interrupt MASK Per Type Per DS1E1
Reg Addr   : 0x74000-0x77FFF
Reg Formula: 0x74000+$G*0x400+$H*0x20+$I
    Where  :
           + $G(0-7) : DS1/E1 Level-1 Group
           + $H(0-23) : DS1/E1 Level-2 Group
           + $I(0-27) : DS1/E1
Reg Desc   :
FMPM TCA Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_Base                                  0x74000
#define cAf6Reg_FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1(G, H, I)        (0x74000+(G)*0x400+(H)*0x20+(I))

/*--------------------------------------
BitField Name: FMPM_TCA_DS1E1_MSK_CV_L
BitField Type: R/W
BitField Desc: CV_L
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_MSK_CV_L_Mask                                  cBit18
#define cAf6_FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_MSK_CV_L_Shift                                      18

/*--------------------------------------
BitField Name: FMPM_TCA_DS1E1_MSK_ES_L
BitField Type: R/W
BitField Desc: ES_L
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_MSK_ES_L_Mask                                  cBit17
#define cAf6_FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_MSK_ES_L_Shift                                      17

/*--------------------------------------
BitField Name: FMPM_TCA_DS1E1_MSK_SES_L
BitField Type: R/W
BitField Desc: SES_L
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_MSK_SES_L_Mask                                  cBit16
#define cAf6_FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_MSK_SES_L_Shift                                      16

/*--------------------------------------
BitField Name: FMPM_TCA_DS1E1_MSK_LOSS_L
BitField Type: R/W
BitField Desc: LOSS_L
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_MSK_LOSS_L_Mask                                  cBit15
#define cAf6_FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_MSK_LOSS_L_Shift                                      15

/*--------------------------------------
BitField Name: FMPM_TCA_DS1E1_MSK_CV_P
BitField Type: R/W
BitField Desc: CV_P
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_MSK_CV_P_Mask                                  cBit14
#define cAf6_FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_MSK_CV_P_Shift                                      14

/*--------------------------------------
BitField Name: FMPM_TCA_DS1E1_MSK_ES_P
BitField Type: R/W
BitField Desc: ES_P
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_MSK_ES_P_Mask                                  cBit13
#define cAf6_FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_MSK_ES_P_Shift                                      13

/*--------------------------------------
BitField Name: FMPM_TCA_DS1E1_MSK_SES_P
BitField Type: R/W
BitField Desc: SES_P
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_MSK_SES_P_Mask                                  cBit12
#define cAf6_FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_MSK_SES_P_Shift                                      12

/*--------------------------------------
BitField Name: FMPM_TCA_DS1E1_MSK_AISS_P
BitField Type: R/W
BitField Desc: AISS_P
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_MSK_AISS_P_Mask                                  cBit11
#define cAf6_FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_MSK_AISS_P_Shift                                      11

/*--------------------------------------
BitField Name: FMPM_TCA_DS1E1_MSK_SAS_P
BitField Type: R/W
BitField Desc: SAS_P
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_MSK_SAS_P_Mask                                  cBit10
#define cAf6_FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_MSK_SAS_P_Shift                                      10

/*--------------------------------------
BitField Name: FMPM_TCA_DS1E1_MSK_CSS_P
BitField Type: R/W
BitField Desc: CSS_P
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_MSK_CSS_P_Mask                                   cBit9
#define cAf6_FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_MSK_CSS_P_Shift                                       9

/*--------------------------------------
BitField Name: FMPM_TCA_DS1E1_MSK_UAS_P
BitField Type: R/W
BitField Desc: UAS_P
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_MSK_UAS_P_Mask                                   cBit8
#define cAf6_FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_MSK_UAS_P_Shift                                       8

/*--------------------------------------
BitField Name: FMPM_TCA_DS1E1_MSK_FC_P
BitField Type: R/W
BitField Desc: FC_P
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_MSK_FC_P_Mask                                   cBit7
#define cAf6_FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_MSK_FC_P_Shift                                       7

/*--------------------------------------
BitField Name: FMPM_TCA_DS1E1_MSK_ES_LFE
BitField Type: R/W
BitField Desc: ES_LFE
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_MSK_ES_LFE_Mask                                   cBit6
#define cAf6_FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_MSK_ES_LFE_Shift                                       6

/*--------------------------------------
BitField Name: FMPM_TCA_DS1E1_MSK_SEFS_PFE
BitField Type: R/W
BitField Desc: SEFS_PFE
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_MSK_SEFS_PFE_Mask                                   cBit5
#define cAf6_FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_MSK_SEFS_PFE_Shift                                       5

/*--------------------------------------
BitField Name: FMPM_TCA_DS1E1_MSK_ES_PFE
BitField Type: R/W
BitField Desc: ES_PFE
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_MSK_ES_PFE_Mask                                   cBit4
#define cAf6_FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_MSK_ES_PFE_Shift                                       4

/*--------------------------------------
BitField Name: FMPM_TCA_DS1E1_MSK_SES_PFE
BitField Type: R/W
BitField Desc: SES_PFE
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_MSK_SES_PFE_Mask                                   cBit3
#define cAf6_FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_MSK_SES_PFE_Shift                                       3

/*--------------------------------------
BitField Name: FMPM_TCA_DS1E1_MSK_CSS_PFE
BitField Type: R/W
BitField Desc: CSS_PFE
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_MSK_CSS_PFE_Mask                                   cBit2
#define cAf6_FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_MSK_CSS_PFE_Shift                                       2

/*--------------------------------------
BitField Name: FMPM_TCA_DS1E1_MSK_FC_PFE
BitField Type: R/W
BitField Desc: FC_PFE
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_MSK_FC_PFE_Mask                                   cBit1
#define cAf6_FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_MSK_FC_PFE_Shift                                       1

/*--------------------------------------
BitField Name: FMPM_TCA_DS1E1_MSK_UAS_PFE
BitField Type: R/W
BitField Desc: UAS_PFE
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_MSK_UAS_PFE_Mask                                   cBit0
#define cAf6_FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_TCA_DS1E1_MSK_UAS_PFE_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA PW Level1 Group Interrupt OR
Reg Addr   : 0x58004
Reg Formula:
    Where  :
Reg Desc   :
FMPM TCA Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_PW_Level1_Group_Interrupt_OR                                                  0x58004

/*--------------------------------------
BitField Name: FMPM_TCA_PW_LEV1_OR
BitField Type: R_O
BitField Desc: Interrupt PW level1 group OR
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_PW_Level1_Group_Interrupt_OR_FMPM_TCA_PW_LEV1_OR_Mask                                   cBit0
#define cAf6_FMPM_TCA_PW_Level1_Group_Interrupt_OR_FMPM_TCA_PW_LEV1_OR_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA PW Level2 Group Interrupt OR
Reg Addr   : 0x580E0-0x580E7
Reg Formula: 0x580E0+$D
    Where  :
           + $D(0-5) : PW  Level-1 Group
Reg Desc   :
FMPM TCA Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_PW_Level2_Group_Interrupt_OR_Base                                             0x580E0
#define cAf6Reg_FMPM_TCA_PW_Level2_Group_Interrupt_OR(D)                                         (0x580E0+(D))

/*--------------------------------------
BitField Name: FMPM_TCA_PW_LEV2_OR
BitField Type: R_O
BitField Desc: PW Level2 Group Interrupt OR, bit per group, the last Level-1
group has 12 Level-2 group
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_PW_Level2_Group_Interrupt_OR_FMPM_TCA_PW_LEV2_OR_Mask                                cBit31_0
#define cAf6_FMPM_TCA_PW_Level2_Group_Interrupt_OR_FMPM_TCA_PW_LEV2_OR_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA PW Framer Interrupt OR AND MASK Per PW
Reg Addr   : 0x5B400-0x5B4FF
Reg Formula: 0x5B400+$D*0x20+$E
    Where  :
           + $D(0-5) : PW  Level-1 Group
           + $E(0-31) : PW  Level-2 Group
Reg Desc   :
FMPM TCA Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_PW_Framer_Interrupt_OR_AND_MASK_Per_PW_Base                                   0x5B400
#define cAf6Reg_FMPM_TCA_PW_Framer_Interrupt_OR_AND_MASK_Per_PW(D, E)                   (0x5B400+(D)*0x20+(E))

/*--------------------------------------
BitField Name: FMPM_TCA_PW_OAMSK
BitField Type: R_O
BitField Desc: Interrupt OR AND MASK per PW, bit per PW
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_PW_Framer_Interrupt_OR_AND_MASK_Per_PW_FMPM_TCA_PW_OAMSK_Mask                                cBit31_0
#define cAf6_FMPM_TCA_PW_Framer_Interrupt_OR_AND_MASK_Per_PW_FMPM_TCA_PW_OAMSK_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA PW Framer Interrupt MASK Per PW
Reg Addr   : 0x5B500-0x5B5FF
Reg Formula: 0x5B500+$D*0x20+$E
    Where  :
           + $D(0-5) : PW  Level-1 Group
           + $E(0-31) : PW  Level-2 Group
Reg Desc   :
FMPM TCA Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_PW_Framer_Interrupt_MASK_Per_PW_Base                                          0x5B500
#define cAf6Reg_FMPM_TCA_PW_Framer_Interrupt_MASK_Per_PW(D, E)                          (0x5B500+(D)*0x20+(E))

/*--------------------------------------
BitField Name: FMPM_TCA_PW_MSK
BitField Type: R/W
BitField Desc: Interrupt OR AND MASK per PW, bit per PW
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_PW_Framer_Interrupt_MASK_Per_PW_FMPM_TCA_PW_MSK_Mask                                cBit31_0
#define cAf6_FMPM_TCA_PW_Framer_Interrupt_MASK_Per_PW_FMPM_TCA_PW_MSK_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA PW Interrupt Sticky Per Type Per PW
Reg Addr   : 0x5C000-0x5DFFF
Reg Formula: 0x5C000+$D*0x400+$E*0x20+$F
    Where  :
           + $D(0-5) : PW  Level-1 Group
           + $E(0-31) : PW  Level-2 Group
           + $F(0-31) : PW-ID
Reg Desc   :
FMPM TCA Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_PW_Interrupt_Sticky_Per_Type_Per_PW_Base                                      0x5C000
#define cAf6Reg_FMPM_TCA_PW_Interrupt_Sticky_Per_Type_Per_PW(D, E, F)         (0x5C000+(D)*0x400+(E)*0x20+(F))

/*--------------------------------------
BitField Name: FMPM_TCA_PW_STK_ES
BitField Type: W1C
BitField Desc: ES
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_FMPM_TCA_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_TCA_PW_STK_ES_Mask                                   cBit6
#define cAf6_FMPM_TCA_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_TCA_PW_STK_ES_Shift                                       6

/*--------------------------------------
BitField Name: FMPM_TCA_PW_STK_SES
BitField Type: W1C
BitField Desc: SES
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_TCA_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_TCA_PW_STK_SES_Mask                                   cBit5
#define cAf6_FMPM_TCA_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_TCA_PW_STK_SES_Shift                                       5

/*--------------------------------------
BitField Name: FMPM_TCA_PW_STK_UAS
BitField Type: W1C
BitField Desc: UAS
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_TCA_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_TCA_PW_STK_UAS_Mask                                   cBit4
#define cAf6_FMPM_TCA_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_TCA_PW_STK_UAS_Shift                                       4

/*--------------------------------------
BitField Name: FMPM_TCA_PW_STK_ES_FE
BitField Type: W1C
BitField Desc: ES_FE
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_TCA_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_TCA_PW_STK_ES_FE_Mask                                   cBit3
#define cAf6_FMPM_TCA_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_TCA_PW_STK_ES_FE_Shift                                       3

/*--------------------------------------
BitField Name: FMPM_TCA_PW_STK_SES_FE
BitField Type: W1C
BitField Desc: SES_FE
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_TCA_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_TCA_PW_STK_SES_FE_Mask                                   cBit2
#define cAf6_FMPM_TCA_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_TCA_PW_STK_SES_FE_Shift                                       2

/*--------------------------------------
BitField Name: FMPM_TCA_PW_STK_UAS_FE
BitField Type: W1C
BitField Desc: UAS_FE
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_TCA_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_TCA_PW_STK_UAS_FE_Mask                                   cBit1
#define cAf6_FMPM_TCA_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_TCA_PW_STK_UAS_FE_Shift                                       1

/*--------------------------------------
BitField Name: FMPM_TCA_PW_STK_FC
BitField Type: W1C
BitField Desc: FC
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_TCA_PW_STK_FC_Mask                                   cBit0
#define cAf6_FMPM_TCA_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_TCA_PW_STK_FC_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA PW Interrupt MASK Per Type Per PW
Reg Addr   : 0x5E000-0x5FFFF
Reg Formula: 0x5E000+$D*0x400+$E*0x20+$F
    Where  :
           + $D(0-5) : PW  Level-1 Group
           + $E(0-31) : PW  Level-2 Group
           + $F(0-31) : PW-ID
Reg Desc   :
FMPM TCA Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW_Base                                        0x5E000
#define cAf6Reg_FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW(D, E, F)         (0x5(E)000+(D)*0x400+(E)*0x20+(F))

/*--------------------------------------
BitField Name: FMPM_TCA_PW_MSK_ES
BitField Type: R/W
BitField Desc: ES
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_TCA_PW_MSK_ES_Mask                                   cBit6
#define cAf6_FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_TCA_PW_MSK_ES_Shift                                       6

/*--------------------------------------
BitField Name: FMPM_TCA_PW_MSK_SES
BitField Type: R/W
BitField Desc: SES
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_TCA_PW_MSK_SES_Mask                                   cBit5
#define cAf6_FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_TCA_PW_MSK_SES_Shift                                       5

/*--------------------------------------
BitField Name: FMPM_TCA_PW_MSK_UAS
BitField Type: R/W
BitField Desc: UAS
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_TCA_PW_MSK_UAS_Mask                                   cBit4
#define cAf6_FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_TCA_PW_MSK_UAS_Shift                                       4

/*--------------------------------------
BitField Name: FMPM_TCA_PW_MSK_ES_FE
BitField Type: R/W
BitField Desc: ES_FE
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_TCA_PW_MSK_ES_FE_Mask                                   cBit3
#define cAf6_FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_TCA_PW_MSK_ES_FE_Shift                                       3

/*--------------------------------------
BitField Name: FMPM_TCA_PW_MSK_SES_FE
BitField Type: R/W
BitField Desc: SES_FE
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_TCA_PW_MSK_SES_FE_Mask                                   cBit2
#define cAf6_FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_TCA_PW_MSK_SES_FE_Shift                                       2

/*--------------------------------------
BitField Name: FMPM_TCA_PW_MSK_UAS_FE
BitField Type: R/W
BitField Desc: UAS_FE
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_TCA_PW_MSK_UAS_FE_Mask                                   cBit1
#define cAf6_FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_TCA_PW_MSK_UAS_FE_Shift                                       1

/*--------------------------------------
BitField Name: FMPM_TCA_PW_MSK_FC
BitField Type: R/W
BitField Desc: FC
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_TCA_PW_MSK_FC_Mask                                   cBit0
#define cAf6_FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_TCA_PW_MSK_FC_Shift                                       0


#ifdef __cplusplus
}
#endif

#endif /* _THMODULEHARDSURREG_H_ */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : XC
 * 
 * File        : ThaVcCrossConnectInternal.h
 * 
 * Created Date: Nov 24, 2016
 *
 * Description : Default VC cross connect.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAVCCROSSCONNECTINTERNAL_H_
#define _THAVCCROSSCONNECTINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/xc/AtCrossConnectInternal.h"
#include "ThaVcCrossConnect.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaVcCrossConnectMethods
    {
    eBool (*ShouldSortHwStsBeforeConnecting)(ThaVcCrossConnect self);
    uint32 (*NumHwSourceSts)(ThaVcCrossConnect self);

    /* Registers */
    uint32 (*SliceIdPageMask)(ThaVcCrossConnect self, uint32 pageId);
    uint8 (*SliceIdPageShift)(ThaVcCrossConnect self, uint32 pageId);
    }tThaVcCrossConnectMethods;

typedef struct tThaVcCrossConnect
    {
    tAtCrossConnect super;
    const tThaVcCrossConnectMethods *methods;
    }tThaVcCrossConnect;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtCrossConnect ThaVcCrossConnectObjectInit(AtCrossConnect self, AtModuleXc module);

#ifdef __cplusplus
}
#endif
#endif /* _THAVCCROSSCONNECTINTERNAL_H_ */


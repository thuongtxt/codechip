/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : XC
 *
 * File        : ThaVcCrossConnect.c
 *
 * Created Date: Nov 24, 2016
 *
 * Description : Default VC cross-connect with multiple incoming sources.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtApsGroup.h"
#include "AtApsSelector.h"
#include "../../../generic/sdh/AtSdhChannelInternal.h"
#include "../../../generic/sdh/AtSdhPathInternal.h"
#include "../../../generic/sdh/AtSdhVcInternal.h"
#include "../../../generic/aps/AtApsSelectorInternal.h"
#include "../ocn/ThaModuleOcn.h"
#include "../sdh/ThaModuleSdh.h"
#include "../sdh/ThaSdhVc.h"
#include "ThaVcCrossConnectInternal.h"
#include "ThaModuleXcReg.h"
#include "ThaModuleXcInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cMaxNumStsPerPath  196

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaVcCrossConnect)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaVcCrossConnectMethods m_methods;

/* Override */
static tAtCrossConnectMethods    m_AtCrossConnectOverride;

/* To save super implementation */
static const tAtCrossConnectMethods *m_AtCrossConnectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool IsTerminatedMapping(AtSdhChannel vc)
    {
    switch (AtSdhChannelMapTypeGet(vc))
        {
        case cAtSdhVcMapTypeVc4Map3xTug3s: return cAtTrue;
        case cAtSdhVcMapTypeVc3Map7xTug2s: return cAtTrue;
        case cAtSdhVcMapTypeVc3MapDe3    : return cAtTrue;
        default:
            return cAtFalse;
        }
    }

static eBool IsLineSideVc(AtSdhChannel vc)
    {
    /* Line-side AU VC always contains STS Pointer processor. */
    return AtSdhPathHasPointerProcessor((AtSdhPath)AtSdhChannelParentChannelGet(vc));
    }

static eBool IsTerminatedSideVc(AtSdhChannel vc)
    {
    return IsLineSideVc(vc) ? cAtFalse : cAtTrue;
    }

static eBool SourceVcShouldEnablePohInsertion(AtSdhChannel sourceVc)
    {
    if (IsTerminatedMapping(sourceVc))
        return cAtTrue;

    if (ThaSdhVcCachedPrbsEngine((ThaSdhVc)sourceVc))
        return cAtTrue;

    return AtChannelBoundPwGet((AtChannel)sourceVc) ? cAtFalse : cAtTrue;
    }

static eBool DestVcShouldEnablePohInsertion(AtSdhChannel destVc)
    {
    uint32 sourceIndex;
    eBool pohEnabled = cAtTrue; /* If no any source, should restore POH insertion */

    for (sourceIndex = 0; sourceIndex < ThaSdhVcNumSourceVcs((ThaSdhVc)destVc); sourceIndex++)
        {
        AtSdhChannel sourceVc = (AtSdhChannel)ThaSdhVcSourceVcGet((ThaSdhVc)destVc, sourceIndex);
        if (sourceVc == NULL)
            continue;

        pohEnabled = cAtFalse;

        if (IsTerminatedSideVc(sourceVc) && SourceVcShouldEnablePohInsertion(sourceVc))
            return cAtTrue;
        }

    return pohEnabled;
    }

static ThaModuleXc XcModule(ThaVcCrossConnect self)
    {
    return (ThaModuleXc)AtCrossConnectModuleGet((AtCrossConnect)self);
    }

static uint32 SxcStsIdMask(ThaVcCrossConnect self, uint32 sourceIndex)
    {
    AtUnused(self);
    if (sourceIndex == 0)   return cAf6_sxcramctl0_SxcStsIdPage0_Mask;
    if (sourceIndex == 1)   return cAf6_sxcramctl0_SxcStsIdPage1_Mask;
    if (sourceIndex == 2)   return cAf6_sxcramctl1_SxcStsIdPage2_Mask;
    return 0;
    }

static uint32 SxcStsIdShift(ThaVcCrossConnect self, uint32 sourceIndex)
    {
    AtUnused(self);
    if (sourceIndex == 0)   return cAf6_sxcramctl0_SxcStsIdPage0_Shift;
    if (sourceIndex == 1)   return cAf6_sxcramctl0_SxcStsIdPage1_Shift;
    if (sourceIndex == 2)   return cAf6_sxcramctl1_SxcStsIdPage2_Shift;
    return 0;
    }

static uint32 SxcStsOffset(ThaVcCrossConnect self, uint8 destSlice, uint8 destSts)
    {
    AtUnused(self);
    return (256UL * destSlice) + destSts;
    }

static uint32 SxcControlAddress(ThaVcCrossConnect self, uint32 sourceIndex)
    {
    AtUnused(self);
    if (sourceIndex < 2)    return cAf6Reg_sxcramctl0_Base;
    if (sourceIndex == 2)   return cAf6Reg_sxcramctl1_Base;
    return 0;
    }

static eAtRet HwStsConnect(ThaVcCrossConnect self, uint8 destSlice, uint8 destSts, uint32 sourceIndex, uint8 sourceSlice, uint8 sourceSts)
    {
    ThaModuleXc moduleXc = XcModule(self);
    uint32 baseAddress = ThaModuleXcBaseAddress(moduleXc);
    uint32 regAddr = baseAddress + SxcControlAddress(self, sourceIndex) + SxcStsOffset(self, destSlice, destSts);
    uint32 regVal  = mModuleHwRead(moduleXc, regAddr);
    mFieldIns(&regVal,
              mMethodsGet(self)->SliceIdPageMask(self, sourceIndex),
              mMethodsGet(self)->SliceIdPageShift(self, sourceIndex),
              sourceSlice);
    mFieldIns(&regVal,
              SxcStsIdMask(self, sourceIndex),
              SxcStsIdShift(self, sourceIndex),
              sourceSts);
    mModuleHwWrite(moduleXc, regAddr, regVal);

    return cAtOk;
    }

static eAtRet HwStsDisconnect(ThaVcCrossConnect self, uint8 destSlice, uint8 destSts, uint32 sourceIndex)
    {
    ThaModuleXc moduleXc = XcModule(self);
    uint32 baseAddress = ThaModuleXcBaseAddress(moduleXc);
    uint32 regAddr = baseAddress + SxcControlAddress(self, sourceIndex) + SxcStsOffset(self, destSlice, destSts);
    uint32 regVal  = mModuleHwRead(moduleXc, regAddr);
    uint32 disconnectSliceId = ThaModuleXcDisconnectSliceId(moduleXc);

    mFieldIns(&regVal,
              mMethodsGet(self)->SliceIdPageMask(self, sourceIndex),
              mMethodsGet(self)->SliceIdPageShift(self, sourceIndex),
              disconnectSliceId);
    mFieldIns(&regVal,
              SxcStsIdMask(self, sourceIndex),
              SxcStsIdShift(self, sourceIndex),
              0x3F);
    mModuleHwWrite(moduleXc, regAddr, regVal);

    return cAtOk;
    }

static eAtRet HwSourceStsGet(ThaVcCrossConnect self, uint8 destSlice, uint8 destSts, uint32 sourceIndex, uint8 *sourceSlice, uint8 *sourceSts)
    {
    ThaModuleXc moduleXc = XcModule(self);
    uint32 baseAddress = ThaModuleXcBaseAddress(moduleXc);
    uint32 regAddr = baseAddress + SxcControlAddress(self, sourceIndex) + SxcStsOffset(self, destSlice, destSts);
    uint32 regVal  = mModuleHwRead(moduleXc, regAddr);
    mFieldGet(regVal,
              mMethodsGet(self)->SliceIdPageMask(self, sourceIndex),
              mMethodsGet(self)->SliceIdPageShift(self, sourceIndex),
              uint8,
              sourceSlice);

    mFieldGet(regVal,
              SxcStsIdMask(self, sourceIndex),
              SxcStsIdShift(self, sourceIndex),
              uint8,
              sourceSts);
    return cAtOk;
    }

static uint32 NumHwSourceSts(ThaVcCrossConnect self)
    {
    AtUnused(self);
    /* Default XC only supports single source per destination. */
    return 1;
    }

static uint32 DefaultIndex(ThaVcCrossConnect self)
    {
    AtUnused(self);
    return cThaXcVcConnectionWorking;
    }

static ThaModuleOcn OcnModule(AtSdhChannel vc)
    {
    return (ThaModuleOcn)ThaModuleOcnFromChannel((AtChannel)vc);
    }

static eAtRet AllHwStsGet(ThaVcCrossConnect self, AtSdhChannel vc, uint8 *allHwSlice, uint8 *allHwSts)
    {
    uint8 numSts = AtSdhChannelNumSts(vc);
    uint8 sts1Index = 0;
    eAtRet ret = cAtOk;
    uint8 swSts = AtSdhChannelSts1Get(vc);
    uint8 numStsPerSlice = ThaModuleOcnNumStsInOneSlice(OcnModule(vc));
    uint8 masterSlice;

    /* Having master IDs */
    ret |= ThaVcCrossConnectHwStsGet(self, vc, swSts, &masterSlice, &(allHwSts[sts1Index]));
    if (ret != cAtOk)
        return ret;

    allHwSlice[sts1Index] = masterSlice;

    /* And also having slave IDs */
    for (sts1Index = 1; sts1Index < numSts; sts1Index++)
        {
        ret |= ThaVcCrossConnectHwStsGet(self, vc, (uint8)(swSts + sts1Index), &(allHwSlice[sts1Index]), &(allHwSts[sts1Index]));
        if (ret != cAtOk)
            return ret;

        if ((numSts <= numStsPerSlice) && (allHwSlice[sts1Index] != masterSlice))
            AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelWarning, AtSourceLocation, "Slave and master slice is different\r\n");
        }

    return cAtOk;
    }

static eAtRet AllSortedHwStsGet(ThaVcCrossConnect self, AtSdhChannel vc, uint8 *allHwSlice, uint8 *allHwSts)
    {
    eAtRet ret;
    uint16 allHwIds[cMaxNumStsPerPath];
    uint8 numSts = AtSdhChannelNumSts(vc);
    uint8 sts1Index;

    ret = AllHwStsGet(self, vc, allHwSlice, allHwSts);
    if (ret != cAtOk)
        return ret;

    /* Concatenate Slice/STS ID before sorting */
    for (sts1Index = 0; sts1Index < numSts; sts1Index++)
        allHwIds[sts1Index] = (uint16)(((uint16)allHwSlice[sts1Index] << 8) | allHwSts[sts1Index]);

    AtStdSort(AtStdSharedStdGet(), &(allHwIds[0]), numSts, sizeof(uint16), AtStdUint16CompareFunction);

    /* De-concatenate Slice/STS ID after sorting */
    for (sts1Index = 0; sts1Index < numSts; sts1Index++)
        {
        allHwSts[sts1Index]   = (uint8)(allHwIds[sts1Index] & cBit7_0);
        allHwSlice[sts1Index] = (uint8)((allHwIds[sts1Index] >> 8) & cBit7_0);
        }

    return cAtOk;
    }

static eBool ShouldSortHwStsBeforeConnecting(ThaVcCrossConnect self)
    {
    /* Some products require this to be sorted before making cross-connect */
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet VcConnect(ThaVcCrossConnect self, AtSdhChannel destVc, uint32 sourceIndex, AtSdhChannel sourceVc)
    {
    uint8 sts1Index;
    uint8 numSts = AtSdhChannelNumSts(destVc);
    eAtRet ret = cAtOk;
    uint8 sourceAllHwSts[cMaxNumStsPerPath], destAllHwSts[cMaxNumStsPerPath];
    uint8 sourceAllHwSlice[cMaxNumStsPerPath], destAllHwSlice[cMaxNumStsPerPath];
    eAtRet (*AllHwStsGetFunc)(ThaVcCrossConnect, AtSdhChannel, uint8 *, uint8 *) = AllHwStsGet;

    if (mMethodsGet(self)->ShouldSortHwStsBeforeConnecting(self))
        AllHwStsGetFunc = AllSortedHwStsGet;

    /* Without sorting, after cross-connect, the remote end will declare B3 */
    ret |= AllHwStsGetFunc(self, destVc, destAllHwSlice, destAllHwSts);
    ret |= AllHwStsGetFunc(self, sourceVc, sourceAllHwSlice, sourceAllHwSts);
    if (ret != cAtOk)
        return ret;

    for (sts1Index = 0; sts1Index < numSts; sts1Index++)
        {
        ret |= ThaVcCrossConnectHwStsConnect(mThis(self),
                                             destAllHwSlice[sts1Index], destAllHwSts[sts1Index],
                                             sourceIndex,
                                             sourceAllHwSlice[sts1Index], sourceAllHwSts[sts1Index]);
        if (ret != cAtOk)
            return ret;
        }

    return ret;
    }

static eAtRet VcConnectAtIndex(ThaVcCrossConnect self, AtSdhChannel destVc, AtSdhChannel sourceVc, uint32 sourceIndex)
            {
    eAtRet ret;

    ret = VcConnect(self, destVc, sourceIndex, sourceVc);
            if (ret != cAtOk)
                return ret;

    /* For pass-through connection, do not need to deal with internal configuration. */
    if (sourceIndex >= ThaSdhVcNumSourceVcs((ThaSdhVc)destVc))
        return ret;

    ThaSdhVcSourceVcSet((ThaSdhVc)destVc, sourceIndex, (ThaSdhVc)sourceVc);
    ret |= ThaSdhVcDestVcAdd((ThaSdhVc)sourceVc, (ThaSdhVc)destVc);

    if (IsLineSideVc(destVc))
        {
        eBool enabled;

        /* POH insertion on destination VC.  */
        enabled = DestVcShouldEnablePohInsertion(destVc);
        ret |= AtSdhPathPohInsertionEnable((AtSdhPath)destVc, enabled);

        /* In some projects, we need to enable POH insertion at source VC. */
        if (IsTerminatedSideVc(sourceVc))
            {
            enabled = SourceVcShouldEnablePohInsertion(sourceVc);
            ret |= AtSdhPathPohInsertionEnable((AtSdhPath)sourceVc, enabled);
            }
        }

    if (IsLineSideVc(sourceVc))
        {
        /* LOM detection must be enabled on source VC when destination VC is channelized to TU1x */
        ret |= ThaSdhAuVcLomMonitorEnable((ThaSdhAuVc)sourceVc, AtSdhVcTu1xChannelized((AtSdhVc)destVc));

        /* POH alarm downstream needs to be disabled on source VC when destination VC is bound to PW */
        if (AtSdhPathHasPohProcessor((AtSdhPath)sourceVc) && AtChannelBoundPwGet((AtChannel)destVc))
            ret |= AtSdhChannelAlarmAffectingDisable(sourceVc);
        }

    return ret;
    }

static eAtRet VcDisconnect(ThaVcCrossConnect self, AtSdhChannel destVc, uint32 sourceIndex)
    {
    eAtRet ret = cAtOk;
    uint32 sts1;
    uint32 numSts = AtSdhChannelNumSts(destVc);
    uint32 destSwSts = AtSdhChannelSts1Get(destVc);

    for (sts1 = 0; sts1 < numSts; sts1++)
        {
        uint8 destHwSlice = 0, destHwSts = 0;
        uint8 destSts1 = (uint8)(destSwSts + sts1);

        ret = ThaVcCrossConnectHwStsGet(mThis(self), destVc, destSts1, &destHwSlice,  &destHwSts);
        if (ret != cAtOk)
            return ret;

        ret = ThaVcCrossConnectHwStsDisconnect(mThis(self), destHwSlice,  destHwSts, sourceIndex);
        if (ret != cAtOk)
            return ret;
        }

    return ret;
    }

static void TxSquelRemove(AtChannel destVc)
    {
    if (AtChannelTxIsSquelched(destVc))
        AtChannelTxSquelch(destVc, cAtFalse);
    }

static eAtRet VcDisconnectAtIndex(ThaVcCrossConnect self, AtSdhChannel destVc, uint32 sourceIndex)
            {
    eAtRet ret;
    ThaSdhVc sourceVc = ThaSdhVcSourceVcGet((ThaSdhVc)destVc, sourceIndex);

    ret = VcDisconnect(self, destVc, sourceIndex);
            if (ret != cAtOk)
                return ret;

    /* For pass-through connection, do not need to deal with internal configuration. */
    if (sourceIndex >= ThaSdhVcNumSourceVcs((ThaSdhVc)destVc) || (sourceVc == NULL))
        return ret;

    if (sourceVc && IsTerminatedSideVc((AtSdhChannel)sourceVc) && IsLineSideVc(destVc))
        TxSquelRemove((AtChannel)destVc);

    ret |= ThaSdhVcDestVcRemove(sourceVc, (ThaSdhVc)destVc);
    ThaSdhVcSourceVcSet((ThaSdhVc)destVc, sourceIndex, NULL);

    /* Restore POH insertion on destination VC */
    if (IsLineSideVc(destVc))
        ret |= AtSdhPathPohInsertionEnable((AtSdhPath)destVc, DestVcShouldEnablePohInsertion(destVc));

    if (IsTerminatedSideVc(destVc))
        {
        /* Disable LOM detection */
        if (IsLineSideVc((AtSdhChannel)sourceVc))
            ret |= ThaSdhAuVcLomMonitorEnable((ThaSdhAuVc)sourceVc, cAtFalse);

        /* POH alarm downstream should be restored */
        if (AtSdhPathHasPohProcessor((AtSdhPath)sourceVc) && AtChannelBoundPwGet((AtChannel)destVc))
            ret |= AtSdhChannelAlarmAffectingRestore((AtSdhChannel)sourceVc);
        }

    return ret;
    }

static eBool VcIsConnectedAtIndex(ThaVcCrossConnect self, AtSdhChannel destVc, AtSdhChannel sourceVc, uint32 sourceIndex)
    {
    uint32 numSts = AtSdhChannelNumSts(destVc);
    uint32 sourceSwSts = cInvalidUint32;
    uint32 destSwSts = AtSdhChannelSts1Get(destVc);
    uint32 disconnectSliceId = ThaModuleXcDisconnectSliceId(XcModule(self));
    uint32 sts1;

    if (sourceVc)
        sourceSwSts = AtSdhChannelSts1Get(sourceVc);

    for (sts1 = 0; sts1 < numSts; sts1++)
        {
        eAtRet ret = cAtOk;
        uint8 sourceHwSlice = 0, sourceHwSts = 0;
        uint8 destHwSlice = 0, destHwSts = 0;
        uint8 sourceSts1 = (uint8)(sourceSwSts + sts1);
        uint8 destSts1   = (uint8)(destSwSts + sts1);
        uint8 actualSourceHwSlice, actualSourceHwSts;

        if (sourceVc)
            ret |= ThaVcCrossConnectHwStsGet(mThis(self), sourceVc, sourceSts1, &sourceHwSlice, &sourceHwSts);
        ret |= ThaVcCrossConnectHwStsGet(mThis(self), destVc, destSts1, &destHwSlice,  &destHwSts);
        if (ret != cAtOk)
            return cAtFalse;

        ret = ThaVcCrossConnectHwSourceStsGet(mThis(self), destHwSlice, destHwSts, sourceIndex, &actualSourceHwSlice, &actualSourceHwSts);
        if (ret != cAtOk)
            return cAtFalse;

        /* The caller wants to check if source is connected to destination */
        if (sourceVc && ((actualSourceHwSlice != sourceHwSlice) || (actualSourceHwSts != sourceHwSts)))
            return cAtFalse;

        /* The caller just wants to check if there is any connection to the destination */
        if ((sourceVc == NULL) && (actualSourceHwSlice == disconnectSliceId))
            return cAtFalse;
        }

    return cAtTrue;
    }

static eAtRet HwStsSelectorSet(ThaVcCrossConnect self, uint8 destSlice, uint8 destSts, uint32 selectorType, uint32 selectorID)
    {
    ThaModuleXc moduleXc = XcModule(self);
    uint32 baseAddress = ThaModuleXcBaseAddress(moduleXc);
    uint32 regAddr = baseAddress + cAf6Reg_apsramctl_Base + SxcStsOffset(self, destSlice, destSts);
    uint32 regVal  = mModuleHwRead(moduleXc, regAddr);

    switch (selectorType)
        {
        case cThaXcVcSelectorDisable:
        case cThaXcVcSelectorDeviceRole:
            mRegFieldSet(regVal, cAf6_apsramctl_SelectorType_, selectorType);
            mRegFieldSet(regVal, cAf6_apsramctl_SelectorID_,   0);
            break;

        case cThaXcVcSelectorUpsr:
            mRegFieldSet(regVal, cAf6_apsramctl_SelectorType_, selectorType);
            mRegFieldSet(regVal, cAf6_apsramctl_SelectorID_,   selectorID);
            break;

        case cThaXcVcSelectorApsGroup:
            mRegFieldSet(regVal, cAf6_apsramctl_SelectorType_, selectorType);
            mRegFieldSet(regVal, cAf6_apsramctl_SelectorID_ApsID_, selectorID);
            break;

        case cThaXcVcSelectorPassthrough:
            mRegFieldSet(regVal, cAf6_apsramctl_SelectorType_, cThaXcVcSelectorApsGroup);
            mRegFieldSet(regVal, cAf6_apsramctl_SelectorID_PassthrID_, selectorID);
            mRegFieldSet(regVal, cAf6_apsramctl_SelectorID_PassthrEn_, 1);
            break;

        default:
            return cAtErrorInvlParm;
        }
    mModuleHwWrite(moduleXc, regAddr, regVal);

    return cAtOk;
    }

static uint32 HwStsSelectorTypeGet(ThaVcCrossConnect self, uint8 destSlice, uint8 destSts)
    {
    ThaModuleXc moduleXc = XcModule(self);
    uint32 baseAddress = ThaModuleXcBaseAddress(moduleXc);
    uint32 regAddr = baseAddress + cAf6Reg_apsramctl_Base + SxcStsOffset(self, destSlice, destSts);
    uint32 regVal  = mModuleHwRead(moduleXc, regAddr);
    return (uint32)mRegField(regVal, cAf6_apsramctl_SelectorType_);
    }

static eAtRet VcSelectorSet(ThaVcCrossConnect self, AtSdhChannel destVc, uint32 selectorType, uint32 selectorId)
    {
    eAtRet ret;
    uint32 sts1;
    uint32 numSts = AtSdhChannelNumSts(destVc);
    uint32 destSwSts = AtSdhChannelSts1Get(destVc);

    for (sts1 = 0; sts1 < numSts; sts1++)
        {
        uint8 destHwSlice = 0, destHwSts = 0;
        uint8 destSts1 = (uint8)(destSwSts + sts1);

        ret = ThaVcCrossConnectHwStsGet(mThis(self), destVc, destSts1, &destHwSlice,  &destHwSts);
        if (ret != cAtOk)
            return ret;

        ret = HwStsSelectorSet(mThis(self), destHwSlice, destHwSts, selectorType, selectorId);
        if (ret != cAtOk)
            return ret;
        }

    return cAtOk;
    }

static uint32 VcSelectorTypeGet(ThaVcCrossConnect self, AtSdhChannel destVc)
    {
    eAtRet ret;
    uint32 sts1 = AtSdhChannelSts1Get(destVc);
    uint8 destHwSlice = 0, destHwSts = 0;

    ret = ThaVcCrossConnectHwStsGet(mThis(self), destVc, (uint8)sts1, &destHwSlice,  &destHwSts);
    if (ret != cAtOk)
        return cThaXcVcSelectorUnknown;

    return HwStsSelectorTypeGet(mThis(self), destHwSlice, destHwSts);
    }

static uint32 DeviceRole2SelectorState(ThaVcCrossConnect self)
    {
    eAtDeviceRole role = AtModuleRoleGet((AtModule)XcModule(self));
    if (role == cAtDeviceRoleActive)    return cThaXcVcSelectorStateActive;
    if (role == cAtDeviceRoleStandby)   return cThaXcVcSelectorStateStandby;
    return cThaXcVcSelectorStateUnknown;
    }

static uint32 ApsGroup2SelectorState(ThaVcCrossConnect self, AtSdhChannel destVc)
    {
    AtApsSelector selector = AtSdhPathApsSelectorGet((AtSdhPath)destVc);
    AtApsGroup group;
    AtUnused(self);

    group = AtApsSelectorGroupGet(selector);
    if (group == NULL)
        return cThaXcVcSelectorStateUnknown;

    if (AtApsGroupStateGet(group) == cAtApsGroupStateProtection)
        return cThaXcVcSelectorStateStandby;

    return cThaXcVcSelectorStateActive;
    }

static uint32 VcSelectorStateGet(ThaVcCrossConnect self, AtSdhChannel destVc)
    {
    uint32 selectorType = ThaVcCrossConnectVcSelectorTypeGet(self, destVc);

    if (selectorType == cThaXcVcSelectorDeviceRole)
        return DeviceRole2SelectorState(self);

    if (selectorType == cThaXcVcSelectorApsGroup)
        return ApsGroup2SelectorState(self, destVc);

    /* TODO: get further information for other selector types, e.g. UPSR. */
    return cThaXcVcSelectorStateUnknown;
    }

static uint32 SliceIdPageMask(ThaVcCrossConnect self, uint32 pageId)
    {
    AtUnused(self);

    if (pageId == 0) return cAf6_sxcramctl0_SxcLineIdPage0_Mask;
    if (pageId == 1) return cAf6_sxcramctl0_SxcLineIdPage1_Mask;
    if (pageId == 2) return cAf6_sxcramctl1_SxcLineIdPage2_Mask;

    return 0;
    }

static uint8 SliceIdPageShift(ThaVcCrossConnect self, uint32 pageId)
    {
    AtUnused(self);

    if (pageId == 0) return cAf6_sxcramctl0_SxcLineIdPage0_Shift;
    if (pageId == 1) return cAf6_sxcramctl0_SxcLineIdPage1_Shift;
    if (pageId == 2) return cAf6_sxcramctl1_SxcLineIdPage2_Shift;

    return 0;
    }

static eBool HwIsConnected(AtCrossConnect self, AtChannel sourceVc, AtChannel destVc)
    {
    return VcIsConnectedAtIndex(mThis(self), (AtSdhChannel)destVc, (AtSdhChannel)sourceVc, DefaultIndex(mThis(self)));
    }

static eAtRet HwConnect(AtCrossConnect self, AtChannel sourceVc, AtChannel destVc)
    {
    return ThaVcCrossConnectVcConnectAtIndex(mThis(self), (AtSdhChannel)destVc, (AtSdhChannel)sourceVc, DefaultIndex(mThis(self)));
    }

static eAtRet HwDisconnect(AtCrossConnect self, AtChannel destVc)
    {
    return ThaVcCrossConnectVcDisconnectAtIndex(mThis(self), (AtSdhChannel)destVc, DefaultIndex(mThis(self)));
    }

static eBool IsHoVc(AtChannel channel)
    {
    return AtSdhChannelIsHoVc((AtSdhChannel)channel);
    }

static eAtRet ConnectCheck(AtCrossConnect self, AtChannel source, AtChannel dest)
    {
    eAtRet ret = m_AtCrossConnectMethods->ConnectCheck(self, source, dest);
    if (ret != cAtOk)
        return ret;

    /* Both of them must be HO VCs */
    if (!IsHoVc(source) || !IsHoVc(dest))
        return cAtModuleXcErrorNotSupportedChannelType;

    /* They must be the same STS capacity */
    if (AtSdhChannelNumSts((AtSdhChannel)source) != AtSdhChannelNumSts((AtSdhChannel)dest))
        return cAtModuleXcErrorDifferentChannelType;

    return cAtOk;
    }

static eBool CanHaveHoVcMapping(AtSdhChannel channel)
    {
    uint32 channelType = AtSdhChannelTypeGet(channel);

    switch (channelType)
        {
        case cAtSdhChannelTypeLine   : return cAtTrue;

        /* AUG */
        case cAtSdhChannelTypeAug16  : return cAtTrue;
        case cAtSdhChannelTypeAug4   : return cAtTrue;
        case cAtSdhChannelTypeAug1   : return cAtTrue;

        /* AU */
        case cAtSdhChannelTypeAu4_16c: return cAtTrue;
        case cAtSdhChannelTypeAu4_4c : return cAtTrue;
        case cAtSdhChannelTypeAu4    : return cAtTrue;
        case cAtSdhChannelTypeAu3    : return cAtTrue;

        default:
            return cAtFalse;
        }
    }

static eAtRet AllVcsDisconnect(AtCrossConnect self, AtSdhChannel channel)
    {
    uint8 numSubChannels;
    uint8 subChannel_i;
    eAtRet ret = cAtOk;

    if (AtSdhChannelIsHoVc(channel))
        return AtCrossConnectVcDisconnectAllConnections(self, channel);

    if (!CanHaveHoVcMapping(channel))
        return cAtOk;

    /* Recursively delete all HoVc connections */
    numSubChannels = AtSdhChannelNumberOfSubChannelsGet(channel);
    for (subChannel_i = 0; subChannel_i < numSubChannels; subChannel_i++)
        {
        AtSdhChannel subChannel = AtSdhChannelSubChannelGet(channel, subChannel_i);
        ret |= AllVcsDisconnect(self, subChannel);
        }

    return ret;
    }

static eAtRet AllConnectionsDelete(AtCrossConnect self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)AtCrossConnectModuleGet(self));
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    uint8 numLines = AtModuleSdhMaxLinesGet(sdhModule);
    uint8 line_i;
    eAtRet ret = cAtOk;

    for (line_i = 0; line_i < numLines; line_i++)
        {
        AtSdhLine line = AtModuleSdhLineGet(sdhModule, line_i);
        ret |= AllVcsDisconnect(self, (AtSdhChannel)line);
        }

    return ret;
    }

static eBool IsBlockingCrossConnect(AtCrossConnect self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static const char *TypeString(AtCrossConnect self)
    {
    AtUnused(self);
    return "hovc_xc";
    }

static eAtModuleXcRet ChannelDisconnect(AtCrossConnect self, AtChannel source, AtChannel dest)
    {
    AtApsEngine engine = AtSdhPathApsEngineGet((AtSdhPath)source);

    /* If source VC is belonged to an active APS engine, does not allow to
     * disconnect them. */
    if ((engine != NULL) && AtChannelIsEnabled((AtChannel)engine))
        return cAtErrorChannelBusy;

    return m_AtCrossConnectMethods->ChannelDisconnect(self, source, dest);
    }

static void OverrideAtCrossConnect(AtCrossConnect self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtCrossConnectMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtCrossConnectOverride, mMethodsGet(self), sizeof(m_AtCrossConnectOverride));

        mMethodOverride(m_AtCrossConnectOverride, HwIsConnected);
        mMethodOverride(m_AtCrossConnectOverride, HwConnect);
        mMethodOverride(m_AtCrossConnectOverride, HwDisconnect);
        mMethodOverride(m_AtCrossConnectOverride, ConnectCheck);
        mMethodOverride(m_AtCrossConnectOverride, AllConnectionsDelete);
        mMethodOverride(m_AtCrossConnectOverride, IsBlockingCrossConnect);
        mMethodOverride(m_AtCrossConnectOverride, TypeString);
        mMethodOverride(m_AtCrossConnectOverride, ChannelDisconnect);
        }

    mMethodsSet(self, &m_AtCrossConnectOverride);
    }

static void Override(AtCrossConnect self)
    {
    OverrideAtCrossConnect(self);
    }

static void MethodsInit(AtCrossConnect self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, NumHwSourceSts);
        mMethodOverride(m_methods, ShouldSortHwStsBeforeConnecting);
        mMethodOverride(m_methods, SliceIdPageMask);
        mMethodOverride(m_methods, SliceIdPageShift);
        }

    mMethodsSet((ThaVcCrossConnect)self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaVcCrossConnect);
    }

AtCrossConnect ThaVcCrossConnectObjectInit(AtCrossConnect self, AtModuleXc module)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtCrossConnectObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

eAtRet ThaVcCrossConnectHwStsGet(ThaVcCrossConnect self, AtSdhChannel sdhVc, uint8 sts1Id, uint8 *hwSlice, uint8 *hwSts)
    {
    AtUnused(self);
    return ThaSdhChannelHwStsGet(sdhVc, cAtModuleXc, sts1Id, hwSlice, hwSts);
    }

uint32 ThaVcCrossConnectNumHwSourceSts(ThaVcCrossConnect self)
    {
    if (self)
        return mMethodsGet(self)->NumHwSourceSts(self);
    return 0;
    }

eAtRet ThaVcCrossConnectHwStsConnect(ThaVcCrossConnect self, uint8 destSlice, uint8 destSts, uint32 sourceIndex, uint8 sourceSlice, uint8 sourceSts)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (ThaVcCrossConnectNumHwSourceSts(self) <= sourceIndex)
        return cAtErrorOutOfRangParm;

    return HwStsConnect(self, destSlice, destSts, sourceIndex, sourceSlice, sourceSts);
    }

eAtRet ThaVcCrossConnectHwStsDisconnect(ThaVcCrossConnect self, uint8 destSlice, uint8 destSts, uint32 sourceIndex)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (ThaVcCrossConnectNumHwSourceSts(self) <= sourceIndex)
        return cAtErrorOutOfRangParm;

    return HwStsDisconnect(self, destSlice, destSts, sourceIndex);
    }

eAtRet ThaVcCrossConnectHwSourceStsGet(ThaVcCrossConnect self, uint8 destSlice, uint8 destSts, uint32 sourceIndex, uint8 *sourceSlice, uint8 *sourceSts)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (ThaVcCrossConnectNumHwSourceSts(self) <= sourceIndex)
        return cAtErrorOutOfRangParm;

    return HwSourceStsGet(self, destSlice, destSts, sourceIndex, sourceSlice, sourceSts);
    }

eAtRet ThaVcCrossConnectVcConnectAtIndex(ThaVcCrossConnect self, AtSdhChannel destVc, AtSdhChannel sourceVc, uint32 sourceIndex)
    {
    if (self)
        return VcConnectAtIndex(self, destVc, sourceVc, sourceIndex);
    return cAtErrorNullPointer;
    }

eAtRet ThaVcCrossConnectVcDisconnectAtIndex(ThaVcCrossConnect self, AtSdhChannel destVc, uint32 sourceIndex)
    {
    if (self)
        return VcDisconnectAtIndex(self, destVc, sourceIndex);
    return cAtErrorNullPointer;
    }

eAtRet ThaVcCrossConnectVcSelectorSet(ThaVcCrossConnect self, AtSdhChannel destVc, uint32 selectorType, uint32 selectorId)
    {
    if (self)
        return VcSelectorSet(self, destVc, selectorType, selectorId);
    return cAtErrorNullPointer;
    }

uint32 ThaVcCrossConnectVcSelectorTypeGet(ThaVcCrossConnect self, AtSdhChannel destVc)
    {
    if (self)
        return VcSelectorTypeGet(self, destVc);
    return cThaXcVcSelectorDisable;
    }

uint32 ThaVcCrossConnectVcSelectorStateGet(ThaVcCrossConnect self, AtSdhChannel destVc)
    {
    if (self)
        return VcSelectorStateGet(self, destVc);
    return cThaXcVcSelectorStateUnknown;
    }

uint32 ThaVcCrossConnectSliceIdPageMask(ThaVcCrossConnect self, uint32 pageId)
    {
    if (self)
        return mMethodsGet(self)->SliceIdPageMask(self, pageId);
    return cInvalidUint32;
    }

uint8 ThaVcCrossConnectSliceIdPageShift(ThaVcCrossConnect self, uint32 pageId)
    {
    if (self)
        return mMethodsGet(self)->SliceIdPageShift(self, pageId);
    return cInvalidUint8;
    }

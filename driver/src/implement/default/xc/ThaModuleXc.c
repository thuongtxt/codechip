/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Cross connect
 *
 * File        : ThaModuleXc.c
 *
 * Created Date: May 18, 2015
 *
 * Description : Default implement of module cross connect
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaModuleXcInternal.h"
#include "../ocn/ThaModuleOcn.h"
#include "../man/ThaDevice.h"
#include "ThaModuleXcReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaModuleXcMethods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void SdhChannelRegsShow(ThaModuleXc self, AtSdhChannel sdhChannel)
    {
    AtUnused(self);
    AtUnused(sdhChannel);
    }

static uint32 BaseAddress(ThaModuleXc self)
    {
    /* As default, XC module is built-in inside OCN module.*/
    return ThaModuleOcnBaseAddress((ThaModuleOcn)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModuleOcn));
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaModuleXc);
    }

static ThaModuleXc ModuleXc(AtSdhChannel channel)
    {
    return (ThaModuleXc)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)channel), cAtModuleXc);
    }

static uint32 DisconnectSliceId(ThaModuleXc self)
    {
    AtUnused(self);
    return 14;
    }

static void MethodsInit(AtModuleXc self)
    {
    ThaModuleXc moduleXc = (ThaModuleXc)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, SdhChannelRegsShow);
        mMethodOverride(m_methods, BaseAddress);
        mMethodOverride(m_methods, DisconnectSliceId);
        }

    mMethodsSet(moduleXc, &m_methods);
    }

AtModuleXc ThaModuleXcObjectInit(AtModuleXc self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleXcObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

void ThaModuleXcSdhChannelRegsShow(AtSdhChannel sdhChannel)
    {
    ThaModuleXc self = ModuleXc(sdhChannel);
    if (self)
        mMethodsGet(self)->SdhChannelRegsShow(self, sdhChannel);
    }

uint32 ThaModuleXcBaseAddress(ThaModuleXc self)
    {
    if (self)
        return mMethodsGet(self)->BaseAddress(self);
    return cBit31_0;
    }

uint32 ThaModuleXcDisconnectSliceId(ThaModuleXc self)
    {
    if (self)
        return mMethodsGet(self)->DisconnectSliceId(self);
    return cInvalidUint32;
    }

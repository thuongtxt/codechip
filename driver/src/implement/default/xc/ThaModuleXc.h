/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Cross connect
 * 
 * File        : ThaModuleXc.h
 * 
 * Created Date: May 18, 2015
 *
 * Description : Default module XC
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEXC_H_
#define _THAMODULEXC_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleXc.h"
#include "AtSdhChannel.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleXc * ThaModuleXc;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void ThaModuleXcSdhChannelRegsShow(AtSdhChannel sdhChannel);
uint32 ThaModuleXcBaseAddress(ThaModuleXc self);
uint32 ThaModuleXcDisconnectSliceId(ThaModuleXc self);

/* Concrete products */
AtModuleXc Tha60210061ModuleXcNew(AtDevice device);
AtModuleXc Tha60210011ModuleXcNew(AtDevice device);
AtModuleXc Tha60210012ModuleXcNew(AtDevice device);
AtModuleXc Tha60290022ModuleXcNew(AtDevice device);
AtModuleXc Tha60290021ModuleXcNew(AtDevice device);
AtModuleXc Tha6A290021ModuleXcNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEXC_H_ */


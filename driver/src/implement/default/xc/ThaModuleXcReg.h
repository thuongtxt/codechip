/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : XC
 * 
 * File        : ThaModuleXcReg.h
 * 
 * Created Date: Dec 6, 2016
 *
 * Description : Default XC
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEXCREG_H_
#define _THAMODULEXCREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
/*------------------------------------------------------------------------------
Reg Name   : OCN SXC Control 1 - Config Page 0 and Page 1 of SXC
Reg Addr   : 0x44000 - 0x44b2f
Reg Formula: 0x44000 + 256*LineId + StsId
    Where  :
           + $LineId(0-11)
           + $StsId(0-47)
Reg Desc   :
Each register is used for each outgoing STS of any line (12 lines: 0-3:TFI5 side, 4-7: Line Side, 8-11: Loho) can be randomly configured to //connect  to any ingoing STS of any ingoing line (12 lines: 0-3:TFI5 side, 4-7: Line Side, 8-11: Loho). These registers are used for config Pape0 and page1 of SXC.
# HDL_PATH      : isdhsxc.isxc_rxrd[0].rxsxcramctl0.array

------------------------------------------------------------------------------*/
#define cAf6Reg_sxcramctl0_Base                                                                        0x44000
#define cAf6Reg_sxcramctl0(LineId, StsId)                                       (0x44000+256*(LineId)+(StsId))

/*--------------------------------------
BitField Name: SxcLineIdPage1
BitField Type: RW
BitField Desc: Contains the ingoing LineID Page1 (0-11: 0-3:TFI5 side, 4-7: Line
Side, 8-11: Loho). Disconnect (output is all one) if value LineID is other value
(recommend is 14).
BitField Bits: [27:24]
--------------------------------------*/
#define cAf6_sxcramctl0_SxcLineIdPage1_Mask                                                          cBit27_24
#define cAf6_sxcramctl0_SxcLineIdPage1_Shift                                                                24

/*--------------------------------------
BitField Name: SxcStsIdPage1
BitField Type: RW
BitField Desc: Contains the ingoing STSID Page1 (0-47).
BitField Bits: [21:16]
--------------------------------------*/
#define cAf6_sxcramctl0_SxcStsIdPage1_Mask                                                           cBit21_16
#define cAf6_sxcramctl0_SxcStsIdPage1_Shift                                                                 16

/*--------------------------------------
BitField Name: SxcLineIdPage0
BitField Type: RW
BitField Desc: Contains the ingoing LineID Page0 (0-11: 0-3:TFI5 side, 4-7: Line
Side, 8-11: Loho). Disconnect (output is all one) if value LineID is other value
(recommend is 14).
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_sxcramctl0_SxcLineIdPage0_Mask                                                           cBit11_8
#define cAf6_sxcramctl0_SxcLineIdPage0_Shift                                                                 8

/*--------------------------------------
BitField Name: SxcStsIdPage0
BitField Type: RW
BitField Desc: Contains the ingoing STSID Page0 (0-47).
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_sxcramctl0_SxcStsIdPage0_Mask                                                             cBit5_0
#define cAf6_sxcramctl0_SxcStsIdPage0_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : OCN SXC Control 2 - Config Page 2 of SXC
Reg Addr   : 0x44040 - 0x44b6f
Reg Formula: 0x44040 + 256*LineId + StsId
    Where  :
           + $LineId(0-11)
           + $StsId(0-47)
Reg Desc   :
Each register is used for each outgoing STS of any line (12 lines: 0-3:TFI5 side, 4-7: Line Side, 8-11: Loho) can be randomly configured to //connect  to any ingoing STS of any ingoing line (12 lines: 0-3:TFI5 side, 4-7: Line Side, 8-11: Loho). These registers are used for config Pape2 of SXC.
# HDL_PATH      : isdhsxc.isxc_rxrd[0].rxsxcramctl1.array

------------------------------------------------------------------------------*/
#define cAf6Reg_sxcramctl1_Base                                                                        0x44040
#define cAf6Reg_sxcramctl1(LineId, StsId)                                       (0x44040+256*(LineId)+(StsId))

/*--------------------------------------
BitField Name: SxcLineIdPage2
BitField Type: RW
BitField Desc: Contains the ingoing LineID Page2 (0-11: 0-3:TFI5 side, 4-7: Line
Side, 8-11: Loho). Disconnect (output is all one) if value LineID is other value
(recommend is 14).
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_sxcramctl1_SxcLineIdPage2_Mask                                                           cBit11_8
#define cAf6_sxcramctl1_SxcLineIdPage2_Shift                                                                 8

/*--------------------------------------
BitField Name: SxcStsIdPage2
BitField Type: RW
BitField Desc: Contains the ingoing STSID Page2 (0-47).
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_sxcramctl1_SxcStsIdPage2_Mask                                                             cBit5_0
#define cAf6_sxcramctl1_SxcStsIdPage2_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : OCN SXC Control 3 - Config APS
Reg Addr   : 0x44080 - 0x44baf
Reg Formula: 0x44080 + 256*LineId + StsId
    Where  :
           + $LineId(0-11)
           + $StsId(0-47)
Reg Desc   :
Each register is used for APS configuration
# HDL_PATH      : isdhsxc.isxc_rxrd[0].apsctl_ram.array

------------------------------------------------------------------------------*/
#define cAf6Reg_apsramctl_Base                                                                         0x44080
#define cAf6Reg_apsramctl(LineId, StsId)                                        (0x44080+256*(LineId)+(StsId))

/*--------------------------------------
BitField Name: SelectorType
BitField Type: RW
BitField Desc: Selector Type. 3: FSM. Choose based FSM input 2: APS Grouping. 1:
UPSR/SNCP Grouping. 0: Disable APS/UPSR.
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_apsramctl_SelectorType_Mask                                                             cBit13_12
#define cAf6_apsramctl_SelectorType_Shift                                                                   12

/*--------------------------------------
BitField Name: SelectorID
BitField Type: RW
BitField Desc: Contains Selector ID. When Selector Type is: - FSM: Unused this
field. - APS: [8]  : Passthrough Enable for BLSR [7:4]: Passthrough Group ID for
BLSR [3:0]: APS Group ID - UPSR/SNCP: Select UPSR ID.
BitField Bits: [8:0]
--------------------------------------*/
#define cAf6_apsramctl_SelectorID_Mask                                                                 cBit8_0
#define cAf6_apsramctl_SelectorID_Shift                                                                      0

#define cAf6_apsramctl_SelectorID_PassthrEn_Mask                                                         cBit8
#define cAf6_apsramctl_SelectorID_PassthrEn_Shift                                                            8
#define cAf6_apsramctl_SelectorID_PassthrID_Mask                                                       cBit7_4
#define cAf6_apsramctl_SelectorID_PassthrID_Shift                                                            4
#define cAf6_apsramctl_SelectorID_ApsID_Mask                                                           cBit3_0
#define cAf6_apsramctl_SelectorID_ApsID_Shift                                                                0

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEXCREG_H_ */


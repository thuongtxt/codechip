/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Cross connect
 * 
 * File        : ThaModuleXcInternal.h
 * 
 * Created Date: May 18, 2015
 *
 * Description : Module cross connect declaration
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEXCINTERNAL_H_
#define _THAMODULEXCINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/xc/AtModuleXcInternal.h"
#include "ThaModuleXc.h"
#include "AtSdhChannel.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleXcMethods
    {
    void (*SdhChannelRegsShow)(ThaModuleXc self, AtSdhChannel sdhChannel);
    uint32 (*BaseAddress)(ThaModuleXc self);
    uint32 (*DisconnectSliceId)(ThaModuleXc self);
    }tThaModuleXcMethods;

typedef struct tThaModuleXc
    {
    tAtModuleXc super;
    const tThaModuleXcMethods *methods;
    }tThaModuleXc;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleXc ThaModuleXcObjectInit(AtModuleXc self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEXCINTERNAL_H_ */


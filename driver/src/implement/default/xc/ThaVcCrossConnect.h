/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : XC
 * 
 * File        : ThaVcCrossConnect.h
 * 
 * Created Date: Nov 24, 2016
 *
 * Description : Default STS cross-connect
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAVCCROSSCONNECT_H_
#define _THAVCCROSSCONNECT_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSdhChannel.h"
#include "AtModuleXc.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaVcCrossConnect* ThaVcCrossConnect;

typedef enum eThaXcVcSelectorType
    {
    cThaXcVcSelectorDisable     = 0,
    cThaXcVcSelectorUpsr        = 1,
    cThaXcVcSelectorApsGroup    = 2,
    cThaXcVcSelectorDeviceRole  = 3,
    cThaXcVcSelectorPassthrough = 4,
    cThaXcVcSelectorUnknown     = 5
    }eThaXcVcSelectorType;

typedef enum eThaXcVcConnectionType
    {
    cThaXcVcConnectionWorking       = 0,
    cThaXcVcConnectionProtection    = 1,
    cThaXcVcConnectionPassthrough   = 2,
    cThaXcVcConnectionUnknown       = 3
    }eThaXcVcConnectionType;

typedef enum eThaXcVcSelectorState
    {
    cThaXcVcSelectorStateActive      = 0,
    cThaXcVcSelectorStateStandby     = 1,
    cThaXcVcSelectorStateUnknown     = 2
    }eThaXcVcSelectorState;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet ThaVcCrossConnectHwStsGet(ThaVcCrossConnect self, AtSdhChannel channel, uint8 sts1Id, uint8 *hwSlice, uint8 *hwSts);

/* Cross-connect handling in SDH VC basis. */
eAtRet ThaVcCrossConnectVcConnectAtIndex(ThaVcCrossConnect self, AtSdhChannel destVc, AtSdhChannel sourceVc, uint32 sourceIndex);
eAtRet ThaVcCrossConnectVcDisconnectAtIndex(ThaVcCrossConnect self, AtSdhChannel destVc, uint32 sourceIndex);
eAtRet ThaVcCrossConnectVcSelectorSet(ThaVcCrossConnect self, AtSdhChannel destVc, uint32 selectorType, uint32 selectorId);
uint32 ThaVcCrossConnectVcSelectorTypeGet(ThaVcCrossConnect self, AtSdhChannel destVc);
uint32 ThaVcCrossConnectVcSelectorStateGet(ThaVcCrossConnect self, AtSdhChannel destVc);

/* Cross-connect handling in hardware STS-1 basis. */
uint32 ThaVcCrossConnectNumHwSourceSts(ThaVcCrossConnect self);
eAtRet ThaVcCrossConnectHwStsConnect(ThaVcCrossConnect self, uint8 destSlice, uint8 destSts, uint32 sourceIndex, uint8 sourceSlice, uint8 sourceSts);
eAtRet ThaVcCrossConnectHwStsDisconnect(ThaVcCrossConnect self, uint8 destSlice, uint8 destSts, uint32 sourceIndex);
eAtRet ThaVcCrossConnectHwSourceStsGet(ThaVcCrossConnect self, uint8 destSlice, uint8 destSts, uint32 sourceIndex, uint8 *sourceSlice, uint8 *sourceSts);

uint32 ThaVcCrossConnectSliceIdPageMask(ThaVcCrossConnect self, uint32 pageId);
uint8 ThaVcCrossConnectSliceIdPageShift(ThaVcCrossConnect self, uint32 pageId);

/* Concrete products */
AtCrossConnect Tha60290022HoVcCrossConnectNew(AtModuleXc module);

#ifdef __cplusplus
}
#endif
#endif /* _THAVCCROSSCONNECT_H_ */


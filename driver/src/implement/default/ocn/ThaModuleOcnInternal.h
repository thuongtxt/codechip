/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : OCN internal module
 * 
 * File        : ThaModuleOcnInternal.h
 * 
 * Created Date: Sep 10, 2012
 *
 * Description :
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEOCNINTERNAL_H_
#define _THAMODULEOCNINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/man/AtModuleInternal.h"
#include "../../../generic/pw/AtPwInternal.h"
#include "ThaModuleOcn.h"
#include "AtSdhLine.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

typedef enum eThaOcnVtTuType
    {
    cThaOcnVt15Tu11                 = 0      , /* Define VT15/TU11 type. */
    cThaOcnVt2Tu12                  = 1      , /* Define VT2/TU12 type. */
    cThaOcnVt3                      = 2      , /* Define VT3 type. */
    cThaOcnVt6Tu2                   = 3       /* Define VT6/TU2 type. */
    }eThaOcnVtTuType;

typedef struct tThaModuleOcnMethods
    {
    uint32 (*BaseAddress)(ThaModuleOcn self);
    uint32 (*StsDefaultOffset)(ThaModuleOcn self, AtSdhChannel sdhChannel, uint8 sts);
    uint32 (*StsVtDefaultOffset)(ThaModuleOcn self, AtSdhChannel channel, uint8 sts, uint8 vtg, uint8 vt);
    uint32 (*TxStsVtDefaultOffset)(ThaModuleOcn self, AtSdhChannel channel, uint8 sts, uint8 vtg, uint8 vt);
    uint32 (*TxTerHwHoStsOffsetWithBaseAddress)(ThaModuleOcn self, AtSdhChannel channel, uint8 hwSlice, uint8 hwSts);
    uint32 (*LineDefaultOffset)(ThaModuleOcn self, AtSdhChannel sdhLine);
    uint32 (*LineTxDefaultOffset)(ThaModuleOcn self, AtSdhChannel sdhLine);
    eAtRet (*StsIdSw2Hw)(ThaModuleOcn self, AtSdhChannel sdhChannel, eAtModule moduleId, uint8 sts1Id, uint8 *sliceId, uint8 *hwStsInSlice);
    eAtRet (*ChannelStsIdSw2HwGet)(ThaModuleOcn self, AtSdhChannel sdhChannel, eAtModule moduleId, uint8 swSts, uint8* sliceId, uint8 *hwStsInSlice);
    eAtRet (*DefaultSet)(ThaModuleOcn self);
    eAtRet (*TohThresholdDefaultSet)(ThaModuleOcn self);
    eAtRet (*Tug3PldSet)(ThaModuleOcn self, AtSdhChannel tug3, eAtSdhTugMapType tug3MapType);
    eAtRet (*LineIdSw2HwGet)(ThaModuleOcn self, AtSdhLine sdhLine, eAtModule moduleId, uint8* sliceId, uint8 *hwLineInSlice);

    uint8  (*NumStsInOneSlice)(ThaModuleOcn self);
    uint8  (*NumSlices)(ThaModuleOcn self);
    eAtRet (*PwTohByteEnable)(ThaModuleOcn self, AtPw pw, const tThaTohByte *tohByte, eBool enable);
    eAtRet (*PwTohSourceSet)(ThaModuleOcn self, AtPw pw, AtSdhLine circuit);
    uint32 (*RxStsPiMastIdMask)(ThaModuleOcn self);

    /* Concatenation */
    eAtRet (*PiStsConcatMasterSet)(ThaModuleOcn self, AtSdhChannel channel, uint8 masterStsId);
    eAtRet (*PiStsConcatSlaveSet)(ThaModuleOcn self, AtSdhChannel channel, uint8 masterStsId, uint8 slaveStsId);
    eAtRet (*PgStsConcatMasterSet)(ThaModuleOcn self, AtSdhChannel channel, uint8 masterStsId);
    eAtRet (*PgStsConcatSlaveSet)(ThaModuleOcn self, AtSdhChannel channel, uint8 masterStsId, uint8 slaveStsId);
    eAtRet (*TerPgStsConcatMasterSet)(ThaModuleOcn self, AtSdhChannel channel, uint8 masterStsId);
    eAtRet (*TerPgStsConcatSlaveSet)(ThaModuleOcn self, AtSdhChannel channel, uint8 masterStsId, uint8 slaveStsId);
    eAtRet (*TerPgStsConcatRelease)(ThaModuleOcn self, AtSdhChannel channel, uint8 stsHwSlice, uint8 masterHwSts, uint8 hwSts);
    eBool  (*StsIsSlaveInChannel)(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId);
    eAtRet (*StsConcatRelease)(ThaModuleOcn self, AtSdhChannel aug, uint8 sts1Id);
    eAtRet (*PgSlaveIndicate)(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId, eBool isSlave);
    eAtRet (*TerPgSlaveIndicate)(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId, eBool isSlave);
    eAtRet (*Vc4xPayloadDefault)(ThaModuleOcn self, AtSdhChannel sdhVc);

    /* Pass-through */
    eAtRet (*PiStsGroupIdSet)(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId, uint32 groupId);

    /* VC-3 interworking */
    eAtRet (*Vc3ToTu3VcEnable)(ThaModuleOcn self, AtSdhChannel vc3, eBool enable);
    eBool (*Vc3ToTu3VcIsEnabled)(ThaModuleOcn self, AtSdhChannel vc3);
    eAtRet (*Tu3VcToVc3Enable)(ThaModuleOcn self, AtSdhChannel tu3Vc, eBool enable);
    eBool (*Tu3VcToVc3IsEnabled)(ThaModuleOcn self, AtSdhChannel tu3Vc);

    /* POH processing */
    eBool (*NeedTerminateAsDefault)(ThaModuleOcn self, AtSdhChannel channel);
    void (*VtTerminate)(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId, uint8 vtgId, uint8 vtId, eBool terminate);
    void (*VtPohInsertEnable)(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId, uint8 vtgId, uint8 vtId, eBool enable);
    eBool (*VtPohInsertIsEnabled)(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId, uint8 vtgId, uint8 vtId);
    eAtRet (*RxStsTermEnable)(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId, eBool enable);
    eAtRet (*StsPohInsertEnable)(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId, eBool enable);
    eBool (*StsPohInsertIsEnabled)(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId);
    eAtRet (*StsTxPayloadTypeSet)(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId, eThaStsPayloadType payloadType);
    uint8 (*StsTxPayloadTypeGet)(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId);
    
    /* For debugging */
    void (*SdhChannelRegsShow)(ThaModuleOcn self, AtSdhChannel sdhChannel);

    eAtRet (*RxVtgSet)(ThaModuleOcn self, AtSdhChannel tug2, uint8 stsId, uint8 vtgId, eThaOcnVtTuType vtgTypeCfg);
    eAtRet (*TxVtgSet)(ThaModuleOcn self, AtSdhChannel tug2, uint8 stsId, uint8 vtgId, eThaOcnVtTuType vtgTypeCfg);
    eAtRet (*RxVtgPdhReset)(ThaModuleOcn self, AtSdhChannel tug2, uint8 stsId, uint8 vtgId, eThaOcnVtTuType vtgTypeCfg);
    eAtRet (*TxVtgPdhReset)(ThaModuleOcn self, AtSdhChannel tug2, uint8 stsId, uint8 vtgId, eThaOcnVtTuType vtgTypeCfg);

    /* VT SS bit */
    eAtRet (*PpSsTxVtSet)(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId, uint8 vtgId, uint8 vtId,  uint8 insSsBit, eBool insSsBitEn);
    uint8 (*PpSsTxVtGet)(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId, uint8 vtgId, uint8 vtId);
    eAtRet (*VtTuExpectedSsSet)(ThaModuleOcn self, AtSdhPath vtTu, uint8 value);
    uint8 (*VtTuExpectedSsGet)(ThaModuleOcn self, AtSdhPath vtTu);
    eAtRet (*VtTuSsCompareEnable)(ThaModuleOcn self, AtSdhPath vtTu, eBool enable);
    eBool (*VtTuSsCompareIsEnabled)(ThaModuleOcn self, AtSdhPath vtTu);

    /* VT/TU alarm forcing */
    uint32 (*VtTuTxForcibleAlarms)(ThaModuleOcn self);
    eAtRet (*VtTuHwTxAlarmForce)(ThaModuleOcn self, AtChannel vtTu, uint32 alarmType, eBool force);
    uint32 (*VtTuTxForcedAlarmGet)(ThaModuleOcn self, AtChannel vtTu);
    uint32 (*VtTuRxForcibleAlarms)(ThaModuleOcn self);
    eAtRet (*VtTuHwRxAlarmForce)(ThaModuleOcn self, AtChannel vtTu, uint32 alarmType, eBool force);
    uint32 (*VtTuRxForcedAlarmGet)(ThaModuleOcn self, AtChannel vtTu);

    eAtRet (*Vc3PldSet)(ThaModuleOcn self, AtSdhChannel vc3, eThaOcnVc3PldType vc3Pld);
    eAtRet (*Vc4xTxEnable)(ThaModuleOcn self, AtSdhChannel channel, eBool vc4En);
    eAtRet (*Tug3HwPayloadTypeSet)(ThaModuleOcn self, AtSdhChannel tug3, eAtSdhTugMapType tug3MapType);
    eAtRet (*Au3VcHwPayloadTypeSet)(ThaModuleOcn self, AtSdhChannel vc3, eAtSdhVcMapType mapType);

    /* For remote loopback and RX AIS forcing */
    eAtRet (*StsLoopbackSet)(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId, uint8 loopbackMode);
    uint8 (*StsLoopbackGet)(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId);
    eAtRet (*VtTuLoopbackSet)(ThaModuleOcn self, AtSdhChannel channel, uint8 loopbackMode);
    uint8 (*VtTuLoopbackGet)(ThaModuleOcn self, AtSdhChannel channel);
    eBool (*CanMovePathAisRxForcingPoint)(ThaModuleOcn self);
    eAtRet (*PathAisRxForcingPointMove)(ThaModuleOcn self, AtSdhAu au, eBool move);
    eBool (*PathAisRxForcingPointIsMoved)(ThaModuleOcn self, AtSdhAu au);
    eAtRet (*NotifyLoVcAisToPoh)(ThaModuleOcn self, AtSdhVc vc, eBool notified);

    uint32 (*TohGlobalS1StableMonitoringThresholdControlRegAddr)(ThaModuleOcn self);
    uint32 (*TohGlobalK1StableMonitoringThresholdControlRegAddr)(ThaModuleOcn self);
    uint32 (*TohGlobalK2StableMonitoringThresholdControlRegAddr)(ThaModuleOcn self);

    /* SOH Over ETH */
    uint32 *(*SohOverEthHoldRegistersGet)(ThaModuleOcn self, uint16 *numberOfHoldRegisters);
    AtLongRegisterAccess (*SohOverEthLongRegisterAccessCreate)(ThaModuleOcn self);
    AtLongRegisterAccess (*SohOverEthLongRegisterAccess)(ThaModuleOcn self);
    uint16 (*SohOverEthLongReadOnCore)(ThaModuleOcn self, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen, AtIpCore core);
    uint16 (*SohOverEthLongWriteOnCore)(ThaModuleOcn self, uint32 localAddress, const uint32 *dataBuffer, uint16 bufferLen, AtIpCore core);
    eAtRet (*SohOverEthInit)(ThaModuleOcn self);
    eBool  (*SohOverEthIsSupported)(ThaModuleOcn self);
    eBool  (*KByteOverEthIsSupported)(ThaModuleOcn self);
    uint32 (*SohOverEthBaseAddress)(ThaModuleOcn self);
    uint32 (*RxStsPayloadControl)(ThaModuleOcn self, AtSdhChannel channel);
    uint32 (*TxStsMultiplexingControl)(ThaModuleOcn self, AtSdhChannel channel);
    uint32 (*UpenDccdecBase)(ThaModuleOcn self);
    eBool (*NeedIssolateDcc)(ThaModuleOcn self);
    eBool (*IsSohRegister)(ThaModuleOcn self, uint32 localAddress);
    eAtRet (*LineTxRsDccInsByOhBusEnable)(ThaModuleOcn self, uint32 lineId, eBool enable);
    eBool (*LineTxRsDccInsByOhBusEnabled)(ThaModuleOcn self, uint32 lineId);
    eAtRet (*LineTxMsDccInsByOhBusEnable)(ThaModuleOcn self, uint32 lineId, eBool enable);
    eBool (*LineTxMsDccInsByOhBusEnabled)(ThaModuleOcn self, uint32 lineId);

    /* VC3 mapping DS3/E3 LIU */
    eAtRet (*Vc3MapDe3LiuEnable)(ThaModuleOcn self, AtSdhChannel sdhVc, eBool enable);
    eBool (*Vc3MapDe3LiuIsEnabled)(ThaModuleOcn self, AtSdhChannel sdhVc);
    }tThaModuleOcnMethods;

typedef struct tThaModuleOcn
    {
    tAtModule super;
    const tThaModuleOcnMethods *methods;

    AtLongRegisterAccess tohLongRegisterAccess;
    }tThaModuleOcn;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule ThaModuleOcnObjectInit(AtModule module, AtDevice device);
AtIterator ThaModuleOcnRegisterIteratorCreate(AtModule module);
uint8 ThaModuleOcnVtTuTypeSw2Hw(eThaOcnVtTuType vtTuType);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEOCNINTERNAL_H_ */


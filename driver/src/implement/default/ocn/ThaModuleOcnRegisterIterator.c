/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : OCN
 *
 * File        : ThaModuleOcnRegisterIterator.c
 *
 * Created Date: Jan 6, 2013
 *
 * Description : OCN register iterator
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtRegister.h"

#include "../../../util/AtIteratorInternal.h"
#include "../../../generic/memtest/AtModuleRegisterIteratorInternal.h"
#include "../sdh/ThaModuleSdhReg.h"
#include "../sdh/ThaModuleSdhRegisterIteratorInternal.h"
#include "ThaModuleOcnInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
enum eOcnRegType
    {
    cRegTypeLineJ0 = cSdhRegTypeEnd
    };

typedef struct tThaModuleOcnRegisterIterator * ThaModuleOcnRegisterIterator;

typedef struct tThaModuleOcnRegisterIterator
    {
    tThaModuleSdhRegisterIterator super;

    /* Private data */
    uint8 j0DwId;
    }tThaModuleOcnRegisterIterator;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtIteratorMethods m_AtIteratorOverride;
static tAtModuleRegisterIteratorMethods m_AtModuleRegisterIteratorOverride;

static const tAtModuleRegisterIteratorMethods *m_AtModuleRegisterIteratorMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaModuleOcnRegisterIterator);
    }

static uint8 DefaultCore(AtModuleRegisterIterator self)
    {
    /* May change this */
    return m_AtModuleRegisterIteratorMethods->DefaultCore(self);
    }

static void NextJ0(AtModuleRegisterIterator self)
    {
    ThaModuleOcnRegisterIterator iterator = (ThaModuleOcnRegisterIterator)self;

    iterator->j0DwId = (uint8)(iterator->j0DwId + 1);

    /* Next Line */
    if (iterator->j0DwId == cMaxNumJ0Dwords)
        {
        ThaModuleSdhRegisterIteratorNextLine((ThaModuleSdhRegisterIterator)self);
        iterator->j0DwId = 0;
        }
    }

static uint32 NextJ0Offset(AtModuleRegisterIterator self)
    {
    ThaModuleOcnRegisterIterator iterator = (ThaModuleOcnRegisterIterator)self;
    uint8 line;

    NextJ0(self);
    line = ThaModuleSdhRegisterIteratorLineGet((ThaModuleSdhRegisterIterator)self);
    return (uint32)((line * cMaxNumJ0Dwords) + iterator->j0DwId);
    }

static uint32 NextLineOffset(AtModuleRegisterIterator self)
    {
    ThaModuleSdhRegisterIterator iterator = (ThaModuleSdhRegisterIterator)self;
    return iterator->lineId;
    }

static uint32 NextStsOffset(AtModuleRegisterIterator self)
    {
    uint8 slice, sts;

    ThaModuleSdhRegisterIteratorNextSts((ThaModuleSdhRegisterIterator)self, &slice, &sts);
    return (uint32)((slice * 8192) + sts);
    }

static uint32 NextVtOffset(AtModuleRegisterIterator self)
    {
    uint8 slice, sts, vtg, vt;

    ThaModuleSdhRegisterIteratorNextVt((ThaModuleSdhRegisterIterator)self, &slice, &sts, &vtg, &vt);
    return (uint32)((slice * 8192) + (sts * 32) + (vtg * 4) + vt);
    }

static uint8 CurrentRegType(AtModuleRegisterIterator  self)
    {
    return ThaModuleSdhRegisterIteratorCurrentRegType((ThaModuleSdhRegisterIterator)self);
    }

static uint32 NextOffset(AtModuleRegisterIterator self)
    {
    if (CurrentRegType(self) == cSdhRegTypeLine)
        return NextLineOffset(self);

    if (CurrentRegType(self) == cRegTypeLineJ0)
        return NextJ0Offset(self);

    if (CurrentRegType(self) == cSdhRegTypeSts)
        return NextStsOffset(self);

    if (CurrentRegType(self) == cSdhRegTypeVt)
        return NextVtOffset(self);

    /* Let the super determine this */
    return 0;
    }

static eBool NoJ0DwordLeft(AtModuleRegisterIterator self)
    {
    uint8 line;
    AtDevice device = AtModuleDeviceGet(AtModuleRegisterIteratorModuleGet(self));
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);

    line = ThaModuleSdhRegisterIteratorLineGet((ThaModuleSdhRegisterIterator)self);
    if (line >= AtModuleSdhMaxLinesGet(sdhModule))
        return cAtTrue;

    return cAtFalse;
    }

static eBool NoChannelLeft(AtModuleRegisterIterator self)
    {
    if (CurrentRegType(self) == cRegTypeLineJ0)
        return NoJ0DwordLeft(self);

    return m_AtModuleRegisterIteratorMethods->NoChannelLeft(self);
    }

static void ResetChannel(AtModuleRegisterIterator self)
    {
    ThaModuleOcnRegisterIterator iterator = (ThaModuleOcnRegisterIterator)self;

    m_AtModuleRegisterIteratorMethods->ResetChannel(self);
    iterator->j0DwId = 0;
    }

static void RegTypeSet(AtIterator self, uint8 type)
    {
    ThaModuleSdhRegisterIteratorCurrentRegTypeSet((ThaModuleSdhRegisterIterator)self, type);
    }

static AtObject NextGet(AtIterator self)
    {
    AtModuleRegisterIterator iterator = (AtModuleRegisterIterator)self;

    mNextReg(iterator, 0, cThaRegOcnRxLineEnCtrl);
    mNextReg(iterator, cThaRegOcnRxLineEnCtrl               , cThaRegOcnTxLineEnCtrl);
    mNextReg(iterator, cThaRegOcnTxLineEnCtrl               , cThaRegOcnRxLineRateCtrl);
    mNextReg(iterator, cThaRegOcnRxLineRateCtrl             , cThaRegOcnTxLineRateCtrl);
    mNextReg(iterator, cThaRegOcnTxLineRateCtrl             , cThaRegOcnRxLineOc3linetoPtrSts3Ctrl);
    mNextReg(iterator, cThaRegOcnRxLineOc3linetoPtrSts3Ctrl , cThaRegOcnTxFrmrEngBwCtrl);
    mNextReg(iterator, cThaRegOcnTxFrmrEngBwCtrl            , cThaRegOcnTxSts3SrcOfTxLineCtrl);
    mNextReg(iterator, cThaRegOcnTxSts3SrcOfTxLineCtrl      , cThaRegOcnFrmrAlmAffCtrl);
    mNextReg(iterator, cThaRegOcnFrmrAlmAffCtrl             , cThaRegOcnFrmrAlmTimLAffCtrl);
    mNextReg(iterator, cThaRegOcnFrmrAlmTimLAffCtrl         , cThaRegOcnRxFrmrPerChnCtrl);

    RegTypeSet(self, cSdhRegTypeLine);
    mNextChannelRegister(iterator, cThaRegOcnRxFrmrPerChnCtrl  , cThaRegOcnTxFrmrPerChnCtrl1);
    mNextChannelRegister(iterator, cThaRegOcnTxFrmrPerChnCtrl1 , cThaRegOcnTxFrmrPerChnCtrl2);
    mNextChannelRegister(iterator, cThaRegOcnTxFrmrPerChnCtrl2 , cThaRegOcnJ0MsgInsBuf);

    RegTypeSet(self, cRegTypeLineJ0);
    mNextChannelRegister(iterator, cThaRegOcnJ0MsgInsBuf    , cThaRegOcnLineLoopInCtrl);
    mNextChannelRegister(iterator, cThaRegOcnLineLoopInCtrl , cThaRegOcnTohMonAffCtrl);

    RegTypeSet(self, cSdhRegTypeLine);
    mNextChannelRegister(iterator, cThaRegOcnTohMonAffCtrl    , cThaRegOcnTohMonPerChnCtrl);
    mNextChannelRegister(iterator, cThaRegOcnTohMonPerChnCtrl , cThaRegOcnRxLineperAlmIntrEnCtrl);
    mNextChannelRegister(iterator, cThaRegOcnRxLineperAlmIntrEnCtrl, cThaRegOcnRxStsPiPerChnCtrl);

    RegTypeSet(self, cSdhRegTypeSts);
    mNextChannelRegister(iterator, cThaRegOcnRxStsPiPerChnCtrl   , cThaRegOcnRxPpPerStsVcPldCtrl);
    mNextChannelRegister(iterator, cThaRegOcnRxPpPerStsVcPldCtrl , cThaRegOcnVtTUPIPerChnCtrl);

    RegTypeSet(self, cSdhRegTypeVt);
    mNextChannelRegister(iterator, cThaRegOcnVtTUPIPerChnCtrl , cThaRegOcnTxPgDemuxPerStsVcPldCtrl);
    RegTypeSet(self, cSdhRegTypeSts);
    mNextChannelRegister(iterator, cThaRegOcnTxPgDemuxPerStsVcPldCtrl, cThaRegOcnTxPGPerChnCtrl);
    RegTypeSet(self, cSdhRegTypeVt);
    mNextChannelRegister(iterator, cThaRegOcnTxPGPerChnCtrl, cThaRegOcnTxPohInsPerChnCtrl);
    RegTypeSet(self, cSdhRegTypeSts);
    mNextChannelRegister(iterator, cThaRegOcnTxPohInsPerChnCtrl, cThaRegOcnTxVc4EnCtrl);
    mNextReg(iterator, cThaRegOcnTxVc4EnCtrl, cThaRegOcnRxStsVCperAlmIntrEnCtrl);
    mNextReg(iterator, cThaRegOcnRxStsVCperAlmIntrEnCtrl, cThaRegOcnRxVtTUperAlmIntrEnCtrl);
    RegTypeSet(self, cSdhRegTypeVt);
    mNextReg(iterator, cThaRegOcnRxVtTUperAlmIntrEnCtrl, cAtModuleRegisterIteratorEnd);

    return NULL;
    }

static void OverrideAtIterator(AtIterator self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtIteratorOverride, mMethodsGet(self), sizeof(m_AtIteratorOverride));
        mMethodOverride(m_AtIteratorOverride, NextGet);
        }

    mMethodsSet(self, &m_AtIteratorOverride);
    }

static void OverrideAtModuleRegisterIterator(AtIterator self)
    {
    AtModuleRegisterIterator iterator = (AtModuleRegisterIterator)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleRegisterIteratorMethods = mMethodsGet(iterator);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleRegisterIteratorOverride, mMethodsGet(iterator), sizeof(m_AtModuleRegisterIteratorOverride));
        mMethodOverride(m_AtModuleRegisterIteratorOverride, NextOffset);
        mMethodOverride(m_AtModuleRegisterIteratorOverride, NoChannelLeft);
        mMethodOverride(m_AtModuleRegisterIteratorOverride, ResetChannel);
        mMethodOverride(m_AtModuleRegisterIteratorOverride, DefaultCore);
        }

    mMethodsSet(iterator, &m_AtModuleRegisterIteratorOverride);
    }

static void Override(AtIterator self)
    {
    OverrideAtIterator(self);
    OverrideAtModuleRegisterIterator(self);
    }

static AtIterator ThaModuleOcnRegisterIteratorObjectInit(AtIterator self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    if (AtModuleRegisterIteratorObjectInit((AtModuleRegisterIterator)self, module) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtIterator ThaModuleOcnRegisterIteratorCreate(AtModule module)
    {
    AtIterator newIterator = AtOsalMemAlloc(ObjectSize());

    return ThaModuleOcnRegisterIteratorObjectInit(newIterator, module);
    }

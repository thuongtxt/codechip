/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : OCN (internal)
 *
 * File        : ThaModuleOcn.c
 *
 * Created Date: Sep 10, 2012
 *
 * Description : OCN internal module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModuleSdh.h"
#include "../../../util/coder/AtCoderUtil.h"
#include "../../../generic/sdh/AtSdhChannelInternal.h"
#include "../../default/man/ThaDeviceInternal.h"
#include "../sdh/ThaModuleSdhReg.h"
#include "../sdh/ThaModuleSdh.h"
#include "../pdh/ThaStmModulePdh.h"
#include "../cdr/ThaModuleCdrStm.h"
#include "../map/ThaModuleStmMap.h"
#include "../poh/ThaModulePoh.h"
#include "../sdh/ThaSdhLine.h"
#include "ThaModuleOcnInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaOcnNumVt15InVtg 4
#define cThaOcnNumVt2InVtg 3

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) (ThaModuleOcn)self

/*--------------------------- Local typedefs ---------------------------------*/


/* The following enumeration defines constants indicating SS bit mode. */
typedef enum eThaCfgOcnSsMd
    {
    cThaOcnSsVt15Tu11Md              = 3      , /* SS bit in case of VT1.5/TU11 */
    cThaOcnSsVt2Tu12Md               = 2       /* Ss bit in case of VT2/TU12 */
    }eThaCfgOcnSsMd;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaModuleOcnMethods m_methods;

/* Override */
static tAtModuleMethods m_AtModuleOverride;
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementations */
static const tAtModuleMethods *m_AtModuleMethods = NULL;
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 NumSlices(ThaModuleOcn self)
    {
	AtUnused(self);
    return 1;
    }

static uint8 NumStsInOneSlice(ThaModuleOcn self)
    {
	AtUnused(self);
    return 12;
    }

static ThaDevice Device(ThaModuleOcn self)
    {
    return (ThaDevice)AtModuleDeviceGet((AtModule)self);
    }

static void PartConcatReset(ThaModuleOcn self, uint8 partId)
    {
    uint8 slice, sts, vtg, vt;
    uint32 stsOffset, vtOffset;
    uint32 partOffset = ThaModuleOcnPartOffset(self, partId);
    uint8 numSlices = ThaModuleOcnNumSlices(self);
    uint8 numStsInSlice = ThaModuleOcnNumStsInOneSlice(self);

    for (slice = 0; slice < numSlices; slice++)
        {
        stsOffset = slice * 8192UL;
        for (sts = 0; sts < numStsInSlice; sts++)
            {
            mModuleHwWrite(self, cThaRegOcnTxPohInsPerChnCtrl + stsOffset + sts + partOffset, sts);
            mModuleHwWrite(self, cThaRegOcnRxStsPiPerChnCtrl  + stsOffset + sts + partOffset, sts);

            for (vtg = 0; vtg < 7; vtg++)
                {
                vtOffset = (uint32)(stsOffset + (sts * 32UL) + (vtg * 4UL));
                for (vt = 0; vt < 4; vt++)
                    mModuleHwWrite(self, cThaRegOcnTxPGPerChnCtrl + vtOffset + vt + partOffset, sts);
                }
            }
        }
    }

static uint8 NumLinesPerPart(ThaModuleOcn self)
    {
    AtUnused(self);
    return 4;
    }

static eAtRet PartDefaultSet(ThaModuleOcn self, uint8 partId)
    {
    uint32 regValue, regAddr;
    uint32 partOffset = ThaModuleOcnPartOffset(self, partId);
    uint8 line_i;

    regAddr  = cThaRegOcnTxFrmrEngBwCtrl + partOffset;
    regValue = mModuleHwRead(self, regAddr);
    mFieldIns(&regValue, cThaOcnTxFrmEng1Sts12Mask, cThaOcnTxFrmEng1Sts12Shift, 1);
    mModuleHwWrite(self, regAddr, regValue);

    mModuleHwWrite(self, cThaRegOcnTxSts3SrcOfTxLineCtrl + partOffset, cThaRegOcnTxSts3SrcOfTxLineCtrlRstValue);
    mModuleHwWrite(self, cThaRegOcnFrmrAlmAffCtrl + partOffset, cThaRegOcnFrmrAlmAffCtrlRstVal);

    regAddr  = cThaRegOcnTohMonAffCtrl + partOffset;
    regValue = mModuleHwRead(self, regAddr);
    mFieldIns(&regValue, cThaTohMonAisAffStbEnBMask, cThaTohMonAisAffStbEnBShift, 1);
    mFieldIns(&regValue, cThaLosMaskLofEnableMask, cThaLosMaskLofEnableShift, 1);
    mModuleHwWrite(self, regAddr, regValue);

    PartConcatReset(self, partId);
    mModuleHwWrite(self, cThaRegOcnTuDemuxMasterControl + partOffset, cThaRegOcnTuDemuxMasterControlDefault);

    /* Clear alarm status */
    for (line_i = 0; line_i < NumLinesPerPart(self); line_i++)
        {
        regAddr = cThaRegOcnRxLineperAlmCurrentStat + line_i + partOffset;
        mModuleHwWrite(self, regAddr, 0x0);
        }

    /* Note: hardware default value is 0x5410 and this maybe not correct and
     * make clock extractor fail. The value 0x3210 is the new one that hardware
     * team recommends. */
    mModuleHwWrite(self, cThaRegOcnRxRefSrcForTimingPartCtrl + partOffset, 0x3210);

    return cAtOk;
    }

static eAtRet DefaultSet(ThaModuleOcn self)
    {
    uint8 part_i;
    eAtRet ret = cAtOk;

    for (part_i = 0; part_i < ThaModuleOcnNumParts(self); part_i++)
        ret |= PartDefaultSet(self, part_i);

    return ret;
    }

static eAtRet Init(AtModule self)
    {
    ThaModuleOcn module = mThis(self);
    eAtRet ret;

    ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    ret = mMethodsGet(module)->DefaultSet(module);
    if (ret != cAtOk)
        return ret;

    if (mMethodsGet(module)->SohOverEthIsSupported(module))
        ret |= mMethodsGet(module)->SohOverEthInit(module);

    return cAtOk;
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static void Delete(AtObject self)
    {
    ThaModuleOcn module = (ThaModuleOcn)self;
    AtObjectDelete((AtObject)(module->tohLongRegisterAccess));
    module->tohLongRegisterAccess = NULL;
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaModuleOcn object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObject(tohLongRegisterAccess);
    }

static uint32 ChannelPartOffset(AtSdhChannel channel)
    {
    return ThaModuleOcnPartOffset((ThaModuleOcn)AtChannelModuleGet((AtChannel)channel), ThaModuleSdhPartOfChannel(channel));
    }

static uint32 LocalStsInSliceOffset(AtSdhChannel channel, uint8 hwSlice, uint8 hwStsId)
    {
    return (uint32)(hwSlice << 13 /* 8192 */) + hwStsId + ChannelPartOffset(channel);
    }

static uint32 StsDefaultOffset(ThaModuleOcn self, AtSdhChannel channel, uint8 sts)
    {
    uint8 hwSlice, hwSts;
	AtUnused(self);

	ThaSdhChannelHwStsGet(channel, cThaModuleOcn, sts, &hwSlice, &hwSts);
    return LocalStsInSliceOffset(channel, hwSlice, hwSts);
    }

static uint32 LocalStsVtInSliceOffset(AtSdhChannel channel, uint8 hwSlice, uint8 hwStsId, uint8 vtg, uint8 vt)
    {
    return (uint32)(hwSlice << 13 /* 8192 */) + (uint32)(hwStsId << 5) + (uint32)(vtg << 2) + vt + ChannelPartOffset(channel);
    }

static uint32 StsVtDefaultOffset(ThaModuleOcn self, AtSdhChannel channel, uint8 sts, uint8 vtg, uint8 vt)
    {
    uint8 hwSlice, hwSts;
	AtUnused(self);

	ThaSdhChannelHwStsGet(channel, cThaModuleOcn, sts, &hwSlice, &hwSts);
    return LocalStsVtInSliceOffset(channel, hwSlice, hwSts, vtg, vt);
    }

static uint32 TxStsVtDefaultOffset(ThaModuleOcn self, AtSdhChannel channel, uint8 sts, uint8 vtg, uint8 vt)
    {
    return mMethodsGet(self)->StsVtDefaultOffset(self, channel, sts, vtg, vt);
    }

static uint32 TxTerHwHoStsOffsetWithBaseAddress(ThaModuleOcn self, AtSdhChannel channel, uint8 hwSlice, uint8 hwSts)
    {
    AtUnused(self);
    AtUnused(channel);
    AtUnused(hwSlice);
    AtUnused(hwSts);
    return 0;
    }

static AtIterator RegisterIteratorCreate(AtModule self)
    {
    return ThaModuleOcnRegisterIteratorCreate(self);
    }

static uint8 HwSliceOfLine(AtSdhLine line)
    {
    return (uint8)ThaModuleSdhLineLocalId(line) / 4;
    }

static uint8 Oc3LineToPointerSts3Get(AtSdhLine line)
    {
    uint32 regVal;
    uint8 lineId;
    uint8 sts3Id;

    /* Read the OCN Rx Line OC-3 line to Pointer STS-3 Control register */
    regVal = mChannelHwRead(line, cThaRegOcnTxSts3SrcOfTxLineCtrl + ChannelPartOffset((AtSdhChannel)line), cThaModuleOcn);

    /* Set configuration of line type */
    lineId = (uint8)ThaModuleSdhLineLocalId(line);
    mFieldGet(regVal,
              cThaOcnRxOc3LinetoSts3Mask(lineId),
              cThaOcnRxOc3LinetoSts3Shift(lineId),
              uint8,
              &sts3Id);

    return sts3Id % 4; /* Local STS-3 in one STS-12 slice */
    }

static eAtRet StsIdSw2Hw(ThaModuleOcn self, AtSdhChannel channel, eAtModule moduleId, uint8 sts1Id, uint8 *pHwSliceId, uint8 *pHwStsId)
    {
    eAtSdhLineRate rate;
    AtSdhLine line = AtSdhChannelLineObjectGet(channel);
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(channel);

    AtUnused(self);
    AtUnused(moduleId);

    /* Get hardware slice identifier */
    *pHwSliceId = HwSliceOfLine(line);

    rate = AtSdhLineRateGet(line);

    /* Special case with VC4-4C */
    if ((channelType == cAtSdhChannelTypeAug4)   ||
        (channelType == cAtSdhChannelTypeAu4_4c) ||
        (channelType == cAtSdhChannelTypeVc4_4c))
        {
        *pHwStsId = sts1Id;
        return cAtOk;
        }

    /* OC-12 rate */
    if (rate == cAtSdhLineRateStm4)
        {
        *pHwStsId = (uint8)(((sts1Id % 3) * 4) + (sts1Id / 3));
        return cAtOk;
        }

    /* OC-3 rate: 1,2,3 is STS-12 0,4,8 */
    *pHwStsId = (uint8)((sts1Id * 4) + Oc3LineToPointerSts3Get(line));

    return cAtOk;
    }

static eAtRet PwTohByteEnable(ThaModuleOcn self, AtPw pw, const tThaTohByte *tohByte, eBool enable)
    {
    AtSdhLine line = (AtSdhLine)AtPwBoundCircuitGet(pw);
    uint32 address, regVal;
	AtUnused(self);

    address = cThaRegOcnTxFrmrPerChnCtrl1 + AtChannelHwIdGet((AtChannel)line) + ChannelPartOffset((AtSdhChannel)line);
    regVal = mChannelHwRead(line, address, cThaModuleOcn);

    if ((tohByte->ohByte == cAtSdhLineMsOverheadByteS1) && (tohByte->sts1 == 0))
        mRegFieldSet(regVal, cThaOcnTxS1EnB, mBoolToBin(enable));

    if (((tohByte->ohByte == cAtSdhLineMsOverheadByteK1) || (tohByte->ohByte == cAtSdhLineMsOverheadByteK2)) &&
        (tohByte->sts1 == 0))
        mRegFieldSet(regVal, cThaOcnTxApsEnB, mBoolToBin(enable));

    if ((tohByte->ohByte == cAtSdhLineMsOverheadByteK2) && (tohByte->sts1 == 0))
        mRegFieldSet(regVal, cThaOcnTxRdiLEnB, mBoolToBin(enable));

    if ((tohByte->ohByte == cAtSdhLineMsOverheadByteM1) && (tohByte->sts1 == 2))
        mRegFieldSet(regVal, cThaOcnTxReiLEnB, mBoolToBin(enable));

    if ((tohByte->ohByte == cAtSdhLineRsOverheadByteJ0) && (tohByte->sts1 == 0))
        mRegFieldSet(regVal, cThaOcnTxZ0Md, (enable) ? 3 : 0);

    if ((tohByte->ohByte == cAtSdhLineRsOverheadByteB1) && (tohByte->sts1 == 0))
        mRegFieldSet(regVal, cThaOcnTxAutoB1EnB, mBoolToBin(enable));

    if (tohByte->ohByte == cAtSdhLineMsOverheadByteB2)
        mRegFieldSet(regVal, cThaOcnTxAutoB1EnB, mBoolToBin(enable));

    if (tohByte->ohByte == cAtSdhLineRsOverheadByteA1)
        mRegFieldSet(regVal, cThaOcnTxAutoA1A2EnB, mBoolToBin(enable));

    if (tohByte->ohByte == cAtSdhLineRsOverheadByteA2)
        mRegFieldSet(regVal, cThaOcnTxAutoA1A2EnB, mBoolToBin(enable));

    mChannelHwWrite(line, address, regVal, cThaModuleOcn);
    return cAtOk;
    }

static eAtRet PwTohSourceSet(ThaModuleOcn self, AtPw pw, AtSdhLine circuit)
    {
    uint32 address = cThaRegOcnTxFrmTohSrcCtrl + ChannelPartOffset((AtSdhChannel)circuit);
    uint8 lineId = (uint8)AtChannelHwIdGet((AtChannel)circuit);
    uint32 regVal = mChannelHwRead(circuit, address, cThaModuleOcn);
	AtUnused(pw);
	AtUnused(self);

    mFieldIns(&regVal, cThaRegOcnTxTohLineSrcMask(lineId), cThaRegOcnTxTohLineSrcShift(lineId), lineId);
    mChannelHwWrite(circuit, address, regVal, cThaModuleOcn);
    return cAtOk;
    }

static void StsRegisterDisplay(ThaModuleOcn self, const char* regName, uint32 regBaseAddress, AtSdhChannel sdhChannel)
    {
    uint8 numSts, sts_i;
    uint32 offset, address;

    numSts = AtSdhChannelNumSts(sdhChannel);
    AtPrintc(cSevInfo, "   - %s:\r\n", regName);
    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        offset = mMethodsGet(self)->StsDefaultOffset(self, sdhChannel, (uint8)(AtSdhChannelSts1Get(sdhChannel) + sts_i));
        address = regBaseAddress + offset;
        AtPrintc(cSevNormal, "        0x%08x: 0x%08x\r\n", address, mChannelHwRead(sdhChannel, address, cThaModuleOcn));
        }
    }

static void StsVtRegisterDisplay(ThaModuleOcn self, const char* regName, uint32 regBaseAddress, AtSdhChannel sdhChannel)
    {
    uint8 vtg = AtSdhChannelTug2Get(sdhChannel);
    uint8 vt = AtSdhChannelTu1xGet(sdhChannel);
    uint8 numSts, sts_i;
    uint32 offset, address;

    numSts = AtSdhChannelNumSts(sdhChannel);
    AtPrintc(cSevInfo, "   - %s:\r\n", regName);
    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        offset = mMethodsGet(mThis(self))->StsVtDefaultOffset(mThis(self), sdhChannel, (uint8)(AtSdhChannelSts1Get(sdhChannel) + sts_i), vtg, vt);
        address = regBaseAddress + offset;
        AtPrintc(cSevNormal, "        0x%08x: 0x%08x\r\n", address, mChannelHwRead(sdhChannel, address, cThaModuleOcn));
        }
    }

static eBool SlaveStsIdIsValid(ThaModuleOcn self, AtSdhChannel channel, uint8 swSts)
    {
    uint8 masterSts = AtSdhChannelSts1Get(channel);
    AtUnused(self);
    if ((channel == NULL) || (swSts < masterSts) || (swSts - masterSts >= AtSdhChannelNumSts(channel)))
        return cAtFalse;
    return cAtTrue;
    }

static eAtRet ChannelStsIdSw2HwGet(ThaModuleOcn self, AtSdhChannel channel, eAtModule phyModule, uint8 swSts, uint8* sliceId, uint8 *hwStsInSlice)
    {
    uint8 sts_i;
    uint8 masterSts = AtSdhChannelSts1Get(channel);
    uint16 *allHwSts;

    AtUnused(phyModule);
    AtUnused(self);

    if (!SlaveStsIdIsValid(self, channel, swSts))
        return cAtErrorInvlParm;

    allHwSts = AtSdhChannelAllHwSts(channel);
    if (allHwSts == NULL)
        return cAtErrorRsrcNoAvail;

    /* Calculate if necessary */
    sts_i = (uint8)(swSts - masterSts);
    if (allHwSts[sts_i] == cBit15_0)
        {
        mMethodsGet(self)->StsIdSw2Hw(self, channel, phyModule, swSts, sliceId, hwStsInSlice);
        allHwSts[sts_i] = (uint16)(((uint32)(*sliceId << 7) | (*hwStsInSlice & cBit6_0)));
        return cAtOk;
        }

    /* Calculated, just return from database */
    *hwStsInSlice = (uint8)(allHwSts[sts_i] & cBit6_0);
    *sliceId = (uint8)(allHwSts[sts_i] >> 7);

    return cAtOk;
    }

static uint32 RxStsPiMastIdMask(ThaModuleOcn self)
    {
    AtUnused(self);
    return cThaRxStsPiMastIdMask;
    }

static uint32 ThaRxStsPiMastIdMask(AtSdhChannel channel)
    {
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)channel);
    if (ocnModule)
        return mMethodsGet(ocnModule)->RxStsPiMastIdMask(ocnModule);
    return 0;
    }

static eAtRet PiStsConcatSlaveSet(ThaModuleOcn self, AtSdhChannel channel, uint8 masterStsId, uint8 slaveStsId)
    {
    uint32 regAddr, regVal;
    uint8 mastSliceId, masterHwStsId;

    /* Get hardware IDs of master STS */
    ThaSdhChannelHwStsGet(channel, cThaModuleOcn, masterStsId, &mastSliceId, &masterHwStsId);

    /* Read the OCN RX STS PI Per Channel Control register */
    regAddr = cThaRegOcnRxStsPiPerChnCtrl + mMethodsGet(self)->StsDefaultOffset(self, channel, slaveStsId);
    regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);

    /* Set slaver indication and its master */
    mFieldIns(&regVal, cThaRxStsPiSlvIndMask, cThaRxStsPiSlvIndShift, 1);
    mFieldIns(&regVal, ThaRxStsPiMastIdMask(channel), cThaRxStsPiMastIdShift, masterHwStsId);

    /* It is slaver so disable check SS bit */
    mFieldIns(&regVal, cThaRxStsPiSsDetEnMask, cThaRxStsPiSsDetEnShift, 0);

    /* Apply */
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eAtRet PiStsConcatMasterSet(ThaModuleOcn self, AtSdhChannel channel, uint8 masterStsId)
    {
    uint32 regAddr, regVal;
    uint8 hwSlice, mastHwSts;

    ThaSdhChannelHwStsGet(channel, cThaModuleOcn, masterStsId, &hwSlice, &mastHwSts);

    /* Set configuration for the master STS */
    /* Read the OCN RX STS PI Per Channel Control register */
    regAddr = cThaRegOcnRxStsPiPerChnCtrl + mMethodsGet(self)->StsDefaultOffset(self, channel, masterStsId);
    regVal = mChannelHwRead(channel, regAddr, cThaModuleOcn);

    /* Make it be master of itself */
    mFieldIns(&regVal, ThaRxStsPiMastIdMask(channel), cThaRxStsPiMastIdShift, mastHwSts);
    mFieldIns(&regVal, cThaRxStsPiSlvIndMask, cThaRxStsPiSlvIndShift, 0);

    /* Write configuration to that register */
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static uint8 NumSts1sInLineOfChannel(AtSdhChannel channel)
    {
    AtSdhLine line = AtSdhChannelLineObjectGet(channel);
    eAtSdhLineRate rate = AtSdhLineRateGet(line);
    
    if (rate == cAtSdhLineRateStm0)
        return 1;
    if (rate == cAtSdhLineRateStm1)
        return 3;
    if (rate == cAtSdhLineRateStm4)
        return 12;
    if (rate == cAtSdhLineRateStm16)
        return 48;
    if (rate == cAtSdhLineRateStm64)
        return 192;
    return 0;
    }

static eAtRet TerPgStsConcatMasterSet(ThaModuleOcn self, AtSdhChannel channel, uint8 masterStsId)
    {
    AtUnused(self);
    AtUnused(channel);
    AtUnused(masterStsId);
    return cAtOk;
    }

static eAtRet PgStsConcatMasterSet(ThaModuleOcn self, AtSdhChannel channel, uint8 masterStsId)
    {
    uint32 regAddr, regVal;
    uint8 slice, mastHwSts;

    ThaSdhChannelHwStsGet(channel, cThaModuleOcn, masterStsId, &slice, &mastHwSts);

    /* Configure OCN Tx PG Slide 2 Per Channel Control register */
    regAddr = cThaRegOcnTxPGPerChnCtrl + mMethodsGet(self)->StsVtDefaultOffset(self, channel, masterStsId, 0, 0);
    regVal = mChannelHwRead(channel, regAddr, cThaModuleOcn);

    /* Set slaver indication and its master */
    /* This field is only used when VC-4 is not channelized */
    mFieldIns(&regVal, cThaTxPgStsSlvIndMask, cThaTxPgStsSlvIndShift, 0);
    mFieldIns(&regVal, cThaTxPgStsMastIdMask, cThaTxPgStsMastIdShift, mastHwSts);

    /* Apply */
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);

    /* Configure the OCN TX POH Insertion Slide 1 Per Channel Control register */
    regAddr = cThaRegOcnTxPohInsPerChnCtrl + mMethodsGet(self)->StsDefaultOffset(self, channel, masterStsId);
    regVal = mChannelHwRead(channel, regAddr, cThaModuleOcn);
    mFieldIns(&regVal, cThaTxPg2StsSlvIndMask, cThaTxPg2StsSlvIndShift, 0);
    mFieldIns(&regVal, cThaTxPg2StsMastIDMask, cThaTxPg2StsMastIDShift, mastHwSts);

    /* Apply */
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);
    return cAtOk;
    }

static eAtRet TerPgStsConcatSlaveSet(ThaModuleOcn self, AtSdhChannel channel, uint8 masterStsId, uint8 slaveStsId)
    {
    AtUnused(self);
    AtUnused(channel);
    AtUnused(masterStsId);
    AtUnused(slaveStsId);
    return cAtOk;
    }

static eAtRet PgStsConcatSlaveSet(ThaModuleOcn self, AtSdhChannel channel, uint8 masterStsId, uint8 slaveStsId)
    {
    uint32 regAddr, regVal;
    uint8 slice, mastHwSts;

    ThaSdhChannelHwStsGet(channel, cThaModuleOcn, masterStsId, &slice, &mastHwSts);

    /* Configure OCN Tx PG Slide 2 Per Channel Control register */
    regAddr = cThaRegOcnTxPGPerChnCtrl + mMethodsGet(self)->StsVtDefaultOffset(self, channel, slaveStsId, 0, 0);
    regVal = mChannelHwRead(channel, regAddr, cThaModuleOcn);

    /* Set slaver indication and its master */
    /* This field is only used when VC-4 is not channelized */
    mFieldIns(&regVal, cThaTxPgStsSlvIndMask, cThaTxPgStsSlvIndShift, 1);
    mFieldIns(&regVal, cThaTxPgStsMastIdMask, cThaTxPgStsMastIdShift, mastHwSts);
    mFieldIns(&regVal, cThaTxPg2StsSSInsEnMask, cThaTxPg2StsSSInsEnShift, 0);

    /* Apply */
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);

    /* Configure the OCN Tx POH Insertion Slide 1 Per Channel Control register */
    regAddr = cThaRegOcnTxPohInsPerChnCtrl + mMethodsGet(self)->StsDefaultOffset(self, channel, slaveStsId);
    regVal = mChannelHwRead(channel, regAddr, cThaModuleOcn);
    mFieldIns(&regVal, cThaTxPg2StsSlvIndMask, cThaTxPg2StsSlvIndShift, 1);
    mFieldIns(&regVal, cThaTxPg2StsMastIDMask, cThaTxPg2StsMastIDShift, mastHwSts);

    /* Apply */
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);
    return cAtOk;
    }

static eBool Sts1IsValid(AtSdhChannel channel, uint8 stsId)
    {
    return (stsId < NumSts1sInLineOfChannel(channel)) ? cAtTrue : cAtFalse;
    }

static eAtRet Vc3InTu3PayloadSet(AtSdhChannel vc3, eThaOcnVc3PldType vc3Pld)
    {
    uint8 stsId = AtSdhChannelSts1Get(vc3);
    AtModulePdh pdhModule;
    uint8 hwSlice, hwSts;
    eAtRet ret = cAtOk;
    AtDevice device = AtChannelDeviceGet((AtChannel)vc3);
    ThaModuleCdr cdrModule = (ThaModuleCdr)AtDeviceModuleGet(device, cThaModuleCdr);
    eBool vc3ModeEnabled = cAtFalse;

    ThaSdhChannelHwStsGet(vc3, cThaModuleOcn, stsId, &hwSlice, &hwSts);
    if ((AtChannelBoundPwGet((AtChannel)vc3) != NULL) || (vc3Pld == cThaOcnVc3PldC3) || (vc3Pld == cThaOcnTu3Vc3Pld))
        vc3ModeEnabled = cAtTrue;

    ret |= ThaCdrVc3CepModeEnable(cdrModule, vc3, stsId, vc3ModeEnabled);

    /* Configure PDH mapping */
    pdhModule = (AtModulePdh)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)vc3), cAtModulePdh);
    if (vc3Pld == cThaOcnVc3PldDs3)
        ret |= ThaPdhStsMapMdSet(pdhModule, vc3, cThaStsMapDs3Tu3Md);
    if (vc3Pld == cThaOcnVc3PldE3)
        ret |= ThaPdhStsMapMdSet(pdhModule, vc3, cThaStsMapE3Tu3Md);

    return ret;
    }

static eAtRet HwStsTxPayloadTypeSet(AtSdhChannel channel, uint8 slice, uint8 hwStsId, eThaStsPayloadType payloadType)
    {
    uint32 regAddr, regVal, offset;
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)channel);
    if (ocnModule == NULL)
        return cAtErrorNullPointer;

    offset  = LocalStsInSliceOffset(channel, slice, hwStsId);
    regAddr = (uint32)mMethodsGet(ocnModule)->TxStsMultiplexingControl(ocnModule, channel) + offset;
    regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);
    mFieldIns(&regVal, cThaOcnTxPgStsDemuxSpeTypeMask, cThaOcnTxPgStsDemuxSpeTypeShift, payloadType);
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eAtRet HwStsRxPayloadTypeSet(AtSdhChannel channel, uint8 slice, uint8 hwStsId, eThaStsPayloadType type)
    {
    uint32 regAddr, regVal, offset;
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)channel);
    if (ocnModule == NULL)
        return cAtErrorNullPointer;

    offset = LocalStsInSliceOffset(channel, slice, hwStsId);

    /* For RX */
    regAddr = mMethodsGet(ocnModule)->RxStsPayloadControl(ocnModule, channel) + offset;
    regVal = mChannelHwRead(channel, regAddr, cThaModuleOcn);
    mFieldIns(&regVal, cThaOcnRxPpStsDemuxSpeTypeMask, cThaOcnRxPpStsDemuxSpeTypeShift, type);
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eAtRet HwStsPayloadTypeSet(AtSdhChannel channel, uint8 slice, uint8 hwStsId, eThaStsPayloadType type)
    {
    eAtRet ret = cAtOk;

    ret |= HwStsTxPayloadTypeSet(channel, slice, hwStsId, type);
    ret |= HwStsRxPayloadTypeSet(channel, slice, hwStsId, type);

    return ret;
    }

static eAtRet Au3VcHwPayloadTypeSet(ThaModuleOcn self, AtSdhChannel vc3, eAtSdhVcMapType mapType)
    {
    uint8 hwSlice, hwSts;
    ThaSdhChannel2HwMasterStsId(vc3, cThaModuleOcn, &hwSlice, &hwSts);

    AtUnused(self);

    if (mapType == cAtSdhVcMapTypeVc3Map7xTug2s)
        HwStsPayloadTypeSet(vc3, hwSlice, hwSts, cThaStsVc3ContainVt1x);
    else
        HwStsPayloadTypeSet(vc3, hwSlice, hwSts, cThaStsDontCareVtTu);

    return cAtOk;
    }

static eAtRet Vc3InAu3PayloadSet(ThaModuleOcn self, AtSdhChannel vc3, eThaOcnVc3PldType vc3Pld)
    {
    AtModulePdh pdhModule;
    uint8 hwSlice, hwSts;
    AtDevice device = AtChannelDeviceGet((AtChannel)vc3);
    ThaModuleCdr cdrModule = (ThaModuleCdr)AtDeviceModuleGet(device, cThaModuleCdr);
    eAtSdhVcMapType mapType = (vc3Pld == cThaOcnVc3Pld7Tug2) ? cAtSdhVcMapTypeVc3Map7xTug2s : cAtSdhVcMapTypeVc3MapC3;

    ThaSdhChannel2HwMasterStsId(vc3, cThaModuleCdr, &hwSlice, &hwSts);
    ThaCdrHwStsVc3CepModeEnable(cdrModule, vc3, hwSlice, hwSts, cAtFalse);

    mMethodsGet(self)->Au3VcHwPayloadTypeSet(self, vc3, mapType);

    /* Set payload for this STS at PDH module */
    pdhModule = (AtModulePdh)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)vc3), cAtModulePdh);
    if (vc3Pld == cThaOcnVc3PldDs3)
        return ThaPdhStsMapMdSet(pdhModule, vc3, cThaStsMapDs3Vc3Md);

    if (vc3Pld == cThaOcnVc3PldE3)
        return ThaPdhStsMapMdSet(pdhModule, vc3, cThaStsMapE3Vc3Md);

    return cAtOk;
    }

static eAtRet HwStsPohInsertEnable(AtSdhChannel channel, uint8 slice, uint8 hwStsId, eBool enable)
    {
    uint32 regAddr, regVal, offset;

    offset = LocalStsVtInSliceOffset(channel, slice, hwStsId, 0, 0);
    regAddr = cThaRegOcnTxPGPerChnCtrl + offset;
    regVal = mChannelHwRead(channel, regAddr, cThaModuleOcn);
    mFieldIns(&regVal, cThaTxPgStsVtPohInsEnMask, cThaTxPgStsVtPohInsEnShift, enable ? 1 : 0);
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eAtRet HwSts1PayloadInit(AtSdhChannel channel, uint8 slice, uint8 hwStsId)
    {
    eAtRet ret = cAtOk;
    AtDevice device = AtChannelDeviceGet((AtChannel)channel);
    ThaModuleCdr cdrModule = (ThaModuleCdr)AtDeviceModuleGet(device, cThaModuleCdr);

    /* Init POH */
    HwStsPohInsertEnable(channel, slice, hwStsId, cAtTrue);

    /* Init pointer processor */
    HwStsPayloadTypeSet(channel, slice, hwStsId, cThaStsDontCareVtTu);

    /* Init CDR */
    ret |= ThaCdrHwStsPldSet(cdrModule, channel, slice, hwStsId, cThaOcnVc3PldC3);
    ret |= ThaCdrHwStsVc3CepModeEnable(cdrModule, channel, slice, hwStsId, cAtFalse);

    return ret;
    }

/* Return True if stsId is a slaver of masterStsId */
static eBool PiStsConcatRelease(AtSdhChannel channel, uint8 stsHwSlice, uint8 masterHwSts, uint8 hwSts)
    {
    uint8 fieldVal;
    eBool isSlaver;
    uint32 regAddr, regVal;

    regAddr = cThaRegOcnRxStsPiPerChnCtrl + LocalStsInSliceOffset(channel, stsHwSlice, hwSts);
    regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);

    /* Check if it is slaver STS of this concatenation */
    isSlaver = (regVal & cThaRxStsPiSlvIndMask) ? cAtTrue : cAtFalse;
    if (!isSlaver)
        return cAtFalse;

    /* Check if this is slaver of this master STS */
    mFieldGet(regVal, ThaRxStsPiMastIdMask(channel), cThaRxStsPiMastIdShift, uint8, &fieldVal);
    if (fieldVal != masterHwSts)
        return cAtFalse;

    /* Make slaver be master of itself */
    mFieldIns(&regVal, cThaRxStsPiSlvIndMask, cThaRxStsPiSlvIndShift, 0);
    mFieldIns(&regVal, ThaRxStsPiMastIdMask(channel), cThaRxStsPiMastIdShift, hwSts);
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);

    return cAtTrue;
    }

static eAtRet TerPgStsConcatRelease(ThaModuleOcn self, AtSdhChannel channel, uint8 stsHwSlice, uint8 masterHwSts, uint8 hwSts)
    {
    AtUnused(self);
    AtUnused(channel);
    AtUnused(stsHwSlice);
    AtUnused(masterHwSts);
    AtUnused(hwSts);
    return cAtOk;
    }

/* Return True if stsId is a slaver of masterStsId */
static eBool PgStsConcatRelease(AtSdhChannel channel, uint8 stsHwSlice, uint8 masterHwSts, uint8 hwSts)
    {
    uint8 fieldVal;
    eBool isSlaver;
    uint32 regAddr, regVal;

    /* This register is computed by sts, vtg, vt but don't care VTG and VT in this case */
    regAddr = cThaRegOcnTxPGPerChnCtrl + LocalStsVtInSliceOffset(channel, stsHwSlice, hwSts, 0, 0);
    regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);

    /* Check if it is slaver STS of this concatenation */
    isSlaver = (regVal & cThaTxPgStsSlvIndMask) ? cAtTrue : cAtFalse;
    if (!isSlaver)
        return cAtFalse;

    /* Check if this is slaver of master STS */
    mFieldGet(regVal, cThaTxPgStsMastIdMask, cThaTxPgStsMastIdShift, uint8, &fieldVal);
    if (fieldVal != masterHwSts)
        return cAtFalse;

    /* Set slaver indication and its master */
    /* This field is only used when VC-4 is not channelized */
    mFieldIns(&regVal, cThaTxPgStsSlvIndMask, cThaTxPgStsSlvIndShift, 0);
    mFieldIns(&regVal, cThaTxPgStsMastIdMask, cThaTxPgStsMastIdShift, hwSts);
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);

    regAddr = cThaRegOcnTxPohInsPerChnCtrl + LocalStsInSliceOffset(channel, stsHwSlice, hwSts);
    regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);
    mFieldIns(&regVal, cThaTxPg2StsSlvIndMask, cThaTxPg2StsSlvIndShift, 0);
    mFieldIns(&regVal, cThaTxPg2StsMastIDMask, cThaTxPg2StsMastIDShift, hwSts);
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);

    return cAtTrue;
    }

static eAtRet StsConcatRelease(ThaModuleOcn self, AtSdhChannel channel, uint8 sts1Id)
    {
    uint8 hwSts_i;
    eAtRet ret;
    uint8 slice, hwSts;
    uint8 numSts1sToRelease;
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)channel);

    AtUnused(self);

    ret = ThaSdhChannelHwStsGet(channel, cThaModuleOcn, sts1Id, &slice, &hwSts);
    if (ret != cAtOk)
        return ret;

    numSts1sToRelease = mMethodsGet(ocnModule)->NumStsInOneSlice(ocnModule);
    for (hwSts_i = 0; hwSts_i < numSts1sToRelease; hwSts_i++)
        {
        eBool isSlave = cAtFalse;

        isSlave |= PiStsConcatRelease(channel, slice, hwSts, hwSts_i);
        isSlave |= PgStsConcatRelease(channel, slice, hwSts, hwSts_i);
        mMethodsGet(ocnModule)->TerPgStsConcatRelease(ocnModule, channel, slice, hwSts, hwSts_i);
        if (isSlave)
            ret |= HwSts1PayloadInit(channel, slice, hwSts_i);
        }

    /* Also initialize the master */
    ret |= HwSts1PayloadInit(channel, slice, hwSts);

    return ret;
    }
static eAtRet RxVtgPdhReset(ThaModuleOcn self, AtSdhChannel tug2, uint8 stsId, uint8 vtgId, eThaOcnVtTuType vtgTypeCfg)
    {
    AtUnused(self);
    AtUnused(tug2);
    AtUnused(stsId);
    AtUnused(vtgId);
    AtUnused(vtgTypeCfg);
    return cAtOk;
    }
static eAtRet RxVtgSet(ThaModuleOcn self, AtSdhChannel tug2, uint8 stsId, uint8 vtgId, eThaOcnVtTuType vtgTypeCfg)
    {
    uint32 regAddr, regVal;

    regAddr = mMethodsGet(self)->RxStsPayloadControl(self, tug2) + mMethodsGet(self)->StsDefaultOffset(self, tug2, stsId);
    regVal = mChannelHwRead(tug2, regAddr, cThaModuleOcn);
    mFieldIns(&regVal,
              cThaOcnRxPpStsDemuxTug2TypeMask(vtgId),
              cThaOcnRxPpStsDemuxTug2TypeShift(vtgId),
              ThaModuleOcnVtTuTypeSw2Hw(vtgTypeCfg));
    mChannelHwWrite(tug2, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }
static eAtRet TxVtgPdhReset(ThaModuleOcn self, AtSdhChannel tug2, uint8 stsId, uint8 vtgId, eThaOcnVtTuType vtgTypeCfg)
    {
    AtUnused(self);
    AtUnused(tug2);
    AtUnused(stsId);
    AtUnused(vtgId);
    AtUnused(vtgTypeCfg);
    return cAtOk;
    }
static eAtRet TxVtgSet(ThaModuleOcn self, AtSdhChannel tug2, uint8 stsId, uint8 vtgId, eThaOcnVtTuType vtgTypeCfg)
    {
    uint32 regAddr, regVal;

    regAddr = mMethodsGet(self)->TxStsMultiplexingControl(self, tug2) + mMethodsGet(self)->StsDefaultOffset(self, tug2, stsId);
    regVal = mChannelHwRead(tug2, regAddr, cThaModuleOcn);
    mFieldIns(&regVal,
              cThaOcnTxPgStsDemuxTug2TypeMask(vtgId),
              cThaOcnTxPgStsDemuxTug2TypeShift(vtgId),
              ThaModuleOcnVtTuTypeSw2Hw(vtgTypeCfg));
    mChannelHwWrite(tug2, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eAtRet PpSsTxVtSet(ThaModuleOcn self,
                          AtSdhChannel channel,
                          uint8 stsId,
                          uint8 vtgId,
                          uint8 vtId,
                          uint8 insSsBit,
                          eBool insSsBitEn)
    {
    uint32 regAddr = cThaRegOcnTxPGPerChnCtrl + mMethodsGet(self)->StsVtDefaultOffset(self, channel, stsId, vtgId, vtId);
    uint32 regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);

    /* Set SS bit value that will be inserted */
    mFieldIns(&regVal,
              cThaTxPg1Sts_Vt_TuSSInsPatMask,
              cThaTxPg1Sts_Vt_TuSSInsPatShift,
              insSsBit);

    /* Set SS bit insertion */
    mFieldIns(&regVal,
              cThaTxPg1Sts_Vt_TuSSInsEnMask,
              cThaTxPg1Sts_Vt_TuSSInsEnShift,
              mBoolToBin(insSsBitEn));

    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static uint8 PpSsTxVtGet(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId, uint8 vtgId, uint8 vtId)
    {
    uint32 regAddr = cThaRegOcnTxPGPerChnCtrl + mMethodsGet(self)->StsVtDefaultOffset(self, channel, stsId, vtgId, vtId);
    uint32 regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);
    return (uint8)mRegField(regVal, cThaTxPg1Sts_Vt_TuSSInsPat);
    }

static void Vc4SlaverIndicate(AtSdhChannel channel,
                              uint8 stsId,
                              eBool isSlaver,
                              eBool pohInsertEnable,
                              uint8 masterHwSts)
    {
    uint32 regAddr, regVal;
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)channel);
    if (ocnModule == NULL)
        return;

    regAddr = cThaRegOcnTxPGPerChnCtrl + mMethodsGet(ocnModule)->StsVtDefaultOffset(ocnModule, channel, stsId, 0, 0);
    regVal = mChannelHwRead(channel, regAddr, cThaModuleOcn);
    mFieldIns(&regVal, cThaTxPgStsVtPohInsEnMask, cThaTxPgStsVtPohInsEnShift, pohInsertEnable ? 1 : 0);
    mFieldIns(&regVal, cThaTxPgStsSlvIndMask, cThaTxPgStsSlvIndShift, isSlaver ? 1 : 0);
    mFieldIns(&regVal, cThaTxPgStsMastIdMask, cThaTxPgStsMastIdShift, masterHwSts);
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);
    }

static void Vc4StsPayloadDefault(AtSdhChannel channel, uint8 stsId, eThaStsPayloadType stsPayloadType)
    {
    uint8 slice, hwSts;
    ThaSdhChannelHwStsGet(channel, cThaModuleOcn, stsId, &slice, &hwSts);
    HwStsPayloadTypeSet(channel, slice, hwSts, stsPayloadType);
    }

static eAtRet HwStsRxTermEnable(AtSdhChannel channel, uint8 slice, uint8 hwSts, eBool enable)
    {
    uint32 regAddr, regVal;

    regAddr = cThaRegOcnRxStsPiPerChnCtrl + LocalStsInSliceOffset(channel, slice, hwSts);
    regVal = mChannelHwRead(channel, regAddr, cThaModuleOcn);
    mFieldIns(&regVal,
              cThaRxStsPiStsTermEnMask,
              cThaRxStsPiStsTermEnShift,
              mBoolToBin(enable));
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eThaOcnVc3PldType Vc3InTu3PayloadGet(AtSdhChannel vc3)
    {
    AtModulePdh pdhModule;
    eThaStsMapMd pdhStsMapMode;

    pdhModule = (AtModulePdh)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)vc3), cAtModulePdh);
    pdhStsMapMode = ThaPdhStsMapMdGet(pdhModule, vc3);

    if (pdhStsMapMode == cThaStsMapE3Tu3Md)
        return cThaOcnVc3PldE3;

    if (pdhStsMapMode == cThaStsMapDs3Tu3Md)
        return cThaOcnVc3PldDs3;

    return cThaOcnVc3PldC3;
    }

static eThaOcnVc3PldType Vc3InAu3PayloadGet(AtSdhChannel vc3)
    {
    uint8 stsId = AtSdhChannelSts1Get(vc3);
    AtModulePdh pdhModule;
    eThaStsMapMd pdhStsMapMode;

    if (ThaOcnStsRxPayloadTypeGet(vc3, stsId) == cThaStsVc3ContainVt1x)
        return cThaOcnVc3Pld7Tug2;

    pdhModule = (AtModulePdh)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)vc3), cAtModulePdh);
    pdhStsMapMode = ThaPdhStsMapMdGet(pdhModule, vc3);

    if (pdhStsMapMode == cThaStsMapE3Vc3Md)
        return cThaOcnVc3PldE3;

    if (pdhStsMapMode == cThaStsMapDs3Vc3Md)
        return cThaOcnVc3PldDs3;

    return cThaOcnVc3PldC3;
    }

static eBool StsIsSlaveInChannel(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId)
    {
    uint32 regAddr, regVal;
    uint8 mastSliceId, masterHwStsId, stsHwSlice, hwSts, fieldVal;
    eBool isSlave;

    AtUnused(self);

    /* Get hardware IDs of master STS */
    if (ThaSdhChannel2HwMasterStsId(channel, cThaModuleOcn, &mastSliceId, &masterHwStsId) != cAtOk)
        return cAtFalse;
    ThaSdhChannelHwStsGet(channel, cThaModuleOcn, stsId, &stsHwSlice, &hwSts);

    regAddr = cThaRegOcnRxStsPiPerChnCtrl + LocalStsInSliceOffset(channel, stsHwSlice, hwSts);
    regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);

    /* Check if it is slaver STS of this concatenation */
    isSlave = (regVal & cThaRxStsPiSlvIndMask) ? cAtTrue : cAtFalse;
    if (!isSlave)
        return cAtFalse;

    /* Check if this is slaver of this master STS */
    mFieldGet(regVal, ThaRxStsPiMastIdMask(channel), cThaRxStsPiMastIdShift, uint8, &fieldVal);
    if (fieldVal == masterHwStsId)
        return cAtTrue;

    return cAtFalse;
    }

static uint8 RxVtgGet(AtSdhChannel channel, uint8 stsId, uint8 vtgId)
    {
    uint32 regAddr, regVal;
    uint8 val;

    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)channel);
    if (ocnModule == NULL)
        return cAtErrorNullPointer;

    regAddr = mMethodsGet(ocnModule)->RxStsPayloadControl(ocnModule, channel) + mMethodsGet(ocnModule)->StsDefaultOffset(ocnModule, channel, stsId);
    regVal = mChannelHwRead(channel, regAddr, cThaModuleOcn);
    mFieldGet(regVal, cThaOcnRxPpStsDemuxTug2TypeMask(vtgId), cThaOcnRxPpStsDemuxTug2TypeShift(vtgId), uint8, &val);

    return val;
    }

static uint8 TxVtgGet(AtSdhChannel channel, uint8 stsId, uint8 vtgId)
    {
    uint32 regAddr, regVal;
    uint8 val;
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)channel);
    if (ocnModule == NULL)
        return cAtErrorNullPointer;

    regAddr = mMethodsGet(ocnModule)->TxStsMultiplexingControl(ocnModule, channel) + mMethodsGet(ocnModule)->StsDefaultOffset(ocnModule, channel, stsId);
    regVal = mChannelHwRead(channel, regAddr, cThaModuleOcn);
    mFieldGet(regVal, cThaOcnTxPgStsDemuxTug2TypeMask(vtgId), cThaOcnTxPgStsDemuxTug2TypeShift(vtgId), uint8, &val);

    return val;
    }

static uint8 VtTuTypeHw2Sw(uint8 hwType)
    {
    if (hwType == 0) return (uint8)cThaOcnVtgTug2PldVt15;
    if (hwType == 1) return (uint8)cThaOcnVtgTug2PldVt2;

    return 0xFF;
    }

static void SdhChannelRegsShow(ThaModuleOcn self, AtSdhChannel sdhChannel)
    {
    AtPrintc(cSevInfo, "* OCN registers of channel '%s':\r\n", AtObjectToString((AtObject)sdhChannel));
    StsRegisterDisplay(self, "OCN Rx STS PI Per Channel Control", cThaRegOcnRxStsPiPerChnCtrl, sdhChannel);
    StsRegisterDisplay(self, "OCN Rx PP Per STS/VC Payload Control", mMethodsGet(self)->RxStsPayloadControl(self, sdhChannel), sdhChannel);
    StsVtRegisterDisplay(self, "OCN VT/TU PI Per Channel Control", cThaRegOcnVtTUPIPerChnCtrl, sdhChannel);

    StsRegisterDisplay(self, "OCN Tx PG Demux Per STS/VC Payload Control", mMethodsGet(self)->TxStsMultiplexingControl(self, sdhChannel), sdhChannel);
    StsVtRegisterDisplay(self, "OCN Tx PG Per Channel Control", cThaRegOcnTxPGPerChnCtrl, sdhChannel);
    StsRegisterDisplay(self, "OCN Tx POH Insertion Per Channel Control", cThaRegOcnTxPohInsPerChnCtrl, sdhChannel);

    AtPrintc(cSevNormal, "\r\n");
    }

static eAtRet Tug3HwPayloadTypeSet(ThaModuleOcn self, AtSdhChannel tug3, eAtSdhTugMapType tug3MapType)
    {
    uint8 slice, hwStsId;
    eAtRet ret = ThaSdhChannel2HwMasterStsId(tug3, cThaModuleOcn, &slice, &hwStsId);
    if (ret != cAtOk)
        return ret;

    AtUnused(self);
    if (tug3MapType == cAtSdhTugMapTypeTug3Map7xTug2s)
        HwStsPayloadTypeSet(tug3, slice, hwStsId, cThaStsTug3ContainVt1x);

    if (tug3MapType == cAtSdhTugMapTypeTug3MapVc3)
        HwStsPayloadTypeSet(tug3, slice, hwStsId, cThaStsTug3ContainTu3);

    return cAtOk;
    }

static eAtRet Tug3PldSet(ThaModuleOcn self, AtSdhChannel tug3, eAtSdhTugMapType tug3MapType)
    {
    eAtRet ret = cAtOk;
    uint8 slice, hwSts;
    AtDevice device = AtChannelDeviceGet((AtChannel)tug3);
    ThaModuleCdr cdrModule = (ThaModuleCdr)AtDeviceModuleGet(device, cThaModuleCdr);

    AtUnused(self);

    ret = ThaSdhChannel2HwMasterStsId(tug3, cThaModuleCdr, &slice, &hwSts);
    if (ret != cAtOk)
        return ret;

    ret |= mMethodsGet(self)->Tug3HwPayloadTypeSet(self, tug3, tug3MapType);
    
    if (tug3MapType == cAtSdhTugMapTypeTug3Map7xTug2s)
        ret |= ThaCdrHwStsPldSet(cdrModule, tug3, slice, hwSts, cThaOcnVc3Pld7Tug2);

    if (tug3MapType == cAtSdhTugMapTypeTug3MapVc3)
        ret |= ThaCdrHwStsPldSet(cdrModule, tug3, slice, hwSts, cThaOcnTu3Vc3Pld);
        
    ret |= ThaModuleOcnRxStsTermEnable(tug3, AtSdhChannelSts1Get(tug3), cAtTrue);
    ret |= ThaCdrHwStsVc3CepModeEnable(cdrModule, tug3, slice, hwSts, cAtFalse);

    return ret;
    }

static void VtPohInsertEnable(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId, uint8 vtgId, uint8 vtId, eBool enable)
    {
    uint32 regAddr, regVal;

    regAddr = cThaRegOcnTxPGPerChnCtrl + mMethodsGet(self)->StsVtDefaultOffset(self, channel, stsId, vtgId, vtId);
    regVal = mChannelHwRead(channel, regAddr, cThaModuleOcn);
    mFieldIns(&regVal, cThaTxPgStsVtPohInsEnMask, cThaTxPgStsVtPohInsEnShift, enable ? 1 : 0);
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);
    }

static eBool VtPohInsertIsEnabled(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId, uint8 vtgId, uint8 vtId)
    {
    uint32 regAddr, regVal;

    regAddr = cThaRegOcnTxPGPerChnCtrl + mMethodsGet(self)->StsVtDefaultOffset(self, channel, stsId, vtgId, vtId);
    regVal = mChannelHwRead(channel, regAddr, cThaModuleOcn);
    return mBinToBool(mRegField(regVal, cThaTxPgStsVtPohInsEn));
    }

static void VtTerminate(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId, uint8 vtgId, uint8 vtId, eBool terminate)
    {
    uint32 regAddr = cThaRegOcnVtTUPIPerChnCtrl + mMethodsGet(self)->StsVtDefaultOffset(self, channel, stsId, vtgId, vtId);
    uint32 regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);
    mFieldIns(&regVal, cThaRxStsPiVtTermEnMask, cThaRxStsPiVtTermEnShift, terminate ? 1 : 0);
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);
    }

static uint32 VtDetaultOffset(ThaModuleOcn self, AtSdhPath vtTu)
    {
    AtSdhChannel channel = (AtSdhChannel)vtTu;
    uint8 stsId = AtSdhChannelSts1Get(channel);
    uint8 vtgId = AtSdhChannelTug2Get(channel);
    uint8 vtId  = AtSdhChannelTu1xGet(channel);
    return mMethodsGet(self)->StsVtDefaultOffset(self, channel, stsId, vtgId, vtId);
    }

static eAtRet VtTuExpectedSsSet(ThaModuleOcn self, AtSdhPath vtTu, uint8 value)
    {
    uint32 address = cThaRegOcnVtTUPIPerChnCtrl + VtDetaultOffset(self, vtTu);
    uint32 regVal  = mChannelHwRead(vtTu, address, cThaModuleOcn);
    mFieldIns(&regVal, cThaVtPiVtTypeDetPatMask, cThaVtPiVtTypeDetPatShift, value);
    mChannelHwWrite(vtTu, address, regVal, cThaModuleOcn);

    return cAtOk;
    }

static uint8 VtTuExpectedSsGet(ThaModuleOcn self, AtSdhPath vtTu)
    {
    uint32 address = cThaRegOcnVtTUPIPerChnCtrl + VtDetaultOffset(self, vtTu);
    uint32 regVal  = mChannelHwRead(vtTu, address, cThaModuleOcn);
    return (uint8)mRegField(regVal, cThaVtPiVtTypeDetPat);
    }

static eAtRet VtTuSsCompareEnable(ThaModuleOcn self, AtSdhPath vtTu, eBool enable)
    {
    uint32 address = cThaRegOcnVtTUPIPerChnCtrl + VtDetaultOffset(self, vtTu);
    uint32 regVal  = mChannelHwRead(vtTu, address, cThaModuleOcn);
    mFieldIns(&regVal, cThaVtPiVtTypeDetEnMask, cThaVtPiVtTypeDetEnShift, mBoolToBin(enable));
    mChannelHwWrite(vtTu, address, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eBool VtTuSsCompareIsEnabled(ThaModuleOcn self, AtSdhPath vtTu)
    {
    uint32 address = cThaRegOcnVtTUPIPerChnCtrl + VtDetaultOffset(self, vtTu);
    uint32 regVal  = mChannelHwRead(vtTu, address, cThaModuleOcn);
    return (regVal & cThaVtPiVtTypeDetEnMask) ? cAtTrue : cAtFalse;
    }

static uint32 VtTuTxForcibleAlarms(ThaModuleOcn self)
    {
    AtUnused(self);
    return cAtSdhPathAlarmAis | cAtSdhPathAlarmLop;
    }

static eBool VtTuCanForceTxAlarms(ThaModuleOcn self, uint32 alarmType)
    {
    return (alarmType & mMethodsGet(self)->VtTuTxForcibleAlarms(self)) ? cAtTrue : cAtFalse;
    }

static eAtRet VtTuHwTxAlarmForce(ThaModuleOcn self, AtChannel vtTu, uint32 alarmType, eBool force)
    {
    uint32 address = cThaRegOcnTxPGPerChnCtrl + VtDetaultOffset(self, (AtSdhPath)vtTu);
    uint32 regVal  = mChannelHwRead(vtTu, address, cThaModuleOcn);

    if (alarmType & cAtSdhPathAlarmAis)
        mRegFieldSet(regVal, cThaTxPgStsVtTuAisPFrc, mBoolToBin(force));
    if (alarmType & cAtSdhPathAlarmLop)
        mRegFieldSet(regVal, cThaTxPgStsVtTuLopPFrc, mBoolToBin(force));

    mChannelHwWrite(vtTu, address, regVal, cThaModuleOcn);

    return cAtOk;
    }

static uint32 VtTuTxForcedAlarmGet(ThaModuleOcn self, AtChannel vtTu)
    {
    uint32 regAddr = cThaRegOcnTxPGPerChnCtrl + VtDetaultOffset(self, (AtSdhPath)vtTu);
    uint32 regVal  = mChannelHwRead(vtTu, regAddr, cThaModuleOcn);
    uint32 forcedAlarms = 0;

    if (regVal & cThaTxPgStsVtTuAisPFrcMask)
        forcedAlarms |= cAtSdhPathAlarmAis;

    if (regVal & cThaTxPgStsVtTuLopPFrcMask)
        forcedAlarms |= cAtSdhPathAlarmLop;

    return forcedAlarms;
    }

static uint32 VtTuRxForcibleAlarms(ThaModuleOcn self)
    {
    AtUnused(self);
    return 0;
    }

static eBool VtTuCanForceRxAlarms(ThaModuleOcn self, uint32 alarmType)
    {
    return (alarmType & mMethodsGet(self)->VtTuRxForcibleAlarms(self)) ? cAtTrue : cAtFalse;
    }

static uint32 VtTuRxForcedAlarmGet(ThaModuleOcn self, AtChannel vtTu)
    {
    AtUnused(self);
    AtUnused(vtTu);
    return 0;
    }

static eAtRet VtTuHwRxAlarmForce(ThaModuleOcn self, AtChannel vtTu, uint32 alarmType, eBool force)
    {
    AtUnused(self);
    AtUnused(vtTu);
    AtUnused(alarmType);
    AtUnused(force);
    return cAtErrorModeNotSupport;
    }

static const char *TypeString(AtModule self)
    {
    AtUnused(self);
    return "ocn";
    }

static eBool NeedTerminateAsDefault(ThaModuleOcn self, AtSdhChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return cAtTrue;
    }

static eAtRet Vc3PldSet(ThaModuleOcn self, AtSdhChannel vc3, eThaOcnVc3PldType vc3Pld)
    {
    eAtRet ret = cAtOk;
    uint8 stsId = AtSdhChannelSts1Get(vc3);
    AtDevice device = AtChannelDeviceGet((AtChannel)vc3);
    ThaModuleCdr cdrModule = (ThaModuleCdr)AtDeviceModuleGet(device, cThaModuleCdr);
    eAtSdhChannelType vc3ParentType = AtSdhChannelTypeGet(AtSdhChannelParentChannelGet(vc3));
    eBool stsTerminated;

    AtUnused(self);

    if (vc3ParentType == cAtSdhChannelTypeTu3)
        {
        ThaOcnVtPohInsertEnable(vc3, stsId, 0, 0, cAtTrue);
        ret = ThaCdrStsPldSet(cdrModule, vc3, stsId, vc3Pld);
        ret |= Vc3InTu3PayloadSet(vc3, vc3Pld);
        }

    if (vc3ParentType == cAtSdhChannelTypeAu3)
        {
        ret |= ThaOcnStsPohInsertEnable(vc3, stsId, cAtTrue);
        ret |= ThaCdrStsPldSet(cdrModule, vc3, stsId, vc3Pld);
        ret |= Vc3InAu3PayloadSet(self, vc3, vc3Pld);
        }

    stsTerminated = ((vc3ParentType == cAtSdhChannelTypeAu3) && (vc3Pld == cThaOcnVc3PldC3)) ? cAtFalse : cAtTrue;
    ret |= ThaModuleOcnRxStsTermEnable(vc3, stsId, stsTerminated);

    return ret;
    }

static uint32 Vc4TxEnableSliceOffset(uint8 hwSlice)
    {
    return (uint32)(hwSlice << 13);
    }

static eAtRet Vc4xTxEnable(ThaModuleOcn self, AtSdhChannel channel, eBool vc4En)
    {
    uint8 hwSlice, hwSts, i;
    uint32 regVal, regAddr;
    uint8 startSts = AtSdhChannelSts1Get(channel);
    uint8 numSts   = AtSdhChannelNumSts(channel);

    AtUnused(self);

    ThaSdhChannelHwStsGet(channel, cThaModuleOcn, startSts, &hwSlice, &hwSts);

    /* OCN Tx VC4 Enable Control register */
    regAddr = cThaRegOcnTxVc4EnCtrl + Vc4TxEnableSliceOffset(hwSlice) + ChannelPartOffset(channel);
    regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);

    for (i = 0; i < numSts; i++)
        {
        /* Get hardware ID */
        ThaSdhChannelHwStsGet(channel, cThaModuleOcn, (uint8)(startSts + i), &hwSlice, &hwSts);
        mFieldIns(&regVal, OCNTxVC4EnMask(hwSts), OCNTxVC4EnShift(hwSts), vc4En ? 1 : 0);
        }

    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);
    return cAtOk;
    }

static eAtRet RxStsTermEnable(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId, eBool enable)
    {
    uint8 slice, hwSts;
    eAtRet ret = ThaSdhChannelHwStsGet(channel, cThaModuleOcn, stsId, &slice, &hwSts);
    if (ret != cAtOk)
        return ret;

    AtUnused(self);

    return HwStsRxTermEnable(channel, slice, hwSts, enable);
    }

static eAtRet StsTxPayloadTypeSet(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId, eThaStsPayloadType payloadType)
    {
    uint32 offset  = mMethodsGet(self)->StsDefaultOffset(self, channel, stsId);
    uint32 regAddr = mMethodsGet(self)->TxStsMultiplexingControl(self, channel) + offset;
    uint32 regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);
    mFieldIns(&regVal, cThaOcnTxPgStsDemuxSpeTypeMask, cThaOcnTxPgStsDemuxSpeTypeShift, payloadType);
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static uint8 StsTxPayloadTypeGet(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId)
    {
    uint32 offset  = mMethodsGet(self)->StsDefaultOffset(self, channel, stsId);
    uint32 regAddr = mMethodsGet(self)->TxStsMultiplexingControl(self, channel) + offset;
    uint32 regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);

    return (uint8)mRegField(regVal, cThaOcnTxPgStsDemuxSpeType);
    }

static eAtRet StsPohInsertEnable(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId, eBool enable)
    {
    uint32 regAddr, regVal;

    regAddr = cThaRegOcnTxPGPerChnCtrl + mMethodsGet(self)->StsVtDefaultOffset(self, channel, stsId, 0, 0);
    regVal = mChannelHwRead(channel, regAddr, cThaModuleOcn);
    mFieldIns(&regVal, cThaTxPgStsVtPohInsEnMask, cThaTxPgStsVtPohInsEnShift, enable ? 1 : 0);
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eBool StsPohInsertIsEnabled(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId)
    {
    uint32 regAddr, regVal;

    regAddr = cThaRegOcnTxPGPerChnCtrl + mMethodsGet(self)->StsVtDefaultOffset(self, channel, stsId, 0, 0);
    regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);
    return (regVal & cThaTxPgStsVtPohInsEnMask) ? cAtTrue : cAtFalse;
    }

static eAtRet TerPgSlaveIndicate(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId, eBool isSlave)
    {
    AtUnused(self);
    AtUnused(channel);
    AtUnused(stsId);
    AtUnused(isSlave);
    return cAtOk;
    }

static eAtRet PgSlaveIndicate(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId, eBool isSlave)
    {
    uint32 regAddr, regVal;

    regAddr = cThaRegOcnTxPGPerChnCtrl + mMethodsGet(self)->StsVtDefaultOffset(self, channel, stsId, 0, 0);
    regVal = mChannelHwRead(channel, regAddr, cThaModuleOcn);
    mFieldIns(&regVal, cThaTxPgStsSlvIndMask, cThaTxPgStsSlvIndShift, isSlave ? 1 : 0);
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static uint32 BaseAddress(ThaModuleOcn self)
    {
    AtUnused(self);
    return 0;
    }

static eBool SohOverEthIsSupported(ThaModuleOcn self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool KByteOverEthIsSupported(ThaModuleOcn self)
    {
    return mMethodsGet(self)->SohOverEthIsSupported(self);
    }

static uint32 SohOverEthBaseAddress(ThaModuleOcn self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 UpenDccdecBase(ThaModuleOcn self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet SohOverEthInit(ThaModuleOcn self)
    {
    AtUnused(self);
    return cAtOk;
    }

static AtLongRegisterAccess SohOverEthLongRegisterAccessCreate(ThaModuleOcn self)
    {
    AtUnused(self);
    return NULL;
    }

static AtLongRegisterAccess SohOverEthLongRegisterAccess(ThaModuleOcn self)
    {
    if (self->tohLongRegisterAccess == NULL)
        self->tohLongRegisterAccess = mMethodsGet(self)->SohOverEthLongRegisterAccessCreate(self);

    if (self->tohLongRegisterAccess)
        return self->tohLongRegisterAccess;

    return AtDeviceGlobalLongRegisterAccess(AtModuleDeviceGet((AtModule)self));
    }

static uint16 SohOverEthLongWriteOnCore(ThaModuleOcn self, uint32 localAddress, const uint32 *dataBuffer, uint16 bufferLen, AtIpCore core)
    {
    AtLongRegisterAccess registerAccess = mMethodsGet(self)->SohOverEthLongRegisterAccess(self);
    return AtLongRegisterAccessWrite(registerAccess, AtIpCoreHalGet(core), localAddress, dataBuffer, bufferLen);
    }

static uint16 SohOverEthLongReadOnCore(ThaModuleOcn self, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen, AtIpCore core)
    {
    AtLongRegisterAccess registerAccess = mMethodsGet(self)->SohOverEthLongRegisterAccess(self);
    return AtLongRegisterAccessRead(registerAccess, AtIpCoreHalGet(core), localAddress, dataBuffer, bufferLen);
    }

static uint32 *SohOverEthHoldRegistersGet(ThaModuleOcn self, uint16 *numberOfHoldRegisters)
    {
    AtUnused(self);
    /* Default implementation just return no hold register */
    if (numberOfHoldRegisters)
        *numberOfHoldRegisters = 0;

    return NULL;
    }

static eAtRet Vc4xPayloadDefault(ThaModuleOcn self, AtSdhChannel sdhVc)
    {
    uint8 sts_i, hwMasterSts, hwSlice;
    uint8 stsMasterId = AtSdhChannelSts1Get(sdhVc);
    uint8 numSts = AtSdhChannelNumSts(sdhVc);

    AtUnused(self);
    ThaSdhChannelHwStsGet(sdhVc, cThaModuleOcn, stsMasterId, &hwSlice, &hwMasterSts);

    /* Set slave mode */
    for (sts_i = 1; sts_i < numSts; sts_i++)
        {
        Vc4StsPayloadDefault(sdhVc, (uint8)(stsMasterId + sts_i), cThaStsDontCareVtTu);
        Vc4SlaverIndicate(sdhVc, (uint8)(stsMasterId + sts_i), cAtTrue, cAtFalse, hwMasterSts);
        }

    /* Set master mode */
    Vc4StsPayloadDefault(sdhVc, stsMasterId, cThaStsDontCareVtTu);
    Vc4SlaverIndicate(sdhVc, stsMasterId, cAtFalse, cAtTrue, hwMasterSts);

    return cAtOk;
    }

static eBool CanMovePathAisRxForcingPoint(ThaModuleOcn self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet PathAisRxForcingPointMove(ThaModuleOcn self, AtSdhAu au, eBool move)
    {
    AtUnused(self);
    AtUnused(au);
    AtUnused(move);
    return cAtErrorModeNotSupport;
    }

static eBool PathAisRxForcingPointIsMoved(ThaModuleOcn self, AtSdhAu au)
    {
    AtUnused(self);
    AtUnused(au);
    return cAtFalse;
    }

static eAtRet NotifyLoVcAisToPoh(ThaModuleOcn self, AtSdhVc loVc, eBool notified)
    {
    AtUnused(self);
    AtUnused(loVc);
    AtUnused(notified);
    return cAtErrorModeNotSupport;
    }

static uint32 TohGlobalS1StableMonitoringThresholdControlRegAddr(ThaModuleOcn self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static uint32 TohGlobalK1StableMonitoringThresholdControlRegAddr(ThaModuleOcn self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static uint32 TohGlobalK2StableMonitoringThresholdControlRegAddr(ThaModuleOcn self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static eAtRet PiStsGroupIdSet(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId, uint32 groupId)
    {
    AtUnused(self);
    AtUnused(channel);
    AtUnused(stsId);
    AtUnused(groupId);
    return cAtErrorNotImplemented;
    }

static uint32 RxStsPayloadControl(ThaModuleOcn self, AtSdhChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return cThaRegOcnRxPpPerStsVcPldCtrl;
    }

static uint32 TxStsMultiplexingControl(ThaModuleOcn self, AtSdhChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return cThaRegOcnTxPgDemuxPerStsVcPldCtrl;
    }

static eBool NeedIssolateDcc(ThaModuleOcn self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet Vc3ToTu3VcEnable(ThaModuleOcn self, AtSdhChannel vc3, eBool enable)
    {
    AtUnused(self);
    AtUnused(vc3);
    AtUnused(enable);
    return cAtErrorModeNotSupport;
    }

static eBool Vc3ToTu3VcIsEnabled(ThaModuleOcn self, AtSdhChannel vc3)
    {
    AtUnused(self);
    AtUnused(vc3);
    return cAtFalse;
    }

static eAtRet Tu3VcToVc3Enable(ThaModuleOcn self, AtSdhChannel tu3Vc, eBool enable)
    {
    AtUnused(self);
    AtUnused(tu3Vc);
    AtUnused(enable);
    return cAtErrorModeNotSupport;
    }

static eBool Tu3VcToVc3IsEnabled(ThaModuleOcn self, AtSdhChannel tu3Vc)
    {
    AtUnused(self);
    AtUnused(tu3Vc);
    return cAtFalse;
    }

static eBool IsSohRegister(ThaModuleOcn self, uint32 localAddress)
    {
    AtUnused(self);
    AtUnused(localAddress);
    return cAtFalse;
    }

static eAtRet LineTxRsDccInsByOhBusEnable(ThaModuleOcn self, uint32 lineId, eBool enable)
    {
    AtUnused(self);
    AtUnused(lineId);
    AtUnused(enable);
    return cAtErrorModeNotSupport;
    }

static eBool  LineTxRsDccInsByOhBusEnabled(ThaModuleOcn self, uint32 lineId)
    {
    AtUnused(self);
    AtUnused(lineId);
    return cAtFalse;
    }

static eAtRet LineTxMsDccInsByOhBusEnable(ThaModuleOcn self, uint32 lineId, eBool enable)
    {
    AtUnused(self);
    AtUnused(lineId);
    AtUnused(enable);
    return cAtErrorModeNotSupport;
    }

static eBool  LineTxMsDccInsByOhBusEnabled(ThaModuleOcn self, uint32 lineId)
    {
    AtUnused(self);
    AtUnused(lineId);
    return cAtFalse;
    }

static eBool HasRegister(AtModule self, uint32 localAddress)
    {
    eBool hasRegisters = m_AtModuleMethods->HasRegister(self, localAddress);

    if (hasRegisters)
        return hasRegisters;

    return mMethodsGet(mThis(self))->IsSohRegister(mThis(self), localAddress);
    }

static eAtRet LineIdSw2HwGet(ThaModuleOcn self, AtSdhLine sdhLine, eAtModule moduleId, uint8* sliceId, uint8 *hwLineInSlice)
    {
    AtUnused(self);
    AtUnused(moduleId);
    *sliceId = 0;
    *hwLineInSlice = (uint8)AtChannelHwIdGet((AtChannel)sdhLine);
    return cAtOk;
    }

static eAtRet StsLoopbackSet(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId, uint8 loopbackMode)
    {
    AtUnused(self);
    AtUnused(channel);
    AtUnused(stsId);
    AtUnused(loopbackMode);
    return cAtError;
    }

static uint8 StsLoopbackGet(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId)
    {
    AtUnused(self);
    AtUnused(channel);
    AtUnused(stsId);
    return cAtLoopbackModeRelease;
    }

static eAtRet VtTuLoopbackSet(ThaModuleOcn self, AtSdhChannel channel, uint8 loopbackMode)
    {
    AtUnused(self);
    AtUnused(channel);
    AtUnused(loopbackMode);
    return cAtError;
    }

static uint8 VtTuLoopbackGet(ThaModuleOcn self, AtSdhChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return cAtLoopbackModeRelease;
    }

static eAtRet Vc3MapDe3LiuEnable(ThaModuleOcn self, AtSdhChannel sdhVc, eBool enable)
    {
    AtUnused(self);
    AtUnused(sdhVc);
    return (enable) ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool Vc3MapDe3LiuIsEnabled(ThaModuleOcn self, AtSdhChannel sdhVc)
    {
    AtUnused(self);
    AtUnused(sdhVc);
    return cAtFalse;
    }

static uint32 LineDefaultOffset(ThaModuleOcn self, AtSdhChannel sdhLine)
    {
    AtUnused(self);
    AtUnused(sdhLine);
    /* Let subclass determine */
    return 0;
    }

static uint32 LineTxDefaultOffset(ThaModuleOcn self, AtSdhChannel sdhLine)
    {
    AtUnused(self);
    AtUnused(sdhLine);
    /* Let subclass determine */
    return 0;
    }

static eAtRet TohThresholdDefaultSet(ThaModuleOcn self)
    {
    AtUnused(self);
    /* Let subclass determine */
    return cAtOk;
    }

static void OverrideAtObject(AtModule self)
    {
    AtObject object = (AtObject)self;

    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, RegisterIteratorCreate);
        mMethodOverride(m_AtModuleOverride, TypeString);
        mMethodOverride(m_AtModuleOverride, HasRegister);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideAtObject(self);
    }

static void MethodsInit(AtModule self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, StsDefaultOffset);
        mMethodOverride(m_methods, StsVtDefaultOffset);
        mMethodOverride(m_methods, TxStsVtDefaultOffset);
        mMethodOverride(m_methods, TxTerHwHoStsOffsetWithBaseAddress);
        mMethodOverride(m_methods, LineDefaultOffset);
        mMethodOverride(m_methods, LineTxDefaultOffset);
        mMethodOverride(m_methods, StsIdSw2Hw);
        mMethodOverride(m_methods, NumSlices);
        mMethodOverride(m_methods, NumStsInOneSlice);
        mMethodOverride(m_methods, PwTohByteEnable);
        mMethodOverride(m_methods, PwTohSourceSet);
        mMethodOverride(m_methods, RxStsPiMastIdMask);
        mMethodOverride(m_methods, ChannelStsIdSw2HwGet);
        mMethodOverride(m_methods, LineIdSw2HwGet);
        mMethodOverride(m_methods, PiStsConcatMasterSet);
        mMethodOverride(m_methods, PiStsConcatSlaveSet);
        mMethodOverride(m_methods, PgStsConcatMasterSet);
        mMethodOverride(m_methods, PgStsConcatSlaveSet);
        mMethodOverride(m_methods, TerPgStsConcatMasterSet);
        mMethodOverride(m_methods, TerPgStsConcatSlaveSet);
        mMethodOverride(m_methods, TerPgStsConcatRelease);
        mMethodOverride(m_methods, StsIsSlaveInChannel);
        mMethodOverride(m_methods, DefaultSet);
        mMethodOverride(m_methods, TohThresholdDefaultSet);
        mMethodOverride(m_methods, Tug3PldSet);
        mMethodOverride(m_methods, VtPohInsertEnable);
        mMethodOverride(m_methods, VtPohInsertIsEnabled);
        mMethodOverride(m_methods, StsPohInsertEnable);
        mMethodOverride(m_methods, StsPohInsertIsEnabled);
        mMethodOverride(m_methods, VtTerminate);
        mMethodOverride(m_methods, RxVtgPdhReset);
        mMethodOverride(m_methods, RxVtgSet);
        mMethodOverride(m_methods, TxVtgPdhReset);
        mMethodOverride(m_methods, TxVtgSet);
        mMethodOverride(m_methods, PpSsTxVtSet);
        mMethodOverride(m_methods, PpSsTxVtGet);
        mMethodOverride(m_methods, VtTuExpectedSsSet);
        mMethodOverride(m_methods, VtTuExpectedSsGet);
        mMethodOverride(m_methods, VtTuSsCompareEnable);
        mMethodOverride(m_methods, VtTuSsCompareIsEnabled);
        mMethodOverride(m_methods, VtTuTxForcibleAlarms);
        mMethodOverride(m_methods, VtTuHwTxAlarmForce);
        mMethodOverride(m_methods, VtTuTxForcedAlarmGet);
        mMethodOverride(m_methods, VtTuRxForcibleAlarms);
        mMethodOverride(m_methods, VtTuHwRxAlarmForce);
        mMethodOverride(m_methods, VtTuRxForcedAlarmGet);
        mMethodOverride(m_methods, SdhChannelRegsShow);
        mMethodOverride(m_methods, StsConcatRelease);
        mMethodOverride(m_methods, NeedTerminateAsDefault);
        mMethodOverride(m_methods, Vc3PldSet);
        mMethodOverride(m_methods, Vc4xTxEnable);
        mMethodOverride(m_methods, RxStsTermEnable);
        mMethodOverride(m_methods, Tug3HwPayloadTypeSet);
        mMethodOverride(m_methods, Au3VcHwPayloadTypeSet);
        mMethodOverride(m_methods, PgSlaveIndicate);
        mMethodOverride(m_methods, TerPgSlaveIndicate);
        mMethodOverride(m_methods, StsTxPayloadTypeSet);
        mMethodOverride(m_methods, StsTxPayloadTypeGet);
        mMethodOverride(m_methods, Vc4xPayloadDefault);
        mMethodOverride(m_methods, CanMovePathAisRxForcingPoint);
        mMethodOverride(m_methods, PathAisRxForcingPointMove);
        mMethodOverride(m_methods, PathAisRxForcingPointIsMoved);
        mMethodOverride(m_methods, NotifyLoVcAisToPoh);
        mMethodOverride(m_methods, StsLoopbackSet);
        mMethodOverride(m_methods, StsLoopbackGet);
        mMethodOverride(m_methods, VtTuLoopbackSet);
        mMethodOverride(m_methods, VtTuLoopbackGet);
        mMethodOverride(m_methods, BaseAddress);
        mMethodOverride(m_methods, TohGlobalS1StableMonitoringThresholdControlRegAddr);
        mMethodOverride(m_methods, TohGlobalK1StableMonitoringThresholdControlRegAddr);
        mMethodOverride(m_methods, TohGlobalK2StableMonitoringThresholdControlRegAddr);
        mMethodOverride(m_methods, PiStsGroupIdSet);
        mMethodOverride(m_methods, Vc3ToTu3VcEnable);
        mMethodOverride(m_methods, Vc3ToTu3VcIsEnabled);
        mMethodOverride(m_methods, Tu3VcToVc3Enable);
        mMethodOverride(m_methods, Tu3VcToVc3IsEnabled);

        /* For TOH DCC, KByte purpose */
        mMethodOverride(m_methods, SohOverEthHoldRegistersGet);
        mMethodOverride(m_methods, SohOverEthLongRegisterAccessCreate);
        mMethodOverride(m_methods, SohOverEthLongRegisterAccess);
        mMethodOverride(m_methods, SohOverEthLongReadOnCore);
        mMethodOverride(m_methods, SohOverEthLongWriteOnCore);
        mMethodOverride(m_methods, SohOverEthInit);
        mMethodOverride(m_methods, SohOverEthIsSupported);
        mMethodOverride(m_methods, KByteOverEthIsSupported);
        mMethodOverride(m_methods, SohOverEthBaseAddress);
        mMethodOverride(m_methods, RxStsPayloadControl);
        mMethodOverride(m_methods, TxStsMultiplexingControl);
        mMethodOverride(m_methods, UpenDccdecBase);
        mMethodOverride(m_methods, NeedIssolateDcc);
        mMethodOverride(m_methods, IsSohRegister);
        mMethodOverride(m_methods, LineTxRsDccInsByOhBusEnable);
        mMethodOverride(m_methods, LineTxRsDccInsByOhBusEnabled);
        mMethodOverride(m_methods, LineTxMsDccInsByOhBusEnable);
        mMethodOverride(m_methods, LineTxMsDccInsByOhBusEnabled);
        mMethodOverride(m_methods, Vc3MapDe3LiuEnable);
        mMethodOverride(m_methods, Vc3MapDe3LiuIsEnabled);
        }

    mMethodsSet((ThaModuleOcn)self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaModuleOcn);
    }

AtModule ThaModuleOcnObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleObjectInit(self, cThaModuleOcn, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtModule ThaModuleOcnNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ThaModuleOcnObjectInit(newModule, device);
    }

void ThaModuleOcnSdhChannelRegsShow(AtSdhChannel sdhChannel)
    {
    ThaModuleOcn self = ThaModuleOcnFromChannel((AtChannel)sdhChannel);
    ThaModuleSdhStsMappingDisplay(sdhChannel);
    AtPrintc(cSevInfo, "* OCN registers of channel '%s':\r\n", AtObjectToString((AtObject)sdhChannel));
    if (self)
        mMethodsGet(self)->SdhChannelRegsShow(self, sdhChannel);

    AtPrintc(cSevInfo, "\r\n");
    }

ThaModuleOcn ThaModuleOcnFromChannel(AtChannel channel)
    {
    return (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet(channel), cThaModuleOcn);
    }

eAtRet ThaOcnStsTxPayloadTypeSet(AtSdhChannel channel, uint8 stsId, eThaStsPayloadType payloadType)
    {
    ThaModuleOcn self = (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)channel), cThaModuleOcn);
    if (self)
        return mMethodsGet(self)->StsTxPayloadTypeSet(self, channel, stsId, payloadType);
    return cAtErrorNullPointer;
    }

uint8 ThaOcnStsTxPayloadTypeGet(AtSdhChannel channel, uint8 stsId)
    {
    ThaModuleOcn self = (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)channel), cThaModuleOcn);
    if (self)
        return mMethodsGet(self)->StsTxPayloadTypeGet(self, channel, stsId);
    return 0;
    }

uint8 ThaOcnStsRxPayloadTypeGet(AtSdhChannel channel, uint8 stsId)
    {
    static const uint8 cRxInvalidPayloadType = 0xff;
    ThaModuleOcn self;
    uint32       regAddr, regVal, offset;

    self = (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)channel), cThaModuleOcn);
    if (self == NULL)
        return cRxInvalidPayloadType;

    offset  = mMethodsGet(self)->StsDefaultOffset(self, channel, stsId);
    regAddr = mMethodsGet(self)->RxStsPayloadControl(self, channel) + offset;
    regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);

    return (uint8)mRegField(regVal, cThaOcnRxPpStsDemuxSpeType);
    }

eAtSdhLineRate ThaOcnLineRateGet(AtSdhLine line)
    {
    eAtSdhLineRate    lineRate;
    uint32            regVal;
    uint8             hwLineId;
    eAtSdhLineRate    highRate = ThaSdhLineHighRate((ThaSdhLine)line);
    eAtSdhLineRate    lowRate = ThaSdhLineLowRate((ThaSdhLine)line);

    /* Get hwLine identifier */
    hwLineId = (uint8)ThaModuleSdhLineLocalId(line);

    /* Read the OCN Rx Line Rate Control register */
    regVal = mChannelHwRead(line, cThaRegOcnRxLineRateCtrl + ChannelPartOffset((AtSdhChannel)line), cThaModuleOcn);
    if ((hwLineId == 0) || (hwLineId == 1))
        {
        mFieldGet(regVal, cThaOcnRxLineRateMask(hwLineId), cThaOcnRxLineRateShift(hwLineId), uint8, &regVal);
        if (regVal == 0x0)
            lineRate = lowRate;
        else if (regVal == 0x1)
            lineRate = highRate;
        else
            lineRate = cAtSdhLineRateInvalid;
        }
    else
        {
        mFieldGet(regVal, cThaOcnRxLineRateMask(0), cThaOcnRxLineRateShift(0), uint8, &regVal);
        lineRate = (regVal == 0x1) ? cAtSdhLineRateInvalid : lowRate;
        }

    return lineRate;
    }

eAtRet ThaOcnLineRateSet(AtSdhLine line, eAtSdhLineRate rate)
    {
    uint32 regVal, regAddr;
    uint8  i;
    uint32 partOffset = ChannelPartOffset((AtSdhChannel)line);
    uint8 numLinesPerPart = ThaModuleSdhNumLinesPerPart((AtModuleSdh)AtChannelModuleGet((AtChannel)line));
    eAtSdhLineRate highRate = ThaSdhLineHighRate((ThaSdhLine)line);
    eAtSdhLineRate lowRate = ThaSdhLineLowRate((ThaSdhLine)line);

    /* Configure hardware - LOW-RATE */
    if (rate == lowRate)
        {
        /* All line rates are also low rate */
        regAddr = cThaRegOcnRxLineRateCtrl + partOffset;
        regVal = mChannelHwRead(line, regAddr, cThaModuleOcn);
        for (i = 0; i < numLinesPerPart; i++)
            mFieldIns(&regVal, cThaOcnRxLineRateMask(i), cThaOcnRxLineRateShift(i), 0x0);
        mChannelHwWrite(line, regAddr, regVal, cThaModuleOcn);

        /* All line rates are also low rate */
        regAddr = cThaRegOcnTxLineRateCtrl + partOffset;
        regVal = mChannelHwRead(line, regAddr, cThaModuleOcn);
        for (i = 0; i < numLinesPerPart; i++)
            mFieldIns(&regVal, cThaOcnTxLineRateMask(i), cThaOcnTxLineRateShift(i), 0x0);
        mChannelHwWrite(line, regAddr, regVal, cThaModuleOcn);

        if (lowRate == cAtSdhLineRateStm1)
            {
            /* Engine physical bandwidth */
            regAddr = cThaRegOcnTxFrmrEngBwCtrl + partOffset;
            regVal = mChannelHwRead(line, regAddr, cThaModuleOcn);
            mFieldIns(&regVal, cThaOcnTxFrmEng1Oc12Mask, cThaOcnTxFrmEng1Oc12Shift, 0);
            mChannelHwWrite(line, regAddr, regVal, cThaModuleOcn);
            }
        }

    /* Configure hardware - HIGH-RATE */
    if (rate == highRate)
        {
        /* Configure RX rate */
        regAddr = cThaRegOcnRxLineRateCtrl + partOffset;
        regVal = mChannelHwRead(line, regAddr, cThaModuleOcn);
        mFieldIns(&regVal, cThaOcnRxLineRateMask(0), cThaOcnRxLineRateShift(0), 0x1);
        mChannelHwWrite(line, regAddr, regVal, cThaModuleOcn);

        /* Disable Rx lines 2,3 */
        regAddr = cThaRegOcnRxLineEnCtrl + partOffset;
        regVal = mChannelHwRead(line, regAddr, cThaModuleOcn);
        mFieldIns(&regVal, cThaOcnRxLineEnbMask(1), cThaOcnRxLineEnbShift(1), 0x0);
        mFieldIns(&regVal, cThaOcnRxLineEnbMask(2), cThaOcnRxLineEnbShift(2), 0x0);
        mFieldIns(&regVal, cThaOcnRxLineEnbMask(3), cThaOcnRxLineEnbShift(3), 0x0);
        mChannelHwWrite(line, regAddr, regVal, cThaModuleOcn);

        /* Configure TX rate */
        regAddr = cThaRegOcnTxLineRateCtrl + partOffset;
        regVal = mChannelHwRead(line, regAddr, cThaModuleOcn);
        mFieldIns(&regVal, cThaOcnTxLineRateMask(0), cThaOcnTxLineRateShift(0), 0x1);
        mChannelHwWrite(line, regAddr, regVal, cThaModuleOcn);

        /* Disable Tx lines 2,3 */
        regAddr = cThaRegOcnTxLineEnCtrl + partOffset;
        regVal = mChannelHwRead(line, regAddr, cThaModuleOcn);
        mFieldIns(&regVal, cThaOcnTxLineEnbMask(1), cThaOcnTxLineEnbShift(1), 0x0);
        mFieldIns(&regVal, cThaOcnTxLineEnbMask(2), cThaOcnTxLineEnbShift(2), 0x0);
        mFieldIns(&regVal, cThaOcnTxLineEnbMask(3), cThaOcnTxLineEnbShift(3), 0x0);
        mChannelHwWrite(line, regAddr, regVal, cThaModuleOcn);

        if (highRate == cAtSdhLineRateStm4)
            {
            /* Engine physical bandwidth */
            regAddr = cThaRegOcnTxFrmrEngBwCtrl + partOffset;
            regVal = mChannelHwRead(line, regAddr, cThaModuleOcn);
            mFieldIns(&regVal, cThaOcnTxFrmEng1Oc12Mask, cThaOcnTxFrmEng1Oc12Shift, 1);
            mChannelHwWrite(line, regAddr, regVal, cThaModuleOcn);
            }
        }

    return cAtOk;
    }

eAtRet ThaOcnVc3PldSet(AtSdhChannel vc3, eThaOcnVc3PldType vc3Pld)
    {
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)vc3);
    if (ocnModule)
        return mMethodsGet(ocnModule)->Vc3PldSet(ocnModule, vc3, vc3Pld);
    return cAtErrorNullPointer;
    }

eThaOcnVc3PldType ThaOcnVc3PldGet(AtSdhChannel vc3)
    {
    eAtSdhChannelType vc3ParentType = AtSdhChannelTypeGet(AtSdhChannelParentChannelGet(vc3));

    if (vc3ParentType == cAtSdhChannelTypeTu3)
        return Vc3InTu3PayloadGet(vc3);

    if (vc3ParentType == cAtSdhChannelTypeAu3)
        return Vc3InAu3PayloadGet(vc3);

    return cThaOcnVc3PldC3;
    }

eBool ThaOcnChannelIsConcatenated(AtSdhChannel sdhChannel)
    {
    uint8 sts_i;
    uint8 numStsInChannel = AtSdhChannelNumSts(sdhChannel);
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)sdhChannel);

    if (ocnModule == NULL)
        return cAtFalse;

    for (sts_i = 1; sts_i < numStsInChannel; sts_i++)
        {
        uint8 masterSts = AtSdhChannelSts1Get(sdhChannel);
        if (!mMethodsGet(ocnModule)->StsIsSlaveInChannel(ocnModule, sdhChannel, (uint8)(sts_i + masterSts)))
            return cAtFalse;
        }

    return cAtTrue;
    }

eAtRet ThaOcnStsConcat(AtSdhChannel channel, uint8 startSts, uint8 numSts)
    {
    eAtRet ret = cAtOk;
    uint16 slave_i;
    uint32 numSlaves;
    AtDevice device = AtChannelDeviceGet((AtChannel)channel);
    ThaModuleOcn ocnModule = (ThaModuleOcn)AtDeviceModuleGet(device, cThaModuleOcn);

    if (!Sts1IsValid(channel, startSts))
        return cAtErrorInvlParm;

    ret |= mMethodsGet(ocnModule)->PiStsConcatMasterSet(ocnModule, channel, startSts);
    ret |= mMethodsGet(ocnModule)->PgStsConcatMasterSet(ocnModule, channel, startSts);
    ret |= mMethodsGet(ocnModule)->TerPgStsConcatMasterSet(ocnModule, channel, startSts);

    /* Configure slave STS, ignore master sts */
    numSlaves = numSts - 1UL;
    for(slave_i = 0; slave_i < numSlaves; slave_i++)
        {
        ret |= mMethodsGet(ocnModule)->PiStsConcatSlaveSet(ocnModule, channel, startSts, (uint8)(startSts + slave_i + 1));
        ret |= mMethodsGet(ocnModule)->PgStsConcatSlaveSet(ocnModule, channel, startSts, (uint8)(startSts + slave_i + 1));
        ret |= mMethodsGet(ocnModule)->TerPgStsConcatSlaveSet(ocnModule, channel, startSts, (uint8)(startSts + slave_i + 1));
        }

    return ret;
    }

eAtRet ThaOcnStsPgSlaveIndicate(AtSdhChannel channel, uint8 stsId, eBool isSlave)
    {
    eAtRet ret = cAtOk;
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)channel);
    if (ocnModule == NULL)
        return cAtErrorNullPointer;

    ret |= mMethodsGet(ocnModule)->PgSlaveIndicate(ocnModule, channel, stsId, isSlave);
    ret |= mMethodsGet(ocnModule)->TerPgSlaveIndicate(ocnModule, channel, stsId, isSlave);
    return ret;
    }

eAtRet ThaOcnStsPohInsertEnable(AtSdhChannel channel, uint8 stsId, eBool enable)
    {
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)channel);
    if (ocnModule)
        return mMethodsGet(ocnModule)->StsPohInsertEnable(ocnModule, channel, stsId, enable);
    return cAtErrorNullPointer;
    }

eBool ThaOcnStsPohInsertIsEnabled(AtSdhChannel channel, uint8 stsId)
    {
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)channel);
    if (ocnModule)
        return mMethodsGet(ocnModule)->StsPohInsertIsEnabled(ocnModule, channel, stsId);
    return cAtFalse;
    }

void ThaOcnVtPohInsertEnable(AtSdhChannel channel, uint8 stsId, uint8 vtgId, uint8 vtId, eBool enable)
    {
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)channel);
    if (ocnModule)
        mMethodsGet(ocnModule)->VtPohInsertEnable(ocnModule, channel, stsId, vtgId, vtId, enable);
    }

eBool ThaOcnVtPohInsertIsEnabled(AtSdhChannel channel, uint8 stsId, uint8 vtgId, uint8 vtId)
    {
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)channel);
    if (ocnModule)
        return mMethodsGet(ocnModule)->VtPohInsertIsEnabled(ocnModule, channel, stsId, vtgId, vtId);
    return cAtFalse;
    }

/*
 * Get HW sts ID of a SDH channel
 *
 * @param sdhChannel SDH channel
 * @param phyModule Physical module context in which slice and HW ID of channel need to be retrieved
 * @param swSts Software STS ID, for example with VC4, there are 3 software STSs with master ID 0, and 2 slave IDs 1 and 2
 *
 * @param sliceId Hardware slice
 * @param hwStsInSlice Hardware STS in slice
 * @return AT return code
 */
eAtRet ThaSdhChannelHwStsGet(AtSdhChannel sdhChannel, eAtModule phyModule, uint8 swSts, uint8* sliceId, uint8 *hwStsInSlice)
    {
    eAtRet ret = cAtErrorNullPointer;
    ThaModuleOcn ocnModule = (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)sdhChannel), cThaModuleOcn) ;

    if (ocnModule)
        ret = mMethodsGet(ocnModule)->ChannelStsIdSw2HwGet(ocnModule, sdhChannel, phyModule, swSts, sliceId, hwStsInSlice);

    return ret;
    }

/*
 * Get HW Line ID of a SDH Line
 *
 * @param sdhLine SDH Line
 * @param phyModule Physical module context in which slice and HW ID of channel need to be retrieved
 *
 * @param sliceId Hardware slice
 * @param hwLineInSlice Hardware Line ID in slice
 * @return AT return code
 */
eAtRet ThaSdhLineHwIdGet(AtSdhLine sdhLine, eAtModule phyModule, uint8* sliceId, uint8 *hwLineInSlice)
    {
    eAtRet ret = cAtErrorNullPointer;
    ThaModuleOcn ocnModule = (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)sdhLine), cThaModuleOcn) ;

    if (ocnModule)
        ret = mMethodsGet(ocnModule)->LineIdSw2HwGet(ocnModule, sdhLine, phyModule, sliceId, hwLineInSlice);

    return ret;
    }

eAtRet ThaOcnSts1Init(AtSdhChannel aug, uint8 sts1Id)
    {
    ThaModuleOcn ocnModule;

    ocnModule = ThaModuleOcnFromChannel((AtChannel)aug);
    if (ocnModule == NULL)
        return cAtErrorNullPointer;

    if (!Sts1IsValid(aug, sts1Id))
        return cAtErrorInvlParm;

    /* Release all concatenation of which this STS is master */
    mMethodsGet(ocnModule)->StsConcatRelease(ocnModule, aug, sts1Id);

    /* Terminated it as default */
    if (mMethodsGet(ocnModule)->NeedTerminateAsDefault(ocnModule, aug))
        ThaModuleOcnRxStsTermEnable(aug, sts1Id, cAtTrue);

    /* Make it be master of itself */
    return ThaOcnStsConcat(aug, sts1Id, 1);
    }

eAtRet ThaOcnTug3PldSet(AtSdhChannel tug3, eAtSdhTugMapType tug3MapType)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)tug3);
    ThaModuleOcn ocnModule = (ThaModuleOcn)AtDeviceModuleGet(device, cThaModuleOcn);
    if (ocnModule)
        return mMethodsGet(ocnModule)->Tug3PldSet(ocnModule, tug3, tug3MapType);
    return cAtErrorNullPointer;
    }

eAtRet ThaOcnVtgPldSet(AtSdhChannel tug2, uint8 stsId, uint8 vtgId, eThaOcnVtgTug2PldType vtPldType)
    {
    eAtRet ret;
    uint8 vt_i, numVtInVtg;
    eThaOcnVtTuType vtgType;
    eThaCfgOcnSsMd ssVal;
    eThaDatMapPldMd mapVtgType;
    AtModule module;
    AtDevice device = AtChannelDeviceGet((AtChannel)tug2);
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)tug2);

    if (!Sts1IsValid(tug2, stsId))
        return cAtErrorInvlParm;

    if (vtPldType == cThaOcnVtgTug2PldVt15)
        {
        vtgType     = cThaOcnVt15Tu11;
        numVtInVtg  = cThaOcnNumVt15InVtg;
        ssVal       = cThaOcnSsVt15Tu11Md;
        mapVtgType  = cThaCfgMapPldVt15;
        }

    else if (vtPldType == cThaOcnVtgTug2PldVt2)
        {
        vtgType     = cThaOcnVt2Tu12;
        numVtInVtg  = cThaOcnNumVt2InVtg;
        ssVal       = cThaOcnSsVt2Tu12Md;
        mapVtgType  = cThaCfgMapPldVt2;
        }

    else
        return cAtErrorInvlParm;

    /* Configure VTG pay-load for two direction (TX and RX) */
    ret = mMethodsGet(ocnModule)->RxVtgPdhReset(ocnModule, tug2, stsId, vtgId, vtgType);
    ret = mMethodsGet(ocnModule)->RxVtgSet(ocnModule, tug2, stsId, vtgId, vtgType);
    if(ret != cAtOk)
        return ret;
    ret = mMethodsGet(ocnModule)->TxVtgPdhReset(ocnModule, tug2, stsId, vtgId, vtgType);
    ret = mMethodsGet(ocnModule)->TxVtgSet(ocnModule, tug2, stsId, vtgId, vtgType);
    if(ret != cAtOk)
        return ret;

    for(vt_i = 0; vt_i < numVtInVtg; vt_i++)
        {
        /* Configure SS bit to transmit */
        ret = mMethodsGet(ocnModule)->PpSsTxVtSet(ocnModule, tug2, stsId, vtgId, vt_i, ssVal, cAtTrue);
        if(ret != cAtOk)
            return ret;
        }

    /* Configure payload mode at CDR module */
    module = AtDeviceModuleGet(device, cThaModuleCdr);
    ret = ThaCdrVtgPldSet((ThaModuleCdr)module, tug2, stsId, vtgId, vtPldType);
    if (ret != cAtOk)
        return ret;

    /* Configure payload mode at MAP module */
    module = AtDeviceModuleGet(device, cThaModuleMap);
    ret = ThaMapVtgPldMdSet((ThaModuleMap)module, tug2, stsId, vtgId, mapVtgType);

    return ret;
    }

uint8 ThaOcnVtgPldGet(AtSdhChannel channel, uint8 stsId, uint8 vtgId)
    {
    uint8 hwType = TxVtgGet(channel, stsId, vtgId);

    if (hwType != RxVtgGet(channel, stsId, vtgId))
        return 0xFF;

    return VtTuTypeHw2Sw(hwType);
    }

eAtRet ThaOcnVtPathInit(AtSdhChannel channel, uint8 stsId, uint8 vtgId, uint8 vtId)
    {
    ThaModuleOcn ocnModule;

    if (!Sts1IsValid(channel, stsId))
        return cAtErrorInvlParm;

    ThaOcnVtPohInsertEnable(channel, stsId, vtgId, vtId, cAtTrue);
    ocnModule = ThaModuleOcnFromChannel((AtChannel)channel);
    mMethodsGet(ocnModule)->VtTerminate(ocnModule, channel, stsId, vtgId, vtId, cAtTrue);

    return cAtOk;
    }

eAtRet ThaModuleOcnRxStsTermEnable(AtSdhChannel channel, uint8 stsId, eBool enable)
    {
    ThaModuleOcn ocn = ThaModuleOcnFromChannel((AtChannel)channel);
    if (ocn)
        return mMethodsGet(ocn)->RxStsTermEnable(ocn, channel, stsId, enable);
    return cAtErrorNullPointer;
    }

eBool ThaModuleOcnRxStsIsTerminated(AtSdhChannel channel, uint8 stsId)
    {
    uint32 regAddr, regVal;
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)channel);
    uint32 bitVal;

    if (ocnModule == NULL)
        return cAtFalse;

    regAddr = cThaRegOcnRxStsPiPerChnCtrl + mMethodsGet(ocnModule)->StsDefaultOffset(ocnModule, channel, stsId);
    regVal = mChannelHwRead(channel, regAddr, cThaModuleOcn);
    bitVal = mRegField(regVal, cThaRxStsPiStsTermEn);

    return (bitVal) ? cAtTrue : cAtFalse;
    }

uint32 ThaModuleOcnPartOffset(ThaModuleOcn self, uint8 partId)
    {
    return ThaDeviceModulePartOffset(Device(self), cThaModuleOcn, partId);
    }

uint8 ThaModuleOcnNumParts(ThaModuleOcn self)
    {
    return ThaDeviceNumPartsOfModule(Device(self), cThaModuleOcn);
    }

eAtRet ThaOcnVc4xPayloadDefault(ThaModuleOcn self, AtSdhChannel sdhVc)
    {
    if (self)
        return mMethodsGet(self)->Vc4xPayloadDefault(self, sdhVc);
    return cAtErrorNullPointer;
    }

uint8 ThaModuleOcnNumSlices(ThaModuleOcn self)
    {
    if (self)
        return mMethodsGet(self)->NumSlices(self);
    return 0;
    }

uint8 ThaModuleOcnNumStsInOneSlice(ThaModuleOcn self)
    {
    if (self)
        return mMethodsGet(self)->NumStsInOneSlice(self);
    return 0;
    }

eAtRet ThaModuleOcnPwTohByteEnable(ThaModuleOcn self, AtPw pw, const tThaTohByte *tohByte, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->PwTohByteEnable(self, pw, tohByte, enable);
    return cAtError;
    }

uint32 ThaModuleOcnStsDefaultOffset(ThaModuleOcn self, AtSdhChannel channel, uint8 sts)
    {
    if (self)
        return mMethodsGet(self)->StsDefaultOffset(self, channel, sts);
    return 0;
    }

uint32 ThaModuleOcnStsVtDefaultOffset(ThaModuleOcn self, AtSdhChannel channel, uint8 sts, uint8 vtg, uint8 vt)
    {
    if (self)
        return mMethodsGet(self)->StsVtDefaultOffset(self, channel, sts, vtg, vt);
    return 0;
    }

uint32 ThaModuleOcnTxStsVtDefaultOffset(ThaModuleOcn self, AtSdhChannel channel, uint8 sts, uint8 vtg, uint8 vt)
    {
    if (self)
        return mMethodsGet(self)->TxStsVtDefaultOffset(self, channel, sts, vtg, vt);
    return 0;
    }

uint32 ThaModuleOcnLineDefaultOffset(ThaModuleOcn self, AtSdhChannel sdhLine)
    {
    if (self)
        return mMethodsGet(self)->LineDefaultOffset(self, sdhLine);
    return 0;
    }

uint32 ThaModuleOcnLineTxDefaultOffset(ThaModuleOcn self, AtSdhChannel sdhLine)
    {
    if (self)
        return mMethodsGet(self)->LineTxDefaultOffset(self, sdhLine);
    return 0;
    }

/* Helper function for easy to use when just need HW master ID */
eAtRet ThaSdhChannel2HwMasterStsId(AtSdhChannel channel, eAtModule phyModule, uint8 *slice, uint8 *hwStsInSlice)
    {
    return ThaSdhChannelHwStsGet(channel, phyModule, AtSdhChannelSts1Get(channel), slice, hwStsInSlice);
    }

uint8 ThaModuleOcnVtTuTypeSw2Hw(eThaOcnVtTuType vtTuType)
    {
    if (vtTuType == cThaOcnVt15Tu11) return 0;
    if (vtTuType == cThaOcnVt2Tu12)  return 1;
    if (vtTuType == cThaOcnVt3)      return 2;
    if (vtTuType == cThaOcnVt6Tu2)   return 3;

    /* TU11 as default */
    return 0;
    }

eAtRet ThaOcnPpSsTxVtSet(AtSdhChannel vt, uint8 insSsBit,eBool insSsBitEn)
    {
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)vt);
    uint8 stsId, vtgId, vtId;

    if (ocnModule == NULL)
        return cAtErrorNullPointer;

    stsId = AtSdhChannelSts1Get(vt);
    vtgId = AtSdhChannelTug2Get(vt);
    vtId  = AtSdhChannelTu1xGet(vt);
    return mMethodsGet(ocnModule)->PpSsTxVtSet(ocnModule, vt, stsId, vtgId, vtId, insSsBit, insSsBitEn);
    }

uint8 ThaOcnPpSsTxVtGet(AtSdhChannel vt)
    {
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)vt);
    uint8 stsId, vtgId, vtId;

    if (ocnModule == NULL)
        return cAtErrorNullPointer;

    stsId = AtSdhChannelSts1Get(vt);
    vtgId = AtSdhChannelTug2Get(vt);
    vtId  = AtSdhChannelTu1xGet(vt);
    return mMethodsGet(ocnModule)->PpSsTxVtGet(ocnModule, vt, stsId, vtgId, vtId);
    }

eAtRet ThaOcnVtTuExpectedSsSet(AtSdhPath vtTu, uint8 value)
    {
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)vtTu);
    if (ocnModule)
        return mMethodsGet(ocnModule)->VtTuExpectedSsSet(ocnModule, vtTu, value);
    return cAtErrorNullPointer;
    }

uint8 ThaOcnVtTuExpectedSsGet(AtSdhPath vtTu)
    {
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)vtTu);
    if (ocnModule)
        return mMethodsGet(ocnModule)->VtTuExpectedSsGet(ocnModule, vtTu);
    return 0;
    }

eAtRet ThaOcnVtTuSsCompareEnable(AtSdhPath vtTu, eBool enable)
    {
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)vtTu);
    if (ocnModule)
        return mMethodsGet(ocnModule)->VtTuSsCompareEnable(ocnModule, vtTu, enable);
    return cAtErrorNullPointer;
    }

eBool ThaOcnVtTuSsCompareIsEnabled(AtSdhPath vtTu)
    {
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)vtTu);
    if (ocnModule)
        return mMethodsGet(ocnModule)->VtTuSsCompareIsEnabled(ocnModule, vtTu);
    return cAtFalse;
    }

uint32 ThaOcnVtTuTxForcibleAlarms(ThaModuleOcn self)
    {
    if (self)
        return mMethodsGet(self)->VtTuTxForcibleAlarms(self);
    return 0;
    }

uint32 ThaOcnVtTuRxForcibleAlarms(ThaModuleOcn self)
    {
    if (self)
        return mMethodsGet(self)->VtTuRxForcibleAlarms(self);
    return 0;
    }

eAtRet ThaOcnVtTuTxAlarmForce(AtChannel vtTu, uint32 alarmType)
    {
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)vtTu);

    if (ocnModule == NULL)
        return cAtErrorNullPointer;
    if (VtTuCanForceTxAlarms(ocnModule, alarmType))
        return mMethodsGet(ocnModule)->VtTuHwTxAlarmForce(ocnModule, vtTu, alarmType, cAtTrue);

    return cAtErrorModeNotSupport;
    }

eAtRet ThaOcnVtTuTxAlarmUnForce(AtChannel vtTu, uint32 alarmType)
    {
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)vtTu);

    if (ocnModule == NULL)
        return cAtErrorNullPointer;
    if (VtTuCanForceTxAlarms(ocnModule, alarmType))
        return mMethodsGet(ocnModule)->VtTuHwTxAlarmForce(ocnModule, vtTu, alarmType, cAtFalse);

    return cAtErrorModeNotSupport;
    }

uint32 ThaOcnVtTuTxForcedAlarmGet(AtChannel vtTu)
    {
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)vtTu);
    if (ocnModule)
        return mMethodsGet(ocnModule)->VtTuTxForcedAlarmGet(ocnModule, vtTu);
    return 0x0;
    }

eAtRet ThaOcnVtTuRxAlarmForce(AtChannel vtTu, uint32 alarmType)
    {
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)vtTu);

    if (ocnModule == NULL)
        return cAtErrorNullPointer;
    if (VtTuCanForceRxAlarms(ocnModule, alarmType))
        return mMethodsGet(ocnModule)->VtTuHwRxAlarmForce(ocnModule, vtTu, alarmType, cAtTrue);

    return cAtErrorModeNotSupport;
    }

eAtRet ThaOcnVtTuRxAlarmUnForce(AtChannel vtTu, uint32 alarmType)
    {
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)vtTu);

    if (ocnModule == NULL)
        return cAtErrorNullPointer;
    if (VtTuCanForceRxAlarms(ocnModule, alarmType))
        return mMethodsGet(ocnModule)->VtTuHwRxAlarmForce(ocnModule, vtTu, alarmType, cAtFalse);

    return cAtErrorModeNotSupport;
    }

uint32 ThaOcnVtTuRxForcedAlarmGet(AtChannel vtTu)
    {
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)vtTu);
    if (ocnModule)
        return mMethodsGet(ocnModule)->VtTuRxForcedAlarmGet(ocnModule, vtTu);
    return 0x0;
    }

eAtRet ThaModuleOcnVc4xTxEnable(AtSdhChannel channel, eBool vc4En)
    {
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)channel);
    if (ocnModule)
        return mMethodsGet(ocnModule)->Vc4xTxEnable(ocnModule, channel, vc4En);
    return cAtErrorNullPointer;
    }

uint32 ThaModuleOcnBaseAddress(ThaModuleOcn self)
    {
    if (self)
        return mMethodsGet(self)->BaseAddress(self);
    return cBit31_0;
    }

eBool ThaModuleOcnSlaveStsIdIsValid(ThaModuleOcn self, AtSdhChannel channel, uint8 swSts)
    {
    uint8 masterSts = AtSdhChannelSts1Get(channel);
    AtUnused(self);
    
    if ((channel == NULL) || (swSts < masterSts) || (swSts - masterSts >= AtSdhChannelNumSts(channel)))
        return cAtFalse;
        
    return cAtTrue;
    }

eBool ThaModuleOcnSohOverEthIsSupported(ThaModuleOcn self)
    {
    if (self)
        return mMethodsGet(self)->SohOverEthIsSupported(self);
    return cAtFalse;
    }

eBool ThaModuleOcnKByteOverEthIsSupported(ThaModuleOcn self)
    {
    if (self)
        return mMethodsGet(self)->KByteOverEthIsSupported(self);
    return cAtFalse;
    }

uint32 ThaModuleOcnSohOverEthBaseAddress(ThaModuleOcn self)
    {
    if (self)
        return mMethodsGet(self)->SohOverEthBaseAddress(self);
    return cBit31_0;
    }

uint32 ThaModuleOcnUpenDccdecBase(ThaModuleOcn self)
    {
    if (self)
        return mMethodsGet(self)->UpenDccdecBase(self);
    return cBit31_0;
    }

uint32 *ThaModuleOcnSohOverEthHoldRegistersGet(ThaModuleOcn self, uint16 *numberOfHoldRegisters)
    {
    if (self)
        return mMethodsGet(self)->SohOverEthHoldRegistersGet(self, numberOfHoldRegisters);

    return NULL;
    }

uint16 ThaModuleOcnSohOverEthLongReadOnCore(ThaModuleOcn self, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen, AtIpCore core)
    {
    uint16 numDwords;

    if (self == NULL)
        return 0;

    /* Just Doing nothing if feature is not supported */
    if (!ThaModuleOcnSohOverEthIsSupported(self))
        return 0;

    numDwords = mMethodsGet(self)->SohOverEthLongReadOnCore(self, localAddress, dataBuffer, bufferLen, core);
    if (AtDriverDebugIsEnabled())
        AtDeviceLongReadNotify(AtModuleDeviceGet((AtModule) self), localAddress, dataBuffer, numDwords, 0);

    return numDwords;
    }

uint16 ThaModuleOcnSohOverEthLongWriteOnCore(ThaModuleOcn self, uint32 localAddress, const uint32 *dataBuffer, uint16 bufferLen, AtIpCore core)
    {
    uint16 numDwords;

    if (self == NULL)
        return 0;

    /* Just Doing nothing if feature is not supported */
    if (!ThaModuleOcnSohOverEthIsSupported(self))
        return 0;

    numDwords = mMethodsGet(self)->SohOverEthLongWriteOnCore(self, localAddress, dataBuffer, bufferLen, core);
    if (AtDriverDebugIsEnabled())
        AtDeviceLongWriteNotify(AtModuleDeviceGet((AtModule) self), localAddress, dataBuffer, numDwords, 0);

    return numDwords;
    }

void ThaModuleOcnVtTerminate(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId, uint8 vtgId, uint8 vtId, eBool terminate)
    {
    if (self)
        mMethodsGet(self)->VtTerminate(self, channel, stsId, vtgId, vtId, terminate);
    }

eBool ThaModuleOcnCanMovePathAisRxForcingPoint(ThaModuleOcn self)
    {
    if (self)
        return mMethodsGet(self)->CanMovePathAisRxForcingPoint(self);
    return cAtFalse;
    }

eAtRet ThaModuleOcnPathAisRxForcingPointMove(ThaModuleOcn self, AtSdhAu au, eBool move)
    {
    if (self)
        return mMethodsGet(self)->PathAisRxForcingPointMove(self, au, move);
    return cAtErrorNullPointer;
    }

eBool ThaModuleOcnPathAisRxForcingPointIsMoved(ThaModuleOcn self, AtSdhAu au)
    {
    if (self)
        return mMethodsGet(self)->PathAisRxForcingPointIsMoved(self, au);
    return cAtFalse;
    }

eAtRet ThaModuleOcnNotifyLoVcAisToPoh(ThaModuleOcn self, AtSdhVc vc, eBool notified)
    {
    if (self)
        return mMethodsGet(self)->NotifyLoVcAisToPoh(self, vc, notified);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleOcnNotifyLoVcAisToPohReset(ThaModuleOcn self, AtSdhVc vc)
    {
    if (ThaModuleOcnCanMovePathAisRxForcingPoint(self))
        return ThaModuleOcnNotifyLoVcAisToPoh(self, vc, cAtFalse);
    return cAtOk;
    }

uint32 ThaModuleOcnTohGlobalS1StableMonitoringThresholdControlRegAddr(ThaModuleOcn self)
    {
    if (self)
        return mMethodsGet(self)->TohGlobalS1StableMonitoringThresholdControlRegAddr(self);
    return cInvalidUint32;
    }

uint32 ThaModuleOcnTohGlobalK1StableMonitoringThresholdControlRegAddr(ThaModuleOcn self)
    {
    if (self)
        return mMethodsGet(self)->TohGlobalK1StableMonitoringThresholdControlRegAddr(self);
    return cInvalidUint32;
    }

uint32 ThaModuleOcnTohGlobalK2StableMonitoringThresholdControlRegAddr(ThaModuleOcn self)
    {
    if (self)
        return mMethodsGet(self)->TohGlobalK2StableMonitoringThresholdControlRegAddr(self);
    return cInvalidUint32;
    }

/* Note, we have to input groupId as cBit31_0 to clear group configuration. */
eAtRet ThaOcnStsGroupIdSet(AtSdhChannel channel, uint8 stsId, uint32 groupId)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)channel);
    ThaModuleOcn ocnModule = (ThaModuleOcn)AtDeviceModuleGet(device, cThaModuleOcn);

    if (!Sts1IsValid(channel, stsId))
        return cAtErrorInvlParm;

    return mMethodsGet(ocnModule)->PiStsGroupIdSet(ocnModule, channel, stsId, groupId);
    }

AtLongRegisterAccess ThaModuleOcnSohOverEthLongRegisterAccess(ThaModuleOcn self)
    {
    if (self)
        return SohOverEthLongRegisterAccess(self);
    return NULL;
    }

eAtRet ThaOcnVc3ToTu3VcEnable(AtSdhChannel vc3, eBool enable)
    {
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)vc3);
    if (ocnModule == NULL)
        return cAtErrorNullPointer;

    return mMethodsGet(ocnModule)->Vc3ToTu3VcEnable(ocnModule, vc3, enable);
    }

eBool ThaOcnVc3ToTu3VcIsEnabled(AtSdhChannel vc3)
    {
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)vc3);
    if (ocnModule == NULL)
        return cAtErrorNullPointer;

    return mMethodsGet(ocnModule)->Vc3ToTu3VcIsEnabled(ocnModule, vc3);
    }

eAtRet ThaOcnTu3VcToVc3Enable(AtSdhChannel tu3Vc, eBool enable)
    {
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)tu3Vc);
    if (ocnModule == NULL)
        return cAtErrorNullPointer;

    return mMethodsGet(ocnModule)->Tu3VcToVc3Enable(ocnModule, tu3Vc, enable);
    }

eBool ThaOcnTu3VcToVc3IsEnabled(AtSdhChannel tu3Vc)
    {
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)tu3Vc);
    if (ocnModule == NULL)
        return cAtErrorNullPointer;

    return mMethodsGet(ocnModule)->Tu3VcToVc3IsEnabled(ocnModule, tu3Vc);
    }

uint32 ThaModuleOcnRxStsPayloadControl(ThaModuleOcn self, AtSdhChannel channel)
    {
    if (self)
        return mMethodsGet(self)->RxStsPayloadControl(self, channel);
    return cInvalidUint32;
    }

uint32 ThaModuleOcnTxStsMultiplexingControl(ThaModuleOcn self, AtSdhChannel channel)
    {
    if (self)
        return mMethodsGet(self)->TxStsMultiplexingControl(self, channel);
    return cInvalidUint32;
    }

eBool ThaModuleOcnIsSohRegister(ThaModuleOcn self, uint32 localAddress)
    {
    if (self)
        return mMethodsGet(self)->IsSohRegister(self, localAddress);
    return cAtFalse;
    }

eAtRet ThaOcnStsLoopbackSet(AtSdhChannel channel, uint8 stsId, uint8 loopbackMode)
    {
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)channel);
    if (ocnModule)
        return mMethodsGet(ocnModule)->StsLoopbackSet(ocnModule, channel, stsId, loopbackMode);
    return cAtErrorNullPointer;
    }

uint8 ThaOcnStsLoopbackGet(AtSdhChannel channel, uint8 stsId)
    {
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)channel);
    if (ocnModule)
        return mMethodsGet(ocnModule)->StsLoopbackGet(ocnModule, channel, stsId);
    return cAtLoopbackModeRelease;
    }

eAtRet ThaOcnVtTuLoopbackSet(AtSdhChannel channel, uint8 loopbackMode)
    {
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)channel);
    if (ocnModule)
        return mMethodsGet(ocnModule)->VtTuLoopbackSet(ocnModule, channel, loopbackMode);
    return cAtErrorNullPointer;
    }

uint8 ThaOcnVtTuLoopbackGet(AtSdhChannel channel)
    {
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)channel);
    if (ocnModule)
        return mMethodsGet(ocnModule)->VtTuLoopbackGet(ocnModule, channel);
    return cAtLoopbackModeRelease;
    }

eAtRet ThaModuleOcnLineTxRsDccInsByOhBusEnable(ThaModuleOcn self, uint32 lineId, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->LineTxRsDccInsByOhBusEnable(self, lineId, enable);
    return cAtErrorNullPointer;
    }

eBool  ThaModuleOcnLineTxRsDccInsByOhBusEnabled(ThaModuleOcn self, uint32 lineId)
    {
    if (self)
        return mMethodsGet(self)->LineTxRsDccInsByOhBusEnabled(self, lineId);
    return cAtFalse;
    }

eAtRet ThaModuleOcnLineTxMsDccInsByOhBusEnable(ThaModuleOcn self, uint32 lineId, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->LineTxMsDccInsByOhBusEnable(self, lineId, enable);
    return cAtErrorNullPointer;
    }

eBool  ThaModuleOcnLineTxMsDccInsByOhBusEnabled(ThaModuleOcn self, uint32 lineId)
    {
    if (self)
        return mMethodsGet(self)->LineTxMsDccInsByOhBusEnabled(self, lineId);
    return cAtFalse;
    }

eAtRet ThaOcnVc3MapDe3LiuEnable(AtSdhChannel sdhVc, eBool enable)
    {
    ThaModuleOcn moduleOcn = ThaModuleOcnFromChannel((AtChannel)sdhVc);
    if (moduleOcn)
        return mMethodsGet(moduleOcn)->Vc3MapDe3LiuEnable(moduleOcn, sdhVc, enable);
    return cAtErrorNullPointer;
    }

eBool ThaOcnVc3MapDe3LiuIsEnabled(AtSdhChannel sdhVc)
    {
    ThaModuleOcn moduleOcn = ThaModuleOcnFromChannel((AtChannel)sdhVc);
    if (moduleOcn)
        return mMethodsGet(moduleOcn)->Vc3MapDe3LiuIsEnabled(moduleOcn, sdhVc);
    return cAtFalse;
    }

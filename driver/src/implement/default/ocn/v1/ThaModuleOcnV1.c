/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : OCN
 *
 * File        : ThaModuleOcnV1.c
 *
 * Created Date: Mar 28, 2013
 *
 * Description : OCN module of previous hardware version
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../ThaModuleOcnInternal.h"
#include "ThaModuleOcnV1Reg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaModuleOcnV1
    {
    tThaModuleOcn super;

    /* Private data */
    }tThaModuleOcnV1;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods m_AtModuleOverride;

/* Save super implementations */
static const tAtModuleMethods *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaModuleOcnV1);
    }

static eAtRet PartDefaultSet(AtModule self, uint8 partId)
    {
    uint32 partOffset = ThaModuleOcnPartOffset((ThaModuleOcn)self, partId);
    uint32 regAddr = cThaRegOcnPiMastCtrl + partOffset;
    uint32 regVal  = mModuleHwRead(self, regAddr);

    mFieldIns(&regVal, cThaOcnPiStsDefGenAisDownStrEnMask, cThaOcnPiStsDefGenAisDownStrEnShift, cThaOcnPiStsDefGenAisDownStrEnRstVal);
    mFieldIns(&regVal, cThaOcnPiVtAisDownStrEnMask, cThaOcnPiVtAisDownStrEnShift, cThaOcnPiVtAisDownStrEnRstVal);
    mFieldIns(&regVal, cThaOcnPiStsAisDownStrEnMask, cThaOcnPiStsAisDownStrEnShift, cThaOcnPiStsAisDownStrEnRstVal);
    mFieldIns(&regVal, cThaOcnPiStsPohAisTypeMask, cThaOcnPiStsPohAisTypeShift, cThaOcnPiStsPohAisTypeRstVal);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet DefaultSet(AtModule self)
    {
    uint8 part_i;
    eAtRet ret = cAtOk;

    for (part_i = 0; part_i < ThaModuleOcnNumParts((ThaModuleOcn)self); part_i++)
        ret |= PartDefaultSet(self, part_i);

    return ret;
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret;

    /* Super Initialization */
    ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return DefaultSet(self);
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleOcnObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule ThaModuleOcnV1New(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

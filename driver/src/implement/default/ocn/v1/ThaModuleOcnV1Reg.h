/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : OCN
 * 
 * File        : ThaModuleOcnV1Reg.h
 * 
 * Created Date: Mar 28, 2013
 *
 * Description : Registers of OCN module (previous hardware version)
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEOCNV1REG_H_
#define _THAMODULEOCNV1REG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*------------------------------------------------------------------------------
Reg Name: OCN Pointer Interpreter Master Control
Reg Addr: 0x00050000
Reg Desc: Master configuration for SONPI
------------------------------------------------------------------------------*/
#define cThaRegOcnPiMastCtrl                          0x00050000
/*
#define cThaUnusedMask                                 cBit31_25
#define cThaUnusedShift                                24
#define cThaUnusedRstVal                               0x0
*/

/*--------------------------------------
BitField Name: Testmode2
BitField Type: R/W
BitField Desc: Set AIS downstream for each type of STS defect
--------------------------------------*/
#define cThaOcnPiStsDefGenAisDownStrEnMask                cBit31_28
#define cThaOcnPiStsDefGenAisDownStrEnShift               28
#define cThaOcnPiStsDefGenAisDownStrEnRstVal              0xF

/*--------------------------------------
BitField Name: Testmode2
BitField Type: R/W
BitField Desc: Enable insert AIS downstream when VT defects happen
--------------------------------------*/
#define cThaOcnPiVtAisDownStrEnMask                       cBit27
#define cThaOcnPiVtAisDownStrEnShift                      27
#define cThaOcnPiVtAisDownStrEnRstVal                     0x1

/*--------------------------------------
BitField Name: Testmode1
BitField Type: R/W
BitField Desc: Enable insert AIS downstream when STS defects happen
--------------------------------------*/
#define cThaOcnPiStsAisDownStrEnMask                       cBit26
#define cThaOcnPiStsAisDownStrEnShift                      26
#define cThaOcnPiStsAisDownStrEnRstVal                     0x1

/*--------------------------------------
BitField Name: OcnPiStsPohAisType[4]
BitField Type: R/W
BitField Desc: High to enable STS POH defect types to generate AIS to
downstream in case of terminating the related STS such as the STS carries VT/TU
    Bit[0]: Enable for TIM defect
    Bit[1]: Enable for Uneqiped defect
    Bit[2]: Enable for VC-AIS
    Bit[3]: Enable for PLM defect
--------------------------------------*/
#define cThaOcnPiStsPohAisTypeMask                       cBit3_0
#define cThaOcnPiStsPohAisTypeShift                      0
#define cThaOcnPiStsPohAisTypeRstVal                     0xF

/*------------------------------------------------------------------------------
Reg Name: OCN Framer Alarm Affect Control
Reg Addr: 0x00040223
Reg Desc: Configure to enable to generate AIS-P into payload data to STS/VC XC
          or RDI-L into K2 byte at the transmit direction when some kinds of
          defects are detected.
------------------------------------------------------------------------------*/
#define cThaRegOcnFrmrAlmAffCtrl                      0x00040223

/*-----------
BitField Name: TimLAisPEn
BitField Type: R/W
BitField Desc: Enable/disable the insertion of P-AIS at STS pointer generator
               when detecting TIM-L condition at SONTOHMON
               - 1: Enable.
               - 0: Disable.
------------*/
#define cThaTimLAisPEnMask                            cBit9
#define cThaTimLAisPEnShift                           9

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEOCNV1REG_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : OCN (internal module)
 * 
 * File        : ThaModuleOcn.h
 * 
 * Created Date: Sep 10, 2012
 *
 * Description :
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEOCN_H_
#define _THAMODULEOCN_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSdhLine.h"
#include "AtSdhTug.h"
#include "../pw/ThaPwInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleOcn * ThaModuleOcn;

typedef enum eThaOcnVc3PldType
    {
    cThaOcnVc3Pld7Tug2  = 0, /* 7 TUG2 */
    cThaOcnVc3PldC3     = 1, /* C3 */
    cThaOcnVc3PldDs3    = 2, /* DS3 */
    cThaOcnVc3PldE3     = 3, /* E3 */
    cThaOcnTu3Vc3Pld    = 4  /* Tu3 */
    }eThaOcnVc3PldType;

typedef enum eThaOcnVtgTug2PldType
    {
    /* Note: these two below constants are still used for other internal module
     * for default settings. So, do not delete them */
    cThaOcnVtgTug2PldE1        = 0, /* E1 */
    cThaOcnVtgTug2PldDs1       = 1, /* Ds1 */

    /* Normal constants */
    cThaOcnVtgTug2PldVt15      = 2, /* VT1.5/TU11 */
    cThaOcnVtgTug2PldVt2       = 3  /* VT2/TU12 */
    }eThaOcnVtgTug2PldType;

typedef enum eThaStsPayloadType
    {
    cThaStsDontCareVtTu    = 0,
    cThaStsVc3ContainVt1x  = 1,
    cThaStsTug3ContainVt1x = 2,
    cThaStsTug3ContainTu3  = 3
    }eThaStsPayloadType;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Methods of OCN go here */
AtModule ThaModuleOcnNew(AtDevice device);
AtModule ThaModuleOcnV1New(AtDevice device);

uint32 ThaModuleOcnBaseAddress(ThaModuleOcn self);
eAtRet ThaOcnLineRateSet(AtSdhLine line, eAtSdhLineRate rate);
eAtSdhLineRate ThaOcnLineRateGet(AtSdhLine line);

eAtRet ThaOcnSts1Init(AtSdhChannel channel, uint8 sts1Id);
eAtRet ThaOcnStsConcat(AtSdhChannel channel, uint8 startSts, uint8 numSts);
uint8 ThaOcnStsConcatGet(AtSdhChannel channel, uint8* allSts);
eAtRet ThaStsConcatRelease(AtSdhChannel channel, uint8 sts1Id);
eAtRet ThaOcnVtPathInit(AtSdhChannel channel, uint8 stsId, uint8 vtgId, uint8 vtId);
eAtRet ThaOcnVtgPldSet (AtSdhChannel channel, uint8 stsId, uint8 vtgId, eThaOcnVtgTug2PldType  vtPldType);
eAtRet ThaSdhChannelHwStsGet(AtSdhChannel sdhChannel, eAtModule module, uint8 swSts, uint8* sliceId, uint8 *hwStsInSlice);
eAtRet ThaSdhLineHwIdGet(AtSdhLine sdhLine, eAtModule phyModule, uint8* sliceId, uint8 *hwLineInSlice);

/* OCN module helper functions */
eAtRet ThaOcnStsPohInsertEnable(AtSdhChannel channel, uint8 stsId, eBool enable);
eBool ThaOcnStsPohInsertIsEnabled(AtSdhChannel channel, uint8 stsId);
void ThaOcnVtPohInsertEnable(AtSdhChannel channel, uint8 stsId, uint8 vtgId, uint8 vtId, eBool enable);
eBool ThaOcnVtPohInsertIsEnabled(AtSdhChannel channel, uint8 stsId, uint8 vtgId, uint8 vtId);
eAtRet ThaOcnStsPgSlaveIndicate(AtSdhChannel channel, uint8 stsId, eBool isSlaver);
eBool ThaOcnVtPohInsertIsEnabled(AtSdhChannel channel, uint8 stsId, uint8 vtgId, uint8 vtId);
eAtRet ThaOcnVc3PldSet(AtSdhChannel vc3, eThaOcnVc3PldType vc3Pld);
ThaModuleOcn ThaModuleOcnFromChannel(AtChannel channel);
eAtRet ThaOcnTug3PldSet(AtSdhChannel tug3, eAtSdhTugMapType tug3MapType);
eAtRet ThaModuleOcnRxStsTermEnable(AtSdhChannel channel, uint8 stsId, eBool enable);
eAtRet ThaOcnVc4xPayloadDefault(ThaModuleOcn self, AtSdhChannel sdhVc);
eAtRet ThaOcnStsTxPayloadTypeSet(AtSdhChannel channel, uint8 stsId, eThaStsPayloadType payloadType);
uint8 ThaOcnStsTxPayloadTypeGet(AtSdhChannel channel, uint8 stsId);
uint8 ThaOcnStsRxPayloadTypeGet(AtSdhChannel channel, uint8 stsId);
eBool ThaModuleOcnRxStsIsTerminated(AtSdhChannel channel, uint8 stsId);
uint8 ThaOcnVtgPldGet(AtSdhChannel channel, uint8 stsId, uint8 vtgId);
eThaOcnVc3PldType ThaOcnVc3PldGet(AtSdhChannel vc3);
void ThaModuleOcnVtTerminate(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId, uint8 vtgId, uint8 vtId, eBool terminate);
eAtRet ThaOcnStsGroupIdSet(AtSdhChannel channel, uint8 stsId, uint32 groupId);

/* VC-3 interworking */
eAtRet ThaOcnVc3ToTu3VcEnable(AtSdhChannel vc3, eBool enable);
eBool ThaOcnVc3ToTu3VcIsEnabled(AtSdhChannel vc3);
eAtRet ThaOcnTu3VcToVc3Enable(AtSdhChannel tu3Vc, eBool enable);
eBool ThaOcnTu3VcToVc3IsEnabled(AtSdhChannel tu3Vc);

/* VT/TU SS bit */
eAtRet ThaOcnPpSsTxVtSet(AtSdhChannel vt, uint8 insSsBit,eBool insSsBitEn);
uint8 ThaOcnPpSsTxVtGet(AtSdhChannel vt);
eAtRet ThaOcnVtTuExpectedSsSet(AtSdhPath vtTu, uint8 value);
uint8 ThaOcnVtTuExpectedSsGet(AtSdhPath vtTu);
eAtRet ThaOcnVtTuSsCompareEnable(AtSdhPath vtTu, eBool enable);
eBool ThaOcnVtTuSsCompareIsEnabled(AtSdhPath vtTu);

/* VT/TU alarm forcing */
uint32 ThaOcnVtTuTxForcibleAlarms(ThaModuleOcn self);
eAtRet ThaOcnVtTuTxAlarmForce(AtChannel vtTu, uint32 alarmType);
eAtRet ThaOcnVtTuTxAlarmUnForce(AtChannel vtTu, uint32 alarmType);
uint32 ThaOcnVtTuTxForcedAlarmGet(AtChannel vtTu);
uint32 ThaOcnVtTuRxForcibleAlarms(ThaModuleOcn self);
eAtRet ThaOcnVtTuRxAlarmForce(AtChannel vtTu, uint32 alarmType);
eAtRet ThaOcnVtTuRxAlarmUnForce(AtChannel vtTu, uint32 alarmType);
uint32 ThaOcnVtTuRxForcedAlarmGet(AtChannel vtTu);

/* Parts management */
uint32 ThaModuleOcnPartOffset(ThaModuleOcn self, uint8 partId);
uint8 ThaModuleOcnNumParts(ThaModuleOcn self);
uint8 ThaModuleOcnNumStsInOneSlice(ThaModuleOcn self);
uint8 ThaModuleOcnNumSlices(ThaModuleOcn self);

eAtRet ThaModuleOcnPwTohByteEnable(ThaModuleOcn self, AtPw pw, const tThaTohByte *tohByte, eBool enable);
uint32 ThaModuleOcnStsDefaultOffset(ThaModuleOcn self, AtSdhChannel channel, uint8 sts);
uint32 ThaModuleOcnStsVtDefaultOffset(ThaModuleOcn self, AtSdhChannel channel, uint8 sts, uint8 vtg, uint8 vt);
uint32 ThaModuleOcnTxStsVtDefaultOffset(ThaModuleOcn self, AtSdhChannel channel, uint8 sts, uint8 vtg, uint8 vt);
uint32 ThaModuleOcnLineDefaultOffset(ThaModuleOcn self, AtSdhChannel sdhLine);
uint32 ThaModuleOcnLineTxDefaultOffset(ThaModuleOcn self, AtSdhChannel sdhLine);
eAtRet ThaSdhChannel2HwMasterStsId(AtSdhChannel channel, eAtModule module, uint8 *slice, uint8 *hwStsInSlice);
void ThaModuleOcnSdhChannelRegsShow(AtSdhChannel channel);
eBool ThaOcnChannelIsConcatenated(AtSdhChannel sdhChannel);
eAtRet ThaModuleOcnChannelPohHwStsGet(ThaModuleOcn self, AtSdhChannel channel, uint8 *hwFlatSts);
eAtRet ThaModuleOcnChannelStsIdSw2HwGet(ThaModuleOcn self, AtSdhChannel channel, uint8 sts1Id, uint8 *slice, uint8 *sts);
uint8 Stm16SdhChannelSwStsId2HwFlatId(ThaModuleOcn self, AtSdhChannel channel, uint8 sts1Id);
eAtRet ThaModuleOcnVc4xTxEnable(AtSdhChannel channel, eBool vc4En);

/* Slaver StsId Checking */
eBool ThaModuleOcnSlaveStsIdIsValid(ThaModuleOcn self, AtSdhChannel channel, uint8 swSts);

eBool ThaModuleOcnCanMovePathAisRxForcingPoint(ThaModuleOcn self);
eAtRet ThaModuleOcnPathAisRxForcingPointMove(ThaModuleOcn self, AtSdhAu au, eBool downstream);
eBool ThaModuleOcnPathAisRxForcingPointIsMoved(ThaModuleOcn self, AtSdhAu au);
eAtRet ThaModuleOcnNotifyLoVcAisToPoh(ThaModuleOcn self, AtSdhVc vc, eBool notified);
eAtRet ThaModuleOcnNotifyLoVcAisToPohReset(ThaModuleOcn self, AtSdhVc vc);

/* For handle SECTION Overhead Bytes Over ETH */
eBool ThaModuleOcnSohOverEthIsSupported(ThaModuleOcn self);
uint32 ThaModuleOcnSohOverEthBaseAddress(ThaModuleOcn self);
uint32 ThaModuleOcnUpenDccdecBase(ThaModuleOcn self);
uint32 *ThaModuleOcnSohOverEthHoldRegistersGet(ThaModuleOcn self, uint16 *numberOfHoldRegisters);
uint16 ThaModuleOcnSohOverEthLongReadOnCore(ThaModuleOcn self, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen, AtIpCore core);
uint16 ThaModuleOcnSohOverEthLongWriteOnCore(ThaModuleOcn self, uint32 localAddress, const uint32 *dataBuffer, uint16 bufferLen, AtIpCore core);
AtLongRegisterAccess ThaModuleOcnSohOverEthLongRegisterAccess(ThaModuleOcn self);
eBool ThaModuleOcnIsSohRegister(ThaModuleOcn self, uint32 localAddress);
eBool ThaModuleOcnKByteOverEthIsSupported(ThaModuleOcn self);

eAtRet ThaModuleOcnLineTxRsDccInsByOhBusEnable(ThaModuleOcn self, uint32 lineId, eBool enable);
eBool  ThaModuleOcnLineTxRsDccInsByOhBusEnabled(ThaModuleOcn self, uint32 lineId);
eAtRet ThaModuleOcnLineTxMsDccInsByOhBusEnable(ThaModuleOcn self, uint32 lineId, eBool enable);
eBool  ThaModuleOcnLineTxMsDccInsByOhBusEnabled(ThaModuleOcn self, uint32 lineId);

uint32 ThaModuleOcnTohGlobalS1StableMonitoringThresholdControlRegAddr(ThaModuleOcn self);
uint32 ThaModuleOcnTohGlobalK1StableMonitoringThresholdControlRegAddr(ThaModuleOcn self);
uint32 ThaModuleOcnTohGlobalK2StableMonitoringThresholdControlRegAddr(ThaModuleOcn self);

uint32 ThaModuleOcnRxStsPayloadControl(ThaModuleOcn self, AtSdhChannel channel);
uint32 ThaModuleOcnTxStsMultiplexingControl(ThaModuleOcn self, AtSdhChannel channel);

/* Path loopback */
eAtRet ThaOcnStsLoopbackSet(AtSdhChannel channel, uint8 stsId, uint8 loopbackMode);
uint8 ThaOcnStsLoopbackGet(AtSdhChannel channel, uint8 stsId);
eAtRet ThaOcnVtTuLoopbackSet(AtSdhChannel channel, uint8 loopbackMode);
uint8 ThaOcnVtTuLoopbackGet(AtSdhChannel channel);

/* VC-3 mapping DS3/E3 LIU */
eAtRet ThaOcnVc3MapDe3LiuEnable(AtSdhChannel sdhVc, eBool enable);
eBool ThaOcnVc3MapDe3LiuIsEnabled(AtSdhChannel sdhVc);

/* Product concretes */
AtModule Tha60060011ModuleOcnNew(AtDevice device);
AtModule Tha60150011ModuleOcnNew(AtDevice device);
AtModule Tha60210011ModuleOcnNew(AtDevice device);
AtModule Tha61210011ModuleOcnNew(AtDevice device);
AtModule Tha60210031ModuleOcnNew(AtDevice device);
AtModule Tha6A000010ModuleOcnNew(AtDevice device);
AtModule Tha60210012ModuleOcnNew(AtDevice device);
AtModule Tha60210061ModuleOcnNew(AtDevice device);
AtModule Tha60290011ModuleOcnNew(AtDevice device);
AtModule Tha60290022ModuleOcnNew(AtDevice device);
AtModule Tha6A290011ModuleOcnNew(AtDevice device);
AtModule Tha60291022ModuleOcnNew(AtDevice device);
AtModule Tha60290081ModuleOcnNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEOCN_H_ */


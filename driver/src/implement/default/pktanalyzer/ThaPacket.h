/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : ThaPacket.h
 * 
 * Created Date: Feb 28, 2015
 *
 * Description : Common declarations of all Thalassa packet sub classes
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPACKET_H_
#define _THAPACKET_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPacket ThaEthPacketV2New(uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode);

/* Product concretes */
AtPacket Tha60000031EthPacketNew(uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode);
AtPacket Tha60031031EthPacketNew(uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode);
AtPacket Tha60031032EthPacketNew(uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode);
AtPacket Tha60031131EthHigigPacketNew(uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode);
AtPacket Tha60091132EthHigigPacketNew(uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode);
AtPacket ThaStmPwProductEthPacketNew(uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode);

#ifdef __cplusplus
}
#endif
#endif /* _THAPACKET_H_ */


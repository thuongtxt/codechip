/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : ThaPktAnalyzerReg.h
 * 
 * Created Date: Jan 2, 2013
 *
 * Description : Packet analyzer registers
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPKTANALYZERREG_H_
#define _THAPKTANALYZERREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/*------------------------------------------------------------------------------
Reg Name: Debug Dump Connection ID
Reg Addr: 0x40f002
Reg Desc: This register is Debug Dump Packet Control
------------------------------------------------------------------------------*/
#define cThaDiagPktDumpModeConId         0x2

#define cThaDebugDumChannelIdMask        cBit15_0
#define cThaDebugDumChannelIdShift       0

#define cThaDebugDumPktTypeMask          cBit19_16
#define cThaDebugDumPktTypeShift         16

/*------------------------------------------------------------------------------
Reg Name: Debug Dump Packet Control
Reg Addr: 0x40f000
Reg Desc: This register is Debug Dump Packet Control
------------------------------------------------------------------------------*/
#define cThaDiagPktDumpModeCtrl          0x0

#define cThaDebugDumPktSourceMask        cBit7_0
#define cThaDebugDumPktSourceShift       0

#define cThaDebugDumPktCtlSelMask        cBit9_8
#define cThaDebugDumPktCtlSelShift       8

#define cThaDebugDumPktCtlTypeMask       cBit10
#define cThaDebugDumPktCtlTypeShift      10

#define cThaDebugDumPktCtlRoMask         cBit11
#define cThaDebugDumPktCtlRoShift        11

#define cThaDebugDumPktCtlLenMask        cBit15_12
#define cThaDebugDumPktCtlLenShift       12

#define cThaDebugDumPktCtlPat0Mask       cBit19_16
#define cThaDebugDumPktCtlPat0Shift      16

/*------------------------------------------------------------------------------
Reg Name: Debug Dump Packet Count
Reg Addr: 0x40f004
Reg Desc: This register is Debug Dump Packet Control
------------------------------------------------------------------------------*/
#define cThaDiagPktDumpModePktCount         0x4

#define cThaDebugDumPktCntMask              cBit7_0
#define cThaDebugDumPktCntShift             0

/*------------------------------------------------------------------------------
Reg Name: Debug Dump Packet information
Reg Addr: 0x40f100
Reg Desc: This register is Debug Dump Packet Control
------------------------------------------------------------------------------*/
#define cThaDiagPktDumpModePktInfo(EthDumPktInfoId)  (0x100 + (EthDumPktInfoId))

#define cThaDebugDumPktInfoStartAddrMask    cBit11_0
#define cThaDebugDumPktInfoStartAddrShift   0

#define cThaDebugDumPktInfoLenMask          cBit29_16
#define cThaDebugDumPktInfoLenShift         16

#define cThaDebugDumPktByteGetMask(bIndex)			(cBit31_24 >> (bIndex * 8))
#define cThaDebugDumPktByteGetShift(bIndex)			(24 - (bIndex * 8))

/*------------------------------------------------------------------------------
Reg Name: Debug Dump Packet Data Report
Reg Addr: 0x40f400
Reg Desc: This register is Debug Dump Packet Control
------------------------------------------------------------------------------*/
#define cThaDiagPktDumpModePktDataReg(EthDumPktBuffLen)         (uint32)(0x400 + (EthDumPktBuffLen))

#ifdef __cplusplus
}
#endif
#endif /* _THAPKTANALYZERREG_H_ */


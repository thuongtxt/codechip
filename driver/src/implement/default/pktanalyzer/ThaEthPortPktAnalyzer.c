/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : ThaEthPortPktAnalyzer.c
 *
 * Created Date: Dec 20, 2012
 *
 * Description : Ethernet Port packet analyzer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaEthPortPktAnalyzerInternal.h"
#include "AtEthPacket.h"
#include "ThaPacketReaderInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPktAnalyzerMethods  m_AtPktAnalyzerOverride;
static tThaPktAnalyzerMethods m_ThaPktAnalyzerOverride;

/* Save super implementation */
static const tAtPktAnalyzerMethods *m_AtPktAnalyzerMethods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Init(AtPktAnalyzer self)
    {
    /* Super */
    m_AtPktAnalyzerMethods->Init(self);
    ThaPktAnalyzerDumpModeSet((ThaPktAnalyzer)self, cThaPktAnalyzerDumpGbeTx);
    mMethodsGet(self)->Recapture(self);
    }

static AtPacket PacketCreate(AtPktAnalyzer self, uint8 *data, uint32 length, eAtPktAnalyzerDirection direction)
    {
    return AtPacketFactoryEthPacketCreate(AtPktAnalyzerPacketFactory(self), data, length, cAtPacketCacheModeCacheData, direction);
    }

static void AllTxPacketsRead(ThaPktAnalyzer self)
	{
	ThaPacketReaderPacketRead(ThaPktAnalyzerTxPacketReaderGet(self), cThaPktAnalyzerDumpGbeTx);
	}

static void AllRxPacketsRead(ThaPktAnalyzer self)
	{
	ThaPacketReaderPacketRead(ThaPktAnalyzerRxPacketReaderGet(self), cThaPktAnalyzerDumpGbeRx);
	}

static AtList CaptureTxPackets(AtPktAnalyzer self)
    {
	ThaPacketReaderPacketRead(ThaPktAnalyzerTxPacketReaderGet((ThaPktAnalyzer)self), cThaPktAnalyzerDumpGbeTx);
    return AtPktAnalyzerPacketListGet(self);
    }

static AtList CaptureRxPackets(AtPktAnalyzer self)
    {
	ThaPacketReaderPacketRead(ThaPktAnalyzerRxPacketReaderGet((ThaPktAnalyzer)self), cThaPktAnalyzerDumpGbeRx);
    return AtPktAnalyzerPacketListGet(self);
    }

static uint32 PartOffset(ThaPktAnalyzer self)
    {
    ThaEthPort port = (ThaEthPort)AtPktAnalyzerChannelGet((AtPktAnalyzer)self);
    return ThaEthPortPartOffset(port);
    }

static void OverrideThaPktAnalyzer(ThaEthPortPktAnalyzer self)
    {
    ThaPktAnalyzer pktAnalyzer = (ThaPktAnalyzer)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPktAnalyzerOverride, mMethodsGet(pktAnalyzer), sizeof(tThaPktAnalyzerMethods));
        mMethodOverride(m_ThaPktAnalyzerOverride, PartOffset);
        mMethodOverride(m_ThaPktAnalyzerOverride, AllTxPacketsRead);
        mMethodOverride(m_ThaPktAnalyzerOverride, AllRxPacketsRead);
        }

    mMethodsSet(pktAnalyzer, &m_ThaPktAnalyzerOverride);
    }

static void OverrideAtPktAnalyzer(ThaEthPortPktAnalyzer self)
    {
    AtPktAnalyzer pktAnalyzer = (AtPktAnalyzer)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPktAnalyzerMethods = mMethodsGet(pktAnalyzer);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPktAnalyzerOverride, m_AtPktAnalyzerMethods, sizeof(tAtPktAnalyzerMethods));
        mMethodOverride(m_AtPktAnalyzerOverride, Init);
        mMethodOverride(m_AtPktAnalyzerOverride, PacketCreate);
        mMethodOverride(m_AtPktAnalyzerOverride, CaptureTxPackets);
        mMethodOverride(m_AtPktAnalyzerOverride, CaptureRxPackets);
        }

    mMethodsSet(pktAnalyzer, &m_AtPktAnalyzerOverride);
    }

static void Override(ThaEthPortPktAnalyzer self)
    {
    OverrideAtPktAnalyzer(self);
    OverrideThaPktAnalyzer(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaEthPortPktAnalyzer);
    }

AtPktAnalyzer ThaEthPortPktAnalyzerObjectInit(AtPktAnalyzer self, AtEthPort port)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPktAnalyzerObjectInit(self, (AtChannel)port) == NULL)
        return NULL;

    /* Setup class */
    Override((ThaEthPortPktAnalyzer)self);
    m_methodsInit = 1;

    return self;
    }

AtPktAnalyzer ThaEthPortPktAnalyzerNew(AtEthPort port)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtPktAnalyzer newPktAnalyzer = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPktAnalyzer == NULL)
        return NULL;

    /* Construct it */
    return ThaEthPortPktAnalyzerObjectInit(newPktAnalyzer, port);
    }

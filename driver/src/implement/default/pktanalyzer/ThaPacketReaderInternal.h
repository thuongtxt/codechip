/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : ThaPacketReaderInternal.h
 *
 * Created Date: Sep 9, 2016
 *
 * Description : Thalassa default packet reader
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _THAPACKETREADERINTERNAL_H_
#define _THAPACKETREADERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaPktAnalyzerInternal.h"
#include "ThaPacketReader.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPacketReaderMethods
	{
	void (*Read)(ThaPacketReader self, eThaPktAnalyzerPktDumpMode dumpMode);
	AtPktAnalyzer (*AnalyzerGet)(ThaPacketReader self);
	}tThaPacketReaderMethods;

typedef struct tThaPacketReader
	{
	tAtObject super;
	const tThaPacketReaderMethods *methods;

	/* Private data */
	AtPktAnalyzer analyzer;
	}tThaPacketReader;

typedef struct tThaPacketReaderDefault
	{
	tThaPacketReader super;
	}tThaPacketReaderDefault;

typedef struct tThaPacketReaderV2
	{
	tThaPacketReaderDefault super;
	}tThaPacketReaderV2;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPacketReader ThaPacketReaderObjectInit(ThaPacketReader self, AtPktAnalyzer analyzer);
ThaPacketReader ThaPacketReaderDefaultObjectInit(ThaPacketReader self, AtPktAnalyzer analyzer);
ThaPacketReader ThaPacketReaderV2ObjectInit(ThaPacketReader self, AtPktAnalyzer analyzer);
void ThaPacketReaderPacketRead(ThaPacketReader self, eThaPktAnalyzerPktDumpMode dumpMode);

#ifdef __cplusplus
}
#endif
#endif /* _THAPACKETREADERINTERNAL_H_ */

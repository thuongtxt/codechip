/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : ThaModulePktAnalyzerPpp.c
 *
 * Created Date: Apr 4, 2015
 *
 * Description : Packet analyzer for PPP product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtHdlcChannel.h"
#include "ThaModulePktAnalyzerInternal.h"
#include "ThaPktAnalyzer.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePktAnalyzerMethods m_AtModulePktAnalyzerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPktAnalyzer EthFlowPktAnalyzerCreate(AtModulePktAnalyzer self, AtEthFlow flow)
    {
    AtUnused(self);
    return ThaEthFlowPktAnalyzerNew(flow);
    }

static AtPktAnalyzer PppLinkPktAnalyzerCreate(AtModulePktAnalyzer self, AtPppLink link)
    {
    AtUnused(self);
    return ThaPppLinkPktAnalyzerNew(link);
    }

static AtPktAnalyzer HdlcChannelPktAnalyzerCreate(AtModulePktAnalyzer self, AtHdlcChannel channel)
    {
    return mMethodsGet(self)->PppLinkPktAnalyzerCreate(self, (AtPppLink)AtHdlcChannelHdlcLinkGet(channel));
    }

static void OverrideAtModulePktAnalyzer(AtModulePktAnalyzer self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePktAnalyzerOverride, mMethodsGet(self), sizeof(m_AtModulePktAnalyzerOverride));

        mMethodOverride(m_AtModulePktAnalyzerOverride, EthFlowPktAnalyzerCreate);
        mMethodOverride(m_AtModulePktAnalyzerOverride, PppLinkPktAnalyzerCreate);
        mMethodOverride(m_AtModulePktAnalyzerOverride, HdlcChannelPktAnalyzerCreate);
        }

    mMethodsSet(self, &m_AtModulePktAnalyzerOverride);
    }

static void Override(AtModulePktAnalyzer self)
    {
    OverrideAtModulePktAnalyzer(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaModulePktAnalyzerPpp);
    }

static AtModulePktAnalyzer ObjectInit(AtModulePktAnalyzer self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModulePktAnalyzerObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePktAnalyzer ThaModulePktAnalyzerPppNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePktAnalyzer newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newModule, device);
    }

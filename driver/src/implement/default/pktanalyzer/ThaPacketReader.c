/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : ThaPacketReader.c
 *
 * Created Date: Sep 9, 2016
 *
 * Description : Packet reader
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaPacketReaderInternal.h"

/*--------------------------- Define -----------------------------------------*/
static uint8 m_methodsInit = 0;
static tThaPacketReaderMethods m_methods;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Read(ThaPacketReader self, eThaPktAnalyzerPktDumpMode dumpMode)
	{
	AtUnused(self);
	AtUnused(dumpMode);
	}

static AtPktAnalyzer AnalyzerGet(ThaPacketReader self)
	{
	return self->analyzer;
	}

static void MethodsInit(ThaPacketReader self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, Read);
        mMethodOverride(m_methods, AnalyzerGet);
        }

    mMethodsSet(self, &m_methods);
    }

/* Constructor */
ThaPacketReader ThaPacketReaderObjectInit(ThaPacketReader self, AtPktAnalyzer analyzer)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaPacketReader));

    /* Call super constructor to reuse all of its implementation */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    self->analyzer = analyzer;
    m_methodsInit = 1;

    return self;
    }

void ThaPacketReaderPacketRead(ThaPacketReader self, eThaPktAnalyzerPktDumpMode dumpMode)
	{
	if (self)
		mMethodsGet(self)->Read(self, dumpMode);
	}

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : ThaModulePktAnalyzer.h
 * 
 * Created Date: May 3, 2013
 *
 * Description : Packet analyzer for Thalassa product
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEPKTANALYZER_H_
#define _THAMODULEPKTANALYZER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModulePktAnalyzer.h" /* Super */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModulePktAnalyzer * ThaModulePktAnalyzer;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePktAnalyzer ThaModulePktAnalyzerNew(AtDevice device);
AtModulePktAnalyzer ThaModulePktAnalyzerPppNew(AtDevice device);
AtModulePktAnalyzer ThaModulePktAnalyzerPwNew(AtDevice device);

eBool ThaModulePktAnalyzerHigigEthPacketIsSupported(ThaModulePktAnalyzer self);

/* Product concretes */
AtModulePktAnalyzer Tha60031033ModulePktAnalyzerNew(AtDevice device);
AtModulePktAnalyzer Tha60000031ModulePktAnalyzerNew(AtDevice device);
AtModulePktAnalyzer Tha60031031ModulePktAnalyzerNew(AtDevice device);
AtModulePktAnalyzer Tha60031131ModulePktAnalyzerNew(AtDevice device);
AtModulePktAnalyzer Tha60091023ModulePktAnalyzerNew(AtDevice device);
AtModulePktAnalyzer Tha60150011ModulePktAnalyzerNew(AtDevice device);
AtModulePktAnalyzer Tha60031032ModulePktAnalyzerNew(AtDevice device);
AtModulePktAnalyzer Tha60091132ModulePktAnalyzerNew(AtDevice device);
AtModulePktAnalyzer Tha60091135ModulePktAnalyzerNew(AtDevice device);
AtModulePktAnalyzer Tha60210011ModulePktAnalyzerNew(AtDevice device);
AtModulePktAnalyzer Tha60210012ModulePktAnalyzerNew(AtDevice device);
AtModulePktAnalyzer Tha60290011ModulePktAnalyzerNew(AtDevice device);
AtModulePktAnalyzer ThaStmPwProductModulePktAnalyzerNew(AtDevice device);
AtModulePktAnalyzer Tha60290021ModulePktAnalyzerNew(AtDevice device);
AtModulePktAnalyzer Tha60290011ModulePktAnalyzerNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEPKTANALYZER_H_ */


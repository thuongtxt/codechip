/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : ThaPktAnalyzerInternal.h
 * 
 * Created Date: Dec 18, 2012
 *
 * Description : Thalassa default packet analyzer
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPKTANALYZERINTERNAL_H_
#define _THAPKTANALYZERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "atclib.h"
#include "../../../generic/pktanalyzer/AtPktAnalyzerInternal.h"
#include "../man/ThaDeviceInternal.h"
#include "../util/ThaUtil.h"
#include "ThaPktAnalyzer.h"
#include "ThaPktAnalyzerReg.h"
#include "ThaPacket.h"
#include "ThaPacketFactory.h"
#include "ThaPacketReader.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cDumpControlReg  0x0
#define cBufferStartReg  0x1000
#define cBufferStopReg   0x10FE

/* Packet meta data */
#define cNumBytesMask       cBit1_0
#define cNumBytesShift      0
#define cEndOfPacketMask    cBit2
#define cStartOfPacketMask  cBit3

#define cStateFindStartOfPacket 0
#define cStateReading           1
#define cStateEndOfPacket       2
/*--------------------------- Macros -----------------------------------------*/
#define mMetaDwordIndex(self) mMethodsGet(self)->MetaDwordIndex(self)

/*--------------------------- Typedefs ---------------------------------------*/
typedef enum eThaPktAnalyzerPktDumpMode
    {
    cThaPktAnalyzerDumpGbeTx,
    cThaPktAnalyzerDumpGbeRx,
    cThaPktAnalyzerDumpMpigToGbe,
    cThaPktAnalyzerDumpDecToMpig,
    cThaPktAnalyzerDumpMpigOut,
    cThaPktAnalyzerDumpMpegToEnc,
    cThaPktAnalyzerDumpGbeToCla,
    cThaPktAnalyzerDumpClaToMpeg,
    cThaPktAnalyzerDumpClaToPda,
    cThaPktAnalyzerDumpPdaToMap,
    cThaPktAnalyzerDumpPlaOut,
    cThaPktAnalyzerDumpPweRx,
    cThaPktAnalyzerDumpPlaTxHeader,
	cThaPktAnalyzerDumpPweRxHeader,
    cThaPktAnalyzerDumpUnknown
    }eThaPktAnalyzerPktDumpMode;

typedef struct tThaPktAnalyzerMethods
    {
    void (*ChannelIdSet)(ThaPktAnalyzer self);
    uint32 (*PartOffset)(ThaPktAnalyzer self);
    eBool (*SkipLastInCompletePacket)(ThaPktAnalyzer self);
    uint32 (*StartAddress)(ThaPktAnalyzer self);
    uint32 (*EthPortHwDumpMode)(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode);
    uint32 (*PppLinkHwDumpMode)(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode);
    uint32 (*PppLinkDumpModeHw2Sw)(ThaPktAnalyzer self, uint32 hwPktDumpMode);
    uint32 (*EthPortDumpModeHw2Sw)(ThaPktAnalyzer self, uint32 hwPktDumpMode);
    eBool (*ShouldRecapture)(ThaPktAnalyzer self, uint8 pktDumpMode, uint8 currentDumpMode);
    eBool (*JustDisplayRawPacket)(ThaPktAnalyzer self);
    uint32 (*NumPacketInBuffer)(ThaPktAnalyzer self);
    uint32 (*PacketLenghthGet)(ThaPktAnalyzer self, uint32 packetId, uint16 *startAddress);
    uint32 (*PacketDumpDataRegister)(ThaPktAnalyzer self, uint32 address_i);
    void (*DumpModeSet)(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode);
    uint32 (*DumpModeGet)(ThaPktAnalyzer self);

    /* For reading packets by state machine */
    ThaPacketReader (*TxPacketReaderCreate)(ThaPktAnalyzer self);
    ThaPacketReader (*RxPacketReaderCreate)(ThaPktAnalyzer self);
    void (*AllTxPacketsRead)(ThaPktAnalyzer self);
	void (*AllRxPacketsRead)(ThaPktAnalyzer self);
    eBool (*DumpModeIsSupported)(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode);
    uint32 (*FirstAddressOfBuffer)(ThaPktAnalyzer self);
    uint32 (*LastAddressOfBuffer)(ThaPktAnalyzer self);
    eBool (*IsStartOfPacket)(ThaPktAnalyzer self, uint32 address, uint32 *data);
    eBool (*IsEndOfPacket)(ThaPktAnalyzer self, uint32 address, uint32 *data);
    eBool (*IsErrorPacket)(ThaPktAnalyzer self, uint32 address, uint32 *data);
    uint8 (*EntryRead)(ThaPktAnalyzer self, uint32 address, uint32 *data);
    uint16 (*EntryInfoRead)(ThaPktAnalyzer self, uint32 address, uint32 *data);
    uint8 (*MetaDwordIndex)(ThaPktAnalyzer self);
    uint16 (*LongRead)(ThaPktAnalyzer self, uint32 address, uint32 *data);
    eBool (*AutoFlushPackets)(ThaPktAnalyzer self);
    }tThaPktAnalyzerMethods;

typedef struct tThaPktAnalyzer
    {
    tAtPktAnalyzer super;
    const tThaPktAnalyzerMethods *methods;

    /* Private */
    ThaPacketReader txPktReader;
    ThaPacketReader rxPktReader;
    }tThaPktAnalyzer;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPktAnalyzer ThaPktAnalyzerObjectInit(AtPktAnalyzer self, AtChannel channel);

AtList ThaPktAnalyzerCapturePackets(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode);
void ThaPktAnalyzerDumpModeSet(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode);
uint32 ThaPktAnalyzerDumpModeGet(ThaPktAnalyzer self);
uint32 ThaPktAnalyzerRead(ThaPktAnalyzer self, uint32 address);
void ThaPktAnalyzerWrite(ThaPktAnalyzer self, uint32 address, uint32 value);

eAtPktAnalyzerDirection ThaPktAnalyzerDirectionFromDumpMode(eThaPktAnalyzerPktDumpMode pktDumpMode);
void ThaPktAnalyzerAllPacketRead(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode);
ThaPacketReader ThaPktAnalyzerTxPacketReaderGet(ThaPktAnalyzer self);
ThaPacketReader ThaPktAnalyzerRxPacketReaderGet(ThaPktAnalyzer self);
void ThaPktAnalyzerCreateAllPacketReader(ThaPktAnalyzer self);
void ThaPktAnalyzerPktDumpModeSet(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode);

#ifdef __cplusplus
}
#endif
#endif /* _THAPKTANALYZERINTERNAL_H_ */


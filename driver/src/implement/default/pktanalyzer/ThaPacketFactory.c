/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : ThaPacketFactory.c
 *
 * Created Date: Mar 29, 2014
 *
 * Description : Packet factory
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtMplsPacket.h"
#include "AtMefPacket.h"
#include "AtUdpPacket.h"
#include "AtPwCep.h"
#include "ThaPacketFactoryInternal.h"

#include "../cla/ThaModuleCla.h"
#include "../cla/hbce/ThaHbce.h"
#include "../man/ThaDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((tThaPacketFactory *)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPacketFactoryMethods m_AtPacketFactoryOverride;

/* Save super implementation */
static const tAtPacketFactoryMethods *m_AtPacketFactoryMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPw FindPwFromLabelInPacket(AtPacketFactory self, eAtPwPsnType psnType, uint32 label, eAtPacketFactoryDirection direction)
    {
    ThaModuleCla claModule = (ThaModuleCla)AtDeviceModuleGet(mThis(self)->device, cThaModuleCla);
    AtModuleEth ethModule  = (AtModuleEth)AtDeviceModuleGet(mThis(self)->device, cAtModuleEth);
    ThaClaPwController controller = ThaModuleClaPwControllerGet(claModule);
    uint8 part_i;
    ThaPwAdapter pwAdapter = NULL;

    /* Cannot lookup for PW at TX direction */
    if (direction == cAtPacketFactoryDirectionTx)
        return NULL;

    for (part_i = 0; part_i < ThaModuleClaNumParts(claModule); part_i++)
        {
        ThaHbce hbce = ThaClaPwControllerHbceForPart(controller, part_i);
        uint8 ethPort_i;

        /* Find all of possible cases */
        for (ethPort_i = 0; ethPort_i < AtModuleEthMaxPortsGet(ethModule); ethPort_i++)
            {
            AtEthPort port = AtModuleEthPortGet(ethModule, ethPort_i);
            pwAdapter = ThaHbcePwAdapterByPsn(hbce, label, psnType, port);
            if (pwAdapter)
                break;
            }

        /* Found */
        if (pwAdapter)
            break;
        }

    return ThaPwAdapterPwGet(pwAdapter);
    }

static eAtPwPacketType CepPwPacketFractionalMode(AtPw pw)
    {
    eBool ebmEnabled = AtPwCepCwEbmIsEnabled((AtPwCep)pw);
    uint32 channelType = AtSdhChannelTypeGet((AtSdhChannel)AtPwBoundCircuitGet(pw));
    switch (channelType)
        {
        case cAtSdhChannelTypeVc3: return ebmEnabled ? cAtPwPacketTypeCEPVc3FactionalEbm : cAtPwPacketTypeCEPVc3FactionalNoEbm;
        case cAtSdhChannelTypeVc4: return ebmEnabled ? cAtPwPacketTypeCEPVc4FactionalEbm : cAtPwPacketTypeCEPVc4FactionalNoEbm;
        default:
            return cAtPwPacketTypeUnknown;
        }
    }

static eAtPwPacketType CepPwPacketTypeForPw(AtPw pw)
    {
    uint32 mode = AtPwCepModeGet((AtPwCep)pw);
    switch (mode)
        {
        case cAtPwCepModeBasic     : return cAtPwPacketTypeCEPBasic;
        case cAtPwCepModeFractional: return CepPwPacketFractionalMode(pw);
        case cAtPwCepModeUnknown   : return cAtPwPacketTypeUnknown;
        default:
            return cAtPwPacketTypeUnknown;
        }
    }

static eAtPwPacketType PwPacketTypeForPw(AtPw pw)
    {
    uint32 type = AtPwTypeGet(pw);
    switch (type)
        {
        case cAtPwTypeToh  : return cAtPwPacketTypeToh;
        case cAtPwTypeSAToP: return cAtPwPacketTypeSAToP;
        case cAtPwTypeCESoP: return cAtPwPacketTypeCESoP;
        case cAtPwTypeCEP  : return CepPwPacketTypeForPw(pw);

        case cAtPwTypeInvalid:
        case cAtPwTypeATM:
        case cAtPwTypeHdlc:
        case cAtPwTypePpp:
        case cAtPwTypeFr:
        default:
            return cAtPwPacketTypeUnknown;
        }
    }

static AtPacket MplsPacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction)
    {
    if (AtDeviceModuleGet(mThis(self)->device, cAtModulePw))
        {
        AtPacket mplsPacket = AtMplsPwPacketNew(dataBuffer, length, cacheMode, cAtPwPacketTypeUnknown, cAtPwPacketRtpModeRtp);
        AtPw pw;

        if (mplsPacket == NULL)
            return NULL;

        pw = FindPwFromLabelInPacket(self, cAtPwPsnTypeMpls, AtMplsPacketInnerLabel(mplsPacket), direction);
        AtMplsPwPacketTypeSet(mplsPacket, PwPacketTypeForPw(pw));
        AtMplsPwPacketRtpModeSet(mplsPacket, (pw && AtPwRtpIsEnabled(pw)) ? cAtPwPacketRtpModeRtp : cAtPwPacketRtpModeNoRtp);
        return mplsPacket;
        }

    return m_AtPacketFactoryMethods->MplsPacketCreate(self, dataBuffer, length, cacheMode, direction);
    }

static AtPacket MefPacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction)
    {
    if (AtDeviceModuleGet(mThis(self)->device, cAtModulePw))
        {
        AtPacket mefPacket = AtMefPwPacketNew(dataBuffer, length, cacheMode, cAtPwPacketTypeUnknown, cAtPwPacketRtpModeRtp);
        AtPw pw;

        if (mefPacket == NULL)
            return NULL;

        pw = FindPwFromLabelInPacket(self, cAtPwPsnTypeMef, AtMefPacketEcid(mefPacket), direction);
        AtMefPwPacketTypeSet(mefPacket, PwPacketTypeForPw(pw));
        AtMefPwPacketRtpModeSet(mefPacket, (pw && AtPwRtpIsEnabled(pw)) ? cAtPwPacketRtpModeRtp : cAtPwPacketRtpModeNoRtp);
        return mefPacket;
        }

    return m_AtPacketFactoryMethods->MefPacketCreate(self, dataBuffer, length, cacheMode, direction);
    }

static AtPacket MefMplsPacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction)
    {
    if (AtDeviceModuleGet(mThis(self)->device, cAtModulePw))
        {
        AtPw pw;
        AtPacket mefPacket = AtMefMplsPacketNew(dataBuffer, length, cacheMode, cAtPwPacketTypeUnknown, cAtPwPacketRtpModeRtp);
        if (mefPacket == NULL)
            return NULL;

        pw = FindPwFromLabelInPacket(self, cAtPwPsnTypeMpls, AtMefPacketEcid(mefPacket), direction);
        AtMefPwPacketTypeSet(mefPacket, PwPacketTypeForPw(pw));
        AtMefPwPacketRtpModeSet(mefPacket, (pw && AtPwRtpIsEnabled(pw)) ? cAtPwPacketRtpModeRtp : cAtPwPacketRtpModeNoRtp);
        return mefPacket;
        }

    return m_AtPacketFactoryMethods->MefMplsPacketCreate(self, dataBuffer, length, cacheMode, direction);
    }

static AtPacket UdpPwPacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction)
    {
    AtPacket udpPacket;
    AtPw pw;

    if (AtDeviceModuleGet(mThis(self)->device, cAtModulePw) == NULL)
        return NULL;

    udpPacket = AtUdpPwPacketNew(dataBuffer, length, cacheMode, cAtPwPacketTypeUnknown, cAtPwPacketRtpModeRtp);
    if (udpPacket == NULL)
        return NULL;

    pw = FindPwFromLabelInPacket(self, cAtPwPsnTypeUdp, AtUdpPacketUdpLookupPort(udpPacket), direction);
    if (pw == NULL)
        {
        AtObjectDelete((AtObject)udpPacket);
        return NULL;
        }

    AtUdpPwPacketTypeSet(udpPacket, PwPacketTypeForPw(pw));
    AtUdpPwPacketRtpModeSet(udpPacket, (pw && AtPwRtpIsEnabled(pw)) ? cAtPwPacketRtpModeRtp : cAtPwPacketRtpModeNoRtp);
    return udpPacket;
    }

static AtPacket UdpPacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction)
    {
    AtPacket udpPacket = UdpPwPacketCreate(self, dataBuffer, length, cacheMode, direction);
    if (udpPacket)
        return udpPacket;

    return m_AtPacketFactoryMethods->UdpPacketCreate(self, dataBuffer, length, cacheMode, direction);
    }

static void OverrideAtPacketFactory(AtPacketFactory self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPacketFactoryMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPacketFactoryOverride, m_AtPacketFactoryMethods, sizeof(m_AtPacketFactoryOverride));

        mMethodOverride(m_AtPacketFactoryOverride, MplsPacketCreate);
        mMethodOverride(m_AtPacketFactoryOverride, MefPacketCreate);
        mMethodOverride(m_AtPacketFactoryOverride, MefMplsPacketCreate);
        mMethodOverride(m_AtPacketFactoryOverride, UdpPacketCreate);
        }

    mMethodsSet(self, &m_AtPacketFactoryOverride);
    }

static void Override(AtPacketFactory self)
    {
    OverrideAtPacketFactory(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPacketFactory);
    }

AtPacketFactory ThaPacketFactoryObjectInit(AtPacketFactory self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPacketFactoryObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Save private data */
    mThis(self)->device = device;

    return self;
    }

AtPacketFactory ThaPacketFactoryNew(AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtPacketFactory newFactory = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newFactory == NULL)
        return NULL;

    return ThaPacketFactoryObjectInit(newFactory, device);
    }

AtDevice ThaPacketFactoryDeviceGet(AtPacketFactory self)
    {
    return self ? mThis(self)->device : NULL;
    }

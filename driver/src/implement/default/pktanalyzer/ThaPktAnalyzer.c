/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : ThaPktAnalyzer.c
 *
 * Created Date: Dec 18, 2012
 *
 * Description : Thalassa default packet analyzer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../util/coder/AtCoderUtil.h"
#include "AtPacket.h"
#include "ThaPacketFactory.h"
#include "ThaPktAnalyzerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaPktAnalyzer)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaPktAnalyzerMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;
static tAtPktAnalyzerMethods m_AtPktAnalyzerOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;
static const tAtPktAnalyzerMethods *m_AtPktAnalyzerMethods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 PartOffset(ThaPktAnalyzer self)
    {
	AtUnused(self);
    return 0;
    }

static void ChannelIdSet(ThaPktAnalyzer self)
    {
    uint32 regAddr = cThaDiagPktDumpModeConId + ThaPktAnalyzerBaseAddress(self);
    uint32 regVal  = ThaPktAnalyzerRead(self, regAddr);

    mFieldIns(&regVal, cThaDebugDumChannelIdMask, cThaDebugDumChannelIdShift, AtChannelHwIdGet(AtPktAnalyzerChannelGet((AtPktAnalyzer)self)));
    ThaPktAnalyzerWrite(self, regAddr, regVal);
    }

static uint32 DumpModeSw2Hw(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode)
    {
    uint32 mode = pktDumpMode;
    switch (mode)
        {
        case cThaPktAnalyzerDumpGbeTx      : return mMethodsGet(self)->EthPortHwDumpMode(self, cThaPktAnalyzerDumpGbeTx);
        case cThaPktAnalyzerDumpGbeRx      : return mMethodsGet(self)->EthPortHwDumpMode(self, cThaPktAnalyzerDumpGbeRx);
        case cThaPktAnalyzerDumpDecToMpig  : return mMethodsGet(self)->PppLinkHwDumpMode(self, cThaPktAnalyzerDumpDecToMpig);
        case cThaPktAnalyzerDumpMpegToEnc  : return mMethodsGet(self)->PppLinkHwDumpMode(self, cThaPktAnalyzerDumpMpegToEnc);
        case cThaPktAnalyzerDumpMpigToGbe  : return 0x14;
        case cThaPktAnalyzerDumpMpigOut    : return 0x55;
        case cThaPktAnalyzerDumpGbeToCla   : return 0x12;
        case cThaPktAnalyzerDumpClaToMpeg  : return 0x13;
        default                            : return 0;
        }
    }

static uint32 DumpModeHw2Sw(ThaPktAnalyzer self, uint32 pktDumpMode)
    {
    eThaPktAnalyzerPktDumpMode swDumpMode;

    if (pktDumpMode == 0x14) return cThaPktAnalyzerDumpMpigToGbe;
    if (pktDumpMode == 0x55) return cThaPktAnalyzerDumpMpigOut;
    if (pktDumpMode == 0x12) return cThaPktAnalyzerDumpGbeToCla;
    if (pktDumpMode == 0x13) return cThaPktAnalyzerDumpClaToMpeg;

    /* Other modes */
    swDumpMode = mMethodsGet(self)->EthPortDumpModeHw2Sw(self, pktDumpMode);
    if (swDumpMode != cThaPktAnalyzerDumpUnknown)
        return swDumpMode;

    return mMethodsGet(self)->PppLinkDumpModeHw2Sw(self, pktDumpMode);
    }

static void DumpModeSet(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode)
    {
    uint32 regAddr = cThaDiagPktDumpModeCtrl + ThaPktAnalyzerBaseAddress(self);
    uint32 regVal = ThaPktAnalyzerRead(mThis(self), regAddr);

    mFieldIns(&regVal, cThaDebugDumPktSourceMask, cThaDebugDumPktSourceShift, DumpModeSw2Hw(self, pktDumpMode));
    mFieldIns(&regVal, cThaDebugDumPktCtlTypeMask, cThaDebugDumPktCtlTypeShift, 0);
    mFieldIns(&regVal, cThaDebugDumPktCtlRoMask, cThaDebugDumPktCtlRoShift, 1);
    ThaPktAnalyzerWrite(mThis(self), regAddr, regVal);
    }

static uint32 DumpModeGet(ThaPktAnalyzer self)
    {
    uint32 regAddr = cThaDiagPktDumpModeCtrl + ThaPktAnalyzerBaseAddress(self);
    uint32 regVal = ThaPktAnalyzerRead(self, regAddr);
    uint32  pktDumpMode;

    mFieldGet(regVal, cThaDebugDumPktSourceMask, cThaDebugDumPktSourceShift, uint32, &pktDumpMode);
    return DumpModeHw2Sw(self, pktDumpMode);
    }

static void AllTxPacketsRead(ThaPktAnalyzer self)
	{
	AtUnused(self);
	}

static void AllRxPacketsRead(ThaPktAnalyzer self)
	{
	AtUnused(self);
	}

static void DeleteAllPacketReader(ThaPktAnalyzer self)
    {
    /* Delete packet reader if existed */
    AtObjectDelete((AtObject)(self->txPktReader));
    self->txPktReader = NULL;
    AtObjectDelete((AtObject)(self->rxPktReader));
    self->rxPktReader = NULL;
    }

void ThaPktAnalyzerCreateAllPacketReader(ThaPktAnalyzer self)
    {
    if (self == NULL)
        return;

    /* Delete packet reader if existed*/
	DeleteAllPacketReader(self);

    self->txPktReader = mMethodsGet(self)->TxPacketReaderCreate(self);
    self->rxPktReader = mMethodsGet(self)->RxPacketReaderCreate(self);
    }

static void Init(AtPktAnalyzer self)
    {
    m_AtPktAnalyzerMethods->Init(self);
    mMethodsGet((ThaPktAnalyzer)self)->ChannelIdSet((ThaPktAnalyzer)self);
    ThaPktAnalyzerCreateAllPacketReader((ThaPktAnalyzer)self);
    }

static void Recapture(AtPktAnalyzer self)
    {
	uint32 regVal;
	uint32 regAddr = cThaDiagPktDumpModeCtrl + ThaPktAnalyzerBaseAddress(mThis(self));
    regVal = ThaPktAnalyzerRead(mThis(self), regAddr);
	mFieldIns(&regVal, cThaDebugDumPktCtlRoMask, cThaDebugDumPktCtlRoShift, 0);
	ThaPktAnalyzerWrite(mThis(self), regAddr,  regVal);
    }

static void PatternSet(AtPktAnalyzer self, uint8 *pattern)
    {
	AtUnused(pattern);
	AtUnused(self);
    /* Not used now*/
    }

static void PatternMaskSet(AtPktAnalyzer self, uint32 patternMask)
    {
	AtUnused(patternMask);
	AtUnused(self);
    /* Not used now*/
    }

static uint8 PacketLengthModeSw2Hw(uint32 packetLength)
	{
    /* Full */
    if (packetLength == 0) return 0;

    /* Packet with length */
    if (packetLength <= 32)   return 1;
    if (packetLength <= 64)   return 2;
    if (packetLength <= 128)  return 3;
    if (packetLength <= 256)  return 4;
    if (packetLength <= 1024) return 5;

    /* Full */
    return 0;
	}

static uint8 PppPacketModeSw2Hw(eAtPktAnalyzerPppPktType packetMode)
	{
    if (packetMode == cAtPktAnalyzerPppPktTypeMlpppProtocol) return 0;
    if (packetMode == cAtPktAnalyzerPppPktTypePppProtocol)   return 1;
    if (packetMode == cAtPktAnalyzerPppPktTypeHdlc)          return 2;
    if (packetMode == cAtPktAnalyzerPppPktTypeOamPacket)     return 3;
    if (packetMode == cAtPktAnalyzerPppPktTypeMlpppPacket)   return 4;
    if (packetMode == cAtPktAnalyzerPppPktTypePppPacket)     return 5;

    return 0;
	}

static uint8 PatternCompareModeSw2Hw(eAtPktAnalyzerPatternCompareMode compareMode)
	{
    if (compareMode == cAtPktAnalyzerPatternCompareModeAny)       return 0;
    if (compareMode == cAtPktAnalyzerPatternCompareModeSamePkt)   return 1;
    if (compareMode == cAtPktAnalyzerPatternCompareModeDifferent) return 2;

    return 0;
	}

static void PacketLengthSet(AtPktAnalyzer self, uint32 packetLength)
    {
	uint32 regAddr = cThaDiagPktDumpModeCtrl + ThaPktAnalyzerBaseAddress(mThis(self));
    uint32 regVal = ThaPktAnalyzerRead(mThis(self), regAddr);

	mFieldIns(&regVal, cThaDebugDumPktCtlLenMask, cThaDebugDumPktCtlLenShift, PacketLengthModeSw2Hw(packetLength));
	ThaPktAnalyzerWrite(mThis(self), regAddr,  regVal);
    }

static void PppPacketModeSet(AtPktAnalyzer self, eAtPktAnalyzerPppPktType packetMode)
    {
	uint32 regAddr = cThaDiagPktDumpModeConId + ThaPktAnalyzerBaseAddress(mThis(self));
    uint32 regVal = ThaPktAnalyzerRead(mThis(self), regAddr);

	mFieldIns(&regVal, cThaDebugDumPktTypeMask, cThaDebugDumPktTypeShift, PppPacketModeSw2Hw(packetMode));
	ThaPktAnalyzerWrite(mThis(self), regAddr,  regVal);
    }

static void PatternCompareModeSet(AtPktAnalyzer self, eAtPktAnalyzerPatternCompareMode compareMode)
    {
	uint32 regAddr = cThaDiagPktDumpModeCtrl + ThaPktAnalyzerBaseAddress(mThis(self));
    uint32 regVal = ThaPktAnalyzerRead(mThis(self), regAddr);

	mFieldIns(&regVal, cThaDebugDumPktCtlPat0Mask, cThaDebugDumPktCtlPat0Shift, PatternCompareModeSw2Hw(compareMode));
	ThaPktAnalyzerWrite(mThis(self), regAddr,  regVal);
    }

static void PacketDumpTypeSet(AtPktAnalyzer self, eAtPktAnalyzerPktDumpType packetDumpType)
    {
    uint32 regAddr = cThaDiagPktDumpModeCtrl + ThaPktAnalyzerBaseAddress(mThis(self));
    uint32 regVal = ThaPktAnalyzerRead(mThis(self), regAddr);

    mFieldIns(&regVal, cThaDebugDumPktCtlSelMask, cThaDebugDumPktCtlSelShift, packetDumpType);
    ThaPktAnalyzerWrite(mThis(self), regAddr, regVal);
    }

static uint32 NumPacketInBuffer(ThaPktAnalyzer self)
    {
    uint32 packetNum;
    uint32 regAddr = cThaDiagPktDumpModePktCount + ThaPktAnalyzerBaseAddress(self);
    uint32 regVal = ThaPktAnalyzerRead(self, regAddr);

    mFieldGet(regVal, cThaDebugDumPktCntMask, cThaDebugDumPktCntShift, uint16, &packetNum);

    return packetNum;
    }

static uint32 PacketLenghthGet(ThaPktAnalyzer self, uint32 packetId, uint16 *startAddress)
    {
    uint32 regVal = ThaPktAnalyzerRead(self, cThaDiagPktDumpModePktInfo(packetId) + ThaPktAnalyzerBaseAddress(self));
    uint32 packetLength;

    mFieldGet(regVal, cThaDebugDumPktInfoStartAddrMask, cThaDebugDumPktInfoStartAddrShift, uint16, startAddress);
    mFieldGet(regVal, cThaDebugDumPktInfoLenMask, cThaDebugDumPktInfoLenShift, uint16, &packetLength);
    return packetLength;
    }

static uint32 PacketDumpDataRegister(ThaPktAnalyzer self, uint32 address_i)
    {
    AtUnused(self);
    return cThaDiagPktDumpModePktDataReg(address_i);
    }

static AtPacket PacketCreate(AtPktAnalyzer self, uint8 *data, uint32 length, eAtPktAnalyzerDirection direction)
    {
    return AtPacketFactoryRawPacketCreate(AtPktAnalyzerPacketFactory(self), data, length, cAtPacketCacheModeCacheData, direction);
    }

static AtPacketFactory PacketFactoryCreate(AtPktAnalyzer self)
    {
    return ThaPacketFactoryNew(AtChannelDeviceGet(AtPktAnalyzerChannelGet(self)));
    }

static eBool SkipLastInCompletePacket(ThaPktAnalyzer self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static uint32 StartAddress(ThaPktAnalyzer self)
    {
	AtUnused(self);
    return 0x40F000;
    }

static uint32 EthPortHwDumpMode(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode)
    {
	AtUnused(self);
    if (pktDumpMode == cThaPktAnalyzerDumpGbeTx) return 0x0;
    if (pktDumpMode == cThaPktAnalyzerDumpGbeRx) return 0x1;

    /* Invalid value */
    return 0x8;
    }

static uint32 PppLinkHwDumpMode(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode)
    {
	AtUnused(self);
    if (pktDumpMode == cThaPktAnalyzerDumpMpegToEnc) return 0x10;
    if (pktDumpMode == cThaPktAnalyzerDumpDecToMpig) return 0x15;

    /* Invalid value */
    return 0x8;
    }

static uint32 PppLinkDumpModeHw2Sw(ThaPktAnalyzer self, uint32 hwPktDumpMode)
    {
	AtUnused(self);
    if (hwPktDumpMode == 0x10) return cThaPktAnalyzerDumpMpegToEnc;
    if (hwPktDumpMode == 0x15) return cThaPktAnalyzerDumpDecToMpig;

    return cThaPktAnalyzerDumpUnknown;
    }

static uint32 EthPortDumpModeHw2Sw(ThaPktAnalyzer self, uint32 hwPktDumpMode)
    {
	AtUnused(self);
    if (hwPktDumpMode == 0x0)  return cThaPktAnalyzerDumpGbeTx;
    if (hwPktDumpMode == 0x1)  return cThaPktAnalyzerDumpGbeRx;

    return cThaPktAnalyzerDumpUnknown;
    }

static eBool ShouldRecapture(ThaPktAnalyzer self, uint8 newPktDumpMode, uint8 currentDumpMode)
    {
	AtUnused(self);
    if ((currentDumpMode != newPktDumpMode) && (currentDumpMode != cThaPktAnalyzerDumpUnknown))
        return cAtTrue;

    return cAtFalse;
    }

static eBool JustDisplayRawPacket(ThaPktAnalyzer self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 FirstAddressOfBuffer(ThaPktAnalyzer self)
	{
	AtUnused(self);
	return cBit31_0;
	}

static uint32 LastAddressOfBuffer(ThaPktAnalyzer self)
	{
	AtUnused(self);
	return cBit31_0;
	}

static eBool IsStartOfPacket(ThaPktAnalyzer self, uint32 address, uint32 *data)
	{
	AtUnused(self);
	AtUnused(address);
	AtUnused(data);
	return cAtFalse;
	}

static eBool IsEndOfPacket(ThaPktAnalyzer self, uint32 address, uint32 *data)
	{
	AtUnused(self);
	AtUnused(address);
	AtUnused(data);
	return cAtFalse;
	}

static uint8 EntryRead(ThaPktAnalyzer self, uint32 address, uint32 *data)
	{
	AtUnused(self);
	AtUnused(address);
	AtUnused(data);
	return 0;
	}

static uint8 MetaDwordIndex(ThaPktAnalyzer self)
	{
	AtUnused(self);
	return 1;
	}

static eBool DumpModeIsSupported(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode)
    {
	AtUnused(self);
	AtUnused(pktDumpMode);
    return cAtFalse;
    }

static eAtPktAnalyzerDisplayMode DisplayMode(ThaPktAnalyzer self)
    {
    if (mMethodsGet(self)->JustDisplayRawPacket(self))
        return cAtPktAnalyzerDisplayModeRaw;
    return cAtPktAnalyzerDisplayModeHumanReadable;
    }

static void AnalyzeTxPackets(AtPktAnalyzer self)
    {
	ThaPktAnalyzer analyzer = (ThaPktAnalyzer)self;
	mMethodsGet(analyzer)->AllTxPacketsRead(analyzer);
	AtPktAnalyzerAllPacketsDisplay(AtPktAnalyzerPacketListGet(self), DisplayMode(analyzer));
    }

static void AnalyzeRxPackets(AtPktAnalyzer self)
    {
	ThaPktAnalyzer analyzer = (ThaPktAnalyzer)self;
	mMethodsGet(analyzer)->AllRxPacketsRead(analyzer);
	AtPktAnalyzerAllPacketsDisplay(AtPktAnalyzerPacketListGet(self), DisplayMode(analyzer));
    }

static ThaPacketReader TxPacketReaderCreate(ThaPktAnalyzer self)
	{
    return ThaPacketReaderDefaultNew((AtPktAnalyzer)self);
	}

static ThaPacketReader RxPacketReaderCreate(ThaPktAnalyzer self)
	{
    return ThaPacketReaderDefaultNew((AtPktAnalyzer)self);
	}

static void Delete(AtObject self)
    {
    DeleteAllPacketReader((ThaPktAnalyzer)self);
    m_AtObjectMethods->Delete(self);
    }

static uint16 LongRead(ThaPktAnalyzer self, uint32 address, uint32 *data)
    {
    AtChannel port = ((AtPktAnalyzer)self)->channel;
    return mChannelHwLongRead(port, address, data, cThaLongRegMaxSize, cAtModuleEth);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaPktAnalyzer object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObject(txPktReader);
    mEncodeObject(rxPktReader);
    }

static uint16 EntryInfoRead(ThaPktAnalyzer self, uint32 address, uint32 *data)
    {
    /* Do nothing so that current products will not be affected */
    AtUnused(self);
    AtUnused(address);
    AtUnused(data);
    return 0;
    }

static eBool IsErrorPacket(ThaPktAnalyzer self, uint32 address, uint32 *data)
    {
    AtUnused(self);
    AtUnused(address);
    AtUnused(data);
    return cAtFalse;
    }

static eBool AutoFlushPackets(ThaPktAnalyzer self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideAtObject(ThaPktAnalyzer self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtPktAnalyzer(ThaPktAnalyzer self)
    {
    AtPktAnalyzer pktAnalyzer = (AtPktAnalyzer)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPktAnalyzerMethods = mMethodsGet(pktAnalyzer);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPktAnalyzerOverride, m_AtPktAnalyzerMethods, sizeof(tAtPktAnalyzerMethods));
        mMethodOverride(m_AtPktAnalyzerOverride, Init);
        mMethodOverride(m_AtPktAnalyzerOverride, Recapture);
        mMethodOverride(m_AtPktAnalyzerOverride, PatternSet);
        mMethodOverride(m_AtPktAnalyzerOverride, PatternMaskSet);
        mMethodOverride(m_AtPktAnalyzerOverride, PacketLengthSet);
        mMethodOverride(m_AtPktAnalyzerOverride, PppPacketModeSet);
        mMethodOverride(m_AtPktAnalyzerOverride, PacketDumpTypeSet);
        mMethodOverride(m_AtPktAnalyzerOverride, PatternCompareModeSet);
        mMethodOverride(m_AtPktAnalyzerOverride, PacketCreate);
        mMethodOverride(m_AtPktAnalyzerOverride, PacketFactoryCreate);
        mMethodOverride(m_AtPktAnalyzerOverride, AnalyzeTxPackets);
        mMethodOverride(m_AtPktAnalyzerOverride, AnalyzeRxPackets);
        }

    mMethodsSet(pktAnalyzer, &m_AtPktAnalyzerOverride);
    }

static void Override(ThaPktAnalyzer self)
    {
    OverrideAtObject(self);
    OverrideAtPktAnalyzer(self);
    }

static void MethodsInit(ThaPktAnalyzer self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, ChannelIdSet);
        mMethodOverride(m_methods, PartOffset);
        mMethodOverride(m_methods, StartAddress);
        mMethodOverride(m_methods, SkipLastInCompletePacket);
        mMethodOverride(m_methods, EthPortHwDumpMode);
        mMethodOverride(m_methods, PppLinkHwDumpMode);
        mMethodOverride(m_methods, PppLinkDumpModeHw2Sw);
        mMethodOverride(m_methods, EthPortDumpModeHw2Sw);
        mMethodOverride(m_methods, ShouldRecapture);
        mMethodOverride(m_methods, JustDisplayRawPacket);
        mMethodOverride(m_methods, NumPacketInBuffer);
        mMethodOverride(m_methods, PacketLenghthGet);
        mMethodOverride(m_methods, PacketDumpDataRegister);
        mMethodOverride(m_methods, DumpModeSet);
        mMethodOverride(m_methods, DumpModeGet);
        mMethodOverride(m_methods, AllTxPacketsRead);
        mMethodOverride(m_methods, AllRxPacketsRead);
        mMethodOverride(m_methods, TxPacketReaderCreate);
        mMethodOverride(m_methods, RxPacketReaderCreate);
        mMethodOverride(m_methods, FirstAddressOfBuffer);
        mMethodOverride(m_methods, LastAddressOfBuffer);
        mMethodOverride(m_methods, IsStartOfPacket);
        mMethodOverride(m_methods, IsEndOfPacket);
        mMethodOverride(m_methods, EntryRead);
        mMethodOverride(m_methods, MetaDwordIndex);
        mMethodOverride(m_methods, DumpModeIsSupported);
        mMethodOverride(m_methods, MetaDwordIndex);
        mMethodOverride(m_methods, LongRead);
        mMethodOverride(m_methods, EntryInfoRead);
        mMethodOverride(m_methods, IsErrorPacket);
        mMethodOverride(m_methods, AutoFlushPackets);
        }

    mMethodsSet(self, &m_methods);
    }

AtPktAnalyzer ThaPktAnalyzerObjectInit(AtPktAnalyzer self, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtPktAnalyzer));

    /* Super constructor */
    if (AtPktAnalyzerObjectInit(self, channel) == NULL)
        return NULL;

    /* Setup class */
    Override((ThaPktAnalyzer)self);
    MethodsInit((ThaPktAnalyzer)self);
    m_methodsInit = 1;

    return self;
    }

uint32 ThaPktAnalyzerRead(ThaPktAnalyzer self, uint32 address)
    {
    return mChannelHwRead(AtPktAnalyzerChannelGet((AtPktAnalyzer)self), address, cAtModuleEth);
    }

void ThaPktAnalyzerWrite(ThaPktAnalyzer self, uint32 address, uint32 value)
    {
    mChannelHwWrite(AtPktAnalyzerChannelGet((AtPktAnalyzer)self), address, value, cAtModuleEth);
    }

eAtPktAnalyzerDirection ThaPktAnalyzerDirectionFromDumpMode(eThaPktAnalyzerPktDumpMode pktDumpMode)
    {
    if (pktDumpMode == cThaPktAnalyzerDumpGbeTx)
        return cAtPktAnalyzerDirectionTx;
    if (pktDumpMode == cThaPktAnalyzerDumpGbeRx)
        return cAtPktAnalyzerDirectionRx;
    return cAtPktAnalyzerDirectionNone;
    }

uint32 ThaPktAnalyzerBaseAddress(ThaPktAnalyzer self)
    {
    return mMethodsGet(self)->PartOffset(self) + mMethodsGet(self)->StartAddress(self);
    }

void ThaPktAnalyzerDumpModeSet(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode)
    {
    if (self)
        mMethodsGet(self)->DumpModeSet(self, pktDumpMode);
    return;
    }

uint32 ThaPktAnalyzerDumpModeGet(ThaPktAnalyzer self)
    {
    if (self)
        return mMethodsGet(self)->DumpModeGet(self);
    return cInvalidUint32;
    }

ThaPacketReader ThaPktAnalyzerTxPacketReaderGet(ThaPktAnalyzer self)
	{
	if (self)
		return self->txPktReader;
		
	return NULL;
	}

ThaPacketReader ThaPktAnalyzerRxPacketReaderGet(ThaPktAnalyzer self)
	{
	if (self)
		return self->rxPktReader;
		
	return NULL;
	}

void ThaPktAnalyzerPktDumpModeSet(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode)
	{
	DumpModeSet(self, pktDumpMode);
	}

uint16 ThaPktAnalyzerLongRead(ThaPktAnalyzer self, uint32 address, uint32 *data)
    {
    if (self)
        return mMethodsGet(self)->LongRead(self, address, data);
    return 0;
    }

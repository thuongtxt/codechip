/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : ThaEthPortPktAnalyzerV2.c
 *
 * Created Date: Jun 19, 2013
 *
 * Description : Ethernet packet analyzer - to work with FPGA version 2
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPacket.h"
#include "../eth/ThaModuleEth.h"
#include "ThaPacket.h"
#include "ThaEthPortPktAnalyzerInternal.h"
#include "ThaPacketReaderInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cDumpControlReg  0x0
#define cBufferStartReg  0x1000
#define cBufferStopReg   0x10FE

/* Packet meta data */
#define cNumBytesMask       cBit1_0
#define cNumBytesShift      0
#define cEndOfPacketMask    cBit2
#define cStartOfPacketMask  cBit3

#define cStateFindStartOfPacket 0
#define cStateReading           1
#define cStateEndOfPacket       2

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaEthPortPktAnalyzerV2)self)
/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPktAnalyzerMethods m_ThaPktAnalyzerOverride;
static tAtPktAnalyzerMethods  m_AtPktAnalyzerOverride;

/* Save super implementation */
static tAtPktAnalyzerMethods  *m_AtPktAnalyzerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 FirstAddressOfBuffer(ThaPktAnalyzer self)
	{
	return (ThaPktAnalyzerBaseAddress(self) + cBufferStartReg);
	}

static uint32 LastAddressOfBuffer(ThaPktAnalyzer self)
	{
	return (ThaPktAnalyzerBaseAddress(self) + cBufferStopReg);
	}

static eBool IsStartOfPacket(ThaPktAnalyzer self, uint32 address, uint32 *data)
	{
	uint32 longRegVal[cThaLongRegMaxSize];
	AtChannel port = ((AtPktAnalyzer)self)->channel;
	AtUnused(data);
	mChannelHwLongRead(port, address, longRegVal, cThaLongRegMaxSize, cAtModuleEth);
	return (longRegVal[mMetaDwordIndex(self)] & cStartOfPacketMask) ? cAtTrue : cAtFalse;
	}

static eBool IsEndOfPacket(ThaPktAnalyzer self, uint32 address, uint32 *data)
	{
	uint32 longRegVal[cThaLongRegMaxSize];
	AtChannel port = ((AtPktAnalyzer)self)->channel;
	AtUnused(data);
	mChannelHwLongRead(port, address, longRegVal, cThaLongRegMaxSize, cAtModuleEth);
	return (longRegVal[mMetaDwordIndex(self)] & cEndOfPacketMask) ? cAtTrue : cAtFalse;
	}

static uint8 EntryRead(ThaPktAnalyzer self, uint32 address, uint32 *data)
	{
	uint32 longRegVal[cThaLongRegMaxSize];
	uint8 numBytes;

	ThaPktAnalyzerLongRead(self, address, longRegVal);
	numBytes = (uint8)(mRegField(longRegVal[mMetaDwordIndex(self)], cNumBytes) + 1);
	*data = longRegVal[0];

	return numBytes;
	}

static void Init(AtPktAnalyzer self)
    {
	ThaPktAnalyzerCreateAllPacketReader((ThaPktAnalyzer)self);
    }

static void Recapture(AtPktAnalyzer self)
    {
	AtUnused(self);
    }

static void PatternSet(AtPktAnalyzer self, uint8 *pattern)
    {
	AtUnused(pattern);
	AtUnused(self);
    }

static void PatternMaskSet(AtPktAnalyzer self, uint32 patternMask)
    {
	AtUnused(patternMask);
	AtUnused(self);
    }

static void PacketNumSet(AtPktAnalyzer self, uint32 packetNums)
    {
	AtUnused(packetNums);
	AtUnused(self);
    }

static void PacketLengthSet(AtPktAnalyzer self, uint32 packetLength)
    {
	AtUnused(packetLength);
	AtUnused(self);
    }

static void PacketDumpTypeSet(AtPktAnalyzer self, eAtPktAnalyzerPktDumpType dumpType)
    {
	AtUnused(dumpType);
	AtUnused(self);
    }

static void PppPacketModeSet(AtPktAnalyzer self, eAtPktAnalyzerPppPktType packetMode)
    {
	AtUnused(packetMode);
	AtUnused(self);
    }

static void PatternCompareModeSet(AtPktAnalyzer self, eAtPktAnalyzerPatternCompareMode compareMode)
    {
	AtUnused(compareMode);
	AtUnused(self);
    }

static AtPacket PacketCreate(AtPktAnalyzer self, uint8 *data, uint32 length, eAtPktAnalyzerDirection direction)
    {
	AtUnused(direction);
	AtUnused(self);
    return ThaEthPacketV2New(data, length, cAtPacketCacheModeCacheData);
    }

static uint32 StartAddress(ThaPktAnalyzer self)
    {
	AtUnused(self);
    return 0x180000;
    }

static uint32 PartOffset(ThaPktAnalyzer self)
    {
    AtEthPort port = (AtEthPort)AtPktAnalyzerChannelGet((AtPktAnalyzer)self);
    AtDevice device = AtChannelDeviceGet((AtChannel)port);
    ThaModuleEth ethModule = (ThaModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    return ThaModuleEthPartOffset(ethModule, ThaModuleEthPartOfPort(ethModule, port));
    }

static void DumpModeSet(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode)
    {
    static const uint32 cDumpTx          = 0x1000;
    static const uint32 cDumpRx          = 0;
    uint32 baseAddress = ThaPktAnalyzerBaseAddress(self);

    if (pktDumpMode == cThaPktAnalyzerDumpGbeTx)
        ThaPktAnalyzerWrite(self, cDumpControlReg + baseAddress, cDumpTx);
    if (pktDumpMode == cThaPktAnalyzerDumpGbeRx)
        ThaPktAnalyzerWrite(self, cDumpControlReg + baseAddress, cDumpRx);
    }

static eBool DumpModeIsSupported(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode)
    {
    AtUnused(self);

    if ((pktDumpMode == cThaPktAnalyzerDumpGbeTx) ||
        (pktDumpMode == cThaPktAnalyzerDumpGbeRx))
        return cAtTrue;

    return cAtFalse;
    }

static ThaPacketReader TxPacketReaderCreate(ThaPktAnalyzer self)
	{
	return ThaPacketReaderV2New((AtPktAnalyzer)self);
	}

static ThaPacketReader RxPacketReaderCreate(ThaPktAnalyzer self)
	{
	return ThaPacketReaderV2New((AtPktAnalyzer)self);
	}

static void OverrideThaPktAnalyzer(AtPktAnalyzer self)
    {
    ThaPktAnalyzer analyzer = (ThaPktAnalyzer)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPktAnalyzerOverride, mMethodsGet(analyzer), sizeof(m_ThaPktAnalyzerOverride));

        mMethodOverride(m_ThaPktAnalyzerOverride, StartAddress);
        mMethodOverride(m_ThaPktAnalyzerOverride, PartOffset);
        mMethodOverride(m_ThaPktAnalyzerOverride, DumpModeSet);
        mMethodOverride(m_ThaPktAnalyzerOverride, DumpModeIsSupported);
        mMethodOverride(m_ThaPktAnalyzerOverride, FirstAddressOfBuffer);
        mMethodOverride(m_ThaPktAnalyzerOverride, LastAddressOfBuffer);
        mMethodOverride(m_ThaPktAnalyzerOverride, IsStartOfPacket);
        mMethodOverride(m_ThaPktAnalyzerOverride, IsEndOfPacket);
        mMethodOverride(m_ThaPktAnalyzerOverride, EntryRead);
        mMethodOverride(m_ThaPktAnalyzerOverride, TxPacketReaderCreate);
		mMethodOverride(m_ThaPktAnalyzerOverride, RxPacketReaderCreate);
        }

    mMethodsSet(analyzer, &m_ThaPktAnalyzerOverride);
    }

static void OverrideAtPktAnalyzer(AtPktAnalyzer self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPktAnalyzerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPktAnalyzerOverride, m_AtPktAnalyzerMethods, sizeof(tAtPktAnalyzerMethods));
        mMethodOverride(m_AtPktAnalyzerOverride, Init);
        mMethodOverride(m_AtPktAnalyzerOverride, Recapture);
        mMethodOverride(m_AtPktAnalyzerOverride, PatternSet);
        mMethodOverride(m_AtPktAnalyzerOverride, PatternMaskSet);
        mMethodOverride(m_AtPktAnalyzerOverride, PacketLengthSet);
        mMethodOverride(m_AtPktAnalyzerOverride, PppPacketModeSet);
        mMethodOverride(m_AtPktAnalyzerOverride, PacketDumpTypeSet);
        mMethodOverride(m_AtPktAnalyzerOverride, PatternCompareModeSet);
        mMethodOverride(m_AtPktAnalyzerOverride, PacketNumSet);
        mMethodOverride(m_AtPktAnalyzerOverride, PacketCreate);
        }

    mMethodsSet(self, &m_AtPktAnalyzerOverride);
    }

static void Override(AtPktAnalyzer self)
    {
    OverrideThaPktAnalyzer(self);
    OverrideAtPktAnalyzer(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaEthPortPktAnalyzerV2);
    }

AtPktAnalyzer ThaEthPortPktAnalyzerV2ObjectInit(AtPktAnalyzer self, AtEthPort port)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaEthPortPktAnalyzerObjectInit(self, port) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPktAnalyzer ThaEthPortPktAnalyzerV2New(AtEthPort port)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtPktAnalyzer newPktAnalyzer = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPktAnalyzer == NULL)
        return NULL;

    /* Construct it */
    return ThaEthPortPktAnalyzerV2ObjectInit(newPktAnalyzer, port);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : ThaEthPacketV2Internal.h
 * 
 * Created Date: Oct 14, 2013
 *
 * Description : ETH packet of PW product version 2
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAETHPACKETV2INTERNAL_H_
#define _THAETHPACKETV2INTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/pktanalyzer/AtEthPacketInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaEthPacketV2
    {
    tAtEthPacket super;
    }tThaEthPacketV2;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THAETHPACKETV2INTERNAL_H_ */


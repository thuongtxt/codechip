/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : ThaPacketFactory.h
 * 
 * Created Date: Mar 29, 2014
 *
 * Description : Packet factory
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPACKETFACTORY_H_
#define _THAPACKETFACTORY_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtDevice.h"
#include "AtPacketFactory.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPacketFactory ThaPacketFactoryNew(AtDevice device);

AtDevice ThaPacketFactoryDeviceGet(AtPacketFactory self);

/* Product concretes */
AtPacketFactory Tha60031031PacketFactoryNew(AtDevice device);
AtPacketFactory Tha60210012PacketFactoryNew(AtDevice device);
AtPacketFactory ThaStmPwProductPacketFactoryNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAPACKETFACTORY_H_ */


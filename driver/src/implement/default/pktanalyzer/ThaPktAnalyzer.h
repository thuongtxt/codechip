/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : ThaPktAnalyzer.h
 * 
 * Created Date: Dec 21, 2012
 *
 * Description : Thalassa default packet analyzer
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPKTANALYZER_H_
#define _THAPKTANALYZER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPktAnalyzer.h" /* Super class */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPktAnalyzer * ThaPktAnalyzer;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Default concretes */
AtPktAnalyzer ThaEthFlowPktAnalyzerNew(AtEthFlow port);
AtPktAnalyzer ThaEthPortPktAnalyzerNew(AtEthPort port);
AtPktAnalyzer ThaEthPortPktAnalyzerV2New(AtEthPort port);
AtPktAnalyzer ThaPppLinkPktAnalyzerNew(AtPppLink link);

uint32 ThaPktAnalyzerBaseAddress(ThaPktAnalyzer self);
uint16 ThaPktAnalyzerLongRead(ThaPktAnalyzer self, uint32 address, uint32 *data);

/* Product concretes */
AtPktAnalyzer Tha60000031EthPortPktAnalyzerNew(AtEthPort port);
AtPktAnalyzer Tha60031031EthPortPktAnalyzerNew(AtEthPort port);
AtPktAnalyzer Tha60031032EthPortPktAnalyzerNew(AtEthPort port);
AtPktAnalyzer Tha60031131EthPortPktAnalyzerNew(AtEthPort port);
AtPktAnalyzer Tha60091023EthPortPktAnalyzerNew(AtEthPort port);
AtPktAnalyzer Tha60091132EthFlowPktAnalyzerNew(AtEthFlow flow);
AtPktAnalyzer Tha60091132EthPortPktAnalyzerNew(AtEthPort port);
AtPktAnalyzer Tha60091132PppLinkPktAnalyzerNew(AtPppLink link);
AtPktAnalyzer Tha60150011EthPortPktAnalyzerNew(AtEthPort port);
AtPktAnalyzer Tha60210011EthPortPktAnalyzerNew(AtEthPort port);
AtPktAnalyzer Tha60210012EthPortPktAnalyzerNew(AtEthPort port);
AtPktAnalyzer Tha60290011EthPortPktAnalyzerNew(AtEthPort port);
AtPktAnalyzer Tha60210012PppLinkPktAnalyzerNew(AtPppLink link);
AtPktAnalyzer ThaStmPwProductEthPortPktAnalyzerNew(AtEthPort port);
AtPktAnalyzer Tha60290021EthPortSgmiiPktAnalyzerNew(AtEthPort port);
AtPktAnalyzer Tha60290011EthPortSgmiiDccPktAnalyzerNew(AtEthPort port);
AtPktAnalyzer Tha60290011EthPortAxi4PktAnalyzerNew(AtEthPort port);
AtPktAnalyzer Tha60290011EthPortDccSgmiiPktAnalyzerV2New(AtEthPort port);
AtPktAnalyzer Tha60290011DccHdlcPktAnalyzerNew(AtEthPort port, AtHdlcChannel hdlcChannel);

#ifdef __cplusplus
}
#endif
#endif /* _THAPKTANALYZER_H_ */

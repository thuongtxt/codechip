/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : ThaEthFlowPktAnalyzerInternal.h
 * 
 * Created Date: Dec 21, 2012
 *
 * Description : Ethernet flow packet analyzer
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAETHFLOWPKTANALYZERINTERNAL_H_
#define _THAETHFLOWPKTANALYZERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaPktAnalyzerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaEthFlowPktAnalyzer * ThaEthFlowPktAnalyzer;

typedef struct tThaEthFlowPktAnalyzer
    {
    tThaPktAnalyzer super;

    /* Private */
    }tThaEthFlowPktAnalyzer;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPktAnalyzer ThaEthFlowPktAnalyzerObjectInit(AtPktAnalyzer self, AtEthFlow flow);

#ifdef __cplusplus
}
#endif
#endif /* _THAETHFLOWPKTANALYZERINTERNAL_H_ */


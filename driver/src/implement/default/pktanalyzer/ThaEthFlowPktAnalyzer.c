/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : ThaEthFlowPktAnalyzer.c
 *
 * Created Date: Dec 20, 2012
 *
 * Description : Ethernet flow packet analyzer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaEthFlowPktAnalyzerInternal.h"
#include "ThaPktAnalyzerReg.h"
#include "AtModuleEncap.h"
#include "AtModulePpp.h"
#include "../eth/ThaEthFlowInternal.h"
#include "ThaPacketReaderInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPktAnalyzerMethods  m_AtPktAnalyzerOverride;
static tThaPktAnalyzerMethods m_ThaPktAnalyzerOverride;

/* Save super implementation */
static const tAtPktAnalyzerMethods  *m_AtPktAnalyzerMethods;
static const tThaPktAnalyzerMethods *m_ThaPktAnalyzerMethods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtEthFlow EthFlowGet(ThaPktAnalyzer self)
    {
    return (AtEthFlow)AtPktAnalyzerChannelGet((AtPktAnalyzer)self);
    }

static void ChannelIdSet(ThaPktAnalyzer self)
    {
    uint32 regAddr = cThaDiagPktDumpModeConId + ThaPktAnalyzerBaseAddress(self);
    uint32 regVal = ThaPktAnalyzerRead(self, regAddr);
    uint32 channelId;
    AtEthFlow flow = EthFlowGet(self);
    AtHdlcLink link = AtEthFlowHdlcLinkGet(flow);
    AtMpBundle bundle = AtEthFlowMpBundleGet(flow);

    if (flow == NULL)
        return;

    if (!mMethodsGet(flow)->IsBusy(flow))
        {
        AtPrintc(cSevCritical, "Flow %u is not activated\r\n", AtChannelIdGet((AtChannel)flow) + 1);
        return;
        }

    if (link != NULL)
        channelId = AtChannelIdGet((AtChannel)link);
    else
        {
        /* Currently, no support dump packet of flow joined to bundle */
        AtPrintc(cSevCritical, "Flow %u is in bundle %u, not support dump packet\r\n", AtChannelIdGet((AtChannel)flow) + 1, AtChannelIdGet((AtChannel)bundle) + 1);
        return;
        }

    mFieldIns(&regVal, cThaDebugDumChannelIdMask, cThaDebugDumChannelIdShift, channelId);
    ThaPktAnalyzerWrite(self, regAddr, regVal);
    }

static void Init(AtPktAnalyzer self)
    {
    /* Super */
    m_AtPktAnalyzerMethods->Init(self);
    ThaPktAnalyzerDumpModeSet((ThaPktAnalyzer)self, cThaPktAnalyzerDumpMpigToGbe);
    mMethodsGet(self)->Recapture(self);
    }

static void AllTxPacketsRead(ThaPktAnalyzer self)
	{
	ThaPacketReaderPacketRead(ThaPktAnalyzerTxPacketReaderGet(self), cThaPktAnalyzerDumpMpigToGbe);
	}

static void AllRxPacketsRead(ThaPktAnalyzer self)
	{
	ThaPacketReaderPacketRead(ThaPktAnalyzerRxPacketReaderGet(self), cThaPktAnalyzerDumpClaToMpeg);
	}

static ThaPacketReader TxPacketReaderCreate(ThaPktAnalyzer self)
	{
	return ThaPacketReaderDefaultNew((AtPktAnalyzer)self);
	}

static ThaPacketReader RxPacketReaderCreate(ThaPktAnalyzer self)
	{
	return ThaPacketReaderDefaultNew((AtPktAnalyzer)self);
	}

static uint32 PartOffset(ThaPktAnalyzer self)
    {
    ThaEthFlow flow = (ThaEthFlow)AtPktAnalyzerChannelGet((AtPktAnalyzer)self);
    return ThaEthFlowPartOffset(flow, cAtModuleEth);
    }

static void OverrideAtPktAnalyzer(ThaEthFlowPktAnalyzer self)
    {
    AtPktAnalyzer pktAnalyzer = (AtPktAnalyzer)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPktAnalyzerMethods = mMethodsGet(pktAnalyzer);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPktAnalyzerOverride, m_AtPktAnalyzerMethods, sizeof(tAtPktAnalyzerMethods));
        mMethodOverride(m_AtPktAnalyzerOverride, Init);
        }

    mMethodsSet(pktAnalyzer, &m_AtPktAnalyzerOverride);
    }

static void OverrideThaPktAnalyzer(ThaEthFlowPktAnalyzer self)
    {
    ThaPktAnalyzer pktAnalyzer = (ThaPktAnalyzer)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPktAnalyzerMethods = mMethodsGet(pktAnalyzer);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPktAnalyzerOverride, m_ThaPktAnalyzerMethods, sizeof(tThaPktAnalyzerMethods));
        mMethodOverride(m_ThaPktAnalyzerOverride, ChannelIdSet);
        mMethodOverride(m_ThaPktAnalyzerOverride, PartOffset);
        mMethodOverride(m_ThaPktAnalyzerOverride, AllTxPacketsRead);
        mMethodOverride(m_ThaPktAnalyzerOverride, AllRxPacketsRead);
        mMethodOverride(m_ThaPktAnalyzerOverride, TxPacketReaderCreate);
        mMethodOverride(m_ThaPktAnalyzerOverride, RxPacketReaderCreate);
        }

    mMethodsSet(pktAnalyzer, &m_ThaPktAnalyzerOverride);
    }

static void Override(ThaEthFlowPktAnalyzer self)
    {
    OverrideAtPktAnalyzer(self);
    OverrideThaPktAnalyzer(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaEthFlowPktAnalyzer);
    }

AtPktAnalyzer ThaEthFlowPktAnalyzerObjectInit(AtPktAnalyzer self, AtEthFlow flow)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPktAnalyzerObjectInit(self, (AtChannel)flow) == NULL)
        return NULL;

    /* Setup class */
    Override((ThaEthFlowPktAnalyzer)self);
    m_methodsInit = 1;

    return self;
    }

AtPktAnalyzer ThaEthFlowPktAnalyzerNew(AtEthFlow flow)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPktAnalyzer newPktAnalyzer = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPktAnalyzer == NULL)
        return NULL;

    /* Construct it */
    return ThaEthFlowPktAnalyzerObjectInit(newPktAnalyzer, flow);
    }

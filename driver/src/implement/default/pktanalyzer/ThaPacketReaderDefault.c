/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : ThaPacketReaderDefault.c
 *
 * Created Date: Sep 9, 2016
 *
 * Description : Default packet reader
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaPacketReaderInternal.h"
#include "AtPktAnalyzer.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

static tThaPacketReaderMethods m_ThaPacketReaderOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 *PacketRead(ThaPktAnalyzer self, uint32 packetId, uint32 *length, eBool *isCompletedPacket)
    {
    uint16 dword_i;
    uint32 pktLenInDwords;
    uint32 bufferSize;
    uint8 *pBuffer = AtPktAnalyzerBufferGet((AtPktAnalyzer)self, &bufferSize);
    uint32 regVal;
    uint16 startAddr;
    uint32 packetLength;
    uint32 baseAddress = ThaPktAnalyzerBaseAddress(self);

    *isCompletedPacket = cAtTrue;
    /* Find where to get the packet and its length */
    packetLength = mMethodsGet(self)->PacketLenghthGet(self, packetId, &startAddr);
    pktLenInDwords = packetLength / 4;
    if ((packetLength % 4) != 0)
        pktLenInDwords++;
    if (pktLenInDwords == 0)
        return NULL;

    /* Read all of dwords */
    *length = 0;
    for (dword_i = 0; dword_i < pktLenInDwords; dword_i++)
        {
        uint8  byte_i;
        uint32 regAddress = mMethodsGet(self)->PacketDumpDataRegister(self, (uint32)(startAddr + dword_i));

        /* Get each byte of packet until finish one packet or buffer full */
        regVal = ThaPktAnalyzerRead(self, regAddress + baseAddress);
        byte_i = 0;
        while (byte_i < 4)
            {
            /* Read this byte */
            mFieldGet(regVal,
                      cThaDebugDumPktByteGetMask(byte_i),
                      cThaDebugDumPktByteGetShift(byte_i),
                      uint8,
                      &(pBuffer[*length]));
            *length = *length + 1;

            /* Check if it's in-completed packet */
            if ((*length < packetLength) && (*length >= bufferSize))
            	*isCompletedPacket = cAtFalse;

            /* Enough one packet or buffer is full */
            if ((*length == packetLength) || (*length >= bufferSize))
                return pBuffer;

            /* Next byte */
            byte_i = (uint8)(byte_i + 1);
            }
        }

    return pBuffer;
    }

static void AllPacketsDefaultRead(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode)
    {
    uint32 packetNum, numDislayedPackets, packet_i;
    eThaPktAnalyzerPktDumpMode currentDumpMode = ThaPktAnalyzerDumpModeGet(self);
    AtPktAnalyzer analyzer = (AtPktAnalyzer)self;
    eAtPktAnalyzerDirection direction = ThaPktAnalyzerDirectionFromDumpMode(pktDumpMode);

    if (mMethodsGet(self)->AutoFlushPackets(self))
        AtPktAnalyzerPacketFlush(analyzer);

    /* Only allow set PPP packet type when dump packet in Rx of Ethernet flow
     * All other cases need to reset to hardware default */
    if (pktDumpMode != cThaPktAnalyzerDumpClaToMpeg)
        AtPktAnalyzerPppPacketTypeSet(analyzer, cAtPktAnalyzerPppPktTypeMlpppProtocol);

    /* If switch dump mode, reset hardware dump engine */
    if (mMethodsGet(self)->ShouldRecapture(self, (uint8)pktDumpMode, (uint8)currentDumpMode))
        {
        AtOsal osal;
        ThaPktAnalyzerDumpModeSet(self, pktDumpMode);
        osal = AtSharedDriverOsalGet();
        AtPktAnalyzerRecapture(analyzer);
        mMethodsGet(osal)->USleep(osal, 500000);
        }

    /* Get the number of packet will be displayed */
    packetNum = mMethodsGet(self)->NumPacketInBuffer(self);
    numDislayedPackets = (packetNum >= analyzer->displayedPacketNum) ? analyzer->displayedPacketNum : packetNum;
    for(packet_i = 0; packet_i < numDislayedPackets; packet_i++)
        {
        uint32 length = 0;
        eBool isCompletedPacket;
        uint8 *data = PacketRead(self, packet_i, &length, &isCompletedPacket);
        AtPacket packet;
        if (data == NULL)
            continue;

        packet = mMethodsGet(analyzer)->PacketCreate(analyzer, data, length, direction);
        AtPacketIsCompletedMark(packet, isCompletedPacket);
        AtPacketFactorySet(packet, AtPktAnalyzerPacketFactory(analyzer));
        AtListObjectAdd(AtPktAnalyzerPacketListGet(analyzer), (AtObject)packet);
        }
    }

static void Read(ThaPacketReader self, eThaPktAnalyzerPktDumpMode dumpMode)
	{
	ThaPktAnalyzer analyzer = (ThaPktAnalyzer)mMethodsGet(self)->AnalyzerGet(self);
	AllPacketsDefaultRead(analyzer, dumpMode);
	}

static void OverrideThaPacketReader(ThaPacketReaderDefault self)
    {
	ThaPacketReader reader = (ThaPacketReader)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPacketReaderOverride, mMethodsGet(reader), sizeof(tThaPacketReaderMethods));
        mMethodOverride(m_ThaPacketReaderOverride, Read);
        }

    mMethodsSet(reader, &m_ThaPacketReaderOverride);
    }

static void Override(ThaPacketReaderDefault self)
	{
	OverrideThaPacketReader(self);
	}

ThaPacketReader ThaPacketReaderDefaultObjectInit(ThaPacketReader self, AtPktAnalyzer analyzer)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaPacketReaderDefault));

    /* Call super constructor to reuse all of its implementation */
    if (ThaPacketReaderObjectInit(self, analyzer) == NULL)
        return NULL;

    /* Setup class */
    Override((ThaPacketReaderDefault)self);
    m_methodsInit = 1;

    return self;
    }

ThaPacketReader ThaPacketReaderDefaultNew(AtPktAnalyzer self)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPacketReader reader = mMethodsGet(osal)->MemAlloc(osal, sizeof(tThaPacketReaderDefault));
    if (self == NULL)
        return NULL;

    /* Construct it */
    return ThaPacketReaderDefaultObjectInit(reader, self);
    }

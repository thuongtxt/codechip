/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : ThaModulePktAnalyzer.c
 *
 * Created Date: Apr 26, 2013
 *
 * Description : Packet analyzer for Thalassa product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaModulePktAnalyzerInternal.h"
#include "ThaEthPortPktAnalyzerInternal.h"
#include "ThaEthFlowPktAnalyzerInternal.h"
#include "ThaModulePktAnalyzer.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaModulePktAnalyzer)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaModulePktAnalyzerMethods m_methods;

/* Override */
static tAtModulePktAnalyzerMethods m_AtModulePktAnalyzerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaModulePktAnalyzer);
    }

static AtPktAnalyzer EthPortPktAnalyzerCreate(AtModulePktAnalyzer self, AtEthPort port)
    {
	AtUnused(self);
    return ThaEthPortPktAnalyzerNew(port);
    }

static uint32 StartVersionSupportsHigig(ThaModulePktAnalyzer self)
    {
	AtUnused(self);
    /* Concrete product know that. Just return a maximum value to indicate that
     * this feature has not been supported. */
    return cBit31_0;
    }

static void OverrideAtModulePktAnalyzer(AtModulePktAnalyzer self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePktAnalyzerOverride, mMethodsGet(self), sizeof(m_AtModulePktAnalyzerOverride));

        mMethodOverride(m_AtModulePktAnalyzerOverride, EthPortPktAnalyzerCreate);
        }

    mMethodsSet(self, &m_AtModulePktAnalyzerOverride);
    }

static void Override(AtModulePktAnalyzer self)
    {
    OverrideAtModulePktAnalyzer(self);
    }

static void MethodsInit(ThaModulePktAnalyzer self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, StartVersionSupportsHigig);
        }

    mMethodsSet(self, &m_methods);
    }

AtModulePktAnalyzer ThaModulePktAnalyzerObjectInit(AtModulePktAnalyzer self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtModulePktAnalyzerObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtModulePktAnalyzer ThaModulePktAnalyzerNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePktAnalyzer newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ThaModulePktAnalyzerObjectInit(newModule, device);
    }

eBool ThaModulePktAnalyzerHigigEthPacketIsSupported(ThaModulePktAnalyzer self)
    {
    uint32 supportedVersion;
    if (self == NULL)
        return cAtFalse;

    supportedVersion = mMethodsGet(self)->StartVersionSupportsHigig(self);
    return AtDeviceVersionNumber(AtModuleDeviceGet((AtModule)self)) >= supportedVersion;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : ThaEthPortPktAnalyzerInternal.h
 * 
 * Created Date: Dec 21, 2012
 *
 * Description : Ethernet port packet analyzer
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAETHPORTPKTANALYZERINTERNAL_H_
#define _THAETHPORTPKTANALYZERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaPktAnalyzerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaEthPortPktAnalyzer * ThaEthPortPktAnalyzer;

typedef struct tThaEthPortPktAnalyzer
    {
    tThaPktAnalyzer super;

    /* Private */
    }tThaEthPortPktAnalyzer;

typedef struct tThaEthPortPktAnalyzerV2 * ThaEthPortPktAnalyzerV2;

typedef struct tThaEthPortPktAnalyzerV2
    {
    tThaEthPortPktAnalyzer super;

    /* Private data */
    }tThaEthPortPktAnalyzerV2;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPktAnalyzer ThaEthPortPktAnalyzerObjectInit(AtPktAnalyzer self, AtEthPort port);
AtPktAnalyzer ThaEthPortPktAnalyzerV2ObjectInit(AtPktAnalyzer self, AtEthPort port);

#ifdef __cplusplus
}
#endif
#endif /* _THAETHPORTPKTANALYZERINTERNAL_H_ */


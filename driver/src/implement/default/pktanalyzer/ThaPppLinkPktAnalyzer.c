/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : ThaPppLinkPktAnalyzer.c
 *
 * Created Date: Dec 20, 2012
 *
 * Description : PPP Link packet analyzer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtHdlcPacket.h"
#include "AtPppPacket.h"
#include "../ppp/ThaPppLink.h"
#include "ThaPppLinkPktAnalyzerInternal.h"
#include "ThaPacketReaderInternal.h"

/*--------------------------- Define -----------------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPktAnalyzerMethods  m_AtPktAnalyzerOverride;
static tThaPktAnalyzerMethods m_ThaPktAnalyzerOverride;

/* Save super implementation */
static tAtPktAnalyzerMethods *m_AtPktAnalyzerMethods;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Init(AtPktAnalyzer self)
    {
    /* Super */
    m_AtPktAnalyzerMethods->Init(self);
    ThaPktAnalyzerDumpModeSet((ThaPktAnalyzer)self, cThaPktAnalyzerDumpMpegToEnc);
    mMethodsGet(self)->Recapture(self);
    }

static AtPacket PacketCreate(AtPktAnalyzer self, uint8 *data, uint32 length, eAtPktAnalyzerDirection direction)
    {
    AtPacketFactory factory = AtPktAnalyzerPacketFactory(self);

    if (ThaPktAnalyzerDumpModeGet((ThaPktAnalyzer)self) == cThaPktAnalyzerDumpMpegToEnc)
        return AtPacketFactoryPppPacketCreate(factory, data, length, cAtPacketCacheModeCacheData, direction);
    else
        return AtPacketFactoryHdlcPacketCreate(factory, data, length, cAtPacketCacheModeCacheData, direction);
    }

static void AllTxPacketsRead(ThaPktAnalyzer self)
	{
	ThaPacketReaderPacketRead(ThaPktAnalyzerTxPacketReaderGet(self), cThaPktAnalyzerDumpMpegToEnc);
	}

static void AllRxPacketsRead(ThaPktAnalyzer self)
	{
	ThaPacketReaderPacketRead(ThaPktAnalyzerRxPacketReaderGet(self), cThaPktAnalyzerDumpDecToMpig);
	}

static uint32 PartOffset(ThaPktAnalyzer self)
    {
    ThaPppLink link = (ThaPppLink)AtPktAnalyzerChannelGet((AtPktAnalyzer)self);
    return ThaPppLinkPartOffset(link);
    }

static void OverrideThaPktAnalyzer(ThaPppLinkPktAnalyzer self)
    {
    ThaPktAnalyzer pktAnalyzer = (ThaPktAnalyzer)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPktAnalyzerOverride, mMethodsGet(pktAnalyzer), sizeof(tThaPktAnalyzerMethods));
        mMethodOverride(m_ThaPktAnalyzerOverride, PartOffset);
        mMethodOverride(m_ThaPktAnalyzerOverride, AllTxPacketsRead);
        mMethodOverride(m_ThaPktAnalyzerOverride, AllRxPacketsRead);
        }

    mMethodsSet(pktAnalyzer, &m_ThaPktAnalyzerOverride);
    }

static void OverrideAtPktAnalyzer(ThaPppLinkPktAnalyzer self)
    {
    AtPktAnalyzer pktAnalyzer = (AtPktAnalyzer)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPktAnalyzerMethods = mMethodsGet(pktAnalyzer);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPktAnalyzerOverride, m_AtPktAnalyzerMethods, sizeof(tAtPktAnalyzerMethods));
        mMethodOverride(m_AtPktAnalyzerOverride, Init);
        mMethodOverride(m_AtPktAnalyzerOverride, PacketCreate);
        }

    mMethodsSet(pktAnalyzer, &m_AtPktAnalyzerOverride);
    }

static void Override(ThaPppLinkPktAnalyzer self)
    {
    OverrideAtPktAnalyzer(self);
    OverrideThaPktAnalyzer(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPppLinkPktAnalyzer);
    }

AtPktAnalyzer ThaPppLinkPktAnalyzerObjectInit(AtPktAnalyzer self, AtPppLink link)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPktAnalyzerObjectInit(self, (AtChannel)link) == NULL)
        return NULL;

    /* Setup class */
    Override((ThaPppLinkPktAnalyzer)self);
    m_methodsInit = 1;

    return self;
    }

AtPktAnalyzer ThaPppLinkPktAnalyzerNew(AtPppLink link)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPktAnalyzer newPktAnalyzer = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPktAnalyzer == NULL)
        return NULL;

    /* Construct it */
    return ThaPppLinkPktAnalyzerObjectInit(newPktAnalyzer, link);
    }

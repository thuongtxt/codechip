/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : ThaPacketFactoryInternal.h
 * 
 * Created Date: Mar 31, 2014
 *
 * Description : Packet factory
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPACKETFACTORYINTERNAL_H_
#define _THAPACKETFACTORYINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/pktanalyzer/AtPacketFactoryInternal.h"
#include "ThaPacketFactory.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPacketFactory
    {
    tAtPacketFactory super;

    /* Private data */
    AtDevice device;
    }tThaPacketFactory;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPacketFactory ThaPacketFactoryObjectInit(AtPacketFactory self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAPACKETFACTORYINTERNAL_H_ */


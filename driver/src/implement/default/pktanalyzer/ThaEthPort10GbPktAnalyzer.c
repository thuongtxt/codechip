/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : ThaEthPort10GbPktAnalyzer.c
 *
 * Created Date: Jul 20, 2015
 *
 * Description : Ethernet Port 10G packet analyzer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaEthPort10GbPktAnalyzer.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 PortIdToAnalyze(ThaPktAnalyzer self)
    {
    AtEthPort port = (AtEthPort)AtPktAnalyzerChannelGet((AtPktAnalyzer)self);
    return (uint8)AtChannelIdGet((AtChannel)port);
    }

static uint8 CurrentPortId(ThaPktAnalyzer self)
    {
    uint32 regAddr = cThaDiagPktDumpModeCtrl + ThaPktAnalyzerBaseAddress(self);
    uint32 regVal = ThaPktAnalyzerRead(self, regAddr);
    uint32 hwPktMode = mRegField(regVal, cThaDebugDumPktSource);

    if ((hwPktMode == 0x2) || (hwPktMode == 0x3)) return 0x1;
    if ((hwPktMode == 0x0) || (hwPktMode == 0x1)) return 0x0;

    return 0x0;
    }

uint32 Tha10GProductPktAnalyzerStartAddress(ThaPktAnalyzer self)
    {
    AtUnused(self);
    return 0x3cf000;
    }

uint32 Tha10GPktAnalyzerEthPortHwDumpMode(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode)
    {
    uint8 portId = PortIdToAnalyze(self);

    if (pktDumpMode == cThaPktAnalyzerDumpGbeTx)
        return (portId == 0) ? 0x0 : 0x2;

    if (pktDumpMode == cThaPktAnalyzerDumpGbeRx)
        return (portId == 0) ? 0x1 : 0x3;

    /* Invalid value */
    return 0x8;
    }

uint32 Tha10GPktAnalyzerEthPortDumpModeHw2Sw(ThaPktAnalyzer self, uint32 pktDumpMode)
    {
    AtUnused(self);
    if ((pktDumpMode == 0x0) || (pktDumpMode == 0x2)) return cThaPktAnalyzerDumpGbeTx;
    if ((pktDumpMode == 0x1) || (pktDumpMode == 0x3)) return cThaPktAnalyzerDumpGbeRx;

    return cThaPktAnalyzerDumpUnknown;
    }

eBool Tha10GbPktAnalyzerShouldRecapture(ThaPktAnalyzer self, uint8 pktDumpMode, uint8 currentDumpMode)
    {
    eBool portChanged     = (PortIdToAnalyze(self) != CurrentPortId(self));
    eBool dumpModeChanged = (currentDumpMode != pktDumpMode) && (currentDumpMode != cThaPktAnalyzerDumpUnknown);

    if (dumpModeChanged || portChanged)
        return cAtTrue;

    return cAtFalse;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : ThaModulePktAnalyzerInternal.h
 * 
 * Created Date: Jun 20, 2013
 *
 * Description : Packet analyzer module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEPKTANALYZERINTERNAL_H_
#define _THAMODULEPKTANALYZERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/pktanalyzer/AtModulePktAnalyzerInternal.h"
#include "ThaModulePktAnalyzer.h"
#include "ThaPktAnalyzer.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModulePktAnalyzerMethods
    {
    uint32 (*StartVersionSupportsHigig)(ThaModulePktAnalyzer self);
    }tThaModulePktAnalyzerMethods;

typedef struct tThaModulePktAnalyzer
    {
    tAtModulePktAnalyzer super;
    const tThaModulePktAnalyzerMethods *methods;
    }tThaModulePktAnalyzer;

typedef struct tThaModulePktAnalyzerPw
    {
    tThaModulePktAnalyzer super;
    }tThaModulePktAnalyzerPw;

typedef struct tThaModulePktAnalyzerPpp
    {
    tThaModulePktAnalyzer super;
    }tThaModulePktAnalyzerPpp;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePktAnalyzer ThaModulePktAnalyzerObjectInit(AtModulePktAnalyzer self, AtDevice device);
AtModulePktAnalyzer ThaModulePktAnalyzerPwObjectInit(AtModulePktAnalyzer self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEPKTANALYZERINTERNAL_H_ */


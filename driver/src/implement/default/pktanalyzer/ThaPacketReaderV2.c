/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : ThaPacketReaderStateMachine.c
 *
 * Created Date: Sep 9, 2016
 *
 * Description : Packet reader follow state machine
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaPacketReaderInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

static tThaPacketReaderMethods m_ThaPacketReaderOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 *PutByte(uint8 value, uint8 *currentByte, uint8 *buffer, uint32 bufferSize)
    {
    if (currentByte == NULL)
        return NULL;

    /* No space left */
    if ((uint32)(currentByte - buffer) >= bufferSize)
        return currentByte;

    *currentByte = value;

    /* Next byte position */
    return currentByte + 1;
    }

static uint8 *PutBytesFromDword(uint32 value, uint8 numBytes, uint8 *currentByte, uint8 *buffer, uint32 bufferSize)
    {
    uint32 mask = cBit31_24;
    uint8 shift = 24;
    uint8 byte_i, byteValue;

    for (byte_i = 0; byte_i < numBytes; byte_i++)
        {
        uint8 *nextByte;

        /* Put this byte */
        mFieldGet(value, mask, shift, uint8, &byteValue);
        nextByte = PutByte(byteValue, currentByte, buffer, bufferSize);

        /* Cannot put any more */
        if (nextByte == currentByte)
            break;

        /* Next byte */
        currentByte = nextByte;
        mask  = mask >> 8;
        shift = (uint8)(shift - 8);
        }

    return currentByte;
    }

static uint8 *PutDataToBuffer(ThaPktAnalyzer self, uint32 *longVal, uint8 numBytes, uint8 *currentByte, uint8 *buffer, uint32 bufferSize)
	{
	static const uint8 cNumBytePerDword = 4;
	uint8 msbDataIndex = (uint8)(mMetaDwordIndex(self) - 1);

	if (numBytes > cNumBytePerDword)
		{
		currentByte = PutBytesFromDword(longVal[1], cNumBytePerDword, currentByte, buffer, bufferSize);
		return PutBytesFromDword(longVal[0], (uint8)(numBytes - cNumBytePerDword), currentByte, buffer, bufferSize);
		}

	return PutBytesFromDword(longVal[msbDataIndex], numBytes, currentByte, buffer, bufferSize);
	}

static void AllPacketsReadByStateMachine(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode)
    {
    static const uint32 cWaitPacketsInMs = 500;

    uint32 data[cThaLongRegMaxSize];
    uint32 prevAddr, regAddr, lastAddr, bufferSize, packetLength;
    AtPktAnalyzer analyzer = (AtPktAnalyzer)self;
    AtOsal osal = AtSharedDriverOsalGet();
    uint8 readingState, numBytes;
    uint8 *buffer = AtPktAnalyzerBufferGet((AtPktAnalyzer)self, &bufferSize);
    uint8 *pCurrentByte = buffer;
    eAtPktAnalyzerDirection direction = ThaPktAnalyzerDirectionFromDumpMode(pktDumpMode);
    eBool packetError = cAtFalse;

    if (!mMethodsGet(self)->DumpModeIsSupported(self, pktDumpMode))
        return;

    /* Flush all packets */
    if (mMethodsGet(self)->AutoFlushPackets(self))
        AtPktAnalyzerPacketFlush(analyzer);
    mMethodsGet(osal)->MemInit(osal, buffer, 0, sizeof(uint8) * bufferSize);

    /* Activate */
    mMethodsGet(self)->DumpModeSet(self, pktDumpMode);

    /* Wait a moment for packets come */
    mMethodsGet(osal)->USleep(osal, cWaitPacketsInMs * 1000);

    /* Read all of packets */
    readingState = cStateFindStartOfPacket;
    regAddr = mMethodsGet(self)->FirstAddressOfBuffer(self);
    lastAddr = mMethodsGet(self)->LastAddressOfBuffer(self);
    prevAddr = cInvalidUint32;
    while (regAddr <= lastAddr)
        {
        /* There are some interfaces that have an issue that when reading data
         * buffer register at one specific address more than one times, the
         * subsequence read will have wrong value returned. So, to fix this
         * issue and still have current interfaces that have been working so far,
         * data buffer registers are only read once */
        if (prevAddr != regAddr)
            mMethodsGet(self)->EntryInfoRead(self, regAddr, data);
        prevAddr = regAddr;

        packetError = mMethodsGet(self)->IsErrorPacket(self, regAddr, data);

        switch (readingState)
            {
            /* Find start of packet */
            case cStateFindStartOfPacket:
                if (mMethodsGet(self)->IsStartOfPacket(self, regAddr, data))
                    {
                    numBytes = mMethodsGet(self)->EntryRead(self, regAddr, data);
                    pCurrentByte = PutDataToBuffer(self, data, numBytes, pCurrentByte, buffer, bufferSize);
                    readingState = cStateReading;
                    }
                regAddr++;
                break;

            /* Reading packet */
            case cStateReading:
                if (mMethodsGet(self)->IsEndOfPacket(self, regAddr, data))
                    readingState = cStateEndOfPacket;
                else
                    {
                    numBytes = mMethodsGet(self)->EntryRead(self, regAddr, data);
                    pCurrentByte = PutDataToBuffer(self, data, numBytes, pCurrentByte, buffer, bufferSize);
                    regAddr++;
                    }
                break;

            /* End of packet */
            case cStateEndOfPacket:
                {
                AtPacket packet;
                numBytes = mMethodsGet(self)->EntryRead(self, regAddr, data);
                pCurrentByte = PutDataToBuffer(self, data, numBytes, pCurrentByte, buffer, bufferSize);

                /* Create packet */
                packetLength = (uint32)(pCurrentByte - buffer);
                packet = mMethodsGet(analyzer)->PacketCreate(analyzer, buffer, packetLength, direction);
                AtPacketMarkError(packet, packetError);
                AtPacketFactorySet(packet, AtPktAnalyzerPacketFactory(analyzer));
                AtListObjectAdd(AtPktAnalyzerPacketListGet(analyzer), (AtObject)packet);

                /* For next packet */
                buffer       = buffer + packetLength;
                pCurrentByte = buffer;
                bufferSize   = bufferSize - packetLength;
                readingState = cStateFindStartOfPacket;
                packetError  = cAtFalse;
                regAddr++;
                break;
                }

            /* Ignore unknown case */
            default:
                break;
            }

        /* No space left */
        if ((uint32)(pCurrentByte - buffer) >= bufferSize)
            break;
        }

    /* May be the last packet is not end, still put it */
    if (mMethodsGet(self)->SkipLastInCompletePacket(self))
        return;

    /* Just put in complete packet that has length >= minimum length */
    packetLength = (uint32)(pCurrentByte - buffer);
    if (packetLength >= 64)
        {
        AtPacket packet = mMethodsGet(analyzer)->PacketCreate(analyzer, buffer, packetLength, direction);
        AtPacketIsCompletedMark(packet, cAtFalse);
        AtPacketFactorySet(packet, AtPktAnalyzerPacketFactory(analyzer));
        AtListObjectAdd(AtPktAnalyzerPacketListGet(analyzer), (AtObject)packet);
        }
    }

static void Read(ThaPacketReader self, eThaPktAnalyzerPktDumpMode dumpMode)
	{
	ThaPktAnalyzer analyzer = (ThaPktAnalyzer)mMethodsGet(self)->AnalyzerGet(self);
	AllPacketsReadByStateMachine(analyzer, dumpMode);
	}

static void OverrideThaPacketReader(ThaPacketReader self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPacketReaderOverride, mMethodsGet(self), sizeof(tThaPacketReaderMethods));
        mMethodOverride(m_ThaPacketReaderOverride, Read);
        }

    mMethodsSet(self, &m_ThaPacketReaderOverride);
    }

static void Override(ThaPacketReader self)
	{
	OverrideThaPacketReader(self);
	}

ThaPacketReader ThaPacketReaderV2ObjectInit(ThaPacketReader self, AtPktAnalyzer analyzer)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaPacketReaderV2));

    /* Call super constructor to reuse all of its implementation */
    if (ThaPacketReaderDefaultObjectInit(self, analyzer) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPacketReader ThaPacketReaderV2New(AtPktAnalyzer self)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPacketReader reader = mMethodsGet(osal)->MemAlloc(osal, sizeof(tThaPacketReaderV2));
    if (self == NULL)
        return NULL;

    /* Construct it */
    return ThaPacketReaderV2ObjectInit(reader, self);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : ThaPppLinkPktAnalyzerInternal.h
 * 
 * Created Date: Dec 21, 2012
 *
 * Description : PPP Link packet analyzer
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPPPLINKPKTANALYZERINTERNAL_H_
#define _THAPPPLINKPKTANALYZERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaPktAnalyzerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
typedef struct tThaPppLinkPktAnalyzer * ThaPppLinkPktAnalyzer;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPppLinkPktAnalyzer
    {
    tThaPktAnalyzer super;

    /* Private */
    }tThaPppLinkPktAnalyzer;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPktAnalyzer ThaPppLinkPktAnalyzerObjectInit(AtPktAnalyzer self, AtPppLink link);


#ifdef __cplusplus
}
#endif
#endif /* _THAPPPLINKPKTANALYZERINTERNAL_H_ */


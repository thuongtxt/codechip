/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : ThaPacketReader.h
 *
 * Created Date: Sep 9, 2016
 *
 * Description : Thalassa default packet reader
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _THAPACKETREADER_H_
#define _THAPACKETREADER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h" /* Super class */


/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPacketReader * ThaPacketReader;
typedef struct tThaPacketReaderDefault * ThaPacketReaderDefault;
typedef struct tThaPacketReaderStateMachine * ThaPacketReaderStateMachine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPacketReader ThaPacketReaderDefaultNew(AtPktAnalyzer self);
ThaPacketReader ThaPacketReaderV2New(AtPktAnalyzer self);

#ifdef __cplusplus
}
#endif
#endif /* _THAPACKETREADER_H_ */

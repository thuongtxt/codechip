/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : ThaEthPort10GbPktAnalyzer.h
 * 
 * Created Date: Jul 20, 2015
 *
 * Description : Ethernet Port 10G packet analyzer
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _DRIVER_SRC_IMPLEMENT_DEFAULT_PKTANALYZER_THAETHPORT10GBPKTANALYZER_H_
#define _DRIVER_SRC_IMPLEMENT_DEFAULT_PKTANALYZER_THAETHPORT10GBPKTANALYZER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaEthPortPktAnalyzerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 Tha10GPktAnalyzerEthPortHwDumpMode(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode);
uint32 Tha10GPktAnalyzerEthPortDumpModeHw2Sw(ThaPktAnalyzer self, uint32 pktDumpMode);
eBool Tha10GbPktAnalyzerShouldRecapture(ThaPktAnalyzer self, uint8 pktDumpMode, uint8 currentDumpMode);
uint32 Tha10GProductPktAnalyzerStartAddress(ThaPktAnalyzer self);

#ifdef __cplusplus
}
#endif
#endif /* _DRIVER_SRC_IMPLEMENT_DEFAULT_PKTANALYZER_THAETHPORT10GBPKTANALYZER_H_ */


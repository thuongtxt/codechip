/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha6021EthSerdesManagerInternal.h
 * 
 * Created Date: Jul 18, 2015
 *
 * Description : Serdes manager
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6021ETHSERDESMANAGERINTERNAL_H_
#define _THA6021ETHSERDESMANAGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/man/AtDriverInternal.h"
#include "AtObject.h"

#include "ThaEthSerdesManager.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaEthSerdesManagerMethods
    {
    eAtRet (*Setup)(ThaEthSerdesManager self);
    eAtRet (*Init)(ThaEthSerdesManager self);
    AtSerdesController (*SerdesControllerCreate)(ThaEthSerdesManager self, AtEthPort port, uint32 serdesId);
    AtSerdesController (*SerdesController)(ThaEthSerdesManager self, uint32 serdesId);
    uint32 (*NumSerdesController)(ThaEthSerdesManager self);
    eBool (*ShouldEnableSerdes)(ThaEthSerdesManager self, uint32 serdesId);
    eBool (*ShouldInitSerdes)(ThaEthSerdesManager self, uint32 serdesId);
    eBool (*CanControlEqualizer)(ThaEthSerdesManager self, AtSerdesController serdes);
    eBool (*SerdesIsUsed)(ThaEthSerdesManager self, AtSerdesController serdesController);
    eBool (*HasXfiGroups)(ThaEthSerdesManager self);
    uint32 (*NumXfiGroups)(ThaEthSerdesManager self);
    const uint32 *(*XfisForGroup)(ThaEthSerdesManager self, uint32 groupId, uint32 *numPorts);
    }tThaEthSerdesManagerMethods;

typedef struct tThaEthSerdesManager
    {
    tAtObject super;
    const tThaEthSerdesManagerMethods *methods;

    /* Private data */
    AtModuleEth ethModule;
    AtSerdesController *serdesControllers;

    /* There is case when number of actually created controllers is
     * different with number of controller returned by method NumSerdesController due to
     * version compatible, need to cache number of actually created controller for deleting purpose
     * to prevent mem leak */
    uint32 numCreatedController;
    }tThaEthSerdesManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaEthSerdesManager ThaEthSerdesManagerObjectInit(ThaEthSerdesManager self, AtModuleEth ethModule);

#ifdef __cplusplus
}
#endif
#endif /* _THA6021ETHSERDESMANAGERINTERNAL_H_ */


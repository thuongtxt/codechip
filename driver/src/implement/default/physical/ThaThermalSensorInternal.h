/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PHYSICAL
 * 
 * File        : ThaThermalSensorInternal.h
 * 
 * Created Date: Dec 15, 2015
 *
 * Description : Default thermal sensor represenation.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THATHERMALSENSORINTERNAL_H_
#define _THATHERMALSENSORINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/physical/AtThermalSensorInternal.h"
#include "ThaThermalSensor.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaThermalSensorMethods
    {
    uint32 (*BaseAddress)(ThaThermalSensor self);
    }tThaThermalSensorMethods;

typedef struct tThaThermalSensor
    {
    tAtThermalSensor super;
    const tThaThermalSensorMethods *methods;
    }tThaThermalSensor;

/*--------------------------- Forward declarations ---------------------------*/
AtThermalSensor ThaThermalSensorObjectInit(AtThermalSensor self, AtDevice device);

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THATHERMALSENSORINTERNAL_H_ */


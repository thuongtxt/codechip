/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : ThaSdhLineSerdesController.h
 * 
 * Created Date: Dec 17, 2013
 *
 * Description : SDH Line SERDES controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASDHLINESERDESCONTROLLER_H_
#define _THASDHLINESERDESCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaSerdesController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet ThaSdhLineSerdesControllerRateSet(ThaSdhLineSerdesController self, eAtSdhLineRate rate);

/* Concrete products */
AtSerdesController Tha60001031SdhLineSerdesControllerNew(AtSdhLine sdhLine, uint32 serdesId);

#ifdef __cplusplus
}
#endif
#endif /* _THASDHLINESERDESCONTROLLER_H_ */


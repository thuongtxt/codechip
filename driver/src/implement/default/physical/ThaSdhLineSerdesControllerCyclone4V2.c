/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Module SDH
 *
 * File        : ThaSdhLineSerdesControllerCylone4V2.c
 *
 * Created Date: Apr 8, 2014
 *
 * Description : Cylone4 SDH Line Serdes
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaSdhLineSerdesControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
/* Physical parameters register */
#define cParamValueReg      0xF40000
#define cRxDcGainMask       cBit3_0
#define cRxDcGainShift      0

#define cRxEqualizerMask    cBit7_4
#define cRxEqualizerShift   4

#define cTxVodMask          cBit11_8
#define cTxVodShift         8

#define cTxPreEmphasisMask  cBit19_12
#define cTxPreEmphasisShift 12

/* Loopback register
 * 0: loop back release
 * 1: loop back in
 * 2: loop back out post CDR
 * 3: loop back out pre-CDR
 * */
#define cLoopbackReg        0xF40010
#define cLoopbackModeMask   cBit1_0
#define cLoopbackModeShift  0

/* All hardware parameters */
#define cHwParamVod                   0
#define cHwParamPreTap                1
#define cHwParamRxEqualizationDcGain  3
#define cHwParamRxEqualizationControl 4

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)   ((tThaSdhLineSerdesControllerCylone4V2 *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaSdhLineSerdesControllerCylone4V2
    {
    tThaSdhLineSerdesControllerCyclone4 super;

    /* Private data */
    uint32 vod;
    uint32 preTap;
    uint32 rxEqualizationDcGain;
    uint32 rxEqualizationControl;
    eBool  loopinIsEnabled;
    eBool  loopoutIsEnabled;
    }tThaSdhLineSerdesControllerCylone4V2;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaSerdesControllerMethods m_ThaSerdesControllerOverride;
static tAtSerdesControllerMethods  m_AtSerdesControllerOverride;

/* Save super implementation */
static const tAtSerdesControllerMethods *m_AtSerdesControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaSdhLineSerdesControllerCylone4V2);
    }

static uint32 PhysicalPortIdGet(AtSerdesController self)
    {
    return AtChannelIdGet(AtSerdesControllerPhysicalPortGet(self));
    }

static uint32 ParamRegisterReadOnDatabase(ThaSerdesController self)
    {
    uint32 regVal = 0;
    mRegFieldSet(regVal, cTxVod,         mThis(self)->vod);
    mRegFieldSet(regVal, cRxDcGain,      mThis(self)->rxEqualizationDcGain);
    mRegFieldSet(regVal, cRxEqualizer,   mThis(self)->rxEqualizationControl);
    mRegFieldSet(regVal, cTxPreEmphasis, mThis(self)->preTap);

    return regVal;
    }

static void WaitHwUpdate(uint32 timeInUs)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->USleep(osal, timeInUs);
    }

static eAtRet HwVodSet(ThaSerdesController self, uint32 value)
    {
    AtSerdesController controller = (AtSerdesController)self;
    uint32 address = cParamValueReg + PhysicalPortIdGet(controller);
    uint32 regValue = ParamRegisterReadOnDatabase(self);
    mRegFieldSet(regValue, cTxVod, value);
    AtSerdesControllerWrite(controller, address, regValue, cAtModuleSdh);
    WaitHwUpdate(5000);
    mThis(self)->vod = value;

    return cAtOk;
    }

static uint32 HwVodGet(ThaSerdesController self)
    {
    return mThis(self)->vod;
    }

static eAtRet HwPreEmphasisSet(ThaSerdesController self, uint32 value)
    {
    AtSerdesController controller = (AtSerdesController)self;
    uint32 regAddr  = cParamValueReg + PhysicalPortIdGet(controller);
    uint32 regValue = ParamRegisterReadOnDatabase(self);
    mRegFieldSet(regValue, cTxPreEmphasis, value);
    AtSerdesControllerWrite(controller, regAddr, regValue, cAtModuleSdh);
    WaitHwUpdate(5000);
    mThis(self)->preTap = value;

    return cAtOk;
    }

static uint32 HwPreEmphasisGet(ThaSerdesController self)
    {
    return mThis(self)->preTap;
    }

static eAtRet HwRxEqualizerSet(ThaSerdesController self, uint32 value)
    {
    AtSerdesController controller = (AtSerdesController)self;
    uint32 regAddr  = cParamValueReg + PhysicalPortIdGet(controller);
    uint32 regValue = ParamRegisterReadOnDatabase(self);
    mRegFieldSet(regValue, cRxEqualizer, value);
    AtSerdesControllerWrite(controller, regAddr, regValue, cAtModuleSdh);
    WaitHwUpdate(5000);
    mThis(self)->rxEqualizationControl = value;

    return cAtOk;
    }

static uint32 HwPreRxEqualizerGet(ThaSerdesController self)
    {
    return mThis(self)->rxEqualizationControl;
    }

static eAtRet HwRxDcGainSet(ThaSerdesController self, uint32 value)
    {
    AtSerdesController controller = (AtSerdesController)self;
    uint32 regAddr  = cParamValueReg + PhysicalPortIdGet(controller);
    uint32 regValue = ParamRegisterReadOnDatabase(self);
    mRegFieldSet(regValue, cRxDcGain, value);
    AtSerdesControllerWrite(controller, regAddr, regValue, cAtModuleSdh);
    WaitHwUpdate(5000);
    mThis(self)->rxEqualizationDcGain = value;

    return cAtOk;
    }

static uint32 HwPreRxDcGainGet(ThaSerdesController self)
    {
    return mThis(self)->rxEqualizationDcGain;
    }

static void HwParamsReConfigure(ThaSerdesController self)
    {
    HwVodSet(self, mThis(self)->vod);
    HwPreEmphasisSet(self, mThis(self)->preTap);
    HwRxDcGainSet(self, mThis(self)->rxEqualizationDcGain);
    HwRxEqualizerSet(self, mThis(self)->rxEqualizationControl);
    }

static eAtRet HwPhysicalParamSet(ThaSerdesController self, uint32 hwParam, uint32 value)
    {
    if (hwParam == cHwParamVod)                  return HwVodSet(self, value);
    if (hwParam == cHwParamPreTap)               return HwPreEmphasisSet(self, value);
    if (hwParam == cHwParamRxEqualizationDcGain) return HwRxDcGainSet(self, value);
    if (hwParam == cHwParamRxEqualizationControl)return HwRxEqualizerSet(self, value);

    return cAtErrorModeNotSupport;
    }

static uint32 HwPhysicalParamGet(ThaSerdesController self, uint32 hwParam)
    {
    if (hwParam == cHwParamVod)                  return HwVodGet(self);
    if (hwParam == cHwParamPreTap)               return HwPreEmphasisGet(self);
    if (hwParam == cHwParamRxEqualizationDcGain) return HwPreRxDcGainGet(self);
    if (hwParam == cHwParamRxEqualizationControl)return HwPreRxEqualizerGet(self);

    return cAtErrorModeNotSupport;
    }

static eAtRet Init(AtSerdesController self)
    {
    m_AtSerdesControllerMethods->Init(self);

    AtSerdesControllerPhysicalParamSet(self, cAtSerdesParamVod, 0x7);
    AtSerdesControllerPhysicalParamSet(self, cAtSerdesParamPreEmphasisPreTap, 0x9);
    AtSerdesControllerPhysicalParamSet(self, cAtSerdesParamRxEqualizationControl, 0x0);
    AtSerdesControllerPhysicalParamSet(self, cAtSerdesParamRxEqualizationDcGain, 0x0);

    return cAtOk;
    }

static uint32 LoopbackReadReadOnDatabase(ThaSerdesController self)
    {
    if (mThis(self)->loopinIsEnabled)
        return 0x1;
    if (mThis(self)->loopoutIsEnabled)
        return 0x2;

    return 0x0;
    }

static eAtRet LoopInEnable(ThaSerdesController self, eBool enable)
    {
    AtSerdesController controller = (AtSerdesController)self;
    uint32 regAddr = cLoopbackReg + PhysicalPortIdGet(controller);
    uint32 regVal  = LoopbackReadReadOnDatabase(self);
    mRegFieldSet(regVal, cLoopbackMode, enable ? 1 : 0);
    AtSerdesControllerWrite(controller, regAddr, regVal, cAtModuleSdh);
    WaitHwUpdate(10000);
    mThis(self)->loopinIsEnabled = enable;

    return cAtOk;
    }

static eAtRet LoopOutEnable(ThaSerdesController self, eBool enable)
    {
    AtSerdesController controller = (AtSerdesController)self;
    uint32 regAddr  = cLoopbackReg + PhysicalPortIdGet(controller);
    uint32 regVal  = LoopbackReadReadOnDatabase(self);
    mRegFieldSet(regVal, cLoopbackMode, enable ? 2 : 0);
    AtSerdesControllerWrite(controller, regAddr, regVal, cAtModuleSdh);
    WaitHwUpdate(10000);
    mThis(self)->loopoutIsEnabled = enable;

    /* And restore cache */
    if ((!mThis(self)->loopinIsEnabled) && (!mThis(self)->loopoutIsEnabled))
        HwParamsReConfigure(self);

    return cAtOk;
    }

static eBool LoopInIsEnabled(ThaSerdesController self)
    {
    return mThis(self)->loopinIsEnabled;
    }

static eBool LoopOutIsEnabled(ThaSerdesController self)
    {
    return mThis(self)->loopoutIsEnabled;
    }

static uint32 *RxEqualizationDcGainValidValues(uint8 *numValidValue)
    {
    static uint32 validValues[] = {0, 1, 2};
    *numValidValue = mCount(validValues);
    return validValues;
    }

static void Debug(AtSerdesController self)
    {
    uint32 regValue;

    m_AtSerdesControllerMethods->Debug(self);

    /* Parameters */
    regValue = AtSerdesControllerRead(self, cParamValueReg + PhysicalPortIdGet(self), cAtModuleSdh);
    AtPrintc(cSevNormal, "VOD                  : 0x%x\r\n", mRegField(regValue, cTxVod));
    AtPrintc(cSevNormal, "Pre-tap              : 0x%x\r\n", mRegField(regValue, cTxPreEmphasis));
    AtPrintc(cSevNormal, "RxEqualizationDcGain : 0x%x\r\n", mRegField(regValue, cRxDcGain));
    AtPrintc(cSevNormal, "RxEqualizationControl: 0x%x\r\n", mRegField(regValue, cRxEqualizer));

    /* Loopback */
    regValue = AtSerdesControllerRead(self, cLoopbackReg + PhysicalPortIdGet(self), cAtModuleSdh);
    AtPrintc(cSevNormal, "Loopback             : %u\r\n", mRegField(regValue, cLoopbackMode));
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSerdesControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, m_AtSerdesControllerMethods, sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, Init);
        mMethodOverride(m_AtSerdesControllerOverride, Debug);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void OverrideThaSerdesController(AtSerdesController self)
    {
    ThaSerdesController controller = (ThaSerdesController)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSerdesControllerOverride, mMethodsGet(controller), sizeof(m_ThaSerdesControllerOverride));

        mMethodOverride(m_ThaSerdesControllerOverride, LoopInEnable);
        mMethodOverride(m_ThaSerdesControllerOverride, LoopOutEnable);
        mMethodOverride(m_ThaSerdesControllerOverride, LoopInIsEnabled);
        mMethodOverride(m_ThaSerdesControllerOverride, LoopOutIsEnabled);
        mMethodOverride(m_ThaSerdesControllerOverride, HwPhysicalParamSet);
        mMethodOverride(m_ThaSerdesControllerOverride, HwPhysicalParamGet);
        mMethodOverride(m_ThaSerdesControllerOverride, RxEqualizationDcGainValidValues);
        }

    mMethodsSet(controller, &m_ThaSerdesControllerOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtSerdesController(self);
    OverrideThaSerdesController(self);
    }

static AtSerdesController ThaSdhLineSerdesControllerCyclone4V2ObjectInit(AtSerdesController self, AtSdhLine sdhLine, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaSdhLineSerdesControllerCyclone4ObjectInit(self, sdhLine, serdesId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController ThaSdhLineSerdesControllerCyclone4V2New(AtSdhLine sdhLine, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ThaSdhLineSerdesControllerCyclone4V2ObjectInit(newController, sdhLine, serdesId);
    }

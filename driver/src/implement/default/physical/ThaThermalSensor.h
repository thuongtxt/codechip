/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PHYSICAL
 * 
 * File        : ThaThermalSensor.h
 * 
 * Created Date: Dec 15, 2015
 *
 * Description : Default thermal sensor interface.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THATHERMALSENSOR_H_
#define _THATHERMALSENSOR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtThermalSensor.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaThermalSensor * ThaThermalSensor;

/*--------------------------- Forward declarations ---------------------------*/
AtThermalSensor ThaThermalSensorNew(AtDevice device);
AtThermalSensor Tha60290021ThermalSensorNew(AtDevice device);

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THATHERMALSENSOR_H_ */


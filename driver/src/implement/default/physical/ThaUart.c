/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAN
 *
 * File        : Tha60210011Uart.c
 *
 * Created Date: Apr 20, 2016
 *
 * Description : Tha60210011Uart implementations
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../generic/physical/AtUartInternal.h"
#include "../../../generic/man/AtDeviceInternal.h"
#include "ThaSemController.h"
#include "ThaUartReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cAtUartMaxWriteBufferLenInBytes 16

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tThaUart *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaUart
    {
    tAtUart super;
    uint8 asciiBuffer[cAtUartMaxWriteBufferLenInBytes];
    }tThaUart;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUartMethods m_AtUartOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet Init(AtUart self)
    {
    AtUnused(self);
    return cAtOk;
    }

static AtDevice Device(AtUart self)
    {
    return AtUartDeviceGet(self);
    }

static AtSemController SemController(AtUart self)
    {
    return AtDeviceSemControllerGetByIndex(AtUartDeviceGet(self), AtUartSuperLogicRegionIdGet(self));
    }

static uint32 DefaultOffset(AtUart self)
    {
    return ThaSemControllerDefaultOffset((ThaSemController)SemController(self));
    }

static AtHal HalGet(AtUart self)
    {
    return AtDeviceIpCoreHalGet(AtUartDeviceGet(self), 0);
    }

static uint32 ReadWithOffset(AtUart self, uint32 regAddr)
    {
    return AtHalRead(HalGet(self), regAddr + DefaultOffset(self));
    }

static void WriteWithOffset(AtUart self, uint32 regAddr, uint32 value)
    {
    AtHalWrite(HalGet(self), regAddr + DefaultOffset(self), value);
    }

static eAtRet TriggerWrite(AtUart self)
    {
    static const uint32 cTimeoutInMs = 5000;
    static const uint32 cRestTimeMs = 200;
    uint32 regVal;
    uint32 elapseTime = 0;
    tAtOsalCurTime curTime, prevTime;
    uint32 cycle_i = 0;

    regVal = ReadWithOffset(self, cReg_sem_mon_ctrl_Base);
    mRegFieldSet(regVal, c_sem_mon_ctrl_mon_rx_ctrl_trig_, 1);
    WriteWithOffset(self, cReg_sem_mon_ctrl_Base, regVal);

    regVal = ReadWithOffset(self, cReg_sem_mon_ctrl_Base);
    mRegFieldSet(regVal, c_sem_mon_ctrl_mon_rx_ctrl_trig_, 0);
    WriteWithOffset(self, cReg_sem_mon_ctrl_Base, regVal);

    /* Check HW is done */
    AtOsalCurTimeGet(&prevTime);
    while (elapseTime < cTimeoutInMs)
        {
        uint32 numCycles;

        regVal = ReadWithOffset(self, cReg_sem_mon_ctrl_Base);

        if (AtDeviceIsSimulated(Device(self)))
            return cAtOk;

        if (regVal & c_sem_mon_ctrl_mon_rx_ctrl_done_Mask)
            return cAtOk;

        numCycles = (elapseTime / 100);
        if (numCycles > cycle_i)
            {
            cycle_i = numCycles;
            AtOsalUSleep(cRestTimeMs * 1000);
            }

        AtOsalCurTimeGet(&curTime);
        elapseTime = mTimeIntervalInMsGet(prevTime, curTime);
        }

    return cAtErrorSemTimeOut;
    }

static eAtRet HwBufferWrite(AtUart self, uint8 *bufferInAscii, uint32 commandLen)
    {
    uint32 i;
    uint32 regVal;

    if (commandLen > cAtUartMaxWriteBufferLenInBytes)
        return cAtErrorOutOfRangParm;

    /* Put content */
    for (i = 0; i < commandLen; i += 4)
        {
        regVal =  (uint32)(bufferInAscii[i] << 24UL);
        regVal |= (uint32)(bufferInAscii[i + 1] << 16UL);
        regVal |= (uint32)(bufferInAscii[i + 2] << 8UL);
        regVal |= (uint32)(bufferInAscii[i + 3]);

        WriteWithOffset(self, cReg_sem_mon_cmd_part0_Base + (i / 4), regVal);
        }

    /* Set command length */
    regVal = ReadWithOffset(self, cReg_sem_mon_ctrl_Base);
    mRegFieldSet(regVal, c_sem_mon_ctrl_mon_rx_ctrl_len_, commandLen);
    WriteWithOffset(self, cReg_sem_mon_ctrl_Base, regVal);

    /* Ask HW start to send command to IP and trigger */
    return TriggerWrite(self);
    }

static uint8 *String2AsciiCode(AtUart self, const char *buffer, uint32 bufferSize)
    {
    uint32 i;
    uint8 *sharedBuffer = mThis(self)->asciiBuffer;

    if (bufferSize > AtStrlen(buffer))
        bufferSize = AtStrlen(buffer);

    for (i = 0; i < bufferSize; i++)
        sharedBuffer[i] = (uint8)buffer[i];

    return sharedBuffer;
    }

static uint32 ReadFifoLength(AtUart self)
    {
    uint32 currentLen, previousLen, regVal;

    /* HW recommended this way to check length is stable. Improve later */
    regVal = ReadWithOffset(self, cReg_sem_mon_sta_Base);
    previousLen = mRegField(regVal, c_sem_mon_sta_mon_tx_len_cur_);

    regVal = ReadWithOffset(self, cReg_sem_mon_sta_Base);
    currentLen = mRegField(regVal, c_sem_mon_sta_mon_tx_len_cur_);

    while (currentLen != previousLen)
        {
        regVal = ReadWithOffset(self, cReg_sem_mon_sta_Base);
        previousLen = mRegField(regVal, c_sem_mon_sta_mon_tx_len_cur_);

        regVal = ReadWithOffset(self, cReg_sem_mon_sta_Base);
        currentLen = mRegField(regVal, c_sem_mon_sta_mon_tx_len_cur_);
        }

    return currentLen;
    }

static eBool ReadFifoIsEmpty(AtUart self)
    {
    uint32 regVal = ReadWithOffset(self, cReg_sem_mon_sta_Base);

    if (AtDeviceIsSimulated(Device(self)))
        return cAtTrue;

    return (mRegField(regVal, c_sem_mon_sta_mon_tx_ept_cur_) &&
           (ReadFifoLength(self) == 0x0));
    }

static void HwFifoReset(AtUart self)
	{
    uint32 regVal;
    regVal = ReadWithOffset(self, cReg_sem_mon_sta_Base);
    mRegFieldSet(regVal, c_sem_mon_sta_mon_tx_ctrl_trig_, 0);
    WriteWithOffset(self, cReg_sem_mon_sta_Base, regVal);

    regVal = ReadWithOffset(self, cReg_sem_mon_sta_Base);
    mRegFieldSet(regVal, c_sem_mon_sta_mon_tx_ctrl_trig_, 1);
    WriteWithOffset(self, cReg_sem_mon_sta_Base, regVal);
	}

static eAtRet FifoReset(AtUart self)
    {
    static const uint32 cRestTimeMs = 200;
    const uint32 cTimeoutInMs = 1000;
    uint32 cycle_i = 0;
    uint32 elapseTime = 0;
    tAtOsalCurTime curTime, prevTime;

    /* Request Hardware Reset FIFO */
    HwFifoReset(self);

    /* Check HW is done */
    AtOsalCurTimeGet(&prevTime);
    while (elapseTime < cTimeoutInMs)
        {
        uint32 numCycles;

        if (ReadFifoIsEmpty(self))
            return cAtOk;

        numCycles = (elapseTime / 100);
        if (numCycles > cycle_i)
            {
            cycle_i = numCycles;
            AtOsalUSleep(cRestTimeMs * 1000);
            }

        /* Request Reset again */
        HwFifoReset(self);
        AtOsalCurTimeGet(&curTime);
        elapseTime = mTimeIntervalInMsGet(prevTime, curTime);
        }

    return cAtErrorSemTimeOut;
    }

static eAtRet Transmit(AtUart self, const char *buffer, uint32 bufferSize)
    {
    /* Reset FIFO to make sure Reading FIFO is empty first */
    eAtRet ret = FifoReset(self);
    if (ret != cAtOk)
        return ret;

    return HwBufferWrite(self, String2AsciiCode(self, buffer, bufferSize), bufferSize);
    }

static eBool ReadFifoIsReady(AtUart self)
    {
    uint32 regVal = ReadWithOffset(self, cReg_sem_mon_sta_Base);

    if (AtDeviceIsSimulated(Device(self)))
        return cAtTrue;

    return mRegField(regVal, c_sem_mon_sta_mon_tx_nept_cur_) ? cAtTrue : cAtFalse;
    }

static uint32 FifoRead(AtUart self, uint8 *buffer, uint32 bufferSize)
    {
    uint32 i = 0;
    uint32 bufferLen = ReadFifoLength(self);
    uint32 calculatedLen = 0;

    while (ReadFifoIsReady(self) && (calculatedLen < bufferLen))
        {
    	if (i < bufferSize) /* To avoid leak memory */
			buffer[i++] = (ReadWithOffset(self, cReg_sem_mon_val_Base) & c_sem_mon_val_mon_tx_val_Mask);
        calculatedLen++;
        }

    if (bufferLen < calculatedLen)
        bufferLen = calculatedLen;

    if (bufferLen > bufferSize)
        bufferLen = bufferSize;

    return bufferLen;
    }

static uint32 Receive(AtUart self, char *buffer, uint32 bufferSize)
    {
    return FifoRead(self, (uint8 *)buffer, bufferSize);
    }

static void BitPrint(uint32 regVal, uint32 mask, const char *message)
    {
    uint32 set = regVal & mask;
    AtPrintc(cSevNormal, "    - %s: ", message);
    AtPrintc(set ? cSevWarning : cSevInfo, "%s\r\n", set ? "Set" : "Clear");
    }

static void Debug(AtUart self)
    {
    uint32 regVal;

    AtPrintc(cSevInfo, "* Reading FIFO UART status of SLR #%u:\r\n", AtUartSuperLogicRegionIdGet(self) + 1);

    regVal = ReadWithOffset(self, cReg_sem_mon_sta_Base);
    BitPrint(regVal, c_sem_mon_sta_mon_tx_eful_stk_Mask, "Sticky Early full");
    BitPrint(regVal, c_sem_mon_sta_mon_tx_full_stk_Mask, "Sticky Full");
    BitPrint(regVal, c_sem_mon_sta_mon_tx_ept_stk_Mask,  "Sticky Empty");
    BitPrint(regVal, c_sem_mon_sta_mon_tx_nept_stk_Mask, "Sticky Not Empty");
    AtPrintc(cSevInfo, "\r\n");

    BitPrint(regVal, c_sem_mon_sta_mon_tx_eful_cur_Mask, "Current Early full");
    BitPrint(regVal, c_sem_mon_sta_mon_tx_full_cur_Mask, "Current Full");
    BitPrint(regVal, c_sem_mon_sta_mon_tx_ept_cur_Mask,  "Current Empty");
    BitPrint(regVal, c_sem_mon_sta_mon_tx_nept_cur_Mask, "Current Not Empty");

    AtPrintc(cSevInfo, "\r\n\r\n");

    /* Clear sticky: W1C */
    regVal = ReadWithOffset(self, cReg_sem_mon_sta_Base);
    mRegFieldSet(regVal, c_sem_mon_sta_mon_tx_eful_stk_, 1);
    mRegFieldSet(regVal, c_sem_mon_sta_mon_tx_full_stk_, 1);
    mRegFieldSet(regVal, c_sem_mon_sta_mon_tx_ept_stk_, 1);
    mRegFieldSet(regVal, c_sem_mon_sta_mon_tx_nept_stk_, 1);
    WriteWithOffset(self, cReg_sem_mon_sta_Base, regVal);
    }

static void ReceiveDataWait(AtUart self)
	{
	AtUnused(self);
    AtOsalUSleep(1000);
	}

static void OverrideAtUart(AtUart self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtUartOverride, mMethodsGet(self), sizeof(m_AtUartOverride));

        mMethodOverride(m_AtUartOverride, Init);
        mMethodOverride(m_AtUartOverride, Transmit);
        mMethodOverride(m_AtUartOverride, Receive);
        mMethodOverride(m_AtUartOverride, ReceiveDataWait);
        mMethodOverride(m_AtUartOverride, Debug);
        }

    mMethodsSet(self, &m_AtUartOverride);
    }

static void Override(AtUart self)
    {
    OverrideAtUart(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaUart);
    }

static AtUart ObjectInit(AtUart self, AtDevice device, uint8 slrId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtUartObjectInit(self, device, slrId) == NULL)
        return NULL;

    /* Setup methods */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtUart ThaUartNew(AtDevice device, uint8 slrId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtUart controller = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (controller == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(controller, device, slrId);
    }


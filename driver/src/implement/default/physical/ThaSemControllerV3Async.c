/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : ThaSemControllerAsync.c
 *
 * Created Date: Feb 15, 2017
 *
 * Description : Implement async operation for SEM.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../util/coder/AtCoderUtil.h"
#include "../../../generic/man/AtDeviceAsyncInit.h"
#include "../man/ThaDeviceInternal.h"
#include "ThaSemControllerV3Internal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)                 ((ThaSemController)self)
#define mLfa(self)                  (mThis(self)->lfa)
#define mDwIdxInLfa(self)           (mThis(self)->dwIdxInLfa)
#define mBitIdxInDwOfLfa(self)      (mThis(self)->bitIdxInDwOfLfa)
#define mCmdString64bytes(self)     (mThis(self)->cmdString64bytes)

/*--------------------------- Local typedefs ---------------------------------*/
typedef enum eThaSemControllerValidateByUartAsyncStates
    {
    cThaSemControllerValidateAsyncStateStart = 0,
    cThaSemControllerValidateAsyncStateRetry
    }eThaSemControllerValidateByUartAsyncStates;

typedef enum eThaSemControllerAsyncRetryStates
    {
    cThaSemControllerAsyncRetryState_SemIdle0,
    cThaSemControllerAsyncRetryState_AddressGenerate,
    cThaSemControllerAsyncRetryState_ErrorInject,
    cThaSemControllerAsyncRetryState_SemIdle1,
    cThaSemControllerAsyncRetryState_SemObservation,
    cThaSemControllerAsyncRetryState_ValidateResult
    } eThaSemControllerAsyncRetryStates;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void ValidatedAsyncStateSet(ThaSemController self, uint32 state)
    {
    self->validatedAsyncState = state;
    }

static void ValidatedAsyncStateReset(ThaSemController self)
    {
    self->validatedAsyncState = cThaSemControllerValidateAsyncStateStart;
    }

static uint32 ValidatedAsyncStateGet(ThaSemController self)
    {
    return self->validatedAsyncState;
    }

static void ValidatedAsyncRetryIncrease(ThaSemController self)
    {
    self->validatedRetries++;
    }

static void ValidatedAsyncRetryReset(ThaSemController self)
    {
    self->validatedRetries = 0;
    }

static uint32 ValidatedAsyncRetryGet(ThaSemController self)
    {
    return self->validatedRetries;
    }

static void ValidatedAsyncRetrySubStateSet(ThaSemController self, uint32 state)
    {
    self->validatedRetrySubState = state;
    }

static void ValidatedAsyncRetrySubStateReset(ThaSemController self)
    {
    self->validatedRetrySubState = cThaSemControllerAsyncRetryState_SemIdle0;
    }

static uint32 ValidatedAsyncRetrySubStateGet(ThaSemController self)
    {
    return self->validatedRetrySubState;
    }

static AtDevice DeviceGet(AtSemController self)
    {
    return AtSemControllerDeviceGet(self);
    }

static eAtRet UartAsyncPollingValidateResult(AtSemController self, uint32 previousCorrectedCounter, uint32 errorType)
    {
    tAtOsalCurTime curTime;
    uint32 elapse = 0;

    /* Calculate elapse time */
    AtOsalCurTimeGet(&curTime);
    elapse = mTimeIntervalInMsGet(mThis(self)->validatedCounterStartTime, curTime);
    if (elapse < cUartTimeoutInMs)
        {
        if (AtSemControllerCounterGet(self, errorType) >= (previousCorrectedCounter + 1))
            return cAtOk;
        else
            return cAtErrorAgain;
        }

    return cAtErrorSemGotZeroErrorCounter;
    }

static eAtRet AsyncValidateRetrySubStateProcess(AtSemController self, uint32 errorType)
    {
    eAtRet ret = cAtErrorDevFail;
    uint32 state = ValidatedAsyncRetrySubStateGet(mThis(self));
    char stateString[32];
    tAtOsalCurTime prevTime;

    AtOsalCurTimeGet(&prevTime);

    AtSprintf(stateString, "state: %d\r\n", state);

    switch(state)
        {
        case cThaSemControllerAsyncRetryState_SemIdle0:
            /* Move to Idle state */
            ret = ThaSemControllerMoveToStateByUart(self, cAtSemControllerAlarmIdle);
            AtDeviceAccessTimeInMsSinceLastProfileCheck(DeviceGet(self), cAtTrue, &prevTime, AtSourceLocation, "ThaSemControllerAsyncMoveToState");

            if ((ret != cAtOk) && (!AtDeviceAsyncRetValIsInState(ret)))
                {
                mDriverErrorLog(ret, "MoveToState cAtSemControllerAlarmIdle");
                ValidatedAsyncRetryReset(mThis(self));
                }
            else if (ret == cAtOk)
                {
                ValidatedAsyncRetrySubStateSet(mThis(self), cThaSemControllerAsyncRetryState_AddressGenerate);
                ret = cAtErrorAgain;
                }
            break;

        case cThaSemControllerAsyncRetryState_AddressGenerate:
            if (ThaSemControllerAsyncAddressGenerate(self, cAtTrue, &mLfa(self), &mDwIdxInLfa(self), &mBitIdxInDwOfLfa(self)) != cAtTrue)
                {
                ValidatedAsyncRetrySubStateReset(mThis(self));
                ret = cAtErrorSemLfaAddressGenerate;
                }
            else
                {
                ValidatedAsyncRetrySubStateSet(mThis(self), cThaSemControllerAsyncRetryState_ErrorInject);
                ret = cAtErrorAgain;
                }
            AtDeviceAccessTimeInMsSinceLastProfileCheck(DeviceGet(self), cAtTrue, &prevTime, AtSourceLocation, "ThaSemControllerAsyncAddressGenerate");

            break;

        case cThaSemControllerAsyncRetryState_ErrorInject:
            /* Force error and wait move back to Idle state */
            ret = ThaSemControllerAsyncLfaErrorInject(self,  mLfa(self), mDwIdxInLfa(self), mBitIdxInDwOfLfa(self), mCmdString64bytes(self));
            AtDeviceAccessTimeInMsSinceLastProfileCheck(DeviceGet(self), cAtTrue, &prevTime, AtSourceLocation, "ThaSemControllerAsyncLfaErrorInject");

            if ((ret != cAtOk) && (!AtDeviceAsyncRetValIsInState(ret)))
                {
                ValidatedAsyncRetrySubStateReset(mThis(self));
                ret = cAtErrorSemLfaAddressGenerate;
                }
            else if (ret == cAtOk)
                {
                ValidatedAsyncRetrySubStateSet(mThis(self), cThaSemControllerAsyncRetryState_SemIdle1);
                ret = cAtErrorAgain;
                }
            break;

        case cThaSemControllerAsyncRetryState_SemIdle1:
            ret = ThaSemControllerAsyncWaitMoveToState(self, cAtSemControllerAlarmIdle);
            AtDeviceAccessTimeInMsSinceLastProfileCheck(DeviceGet(self), cAtTrue, &prevTime, AtSourceLocation, "ThaSemControllerAsyncWaitMoveToState");

            if ((ret != cAtOk) && (!AtDeviceAsyncRetValIsInState(ret)))
                {
                mDriverErrorLog(ret, "WaitMoveToState cAtSemControllerAlarmIdle");
                ValidatedAsyncRetrySubStateReset(mThis(self));
                }

            else if (ret == cAtOk)
                {
                ValidatedAsyncRetrySubStateSet(mThis(self), cThaSemControllerAsyncRetryState_SemObservation);
                ret = cAtErrorAgain;
                }
            break;

        case cThaSemControllerAsyncRetryState_SemObservation:
            ret = ThaSemControllerMoveToStateByUart(self, cAtSemControllerAlarmObservation);
            AtDeviceAccessTimeInMsSinceLastProfileCheck(DeviceGet(self), cAtTrue, &prevTime, AtSourceLocation, "ThaSemControllerAsyncMoveToState");

            if ((ret != cAtOk) && (!AtDeviceAsyncRetValIsInState(ret)))
                {
                mDriverErrorLog(ret, "MoveToState cAtSemControllerAlarmObservation");
                ValidatedAsyncRetrySubStateReset(mThis(self));
                }
            else if (ret == cAtOk)
                {
                ValidatedAsyncRetrySubStateSet(mThis(self), cThaSemControllerAsyncRetryState_ValidateResult);
                /* Start time for validating counter */
                AtOsalCurTimeGet(&mThis(self)->validatedCounterStartTime);
                ret = cAtErrorAgain;
                }
            break;

        case cThaSemControllerAsyncRetryState_ValidateResult:
            ret = UartAsyncPollingValidateResult(self, 0, errorType);
            AtDeviceAccessTimeInMsSinceLastProfileCheck(DeviceGet(self), cAtTrue, &prevTime, AtSourceLocation, "UartAsyncPollingValidateResult");

            if ((ret != cAtOk) && (!AtDeviceAsyncRetValIsInState(ret)))
                {
                AtDriverLog(AtDriverSharedDriverGet(), cAtLogLevelWarning, "%s %s\r\n", "error injection failed at: ", mCmdString64bytes(self));
                ValidatedAsyncRetrySubStateReset(mThis(self));
                }

            else if (ret == cAtOk)
                {
                AtDriverLog(AtDriverSharedDriverGet(), cAtLogLevelWarning, "%s %s\r\n", "error injection Ok at: ", mCmdString64bytes(self));
                ValidatedAsyncRetrySubStateReset(mThis(self));
                }
            break;
        default:
            ValidatedAsyncRetrySubStateReset(mThis(self));
            ret = cAtErrorDevFail;
        }

    return ret;
    }

static eAtRet AsyncValidatedRetry(AtSemController self, uint32 errorType)
    {
    uint32 cMaxRetry = 1000;
    eAtRet ret = cAtErrorAgain;

    if (ValidatedAsyncRetryGet(mThis(self)) < cMaxRetry)
        {
        ret = AsyncValidateRetrySubStateProcess(self, errorType);
        if (ret == cAtErrorSemLfaAddressGenerate || ret == cAtErrorSemGotZeroErrorCounter)
            {
            ValidatedAsyncRetryIncrease(mThis(self));
            ret = cAtErrorAgain;
            }
        else if (!AtDeviceAsyncRetValIsInState(ret))
            ValidatedAsyncRetryReset(mThis(self));

        return ret;
        }

    ValidatedAsyncRetryReset(mThis(self));
    return cAtErrorSemValidateFail;
    }

eAtRet ThaSemControllerAsyncValidateByUartWithErrorType(AtSemController self, uint32 errorType)
    {
    uint32 state = ValidatedAsyncStateGet(mThis(self));
    eAtRet ret = cAtErrorAgain;
    char stateString[32];
    tAtOsalCurTime prevTime;

    AtOsalCurTimeGet(&prevTime);

    AtSprintf(stateString, "state: %d\r\n", state);
    switch (state)
        {
        case cThaSemControllerValidateAsyncStateStart:
            AtSemControllerCounterClear(self, errorType);
            AtDeviceAccessTimeInMsSinceLastProfileCheck(DeviceGet(self), cAtTrue, &prevTime, AtSourceLocation, "AtSemControllerCounterClear");
            ValidatedAsyncStateSet(mThis(self), cThaSemControllerValidateAsyncStateRetry);
            ret = cAtErrorAgain;
            break;
        case cThaSemControllerValidateAsyncStateRetry:
            ret = AsyncValidatedRetry(self, errorType);
            AtDeviceAccessTimeInMsSinceLastProfileCheck(DeviceGet(self), cAtTrue, &prevTime, AtSourceLocation, "AsyncValidatedRetry");
            if (!AtDeviceAsyncRetValIsInState(ret))
                ValidatedAsyncStateReset(mThis(self));
            break;
        default:
            ValidatedAsyncStateReset(mThis(self));
            ret = cAtErrorDevFail;
        }

    return ret;
    }

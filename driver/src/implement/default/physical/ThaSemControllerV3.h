/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : ThaSemControllerAsync.h
 * 
 * Created Date: Feb 15, 2017
 *
 * Description : SEM controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASEMCONTROLLERASYNC_H_
#define _THASEMCONTROLLERASYNC_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSemController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cUartTimeoutInMs 3000

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet ThaSemControllerAsyncValidateByUartWithErrorType(AtSemController self, uint32 errorType);
eAtRet ThaSemControllerAsyncLfaErrorInject(AtSemController self, uint32 lfa, uint32 dwIdx, uint32 bitIdx, char *cmdString64bytes);
eBool ThaSemControllerAsyncAddressGenerate(AtSemController self, eBool isBasic, uint32 *lfa, uint32 *dwIdx, uint32 *bitIdx);
eAtRet ThaSemControllerMoveToStateByUart(AtSemController self, eAtSemControllerAlarmType state);
eAtRet ThaSemControllerAsyncWaitMoveToState(AtSemController self, eAtSemControllerAlarmType state);
eAtRet ThaSemControllerAsyncMoveToStateByUart(AtSemController self, eAtSemControllerAlarmType state);

#ifdef __cplusplus
}
#endif
#endif /* _THASEMCONTROLLERASYNC_H_ */


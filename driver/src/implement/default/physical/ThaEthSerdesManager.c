/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : ThaEthSerdesManager.c
 *
 * Created Date: Jul 18, 2015
 *
 * Description : SERDES manager
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../generic/physical/AtSerdesControllerInternal.h"
#include "../../../util/coder/AtCoderUtil.h"
#include "ThaEthSerdesManagerInternal.h"
#include "../eth/ThaModuleEthInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaEthSerdesManager)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaEthSerdesManagerMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void ControllersDelete(AtSerdesController *controllers, uint32 numControllers)
    {
    uint32 controller_i;

    for (controller_i = 0; controller_i < numControllers; controller_i++)
        AtObjectDelete((AtObject)controllers[controller_i]);

    AtOsalMemFree(controllers);
    }

static uint32 NumStandbyPorts(ThaEthSerdesManager self)
    {
    return AtModuleEthMaxPortsGet(self->ethModule);
    }

static AtSerdesController SerdesControllerCreate(ThaEthSerdesManager self, AtEthPort port, uint32 serdesId)
    {
    AtUnused(self);
    AtUnused(port);
    AtUnused(serdesId);
    return NULL;
    }

static AtSerdesController *SerdesControllersCreate(ThaEthSerdesManager self,
                                                   AtSerdesController (*SerdesControllerCreateFunc)(ThaEthSerdesManager, AtEthPort, uint32))
    {
    uint32 controller_i, numControllers = ThaEthSerdesManagerNumSerdesController(self);
    uint32 memorySize = numControllers * sizeof(AtSerdesController);
    AtSerdesController *controllers = AtOsalMemAlloc(memorySize);

    if (controllers == NULL)
        return NULL;
    AtOsalMemInit(controllers, 0, memorySize);

    self->numCreatedController = 0;
    for (controller_i = 0; controller_i < numControllers; controller_i++)
        {
        uint32 portId = mMethodsGet((ThaModuleEth)(self->ethModule))->SerdesIdToEthPortId((ThaModuleEth)(self->ethModule), controller_i);
        AtEthPort port = AtModuleEthPortGet(self->ethModule, (uint8)(portId));

        /* Create */
        controllers[controller_i] = SerdesControllerCreateFunc(self, port, controller_i);
        if (controllers[controller_i])
            continue;

        /* Need to cleanup on failure */
        ControllersDelete(controllers, controller_i);
        return NULL;
        }

    self->numCreatedController = numControllers;
    return controllers;
    }

static eAtRet Setup(ThaEthSerdesManager self)
    {
    if (self->serdesControllers == NULL)
        self->serdesControllers = ThaEthSerdesManagerSerdesControllersCreate(self, mMethodsGet(self)->SerdesControllerCreate);

    if (self->serdesControllers == NULL)
        return cAtErrorRsrcNoAvail;

    return cAtOk;
    }

static eBool ShouldEnableSerdes(ThaEthSerdesManager self, uint32 serdesId)
    {
    AtUnused(self);
    AtUnused(serdesId);
    return cAtFalse;
    }

static eBool ShouldInitSerdes(ThaEthSerdesManager self, uint32 serdesId)
    {
    /* Let concrete determine if SERDES need to be touched */
    AtUnused(self);
    AtUnused(serdesId);
    return cAtFalse;
    }

static eAtRet SerdesParamSet(AtSerdesController serdes, eAtSerdesParam param, uint32 value)
    {
    if (AtSerdesControllerPhysicalParamIsSupported(serdes, param))
        return AtSerdesControllerPhysicalParamSet(serdes, param, value);
    return cAtOk;
    }

static eAtRet SerdesDefaultParamSet(AtSerdesController serdes)
    {
    eAtRet ret;

    ret = SerdesParamSet(serdes, cAtSerdesParamTxDiffCtrl, 0xF);
    if (ret != cAtOk)
        return ret;

    ret = SerdesParamSet(serdes, cAtSerdesParamTxPostCursor, 0x14);
    if (ret != cAtOk)
        return ret;

    return cAtOk;
    }

static eBool SerdesIsUsed(ThaEthSerdesManager self, AtSerdesController serdesController)
    {
    AtUnused(self);
    AtUnused(serdesController);
    return cAtTrue;
    }

static eAtRet Init(ThaEthSerdesManager self)
    {
    uint32 numSerdes = ThaEthSerdesManagerNumSerdesController(self);
    uint32 serdes_i;
    eAtRet ret = cAtOk;

    for (serdes_i = 0; serdes_i < numSerdes; serdes_i++)
        {
        AtSerdesController serdes = mThis(self)->serdesControllers[serdes_i];
        eBool enabled = mMethodsGet(self)->ShouldEnableSerdes(self, serdes_i);

        if (!ThaEthSerdesManagerSerdesIsUsed(self, serdes))
            continue;

        if (mMethodsGet(self)->ShouldInitSerdes(self, serdes_i))
            ret |= AtSerdesControllerInit(serdes);

        if (AtSerdesControllerCanEnable(serdes, enabled))
            ret |= AtSerdesControllerEnable(serdes, enabled);

        ret |= SerdesDefaultParamSet(serdes);
        }

    return ret;
    }

static void Delete(AtObject self)
    {
    ControllersDelete(mThis(self)->serdesControllers, mThis(self)->numCreatedController);
    mThis(self)->serdesControllers = NULL;
    mThis(self)->numCreatedController = 0;
    m_AtObjectMethods->Delete(self);
    }

static AtSerdesController SerdesController(ThaEthSerdesManager self, uint32 serdesId)
    {
    uint32 minNumSerdes = ThaEthSerdesManagerNumberOfCreatedSerdesControllerGet(self);
    if (serdesId < minNumSerdes)
        return mThis(self)->serdesControllers[serdesId];
    return NULL;
    }

static uint32 NumSerdesController(ThaEthSerdesManager self)
    {
    return AtModuleEthMaxPortsGet(self->ethModule) + NumStandbyPorts(self);
    }

static eBool CanControlEqualizer(ThaEthSerdesManager self, AtSerdesController serdes)
    {
    AtUnused(self);
    AtUnused(serdes);
    return cAtFalse;
    }

static eAtRet AllSerdesReset(ThaEthSerdesManager self)
    {
    uint32 numSerdes = ThaEthSerdesManagerNumSerdesController(self);
    uint32 serdes_i;
    eAtRet ret = cAtOk;

    for (serdes_i = 0; serdes_i < numSerdes; serdes_i++)
        {
        AtSerdesController serdes = ThaEthSerdesManagerSerdesController(self, serdes_i);
        eAtRet serdesRet = cAtOk;

        if (!ThaEthSerdesManagerSerdesIsUsed(self, serdes))
            continue;

        serdesRet = AtSerdesControllerReset(serdes);
        if (serdesRet != cAtOk)
            AtDriverLog(AtDriverSharedDriverGet(), cAtLogLevelWarning, "Reset SERDES %d fail with ret = %s\r\n", serdes_i + 1, AtRet2String(serdesRet));

        ret |= serdesRet;
        }

    return ret;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaEthSerdesManager object = (ThaEthSerdesManager)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(ethModule);
    mEncodeUInt(numCreatedController);
    mEncodeObjectDescriptionArray(serdesControllers, object->numCreatedController);
    }

static eBool HasXfiGroups(ThaEthSerdesManager self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 NumXfiGroups(ThaEthSerdesManager self)
    {
    /* Concrete must know */
    AtUnused(self);
    return 0;
    }

static const uint32 *XfisForGroup(ThaEthSerdesManager self, uint32 groupId, uint32 *numPorts)
    {
    AtUnused(self);
    AtUnused(groupId);

    if (numPorts)
        *numPorts = 0;

    return NULL;
    }

static void MethodsInit(ThaEthSerdesManager self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, Setup);
        mMethodOverride(m_methods, Init);
        mMethodOverride(m_methods, SerdesControllerCreate);
        mMethodOverride(m_methods, SerdesController);
        mMethodOverride(m_methods, NumSerdesController);
        mMethodOverride(m_methods, ShouldEnableSerdes);
        mMethodOverride(m_methods, ShouldInitSerdes);
        mMethodOverride(m_methods, CanControlEqualizer);
        mMethodOverride(m_methods, SerdesIsUsed);
        mMethodOverride(m_methods, HasXfiGroups);
        mMethodOverride(m_methods, NumXfiGroups);
        mMethodOverride(m_methods, XfisForGroup);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtObject(ThaEthSerdesManager self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(ThaEthSerdesManager self)
    {
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaEthSerdesManager);
    }

ThaEthSerdesManager ThaEthSerdesManagerObjectInit(ThaEthSerdesManager self, AtModuleEth ethModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Private data */
    self->ethModule = ethModule;

    return self;
    }

eAtRet ThaEthSerdesManagerSetup(ThaEthSerdesManager self)
    {
    if (self)
        return mMethodsGet(self)->Setup(self);
    return cAtErrorNullPointer;
    }

eAtRet ThaEthSerdesManagerInit(ThaEthSerdesManager self)
    {
    if (self)
        return mMethodsGet(self)->Init(self);
    return cAtErrorNullPointer;
    }

uint32 ThaEthSerdesManagerNumSerdesController(ThaEthSerdesManager self)
    {
    if (self)
        return mMethodsGet(self)->NumSerdesController(self);
    return 0;
    }

AtSerdesController ThaEthSerdesManagerSerdesController(ThaEthSerdesManager self, uint32 serdesId)
    {
    if (self)
        return mMethodsGet(self)->SerdesController(self, serdesId);
    return NULL;
    }

AtSerdesController *ThaEthSerdesManagerSerdesControllersCreate(ThaEthSerdesManager self,
                                                   AtSerdesController (*SerdesControllerCreateFunc)(ThaEthSerdesManager, AtEthPort, uint32))
    {
    if (self)
        return SerdesControllersCreate(self, SerdesControllerCreateFunc);
    return NULL;
    }

AtSerdesController ThaEthSerdesManagerControllerGet(ThaEthSerdesManager self, uint32 serdesId)
    {
    if (self)
        return mThis(self)->serdesControllers[serdesId];
    return NULL;
    }

eBool ThaEthSerdesManagerCanControlEqualizer(ThaEthSerdesManager self, AtSerdesController serdes)
    {
    if (self)
        return mMethodsGet(self)->CanControlEqualizer(self, serdes);
    return cAtFalse;
    }

eAtRet ThaEthSerdesManagerAllSerdesReset(ThaEthSerdesManager self)
    {
    if (self)
        return AllSerdesReset(self);
    return cAtErrorNullPointer;
    }

eBool ThaEthSerdesManagerHasXfiGroups(ThaEthSerdesManager self)
    {
    if (self)
        return mMethodsGet(self)->HasXfiGroups(self);
    return cAtFalse;
    }

uint32 ThaEthSerdesManagerNumXfiGroups(ThaEthSerdesManager self)
    {
    if (self)
        return mMethodsGet(self)->NumXfiGroups(self);
    return 0;
    }

AtModuleEth ThaEthSerdesManagerModuleGet(ThaEthSerdesManager self)
    {
    return self ? self->ethModule : NULL;
    }

eBool ThaEthSerdesManagerSerdesIsUsed(ThaEthSerdesManager self, AtSerdesController serdesController)
    {
    if (self)
        return mMethodsGet(self)->SerdesIsUsed(self, serdesController);
    return cAtFalse;
    }

const uint32 *ThaEthSerdesManagerXfisForGroup(ThaEthSerdesManager self, uint32 groupId, uint32 *numPorts)
    {
    if (self)
        return mMethodsGet(self)->XfisForGroup(self, groupId, numPorts);
    return NULL;
    }

uint32 ThaEthSerdesManagerNumberOfCreatedSerdesControllerGet(ThaEthSerdesManager self)
    {
    if (self)
        return self->numCreatedController;

    return 0;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : ThaSemControllerV3.c
 *
 * Created Date: Aug 22, 2016
 *
 * Description : SEM controller version 3 with changes in interrupt, alarms, counters and UART
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtOsal.h"
#include "../../../generic/man/AtDriverInternal.h"
#include "../../../generic/man/AtDeviceInternal.h"
#include "../../../util/coder/AtCoderUtil.h"
#include "../man/ThaDevice.h"
#include "ThaSemControllerV3Internal.h"

/*--------------------------- Define -----------------------------------------*/
/* Current alarm mask/shift */
#define cTha_sem_cur_heartbeat_Mask    cBit12
#define cTha_sem_cur_heartbeat_Shift   12
#define cTha_sem_cur_uncor_Mask        cBit11
#define cTha_sem_cur_uncor_Shift       11
#define cTha_sem_cur_correctable_Mask  cBit10
#define cTha_sem_cur_correctable_Shift 10
#define cTha_sem_cur_essen_Mask    cBit8
#define cTha_sem_cur_essen_Shift   8
#define cTha_sem_cur_act_Mask      cBit7
#define cTha_sem_cur_act_Shift       7
#define cTha_sem_cur_idle_Mask     cBit6
#define cTha_sem_cur_idle_Shift      6
#define cTha_sem_cur_init_Mask     cBit5
#define cTha_sem_cur_init_Shift      5
#define cTha_sem_cur_injec_Mask    cBit4
#define cTha_sem_cur_injec_Shift     4
#define cTha_sem_cur_class_Mask    cBit3
#define cTha_sem_cur_class_Shift     3
#define cTha_sem_cur_fata_Mask     cBit2
#define cTha_sem_cur_fata_Shift      2
#define cTha_sem_cur_obsv_Mask     cBit1
#define cTha_sem_cur_obsv_Shift      1
#define cTha_sem_cur_corr_Mask     cBit0
#define cTha_sem_cur_corr_Shift      0

#define cSeuPciCorrectableMask     cBit10

#define cAsyncStop         0
#define cAsyncStartWaiting 1
#define cAsyncWait         2

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tThaSemControllerV3*)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods         m_AtObjectOverride;
static tThaSemControllerMethods m_ThaSemControllerOverride;
static tAtSemControllerMethods  m_AtSemControllerOverride;

/* Super implementations */
static const tAtObjectMethods         *m_AtObjectMethods        = NULL;
static const tAtSemControllerMethods  *m_AtSemControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
mDefineMaskShift(ThaSemController, _sem_cur_essen_)
mDefineMaskShift(ThaSemController, _sem_cur_heartbeat_)
mDefineMaskShift(ThaSemController, _sem_cur_correctable_)
mDefineMaskShift(ThaSemController, _sem_cur_uncor_)
mDefineMaskShift(ThaSemController, _sem_cur_act_)
mDefineMaskShift(ThaSemController, _sem_cur_idle_)
mDefineMaskShift(ThaSemController, _sem_cur_init_)
mDefineMaskShift(ThaSemController, _sem_cur_injec_)
mDefineMaskShift(ThaSemController, _sem_cur_class_)
mDefineMaskShift(ThaSemController, _sem_cur_fata_)
mDefineMaskShift(ThaSemController, _sem_cur_obsv_)
mDefineMaskShift(ThaSemController, _sem_cur_corr_)

static uint32 SeuStatusRegister(AtSemController self)
    {
    return ThaSemControllerCurrentAlarmRegister((ThaSemController)self) + mDefaultOffset(self);
    }

static uint32 SeuIntrStatusRegister(AtSemController self)
    {
    return ThaSemControllerHistoryAlarmRegister((ThaSemController)self) + mDefaultOffset(self);
    }

static AtHal HalGet(AtSemController self)
    {
    return AtDeviceIpCoreHalGet(AtSemControllerDeviceGet(self), 0);
    }

static uint32 ReadWithOffset(AtSemController self, uint32 regAddr)
    {
    return AtHalRead(HalGet(self), regAddr + ThaSemControllerDefaultOffset((ThaSemController)self));
    }

static uint32 AlarmGet(AtSemController self)
    {
    uint32 alarmMask = 0;
    uint32 regVal = ReadWithOffset(self, ThaSemControllerCurrentAlarmRegister((ThaSemController)self));

    if (regVal & mFieldMask((ThaSemController)self, _sem_cur_act_))
        alarmMask |= cAtSemControllerAlarmActive;

    if (regVal & mFieldMask((ThaSemController)self, _sem_cur_idle_))
        alarmMask |= cAtSemControllerAlarmIdle;

    if (regVal & mFieldMask((ThaSemController)self, _sem_cur_init_))
        alarmMask |= cAtSemControllerAlarmInitialization;

    if (regVal & mFieldMask((ThaSemController)self, _sem_cur_injec_))
        alarmMask |= cAtSemControllerAlarmInjection;

   if (regVal & mFieldMask((ThaSemController)self, _sem_cur_class_))
       alarmMask |= cAtSemControllerAlarmClassification;

   if (regVal & mFieldMask((ThaSemController)self, _sem_cur_fata_))
       alarmMask |= cAtSemControllerAlarmFatalError;

   if (regVal & mFieldMask((ThaSemController)self, _sem_cur_obsv_))
       alarmMask |= cAtSemControllerAlarmObservation;

   if (regVal & mFieldMask((ThaSemController)self, _sem_cur_corr_))
       alarmMask |= cAtSemControllerAlarmCorrection;

   if (regVal & mFieldMask((ThaSemController)self, _sem_cur_act1_))
       alarmMask |= cAtSemControllerAlarmActiveSlr1;

   if (regVal & mFieldMask((ThaSemController)self, _sem_cur_act2_))
       alarmMask |= cAtSemControllerAlarmActiveSlr2;

    return alarmMask;
    }

static eAtSemControllerAlarm AlarmRead2Clear(AtSemController self, eBool read2Clear)
    {
    uint32 alarmMask = cAtSemControllerAlarmNone;
    uint32 regVal = ReadWithOffset(self, ThaSemControllerHistoryAlarmRegister((ThaSemController)self));

    if (regVal & mFieldMask((ThaSemController)self, _sem_cur_heartbeat_))
        alarmMask |= cAtSemControllerAlarmHeartbeat;

    if (regVal & mFieldMask((ThaSemController)self, _sem_cur_uncor_))
        alarmMask |= cAtSemControllerAlarmErrorNoncorrectable;

    if (regVal & mFieldMask((ThaSemController)self, _sem_cur_correctable_))
        alarmMask |= cAtSemControllerAlarmErrorCorrectable;

    if (regVal & mFieldMask((ThaSemController)self, _sem_cur_essen_))
        alarmMask |= cAtSemControllerAlarmErrorEssential;

    if (regVal & mFieldMask((ThaSemController)self, _sem_cur_act_))
        alarmMask |= cAtSemControllerAlarmActive;

    if (regVal & mFieldMask((ThaSemController)self, _sem_cur_idle_))
        alarmMask |= cAtSemControllerAlarmIdle;

    if (regVal & mFieldMask((ThaSemController)self, _sem_cur_init_))
        alarmMask |= cAtSemControllerAlarmInitialization;

    if (regVal & mFieldMask((ThaSemController)self, _sem_cur_injec_))
        alarmMask |= cAtSemControllerAlarmInjection;

    if (regVal & mFieldMask((ThaSemController)self, _sem_cur_class_))
       alarmMask |= cAtSemControllerAlarmClassification;

    if (regVal & mFieldMask((ThaSemController)self, _sem_cur_fata_))
       alarmMask |= cAtSemControllerAlarmFatalError;

    if (regVal & mFieldMask((ThaSemController)self, _sem_cur_obsv_))
       alarmMask |= cAtSemControllerAlarmObservation;

    if (regVal & mFieldMask((ThaSemController)self, _sem_cur_corr_))
       alarmMask |= cAtSemControllerAlarmCorrection;

    if (regVal & mFieldMask((ThaSemController)self, _sem_cur_heartbeat1_))
        alarmMask |= cAtSemControllerAlarmHeartbeatSlr1;

    if (regVal & mFieldMask((ThaSemController)self, _sem_cur_heartbeat2_))
        alarmMask |= cAtSemControllerAlarmHeartbeatSlr2;

    if (regVal & mFieldMask((ThaSemController)self, _sem_cur_act1_))
        alarmMask |= cAtSemControllerAlarmActiveSlr1;

    if (regVal & mFieldMask((ThaSemController)self, _sem_cur_act2_))
        alarmMask |= cAtSemControllerAlarmActiveSlr2;

    if (read2Clear)
        ThaSemControllerClearAlarm((ThaSemController)self, regVal);

    if (AtDeviceIsSimulated(AtSemControllerDeviceGet(self)))
        return cAtSemControllerAlarmNone;

    return alarmMask;
    }

static uint32 AlarmHistoryGet(AtSemController self)
    {
    return AlarmRead2Clear(self, cAtFalse);
    }

static uint32 AlarmHistoryClear(AtSemController self)
    {
    return AlarmRead2Clear(self, cAtTrue);
    }

static uint32 SeuIntrEnableRegister(AtSemController self)
    {
    return 0x4 + mDefaultOffset(self);
    }

static eAtRet InterruptMaskSet(AtSemController self, uint32 alarmMask, uint32 enableMask)
    {
    AtHal hal = HalGet(self);
    uint32 regVal, mask = 0;
    uint32 regAddr = SeuIntrEnableRegister(self);

    regVal = AtHalRead(hal, regAddr);

    if (alarmMask & cAtSemControllerAlarmHeartbeat)
        {
        mask = mFieldMask((ThaSemController)self, _sem_cur_heartbeat_);
        regVal = (enableMask & cAtSemControllerAlarmHeartbeat) ? (regVal | mask) : (regVal & (uint32)~mask);
        }

    if (alarmMask & cAtSemControllerAlarmErrorNoncorrectable)
        {
        mask = mFieldMask((ThaSemController)self, _sem_cur_uncor_);
        regVal = (enableMask & cAtSemControllerAlarmErrorNoncorrectable) ? (regVal | mask) : (regVal & (uint32)~mask);
        }

    if (alarmMask & cAtSemControllerAlarmErrorCorrectable)
        {
        mask = mFieldMask((ThaSemController)self, _sem_cur_correctable_);
        regVal = (enableMask & cAtSemControllerAlarmErrorCorrectable) ? (regVal | mask) : (regVal & (uint32)~mask);
        }

    if (alarmMask & cAtSemControllerAlarmErrorEssential)
        {
        mask = mFieldMask((ThaSemController)self, _sem_cur_essen_);
        regVal = (enableMask & cAtSemControllerAlarmErrorEssential) ? (regVal | mask) : (regVal & (uint32)~mask);
        }

    if (alarmMask & cAtSemControllerAlarmActive)
        {
        mask = mFieldMask((ThaSemController)self, _sem_cur_act_);
        regVal = (enableMask & cAtSemControllerAlarmActive) ? (regVal | mask) : (regVal & (uint32)~mask);
        }

    if (alarmMask & cAtSemControllerAlarmIdle)
        {
        mask = mFieldMask((ThaSemController)self, _sem_cur_idle_);
        regVal = (enableMask & cAtSemControllerAlarmIdle) ? (regVal | mask) : (regVal & (uint32)~mask);
        }

    if (alarmMask & cAtSemControllerAlarmInjection)
        {
        mask = mFieldMask((ThaSemController)self, _sem_cur_injec_);
        regVal = (enableMask & cAtSemControllerAlarmInjection) ? (regVal | mask) : (regVal & (uint32)~mask);
        }

    if (alarmMask & cAtSemControllerAlarmClassification)
        {
        mask = mFieldMask((ThaSemController)self, _sem_cur_class_);
        regVal = (enableMask & cAtSemControllerAlarmClassification) ? (regVal | mask) : (regVal & (uint32)~mask);
        }

    if (alarmMask & cAtSemControllerAlarmFatalError)
        {
        mask = mFieldMask((ThaSemController)self, _sem_cur_fata_);
        regVal = (enableMask & cAtSemControllerAlarmFatalError) ? (regVal | mask) : (regVal & (uint32)~mask);
        }

    if (alarmMask & cAtSemControllerAlarmObservation)
        {
        mask = mFieldMask((ThaSemController)self, _sem_cur_obsv_);
        regVal = (enableMask & cAtSemControllerAlarmObservation) ? (regVal | mask) : (regVal & (uint32)~mask);
        }

    if (alarmMask & cAtSemControllerAlarmCorrection)
        {
        mask = mFieldMask((ThaSemController)self, _sem_cur_corr_);
        regVal = (enableMask & cAtSemControllerAlarmCorrection) ? (regVal | mask) : (regVal & (uint32)~mask);
        }

    if (alarmMask & cAtSemControllerAlarmInitialization)
        {
        mask = mFieldMask((ThaSemController)self, _sem_cur_init_);
        regVal = (enableMask & cAtSemControllerAlarmInitialization) ? (regVal | mask) : (regVal & (uint32)~mask);
        }

    if (alarmMask & cAtSemControllerAlarmHeartbeatSlr1)
        {
        mask = mFieldMask((ThaSemController)self, _sem_cur_heartbeat1_);
        regVal = (enableMask & cAtSemControllerAlarmHeartbeatSlr1) ? (regVal | mask) : (regVal & (uint32)~mask);
        }

    if (alarmMask & cAtSemControllerAlarmHeartbeatSlr2)
        {
        mask = mFieldMask((ThaSemController)self, _sem_cur_heartbeat2_);
        regVal = (enableMask & cAtSemControllerAlarmHeartbeatSlr2) ? (regVal | mask) : (regVal & (uint32)~mask);
        }

    if (alarmMask & cAtSemControllerAlarmActiveSlr1)
        {
        mask = mFieldMask((ThaSemController)self, _sem_cur_act1_);
        regVal = (enableMask & cAtSemControllerAlarmActiveSlr1) ? (regVal | mask) : (regVal & (uint32)~mask);
        }

    if (alarmMask & cAtSemControllerAlarmActiveSlr2)
        {
        mask = mFieldMask((ThaSemController)self, _sem_cur_act2_);
        regVal = (enableMask & cAtSemControllerAlarmActiveSlr2) ? (regVal | mask) : (regVal & (uint32)~mask);
        }

    AtHalWrite(hal, regAddr, regVal);

    return cAtOk;
    }

static uint32 Hw2SwAlarm(AtSemController self, uint32 regVal)
    {
    uint32 alarm = 0, mask = 0;

    mask = mFieldMask((ThaSemController)self, _sem_cur_heartbeat_);
    if (regVal & mask)
        alarm |= cAtSemControllerAlarmHeartbeat;

    mask = mFieldMask((ThaSemController)self, _sem_cur_uncor_);
    if (regVal & mask)
        alarm |= cAtSemControllerAlarmErrorNoncorrectable;

    mask = mFieldMask((ThaSemController)self, _sem_cur_correctable_);
    if (regVal & mask)
        alarm |= cAtSemControllerAlarmErrorCorrectable;

    mask = mFieldMask((ThaSemController)self, _sem_cur_essen_);
    if (regVal & mask)
        alarm |= cAtSemControllerAlarmErrorEssential;

    mask = mFieldMask((ThaSemController)self, _sem_cur_act_);
    if (regVal & mask)
        alarm |= cAtSemControllerAlarmActive;

    mask = mFieldMask((ThaSemController)self, _sem_cur_idle_);
    if (regVal & mask)
        alarm |= cAtSemControllerAlarmIdle;

    mask = mFieldMask((ThaSemController)self, _sem_cur_injec_);
    if (regVal & mask)
        alarm |= cAtSemControllerAlarmInjection;

    mask = mFieldMask((ThaSemController)self, _sem_cur_class_);
    if (regVal & mask)
        alarm |= cAtSemControllerAlarmClassification;

    mask = mFieldMask((ThaSemController)self, _sem_cur_fata_);
    if (regVal & mask)
        alarm |= cAtSemControllerAlarmFatalError;

    mask = mFieldMask((ThaSemController)self, _sem_cur_obsv_);
    if (regVal & mask)
        alarm |= cAtSemControllerAlarmObservation;

    mask = mFieldMask((ThaSemController)self, _sem_cur_corr_);
    if (regVal & mask)
        alarm |= cAtSemControllerAlarmCorrection;

    mask = mFieldMask((ThaSemController)self, _sem_cur_init_);
    if (regVal & mask)
        alarm |= cAtSemControllerAlarmInitialization;

    mask = mFieldMask((ThaSemController)self, _sem_cur_heartbeat1_);
    if (regVal & mask)
        alarm |= cAtSemControllerAlarmHeartbeatSlr1;

    mask = mFieldMask((ThaSemController)self, _sem_cur_heartbeat2_);
    if (regVal & mask)
        alarm |= cAtSemControllerAlarmHeartbeatSlr2;

    mask = mFieldMask((ThaSemController)self, _sem_cur_act1_);
    if (regVal & mask)
        alarm |= cAtSemControllerAlarmActiveSlr1;

    mask = mFieldMask((ThaSemController)self, _sem_cur_act2_);
    if (regVal & mask)
        alarm |= cAtSemControllerAlarmActiveSlr2;

    return alarm;
    }

static uint32 InterruptMaskGet(AtSemController self)
    {
    return Hw2SwAlarm(self, AtHalRead(HalGet(self), SeuIntrEnableRegister(self)));
    }

static uint32 EssentialCounterRegisterR2c(eBool r2c)
    {
    return r2c ? 0x8 : 0x9;
    }

static uint32 CorrectableCounterRegisterR2c(eBool r2c)
    {
    return r2c ? 0xC : 0xD;
    }

static uint32 UnCorrectableCounterRegisterR2c(eBool r2c)
    {
    return r2c ? 0xA : 0xB;
    }

static uint32 CountersR2c(AtSemController self, uint32 counterType, eBool r2c)
    {
    if (counterType == cAtSemControllerCounterTypeEssential)
        return ReadWithOffset(self, EssentialCounterRegisterR2c(r2c)) & cBit7_0;

    if (counterType == cAtSemControllerCounterTypeCorrectable)
        return ReadWithOffset(self, CorrectableCounterRegisterR2c(r2c)) & cBit7_0;

    if (counterType == cAtSemControllerCounterTypeUncorrectable)
        return ReadWithOffset(self, UnCorrectableCounterRegisterR2c(r2c)) & cBit7_0;

    return 0;
    }

static uint32 CounterGet(AtSemController self, uint32 counterType)
    {
    return CountersR2c(self, counterType, cAtFalse);
    }

static uint32 CounterClear(AtSemController self, uint32 counterType)
    {
    return CountersR2c(self, counterType, cAtTrue);
    }

static AtUart UartObjectCreate(AtSemController self)
    {
    return ThaUartNew(AtSemControllerDeviceGet(self), (uint8)AtSemControllerIdGet(self));
    }

static uint8 CorrectedErrorCounterGet(AtSemController self)
    {
    return (uint8)AtSemControllerCounterGet(self, cAtSemControllerCounterTypeCorrectable);
    }

static void InterruptProcess(AtSemController self, AtIpCore ipCore)
    {
    AtHal hal = AtIpCoreHalGet(ipCore);
    uint32 regIntrAddr = SeuIntrStatusRegister(self);
    uint32 regVal = AtHalRead(hal, regIntrAddr);
    uint32 regIntrEnVal = AtHalRead(hal, SeuIntrEnableRegister(self));
    uint32 regStatusVal;
    uint32 event = 0, alarm = 0, statusMask = 0;
    uint32 statusRegAddrress = SeuStatusRegister(self);

    regVal &= regIntrEnVal;

    /* Clear interrupt*/
    AtHalWrite(hal, regIntrAddr, regVal);
    regStatusVal = AtHalRead(hal, statusRegAddrress);

    statusMask = mFieldMask((ThaSemController)self, _sem_cur_act_);
    if (regVal & statusMask)
        {
        event |= cAtSemControllerAlarmActive;
        if (regStatusVal & statusMask)
            alarm |= cAtSemControllerAlarmActive;
        }

    statusMask = mFieldMask((ThaSemController)self, _sem_cur_corr_);
    if (regVal & statusMask)
        {
        event |= cAtSemControllerAlarmCorrection;
        if (regStatusVal & statusMask)
            alarm |= cAtSemControllerAlarmCorrection;
        }

    statusMask = mFieldMask((ThaSemController)self, _sem_cur_obsv_);
    if (regVal & statusMask)
        {
        event |= cAtSemControllerAlarmObservation;
        if (regStatusVal & statusMask)
            alarm |= cAtSemControllerAlarmObservation;
        }

    statusMask = mFieldMask((ThaSemController)self, _sem_cur_fata_);
    if (regVal & statusMask)
        {
        event |= cAtSemControllerAlarmFatalError;
        if (regStatusVal & statusMask)
            alarm |= cAtSemControllerAlarmFatalError;
        }

    statusMask = mFieldMask((ThaSemController)self, _sem_cur_class_);
    if (regVal & statusMask)
        {
        event |= cAtSemControllerAlarmClassification;
        if (regStatusVal & statusMask)
            alarm |= cAtSemControllerAlarmClassification;
        }

    statusMask = mFieldMask((ThaSemController)self, _sem_cur_injec_);
    if (regVal & statusMask)
        {
        event |= cAtSemControllerAlarmInjection;
        if (regStatusVal & statusMask)
            alarm |= cAtSemControllerAlarmInjection;
        }

    statusMask = mFieldMask((ThaSemController)self, _sem_cur_init_);
    if (regVal & statusMask)
        {
        event |= cAtSemControllerAlarmInitialization;
        if (regStatusVal & statusMask)
            alarm |= cAtSemControllerAlarmInitialization;
        }

    statusMask = mFieldMask((ThaSemController)self, _sem_cur_idle_);
    if (regVal & statusMask)
        {
        event |= cAtSemControllerAlarmIdle;
        if (regStatusVal & statusMask)
            alarm |= cAtSemControllerAlarmIdle;
        }

    statusMask = mFieldMask((ThaSemController)self, _sem_cur_essen_);
    if (regVal & statusMask)/*essential event*/
        {
        event |= cAtSemControllerAlarmErrorEssential;
        alarm |= cAtSemControllerAlarmErrorEssential;
        }

    statusMask = mFieldMask((ThaSemController)self, _sem_cur_correctable_);
    if (regVal & statusMask)/*correctabl event*/
        {
        event |= cAtSemControllerAlarmErrorCorrectable;
        alarm |= cAtSemControllerAlarmErrorCorrectable;
        }

    statusMask = mFieldMask((ThaSemController)self, _sem_cur_uncor_);
    if (regVal & statusMask)/*uncorrectable event*/
        {
        event |= cAtSemControllerAlarmErrorNoncorrectable;
        alarm |= cAtSemControllerAlarmErrorNoncorrectable;
        }

    statusMask = mFieldMask((ThaSemController)self, _sem_cur_heartbeat_);
    if (regVal & statusMask)/*heartbeat event*/
        {
        event |= cAtSemControllerAlarmHeartbeat;
        alarm |= cAtSemControllerAlarmHeartbeat;
        }

    statusMask = mFieldMask((ThaSemController)self, _sem_cur_act1_);
    if (regVal & statusMask)
        {
        event |= cAtSemControllerAlarmActiveSlr1;
        if (regStatusVal & statusMask)
            alarm |= cAtSemControllerAlarmActiveSlr1;
        }

    statusMask = mFieldMask((ThaSemController)self, _sem_cur_act2_);
    if (regVal & statusMask)
        {
        event |= cAtSemControllerAlarmActiveSlr2;
        if (regStatusVal & statusMask)
            alarm |= cAtSemControllerAlarmActiveSlr2;
        }

    statusMask = mFieldMask((ThaSemController)self, _sem_cur_heartbeat1_);
    if (regVal & statusMask)/*heartbeat event*/
        {
        event |= cAtSemControllerAlarmHeartbeatSlr1;
        alarm |= cAtSemControllerAlarmHeartbeatSlr1;
        }

    statusMask = mFieldMask((ThaSemController)self, _sem_cur_heartbeat2_);
    if (regVal & statusMask)/*heartbeat event*/
        {
        event |= cAtSemControllerAlarmHeartbeatSlr2;
        alarm |= cAtSemControllerAlarmHeartbeatSlr2;
        }

    AtSemControllerAllAlarmListenersCall(self, event, alarm);
    }

static uint32 HeartBeatMaxTimeInUs(ThaSemController self)
    {
    AtUnused(self);
    return 20000;/*20ms*/
    }

static eBool IsHwActiveDetected(ThaSemController self)
    {
    AtHal hal = AtDeviceIpCoreHalGet(AtSemControllerDeviceGet((AtSemController)(self)), 0);
    uint32 regAddress, regVal, heartbeatMask;
    uint32 cHeartbeatSoakingTime = mMethodsGet(self)->HeartBeatMaxTimeInUs(self);

    regAddress = SeuIntrStatusRegister((AtSemController)self);
    regVal = AtHalRead(hal, regAddress);
    heartbeatMask = mFieldMask((ThaSemController)self, _sem_cur_heartbeat_)
                    | mFieldMask((ThaSemController)self, _sem_cur_heartbeat1_)
                    | mFieldMask((ThaSemController)self, _sem_cur_heartbeat2_);

    regVal = regVal & heartbeatMask;


    /* clear old heartbeat */
    if (regVal)
        AtHalWrite(hal, regAddress, regVal);
    AtOsalUSleep(cHeartbeatSoakingTime);

    regVal = AtHalRead(hal, regAddress);
    regVal = regVal & heartbeatMask;
    /* Check if the heartbeat is raised */
    if (regVal)
        {
        AtHalWrite(hal, regAddress, regVal);
        return cAtTrue;
        }

    return cAtFalse;
    }

static eAtRet WaitMoveToState(AtSemController self, eAtSemControllerAlarmType state)
    {
    tAtOsalCurTime curTime, startTime;
    uint32 elapse = 0;
    uint32 timeoutMs = 3000;
    eBool isSimulated = AtDeviceIsSimulated(AtSemControllerDeviceGet(self));

    AtOsalCurTimeGet(&startTime);
    while (elapse < timeoutMs)
        {
        if (AtSemControllerAlarmGet(self) & state)
            return cAtOk;

        /* Calculate elapse time */
        AtOsalCurTimeGet(&curTime);
        elapse = mTimeIntervalInMsGet(startTime, curTime);

        if (isSimulated)
            return cAtOk;
        }

    return cAtErrorSemStateMoveFail;
    }

static eAtRet AsyncStopAndReturn(AtSemController self, eAtRet ret)
    {
    mThis(self)->asyncState = cAsyncStop;
    return ret;
    }

static eAtRet AsyncWaitMoveToState(AtSemController self, eAtSemControllerAlarmType state)
    {
    tAtOsalCurTime curTime;
    uint32 elapse = 0;
    uint32 timeoutMs = 3000;
    eBool isSimulated = AtDeviceIsSimulated(AtSemControllerDeviceGet(self));
    uint32 semAlarm;

    if (mThis(self)->asyncState == cAsyncStartWaiting)
        {
        AtOsalCurTimeGet(&(mThis(self)->startTime));
        mThis(self)->asyncState = cAsyncWait;
        }

    semAlarm = AtSemControllerAlarmGet(self);
    if (isSimulated || (semAlarm & state))
        return AsyncStopAndReturn(self, cAtOk);

    /* Calculate elapse time */
    AtOsalCurTimeGet(&curTime);
    elapse = mTimeIntervalInMsGet(mThis(self)->startTime, curTime);

    if (elapse < timeoutMs)
        return cAtErrorAgain;

    return AsyncStopAndReturn(self, cAtErrorSemStateMoveFail);
    }

static eAtRet StateChangeCommandTransmit(AtSemController self, eAtSemControllerAlarmType state)
    {
    eAtRet ret = cAtOk;
    AtUart uart = AtSemControllerUartGet(self);

    switch (state)
        {
        case cAtSemControllerAlarmObservation:
            ret = AtUartTransmit(uart, "O", AtStrlen("O"));
            break;
        case cAtSemControllerAlarmIdle:
            ret = AtUartTransmit(uart, "I", AtStrlen("I"));
            break;

        case cAtSemControllerAlarmNone:
        case cAtSemControllerAlarmActive:
        case cAtSemControllerAlarmInitialization:
        case cAtSemControllerAlarmCorrection:
        case cAtSemControllerAlarmClassification:
        case cAtSemControllerAlarmInjection:
        case cAtSemControllerAlarmFatalError:
        case cAtSemControllerAlarmErrorEssential:
        case cAtSemControllerAlarmErrorCorrectable:
        case cAtSemControllerAlarmErrorNoncorrectable:
        case cAtSemControllerAlarmHeartbeat:
        case cAtSemControllerAlarmHeartbeatSlr1:
        case cAtSemControllerAlarmHeartbeatSlr2:
        case cAtSemControllerAlarmActiveSlr1:
        case cAtSemControllerAlarmActiveSlr2:
        default:
            return cAtErrorSemStateMoveFail;
        }

    return ret;
    }

static eAtRet MoveToStateByUart(AtSemController self, eAtSemControllerAlarmType state)
    {
    eAtRet ret = StateChangeCommandTransmit(self, state);
    if (ret != cAtOk)
        return ret;

    /* Polling */
    return WaitMoveToState(self, state);
    }

static eAtRet AsyncMoveToStateByUart(AtSemController self, eAtSemControllerAlarmType state)
    {
    if (mThis(self)->asyncState == cAsyncStop)
        {
        eAtRet ret = StateChangeCommandTransmit(self, state);
        if (ret != cAtOk)
            return ret;

        mThis(self)->asyncState = cAsyncStartWaiting;
        }

    /* Polling */
    return AsyncWaitMoveToState(self, state);
    }

static eAtRet UartPollingValidateResult(AtSemController self, uint32 previousCorrectedCounter, uint32 errorType)
    {
    tAtOsalCurTime curTime, startTime;
    uint32 elapse = 0;

    AtOsalCurTimeGet(&startTime);
    while (elapse < cUartTimeoutInMs)
        {
        if (AtSemControllerCounterGet(self, errorType) == (previousCorrectedCounter + 1))
            return cAtOk;

        /* Calculate elapse time */
        AtOsalCurTimeGet(&curTime);
        elapse = mTimeIntervalInMsGet(startTime, curTime);
        }

    return cAtErrorSemValidateFail;
    }

static eAtRet LfaErrorInject(AtSemController self, uint32 lfa, uint32 dwIdx, uint32 bitIdx, char *cmdString64bytes)
    {
    return ThaSemControllerErrorInject((ThaSemController)self, lfa, dwIdx, bitIdx, cmdString64bytes);
    }

static uint32 ErrorInjectionWordIdxGenerate(uint32 frameSize, eBool isBasic)
    {
    if (isBasic)
        return frameSize / 2;

    return AtStdRand(AtStdSharedStdGet()) % frameSize;
    }

static uint32 ErrorInjectionBitIdxGenerate(uint32 wordSize, eBool isBasic)
    {
    if (isBasic)
        return 0;

    return AtStdRand(AtStdSharedStdGet()) % wordSize;
    }

static eBool AddressGenerate(AtSemController self, eBool isBasic, uint32 *lfa, uint32 *dwIdx, uint32 *bitIdx)
    {
    const uint32 frameSize = AtSemControllerFrameSize(self);
    const uint32 wordSize = 32;

    if (lfa)
        {
        *lfa = ThaSemControllerRandomLinearAddress(self);
        if (*lfa == cInvalidUint32)
            {
            AtDriverLog(AtDriverSharedDriverGet(), cAtLogLevelCritical, "Can not find a valid address to inject error\r\n");
            return cAtFalse;
            }
        }

    if (dwIdx)
        *dwIdx = ErrorInjectionWordIdxGenerate(frameSize, isBasic);
    if (bitIdx)
        *bitIdx = ErrorInjectionBitIdxGenerate(wordSize, isBasic);

    return cAtTrue;
    }

static eAtRet ValidateByUartWithErrorType(AtSemController self, uint32 errorType)
    {
    uint32 correctedCounter = AtSemControllerCounterClear(self, errorType);
    uint32 retry = 0, cMaxRetry = 1000, lfa, dwIdx, bitIdx;
    eAtRet ret = cAtErrorAgain;
    char cmdString64bytes[64];

    correctedCounter = 0;
    while ((ret == cAtErrorAgain) && (retry < cMaxRetry))
        {
        /* Move to Idle state */
        ret = MoveToStateByUart(self, cAtSemControllerAlarmIdle);
        if (ret != cAtOk)
            {
            mDriverErrorLog(ret, "MoveToState cAtSemControllerAlarmIdle");
            return ret;
            }

        if (AddressGenerate(self, cAtTrue, &lfa, &dwIdx, &bitIdx) != cAtTrue)
            {
            ret = cAtErrorAgain;
            retry ++;
            continue;
            }

        /* Force error and wait move back to Idle state */
        ret = LfaErrorInject(self, lfa, dwIdx, bitIdx, cmdString64bytes);
        if (ret != cAtOk)
            {
            ret = cAtErrorAgain;
            retry++;
            continue;
            }

        ret = WaitMoveToState(self, cAtSemControllerAlarmIdle);
        if (ret != cAtOk)
            {
            mDriverErrorLog(ret, "WaitMoveToState cAtSemControllerAlarmIdle");
            return ret;
            }

        ret = MoveToStateByUart(self, cAtSemControllerAlarmObservation);
        if (ret != cAtOk)
            {
            mDriverErrorLog(ret, "MoveToState cAtSemControllerAlarmObservation");
            return ret;
            }

        ret = UartPollingValidateResult(self, correctedCounter, errorType);
        if (ret != cAtOk)
            {
            AtDriverLog(AtDriverSharedDriverGet(), cAtLogLevelWarning, "%s %s\r\n", "error injection failed at: ", cmdString64bytes);
            ret = cAtErrorAgain;
            }
        else
            AtDriverLog(AtDriverSharedDriverGet(), cAtLogLevelWarning, "%s %s\r\n", "error injection Ok at: ", cmdString64bytes);

        retry = retry + 1;
        }

    if (retry == cMaxRetry)
        ret = cAtErrorSemValidateFail;

    return ret;
    }

static eAtRet ValidateByUart(AtSemController self)
    {
    return ValidateByUartWithErrorType(self, cAtSemControllerCounterTypeCorrectable);
    }

static eAtRet ForceErrorByUart(AtSemController self, eAtSemControllerCounterType errorType)
    {
    return ValidateByUartWithErrorType(self, errorType);
    }

static eAtRet AsyncForceErrorByUart(AtSemController self, eAtSemControllerCounterType errorType)
    {
    return ThaSemControllerAsyncValidateByUartWithErrorType(self, errorType);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeNone(startTime);
    mEncodeNone(asyncState);
    }

static void OverrideAtObject(AtSemController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideThaSemController(AtSemController self)
    {
    ThaSemController controller = (ThaSemController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSemControllerOverride, mMethodsGet(controller), sizeof(m_ThaSemControllerOverride));

        mMethodOverride(m_ThaSemControllerOverride, IsHwActiveDetected);
        mMethodOverride(m_ThaSemControllerOverride, HeartBeatMaxTimeInUs);

        mBitFieldOverride(ThaSemController, m_ThaSemControllerOverride, _sem_cur_heartbeat_)
        mBitFieldOverride(ThaSemController, m_ThaSemControllerOverride, _sem_cur_correctable_)
        mBitFieldOverride(ThaSemController, m_ThaSemControllerOverride, _sem_cur_essen_)
        mBitFieldOverride(ThaSemController, m_ThaSemControllerOverride, _sem_cur_act_)
        mBitFieldOverride(ThaSemController, m_ThaSemControllerOverride, _sem_cur_uncor_)
        mBitFieldOverride(ThaSemController, m_ThaSemControllerOverride, _sem_cur_fata_)
        mBitFieldOverride(ThaSemController, m_ThaSemControllerOverride, _sem_cur_injec_)
        mBitFieldOverride(ThaSemController, m_ThaSemControllerOverride, _sem_cur_idle_)
        mBitFieldOverride(ThaSemController, m_ThaSemControllerOverride, _sem_cur_class_)
        mBitFieldOverride(ThaSemController, m_ThaSemControllerOverride, _sem_cur_corr_)
        mBitFieldOverride(ThaSemController, m_ThaSemControllerOverride, _sem_cur_obsv_)
        mBitFieldOverride(ThaSemController, m_ThaSemControllerOverride, _sem_cur_init_)
        }

    mMethodsSet(controller, &m_ThaSemControllerOverride);
    }

static void OverrideAtSemController(AtSemController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSemControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSemControllerOverride, m_AtSemControllerMethods, sizeof(m_AtSemControllerOverride));

        mMethodOverride(m_AtSemControllerOverride, AlarmGet);
        mMethodOverride(m_AtSemControllerOverride, AlarmHistoryGet);
        mMethodOverride(m_AtSemControllerOverride, AlarmHistoryClear);
        mMethodOverride(m_AtSemControllerOverride, InterruptMaskSet);
        mMethodOverride(m_AtSemControllerOverride, InterruptMaskGet);
        mMethodOverride(m_AtSemControllerOverride, CounterGet);
        mMethodOverride(m_AtSemControllerOverride, CounterClear);
        mMethodOverride(m_AtSemControllerOverride, UartObjectCreate);
        mMethodOverride(m_AtSemControllerOverride, CorrectedErrorCounterGet);
        mMethodOverride(m_AtSemControllerOverride, InterruptProcess);
        mMethodOverride(m_AtSemControllerOverride, ValidateByUart);
        mMethodOverride(m_AtSemControllerOverride, ForceErrorByUart);
        mMethodOverride(m_AtSemControllerOverride, AsyncForceErrorByUart);
        }

    mMethodsSet(self, &m_AtSemControllerOverride);
    }

static void Override(AtSemController self)
    {
    OverrideAtObject(self);
    OverrideThaSemController(self);
    OverrideAtSemController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaSemController);
    }

AtSemController ThaSemControllerV3ObjectInit(AtSemController self, AtDevice device, uint32 semId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaSemControllerObjectInit(self, device, semId) == NULL)
        return NULL;

    /* Setup methods */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

eAtRet ThaSemControllerAsyncWaitMoveToState(AtSemController self, eAtSemControllerAlarmType state)
    {
    mThis(self)->asyncState = cAsyncStartWaiting;
    return AsyncWaitMoveToState(self, state);
    }

eAtRet ThaSemControllerAsyncLfaErrorInject(AtSemController self, uint32 lfa, uint32 dwIdx, uint32 bitIdx, char *cmdString64bytes)
    {
    return LfaErrorInject(self, lfa, dwIdx, bitIdx, cmdString64bytes);
    }

eBool ThaSemControllerAsyncAddressGenerate(AtSemController self, eBool isBasic, uint32 *lfa, uint32 *dwIdx, uint32 *bitIdx)
    {
    return AddressGenerate(self, isBasic, lfa, dwIdx, bitIdx);
    }

eAtRet ThaSemControllerMoveToStateByUart(AtSemController self, eAtSemControllerAlarmType state)
    {
    return MoveToStateByUart(self, state);
    }

eAtRet ThaSemControllerAsyncMoveToStateByUart(AtSemController self, eAtSemControllerAlarmType state)
    {
    return AsyncMoveToStateByUart(self, state);
    }

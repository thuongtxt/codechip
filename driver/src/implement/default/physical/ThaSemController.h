/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PHYSICAL
 * 
 * File        : ThaSemController.h
 * 
 * Created Date: Mar 23, 2016
 *
 * Description : Default SEM controller interface.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef THASEMCONTROLLER_H_
#define THASEMCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSemController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaSemController * ThaSemController;
typedef struct tThaSemControllerV2 * ThaSemControllerV2;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSemController ThaSemControllerNew(AtDevice device, uint32 semId);
AtSemController ThaSemControllerV2New(AtDevice device, uint32 semId); /* V2: Support interrupt. */
AtSemController ThaSemControllerV3New(AtDevice device, uint32 semId); /* Support UART and new interrupt and counters */
AtUart ThaUartNew(AtDevice device, uint8 slrId);

uint32 ThaSemControllerDefaultOffset(ThaSemController self);
eBool ThaSemControllerMoveToState(AtSemController self, uint8 stateValue);

#ifdef __cplusplus
}
#endif
#endif /* THASEMCONTROLLER_H_ */


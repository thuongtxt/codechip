/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : ThaSdhLineSerdesControllerInternal.h
 * 
 * Created Date: Dec 17, 2013
 *
 * Description : SDH Line SERDES controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASDHLINESERDESCONTROLLERINTERNAL_H_
#define _THASDHLINESERDESCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaSdhLineSerdesController.h"
#include "ThaSerdesControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaSdhLineSerdesControllerMethods
    {
    uint32 (*LockToDataRegAddress)(ThaSdhLineSerdesController self);
    uint32 (*LockToDataRegMask)(ThaSdhLineSerdesController self);
    uint32 (*LockToDataRegShift)(ThaSdhLineSerdesController self);

    uint32 (*LockToRefRegAddress)(ThaSdhLineSerdesController self);
    uint32 (*LockToRefRegMask)(ThaSdhLineSerdesController self);
    uint32 (*LockToRefRegShift)(ThaSdhLineSerdesController self);

    eAtRet (*RateSet)(ThaSdhLineSerdesController self, eAtSdhLineRate rate);
    eAtSdhLineRate (*RateGet)(ThaSdhLineSerdesController self);
    }tThaSdhLineSerdesControllerMethods;

typedef struct tThaSdhLineSerdesController
    {
    tThaSerdesController super;
    const tThaSdhLineSerdesControllerMethods *methods;

    /* Private data */
    }tThaSdhLineSerdesController;

typedef struct tThaSdhLineSerdesControllerCyclone4
    {
    tThaSdhLineSerdesController super;

    /* Private data */
    eBool loopinEnabled;

    /* Cache */
    uint32 vod;
    uint32 preTap;
    uint32 rxEqualizationDcGain;
    uint32 rxEqualizationControl;
    }tThaSdhLineSerdesControllerCyclone4;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSerdesController ThaSdhLineSerdesControllerObjectInit(AtSerdesController self, AtSdhLine sdhLine, uint32 serdesId);
AtSerdesController ThaSdhLineSerdesControllerCyclone4ObjectInit(AtSerdesController self, AtSdhLine sdhLine, uint32 serdesId);

#ifdef __cplusplus
}
#endif
#endif /* _THASDHLINESERDESCONTROLLERINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : ThaSemControllerV3Internal.h
 * 
 * Created Date: Aug 31, 2017
 *
 * Description : SEM controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASEMCONTROLLERV3INTERNAL_H_
#define _THASEMCONTROLLERV3INTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaSemControllerInternal.h"
#include "ThaSemControllerV3.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaSemControllerV3
    {
    tThaSemController super;

    /* For async operations */
    tAtOsalCurTime startTime;
    uint8 asyncState;
    }tThaSemControllerV3;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THASEMCONTROLLERV3INTERNAL_H_ */


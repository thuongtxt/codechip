/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PHYSICAL
 *
 * File        : ThaSemController.c
 *
 * Created Date: May 27, 2015
 *
 * Description : Default SEM controller implementation.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../util/coder/AtCoderUtil.h"
#include "../man/ThaDeviceInternal.h"
#include "ThaSemControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaInvalidRegister 0xCAFECAFE

/* SEM state machine control */
#define cSeuPciConfiguration0           0x0
#define cSeuCmdCtrlDoneMask             cBit10
#define cSeuCmdCtrlDoneShift            10
#define cSeuInjectStrobeMask            cBit8
#define cSeuInjectStrobeShift           8
#define cSeuOperationMask               cBit7_4
#define cSeuOperationShift              4

/* Frame address select */
#define cSeuPciConfiguration1           0x1
#define cSeuFrameBitAddressMask         cBit4_0
#define cSeuFrameBitAddressShift        0
#define cSeuFrameWordAddressMask        cBit11_5
#define cSeuFrameWordAddressShift       5
#define cSeuFrameLinearAddressMask      cBit28_12
#define cSeuFrameLinearAddressShift     12
#define cSeuFrameSlrNumberMask          cBit30_29
#define cSeuFrameSlrNumberShift         29

/* SEM alarm, write to clear */
#define cSeuPciAlarm                    0x2
#define cSeuPciAlarmInitStateMask       cBit9
#define cSeuPciAlarmFatalSemErrMask     cBit2
#define cSeuPciAlarmCorrectionMask      cBit0

/* SEM status */
#define cSeuPciStatus              0x3
#define cTha_sem_cur_idle_Mask    cBit16
#define cTha_sem_cur_idle_Shift     16
#define cSeuPciSemErrorCntMask     cBit15_8
#define cSeuPciSemErrorCntShift    8
#define cTha_sem_cur_heartbeat_Mask   0
#define cTha_sem_cur_heartbeat_Shift  0
#define cTha_sem_cur_correctable_Mask   0
#define cTha_sem_cur_correctable_Shift  0
#define cTha_sem_cur_essen_Mask   0
#define cTha_sem_cur_essen_Shift  0
#define cTha_sem_cur_act_Mask   cBit7
#define cTha_sem_cur_act_Shift  7
#define cTha_sem_cur_uncor_Mask   cBit6
#define cTha_sem_cur_uncor_Shift    6
#define cTha_sem_cur_obsv_Mask    cBit5
#define cTha_sem_cur_obsv_Shift     5
#define cTha_sem_cur_init_Mask    cBit4
#define cTha_sem_cur_init_Shift     4
#define cTha_sem_cur_fata_Mask     cBit3
#define cTha_sem_cur_fata_Shift      3
#define cTha_sem_cur_class_Mask    cBit2
#define cTha_sem_cur_class_Shift     2
#define cTha_sem_cur_injec_Mask    cBit1
#define cTha_sem_cur_injec_Shift     1
#define cTha_sem_cur_corr_Mask     cBit0
#define cTha_sem_cur_corr_Shift        0

#define cTha_sem_cur_heartbeat1_Mask   0
#define cTha_sem_cur_heartbeat1_Shift  0
#define cTha_sem_cur_heartbeat2_Mask   0
#define cTha_sem_cur_heartbeat2_Shift  0
#define cTha_sem_cur_act1_Mask         0
#define cTha_sem_cur_act1_Shift        0
#define cTha_sem_cur_act2_Mask         0
#define cTha_sem_cur_act2_Shift        0

#define cFsmTimeoutInMs    1200
#define cSemNoAction       0xff


/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)             ((ThaSemController)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaSemControllerMethods m_methods;

/* Override */
static tAtObjectMethods        m_AtObjectOverride;
static tAtSemControllerMethods m_AtSemControllerOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
mDefineMaskShift(ThaSemController, _sem_cur_heartbeat_)
mDefineMaskShift(ThaSemController, _sem_cur_essen_)
mDefineMaskShift(ThaSemController, _sem_cur_act_)
mDefineMaskShift(ThaSemController, _sem_cur_correctable_)
mDefineMaskShift(ThaSemController, _sem_cur_uncor_)
mDefineMaskShift(ThaSemController, _sem_cur_fata_)
mDefineMaskShift(ThaSemController, _sem_cur_injec_)
mDefineMaskShift(ThaSemController, _sem_cur_idle_)
mDefineMaskShift(ThaSemController, _sem_cur_class_)
mDefineMaskShift(ThaSemController, _sem_cur_corr_)
mDefineMaskShift(ThaSemController, _sem_cur_obsv_)
mDefineMaskShift(ThaSemController, _sem_cur_init_)
mDefineMaskShift(ThaSemController, _sem_cur_heartbeat1_)
mDefineMaskShift(ThaSemController, _sem_cur_heartbeat2_)
mDefineMaskShift(ThaSemController, _sem_cur_act1_)
mDefineMaskShift(ThaSemController, _sem_cur_act2_)

/* Steps to validate SEM IP
 * ============================
 * - Step1: Check if the bits "Sem_Act" and "ObSer_St" are both set to 1 in the SEU PCI Status register.
 * - Step2: Set into SEU PCI Configuration 0 register, OP field to idle (0xE)
 * - Step3: Assert the "SEU_INJECT_STROBE" bit in the EU PCI Configuration 0 register.
 * - Step4: Deassert the  "SEU_INJECT_STROBE" bit
 * - Step5: Wait until the "Idle_St" bit goes to 1 in the  SEU PCI Status register.
 * - Step6: Put a random value inside the "FR_ADD" field in the SEU PCI Configuration 1 register.
 * - Step7: Set OP field to linear (0xC) in the SEU PCI Configuration 0 register.
 * - Step8: Assert the "SEU_INJECT_STROBE" bit in the SEU PCI Configuration 0 register.
 * - Step9: Deassert the  "SEU_INJECT_STROBE" bit.
 * - Step10: Wait until the "Idle_St" bit goes to 1 in the SEU PCI Status register.
 * - Step11: Set OP field to observation (0xA) in the SEU PCI Configuration 0 register.
 * - Step12: Assert the "SEU_INJECT_STROBE" bit in the SEU PCI Configuration 0 register.
 * - Step13: Deassert the  "SEU_INJECT_STROBE" bit.
 * - Step14: Wait until the "ObSer_St" bit goes to 1 in the  SEU PCI Status register.
 * - Step15: Poll "CorSt" alarm bit inside  SEU Alarm  register is set to 1, It means that a CRC error was injected.
 * - Step16: Clear SEU Alarm register. If the field "UnCor_St" inside the SEU PCI Status register is set to 1,
 *           then the injected error has lead toward an irreversible situation : a FPGA download is required,
 *           otherwise the error has been corrected if also the SEU Alarm register is all clear.
 */
static eBool WaitFsmToMoveToState(AtSemController self, uint8 state)
    {
    tAtOsalCurTime curTime, startTime;
    uint32 elapse = 0;
    AtDevice device = AtSemControllerDeviceGet(self);
    AtHal hal = AtDeviceIpCoreHalGet(device, 0);
    uint32 stateMask = (state == cFsmOBSERVATION) ? mFieldMask(mThis(self), _sem_cur_obsv_) : mFieldMask(mThis(self), _sem_cur_idle_);

    AtOsalCurTimeGet(&startTime);
    while (elapse < cFsmTimeoutInMs)
        {
        uint32 regVal = AtHalRead(hal, ThaSemControllerCurrentAlarmRegister(mThis(self)) + mDefaultOffset(self));

        if (regVal & stateMask)
            {
            if (elapse > AtSemControllerMaxElapseTimeToMoveFsm(self))
                mThis(self)->maxElapseTimeToMoveFsm = elapse;
            return cAtTrue;
            }

        if (AtDeviceIsSimulated(device))
            return cAtTrue;

        /* Calculate elapse time and retry */
        AtOsalCurTimeGet(&curTime);
        elapse = mTimeIntervalInMsGet(startTime, curTime);
        }

    AtDeviceLog(device, cAtLogLevelCritical, AtSourceLocation, "WaitFsmToMoveToState fail!\r\n");
    return cAtFalse;
    }

static void CmdControlDoneStateClear(AtSemController self, AtHal hal)
    {
    AtHalWrite(hal, cSeuPciConfiguration0 + mDefaultOffset(self), cSeuCmdCtrlDoneMask);
    }

static eBool WaitCmdControlDone(AtSemController self, AtHal hal)
    {
    tAtOsalCurTime curTime, startTime;
    uint32 regVal;
    uint32 elapse = 0;
    AtDevice device = AtSemControllerDeviceGet(self);

    AtOsalCurTimeGet(&startTime);
    while (elapse < cFsmTimeoutInMs)
        {
        regVal = AtHalRead(hal, cSeuPciConfiguration0 + mDefaultOffset(self));

        if (AtDeviceIsSimulated(device))
            return cAtTrue;

        if ((regVal & cSeuCmdCtrlDoneMask))
            return cAtTrue;

        /* Calculate elapse time and retry */
        AtOsalCurTimeGet(&curTime);
        elapse = mTimeIntervalInMsGet(startTime, curTime);
        }

    AtDeviceLog(device, cAtLogLevelCritical, AtSourceLocation, "SEM command processing is not done!\r\n");
    return cAtFalse;
    }

static eBool SeuFsmMoveToState(AtSemController self, uint8 state)
    {
    AtDevice device = AtSemControllerDeviceGet(self);
    AtHal hal = AtDeviceIpCoreHalGet(device, 0);
    uint32 regVal = 0;
    eBool useDoneBit = mMethodsGet(mThis(self))->NeedUseCmdCtrlDoneBit(mThis(self));
    eBool simulated;

    if (useDoneBit)
        CmdControlDoneStateClear(self, hal);

    /* Set state and strobe bit */
    mRegFieldSet(regVal, cSeuOperation, state);
    mRegFieldSet(regVal, cSeuInjectStrobe, 1);
    AtHalWrite(hal, cSeuPciConfiguration0 + mDefaultOffset(self), regVal);

    simulated = AtDeviceIsSimulated(device);
    if (!simulated)
        AtOsalUSleep(1000);

    /* Clear strobe and wait until FSM move to new state completely */
    mRegFieldSet(regVal, cSeuInjectStrobe, 0);
    AtHalWrite(hal, cSeuPciConfiguration0 + mDefaultOffset(self), regVal);

    if (!simulated)
        AtOsalUSleep(1000);

    if (useDoneBit && (WaitCmdControlDone(self, hal) == cAtFalse))
        return cAtFalse;

    return WaitFsmToMoveToState(self, state);
    }

static eBool WaitForCrcErrorCorrection(AtSemController self)
    {
    tAtOsalCurTime curTime, startTime;
    AtHal hal = AtDeviceIpCoreHalGet(AtSemControllerDeviceGet(self), 0);
    uint32 elapse = 0;
    eBool errorInjected = cAtFalse;
    uint32 regVal;

    AtOsalCurTimeGet(&startTime);
    while (elapse < cFsmTimeoutInMs)
        {
        regVal = AtHalRead(hal, ThaSemControllerHistoryAlarmRegister(mThis(self)) + mDefaultOffset(self));
        if (regVal & cSeuPciAlarmCorrectionMask)
            {
            if (elapse > AtSemControllerMaxElapseTimeToInsertError(self))
                mThis(self)->maxElapseTimeToInsertError = elapse;

            errorInjected = cAtTrue;
            break;
            }

        /* Calculate elapse time and retry */
        AtOsalCurTimeGet(&curTime);
        elapse = mTimeIntervalInMsGet(startTime, curTime);
        }

    return errorInjected;
    }

static eBool IsSimulated(AtSemController self)
    {
    return AtDeviceIsSimulated(AtSemControllerDeviceGet(self));
    }

void ThaSemControllerClearAlarm(ThaSemController self, uint32 hwAlarms)
    {
    const uint16 cDelayTimeToClearAlarmInMs = 5;
    AtSemController controller = (AtSemController)self;
    AtHal hal = AtDeviceIpCoreHalGet(AtSemControllerDeviceGet(controller), 0);

    if (!IsSimulated(controller))
        AtOsalUSleep(cDelayTimeToClearAlarmInMs * 1000UL);

    AtHalWrite(hal, ThaSemControllerHistoryAlarmRegister(mThis(self)) + mDefaultOffset(self), hwAlarms);
    }

static eAtSemControllerAlarm AlarmRead2Clear(AtSemController self, eBool read2Clear)
    {
    AtHal hal;
    uint32 regVal;
    uint32 alarm = cAtSemControllerAlarmNone;

    hal = AtDeviceIpCoreHalGet(AtSemControllerDeviceGet(self), 0);
    regVal = AtHalRead(hal, ThaSemControllerHistoryAlarmRegister(mThis(self)) + mDefaultOffset(self));

    if (regVal & cSeuPciAlarmInitStateMask)
        alarm |= cAtSemControllerAlarmInitialization;

    if (regVal & cSeuPciAlarmFatalSemErrMask)
        alarm |= cAtSemControllerAlarmFatalError;

    if (regVal & cSeuPciAlarmCorrectionMask)
        alarm |= cAtSemControllerAlarmCorrection;

    if (read2Clear)
        ThaSemControllerClearAlarm((ThaSemController)self, regVal);

    if (IsSimulated(self))
        return cAtSemControllerAlarmNone;

    return alarm;
    }

static eAtRet SemCorrectionResultGet(ThaSemController self)
    {
    AtHal hal = AtDeviceIpCoreHalGet(AtSemControllerDeviceGet((AtSemController)self), 0);
    uint32 regVal;

    regVal = AtHalRead(hal, ThaSemControllerCurrentAlarmRegister(self) + mDefaultOffset(self));
    if (regVal & mFieldMask(self, _sem_cur_uncor_))
        return cAtErrorSemUncorrectable;

    if (AtSemControllerAlarmHistoryClear((AtSemController)self) & cAtSemControllerAlarmFatalError)
        return cAtErrorSemFatalError;

    return cAtOk;
    }

static void ClearCorrectableSticky(ThaSemController self)
    {
	AtUnused(self);
    }

static void FrameAddressSet(AtSemController self, uint32 address)
    {
    AtHal hal = AtDeviceIpCoreHalGet(AtSemControllerDeviceGet(self), 0);
    uint32 regVal = 0;

    mRegFieldSet(regVal, cSeuFrameWordAddress, mMethodsGet(mThis(self))->DefaultFrameWordAddress(mThis(self)));
    mRegFieldSet(regVal, cSeuFrameLinearAddress, address);
    mRegFieldSet(regVal, cSeuFrameSlrNumber, AtSemControllerIdGet(self));
    AtHalWrite(hal, cSeuPciConfiguration1 + ThaSemControllerDefaultOffset(mThis(self)), regVal);
    }

static eAtRet Validate(AtSemController self)
    {
    static const uint8 cMaxTryValidationTimes = 10;
    eAtRet ret;
    uint8 checkingTime = 0;
    eBool errorInjected = cAtFalse;
    eAtSemControllerAlarm semAlarm;

    ret = AtSemControllerMoveObservationModeBeforeActiveChecking(self);
    if (ret != cAtOk)
        return ret;

    if (!AtSemControllerIsActive(self))
        return cAtErrorSemNotActive;

    while ((errorInjected == cAtFalse) && (checkingTime < cMaxTryValidationTimes))
        {
        /* Move to Idle state */
        if (SeuFsmMoveToState(self, cFsmIDLE) == cAtFalse)
            return cAtErrorSemFsmFailed;

        /* Get Random Address and force */
        FrameAddressSet(self, ThaSemControllerRandomLinearAddress(self));

        /* Move to Linear state */
        if (SeuFsmMoveToState(self, cFsmLINEAR) == cAtFalse)
            return cAtErrorSemFsmFailed;

        /* Move to Observation state */
        if (SeuFsmMoveToState(self, cFsmOBSERVATION) == cAtFalse)
            return cAtErrorSemFsmFailed;

        /* Clear Old Sticky */
        mMethodsGet(mThis(self))->ClearCorrectableSticky(mThis(self));

        /* Insert Error */
        errorInjected = WaitForCrcErrorCorrection(self);
        if (errorInjected)
            return mMethodsGet(mThis(self))->SemCorrectionResultGet(mThis(self));

        /* If sem has any alarm */
        semAlarm = AtSemControllerAlarmHistoryClear(self);
        if (semAlarm & cAtSemControllerAlarmFatalError)
            break;

        checkingTime = (uint8)(checkingTime + 1);
        }

    /* Reach max try time but correction does not occur */
    return cAtErrorSemFatalError;
    }

static uint8 CorrectedErrorCounterGet(AtSemController self)
    {
    AtHal hal;
    uint32 regVal;

    hal = AtDeviceIpCoreHalGet(AtSemControllerDeviceGet(self), 0);
    regVal = AtHalRead(hal, ThaSemControllerCurrentAlarmRegister(mThis(self)) + mDefaultOffset(self));

    return (uint8)mRegField(regVal, cSeuPciSemErrorCnt);
    }

static uint32 AlarmGet(AtSemController self)
    {
    AtHal hal;
    uint32 regVal;
    uint32 status = 0;

    hal = AtDeviceIpCoreHalGet(AtSemControllerDeviceGet(self), 0);
    regVal = AtHalRead(hal, ThaSemControllerCurrentAlarmRegister(mThis(self)) + mDefaultOffset(self));

    if (regVal & mFieldMask(mThis(self), _sem_cur_idle_))
        status |= cAtSemControllerAlarmIdle;

    if (regVal & mFieldMask(mThis(self), _sem_cur_act_))
        status |= cAtSemControllerAlarmActive;

    if (regVal & mFieldMask(mThis(self), _sem_cur_obsv_))
        status |= cAtSemControllerAlarmObservation;

    if (regVal & mFieldMask(mThis(self), _sem_cur_init_))
        status |= cAtSemControllerAlarmInitialization;

    return (uint8)status;
    }

static uint8 StatusFromAlarm(uint32 alarm)
    {
    uint8 status = cAtSemControllerStatusNotSupport;

    if (alarm & cAtSemControllerAlarmIdle)
        status |= cAtSemControllerStatusIdle;

    if (alarm & cAtSemControllerAlarmActive)
        status |= cAtSemControllerStatusActive;

    if (alarm & cAtSemControllerAlarmObservation)
        status |= cAtSemControllerStatusObservation;

    if (alarm & cAtSemControllerAlarmInitialization)
        status |= cAtSemControllerStatusInitialization;

    return status;
    }

static uint8 StatusGet(AtSemController self)
    {
    AtHal hal;
    uint32 regVal;
    uint8 status = StatusFromAlarm(AtSemControllerAlarmGet(self));

    /* Get more deprecated status from HW */
    hal = AtDeviceIpCoreHalGet(AtSemControllerDeviceGet(self), 0);
    regVal = AtHalRead(hal, ThaSemControllerCurrentAlarmRegister(mThis(self)) + mDefaultOffset(self));

    if (regVal & mFieldMask(mThis(self), _sem_cur_uncor_))
        status |= cAtSemControllerStatusUncorrectable;

    return status;
    }

static uint32 AlarmHistoryGet(AtSemController self)
    {
    return AlarmRead2Clear(self, cAtFalse);
    }

static uint32 AlarmHistoryClear(AtSemController self)
    {
    return AlarmRead2Clear(self, cAtTrue);
    }

static uint32 MaxElapseTimeToInsertError(AtSemController self)
    {
    return mThis(self)->maxElapseTimeToInsertError;
    }

static uint32 MaxElapseTimeToMoveFsm(AtSemController self)
    {
    return mThis(self)->maxElapseTimeToMoveFsm;
    }

static eBool IsInObservationState(AtSemController self)
    {
    AtHal hal = AtDeviceIpCoreHalGet(AtSemControllerDeviceGet(self), 0);
    uint32 regAddr = ThaSemControllerCurrentAlarmRegister(mThis(self)) + ThaSemControllerDefaultOffset(mThis(self));
    uint32 regVal = AtHalRead(hal, regAddr);

    /* Check if the bits "Sem_Act" and "ObSer_St" are both set to 1 */
    if (regVal & mFieldMask(mThis(self), _sem_cur_obsv_))
        return cAtTrue;
    return cAtFalse;
    }

static eBool IsHwActiveDetected(ThaSemController self)
    {
    AtHal hal = AtDeviceIpCoreHalGet(AtSemControllerDeviceGet((AtSemController)self), 0);
    uint32 regAddr = ThaSemControllerCurrentAlarmRegister(mThis(self)) + ThaSemControllerDefaultOffset(mThis(self));
    uint32 regVal = AtHalRead(hal, regAddr);

    /* Check if the bits "Sem_Act" and "ObSer_St" are both set to 1 */
    if (regVal & mFieldMask(mThis(self), _sem_cur_act_))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 HeartBeatMaxTimeInUs(ThaSemController self)
    {
    AtUnused(self);
    return 0;
    }

static eBool IsActive(AtSemController self)
    {
    static const uint32 cWaitForActiveInMs = 20000;
    static const uint32 cCpuSleepTimeInUs = 500000;
    tAtOsalCurTime curTime, startTime;
    uint32 elapse = 0;
    eBool isSimulated = IsSimulated(self);

    AtOsalCurTimeGet(&startTime);
    while (elapse < cWaitForActiveInMs)
        {
        /* Check if the bits "Sem_Act" and "ObSer_St" are both set to 1 */
        if (IsInObservationState(self) && mMethodsGet(mThis(self))->IsHwActiveDetected(mThis(self)))
            return cAtTrue;

        if (IsSimulated(self))
            return cAtTrue;

        /* Calculate elapse time and retry */
        AtOsalCurTimeGet(&curTime);
        elapse = mTimeIntervalInMsGet(startTime, curTime);

        if (isSimulated)
            return cAtTrue;

        /* Let CPU has a rest */
        AtOsalUSleep(cCpuSleepTimeInUs);
        }

    return cAtFalse;
    }

static uint32 DefaultOffset(ThaSemController self)
    {
    AtUnused(self);
    return 0xF43000;
    }

static eBool NeedUseCmdCtrlDoneBit(ThaSemController self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 DefaultFrameWordAddress(ThaSemController self)
    {
    AtUnused(self);
    return 50;
    }

static uint8 NumSlr(AtSemController self)
    {
    AtUnused(self);
    return 1;
    }

static uint32 MaxLfa(AtSemController self)
    {
    AtUnused(self);
    return 0x7164;
    }

static uint32 FrameSize(AtSemController self)
    {
    AtUnused(self);
    return 101;/* 101 word for K7Series, 123 for ulstrascale, 93 for ultrascale+, 81 for virtex6 */
    }

static uint32 GlobalLfaToLocalLfa(AtSemController self, uint32 globalLfa)
    {
    AtUnused(self);

    return (globalLfa & cBit16_0);
    }

static uint32 GlobalLfaToSlrId(AtSemController self, uint32 globalLfa)
    {
    AtUnused(self);
    return (globalLfa & cBit18_17) >> 17;
    }

static eAtRet AddressCommandString(ThaSemController self, char *addressString, uint32 numChar, uint32 lfa, uint32 dwordIdxInLfa, uint32 bitIdxInDword)
    {
    char token[32];
    AtUart uart = AtSemControllerUartGet((AtSemController)self);
    uint32 value32bit = 0;

    /* Build command */
    if (numChar < (sizeof(token) + 2))
        return cAtErrorInvlParm;

    AtOsalMemInit(addressString, 0, numChar);
    AtStrcat(addressString, "C0");

    /* 1100  0000  0ssl  llll  llll  llll  llll  wwww  wwwb bbbb
     * ss is Hardware slr number (2-bit). Valid range: 0..2
     * Linear frame address (17-bit). Valid range: 0..Max Frame - 2
     * Word address (7-bit). Valid range: 0..122
     * Bit address (5-bit). Valid range: 0..31 */
    AtOsalMemInit(token, 0, sizeof(token));
    mFieldIns(&value32bit, cBit4_0, 0, bitIdxInDword);
    mFieldIns(&value32bit, cBit11_5, 5, dwordIdxInLfa);
    mFieldIns(&value32bit, cBit28_12, 12, lfa);
    mFieldIns(&value32bit, cBit30_29, 29, AtUartSuperLogicRegionIdGet(uart));
    mFieldIns(&value32bit, cBit31, 31, 0);
    AtSprintf(token, "%08X", value32bit);
    AtStrcat(addressString, token);

    return cAtOk;
    }

static eAtRet ErrorInject(ThaSemController self, uint32 lfa, uint32 dwIdx, uint32 bitIdx, char *cmdString64bytes)
    {
    char addressString[34];
    eAtRet ret = cAtOk;
    AtUart uart = AtSemControllerUartGet((AtSemController)self);

    /* Build command */
    AtSprintf(cmdString64bytes, "%s", "N ");

    /* 1100  0000  0ssl  llll  llll  llll  llll  wwww  wwwb bbbb
     * ss is Hardware slr number (2-bit). Valid range: 0..2
     * Linear frame address (17-bit). Valid range: 0..Max Frame - 2
     * Word address (7-bit). Valid range: 0..122
     * Bit address (5-bit). Valid range: 0..31 */
    AtOsalMemInit(addressString, 0, sizeof(addressString));
    ret = ThaSemControllerAddressCommandString(self, addressString, (uint32)sizeof(addressString), lfa, dwIdx, bitIdx);
    if (ret != cAtOk)
        {
        AtDriverLog(AtDriverSharedDriverGet(), cAtLogLevelCritical, "%s", "ErrorInject: Build SEM command String in failure\r\n");
        return ret;
        }
    AtStrcat(cmdString64bytes, addressString);

    ret = AtUartTransmit(uart, cmdString64bytes, AtStrlen(cmdString64bytes));
    if (ret != cAtOk)
        {
        AtDriverLog(AtDriverSharedDriverGet(), cAtLogLevelCritical, "%s", "ErrorInject: send command string in failure\r\n");
        return ret;
        }

    return ret;
    }

static uint32 RandomLinearAddress(ThaSemController self)
    {
    const uint32 cMaxLinearAddress =AtSemControllerMaxLfa((AtSemController)self);
    return (AtStdRand(AtStdSharedStdGet()) % (cMaxLinearAddress - 2)); /*force-able address is MaxLfa - 2*/
    }

static eAtRet ValidateByUart(AtSemController self)
    {
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eAtRet ForceErrorByUart(AtSemController self, eAtSemControllerCounterType errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    return cAtErrorModeNotSupport;
    }

static eAtRet AsyncForceErrorByUart(AtSemController self, eAtSemControllerCounterType errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    return cAtErrorModeNotSupport;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeNone(maxElapseTimeToMoveFsm);
    mEncodeNone(maxElapseTimeToInsertError);

    mEncodeNone(validatedAsyncState);
    mEncodeNone(validatedRetries);
    mEncodeNone(validatedRetrySubState);
    mEncodeNone(validatedCounterStartTime);
    mEncodeNone(cmdString64bytes);
    mEncodeNone(lfa);
    mEncodeNone(dwIdxInLfa);
    mEncodeNone(bitIdxInDwOfLfa);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaSemController);
    }

static void OverrideAtObject(AtSemController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtSemController(AtSemController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSemControllerOverride, mMethodsGet(self), sizeof(m_AtSemControllerOverride));

        mMethodOverride(m_AtSemControllerOverride, Validate);
        mMethodOverride(m_AtSemControllerOverride, CorrectedErrorCounterGet);
        mMethodOverride(m_AtSemControllerOverride, StatusGet);
        mMethodOverride(m_AtSemControllerOverride, AlarmGet);
        mMethodOverride(m_AtSemControllerOverride, AlarmHistoryGet);
        mMethodOverride(m_AtSemControllerOverride, AlarmHistoryClear);
        mMethodOverride(m_AtSemControllerOverride, IsActive);
        mMethodOverride(m_AtSemControllerOverride, MaxElapseTimeToInsertError);
        mMethodOverride(m_AtSemControllerOverride, MaxElapseTimeToMoveFsm);
        mMethodOverride(m_AtSemControllerOverride, ValidateByUart);
        mMethodOverride(m_AtSemControllerOverride, ForceErrorByUart);
        mMethodOverride(m_AtSemControllerOverride, AsyncForceErrorByUart);
        mMethodOverride(m_AtSemControllerOverride, NumSlr);
        mMethodOverride(m_AtSemControllerOverride, MaxLfa);
        mMethodOverride(m_AtSemControllerOverride, FrameSize);
        mMethodOverride(m_AtSemControllerOverride, GlobalLfaToLocalLfa);
        mMethodOverride(m_AtSemControllerOverride, GlobalLfaToSlrId);
        }

    mMethodsSet(self, &m_AtSemControllerOverride);
    }

static void Override(AtSemController self)
    {
    OverrideAtObject(self);
    OverrideAtSemController(self);
    }

static void MethodsInit(AtSemController self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, DefaultOffset);
        mMethodOverride(m_methods, NeedUseCmdCtrlDoneBit);
        mMethodOverride(m_methods, DefaultFrameWordAddress);
        mMethodOverride(m_methods, RandomLinearAddress);
        mMethodOverride(m_methods, ClearCorrectableSticky);
        mMethodOverride(m_methods, SemCorrectionResultGet);
        mMethodOverride(m_methods, AddressCommandString);
        mMethodOverride(m_methods, IsHwActiveDetected);
        mMethodOverride(m_methods, HeartBeatMaxTimeInUs);
        mMethodOverride(m_methods, ErrorInject);

        mBitFieldOverride(ThaSemController, m_methods, _sem_cur_heartbeat_)
        mBitFieldOverride(ThaSemController, m_methods, _sem_cur_essen_)
        mBitFieldOverride(ThaSemController, m_methods, _sem_cur_act_)
        mBitFieldOverride(ThaSemController, m_methods, _sem_cur_correctable_)
        mBitFieldOverride(ThaSemController, m_methods, _sem_cur_uncor_)
        mBitFieldOverride(ThaSemController, m_methods, _sem_cur_fata_)
        mBitFieldOverride(ThaSemController, m_methods, _sem_cur_injec_)
        mBitFieldOverride(ThaSemController, m_methods, _sem_cur_idle_)
        mBitFieldOverride(ThaSemController, m_methods, _sem_cur_class_)
        mBitFieldOverride(ThaSemController, m_methods, _sem_cur_corr_)
        mBitFieldOverride(ThaSemController, m_methods, _sem_cur_obsv_)
        mBitFieldOverride(ThaSemController, m_methods, _sem_cur_init_)
        mBitFieldOverride(ThaSemController, m_methods, _sem_cur_heartbeat1_)
        mBitFieldOverride(ThaSemController, m_methods, _sem_cur_heartbeat2_)
        mBitFieldOverride(ThaSemController, m_methods, _sem_cur_act1_)
        mBitFieldOverride(ThaSemController, m_methods, _sem_cur_act2_)
        }

    mMethodsSet((ThaSemController)self, &m_methods);
    }

AtSemController ThaSemControllerObjectInit(AtSemController self, AtDevice device, uint32 semId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtSemControllerObjectInit(self, device, semId) == NULL)
        return NULL;

    /* Setup methods */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtSemController ThaSemControllerNew(AtDevice device, uint32 semId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSemController controller = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (controller == NULL)
        return NULL;

    /* Construct it */
    return ThaSemControllerObjectInit(controller, device, semId);
    }

uint32 ThaSemControllerDefaultOffset(ThaSemController self)
    {
    if (self)
        return mMethodsGet(self)->DefaultOffset(self);
    return 0;
    }

uint32 ThaSemControllerCurrentAlarmRegister(ThaSemController self)
    {
    AtUnused(self);
    return cSeuPciStatus;
    }

uint32 ThaSemControllerHistoryAlarmRegister(ThaSemController self)
    {
    AtUnused(self);
    return cSeuPciAlarm;
    }

eBool ThaSemControllerMoveToState(AtSemController self, uint8 stateValue)
    {
    return SeuFsmMoveToState(self, stateValue);
    }

uint32 ThaSemControllerRandomLinearAddress(AtSemController self)
    {
    if (self)
        return mMethodsGet(mThis(self))->RandomLinearAddress((ThaSemController)self);
    return 0;
    }

eAtRet ThaSemControllerAddressCommandString(ThaSemController self, char *addressString, uint32 numChar, uint32 lfa, uint32 dwordIdxInLfa, uint32 bitIdxInDword)
    {
    if (self)
        return mMethodsGet(mThis(self))->AddressCommandString(self, addressString, numChar, lfa, dwordIdxInLfa, bitIdxInDword);
    return cAtErrorNullPointer;
    }

eAtRet ThaSemControllerErrorInject(ThaSemController self, uint32 lfa, uint32 dwIdx, uint32 bitIdx, char *cmdString64bytes)
    {
    if (self)
        return mMethodsGet(self)->ErrorInject(self, lfa, dwIdx, bitIdx, cmdString64bytes);
    return cAtErrorNullPointer;
    }

eAtRet ThaSemControllerOnlyErrorInject(ThaSemController self, uint32 lfa, uint32 dwIdx, uint32 bitIdx, char *cmdString64bytes)
    {
    return ErrorInject(self, lfa, dwIdx, bitIdx, cmdString64bytes);
    }

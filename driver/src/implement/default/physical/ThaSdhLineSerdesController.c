/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : ThaSdhLineSerdesController.c
 *
 * Created Date: Aug 16, 2013
 *
 * Description : SDH Line SERDES controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../sdh/ThaModuleSdh.h"
#include "ThaSdhLineSerdesControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cInvalidValue 0xCAFECAFE

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaSdhLineSerdesController)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaSdhLineSerdesControllerMethods m_methods;

/* Override */
static tAtSerdesControllerMethods  m_AtSerdesControllerOverride;
static tThaSerdesControllerMethods m_ThaSerdesControllerOverride;

/* Save super implementation */
static const tAtSerdesControllerMethods  *m_AtSerdesControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(ThaSerdesController self)
    {
	AtUnused(self);
    return 0xf28000;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaSdhLineSerdesController);
    }

static eAtRet Init(AtSerdesController self)
    {
    m_AtSerdesControllerMethods->Init(self);
    AtSerdesControllerPhysicalParamSet(self, cAtSerdesParamVod, 0x3f);
    AtSerdesControllerPhysicalParamSet(self, cAtSerdesParamPreEmphasisPreTap, 0x0);
    AtSerdesControllerPhysicalParamSet(self, cAtSerdesParamPreEmphasisFirstPostTap, 0x1);
    AtSerdesControllerPhysicalParamSet(self, cAtSerdesParamPreEmphasisSecondPostTap, 0x0);

    return cAtOk;
    }

static uint8 HwLineId(ThaSerdesController self)
    {
    return (uint8)ThaModuleSdhLineLocalId((AtSdhLine)AtSerdesControllerPhysicalPortGet((AtSerdesController)self));
    }

static uint32 LoopInRegAddress(ThaSerdesController self)
    {
    uint8 lineId = HwLineId(self);

    if ((lineId == 0) || (lineId == 1))
        return 0xf28861;

    if ((lineId == 2) || (lineId == 3))
        return (lineId == 2) ? 0xf29061 : 0xf29861;

    if ((lineId == 4) || (lineId == 5))
        return (lineId == 4) ? 0xf29061 : 0xf29861;

    return 0;
    }

static uint32 LoopInMask(ThaSerdesController self)
    {
    uint8 lineId = HwLineId(self);

    if ((lineId == 0) || (lineId == 1))
        return cBit0 << lineId;

    if ((lineId == 2) || (lineId == 3))
        return cBit0;

    if ((lineId == 4) || (lineId == 5))
        return cBit1;

    return cBit0;
    }

static uint32 LoopInShift(ThaSerdesController self)
    {
    uint8 lineId = HwLineId(self);

    /* Port 1 and 2 */
    if ((lineId == 0) || (lineId == 1))
        return lineId;

    /* Port 3 and 4 */
    if ((lineId == 2) || (lineId == 3))
        return 0;

    /* APS port (5, 6) */
    if ((lineId == 4) || (lineId == 5))
        return 1;

    return 0;
    }

static AtPrbsEngine PrbsEngineCreate(AtSerdesController self)
    {
    AtSdhLine line = (AtSdhLine)AtSerdesControllerPhysicalPortGet(self);
    ThaModuleSdh sdhModule = (ThaModuleSdh)AtChannelModuleGet((AtChannel)line);
    return ThaModuleSdhLineSerdesPrbsEngineCreate(sdhModule, line, self);
    }

static eAtModule ModuleId(AtSerdesController self)
    {
    AtModule module = AtChannelModuleGet(AtSerdesControllerPhysicalPortGet(self));
    return AtModuleTypeGet(module);
    }

static uint32 LockToDataRegAddress(ThaSdhLineSerdesController self)
    {
	AtUnused(self);
    /* Concrete device has it's own address */
    return cInvalidValue;
    }

static uint32 LockToDataRegMask(ThaSdhLineSerdesController self)
    {
	AtUnused(self);
    /* Concrete device has it's own address */
    return 0;
    }

static uint32 LockToDataRegShift(ThaSdhLineSerdesController self)
    {
	AtUnused(self);
    /* Concrete device has it's own address */
    return 0;
    }

static uint32 LockToRefRegAddress(ThaSdhLineSerdesController self)
    {
	AtUnused(self);
    /* Concrete device has it's own address */
    return cInvalidValue;
    }

static uint32 LockToRefRegMask(ThaSdhLineSerdesController self)
    {
	AtUnused(self);
    /* Concrete device has it's own address */
    return 0;
    }

static uint32 LockToRefRegShift(ThaSdhLineSerdesController self)
    {
	AtUnused(self);
    /* Concrete device has it's own address */
    return 0;
    }

static eAtRet TimingModeSet(AtSerdesController self, eAtSerdesTimingMode timingMode)
    {
    uint32 regVal, address, mask, shift;
    eAtModule moduleId = ModuleId(self);

    /* Lock to data register, mode is lock-to-data write 1, otherwise write 0 */
    address = mMethodsGet(mThis(self))->LockToDataRegAddress(mThis(self));
    mask    = mMethodsGet(mThis(self))->LockToDataRegMask(mThis(self));
    shift   = mMethodsGet(mThis(self))->LockToDataRegShift(mThis(self));

    regVal = AtSerdesControllerRead(self, address, moduleId);
    mFieldIns(&regVal, mask, shift, (timingMode == cAtSerdesTimingModeLockToData) ? 1 : 0);
    AtSerdesControllerWrite(self, address, regVal, moduleId);

    /* Lock to ref register, mode is lock-to-ref write 1, otherwise write 0 */
    address = mMethodsGet(mThis(self))->LockToRefRegAddress(mThis(self));
    mask    = mMethodsGet(mThis(self))->LockToRefRegMask(mThis(self));
    shift   = mMethodsGet(mThis(self))->LockToRefRegShift(mThis(self));

    regVal = AtSerdesControllerRead(self, address, moduleId);
    mFieldIns(&regVal, mask, shift, (timingMode == cAtSerdesTimingModeLockToRef) ? 1 : 0);
    AtSerdesControllerWrite(self, address, regVal, moduleId);

    return cAtOk;
    }

static eAtSerdesTimingMode TimingModeGet(AtSerdesController self)
    {
    uint32 regVal, address, mask, shift;
    eAtModule moduleId = ModuleId(self);
    uint8 bitVal;

    /* If lock to data */
    address = mMethodsGet(mThis(self))->LockToDataRegAddress(mThis(self));
    mask    = mMethodsGet(mThis(self))->LockToDataRegMask(mThis(self));
    shift   = mMethodsGet(mThis(self))->LockToDataRegShift(mThis(self));

    regVal = AtSerdesControllerRead(self, address, moduleId);
    mFieldGet(regVal, mask, shift, uint8, &bitVal);
    if (bitVal)
        return cAtSerdesTimingModeLockToData;

    /* If lock to ref */
    address = mMethodsGet(mThis(self))->LockToRefRegAddress(mThis(self));
    mask    = mMethodsGet(mThis(self))->LockToRefRegMask(mThis(self));
    shift   = mMethodsGet(mThis(self))->LockToRefRegShift(mThis(self));

    regVal = AtSerdesControllerRead(self, address, moduleId);
    mFieldGet(regVal, mask, shift, uint8, &bitVal);
    if (bitVal)
        return cAtSerdesTimingModeLockToRef;

    return cAtSerdesTimingModeAuto;
    }

static eAtRet RateSet(ThaSdhLineSerdesController self, eAtSdhLineRate rate)
    {
	AtUnused(rate);
	AtUnused(self);
    /* Let subclass do */
    return cAtErrorModeNotSupport;
    }

static eAtSdhLineRate RateGet(ThaSdhLineSerdesController self)
    {
	AtUnused(self);
    /* Let subclass do */
    return cAtSdhLineRateInvalid;
    }

static eAtRet ModeSet(AtSerdesController self, eAtSerdesMode mode)
    {
    AtUnused(self);
    return ((mode == cAtSerdesModeOcn) ? cAtOk : cAtErrorModeNotSupport);
    }

static eAtSerdesMode ModeGet(AtSerdesController self)
    {
    AtUnused(self);
    return cAtSerdesModeOcn;
	}

static AtEyeScanController EyeScanControllerObjectCreate(AtSerdesController self, uint32 drpBaseAddress)
    {
    return AtEyeScanControllerStmNew(self, drpBaseAddress);
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSerdesControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, m_AtSerdesControllerMethods, sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, Init);
        mMethodOverride(m_AtSerdesControllerOverride, PrbsEngineCreate);
        mMethodOverride(m_AtSerdesControllerOverride, TimingModeSet);
        mMethodOverride(m_AtSerdesControllerOverride, TimingModeGet);
        mMethodOverride(m_AtSerdesControllerOverride, ModeSet);
        mMethodOverride(m_AtSerdesControllerOverride, ModeGet);
        mMethodOverride(m_AtSerdesControllerOverride, EyeScanControllerObjectCreate);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void OverrideThaSerdesController(AtSerdesController self)
    {
    ThaSerdesController controller = (ThaSerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSerdesControllerOverride, mMethodsGet(controller), sizeof(m_ThaSerdesControllerOverride));

        mMethodOverride(m_ThaSerdesControllerOverride, BaseAddress);
        mMethodOverride(m_ThaSerdesControllerOverride, LoopInRegAddress);
        mMethodOverride(m_ThaSerdesControllerOverride, LoopInMask);
        mMethodOverride(m_ThaSerdesControllerOverride, LoopInShift);
        }

    mMethodsSet(controller, &m_ThaSerdesControllerOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtSerdesController(self);
    OverrideThaSerdesController(self);
    }

static void MethodsInit(ThaSdhLineSerdesController self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, LockToDataRegAddress);
        mMethodOverride(m_methods, LockToDataRegMask);
        mMethodOverride(m_methods, LockToDataRegShift);
        mMethodOverride(m_methods, LockToRefRegAddress);
        mMethodOverride(m_methods, LockToRefRegMask);
        mMethodOverride(m_methods, LockToRefRegShift);
        mMethodOverride(m_methods, RateSet);
        mMethodOverride(m_methods, RateGet);
        }

    mMethodsSet(self, &m_methods);
    }

AtSerdesController ThaSdhLineSerdesControllerObjectInit(AtSerdesController self, AtSdhLine sdhLine, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaSerdesControllerObjectInit(self, (AtChannel)sdhLine, serdesId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit((ThaSdhLineSerdesController)self);

    m_methodsInit = 1;

    return self;
    }

AtSerdesController ThaSdhLineSerdesControllerNew(AtSdhLine sdhLine, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ThaSdhLineSerdesControllerObjectInit(newController, sdhLine, serdesId);
    }

eAtRet ThaSdhLineSerdesControllerRateSet(ThaSdhLineSerdesController self, eAtSdhLineRate rate)
    {
    if (self)
        return mMethodsGet(self)->RateSet(self, rate);

    return cAtError;
    }

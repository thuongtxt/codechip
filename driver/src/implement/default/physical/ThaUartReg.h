/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAN
 * 
 * File        : ThaUartReg.h
 * 
 * Created Date: Apr 20, 2016
 *
 * Description : ThaUartReg declarations
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAUARTREG_H_
#define _THAUARTREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*------------------------------------------------------------------------------
Reg Name   : SEM Monitor Interface Status FiFo TX Side
Reg Addr   : 0x011-0x211
Reg Formula: 0x011+$slrid*0x100+0x11
    Where  :
           + $slrid(0-2) : Hardware SLR
Reg Desc   :
Monitor Interface bus at TX side of each SLR

------------------------------------------------------------------------------*/
#define cReg_sem_mon_sta_Base                                                                              0x11
#define cReg_sem_mon_sta_WidthVal                                                                           32
#define cReg_sem_mon_sta_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: mon_tx_ctrl_trig
BitField Type: R/W
BitField Desc: Trigger 0->1 to start reseting FiFo
BitField Bits: [31:31]
--------------------------------------*/
#define c_sem_mon_sta_mon_tx_ctrl_trig_Bit_Start                                                            31
#define c_sem_mon_sta_mon_tx_ctrl_trig_Bit_End                                                              31
#define c_sem_mon_sta_mon_tx_ctrl_trig_Mask                                                             cBit31
#define c_sem_mon_sta_mon_tx_ctrl_trig_Shift                                                                31
#define c_sem_mon_sta_mon_tx_ctrl_trig_MaxVal                                                              0x1
#define c_sem_mon_sta_mon_tx_ctrl_trig_MinVal                                                              0x0
#define c_sem_mon_sta_mon_tx_ctrl_trig_RstVal                                                              0x0

/*--------------------------------------
BitField Name: mon_tx_eful_stk
BitField Type: W1C
BitField Desc: Sticky Commnon TX fifo is early full
BitField Bits: [23:23]
--------------------------------------*/
#define c_sem_mon_sta_mon_tx_eful_stk_Bit_Start                                                             23
#define c_sem_mon_sta_mon_tx_eful_stk_Bit_End                                                               23
#define c_sem_mon_sta_mon_tx_eful_stk_Mask                                                              cBit23
#define c_sem_mon_sta_mon_tx_eful_stk_Shift                                                                 23
#define c_sem_mon_sta_mon_tx_eful_stk_MaxVal                                                               0x1
#define c_sem_mon_sta_mon_tx_eful_stk_MinVal                                                               0x0
#define c_sem_mon_sta_mon_tx_eful_stk_RstVal                                                               0x0

/*--------------------------------------
BitField Name: mon_tx_full_stk
BitField Type: W1C
BitField Desc: Sticky Common TX fifo is full
BitField Bits: [22:22]
--------------------------------------*/
#define c_sem_mon_sta_mon_tx_full_stk_Bit_Start                                                             22
#define c_sem_mon_sta_mon_tx_full_stk_Bit_End                                                               22
#define c_sem_mon_sta_mon_tx_full_stk_Mask                                                              cBit22
#define c_sem_mon_sta_mon_tx_full_stk_Shift                                                                 22
#define c_sem_mon_sta_mon_tx_full_stk_MaxVal                                                               0x1
#define c_sem_mon_sta_mon_tx_full_stk_MinVal                                                               0x0
#define c_sem_mon_sta_mon_tx_full_stk_RstVal                                                               0x0

/*--------------------------------------
BitField Name: mon_tx_ept_stk
BitField Type: W1C
BitField Desc: Sticky Common TX fifo is empty
BitField Bits: [21:21]
--------------------------------------*/
#define c_sem_mon_sta_mon_tx_ept_stk_Bit_Start                                                              21
#define c_sem_mon_sta_mon_tx_ept_stk_Bit_End                                                                21
#define c_sem_mon_sta_mon_tx_ept_stk_Mask                                                               cBit21
#define c_sem_mon_sta_mon_tx_ept_stk_Shift                                                                  21
#define c_sem_mon_sta_mon_tx_ept_stk_MaxVal                                                                0x1
#define c_sem_mon_sta_mon_tx_ept_stk_MinVal                                                                0x0
#define c_sem_mon_sta_mon_tx_ept_stk_RstVal                                                                0x0

/*--------------------------------------
BitField Name: mon_tx_nept_stk
BitField Type: W1C
BitField Desc: Sticky Common TX fifo is not empty
BitField Bits: [20:20]
--------------------------------------*/
#define c_sem_mon_sta_mon_tx_nept_stk_Bit_Start                                                             20
#define c_sem_mon_sta_mon_tx_nept_stk_Bit_End                                                               20
#define c_sem_mon_sta_mon_tx_nept_stk_Mask                                                              cBit20
#define c_sem_mon_sta_mon_tx_nept_stk_Shift                                                                 20
#define c_sem_mon_sta_mon_tx_nept_stk_MaxVal                                                               0x1
#define c_sem_mon_sta_mon_tx_nept_stk_MinVal                                                               0x0
#define c_sem_mon_sta_mon_tx_nept_stk_RstVal                                                               0x0

/*--------------------------------------
BitField Name: mon_tx_eful_cur
BitField Type: R_O
BitField Desc: Current Commnon TX fifo is early full
BitField Bits: [19:19]
--------------------------------------*/
#define c_sem_mon_sta_mon_tx_eful_cur_Bit_Start                                                             19
#define c_sem_mon_sta_mon_tx_eful_cur_Bit_End                                                               19
#define c_sem_mon_sta_mon_tx_eful_cur_Mask                                                              cBit19
#define c_sem_mon_sta_mon_tx_eful_cur_Shift                                                                 19
#define c_sem_mon_sta_mon_tx_eful_cur_MaxVal                                                               0x1
#define c_sem_mon_sta_mon_tx_eful_cur_MinVal                                                               0x0
#define c_sem_mon_sta_mon_tx_eful_cur_RstVal                                                               0x0

/*--------------------------------------
BitField Name: mon_tx_full_cur
BitField Type: R_O
BitField Desc: Current Common TX fifo is full
BitField Bits: [18:18]
--------------------------------------*/
#define c_sem_mon_sta_mon_tx_full_cur_Bit_Start                                                             18
#define c_sem_mon_sta_mon_tx_full_cur_Bit_End                                                               18
#define c_sem_mon_sta_mon_tx_full_cur_Mask                                                              cBit18
#define c_sem_mon_sta_mon_tx_full_cur_Shift                                                                 18
#define c_sem_mon_sta_mon_tx_full_cur_MaxVal                                                               0x1
#define c_sem_mon_sta_mon_tx_full_cur_MinVal                                                               0x0
#define c_sem_mon_sta_mon_tx_full_cur_RstVal                                                               0x0

/*--------------------------------------
BitField Name: mon_tx_ept_cur
BitField Type: R_O
BitField Desc: Current Common TX fifo is empty
BitField Bits: [17:17]
--------------------------------------*/
#define c_sem_mon_sta_mon_tx_ept_cur_Bit_Start                                                              17
#define c_sem_mon_sta_mon_tx_ept_cur_Bit_End                                                                17
#define c_sem_mon_sta_mon_tx_ept_cur_Mask                                                               cBit17
#define c_sem_mon_sta_mon_tx_ept_cur_Shift                                                                  17
#define c_sem_mon_sta_mon_tx_ept_cur_MaxVal                                                                0x1
#define c_sem_mon_sta_mon_tx_ept_cur_MinVal                                                                0x0
#define c_sem_mon_sta_mon_tx_ept_cur_RstVal                                                                0x0

/*--------------------------------------
BitField Name: mon_tx_nept_cur
BitField Type: R_O
BitField Desc: Current Common TX fifo is not empty
BitField Bits: [16:16]
--------------------------------------*/
#define c_sem_mon_sta_mon_tx_nept_cur_Bit_Start                                                             16
#define c_sem_mon_sta_mon_tx_nept_cur_Bit_End                                                               16
#define c_sem_mon_sta_mon_tx_nept_cur_Mask                                                              cBit16
#define c_sem_mon_sta_mon_tx_nept_cur_Shift                                                                 16
#define c_sem_mon_sta_mon_tx_nept_cur_MaxVal                                                               0x1
#define c_sem_mon_sta_mon_tx_nept_cur_MinVal                                                               0x0
#define c_sem_mon_sta_mon_tx_nept_cur_RstVal                                                               0x0

/*--------------------------------------
BitField Name: mon_tx_len_cur
BitField Type: R_O
BitField Desc: Current length of Common fifo
BitField Bits: [11:00]
--------------------------------------*/
#define c_sem_mon_sta_mon_tx_len_cur_Bit_Start                                                               0
#define c_sem_mon_sta_mon_tx_len_cur_Bit_End                                                                11
#define c_sem_mon_sta_mon_tx_len_cur_Mask                                                             cBit11_0
#define c_sem_mon_sta_mon_tx_len_cur_Shift                                                                   0
#define c_sem_mon_sta_mon_tx_len_cur_MaxVal                                                              0xfff
#define c_sem_mon_sta_mon_tx_len_cur_MinVal                                                                0x0
#define c_sem_mon_sta_mon_tx_len_cur_RstVal                                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : SEM Monitor Interface Value FiFo TX Side
Reg Addr   : 0x012-0x212
Reg Formula: 0x012+$slrid*0x100+0x12
    Where  :
           + $slrid(0-2) : Hardware SLR
Reg Desc   :
Monitor Interface bus at TX side of each SLR

------------------------------------------------------------------------------*/
#define cReg_sem_mon_val_Base                                                                             0x12
#define cReg_sem_mon_val_WidthVal                                                                           32
#define cReg_sem_mon_val_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: mon_tx_val
BitField Type: R_O
BitField Desc: Status of SEM controller, ASCII format, you have to monitor
status of FiFo TX to get valid value
BitField Bits: [07:00]
--------------------------------------*/
#define c_sem_mon_val_mon_tx_val_Bit_Start                                                                   0
#define c_sem_mon_val_mon_tx_val_Bit_End                                                                     7
#define c_sem_mon_val_mon_tx_val_Mask                                                                  cBit7_0
#define c_sem_mon_val_mon_tx_val_Shift                                                                       0
#define c_sem_mon_val_mon_tx_val_MaxVal                                                                   0xff
#define c_sem_mon_val_mon_tx_val_MinVal                                                                    0x0
#define c_sem_mon_val_mon_tx_val_RstVal                                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : SEM Monitor Interface Control RX Side
Reg Addr   : 0x013-0x213
Reg Formula: 0x013+$slrid*0x100+0x13
    Where  :
           + $slrid(0-2) : Hardware SLR
Reg Desc   :
Monitor Interface bus at RX side of each SLR

------------------------------------------------------------------------------*/
#define cReg_sem_mon_ctrl_Base                                                                           0x13
#define cReg_sem_mon_ctrl_WidthVal                                                                          32
#define cReg_sem_mon_ctrl_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: mon_rx_ctrl_done
BitField Type: W1C
BitField Desc: Trigger is done
BitField Bits: [09:09]
--------------------------------------*/
#define c_sem_mon_ctrl_mon_rx_ctrl_done_Bit_Start                                                            9
#define c_sem_mon_ctrl_mon_rx_ctrl_done_Bit_End                                                              9
#define c_sem_mon_ctrl_mon_rx_ctrl_done_Mask                                                             cBit9
#define c_sem_mon_ctrl_mon_rx_ctrl_done_Shift                                                                9
#define c_sem_mon_ctrl_mon_rx_ctrl_done_MaxVal                                                             0x1
#define c_sem_mon_ctrl_mon_rx_ctrl_done_MinVal                                                             0x0
#define c_sem_mon_ctrl_mon_rx_ctrl_done_RstVal                                                             0x0

/*--------------------------------------
BitField Name: mon_rx_ctrl_trig
BitField Type: R/W
BitField Desc: Trigger 1->0 to HW start sending command to SEM controller via
Monitor interface
BitField Bits: [08:08]
--------------------------------------*/
#define c_sem_mon_ctrl_mon_rx_ctrl_trig_Bit_Start                                                            8
#define c_sem_mon_ctrl_mon_rx_ctrl_trig_Bit_End                                                              8
#define c_sem_mon_ctrl_mon_rx_ctrl_trig_Mask                                                             cBit8
#define c_sem_mon_ctrl_mon_rx_ctrl_trig_Shift                                                                8
#define c_sem_mon_ctrl_mon_rx_ctrl_trig_MaxVal                                                             0x1
#define c_sem_mon_ctrl_mon_rx_ctrl_trig_MinVal                                                             0x0
#define c_sem_mon_ctrl_mon_rx_ctrl_trig_RstVal                                                             0x0

/*--------------------------------------
BitField Name: mon_rx_ctrl_len
BitField Type: R/W
BitField Desc: Command length
BitField Bits: [03:00]
--------------------------------------*/
#define c_sem_mon_ctrl_mon_rx_ctrl_len_Bit_Start                                                             0
#define c_sem_mon_ctrl_mon_rx_ctrl_len_Bit_End                                                               3
#define c_sem_mon_ctrl_mon_rx_ctrl_len_Mask                                                            cBit3_0
#define c_sem_mon_ctrl_mon_rx_ctrl_len_Shift                                                                 0
#define c_sem_mon_ctrl_mon_rx_ctrl_len_MaxVal                                                              0xf
#define c_sem_mon_ctrl_mon_rx_ctrl_len_MinVal                                                              0x0
#define c_sem_mon_ctrl_mon_rx_ctrl_len_RstVal                                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : SEM Monitor Interface Command Value Part0 RX Side
Reg Addr   : 0x014-0x214
Reg Formula: 0x014+$slrid*0x100+0x14
    Where  :
           + $slrid(0-2) : Hardware SLR
Reg Desc   :
Monitor Interface bus at RX side of each SLR

------------------------------------------------------------------------------*/
#define cReg_sem_mon_cmd_part0_Base                                                                       0x14
#define cReg_sem_mon_cmd_part0_WidthVal                                                                     32
#define cReg_sem_mon_cmd_part0_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: mon_rx_cmd_val0
BitField Type: R/W
BitField Desc: Command value part0, this is MSB, ASCII format
BitField Bits: [31:00]
--------------------------------------*/
#define c_sem_mon_cmd_part0_mon_rx_cmd_val0_Bit_Start                                                        0
#define c_sem_mon_cmd_part0_mon_rx_cmd_val0_Bit_End                                                         31
#define c_sem_mon_cmd_part0_mon_rx_cmd_val0_Mask                                                      cBit31_0
#define c_sem_mon_cmd_part0_mon_rx_cmd_val0_Shift                                                            0
#define c_sem_mon_cmd_part0_mon_rx_cmd_val0_MaxVal                                                  0xffffffff
#define c_sem_mon_cmd_part0_mon_rx_cmd_val0_MinVal                                                         0x0
#define c_sem_mon_cmd_part0_mon_rx_cmd_val0_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : SEM Monitor Interface Command Value Part1 RX Side
Reg Addr   : 0x015-0x215
Reg Formula: 0x015+$slrid*0x100+0x15
    Where  :
           + $slrid(0-2) : Hardware SLR
Reg Desc   :
Monitor Interface bus at RX side of each SLR

------------------------------------------------------------------------------*/
#define cReg_sem_mon_cmd_part1(slrid)                                               (0x015+(slrid)*0x100+0x15)
#define cReg_sem_mon_cmd_part1_WidthVal                                                                     32
#define cReg_sem_mon_cmd_part1_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: mon_rx_cmd_val1
BitField Type: R/W
BitField Desc: Command value part1, it is followed Part0, ASCII format
BitField Bits: [31:00]
--------------------------------------*/
#define c_sem_mon_cmd_part1_mon_rx_cmd_val1_Bit_Start                                                        0
#define c_sem_mon_cmd_part1_mon_rx_cmd_val1_Bit_End                                                         31
#define c_sem_mon_cmd_part1_mon_rx_cmd_val1_Mask                                                      cBit31_0
#define c_sem_mon_cmd_part1_mon_rx_cmd_val1_Shift                                                            0
#define c_sem_mon_cmd_part1_mon_rx_cmd_val1_MaxVal                                                  0xffffffff
#define c_sem_mon_cmd_part1_mon_rx_cmd_val1_MinVal                                                         0x0
#define c_sem_mon_cmd_part1_mon_rx_cmd_val1_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : SEM Monitor Interface Command Value Part2 RX Side
Reg Addr   : 0x016-0x216
Reg Formula: 0x016+$slrid*0x100+0x16
    Where  :
           + $slrid(0-2) : Hardware SLR
Reg Desc   :
Monitor Interface bus at RX side of each SLR

------------------------------------------------------------------------------*/
#define cReg_sem_mon_cmd_part2(slrid)                                               (0x016+(slrid)*0x100+0x16)
#define cReg_sem_mon_cmd_part2_WidthVal                                                                     32
#define cReg_sem_mon_cmd_part2_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: mon_rx_cmd_val2
BitField Type: R/W
BitField Desc: Command value part2, it is followed Part1, ASCII format
BitField Bits: [31:00]
--------------------------------------*/
#define c_sem_mon_cmd_part2_mon_rx_cmd_val2_Bit_Start                                                        0
#define c_sem_mon_cmd_part2_mon_rx_cmd_val2_Bit_End                                                         31
#define c_sem_mon_cmd_part2_mon_rx_cmd_val2_Mask                                                      cBit31_0
#define c_sem_mon_cmd_part2_mon_rx_cmd_val2_Shift                                                            0
#define c_sem_mon_cmd_part2_mon_rx_cmd_val2_MaxVal                                                  0xffffffff
#define c_sem_mon_cmd_part2_mon_rx_cmd_val2_MinVal                                                         0x0
#define c_sem_mon_cmd_part2_mon_rx_cmd_val2_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : SEM Monitor Interface Command Value Part3 RX Side
Reg Addr   : 0x017-0x217
Reg Formula: 0x017+$slrid*0x100+0x17
    Where  :
           + $slrid(0-2) : Hardware SLR
Reg Desc   :
Monitor Interface bus at RX side of each SLR

------------------------------------------------------------------------------*/
#define cReg_sem_mon_cmd_part3(slrid)                                               (0x017+(slrid)*0x100+0x17)
#define cReg_sem_mon_cmd_part3_WidthVal                                                                     32
#define cReg_sem_mon_cmd_part3_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: mon_rx_cmd_val3
BitField Type: R/W
BitField Desc: Command value part3, it is followed Part2, this is LSB part,
ASCII format
BitField Bits: [31:00]
--------------------------------------*/
#define c_sem_mon_cmd_part3_mon_rx_cmd_val3_Bit_Start                                                        0
#define c_sem_mon_cmd_part3_mon_rx_cmd_val3_Bit_End                                                         31
#define c_sem_mon_cmd_part3_mon_rx_cmd_val3_Mask                                                      cBit31_0
#define c_sem_mon_cmd_part3_mon_rx_cmd_val3_Shift                                                            0
#define c_sem_mon_cmd_part3_mon_rx_cmd_val3_MaxVal                                                  0xffffffff
#define c_sem_mon_cmd_part3_mon_rx_cmd_val3_MinVal                                                         0x0
#define c_sem_mon_cmd_part3_mon_rx_cmd_val3_RstVal                                                         0x0

#ifdef __cplusplus
}
#endif
#endif /* _THAUARTREG_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PHYSICAL
 * 
 * File        : ThaSemControllerInternal.h
 * 
 * Created Date: Dec 10, 2015
 *
 * Description : Default SEM controller representation.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef THASEMCONTROLLERINTERNAL_H_
#define THASEMCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../util/ThaUtil.h"
#include "../../../generic/physical/AtSemControllerInternal.h"
#include "ThaSemController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cFsmOBSERVATION                 0xA
#define cFsmLINEAR                      0xC
#define cFsmIDLE                        0xE

/*--------------------------- Macros -----------------------------------------*/
#define mDefaultOffset(self)    (ThaSemControllerDefaultOffset((ThaSemController)self))

#define mFieldMask(module, field)                                              \
        mMethodsGet(module)->field##Mask(module)
#define mFieldShift(module, field)                                             \
        mMethodsGet(module)->field##Shift(module)

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaSemControllerMethods
    {
    eAtRet (*AddressCommandString)(ThaSemController self, char *addressString, uint32 numChar, uint32 lfa, uint32 dwordIdx, uint32 bitIdx);
    eAtRet (*ErrorInject)(ThaSemController self, uint32 lfa, uint32 dwIdx, uint32 bitIdx, char *cmdString64bytes);
    uint32 (*DefaultOffset)(ThaSemController self);
    eBool (*NeedUseCmdCtrlDoneBit)(ThaSemController self);
    uint32 (*DefaultFrameWordAddress)(ThaSemController self);
    uint32 (*RandomLinearAddress)(ThaSemController self);
    void (*ClearCorrectableSticky)(ThaSemController self);
    eAtRet (*SemCorrectionResultGet)(ThaSemController self);
    eBool (*IsHwActiveDetected)(ThaSemController self);
    uint32 (*HeartBeatMaxTimeInUs)(ThaSemController self);

    /* bitmap */
    mDefineMaskShiftField(ThaSemController, _sem_cur_heartbeat_)
    mDefineMaskShiftField(ThaSemController, _sem_cur_correctable_)
    mDefineMaskShiftField(ThaSemController, _sem_cur_uncor_)
    mDefineMaskShiftField(ThaSemController, _sem_cur_essen_)

    mDefineMaskShiftField(ThaSemController, _sem_cur_diag_)
    mDefineMaskShiftField(ThaSemController, _sem_cur_act_)
    mDefineMaskShiftField(ThaSemController, _sem_cur_idle_)
    mDefineMaskShiftField(ThaSemController, _sem_cur_init_)
    mDefineMaskShiftField(ThaSemController, _sem_cur_injec_)
    mDefineMaskShiftField(ThaSemController, _sem_cur_class_)
    mDefineMaskShiftField(ThaSemController, _sem_cur_fata_)
    mDefineMaskShiftField(ThaSemController, _sem_cur_obsv_)
    mDefineMaskShiftField(ThaSemController, _sem_cur_corr_)

    mDefineMaskShiftField(ThaSemController, _sem_cur_heartbeat1_)
    mDefineMaskShiftField(ThaSemController, _sem_cur_heartbeat2_)
    mDefineMaskShiftField(ThaSemController, _sem_cur_act1_)
    mDefineMaskShiftField(ThaSemController, _sem_cur_act2_)
    }tThaSemControllerMethods;

typedef struct tThaSemController
    {
    tAtSemController super;
    const tThaSemControllerMethods * methods;

    /* For debugging */
    uint32 maxElapseTimeToMoveFsm;
    uint32 maxElapseTimeToInsertError;

    /* Async error force */
    uint32 validatedAsyncState;
    uint32 validatedRetries;
    uint32 validatedRetrySubState;
    tAtOsalCurTime validatedCounterStartTime;
    char cmdString64bytes[64];
    uint32 lfa;
    uint32 dwIdxInLfa;
    uint32 bitIdxInDwOfLfa;
    }tThaSemController;

typedef struct tThaSemControllerV2
    {
    tThaSemController super;
    }tThaSemControllerV2;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSemController ThaSemControllerObjectInit(AtSemController self, AtDevice device, uint32 semId);
AtSemController ThaSemControllerV3ObjectInit(AtSemController self, AtDevice device, uint32 semId);

void ThaSemControllerClearAlarm(ThaSemController self, uint32 hwAlarms);
uint32 ThaSemControllerCurrentAlarmRegister(ThaSemController self);
uint32 ThaSemControllerHistoryAlarmRegister(ThaSemController self);
uint32 ThaSemControllerRandomLinearAddress(AtSemController self);
eAtRet ThaSemControllerAddressCommandString(ThaSemController self, char *addressString, uint32 numChar, uint32 lfa, uint32 dwordIdxInLfa, uint32 bitIdxInDword);
eAtRet ThaSemControllerErrorInject(ThaSemController self, uint32 lfa, uint32 dwIdx, uint32 bitIdx, char *cmdString64bytes);
eAtRet ThaSemControllerOnlyErrorInject(ThaSemController self, uint32 lfa, uint32 dwIdx, uint32 bitIdx, char *cmdString64bytes);
eAtRet ThaSemControllerAsyncMoveToState(AtSemController self, eAtSemControllerAlarmType state);
eAtRet ThaSemControllerAsyncLfaErrorInject(AtSemController self, uint32 lfa, uint32 dwIdx, uint32 bitIdx, char *cmdString64bytes);
eAtRet ThaSemControllerAsyncWaitMoveToState(AtSemController self, eAtSemControllerAlarmType state);

#ifdef __cplusplus
}
#endif
#endif /* THASEMCONTROLLERINTERNAL_H_ */


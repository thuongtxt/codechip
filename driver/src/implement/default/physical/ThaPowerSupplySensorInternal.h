/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PHYSICAL
 * 
 * File        : ThaPowerSupplySensorInternal.h
 * 
 * Created Date: Oct 4, 2016
 *
 * Description : ThaPowerSupplySensorInternal declarations
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPOWERSUPPLYSENSORINTERNAL_H_
#define _THAPOWERSUPPLYSENSORINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaPowerSupplySensor.h"
#include "../../../generic/physical/AtPowerSupplySensorInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPowerSupplySensorMethods
    {
    uint32 (*BaseAddress)(ThaPowerSupplySensor self);
    uint32 (*DefaultLowerThreshold)(ThaPowerSupplySensor self, eAtPowerSupplyVoltage type);
    }tThaPowerSupplySensorMethods;

typedef struct tThaPowerSupplySensor
    {
    tAtPowerSupplySensor super;
    const tThaPowerSupplySensorMethods *methods;
    }tThaPowerSupplySensor;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPowerSupplySensor ThaPowerSupplySensorObjectInit(AtPowerSupplySensor self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAPOWERSUPPLYSENSORINTERNAL_H_ */


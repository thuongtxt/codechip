/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Module SDH
 *
 * File        : ThaSdhLineSerdesControllerCyclone4.c
 *
 * Created Date: Dec 23, 2013
 *
 * Description : Cylone4 SDH Line Serdes
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaSdhLineSerdesControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cConfigureModeReg         0xf40000
#define cConfigureModePhysical    0
#define cConfigureModeReconfigure 1

#define cChannelIdReg             0xf40001

#define cParamIdReg               0xf40002
#define cParamValueReg            0xf40003

#define cRequestReg               0xf40004
#define cRequestModeWrite         1
#define cRequestModeRead          2

#define cReconfigureActionReg            0xf40005
#define cReconfigureActionPreCdrLoopIn   4
#define cReconfigureActionPreCdrLooppout 3
#define cReconfigureActionNoLoop         0

#define cInvalidValue 0xCAFECAFE

/* All hardware parameters */
#define cHwParamVod                   0
#define cHwParamPreTap                1
#define cHwParamRxEqualizationDcGain  3
#define cHwParamRxEqualizationControl 4

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tThaSdhLineSerdesControllerCyclone4 *)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSerdesControllerMethods  m_AtSerdesControllerOverride;
static tThaSerdesControllerMethods m_ThaSerdesControllerOverride;

/* Save super implementation */
static const tAtSerdesControllerMethods *m_AtSerdesControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaSdhLineSerdesControllerCyclone4);
    }

static uint32 HwParameterId(AtSerdesController self, eAtSerdesParam param)
    {
	AtUnused(self);
    if (param == cAtSerdesParamVod)                   return cHwParamVod;
    if (param == cAtSerdesParamPreEmphasisPreTap)     return cHwParamPreTap;
    if (param == cAtSerdesParamRxEqualizationDcGain)  return cHwParamRxEqualizationDcGain;
    if (param == cAtSerdesParamRxEqualizationControl) return cHwParamRxEqualizationControl;

    /* Default is param rxEqualizationControl */
    return 4;
    }

static eBool IsSimulated(AtSerdesController self)
    {
    AtChannel physical = AtSerdesControllerPhysicalPortGet(self);
    return AtDeviceIsSimulated(AtChannelDeviceGet(physical));
    }

static eBool HwDone(AtSerdesController self)
    {
    tAtOsalCurTime startTime, curTime;
    uint32 elapseTime = 0;
    AtOsal osal = AtSharedDriverOsalGet();
    const uint32 cTimeoutMs = 10;

    mMethodsGet(osal)->CurTimeGet(osal, &startTime);
    while (elapseTime < cTimeoutMs)
        {
        /* Done */
        uint32 regVal = AtSerdesControllerRead(self, cRequestReg, cAtModuleSdh);

        if (IsSimulated(self))
            return cAtTrue;

        if (regVal == 0)
            return cAtTrue;

        /* Continue waiting */
        mMethodsGet(osal)->CurTimeGet(osal, &curTime);
        elapseTime = mTimeIntervalInMsGet(startTime, curTime);
        }

    /* Timeout */
    return cAtFalse;
    }

static void HwParamsReCache(ThaSerdesController self)
    {
    mThis(self)->vod    = mMethodsGet(self)->HwPhysicalParamGet(self, cHwParamVod);
    mThis(self)->preTap = mMethodsGet(self)->HwPhysicalParamGet(self, cHwParamPreTap);
    mThis(self)->rxEqualizationDcGain  = mMethodsGet(self)->HwPhysicalParamGet(self, cHwParamRxEqualizationDcGain);
    mThis(self)->rxEqualizationControl = mMethodsGet(self)->HwPhysicalParamGet(self, cHwParamRxEqualizationControl);
    }

static void HwParamsCacheWrite(ThaSerdesController self)
    {
    AtSerdesController controller = (AtSerdesController)self;

    AtSerdesControllerWrite(controller, cParamIdReg, cHwParamVod, cAtModuleSdh);
    AtSerdesControllerWrite(controller, cParamValueReg, mThis(self)->vod, cAtModuleSdh);
    AtSerdesControllerWrite(controller, cParamIdReg, cHwParamPreTap, cAtModuleSdh);
    AtSerdesControllerWrite(controller, cParamValueReg, mThis(self)->preTap, cAtModuleSdh);
    AtSerdesControllerWrite(controller, cParamIdReg, cHwParamRxEqualizationDcGain, cAtModuleSdh);
    AtSerdesControllerWrite(controller, cParamValueReg, mThis(self)->rxEqualizationDcGain, cAtModuleSdh);
    AtSerdesControllerWrite(controller, cParamIdReg, cHwParamRxEqualizationControl, cAtModuleSdh);
    AtSerdesControllerWrite(controller, cParamValueReg, mThis(self)->rxEqualizationControl, cAtModuleSdh);
    }

static void HwPhysicalParamRequestStart(ThaSerdesController self)
    {
    AtSerdesController controller = (AtSerdesController)self;

    AtSerdesControllerWrite(controller, cConfigureModeReg, cConfigureModePhysical, cAtModuleSdh);
    AtSerdesControllerWrite(controller, cChannelIdReg, AtChannelIdGet(AtSerdesControllerPhysicalPortGet(controller)), cAtModuleSdh);
    }

static eAtRet HwPhysicalParamRequestFinish(ThaSerdesController self)
    {
    AtSerdesController controller = (AtSerdesController)self;
    AtSerdesControllerWrite(controller, cRequestReg, cRequestModeWrite, cAtModuleSdh);
    return HwDone(controller) ? cAtOk : cAtErrorDevBusy;
    }

static eAtRet HwParamsCacheApply(ThaSerdesController self)
    {
    HwPhysicalParamRequestStart(self);
    HwParamsCacheWrite(self);
    return HwPhysicalParamRequestFinish(self);
    }

static eAtRet HwPhysicalParamSet(ThaSerdesController self, uint32 hwParam, uint32 value)
    {
    AtSerdesController controller = (AtSerdesController)self;
    eAtRet ret;
    AtOsal osal = AtSharedDriverOsalGet();

    /* Cache then make request */
    HwParamsReCache(self);
    HwPhysicalParamRequestStart(self);

    /* As required by hardware, need to write all cache values first */
    HwParamsCacheWrite(self);

    /* Then this parameter */
    AtSerdesControllerWrite(controller, cParamIdReg, hwParam, cAtModuleSdh);
    AtSerdesControllerWrite(controller, cParamValueReg, value, cAtModuleSdh);

    /* Make request */
    ret = HwPhysicalParamRequestFinish(self);

    /* Give hardware a moment to update hardware */
    mMethodsGet(osal)->USleep(osal, 500);

    return ret;
    }

static uint32 HwPhysicalParamGet(ThaSerdesController self, uint32 hwParam)
    {
    AtSerdesController controller = (AtSerdesController)self;

    AtSerdesControllerWrite(controller, cConfigureModeReg, cConfigureModePhysical, cAtModuleSdh);
    AtSerdesControllerWrite(controller, cChannelIdReg, AtChannelIdGet(AtSerdesControllerPhysicalPortGet(controller)), cAtModuleSdh);
    AtSerdesControllerWrite(controller, cParamIdReg, hwParam, cAtModuleSdh);
    AtSerdesControllerWrite(controller, cRequestReg, cRequestModeRead, cAtModuleSdh);

    if (HwDone(controller))
        return AtSerdesControllerRead(controller, cParamValueReg, cAtModuleSdh);
    else
        return cInvalidValue;
    }

static eAtRet Init(AtSerdesController self)
    {
	AtUnused(self);
    return cAtOk;
    }

static eAtRet Reconfigure(ThaSerdesController self, uint32 hwAction)
    {
    AtSerdesController controller = (AtSerdesController)self;
    eAtRet ret = cAtOk;
    AtOsal osal = AtSharedDriverOsalGet();

    /* Need to re-cache to recover later */
    HwParamsReCache(self);

    /* Run the reconfigure sequence */
    AtSerdesControllerWrite(controller, cConfigureModeReg, cConfigureModeReconfigure, cAtModuleSdh);
    AtSerdesControllerWrite(controller, cChannelIdReg, AtChannelIdGet(AtSerdesControllerPhysicalPortGet(controller)), cAtModuleSdh);
    AtSerdesControllerWrite(controller, cReconfigureActionReg, hwAction, cAtModuleSdh);
    AtSerdesControllerWrite(controller, cRequestReg, cRequestModeWrite, cAtModuleSdh);
    ret = HwDone(controller) ? cAtOk : cAtErrorDevBusy;

    /* Give hardware a moment to update ROM */
    mMethodsGet(osal)->USleep(osal, 1000);

    /* And restore cache */
    HwParamsCacheApply(self);

    return ret;
    }

static eAtRet LoopInEnable(ThaSerdesController self, eBool enable)
    {
    uint8 loopback = enable ? cReconfigureActionPreCdrLoopIn : cReconfigureActionNoLoop;
    eAtRet ret = Reconfigure(self, loopback);
    if (ret == cAtOk)
        mThis(self)->loopinEnabled = enable;

    return ret;
    }

static eAtRet LoopOutHwEnable(ThaSerdesController self, eBool enable)
    {
    uint8 loopback = enable ? cReconfigureActionPreCdrLooppout : cReconfigureActionNoLoop;
    return Reconfigure(self, loopback);
    }

static eBool LoopInIsEnabled(ThaSerdesController self)
    {
    return mThis(self)->loopinEnabled;
    }

static eAtRet Enable(AtSerdesController self, eBool enable)
    {
	AtUnused(self);
    return enable ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool IsEnabled(AtSerdesController self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static eBool CanEnable(AtSerdesController self, eBool enable)
    {
    AtUnused(self);
    return enable ? cAtTrue : cAtFalse;
    }

static eBool PhysicalParamIsSupported(AtSerdesController self, eAtSerdesParam param)
    {
	AtUnused(self);
    if ((param == cAtSerdesParamVod)                  ||
        (param == cAtSerdesParamPreEmphasisPreTap)    ||
        (param == cAtSerdesParamRxEqualizationDcGain) ||
        (param == cAtSerdesParamRxEqualizationControl))
        return cAtTrue;

    return cAtFalse;
    }

static eBool PhysicalParamCanRead(ThaSerdesController self, eAtSerdesParam param)
    {
    return AtSerdesControllerPhysicalParamIsSupported((AtSerdesController)self, param);
    }

static uint32 *VodValidValues(uint8 *numValidValue)
    {
    static uint32 validValues[] = {0x1, 0x2, 0x3, 0x4, 0x5, 0x7};
    *numValidValue = mCount(validValues);
    return validValues;
    }

static uint32 *PreEmphasisPreTapValidValues(uint8 *numValidValue)
    {
    static uint32 validValues[] = {0x1, 0x5, 0x9, 0xD, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15};
    *numValidValue = mCount(validValues);
    return validValues;
    }

static uint32 *RxEqualizationDcGainValidValues(uint8 *numValidValue)
    {
    static uint32 validValues[] = {0, 1, 3};
    *numValidValue = mCount(validValues);
    return validValues;
    }

static uint32 *RxEqualizationControlValidValues(uint8 *numValidValue)
    {
    static uint32 validValues[] = {1, 4, 5, 7};
    *numValidValue = mCount(validValues);
    return validValues;
    }

static uint32 *ValidValuesOfParam(ThaSerdesController self, eAtSerdesParam param, uint8 *numValidValues)
    {
    if (param == cAtSerdesParamVod)
        return VodValidValues(numValidValues);
    if (param == cAtSerdesParamPreEmphasisPreTap)
        return PreEmphasisPreTapValidValues(numValidValues);
    if (param == cAtSerdesParamRxEqualizationDcGain)
        return mMethodsGet(self)->RxEqualizationDcGainValidValues(numValidValues);
    if (param == cAtSerdesParamRxEqualizationControl)
        return RxEqualizationControlValidValues(numValidValues);

    *numValidValues = 0;
    return NULL;
    }

static eBool PhysicalParamValueIsInRange(ThaSerdesController self, eAtSerdesParam param, uint32 value)
    {
    uint8 numValidValues;
    uint32 *validValues = ValidValuesOfParam(self, param, &numValidValues);
    uint8 i;

    if (validValues == NULL)
        return cAtFalse;

    for (i = 0; i < numValidValues; i++)
        {
        if (value == validValues[i])
            return cAtTrue;
        }

    return cAtFalse;
    }

static void PrintValues(uint32 *values, uint8 numValues)
    {
    uint8 i;

    for (i = 0; i < numValues; i++)
        AtPrintc(cSevNormal, (i == 0) ? "0x%x" : ", 0x%x", values[i]);
    }

static void Debug(AtSerdesController self)
    {
    uint8 numValues;
    uint32 *validValues;

    m_AtSerdesControllerMethods->Debug(self);

    validValues = VodValidValues(&numValues);
    AtPrintc(cSevNormal, "* Valid VOD values: ");
    PrintValues(validValues, numValues);
    AtPrintc(cSevNormal, "\r\n");

    validValues = PreEmphasisPreTapValidValues(&numValues);
    AtPrintc(cSevNormal, "* Valid pre-tap values: ");
    PrintValues(validValues, numValues);
    AtPrintc(cSevNormal, "\r\n");

    validValues = RxEqualizationDcGainValidValues(&numValues);
    AtPrintc(cSevNormal, "* Valid rxEqualizationDcGain values: ");
    PrintValues(validValues, numValues);
    AtPrintc(cSevNormal, "\r\n");

    validValues = RxEqualizationControlValidValues(&numValues);
    AtPrintc(cSevNormal, "* Valid rxEqualizationControl values: ");
    PrintValues(validValues, numValues);
    AtPrintc(cSevNormal, "\r\n");
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSerdesControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, m_AtSerdesControllerMethods, sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, Init);
        mMethodOverride(m_AtSerdesControllerOverride, HwParameterId);
        mMethodOverride(m_AtSerdesControllerOverride, Enable);
        mMethodOverride(m_AtSerdesControllerOverride, IsEnabled);
        mMethodOverride(m_AtSerdesControllerOverride, CanEnable);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalParamIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, Debug);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void OverrideThaSerdesController(AtSerdesController self)
    {
    ThaSerdesController controller = (ThaSerdesController)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSerdesControllerOverride, mMethodsGet(controller), sizeof(m_ThaSerdesControllerOverride));

        mMethodOverride(m_ThaSerdesControllerOverride, HwPhysicalParamSet);
        mMethodOverride(m_ThaSerdesControllerOverride, HwPhysicalParamGet);
        mMethodOverride(m_ThaSerdesControllerOverride, LoopOutHwEnable);
        mMethodOverride(m_ThaSerdesControllerOverride, LoopInEnable);
        mMethodOverride(m_ThaSerdesControllerOverride, LoopInIsEnabled);
        mMethodOverride(m_ThaSerdesControllerOverride, PhysicalParamCanRead);
        mMethodOverride(m_ThaSerdesControllerOverride, PhysicalParamValueIsInRange);
        mMethodOverride(m_ThaSerdesControllerOverride, RxEqualizationDcGainValidValues);
        }

    mMethodsSet(controller, &m_ThaSerdesControllerOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtSerdesController(self);
    OverrideThaSerdesController(self);
    }

AtSerdesController ThaSdhLineSerdesControllerCyclone4ObjectInit(AtSerdesController self, AtSdhLine sdhLine, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaSdhLineSerdesControllerObjectInit(self, sdhLine, serdesId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController ThaSdhLineSerdesControllerCyclone4New(AtSdhLine sdhLine, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ThaSdhLineSerdesControllerCyclone4ObjectInit(newController, sdhLine, serdesId);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : ThaSerdesController.h
 * 
 * Created Date: Aug 16, 2013
 *
 * Description : Default SERDES controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASERDESCONTROLLER_H_
#define _THASERDESCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSerdesController.h"
#include "AtEthPort.h"
#include "AtSdhLine.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaSerdesController * ThaSerdesController;
typedef struct tThaSdhLineSerdesController * ThaSdhLineSerdesController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Default concretes */
AtSerdesController ThaEthPortSerdesControllerNew(AtEthPort ethPort, uint32 serdesId);
AtSerdesController ThaSdhLineSerdesControllerNew(AtSdhLine sdhLine, uint32 serdesId);
AtSerdesController ThaSdhLineSerdesControllerCyclone4New(AtSdhLine sdhLine, uint32 serdesId);
AtSerdesController ThaSdhLineSerdesControllerCyclone4V2New(AtSdhLine sdhLine, uint32 serdesId);

eAtRet ThaSerdesControllerDefaultParamSet(ThaSerdesController self, eAtSerdesParam param);

/* Product concretes */
AtSerdesController Tha60001031SdhLineSerdesControllerNew(AtSdhLine sdhLine, uint32 serdesId);
AtSerdesController Tha60030081SdhLineSerdesControllerNew(AtSdhLine sdhLine, uint32 serdesId);
AtSerdesController Tha60031021EthPortSerdesControllerNew(AtEthPort ethPort, uint32 serdesId);
AtSerdesController Tha60031031EthPort10GSerdesControllerNew(AtEthPort ethPort, uint32 serdesId);
AtSerdesController Tha60031031EthPortSerdesControllerNew(AtEthPort ethPort, uint32 serdesId);
AtSerdesController Tha60031031SdhLineSerdesControllerNew(AtSdhLine sdhLine, uint32 serdesId);
AtSerdesController Tha60035021EthPortSerdesControllerNew(AtEthPort ethPort, uint32 serdesId);
AtSerdesController Tha60035021SdhLineSerdesControllerCyclone4New(AtSdhLine sdhLine, uint32 serdesId);
AtSerdesController Tha60060011SdhLineSerdesControllerNew(AtSdhLine sdhLine, uint32 serdesId);
AtSerdesController Tha6007EthPortSerdesControllerNew(AtEthPort ethPort, uint32 serdesId);
AtSerdesController Tha60070041EthPortSerdesControllerNew(AtEthPort ethPort, uint32 serdesId);
AtSerdesController Tha60070061SdhLineSerdesControllerNew(AtSdhLine sdhLine, uint32 serdesId);
AtSerdesController Tha60150011SdhLineSerdesControllerNew(AtSdhLine sdhLine, uint32 serdesId);
AtSerdesController Tha60200011SdhLineSerdesControllerNew(AtSdhLine sdhLine, uint32 serdesId);
AtSerdesController Tha60210011SdhLineSerdesControllerNew(AtSdhLine sdhLine, uint32 serdesId);
AtSerdesController ThaStmPwProductEthPort10GSerdesControllerNew(AtEthPort ethPort, uint32 serdesId);
AtSerdesController ThaStmPwProductEthPortSerdesControllerNew(AtEthPort ethPort, uint32 serdesId);
AtSerdesController ThaStmPwProductSdhLineSerdesControllerNew(AtSdhLine sdhLine, uint32 serdesId);
AtSerdesController ThaPdhPwProductEthPortSerdesControllerNew(AtEthPort ethPort, uint32 serdesId);
AtSerdesController Tha60210012XauiSerdesControllerNew(AtEthPort ethPort, uint32 serdesId);

#ifdef __cplusplus
}
#endif
#endif /* _THASERDESCONTROLLER_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha6021EthSerdesManager.h
 * 
 * Created Date: Jul 18, 2015
 *
 * Description : SERDES manager
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6021ETHSERDESMANAGER_H_
#define _THA6021ETHSERDESMANAGER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleEth.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaEthSerdesManager * ThaEthSerdesManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleEth ThaEthSerdesManagerModuleGet(ThaEthSerdesManager self);
eAtRet ThaEthSerdesManagerSetup(ThaEthSerdesManager self);
eAtRet ThaEthSerdesManagerInit(ThaEthSerdesManager self);
eAtRet ThaEthSerdesManagerAllSerdesReset(ThaEthSerdesManager self);
uint32 ThaEthSerdesManagerNumSerdesController(ThaEthSerdesManager self);
AtSerdesController ThaEthSerdesManagerSerdesController(ThaEthSerdesManager self, uint32 serdesId);
AtSerdesController *ThaEthSerdesManagerSerdesControllersCreate(ThaEthSerdesManager self,
                                                   AtSerdesController (*SerdesControllerCreateFunc)(ThaEthSerdesManager, AtEthPort, uint32));
AtSerdesController ThaEthSerdesManagerControllerGet(ThaEthSerdesManager self, uint32 serdesId);
eBool ThaEthSerdesManagerCanControlEqualizer(ThaEthSerdesManager self, AtSerdesController serdes);
eBool ThaEthSerdesManagerSerdesIsUsed(ThaEthSerdesManager self, AtSerdesController serdesController);

/* XFI group */
eBool ThaEthSerdesManagerHasXfiGroups(ThaEthSerdesManager self);
uint32 ThaEthSerdesManagerNumXfiGroups(ThaEthSerdesManager self);
const uint32 *ThaEthSerdesManagerXfisForGroup(ThaEthSerdesManager self, uint32 groupId, uint32 *numPorts);
uint32 ThaEthSerdesManagerNumberOfCreatedSerdesControllerGet(ThaEthSerdesManager self);

/* Concrete managers */
ThaEthSerdesManager Tha60210011EthSerdesManagerNew(AtModuleEth ethModule);
ThaEthSerdesManager Tha60210021EthSerdesManagerNew(AtModuleEth ethModule);
ThaEthSerdesManager Tha60210031EthSerdesManagerNew(AtModuleEth ethModule);
ThaEthSerdesManager Tha60210012EthSerdesManagerNew(AtModuleEth ethModule);
ThaEthSerdesManager Tha60210051EthPortSerdesManagerNew(AtModuleEth ethModule);

#ifdef __cplusplus
}
#endif
#endif /* _THA6021ETHSERDESMANAGER_H_ */


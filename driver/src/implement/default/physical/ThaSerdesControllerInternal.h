/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : ThaSerdesControllerInternal.h
 * 
 * Created Date: Aug 16, 2013
 *
 * Description : Default SERDES controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASERDESCONTROLLERINTERNAL_H_
#define _THASERDESCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/common/AtChannelInternal.h" /* For read/write macros */
#include "../../../generic/physical/AtSerdesControllerInternal.h"
#include "../man/ThaDeviceInternal.h"
#include "ThaSerdesController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaSerdesControllerMethods
    {
    uint32 (*BaseAddress)(ThaSerdesController self);
    uint32 (*PartOffset)(ThaSerdesController self);

    eBool (*PhysicalParamCanRead)(ThaSerdesController self, eAtSerdesParam param);
    uint32 (*PhysicalParamCache)(ThaSerdesController self, eAtSerdesParam param);
    eBool (*PhysicalParamValueIsInRange)(ThaSerdesController self, eAtSerdesParam param, uint32 value);

    eAtRet (*HwPhysicalParamSet)(ThaSerdesController self, uint32 hwParam, uint32 value);
    uint32 (*HwPhysicalParamGet)(ThaSerdesController self, uint32 hwParam);

    uint32 *(*RxEqualizationDcGainValidValues)(uint8 *numValidValue);

    /* Default parameter */
    uint32 (*PhysicalParamDefaultValue)(ThaSerdesController self, eAtSerdesParam param);

    /* Loopin */
    eAtRet (*LoopInEnable)(ThaSerdesController self, eBool enable);
    eBool (*LoopInIsEnabled)(ThaSerdesController self);
    uint32 (*LoopInRegAddress)(ThaSerdesController self);
    uint32 (*LoopInMask)(ThaSerdesController self);
    uint32 (*LoopInShift)(ThaSerdesController self);

    /* Loopout */
    eAtRet (*LoopOutEnable)(ThaSerdesController self, eBool enable);
    eBool (*LoopOutIsEnabled)(ThaSerdesController self);
    eAtRet (*LoopOutHwEnable)(ThaSerdesController self, eBool enable);
    }tThaSerdesControllerMethods;

typedef struct tThaSerdesController
    {
    tAtSerdesController super;
    const tThaSerdesControllerMethods *methods;

    /* Private data */
    eBool loopoutEnabled;
    uint8 rxEqualizationControl;
    }tThaSerdesController;

typedef struct tThaEthPortSerdesController
    {
    tThaSerdesController super;

    /* Private data */
    }tThaEthPortSerdesController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSerdesController ThaSerdesControllerObjectInit(AtSerdesController self, AtChannel phyPort, uint32 serdesId);

#ifdef __cplusplus
}
#endif
#endif /* _THASERDESCONTROLLERINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PHYSICAL
 *
 * File        : ThaPowerSupplySensor.c
 *
 * Created Date: Oct 3, 2016
 *
 * Description : ThaPowerSupplySensor implementations
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaPowerSupplySensorInternal.h"
#include "ThaPowerSupplySensorReg.h"
#include "AtDevice.h"
#include "../../../generic/man/AtDriverInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaPowerSupplySensorBase   0xF44000
#define cMaxAdcCode 0xFFFF
#define cInvalidRegister 0xFFFFFFFF

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)  ((ThaPowerSupplySensor)self)
#define mBaseAddress(self) ThaPowerSupplySensorBaseAddress((ThaPowerSupplySensor)self)
#define mRead(self, address)  (AtHalRead(Hal((AtSensor)self), address) & cBit15_0)
#define mWrite(self, address, value) AtHalWrite(Hal((AtSensor)self), address, value & cBit15_0);

#define mBuildRegister(regName, voltageType) (uint32)(cReg##regName##Voltage##voltageType + mBaseAddress(self))

#define mRegisterAddressFromType(address, regName, voltageType)    \
    {                                                              \
    switch (voltageType)                                           \
        {                                                          \
        case cAtPowerSupplyVoltageInt:                             \
            address = mBuildRegister(regName, Int);                \
            break;                                                 \
        case cAtPowerSupplyVoltageAux:                             \
            address = mBuildRegister(regName, Aux);                \
            break;                                                 \
        case cAtPowerSupplyVoltageBram:                            \
            address = mBuildRegister(regName, Bram);               \
            break;                                                 \
        case cAtPowerSupplyVoltagePsintlp:                         \
            address = mBuildRegister(regName, Psintlp);            \
            break;                                                 \
        case cAtPowerSupplyVoltagePsintfp:                         \
            address = mBuildRegister(regName, Psintfp);            \
            break;                                                 \
        case cAtPowerSupplyVoltagePsaux:                           \
            address = mBuildRegister(regName, Psaux);              \
            break;                                                 \
        case cAtPowerSupplyVoltageUser0:                           \
            address = mBuildRegister(regName, User0);              \
            break;                                                 \
        case cAtPowerSupplyVoltageUser1:                           \
            address = mBuildRegister(regName, User1);              \
            break;                                                 \
        case cAtPowerSupplyVoltageUser2:                           \
            address = mBuildRegister(regName, User2);              \
            break;                                                 \
        case cAtPowerSupplyVoltageUser3:                           \
            address = mBuildRegister(regName, User3);              \
            break;                                                 \
        default:                                                   \
            address = cInvalidRegister;                            \
            break;                                                 \
        }                                                          \
    }

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaPowerSupplySensorMethods m_methods;

/* Override */
static tAtSensorMethods            m_AtSensorOverride;
static tAtPowerSupplySensorMethods m_AtPowerSupplySensorOverride;

/* Save super implementation */
static const tAtSensorMethods     *m_AtSensorMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(ThaPowerSupplySensor self)
    {
    AtUnused(self);
    return cThaPowerSupplySensorBase;
    }

static AtHal Hal(AtSensor self)
    {
    return AtDeviceIpCoreHalGet(AtSensorDeviceGet(self), 0);
    }

static uint32 RoundUpUnSignedDivision(uint32 divisor, uint32 dividend)
    {
    /* Just round if remainder is more than a half of dividend */
    if ((divisor % dividend) >= (dividend / 2))
        return ((divisor / dividend) + 1);

    return (divisor / dividend);
    }

static uint32 AdcCode2MiliVolts(uint32 adcCode)
    {
    return RoundUpUnSignedDivision((adcCode * 3000), 65536);
    }

static uint32 MiliVolts2AdcCode(uint32 mV)
    {
    return RoundUpUnSignedDivision((mV * 65536), 3000);
    }

static uint32 VoltageGet(AtPowerSupplySensor self, eAtPowerSupplyVoltage voltageType)
    {
    uint32 regAddr;

    mRegisterAddressFromType(regAddr, Current, voltageType);
    if (regAddr == cInvalidRegister)
        return 0x0;

    return AdcCode2MiliVolts(mRead(self, regAddr));
    }

static eBool MiliVoltsIsValid(uint32 voltage)
    {
    return (MiliVolts2AdcCode(voltage) > cMaxAdcCode) ? cAtFalse : cAtTrue;
    }

static eAtRet AlarmSetThresholdSet(AtPowerSupplySensor self, eAtPowerSupplyVoltage voltageType, uint32 voltage)
    {
    uint32 regAddr;

    if (!MiliVoltsIsValid(voltage))
        return cAtErrorOutOfRangParm;

    if (voltage < AtPowerSupplySensorAlarmLowerThresholdGet(self, voltageType))
        return cAtErrorInvlParm;

    mRegisterAddressFromType(regAddr, Upper, voltageType);
    if (regAddr == cInvalidRegister)
        return cAtErrorInvalidAddress;

    mWrite(self, regAddr, MiliVolts2AdcCode(voltage));
    return cAtOk;
    }

static uint32 AlarmSetThresholdGet(AtPowerSupplySensor self, eAtPowerSupplyVoltage voltageType)
    {
    uint32 regAddr;

    mRegisterAddressFromType(regAddr, Upper, voltageType);
    if (regAddr == cInvalidRegister)
        return 0x0;

    return AdcCode2MiliVolts(mRead(self, regAddr));
    }

static eAtRet AlarmClearThresholdSet(AtPowerSupplySensor self, eAtPowerSupplyVoltage voltageType, uint32 voltage)
    {
    uint32 regAddr;

    if (!MiliVoltsIsValid(voltage))
        return cAtErrorOutOfRangParm;

    if (voltage > AtPowerSupplySensorAlarmUpperThresholdGet(self, voltageType))
        return cAtErrorInvlParm;

    mRegisterAddressFromType(regAddr, Lower, voltageType);
    if (regAddr == cInvalidRegister)
        return cAtErrorInvalidAddress;

    mWrite(self, regAddr, MiliVolts2AdcCode(voltage));
    return cAtOk;
    }

static uint32 AlarmClearThresholdGet(AtPowerSupplySensor self, eAtPowerSupplyVoltage voltageType)
    {
    uint32 regAddr;

    mRegisterAddressFromType(regAddr, Lower, voltageType);
    if (regAddr == cInvalidRegister)
        return 0x0;

    return AdcCode2MiliVolts(mRead(self, regAddr));
    }

static uint32 MinRecordedVoltageGet(AtPowerSupplySensor self, eAtPowerSupplyVoltage voltageType)
    {
    uint32 regAddr;

    mRegisterAddressFromType(regAddr, MinRecorded, voltageType);
    if (regAddr == cInvalidRegister)
        return 0x0;

    return AdcCode2MiliVolts(mRead(self, regAddr));
    }

static uint32 MaxRecordedVoltageGet(AtPowerSupplySensor self, eAtPowerSupplyVoltage voltageType)
    {
    uint32 regAddr;

    mRegisterAddressFromType(regAddr, MaxRecorded, voltageType);
    if (regAddr == cInvalidRegister)
        return 0x0;

    return AdcCode2MiliVolts(mRead(self, regAddr));
    }

static uint32 AlarmGet(AtSensor self)
    {
    uint32 regAddr = cRegCurrentAlarmFlag1 + mBaseAddress(self);
    uint32 regVal = mRead(self, regAddr);
    uint32 alarmMask = 0;

    if (regVal & cRegCurrentAlarmFlag1User0Mask)
        alarmMask |= cAtPowerSupplySensorAlarmOfVccUser0;
    if (regVal & cRegCurrentAlarmFlag1User1Mask)
        alarmMask |= cAtPowerSupplySensorAlarmOfVccUser1;
    if (regVal & cRegCurrentAlarmFlag1User2Mask)
        alarmMask |= cAtPowerSupplySensorAlarmOfVccUser2;
    if (regVal & cRegCurrentAlarmFlag1User3Mask)
        alarmMask |= cAtPowerSupplySensorAlarmOfVccUser3;

    regAddr = cRegCurrentAlarmFlag0 + mBaseAddress(self);
    regVal = mRead(self, regAddr);

    if (regVal & cRegCurrentAlarmFlag0VccIntMask)
        alarmMask |= cAtPowerSupplySensorAlarmOfVccInt;
    if (regVal & cRegCurrentAlarmFlag0VccAuxMask)
        alarmMask |= cAtPowerSupplySensorAlarmOfVccAux;
    if (regVal & cRegCurrentAlarmFlag0VccBramMask)
        alarmMask |= cAtPowerSupplySensorAlarmOfVccBram;
    if (regVal & cRegCurrentAlarmFlag0VccPsIntlpMask)
        alarmMask |= cAtPowerSupplySensorAlarmOfVccPsintlp;
    if (regVal & cRegCurrentAlarmFlag0VccPsIntfpMask)
        alarmMask |= cAtPowerSupplySensorAlarmOfVccPsintfp;
    if (regVal & cRegCurrentAlarmFlag0VccPsAuxMask)
        alarmMask |= cAtPowerSupplySensorAlarmOfVccPsaux;

    return alarmMask;
    }

static uint32 DefaultLowerThreshold(ThaPowerSupplySensor self, eAtPowerSupplyVoltage type)
    {
    AtUnused(self);

    switch ((uint32)type)
        {
        case cAtPowerSupplyVoltageInt:   return 688;
        case cAtPowerSupplyVoltageAux:   return 1787;
        case cAtPowerSupplyVoltageBram:  return 846;
        case cAtPowerSupplyVoltageUser0: return 840;
        case cAtPowerSupplyVoltageUser1: return 1790;
        case cAtPowerSupplyVoltageUser2: return 1490;
        case cAtPowerSupplyVoltageUser3: return 1491;
        default:
            return cInvalidUint32;
        }
    }

static eAtRet DefaultLowerThresholdSet(AtPowerSupplySensor self, eAtPowerSupplyVoltage type)
    {
    uint32 defaultThreshold = mMethodsGet(mThis(self))->DefaultLowerThreshold(mThis(self), type);
    if (defaultThreshold == cInvalidUint32)
        return cAtOk;
    return AtPowerSupplySensorAlarmLowerThresholdSet(self, type, defaultThreshold);
    }

static eAtRet DefaultThresholdInit(AtPowerSupplySensor self)
    {
    eAtRet ret = cAtOk;

    /* Note, the below default configuration is made according to the min
     * measured value returned by IP. For each concrete product, hardware
     * should review and apply the correct thresholds */
    ret |= DefaultLowerThresholdSet(self, cAtPowerSupplyVoltageInt);
    ret |= DefaultLowerThresholdSet(self, cAtPowerSupplyVoltageAux);
    ret |= DefaultLowerThresholdSet(self, cAtPowerSupplyVoltageBram);
    ret |= DefaultLowerThresholdSet(self, cAtPowerSupplyVoltageUser0);
    ret |= DefaultLowerThresholdSet(self, cAtPowerSupplyVoltageUser1);
    ret |= DefaultLowerThresholdSet(self, cAtPowerSupplyVoltageUser2);
    ret |= DefaultLowerThresholdSet(self, cAtPowerSupplyVoltageUser3);

    return 0;
    }

static eAtRet Init(AtSensor self)
    {
    eAtRet ret;

    ret = m_AtSensorMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return DefaultThresholdInit((AtPowerSupplySensor)self);
    }

static void OverrideAtSensor(AtPowerSupplySensor self)
    {
    AtSensor sensor = (AtSensor)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSensorMethods = mMethodsGet(sensor);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSensorOverride, mMethodsGet(sensor), sizeof(m_AtSensorOverride));

        mMethodOverride(m_AtSensorOverride, AlarmGet);
        mMethodOverride(m_AtSensorOverride, Init);
        }

    mMethodsSet(sensor, &m_AtSensorOverride);
    }

static void OverrideAtPowerSupplySensor(AtPowerSupplySensor self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_AtPowerSupplySensorOverride, 0, sizeof(m_AtPowerSupplySensorOverride));

        /* Setup methods */
        mMethodOverride(m_AtPowerSupplySensorOverride, VoltageGet);
        mMethodOverride(m_AtPowerSupplySensorOverride, AlarmSetThresholdSet);
        mMethodOverride(m_AtPowerSupplySensorOverride, AlarmSetThresholdGet);
        mMethodOverride(m_AtPowerSupplySensorOverride, AlarmClearThresholdSet);
        mMethodOverride(m_AtPowerSupplySensorOverride, AlarmClearThresholdGet);
        mMethodOverride(m_AtPowerSupplySensorOverride, MinRecordedVoltageGet);
        mMethodOverride(m_AtPowerSupplySensorOverride, MaxRecordedVoltageGet);
        }

    mMethodsSet(self, &m_AtPowerSupplySensorOverride);
    }

static void Override(AtPowerSupplySensor self)
    {
    OverrideAtSensor(self);
    OverrideAtPowerSupplySensor(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPowerSupplySensor);
    }

static void MethodsInit(AtPowerSupplySensor self)
    {
    ThaPowerSupplySensor sensor = (ThaPowerSupplySensor)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, BaseAddress);
        mMethodOverride(m_methods, DefaultLowerThreshold);
        }

    mMethodsSet(sensor, &m_methods);
    }

AtPowerSupplySensor ThaPowerSupplySensorObjectInit(AtPowerSupplySensor self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPowerSupplySensorObjectInit(self, device) == NULL)
        return NULL;

    /* Setup methods */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtPowerSupplySensor ThaPowerSupplySensorNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPowerSupplySensor newSensor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newSensor == NULL)
        return NULL;

    return ThaPowerSupplySensorObjectInit(newSensor, device);
    }

uint32 ThaPowerSupplySensorBaseAddress(ThaPowerSupplySensor self)
    {
    if (self)
        return mMethodsGet(mThis(self))->BaseAddress(mThis(self));
    return 0x0;
    }

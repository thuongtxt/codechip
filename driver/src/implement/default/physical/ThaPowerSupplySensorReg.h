/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PHYSICAL
 * 
 * File        : ThaPowerSupplySensorReg.h
 * 
 * Created Date: Oct 3, 2016
 *
 * Description : ThaPowerSupplySensorReg declarations
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPOWERSUPPLYSENSORREG_H_
#define _THAPOWERSUPPLYSENSORREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cRegCurrentVoltageInt       0x1
#define cRegCurrentVoltageAux       0x2
#define cRegCurrentVoltageBram      0x6
#define cRegCurrentVoltagePsintlp   0xD
#define cRegCurrentVoltagePsintfp   0xE
#define cRegCurrentVoltagePsaux     0xF
#define cRegCurrentVoltageUser0     0x80
#define cRegCurrentVoltageUser1     0x81
#define cRegCurrentVoltageUser2     0x82
#define cRegCurrentVoltageUser3     0x83

#define cRegUpperVoltageInt       0x51
#define cRegUpperVoltageAux       0x52
#define cRegUpperVoltageBram      0x58
#define cRegUpperVoltagePsintlp   0x59
#define cRegUpperVoltagePsintfp   0x5A
#define cRegUpperVoltagePsaux     0x5B
#define cRegUpperVoltageUser0     0x60
#define cRegUpperVoltageUser1     0x61
#define cRegUpperVoltageUser2     0x62
#define cRegUpperVoltageUser3     0x63

#define cRegLowerVoltageInt       0x55
#define cRegLowerVoltageAux       0x56
#define cRegLowerVoltageBram      0x5C
#define cRegLowerVoltagePsintlp   0x5D
#define cRegLowerVoltagePsintfp   0x5E
#define cRegLowerVoltagePsaux     0x5F
#define cRegLowerVoltageUser0     0x68
#define cRegLowerVoltageUser1     0x69
#define cRegLowerVoltageUser2     0x6A
#define cRegLowerVoltageUser3     0x6B

#define cRegMaxRecordedVoltageInt       0x21
#define cRegMaxRecordedVoltageAux       0x22
#define cRegMaxRecordedVoltageBram      0x23
#define cRegMaxRecordedVoltagePsintlp   0x28
#define cRegMaxRecordedVoltagePsintfp   0x29
#define cRegMaxRecordedVoltagePsaux     0x2A
#define cRegMaxRecordedVoltageUser0     0xA0
#define cRegMaxRecordedVoltageUser1     0xA1
#define cRegMaxRecordedVoltageUser2     0xA2
#define cRegMaxRecordedVoltageUser3     0xA3

#define cRegMinRecordedVoltageInt       0x25
#define cRegMinRecordedVoltageAux       0x26
#define cRegMinRecordedVoltageBram      0x27
#define cRegMinRecordedVoltagePsintlp   0x2C
#define cRegMinRecordedVoltagePsintfp   0x2D
#define cRegMinRecordedVoltagePsaux     0x2E
#define cRegMinRecordedVoltageUser0     0xA8
#define cRegMinRecordedVoltageUser1     0xA9
#define cRegMinRecordedVoltageUser2     0xAA
#define cRegMinRecordedVoltageUser3     0xAB

#define cRegCurrentAlarmFlag1    0x3E
/*bit[3:0]=[ALM11, ALM10, ALM9, ALM8]*/
#define cRegCurrentAlarmFlag1User0Mask    cBit0
#define cRegCurrentAlarmFlag1User0Shift   0

#define cRegCurrentAlarmFlag1User1Mask    cBit1
#define cRegCurrentAlarmFlag1User1Shift   1

#define cRegCurrentAlarmFlag1User2Mask    cBit2
#define cRegCurrentAlarmFlag1User2Shift   2

#define cRegCurrentAlarmFlag1User3Mask    cBit3
#define cRegCurrentAlarmFlag1User3Shift   3

#define cRegCurrentAlarmFlag0    0x3F
/*bit[7:4] = [ALM6, ALM5, ALM4, ALM3], bit[3] = [OT], bit[2:0]=[ALM2, ALM1, ALM0]*/
#define cRegCurrentAlarmFlag0VccIntMask    cBit1
#define cRegCurrentAlarmFlag0VccIntShift   1

#define cRegCurrentAlarmFlag0VccAuxMask    cBit2
#define cRegCurrentAlarmFlag0VccAuxShift   2

#define cRegCurrentAlarmFlag0VccBramMask    cBit4
#define cRegCurrentAlarmFlag0VccBramShift   4

#define cRegCurrentAlarmFlag0VccPsIntlpMask    cBit5
#define cRegCurrentAlarmFlag0VccPsIntlpShift   5

#define cRegCurrentAlarmFlag0VccPsIntfpMask    cBit6
#define cRegCurrentAlarmFlag0VccPsIntfpShift   6

#define cRegCurrentAlarmFlag0VccPsAuxMask    cBit7
#define cRegCurrentAlarmFlag0VccPsAuxShift   7

#ifdef __cplusplus
}
#endif
#endif /* _THAPOWERSUPPLYSENSORREG_H_ */


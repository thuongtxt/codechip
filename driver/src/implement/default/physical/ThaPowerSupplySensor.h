/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PHYSICAL
 * 
 * File        : ThaPowerSupplySensor.h
 * 
 * Created Date: Oct 3, 2016
 *
 * Description : ThaPowerSupplySensor declarations
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPOWERSUPPLYSENSOR_H_
#define _THAPOWERSUPPLYSENSOR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPowerSupplySensor.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPowerSupplySensor * ThaPowerSupplySensor;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPowerSupplySensor ThaPowerSupplySensorNew(AtDevice device);
AtPowerSupplySensor Tha60290021PowerSupplySensorNew(AtDevice device);
AtPowerSupplySensor Tha60290022PowerSupplySensorNew(AtDevice device);

uint32 ThaPowerSupplySensorBaseAddress(ThaPowerSupplySensor self);

#ifdef __cplusplus
}
#endif
#endif /* _THAPOWERSUPPLYSENSOR_H_ */


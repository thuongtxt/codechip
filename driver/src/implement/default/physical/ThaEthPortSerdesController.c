/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : ThaEthPortSerdesController.c
 *
 * Created Date: Aug 16, 2013
 *
 * Description : SERDES controller for ETH port
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../generic/eth/AtModuleEthInternal.h"
#include "ThaSerdesControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSerdesControllerMethods  m_AtSerdesControllerOverride;
static tThaSerdesControllerMethods m_ThaSerdesControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(ThaSerdesController self)
    {
	AtUnused(self);
    return 0xf28000;
    }

static AtPrbsEngine PrbsEngineCreate(AtSerdesController self)
    {
    AtEthPort port = (AtEthPort)AtSerdesControllerPhysicalPortGet(self);
    ThaModuleEth ethModule = (ThaModuleEth)AtChannelModuleGet((AtChannel)port);
    return ThaModuleEthPortSerdesPrbsEngineCreate(ethModule, port, AtSerdesControllerIdGet(self));
    }

static AtMdio Mdio(AtSerdesController self)
    {
    AtChannel port = AtSerdesControllerPhysicalPortGet(self);
    AtModuleEth ethModule = (AtModuleEth)AtChannelModuleGet(port);
    return AtModuleEthSerdesMdio(ethModule, self);
    }

static eAtRet ModeSet(AtSerdesController self, eAtSerdesMode mode)
    {
    AtUnused(self);
    return ((mode == cAtSerdesModeGe) ? cAtOk : cAtErrorModeNotSupport);
    }

static eAtSerdesMode ModeGet(AtSerdesController self)
    {
    AtUnused(self);
    return cAtSerdesModeGe;
    }

static AtEyeScanController EyeScanControllerObjectCreate(AtSerdesController self, uint32 drpBaseAddress)
    {
    return AtEyeScanControllerXauiNew(self, drpBaseAddress);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaEthPortSerdesController);
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, mMethodsGet(self), sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, PrbsEngineCreate);
        mMethodOverride(m_AtSerdesControllerOverride, Mdio);
        mMethodOverride(m_AtSerdesControllerOverride, ModeSet);
        mMethodOverride(m_AtSerdesControllerOverride, ModeGet);
        mMethodOverride(m_AtSerdesControllerOverride, EyeScanControllerObjectCreate);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void OverrideThaSerdesController(AtSerdesController self)
    {
    ThaSerdesController controller = (ThaSerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSerdesControllerOverride, mMethodsGet(controller), sizeof(m_ThaSerdesControllerOverride));

        mMethodOverride(m_ThaSerdesControllerOverride, BaseAddress);
        }

    mMethodsSet(controller, &m_ThaSerdesControllerOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtSerdesController(self);
    OverrideThaSerdesController(self);
    }

static AtSerdesController ObjectInit(AtSerdesController self, AtEthPort ethPort, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaSerdesControllerObjectInit(self, (AtChannel)ethPort, serdesId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController ThaEthPortSerdesControllerNew(AtEthPort ethPort, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, ethPort, serdesId);
    }

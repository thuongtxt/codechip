/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : ThaSerdesController.c
 *
 * Created Date: Aug 16, 2013
 *
 * Description : Default SERDES controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaSerdesControllerInternal.h"
#include "../../../util/coder/AtCoderUtil.h"

/*--------------------------- Define -----------------------------------------*/
#define cLoopoutPreParamId 0x20
#define cInvalidValue 0xCAFECAFE

#define cEyeMonitor1DEyeMask      cBit13
#define cEyeMonitor1DEyeShift     13
#define cEyeMonitorBandwidthMask  cBit10_9
#define cEyeMonitorBandwidthShift 9

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaSerdesController)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaSerdesControllerMethods m_methods;

/* Override */
static tAtObjectMethods           m_AtObjectOverride;
static tAtSerdesControllerMethods m_AtSerdesControllerOverride;

/* Save super implementation */
static const tAtObjectMethods    *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(ThaSerdesController self)
    {
	AtUnused(self);
    /* Sub class should do */
    return cInvalidValue;
    }

static eAtRet Init(AtSerdesController self)
    {
    eAtRet ret = cAtOk;
    AtEyeScanController eyeScanController = AtSerdesControllerEyeScanControllerGet(self);

    if (eyeScanController)
        ret |= AtEyeScanControllerInit(eyeScanController);

    return ret;
    }

static eBool VodIsInRange(uint32 vod)
    {
    return (vod <= 63) ? cAtTrue : cAtFalse;
    }

static eBool PreEmphasisIsInRange(uint32 preEmphasis)
    {
    return (preEmphasis < 32) ? cAtTrue : cAtFalse;
    }

static eBool RxEqualizationDcGainIsInRange(uint32 rxEqualizationDcGain)
    {
    return (rxEqualizationDcGain <= 4) ? cAtTrue : cAtFalse;
    }

static eBool RxEqualizationControlIsInRange(uint32 rxEqualizationControl)
    {
    return (rxEqualizationControl <= 15) ? cAtTrue : cAtFalse;
    }

static eBool PhysicalParamValueIsInRange(ThaSerdesController self, eAtSerdesParam param, uint32 value)
    {
	AtUnused(self);
    if (param == cAtSerdesParamVod)
        return VodIsInRange(value);

    if (!mOutOfRange(param, cAtSerdesParamPreEmphasisPreTap, cAtSerdesParamPreEmphasisSecondPostTap))
        return PreEmphasisIsInRange(value);

    if (param == cAtSerdesParamRxEqualizationDcGain)
        return RxEqualizationDcGainIsInRange(value);

    if (param == cAtSerdesParamRxEqualizationControl)
        return RxEqualizationControlIsInRange(value);

    return cAtTrue;
    }

static eAtModule ModuleId(AtSerdesController self)
    {
    AtModule module = AtChannelModuleGet(AtSerdesControllerPhysicalPortGet(self));
    return AtModuleTypeGet(module);
    }

static eAtRet HwPhysicalParamSet(ThaSerdesController self, uint32 hwParam, uint32 value)
    {
    uint32 phyBaseAddress;
    AtSerdesController controller = (AtSerdesController)self;
    eAtModule moduleId = ModuleId(controller);

    phyBaseAddress = mMethodsGet(self)->BaseAddress(self) +
                     mMethodsGet(self)->PartOffset(self);
    AtSerdesControllerWrite(controller, phyBaseAddress + 0x8, AtSerdesControllerIdGet(controller), moduleId); /* Select port */
    AtSerdesControllerWrite(controller, phyBaseAddress + 0xB, hwParam                      , moduleId); /* Access parameter by ID */
    AtSerdesControllerWrite(controller, phyBaseAddress + 0xC, value                        , moduleId); /* Data[5:0] */
    AtSerdesControllerWrite(controller, phyBaseAddress + 0xA, 0x1                          , moduleId); /* Trigger write operation */

    return cAtOk;
    }

static eAtRet OnPhysicalParamSet(AtSerdesController self, eAtSerdesParam param, uint32 value)
    {
    if (param == cAtSerdesParamRxEqualizationControl)
        mThis(self)->rxEqualizationControl = (uint8)value;
    return cAtOk;
    }

static eAtRet PhysicalParamSet(AtSerdesController self, eAtSerdesParam param, uint32 value)
    {
    eAtRet ret = cAtOk;

    if (!mMethodsGet(mThis(self))->PhysicalParamValueIsInRange(mThis(self), param, value))
        return cAtErrorOutOfRangParm;

    ret = mMethodsGet(mThis(self))->HwPhysicalParamSet(mThis(self), AtSerdesControllerHwParameterId(self, param), value);
    if (ret == cAtOk)
        ret |= OnPhysicalParamSet(self, param, value);

    return ret;
    }

static uint32 HwPhysicalParamGet(ThaSerdesController self, uint32 hwParam)
    {
    uint32 phyBaseAddress;
    AtSerdesController controller = (AtSerdesController)self;
    eAtModule moduleId = ModuleId(controller);

    phyBaseAddress = mMethodsGet(self)->BaseAddress(self) +
                     mMethodsGet(self)->PartOffset(self);
    AtSerdesControllerWrite(controller, phyBaseAddress + 0x8, AtSerdesControllerIdGet(controller), moduleId); /* Select port */
    AtSerdesControllerWrite(controller, phyBaseAddress + 0xB, hwParam                      , moduleId); /* Access parameter by ID */
    AtSerdesControllerWrite(controller, phyBaseAddress + 0xA, 0x2                          , moduleId); /* Trigger read operation */

    return AtSerdesControllerRead(controller, phyBaseAddress + 0xC, moduleId); /* Data[5:0] */
    }

static eBool PhysicalParamCanRead(ThaSerdesController self, eAtSerdesParam param)
    {
	AtUnused(self);
    if (param == cAtSerdesParamRxEqualizationControl)
        return cAtFalse;
    return cAtTrue;
    }

static uint32 PhysicalParamCache(ThaSerdesController self, eAtSerdesParam param)
    {
    if (param == cAtSerdesParamRxEqualizationControl)
        return mThis(self)->rxEqualizationControl;
    return cInvalidValue;
    }

static uint32 PhysicalParamGet(AtSerdesController self, eAtSerdesParam param)
    {
    if (mMethodsGet(mThis(self))->PhysicalParamCanRead(mThis(self), param))
            return mMethodsGet(mThis(self))->HwPhysicalParamGet(mThis(self), AtSerdesControllerHwParameterId(self, param));

    return mMethodsGet(mThis(self))->PhysicalParamCache(mThis(self), param);
    }

static uint32 PartOffset(ThaSerdesController self)
    {
	AtUnused(self);
    return 0x0;
    }

static uint32 LoopInRegAddress(ThaSerdesController self)
    {
	AtUnused(self);
    /* Subclass should know */
    return cInvalidValue;
    }

static uint32 LoopInMask(ThaSerdesController self)
    {
	AtUnused(self);
    /* Subclass should know */
    return cInvalidValue;
    }

static uint32 LoopInShift(ThaSerdesController self)
    {
	AtUnused(self);
    /* Subclass should know */
    return cInvalidValue;
    }

static eBool CanControlSerdes(ThaSerdesController self)
    {
    ThaDevice device = (ThaDevice)AtChannelDeviceGet(AtSerdesControllerPhysicalPortGet((AtSerdesController)self));
    return ThaDeviceIsEp(device) ? cAtFalse : cAtTrue;
    }

static eAtRet LoopInEnable(ThaSerdesController self, eBool enable)
    {
    uint32 regAddr, regVal, mask, shift;
    AtSerdesController controller;
    eAtModule moduleId;

    if (!CanControlSerdes(self))
        return cAtOk;

    controller = (AtSerdesController)self;
    moduleId   = ModuleId(controller);

    regAddr    = mMethodsGet(mThis(self))->LoopInRegAddress(mThis(self));
    mask       = mMethodsGet(mThis(self))->LoopInMask(mThis(self));
    shift      = mMethodsGet(mThis(self))->LoopInShift(mThis(self));
    regVal = AtSerdesControllerRead(controller, regAddr, moduleId);
    mFieldIns(&regVal, mask, shift, enable ? 1 : 0);
    AtSerdesControllerWrite(controller, regAddr, regVal, moduleId);

    return cAtOk;
    }

static eBool LoopInIsEnabled(ThaSerdesController self)
    {
    uint32 regVal;
    AtSerdesController controller;
    eAtModule moduleId;

    if (!CanControlSerdes(self))
        return cAtFalse;

    controller = (AtSerdesController)self;
    moduleId   = ModuleId(controller);
    regVal     = AtSerdesControllerRead(controller, mMethodsGet(mThis(self))->LoopInRegAddress(mThis(self)), moduleId);
    return (regVal & mMethodsGet(mThis(self))->LoopInMask(mThis(self))) ? cAtTrue : cAtFalse;
    }

static eAtRet LoopOutHwEnable(ThaSerdesController self, eBool enable)
    {
    return AtSerdesControllerPhysicalParamSet((AtSerdesController)self, cLoopoutPreParamId, enable ? 1 : 0);
    }

static eAtRet LoopOutEnable(ThaSerdesController self, eBool enable)
    {
    eAtRet ret;

    if (!CanControlSerdes(self))
        return cAtOk;

    ret = mMethodsGet(self)->LoopOutHwEnable(self, enable);
    if (ret == cAtOk)
        self->loopoutEnabled = enable;

    return ret;
    }

static eBool LoopOutIsEnabled(ThaSerdesController self)
    {
    return self->loopoutEnabled;
    }

static eAtRet LoopbackEnable(AtSerdesController self, eAtLoopbackMode loopbackMode, eBool enable)
    {
    if (loopbackMode == cAtLoopbackModeLocal)
        return mMethodsGet(mThis(self))->LoopInEnable(mThis(self), enable);
    if (loopbackMode == cAtLoopbackModeRemote)
        return mMethodsGet(mThis(self))->LoopOutEnable(mThis(self), enable);

    return cAtErrorInvlParm;
    }

static eBool LoopbackIsEnabled(AtSerdesController self, eAtLoopbackMode loopbackMode)
    {
    if (loopbackMode == cAtLoopbackModeLocal)
        return mMethodsGet(mThis(self))->LoopInIsEnabled(mThis(self));
    if (loopbackMode == cAtLoopbackModeRemote)
        return mMethodsGet(mThis(self))->LoopOutIsEnabled(mThis(self));

    return cAtFalse;
    }

static eBool PhysicalParamIsSupported(AtSerdesController self, eAtSerdesParam param)
    {
	AtUnused(param);
	AtUnused(self);
    /* Let's assume all of parameters have to be supported. Concrete class
     * should override */
    return cAtTrue;
    }

static uint32 PhysicalParamDefaultValue(ThaSerdesController self, eAtSerdesParam param)
    {
	AtUnused(param);
	AtUnused(self);
    return 0x0;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaSerdesController object = (ThaSerdesController)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(loopoutEnabled);
    mEncodeUInt(rxEqualizationControl);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaSerdesController);
    }

static void MethodsInit(ThaSerdesController self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, BaseAddress);
        mMethodOverride(m_methods, PartOffset);
        mMethodOverride(m_methods, LoopInEnable);
        mMethodOverride(m_methods, LoopInIsEnabled);
        mMethodOverride(m_methods, LoopInRegAddress);
        mMethodOverride(m_methods, LoopInMask);
        mMethodOverride(m_methods, LoopInShift);
        mMethodOverride(m_methods, LoopOutEnable);
        mMethodOverride(m_methods, LoopOutHwEnable);
        mMethodOverride(m_methods, LoopOutIsEnabled);
        mMethodOverride(m_methods, HwPhysicalParamSet);
        mMethodOverride(m_methods, HwPhysicalParamGet);
        mMethodOverride(m_methods, PhysicalParamCanRead);
        mMethodOverride(m_methods, PhysicalParamCache);
        mMethodOverride(m_methods, PhysicalParamValueIsInRange);
        mMethodOverride(m_methods, PhysicalParamDefaultValue);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtObject(AtSerdesController self)
    {
    AtObject object = (AtObject)self;

    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, mMethodsGet(self), sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, Init);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalParamSet);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalParamGet);
        mMethodOverride(m_AtSerdesControllerOverride, LoopbackEnable);
        mMethodOverride(m_AtSerdesControllerOverride, LoopbackIsEnabled);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalParamIsSupported);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtObject(self);
    OverrideAtSerdesController(self);
    }

AtSerdesController ThaSerdesControllerObjectInit(AtSerdesController self, AtChannel phyPort, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtSerdesControllerObjectInit(self, phyPort, serdesId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

eAtRet ThaSerdesControllerDefaultParamSet(ThaSerdesController self, eAtSerdesParam param)
    {
    if (self)
        {
        uint32 value = mMethodsGet(self)->PhysicalParamDefaultValue(self, param);
        return AtSerdesControllerPhysicalParamSet((AtSerdesController)self, param, value);
        }

    return cAtErrorNullPointer;
    }

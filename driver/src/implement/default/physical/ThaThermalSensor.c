/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PHYSICAL
 *
 * File        : ThaThermalSensor.c
 *
 * Created Date: Dec 15, 2015
 *
 * Description : Default thermal sensor implementation.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../man/ThaDeviceInternal.h"
#include "ThaThermalSensorInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaThermalSensorBase   0xFE2000

/* ------------ ADC code of temperature --------------*/
#define cThaRegCurrentTemperature       (0x0 + mBaseAddress(self))
#define cThaRegMaxRecordedTemperature   (0x20 + mBaseAddress(self))
#define cThaRegMinRecordedTemperature   (0x24 + mBaseAddress(self))

#define cThaRegTemperatureAdcCodeMask   cBit15_4
#define cThaRegTemperatureAdcCodeShift  4

/* ------------- Alarm of temperature ----------------*/
#define cThaRegTemperatureAlarm         (0x3F + mBaseAddress(self))
#define cThaRegTemperatureAlarmMask     cBit0

#define cThaRegTemperatureAlarmSetThreshold         (0x50 + mBaseAddress(self))
#define cThaRegTemperatureAlarmSetThresholdMask     cBit15_4
#define cThaRegTemperatureAlarmSetThresholdShift    4

#define cThaRegTemperatureAlarmClearThreshold       (0x54 + mBaseAddress(self))
#define cThaRegTemperatureAlarmClearThresholdMask   cBit15_4
#define cThaRegTemperatureAlarmClearThresholdShift  4

#define cThaRegTemperatureInterruptStatus       0xD0
#define cThaRegTemperatureInterruptStatusMask   cBit0

#define cThaRegTemperatureInterruptEnable       0x3
#define cThaRegTemperatureInterruptEnableMask   cBit27

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)  ((ThaThermalSensor)self)
#define mBaseAddress(self) mMethodsGet(mThis(self))->BaseAddress(mThis(self))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaThermalSensorMethods m_methods;

/* Override */
static tAtSensorMethods         m_AtSensorOverride;
static tAtThermalSensorMethods  m_AtThermalSensorOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(ThaThermalSensor self)
    {
    AtUnused(self);
    return cThaThermalSensorBase;
    }

static AtHal Hal(AtSensor self)
    {
    return AtDeviceIpCoreHalGet(AtSensorDeviceGet(self), 0);
    }

static uint32 AlarmGet(AtSensor self)
    {
    uint32 regVal = AtHalRead(Hal(self), cThaRegTemperatureAlarm);
    return (regVal & cThaRegTemperatureAlarmMask) ? cAtThermalSensorAlarmOverheat : 0;
    }

static uint32 AlarmHistoryGet(AtSensor self)
    {
    uint32 regVal = AtHalRead(Hal(self), cThaRegTemperatureInterruptStatus);
    return (regVal & cThaRegTemperatureInterruptStatusMask) ? cAtThermalSensorAlarmOverheat : 0;
    }

static uint32 AlarmHistoryClear(AtSensor self)
    {
    uint32 regVal = AtHalRead(Hal(self), cThaRegTemperatureInterruptStatus);

    /* Clear interrupt. */
    AtHalWrite(Hal(self), cThaRegTemperatureInterruptStatus, regVal);
    return (regVal & cThaRegTemperatureInterruptStatusMask) ? cAtThermalSensorAlarmOverheat : 0;
    }

static eAtRet InterruptMaskSet(AtSensor self, uint32 alarmMask, uint32 enableMask)
    {
    uint32 regVal = AtHalRead(Hal(self), cThaRegTemperatureInterruptEnable);

    if (alarmMask & cAtThermalSensorAlarmOverheat)
        {
        if (enableMask & cAtThermalSensorAlarmOverheat)
            regVal |= cThaRegTemperatureInterruptEnableMask;
        else
            regVal &= (uint32)~cThaRegTemperatureInterruptEnableMask;
        }

    AtHalWrite(Hal(self), cThaRegTemperatureInterruptEnable, regVal);

    return cAtOk;
    }

static uint32 InterruptMaskGet(AtSensor self)
    {
    uint32 regVal = AtHalRead(Hal(self), cThaRegTemperatureInterruptEnable);

    return (regVal & cThaRegTemperatureInterruptEnableMask) ? cAtThermalSensorAlarmOverheat : 0;
    }

static void InterruptProcess(AtSensor self, AtIpCore ipCore)
    {
    uint32 changedAlarms = AtSensorAlarmHistoryClear(self);
    uint32 currentAlarms = AtSensorAlarmGet(self);
    AtUnused(ipCore);

    AtSensorAllAlarmListenersCall(self, changedAlarms, currentAlarms);
    }

static uint32 Abs(int32 signedValue)
    {
    return AtStdAbs(AtStdSharedStdGet(), signedValue);
    }

static int32 RoundUpSignedDivision(int32 divisor, int32 dividend)
    {
    int32 devideResult;

    /* Just round if remainder is more than a half of dividend */
    if (Abs(divisor % dividend) < Abs(dividend / 2))
        return (divisor / dividend);

    devideResult = divisor / dividend;

    if (devideResult > 0)
        return (devideResult + 1);

    if (devideResult < 0)
        return (devideResult - 1);

    if (divisor > 0)
        return 1;

    if (divisor < 0)
        return -1;

    return 0;
    }

static uint32 RoundUpUnSignedDivision(uint32 divisor, uint32 dividend)
    {
    /* Just round if remainder is more than a half of dividend */
    if ((divisor % dividend) >= (dividend / 2))
        return ((divisor / dividend) + 1);

    return (divisor / dividend);
    }

static int32 AdcCodeToCelsiusValue(uint32 adcCode)
    {
    /* Temperature(oC) = (ADC_code * 503.975) / 4096 - 273.15 */
    int32 divisor = (int32)((adcCode * 503975) - (4096 * 273150));
    return RoundUpSignedDivision(divisor, 4096000);
    }

static uint32 CelsiusValueToAdcCode(int32 celsiusValue)
    {
    /*     ADC_code (hexa) = (4096 * (Temperature(oC) + 273.15)) / 503.975
     * <=> ADC_code (hexa) = (4096 * (1000 * Temperature(oC) + 273150)) / 503975 */
    uint32 divisor = (uint32)(4096 * (273150 + (int32)(1000 * celsiusValue)));
    return RoundUpUnSignedDivision(divisor, 503975);
    }

static int32 MinCelsius(AtThermalSensor self)
    {
    AtUnused(self);
    return -273;
    }

static int32 MaxCelsius(AtThermalSensor self)
    {
    AtUnused(self);
    return 230;
    }

static eBool CelsiusOutOfRange(AtThermalSensor self, int32 celsius)
    {
    /* Maximum is about 230 Celsius with maximum ADC code is 0xFFF
     * and minimum is about -273 celsius with minimum ADC code is 0x0 */
    return ((celsius > MaxCelsius(self)) || (celsius < MinCelsius(self))) ? cAtTrue : cAtFalse;
    }

static eBool CelsiusSetThresholdValueOutOfRange(AtThermalSensor self, int32 celsius)
    {
    if (celsius < AtThermalSensorAlarmClearThresholdGet(self))
        return cAtTrue;

    return CelsiusOutOfRange(self, celsius) ? cAtTrue : cAtFalse;
    }

static eBool CelsiusClearThresholdValueOutOfRange(AtThermalSensor self, int32 celsius)
    {
    if (celsius > AtThermalSensorAlarmSetThresholdGet(self))
        return cAtTrue;

    return CelsiusOutOfRange(self, celsius) ? cAtTrue : cAtFalse;
    }

static int32 CurrentTemperatureGet(AtThermalSensor self)
    {
    uint32 regVal = AtHalRead(Hal((AtSensor)self), cThaRegCurrentTemperature);
    uint32 adcCode = mRegField(regVal, cThaRegTemperatureAdcCode);

    return AdcCodeToCelsiusValue(adcCode);
    }

static int32 MinRecordedTemperatureGet(AtThermalSensor self)
    {
    uint32 regVal = AtHalRead(Hal((AtSensor)self), cThaRegMinRecordedTemperature);
    uint32 adcCode = mRegField(regVal, cThaRegTemperatureAdcCode);

    return AdcCodeToCelsiusValue(adcCode);
    }

static int32 MaxRecordedTemperatureGet(AtThermalSensor self)
    {
    uint32 regVal = AtHalRead(Hal((AtSensor)self), cThaRegMaxRecordedTemperature);
    uint32 adcCode = mRegField(regVal, cThaRegTemperatureAdcCode);

    return AdcCodeToCelsiusValue(adcCode);
    }

static eAtRet AlarmSetThresholdSet(AtThermalSensor self, int32 celsius)
    {
    uint32 regVal;

    if (CelsiusSetThresholdValueOutOfRange(self, celsius))
        return cAtErrorOutOfRangParm;

    regVal = AtHalRead(Hal((AtSensor)self), cThaRegTemperatureAlarmSetThreshold);
    mRegFieldSet(regVal, cThaRegTemperatureAlarmSetThreshold, CelsiusValueToAdcCode(celsius));
    AtHalWrite(Hal((AtSensor)self), cThaRegTemperatureAlarmSetThreshold, regVal);

    return cAtOk;
    }

static eAtRet AlarmClearThresholdSet(AtThermalSensor self, int32 celsius)
    {
    uint32 regVal;

    if (CelsiusClearThresholdValueOutOfRange(self, celsius))
        return cAtErrorOutOfRangParm;

    regVal = AtHalRead(Hal((AtSensor)self), cThaRegTemperatureAlarmClearThreshold);
    mRegFieldSet(regVal, cThaRegTemperatureAlarmClearThreshold, CelsiusValueToAdcCode(celsius));
    AtHalWrite(Hal((AtSensor)self), cThaRegTemperatureAlarmClearThreshold, regVal);

    return cAtOk;
    }

static int32 AlarmSetThresholdGet(AtThermalSensor self)
    {
    uint32 regVal = AtHalRead(Hal((AtSensor)self), cThaRegTemperatureAlarmSetThreshold);
    uint32 adcCode = mRegField(regVal, cThaRegTemperatureAlarmSetThreshold);
    return AdcCodeToCelsiusValue(adcCode);
    }

static int32 AlarmClearThresholdGet(AtThermalSensor self)
    {
    uint32 regVal = AtHalRead(Hal((AtSensor)self), cThaRegTemperatureAlarmClearThreshold);
    uint32 adcCode = mRegField(regVal, cThaRegTemperatureAlarmClearThreshold);
    return AdcCodeToCelsiusValue(adcCode);
    }

static int32 AlarmSetThresholdMax(AtThermalSensor self)
    {
    return MaxCelsius(self);
    }

static int32 AlarmSetThresholdMin(AtThermalSensor self)
    {
    int32 clearThreshold = AtThermalSensorAlarmClearThresholdGet(self);
    int32 minCelsius = MinCelsius(self);
    return mMax(clearThreshold, minCelsius);
    }

static int32 AlarmClearThresholdMax(AtThermalSensor self)
    {
    int32 setThreshold = AtThermalSensorAlarmSetThresholdGet(self);
    int32 maxCelsius = MaxCelsius(self);
    return mMin(setThreshold, maxCelsius);
    }

static int32 AlarmClearThresholdMin(AtThermalSensor self)
    {
    return MinCelsius(self);
    }

static void OverrideAtSensor(AtThermalSensor self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSensorOverride, mMethodsGet((AtSensor)self), sizeof(m_AtSensorOverride));

        mMethodOverride(m_AtSensorOverride, AlarmGet);
        mMethodOverride(m_AtSensorOverride, AlarmHistoryGet);
        mMethodOverride(m_AtSensorOverride, AlarmHistoryClear);
        mMethodOverride(m_AtSensorOverride, InterruptMaskSet);
        mMethodOverride(m_AtSensorOverride, InterruptMaskGet);
        mMethodOverride(m_AtSensorOverride, InterruptProcess);
        }

    mMethodsSet(((AtSensor)self), &m_AtSensorOverride);
    }

static void OverrideAtThermalSensor(AtThermalSensor self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_AtThermalSensorOverride, 0, sizeof(m_AtThermalSensorOverride));

        /* Setup methods */
        mMethodOverride(m_AtThermalSensorOverride, CurrentTemperatureGet);
        mMethodOverride(m_AtThermalSensorOverride, MinRecordedTemperatureGet);
        mMethodOverride(m_AtThermalSensorOverride, MaxRecordedTemperatureGet);
        mMethodOverride(m_AtThermalSensorOverride, AlarmSetThresholdSet);
        mMethodOverride(m_AtThermalSensorOverride, AlarmClearThresholdSet);
        mMethodOverride(m_AtThermalSensorOverride, AlarmSetThresholdGet);
        mMethodOverride(m_AtThermalSensorOverride, AlarmClearThresholdGet);
        mMethodOverride(m_AtThermalSensorOverride, AlarmSetThresholdMax);
        mMethodOverride(m_AtThermalSensorOverride, AlarmSetThresholdMin);
        mMethodOverride(m_AtThermalSensorOverride, AlarmClearThresholdMax);
        mMethodOverride(m_AtThermalSensorOverride, AlarmClearThresholdMin);
        }

    mMethodsSet(self, &m_AtThermalSensorOverride);
    }

static void Override(AtThermalSensor self)
    {
    OverrideAtSensor(self);
    OverrideAtThermalSensor(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaThermalSensor);
    }

static void MethodsInit(AtThermalSensor self)
    {
    ThaThermalSensor sensor = (ThaThermalSensor)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, BaseAddress);
        }

    mMethodsSet(sensor, &m_methods);
    }

AtThermalSensor ThaThermalSensorObjectInit(AtThermalSensor self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtThermalSensorObjectInit(self, device) == NULL)
        return NULL;

    /* Setup methods */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtThermalSensor ThaThermalSensorNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtThermalSensor newSensor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newSensor == NULL)
        return NULL;

    return ThaThermalSensorObjectInit(newSensor, device);
    }

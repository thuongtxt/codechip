/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : ThaSdhPointerAccumulator.h
 * 
 * Created Date: Jan 25, 2016
 *
 * Description : Pointer accumulator
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASDHPOINTERACCUMULATOR_H_
#define _THASDHPOINTERACCUMULATOR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h" /* Super class */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaSdhPointerAccumulator * ThaSdhPointerAccumulator;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaSdhPointerAccumulator ThaSdhPointerAccumulatorNew(void);

uint32 ThaSdhPointerAccumulatorCounterUpdateThenClear(ThaSdhPointerAccumulator self, uint16 counterType, uint32 counterValue, eBool clear);
uint32 ThaSdhPointerAccumulatorCounterRead2Clear(ThaSdhPointerAccumulator self, uint16 counterType, eBool read2Clear);

#ifdef __cplusplus
}
#endif
#endif /* _THASDHPOINTERACCUMULATOR_H_ */


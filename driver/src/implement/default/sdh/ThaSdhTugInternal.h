/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : ThaSdhTugInternal.h
 * 
 * Created Date: Jun 29, 2015
 *
 * Description : Thalassa SDH TUG default implementation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASDHTUGINTERNAL_H_
#define _THASDHTUGINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/sdh/AtSdhTugInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaSdhTug * ThaSdhTug;

typedef struct tThaSdhTug
    {
    tAtSdhTug super;
    }tThaSdhTug;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhTug ThaSdhTugObjectInit(AtSdhTug self, uint32 channelId, uint8 channelType, AtModuleSdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THASDHTUGINTERNAL_H_ */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2009 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SONET/SDH
 *
 * File        : ThaLineSdhReg.h
 *
 * Created Date: 07-Apr-09
 *
 * Description : This file contain all register definitions of registers of
 *               SONET/SDH block.
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/
#ifndef THAOCNREG_HEADER
#define THAOCNREG_HEADER

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/* Number of Double word (4 bytes) in one J0 message */

#define VTMAX 28UL
#define LIDMAX 2UL

/* Define CR and LF character */
#define cSonetCr                                    '\r'
#define cSonetLf                                    '\n'

#define cThaOcnJ0CapBufId                           1

#define cThaPohIndirectAcsTimeOut 20 /* ms */

#define cLineMask(lineId)  (cBit0 << (lineId))
#define cLineShift(lineId) (lineId)

/*------------------------------------------------------------------------------
Reg Name: OCN Rx Line Enable Control
Reg Addr: 0x00040001
Reg Desc: Configure rate for 4 SONET/SDH lines at Rx direction.
------------------------------------------------------------------------------*/
#define cThaRegOcnRxLineEnCtrl                      0x00040001

/*-----------
BitField Name: OCNRxLinetEnb
BitField Type: R/W
BitField Desc: These 4 bits is used to configure OC-3/STM-1 modes for 4
               SONET/SDH lines at network side. The bit zero is used for line
               0. Bit is set to enable line.
------------*/
#define cThaOcnRxLineEnbMask(lineId)                    (cBit0 << (lineId))
#define cThaOcnRxLineEnbShift(lineId)                   (lineId)
#define cThaRegOcnRxLineEnCtrlNoneEditableFieldsMask cBit31_8

/*------------------------------------------------------------------------------
Reg Name: OCN Tx Line Enable Control
Reg Addr: 0x00040005
Reg Desc: Configure rate for 4 SONET/SDH lines at Tx direction.
------------------------------------------------------------------------------*/
#define cThaRegOcnTxLineEnCtrl                      0x00040005

/*-----------
BitField Name: OCNTxLinetEnb
BitField Type: R/W
BitField Desc: These 4 bits is used to configure OC-3/STM-1 modes for 4
               SONET/SDH lines at network side. The bit zero is used for line
               0. Bit is set to enable line.
------------*/
#define cThaOcnTxLineEnbMask(lineId)                    (cBit0 << (lineId))
#define cThaOcnTxLineEnbShift(lineId)                   (lineId)

#define cThaRegOcnTxLineEnCtrlNoneEditableFieldsMask    cBit31_8

/*------------------------------------------------------------------------------
Reg Name: OCN Rx Line Rate Control
Reg Addr: 0x00040000
Reg Desc: Configure rate for 8 SONET/SDH lines at Rx direction.
------------------------------------------------------------------------------*/
#define cThaRegOcnRxLineRateCtrl                    0x00040000

/*-----------
BitField Name: OCNRxLineRate[7:0]
BitField Type: R/W
BitField Desc: These 8 bits is used to configure OC-3/STM-1 modes for 8
               SONET/SDH lines at network side. The bit zero is used for line
               0.
               - 0: The line is OC-3/STM-1 line
               - 1: The line is OC-12/STM-4 line
------------*/
#define cThaOcnRxLineRateMask(lineId)                    (cBit0 << (lineId))
#define cThaOcnRxLineRateShift(lineId)                   (lineId)
#define cThaRegOcnRxLineRateCtrlNoneEditableFieldsMask   cBit31_8

/*------------------------------------------------------------------------------
Reg Name: OCN Tx Line Rate Control
Reg Addr: 0x00040004
Reg Desc: Configure rate for 8 SONET/SDH lines at Tx direction.
------------------------------------------------------------------------------*/
#define cThaRegOcnTxLineRateCtrl                    0x00040004

/*-----------
BitField Name: OCNTxLinetRate[7:0]
BitField Type: R/W
BitField Desc: These 8 bits is used to configure OC-3/STM-1 modes for 8
               SONET/SDH lines at network side. The bit zero is used for line
               0.
               - 0: The line is OC-3/STM-1 line
               - 1: The line is OC-12/STM-4 line
------------*/
#define cThaOcnTxLineRateMask(lineId)                    (cBit0 << (lineId))
#define cThaOcnTxLineRateShift(lineId)                   (lineId)
#define cThaRegOcnTxLineRateCtrlNoneEditableFieldsMask   cBit31_8

/*------------------------------------------------------------------------------
Reg Name: OCN Rx Line per Alarm Current Status
Reg Addr: 0x00074A20 - 0x00074A27
          The address format for these registers is 0x00074A20 + lineID
          Where: lineID (0 - 7) SONET/SDH line IDs
Reg Desc: This is the per Alarm current status of Rx Framer and TOH
          Monitoring. Each register is used to store 6 bits to store current
          status of 6 alarms in the line.
------------------------------------------------------------------------------*/
#define cThaRegOcnRxLineperAlmCurrentStat           0x074A20UL

/*--------------------------------------
BitField Name: RxLRdiILDefCurStatus
BitField Type: R/W/C
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaRxLRdiILDefCurStatMask                     cBit4

/*--------------------------------------
BitField Name: RxLAisLDefCurStatus
BitField Type: R/W/C
BitField Desc: AIS-L Defect current status in the related line. When it changes
               for 0 to 1 or vice versa, the  RxLAisLStateChgIntr bit in the
               OCN Rx Line per Alarm Interrupt Status register of the related
               line is set.
               - 0: AIS-L defect is cleared
               - 1: AIS-L defect is set
--------------------------------------*/
#define cThaRxLAisLDefCurStatMask                      cBit3

/*--------------------------------------
BitField Name: RxLOofCurStatus
BitField Type: R/W/C
BitField Desc: OOF current status in the related line. When it changes for 0 to
               1 or vice versa, the  RxLOofStateChgIntr bit in the OCN Rx Line
               per Alarm Interrupt Status register of the related line is set.
               - 0: not OOF state
               - 1: OOF state
--------------------------------------*/
#define cThaRxLOofCurStatMask                          cBit2

/*--------------------------------------
BitField Name: RxLLofCurStatus
BitField Type: R/W/C
BitField Desc: LOF current status in the related line. When it changes for 0 to
               1 or vice versa, the  RxLALofStateChgIntr bit in the OCN Rx Line
               per Alarm Interrupt Status register of the related line is set.
               - 0: not LOF state
               - 1: LOF state
--------------------------------------*/
#define cThaRxLLofCurStatMask                          cBit1

/*--------------------------------------
BitField Name: RxLLosCurStatus
BitField Type: R/W/C
BitField Desc: LOS current status in the related line. When it changes for 0 to
               1 or vice versa, the  RxLALosStateChgIntr bit in the OCN Rx Line
               per Alarm Interrupt Status register of the related line is set.
               - 0: not LOS state
               - 1: LOS state
--------------------------------------*/
#define cThaRxLLosCurStatMask                          cBit0

/*------------------------------------------------------------------------------
Reg Name: OCN Rx Line per Alarm Interrupt Enable Control
Reg Addr: 0x00074A00 - 0x00074A07
          The address format for these registers is 0x00074A00 + lineID
          Where: lineID (0 - 7) SONET/SDH line IDs
Reg Desc: This is the per Alarm interrupt enable of Rx framer and TOH
          monitoring. Each register is used to store 12 bits to enable
          interrupts when the related alarms in related line happen.
------------------------------------------------------------------------------*/
#define cThaRegOcnRxLineperAlmIntrEnCtrl            0x074A00

/*--------------------------------------
BitField Name: RxLRdiILDefStateChgIntrEn
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaRxLRdiLDefStateChgIntrEnMask              cBit4
#define cThaRxLRdiLDefStateChgIntrEnShift             4

/*--------------------------------------
BitField Name: RxLAisLDefStateChgIntrEn
BitField Type: R/W
BitField Desc: Set 1 to enable AIS-L Defect Stable state change event in the
               related line to generate an interrupt.
--------------------------------------*/
#define cThaRxLAisLDefStateChgIntrEnMask               cBit3
#define cThaRxLAisLDefStateChgIntrEnShift              3

/*--------------------------------------
BitField Name: RxLOofStateChgIntrEn
BitField Type: R/W
BitField Desc: Set 1 to enable OOF state change event in the related line to
               generate an interrupt.
--------------------------------------*/
#define cThaRxLOofStateChgIntrEnMask                   cBit2
#define cThaRxLOofStateChgIntrEnShift                  2

/*--------------------------------------
BitField Name: RxLLofStateChgIntrEn
BitField Type: R/W
BitField Desc: Set 1 to enable LOF state change event in the related line to
               generate an interrupt.
--------------------------------------*/
#define cThaRxLLofStateChgIntrEnMask                   cBit1
#define cThaRxLLofStateChgIntrEnShift                  1

/*--------------------------------------
BitField Name: RxLLosStateChgIntrEn
BitField Type: R/W
BitField Desc: Set 1 to enable LOS state change event in the related line to
               generate an interrupt.
--------------------------------------*/
#define cThaRxLLosStateChgIntrEnMask                   cBit0
#define cThaRxLLosStateChgIntrEnShift                  0

#define cThaRegOcnRxLineperAlmIntrEnCtrlNoneEditableFieldsMask cBit31_12

/*------------------------------------------------------------------------------
Reg Name: OCN Rx Line per Line Interrupt OR Status
Reg Addr: 0x00074A3F
Reg Desc: The register consists of 8 bits for 8 lines at Rx side. Each bit is
          used to store Interrupt OR status of the related line. If there are
          any bits in this register and they are enabled to raise interrupt,
          the Rx line level interrupt bit is set.
------------------------------------------------------------------------------*/
#define cThaRegOcnRxLineperLineIntrOrStat           0x074A3F

/*------------------------------------------------------------------------------
Reg Name: OCN Rx Line per Line Interrupt Enable
Reg Addr: 0x00074A3F
Reg Desc: The register consists of 8 bits for 8 lines at Rx side. Each bit is
          used to store Interrupt OR status of the related line. If there are
          any bits in this register and they are enabled to raise interrupt,
          the Rx line level interrupt bit is set.
------------------------------------------------------------------------------*/
#define cThaRegOcnRxLineperLineIntrEnable           0x074A3E
#define cThaRegOcnRxLineperLineMask(lineId)         (cBit0 << (lineId))
#define cThaRegOcnRxLineperLineShift(lineId)        (lineId)

/*--------------------------------------
BitField Name: OCNRxLLineIntr[7:0]
BitField Type: R_0
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaOcnRxLLineIntrLineIdMask(lineId)           (cBit0 << (lineId))

/*------------------------------------------------------------------------------
Reg Name: OCN Tx Framer Per Channel Control 1
Reg Addr: 0x00040A00 - 0x00040A07
          The address format for these registers is 0x00040A00 + (SliceId*8192) + lineID
          Where:SliceId (0-1): STS-12 slice ID. Slice #0 for lines from 1 to 4, Slice #1 for lines from 5 to 8
                lineID: (0 - 3) SONET/SDH line IDs
Reg Desc: Each register is used to configure operation modes for Tx framer
          engine and APS pattern inserted into APS bytes (K1 and K2 bytes) of
          the SONET/SDH line being relative with the address of the address of
          the register.
------------------------------------------------------------------------------*/
#define cThaRegOcnTxFrmrPerChnCtrl1                  0x00040A00

/*-----------
BitField Name: OcnTxK1Pat
BitField Type: R/W
BitField Desc: Pattern to insert into K1.
------------*/
#define cThaOcnTxK1PatMask                            cBit30_23
#define cThaOcnTxK1PatShift                           23

/*-----------
BitField Name: OcnTxK2Pat
BitField Type: R/W
BitField Desc: Pattern to insert into K2.
------------*/
#define cThaOcnTxK2PatMask                            cBit22_15
#define cThaOcnTxK2PatShift                           15

/*-----------
BitField Name: OcnTxS1EnB
BitField Type: R/W
BitField Desc: S1 enable
               - 1: Indicate that S1 byte inserted into S1 position is from
                    TOH output look-up table or OH insert port.
               - 0: Enable inserting S1 pattern from the OCN Tx Framer Per
                    Channel Control 2 into S1 position.
------------*/
#define cThaOcnTxS1EnBMask                            cBit14
#define cThaOcnTxS1EnBShift                           14

/*-----------
BitField Name: OcnTxApsEnB
BitField Type: R/W
BitField Desc: APS enable
               - 1: Indicate that APS bytes are inserted from TOH buffer or
                    TOH insert port.
               - 0: Enable insert APSPat[15:0] into APS bytes.
------------*/
#define cThaOcnTxApsEnBMask                           cBit13
#define cThaOcnTxApsEnBShift                          13

/*-----------
BitField Name: OcnTxZ0Md
BitField Type: R/W
BitField Desc: Modes for inserting Z0 bytes
               - 00   : All Z0 bytes are inserted by Z0pat1 in the OCN Z0
                        Insertion Pattern register.
               - 01   : All Z0 bytes are inserted by Z0pat2 the OCN Z0
                        Insertion Pattern register.
               - 10,11: 00: All Z0 bytes are inserted by TOH output look-up
                        table or OH insert port.
------------*/
#define cThaOcnTxZ0MdMask                             cBit12_11
#define cThaOcnTxZ0MdShift                            11

/*-----------
BitField Name: OcnTxReiLEnB
BitField Type: R/W
BitField Desc: REI enable
               - 1: Disable insertion of REI-L errors detected at Rx side
                    into REI-L positions. REI position will be inserted from
                    TOH output look-up table or OH insert port.
               - 0: Enable automatically inserting REI errors detected at Rx
                    side into REI position.
------------*/
#define cThaOcnTxReiLEnBMask                          cBit10
#define cThaOcnTxReiLEnBShift                         10

/*-----------
BitField Name: OcnTxRdiLEnB
BitField Type: R/W
BitField Desc: RDI-L enable
               - 1: Disable inserting RDI-L defect detected at Rx side into
                    K2[7:5]. K2[7:5] value will be inserted from TOH output
                    look-up table or OH insert port.
               - 0: Enable automatically inserting RDI-L defect detected at
                    Rx side into K2[7:5].
------------*/
#define cThaOcnTxRdiLEnBMask                          cBit9
#define cThaOcnTxRdiLEnBShift                         9

/*-----------
BitField Name: OcnTxAutoB2EnB
BitField Type: R/W
BitField Desc: Auto B2 enable
               - 1: Disable inserting calculated B2 values into B2 positions
                    automatically. B2 bytes will be inserted from TOH output
                    look-up table or OH insert port.
               - 0: Enable automatically inserting calculated B2 values into
                    B2 positions.
------------*/
#define cThaOcnTxAutoB2EnBMask                        cBit7
#define cThaOcnTxAutoB2EnBShift                       7

/*-----------
BitField Name: OcnTxAutoB1EnB
BitField Type: R/W
BitField Desc: Auto B1 enable
               - 1: Disable inserting calculated B1 values into B1 positions
                    automatically. B1 bytes will be inserted from TOH output
                    look-up table or OH insert port.
               - 0: Enable automatically inserting calculated B1 values into
                    B1 positions.
------------*/
#define cThaOcnTxAutoB1EnBMask                        cBit6
#define cThaOcnTxAutoB1EnBShift                       6

/*-----------
BitField Name: OcnTxAutoA1A2EnB
BitField Type: R/W
BitField Desc: Auto A1A2 enable
               - 1: Disable generating frame patterns (A1 and A2 bytes)
                    automatically to insert into A1, A2 positions. Frame
                    patterns will be inserted from TOH output look-up table or
                    OH insert port.
               - 0: Automatically generate frame patterns to insert into A1,
                    A2 positions.
------------*/
#define cThaOcnTxAutoA1A2EnBMask                      cBit5
#define cThaOcnTxAutoA1A2EnBShift                     5

/*-----------
BitField Name: OcnTxApsProEn
BitField Type: R/W
BitField Desc: Enable to process APS
               - 1: Enable APS process.
               - 0: Disable APS process
------------*/
#define cThaOcnTxApsProEnMask                         cBit3

/*-----------
BitField Name: OcnTxRdiLFrc
BitField Type: R/W
BitField Desc: RDI-L force
               - 1: Force RDI-L defect into transmit data.
               - 0: Not force LOS
------------*/
#define cThaOcnTxRdiLFrcMask                          cBit2
#define cThaOcnTxRdiLFrcShift                         2

/*-----------
BitField Name: OcnTxAisLFrc
BitField Type: R/W
BitField Desc: AIS-L force
               - 1: Force AIS-L defect into transmit data.
               - 0: Not force AIS-L
------------*/
#define cThaOcnTxAisLFrcMask                          cBit1
#define cThaOcnTxAisLFrcShift                         1

/*-----------
BitField Name: OcnTxScrEnB
BitField Type: R/W
BitField Desc: Scramble enable
               - 1: Disable scrambling at Tx framer.
               - 0: Enable scrambling at Tx framer.
------------*/
#define cThaOcnTxScrEnBMask                           cBit0
#define cThaOcnTxScrEnBShift                          0

#define cThaRegOcnTxFrmrPerChnCtrl1NoneEditableFieldsMask cBit31

/*------------------------------------------------------------------------------
Reg Name: OCN Rx Framer Per Channel Control
Reg Addr: 0x00040200-0x00040207
          The address format for these registers is 0x00040200 + lineID
          Where: lineID (0 - 7) SONET/SDH line IDs at receive framer
Reg Desc: Each register is used to configure for Rx framer engine of the line
          being relative with the address of the register.
------------------------------------------------------------------------------*/
#define cThaRegOcnRxFrmrPerChnCtrl                     0x00040200

/*-----------
BitField Name: OcnRxAisLFrc
BitField Type: R/W
BitField Desc: AIS-L Force
               - 1: Force AIS-L state to downstream (to XC). This mode is
                    used while the line is in loopback out mode to avoid
                    misunderstanding data at downstream.
               - 0: Not force AIS-L (Normal mode)
------------*/
#define cThaOcnRxAisLFrcMask                          cBit4
#define cThaOcnRxAisLFrcShift                         4


/*-----------
BitField Name: OcnRxScrEnB
BitField Type: R/W
BitField Desc: Scramble enable
               - 1: Disable de-scrambling of the coming data stream
               - 0: Enable de-scrambling of the coming data stream
------------*/
#define cThaOcnRxScrEnBMask                           cBit0
#define cThaOcnRxScrEnBShift                          0

#define cThaRegOcnRxFrmrPerChnCtrlNoneEditableFieldsMask cBit31_5

/*------------------------------------------------------------------------------
Reg Name: OCN SONET/SDH Line Loop-In Control
Reg Addr: 0x00040017
Reg Desc:
------------------------------------------------------------------------------*/
#define cThaRegOcnLineLoopInCtrl            0x00040017

#define cThaRegOcnLineLoopInMask(lineId)    (cBit0 << (lineId))
#define cThaRegOcnLineLoopInShift(lineId)   (lineId)

#define cThaRegOcnLineLoopInCtrlNoneEditableFieldsMask cBit31_8

/*------------------------------------------------------------------------------
Reg Name: OCN SONET/SDH Line Loop-Out Control
Reg Addr: 0xF00044
Reg Desc:
------------------------------------------------------------------------------*/
#define cThaRegOcnLineLoopOutCtrl            0xF00044

#define cThaRegOcnLineLoopOutMask(lineId)    (cBit0 << (lineId))
#define cThaRegOcnLineLoopOutShift(lineId)   (lineId)

/*------------------------------------------------------------------------------
Reg Name: OCN B1 Error Counter
Reg Addr: 0x0074900 - 0x0074907 (R_O); 0x0074940 - 0x0074947 (R2C)
          The address format for these registers is 0x0074900 + lineID (R_O);
          0x0074940 + lineID (R2C)
          Where: lineID (0 - 7) SONET/SDH line IDs at TOH monitoring
Reg Desc: Each register is used to store B1 error counter for the SONET/SDH
          line being relative with the address of the register. These counters
          are in saturation mode or rollover mode depending on the
          TohMonB1ErrCntrSatMd bit in the OCN counter control register.
------------------------------------------------------------------------------*/
#define cThaRegOcnB1ErrRoCnt                         0x0074900
#define cThaRegOcnB1ErrR2cCnt                        0x0074940
#define cThaRegOcnB1BlockErrRoCnt                    0x0074A80
#define cThaRegOcnB1BlockErrR2cCnt                   0x0074AC0

/*-----------
BitField Name: B1ErrCnt
BitField Type: R_O
BitField Desc: The B1 error counter. In case of saturation mode, when a B1
               error counter is over the B1 error counter scan threshold, the
               interrupt is generated, if it is enable.
------------*/
#define cThaB1ErrCntMask                              cBit22_0

/*------------------------------------------------------------------------------
Reg Name: OCN B2 Error Counter
Reg Addr: 0x0074908 - 0x007490F (R_O); 0x0074948 - 0x7494F (R2C)
          The address format for these registers is 0x0074908 + lineID (R_O);
          0x0074948 + lineID (R2C)
          Where: lineID (0 - 3) SONET/SDH line IDs at TOH monitoring
Reg Desc: Each register is used to store B2 error counter for the SONET/SDH
          line being relative with the address of the register. These counters
          are in saturation mode or rollover mode depending on the
          TohMonB2ErrCntrSatMd bit in the OCN counter control register.
------------------------------------------------------------------------------*/
#define cThaRegOcnB2ErrRoCnt                         0x0074908
#define cThaRegOcnB2ErrR2cCnt                        0x0074948
#define cThaRegOcnB2BlockErrRoCnt                    0x0074A88
#define cThaRegOcnB2BlockErrR2cCnt                   0x0074AC8

/*-----------
BitField Name: B2ErrCnt
BitField Type: R_O
BitField Desc: The B2 error counter. In case of saturation mode, when a B2
               error counter is over the B2 error counter scan threshold, the
               interrupt is generated, if it is enable.
------------*/
#define cThaOcnB2ErrCntMask                           cBit22_0

/*------------------------------------------------------------------------------
Reg Name: OCN REI-L Error Counter
Reg Addr: 0x0074928 - 0x007492F (R_O); 0x0074968 - 0x7496F (R2C)
          The address format for these registers is 0x0074928 + lineID (R_O);
          0x0074968 + lineID (R2C)
          Where: lineID (0 - 3) SONET/SDH line IDs at TOH monitoring
Reg Desc: Each register is used to store REI-L error counter for the SONET/SDH
          line being relative with the address of the register. These counters
          are in saturation mode or rollover mode depending on the
          TohMonReiErrCntrSatMd bit in the OCN counter control register.
------------------------------------------------------------------------------*/
#define cThaRegOcnReiLErrRoCnt                        0x0074928
#define cThaRegOcnReiLErrR2cCnt                       0x0074968
#define cThaRegOcnReiLBlockErrRoCnt                   0x0074AA8
#define cThaRegOcnReiLBlockErrR2cCnt                  0x0074AE8

/*-----------
BitField Name: OcnReiLErrCnt
BitField Type: R_O
BitField Desc: The REI error counter. In case of saturation mode, when a REI
               error counter is over the REI error counter scan threshold, the
               interrupt is generated, if it is enable.
------------*/
#define cThaOcnReiLErrCntMask                         cBit22_0

/*------------------------------------------------------------------------------
Reg Name: OCN Tx Framer Per Channel Control 2
Reg Addr: 0x00040A10 - 0x00040A17
          The address format for these registers is 0x00040A10 + lineID
          Where: lineID: (0 - 7) SONET/SDH line IDs
Reg Desc: Each register is used to configure S1 pattern to insert into S1
          position of the SONET/SDH line being relative with the address of
          the address of the register.
------------------------------------------------------------------------------*/
#define cThaRegOcnTxFrmrPerChnCtrl2                   0x00040A10

/*-----------
BitField Name: OcnTxS1Pat
BitField Type: R/W
BitField Desc: S1 pattern for insertion at Tx framer.
------------*/
#define cThaOcnTxS1PatMask                            cBit7_0

#define cThaRegOcnTxFrmrPerChnCtrl2NoneEditableFieldsMask cBit31_8

/*------------------------------------------------------------------------------
Reg Name: OCN S1 Monitoring Status
Reg Addr: 0x0074920 - 0x0074927
          The address format for these registers is 0x0074920 + lineID
          Where: lineID (0 - 3) SONET/SDH line IDs at TOH monitoring
Reg Desc: Each register is used to store status when monitoring S1 bytes of
          the SONET/SDH line being relative with the address of the register.
------------------------------------------------------------------------------*/
#define cThaRegOcnS1MonStat                           0x0074920

/*-----------
BitField Name: StbS1Val
BitField Type: R/W
BitField Desc: Stable S1 value. It is updated when detecting a new stable
               value.
------------*/
#define cThaStbS1ValMask                              cBit15_8
#define cThaStbS1ValShift                             8

/*------------------------------------------------------------------------------
Reg Name: OCN K1 Monitoring Status
Reg Addr: 0x0074910 - 0x0074917
          The address format for these registers is 0x0074910 + lineID
          Where: lineID (0 - 3) SONET/SDH line IDs at TOH monitoring
Reg Desc: Each register is used to store status when monitoring K1 bytes of
          the SONET/SDH line being relative with the address of the register.
------------------------------------------------------------------------------*/
#define cThaRegOcnK1MonStat                            0x0074910

/*-----------
BitField Name: SameK1Cnt
BitField Type: R/W
BitField Desc: The number of same contiguous K1 bytes. It is held at StbK1Thr
               value when the number of same contiguous K1 bytes is equal to
               or more than the StbK1Thr value. In this case, K1 bytes are
               stable.
------------*/
#define cThaSameK1CntMask                             cBit18_16
#define cThaSameK1CntShift                            16

/*-----------
BitField Name: StbK1Val
BitField Type: R/W
BitField Desc: Stable K1 value. It is updated when detecting a new stable
               value.
------------*/
#define cThaStbK1ValMask                              cBit15_8
#define cThaStbK1ValShift                             8

/*-----------
BitField Name: CurK1
BitField Type: R/W
BitField Desc: Current K1 byte.
------------*/
#define cThaCurK1Mask                                 cBit7_0
#define cThaCurK1Shift                                0

/*------------------------------------------------------------------------------
Reg Name: OCN K2 Monitoring Status
Reg Addr: 0x0074918 - 0x007491F
          The address format for these registers is 0x0074918 + lineID
          Where: lineID (0 - 3) SONET/SDH line IDs at TOH monitoring
Reg Desc: Each register is used to store status when monitoring K2 bytes of
          the SONET/SDH line being relative with the address of the register.
------------------------------------------------------------------------------*/
#define cThaRegOcnK2MonStat                           0x0074918

/*-----------
BitField Name: SameK2Cnt
BitField Type: R/W
BitField Desc: The number of same contiguous K2 bytes. It is held at StbK2Thr
               value when the number of same contiguous K2 bytes is equal to
               or more than the StbK2Thr value. In this case, K2 bytes are
               stable.
------------*/
#define cThaSameK2CntMask                             cBit18_16
#define cThaSameK2CntShift                            16

/*-----------
BitField Name: StbK2Val
BitField Type: R/W
BitField Desc: Stable K2 value. It is updated when detecting a new stable
               value.
------------*/
#define cThaStbK2ValMask                              cBit15_8
#define cThaStbK2ValShift                             8

/*-----------
BitField Name: CurK2
BitField Type: R/W
BitField Desc: Current K2 byte.
------------*/
#define cThaCurK2Mask                                 cBit7_0
#define cThaCurK2Shift                                0

/*------------------------------------------------------------------------------
Reg Name: OCN J0 Insertion Buffer (Thalassa)
Reg Addr: 0x0075800 - 0x7587F
          The address format for these registers is 0x0075800 + lineId*16 +
          wordID
          Where: wordId: (0 - 15): Double Word ID in 64-byte message.
          LineID: (0 - 7) SONET/SDH line IDs
Reg Desc: Each register stores one J0 byte in 64-byte message of the
          SONET/SDH line being relative with the address of the address of the
          register inserted into J0 positions.
------------------------------------------------------------------------------*/
#define cThaRegOcnJ0MsgInsBuf                            0x0075800

/*-----------
BitField Name: OcnTxJ0Msg
BitField Type: R/W
BitField Desc: J0 pattern for insertion.
------------*/
#define cThaOcnTxJ0MsgMask(position)                    ((uint32) cBit7_0 << ((uint32)(8 * position)))
#define cThaOcnTxJ0MsgShift(position)                   (8 * position)

#define cThaRegOcnJ0MsgInsBufNoneEditableFieldsMask      0

/*------------------------------------------------------------------------------
Reg Name: POH SDH/SONET Control (POH J0 Control)
Reg Addr: 0x0840B4 - 0x0840B5
          The address format for these registers is 0x084000 + VTMAX*STSMAX +
          STSMAX*2 + LineID
          Where: STSMAX = 6, VTMAX = 28, LIDMAX = 2, LineID: 0 -> LIDMAX-1
Reg Desc: SDH/SONET Control register used for control the SDH/SONET monitor
          engine.
------------------------------------------------------------------------------*/
#define cThaRegPohSdhSonetCtrl                         (0x084000 + (VTMAX * STSMAX) + (STSMAX * 2UL))

/*-----------
BitField Name: TimLAffAisLRdiLEn
BitField Type: R/W
BitField Desc:  Enable forwarding AIS to downstream when TIM-L defected
                1: Enable.
                0: Disable.
------------*/
#define cJ0TimL2AisEnbMask                            cBit28
#define cJ0TimL2AisEnbShift                           28

/*-----------
BitField Name: J0TimMonEnable
BitField Type: R/W
BitField Desc: Enable/Disable TIM monitoring
               - 1: Enable to monitor J0 TIM function
               - 0: Disable to monitor J0 TIM function
-----------*/
#define cThaJ0TimMonEnableMask                        cBit2
#define cThaJ0TimMonEnableShift                       2

/*-----------
BitField Name: J0FrmMd
BitField Type: R/W
BitField Desc: Indicates J0 Frame Mode
               - 2'b00: byte mode.
               - 2'b01: TTI message with 16 bytes length. The frame format
                   according ITU-T
               - 2'b10: TTI message with 64 bytes length. The frame format
                   has CR and LF at the end of frame
               - 2'b11: TTI message float mode. (jn byte captured cycle of a
                   buffer 64 bytes).
-----------*/
#define cThaJ0FrmMdMask                               cBit1_0
#define cThaJ0FrmMdShift                              0

#define cThaRegPohSdhSonetCtrlNoneEditableFieldsMask cBit27_25

/*------------------------------------------------------------------------------
Reg Name: POH Indirect Access Control Register
Reg Addr: 0x080100
Reg Desc: When SW want to read the TTI message from POH Engine.
          It will write the address of TTI message into the IndAdd field and set
          the bit IndAcsEnb and bit IndRnW to one. After that SW pool this register while the bit IndAcsEnb cleared.
          When the bit IndAcsEnb is cleared by POH HW engine, the read action is done.
          SW will get the data value from register POH Indirect Access Data Register.

          When SW want to write the TTI message to POH Engine.
          It will write the data to the  POH Indirect Access Data Register first.
          After that SW will write  the address of TTI message into the IndAdd field
          and set the bit IndAcsEnb to one and bit IndRnW to zero.
          After that SW pool this register while the bit IndAcsEnb cleared.
          When the bit IndAcsEnb is cleared by POH HW engine, the write action is done
------------------------------------------------------------------------------*/
#define cThaRegPohIndAcsControl                   0x080100

#define cThaRegPohIndAcsControlNoneEditableFieldsMask (cBit31 | cBit29_16)

/*------------------------------------------------------------------------------
Reg Name: OCN Rx STS/VC per Alarm Current Status
Reg Addr: 0x000521C0 - 0x000541CB
          The address format for these registers is 0x000521C0 + SliceID*8192
          + Stsid
          The address format for these registers is
          Where: SliceID: (0-1) STS-12 slice ID
          StsID: (0-11) STS-1/VC-3 ID
Reg Desc: This is the per Alarm current status of STS/VC pointer interpreter
          and STS/VC VT/TU Demux. Each register is used to store 6 bits to
          store current status of 6 alarms in the STS/VC.
------------------------------------------------------------------------------*/
#define cThaRegOcnRxStsVCperAlmCurrentStat               0x0521C0

/*--------------------------------------
BitField Name: StsPiAisCurStatus
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaStsPiUneqPCurStatMask                        cBit3

/*--------------------------------------
BitField Name: StsPiAisCurStatus
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaStsPiAisCurStatMask                        cBit2

/*--------------------------------------
BitField Name: StsPILopCurStatus
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaStsPILopCurStatMask                        cBit1

/*--------------------------------------
BitField Name: StsLomCurStatus
BitField Type: R/W
BitField Desc: LOM current status in the related STS/VC. When it changes for 0
               to 1 or vice versa, the  StsPiStsLomStateChgIntr bit in the OCN
               Rx STS/VC per Alarm Interrupt Status register of the related
               STS/VC is set.
--------------------------------------*/
#define cThaStsLomCurStatMask                          cBit0

/*------------------------------------------------------------------------------
Reg Name: OCN Rx STS/VC per Alarm Interrupt Status
Reg Addr: 0x000521A0 - 0x000541AB
          The address format for these registers is 0x000521A0 + SliceID*8192
          + Stsid
          The address format for these registers is
          Where: SliceID: (0-1) STS-12 slice ID
          StsID: (0-11) STS-1/VC-3 ID
Reg Desc: This is the per Alarm interrupt status of STS/VC pointer interpreter
          and STS/VC VT/TU Demux. Each register is used to store 6 sticky bits
          for 6 alarms in the STS/VC.
------------------------------------------------------------------------------*/
#define cThaRegOcnRxStsVCperAlmIntrStat             0x0521A0

/*--------------------------------------
BitField Name: StsPiAisStateChgIntr
BitField Type: R/W/C
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaStsPiUneqPStateChgIntrMask                   cBit3

/*--------------------------------------
BitField Name: StsPiAisStateChgIntr
BitField Type: R/W/C
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaStsPiAisStateChgIntrMask                   cBit2

/*--------------------------------------
BitField Name: StsPILopStateChgIntr
BitField Type: R/W/C
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaStsPILopStateChgIntrMask                   cBit1

/*--------------------------------------
BitField Name: StsLomStateChgIntr
BitField Type: R/W/C
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaStsLomStateChgIntrMask                     cBit0

/*------------------------------------------------------------------------------
Reg Name: OCN Rx PP Per STS/VC Payload Control
Reg Addr: 0x000520C0 - 0x000540CB
          The address format for these registers is 0x000520C0 + SliceID*8192 +
          StsID
          Where: SliceID: (0) STS-12 slice ID
          Stsid: (0-11) STS-1/VC-3/TUG-3 ID in the STS-12 slice
Reg Desc: These registers configure modes of STS-1/VC-3/TUG-3 channels at Rx
          pointer processor. Each register is used to configure the
          STS-1/VC-3/TUG-3 being relative with the address of the register to
          be in VT/TU mode or not. If it is in VT/TU mode, it is also used to
          configure this channel is in STS-1/VC-3 or TUG-3 mode and types of
          VT/TUs in the STS-1/VC-3/TUG-3.
------------------------------------------------------------------------------*/
#define cThaRegOcnRxPpPerStsVcPldCtrl                 0x000520C0

/*-----------
BitField Name: OcnRxPpStsDemuxSpeType
BitField Type: R/W
BitField Desc: the kind of SPE: TUG-3 or VC-3/STS SPE
               - 0: Disable processing pointers of all VT/TUs in this SPE.
               - 1: VC-3 type or STS SPE containing VT/TU exception TU-3
               - 2: TUG-3 type containing VT/TU exception TU-3
               - 3: TUG-3 type containing TU-3.
------------*/
#define cThaOcnRxPpStsDemuxSpeTypeMask                cBit15_14
#define cThaOcnRxPpStsDemuxSpeTypeShift               14

/*-----------
BitField Name: OcnRxPpStsDemuxTug2Type
BitField Type: R/W
BitField Desc: Define types of VT/TUs in TUG-2
               - 0: TU11 type
               - 1: TU12 type
               - 2: VT3 type
               - 3: TU2 type
------------*/
#define cThaOcnRxPpStsDemuxTug2TypeMask(tug2Id)     (cBit1_0 << (uint16)((tug2Id) * 2))
#define cThaOcnRxPpStsDemuxTug2TypeShift(tug2Id)    ((tug2Id) * 2)

#define cThaRegOcnRxPpPerStsVcPldCtrlNoneEditableFieldsMask cBit31_16

/*------------------------------------------------------------------------------
Reg Name: POH CPE STS Alarm Interrupt Sticky
Reg Addr: 0x086800 - 0x086FFF
          The address format for these registers is 0x086800 + VTMAX * STSMAX + STSID
          Where,
          STSMAX = 6, VTMAX = 28, STSID: 0 -> STSMAX - 1
Reg Desc: This is the POH CPE STS alarm interrupt sticky register.
          It will be used to indicate and sticky the event states change when
          the POH CPE engine detected a changing of  event state.
------------------------------------------------------------------------------*/

#define cThaRegPohCpeStsAlmIntrStk                  (0x086800 + (VTMAX * STSMAX))

/*--------------------------------------
BitField Name: POHRdiStbChgInt
BitField Type: R/W/C
BitField Desc: It is the RDI-P Stable Change interrupt sticky bit.
               It will be set to one when there is a change
               of RDI_P state at the POH CPE engine.
BitField Bits: 6
--------------------------------------*/
#define cThaPohRdiStbChgIntMask                        cBit6

/*--------------------------------------
BitField Name: POHUneqStbChgInt
BitField Type: R/W/C
BitField Desc: It is the Uneq-P Stable Change interrupt sticky bit.
               It will be set to one when there is a change
               of Uneq-P state at the POH CPE engine.
BitField Bits: 4
--------------------------------------*/
#define cThaPohUneqStbChgIntMask                        cBit4

/*--------------------------------------
BitField Name: POHPlmStbChgInt
BitField Type: R/W/C
BitField Desc: It is the PLM-P Stable Change interrupt sticky bit.
               It will be set to one when there is a change
               of PLM-P state at the POH CPE engine.
BitField Bits: 3
--------------------------------------*/
#define cThaPohPlmStbChgIntMask                        cBit3

/*--------------------------------------
BitField Name: POHVcAisStbChgInt
BitField Type: R/W/C
BitField Desc: It is the VcAIS-P Stable Change interrupt sticky bit.
               It will be set to one when there is a change
               of VcAIS-P state at the POH CPE engine.
BitField Bits: 2
--------------------------------------*/
#define cThaPohVcAisStbChgIntMask                        cBit2

/*--------------------------------------
BitField Name: POHTtiTimStbChgInt
BitField Type: R/W/C
BitField Desc: It is the TIM-P Stable Change interrupt sticky bit.
               It will be set to one when there is a change
               of TIM-P state at the POH CPE engine.
BitField Bits: 1
--------------------------------------*/
#define cThaPohTtiTimStbChgIntMask                        cBit1

/*------------------------------------------------------------------------------
Reg Name: OCN Rx STS/VC per Alarm Interrupt Enable Control
Reg Addr: 0x00052180 - 0x0005418B
          The address format for these registers is 0x00052180 + SliceID*8192
          + Stsid
          The address format for these registers is
          Where: SliceID: (0-1) STS-12 slice ID
          StsID: (0-11) STS-1/VC-3 ID
Reg Desc: This is the per Alarm interrupt enable of STS/VC pointer interpreter
          and STS/VC VT/TU Demux. Each register is used to store 6 bits to
          enable interrupts when the related alarms in related STS/VC happen.
------------------------------------------------------------------------------*/
#define cThaRegOcnRxStsVCperAlmIntrEnCtrl           0x052180


/*--------------------------------------
BitField Name: StsPiStsConcDetIntrEn
BitField Type: R/W
BitField Desc: Set 1 to enable Concatenation Detection event in the related
               STS/VC to generate an interrupt.
--------------------------------------*/

/*--------------------------------------
BitField Name: StsPiStsNewPtrDetIntrEn
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/

/*--------------------------------------
BitField Name: StsPiStsNdfIntrEn
BitField Type: R/W
BitField Desc: Set 1 to enable NDF event in the related STS/VC to generate an
               interrupt.
--------------------------------------*/

/*--------------------------------------
BitField Name: StsPiAisStateChgIntrEn
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/

/*--------------------------------------
BitField Name: StsPiAisStateChgIntrEn
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaStsPiAisStateChgIntrEnMask                 cBit2
#define cThaStsPiAisStateChgIntrEnShift                2

/*--------------------------------------
BitField Name: StsPILopStateChgIntrEn
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaStsPILopStateChgIntrEnMask                 cBit1
#define cThaStsPILopStateChgIntrEnShift                1

/*--------------------------------------
BitField Name: StsLomStateChgIntrEn
BitField Type: R/W
BitField Desc: Set 1 to enable change LOM state event from LOM to no LOM and
               vice versa in the related STS/VC to generate an interrupt.
--------------------------------------*/

#define cThaRegOcnRxStsVCperAlmIntrEnCtrlNoneEditableFieldsMask cBit31_4

/*------------------------------------------------------------------------------
Reg Name: POH CPE STS Alarm Interrupt Enable Control
Reg Addr: 0x086000 - 0x0867FF
          The address format for these registers is 0x086000 + VTMAX * STSMAX + STSID
          Where: STSMAX = 6, VTMAX = 28, STSID: 0 -> STSMAX - 1
Reg Desc: This is the per channel POH CPE STS alarm interrupt enable control register.
          It is using to enable each event an alarm when it is detected.
------------------------------------------------------------------------------*/
#define cThaRegPohCpeStsAlmIntrEnCtrl               0x086000
#define cThaRegPohCpeTu3AlmIntrEnCtrl               0x08E210

/*--------------------------------------
BitField Name: POHRdiStbChgIntEnb
BitField Type: R/W
BitField Desc: It is the RDI-P Stable Changed interrupt enable control. It is active high.
               When it is high, the POH CPE will send an alarm interrupt signal to SW
               when the CPE engine detected a stable change of RDI-P.
BitField Bits: 6
--------------------------------------*/
#define cThaPohRdiStbChgIntEnbMask                  cBit6
#define cThaPohRdiStbChgIntEnbShift                 6

/*--------------------------------------
BitField Name: POHUneqStbChgIntEnb
BitField Type: R/W
BitField Desc: It is the Uneq-P Stable Changed interrupt enable control. It is active high.
               When it is high, the POH CPE will send an alarm interrupt signal to SW
               when the CPE engine detected a stable change of Uneq-P event.
BitField Bits: 4
--------------------------------------*/
#define cThaPohUneqStbIntEnbMask                     cBit4
#define cThaPohUneqStbIntEnbShift                    4

/*--------------------------------------
BitField Name: POHPlmStbChgIntEnb
BitField Type: R/W
BitField Desc: It is the PLM-P Stable Changed interrupt enable control. It is active high.
               When it is high, the POH CPE will send an alarm interrupt signal to SW
               when the CPE engine detected a stable change of PLM-P event.
BitField Bits: 3
--------------------------------------*/
#define cThaPohPlmStbIntEnbMask                       cBit3
#define cThaPohPlmStbIntEnbShift                      3

/*--------------------------------------
BitField Name: POHVcAisStbChgIntEnb
BitField Type: R/W
BitField Desc: It is the VC AIS Stable Changed interrupt enable control. It is active high.
               When it is high, the POH CPE will send an alarm interrupt signal to SW
               when the CPE engine detected a stable change of  VC AIS event.
BitField Bits: 2
--------------------------------------*/
#define cThaPohVcAisStbIntEnbMask                       cBit2
#define cThaPohVcAisStbIntEnbShift                      2

/*--------------------------------------
BitField Name: POHTtiTimStbChgIntEnb
BitField Type: R/W
BitField Desc: It is the TTI-P TIM Stable Changed interrupt enable control. It is active high.
               When it is high, the POH CPE will send an alarm interrupt signal to SW
               when the CPE engine detected  a stable change of TIM-P event.
BitField Bits: 1
--------------------------------------*/
#define cThaPohTtiTimStbIntEnbMask                       cBit1
#define cThaPohTtiTimStbIntEnbShift                      1

/*------------------------------------------------------------------------------
Reg Name: OCN Rx STS/VC Per STS/VC Interrupt OR Status
Reg Addr: 0x00521EF - 0x00541EF
          The address format for these registers is 0x00521EF + SliceID*8192
          Where: SliceID: (0-1) STS-12 slice ID
Reg Desc: The register consists of 12 bits for 12 STS/VC of the related STS-12
          slice in the STS/VC Pointer interpreter block. Each bit is used to
          store Interrupt OR status of the related STS/VC.
------------------------------------------------------------------------------*/
#define cThaRegOcnRxStsVCPerStsVCIntrOrStat         0x0521EF

/*--------------------------------------
BitField Name: OCNRxStsStsIntr[11:0]
BitField Type: R_0
BitField Desc: Set to 1 to indicate that there is any interrupt status bit in
               the OCN Rx STS/VC per Alarm Interrupt Status register  of the
               related STS/VC to be set and they are enabled to raise interrupt
               Bit 0 for STS-1/VC-3/TUG-3 #0, respectively.
--------------------------------------*/
#define cThaOcnRxStsStsIntrIdMask(stsId)               (cBit0 << (stsId))

/*------------------------------------------------------------------------------
Reg Name: OCN Tx PG Per Channel Control
Reg Addr: 0x00062800-0x0006497F
          The address format for these registers is 0x00062800 + SliceID*8192
          + StsID*32 + Tug2ID*4 + VtnID
          Where: SliceID: (1) STS-12 slice ID
          StsID: (0-11) STS-1/VC-3 ID
          Tug2ID: (0) TUG-2/VTG ID
          VtnID: (0) VT/TU number ID in the TUG-2/VTG
Reg Desc: Each register is used to configure for STS/VC PG of the channel
          being relative with the address of the STS/VC at Tx direction.
------------------------------------------------------------------------------*/
#define cThaRegOcnTxPGPerChnCtrl              0x00062800

/*--------------------------------------
BitField Name: TxPgSts_Vt_TULopPFrc
BitField Type: R/W
BitField Desc: Set 1 to Insert invalid value into pointer to cause LOP
event at remote site
--------------------------------------*/
#define cThaTxPgStsVtTuLopPFrcMask                         cBit21
#define cThaTxPgStsVtTuLopPFrcShift                        21

/*--------------------------------------
BitField Name: TxPgSts_Vt_TUBipErins
BitField Type: R/W
BitField Desc: Set 1 to insert errors into TU3 B3 or TU BIP2.
  This bit is ignored when  TxPgSts_Vt_PohInsEn (bit18) bit is clear.
--------------------------------------*/
#define cThaTxPgStsVtBipErrInsMask                        cBit20
#define cThaTxPgStsVtBipErrInsShift                       20

/*--------------------------------------
BitField Name: TxPgSts_Vt_EparEn
BitField Type: R/W
BitField Desc: EPAR Mode
               - 1: Enable.
               - 0: Disable.
--------------------------------------*/
#define cThaTxPgStsVtEparEnMask                        cBit19
#define cThaTxPgStsVtEparEnShift                       19

/*--------------------------------------
BitField Name: TxPgSts_Vt_PohInsEn
BitField Type: R/W
BitField Desc: SS bits or VT/TU type insert enable bit
               - 1: Enable insertion Poh.
               - 0: Disable insertion.
--------------------------------------*/
#define cThaTxPgStsVtPohInsEnMask                        cBit18
#define cThaTxPgStsVtPohInsEnShift                       18

/*--------------------------------------
BitField Name: TxPg2StsSSInsEn
BitField Type: R/W
BitField Desc: SS bits or VT/TU type insert enable bit
               - 1: Enable insertion of TxPg2StsSSInsPat[1:0] into SS bits
                 position in STS/VC pointers.
               - 0: SS bits position in STS/VC pointer is passed through.
--------------------------------------*/
#define cThaTxPg2StsSSInsEnMask                        cBit15
#define cThaTxPg2StsSSInsEnShift                       15

/*--------------------------------------
BitField Name: TxPgSts_Vt_TuSSInsPat[1:0]
BitField Type: R/W
BitField Desc: Pattern to insert to SS bits or VT/TU type positions in
               STS/VC/VT/TU the pointer.
--------------------------------------*/
#define cThaTxPgStsVtTuSSInsPatMask                    cBit17_16
#define cThaTxPgStsVtTuSSInsPatShift                   16

/*--------------------------------------
BitField Name: TxPgSts_Vt_TuUneqPFrc
BitField Type: R/W
BitField Desc: Path Un-equip force
               - 1: Insert all zeros into all payload bytes with valid
                 pointers.
               - 0: Path Un-equip forcing is disabled.
--------------------------------------*/
#define cThaTxPgStsVtTuUneqPFrcMask                    cBit14
#define cThaTxPgStsVtTuUneqPFrcShift                   14

/*--------------------------------------
BitField Name: TxPgSts_Vt_TuAisPFrc
BitField Type: R/W
BitField Desc: Path AIS force
               - 1: Insert all AIS into all payload bytes and pointer bytes.
               - 0: Path AIS forcing is disabled
--------------------------------------*/
#define cThaTxPgStsVtTuAisPFrcMask                     cBit13
#define cThaTxPgStsVtTuAisPFrcShift                    13

/*--------------------------------------
BitField Name: TxPgStsSlvInd
BitField Type: R/W
BitField Desc: Slave bit
               - 1: Indicate that the STS/VC is the slave STS/VC in the
                 concatenation.
               - 0: Indicate that the STS/VC is not in the concatenation or it
                 is the master STS/VC if it is the concatenation.
--------------------------------------*/
#define cThaTxPgStsSlvIndMask                          cBit8
#define cThaTxPgStsSlvIndShift                         8

/*--------------------------------------
BitField Name: TxPgStsMastID[4:0]
BitField Type: R/W
BitField Desc: (0-11): the master STS/VC ID. This is the ID of the master
               STS/VC/ in the concatenation that contains this STS/VC/. If this
               channel is not in the concatenation, Masterid[4:0] is ignored.
--------------------------------------*/
#define cThaTxPgStsMastIdMask                          cBit4_0
#define cThaTxPgStsMastIdShift                         0

#define cThaRegOcnTxPGPerChnCtrlNoneEditableFieldsMask (cBit31_21 | cBit12_9 | cBit7_5)

/*------------------------------------------------------------------------------
Reg Name: OCN Tx POH Insertion Per Channel Control
Reg Addr: 0x000620C0-0x0005018B
          The address format for these registers is 0x000620C0 + SliceID*8192 +
          StsID
          Where: SliceID: (1) STS-12 slice ID
          StsID: (0-11) STS-1/VC-3 ID
Reg Desc: Each register is used to configure for STS/VC PG of the channel
          being relative with the address of the STS/VC at Tx direction.
------------------------------------------------------------------------------*/
#define cThaRegOcnTxPohInsPerChnCtrl          0x000620C0

/*--------------------------------------
BitField Name: TxPg2StsLomPFrc
BitField Type: R/W
BitField Desc: Set high to Insert invalid (zero) into multiframe indicator
in H4 byte to cause LOM event at remote site
--------------------------------------*/
#define cThaTxPg2StsLomPFrcMask                       cBit15
#define cThaTxPg2StsLomPFrcShift                      15

/*--------------------------------------
BitField Name: TxPg2StsLopPFrc
BitField Type: R/W
BitField Desc: Set high to Insert invalid value into pointer to cause LOP
event at the remote site.
--------------------------------------*/
#define cThaTxPg2StsLopPFrcMask                       cBit14
#define cThaTxPg2StsLopPFrcShift                      14

/*--------------------------------------
BitField Name: TxPg2StsB3ErIns
BitField Type: R/W
BitField Desc: Set high to insert errors into B3 byte in VC-3/VC-4.
 This bit is ignored when POH insertion is disabled (VC-3/VC-4 CEP).
--------------------------------------*/
#define cThaTxPg2StsB3ErrInsMask                       cBit13
#define cThaTxPg2StsB3ErrInsShift                      13

/*--------------------------------------
BitField Name: TxPg2StsSSInsPat[1:0]
BitField Type: R/W
BitField Desc: Pattern to insert to SS bits  in STS/VC the pointer.
--------------------------------------*/
/*_ERRORS___BIT_FIELD___RE_DEFINE
TxPg2StsSSInsPat in 1.2.9.OCN Tx POH Insertion Slide 1 Per Channel Control
 have declare in 1.2.8.1. OCN Tx PG Slide 2 Per Channel Control
*/
#define cThaTxPg2StsSSInsPatMask                       cBit12_11
#define cThaTxPg2StsSSInsPatShift                      11

/*--------------------------------------
BitField Name: TxPg2StsUneqPFrc
BitField Type: R/W
BitField Desc: Path Un-equip force
               - 1: Insert all zeros into all payload bytes with valid
                 pointers.
               - 0: Path Un-equip forcing is disabled.
--------------------------------------*/
/*_ERRORS___BIT_FIELD___RE_DEFINE
TxPg2StsUneqPFrc in 1.2.9.OCN Tx POH Insertion Slide 1 Per Channel Control
 have declare in 1.2.8.1. OCN Tx PG Slide 2 Per Channel Control
*/
#define cThaTxPg2StsUneqPFrcMask                       cBit10
#define cThaTxPg2StsUneqPFrcShift                      10

/*--------------------------------------
BitField Name: TxPg2StsAisPFrc
BitField Type: R/W
BitField Desc: Path AIS force
               - 1: Insert all AIS into all payload bytes and pointer bytes.
               - 0: Path AIS forcing is disabled
--------------------------------------*/
/*_ERRORS___BIT_FIELD___RE_DEFINE
TxPg2StsAisPFrc in 1.2.9.OCN Tx POH Insertion Slide 1 Per Channel Control
 have declare in 1.2.8.1. OCN Tx PG Slide 2 Per Channel Control
*/
#define cThaTxPg2StsAisPFrcMask                        cBit9
#define cThaTxPg2StsAisPFrcShift                       9

/*--------------------------------------
BitField Name: TxPg2StsSlvInd
BitField Type: R/W
BitField Desc: Slave bit
               - 1: Indicate that the STS/VC is the slave STS/VC in the
                 concatenation.
               - 0: Indicate that the STS/VC is not in the concatenation or it
                 is the master STS/VC if it is the concatenation.
--------------------------------------*/
/*_ERRORS___BIT_FIELD___RE_DEFINE
TxPg2StsSlvInd in 1.2.9.OCN Tx POH Insertion Slide 1 Per Channel Control
 have declare in 1.2.8.1. OCN Tx PG Slide 2 Per Channel Control
*/
#define cThaTxPg2StsSlvIndMask                         cBit8
#define cThaTxPg2StsSlvIndShift                        8

/*--------------------------------------
BitField Name: TxPg2StsMastID[4:0]
BitField Type: R/W
BitField Desc: (0-11): the master STS/VC ID. This is the ID of the master
               STS/VC/ in the concatenation that contains this STS/VC/. If this
               channel is not in the concatenation, Masterid[4:0] is ignored.
               Arrive Technologies Inc. Register Description     This
               controlled document is the proprietary of Arrive Technologies
               Inc.. Any duplication, reproduction, or transmission to
               unauthorized parties is prohibited. Copyright Staff Website:
               www.arrivetechnologies.com        AT4848 - Highly-Integrated
               OC-48/STM-16 ADM-on-a-chip Register Descriptions      Copyright
               ? Staff. Arrive Technologies Inc.     Page ii Internal Doc.
               Subject to Change   Thalassa 6200 Register Descriptions
               Copyright ? Staff. Arrive Technologies Inc.     Page i Internal
               Doc. Subject to Change
               Thalassa 6200 Register Descriptions     Copyright ? Staff.
               Arrive Technologies Inc.     Page iv Internal Doc. Subject to
               Change
               Thalassa 6200 Register Descriptions     Copyright ? Staff.
               Arrive Technologies Inc.     Page v Internal Doc. Subject to
               Change
               AT4848 - Highly-Integrated OC-48/STM-16 ADM-on-a-chip Register
               Descriptions      Copyright ? Staff. Arrive Technologies Inc.
               Page 11 Internal Doc. Subject to Change   Thalassa 6200 Register
               Descriptions   Copyright ? Staff. Arrive Technologies Inc.
               Page 6 Internal Doc. Subject to Change
--------------------------------------*/
/*_ERRORS___BIT_FIELD___RE_DEFINE
TxPg2StsMastID in 1.2.9.OCN Tx POH Insertion Slide 1 Per Channel Control
 have declare in 1.2.8.1. OCN Tx PG Slide 2 Per Channel Control
*/
#define cThaTxPg2StsMastIDMask                         cBit4_0
#define cThaTxPg2StsMastIDShift                        0

#define cThaRegOcnTxPohInsPerChnCtrlNoneEditableFieldsMask (cBit31_14 | cBit7_5)

/*------------------------------------------------------------------------------
Reg Name: OCN Rx STS PI Per Channel Control
Reg Addr: 0x00052040 - 0x0005404B
          The address format for these registers is 0x00052040 + SliceID*8192 +
          Stsid
          Where: SliceID: (0-1) STS-12 slice ID
          Stsid: (0-11) STS-1/VC-3 ID
Reg Desc: Each register is used to configure for STS pointer interpreter
          engines of the STS-1/VC-3 being relative with the address of the
          address of the register. It is noted that these registers must be
          accessed in indirect mode through OCN Indirect Access Control
          register.
------------------------------------------------------------------------------*/
#define cThaRegOcnRxStsPiPerChnCtrl                   0x00052040

/*-----------
BitField Name: RxStsPiStsTermEn
BitField Type: R/W
BitField Desc: Enable to terminate the related STS/VC. It means that STS POH
               defects related to the STS/VC to generate AIS to downstream:
               - 1: Enable.
               - 0: Disable.
------------*/
#define cThaRxStsPiStsTermEnMask                         cBit19
#define cThaRxStsPiStsTermEnShift                        19

/*-----------
BitField Name: RxStsPiSsDetPat
BitField Type: R/W
BitField Desc: This is the pattern that is used to compare with the extracted
               SS bits from receive direction.
------------*/
#define cThaRxStsPiSsDetPatMask                       cBit12_11
#define cThaRxStsPiSsDetPatShift                      11

/*-----------
BitField Name: RxStsPiSsDetEn
BitField Type: R/W
BitField Desc: SS detecting enable
               - 1: Enable checking SS bits in the pointer interpreter
                    engine.
               - 0: SS bit checking function in the pointer interpreter
                    engine is disabled.
------------*/
#define cThaRxStsPiSsDetEnMask                        cBit10
#define cThaRxStsPiSsDetEnShift                       10

/*-----------
BitField Name: RxStsPiSlvInd
BitField Type: R/W
BitField Desc: Slaver bit
               - 1: Indicate that the STS-1/VC-3 is the slaver STS-1/VC-3 in
                    the concatenation.
               - 0: Indicate that the STS-1/VC-3 is not in the concatenation
                    or the master STS-1/VC-3 if it is the concatenation.
------------*/
#define cThaRxStsPiSlvIndMask                         cBit8
#define cThaRxStsPiSlvIndShift                        8

/*-----------
BitField Name: RxStsPiMastId
BitField Type: R/W
BitField Desc: the master STS-1 ID. This is the ID of the master STS-1 in the
               concatenation that contains this STS-1. If this STS-1 is not in
               the concatenation, this field is ignored. It is noted that all
               STS-1/VC-3s in the concatenation group are in one SONET/SDH
               line.
------------*/
#define cThaRxStsPiMastIdMask                         cBit3_0
#define cThaRxStsPiMastIdShift                        0

#define cThaRegOcnRxStsPiPerChnCtrlNoneEditableFieldsMask (cBit31_20 | cBit17_13 | cBit7_4)

/*------------------------------------------------------------------------------
Reg Name: OCN Rx VT/TU Per STS/VC Interrupt OR Status
Reg Addr: 0x0053CFF - 0x0057CFF
          The address format for these registers is 0x0053CFF + SliceID*8192
          Where: SliceID: (0-1) STS-12 slice ID
Reg Desc: The register consists of 12 bits for 12 STS/VCs of the related
          STS-12 slice in the VT/TU Pointer interpreter block. Each bit is
          used to store Interrupt OR status of the related STS/VC.
------------------------------------------------------------------------------*/
#define cThaRegOcnRxVtTUPerStsVCIntrOrStat (0x053CFF)
#define cThaRegOcnRxVtTuPerStsStsIdMask(stsId) (cBit0 << (stsId))

/*------------------------------------------------------------------------------
Reg Name: OCN Rx VT/TU Per STS/VC Interrupt Enable Control
Reg Addr: 0x0053CFE - 0x0057CFE
          The address format for these registers is 0x0053CFE` + SliceID*8192
          Where: SliceID: (0-1) STS-12 slice ID
Reg Desc: The register consists of 12 bits for 12 STS/VCs in the related
          STS-12 slice to enable interrupts when alarms in related STS/VC
          happen.
------------------------------------------------------------------------------*/
#define cThaRegOcnRxVtTUPerStsVCIntrEnCtrl (0x0053CFE)

/*------------------------------------------------------------------------------
Reg Name: OCN Rx VT/TU Per VT/TU Interrupt OR Status
Reg Addr: 0x0053C00 - 0x0057C00
          The address format for these registers is 0x0053C00 + SliceID*8192 +
          Stsid
          Where: SliceID: (0-1) STS-12 slice ID
          StsID: (0-11) STS-1/VC-3 ID
Reg Desc: The register consists of 28 bits for 28 VT/TUs of the related STS/VC
          in the VT/TU Pointer interpreter block. Each bit is used to store
          Interrupt OR status of the related VT/TU.
------------------------------------------------------------------------------*/
#define cThaRegOcnRxVtTUPerVtTUIntrVtIdMask(vtId) (cBit0 << (vtId))

/*------------------------------------------------------------------------------
Reg Name: POH CPE STS Control
Reg Addr: 0x084000 - 0x0841FF
          The address format for these registers is 0x084000 + STSMAX * VTMAX + STSID
          Where: STSMAX = 6, VTMAX = 28, STSID: 0 -> STSMAX-1
Reg Desc: This is the Common Processor Engine Control Register for STS
------------------------------------------------------------------------------*/
#define cThaRegPohCpeStsTu3Ctrl                       (0x084000)

/*-----------
BitField Name: Plm2AisEnb
BitField Type: R/W
BitField Desc: Enable forwarding AIS to downstream when PLM defected. Active high.
-----------*/
#define cThaPlm2AisEnbMask                            cBit31
#define cThaPlm2AisEnbShift                           31

/*-----------
BitField Name: VcAis2AisEnb
BitField Type: R/W
BitField Desc: Enable forwarding AIS to downstream when VCAIS defected. Active high.
-----------*/
#define cThaVcAis2AisEnbMask                          cBit30
#define cThaVcAis2AisEnbShift                         30

/*-----------
BitField Name: Uneq2AisEnb
BitField Type: R/W
BitField Desc: Enable forwarding AIS to downstream when Uneq defected. Active high.
-----------*/
#define cThaUneq2AisEnbMask                            cBit29
#define cThaUneq2AisEnbShift                           29

/*-----------
BitField Name: Uneq2AisEnb
BitField Type: R/W
BitField Desc: Enable forwarding AIS to downstream when Uneq defected. Active high.
-----------*/
#define cThaTim2AisEnbMask                            cBit28
#define cThaTim2AisEnbShift                           28

/*-----------
BitField Name: RdiInsVal
BitField Type: R/W
BitField Desc: RDI (G1[b5]) insert value
-----------*/
#define cThaRdiInsValMask                            cBit23
#define cThaRdiInsValShift                           23

/*-----------
BitField Name: ERdiInsVal
BitField Type: R/W
BitField Desc: ERDI (G1[b6-b7]) insert value
-----------*/
#define cThaERdiInsValMask                            cBit22_21
#define cThaERdiInsValShift                           21

/*-----------
BitField Name: SpareInsVal
BitField Type: R/W
BitField Desc: The spare bit (G1[b8]) of G1 byte insert value
-----------*/
#define cThaSpareInsValMask                            cBit20
#define cThaSpareInsValShift                           20

/*-----------
BitField Name: ERdiInsEnb
BitField Type: R/W
BitField Desc: Enable ERDI function for insertion
               1 - Enable ERDI
               0 - Disable ERDI. G1[b6-b7] inserted from ERdiInsVal[1:0]
-----------*/
#define cThaERdiInsEnbMask                            cBit19
#define cThaERdiInsEnbShift                           19

/*-----------
BitField Name: RdiInsEnb
BitField Type: R/W
BitField Desc: 1 - Enable insertion of RDI value from RdiInsVal and ERdiInsVal.
               0 - HW automatically inserts.
-----------*/
#define cThaRdiInsEnbMask                            cBit18
#define cThaRdiInsEnbShift                           18

/*-----------
BitField Name: AisLopInsMsk
BitField Type: R/W
BitField Desc: 1 - Disable insert RDI/ERDI defect when AIS or LOP detected.
               0 - Enable (auto insert mode).
-----------*/
#define cThaAisLopInsMskMask                          cBit17
#define cThaAisLopInsMskShift                         17

/*-----------
BitField Name: TimInsMsk
BitField Type: R/W
BitField Desc: 1 - Disable insert RDI/ERDI defect when TIM detected.
               0 - Enable (auto insert mode).
-----------*/
#define cThaTimInsMskMask                             cBit16
#define cThaTimInsMskShift                            16

/*-----------
BitField Name: UneqInsMsk
BitField Type: R/W
BitField Desc: 1 - Disable insert RDI/ERDI defect when UNEQ detected.
               0 - Enable (auto insert mode).
-----------*/
#define cThaUneqInsMskMask                             cBit15
#define cThaUneqInsMskShift                            15

/*-----------
BitField Name: PlmInsMsk
BitField Type: R/W
BitField Desc: 1 - Disable insert RDI (ERDI) defect when PLM detected.
               0 - Enable (auto insert mode).
-----------*/
#define cThaPlmInsMskMask                             cBit14
#define cThaPlmInsMskShift                            14

/*-----------
BitField Name: ERdiEn
BitField Type: R/W
BitField Desc: Enable ERDI mode. Active high.
-----------*/
#define cThaERdiEnMask                             cBit13
#define cThaERdiEnShift                            13

/*-----------
BitField Name: PslExptVal[7:0]
BitField Type: R/W
BitField Desc: The expected value of Path Signal Label
-----------*/
#define cThaPslExptValMask                          cBit11_4
#define cThaPslExptValShift                         4

/*-----------
BitField Name: J1MonEnb
BitField Type: R/W
BitField Desc: Enable to monitor J1 message. Active high.
-----------*/
#define cThaJ1MonEnbMask                            cBit2
#define cThaJ1MonEnbShift                           2

/*-----------
BitField Name: J1FrmMd[1:0]
BitField Type: R/W
BitField Desc: Indicates J1 Frame Mode
               2’b00: byte mode.
               2’b01: TTI message with 16 bytes length. The frame format according ITU-T
               2’b10: TTI message with 64 bytes length. The frame format has CR and LF at the end of frame
               2’b11: TTI message float mode. (J1 byte captured cycle of a buffer 64 bytes).
-----------*/
#define cThaJ1FrmMdMask                            cBit1_0
#define cThaJ1FrmMdShift                           0

#define cThaRegPohCpeStsTu3CtrlNoneEditableFieldsMask cBit27_25

/*-----------
BitField Name: MsgByte0MsgByte1MsgByte2MsgByte3
BitField Type: R/W
BitField Desc: ERRORS_LACK_OF_DESCRIPTION
-----------*/
#define cThaTraceMsgInsByteMask(byteId)                ((uint32)cBit31_24 >> (uint32)((byteId) * 8))
#define cThaTraceMsgInsByteShift(byteId)               (8 * (3 - ((byteId) % 4)))


/*------------------------------------------------------------------------------
Reg Name: POH TTI Expected/Accepted Message Buffer
Reg Addr: 0x08D400-0x08D5FF
          The address format for these registers is 0x50_D400 + BufID*16 +
          WCNT
          Where: BufID: 0 -> 31
          WCNT: 0 -> 15
Reg Desc: TTI Expected Message Buffer use to store expected or accepted TTI
          Message. The MsgByte0 is MSB byte of message.
------------------------------------------------------------------------------*/
#define cThaRegPohTtiExptedAcpMsgBuf                   0x08D400
#define cThaRegPohTtiExptedAcpMsgBufRstVal             0x0


#define cThaTtiExptAcpMsgByteMask(byteId)             ((uint32)cBit31_24 >> (uint32)((byteId) * 8))
#define cThaTtiExptAcpMsgByteShift(byteId)            (8 * (3 - ((byteId) % 4)))
#define cThaTtiExptAcpMsgByteMaxVal                   0xFF
#define cThaTtiExptAcpMsgByteMinVal                   0x0
#define cThaTtiExptAcpMsgByteRstVal                   0x0


/*------------------------------------------------------------------------------
Reg Name: POH Path Terminate Jn Insertion Control
Reg Addr: 0x093800-0x0A082F
          The address format for these registers is 0x520800 + + STS*4 + SLICE
          Where
          SLICE[1:0]: 0 -> 1
          STS[3:0]: 0-> 11
Reg Desc: Path Terminate Insert configuration register ([27:0] Vtmode, [28]:
          STS, [29] Tu3)
------------------------------------------------------------------------------*/
#define cThaRegPohPathTerJnInsCtrl                    0x093800
#define cThaRegPohPathTerJnInsCtrlRstVal              0x0

/*-----------
BitField Name: J1Tu3InsMd
BitField Type: R/W
BitField Desc: The J1 Insert Mode.
               - 1'b0: Jn insert from CPU
               - 1'b1: Jn insert from buffer
-----------*/
#define cThaJ1Tu3InsMdMask                            cBit29
#define cThaJ1Tu3InsMdShift                           29
#define cThaJ1Tu3InsMdMaxVal                          0x1
#define cThaJ1Tu3InsMdMinVal                          0x0
#define cThaJ1Tu3InsMdRstVal                          0x0

/*-----------
BitField Name: J1StsInsMd
BitField Type: R/W
BitField Desc: The J1 Insert Mode.
               - 1'b0: Jn insert from CPU
               - 1'b1: Jn insert from buffer
-----------*/
#define cThaJ1StsInsMdMask                            cBit28
#define cThaJ1StsInsMdShift                           28
#define cThaJ1StsInsMdMaxVal                          0x1
#define cThaJ1StsInsMdMinVal                          0x0
#define cThaJ1StsInsMdRstVal                          0x0

/*-----------
BitField Name: J2InsMd
BitField Type: R/W
BitField Desc: The J2 Insert Mode.
               - 1'b0: Jn insert from CPU
               - 1'b1: Jn insert from buffer
-----------*/
#define cThaJ2InsMdMask(vt)                           (cBit0 << (vt))
#define cThaJ2InsMdShift(vt)                          (vt)
#define cThaJ2InsMdMaxVal                             0x1
#define cThaJ2InsMdMinVal                             0x0
#define cThaJ2InsMdRstVal                             0x0

/*------------------------------------------------------------------------------
Reg Name: POH CPE Status 1
Reg Addr: 0x089000-0x5097FF
          The address format for these registers is 0x50_9000 + STS*128 +
          Slice*32 + VT
          Where: Slice: 0 -> 3
          STS: 0 -> 11
          VT: 0 -> 29 (0-27:VT; 28: STS; 29 Tu3)
Reg Desc: This is CPE Status 1 register. There are two cases to use the POH
          CPE Status 1 register, one for STS/TU3 and one for VT.
------------------------------------------------------------------------------*/
#define cThaRegPohCpeStat1                           0x089000

/*------------------------------------------------------------------------------
Reg Name: OCN Rx VT/TU per Alarm Current Status
Reg Addr: 0x00053800 - 0x0005387B
          The address format for these registers is 0x00053800 + SliceID*8192
          + StsID*32 + Tug2ID*4 + VtnID
          Where: SliceID: (0-11) STS-12 slice ID
          StsID: (0-11) STS-1/VC-3 ID
          Tug2ID: (0-6) TUG-2/VTG ID
          VtnID: (0-3) VT/TU number ID in the TUG-2/VTG
Reg Desc: This is the per Alarm current status of VT/TU pointer interpreter.
          Each register is used to store 6 bits to store current status of 6
          alarms in the VT/TU.
------------------------------------------------------------------------------*/
#define cThaRegOcnRxVtTUperAlmCurrentStat           0x053800


#define cThaVtPiCepUneqCurStatMask                     cBit2

/*--------------------------------------
BitField Name: VtPiAisCurStatus
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaVtPiAisCurStatMask                         cBit1

/*--------------------------------------
BitField Name: VtPiLopCurStatus
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaVtPiLopCurStatMask                         cBit0

/*------------------------------------------------------------------------------
Reg Name: OCN Rx VT/TU per Alarm Interrupt Status
Reg Addr: 0x00053400 - 0x0005347B
          The address format for these registers is 0x00053400 + SliceID*8192
          + StsID*32 + Tug2ID*4 + VtnID
          Where: SliceID: (0-11) STS-12 slice ID
          StsID: (0-11) STS-1/VC-3 ID
          Tug2ID: (0-6) TUG-2/VTG ID
          VtnID: (0-3) VT/TU number ID in the TUG-2/VTG
Reg Desc: This is the per Alarm interrupt status of VT/TU pointer interpreter
          . Each register is used to store 6 sticky bits for 6 alarms in the
          VT/TU.
------------------------------------------------------------------------------*/
#define cThaRegOcnRxVtTUperAlmIntrStat              0x053400

/*
#define cThaUnusedMask                                 cBit31_5
#define cThaUnusedShift                                5
#define cThaUnusedRstVal                               0x0
*/

/*--------------------------------------
BitField Name: VtPiStsConcDetIntr
BitField Type: R/W/C
BitField Desc: Set to 1 while an Concatenation Detection event is detected at
               VT/TU pointer interpreter.
--------------------------------------*/

/*--------------------------------------
BitField Name: VtPiStsNewPtrDetIntr
BitField Type: R/W/C
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/

/*--------------------------------------
BitField Name: VtPiStsNdfIntr
BitField Type: R/W/C
BitField Desc: Set to 1 while an NDF event is detected at VT/TU pointer
               interpreter.
--------------------------------------*/

#define cThaVtPiCepUneqVStateChgIntrMask                    cBit2

/*--------------------------------------
BitField Name: VtPiAisStateChgIntr
BitField Type: R/W/C
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaVtPiAisStateChgIntrMask                    cBit1

/*--------------------------------------
BitField Name: VtPiLopStateChgIntr
BitField Type: R/W/C
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaVtPiLopStateChgIntrMask                    cBit0

/*------------------------------------------------------------------------------
Reg Name: OCN Rx VT/TU Per VT/TU Interrupt OR Status
Reg Addr: 0x0053C00 - 0x0057C00
          The address format for these registers is 0x0053C00 + SliceID*8192 +
          Stsid
          Where: SliceID: (0-1) STS-12 slice ID
          StsID: (0-11) STS-1/VC-3 ID
Reg Desc: The register consists of 28 bits for 28 VT/TUs of the related STS/VC
          in the VT/TU Pointer interpreter block. Each bit is used to store
          Interrupt OR status of the related VT/TU.
------------------------------------------------------------------------------*/
#define cThaRegOcnRxVtTUPerVtTUIntrOrStat           0x053C00

/*------------------------------------------------------------------------------
Reg Name: OCN Rx VT/TU per Alarm Interrupt Enable Control
Reg Addr: 0x00053000-0x0005307B
          The address format for these registers is 0x00053000 + SliceID*8192
          + StsID*32 + Tug2ID*4 + VtnID
          Where: SliceID: (0-11) STS-12 slice ID
          StsID: (0-11) STS-1/VC-3 ID
          Tug2ID: (0-6) TUG-2/VTG ID
          VtnID: (0-3) VT/TU number ID in the TUG-2/VTG
Reg Desc: This is the per Alarm interrupt enable of VT/TU pointer interpreter.
          Each register is used to store 6 bits to enable interrupts when the
          related alarms in related VT/TU happen.
------------------------------------------------------------------------------*/
#define cThaRegOcnRxVtTUperAlmIntrEnCtrl            0x053000

/*--------------------------------------
BitField Name: VtPiAisStateChgIntrEn
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaVtPiAisStateChgIntrEnMask                  cBit1
#define cThaVtPiAisStateChgIntrEnShift                 1

/*--------------------------------------
BitField Name: VtPiLopStateChgIntrEn
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaVtPiLopStateChgIntrEnMask                  cBit0
#define cThaVtPiLopStateChgIntrEnShift                 0

#define cThaRegOcnRxVtTUperAlmIntrEnCtrlNoneEditableFieldsMask cBit31_3

/*--------------------------------------
BitField Name: TxPg1Sts_Vt_TuSSInsPat[1:0]
BitField Type: R/W
BitField Desc: Pattern to insert to SS bits or VT/TU type positions in
               STS/VC/VT/TU the pointer.
--------------------------------------*/
#define cThaTxPg1Sts_Vt_TuSSInsPatMask                 cBit17_16
#define cThaTxPg1Sts_Vt_TuSSInsPatShift                16

/*--------------------------------------
BitField Name: TxPg1Sts_Vt_TuSSInsEn
BitField Type: R/W
BitField Desc: SS bits or VT/TU type insert enable bit
               - 1: Enable insertion of TxPg1Sts_Vt_TuSSInsPat[1:0] into SS
                 bits or VT/TU type position in STS/VC/VT/TU pointers.
               - 0: SS bits or VT/TU type position in STS/VC/VT/TU pointer is
                 passed through.
--------------------------------------*/
#define cThaTxPg1Sts_Vt_TuSSInsEnMask                  cBit15
#define cThaTxPg1Sts_Vt_TuSSInsEnShift                 15

/*------------------------------------------------------------------------------
Reg Name: OCN VT/TU PI Per Channel Control
Reg Addr: 0x00052400 � 0x0005457B
          The address format for these registers is 0x00052400 + SliceID*8192
          + StsID*32 + Tug2ID*4 + VtnID
          Where: SliceID: (0) STS-12 slice ID
          StsID: (0-11) STS-1/VC-3 ID
          Tug2ID: (0-6) TUG-2/VTG ID
          VtnID: (0-3) VT/TU number ID in the TUG-2/VTG
          It is noted that for STS level, Tug2ID and VtnID have to be set zero
          value.
Reg Desc: Each register is used to configure for STS pointer generator and
          VT/TU pointer processor including VT pointer interpreter and VT/TU
          pointer generator of the related STS/VT/TU.
------------------------------------------------------------------------------*/
#define cThaRegOcnVtTUPIPerChnCtrl                  0x00052400

/*-----------
BitField Name: RxStsPiVtTermEn
BitField Type: R/W
BitField Desc: Enable to terminate the related VT/Tu. It means that VT/TU
               POH defects related to the VT/TU to generate AIS to
               downstream:
               - 1: Enable.
               - 0: Disable.
------------*/
#define cThaRxStsPiVtTermEnMask                         cBit19
#define cThaRxStsPiVtTermEnShift                        19

/*-----------
BitField Name: VtPiVtTypeDetPat
BitField Type: R/W
BitField Desc: This is the pattern that is used to compare with the received VT
               Type.
------------*/
#define cThaVtPiVtTypeDetPatMask                      cBit12_11
#define cThaVtPiVtTypeDetPatShift                     11

/*-----------
BitField Name: VtPiVtTypeDetEn
BitField Type: R/W
BitField Desc: VT-Type detection enable
               - 1: Enable checking VT Type in the pointer interpreter engine.
               - 0: VT Type checking function is disabled.
------------*/
#define cThaVtPiVtTypeDetEnMask                       cBit10
#define cThaVtPiVtTypeDetEnShift                      10

#define cThaVtppStsPgSlvIndMask  cBit8
#define cThaVtppStsPgSlvIndShift 8

#define cThaVtppStsPgMastIDMask  cBit4_0
#define cThaVtppStsPgMastIDShift 0

/*-----------
BitField Name: VtPiAdjRule
BitField Type: R/W
BitField Desc: Select the rule for implementing pointer adjustment in the
               pointer interpreter
               - 1: The n of 5 rule is selected. This mode is applied for
                    SDH mode
               - 0: The 8 of 10 rule is selected. This mode is applied for
                    SONET mode
------------*/

#define cThaRegOcnVtTUPIPerChnCtrlNoneEditableFieldsMask (cBit31_20 | cBit17_13)

/*------------------------------------------------------------------------------
Reg Name: POH J0 Status 1
Reg Addr: 0x08901F_0x08903F
          The address format for these registers is 0x50901F + LineID*128
          Where: LineID: 0 -> 3
Reg Desc: J0 Status 1 Register for SDH/SONET Line.
------------------------------------------------------------------------------*/
#define cThaRegPohJ0Stat1                             0x08901F

/*------------------------------------------------------------------------------
Reg Name: POH STS G1 Insert Buffer
Address: 0x088300 � 0x088300 + STSMAX-1
The address format for these registers is 0x088300 + STSID
Where: STSMAX = 6
    STSID: 0 -> STSMAX-1
Description: This is the G1 byte insert buffer using for transmit.
 ------------------------------------------------------------------------------*/
#define cThaRegPohStsG1InsBuffer 0x088300
#define cThaRegPohStsG1InsByteMask cBit7_0
#define cThaRegPohStsG1InsByteShift 0

/*------------------------------------------------------------------------------
Reg Name: OCN Framer Alarm Affect Control
Reg Addr: 0x0004022E
Reg Desc: Configure to enable to generate AIS-L into payload data to STS/VC XC or
          RDI-L into K2 byte at the transmit direction when TIM-L is detected.
------------------------------------------------------------------------------*/
#define cThaRegOcnFrmrAlmTimLAffCtrl                   0x0004022E

/*-----------
BitField Name: TimLAffAisLRdiLEn
BitField Type: R/W
BitField Desc: Enable/disable the insertion of L-AIS and L-RDI when detecting TIM-L condition at SONTOHMON.
                1: Enable.
                0: Disable.
------------*/
#define cThaTimLAffAisLRdiLEnMask(lineId)              (cBit0 << (lineId))
#define cThaTimLAffAisLRdiLEnShift(lineId)             (lineId)

#define cThaRegOcnFrmrAlmTimLAffCtrlNoneEditableFieldsMask cBit31_8

/*
 * POH registers
 */
/*------------------------------------------------------------------------------
Reg Name: POH J0 STI Message Alarm Status registers
Address: 0x087000 - 0x0877FF
The address format for these registers is 0x087000 + VTMAX * STSMAX + STSMAX * 2 + LID
Where,  STSMAX = 6
    VTMAX = 28
    LID: 0 -> 1
Description: This is the J0 STI Message alarm current status register.
It will be used to indicate current status of the event.
------------------------------------------------------------------------------*/
#define cThaRegPohJ0StiMsgAlarmStatus (0x087000 + (VTMAX * STSMAX) + (STSMAX * 2))
#define cThaRegPohJ0StiTimMask cBit1

/*------------------------------------------------------------------------------
Reg Name: POH J0 STI Message Alarm Interrupt Sticky registers
Address: 0x086800 -0x086FFF
The address format for these registers is 0x086800 + VTMAX * STSMAX + STSMAX * 2 + LID
Where,  STSMAX = 6
    VTMAX = 28
    LID: 0 -> 1
Description: This is the J0 STI Messagge alarm interrupt sticky register.
It will be used to indicate and sticky the event states change when the POH CPE engine detected a changing of  event state.
------------------------------------------------------------------------------*/
#define cThaRegPohJ0StiMsgAlarmIntrSticky (0x086800 + (VTMAX * STSMAX) + (STSMAX * 2))
#define cThaRegPohJ0StiTimStbChgIntrMask cBit1


/*------------------------------------------------------------------------------
Reg Name: POH J0 STI Message Alarm Interrupt Enable Control register
Address: 0x086000 - 0x867FF
The address format for these registers is 0x086000 + VTMAX * STSMAX + STSMAX * 2 + LID
Where,  STSMAX = 6
    VTMAX = 28
    LID: 0 -> 1
Description: This is the J0 STI Message alarm interrupt enable control register.
It is using to enable each event an alarm when it is detected.
------------------------------------------------------------------------------*/
#define cThaRegPohJ0StiMsgAlarmIntrEnable (0x086000 + (VTMAX * STSMAX) + (STSMAX * 2))
#define cThaRegPohJ0StiTimStbChgIntrEnMask cBit1
#define cThaRegPohJ0StiTimStbChgIntrEnShift 1

/*------------------------------------------------------------------------------
Reg Name: OCN Rx Line per Alarm Interrupt Status
Reg Addr: 0x00074A10 - 0x00074A17
          The address format for these registers is 0x00074A10 + lineID
          Where: lineID (0 - 7) SONET/SDH line IDs
Reg Desc: This is the per Alarm interrupt status of Rx framer and TOH
          monitoring. Each register is used to store 12 sticky bits for 12
          alarms in the line.
------------------------------------------------------------------------------*/
#define cThaRegOcnRxLineperAlmIntrStat              0x074A10


/*--------------------------------------
BitField Name: RxLRdiILDefStateChgIntr
BitField Type: R/W/C
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaRxLRdiILDefStateChgIntrMask                cBit4

/*--------------------------------------
BitField Name: RxLAisLDefStateChgIntr
BitField Type: R/W/C
BitField Desc: Set 1 while AIS-L Defect state change event happens in the
               related line, and it is generated an interrupt if it is enabled.
--------------------------------------*/
#define cThaRxLAisLDefStateChgIntrMask                 cBit3

/*--------------------------------------
BitField Name: RxLOofStateChgIntr
BitField Type: R/W/C
BitField Desc: Set 1 while OOF state change event happens in the related line,
               and it is generated an interrupt if it is enabled.
--------------------------------------*/
#define cThaRxLOofStateChgIntrMask                     cBit2

/*--------------------------------------
BitField Name: RxLLofStateChgIntr
BitField Type: R/W/C
BitField Desc: Set 1 while LOF state change event happens in the related line,
               and it is generated an interrupt if it is enabled.
--------------------------------------*/
#define cThaRxLLofStateChgIntrMask                     cBit1

/*--------------------------------------
BitField Name: RxLLosStateChgIntr
BitField Type: R/W/C
BitField Desc: Set 1 while LOS state change event happens in the related line,
               and it is generated an interrupt if it is enabled.
--------------------------------------*/
#define cThaRxLLosStateChgIntrMask                     cBit0


/*------------------------------------------------------------------------------
Reg Name: POH Indirect Access Control Register
Reg Addr: 0x080100
Reg Desc: This is the indirect access control register. It is used to access the
          TTI messages for J0, J1, J2 bytes.
------------------------------------------------------------------------------*/
#define cThaPohIndirectAccessCtrl   0x080100
#define cThaPohIndirectAddressMask  cBit15_0
#define cThaPohIndirectAddressShift 0

#define cThaPohIndirectRnWMask      cBit30
#define cThaPohIndirectRnWShift     30

#define cThaPohIndirectAcsEnMask    cBit31
#define cThaPohIndirectAcsEnShift   31

/*------------------------------------------------------------------------------
Reg Name: POH Indirect Access Data Register
Reg Addr: 0x080101
Reg Desc: This is the indirect access Data register. It is used to get or write
          the TTI messages for J0, J1, J2 bytes..
------------------------------------------------------------------------------*/
#define cThaPohIndirectAcsData      0x080101

#define cThaOcnJnMsgMask(byteIndex)  ((uint32) cBit7_0 << ((uint32)(cThaOcnJnMsgShift(byteIndex))))
#define cThaOcnJnMsgShift(byteIndex) (8 * (3 - (byteIndex)))

/*------------------------------------------------------------------------------
Reg Name: POH STS J1 Insert Buffer
Address: 0x088000 � 0x088000 + STSMAX-1
The address format for these registers is 0x088000 + STSID
Where: STSMAX = 6
    STSID: 0 -> STSMAX-1
Description: This is the J1 byte temporary insert buffer using for transmit.
------------------------------------------------------------------------------*/
#define cThaRegPohStsTuVtInsByteMask    cBit7_0
#define cThaRegPohStsTuVtInsByteShift   0

/*------------------------------------------------------------------------------
Reg Name: POH STS N1 Insert Buffer
Address: 0x088100 � 0x088100 + STSMAX-1
The address format for these registers is 0x088200 + STSID
Where: STSMAX = 6
    STSID: 0 -> STSMAX-1
Description: This is the N1 byte insert buffer using for transmit.
 ------------------------------------------------------------------------------*/
#define cThaRegPohStsN1InsBuffer 0x088100

/*------------------------------------------------------------------------------
Reg Name: POH STS C2 Insert Buffer
Address: 0x088200 � 0x088200 + STSMAX-1
The address format for these registers is 0x088200 + STSID
Where: STSMAX = 6
    STSID: 0 -> STSMAX-1
Description: This is the C2 byte insert buffer using for transmit.
 ------------------------------------------------------------------------------*/
#define cThaRegPohStsC2InsBuffer 0x088200
#define cThaRegPohStsC2InsByteMask cBit7_0
#define cThaRegPohStsC2InsByteShift 0
#define cThaRegPohStsC2InsBufferNoneEditableFieldsMask cBit31_8

/*------------------------------------------------------------------------------
Reg Name: POH STS G1 Insert Buffer
Address: 0x088300 � 0x088300 + STSMAX-1
The address format for these registers is 0x088300 + STSID
Where: STSMAX = 6
    STSID: 0 -> STSMAX-1
Description: This is the G1 byte insert buffer using for transmit.
 ------------------------------------------------------------------------------*/
#define cThaRegPohStsG1InsBuffer 0x088300

/*------------------------------------------------------------------------------
Reg Name: POH STS F2 Insert Buffer
Address: 0x088400 � 0x088400 + STSMAX-1
The address format for these registers is 0x088400 + STSID
Where: STSMAX = 6
    STSID: 0 -> STSMAX-1
Description: This is the F2 byte insert buffer using for transmit.
 ------------------------------------------------------------------------------*/
#define cThaRegPohStsF2InsBuffer 0x088400

/*------------------------------------------------------------------------------
Reg Name: POH STS H4 Insert Buffer
Address: 0x088500 � 0x088500 + STSMAX-1
The address format for these registers is 0x088500 + STSID
Where: STSMAX = 6
    STSID: 0 -> STSMAX-1
Description: This is the H4 byte insert buffer using for transmit.
 ------------------------------------------------------------------------------*/
#define cThaRegPohStsH4InsBuffer 0x088500

/*------------------------------------------------------------------------------
Reg Name: POH STS F3 Insert Buffer
Address: 0x088600 � 0x088600 + STSMAX-1
The address format for these registers is 0x088600 + STSID
Where: STSMAX = 6
    STSID: 0 -> STSMAX-1
Description: This is the F3 byte insert buffer using for transmit.
 ------------------------------------------------------------------------------*/
#define cThaRegPohStsF3InsBuffer 0x088600

/*------------------------------------------------------------------------------
Reg Name: POH STS K3 Insert Buffer
Address: 0x088700 � 0x088700 + STSMAX-1
The address format for these registers is 0x088700 + STSID
Where: STSMAX = 6
    STSID: 0 -> STSMAX-1
Description: This is the K3 byte insert buffer using for transmit.
 ------------------------------------------------------------------------------*/
#define cThaRegPohStsK3InsBuffer 0x088700

/*------------------------------------------------------------------------------
Reg Name: POH TU3 C2 Insert Buffer
Address: 0x088A00 � 0x088A00 + TU3MAX-1
The address format for these registers is 0x088A00 + TU3ID
Where: TU3MAX = 6
    TU3ID: 0 -> TU3MAX-1
Description: This is the C2 byte insert buffer using for transmit.
------------------------------------------------------------------------------*/
#define cThaRegPohTu3C2InsBuffer 0x088A00
#define cThaRegPohTu3C2InsByteMask cBit7_0
#define cThaRegPohTu3C2InsByteShift 0
#define cThaRegPohTu3C2InsBufferNoneEditableFieldsMask cBit31_8

/*------------------------------------------------------------------------------
Reg Name: POH TU3 N1 Insert Buffer
Address: 0x088900 � 0x088900+ TU3MAX-1
The address format for these registers is 0x088900 + TU3ID
Where: TU3MAX = 6
    TU3ID: 0 -> TU3MAX-1
Description: This is the N1 byte temporary insert buffer using for transmit.
------------------------------------------------------------------------------*/
#define cThaRegPohTu3N1InsBuffer        0x088900

/*------------------------------------------------------------------------------
Reg Name: POH TU3 G1 Insert Buffer
Address: 0x088B00 � 0x088B00+ TU3MAX-1
The address format for these registers is 0x088B00 + TU3ID
Where: TU3MAX = 6
    TU3ID: 0 -> TU3MAX-1
Description: This is the G1 byte temporary insert buffer using for transmit.
------------------------------------------------------------------------------*/
#define cThaRegPohTu3G1InsBuffer        0x088B00

/*------------------------------------------------------------------------------
Reg Name: POH TU3 F2 Insert Buffer
Address: 0x088C00 � 0x088C00+ TU3MAX-1
The address format for these registers is 0x088900 + TU3ID
Where: TU3MAX = 6
    TU3ID: 0 -> TU3MAX-1
Description: This is the F2 byte temporary insert buffer using for transmit.
------------------------------------------------------------------------------*/
#define cThaRegPohTu3F2InsBuffer        0x088C00

/*------------------------------------------------------------------------------
Reg Name: POH TU3 H4 Insert Buffer
Address: 0x088D00 � 0x088D00+ TU3MAX-1
The address format for these registers is 0x088D00 + TU3ID
Where: TU3MAX = 6
    TU3ID: 0 -> TU3MAX-1
Description: This is the H4 byte temporary insert buffer using for transmit.
------------------------------------------------------------------------------*/
#define cThaRegPohTu3H4InsBuffer        0x088D00

/*------------------------------------------------------------------------------
Reg Name: POH TU3 F3 Insert Buffer
Address: 0x088E00 � 0x088E00+ TU3MAX-1
The address format for these registers is 0x088E00 + TU3ID
Where: TU3MAX = 6
    TU3ID: 0 -> TU3MAX-1
Description: This is the F3 byte temporary insert buffer using for transmit.
------------------------------------------------------------------------------*/
#define cThaRegPohTu3F3InsBuffer        0x088E00

/*------------------------------------------------------------------------------
Reg Name: POH TU3 K3 Insert Buffer
Address: 0x088F00 � 0x088F00+ TU3MAX-1
The address format for these registers is 0x088F00 + TU3ID
Where: TU3MAX = 6
    TU3ID: 0 -> TU3MAX-1
Description: This is the K3 byte temporary insert buffer using for transmit.
------------------------------------------------------------------------------*/
#define cThaRegPohTu3K3InsBuffer        0x088F00

/*------------------------------------------------------------------------------
Reg Name: POH CPE TU3 Control Registers
Address: 0x084000 - 0x0841FF
The address format for these registers is 0x084000 + STSMAX * VTMAX + STSMAX + TU3ID
Where: STSMAX = 6
    VTMAX = 28
    TU3ID: 0 -> STSMAX-1
Description: This is the Common Processor Engine Control Register for TU3.
------------------------------------------------------------------------------*/
#define cThaPohRegCpeTu3Ctrl (0x084000 + (STSMAX * VTMAX) + STSMAX)

#define cThaPohRegCpeTu3CtrlNoneEditableFieldsMask cBit27_25

/*------------------------------------------------------------------------------
Reg Name: POH VT V5 Insert Buffer
Address: 0x089000 � 0x089000+ VTMAX*STSMAX-1
The address format for these registers is 0x089000 + STSID * VTMAX + VTID
Where: STSMAX = 6
    VTMAX = 28
    STSID: 0 -> STSMAX-1
    VTID: 0 -> VTMAX-1
Description: This is the V5 byte insert buffer using for transmit.
------------------------------------------------------------------------------*/
#define cThaRegPohVtV5InsBuffer 0x089000
#define cThaRegPohVtV5InsByteMask cBit7_0
#define cThaRegPohVtV5InsByteShift 0

/*------------------------------------------------------------------------------
Reg Name: POH VT N2 Insert Buffer
Address: 0x089400 � 0x089400+ VTMAX*STSMAX-1
The address format for these registers is 0x089400 + STSID * VTMAX + VTID
Where: STSMAX = 6
    VTMAX = 28
    STSID: 0 -> STSMAX-1
    VTID: 0 -> VTMAX-1
Description: This is the N2 byte insert buffer using for transmit.
------------------------------------------------------------------------------*/
#define cThaRegPohVtN2InsBuffer 0x089400

/*------------------------------------------------------------------------------
Reg Name: POH VT K4 Insert Buffer
Address: 0x089800 � 0x089800+ VTMAX*STSMAX-1
The address format for these registers is 0x089800 + STSID * VTMAX + VTID
Where: STSMAX = 6
    VTMAX = 28
    STSID: 0 -> STSMAX-1
    VTID: 0 -> VTMAX-1
Description: This is the K4 byte insert buffer using for transmit.
------------------------------------------------------------------------------*/
#define cThaRegPohVtK4InsBuffer 0x089800
#define cThaRegPohVtK4InsByteMask cBit7_0
#define cThaRegPohVtK4InsByteShift 0

/*------------------------------------------------------------------------------
Reg Name: POH B3/REI-P Counter Mode Control
Address: 0x080007
Description: This register is used for control the B3/REI Counter Mode.
------------------------------------------------------------------------------*/
#define cThaPohRegB3ReiCntMdCtrl 0x080007
#define cThaPohRegB3ReiCntMdMask    cBit1
#define cThaPohRegB3ReiCntMdShift   1

/*------------------------------------------------------------------------------
Reg Name: POH BIP2/REI-V Counter Mode Control
Address: 0x08000C
Description: This register is used for control the BIP2/REI-V Counter Mode.
------------------------------------------------------------------------------*/
#define cThaPohRegBip2ReiVCntMdCtrl 0x08000C
#define cThaPohRegBip2ReiVCntMdMask    cBit1
#define cThaPohRegBip2ReiVCntMdShift   1

/*------------------------------------------------------------------------------
Reg Name: POH CPE VT Control Registers
Address: 0x084000 - 0x0841FF
The address format for these registers is 0x084000 + STSID * VTMAX + VTID
Where: STSMAX = 6
    VTMAX = 28
    STSID: 0 -> STSMAX-1
    VTID: 0 -> VTMAX-1
Description: This is the Common Processor Engine Control Register for VT.
------------------------------------------------------------------------------*/
#define cThaPohRegCpeVtControl 0x084000

/*--------------------------------------
BitField Name: Plm2AisEnb: Enable forwarding AIS to downstream when PLM defected. Active high.
BitField Type: R/W/C
BitField Desc:
--------------------------------------*/
#define cThaPohVtPlm2AisEnbMask cBit31
#define cThaPohVtPlm2AisEnbShift 31

/*--------------------------------------
BitField Name: VcAis2AisEnb: Enable forwarding AIS to downstream when VCAIS defected. Active high.
BitField Type: R/W/C
BitField Desc:
--------------------------------------*/
#define cThaPohVtVcAis2AisEnbMask cBit30
#define cThaPohVtVcAis2AisEnbShift 30

/*--------------------------------------
BitField Name: Uneq2AisEnb: Enable forwarding AIS to downstream when Uneq defected. Active high.
BitField Type: R/W/C
BitField Desc:
--------------------------------------*/
#define cThaPohVtUneq2AisEnbMask cBit29
#define cThaPohVtUneq2AisEnbShift 29

/*--------------------------------------
BitField Name: Tim2AisEnb: Enable forwarding AIS to downstream when TIM defected. Active high.
BitField Type: R/W/C
BitField Desc:
--------------------------------------*/
#define cThaPohVtTim2AisEnbMask cBit28
#define cThaPohVtTim2AisEnbShift 28

/*--------------------------------------
BitField Name: K4b1b4InsVal[3:0]: The insert value of bit b1 to b4 of K4 byte.
BitField Type: R/W/C
BitField Desc:
--------------------------------------*/
#define cThaPohVtK4b1b4InsValMask cBit26_23
#define cThaPohVtK4b1b4InsValShift 23

/*--------------------------------------
BitField Name: K4b8InsVal: The insert value of bit b8 of K4 byte.
BitField Type: R/W/C
BitField Desc:
--------------------------------------*/
#define cThaPohVtK4b8InsValMask cBit22
#define cThaPohVtK4b8InsValShift 22

/*--------------------------------------
BitField Name: RfiInsVal: RFI Insert Value.
BitField Type: R/W/C
BitField Desc:
--------------------------------------*/
#define cThaPohVtRfiInsValMask cBit21
#define cThaPohVtRfiInsValShift 21

/*--------------------------------------
BitField Name: VslInsVal[2:0]: VSL, Low-order Path Signal Label value.
BitField Type: R/W/C
BitField Desc:
--------------------------------------*/
#define cThaPohVtVslInsValMask cBit20_18
#define cThaPohVtVslInsValShift 18

/*--------------------------------------
BitField Name: RdiInsVal: RDI (V5[b8]) insert value
BitField Type: R/W/C
BitField Desc:
--------------------------------------*/
#define cThaPohVtRdiInsValMask cBit17
#define cThaPohVtRdiInsValShift 17

/*--------------------------------------
BitField Name: ERdiInsVal[2:0]: ERDI (K4[b5-b7]) insert value
BitField Type: R/W/C
BitField Desc:
--------------------------------------*/
#define cThaPohVtERdiInsValMask cBit16_14
#define cThaPohVtERdiInsValShift 14

/*--------------------------------------
BitField Name: ERdiInsEnb: Enable ERDI function for insertion.
BitField Type: R/W/C
BitField Desc:
    1: Enable ERDI.
    0: Disable ERDI. G1[b6-b7] inserted from ERdiInsVal[1:0].
--------------------------------------*/
#define cThaPohVtERdiInsEnbMask cBit13
#define cThaPohVtERdiInsEnbShift 13

/*--------------------------------------
BitField Name: RdiInsEnb:
BitField Type: R/W/C
BitField Desc:
    1: enable insertion of RDI value from RdiInsVal and ERdiInsVal.
    0: HW automatically inserts.
--------------------------------------*/
#define cThaPohVtRdiInsEnbMask cBit12
#define cThaPohVtRdiInsEnbShift 12

/*--------------------------------------
BitField Name: AisLopInsMsk:
BitField Type: R/W/C
BitField Desc:
    1: Disable insert RDI/ERDI defect when AIS or LOP detected.
    0: Enable (auto insert mode).
--------------------------------------*/
#define cThaPohVtAisMskMask cBit11
#define cThaPohVtAisMskShift 11

/*--------------------------------------
BitField Name: TimInsMsk:
BitField Type: R/W/C
BitField Desc:
    1: Disable insert RDI/ERDI defect when TIM detected.
    0: Enable (auto insert mode).
--------------------------------------*/
#define cThaPohVtTimMskMask cBit10
#define cThaPohVtTimMskShift 10

/*--------------------------------------
BitField Name: UneqInsMsk:
BitField Type: R/W/C
BitField Desc:
    1: Disable insert RDI/ERDI defect when UNEQ detected.
    0: Enable (auto insert mode).
--------------------------------------*/
#define cThaPohVtUneqMskMask cBit9
#define cThaPohVtUneqMskShift 9

/*--------------------------------------
BitField Name: PlmInsMsk:
BitField Type: R/W/C
BitField Desc:
    1: Disable insert RDI (ERDI) defect when PLM detected.
    0: Enable (auto insert mode).
--------------------------------------*/
#define cThaPohVtPlmMskMask cBit8
#define cThaPohVtPlmMskShift 8

/*--------------------------------------
BitField Name: VslExptVal[2:0]: The expected value of Low-Order Path Signal Label
BitField Type: R/W/C
BitField Desc:
--------------------------------------*/
#define cThaPohVtVslExptValMask cBit6_4
#define cThaPohVtVslExptValShift 4

/*--------------------------------------
BitField Name: J2MonEnb: Enable to monitor J2 message. Active high.
--------------------------------------*/
#define cThaPohVtJ2MonEnbMask cBit2
#define cThaPohVtJ2MonEnbShift 2

/*--------------------------------------
BitField Name: J2FrmMd[1:0]: Indicates J2 Frame Mode
BitField Type: R/W/C
BitField Desc:
    2�b00: byte mode.
    2�b01: TTI message with 16 bytes length. The frame format according ITU-T
    2�b10: TTI message with 64 bytes length. The frame format has CR and LF at the end of frame
    2�b11: TTI message float mode. (J2 byte captured cycle of a buffer 64 bytes).
--------------------------------------*/
#define cThaPohVtJ2FrmMdMask cBit1_0
#define cThaPohVtJ2FrmMdShift 0

#define cThaPohRegCpeVtControlNoneEditableFieldsMask cBit27

/*------------------------------------------------------------------------------
Reg Name: POH CPE  STS Alarm Status register
Address: 0x087000 - 0x0877FF
The address format for these registers is 0x087000 + VTMAX * STSMAX + STSID
Where,  STSMAX = 6
    VTMAX = 28
    STSID: 0 -> STSMAX - 1
Description: This is the POH CPE STS  alarm current status register.
It will be used to indicate current status of the event.
------------------------------------------------------------------------------*/
#define cThaPohRegSpeStsTu3AlarmStatus (0x087000)

#define cThaPohAlarmRdiStbMask cBit6

#define cThaPohAlarmUneqMask cBit4

#define cThaPohAlarmPlmMask cBit3

#define cThaPohAlarmVcAisMask cBit2

#define cThaPohAlarmTtiTimMask cBit1

/*------------------------------------------------------------------------------
Reg Name: POH CPE  STS Alarm Interrupt Sticky registers
Address: 0x086800 - 0x086FFF
The address format for these registers is 0x086800 + VTMAX * STSMAX + STSID
Where,  STSMAX = 6
    VTMAX = 28
    STSID: 0 -> STSMAX - 1
Description: This is the POH CPE STS alarm interrupt sticky register.
It will be used to indicate and sticky the event states change when the
POH CPE engine detected a changing of  event state.
------------------------------------------------------------------------------*/
#define cThaPohRegCpeStsTu3AlarmIntrSticky (0x086800)

/*------------------------------------------------------------------------------
Reg Name: POH CPE STS Alarm Interrupt Enable Control register
Address: 0x086000 - 0x0867FF
The address format for these registers is 0x086000 + VTMAX * STSMAX + STSID
Where,  STSMAX = 6
    VTMAX = 28
    STSID: 0 -> STSMAX - 1
Description: This is the per channel POH CPE STS alarm interrupt enable control register.
It is using to enable each event an alarm when it is detected.
------------------------------------------------------------------------------*/
#define cThaPohRegCpeStsTu3AlarmIntrEnable (0x086000)

/*------------------------------------------------------------------------------
Reg Name: POH CPE  STS B3 Error Counter Registers
Address: 0x085000 - 0x0851FF
The address format for these registers is 0x085000 + STSMAX * VTMAX + STSID
Where: STSMAX = 6
    VTMAX = 28
    STSID: 0 -> STSMAX-1
Description: This is the Common Processor Engine B3 BIP-8 Error Counter.
------------------------------------------------------------------------------*/
#define cThaPohRegCpeStsTu3B3BipErrCnt (0x085000)
#define cThaB3ErrCntMask cBit23_0

/*------------------------------------------------------------------------------
Reg Name: POH CPE STS REI-P Counter Registers
Address: 0x085800 - 0x0859FF
The address format for these registers is 0x085800 +  STSMAX * VTMAX + STSID
Where: STSMAX = 6
    VTMAX = 28
    STSID: 0 -> STSMAX-1
Description: This is the Common Processor Engine REI Counter.
------------------------------------------------------------------------------*/
#define cThaPohRegCpeStsTu3ReiPErrCnt (0x085800)
#define cThaReiPErrCntMask cBit23_0

/*------------------------------------------------------------------------------
Reg Name: POH CPE  VT Alarm Status register
Address: 0x087000 - 0x0877FF
The address format for these registers is 0x087000 + STSID * VTMAX + VTID
Where,  STSMAX = 6
    VTMAX = 28
    STSID: 0 -> STSMAX � 1
    VTID: 0 -> VTMAX � 1
Description: This is the POH CPE VT  alarm current status register.
It will be used to indicate current status of the event.
------------------------------------------------------------------------------*/
#define cThaRegPohSpeVtAlarmStatus 0x087000

#define cThaPohVtAlarmRdiStbMask cBit7
#define cThaPohVtAlarmRfiStbMask cBit6
#define cThaPohVtAlarmUneqMask cBit4
#define cThaPohVtAlarmPlmMask cBit3
#define cThaPohVtAlarmVcAisMask cBit2
#define cThaPohVtAlarmTtiTimMask cBit1

/*------------------------------------------------------------------------------
Reg Name: POH CPE  VT Alarm Interrupt Sticky registers
Address: 0x086800 - 0x086FFF
The address format for these registers is 0x086800 + STSID * VTMAX + VTID
Where,  STSMAX = 6
    VTMAX = 28
    STSID: 0 -> STSMAX � 1
    VTID: 0 -> VTMAX � 1
Description: This is the POH CPE VT alarm interrupt sticky register.
It will be used to indicate and sticky the event states change when the POH CPE engine
detected a changing of  event state.
------------------------------------------------------------------------------*/
#define cThaRegPohCpeVtAlarmIntrSticky 0x086800

#define cThaPohVtRdiStbChgIntMask cBit7
#define cThaPohVtPlmStbChgIntMask cBit3
#define cThaPohVtTtiTimStbChgIntMask cBit1

/*------------------------------------------------------------------------------
Reg Name: POH CPE VT Alarm Interrupt Enable Control register
Address: 0x086000 � 0x0867FF
The address format for these registers is 0x086000 + STSID * VTMAX + VTID
Where,  STSMAX = 6
    VTMAX = 28
    STSID: 0 -> STSMAX � 1
    VTID: 0 -> VTMAX - 1
Description: This is the per channel POH CPE VT alarm interrupt enable control register.
It is using to enable each event an alarm when it is detected.
------------------------------------------------------------------------------*/
#define cThaRegPohCpeVtAlarmIntrEnable 0x086000

#define cThaPohVtRdiStbChgIntEnbMask cBit7
#define cThaPohVtRdiStbChgIntEnbShift 7

#define cThaPohVtRfiStbChgIntEnbMask cBit6
#define cThaPohVtRfiStbChgIntEnbShift 6

#define cThaPohVtUneqStbChgIntEnbMask cBit4
#define cThaPohVtUneqStbChgIntEnbShift 4

#define cThaPohVtPlmStbChgIntEnbMask cBit3
#define cThaPohVtPlmStbChgIntEnbShift 3

#define cThaPohVtVcAisStbChgIntEnbMask cBit2
#define cThaPohVtVcAisStbChgIntEnbShift 2

#define cThaPohVtTtiTimStbChgIntEnbMask cBit1
#define cThaPohVtTtiTimStbChgIntEnbShift 1

/*------------------------------------------------------------------------------
Reg Name: POH CPE  VT BIP2 Error Counter
Address: 0x085000 - 0x0851FF
The address format for these registers is 0x085000 + STSID  * VTMAX + VTID
Where: STSMAX = 6
    VTMAX = 28
    STSID: 0 -> STSMAX-1
    VTID: 0 -> VTMAX-1
Description: This is the Common Processor Engine V5 BIP-2 Error Counter.
------------------------------------------------------------------------------*/
#define cThaRegPohCpeVtBipErrCnt 0x085000
#define cThaRegPohCpeVtBipErrMask cBit23_0

/*------------------------------------------------------------------------------
Reg Name: POH CPE VT REI-V Counter
Address: 0x085800 - 0x0859FF
The address format for these registers is 0x085800 + STSID * VTMAX + VTID
Where: STSMAX = 6
    VTMAX = 28
    STSID: 0 -> STSMAX-1
    VTID: 0 -> VTMAX-1
Description: This is the Common Processor Engine REI Counter.
------------------------------------------------------------------------------*/
#define cThaRegPohCpeVtReiVCnt 0x085800
#define cThaRegPohCpeVtReiVMask cBit23_0

/*------------------------------------------------------------------------------
Reg Name: POH CPE STS Path Over Head Grabber 1 Registers
Address: 0x084800 � 0x084800
The address format for these registers is 0x084800 +  STSMAX * VTMAX + STSID
Where: STSMAX = 6
    VTMAX = 28
    STSID: 0 -> STSMAX-1
Description: This is the Common Processor Engine Path Over Head Grabber Register 1.
------------------------------------------------------------------------------*/
#define cThaRegPohCpeStsTu3PathOhGrabber1    0x084800
#define cThaRegPohCpeStsTu3PathC2GrabberMask cBit23_16
#define cThaRegPohCpeStsTu3PathC2GrabberShift 16
#define cThaRegPohCpeStsTu3PathG1GrabberMask cBit15_8
#define cThaRegPohCpeStsTu3PathG1GrabberShift 8
#define cThaRegPohCpeStsTu3PathF2GrabberMask cBit7_0
#define cThaRegPohCpeStsTu3PathF2GrabberShift 0

/*------------------------------------------------------------------------------
Reg Name: POH CPE STS Path Over Head Grabber 2 Registers
Address: 0x084800 - 0x0848FF
The address format for these registers is 0x084800 +  STSMAX * VTMAX + STSMAX*2 + LIDMAX + STSID
Where: STSMAX = 6
    VTMAX = 28
    LIDMAX = 2
    STSID: 0 -> STSMAX-1
Description: This is the Common Processor Engine Path Over Head Grabber Register 2.
------------------------------------------------------------------------------*/
#define cThaRegPohCpeStsTu3PathOhGrabber2 (0x084800UL + STSMAX*2 + LIDMAX)
#define cThaRegPohCpeStsTu3PathH4GrabberMask cBit31_24
#define cThaRegPohCpeStsTu3PathH4GrabberShift 24
#define cThaRegPohCpeStsTu3PathF3GrabberMask cBit23_16
#define cThaRegPohCpeStsTu3PathF3GrabberShift 16
#define cThaRegPohCpeStsTu3PathK3GrabberMask cBit15_8
#define cThaRegPohCpeStsTu3PathK3GrabberShift 8
#define cThaRegPohCpeStsTu3PathN1GrabberMask cBit7_0
#define cThaRegPohCpeStsTu3PathN1GrabberShift 0

/*------------------------------------------------------------------------------
6.3.14. POH CPE VT Path Over Head Grabber 1 Registers
Address: 0x084800 � 0x084800
The address format for these registers is 0x084800 + STSID * VTMAX + VTID
Where: STSMAX = 6
    VTMAX = 28
    STSID: 0 -> STSMAX-1
    VTID: 0 -> VTMAX-1
Description: This is the Common Processor Engine Path Over Head Grabber Register 1.
------------------------------------------------------------------------------*/
#define cThaRegPohSpeVtPathOhGrabber1 (0x084800)
#define cThaRegPohCpeVtPathV5GrabberMask cBit31_24
#define cThaRegPohCpeVtPathV5GrabberShift 24
#define cThaRegPohCpeVtPathK4GrabberMask cBit15_8
#define cThaRegPohCpeVtPathK4GrabberShift 8
#define cThaRegPohCpeVtPathN2GrabberMask cBit7_0
#define cThaRegPohCpeVtPathN2GrabberShift 0

/*------------------------------------------------------------------------------
Reg Name: OCN Rx Line OC-3 line to Pointer STS-3 Control
Reg Addr: 0x0004000A
Reg Desc: Configure STS-3 ID for OC-3 lines in relative STS-12 slice at
          pointer processor.
------------------------------------------------------------------------------*/
#define cThaRegOcnRxLineOc3linetoPtrSts3Ctrl        0x04000A
#define cThaRegOcnTxSts3SrcOfTxLineCtrl             0x040013
#define cThaRegOcnTxSts3SrcOfTxLineCtrlRstValue     0x76543210

#define cThaOcnRxOc3LinetoSts3Mask(lineId)          (cBit2_0 << (lineId * 4))
#define cThaOcnRxOc3LinetoSts3Shift(lineId)         (0 + (lineId * 4))

#define cThaRegOcnRxLineOc3linetoPtrSts3CtrlNoneEditableFieldsMask (cBit31 | cBit27 | cBit23 | cBit19 | cBit15 | cBit11 | cBit7 | cBit3)
#define cThaRegOcnTxSts3SrcOfTxLineCtrlNoneEditableFieldsMask (cBit31 | cBit27 | cBit23 | cBit19 | cBit15 | cBit11 | cBit7 | cBit3)

/*------------------------------------------------------------------------------
Reg Name: OCN Tx PG Demux Per STS/VC Payload Control
Reg Addr: 0x00062080 - 0x0006408B
          The address format for these registers is 0x00062080 + SliceID*8192 +
          StsID
          Where: SliceID: (0) STS-12 slice ID
          Stsid: (0-11) STS-1/VC-3 ID
Reg Desc: These registers configure modes of STS-1/VC-3/TUG-3 channels at Tx
          pointer processor. Each register is used to configure the
          STS-1/VC-3/TUG-3 being relative with the address of the register to
          be in VT/TU mode or not. If it is in VT/TU mode, it is also used to
          configure this channel is in STS-1/VC-3 or TUG-3 mode and types of
          VT/TUs in the STS-1/VC-3/TUG-3.
------------------------------------------------------------------------------*/
#define cThaRegOcnTxPgDemuxPerStsVcPldCtrl 0x00062080

/*-----------
BitField Name: OcnTxPgStsDemuxSpeType
BitField Type: R/W
BitField Desc: the kind of SPE: TUG-3 or VC-3/STS SPE
               - 0: Disable processing pointers of all VT/TUs in this SPE.
               - 1: VC-3 type or STS SPE containing VT/TU exception TU-3
               - 2: TUG-3 type containing VT/TU exception TU-3
               - 3: TUG-3 type containing TU-3.
------------*/
#define cThaOcnTxPgStsDemuxSpeTypeMask                cBit15_14
#define cThaOcnTxPgStsDemuxSpeTypeShift               14

/*-----------
BitField Name: OcnTxPgStsDemuxTug2Type
BitField Type: R/W
BitField Desc: Define types of VT/TUs in TUG-2.
               - 0: TU11 type
               - 1: TU12 type
               - 2: VT3 type
               - 3: TU2 type
------------*/
#define cThaOcnTxPgStsDemuxTug2TypeMask(tug2Id)                               \
    (cBit1_0 << (uint16)((tug2Id) * 2))
#define cThaOcnTxPgStsDemuxTug2TypeShift(tug2Id)  ((tug2Id) * 2)

#define cThaRegOcnTxPgDemuxPerStsVcPldCtrlNoneEditableFieldsMask cBit31_16

/*------------------------------------------------------------------------------
Reg Name: OCN Tx VC4 Enable Control
Reg Addr: 0x0062023
Reg Desc: Configure rate for 4 SONET/SDH lines at Rx direction.
------------------------------------------------------------------------------*/
#define cThaRegOcnTxVc4EnCtrl                      0x0062023

#define OCNTxVC4EnMask(wStsId)                     (cBit0 << wStsId)
#define OCNTxVC4EnShift(wStsId)                    wStsId

#define cThaRegOcnTxVc4EnCtrlNoneEditableFieldsMask cBit31_12

/*------------------------------------------------------------------------------
Reg Name: OCN Tx Framer Engine Bandwidth Control
Reg Addr: 0x00060020
Reg Desc: Configure bandwidth Tx Framer/Pointer engines at Tx side.
------------------------------------------------------------------------------*/
#define cThaRegOcnTxFrmrEngBwCtrl                   0x00060020

/*--------------------------------------
BitField Name: OCNTxFrmEng1STS12
BitField Type:
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaOcnTxFrmEng1Sts12Mask                      cBit1
#define cThaOcnTxFrmEng1Sts12Shift                     1

/*--------------------------------------
BitField Name: OCNTxFrmEng1OC12
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
               - 0: Tx Framer engine is from OC-3 lines
               - 1: Tx Framer engine is only from one OC-12 line. It is noted
                 that this case is ignored in case of STS-3 bandwidth
--------------------------------------*/
#define cThaOcnTxFrmEng1Oc12Mask                       cBit0
#define cThaOcnTxFrmEng1Oc12Shift                      0

#define cThaRegOcnTxFrmrEngBwCtrlNoneEditableFieldsMask (cBit31_6 | cBit3_2)

/*------------------------------------------------------------------------------
Reg Name: OCN TOH Monitoring Per Channel Control
Reg Addr: 0x0074880 - 0x0074887
          The address format for these registers is 0x0074880 + lineID
          Where: lineID (0 - 3) SONET/SDH line IDs at TOH monitoring
Reg Desc: Each register is used to configure for TOH monitoring engine of the
          related line.
------------------------------------------------------------------------------*/
#define cThaRegOcnTohMonPerChnCtrl                  0x0074880

/*-----------
BitField Name: K2StbMd
BitField Type: R/W
BitField Desc: or K2[7:0] to detect validated K2 value
               - 1: K2[7:0] value is selected
               - 0: K2[7:3] value is selected
------------*/
#define cThaK2StbMdMask                               cBit3
#define cThaK2StbMdShift                              3

/*-----------
BitField Name: StbK1K2ThresSel
BitField Type: R/W
BitField Desc: Select the thresholds for detecting stable or non-stable K1/K2
               status.
               - 1: Threshold 2 is selected
               - 0: Threshold 1 is selected
------------*/
#define cThaStbK1K2ThresSelMask                       cBit2
#define cThaStbK1K2ThresSelShift                      2

/*-----------
BitField Name: StbS1ThresSel
BitField Type: R/W
BitField Desc: Select the thresholds for detecting stable or non-stable S1
               status
               - 1: Threshold 2 is selected
               - 0: Threshold 1 is selected
------------*/
#define cThaStbS1ThresSelMask                         cBit1
#define cThaStbS1ThresSelShift                        1

#define cThaRegOcnTohMonPerChnCtrlNoneEditableFieldsMask cBit31_9

/*------------------------------------------------------------------------------
Reg Name: OCN Framer Alarm Affect Control
Reg Addr: 0x00040223
Reg Desc: Configure to enable to generate AIS-P into payload data to STS/VC XC
          or RDI-L into K2 byte at the transmit direction when some kinds of
          defects are detected.
------------------------------------------------------------------------------*/
#define cThaRegOcnFrmrAlmAffCtrl                      0x00040223
#define cThaRegOcnFrmrAlmAffCtrlRstVal                0xFF

/*-----------
BitField Name: TimLAisPEn
BitField Type: R/W
BitField Desc: Enable/disable the insertion of P-AIS at STS pointer generator
               when detecting TIM-L condition at SONTOHMON
               - 1: Enable.
               - 0: Disable.
------------*/
#define cThaTimLAisPEnMask                            cBit9
#define cThaTimLAisPEnShift                           9

/*-----------
BitField Name: TimLRdiLEn
BitField Type: R/W
BitField Desc: Enable/disable the insertion of RDI-L at TOH insertion when
               detecting TIM-L condition at SONTOHMON
               - 1: Enable.
               - 0: Disable.
------------*/
#define cThaTimLRdiLEnMask                            cBit8
#define cThaTimLRdiLEnShift                           8

/*-----------
BitField Name: AisLAisPEn
BitField Type: R/W
BitField Desc: Enable/disable the insertion of P-AIS at STS pointer generator
               when detecting AIS-L condition at SONTOHMON
               - 1: Enable.
               - 0: Disable.
------------*/

/*-----------
BitField Name: LosAisPEn
BitField Type: R/W
BitField Desc: Enable/disable the insertion of P-AIS at STS pointer generator
               when detecting LOS condition at SONTOHMON.
               - 1: Enable
               - 0: Disable
------------*/

/*-----------
BitField Name: LofAisPEn
BitField Type: R/W
BitField Desc: Enable/disable to insert P-AIS at STS pointer generator when
               detecting LOF condition at SONTOHMON.
               - 1: Enable
               - 0: Disable
------------*/

/*-----------
BitField Name: OofAisPEn
BitField Type: R/W
BitField Desc: Enable/disable the insertion of P-AIS at STS pointer generator
               when detecting OOF condition at SONTOHMON
               - 1: Enable.
               - 0: Disable.
------------*/

/*-----------
BitField Name: AisLRdiLEn
BitField Type: R/W
BitField Desc: Enable/disable the insertion of RDI-L at TOH insertion when
               detecting AIS-L condition at SONTOHMON
               - 1: Enable.
               - 0: Disable.
------------*/
#define cThaAisLRdiLEnMask                            cBit3
#define cThaAisLRdiLEnShift                           3

/*-----------
BitField Name: LosRdiLEn
BitField Type: R/W
BitField Desc: Enable/disable the insertion of RDI-L at TOH insertion when
               detecting LOS condition at SONTOHMON
               - 1: Enable.
               - 0: RDI in the transmit K2 byte isn't inserted regardless of
                   detection of LOS condition.
------------*/
#define cThaLosRdiLEnMask                             cBit2
#define cThaLosRdiLEnShift                            2

/*-----------
BitField Name: LofRdiLEn
BitField Type: R/W
BitField Desc: Enable/disable the insertion of RDI-L at TOH insertion when
               detecting LOF condition at SONTOHMON
               - 1: Enable.
               - 0: Disable.
------------*/
#define cThaLofRdiLEnMask                             cBit1
#define cThaLofRdiLEnShift                            1

/*-----------
BitField Name: OofRdiLEn
BitField Type: R/W
BitField Desc: Enable/disable the insertion of RDI-L at TOH insertion when
               detecting OOF condition at SONTOHMON
               - 1: Enable.
               - 0: Disable.
------------*/
#define cThaOofRdiLEnMask                             cBit0
#define cThaOofRdiLEnShift                            0

#define cThaRegOcnFrmrAlmAffCtrlNoneEditableFieldsMask cBit31_10

/*------------------------------------------------------------------------------
Reg Name: OCN TOH Monitoring affect Control
Reg Addr: 0x007498F
Reg Desc: Configure affective mode for TOH monitoring.
------------------------------------------------------------------------------*/
#define cThaRegOcnTohMonAffCtrl                       0x007498F

#define cThaLosMaskLofEnableMask    cBit6
#define cThaLosMaskLofEnableShift   6

/*-----------
BitField Name: TohMonAisAffStbEnB
BitField Type: R/W
BitField Desc: AIS affects to stable monitoring
               - 1: Disable clearing stable monitoring status of the line at
                    which LOF or LOS is detected.
               - 0: Enable clearing stable and APS monitoring status of the
                    line at which LOF or LOS is detected.
------------*/
#define cThaTohMonAisAffStbEnBMask                    cBit3
#define cThaTohMonAisAffStbEnBShift                   3

/*-----------
BitField Name: TohMonAisAffErrCntEnB
BitField Type: R/W
BitField Desc: AIS affects to error counters (B1, REI)
               - 1: Disable clearing error counters of the line at which LOF
                    or LOS is detected.
               - 0: Enable clearing error counters of the line at which LOF
                    or LOS is detected.
------------*/
#define cThaTohMonAisAffErrCntEnBMask                 cBit0
#define cThaTohMonAisAffErrCntEnBShift                0

#define cThaRegOcnRxStsPtrAdjPerChnCnt 0x00052300
#define cThaRegOcnPtrAdjCntMask cBit17_0

#define cThaRegOcnRxVtPtrAdjPerChnCnt 0x00056000
#define cThaRegOcnTohMonAffCtrlNoneEditableFieldsMask cBit31_5

#define cThaRegOcnTxStsVtPtrAdjPerChnCnt 0x00066000

/*------------------------------------------------------------------------------
Reg Name: OCN LED Control
Reg Addr: 0x40
Reg Desc: Control LEDs of STM ports
------------------------------------------------------------------------------*/
#define cThaRegOcnLedCtrl 0x40
#define cThaRegOcnLedCtrlRstValue 0x70000

#define cThaRegLineLedStateForceMask(portId)   (cBit0 << cThaRegLineLedStateForceShift(portId))
#define cThaRegLineLedStateForceShift(portId)  (portId * 4)
#define cThaRegLineLedStateStatMask(portId)    (cBit0 << cThaRegLineLedStateStatShift(portId))
#define cThaRegLineLedStateStatShift(portId)   ((portId * 4) + 1)

/*------------------------------------------------------------------------------
Reg Name: Top Ref Clock Output Control
Reg Addr: f00002
Reg Desc: Control Ref Clock Output
------------------------------------------------------------------------------*/
#define cThaRegClockOutputCtrl 0xf00002

#define cThaRegClockOutputCtrlRefClkInModMask(portId)  (cBit24 << (portId))
#define cThaRegClockOutputCtrlRefClkInModShift(portId) ((24 + (portId)))

#define cThaRegClockOutputCtrlRefClkOutDisMask(outClockId)  (cBit20 << (outClockId))
#define cThaRegClockOutputCtrlRefClkOutDisShift(outClockId) (20 + (outClockId))

#define cThaRegClockOutputCtrlRefClkOutSelMask(outClockId)  (cBit3_0 << ((outClockId) * 4))
#define cThaRegClockOutputCtrlRefClkOutSelShift(outClockId) ((outClockId) * 4)

/*------------------------------------------------------------------------------
Reg Name: OCN counter control
Address: 0x007498D
Description: Configure mode for counters in TOH monitoring.
------------------------------------------------------------------------------*/
#define cThaRegOcnCounterControl 0x007498D
#define cThaRegOcnB2ErrCntMdMask cBit2
#define cThaRegOcnB2ErrCntMdShift 2
#define cThaRegOcnReiErrCntMdMask cBit1
#define cThaRegOcnReiErrCntMdShift 1
#define cThaRegOcnB1ErrCntMdMask cBit0
#define cThaRegOcnB1ErrCntMdShift 0

/*------------------------------------------------------------------------------
Reg Name: OCN Pointer Interpreter Master Control
Reg Addr: 0x00050000
Reg Desc: Master configuration for SONPI
------------------------------------------------------------------------------*/
#define cThaRegOcnPiMastCtrl                          0x00050000

/*--------------------------------------
BitField Name: Testmode2
BitField Type: R/W
BitField Desc: Set AIS downstream for each type of STS defect
--------------------------------------*/
#define cThaOcnPiStsDefGenAisDownStrEnMask                cBit31_28
#define cThaOcnPiStsDefGenAisDownStrEnShift               28

/*--------------------------------------
BitField Name: Testmode2
BitField Type: R/W
BitField Desc: Enable insert AIS downstream when VT defects happen
--------------------------------------*/
#define cThaOcnPiVtAisDownStrEnMask                       cBit27
#define cThaOcnPiVtAisDownStrEnShift                      27

/*--------------------------------------
BitField Name: Testmode1
BitField Type: R/W
BitField Desc: Enable insert AIS downstream when STS defects happen
--------------------------------------*/
#define cThaOcnPiStsAisDownStrEnMask                       cBit26
#define cThaOcnPiStsAisDownStrEnShift                      26

/*--------------------------------------
BitField Name: OCNPiCepVc4
BitField Type: R/W
BitField Desc: Enable to support VC-4 CEP.
--------------------------------------*/

/*--------------------------------------
BitField Name: OCNPiCepEn
BitField Type: R/W
BitField Desc: Enable to support CEP mode.
--------------------------------------*/

/*--------------------------------------
BitField Name: OCNPiNdfPtrThres[3:0]
BitField Type: R/W
BitField Desc: Threshold of the number of contiguous NDF pointers for entering
               LOP state at pointer interpreter.
--------------------------------------*/

/*--------------------------------------
BitField Name: OCNPiAffMode1
BitField Type: R/W
BitField Desc: Define affect mode for STS/VC/VT/TU pointer interpreter state
               machine when receiving hi-order defect (LOF, LOS, STS LOP, ect).
               - 1: the state machine always operates regardless hi-order
                 defect
               - 0: The pointer interpreter is forced to normal state when
                 receiving hi-order defect
--------------------------------------*/

/*--------------------------------------
BitField Name: OCNPiAffMode0
BitField Type: R/W
BitField Desc: Define affect mode for sticky bits at STS/VC/VT/TU pointer
               interpreter when receiving hi-order defect (LOF, LOS, STS LOP,
               ect).
               - 1: sticky bits are always set regardless hi-order defect
               - 0: sticky bits are cleared when receiving hi-order defect
                 (recommended)
--------------------------------------*/

/*--------------------------------------
BitField Name: OCNPiNormThres[1:0]
BitField Type: R/W
BitField Desc: Define the number of normal pointer frames between two
               contiguous frames with pointer adjustments.
--------------------------------------*/

/*--------------------------------------
BitField Name: OCNPiAisAisPEn
BitField Type: R/W
BitField Desc: High to enable generating P-AIS at Pointer generator when AIS
               state is detected at Pointer interpreter.
--------------------------------------*/

/*--------------------------------------
BitField Name: OCNPiLopAisPEn
BitField Type: R/W
BitField Desc: High to enable generating P-AIS at Pointer generator when Lop
               state is detected at Pointer interpreter.
--------------------------------------*/

/*--------------------------------------
BitField Name: OCNPiMajorEn
BitField Type: R/W
BitField Desc: Select majority mode for in STS pointer Interpreter. It is used
               for rule n of 5.
               - 1:  n = 3.
               - 0:  n = 5.
--------------------------------------*/

/*--------------------------------------
BitField Name: OCNPiAis1En
BitField Type: R/W
BitField Desc: Default zero
               - 1: AIS state is entered only with 1 AIS indication only for
                 testing
               - 0: AIS state is entered with 3 AIS indications.
--------------------------------------*/

/*--------------------------------------
BitField Name: OCNPiBadPtrThres[3:0]
BitField Type: R/W
BitField Desc: Threshold of the number of bad pointers for entering LOP state
               at pointer interpreter.
--------------------------------------*/

/*--------------------------------------
BitField Name: OCNUneqmode[4]
BitField Type: R/W
BitField Desc: Threshold of the number of bad pointers for entering LOP state
               at pointer interpreter.
--------------------------------------*/

/*--------------------------------------
BitField Name: OcnPiStsPohAisType[4]
BitField Type: R/W
BitField Desc: High to enable STS POH defect types to generate AIS to
downstream in case of terminating the related STS such as the STS carries VT/TU
    Bit[0]: Enable for TIM defect
    Bit[1]: Enable for Uneqiped defect
    Bit[2]: Enable for VC-AIS
    Bit[3]: Enable for PLM defect
--------------------------------------*/
#define cThaOcnPiStsPohAisTypeMask                       cBit3_0
#define cThaOcnPiStsPohAisTypeShift                      0

/*--------------------------------------
OCN TU De-multiplexing  Master Control
Address: 0x00050001
Description:  Master configuration for De-multiplexing TU in STS-1/VC3/TUG3 payload
--------------------------------------*/
#define cThaRegOcnTuDemuxMasterControl 0x50001
#define cThaRegOcnTuDemuxMasterControlDefault 0x122

/* Source option
 * 0x0: normal
 * 0x1: from dedicated line on FPGA#2 (1<->5, 2<->6,...)
 * 0x3: from dedicated line on card#2 (1<->1, 2<->2,...)
 *  */
#define cThaRegOcnRxLineSourceCtrl               0x4022F
#define cThaOcnRxLineSourceMask(lineId)          (cBit3_0 << ((lineId) * 4))
#define cThaOcnRxLineSourceShift(lineId)         ((lineId) * 4)

/* Transmit option, line 5->8, only option 1 & 3 is supported
 * 0x0: normal
 * 0x1: duplicate data to dedicated line on FPGA#2 (1<->5, 2<->6,...)
 * 0x3: duplicate data to dedicated line on card#2 (1<->1, 2<->2,...)
 *  */

/*
 *  Configure to set APS working mode for line
 * 0x1: Switch mode for dedicated line
 * 0x0: Bridge mode for dedicated line
 *  */

/*------------------------------------------------------------------------------
Reg Name: OCN Tx Framer  Source Control
Reg Addr: 0x00040A25
Reg Desc:
------------------------------------------------------------------------------*/
#define cThaRegOcnTxFrmSrcCtrl     0x00040A25

/* OCNTxTOHLineSrc */

/*------------------------------------------------------------------------------
Reg Name: OCN RX LOS force
Reg Addr: 0x0004000B
Reg Desc: Configure rate for 8 SONET/SDH lines at Rx direction.
------------------------------------------------------------------------------*/
#define cThaRegOcnLosForce                     0x0004001A

/*------------------------------------------------------------------------------
Reg Name: OCN Global Control
Reg Addr: 0x0004000B
Reg Desc: Configure rate for 8 SONET/SDH lines at Rx direction.
------------------------------------------------------------------------------*/
#define cThaRegOcnGlbCtrl                     0x0004000B

#define cThaOcnSysBusLoopOutEnMask             cBit0
#define cThaOcnSysBusLoopOutEnShift            0

#define cThaOcnSlaverPartMask                 cBit1
#define cThaOcnSlaverPartShift                1

/*------------------------------------------------------------------------------
Reg Name: OCN SD/SF Line/Section Status Control
Reg Addr: 0x0074991
Reg Desc: This register is used to store Software SD/SF status. Software SD/SF engine will update
status into this register
------------------------------------------------------------------------------*/
#define cThaRegOcnSDSFLineSectionInterruptStatusLatchControl  0x0074991

#define cThaOcnLineSfLatchMask(lineId)  (cBit0 << (lineId))

#define cThaOcnLineSdLatchMask(lineId)  (cBit4 << (lineId))

#define cThaOcnSectSfLatchMask(lineId)  (cBit8 << (lineId))

#define cThaOcnSectSdLatchMask(lineId)  (cBit12 << (lineId))

/*------------------------------------------------------------------------------
Reg Name: OCN LOS detection thresholds
Reg Addr: 0x4001b
Reg Desc: Threshold in 154.32ns unit
------------------------------------------------------------------------------*/



/*------------------------------------------------------------------------------
Reg Name: POH Master Control
Reg Addr: 0x080005
Reg Desc: This Master Control Register controls the operation of Path Overhead
          Processor Engine.
------------------------------------------------------------------------------*/
#define cThaRegPohMastCtrl            0x080005

#define cThaUneqModeWhenC2ExpIs0Mask  cBit3
#define cThaUneqModeWhenC2ExpIs0Shift 3

/*------------------------------------------------------------------------------
Reg Name: OCN STS PI CEP VT-Fractional Mode
Reg Addr: 0x00052007
Reg Desc: The register consists of 12 bits for 12 VCs in the related STS-12 slice
          to set the CEP VT-Fractional mode for 12 STS/VCs.
------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name: POH BIP8/REI-P Block Counter Registers
Reg Addr: 0x085C00 - 0x085DFF
          Address is as following:
          - STS: 0x085C00 +  STSMAX * VTMAX + STSID
          - TU3: 0x085C00 +  STSMAX * VTMAX + STSMAX + TU3ID
          - VT:  0x085C00 + STSID  * VTMAX + VTID
Reg Desc:
------------------------------------------------------------------------------*/
#define cThaRegPohCpeBip8ReiBlockCounter 0x085C00

#define cThaPohCpeBip8CounterMask  cBit15_0
#define cThaPohCpeBip8CounterShift 0

#define cThaPohCpeReiCounterMask  cBit31_16
#define cThaPohCpeReiCounterShift 16

/*------------------------------------------------------------------------------
Reg Name: OCN Tx Framer TOH Source Control
Reg Addr: 0x00040A24
Reg Desc:
------------------------------------------------------------------------------*/
#define cThaRegOcnTxFrmTohSrcCtrl           (0x00040A24)

/* OCNTxTOHLineSrc */
#define cThaRegOcnTxTohLineSrcMask(lineId)  (cBit1_0 << (lineId * 2))
#define cThaRegOcnTxTohLineSrcShift(lineId) (lineId * 2)

/*------------------------------------------------------------------------------
Reg Name: OCN Rx Reference Timing to Timing Part Control
Reg Addr: 0x0040015
Reg Desc: Configure 4 sources for 4 timing reference signals to Timing Part.
------------------------------------------------------------------------------*/
#define cThaRegOcnRxRefSrcForTimingPartCtrl             0x0040015

/*------------------------------------------------------------------------------
Reg Name: OCN Rx Line Output Control
Reg Addr: 0x00040231
Reg Desc: Configure to enable output data to downstream or other core or not.
------------------------------------------------------------------------------*/
#define cThaOcnRxLineOutputControl             0x00040231

#define cThaOcnRxLOut2DwnstEnMask(lineId)      (cBit0 << (lineId))
#define cThaOcnRxLOut2DwnstEnShift(lineId)     (lineId)

/*------------------------------------------------------------------------------
Reg Name: OCN K1 Stable Monitoring Threshold Control
Reg Addr: 0x0074984
Reg Desc: Store thresholds for monitoring change of K1 state at TOH monitoring.
There are two thresholds for each kind of threshold. Software configures to select one threshold for use at the time.
------------------------------------------------------------------------------*/
#define cThaRegOcnK1StableMonThresControl                    0x0074984

#define cThaRegOcnK1StableThreshold2Mask                     cBit10_8
#define cThaRegOcnK1StableThreshold2Shift                    8

#define cThaRegOcnK1StableThreshold1Mask                     cBit2_0
#define cThaRegOcnK1StableThreshold1Shift                    0

/*------------------------------------------------------------------------------
Reg Name: OCN K2 Stable Monitoring Threshold Control
Reg Addr: 0x0074985
Reg Desc: Store thresholds for monitoring change of K2 state at TOH monitoring.
There are two thresholds for each kind of threshold. Software configures to select one threshold for use at the time.
------------------------------------------------------------------------------*/
#define cThaRegOcnK2StableMonThresControl                    0x0074985

#define cThaRegOcnK2StableThreshold2Mask                     cBit10_8
#define cThaRegOcnK2StableThreshold2Shift                    8

#define cThaRegOcnK2StableThreshold1Mask                     cBit2_0
#define cThaRegOcnK2StableThreshold1Shift                    0

/*------------------------------------------------------------------------------
Reg Name: Telecombus Rx Per STS/VC Payload Control
Reg Address: 0x00058000 � 0x0005800B
         The address format for these registers is 0x00058000 + SliceID*8192 + StsID
         Where: SliceID: (0) STS-12 slice ID
         Stsid: (0-11) STS-1/VC-3/TUG-3 ID in the STS-12 slice
Reg Description: These registers configure modes of STS-1/VC-3/TUG-3 channels at Rx Telecombus. Each register
is used to configure the STS-1/VC-3/TUG-3 being relative with the address of the register to be in VT/TU mode
or not. If it is in VT/TU mode, it is also used to configure this channel is in STS-1/VC-3 or TUG-3 mode and types
of VT/TUs in the STS-1/VC-3/TUG-3.
------------------------------------------------------------------------------*/
#define cThaRegOcnTelecombusRxPerStsPayloadCtrl              0x00058000

/*--------------------------------------
BitField Name: RxtelcomSlvInd
BitField Type: R/W
BitField Desc: Slaver bit
               1: Indicate that the STS-1/VC-3 is the slaver STS-1/VC-3 in the concatenation.
               0: Indicate that the STS-1/VC-3 is not in the concatenation or the master STS-1/VC-3
               if it is the concatenation.
--------------------------------------*/
#define cThaRxtelcomSlvIndMask                         cBit20
#define cThaRxtelcomSlvIndShift                        20

/*--------------------------------------
BitField Name: RxtelcomMastID
BitField Type: R/W
BitField Desc: The master STS-1 ID. This is the ID of the master STS-1 in the concatenation
that contains this STS-1. If this STS-1 is not in the concatenation, this field is ignored. It is noted that
all STS-1/VC-3s in the concatenation group are in one SONET/SDH line.
--------------------------------------*/
#define cThaRxtelcomMastIDMask                         cBit19_16
#define cThaRxtelcomMastIDShift                        16

/*-----------
BitField Name: RxTelcomStsDemuxSpeType
BitField Type: R/W
BitField Desc: the kind of SPE: TUG-3 or VC-3/STS SPE
               - 0: Disable processing pointers of all VT/TUs in this SPE.
               - 1: VC-3 type or STS SPE containing VT/TU exception TU-3
               - 2: TUG-3 type containing VT/TU exception TU-3
               - 3: TUG-3 type containing TU-3.
------------*/
#define cThaOcnRxTelcomStsDemuxSpeTypeMask             cBit15_14
#define cThaOcnRxTelcomStsDemuxSpeTypeShift            14

/*-----------
BitField Name: RxTelcomStsDemuxTug2Type
BitField Type: R/W
BitField Desc: Define types of VT/TUs in TUG-2
               - 0: TU11 type
               - 1: TU12 type
               - 2: VT3 type
               - 3: TU2 type
------------*/
#define cThaOcnRxTelcomStsDemuxTug2TypeMask(tug2Id)     (cBit1_0 << (uint16)((tug2Id) * 2))
#define cThaOcnRxTelcomStsDemuxTug2TypeShift(tug2Id)    ((tug2Id) * 2)

/*------------------------------------------------------------------------------
Reg Name: Telecombus Rx Alarm Interrupt Status
Reg Address: 0x0005801F
Reg Description: This is the per Alarm interrupt status of Rx telecombus.
------------------------------------------------------------------------------*/
#define cThaRegOcnTelecombusRxAlarmInteruptStatus      0x0005801F

/*-----------
BitField Name: RxTelParityErr
BitField Type: R/W
BitField Desc: Set to 1 when detecting one parity error.
------------*/
#define cThaOcnRxTelcomParityErrorMask               cBit3
#define cThaOcnRxTelcomParityErrorShift              3

/*-----------
BitField Name: RxTelSyncErr
BitField Type: R/W
BitField Desc: Set to 1 when detecting one error in frame sync.
------------*/
#define cThaOcnRxTelcomFrameSyncErrorMask             cBit2
#define cThaOcnRxTelcomFrameSyncErrorShift            2

/*-----------
BitField Name: RxTelFFHalfFull
BitField Type: R/W
BitField Desc: Set to 1 when Rx FiFo conversion is nearly full. It's for debugging only
------------*/
#define cThaOcnRxTelcomFifoHalfFullMask               cBit1
#define cThaOcnRxTelcomFifoHalfFullShift              1

/*-----------
BitField Name: RxTelFFFull
BitField Type: R/W
BitField Desc: Set to 1 when Rx FiFo conversion is full. It's for debugging only
------------*/
#define cThaOcnRxTelcomFifoFullMask                    cBit0
#define cThaOcnRxTelcomFifoFullShift                   0

/*------------------------------------------------------------------------------
Reg Name: 24-bit block error counters
Reg Address:
Reg Description:
------------------------------------------------------------------------------*/
#define cThaRegPohCpeBipBlockError24BitsCounter 0x085C00
#define cThaRegPohCpeReiBlockError24BitsCounter 0x085400

#ifdef __cplusplus
}
#endif
#endif

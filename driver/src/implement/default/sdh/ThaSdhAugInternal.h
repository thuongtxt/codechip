/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : ThaSdhAugInternal.h
 * 
 * Created Date: Jun 12, 2014
 *
 * Description : SDH AUG
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASDHAUGINTERNAL_H_
#define _THASDHAUGINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/sdh/AtSdhAugInternal.h"
#include "ThaModuleSdh.h"
#include "AtSdhVc.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaSdhAug * ThaSdhAug;

typedef struct tThaSdhAugMethods
    {
    eBool (*NeedOcnConcate)(ThaSdhAug self);
    eBool (*NeedPdhConcate)(ThaSdhAug self);
    eBool (*NeedMapConcate)(ThaSdhAug self);
    eBool (*OcnIsConcatenated)(ThaSdhAug self);
    eBool (*PdhIsConcatenated)(ThaSdhAug self);
    eBool (*MapIsConcatenated)(ThaSdhAug self);

    /* To handle default mapping type */
    eAtSdhVcMapType (*DefaultVc4MapType)(ThaSdhAug self);
    eAtSdhVcMapType (*DefaultVc3MapType)(ThaSdhAug self);
    }tThaSdhAugMethods;

typedef struct tThaSdhAug
    {
    tAtSdhAug super;
    const tThaSdhAugMethods *methods;

    /* Private data */
    }tThaSdhAug;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhAug ThaSdhAugObjectInit(AtSdhAug self, uint32 channelId, uint8 channelType, AtModuleSdh module);
eAtRet ThaSdhAugSts1sInit(AtSdhChannel aug);

#endif /* _THASDHAUGINTERNAL_H_ */


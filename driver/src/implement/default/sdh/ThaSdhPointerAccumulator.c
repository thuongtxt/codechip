/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60210011SdhPointerAdjustmentDb.c
 *
 * Created Date: Jan 25, 2016
 *
 * Description : Pointer counter accumulator
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSdhPath.h"
#include "ThaSdhPointerAccumulatorInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 *CounterAddress(ThaSdhPointerAccumulator self, uint16 counterType)
    {
    switch (counterType)
        {
        case cAtSdhPathCounterTypeRxPPJC: return &self->rxPiIncCnt;
        case cAtSdhPathCounterTypeRxNPJC: return &self->rxPiDecCnt;
        case cAtSdhPathCounterTypeTxPPJC: return &self->txPgIncrCnt;
        case cAtSdhPathCounterTypeTxNPJC: return &self->txPgDecCnt;
        default: return NULL;
        }
    }

static uint32 CounterUpdateThenClear(ThaSdhPointerAccumulator self, uint16 counterType, uint32 counterValue, eBool clear)
    {
    uint32 counter;
    uint32 *pCounter;

    pCounter = CounterAddress(self, counterType);
    if (pCounter == NULL)
        return 0;

    *pCounter = *pCounter + counterValue;
    counter = *pCounter;
    if (clear)
        *pCounter = 0;

    return counter;
    }

static uint32 CounterRead2Clear(ThaSdhPointerAccumulator self, uint16 counterType, eBool read2Clear)
    {
    uint32 *pCounter;
    uint32 counter;

    pCounter = CounterAddress(self, counterType);
    if (pCounter == NULL)
        return 0;

    counter = *pCounter;
    if (read2Clear)
        *pCounter = 0;

    return counter;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaSdhPointerAccumulator);
    }

static ThaSdhPointerAccumulator ObjectInit(ThaSdhPointerAccumulator self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    return self;
    }

ThaSdhPointerAccumulator ThaSdhPointerAccumulatorNew(void)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaSdhPointerAccumulator self = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (self == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(self);
    }

uint32 ThaSdhPointerAccumulatorCounterUpdateThenClear(ThaSdhPointerAccumulator self, uint16 counterType, uint32 counterValue, eBool clear)
    {
    if (self)
        return CounterUpdateThenClear(self, counterType, counterValue, clear);
    return 0;
    }

uint32 ThaSdhPointerAccumulatorCounterRead2Clear(ThaSdhPointerAccumulator self, uint16 counterType, eBool read2Clear)
    {
    if (self)
        return CounterRead2Clear(self, counterType, read2Clear);
    return 0;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : ThaSdhVc1x.c
 *
 * Created Date: Oct 19, 2012
 *
 * Description : VC-1x
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../generic/sdh/AtSdhAugInternal.h"
#include "../../../generic/pdh/AtModulePdhInternal.h"

#include "ThaSdhVcInternal.h"
#include "ThaModuleSdh.h"

#include "../map/ThaModuleStmMap.h"
#include "../cdr/ThaModuleCdrStm.h"
#include "../pdh/ThaPdhVcDe1.h"
#include "../pdh/ThaStmModulePdh.h"
#include "../ocn/ThaModuleOcn.h"
#include "../cla/pw/ThaModuleClaPwInternal.h"
#include "../pdh/ThaModulePdhReg.h"
#include "../pdh/ThaModulePdhInternal.h"
#include "../../include/att/AtAttController.h"
#include "../../../util/coder/AtCoderUtil.h"
#include "ThaModuleSdhInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/
#define mThis(self)   ((tThaSdhVc1x*)self)
/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaSdhVc1xMethods m_methods;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtChannelMethods    m_AtChannelOverride;
static tAtSdhPathMethods    m_AtSdhPathOverride;
static tThaSdhVcMethods     m_ThaSdhVcOverride;
static tAtSdhChannelMethods m_AtSdhChannelOverride;

/* To save super implementation */
static const tAtObjectMethods     *m_AtObjectMethods     = NULL;
static const tAtSdhChannelMethods *m_AtSdhChannelMethods = NULL;
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtObjectAny AttController(AtChannel self)
    {
    ThaModuleSdh module = (ThaModuleSdh)AtChannelModuleGet(self);
    if (mThis(self)->attController == NULL)
        {
        mThis(self)->attController = mMethodsGet(module)->Vc1xAttControllerCreate(module, self);
        if (mThis(self)->attController)
            {
            AtAttControllerSetUp(mThis(self)->attController);
            AtAttControllerInit(mThis(self)->attController);
            }
        }
    return mThis(self)->attController;
    }

static void AttControllerDelete(ThaSdhVc1x self)
    {
    if (mThis(self)->attController)
        AtObjectDelete((AtObject)mThis(self)->attController);
    mThis(self)->attController = NULL;
    }

static uint32 PartOffset(ThaSdhVc self)
    {
    ThaDevice device = (ThaDevice)AtChannelDeviceGet((AtChannel)self);
    return ThaDeviceModulePartOffset(device, cThaModuleOcn, ThaModuleSdhPartOfChannel((AtSdhChannel)self));
    }

static uint32 OcnDefaultOffset(ThaSdhVc self)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)self;
    uint8 tug2Id = AtSdhChannelTug2Get(sdhChannel);
    uint8 tuId   = AtSdhChannelTu1xGet(sdhChannel);
    uint8 sliceId, stsId;

    ThaSdhChannel2HwMasterStsId(sdhChannel, cThaModuleOcn, &sliceId, &stsId);

    return (uint32)(((sliceId * 8192UL) + (stsId * 32UL) + (tug2Id * 4UL) + tuId) + PartOffset(self));
    }

static AtChannel MapChannelGet(AtSdhChannel self)
    {
    if (AtSdhChannelMapTypeGet(self) == cAtSdhVcMapTypeVc1xMapDe1)
        return (AtChannel)SdhVcPdhChannelGet((AtSdhVc)self);
    return NULL;
    }

static uint8 MapTypeOfMapModule(AtSdhChannel self, uint8 mapType)
	{
    eAtSdhChannelType vcType = AtSdhChannelTypeGet(self);

	if (mapType == cAtSdhVcMapTypeVc1xMapC1x)
		{
		if (vcType == cAtSdhChannelTypeVc12) return cThaCfgMapPldVt2;
		if (vcType == cAtSdhChannelTypeVc11) return cThaCfgMapPldVt15;
		}
	else
		{
		if (vcType == cAtSdhChannelTypeVc12) return cThaCfgMapPldE1;
		if (vcType == cAtSdhChannelTypeVc11) return cThaCfgMapPldDs1;
		}

	return cThaCfgMapPldE1;
	}

static eThaPdhVtMapMd PdhVcMapMode(AtSdhChannel self, uint8 mapType)
    {
    if (mapType == cAtSdhVcMapTypeVc1xMapDe1)
        return (AtSdhChannelTypeGet(self) == cAtSdhChannelTypeVc12) ? cThaVt2E1MapMode : cThaVt15Ds1MapMode;

    return (AtSdhChannelTypeGet(self) == cAtSdhChannelTypeVc12) ? cThaVt2C12MapMode : cThaVt15C11MapMode;
    }

static eAtModuleSdhRet MapTypeSet(AtSdhChannel self, uint8 mapType)
    {
    eAtRet ret;
    ThaModuleMap mapModule;
    AtPdhChannel pdhChannel;
    ThaModuleCdr cdrModule;
    eBool hwAccess = cAtTrue;
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cAtModulePdh);

    if(!AtSdhChannelNeedToUpdateMapType(self, mapType))
        return cAtOk;

    /* Let the super do first */
    ret = m_AtSdhChannelMethods->MapTypeSet(self, mapType);
    if (ret != cAtOk)
        return ret;

    /* When mapping is none, just need to disable POH insertion of this layer so
     * that internal loopback feature can work */
    if (mapType == cAtSdhVcMapTypeNone)
        {
        ThaOcnVtPohInsertEnable(self, AtSdhChannelSts1Get(self), AtSdhChannelTug2Get(self), AtSdhChannelTu1xGet(self), cAtFalse);
        return cAtOk;
        }

    /* Other mapping, need enable POH insertion */
    ThaOcnVtPohInsertEnable(self, AtSdhChannelSts1Get(self), AtSdhChannelTug2Get(self), AtSdhChannelTu1xGet(self), cAtTrue);
    if (AtChannelDeviceInWarmRestore((AtChannel)self))
        hwAccess = cAtFalse;

    if (hwAccess)
        {
        mapModule = (ThaModuleMap)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cThaModuleMap);

        /* Configure MAP module */
        ret |= ThaMapVtPldMdSet(mapModule, self, AtSdhChannelSts1Get(self), AtSdhChannelTug2Get(self), AtSdhChannelTu1xGet(self), MapTypeOfMapModule(self, mapType));
        ret |= ThaPdhVtMapMdSet(pdhModule, self, PdhVcMapMode(self, mapType));

        /* Configure CDR module */
        cdrModule = (ThaModuleCdr)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cThaModuleCdr);
        ret |= ThaCdrVtPldSet(cdrModule, self, mapType);
        }

    /* Map DS1/E1 */
    if (mapType != cAtSdhVcMapTypeVc1xMapDe1)
        {
        ThaSdhVcPdhChannelDelete((AtSdhVc)self);
        return cAtOk;
        }

    /* This VC will have one AtPdhDe1 mapped inside */
    pdhChannel = SdhVcPdhChannelGet((AtSdhVc)self);
    if (pdhChannel == NULL)
        {
        pdhChannel = (AtPdhChannel)mMethodsGet((ThaSdhVc1x)self)->De1Create((ThaSdhVc1x)self, pdhModule);
        SdhVcPdhChannelSet((AtSdhVc)self, pdhChannel);
        }

    if (Aug1RestoreIsInProgress(self))
        return ret;

    /* Initialize the internal DS1/E1 object */
    if (hwAccess)
        ret |= AtChannelInit((AtChannel)pdhChannel);

    return ret;
    }

static eBool MapTypeIsSupported(AtSdhChannel self, uint8 mapType)
    {
    AtUnused(self);

    if ((mapType == cAtSdhVcMapTypeVc1xMapC1x) ||
        (mapType == cAtSdhVcMapTypeVc1xMapDe1) ||
        (mapType == cAtSdhVcMapTypeNone))
        return cAtTrue;

    return cAtFalse;
    }

static uint8 HwMapTypeGet(AtSdhChannel self)
    {
    ThaModuleCdr cdrModule = (ThaModuleCdr)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cThaModuleCdr);
    return ThaCdrVtPldGet(cdrModule, self);
    }

static AtPdhChannel De1Create(ThaSdhVc1x self, AtModulePdh modulePdh)
    {
    return (AtPdhChannel)AtModulePdhVcDe1Create(modulePdh, (AtSdhChannel)self);
    }

static eBool PohInsEn(ThaSdhVc1x self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static ThaPohProcessor PohProcessorCreate(ThaSdhVc self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    ThaModulePoh pohModule = (ThaModulePoh)AtDeviceModuleGet(device, cThaModulePoh);

    return ThaModulePohVc1xPohProcessorCreate(pohModule, (AtSdhVc)self);
    }

static ThaCdrController CdrControllerCreate(ThaSdhVc self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    ThaModuleCdr cdrModule = (ThaModuleCdr)AtDeviceModuleGet(device, cThaModuleCdr);
    return ThaModuleCdrVc1xCdrControllerCreate(cdrModule, (AtSdhVc)self);
    }

static eAtRet VcPwCepSet(AtSdhChannel sdhChannel)
    {
    uint8 vtgId, vtId;
    uint32 enabledAlarm = 0;
    eBool enable=cAtFalse;

    /* Backup enabled alarms for restoring */
    if (AtSdhChannelAlarmAffectingIsEnabled(sdhChannel, cAtSdhPathAlarmUneq))
        enabledAlarm |= cAtSdhPathAlarmUneq;

    if (AtSdhChannelAlarmAffectingIsEnabled(sdhChannel, cAtSdhPathAlarmPlm))
        enabledAlarm |= cAtSdhPathAlarmPlm;

    if (AtSdhChannelAlarmAffectingIsEnabled(sdhChannel, cAtSdhPathAlarmTim))
        enabledAlarm |= cAtSdhPathAlarmTim;

    sdhChannel->enabledAlarmAffect = enabledAlarm;

    /* Need to disable POH insertion and disable POH alarm forwarding */
    vtgId = AtSdhChannelTug2Get(sdhChannel);
    vtId  = AtSdhChannelTu1xGet(sdhChannel);
    enable = mMethodsGet(mThis(sdhChannel))->PohInsEn(mThis(sdhChannel));
    ThaOcnVtPohInsertEnable(sdhChannel, AtSdhChannelSts1Get(sdhChannel), vtgId, vtId, enable);

    if (ThaModuleSdhPohIsReady((ThaModuleSdh)AtChannelModuleGet((AtChannel)sdhChannel)))
        return AtSdhChannelAlarmAffectingEnable(sdhChannel, cAtSdhPathAlarmUneq | cAtSdhPathAlarmPlm | cAtSdhPathAlarmTim, cAtFalse);

    return cAtOk;
    }

static eAtRet VcRestoreWhenCepUnbind(AtSdhChannel sdhChannel)
    {
    uint8 vtgId, vtId;

    vtgId = AtSdhChannelTug2Get(sdhChannel);
    vtId  = AtSdhChannelTu1xGet(sdhChannel);
    ThaOcnVtPohInsertEnable(sdhChannel, AtSdhChannelSts1Get(sdhChannel), vtgId, vtId, cAtTrue);

    if (ThaModuleSdhPohIsReady((ThaModuleSdh)AtChannelModuleGet((AtChannel)sdhChannel)))
        return AtSdhChannelAlarmAffectingEnable(sdhChannel, sdhChannel->enabledAlarmAffect, cAtTrue);

    return cAtOk;
    }

static eAtRet PdhPwBindingCtrlSet(AtSdhChannel self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(device, cAtModulePdh);
    eAtSdhChannelType vcType = AtSdhChannelTypeGet(self);
    eAtRet ret = cAtOk;

    if (vcType == cAtSdhChannelTypeVc11)
        {
        ret |= ThaPdhStsVtMapCtrlSet(pdhModule, self, cThaStsVtVc11BCepMapMd);
        ret |= ThaPdhStsVtDeMapCtrlSet(pdhModule, self, cThaStsVtVt15BCepDmapMd);
        }
    else
        {
        ret |= ThaPdhStsVtMapCtrlSet(pdhModule, self, cThaStsVtVc12BCepMapMd);
        ret |= ThaPdhStsVtDeMapCtrlSet(pdhModule, self, cThaStsVtVt2BCepDmapMd);
        }

    return ret;
    }

static eAtRet BindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    eAtRet ret = cAtOk;
    AtDevice device = AtChannelDeviceGet(self);
    AtSdhChannel sdhChannel = (AtSdhChannel)self;
    ThaModuleAbstractMap mapDemapModule;

    if (pseudowire && (AtPwTypeGet(pseudowire) != cAtPwTypeCEP))
        return cAtErrorModeNotSupport;

    /* If it is mappped DS1/E1, also do not allow binding to CE PW */
    if (AtSdhChannelMapTypeGet(sdhChannel) == cAtSdhVcMapTypeVc1xMapDe1)
        return cAtErrorChannelBusy;

    /* Let supper do first */
    m_AtChannelMethods->BindToPseudowire(self, pseudowire);

    /* In warm restore, don't need to configure HW */
    if (AtChannelDeviceInWarmRestore((AtChannel)self))
        return cAtOk;

    /* PDH MAP/DEMAP control */
    ret |= PdhPwBindingCtrlSet(sdhChannel);

    /* Frame mode and map signal type */
    mapDemapModule = (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap);
    ret |= ThaModuleAbstractMapVc1xFrameModeSet(mapDemapModule, (AtSdhVc)self, cMapFrameTypeCepBasic);

    /* Bind to pseudowire at MAP/DEMAP module */
    ret |= ThaModuleAbstractMapBindVc1xToPseudowire((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap)  , (AtSdhVc)self, pseudowire);
    ret |= ThaModuleAbstractMapBindVc1xToPseudowire((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap), (AtSdhVc)self, pseudowire);
    if (ret != cAtOk)
        return ret;

    /* Configure VC bind to pseudowire CEP */
    if (pseudowire)
        ret |= VcPwCepSet(sdhChannel);

    /* When bind vc to pseudowire, need to modify something to suit CEP application,
     * So when unbind, need to revert things which were previously modified
     */
    else
        ret |= VcRestoreWhenCepUnbind(sdhChannel);

    return ret;
    }

static uint8 NumBlocksNeedToBindToEncapChannel(AtChannel self, AtEncapChannel encapChannel)
    {
    eAtSdhChannelType sdhChannelType = AtSdhChannelTypeGet((AtSdhChannel)self);
	AtUnused(encapChannel);
    if (sdhChannelType == cAtSdhChannelTypeVc12)
        return 16;
    if (sdhChannelType == cAtSdhChannelTypeVc11)
        return 12;

    return 0;
    }

static eBool CanBindToEncapChannel(AtSdhChannel self)
    {
    eAtSdhChannelType sdhChannelType = AtSdhChannelTypeGet(self);

    if ((sdhChannelType == cAtSdhChannelTypeVc12) ||
        (sdhChannelType == cAtSdhChannelTypeVc11))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet SdhVcPdhBypass(AtChannel self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(AtChannelDeviceGet(self), cAtModulePdh);
    eAtRet ret = cAtOk;

    ret |= ThaPdhVcBypassMapMdSet((AtSdhVc)channel);
    ret |= ThaSdhVcPdhFrameBypass((AtSdhVc)channel);
    ret |= AtChannelTimingSet(self, cAtTimingModeSys, NULL);
    ThaPdhSts1M13Bypass(pdhModule, channel, AtSdhChannelSts1Get(channel));

    return ret;
    }

static AtEncapBinder EncapBinder(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return AtDeviceEncapBinder(device);
    }

static eAtRet BindToEncapChannel(AtChannel self, AtEncapChannel encapChannel)
    {
    eAtRet ret = cAtOk;

    if (!CanBindToEncapChannel((AtSdhChannel)self))
        return cAtErrorModeNotSupport;

    /* Make sure that this VC does not map DE1 */
    ret |= AtSdhChannelMapTypeSet((AtSdhChannel)self, cAtSdhVcMapTypeVc1xMapC1x);
    if (ret != cAtOk)
        return ret;

    /* Hardware binding */
    ret |= AtEncapBinderBindVc1xToEncapChannel(EncapBinder(self), self, encapChannel);

    if (encapChannel)
        ret |= SdhVcPdhBypass(self);

    /* Let super deal with database */
    ret |= m_AtChannelMethods->BindToEncapChannel(self, encapChannel);

    return ret;
    }

static uint32 EncapHwIdAllocate(AtChannel self)
    {
    return AtEncapBinderVc1xEncapHwIdAllocate(EncapBinder(self), (AtSdhVc)self);
    }

static uint8 PppBlockRange(AtChannel self)
    {
	AtUnused(self);
    return 2;
    }

static eAtRet EncapConnectionEnable(AtChannel self, eBool enable)
    {
    eAtRet ret = cAtOk;
    AtDevice device = AtChannelDeviceGet(self);

    ret |= ThaModuleAbstractMapVc1xEncapConnectionEnable((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap)  , (AtSdhVc)self, enable);
    ret |= ThaModuleAbstractMapVc1xEncapConnectionEnable((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap), (AtSdhVc)self, enable);

    return ret;
    }

static void De1CleanUp(ThaPdhDe1 de1)
    {
    AtClockExtractor clockExtractor;

    if (de1 == NULL)
        return;

    clockExtractor = ThaPdhDe1ClockExtractorGet(de1);
    if ((clockExtractor == NULL) || (AtClockExtractorPdhDe1Get(clockExtractor) != (AtPdhDe1)de1))
        return;

    AtClockExtractorSystemClockExtract(clockExtractor);
    }

static void Delete(AtObject self)
    {
    De1CleanUp((ThaPdhDe1)SdhVcPdhChannelGet((AtSdhVc)self));
    AttControllerDelete((ThaSdhVc1x)self);
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeNone(attController);
    }

static eAtRet TxTrafficEnable(AtChannel self, eBool enable)
    {
    AtDevice device = AtChannelDeviceGet(self);
    ThaModuleAbstractMap mapModule = (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap);
    return ThaModuleAbstractMapVc1xEncapConnectionEnable(mapModule, (AtSdhVc)self, enable);
    }

static eAtRet RxTrafficEnable(AtChannel self, eBool enable)
    {
    AtDevice device = AtChannelDeviceGet(self);
    ThaModuleAbstractMap mapModule = (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap);
    return ThaModuleAbstractMapVc1xEncapConnectionEnable(mapModule, (AtSdhVc)self, enable);
    }

static eBool TxTrafficIsEnabled(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    ThaModuleAbstractMap mapModule = (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap);
    return ThaModuleAbstractMapVc1xEncapConnectionIsEnabled(mapModule, (AtSdhVc)self);
    }

static eBool RxTrafficIsEnabled(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    ThaModuleAbstractMap mapModule = (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap);
    return ThaModuleAbstractMapVc1xEncapConnectionIsEnabled(mapModule, (AtSdhVc)self);
    }

static eBool ServiceIsRunning(AtChannel self)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)self;

    if (m_AtChannelMethods->ServiceIsRunning(self))
        return cAtTrue;

    if (AtSdhChannelMapTypeGet(sdhChannel) == cAtSdhVcMapTypeVc1xMapDe1)
        return AtChannelServiceIsRunning(AtSdhChannelMapChannelGet(sdhChannel));

    return cAtFalse;
    }

static uint8 MlpppBlockRange(AtChannel self)
    {
    static uint8 cInvalidRange = 0xFF;

    if ((AtSdhChannelTypeGet((AtSdhChannel)self) == cAtSdhChannelTypeVc12) ||
        (AtSdhChannelTypeGet((AtSdhChannel)self) == cAtSdhChannelTypeVc11))
        return 6;

    return cInvalidRange;
    }

static eAtRet WarmRestore(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    uint32 mapPwHwId;
    AtPw pw;
    AtModulePw modulePw;

    /* Let super restore first */
    eAtRet ret = m_AtChannelMethods->WarmRestore(self);
    if (ret != cAtOk)
        return ret;

    if (AtSdhChannelMapTypeGet((AtSdhChannel)self) == cAtSdhVcMapTypeVc1xMapDe1)
        return AtChannelWarmRestore((AtChannel)SdhVcPdhChannelGet((AtSdhVc)self));

    /* In case this VC1x map C1x, maybe it is bound pw, restore pw */
    mapPwHwId = ThaModuleAbstractMapVc1xBoundPwHwIdGet((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap), (AtSdhVc)self);
    if (mapPwHwId == cThaModulePwInvalidPwId)
        {
        AtChannelBoundPwSet(self, NULL);
        return cAtOk;
        }

    /* PW hw id is valid, restore pw */
    modulePw = (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);
    pw = (AtPw)AtModulePwCepCreate(modulePw, mapPwHwId, cAtPwCepModeBasic);
    ret = AtPwCircuitBind(pw, self);

    if (ret != cAtOk)
        {
        AtModulePwDeletePw(modulePw, mapPwHwId);
        return ret;
        }

    return AtChannelWarmRestore((AtChannel)pw);
    }

static ThaModuleAbstractMap ModuleMap(AtChannel self)
    {
    return (ThaModuleAbstractMap)AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModuleMap);
    }

static ThaModuleAbstractMap ModuleDemap(AtChannel self)
    {
    return (ThaModuleAbstractMap)AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModuleDemap);
    }

static uint32 BoundPwHwIdGet(AtChannel self)
    {
    return ThaModuleAbstractMapVc1xBoundPwHwIdGet(ModuleMap(self), (AtSdhVc)self);
    }

static uint32 BoundEncapHwIdGet(AtChannel self)
    {
    return AtEncapBinderVc1xBoundEncapHwIdGet(EncapBinder(self), self);
    }

static eAtRet TxEncapConnectionEnable(AtChannel self, eBool enable)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return ThaModuleAbstractMapVc1xEncapConnectionEnable((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap), (AtSdhVc)self, enable);
    }

static eBool TxEncapConnectionIsEnabled(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return ThaModuleAbstractMapVc1xEncapConnectionIsEnabled((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap), (AtSdhVc)self);
    }

static eAtRet RxEncapConnectionEnable(AtChannel self, eBool enable)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return ThaModuleAbstractMapVc1xEncapConnectionEnable((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap), (AtSdhVc)self, enable);
    }

static eBool RxEncapConnectionIsEnabled(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return ThaModuleAbstractMapVc1xEncapConnectionIsEnabled((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap), (AtSdhVc)self);
    }

static eAtClockState HwClockStateTranslate(AtChannel self, uint8 hwState)
    {
    ThaModuleCdr moduleCdr = (ThaModuleCdr)AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModuleCdr);

    return ThaModuleCdrClockStateFromLoChannelHwStateGet(moduleCdr, hwState);
    }

static eAtRet PohInsertionEnable(AtSdhPath self, eBool enabled)
    {
    AtSdhChannel channel = (AtSdhChannel)self;
    uint8 sts1 = AtSdhChannelSts1Get(channel);
    uint8 vtg = AtSdhChannelTug2Get(channel);
    uint8 vt  = AtSdhChannelTu1xGet(channel);
    ThaOcnVtPohInsertEnable(channel, sts1, vtg, vt, enabled);
    return cAtOk;
    }

static eBool PohInsertionIsEnabled(AtSdhPath self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;
    uint8 sts1 = AtSdhChannelSts1Get(channel);
    uint8 vtg = AtSdhChannelTug2Get(channel);
    uint8 vt  = AtSdhChannelTu1xGet(channel);
    return ThaOcnVtPohInsertIsEnabled(channel, sts1, vtg, vt);
    }

static ThaModuleConcate ModuleConcate(AtChannel self)
    {
    return (ThaModuleConcate)AtDeviceModuleGet(AtChannelDeviceGet(self), cAtModuleConcate);
    }

static eAtRet BindToSourceGroup(AtChannel self, AtConcateGroup group)
    {
    eAtRet ret = cAtOk;

    ret |= ThaModuleConcateBindVc1xToSourceConcateGroup(ModuleConcate(self), self, group);
    ret |= ThaModuleAbstractMapBindVc1xToConcateGroup(ModuleMap(self), (AtSdhVc)self, group);

    return ret;
    }

static eAtRet BindToSinkGroup(AtChannel self, AtConcateGroup group)
    {
    eAtRet ret = cAtOk;

    ret |= ThaModuleConcateBindVc1xToSinkConcateGroup(ModuleConcate(self), self, group);
    ret |= ThaModuleAbstractMapBindVc1xToConcateGroup(ModuleDemap(self), (AtSdhVc)self, group);

    return ret;
    }

static uint32 BoundConcateHwIdGet(AtChannel self)
    {
    return ThaModuleAbstractMapVc1xBoundConcateHwIdGet(ModuleMap(self), (AtSdhVc)self);
    }

static void OverrideAtSdhPath(ThaSdhVc1x self)
    {
    AtSdhPath path = (AtSdhPath)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhPathOverride, mMethodsGet(path), sizeof(m_AtSdhPathOverride));
        mMethodOverride(m_AtSdhPathOverride, PohInsertionEnable);
        mMethodOverride(m_AtSdhPathOverride, PohInsertionIsEnabled);
        }

    mMethodsSet(path, &m_AtSdhPathOverride);
    }

static void OverrideAtObject(ThaSdhVc1x self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(ThaSdhVc1x self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, BindToEncapChannel);
        mMethodOverride(m_AtChannelOverride, NumBlocksNeedToBindToEncapChannel);
        mMethodOverride(m_AtChannelOverride, PppBlockRange);
        mMethodOverride(m_AtChannelOverride, BindToPseudowire);
        mMethodOverride(m_AtChannelOverride, EncapConnectionEnable);
        mMethodOverride(m_AtChannelOverride, RxTrafficEnable);
        mMethodOverride(m_AtChannelOverride, TxTrafficEnable);
        mMethodOverride(m_AtChannelOverride, RxTrafficIsEnabled);
        mMethodOverride(m_AtChannelOverride, TxTrafficIsEnabled);
        mMethodOverride(m_AtChannelOverride, MlpppBlockRange);
        mMethodOverride(m_AtChannelOverride, WarmRestore);
        mMethodOverride(m_AtChannelOverride, EncapHwIdAllocate);
        mMethodOverride(m_AtChannelOverride, BoundPwHwIdGet);
        mMethodOverride(m_AtChannelOverride, BoundEncapHwIdGet);
        mMethodOverride(m_AtChannelOverride, ServiceIsRunning);
        mMethodOverride(m_AtChannelOverride, RxEncapConnectionIsEnabled);
        mMethodOverride(m_AtChannelOverride, RxEncapConnectionEnable);
        mMethodOverride(m_AtChannelOverride, TxEncapConnectionIsEnabled);
        mMethodOverride(m_AtChannelOverride, TxEncapConnectionEnable);
        mMethodOverride(m_AtChannelOverride, AttController);
        mMethodOverride(m_AtChannelOverride, HwClockStateTranslate);
        mMethodOverride(m_AtChannelOverride, BindToSourceGroup);
        mMethodOverride(m_AtChannelOverride, BindToSinkGroup);
        mMethodOverride(m_AtChannelOverride, BoundConcateHwIdGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideThaSdhVc(ThaSdhVc1x self)
    {
    ThaSdhVc thaVc = (ThaSdhVc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhVcOverride, mMethodsGet(thaVc), sizeof(m_ThaSdhVcOverride));
        mMethodOverride(m_ThaSdhVcOverride, PohProcessorCreate);
        mMethodOverride(m_ThaSdhVcOverride, CdrControllerCreate);
        mMethodOverride(m_ThaSdhVcOverride, OcnDefaultOffset);
        }

    mMethodsSet(thaVc, &m_ThaSdhVcOverride);
    }

static void OverrideAtSdhChannel(ThaSdhVc1x self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(m_AtSdhChannelOverride));
        mMethodOverride(m_AtSdhChannelOverride, MapTypeSet);
        mMethodOverride(m_AtSdhChannelOverride, MapChannelGet);
        mMethodOverride(m_AtSdhChannelOverride, HwMapTypeGet);
        mMethodOverride(m_AtSdhChannelOverride, MapTypeIsSupported);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void Override(ThaSdhVc1x self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtSdhPath(self);
    OverrideThaSdhVc(self);
    OverrideAtSdhChannel(self);
    }

static void MethodsInit(AtSdhVc self)
    {
    ThaSdhVc1x vc1x = (ThaSdhVc1x)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Initialize method */
        mMethodOverride(m_methods, De1Create);
        mMethodOverride(m_methods, PohInsEn);
        }

    mMethodsSet(vc1x, &m_methods);
    }

AtSdhVc ThaSdhVc1xObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaSdhVc1x));

    /* Super constructor */
    if (ThaSdhVcObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Override */
    Override((ThaSdhVc1x)self);

    /* Setup class */
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhVc ThaSdhVc1xNew(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhVc newVc = mMethodsGet(osal)->MemAlloc(osal, sizeof(tThaSdhVc1x));
    if (newVc == NULL)
        return NULL;

    /* Construct it */
    return ThaSdhVc1xObjectInit(newVc, channelId, channelType, module);
    }

uint8 ThaSdhVc1xPdhVcMapMode(AtSdhChannel self, uint8 mapType)
    {
    if (self)
        return PdhVcMapMode(self, mapType);
    return cInvalidUint8;
    }

uint8 ThaSdhVc1xMapTypeOfMapModule(AtSdhChannel self, uint8 mapType)
    {
    if (self)
        return MapTypeOfMapModule(self, mapType);
    return cInvalidUint8;
    }

eAtRet ThaSdhVc1xPdhPwBindingCtrlSet(AtSdhChannel self)
    {
    if (self)
        return PdhPwBindingCtrlSet(self);
    return cAtErrorObjectNotExist;
    }

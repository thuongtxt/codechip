/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : ThaSdhAuVc.c
 *
 * Created Date: Oct 19, 2012
 *
 * Description : Thalassa AU VC
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../generic/sur/AtSurEngineInternal.h"
#include "../att/ThaAttController.h"
#include "../../../generic/pdh/AtPdhSerialLineInternal.h"
#include "../../../util/coder/AtCoderUtil.h"
#include "ThaSdhVcInternal.h"
#include "ThaModuleSdh.h"
#include "ThaModuleSdhInternal.h"
#include "ThaModuleSdhReg.h"
#include "ThaSdhVc.h"

#include "../pdh/ThaPdhDe3Internal.h"
#include "../pdh/ThaModulePdhInternal.h"
#include "../map/ThaModuleAbstractMap.h"
#include "../pdh/ThaStmModulePdh.h"
#include "../cla/pw/ThaModuleClaPwInternal.h"
#include "../cdr/ThaModuleCdr.h"
#include "../cdr/ThaModuleCdrStm.h"
#include "../pdh/ThaStmModulePdh.h"
#include "../ocn/ThaModuleOcn.h"
#include "../prbs/ThaPrbsEngine.h"
#include "../pw/ThaModulePw.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaOcnNotCare         0
#define cThaOcnNumStsInAu4     3
#define cThaOcnNumStsInAu4_4c  12
#define cThaOcnNumStsInAu4_16c 48
#define cThaOcnNumStsInAu4_64c 192
#define cMaxNumStsOfLine       12

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaSdhAuVc)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaSdhAuVcMethods   m_methods;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tThaSdhVcMethods     m_ThaSdhVcOverride;
static tAtSdhChannelMethods m_AtSdhChannelOverride;
static tAtChannelMethods    m_AtChannelOverride;
static tAtSdhPathMethods    m_AtSdhPathOverride;
static tAtSdhVcMethods      m_AtSdhVcOverride;
static tAtObjectMethods    m_AtObjectOverride;

/* To save super implementation */
static const tAtObjectMethods     *m_AtObjectMethods     = NULL;
static const tAtSdhChannelMethods *m_AtSdhChannelMethods = NULL;
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;
static const tAtSdhVcMethods      *m_AtSdhVcMethods      = NULL;

/*--------------------------- Forward declarations ---------------------------*/
extern eAtRet ThaSdhVcPdhFrameBypass(AtSdhVc self);
extern eAtRet ThaSdhAuTxSsSet(AtSdhPath sdhChannel, uint8 value, eBool isChannelized);
static uint8 Vc3PldTypeGet(ThaSdhAuVc sdhVc);

/*--------------------------- Implementation ---------------------------------*/
static void AttControllerDelete(AtObject self)
    {
    if (mThis(self)->attController)
        AtObjectDelete((AtObject)mThis(self)->attController);
    mThis(self)->attController = NULL;
    }

static void Vc4xStsIdListGet(AtSdhChannel self, uint8 *pStsLst)
    {
    uint8 startSts = AtSdhChannelSts1Get(self);
    uint8 i;

    for (i = 0; i < AtSdhChannelNumSts(self); i++)
        pStsLst[i] = (uint8)(startSts + i);
    }

static void Delete(AtObject self)
    {
    AttControllerDelete(self);
    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static AtObjectAny AttController(AtChannel self)
    {
    ThaModuleSdh module = (ThaModuleSdh)AtChannelModuleGet(self);
    if (mThis(self)->attController == NULL)
        {
        mThis(self)->attController = mMethodsGet(module)->PathAttControllerCreate(module, self);
        if (mThis(self)->attController)
            {
            AtAttControllerSetUp(mThis(self)->attController);
            AtAttControllerInit(mThis(self)->attController);
            }
        }
    return mThis(self)->attController;
    }

static eAtRet Vc4xTxEnable(AtSdhChannel self, eBool vc4En)
    {
    return ThaModuleOcnVc4xTxEnable(self, vc4En);
    }

static eAtRet Vc4xPohInsertEnableSet(AtSdhChannel self, uint8* stsIds)
    {
    uint8 i, enable;
    eAtRet ret = cAtOk;

    for (i = 0; i < AtSdhChannelNumSts(self); i++)
        {
        enable = (i == 0) ? 1 : 0; /* First sts is master, enable it, disable for slaves */
        ret |= ThaOcnStsPohInsertEnable(self, stsIds[i], enable);
        }

    return ret;
    }

static eAtRet PohInsertDisable(AtSdhChannel self)
    {
    uint8 i;
    eAtRet ret = cAtOk;
    uint8 startSts = AtSdhChannelSts1Get(self);

    for (i = 0; i < AtSdhChannelNumSts(self); i++)
        ret |= ThaOcnStsPohInsertEnable(self, (uint8)(startSts + i), cAtFalse);

    return ret;
    }

static eBool SdhChannelTypeIsVc4x(AtSdhChannel vc)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(vc);
    if ((channelType == cAtSdhChannelTypeVc4) ||
        (channelType == cAtSdhChannelTypeVc4_4c) ||
        (channelType == cAtSdhChannelTypeVc4_16c)||
        (channelType == cAtSdhChannelTypeVc4_64c))
        return cAtTrue;
    return cAtFalse;
    }

static ThaModuleOcn ModuleOcn(AtSdhChannel sdhVc)
    {
    return (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)sdhVc), cThaModuleOcn);
    }

static eAtRet HoVc4xPayloadDefault(AtSdhChannel sdhVc)
    {
    if (!SdhChannelTypeIsVc4x(sdhVc))
        return cAtOk;

    return ThaOcnVc4xPayloadDefault(ModuleOcn(sdhVc), sdhVc);
    }

static uint8 Vc4_4cHwMapTypeGet(AtSdhChannel self)
    {
    AtUnused(self);
    return cAtSdhVcMapTypeVc4_4cMapC4_4c;
    }

static uint8 Vc4_16cHwMapTypeGet(AtSdhChannel self)
    {
    AtUnused(self);
    return cAtSdhVcMapTypeVc4_16cMapC4_16c;
    }

static uint8 Vc4_64cHwMapTypeGet(AtSdhChannel self)
    {
    AtUnused(self);
    return cAtSdhVcMapTypeVc4_64cMapC4_64c;
    }

static ThaModuleAbstractMap AbstractMapModule(AtSdhChannel self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap);
    }

static eAtRet Vc4xPldSet(ThaSdhAuVc self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return ThaCdrVC4xPldSet((ThaModuleCdr)AtDeviceModuleGet(device, cThaModuleCdr), (AtSdhChannel)self);
    }

static eAtRet Vc4xMapTypeSet(AtSdhChannel self, eAtSdhVcMapType vc4xMapType)
    {
    eAtRet ret;
    uint8 stsIds[cThaOcnNumStsInAu4_64c];
    AtSdhPath path = (AtSdhPath)self;

    if (AtChannelDeviceInWarmRestore((AtChannel)self))
        return cAtOk;

    /* Get all STS in one AU-4-x */
    Vc4xStsIdListGet(self, stsIds);

    ret  = Vc4xPohInsertEnableSet(self, stsIds);
    ret |= ThaSdhAuVcPgStsSlaverIndicate(self, vc4xMapType);
    ret |= Vc4xTxEnable(self, cAtTrue);

    /* Default Timing mode is Internal */
    if (!ThaSdhVcIsInPwBindingProgress((ThaSdhVc)self))
        ret |= AtChannelTimingSet((AtChannel)self, cAtTimingModeSys, (AtChannel)self);

    /* Set default sigType mapping */
    ret |= mMethodsGet(mThis(self))->MapFrameModeDefaultSet(mThis(self));

    /* Need to configure CDR */
    ret |= mMethodsGet(mThis(self))->Vc4xPldSet(mThis(self));

    /* Set default C2 */
    if (AtSdhPathHasPohProcessor(path) && (!ThaSdhVcIsInPwBindingProgress((ThaSdhVc)self)))
        {
        ret |= AtSdhPathTxPslSet(path, 0x1);
        ret |= AtSdhPathExpectedPslSet(path, 0x1);
        }

    ret |= AtSdhAuDefaultSsSet((AtSdhPath)self);

    return ret;
    }

static AtModulePdh ModulePdh(AtSdhChannel self)
    {
    return (AtModulePdh)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cAtModulePdh);
    }

static eAtRet SdhVcPdhBypass(AtChannel self)
    {
    AtSdhChannel sdhVc = (AtSdhChannel)self;
    AtModulePdh pdhModule = ModulePdh(sdhVc);
    uint8 sts_i;
    eAtRet ret = cAtOk;

    if (!mMethodsGet(mThis(self))->NeedConfigurePdh(mThis(self)))
        return cAtOk;

    for (sts_i = 0; sts_i < AtSdhChannelNumSts(sdhVc); sts_i++)
        {
        uint8 sts1Id = (uint8)(AtSdhChannelSts1Get(sdhVc) + sts_i);

        ret |= ThaModuleOcnRxStsTermEnable(sdhVc, sts1Id, cAtFalse);
        ret |= ThaPdhVcBypassMapMdSet((AtSdhVc)sdhVc);
        ThaPdhSts1M13Bypass(pdhModule, sdhVc, sts1Id);
        }

    ret |= HoVc4xPayloadDefault(sdhVc);
    ret |= ThaSdhVcPdhFrameBypass((AtSdhVc)sdhVc);
    ret |= AtChannelTimingSet(self, cAtTimingModeSys, NULL);

    return  ret;
    }

static uint8 Vc4HwMapTypeGet(AtSdhChannel self)
    {
    uint8 stsPldType = ThaOcnStsRxPayloadTypeGet(self, AtSdhChannelSts1Get(self));

    if (stsPldType == cThaStsDontCareVtTu)
        return cAtSdhVcMapTypeVc4MapC4;

    if ((stsPldType == cThaStsTug3ContainTu3) ||
        (stsPldType == cThaStsTug3ContainVt1x))
        return cAtSdhVcMapTypeVc4Map3xTug3s;

    return cAtSdhVcMapTypeNone;
    }

static eAtSdhTugMapType DefaultTug3MapType(ThaSdhAuVc self)
    {
    AtUnused(self);
    return cAtSdhTugMapTypeTug3MapVc3;
    }
static eAtRet Vc4MapTypeSet(AtSdhChannel self, eAtSdhVcMapType vc4MapType)
    {
    eAtRet ret;
    uint8 stsIds[cThaOcnNumStsInAu4];
    uint8 subChannel_i;
    eBool surEnabled;
    AtSurEngine surEngine;
    AtSdhPath au4;

    if (AtChannelDeviceInWarmRestore((AtChannel)self))
        return cAtOk;

    /* Get 3 STS in one AU-4 */
    Vc4xStsIdListGet(self, stsIds);

    ret  = Vc4xPohInsertEnableSet(self, stsIds);
    ret |= ThaSdhAuVcPgStsSlaverIndicate(self, vc4MapType);
    ret |= Vc4xTxEnable(self, cAtTrue);

    /* Default Timing mode is Internal */
    ret |= AtChannelTimingSet((AtChannel)self, cAtTimingModeSys, (AtChannel)self);

	/* The SS bit should also be reconfigured when mapping change */
    au4 = (AtSdhPath)AtSdhChannelParentChannelGet(self);
    if (AtSdhPathHasPointerProcessor(au4))
    AtSdhPathTxSsSet(au4, AtSdhPathTxSsGet(au4));

    /* Set default sigType mapping */
    if (vc4MapType == cAtSdhVcMapTypeVc4MapC4)
        ret |= mMethodsGet(mThis(self))->MapFrameModeDefaultSet(mThis(self));

    /* Need to configure CDR */
    if (vc4MapType != cAtSdhVcMapTypeVc4Map3xTug3s)
        {
		ret |= SdhVcPdhBypass((AtChannel)self);
		ret |= mMethodsGet(mThis(self))->Vc4xPldSet(mThis(self));
        ret |= ThaModuleOcnRxStsTermEnable(self, AtSdhChannelSts1Get(self), cAtFalse);

        /* For none mapping, need to reset concatenation and disable POH function.
         * If not, bus loop back will not work and if remote side is mapping VC1x,
         * there would be TU-LOP or TU-AIS */
        if (vc4MapType == cAtSdhVcMapTypeNone)
            {
            ret |= HoVc4xPayloadDefault(self);
            ret |= PohInsertDisable(self);
            }

        if (vc4MapType == cAtSdhVcMapTypeVc4MapC4)
            {
            /* Set default C2 */
            if (AtSdhPathHasPohProcessor((AtSdhPath)self))
                {
                ret |= AtSdhPathTxPslSet((AtSdhPath)self, 0x1);
                ret |= AtSdhPathExpectedPslSet((AtSdhPath)self, 0x1);
                }
            }

        return ret;
        }

    /* Set default C2 */
    if (AtSdhPathHasPohProcessor((AtSdhPath)self))
    	{
        ret |= AtSdhPathTxPslSet((AtSdhPath)self, 0x2);
        ret |= AtSdhPathExpectedPslSet((AtSdhPath)self, 0x2);
	    }

    /* Set default mapping for all of sub channels */
    for (subChannel_i = 0; subChannel_i < AtSdhChannelNumberOfSubChannelsGet(self); subChannel_i++)
        {
        AtSdhChannel subChannel = AtSdhChannelSubChannelGet(self, subChannel_i);
        if (subChannel)
            ret |= mMethodsGet(subChannel)->MapTypeSet(subChannel, mMethodsGet(mThis(self))->DefaultTug3MapType(mThis(self)));
        }

    surEngine = AtChannelSurEngineGet((AtChannel)self);
    if  (AtSurEngineIsProvisioned(surEngine) == cAtFalse)
        return ret;

    /* When change mapping type, need to de-provision and provision again */
    surEnabled = AtSurEngineIsEnabled(surEngine);
    ret |= AtSurEngineProvision(surEngine, cAtFalse);
    ret |= AtSurEngineProvision(surEngine, cAtTrue);
    ret |= AtSurEngineEnable(surEngine, surEnabled);

    return ret;
    }

static AtPdhDe3 Vc3CreateDe3(AtSdhChannel self)
    {
    AtModulePdh modulePdh = ModulePdh(self);
    AtPdhDe3 de3, newDe3;
    eAtRet ret;

    /* Try to get DS3/E3 object. If none is found, create DS3/E3 itself */
    de3 = (AtPdhDe3)ThaModulePdhVcDe3ObjectGet((ThaModulePdh)modulePdh, self);
    if (de3 == NULL)
        newDe3 = AtModulePdhVcDe3Create(modulePdh, self);
    else
        newDe3 = de3;

    if (newDe3 == NULL)
        return NULL;

    AtPdhChannelVcSet((AtPdhChannel)newDe3, (AtSdhVc)self);
    SdhVcPdhChannelSet((AtSdhVc)self, (AtPdhChannel)newDe3);

    /* DS3/E3 is managed by module PDH, we do not need to re-initialize it.
     * It may be configured something by application. */
    if (de3)
        return newDe3;

    ret = AtChannelInit((AtChannel)newDe3);
    if (ret != cAtOk)
        return NULL;

    return newDe3;
    }

static uint8 Vc3HwMapTypeGet(AtSdhChannel self)
    {
    eThaOcnVc3PldType vc3PldType = ThaOcnVc3PldGet(self);
    if (vc3PldType == cThaOcnVc3Pld7Tug2)
        return cAtSdhVcMapTypeVc3Map7xTug2s;

    if (vc3PldType == cThaOcnVc3PldC3)
        return cAtSdhVcMapTypeVc3MapC3;

    if (vc3PldType == cThaOcnVc3PldDs3 || vc3PldType == cThaOcnVc3PldE3)
        return cAtSdhVcMapTypeVc3MapDe3;

    return cAtSdhVcMapTypeNone;
    }

static eAtRet MapFrameModeDefaultSet(ThaSdhAuVc self)
    {
    return ThaModuleAbstractMapAuVcFrameModeSet(AbstractMapModule((AtSdhChannel)self), (AtSdhVc)self);
    }

static eAtRet PdhDe3HwFrameSet(AtPdhChannel de3)
    {
    eAtRet ret = cAtOk;
    eAtTimingMode timingMode = AtChannelTimingModeGet((AtChannel)de3);
    AtChannel timingSource = AtChannelTimingSourceGet((AtChannel)de3);

    ret |= ThaPdhDe3HwFrameTypeSet((ThaPdhDe3)de3, AtPdhChannelFrameTypeGet(de3));
    ret |= ThaPdhDe3MapSet(de3);

    /* Reconfigure DS3/E3 hardware timing */
    ret |= AtChannelTimingSet((AtChannel)de3, timingMode, timingSource);

    return ret;
    }

static eAtRet Vc3MapDe3LiuEnable(AtSdhChannel self, eBool enable)
    {
    eAtRet ret = cAtOk;
    AtModulePdh modulePdh = ModulePdh(self);
    ThaPdhDe3 de3 = (ThaPdhDe3)SdhVcPdhChannelGet((AtSdhVc)self);

    if (!ThaStmModulePdhLiuDe3OverVc3IsSupported((ThaStmModulePdh)modulePdh))
        return cAtOk;

    /* Control LIU De3 Mapping to SONET */
    ret |= ThaPdhStsDe3LiuMapVc3Enable((ThaStmModulePdh)modulePdh, (AtSdhChannel)self, enable);
    ret |= ThaSdhAuVcMapDe3LiuEnable((ThaSdhAuVc)self, enable);
    ret |= ThaPdhDe3FramingBypass(de3, enable);

    if (enable)
        {
        ret |= ThaPdhDe3WorkingSideSet(de3, cThaPdhDe3WorkingSideSdh);
        ret |= AtPdhChannelAlarmAffectingDisable((AtPdhChannel)de3);
        ret |= ThaPdhDe3MappingDisable(de3);
        }
    else
        {
        ret |= ThaPdhDe3WorkingSideSet(de3, cThaPdhDe3WorkingSideLiu);
        ret |= AtPdhChannelAlarmAffectingRestore((AtPdhChannel)de3);
        ret |= ThaPdhDe3MappingRestore(de3);
        ret |= PdhDe3HwFrameSet((AtPdhChannel)de3);
        }

    return ret;
    }

static eBool Vc3IsCreatingDe3(AtSdhChannel self)
    {
    ThaModulePdh modulePdh = (ThaModulePdh)ModulePdh(self);
    return ThaModulePdhVcDe3ObjectGet(modulePdh, self) ? cAtFalse : cAtTrue;
    }

static eAtRet Vc3MapDe3(AtSdhChannel self)
    {
    eAtRet ret = cAtOk;
    AtPdhChannel de3 = SdhVcPdhChannelGet((AtSdhVc)self);

    PdhDe3SdhParentMappedTypeSet(de3);

    /* STM VC-3 mapping DS3/E3*/
    if (Vc3IsCreatingDe3(self))
        return ThaOcnVc3PldSet(self, Vc3PldTypeGet((ThaSdhAuVc)self));

    /* EC1 VC-3 mapping a standalone DS3/E3 (managed by module PDH) */
    if (!AtPdhChannelHasLineLayer(de3))
        {
        ret |= ThaOcnVc3PldSet(self, Vc3PldTypeGet((ThaSdhAuVc)self));
        ret |= PdhDe3HwFrameSet(de3);
        return ret;
        }

    /* CEP VC-3 mapping DS3/E3 line */
    ret |= Vc3MapDe3LiuEnable(self, cAtTrue);
    ret |= ThaOcnVc3PldSet(self, cThaOcnVc3PldC3);
    ret |= mMethodsGet(mThis(self))->MapFrameModeDefaultSet(mThis(self));

    return ret;
    }

static eAtRet Vc3DeleteDe3(AtSdhChannel self)
    {
    eAtRet ret = cAtOk;

    /* If CEP VC-3 is mapping DS3/E3 line, need to disable VC-3 mapping. */
    if (!Vc3IsCreatingDe3(self) && ThaSdhAuVcMapDe3LiuIsEnabled((ThaSdhAuVc)self))
        ret |= Vc3MapDe3LiuEnable(self, cAtFalse);

    ThaSdhVcPdhChannelDelete((AtSdhVc)self);

    return ret;
    }

static eBool Vc3IsOverSdhLine(AtSdhChannel self)
    {
    AtPdhChannel de3 = ThaModulePdhVcDe3ObjectGet((ThaModulePdh)ModulePdh(self), self);

    /* VC-3 over STM-x. */
    if (de3 == NULL)
        return cAtTrue;

    if (AtPdhChannelHasLineLayer(de3))
        return cAtFalse;

    /* VC-3 over EC1. */
    return cAtTrue;
    }

static eAtRet Vc3HwMapTypeSet(AtSdhChannel self, eAtSdhVcMapType vc3MapType)
    {
    eAtRet ret = cAtOk;
    eThaOcnVc3PldType ocnPldType = ThaSdhVcOcnVc3PldType(self, vc3MapType);

    if (vc3MapType == cAtSdhVcMapTypeNone)
        {
        if (Vc3IsOverSdhLine(self))
            ret |= ThaOcnVc3PldSet(self, ocnPldType);

        /* Delete PDH mapping channel */
        ret |= Vc3DeleteDe3(self);

        return ret;
        }

    if (vc3MapType == cAtSdhVcMapTypeVc3MapDe3)
        return Vc3MapDe3(self);

    if (vc3MapType == cAtSdhVcMapTypeVc3Map7xTug2s)
        {
        /* Set bypass at PDH */
        ret |= SdhVcPdhBypass((AtChannel)self);
        ret |= ThaOcnVc3PldSet(self, ocnPldType);
        }

    if (vc3MapType == cAtSdhVcMapTypeVc3MapC3)
        {
        if (Vc3IsOverSdhLine(self))
            {
            /* Set bypass at PDH */
            ret |= SdhVcPdhBypass((AtChannel)self);
            ret |= ThaOcnVc3PldSet(self, cThaOcnVc3PldC3);

            /* Set default sigType mapping */
            ret |= mMethodsGet(mThis(self))->MapFrameModeDefaultSet(mThis(self));
            }
        }

    /* Delete PDH mapping channel */
    ret |= Vc3DeleteDe3(self);

    return ret;
    }

static eAtRet Vc3MapTypeSet(AtSdhChannel self, eAtSdhVcMapType vc3MapType)
    {
    eAtRet ret = cAtOk;
    AtSdhPath au3;
    eBool isSimulated;

    if (AtChannelDeviceInWarmRestore((AtChannel)self))
        return cAtOk;

    /* Disable VC4 mode */
    ret |= Vc4xTxEnable(self, cAtFalse);

    /* Default Timing mode is Internal */
    ret |= AtChannelTimingSet((AtChannel)self, cAtTimingModeSys, NULL);

    /* Check if this mapping is None, just need to disable POH at this layer */
    if (vc3MapType == cAtSdhVcMapTypeNone)
        {
        ret |= Vc3HwMapTypeSet(self, vc3MapType);

        if (AtSdhPathHasPohProcessor((AtSdhPath)self))
            ret |= AtSdhPathPohInsertionEnable((AtSdhPath)self, cAtFalse);

        return ret;
        }

    /* Need to enable POH insert in other mapping */
    if (AtSdhPathHasPohProcessor((AtSdhPath)self))
        ret |= AtSdhPathPohInsertionEnable((AtSdhPath)self, cAtTrue);

    /* Set default SS bit again because register for configure SS bit depends on VC mapping type */
    if (AtSdhPathHasPointerProcessor((AtSdhPath)self))
    AtSdhAuDefaultSsSet((AtSdhPath)self);

    /* Reconfigure SS bit when mapping change */
    au3 = (AtSdhPath)AtSdhChannelParentChannelGet(self);
    if (AtSdhPathHasPointerProcessor((AtSdhPath)au3))
    AtSdhPathTxSsSet(au3, AtSdhPathTxSsGet(au3));

    /* TUG-2 mapping */
    if (vc3MapType == cAtSdhVcMapTypeVc3Map7xTug2s)
        {
        AtDevice device = AtChannelDeviceGet((AtChannel)self);
        ThaModulePoh pohModule = (ThaModulePoh)AtDeviceModuleGet(device, cThaModulePoh);

        if (AtSdhPathHasPohProcessor((AtSdhPath)self))
            {
            AtSdhPathPohMonitorEnable((AtSdhPath)self, cAtFalse);
            ThaModulePohAllVtsPohMonEnable(pohModule, (AtSdhVc)self, cAtFalse);
            }

        ret |= Vc3HwMapTypeSet(self, vc3MapType);

        /* Set default C-2 */
        if (AtSdhPathHasPohProcessor((AtSdhPath)self))
            {
            ret |= AtSdhPathTxPslSet((AtSdhPath)self, 0x2);
            ret |= AtSdhPathExpectedPslSet((AtSdhPath)self, 0x2);
            }

        /* Give hardware a moment to search frame */
        if (!AtDeviceIsSimulated(AtChannelDeviceGet((AtChannel)self)))
            AtOsalUSleep(1000);

        if (AtSdhPathHasPohProcessor((AtSdhPath)self))
            AtSdhPathPohMonitorEnable((AtSdhPath)self, cAtTrue);

        return ret;
        }

    /* VC-3 mapping */
    isSimulated = AtDeviceIsSimulated(AtChannelDeviceGet((AtChannel)self));
    if (vc3MapType == cAtSdhVcMapTypeVc3MapC3)
        {
        if (AtSdhPathHasPohProcessor((AtSdhPath)self))
            AtSdhPathPohMonitorEnable((AtSdhPath)self, cAtFalse);

        ret |= Vc3HwMapTypeSet(self, vc3MapType);

        /* Set default C-2 */
        if (AtSdhPathHasPohProcessor((AtSdhPath)self))
            {
            ret |= AtSdhPathTxPslSet((AtSdhPath)self, 0x2);
            ret |= AtSdhPathExpectedPslSet((AtSdhPath)self, 0x2);
            }

        /* Give hardware a moment to search frame */
        if (!isSimulated)
            AtOsalUSleep(1000);

        if (AtSdhPathHasPohProcessor((AtSdhPath)self))
            AtSdhPathPohMonitorEnable((AtSdhPath)self, cAtTrue);

        return ret;
        }

    /* For DS3/E3 mapping */
    if (vc3MapType == cAtSdhVcMapTypeVc3MapDe3)
        {
        if (AtSdhPathHasPohProcessor((AtSdhPath)self))
            AtSdhPathPohMonitorEnable((AtSdhPath)self, cAtFalse);

        /* Set default C-2 */
        if (AtSdhPathHasPohProcessor((AtSdhPath)self))
            {
            ret |= AtSdhPathTxPslSet((AtSdhPath)self, 0x4);
            ret |= AtSdhPathExpectedPslSet((AtSdhPath)self, 0x4);
            }

        /* Create internal DE3 object */
        if (SdhVcPdhChannelGet((AtSdhVc)self) == NULL)
            {
            if (Vc3CreateDe3(self) == NULL)
                return cAtErrorRsrcNoAvail;
            }

        /* If still NULL */
        if (SdhVcPdhChannelGet((AtSdhVc)self) == NULL)
            return cAtErrorRsrcNoAvail;

        ret |= Vc3HwMapTypeSet(self, vc3MapType);

        /* Give hardware a moment to search frame */
        if (!isSimulated)
            AtOsalUSleep(1000);

        if (AtSdhPathHasPohProcessor((AtSdhPath)self))
        AtSdhPathPohMonitorEnable((AtSdhPath)self, cAtTrue);

        return ret;
        }

    return cAtErrorInvlParm;
    }

static ThaPohProcessor PohProcessorCreate(ThaSdhVc self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    ThaModulePoh pohModule = (ThaModulePoh)AtDeviceModuleGet(device, cThaModulePoh);

    return ThaModulePohAuVcPohProcessorCreate(pohModule, (AtSdhVc)self);
    }

static eAtModuleSdhRet MapTypeSet(AtSdhChannel self, uint8 mapType)
    {
    eAtSdhChannelType vcType;
    eAtRet ret;

    /* Call super implementation */
    ret = m_AtSdhChannelMethods->MapTypeSet(self, mapType);
    if (ret != cAtOk)
        return ret;

    /* Only VC-4, VC4-4c and VC-3 are mappable */
    vcType = AtSdhChannelTypeGet(self);

    if ((vcType == cAtSdhChannelTypeVc4_4c)  ||
        (vcType == cAtSdhChannelTypeVc4_16c) ||
        (vcType == cAtSdhChannelTypeVc4_64c) ||
        (vcType == cAtSdhChannelTypeVc4_nc)  ||
        (vcType == cAtSdhChannelTypeVc4_16nc))
        return Vc4xMapTypeSet(self, mapType);
    if (vcType == cAtSdhChannelTypeVc4)
        return Vc4MapTypeSet(self, mapType);
    if (vcType == cAtSdhChannelTypeVc3)
        return Vc3MapTypeSet(self, mapType);

    /* Just do nothing for other mapping mode because a corresponding C is
     * mapped to it */
    return cAtOk;
    }

static ThaCdrController CdrControllerCreate(ThaSdhVc self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    ThaModuleCdr cdrModule = (ThaModuleCdr)AtDeviceModuleGet(device, cThaModuleCdr);
    return ThaModuleCdrHoVcCdrControllerCreate(cdrModule, (AtSdhVc)self);
    }

static eThaPdhStsVtMapCtrl Vc3FractionalMapControl(AtSdhChannel self)
    {
    AtSdhChannel tug2 = AtSdhChannelSubChannelGet(self, 0);
    if (AtSdhChannelMapTypeGet(tug2) == cAtSdhTugMapTypeTug2Map3xTu12s)
        return cThaStsVtVc3FracCepVt2MapMd;

    return cThaStsVtVc3FracCepVt15MapMd;
    }

static eThaPdhStsVtMapCtrl Vc4FractionalMapControl(AtSdhChannel self)
    {
    AtSdhChannel tug = AtSdhChannelSubChannelGet(self, 0);
    if (AtSdhChannelMapTypeGet(tug) == cAtSdhTugMapTypeTug3MapVc3)
        return cThaStsVtVc4FracCepE3MapMd;

    /* Tug3 map 7xtug2 */
    tug = AtSdhChannelSubChannelGet(tug, 0);
    if (AtSdhChannelMapTypeGet(tug) == cAtSdhTugMapTypeTug2Map3xTu12s)
        return cThaStsVtVc4FracCepVt2MapMd;

    return cThaStsVtVc4FracCepVt15MapMd;
    }

static eBool Vc3PdhChannelIsE3(AtPdhChannel pdhChannel)
    {
    eAtPdhDe3FrameType de3FrameType = AtPdhChannelFrameTypeGet(pdhChannel);
    if ((de3FrameType == cAtPdhE3Unfrm )  ||
        (de3FrameType == cAtPdhE3Frmg832) ||
        (de3FrameType == cAtPdhE3FrmG751))
        return cAtTrue;

    return cAtFalse;
    }

static eThaPdhStsVtMapCtrl Vc3FractionalAsyncDe3MapControl(AtSdhChannel self, eAtSdhChannelType parentType)
    {
    if (Vc3PdhChannelIsE3((AtPdhChannel)AtSdhChannelMapChannelGet(self)))
        return (parentType == cAtSdhChannelTypeAu3) ? cThaStsVtVc3FracCepE3MapMd : cThaStsVtVc4FracCepE3MapMd;

    return (parentType == cAtSdhChannelTypeAu3) ? cThaStsVtVc3FracCepDs3MapMd : cThaStsVtVc4FracCepDs3MapMd;
    }

static eBool IsTu3VcCepMode(AtSdhChannel vc3)
    {
    return AtSdhVcStuffIsEnabled((AtSdhVc)vc3) ? cAtFalse : cAtTrue;
    }

static eThaPdhStsVtMapCtrl De3LiuOverCepMapControl(AtSdhChannel sdhVc)
    {
    AtPdhChannel de3 = (AtPdhChannel)AtSdhChannelMapChannelGet(sdhVc);
    eAtPdhChannelType channelType = AtPdhChannelTypeGet(de3);
    if (IsTu3VcCepMode(sdhVc) == cAtFalse)
        return (channelType == cAtPdhChannelTypeDs3) ? cThaStsVtVc3Ds3MapMd : cThaStsVtVc3E3MapMd;

    return (channelType == cAtPdhChannelTypeDs3) ? cThaStsVtTu3Ds3MapMd : cThaStsVtTu3E3MapMd;
    }

static eThaPdhStsVtMapCtrl CepPdhMapControl(AtChannel self)
    {
    AtSdhChannel sdhVc = (AtSdhChannel)self;
    eAtSdhChannelType vcType = AtSdhChannelTypeGet(sdhVc);

    if (vcType == cAtSdhChannelTypeVc3)
        {
        eAtSdhChannelType vc3ParentType = AtSdhChannelTypeGet(AtSdhChannelParentChannelGet((AtSdhChannel)self));
        if (AtSdhChannelMapTypeGet(sdhVc) == cAtSdhVcMapTypeVc3Map7xTug2s)
            return Vc3FractionalMapControl(sdhVc);

        if (AtSdhChannelMapTypeGet(sdhVc) == cAtSdhVcMapTypeVc3MapDe3)
            {
            if (ThaSdhAuVcMapDe3LiuIsEnabled((ThaSdhAuVc)sdhVc))
                return De3LiuOverCepMapControl(sdhVc);

            return Vc3FractionalAsyncDe3MapControl(sdhVc, vc3ParentType);
            }
            
        return IsTu3VcCepMode(sdhVc) ? cThaStsVtTu3BCepMapMd : cThaStsVtVc3BCepMapMd;
        }

    if (vcType == cAtSdhChannelTypeVc4)
        {
        if (AtSdhChannelMapTypeGet(sdhVc) == cAtSdhVcMapTypeVc4Map3xTug3s)
            return Vc4FractionalMapControl(sdhVc);

        return cThaStsVtVc4BCepMapMd;
        }

    if (vcType == cAtSdhChannelTypeVc4_4c)
        return cThaStsVtVc4_4cBCepMapMd;

    return 0xFF;
    }

static eThaPdhStsVtDmapCtrl Vc3FractionalDemapControl(AtSdhChannel self)
    {
    AtSdhChannel tug2 = AtSdhChannelSubChannelGet(self, 0);
    if (AtSdhChannelMapTypeGet(tug2) == cAtSdhTugMapTypeTug2Map3xTu12s)
        return cThaStsVtVc3FracCepVt2DmapMd;

    return cThaStsVtVc3FracCepVt15DmapMd;
    }

static eThaPdhStsVtDmapCtrl Vc4FractionalDemapControl(AtSdhChannel self)
    {
    AtSdhChannel tug = AtSdhChannelSubChannelGet(self, 0);
    if (AtSdhChannelMapTypeGet(tug) == cAtSdhTugMapTypeTug3MapVc3)
        return cThaStsVtVc4FracCepE3DmapMd;

    /* Tug3 map 7xtug2 */
    tug = AtSdhChannelSubChannelGet(tug, 0);
    if (AtSdhChannelMapTypeGet(tug) == cAtSdhTugMapTypeTug2Map3xTu12s)
        return cThaStsVtVc4FracCepVt2DmapMd;

    return cThaStsVtVc4FracCepVt15DmapMd;
    }

static eThaPdhStsVtMapCtrl Vc3FractionalAsyncDe3DemapControl(AtSdhChannel self, eAtSdhChannelType parentType)
    {
    if (Vc3PdhChannelIsE3((AtPdhChannel)AtSdhChannelMapChannelGet(self)))
        return (parentType == cAtSdhChannelTypeAu3) ? cThaStsVtVc3FracCepE3DmapMd : cThaStsVtVc4FracCepE3DmapMd;

    return (parentType == cAtSdhChannelTypeAu3) ? cThaStsVtVc3FracCepDs3DmapMd : cThaStsVtVc4FracCepDs3DmapMd;
    }

static eThaPdhStsVtDmapCtrl De3LiuOverCepDeMapControl(AtSdhChannel sdhVc)
    {
    AtPdhChannel de3 = (AtPdhChannel)AtSdhChannelMapChannelGet(sdhVc);
    return (AtPdhChannelTypeGet(de3) == cAtPdhChannelTypeDs3) ? cThaStsVtVc3Ds3DmapMd : cThaStsVtVc3E3DmapMd;
    }

static eThaPdhStsVtDmapCtrl CepPdhDemapControl(AtChannel self)
    {
    AtSdhChannel sdhVc = (AtSdhChannel)self;
    eAtSdhChannelType vcType = AtSdhChannelTypeGet(sdhVc);

    if (vcType == cAtSdhChannelTypeVc3)
        {
        eAtSdhChannelType vc3ParentType = AtSdhChannelTypeGet(AtSdhChannelParentChannelGet((AtSdhChannel)self));

        if (AtSdhChannelMapTypeGet(sdhVc) == cAtSdhVcMapTypeVc3Map7xTug2s)
            return Vc3FractionalDemapControl(sdhVc);

        if (AtSdhChannelMapTypeGet(sdhVc) == cAtSdhVcMapTypeVc3MapDe3)
            {
            if(ThaSdhAuVcMapDe3LiuIsEnabled((ThaSdhAuVc)sdhVc))
                return De3LiuOverCepDeMapControl(sdhVc);
            return Vc3FractionalAsyncDe3DemapControl(sdhVc, vc3ParentType);
            }

        return IsTu3VcCepMode(sdhVc) ? cThaStsVtTu3BCepDmapMd : cThaStsVtVc3BCepDmapMd;
        }

    if (vcType == cAtSdhChannelTypeVc4)
        {
        if (AtSdhChannelMapTypeGet(sdhVc) == cAtSdhVcMapTypeVc4Map3xTug3s)
            return Vc4FractionalDemapControl(sdhVc);

        return cThaStsVtVc4BCepDmapMd;
        }

    if (vcType == cAtSdhChannelTypeVc4_4c)
        return cThaStsVtVc4_4cBCepDmapMd;

    return 0xFF;
    }

static eBool IsCepVc4FractionalVc3(AtChannel self)
    {
    AtSdhChannel sdhVc = (AtSdhChannel)self;
    eAtSdhChannelType vcType = AtSdhChannelTypeGet(sdhVc);
    AtSdhChannel tug3;

    if (vcType != cAtSdhChannelTypeVc4)
        return cAtFalse;

    if (AtSdhChannelMapTypeGet(sdhVc) != cAtSdhVcMapTypeVc4Map3xTug3s)
        return cAtFalse;

    tug3 = AtSdhChannelSubChannelGet(sdhVc, 0);
    if (AtSdhChannelMapTypeGet(tug3) == cAtSdhTugMapTypeTug3MapVc3)
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet Vc4CepPwSet(AtSdhChannel vc4)
    {
    eAtRet ret = ThaSdhAuVcPgStsSlaverIndicate(vc4, cAtSdhVcMapTypeVc4MapC4);
    ret |= mMethodsGet(mThis(vc4))->Vc4xPldSet(mThis(vc4));
    return ret;
    }

static eAtRet Vc3CepPwSet(AtSdhChannel vc3)
    {
    eAtRet ret;
    ThaModuleCdr moduleCdr = (ThaModuleCdr)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)vc3), cThaModuleCdr);
    ret  = ThaCdrVc3CepPldSet(moduleCdr, vc3);
    ret |= ThaCdrVc3CepModeEnable(moduleCdr, vc3, AtSdhChannelSts1Get(vc3), IsTu3VcCepMode(vc3));

    return ret;
    }

static eBool IsTu3Vc(AtSdhChannel sdhChannel)
    {
    AtSdhChannel parent = AtSdhChannelParentChannelGet(sdhChannel);
    return (AtSdhChannelTypeGet(parent) == cAtSdhChannelTypeTu3);
    }

static eBool NeedReconfigureSSBitWhenBindingPw(ThaSdhAuVc self)
    {
    /* SS bit of AU will be overwritten when configure mapping to VC1x, so need to set again to recover */
    AtUnused(self);
    return cAtTrue;
    }

static eBool NeedChangeAlarmAffectingWhenBindingPw(ThaSdhAuVc self)
    {
    if (!AtSdhPathHasPohProcessor((AtSdhPath)self))
        return cAtFalse;

    /* For VC-3 mapping DS3/E3 LIU, we have to keep alarm affecting to
     * generate AIS downstream toward DS3/E3 layer. */
    if (ThaSdhAuVcMapDe3LiuIsEnabled(mThis(self)))
        return cAtFalse;

    return cAtTrue;
    }

static eAtRet PwCepSet(ThaSdhVc self)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)self;
    uint8 sts_i;
    eAtRet ret = cAtOk;
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(sdhChannel);

    /* Disable POH alarm forwarding */
    if (NeedChangeAlarmAffectingWhenBindingPw(mThis(self)))
        ret = AtSdhChannelAlarmAffectingDisable(sdhChannel);

    for (sts_i = 0; sts_i < AtSdhChannelNumSts(sdhChannel); sts_i++)
        {
        uint8 stsId = (uint8)(AtSdhChannelSts1Get(sdhChannel) + sts_i);
        AtPrbsEngine engine = AtChannelPrbsEngineGet((AtChannel)sdhChannel);

        /* CEP application, POH in ENCAP/DECAP from Pseudowire packets, so SDH module need disable POH processing */
        /* If this channel has a PRBS engine running, just save disable state to
         * DB due to PRBS engine need POH insert enabled to run */
        if ((engine == NULL) || (AtPrbsEngineIsEnabled(engine) == cAtFalse))
            {
            if (AtSdhPathHasPohProcessor((AtSdhPath)self))
                ret |= AtSdhPathPohInsertionEnable((AtSdhPath)self, cAtFalse);
            }

        if (!IsTu3Vc(sdhChannel))
            {
            /* Disable Rx termination */
            ret |= ThaModuleOcnRxStsTermEnable(sdhChannel, stsId, cAtFalse);

            /* Disable Tx pointer generation */
            ret |= ThaOcnStsTxPayloadTypeSet(sdhChannel, stsId, cThaStsDontCareVtTu);
            }
        }

    if (channelType == cAtSdhChannelTypeVc4)
        ret |= Vc4CepPwSet(sdhChannel);

    else if (channelType == cAtSdhChannelTypeVc3)
        ret |= Vc3CepPwSet(sdhChannel);

    /* SS bit of AU will be overwritten when configure mapping to VC1x, so need to set again to recover */
    if (!IsTu3Vc(sdhChannel) && mMethodsGet(mThis(sdhChannel))->NeedReconfigureSSBitWhenBindingPw(mThis(sdhChannel)))
        ret |= ThaSdhAuTxSsSet((AtSdhPath)AtSdhChannelParentChannelGet(sdhChannel), AtSdhPathTxSsGet((AtSdhPath)sdhChannel), cAtFalse);

    return ret;
    }

static eAtRet Tu1xSsBitRestore(AtSdhChannel tug3vc3)
    {
    uint8 tug2_i, tu_i;
    AtSdhChannel tug2;
    eAtRet ret = cAtOk;

    for (tug2_i = 0; tug2_i < AtSdhChannelNumberOfSubChannelsGet(tug3vc3); tug2_i++)
        {
        tug2 = AtSdhChannelSubChannelGet(tug3vc3, tug2_i);
        for (tu_i = 0; tu_i < AtSdhChannelNumberOfSubChannelsGet(tug2); tu_i++)
            ret |= AtSdhTuDefaultSsSet((AtSdhPath)AtSdhChannelSubChannelGet(tug2, tu_i));
        }

    return ret;
    }

static eAtRet Vc4xMappingRestore(AtSdhChannel self)
    {
    eAtRet ret;
    uint8 stsIds[cThaOcnNumStsInAu4_64c];
    uint8 subChannel_i, tug3MapType;
    uint8 vc4MapType = AtSdhChannelMapTypeGet(self);
    AtSdhChannel subChannel;

    /* Get 3 STS in one AU-4 */
    Vc4xStsIdListGet(self, stsIds);

    ret  = Vc4xPohInsertEnableSet(self, stsIds);
    ret |= ThaSdhAuVcPgStsSlaverIndicate(self, vc4MapType);

    /* Need to configure CDR */
    if (vc4MapType != cAtSdhVcMapTypeVc4Map3xTug3s)
        {
        ret |= SdhVcPdhBypass((AtChannel)self);
        ret |= mMethodsGet(mThis(self))->Vc4xPldSet(mThis(self));
        ret |= ThaModuleOcnRxStsTermEnable(self, AtSdhChannelSts1Get(self), cAtFalse);
        return ret;
        }

    for (subChannel_i = 0; subChannel_i < AtSdhChannelNumberOfSubChannelsGet(self); subChannel_i++)
        {
        subChannel = AtSdhChannelSubChannelGet(self, subChannel_i);
        tug3MapType = AtSdhChannelMapTypeGet(subChannel);
        ret |= ThaOcnTug3PldSet(subChannel, tug3MapType);
        ret |= Tu1xSsBitRestore(subChannel);
        }

    return ret;
    }

static eAtRet Vc3MappingRestore(AtSdhChannel sdhVc)
    {
    uint8 mapType = AtSdhChannelMapTypeGet(sdhVc);

    if (mapType == cAtSdhVcMapTypeVc3Map7xTug2s)
        {
        eAtRet ret = ThaOcnVc3PldSet(sdhVc, cThaOcnVc3Pld7Tug2);
        ret |= Tu1xSsBitRestore(sdhVc);
        return ret;
        }

    if (mapType == cAtSdhVcMapTypeVc3MapC3)
        return ThaOcnVc3PldSet(sdhVc, cThaOcnVc3PldC3);

    if (mapType == cAtSdhVcMapTypeVc3MapDe3)
        return ThaOcnVc3PldSet(sdhVc, Vc3PldTypeGet((ThaSdhAuVc)sdhVc));

    return cAtOk;
    }

static eAtRet MappingRestore(ThaSdhVc self)
    {
    AtSdhChannel sdhVc = (AtSdhChannel)self;
    eAtRet ret = cAtOk;
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(sdhVc);

    /* Restore POH alarm forwarding */
    if (NeedChangeAlarmAffectingWhenBindingPw(mThis(self)))
        ret = AtSdhChannelAlarmAffectingRestore(sdhVc);

    if ((channelType == cAtSdhChannelTypeVc4)     ||
        (channelType == cAtSdhChannelTypeVc4_4c)  ||
        (channelType == cAtSdhChannelTypeVc4_16c) ||
        (channelType == cAtSdhChannelTypeVc4_64c) ||
        (channelType == cAtSdhChannelTypeVc4_nc))
        ret |= Vc4xMappingRestore(sdhVc);

    else if (channelType == cAtSdhChannelTypeVc3)
        ret |= Vc3MappingRestore(sdhVc);

    return ret;
    }

static eAtRet SdhVcM13Bypass(AtSdhChannel self)
    {
    AtModulePdh pdhModule = ModulePdh(self);
    uint8 sts_i;
    eAtRet ret = cAtOk;

    if (!mMethodsGet(mThis(self))->NeedConfigurePdh(mThis(self)))
        return cAtOk;

    /* If VC-3 is carrying an DS3/E3 LIU, do not bypass its framing. */
    if (ThaSdhAuVcMapDe3LiuIsEnabled((ThaSdhAuVc)self))
        return cAtOk;

    for (sts_i = 0; sts_i < AtSdhChannelNumSts(self); sts_i++)
        {
        uint8 sts1Id = (uint8)(AtSdhChannelSts1Get(self) + sts_i);

        ret |= ThaPdhSts1M13Bypass(pdhModule, self, sts1Id);
        ret |= ThaSdhVcPdhFrameBypass((AtSdhVc)self);
        }

    return  ret;
    }

static eAtRet BindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    eAtRet ret = cAtOk;
    AtDevice device = AtChannelDeviceGet(self);
    ThaModuleAbstractMap mapDemapModule;
    ThaModuleSdh sdhModule = (ThaModuleSdh)AtChannelModuleGet(self);
    AtSdhChannel sdhChannel = (AtSdhChannel)self;
    AtSdhVc sdhVc = (AtSdhVc)self;

    /* Release loopback first */
    if (ThaModuleSdhShouldRemoveLoopbackOnBinding(sdhModule))
        {
        ret = AtChannelLoopbackSet(self, cAtLoopbackModeRelease);
        if (ret != cAtOk)
            mChannelLog(self, cAtLogLevelWarning, "Release loopback fail\r\n");
        }

    /* Let supper do first */
    ret = m_AtChannelMethods->BindToPseudowire(self, pseudowire);
    if (ret != cAtOk)
        return ret;

    /* In warm restore, don't need to configure map module */
    if (AtChannelDeviceInWarmRestore((AtChannel)self))
        return cAtOk;

    /* PDH MAP/DEMAP control */
    if (mMethodsGet(mThis(self))->NeedConfigurePdh(mThis(self)))
        {
        AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(device, cAtModulePdh);
        ret |= ThaPdhStsVtMapCtrlSet(pdhModule, sdhChannel, CepPdhMapControl(self));
        ret |= ThaPdhStsDeMapCtrlSet(pdhModule, sdhChannel, CepPdhDemapControl(self));
        ret |= ThaPdhStsVtDeMapVc4FractionalVc3Set(pdhModule, sdhChannel, IsCepVc4FractionalVc3(self));
        }

    /* Bypass PDH framing */
    if (pseudowire)
        ret |= SdhVcM13Bypass(sdhChannel);

    /* Frame mode and map signal type */
    mapDemapModule = (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap);
    ret |= mMethodsGet(mThis(self))->MapFrameModeDefaultSet(mThis(self));

    /* Bind to pseudowire at MAP/DEMAP module */
    ret |= ThaModuleAbstractMapBindAuVcToPseudowire(mapDemapModule, sdhVc, pseudowire);
    mapDemapModule = (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap);
    ret |= ThaModuleAbstractMapBindAuVcToPseudowire(mapDemapModule, sdhVc, pseudowire);

    /* Configure VC bind to pseudowire CEP */
    if (pseudowire)
        ret |= mMethodsGet((ThaSdhVc)self)->PwCepSet((ThaSdhVc)self);

    /* When bind vc to pseudowire, need to modify something to suit CEP application,
     * So when unbind, need to revert things which were previously modified
     */
    else
        ret |= mMethodsGet((ThaSdhVc)self)->MappingRestore((ThaSdhVc)self);

    return ret;
    }

static uint8 NumSts(AtSdhChannel self)
    {
    eAtSdhChannelType vcType = AtSdhChannelTypeGet(self);

    if (vcType == cAtSdhChannelTypeVc3)     return 1;
    if (vcType == cAtSdhChannelTypeVc4)     return 3;
    if (vcType == cAtSdhChannelTypeVc4_4c)  return 12;
    if (vcType == cAtSdhChannelTypeVc4_16c) return 48;
    if (vcType == cAtSdhChannelTypeVc4_64c) return 192;
    if (vcType == cAtSdhChannelTypeVc4_nc)  return m_AtSdhChannelMethods->NumSts(self);
    if (vcType == cAtSdhChannelTypeVc4_16nc)return m_AtSdhChannelMethods->NumSts(self);

    return 0;
    }

static eBool NeedToResetConcate(AtSdhChannel self)
    {
    AtSdhChannel parent;

    if (AtSdhChannelTypeGet(self) != cAtSdhChannelTypeVc3)
        return cAtTrue;

    parent = AtSdhChannelParentChannelGet(self);
    if (AtSdhChannelTypeGet(parent) == cAtSdhChannelTypeTu3)
        return cAtFalse;

    return cAtTrue;
    }

static uint8 NumBlocksNeedToBindToEncapChannel(AtChannel self, AtEncapChannel encapChannel)
    {
    eAtSdhChannelType sdhChannelType = AtSdhChannelTypeGet((AtSdhChannel)self);
	AtUnused(encapChannel);

    if (sdhChannelType == cAtSdhChannelTypeVc4)
        return 3;

    return 0;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = cAtOk;
    AtSdhChannel sdhChannel = (AtSdhChannel)self;

    /* Set default concatenation */
    if (NeedToResetConcate(sdhChannel))
        {
        ret |= ThaOcnStsConcat(sdhChannel, AtSdhChannelSts1Get(sdhChannel), NumSts(sdhChannel));
        if (ret != cAtOk)
            return cAtOk;
        }

    /* Disable VC-3 mapping DS3/E3 Line if any */
    if (ThaSdhAuVcMapDe3LiuIsEnabled((ThaSdhAuVc)self))
        ThaSdhAuVcMapDe3LiuEnable((ThaSdhAuVc)self, cAtFalse);

    /* Super's initialization */
    return m_AtChannelMethods->Init(self);
    }

static eBool CanBindToEncapChannel(AtSdhChannel self)
    {
    eAtSdhChannelType sdhChannelType = AtSdhChannelTypeGet(self);

    if ((sdhChannelType == cAtSdhChannelTypeVc4_16c)||
        (sdhChannelType == cAtSdhChannelTypeVc4_4c) ||
        (sdhChannelType == cAtSdhChannelTypeVc4)    ||
        (sdhChannelType == cAtSdhChannelTypeVc3))
        return cAtTrue;

    return cAtFalse;
    }

static uint8 CxMapping(AtSdhChannel self)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(self);
    switch (channelType)
        {
        case cAtSdhChannelTypeVc4_64c: return cAtSdhVcMapTypeVc4_64cMapC4_64c;
        case cAtSdhChannelTypeVc4_16c: return cAtSdhVcMapTypeVc4_16cMapC4_16c;
        case cAtSdhChannelTypeVc4_4c : return cAtSdhVcMapTypeVc4_4cMapC4_4c;
        case cAtSdhChannelTypeVc4_16nc: return cAtSdhVcMapTypeVc4_16ncMapC4_16nc;
        case cAtSdhChannelTypeVc4_nc : return cAtSdhVcMapTypeVc4_ncMapC4_nc;
        case cAtSdhChannelTypeVc4    : return cAtSdhVcMapTypeVc4MapC4;
        case cAtSdhChannelTypeVc3    : return cAtSdhVcMapTypeVc3MapC3;
        case cAtSdhChannelTypeVc12   : return cAtSdhVcMapTypeVc1xMapC1x;
        case cAtSdhChannelTypeVc11   : return cAtSdhVcMapTypeVc1xMapC1x;

        /* Impossible, but ... */
        case cAtSdhChannelTypeUnknown:
        case cAtSdhChannelTypeLine   :
        case cAtSdhChannelTypeAug64  :
        case cAtSdhChannelTypeAug16  :
        case cAtSdhChannelTypeAug4   :
        case cAtSdhChannelTypeAug1   :
        case cAtSdhChannelTypeAu4_64c:
        case cAtSdhChannelTypeAu4_16c:
        case cAtSdhChannelTypeAu4_4c :
        case cAtSdhChannelTypeAu4_16nc:
        case cAtSdhChannelTypeAu4_nc :
        case cAtSdhChannelTypeAu4    :
        case cAtSdhChannelTypeAu3    :
        case cAtSdhChannelTypeTug3   :
        case cAtSdhChannelTypeTug2   :
        case cAtSdhChannelTypeTu3    :
        case cAtSdhChannelTypeTu12   :
        case cAtSdhChannelTypeTu11   :
        default:
            AtChannelLog((AtChannel)self, cAtLogLevelCritical, AtSourceLocation, "Wrong channel type, got %d\r\n", channelType);
            return cAtSdhVcMapTypeNone;
        }
    }

static AtEncapBinder EncapBinder(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return AtDeviceEncapBinder(device);
    }

static eAtRet HardwareBindToEncapChannel(AtEncapBinder encapBinder, AtChannel self, AtEncapChannel encapChannel)
    {
    if (IsTu3Vc((AtSdhChannel)self))
        return AtEncapBinderBindTu3VcToEncapChannel(encapBinder, self, encapChannel);
    else
        return AtEncapBinderBindHoVcToEncapChannel(encapBinder, self, encapChannel);
    }

/* Binding to Encapsulation */
static eAtRet BindToEncapChannel(AtChannel self, AtEncapChannel encapChannel)
    {
    eAtRet ret = cAtOk;
    const uint8 cPslEquippedNonspecificPayload = 1;
    const uint8 cPslHdlcOverSdhMapping = 0x16;
    AtSdhPath path = (AtSdhPath)self;
    AtSdhChannel sdhChannel = (AtSdhChannel)self;
    uint8 label;

    if (!CanBindToEncapChannel((AtSdhChannel)self))
        return cAtErrorModeNotSupport;

    /* Reset mapping before binding. By this way, all of sub channels will be deleted */
    if (encapChannel)
        {
        /* Cache SSbit */
        eBool ssBitEnabled = AtSdhPathSsCompareIsEnabled((AtSdhPath)sdhChannel);
        uint8 txSs = AtSdhPathTxSsGet((AtSdhPath)sdhChannel);
        uint8 expectedSs = AtSdhPathExpectedSsGet((AtSdhPath)sdhChannel);

        ret |= AtSdhChannelMapTypeSet(sdhChannel, cAtSdhVcMapTypeNone);
        if (ret != cAtOk)
            return ret;

        /* Use Cx mapping */
        ret |= AtSdhChannelMapTypeSet(sdhChannel, CxMapping(sdhChannel));

        /* Re-configure old value */
        AtSdhPathTxSsSet((AtSdhPath)sdhChannel, txSs);
        AtSdhPathExpectedSsSet((AtSdhPath)sdhChannel, expectedSs);
        AtSdhPathSsCompareEnable((AtSdhPath)sdhChannel, ssBitEnabled);
        }

    /* Hardware binding */
    ret |= HardwareBindToEncapChannel(EncapBinder(self), self, encapChannel);
    
    /* Set default stuff byte mode in VC4/VC3 */
    if (encapChannel)
        ret |= SdhVcPdhBypass(self);

    /* Let super deal with database */
    ret |= m_AtChannelMethods->BindToEncapChannel(self, encapChannel);

    /* Set correct C2 */
    label = AtSdhPathExpectedPslGet(path);
    if ((label == cPslEquippedNonspecificPayload) || (label == 0))
        AtSdhPathExpectedPslSet(path, cPslHdlcOverSdhMapping);

    label = AtSdhPathTxPslGet(path);
    if ((label == cPslEquippedNonspecificPayload) || (label == 0))
        AtSdhPathTxPslSet(path, cPslHdlcOverSdhMapping);

    return ret;
    }

static uint32 EncapHwIdAllocate(AtChannel self)
    {
    return AtEncapBinderAuVcEncapHwIdAllocate(EncapBinder(self), (AtSdhVc)self);
    }

static uint8 Vc3PldTypeGet(ThaSdhAuVc sdhVc)
    {
    AtPdhDe3 de3 = (AtPdhDe3)SdhVcPdhChannelGet((AtSdhVc)sdhVc);

    if (ThaSdhAuVcMapDe3LiuIsEnabled(sdhVc))
        return cThaOcnVc3PldC3;

    if (AtPdhDe3IsDs3(de3))
        return cThaOcnVc3PldDs3;

    if (AtPdhDe3IsE3(de3))
        return cThaOcnVc3PldE3;

    return cThaOcnVc3PldC3;
    }

static eAtRet Ds3Map(ThaSdhAuVc self)
    {
    return ThaOcnVc3PldSet((AtSdhChannel)self, Vc3PldTypeGet(self));
    }

static uint8 PppBlockRange(AtChannel self)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet((AtSdhChannel)self);

    if (channelType == cAtSdhChannelTypeVc4_4c) return 7;
    if (channelType == cAtSdhChannelTypeVc4)    return 4;
    if (channelType == cAtSdhChannelTypeVc3)    return 3;

    return 4;
    }

static eAtRet EncapConnectionEnable(AtChannel self, eBool enable)
    {
    eAtRet ret = cAtOk;
    AtDevice device = AtChannelDeviceGet(self);

    ret |= ThaModuleAbstractMapVcxEncapConnectionEnable((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap)  , (AtSdhVc)self, enable);
    ret |= ThaModuleAbstractMapVcxEncapConnectionEnable((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap), (AtSdhVc)self, enable);

    return ret;
    }

static eAtRet TxTrafficEnable(AtChannel self, eBool enable)
    {
    AtDevice device = AtChannelDeviceGet(self);
    ThaModuleAbstractMap mapModule = (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap);
    return ThaModuleAbstractMapVcxEncapConnectionEnable(mapModule, (AtSdhVc)self, enable);
    }

static eAtRet RxTrafficEnable(AtChannel self, eBool enable)
    {
    AtDevice device = AtChannelDeviceGet(self);
    ThaModuleAbstractMap mapModule = (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap);
    return ThaModuleAbstractMapVcxEncapConnectionEnable(mapModule, (AtSdhVc)self, enable);
    }

static eBool TxTrafficIsEnabled(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    ThaModuleAbstractMap mapModule = (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap);
    return ThaModuleAbstractMapVcxEncapConnectionIsEnabled(mapModule, (AtSdhVc)self);
    }

static eBool RxTrafficIsEnabled(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    ThaModuleAbstractMap mapModule = (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap);
    return ThaModuleAbstractMapVcxEncapConnectionIsEnabled(mapModule, (AtSdhVc)self);
    }

static uint8 HwMapTypeGet(AtSdhChannel self)
    {
    eAtSdhChannelType vcType = AtSdhChannelTypeGet(self);

    if (vcType == cAtSdhChannelTypeVc4_64c)
        return Vc4_64cHwMapTypeGet(self);
    if (vcType == cAtSdhChannelTypeVc4_16c)
        return Vc4_16cHwMapTypeGet(self);
    if (vcType == cAtSdhChannelTypeVc4_4c)
        return Vc4_4cHwMapTypeGet(self);
    if (vcType == cAtSdhChannelTypeVc4)
        return Vc4HwMapTypeGet(self);
    if (vcType == cAtSdhChannelTypeVc3)
        return Vc3HwMapTypeGet(self);

    return cAtSdhVcMapTypeNone;
    }

static eAtRet WarmRestore(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    uint32 mapPwHwId;
    AtPw pw;
    AtModulePw modulePw;
    eAtPwCepMode cepMode;

    /* Let super restore first */
    eAtRet ret = m_AtChannelMethods->WarmRestore(self);
    if (ret != cAtOk)
        return ret;

    mapPwHwId = ThaModuleAbstractMapAuVcBoundPwHwIdGet((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap), (AtSdhVc)self, &cepMode);
    if (mapPwHwId == cThaModulePwInvalidPwId)
        {
        AtChannelBoundPwSet(self, NULL);
        return cAtOk;
        }

    /* PW hw id is valid, restore pw */
    modulePw = (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);
    pw = (AtPw)AtModulePwCepCreate(modulePw, mapPwHwId, cepMode);
    ret = AtPwCircuitBind(pw, self);

    if (ret != cAtOk)
        {
        AtModulePwDeletePw(modulePw, mapPwHwId);
        return ret;
        }

    return AtChannelWarmRestore((AtChannel)pw);
    }

static eBool NeedConfigurePdh(ThaSdhAuVc self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet PgStsSlaverIndicate(ThaSdhAuVc self, eAtSdhVcMapType vcMapType)
    {
    uint8 sts_i;
    AtSdhChannel sdhChannel = (AtSdhChannel)self;
    uint8 startSts = AtSdhChannelSts1Get(sdhChannel);

    /* First is master */
    eAtRet ret = ThaOcnStsPgSlaveIndicate(sdhChannel, startSts, cAtFalse);

    /* Others depend on vcMapType,
     * As HW recommended, just set PG slaver indicate in case of VC4MapC4 and VC4_4cMapC4_4c
     * because this register is used for concatenation in case of VC4 unchannelized only,
     * when VC4 is channelized, another register is used instead, so need to release concatenation in this register
     */
    for (sts_i = 1; sts_i < AtSdhChannelNumSts(sdhChannel); sts_i++)
        {
        uint8 sts1Id = (uint8)(sts_i + startSts);
        if ((vcMapType == cAtSdhVcMapTypeVc4MapC4)        ||
            (vcMapType == cAtSdhVcMapTypeVc4_4cMapC4_4c)  ||
            (vcMapType == cAtSdhVcMapTypeVc4_16cMapC4_16c)||
            (vcMapType == cAtSdhVcMapTypeVc4_64cMapC4_64c))
            ret |= ThaOcnStsPgSlaveIndicate(sdhChannel, sts1Id, cAtTrue);
        else
            ret |= ThaOcnStsPgSlaveIndicate(sdhChannel, sts1Id, cAtFalse);
        }

    return ret;
    }

static eBool Vc4MapTypeIsSupported(AtSdhChannel self, uint8 mapType)
    {
    switch (mapType)
        {
        case cAtSdhVcMapTypeVc4MapC4     :
            return cAtTrue;

        case cAtSdhVcMapTypeVc4Map3xTug3s:
            if (!mMethodsGet((ThaSdhVc)self)->HasLoOrderMapping((ThaSdhVc)self))
                return cAtFalse;
            return (AtSdhChannelModeGet(self) == cAtSdhChannelModeSdh) ? cAtTrue : cAtFalse;

        default:
            return cAtFalse;
        }
    }

static eBool Vc3MapTypeIsSupported(AtSdhChannel self, uint8 mapType)
    {
    switch (mapType)
        {
        case cAtSdhVcMapTypeVc3MapC3     :
            return cAtTrue;
        case cAtSdhVcMapTypeVc3Map7xTug2s:
            return (mMethodsGet((ThaSdhVc)self)->HasLoOrderMapping((ThaSdhVc)self)) ? cAtTrue : cAtFalse;
        case cAtSdhVcMapTypeVc3MapDe3    :
            return (mMethodsGet((ThaSdhVc)self)->HasLoOrderMapping((ThaSdhVc)self)) ? cAtTrue : cAtFalse;
        default:
            return cAtFalse;
        }
    }

static eBool MapTypeIsSupported(AtSdhChannel self, uint8 mapType)
    {
    uint32 vcType = AtSdhChannelTypeGet(self);

    if (mapType == cAtSdhVcMapTypeNone)
        return cAtTrue;

    switch (vcType)
        {
        case cAtSdhChannelTypeVc4_64c: return (mapType == cAtSdhVcMapTypeVc4_64cMapC4_64c) ? cAtTrue : cAtFalse;
        case cAtSdhChannelTypeVc4_16c: return (mapType == cAtSdhVcMapTypeVc4_16cMapC4_16c) ? cAtTrue : cAtFalse;
        case cAtSdhChannelTypeVc4_4c : return (mapType == cAtSdhVcMapTypeVc4_4cMapC4_4c)   ? cAtTrue : cAtFalse;
        case cAtSdhChannelTypeVc4_nc : return (mapType == cAtSdhVcMapTypeVc4_ncMapC4_nc)   ? cAtTrue : cAtFalse;
        case cAtSdhChannelTypeVc4_16nc: return (mapType == cAtSdhVcMapTypeVc4_16ncMapC4_16nc) ? cAtTrue : cAtFalse;
        case cAtSdhChannelTypeVc4    : return Vc4MapTypeIsSupported(self, mapType);
        case cAtSdhChannelTypeVc3    : return Vc3MapTypeIsSupported(self, mapType);
        default:
            return cAtFalse;
        }
    }

static eAtRet De3LiuEnable(ThaSdhAuVc self, eBool enable)
    {
    return ThaOcnVc3MapDe3LiuEnable((AtSdhChannel)self, enable);
    }

static eBool De3LiuIsEnabled(ThaSdhAuVc self)
    {
    return ThaOcnVc3MapDe3LiuIsEnabled((AtSdhChannel)self);
    }

static uint32 BoundPwHwIdGet(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    AtSdhVc vc = (AtSdhVc)self;
    eAtPwCepMode cepMode;

    return ThaModuleAbstractMapAuVcBoundPwHwIdGet((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap), vc, &cepMode);
    }

static uint32 BoundEncapHwIdGet(AtChannel self)
    {
    return AtEncapBinderHoVcBoundEncapHwIdGet(EncapBinder(self), self);
    }

static eAtRet TxEncapConnectionEnable(AtChannel self, eBool enable)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return ThaModuleAbstractMapVcxEncapConnectionEnable((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap), (AtSdhVc)self, enable);
    }

static eBool TxEncapConnectionIsEnabled(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return ThaModuleAbstractMapVcxEncapConnectionIsEnabled((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap), (AtSdhVc)self);
    }

static eAtRet RxEncapConnectionEnable(AtChannel self, eBool enable)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return ThaModuleAbstractMapVcxEncapConnectionEnable((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap), (AtSdhVc)self, enable);
    }

static eBool RxEncapConnectionIsEnabled(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return ThaModuleAbstractMapVcxEncapConnectionIsEnabled((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap), (AtSdhVc)self);
    }

static eAtClockState HwClockStateTranslate(AtChannel self, uint8 hwState)
    {
    ThaModuleCdr moduleCdr = (ThaModuleCdr)AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModuleCdr);

    return ThaModuleCdrClockStateFromHoChannelHwStateGet(moduleCdr, hwState);
    }

static eAtRet PohInsertionEnable(AtSdhPath self, eBool enabled)
    {
    AtSdhChannel channel = (AtSdhChannel)self;
    return ThaOcnStsPohInsertEnable(channel, AtSdhChannelSts1Get(channel), enabled);
    }

static eBool PohInsertionIsEnabled(AtSdhPath self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;
    return ThaOcnStsPohInsertIsEnabled(channel, AtSdhChannelSts1Get(channel));
    }

static eAtRet LomMonitorEnable(ThaSdhAuVc self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorModeNotSupport;
    }

static eBool LomMonitorIsEnabled(ThaSdhAuVc self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet CanChangeStuffing(AtSdhVc self, eBool enable)
    {
    AtUnused(enable);
    if (AtChannelServiceIsRunning((AtChannel)self))
        return cAtErrorChannelBusy;

    return cAtOk;
    }

static eAtRet StuffEnable(AtSdhVc self, eBool enable)
    {
    eAtRet ret;
    ThaSdhVc vc = (ThaSdhVc)self;

    if (enable == AtSdhVcStuffIsEnabled(self))
        return cAtOk;

    ret = AtSdhVcCanChangeStuffing(self, enable);
    if (ret != cAtOk)
        return ret;

    if (mMethodsGet(vc)->HasOcnStuffing(vc))
        return ThaOcnVc3ToTu3VcEnable((AtSdhChannel)self, enable ? cAtFalse : cAtTrue);

    return m_AtSdhVcMethods->StuffEnable(self, enable);
    }

static eBool StuffIsEnabled(AtSdhVc self)
    {
    ThaSdhVc vc = (ThaSdhVc)self;

    if (mMethodsGet(vc)->HasOcnStuffing(vc))
        return ThaOcnVc3ToTu3VcIsEnabled((AtSdhChannel)self) ? cAtFalse : cAtTrue;

    return m_AtSdhVcMethods->StuffIsEnabled(self);
    }

static eBool ShouldCleanupMapChannel(AtSdhChannel self)
    {
    uint8 mapType = AtSdhChannelActualMapTypeGet(self);

    if (AtSdhChannelTypeGet(self) != cAtSdhChannelTypeVc3)
        return cAtTrue;

    /* If current mapping is VC-3 mapping DS3/E3, only clean-up DS3/E3 channel
     * if it is created and belonged to this VC-3. */
    if (mapType == cAtSdhVcMapTypeVc3MapDe3)
        return Vc3IsCreatingDe3(self);

    return cAtTrue;
    }

static ThaModuleConcate ModuleConcate(AtChannel self)
    {
    return (ThaModuleConcate)AtDeviceModuleGet(AtChannelDeviceGet(self), cAtModuleConcate);
    }

static eAtRet BindToSourceGroup(AtChannel self, AtConcateGroup group)
    {
    return ThaModuleConcateBindHoVcToSourceConcateGroup(ModuleConcate(self), self, group);
    }

static eAtRet BindToSinkGroup(AtChannel self, AtConcateGroup group)
    {
    return ThaModuleConcateBindHoVcToSinkConcateGroup(ModuleConcate(self), self, group);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaSdhAuVc object = (ThaSdhAuVc)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(isMapDe3Liu);
    mEncodeNone(attController);
    }

static void OverrideAtObject(ThaSdhAuVc self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtSdhPath(ThaSdhAuVc self)
    {
    AtSdhPath path = (AtSdhPath)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhPathOverride, mMethodsGet(path), sizeof(m_AtSdhPathOverride));
        mMethodOverride(m_AtSdhPathOverride, PohInsertionEnable);
        mMethodOverride(m_AtSdhPathOverride, PohInsertionIsEnabled);
        }

    mMethodsSet(path, &m_AtSdhPathOverride);
    }

static void OverrideAtChannel(ThaSdhAuVc self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, BindToEncapChannel);
        mMethodOverride(m_AtChannelOverride, NumBlocksNeedToBindToEncapChannel);
        mMethodOverride(m_AtChannelOverride, PppBlockRange);
        mMethodOverride(m_AtChannelOverride, BindToPseudowire);
        mMethodOverride(m_AtChannelOverride, EncapConnectionEnable);
        mMethodOverride(m_AtChannelOverride, RxTrafficEnable);
        mMethodOverride(m_AtChannelOverride, TxTrafficEnable);
        mMethodOverride(m_AtChannelOverride, RxTrafficIsEnabled);
        mMethodOverride(m_AtChannelOverride, TxTrafficIsEnabled);
        mMethodOverride(m_AtChannelOverride, WarmRestore);
        mMethodOverride(m_AtChannelOverride, EncapHwIdAllocate);
        mMethodOverride(m_AtChannelOverride, BoundPwHwIdGet);
        mMethodOverride(m_AtChannelOverride, BoundEncapHwIdGet);
        mMethodOverride(m_AtChannelOverride, RxEncapConnectionIsEnabled);
        mMethodOverride(m_AtChannelOverride, RxEncapConnectionEnable);
        mMethodOverride(m_AtChannelOverride, TxEncapConnectionIsEnabled);
        mMethodOverride(m_AtChannelOverride, TxEncapConnectionEnable);
        mMethodOverride(m_AtChannelOverride, AttController);
        mMethodOverride(m_AtChannelOverride, HwClockStateTranslate);
        mMethodOverride(m_AtChannelOverride, BindToSourceGroup);
        mMethodOverride(m_AtChannelOverride, BindToSinkGroup);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideThaSdhVc(ThaSdhAuVc self)
    {
    ThaSdhVc thaVc = (ThaSdhVc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhVcOverride, mMethodsGet(thaVc), sizeof(m_ThaSdhVcOverride));

        mMethodOverride(m_ThaSdhVcOverride, PohProcessorCreate);
        mMethodOverride(m_ThaSdhVcOverride, CdrControllerCreate);
        mMethodOverride(m_ThaSdhVcOverride, PwCepSet);
        mMethodOverride(m_ThaSdhVcOverride, MappingRestore);
        }

    mMethodsSet(thaVc, &m_ThaSdhVcOverride);
    }

static void OverrideAtSdhChannel(ThaSdhAuVc self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(m_AtSdhChannelOverride));
        mMethodOverride(m_AtSdhChannelOverride, MapTypeSet);
        mMethodOverride(m_AtSdhChannelOverride, HwMapTypeGet);
        mMethodOverride(m_AtSdhChannelOverride, MapTypeIsSupported);
        mMethodOverride(m_AtSdhChannelOverride, NumSts);
        mMethodOverride(m_AtSdhChannelOverride, ShouldCleanupMapChannel);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void OverrideAtSdhVc(ThaSdhAuVc self)
    {
    AtSdhVc vc = (AtSdhVc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhVcMethods = mMethodsGet(vc);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhVcOverride, mMethodsGet(vc), sizeof(m_AtSdhVcOverride));
        mMethodOverride(m_AtSdhVcOverride, CanChangeStuffing);
        mMethodOverride(m_AtSdhVcOverride, StuffEnable);
        mMethodOverride(m_AtSdhVcOverride, StuffIsEnabled);
        }

    mMethodsSet(vc, &m_AtSdhVcOverride);
    }

static void Override(ThaSdhAuVc self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtSdhPath(self);
    OverrideThaSdhVc(self);
    OverrideAtSdhChannel(self);
    OverrideAtSdhVc(self);
    }

static void MethodsInit(AtSdhVc self)
    {
    ThaSdhAuVc sdhVc = (ThaSdhAuVc)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Initialize method */
        mMethodOverride(m_methods, Ds3Map);
        mMethodOverride(m_methods, NeedConfigurePdh);
        mMethodOverride(m_methods, PgStsSlaverIndicate);
        mMethodOverride(m_methods, NeedReconfigureSSBitWhenBindingPw);
        mMethodOverride(m_methods, DefaultTug3MapType);
        mMethodOverride(m_methods, MapFrameModeDefaultSet);
        mMethodOverride(m_methods, Vc4xPldSet);
        mMethodOverride(m_methods, LomMonitorEnable);
        mMethodOverride(m_methods, LomMonitorIsEnabled);
        }

    mMethodsSet(sdhVc, &m_methods);
    }

AtSdhVc ThaSdhAuVcObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaSdhAuVc));

    /* Super constructor */
    if (ThaSdhVcObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Override */
    Override((ThaSdhAuVc)self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhVc ThaSdhAuVcNew(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhVc newVc = mMethodsGet(osal)->MemAlloc(osal, sizeof(tThaSdhAuVc));
    if (newVc == NULL)
        return NULL;

    /* Construct it */
    return ThaSdhAuVcObjectInit(newVc, channelId, channelType, module);
    }

eAtRet ThaSdhAuVcDs3Map(ThaSdhAuVc self)
    {
    if (self)
        return mMethodsGet(self)->Ds3Map(self);

    return cAtError;
    }

eAtRet ThaSdhAuVcPgStsSlaverIndicate(AtSdhChannel self, eAtSdhVcMapType vcMapType)
    {
    if (self)
        return mMethodsGet(mThis(self))->PgStsSlaverIndicate(mThis(self), vcMapType);

    return cAtErrorNullPointer;
    }

eAtRet ThaSdhAuVcMapDe3LiuEnable(ThaSdhAuVc self, eBool enable)
    {
    if (self == NULL)
        return cAtFalse;

    self->isMapDe3Liu = enable;

    if (AtChannelAccessible((AtChannel)self))
        return De3LiuEnable(self, enable);

    return cAtOk;
    }

eBool ThaSdhAuVcMapDe3LiuIsEnabled(ThaSdhAuVc self)
    {
    if (self == NULL)
        return cAtFalse;

    if (AtChannelInAccessible((AtChannel)self))
        return self->isMapDe3Liu;

    return De3LiuIsEnabled(self);
    }

eAtRet ThaSdhAuVcLomMonitorEnable(ThaSdhAuVc self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->LomMonitorEnable(self, enable);
    return cAtErrorNullPointer;
    }

eBool ThaSdhAuVcLomMonitorIsEnabled(ThaSdhAuVc self)
    {
    if (self)
        return mMethodsGet(self)->LomMonitorIsEnabled(self);
    return cAtFalse;
    }

uint32 ThaSdhVcOcnVc3PldType(AtSdhChannel vc, eAtSdhVcMapType vc3MapType)
    {
    switch ((uint32)vc3MapType)
        {
        case cAtSdhVcMapTypeNone         : return AtSdhChannelIsHoVc(vc) ? cThaOcnVc3PldC3 : cThaOcnTu3Vc3Pld;
        case cAtSdhVcMapTypeVc3Map7xTug2s: return cThaOcnVc3Pld7Tug2;
        case cAtSdhVcMapTypeVc3MapC3     : return AtSdhChannelIsHoVc(vc) ? cThaOcnVc3PldC3 : cThaOcnTu3Vc3Pld;
        case cAtSdhVcMapTypeVc3MapDe3    : return cThaOcnVc3PldE3;
        default:
            return cInvalidUint32;
        }
    }

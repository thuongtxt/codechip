/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : ThaSdhLineInternal.h
 * 
 * Created Date: Mar 27, 2013
 *
 * Description : SDH Line of Thalassa product
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASDHLINEINTERNAL_H_
#define _THASDHLINEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "atcrc7.h"
#include "../../../generic/sdh/AtSdhLineInternal.h"
#include "../../../generic/man/AtModuleInternal.h"
#include "../../../generic/common/AtChannelInternal.h"
#include "../../../implement/default/man/ThaDeviceInternal.h"
#include "../poh/ThaModulePoh.h"
#include "../man/ThaDeviceInternal.h"

#include "ThaSdhLine.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaSdhLineMethods
    {
    /* SERDES Loopback */
    eAtRet (*SerdesLoopoutEnable)(ThaSdhLine self, eBool enable);
    eBool (*SerdesLoopoutIsEnabled)(ThaSdhLine self);
    eAtRet (*SerdesLoopInEnable)(ThaSdhLine self, eBool enable);
    eBool (*SerdesLoopInIsEnabled)(ThaSdhLine self);

    /* FPGA loopback */
    eBool (*FpgaLoopOutIsApplicable)(ThaSdhLine self);
    eAtRet (*FpgaLoopOutEnable)(ThaSdhLine self, eBool enable);
    eBool (*FpgaLoopOutIsEnable)(ThaSdhLine self);
    eAtRet (*FpgaLoopInEnable)(ThaSdhLine self, eBool enable);
    eBool (*FpgaLoopInIsEnable)(ThaSdhLine self);

    /* Alarms */
    uint32 (*AlarmStickyAddress)(ThaSdhLine self, eBool read2Clear);
    eBool (*StickyRegisterIsRead2Clear)(ThaSdhLine self);

    /* All loopback */
    eAtRet (*LoopInEnable)(ThaSdhLine self, eBool enable);
    eBool (*LoopInIsEnabled)(ThaSdhLine self);
    eAtRet (*LoopOutEnable)(ThaSdhLine self, eBool enable);
    eBool (*LoopOutIsEnabled)(ThaSdhLine self);

    uint32 (*DefaultOffset)(ThaSdhLine self);

    /* Register masks */
    uint32 (*SfLMask)(ThaSdhLine self);
    uint32 (*SfLShift)(ThaSdhLine self);
    uint32 (*SdLMask)(ThaSdhLine self);
    uint32 (*SdLShift)(ThaSdhLine self);
    uint32 (*SfSMask)(ThaSdhLine self);
    uint32 (*SfSShift)(ThaSdhLine self);
    uint32 (*SdSMask)(ThaSdhLine self);
    uint32 (*SdSShift)(ThaSdhLine self);
    uint32 (*S1ChgMask)(ThaSdhLine self);
    uint32 (*S1ChgShift)(ThaSdhLine self);
    uint32 (*K1ChgMask)(ThaSdhLine self);
    uint32 (*K1ChgShift)(ThaSdhLine self);
    uint32 (*K2ChgMask)(ThaSdhLine self);
    uint32 (*K2ChgShift)(ThaSdhLine self);
    uint8 (*CurrentStatusStartBit)(ThaSdhLine self);
    uint32 (*DiagnosticBaseAddress)(ThaSdhLine self);

    eAtSdhLineRate(*LowRate)(ThaSdhLine self);
    eAtSdhLineRate(*HighRate)(ThaSdhLine self);
    eBool (*HasRateHwConfiguration)(ThaSdhLine self);
    eBool (*HasToh)(ThaSdhLine self);
    eAtRet (*TohMonitoringDefaultSet)(ThaSdhLine self);
    eAtRet (*HwRateSet)(ThaSdhLine self, eAtSdhLineRate rate);
    eAtSdhLineRate (*HwRateGet)(ThaSdhLine self);

    uint32 (*HwAlarmGet)(ThaSdhLine self);
    uint32 (*RsRealAlarmGet)(ThaSdhLine self, uint32 alarmRegValue);
    uint32 (*MsRealAlarmGet)(ThaSdhLine self, uint32 alarmRegValue);

    /* Extended K-Byte */
    eAtRet (*ExtendedKByteEnable)(ThaSdhLine self, eBool enable);
    eBool (*ExtendedKByteIsEnabled)(ThaSdhLine self);
    eAtRet (*ExtendedKBytePositionSet)(ThaSdhLine self, eThaSdhLineExtendedKBytePosition position);
    eThaSdhLineExtendedKBytePosition (*ExtendedKBytePositionGet)(ThaSdhLine self);
    eAtRet (*KByteSourceSet)(ThaSdhLine self, eThaSdhLineKByteSource source);
    eThaSdhLineKByteSource (*KByteSourceGet)(ThaSdhLine self);
    eAtRet (*EK1TxSet)(ThaSdhLine self, uint8 value);
    eAtRet (*EK2TxSet)(ThaSdhLine self, uint8 value);
    uint8  (*EK1TxGet)(ThaSdhLine self);
    uint8  (*EK2TxGet)(ThaSdhLine self);
    uint8  (*EK1RxGet)(ThaSdhLine self);
    uint8  (*EK2RxGet)(ThaSdhLine self);
    }tThaSdhLineMethods;

typedef struct tThaSdhLine
    {
    tAtSdhLine super;
    const tThaSdhLineMethods *methods;

    /* Private data */
    ThaTtiProcessor ttiProcessor;

    uint32 b1ErrorCnt;   /* Cache BIP error counter value for main thread */
    uint32 b2ErrorCnt;   /* Cache BIP error counter value for main thread */
    AtSerdesController serdesController;

    /* For APS */
    AtSdhLine switchToLine;
    AtSdhLine switchFromLine;
    AtSdhLine bridgeToLine;
    AtSdhLine bridgeFromLine;

    /* For debugging */
    uint32 latchedAlarms;

    /* For backward compatible */
    uint8 version;

    AtSurEngine surEngine;
    AtPrbsEngine linePrbsEngine;
    /* Private data */
    AtObjectAny attController; /*AtAttController*/
    }tThaSdhLine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhLine ThaSdhLineObjectInit(AtSdhLine self, uint32 channelId, AtModuleSdh module, uint8 version);

uint8 ThaSdhLineSerdesIdGet(ThaSdhLine self);

#ifdef __cplusplus
}
#endif
#endif /* _THASDHLINEINTERNAL_H_ */


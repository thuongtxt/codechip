/*-----------------------------------------------------------------------------
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies. 
 *
 * Module      : SDH
 *
 * File        : ThaSdhAuInternal.h
 *
 * Created Date: Apr 23, 2015
 *
 * Description : AU internal definition
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASDHAUINTERNAL_H_
#define _THASDHAUINTERNAL_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../../generic/sdh/AtSdhAuInternal.h"

#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaSdhAu
    {
    tAtSdhAu super;

    /* Private data */
    uint8 enabled;

    /* HW have two registers to configure SS bit for AU in 2 cases: channelized or unchannelized
     * when channelized, the regiter currently contain ss bit for AU will be used for TU and AU need to use another one,
     * Because of this ambiguous point, SW should save SS bit of AU */
    uint8 ssBit;
    AtObjectAny attController;
    }tThaSdhAu;

typedef tThaSdhAu * ThaSdhAu;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/
AtSdhAu ThaSdhAuObjectInit(AtSdhAu self, uint32 channelId, uint8 channelType, AtModuleSdh module);

#ifdef __cplusplus
}
#endif

#endif /* _THASDHAUINTERNAL_H_ */

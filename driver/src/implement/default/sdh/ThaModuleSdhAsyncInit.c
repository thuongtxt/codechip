/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : ThaModuleSdhAsyncInit.c
 *
 * Created Date: Aug 20, 2016
 *
 * Description : AsyncInit
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtEyeScanController.h"
#include "../../../generic/prbs/AtModulePrbsInternal.h"
#include "../physical/ThaSerdesController.h"
#include "../man/ThaIpCoreInternal.h"
#include "../prbs/ThaPrbsEngineSerdes.h"
#include "ThaModuleSdhInternal.h"
#include "ThaModuleSdhReg.h"
#include "ThaSdhVcInternal.h"
#include "ThaSdhLine.h"
#include "ThaModuleSdhAsyncInit.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaModuleSdh)self)

/*--------------------------- Local typedefs ---------------------------------*/

typedef enum eThaModuleSdhAsyncState
    {
    cThaModuleSdhAsyncSuperInit = 0,
    cThaModuleSdhAsyncLineDefaultSet,
    cThaModuleSdhAsyncPostInit
    }eThaModuleSdhAsyncState;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void AsyncInitStateSet(ThaModuleSdh self, uint32 state)
    {
    self->asyncInitState = state;
    }

static uint32 AsyncInitStateGet(ThaModuleSdh self)
    {
    return self->asyncInitState;
    }

static void AsyncDefaultResetStateSet(ThaModuleSdh self, uint32 state)
    {
    self->asyncLineDefaultInitState = state;
    }

static uint32 AsyncDefaultResetStateGet(ThaModuleSdh self)
    {
    return self->asyncLineDefaultInitState;
    }

eAtRet ThaSdhModuleAsyncLineDefaultMain(ThaModuleSdh self)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)self;
    uint8 numLines = AtModuleSdhMaxLinesGet(sdhModule);
    eAtRet ret = cAtOk;
    uint32 state = AsyncDefaultResetStateGet(self);

    ThaSdhModuleLineDefaultSet(self, state);
    state += 1;
    if (state == numLines)
        {
        ret = cAtOk;
        state = 0;
        }
    else
        ret = cAtErrorAgain;

    AsyncDefaultResetStateSet(self, state);
    return ret;
    }

eAtRet ThaModuleSdhAsyncInitMain(AtModule self)
    {
    eAtRet ret = cAtOk;
    uint32 state = AsyncInitStateGet((ThaModuleSdh)self);

    switch (state)
        {
        case cThaModuleSdhAsyncSuperInit:
            ret = ThaModuleSdhAsyncSuperInit(self);
            AtDeviceAccessTimeCountSinceLastProfileCheck(AtModuleDeviceGet((AtModule)self), cAtTrue, AtSourceLocation, "ThaModuleSdhAsyncSuperInit");
            if (ret == cAtOk)
                {
                state = cThaModuleSdhAsyncLineDefaultSet;
                ret = cAtErrorAgain;
                }
            break;
        case cThaModuleSdhAsyncLineDefaultSet:
            ret = mMethodsGet(mThis(self))->AsyncLineDefaultSet(mThis(self));
            AtDeviceAccessTimeCountSinceLastProfileCheck(AtModuleDeviceGet((AtModule)self), cAtTrue, AtSourceLocation, "AsyncLineDefaultSet");
            if (ret == cAtOk)
                {
                state = cThaModuleSdhAsyncPostInit;
                ret = cAtErrorAgain;
                }
            else if (!AtDeviceAsyncRetValIsInState(ret))
                state = cThaModuleSdhAsyncSuperInit;
            break;
        case cThaModuleSdhAsyncPostInit:
            ThaModuleSdhPostInit(self);
            AtDeviceAccessTimeCountSinceLastProfileCheck(AtModuleDeviceGet((AtModule)self), cAtTrue, AtSourceLocation, "ThaModuleSdhPostInit");
            ret = cAtOk;
            state = cThaModuleSdhAsyncSuperInit;
            break;
        default:
            state = cThaModuleSdhAsyncSuperInit;
            ret = cAtErrorDevFail;
        }

    AsyncInitStateSet((ThaModuleSdh)self, state);
    return ret;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : ThaSdhTu.c
 *
 * Created Date: Sep 7, 2012
 *
 * Description : Thalassa SDH TU default implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../generic/sdh/AtSdhTuInternal.h"
#include "../../../util/coder/AtCoderUtil.h"
#include "../att/ThaAttController.h"
#include "../man/ThaDeviceInternal.h"
#include "ThaModuleSdh.h"
#include "ThaModuleSdhInternal.h"
#include "ThaModuleSdhReg.h"
#include "ThaSdhTuInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaSdhTu)self)

/*--------------------------- Local typedefs ---------------------------------*/
/* TU class */

/*--------------------------- Global variables -------------------------------*/
AtSdhTu ThaSdhTuNew(uint32 channelId, uint8 channelType, AtModuleSdh module);

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtChannelMethods    m_AtChannelOverride;
static tAtSdhPathMethods    m_AtSdhPathOverride;

/* To save super implementation */
static const tAtObjectMethods     *m_AtObjectMethods     = NULL;
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtObjectAny AttController(AtChannel self)
    {
    ThaModuleSdh module = (ThaModuleSdh)AtChannelModuleGet(self);
    if (mThis(self)->attController == NULL)
        {
        eAtSdhChannelType channelType = AtSdhChannelTypeGet((AtSdhChannel)self);
        if (channelType==cAtSdhChannelTypeTu3)
            mThis(self)->attController = mMethodsGet(module)->PathAttControllerCreate(module, self);
        else
            {
            mThis(self)->attController = mMethodsGet(module)->Vc1xAttControllerCreate(module, self);
            }

        if (mThis(self)->attController)
            {
            AtAttControllerSetUp(mThis(self)->attController);
            AtAttControllerInit(mThis(self)->attController);
            }
        }
    return mThis(self)->attController;
    }

static void AttControllerDelete(AtObject self)
    {
    if (mThis(self)->attController)
        AtObjectDelete((AtObject)mThis(self)->attController);

    /* Fully delete this object */
    mThis(self)->attController = NULL;
    }

static void Delete(AtObject self)
    {
    AttControllerDelete(self);
    m_AtObjectMethods->Delete(self);
    }

static uint32 DefaultOffset(AtSdhTu self)
    {
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)self);
    AtSdhChannel sdhChannel = (AtSdhChannel)self;
    uint8 stsId = AtSdhChannelSts1Get(sdhChannel);
    uint8 vtgId = AtSdhChannelTug2Get(sdhChannel);
    uint8 vtId  = AtSdhChannelTu1xGet(sdhChannel);
    return ThaModuleOcnStsVtDefaultOffset(ocnModule, (AtSdhChannel)self, stsId, vtgId, vtId);
    }

static eAtRet Enable(AtChannel self, eBool enable)
    {
    ((ThaSdhTu)self)->enabled = enable;

    return cAtOk;
    }

static eBool IsEnabled(AtChannel self)
    {
    return ((ThaSdhTu)self)->enabled;
    }

static uint32 DefectGet(AtChannel self)
    {
    uint32 regVal, address;
    uint32 alarmState = 0;

    /* Get hardware alarm status */
    address = cThaRegOcnRxVtTUperAlmCurrentStat + DefaultOffset((AtSdhTu)self);
    regVal = mChannelHwRead(self, address, cThaModuleOcn);

    /* Return if AIS or LOP */
    if (regVal & cThaVtPiAisCurStatMask)
        return cAtSdhPathAlarmAis;

    if (regVal & cThaVtPiLopCurStatMask)
        return cAtSdhPathAlarmLop;

    /* POH alarm get (need to mask VC-AIS) */
    alarmState |= AtChannelDefectGet((AtChannel)AtSdhChannelSubChannelGet((AtSdhChannel)self, 0)) & (uint32)(~cAtSdhPathAlarmAis);

    return alarmState;
    }

static uint32 InterruptStatusRead2Clear(AtChannel self, eBool read2Clear)
    {
    uint32 regVal, address;
    uint32 alarmState = 0;
    AtSdhChannel vc;

    /* Get hardware alarm status */
    address = cThaRegOcnRxVtTUperAlmIntrStat + DefaultOffset((AtSdhTu)self);
    regVal = mChannelHwRead(self, address, cThaModuleOcn);

    /* Convert to hardware */
    if (regVal & cThaVtPiAisStateChgIntrMask)
        alarmState |= cAtSdhPathAlarmAis;
    if (regVal & cThaVtPiLopStateChgIntrMask)
        alarmState |= cAtSdhPathAlarmLop;

    /* Clear */
    if (read2Clear)
        mChannelHwWrite(self, address, (cThaVtPiAisStateChgIntrMask | cThaVtPiLopStateChgIntrMask), cThaModuleOcn);

    /* POH alarm (need to mask VC-AIS) */
    vc = AtSdhChannelSubChannelGet((AtSdhChannel)self, 0);
    if (read2Clear)
        alarmState |= AtChannelDefectInterruptClear((AtChannel)vc) & (uint32)(~cAtSdhPathAlarmAis);
    else
        alarmState |= AtChannelDefectInterruptGet((AtChannel)vc) & (uint32)(~cAtSdhPathAlarmAis);

    return alarmState;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    return InterruptStatusRead2Clear(self, cAtFalse);
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    return InterruptStatusRead2Clear(self, cAtTrue);
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
    AtUnused(self);
    return cAtSdhPathAlarmAis | cAtSdhPathAlarmLop;
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    uint32 regVal, address;

    /* Get hardware configuration */
    address = cThaRegOcnRxVtTUperAlmIntrEnCtrl + DefaultOffset((AtSdhTu)self);
    regVal = mChannelHwRead(self, address, cThaModuleOcn);

    /* Configure interrupt mask */
    if (defectMask & cAtSdhPathAlarmAis)
        mFieldIns(&regVal, cThaVtPiAisStateChgIntrEnMask, cThaVtPiAisStateChgIntrEnShift, (enableMask & cAtSdhPathAlarmAis) ? 1 : 0);
    if (defectMask & cAtSdhPathAlarmLop)
        mFieldIns(&regVal, cThaVtPiLopStateChgIntrEnMask, cThaVtPiLopStateChgIntrEnShift, (enableMask & cAtSdhPathAlarmLop) ? 1 : 0);

    /* Apply */
    mChannelHwWrite(self, address, regVal, cThaModuleOcn);

    return cAtOk;
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    uint32 address, regVal;
    uint32 mask = 0;

    /* Get hardware configuration */
    address = cThaRegOcnRxVtTUperAlmIntrEnCtrl + DefaultOffset((AtSdhTu)self);
    regVal = mChannelHwRead(self, address, cThaModuleOcn);

    /* Return software mask */
    if (regVal & cThaVtPiAisStateChgIntrEnMask)
        mask |= cAtSdhPathAlarmAis;
    if (regVal & cThaVtPiLopStateChgIntrEnMask)
        mask |= cAtSdhPathAlarmLop;

    return mask;
    }

static uint32 TxForcibleAlarmsGet(AtChannel self)
    {
    return ThaOcnVtTuTxForcibleAlarms(ThaModuleOcnFromChannel((AtChannel)self));
    }

static eAtRet CacheForcedTxAlarm(AtChannel self, uint32 alarmType, eBool forced)
    {
    if (alarmType & cAtSdhPathAlarmAis)
        mThis(self)->txAisExplicitlyForced = forced;
    return cAtOk;
    }

static eAtRet TxAlarmForce(AtChannel self, uint32 alarmType)
    {
    eAtRet ret = cAtOk;

    ret |= ThaOcnVtTuTxAlarmForce(self, alarmType);
    ret |= CacheForcedTxAlarm(self, alarmType, cAtTrue);

    return ret;
    }

static eAtRet TxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    eAtRet ret = cAtOk;

    ret |= ThaOcnVtTuTxAlarmUnForce(self, alarmType);
    ret |= CacheForcedTxAlarm(self, alarmType, cAtFalse);

    return ret;
    }

static uint32 TxForcedAlarmGet(AtChannel self)
    {
    return ThaOcnVtTuTxForcedAlarmGet(self);
    }

static uint32 RxForcibleAlarmsGet(AtChannel self)
    {
    return ThaOcnVtTuRxForcibleAlarms(ThaModuleOcnFromChannel((AtChannel)self));
    }

static void AllAlarmsUnforce(AtChannel self)
    {
    AtChannelTxAlarmUnForce(self, AtChannelTxForcableAlarmsGet(self));
    AtChannelRxAlarmUnForce(self, AtChannelRxForcableAlarmsGet(self));
    }

static eAtModuleSdhRet TxSsSet(AtSdhPath self, uint8 value)
    {
    return ThaOcnPpSsTxVtSet((AtSdhChannel)self, value, cAtTrue);
    }

static uint8 TxSsGet(AtSdhPath self)
    {
    return ThaOcnPpSsTxVtGet((AtSdhChannel)self);
    }

static eAtModuleSdhRet ExpectedSsSet(AtSdhPath self, uint8 value)
    {
    return ThaOcnVtTuExpectedSsSet(self, value);
    }

static uint8 ExpectedSsGet(AtSdhPath self)
    {
    return ThaOcnVtTuExpectedSsGet(self);
    }

static eAtModuleSdhRet SsCompareEnable(AtSdhPath self, eBool enable)
    {
    return ThaOcnVtTuSsCompareEnable(self, enable);
    }

static eBool SsCompareIsEnabled(AtSdhPath self)
    {
    return ThaOcnVtTuSsCompareIsEnabled(self);
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret;

    ret = m_AtChannelMethods->Init(self);
    if (ret == cAtOk)
        AllAlarmsUnforce(self);

    return ret;
    }

static eAtRet Debug(AtChannel self)
    {
    m_AtChannelMethods->Debug(self);
    return ThaSdhChannelCommonRegisterOffsetsDisplay((AtSdhChannel)self);
    }

static eAtRet RxAlarmForce(AtChannel self, uint32 alarmType)
    {
    return ThaOcnVtTuRxAlarmForce(self, alarmType);
    }

static eAtRet RxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    return ThaOcnVtTuRxAlarmUnForce(self, alarmType);
    }

static uint32 RxForcedAlarmGet(AtChannel self)
    {
    return ThaOcnVtTuRxForcedAlarmGet(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaSdhTu object = (ThaSdhTu)self;
    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeUInt(enabled);
    mEncodeUInt(txAisExplicitlyForced);
    mEncodeNone(attController);
    }

static eAtRet WarmRestore(AtChannel self)
    {
    ThaSdhChannelRestoreAllHwSts((AtSdhChannel)self);
    return m_AtChannelMethods->WarmRestore(self);
    }

static eAtRet TxSquelch(AtChannel self, eBool squelched)
    {
    if (squelched)
        return ThaOcnVtTuTxAlarmForce(self, cAtSdhPathAlarmAis);

    if (mThis(self)->txAisExplicitlyForced)
        return ThaOcnVtTuTxAlarmForce(self, cAtSdhPathAlarmAis);

    return ThaOcnVtTuTxAlarmUnForce(self, cAtSdhPathAlarmAis);
    }

static void OverrideAtObject(AtSdhTu self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtSdhTu self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, IsEnabled);
        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, TxAlarmForce);
        mMethodOverride(m_AtChannelOverride, TxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcedAlarmGet);
        mMethodOverride(m_AtChannelOverride, TxForcibleAlarmsGet);
        mMethodOverride(m_AtChannelOverride, RxAlarmForce);
        mMethodOverride(m_AtChannelOverride, RxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, RxForcedAlarmGet);
        mMethodOverride(m_AtChannelOverride, RxForcibleAlarmsGet);
        mMethodOverride(m_AtChannelOverride, Debug);
        mMethodOverride(m_AtChannelOverride, WarmRestore);
        mMethodOverride(m_AtChannelOverride, AttController);
        mMethodOverride(m_AtChannelOverride, TxSquelch);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtSdhPath(AtSdhTu self)
    {
    AtSdhPath path = (AtSdhPath)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhPathOverride, mMethodsGet(path), sizeof(m_AtSdhPathOverride));
        mMethodOverride(m_AtSdhPathOverride, TxSsSet);
        mMethodOverride(m_AtSdhPathOverride, TxSsGet);
        mMethodOverride(m_AtSdhPathOverride, ExpectedSsSet);
        mMethodOverride(m_AtSdhPathOverride, ExpectedSsGet);
        mMethodOverride(m_AtSdhPathOverride, SsCompareEnable);
        mMethodOverride(m_AtSdhPathOverride, SsCompareIsEnabled);
        }

    mMethodsSet(path, &m_AtSdhPathOverride);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaSdhTu);
    }

static void Override(AtSdhTu self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtSdhPath(self);
    }

AtSdhTu ThaSdhTuObjectInit(AtSdhTu self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtSdhTuObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhTu ThaSdhTuNew(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhTu newTu = mMethodsGet(osal)->MemAlloc(osal, sizeof(tThaSdhTu));
    if (newTu == NULL)
        return NULL;

    /* Construct it */
    return ThaSdhTuObjectInit(newTu, channelId, channelType, module);
    }

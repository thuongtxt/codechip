/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : ThaSdhAug.c
 *
 * Created Date: Sep 7, 2012
 *
 * Description : Thalassa AUG default implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSdhPath.h"
#include "AtSdhVc.h"
#include "../../../generic/sdh/AtSdhPathInternal.h"
#include "../ocn/ThaModuleOcn.h"
#include "../pdh/ThaModulePdh.h"
#include "../map/ThaModuleStmMap.h"
#include "../man/ThaDeviceInternal.h"
#include "ThaModuleSdh.h"
#include "../pdh/ThaStmModulePdh.h"
#include "ThaSdhAugInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaSdhAug)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaSdhAugMethods m_methods;

/* Override */
static tAtSdhChannelMethods m_AtSdhChannelOverride;
static tAtChannelMethods    m_AtChannelOverride;

/* Save super implementation */
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;
static const tAtSdhChannelMethods *m_AtSdhChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePdh PdhModule(AtSdhChannel self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaModulePdh)AtDeviceModuleGet(device, cAtModulePdh);
    }

static ThaModuleAbstractMap MapModule(AtSdhChannel self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap);
    }

static eAtRet SubChannelsInit(AtSdhChannel self)
    {
    uint8 numSubChannel = AtSdhChannelNumberOfSubChannelsGet(self);
    uint8 subChannel_i;
    eAtRet ret = cAtOk;

    for (subChannel_i = 0; subChannel_i < numSubChannel; subChannel_i++)
        ret |= AtChannelInit((AtChannel)AtSdhChannelSubChannelGet(self, subChannel_i));

    return ret;
    }

static eAtRet AugSts1sInit(AtSdhChannel self)
    {
    uint8 startSts1 = AtSdhChannelSts1Get(self);
    uint8 numberOfSts1s = AtSdhChannelNumSts(self);
    uint8 i;
    eAtRet ret = cAtOk;
    ThaModulePdh modulePdh = PdhModule(self);
    ThaSdhAug aug = mThis(self);

    for (i = 0; i < numberOfSts1s; i++)
        {
        uint8 stsId = (uint8)(startSts1 + i);

        if (mMethodsGet(aug)->NeedOcnConcate(aug))
            ret |= ThaOcnSts1Init(self, stsId);

        if (mMethodsGet(aug)->NeedPdhConcate(aug))
            {
            ret |= ThaPdhStsVtDemapConcatRelease((AtModulePdh)modulePdh, self, stsId);
            ThaModulePdhVc4_4cConfigurationClear(modulePdh, self, stsId);
            }

        if (mMethodsGet(aug)->NeedMapConcate(aug))
            ThaModuleStmMapVc4_4cConfigurationClear(MapModule(self), self, stsId);
        }

    return ret;
    }

static eAtRet Aug64MapTypeSet(AtSdhChannel self, uint8 mapType)
    {
    eAtRet ret;
    static const uint8 cNumSts1sInAug64 = 192;
    AtSdhPath au4_64c;
    AtSdhChannel vc4_64c;
    ThaSdhAug aug = (ThaSdhAug)self;

    if (AtChannelDeviceInWarmRestore((AtChannel)self))
        return cAtOk;

    ret = AugSts1sInit(self);
    if (ret != cAtOk)
        return ret;

    ret = SubChannelsInit(self);
    if (ret != cAtOk)
        return ret;

    /* Map 4xAUG-16s, hardware does not need to be configured */
    if (mapType == cAtSdhAugMapTypeAug64Map4xAug16s)
        return cAtOk;

    /* Invalid mapping type */
    if (mapType != cAtSdhAugMapTypeAug64MapVc4_64c)
        return cAtErrorInvlParm;

    /* Map Vc4_64c by concatenate 48 STSs and initialize cross-connect for them */
    if (mMethodsGet(aug)->NeedOcnConcate(aug))
        ret = ThaOcnStsConcat(self, AtSdhChannelSts1Get(self), cNumSts1sInAug64);

    /* Set default Signal label to 0 because of traffic is not determined */
    au4_64c = (AtSdhPath)AtSdhChannelSubChannelGet(self, 0);
    if (AtSdhPathHasPohProcessor(au4_64c))
        {
        ret |= AtSdhPathExpectedPslSet(au4_64c, 0x1);
        ret |= AtSdhPathTxPslSet(au4_64c, 0x1);
        }

    vc4_64c = AtSdhChannelSubChannelGet((AtSdhChannel)au4_64c, 0);
    ret |= AtChannelInit((AtChannel)vc4_64c);
    ret |= mMethodsGet(vc4_64c)->MapTypeSet(vc4_64c, cAtSdhVcMapTypeVc4_64cMapC4_64c);

    return ret;
    }

static eAtRet Aug16MapTypeSet(AtSdhChannel self, uint8 mapType)
    {
    eAtRet ret;
    static const uint8 cNumSts1sInAug16 = 48;
    AtSdhPath au4_16c;
    AtSdhChannel vc4_16c;
    ThaSdhAug aug = (ThaSdhAug)self;

    if (AtChannelDeviceInWarmRestore((AtChannel)self))
        return cAtOk;

    ret = AugSts1sInit(self);
    if (ret != cAtOk)
        return ret;

    ret = SubChannelsInit(self);
    if (ret != cAtOk)
        return ret;

    /* Map 4xAUG-4s, hardware does not need to be configured */
    if (mapType == cAtSdhAugMapTypeAug16Map4xAug4s)
        return cAtOk;

    /* Invalid mapping type */
    if (mapType != cAtSdhAugMapTypeAug16MapVc4_16c)
        return cAtErrorInvlParm;

    /* Map Vc4_16c by concatenate 48 STSs and initialize cross-connect for them */
    if (mMethodsGet(aug)->NeedOcnConcate(aug))
        ret = ThaOcnStsConcat(self, AtSdhChannelSts1Get(self), cNumSts1sInAug16);

    /* Set default Signal label to 0 because of traffic is not determined */
    au4_16c = (AtSdhPath)AtSdhChannelSubChannelGet(self, 0);
    if (AtSdhPathHasPohProcessor(au4_16c))
        {
        ret |= AtSdhPathExpectedPslSet(au4_16c, 0x1);
        ret |= AtSdhPathTxPslSet(au4_16c, 0x1);
        }

    vc4_16c = AtSdhChannelSubChannelGet((AtSdhChannel)au4_16c, 0);
    ret |= AtChannelInit((AtChannel)vc4_16c);
    ret |= mMethodsGet(vc4_16c)->MapTypeSet(vc4_16c, cAtSdhVcMapTypeVc4_16cMapC4_16c);

    return ret;
    }

static eAtRet Aug4MapTypeSet(AtSdhChannel self, uint8 mapType)
    {
    eAtRet ret = cAtOk;
    static const uint8 cNumSts1sInAug4 = 12;
    AtSdhPath au4_4c;
    AtSdhChannel vc4_4c;
    AtModulePdh modulePdh = (AtModulePdh)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cAtModulePdh);

    if (AtChannelDeviceInWarmRestore((AtChannel)self))
        return cAtOk;

    /* Initialize all STS-1s belong to this AUG */
    ret = AugSts1sInit(self);
    if (ret != cAtOk)
        return ret;

    ret = SubChannelsInit(self);
    if (ret != cAtOk)
        return ret;

    /* Map 4xAUG-1s, hardware does not need to be configured */
    if (mapType == cAtSdhAugMapTypeAug4Map4xAug1s)
        return cAtOk;

    /* Invalid mapping type */
    if (mapType != cAtSdhAugMapTypeAug4MapVc4_4c)
        return cAtErrorInvlParm;

    /* Map VC4-4c by concatenate 12 STSs and initialize cross-connect for them */
    au4_4c = (AtSdhPath)AtSdhChannelSubChannelGet(self, 0);
    if (mMethodsGet(mThis(self))->NeedOcnConcate(mThis(self)))
        ret = ThaOcnStsConcat(self, AtSdhChannelSts1Get(self), cNumSts1sInAug4);
    if (mMethodsGet(mThis(self))->NeedPdhConcate(mThis(self)))
        ret |= ThaPdhStsVtDemapConcatSet(modulePdh, self, AtSdhChannelSts1Get(self), cNumSts1sInAug4);

    /* Set default Signal label to 0x1 because of traffic is not determined */
    if (AtSdhPathHasPohProcessor(au4_4c))
        {
        ret |= AtSdhPathExpectedPslSet(au4_4c, 0x1);
        ret |= AtSdhPathTxPslSet(au4_4c, 0x1);
        }

    vc4_4c = AtSdhChannelSubChannelGet((AtSdhChannel)au4_4c, 0);
    ret |= AtChannelInit((AtChannel)vc4_4c);
    ret |= mMethodsGet(vc4_4c)->MapTypeSet(vc4_4c, cAtSdhVcMapTypeVc4_4cMapC4_4c);

    return ret;
    }

static eBool IsConcatenated(AtSdhChannel self)
    {
    ThaSdhAug aug = mThis(self);

    if (mMethodsGet(aug)->NeedOcnConcate(aug))
        return mMethodsGet(aug)->OcnIsConcatenated(aug);
    if (mMethodsGet(aug)->NeedPdhConcate(aug))
        return mMethodsGet(aug)->PdhIsConcatenated(aug);
    if (mMethodsGet(aug)->NeedMapConcate(aug))
        return mMethodsGet(aug)->MapIsConcatenated(aug);
    return cAtFalse;
    }

static uint8 Aug1HwMapTypeGet(AtSdhChannel self)
    {
    if (IsConcatenated(self))
        return cAtSdhAugMapTypeAug1MapVc4;

    return cAtSdhAugMapTypeAug1Map3xVc3s;
    }

static eBool OcnIsConcatenated(ThaSdhAug self)
    {
    return ThaOcnChannelIsConcatenated((AtSdhChannel)self);
    }

static eBool PdhIsConcatenated(ThaSdhAug self)
    {
    return ThaModulePdhSdhChannelIsConcatenated((AtSdhChannel)self);
    }

static eBool MapIsConcatenated(ThaSdhAug self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint8 Aug64HwMapTypeGet(AtSdhChannel self)
    {
    if (IsConcatenated(self))
        return cAtSdhAugMapTypeAug64MapVc4_64c;
    return cAtSdhAugMapTypeAug64Map4xAug16s;
    }

static uint8 Aug16HwMapTypeGet(AtSdhChannel self)
    {
    if (IsConcatenated(self))
        return cAtSdhAugMapTypeAug16MapVc4_16c;

    return cAtSdhAugMapTypeAug16Map4xAug4s;
    }

static uint8 Aug4HwMapTypeGet(AtSdhChannel self)
    {
    if (IsConcatenated(self))
        return cAtSdhAugMapTypeAug4MapVc4_4c;

    return cAtSdhAugMapTypeAug4Map4xAug1s;
    }

static eAtSdhVcMapType DefaultVc4MapType(ThaSdhAug self)
    {
    AtUnused(self);
    return cAtSdhVcMapTypeVc4MapC4;
    }

static eAtSdhVcMapType DefaultVc3MapType(ThaSdhAug self)
    {
    AtUnused(self);
    return cAtSdhVcMapTypeVc3MapC3;
    }

static eAtRet Aug1MapTypeSet(AtSdhChannel self, uint8 mapType)
    {
    eAtRet ret = cAtOk;
    static const uint8 cNumSts1sInAug1 = 3;
    AtModulePdh modulePdh = (AtModulePdh)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cAtModulePdh);
    eBool isSimulated = cAtFalse;

    if (AtChannelDeviceInWarmRestore((AtChannel)self))
        return cAtOk;

    /* Initialize all STS-1s belong to this AUG */
    ret = AugSts1sInit(self);
    if (ret != cAtOk)
        return ret;

    /* Map VC-4 by concatenate these 3 STSs and initialize cross-connect */
    isSimulated = AtDeviceIsSimulated(AtChannelDeviceGet((AtChannel)self));
    if (mapType == cAtSdhAugMapTypeAug1MapVc4)
        {
        AtSdhChannel au = AtSdhChannelSubChannelGet(self, 0);
        AtSdhChannel vc = AtSdhChannelSubChannelGet(au, 0);

        AtSdhPathPohMonitorEnable((AtSdhPath)vc, cAtFalse);

        if (mMethodsGet(mThis(self))->NeedOcnConcate(mThis(self)))
            ret |= ThaOcnStsConcat(self, AtSdhChannelSts1Get(self), cNumSts1sInAug1);
        if (mMethodsGet(mThis(self))->NeedPdhConcate(mThis(self)))
            ret |= ThaPdhStsVtDemapConcatSet(modulePdh, self, AtSdhChannelSts1Get(self), cNumSts1sInAug1);

        ret |= AtChannelInit((AtChannel)au);
        if (ret == cAtOk)
            {
            if (vc == NULL)
                return cAtErrorNullPointer;
            ret = mMethodsGet(vc)->MapTypeSet(vc, mMethodsGet(mThis(self))->DefaultVc4MapType(mThis(self)));
            }

        /* Give HW a time to search frame */
        if (!isSimulated)
            AtOsalUSleep(1000);
        AtSdhPathPohMonitorEnable((AtSdhPath)vc, cAtTrue);
        }

    /* Initialize AU-3s */
    if (mapType == cAtSdhAugMapTypeAug1Map3xVc3s)
        {
        uint8 i;
        for (i = 0; i < AtSdhChannelNumberOfSubChannelsGet(self); i++)
            {
            AtSdhChannel au = AtSdhChannelSubChannelGet(self, i);
            AtSdhChannel vc = AtSdhChannelSubChannelGet(au, 0);

            AtSdhPathPohMonitorEnable((AtSdhPath)vc, cAtFalse);

            ret |= AtChannelInit((AtChannel)au);
            if (ret == cAtOk)
                {
                if (vc == NULL)
                    return cAtErrorNullPointer;
                ret = mMethodsGet(vc)->MapTypeSet(vc, mMethodsGet(mThis(self))->DefaultVc3MapType(mThis(self)));
                }
            }

        /* Give HW a time to search frame */
        if (!isSimulated)
            AtOsalUSleep(1000);
        for (i = 0; i < AtSdhChannelNumberOfSubChannelsGet(self); i++)
            {
            AtSdhPath au = (AtSdhPath)AtSdhChannelSubChannelGet(self, i);
            AtSdhPathPohMonitorEnable(au, cAtTrue);
            }
        }

    /* Restore when change mapping if required */
    if (AtSdhAug1NeedToRestoreSubMapping((AtSdhAug)self))
        ret |= HelperAug1SubMappingRestore(self);

    return ret;
    }

static eAtModuleSdhRet MapTypeSet(AtSdhChannel self, uint8 mapType)
    {
    eAtRet ret;
    eAtSdhChannelType augType;

    if (!AtSdhChannelNeedToUpdateMapType(self, mapType))
        return cAtOk;

    if (!AtSdhChannelMapTypeIsSupported(self, mapType))
        return cAtErrorModeNotSupport;

    ret = AtSdhChannelCanChangeMapping(self, mapType);
    if (ret != cAtOk)
        return ret;

    /* Call super */
    ret = m_AtSdhChannelMethods->MapTypeSet(self, mapType);
    if (ret != cAtOk)
        return ret;

    augType = AtSdhChannelTypeGet(self);

    /* Set mapping for AUG-64 */
    if (augType == cAtSdhChannelTypeAug64)
       return Aug64MapTypeSet(self, mapType);

    /* Set mapping for AUG-16 */
    if (augType == cAtSdhChannelTypeAug16)
       return Aug16MapTypeSet(self, mapType);

    /* Set mapping for AUG-4 */
    if (augType == cAtSdhChannelTypeAug4)
       return Aug4MapTypeSet(self, mapType);

    /* Set mapping for AUG-1 */
    if (augType == cAtSdhChannelTypeAug1)
       return Aug1MapTypeSet(self, mapType);

    return cAtErrorInvlParm;
    }

static eAtRet Debug(AtChannel self)
    {
    m_AtChannelMethods->Debug(self);
    ThaModuleSdhStsMappingDisplay((AtSdhChannel)self);
    return ThaSdhChannelCommonRegisterOffsetsDisplay((AtSdhChannel)self);
    }

static uint8 HwMapTypeGet(AtSdhChannel self)
    {
    eAtSdhChannelType augType = AtSdhChannelTypeGet(self);
    if (augType == cAtSdhChannelTypeAug64)
        return Aug64HwMapTypeGet(self);

    if (augType == cAtSdhChannelTypeAug16)
        return Aug16HwMapTypeGet(self);

    if (augType == cAtSdhChannelTypeAug4)
        return Aug4HwMapTypeGet(self);

    if (augType == cAtSdhChannelTypeAug1)
        return Aug1HwMapTypeGet(self);

    return cAtSdhAugMapTypeNone;
    }

static eBool NeedOcnConcate(ThaSdhAug self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool NeedPdhConcate(ThaSdhAug self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool NeedMapConcate(ThaSdhAug self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint8 AugId(AtSdhChannel self, eAtSdhChannelType channelType)
    {
    const uint8 cNumAugInParent = 4;
    uint8 localAugId = (uint8)AtChannelIdGet((AtChannel)self);
    AtSdhChannel aug16, aug4;

    if (channelType == cAtSdhChannelTypeAug4)
        {
        aug16 = AtSdhChannelParentChannelGet(self);
        if (AtSdhChannelTypeGet(aug16) == cAtSdhChannelTypeAug16)
            return (uint8)(AtChannelIdGet((AtChannel)aug16) * cNumAugInParent + localAugId + 1);
        else
            return (uint8)(localAugId + 1);
        }

    aug4 = AtSdhChannelParentChannelGet(self);
    if (AtSdhChannelTypeGet(aug4) != cAtSdhChannelTypeAug4)
        return (uint8)(localAugId + 1);

    aug16 = AtSdhChannelParentChannelGet(aug4);
    if (AtSdhChannelTypeGet(aug16) != cAtSdhChannelTypeAug16)
        return (uint8)(cNumAugInParent * AtChannelIdGet((AtChannel)aug4) + localAugId + 1);

    return (uint8)(cNumAugInParent * ((AtChannelIdGet((AtChannel)aug16) * cNumAugInParent) + AtChannelIdGet((AtChannel)aug4)) + localAugId + 1);
    }

static const char *IdString(AtChannel self)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)self;
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(sdhChannel);

    /* Just need to convert ID string of AUG4 and AUG1 */
    if ((channelType == cAtSdhChannelTypeAug1) || (channelType == cAtSdhChannelTypeAug4))
        {
        static char idString[16];
        AtSprintf(idString, "%d.%d", AtSdhChannelLineGet(sdhChannel) + 1, AugId(sdhChannel, channelType));
        return idString;
        }

    return m_AtChannelMethods->IdString(self);
    }

static void OverrideAtChannel(AtSdhAug self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, Debug);
        mMethodOverride(m_AtChannelOverride, IdString);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtSdhChannel(AtSdhAug self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(tAtSdhChannelMethods));

        mMethodOverride(m_AtSdhChannelOverride, MapTypeSet);
        mMethodOverride(m_AtSdhChannelOverride, HwMapTypeGet);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void Override(AtSdhAug self)
    {
    OverrideAtChannel(self);
    OverrideAtSdhChannel(self);
    }

static void MethodsInit(ThaSdhAug self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, NeedOcnConcate);
        mMethodOverride(m_methods, NeedPdhConcate);
        mMethodOverride(m_methods, NeedMapConcate);
        mMethodOverride(m_methods, OcnIsConcatenated);
        mMethodOverride(m_methods, PdhIsConcatenated);
        mMethodOverride(m_methods, MapIsConcatenated);
        mMethodOverride(m_methods, DefaultVc4MapType);
        mMethodOverride(m_methods, DefaultVc3MapType);
        }

    mMethodsSet(self, &m_methods);
    }

AtSdhAug ThaSdhAugObjectInit(AtSdhAug self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaSdhAug));

    /* Super constructor */
    if (AtSdhAugObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Override */
    Override(self);
    MethodsInit((ThaSdhAug)self);

    /* Only initialize method structures one time */
    m_methodsInit = 1;

    return self;
    }

AtSdhAug ThaSdhAugNew(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhAug newAug = mMethodsGet(osal)->MemAlloc(osal, sizeof(tThaSdhAug));
    if (newAug == NULL)
        return NULL;

    /* Construct it */
    return ThaSdhAugObjectInit(newAug, channelId, channelType, module);
    }

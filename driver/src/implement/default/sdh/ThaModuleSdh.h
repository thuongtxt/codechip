/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH Module
 * 
 * File        : ThaModuleSdh.h
 * 
 * Created Date: Nov 15, 2012
 *
 * Description : Functions to work with other modules
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULESDH_H_
#define _THAMODULESDH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleSdh.h" /* Super class */
#include "AtSerdesController.h"
#include "AtSdhChannel.h"
#include "AtSdhLine.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cThaSdhMaxNumStsPerLine 48

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleSdh * ThaModuleSdh;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Concrete modules */
AtModuleSdh ThaModuleSdhNew(AtDevice device);
AtModuleSdh ThaModuleSdhV1New(AtDevice device);
AtModuleSdh ThaStmMlpppModuleSdhNew(AtDevice device);

/* Channels */
AtSdhAu ThaSdhAuNew(uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhAug ThaSdhAugNew(uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhTug ThaSdhTugNew(uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhTu ThaSdhTuNew(uint32 channelId, uint8 channelType, AtModuleSdh module);

AtSdhLine ThaSdhLineFromChannel(AtSdhChannel channel);
eBool ThaModuleSdhPayloadUneqIsApplicable(ThaModuleSdh self);
eBool ThaModuleSdhErdiIsEnabledByDefault(ThaModuleSdh self);
eBool ThaModuleSdhVcAisIsEnabledByDefault(ThaModuleSdh self);
eBool ThaModuleSdhBlockErrorCountersSupported(ThaModuleSdh self);
eBool ThaModuleSdhHas24BitsBlockErrorCounter(ThaModuleSdh self);
uint32 ThaSdhChannelCounterFromReadOnlyCounterGet(AtChannel self,
                                                  uint32 newCounterValue,
                                                  uint32 *lastValue,
                                                  uint32 maxValue,
                                                  uint32 counterType,
                                                  eBool read2clear);

uint8 ThaModuleSdhNumLinesPerPart(AtModuleSdh self);
uint8 ThaModuleSdhPartOfChannel(AtSdhChannel channel);
uint8 ThaModuleSdhLineLocalId(AtSdhLine line);
uint8 ThaModuleSdhLineIdLocalId(ThaModuleSdh self, uint8 lineId);
eBool ThaModuleSdhAlwaysDisableRdiBackwardWhenTimNotDownstreamAis(ThaModuleSdh self);
eBool ThaModuleSdhShouldUpdateTimRdiBackwardWhenAisDownstreamChange(ThaModuleSdh self);
eBool ThaModuleSdhShouldUpdatePlmMonitorWhenExpectedPslChange(ThaModuleSdh self);
eBool ThaModuleSdhCanDisablePwPacketsToPsn(ThaModuleSdh self);
eBool ThaModuleSdhUseStaticXc(ThaModuleSdh self);
uint8 ThaSdhHwChannelMode(eAtSdhChannelMode mode);
eAtSdhChannelMode ThaSdhSwChannelMode(uint8 hwMode);
void ThaModuleSdhDebugCounterModuleShow(ThaModuleSdh self);

AtSerdesController ThaModuleSdhSerdesControllerCreate(ThaModuleSdh self, AtSdhLine line, uint32 serdesId);
uint8 ThaModuleSdhSerdesCoreIdOfLine(ThaModuleSdh self, AtSdhLine line);
uint8 ThaModuleSdhSerdesIdOfLine(ThaModuleSdh self, AtSdhLine line);

AtPrbsEngine ThaModuleSdhLineSerdesPrbsEngineCreate(ThaModuleSdh self, AtSdhLine line, AtSerdesController serdes);
eBool ThaModuleSdhBerHardwareInterruptIsSupported(ThaModuleSdh self);
eAtSerdesTimingMode ThaModuleSdhDefaultSerdesTimingModeForLine(ThaModuleSdh self, AtSdhLine line);
eBool ThaModuleSdhShouldRemoveLoopbackOnBinding(ThaModuleSdh self);

/* Default mapping */
uint8 ThaModuleSdhDefaultVc1xSubMapping(ThaModuleSdh self);

/* Utils */
eAtRet ThaSdhChannelCommonRegisterOffsetsDisplay(AtSdhChannel self);
AtSdhChannel ThaSdhVcForPhysicalChannel(AtChannel physicalChannel);
uint32 ThaModuleSdhAutoMsRdiAlarms(ThaModuleSdh self);
void ThaModuleSdhStsMappingDisplay(AtSdhChannel self);
void ThaSdhChannelRestoreAllHwSts(AtSdhChannel self);

/* These functions are used in interrupt processing to parser hardware interrupt ID to SDH objects.
 * Using them in other purposes should be carefully considered. */
AtSdhLine ThaModuleSdhLineFromHwIdGet(ThaModuleSdh self, eAtModule phyModule, uint8 sliceId, uint8 localLineInSlice);
AtSdhPath ThaModuleSdhAuPathFromHwIdGet(ThaModuleSdh self, eAtModule phyModule, uint8 sliceId, uint8 stsId);
AtSdhPath ThaModuleSdhTuPathFromHwIdGet(ThaModuleSdh self, eAtModule phyModule, uint8 sliceId, uint8 stsId, uint8 vtgId, uint8 vtId);

AtPrbsEngine ThaModuleSdhVcPrbsEngineCreate(ThaModuleSdh self, AtSdhVc vc);

/* Product concretes */
AtModuleSdh Tha60031031CommonModuleSdhNew(AtDevice device);
AtModuleSdh Tha60001031ModuleSdhNew(AtDevice device);
AtModuleSdh Tha60030080ModuleSdhNew(AtDevice device);
AtModuleSdh Tha60030081ModuleSdhNew(AtDevice device);
AtModuleSdh Tha60150011ModuleSdhNew(AtDevice device);
AtModuleSdh Tha60031031EpModuleSdhNew(AtDevice device);
AtModuleSdh Tha60031031ModuleSdhNew(AtDevice device);
AtModuleSdh Tha60031032ModuleSdhNew(AtDevice device);
AtModuleSdh Tha60031035ModuleSdhNew(AtDevice device);
AtModuleSdh Tha60091135ModuleSdhNew(AtDevice device);
AtModuleSdh Tha60091132ModuleSdhNew(AtDevice device);
AtModuleSdh Tha60070061ModuleSdhNew(AtDevice device);
AtModuleSdh Tha60200011ModuleSdhNew(AtDevice device);
AtModuleSdh Tha61031031ModuleSdhNew(AtDevice device);
AtModuleSdh Tha60035021ModuleSdhNew(AtDevice device);
AtModuleSdh Tha60030051ModuleSdhNew(AtDevice device);
AtModuleSdh Tha60060011ModuleSdhNew(AtDevice device);
AtModuleSdh Tha60070023ModuleSdhNew(AtDevice device);
AtModuleSdh Tha60071032ModuleSdhNew(AtDevice device);
AtModuleSdh Tha60030101ModuleSdhNew(AtDevice device);
AtModuleSdh Tha60210021ModuleSdhNew(AtDevice device);
AtModuleSdh Tha60210011ModuleSdhNew(AtDevice device);
AtModuleSdh Tha61210011ModuleSdhNew(AtDevice device);
AtModuleSdh Tha60210031ModuleSdhNew(AtDevice device);
AtModuleSdh Tha61210031ModuleSdhNew(AtDevice device);
AtModuleSdh Tha60210061ModuleSdhNew(AtDevice device);
AtModuleSdh ThaStmPwProductModuleSdhNew(AtDevice device);
AtModuleSdh Tha60210051ModuleSdhNew(AtDevice device);
AtModuleSdh Tha60290021ModuleSdhNew(AtDevice device);
AtModuleSdh Tha60290022ModuleSdhNew(AtDevice device);
AtModuleSdh Tha6A000011ModuleSdhNew(AtDevice device);
AtModuleSdh Tha60290011ModuleSdhNew(AtDevice device);
AtModuleSdh Tha60290022ModuleSdhNew(AtDevice device);
AtModuleSdh Tha6A033111ModuleSdhNew(AtDevice device);
AtModuleSdh Tha6A210031ModuleSdhNew(AtDevice device);
AtModuleSdh Tha6A290011ModuleSdhNew(AtDevice device);
AtModuleSdh Tha6A290021ModuleSdhNew(AtDevice device);
AtModuleSdh Tha6A290022ModuleSdhNew(AtDevice device);
AtModuleSdh Tha6A291022ModuleSdhNew(AtDevice device);
AtModuleSdh Tha6A033111ModuleSdhNew(AtDevice device);
AtModuleSdh Tha60290022ModuleSdhV2New(AtDevice device);
AtModuleSdh Tha60291022ModuleSdhNew(AtDevice device);
AtModuleSdh Tha60290081ModuleSdhNew(AtDevice device);

/* Product concrete channels */
AtSdhAug Tha60150011SdhAugNew(uint32 channelId, uint8 channelType, AtModuleSdh module);

eAtRet ThaSdhTtiWrite(AtSdhChannel self, const tAtSdhTti *tti, uint32 indirectAddr);
eAtRet ThaSdhTtiRead(AtSdhChannel self, tAtSdhTti *tti, uint32 indirectAddr);
AtPrbsEngine ThaModuleSdhVcPrbsEngineCreate(ThaModuleSdh self, AtSdhVc vc);
eBool ThaModuleSdhPohIsReady(ThaModuleSdh self);

/* STS conversion. */
void ThaModuleSdhHwSts24ToHwSts48Get(ThaModuleSdh self, uint8 slice24, uint8 sts24, uint8 *slice48, uint8* sts48);
void ThaModuleSdhHwSts48ToHwSts24Get(uint8 slice48, uint8 sts48, uint8 *slice24, uint8* sts24);

AtPrbsEngine ThaModuleSdhLinePrbsEngineCreate(ThaModuleSdh self, AtSdhLine line);
eBool ThaModuleSdhLineTimingIsSupported(ThaModuleSdh self);
eBool ThaModuleSdhLiuDe3MapVc3IsSupported(ThaModuleSdh self);

/* Indicate which module is used for getting counter */
eAtRet ThaModuleSdhDebugCountersModuleSet(ThaModuleSdh self, uint32 module);
uint32 ThaModuleSdhDebugCountersModuleGet(ThaModuleSdh self);

/* For DCC channel handle */
uint32 ThaModuleSdhMaxNumDccHdlcChannels(ThaModuleSdh self);

/* Interrupt enable */
eAtRet ThaModuleSdhLineInterruptEnable(ThaModuleSdh self, eBool enable);
eAtRet ThaModuleSdhStsInterruptEnable(ThaModuleSdh self, eBool enable);
eAtRet ThaModuleSdhVtInterruptEnable(ThaModuleSdh self, eBool enable);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULESDH_H_ */

/*-----------------------------------------------------------------------------
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies. 
 *
 * Module      : SDH
 *
 * File        : ThaSdhTuInternal.h
 *
 * Created Date: Apr 23, 2015
 *
 * Description : Thalassa SDH TU representation.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASDHTUINTERNAL_H_
#define _THASDHTUINTERNAL_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../../generic/sdh/AtSdhTuInternal.h"
#include "../../../../include/sdh/AtSdhTu.h"

#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaSdhTu * ThaSdhTu;

typedef struct tThaSdhTuMethods
    {
    uint8 dummy;
    }tThaSdhTuMethods;

typedef struct tThaSdhTu
    {
    tAtSdhTu super;
    const tThaSdhTuMethods *methods;

    /* Private data */
    uint8 enabled;
    AtObjectAny attController;
    uint8 txAisExplicitlyForced;
    }tThaSdhTu;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/
AtSdhTu ThaSdhTuObjectInit(AtSdhTu self, uint32 channelId, uint8 channelType, AtModuleSdh module);

#ifdef __cplusplus
}
#endif

#endif /* _THASDHTUINTERNAL_H_ */

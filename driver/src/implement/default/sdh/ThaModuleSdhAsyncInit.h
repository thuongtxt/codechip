/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : ThaModuleSdhAsyncInit.h
 * 
 * Created Date: Aug 20, 2016
 *
 * Description : AsyncInit
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULESDHASYNCINIT_H_
#define _THAMODULESDHASYNCINIT_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet ThaSdhModuleAsyncLineDefaultMain(ThaModuleSdh self);
eAtRet ThaModuleSdhAsyncInitMain(AtModule self);
eAtRet ThaModuleSdhAsyncSuperInit(AtModule self);


#ifdef __cplusplus
}
#endif
#endif /* _THAMODULESDHASYNCINIT_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : ThaSdhTug.c
 *
 * Created Date: Sep 7, 2012
 *
 * Description : Thalassa SDH TUG default implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSdhVc.h"
#include "../../../generic/sdh/AtModuleSdhInternal.h" /* For channel factory */
#include "../../../generic/sdh/AtSdhPathInternal.h"
#include "../sdh/ThaModuleSdh.h"
#include "../ocn/ThaModuleOcn.h"
#include "../pdh/ThaStmModulePdh.h"
#include "ThaSdhTugInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((ThaSdhTug)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods    m_AtChannelOverride;
static tAtSdhChannelMethods m_AtSdhChannelOverride;

/* To save super implementation */
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;
static const tAtSdhChannelMethods *m_AtSdhChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
AtSdhTug ThaSdhTugNew(uint32 channelId, uint8 channelType, AtModuleSdh module);

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaSdhTug);
    }

static eAtSdhTugMapType Tug3HwMapTypeGet(AtSdhChannel tug3)
    {
    uint8 stsRxPldType = ThaOcnStsRxPayloadTypeGet(tug3, AtSdhChannelSts1Get(tug3));
    if (stsRxPldType == cThaStsTug3ContainVt1x)
        return cAtSdhTugMapTypeTug3Map7xTug2s;

    if (stsRxPldType == cThaStsTug3ContainTu3)
        return cAtSdhTugMapTypeTug3MapVc3;

    return cAtSdhTugMapTypeTugMapNone;
    }

static eAtRet Tug3MapTypeSet(AtSdhChannel tug3, eAtSdhTugMapType tug3MapType)
    {
    if (AtChannelDeviceInWarmRestore((AtChannel)tug3))
        return cAtOk;

    if (tug3MapType == cAtSdhTugMapTypeTug3MapVc3)
        {
        eAtRet ret;
        static const uint8 defaultVc3MapType = cAtSdhVcMapTypeVc3MapC3;
        AtSdhChannel tu3 = AtSdhChannelSubChannelGet(tug3, 0);
        AtSdhChannel vc3 = AtSdhChannelSubChannelGet(tu3, 0);

        ret  = AtChannelInit((AtChannel)tu3);
        ret |= AtChannelInit((AtChannel)vc3);

        /* Set TUG-3 payload */
        ret |= ThaOcnTug3PldSet(tug3, tug3MapType);
        if (ret != cAtOk)
            return ret;

        /* Default VC-3 mapping type */
        return mMethodsGet(vc3)->MapTypeSet(vc3, defaultVc3MapType);
        }

    else
        {
        uint8 subChannel_i;
        static const uint8 cNumTug2InTug3 = 7;

        for (subChannel_i = 0; subChannel_i < cNumTug2InTug3; subChannel_i++)
            {
            AtSdhChannel subChannel = AtSdhChannelSubChannelGet(tug3, subChannel_i);
            AtChannelInit((AtChannel)subChannel);
            }

        return ThaOcnTug3PldSet(tug3, tug3MapType);
        }
    }

static void SubChannelsInit(AtSdhChannel tug2, eAtSdhTugMapType tug2MapType)
    {
    uint8 i, numSubChannels = 0;

    if (tug2MapType == cAtSdhTugMapTypeTug2Map3xTu12s)
        numSubChannels = 3;
    else if (tug2MapType == cAtSdhTugMapTypeTug2Map4xTu11s)
        numSubChannels = 4;

    /* Cache Line, STS and TUG-2 ID */
    for (i = 0; i < numSubChannels; i++)
        {
        AtSdhChannel tu = AtSdhChannelSubChannelGet(tug2, i);
        AtSdhChannel vc = AtSdhChannelSubChannelGet(tu, 0);

        /* Initialize them */
        AtChannelInit((AtChannel)tu);
        AtChannelInit((AtChannel)vc);
        }
    }

static eAtSdhTugMapType Tug2HwMapTypeGet(AtSdhChannel tug2)
    {
    uint8 vtgPldType = ThaOcnVtgPldGet(tug2, AtSdhChannelSts1Get(tug2), AtSdhChannelTug2Get(tug2));
    if (vtgPldType == cThaOcnVtgTug2PldVt15)
        return cAtSdhTugMapTypeTug2Map4xTu11s;

    if (vtgPldType == cThaOcnVtgTug2PldVt2)
        return cAtSdhTugMapTypeTug2Map3xTu12s;

    return cAtSdhTugMapTypeTugMapNone;
    }

static eAtRet Tug2MapTypeSet(AtSdhChannel tug2, eAtSdhTugMapType tug2MapType)
    {
    eAtRet ret = cAtOk;
    static const uint8 cNumTu12InTug2 = 3;
    static const uint8 cNumTu11InTug2 = 4;
    uint8 numTu = 0, tuId;
    AtModulePdh pdhModule;
    ThaModuleSdh sdhModule;
    uint8 sts1Id, tug2Id, defaultVc1xMapping;

    if (AtChannelDeviceInWarmRestore((AtChannel)tug2))
        return cAtOk;

    pdhModule = (AtModulePdh)AtDeviceModuleGet((AtChannelDeviceGet((AtChannel)tug2)), cAtModulePdh);
    sdhModule = (ThaModuleSdh)AtChannelModuleGet((AtChannel)tug2);
    sts1Id = AtSdhChannelSts1Get(tug2);
    tug2Id = AtSdhChannelTug2Get(tug2);
    defaultVc1xMapping = ThaModuleSdhDefaultVc1xSubMapping(sdhModule);

    /* Set TUG-2 mapping type */
    if (tug2MapType == cAtSdhTugMapTypeTug2Map4xTu11s)
        {
        ret  = ThaOcnVtgPldSet(tug2, sts1Id, tug2Id, cThaOcnVtgTug2PldVt15);
        ret |= ThaPdhRxVtgTypeSet(pdhModule, tug2, sts1Id, tug2Id, cThaPdhTu11Vt15);
        numTu = cNumTu11InTug2;
        }

    if (tug2MapType == cAtSdhTugMapTypeTug2Map3xTu12s)
        {
        ret |= ThaOcnVtgPldSet(tug2, AtSdhChannelSts1Get(tug2), tug2Id, cThaOcnVtgTug2PldVt2);
        ret |= ThaPdhRxVtgTypeSet(pdhModule, tug2, sts1Id, tug2Id, cThaPdhTu12Vt2);
        numTu = cNumTu12InTug2;
        }

    /* Init all sub channels */
    SubChannelsInit(tug2, tug2MapType);

    /* Initialize all of its TU-1x */
    for (tuId = 0; tuId < numTu; tuId++)
        {
        AtSdhChannel vc1x = AtSdhChannelSubChannelGet(AtSdhChannelSubChannelGet(tug2, tuId), 0);
        if (vc1x == NULL)
            return cAtErrorNullPointer;

        ret |= mMethodsGet(vc1x)->MapTypeSet(vc1x, defaultVc1xMapping);
        ret |= ThaOcnVtPathInit(tug2, AtSdhChannelSts1Get(tug2), AtSdhChannelTug2Get(tug2), tuId);
        }

    /* Give hardware a moment to in-frame */
    if (!AtDeviceIsSimulated(AtChannelDeviceGet((AtChannel)tug2)))
        AtOsalUSleep(2500);

    for (tuId = 0; tuId < numTu; tuId++)
        {
        AtSdhPath vc1x = (AtSdhPath)AtSdhChannelSubChannelGet(AtSdhChannelSubChannelGet(tug2, tuId), 0);
        AtSdhPathPohMonitorEnable(vc1x, cAtTrue);
        }

    return ret;
    }

static eAtModuleSdhRet MapTypeSet(AtSdhChannel self, uint8 mapType)
    {
    eAtSdhChannelType tugType;
    eAtRet ret;

    if(!AtSdhChannelNeedToUpdateMapType(self, mapType))
        return cAtOk;

    /* Super */
    ret = m_AtSdhChannelMethods->MapTypeSet(self, mapType);
    if (ret != cAtOk)
        return ret;

    tugType = AtSdhChannelTypeGet(self);
    if (tugType == cAtSdhChannelTypeTug3)
        return Tug3MapTypeSet(self, mapType);
    if (tugType == cAtSdhChannelTypeTug2)
        return Tug2MapTypeSet(self, mapType);

    return cAtErrorInvlParm;
    }

static eAtRet Debug(AtChannel self)
    {
    m_AtChannelMethods->Debug(self);
    ThaModuleSdhStsMappingDisplay((AtSdhChannel)self);
    return ThaSdhChannelCommonRegisterOffsetsDisplay((AtSdhChannel)self);
    }

static uint8 HwMapTypeGet(AtSdhChannel self)
    {
    eAtSdhChannelType tugType = AtSdhChannelTypeGet(self);

    if (tugType == cAtSdhChannelTypeTug3)
        return Tug3HwMapTypeGet(self);
    if (tugType == cAtSdhChannelTypeTug2)
        return Tug2HwMapTypeGet(self);

    return cAtSdhVcMapTypeNone;
    }

static void OverrideAtChannel(AtSdhTug self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, Debug);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtSdhChannel(AtSdhTug self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(m_AtSdhChannelOverride));
        mMethodOverride(m_AtSdhChannelOverride, MapTypeSet);
        mMethodOverride(m_AtSdhChannelOverride, HwMapTypeGet);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void Override(AtSdhTug self)
    {
    OverrideAtChannel(self);
    OverrideAtSdhChannel(self);
    }

AtSdhTug ThaSdhTugObjectInit(AtSdhTug self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaSdhTug));

    /* Super constructor */
    if (AtSdhTugObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhTug ThaSdhTugNew(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhTug newTug = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newTug == NULL)
        return NULL;

    /* Construct it */
    return ThaSdhTugObjectInit(newTug, channelId, channelType, module);
    }

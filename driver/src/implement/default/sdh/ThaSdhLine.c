/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : ThaSdhLine.c
 *
 * Created Date: Sep 7, 2012
 *
 * Description : Thalassa SDH Line default implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../generic/sur/AtModuleSurInternal.h"
#include "../../../generic/sur/AtSurEngineInternal.h"
#include "../../../generic/prbs/AtModulePrbsInternal.h"
#include "ThaModuleSdh.h"
#include "ThaModuleSdhReg.h"
#include "ThaModuleSdhInternal.h"
#include "ThaSdhLineInternal.h"
#include "../physical/ThaSdhLineSerdesController.h"
#include "../../../util/coder/AtCoderUtil.h"
#include "../att/ThaAttController.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mTtiProcessor(self) TtiProcessor((AtSdhLine)self)
#define mThis(self) ((ThaSdhLine)self)
#define mZeroVersion(self) (mThis(self)->version == 0)
#define mInAccessible(self) AtChannelInAccessible((AtChannel)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaSdhLineMethods m_methods;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtChannelMethods    m_AtChannelOverride;
static tAtSdhChannelMethods m_AtSdhChannelOverride;
static tAtSdhLineMethods    m_AtSdhLineOverride;

/* Save super implementation */
static const tAtSdhLineMethods    *m_AtSdhLineMethods    = NULL;
static const tAtObjectMethods     *m_AtObjectMethods     = NULL;
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;
static const tAtSdhChannelMethods *m_AtSdhChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
extern eAtRet ThaTtiMsgGet(const tAtSdhTti *tti, uint8 *pTtiMsg, uint8 *pHwJ0Md, uint8 *pJ0Len);

/*--------------------------- Implementation ---------------------------------*/
static AtObjectAny AttController(AtChannel self)
    {
    ThaModuleSdh module =(ThaModuleSdh) AtChannelModuleGet(self);
    if (mThis(self)->attController == NULL)
        {
        mThis(self)->attController = mMethodsGet(module)->LineAttControllerCreate(module, self);
        if (mThis(self)->attController)
            {
            AtAttControllerSetUp(mThis(self)->attController);
            AtAttControllerInit(mThis(self)->attController);
            }
        }
    return mThis(self)->attController;
    }

static void AttControllerDelete(AtObject self)
    {
    if (mThis(self)->attController)
        AtObjectDelete((AtObject)mThis(self)->attController);
    mThis(self)->attController = NULL;
    }

static AtModuleSdh SdhModule(AtSdhLine self)
    {
    return (AtModuleSdh)AtChannelModuleGet((AtChannel)self);
    }

static ThaTtiProcessor TtiProcessor(AtSdhLine self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    ThaModulePoh pohModule = (ThaModulePoh)AtDeviceModuleGet(device, cThaModulePoh);
    ThaSdhLine sdhLine = (ThaSdhLine)self;

    /* Create processor if it has not */
    if (sdhLine->ttiProcessor == NULL)
        sdhLine->ttiProcessor = ThaModulePohLineTtiProcessorCreate(pohModule, self);

    return sdhLine->ttiProcessor;
    }

static eAtRet TohMonitoringDefaultSet(ThaSdhLine self)
    {
    uint32 regAddr, regVal;

    regAddr = cThaRegOcnTohMonPerChnCtrl + mMethodsGet(self)->DefaultOffset(self);
    regVal  = 0;
    mFieldIns(&regVal, cThaK2StbMdMask, cThaK2StbMdShift, 1);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static void AllAlarmsUnforce(AtChannel self)
    {
    uint32 forcableAlarms = AtChannelRxForcableAlarmsGet(self);
    eAtRet ret = cAtOk;

    if (forcableAlarms)
        ret |= AtChannelRxAlarmUnForce(self, forcableAlarms);

    forcableAlarms = AtChannelTxForcableAlarmsGet(self);
    if (forcableAlarms)
        ret |= AtChannelTxAlarmUnForce(self, forcableAlarms);

    ret |= AtChannelTxErrorUnForce(self, cAtSdhLineCounterTypeB1);
    ret |= AtChannelTxErrorUnForce(self, cAtSdhLineCounterTypeB2);

    if (ret != cAtOk)
        mChannelLog(self, cAtLogLevelCritical, "All alarms unforce fail");
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret;
    AtSdhLine line = (AtSdhLine)self;
    AtSerdesController serdesController = AtSdhLineSerdesController(line);
    ThaModuleSdh moduleSdh = (ThaModuleSdh)SdhModule(line);
    eAtRet ledRet;

    ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    if (!AtChannelShouldFullyInit(self))
        return cAtOk;

    /* Set TOH and let hardware auto control the LED */
    if (mMethodsGet(mThis(self))->HasToh(mThis(self)))
        ret = mMethodsGet(mThis(self))->TohMonitoringDefaultSet(mThis(self));

    /* Should ignore LED=Access if is not supported */
    if (mMethodsGet(moduleSdh)->HasLedEngine(moduleSdh))
        {
        ledRet = AtSdhLineLedStateSet(line, cAtLedStateBlink);
        if (ledRet != cAtErrorNotApplicable)
            ret |= ledRet;
        }

    AllAlarmsUnforce(self);

    /* Set default timing mode */
    if (ThaModuleSdhShouldUpdateSerdesTimingWhenLineRateChange(moduleSdh))
    AtSerdesControllerTimingModeSet(serdesController, ThaModuleSdhDefaultSerdesTimingModeForLine(moduleSdh, line));

    if (AtSdhLinePrbsEngineGet((AtSdhLine)self))
        AtPrbsEngineEnable(AtSdhLinePrbsEngineGet((AtSdhLine)self), cAtFalse);

    return ret;
    }

static uint8 HwLineIdGet(ThaSdhLine self)
    {
    return (uint8)ThaModuleSdhLineLocalId((AtSdhLine)self);
    }

/* Default implementation of register formula */
static uint32 DefaultOffset(ThaSdhLine self)
    {
    return HwLineIdGet(self) + ThaSdhLinePartOffset(self);
    }

static ThaModuleSdh ThaSdhModule(AtChannel self)
    {
    return (ThaModuleSdh)AtChannelModuleGet(self);
    }

static AtPrbsEngine PrbsEngineGet(AtSdhLine self)
    {
    if (mThis(self)->linePrbsEngine == NULL)
        mThis(self)->linePrbsEngine = ThaModuleSdhLinePrbsEngineCreate(ThaSdhModule((AtChannel)self), self);
    return mThis(self)->linePrbsEngine;
    }

static void Delete(AtObject self)
    {
    ThaSdhLine line = (ThaSdhLine)self;
    
    /* Delete private data */
    AtObjectDelete((AtObject)mThis(self)->ttiProcessor);
    mThis(self)->ttiProcessor = NULL;
    AtObjectDelete((AtObject)mThis(self)->serdesController);
    mThis(self)->serdesController = NULL;
    AtObjectDelete((AtObject)line->linePrbsEngine);
    line->linePrbsEngine = NULL;
    AttControllerDelete(self);
    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static eAtRet Enable(AtChannel self, eBool enable)
    {
    eAtRet ret = cAtOk;

    ret |= AtChannelTxEnable(self, enable);
    ret |= AtChannelRxEnable(self, enable);

    return ret;
    }

static eBool IsEnabled(AtChannel self)
    {
    return AtChannelTxIsEnabled(self);
    }

static uint32 SdSfHistoryGet(ThaSdhLine self, eBool clear)
    {
    AtBerController berController;
    uint32 almHistory = 0;

    /* Get RS SD, SF */
    berController = SdhLineRsBerControllerGet((AtSdhLine)self);
    if (AtBerControllerSdHistoryGet(berController, clear))
        almHistory |= cAtSdhLineAlarmRsBerSd;
    if (AtBerControllerSfHistoryGet(berController, clear))
        almHistory |= cAtSdhLineAlarmRsBerSf;

    /* Get MS SD, SF */
    berController = SdhLineMsBerControllerGet((AtSdhLine)self);
    if (AtBerControllerSdHistoryGet(berController, clear))
        almHistory |= cAtSdhLineAlarmBerSd;
    if (AtBerControllerSfHistoryGet(berController, clear))
        almHistory |= cAtSdhLineAlarmBerSf;

    return almHistory;
    }

static eBool BerHardwareInterruptIsSupported(AtChannel self)
    {
    ThaModuleSdh sdhModule = (ThaModuleSdh)AtChannelModuleGet(self);
    return ThaModuleSdhBerHardwareInterruptIsSupported(sdhModule);
    }

static uint32 RsRealAlarmGet(ThaSdhLine self, uint32 alarmRegValue)
    {
    uint32 alarm = 0;
    uint8 startBit;

    if (self == NULL)
        return 0;

    startBit = mMethodsGet(self)->CurrentStatusStartBit(self);
    if (alarmRegValue & (cThaRxLLosCurStatMask << startBit))
        alarm |= cAtSdhLineAlarmLos;
    if (alarmRegValue & (cThaRxLLofCurStatMask << startBit))
        alarm |= cAtSdhLineAlarmLof;
    if (alarmRegValue & (cThaRxLOofCurStatMask << startBit))
        alarm |= cAtSdhLineAlarmOof;

    /* TIM */
    alarm |= ThaTtiProcessorAlarmGet(mTtiProcessor(self));

    return alarm;
    }

static uint32 RsAlarmGet(ThaSdhLine self, uint32 alarmRegValue)
    {
    uint32 realAlarm = mMethodsGet(self)->RsRealAlarmGet(self, alarmRegValue);

    /* Just keep root critical alarm */
    if (realAlarm & cAtSdhLineAlarmLos)
        return cAtSdhLineAlarmLos;
    if (realAlarm & cAtSdhLineAlarmLof)
        return cAtSdhLineAlarmLof;
    if (realAlarm & cAtSdhLineAlarmOof)
        return cAtSdhLineAlarmOof;

    /* TIM */
    return realAlarm;
    }

static uint32 MsRealAlarmGet(ThaSdhLine self, uint32 alarmRegValue)
    {
    uint32 alarms = 0;
    uint8 startBit;

    if (self == NULL)
        return 0;

    startBit = mMethodsGet(self)->CurrentStatusStartBit(self);
    if (alarmRegValue & (cThaRxLAisLDefCurStatMask << startBit))
        alarms |= cAtSdhLineAlarmAis;

    if (alarmRegValue & (cThaRxLRdiILDefCurStatMask << startBit))
        alarms |= cAtSdhLineAlarmRdi;
	
	/* Changes on K1/K2/S1 are just event, they are not alarms. So, do not
	 * need to read them here */
	
    alarms |= ThaSdhLineBerDefectGet(self);

    return alarms;
    }

static uint32 MsAlarmGet(ThaSdhLine self, uint32 alarmRegValue)
    {
    uint32 realAlarms = mMethodsGet(self)->MsRealAlarmGet(self, alarmRegValue);

    if (realAlarms & cAtSdhLineAlarmAis)
        return cAtSdhLineAlarmAis;

    return realAlarms;
    }

static uint32 HwAlarmGet(ThaSdhLine self)
    {
    uint32 address = cThaRegOcnRxLineperAlmCurrentStat + mMethodsGet(self)->DefaultOffset(self);
    return mChannelHwRead(self, address, cThaModuleOcn);
    }

static uint32 AlarmFromHwAlarm(ThaSdhLine self, uint32 hwAlarm)
    {
    return RsAlarmGet(self, hwAlarm) | MsAlarmGet(self, hwAlarm);
    }

static uint32 DefectGet(AtChannel self)
    {
    ThaSdhLine line = mThis(self);
    return AlarmFromHwAlarm(line, mMethodsGet(line)->HwAlarmGet(line));
    }

static uint32 AlarmHistoryRead2Clear(ThaSdhLine self, eBool read2Clear, uint32 *pStickyRegValue)
    {
    uint32 almIntrStatus = 0;
    uint32 regAddr = mMethodsGet(self)->AlarmStickyAddress(self, read2Clear);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);

    if (regVal & cThaRxLLosStateChgIntrMask)
        almIntrStatus |= cAtSdhLineAlarmLos;
    if (regVal & cThaRxLLofStateChgIntrMask)
        almIntrStatus |= cAtSdhLineAlarmLof;
    if (regVal & cThaRxLOofStateChgIntrMask)
        almIntrStatus |= cAtSdhLineAlarmOof;
    if (regVal & cThaRxLAisLDefStateChgIntrMask)
        almIntrStatus |= cAtSdhLineAlarmAis;
    if (regVal & cThaRxLRdiILDefStateChgIntrMask)
        almIntrStatus |= cAtSdhLineAlarmRdi;

    if ((regVal & mMethodsGet(self)->K1ChgMask(self)))
        almIntrStatus |= cAtSdhLineAlarmK1Change;
    if ((regVal & mMethodsGet(self)->K2ChgMask(self)))
        almIntrStatus |= cAtSdhLineAlarmK2Change;
    if (regVal & mMethodsGet(self)->S1ChgMask(self))
        almIntrStatus |= cAtSdhLineAlarmS1Change;

    /* Get SD, SF */
    if (BerHardwareInterruptIsSupported((AtChannel)self))
        {
        AtBerController controller;

        /* RS BER */
        controller = AtSdhLineRsBerControllerGet((AtSdhLine)self);
        if (controller && AtBerControllerIsEnabled(controller))
            {
            if (regVal & mMethodsGet(self)->SdSMask(self))
                almIntrStatus |= cAtSdhLineAlarmRsBerSd;
            if (regVal & mMethodsGet(self)->SfSMask(self))
                almIntrStatus |= cAtSdhLineAlarmRsBerSf;
            }

        /* MS BER */
        controller = AtSdhLineMsBerControllerGet((AtSdhLine)self);
        if (controller && AtBerControllerIsEnabled(controller))
            {
            if (regVal & mMethodsGet(self)->SdLMask(self))
                almIntrStatus |= cAtSdhLineAlarmBerSd;
            if (regVal & mMethodsGet(self)->SfLMask(self))
                almIntrStatus |= cAtSdhLineAlarmBerSf;
            }
        }
    else
        almIntrStatus |= SdSfHistoryGet(self, read2Clear);

    /* Clear history after reading */
    if (read2Clear && !mMethodsGet(self)->StickyRegisterIsRead2Clear(self))
        mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    /* Get TIM-S interrupt */
    if (read2Clear)
        almIntrStatus |= ThaTtiProcessorAlarmInterruptClear(mTtiProcessor(self));
    else
        almIntrStatus |= ThaTtiProcessorAlarmInterruptGet(mTtiProcessor(self));

    if (pStickyRegValue)
        *pStickyRegValue = regVal;

    return almIntrStatus;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    return AlarmHistoryRead2Clear(mThis(self), cAtFalse, NULL);
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    return AlarmHistoryRead2Clear(mThis(self), cAtTrue, NULL);
    }

static eAtRet InterruptPerLineEnable(AtChannel self, eBool enable)
    {
    uint32 regVal, address;
    uint32 offset = ThaSdhLinePartOffset((ThaSdhLine)self);
    uint32 lineLocalId = ThaModuleSdhLineLocalId((AtSdhLine)self);

    address = cThaRegOcnRxLineperLineIntrEnable + offset;
    regVal  = mChannelHwRead(self, address, cThaModuleOcn);
    mFieldIns(&regVal,
              cThaRegOcnRxLineperLineMask(lineLocalId),
              cThaRegOcnRxLineperLineShift(lineLocalId),
              mBoolToBin(enable));

    mChannelHwWrite(self, address, regVal, cThaModuleOcn);
    return cAtOk;
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
    AtUnused(self);

    return (cAtSdhLineAlarmLos         |
            cAtSdhLineAlarmLof         |
            cAtSdhLineAlarmOof         |
            cAtSdhLineAlarmAis         |
            cAtSdhLineAlarmRdi         |
            cAtSdhLineAlarmKByteChange |
            cAtSdhLineAlarmK1Change    |
            cAtSdhLineAlarmK2Change    |
            cAtSdhLineAlarmS1Change    |
            cAtSdhLineAlarmRsBerSd     |
            cAtSdhLineAlarmRsBerSf     |
            cAtSdhLineAlarmBerSd       |
            cAtSdhLineAlarmBerSf);
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    uint32 regVal, address, currentMask;
    ThaSdhLine sdhLine = (ThaSdhLine)self;
    eAtRet ret;

    if (defectMask == 0)
        return cAtOk;

    address = cThaRegOcnRxLineperAlmIntrEnCtrl + mMethodsGet(sdhLine)->DefaultOffset(sdhLine);
    regVal  = mChannelHwRead(sdhLine, address, cThaModuleOcn);
    if (defectMask & cAtSdhLineAlarmLos)
        mFieldIns(&regVal,
                  cThaRxLLosStateChgIntrEnMask,
                  cThaRxLLosStateChgIntrEnShift,
                  (enableMask & cAtSdhLineAlarmLos) ? 1 : 0);

    if (defectMask & cAtSdhLineAlarmLof)
        mFieldIns(&regVal,
                  cThaRxLLofStateChgIntrEnMask,
                  cThaRxLLofStateChgIntrEnShift,
                  (enableMask & cAtSdhLineAlarmLof) ? 1 : 0);

    if (defectMask & cAtSdhLineAlarmOof)
        mFieldIns(&regVal,
                  cThaRxLOofStateChgIntrEnMask,
                  cThaRxLOofStateChgIntrEnShift,
                  (enableMask & cAtSdhLineAlarmOof) ? 1 : 0);

    if (defectMask & cAtSdhLineAlarmAis)
        mFieldIns(&regVal,
                  cThaRxLAisLDefStateChgIntrEnMask,
                  cThaRxLAisLDefStateChgIntrEnShift,
                  (enableMask & cAtSdhLineAlarmAis) ? 1 : 0);

    if (defectMask & cAtSdhLineAlarmRdi)
        mFieldIns(&regVal,
                  cThaRxLRdiLDefStateChgIntrEnMask,
                  cThaRxLRdiLDefStateChgIntrEnShift,
                  (enableMask & cAtSdhLineAlarmRdi) ? 1 : 0);

    if (defectMask & cAtSdhLineAlarmK1Change)
        mFieldIns(&regVal,
                  mMethodsGet(sdhLine)->K1ChgMask(sdhLine),
                  mMethodsGet(sdhLine)->K1ChgShift(sdhLine),
                  (enableMask & cAtSdhLineAlarmK1Change) ? 1 : 0);
        
    if (defectMask & cAtSdhLineAlarmK2Change)
        mFieldIns(&regVal,
                  mMethodsGet(sdhLine)->K2ChgMask(sdhLine),
                  mMethodsGet(sdhLine)->K2ChgShift(sdhLine),
                  (enableMask & cAtSdhLineAlarmK2Change) ? 1 : 0);

    if (defectMask & cAtSdhLineAlarmS1Change)
        mFieldIns(&regVal,
                  mMethodsGet(sdhLine)->S1ChgMask(sdhLine),
                  mMethodsGet(sdhLine)->S1ChgShift(sdhLine),
                  (enableMask & cAtSdhLineAlarmS1Change) ? 1 : 0);

    if (BerHardwareInterruptIsSupported(self))
        {
        if (defectMask & cAtSdhLineAlarmRsBerSd)
            mFieldIns(&regVal,
                      mMethodsGet(sdhLine)->SdSMask(sdhLine),
                      mMethodsGet(sdhLine)->SdSShift(sdhLine),
                      (enableMask & cAtSdhLineAlarmRsBerSd) ? 1 : 0);

        if (defectMask & cAtSdhLineAlarmRsBerSf)
            mFieldIns(&regVal,
                      mMethodsGet(sdhLine)->SfSMask(sdhLine),
                      mMethodsGet(sdhLine)->SfSShift(sdhLine),
                      (enableMask & cAtSdhLineAlarmRsBerSf) ? 1 : 0);

        if (defectMask & cAtSdhLineAlarmBerSd)
            mFieldIns(&regVal,
                      mMethodsGet(sdhLine)->SdLMask(sdhLine),
                      mMethodsGet(sdhLine)->SdLShift(sdhLine),
                      (enableMask & cAtSdhLineAlarmBerSd) ? 1 : 0);

        if (defectMask & cAtSdhLineAlarmBerSf)
            mFieldIns(&regVal,
                      mMethodsGet(sdhLine)->SfLMask(sdhLine),
                      mMethodsGet(sdhLine)->SfLShift(sdhLine),
                      (enableMask & cAtSdhLineAlarmBerSf) ? 1 : 0);
        }

    mChannelHwWrite(sdhLine, address, regVal, cThaModuleOcn);

    /* Enable/Disable for TIM-S */
    ret = ThaTtiProcessorInterruptMaskSet(mTtiProcessor(self), defectMask, enableMask);
    if (ret != cAtOk)
        return ret;

    /* Enable line interrupt if there is at least one alarm is enabled */
    currentMask = mMethodsGet(self)->InterruptMaskGet(self);

    return InterruptPerLineEnable(self, (currentMask) ? cAtTrue : cAtFalse);
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    uint32 address, regVal, mask = 0;
    ThaSdhLine sdhLine = (ThaSdhLine)self;

    address = cThaRegOcnRxLineperAlmIntrEnCtrl + mMethodsGet(sdhLine)->DefaultOffset(sdhLine);
    regVal = mChannelHwRead(self, address, cThaModuleOcn);

    if (regVal & cThaRxLLosStateChgIntrEnMask)
        mask |= cAtSdhLineAlarmLos;
    if (regVal & cThaRxLLofStateChgIntrEnMask)
        mask |= cAtSdhLineAlarmLof;
    if (regVal & cThaRxLOofStateChgIntrEnMask)
        mask |= cAtSdhLineAlarmOof;
    if (regVal & cThaRxLAisLDefStateChgIntrEnMask)
        mask |= cAtSdhLineAlarmAis;
    if (regVal & cThaRxLRdiLDefStateChgIntrEnMask)
        mask |= cAtSdhLineAlarmRdi;
    if (regVal & mMethodsGet(sdhLine)->K1ChgMask(sdhLine))
        mask |= cAtSdhLineAlarmK1Change;
    if (regVal & mMethodsGet(sdhLine)->K2ChgMask(sdhLine))
        mask |= cAtSdhLineAlarmK2Change;
    if (regVal & mMethodsGet(sdhLine)->S1ChgMask(sdhLine))
        mask |= cAtSdhLineAlarmS1Change;

    if (BerHardwareInterruptIsSupported(self))
        {
        if (regVal & mMethodsGet(sdhLine)->SdSMask(sdhLine))
            mask |= cAtSdhLineAlarmRsBerSd;
        if (regVal & mMethodsGet(sdhLine)->SfSMask(sdhLine))
            mask |= cAtSdhLineAlarmRsBerSf;
        if (regVal & mMethodsGet(sdhLine)->SdLMask(sdhLine))
            mask |= cAtSdhLineAlarmBerSd;
        if (regVal & mMethodsGet(sdhLine)->SfLMask(sdhLine))
            mask |= cAtSdhLineAlarmBerSf;
        }

    mask |= ThaTtiProcessorInterruptMaskGet(mTtiProcessor(self));

    return mask;
    }

static uint32 TxForcibleAlarmsGet(AtChannel self)
    {
	AtUnused(self);
    return cAtSdhLineAlarmAis | cAtSdhLineAlarmRdi | cAtSdhLineAlarmLof;
    }

static eBool CanForceTxAlarms(ThaSdhLine self, uint32 alarmType)
    {
    return (alarmType & AtChannelTxForcableAlarmsGet((AtChannel)self)) ? cAtTrue : cAtFalse;
    }

static eAtRet HelperTxAlarmForce(AtChannel self, uint32 alarmType, eBool force)
    {
    uint32 regVal, address;

    if (!CanForceTxAlarms(mThis(self), alarmType))
        return cAtErrorModeNotSupport;

    address = cThaRegOcnTxFrmrPerChnCtrl1 + mMethodsGet((ThaSdhLine)self)->DefaultOffset((ThaSdhLine)self);
    regVal  = mChannelHwRead(self, address, cThaModuleOcn);

    if (alarmType & cAtSdhLineAlarmAis)
        mFieldIns(&regVal, cThaOcnTxAisLFrcMask, cThaOcnTxAisLFrcShift, mBoolToBin(force));

    if (alarmType & cAtSdhLineAlarmRdi)
        mFieldIns(&regVal, cThaOcnTxRdiLFrcMask, cThaOcnTxRdiLFrcShift, mBoolToBin(force));

    if (alarmType & cAtSdhLineAlarmLof)
        mFieldIns(&regVal, cThaOcnTxAutoA1A2EnBMask, cThaOcnTxAutoA1A2EnBShift, mBoolToBin(force));

    mChannelHwWrite(self, address, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eAtRet TxAlarmForce(AtChannel self, uint32 alarmType)
    {
    eAtRet ret = HelperTxAlarmForce(self, alarmType, cAtTrue);
    m_AtChannelMethods->TxAlarmForce(self, alarmType);
    return ret;
    }

static eAtRet TxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    eAtRet ret = HelperTxAlarmForce(self, alarmType, cAtFalse);
    m_AtChannelMethods->TxAlarmUnForce(self, alarmType);
    return ret;
    }

static uint32 TxForcedAlarmGet(AtChannel self)
    {
    uint32 regAddr = cThaRegOcnTxFrmrPerChnCtrl1 + mMethodsGet((ThaSdhLine)self)->DefaultOffset((ThaSdhLine)self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);
    uint32 forcedAlarms = 0;

    if (mInAccessible(self))
        return m_AtChannelMethods->TxForcedAlarmGet(self);

    if (regVal & cThaOcnTxAisLFrcMask)
        forcedAlarms |= cAtSdhLineAlarmAis;
    if (regVal & cThaOcnTxRdiLFrcMask)
        forcedAlarms |= cAtSdhLineAlarmRdi;
    if (regVal & cThaOcnTxAutoA1A2EnBMask)
        forcedAlarms |= cAtSdhLineAlarmLof;

    return forcedAlarms;
    }

static uint32 RxForcibleAlarmsGet(AtChannel self)
    {
	AtUnused(self);
    return cAtSdhLineAlarmAis | cAtSdhLineAlarmLos;
    }

static eBool CanForceRxAlarms(ThaSdhLine self, uint32 alarmType)
    {
    return (alarmType & AtChannelRxForcableAlarmsGet((AtChannel)self)) ? cAtTrue : cAtFalse;
    }

static eAtRet HelperRxAlarmForce(AtChannel self, uint32 alarmType, eBool force)
    {
    uint32 regVal, address;

    if (!CanForceRxAlarms(mThis(self), alarmType))
        return cAtErrorModeNotSupport;

    if (alarmType & cAtSdhLineAlarmAis)
        {
        address = cThaRegOcnRxFrmrPerChnCtrl + mMethodsGet((ThaSdhLine)self)->DefaultOffset((ThaSdhLine)self);
        regVal  = mChannelHwRead(self, address, cThaModuleOcn);
        mFieldIns(&regVal, cThaOcnRxAisLFrcMask, cThaOcnRxAisLFrcShift, force ? 1 : 0);
        mChannelHwWrite(self, address, regVal, cThaModuleOcn);
        }

    if (alarmType & cAtSdhLineAlarmLos)
        {
        uint8 localId = (uint8)ThaModuleSdhLineLocalId((AtSdhLine)self);
        address = cThaRegOcnLosForce + ThaSdhLinePartOffset(mThis(self));
        regVal  = mChannelHwRead(self, address, cThaModuleOcn);
        mFieldIns(&regVal, cLineMask(localId), cLineShift(localId), force ? 1 : 0);
        mChannelHwWrite(self, address, regVal, cThaModuleOcn);
        }

    return cAtOk;
    }

static eAtRet RxAlarmForce(AtChannel self, uint32 alarmType)
    {
    return HelperRxAlarmForce(self, alarmType, cAtTrue);
    }

static eAtRet RxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    return HelperRxAlarmForce(self, alarmType, cAtFalse);
    }

static uint32 RxForcedAlarmGet(AtChannel self)
    {
    uint32 address, regVal;
    uint32 swMask = 0;
    uint8 localId = (uint8)ThaModuleSdhLineLocalId((AtSdhLine)self);

    address = cThaRegOcnRxFrmrPerChnCtrl + mMethodsGet((ThaSdhLine)self)->DefaultOffset((ThaSdhLine)self);
    regVal = mChannelHwRead(self, address, cThaModuleOcn);
    if (regVal & cThaOcnRxAisLFrcMask)
        swMask |= cAtSdhLineAlarmAis;

    address = cThaRegOcnLosForce + ThaSdhLinePartOffset(mThis(self));
    regVal  = mChannelHwRead(self, address, cThaModuleOcn);
    if (regVal & cLineMask(localId))
        swMask |= cAtSdhLineAlarmLos;

    return swMask;
    }

static eAtRet ForceB1(AtChannel self, eBool force)
    {
    uint32 regVal, address;

    address = cThaRegOcnTxFrmrPerChnCtrl1 + mMethodsGet((ThaSdhLine)self)->DefaultOffset((ThaSdhLine)self);
    regVal  = mChannelHwRead(self, address, cThaModuleOcn);
    mFieldIns(&regVal, cThaOcnTxAutoB1EnBMask, cThaOcnTxAutoB1EnBShift, force ? 1 : 0);
    mChannelHwWrite(self, address, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eBool ForcedB1(AtChannel self)
    {
    uint32 regVal, address;

    address = cThaRegOcnTxFrmrPerChnCtrl1 + mMethodsGet((ThaSdhLine)self)->DefaultOffset((ThaSdhLine)self);
    regVal  = mChannelHwRead(self, address, cThaModuleOcn);
    return (regVal & cThaOcnTxAutoB1EnBMask) ? cAtTrue : cAtFalse;
    }

static eAtRet ForceB2(AtChannel self, eBool force)
    {
    uint32 regVal, address;

    address = cThaRegOcnTxFrmrPerChnCtrl1 + mMethodsGet((ThaSdhLine)self)->DefaultOffset((ThaSdhLine)self);
    regVal  = mChannelHwRead(self, address, cThaModuleOcn);
    mFieldIns(&regVal, cThaOcnTxAutoB2EnBMask, cThaOcnTxAutoB2EnBShift, force ? 1 : 0);
    mChannelHwWrite(self, address, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eAtRet ForcedB2(AtChannel self)
    {
    uint32 regVal, address;

    address = cThaRegOcnTxFrmrPerChnCtrl1 + mMethodsGet((ThaSdhLine)self)->DefaultOffset((ThaSdhLine)self);
    regVal  = mChannelHwRead(self, address, cThaModuleOcn);
    return (regVal & cThaOcnTxAutoB2EnBMask) ? cAtTrue : cAtFalse;
    }

static eAtRet TxErrorForce(AtChannel self, uint32 errorType)
    {
    /* Release force first */
    ForceB1(self, cAtFalse);
    ForceB2(self, cAtFalse);

    if (errorType == cAtSdhLineErrorB1) return ForceB1(self, cAtTrue);
    if (errorType == cAtSdhLineErrorB2) return ForceB2(self, cAtTrue);

    return cAtErrorModeNotSupport;
    }

static eAtRet TxErrorUnForce(AtChannel self, uint32 errorType)
    {
    if (errorType == cAtSdhLineErrorB1) return ForceB1(self, cAtFalse);
    if (errorType == cAtSdhLineErrorB2) return ForceB2(self, cAtFalse);

    return cAtErrorModeNotSupport;
    }

static uint32 TxForcedErrorGet(AtChannel self)
    {
    if (ForcedB1(self)) return cAtSdhLineErrorB1;
    if (ForcedB2(self)) return cAtSdhLineErrorB2;

    return cAtSdhLineErrorNone;
    }

static eAtRet FpgaLoopInEnable(ThaSdhLine self, eBool enable)
    {
    uint32 regVal, regAddr;
    uint8  hwLineId;

    hwLineId = HwLineIdGet((ThaSdhLine)self);
    regAddr = cThaRegOcnLineLoopInCtrl + ThaSdhLinePartOffset(self);
    regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    mFieldIns(&regVal, cThaRegOcnLineLoopInMask(hwLineId), cThaRegOcnLineLoopInShift(hwLineId), (enable ? 1 : 0));
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eBool FpgaLoopInIsEnable(ThaSdhLine self)
    {
    uint32 regVal;
    uint8  hwLineId;

    hwLineId = HwLineIdGet((ThaSdhLine)self);
    regVal = mChannelHwRead(self, cThaRegOcnLineLoopInCtrl + ThaSdhLinePartOffset(self), cThaModuleOcn);

    return (regVal & cThaRegOcnLineLoopInMask(hwLineId)) ? cAtTrue : cAtFalse;
    }

static eAtRet SerdesLoopInEnable(ThaSdhLine self, eBool enable)
    {
    AtSerdesController controller = AtSdhLineSerdesController((AtSdhLine)self);
    if (controller)
        return AtSerdesControllerLoopbackEnable(controller, cAtLoopbackModeLocal, enable);
    return cAtOk;
    }

static eBool SerdesLoopInIsEnabled(ThaSdhLine self)
    {
    AtSerdesController controller = AtSdhLineSerdesController((AtSdhLine)self);
    if (controller)
        return AtSerdesControllerLoopbackIsEnabled(controller, cAtLoopbackModeLocal);
    return cAtFalse;
    }

/* Note, we may think that one loopback point is enough and try deleting one
 * of these points. Don't do that!. The reason that we still need loopin inside
 * FPGA is to mask LOS signal from the outside. */
static eAtRet LoopInEnable(ThaSdhLine self, eBool enable)
    {
    eAtRet ret = cAtOk;

    /* SERDES loopin. Just ignore the case that SERDES does not support */
    ret = mMethodsGet(self)->SerdesLoopInEnable(self, enable);
    if ((ret == cAtErrorNotImplemented) || (ret == cAtErrorModeNotSupport))
        ret = cAtOk;

    /* FPGA loopin */
    ret |= mMethodsGet(self)->FpgaLoopInEnable(self, enable);

    return ret;
    }

static eBool LoopInIsEnabled(ThaSdhLine self)
    {
    return (mMethodsGet(self)->SerdesLoopInIsEnabled(self) || mMethodsGet(self)->FpgaLoopInIsEnable(self)) ? cAtTrue : cAtFalse;
    }

static uint8 SerdesIdGet(ThaSdhLine self)
    {
    AtSdhLine line = (AtSdhLine)self;
    return (uint8)ThaModuleSdhSerdesCoreIdOfLine((ThaModuleSdh)SdhModule(line), line);
    }

static eAtRet SerdesLoopoutEnable(ThaSdhLine self, eBool enable)
    {
    AtSerdesController controller = AtSdhLineSerdesController((AtSdhLine)self);
    if (controller)
        return AtSerdesControllerLoopbackEnable(controller, cAtLoopbackModeRemote, enable);
    return cAtOk;
    }

static eAtRet FpgaLoopOutEnable(ThaSdhLine self, eBool enable)
    {
    uint32 regVal, regAddr;
    uint8  hwLineId;

    if (!mMethodsGet(self)->FpgaLoopOutIsApplicable(self))
        return cAtErrorModeNotSupport;

    hwLineId = HwLineIdGet(self);
    regAddr = cThaRegOcnLineLoopOutCtrl + ThaSdhLinePartOffset(self);
    regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    mFieldIns(&regVal, cThaRegOcnLineLoopOutMask(hwLineId), cThaRegOcnLineLoopOutShift(hwLineId), ((enable) ? 1 : 0));
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eBool FpgaLoopOutIsEnable(ThaSdhLine self)
    {
    uint32 regVal;
    uint8  hwLineId;

    if (!mMethodsGet(self)->FpgaLoopOutIsApplicable(self))
        return cAtFalse;

    hwLineId = HwLineIdGet(self);
    regVal = mChannelHwRead(self, cThaRegOcnLineLoopOutCtrl + ThaSdhLinePartOffset(self), cThaModuleOcn);

    return (regVal & cThaRegOcnLineLoopOutMask(hwLineId)) ? cAtTrue : cAtFalse;
    }

static eBool FpgaLoopOutIsApplicable(ThaSdhLine self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static eAtRet LoopOutEnable(ThaSdhLine self, eBool enable)
    {
    eAtRet ret = cAtOk;

    /* Try  serdes loop first */
    ret = mMethodsGet(self)->SerdesLoopoutEnable(self, enable);

    /* If serdes loop is not supported, ignore and try FPGA loop */
    if ((ret == cAtErrorNotImplemented) || (ret == cAtErrorModeNotSupport))
        {
        if (mMethodsGet(self)->FpgaLoopOutIsApplicable(self))
            return mMethodsGet(self)->FpgaLoopOutEnable(self, enable);
        return ret;
        }

    /* Serdes loop is supported, also loop inside FPGA as HW recommended */
    if (mMethodsGet(self)->FpgaLoopOutIsApplicable(self))
        ret |= mMethodsGet(self)->FpgaLoopOutEnable(self, enable);

    return ret;
    }

static eBool SerdesLoopoutIsEnabled(ThaSdhLine self)
    {
    AtSerdesController controller = AtSdhLineSerdesController((AtSdhLine)self);
    if (controller)
        return AtSerdesControllerLoopbackIsEnabled(controller, cAtLoopbackModeRemote);
    return cAtFalse;
    }

static eBool LoopOutIsEnabled(ThaSdhLine self)
    {
    return (mMethodsGet(self)->SerdesLoopoutIsEnabled(self) || mMethodsGet(self)->FpgaLoopOutIsEnable(self)) ? cAtTrue : cAtFalse;
    }

static eAtRet LoopbackSet(AtChannel self, uint8 loopbackMode)
    {
    eAtRet ret = cAtOk;

    /* Just one mode should be enabled at a time */
    switch (loopbackMode)
        {
        case cAtLoopbackModeLocal:
            ret |= mMethodsGet(mThis(self))->LoopOutEnable(mThis(self), cAtFalse);
            ret |= mMethodsGet(mThis(self))->LoopInEnable(mThis(self), cAtTrue);
            break;

        case cAtLoopbackModeRemote:
            ret |= mMethodsGet(mThis(self))->LoopInEnable(mThis(self), cAtFalse);
            ret |= mMethodsGet(mThis(self))->LoopOutEnable(mThis(self), cAtTrue);
            break;

        case cAtLoopbackModeRelease:
            ret |= mMethodsGet(mThis(self))->LoopInEnable(mThis(self), cAtFalse);
            ret |= mMethodsGet(mThis(self))->LoopOutEnable(mThis(self), cAtFalse);
            break;

        default:
            return cAtErrorModeNotSupport;
        }

    m_AtChannelMethods->LoopbackSet(self, loopbackMode);

    return ret;
    }

static uint8 LoopbackGet(AtChannel self)
    {
    if (mInAccessible(self))
        return m_AtChannelMethods->LoopbackGet(self);

    if (mMethodsGet(mThis(self))->LoopInIsEnabled(mThis(self)))
        return cAtLoopbackModeLocal;

    if (mMethodsGet(mThis(self))->LoopOutIsEnabled(mThis(self)))
        return cAtLoopbackModeRemote;

    return cAtLoopbackModeRelease;
    }

static uint32 ReadOnlyCntGet(AtChannel self, uint32 counterType)
    {
    uint32 offset = mMethodsGet((ThaSdhLine)self)->DefaultOffset((ThaSdhLine)self);

    if (counterType == cAtSdhLineCounterTypeB2)
        return (mChannelHwRead(self, cThaRegOcnB2ErrRoCnt + offset, cThaModuleOcn) & cThaOcnB2ErrCntMask);
    if (counterType == cAtSdhLineCounterTypeB1)
        return (mChannelHwRead(self, cThaRegOcnB1ErrRoCnt + offset, cThaModuleOcn) & cThaB1ErrCntMask);

    return 0;
    }

/* Special case for B2 counter when BER engine for this channel is enabled */
static uint32 B2CounterGet(ThaSdhLine self, eBool read2Clear)
    {
    uint32 offset = mMethodsGet(self)->DefaultOffset(self);
    AtBerController berController = SdhLineMsBerControllerGet((AtSdhLine)self);

    /* BER engine is enabled, need to calculate base on read only register */
    if (berController && AtBerControllerIsEnabled(berController))
        {
        AtChannel channel = (AtChannel)self;
        uint32 newCounter = AtChannelReadOnlyCntGet(channel, cAtSdhLineCounterTypeB2);
        return ThaSdhChannelCounterFromReadOnlyCounterGet((AtChannel)self, newCounter, &(self->b2ErrorCnt), cThaOcnB2ErrCntMask, cAtSdhLineCounterTypeB2, read2Clear);
        }

    if (read2Clear)
        return (mChannelHwRead(self, cThaRegOcnB2ErrR2cCnt + offset, cThaModuleOcn) & cThaOcnB2ErrCntMask);

    return (mChannelHwRead(self, cThaRegOcnB2ErrRoCnt + offset, cThaModuleOcn) & cThaOcnB2ErrCntMask);
    }

/* Special case for B1 counter when BER engine for this channel is enabled */
static uint32 B1CounterGet(ThaSdhLine self, eBool read2Clear)
    {
    uint32 offset = mMethodsGet(self)->DefaultOffset(self);
    AtBerController berController = SdhLineRsBerControllerGet((AtSdhLine)self);

    /* BER engine is enabled, need to calculate base on read only register */
    if (berController && AtBerControllerIsEnabled(berController))
        {
        AtChannel channel = (AtChannel)self;
        uint32 newCounter = AtChannelReadOnlyCntGet(channel, cAtSdhLineCounterTypeB1);
        return ThaSdhChannelCounterFromReadOnlyCounterGet(channel, newCounter, &(self->b1ErrorCnt), cThaB1ErrCntMask, cAtSdhLineCounterTypeB1, read2Clear);
        }

    if (read2Clear)
        return (mChannelHwRead(self, cThaRegOcnB1ErrR2cCnt + offset, cThaModuleOcn) & cThaB1ErrCntMask);

    return (mChannelHwRead(self, cThaRegOcnB1ErrRoCnt + offset, cThaModuleOcn) & cThaB1ErrCntMask);
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    if (counterType == cAtSdhLineCounterTypeB1)
        return B1CounterGet((ThaSdhLine)self, cAtFalse);

    if (counterType == cAtSdhLineCounterTypeB2)
        return B2CounterGet((ThaSdhLine)self, cAtFalse);

    if (counterType == cAtSdhLineCounterTypeRei)
        {
        uint32 offset = mMethodsGet((ThaSdhLine)self)->DefaultOffset((ThaSdhLine)self);
        return (mChannelHwRead(self, cThaRegOcnReiLErrRoCnt + offset, cThaModuleOcn) & cThaOcnReiLErrCntMask);
        }

    /* Unknown counter */
    return 0x0;
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    if (counterType == cAtSdhLineCounterTypeB1)
        return B1CounterGet((ThaSdhLine)self, cAtTrue);

    if (counterType == cAtSdhLineCounterTypeB2)
        return B2CounterGet((ThaSdhLine)self, cAtTrue);

    if (counterType == cAtSdhLineCounterTypeRei)
        {
        uint32 offset = mMethodsGet((ThaSdhLine)self)->DefaultOffset((ThaSdhLine)self);
        return (mChannelHwRead(self, cThaRegOcnReiLErrR2cCnt + offset, cThaModuleOcn) & cThaOcnReiLErrCntMask);
        }

    /* Unknown counter */
    return 0x0;
    }

static uint32 BlockErrorCounterRead2Clear(AtSdhChannel self, uint16 counterType, eBool clear)
    {
    uint32 offset;
    uint32 regAddr = 0;

    if (!ThaModuleSdhBlockErrorCountersSupported((ThaModuleSdh)SdhModule((AtSdhLine)self)))
        return 0x0;

    offset = mMethodsGet((ThaSdhLine)self)->DefaultOffset((ThaSdhLine)self);
    if (counterType == cAtSdhLineCounterTypeB1)
        regAddr = clear ? cThaRegOcnB1BlockErrR2cCnt : cThaRegOcnB1BlockErrRoCnt;
    if (counterType == cAtSdhLineCounterTypeB2)
        regAddr = clear ? cThaRegOcnB2BlockErrR2cCnt : cThaRegOcnB2BlockErrRoCnt;
    if (counterType == cAtSdhLineCounterTypeRei)
        regAddr = clear ? cThaRegOcnReiLBlockErrR2cCnt : cThaRegOcnReiLBlockErrRoCnt;

    /* Invalid counter type */
    if (regAddr == 0)
        return 0;

    return mChannelHwRead(self, regAddr + offset, cThaModuleOcn);
    }

static uint32 BlockErrorCounterGet(AtSdhChannel self, uint16 counterType)
    {
    return BlockErrorCounterRead2Clear(self, counterType, cAtFalse);
    }

static uint32 BlockErrorCounterClear(AtSdhChannel self, uint16 counterType)
    {
    return BlockErrorCounterRead2Clear(self, counterType, cAtTrue);
    }

static void EnableOtherLines(AtSdhLine self, eBool enable)
    {
    uint8 lineId_i;
    uint8 thisLineId = (uint8)AtChannelIdGet((AtChannel)self);
    AtModuleSdh sdhModule = (AtModuleSdh)AtChannelModuleGet((AtChannel)self);
    uint8 partId = ThaModuleSdhPartOfChannel((AtSdhChannel)self);
    uint8 partStartLineId = (uint8)(partId * ThaModuleSdhNumLinesPerPart(sdhModule));
    uint8 partEndLineId = (uint8)(partStartLineId + ThaModuleSdhNumLinesPerPart(sdhModule) - 1);

    for (lineId_i = (uint8)(partStartLineId); lineId_i <= partEndLineId; lineId_i++)
        {
        AtSdhLine unusedLine;
        if (thisLineId == lineId_i)
            continue;

        unusedLine = AtModuleSdhLineGet(sdhModule, lineId_i);
        if (!enable)
            SdhChannelAllBerControllersDelete((AtSdhChannel)unusedLine);
        AtChannelEnable((AtChannel)unusedLine, enable);
        }
    }

static AtSerdesController SerdesController(AtSdhLine self)
    {
    if (mThis(self)->serdesController == NULL)
        {
        ThaModuleSdh sdhModule = (ThaModuleSdh)AtChannelModuleGet((AtChannel)self);
        mThis(self)->serdesController = ThaModuleSdhSerdesControllerCreate(sdhModule, self, SerdesIdGet(mThis(self)));
        }

    return mThis(self)->serdesController;
    }

static eBool OtherLinesHaveServiceRunning(AtSdhLine self)
    {
    uint32 lineId;
    AtModuleSdh sdhModule = (AtModuleSdh)AtChannelModuleGet((AtChannel)self);
    uint32 partId = ThaModuleSdhPartOfChannel((AtSdhChannel)self);
    uint32 partStartLineId = partId * ThaModuleSdhNumLinesPerPart(sdhModule);
    uint32 partEndLineId = partStartLineId + ThaModuleSdhNumLinesPerPart(sdhModule) - 1;
    uint8 thisLineId = (uint8)AtChannelIdGet((AtChannel)self);

    for (lineId = partStartLineId; lineId <= partEndLineId; lineId++)
        {
        if (thisLineId == lineId)
            continue;

        if (AtSdhChannelServiceIsRunning((AtSdhChannel)AtModuleSdhLineGet(sdhModule, (uint8)lineId)))
            return cAtTrue;
        }

    return cAtFalse;
    }

static eBool IsHighRateInOtherLines(ThaSdhLine self)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)AtChannelModuleGet((AtChannel)self);
    uint8 partId = ThaModuleSdhPartOfChannel((AtSdhChannel)self);
    uint8 partStartLineId = (uint8)(partId * ThaModuleSdhNumLinesPerPart(sdhModule));
    uint8 partEndLineId = (uint8)((partStartLineId + ThaModuleSdhNumLinesPerPart(sdhModule)) - 1);
    uint32 curLineId = AtChannelIdGet((AtChannel)self);
    eAtSdhLineRate highRate = ThaSdhLineHighRate(self);
    uint8 lineId;

    for (lineId = (uint8)partStartLineId; lineId <= partEndLineId; lineId++)
        {
        if (curLineId == lineId)
            continue;

        if (AtSdhLineRateGet(AtModuleSdhLineGet(sdhModule, lineId)) == highRate)
            return cAtTrue;
        }

    return cAtFalse;
    }

static eAtRet CheckRateToApply(AtSdhLine self, eAtSdhLineRate rate)
    {
    eAtSdhLineRate highRate = ThaSdhLineHighRate(mThis(self));
    eAtSdhLineRate lowRate = ThaSdhLineLowRate(mThis(self));
    
    if ((lowRate == highRate) && (rate == highRate))
        return cAtOk;

    if (rate == highRate)
        {
        if (OtherLinesHaveServiceRunning(self))
            return cAtErrorChannelBusy;
        }
        
	/* Low rate can apply on any port, but if there is a line busy on High rate, not allow */
    else
        {
        if (IsHighRateInOtherLines(mThis(self)))
            return cAtErrorResourceBusy;
        }

    return cAtOk;
    }

static eAtRet UpdateRateForOtherLines(AtSdhLine self, eAtSdhLineRate rate)
    {
    uint8 lineId;
    eAtRet ret = cAtOk;
    AtModuleSdh sdhModule = (AtModuleSdh)AtChannelModuleGet((AtChannel)self);
    uint8 partId = ThaModuleSdhPartOfChannel((AtSdhChannel)self);
    uint8 partStartLineId = (uint8)(partId * ThaModuleSdhNumLinesPerPart(sdhModule));
    uint8 partEndLineId = (uint8)((partStartLineId + ThaModuleSdhNumLinesPerPart(sdhModule)) - 1);
    uint32 curLineId = AtChannelIdGet((AtChannel)self);

    for (lineId = (uint8)partStartLineId; lineId <= partEndLineId; lineId++)
        {
        AtSdhLine line;
        if (curLineId == lineId)
            continue;

        line = AtModuleSdhLineGet(sdhModule, lineId);
        if (AtSdhLineRateIsSupported(line, rate))
            ret |= mMethodsGet(line)->RateSet(line, rate);
        }

    return ret;
    }

static eBool HasRateHwConfiguration(ThaSdhLine self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtModuleSdhRet RateSet(AtSdhLine self, eAtSdhLineRate rate)
    {
    eAtSdhLineRate currentRate;
    eAtRet ret = cAtOk;
    eAtSdhLineRate highRate = ThaSdhLineHighRate(mThis(self));
    eAtSdhLineRate lowRate  = ThaSdhLineLowRate(mThis(self));
    eBool needHwConfigure = mMethodsGet(mThis(self))->HasRateHwConfiguration(mThis(self));
    eBool multiRateAreSupported = (highRate != lowRate) ? cAtTrue : cAtFalse;

    if (!needHwConfigure)
        return m_AtSdhLineMethods->RateSet(self, rate);

    /* In warm restore let super deal with database */
    if (AtChannelDeviceInWarmRestore((AtChannel)self))
        return m_AtSdhLineMethods->RateSet(self, rate);

    if (AtSdhChannelServiceIsRunning((AtSdhChannel)self))
        return cAtErrorChannelBusy;

    ret = CheckRateToApply(self, rate);
    if (ret != cAtOk)
        return ret;

    /* if configure one line is high-rate, other lines need to be disable */
    if (multiRateAreSupported && (rate == highRate))
        EnableOtherLines(self, cAtFalse);

    /* Change the hardware */
    currentRate = AtSdhLineRateGet(self);
    ret = mMethodsGet(mThis(self))->HwRateSet(mThis(self), rate);

    /* Let super deal with database */
    if (ret == cAtOk)
        ret = m_AtSdhLineMethods->RateSet(self, rate);

    if (ret != cAtOk)
        {
        ret |= mMethodsGet(mThis(self))->HwRateSet(mThis(self), currentRate);
        return ret;
        }

    ret |= AtChannelInit((AtChannel)self);
    ret |= AtChannelEnable((AtChannel)self, cAtTrue);

    /* In case of changing from high-rate to low-rate, other low-rate lines should be enabled.
     * If not, RX/TX directions are disabled and status on these lines are
     * undetermined because of no clock */
    if ((multiRateAreSupported) && (currentRate == highRate) && (rate != highRate))
        {
        EnableOtherLines(self, cAtTrue);

        /* When changing line rate for the first port in group from STM-4 to
         * STM-1, other ports need be reset to STM-1. Otherwise, concatenation
         * of other ports are unknown. */
        ret |= UpdateRateForOtherLines(self, lowRate);
        }

    /* Also need to configure the SERDES */
    ThaSdhLineSerdesControllerRateSet((ThaSdhLineSerdesController)SerdesController(self), rate);

    if (rate != currentRate)
        {
        eBool surEnabled ;
        AtSurEngine surEngine = AtChannelSurEngineGet((AtChannel)self);
        if  (AtSurEngineIsProvisioned(surEngine) == cAtFalse)
            return ret;

        /* When change mapping type, need to de-provision and provision again */
        surEnabled = AtSurEngineIsEnabled(surEngine);
        ret |= AtSurEngineProvision(surEngine, cAtFalse);
        ret |= AtSurEngineProvision(surEngine, cAtTrue);
        ret |= AtSurEngineEnable(surEngine, surEnabled);
        }

    return ret;
    }

static eBool RateIsSupported(AtSdhLine self, eAtSdhLineRate rate)
    {
    uint8 lineId = (uint8)ThaModuleSdhLineLocalId((AtSdhLine)self);
    uint8 partId = ThaModuleSdhPartOfChannel((AtSdhChannel)self);
    AtModuleSdh sdhModule = (AtModuleSdh)AtChannelModuleGet((AtChannel)self);
    uint8 partStartLineId = (uint8)(partId * ThaModuleSdhNumLinesPerPart(sdhModule));
    eAtSdhLineRate highRate = ThaSdhLineHighRate(mThis(self));
    eAtSdhLineRate lowRate  = ThaSdhLineLowRate(mThis(self));

    /* If port 1 are working at high rate. Other lines are not used and
     * all rates are not supported */
    if (lineId != 0)
        {
        if (AtSdhLineRateGet(AtModuleSdhLineGet(sdhModule, partStartLineId)) == highRate)
            return cAtFalse;
        }

    /* All ports support low rate */
    if (rate == lowRate)
        return cAtTrue;

    /* Only port 0 support high rate */
    if ((rate == highRate) && (lineId == 0))
        return cAtTrue;

    /* Not support for other cases */
    return cAtFalse;
    }

static eAtSdhLineRate RateGet(AtSdhLine self)
    {
    if (mInAccessible(self))
        return m_AtSdhLineMethods->RateGet(self);
    return mMethodsGet(mThis(self))->HwRateGet(mThis(self));
    }

static eAtModuleSdhRet TxS1Set(AtSdhLine self, uint8 value)
    {
    uint32 regVal, address;
    uint32 offset = mMethodsGet((ThaSdhLine)self)->DefaultOffset((ThaSdhLine)self);

    /* Set enable inserting S1 pattern */
    address = cThaRegOcnTxFrmrPerChnCtrl1 + offset;
    regVal = mChannelHwRead(self, address, cThaModuleOcn);
    mFieldIns(&regVal, cThaOcnTxS1EnBMask, cThaOcnTxS1EnBShift, 0);
    mChannelHwWrite(self, address, regVal, cThaModuleOcn);

    /* Set S1 pattern value */
    address = cThaRegOcnTxFrmrPerChnCtrl2 + offset;
    mChannelHwWrite(self, address, value, cThaModuleOcn);

    m_AtSdhLineMethods->TxS1Set(self, value);

    return cAtOk;
    }

static uint8 TxS1Get(AtSdhLine self)
    {
    uint32 address;

    if (mInAccessible(self))
        return m_AtSdhLineMethods->TxS1Get(self);

    /* Get S1 pattern value */
    address = cThaRegOcnTxFrmrPerChnCtrl2 + mMethodsGet((ThaSdhLine)self)->DefaultOffset((ThaSdhLine)self);
    return mChannelHwRead(self, address, cThaModuleOcn) & cThaOcnTxS1PatMask;
    }

static uint8 RxS1Get(AtSdhLine self)
    {
    uint32 regVal, address;
    uint8 s1;

    /*  Get current S1 byte */
    address = cThaRegOcnS1MonStat + mMethodsGet((ThaSdhLine)self)->DefaultOffset((ThaSdhLine)self);
    regVal = mChannelHwRead(self, address, cThaModuleOcn);
    mFieldGet(regVal, cThaStbS1ValMask, cThaStbS1ValShift, uint8, &s1);

    return s1;
    }

static eBool HwApsIsEnabled(AtSdhLine self)
    {
    uint32 address, regVal;

    /* Check if APS function is disabled */
    address = cThaRegOcnTxFrmrPerChnCtrl1 + mMethodsGet((ThaSdhLine)self)->DefaultOffset((ThaSdhLine)self);
    regVal = mChannelHwRead(self, address, cThaModuleOcn);

    return (regVal & cThaOcnTxApsProEnMask) ? cAtTrue : cAtFalse;
    }

/* K1 */
static eAtModuleSdhRet TxK1Set(AtSdhLine self, uint8 value)
    {
    uint32 address, regVal;

    if (HwApsIsEnabled(self))
        return cAtErrorModeNotSupport;

    address = cThaRegOcnTxFrmrPerChnCtrl1 + mMethodsGet((ThaSdhLine)self)->DefaultOffset((ThaSdhLine)self);
    regVal = mChannelHwRead(self, address, cThaModuleOcn);
    mFieldIns(&regVal, cThaOcnTxK1PatMask, cThaOcnTxK1PatShift, value);
    mFieldIns(&regVal, cThaOcnTxApsEnBMask, cThaOcnTxApsEnBShift, 0x0);
    mChannelHwWrite(self, address, regVal, cThaModuleOcn);

    m_AtSdhLineMethods->TxK1Set(self, value);

    return cAtOk;
    }

static uint8 TxK1Get(AtSdhLine self)
    {
    uint32 address, regVal;
    uint8 kByte;

    address = cThaRegOcnTxFrmrPerChnCtrl1 + mMethodsGet((ThaSdhLine)self)->DefaultOffset((ThaSdhLine)self);
    regVal = mChannelHwRead(self, address, cThaModuleOcn);
    mFieldGet(regVal, cThaOcnTxK1PatMask, cThaOcnTxK1PatShift, uint8, &kByte);

    return kByte;
    }

static uint8 KBytesStableSelectedThreshold(ThaSdhLine self)
	{
    uint32 regAddr = cThaRegOcnTohMonPerChnCtrl + mMethodsGet(self)->DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);
    return (uint8)mRegField(regVal, cThaStbK1K2ThresSel);
	}

static uint8 K1StableThreshold(ThaSdhLine self)
	{
    uint8 selectedThreshold = KBytesStableSelectedThreshold(self);
    uint32 regAddr = cThaRegOcnK1StableMonThresControl + ThaSdhLinePartOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);

    if (selectedThreshold == 0)
        return (uint8)mRegField(regVal, cThaRegOcnK1StableThreshold1);

    return (uint8)mRegField(regVal, cThaRegOcnK1StableThreshold2);
	}

static uint8 RxK1Get(AtSdhLine self)
    {
    uint32 address = cThaRegOcnK1MonStat + mMethodsGet(mThis(self))->DefaultOffset(mThis(self));
    uint32 regVal  = mChannelHwRead(self, address, cThaModuleOcn);

    if (mRegField(regVal, cThaSameK1Cnt) >= K1StableThreshold(mThis(self)))
    	return (uint8)mRegField(regVal, cThaStbK1Val);

    return (uint8)mRegField(regVal, cThaCurK1);
    }

/* K2 */
static eAtModuleSdhRet TxK2Set(AtSdhLine self, uint8 value)
    {
    uint32 address, regVal;

    if (HwApsIsEnabled(self))
        return cAtErrorModeNotSupport;

    address = cThaRegOcnTxFrmrPerChnCtrl1 + mMethodsGet((ThaSdhLine)self)->DefaultOffset((ThaSdhLine)self);
    regVal = mChannelHwRead(self, address, cThaModuleOcn);
    mFieldIns(&regVal, cThaOcnTxK2PatMask, cThaOcnTxK2PatShift, value);
    mFieldIns(&regVal, cThaOcnTxApsEnBMask, cThaOcnTxApsEnBShift, 0x0);
    mChannelHwWrite(self, address, regVal, cThaModuleOcn);

    m_AtSdhLineMethods->TxK2Set(self, value);

    return cAtOk;
    }

static uint8 TxK2Get(AtSdhLine self)
    {
    uint32 address, regVal;
    uint8 kByte;

    address = cThaRegOcnTxFrmrPerChnCtrl1 + mMethodsGet((ThaSdhLine)self)->DefaultOffset((ThaSdhLine)self);
    regVal = mChannelHwRead(self, address, cThaModuleOcn);
    mFieldGet(regVal, cThaOcnTxK2PatMask, cThaOcnTxK2PatShift, uint8, &kByte);

    return kByte;
    }

static uint8 K2StableThreshold(ThaSdhLine self)
	{
    uint8 selectedThreshold = KBytesStableSelectedThreshold(self);
    uint32 regAddr = cThaRegOcnK2StableMonThresControl + ThaSdhLinePartOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);
    if (selectedThreshold == 0)
        return (uint8)mRegField(regVal, cThaRegOcnK2StableThreshold1);
    return (uint8)mRegField(regVal, cThaRegOcnK2StableThreshold2);
	}

static uint8 RxK2Get(AtSdhLine self)
    {
    uint32 regAddr = cThaRegOcnK2MonStat + mMethodsGet(mThis(self))->DefaultOffset(mThis(self));
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);

    if (mRegField(regVal, cThaSameK2Cnt) >= K2StableThreshold(mThis(self)))
    	return (uint8)mRegField(regVal, cThaStbK2Val);

    return (uint8)mRegField(regVal, cThaCurK2);
    }

static eAtModuleSdhRet TxTtiGet(AtSdhChannel self, tAtSdhTti *tti)
    {
    if (mInAccessible(self))
        return m_AtSdhChannelMethods->TxTtiGet(self, tti);
    return ThaTtiProcessorTxTtiGet(mTtiProcessor(self), tti);
    }

static eAtModuleSdhRet TxTtiSet(AtSdhChannel self, const tAtSdhTti *tti)
    {
    eAtRet ret = ThaTtiProcessorTxTtiSet(mTtiProcessor(self), tti);

    m_AtSdhChannelMethods->TxTtiSet(self, tti);

    return ret;
    }

static eAtModuleSdhRet ExpectedTtiGet(AtSdhChannel self, tAtSdhTti *tti)
    {
    if (mInAccessible(self))
        return m_AtSdhChannelMethods->ExpectedTtiGet(self, tti);
    return ThaTtiProcessorExpectedTtiGet(mTtiProcessor(self), tti);
    }

static eAtModuleSdhRet ExpectedTtiSet(AtSdhChannel self, const tAtSdhTti *tti)
    {
    eAtRet ret = ThaTtiProcessorExpectedTtiSet(mTtiProcessor(self), tti);
    m_AtSdhChannelMethods->ExpectedTtiSet(self, tti);
    return ret;
    }

static eAtModuleSdhRet RxTtiGet(AtSdhChannel self, tAtSdhTti *tti)
    {
    return ThaTtiProcessorRxTtiGet(mTtiProcessor(self), tti);
    }

static eAtModuleSdhRet TimMonitorEnable(AtSdhChannel self, eBool enable)
    {
    return ThaTtiProcessorMonitorEnable(mTtiProcessor(self), enable);
    }

static eBool TimMonitorIsEnabled(AtSdhChannel self)
    {
    return ThaTtiProcessorMonitorIsEnabled(mTtiProcessor(self));
    }

static eAtModuleSdhRet TtiCompareEnable(AtSdhChannel self, eBool enable)
    {
    return ThaTtiProcessorCompareEnable(mTtiProcessor(self), enable);
    }

static eBool TtiCompareIsEnabled(AtSdhChannel self)
    {
    return ThaTtiProcessorCompareIsEnabled(mTtiProcessor(self));
    }

static eBool CanChangeAlarmAffecting(AtSdhChannel self, uint32 alarmType)
    {
    AtUnused(self);
    if (alarmType == cAtSdhLineAlarmTim)
        return cAtTrue;

    return cAtFalse;
    }

static uint32 DefaultAlarmAffecting(AtSdhChannel self)
    {
    static const uint32 alarms = cAtSdhLineAlarmLos |
                                 cAtSdhLineAlarmOof |
                                 cAtSdhLineAlarmLof |
                                 cAtSdhLineAlarmAis;
    AtUnused(self);
    return alarms;
    }

static eAtModuleSdhRet AlarmAffectingEnable(AtSdhChannel self, uint32 alarmType, eBool enable)
    {
    if (alarmType & cAtSdhLineAlarmTim)
        return ThaTtiProcessorTimAffectingEnable(mTtiProcessor(self), enable);

    /* Hardware Default enable */
    if (alarmType & DefaultAlarmAffecting(self))
    	return enable ? cAtOk: cAtErrorModeNotSupport;

    return cAtErrorModeNotSupport;
    }

static eBool AlarmAffectingIsEnabled(AtSdhChannel self, uint32 alarmType)
    {
    if (alarmType & cAtSdhLineAlarmTim)
        return ThaTtiProcessorTimAffectingIsEnabled(mTtiProcessor(self));

    if (alarmType & DefaultAlarmAffecting(self))
    	return cAtTrue;

    return cAtFalse;
    }

static uint32 AlarmAffectingMaskGet(AtSdhChannel self)
    {
    uint32 alarms = DefaultAlarmAffecting(self);
    if (ThaTtiProcessorTimAffectingIsEnabled(mTtiProcessor(self)))
        alarms |= cAtSdhLineAlarmTim;

    return alarms;
    }

static eAtModuleSdhRet LedStateSet(AtSdhLine self, eAtLedState ledState)
    {
    uint32 regVal;
    uint32 regAddr = cThaRegOcnLedCtrl + ThaSdhLinePartOffset((ThaSdhLine)self);
    uint8 portId = (uint8)ThaModuleSdhLineLocalId(self);
    uint8 force;
    uint8 state;

    if (!AtLedStateIsValid(ledState))
        return cAtErrorModeNotSupport;

    regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    force = (ledState == cAtLedStateBlink) ? 0 : 1;
    mFieldIns(&regVal,
              cThaRegLineLedStateForceMask(portId),
              cThaRegLineLedStateForceShift(portId),
              force);
    state = (ledState == cAtLedStateOn) ? 1 : 0;
    mFieldIns(&regVal,
              cThaRegLineLedStateStatMask(portId),
              cThaRegLineLedStateStatShift(portId),
              state);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eAtLedState LedStateGet(AtSdhLine self)
    {
    uint32 regVal;
    eBool forced, isOn;
    uint8 portId = (uint8)ThaModuleSdhLineLocalId((AtSdhLine)self);

    regVal = mChannelHwRead(self, cThaRegOcnLedCtrl + ThaSdhLinePartOffset((ThaSdhLine)self), cThaModuleOcn);
    forced = (regVal & cThaRegLineLedStateForceMask(portId)) ? cAtTrue : cAtFalse;
    isOn   = (regVal & cThaRegLineLedStateStatMask(portId))  ? cAtTrue : cAtFalse;

    if (forced)
        return isOn ? cAtLedStateOn : cAtLedStateOff;
    else
        return cAtLedStateBlink;
    }

static eAtModuleSdhRet ScrambleEnable(AtSdhLine self, eBool enable)
    {
    uint32 regAddr, regVal;
    uint32 offset = mMethodsGet((ThaSdhLine)self)->DefaultOffset((ThaSdhLine)self);

    /* RX */
    regAddr = cThaRegOcnRxFrmrPerChnCtrl + offset;
    regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);
    mRegFieldSet(regVal, cThaOcnRxScrEnB, enable ? 0 : 1);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    /* TX */
    regAddr = cThaRegOcnTxFrmrPerChnCtrl1 + offset;
    regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);
    mRegFieldSet(regVal, cThaOcnTxScrEnB, enable ? 0 : 1);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eBool ScrambleIsEnabled(AtSdhLine self)
    {
    uint32 regAddr, regVal;
    uint32 offset = mMethodsGet((ThaSdhLine)self)->DefaultOffset((ThaSdhLine)self);

    regAddr = cThaRegOcnRxFrmrPerChnCtrl + offset;
    regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);

    return (regVal * cThaOcnRxScrEnBMask) ? cAtFalse : cAtTrue;
    }

static AtSdhLine SwitchedLineGet(AtSdhLine self)
    {
    return mThis(self)->switchToLine;
    }

static AtSdhLine BridgedLineGet(AtSdhLine self)
    {
    return mThis(self)->bridgeToLine;
    }

static eAtRet CopyHwConfiguration(AtSdhLine source, AtSdhLine dest)
    {
    uint32 regVal, regAddr;
    uint8 sourceLocalLine   = (uint8)ThaModuleSdhLineLocalId(source);
    uint32 sourcePartOffset = ThaSdhLinePartOffset((ThaSdhLine)source);
    uint8 destLocalLine     = (uint8)ThaModuleSdhLineLocalId(dest);
    uint32 destPartOffset   = ThaSdhLinePartOffset((ThaSdhLine)dest);
    uint8 isStm4, isEnabled;

    /* Copy line rate for Tx and Rx */
    regAddr = cThaRegOcnRxLineRateCtrl + destPartOffset;
    regVal  = mChannelHwRead(source, regAddr, cThaModuleOcn);
    mFieldGet(regVal, cThaOcnRxLineRateMask(sourceLocalLine), cThaOcnRxLineRateShift(sourceLocalLine), uint8, &isStm4);
    mFieldIns(&regVal, cThaOcnRxLineRateMask(destLocalLine), cThaOcnRxLineRateShift(destLocalLine), isStm4);
    mChannelHwWrite(dest, regAddr, regVal, cThaModuleOcn);

    regAddr = cThaRegOcnTxLineRateCtrl + destPartOffset;
    regVal  = mChannelHwRead(source, regAddr, cThaModuleOcn);
    mFieldIns(&regVal, cThaOcnTxLineRateMask(destLocalLine), cThaOcnTxLineRateShift(destLocalLine), isStm4);
    mChannelHwWrite(dest, regAddr, regVal, cThaModuleOcn);

    /* Copy enabling for Tx and Rx */
    regAddr = cThaRegOcnRxLineEnCtrl + destPartOffset;
    regVal  = mChannelHwRead(source, cThaRegOcnRxLineEnCtrl + sourcePartOffset, cThaModuleOcn);
    mFieldGet(regVal, cThaOcnRxLineEnbMask(sourceLocalLine), cThaOcnRxLineEnbShift(sourceLocalLine), uint8, &isEnabled);
    mFieldIns(&regVal, cThaOcnRxLineEnbMask(destLocalLine), cThaOcnRxLineEnbShift(destLocalLine), isEnabled);
    mChannelHwWrite(dest, regAddr, regVal, cThaModuleOcn);

    regAddr = cThaRegOcnTxLineEnCtrl + destPartOffset;
    regVal  = mChannelHwRead(source, regAddr, cThaModuleOcn);
    mFieldIns(&regVal, cThaOcnTxLineEnbMask(destLocalLine), cThaOcnTxLineEnbShift(destLocalLine), isEnabled);
    mChannelHwWrite(dest, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eAtModuleSdhRet Switch(AtSdhLine self, AtSdhLine toLine)
    {
    uint32 fromLineLocalId = ThaModuleSdhLineLocalId(self);
    uint32 toLineLocalId   = ThaModuleSdhLineLocalId(toLine);
    uint32 regAddr, regVal;

    /* Release switch */
    if ((toLine == NULL) || (self == toLine))
        return AtSdhLineSwitchRelease(self);

    /* Switch already was made */
    if (mThis(self)->switchToLine == toLine)
        return cAtOk;

    /* They are all busy */
    if ((mThis(self)->switchToLine   || mThis(self)->switchFromLine) ||
        (mThis(toLine)->switchToLine || mThis(toLine)->switchFromLine))
        return cAtErrorChannelBusy;

    /* Make sure that two of them have the same rate and enabling */
    CopyHwConfiguration(self, toLine);

    regAddr = cThaRegOcnRxLineSourceCtrl + ThaSdhLinePartOffset((ThaSdhLine)self);
    regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);
    if (ThaModuleSdhPartOfChannel((AtSdhChannel)self) == ThaModuleSdhPartOfChannel((AtSdhChannel)toLine))
        mFieldIns(&regVal,
                  cThaOcnRxLineSourceMask(fromLineLocalId),
                  cThaOcnRxLineSourceShift(fromLineLocalId),
                  toLineLocalId);
    else
        {
        uint8 numLinesPerPart = ThaModuleSdhNumLinesPerPart((AtModuleSdh)AtChannelModuleGet((AtChannel)self));
        mFieldIns(&regVal,
                  cThaOcnRxLineSourceMask(fromLineLocalId),
                  cThaOcnRxLineSourceShift(fromLineLocalId),
                  toLineLocalId + numLinesPerPart);
        }

    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    mThis(self)->switchToLine     = toLine;
    mThis(toLine)->switchFromLine = self;

    return cAtOk;
    }

static eAtModuleSdhRet SwitchRelease(AtSdhLine self)
    {
    uint32 fromLineLocalId;
    uint32 regAddr, regVal;
    AtSdhLine switchedLine = AtSdhLineSwitchedLineGet(self);

    if (switchedLine == NULL)
        return cAtOk;

    regAddr = cThaRegOcnRxLineSourceCtrl + ThaSdhLinePartOffset((ThaSdhLine)self);
    regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);
    fromLineLocalId = ThaModuleSdhLineLocalId(self);
    mFieldIns(&regVal,
              cThaOcnRxLineSourceMask(fromLineLocalId),
              cThaOcnRxLineSourceShift(fromLineLocalId),
              fromLineLocalId);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    mThis(self)->switchToLine           = NULL;
    mThis(switchedLine)->switchFromLine = NULL;

    return cAtOk;
    }

static eAtModuleSdhRet Bridge(AtSdhLine self, AtSdhLine toLine)
    {
    AtSdhLine bridgedLine = AtSdhLineBridgedLineGet(self);
    uint32 fromLineLocalId, toLineLocalId;
    uint32 regAddr, regVal;

    /* Release bridge */
    if ((toLine == NULL) || (toLine == self))
        return AtSdhLineSwitchRelease(self);

    /* Bridge exist */
    if (bridgedLine == toLine)
        return cAtOk;

    /* All of them are busy */
    if ((mThis(self)->bridgeToLine   || mThis(self)->bridgeFromLine) ||
        (mThis(toLine)->bridgeToLine || mThis(toLine)->bridgeFromLine))
        return cAtErrorChannelBusy;

    /* Check if destination line is bridging traffic for another line */
    if (mThis(toLine)->bridgeFromLine)
        {
        uint32 sourceId = AtChannelIdGet((AtChannel)self);
        uint32 destId   = AtChannelIdGet((AtChannel)toLine);
        AtChannelLog((AtChannel)self,
                     cAtLogLevelCritical, AtSourceLocation,
                     "Cannot bridge from line %d to %d, line %d is bridging traffic for line %d\r\n",
                     sourceId, destId,
                     destId, AtChannelIdGet((AtChannel)mThis(toLine)->bridgeFromLine));
        return cAtErrorChannelBusy;
        }

    /* Make sure that two of them have the same rate and enabling */
    CopyHwConfiguration(self, toLine);

    fromLineLocalId = ThaModuleSdhLineLocalId(self);
    toLineLocalId   = ThaModuleSdhLineLocalId(toLine);
    regAddr         = cThaRegOcnTxFrmSrcCtrl + ThaSdhLinePartOffset((ThaSdhLine)toLine);
    regVal          = mChannelHwRead(toLine, regAddr, cThaModuleOcn);
    if (ThaModuleSdhPartOfChannel((AtSdhChannel)self) == ThaModuleSdhPartOfChannel((AtSdhChannel)toLine))
        mFieldIns(&regVal,
                  cThaOcnRxLineSourceMask(toLineLocalId),
                  cThaOcnRxLineSourceShift(toLineLocalId),
                  fromLineLocalId);
    else
        {
        uint8 numLinesPerPart = ThaModuleSdhNumLinesPerPart((AtModuleSdh)AtChannelModuleGet((AtChannel)self));
        mFieldIns(&regVal,
                  cThaOcnRxLineSourceMask(toLineLocalId),
                  cThaOcnRxLineSourceShift(toLineLocalId),
                  fromLineLocalId + numLinesPerPart);
        }

    mChannelHwWrite(toLine, regAddr, regVal, cThaModuleOcn);

    /* Update database */
    mThis(self)->bridgeToLine     = toLine;
    mThis(toLine)->bridgeFromLine = self;

    return cAtOk;
    }

static eAtModuleSdhRet BridgeRelease(AtSdhLine self)
    {
    AtSdhLine bridgedLine = AtSdhLineBridgedLineGet(self);
    uint32 bridgedLineLocalId;
    uint32 regAddr, regVal;

    /* Not bridged yet */
    if (bridgedLine == NULL)
        return cAtOk;

    regAddr = cThaRegOcnTxFrmSrcCtrl + ThaSdhLinePartOffset((ThaSdhLine)bridgedLine);
    regVal  = mChannelHwRead(bridgedLine, regAddr, cThaModuleOcn);
    bridgedLineLocalId = ThaModuleSdhLineLocalId(bridgedLine);
    mFieldIns(&regVal,
              cThaOcnRxLineSourceMask(bridgedLineLocalId),
              cThaOcnRxLineSourceShift(bridgedLineLocalId),
              bridgedLineLocalId);
    mChannelHwWrite(bridgedLine, regAddr, regVal, cThaModuleOcn);

    /* Reset database */
    mThis(self)->bridgeToLine          = NULL;
    mThis(bridgedLine)->bridgeFromLine = NULL;

    return cAtOk;
    }

static eBool IsSimulated(ThaSdhLine self)
    {
    return AtDeviceIsSimulated(AtChannelDeviceGet((AtChannel)self));
    }

static eAtModuleSdhRet BerControllersReCreate(AtSdhLine self)
    {
    uint32 berAlarms = cAtSdhLineAlarmBerSd   |
                       cAtSdhLineAlarmBerSf   |
                       cAtSdhLineAlarmRsBerSd |
                       cAtSdhLineAlarmRsBerSf;

    /* Need to clear hardware current status */
    if (BerHardwareInterruptIsSupported((AtChannel)self))
        ThaSdhLineBerStatusLatch((ThaSdhLine)self, berAlarms, 0);

    return m_AtSdhLineMethods->BerControllersReCreate(self);
    }

static eAtRet BindToPseudowire(AtChannel self, AtPw pwAdapter)
    {
    AtDevice device = AtChannelDeviceGet(self);
    ThaModulePda modulePda = (ThaModulePda)AtDeviceModuleGet(device, cThaModulePda);
    ThaModulePwe modulePwe = (ThaModulePwe)AtDeviceModuleGet(device, cThaModulePwe);
    AtSdhLine line = (AtSdhLine)self;
    AtPw realPw = ThaPwAdapterPwGet((ThaPwAdapter)pwAdapter);

    /* Let modules do binding */
    eAtRet ret = ThaModulePdaBindLineToPw(modulePda, line, pwAdapter);
    ret |= ThaModulePweBindLineToPw(modulePwe, line, pwAdapter);

    if (ret == cAtOk)
        AtChannelBoundPwSet(self, realPw);

    return ret;
    }

static uint32 HwIdGet(AtChannel self)
    {
    return HwLineIdGet((ThaSdhLine)self);
    }

static void LatchedAlarmsDisplay(ThaSdhLine self)
    {
    uint32 alarms = self->latchedAlarms;

    AtPrintc(cSevWarning, "* Latched alarms:");

    if (alarms & cAtSdhLineAlarmLos)         AtPrintc(cSevCritical, " LOS");
    if (alarms & cAtSdhLineAlarmOof)         AtPrintc(cSevCritical, " OOF");
    if (alarms & cAtSdhLineAlarmLof)         AtPrintc(cSevCritical, " LOF");
    if (alarms & cAtSdhLineAlarmTim)         AtPrintc(cSevCritical, " TIM");
    if (alarms & cAtSdhLineAlarmAis)         AtPrintc(cSevCritical, " AIS");
    if (alarms & cAtSdhLineAlarmRdi)         AtPrintc(cSevCritical, " RDI");
    if (alarms & cAtSdhLineAlarmBerSd)       AtPrintc(cSevCritical, " MS-SD");
    if (alarms & cAtSdhLineAlarmBerSf)       AtPrintc(cSevCritical, " MS-SF");
    if (alarms & cAtSdhLineAlarmS1Change)    AtPrintc(cSevCritical, " S1Changed");
    if (alarms & cAtSdhLineAlarmRsBerSd)     AtPrintc(cSevCritical, " RS-SD");
    if (alarms & cAtSdhLineAlarmRsBerSf)     AtPrintc(cSevCritical, " RS-SF");
    if (alarms & cAtSdhLineAlarmK1Change)    AtPrintc(cSevCritical, " K1Change");
    if (alarms & cAtSdhLineAlarmK2Change)    AtPrintc(cSevCritical, " K2Change");

    AtPrintc(cSevNormal, "\r\n");
    }

static eAtRet Debug(AtChannel self)
    {
    m_AtChannelMethods->Debug(self);

    AtPrintc(cSevNormal, "* Version: %d\r\n", mThis(self)->version);

    if (mThis(self)->latchedAlarms)
        LatchedAlarmsDisplay(mThis(self));

    return cAtOk;
    }

static uint32 SfLMask(ThaSdhLine self)
    {
    return mZeroVersion(self) ? cBit12 : cBit9;
    }

static uint32 SfLShift(ThaSdhLine self)
    {
    return mZeroVersion(self) ? 12 : 9;
    }

static uint32 SdLMask(ThaSdhLine self)
    {
    return mZeroVersion(self) ? cBit11 : cBit8;
    }

static uint32 SdLShift(ThaSdhLine self)
    {
    return mZeroVersion(self) ? 11 : 8;
    }

static uint32 SfSMask(ThaSdhLine self)
    {
    return mZeroVersion(self) ? cBit10 : cBit7;
    }

static uint32 SfSShift(ThaSdhLine self)
    {
    return mZeroVersion(self) ? 10 : 7;
    }

static uint32 SdSMask(ThaSdhLine self)
    {
    return mZeroVersion(self) ? cBit9 : cBit6;
    }

static uint32 SdSShift(ThaSdhLine self)
    {
    return mZeroVersion(self) ? 9 : 6;
    }

static uint32 S1ChgMask(ThaSdhLine self)
    {
    return mZeroVersion(self) ? cBit8 : cBit12;
    }

static uint32 S1ChgShift(ThaSdhLine self)
    {
    return mZeroVersion(self) ? 8 : 12;
    }

static uint32 K1ChgMask(ThaSdhLine self)
    {
    return mZeroVersion(self) ? cBit7 : cBit11;
    }

static uint32 K1ChgShift(ThaSdhLine self)
    {
    return mZeroVersion(self) ? 7 : 11;
    }

static uint32 K2ChgMask(ThaSdhLine self)
    {
    return mZeroVersion(self) ? cBit5 : cBit10;
    }

static uint32 K2ChgShift(ThaSdhLine self)
    {
    return mZeroVersion(self) ? 5 : 10;
    }

static uint8 CurrentStatusStartBit(ThaSdhLine self)
    {
    return mZeroVersion(self) ? 0 : 13;
    }

static eBool StickyRegisterIsRead2Clear(ThaSdhLine self)
    {
    return mZeroVersion(self) ? cAtFalse : cAtTrue;
    }

static uint32 AlarmStickyAddress(ThaSdhLine self, eBool read2Clear)
    {
    if (mZeroVersion(self))
        return cThaRegOcnRxLineperAlmIntrStat + mMethodsGet(self)->DefaultOffset(self);
    else
        {
        uint32 offset  = mMethodsGet(self)->DefaultOffset(self);
        uint32 address = read2Clear ? cThaRegOcnRxLineperAlmIntrStat : cThaRegOcnRxLineperAlmCurrentStat;
        return address + offset;
        }
    }

static eAtRet HwRateSet(ThaSdhLine self, eAtSdhLineRate rate)
    {
    return ThaOcnLineRateSet((AtSdhLine)self, rate);
    }

static eAtSdhLineRate HwRateGet(ThaSdhLine self)
    {
    return ThaOcnLineRateGet((AtSdhLine)self);
    }

static eAtRet InterruptAndDefectGetV0(AtSdhChannel self, uint32 *changedAlarms, uint32 *currentStatus, eBool read2Clear)
    {
    ThaSdhLine line = mThis(self);
    uint32 hwAlarm = HwAlarmGet(line);
    AtChannel channel = (AtChannel)line;

    *currentStatus = mMethodsGet(line)->RsRealAlarmGet(line, hwAlarm) |
                     mMethodsGet(line)->MsRealAlarmGet(line, hwAlarm);
    *changedAlarms = read2Clear ? AtChannelDefectInterruptClear(channel) : AtChannelDefectInterruptGet(channel);

    return cAtOk;
    }

static eAtRet InterruptAndDefectGetV1(AtSdhChannel self, uint32 *changedAlarms, uint32 *currentStatus, eBool read2Clear)
    {
    uint32 stickyValue;
    ThaSdhLine sdhLine = (ThaSdhLine)self;

    *changedAlarms = AlarmHistoryRead2Clear(sdhLine, read2Clear, &stickyValue);
    *currentStatus = AlarmFromHwAlarm(sdhLine, stickyValue);

    return cAtOk;
    }

static eAtRet InterruptAndDefectGet(AtSdhChannel self, uint32 *changedDefects, uint32 *currentStatus, eBool read2Clear)
    {
    if (mZeroVersion(self))
        return InterruptAndDefectGetV0(self, changedDefects, currentStatus, read2Clear);
    else
        return InterruptAndDefectGetV1(self, changedDefects, currentStatus, read2Clear);
    }

static eAtSdhLineRate LowRate(ThaSdhLine self)
    {
    AtUnused(self);
    return cAtSdhLineRateStm1;
    }

static eAtSdhLineRate HighRate(ThaSdhLine self)
    {
    AtUnused(self);
    return cAtSdhLineRateStm4;
    }

static eAtRet TxEnable(AtChannel self, eBool enable)
    {
    uint8 hwLineId = HwLineIdGet((ThaSdhLine)self);
    uint32 regAddr = cThaRegOcnTxLineEnCtrl + ThaSdhLinePartOffset((ThaSdhLine)self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);

    mFieldIns(&regVal, cThaOcnTxLineEnbMask(hwLineId), cThaOcnTxLineEnbShift(hwLineId), enable ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    m_AtChannelMethods->TxEnable(self, enable);

    return cAtOk;
    }

static eBool TxIsEnabled(AtChannel self)
    {
    uint32 regVal;

    if (mInAccessible(self))
        return m_AtChannelMethods->TxIsEnabled(self);

    regVal = mChannelHwRead(self, cThaRegOcnTxLineEnCtrl + ThaSdhLinePartOffset((ThaSdhLine)self), cThaModuleOcn);
    return ((regVal & cThaOcnTxLineEnbMask(HwLineIdGet((ThaSdhLine)self))) ? cAtTrue : cAtFalse);
    }

static eAtRet RxEnable(AtChannel self, eBool enable)
    {
    uint8 hwLineId = HwLineIdGet((ThaSdhLine)self);
    uint32 regAddr, regVal;
    uint32 partOffset = ThaSdhLinePartOffset((ThaSdhLine)self);

    /* Do not allow application disable RX direction now, if disabled, there is
     * no clock and the receive status is undetermined */
    if (enable)
        {
        regAddr = cThaRegOcnRxLineEnCtrl + partOffset;
        regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);
        mFieldIns(&regVal, cThaOcnRxLineEnbMask(hwLineId), cThaOcnRxLineEnbShift(hwLineId), enable ? 1 : 0);
        mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);
        }

    /* Also enable/disable PW packets sent to PSN side */
    if (ThaModuleSdhCanDisablePwPacketsToPsn((ThaModuleSdh)SdhModule((AtSdhLine)self)))
        {
        regAddr = cThaOcnRxLineOutputControl + partOffset;
        regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);
        mFieldIns(&regVal, cThaOcnRxLOut2DwnstEnMask(hwLineId), cThaOcnRxLOut2DwnstEnShift(hwLineId), enable ? 1 : 0);
        mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);
        }

    m_AtChannelMethods->RxEnable(self, enable);

    return cAtOk;
    }

static eBool RxIsEnabled(AtChannel self)
    {
    uint32 regVal;
    uint32 partOffset = ThaSdhLinePartOffset(mThis(self));
    uint32 hwLineId = HwLineIdGet(mThis(self));

    if (mInAccessible(self))
        return m_AtChannelMethods->RxIsEnabled(self);

    if (ThaModuleSdhCanDisablePwPacketsToPsn((ThaModuleSdh)SdhModule((AtSdhLine)self)))
        {
        regVal = mChannelHwRead(self, cThaOcnRxLineOutputControl + partOffset, cThaModuleOcn);
        return ((regVal & cThaOcnRxLOut2DwnstEnMask(hwLineId)) ? cAtTrue : cAtFalse);
        }

    regVal = mChannelHwRead(self, cThaRegOcnRxLineEnCtrl + partOffset, cThaModuleOcn);
    return ((regVal & cThaOcnRxLineEnbMask(hwLineId)) ? cAtTrue : cAtFalse);
    }

static eAtRet AutoRdiEnable(AtSdhChannel self, eBool enable)
    {
	AtUnused(self);
    return enable ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool AutoRdiIsEnabled(AtSdhChannel self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static eAtRet AutoReiEnable(AtSdhChannel self, eBool enable)
    {
    AtUnused(self);
    return enable ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool AutoReiIsEnabled(AtSdhChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static AtSurEngine *SurEngineAddress(AtChannel self)
    {
    return &mThis(self)->surEngine;
    }

static AtSurEngine SurEngineObjectCreate(AtChannel self)
    {
    /* Only standard STMm/OCn has FM/PM function */
    if (!mMethodsGet(mThis(self))->HasToh(mThis(self)))
        return NULL;

    return m_AtChannelMethods->SurEngineObjectCreate(self);
    }

static eAtRet ExtendedKByteEnable(ThaSdhLine self, eBool enable)
    {
    AtUnused(self);
    return enable ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool ExtendedKByteIsEnabled(ThaSdhLine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet ExtendedKBytePositionSet(ThaSdhLine self, eThaSdhLineExtendedKBytePosition position)
    {
    AtUnused(self);
    AtUnused(position);
    return cAtErrorModeNotSupport;
    }

static eThaSdhLineExtendedKBytePosition ExtendedKBytePositionGet(ThaSdhLine self)
    {
    AtUnused(self);
    return cThaSdhLineExtendedKBytePositionInvalid;
    }

static eAtRet KByteSourceSet(ThaSdhLine self, eThaSdhLineKByteSource source)
	{
    AtUnused(self);
    return (source == cThaSdhLineKByteSourceCpu) ? cAtOk : cAtErrorModeNotSupport;
	}

static eThaSdhLineKByteSource KByteSourceGet(ThaSdhLine self)
	{
    AtUnused(self);
    return cThaSdhLineKByteSourceCpu;
	}

static eAtRet EK1TxSet(ThaSdhLine self, uint8 value)
	{
    AtUnused(self);
    AtUnused(value);
    return cAtErrorModeNotSupport;
	}

static eAtRet EK2TxSet(ThaSdhLine self, uint8 value)
	{
    AtUnused(self);
    AtUnused(value);
    return cAtErrorModeNotSupport;
	}

static uint8  EK1TxGet(ThaSdhLine self)
	{
    AtUnused(self);
    return 0xFF;
	}

static uint8  EK2TxGet(ThaSdhLine self)
	{
    AtUnused(self);
    return 0xFF;
	}

static uint8  EK1RxGet(ThaSdhLine self)
    {
    AtUnused(self);
    return 0xFF;
    }

static uint8  EK2RxGet(ThaSdhLine self)
    {
    AtUnused(self);
    return 0xFF;
    }

static eAtRet WarmRestore(AtChannel self)
    {
    AtSdhLine line = (AtSdhLine)self;
    eAtRet ret;

    /* Re-set current rate to build sub channel */
    eAtSdhLineRate currentRate = AtSdhLineRateGet(line);
    ret = AtSdhLineRateSet(line, currentRate);

    /* Restore subChannel */
    ret |= AtChannelWarmRestore((AtChannel)AtSdhChannelSubChannelGet((AtSdhChannel)self, 0));

    if (ret != cAtOk)
        return ret;

    /* Don't know how to get line mode, just set to SDH mode as default */
    ((AtSdhChannel)self)->mode = cAtSdhChannelModeSdh;

    /* Restore TTI processor */
    return ThaTtiProcessorWarmRestore(mTtiProcessor(self));
    }

static eBool HasToh(ThaSdhLine self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaSdhLine object = (ThaSdhLine)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(version);
    mEncodeObject(ttiProcessor);
    mEncodeNone(b1ErrorCnt);
    mEncodeNone(b2ErrorCnt);
    mEncodeObject(serdesController);
    mEncodeNone(latchedAlarms);

    mEncodeChannelIdString(switchToLine);
    mEncodeChannelIdString(switchFromLine);
    mEncodeChannelIdString(bridgeToLine);
    mEncodeChannelIdString(bridgeFromLine);

    mEncodeObject(linePrbsEngine);
    mEncodeNone(surEngine); /* It was done at AtChannel, just pass policy scanning. */
    mEncodeNone(attController);
    }

static eBool ExtendedKBytePositionIsValid(ThaSdhLine self, eThaSdhLineExtendedKBytePosition position)
    {
    AtUnused(self);

    switch (position)
        {
        case cThaSdhLineExtendedKBytePositionD1Sts4  : return cAtTrue;
        case cThaSdhLineExtendedKBytePositionD1Sts10 : return cAtTrue;

        case cThaSdhLineExtendedKBytePositionInvalid :
        default:
            return cAtFalse;
        }
    }

static uint32 DiagnosticBaseAddress(ThaSdhLine self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static void MethodsInit(ThaSdhLine self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* SERDES Loopback */
        mMethodOverride(m_methods, SerdesLoopInEnable);
        mMethodOverride(m_methods, SerdesLoopInIsEnabled);
        mMethodOverride(m_methods, SerdesLoopoutEnable);
        mMethodOverride(m_methods, SerdesLoopoutIsEnabled);

        /* FPGA loopback */
        mMethodOverride(m_methods, FpgaLoopOutIsApplicable);
        mMethodOverride(m_methods, FpgaLoopOutEnable);
        mMethodOverride(m_methods, FpgaLoopOutIsEnable);
        mMethodOverride(m_methods, FpgaLoopInEnable);
        mMethodOverride(m_methods, FpgaLoopInIsEnable);

        /* All loopback */
        mMethodOverride(m_methods, LoopInEnable);
        mMethodOverride(m_methods, LoopInIsEnabled);
        mMethodOverride(m_methods, LoopOutEnable);
        mMethodOverride(m_methods, LoopOutIsEnabled);

        /* Alarms */
        mMethodOverride(m_methods, CurrentStatusStartBit);
        mMethodOverride(m_methods, StickyRegisterIsRead2Clear);
        mMethodOverride(m_methods, AlarmStickyAddress);

        mMethodOverride(m_methods, DefaultOffset);

        mMethodOverride(m_methods, SfLMask);
        mMethodOverride(m_methods, SfLShift);
        mMethodOverride(m_methods, SdLMask);
        mMethodOverride(m_methods, SdLShift);
        mMethodOverride(m_methods, SfSMask);
        mMethodOverride(m_methods, SfSShift);
        mMethodOverride(m_methods, SdSMask);
        mMethodOverride(m_methods, SdSShift);
        mMethodOverride(m_methods, S1ChgMask);
        mMethodOverride(m_methods, S1ChgShift);
        mMethodOverride(m_methods, K1ChgMask);
        mMethodOverride(m_methods, K1ChgShift);
        mMethodOverride(m_methods, K2ChgMask);
        mMethodOverride(m_methods, K2ChgShift);
        mMethodOverride(m_methods, DiagnosticBaseAddress);

        mMethodOverride(m_methods, LowRate);
        mMethodOverride(m_methods, HighRate);
        mMethodOverride(m_methods, HasRateHwConfiguration);
        mMethodOverride(m_methods, HasToh);
        mMethodOverride(m_methods, TohMonitoringDefaultSet);
        mMethodOverride(m_methods, HwRateSet);
        mMethodOverride(m_methods, HwRateGet);
        mMethodOverride(m_methods, HwAlarmGet);
        mMethodOverride(m_methods, RsRealAlarmGet);
        mMethodOverride(m_methods, MsRealAlarmGet);
        mMethodOverride(m_methods, ExtendedKByteEnable);
        mMethodOverride(m_methods, ExtendedKByteIsEnabled);
        mMethodOverride(m_methods, ExtendedKBytePositionSet);
        mMethodOverride(m_methods, ExtendedKBytePositionGet);
        mMethodOverride(m_methods, KByteSourceSet);
        mMethodOverride(m_methods, KByteSourceGet);
        mMethodOverride(m_methods, EK1TxSet);
        mMethodOverride(m_methods, EK2TxSet);
        mMethodOverride(m_methods, EK1TxGet);
        mMethodOverride(m_methods, EK2TxGet);
        mMethodOverride(m_methods, EK1RxGet);
        mMethodOverride(m_methods, EK2RxGet);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtSdhLine(AtSdhLine self)
    {
    AtSdhLine line = (AtSdhLine)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhLineMethods = mMethodsGet(line);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhLineOverride, m_AtSdhLineMethods, sizeof(m_AtSdhLineOverride));

        mMethodOverride(m_AtSdhLineOverride, RateSet);
        mMethodOverride(m_AtSdhLineOverride, RateIsSupported);
        mMethodOverride(m_AtSdhLineOverride, RateGet);
        mMethodOverride(m_AtSdhLineOverride, TxS1Set);
        mMethodOverride(m_AtSdhLineOverride, TxS1Get);
        mMethodOverride(m_AtSdhLineOverride, RxS1Get);
        mMethodOverride(m_AtSdhLineOverride, TxK1Set);
        mMethodOverride(m_AtSdhLineOverride, TxK1Get);
        mMethodOverride(m_AtSdhLineOverride, RxK1Get);
        mMethodOverride(m_AtSdhLineOverride, TxK2Set);
        mMethodOverride(m_AtSdhLineOverride, TxK2Get);
        mMethodOverride(m_AtSdhLineOverride, RxK2Get);
        mMethodOverride(m_AtSdhLineOverride, LedStateGet);
        mMethodOverride(m_AtSdhLineOverride, LedStateSet);
        mMethodOverride(m_AtSdhLineOverride, ScrambleEnable);
        mMethodOverride(m_AtSdhLineOverride, ScrambleIsEnabled);
        mMethodOverride(m_AtSdhLineOverride, SerdesController);
        mMethodOverride(m_AtSdhLineOverride, SwitchedLineGet);
        mMethodOverride(m_AtSdhLineOverride, BridgedLineGet);
        mMethodOverride(m_AtSdhLineOverride, Switch);
        mMethodOverride(m_AtSdhLineOverride, SwitchRelease);
        mMethodOverride(m_AtSdhLineOverride, Bridge);
        mMethodOverride(m_AtSdhLineOverride, BridgeRelease);
        mMethodOverride(m_AtSdhLineOverride, BerControllersReCreate);
        mMethodOverride(m_AtSdhLineOverride, PrbsEngineGet);
        }

    mMethodsSet(line, &m_AtSdhLineOverride);
    }

static void OverrideAtSdhChannel(AtSdhLine self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, mMethodsGet(channel), sizeof(m_AtSdhChannelOverride));
        mMethodOverride(m_AtSdhChannelOverride, TxTtiGet);
        mMethodOverride(m_AtSdhChannelOverride, TxTtiSet);
        mMethodOverride(m_AtSdhChannelOverride, ExpectedTtiGet);
        mMethodOverride(m_AtSdhChannelOverride, ExpectedTtiSet);
        mMethodOverride(m_AtSdhChannelOverride, RxTtiGet);
        mMethodOverride(m_AtSdhChannelOverride, AlarmAffectingEnable);
        mMethodOverride(m_AtSdhChannelOverride, AlarmAffectingIsEnabled);
        mMethodOverride(m_AtSdhChannelOverride, AlarmAffectingMaskGet);
        mMethodOverride(m_AtSdhChannelOverride, TimMonitorEnable);
        mMethodOverride(m_AtSdhChannelOverride, TimMonitorIsEnabled);
        mMethodOverride(m_AtSdhChannelOverride, BlockErrorCounterGet);
        mMethodOverride(m_AtSdhChannelOverride, BlockErrorCounterClear);
        mMethodOverride(m_AtSdhChannelOverride, InterruptAndDefectGet);
        mMethodOverride(m_AtSdhChannelOverride, AutoRdiEnable);
        mMethodOverride(m_AtSdhChannelOverride, AutoRdiIsEnabled);
        mMethodOverride(m_AtSdhChannelOverride, AutoReiEnable);
        mMethodOverride(m_AtSdhChannelOverride, AutoReiIsEnabled);
        mMethodOverride(m_AtSdhChannelOverride, CanChangeAlarmAffecting);
        mMethodOverride(m_AtSdhChannelOverride, TtiCompareEnable);
        mMethodOverride(m_AtSdhChannelOverride, TtiCompareIsEnabled);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void OverrideAtChannel(AtSdhLine self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));
        mMethodOverride(m_AtChannelOverride, Init);

        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, IsEnabled);
        mMethodOverride(m_AtChannelOverride, TxEnable);
        mMethodOverride(m_AtChannelOverride, TxIsEnabled);
        mMethodOverride(m_AtChannelOverride, RxEnable);
        mMethodOverride(m_AtChannelOverride, RxIsEnabled);

        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, TxAlarmForce);
        mMethodOverride(m_AtChannelOverride, TxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcedAlarmGet);
        mMethodOverride(m_AtChannelOverride, RxAlarmForce);
        mMethodOverride(m_AtChannelOverride, RxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, RxForcedAlarmGet);
        mMethodOverride(m_AtChannelOverride, TxErrorForce);
        mMethodOverride(m_AtChannelOverride, TxErrorUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcedErrorGet);
        mMethodOverride(m_AtChannelOverride, LoopbackSet);
        mMethodOverride(m_AtChannelOverride, LoopbackGet);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, ReadOnlyCntGet);
        mMethodOverride(m_AtChannelOverride, BindToPseudowire);
        mMethodOverride(m_AtChannelOverride, HwIdGet);
        mMethodOverride(m_AtChannelOverride, Debug);
        mMethodOverride(m_AtChannelOverride, WarmRestore);
        mMethodOverride(m_AtChannelOverride, RxForcibleAlarmsGet);
        mMethodOverride(m_AtChannelOverride, TxForcibleAlarmsGet);
        mMethodOverride(m_AtChannelOverride, SurEngineAddress);
        mMethodOverride(m_AtChannelOverride, AttController);
        mMethodOverride(m_AtChannelOverride, SurEngineObjectCreate);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtObject(AtSdhLine self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtSdhLine self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtSdhChannel(self);
    OverrideAtSdhLine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaSdhLine);
    }

AtSdhLine ThaSdhLineObjectInit(AtSdhLine self, uint32 channelId, AtModuleSdh module, uint8 version)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtSdhLineObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->version = version;

    return self;
    }

AtSdhLine ThaSdhLineNew(uint32 channelId, AtModuleSdh module, uint8 version)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhLine newLine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newLine == NULL)
        return NULL;

    /* Construct it */
    return ThaSdhLineObjectInit(newLine, channelId, module, version);
    }

/*
 * This function is get TTI message
 *
 * @param tti         - Input TTI message and its mode
 * @param pTtiMsg     - Output to point to buffer
 * @param pHwJ0Md     - Output to get value of message mode
 * @param pJ0Len      - Output to get TTI message length
 *
 * @return cAtOk      - If success
 */
eAtRet ThaTtiMsgGet(const tAtSdhTti *tti, uint8 *pTtiMsg, uint8 *pHwJ0Md, uint8 *pJ0Len)
    {
    uint8 i, length = 0;

    switch(tti->mode)
        {
        case cAtSdhTtiMode1Byte:
            length = cThaOcnJnLenInByteMd;
            if (pTtiMsg)
                pTtiMsg[0] = tti->message[0];
            break;

        /* TTI 16-byte message */
        case cAtSdhTtiMode16Byte:
            length = cThaOcnJnLenIn16ByteMd;
            if (pTtiMsg)
                {
                for (i = 0; i < length; i++)
                    pTtiMsg[i] = tti->message[i];

                AtCrc7Ins(pTtiMsg, length);
                }
            break;

        /* TTI 64-byte message */
        case cAtSdhTtiMode64Byte:
            length = cThaOcnJnLenIn64ByteMd;
            if (pTtiMsg)
                {
                for (i = 0; i < length; i++)
                    pTtiMsg[i] = tti->message[i];
                pTtiMsg[length - 2]  = cSonetCr;
                pTtiMsg[length - 1]  = cSonetLf;
                }
            break;

        /* Return error */
        default:
            return cAtErrorInvlParm;
        }

    if (pHwJ0Md)
        *pHwJ0Md = tti->mode;
    if (pJ0Len)
        *pJ0Len = length;

    return cAtOk;
    }

uint32 ThaSdhLinePartOffset(ThaSdhLine self)
    {
    ThaDevice device = (ThaDevice)AtChannelDeviceGet((AtChannel)self);
    return ThaDeviceModulePartOffset(device, cThaModuleOcn, ThaModuleSdhPartOfChannel((AtSdhChannel)self));
    }

void ThaSdhLineBerStatusLatch(ThaSdhLine self, uint32 changedAlarms, uint32 currentStatus)
    {
    uint32 regAddr     = cThaRegOcnSDSFLineSectionInterruptStatusLatchControl + ThaSdhLinePartOffset(self);
    uint32 regVal      = 0;
    uint32 localLineId = ThaModuleSdhLineLocalId((AtSdhLine)self);
    uint32 mask;

    if (self == NULL)
        return;

    regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);

    if (changedAlarms & cAtSdhLineAlarmRsBerSd)
        {
        mask   = cThaOcnSectSdLatchMask(localLineId);
        regVal = (currentStatus & cAtSdhLineAlarmRsBerSd) ? (regVal | mask) : (regVal & (~mask));
        }

    if (changedAlarms & cAtSdhLineAlarmRsBerSf)
        {
        mask   = cThaOcnSectSfLatchMask(localLineId);
        regVal = (currentStatus & cAtSdhLineAlarmRsBerSf) ? (regVal | mask) : (regVal & (~mask));
        }

    if (changedAlarms & cAtSdhLineAlarmBerSd)
        {
        mask   = cThaOcnLineSdLatchMask(localLineId);
        regVal = (currentStatus & cAtSdhLineAlarmBerSd) ? (regVal | mask) : (regVal & (~mask));
        }

    if (changedAlarms & cAtSdhLineAlarmBerSf)
        {
        mask   = cThaOcnLineSfLatchMask(localLineId);
        regVal = (currentStatus & cAtSdhLineAlarmBerSf) ? (regVal | mask) : (regVal & (~mask));
        }

    mChannelHwWrite(self, regAddr, regVal, cAtModuleSdh);

    /* Need also to latch current status in case of simulate */
    if (IsSimulated(self))
        {
        regAddr = cThaRegOcnRxLineperAlmCurrentStat + mMethodsGet(self)->DefaultOffset(self);
        regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);

        if (changedAlarms & cAtSdhLineAlarmRsBerSd)
            {
            mask   = mMethodsGet(mThis(self))->SdSMask(mThis(self));
            regVal = (currentStatus & cAtSdhLineAlarmRsBerSd) ? (regVal | mask) : (regVal & (~mask));
            }

        if (changedAlarms & cAtSdhLineAlarmRsBerSf)
            {
            mask   = mMethodsGet(mThis(self))->SfSMask(mThis(self));
            regVal = (currentStatus & cAtSdhLineAlarmRsBerSf) ? (regVal | mask) : (regVal & (~mask));
            }

        if (changedAlarms & cAtSdhLineAlarmBerSd)
            {
            mask   = mMethodsGet(mThis(self))->SdLMask(mThis(self));
            regVal = (currentStatus & cAtSdhLineAlarmBerSd) ? (regVal | mask) : (regVal & (~mask));
            }

        if (changedAlarms & cAtSdhLineAlarmBerSf)
            {
            mask   = mMethodsGet(mThis(self))->SfLMask(mThis(self));
            regVal = (currentStatus & cAtSdhLineAlarmBerSf) ? (regVal | mask) : (regVal & (~mask));
            }

        mChannelHwWrite(self, regAddr, regVal, cAtModuleSdh);
        }
    }

uint32 ThaSdhLineBerDefectGet(ThaSdhLine self)
    {
    uint32 almCurrentStat = 0;

    if (self == NULL)
        return 0;

    /* Get hardware status for instead */
    if (BerHardwareInterruptIsSupported((AtChannel)self))
        {
        uint32 address = cThaRegOcnRxLineperAlmCurrentStat + mMethodsGet(self)->DefaultOffset(self);
        uint32 regVal  = mChannelHwRead(self, address, cThaModuleOcn);
        uint8 startBit = mMethodsGet(self)->CurrentStatusStartBit(self);
        AtBerController berController;

        /* MS BER */
        berController = AtSdhLineMsBerControllerGet((AtSdhLine)self);
        if (berController && AtBerControllerIsEnabled(berController))
            {
            if (regVal & (mMethodsGet(mThis(self))->SfLMask(mThis(self)) << startBit))
                almCurrentStat |= cAtSdhLineAlarmBerSf;
            if (regVal & (mMethodsGet(mThis(self))->SdLMask(mThis(self)) << startBit))
                almCurrentStat |= cAtSdhLineAlarmBerSd;
            }

        /* RS BER */
        berController = AtSdhLineRsBerControllerGet((AtSdhLine)self);
        if (berController && AtBerControllerIsEnabled(berController))
            {
            if (regVal & (mMethodsGet(mThis(self))->SfSMask(mThis(self)) << startBit))
                almCurrentStat |= cAtSdhLineAlarmRsBerSf;
            if (regVal & (mMethodsGet(mThis(self))->SdSMask(mThis(self)) << startBit))
                almCurrentStat |= cAtSdhLineAlarmRsBerSd;
            }
        }

    /* Get software status if BER hardware interrupt is not supported */
    else
        {
        AtBerController berController;

        /* Get RS SD, SF */
        berController = SdhLineRsBerControllerGet((AtSdhLine)self);
        if (AtBerControllerIsSd(berController))
            almCurrentStat |= cAtSdhLineAlarmRsBerSd;
        if (AtBerControllerIsSf(berController))
            almCurrentStat |= cAtSdhLineAlarmRsBerSf;

        /* Get MS SD, SF */
        berController = SdhLineMsBerControllerGet((AtSdhLine)self);
        if (AtBerControllerIsSd(berController))
            almCurrentStat |= cAtSdhLineAlarmBerSd;
        if (AtBerControllerIsSf(berController))
            almCurrentStat |= cAtSdhLineAlarmBerSf;
        if (AtBerControllerIsTca(berController))
            almCurrentStat |= cAtSdhLineAlarmBerTca;
        }

    return almCurrentStat;
    }

uint32 ThaSdhLineLatchedAlarmsGet(ThaSdhLine self)
    {
    return self ? self->latchedAlarms : 0;
    }

void ThaSdhLineLatchedAlarmsSet(ThaSdhLine self, uint32 alarms)
    {
    if (self)
        self->latchedAlarms = alarms;
    }

eAtSdhLineRate ThaSdhLineLowRate(ThaSdhLine self)
    {
    if (self)
        return mMethodsGet(self)->LowRate(self);
    return cAtSdhLineRateInvalid;
    }

eAtSdhLineRate ThaSdhLineHighRate(ThaSdhLine self)
    {
    if (self)
        return mMethodsGet(self)->HighRate(self);
    return cAtSdhLineRateInvalid;
    }

ThaTtiProcessor ThaSdhLineTtiProcessor(AtSdhLine self)
    {
    return TtiProcessor(self);
    }

eAtRet ThaSdhLineExtendedKByteEnable(ThaSdhLine self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->ExtendedKByteEnable(self, enable);
    return cAtErrorObjectNotExist;
    }

eBool ThaSdhLineExtendedKByteIsEnabled(ThaSdhLine self)
    {
    if (self)
        return mMethodsGet(self)->ExtendedKByteIsEnabled(self);
    return cAtFalse;
    }

eAtRet ThaSdhLineExtendedKBytePositionSet(ThaSdhLine self, eThaSdhLineExtendedKBytePosition position)
    {
    if (self == NULL)
        return cAtErrorObjectNotExist;

    if (ExtendedKBytePositionIsValid(self, position))
        return mMethodsGet(self)->ExtendedKBytePositionSet(self, position);

    return cAtErrorInvlParm;
    }

eThaSdhLineExtendedKBytePosition ThaSdhLineExtendedKBytePositionGet(ThaSdhLine self)
    {
    if (self)
        return mMethodsGet(self)->ExtendedKBytePositionGet(self);
    return cThaSdhLineExtendedKBytePositionInvalid;
    }

uint32 ThaSdhLineDiagnosticBaseAddress(ThaSdhLine self)
    {
    if (self)
        return mMethodsGet(self)->DiagnosticBaseAddress(self);
    return cInvalidUint32;
    }

eAtRet ThaSdhLineKByteSourceSet(ThaSdhLine self, eThaSdhLineKByteSource source)
    {
    if (self)
        return mMethodsGet(self)->KByteSourceSet(self, source);
    return cAtErrorNullPointer;
    }

eThaSdhLineKByteSource ThaSdhLineKByteSourceGet(ThaSdhLine self)
    {
    if (self)
        return mMethodsGet(self)->KByteSourceGet(self);
    return cThaSdhLineKByteSourceUnknown;
    }

eAtRet ThaSdhLineEK1TxSet(ThaSdhLine self, uint8 value)
    {
    if (self)
        return mMethodsGet(self)->EK1TxSet(self, value);
    return cAtErrorNullPointer;
    }

eAtRet ThaSdhLineEK2TxSet(ThaSdhLine self, uint8 value)
    {
    if (self)
        return mMethodsGet(self)->EK2TxSet(self, value);
    return cAtErrorNullPointer;
    }

uint8 ThaSdhLineEK1TxGet(ThaSdhLine self)
    {
    if (self)
        return mMethodsGet(self)->EK1TxGet(self);
    return cInvalidUint8;
    }

uint8 ThaSdhLineEK2TxGet(ThaSdhLine self)
    {
    if (self)
        return mMethodsGet(self)->EK2TxGet(self);
    return cInvalidUint8;
    }

uint8 ThaSdhLineEK1RxGet(ThaSdhLine self)
    {
    if (self)
        return mMethodsGet(self)->EK1RxGet(self);
    return cInvalidUint8;
    }

uint8 ThaSdhLineEK2RxGet(ThaSdhLine self)
    {
    if (self)
        return mMethodsGet(self)->EK2RxGet(self);
    return cInvalidUint8;
    }

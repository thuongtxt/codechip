/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : ThaSdhTu3Vc.c
 *
 * Created Date: Oct 19, 2012
 *
 * Description : Thalassa TU-3 VC
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaModuleSdh.h"
#include "ThaSdhVcInternal.h"
#include "ThaModuleSdhReg.h"

#include "../cdr/ThaModuleCdrStm.h"
#include "../pdh/ThaStmModulePdh.h"
#include "../pdh/ThaPdhDe3Internal.h"
#include "../ocn/ThaModuleOcnInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaSdhVcMethods     m_ThaSdhVcOverride;
static tAtSdhChannelMethods m_AtSdhChannelOverride;
static tAtSdhPathMethods    m_AtSdhPathOverride;
static tThaSdhAuVcMethods   m_ThaSdhAuVcOverride;
static tAtChannelMethods    m_AtChannelOverride;
static tAtSdhVcMethods      m_AtSdhVcOverride;

/* To save super implementation */
static const tAtSdhChannelMethods *m_AtSdhChannelMethods = NULL;
static const tThaSdhAuVcMethods   *m_ThaSdhAuVcMethodss  = NULL;
static const tAtChannelMethods    *m_AtChannelMethods = NULL;
static const tAtSdhVcMethods      *m_AtSdhVcMethods      = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 Tu3DefaultOffset(AtSdhChannel tu3)
    {
    uint8 stsId = AtSdhChannelSts1Get(tu3);
    static const uint8 cVtgDontCare = 0;
    static const uint8 cVtDontCare = 0;
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)tu3);

    if (ocnModule == NULL)
        return 0;

    return mMethodsGet(ocnModule)->StsVtDefaultOffset(ocnModule, tu3, stsId, cVtgDontCare, cVtDontCare);
    }

static ThaModuleOcn OcnModule(AtSdhChannel self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaModuleOcn)AtDeviceModuleGet(device, cThaModuleOcn);
    }

static eAtRet ThaOcnTu3PathInit(AtSdhChannel tu3)
    {
    ThaModuleOcnVtTerminate(OcnModule(tu3), tu3, AtSdhChannelSts1Get(tu3), 0, 0, 1);
    return cAtOk;
    }

static eAtModuleSdhRet MapTypeSet(AtSdhChannel self, uint8 mapType)
    {
    eAtRet ret;

    if(!AtSdhChannelNeedToUpdateMapType(self, mapType))
        return cAtOk;

    /* Let the super do first */
    ret = m_AtSdhChannelMethods->MapTypeSet(self, mapType);
    if (ret != cAtOk)
        return ret;

    if (AtChannelDeviceInWarmRestore((AtChannel)self))
        return cAtOk;

    /* Additional configuration */
    if ((mapType == cAtSdhVcMapTypeVc3MapDe3) ||
        (mapType == cAtSdhVcMapTypeVc3MapC3))
        ret |= ThaOcnTu3PathInit(AtSdhChannelParentChannelGet(self));

    return ret;
    }

static eAtRet Ds3Map(ThaSdhAuVc self)
    {
    AtPdhDe3 de3;
    eAtRet ret = cAtOk;

    ret = ThaOcnTug3PldSet((AtSdhChannel)self, cAtSdhTugMapTypeTug3MapVc3);
    if (ret != cAtOk)
        return ret;

    de3 = (AtPdhDe3)SdhVcPdhChannelGet((AtSdhVc)self);
    ret |= ThaOcnVc3PldSet((AtSdhChannel)self, AtPdhDe3IsDs3(de3) ? cThaOcnVc3PldDs3 : cThaOcnVc3PldE3);

    return ret;
    }

static eAtRet ThaOcnTu3TerminateSet(AtSdhChannel tu3)
    {
    uint32 regAddr, regVal;
    uint32 tu3DefaultOffset = Tu3DefaultOffset(tu3);

    /* Terminate */
    ThaOcnVtPohInsertEnable(tu3, AtSdhChannelSts1Get(tu3), 0, 0, cAtTrue);

    regAddr = cThaRegOcnVtTUPIPerChnCtrl + tu3DefaultOffset;
    regVal = 0;
    mChannelHwWrite(tu3, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static AtEncapBinder EncapBinder(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return AtDeviceEncapBinder(device);
    }

static eAtRet BindToEncapChannel(AtChannel self, AtEncapChannel encapChannel)
    {
    eAtRet ret = cAtOk;

    ret |= ThaOcnTu3TerminateSet(AtSdhChannelParentChannelGet((AtSdhChannel)self));
    ret |= ThaPdhVcBypassMapMdSet((AtSdhVc)self);

    /* Let super do remaining thing */
    ret |= m_AtChannelMethods->BindToEncapChannel(self, encapChannel);

    return ret;
    }

static uint32 EncapHwIdAllocate(AtChannel self)
    {
    return AtEncapBinderTu3VcEncapHwIdAllocate(EncapBinder(self), (AtSdhVc)self);
    }

static uint8 PppBlockRange(AtChannel self)
    {
	AtUnused(self);
    return 3;
    }

static eAtRet BindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    eAtRet ret = cAtOk;

    ret |= m_AtChannelMethods->BindToPseudowire(self, pseudowire);

    /* Nothing to do more, everything is done at superclass */
    return ret;
    }

static uint8 MlpppBlockRange(AtChannel self)
    {
    static uint8 cInvalidRange = 0xFF;

    if (AtSdhChannelTypeGet((AtSdhChannel)self) == cAtSdhChannelTypeVc4) return 7;
    if (AtSdhChannelTypeGet((AtSdhChannel)self) == cAtSdhChannelTypeVc3) return 4;

    return cInvalidRange;
    }

static eAtClockState HwClockStateTranslate(AtChannel self, uint8 hwState)
    {
    ThaModuleCdr moduleCdr = (ThaModuleCdr)AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModuleCdr);

    return ThaModuleCdrClockStateFromLoChannelHwStateGet(moduleCdr, hwState);
    }

static eBool MapTypeIsSupported(AtSdhChannel self, uint8 mapType)
    {
    if (mapType == cAtSdhVcMapTypeVc3Map7xTug2s)
        return cAtFalse;

    return m_AtSdhChannelMethods->MapTypeIsSupported(self, mapType);
    }

static ThaModuleConcate ModuleConcate(AtChannel self)
    {
    return (ThaModuleConcate)AtDeviceModuleGet(AtChannelDeviceGet(self), cAtModuleConcate);
    }

static eAtRet BindToSourceGroup(AtChannel self, AtConcateGroup group)
    {
    return ThaModuleConcateBindTu3VcToSourceConcateGroup(ModuleConcate(self), self, group);
    }

static eAtRet BindToSinkGroup(AtChannel self, AtConcateGroup group)
    {
    return ThaModuleConcateBindTu3VcToSinkConcateGroup(ModuleConcate(self), self, group);
    }

static void OverrideAtChannel(AtSdhVc self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));
        mMethodOverride(m_AtChannelOverride, BindToEncapChannel);
        mMethodOverride(m_AtChannelOverride, PppBlockRange);
        mMethodOverride(m_AtChannelOverride, BindToPseudowire);
        mMethodOverride(m_AtChannelOverride, MlpppBlockRange);
        mMethodOverride(m_AtChannelOverride, EncapHwIdAllocate);
        mMethodOverride(m_AtChannelOverride, HwClockStateTranslate);
        mMethodOverride(m_AtChannelOverride, BindToSourceGroup);
        mMethodOverride(m_AtChannelOverride, BindToSinkGroup);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtSdhChannel(AtSdhVc self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(m_AtSdhChannelOverride));

        mMethodOverride(m_AtSdhChannelOverride, MapTypeSet);
        mMethodOverride(m_AtSdhChannelOverride, MapTypeIsSupported);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static ThaPohProcessor PohProcessorCreate(ThaSdhVc self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    ThaModulePoh pohModule = (ThaModulePoh)AtDeviceModuleGet(device, cThaModulePoh);

    return ThaModulePohTu3VcPohProcessorCreate(pohModule, (AtSdhVc)self);
    }

static ThaCdrController CdrControllerCreate(ThaSdhVc self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    ThaModuleCdr cdrModule = (ThaModuleCdr)AtDeviceModuleGet(device, cThaModuleCdr);
    return ThaModuleCdrTu3VcCdrControllerCreate(cdrModule, (AtSdhVc)self);
    }

static eAtRet PohInsertionEnable(AtSdhPath self, eBool enabled)
    {
    AtSdhChannel channel = (AtSdhChannel)self;
    uint8 sts1 = AtSdhChannelSts1Get(channel);
    ThaOcnVtPohInsertEnable(channel, sts1, 0, 0, enabled);
    return cAtOk;
    }

static eBool PohInsertionIsEnabled(AtSdhPath self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;
    uint8 sts1 = AtSdhChannelSts1Get(channel);
    return ThaOcnVtPohInsertIsEnabled(channel, sts1, 0, 0);
    }

static eAtRet CanChangeStuffing(AtSdhVc self, eBool enable)
    {
    AtUnused(enable);

    if (AtChannelServiceIsRunning((AtChannel)self))
        return cAtErrorChannelBusy;

    return cAtOk;
    }

static eAtRet StuffEnable(AtSdhVc self, eBool enable)
    {
    eAtRet ret;
    ThaSdhVc vc = (ThaSdhVc)self;

    if (enable == AtSdhVcStuffIsEnabled(self))
        return cAtOk;

    ret = AtSdhVcCanChangeStuffing(self, enable);
    if (ret != cAtOk)
        return ret;

    if (mMethodsGet(vc)->HasOcnStuffing(vc))
        return ThaOcnTu3VcToVc3Enable((AtSdhChannel)self, enable);

    return m_AtSdhVcMethods->StuffEnable(self, enable);
    }

static eBool StuffIsEnabled(AtSdhVc self)
    {
    ThaSdhVc vc = (ThaSdhVc)self;

    if (mMethodsGet(vc)->HasOcnStuffing(vc))
        return ThaOcnTu3VcToVc3IsEnabled((AtSdhChannel)self);

    return m_AtSdhVcMethods->StuffIsEnabled(self);
    }

static void OverrideAtSdhPath(AtSdhVc self)
    {
    AtSdhPath path = (AtSdhPath)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhPathOverride, mMethodsGet(path), sizeof(m_AtSdhPathOverride));

        mMethodOverride(m_AtSdhPathOverride, PohInsertionEnable);
        mMethodOverride(m_AtSdhPathOverride, PohInsertionIsEnabled);
        }

    mMethodsSet(path, &m_AtSdhPathOverride);
    }

static void OverrideAtSdhVc(AtSdhVc self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhVcMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhVcOverride, mMethodsGet(self), sizeof(m_AtSdhVcOverride));
        mMethodOverride(m_AtSdhVcOverride, CanChangeStuffing);
        mMethodOverride(m_AtSdhVcOverride, StuffEnable);
        mMethodOverride(m_AtSdhVcOverride, StuffIsEnabled);
        }

    mMethodsSet(self, &m_AtSdhVcOverride);
    }

static void OverrideThaSdhAuVc(AtSdhVc self)
    {
    ThaSdhAuVc sdhAuVc = (ThaSdhAuVc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaSdhAuVcMethodss = mMethodsGet(sdhAuVc);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhAuVcOverride, m_ThaSdhAuVcMethodss, sizeof(m_ThaSdhAuVcOverride));
        mMethodOverride(m_ThaSdhAuVcOverride, Ds3Map);
        }

    mMethodsSet(sdhAuVc, &m_ThaSdhAuVcOverride);
    }

static void OverrideThaSdhVc(AtSdhVc self)
    {
    ThaSdhVc thaVc = (ThaSdhVc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhVcOverride, mMethodsGet(thaVc), sizeof(m_ThaSdhVcOverride));
        mMethodOverride(m_ThaSdhVcOverride, PohProcessorCreate);
        mMethodOverride(m_ThaSdhVcOverride, CdrControllerCreate);
        }

    mMethodsSet(thaVc, &m_ThaSdhVcOverride);
    }

static void Override(AtSdhVc self)
    {
    OverrideThaSdhVc(self);
    OverrideAtSdhChannel(self);
    OverrideAtSdhPath(self);
    OverrideThaSdhAuVc(self);
    OverrideAtChannel(self);
    OverrideAtSdhVc(self);
    }

AtSdhVc ThaSdhTu3VcObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaSdhTu3Vc));

    /* Super constructor */
    if (ThaSdhAuVcObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhVc ThaSdhTu3VcNew(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhVc newVc = mMethodsGet(osal)->MemAlloc(osal, sizeof(tThaSdhTu3Vc));
    if (newVc == NULL)
        return NULL;

    /* Construct it */
    return ThaSdhTu3VcObjectInit(newVc, channelId, channelType, module);
    }

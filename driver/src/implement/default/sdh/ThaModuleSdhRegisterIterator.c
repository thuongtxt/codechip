/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : ThaModuleSdhRegisterIterator.c
 *
 * Created Date: Jan 6, 2013
 *
 * Description : SDH register iterator
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtRegister.h"

#include "../../../util/AtIteratorInternal.h"
#include "ThaModuleSdhRegisterIteratorInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtIteratorMethods m_AtIteratorOverride;
static tAtModuleRegisterIteratorMethods m_AtModuleRegisterIteratorOverride;

static const tAtModuleRegisterIteratorMethods *m_AtModuleRegisterIteratorMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaModuleSdhRegisterIterator);
    }

static uint8 DefaultCore(AtModuleRegisterIterator self)
    {
    /* May change this */
    return m_AtModuleRegisterIteratorMethods->DefaultCore(self);
    }

static uint32 MaxNumChannels(AtModuleRegisterIterator self)
    {
    /* May change this */
    return m_AtModuleRegisterIteratorMethods->MaxNumChannels(self);
    }

static uint32 NextOffset(AtModuleRegisterIterator self)
    {
    /* May change this */
    return m_AtModuleRegisterIteratorMethods->NextOffset(self);
    }

static eBool NoLineLeft(AtModuleRegisterIterator self)
    {
    ThaModuleSdhRegisterIterator iterator = (ThaModuleSdhRegisterIterator)self;
    AtModuleSdh sdhModule = (AtModuleSdh)AtModuleRegisterIteratorModuleGet(self);
    return (iterator->lineId >= AtModuleSdhMaxLinesGet(sdhModule)) ? cAtTrue : cAtFalse;
    }

static eBool NoStsLeft(AtModuleRegisterIterator self)
    {
    ThaModuleSdhRegisterIterator iterator = (ThaModuleSdhRegisterIterator)self;

    if ((iterator->slice == cMaxNumSlices) &&
        (iterator->sts   == cMaxNumSts))
        return cAtTrue;

    return cAtFalse;
    }

static eBool NoVtLeft(AtModuleRegisterIterator self)
    {
    ThaModuleSdhRegisterIterator iterator = (ThaModuleSdhRegisterIterator)self;

    if ((iterator->slice == cMaxNumSlices)   &&
        (iterator->sts   == cMaxNumSts)      &&
        (iterator->vtg   == cMaxNumVtgInSts) &&
        (iterator->vt    == cMaxNumVtInVtg))
        return cAtTrue;

    return cAtFalse;
    }

static eBool NoChannelLeft(AtModuleRegisterIterator self)
    {
    ThaModuleSdhRegisterIterator iterator = (ThaModuleSdhRegisterIterator)self;

    if (ThaModuleSdhRegisterIteratorCurrentRegType(iterator) == cSdhRegTypeLine)
        return NoLineLeft(self);
    if (ThaModuleSdhRegisterIteratorCurrentRegType(iterator) == cSdhRegTypeSts)
        return NoStsLeft(self);
    if (ThaModuleSdhRegisterIteratorCurrentRegType(iterator) == cSdhRegTypeVt)
        return NoVtLeft(self);

    return cAtTrue;
    }

static void ResetChannel(AtModuleRegisterIterator self)
    {
    /* May change this */
    m_AtModuleRegisterIteratorMethods->ResetChannel(self);
    }

static AtObject NextGet(AtIterator self)
    {
	AtUnused(self);
    /* TODO: Find a next register and update the local variable reg */
    return NULL;
    }

static void OverrideAtIterator(AtIterator self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtIteratorOverride, mMethodsGet(self), sizeof(m_AtIteratorOverride));
        mMethodOverride(m_AtIteratorOverride, NextGet);
        }

    mMethodsSet(self, &m_AtIteratorOverride);
    }

static void OverrideAtModuleRegisterIterator(AtIterator self)
    {
    AtModuleRegisterIterator iterator = (AtModuleRegisterIterator)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleRegisterIteratorMethods = mMethodsGet(iterator);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleRegisterIteratorOverride, mMethodsGet(iterator), sizeof(m_AtModuleRegisterIteratorOverride));
        mMethodOverride(m_AtModuleRegisterIteratorOverride, MaxNumChannels);
        mMethodOverride(m_AtModuleRegisterIteratorOverride, NextOffset);
        mMethodOverride(m_AtModuleRegisterIteratorOverride, NoChannelLeft);
        mMethodOverride(m_AtModuleRegisterIteratorOverride, ResetChannel);
        mMethodOverride(m_AtModuleRegisterIteratorOverride, DefaultCore);
        }

    mMethodsSet(iterator, &m_AtModuleRegisterIteratorOverride);
    }

static void Override(AtIterator self)
    {
    OverrideAtIterator(self);
    OverrideAtModuleRegisterIterator(self);
    }

AtIterator ThaModuleSdhRegisterIteratorObjectInit(AtIterator self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    if (AtModuleRegisterIteratorObjectInit((AtModuleRegisterIterator)self, module) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    return self;
    }

uint8 ThaModuleSdhRegisterIteratorCurrentRegType(ThaModuleSdhRegisterIterator self)
    {
    return ((ThaModuleSdhRegisterIterator)self)->currentRegType;
    }

void ThaModuleSdhRegisterIteratorCurrentRegTypeSet(ThaModuleSdhRegisterIterator self, uint8 regType)
    {
    ((ThaModuleSdhRegisterIterator)self)->currentRegType = regType;
    }

uint8 ThaModuleSdhRegisterIteratorNextLine(ThaModuleSdhRegisterIterator self)
    {
    self->lineId = (uint8)(self->lineId + 1);
    return self->lineId;
    }

uint8 ThaModuleSdhRegisterIteratorLineGet(ThaModuleSdhRegisterIterator self)
    {
    return self->lineId;
    }

void ThaModuleSdhRegisterIteratorNextSts(ThaModuleSdhRegisterIterator self,
                                         uint8 *slice,
                                         uint8 *sts)
    {
    ThaModuleSdhRegisterIterator iterator = (ThaModuleSdhRegisterIterator)self;

    iterator->sts = (uint8)(iterator->sts + 1);

    /* Next slice */
    if (iterator->sts == cMaxNumSts)
        {
        iterator->slice = (uint8)(iterator->slice + 1);
        iterator->sts   = 0;
        }

    *slice = iterator->slice;
    *sts   = iterator->sts;
    }

void ThaModuleSdhRegisterIteratorNextVt(ThaModuleSdhRegisterIterator self,
                                        uint8 *slice,
                                        uint8 *sts,
                                        uint8 *vtg,
                                        uint8 *vt)
    {
    ThaModuleSdhRegisterIterator iterator = (ThaModuleSdhRegisterIterator)self;

    *slice = iterator->slice;
    *sts   = iterator->sts;
    *vtg   = iterator->vtg;
    *vt    = iterator->vt;

    *vt = iterator->vt = (uint8)(iterator->vt + 1);
    if (iterator->vt <= cMaxNumVtInVtg)
        return;
    *vt = iterator->vt = 0;

    *vtg = iterator->vtg = (uint8)(iterator->vtg + 1);
    if (iterator->vtg < cMaxNumVtgInSts)
        return;
    *vtg = iterator->vtg = 0;

    *sts = iterator->sts = (uint8)(iterator->sts + 1);
    if (iterator->sts < cMaxNumSts)
        return;
    *sts = iterator->sts = 0;

    *slice = iterator->slice = (uint8)(iterator->slice + 1);
    }

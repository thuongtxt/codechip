/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : ThaSdhVc.c
 *
 * Created Date: Sep 7, 2012
 *
 * Description : Thalassa SDH VC default implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPwCep.h"
#include "../../../generic/common/AtChannelInternal.h"
#include "../../../generic/sdh/AtSdhChannelInternal.h"
#include "../../../generic/sdh/AtModuleSdhInternal.h" /* For channel factory */
#include "../../../generic/prbs/AtModulePrbsInternal.h"
#include "../../../util/coder/AtCoderUtil.h"
#include "../cdr/controllers/ThaCdrController.h"
#include "../pw/adapters/ThaPwAdapter.h"
#include "../pdh/ThaStmModulePdh.h"
#include "../pdh/ThaPdhDe3.h"
#include "../map/ThaModuleStmMap.h"
#include "../prbs/ThaPrbsEngine.h"
#include "ThaModuleSdh.h"
#include "ThaSdhVcInternal.h"
#include "ThaModuleSdhReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mPohProcessor(self) mMethodsGet((ThaSdhVc)self)->PohProcessorGet((ThaSdhVc)self)
#define mThis(self) ((ThaSdhVc)self)
#define mCopyField(fieldName) newVc->fieldName = thisVc->fieldName
#define mInAccessible(self) AtChannelInAccessible((AtChannel)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaSdhVcMethods m_methods;

/* Override */
static tAtObjectMethods            m_AtObjectOverride;
static tAtChannelMethods           m_AtChannelOverride;
static tAtSdhChannelMethods        m_AtSdhChannelOverride;
static tAtSdhPathMethods           m_AtSdhPathOverride;

/* Save super implementation */
static const tAtSdhChannelMethods *m_AtSdhChannelMethods = NULL;
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;
static const tAtObjectMethods     *m_AtObjectMethods     = NULL;
static const tAtSdhPathMethods    *m_AtSdhPathMethods    = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 PartOffset(ThaSdhVc self)
    {
    ThaDevice device = (ThaDevice)AtChannelDeviceGet((AtChannel)self);
    return ThaDeviceModulePartOffset(device, cThaModuleOcn, ThaModuleSdhPartOfChannel((AtSdhChannel)self));
    }

static uint32 OcnDefaultOffset(ThaSdhVc self)
    {
    uint8 stsId   = 0;
    uint8 sliceId = 0;

    ThaSdhChannel2HwMasterStsId((AtSdhChannel)self, cThaModuleOcn, &sliceId, &stsId);

    return (uint32)((sliceId * 8192UL) + (stsId * 32UL) + PartOffset(self));
    }

static void De3ReferenceRelease(AtSdhChannel channel)
    {
    AtPdhChannel de3 = (AtPdhChannel)SdhVcPdhChannelGet((AtSdhVc)channel);
    if (de3 == NULL)
        return;

    if (ThaPdhDe3IsManagedByModulePdh((ThaPdhDe3)de3))
        {
        SdhVcPdhChannelSet((AtSdhVc)channel, NULL);
        AtPdhChannelVcSet(de3, NULL);
        }
    }

void ThaSdhVcPdhChannelDelete(AtSdhVc self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;
    AtPdhChannel pdhChannel;
    AtModule pdhModule;

    if (AtSdhChannelTypeGet(channel) == cAtSdhChannelTypeVc3)
        De3ReferenceRelease(channel);

    pdhChannel = SdhVcPdhChannelGet(self);
    if (pdhChannel == NULL)
        return;

    pdhModule = AtChannelModuleGet((AtChannel)pdhChannel);
    AtModuleLock(pdhModule);
    AtObjectDelete((AtObject)pdhChannel);
    SdhVcPdhChannelSet(self, NULL);
    AtModuleUnLock(pdhModule);
    }

static AtModulePrbs PrbsModule(AtChannel self)
    {
    return (AtModulePrbs)AtDeviceModuleGet(AtChannelDeviceGet(self), cAtModulePrbs);
    }

static void PointerAccumulatorDelete(ThaSdhVc self)
    {
    ThaSdhPointerAccumulator *accumulatorAddress = mMethodsGet(self)->PointerAccumulatorAddress(self);

    if (accumulatorAddress == NULL)
        return;

    if (*accumulatorAddress)
        {
        AtObjectDelete((AtObject)*accumulatorAddress);
        *accumulatorAddress = NULL;
        }
    }

static void Delete(AtObject self)
    {
    ThaSdhVc object = (ThaSdhVc)self;

    AtModulePrbsEngineObjectDelete(PrbsModule((AtChannel)self), object->prbsEngine);
    object->prbsEngine = NULL;

    ThaSdhVcPdhChannelDelete((AtSdhVc)self);
    AtObjectDelete((AtObject)object->pohProcessor);
    object->pohProcessor = NULL;
    AtObjectDelete((AtObject)object->cdrController);
    object->cdrController = NULL;

    PointerAccumulatorDelete(object);

    /* Fully delete itself */
    m_AtObjectMethods->Delete(self);
    }

static ThaPohProcessor PohProcessorGet(ThaSdhVc self)
    {
    if (self->pohProcessor == NULL)
        self->pohProcessor = mMethodsGet(self)->PohProcessorCreate(self);

    return self->pohProcessor;
    }

static uint32 AlwaysDownstreamAlarms(void)
    {
    return cAtSdhPathAlarmAis | cAtSdhPathAlarmLop;
    }

static eAtModuleSdhRet AlarmAffectingEnable(AtSdhChannel self, uint32 alarmType, eBool enable)
    {
    ThaPohProcessor processor = mPohProcessor(self);
    if (processor)
        return AtSdhChannelAlarmAffectingEnable((AtSdhChannel)processor, alarmType, enable);

    if ((alarmType & AlwaysDownstreamAlarms()) && enable)
        return cAtOk;

    return enable ? cAtErrorModeNotSupport : cAtOk;
    }

static eAtModuleSdhRet VcAisAffectEnable(AtSdhChannel self, eBool enable)
    {
    ThaPohProcessor processor = mPohProcessor(self);
    if (processor == NULL)
        return cAtErrorModeNotSupport;
    return ThaPohProcessorVcAisAffectEnable(mPohProcessor(self), enable);
    }

static eBool AlarmAffectingIsEnabled(AtSdhChannel self, uint32 alarmType)
    {
    ThaPohProcessor processor = mPohProcessor(self);

    if (processor)
        return AtSdhChannelAlarmAffectingIsEnabled((AtSdhChannel)mPohProcessor(self), alarmType);

    /* So far, LOP/AIS always downstream */
    if (alarmType & AlwaysDownstreamAlarms())
        return cAtTrue;

    return cAtFalse;
    }

static uint32 AlarmAffectingMaskGet(AtSdhChannel self)
    {
    ThaPohProcessor processor = mPohProcessor(self);
    if (processor == NULL)
        return 0;
    return AtSdhChannelAlarmAffectingMaskGet((AtSdhChannel)mPohProcessor(self));
    }

static uint32 DefectFilter(AtChannel self, uint32 realAlarms)
    {
	AtUnused(self);

    /* Filter PLM when UNEQ */
    if (realAlarms & cAtSdhPathAlarmUneq)
        realAlarms = realAlarms & (uint32)(~cAtSdhPathAlarmPlm);
    
    /* AIS and LOP are of AU/TU, they are not here */
    realAlarms = realAlarms & (uint32)(~cAtSdhPathAlarmAis);
    realAlarms = realAlarms & (uint32)(~cAtSdhPathAlarmLop);

    return realAlarms;
    }

static uint32 DefectGet(AtChannel self)
    {
    uint32 realDefects = AtChannelDefectGet((AtChannel)mPohProcessor(self));

    if (!ThaModuleSdhPayloadUneqIsApplicable((ThaModuleSdh)AtChannelModuleGet(self)))
        realDefects = realDefects & (uint32)(~cAtSdhPathAlarmPayloadUneq);

    return DefectFilter(self, realDefects);
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    uint32 history = AtChannelDefectInterruptGet((AtChannel)mPohProcessor(self));
    ThaCdrController cdrController = ThaSdhVcCdrControllerGet((AtSdhVc)self);

    if (!ThaModuleSdhPayloadUneqIsApplicable((ThaModuleSdh)AtChannelModuleGet(self)))
        history = history & (uint32)(~cAtSdhPathAlarmPayloadUneq);

    if (cdrController)
        history |= ThaCdrControllerInterruptRead2Clear(cdrController, cAtFalse);

    return history;
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    uint32 history = AtChannelDefectInterruptClear((AtChannel)mPohProcessor(self));
    ThaCdrController cdrController = ThaSdhVcCdrControllerGet((AtSdhVc)self);

    if (!ThaModuleSdhPayloadUneqIsApplicable((ThaModuleSdh)AtChannelModuleGet(self)))
        history = history & (uint32)(~cAtSdhPathAlarmPayloadUneq);

    if (cdrController)
        history |= ThaCdrControllerInterruptRead2Clear(cdrController, cAtTrue);

    return history;
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
    uint32 mask = 0;
    ThaCdrController cdrController;
    ThaPohProcessor pohProcessor;

    cdrController = ThaSdhVcCdrControllerGet((AtSdhVc)self);
    if (cdrController && ThaCdrControllerInterruptMaskIsSupported(cdrController, cAtSdhPathAlarmClockStateChange))
        mask |= cAtSdhPathAlarmClockStateChange;

    pohProcessor = mPohProcessor(self);
    if (pohProcessor)
        mask |= AtChannelSupportedInterruptMasks((AtChannel)pohProcessor);

    return mask;
    }

static eAtRet ClockStateChangeInterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    ThaCdrController cdrController;

    if ((defectMask & cAtSdhPathAlarmClockStateChange) == 0)
        return cAtOk;

    cdrController = mMethodsGet(mThis(self))->RedirectCdrController(mThis(self));
    if (cdrController == NULL)
        return cAtErrorModeNotSupport;

    return ThaCdrControllerInterruptMaskSet(cdrController, defectMask, enableMask);
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    eAtRet ret;
    ThaPohProcessor pohProcessor;

    ret = ClockStateChangeInterruptMaskSet(self, defectMask, enableMask);
    if (ret != cAtOk)
        return ret;

    defectMask &= (uint32)(~cAtSdhPathAlarmClockStateChange);
    enableMask &= (uint32)(~cAtSdhPathAlarmClockStateChange);

    pohProcessor = mPohProcessor(self);
    if (pohProcessor)
        return AtChannelInterruptMaskSet((AtChannel)pohProcessor, defectMask, enableMask);

    return enableMask ? cAtErrorModeNotSupport : cAtOk;
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    ThaCdrController cdrController = mMethodsGet(mThis(self))->RedirectCdrController(mThis(self));
    AtChannel pohProcessor = (AtChannel)mPohProcessor(self);
    uint32 pohMask = pohProcessor ? AtChannelInterruptMaskGet(pohProcessor) : 0;
    return pohMask | (cdrController ? ThaCdrControllerInterruptMaskGet(cdrController) : 0);
    }

static eAtRet TxAlarmForce(AtChannel self, uint32 alarmType)
    {
    if (mPohProcessor(self))
        return AtChannelTxAlarmForce((AtChannel)mPohProcessor(self), alarmType);
    return cAtErrorModeNotSupport;
    }

static eAtRet TxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    if (mPohProcessor(self))
        return AtChannelTxAlarmUnForce((AtChannel)mPohProcessor(self), alarmType);
    return cAtErrorModeNotSupport;
    }

static uint32 TxForcedAlarmGet(AtChannel self)
    {
    if (mPohProcessor(self))
        return AtChannelTxForcedAlarmGet((AtChannel)mPohProcessor(self));
    return 0;
    }

static eAtModuleSdhRet TxPslSet(AtSdhPath self, uint8 psl)
    {
    if (mPohProcessor(self))
        return AtSdhPathTxPslSet((AtSdhPath)mPohProcessor(self), psl);
    return cAtErrorModeNotSupport;
    }

static uint8 TxPslGet(AtSdhPath self)
    {
    return AtSdhPathTxPslGet((AtSdhPath)mPohProcessor(self));
    }

static uint8 RxPslGet(AtSdhPath self)
    {
    return AtSdhPathRxPslGet((AtSdhPath)mPohProcessor(self));
    }

static uint8 ExpectedPslGet(AtSdhPath self)
    {
    return AtSdhPathExpectedPslGet((AtSdhPath)mPohProcessor(self));
    }

static ThaModuleSdh ModuleSdh(AtSdhPath self)
    {
    return (ThaModuleSdh)AtChannelModuleGet((AtChannel)self);
    }

static eAtRet PlmMonitorUpdate(AtSdhPath self, uint8 expectedPsl, eAtSdhChannelMode mode)
    {
    static const uint8 cEquippedNonSpecificPsl = 0x1;
    eBool enabled = cAtTrue;

    if (mode == cAtSdhChannelModeSdh)
        enabled = (expectedPsl == cEquippedNonSpecificPsl) ? cAtFalse : cAtTrue;

    return AtSdhPathPlmMonitorEnable((AtSdhPath)mPohProcessor(self), enabled);
    }

static eAtModuleSdhRet ExpectedPslSet(AtSdhPath self, uint8 psl)
    {
    eAtRet ret;

    if (mPohProcessor(self) == NULL)
        return cAtErrorModeNotSupport;

    ret = AtSdhPathExpectedPslSet((AtSdhPath)mPohProcessor(self), psl);
    if (ret != cAtOk)
        return ret;

    if (ThaModuleSdhShouldUpdatePlmMonitorWhenExpectedPslChange(ModuleSdh(self)))
        ret |= PlmMonitorUpdate(self, psl, AtSdhChannelModeGet((AtSdhChannel)self));

    return ret;
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    AtChannel pohProcessor = (AtChannel)mPohProcessor(self);
    if (pohProcessor)
        return mMethodsGet(pohProcessor)->CounterGet(pohProcessor, counterType);
    return 0x0;
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    AtChannel pohProcessor = (AtChannel)mPohProcessor(self);
    if (pohProcessor)
        return mMethodsGet(pohProcessor)->CounterClear(pohProcessor, counterType);
    return cAtOk;
    }

static uint32 BlockErrorCounterGet(AtSdhChannel self, uint16 counterType)
    {
	if (AtSdhPathHasPohProcessor((AtSdhPath)self))
		return AtSdhChannelBlockErrorCounterGet((AtSdhChannel)mPohProcessor(self), counterType);
	return 0;
    }

static uint32 BlockErrorCounterClear(AtSdhChannel self, uint16 counterType)
    {
	if (AtSdhPathHasPohProcessor((AtSdhPath)self))
		return AtSdhChannelBlockErrorCounterClear((AtSdhChannel)mPohProcessor(self), counterType);
	return 0;
    }

static uint32 ReadOnlyCntGet(AtChannel self, uint32 counterType)
    {
    AtChannel pohProcessor = (AtChannel)mPohProcessor(self);
    return mMethodsGet(pohProcessor)->ReadOnlyCntGet(pohProcessor, counterType);
    }

static eAtRet TxErrorForce(AtChannel self, uint32 errorType)
    {
    if (mPohProcessor(self))
        return AtChannelTxErrorForce((AtChannel)mPohProcessor(self), errorType);
    return cAtErrorModeNotSupport;
    }

static eAtRet TxErrorUnForce(AtChannel self, uint32 errorType)
    {
    if (mPohProcessor(self))
        return AtChannelTxErrorUnForce((AtChannel)mPohProcessor(self), errorType);
    return cAtErrorModeNotSupport;
    }

static uint32 TxForcedErrorGet(AtChannel self)
    {
    return AtChannelTxForcedErrorGet((AtChannel)mPohProcessor(self));
    }

static eAtModuleSdhRet TimMonitorEnable(AtSdhChannel self, eBool enable)
    {
    if (mPohProcessor(self))
        return AtSdhChannelTimMonitorEnable((AtSdhChannel)mPohProcessor(self), enable);
    return cAtErrorModeNotSupport;
    }

static eBool TimMonitorIsEnabled(AtSdhChannel self)
    {
    return AtSdhChannelTimMonitorIsEnabled((AtSdhChannel)mPohProcessor(self));
    }

static eAtModuleSdhRet TtiCompareEnable(AtSdhChannel self, eBool enable)
    {
    if (mPohProcessor(self))
        return AtSdhChannelTtiCompareEnable((AtSdhChannel)mPohProcessor(self), enable);
    return cAtErrorModeNotSupport;
    }

static eBool TtiCompareIsEnabled(AtSdhChannel self)
    {
    return AtSdhChannelTtiCompareIsEnabled((AtSdhChannel)mPohProcessor(self));
    }

static eAtModuleSdhRet ERdiEnable(AtSdhPath self, eBool enable)
    {
    ThaPohProcessor processor = mPohProcessor(self);
    eAtRet ret;

    if (processor == NULL)
        return cAtErrorModeNotSupport;

    ret = AtSdhPathERdiEnable((AtSdhPath)processor, enable);
    if (ret == cAtOk)
        mThis(self)->erdiEnabled = enable;

    ret |= ThaPohProcessorDefaultAutoRdiWhenERdiUpdate(processor);

    return ret;
    }

static eBool ERdiIsEnabled(AtSdhPath self)
    {
    return AtSdhPathERdiIsEnabled((AtSdhPath)mPohProcessor(self));
    }

static eAtModuleSdhRet TxTtiSet(AtSdhChannel self, const tAtSdhTti *tti)
    {
    eAtRet ret = cAtErrorModeNotSupport;

    if (mPohProcessor(self))
        ret = AtSdhChannelTxTtiSet((AtSdhChannel)mPohProcessor(self), tti);

    m_AtSdhChannelMethods->TxTtiSet(self, tti);

    return ret;
    }

static eAtModuleSdhRet TxTtiGet(AtSdhChannel self, tAtSdhTti *tti)
    {
    if (mInAccessible(self))
        return m_AtSdhChannelMethods->TxTtiGet(self, tti);

    if (mPohProcessor(self))
        return AtSdhChannelTxTtiGet((AtSdhChannel)mPohProcessor(self), tti);
    return cAtErrorModeNotSupport;
    }

static eAtModuleSdhRet RxTtiGet(AtSdhChannel self, tAtSdhTti *tti)
    {
    if(mPohProcessor(self))
        return AtSdhChannelRxTtiGet((AtSdhChannel)mPohProcessor(self), tti);
    return cAtErrorModeNotSupport;
    }

static eAtModuleSdhRet ExpectedTtiGet(AtSdhChannel self, tAtSdhTti *tti)
    {
    if (mInAccessible(self))
        return m_AtSdhChannelMethods->ExpectedTtiGet(self, tti);

    if (mPohProcessor(self))
        return AtSdhChannelExpectedTtiGet((AtSdhChannel)mPohProcessor(self), tti);
    return cAtErrorModeNotSupport;
    }

static eAtModuleSdhRet ExpectedTtiSet(AtSdhChannel self, const tAtSdhTti *tti)
    {
    eAtRet ret = cAtErrorModeNotSupport;

    if (mPohProcessor(self))
        ret = AtSdhChannelExpectedTtiSet((AtSdhChannel)mPohProcessor(self), tti);

    m_AtSdhChannelMethods->ExpectedTtiSet(self, tti);

    return ret;
    }

static AtChannel MapChannelGet(AtSdhChannel self)
    {
    return (AtChannel)SdhVcPdhChannelGet((AtSdhVc)self);
    }

static eAtRet Enable(AtChannel self, eBool enable)
    {
    ((ThaSdhVc)self)->enabled = (uint8)enable;

    return cAtOk;
    }

static eBool IsEnabled(AtChannel self)
    {
    return ((ThaSdhVc)self)->enabled;
    }

static eAtRet PohDefaultSet(ThaSdhVc self)
    {
    ThaPohProcessor pohProcessor = mPohProcessor(self);
    eAtRet ret = cAtOk;
    ThaModuleSdh sdhModule;

    if (pohProcessor == NULL)
        return cAtOk;

    ret |= AtChannelInit((AtChannel)pohProcessor);
    ret |= AtSdhChannelTimMonitorEnable((AtSdhChannel)self, cAtTrue);

    /* Set default equipped non-specific PSL */
    ret |= AtSdhPathTxPslSet((AtSdhPath)self, 0x1);
    ret |= AtSdhPathExpectedPslSet((AtSdhPath)self, 0x1);

    sdhModule = (ThaModuleSdh)AtChannelModuleGet((AtChannel)self);
    ret |= VcAisAffectEnable((AtSdhChannel)self, ThaModuleSdhVcAisIsEnabledByDefault(sdhModule));
    AtSdhPathERdiEnable((AtSdhPath)self, ThaModuleSdhErdiIsEnabledByDefault(sdhModule));

    return ret;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret;
    ThaModuleMap mapModule = (ThaModuleMap)AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModuleMap);

    ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    ret |= AtChannelTimingSet(self, cAtTimingModeSys, self);
    if (AtSdhPathHasPohProcessor((AtSdhPath)self))
        ret |= mMethodsGet(mThis(self))->PohDefaultSet(mThis(self));

    ret |= ThaModuleMapMapLineControlVcDefaultSet(mapModule, (AtSdhChannel)self);

    return ret;
    }

static ThaPohProcessor PohProcessorCreate(ThaSdhVc self)
    {
	AtUnused(self);
    return NULL;
    }

static ThaCdrController CdrControllerCreate(ThaSdhVc self)
    {
	AtUnused(self);
    return NULL;
    }

static AtObject Clone(AtObject self)
    {
    ThaSdhVc thisVc = (ThaSdhVc)self;
    ThaSdhVc newVc = (ThaSdhVc)m_AtObjectMethods->Clone(self);

    if (newVc == NULL)
        return NULL;

    /* Copy private data */
    mCopyField(enabled);
    mCopyField(prbsEngine);
    mCopyField(isInPwBindingProgress);

    /* Let its association be recreated */
    newVc->pohProcessor  = NULL;
    newVc->cdrController = NULL;
    newVc->surEngine     = NULL;

    return (AtObject)newVc;
    }

/* This function is binding to pseudowire  */
static eAtRet BindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    /* In case of unbinding, need to update timing configuration */
    if (pseudowire == NULL)
        {
        if ((AtChannelTimingModeGet(self) == cAtTimingModeAcr) ||
            (AtChannelTimingModeGet(self) == cAtTimingModeDcr))
            mChannelSuccessAssert(self, AtChannelTimingSet(self, cAtTimingModeSys, NULL));
        }

    return AtChannelBoundPwSet(self, ThaPwAdapterPwGet((ThaPwAdapter)pseudowire));
    }

static ThaCdrController CdrController(AtChannel self)
    {
    return ThaSdhVcCdrControllerGet((AtSdhVc)self);
    }

static eBool TimingModeIsSupported(AtChannel self, eAtTimingMode timingMode)
    {
    return ThaCdrControllerTimingModeIsSupported(CdrController(self), timingMode);
    }

static eAtRet TimingSet(AtChannel self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    return ThaCdrControllerTimingSet(CdrController(self), timingMode, timingSource);
    }

/* This function is to get timing mode for AUVC channel */
static eAtTimingMode TimingModeGet(AtChannel self)
    {
    return ThaCdrControllerTimingModeGet(CdrController(self));
    }

/* This function is to get timing source of a AUVC channel */
static AtChannel TimingSourceGet(AtChannel self)
    {
    return ThaCdrControllerTimingSourceGet(CdrController(self));
    }

static eAtClockState ClockStateGet(AtChannel self)
    {
    return ThaCdrControllerClockStateGet(CdrController(self));
    }

static uint32 PwTimingRestore(AtChannel self, AtPw pw)
    {
    ThaCdrControllerPwTimingSourceSet(CdrController(self), pw);
    return 0;
    }

static uint8 HwClockStateGet(AtChannel self)
    {
    return ThaCdrControllerHwClockStateGet(CdrController(self));
    }

static AtPrbsEngine PrbsEngineGet(AtChannel self)
    {
    if (mThis(self)->prbsEngine == NULL)
        mThis(self)->prbsEngine = ThaModuleSdhVcPrbsEngineCreate((ThaModuleSdh)AtChannelModuleGet(self), (AtSdhVc)self);
    return mThis(self)->prbsEngine;
    }

static eBool PrbsEngineIsCreated(AtChannel self)
    {
    if (mThis(self)->prbsEngine)
        return cAtTrue;

    return cAtFalse;
    }

static eAtModuleSdhRet TxOverheadByteSet(AtSdhChannel self, uint32 overheadByte, uint8 overheadByteValue)
    {
    if (mPohProcessor(self))
        return AtSdhChannelTxOverheadByteSet((AtSdhChannel)mPohProcessor(self), overheadByte, overheadByteValue);
    return cAtErrorModeNotSupport;
    }

static uint8 TxOverheadByteGet(AtSdhChannel self, uint32 overheadByte)
    {
    return AtSdhChannelTxOverheadByteGet((AtSdhChannel)mPohProcessor(self), overheadByte);
    }

static uint8 RxOverheadByteGet(AtSdhChannel self, uint32 overheadByte)
    {
    return AtSdhChannelRxOverheadByteGet((AtSdhChannel)mPohProcessor(self), overheadByte);
    }

static void PrbsEngineSet(AtChannel self, AtPrbsEngine engine)
    {
    mThis(self)->prbsEngine = engine;
    }

static eBool HasPohProcessor(AtSdhPath self)
    {
    return mPohProcessor(self) ? cAtTrue : cAtFalse;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaSdhVc object = (ThaSdhVc)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(enabled);
    mEncodeChannelIdString(pohProcessor);
    mEncodeObject(cdrController);
    mEncodeObject(prbsEngine);
    mEncodeObject(surEngine);
    mEncodeUInt(isInPwBindingProgress);
    mEncodeUInt(erdiEnabled);
    }

static eAtModuleSdhRet PlmMonitorEnable(AtSdhPath self, eBool enable)
    {
    if (mPohProcessor(self))
        return AtSdhPathPlmMonitorEnable((AtSdhPath)mPohProcessor(self), enable);
    return cAtErrorModeNotSupport;
    }

static eBool PlmMonitorIsEnabled(AtSdhPath self)
    {
    if (mPohProcessor(self))
        return AtSdhPathPlmMonitorIsEnabled((AtSdhPath)mPohProcessor(self));
    return cAtFalse;
    }

static AtSurEngine *SurEngineAddress(AtChannel self)
    {
    return &(mThis(self)->surEngine);
    }

static ThaModulePoh PohModule(AtSdhPath self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaModulePoh)AtDeviceModuleGet(device, cThaModulePoh);
    }

static eAtRet PohMonitorEnable(AtSdhPath self, eBool enabled)
    {
    if (ThaModulePohPathPohMonitorEnablingIsSupported(PohModule(self)))
        return AtSdhPathPohMonitorEnable((AtSdhPath)mPohProcessor(self), enabled);
    return m_AtSdhPathMethods->PohMonitorEnable(self, enabled);
    }

static eBool PohMonitorIsEnabled(AtSdhPath self)
    {
    if (ThaModulePohPathPohMonitorEnablingIsSupported(PohModule(self)))
        return AtSdhPathPohMonitorIsEnabled((AtSdhPath)mPohProcessor(self));
    return m_AtSdhPathMethods->PohMonitorIsEnabled(self);
    }

static eBool ERdiDatabaseIsEnabled(AtSdhPath self)
    {
    return (eBool)mThis(self)->erdiEnabled;
    }

static eAtRet PwCepSet(ThaSdhVc self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet MappingRestore(ThaSdhVc self)
    {
    AtUnused(self);
    return cAtOk;
    }

static ThaCdrController RedirectCdrController(ThaSdhVc self)
    {
    return ThaSdhVcCdrControllerObjectGet(self);
    }

static eAtRet TxSquelch(AtChannel self, eBool squelched)
    {
    AtChannel parent = (AtChannel)AtSdhChannelParentChannelGet((AtSdhChannel)self);
    return AtChannelTxSquelch(parent, squelched);
    }

static eAtRet DidBindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    return ThaPrbsEnginePwDidBind((ThaPrbsEngine)AtChannelPrbsEngineGet(self), pseudowire);
    }

static eAtRet WillBindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    return ThaPrbsEnginePwWillBind((ThaPrbsEngine)AtChannelPrbsEngineGet(self), pseudowire);
    }

static void OverrideAtObject(AtSdhVc self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Clone);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtSdhPath(AtSdhVc self)
    {
    AtSdhPath path = (AtSdhPath)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhPathMethods = mMethodsGet(path);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhPathOverride, mMethodsGet(path), sizeof(m_AtSdhPathOverride));
        mMethodOverride(m_AtSdhPathOverride, RxPslGet);
        mMethodOverride(m_AtSdhPathOverride, ExpectedPslGet);
        mMethodOverride(m_AtSdhPathOverride, ExpectedPslSet);
        mMethodOverride(m_AtSdhPathOverride, TxPslSet);
        mMethodOverride(m_AtSdhPathOverride, TxPslGet);
        mMethodOverride(m_AtSdhPathOverride, ERdiEnable);
        mMethodOverride(m_AtSdhPathOverride, ERdiIsEnabled);
        mMethodOverride(m_AtSdhPathOverride, ERdiDatabaseIsEnabled);
        mMethodOverride(m_AtSdhPathOverride, HasPohProcessor);
        mMethodOverride(m_AtSdhPathOverride, PlmMonitorEnable);
        mMethodOverride(m_AtSdhPathOverride, PlmMonitorIsEnabled);
        mMethodOverride(m_AtSdhPathOverride, PohMonitorEnable);
        mMethodOverride(m_AtSdhPathOverride, PohMonitorIsEnabled);
        }

    mMethodsSet(path, &m_AtSdhPathOverride);
    }

static eAtRet TxTrafficEnable(AtChannel self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    return cAtOk;
    }

static eAtRet RxTrafficEnable(AtChannel self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    return cAtOk;
    }

static eBool HasOcnEpar(ThaSdhVc self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet OcnEparEnable(ThaSdhVc self, eBool enable)
    {
    uint32 regAddr = cThaRegOcnTxPGPerChnCtrl + mMethodsGet(self)->OcnDefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);

    mRegFieldSet(regVal, cThaTxPgStsVtEparEn, enable ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eAtRet EparEnable(ThaSdhVc self, eBool enable)
    {
    ThaSdhChannelCdrController cdrController = (ThaSdhChannelCdrController)ThaSdhVcCdrControllerGet((AtSdhVc)self);
    if (mMethodsGet(self)->HasOcnEpar(self))
        OcnEparEnable(self, enable);
    return ThaSdhChannelCdrControllerEparEnable(cdrController, enable);
    }

static eBool OcnEparIsEnabled(ThaSdhVc self)
    {
    uint32 regAddr = cThaRegOcnTxPGPerChnCtrl + mMethodsGet(self)->OcnDefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    return (regVal & cThaTxPgStsVtEparEnMask) ? cAtTrue : cAtFalse;
    }

static eBool EparIsEnabled(ThaSdhVc self)
    {
    ThaSdhChannelCdrController cdrController;
    if (mMethodsGet(self)->HasOcnEpar(self))
        return OcnEparIsEnabled(self);

    cdrController = (ThaSdhChannelCdrController)ThaSdhVcCdrControllerGet((AtSdhVc)self);
    return ThaSdhChannelCdrControllerEparIsEnabled(cdrController);
    }

static eBool HasLoOrderMapping(ThaSdhVc self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet Debug(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Debug(self);
    AtSdhChannel sdhChannel = (AtSdhChannel)self;

    ThaModuleSdhStsMappingDisplay(sdhChannel);
    ret |= ThaSdhChannelCommonRegisterOffsetsDisplay(sdhChannel);
    if (mMethodsGet(mThis(self))->HasLoOrderMapping(mThis(self)))
        ThaModulePdhSdhChannelRegsShow((AtSdhChannel)self);

    return ret;
    }

static uint8 MlpppBlockRange(AtChannel self)
    {
    static const uint8 cInvalidRange = 0xFF;

    if (AtSdhChannelTypeGet((AtSdhChannel)self) == cAtSdhChannelTypeVc4)
        return 7;

    if (AtSdhChannelTypeGet((AtSdhChannel)self) == cAtSdhChannelTypeVc3)
        return 4;

    if ((AtSdhChannelTypeGet((AtSdhChannel)self) == cAtSdhChannelTypeVc12) ||
        (AtSdhChannelTypeGet((AtSdhChannel)self) == cAtSdhChannelTypeVc11))
        return 6;

    return cInvalidRange;
    }

static eAtRet AutoRdiEnable(AtSdhChannel self, eBool enable)
    {
    ThaPohProcessor processor = ThaSdhVcPohProcessorGet((AtSdhVc)self);
    uint32 autoRdiAlarms = ThaPohProcessorDefaultAlarmsNeedAutoRdi(processor, enable);
    uint32 currentAutoRdiAlarms = ThaPohProcessorAutoRdiAlarms(processor);

    /* Nothing changed */
    if (currentAutoRdiAlarms == (enable ? autoRdiAlarms : 0))
        return cAtOk;

    return ThaPohProcessorAutoRdiEnable(mPohProcessor(self), autoRdiAlarms, enable);
    }

static eBool AutoRdiIsEnabled(AtSdhChannel self)
    {
    return (ThaPohProcessorAutoRdiAlarms(mPohProcessor(self)) != 0) ? cAtTrue : cAtFalse;
    }

static uint32 XcBaseAddressGet(ThaSdhVc self)
    {
    AtUnused(self);
    return cBit31_0;
    }

static eAtRet WarmRestore(AtChannel self)
    {
    /* Let super restore first */
    eAtRet ret = m_AtChannelMethods->WarmRestore(self);
    if (ret != cAtOk)
        return ret;

    if (mPohProcessor(self))
        {
        ThaSdhChannelRestoreAllHwSts((AtSdhChannel)self);
        return AtChannelWarmRestore((AtChannel)mPohProcessor(self));
        }

    return ret;
    }

static eAtModuleSdhRet ModeSet(AtSdhChannel self, eAtSdhChannelMode mode)
    {
    AtSdhChannel pohProcessor = (AtSdhChannel)ThaSdhVcPohProcessorGet((AtSdhVc)self);

    if (pohProcessor)
        {
        eAtRet ret = AtSdhChannelModeSet(pohProcessor, mode);

        if (ThaModuleSdhShouldUpdatePlmMonitorWhenExpectedPslChange(ModuleSdh((AtSdhPath)self)))
            ret |= PlmMonitorUpdate((AtSdhPath)self, AtSdhPathExpectedPslGet((AtSdhPath)pohProcessor), mode);

        ret |= ThaPohProcessorDefaultAutoRdiWhenERdiUpdate((ThaPohProcessor)pohProcessor);
        if (ret != cAtOk)
            return ret;
        }

    return m_AtSdhChannelMethods->ModeSet(self, mode);
    }

static eAtSdhChannelMode ModeGet(AtSdhChannel self)
    {
    AtSdhChannel pohProcessor = (AtSdhChannel)ThaSdhVcPohProcessorGet((AtSdhVc)self);

    if (pohProcessor)
        return AtSdhChannelModeGet(pohProcessor);

    return m_AtSdhChannelMethods->ModeGet(self);
    }

static eBool HasUpperVc(AtSdhChannel self)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(self);
    if ((channelType == cAtSdhChannelTypeVc11) ||
        (channelType == cAtSdhChannelTypeVc12))
        return cAtTrue;

    if (channelType == cAtSdhChannelTypeVc3)
        {
        eAtSdhChannelType parentType = AtSdhChannelTypeGet(AtSdhChannelParentChannelGet(self));
        if (parentType == cAtSdhChannelTypeTu3)
            return cAtTrue;
        }

    return cAtFalse;
    }

static AtSdhChannel UpperVcGet(AtSdhChannel self)
    {
    AtSdhChannel parent;
    eAtSdhChannelType parentType;

    /* Only vc11,vc12, tu3-vc3 has upper VC */
    if (!HasUpperVc(self))
        return NULL;

    parent = AtSdhChannelParentChannelGet(self);
    parentType = AtSdhChannelTypeGet(parent);

    /* Go upper to get AU4 or AU3 */
    while ((parentType != cAtSdhChannelTypeAu3) &&
           (parentType != cAtSdhChannelTypeAu4))
        {
        parent = AtSdhChannelParentChannelGet(parent);
        parentType = AtSdhChannelTypeGet(parent);
        }

    return AtSdhChannelSubChannelGet(parent, 0);
    }

static eBool UpperVcIsBoundToPw(AtSdhChannel self)
    {
    AtSdhChannel upperVc = UpperVcGet(self);
    if (upperVc == NULL)
        return cAtFalse;

    return (AtChannelBoundPwGet((AtChannel)upperVc) != NULL) ? cAtTrue : cAtFalse;
    }

static eBool SubVcsAreBoundToPw(AtSdhChannel self)
    {
    uint8 numSubChannels, i;

    numSubChannels = AtSdhChannelNumberOfSubChannelsGet(self);
    for (i = 0; i < numSubChannels; i++)
        {
        AtSdhChannel subChannel = AtSdhChannelSubChannelGet(self, i);
        if (AtChannelBoundPwGet((AtChannel)subChannel))
            return cAtTrue;
        if (SubVcsAreBoundToPw(subChannel))
            return cAtTrue;
        }

    return cAtFalse;
    }

static eAtRet Vc4_ncMapC4_nc(AtSdhChannel channel)
    {
    eAtSdhChannelType vcType = AtSdhChannelTypeGet(channel);
    eAtRet ret = cAtOk;

    if ((vcType == cAtSdhChannelTypeVc4_16c) || (vcType == cAtSdhChannelTypeVc4_4c))
        {
        ThaSdhVcInPwBindingProgressSet((ThaSdhVc)channel, cAtTrue);
        ret = AtSdhChannelMapTypeSet(channel, (vcType == cAtSdhChannelTypeVc4_16c) ? cAtSdhVcMapTypeVc4_16cMapC4_16c : cAtSdhVcMapTypeVc4_4cMapC4_4c);
        ThaSdhVcInPwBindingProgressSet((ThaSdhVc)channel, cAtFalse);
        }

    return ret;
    }

static eBool VcMapTypeIsValidForCepBasic(AtSdhChannel channel)
    {
    eAtSdhChannelType vcType = AtSdhChannelTypeGet(channel);

    if (vcType == cAtSdhChannelTypeVc4_64c)
        return (AtSdhChannelMapTypeGet(channel) == cAtSdhVcMapTypeVc4_64cMapC4_64c);
    if (vcType == cAtSdhChannelTypeVc4_16c)
        return (AtSdhChannelMapTypeGet(channel) == cAtSdhVcMapTypeVc4_16cMapC4_16c);
    if (vcType == cAtSdhChannelTypeVc4_4c)
        return (AtSdhChannelMapTypeGet(channel) == cAtSdhVcMapTypeVc4_4cMapC4_4c);
    if (vcType == cAtSdhChannelTypeVc4_nc)
        return (AtSdhChannelMapTypeGet(channel) == cAtSdhVcMapTypeVc4_ncMapC4_nc);
    if (vcType == cAtSdhChannelTypeVc4)
        return (AtSdhChannelMapTypeGet(channel) == cAtSdhVcMapTypeVc4MapC4);
    if (vcType == cAtSdhChannelTypeVc3)
        {
        if (AtSdhChannelMapTypeGet(channel) == cAtSdhVcMapTypeVc3MapC3)
            return cAtTrue;

        if (AtSdhChannelMapTypeGet(channel) == cAtSdhVcMapTypeVc3MapDe3)
            {
            /* TODO: it is not good when parent class calling subclass method */
            if (ThaSdhAuVcMapDe3LiuIsEnabled((ThaSdhAuVc)channel))
                return cAtTrue;

            return cAtFalse;
            }

        return cAtFalse;
        }

    if ((vcType == cAtSdhChannelTypeVc11) || (vcType == cAtSdhChannelTypeVc12))
        return (AtSdhChannelMapTypeGet(channel) == cAtSdhVcMapTypeVc1xMapC1x);

    return cAtFalse;
    }

static eAtRet CanBindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    AtSdhChannel channel = (AtSdhChannel)self;

    if (pseudowire == NULL)
        return cAtOk;

    if (AtPwTypeGet(pseudowire) != cAtPwTypeCEP)
        return cAtErrorInvlParm;

    if (UpperVcIsBoundToPw(channel) || SubVcsAreBoundToPw(channel))
        return cAtErrorChannelBusy;

    /* If pw is not CEP fractional, need to map this VC to contain a C before binding */
    if (AtPwCepModeGet((AtPwCep)pseudowire) != cAtPwCepModeFractional)
        {
        /* Auto map vc4_4c to c4_4c and vc4_16c to c4_16c, other cases return fail if application does not explicitly map to C */
        eAtRet ret = Vc4_ncMapC4_nc(channel);
        if (ret != cAtOk)
           return ret;

        if (!VcMapTypeIsValidForCepBasic(channel))
            {
            AtChannelLog(self, cAtLogLevelWarning, AtSourceLocation, "Needs to have Cx mapping to be bound to PW\r\n");
            return cAtErrorModeNotSupport;
            }
        }

    return cAtOk;
    }

static ThaSdhPointerAccumulator *PointerAccumulatorAddress(ThaSdhVc self)
    {
    AtUnused(self);
    return NULL;
    }

static uint32 PointerCounterUpdateThenClear(ThaSdhVc self, uint16 counterType, uint32 hwCounter, eBool read2Clear)
    {
    ThaSdhPointerAccumulator *accumulatorAddress = mMethodsGet(self)->PointerAccumulatorAddress(self);
    ThaSdhPointerAccumulator accumulator;

    if (accumulatorAddress == NULL)
        return hwCounter;

    /* The following logic is to save memory since application may not want to
     * read-only but read-to-clear. */

    /* If accumulator was created, just use it */
    accumulator = *accumulatorAddress;
    if (accumulator)
        return ThaSdhPointerAccumulatorCounterUpdateThenClear(accumulator, counterType, hwCounter, read2Clear);

    /* If read-to-clear, just return value read from hardware */
    if (read2Clear)
        return hwCounter;

    /* And for read-only, always need accumulator */
    *accumulatorAddress = ThaSdhPointerAccumulatorNew();
    return ThaSdhPointerAccumulatorCounterUpdateThenClear(*accumulatorAddress, counterType, hwCounter, read2Clear);
    }

static ThaCdrController CdrControllerGet(ThaSdhVc self)
    {
    if (self->cdrController == NULL)
        self->cdrController = mMethodsGet(self)->CdrControllerCreate(self);

    return self->cdrController;
    }

static ThaCdrController CdrControllerObjectGet(ThaSdhVc self)
    {
    return self->cdrController;
    }

static eAtModuleSdhRet AlarmAutoRdiEnable(AtSdhChannel self, uint32 alarms, eBool enable)
    {
    return ThaPohProcessorAutoRdiEnable(mPohProcessor(self), alarms, enable);
    }

static eBool AlarmAutoRdiIsEnabled(AtSdhChannel self, uint32 alarms)
    {
    uint32 autoRdiAlarms = ThaPohProcessorAutoRdiAlarms(mPohProcessor(self));
    return (alarms & autoRdiAlarms) ? cAtTrue : cAtFalse;
    }

static eBool HasOcnStuffing(ThaSdhVc self)
    {
    /* VC has hardware stuffing control */
    AtUnused(self);
    return cAtFalse;
    }

static uint32 NumSourceVcs(ThaSdhVc self)
    {
    AtUnused(self);
    return 0;
    }

static void SourceVcSet(ThaSdhVc self, uint32 vcIndex, ThaSdhVc sourceVc)
    {
    AtUnused(self);
    AtUnused(vcIndex);
    AtUnused(sourceVc);
    }

static ThaSdhVc SourceVcGet(ThaSdhVc self, uint32 vcIndex)
    {
    AtUnused(self);
    AtUnused(vcIndex);
    return NULL;
    }

static AtList DestVcs(ThaSdhVc self)
    {
    AtUnused(self);
    return NULL;
    }

static eAtRet DestVcAdd(ThaSdhVc self, ThaSdhVc destVc)
    {
    if (AtListContainsObject(ThaSdhVcDestVcs(self), (AtObject)destVc))
        return cAtOk;

    return AtListObjectAdd(ThaSdhVcDestVcs(self), (AtObject)destVc);
    }

static eAtRet DestVcRemove(ThaSdhVc self, ThaSdhVc destVc)
    {
    if (!AtListContainsObject(ThaSdhVcDestVcs(self), (AtObject)destVc))
        return cAtOk;

    return AtListObjectRemove(ThaSdhVcDestVcs(self), (AtObject)destVc);
    }

static eAtRet AuVcLoopbackSet(AtSdhChannel self, uint8 loopbackMode)
    {
    eAtRet ret = cAtOk;
    uint8 numSts = AtSdhChannelNumSts(self);
    uint8 stsId = AtSdhChannelSts1Get(self);
    uint8 sts1;

    for (sts1 = 0; sts1 < numSts; sts1++, stsId++)
        ret |= ThaOcnStsLoopbackSet(self, stsId, loopbackMode);

    return ret;
    }

static eAtRet HwLoopbackSet(AtChannel self, uint8 loopbackMode)
    {
    if (mMethodsGet(self)->OnlySupportXcLoopback(self))
        return mMethodsGet(self)->XcLoopbackSet(self, loopbackMode);

    if (AtSdhChannelIsHoVc((AtSdhChannel)self))
        return AuVcLoopbackSet((AtSdhChannel)self, loopbackMode);

    return ThaOcnVtTuLoopbackSet((AtSdhChannel)self, loopbackMode);
    }

static eAtRet LoopbackSet(AtChannel self, uint8 loopbackMode)
    {
    eAtRet ret = cAtOk;
    AtSdhChannel sdhChannel = (AtSdhChannel)self;

    if (loopbackMode == AtChannelLoopbackGet(self))
        return cAtOk;

    ret = HwLoopbackSet(self, loopbackMode);
    if (ret != cAtOk)
        return ret;

    if (AtSdhPathHasPohProcessor((AtSdhPath)self) == cAtFalse)
        return ret;

    /* Deal with POH insertion */
    if (loopbackMode == cAtLoopbackModeRemote)
        {
        /* Disable POH alarm forwarding */
        ret  = AtSdhChannelAlarmAffectingDisable(sdhChannel);

        /* Disable POH insertion. */
        ret |= AtSdhPathPohInsertionEnable((AtSdhPath)self, cAtFalse);

        return ret;
        }

    if (loopbackMode == cAtLoopbackModeRelease)
        {
        if (!mMethodsGet((ThaSdhVc)self)->NeedPohRestore((ThaSdhVc)self))
            return ret;

        /* Restore POH alarm forwarding */
        ret  = AtSdhChannelAlarmAffectingRestore(sdhChannel);

        /* Restore POH insertion. */
        ret |= AtSdhPathPohInsertionEnable((AtSdhPath)self, cAtTrue);

        return ret;
        }

    return ret;
    }

static uint8 LoopbackGet(AtChannel self)
    {
    if (mMethodsGet(self)->OnlySupportXcLoopback(self))
        return mMethodsGet(self)->XcLoopbackGet(self);

    if (AtSdhChannelIsHoVc((AtSdhChannel)self))
        return ThaOcnStsLoopbackGet((AtSdhChannel)self, AtSdhChannelSts1Get((AtSdhChannel)self));

    return ThaOcnVtTuLoopbackGet((AtSdhChannel)self);
    }

static eBool NeedPohRestore(ThaSdhVc self)
    {
    AtPrbsEngine engine = ThaSdhVcCachedPrbsEngine(self);

    /* If this channel has bound CEP and the PRBS engine is not running,
     * do not restore POH configuration. */
    if (AtChannelBoundPwGet((AtChannel)self) && ((engine == NULL) || (AtPrbsEngineIsEnabled(engine) == cAtFalse)))
        return cAtFalse;

    return cAtTrue;
    }

static eAtModuleSdhRet CounterModeSet(AtSdhChannel self, uint16 counterType, eAtSdhChannelCounterMode mode)
    {
    AtSdhChannel pohProcessor = (AtSdhChannel)ThaSdhVcPohProcessorGet((AtSdhVc)self);
    if (pohProcessor)
        return AtSdhChannelCounterModeSet(pohProcessor, counterType, mode);

    return cAtOk;
    }

static eAtRet AutoRdiReiEnable(ThaSdhVc self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtOk;
    }

static eAtRet HoldOffTimerSet(AtSdhChannel self, uint32 timerInMs)
    {
    if (mPohProcessor(self))
        return AtSdhChannelHoldOffTimerSet((AtSdhChannel)mPohProcessor(self), timerInMs);
    return cAtErrorModeNotSupport;
    }

static uint32 HoldOffTimerGet(AtSdhChannel self)
    {
    if (mPohProcessor(self))
        return AtSdhChannelHoldOffTimerGet((AtSdhChannel)mPohProcessor(self));
    return cAtFalse;
    }

static eAtRet AutoReiEnable(AtSdhChannel self, eBool enable)
    {
    if (mPohProcessor(self))
        return AtSdhChannelAutoReiEnable((AtSdhChannel)mPohProcessor(self), enable);
    return cAtErrorModeNotSupport;
    }

static eBool AutoReiIsEnabled(AtSdhChannel self)
    {
    if (mPohProcessor(self))
        return AtSdhChannelAutoReiIsEnabled((AtSdhChannel)mPohProcessor(self));
    return cAtFalse;
    }

static uint32 OcnPJCounterOffset(ThaSdhVc self, eBool isPjPos)
    {
    AtUnused(self);
    AtUnused(isPjPos);
    return cInvalidUint32;
    }

static void OverrideAtChannel(AtSdhVc self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, IsEnabled);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, ReadOnlyCntGet);
        mMethodOverride(m_AtChannelOverride, TxAlarmForce);
        mMethodOverride(m_AtChannelOverride, TxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcedAlarmGet);
        mMethodOverride(m_AtChannelOverride, TxErrorForce);
        mMethodOverride(m_AtChannelOverride, TxErrorUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcedErrorGet);
        mMethodOverride(m_AtChannelOverride, BindToPseudowire);
        mMethodOverride(m_AtChannelOverride, TimingModeIsSupported);
        mMethodOverride(m_AtChannelOverride, TimingSet);
        mMethodOverride(m_AtChannelOverride, TimingModeGet);
        mMethodOverride(m_AtChannelOverride, TimingSourceGet);
        mMethodOverride(m_AtChannelOverride, ClockStateGet);
        mMethodOverride(m_AtChannelOverride, HwClockStateGet);
        mMethodOverride(m_AtChannelOverride, TxTrafficEnable);
        mMethodOverride(m_AtChannelOverride, RxTrafficEnable);
        mMethodOverride(m_AtChannelOverride, PrbsEngineGet);
        mMethodOverride(m_AtChannelOverride, Debug);
        mMethodOverride(m_AtChannelOverride, MlpppBlockRange);
        mMethodOverride(m_AtChannelOverride, WarmRestore);
        mMethodOverride(m_AtChannelOverride, PrbsEngineSet);
        mMethodOverride(m_AtChannelOverride, SurEngineAddress);
        mMethodOverride(m_AtChannelOverride, CanBindToPseudowire);
        mMethodOverride(m_AtChannelOverride, PwTimingRestore);
        mMethodOverride(m_AtChannelOverride, PrbsEngineIsCreated);
        mMethodOverride(m_AtChannelOverride, TxSquelch);
        mMethodOverride(m_AtChannelOverride, PrbsEngineIsCreated);
        mMethodOverride(m_AtChannelOverride, DidBindToPseudowire);
        mMethodOverride(m_AtChannelOverride, WillBindToPseudowire);
        mMethodOverride(m_AtChannelOverride, LoopbackSet);
        mMethodOverride(m_AtChannelOverride, LoopbackGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtSdhChannel(AtSdhVc self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(m_AtSdhChannelOverride));

        mMethodOverride(m_AtSdhChannelOverride, MapChannelGet);
        mMethodOverride(m_AtSdhChannelOverride, TxTtiGet);
        mMethodOverride(m_AtSdhChannelOverride, TxTtiSet);
        mMethodOverride(m_AtSdhChannelOverride, ExpectedTtiGet);
        mMethodOverride(m_AtSdhChannelOverride, ExpectedTtiSet);
        mMethodOverride(m_AtSdhChannelOverride, RxTtiGet);
        mMethodOverride(m_AtSdhChannelOverride, AlarmAffectingEnable);
        mMethodOverride(m_AtSdhChannelOverride, AlarmAffectingIsEnabled);
        mMethodOverride(m_AtSdhChannelOverride, AlarmAffectingMaskGet);
        mMethodOverride(m_AtSdhChannelOverride, AutoRdiEnable);
        mMethodOverride(m_AtSdhChannelOverride, AutoRdiIsEnabled);
        mMethodOverride(m_AtSdhChannelOverride, AutoReiEnable);
        mMethodOverride(m_AtSdhChannelOverride, AutoReiIsEnabled);
        mMethodOverride(m_AtSdhChannelOverride, TimMonitorEnable);
        mMethodOverride(m_AtSdhChannelOverride, TimMonitorIsEnabled);
        mMethodOverride(m_AtSdhChannelOverride, BlockErrorCounterGet);
        mMethodOverride(m_AtSdhChannelOverride, BlockErrorCounterClear);
        mMethodOverride(m_AtSdhChannelOverride, TxOverheadByteSet);
        mMethodOverride(m_AtSdhChannelOverride, TxOverheadByteGet);
        mMethodOverride(m_AtSdhChannelOverride, RxOverheadByteGet);
        mMethodOverride(m_AtSdhChannelOverride, ModeSet);
        mMethodOverride(m_AtSdhChannelOverride, ModeGet);
        mMethodOverride(m_AtSdhChannelOverride, TtiCompareEnable);
        mMethodOverride(m_AtSdhChannelOverride, TtiCompareIsEnabled);
        mMethodOverride(m_AtSdhChannelOverride, AlarmAutoRdiEnable);
        mMethodOverride(m_AtSdhChannelOverride, AlarmAutoRdiIsEnabled);
        mMethodOverride(m_AtSdhChannelOverride, CounterModeSet);
        mMethodOverride(m_AtSdhChannelOverride, HoldOffTimerSet);
        mMethodOverride(m_AtSdhChannelOverride, HoldOffTimerGet);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void Override(AtSdhVc self)
    {
    OverrideAtSdhChannel(self);
    OverrideAtChannel(self);
    OverrideAtObject(self);
    OverrideAtSdhPath(self);
    }

static void MethodsInit(AtSdhVc self)
    {
    ThaSdhVc thaVc = (ThaSdhVc)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, PohProcessorCreate);
        mMethodOverride(m_methods, CdrControllerCreate);
        mMethodOverride(m_methods, EparEnable);
        mMethodOverride(m_methods, EparIsEnabled);
        mMethodOverride(m_methods, OcnDefaultOffset);
        mMethodOverride(m_methods, XcBaseAddressGet);
        mMethodOverride(m_methods, HasOcnEpar);
        mMethodOverride(m_methods, PohProcessorGet);
        mMethodOverride(m_methods, PohDefaultSet);
        mMethodOverride(m_methods, PointerAccumulatorAddress);
        mMethodOverride(m_methods, HasLoOrderMapping);
        mMethodOverride(m_methods, CdrControllerGet);
        mMethodOverride(m_methods, PwCepSet);
        mMethodOverride(m_methods, MappingRestore);
        mMethodOverride(m_methods, RedirectCdrController);
        mMethodOverride(m_methods, HasOcnStuffing);
        mMethodOverride(m_methods, NumSourceVcs);
        mMethodOverride(m_methods, SourceVcSet);
        mMethodOverride(m_methods, SourceVcGet);
        mMethodOverride(m_methods, DestVcs);
        mMethodOverride(m_methods, NeedPohRestore);
        mMethodOverride(m_methods, AutoRdiReiEnable);
        mMethodOverride(m_methods, OcnPJCounterOffset);
        }

    mMethodsSet(thaVc, &m_methods);
    }

AtSdhVc ThaSdhVcObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaSdhVc));

    /* Super constructor */
    if (AtSdhVcObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

ThaCdrController ThaSdhVcCdrControllerGet(AtSdhVc self)
    {
    if (self)
        return mMethodsGet(mThis(self))->CdrControllerGet(mThis(self));
    return NULL;
    }

eAtRet ThaSdhVcEparEnable(ThaSdhVc self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->EparEnable(self, enable);
    return cAtError;
    }

eBool ThaSdhVcEparIsEnabled(ThaSdhVc self)
    {
    if (self)
        return mMethodsGet(self)->EparIsEnabled(self);
    return cAtFalse;
    }

AtPrbsEngine ThaSdhVcCachedPrbsEngine(ThaSdhVc self)
    {
    if (self)
        return self->prbsEngine;
    return NULL;
    }

uint32 ThaSdhVcXcBaseAddressGet(ThaSdhVc self)
    {
    if (self)
        return mMethodsGet(self)->XcBaseAddressGet(self);
    return 0xFFFFFFFF;
    }

ThaPohProcessor ThaSdhVcPohProcessorGet(AtSdhVc self)
    {
    if (self)
        return mPohProcessor(self);
    return NULL;
    }

void ThaSdhVcInPwBindingProgressSet(ThaSdhVc self, eBool isInPwBindingProgress)
    {
    if (self)
        self->isInPwBindingProgress = isInPwBindingProgress;
    }

eBool ThaSdhVcIsInPwBindingProgress(ThaSdhVc self)
    {
    if (self)
        return self->isInPwBindingProgress;

    return cAtFalse;
    }

uint32 ThaSdhVcPointerCounterUpdateThenClear(ThaSdhVc self, uint16 counterType, uint32 hwCounter, eBool read2Clear)
    {
    if (self)
        return PointerCounterUpdateThenClear(self, counterType, hwCounter, read2Clear);
    return hwCounter;
    }

ThaCdrController ThaSdhVcCdrControllerObjectGet(ThaSdhVc self)
    {
    if (self)
        return CdrControllerObjectGet(self);
    return NULL;
    }

uint32 ThaSdhVcNumSourceVcs(ThaSdhVc self)
    {
    if (self)
        return mMethodsGet(self)->NumSourceVcs(self);
    return 0;
    }

void ThaSdhVcSourceVcSet(ThaSdhVc self, uint32 vcIndex, ThaSdhVc sourceVc)
    {
    if (self == NULL)
        return;

    if (vcIndex >= ThaSdhVcNumSourceVcs(self))
        return;

    mMethodsGet(self)->SourceVcSet(self, vcIndex, sourceVc);
    }

ThaSdhVc ThaSdhVcSourceVcGet(ThaSdhVc self, uint32 vcIndex)
    {
    if (self == NULL)
        return NULL;

    if (vcIndex >= ThaSdhVcNumSourceVcs(self))
        return NULL;

    return mMethodsGet(self)->SourceVcGet(self, vcIndex);
    }

AtList ThaSdhVcDestVcs(ThaSdhVc self)
    {
    if (self)
        return mMethodsGet(self)->DestVcs(self);
    return NULL;
    }

eAtRet ThaSdhVcDestVcAdd(ThaSdhVc self, ThaSdhVc destVc)
    {
    if (self)
        return DestVcAdd(self, destVc);
    return cAtErrorNullPointer;
    }

eAtRet ThaSdhVcDestVcRemove(ThaSdhVc self, ThaSdhVc destVc)
    {
    if (self)
        return DestVcRemove(self, destVc);
    return cAtErrorNullPointer;
    }

eAtRet ThaSdhVcAutoRdiReiEnable(ThaSdhVc self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->AutoRdiReiEnable(self, enable);
    return cAtErrorNullPointer;
    }


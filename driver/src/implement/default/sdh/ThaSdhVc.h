/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : ThaSdhVc.h
 * 
 * Created Date: Dec 14, 2013
 *
 * Description : SDH VC
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASDHVC_H_
#define _THASDHVC_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSdhVc.h"
#include "../poh/ThaPohProcessor.h"
#include "../cdr/controllers/ThaCdrController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaSdhVc * ThaSdhVc;

/* AU VC class */
typedef struct tThaSdhAuVc * ThaSdhAuVc;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Default concretes */
AtSdhVc ThaSdhAuVcNew(uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhVc ThaSdhTu3VcNew(uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhVc ThaSdhVc1xNew(uint32 channelId, uint8 channelType, AtModuleSdh module);

/* Controllers */
eAtRet ThaSdhVcPohProcessorSet(AtSdhVc self, ThaPohProcessor processor);
ThaPohProcessor ThaSdhVcPohProcessorGet(AtSdhVc self);
ThaCdrController ThaSdhVcCdrControllerGet(AtSdhVc self);

/* EPAR */
eAtRet ThaSdhVcEparEnable(ThaSdhVc self, eBool enable);
eBool ThaSdhVcEparIsEnabled(ThaSdhVc self);

/* XC */
uint32 ThaSdhVcXcBaseAddressGet(ThaSdhVc self);
AtSurEngine ThaSdhVcSurEngine(ThaSdhVc self);

AtPrbsEngine ThaSdhVcCachedPrbsEngine(ThaSdhVc self);
void ThaSdhVcPdhChannelDelete(AtSdhVc self);

/* DS3/E3 LIU over AU3/VC3 */
eAtRet ThaSdhAuVcMapDe3LiuEnable(ThaSdhAuVc self, eBool enable);
eBool ThaSdhAuVcMapDe3LiuIsEnabled(ThaSdhAuVc self);

uint32 ThaSdhVcOcnVc3PldType(AtSdhChannel self, eAtSdhVcMapType vc3MapType);
uint8 ThaSdhVc1xPdhVcMapMode(AtSdhChannel self, uint8 mapType);
uint8 ThaSdhVc1xMapTypeOfMapModule(AtSdhChannel self, uint8 mapType);
eAtRet ThaSdhVc1xPdhPwBindingCtrlSet(AtSdhChannel self);

/* VC4/VC3 H4[b7:b8] Multiframe Monitoring */
eAtRet ThaSdhAuVcLomMonitorEnable(ThaSdhAuVc self, eBool enable);
eBool ThaSdhAuVcLomMonitorIsEnabled(ThaSdhAuVc self);

/* AutoRDI/REI associated with UPSR operation. */
eAtRet ThaSdhVcAutoRdiReiEnable(ThaSdhVc self, eBool enable);

/* XC function */
uint32 ThaSdhVcNumSourceVcs(ThaSdhVc self);
void ThaSdhVcSourceVcSet(ThaSdhVc self, uint32 vcIndex, ThaSdhVc sourceVc);
ThaSdhVc ThaSdhVcSourceVcGet(ThaSdhVc self, uint32 vcIndex);
AtList ThaSdhVcDestVcs(ThaSdhVc self);
eAtRet ThaSdhVcDestVcAdd(ThaSdhVc self, ThaSdhVc destVc);
eAtRet ThaSdhVcDestVcRemove(ThaSdhVc self, ThaSdhVc destVc);

/* Product concretes */
AtSdhVc Tha60030080SdhAuVcNew(uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhVc Tha60210011SdhLoVcNew(uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhVc Tha60210011HoLineVcNew(uint32 channelId, uint8 channelType, AtModuleSdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THASDHVC_H_ */


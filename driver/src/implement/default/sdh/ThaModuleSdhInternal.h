/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : ThaModuleSdhInternal.h
 * 
 * Created Date: Mar 27, 2013
 *
 * Description : SDH Module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULESDHINTERNAL_H_
#define _THAMODULESDHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/sdh/AtModuleSdhInternal.h"
#include "../../../generic/man/AtIpCoreInternal.h"
#include "../man/ThaDeviceReg.h"
#include "../physical/ThaSerdesController.h"

#include "ThaModuleSdh.h"
#include "ThaSdhLine.h"
#include "ThaSdhVc.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleSdhMethods
    {
    uint32 (*OcnRxStsVCIntrStatDeftOffset)(ThaModuleSdh self, uint8 slice);
    uint32 (*OcnRxVtTuIntrStatDeftOffset)(ThaModuleSdh self, uint8 slice, uint8 sts);
    uint8 (*SerdesCoreIdOfLine)(ThaModuleSdh self, AtSdhLine line);
    uint8 (*SerdesIdOfLine)(ThaModuleSdh self, AtSdhLine line);
    uint8 (*NumLinesPerPart)(ThaModuleSdh self);
    AtSerdesController (*SerdesControllerCreate)(ThaModuleSdh self, AtSdhLine line, uint32 serdesId);
    eBool (*LineSerdesPrbsIsSupported)(ThaModuleSdh self, AtSdhLine line, AtSerdesController serdes);
    AtPrbsEngine (*LineSerdesPrbsEngineCreate)(ThaModuleSdh self, AtSdhLine line, AtSerdesController serdes);
    eBool (*PayloadUneqIsApplicable)(ThaModuleSdh self);
    eBool (*ErdiIsEnabledByDefault)(ThaModuleSdh self);
    eBool (*VcAisIsEnabledByDefault)(ThaModuleSdh self);
    eBool (*BlockErrorCountersSupported)(ThaModuleSdh self);
    eAtSerdesTimingMode (*DefaultSerdesTimingModeForLine)(ThaModuleSdh self, AtSdhLine line);
    eBool (*AlwaysDisableRdiBackwardWhenTimNotDownstreamAis)(ThaModuleSdh self);
    eBool (*ShouldUpdateTimRdiBackwardWhenAisDownstreamChange)(ThaModuleSdh self);
    eBool (*ShouldUpdatePlmMonitorWhenExpectedPslChange)(ThaModuleSdh self);
    eBool (*CanDisablePwPacketsToPsn)(ThaModuleSdh self);
    eAtSdhLineRate (*LineDefaultRate)(ThaModuleSdh self, AtSdhLine line);
    uint32 (*StartVersionSupport24BitsBlockErrorCounter)(ThaModuleSdh self);
    void (*LineDefaultSet)(ThaModuleSdh self);
    eAtRet (*AsyncLineDefaultSet)(ThaModuleSdh self);
    eBool (*HasLedEngine)(ThaModuleSdh self);
    eBool (*HasCounters)(ThaModuleSdh self);
    void (*AlarmDownstreamEnable)(ThaModuleSdh self, eBool enable);
    eBool (*HasClockOutput)(ThaModuleSdh self);
    eBool (*HasAps)(ThaModuleSdh self);
    uint8 (*PartOfLine)(ThaModuleSdh self, uint8 lineId);
    uint8 (*LocalLineId)(ThaModuleSdh self, uint32 lineId);
    eBool (*NeedSetLineModeForVc)(ThaModuleSdh self);
    eBool (*ShouldRemoveLoopbackOnBinding)(ThaModuleSdh self);
    eBool (*ShouldUpdateSerdesTimingWhenLineRateChange)(ThaModuleSdh self);
    eBool (*UseStaticXc)(ThaModuleSdh self);

    /* Default mapping provider */
    uint8 (*DefaultVc1xSubMapping)(ThaModuleSdh self);

    /* For version controlling */
    AtSdhChannel (*ChannelCreateWithVersion)(ThaModuleSdh self, uint8 lineId, AtSdhChannel parent, uint8 channelType, uint8 channelId, uint8 version);

    /* For interrupt */
    eBool (*HasLineInterrupt)(ThaModuleSdh self, uint32 glbIntr, AtIpCore ipCore);
    eBool (*HasStsInterrupt)(ThaModuleSdh self, uint32 glbIntr, AtIpCore ipCore);
    eBool (*HasVtInterrupt)(ThaModuleSdh self, uint32 glbIntr, AtIpCore ipCore);
    eBool (*BerHardwareInterruptIsSupported)(ThaModuleSdh self);
    uint32 (*StsInterruptStatusGet)(ThaModuleSdh self, uint32 glbIntr, AtHal hal); /* Return slice OR interrupt */
    uint32 (*VtInterruptStatusGet)(ThaModuleSdh self, uint32 glbIntr, AtHal hal); /* Return slice OR interrupt */
    uint32 (*LineInterruptStatusGet)(ThaModuleSdh self, uint32 glbIntr, AtHal hal); /* Return slice OR interrupt */
    void (*LineInterruptProcess)(ThaModuleSdh self, uint32 glbLineIntr, AtHal hal);
    void (*StsInterruptProcess)(ThaModuleSdh self, uint32 glbStsIntr, AtHal hal);
    void (*VtInterruptProcess)(ThaModuleSdh self, uint32 glbVtIntr, AtHal hal);
    eAtRet (*LineInterruptEnable)(ThaModuleSdh self, eBool enable);
    eAtRet (*StsInterruptEnable)(ThaModuleSdh self, eBool enable);
    eAtRet (*VtInterruptEnable)(ThaModuleSdh self, eBool enable);

    /* PRBS */
    AtPrbsEngine (*VcPrbsEngineCreate)(ThaModuleSdh self, AtSdhVc vc);

    /* For interrupt processing only
     * Please important notice that sliceId and stsId are hardware ID numbering
     * for interrupt processing. They may not be compatible to hardware ID
     * numbering of configuration. */
    AtSdhLine (*LineFromHwIdGet)(ThaModuleSdh self, eAtModule phyModule, uint8 sliceId, uint8 lineInSlice);
    AtSdhPath (*AuPathFromHwIdGet)(ThaModuleSdh self, eAtModule phyModule, uint8 sliceId, uint8 stsId);
    AtSdhPath (*TuPathFromHwIdGet)(ThaModuleSdh self, eAtModule phyModule, uint8 sliceId, uint8 stsId, uint8 vtgId, uint8 vtId);

    /* For debugging */
    eAtRet (*CommonRegisterOffsetsDisplay)(ThaModuleSdh self, AtSdhChannel channel);
    eBool (*PohIsReady)(ThaModuleSdh self);
    eAtRet (*DebugCountersModuleSet)(ThaModuleSdh self, uint32 module);

    /* Backward compatible */
    uint32 (*StartVersionSupportLineTiming)(ThaModuleSdh self);
    AtObjectAny (*LineAttControllerCreate)(ThaModuleSdh self, AtChannel channel);
    AtObjectAny (*PathAttControllerCreate)(ThaModuleSdh self, AtChannel channel);
    AtObjectAny (*Vc1xAttControllerCreate)(ThaModuleSdh self, AtChannel channel);

    /* DCC */
    AtHdlcChannel (*DccChannelObjectCreate)(ThaModuleSdh self, uint32 dccId, AtSdhLine line, eAtSdhLineDccLayer layer);
    uint32 (*MaxNumDccHdlcChannels)(ThaModuleSdh self);
    }tThaModuleSdhMethods;

typedef struct tThaModuleSdh
    {
    tAtModuleSdh super;
    const tThaModuleSdhMethods *methods;

    /* Private data */
    eBool interruptEnabled;
    /*async init*/
    uint32 asyncInitState;
    uint32 asyncLineDefaultInitState;

    /* For select module to get counter from */
	uint32 countersModule;
    }tThaModuleSdh;

typedef struct tThaModuleSdhV1 * ThaModuleSdhV1;
typedef struct tThaModuleSdhV1
    {
    tThaModuleSdh super;

    /* Private data */
    }tThaModuleSdhV1;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleSdh ThaModuleSdhObjectInit(AtModuleSdh self, AtDevice device);
void ThaSdhModuleLineDefaultSet(ThaModuleSdh self, uint32 dbLineId);
void ThaModuleSdhPostInit(AtModule self);
eAtRet ThaModuleSdhLineIdFromDbLineId(AtModuleSdh self, uint8 dbLineId);
eBool ThaModuleSdhShouldUpdateSerdesTimingWhenLineRateChange(ThaModuleSdh self);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULESDHINTERNAL_H_ */


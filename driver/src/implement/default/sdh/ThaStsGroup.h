/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : ThaStsGroup.h
 * 
 * Created Date: Nov 4, 2016
 *
 * Description : STS group of product
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASTSGROUP_H_
#define _THASTSGROUP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModule.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaStsGroup* ThaStsGroup;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtStsGroup ThaStsGroupNew(AtModule module, uint32 groupId);

#ifdef __cplusplus
}
#endif
#endif /* _THASTSGROUP_H_ */


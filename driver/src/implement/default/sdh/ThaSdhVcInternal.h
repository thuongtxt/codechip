/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : ThaSdhVcInternal.h
 * 
 * Created Date: Oct 19, 2012
 *
 * Description : Thalassa VC concrete classes
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASDHVCINTERNAL_H_
#define _THASDHVCINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPdhDe3.h"
#include "AtPrbsEngine.h"
#include "AtSurEngine.h"

#include "../../../generic/sdh/AtSdhVcInternal.h"
#include "../ocn/ThaModuleOcn.h"
#include "../man/ThaDeviceInternal.h"
#include "../man/ThaDeviceInternal.h"

#include "ThaSdhVc.h"
#include "ThaSdhPointerAccumulator.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaSdhVcMethods
    {
    ThaPohProcessor (*PohProcessorCreate)(ThaSdhVc self);
    ThaCdrController (*CdrControllerCreate)(ThaSdhVc self);
    ThaCdrController (*CdrControllerGet)(ThaSdhVc self);
    ThaCdrController (*RedirectCdrController)(ThaSdhVc self);
    eAtRet (*EparEnable)(ThaSdhVc self, eBool enable);
    eBool  (*EparIsEnabled)(ThaSdhVc self);
    eBool (*HasOcnEpar)(ThaSdhVc self);
    uint32 (*OcnDefaultOffset)(ThaSdhVc self);
    ThaPohProcessor (*PohProcessorGet)(ThaSdhVc self);
    eAtRet (*PohDefaultSet)(ThaSdhVc self);
    ThaSdhPointerAccumulator *(*PointerAccumulatorAddress)(ThaSdhVc self);
    eBool (*HasLoOrderMapping)(ThaSdhVc self);
    eAtRet (*PwCepSet)(ThaSdhVc self);
    eAtRet (*MappingRestore)(ThaSdhVc self);
    eBool (*HasOcnStuffing)(ThaSdhVc self);
    eBool (*NeedPohRestore)(ThaSdhVc self);
    eAtRet (*AutoRdiReiEnable)(ThaSdhVc self, eBool enable);
    uint32 (*OcnPJCounterOffset)(ThaSdhVc self, eBool isPjPos);

    /* Function for XC using */
    uint32 (*XcBaseAddressGet)(ThaSdhVc self);
    uint32 (*NumSourceVcs)(ThaSdhVc self);
    void (*SourceVcSet)(ThaSdhVc self, uint32 vcIndex, ThaSdhVc sourceVc);
    ThaSdhVc (*SourceVcGet)(ThaSdhVc self, uint32 vcIndex);
    AtList (*DestVcs)(ThaSdhVc self);
    }tThaSdhVcMethods;

typedef struct tThaSdhVc
    {
    tAtSdhVc super;
    const tThaSdhVcMethods *methods;

    /* Private data */
    uint8 enabled;
    ThaPohProcessor pohProcessor;
    ThaCdrController cdrController;
    AtPrbsEngine prbsEngine;
    AtSurEngine surEngine;
    eBool isInPwBindingProgress;
    uint8 erdiEnabled;
    }tThaSdhVc;

/* AU VC additional methods */
typedef struct tThaSdhAuVcMethods
    {
    eAtRet (*Ds3Map)(ThaSdhAuVc self);
    eBool  (*NeedConfigurePdh)(ThaSdhAuVc self);
    eAtRet (*PgStsSlaverIndicate)(ThaSdhAuVc self, eAtSdhVcMapType vcMapType);
    eBool  (*NeedReconfigureSSBitWhenBindingPw)(ThaSdhAuVc self);
    eAtSdhTugMapType (*DefaultTug3MapType)(ThaSdhAuVc self);
    eAtRet (*MapFrameModeDefaultSet)(ThaSdhAuVc self);
    eAtRet (*Vc4xPldSet)(ThaSdhAuVc self);

    /* H4 LOM monitoring. */
    eAtRet (*LomMonitorEnable)(ThaSdhAuVc self, eBool enable);
    eBool  (*LomMonitorIsEnabled)(ThaSdhAuVc self);
    }tThaSdhAuVcMethods;

/* AU VC class representation */
typedef struct tThaSdhAuVc
    {
    tThaSdhVc super;
    const tThaSdhAuVcMethods *methods;

    /* Private data */
    AtObjectAny attController;
    eBool isMapDe3Liu;
    }tThaSdhAuVc;

/* TU-3 VC class representation */
typedef struct tThaSdhTu3Vc
    {
    tThaSdhAuVc super;

    /* Private data */
    }tThaSdhTu3Vc;

/* VC-1x class */
typedef struct tThaSdhVc1x * ThaSdhVc1x;

/* VC-1x additional methods */
typedef struct tThaSdhVc1xMethods
    {
    AtPdhChannel (*De1Create)(ThaSdhVc1x self, AtModulePdh modulePdh);
    eBool  (*PohInsEn)(ThaSdhVc1x self);
    }tThaSdhVc1xMethods;

/* VC-1x class representation */
typedef struct tThaSdhVc1x
    {
    tThaSdhVc super;
    const tThaSdhVc1xMethods *methods;

    /* Private data */
    AtObjectAny attController;
    }tThaSdhVc1x;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhVc ThaSdhAuVcObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhVc ThaSdhVc1xObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhVc ThaSdhTu3VcObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module);

eAtRet ThaSdhAuVcDs3Map(ThaSdhAuVc self);
ThaCdrController ThaSdhVcCdrControllerObjectGet(ThaSdhVc self);

/* Default constructor */
AtSdhVc ThaSdhVcObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module);

/* Util functions to configure Thalassa */
eAtSdhTtiMode ThaDefaultPohProcessorV2TtiModeHw2Sw(uint8 value);
uint8 ThaDefaultPohProcessorV2TtiHwValueByModeGet(eAtSdhTtiMode ttiMode);

uint8 SdhTug2HwStsGet(AtSdhChannel self);
AtPdhDe3 ThaSdhVc3CreateDe3(AtSdhChannel sdhVc);

eAtRet ThaSdhAuVcPgStsSlaverIndicate(AtSdhChannel self, eAtSdhVcMapType vcMapType);

void ThaSdhVcInPwBindingProgressSet(ThaSdhVc self, eBool isInPwBindingProgress);
eBool ThaSdhVcIsInPwBindingProgress(ThaSdhVc self);
uint32 ThaSdhVcPointerCounterUpdateThenClear(ThaSdhVc self, uint16 counterType,uint32 hwCounter, eBool read2Clear);

#ifdef __cplusplus
}
#endif
#endif /* _THASDHVCINTERNAL_H_ */


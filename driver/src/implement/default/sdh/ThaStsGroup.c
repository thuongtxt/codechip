/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APS
 *
 * File        : ThaStsGroup.c
 *
 * Created Date: Nov 4, 2016
 *
 * Description : Default STS pass-through group implementation.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtStsGroup.h"
#include "../../../generic/man/AtModuleInternal.h"
#include "../../../generic/sdh/AtStsGroupInternal.h"
#include "../man/ThaDevice.h"
#include "../ocn/ThaModuleOcn.h"
#include "../xc/ThaVcCrossConnect.h"
#include "../aps/ThaModuleApsOcnReg.h"
#include "../aps/ThaModuleHardAps.h"
#include "ThaStsGroup.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((ThaStsGroup)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaStsGroup
    {
    tAtStsGroup super;
    }tThaStsGroup;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtStsGroupMethods       m_AtStsGroupOveride;

/* Save superclass's implementation. */
static const tAtStsGroupMethods *m_AtStsGroupMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtDevice DeviceGet(AtStsGroup self)
    {
    return AtModuleDeviceGet((AtModule)AtStsGroupModuleGet(self));
    }

static ThaModuleOcn ModuleOcn(AtStsGroup self)
    {
    return (ThaModuleOcn)AtDeviceModuleGet(DeviceGet(self), cThaModuleOcn);
    }

static ThaVcCrossConnect VcCrossConnectGet(AtStsGroup self)
    {
    AtModuleXc module = (AtModuleXc)AtDeviceModuleGet(DeviceGet(self), cAtModuleXc);
    return (ThaVcCrossConnect)AtModuleXcVcCrossConnectGet(module);
    }

static eAtRet StsAdd(AtStsGroup self, AtSdhSts sts1)
    {
    eAtRet ret;
    ThaVcCrossConnect vcXc = VcCrossConnectGet(self);
    AtSdhChannel channel = (AtSdhChannel)sts1;

    /* RX group ID */
    ret = ThaOcnStsGroupIdSet(channel, AtSdhChannelSts1Get(channel), AtStsGroupIdGet(self));
    if (ret != cAtOk)
        return ret;

    /* TX group ID */
    ret = ThaVcCrossConnectVcSelectorSet(vcXc, (AtSdhChannel)sts1, cThaXcVcSelectorPassthrough, AtStsGroupIdGet(self));
    if (ret != cAtOk)
        return ret;

    return m_AtStsGroupMethods->StsAdd(self, sts1);
    }

static eAtRet StsDelete(AtStsGroup self, AtSdhSts sts1)
    {
    eAtRet ret;
    ThaVcCrossConnect vcXc = VcCrossConnectGet(self);
    AtSdhChannel channel = (AtSdhChannel)sts1;

    /* RX group ID */
    ret = ThaOcnStsGroupIdSet(channel, AtSdhChannelSts1Get(channel), cBit31_0);
    if (ret != cAtOk)
        return ret;

    /* TX group ID */
    ret = ThaVcCrossConnectVcSelectorSet(vcXc, (AtSdhChannel)sts1, cThaXcVcSelectorPassthrough, cInvalidUint32);
    if (ret != cAtOk)
        return ret;

    return m_AtStsGroupMethods->StsDelete(self, sts1);
    }

static eAtRet Sts1DisconnectAtIndex(AtStsGroup dest, uint32 stsIndex)
    {
    ThaVcCrossConnect vcXc = VcCrossConnectGet(dest);
    AtSdhChannel destSts = (AtSdhChannel)AtStsGroupStsAtIndex(dest, stsIndex);
    return ThaVcCrossConnectVcDisconnectAtIndex(vcXc, destSts, cThaXcVcConnectionPassthrough);
    }

static eAtRet Sts1ConnectAtIndex(AtStsGroup self, AtStsGroup dest, uint32 stsIndex)
    {
    ThaVcCrossConnect vcXc = VcCrossConnectGet(dest);
    AtSdhChannel destSts = (AtSdhChannel)AtStsGroupStsAtIndex(dest, stsIndex);
    AtSdhChannel sourceSts = (AtSdhChannel)AtStsGroupStsAtIndex(self, stsIndex);
    return ThaVcCrossConnectVcConnectAtIndex(vcXc, destSts, sourceSts, cThaXcVcConnectionPassthrough);
    }

static eAtRet DestGroupSet(AtStsGroup self, AtStsGroup destGroup)
    {
    uint32 stsIndex;
    uint32 numSts = AtStsGroupNumSts(self);
    AtStsGroup oldGroup = AtStsGroupDestGroupGet(self);
    eAtRet ret = cAtOk;

    if (oldGroup != NULL)
        {
        for (stsIndex = 0; stsIndex < numSts; stsIndex++)
            ret |= Sts1DisconnectAtIndex(oldGroup, stsIndex);
        }

    if (ret != cAtOk)
        return ret;

    if (destGroup != NULL)
        {
        for (stsIndex = 0; stsIndex < numSts; stsIndex++)
            ret |= Sts1ConnectAtIndex(self, destGroup, stsIndex);
        }

    if (ret != cAtOk)
        return ret;

    return m_AtStsGroupMethods->DestGroupSet(self, destGroup);
    }

static uint32 TxGroupMask(AtStsGroup self)
    {
    return cBit16 << AtStsGroupIdGet(self);
    }

static uint32 TxGroupShift(AtStsGroup self)
    {
    return AtStsGroupIdGet(self) + 16;
    }

static uint32 RxGroupMask(AtStsGroup self)
    {
    return cBit0 << AtStsGroupIdGet(self);
    }

static uint32 RxGroupShift(AtStsGroup self)
    {
    return AtStsGroupIdGet(self);
    }

static eAtRet PassThroughEnable(AtStsGroup self, eBool enable)
    {
    ThaModuleOcn ocnModule = ModuleOcn(self);
    AtStsGroup destGroup = AtStsGroupDestGroupGet(self);
    uint32 regAddr = ThaModuleOcnBaseAddress(ocnModule) + cAf6Reg_glbpasgrpenb_reg;
    uint32 regVal = mModuleHwRead(ocnModule, regAddr);

    mFieldIns(&regVal, RxGroupMask(self), RxGroupShift(self), (enable) ? 1 : 0);
    mFieldIns(&regVal, TxGroupMask(destGroup), TxGroupShift(destGroup), (enable) ? 1 : 0);
    mModuleHwWrite(ocnModule, regAddr, regVal);

    return cAtOk;
    }

static eBool PassThroughIsEnabled(AtStsGroup self)
    {
    ThaModuleOcn ocnModule = ModuleOcn(self);
    uint32 regAddr = ThaModuleOcnBaseAddress(ocnModule) + cAf6Reg_glbpasgrpenb_reg;
    uint32 regVal = mModuleHwRead(ocnModule, regAddr);
    uint8 fieldVal;
    mFieldGet(regVal, RxGroupMask(self), RxGroupShift(self), uint8, &fieldVal);
    return (fieldVal) ? cAtTrue : cAtFalse;
    }

static void OverrideAtStsGroup(AtStsGroup self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtStsGroupMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtStsGroupOveride, m_AtStsGroupMethods, sizeof(m_AtStsGroupOveride));

        mMethodOverride(m_AtStsGroupOveride, StsAdd);
        mMethodOverride(m_AtStsGroupOveride, StsDelete);
        mMethodOverride(m_AtStsGroupOveride, DestGroupSet);
        mMethodOverride(m_AtStsGroupOveride, PassThroughEnable);
        mMethodOverride(m_AtStsGroupOveride, PassThroughIsEnabled);
        }

    mMethodsSet(self, &m_AtStsGroupOveride);
    }

static void Override(AtStsGroup self)
    {
    OverrideAtStsGroup(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaStsGroup);
    }

static AtStsGroup ObjectInit(AtStsGroup self, uint32 groupId, AtModule module)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Supper constructor */
    if (AtStsGroupObjectInit(self, groupId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtStsGroup ThaStsGroupNew(AtModule module, uint32 groupId)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtStsGroup newStsGroup = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    if (newStsGroup == NULL)
        return NULL;

    return ObjectInit(newStsGroup, groupId, module);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : ThaSdhLineV1.c
 *
 * Created Date: Mar 27, 2013
 *
 * Description : SDH Line of previous hardware version
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../ThaSdhLineInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaSdhLineV1
    {
    tThaSdhLine super;

    /* Private data */
    }tThaSdhLineV1;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaSdhLineMethods m_ThaSdhLineOverride;

/* Save super implementation */
static const tThaSdhLineMethods *m_ThaSdhLineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet LoopInEnable(ThaSdhLine self, eBool enable)
    {
    return mMethodsGet(self)->FpgaLoopInEnable(self, enable);
    }

static eAtRet LoopOutEnable(ThaSdhLine self, eBool enable)
    {
    return mMethodsGet(self)->FpgaLoopOutEnable(self, enable);
    }

static eBool LoopInIsEnabled(ThaSdhLine self)
    {
    return mMethodsGet(self)->FpgaLoopInIsEnable(self);
    }

static eBool LoopOutIsEnabled(ThaSdhLine self)
    {
    return mMethodsGet(self)->FpgaLoopOutIsEnable(self);
    }

static void OverrideThaSdhLine(AtSdhLine self)
    {
    ThaSdhLine line = (ThaSdhLine)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaSdhLineMethods = mMethodsGet(line);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhLineOverride, m_ThaSdhLineMethods, sizeof(m_ThaSdhLineOverride));

        mMethodOverride(m_ThaSdhLineOverride, LoopInEnable);
        mMethodOverride(m_ThaSdhLineOverride, LoopInIsEnabled);
        mMethodOverride(m_ThaSdhLineOverride, LoopOutEnable);
        mMethodOverride(m_ThaSdhLineOverride, LoopOutIsEnabled);
        }

    mMethodsSet(line, &m_ThaSdhLineOverride);
    }

static void Override(AtSdhLine self)
    {
    OverrideThaSdhLine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaSdhLineV1);
    }

static AtSdhLine ObjectInit(AtSdhLine self, uint32 channelId, AtModuleSdh module, uint8 version)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaSdhLineObjectInit(self, channelId, module, version) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhLine ThaSdhLineV1New(uint32 channelId, AtModuleSdh module, uint8 version)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhLine newLine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newLine == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newLine, channelId, module, version);
    }

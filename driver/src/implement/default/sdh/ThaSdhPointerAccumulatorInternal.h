/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : ThaSdhPointerAccumulatorInternal.h
 * 
 * Created Date: Jan 25, 2016
 *
 * Description : Adjustment counter accumulator
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASDHPOINTERACCUMULATORINTERNAL_H_
#define _THASDHPOINTERACCUMULATORINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/man/AtDriverInternal.h"
#include "ThaSdhPointerAccumulator.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/


/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
typedef struct tThaSdhPointerAccumulator
    {
    tAtObject super;

    /* Private data */
    uint32 rxPiIncCnt;
    uint32 rxPiDecCnt;
    uint32 txPgIncrCnt;
    uint32 txPgDecCnt;
    }tThaSdhPointerAccumulator;

#ifdef __cplusplus
}
#endif
#endif /* _THASDHPOINTERACCUMULATORINTERNAL_H_ */

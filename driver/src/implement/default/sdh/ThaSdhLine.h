/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : ThaSdhLine.h
 * 
 * Created Date: Mar 27, 2013
 *
 * Description : SDH Line
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASDHLINE_H_
#define _THASDHLINE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSdhLine.h"
#include "../poh/ThaTtiProcessor.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaSdhLine * ThaSdhLine;

typedef enum eThaSdhLineExtendedKBytePosition
    {
    cThaSdhLineExtendedKBytePositionInvalid,
    cThaSdhLineExtendedKBytePositionD1Sts4,
    cThaSdhLineExtendedKBytePositionD1Sts10
    } eThaSdhLineExtendedKBytePosition;

typedef enum eThaSdhLineKByteSource
	{
	cThaSdhLineKByteSourceUnknown,
	cThaSdhLineKByteSourceCpu,
	cThaSdhLineKByteSourceEth
	}eThaSdhLineKByteSource;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Default concretes */
AtSdhLine ThaSdhLineNew(uint32 channelId, AtModuleSdh module, uint8 version);
AtSdhLine ThaSdhLineV1New(uint32 channelId, AtModuleSdh module, uint8 version);

eAtRet ThaSdhLineSerdesPhysicalParameterSet(AtSdhLine self, uint8 parameterId, uint32 value);
uint32 ThaSdhLineSerdesPhysicalParameterGet(AtSdhLine self, uint8 parameterId);

uint32 ThaSdhLinePartOffset(ThaSdhLine self);
uint32 ThaSdhLineDiagnosticBaseAddress(ThaSdhLine self);

uint32 ThaSdhLineBerDefectGet(ThaSdhLine self);
void ThaSdhLineBerStatusLatch(ThaSdhLine self, uint32 changedAlarms, uint32 currentStatus);

/* For interrupt debugging */
uint32 ThaSdhLineLatchedAlarmsGet(ThaSdhLine self);
void ThaSdhLineLatchedAlarmsSet(ThaSdhLine self, uint32 alarms);

eAtSdhLineRate ThaSdhLineHighRate(ThaSdhLine self);
eAtSdhLineRate ThaSdhLineLowRate(ThaSdhLine self);

ThaTtiProcessor ThaSdhLineTtiProcessor(AtSdhLine self);

/* Extended K-Byte */
eAtRet ThaSdhLineExtendedKByteEnable(ThaSdhLine self, eBool enable);
eBool ThaSdhLineExtendedKByteIsEnabled(ThaSdhLine self);
eAtRet ThaSdhLineExtendedKBytePositionSet(ThaSdhLine self, eThaSdhLineExtendedKBytePosition position);
eThaSdhLineExtendedKBytePosition ThaSdhLineExtendedKBytePositionGet(ThaSdhLine self);
eAtRet ThaSdhLineKByteSourceSet(ThaSdhLine self, eThaSdhLineKByteSource source);
eThaSdhLineKByteSource ThaSdhLineKByteSourceGet(ThaSdhLine self);
eAtRet ThaSdhLineEK1TxSet(ThaSdhLine self, uint8 value);
eAtRet ThaSdhLineEK2TxSet(ThaSdhLine self, uint8 value);
uint8 ThaSdhLineEK1TxGet(ThaSdhLine self);
uint8 ThaSdhLineEK2TxGet(ThaSdhLine self);
uint8 ThaSdhLineEK1RxGet(ThaSdhLine self);
uint8 ThaSdhLineEK2RxGet(ThaSdhLine self);

/* Product concretes */
AtSdhLine Tha60030081SdhLineNew(uint32 channelId, AtModuleSdh module, uint8 version);
AtSdhLine Tha60031031SdhLineNew(uint32 channelId, AtModuleSdh module, uint8 version);
AtSdhLine Tha60035021SdhLineNew(uint32 channelId, AtModuleSdh module, uint8 version);
AtSdhLine Tha60060011SdhLineNew(uint32 channelId, AtModuleSdh module, uint8 version);
AtSdhLine Tha60070023SdhLineNew(uint32 channelId, AtModuleSdh module, uint8 version);
AtSdhLine Tha60150011SdhLineNew(uint32 channelId, AtModuleSdh module, uint8 version);
AtSdhLine Tha60070061SdhLineNew(uint32 channelId, AtModuleSdh module, uint8 version);
AtSdhLine Tha60210031SdhLineNew(uint32 channelId, AtModuleSdh module);
AtSdhLine Tha60210061SdhLineNew(uint32 channelId, AtModuleSdh module);
AtSdhLine Tha60290011SdhLineNew(uint32 channelId, AtModuleSdh module);
AtSdhLine ThaStmPwProductSdhLineNew(uint32 channelId, AtModuleSdh module, uint8 version);

#ifdef __cplusplus
}
#endif
#endif /* _THASDHLINE_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : ThaModuleSdhRegisterIteratorInternal.h
 * 
 * Created Date: Jan 9, 2013
 *
 * Description : SDH module iterator
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULESDHREGISTERITERATORINTERNAL_H_
#define _THAMODULESDHREGISTERITERATORINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleSdh.h"
#include "../../../generic/memtest/AtModuleRegisterIteratorInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
#define cMaxNumJ0Dwords 16
#define cMaxNumSlices 1
#define cMaxNumSts 12
#define cMaxNumVtInVtg 4
#define cMaxNumVtgInSts 7

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
enum eSdhRegType
    {
    cSdhRegTypeLine,
    cSdhRegTypeSts,
    cSdhRegTypeVt,
    cSdhRegTypeEnd
    };

typedef struct tThaModuleSdhRegisterIterator * ThaModuleSdhRegisterIterator;
typedef struct tThaModuleSdhRegisterIterator
    {
    tAtModuleRegisterIterator super;

    /* Private data */
    uint8 currentRegType;
    uint8 lineId;
    uint8 slice;
    uint8 sts;
    uint8 vtg;
    uint8 vt;
    }tThaModuleSdhRegisterIterator;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtIterator ThaModuleSdhRegisterIteratorObjectInit(AtIterator self, AtModule module);

uint8 ThaModuleSdhRegisterIteratorCurrentRegType(ThaModuleSdhRegisterIterator self);
void ThaModuleSdhRegisterIteratorCurrentRegTypeSet(ThaModuleSdhRegisterIterator self, uint8 regType);
uint8 ThaModuleSdhRegisterIteratorNextLine(ThaModuleSdhRegisterIterator self);
uint8 ThaModuleSdhRegisterIteratorLineGet(ThaModuleSdhRegisterIterator self);
void ThaModuleSdhRegisterIteratorNextSts(ThaModuleSdhRegisterIterator self,
                                         uint8 *slice,
                                         uint8 *sts);
void ThaModuleSdhRegisterIteratorNextVt(ThaModuleSdhRegisterIterator self,
                                        uint8 *slice,
                                        uint8 *sts,
                                        uint8 *vtg,
                                        uint8 *vt);
#ifdef __cplusplus
}
#endif
#endif /* _THAMODULESDHREGISTERITERATORINTERNAL_H_ */

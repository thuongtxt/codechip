/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : ThaSdhAu.c
 *
 * Created Date: Sep 7, 2012
 *
 * Description : Thalassa SDH default implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../generic/sdh/AtSdhAuInternal.h"
#include "../../../generic/man/AtModuleInternal.h"
#include "../../../util/coder/AtCoderUtil.h"
#include "../man/ThaDeviceInternal.h"

#include "ThaModuleSdh.h"
#include "ThaModuleSdhInternal.h"
#include "ThaModuleSdhReg.h"
#include "../ocn/ThaModuleOcn.h"
#include "ThaSdhAuInternal.h"
#include "../att/AtAttController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaSdhAu)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtChannelMethods    m_AtChannelOverride;
static tAtSdhChannelMethods m_AtSdhChannelOverride;
static tAtSdhPathMethods    m_AtSdhPathOverride;

/* To save super implementation */
static const tAtObjectMethods     *m_AtObjectMethods     = NULL;
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;
static const tAtSdhChannelMethods *m_AtSdhChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
eAtRet ThaSdhAuTxSsSet(AtSdhPath self, uint8 value, eBool isChannelized);

/*--------------------------- Implementation ---------------------------------*/
static AtObjectAny AttController(AtChannel self)
    {
    ThaModuleSdh module = (ThaModuleSdh)AtChannelModuleGet(self);
    if (mThis(self)->attController == NULL)
        {
        mThis(self)->attController = mMethodsGet(module)->PathAttControllerCreate(module, self);
        if (mThis(self)->attController)
            {
            AtAttControllerSetUp(mThis(self)->attController);
            AtAttControllerInit(mThis(self)->attController);
            }
        }
    return mThis(self)->attController;
    }

static void AttControllerDelete(AtObject self)
    {
    if (mThis(self)->attController)
        AtObjectDelete((AtObject)mThis(self)->attController);
    mThis(self)->attController = NULL;
    }

static void Delete(AtObject self)
    {
    /* Fully delete this object */
    AttControllerDelete(self);
    m_AtObjectMethods->Delete(self);
    }

static ThaModuleOcn ModuleOcn(AtChannel self)
    {
    return (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModuleOcn);
    }

static uint32 OcnTxPGPerChnCtrlDefaultOffset(AtSdhAu self)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)self;
    ThaModuleOcn moduleOcn = ModuleOcn((AtChannel)self);

    return ThaModuleOcnStsVtDefaultOffset(moduleOcn, sdhChannel, AtSdhChannelSts1Get(sdhChannel), 0, 0);
    }

static eAtRet Enable(AtChannel self, eBool enable)
    {
    ((ThaSdhAu)self)->enabled = enable;

    return cAtOk;
    }

static eBool IsEnabled(AtChannel self)
    {
    return ((ThaSdhAu)self)->enabled;
    }

static eBool Au4MapVc1x(AtSdhChannel self)
    {
    uint8 subChannel_i;
    AtSdhChannel vc4;

    if (AtSdhChannelTypeGet(self) != cAtSdhChannelTypeAu4)
        return cAtFalse;

    vc4 = AtSdhChannelSubChannelGet(self, 0);
    if (AtSdhChannelMapTypeGet(vc4) != cAtSdhVcMapTypeVc4Map3xTug3s)
        return cAtFalse;

    for (subChannel_i = 0; subChannel_i < AtSdhChannelNumberOfSubChannelsGet(vc4); subChannel_i++)
        {
        AtSdhChannel tug3 = AtSdhChannelSubChannelGet(vc4, subChannel_i);
        if (AtSdhChannelMapTypeGet(tug3) == cAtSdhTugMapTypeTug3Map7xTug2s)
            return cAtTrue;
        }

    return cAtFalse;
    }

static uint32 DefaultOffset(AtChannel self)
    {
    ThaModuleOcn moduleOcn = ModuleOcn(self);
    AtSdhChannel sdhChannel = (AtSdhChannel)self;

    if (moduleOcn)
        return ThaModuleOcnStsDefaultOffset(moduleOcn, sdhChannel, AtSdhChannelSts1Get(sdhChannel));

    return 0;
    }

static uint32 DefectGet(AtChannel self)
    {
    uint32 address, regVal, alarmState = 0;

    /* Get hardware status */
    address = cThaRegOcnRxStsVCperAlmCurrentStat + DefaultOffset(self);
    regVal = mChannelHwRead(self, address, cThaModuleOcn);

    /* Convert it to software value */
    if (regVal & cThaStsPILopCurStatMask)
        return cAtSdhPathAlarmLop;
    if (regVal & cThaStsPiAisCurStatMask)
        return cAtSdhPathAlarmAis;

    if (Au4MapVc1x((AtSdhChannel)self) && (regVal & cThaStsLomCurStatMask))
        alarmState |= cAtSdhPathAlarmLom;

    /* POH alarm get, not include VC-AIS */
    alarmState |= (AtChannelDefectGet((AtChannel)AtSdhChannelSubChannelGet((AtSdhChannel)self, 0)) & (uint32)(~cAtSdhPathAlarmAis));

    return alarmState;
    }

static uint32 InterruptRead2Clear(AtChannel self, eBool read2Clear)
    {
    uint32 address, regVal;
    uint32 alarmState = 0;
    AtSdhChannel vc;

    /* Read hardware status */
    address = cThaRegOcnRxStsVCperAlmIntrStat + DefaultOffset(self);
    regVal = mChannelHwRead(self, address, cThaModuleOcn);

    /* Convert it to software value */
    if (regVal & cThaStsPiAisStateChgIntrMask)
        alarmState |= cAtSdhPathAlarmAis;
    if (regVal & cThaStsPILopStateChgIntrMask)
        alarmState |= cAtSdhPathAlarmLop;
    if (Au4MapVc1x((AtSdhChannel)self) && (regVal & cThaStsLomStateChgIntrMask))
        alarmState |= cAtSdhPathAlarmLom;

    /* Clear */
    if (read2Clear)
        mChannelHwWrite(self, address, regVal, cThaModuleOcn);

    /* POH alarm clear not include VC-AIS */
    vc = AtSdhChannelSubChannelGet((AtSdhChannel)self, 0);
    if (read2Clear)
        alarmState |= AtChannelDefectInterruptClear((AtChannel)vc) & (uint32)(~cAtSdhPathAlarmAis);
    else
        alarmState |= AtChannelDefectInterruptGet((AtChannel)vc) & (uint32)(~cAtSdhPathAlarmAis);

    return alarmState;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    return InterruptRead2Clear(self, cAtFalse);
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    return InterruptRead2Clear(self, cAtTrue);
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
    AtUnused(self);
    return cAtSdhPathAlarmAis | cAtSdhPathAlarmLop;
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    uint32 regVal, address;

    /* Read to preserve other configuration */
    address = cThaRegOcnRxStsVCperAlmIntrEnCtrl + DefaultOffset(self);
    regVal = mChannelHwRead(self, address, cThaModuleOcn);

    /* Configure mask */
    if (defectMask & cAtSdhPathAlarmAis)
        mFieldIns(&regVal, cThaStsPiAisStateChgIntrEnMask, cThaStsPiAisStateChgIntrEnShift, (enableMask & cAtSdhPathAlarmAis) ? 1 : 0);
    if (defectMask & cAtSdhPathAlarmLop)
        mFieldIns(&regVal, cThaStsPILopStateChgIntrEnMask, cThaStsPILopStateChgIntrEnShift, (enableMask & cAtSdhPathAlarmLop) ? 1 : 0);

    /* Apply */
    mChannelHwWrite(self, address, regVal, cThaModuleOcn);

    return cAtOk;
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    uint32 regVal, address;
    uint32 mask = 0;

    /* Get hardware masks */
    address = cThaRegOcnRxStsVCperAlmIntrEnCtrl + DefaultOffset(self);
    regVal = mChannelHwRead(self, address, cThaModuleOcn);

    /* Convert to software masks */
    if (regVal & cThaStsPiAisStateChgIntrEnMask)
        mask |= cAtSdhPathAlarmAis;
    if (regVal & cThaStsPILopStateChgIntrEnMask)
        mask |= cAtSdhPathAlarmLop;

    return mask;
    }

static eBool VcIsChannelized(ThaSdhAu self)
    {
    eAtSdhVcMapType vcMapType;
    AtSdhChannel vc = AtSdhChannelSubChannelGet((AtSdhChannel)self, 0);

    if (vc == NULL)
        return cAtFalse;

    vcMapType = AtSdhChannelMapTypeGet(vc);
    if ((vcMapType == cAtSdhVcMapTypeVc4Map3xTug3s) ||
       (vcMapType == cAtSdhVcMapTypeVc3Map7xTug2s))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 TxForcibleAlarmsGet(AtChannel self)
    {
    if (VcIsChannelized((ThaSdhAu)self))
        return cAtSdhPathAlarmAis | cAtSdhPathAlarmLop | cAtSdhPathAlarmLom;

    return cAtSdhPathAlarmAis | cAtSdhPathAlarmLop;
    }

static eBool CanForceTxAlarms(ThaSdhAu self, uint32 alarmType)
    {
    return (alarmType & AtChannelTxForcableAlarmsGet((AtChannel)self)) ? cAtTrue : cAtFalse;
    }

static eAtRet HelperTxAlarmForceOnStsRegister(AtChannel self, uint32 alarmType, eBool enable)
    {
    uint32 regVal, address;

    address = cThaRegOcnTxPohInsPerChnCtrl + DefaultOffset(self);
    regVal  = mChannelHwRead(self, address, cThaModuleOcn);

    if (alarmType & cAtSdhPathAlarmAis)
        mFieldIns(&regVal, cThaTxPg2StsAisPFrcMask, cThaTxPg2StsAisPFrcShift, mBoolToBin(enable));

    if (alarmType & cAtSdhPathAlarmLom)
        mFieldIns(&regVal, cThaTxPg2StsLomPFrcMask, cThaTxPg2StsLomPFrcShift, mBoolToBin(enable));

    if (alarmType & cAtSdhPathAlarmLop)
        mFieldIns(&regVal, cThaTxPg2StsLopPFrcMask, cThaTxPg2StsLopPFrcShift, mBoolToBin(enable));

    mChannelHwWrite(self, address, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eAtRet HelperTxAlarmForceOnStsVtRegister(AtChannel self, uint32 alarmType, eBool enable)
    {
    uint32 regVal, address;

    address = cThaRegOcnTxPGPerChnCtrl + OcnTxPGPerChnCtrlDefaultOffset((AtSdhAu)self);
    regVal  = mChannelHwRead(self, address, cThaModuleOcn);

    if (alarmType & cAtSdhPathAlarmAis)
        mFieldIns(&regVal, cThaTxPgStsVtTuAisPFrcMask, cThaTxPgStsVtTuAisPFrcShift, mBoolToBin(enable));

    if (alarmType & cAtSdhPathAlarmLop)
        mFieldIns(&regVal, cThaTxPgStsVtTuLopPFrcMask, cThaTxPgStsVtTuLopPFrcShift, mBoolToBin(enable));

    mChannelHwWrite(self, address, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eAtRet HwTxAlarmForce(AtChannel self, uint32 alarmType, eBool enable)
    {
    if (!CanForceTxAlarms(mThis(self), alarmType))
        return cAtErrorModeNotSupport;

    if (VcIsChannelized((ThaSdhAu)self))
        return HelperTxAlarmForceOnStsRegister(self, alarmType, enable);

    return HelperTxAlarmForceOnStsVtRegister(self, alarmType, enable);
    }

static eAtRet TxAlarmForce(AtChannel self, uint32 alarmType)
    {
    return HwTxAlarmForce(self, alarmType, cAtTrue);
    }

static eAtRet TxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    return HwTxAlarmForce(self, alarmType, cAtFalse);
    }

static uint32 TxForcedAlarmGet(AtChannel self)
    {
    uint32 forcedAlarms = 0;
    uint32 regAddr = cThaRegOcnTxPohInsPerChnCtrl + DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);

    if (regVal & cThaTxPg2StsAisPFrcMask)
        forcedAlarms |= cAtSdhPathAlarmAis;

    if (regVal & cThaTxPg2StsLomPFrcMask)
        forcedAlarms |= cAtSdhPathAlarmLom;

    if (regVal & cThaTxPg2StsLopPFrcMask)
        forcedAlarms |= cAtSdhPathAlarmLop;

    return forcedAlarms;
    }

static void AllAlarmsUnForce(AtChannel self)
    {
    AtChannelTxAlarmUnForce(self, AtChannelTxForcableAlarmsGet(self));
    AtChannelRxAlarmUnForce(self, AtChannelRxForcableAlarmsGet(self));
    }

static eBool IsChannellized(AtSdhPath self)
    {
    uint8 vcMapType;
    AtSdhChannel vc = AtSdhChannelSubChannelGet((AtSdhChannel)self, 0);

    if (vc == NULL)
        return cAtFalse;

    vcMapType = AtSdhChannelMapTypeGet(vc);
    if ((vcMapType == cAtSdhVcMapTypeVc4Map3xTug3s) || (vcMapType == cAtSdhVcMapTypeVc3Map7xTug2s))
        return cAtTrue;

    return cAtFalse;
    }

static eAtModuleSdhRet TxSsSet(AtSdhPath self, uint8 value)
    {
    mThis(self)->ssBit = value;
    return ThaSdhAuTxSsSet(self, value, IsChannellized(self));
    }

static uint8 TxSsGet(AtSdhPath self)
    {
    return mThis(self)->ssBit;
    }

static eAtModuleSdhRet ExpectedSsSet(AtSdhPath self, uint8 value)
    {
    uint32 address, regVal;

    /* Read the OCN Tx POH Insertion Slide 1 Per Channel Control register */
    address = cThaRegOcnRxStsPiPerChnCtrl + DefaultOffset((AtChannel)self);
    regVal = mChannelHwRead(self, address, cThaModuleOcn);

    /* Set SS bit value */
    mFieldIns(&regVal, cThaRxStsPiSsDetPatMask, cThaRxStsPiSsDetPatShift, value);
    mChannelHwWrite(self, address, regVal, cThaModuleOcn);

    return cAtOk;
    }

static uint8 ExpectedSsGet(AtSdhPath self)
    {
    uint32 address, regVal;
    uint8 ssValue;

    /* Read the OCN Tx POH Insertion Slide 1 Per Channel Control register */
    address = cThaRegOcnRxStsPiPerChnCtrl + DefaultOffset((AtChannel)self);
    regVal = mChannelHwRead(self, address, cThaModuleOcn);

    /* Get SS bit value */
    mFieldGet(regVal , cThaRxStsPiSsDetPatMask , cThaRxStsPiSsDetPatShift , uint8, &ssValue);

    return ssValue;
    }

static eAtModuleSdhRet SsCompareEnable(AtSdhPath self, eBool enable)
    {
    uint32 regVal, address;

    address = cThaRegOcnRxStsPiPerChnCtrl + DefaultOffset((AtChannel)self);
    regVal = mChannelHwRead(self, address, cThaModuleOcn);
    mFieldIns(&regVal, cThaRxStsPiSsDetEnMask, cThaRxStsPiSsDetEnShift, mBoolToBin(enable));
    mChannelHwWrite(self, address, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eBool SsCompareIsEnabled(AtSdhPath self)
    {
    uint32 address, regVal;

    address = cThaRegOcnRxStsPiPerChnCtrl + DefaultOffset((AtChannel)self);
    regVal = mChannelHwRead(self, address, cThaModuleOcn);
    return (regVal & cThaRxStsPiSsDetEnMask) ? cAtTrue : cAtFalse;
    }

static uint8 NumChannels(AtSdhChannel self)
    {
    return (uint8)(AtSdhChannelNumSlaveChannels(self) + 1);
    }

static uint8 NumSts(AtSdhChannel self)
    {
    eAtSdhChannelType auType = AtSdhChannelTypeGet(self);

    if (auType == cAtSdhChannelTypeAu3)     return 1;
    if (auType == cAtSdhChannelTypeAu4)     return 3;
    if (auType == cAtSdhChannelTypeAu4_4c)  return 12;
    if (auType == cAtSdhChannelTypeAu4_16c) return 48;
    if (auType == cAtSdhChannelTypeAu4_64c) return 192;
    if (auType == cAtSdhChannelTypeAu4_nc)  return (uint8)(NumChannels(self) * 3);
    if (auType == cAtSdhChannelTypeAu4_16nc)return (uint8)(NumChannels(self) * 48);

    return 0;
    }

static uint8 VcType(AtSdhChannel self)
    {
    eAtSdhChannelType auType = AtSdhChannelTypeGet(self);

    if (auType == cAtSdhChannelTypeAu3)     return cAtSdhChannelTypeVc3;
    if (auType == cAtSdhChannelTypeAu4)     return cAtSdhChannelTypeVc4;
    if (auType == cAtSdhChannelTypeAu4_4c)  return cAtSdhChannelTypeVc4_4c;
    if (auType == cAtSdhChannelTypeAu4_16c) return cAtSdhChannelTypeVc4_16c;
    if (auType == cAtSdhChannelTypeAu4_64c) return cAtSdhChannelTypeVc4_64c;
    if (auType == cAtSdhChannelTypeAu4_nc)  return cAtSdhChannelTypeVc4_nc;
    if (auType == cAtSdhChannelTypeAu4_16nc)return cAtSdhChannelTypeVc4_16nc;

    return cAtSdhChannelTypeUnknown;
    }

static uint8 CxMapType(AtSdhChannel auVc)
    {
    uint8 channelType = AtSdhChannelTypeGet(auVc);
    switch (channelType)
        {
        case cAtSdhChannelTypeVc4_64c : return cAtSdhVcMapTypeVc4_64cMapC4_64c;
        case cAtSdhChannelTypeVc4_16c : return cAtSdhVcMapTypeVc4_16cMapC4_16c;
        case cAtSdhChannelTypeVc4_4c  : return cAtSdhVcMapTypeVc4_4cMapC4_4c;
        case cAtSdhChannelTypeVc4_16nc: return cAtSdhVcMapTypeVc4_16ncMapC4_16nc;
        case cAtSdhChannelTypeVc4_nc  : return cAtSdhVcMapTypeVc4_ncMapC4_nc;
        case cAtSdhChannelTypeVc4     : return cAtSdhVcMapTypeVc4MapC4;
        case cAtSdhChannelTypeVc3     : return cAtSdhVcMapTypeVc3MapC3;
        default:                        return cAtSdhVcMapTypeNone;
        }
    }

static eAtRet ConcateReset(AtSdhChannel self)
    {
    return ThaOcnStsConcat(self, AtSdhChannelSts1Get(self), NumSts(self));
    }

static void AllHwStsReset(AtSdhChannel self)
    {
    /* Delete old HW STS IDs, then they will be automatically re-created later. */
    AtSdhChannelAllHwStsDelete(self);
    AtSdhChannelAllHwStsDelete(AtSdhChannelSubChannelGet(self, 0));
    }

static eAtRet Concate(AtSdhChannel self, AtSdhChannel *slaveList, uint8 numSlaves)
    {
    eAtRet ret;
    uint8 i;
    AtSdhChannel vc = AtSdhChannelSubChannelGet(self, 0);

    ret = AtSdhChannelConcateCheck(self, slaveList, numSlaves);
    if (ret != cAtOk)
        return ret;

    ret = m_AtSdhChannelMethods->Concate(self, slaveList, numSlaves);
    if (ret != cAtOk)
        return ret;

    /* Clear slave channels configuration */
    for (i = 0; i < numSlaves; i++)
        {
        AtSdhChannel slave = slaveList[i];
        ret |= AtChannelTimingSet((AtChannel)AtSdhChannelSubChannelGet(slave, 0), cAtTimingModeSys, NULL);
        ret |= ThaOcnStsPohInsertEnable(slave, AtSdhChannelSts1Get(slave), cAtFalse);
        }

    AllHwStsReset(self);
    SdhChannelTypeSet(vc, VcType(self));

    ret |= ThaOcnStsConcat(self, AtSdhChannelSts1Get(self), NumSts(self));
    ret |= mMethodsGet(vc)->MapTypeSet(vc, CxMapType(vc));
    return ret;
    }

static eAtRet Deconcate(AtSdhChannel self)
    {
    eAtRet ret = cAtOk;
    uint8 i;
    uint32 numSlaves = AtSdhChannelNumSlaveChannels(self);
    AtSdhChannel vc = AtSdhChannelSubChannelGet(self, 0);

    if (AtSdhChannelServiceIsRunning(self))
        return cAtErrorChannelBusy;

    /* Restore slave channels configuration */
    for (i = 0; i < numSlaves; i++)
        {
        AtSdhChannel slave = AtSdhChannelSlaveChannelAtIndex(self, i);

        ret |= ThaOcnStsPohInsertEnable(slave, (uint8)AtSdhChannelSts1Get(slave), cAtTrue);
        ret |= ThaOcnStsConcat(slave, AtSdhChannelSts1Get(slave), AtSdhChannelNumSts(slave));
        if (ret != cAtOk)
            return ret;
        }

    /* Call super implementation */
    ret = m_AtSdhChannelMethods->Deconcate(self);
    if (ret != cAtOk)
        return ret;

    AllHwStsReset(self);
    SdhChannelTypeSet(vc, VcType(self));

    return mMethodsGet(vc)->MapTypeSet(vc, CxMapType(vc));
    }

static eAtRet Stm0SubChannelCreate(AtChannel self)
	{
	AtSdhChannel channel = (AtSdhChannel)self;
    AtSdhLine line = AtSdhChannelLineObjectGet(channel);

    /* Create sub channels */
    AtSdhChannelAllSubChannelsHardwareCleanup(channel);
    SdhChannelSubChannelsDelete((AtSdhChannel)self);
    return SdhChannelSubChannelsCreate(channel, (uint8)AtChannelIdGet((AtChannel)line), 1, cAtSdhChannelTypeVc3);
	}

static eAtRet Init(AtChannel self)
    {
    AtSdhLine line = AtSdhChannelLineObjectGet((AtSdhChannel)self);
    eAtSdhLineRate rate = AtSdhLineRateGet(line);
    eAtRet ret = ConcateReset((AtSdhChannel)self);
    if (ret != cAtOk)
        return ret;

    if (rate == cAtSdhLineRateStm0)
        Stm0SubChannelCreate(self);

    ret = m_AtChannelMethods->Init(self);
    if (ret == cAtOk)
        {
        if (AtSdhPathHasPohProcessor((AtSdhPath)self))
        	AllAlarmsUnForce(self);
        }

    return ret;
    }

static eAtRet Debug(AtChannel self)
    {
    m_AtChannelMethods->Debug(self);
    return ThaSdhChannelCommonRegisterOffsetsDisplay((AtSdhChannel)self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaSdhAu object = (ThaSdhAu)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(enabled);
    mEncodeUInt(ssBit);
    mEncodeNone(allHwSts);
    mEncodeNone(attController);
    }

static uint8 WarmRestoreAuTxSsGet(ThaSdhAu self, eBool isChannelized)
    {
    if (isChannelized)
        {
        uint32 address = cThaRegOcnTxPohInsPerChnCtrl + DefaultOffset((AtChannel)self);
        uint32 regVal  = mChannelHwRead(self, address, cThaModuleOcn);

        return (uint8)mRegField(regVal, cThaTxPg2StsSSInsPat);
        }
    else
        {
        uint32 offset  = OcnTxPGPerChnCtrlDefaultOffset((AtSdhAu)self);
        uint32 address = cThaRegOcnTxPGPerChnCtrl + offset;
        uint32 regVal  = mChannelHwRead(self, address, cThaModuleOcn);

        return (uint8)mRegField(regVal, cThaTxPgStsVtTuSSInsPat);
        }

    return 0x0;
    }

static eAtRet WarmRestore(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->WarmRestore(self);

    if (ret == cAtOk)
        mThis(self)->ssBit = WarmRestoreAuTxSsGet(mThis(self), IsChannellized((AtSdhPath)self));

    ThaSdhChannelRestoreAllHwSts((AtSdhChannel)self);

    return ret;
    }

static void OverrideAtObject(AtSdhAu self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtSdhAu self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, IsEnabled);
        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, TxAlarmForce);
        mMethodOverride(m_AtChannelOverride, TxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcedAlarmGet);
        mMethodOverride(m_AtChannelOverride, TxForcibleAlarmsGet);
        mMethodOverride(m_AtChannelOverride, Debug);
        mMethodOverride(m_AtChannelOverride, WarmRestore);
        mMethodOverride(m_AtChannelOverride, AttController);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtSdhChannel(AtSdhAu self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(m_AtSdhChannelOverride));

        mMethodOverride(m_AtSdhChannelOverride, Concate);
        mMethodOverride(m_AtSdhChannelOverride, Deconcate);
        mMethodOverride(m_AtSdhChannelOverride, NumSts);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void OverrideAtSdhPath(AtSdhAu self)
    {
    AtSdhPath path = (AtSdhPath)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhPathOverride, mMethodsGet(path), sizeof(m_AtSdhPathOverride));
        mMethodOverride(m_AtSdhPathOverride, TxSsSet);
        mMethodOverride(m_AtSdhPathOverride, TxSsGet);
        mMethodOverride(m_AtSdhPathOverride, ExpectedSsSet);
        mMethodOverride(m_AtSdhPathOverride, ExpectedSsGet);
        mMethodOverride(m_AtSdhPathOverride, SsCompareEnable);
        mMethodOverride(m_AtSdhPathOverride, SsCompareIsEnabled);
        }

    mMethodsSet(path, &m_AtSdhPathOverride);
    }

static void Override(AtSdhAu self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtSdhChannel(self);
    OverrideAtSdhPath(self);
    }

AtSdhAu ThaSdhAuObjectInit(AtSdhAu self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaSdhAu));

    /* Super constructor */
    if (AtSdhAuObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhAu ThaSdhAuNew(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhAu newAu = mMethodsGet(osal)->MemAlloc(osal, sizeof(tThaSdhAu));
    if (newAu == NULL)
        return NULL;

    /* Construct it */
    return ThaSdhAuObjectInit(newAu, channelId, channelType, module);
    }

eAtRet ThaSdhAuTxSsSet(AtSdhPath self, uint8 value, eBool isChannelized)
    {
	if (isChannelized)
		{
		uint32 address = cThaRegOcnTxPohInsPerChnCtrl + DefaultOffset((AtChannel)self);
		uint32 regVal  = mChannelHwRead(self, address, cThaModuleOcn);
		mFieldIns(&regVal, cThaTxPg2StsSSInsPatMask, cThaTxPg2StsSSInsPatShift, value);
		mChannelHwWrite(self, address, regVal, cThaModuleOcn);
		}
	else
		{
		uint32 offset  = OcnTxPGPerChnCtrlDefaultOffset((AtSdhAu)self);
		uint32 address = cThaRegOcnTxPGPerChnCtrl + offset;
		uint32 regVal  = mChannelHwRead(self, address, cThaModuleOcn);
		mFieldIns(&regVal, cThaTxPg2StsSSInsEnMask, cThaTxPg2StsSSInsEnShift, 1);
		mFieldIns(&regVal, cThaTxPgStsVtTuSSInsPatMask, cThaTxPgStsVtTuSSInsPatShift, value);
		mChannelHwWrite(self, address, regVal, cThaModuleOcn);
		}

	return cAtOk;
    }


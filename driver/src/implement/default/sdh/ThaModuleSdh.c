/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : ThaModuleSdh.c
 *
 * Created Date: Sep 7, 2012
 *
 * Description : Thalassa SDH module default implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../util/coder/AtCoderUtil.h"
#include "../../../generic/prbs/AtModulePrbsInternal.h"
#include "../physical/ThaSerdesController.h"
#include "../man/ThaIpCoreInternal.h"
#include "../prbs/ThaPrbsEngineSerdes.h"
#include "ThaModuleSdhInternal.h"
#include "ThaModuleSdhReg.h"
#include "ThaSdhVcInternal.h"
#include "ThaSdhLine.h"
#include "ThaModuleSdhAsyncInit.h"
#include "../../../../include/att/AtAttSdhManager.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaModuleSdh)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Declare new internal method variable */
static tThaModuleSdhMethods m_methods;

/* Override */
static tAtModuleMethods m_AtModuleOverride;
static tAtObjectMethods m_AtObjectOverride;
static tAtModuleSdhMethods m_AtModuleSdhOverride;

/* Save super implementations */
static const tAtModuleMethods *m_AtModuleMethods = NULL;
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ForcePointerPeriodicProcess(AtModule self, uint32 periodInMs)
    {
    AtAttSdhManager mngr = AtModuleSdhAttManager((AtModuleSdh)self);
    if (mngr)
        AtAttSdhManagerForcePointerPeriodicProcess(mngr, periodInMs);
    return 0;
    }
    
static uint8 NumParts(AtModuleSdh self)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    return ThaDeviceNumPartsOfModule(device, cThaModuleOcn);
    }

static uint8 LocalLineId(ThaModuleSdh self, uint32 lineId)
    {
    return (uint8)(lineId  % ThaModuleSdhNumLinesPerPart((AtModuleSdh)self));
    }

static uint32 PartOffset(AtModuleSdh self, uint8 partId)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    return ThaDeviceModulePartOffset(device, cThaModuleOcn, partId);
    }

static eAtRet ClockOutWhenLosEnable(AtModuleSdh self, uint8 portId, eBool enable)
    {
    uint32 regVal, regAddr;
    uint32 localPort = mMethodsGet(mThis(self))->LocalLineId(mThis(self), portId);

    regAddr = cThaRegClockOutputCtrl + PartOffset(self, mMethodsGet(mThis(self))->PartOfLine(mThis(self), portId));
    regVal = mModuleHwRead(self, regAddr);
    mFieldIns(&regVal,
              cThaRegClockOutputCtrlRefClkInModMask(localPort),
              cThaRegClockOutputCtrlRefClkInModShift(localPort),
              enable ? 1 : 0);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static void LedEngineInit(AtModule self)
    {
    uint8 part_i;
    AtModuleSdh sdhModule = (AtModuleSdh)self;

    if (!mMethodsGet(mThis(self))->HasLedEngine(mThis(self)))
        return;

    for (part_i = 0; part_i < NumParts(sdhModule); part_i++)
        {
        mModuleHwWrite(self, cThaRegOcnLedCtrl + PartOffset(sdhModule, part_i), cThaRegOcnLedCtrlRstValue);
        }
    }

static eAtRet PartDefaultCounterModeSet(AtModule self, uint32 counterType, uint8 partId)
    {
    uint32 regAddr = cThaRegOcnCounterControl + PartOffset((AtModuleSdh)self, partId);
    uint32 regVal = mModuleHwRead(self, regAddr);
    const uint8 cRollOver = 0;

    if (counterType == cAtSdhLineCounterTypeB1)
        mRegFieldSet(regVal, cThaRegOcnB1ErrCntMd, cRollOver);

    else if (counterType == cAtSdhLineCounterTypeB2)
        mRegFieldSet(regVal, cThaRegOcnB2ErrCntMd, cRollOver);

    else if (counterType == cAtSdhLineCounterTypeRei)
        mRegFieldSet(regVal, cThaRegOcnReiErrCntMd, cRollOver);

    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static void PartAlarmDownstreamEnable(ThaModuleSdh self, eBool enable, uint8 partId)
    {
    uint32 regAddr = cThaRegOcnPiMastCtrl + PartOffset((AtModuleSdh)self, partId);
    uint32 regVal;
    uint8 hwEnable;

    regVal = mModuleHwRead(self, regAddr);

    hwEnable = enable ? 0xf : 0;
    mFieldIns(&regVal, cThaOcnPiVtAisDownStrEnMask, cThaOcnPiVtAisDownStrEnShift, hwEnable);
    mFieldIns(&regVal, cThaOcnPiStsAisDownStrEnMask, cThaOcnPiStsAisDownStrEnShift, hwEnable);
    mFieldIns(&regVal, cThaOcnPiStsDefGenAisDownStrEnMask, cThaOcnPiStsDefGenAisDownStrEnShift, hwEnable);
    mModuleHwWrite(self, regAddr, regVal);
    }

static void AlarmDownstreamEnable(ThaModuleSdh self, eBool enable)
    {
    uint8 part_i;
    AtModuleSdh sdhModule = (AtModuleSdh)self;

    for (part_i = 0; part_i < NumParts(sdhModule); part_i++)
        PartAlarmDownstreamEnable(self, enable, part_i);
    }

static eAtRet PartCounterEngineInit(AtModule self, uint8 partId)
    {
    uint32 regVal;
    uint32 regAddr;
    eAtRet ret = cAtOk;

    /* Counter mode is roll-over as default */
    ret |= PartDefaultCounterModeSet(self, cAtSdhLineCounterTypeB1,  partId);
    ret |= PartDefaultCounterModeSet(self, cAtSdhLineCounterTypeB2,  partId);
    ret |= PartDefaultCounterModeSet(self, cAtSdhLineCounterTypeRei, partId);

    /* When alarm happen, do not count error anymore */
    regAddr = cThaRegOcnTohMonAffCtrl + PartOffset((AtModuleSdh)self, partId);
    regVal = mModuleHwRead(self, regAddr);
    mFieldIns(&regVal, cThaTohMonAisAffErrCntEnBMask, cThaTohMonAisAffErrCntEnBShift, 0);
    mModuleHwWrite(self, regAddr, regVal);

    return ret;
    }

static eAtRet CounterEngineInit(AtModule self)
    {
    uint8 part_i;
    eAtRet ret = cAtOk;

    if (!mMethodsGet(mThis(self))->HasCounters(mThis(self)))
        return cAtOk;

    for (part_i = 0; part_i < NumParts((AtModuleSdh)self); part_i++)
        ret |= PartCounterEngineInit(self, part_i);

    return ret;
    }

static void ApsInit(AtModuleSdh self)
    {
    uint8 part_i;
    uint32 regAddr, regVal;

    for (part_i = 0; part_i < NumParts(self); part_i++)
        {
        regAddr = cThaRegOcnGlbCtrl + PartOffset(self, part_i);
        regVal  = mModuleHwRead(self, regAddr);
        mRegFieldSet(regVal, cThaOcnSlaverPart, (part_i == 0) ? 0 : 1);
        mModuleHwWrite(self, regAddr, regVal);
        }
    }

static eAtSdhLineRate LineDefaultRate(ThaModuleSdh self, AtSdhLine line)
    {
    AtUnused(self);
    return ThaSdhLineLowRate((ThaSdhLine)line);
    }

static void LineDefaultSet(ThaModuleSdh self)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)self;
    uint8 dbLineId, numLines = AtModuleSdhMaxLinesGet(sdhModule);

    for (dbLineId = 0; dbLineId < numLines; dbLineId++)
        {
        if (AtModuleSdhLineCanBeUsed(sdhModule, dbLineId))
            ThaSdhModuleLineDefaultSet(self, dbLineId);
        }
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = cAtOk;

    ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    mMethodsGet(mThis(self))->LineDefaultSet(mThis(self));

    ThaModuleSdhPostInit(self);

    return cAtOk;
    }

static eAtRet AsyncLineDefaultSet(ThaModuleSdh self)
    {
    return ThaSdhModuleAsyncLineDefaultMain(self);
    }

static eAtRet AsyncInit(AtModule self)
    {
    return ThaModuleSdhAsyncInitMain(self);
    }

static void Delete(AtObject self)
    {
    /* Delete private data */

    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static uint8 MaxLinesGet(AtModuleSdh self)
    {
	AtUnused(self);
    return 4;
    }

static uint32 MaxLineRate(AtModuleSdh self)
    {
	AtUnused(self);
    return cAtSdhLineRateStm4;
    }

static uint8 ChannelVersion(ThaModuleSdh self, eAtSdhChannelType channelType)
    {
    if (channelType == cAtSdhChannelTypeLine)
        return AtDeviceModuleVersion(AtModuleDeviceGet((AtModule)self), cThaModuleOcn);
    return 0;
    }

static AtSdhChannel ChannelCreate(AtModuleSdh self, uint8 lineId, AtSdhChannel parent, uint8 channelType, uint8 channelId)
    {
    uint8 channelVersion = ChannelVersion(mThis(self), channelType);
    AtUnused(lineId);
    return mMethodsGet(mThis(self))->ChannelCreateWithVersion(mThis(self), lineId, parent, channelType, channelId, channelVersion);
    }

static void AlarmLatch(ThaSdhLine self, uint32 interruptChange, uint32 currentStatus)
    {
    uint32 latchedAlarms = ThaSdhLineLatchedAlarmsGet(self);
    uint32 clearedAlarms = interruptChange & (~currentStatus);
    uint32 raisedAlarms  = interruptChange & currentStatus;
    latchedAlarms |= raisedAlarms;
    latchedAlarms &= (~clearedAlarms);
    ThaSdhLineLatchedAlarmsSet(self, latchedAlarms);
    }

static uint32 FlatLineIdOnParts(AtModuleSdh sdhModule, uint8 partId, uint8 localIdInPart)
    {
    return (uint32)((partId * ThaModuleSdhNumLinesPerPart(sdhModule)) + localIdInPart);
    }

static void RxLinesInterruptProcessOnPart(AtModule self, AtHal hal, uint8 partId)
    {
    uint8 localLineId;
    AtModuleSdh sdhModule = (AtModuleSdh)self;
    uint32 perLineStatus = AtHalNoLockRead(hal, cThaRegOcnRxLineperLineIntrOrStat + PartOffset(sdhModule, partId));

    /* Find which line cause interrupt */
    for (localLineId = 0; localLineId < ThaModuleSdhNumLinesPerPart(sdhModule); localLineId++)
        {
        ThaSdhLine sdhLine = (ThaSdhLine)AtModuleSdhLineGet((AtModuleSdh)sdhModule, (uint8)FlatLineIdOnParts(sdhModule, partId, localLineId));
        if (perLineStatus & cThaOcnRxLLineIntrLineIdMask(localLineId))
            {
            uint32 interruptChange, currentStatus;
            uint32 interruptMask = AtChannelInterruptMaskGet((AtChannel)sdhLine);

            /* Get interrupt change and current status */
            AtSdhChannelInterruptAndAlarmGet((AtSdhChannel)sdhLine, &interruptChange, &currentStatus, cAtTrue);
            interruptChange = interruptChange & interruptMask;

            /* Update cached alarm to debug lost events */
            AlarmLatch(sdhLine, interruptChange, currentStatus);

            /* Notify all of listeners */
            AtChannelAllAlarmListenersCall((AtChannel)sdhLine, interruptChange, currentStatus);
            }
        }
    }

static void RxLinesInterruptProcess(AtModule self, AtHal hal)
    {
    uint8 part_i;

    for (part_i = 0; part_i < NumParts((AtModuleSdh)self); part_i++)
        RxLinesInterruptProcessOnPart(self, hal, part_i);
    }

/* FIXME: if this module has two parts, the following needs to be considered */
static uint32 OcnRxStsVCIntrStatDeftOffset(ThaModuleSdh self, uint8 slice)
    {
	AtUnused(self);
    return (uint32)(slice * 8192);
    }

/* FIXME: if this module has two parts, the following needs to be considered */
static uint32 OcnRxVtTuIntrStatDeftOffset(ThaModuleSdh self, uint8 slice, uint8 sts)
    {
	AtUnused(self);
    return (uint32)(slice * 8192 + sts);
    }

static eBool AlwaysDisableRdiBackwardWhenTimNotDownstreamAis(ThaModuleSdh self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static eBool ShouldUpdateTimRdiBackwardWhenAisDownstreamChange(ThaModuleSdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool ShouldUpdatePlmMonitorWhenExpectedPslChange(ThaModuleSdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void RxStsInterruptProcess(AtModule self, AtHal hal)
    {
    uint8 sliceId, stsId;
    uint32 perStsVcStatus;
    uint32 perSliceStatus;
    uint32 address;
    ThaModuleSdh moduleSdh = mThis(self);
    AtSdhPath sdhChannel;

    /* Get which slice cause interrupt */
    perSliceStatus = AtHalNoLockRead(hal, cThaRegOcnRxStsVCPerSliceIntrOrStat);
    for (sliceId = 0; sliceId < cThaRegOcnNumberSlice; sliceId++)
        {
        if (!(perSliceStatus & cThaRegOcnRxStsVcPerSliceIntrMask(sliceId)))
            continue;

        /* Get which STS in slice cause interrupt */
        address = cThaRegOcnRxStsVCPerStsVCIntrOrStat + (mMethodsGet(moduleSdh)->OcnRxStsVCIntrStatDeftOffset(moduleSdh, sliceId));
        perStsVcStatus = AtHalNoLockRead(hal, address);
        for (stsId = 0; stsId < cThaOcnNumStsInOc12; stsId++)
            {
            if (!(perStsVcStatus & cThaOcnRxStsStsIntrIdMask(stsId)))
                continue;

            /* Get SDH channel from STS HW ID */
            sdhChannel = ThaModuleSdhAuPathFromHwIdGet(moduleSdh, cAtModuleSdh, sliceId, stsId);

            /* Call all listeners register on this channel */
            if (sdhChannel)
                {
                AtChannel channel = (AtChannel)sdhChannel;
                AtChannelAllAlarmListenersCall((AtChannel)channel,
                                               AtChannelDefectInterruptClear(channel),
                                               AtChannelDefectGet(channel));
                }
            }
        }
    }

static void RxVtInterruptProcess(AtModule self, AtHal hal)
    {
    uint8 sliceId, stsId, vtId;
    uint32 perStsVcStatus;
    uint32 perStsVcEn;
    uint32 perSliceStatus;
    uint32 perVtStatus;
    uint32 address;
    ThaModuleSdh moduleSdh = mThis(self);
    AtSdhPath sdhChannel;

    /* Get which slice cause interrupt */
    perSliceStatus = AtHalNoLockRead(hal, cThaRegOcnRxVtTUPerSliceIntrOrStat);
    for (sliceId = 0; sliceId < cThaRegOcnNumberSlice; sliceId++)
        {
        if (!(perSliceStatus & cThaRegOcnRxVtTUPerSliceIntrMask(sliceId)))
            continue;

        /* Get which STS in slice cause interrupt */
        address = cThaRegOcnRxVtTUPerStsVCIntrEnCtrl + (mMethodsGet(moduleSdh)->OcnRxStsVCIntrStatDeftOffset(moduleSdh, sliceId));
        perStsVcEn = AtHalNoLockRead(hal, address);

        address = cThaRegOcnRxVtTUPerStsVCIntrOrStat + (mMethodsGet(moduleSdh)->OcnRxStsVCIntrStatDeftOffset(moduleSdh, sliceId));
        perStsVcStatus = AtHalNoLockRead(hal, address);

        perStsVcStatus &= perStsVcEn;
        for (stsId = 0; stsId < cThaOcnNumStsInOc12; stsId++)
            {
            if (!(perStsVcStatus & cThaRegOcnRxVtTuPerStsStsIdMask(stsId)))
                continue;

            /* Get which VT cause interrupt */
            address = cThaRegOcnRxVtTUPerVtTUIntrOrStat + (mMethodsGet(moduleSdh)->OcnRxVtTuIntrStatDeftOffset(moduleSdh, sliceId, stsId));
            perVtStatus = AtHalNoLockRead(hal, address);
            for (vtId = 0; vtId < cThaOcnNumVtInSts; vtId++)
                {
                if (!(perVtStatus & cThaRegOcnRxVtTUPerVtTUIntrVtIdMask(vtId)))
                    continue;

                /* Get SDH channel */
                sdhChannel = ThaModuleSdhTuPathFromHwIdGet(moduleSdh, cAtModuleSdh, sliceId, stsId, (vtId / 4), (vtId % 4));
                if (sdhChannel)
                    {
                    AtChannel channel = (AtChannel)sdhChannel;
                    AtChannelAllAlarmListenersCall(channel,
                                                   AtChannelDefectInterruptClear(channel),
                                                   AtChannelDefectGet(channel));
                    }
                }
            }
        }
    }

static eBool HasLineInterrupt(ThaModuleSdh self, uint32 glbIntr, AtIpCore ipCore)
    {
	AtUnused(ipCore);
	AtUnused(self);
    return (glbIntr & cThaRegChipIntrStaRxLineOcnIntAlarm) ? cAtTrue : cAtFalse;
    }

static eBool HasStsInterrupt(ThaModuleSdh self, uint32 glbIntr, AtIpCore ipCore)
    {
	AtUnused(ipCore);
	AtUnused(self);
    return (glbIntr & cThaRegChipIntrStaRxSTSOcnIntAlarm) ? cAtTrue : cAtFalse;
    }

static eBool HasVtInterrupt(ThaModuleSdh self, uint32 glbIntr, AtIpCore ipCore)
    {
	AtUnused(ipCore);
	AtUnused(self);
    return (glbIntr & cThaRegChipIntrStaRxVTOcnIntAlarm) ? cAtTrue : cAtFalse;
    }

static eBool BerHardwareInterruptIsSupported(ThaModuleSdh self)
    {
	AtUnused(self);
    /* Not all of products can support this feature. Let concrete product
     * determine */
    return cAtFalse;
    }

static void LineInterruptProcess(ThaModuleSdh self, uint32 glbLineIntr, AtHal hal)
    {
    AtUnused(glbLineIntr);
    RxLinesInterruptProcess((AtModule)self, hal);
    }

static void StsInterruptProcess(ThaModuleSdh self, uint32 glbStsIntr, AtHal hal)
    {
    AtUnused(glbStsIntr);
    RxStsInterruptProcess((AtModule)self, hal);
    }

static void VtInterruptProcess(ThaModuleSdh self, uint32 glbVtIntr, AtHal hal)
    {
    AtUnused(glbVtIntr);
    RxVtInterruptProcess((AtModule)self, hal);
    }

static void InterruptProcess(AtModule self, uint32 glbIntr, AtIpCore ipCore)
    {
    AtHal hal = AtIpCoreHalGet(ipCore);
    ThaModuleSdh sdhModule = (ThaModuleSdh)self;

    if (hal == NULL)
        return;

    if (mMethodsGet(sdhModule)->HasLineInterrupt(sdhModule, glbIntr, ipCore))
        {
        uint32 lineIntr = mMethodsGet(sdhModule)->LineInterruptStatusGet(sdhModule, glbIntr, hal);
        mMethodsGet(sdhModule)->LineInterruptProcess(sdhModule, lineIntr, hal);
        }

    if (mMethodsGet(sdhModule)->HasStsInterrupt(sdhModule, glbIntr, ipCore))
        {
        uint32 stsIntr = mMethodsGet(sdhModule)->StsInterruptStatusGet(sdhModule, glbIntr, hal);
        mMethodsGet(sdhModule)->StsInterruptProcess(sdhModule, stsIntr, hal);
        }

    if (mMethodsGet(sdhModule)->HasVtInterrupt(sdhModule, glbIntr, ipCore))
        {
        uint32 vtIntr = mMethodsGet(sdhModule)->VtInterruptStatusGet(sdhModule, glbIntr, hal);
        mMethodsGet(sdhModule)->VtInterruptProcess(sdhModule, vtIntr, hal);
        }
    }

static void InterruptHwEnable(AtIpCore ipCore, eBool enable)
    {
    ThaIpCoreSdhHwInterruptEnable((ThaIpCore)ipCore, enable);
    }

static eAtRet InterruptEnable(AtModule self, eBool enable)
    {
    uint8 i, numCore;
    AtIpCore ipCore;
    AtDevice device = AtModuleDeviceGet(self);

    numCore = AtDeviceNumIpCoresGet(device);

    for (i = 0; i < numCore; i++)
        {
        ipCore = AtDeviceIpCoreGet(device, i);

        /* If a product supports interrupt pin enable register, just simple
         * enable module interrupt on IPcore. Otherwise, we need to check if
         * IPcore interrupt was enabled before enable module interrupt. */
        if (ThaIpCoreCanEnableHwInterruptPin((ThaIpCore)ipCore) || AtIpCoreInterruptIsEnabled(ipCore))
            InterruptHwEnable(ipCore, enable);

        /* Store to DB */
        mThis(self)->interruptEnabled = enable;
        }

    return cAtOk;
    }

static eBool InterruptIsEnabled(AtModule self)
    {
    return mThis(self)->interruptEnabled;
    }

static eAtRet LoopbackSet(AtModule self, uint8 loopbackMode)
    {
    uint8 part_i;

    if (!AtModuleDebugLoopbackIsSupported(self, loopbackMode))
        return cAtErrorModeNotSupport;

    for (part_i = 0; part_i < NumParts((AtModuleSdh)self); part_i++)
        {
        uint32 regAddr = cThaRegOcnGlbCtrl + PartOffset((AtModuleSdh)self, part_i);
        uint32 regVal  = mModuleHwRead(self, regAddr);
        mRegFieldSet(regVal, cThaOcnSysBusLoopOutEn, (loopbackMode == cAtLoopbackModeRemote) ? 1 : 0);
        mModuleHwWrite(self, regAddr, regVal);
        }

    return cAtOk;
    }

static uint8 LoopbackGet(AtModule self)
    {
    uint32 regAddr = cThaRegOcnGlbCtrl + PartOffset((AtModuleSdh)self, 0);
    uint32 regVal  = mModuleHwRead(self, regAddr);
    return (regVal & cThaOcnSysBusLoopOutEnMask) ? cAtLoopbackModeRemote : cAtLoopbackModeRelease;
    }

static eBool LoopbackIsSupported(AtModule self, uint8 loopbackMode)
    {
    if (loopbackMode == cAtLoopbackModeRemote)
        return cAtTrue;
    return m_AtModuleMethods->LoopbackIsSupported(self, loopbackMode);
    }

/* Enable interrupt on one core */
static void InterruptOnIpCoreEnable(AtModule self, eBool enable, AtIpCore ipCore)
    {
    if(InterruptIsEnabled(self))
        InterruptHwEnable(ipCore, enable);
    }

static AtSerdesController SerdesControllerCreate(ThaModuleSdh self, AtSdhLine line, uint32 serdesId)
    {
    if (ThaDeviceIsEp((ThaDevice)AtModuleDeviceGet((AtModule)self)))
        return ThaSdhLineSerdesControllerCyclone4New(line, serdesId);

    return ThaSdhLineSerdesControllerNew(line, serdesId);
    }

static eBool LineSerdesPrbsIsSupported(ThaModuleSdh self, AtSdhLine line, AtSerdesController serdes)
    {
    AtUnused(line);
    AtUnused(self);
    AtUnused(serdes);
    /* Let concrete product determine */
    return cAtFalse;
    }

static AtPrbsEngine LineSerdesPrbsEngineCreate(ThaModuleSdh self, AtSdhLine line, AtSerdesController serdes)
    {
    AtUnused(self);
    AtUnused(line);
    return ThaPrbsEngineSerdesSdhLineNew(serdes);
    }

static uint8 SerdesCoreIdOfLine(ThaModuleSdh self, AtSdhLine line)
    {
	AtUnused(self);
    return (uint8)AtChannelIdGet((AtChannel)line);
    }

static uint8 SerdesIdOfLine(ThaModuleSdh self, AtSdhLine line)
    {
    return ThaModuleSdhSerdesCoreIdOfLine(self, line);
    }

static eBool PayloadUneqIsApplicable(ThaModuleSdh self)
    {
	AtUnused(self);
    /* Only CEP products support this defect. So they should override this
     * method. */
    return cAtFalse;
    }

static eBool ErdiIsEnabledByDefault(ThaModuleSdh self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static eBool VcAisIsEnabledByDefault(ThaModuleSdh self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static uint8 DefaultVc1xSubMapping(ThaModuleSdh self)
    {
	AtUnused(self);
    return cAtSdhVcMapTypeVc1xMapDe1;
    }

static eBool BlockErrorCountersSupported(ThaModuleSdh self)
    {
	AtUnused(self);
    /* Almost products do not support these counter types. Let concrete class
     * determine */
    return cAtFalse;
    }

static eAtSerdesTimingMode DefaultSerdesTimingModeForLine(ThaModuleSdh self, AtSdhLine line)
    {
	AtUnused(self);
    if (AtSdhLineRateGet(line) == cAtSdhLineRateStm1)
        return cAtSerdesTimingModeLockToRef;

    return cAtSerdesTimingModeAuto;
    }

static void OffsetDisplay(const char *title, uint32 value)
    {
    AtPrintc(cSevNormal, "%s: %u(0x%x)\r\n", title, value, value);
    }

static AtSdhChannel ChannelCreateWithVersion(ThaModuleSdh self, uint8 lineId, AtSdhChannel parent, uint8 channelType, uint8 channelId, uint8 version)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)self;

    AtUnused(lineId);

    /* Create line */
    if (channelType == cAtSdhChannelTypeLine)
        return (AtSdhChannel)ThaSdhLineNew(channelId, sdhModule, version);

    /* Create AUG */
    if ((channelType == cAtSdhChannelTypeAug1) ||
        (channelType == cAtSdhChannelTypeAug4) ||
        (channelType == cAtSdhChannelTypeAug16) ||
        (channelType == cAtSdhChannelTypeAug64))
        return (AtSdhChannel)ThaSdhAugNew(channelId, channelType, sdhModule);

    /* Create AU */
    if ((channelType == cAtSdhChannelTypeAu4_64c) ||
        (channelType == cAtSdhChannelTypeAu4_16c) ||
        (channelType == cAtSdhChannelTypeAu4_4c) ||
        (channelType == cAtSdhChannelTypeAu4) ||
        (channelType == cAtSdhChannelTypeAu3))
        return (AtSdhChannel)ThaSdhAuNew(channelId, channelType, sdhModule);

    /* Create TUG */
    if ((channelType == cAtSdhChannelTypeTug3) ||
        (channelType == cAtSdhChannelTypeTug2))
        return (AtSdhChannel)ThaSdhTugNew(channelId, channelType, sdhModule);

    /* Create TU */
    if ((channelType == cAtSdhChannelTypeTu3) ||
        (channelType == cAtSdhChannelTypeTu12) ||
        (channelType == cAtSdhChannelTypeTu11))
        return (AtSdhChannel)ThaSdhTuNew(channelId, channelType, sdhModule);

    /* Create AU-VC */
    if ((channelType == cAtSdhChannelTypeVc4_64c) ||
        (channelType == cAtSdhChannelTypeVc4_16c) ||
        (channelType == cAtSdhChannelTypeVc4_4c) ||
        (channelType == cAtSdhChannelTypeVc4))
        return (AtSdhChannel)ThaSdhAuVcNew(channelId, channelType, sdhModule);

    /* Create VC-1x */
    if ((channelType == cAtSdhChannelTypeVc12) || (channelType == cAtSdhChannelTypeVc11))
        return (AtSdhChannel)ThaSdhVc1xNew(channelId, channelType, sdhModule);

    /* For VC-3, hardware needs to know whether it is mapped to TU-3 or AU-3. So,
     * parent mapping needs to be checked */
    if (channelType == cAtSdhChannelTypeVc3)
        {
        if (AtSdhChannelTypeGet(parent) == cAtSdhChannelTypeTu3)
            return (AtSdhChannel)ThaSdhTu3VcNew(channelId, channelType, sdhModule);
        else
            return (AtSdhChannel)ThaSdhAuVcNew(channelId, channelType, sdhModule);
        }

    /* Cannot create a channel with invalid type */
    return NULL;
    }

static eBool CanDisablePwPacketsToPsn(ThaModuleSdh self)
    {
	AtUnused(self);
    /* Not all of products require this, so let concrete products determine */
    return cAtFalse;
    }

static uint32 StartVersionSupport24BitsBlockErrorCounter(ThaModuleSdh self)
    {
	AtUnused(self);
    /* Let products determine whether this can be supported */
    return cBit31_0;
    }

static AtPrbsEngine VcPrbsEngineCreate(ThaModuleSdh self, AtSdhVc vc)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    AtModulePrbs prbsModule = (AtModulePrbs)AtDeviceModuleGet(device, cAtModulePrbs);

    if (AtModulePrbsAllChannelsSupported(prbsModule))
        return AtModulePrbsSdhVcPrbsEngineCreate(prbsModule, 0, (AtSdhChannel)vc);

    return NULL;
    }

void ThaModuleSdhStsMappingDisplay(AtSdhChannel self)
    {
    uint8 slice, hwSts, sts_i;

    AtPrintc(cSevInfo, "* OCN STS mapping of channel '%s':\r\n", AtObjectToString((AtObject)self));
    AtPrintc(cSevInfo,   "* SW-STS    Slice-oc48    HW-STS(0-47)\r\n");

    for (sts_i = 0; sts_i < AtSdhChannelNumSts(self); sts_i++)
        {
        uint8 stsId =  (uint8)(AtSdhChannelSts1Get(self) + sts_i);
        ThaSdhChannelHwStsGet(self, cThaModuleOcn, stsId, &slice, &hwSts);
        AtPrintc(cSevNormal, "%6u    %5u         %6u\r\n", stsId, slice, hwSts);
        }

    AtPrintc(cSevNormal, "\r\n");
    }

static eAtRet CommonRegisterOffsetsDisplay(ThaModuleSdh self, AtSdhChannel channel)
    {
    uint8 slice, stsId, vtgId, vtId;

    AtUnused(self);

    ThaSdhChannel2HwMasterStsId(channel, cThaModuleOcn, &slice, &stsId);
    vtgId = AtSdhChannelTug2Get(channel);
    vtId  = AtSdhChannelTu1xGet(channel);

    AtPrintc(cSevNormal, "\r\n");
    ThaModuleSdhStsMappingDisplay(channel);
    AtPrintc(cSevInfo,   "* Hardware ID: "); AtPrintc(cSevNormal, "slice = %d, sts = %d (local in slice), vtg = %d, vt = %d\r\n", slice, stsId, vtgId, vtId);
    AtPrintc(cSevInfo,   "* Common register offsets:\r\n");
    OffsetDisplay(       "  - (8192 * slice) + (stsId)                                     ", (8192UL * slice) + stsId);
    OffsetDisplay(       "  - (8192 * slice) + (32 * stsId) + (4 * vtgId) + vtId           ", (8192UL * slice) + (32UL * stsId) + (4UL * vtgId) + vtId);
    OffsetDisplay(       "  - (8192 * slice) + (672 * stsId)                               ", (8192UL * slice) + (672UL * stsId));
    OffsetDisplay(       "  - (8192 * slice) + (672 * stsId) + (96 * vtgId) + (24 * vtId)  ", (8192UL * slice) + (672UL * stsId) + (96UL * vtgId) + (24UL * vtId));
    OffsetDisplay(       "  - (8192 * slice) + (672 * stsId) + (96 * vtgId) + (32 * vtId)  ", (8192UL * slice) + (672UL * stsId) + (96UL * vtgId) + (32UL * vtId));
    OffsetDisplay(       "  - (8192 * slice) + (672 * stsId) + (96 * vtgId)                ", (8192UL * slice) + (672UL * stsId) + (96UL * vtgId));
    OffsetDisplay(       "  - (8192 * slice) + (1024 * stsId) + (128 * vtgId) + (32 * vtId)", (8192UL * slice) + (1024UL * stsId) + (128UL * vtgId) + (32UL * vtId));

    ThaModuleOcnSdhChannelRegsShow(channel);
    return cAtOk;
    }

static eBool HasLedEngine(ThaModuleSdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool HasCounters(ThaModuleSdh self)
    {
    /* Some products have counters moved to another modules. But let them determine */
    AtUnused(self);
    return cAtTrue;
    }

static uint8 NumLinesPerPart(ThaModuleSdh self)
    {
    return AtModuleSdhMaxLinesGet((AtModuleSdh)self) / ThaDeviceNumPartsOfModule((ThaDevice)AtModuleDeviceGet((AtModule)self), cAtModuleSdh);
    }

static eBool PohIsReady(ThaModuleSdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 StartVersionSupportLineTiming(ThaModuleSdh self)
    {
    AtUnused(self);
    return cBit31_0;
    }

static eBool HasClockOutput(ThaModuleSdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaModuleSdh object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(interruptEnabled);
    mEncodeUInt(asyncInitState);
    mEncodeUInt(asyncLineDefaultInitState);
    mEncodeUInt(countersModule);
    }

static AtSdhLine LineFromHwIdGet(ThaModuleSdh self, eAtModule phyModule, uint8 sliceId, uint8 localLineInSlice)
    {
    AtUnused(phyModule);
    AtUnused(sliceId);
    /* Default implementation of simple HW line ID numbering */
    return AtModuleSdhLineGet((AtModuleSdh)self, localLineInSlice);
    }

static AtSdhPath AuPathFromHwIdGet(ThaModuleSdh self, eAtModule phyModule, uint8 sliceId, uint8 stsId)
    {
    AtUnused(self);
    AtUnused(phyModule);
    AtUnused(sliceId);
    AtUnused(stsId);
    return NULL;
    }

static AtSdhPath TuPathFromHwIdGet(ThaModuleSdh self, eAtModule phyModule, uint8 sliceId, uint8 stsId, uint8 vtgId, uint8 vtId)
    {
    AtUnused(self);
    AtUnused(phyModule);
    AtUnused(sliceId);
    AtUnused(stsId);
    AtUnused(vtgId);
    AtUnused(vtId);
    return NULL;
    }

static eBool HasAps(ThaModuleSdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint8 PartOfLine(ThaModuleSdh self, uint8 lineId)
    {
    uint8 numLinePerPart = ThaModuleSdhNumLinesPerPart((AtModuleSdh)self);

    if (numLinePerPart)
        return (uint8)(lineId / numLinePerPart);

    return 0;
    }

static uint32 LineInterruptStatusGet(ThaModuleSdh self, uint32 glbIntr, AtHal hal)
    {
    AtUnused(self);
    AtUnused(glbIntr);
    AtUnused(hal);
    return 0;
    }

static uint32 StsInterruptStatusGet(ThaModuleSdh self, uint32 glbIntr, AtHal hal)
    {
    AtUnused(self);
    AtUnused(glbIntr);
    AtUnused(hal);
    return 0;
    }

static uint32 VtInterruptStatusGet(ThaModuleSdh self, uint32 glbIntr, AtHal hal)
    {
    AtUnused(self);
    AtUnused(glbIntr);
    AtUnused(hal);
    return 0;
    }

static AtModulePrbs PrbsModule(ThaModuleSdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return (AtModulePrbs)AtDeviceModuleGet(device, cAtModulePrbs);
    }

static AtPrbsEngine SdhLinePrbsEngineCreate(ThaModuleSdh self, AtSdhLine line)
    {
    AtModulePrbs prbsModule = PrbsModule(self);
    if (line == NULL)
        return NULL;
    return AtModulePrbsSdhLinePrbsEngineCreate(prbsModule, line);
    }

static eBool ShouldRemoveLoopbackOnBinding(ThaModuleSdh self)
    {
    /* Let concrete product determine, they may have some special purposes of
     * doing this */
    AtUnused(self);
    return cAtFalse;
    }

static eBool ShouldUpdateSerdesTimingWhenLineRateChange(ThaModuleSdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static AtObjectAny LineAttControllerCreate(ThaModuleSdh self, AtChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return NULL;
    }
static AtObjectAny PathAttControllerCreate(ThaModuleSdh self, AtChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return NULL;
    }
static AtObjectAny Vc1xAttControllerCreate(ThaModuleSdh self, AtChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return NULL;
    }


static void HwSts24ToHwSts48Get(ThaModuleSdh self,uint8 slice24, uint8 sts24, uint8 *slice48, uint8* sts48)
    {
    AtUnused(self);
    *sts48 = (uint8)((sts24 << 1) + (slice24 & 0x1));
    *slice48 = slice24 >> 1;
    }

static eBool DebugCountersModuleIsSupported(ThaModuleSdh self, uint32 module)
    {
    AtUnused(self);
    if ((module == cAtModuleSdh) || (module == cThaModulePmc))
        return cAtTrue;
    return cAtFalse;
    }

static eAtRet DebugCountersModuleSet(ThaModuleSdh self, uint32 module)
    {
    if (DebugCountersModuleIsSupported(self, module))
        {
        self->countersModule = module;
        return cAtOk;
        }

    return cAtErrorModeNotSupport;
    }

static void CounterModuleSetup(AtModule self)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet(self);
    mThis(self)->countersModule = ThaDeviceDefaultCounterModule(device, cAtModuleSdh);
    }

static eAtRet Setup(AtModule self)
    {
    CounterModuleSetup(self);
    return m_AtModuleMethods->Setup(self);
    }

static uint32 MaxNumDccHdlcChannels(ThaModuleSdh self)
    {
    /* Let concrete product determine, they may have some special purposes of
     * doing this */
    AtUnused(self);
    return 0;
    }

static eBool UseStaticXc(ThaModuleSdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet LineInterruptEnable(ThaModuleSdh self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorNotImplemented;
    }

static eAtRet StsInterruptEnable(ThaModuleSdh self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorNotImplemented;
    }

static eAtRet VtInterruptEnable(ThaModuleSdh self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorNotImplemented;
    }

static void DebugCounterModuleShow(ThaModuleSdh self)
    {
    AtModule counterModule = AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), mThis(self)->countersModule);
    AtPrintc(cSevNormal, "* Counter module: %s\r\n", AtModuleTypeString(counterModule));
    }

static eAtRet Debug(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Debug(self);
    DebugCounterModuleShow((ThaModuleSdh)self);
    return ret;
    }

static void OverrideAtObject(ThaModuleSdh self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(ThaModuleSdh self)
    {
    AtModule module = (AtModule)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, InterruptProcess);
        mMethodOverride(m_AtModuleOverride, InterruptOnIpCoreEnable);
        mMethodOverride(m_AtModuleOverride, InterruptEnable);
        mMethodOverride(m_AtModuleOverride, InterruptIsEnabled);
        mMethodOverride(m_AtModuleOverride, LoopbackSet);
        mMethodOverride(m_AtModuleOverride, LoopbackGet);
        mMethodOverride(m_AtModuleOverride, LoopbackIsSupported);
        mMethodOverride(m_AtModuleOverride, Setup);
        mMethodOverride(m_AtModuleOverride, Debug);
        mMethodOverride(m_AtModuleOverride, ForcePointerPeriodicProcess);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtModuleSdh(ThaModuleSdh self)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleSdhOverride, mMethodsGet(sdhModule), sizeof(m_AtModuleSdhOverride));
        mMethodOverride(m_AtModuleSdhOverride, MaxLinesGet);
        mMethodOverride(m_AtModuleSdhOverride, MaxLineRate);
        mMethodOverride(m_AtModuleSdhOverride, ChannelCreate);
        }

    mMethodsSet(sdhModule, &m_AtModuleSdhOverride);
    }

static void Override(ThaModuleSdh self)
    {
    OverrideAtModuleSdh(self);
    OverrideAtModule(self);
    OverrideAtObject(self);
    }

static void MethodsInit(ThaModuleSdh self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, OcnRxStsVCIntrStatDeftOffset);
        mMethodOverride(m_methods, OcnRxVtTuIntrStatDeftOffset);
        mMethodOverride(m_methods, SerdesControllerCreate);
        mMethodOverride(m_methods, SerdesCoreIdOfLine);
        mMethodOverride(m_methods, SerdesIdOfLine);
        mMethodOverride(m_methods, PayloadUneqIsApplicable);
        mMethodOverride(m_methods, HasLineInterrupt);
        mMethodOverride(m_methods, HasStsInterrupt);
        mMethodOverride(m_methods, HasVtInterrupt);
        mMethodOverride(m_methods, BerHardwareInterruptIsSupported);
        mMethodOverride(m_methods, LineSerdesPrbsIsSupported);
        mMethodOverride(m_methods, LineSerdesPrbsEngineCreate);
        mMethodOverride(m_methods, ErdiIsEnabledByDefault);
        mMethodOverride(m_methods, VcAisIsEnabledByDefault);
        mMethodOverride(m_methods, DefaultVc1xSubMapping);
        mMethodOverride(m_methods, BlockErrorCountersSupported);
        mMethodOverride(m_methods, DefaultSerdesTimingModeForLine);
        mMethodOverride(m_methods, ChannelCreateWithVersion);
        mMethodOverride(m_methods, AlwaysDisableRdiBackwardWhenTimNotDownstreamAis);
        mMethodOverride(m_methods, ShouldUpdateTimRdiBackwardWhenAisDownstreamChange);
        mMethodOverride(m_methods, ShouldUpdatePlmMonitorWhenExpectedPslChange);
        mMethodOverride(m_methods, CanDisablePwPacketsToPsn);
        mMethodOverride(m_methods, LineDefaultRate);
        mMethodOverride(m_methods, StartVersionSupport24BitsBlockErrorCounter);
        mMethodOverride(m_methods, CommonRegisterOffsetsDisplay);
        mMethodOverride(m_methods, HasLedEngine);
        mMethodOverride(m_methods, HasCounters);
        mMethodOverride(m_methods, AlarmDownstreamEnable);
        mMethodOverride(m_methods, LineDefaultSet);
        mMethodOverride(m_methods, AsyncLineDefaultSet);
        mMethodOverride(m_methods, NumLinesPerPart);
        mMethodOverride(m_methods, PohIsReady);
        mMethodOverride(m_methods, HasClockOutput);
        mMethodOverride(m_methods, VcPrbsEngineCreate);
        mMethodOverride(m_methods, HasAps);
        mMethodOverride(m_methods, PartOfLine);
        mMethodOverride(m_methods, LocalLineId);
        mMethodOverride(m_methods, LineFromHwIdGet);
        mMethodOverride(m_methods, AuPathFromHwIdGet);
        mMethodOverride(m_methods, TuPathFromHwIdGet);
        mMethodOverride(m_methods, LineInterruptStatusGet);
        mMethodOverride(m_methods, StsInterruptStatusGet);
        mMethodOverride(m_methods, VtInterruptStatusGet);
        mMethodOverride(m_methods, StartVersionSupportLineTiming);
        mMethodOverride(m_methods, ShouldRemoveLoopbackOnBinding);
        mMethodOverride(m_methods, ShouldUpdateSerdesTimingWhenLineRateChange);
        mMethodOverride(m_methods, LineAttControllerCreate);
        mMethodOverride(m_methods, PathAttControllerCreate);
        mMethodOverride(m_methods, Vc1xAttControllerCreate);
        mMethodOverride(m_methods, DebugCountersModuleSet);
        mMethodOverride(m_methods, LineInterruptProcess);
        mMethodOverride(m_methods, StsInterruptProcess);
        mMethodOverride(m_methods, VtInterruptProcess);
        mMethodOverride(m_methods, MaxNumDccHdlcChannels);
        mMethodOverride(m_methods, UseStaticXc);
        mMethodOverride(m_methods, LineInterruptEnable);
        mMethodOverride(m_methods, StsInterruptEnable);
        mMethodOverride(m_methods, VtInterruptEnable);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaModuleSdh);
    }

AtModuleSdh ThaModuleSdhObjectInit(AtModuleSdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleSdhObjectInit(self, device) == NULL)
        return NULL;

    /* Override */
    Override(mThis(self));
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtModuleSdh ThaModuleSdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ThaModuleSdhObjectInit(newModule, device);
    }

uint8 ThaModuleSdhNumLinesPerPart(AtModuleSdh self)
    {
    if (self)
        return mMethodsGet((ThaModuleSdh)self)->NumLinesPerPart((ThaModuleSdh)self);
    return 0xFF;
    }

AtSdhLine ThaSdhLineFromChannel(AtSdhChannel channel)
    {
    return AtSdhChannelLineObjectGet(channel);
    }

uint8 ThaModuleSdhPartOfChannel(AtSdhChannel channel)
    {
    if (channel)
        {
        AtSdhLine line = AtSdhChannelLineObjectGet(channel);
        ThaModuleSdh self = (ThaModuleSdh)AtChannelModuleGet((AtChannel)channel);
        if ((self) && (line))
            return (uint8)mMethodsGet(self)->PartOfLine(self, (uint8)AtChannelIdGet((AtChannel)line));
        }
    return 0;
    }

uint8 ThaModuleSdhLineIdLocalId(ThaModuleSdh self, uint8 lineId)
    {
    if (self)
        return mMethodsGet(self)->LocalLineId(self, lineId);
    return 0;
    }

uint8 ThaModuleSdhLineLocalId(AtSdhLine line)
    {
    AtChannel channel = (AtChannel)line;
    ThaModuleSdh self = (ThaModuleSdh)AtChannelModuleGet((AtChannel)channel);
    if (self)
        return mMethodsGet(self)->LocalLineId(self, AtChannelIdGet(channel));
    return 0;
    }

uint32 ThaSdhChannelCounterFromReadOnlyCounterGet(AtChannel self, uint32 newCounterValue, uint32 *lastValue, uint32 maxValue, uint32 counterType, eBool read2clear)
    {
    uint32 resultCounterValue;
    eBool isRolled;
	AtUnused(self);

    if (lastValue == NULL)
        return newCounterValue;

    /* Calculate result counter needs to be returned */
    isRolled = (newCounterValue < *lastValue) ? cAtTrue : cAtFalse;
    if (isRolled)
        {
        resultCounterValue = newCounterValue + (maxValue - *lastValue);
        if ((counterType == cAtSdhPathCounterTypeBip) || (counterType == cAtSdhPathCounterTypeRei))
            resultCounterValue++;
        }
    else
        resultCounterValue = newCounterValue - *lastValue;

    /* Only need to cache last value for next reading when reading the counter
     * in read to clear mode */
    if (read2clear)
        *lastValue = newCounterValue;

    return resultCounterValue;
    }

AtSerdesController ThaModuleSdhSerdesControllerCreate(ThaModuleSdh self, AtSdhLine line, uint32 serdesId)
    {
    if (self)
        return mMethodsGet(self)->SerdesControllerCreate(self, line, serdesId);
    return NULL;
    }

uint8 ThaModuleSdhSerdesCoreIdOfLine(ThaModuleSdh self, AtSdhLine line)
    {
    if (self)
        return mMethodsGet(self)->SerdesCoreIdOfLine(self, line);

    return (uint8)0xCAFE;
    }

uint8 ThaModuleSdhSerdesIdOfLine(ThaModuleSdh self, AtSdhLine line)
    {
    if (self)
        return mMethodsGet(self)->SerdesIdOfLine(self, line);
    return (uint8)0xCAFE;
    }

eBool ThaModuleSdhPayloadUneqIsApplicable(ThaModuleSdh self)
    {
    if (self)
        return mMethodsGet(self)->PayloadUneqIsApplicable(self);
    return cAtFalse;
    }

eBool ThaModuleSdhBerHardwareInterruptIsSupported(ThaModuleSdh self)
    {
    if (self)
        return mMethodsGet(self)->BerHardwareInterruptIsSupported(self);
    return cAtFalse;
    }

AtPrbsEngine ThaModuleSdhLineSerdesPrbsEngineCreate(ThaModuleSdh self, AtSdhLine line, AtSerdesController serdes)
    {
    if (self == NULL)
        return NULL;

    if (!mMethodsGet(self)->LineSerdesPrbsIsSupported(self, line, serdes))
        return NULL;

    return mMethodsGet(self)->LineSerdesPrbsEngineCreate(self, line, serdes);
    }

eBool ThaModuleSdhErdiIsEnabledByDefault(ThaModuleSdh self)
    {
    if (self)
        return mMethodsGet(self)->ErdiIsEnabledByDefault(self);
    return cAtFalse;
    }

eBool ThaModuleSdhVcAisIsEnabledByDefault(ThaModuleSdh self)
    {
    if (self)
        return mMethodsGet(self)->VcAisIsEnabledByDefault(self);

    return cAtFalse;
    }

uint8 ThaModuleSdhDefaultVc1xSubMapping(ThaModuleSdh self)
    {
    if (self)
        return mMethodsGet(self)->DefaultVc1xSubMapping(self);
    return cAtSdhVcMapTypeNone;
    }

eBool ThaModuleSdhBlockErrorCountersSupported(ThaModuleSdh self)
    {
    if (self)
        return mMethodsGet(self)->BlockErrorCountersSupported(self);
    return cAtFalse;
    }

eAtSerdesTimingMode ThaModuleSdhDefaultSerdesTimingModeForLine(ThaModuleSdh self, AtSdhLine line)
    {
    if (self)
        return mMethodsGet(self)->DefaultSerdesTimingModeForLine(self, line);
    return cAtSerdesTimingModeAuto;
    }

eAtRet ThaSdhChannelCommonRegisterOffsetsDisplay(AtSdhChannel channel)
    {
    ThaModuleSdh self = (ThaModuleSdh)AtChannelModuleGet((AtChannel)channel);
    if (self)
        return mMethodsGet(self)->CommonRegisterOffsetsDisplay(self, channel);
    return cAtErrorNullPointer;
    }

eBool ThaModuleSdhAlwaysDisableRdiBackwardWhenTimNotDownstreamAis(ThaModuleSdh self)
    {
    if (self)
        return mMethodsGet(self)->AlwaysDisableRdiBackwardWhenTimNotDownstreamAis(self);

    return cAtFalse;
    }

eBool ThaModuleSdhShouldUpdateTimRdiBackwardWhenAisDownstreamChange(ThaModuleSdh self)
    {
    if (self)
        return mMethodsGet(self)->ShouldUpdateTimRdiBackwardWhenAisDownstreamChange(self);

    return cAtFalse;
    }

eBool ThaModuleSdhShouldUpdatePlmMonitorWhenExpectedPslChange(ThaModuleSdh self)
    {
    if (self)
        return mMethodsGet(self)->ShouldUpdatePlmMonitorWhenExpectedPslChange(self);
    return cAtFalse;
    }

AtSdhChannel ThaSdhVcForPhysicalChannel(AtChannel physicalChannel)
    {
    eAtModule module;

    if (physicalChannel == NULL)
        return NULL;

    module = AtModuleTypeGet(AtChannelModuleGet(physicalChannel));
    switch (module)
        {
        case cAtModulePdh:
            return (AtSdhChannel)AtPdhChannelVcInternalGet((AtPdhChannel)physicalChannel);
        case cAtModuleSdh:
            return (AtSdhChannel)physicalChannel;

        /* Impossible, but... */
        case cAtModulePpp:
        case cAtModuleFr:
        case cAtModuleEncap:
        case cAtModuleEth:
        case cAtModulePw:
        case cAtModulePrbs:
        case cAtModuleIma:
        case cAtModuleAtm:
        case cAtModuleRam:
        case cAtModuleBer:
        case cAtModulePktAnalyzer:
        case cAtModuleClock:
        case cAtModuleXc:
        case cAtModuleAps:
        case cAtModuleSur:
        case cAtModuleConcate:
        case cAtModulePtp:
        case cAtModuleInvalid:
        default:
            AtChannelLog(physicalChannel, cAtLogLevelWarning, AtSourceLocation, "Invalid module, got %d\r\n", module);
            return NULL;
        }
    }

uint32 ThaModuleSdhAutoMsRdiAlarms(ThaModuleSdh self)
    {
    uint32 alarm = 0;
    uint32 regVal = mModuleHwRead(self, cThaRegOcnFrmrAlmAffCtrl + PartOffset((AtModuleSdh)self, 0));

    if (regVal & cThaTimLRdiLEnMask) alarm |= cAtSdhLineAlarmTim;
    if (regVal & cThaAisLRdiLEnMask) alarm |= cAtSdhLineAlarmAis;
    if (regVal & cThaLosRdiLEnMask)  alarm |= cAtSdhLineAlarmLos;
    if (regVal & cThaLofRdiLEnMask)  alarm |= cAtSdhLineAlarmLof;
    if (regVal & cThaOofRdiLEnMask)  alarm |= cAtSdhLineAlarmOof;

    return alarm;
    }

eBool ThaModuleSdhCanDisablePwPacketsToPsn(ThaModuleSdh self)
    {
    if (self)
        return mMethodsGet(self)->CanDisablePwPacketsToPsn(self);
    return cAtFalse;
    }

eBool ThaModuleSdhHas24BitsBlockErrorCounter(ThaModuleSdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    uint32 startSupportedVersion = mMethodsGet(self)->StartVersionSupport24BitsBlockErrorCounter(self);
    return (AtDeviceVersionNumber(device) >= startSupportedVersion) ? cAtTrue : cAtFalse;
    }

AtPrbsEngine ThaModuleSdhVcPrbsEngineCreate(ThaModuleSdh self, AtSdhVc vc)
    {
    if (self)
        return mMethodsGet(self)->VcPrbsEngineCreate(self, vc);
    return NULL;
    }

eBool ThaModuleSdhPohIsReady(ThaModuleSdh self)
    {
    if (self)
        return mMethodsGet(self)->PohIsReady(self);
    return cAtFalse;
    }

void ThaSdhChannelRestoreAllHwSts(AtSdhChannel self)
    {
    uint8 slice, hwSts, sts_i;

    for (sts_i = 0; sts_i < AtSdhChannelNumSts(self); sts_i++)
        ThaSdhChannelHwStsGet(self, cThaModuleOcn,(uint8)(AtSdhChannelSts1Get(self) + sts_i), &slice, &hwSts);
    }

AtSdhLine ThaModuleSdhLineFromHwIdGet(ThaModuleSdh self, eAtModule phyModule, uint8 sliceId, uint8 localLineInSlice)
    {
    if (self)
        return mMethodsGet(self)->LineFromHwIdGet(self, phyModule, sliceId, localLineInSlice);
    return NULL;
    }

AtSdhPath ThaModuleSdhAuPathFromHwIdGet(ThaModuleSdh self, eAtModule phyModule, uint8 sliceId, uint8 stsId)
    {
    if (self)
        return mMethodsGet(self)->AuPathFromHwIdGet(self, phyModule, sliceId, stsId);
    return NULL;
    }

AtSdhPath ThaModuleSdhTuPathFromHwIdGet(ThaModuleSdh self, eAtModule phyModule, uint8 sliceId, uint8 stsId, uint8 vtgId, uint8 vtId)
    {
    if (self)
        return mMethodsGet(self)->TuPathFromHwIdGet(self, phyModule, sliceId, stsId, vtgId, vtId);
    return NULL;
    }

void ThaModuleSdhHwSts24ToHwSts48Get(ThaModuleSdh self, uint8 slice24, uint8 sts24, uint8 *slice48, uint8* sts48)
    {
    if (self)
        HwSts24ToHwSts48Get(self, slice24, sts24, slice48, sts48);
    else
        {
        *sts48     = cInvalidUint8;
        *slice48   = cInvalidUint8;
        }
    }

void ThaModuleSdhHwSts48ToHwSts24Get(uint8 slice48, uint8 sts48, uint8 *slice24, uint8* sts24)
    {
    *slice24 = (uint8)((slice48 << 1) | (sts48 & 1));
    *sts24 = sts48 >> 1;
    }

AtPrbsEngine ThaModuleSdhLinePrbsEngineCreate(ThaModuleSdh self, AtSdhLine line)
    {
    if (self)
        return SdhLinePrbsEngineCreate(self, line);
    return NULL;
    }

eBool ThaModuleSdhLineTimingIsSupported(ThaModuleSdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    uint32 startSupportedVersion;

    if (AtDeviceAllFeaturesAvailableInSimulation(device))
        return cAtTrue;

    startSupportedVersion = mMethodsGet(self)->StartVersionSupportLineTiming(self);
    return (AtDeviceVersionNumber(device) >= startSupportedVersion) ? cAtTrue : cAtFalse;
    }

eBool ThaModuleSdhShouldRemoveLoopbackOnBinding(ThaModuleSdh self)
    {
    if (self)
        return mMethodsGet(self)->ShouldRemoveLoopbackOnBinding(self);
    return cAtFalse;
    }

eAtRet ThaModuleSdhAsyncSuperInit(AtModule self)
    {
    return m_AtModuleMethods->AsyncInit(self);
    }

eAtRet ThaModuleSdhLineIdFromDbLineId(AtModuleSdh self, uint8 dbLineId)
    {
    return mMethodsGet(self)->LineIdFromDbLineId(self, dbLineId);
    }

void ThaSdhModuleLineDefaultSet(ThaModuleSdh self, uint32 dbLineId)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)self;
    eBool hasClockOutput = mMethodsGet(self)->HasClockOutput(self);
    uint8 lineId = mMethodsGet(sdhModule)->LineIdFromDbLineId(sdhModule, (uint8)dbLineId);
    AtSdhLine line = AtModuleSdhLineGet(sdhModule, lineId);
    if (line)
        {
        eAtSdhLineRate rate = mMethodsGet(mThis(self))->LineDefaultRate(mThis(self), line);
        eAtSdhChannelMode mode = AtModuleSdhLineDefaultMode(sdhModule, line);

        AtSdhChannelModeSet((AtSdhChannel)line, mode);
        AtSdhLineRateSet(line, rate);
        if (hasClockOutput)
            ClockOutWhenLosEnable(sdhModule, lineId, cAtFalse);
        }
    }

void ThaModuleSdhPostInit(AtModule self)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)self;
    LedEngineInit(self);
    CounterEngineInit(self);
    mMethodsGet(mThis(self))->AlarmDownstreamEnable(mThis(self), cAtTrue);
    if (mMethodsGet(mThis(self))->HasAps(mThis(self)))
        ApsInit(sdhModule);
    }

eBool ThaModuleSdhShouldUpdateSerdesTimingWhenLineRateChange(ThaModuleSdh self)
    {
    if (self)
        return mMethodsGet(self)->ShouldUpdateSerdesTimingWhenLineRateChange(self);

    return cAtFalse;
    }

uint32 ThaModuleSdhMaxNumDccHdlcChannels(ThaModuleSdh self)
    {
    if (self)
        return mMethodsGet(self)->MaxNumDccHdlcChannels(self);
    return 0;
    }

eAtRet ThaModuleSdhDebugCountersModuleSet(ThaModuleSdh self, uint32 module)
    {
    if (self)
        return mMethodsGet(self)->DebugCountersModuleSet(self, module);
    return cAtErrorNullPointer;
    }

uint32 ThaModuleSdhDebugCountersModuleGet(ThaModuleSdh self)
    {
    return self ? self->countersModule : cAtModuleSdh;
    }

eBool ThaModuleSdhUseStaticXc(ThaModuleSdh self)
    {
    if (self)
        return mMethodsGet(self)->UseStaticXc(self);
    return cAtTrue;
    }

uint8 ThaSdhHwChannelMode(eAtSdhChannelMode mode)
    {
    return (mode == cAtSdhChannelModeSdh) ? 1 : 0;
    }

eAtSdhChannelMode ThaSdhSwChannelMode(uint8 hwMode)
    {
    return (hwMode) ? cAtSdhChannelModeSdh : cAtSdhChannelModeSonet;
    }

eAtRet ThaModuleSdhLineInterruptEnable(ThaModuleSdh self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->LineInterruptEnable(self, enable);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleSdhStsInterruptEnable(ThaModuleSdh self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->StsInterruptEnable(self, enable);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleSdhVtInterruptEnable(ThaModuleSdh self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->VtInterruptEnable(self, enable);
    return cAtErrorNullPointer;
    }

void ThaModuleSdhDebugCounterModuleShow(ThaModuleSdh self)
    {
    if (self)
        DebugCounterModuleShow(self);
    }

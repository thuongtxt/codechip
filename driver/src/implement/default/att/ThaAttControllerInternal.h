/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : AtAttControllerInternal.h
 * 
 * Created Date: May 17, 2016
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAATTCONTROLLERINTERNAL_H_
#define _THAATTCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../cdr/ThaModuleCdr.h"
#include "../cdr/controllers/ThaCdrControllerInternal.h"
#include "../../../generic/att/AtAttControllerInternal.h"
#include "ThaAttController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cRegAttFreqOffControl         0x20800
#define cRegAttFreqOffHoldover        0x21800
/* Basic NCO value corresponding to system clk 155Mhz */
#define cBaseNcoValE1   3817748707UL
#define cBaseNcoValDs1  3837632815UL
#define cBaseNcoValVt2  4175662648UL
#define cBaseNcoValVt15 4135894433UL
#define cBaseNcoValE3   2912117977UL
#define cBaseNcoValDs3  3790634015UL
#define cBaseNcoValTu3  4148547956UL
#define cBaseNcoValSts1 4246160849UL

#define cNcoValPpbOffsetE1    262
#define cNcoValPpbOffsetDS1   260
#define cNcoValPpbOffsetVT2   239
#define cNcoValPpbOffsetVT15  241
#define cNcoValPpbOffsetE3    343
#define cNcoValPpbOffsetDS3   263
#define cNcoValPpbOffsetTU3   239
#define cNcoValPpbOffsetSTS1  239

#define cPpb2Ppm 1000000

#define cThaAttForceAlarm          0
#define cThaAttForceError          1
#define cThaAttForcePointerAdj     2

#define cAtNRate1E2 100
#define cAtNRate1E3 1000
#define cAtNRate1E4 10000
#define cAtNRate1E5 100000
#define cAtNRate1E6 1000000
#define cAtNRate1E7 10000000
#define cAtNRate1E8 100000000
#define cAtNRate1E9 1000000000

#define cAtNbitPerFrameStm0  6480    /* 51840/8   |   9 rows * 90 (columns)bytes = 810 bytes   = 6480 bit | Time 1 frame = 125 microseconds */
#define cAtNbitPerFrameStm1  19440   /* 155520/8  |   9 rows *270 (columns)bytes = 2430 bytes  = 19440 bit| Time 1 frame = 125 microseconds */
#define cAtNbitPerFrameStm4  77760   /* 622080/8  |  36 rows *270 (columns)bytes = 9720 bytes  = 77760 bit| Time 1 frame = 125 microseconds */
#define cAtNbitPerFrameStm16 311040  /* 2488320/8 | 144 rows *270 (columns)bytes */
#define cAtNbitPerFrameStm64 1244160 /* 9953280/8 | 576 rows *270 (columns)bytes */
/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

typedef struct tRateInfo
    {
    uint16 step;
    uint16 positionMask;
    uint8  frc_en_mask;

    uint16 step_mini;
    uint16 positionMask_mini;
    uint8  frc_en_mask_mini;

    uint16 step_3rd;
    uint16 positionMask_3rd;
    uint8  frc_en_mask_3rd;

    uint8  max_pos;
    double  value;
    double  diff;
    }tRateInfo;

typedef struct tThaAttControllerMethods
    {
    ThaCdrController (*CdrControllerGet)(ThaAttController self);
    uint32 (*FrequenceOffsetControlRegister)(ThaAttController self);
    uint32 (*FrequenceOffsetHoldoverRegister)(ThaAttController self);
    uint32 (*FrequenceOffsetDefaultOffset)(ThaAttController self);
    uint32 (*FrequenceOffsetBase)(ThaAttController self);
    uint32 (*FrequenceOffsetStep)(ThaAttController self);
    uint32 (*ChannelSpeed)(ThaAttController self);
    uint32 (*NbitPerFrame)(ThaAttController self);
    uint32 (*MaxStep)(ThaAttController self, uint32 isError, uint32 errorType, uint32 Nrate, uint32 NbitPerFrame);
    uint32 (*StartVersionSupportStep14bit)(ThaAttController self, uint32 errorType); /* For B1/B2 */
    eBool  (*IsDataMask8bit)(ThaAttController self, uint32 errorType);
    eBool  (*IsDataMask2bit)(ThaAttController self, uint32 errorType);
    uint32 (*StartVersionSupportDataMask8bit)(ThaAttController self, uint32 errorType);
    uint32 (*StartVersionSupportDataMask2bit)(ThaAttController self, uint32 errorType);
    eBool  (*IsStep16bit)(ThaAttController self, uint32 errorType);
    uint32 (*StartVersionSupportStep16bit)(ThaAttController self); /* DS1 CRC */

    eBool  (*NeedRemoveSoh)(ThaAttController self, uint32 errorType);
    uint32 (*NbitPerFrameSoh)(ThaAttController self);
    eAtRet (*DefaultSet)(ThaAttController self);
    eAtRet (*InitErrorGap)(ThaAttController self);
    eAtRet (*InitDb)(ThaAttController self);
    eBool  (*UseAdjust)(ThaAttController self, uint32 errorType);
    uint32 (*MaxNumberofBytePerFrame)(ThaAttController self, uint32 errorType);
    eAtRet (*HwForceErrorRate)(ThaAttController self, uint32 errorType, uint32 rate);
    uint16 (*PositionMaskGet)(uint8 array_max_pos, uint16 array_positionMask);
    eBool  (*IsCheckRateShow)(ThaAttController self);
    eBool  (*IsCheckRate)(ThaAttController self);
    eBool  (*RateCalib)(ThaAttController self, uint32 NbitPerFrame, uint32 Nrate, uint16 *err_step, uint16 *PositionMask, uint8 *frc_en_mask, uint8 *hs);
    eBool  (*RateConvertStepIsSupport)(ThaAttController self);
    eAtRet (*RateConvertStep)(ThaAttController self, uint32 NbitPerFrame, uint32 Nrate, uint16 *err_step);
    eBool  (*IsB1)(ThaAttController self, uint32 errorType);
    eBool  (*IsE1)(ThaAttController self);
    uint32 (*CalculateNRate)(ThaAttController self, uint32 rate);

    tAtOsalCurTime* (*ForcePointerBeginTimeGet)(ThaAttController self);
    }tThaAttControllerMethods;

typedef struct tThaAttController
    {
    tAtAttController super;
    const tThaAttControllerMethods *methods;

    /* Private data */
    }tThaAttController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtAttController ThaAttControllerObjectInit(AtAttController self, AtChannel channel);
float  ThaAttControllerNFramePerSecond(ThaAttController self);
uint32 ThaAttControllerChannelSpeed(ThaAttController self);
uint32 ThaAttControllerNbitPerFrame(ThaAttController self);
uint32 ThaAttControllerNbitPerFrameSoh(ThaAttController self);
eBool  ThaAttControllerNeedRemoveSoh(ThaAttController self, uint32 errorType);
uint32 ThaAttControllerMaxStep(ThaAttController self, uint32 isError, uint32 errorType, uint32 Nrate, uint32 NbitPerFrame);
eBool  ThaAttControllerIsStep14bit(ThaAttController self, uint32 errorType);
eBool  ThaAttControllerIsDataMask2bit(ThaAttController self, uint32 errorType);
eBool  ThaAttControllerIsDataMask8bit(ThaAttController self, uint32 errorType);
eBool  ThaAttControllerIsStep16bit(ThaAttController self, uint32 errorType);
eBool  ThaAttControllerUseAdjust(ThaAttController self, uint32 errorType);
ThaVersionReader ThaAttControllerVersionReader(ThaAttController self);

#ifdef __cplusplus
}
#endif
#endif /* _THAATTCONTROLLERINTERNAL_H_ */


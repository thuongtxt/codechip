/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : ThaPrbsRegProvider.c
 *
 * Created Date: Jan 6, 2016
 *
 * Description : Register provider
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaAttPrbsRegProviderInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaAttPrbsRegProviderMethods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static uint32 PrbsGenReg(ThaAttPrbsRegProvider self, eBool isHo)
    {
    AtUnused(self);
    AtUnused(isHo);
    return cInvalidUint32;
    }

static uint32 PrbsMonReg(ThaAttPrbsRegProvider self, eBool isHo)
    {
    AtUnused(self);
    AtUnused(isHo);
    return cInvalidUint32;
    }

static uint8 LoPrbsEngineOc48IdShift(ThaAttPrbsRegProvider self)
    {
    AtUnused(self);
    return cInvalidUint8;
    }

static uint8 LoPrbsEngineOc24SliceIdShift(ThaAttPrbsRegProvider self)
    {
    AtUnused(self);
    return cInvalidUint8;
    }

static uint32 PrbsDelayConfigReg(ThaAttPrbsRegProvider self, eBool isHo, uint8 slice)
	{
	AtUnused(self);
	AtUnused(isHo);
	AtUnused(slice);
	return cInvalidUint32;
	}

static uint32 PrbsDelayIdMask(ThaAttPrbsRegProvider self, eBool isHo)
	{
	AtUnused(self);
	AtUnused(isHo);
	return cInvalidUint32;
	}

static uint32 PrbsDelayEnableMask(ThaAttPrbsRegProvider self, eBool isHo)
	{
	AtUnused(self);
	AtUnused(isHo);
	return cInvalidUint32;
	}

static uint32 PrbsMaxDelayReg(ThaAttPrbsRegProvider self, eBool isHo, eBool r2c, uint8 slice)
	{
	AtUnused(self);
	AtUnused(isHo);
	AtUnused(r2c);
	AtUnused(slice);
	return cInvalidUint32;
	}

static uint32 PrbsMinDelayReg(ThaAttPrbsRegProvider self, eBool isHo, eBool r2c, uint8 slice)
	{
	AtUnused(self);
	AtUnused(isHo);
	AtUnused(r2c);
	AtUnused(slice);
	return cInvalidUint32;
	}

static uint32 PrbsModeMask(ThaAttPrbsRegProvider self, eBool isHo, eBool isTx)
	{
	AtUnused(self);
	AtUnused(isHo);
	AtUnused(isTx);
	return cInvalidUint32;
	}

static uint32 PrbsStepMask(ThaAttPrbsRegProvider self, eBool isHo, eBool isTx)
	{
	AtUnused(self);
	AtUnused(isHo);
	AtUnused(isTx);
	return cInvalidUint32;
	}

static uint32 PrbsEnableMask(ThaAttPrbsRegProvider self, eBool isHo, eBool isTx)
	{
	AtUnused(self);
	AtUnused(isHo);
	AtUnused(isTx);
	return cInvalidUint32;
	}

static uint32 PrbsFixPatternMask(ThaAttPrbsRegProvider self, eBool isHo, eBool isTx)
	{
	AtUnused(self);
	AtUnused(isHo);
	AtUnused(isTx);
	return cInvalidUint32;
	}

static uint32 PrbsFixPatternReg(ThaAttPrbsRegProvider self, eBool isHo, eBool isTx)
	{
	AtUnused(self);
	AtUnused(isHo);
	AtUnused(isTx);
	return cInvalidUint32;
	}

static uint32 PrbsCounterReg(ThaAttPrbsRegProvider self, eBool isHo, eBool r2c)
	{
	AtUnused(self);
	AtUnused(isHo);
	AtUnused(r2c);
	return cInvalidUint32;
	}

static uint32 PrbsCounterMask(ThaAttPrbsRegProvider self, uint16 counterType)
	{
	AtUnused(self);
	AtUnused(counterType);
	return cInvalidUint32;
	}

static void MethodsInit(ThaAttPrbsRegProvider self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, PrbsGenReg);
        mMethodOverride(m_methods, PrbsMonReg);
        mMethodOverride(m_methods, LoPrbsEngineOc48IdShift);
        mMethodOverride(m_methods, LoPrbsEngineOc24SliceIdShift);

        mMethodOverride(m_methods, PrbsDelayIdMask);
        mMethodOverride(m_methods, PrbsDelayEnableMask);
        mMethodOverride(m_methods, PrbsDelayConfigReg);
        mMethodOverride(m_methods, PrbsMaxDelayReg);
        mMethodOverride(m_methods, PrbsMinDelayReg);
        mMethodOverride(m_methods, PrbsModeMask);
        mMethodOverride(m_methods, PrbsStepMask);
        mMethodOverride(m_methods, PrbsEnableMask);
        mMethodOverride(m_methods, PrbsFixPatternMask);
        mMethodOverride(m_methods, PrbsFixPatternReg);
        mMethodOverride(m_methods, PrbsCounterReg);
        mMethodOverride(m_methods, PrbsCounterMask);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaAttPrbsRegProvider);
    }

ThaAttPrbsRegProvider ThaAttPrbsRegProviderObjectInit(ThaAttPrbsRegProvider self, ThaAttModulePrbs prbsModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    m_methodsInit = 1;
    self->prbsModule = prbsModule;
    return self;
    }

ThaAttPrbsRegProvider ThaAttPrbsRegProviderNew(ThaAttModulePrbs prbsModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaAttPrbsRegProvider newProvider = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newProvider == NULL)
        return NULL;

    /* Construct it */
    return ThaAttPrbsRegProviderObjectInit(newProvider, prbsModule);
    }

ThaAttModulePrbs ThaAttPrbsRegProviderPrbsModule(ThaAttPrbsRegProvider self)
    {
    if (self)
        return self->prbsModule;
    return NULL;
    }

uint32 ThaAttPrbsRegProviderPrbsGenCtrlReg(ThaAttPrbsRegProvider self, eBool isHo)
    {
    if (self)
        return mMethodsGet(self)->PrbsGenReg(self, isHo);
    return cInvalidUint32;
    }

uint32 ThaAttPrbsRegProviderPrbsMonCtrlReg(ThaAttPrbsRegProvider self, eBool isHo)
    {
    if (self)
        return mMethodsGet(self)->PrbsMonReg(self, isHo);
    return cInvalidUint32;
    }

uint8 ThaAttPrbsRegProviderLoPrbsEngineOc48IdShift(ThaAttPrbsRegProvider self)
    {
    if (self)
            return mMethodsGet(self)->LoPrbsEngineOc48IdShift(self);
    return cInvalidUint8;
    }

uint8 ThaAttPrbsRegProviderLoPrbsEngineOc24SliceIdShift(ThaAttPrbsRegProvider self)
    {
    if (self)
        return mMethodsGet(self)->LoPrbsEngineOc24SliceIdShift(self);
    return cInvalidUint8;
    }

uint32 ThaAttPrbsRegProviderPrbsFixPatternReg(ThaAttPrbsRegProvider self, eBool isHo, eBool isTx)
	{
	if (self)
		return mMethodsGet(self)->PrbsFixPatternReg(self, isHo, isTx);
	return cInvalidUint32;
	}

uint32 ThaAttPrbsRegProviderPrbsDelayEnableMask(ThaAttPrbsRegProvider self, eBool isHo)
	{
	if (self)
		return mMethodsGet(self)->PrbsDelayEnableMask(self, isHo);
	return cInvalidUint32;
	}

uint32 ThaAttPrbsRegProviderPrbsDelayIdMask(ThaAttPrbsRegProvider self, eBool isHo)
	{
	if (self)
		return mMethodsGet(self)->PrbsDelayIdMask(self, isHo);
	return cInvalidUint32;
	}

uint32 ThaAttPrbsRegProviderPrbsDelayConfigReg(ThaAttPrbsRegProvider self, eBool isHo, uint8 slice)
	{
	if (self)
		return mMethodsGet(self)->PrbsDelayConfigReg(self, isHo, slice);
	return cInvalidUint32;
	}

uint32 ThaAttPrbsRegProviderPrbsMaxDelayReg(ThaAttPrbsRegProvider self, eBool isHo, eBool r2c, uint8 slice)
	{
	if (self)
		return mMethodsGet(self)->PrbsMaxDelayReg(self, isHo, r2c, slice);
	return cInvalidUint32;
	}

uint32 ThaAttPrbsRegProviderPrbsMinDelayReg(ThaAttPrbsRegProvider self, eBool isHo, eBool r2c, uint8 slice)
	{
	if (self)
		return mMethodsGet(self)->PrbsMinDelayReg(self, isHo, r2c, slice);
	return cInvalidUint32;
	}

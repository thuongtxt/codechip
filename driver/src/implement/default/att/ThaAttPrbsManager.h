/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ThaAttPdhManager
 *
 * File        : ThaAttPdhManager.h
 *
 * Created Date: July 26, 2017
 *
 * Author      : chaudpt
 *
 * Description : This file contains common prototypes of ATT PDH Manager of default
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _THAATTPRBSMANAGER_H_
#define _THAATTPRBSMANAGER_H_

/*--------------------------- Include files ----------------------------------*/
#include "AtModulePrbs.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Typedef ----------------------------------------*/
typedef struct tThaAttPrbsManager * ThaAttPrbsManager;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtAttPrbsManager ThaAttPrbsManagerNew(AtModulePrbs prbs);
AtAttPrbsManager Tha6A210021AttPrbsManagerNew(AtModulePrbs prbs);
AtAttPrbsManager Tha6A290022AttPrbsManagerNew(AtModulePrbs prbs);
#ifdef __cplusplus
}
#endif
#endif /* _THAATTPRBSMANAGER_H_ */

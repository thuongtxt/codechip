/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : ThaModulePrbsInternal.h
 * 
 * Created Date: May 8, 2015
 *
 * Description : PRBS module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAATTMODULEPRBSINTERNAL_H_
#define _THAATTMODULEPRBSINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/prbs/AtModulePrbsInternal.h"
#include "ThaAttModulePrbs.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThaModulePrbs(engine)  ThaModulePrbs ThaPrbsEngineModuleGet(engine)

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaAttModulePrbsMethods
    {
    ThaAttPrbsRegProvider (*RegProviderCreate)(ThaAttModulePrbs self);
    uint32 (*StartVersionSupportDelayAverage)(ThaAttModulePrbs self);
    eBool (*NeedDelayAverage)(ThaAttModulePrbs self);
    }tThaAttModulePrbsMethods;

typedef struct tThaAttModulePrbs
    {
    tAtModulePrbs super;
    const tThaAttModulePrbsMethods *methods;
    ThaAttPrbsRegProvider regProvider;
    }tThaAttModulePrbs;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePrbs ThaAttModulePrbsObjectInit(AtModulePrbs self, AtDevice device);

/* Register access */
ThaAttPrbsRegProvider ThaAttModulePrbsRegProvider(ThaAttModulePrbs self);


#endif /* _THAMODULEPRBSINTERNAL_H_ */


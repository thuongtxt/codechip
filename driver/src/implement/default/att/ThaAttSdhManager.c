/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : AtModulePdh.c
 *
 * Created Date: July 26, 2017
 *
 * Author      : chaudpt
 *
 * Description : Default implementation of PDH module of Thalassa device
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../generic/sdh/AtModuleSdhInternal.h"
#include "../../../generic/sdh/AtSdhChannelInternal.h"
#include "../../../generic/att/AtAttWanderChannelInternal.h"
#include "../../../util/coder/AtCoderUtil.h"
#include "../att/ThaAttWanderChannelReg.h"
#include "../att/ThaAttController.h"
#include "ThaAttSdhManagerInternal.h"
#include "../sdh/ThaModuleSdhInternal.h"

/*--------------------------- Define -----------------------------------------*/
int AtCliExecute(const char* str);
const char* AtCliTime2String(tAtOsalCurTime *t);

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaAttSdhManager)self)
#define cAtAttPointerAdjustTimeT0            30000
#define cAtAttPointerAdjustTimeT0Min         30000
#define cAtAttPointerAdjustTimeT0Max         60000
#define cAtAttPointerAdjustTimeT1            10000
#define cAtAttPointerAdjustTimeT1Max         30000
#define cAtAttPointerAdjustTimeT2            1000
#define cAtAttPointerAdjustTimeT2Min         750
#define cAtAttPointerAdjustTimeT2Max         30000
#define cAtAttPointerAdjustTimeT3            5 /* d */
#define cAtAttPointerAdjustTimeT3Min         5
#define cAtAttPointerAdjustTimeT3Max         10000
#define cAtAttPointerAdjustTimeT3Note2       2 /* for a, b, c */
#define cAtAttPointerAdjustTimeT4            2
#define cAtAttPointerAdjustTimeT4Note5       ((float)2.0)
#define cAtAttPointerAdjustTimeT4Note6       2
#define cAtAttPointerAdjustTimeT5            500
#define cAtAttPointerAdjustTimeT5Min         34
#define cAtAttPointerAdjustTimeT5Max         10000
#define cAtAttPointerAdjustTimeT5Note6       5000
#define cAtAttPointerAdjustTimeT5Note6Min    34
#define cAtAttPointerAdjustTimeT5Note6Max    10000
#define cAtAttPointerAdjustPeriod            50
/*--------------------------- Local typedefs ---------------------------------*/
/*static uint32 lock_count = 0;
static uint32 unlock_count = 0;*/
/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaAttSdhManagerMethods m_methods;

/* Override */
static tAtAttSdhManagerMethods m_AtAttSdhManagerOverride;
static tAtObjectMethods        m_AtObjectOverride;

/* Save super implementations */
static const tAtObjectMethods *m_AtObjectMethods = NULL;
static const tAtAttSdhManagerMethods  *m_AtAttSdhManagerMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Internal functions -----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet   Init(AtAttSdhManager self)
    {
    eAtRet ret = m_AtAttSdhManagerMethods->Init(self);

    ret |= ThaAttSdhManagerSohColRemoveSet((ThaAttSdhManager)self, 3);
    ret |= ThaAttSdhManagerCheckForceRateShowSet((ThaAttSdhManager) self,  cAtFalse);
    ret |= ThaAttSdhManagerCheckForceRateEnSet((ThaAttSdhManager) self, cAtTrue);
    ret |= ThaAttSdhManagerAlarmDurationUnitSet((ThaAttSdhManager) self, cAttSdhForceAlarmDurationUnitSecond);
    mThis(self)->g783.debug      = cAtFalse;
    mThis(self)->g783.T0         = cAtAttPointerAdjustTimeT0;
    mThis(self)->g783.T1         = cAtAttPointerAdjustTimeT1;
    mThis(self)->g783.T2         = cAtAttPointerAdjustTimeT2;
    mThis(self)->g783.T3         = cAtAttPointerAdjustTimeT3;
    mThis(self)->g783.T3Note2    = cAtAttPointerAdjustTimeT3Note2;
    mThis(self)->g783.T4         = cAtAttPointerAdjustTimeT4;
    mThis(self)->g783.T4Note5    = cAtAttPointerAdjustTimeT4Note5;
    mThis(self)->g783.T4Note6    = cAtAttPointerAdjustTimeT4Note6;
    mThis(self)->g783.T5         = cAtAttPointerAdjustTimeT5;
    mThis(self)->g783.T5Note6    = cAtAttPointerAdjustTimeT5Note6;
    mThis(self)->g783.period     = cAtAttPointerAdjustPeriod;
    return ret;
    }

/*-----------------------------------------------------------------------*/

static eAtRet SourceMultiplexingResync(ThaAttSdhManager self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet AlarmDurationUnitSet(ThaAttSdhManager self, eAttSdhForceAlarmDurationUnit unit)
    {
    AtUnused(self);
    self->alarmDurationUnit = unit;
    return cAtOk;
    }
static eAttSdhForceAlarmDurationUnit AlarmDurationUnitGet(ThaAttSdhManager self)
    {
    AtUnused(self);
    return self->alarmDurationUnit;
    }

static eAtRet SetResetTimeAllHoChannel(ThaAttSdhManager self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet SetResetTimeAllLoChannel(ThaAttSdhManager self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet DebugHoPointerLooptimeEnable(ThaAttSdhManager self, eBool en)
    {
    AtUnused(self);
    AtUnused(en);
    return cAtOk;
    }

static eAtRet DebugLoPointerLooptimeEnable(ThaAttSdhManager self, eBool en)
    {
    AtUnused(self);
    AtUnused(en);
    return cAtOk;
    }

/*-----------------------------------------------------------------------*/
static eAtRet DebugCheckForceRateShowSet(ThaAttSdhManager self, eBool en)
    {
    mThis(self)->forceRateShowEn = en;
    return cAtOk;
    }

static eBool  DebugCheckForceRateShowGet(ThaAttSdhManager self)
    {
    return mThis(self)->forceRateShowEn;
    }


static eAtRet DebugCheckForceRateEnSet(ThaAttSdhManager self, eBool en)
    {
    mThis(self)->forceRateCheckEn = en;
    return cAtOk;
    }

static eBool  DebugCheckForceRateEnGet(ThaAttSdhManager self)
    {
    return mThis(self)->forceRateCheckEn;
    }

static eAtRet DebugCheckForceRatePecentSet(ThaAttSdhManager self, uint8 percent)
    {
    mThis(self)->forceRateCheckPercent = percent;
    return cAtOk;
    }

static uint8  DebugCheckForceRatePecentGet(ThaAttSdhManager self)
    {
    return mThis(self)->forceRateCheckPercent;
    }

/*-----------------------------------------------------------------------*/

static void ThaAttSdhManagerDeleteForcePointerChannelList(ThaAttSdhManager self)
    {
    if (mThis(self)->sdhPathForcePointer != NULL)
        {
        AtList list = mThis(self)->sdhPathForcePointer;
        uint32 length = AtListLengthGet(list);
        while (length>0)
            {
            tAtAttForcePointer *object = (tAtAttForcePointer*)AtListObjectRemoveAtIndex(list, length-1);
            AtOsalMemFree(object);
            }
        AtObjectDelete((AtObject)mThis(self)->sdhPathForcePointer);
        }
    mThis(self)->sdhPathForcePointer = NULL;
    }

static uint32 OpositePointerType(uint32 pointerType)
    {
    if (pointerType == cAtSdhPathPointerAdjTypeIncrease)
        return cAtSdhPathPointerAdjTypeDecrease;
    return cAtSdhPathPointerAdjTypeIncrease;
    }

static void ForcePointerAdjModeG783a(tAtAttForcePointer *object, tThaAttSdhG783 *g783, const char* str)
    {
    if (object->state == cAtAttForcePointerStateStart)
        {
        AtPrintf("\r\n***Begin forcing %s \r\n", str);
        AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 1);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        AtOsalCurTimeGet(&object->start_time);
        object->state    = cAtAttForcePointerState1stForce;
        object->waitTime = g783->T1;
        object->count    = object->count + 1;

        if (g783->debug)
            {
            AtPrintf("\r\n    -Force %d st %s at %s\r\n", object->count, str, AtCliTime2String(&object->start_time));
            }
        }
    else if (object->state == cAtAttForcePointerState1stForce)
        {
        uint32         TimeMs = 0;
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
        if (TimeMs > (object->waitTime-g783->period))
            {
            AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, OpositePointerType(object->pointerType), 0);
            object->state    = cAtAttForcePointerState1stForceDelay;
            }
        }
    else if (object->state == cAtAttForcePointerState1stForceDelay)
        {
        AtAttControllerForcePointerAdjNumEventSet(object->att, OpositePointerType(object->pointerType), 1);
        AtAttControllerHwForcePointerAdj(object->att, OpositePointerType(object->pointerType));
        object->count    = object->count + 1;
        object->state    = cAtAttForcePointerStateEnd;
        object->waitTime = 300;
        AtOsalCurTimeGet(&object->start_time);
        if (g783->debug) AtPrintf("    -Force %d nd, wait %s at %s\r\n", object->count, str, AtCliTime2String(&object->start_time));
        }
    else if (object->state == cAtAttForcePointerStateEnd)
        {
        uint32         TimeMs = 0;
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
        if (object->waitTime == cInvalidUint32)
            object->state    =  cAtAttForcePointerStateEnd;
        else if (TimeMs > (object->waitTime-g783->period))
            {
            AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
            if (g783->debug) AtPrintf("\r\n    -Force end %s\r\n", str);
            object->count = 0;
            object->waitTime = cInvalidUint32;
            AtOsalCurTimeGet(&object->start_time);
            AtPrintf("\r\n***Forcing complete %s\r\n", str);
            }
        }
    }

static void ForcePointerAdjModeG783b(tAtAttForcePointer *object, tThaAttSdhG783 *g783, const char* str)
    {
    if (object->state == cAtAttForcePointerStateStart)
        {
        AtPrintf("\r\n***Begin forcing %s \r\n",  str);
        AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 1);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        AtOsalCurTimeGet(&object->start_time);
        object->state    = cAtAttForcePointerState1stForce;
        object->waitTime = g783->T2;
        object->count    = object->count + 1;
        if (g783->debug)
            {
            AtPrintf("\r\n");
            AtPrintf("    -Force 1st 1 event %s at %s\r\n", str, AtCliTime2String(&object->start_time));
            }
        }
    else if (object->state == cAtAttForcePointerState1stForce)
        {
        uint32         TimeMs=0;
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
        if (TimeMs > (object->waitTime-g783->period))
            {
            AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
            object->state    = cAtAttForcePointerState1stForceDelay;
            if (g783->debug)AtPrintf("\r\n    ------%s: %s------------\r\n", str, AtCliTime2String(&cur_time));
            }
        }
    else if (object->state == cAtAttForcePointerState1stForceDelay)
        {
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 1);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        AtOsalCurTimeGet(&object->start_time);
        object->waitTime = g783->T2;
        object->state    = cAtAttForcePointerState2ndForce;
        object->count    = object->count + 1;
        if (g783->debug)
            {
            AtPrintf("\r\n    -Force 2nd %s 1 event at %s\r\n", str, AtCliTime2String(&object->start_time));
            }
        }
    else if (object->state == cAtAttForcePointerState2ndForce)
        {
        uint32         TimeMs=0;
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
        if (TimeMs > (object->waitTime-g783->period))
            {
            AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
            object->state    = cAtAttForcePointerState2ndForceDelay;
            if (g783->debug)AtPrintf("\r\n    ------%s: %s------------\r\n", str, AtCliTime2String(&cur_time));
            }
        }
    else if (object->state == cAtAttForcePointerState2ndForceDelay)
        {
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 1);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        AtOsalCurTimeGet(&object->start_time);
        object->waitTime = g783->T2;
        object->state    = cAtAttForcePointerState3rdForce;
        object->count    = object->count + 1;
        if (g783->debug)
            {
            AtPrintf("\r\n    -Force 3rd %s 1 event at %s\r\n", str, AtCliTime2String(&object->start_time));
            }
        }
    else if (object->state == cAtAttForcePointerState3rdForce)
        {
        uint32         TimeMs = 0;
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
        if (TimeMs > (object->waitTime-g783->period))
            {
            AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
            object->state    = cAtAttForcePointerState3rdForceDelay;
            if (g783->debug)AtPrintf("\r\n    ------%s: %s------------\r\n", str, AtCliTime2String(&cur_time));
            }
        }
    else if (object->state == cAtAttForcePointerState3rdForceDelay)
        {
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 1);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        AtOsalCurTimeGet(&object->start_time);
        object->waitTime = g783->T2;
        object->state    = cAtAttForcePointerState4thForce;
        object->count    = object->count + 1;
        if (g783->debug)
            {
            AtPrintf("\r\n    -Force 4th %s 1 event at %s\r\n", str, AtCliTime2String(&object->start_time));
            }
        }
    else if (object->state == cAtAttForcePointerState4thForce)
        {
        uint32         TimeMs = 0;
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
        if (TimeMs > (object->waitTime-g783->period))
            {
            AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
            object->state    = cAtAttForcePointerState4thForceDelay;
            if (g783->debug)AtPrintf("\r\n    ------%s: %s------------\r\n", str, AtCliTime2String(&cur_time));
            }
        }
    else if (object->state == cAtAttForcePointerState4thForceDelay)
        {
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 2);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        AtOsalCurTimeGet(&object->start_time);
        object->waitTime = g783->T2+cAtAttPointerAdjustTimeT3;
        object->state    = cAtAttForcePointerState5thForce;
        object->count    = object->count + 1;
        if (g783->debug)
            {
            AtPrintf("\r\n    -Force 5th (2 events) %s at %s\r\n", str, AtCliTime2String(&object->start_time));
            }
        }
    else if (object->state == cAtAttForcePointerState5thForce)
        {
        uint32         TimeMs = 0;
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
        if (TimeMs > (object->waitTime-g783->period))
            {
            AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
            object->state    = cAtAttForcePointerState5thForceDelay;
            if (g783->debug)AtPrintf("\r\n    ------%s: %s------------\r\n", str, AtCliTime2String(&cur_time));
            }
        }
    else if (object->state == cAtAttForcePointerState5thForceDelay)
        {
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 1);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        object->count    = object->count + 1;
        AtOsalCurTimeGet(&object->start_time);
        object->state    = cAtAttForcePointerStateEnd;
        AtOsalCurTimeGet(&object->start_time);
        if (g783->debug)
            {
            AtPrintf("\r\n    -Force 6th 1 event %s at %s\r\n", str, AtCliTime2String(&object->start_time));
            }
        object->waitTime= g783->T2 + 10;
        }
    else if (object->state == cAtAttForcePointerStateEnd)
        {
        uint32         TimeMs = 0;
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
        if (object->waitTime == cInvalidUint32)
            object->state    =  cAtAttForcePointerStateEnd;
        else if (TimeMs      > (object->waitTime))
            {
            if (g783->debug)AtCliExecute("show sdh path counters tu1x.1.1.1.1.1-1.1.1.1.3 ro");
            AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
            if (g783->debug) AtPrintf("\r\n    -Force end id=%s %s\r\n", str, AtCliTime2String(&object->start_time));
            object->count    = 0;
            object->waitTime = cInvalidUint32;
            object->state    =  cAtAttForcePointerStateEnd;
            AtOsalCurTimeGet(&object->start_time);
            AtPrintf("\r\n***Forcing complete %s\r\n",  str);
            }
        }
    }

static void ForcePointerAdjModeG783c(tAtAttForcePointer *object, tThaAttSdhG783 *g783, const char* str)
    {
    if (object->state == cAtAttForcePointerStateStart)
        {
        AtPrintf("\r\n***Begin forcing %s\r\n", str);
        AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 1);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        AtOsalCurTimeGet(&object->start_time);
        object->state    = cAtAttForcePointerState1stForce;
        object->waitTime = g783->T2;
        object->count    = object->count + 1;
        if (g783->debug) AtPrintf("\r\n    -Force %d st id=%s 1 event at %s\r\n", object->count, str, AtCliTime2String(&object->start_time));
        }
    else if (object->state == cAtAttForcePointerState1stForce)
        {
        uint32         TimeMs=0;
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
        if (TimeMs > (object->waitTime-g783->period))
            {
            if (object->count < 3)
                {
                AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
                object->state    = cAtAttForcePointerState1stForceDelay;
                if (g783->debug) AtPrintf("\r\n 1stForce Dis\r\n");
                }
            else
                {
                object->state    = cAtAttForcePointerState2ndForce;
                AtOsalCurTimeGet(&object->start_time);
                object->waitTime = g783->period;
                }
            }
        }
    else if (object->state == cAtAttForcePointerState1stForceDelay)
        {
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 1);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        AtOsalCurTimeGet(&object->start_time);
        object->waitTime = g783->T2;
        object->state    = cAtAttForcePointerState1stForce;
        object->count    = object->count + 1;
        if (g783->debug) AtPrintf("\r\n    -Force %d nd id=%s  1 event at %s\r\n", object->count, str, AtCliTime2String(&object->start_time));
        }
    else if (object->state == cAtAttForcePointerState2ndForce)
        {
        uint32         TimeMs=0;
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
        if (TimeMs > (object->waitTime-g783->period))
            {
            AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
            object->state    = cAtAttForcePointerState2ndForceDelay;
            if (g783->debug) AtPrintf("\r\n 2ndForce Dis \r\n");
            }
        }
    else if (object->state == cAtAttForcePointerState2ndForceDelay)
        {
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 1);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        AtOsalCurTimeGet(&object->start_time);
        object->waitTime = g783->T2*2+2;
        object->state    = cAtAttForcePointerState3rdForce;
        object->count    = object->count + 1;
        if (g783->debug) AtPrintf("\r\n    -Force %d th and delay longer %s at %s\r\n", object->count, str, AtCliTime2String(&object->start_time));
        }
    else if (object->state == cAtAttForcePointerState3rdForce)
        {
        uint32         TimeMs = 0;
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
        if (TimeMs > (object->waitTime-g783->period))
            {
            AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
            object->state    = cAtAttForcePointerState3rdForceDelay;
            if (g783->debug) AtPrintf("\r\n 3rd Force Dis \r\n");
            }
        }
    else if (object->state == cAtAttForcePointerState3rdForceDelay)
        {
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 1);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        AtOsalCurTimeGet(&object->start_time);
        object->state    = cAtAttForcePointerStateEnd;
        object->count    = object->count + 1;
        if (g783->debug) AtPrintf("\r\n    -Force %d th the last force %s at %s\r\n", object->count, str, AtCliTime2String(&object->start_time));
        AtOsalCurTimeGet(&object->start_time);
        object->waitTime = g783->period;
        }
    else if (object->state == cAtAttForcePointerStateEnd)
        {
        uint32         TimeMs = 0;
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
        if (object->waitTime == cInvalidUint32)
            object->state    =  cAtAttForcePointerStateEnd;
        else if (TimeMs      > (object->waitTime))
            {
            if (g783->debug) AtPrintf("\r\n    -Force --> end %s\r\n", str);
            AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
            object->count    = 0;
            object->waitTime = cInvalidUint32;
            AtOsalCurTimeGet(&object->start_time);
            object->state    =  cAtAttForcePointerStateEnd;
            AtPrintf("\r\n***Forcing complete %s\r\n", str);
            }
        }
    }

static void ForcePointerAdjModeG783d(tAtAttForcePointer *object, tThaAttSdhG783 *g783, const char* str)
    {
    if (object->state == cAtAttForcePointerStateStart)
        {
        AtPrintf("\r\n***Begin forcing %s\r\n", str);
        AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
        AtAttControllerForcePointerAdjStepSet(object->att, object->pointerType, g783->T3*8);
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 2);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        AtOsalCurTimeGet(&object->start_time);
        object->state    = cAtAttForcePointerState1stForce;
        object->waitTime = g783->T1 + g783->T3;
        object->count    = object->count + 1;
        if (g783->debug) AtPrintf("\r\n    -Force %d event in T3=%dms 1st  %s \r\n", 2,  g783->T3 , str);
        }
    else if (object->state == cAtAttForcePointerState1stForce)
        {
        uint32         TimeMs=0;
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
        if (TimeMs > (object->waitTime-g783->period))
            {
            AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, OpositePointerType(object->pointerType), 0);
            object->state    = cAtAttForcePointerState1stForceDelay;
            }
        }
    else if (object->state == cAtAttForcePointerState1stForceDelay)
        {
        AtAttControllerForcePointerAdjStepSet(object->att, OpositePointerType(object->pointerType), g783->T3*8);
        AtAttControllerForcePointerAdjNumEventSet(object->att, OpositePointerType(object->pointerType), 2);
        AtAttControllerHwForcePointerAdj(object->att, OpositePointerType(object->pointerType));
        AtOsalCurTimeGet(&object->start_time);
        object->waitTime = g783->T1 + g783->T3;
        object->state    = cAtAttForcePointerStateEnd;
        object->count    = object->count + 1;
        if (g783->debug) AtPrintf("\r\n    -Force %d event in T3=%dms %s at %s\r\n", 2, g783->T3, str, AtCliTime2String(&object->start_time));
        AtOsalCurTimeGet(&object->start_time);
        object->waitTime = g783->period;
        }
    else if (object->state == cAtAttForcePointerStateEnd)
        {
        uint32         TimeMs = 0;
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
        if (object->waitTime == cInvalidUint32)
            object->state    =  cAtAttForcePointerStateEnd;
        else if (TimeMs      > (object->waitTime))
            {
            AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
            if (g783->debug) AtPrintf("\r\n    -Force --> end\r\n");
            object->count    = 0;
            object->waitTime = cInvalidUint32;
            AtOsalCurTimeGet(&object->start_time);
            AtPrintf("\r\n***Forcing complete\r\n");
            }
        }
    }

static void ForcePointerAdjModeG783e(tAtAttForcePointer *object, tThaAttSdhG783 *g783, const char* str)
    {
    if (object->state == cAtAttForcePointerStateStart)
        {
        AtPrintf("\r\n***Begin forcing %s\r\n", str);
        AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 1);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        AtOsalCurTimeGet(&object->start_time);
        AtOsalCurTimeGet(&object->begin_time);
        object->state    = cAtAttForcePointerState1stForce;
        object->waitTime = g783->T0;
        object->count    = object->count + 1;
        if (g783->debug)
            {
            AtPrintf("\r\n    -Force %d st  wait %d ms \r\n", object->count, object->waitTime);
            }
        }
    else if (object->state == cAtAttForcePointerState1stForce)
        {
        uint32         TimeMs = 0, TimeMs_Begin=0;
        tAtOsalCurTime cur_time;
        uint32 duration= AtAttControllerForcePointerAdjDurationGet(object->att, object->pointerType)*1000UL ;

        AtOsalCurTimeGet(&cur_time);
        TimeMs_Begin = mTimeIntervalInMsGet(object->begin_time, cur_time);
        if (TimeMs_Begin > duration)
            {
            object->state    = cAtAttForcePointerStateEnd;
            object->waitTime = g783->period;
            }
        else
            {
            TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
            if (TimeMs > (object->waitTime-g783->period))
                {
                AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
                object->state    = cAtAttForcePointerState1stForceDelay;
                }
            }
        }
    else if (object->state == cAtAttForcePointerState1stForceDelay)
        {
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 1);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        object->count = object->count + 1;
        object->state    = cAtAttForcePointerState1stForce;
        object->waitTime = g783->T0;
        AtOsalCurTimeGet(&object->start_time);
        if (g783->debug) AtPrintf("\r\n    -Force %d nd, wait %d ms at %s\r\n", object->count, object->waitTime, AtCliTime2String(&object->start_time));
        }
    else if (object->state == cAtAttForcePointerStateEnd)
        {
        uint32         TimeMs = 0;
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
        if (object->waitTime == cInvalidUint32)
            object->state    =  cAtAttForcePointerStateEnd;
        else if (TimeMs      > (object->waitTime))
            {
            AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
            if (g783->debug) AtPrintf("\r\n    -Force end %s at %s\r\n", str, AtCliTime2String(&object->start_time));
            object->count    = 0;
            object->waitTime = cInvalidUint32;
            AtOsalCurTimeGet(&object->start_time);
            AtPrintf("\r\n***Forcing complete %s\r\n", str);
            }
        }
    }

static void ForcePointerAdjModeG783f(tAtAttForcePointer *object, tThaAttSdhG783 *g783, const char* str)
    {
    if (object->state == cAtAttForcePointerStateStart)
        {
        AtPrintf("\r\n***Begin forcing %s \r\n", str);
        AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
        AtAttControllerForcePointerAdjStepSet(object->att, object->pointerType, g783->T4*8);
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 3);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType); /*  Enable force */
        AtOsalCurTimeGet(&object->start_time);
        AtOsalCurTimeGet(&object->begin_time);
        object->state    = cAtAttForcePointerState1stForce;
        object->waitTime = g783->T0;
        object->count    = object->count + 1;
        if (g783->debug)
            {
            AtPrintf("\r\n    -Force %d st %s at %s\r\n", object->count, str, AtCliTime2String(&object->start_time));
            }
        }
    else if (object->state == cAtAttForcePointerState1stForce) /* State delay HW detect status change */
        {
        uint32         TimeMs = 0, TimeMs_Begin=0;
        tAtOsalCurTime cur_time;
        uint32 duration= AtAttControllerForcePointerAdjDurationGet(object->att, object->pointerType)*1000UL;
        AtOsalCurTimeGet(&cur_time);
        TimeMs_Begin = mTimeIntervalInMsGet(object->begin_time, cur_time);
        if (TimeMs_Begin > duration)
            {
            object->state    = cAtAttForcePointerStateEnd;
            object->waitTime = g783->period;
            }
        else
            {
            TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
            if (TimeMs > (g783->T0*object->count))
                {
                AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
                object->state    = cAtAttForcePointerState1stForceDelay;
                }
            }
        }
    else if (object->state == cAtAttForcePointerState1stForceDelay)
        {
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        AtAttControllerForcePointerAdjStepSet(object->att, object->pointerType, g783->T4*8);
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 3);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        object->count    = object->count + 1;
        object->waitTime = g783->T0;
        object->state    = cAtAttForcePointerState1stForce;
        if (g783->debug)
            AtPrintf("\r\n    -(1)Force %d nd %s  3-events at %s\r\n", object->count, str, AtCliTime2String(&cur_time));
        /*AtOsalCurTimeGet(&object->start_time);*/
        }
    else if (object->state == cAtAttForcePointerStateEnd)
        {
        uint32         TimeMs = 0;
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
        if (object->waitTime == cInvalidUint32)
            object->state    =  cAtAttForcePointerStateEnd;
        else if (TimeMs      > (object->waitTime))
            {
            AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
            if (g783->debug)
                AtPrintf("\r\n    -Force end %s at %s \r\n", str, AtCliTime2String(&cur_time));
            object->count    = 0;
            AtOsalCurTimeGet(&object->start_time);
            object->state    =  cAtAttForcePointerStateEnd;
            object->waitTime = cInvalidUint32;
            AtPrintf("\r\n***Forcing complete %s\r\n", str);
            }
        }
    }

/* G for only AU3  */
static void ForcePointerAdjModeG783gPart1(tAtAttForcePointer *object, tThaAttSdhG783 *g783, const char* str)
    {
    if (object->state == cAtAttForcePointerStateStart)
        {
        AtOsalCurTimeGet(&object->begin_time);
        AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 1);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        AtOsalCurTimeGet(&object->start_time);
        object->state    = cAtAttForcePointerState1stForce;
        object->waitTime = g783->T0;
        object->count    = object->count + 1;
        AtPrintf("\r\n***Begin forcing\r\n");
        if (g783->debug)
            {
            AtPrintf("\r\n    -Force %d st at %s\r\n", object->count, AtCliTime2String(&object->start_time));
            }
        }
    else if (object->state == cAtAttForcePointerState1stForce)
        {
        uint32         TimeMs = 0;
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
        if (TimeMs > (object->waitTime-g783->period))
            {
            object->count    = 0;
            AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
            object->state    = cAtAttForcePointerState1stForceDelay;
            }
        }
    else if (object->state == cAtAttForcePointerState1stForceDelay)
        {
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 1);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        AtOsalCurTimeGet(&object->start_time);
        object->state    = cAtAttForcePointerState1stForce;
        object->waitTime = g783->T0;
        object->count    = object->count + 1;
        if (g783->debug)
            AtPrintf("\r\n    -Force %d st 1 event for %s at %s\r\n", object->count, str, AtCliTime2String(&cur_time));

        }
    else if (object->state == cAtAttForcePointerState2ndForce)
        {
        uint32         TimeMs = 0;
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
        if (TimeMs > (object->waitTime-g783->period))
            {
            if (object->count < 87)
                {
                AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
                object->state    = cAtAttForcePointerState2ndForceDelay;
                }
            else
                {
                object->state    = cAtAttForcePointerState1stForce;
                object->count    = 0;
                AtOsalCurTimeGet(&object->start_time);
                object->waitTime = g783->T0; /* Nghi 30s truoc khi thoat*/
                }
            }
        }
    else if (object->state == cAtAttForcePointerState2ndForceDelay)
        {
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 3);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        object->waitTime = g783->T0;
        object->count    = object->count + 1;
        object->state    = cAtAttForcePointerState2ndForce;
        if (g783->debug) AtPrintf("\r\n    -Force %d nd 3 event id=%s at %s\r\n", object->count, str, AtCliTime2String(&cur_time));
        AtOsalCurTimeGet(&object->start_time);
        }
    else if (object->state == cAtAttForcePointerStateEnd)
        {
        uint32         TimeMs = 0;
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
        if (object->waitTime == cInvalidUint32)
            object->state    =  cAtAttForcePointerStateEnd;
        else if (TimeMs      >  (object->waitTime))
            {
            AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
            if (g783->debug) AtPrintf("\r\n -Force end at %s\r\n", AtCliTime2String(&cur_time));
            object->count    = 0;
            AtOsalCurTimeGet(&object->start_time);
            object->waitTime = cInvalidUint32;
            AtPrintf("\r\n***Forcing complete %s\r\n", str);
            }
        }
    }

static void ForcePointerAdjModeG783gPart2(tAtAttForcePointer *object, tThaAttSdhG783 *g783, const char* str)
    {
    if (object->state == cAtAttForcePointerStateStart)
        {
        AtPrintf("\r\n***Begin forcing %s\r\n", str);
        AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 1);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        AtOsalCurTimeGet(&object->begin_time);
        AtOsalCurTimeGet(&object->start_time);
        object->state    = cAtAttForcePointerState2ndForce;
        object->waitTime = g783->T5;
        object->count    = object->count + 1;
        if (g783->debug)
            {
            AtPrintf("\r\n    -(1)Force %d st 1 event %s at %s\r\n", object->count, str, AtCliTime2String(&object->start_time));
            }
        }
    else if (object->state == cAtAttForcePointerState1stForce)
        {
        uint32         TimeMs_Begin;
        uint32 duration= AtAttControllerForcePointerAdjDurationGet(object->att, object->pointerType)*1000UL;
        tAtOsalCurTime cur_time;

        AtOsalCurTimeGet(&cur_time);
        TimeMs_Begin = mTimeIntervalInMsGet(object->begin_time, cur_time);

        if (TimeMs_Begin > (duration + duration/45000))
            {
            object->state    = cAtAttForcePointerStateEnd;
            object->waitTime = g783->period;
            }
        else
            {
            uint32         TimeMs = 0;
            TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
            if (TimeMs > (object->count*g783->T5))
                {
                object->count = 0;
                AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
                object->state    = cAtAttForcePointerState1stForceDelay;
                }
            }
        }
    else if (object->state == cAtAttForcePointerState1stForceDelay)
        {
        tAtOsalCurTime cur_time;

        AtOsalCurTimeGet(&cur_time);
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 1);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        AtOsalCurTimeGet(&object->start_time);
        object->state    = cAtAttForcePointerState2ndForce;
        object->waitTime = g783->T5;
        object->count    = object->count + 1;
        if (g783->debug)
            AtPrintf("\r\n    -(1)Force %d st 1 event for %s at %s\r\n", object->count,str, AtCliTime2String(&cur_time));
        }
    else if (object->state == cAtAttForcePointerState2ndForce)
        {
        uint32         TimeMs_Begin;
        uint32 duration= AtAttControllerForcePointerAdjDurationGet(object->att, object->pointerType)*1000UL+40UL;
        tAtOsalCurTime cur_time;

        AtOsalCurTimeGet(&cur_time);
        TimeMs_Begin = mTimeIntervalInMsGet(object->begin_time, cur_time);
        if (TimeMs_Begin > (duration + (duration/45000)*g783->period))
            {
            object->state    = cAtAttForcePointerStateEnd;
            object->waitTime = g783->period;
            }
        else
            {
            uint32 TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
            if (TimeMs > (object->count*g783->T5))
                {
                if (object->count < 87)
                    {
                    AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
                    object->state = cAtAttForcePointerState2ndForceDelay;
                    }
                else
                    {
                    object->state    = cAtAttForcePointerState3rdForce;
                    object->waitTime = g783->T5; /* Nghi 1.5s truoc khi ngung force */
                    /*AtOsalCurTimeGet(&object->start_time);*/
                    object->waitTime = g783->period;
                    }
                }
            }
        }
    else if (object->state == cAtAttForcePointerState2ndForceDelay)
        {
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 1);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        object->count    = object->count + 1;
        object->state    = cAtAttForcePointerState2ndForce;
        if (g783->debug) AtPrintf("\r\n    -(2)Force %d nd 1 event for %s at %s\r\n", object->count, str, AtCliTime2String(&cur_time));
        object->waitTime = g783->T5;
        }
    else if (object->state == cAtAttForcePointerState3rdForce)
        {
        uint32         TimeMs_Begin;
        uint32 duration = AtAttControllerForcePointerAdjDurationGet(object->att, object->pointerType)*1000UL;
        tAtOsalCurTime    cur_time;

        AtOsalCurTimeGet(&cur_time);
        TimeMs_Begin = mTimeIntervalInMsGet(object->begin_time, cur_time);
        if (TimeMs_Begin > (duration + (duration/45000)*g783->period))
            {
            object->state    = cAtAttForcePointerStateEnd;
            object->waitTime = g783->period;
            }
        else
            {
            uint32 TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
            if (TimeMs > (object->count*g783->T5))
                {
                if (object->count < 90)
                    {
                    AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
                    if (g783->debug) AtPrintf("\r\n    -(3)No adjustment %d th for %s at %s\r\n", object->count, str, AtCliTime2String(&cur_time));
                    object->count    = object->count + 1;
                    object->waitTime = g783->T5;
                    }
                else
                    {
                    object->state    = cAtAttForcePointerState1stForce;
                    object->count    = 0;
                    /*AtOsalCurTimeGet(&object->start_time);*/
                    object->waitTime = g783->T5;
                    }
                }
            }
        }
    else if (object->state == cAtAttForcePointerStateEnd)
        {
        uint32         TimeMs = 0;
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
        if (object->waitTime == cInvalidUint32)
            object->state    =  cAtAttForcePointerStateEnd;
        else if (TimeMs      >  (object->waitTime))
            {
            AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
            if (g783->debug) AtPrintf("\r\n -Force end %s at %s\r\n", str, AtCliTime2String(&cur_time));
            object->count    = 0;
            AtOsalCurTimeGet(&object->start_time);
            object->waitTime = cInvalidUint32;
            AtPrintf("\r\n***Forcing complete %s\r\n", str);
            }
        }
    }

static uint32 G783gStep(AtAttController att)
    {
    AtSdhChannel channel =(AtSdhChannel) AtAttControllerChannelGet(att);
    if (AtSdhChannelIsHoVc(channel))
        return 4;
    return 16;
    }

static void ForcePointerAdjModeG783gPart3(tAtAttForcePointer *object, tThaAttSdhG783 *g783, const char* str)
    {
    if (object->state == cAtAttForcePointerStateStart)
        {
        AtPrintf("\r\n***Begin forcing %s\r\n", str);
        AtOsalCurTimeGet(&object->begin_time);
        AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
        AtAttControllerForcePointerAdjStepSet(object->att, object->pointerType, 1);
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 1);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        AtOsalCurTimeGet(&object->start_time);
        object->state    = cAtAttForcePointerState2ndForce;
        object->waitTime = g783->T5;
        object->count    = object->count + 1;
        if (g783->debug)
            AtPrintf("\r\n    -(1)Force %d st %s at %s\r\n", object->count, str, AtCliTime2String(&object->start_time));
        }
    else if (object->state == cAtAttForcePointerState1stForce)
        {
        uint32         TimeMs_Begin;
        uint32 duration = AtAttControllerForcePointerAdjDurationGet(object->att, object->pointerType)*1000UL
                        + g783->period;
        tAtOsalCurTime cur_time;

        AtOsalCurTimeGet(&cur_time);
        TimeMs_Begin = mTimeIntervalInMsGet(object->begin_time, cur_time);
        if (TimeMs_Begin > duration + duration/45000)
            {
            object->state    = cAtAttForcePointerStateEnd;
            object->waitTime = g783->period;
            }
        else
            {
            uint32         TimeMs = 0;
            TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
            if (TimeMs > (object->waitTime-(2*g783->period)-11))
                {
                object->count = 0;
                AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
                object->state    = cAtAttForcePointerState1stForceDelay;
                }
            }
        }
    else if (object->state == cAtAttForcePointerState1stForceDelay)
        {
        tAtOsalCurTime cur_time;

        AtOsalCurTimeGet(&cur_time);
        AtAttControllerForcePointerAdjStepSet(object->att, object->pointerType, 1);
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 1);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        AtOsalCurTimeGet(&object->start_time);
        object->state    = cAtAttForcePointerState2ndForce;
        object->waitTime = g783->T5;
        object->count    = object->count + 1;
        if (g783->debug)
            AtPrintf("\r\n    -(1)Force %d st %s at %s\r\n", object->count, str, AtCliTime2String(&cur_time));
        }
    else if (object->state == cAtAttForcePointerState2ndForce) /* 42 events at index 1-42 */
        {
        uint32 TimeMs_Begin;
        uint32 duration= AtAttControllerForcePointerAdjDurationGet(object->att, object->pointerType)*1000UL;
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        TimeMs_Begin = mTimeIntervalInMsGet(object->begin_time, cur_time);
        if (TimeMs_Begin > (duration+ (duration/45000)*g783->period))
            {
            object->state    = cAtAttForcePointerStateEnd;
            object->waitTime = g783->period;
            }
        else
            {
            uint32         TimeMs = 0;
            TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
            if (TimeMs > (object->count*g783->T5))
                {
                if (object->count < 42)
                    {
                    AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
                    object->state    = cAtAttForcePointerState2ndForceDelay;
                    }
                else
                    {
                    object->state    = cAtAttForcePointerState3rdForce;
                    /*AtOsalCurTimeGet(&object->start_time);*/
                    object->waitTime = g783->T5;
                    }
                }
            }
        }
    else if (object->state == cAtAttForcePointerState2ndForceDelay)
        {
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        AtAttControllerForcePointerAdjStepSet(object->att, object->pointerType, 1);
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 1);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        object->count ++;
        object->state    = cAtAttForcePointerState2ndForce;
        if (g783->debug)
            AtPrintf("\r\n    -(2)Force %d nd 1 event %s at %s\r\n", object->count, str, AtCliTime2String(&cur_time));
        object->waitTime = g783->T5;
        /*AtOsalCurTimeGet(&object->start_time);*/
        }
    else if (object->state == cAtAttForcePointerState3rdForce) /* Force 2 event at index 43 */
        {
        uint32 TimeMs_Begin;
        uint32 duration = AtAttControllerForcePointerAdjDurationGet(object->att, object->pointerType)*1000UL;
        tAtOsalCurTime cur_time;

        AtOsalCurTimeGet(&cur_time);
        TimeMs_Begin = mTimeIntervalInMsGet(object->begin_time, cur_time);
        if (TimeMs_Begin > (duration + (duration/45000)*g783->period))
            {
            object->state    = cAtAttForcePointerStateEnd;
            object->waitTime = g783->period;
            }
        else
            {
            uint32         TimeMs = 0;
            TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
            if (TimeMs > (object->count*g783->T5))
                {
                if (object->count < 43)
                    {
                    AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
                    object->state    = cAtAttForcePointerState3rdForceDelay;
                    }
                else
                    {
                    /*AtOsalCurTimeGet(&object->start_time);*/
                    object->state = cAtAttForcePointerState4thForce;
                    object->waitTime = g783->period;
                    }
                }
            }
        }
    else if (object->state == cAtAttForcePointerState3rdForceDelay)
        {
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        AtAttControllerForcePointerAdjStepSet(object->att, object->pointerType, G783gStep(object->att));
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 2);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        object->count ++;
        if (g783->debug)
            AtPrintf("\r\n    -Force %d nd 2 events %s at %s \r\n", object->count, str, AtCliTime2String(&cur_time));
        object->state    = cAtAttForcePointerState4thForce;
        object->waitTime = g783->T5;
        /*AtOsalCurTimeGet(&object->start_time);*/
        }
    else if (object->state == cAtAttForcePointerState4thForce)/* Force next 44 event at index 44-87 */
        {
        uint32         TimeMs_Begin = 0;
        uint32         duration     = AtAttControllerForcePointerAdjDurationGet(object->att, object->pointerType)*1000UL;
        tAtOsalCurTime cur_time;

        AtOsalCurTimeGet(&cur_time);
        TimeMs_Begin = mTimeIntervalInMsGet(object->begin_time, cur_time);
        if (TimeMs_Begin > duration)
            {
            /*AtOsalCurTimeGet(&object->start_time);*/
            object->state    = cAtAttForcePointerStateEnd;
            object->waitTime = g783->period;
            }
        else
            {
            uint32 TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
            if (TimeMs > (object->count*g783->T5))
                {
                if (object->count < 87)
                    {
                    AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
                    object->state    = cAtAttForcePointerState4thForceDelay;
                    }
                else
                    {
                    /*AtOsalCurTimeGet(&object->start_time);*/
                    object->state    = cAtAttForcePointerState5thForce;
                    object->waitTime = g783->T5; /* Nghi 15s truoc khi ngung force */
                    }
                }
            }
        }
    else if (object->state == cAtAttForcePointerState4thForceDelay)
        {
        tAtOsalCurTime cur_time;

        AtOsalCurTimeGet(&cur_time);
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 1);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        object->count    = object->count + 1;
        object->state    = cAtAttForcePointerState4thForce;
        object->waitTime = g783->T5;
        if (g783->debug)
            AtPrintf("\r\n    -(4)Force %d nd 1 event %s at %s \r\n", object->count, str, AtCliTime2String(&cur_time));
        /*AtOsalCurTimeGet(&object->start_time);*/
        }
    else if (object->state == cAtAttForcePointerState5thForce)
        {
        uint32         TimeMs_Begin = 0;
        uint32         duration     = AtAttControllerForcePointerAdjDurationGet(object->att, object->pointerType)*1000UL;
        tAtOsalCurTime cur_time;


        AtOsalCurTimeGet(&cur_time);
        TimeMs_Begin = mTimeIntervalInMsGet(object->begin_time, cur_time);
        if (TimeMs_Begin > (duration+ (duration/45000)*g783->period))
            {
            /*AtOsalCurTimeGet(&object->start_time);*/
            object->state    = cAtAttForcePointerStateEnd;
            object->waitTime = g783->period;
            }
        else
            {
            uint32 TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
            if (TimeMs > (object->count*g783->T5))
                {
                if (object->count < 90)
                    {
                    /*AtOsalCurTimeGet(&object->start_time);*/
                    object->waitTime = g783->T5;
                    object->count    = object->count + 1;
                    if (g783->debug)
                        AtPrintf("\r\n    -(5)No adjustment %d th id=%s at %s\r\n", object->count, str, AtCliTime2String(&cur_time));
                    }
                else
                    {
                    AtOsalCurTimeGet(&object->start_time);
                    object->waitTime = g783->T5;
                    object->state    = cAtAttForcePointerState1stForce;
                    object->count = 0;
                    if (g783->debug)
                        AtPrintf("\r\n    -----Next Period for %s at %s\r\n", str, AtCliTime2String(&cur_time));
                    }
                }
            }
        }
    else if (object->state == cAtAttForcePointerStateEnd)
        {
        uint32         TimeMs = 0;
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
        if (object->waitTime == cInvalidUint32)
            object->state    =  cAtAttForcePointerStateEnd;
        else if (TimeMs  >  (object->waitTime))
            {
            AtOsalCurTimeGet(&object->start_time);
            AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
            if (g783->debug)
                AtPrintf("\r\n -Force end for %s at %s \r\n", str, AtCliTime2String(&cur_time));
            object->count    = 0;
            object->waitTime = cInvalidUint32;
            AtPrintf("\r\n***Forcing complete %s\r\n", str);
            }
        }
    }

static void ForcePointerAdjModeG783gPart4(tAtAttForcePointer *object, tThaAttSdhG783 *g783, const char* str)
    {
    if (object->state == cAtAttForcePointerStateStart)
        {
        AtOsalCurTimeGet(&object->begin_time);
        AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 1);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        AtOsalCurTimeGet(&object->start_time);
        object->state    = cAtAttForcePointerState2ndForce;
        object->waitTime = g783->T5;
        object->count    = object->count + 1;
        AtPrintf("\r\n***Begin forcing %s\r\n", str);
        if (g783->debug)
            AtPrintf("\r\n    -(1)Force %d st for %s at %s \r\n", object->count, str, AtCliTime2String(&object->start_time));
        }
    else if (object->state == cAtAttForcePointerState1stForce)
        {
        uint32 TimeMs_Begin;
        uint32 duration= AtAttControllerForcePointerAdjDurationGet(object->att, object->pointerType)*1000UL+40UL;
        tAtOsalCurTime cur_time;

        AtOsalCurTimeGet(&cur_time);
        TimeMs_Begin = mTimeIntervalInMsGet(object->begin_time, cur_time);
        if (TimeMs_Begin > duration)
            {
            AtOsalCurTimeGet(&object->start_time);
            object->state    = cAtAttForcePointerStateEnd;
            object->waitTime = g783->period;
            }
        else
            {
            uint32         TimeMs = 0;
            TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
            if (TimeMs > (object->waitTime-g783->period))
                {
                AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
                object->state    = cAtAttForcePointerState1stForceDelay;
                }
            }
        }
    else if (object->state == cAtAttForcePointerState1stForceDelay)
        {
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 1);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        AtOsalCurTimeGet(&object->start_time);
        object->state    = cAtAttForcePointerState2ndForce;
        object->waitTime = g783->T5;
        object->count    = object->count + 1;
        if (g783->debug)
            AtPrintf("\r\n    -(1)Force %d st %s at %s\r\n", object->count, str, AtCliTime2String(&cur_time));
        }
    else if (object->state == cAtAttForcePointerState2ndForce)
        {
        uint32         TimeMs_Begin;
        tAtOsalCurTime cur_time;
        uint32 duration= AtAttControllerForcePointerAdjDurationGet(object->att, object->pointerType)*1000UL+40UL;

        AtOsalCurTimeGet(&cur_time);
        TimeMs_Begin = mTimeIntervalInMsGet(object->begin_time, cur_time);
        if (TimeMs_Begin > duration)
            {
            /*AtOsalCurTimeGet(&object->start_time);*/
            object->state    = cAtAttForcePointerStateEnd;
            object->waitTime = g783->period;
            }
        else
            {
            uint32 TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
            if (TimeMs > (object->count*g783->T5))
                {
                if (object->count < 86)
                    {
                    AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
                    object->state    = cAtAttForcePointerState2ndForceDelay;
                    }
                else
                    {
                    object->state    = cAtAttForcePointerState3rdForce;
                    /*AtOsalCurTimeGet(&object->start_time);*/
                    object->waitTime = g783->T5; /* Nghi 20s truoc khi ngung force */
                    }
                }
            }
        }
    else if (object->state == cAtAttForcePointerState2ndForceDelay)
        {
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 1);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        object->count    = object->count + 1;
        object->state    = cAtAttForcePointerState2ndForce;
        object->waitTime = g783->T5;
        if (g783->debug)
            AtPrintf("\r\n    -(State: 2) Force %d nd for %s at %s\r\n", object->count, str, AtCliTime2String(&cur_time));
        }
    else if (object->state == cAtAttForcePointerState3rdForce)
        {
        uint32         TimeMs_Begin;
        tAtOsalCurTime cur_time;
        uint32 duration = AtAttControllerForcePointerAdjDurationGet(object->att, object->pointerType)*1000UL +
                          g783->period;

        AtOsalCurTimeGet(&cur_time);
        TimeMs_Begin = mTimeIntervalInMsGet(object->begin_time, cur_time);
        if (TimeMs_Begin > duration)
            {
            /*AtOsalCurTimeGet(&object->start_time);*/
            object->state    = cAtAttForcePointerStateEnd;
            object->waitTime = g783->period;
            }
        else
            {
            uint32 TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
            if (TimeMs > (object->count*g783->T5))
                {
                if (object->count < 90)
                    {
                    object->count ++;
                    object->waitTime = g783->T5;
                    if (g783->debug) AtPrintf("\r\n    -(State:3) No adjustment %d th for %s at %s \r\n", object->count, str , AtCliTime2String(&cur_time));
                    }
                else
                    {
                    object->count = 0;
                    object->state    = cAtAttForcePointerState1stForce;
                    object->waitTime = g783->T5;
                    }
                }
            }
        }
    else if (object->state == cAtAttForcePointerStateEnd)
        {
        uint32         TimeMs = 0;
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
        if (object->waitTime == cInvalidUint32)
            object->state    =  cAtAttForcePointerStateEnd;
        else if (TimeMs  >  (object->waitTime))
            {
            AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
            if (g783->debug) AtPrintf("\r\n -Force end for %s at %s\r\n", str, AtCliTime2String(&cur_time));
            object->count    = 0;
            AtOsalCurTimeGet(&object->start_time);
            object->waitTime = cInvalidUint32;
            AtPrintf("\r\n***Forcing complete %s\r\n", str);
            }
        }
    }

static void ForcePointerAdjModeG783hParta(tAtAttForcePointer *object, tThaAttSdhG783 *g783, const char* str)
    {
    if (object->state == cAtAttForcePointerStateStart)
        {
        AtPrintf("\r\n***Begin forcing %s\r\n", str);
        AtOsalCurTimeGet(&object->begin_time);
        AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 1);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        AtOsalCurTimeGet(&object->start_time);
        object->state    = cAtAttForcePointerState2ndForce;
        object->count    = object->count + 1;
        object->waitTime = g783->T5;
        if (g783->debug)
            AtPrintf("\r\n    -1st Force %d st for %s at %s\r\n", object->count, str, AtCliTime2String(&object->start_time));
        }
    else if (object->state == cAtAttForcePointerState1stForce)
        {
        uint32         TimeMs_Begin;
        tAtOsalCurTime cur_time;
        uint32 duration = AtAttControllerForcePointerAdjDurationGet(object->att, object->pointerType)*1000UL;

        AtOsalCurTimeGet(&cur_time);
        TimeMs_Begin = mTimeIntervalInMsGet(object->begin_time, cur_time);
        if (TimeMs_Begin > (duration + g783->period))
            {
            object->state    = cAtAttForcePointerStateEnd;
            object->waitTime = g783->period;
            }
        else
            {
            uint32         TimeMs = 0;
            TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
            if (TimeMs > (object->count*g783->T5))
                {
                AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
                object->waitTime = g783->T5;

                }
            }
        }
    else if (object->state == cAtAttForcePointerState1stForceDelay)
        {
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 1);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        object->count    = object->count + 1;
        object->state    = cAtAttForcePointerState1stForce;
        if (g783->debug)
            AtPrintf("\r\n    -Force %d nd delay %d at %s\r\n", object->count, g783->T5, AtCliTime2String(&cur_time));
        }
    else if (object->state == cAtAttForcePointerStateEnd)
        {
        uint32         TimeMs = 0;
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
        if (object->waitTime == cInvalidUint32)
            object->state    =  cAtAttForcePointerStateEnd;
        else if (TimeMs      >  (object->waitTime))
            {
            AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
            object->count    = 0;
            object->waitTime = cInvalidUint32;
            if (g783->debug) AtPrintf("\r\n -Force end for %s at %s\r\n", str,  AtCliTime2String(&cur_time));
            AtPrintf("\r\n***Forcing complete %s\r\n", str);
            }
        }
    }
static void ForcePointerAdjModeG783hPartb(tAtAttForcePointer *object, tThaAttSdhG783 *g783, const char* str)
    {
    if (object->state == cAtAttForcePointerStateStart)
        {
        AtPrintf("\r\n***Begin forcing %s\r\n", str);
        AtOsalCurTimeGet(&object->begin_time);
        AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 1);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        AtOsalCurTimeGet(&object->start_time);
        object->state    = cAtAttForcePointerState1stForce;
        object->waitTime = g783->T5;
        object->count    = object->count + 1;
        if (g783->debug)
            AtPrintf("\r\n    -Force %d st %s at %s\r\n", object->count, str, AtCliTime2String(&object->start_time));
        }
    else if (object->state == cAtAttForcePointerState1stForce)
        {
        uint32         TimeMs_Begin;
        tAtOsalCurTime cur_time;
        uint32 duration= AtAttControllerForcePointerAdjDurationGet(object->att, object->pointerType)*1000UL+40UL;

        AtOsalCurTimeGet(&cur_time);
        TimeMs_Begin = mTimeIntervalInMsGet(object->begin_time, cur_time);
        if (TimeMs_Begin > duration)
            {
            /*AtOsalCurTimeGet(&object->start_time);*/
            object->state    = cAtAttForcePointerStateEnd;
            object->waitTime = g783->period;
            }
        else
            {
            uint32         TimeMs = 0;
            TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
            if (TimeMs > (object->count*g783->T5))
                {
                AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
                AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 2);
                AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
                object->count    = object->count + 1;
                object->state    = cAtAttForcePointerState1stForce;
                if (g783->debug)
                    AtPrintf("\r\n    -Force %d nd %s at %s\r\n", object->count, str, AtCliTime2String(&cur_time));
                object->waitTime = g783->T5;
                /*AtOsalCurTimeGet(&object->start_time);*/
                }
            }
        }
    else if (object->state == cAtAttForcePointerStateEnd)
        {
        uint32         TimeMs = 0;
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
        if (object->waitTime == cInvalidUint32)
            object->state    =  cAtAttForcePointerStateEnd;
        else if (TimeMs      >  (object->waitTime))
            {
            AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
            if (g783->debug) AtPrintf("\r\n    -Force end %s at %s\r\n", str, AtCliTime2String(&cur_time));
            object->count    = 0;
            /*AtOsalCurTimeGet(&object->start_time);*/
            object->waitTime = cInvalidUint32;
            AtPrintf("\r\n***Forcing complete %s\r\n", str);
            }
        }
    }
static void ForcePointerAdjModeG783hPartc(tAtAttForcePointer *object, tThaAttSdhG783 *g783, const char* str)
    {
    if (object->state == cAtAttForcePointerStateStart)
        {
        AtOsalCurTimeGet(&object->begin_time);
        AtPrintf("\r\n***Begin forcing %s\r\n", str);
        AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
        AtAttControllerForcePointerAdjStepSet(object->att, object->pointerType, G783gStep(object->att));
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 2);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        object->state    = cAtAttForcePointerState1stForce;
        object->count    = object->count + 1;
        AtOsalCurTimeGet(&object->start_time);
        object->waitTime = g783->T5;
        if (g783->debug)
            AtPrintf("\r\n    -Force %d st %s at %s\r\n", object->count, str, AtCliTime2String(&object->start_time));
        }
    else if (object->state == cAtAttForcePointerState1stForce)
        {
        uint32         TimeMs_Begin;
        tAtOsalCurTime cur_time;
        uint32 duration= AtAttControllerForcePointerAdjDurationGet(object->att, object->pointerType)*1000UL+40UL;

        AtOsalCurTimeGet(&cur_time);
        TimeMs_Begin = mTimeIntervalInMsGet(object->begin_time, cur_time);
        if (TimeMs_Begin > duration)
            {
            object->state    = cAtAttForcePointerStateEnd;
            object->waitTime = g783->period;
            }
        else
            {
            uint32         TimeMs = 0;
            TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
            if (TimeMs > (object->count*g783->T5))
                {
                AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
                object->state    = cAtAttForcePointerState1stForceDelay;
                object->waitTime = g783->T5;
                }
            }
        }
    else if (object->state == cAtAttForcePointerState1stForceDelay)
        {
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        AtAttControllerForcePointerAdjStepSet(object->att, object->pointerType,1);
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 1);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        object->count    = object->count + 1;
        object->state    = cAtAttForcePointerState1stForce;
        if (g783->debug)
            AtPrintf("\r\n    -Force %d nd 1 event for %s at %s\r\n", object->count, str, AtCliTime2String(&cur_time));
        object->waitTime = g783->T5;
        }
    else if (object->state == cAtAttForcePointerStateEnd)
        {
        uint32         TimeMs = 0;
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
        if (object->waitTime == cInvalidUint32)
            object->state    =  cAtAttForcePointerStateEnd;
        else if (TimeMs >  (object->waitTime))
            {
            AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
            if (g783->debug) AtPrintf("\r\n -Force end %s at %s \r\n", str, AtCliTime2String(&cur_time));
            object->count    = 0;
            AtOsalCurTimeGet(&object->start_time);
            object->waitTime = cInvalidUint32;
            AtPrintf("\r\n***Forcing complete %s\r\n", str);
            }
        }
    }

static void ForcePointerAdjModeG783hPartd(tAtAttForcePointer *object, tThaAttSdhG783 *g783, const char* str)
    {
    if (object->state == cAtAttForcePointerStateStart)
        {
        AtOsalCurTimeGet(&object->begin_time);
        AtPrintf("\r\n***Begin forcing %s\r\n", str);
        AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 1);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        object->state    = cAtAttForcePointerState1stForce;
        object->count    = object->count + 1;
        AtOsalCurTimeGet(&object->start_time);
        if (g783->debug)
            AtPrintf("\r\n    -Force %d st %s at %s\r\n", object->count, str,AtCliTime2String(&object->start_time));
        object->waitTime = g783->T5;

        }
    else if (object->state == cAtAttForcePointerState1stForce)
        {
        uint32         TimeMs_Begin;
        tAtOsalCurTime cur_time;
        uint32 duration= AtAttControllerForcePointerAdjDurationGet(object->att, object->pointerType)*1000UL;

        AtOsalCurTimeGet(&cur_time);
        TimeMs_Begin = mTimeIntervalInMsGet(object->begin_time, cur_time);
        if (TimeMs_Begin > (duration + g783->period))
            {
            object->state    = cAtAttForcePointerStateEnd;
            object->waitTime = g783->period;
            }
        else
            {
            uint32         TimeMs = 0;
            TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
            if (TimeMs > (object->count*g783->T5))
                {
                AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
                object->state    = cAtAttForcePointerState1stForceDelay;
                }
            }
        }
    else if (object->state == cAtAttForcePointerState1stForceDelay)
        {
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 1);
        AtAttControllerForcePointerAdj           (object->att, object->pointerType);
        object->count    = object->count + 1;

        object->waitTime = g783->T5;
        if (object->count<14)
            object->state    = cAtAttForcePointerState1stForce;
        else
            object->state    = cAtAttForcePointerState2ndForce;

        if (g783->debug)
            AtPrintf("\r\n    -(State 1) Force %d nd for %s at %s\r\n", object->count, str, AtCliTime2String(&cur_time));
        }
    else if (object->state == cAtAttForcePointerState2ndForce)
        {
        uint32         TimeMs_Begin;
        tAtOsalCurTime cur_time;
        uint32         duration = AtAttControllerForcePointerAdjDurationGet(object->att, object->pointerType)*1000UL+40UL;

        AtOsalCurTimeGet(&cur_time);
        TimeMs_Begin = mTimeIntervalInMsGet(object->begin_time, cur_time);
        if (TimeMs_Begin > duration)
            {
            AtOsalCurTimeGet(&object->start_time);
            object->state    = cAtAttForcePointerStateEnd;
            object->waitTime = g783->period;
            }
        else
            {
            uint32         TimeMs = 0;
            TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
            if (TimeMs > (object->count*g783->T5))
                {
                AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
                object->state    = cAtAttForcePointerState2ndForceDelay;
                }
            }
        }
    else if (object->state == cAtAttForcePointerState2ndForceDelay)
        {
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        object->count    = object->count + 1;
        object->state    = cAtAttForcePointerState3rdForce;
        if (g783->debug)
            AtPrintf("\r\n    -(State 2)  Force %d nd 0 event %s at %s\r\n", object->count, str, AtCliTime2String(&cur_time));
        }
    else if (object->state == cAtAttForcePointerState3rdForce)
        {
        uint32         TimeMs_Begin;
        tAtOsalCurTime cur_time;
        uint32         duration= AtAttControllerForcePointerAdjDurationGet(object->att, object->pointerType)*1000UL+40UL;

        AtOsalCurTimeGet(&cur_time);
        TimeMs_Begin = mTimeIntervalInMsGet(object->begin_time, cur_time);
        if (TimeMs_Begin > (duration +g783->period))
            {
            AtOsalCurTimeGet(&object->start_time);
            object->state    = cAtAttForcePointerStateEnd;
            object->waitTime = g783->period;
            }
        else
            {
            uint32 TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
            if (TimeMs > (object->count*g783->T5))
                {
                AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
                object->state    = cAtAttForcePointerState2ndForceDelay;
                }
            }
        }
    else if (object->state == cAtAttForcePointerState3rdForceDelay)
        {
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        AtAttControllerForcePointerAdjStepSet(object->att, object->pointerType,1);
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 1);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        object->count    = object->count + 1;
        object->state    = cAtAttForcePointerState1stForce;
        if (g783->debug)
            AtPrintf("\r\n    -3rd Force %d nd 1 event %s at %s \r\n", object->count, str, AtCliTime2String(&cur_time));
        object->waitTime = g783->T5;
        }
    else if (object->state == cAtAttForcePointerStateEnd)
        {
        uint32         TimeMs = 0;
        tAtOsalCurTime cur_time;

        AtOsalCurTimeGet(&cur_time);
        TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
        if (object->waitTime == cInvalidUint32)
            object->state    =  cAtAttForcePointerStateEnd;
        else if (TimeMs      >  (object->waitTime))
            {
            AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
            if (g783->debug) AtPrintf("\r\n    -Force end %s\r\n", str);
            object->count    = 0;
            AtOsalCurTimeGet(&object->start_time);
            object->waitTime = cInvalidUint32;
            }
        }
    }

static void ForcePointerAdjModeG783i(tAtAttForcePointer *object, tThaAttSdhG783 *g783, const char* str)
    {
    if (object->state == cAtAttForcePointerStateStart)
        {
        AtOsalCurTimeGet(&object->begin_time);
        AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 1);
        AtAttControllerHwForcePointerAdj         (object->att, object->pointerType);
        AtOsalCurTimeGet(&object->start_time);
        object->state    = cAtAttForcePointerState1stForce;
        object->waitTime = 250;
        object->count    = object->count + 1;
        object->intervalcount =object->intervalcount + 1;
        AtPrintf("\r\n***Begin forcing %s at %s\r\n", str, AtCliTime2String(&object->start_time));
        if (g783->debug)
            {
            AtPrintf("\r\n    -Force %d st 1 event (next 250ms) \r\n", object->count);
            }
        }
    else if (object->state == cAtAttForcePointerState1stForce) /* Repeat each T0 */
        {
        uint32         TimeMs_Begin;
        tAtOsalCurTime cur_time;
        uint32 duration= AtAttControllerForcePointerAdjDurationGet(object->att, object->pointerType)*1000UL;

        AtOsalCurTimeGet(&cur_time);
        TimeMs_Begin = mTimeIntervalInMsGet(object->begin_time, cur_time);
        if (TimeMs_Begin > duration)
            {
            AtOsalCurTimeGet(&object->start_time);
            object->state    = cAtAttForcePointerStateEnd;
            object->waitTime = 250;
            }
        else
            {
            uint32         TimeMs = 0;
            TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
            if (TimeMs > (250*object->count))
                {
                if (object->count < 3)
                    {
                    AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
                    object->state    = cAtAttForcePointerState1stForceDelay;
                    }
                else
                    {
                    object->state    = cAtAttForcePointerState2ndForce;
                    object->count    = 0;
                    AtOsalCurTimeGet(&object->start_time);
                    object->waitTime = 500;
                    }
                }
            }
        }
    else if (object->state == cAtAttForcePointerState1stForceDelay)
        {
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        AtAttControllerForcePointerAdjStepSet(object->att, object->pointerType,1);
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 1);
        AtAttControllerHwForcePointerAdj           (object->att, object->pointerType);
        object->count    = object->count + 1;
        if (object->count<3)
            object->state    = cAtAttForcePointerState1stForce;
        else
            object->state    = cAtAttForcePointerState2ndForce;

        if (g783->debug)
            AtPrintf("\r\n    -Force %d nd 1 event at %s (next 250ms) \r\n", object->count, AtCliTime2String(&cur_time));
        object->waitTime = g783->T5;
        AtOsalCurTimeGet(&object->start_time);
        }
    else if (object->state == cAtAttForcePointerState2ndForce)
        {
        uint32         TimeMs_Begin;
        tAtOsalCurTime cur_time;
        uint32 duration= AtAttControllerForcePointerAdjDurationGet(object->att, object->pointerType)*1000UL+g783->period;

        AtOsalCurTimeGet(&cur_time);
        TimeMs_Begin = mTimeIntervalInMsGet(object->begin_time, cur_time);
        if (TimeMs_Begin > duration)
            {
            AtOsalCurTimeGet(&object->start_time);
            object->state    = cAtAttForcePointerStateEnd;
            object->waitTime = g783->period;
            }
        else
            {
            if (object->count < 7)
                {
                AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
                object->state    = cAtAttForcePointerState2ndForceDelay;
                }
            else
                {
                object->state    = cAtAttForcePointerState3rdForce;
                }

            }

        }
    else if (object->state == cAtAttForcePointerState2ndForceDelay)
        {
        AtAttControllerForcePointerAdjStepSet(object->att, object->pointerType,1);
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 1);
        AtAttControllerHwForcePointerAdj         (object->att, object->pointerType);
        object->count    = object->count + 1;
        object->state    = cAtAttForcePointerState2ndForce;
        if (g783->debug) AtPrintf("\r\n    -Force %d nd 1 event (next 500ms)\r\n", object->count);
        }
    else if (object->state == cAtAttForcePointerState3rdForce)
        {
        uint32         TimeMs_Begin;
        tAtOsalCurTime cur_time;
        uint32 duration = AtAttControllerForcePointerAdjDurationGet(object->att, object->pointerType)*1000UL+g783->period;

        AtOsalCurTimeGet(&cur_time);
        TimeMs_Begin = mTimeIntervalInMsGet(object->begin_time, cur_time);
        if (TimeMs_Begin > duration)
            {
            AtOsalCurTimeGet(&object->start_time);
            object->state    = cAtAttForcePointerStateEnd;
            object->waitTime = g783->period;
            }
        else
            {
            uint32         TimeMs = 0;
            TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
            if (TimeMs > ((g783->T0*object->intervalcount) - g783->period))
                {
                object->count    = 0;
                object->intervalcount =object->intervalcount + 1;
                object->state    = cAtAttForcePointerState1stForce;
                }
            }
        }
    else if (object->state == cAtAttForcePointerStateEnd)
        {
        uint32         TimeMs = 0;
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
        if (object->waitTime == cInvalidUint32)
            object->state    =  cAtAttForcePointerStateEnd;
        else if (TimeMs      > (object->waitTime))
            {
            AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
            if (g783->debug) AtPrintf("\r\n    -Force end at %s\r\n", AtCliTime2String(&object->start_time));
            object->count    = 0;
            object->intervalcount = 0;
            AtOsalCurTimeGet(&object->start_time);
            object->waitTime = cInvalidUint32;
            AtPrintf("\r\n***Forcing complete %s\r\n", str);
            }
        }
    }

/* For VC11 */
static void ForcePointerAdjModeG783jParta(tAtAttForcePointer *object, tThaAttSdhG783 *g783, const char* str)
    {
    AtChannel channel =AtAttControllerChannelGet(object->att);
    eAtSdhChannelType channelType =AtSdhChannelTypeGet((AtSdhChannel)channel);
    if (channelType!=cAtSdhChannelTypeTu11 && channelType != cAtSdhChannelTypeVc11)
        return;
    if (object->state == cAtAttForcePointerStateStart)
        {
        AtPrintf("\r\n***Begin forcing %s\r\n", str);
        AtOsalCurTimeGet(&object->begin_time);
        AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0x1);
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 1);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        AtOsalCurTimeGet(&object->start_time);
        object->state    = cAtAttForcePointerState1stForce;
        object->waitTime = g783->T5;
        object->count    = object->count + 1;

        if (g783->debug)
            AtPrintf("\r\n    -Force %d st for %s at %s\r\n", object->count, str,AtCliTime2String(&object->start_time));

        }
    else if (object->state == cAtAttForcePointerState1stForce)
        {
        uint32         TimeMs_Begin;
        tAtOsalCurTime cur_time;
        uint32 duration= AtAttControllerForcePointerAdjDurationGet(object->att, object->pointerType)*1000UL+40UL;

        AtOsalCurTimeGet(&cur_time);
        TimeMs_Begin = mTimeIntervalInMsGet(object->begin_time, cur_time);
        if (TimeMs_Begin > duration)
            {
            AtOsalCurTimeGet(&object->start_time);
            object->state    = cAtAttForcePointerStateEnd;
            object->waitTime = g783->T5;
            }
        else
            {
            uint32         TimeMs = 0;

            TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
            if (TimeMs > (object->waitTime-g783->period))
                {
                AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
                object->state    = cAtAttForcePointerState1stForceDelay;
                }
            }
        }
    else if (object->state == cAtAttForcePointerState1stForceDelay)
        {
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 1);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        AtOsalCurTimeGet(&object->start_time);
        object->state    = cAtAttForcePointerState2ndForce;
        object->waitTime = g783->T5;
        object->count    = object->count + 1;
        if (g783->debug)
            {
            AtPrintf("\r\n    -Force %d st for %s at %s\r\n", object->count, str,AtCliTime2String(&cur_time));
            }

        }
    else if (object->state == cAtAttForcePointerState2ndForce)
        {
        uint32         TimeMs_Begin;
        tAtOsalCurTime cur_time;
        uint32 duration= AtAttControllerForcePointerAdjDurationGet(object->att, object->pointerType)*1000UL+40UL;

        AtOsalCurTimeGet(&cur_time);
        TimeMs_Begin = mTimeIntervalInMsGet(object->begin_time, cur_time);
        if (TimeMs_Begin > duration)
            {
            AtOsalCurTimeGet(&object->start_time);
            object->state    = cAtAttForcePointerStateEnd;
            object->waitTime = g783->T5;
            }
        else
            {
            uint32         TimeMs = 0;
            TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
            if (TimeMs > (object->waitTime-g783->period))
                {
                if (object->count < 26)
                    {
                    AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
                    object->state    = cAtAttForcePointerState2ndForceDelay;
                    }
                else
                    {
                    object->waitTime = g783->T5;
                    object->state    = cAtAttForcePointerState1stForce;
                    object->count    = 0;
                    }
                }
            }
        }
    else if (object->state == cAtAttForcePointerState2ndForceDelay)
        {
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 1);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        object->count    = object->count + 1;
        object->waitTime = g783->T5;
        object->state    = cAtAttForcePointerState2ndForce;
        if (g783->debug)
            AtPrintf("\r\n    -Force %d nd for %s at %s \r\n", object->count, str, AtCliTime2String(&cur_time));
        AtOsalCurTimeGet(&object->start_time);
        }
    else if (object->state == cAtAttForcePointerStateEnd)
        {
        uint32         TimeMs = 0;
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
        if (object->waitTime == cInvalidUint32)
            object->state    =  cAtAttForcePointerStateEnd;
        else if (TimeMs      >  (object->waitTime))
            {
            AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
            if (g783->debug)
                AtPrintf("\r\n    -Force end for %s at %s\r\n", str, AtCliTime2String(&cur_time));
            object->count    = 0;
            AtOsalCurTimeGet(&object->start_time);
            object->waitTime = cInvalidUint32;
            AtPrintf("\r\n***Forcing complete %s\r\n", str);
            }
        }
    }

static void ForcePointerAdjModeG783jPartb(tAtAttForcePointer *object, tThaAttSdhG783 *g783, const char* str)
    {
    AtChannel channel =AtAttControllerChannelGet(object->att);
    eAtSdhChannelType channelType =AtSdhChannelTypeGet((AtSdhChannel)channel);
    if (channelType!=cAtSdhChannelTypeTu11 && channelType != cAtSdhChannelTypeVc11)
        return;
    if (object->state == cAtAttForcePointerStateStart)
        {
        AtPrintf("\r\n***Begin forcing %s\r\n", str);
        AtOsalCurTimeGet(&object->begin_time);
        AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 3);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        AtOsalCurTimeGet(&object->start_time);
        object->state    = cAtAttForcePointerState1stForce;
        object->waitTime = g783->T5;
        object->count    = object->count + 1;
        if (g783->debug)
            AtPrintf("\r\n    -Force %d st for %s at %s\r\n", object->count, str, AtCliTime2String(&object->start_time));
        }
    else if (object->state == cAtAttForcePointerState1stForce)
        {
        uint32         TimeMs_Begin;
        tAtOsalCurTime cur_time;
        uint32 duration= AtAttControllerForcePointerAdjDurationGet(object->att, object->pointerType)*1000UL+40UL;

        AtOsalCurTimeGet(&cur_time);
        TimeMs_Begin = mTimeIntervalInMsGet(object->begin_time, cur_time);
        if (TimeMs_Begin > duration)
            {
            AtOsalCurTimeGet(&object->start_time);
            object->state    = cAtAttForcePointerStateEnd;
            object->waitTime = g783->period;
            }
        else
            {
            uint32         TimeMs = 0;
            TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
            if (TimeMs > (object->waitTime-g783->period))
                {
                AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
                object->state    = cAtAttForcePointerState1stForceDelay;
                }
            }
        }
    else if (object->state == cAtAttForcePointerState1stForceDelay)
        {
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 3);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        AtOsalCurTimeGet(&object->start_time);
        object->state    = cAtAttForcePointerState2ndForce;
        object->waitTime = g783->T5;
        object->count    = object->count + 1;
        if (g783->debug) AtPrintf("\r\n    -Force %d st for %s at %s\r\n", object->count, str, AtCliTime2String(&cur_time));
        }
    else if (object->state == cAtAttForcePointerState2ndForce)
        {
        uint32         TimeMs_Begin;
        tAtOsalCurTime cur_time;
        uint32 duration= AtAttControllerForcePointerAdjDurationGet(object->att, object->pointerType)*1000UL;

        AtOsalCurTimeGet(&cur_time);
        TimeMs_Begin = mTimeIntervalInMsGet(object->begin_time, cur_time);
        if (TimeMs_Begin > (duration + g783->period))
            {
            AtOsalCurTimeGet(&object->start_time);
            object->state    = cAtAttForcePointerStateEnd;
            object->waitTime = g783->period;
            }
        else
            {
            uint32         TimeMs = 0;

            TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
            if (TimeMs > (object->waitTime-g783->period))
                {
                if (object->count < 26)
                    {
                    AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
                    object->state    = cAtAttForcePointerState2ndForceDelay;
                    }
                else
                    {
                    object->state    = cAtAttForcePointerState3rdForce;
                    object->count    = 0;
                    AtOsalCurTimeGet(&object->start_time);
                    object->waitTime = g783->T5;
                    }
                }

            }
        }
    else if (object->state == cAtAttForcePointerState2ndForceDelay)
        {
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 3);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        object->count    = object->count + 1;
        object->state    = cAtAttForcePointerState2ndForce;
        if (g783->debug)
            AtPrintf("\r\n    -Force %d nd for %s at %s\r\n", object->count, str, AtCliTime2String(&cur_time));
        AtOsalCurTimeGet(&object->start_time);
        object->waitTime = g783->T5;
        }
    else if (object->state == cAtAttForcePointerState3rdForce)
        {
        uint32         TimeMs_Begin;
        tAtOsalCurTime cur_time;
        uint32 duration= AtAttControllerForcePointerAdjDurationGet(object->att, object->pointerType)*1000UL;

        AtOsalCurTimeGet(&cur_time);
        TimeMs_Begin = mTimeIntervalInMsGet(object->begin_time, cur_time);
        if (TimeMs_Begin > duration)
            {
            AtOsalCurTimeGet(&object->start_time);
            object->state    = cAtAttForcePointerStateEnd;
            object->waitTime = g783->period;
            }
        else
            {
            uint32         TimeMs = 0;
            TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
            if (TimeMs > (object->waitTime-g783->period))
                {
                object->state    = cAtAttForcePointerState1stForce;
                object->count    = 0;
                }
            }
        }
    else if (object->state == cAtAttForcePointerStateEnd)
        {
        uint32         TimeMs = 0;
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
        if (object->waitTime == cInvalidUint32)
            object->state    =  cAtAttForcePointerStateEnd;
        else if (TimeMs      >  (object->waitTime))
            {
            AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
            if (g783->debug) AtPrintf("\r\n    -Force end for %s at %s \r\n", str, AtCliTime2String(&cur_time));
            object->count    = 0;
            AtOsalCurTimeGet(&object->start_time);
            object->waitTime = cInvalidUint32;
            AtPrintf("\r\n***Forcing complete\r\n");
            }
        }
    }

static void ForcePointerAdjModeG783jPartc(tAtAttForcePointer *object, tThaAttSdhG783 *g783, const char* str)
    {
    AtChannel channel =AtAttControllerChannelGet(object->att);
    eAtSdhChannelType channelType =AtSdhChannelTypeGet((AtSdhChannel)channel);
    if (channelType!=cAtSdhChannelTypeTu11 && channelType != cAtSdhChannelTypeVc11)
        return;
    if (object->state == cAtAttForcePointerStateStart)
        {
        AtOsalCurTimeGet(&object->begin_time);
        AtPrintf("\r\n***Begin forcing %s\r\n", str);
        AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 1);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        object->state    = cAtAttForcePointerState1stForce;
        object->count    = object->count + 1;
        AtOsalCurTimeGet(&object->start_time);
        object->waitTime = g783->T5;
        if (g783->debug)
            AtPrintf("\r\n    -Force %d st for %s at %s\r\n", object->count, str,AtCliTime2String(&object->start_time));
        }
    else if (object->state == cAtAttForcePointerState1stForce)
        {
        uint32         TimeMs = 0;
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
        if (TimeMs > (g783->T5*object->count))
            {
            if (object->count < 26)
                {
                AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
                AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 1);
                AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
                object->count    = object->count + 1;
                object->state    = cAtAttForcePointerState1stForce;
                if (g783->debug)
                    AtPrintf("\r\n    -Force %d nd for %s at %s\r\n", object->count, str,AtCliTime2String(&cur_time));
                AtOsalCurTimeGet(&object->start_time);
                }
            else
                {
                object->state    = cAtAttForcePointerState2ndForce;
                object->count    = 0;
                AtOsalCurTimeGet(&object->start_time);
                object->waitTime = 10000;
                }

            }
        }
    else if (object->state == cAtAttForcePointerState2ndForce)
        {
        uint32         TimeMs_Begin;
        tAtOsalCurTime cur_time;
        uint32 duration= AtAttControllerForcePointerAdjDurationGet(object->att, object->pointerType)*1000UL;

        AtOsalCurTimeGet(&cur_time);
        TimeMs_Begin = mTimeIntervalInMsGet(object->begin_time, cur_time);
        if (TimeMs_Begin > (duration+ g783->period))
            {
            AtOsalCurTimeGet(&object->start_time);
            object->state    = cAtAttForcePointerStateEnd;
            object->waitTime = g783->period;
            }
        else
            {
            uint32         TimeMs = 0;
            TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
            if (TimeMs > (object->waitTime-g783->period))
                {
                AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
                AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 1);
                AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
                object->state    = cAtAttForcePointerStateEnd;
                object->waitTime = g783->period;
                AtOsalCurTimeGet(&object->start_time);
                }
            }
        }
    else if (object->state == cAtAttForcePointerStateEnd)
        {
        uint32         TimeMs = 0;
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
        if (object->waitTime == cInvalidUint32)
            object->state    =  cAtAttForcePointerStateEnd;
        else if (TimeMs      >  (object->waitTime))
            {
            AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
            if (g783->debug)
                AtPrintf("\r\n    -Force end for %s at %s\r\n", str, AtCliTime2String(&cur_time));
            object->count    = 0;
            object->waitTime = cInvalidUint32;
            AtOsalCurTimeGet(&object->start_time);
            AtPrintf("\r\n***Forcing complete %s\r\n", str);
            }
        }
    }

static void ForcePointerAdjModeG783jPartd(tAtAttForcePointer *object, tThaAttSdhG783 *g783, const char* str)
    {
    AtChannel channel =AtAttControllerChannelGet(object->att);
    eAtSdhChannelType channelType =AtSdhChannelTypeGet((AtSdhChannel)channel);
    if (channelType!=cAtSdhChannelTypeTu11 && channelType != cAtSdhChannelTypeVc11)
        return;
    if (object->state == cAtAttForcePointerStateStart)
        {
        AtPrintf("\r\n***Begin forcing %s\r\n", str);
        AtOsalCurTimeGet(&object->begin_time);
        AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 1);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        AtOsalCurTimeGet(&object->start_time);
        object->state    = cAtAttForcePointerState1stForce;
        object->waitTime = g783->T5;
        object->count    = object->count + 1;
        if (g783->debug)
            AtPrintf("\r\n -Force %d st for %s at %s \r\n", object->count, str, AtCliTime2String(&object->begin_time));

        }
    else if (object->state == cAtAttForcePointerState1stForce) /* Repeat*/
        {
        uint32         TimeMs_Begin;
        tAtOsalCurTime cur_time;
        uint32 duration= AtAttControllerForcePointerAdjDurationGet(object->att, object->pointerType)*1000UL;

        AtOsalCurTimeGet(&cur_time);
        TimeMs_Begin = mTimeIntervalInMsGet(object->begin_time, cur_time);
        if (TimeMs_Begin > duration)
            {
            AtOsalCurTimeGet(&object->start_time);
            object->state    = cAtAttForcePointerStateEnd;
            object->waitTime = g783->period;
            }
        else
            {
            uint32         TimeMs = 0;
            TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
            if (TimeMs > (object->count*g783->T5))
                {
                if (object->count < 25)
                    {
                    AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
                    object->state    = cAtAttForcePointerState1stForceDelay;
                    }
                else
                    {
                    object->state    = cAtAttForcePointerState2ndForce;
                    object->count    = 0;
                    AtOsalCurTimeGet(&object->start_time);
                    object->waitTime = g783->T5*3;
                    }

                }
            }
        }
    else if (object->state == cAtAttForcePointerState1stForceDelay)
        {
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 1);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        object->count    = object->count + 1;
        object->state    = cAtAttForcePointerState1stForce;
        if (g783->debug)
            AtPrintf("\r\n -Force %d th %s at %s\r\n", object->count, str, AtCliTime2String(&cur_time));
        object->waitTime = g783->T5;
        AtOsalCurTimeGet(&object->start_time);
        }
    else if (object->state == cAtAttForcePointerState2ndForce)/* Force 26 */
        {
        uint32         TimeMs_Begin;
        tAtOsalCurTime cur_time;
        uint32 duration= AtAttControllerForcePointerAdjDurationGet(object->att, object->pointerType)*1000UL+40UL;

        AtOsalCurTimeGet(&cur_time);
        TimeMs_Begin = mTimeIntervalInMsGet(object->begin_time, cur_time);
        if (TimeMs_Begin > duration)
            {
            AtOsalCurTimeGet(&object->start_time);
            object->state    = cAtAttForcePointerStateEnd;
            object->waitTime = g783->period;
            }
        else
            {
            uint32         TimeMs = 0;
            TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
            if (TimeMs > (object->count*g783->T5))
                {
                if (object->count < 25)
                    {
                    AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
                    object->state    = cAtAttForcePointerState2ndForceDelay;
                    }
                else
                    {
                    object->state    = cAtAttForcePointerState2ndForce;
                    object->count    = 0;
                    AtOsalCurTimeGet(&object->start_time);
                    object->waitTime = g783->T5*3;
                    object->count = object->count + 3;
                    }

                }
            }
        }
    else if (object->state == cAtAttForcePointerState2ndForceDelay)
        {
        tAtOsalCurTime cur_time;

        AtOsalCurTimeGet(&cur_time);
        AtAttControllerForcePointerAdjNumEventSet(object->att, object->pointerType, 1);
        AtAttControllerHwForcePointerAdj(object->att, object->pointerType);
        object->count    = object->count + 1;
        object->state    = cAtAttForcePointerState1stForce;
        if (g783->debug) AtPrintf("\r\n -Force %d th %s at %s\r\n", object->count, str, AtCliTime2String(&cur_time));
        object->waitTime = g783->T5;
        }
    else if (object->state == cAtAttForcePointerState3rdForce) /* Ignore 2 */
        {
        uint32         TimeMs = 0;
        tAtOsalCurTime cur_time;

        AtOsalCurTimeGet(&object->start_time);
        TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
        if (TimeMs > (object->count*g783->T5))
            {
            if (object->count < 28)
                {
                object->state = cAtAttForcePointerState3rdForce;
                object->count ++;
                if (g783->debug) AtPrintf("\r\n    -No adj %s at %s\r\n", str, AtCliTime2String(&cur_time));
                object->waitTime =g783->T5;
                }
            else
                {
                object->state = cAtAttForcePointerState1stForce;
                object->count = 0;
                object->waitTime =g783->T5;
                }
            }
        }
    else if (object->state == cAtAttForcePointerStateEnd)
        {
        uint32         TimeMs = 0;
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        TimeMs = mTimeIntervalInMsGet(object->start_time, cur_time);
        if (object->waitTime == cInvalidUint32)
            object->state    =  cAtAttForcePointerStateEnd;
        else if (TimeMs      >  (object->waitTime))
            {
            AtAttControllerPointerAdjForcingHwDataMaskSet(object->att, object->pointerType, 0);
            if (g783->debug) AtPrintf("\r\n    -Force end %s at %s \r\n", str, AtCliTime2String(&cur_time));
            object->count    = 0;
            object->waitTime = cInvalidUint32;
            AtOsalCurTimeGet(&object->start_time);
            AtPrintf("\r\n***Forcing complete %s\r\n", str);
            }
        }
    }

static void ForcePointerProcess(AtAttSdhManager self)
    {
    uint32 forcePointerExecuteTimeUs = 0;
    tAtOsalCurTime startTime;
    tAtOsalCurTime curTime;

    AtOsalCurTimeGet(&startTime);
    AtOsalMutexLock(mThis(self)->mutex);
    mThis(self)->lock++;
    /*AtPrintf("Lock %d Line=%d %s\r\n", lock_count++, __LINE__, "ForcePointerProcess");*/
        {
        uint32 i=0;
        AtList list = ThaAttSdhManagerForcePointerChannelListGet(mThis(self));
        uint32 length = AtListLengthGet(list);
        for (i = 0; i <length; i++)
            {
            tAtAttForcePointer *object = (tAtAttForcePointer*)AtListObjectGet(list, i);
            self->process_index = i;
            if (object->mode == cAtAttForcePointerAdjModeG783a)
                ForcePointerAdjModeG783a(object, &mThis(self)->g783,      AtObjectToString((AtObject)AtAttControllerChannelGet(object->att))); /* 2 events */
            else if (object->mode == cAtAttForcePointerAdjModeG783b)
                ForcePointerAdjModeG783b(object, &mThis(self)->g783,      AtObjectToString((AtObject)AtAttControllerChannelGet(object->att))); /* 7 events */
            else if (object->mode == cAtAttForcePointerAdjModeG783c)
                ForcePointerAdjModeG783c(object, &mThis(self)->g783,      AtObjectToString((AtObject)AtAttControllerChannelGet(object->att))); /* 5 events */
            else if (object->mode == cAtAttForcePointerAdjModeG783d)
                ForcePointerAdjModeG783d(object, &mThis(self)->g783,      AtObjectToString((AtObject)AtAttControllerChannelGet(object->att))); /* 4 events */
            else if (object->mode == cAtAttForcePointerAdjModeG783e)
                ForcePointerAdjModeG783e(object, &mThis(self)->g783,      AtObjectToString((AtObject)AtAttControllerChannelGet(object->att)));
            else if (object->mode == cAtAttForcePointerAdjModeG783f)
                ForcePointerAdjModeG783f(object, &mThis(self)->g783,      AtObjectToString((AtObject)AtAttControllerChannelGet(object->att)));
            else if (object->mode == cAtAttForcePointerAdjModeG783gPart1)
                ForcePointerAdjModeG783gPart1(object, &mThis(self)->g783, AtObjectToString((AtObject)AtAttControllerChannelGet(object->att)));
            else if (object->mode == cAtAttForcePointerAdjModeG783gPart2)
                ForcePointerAdjModeG783gPart2(object, &mThis(self)->g783, AtObjectToString((AtObject)AtAttControllerChannelGet(object->att)));
            else if (object->mode == cAtAttForcePointerAdjModeG783gPart3)
                ForcePointerAdjModeG783gPart3(object, &mThis(self)->g783, AtObjectToString((AtObject)AtAttControllerChannelGet(object->att)));
            else if (object->mode == cAtAttForcePointerAdjModeG783gPart4)
                ForcePointerAdjModeG783gPart4(object, &mThis(self)->g783, AtObjectToString((AtObject)AtAttControllerChannelGet(object->att)));
            else if (object->mode == cAtAttForcePointerAdjModeG783hParta)
                ForcePointerAdjModeG783hParta(object, &mThis(self)->g783, AtObjectToString((AtObject)AtAttControllerChannelGet(object->att)));
            else if (object->mode == cAtAttForcePointerAdjModeG783hPartb)
                ForcePointerAdjModeG783hPartb(object, &mThis(self)->g783, AtObjectToString((AtObject)AtAttControllerChannelGet(object->att)));
            else if (object->mode == cAtAttForcePointerAdjModeG783hPartc)
                ForcePointerAdjModeG783hPartc(object, &mThis(self)->g783, AtObjectToString((AtObject)AtAttControllerChannelGet(object->att)));
            else if (object->mode == cAtAttForcePointerAdjModeG783hPartd)
                ForcePointerAdjModeG783hPartd(object, &mThis(self)->g783, AtObjectToString((AtObject)AtAttControllerChannelGet(object->att)));
            else if (object->mode == cAtAttForcePointerAdjModeG783i)
                ForcePointerAdjModeG783i(object, &mThis(self)->g783,      AtObjectToString((AtObject)AtAttControllerChannelGet(object->att)));
            else if (object->mode == cAtAttForcePointerAdjModeG783jParta)
                ForcePointerAdjModeG783jParta(object, &mThis(self)->g783, AtObjectToString((AtObject)AtAttControllerChannelGet(object->att)));
            else if (object->mode == cAtAttForcePointerAdjModeG783jPartb)
                ForcePointerAdjModeG783jPartb(object, &mThis(self)->g783, AtObjectToString((AtObject)AtAttControllerChannelGet(object->att)));
            else if (object->mode == cAtAttForcePointerAdjModeG783jPartc)
                ForcePointerAdjModeG783jPartc(object, &mThis(self)->g783, AtObjectToString((AtObject)AtAttControllerChannelGet(object->att)));
            else if (object->mode == cAtAttForcePointerAdjModeG783jPartd)
                ForcePointerAdjModeG783jPartd(object, &mThis(self)->g783, AtObjectToString((AtObject)AtAttControllerChannelGet(object->att)));
            };
        }
    AtOsalMutexUnLock(mThis(self)->mutex);
    mThis(self)->unlock++;
    AtOsalCurTimeGet(&curTime);
    forcePointerExecuteTimeUs = AtOsalDifferenceTimeInMs(&curTime, &startTime);
    if (forcePointerExecuteTimeUs > mThis(self)->forcePointerExecuteTimeMs)
        mThis(self)->forcePointerExecuteTimeMs = forcePointerExecuteTimeUs;


    /*AtPrintf("UnLock %d Line=%d %s\r\n", unlock_count++, __LINE__,"ForcePointerProcess");*/
    }
static void ThaAttSdhManagerDeleteMutex(ThaAttSdhManager self)
    {
    if (self->mutex)
        AtOsalMutexDestroy(self->mutex);
    self->mutex = NULL;
    }
/*-----------------------------------------------------------------------*/
static void Delete(AtObject self)
    {
    AtOsalMutexLock(mThis(self)->mutex);
    mThis(self)->lock++;
    ThaAttSdhManagerDeleteForcePointerChannelList(mThis(self));
    AtOsalMutexUnLock(mThis(self)->mutex);
    mThis(self)->unlock++;
    ThaAttSdhManagerDeleteMutex(mThis(self));
    m_AtObjectMethods->Delete(self);
    }

static void OverrideAtObject(AtObject self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(self, &m_AtObjectOverride);
    }

static void OverrideAtAttSdhManager(AtAttSdhManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtAttSdhManagerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtAttSdhManagerOverride, m_AtAttSdhManagerMethods, sizeof(m_AtAttSdhManagerOverride));
        mMethodOverride(m_AtAttSdhManagerOverride, Init);
        mMethodOverride(m_AtAttSdhManagerOverride, ForcePointerProcess);
        }

    mMethodsSet(self, &m_AtAttSdhManagerOverride);
    }

static void Override(ThaAttSdhManager self)
    {
    OverrideAtObject((AtObject) self);
    OverrideAtAttSdhManager((AtAttSdhManager)self);
    }

static void MethodsInit(ThaAttSdhManager self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));
        mMethodOverride(m_methods, SetResetTimeAllHoChannel);
        mMethodOverride(m_methods, SetResetTimeAllLoChannel);
        mMethodOverride(m_methods, DebugHoPointerLooptimeEnable);
        mMethodOverride(m_methods, DebugLoPointerLooptimeEnable);
        mMethodOverride(m_methods, DebugCheckForceRateShowSet);
        mMethodOverride(m_methods, DebugCheckForceRateShowGet);
        mMethodOverride(m_methods, DebugCheckForceRateEnSet);
        mMethodOverride(m_methods, DebugCheckForceRateEnGet);
        mMethodOverride(m_methods, DebugCheckForceRatePecentSet);
        mMethodOverride(m_methods, DebugCheckForceRatePecentGet);
        mMethodOverride(m_methods, AlarmDurationUnitSet);
        mMethodOverride(m_methods, AlarmDurationUnitGet);
        mMethodOverride(m_methods, SourceMultiplexingResync);
        }

    mMethodsSet(self, &m_methods);
    }

AtAttSdhManager ThaAttSdhManagerObjectInit(AtAttSdhManager self, AtModuleSdh sdh)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaAttSdhManager));

    /* Super constructor */
    if (AtAttSdhManagerObjectInit(self, sdh) == NULL)
        return NULL;

    /* Setup class */
    Override(mThis(self));
    MethodsInit(mThis(self));
    mThis(self)->mutex = AtOsalMutexCreate();
    m_methodsInit = 1;

    return self;
    }

eAtRet ThaAttSdhManagerDebugForcingLogicSet(ThaAttSdhManager self, eBool isDut)
    {
    if (self)
        {
        self->forceFromDut = isDut;
        return cAtOk;
        }

    return cAtErrorNullPointer;
    }

eBool ThaAttSdhManagerDebugForcingLogicGet(ThaAttSdhManager self)
    {
    return (eBool)(self ? self->forceFromDut : cAtFalse);
    }

eAtRet ThaAttSdhManagerDebugHoPointerLooptimeEnable(ThaAttSdhManager self, eBool en)
    {
    if (self)
        return mMethodsGet(self)->DebugHoPointerLooptimeEnable(self, en);
    return cAtErrorNullPointer;
    }


eAtRet ThaAttSdhManagerDebugLoPointerLooptimeEnable(ThaAttSdhManager self, eBool en)
    {
    if (self)
        return mMethodsGet(self)->DebugLoPointerLooptimeEnable(self, en);
    return cAtErrorNullPointer;
    }

eAtRet ThaAttSdhManagerCheckForceRateShowSet(ThaAttSdhManager self, eBool en)
    {
    if (self)
        return mMethodsGet(self)->DebugCheckForceRateShowSet(self, en);
    return cAtErrorNullPointer;
    }

eBool  ThaAttSdhManagerCheckForceRateShowGet(ThaAttSdhManager self)
    {
    if (self)
        return mMethodsGet(self)->DebugCheckForceRateShowGet(self);
    return cAtFalse;
    }

eAtRet ThaAttSdhManagerCheckForceRateEnSet(ThaAttSdhManager self, eBool en)
    {
    if (self)
        return mMethodsGet(self)->DebugCheckForceRateEnSet(self, en);
    return cAtErrorNullPointer;
    }

eBool  ThaAttSdhManagerCheckForceRateEnGet(ThaAttSdhManager self)
    {
    if (self)
        return mMethodsGet(self)->DebugCheckForceRateEnGet(self);
    return cAtFalse;
    }

eAtRet ThaAttSdhManagerCheckForceRatePecentSet(ThaAttSdhManager self, uint8 percent)
    {
    if (self)
        return mMethodsGet(self)->DebugCheckForceRatePecentSet(self, percent);
    return cAtErrorNullPointer;
    }

uint8  ThaAttSdhManagerCheckForceRatePecentGet(ThaAttSdhManager self)
    {
    if (self)
        return mMethodsGet(self)->DebugCheckForceRatePecentGet(self);
    return 0;
    }

eAtRet ThaAttSdhManagerSohColRemoveSet(ThaAttSdhManager self, uint8 SohColRemove)
    {
    if (self)
        {
        if (SohColRemove==0 || SohColRemove==3 || SohColRemove==9)
            {
            self->SohColRemove = SohColRemove;
            return cAtOk;
            }
        }
    return cAtError;
    }

uint8 ThaAttSdhManagerSohColRemoveGet(ThaAttSdhManager self)
    {
    if (self)
        return self->SohColRemove;
    return 0;
    }


eAtRet ThaAttSdhManagerG783DebugSet(ThaAttSdhManager self, eBool debugEn)
    {
    if (self)
        {
        self->g783.debug = debugEn;
        return cAtOk;
        }
    return cAtErrorNullPointer;
    }

eAtRet ThaAttSdhManagerG783T0Set(ThaAttSdhManager self, uint32 T0)
    {
    if (T0 < cAtAttPointerAdjustTimeT0Min || T0 > cAtAttPointerAdjustTimeT0Max)
        return cAtErrorOutOfRangParm;

    if (self)
        {
        self->g783.T0 = T0;
        return cAtOk;
        }
    return cAtError;
    }
uint32 ThaAttSdhManagerG783T0Get(ThaAttSdhManager self)
    {
    if (self)
        return self->g783.T0;
    return 0;
    }

eAtRet ThaAttSdhManagerG783T1Set(ThaAttSdhManager self, uint32 T1)
    {
    if (T1 < cAtAttPointerAdjustTimeT1 || T1 > cAtAttPointerAdjustTimeT1Max)
        return cAtErrorOutOfRangParm;

    if (self)
        {
        self->g783.T1 = T1;
        return cAtOk;
        }
    return cAtErrorNullPointer;
    }

uint32 ThaAttSdhManagerG783T1Get(ThaAttSdhManager self)
    {
    if (self)
        return self->g783.T1;
    return 0;
    }

eAtRet ThaAttSdhManagerG783T2Set(ThaAttSdhManager self, uint32 T2)
    {
    if (T2 <= cAtAttPointerAdjustTimeT2Min || T2 > cAtAttPointerAdjustTimeT2Max)
        return cAtErrorOutOfRangParm;
    if (self)
        {
        self->g783.T2 = T2;
        return cAtOk;
        }
    return cAtError;
    }

uint32 ThaAttSdhManagerG783T2Get(ThaAttSdhManager self)
    {
    if (self)
        return self->g783.T2;
    return 0;
    }

eAtRet ThaAttSdhManagerG783T3Set(ThaAttSdhManager self, uint32 T3)
    {
    if (    T3 <  cAtAttPointerAdjustTimeT3Min ||
            T3 > cAtAttPointerAdjustTimeT3Max)
        return cAtErrorOutOfRangParm;
    if (self)
        {
        self->g783.T3 = T3;
        return cAtOk;
        }
    return cAtErrorNullPointer;
    }

uint32 ThaAttSdhManagerG783T3Get(ThaAttSdhManager self)
    {
    if (self)
        return self->g783.T3;
    return 0;
    }
eAtRet ThaAttSdhManagerG783T3Note2Set(ThaAttSdhManager self, uint32 T3Note2)
    {
    if (T3Note2 != cAtAttPointerAdjustTimeT3Note2)
        return cAtErrorOutOfRangParm;
    if (self)
        {
        self->g783.T3Note2 = T3Note2;
        return cAtOk;
        }
    return cAtErrorNullPointer;
    }

uint32 ThaAttSdhManagerG783T3Note2Get(ThaAttSdhManager self)
    {
    if (self)
        return self->g783.T3Note2;
    return 0;
    }

eAtRet ThaAttSdhManagerG783T4Set(ThaAttSdhManager self, uint32 T4)
    {
    if (T4 != cAtAttPointerAdjustTimeT4)
        return cAtErrorOutOfRangParm;
    if (self)
        {
        self->g783.T4 = T4;
        return cAtOk;
        }
    return cAtErrorNullPointer;
    }

uint32 ThaAttSdhManagerG783T4Get(ThaAttSdhManager self)
    {
    if (self)
        return self->g783.T4;
    return 0;
    }

eAtRet ThaAttSdhManagerG783T4Note5Set(ThaAttSdhManager self, float T4Note5)
    {
    if (((uint32)(T4Note5*10)) !=  ((uint32)(cAtAttPointerAdjustTimeT4Note5*10)))
        return cAtErrorOutOfRangParm;

    if (self)
        {
        self->g783.T4Note5 = (float)T4Note5;
        return cAtOk;
        }

    return cAtErrorNullPointer;
    }

float ThaAttSdhManagerG783T4Note5Get(ThaAttSdhManager self)
    {
    if (self)
        return self->g783.T4Note5;
    return 0;
    }

eAtRet ThaAttSdhManagerG783T5Set(ThaAttSdhManager self, uint32 T5)
    {
    if (    T5 <  cAtAttPointerAdjustTimeT5Min ||
            T5 > cAtAttPointerAdjustTimeT5Max)
        return cAtErrorOutOfRangParm;

    if (self)
        {
        self->g783.T5 = T5; /* or T5 Note 5 */
        return cAtOk;
        }

    return cAtErrorNullPointer;
    }

uint32 ThaAttSdhManagerG783T5Get(ThaAttSdhManager self)
    {
    if (self)
        return self->g783.T5;
    return 0;
    }

eAtRet ThaAttSdhManagerG783T5Note6Set(ThaAttSdhManager self, uint32 T5Note6)
    {
    if (    T5Note6 <  cAtAttPointerAdjustTimeT5Note6Min ||
            T5Note6 >  cAtAttPointerAdjustTimeT5Note6Max)
        return cAtErrorOutOfRangParm;

    if (self)
        {
        self->g783.T5Note6 = T5Note6; /* or T5 Note 6 */
        return cAtOk;
        }
    return cAtErrorNullPointer;
    }

uint32 ThaAttSdhManagerG783T5Note6Get(ThaAttSdhManager self)
    {
    if (self)
        return self->g783.T5Note6;
    return 0;
    }

AtList ThaAttSdhManagerForcePointerChannelListGet(ThaAttSdhManager self)
    {
    if (self->sdhPathForcePointer ==NULL)
        self->sdhPathForcePointer = AtListCreate(0);

    return self->sdhPathForcePointer;
    }

static uint32 ChannelIsInList(ThaAttSdhManager self, AtAttController channel, eAtAttForcePointerAdjMode mode, uint32 pointerType)
    {
    AtList list = ThaAttSdhManagerForcePointerChannelListGet(self);
    uint32 index = 0;
    uint32 length = AtListLengthGet(list);

    for (index = 0; index < length; index ++)
        {
        tAtAttForcePointer *object = NULL;
        object = (tAtAttForcePointer*)AtListObjectGet(list, index);
        if (    object->att        == channel &&
                object->mode       == mode    &&
                object->pointerType==pointerType)
            {
            return index;
            }
        }

    return cInvalidUint32;
    }

/* When enable, we add to the list with the start */
eAtRet ThaAttSdhManagerForcePointerChannelAdd(ThaAttSdhManager self, AtAttController channel, eAtAttForcePointerAdjMode mode, uint32 pointerType)
    {
    AtOsalMutexLock(self->mutex);
    mThis(self)->lock++;
    /*AtPrintf("Lock %d Line=%d %s\r\n", lock_count++, __LINE__, "ThaAttSdhManagerForcePointerChannelAdd");*/
        {
        AtList list   = ThaAttSdhManagerForcePointerChannelListGet(self);
        uint32 index = ChannelIsInList(self, channel, mode, pointerType);
        if (index == cInvalidUint32)
            {
            tAtAttForcePointer *object = NULL;
            object              = AtOsalMemAlloc(sizeof(tAtAttForcePointer));
            object->att         = channel;
            object->mode        = mode;
            object->pointerType = pointerType;
            object->state       = cAtAttForcePointerStateStart;
            object->count       = 0;
            object->intervalcount = 0;
            object->waitTime    = 10;
            AtOsalCurTimeGet(&object->start_time);
            AtListObjectAdd(list, (AtObject)object);
            }
        else
            {
            tAtAttForcePointer *object = NULL;
            object = (tAtAttForcePointer*)AtListObjectGet(list, index);
            object->state       = cAtAttForcePointerStateStart;
            object->pointerType = pointerType;
            object->att         = channel;
            object->mode        = mode;
            object->count       = 0;
            object->waitTime    = 10;
            AtOsalCurTimeGet(&object->start_time);
            }
        }
    AtOsalMutexUnLock(self->mutex);
    mThis(self)->unlock++;
    /*AtPrintf("UnLock %d Line=%d %s\r\n", unlock_count++, __LINE__, "ThaAttSdhManagerForcePointerChannelAdd");*/
    return cAtOk;
    }

eAtRet ThaAttSdhManagerForcePointerChannelRemove(ThaAttSdhManager self, AtAttController channel, eAtAttForcePointerAdjMode mode, uint32 pointerType, eBool accessHw)
    {
    AtOsalMutexLock(self->mutex);
    mThis(self)->lock++;
    /*AtPrintf("Lock %d Line=%d %s\r\n", lock_count++, __LINE__,"ThaAttSdhManagerForcePointerChannelRemove");*/
        {
        uint32 index = ChannelIsInList(self, channel, mode, pointerType);
        if (index != cInvalidUint32)
            {
            AtList list = ThaAttSdhManagerForcePointerChannelListGet(self);
            tAtAttForcePointer *object = (tAtAttForcePointer *)AtListObjectRemoveAtIndex(list, index);
            object->state       = cAtAttForcePointerStateEnd;
            if (self->g783.debug)AtPrintf("Remove\r\n");
            if (accessHw)
                AtAttControllerHwUnForcePointerAdj(object->att, object->pointerType);
            AtOsalMemFree(object);
            }
        }
    AtOsalMutexUnLock(self->mutex);
    mThis(self)->unlock++;
    /*AtPrintf("UnLock %d Line=%d %s\r\n", unlock_count++, __LINE__,"ThaAttSdhManagerForcePointerChannelRemove");*/
    return cAtOk;
    }


eAtRet ThaAttSdhManagerAlarmDurationUnitSet(ThaAttSdhManager self, eAttSdhForceAlarmDurationUnit unit)
    {
    mNumericalAttributeSet(AlarmDurationUnitSet, unit);
    }

eAttSdhForceAlarmDurationUnit ThaAttSdhManagerAlarmDurationUnitGet(ThaAttSdhManager self)
    {
    mAttributeGet(AlarmDurationUnitGet, eAttSdhForceAlarmDurationUnit, cAttSdhForceAlarmDurationUnitSecond);
    }

uint32 ThaAttSdhManagerForcePointerExecuteTimeMsGet(ThaAttSdhManager self)
    {
    if (self)
        return self->forcePointerExecuteTimeMs;
    return 0;
    }

uint32 ThaAttSdhManagerForcePointerNumChannelGet(ThaAttSdhManager self)
    {
    if (self)
        {
        uint32 num = 0;
        AtOsalMutexLock(self->mutex);
        mThis(self)->lock++;
        num =  AtListLengthGet(self->sdhPathForcePointer);
        AtOsalMutexUnLock(self->mutex);
        mThis(self)->unlock++;
        return num;
        }
    return 0;
    }

eAtRet ThaAttSdhManagerForcePointerSourceMultiplexingResync(ThaAttSdhManager self)
    {
    if(self!=NULL)
        {
        mMethodsGet(self)->SourceMultiplexingResync(self);
        return cAtOk;
        }
    return cAtError;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : AtAttWanderChannelInternal.h
 * 
 * Created Date: Jun 13, 2017
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAATTWANDERCHANNELINTERNAL_H_
#define _THAATTWANDERCHANNELINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/att/AtAttWanderChannelInternal.h"
#include "ThaAttWanderChannelReg.h"
/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
typedef struct tThaAttWanderChannel* ThaAttWanderChannel;
/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaAttWanderChannelMethods
    {
    uint32 (*MeasureWanderControl)(ThaAttWanderChannel self);
    uint32 (*ModeCfgMask)(ThaAttWanderChannel self);
    uint32 (*InvertCfgMask)(ThaAttWanderChannel self);
    uint32 (*FlushCfgMask)(ThaAttWanderChannel self);
    uint32 (*MeasureWanderStatus)(ThaAttWanderChannel self);
    uint32 (*MeasureWanderOffset)(ThaAttWanderChannel self);
    uint32 (*UnitCntMask)(ThaAttWanderChannel self);
    uint32 (*SignCntMask)(ThaAttWanderChannel self);
    uint32 (*NClkCntMask)(ThaAttWanderChannel self);

    uint32 (*WanderRead)(ThaAttWanderChannel self, uint32 regAddress);
    eAtRet (*WanderWrite)(ThaAttWanderChannel self, uint32 regAddress, uint32 regValue);
    }tThaAttWanderChannelMethods;

typedef struct tThaAttWanderChannel
    {
    tAtAttWanderChannel super;
    const tThaAttWanderChannelMethods *methods;
    }tThaAttWanderChannel;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtAttWanderChannel ThaAttWanderChannelObjectInit(AtAttWanderChannel self, AtChannel channel);
AtAttWanderChannel ThaAttWanderChannelNew(AtChannel channel);
#ifdef __cplusplus
}
#endif
#endif /* _THAATTWANDERCHANNELINTERNAL_H_ */


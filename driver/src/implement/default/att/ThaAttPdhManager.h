/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ThaAttPdhManager
 *
 * File        : ThaAttPdhManager.h
 *
 * Created Date: July 26, 2017
 *
 * Author      : chaudpt
 *
 * Description : This file contains common prototypes of ATT PDH Manager of default
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _THAATTPDHMANAGER_H_
#define _THAATTPDHMANAGER_H_

/*--------------------------- Include files ----------------------------------*/
#include "AtModulePdh.h"
#include "../../../generic/att/AtAttPdhManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Typedef ----------------------------------------*/
typedef struct tThaAttPdhManager* ThaAttPdhManager;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtAttPdhManager Tha6A210021AttPdhManagerNew(AtModulePdh pdh);
AtAttPdhManager Tha6A210031AttPdhManagerNew(AtModulePdh pdh);
AtAttPdhManager Tha6A000010AttPdhManagerNew(AtModulePdh pdh);
AtAttPdhManager Tha6A290021AttPdhManagerNew(AtModulePdh pdh);
AtAttPdhManager Tha6A290011AttPdhManagerNew(AtModulePdh pdh);
AtAttPdhManager Tha6A290022AttPdhManagerNew(AtModulePdh pdh);

eAtRet ThaAttPdhManagerDebugForcingLogicSet(ThaAttPdhManager self, eBool isDut);
eBool ThaAttPdhManagerDebugForcingLogicGet(ThaAttPdhManager self);

eAtRet ThaAttPdhManagerCheckForceRateShowSet(ThaAttPdhManager self, eBool en);
eBool  ThaAttPdhManagerCheckForceRateShowGet(ThaAttPdhManager self);
eAtRet ThaAttPdhManagerCheckForceRateEnSet(ThaAttPdhManager self, eBool en);
eBool  ThaAttPdhManagerCheckForceRateEnGet(ThaAttPdhManager self);
eAtRet ThaAttPdhManagerCheckForceRatePecentSet(ThaAttPdhManager self, uint8 percent);
uint8  ThaAttPdhManagerCheckForceRatePecentGet(ThaAttPdhManager self);
eAtRet ThaAttPdhManagerAlarmDurationUnitSet(ThaAttPdhManager self, eAttPdhForceAlarmDurationUnit unit);
eAttPdhForceAlarmDurationUnit ThaAttPdhManagerAlarmDurationUnitGet(ThaAttPdhManager self);
#ifdef __cplusplus
}
#endif
#endif /* _THAATTPDHMANAGER_H_ */

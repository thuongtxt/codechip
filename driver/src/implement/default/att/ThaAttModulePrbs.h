/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : ThaModulePrbs.h
 * 
 * Created Date: Aug 23, 2013
 *
 * Description : PRBS module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAATTMODULEPRBS_H_
#define _THAATTMODULEPRBS_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModulePrbs.h" /* Super class */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaAttModulePrbs * ThaAttModulePrbs;
typedef struct tThaAttPrbsRegProvider * ThaAttPrbsRegProvider;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePrbs ThaAttModulePrbsNew(AtDevice device);
ThaAttPrbsRegProvider Tha6A000010AttPrbsRegProviderNew(ThaAttModulePrbs prbsModule);
ThaAttPrbsRegProvider Tha6A290021AttPrbsRegProviderNew(ThaAttModulePrbs prbsModule);
ThaAttPrbsRegProvider Tha6A290022AttPrbsRegProviderNew(ThaAttModulePrbs prbsModule);

ThaAttPrbsRegProvider ThaAttModulePrbsRegProvider(ThaAttModulePrbs self);
eBool ThaAttModulePrbsNeedDelayAverage(ThaAttModulePrbs self);


/* Product concretes */
ThaAttPrbsRegProvider ThaAttPrbsRegProviderNew(ThaAttModulePrbs prbsModule);

#ifdef __cplusplus
}
#endif
#endif /* _THAATTMODULEPRBS_H_ */


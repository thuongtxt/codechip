/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : ThaAttPdhManagerInternal.h
 * 
 * Created Date: July 26, 2017
 *
 * Author      : chaudpt
 *
 * Description : ATT PDH manager class descriptor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAATTSDHMANAGERINTERNAL_H_
#define _THAATTSDHMANAGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/att/AtAttSdhManagerInternal.h"
#include "../../../../include/att/AtAttController.h"
#include "ThaAttSdhManager.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaAttSdhManagerMethods
    {
    eAtRet (*SetResetTimeAllHoChannel)(ThaAttSdhManager self);
    eAtRet (*SetResetTimeAllLoChannel)(ThaAttSdhManager self);

    eAtRet (*ForcePeriodSet)(ThaAttSdhManager self, uint32 numSeconds, uint32 regAddr, uint32 regFieldMask);
    uint32 (*ForcePeriodGet)(ThaAttSdhManager self, uint32 regAddr, uint32 regFieldMask);

    eAtRet (*DebugHoPointerLooptimeEnable)(ThaAttSdhManager self, eBool en);
    eAtRet (*DebugLoPointerLooptimeEnable)(ThaAttSdhManager self, eBool en);

    eAtRet (*DebugCheckForceRateShowSet)(ThaAttSdhManager self, eBool en);
    eBool  (*DebugCheckForceRateShowGet)(ThaAttSdhManager self);
    eAtRet (*DebugCheckForceRateEnSet)(ThaAttSdhManager self, eBool en);
    eBool  (*DebugCheckForceRateEnGet)(ThaAttSdhManager self);
    eAtRet (*DebugCheckForceRatePecentSet)(ThaAttSdhManager self, uint8 percent);
    uint8  (*DebugCheckForceRatePecentGet)(ThaAttSdhManager self);
    eAtRet (*AlarmDurationUnitSet)(ThaAttSdhManager self, eAttSdhForceAlarmDurationUnit unit);
    eAttSdhForceAlarmDurationUnit (*AlarmDurationUnitGet)(ThaAttSdhManager self);
    eAtRet (*SourceMultiplexingResync)(ThaAttSdhManager self);
    }tThaAttSdhManagerMethods;

typedef struct tThaAttSdhG783
    {
    uint32 T0;
    uint32 T1;
    uint32 T2;
    uint32 T3;
    uint32 T3Note2;
    uint32 T4;
    float  T4Note5;
    uint32 T4Note6;
    uint32 T5;
    uint32 T5Note6;
    eBool  debug;
    uint32  period;
    }tThaAttSdhG783;
typedef struct tThaAttSdhManager
    {
    tAtAttSdhManager super;
    const tThaAttSdhManagerMethods *methods;

    /* Forcing from ATT or DUT */
    eBool forceFromDut;
    eBool forceRateShowEn;
    eBool forceRateCheckEn;
    uint8 forceRateCheckPercent;
    uint8 SohColRemove;
    AtOsalMutex mutex;
    uint32 lock;
    uint32 unlock;
    AtList sdhPathForcePointer;
    tThaAttSdhG783 g783;
    eAttSdhForceAlarmDurationUnit alarmDurationUnit;
    uint32 forcePointerExecuteTimeMs;
    uint32 forcePointerNumChannel;
    }tThaAttSdhManager;


typedef struct tAtAttForcePointer
    {
    AtAttController           att;
    uint32                    pointerType;
    eAtAttForcePointerState   state;
    uint32                    waitTime; /* in mili seconds*/
    eAtAttForcePointerAdjMode mode;
    tAtOsalCurTime            start_time;
    tAtOsalCurTime            begin_time;
    uint32                    count;
    uint32                    intervalcount;
    }tAtAttForcePointer;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtAttSdhManager ThaAttSdhManagerObjectInit(AtAttSdhManager self, AtModuleSdh sdh);

/* Debugger */

#ifdef __cplusplus
}
#endif
#endif /* _THAATTSDHMANAGERINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : ThaAttPdhManagerInternal.h
 * 
 * Created Date: July 26, 2017
 *
 * Author      : chaudpt
 *
 * Description : ATT PDH manager class descriptor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAATTPDHMANAGERINTERNAL_H_
#define _THAATTPDHMANAGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/att/AtAttPdhManagerInternal.h"
#include "ThaAttPdhManager.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaAttPdhManagerMethods
    {
    eAtRet   (*SetResetTimeAllDe3Channel)(ThaAttPdhManager self);
    eAtRet   (*SetResetTimeAllDe1Channel)(ThaAttPdhManager self);
    AtPdhDe1 (*De1ChannelFromFlatId)(ThaAttPdhManager self, uint32 flatHwId);

    eAtRet (*ForcePeriodSet)(ThaAttPdhManager self, uint32 numSeconds, uint32 regAddr, uint32 regFieldMask);
    uint32 (*ForcePeriodGet)(ThaAttPdhManager self, uint32 regAddr, uint32 regFieldMask);

    eAtRet (*DebugCheckForceRateShowSet)(ThaAttPdhManager self, eBool en);
    eBool  (*DebugCheckForceRateShowGet)(ThaAttPdhManager self);
    eAtRet (*DebugCheckForceRateEnSet)(ThaAttPdhManager self, eBool en);
    eBool  (*DebugCheckForceRateEnGet)(ThaAttPdhManager self);
    eAtRet (*DebugCheckForceRatePecentSet)(ThaAttPdhManager self, uint8 percent);
    uint8  (*DebugCheckForceRatePecentGet)(ThaAttPdhManager self);
    eAtRet (*AlarmDurationUnitSet)(ThaAttPdhManager self, eAttPdhForceAlarmDurationUnit unit);
    eAttPdhForceAlarmDurationUnit (*AlarmDurationUnitGet)(ThaAttPdhManager self);
    } tThaAttPdhManagerMethods;

typedef struct tThaAttPdhManager
    {
    tAtAttPdhManager super;
    const tThaAttPdhManagerMethods *methods;

    /* Forcing from ATT or DUT */
    eBool forceFromDut;
    eBool forceRateShowEn;
    eBool forceRateCheckEn;
    uint8 forceRateCheckPercent;
    eAttPdhForceAlarmDurationUnit alarmDurationUnit;
    }tThaAttPdhManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtAttPdhManager ThaAttPdhManagerObjectInit(AtAttPdhManager self, AtModulePdh pdh);

/* Debugger */

#ifdef __cplusplus
}
#endif
#endif /* _THAATTPDHMANAGERINTERNAL_H_ */


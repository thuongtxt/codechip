/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : ThaPrbsRegProviderInternal.h
 * 
 * Created Date: Jan 6, 2016
 *
 * Description : Register provider
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAATTPRBSREGPROVIDERINTERNAL_H_
#define _THAATTPRBSREGPROVIDERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/man/AtDriverInternal.h" /* For OSAL */
#include "AtObject.h" /* For object related macros */
#include "ThaAttPrbsRegProvider.h"
#include "ThaAttModulePrbs.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaAttPrbsRegProviderMethods
    {
    /* Factor to calculation channel ID */

    /* Registers */
    uint32 (*PrbsGenReg)(ThaAttPrbsRegProvider self, eBool isHo);
    uint32 (*PrbsMonReg)(ThaAttPrbsRegProvider self, eBool isHo);
    uint8 (*LoPrbsEngineOc48IdShift)(ThaAttPrbsRegProvider self);
    uint8 (*LoPrbsEngineOc24SliceIdShift)(ThaAttPrbsRegProvider self);

    uint32 (*PrbsDelayConfigReg)(ThaAttPrbsRegProvider self, eBool isHo, uint8 slice);
    uint32 (*PrbsDelayIdMask)(ThaAttPrbsRegProvider self, eBool isHo);
    uint32 (*PrbsDelayEnableMask)(ThaAttPrbsRegProvider self, eBool isHo);
    uint32 (*PrbsMaxDelayReg)(ThaAttPrbsRegProvider self, eBool isHo, eBool r2c, uint8 slice);
    uint32 (*PrbsMinDelayReg)(ThaAttPrbsRegProvider self, eBool isHo, eBool r2c, uint8 slice);
    uint32 (*PrbsAverageDelayReg)(ThaAttPrbsRegProvider self, eBool isHo, eBool r2c, uint8 slice);

    uint32 (*PrbsModeMask)(ThaAttPrbsRegProvider self, eBool isHo, eBool isTx);
	uint32 (*PrbsStepMask)(ThaAttPrbsRegProvider self, eBool isHo, eBool isTx);
	uint32 (*PrbsEnableMask)(ThaAttPrbsRegProvider self, eBool isHo, eBool isTx);
	uint32 (*PrbsFixPatternMask)(ThaAttPrbsRegProvider self, eBool isHo, eBool isTx);
	uint32 (*PrbsFixPatternReg)(ThaAttPrbsRegProvider self, eBool isHo, eBool isTx);

	uint32 (*PrbsCounterReg)(ThaAttPrbsRegProvider self, eBool isHo, eBool r2c);
	uint32 (*PrbsCounterMask)(ThaAttPrbsRegProvider self, uint16 counterType);

    /* Bit fields */

    }tThaAttPrbsRegProviderMethods;

typedef struct tThaAttPrbsRegProvider
    {
    tAtObject super;
    const tThaAttPrbsRegProviderMethods *methods;
    ThaAttModulePrbs prbsModule;
    }tThaAttPrbsRegProvider;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaAttPrbsRegProvider ThaAttPrbsRegProviderObjectInit(ThaAttPrbsRegProvider self, ThaAttModulePrbs prbsModule);

#ifdef __cplusplus
}
#endif
#endif /* _THAATTPRBSREGPROVIDERINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : ThaPrbsRegProvider.h
 * 
 * Created Date: Jan 6, 2016
 *
 * Description : Register provider
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAATTPRBSREGPROVIDER_H_
#define _THAATTPRBSREGPROVIDER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaAttModulePrbs.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* PRBS Module */
ThaAttModulePrbs ThaAttPrbsRegProviderPrbsModule(ThaAttPrbsRegProvider self);

/* Reg Provider */
uint32 ThaAttPrbsRegProviderPrbsGenCtrlReg(ThaAttPrbsRegProvider self, eBool isHo);
uint32 ThaAttPrbsRegProviderPrbsMonCtrlReg(ThaAttPrbsRegProvider self, eBool isHo);
uint8 ThaAttPrbsRegProviderLoPrbsEngineOc48IdShift(ThaAttPrbsRegProvider self);
uint8 ThaAttPrbsRegProviderLoPrbsEngineOc24SliceIdShift(ThaAttPrbsRegProvider self);
uint32 ThaAttPrbsRegProviderPrbsFixPatternReg(ThaAttPrbsRegProvider self, eBool isHo, eBool isTx);
uint32 ThaAttPrbsRegProviderPrbsDelayEnableMask(ThaAttPrbsRegProvider self, eBool isHo);
uint32 ThaAttPrbsRegProviderPrbsDelayIdMask(ThaAttPrbsRegProvider self, eBool isHo);
uint32 ThaAttPrbsRegProviderPrbsDelayConfigReg(ThaAttPrbsRegProvider self, eBool isHo, uint8 slice);
uint32 ThaAttPrbsRegProviderPrbsMaxDelayReg(ThaAttPrbsRegProvider self, eBool isHo, eBool r2c, uint8 slice);
uint32 ThaAttPrbsRegProviderPrbsMinDelayReg(ThaAttPrbsRegProvider self, eBool isHo, eBool r2c, uint8 slice);
#ifdef __cplusplus
}
#endif
#endif /* _THAATTPRBSREGPROVIDER_H_ */


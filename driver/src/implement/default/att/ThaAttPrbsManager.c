/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : AtModulePdh.c
 *
 * Created Date: July 26, 2017
 *
 * Author      : chaudpt
 *
 * Description : Default implementation of PDH module of Thalassa device
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../generic/prbs/AtModulePrbsInternal.h"
#include "../../../generic/att/AtAttWanderChannelInternal.h"
#include "../att/ThaAttController.h"
#include "ThaAttPrbsManagerInternal.h"
#include "../prbs/ThaModulePrbsInternal.h"
#include "../att/ThaAttPrbsManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaAttPrbsManager)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtAttPrbsManagerMethods m_AtAttPrbsManagerOverride;
/* Save super implementations */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Internal functions -----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet   Init(AtAttPrbsManager self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet TimeMeasurementErrorSet(AtAttPrbsManager self, uint32 error)
    {
    mThis(self)->timeMeasurementError = error;
    return cAtOk;
    }

static uint32 TimeMeasurementErrorGet(AtAttPrbsManager self)
    {
    return mThis(self)->timeMeasurementError;
    }

/*-----------------------------------------------------------------------*/

static void OverrideAtAttSdhManager(AtAttPrbsManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtAttPrbsManagerOverride, mMethodsGet(self), sizeof(m_AtAttPrbsManagerOverride));
        mMethodOverride(m_AtAttPrbsManagerOverride, Init);
        mMethodOverride(m_AtAttPrbsManagerOverride, TimeMeasurementErrorSet);
        mMethodOverride(m_AtAttPrbsManagerOverride, TimeMeasurementErrorGet);
        }

    mMethodsSet(self, &m_AtAttPrbsManagerOverride);
    }

static void Override(ThaAttPrbsManager self)
    {
    OverrideAtAttSdhManager((AtAttPrbsManager)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaAttPrbsManager);
    }


AtAttPrbsManager ThaAttPrbsManagerObjectInit(AtAttPrbsManager self, AtModulePrbs prbs)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtAttPrbsManagerObjectInit(self, prbs) == NULL)
        return NULL;

    /* Setup class */
    Override(mThis(self));
    m_methodsInit = 1;
    mThis(self)->timeMeasurementError = 1000;
    return self;
    }

AtAttPrbsManager ThaAttPrbsManagerNew(AtModulePrbs prbs)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtAttPrbsManager newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ThaAttPrbsManagerObjectInit(newModule, prbs);
    }

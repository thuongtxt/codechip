/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date: Sep 8, 2017                                                    
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef __REG_ATTCES_RD_MRWANDER_H_
#define __REG_ATTCES_RD_MRWANDER_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Measure Wander Control
Reg Addr   : 0x0000
Reg Formula: 0x0000
    Where  : 
Reg Desc   : 
The Control is use to configure Mode to measure TIE.

------------------------------------------------------------------------------*/
#define cReg_Measure_Wander_Control                                                                     0x0000
#define cReg_Measure_Wander_Control_WidthVal                                                                32

/*--------------------------------------
BitField Name: FlushCfg
BitField Type: RW
BitField Desc: Flush Configure, Create Strobe to restart checking TIE, write 1
then 0
BitField Bits: [2]
--------------------------------------*/
#define c_Measure_Wander_Control_FlushCfg_Mask                                                           cBit2
#define c_Measure_Wander_Control_FlushCfg_Shift                                                              2

/*--------------------------------------
BitField Name: InvertCfg
BitField Type: RW
BitField Desc: Invert Configure, 1 : sample clock at positive edge, 1 : sample
at negitive
BitField Bits: [1]
--------------------------------------*/
#define c_Measure_Wander_Control_InvertCfg_Mask                                                          cBit1
#define c_Measure_Wander_Control_InvertCfg_Shift                                                             1

/*--------------------------------------
BitField Name: ModeCfg
BitField Type: RW
BitField Desc: Mode Configure, 1 : clock refer to receive clock, 0 : refer to
reference clock
BitField Bits: [0]
--------------------------------------*/
#define c_Measure_Wander_Control_ModeCfg_Mask                                                            cBit0
#define c_Measure_Wander_Control_ModeCfg_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Measure Wander FifoLength
Reg Addr   : 0x0001
Reg Formula: 0x0001
    Where  :
Reg Desc   :
The Control is use to configure Mode to measure TIE.

------------------------------------------------------------------------------*/
#define cReg_Measure_Wander_FifoLength                                                                     0x0001
#define cReg_Measure_Wander_FifoLength_WidthVal                                                                32


/*------------------------------------------------------------------------------
Reg Name   : Measure Wander Status
Reg Addr   : 8000 - 0x8029
Reg Formula: 8000 +  portid
    Where  : 
           + $portid(0-47):
Reg Desc   : 
The Status is use to draw graph TIE for per LIU port.

------------------------------------------------------------------------------*/
#define cReg_Measure_Wander_Status                                                                        0x8000

/*--------------------------------------
BitField Name: UnitCnt
BitField Type: RW
BitField Desc: Unit Counter, the deviation between Tx Clock and Rx Clock/Ref
clock depend on control register
BitField Bits: [31:7]
--------------------------------------*/
#define c_Measure_Wander_Status_UnitCnt_Mask                                                          cBit31_7
#define c_Measure_Wander_Status_UnitCnt_Shift                                                                7

/*--------------------------------------
BitField Name: Signcnt
BitField Type: RW
BitField Desc: Sign indicate Nclkcnt is positive or negative sign.
BitField Bits: [25:25]
--------------------------------------*/
#define c_Measure_Wander_Status_Signcnt_Mask                                                             cBit6
#define c_Measure_Wander_Status_Signcnt_Shift                                                                6

/*--------------------------------------
BitField Name: NClkCnt
BitField Type: RW
BitField Desc: Number Clock Counter, the deviation caculate from egde of Tx
Clock to edge of Rx Clock/Ref clock depend on control register
BitField Bits: [24:0]
--------------------------------------*/
#define c_Measure_Wander_Status_NClkCnt_Mask                                                          cBit5_0
#define c_Measure_Wander_Status_NClkCnt_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Measure Wander Status v2
Reg Addr   : 0002
Reg Formula:
Reg Desc   :
The Status is use to draw graph TIE for per LIU port.
// Field: [31:26] %%  Portid %% Port ID , TIE of which channel  %% RW %% 0x0 %% 0x0
// Field: [25:7] %%  UnitCnt %% Unit Counter, the deviation between Tx Clock and Rx Clock/Ref clock depend on control register %% RW %% 0x0 %% 0x0
// Field: [6]  %%   Signcnt %% Sign indicate Nclkcnt is positive or negative sign.
// Field: [5:0] %%  NClkCnt %% Number Clock Counter, the deviation caculate from egde of Tx Clock to edge of Rx Clock/Ref clock depend on control register %% RW %% 0x0 %% 0x0
------------------------------------------------------------------------------*/
#define cReg_Measure_Wander_StatusV2                                                                        0x0002

/*--------------------------------------
BitField Name: PortId
BitField Type: RW
BitField Desc: Sign indicate Nclkcnt is positive or negative sign.
BitField Bits: [31:26]
--------------------------------------*/
#define c_Measure_Wander_StatusV2_PortId_Mask                                                           cBit31_26
#define c_Measure_Wander_StatusV2_PortId_Shift                                                                 26

/*--------------------------------------
BitField Name: UnitCnt
BitField Type: RW
BitField Desc: Unit Counter, the deviation between Tx Clock and Rx Clock/Ref
clock depend on control register
BitField Bits: [25:7]
--------------------------------------*/
#define c_Measure_Wander_StatusV2_UnitCnt_Mask                                                          cBit25_7
#define c_Measure_Wander_StatusV2_UnitCnt_Shift                                                                7

/*--------------------------------------
BitField Name: Signcnt
BitField Type: RW
BitField Desc: Sign indicate Nclkcnt is positive or negative sign.
BitField Bits: [6:6]
--------------------------------------*/
#define c_Measure_Wander_StatusV2_Signcnt_Mask                                                             cBit6
#define c_Measure_Wander_StatusV2_Signcnt_Shift                                                                6

/*--------------------------------------
BitField Name: NClkCnt
BitField Type: RW
BitField Desc: Number Clock Counter, the deviation caculate from egde of Tx
Clock to edge of Rx Clock/Ref clock depend on control register
BitField Bits: [5:0]
--------------------------------------*/
#define c_Measure_Wander_StatusV2_NClkCnt_Mask                                                          cBit5_0
#define c_Measure_Wander_StatusV2_NClkCnt_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Measure Wander Status v3
Reg Addr   : 0002
Reg Formula:
Reg Desc   :
The Status is use to draw graph TIE for per LIU port.
{portid[31:26],unitcnt[25:8],signcnt[7],Nclkcnt[6:0]}
------------------------------------------------------------------------------*/
#define cReg_Measure_Wander_StatusV3                                                                        0x0002

/*--------------------------------------
BitField Name: PortId
BitField Type: RW
BitField Desc: Sign indicate Nclkcnt is positive or negative sign.
BitField Bits: [31:26]
--------------------------------------*/
#define c_Measure_Wander_StatusV3_PortId_Mask                                                           cBit31_26
#define c_Measure_Wander_StatusV3_PortId_Shift                                                                 26

/*--------------------------------------
BitField Name: UnitCnt
BitField Type: RW
BitField Desc: Unit Counter, the deviation between Tx Clock and Rx Clock/Ref
clock depend on control register
BitField Bits: [25:8]
--------------------------------------*/
#define c_Measure_Wander_StatusV3_UnitCnt_Mask                                                          cBit25_8
#define c_Measure_Wander_StatusV3_UnitCnt_Shift                                                                8

/*--------------------------------------
BitField Name: Signcnt
BitField Type: RW
BitField Desc: Sign indicate Nclkcnt is positive or negative sign.
BitField Bits: [7:7]
--------------------------------------*/
#define c_Measure_Wander_StatusV3_Signcnt_Mask                                                             cBit7
#define c_Measure_Wander_StatusV3_Signcnt_Shift                                                                7

/*--------------------------------------
BitField Name: NClkCnt
BitField Type: RW
BitField Desc: Number Clock Counter, the deviation caculate from egde of Tx
Clock to edge of Rx Clock/Ref clock depend on control register
BitField Bits: [6:0]
--------------------------------------*/
#define c_Measure_Wander_StatusV3_NClkCnt_Mask                                                          cBit6_0
#define c_Measure_Wander_StatusV3_NClkCnt_Shift                                                               0


#endif /* __REG_ATTCES_RD_MRWANDER_H_ */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ATT
 * 
 * File        : AtAttController.h
 * 
 * Created Date: May 16, 2016
 *
 * Description : ATT controller header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAATTCONTROLLER_H_
#define _THAATTCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPdhDe3.h"
#include "AtPdhDe1.h"
#include "AtAttController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaAttController * ThaAttController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 ThaAttControllerFrequenceOffsetControlRegister(ThaAttController self);
uint32 ThaAttControllerFrequenceOffsetHoldoverRegister(ThaAttController);
uint32 ThaAttControllerFrequenceOffsetDefaultOffset(ThaAttController self);
tAtOsalCurTime* ThaAttControllerForcePointerBeginTimeGet(ThaAttController self);

#ifdef __cplusplus
}
#endif
#endif /* _THAATTCONTROLLER_H_ */


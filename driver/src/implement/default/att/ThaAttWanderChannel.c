/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ATT
 *
 * File        : ThaAtWanderChannel.c
 *
 * Created Date: Jun 13, 2017
 *
 * Description : ATT controller abstract
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaAttWanderChannelInternal.h"
#include "../../../generic/common/AtChannelInternal.h" /*AtChannelHwReadOnModule, AtChannelHwWriteOnModule */
#include "ThaAttControllerInternal.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaAttWanderChannel)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaAttWanderChannelMethods m_methods;

/* Override */
static tAtAttWanderChannelMethods m_AtAttWanderChannelOverride;

/* Save super implementation */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ClockRate(AtAttWanderChannel self)
    {
    ThaAttController att = (ThaAttController)AtChannelAttController(self->channel);
    if (att)
        return ThaAttControllerChannelSpeed(att);
    return 0;
    }


static uint32 WanderValue(AtAttWanderChannel self, eBool *sign, uint8 *NClkCnt)
    {
    ThaAttWanderChannel _self = mThis(self);
    uint32 address = mMethodsGet(_self)->MeasureWanderStatus(_self);
    uint32 regValue = mMethodsGet(_self)->WanderRead(_self, address);
    uint32 regFieldMask = mMethodsGet(mThis(self))->UnitCntMask(mThis(self));
    uint32 regFieldShift = AtRegMaskToShift(regFieldMask);
    uint32 numclock_of_system_clock = (uint32)mRegField(regValue, regField);

    regFieldMask = mMethodsGet(mThis(self))->NClkCntMask(mThis(self));
    regFieldShift = AtRegMaskToShift(regFieldMask);
    *NClkCnt = (uint8)mRegField(regValue, regField);

    regFieldMask = mMethodsGet(mThis(self))->SignCntMask(mThis(self));
    regFieldShift = AtRegMaskToShift(regFieldMask);
    *sign = (uint8)mRegField(regValue, regField);
    return numclock_of_system_clock;
    }

static void OverrideAtAttWanderChannel(AtAttWanderChannel self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtAttWanderChannelOverride, mMethodsGet(self), sizeof(m_AtAttWanderChannelOverride));

        mMethodOverride(m_AtAttWanderChannelOverride, WanderValue);
        mMethodOverride(m_AtAttWanderChannelOverride, ClockRate);
        }

    mMethodsSet(self, &m_AtAttWanderChannelOverride);
    }

static void Override(AtAttWanderChannel self)
    {
    OverrideAtAttWanderChannel((AtAttWanderChannel)self);
    }

static uint32 MeasureWanderControl(ThaAttWanderChannel self)
    {
    AtUnused(self);
    return cReg_Measure_Wander_Control;
    }

static uint32 MeasureWanderStatus (ThaAttWanderChannel self)
    {
    AtUnused(self);
    return cReg_Measure_Wander_Status;
    }

static uint32 MeasureWanderOffset(ThaAttWanderChannel self)
    {
    return AtChannelIdGet(((AtAttWanderChannel)self)->channel) + 0x840000;
    }

static uint32 ModeCfgMask(ThaAttWanderChannel self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 InvertCfgMask(ThaAttWanderChannel self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 FlushCfgMask(ThaAttWanderChannel self)
    {
    AtUnused(self);
    return 0;
    }
static uint32 SignCntMask(ThaAttWanderChannel self)
    {
    AtUnused(self);
    return c_Measure_Wander_Status_Signcnt_Mask;
    }
static uint32 UnitCntMask(ThaAttWanderChannel self)
    {
    AtUnused(self);
    return c_Measure_Wander_Status_UnitCnt_Mask;
    }

static uint32 NClkCntMask(ThaAttWanderChannel self)
    {
    AtUnused(self);
    return c_Measure_Wander_Status_NClkCnt_Mask;
    }

static eAtModule ModuleId(ThaAttWanderChannel self)
    {
    AtChannel channel = ((AtAttWanderChannel)self)->channel;
    AtModule module = AtChannelModuleGet(channel);
    eAtModule moduleId = AtModuleTypeGet(module);
    return moduleId;
    }

static uint32 WanderRead(ThaAttWanderChannel self, uint32 regAddress)
    {
    AtChannel channel = ((AtAttWanderChannel)self)->channel;
    uint32 realAddress = regAddress + mMethodsGet(self)->MeasureWanderOffset(self);
    uint32 value = AtChannelHwReadOnModule(channel,realAddress, ModuleId(self));
    return value;
    }

static eAtRet WanderWrite(ThaAttWanderChannel self, uint32 regAddress, uint32 regValue)
    {
    AtChannel channel = ((AtAttWanderChannel)self)->channel;
    uint32 realAddress = regAddress + mMethodsGet(self)->MeasureWanderOffset(self);
    AtChannelHwWriteOnModule(channel,realAddress, regValue, ModuleId(self));
    return cAtOk;
    }

static void MethodsInit(ThaAttWanderChannel self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, MeasureWanderControl);
        mMethodOverride(m_methods, MeasureWanderStatus);
        mMethodOverride(m_methods, MeasureWanderOffset);
        mMethodOverride(m_methods, ModeCfgMask);
        mMethodOverride(m_methods, FlushCfgMask);
        mMethodOverride(m_methods, InvertCfgMask);
        mMethodOverride(m_methods, UnitCntMask);
        mMethodOverride(m_methods, SignCntMask);
        mMethodOverride(m_methods, NClkCntMask);
        mMethodOverride(m_methods, WanderRead);
        mMethodOverride(m_methods, WanderWrite);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaAttWanderChannel);
    }

AtAttWanderChannel ThaAttWanderChannelObjectInit(AtAttWanderChannel self, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtAttWanderChannelObjectInit(self, channel) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(mThis(self));
    Override(self);
    m_methodsInit = 1;
    return self;
    }

AtAttWanderChannel ThaAttWanderChannelNew(AtChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtAttWanderChannel newObject = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newObject == NULL)
        return NULL;

    /* Construct it */
    return ThaAttWanderChannelObjectInit(newObject, channel);
    }


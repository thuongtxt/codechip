/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : ThaModulePrbs.c
 *
 * Created Date: Aug 23, 2013
 *
 * Description : PRBS module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaAttModulePrbsInternal.h"


/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaAttModulePrbs)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods      m_AtObjectOverride;
static tThaAttModulePrbsMethods m_ThaAttModulePrbsOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static ThaAttPrbsRegProvider RegProviderCreate(ThaAttModulePrbs self)
    {
    return ThaAttPrbsRegProviderNew(self);
    }

static ThaAttPrbsRegProvider RegProvider(ThaAttModulePrbs self)
    {
    AtUnused(self);
    if (mThis(self)->regProvider ==NULL)
        mThis(self)->regProvider = mMethodsGet(mThis(self))->RegProviderCreate(mThis(self));
    return mThis(self)->regProvider;
    }

static uint32 StartVersionSupportDelayAverage(ThaAttModulePrbs self)
	{
	AtUnused(self);
	return 0xFFFFFFFF;
	}

static eBool NeedDelayAverage(ThaAttModulePrbs self)
	{
	AtUnused(self);
	return cAtFalse;
	}

static void MethodsInit(ThaAttModulePrbs self)
    {
    if (!m_methodsInit)
        {
        mMethodOverride(m_ThaAttModulePrbsOverride, RegProviderCreate);
        mMethodOverride(m_ThaAttModulePrbsOverride, StartVersionSupportDelayAverage);
        mMethodOverride(m_ThaAttModulePrbsOverride, NeedDelayAverage);
        }

    mMethodsSet(self, &m_ThaAttModulePrbsOverride);
    }

static void DeleteRegProvider(ThaAttModulePrbs self)
    {
    if (self->regProvider !=NULL)
        AtObjectDelete((AtObject)(self->regProvider));
    self->regProvider = NULL;
    }

static void Delete(AtObject self)
    {
    /* Delete private data */
    DeleteRegProvider(mThis(self));

    /* Call super function to fully delete */
    m_AtObjectMethods->Delete((AtObject)self);
    }
static void OverrideAtObject(AtObject self)
    {

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(self, &m_AtObjectOverride);
    }

static void Override(ThaAttModulePrbs self)
    {
    OverrideAtObject((AtObject)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaAttModulePrbs);
    }

AtModulePrbs ThaAttModulePrbsObjectInit(AtModulePrbs self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtModulePrbsObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(mThis(self));
    MethodsInit(mThis(self));
    m_methodsInit = 1;
    mThis(self)->regProvider = NULL;

    return self;
    }

AtModulePrbs ThaAttModulePrbsNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePrbs newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ThaAttModulePrbsObjectInit(newModule, device);
    }

ThaAttPrbsRegProvider ThaAttModulePrbsRegProvider(ThaAttModulePrbs self)
    {
    if (self)
        return RegProvider(self);
    return NULL;
    }

eBool ThaAttModulePrbsNeedDelayAverage(ThaAttModulePrbs self)
	{
	if (self)
		return mMethodsGet(self)->NeedDelayAverage(self);
	return cAtFalse;
	}


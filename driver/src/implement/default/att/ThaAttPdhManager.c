/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : AtModulePdh.c
 *
 * Created Date: July 26, 2017
 *
 * Author      : chaudpt
 *
 * Description : Default implementation of PDH module of Thalassa device
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../generic/pdh/AtModulePdhInternal.h"
#include "../../../generic/att/AtAttWanderChannelInternal.h"
#include "../../../util/coder/AtCoderUtil.h"
#include "../att/ThaAttWanderChannelReg.h"
#include "../att/ThaAttController.h"
#include "ThaAttPdhManagerInternal.h"
#include "../pdh/ThaModulePdhInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaAttPdhManager)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaAttPdhManagerMethods m_methods;

/* Override */
static tAtAttPdhManagerMethods m_AtAttPdhManagerOverride;
/* Save super implementations */
static const tAtAttPdhManagerMethods  *m_AtAttPdhManagerMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Internal functions -----------------------------*/

/*--------------------------- Implementation ---------------------------------*/


/*-----------------------------------------------------------------------*/
static eAtRet   Init(AtAttPdhManager self)
    {
    eAtRet ret = m_AtAttPdhManagerMethods->Init(self);
    ret |= ThaAttPdhManagerDebugForcingLogicSet((ThaAttPdhManager) self, cAtFalse);
    ret |= ThaAttPdhManagerAlarmDurationUnitSet((ThaAttPdhManager) self, cAttPdhForceAlarmDurationUnitSecond);

    return ret;
    }

static eAtRet AlarmDurationUnitSet(ThaAttPdhManager self, eAttPdhForceAlarmDurationUnit unit)
    {
    AtUnused(self);
    self->alarmDurationUnit = unit;
    return cAtOk;
    }
static eAttPdhForceAlarmDurationUnit AlarmDurationUnitGet(ThaAttPdhManager self)
    {
    AtUnused(self);
    return self->alarmDurationUnit;
    }

static eAtRet DebugCheckForceRateShowSet(ThaAttPdhManager self, eBool en)
    {
    mThis(self)->forceRateShowEn = en;
    return cAtOk;
    }

static eBool  DebugCheckForceRateShowGet(ThaAttPdhManager self)
    {
    return mThis(self)->forceRateShowEn;
    }

static eAtRet DebugCheckForceRateEnSet(ThaAttPdhManager self, eBool en)
    {
    mThis(self)->forceRateCheckEn = en;
    return cAtOk;
    }

static eBool  DebugCheckForceRateEnGet(ThaAttPdhManager self)
    {
    return mThis(self)->forceRateCheckEn;
    }

static eAtRet DebugCheckForceRatePecentSet(ThaAttPdhManager self, uint8 percent)
    {
    mThis(self)->forceRateCheckPercent = percent;
    return cAtOk;
    }

static uint8  DebugCheckForceRatePecentGet(ThaAttPdhManager self)
    {
    return mThis(self)->forceRateCheckPercent;
    }

/*-----------------------------------------------------------------------*/
static eAtRet ForcePerframeSet(AtAttPdhManager self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtOk;
    }

static eAtRet SetResetTimeAllDe1Channel(ThaAttPdhManager self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet SetResetTimeAllDe3Channel(ThaAttPdhManager self)
    {
    AtUnused(self);
    return cAtOk;
    }

static AtPdhDe1 De1ChannelFromFlatId(ThaAttPdhManager self, uint32 flatHwId)
    {
    AtUnused(self);
    AtUnused(flatHwId);
    /*AtPdhDe1 de1Channel = ThaModulePdhDe1ChannelFromHwIdGet(self, (uint8)slice, de3, de2, de1);*/
    return NULL;
    }

/*-----------------------------------------------------------------------*/
static eAtRet De1TieInit(AtAttPdhManager self)
    {
    AtModulePdh pdh = AtAttPdhManagerModulePdh(self);
    uint32 address = cReg_Measure_Wander_Control + 0x840000;
    uint32 regVal = mModuleHwRead(pdh, address);

    /* Write 1 then write 0 */
    mFieldIns(&regVal, c_Measure_Wander_Control_FlushCfg_Mask, c_Measure_Wander_Control_FlushCfg_Shift, 1);
    mModuleHwWrite(pdh, address, regVal);
    AtOsalUSleep(2000);
    mFieldIns(&regVal, c_Measure_Wander_Control_FlushCfg_Mask, c_Measure_Wander_Control_FlushCfg_Shift, 0);
    mModuleHwWrite(pdh, address, regVal);
    return mMethodsGet(mThis(self))->SetResetTimeAllDe1Channel(mThis(self));
    }

static uint32 WanderValueV2(uint32 regValue, eBool *sign, uint8 *NClkCnt, uint32 *portId)
    {
    uint32 regFieldMask = c_Measure_Wander_StatusV2_UnitCnt_Mask;
    uint32 regFieldShift = AtRegMaskToShift(regFieldMask);
    uint32 numclock_of_system_clock = (uint32)mRegField(regValue, regField);

    regFieldMask = c_Measure_Wander_StatusV2_NClkCnt_Mask;
    regFieldShift = AtRegMaskToShift(regFieldMask);
    *NClkCnt = (uint8)mRegField(regValue, regField);

    regFieldMask = c_Measure_Wander_StatusV2_Signcnt_Mask;
    regFieldShift = AtRegMaskToShift(regFieldMask);
    *sign = (uint8)mRegField(regValue, regField);

    regFieldMask = c_Measure_Wander_StatusV2_PortId_Mask;
    regFieldShift = AtRegMaskToShift(regFieldMask);
    *portId = (uint32)mRegField(regValue, regField);
    return numclock_of_system_clock;
    }

static uint32 WanderValueV3(uint32 regValue, eBool *sign, uint8 *NClkCnt, uint32 *portId)
    {
    uint32 regFieldMask = c_Measure_Wander_StatusV3_UnitCnt_Mask;
    uint32 regFieldShift = AtRegMaskToShift(regFieldMask);
    uint32 numclock_of_system_clock = (uint32)mRegField(regValue, regField);

    regFieldMask = c_Measure_Wander_StatusV3_NClkCnt_Mask;
    regFieldShift = AtRegMaskToShift(regFieldMask);
    *NClkCnt = (uint8)mRegField(regValue, regField);

    regFieldMask = c_Measure_Wander_StatusV3_Signcnt_Mask;
    regFieldShift = AtRegMaskToShift(regFieldMask);
    *sign = (uint8)mRegField(regValue, regField);

    regFieldMask = c_Measure_Wander_StatusV3_PortId_Mask;
    regFieldShift = AtRegMaskToShift(regFieldMask);
    *portId = (uint32)mRegField(regValue, regField);
    return numclock_of_system_clock;
    }

static uint32 WanderValue(uint32 regValue, eBool *sign, uint8 *NClkCnt, uint32 *portId)
    {
    if (0) return WanderValueV2(regValue, sign, NClkCnt, portId);
    return WanderValueV3(regValue, sign, NClkCnt, portId);
    }

static eAtRet De1TieBufRead(AtAttPdhManager self)
    {
    AtModulePdh pdh = AtAttPdhManagerModulePdh(self);
    uint32 address = cReg_Measure_Wander_FifoLength + 0x840000;
    uint32 regVal = mModuleHwRead(pdh, address);
    uint32 length = regVal;
    uint32 fifo_i = 0;
    /* Write 1 then write 0 */
    if (AtDeviceIsSimulated(AtModuleDeviceGet((AtModule)pdh))) length = 0x30;
    if (length>0)
        {
        for (fifo_i=0; fifo_i<length; fifo_i++)
            {
            uint32                numclock= 0, de1Id=0;
            eBool                 sign    = cAtTrue;
            uint8                 NClkCnt = 0;
            AtPdhDe1              de1Channel=NULL;
            AtAttController       att    = NULL;
            AtAttWanderChannel    wander = NULL;
            address             = cReg_Measure_Wander_StatusV2 + 0x840000;
            regVal              = mModuleHwRead(pdh, address);
            numclock            = WanderValue(regVal, &sign,&NClkCnt, &de1Id);
            de1Channel          = mMethodsGet(mThis(self))->De1ChannelFromFlatId(mThis(self), de1Id);
            att = AtChannelAttController((AtChannel)de1Channel);
            wander = AtAttControllerWanderGet(att);
            AtAttWanderChannelAddValue(wander, numclock, sign, NClkCnt, regVal);
            }
        }
    return cAtOk;
    }

static  AtAttWanderChannel WanderGet(AtChannel channel)
    {
    if (channel)
        {
        AtAttController att = AtChannelAttController(channel);
        if (att)
            {
            AtAttWanderChannel wander = AtAttControllerWanderGet(att);
            return wander;
            }
        }
    return NULL;
    }
static void De3TieProcess(AtAttPdhManager mngr)
    {
    AtModulePdh self = mngr->pdh;
    uint32 de3Id =0;
    uint32 numde3 = AtModulePdhNumberOfDe3sGet(self);
    for (de3Id=0; de3Id<numde3; numde3++)
        {
        AtPdhDe3 de3 = AtModulePdhDe3Get(self, de3Id);
        AtAttWanderChannel wander =  WanderGet((AtChannel)de3);
        if (wander)
            AtAttWanderChannelPeriodicProcess(wander);
        }
    }

static void SaveDe3TieProcess(AtAttPdhManager self)
    {
    AtModulePdh pdh = AtAttPdhManagerModulePdh(self);
    uint32 de3Id =0;
    uint32 numde3 = AtModulePdhNumberOfDe3sGet(pdh);
    for (de3Id=0; de3Id<numde3; numde3++)
        {
        AtPdhDe3 de3 = AtModulePdhDe3Get(pdh, de3Id);
        AtAttWanderChannel wander =  WanderGet((AtChannel)de3);
        if (wander)
            AtAttWanderChannelSavePeriodicProcess(wander);
        }
    }

static void De1TieProcess(AtAttPdhManager self)
    {
    AtModulePdh pdh = AtAttPdhManagerModulePdh(self);
    uint32 de1Id =0;
    uint32 numde1 = AtModulePdhNumberOfDe1sGet(pdh);
    for (de1Id=0; de1Id<numde1; de1Id++)
        {
        AtPdhDe1 de1 = AtModulePdhDe1Get(pdh, de1Id);
        AtAttWanderChannel wander =  WanderGet((AtChannel)de1);
        if (wander)
            AtAttWanderChannelPeriodicProcess(wander);
        }
    }

static void SaveDe1TieProcess(AtAttPdhManager self)
    {
    AtModulePdh pdh = AtAttPdhManagerModulePdh(self);
    uint32 de1Id =0;
    uint32 numde1 = AtModulePdhNumberOfDe1sGet(pdh);
    for (de1Id=0; de1Id<numde1; de1Id++)
        {
        AtPdhDe1 de1 = AtModulePdhDe1Get(pdh, de1Id);
        AtAttWanderChannel wander =  WanderGet((AtChannel)de1);
        if (wander)
            AtAttWanderChannelSavePeriodicProcess(wander);
        }
    }


static void OverrideAtAttPdhManager(AtAttPdhManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtAttPdhManagerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtAttPdhManagerOverride, m_AtAttPdhManagerMethods, sizeof(m_AtAttPdhManagerOverride));
        mMethodOverride(m_AtAttPdhManagerOverride, Init);
        mMethodOverride(m_AtAttPdhManagerOverride, De1TieInit);
        mMethodOverride(m_AtAttPdhManagerOverride, De1TieBufRead);
        mMethodOverride(m_AtAttPdhManagerOverride, De3TieProcess);
        mMethodOverride(m_AtAttPdhManagerOverride, De1TieProcess);
        mMethodOverride(m_AtAttPdhManagerOverride, SaveDe3TieProcess);
        mMethodOverride(m_AtAttPdhManagerOverride, SaveDe1TieProcess);
        mMethodOverride(m_AtAttPdhManagerOverride, ForcePerframeSet);
        }

    mMethodsSet(self, &m_AtAttPdhManagerOverride);
    }

static void Override(ThaAttPdhManager self)
    {
    OverrideAtAttPdhManager((AtAttPdhManager)self);
    }

static void MethodsInit(ThaAttPdhManager self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));
        mMethodOverride(m_methods, SetResetTimeAllDe1Channel);
        mMethodOverride(m_methods, SetResetTimeAllDe3Channel);
        mMethodOverride(m_methods, De1ChannelFromFlatId);
        mMethodOverride(m_methods, DebugCheckForceRateShowSet);
        mMethodOverride(m_methods, DebugCheckForceRateShowGet);
        mMethodOverride(m_methods, DebugCheckForceRateEnSet);
        mMethodOverride(m_methods, DebugCheckForceRateEnGet);
        mMethodOverride(m_methods, DebugCheckForceRatePecentSet);
        mMethodOverride(m_methods, DebugCheckForceRatePecentGet);
        mMethodOverride(m_methods, AlarmDurationUnitSet);
        mMethodOverride(m_methods, AlarmDurationUnitGet);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaAttPdhManager);
    }

AtAttPdhManager ThaAttPdhManagerObjectInit(AtAttPdhManager self, AtModulePdh pdh)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtAttPdhManagerObjectInit(self, pdh) == NULL)
        return NULL;

    /* Setup class */
    Override(mThis(self));
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }


eAtRet ThaAttPdhManagerDebugForcingLogicSet(ThaAttPdhManager self, eBool isDut)
    {
    if (self)
        {
        self->forceFromDut = isDut;
        return cAtOk;
        }

    return cAtErrorNullPointer;
    }

eBool ThaAttPdhManagerDebugForcingLogicGet(ThaAttPdhManager self)
    {
    return (eBool)(self ? self->forceFromDut : cAtFalse);
    }

eAtRet ThaAttPdhManagerCheckForceRateShowSet(ThaAttPdhManager self, eBool en)
    {
    if (self)
        return mMethodsGet(self)->DebugCheckForceRateShowSet(self, en);
    return cAtErrorNullPointer;
    }

eBool  ThaAttPdhManagerCheckForceRateShowGet(ThaAttPdhManager self)
    {
    if (self)
        return mMethodsGet(self)->DebugCheckForceRateShowGet(self);
    return cAtFalse;
    }


eAtRet ThaAttPdhManagerCheckForceRateEnSet(ThaAttPdhManager self, eBool en)
    {
    if (self)
        return mMethodsGet(self)->DebugCheckForceRateEnSet(self, en);
    return cAtErrorNullPointer;
    }

eBool  ThaAttPdhManagerCheckForceRateEnGet(ThaAttPdhManager self)
    {
    if (self)
        return mMethodsGet(self)->DebugCheckForceRateEnGet(self);
    return cAtFalse;
    }

eAtRet ThaAttPdhManagerCheckForceRatePecentSet(ThaAttPdhManager self, uint8 percent)
    {
    if (self)
        return mMethodsGet(self)->DebugCheckForceRatePecentSet(self, percent);
    return cAtErrorNullPointer;
    }

uint8  ThaAttPdhManagerCheckForceRatePecentGet(ThaAttPdhManager self)
    {
    if (self)
        return mMethodsGet(self)->DebugCheckForceRatePecentGet(self);
    return 0;
    }


eAtRet ThaAttPdhManagerAlarmDurationUnitSet(ThaAttPdhManager self, eAttPdhForceAlarmDurationUnit unit)
    {
    mNumericalAttributeSet(AlarmDurationUnitSet, unit);
    }
eAttPdhForceAlarmDurationUnit ThaAttPdhManagerAlarmDurationUnitGet(ThaAttPdhManager self)
    {
    mAttributeGet(AlarmDurationUnitGet, eAttPdhForceAlarmDurationUnit, cAttPdhForceAlarmDurationUnitSecond);
    }

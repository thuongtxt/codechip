/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ATT
 *
 * File        : AtAttController.c
 *
 * Created Date: May 16, 2016
 *
 * Description : ATT controller abstract
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../include/att/AtAttPdhManager.h"
#include "../../../generic/att/AtAttControllerInternal.h"
#include "ThaAttControllerInternal.h"
#include "ThaAttWanderChannelInternal.h"
#include "ThaAttSdhManagerInternal.h"
#include "ThaAttPdhManager.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaAttController)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaAttControllerMethods m_methods;

/* Override */
static tAtAttControllerMethods m_AtAttControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtAttWanderChannel WanderCreate(AtChannel channel)
    {
    AtUnused(channel);
    return ThaAttWanderChannelNew(channel);
    }

static eBool  UseAdjust(ThaAttController self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    return cAtFalse;
    }

static uint32 MaxNumberofBytePerFrame(ThaAttController self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    return 1;
    }

static eBool NeedRemoveSoh(ThaAttController self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    return cAtFalse;
    }

static eBool  RateCalib(ThaAttController self, uint32 NbitPerFrame, uint32 Nrate, uint16 *err_step, uint16 *PositionMask, uint8 *frc_en_mask, uint8 *hs)
    {
    AtUnused(self);
    AtUnused(NbitPerFrame);
    AtUnused(Nrate);
    AtUnused(err_step);
    AtUnused(PositionMask);
    AtUnused(frc_en_mask);
    AtUnused(hs);
    return cAtFalse;
    }

static eAtRet  RateConvertStep(ThaAttController self, uint32 NbitPerFrame, uint32 Nrate, uint16 *err_step)
    {
    AtUnused(self);
    AtUnused(NbitPerFrame);
    AtUnused(Nrate);
    AtUnused(err_step);
    return cAtOk;
    }

static eBool  RateConvertStepIsSupport(ThaAttController self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 NbitPerFrameSoh(ThaAttController self)
    {
    AtUnused(self);
    return 0;
    }

static eBool IsDataMask8bit(ThaAttController self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    return cAtFalse;
    }

static eBool IsDataMask2bit(ThaAttController self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    return cAtFalse;
    }

static eBool IsStep16bit(ThaAttController self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    return cAtFalse;
    }

static uint32 StartVersionSupportDataMask8bit(ThaAttController self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    return 0xFFFFFFFF;
    }

static uint32 MaxStep(ThaAttController self, uint32 isError, uint32 errorType, uint32 Nrate, uint32 NbitPerFrame)
    {
    AtUnused(self);
    AtUnused(isError);
    AtUnused(errorType);
    AtUnused(Nrate);
    AtUnused(NbitPerFrame);
    return 255;
    }

static uint32 StartVersionSupportStep14bit(ThaAttController self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    return 0xFFFFFFFF;
    }

static uint32 StartVersionSupportStep16bit(ThaAttController self)
    {
    AtUnused(self);
    return 0xFFFFFFFF;
    }

static ThaCdrController CdrControllerGet(ThaAttController self)
    {
    AtUnused(self);
    return NULL;
    }

static uint32 FrequenceOffsetControlRegister(ThaAttController self)
    {
    AtUnused(self);
    return cRegAttFreqOffControl;
    }

static uint32 FrequenceOffsetHoldoverRegister(ThaAttController self)
    {
    AtUnused(self);
    return cRegAttFreqOffHoldover;
    }

static uint32 FrequenceOffsetDefaultOffset(ThaAttController self)
    {
    ThaCdrController cdr = mMethodsGet(self)->CdrControllerGet(self);
    if (cdr)
        {
        ThaModuleCdr module = ThaCdrControllerModuleGet(cdr);
        if (module)
            return ThaCdrControllerDefaultOffset(cdr) + ThaModuleCDRLineModeControlRegister(module);
        }
    return 0;
    }

static uint32 FrequenceOffsetBase(ThaAttController self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 FrequenceOffsetStep(ThaAttController self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 ChannelSpeed(ThaAttController self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 Ppm2NcoValue(ThaAttController self, float ppm)
    {
    eBool debug=cAtFalse;
    uint32 unsignPpmTmp = (ppm < 0) ? (uint32)(cPpb2Ppm * (0 - ppm)) : (uint32)(cPpb2Ppm * ppm);
    uint32 ncoAdjust = (unsignPpmTmp / mMethodsGet(self)->FrequenceOffsetStep(self));
    uint32 ncoValue = (ppm < 0) ? (mMethodsGet(self)->FrequenceOffsetBase(self) - ncoAdjust) :
                                  (mMethodsGet(self)->FrequenceOffsetBase(self) + ncoAdjust) ;
    if (debug)
    AtPrintc(cSevInfo, "base %u, step %d, ncoAdjust %u, ncoValue %u\n", mMethodsGet(self)->FrequenceOffsetBase(self),
             mMethodsGet(self)->FrequenceOffsetStep(self), ncoAdjust, ncoValue);
    return ncoValue;
    }

static float NcoValue2Ppm(ThaAttController self, uint32 ncoValue)
    {
    eBool debug=cAtFalse;
    eBool isPositive = (ncoValue > mMethodsGet(self)->FrequenceOffsetBase(self));
    uint32 ncoAdjust = isPositive ? (ncoValue - mMethodsGet(self)->FrequenceOffsetBase(self)) :
                                    (mMethodsGet(self)->FrequenceOffsetBase(self) - ncoValue);
    float ppm = (float)(ncoAdjust * (mMethodsGet(self)->FrequenceOffsetStep(self)));
    if (debug)
    AtPrintc(cSevInfo, "base %u, step %d, isPositive %d ncoAdjust %u, ppm %f\n", mMethodsGet(self)->FrequenceOffsetBase(self), mMethodsGet(self)->FrequenceOffsetStep(self),
             isPositive, ncoAdjust, ppm);
    return isPositive ? (ppm / cPpb2Ppm) : ((0 - ppm) / cPpb2Ppm);
    }

static eAtRet FrequenceOffsetSet(AtAttController self, float ppm)
    {
    uint32 loop=1;
    eBool isok=cAtFalse;
    ThaAttController thaAtt = (ThaAttController) self;
    ThaCdrController cdr = mMethodsGet(thaAtt)->CdrControllerGet(thaAtt);
    if (cdr)
        {
        uint32 expValue = Ppm2NcoValue(thaAtt, ppm);
        ThaModuleCdr cdrModule = ThaCdrControllerModuleGet(cdr) ;
        uint32 offset =  ThaModuleCDRLineModeControlRegister(cdrModule) + mMethodsGet(cdr)->TimingCtrlOffset(cdr);
        uint32 regAddress = ThaAttControllerFrequenceOffsetHoldoverRegister(thaAtt) + offset;

        ThaCdrControllerWrite(cdr, regAddress, expValue, cThaModuleCdr); /* Write 3 times if not good*/
        while (loop < 100)
            {
            uint32 readValue = ThaCdrControllerRead(cdr, regAddress, cThaModuleCdr);
            if (readValue!= expValue)
                {
                ThaCdrControllerWrite(cdr, regAddress, expValue, cThaModuleCdr);
                }
            else
                {
                if (loop!= 1)
                    AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical, AtSourceLocation, "API OK %d times %s\r\n", loop, AtObjectToString((AtObject)AtAttControllerChannelGet(self)));

                isok = cAtTrue;
                break;
                }
            loop++;
            }
        if (!isok)
            {
            AtPrintc(cSevCritical, "ERROR: API Error: Write %d times still Fail %s expectvalue=0x%X\r\n", loop, AtObjectToString((AtObject)AtAttControllerChannelGet(self)), expValue);
            }
        }
    return cAtOk;
    }

static float FrequenceOffsetGet(AtAttController self)
    {
    ThaAttController thaAtt = (ThaAttController) self;
    ThaCdrController cdr = mMethodsGet(thaAtt)->CdrControllerGet(thaAtt);
    uint32 regAddress = ThaAttControllerFrequenceOffsetHoldoverRegister(thaAtt) + ThaAttControllerFrequenceOffsetDefaultOffset(thaAtt);
    uint32 regVal = ThaCdrControllerRead(cdr, regAddress, cThaModuleCdr);

    return NcoValue2Ppm(thaAtt, regVal);
    }

static eAtRet ForceErrorRate(AtAttController self, uint32 errorType, eAtBerRate rate)
    {
    AtPdhDe3 de3 = (AtPdhDe3)AtAttControllerChannelGet((AtAttController)self);
    AtModulePdh pdh = (AtModulePdh)AtChannelModuleGet((AtChannel)de3);
    AtAttPdhManager mngr = AtModulePdhAttManager(pdh);
    uint32 numErrors =0;
    uint32 dataMask = 0;
    uint32 numSeconds = 0;
    AtUnused(self);
    numErrors = mMethodsGet((ThaAttController)self)->ChannelSpeed((ThaAttController)self); /* kbit/s */
    switch ((uint32)rate)
        {
        case cAtBerRate1E3:
            dataMask     = 0x1;
            numSeconds   = 1;
            break;
        case cAtBerRate1E4:
            dataMask       = 0x1;
            numSeconds     = 10;
            break;
        case cAtBerRate1E5:
            dataMask       = 0x1;
            numSeconds     = 100;
            break;
        case cAtBerRate1E6:
            dataMask       = 0x1;
            numSeconds     = 1000;
            break;
        case cAtBerRate1E7:
            dataMask       = 0x1;
            numSeconds     = 10000;
            break;
        case cAtBerRate1E8:
            dataMask       = 0x101;
            numSeconds     = 10000;
            break;
        case cAtBerRate1E9:
            dataMask       = 0x1111;
            numSeconds     = 10000;
            break;
        case cAtBerRate1E10:
            dataMask       = 0x5555;
            numSeconds     = 10000;
            break;
        default:
            numErrors = 0;
            dataMask = 0x1;
            break;
        }

    /* 1 second */
    AtAttPdhManagerDe3ForcePeriodSet(mngr, numSeconds);
    AtAttControllerForceErrorNumErrorsSet(self, errorType, numErrors);
    AtAttControllerForceErrorDataMaskSet(self, errorType, dataMask);
    /* Let concrete class do */
    return cAtOk;
    }

static eAtRet InitErrorGap(ThaAttController self)
    {
    AtUnused(self);
    return cAtOk;
    }
    
static eAtRet InitDb(ThaAttController self)
    {
    AtUnused(self);
    return cAtOk;
    }
    
static eAtRet DefaultSet(ThaAttController self)
    {
    ThaCdrController cdr = mMethodsGet(self)->CdrControllerGet(self);
    if (cdr)
        {
        uint32 regAddress = ThaAttControllerFrequenceOffsetControlRegister(self) + ThaAttControllerFrequenceOffsetDefaultOffset(self);
        uint32 regVal = ThaCdrControllerRead(cdr, regAddress, cThaModuleCdr);
        /* Set Free-run mode */
        mFieldIns(&regVal, cBit3_0, 0, 0x7);
        ThaCdrControllerWrite(cdr, regAddress, regVal, cThaModuleCdr);
        }
    return cAtOk;
    }

static eAtRet Init(AtAttController self)
    {
    mMethodsGet(mThis(self))->InitErrorGap(mThis(self));
    mMethodsGet(mThis(self))->DefaultSet(mThis(self));
    self->txAlarmTypes = 0;
    return mMethodsGet(mThis(self))->InitDb(mThis(self));
    }

static eAtRet HwForceErrorRate(ThaAttController self, uint32 errorType, uint32 rate)
    {
    AtUnused(self);
    AtUnused(errorType);
    AtUnused(rate);
    return cAtOk;
    }

static eBool  IsB1(ThaAttController self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    return cAtFalse;
    }

static eBool  IsE1(ThaAttController self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 CalculateNRate(ThaAttController self, uint32 rate)
    {
    uint32 Nrate = 0;

    AtUnused(self);
    switch ((uint32) rate)
        {
        case cAtBerRate1E2:
            Nrate = cAtNRate1E2;
            break;
        case cAtBerRate1E3:
            Nrate = cAtNRate1E3;
            break;
        case cAtBerRate1E4:
            Nrate = cAtNRate1E4;
            break;
        case cAtBerRate1E5:
            Nrate = cAtNRate1E5;
            break;
        case cAtBerRate1E6:
            Nrate = cAtNRate1E6;
            break;
        case cAtBerRate1E7:
            Nrate = cAtNRate1E7;
            break;
        case cAtBerRate1E8:
            Nrate = cAtNRate1E8;
            break;
        case cAtBerRate1E9:
            Nrate = cAtNRate1E9;
            break;
        default:
            return cAtNRate1E9;
            break;
        }

    return Nrate;
    }

static uint16 PositionMaskGet(uint8 array_max_pos, uint16 array_positionMask)
    {
    uint16 PositionMask = 1;
    if (array_max_pos==1)
        {
        uint16     position[2]  = { 0,   0x1};
        PositionMask = position[array_positionMask];
        }
    else if (array_max_pos==2)
        {
        uint16     position[3]  = { 0,   0x1,  0x0003};
        PositionMask = position[array_positionMask];
        }
    else if (array_max_pos==3)
        {
        uint16     position[4]  = { 0,   0x1,  0x0003, 0x0007};
        PositionMask = position[array_positionMask];
        }
    else if (array_max_pos==4)
        {
        uint16     position[5]  = { 0,   0x1,  0x0003, 0x0007, 0x000F};
        PositionMask = position[array_positionMask];
        }
    else if (array_max_pos==5)
        {
        uint16     position[6]  = { 0,   0x1,  0x0011, 0x0013, 0x0017, 0x1F};
        PositionMask = position[array_positionMask];
        }
    else if (array_max_pos==6)
        {
        uint16     position[7]  = { 0,   0x1,  0x0011, 0x0013, 0x0033, 0x37,   0x3F};
        PositionMask = position[array_positionMask];
        }
    else if (array_max_pos==7)
        {
        uint16     position[8]  = { 0,   0x1,  0x0011, 0x0013, 0x0033, 0x37,   0x77,   0x7F};
        PositionMask = position[array_positionMask];
        }
    else if (array_max_pos==8)
        {
        uint16     position[9]  = { 0,   0x1,  0x0011, 0x0013, 0x0033, 0x37,   0x77,   0x7F,   0x0FF};
        PositionMask = position[array_positionMask];
        }
    else if (array_max_pos==9)
        {
        uint16     position[10]  = { 0,   0x1,  0x0110, 0x0111, 0x0131, 0x133,  0x173,  0x177,  0x1F7,  0x1FF};
        PositionMask = position[array_positionMask];
        }
    else if (array_max_pos==10)
        {
        uint16     position[11] = { 0,   0x1,  0x0110, 0x0111, 0x0131, 0x313,  0x333,  0x373,  0x377,  0x3F7,  0x3FF};
        PositionMask = position[array_positionMask];
        }
    else if (array_max_pos==11)
        {
        uint16     position[12] = { 0,   0x1,  0x0110, 0x0111, 0x0131, 0x313,  0x333,  0x373,  0x773,  0x777,  0x7F7,  0x7FF};
        PositionMask = position[array_positionMask];
        }
    else if (array_max_pos==12)
        {
        uint16     position[13] = { 0,   0x1,  0x0110, 0x0111, 0x0131, 0x313,  0x333,  0x373,  0x773,  0x777,  0xF77,  0xFF7,  0xFFF};
        PositionMask = position[array_positionMask];
        }
    else if (array_max_pos==13)
        {
        uint16     position[14] = { 0,   0x1,  0x1010, 0x1110, 0x1111, 0x1311, 0x1331, 0x1333, 0x1733, 0x1737, 0x1F37, 0x1F77, 0x1FF7, 0x1FFF};
        PositionMask = position[array_positionMask];
        }
    else if (array_max_pos==14)
        {
        uint16     position[15] = { 0,   0x1,  0x1010, 0x1110, 0x1111, 0x3111, 0x3131, 0x3133, 0x3333, 0x3337, 0x3737, 0x3777, 0x3F77, 0x3FF7, 0x3FFF};
        PositionMask = position[array_positionMask];
        }
    else if (array_max_pos==15)
        {
        uint16     position[16] = { 0,   0x1,  0x1010, 0x1110, 0x1111, 0x3111, 0x3131, 0x3133, 0x3333, 0x3337, 0x3737, 0x3777, 0x7777, 0x7F77, 0x7FF7, 0x7FFF };
        PositionMask = position[array_positionMask];
        }
    else if (array_max_pos==16)
        {
        uint16     position[17] = { 0,   0x1,  0x1010, 0x1110, 0x1111, 0x3111, 0x3131, 0x3133, 0x3333, 0x3337, 0x3737, 0x3777, 0x7777, 0xF777, 0xF7F7, 0xF7FF, 0xFFFF };
        PositionMask = position[array_positionMask];
        }
    else
        {
        uint16     position[33] = { 0,   0x1,  0x1010, 0x1110, 0x1111, 0x3111, 0x3131, 0x3133, 0x3333, 0x3337, 0x3737, 0x3777, 0x7777, 0xF777, 0xF7F7, 0xF7FF, 0xFFFF,   0x1,  0x1010, 0x1110, 0x1111, 0x3111, 0x3131, 0x3133, 0x3333, 0x3337, 0x3737, 0x3777, 0x7777, 0xF777, 0xF7F7, 0xF7FF, 0xFFFF };
        PositionMask = position[array_positionMask];
        }
    return PositionMask;
    }



static eBool IsCheckRateShow(ThaAttController self)
    {
    AtChannel channel = AtAttControllerChannelGet((AtAttController)self);
    AtModule module = AtChannelModuleGet(channel);
    if (AtModuleTypeGet(module)== cAtModuleSdh)
        return ThaAttSdhManagerCheckForceRateShowGet(AtModuleSdhAttManager((AtModuleSdh) module));

    else if (AtModuleTypeGet(module)== cAtModulePdh)
        {
        ThaAttPdhManager manger = (ThaAttPdhManager)AtModulePdhAttManager((AtModulePdh) module);
        return ThaAttPdhManagerCheckForceRateShowGet(manger);
        }

    return cAtFalse;
    }

static eBool IsCheckRate(ThaAttController self)
    {
    AtChannel channel = AtAttControllerChannelGet((AtAttController)self);
    AtModule module = AtChannelModuleGet(channel);
    if (AtModuleTypeGet(module)== cAtModuleSdh)
        {
        return ThaAttSdhManagerCheckForceRateEnGet(AtModuleSdhAttManager((AtModuleSdh) module));
        }
    return cAtTrue;
    }

static tAtOsalCurTime* ForcePointerBeginTimeGet(ThaAttController self)
    {
    AtUnused(self);

    return NULL;
    }

static void OverrideAtAttController(AtAttController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtAttControllerOverride, mMethodsGet(self), sizeof(m_AtAttControllerOverride));

        mMethodOverride(m_AtAttControllerOverride, FrequenceOffsetSet);
        mMethodOverride(m_AtAttControllerOverride, FrequenceOffsetGet);
        mMethodOverride(m_AtAttControllerOverride, ForceErrorRate);
        mMethodOverride(m_AtAttControllerOverride, Init);
        mMethodOverride(m_AtAttControllerOverride, WanderCreate);
        }

    mMethodsSet(self, &m_AtAttControllerOverride);
    }

static void Override(AtAttController self)
    {
    OverrideAtAttController(self);
    }

static void MethodsInit(AtAttController self)
    {
    ThaAttController controller = (ThaAttController)self;
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, CdrControllerGet);
        mMethodOverride(m_methods, FrequenceOffsetControlRegister);
        mMethodOverride(m_methods, FrequenceOffsetHoldoverRegister);
        mMethodOverride(m_methods, FrequenceOffsetDefaultOffset);
        mMethodOverride(m_methods, FrequenceOffsetBase);
        mMethodOverride(m_methods, FrequenceOffsetStep);
        mMethodOverride(m_methods, ChannelSpeed);
        mMethodOverride(m_methods, NeedRemoveSoh);
        mMethodOverride(m_methods, NbitPerFrameSoh);
        mMethodOverride(m_methods, MaxStep);
        mMethodOverride(m_methods, RateCalib);
        mMethodOverride(m_methods, RateConvertStep);
        mMethodOverride(m_methods, RateConvertStepIsSupport);
        mMethodOverride(m_methods, IsStep16bit);
        mMethodOverride(m_methods, StartVersionSupportStep16bit);
        mMethodOverride(m_methods, StartVersionSupportStep14bit);
        mMethodOverride(m_methods, IsDataMask8bit);
        mMethodOverride(m_methods, IsDataMask2bit);
        mMethodOverride(m_methods, StartVersionSupportDataMask8bit);
        mMethodOverride(m_methods, DefaultSet);
        mMethodOverride(m_methods, InitErrorGap);
        mMethodOverride(m_methods, InitDb);
        mMethodOverride(m_methods, UseAdjust);
        mMethodOverride(m_methods, MaxNumberofBytePerFrame);
        mMethodOverride(m_methods, HwForceErrorRate);
        mMethodOverride(m_methods, PositionMaskGet);
        mMethodOverride(m_methods, IsCheckRateShow);
        mMethodOverride(m_methods, IsCheckRate);
        mMethodOverride(m_methods, IsB1);
        mMethodOverride(m_methods, IsE1);
        mMethodOverride(m_methods, CalculateNRate);
        mMethodOverride(m_methods, ForcePointerBeginTimeGet);
        }

    mMethodsSet(controller, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaAttController);
    }

AtAttController ThaAttControllerObjectInit(AtAttController self, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtAttControllerObjectInit(self, channel) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

uint32 ThaAttControllerFrequenceOffsetControlRegister(ThaAttController self)
    {
    if (self)
        return mMethodsGet(self)->FrequenceOffsetControlRegister(self);
    return 0;
    }

uint32 ThaAttControllerFrequenceOffsetHoldoverRegister(ThaAttController self)
    {
    if (self)
        return mMethodsGet(self)->FrequenceOffsetHoldoverRegister(self);
    return 0;
    }

uint32 ThaAttControllerFrequenceOffsetDefaultOffset(ThaAttController self)
    {
    if (self)
        return mMethodsGet(self)->FrequenceOffsetDefaultOffset(self);
    return 0;
    }

uint32 ThaAttControllerChannelSpeed(ThaAttController self)
    {
    if (self)
        return mMethodsGet(self)->ChannelSpeed(self);
    return 0;
    }

uint32 ThaAttControllerNbitPerFrame(ThaAttController self)
    {
    if (self)
        return mMethodsGet(self)->NbitPerFrame(self);
    return 0;
    }

eBool ThaAttControllerNeedRemoveSoh(ThaAttController self, uint32 errorType)
    {
    if (self)
        return mMethodsGet(self)->NeedRemoveSoh(self, errorType);
    return cAtFalse;
    }

uint32 ThaAttControllerNbitPerFrameSoh(ThaAttController self)
    {
    if (self)
        return mMethodsGet(self)->NbitPerFrameSoh(self);
    return 0;
    }

uint32 ThaAttControllerMaxStep(ThaAttController self, uint32 isError, uint32 errorType, uint32 Nrate, uint32 NbitPerFrame)
    {
    if (self)
        return mMethodsGet(self)->MaxStep(self, isError, errorType, Nrate, NbitPerFrame);
    return 0;
    }

eBool ThaAttControllerIsStep14bit(ThaAttController self, uint32 errorType)
    {
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(ThaAttControllerVersionReader((ThaAttController) self));
    AtUnused(self);
    if (hwVersion >= mMethodsGet(self)->StartVersionSupportStep14bit(self, errorType))
        return cAtTrue;
    return cAtFalse;
    }
eBool  ThaAttControllerIsDataMask2bit(ThaAttController self, uint32 errorType)
    {
    if (self)
        return mMethodsGet(self)->IsDataMask2bit(self, errorType);
    return cAtFalse;
    }
eBool  ThaAttControllerIsDataMask8bit(ThaAttController self, uint32 errorType)
    {
    if (self)
        return mMethodsGet(self)->IsDataMask8bit(self, errorType);
    return cAtFalse;
    }

eBool  ThaAttControllerIsStep16bit(ThaAttController self, uint32 errorType)
    {
    if (self)
        return mMethodsGet(self)->IsStep16bit(self, errorType);
    return cAtFalse;
    }

eBool  ThaAttControllerUseAdjust(ThaAttController self, uint32 errorType)
    {
    if (self)
        return mMethodsGet(self)->UseAdjust(self, errorType);
    return cAtFalse;
    }

ThaVersionReader ThaAttControllerVersionReader(ThaAttController self)
    {
    AtChannel channel = AtAttControllerChannelGet((AtAttController)self);
    AtDevice device = AtChannelDeviceGet(channel);
    return ThaDeviceVersionReader(device);
    }

/*
 STM-16: NFramePerSecond=8000
 STM-64: NFramePerSecond=8000
 */
float ThaAttControllerNFramePerSecond(ThaAttController self)
    {
    if (self)
        {
        uint32 RATE            = ThaAttControllerChannelSpeed((ThaAttController) self);
        uint32 NbitPerFrame    = ThaAttControllerNbitPerFrame((ThaAttController) self);
        float NFramePerSecond  = (((float)RATE * 1000) / (float)NbitPerFrame); /* = 9.398 frame*/
        return NFramePerSecond;
        }
    return 0;
    }

tAtOsalCurTime* ThaAttControllerForcePointerBeginTimeGet(ThaAttController self)
    {
    if (self)
        return mMethodsGet(self)->ForcePointerBeginTimeGet(self);
    return NULL;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : ThaAttPdhManagerInternal.h
 * 
 * Created Date: July 26, 2017
 *
 * Author      : chaudpt
 *
 * Description : ATT PDH manager class descriptor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAATTPRBSMANAGERINTERNAL_H_
#define _THAATTPRBSMANAGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/att/AtAttPrbsManagerInternal.h"
#include "ThaAttPrbsManager.h"
/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

typedef struct tThaAttPrbsManager
    {
    tAtAttPrbsManager super;
    uint32 timeMeasurementError;
    }tThaAttPrbsManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtAttPrbsManager ThaAttPrbsManagerObjectInit(AtAttPrbsManager self, AtModulePrbs prbs);

/* Debugger */

#ifdef __cplusplus
}
#endif
#endif /* _THAATTPRBSMANAGERINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ThaAttPdhManager
 *
 * File        : ThaAttPdhManager.h
 *
 * Created Date: July 26, 2017
 *
 * Author      : chaudpt
 *
 * Description : This file contains common prototypes of ATT PDH Manager of default
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _THAATTSDHMANAGER_H_
#define _THAATTSDHMANAGER_H_

/*--------------------------- Include files ----------------------------------*/
#include "AtModulePdh.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Typedef ----------------------------------------*/
typedef struct tThaAttSdhManager * ThaAttSdhManager;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtAttSdhManager Tha6A210031AttSdhManagerNew(AtModuleSdh sdh);
AtAttSdhManager Tha6A290011AttSdhManagerNew(AtModuleSdh sdh);
AtAttSdhManager Tha6A290021AttSdhManagerNew(AtModuleSdh sdh);
AtAttSdhManager Tha6A290022AttSdhManagerNew(AtModuleSdh sdh);

eAtRet ThaAttSdhManagerDebugForcingLogicSet(ThaAttSdhManager self, eBool isDut);
eBool  ThaAttSdhManagerDebugForcingLogicGet(ThaAttSdhManager self);
eAtRet ThaAttSdhManagerDebugHoPointerLooptimeEnable(ThaAttSdhManager self, eBool en);
eAtRet ThaAttSdhManagerDebugLoPointerLooptimeEnable(ThaAttSdhManager self, eBool en);
eAtRet ThaAttSdhManagerCheckForceRateShowSet(ThaAttSdhManager self, eBool en);
eBool  ThaAttSdhManagerCheckForceRateShowGet(ThaAttSdhManager self);
eAtRet ThaAttSdhManagerCheckForceRateEnSet(ThaAttSdhManager self, eBool en);
eBool  ThaAttSdhManagerCheckForceRateEnGet(ThaAttSdhManager self);
eAtRet ThaAttSdhManagerCheckForceRatePecentSet(ThaAttSdhManager self, uint8 percent);
uint8  ThaAttSdhManagerCheckForceRatePecentGet(ThaAttSdhManager self);
eAtRet ThaAttSdhManagerSohColRemoveSet(ThaAttSdhManager self, uint8 SohColRemove);
uint8  ThaAttSdhManagerSohColRemoveGet(ThaAttSdhManager self);
eAtRet ThaAttSdhManagerG783DebugSet(ThaAttSdhManager self, eBool debugEn);
eAtRet ThaAttSdhManagerG783T0Set(ThaAttSdhManager self, uint32 T0);
uint32 ThaAttSdhManagerG783T0Get(ThaAttSdhManager self);
eAtRet ThaAttSdhManagerG783T1Set(ThaAttSdhManager self, uint32 T1);
uint32 ThaAttSdhManagerG783T1Get(ThaAttSdhManager self);
eAtRet ThaAttSdhManagerG783T2Set(ThaAttSdhManager self, uint32 T2);
uint32 ThaAttSdhManagerG783T2Get(ThaAttSdhManager self);
eAtRet ThaAttSdhManagerG783T3Set(ThaAttSdhManager self, uint32 T3);
uint32 ThaAttSdhManagerG783T3Get(ThaAttSdhManager self);
eAtRet ThaAttSdhManagerG783T3Note2Set(ThaAttSdhManager self, uint32 T3Note2);
uint32 ThaAttSdhManagerG783T3Note2Get(ThaAttSdhManager self);
eAtRet ThaAttSdhManagerG783T4Set(ThaAttSdhManager self, uint32 T4);
uint32 ThaAttSdhManagerG783T4Get(ThaAttSdhManager self);
eAtRet ThaAttSdhManagerG783T4Note5Set(ThaAttSdhManager self, float T4Note5);
float ThaAttSdhManagerG783T4Note5Get(ThaAttSdhManager self);
eAtRet ThaAttSdhManagerG783T5Set(ThaAttSdhManager self, uint32 T5);
uint32 ThaAttSdhManagerG783T5Get(ThaAttSdhManager self);
eAtRet ThaAttSdhManagerG783T5Note6Set(ThaAttSdhManager self, uint32 T5Note6);
uint32 ThaAttSdhManagerG783T5Note6Get(ThaAttSdhManager self);

AtList ThaAttSdhManagerForcePointerChannelListGet(ThaAttSdhManager self);
eAtRet ThaAttSdhManagerForcePointerChannelAdd(ThaAttSdhManager self, AtAttController channel, eAtAttForcePointerAdjMode mode, uint32 pointerType);
eAtRet ThaAttSdhManagerForcePointerChannelRemove(ThaAttSdhManager self, AtAttController channel, eAtAttForcePointerAdjMode mode, uint32 pointerType, eBool accessHw);
eAtRet ThaAttSdhManagerAlarmDurationUnitSet(ThaAttSdhManager self, eAttSdhForceAlarmDurationUnit unit);
eAttSdhForceAlarmDurationUnit ThaAttSdhManagerAlarmDurationUnitGet(ThaAttSdhManager self);
uint32 ThaAttSdhManagerForcePointerExecuteTimeMsGet(ThaAttSdhManager self);
uint32 ThaAttSdhManagerForcePointerNumChannelGet(ThaAttSdhManager self);
eAtRet ThaAttSdhManagerForcePointerSourceMultiplexingResync(ThaAttSdhManager self);
#ifdef __cplusplus
}
#endif
#endif /* _THAATTSDHMANAGER_H_ */

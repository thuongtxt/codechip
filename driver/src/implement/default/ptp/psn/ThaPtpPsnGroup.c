/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PTP
 *
 * File        : ThaPtpPsnGroup.c
 *
 * Created Date: Aug 2, 2018
 *
 * Description : Default PTP PSN multicast group implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPtpPsnGroup.h"
#include "../../../../generic/man/AtModuleInternal.h"
#include "../../../../generic/ptp/psn/AtPtpPsnGroupInternal.h"
#include "../../man/ThaDevice.h"
#include "../ThaModulePtpReg.h"
#include "../ThaModulePtp.h"
#include "ThaPtpPsn.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define cThaPtpAddrOctet_Mask(idx)      (cBit7_0 << ((idx) * 8))
#define cThaPtpAddrOctet_Shift(idx)     ((idx) * 8)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaPtpPsnGroup
    {
    tAtPtpPsnGroup super;
    }tThaPtpPsnGroup;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPtpPsnGroupMethods         m_AtPtpPsnGroupOverride;

/* Save superclass implementation */
static const tAtPtpPsnGroupMethods  *m_AtPtpPsnGroupMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool LocalAddressIsValid(uint32 localAddress)
    {
    return (localAddress == cInvalidUint32) ? cAtFalse : cAtTrue;
    }

static AtIpCore IpCore(AtModule module)
    {
    return AtDeviceIpCoreGet(AtModuleDeviceGet(module), AtModuleDefaultCoreGet(module));
    }

static uint16 LongRead(AtPtpPsnGroup self, uint32 address, uint32 *dataBuffer, uint16 bufferLen)
    {
    AtModule module = (AtModule)AtPtpPsnGroupModuleGet(self);
    return AtModuleHwLongReadOnCore(module, address, dataBuffer, bufferLen, IpCore(module));
    }

static uint16 LongWrite(AtPtpPsnGroup self, uint32 address, uint32 *dataBuffer, uint16 bufferLen)
    {
    AtModule module = (AtModule)AtPtpPsnGroupModuleGet(self);
    return AtModuleHwLongWriteOnCore(module, address, dataBuffer, bufferLen, IpCore(module));
    }

static uint32 Read(AtPtpPsnGroup self, uint32 address)
    {
    AtModule module = (AtModule)AtPtpPsnGroupModuleGet(self);
    return AtModuleHwRead(module, address, AtIpCoreHalGet(IpCore(module)));
    }

static void Write(AtPtpPsnGroup self, uint32 address, uint32 value)
    {
    AtModule module = (AtModule)AtPtpPsnGroupModuleGet(self);
    AtModuleHwWrite(module, address, value, AtIpCoreHalGet(IpCore(module)));
    }

static ThaModulePtp PtpModule(AtPtpPsnGroup self)
    {
    return (ThaModulePtp)AtPtpPsnGroupModuleGet(self);
    }

static uint32 NumGroupsPerPart(void)
    {
    return 2U;
    }

static uint8 PartId(AtPtpPsnGroup self)
    {
    return (uint8)(AtPtpPsnGroupIdGet(self) / NumGroupsPerPart());
    }

static uint32 LocalIdInPart(AtPtpPsnGroup self)
    {
    return AtPtpPsnGroupIdGet(self) % NumGroupsPerPart();
    }

static uint32 BaseAddress(AtPtpPsnGroup self)
    {
    ThaModulePtp ptpModule = PtpModule(self);
    return ThaModulePtpBaseAddress(ptpModule) + ThaModulePtpPartOffset(ptpModule, PartId(self));
    }

static eAtRet MacGroupEnable(AtPtpPsnGroup self, eBool enable)
    {
    uint32 groupEnShift = LocalIdInPart(self) + 1;
    uint32 groupEnMask = cBit0 << groupEnShift;
    uint32 regAddr = BaseAddress(self) + cAf6Reg_upen_macglb_en;
    uint32 regVal;

    regVal = Read(self, regAddr);
    mRegFieldSet(regVal, groupEn, mBoolToBin(enable));
    Write(self, regAddr, regVal);

    return cAtOk;
    }

static uint8 *MacAddressDefault(AtPtpPsnGroup self)
    {
    static uint8 mcastMac1ForPtp[] = {0x01, 0x1B, 0x19, 0x00, 0x00, 0x00};
    static uint8 mcastMac2ForPtp[] = {0x01, 0x80, 0xC2, 0x00, 0x00, 0x0E};
    return (LocalIdInPart(self)) ? mcastMac2ForPtp : mcastMac1ForPtp;
    }

static eAtRet MacGroupDefaultSet(AtPtpPsnGroup self)
    {
    eAtRet ret;
    AtModulePtp ptpModule = AtPtpPsnGroupModuleGet(self);
    uint32 port_i;

    ret  = MacGroupEnable(self, cAtTrue);
    ret |= AtPtpPsnGroupAddressSet(self, MacAddressDefault(self));

    /* MAC multicast group does not have distinct enable bit per port,
     * so once multicast group is enabled, it implies all ports are joined to multicast group. */
    for (port_i = 0; port_i < AtModulePtpNumPortsGet(ptpModule); port_i++)
        {
        AtPtpPort port = AtModulePtpPortGet(ptpModule, port_i);
        if (port == NULL)
            continue;

        if (!ThaModulePtpPortCanJoinPsnGroup((ThaModulePtp)ptpModule, port, self))
            continue;

        ret |= AtPtpPsnGroupPortAdd(self, port);
        if (ret != cAtOk)
            return ret;
        }

    return cAtOk;
    }

static uint8 *IpV4AddressDefault(AtPtpPsnGroup self)
    {
    static uint8 mcastIpV4ForPtpV2[]     = {224, 0, 1, 129};
    static uint8 mcastIpV4ForPtpV2Peer[] = {224, 0, 0, 107};

    return (LocalIdInPart(self)) ? mcastIpV4ForPtpV2Peer : mcastIpV4ForPtpV2;
    }

static eAtRet IpV4GroupDefaultSet(AtPtpPsnGroup self)
    {
    return AtPtpPsnGroupAddressSet(self, IpV4AddressDefault(self));
    }

static uint8 *IpV6AddressDefault(AtPtpPsnGroup self)
    {
    static uint8 mcastIpV6ForPtpV2[]     = {0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                                            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x81};
    static uint8 mcastIpV6ForPtpV2Peer[] = {0xff, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                                            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x6b};
    return (LocalIdInPart(self)) ? mcastIpV6ForPtpV2Peer : mcastIpV6ForPtpV2;
    }

static eAtRet IpV6GroupDefaultSet(AtPtpPsnGroup self)
    {
    return AtPtpPsnGroupAddressSet(self, IpV6AddressDefault(self));
    }

static eAtRet DefaultSet(AtPtpPsnGroup self)
    {
    uint8 psnType = AtPtpPsnGroupTypeGet(self);
    switch (psnType)
        {
        case cAtPtpPsnTypeLayer2:   return MacGroupDefaultSet(self);
        case cAtPtpPsnTypeIpV4:     return IpV4GroupDefaultSet(self);
        case cAtPtpPsnTypeIpV6:     return IpV6GroupDefaultSet(self);
        default:                    return cAtErrorModeNotSupport;
        }
    }

static eAtRet Init(AtPtpPsnGroup self)
    {
    eAtRet ret = m_AtPtpPsnGroupMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return DefaultSet(self);
    }

static eAtRet HelperAddressSet(AtPtpPsnGroup self, uint32 localAddress, const uint8 *address, uint8 addressLen)
    {
    uint32 regAddr;
    uint32 longRegVal[cThaLongRegMaxSize];
    uint8 i, word_i = 0;
    uint8 byte_i;

    if (!LocalAddressIsValid(localAddress))
        return cAtErrorModeNotSupport;

    regAddr = BaseAddress(self) + localAddress;
    LongRead(self, regAddr, longRegVal, cThaLongRegMaxSize);
    for (i = 0, byte_i = (uint8)(addressLen - 1); i < addressLen; i++, byte_i--)
        {
        uint8 j = (uint8)(i % 4);
        mFieldIns(&longRegVal[word_i],
                  cThaPtpAddrOctet_Mask(j),
                  cThaPtpAddrOctet_Shift(j),
                  address[byte_i]);
        if (j == 3)  /* Next word */
            word_i++;
        }
    LongWrite(self, regAddr, longRegVal, cThaLongRegMaxSize);

    return cAtOk;
    }

static eAtRet HelperAddressGet(AtPtpPsnGroup self, uint32 localAddress, uint8 *address, uint8 addressLen)
    {
    uint32 regAddr;
    uint32 longRegVal[cThaLongRegMaxSize];
    uint8 i, word_i = 0;
    uint8 byte_i;

    if (!LocalAddressIsValid(localAddress))
        return cAtErrorModeNotSupport;

    regAddr = BaseAddress(self) + localAddress;
    LongRead(self, regAddr, longRegVal, cThaLongRegMaxSize);
    for (i = 0, byte_i = (uint8)(addressLen - 1); i < addressLen; i++, byte_i--)
        {
        uint8 j = (uint8)(i % 4);
        mFieldGet(longRegVal[word_i],
                  cThaPtpAddrOctet_Mask(j),
                  cThaPtpAddrOctet_Shift(j),
                  uint8,
                  &address[byte_i]);
        if (j == 3) /* Next word */
            word_i++;
        }

    return cAtOk;
    }

static uint32 AddressRegister(AtPtpPsnGroup self)
    {
    uint8 psnType = AtPtpPsnGroupTypeGet(self);
    uint32 groupId = LocalIdInPart(self);

    switch (psnType)
        {
        case cAtPtpPsnTypeLayer2:   return cAf6Reg_upen_mmac1_glb + groupId;
        case cAtPtpPsnTypeIpV4:     return cAf6Reg_upen_v4mip1_glb + groupId;
        case cAtPtpPsnTypeIpV6:     return cAf6Reg_upen_v6mip1_glb + groupId;
        default:                    return cInvalidUint32;
        }
    }

static eAtModulePtpRet AddressSet(AtPtpPsnGroup self, const uint8 *address)
    {
    return HelperAddressSet(self, AddressRegister(self), address, AtPtpPsnGroupAddressLengthInBytes(self));
    }

static eAtModulePtpRet AddressGet(AtPtpPsnGroup self, uint8 *address)
    {
    return HelperAddressGet(self, AddressRegister(self), address, AtPtpPsnGroupAddressLengthInBytes(self));
    }

static void OverrideAtPtpPsnGroup(AtPtpPsnGroup self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPtpPsnGroupMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPtpPsnGroupOverride, mMethodsGet(self), sizeof(m_AtPtpPsnGroupOverride));

        /* Setup methods */
        mMethodOverride(m_AtPtpPsnGroupOverride, Init);
        mMethodOverride(m_AtPtpPsnGroupOverride, AddressSet);
        mMethodOverride(m_AtPtpPsnGroupOverride, AddressGet);
        }

    mMethodsSet(self, &m_AtPtpPsnGroupOverride);
    }

static void Override(AtPtpPsnGroup self)
    {
    OverrideAtPtpPsnGroup(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPtpPsnGroup);
    }

static AtPtpPsnGroup ObjectInit(AtPtpPsnGroup self, uint32 groupId, eAtPtpPsnType psnType, AtModulePtp module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPtpPsnGroupObjectInit(self, groupId, psnType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPtpPsnGroup ThaPtpPsnGroupNew(AtModulePtp module, uint32 groupId, eAtPtpPsnType psnType)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPtpPsnGroup newGroup = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newGroup == NULL)
        return NULL;

    return ObjectInit(newGroup, groupId, psnType, module);
    }


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PTP
 * 
 * File        : ThaPtpPsn.h
 * 
 * Created Date: Jul 6, 2018
 *
 * Description : Default PTP PSN
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPTPPSN_H_
#define _THAPTPPSN_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPtpPsn.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPtpL2Psn ThaPtpL2PsnNew(AtPtpPort port);
AtPtpIpPsn ThaPtpIpV4PsnNew(AtPtpPort port);
AtPtpIpPsn ThaPtpIpV6PsnNew(AtPtpPort port);

AtPtpPsnGroup ThaPtpPsnGroupNew(AtModulePtp module, uint32 groupId, eAtPtpPsnType psnType);

#ifdef __cplusplus
}
#endif
#endif /* _THAPTPPSN_H_ */


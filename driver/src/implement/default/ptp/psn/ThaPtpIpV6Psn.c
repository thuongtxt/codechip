/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PTP
 *
 * File        : ThaPtpIpV6Psn.c
 *
 * Created Date: Jul 9, 2018
 *
 * Description : Default PTP IPv6 PSN implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/common/AtChannelInternal.h"
#include "../../../../generic/ptp/psn/AtPtpPsnInternal.h"
#include "../ThaPtpPort.h"
#include "ThaPtpPsn.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaPtpIpV6Psn
    {
    tAtPtpIpV6Psn super;
    }tThaPtpIpV6Psn;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPtpPsnMethods         m_AtPtpPsnOverride;
static tAtPtpIpPsnMethods       m_AtPtpIpPsnOverride;

/* Save superclass implementation */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaPtpClaController ClaController(AtPtpPsn self)
    {
    return ThaPtpPortClaControllerGet((ThaPtpPort)AtPtpPsnPortGet(self));
    }

static uint32 MaxNumUnicastDestAddressEntryGet(AtPtpIpPsn self)
    {
    return ThaPtpClaControllerMaxNumIpV6AddressesGet(ClaController((AtPtpPsn)self), cAtPtpIpDestAddressUnicast);
    }

static eAtModulePtpRet ExpectedUnicastDestAddressEntrySet(AtPtpIpPsn self, uint32 entryIndex, const uint8 *address)
    {
    return ThaPtpClaControllerIpV6AddressSet(ClaController((AtPtpPsn)self), cAtPtpIpDestAddressUnicast, entryIndex, address);
    }

static eAtModulePtpRet ExpectedUnicastDestAddressEntryGet(AtPtpIpPsn self, uint32 entryIndex, uint8 *address)
    {
    return ThaPtpClaControllerIpV6AddressGet(ClaController((AtPtpPsn)self), cAtPtpIpDestAddressUnicast, entryIndex, address);
    }

static eAtModulePtpRet ExpectedUnicastDestAddressEntryEnable(AtPtpIpPsn self, uint32 entryIndex, eBool enable)
    {
    return ThaPtpClaControllerIpV6AddressEnable(ClaController((AtPtpPsn)self), cAtPtpIpDestAddressUnicast, entryIndex, enable);
    }

static eBool ExpectedUnicastDestAddressEntryIsEnabled(AtPtpIpPsn self, uint32 entryIndex)
    {
    return ThaPtpClaControllerIpV6AddressIsEnabled(ClaController((AtPtpPsn)self), cAtPtpIpDestAddressUnicast, entryIndex);
    }

static uint32 SingleAddressEntry(AtPtpPsn self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModulePtpRet ExpectedMcastDestAddressEnable(AtPtpPsn self, eBool enable)
    {
    return ThaPtpClaControllerIpV6AddressEnable(ClaController(self), cAtPtpIpDestAddressMulticast, SingleAddressEntry(self), enable);
    }

static eBool ExpectedMcastDestAddressIsEnabled(AtPtpPsn self)
    {
    return ThaPtpClaControllerIpV6AddressIsEnabled(ClaController(self), cAtPtpIpDestAddressMulticast, SingleAddressEntry(self));
    }

static eBool IsMcastAddress(AtPtpPsn self, const uint8 *address)
    {
    AtUnused(self);
    /* Multicast addresses in IPv6 use the prefix ff00::/8 */
    return (address[0] == 0xFF) ? cAtTrue : cAtFalse;
    }

static eAtModulePtpRet ExpectedMcastDestAddressSet(AtPtpPsn self, const uint8 *address)
    {
    if (!IsMcastAddress(self, address))
        AtChannelLog((AtChannel)AtPtpPsnPortGet(self), cAtLogLevelWarning, AtSourceLocation,
                     "[%s] is not an IPv6 multicast address\r\n",
                     AtBytes2String(address, AtPtpPsnAddressLengthInBytes(self), 16));

    return ThaPtpClaControllerIpV6AddressSet(ClaController(self), cAtPtpIpDestAddressMulticast, SingleAddressEntry(self), address);
    }

static eAtModulePtpRet ExpectedMcastDestAddressGet(AtPtpPsn self, uint8 *address)
    {
    return ThaPtpClaControllerIpV6AddressGet(ClaController(self), cAtPtpIpDestAddressMulticast, SingleAddressEntry(self), address);
    }

static eAtModulePtpRet ExpectedAnycastDestAddressEnable(AtPtpPsn self, eBool enable)
    {
    return ThaPtpClaControllerIpV6AddressEnable(ClaController(self), cAtPtpIpDestAddressAnycast, SingleAddressEntry(self), enable);
    }

static eBool ExpectedAnycastDestAddressIsEnabled(AtPtpPsn self)
    {
    return ThaPtpClaControllerIpV6AddressIsEnabled(ClaController(self), cAtPtpIpDestAddressAnycast, SingleAddressEntry(self));
    }

static uint32 MaxNumUnicastSourceAddressEntryGet(AtPtpIpPsn self)
    {
    return ThaPtpClaControllerMaxNumIpV6AddressesGet(ClaController((AtPtpPsn)self), cAtPtpIpSourceAddressUnicast);
    }

static eAtModulePtpRet ExpectedUnicastSourceAddressEntrySet(AtPtpIpPsn self, uint32 entryIndex, const uint8 *address)
    {
    return ThaPtpClaControllerIpV6AddressSet(ClaController((AtPtpPsn)self), cAtPtpIpSourceAddressUnicast, entryIndex, address);
    }

static eAtModulePtpRet ExpectedUnicastSourceAddressEntryGet(AtPtpIpPsn self, uint32 entryIndex, uint8 *address)
    {
    return ThaPtpClaControllerIpV6AddressGet(ClaController((AtPtpPsn)self), cAtPtpIpSourceAddressUnicast, entryIndex, address);
    }

static eAtModulePtpRet ExpectedUnicastSourceAddressEntryEnable(AtPtpIpPsn self, uint32 entryIndex, eBool enable)
    {
    return ThaPtpClaControllerIpV6AddressEnable(ClaController((AtPtpPsn)self), cAtPtpIpSourceAddressUnicast, entryIndex, enable);
    }

static eBool ExpectedUnicastSourceAddressEntryIsEnabled(AtPtpIpPsn self, uint32 entryIndex)
    {
    return ThaPtpClaControllerIpV6AddressIsEnabled(ClaController((AtPtpPsn)self), cAtPtpIpSourceAddressUnicast, entryIndex);
    }

static eAtModulePtpRet ExpectedAnycastSourceAddressEnable(AtPtpPsn self, eBool enable)
    {
    return ThaPtpClaControllerIpV6AddressEnable(ClaController(self), cAtPtpIpSourceAddressAnycast, SingleAddressEntry(self), enable);
    }

static eBool ExpectedAnycastSourceAddressIsEnabled(AtPtpPsn self)
    {
    return ThaPtpClaControllerIpV6AddressIsEnabled(ClaController(self), cAtPtpIpSourceAddressAnycast, SingleAddressEntry(self));
    }

/* specific for backplane ptp port */
static uint32 MaxNumDestAddressEntryGet(AtPtpPsn self)
    {
    return MaxNumUnicastDestAddressEntryGet((AtPtpIpPsn)self);
    }

static uint32 MaxNumSourceAddressEntryGet(AtPtpPsn self)
    {
    return MaxNumUnicastSourceAddressEntryGet((AtPtpIpPsn)self);
    }

static eAtModulePtpRet ExpectedDestAddressEntrySet(AtPtpPsn self, uint32 entryIndex, const uint8 *address)
    {
    return ExpectedUnicastDestAddressEntrySet((AtPtpIpPsn)self, entryIndex, address);
    }

static eAtModulePtpRet ExpectedDestAddressEntryGet(AtPtpPsn self, uint32 entryIndex, uint8 *address)
    {
    return ExpectedUnicastDestAddressEntryGet((AtPtpIpPsn)self, entryIndex, address);
    }

static eAtModulePtpRet ExpectedSourceAddressEntrySet(AtPtpPsn self, uint32 entryIndex, const uint8 *address)
    {
    return ExpectedUnicastSourceAddressEntrySet((AtPtpIpPsn)self, entryIndex, address);
    }

static eAtModulePtpRet ExpectedSourceAddressEntryGet(AtPtpPsn self, uint32 entryIndex, uint8 *address)
    {
    return ExpectedUnicastSourceAddressEntryGet((AtPtpIpPsn)self, entryIndex, address);
    }

static eAtModulePtpRet ExpectedDestAddressEntryEnable(AtPtpPsn self, uint32 entryIndex, eBool enable)
    {
    if (entryIndex >= MaxNumDestAddressEntryGet(self))
        return cAtErrorOutOfRangParm;

    return ExpectedUnicastDestAddressEntryEnable((AtPtpIpPsn)self, entryIndex, enable);
    }

static eBool ExpectedDestAddressEntryIsEnabled(AtPtpPsn self, uint32 entryIndex)
    {
    if (entryIndex >= MaxNumDestAddressEntryGet(self))
        return cAtFalse;
    return ExpectedUnicastDestAddressEntryIsEnabled((AtPtpIpPsn)self, entryIndex);
    }

static eAtModulePtpRet ExpectedSourceAddressEntryEnable(AtPtpPsn self, uint32 entryIndex, eBool enable)
    {
    if (entryIndex >= MaxNumSourceAddressEntryGet(self))
        return cAtErrorOutOfRangParm;

    return ExpectedUnicastSourceAddressEntryEnable((AtPtpIpPsn)self, entryIndex, enable);
    }

static eBool ExpectedSourceAddressEntryIsEnabled(AtPtpPsn self, uint32 entryIndex)
    {
    if (entryIndex >= MaxNumSourceAddressEntryGet(self))
        return cAtFalse;

    return ExpectedUnicastSourceAddressEntryIsEnabled((AtPtpIpPsn)self, entryIndex);
    }

static void OverrideAtPtpPsn(AtPtpIpPsn self)
    {
    AtPtpPsn psn = (AtPtpPsn)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPtpPsnOverride, mMethodsGet(psn), sizeof(m_AtPtpPsnOverride));

        /* Setup methods */
        mMethodOverride(m_AtPtpPsnOverride, ExpectedMcastDestAddressSet);
        mMethodOverride(m_AtPtpPsnOverride, ExpectedMcastDestAddressGet);
        mMethodOverride(m_AtPtpPsnOverride, ExpectedMcastDestAddressEnable);
        mMethodOverride(m_AtPtpPsnOverride, ExpectedMcastDestAddressIsEnabled);
        mMethodOverride(m_AtPtpPsnOverride, ExpectedAnycastDestAddressEnable);
        mMethodOverride(m_AtPtpPsnOverride, ExpectedAnycastDestAddressIsEnabled);
        mMethodOverride(m_AtPtpPsnOverride, ExpectedAnycastSourceAddressEnable);
        mMethodOverride(m_AtPtpPsnOverride, ExpectedAnycastSourceAddressIsEnabled);

        mMethodOverride(m_AtPtpPsnOverride, MaxNumDestAddressEntryGet);
        mMethodOverride(m_AtPtpPsnOverride, MaxNumSourceAddressEntryGet);
        mMethodOverride(m_AtPtpPsnOverride, ExpectedDestAddressEntrySet);
        mMethodOverride(m_AtPtpPsnOverride, ExpectedDestAddressEntryGet);
        mMethodOverride(m_AtPtpPsnOverride, ExpectedSourceAddressEntrySet);
        mMethodOverride(m_AtPtpPsnOverride, ExpectedSourceAddressEntryGet);
        mMethodOverride(m_AtPtpPsnOverride, ExpectedDestAddressEntryEnable);
        mMethodOverride(m_AtPtpPsnOverride, ExpectedDestAddressEntryIsEnabled);
        mMethodOverride(m_AtPtpPsnOverride, ExpectedSourceAddressEntryEnable);
        mMethodOverride(m_AtPtpPsnOverride, ExpectedSourceAddressEntryIsEnabled);
        }

    mMethodsSet(psn, &m_AtPtpPsnOverride);
    }

static void OverrideAtPtpIpPsn(AtPtpIpPsn self)
    {
    AtPtpIpPsn psn = (AtPtpIpPsn)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPtpIpPsnOverride, mMethodsGet(psn), sizeof(m_AtPtpIpPsnOverride));

        /* Setup methods */
        mMethodOverride(m_AtPtpIpPsnOverride, MaxNumUnicastDestAddressEntryGet);
        mMethodOverride(m_AtPtpIpPsnOverride, ExpectedUnicastDestAddressEntrySet);
        mMethodOverride(m_AtPtpIpPsnOverride, ExpectedUnicastDestAddressEntryGet);
        mMethodOverride(m_AtPtpIpPsnOverride, ExpectedUnicastDestAddressEntryEnable);
        mMethodOverride(m_AtPtpIpPsnOverride, ExpectedUnicastDestAddressEntryIsEnabled);
        mMethodOverride(m_AtPtpIpPsnOverride, MaxNumUnicastSourceAddressEntryGet);
        mMethodOverride(m_AtPtpIpPsnOverride, ExpectedUnicastSourceAddressEntrySet);
        mMethodOverride(m_AtPtpIpPsnOverride, ExpectedUnicastSourceAddressEntryGet);
        mMethodOverride(m_AtPtpIpPsnOverride, ExpectedUnicastSourceAddressEntryEnable);
        mMethodOverride(m_AtPtpIpPsnOverride, ExpectedUnicastSourceAddressEntryIsEnabled);
        }

    mMethodsSet(psn, &m_AtPtpIpPsnOverride);
    }

static void Override(AtPtpIpPsn self)
    {
    OverrideAtPtpPsn(self);
    OverrideAtPtpIpPsn(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPtpIpV6Psn);
    }

static AtPtpIpPsn ObjectInit(AtPtpIpPsn self, AtPtpPort ptpPort)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPtpIpV6PsnObjectInit(self, ptpPort) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPtpIpPsn ThaPtpIpV6PsnNew(AtPtpPort port)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPtpIpPsn newPsn = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPsn == NULL)
        return NULL;

    return ObjectInit(newPsn, port);
    }


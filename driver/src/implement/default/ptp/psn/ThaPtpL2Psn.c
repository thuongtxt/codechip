/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PTP
 *
 * File        : ThaPtpL2Psn.c
 *
 * Created Date: Jul 6, 2018
 *
 * Description : Default PTP L2 PSN implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/common/AtChannelInternal.h"
#include "../../../../generic/ptp/psn/AtPtpPsnInternal.h"
#include "../ThaPtpPort.h"
#include "ThaPtpPsn.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaPtpL2Psn
    {
    tAtPtpL2Psn super;
    }tThaPtpL2Psn;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPtpPsnMethods         m_AtPtpPsnOverride;

/* Save superclass implementation */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaPtpClaController ClaController(AtPtpPsn self)
    {
    return ThaPtpPortClaControllerGet((ThaPtpPort)AtPtpPsnPortGet(self));
    }

static eAtModulePtpRet ExpectedCVlanSet(AtPtpPsn self, tAtEthVlanTag *cTag)
    {
    return ThaPtpClaControllerExpectedCVlanSet(ClaController(self), cTag);
    }

static tAtEthVlanTag* ExpectedCVlanGet(AtPtpPsn self, tAtEthVlanTag *cTag)
    {
    return ThaPtpClaControllerExpectedCVlanGet(ClaController(self), cTag);
    }

static eAtModulePtpRet ExpectedSVlanSet(AtPtpPsn self, tAtEthVlanTag *sTag)
    {
    return ThaPtpClaControllerExpectedSVlanSet(ClaController(self), sTag);
    }

static tAtEthVlanTag* ExpectedSVlanGet(AtPtpPsn self, tAtEthVlanTag *sTag)
    {
    return ThaPtpClaControllerExpectedSVlanGet(ClaController(self), sTag);
    }

static eAtModulePtpRet ExpectedUnicastDestAddressSet(AtPtpPsn self, const uint8 *address)
    {
    return ThaPtpClaControllerMacAddressSet(ClaController(self), cAtPtpMacDestAddressUnicast, address);
    }

static eAtModulePtpRet ExpectedUnicastDestAddressGet(AtPtpPsn self, uint8 *address)
    {
    return ThaPtpClaControllerMacAddressGet(ClaController(self), cAtPtpMacDestAddressUnicast, address);
    }

static eAtModulePtpRet ExpectedUnicastDestAddressEnable(AtPtpPsn self, eBool enable)
    {
    return ThaPtpClaControllerMacAddressEnable(ClaController(self), cAtPtpMacDestAddressUnicast, enable);
    }

static eBool ExpectedUnicastDestAddressIsEnabled(AtPtpPsn self)
    {
    return ThaPtpClaControllerMacAddressIsEnabled(ClaController(self), cAtPtpMacDestAddressUnicast);
    }

static eAtModulePtpRet ExpectedMcastDestAddressEnable(AtPtpPsn self, eBool enable)
    {
    return ThaPtpClaControllerMacAddressEnable(ClaController(self), cAtPtpMacDestAddressMulticast, enable);
    }

static eBool ExpectedMcastDestAddressIsEnabled(AtPtpPsn self)
    {
    return ThaPtpClaControllerMacAddressIsEnabled(ClaController(self), cAtPtpMacDestAddressMulticast);
    }

static eBool IsMcastAddress(AtPtpPsn self, const uint8 *address)
    {
    AtUnused(self);
    /* A value of 1 in the least-significant bit of the first octet */
    return (address[0] & cBit0) ? cAtTrue : cAtFalse;
    }

static eAtModulePtpRet ExpectedMcastDestAddressSet(AtPtpPsn self, const uint8 *address)
    {
    if (!IsMcastAddress(self, address))
        AtChannelLog((AtChannel)AtPtpPsnPortGet(self), cAtLogLevelWarning, AtSourceLocation,
                     "[%s] is not a multicast MAC address\r\n",
                     AtBytes2String(address, AtPtpPsnAddressLengthInBytes(self), 16));

    return ThaPtpClaControllerMacAddressSet(ClaController(self), cAtPtpMacDestAddressMulticast, address);
    }

static eAtModulePtpRet ExpectedMcastDestAddressGet(AtPtpPsn self, uint8 *address)
    {
    return ThaPtpClaControllerMacAddressGet(ClaController(self), cAtPtpMacDestAddressMulticast, address);
    }

static eAtModulePtpRet ExpectedAnycastDestAddressEnable(AtPtpPsn self, eBool enable)
    {
    return ThaPtpClaControllerMacAddressEnable(ClaController(self), cAtPtpMacDestAddressAnycast, enable);
    }

static eBool ExpectedAnycastDestAddressIsEnabled(AtPtpPsn self)
    {
    return ThaPtpClaControllerMacAddressIsEnabled(ClaController(self), cAtPtpMacDestAddressAnycast);
    }

static eAtModulePtpRet ExpectedUnicastSourceAddressSet(AtPtpPsn self, const uint8 *address)
    {
    return ThaPtpClaControllerMacAddressSet(ClaController(self), cAtPtpMacSourceAddressUnicast, address);
    }

static eAtModulePtpRet ExpectedUnicastSourceAddressGet(AtPtpPsn self, uint8 *address)
    {
    return ThaPtpClaControllerMacAddressGet(ClaController(self), cAtPtpMacSourceAddressUnicast, address);
    }

static eAtModulePtpRet ExpectedUnicastSourceAddressEnable(AtPtpPsn self, eBool enable)
    {
    return ThaPtpClaControllerMacAddressEnable(ClaController(self), cAtPtpMacSourceAddressUnicast, enable);
    }

static eBool ExpectedUnicastSourceAddressIsEnabled(AtPtpPsn self)
    {
    return ThaPtpClaControllerMacAddressIsEnabled(ClaController(self), cAtPtpMacSourceAddressUnicast);
    }

static eAtModulePtpRet ExpectedAnycastSourceAddressEnable(AtPtpPsn self, eBool enable)
    {
    return ThaPtpClaControllerMacAddressEnable(ClaController(self), cAtPtpMacSourceAddressAnycast, enable);
    }

static eBool ExpectedAnycastSourceAddressIsEnabled(AtPtpPsn self)
    {
    return ThaPtpClaControllerMacAddressIsEnabled(ClaController(self), cAtPtpMacSourceAddressAnycast);
    }

/* specific for backplane ptp port */
static uint32 MaxNumDestAddressEntryGet(AtPtpPsn self)
    {
    AtUnused(self);
    return 1;
    }

static uint32 MaxNumSourceAddressEntryGet(AtPtpPsn self)
    {
    AtUnused(self);
    return 1;
    }

static eAtModulePtpRet ExpectedDestAddressEntrySet(AtPtpPsn self, uint32 entryIndex, const uint8 *address)
    {
    if (entryIndex >= MaxNumDestAddressEntryGet(self))
        return cAtErrorOutOfRangParm;

    return ExpectedUnicastDestAddressSet(self, address);
    }

static eAtModulePtpRet ExpectedDestAddressEntryGet(AtPtpPsn self, uint32 entryIndex, uint8 *address)
    {
    if (entryIndex >= MaxNumDestAddressEntryGet(self))
        return cAtErrorOutOfRangParm;

    return ExpectedUnicastDestAddressGet(self, address);
    }

static eAtModulePtpRet ExpectedSourceAddressEntrySet(AtPtpPsn self, uint32 entryIndex, const uint8 *address)
    {
    if (entryIndex >= MaxNumSourceAddressEntryGet(self))
        return cAtErrorOutOfRangParm;

    return ExpectedUnicastSourceAddressSet(self, address);
    }

static eAtModulePtpRet ExpectedSourceAddressEntryGet(AtPtpPsn self, uint32 entryIndex, uint8 *address)
    {
    if (entryIndex >= MaxNumSourceAddressEntryGet(self))
        return cAtErrorOutOfRangParm;

    return ExpectedUnicastSourceAddressGet(self, address);
    }

static eAtModulePtpRet ExpectedDestAddressEntryEnable(AtPtpPsn self, uint32 entryIndex, eBool enable)
    {
    if (entryIndex >= MaxNumDestAddressEntryGet(self))
        return cAtErrorOutOfRangParm;

    return ExpectedUnicastDestAddressEnable(self, enable);
    }

static eBool ExpectedDestAddressEntryIsEnabled(AtPtpPsn self, uint32 entryIndex)
    {
    if (entryIndex >= MaxNumDestAddressEntryGet(self))
        return cAtFalse;
    return ExpectedUnicastDestAddressIsEnabled(self);
    }

static eAtModulePtpRet ExpectedSourceAddressEntryEnable(AtPtpPsn self, uint32 entryIndex, eBool enable)
    {
    if (entryIndex >= MaxNumSourceAddressEntryGet(self))
        return cAtErrorOutOfRangParm;

    return ExpectedUnicastSourceAddressEnable(self, enable);
    }

static eBool ExpectedSourceAddressEntryIsEnabled(AtPtpPsn self, uint32 entryIndex)
    {
    if (entryIndex >= MaxNumSourceAddressEntryGet(self))
        return cAtFalse;

    return ExpectedUnicastSourceAddressIsEnabled(self);
    }

static void OverrideAtPtpPsn(AtPtpL2Psn self)
    {
    AtPtpPsn psn = (AtPtpPsn)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPtpPsnOverride, mMethodsGet(psn), sizeof(m_AtPtpPsnOverride));

        /* Setup methods */
        mMethodOverride(m_AtPtpPsnOverride, ExpectedUnicastDestAddressSet);
        mMethodOverride(m_AtPtpPsnOverride, ExpectedUnicastDestAddressGet);
        mMethodOverride(m_AtPtpPsnOverride, ExpectedUnicastDestAddressEnable);
        mMethodOverride(m_AtPtpPsnOverride, ExpectedUnicastDestAddressIsEnabled);
        mMethodOverride(m_AtPtpPsnOverride, ExpectedMcastDestAddressSet);
        mMethodOverride(m_AtPtpPsnOverride, ExpectedMcastDestAddressGet);
        mMethodOverride(m_AtPtpPsnOverride, ExpectedMcastDestAddressEnable);
        mMethodOverride(m_AtPtpPsnOverride, ExpectedMcastDestAddressIsEnabled);
        mMethodOverride(m_AtPtpPsnOverride, ExpectedAnycastDestAddressEnable);
        mMethodOverride(m_AtPtpPsnOverride, ExpectedAnycastDestAddressIsEnabled);
        mMethodOverride(m_AtPtpPsnOverride, ExpectedUnicastSourceAddressSet);
        mMethodOverride(m_AtPtpPsnOverride, ExpectedUnicastSourceAddressGet);
        mMethodOverride(m_AtPtpPsnOverride, ExpectedUnicastSourceAddressEnable);
        mMethodOverride(m_AtPtpPsnOverride, ExpectedUnicastSourceAddressIsEnabled);
        mMethodOverride(m_AtPtpPsnOverride, ExpectedAnycastSourceAddressEnable);
        mMethodOverride(m_AtPtpPsnOverride, ExpectedAnycastSourceAddressIsEnabled);

        mMethodOverride(m_AtPtpPsnOverride, MaxNumDestAddressEntryGet);
        mMethodOverride(m_AtPtpPsnOverride, MaxNumSourceAddressEntryGet);
        mMethodOverride(m_AtPtpPsnOverride, ExpectedDestAddressEntrySet);
        mMethodOverride(m_AtPtpPsnOverride, ExpectedDestAddressEntryGet);
        mMethodOverride(m_AtPtpPsnOverride, ExpectedSourceAddressEntrySet);
        mMethodOverride(m_AtPtpPsnOverride, ExpectedSourceAddressEntryGet);
        mMethodOverride(m_AtPtpPsnOverride, ExpectedDestAddressEntryEnable);
        mMethodOverride(m_AtPtpPsnOverride, ExpectedDestAddressEntryIsEnabled);
        mMethodOverride(m_AtPtpPsnOverride, ExpectedSourceAddressEntryEnable);
        mMethodOverride(m_AtPtpPsnOverride, ExpectedSourceAddressEntryIsEnabled);

        mMethodOverride(m_AtPtpPsnOverride, ExpectedCVlanSet);
        mMethodOverride(m_AtPtpPsnOverride, ExpectedCVlanGet);
        mMethodOverride(m_AtPtpPsnOverride, ExpectedSVlanSet);
        mMethodOverride(m_AtPtpPsnOverride, ExpectedSVlanGet);
        }

    mMethodsSet(psn, &m_AtPtpPsnOverride);
    }

static void Override(AtPtpL2Psn self)
    {
    OverrideAtPtpPsn(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPtpL2Psn);
    }

static AtPtpL2Psn ObjectInit(AtPtpL2Psn self, AtPtpPort ptpPort)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPtpL2PsnObjectInit(self, ptpPort) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPtpL2Psn ThaPtpL2PsnNew(AtPtpPort port)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPtpL2Psn newPsn = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPsn == NULL)
        return NULL;

    return ObjectInit(newPsn, port);
    }


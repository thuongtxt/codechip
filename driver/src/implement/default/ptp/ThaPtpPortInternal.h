/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PTP
 * 
 * File        : ThaPtpPortInternal.h
 * 
 * Created Date: Jul 6, 2018
 *
 * Description : Default PTP port representation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPTPPORTINTERNAL_H_
#define _THAPTPPORTINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/ptp/AtPtpPortInternal.h"
#include "ThaPtpPort.h"
#include "controllers/ThaPtpClaController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPtpPortMethods
    {
    uint32 (*CounterRead2Clear)(ThaPtpPort self, uint16 counterType, eBool r2c);
    eBool  (*ExpectedVlanIsSupported)(ThaPtpPort self);
    uint32 (*TxDelayAdjustDefaultVlue)(ThaPtpPort self, eAtSerdesMode serdesMode);
    uint32 (*RxDelayAdjustDefaultVlue)(ThaPtpPort self, eAtSerdesMode serdesMode);
    }tThaPtpPortMethods;

typedef struct tThaPtpPort
    {
    tAtPtpPort super;
    const tThaPtpPortMethods *methods;

    /* Private data */
    ThaPtpClaController controller;
    }tThaPtpPort;

typedef struct tThaPtpBackplanePort
    {
    tThaPtpPort super;

    /* Private data */
    }tThaPtpBackplanePort;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPtpPort ThaPtpPortObjectInit(AtPtpPort self, uint32 channelId, AtModule module);
AtPtpPort ThaPtpBackplanePortObjectInit(AtPtpPort self, uint32 channelId, AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _THAPTPPORTINTERNAL_H_ */


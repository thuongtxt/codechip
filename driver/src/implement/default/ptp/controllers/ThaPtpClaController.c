/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PTP
 *
 * File        : ThaPtpClaController.c
 *
 * Created Date: Jul 6, 2018
 *
 * Description : Default PTP CLA controller implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/common/AtChannelInternal.h"
#include "../../man/ThaDevice.h"
#include "../ThaModulePtp.h"
#include "../ThaPtpPort.h"
#include "../ThaModulePtpReg.h"
#include "ThaPtpClaControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaPtpAddrOctet_Mask(idx)      (cBit7_0 << ((idx) * 8))
#define cThaPtpAddrOctet_Shift(idx)     ((idx) * 8)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaPtpClaControllerMethods m_methods;

/* Override */
static tAtObjectMethods         m_AtObjectOverride;

/* Save superclass implementation */
static const tAtObjectMethods  *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePtp PtpModule(ThaPtpClaController self)
    {
    return (ThaModulePtp)AtChannelModuleGet((AtChannel)ThaPtpClaControllerPortGet(self));
    }

static uint32 BaseAddress(ThaPtpClaController self)
    {
    return ThaModulePtpBaseAddress(PtpModule(self));
    }

static uint32 DefaultOffset(ThaPtpClaController self)
    {
    ThaPtpPort port = (ThaPtpPort)ThaPtpClaControllerPortGet(self);
    return ThaPtpPortDefaultOffset(port);
    }

static uint32 FaceplateExpectedVlanRegister(ThaPtpClaController self)
    {
    return BaseAddress(self) + DefaultOffset(self) + cAf6Reg_upen_face_vid_perport_Base;
    }

static uint32 FlatLocalId(ThaPtpClaController self)
    {
    ThaPtpPort port = (ThaPtpPort)ThaPtpClaControllerPortGet(self);
    return AtChannelIdGet((AtChannel)port) - 1U;
    }

static uint32 BackplaneExpectedVlanRegister(ThaPtpClaController self)
    {
    return BaseAddress(self) + FlatLocalId(self) + cAf6Reg_upen_bp_vid_perport_Base;
    }

static eAtRet HelperExpectedVlanSet(ThaPtpClaController self, tAtEthVlanTag *aTag,
                                    uint32 address,
                                    uint32 vlanEnableMask,
                                    uint32 vlanIdMask, uint32 vlanIdShift)
    {
    uint32 regVal = ThaPtpClaControllerRead(self, address);
    regVal = (aTag) ? (regVal | vlanEnableMask) : (regVal & ~vlanEnableMask);
    mRegFieldSet(regVal, vlanId, (aTag) ? aTag->vlanId : 0);
    ThaPtpClaControllerWrite(self, address, regVal);

    return cAtOk;
    }

static tAtEthVlanTag* HelperExpectedVlanGet(ThaPtpClaController self, tAtEthVlanTag *aTag,
                                            uint32 address,
                                            uint32 vlanEnableMask,
                                            uint32 vlanIdMask, uint32 vlanIdShift)
    {
    uint32 regVal;

    if (aTag == NULL)
        return NULL;

    regVal = ThaPtpClaControllerRead(self, address);
    if (!(regVal & vlanEnableMask)) /* Disabled. */
        return NULL;

    aTag->vlanId = (uint16)mRegField(regVal, vlanId);
    return aTag;
    }

static eAtModulePtpRet HelperFaceplateExpectedVlanSet(ThaPtpClaController self, tAtEthVlanTag *aTag,
                                                      uint32 vlanEnableMask,
                                                      uint32 vlanIdMask, uint32 vlanIdShift)
    {
    uint32 address = FaceplateExpectedVlanRegister(self);
    return HelperExpectedVlanSet(self, aTag, address, vlanEnableMask, vlanIdMask, vlanIdShift);
    }

static tAtEthVlanTag* HelperFaceplateExpectedVlanGet(ThaPtpClaController self, tAtEthVlanTag *aTag,
                                                     uint32 vlanEnableMask,
                                                     uint32 vlanIdMask, uint32 vlanIdShift)
    {
    uint32 address = FaceplateExpectedVlanRegister(self);
    return HelperExpectedVlanGet(self, aTag, address, vlanEnableMask, vlanIdMask, vlanIdShift);
    }

static eAtModulePtpRet HelperBackplaneExpectedVlanSet(ThaPtpClaController self, tAtEthVlanTag *aTag,
                                                      uint32 vlanEnableMask,
                                                      uint32 vlanIdMask, uint32 vlanIdShift)
    {
    uint32 address = BackplaneExpectedVlanRegister(self);
    return HelperExpectedVlanSet(self, aTag, address, vlanEnableMask, vlanIdMask, vlanIdShift);
    }

static eAtModulePtpRet FaceplateExpectedCVlanSet(ThaPtpClaController self, tAtEthVlanTag *cTag)
    {
    return HelperFaceplateExpectedVlanSet(self, cTag,
                                          cAf6_upen_face_vid_perport_ena_vlanid1_Mask,
                                          cAf6_upen_face_vid_perport_vlanid1_Mask,
                                          cAf6_upen_face_vid_perport_vlanid1_Shift);
    }

static eAtModulePtpRet BackplaneExpectedCVlanSet(ThaPtpClaController self, tAtEthVlanTag *cTag)
    {
    return HelperBackplaneExpectedVlanSet(self, cTag,
                                          cAf6_upen_bp_vid_perport_ena_bpvlanid1_Mask,
                                          cAf6_upen_bp_vid_perport_cfg_bp_vid1_Mask,
                                          cAf6_upen_bp_vid_perport_cfg_bp_vid1_Shift);
    }

static eAtModulePtpRet ExpectedCVlanSet(ThaPtpClaController self, tAtEthVlanTag *cTag)
    {
    eAtRet ret;
    ret  = FaceplateExpectedCVlanSet(self, cTag);
    ret |= BackplaneExpectedCVlanSet(self, cTag);
    return ret;
    }

static tAtEthVlanTag* ExpectedCVlanGet(ThaPtpClaController self, tAtEthVlanTag *cTag)
    {
    return HelperFaceplateExpectedVlanGet(self, cTag,
                                          cAf6_upen_face_vid_perport_ena_vlanid1_Mask,
                                          cAf6_upen_face_vid_perport_vlanid1_Mask,
                                          cAf6_upen_face_vid_perport_vlanid1_Shift);
    }

static eAtModulePtpRet FaceplateExpectedSVlanSet(ThaPtpClaController self, tAtEthVlanTag *sTag)
    {
    return HelperFaceplateExpectedVlanSet(self, sTag,
                                          cAf6_upen_face_vid_perport_ena_vlanid2_Mask,
                                          cAf6_upen_face_vid_perport_vlanid2_Mask,
                                          cAf6_upen_face_vid_perport_vlanid2_Shift);
    }

static eAtModulePtpRet BackplaneExpectedSVlanSet(ThaPtpClaController self, tAtEthVlanTag *sTag)
    {
    return HelperBackplaneExpectedVlanSet(self, sTag,
                                          cAf6_upen_bp_vid_perport_ena_bpvlanid2_Mask,
                                          cAf6_upen_bp_vid_perport_cfg_bp_vid2_Mask,
                                          cAf6_upen_bp_vid_perport_cfg_bp_vid2_Shift);
    }

static eAtModulePtpRet ExpectedSVlanSet(ThaPtpClaController self, tAtEthVlanTag *sTag)
    {
    eAtRet ret;
    ret  = FaceplateExpectedSVlanSet(self, sTag);
    ret |= BackplaneExpectedSVlanSet(self, sTag);
    return ret;
    }

static tAtEthVlanTag* ExpectedSVlanGet(ThaPtpClaController self, tAtEthVlanTag *sTag)
    {
    return HelperFaceplateExpectedVlanGet(self, sTag,
                                          cAf6_upen_face_vid_perport_ena_vlanid2_Mask,
                                          cAf6_upen_face_vid_perport_vlanid2_Mask,
                                          cAf6_upen_face_vid_perport_vlanid2_Shift);
    }

static eBool LocalAddressIsValid(uint32 localAddress)
    {
    return (localAddress == cInvalidUint32) ? cAtFalse : cAtTrue;
    }

static eAtRet AddressSet(ThaPtpClaController self, uint32 localAddress, const uint8 *address, uint8 addressLen)
    {
    uint32 regAddr;
    uint32 longRegVal[cThaLongRegMaxSize];
    uint8 i, word_i = 0;
    uint8 byte_i;

    if (!LocalAddressIsValid(localAddress))
        return cAtErrorModeNotSupport;

    regAddr = BaseAddress(self) + localAddress;
    ThaPtpClaControllerLongRead(self, regAddr, longRegVal, cThaLongRegMaxSize);
    for (i = 0, byte_i = (uint8)(addressLen - 1); i < addressLen; i++, byte_i--)
        {
        uint8 j = (uint8)(i % 4);
        mFieldIns(&longRegVal[word_i],
                  cThaPtpAddrOctet_Mask(j),
                  cThaPtpAddrOctet_Shift(j),
                  address[byte_i]);
        if (j == 3)  /* Next word */
            word_i++;
        }
    ThaPtpClaControllerLongWrite(self, regAddr, longRegVal, cThaLongRegMaxSize);

    return cAtOk;
    }

static eAtRet AddressGet(ThaPtpClaController self, uint32 localAddress, uint8 *address, uint8 addressLen)
    {
    uint32 regAddr;
    uint32 longRegVal[cThaLongRegMaxSize];
    uint8 i, word_i = 0;
    uint8 byte_i;

    if (!LocalAddressIsValid(localAddress))
        return cAtErrorModeNotSupport;

    regAddr = BaseAddress(self) + localAddress;
    ThaPtpClaControllerLongRead(self, regAddr, longRegVal, cThaLongRegMaxSize);
    for (i = 0, byte_i = (uint8)(addressLen - 1); i < addressLen; i++, byte_i--)
        {
        uint8 j = (uint8)(i % 4);
        mFieldGet(longRegVal[word_i],
                  cThaPtpAddrOctet_Mask(j),
                  cThaPtpAddrOctet_Shift(j),
                  uint8,
                  &address[byte_i]);
        if (j == 3) /* Next word */
            word_i++;
        }

    return cAtOk;
    }

static uint32 MacAddressRegister(ThaPtpClaController self, uint8 macAddrType)
    {
    AtUnused(self);
    switch (macAddrType)
        {
        case cAtPtpMacDestAddressUnicast:
            return cAf6Reg_upen_mac_Base + DefaultOffset(self);
        case cAtPtpMacDestAddressMulticast:
            return cAf6Reg_upen_mac_Base + 16U + DefaultOffset(self);
        default:
            return cInvalidUint32;
        }
    }

static eAtRet MacAddressSet(ThaPtpClaController self, uint8 macAddrType, const uint8 *address)
    {
    return ThaPtpClaControllerAddressSet(self, MacAddressRegister(self, macAddrType), address, cAtMacAddressLen);
    }

static eAtRet MacAddressGet(ThaPtpClaController self, uint8 macAddrType, uint8 *address)
    {
    return ThaPtpClaControllerAddressGet(self, MacAddressRegister(self, macAddrType), address, cAtMacAddressLen);
    }

static uint8 ChannelShift(ThaPtpClaController self)
    {
    return (uint8)AtChannelHwIdGet((AtChannel)ThaPtpClaControllerPortGet(self));
    }

static uint32 ChannelMask(ThaPtpClaController self)
    {
    return cBit0 << ChannelShift(self);
    }

static eAtRet AnycastDestAddressEnable(ThaPtpClaController self, uint32 localAddress, eBool enable)
    {
    uint32 regAddr = BaseAddress(self) + localAddress;
    uint32 regVal = ThaPtpClaControllerRead(self, regAddr);
    mFieldIns(&regVal, ChannelMask(self), ChannelShift(self), mBoolToBin(enable));
    ThaPtpClaControllerWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool AnycastDestAddressIsEnabled(ThaPtpClaController self, uint32 localAddress)
    {
    uint32 regAddr = BaseAddress(self) + localAddress;
    uint32 regVal = ThaPtpClaControllerRead(self, regAddr);
    return (regVal & ChannelMask(self)) ? cAtTrue : cAtFalse;
    }

static eAtRet HelperMacAddressEnable(ThaPtpClaController self, uint32 localAddress, eBool enable)
    {
    uint32 regAddr;
    uint32 longRegVal[cThaLongRegMaxSize];

    if (!LocalAddressIsValid(localAddress))
        return cAtErrorModeNotSupport;

    regAddr = BaseAddress(self) + localAddress;
    ThaPtpClaControllerLongRead(self, regAddr, longRegVal, cThaLongRegMaxSize);
    mRegFieldSet(longRegVal[1], cAf6_upen_mac_macena_, mBoolToBin(enable));
    ThaPtpClaControllerLongWrite(self, regAddr, longRegVal, cThaLongRegMaxSize);

    return cAtOk;
    }

static eBool HelperMacAddressIsEnabled(ThaPtpClaController self, uint32 localAddress)
    {
    uint32 regAddr;
    uint32 longRegVal[cThaLongRegMaxSize];

    if (!LocalAddressIsValid(localAddress))
        return cAtFalse;

    regAddr = BaseAddress(self) + localAddress;
    ThaPtpClaControllerLongRead(self, regAddr, longRegVal, cThaLongRegMaxSize);
    return mRegField(longRegVal[1], cAf6_upen_mac_macena_) ? cAtTrue : cAtFalse;
    }

static eAtRet MacAddressEnable(ThaPtpClaController self, uint8 addrType, eBool enable)
    {
    if (addrType == cAtPtpMacDestAddressAnycast)
        return AnycastDestAddressEnable(self, cAf6Reg_upen_anymac_en, enable);

    return HelperMacAddressEnable(self, MacAddressRegister(self, addrType), enable);
    }

static eBool MacAddressIsEnabled(ThaPtpClaController self, uint8 addrType)
    {
    if (addrType == cAtPtpMacDestAddressAnycast)
        return AnycastDestAddressIsEnabled(self, cAf6Reg_upen_anymac_en);

    return HelperMacAddressIsEnabled(self, MacAddressRegister(self, addrType));
    }

static uint32 IpAddressRegister(ThaPtpClaController self, uint8 ipAddrType, uint32 entryIndex)
    {
    AtUnused(self);
    switch (ipAddrType)
        {
        case cAtPtpIpDestAddressUnicast:
            return cAf6Reg_upen_geip1_Base + entryIndex * 0x20U + DefaultOffset(self);

        case cAtPtpIpDestAddressMulticast:
            if (entryIndex > 0)
                return cInvalidUint32;
            return cAf6Reg_upen_geip1_Base + 16U + DefaultOffset(self);

        default:
            return cInvalidUint32;
        }
    }

static eBool AddressEntryIsValid(uint8 ipAddrType, uint32 entryIndex)
    {
    if (ipAddrType == cAtPtpIpDestAddressUnicast)
        return (entryIndex < 4) ? cAtTrue : cAtFalse;

    if (ipAddrType == cAtPtpIpGlobalAddressMulticast)
        return (entryIndex < 2) ? cAtTrue : cAtFalse;

    return (entryIndex > 0) ? cAtFalse : cAtTrue;
    }

static eAtRet HelperIpAddressEnable(ThaPtpClaController self, uint8 ipAddrType, uint32 entryIndex, eBool enable)
    {
    uint32 regAddr = BaseAddress(self) + cAf6Reg_upen_geipen_Base + DefaultOffset(self);
    uint32 regVal;
    uint32 cAddressEnMask;
    uint8 cAddressEnShift;

    if (!AddressEntryIsValid(ipAddrType, entryIndex))
        return cAtErrorInvlParm;

    switch (ipAddrType)
        {
        case cAtPtpIpDestAddressUnicast:
            cAddressEnMask = cBit0 << entryIndex;
            cAddressEnShift = (uint8)entryIndex;
            break;

        case cAtPtpIpDestAddressMulticast:
            cAddressEnMask = cBit4;
            cAddressEnShift = 4;
            break;

        case cAtPtpIpGlobalAddressMulticast:
            cAddressEnMask = cBit5 << entryIndex;
            cAddressEnShift = (uint8)(entryIndex + 5);
            break;

        default:
            return cAtErrorModeNotSupport;
        }

    regVal = ThaPtpClaControllerRead(self, regAddr);
    mRegFieldSet(regVal, cAddressEn, mBoolToBin(enable));
    ThaPtpClaControllerWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool HelperIpAddressIsEnabled(ThaPtpClaController self, uint8 ipAddrType, uint32 entryIndex)
    {
    uint32 regAddr = BaseAddress(self) + cAf6Reg_upen_geipen_Base + DefaultOffset(self);
    uint32 regVal;
    uint32 cAddressEnMask;
    uint8 cAddressEnShift;

    if (!AddressEntryIsValid(ipAddrType, entryIndex))
        return cAtFalse;

    switch (ipAddrType)
        {
        case cAtPtpIpDestAddressUnicast:
            cAddressEnMask = cBit0 << entryIndex;
            cAddressEnShift = (uint8)entryIndex;
            break;

        case cAtPtpIpDestAddressMulticast:
            cAddressEnMask = cBit4;
            cAddressEnShift = 4;
            break;

        case cAtPtpIpGlobalAddressMulticast:
            cAddressEnMask = cBit5 << entryIndex;
            cAddressEnShift = (uint8)(entryIndex + 5);
            break;

        default:
            return cAtFalse;
        }

    regVal = ThaPtpClaControllerRead(self, regAddr);
    return mRegField(regVal, cAddressEn) ? cAtTrue : cAtFalse;
    }

static eAtRet IpV4AddressEnable(ThaPtpClaController self, uint8 ipAddrType, uint32 entryIndex, eBool enable)
    {
    if (ipAddrType == cAtPtpIpDestAddressAnycast)
        return AnycastDestAddressEnable(self, cAf6Reg_upen_anyip_en, enable);

    return HelperIpAddressEnable(self, ipAddrType, entryIndex, enable);
    }

static eBool IpV4AddressIsEnabled(ThaPtpClaController self, uint8 ipAddrType, uint32 entryIndex)
    {
    if (ipAddrType == cAtPtpIpDestAddressAnycast)
        return AnycastDestAddressIsEnabled(self, cAf6Reg_upen_anyip_en);

    return HelperIpAddressIsEnabled(self, ipAddrType, entryIndex);
    }

static eAtRet IpV4AddressSet(ThaPtpClaController self, uint8 ipAddrType, uint32 entryIndex, const uint8 *address)
    {
    return ThaPtpClaControllerAddressSet(self, IpAddressRegister(self, ipAddrType, entryIndex), address, cAtIpv4AddressLen);
    }

static eAtRet IpV4AddressGet(ThaPtpClaController self, uint8 ipAddrType, uint32 entryIndex, uint8 *address)
    {
    return ThaPtpClaControllerAddressGet(self, IpAddressRegister(self, ipAddrType, entryIndex), address, cAtIpv4AddressLen);
    }

static eAtRet MaxNumIpV4AddressesGet(ThaPtpClaController self, uint8 ipAddrType)
    {
    AtUnused(self);
    if (ipAddrType == cAtPtpIpDestAddressUnicast)
        return 4;
    if (ipAddrType == cAtPtpIpDestAddressMulticast)
        return 1;
    return 0;
    }

static eAtRet IpV6AddressEnable(ThaPtpClaController self, uint8 ipAddrType, uint32 entryIndex, eBool enable)
    {
    if (ipAddrType == cAtPtpIpDestAddressAnycast)
        return AnycastDestAddressEnable(self, cAf6Reg_upen_anyip_en, enable);

    return HelperIpAddressEnable(self, ipAddrType, entryIndex, enable);
    }

static eBool IpV6AddressIsEnabled(ThaPtpClaController self, uint8 ipAddrType, uint32 entryIndex)
    {
    if (ipAddrType == cAtPtpIpDestAddressAnycast)
        return AnycastDestAddressIsEnabled(self, cAf6Reg_upen_anyip_en);

    return HelperIpAddressIsEnabled(self, ipAddrType, entryIndex);
    }

static eAtRet IpV6AddressSet(ThaPtpClaController self, uint8 ipAddrType, uint32 entryIndex, const uint8 *address)
    {
    return ThaPtpClaControllerAddressSet(self, IpAddressRegister(self, ipAddrType, entryIndex), address, cAtIpv6AddressLen);
    }

static eAtRet IpV6AddressGet(ThaPtpClaController self, uint8 ipAddrType, uint32 entryIndex, uint8 *address)
    {
    return ThaPtpClaControllerAddressGet(self, IpAddressRegister(self, ipAddrType, entryIndex), address, cAtIpv6AddressLen);
    }

static eAtRet MaxNumIpV6AddressesGet(ThaPtpClaController self, uint8 ipAddrType)
    {
    AtUnused(self);
    if (ipAddrType == cAtPtpIpDestAddressUnicast)
        return 4;
    if (ipAddrType == cAtPtpIpDestAddressMulticast)
        return 1;
    return 0;
    }

static const char *ToString(AtObject self)
    {
    AtUnused(self);
    return "ptp_cla_controller";
    }

static void Delete(AtObject self)
    {
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaPtpClaController object = (ThaPtpClaController)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObjectDescription(port);
    }

static void OverrideAtObject(ThaPtpClaController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(tAtObjectMethods));

        mMethodOverride(m_AtObjectOverride, ToString);
        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void MethodsInit(ThaPtpClaController self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, ExpectedCVlanSet);
        mMethodOverride(m_methods, ExpectedCVlanGet);
        mMethodOverride(m_methods, ExpectedSVlanSet);
        mMethodOverride(m_methods, ExpectedSVlanGet);

        mMethodOverride(m_methods, MacAddressSet);
        mMethodOverride(m_methods, MacAddressGet);
        mMethodOverride(m_methods, MacAddressEnable);
        mMethodOverride(m_methods, MacAddressIsEnabled);
        mMethodOverride(m_methods, MaxNumIpV4AddressesGet);
        mMethodOverride(m_methods, IpV4AddressSet);
        mMethodOverride(m_methods, IpV4AddressGet);
        mMethodOverride(m_methods, IpV4AddressEnable);
        mMethodOverride(m_methods, IpV4AddressIsEnabled);
        mMethodOverride(m_methods, MaxNumIpV6AddressesGet);
        mMethodOverride(m_methods, IpV6AddressSet);
        mMethodOverride(m_methods, IpV6AddressGet);
        mMethodOverride(m_methods, IpV6AddressEnable);
        mMethodOverride(m_methods, IpV6AddressIsEnabled);
        }

    mMethodsSet(self, &m_methods);
    }

static void Override(ThaPtpClaController self)
    {
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPtpClaController);
    }

ThaPtpClaController ThaPtpClaControllerObjectInit(ThaPtpClaController self, AtPtpPort port)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;
    self->port = port;

    return self;
    }

ThaPtpClaController ThaPtpClaControllerNew(AtPtpPort port)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPtpClaController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    return ThaPtpClaControllerObjectInit(newController, port);
    }

AtPtpPort ThaPtpClaControllerPortGet(ThaPtpClaController self)
    {
    return (self) ? self->port : NULL;
    }

uint32 ThaPtpClaControllerRead(ThaPtpClaController self, uint32 address)
    {
    return AtChannelHwReadOnModule((AtChannel)ThaPtpClaControllerPortGet(self), address, cAtModulePtp);
    }

void ThaPtpClaControllerWrite(ThaPtpClaController self, uint32 address, uint32 value)
    {
    AtChannelHwWriteOnModule((AtChannel)ThaPtpClaControllerPortGet(self), address, value, cAtModulePtp);
    }

uint16 ThaPtpClaControllerLongRead(ThaPtpClaController self, uint32 address, uint32 *dataBuffer, uint16 bufferLen)
    {
    return AtChannelHwLongReadOnModule((AtChannel)ThaPtpClaControllerPortGet(self), address, dataBuffer, bufferLen, cAtModulePtp);
    }

uint16 ThaPtpClaControllerLongWrite(ThaPtpClaController self, uint32 address, uint32 *dataBuffer, uint16 bufferLen)
    {
    return AtChannelHwLongWriteOnModule((AtChannel)ThaPtpClaControllerPortGet(self), address, dataBuffer, bufferLen, cAtModulePtp);
    }

eAtRet ThaPtpClaControllerMacAddressSet(ThaPtpClaController self, uint8 addrType, const uint8 *address)
    {
    if (self)
        return mMethodsGet(self)->MacAddressSet(self, addrType, address);
    return cAtErrorNullPointer;
    }

eAtRet ThaPtpClaControllerMacAddressGet(ThaPtpClaController self, uint8 addrType, uint8 *address)
    {
    if (self)
        return mMethodsGet(self)->MacAddressGet(self, addrType, address);
    return cAtErrorNullPointer;
    }

eAtRet ThaPtpClaControllerMacAddressEnable(ThaPtpClaController self, uint8 addrType, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->MacAddressEnable(self, addrType, enable);
    return cAtErrorNullPointer;
    }

eBool ThaPtpClaControllerMacAddressIsEnabled(ThaPtpClaController self, uint8 addrType)
    {
    if (self)
        return mMethodsGet(self)->MacAddressIsEnabled(self, addrType);
    return cAtFalse;
    }

uint32 ThaPtpClaControllerMaxNumIpV4AddressesGet(ThaPtpClaController self, uint8 ipAddrType)
    {
    if (self)
        return mMethodsGet(self)->MaxNumIpV4AddressesGet(self, ipAddrType);
    return 0;
    }

eAtRet ThaPtpClaControllerIpV4AddressSet(ThaPtpClaController self, uint8 addrType, uint32 entryIndex, const uint8 *address)
    {
    if (self)
        return mMethodsGet(self)->IpV4AddressSet(self, addrType, entryIndex, address);
    return cAtErrorNullPointer;
    }

eAtRet ThaPtpClaControllerIpV4AddressGet(ThaPtpClaController self, uint8 addrType, uint32 entryIndex, uint8 *address)
    {
    if (self)
        return mMethodsGet(self)->IpV4AddressGet(self, addrType, entryIndex, address);
    return cAtErrorNullPointer;
    }

eAtRet ThaPtpClaControllerIpV4AddressEnable(ThaPtpClaController self, uint8 addrType, uint32 entryIndex, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->IpV4AddressEnable(self, addrType, entryIndex, enable);
    return cAtErrorNullPointer;
    }

eBool ThaPtpClaControllerIpV4AddressIsEnabled(ThaPtpClaController self, uint8 addrType, uint32 entryIndex)
    {
    if (self)
        return mMethodsGet(self)->IpV4AddressIsEnabled(self, addrType, entryIndex);
    return cAtFalse;
    }

uint32 ThaPtpClaControllerMaxNumIpV6AddressesGet(ThaPtpClaController self, uint8 ipAddrType)
    {
    if (self)
        return mMethodsGet(self)->MaxNumIpV6AddressesGet(self, ipAddrType);
    return 0;
    }

eAtRet ThaPtpClaControllerIpV6AddressSet(ThaPtpClaController self, uint8 addrType, uint32 entryIndex, const uint8 *address)
    {
    if (self)
        return mMethodsGet(self)->IpV6AddressSet(self, addrType, entryIndex, address);
    return cAtErrorNullPointer;
    }

eAtRet ThaPtpClaControllerIpV6AddressGet(ThaPtpClaController self, uint8 addrType, uint32 entryIndex, uint8 *address)
    {
    if (self)
        return mMethodsGet(self)->IpV6AddressGet(self, addrType, entryIndex, address);
    return cAtErrorNullPointer;
    }

eAtRet ThaPtpClaControllerIpV6AddressEnable(ThaPtpClaController self, uint8 addrType, uint32 entryIndex, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->IpV6AddressEnable(self, addrType, entryIndex, enable);
    return cAtErrorNullPointer;
    }

eBool ThaPtpClaControllerIpV6AddressIsEnabled(ThaPtpClaController self, uint8 addrType, uint32 entryIndex)
    {
    if (self)
        return mMethodsGet(self)->IpV6AddressIsEnabled(self, addrType, entryIndex);
    return cAtFalse;
    }

eAtRet ThaPtpClaControllerAddressSet(ThaPtpClaController self, uint32 localAddress, const uint8 *address, uint8 addressLen)
    {
    if (self)
        return AddressSet(self, localAddress, address, addressLen);
    return cAtErrorNullPointer;
    }

eAtRet ThaPtpClaControllerAddressGet(ThaPtpClaController self, uint32 localAddress, uint8 *address, uint8 addressLen)
    {
    if (self)
        return AddressGet(self, localAddress, address, addressLen);
    return cAtErrorNullPointer;
    }

eAtModulePtpRet ThaPtpClaControllerExpectedCVlanSet(ThaPtpClaController self, tAtEthVlanTag *cTag)
    {
    if (self)
        return mMethodsGet(self)->ExpectedCVlanSet(self, cTag);
    return cAtErrorNullPointer;
    }

tAtEthVlanTag* ThaPtpClaControllerExpectedCVlanGet(ThaPtpClaController self, tAtEthVlanTag *cTag)
    {
    if (self)
        return mMethodsGet(self)->ExpectedCVlanGet(self, cTag);
    return NULL;
    }

eAtModulePtpRet ThaPtpClaControllerExpectedSVlanSet(ThaPtpClaController self, tAtEthVlanTag *sTag)
    {
    if (self)
        return mMethodsGet(self)->ExpectedSVlanSet(self, sTag);
    return cAtErrorNullPointer;
    }

tAtEthVlanTag* ThaPtpClaControllerExpectedSVlanGet(ThaPtpClaController self, tAtEthVlanTag *sTag)
    {
    if (self)
        return mMethodsGet(self)->ExpectedSVlanGet(self, sTag);
    return NULL;
    }

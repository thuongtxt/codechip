/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PTP
 * 
 * File        : ThaPtpClaControllerInternal.h
 * 
 * Created Date: Jul 6, 2018
 *
 * Description : Default PTP CLA controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPTPCLACONTROLLERINTERNAL_H_
#define _THAPTPCLACONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h"
#include "AtModulePtp.h"
#include "ThaPtpClaController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPtpClaControllerMethods
    {
    /* expected vlan */
    eAtModulePtpRet (*ExpectedCVlanSet)(ThaPtpClaController self, tAtEthVlanTag *cTag);
    tAtEthVlanTag* (*ExpectedCVlanGet)(ThaPtpClaController self, tAtEthVlanTag *cTag);
    eAtModulePtpRet (*ExpectedSVlanSet)(ThaPtpClaController self, tAtEthVlanTag *sTag);
    tAtEthVlanTag* (*ExpectedSVlanGet)(ThaPtpClaController self, tAtEthVlanTag *sTag);

    /* MAC */
    eAtRet (*MacAddressSet)(ThaPtpClaController self, uint8 macAddrType, const uint8 *address);
    eAtRet (*MacAddressGet)(ThaPtpClaController self, uint8 macAddrType, uint8 *address);
    eAtRet (*MacAddressEnable)(ThaPtpClaController self, uint8 macAddrType, eBool enable);
    eBool (*MacAddressIsEnabled)(ThaPtpClaController self, uint8 macAddrType);

    /* IPv4 */
    uint32 (*MaxNumIpV4AddressesGet)(ThaPtpClaController self, uint8 ipAddrType);
    eAtRet (*IpV4AddressSet)(ThaPtpClaController self, uint8 ipAddrType, uint32 entryIndex, const uint8 *address);
    eAtRet (*IpV4AddressGet)(ThaPtpClaController self, uint8 ipAddrType, uint32 entryIndex, uint8 *address);
    eAtRet (*IpV4AddressEnable)(ThaPtpClaController self, uint8 ipAddrType, uint32 entryIndex, eBool enable);
    eBool (*IpV4AddressIsEnabled)(ThaPtpClaController self, uint8 ipAddrType, uint32 entryIndex);

    /* IPv6 */
    uint32 (*MaxNumIpV6AddressesGet)(ThaPtpClaController self, uint8 ipAddrType);
    eAtRet (*IpV6AddressSet)(ThaPtpClaController self, uint8 ipAddrType, uint32 entryIndex, const uint8 *address);
    eAtRet (*IpV6AddressGet)(ThaPtpClaController self, uint8 ipAddrType, uint32 entryIndex, uint8 *address);
    eAtRet (*IpV6AddressEnable)(ThaPtpClaController self, uint8 ipAddrType, uint32 entryIndex, eBool enable);
    eBool (*IpV6AddressIsEnabled)(ThaPtpClaController self, uint8 ipAddrType, uint32 entryIndex);
    }tThaPtpClaControllerMethods;

typedef struct tThaPtpClaController
    {
    tAtObject super;
    const tThaPtpClaControllerMethods *methods;

    /* Private data */
    AtPtpPort port;
    }tThaPtpClaController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPtpClaController ThaPtpClaControllerObjectInit(ThaPtpClaController self, AtPtpPort port);

#ifdef __cplusplus
}
#endif
#endif /* _THAPTPCLACONTROLLERINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PTP
 *
 * File        : ThaPtpBackplaneClaController.c
 *
 * Created Date: Jul 10, 2018
 *
 * Description : Default PTP Backplane CLA controller implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/common/AtChannelInternal.h"
#include "../../man/ThaDevice.h"
#include "../ThaModulePtp.h"
#include "../ThaModulePtpReg.h"
#include "ThaPtpClaControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaPtpBackplaneClaController
    {
    tThaPtpClaController super;
    }tThaPtpBackplaneClaController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods             m_AtObjectOverride;
static tThaPtpClaControllerMethods  m_ThaPtpClaControllerOverride;

/* Save superclass implementation */
static const tAtObjectMethods  *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePtp PtpModule(ThaPtpClaController self)
    {
    return (ThaModulePtp)AtChannelModuleGet((AtChannel)ThaPtpClaControllerPortGet(self));
    }

static uint32 BaseAddress(ThaPtpClaController self)
    {
    return ThaModulePtpBaseAddress(PtpModule(self));
    }

static eAtModulePtpRet ExpectedCVlanSet(ThaPtpClaController self, tAtEthVlanTag *cTag)
    {
    AtUnused(self);
    AtUnused(cTag);
    return cAtErrorModeNotSupport;
    }

static tAtEthVlanTag* ExpectedCVlanGet(ThaPtpClaController self, tAtEthVlanTag *cTag)
    {
    AtUnused(self);
    AtUnused(cTag);
    return NULL;
    }

static eAtModulePtpRet ExpectedSVlanSet(ThaPtpClaController self, tAtEthVlanTag *sTag)
    {
    AtUnused(self);
    AtUnused(sTag);
    return cAtErrorModeNotSupport;
    }

static tAtEthVlanTag* ExpectedSVlanGet(ThaPtpClaController self, tAtEthVlanTag *sTag)
    {
    AtUnused(self);
    AtUnused(sTag);
    return NULL;
    }

static uint32 MacAddressRegister(ThaPtpClaController self, uint8 macAddrType)
    {
    AtUnused(self);
    switch (macAddrType)
        {
        case cAtPtpMacDestAddressUnicast:     return cAf6Reg_upen_cfg_mac_da;
        case cAtPtpMacSourceAddressUnicast:   return cAf6Reg_upen_cfg_mac_sa;
        default:                              return cInvalidUint32;
        }
    }

static eAtRet MacAddressSet(ThaPtpClaController self, uint8 macAddrType, const uint8 *address)
    {
    return ThaPtpClaControllerAddressSet(self, MacAddressRegister(self, macAddrType), address, cAtMacAddressLen);
    }

static eAtRet MacAddressGet(ThaPtpClaController self, uint8 macAddrType, uint8 *address)
    {
    return ThaPtpClaControllerAddressGet(self, MacAddressRegister(self, macAddrType), address, cAtMacAddressLen);
    }

static eAtRet HelperAddressEnable(ThaPtpClaController self, uint32 enableMask, eBool enable)
    {
    uint32 regAddr, regVal;

    regAddr = BaseAddress(self) + cAf6Reg_upen_cfg_ptp_bypass;
    regVal = ThaPtpClaControllerRead(self, regAddr);
    if (enable)
        regVal |= enableMask;
    else
        regVal &= (uint32)~enableMask;
    ThaPtpClaControllerWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool HelperAddressIsEnabled(ThaPtpClaController self, uint32 enableMask)
    {
    uint32 regAddr, regVal;
    regAddr = BaseAddress(self) + cAf6Reg_upen_cfg_ptp_bypass;
    regVal = ThaPtpClaControllerRead(self, regAddr);
    return (regVal & enableMask) ? cAtTrue : cAtFalse;
    }

static uint32 MacEnableMask(ThaPtpClaController self, uint8 addrType)
    {
    AtUnused(self);
    if (addrType == cAtPtpMacDestAddressAnycast)
        return cAf6_upen_cfg_ptp_bypass_cfg_ptp_bypass_l2da_Mask;

    if (addrType == cAtPtpMacSourceAddressAnycast)
        return cAf6_upen_cfg_ptp_bypass_cfg_ptp_bypass_l2sa_Mask;

    return 0;
    }

static eAtRet MacAddressEnable(ThaPtpClaController self, uint8 addrType, eBool enable)
    {
    if (addrType == cAtPtpMacDestAddressMulticast)
        return (enable) ? cAtErrorModeNotSupport : cAtOk;

    if ((addrType == cAtPtpMacDestAddressUnicast) ||
        (addrType == cAtPtpMacSourceAddressUnicast))
        return (enable) ? cAtOk : cAtErrorModeNotSupport;

    return HelperAddressEnable(self, MacEnableMask(self, addrType), enable);
    }

static eBool MacAddressIsEnabled(ThaPtpClaController self, uint8 addrType)
    {
    if (addrType == cAtPtpMacDestAddressMulticast)
        return cAtFalse;

    if ((addrType == cAtPtpMacDestAddressUnicast) ||
        (addrType == cAtPtpMacSourceAddressUnicast))
        return cAtTrue;

    return HelperAddressIsEnabled(self, MacEnableMask(self, addrType));
    }

static uint32 IpV4AddressRegister(ThaPtpClaController self, uint8 ipAddrType, uint32 entryIndex)
    {
    AtUnused(self);
    if (entryIndex > 3)
        return cInvalidUint32;

    switch (ipAddrType)
        {
        case cAtPtpIpDestAddressUnicast:
            return cAf6Reg_upen_cfg_ip4_da1 + entryIndex;

        case cAtPtpIpSourceAddressUnicast:
            return cAf6Reg_upen_cfg_ip4_sa1 + entryIndex;

        default:
            return cInvalidUint32;
        }
    }

static uint32 IpV4EnableMask(ThaPtpClaController self, uint8 addrType)
    {
    AtUnused(self);
    if (addrType == cAtPtpMacDestAddressAnycast)
        return cAf6_upen_cfg_ptp_bypass_cfg_ptp_bypass_ipv4da_Mask;

    if (addrType == cAtPtpMacSourceAddressAnycast)
        return cAf6_upen_cfg_ptp_bypass_cfg_ptp_bypass_ipv4sa_Mask;

    return 0;
    }

static eAtRet HelperIpAddressEnable(ThaPtpClaController self, uint8 ipAddrType, uint32 entryIndex, eBool enable, uint32 mask)
    {
    AtUnused(entryIndex);

    if (ipAddrType == cAtPtpIpDestAddressMulticast)
        return (enable) ? cAtErrorModeNotSupport : cAtOk;

    if ((ipAddrType == cAtPtpIpDestAddressUnicast) ||
        (ipAddrType == cAtPtpIpSourceAddressUnicast))
        return (enable) ? cAtOk : cAtErrorModeNotSupport;

    return HelperAddressEnable(self, mask, enable);
    }

static eBool HelperIpAddressIsEnabled(ThaPtpClaController self, uint8 ipAddrType, uint32 entryIndex, uint32 mask)
    {
    AtUnused(entryIndex);

    if (ipAddrType == cAtPtpIpDestAddressMulticast)
        return cAtFalse;

    if ((ipAddrType == cAtPtpIpDestAddressUnicast) ||
        (ipAddrType == cAtPtpIpSourceAddressUnicast))
        return cAtTrue;

    return HelperAddressIsEnabled(self, mask);
    }

static eAtRet IpV4AddressEnable(ThaPtpClaController self, uint8 ipAddrType, uint32 entryIndex, eBool enable)
    {
    return HelperIpAddressEnable(self, ipAddrType, entryIndex, enable, IpV4EnableMask(self, ipAddrType));
    }

static eBool IpV4AddressIsEnabled(ThaPtpClaController self, uint8 ipAddrType, uint32 entryIndex)
    {
    return HelperIpAddressIsEnabled(self, ipAddrType, entryIndex, IpV4EnableMask(self, ipAddrType));
    }

static eAtRet IpV4AddressSet(ThaPtpClaController self, uint8 ipAddrType, uint32 entryIndex, const uint8 *address)
    {
    return ThaPtpClaControllerAddressSet(self, IpV4AddressRegister(self, ipAddrType, entryIndex), address, cAtIpv4AddressLen);
    }

static eAtRet IpV4AddressGet(ThaPtpClaController self, uint8 ipAddrType, uint32 entryIndex, uint8 *address)
    {
    return ThaPtpClaControllerAddressGet(self, IpV4AddressRegister(self, ipAddrType, entryIndex), address, cAtIpv4AddressLen);
    }

static eAtRet MaxNumIpV4AddressesGet(ThaPtpClaController self, uint8 ipAddrType)
    {
    AtUnused(self);
    if ((ipAddrType == cAtPtpIpDestAddressUnicast) ||
        (ipAddrType == cAtPtpIpSourceAddressUnicast))
        return 4;
    return 0;
    }

static uint32 IpV6EnableMask(ThaPtpClaController self, uint8 addrType)
    {
    AtUnused(self);
    if (addrType == cAtPtpMacDestAddressAnycast)
        return cAf6_upen_cfg_ptp_bypass_cfg_ptp_bypass_ipv6da_Mask;

    if (addrType == cAtPtpMacSourceAddressAnycast)
        return cAf6_upen_cfg_ptp_bypass_cfg_ptp_bypass_ipv6sa_Mask;

    return 0;
    }

static eAtRet IpV6AddressEnable(ThaPtpClaController self, uint8 ipAddrType, uint32 entryIndex, eBool enable)
    {
    return HelperIpAddressEnable(self, ipAddrType, entryIndex, enable, IpV6EnableMask(self, ipAddrType));
    }

static eBool IpV6AddressIsEnabled(ThaPtpClaController self, uint8 ipAddrType, uint32 entryIndex)
    {
    return HelperIpAddressIsEnabled(self, ipAddrType, entryIndex, IpV6EnableMask(self, ipAddrType));
    }

static uint32 IpV6AddressRegister(ThaPtpClaController self, uint8 ipAddrType, uint32 entryIndex)
    {
    AtUnused(self);
    if (entryIndex > 3)
        return cInvalidUint32;

    switch (ipAddrType)
        {
        case cAtPtpIpDestAddressUnicast:
            return cAf6Reg_upen_cfg_ip6_da1 + entryIndex;

        case cAtPtpIpSourceAddressUnicast:
            return cAf6Reg_upen_cfg_ip6_sa1 + entryIndex;

        default:
            return cInvalidUint32;
        }
    }

static eAtRet IpV6AddressSet(ThaPtpClaController self, uint8 ipAddrType, uint32 entryIndex, const uint8 *address)
    {
    return ThaPtpClaControllerAddressSet(self, IpV6AddressRegister(self, ipAddrType, entryIndex), address, cAtIpv6AddressLen);
    }

static eAtRet IpV6AddressGet(ThaPtpClaController self, uint8 ipAddrType, uint32 entryIndex, uint8 *address)
    {
    return ThaPtpClaControllerAddressGet(self, IpV6AddressRegister(self, ipAddrType, entryIndex), address, cAtIpv6AddressLen);
    }

static eAtRet MaxNumIpV6AddressesGet(ThaPtpClaController self, uint8 ipAddrType)
    {
    AtUnused(self);
    if ((ipAddrType == cAtPtpIpDestAddressUnicast) ||
        (ipAddrType == cAtPtpIpSourceAddressUnicast))
        return 4;
    return 0;
    }

static const char *ToString(AtObject self)
    {
    AtUnused(self);
    return "ptp_backplane_cla_controller";
    }

static void OverrideAtObject(ThaPtpClaController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(tAtObjectMethods));

        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideThaPtpClaController(ThaPtpClaController self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPtpClaControllerOverride, mMethodsGet(self), sizeof(m_ThaPtpClaControllerOverride));

        /* Setup methods */
        mMethodOverride(m_ThaPtpClaControllerOverride, ExpectedCVlanSet);
        mMethodOverride(m_ThaPtpClaControllerOverride, ExpectedCVlanGet);
        mMethodOverride(m_ThaPtpClaControllerOverride, ExpectedSVlanSet);
        mMethodOverride(m_ThaPtpClaControllerOverride, ExpectedSVlanGet);
        mMethodOverride(m_ThaPtpClaControllerOverride, MacAddressSet);
        mMethodOverride(m_ThaPtpClaControllerOverride, MacAddressGet);
        mMethodOverride(m_ThaPtpClaControllerOverride, MacAddressEnable);
        mMethodOverride(m_ThaPtpClaControllerOverride, MacAddressIsEnabled);
        mMethodOverride(m_ThaPtpClaControllerOverride, MaxNumIpV4AddressesGet);
        mMethodOverride(m_ThaPtpClaControllerOverride, IpV4AddressSet);
        mMethodOverride(m_ThaPtpClaControllerOverride, IpV4AddressGet);
        mMethodOverride(m_ThaPtpClaControllerOverride, IpV4AddressEnable);
        mMethodOverride(m_ThaPtpClaControllerOverride, IpV4AddressIsEnabled);
        mMethodOverride(m_ThaPtpClaControllerOverride, MaxNumIpV6AddressesGet);
        mMethodOverride(m_ThaPtpClaControllerOverride, IpV6AddressSet);
        mMethodOverride(m_ThaPtpClaControllerOverride, IpV6AddressGet);
        mMethodOverride(m_ThaPtpClaControllerOverride, IpV6AddressEnable);
        mMethodOverride(m_ThaPtpClaControllerOverride, IpV6AddressIsEnabled);
        }

    mMethodsSet(self, &m_ThaPtpClaControllerOverride);
    }

static void Override(ThaPtpClaController self)
    {
    OverrideAtObject(self);
    OverrideThaPtpClaController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPtpBackplaneClaController);
    }

static ThaPtpClaController ObjectInit(ThaPtpClaController self, AtPtpPort port)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPtpClaControllerObjectInit(self, port) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPtpClaController ThaPtpBackplaneClaControllerNew(AtPtpPort port)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPtpClaController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    return ObjectInit(newController, port);
    }

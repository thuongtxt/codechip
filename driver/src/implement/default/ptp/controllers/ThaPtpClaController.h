/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PTP
 * 
 * File        : ThaPtpClaController.h
 * 
 * Created Date: Jul 6, 2018
 *
 * Description : Default PTP CLA controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPTPCLACONTROLLER_H_
#define _THAPTPCLACONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPtpClaController *ThaPtpClaController;

typedef enum eAtPtpMacAddressType
    {
    cAtPtpMacDestAddressUnicast,
    cAtPtpMacDestAddressMulticast,
    cAtPtpMacDestAddressAnycast,
    cAtPtpMacSourceAddressUnicast,
    cAtPtpMacSourceAddressAnycast
    }eAtPtpMacAddressType;

typedef enum eAtPtpIpAddressType
    {
    /* Per channel address */
    cAtPtpIpDestAddressUnicast,
    cAtPtpIpDestAddressMulticast,
    cAtPtpIpDestAddressAnycast,
    cAtPtpIpSourceAddressUnicast,
    cAtPtpIpSourceAddressAnycast,

    /* Global address */
    cAtPtpIpGlobalAddressMulticast
    }eAtPtpIpAddressType;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPtpPort ThaPtpClaControllerPortGet(ThaPtpClaController self);

uint32 ThaPtpClaControllerRead(ThaPtpClaController self, uint32 address);
void ThaPtpClaControllerWrite(ThaPtpClaController self, uint32 address, uint32 value);
uint16 ThaPtpClaControllerLongRead(ThaPtpClaController self, uint32 address, uint32 *dataBuffer, uint16 bufferLen);
uint16 ThaPtpClaControllerLongWrite(ThaPtpClaController self, uint32 address, uint32 *dataBuffer, uint16 bufferLen);

/* Address access */
eAtRet ThaPtpClaControllerAddressSet(ThaPtpClaController self, uint32 localAddress, const uint8 *address, uint8 addressLen);
eAtRet ThaPtpClaControllerAddressGet(ThaPtpClaController self, uint32 localAddress, uint8 *address, uint8 addressLen);

/* VLAN */
eAtModulePtpRet ThaPtpClaControllerExpectedCVlanSet(ThaPtpClaController self, tAtEthVlanTag *cTag);
tAtEthVlanTag* ThaPtpClaControllerExpectedCVlanGet(ThaPtpClaController self, tAtEthVlanTag *cTag);
eAtModulePtpRet ThaPtpClaControllerExpectedSVlanSet(ThaPtpClaController self, tAtEthVlanTag *sTag);
tAtEthVlanTag* ThaPtpClaControllerExpectedSVlanGet(ThaPtpClaController self, tAtEthVlanTag *sTag);

/* MAC address */
eAtRet ThaPtpClaControllerMacAddressSet(ThaPtpClaController self, uint8 addrType, const uint8 *address);
eAtRet ThaPtpClaControllerMacAddressGet(ThaPtpClaController self, uint8 addrType, uint8 *address);
eAtRet ThaPtpClaControllerMacAddressEnable(ThaPtpClaController self, uint8 addrType, eBool enable);
eBool ThaPtpClaControllerMacAddressIsEnabled(ThaPtpClaController self, uint8 addrType);

/* IPv4 */
uint32 ThaPtpClaControllerMaxNumIpV4AddressesGet(ThaPtpClaController self, uint8 ipAddrType);
eAtRet ThaPtpClaControllerIpV4AddressSet(ThaPtpClaController self, uint8 addrType, uint32 entryIndex, const uint8 *address);
eAtRet ThaPtpClaControllerIpV4AddressGet(ThaPtpClaController self, uint8 addrType, uint32 entryIndex, uint8 *address);
eAtRet ThaPtpClaControllerIpV4AddressEnable(ThaPtpClaController self, uint8 addrType, uint32 entryIndex, eBool enable);
eBool ThaPtpClaControllerIpV4AddressIsEnabled(ThaPtpClaController self, uint8 addrType, uint32 entryIndex);

/* IPv6 */
uint32 ThaPtpClaControllerMaxNumIpV6AddressesGet(ThaPtpClaController self, uint8 ipAddrType);
eAtRet ThaPtpClaControllerIpV6AddressSet(ThaPtpClaController self, uint8 addrType, uint32 entryIndex, const uint8 *address);
eAtRet ThaPtpClaControllerIpV6AddressGet(ThaPtpClaController self, uint8 addrType, uint32 entryIndex, uint8 *address);
eAtRet ThaPtpClaControllerIpV6AddressEnable(ThaPtpClaController self, uint8 addrType, uint32 entryIndex, eBool enable);
eBool ThaPtpClaControllerIpV6AddressIsEnabled(ThaPtpClaController self, uint8 addrType, uint32 entryIndex);

/* Concrete classes */
ThaPtpClaController ThaPtpClaControllerNew(AtPtpPort port);
ThaPtpClaController ThaPtpBackplaneClaControllerNew(AtPtpPort port);

#ifdef __cplusplus
}
#endif
#endif /* _THAPTPCLACONTROLLER_H_ */


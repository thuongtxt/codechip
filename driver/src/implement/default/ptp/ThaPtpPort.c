/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PTP
 *
 * File        : ThaPtpPort.c
 *
 * Created Date: Jul 6, 2018
 *
 * Description : Default PTP port implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPtpPsnGroup.h"
#include "../../../util/coder/AtCoderUtil.h"
#include "ThaModulePtp.h"
#include "ThaPtpPortInternal.h"
#include "ThaModulePtpReg.h"
#include "psn/ThaPtpPsn.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)         ((ThaPtpPort)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaPtpPortMethods m_methods;

/* Override */
static tAtObjectMethods  m_AtObjectOverride;
static tAtChannelMethods m_AtChannelOverride;
static tAtPtpPortMethods m_AtPtpPortOverride;

/* Save super implementations */
static const tAtObjectMethods   *m_AtObjectMethods  = NULL;
static const tAtChannelMethods  *m_AtChannelMethods = NULL;
static const tAtPtpPortMethods  *m_AtPtpPortMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModulePtp PtpModule(AtPtpPort self)
    {
    return (AtModulePtp)AtChannelModuleGet((AtChannel)self);
    }

static uint32 NumPortsPerPart(void)
    {
    return 16U;
    }

static uint32 FlatLocalId(AtPtpPort self)
    {
    return AtChannelIdGet((AtChannel)self) - 1U;
    }

static uint8 PartId(AtPtpPort self)
    {
    return (uint8)(FlatLocalId(self) / NumPortsPerPart());
    }

static uint32 BaseAddress(AtPtpPort self)
    {
    ThaModulePtp ptpModule = (ThaModulePtp)PtpModule(self);
    return ThaModulePtpBaseAddress(ptpModule);
    }

static uint32 PartBaseAddress(AtPtpPort self)
    {
    ThaModulePtp ptpModule = (ThaModulePtp)PtpModule(self);
    return ThaModulePtpBaseAddress(ptpModule) +
           ThaModulePtpPartOffset(ptpModule, PartId(self));
    }

static uint8 PortShift(AtPtpPort self)
    {
    return (uint8)AtChannelHwIdGet((AtChannel)self);
    }

static uint32 PortMask(AtPtpPort self)
    {
    return cBit0 << PortShift(self);
    }

static eBool IsTransparentClock(AtPtpPort self)
    {
    return (AtModulePtpDeviceTypeGet(PtpModule(self)) == cAtPtpDeviceTypeTransparent) ? cAtTrue : cAtFalse;
    }

static eAtRet FaceplateHwStateSet(AtPtpPort self, eAtPtpPortState state)
    {
    uint32 regAddr = PartBaseAddress(self) + cAf6Reg_upen_ptp_ms;
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePtp);
    uint32 hwBitVal = (state == cAtPtpPortStateMaster) ? 1 : 0;

    mFieldIns(&regVal, PortMask(self), PortShift(self), hwBitVal);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePtp);

    return cAtOk;
    }

static eAtRet BackplaneHwStateSet(AtPtpPort self, eAtPtpPortState state)
    {
    uint32 regAddr = BaseAddress(self) + cAf6Reg_upen_ptp40_ms;
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePtp);
    uint32 hwBitVal = (state == cAtPtpPortStateMaster) ? 1 : 0;
    uint32 channelShift = FlatLocalId(self);
    uint32 channelMask = cBit0 << channelShift;

    mFieldIns(&regVal, channelMask, channelShift, hwBitVal);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePtp);
    return cAtOk;
    }

static eAtRet HwStateSet(AtPtpPort self, eAtPtpPortState state)
    {
    eAtRet ret;

    if (state == cAtPtpPortStatePassive)
        return cAtErrorModeNotSupport;

    ret  = FaceplateHwStateSet(self, state);
    ret |= BackplaneHwStateSet(self, state);
    return ret;
    }

static eAtModulePtpRet StateSet(AtPtpPort self, eAtPtpPortState state)
    {
    if (!IsTransparentClock(self))
        return HwStateSet(self, state);

    return (state == cAtPtpPortStatePassive) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtPtpPortState StateGet(AtPtpPort self)
    {
    uint32 regAddr, regVal;

    if (IsTransparentClock(self))
        return cAtPtpPortStatePassive;

    regAddr = PartBaseAddress(self) + cAf6Reg_upen_ptp_ms;
    regVal = mChannelHwRead(self, regAddr, cAtModulePtp);
    return (regVal & PortMask(self)) ? cAtPtpPortStateMaster : cAtPtpPortStateSlave;
    }

static eAtModulePtpRet FaceplateStepModeSet(AtPtpPort self, eAtPtpStepMode stepMode)
    {
    uint32 regAddr = PartBaseAddress(self) + cAf6Reg_upen_ptp_stmode;
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePtp);
    uint32 hwBitVal = (stepMode == cAtPtpStepModeTwoStep) ? 1 : 0;
    mFieldIns(&regVal, PortMask(self), PortShift(self), hwBitVal);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePtp);

    return cAtOk;
    }

static uint32 BackplaneStepModeRegister(AtPtpPort self)
    {
    ThaModulePtp ptpModule = (ThaModulePtp)PtpModule(self);
    return ThaModulePtpBaseAddress(ptpModule) + cAf6Reg_upen_ptp40_stmode;
    }

static eAtModulePtpRet BackplaneStepModeSet(AtPtpPort self, eAtPtpStepMode stepMode)
    {
    uint32 regAddr = BackplaneStepModeRegister(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePtp);
    uint32 hwBitVal = (stepMode == cAtPtpStepModeTwoStep) ? 1 : 0;
    uint32 channelShift = FlatLocalId(self);
    uint32 channelMask = cBit0 << channelShift;

    mFieldIns(&regVal, channelMask, channelShift, hwBitVal);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePtp);

    return cAtOk;
    }

static eAtModulePtpRet StepModeSet(AtPtpPort self, eAtPtpStepMode stepMode)
    {
    eAtRet ret;

    if (stepMode == cAtPtpStepModeUnknown)
        return cAtErrorModeNotSupport;

    ret  = FaceplateStepModeSet(self, stepMode);
    ret |= BackplaneStepModeSet(self, stepMode);
    return ret;
    }

static eAtPtpStepMode StepModeGet(AtPtpPort self)
    {
    uint32 regAddr = PartBaseAddress(self) + cAf6Reg_upen_ptp_stmode;
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePtp);
    return (regVal & PortMask(self)) ? cAtPtpStepModeTwoStep : cAtPtpStepModeOneStep;
    }

static uint8 TransportTypeSw2Hw(uint8 type)
    {
    switch (type)
        {
        case cAtPtpTransportTypeL2TP:     return 0;
        case cAtPtpTransportTypeIpV4:     return 1;
        case cAtPtpTransportTypeIpV6:     return 2;
        case cAtPtpTransportTypeIpV4Vpn:  return 3;
        case cAtPtpTransportTypeIpV6Vpn:  return 4;
        default:                          return cInvalidUint8;
        }
    }

static eAtPtpTransportType TransportTypeHw2Sw(uint8 type)
    {
    switch (type)
        {
        case 0:     return cAtPtpTransportTypeL2TP;
        case 1:     return cAtPtpTransportTypeIpV4;
        case 2:     return cAtPtpTransportTypeIpV6;
        case 3:     return cAtPtpTransportTypeIpV4Vpn;
        case 4:     return cAtPtpTransportTypeIpV6Vpn;
        default:    return cAtPtpTransportTypeUnknown;
        }
    }

static uint32 DefaultOffset(AtPtpPort self)
    {
    return ThaModulePtpPartOffset((ThaModulePtp)PtpModule(self), PartId(self)) +
           AtChannelHwIdGet((AtChannel)self);
    }

static eAtModulePtpRet TransportTypeSet(AtPtpPort self, eAtPtpTransportType type)
    {
    uint32 regAddr = BaseAddress(self) + cAf6Reg_upen_ptl_Base + DefaultOffset(self);
    uint32 regVal;
    uint8 hwPsnType = TransportTypeSw2Hw(type);

    if (hwPsnType == cInvalidUint8)
        return cAtErrorModeNotSupport;

    regVal = mChannelHwRead(self, regAddr, cAtModulePtp);
    mRegFieldSet(regVal, cAf6_upen_ptl_cfg_ptl_, hwPsnType);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePtp);

    /* Call super to setup PSN */
    return m_AtPtpPortMethods->TransportTypeSet(self, type);
    }

static eAtPtpTransportType TransportTypeGet(AtPtpPort self)
    {
    uint32 regAddr = BaseAddress(self) + cAf6Reg_upen_ptl_Base + DefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePtp);
    uint8 hwValue = (uint8)mRegField(regVal, cAf6_upen_ptl_cfg_ptl_);
    return TransportTypeHw2Sw(hwValue);
    }

static AtPtpPsn L2PsnObjectCreate(AtPtpPort self)
    {
    return (AtPtpPsn)ThaPtpL2PsnNew(self);
    }

static AtPtpPsn IpV4PsnObjectCreate(AtPtpPort self)
    {
    return (AtPtpPsn)ThaPtpIpV4PsnNew(self);
    }

static AtPtpPsn IpV6PsnObjectCreate(AtPtpPort self)
    {
    return (AtPtpPsn)ThaPtpIpV6PsnNew(self);
    }

static AtEthPort EthPortGet(AtPtpPort self)
    {
    return ThaModulePtpPhysicalEthPortGet((ThaModulePtp)PtpModule(self), self);
    }

static uint32 AddressIndex(AtPtpPsnGroup group)
    {
    return AtPtpPsnGroupIdGet(group);
    }

static eAtRet PsnGroupHandle(AtPtpPort self, AtPtpPsnGroup group, eBool enable)
    {
    uint8 psnType = AtPtpPsnGroupTypeGet(group);

    /* Check if this port can support the PSN type */
    if (AtPtpPortPsnGet(self, psnType) == NULL)
        return cAtErrorModeNotSupport;

    if (!ThaModulePtpPortCanJoinPsnGroup((ThaModulePtp)PtpModule(self), self, group))
        return cAtErrorModeNotSupport;

    switch (psnType)
        {
        case cAtPtpPsnTypeLayer2:
            return (enable) ? cAtOk : cAtErrorModeNotSupport;

        case cAtPtpPsnTypeIpV4:
            return ThaPtpClaControllerIpV4AddressEnable(ThaPtpPortClaControllerGet(mThis(self)),
                                                        cAtPtpIpGlobalAddressMulticast,
                                                        AddressIndex(group), enable);
        case cAtPtpPsnTypeIpV6:
            return ThaPtpClaControllerIpV6AddressEnable(ThaPtpPortClaControllerGet(mThis(self)),
                                                        cAtPtpIpGlobalAddressMulticast,
                                                        AddressIndex(group), enable);
        default:
            return cAtErrorModeNotSupport;
        }
    }

static eAtRet PsnGroupJoin(AtPtpPort self, AtPtpPsnGroup group)
    {
    return PsnGroupHandle(self, group, cAtTrue);
    }

static eAtRet PsnGroupLeave(AtPtpPort self, AtPtpPsnGroup group)
    {
    return PsnGroupHandle(self, group, cAtFalse);
    }

static eAtRet Enable(AtChannel self, eBool enable)
    {
    uint32 regAddr = PartBaseAddress((AtPtpPort)self) + cAf6Reg_upen_ptp_en;
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePtp);
    mFieldIns(&regVal, PortMask((AtPtpPort)self), PortShift((AtPtpPort)self), mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cAtModulePtp);

    return cAtOk;
    }

static eBool IsEnabled(AtChannel self)
    {
    uint32 regAddr = PartBaseAddress((AtPtpPort)self) + cAf6Reg_upen_ptp_en;
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePtp);
    return (regVal & PortMask((AtPtpPort)self)) ? cAtTrue : cAtFalse;
    }

static eAtModulePtpRet TxDelayAdjustSet(AtPtpPort self, uint32 nanoSeconds)
    {
    uint32 regAddr = BaseAddress(self) + cAf6Reg_upen_face_latency_mode_Base + DefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePtp);

    mRegFieldSet(regVal, cAf6_upen_face_latency_mode_tx_latency_adj_face_, nanoSeconds);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePtp);
    return cAtOk;
    }

static uint32 TxDelayAdjustGet(AtPtpPort self)
    {
    uint32 regAddr = BaseAddress(self) + cAf6Reg_upen_face_latency_mode_Base + DefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePtp);
    uint32 nanoSeconds = mRegField(regVal, cAf6_upen_face_latency_mode_tx_latency_adj_face_);

    return nanoSeconds;
    }

static eAtModulePtpRet RxDelayAdjustSet(AtPtpPort self, uint32 nanoSeconds)
    {
    uint32 regAddr = BaseAddress(self) + cAf6Reg_upen_face_latency_mode_Base + DefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePtp);

    mRegFieldSet(regVal, cAf6_upen_face_latency_mode_rx_latency_adj_face_, nanoSeconds);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePtp);
    return cAtOk;
    }

static uint32 RxDelayAdjustGet(AtPtpPort self)
    {
    uint32 regAddr = BaseAddress(self) + cAf6Reg_upen_face_latency_mode_Base + DefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePtp);
    uint32 nanoSeconds = mRegField(regVal, cAf6_upen_face_latency_mode_rx_latency_adj_face_);

    return nanoSeconds;
    }

static uint32 CounterRegister(ThaPtpPort self, uint16 counterType, eBool r2c)
    {
    AtUnused(self);
    switch (counterType)
        {
        case cAtPtpPortCounterTypeTxPackets:
            return (r2c) ? cAf6Reg_upen_tx_ptp_cnt_r2c_Base : cAf6Reg_upen_tx_ptp_cnt_ro_Base;
        case cAtPtpPortCounterTypeTxSyncPackets:
            return (r2c) ? cAf6Reg_upen_tx_sync_cnt_r2c_Base : cAf6Reg_upen_tx_sync_cnt_ro_Base;
        case cAtPtpPortCounterTypeTxFollowUpPackets:
            return (r2c) ? cAf6Reg_upen_tx_folu_cnt_r2c_Base : cAf6Reg_upen_tx_folu_cnt_ro_Base;
        case cAtPtpPortCounterTypeTxDelayReqPackets:
            return (r2c) ? cAf6Reg_upen_tx_dreq_cnt_r2c_Base : cAf6Reg_upen_tx_dreq_cnt_ro_Base;
        case cAtPtpPortCounterTypeTxDelayRespPackets:
            return (r2c) ? cAf6Reg_upen_tx_dres_cnt_r2c_Base : cAf6Reg_upen_tx_dres_cnt_ro_Base;

        case cAtPtpPortCounterTypeRxPackets:
            return (r2c) ? cAf6Reg_upen_rx_ptp_cnt_r2c_Base : cAf6Reg_upen_rx_ptp_cnt_ro_Base;
        case cAtPtpPortCounterTypeRxSyncPackets:
            return (r2c) ? cAf6Reg_upen_rx_sync_cnt_r2c_Base : cAf6Reg_upen_rx_sync_cnt_ro_Base;
        case cAtPtpPortCounterTypeRxFollowUpPackets:
            return (r2c) ? cAf6Reg_upen_rx_folu_cnt_r2c_Base : cAf6Reg_upen_rx_folu_cnt_ro_Base;
        case cAtPtpPortCounterTypeRxDelayReqPackets:
            return (r2c) ? cAf6Reg_upen_rx_dreq_cnt_r2c_Base : cAf6Reg_upen_rx_dreq_cnt_ro_Base;
        case cAtPtpPortCounterTypeRxDelayRespPackets:
            return (r2c) ? cAf6Reg_upen_rx_dres_cnt_r2c_Base : cAf6Reg_upen_rx_dres_cnt_ro_Base;
        case cAtPtpPortCounterTypeRxErroredPackets:
            return cInvalidUint32;

        default:
            return cInvalidUint32;
        }
    }

static uint32 CounterRead2Clear(ThaPtpPort self, uint16 counterType, eBool r2c)
    {
    uint32 baseAddress = BaseAddress((AtPtpPort)self);
    uint32 regAddr = CounterRegister(self, counterType, r2c);
    uint32 offset = DefaultOffset((AtPtpPort)self);

    if (regAddr == cInvalidUint32)
        return 0;

    return mChannelHwRead(self, baseAddress + regAddr + offset, cAtModulePtp);
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    return mMethodsGet(mThis(self))->CounterRead2Clear(mThis(self), counterType, cAtFalse);
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    return mMethodsGet(mThis(self))->CounterRead2Clear(mThis(self), counterType, cAtTrue);
    }

static ThaPtpClaController ClaControllerGet(ThaPtpPort self)
    {
    if (self->controller)
        return self->controller;

    self->controller = ThaModulePtpClaControllerCreate((ThaModulePtp)PtpModule((AtPtpPort)self), (AtPtpPort)self);
    return self->controller;
    }

static eBool  ExpectedVlanIsSupported(ThaPtpPort self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet ExpectedVlanDefaultSet(ThaPtpPort self)
    {
    eAtRet ret;
    AtPtpPsn psn;

    if (!mMethodsGet(self)->ExpectedVlanIsSupported(self))
        return cAtOk;

    psn = AtPtpPortPsnGet((AtPtpPort)self, cAtPtpPsnTypeLayer2);
    if (psn == NULL)
        return cAtOk;

    ret  = AtPtpPsnExpectedCVlanSet(psn, NULL);
    ret |= AtPtpPsnExpectedSVlanSet(psn, NULL);
    return ret;
    }

static uint32 TxDelayAdjustDefaultVlue(ThaPtpPort self, eAtSerdesMode serdesMode)
    {
    AtUnused(self);
    if (serdesMode == cAtSerdesModeEth10G)
        return 0x82;
    if (serdesMode == cAtSerdesModeEth1G)
        return 0x198;
    if (serdesMode == cAtSerdesModeEth100M)
        return 0x490;
    return 0;
    }

static uint32 RxDelayAdjustDefaultVlue(ThaPtpPort self, eAtSerdesMode serdesMode)
    {
    AtUnused(self);
    if (serdesMode == cAtSerdesModeEth10G)
        return 0x82;
    if (serdesMode == cAtSerdesModeEth1G)
        return 0x138;
    if (serdesMode == cAtSerdesModeEth100M)
        return 0x581;
    return 0;
    }

static eAtRet HwIdGet(AtChannel self)
    {
    return ThaModulePtpPortLocalId((ThaModulePtp)PtpModule((AtPtpPort)self), (AtPtpPort)self);
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return ExpectedVlanDefaultSet(mThis(self));
    }

static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject)(mThis(self)->controller));
    mThis(self)->controller = NULL;

    /* Call super to fully delete itself */
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaPtpPort object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObject(controller);
    }

static eBool TransportTypeIsSupported(AtPtpPort self, eAtPtpTransportType type)
    {
    AtUnused(self);
    switch (type)
        {
        case cAtPtpTransportTypeL2TP:
        case cAtPtpTransportTypeIpV4:
        case cAtPtpTransportTypeIpV6:
        case cAtPtpTransportTypeIpV4Vpn:
        case cAtPtpTransportTypeIpV6Vpn:
            return cAtTrue;

        case cAtPtpTransportTypeUnknown:
        case cAtPtpTransportTypeAny:
        default:
            return cAtFalse;
        }
    }

static void OverrideAtObject(AtPtpPort self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtPtpPort self)
    {
    AtChannel channel = (AtChannel)self;
    AtOsal osal = AtSharedDriverOsalGet();

    if (!m_methodsInit)
        {
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, IsEnabled);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, HwIdGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtPtpPort(AtPtpPort self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPtpPortMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPtpPortOverride, mMethodsGet(self), sizeof(m_AtPtpPortOverride));

        /* Setup methods */
        mMethodOverride(m_AtPtpPortOverride, StateSet);
        mMethodOverride(m_AtPtpPortOverride, StateGet);
        mMethodOverride(m_AtPtpPortOverride, StepModeSet);
        mMethodOverride(m_AtPtpPortOverride, StepModeGet);
        mMethodOverride(m_AtPtpPortOverride, TransportTypeSet);
        mMethodOverride(m_AtPtpPortOverride, TransportTypeGet);
        mMethodOverride(m_AtPtpPortOverride, L2PsnObjectCreate);
        mMethodOverride(m_AtPtpPortOverride, IpV4PsnObjectCreate);
        mMethodOverride(m_AtPtpPortOverride, IpV6PsnObjectCreate);
        mMethodOverride(m_AtPtpPortOverride, EthPortGet);
        mMethodOverride(m_AtPtpPortOverride, PsnGroupJoin);
        mMethodOverride(m_AtPtpPortOverride, PsnGroupLeave);
        mMethodOverride(m_AtPtpPortOverride, TxDelayAdjustSet);
        mMethodOverride(m_AtPtpPortOverride, TxDelayAdjustGet);
        mMethodOverride(m_AtPtpPortOverride, RxDelayAdjustSet);
        mMethodOverride(m_AtPtpPortOverride, RxDelayAdjustGet);
        mMethodOverride(m_AtPtpPortOverride, TransportTypeIsSupported);
        }

    mMethodsSet(self, &m_AtPtpPortOverride);
    }

static void Override(AtPtpPort self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtPtpPort(self);
    }

static void MethodsInit(ThaPtpPort self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, CounterRead2Clear);
        mMethodOverride(m_methods, ExpectedVlanIsSupported);
        mMethodOverride(m_methods, TxDelayAdjustDefaultVlue);
        mMethodOverride(m_methods, RxDelayAdjustDefaultVlue);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPtpPort);
    }

AtPtpPort ThaPtpPortObjectInit(AtPtpPort self, uint32 channelId, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPtpPortObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(mThis(self));
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPtpPort ThaPtpPortNew(uint32 channelId, AtModule module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPtpPort newPort = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPort == NULL)
        return NULL;

    return ThaPtpPortObjectInit(newPort, channelId, module);
    }

ThaPtpClaController ThaPtpPortClaControllerGet(ThaPtpPort self)
    {
    if (self)
        return ClaControllerGet(self);
    return NULL;
    }

eAtRet ThaPtpPortDelayAdjustDefaultSet(ThaPtpPort self, eAtSerdesMode serdesMode)
    {
    eAtRet ret = cAtOk;
    ret = AtPtpPortTxDelayAdjustSet((AtPtpPort)self, mMethodsGet(self)->TxDelayAdjustDefaultVlue(self, serdesMode));
    ret |= AtPtpPortRxDelayAdjustSet((AtPtpPort)self, mMethodsGet(self)->RxDelayAdjustDefaultVlue(self, serdesMode));
    return ret;
    }

uint32 ThaPtpPortDefaultOffset(ThaPtpPort self)
    {
    if (self)
        return DefaultOffset((AtPtpPort)self);
    return cInvalidUint32;
    }


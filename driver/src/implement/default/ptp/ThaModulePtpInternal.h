/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PTP
 * 
 * File        : ThaModulePtpInternal.h
 * 
 * Created Date: Jul 5, 2018
 *
 * Description : Default module PTP representation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEPTPINTERNAL_H_
#define _THAMODULEPTPINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/ptp/AtModulePtpInternal.h"
#include "ThaModulePtp.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModulePtpMethods
    {
    uint32 (*BaseAddress)(ThaModulePtp self);
    uint8 (*NumParts)(ThaModulePtp self); /* To abstract module instantiation */
    void (*T1T3TimestampContentParser)(ThaModulePtp self, uint32 *buffer, tAtPtpT1T3TimestampContent *content);

    /* Port methods */
    AtEthPort (*PhysicalEthPortGet)(ThaModulePtp self, AtPtpPort port);
    AtPtpPort (*EgressPortIdToPtpPort)(ThaModulePtp self, uint32 egressPortId);
    eBool (*PortCanJoinPsnGroup)(ThaModulePtp self, AtPtpPort port, AtPtpPsnGroup group);
    uint32 (*PortLocalId)(ThaModulePtp self, AtPtpPort port);

    /* Long register access */
    uint32 *(*BackplaneHoldRegistersGet)(ThaModulePtp self, uint16 *numberOfHoldRegisters);

    /* External physical device */
    eAtRet (*PhysicalBackplaneDeviceTypeSet)(ThaModulePtp self, eAtPtpDeviceType deviceType);
    }tThaModulePtpMethods;

typedef struct tThaModulePtp
    {
    tAtModulePtp super;
    const tThaModulePtpMethods *methods;

    /* Private data */
    AtLongRegisterAccess backplanePortLongRegisterAccess;
    eBool interruptIsEnabled;
    }tThaModulePtp;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePtp ThaModulePtpObjectInit(AtModulePtp self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEPTPINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PTP
 *
 * File        : ThaPtpBackplanePort.c
 *
 * Created Date: Jul 6, 2018
 *
 * Description : Default PTP Backplane port implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../util/coder/AtCoderUtil.h"
#include "ThaModulePtp.h"
#include "ThaPtpPortInternal.h"
#include "ThaModulePtpReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)         ((tThaPtpBackplanePort*)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtChannelMethods    m_AtChannelOverride;
static tAtPtpPortMethods    m_AtPtpPortOverride;
static tThaPtpPortMethods   m_ThaPtpPortOverride;

/* Save super implementations */
static const tAtObjectMethods   *m_AtObjectMethods  = NULL;
static const tAtChannelMethods  *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModulePtp PtpModule(AtPtpPort self)
    {
    return (AtModulePtp)AtChannelModuleGet((AtChannel)self);
    }

static uint32 BaseAddress(AtPtpPort self)
    {
    return ThaModulePtpBaseAddress((ThaModulePtp)PtpModule(self));
    }

static eAtModulePtpRet StateSet(AtPtpPort self, eAtPtpPortState state)
    {
    AtUnused(self);
    AtUnused(state);
    return cAtErrorNotApplicable;
    }

static eAtPtpPortState StateGet(AtPtpPort self)
    {
    AtUnused(self);
    return cAtPtpPortStateUnknown;
    }

static eAtModulePtpRet StepModeSet(AtPtpPort self, eAtPtpStepMode stepMode)
    {
    AtUnused(self);
    AtUnused(stepMode);
    return cAtErrorNotApplicable;
    }

static eAtPtpStepMode StepModeGet(AtPtpPort self)
    {
    AtUnused(self);
    return cAtPtpStepModeUnknown;
    }

static eAtModulePtpRet HwEnable(AtPtpPort self, eBool enable)
    {
    uint32 regAddr = BaseAddress(self) + cAf6Reg_upen_ptp40_enable;
    uint32 hwVal = (uint32)(enable ? 1 : 0);
    mChannelHwWrite(self, regAddr, hwVal, cAtModulePtp);

    return cAtOk;
    }

static eBool HwIsEnabled(AtPtpPort self)
    {
    uint32 regAddr = BaseAddress(self) + cAf6Reg_upen_ptp40_enable;
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePtp);
    return (eBool)(regVal > 0 ? cAtTrue : cAtFalse);
    }

static eAtModulePtpRet TransportTypeSet(AtPtpPort self, eAtPtpTransportType type)
    {
    AtUnused(self);
    if (type == cAtPtpTransportTypeAny)
        return cAtOk;
    return cAtErrorModeNotSupport;
    }

static eAtPtpTransportType TransportTypeGet(AtPtpPort self)
    {
    AtUnused(self);
    return cAtPtpTransportTypeAny;
    }

static eBool CanEnable(AtChannel self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtTrue;
    }

static eAtRet Enable(AtChannel self, eBool enable)
    {
    return HwEnable((AtPtpPort)self, enable);
    }

static eBool IsEnabled(AtChannel self)
    {
    return HwIsEnabled((AtPtpPort)self);
    }

static eAtModulePtpRet TxDelayAdjustSet(AtPtpPort self, uint32 nanoSeconds)
    {
    uint32 regAddr = BaseAddress(self) + cAf6Reg_upen_bp_latency_mode_Base;
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePtp);

    mRegFieldSet(regVal, cAf6_upen_bp_latency_mode_tx_latency_adj_bp_, nanoSeconds);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePtp);

    regAddr = BaseAddress(self) + cAf6Reg_upen_bp_latency_mode_Base + 1;
    regVal = mChannelHwRead(self, regAddr, cAtModulePtp);
    mRegFieldSet(regVal, cAf6_upen_bp_latency_mode_tx_latency_adj_bp_, nanoSeconds);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePtp);
    return cAtOk;
    }

static uint32 TxDelayAdjustGet(AtPtpPort self)
    {
    uint32 regAddr = BaseAddress(self) + cAf6Reg_upen_bp_latency_mode_Base;
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePtp);
    uint32 nanoSeconds = mRegField(regVal, cAf6_upen_bp_latency_mode_tx_latency_adj_bp_);

    return nanoSeconds;
    }

static eAtModulePtpRet RxDelayAdjustSet(AtPtpPort self, uint32 nanoSeconds)
    {
    uint32 regAddr = BaseAddress(self) + cAf6Reg_upen_bp_latency_mode_Base;
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePtp);

    mRegFieldSet(regVal, cAf6_upen_bp_latency_mode_rx_latency_adj_bp_, nanoSeconds);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePtp);

    regAddr = BaseAddress(self) + cAf6Reg_upen_bp_latency_mode_Base + 1;
    regVal = mChannelHwRead(self, regAddr, cAtModulePtp);
    mRegFieldSet(regVal, cAf6_upen_bp_latency_mode_rx_latency_adj_bp_, nanoSeconds);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePtp);
    return cAtOk;
    }

static uint32 RxDelayAdjustGet(AtPtpPort self)
    {
    uint32 regAddr = BaseAddress(self) + cAf6Reg_upen_bp_latency_mode_Base;
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePtp);
    uint32 nanoSeconds = mRegField(regVal, cAf6_upen_bp_latency_mode_rx_latency_adj_bp_);

    return nanoSeconds;
    }

static uint32 TxDelayAdjustDefaultVlue(ThaPtpPort self, eAtSerdesMode serdesMode)
    {
    AtUnused(self);
    AtUnused(serdesMode);
    return 0x297;
    }

static uint32 RxDelayAdjustDefaultVlue(ThaPtpPort self, eAtSerdesMode serdesMode)
    {
    AtUnused(self);
    AtUnused(serdesMode);
    return 0xF2;
    }

static eBool  ExpectedVlanIsSupported(ThaPtpPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return ThaPtpPortDelayAdjustDefaultSet((ThaPtpPort)self, cAtSerdesModeEth40G);
    }

static const char *TypeString(AtChannel self)
    {
    AtUnused(self);
    return "backplane_ptp_port";
    }

static uint32 TxCounterRegister(uint32 localAddress, eBool r2c)
    {
    static const uint32 cReadOnlyOffset = 0x40U;
    if (!r2c)
        return cReadOnlyOffset + localAddress;
    return localAddress;
    }

static uint32 CounterRegister(ThaPtpPort self, uint16 counterType, eBool r2c)
    {
    AtUnused(self);
    switch (counterType)
        {
        case cAtPtpPortCounterTypeTxPackets:
            return (r2c) ? cAf6Reg_upen_cnt_txpkt_out_r2c : cAf6Reg_upen_cnt_txpkt_out_ro;
        case cAtPtpPortCounterTypeTxSyncPackets:
            return TxCounterRegister(cAf6Reg_upen_tx_sync_cnt40_Base, r2c);
        case cAtPtpPortCounterTypeTxFollowUpPackets:
            return TxCounterRegister(cAf6Reg_upen_tx_folu_cnt40_Base, r2c);
        case cAtPtpPortCounterTypeTxDelayReqPackets:
            return TxCounterRegister(cAf6Reg_upen_tx_dreq_cnt40_Base, r2c);
        case cAtPtpPortCounterTypeTxDelayRespPackets:
            return TxCounterRegister(cAf6Reg_upen_tx_dres_cnt40_Base, r2c);

        case cAtPtpPortCounterTypeRxPackets:
            return (r2c) ? cAf6Reg_upen_cnt_ptppkt_rx_r2c_Base : cAf6Reg_upen_cnt_ptppkt_rx_ro_Base;
        case cAtPtpPortCounterTypeRxSyncPackets:
            return (r2c) ? cAf6Reg_upen_rx_sync_cnt40_r2c_Base : cAf6Reg_upen_rx_sync_cnt40_ro_Base;
        case cAtPtpPortCounterTypeRxFollowUpPackets:
            return (r2c) ? cAf6Reg_upen_rx_folu_cnt40_r2c_Base : cAf6Reg_upen_rx_folu_cnt40_ro_Base;
        case cAtPtpPortCounterTypeRxDelayReqPackets:
            return (r2c) ? cAf6Reg_upen_rx_dreq_cnt40_r2c_Base : cAf6Reg_upen_rx_dreq_cnt40_ro_Base;
        case cAtPtpPortCounterTypeRxDelayRespPackets:
            return (r2c) ? cAf6Reg_upen_rx_dres_cnt40_r2c_Base : cAf6Reg_upen_rx_dres_cnt40_ro_Base;
        case cAtPtpPortCounterTypeRxErroredPackets:
            return cInvalidUint32;

        default:
            return cInvalidUint32;
        }
    }

static uint32 CounterRead2Clear(ThaPtpPort self, uint16 counterType, eBool r2c)
    {
    uint32 baseAddress = BaseAddress((AtPtpPort)self);
    uint32 regAddr = CounterRegister(self, counterType, r2c);

    if (regAddr == cInvalidUint32)
        return 0;

    return mChannelHwRead(self, baseAddress + regAddr, cAtModulePtp);
    }

static void Delete(AtObject self)
    {
    /* Call super to fully delete itself */
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    m_AtObjectMethods->Serialize(self, encoder);
    }

static eBool TransportTypeIsSupported(AtPtpPort self, eAtPtpTransportType type)
    {
    AtUnused(self);
    if (type == cAtPtpTransportTypeAny) return cAtTrue;
    return cAtFalse;
    }

static void OverrideAtObject(AtPtpPort self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtPtpPort self)
    {
    AtChannel channel = (AtChannel)self;
    AtOsal osal = AtSharedDriverOsalGet();

    if (!m_methodsInit)
        {
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, CanEnable);
        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, IsEnabled);
        mMethodOverride(m_AtChannelOverride, TypeString);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtPtpPort(AtPtpPort self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPtpPortOverride, mMethodsGet(self), sizeof(m_AtPtpPortOverride));

        /* Setup methods */
        mMethodOverride(m_AtPtpPortOverride, StateSet);
        mMethodOverride(m_AtPtpPortOverride, StateGet);
        mMethodOverride(m_AtPtpPortOverride, StepModeSet);
        mMethodOverride(m_AtPtpPortOverride, StepModeGet);
        mMethodOverride(m_AtPtpPortOverride, TransportTypeSet);
        mMethodOverride(m_AtPtpPortOverride, TransportTypeGet);
        mMethodOverride(m_AtPtpPortOverride, TxDelayAdjustSet);
        mMethodOverride(m_AtPtpPortOverride, TxDelayAdjustGet);
        mMethodOverride(m_AtPtpPortOverride, RxDelayAdjustSet);
        mMethodOverride(m_AtPtpPortOverride, RxDelayAdjustGet);
        mMethodOverride(m_AtPtpPortOverride, TransportTypeIsSupported);
        }

    mMethodsSet(self, &m_AtPtpPortOverride);
    }

static void OverrideThaPtpPort(AtPtpPort self)
    {
    ThaPtpPort port = (ThaPtpPort)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPtpPortOverride, mMethodsGet(port), sizeof(m_ThaPtpPortOverride));

        /* Setup methods */
        mMethodOverride(m_ThaPtpPortOverride, CounterRead2Clear);
        mMethodOverride(m_ThaPtpPortOverride, ExpectedVlanIsSupported);
        mMethodOverride(m_ThaPtpPortOverride, TxDelayAdjustDefaultVlue);
        mMethodOverride(m_ThaPtpPortOverride, RxDelayAdjustDefaultVlue);
        }

    mMethodsSet(port, &m_ThaPtpPortOverride);
    }

static void Override(AtPtpPort self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtPtpPort(self);
    OverrideThaPtpPort(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPtpBackplanePort);
    }

AtPtpPort ThaPtpBackplanePortObjectInit(AtPtpPort self, uint32 channelId, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPtpPortObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPtpPort ThaPtpBackplanePortNew(uint32 channelId, AtModule module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPtpPort newPort = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPort == NULL)
        return NULL;

    return ThaPtpBackplanePortObjectInit(newPort, channelId, module);
    }


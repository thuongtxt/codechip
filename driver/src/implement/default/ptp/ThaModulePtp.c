/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PTP
 *
 * File        : ThaModulePtp.c
 *
 * Created Date: Jul 6, 2018
 *
 * Description : Default module PTP implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../util/coder/AtCoderUtil.h"
#include "../man/ThaDevice.h"
#include "../man/ThaIpCoreInternal.h"
#include "psn/ThaPtpPsn.h"
#include "ThaModulePtpInternal.h"
#include "ThaModulePtpReg.h"
#include "ThaPtpPort.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaPtpMacOctet0_Mask          cBit15_8
#define cThaPtpMacOctet0_Shift         8
#define cThaPtpMacOctet1_Mask          cBit7_0
#define cThaPtpMacOctet1_Shift         0
#define cThaPtpMacOctet2_Mask          cBit31_24
#define cThaPtpMacOctet2_Shift         24
#define cThaPtpMacOctet3_Mask          cBit23_16
#define cThaPtpMacOctet3_Shift         16
#define cThaPtpMacOctet4_Mask          cBit15_8
#define cThaPtpMacOctet4_Shift         8
#define cThaPtpMacOctet5_Mask          cBit7_0
#define cThaPtpMacOctet5_Shift         0

#define cThaPtpMacGlbUcastEnaMask      cBit0
#define cThaPtpMacGlbUcastEnaShift     0

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaModulePtp)(self))

#define mDebugField(regVal, fieldNumber)                                        \
    do {                                                                        \
    AtPrintc(cSevNormal, "%s\t\t: ", fieldName##fieldNumber##);                 \
    AtPrintc((regVal & cBit##fieldNumber##) ? cSevInfo : cSevNormal, "%s", (regVal & cBit##fieldNumber##) ? "SET" : "CLEAR");\
    } while (0)

/*--------------------------- Local typedefs ---------------------------------*/
typedef eAtRet (*PtpPartHandler)(ThaModulePtp self, uint8 partId, uint32 value);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaModulePtpMethods m_methods;

/* Override */
static tAtObjectMethods         m_AtObjectOverride;
static tAtModuleMethods         m_AtModuleOverride;
static tAtModulePtpMethods      m_AtModulePtpOverride;

/* Save super implementations */
static const tAtObjectMethods   *m_AtObjectMethods  = NULL;
static const tAtModuleMethods   *m_AtModuleMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtModulePtpRet HelperPartSet(ThaModulePtp self, PtpPartHandler partHandler, uint32 value)
    {
    eAtRet ret = cAtOk;
    uint8 part_i, numParts = ThaModulePtpNumParts(self);

    for (part_i = 0; part_i < numParts; part_i++)
        ret |= partHandler(mThis(self), part_i, value);
    return ret;
    }

static uint32 PartBaseAddress(ThaModulePtp self, uint8 partId)
    {
    return ThaModulePtpBaseAddress(self) + ThaModulePtpPartOffset(self, partId);
    }

static eBool IsBackplanePortId(uint32 portId)
    {
    if (portId == 0)
        return cAtTrue;
    return cAtFalse;
    }

static uint8 NumPtpPorts(AtModulePtp self)
    {
    AtUnused(self);
    return 17;
    }

static eBool IsBackplanePortLocalAddress(ThaModulePtp self, uint32 localAddress)
    {
    AtUnused(self);
    localAddress = localAddress & cBit15_0;
    if ((localAddress >= 0x8000) && (localAddress <= 0xFFFF))
        return cAtTrue;
    return cAtFalse;
    }

static uint32 *HoldRegistersGet(AtModule self, uint16 *numberOfHoldRegisters)
    {
    static uint32 holdRegisters[3] = {0};
    uint32 baseAddress = ThaModulePtpBaseAddress(mThis(self));

    holdRegisters[0] = baseAddress + cAf6Reg_upen_hold1;
    holdRegisters[1] = baseAddress + cAf6Reg_upen_hold2;
    holdRegisters[2] = baseAddress + cAf6Reg_upen_hold3;

    if (numberOfHoldRegisters)
        *numberOfHoldRegisters = mCount(holdRegisters);

    return holdRegisters;
    }

static AtLongRegisterAccess LongRegisterAccessCreate(AtModule self)
    {
    uint16 numHoldRegisters;
    uint32 *holdRegisters = HoldRegistersGet(self, &numHoldRegisters);
    return AtDefaultLongRegisterAccessNew(holdRegisters, numHoldRegisters, holdRegisters, numHoldRegisters);
    }

static uint32 *BackplaneHoldRegistersGet(ThaModulePtp self, uint16 *numberOfHoldRegisters)
    {
    static uint32 holdRegisters[3] = {0};
    uint32 baseAddress = ThaModulePtpBaseAddress(mThis(self));

    holdRegisters[0] = baseAddress + cAf6Reg_upen_hold_40g1;
    holdRegisters[1] = baseAddress + cAf6Reg_upen_hold_40g2;
    holdRegisters[2] = baseAddress + cAf6Reg_upen_hold_40g3;

    if (numberOfHoldRegisters)
        *numberOfHoldRegisters = mCount(holdRegisters);

    return holdRegisters;
    }

static AtLongRegisterAccess BackplaneLongRegisterAccessCreate(ThaModulePtp self)
    {
    uint16 numHoldRegisters;
    uint32 *holdRegisters = mMethodsGet(self)->BackplaneHoldRegistersGet(self, &numHoldRegisters);
    return AtDefaultLongRegisterAccessNew(holdRegisters, numHoldRegisters, holdRegisters, numHoldRegisters);
    }

static AtLongRegisterAccess BackplanePortLongRegisterAccess(ThaModulePtp self)
    {
    if (self->backplanePortLongRegisterAccess)
        return self->backplanePortLongRegisterAccess;

    self->backplanePortLongRegisterAccess = BackplaneLongRegisterAccessCreate(self);
    return self->backplanePortLongRegisterAccess;
    }

static AtLongRegisterAccess LongRegisterAccess(AtModule self, uint32 localAddress)
    {
    if (IsBackplanePortLocalAddress(mThis(self), localAddress))
        return BackplanePortLongRegisterAccess(mThis(self));

    return m_AtModuleMethods->LongRegisterAccess(self, localAddress);
    }

static AtPtpPort PtpPortObjectCreate(AtModulePtp self, uint32 portId)
    {
    if (IsBackplanePortId(portId))
        return ThaPtpBackplanePortNew(portId, (AtModule)self);

    return ThaPtpPortNew(portId, (AtModule)self);
    }

static eBool DeviceTypeIsSupported(AtModulePtp self, eAtPtpDeviceType deviceType)
    {
    AtUnused(self);
    if ((deviceType == cAtPtpDeviceTypeBoundary) ||
        (deviceType == cAtPtpDeviceTypeTransparent))
        return cAtTrue;
    return cAtFalse;
    }

static eAtModulePtpRet PartFacepalteDeviceTypeSet(ThaModulePtp self, uint8 partId, eAtPtpDeviceType deviceType)
    {
    uint32 regAddr = PartBaseAddress(self, partId) + cAf6Reg_upen_ptp_dev;
    uint32 regVal;
    uint32 hwDeviceType = (deviceType == cAtPtpDeviceTypeBoundary) ? 0 : 1;

    regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_upen_ptp_dev_device_Type_, hwDeviceType);
    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eAtModulePtpRet FaceplateDeviceTypeSet(AtModulePtp self, eAtPtpDeviceType deviceType)
    {
    return HelperPartSet(mThis(self), PartFacepalteDeviceTypeSet, deviceType);
    }

static eAtModulePtpRet BackplaneDeviceTypeSet(AtModulePtp self, eAtPtpDeviceType deviceType)
    {
    uint32 regAddr = 0;
    uint32 regVal;
    uint32 hwDeviceType = (deviceType == cAtPtpDeviceTypeBoundary) ? 0 : 1;

    regAddr = ThaModulePtpBaseAddress(mThis(self)) + cAf6Reg_upen_ptp40_dev;
    regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_upen_ptp_dev_device_Type_, hwDeviceType);
    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eAtModulePtpRet PhysicalBackplaneDeviceTypeSet(ThaModulePtp self, eAtPtpDeviceType deviceType)
    {
    AtUnused(self);
    AtUnused(deviceType);
    return cAtOk;
    }

static eBool ShouldUseVendorBcMode(void)
    {
    return cAtFalse;
    }

static eAtModulePtpRet DeviceTypeSet(AtModulePtp self, eAtPtpDeviceType deviceType)
    {
    eAtRet ret = cAtOk;

    if (!AtModulePtpDeviceTypeIsSupported(self, deviceType))
        return cAtErrorModeNotSupport;

    ret  = FaceplateDeviceTypeSet(self, deviceType);
    ret |= BackplaneDeviceTypeSet(self, deviceType);

    if (ShouldUseVendorBcMode())
        ret |= ThaModulePtpPhysicalBackplaneDeviceTypeSet((ThaModulePtp)self, deviceType);

    return ret;
    }

static eAtPtpDeviceType FaceplateDeviceTypeGet(AtModulePtp self)
    {
    uint32 regAddr = ThaModulePtpBaseAddress(mThis(self)) + cAf6Reg_upen_ptp_dev;
    uint32 regVal = mModuleHwRead(self, regAddr);

    if (regVal & cAf6_upen_ptp_dev_device_Type_Mask)
        return cAtPtpDeviceTypeTransparent;

    return cAtPtpDeviceTypeBoundary;
    }

static eAtPtpDeviceType DeviceTypeGet(AtModulePtp self)
    {
    return FaceplateDeviceTypeGet(self);
    }

static uint8 NumExpectedVlan(AtModulePtp self)
    {
    AtUnused(self);
    return 4;
    }

static uint32 BackplaneExpectedVlanRegister(AtModulePtp self)
    {
    return ThaModulePtpBaseAddress(mThis(self)) + cAf6Reg_upen_bp_vid_glb_Base;
    }

static eAtModulePtpRet BackplaneExpectedVlanSet(AtModulePtp self, uint8 index, tAtEthVlanTag *atag)
    {
    uint32 vlanIdAddress, regVal = 0;
    uint32 enabled = 0, supportedIndex = 0;
    uint32 maxIndex = (uint32)NumExpectedVlan(self);

    if (index >= maxIndex)
        return cAtErrorOutOfRangParm;

    if (!atag)
        {
        regVal = 0;
        }
    else
        {
        mRegFieldSet(regVal, cAf6_upen_face_vid_glb_cfg_face_vidglb_, atag->vlanId);
        mRegFieldSet(regVal, cAf6_upen_bp_vid_glb_cfg_bp_vidglb_enable_, 1);
        }

    vlanIdAddress = BackplaneExpectedVlanRegister(self);
    mModuleHwWrite(self, (vlanIdAddress + index), regVal);

    for (supportedIndex = 0; supportedIndex < maxIndex; supportedIndex++)
        {
        regVal = mModuleHwRead(self, (vlanIdAddress + supportedIndex));
        if (regVal & cAf6_upen_bp_vid_glb_cfg_bp_vidglb_enable_Mask)
            enabled = 1;
        }

    regVal = mModuleHwRead(self, vlanIdAddress);
    mRegFieldSet(regVal, cAf6_upen_bp_vid_glb_cfg_bp_vidglb_enable_, enabled);
    mModuleHwWrite(self, vlanIdAddress, regVal);
    return cAtOk;
    }

static eBool BackplaneExpectedVlanIsEnabled(AtModulePtp self)
    {
    uint32 vlanIdAddress, regVal = 0;
    uint32 enabled = 0, supportedIndex = 0;
    uint32 maxIndex = (uint32)NumExpectedVlan(self);

    vlanIdAddress = BackplaneExpectedVlanRegister(self);

    for (supportedIndex = 0; supportedIndex < maxIndex; supportedIndex++)
        {
        regVal = mModuleHwRead(self, (vlanIdAddress + supportedIndex));
        if (regVal & cAf6_upen_bp_vid_glb_cfg_bp_vidglb_enable_Mask)
            enabled = 1;
        }

    return (eBool)enabled;
    }

static eAtModulePtpRet PartFaceplateExpectedVlanSet(ThaModulePtp self, uint8 partId, uint8 index, tAtEthVlanTag *atag)
    {
    uint32 partBaseAddress = PartBaseAddress(self, partId);
    uint32 vlanIdAddress = partBaseAddress + cAf6Reg_upen_face_vid_glb_Base;
    uint32 enabledAddress = partBaseAddress + cAf6Reg_upen_face_vid_glb_en_Base;
    uint32 regVal;
    uint32 enabled;

    regVal = (atag) ? (uint32)atag->vlanId : 0;
    mModuleHwWrite(self, (vlanIdAddress + index), regVal);

    enabled = BackplaneExpectedVlanIsEnabled((AtModulePtp)self);
    mModuleHwWrite(self, enabledAddress, enabled);

    return cAtOk;
    }

static eAtModulePtpRet FaceplateExpectedVlanSet(AtModulePtp self, uint8 index, tAtEthVlanTag *atag)
    {
    uint8 part_i, numParts = ThaModulePtpNumParts(mThis(self));
    eAtRet ret = cAtOk;

    if (index >= (uint32)NumExpectedVlan(self))
        return cAtErrorOutOfRangParm;

    for (part_i = 0; part_i < numParts; part_i++)
        ret |= PartFaceplateExpectedVlanSet(mThis(self), part_i, index, atag);
    return ret;
    }

static tAtEthVlanTag* BackplaneExpectedVlanGet(AtModulePtp self, uint8 index, tAtEthVlanTag *atag)
    {
    uint32 vlanIdAddress, regVal;
    uint32 enabled = 0;
    uint32 maxIndex = (uint32)NumExpectedVlan(self);

    if ((index >= maxIndex) || (atag == NULL))
        return NULL;

    vlanIdAddress = BackplaneExpectedVlanRegister(self);
    regVal = mModuleHwRead(self, (vlanIdAddress + index));
    enabled = mRegField(regVal, cAf6_upen_bp_vid_glb_cfg_bp_vidglb_enable_);
    if (enabled)
        {
        atag->vlanId = regVal & cAf6_upen_face_vid_glb_cfg_face_vidglb_Mask;
        return atag;
        }
    return NULL;
    }

static eAtModulePtpRet ExpectedVlanSet(AtModulePtp self, uint8 index, tAtEthVlanTag *atag)
    {
    eAtRet ret = cAtOk;

    ret  = BackplaneExpectedVlanSet(self, index, atag);
    ret |= FaceplateExpectedVlanSet(self, index, atag);

    return ret;
    }

static tAtEthVlanTag* ExpectedVlanGet(AtModulePtp self, uint8 index, tAtEthVlanTag *atag)
    {
    return BackplaneExpectedVlanGet(self, index, atag);
    }

static AtIpCore IpCore(ThaModulePtp self)
    {
    return AtDeviceIpCoreGet(AtModuleDeviceGet((AtModule)self), AtModuleDefaultCoreGet((AtModule)self));
    }

static eAtModulePtpRet PartDeviceMacAddressEnable(ThaModulePtp self, uint8 partId, eBool enable)
    {
    uint32 regAddr = PartBaseAddress(self, partId) + cAf6Reg_upen_macglb_en;
    uint32 regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cThaPtpMacGlbUcastEna, mBoolToBin(enable));
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtModulePtpRet DeviceMacAddressEnable(AtModulePtp self, eBool enable)
    {
    return HelperPartSet(mThis(self), (PtpPartHandler)PartDeviceMacAddressEnable, enable);
    }

static eBool DeviceMacAddressIsEnabled(AtModulePtp self)
    {
    uint32 regAddr = ThaModulePtpBaseAddress(mThis(self)) + cAf6Reg_upen_macglb_en;
    uint32 regVal = mModuleHwRead(self, regAddr);
    return (regVal & cThaPtpMacGlbUcastEnaMask) ? cAtTrue : cAtFalse;
    }

static eAtModulePtpRet PartDeviceMacAddressSet(ThaModulePtp self, uint8 partId, const uint8 *destMac)
    {
    uint32 regAddr = PartBaseAddress(self, partId) + cAf6Reg_upen_umac_glb;
    uint32 longRegVal[cThaLongRegMaxSize];

    mModuleHwLongRead(self, regAddr, longRegVal, cThaLongRegMaxSize, IpCore(self));
    mRegFieldSet(longRegVal[1], cThaPtpMacOctet0_, destMac[0]);
    mRegFieldSet(longRegVal[1], cThaPtpMacOctet1_, destMac[1]);
    mRegFieldSet(longRegVal[0], cThaPtpMacOctet2_, destMac[2]);
    mRegFieldSet(longRegVal[0], cThaPtpMacOctet3_, destMac[3]);
    mRegFieldSet(longRegVal[0], cThaPtpMacOctet4_, destMac[4]);
    mRegFieldSet(longRegVal[0], cThaPtpMacOctet5_, destMac[5]);
    mModuleHwLongWrite(self, regAddr, longRegVal, cThaLongRegMaxSize, IpCore(self));

    return cAtOk;
    }

static eAtModulePtpRet DeviceMacAddressSet(AtModulePtp self, const uint8 *destMac)
    {
    uint8 part_i, numParts = ThaModulePtpNumParts(mThis(self));
    eAtRet ret = cAtOk;

    for (part_i = 0; part_i < numParts; part_i++)
        ret |= PartDeviceMacAddressSet(mThis(self), part_i, destMac);
    return ret;
    }

static eAtModulePtpRet DeviceMacAddressGet(AtModulePtp self, uint8 *destMac)
    {
    uint32 regAddr = ThaModulePtpBaseAddress(mThis(self)) + cAf6Reg_upen_umac_glb;
    uint32 longRegVal[cThaLongRegMaxSize];

    mModuleHwLongRead(self, regAddr, longRegVal, cThaLongRegMaxSize, IpCore(mThis(self)));
    destMac[0] = (uint8)mRegField(longRegVal[1], cThaPtpMacOctet0_);
    destMac[1] = (uint8)mRegField(longRegVal[1], cThaPtpMacOctet1_);
    destMac[2] = (uint8)mRegField(longRegVal[0], cThaPtpMacOctet2_);
    destMac[3] = (uint8)mRegField(longRegVal[0], cThaPtpMacOctet3_);
    destMac[4] = (uint8)mRegField(longRegVal[0], cThaPtpMacOctet4_);
    destMac[5] = (uint8)mRegField(longRegVal[0], cThaPtpMacOctet5_);

    return cAtOk;
    }

static eAtModulePtpRet PartTimeOfDaySet(ThaModulePtp self, uint8 partId, uint64 seconds)
    {
    uint32 regAddr = PartBaseAddress(self, partId) + cAf6Reg_upen_todinit_cfg;
    uint32 longRegVal[cThaLongRegMaxSize];

    mModuleHwLongRead(self, regAddr, longRegVal, cThaLongRegMaxSize, IpCore(self));
    longRegVal[0] = (seconds & cBit31_0);
    longRegVal[1] = (seconds >> 32) & cBit15_0;
    mModuleHwLongWrite(self, regAddr, longRegVal, cThaLongRegMaxSize, IpCore(self));

    return cAtOk;
    }

static eAtModulePtpRet TimeOfDaySet(AtModulePtp self, uint64 seconds)
    {
    eAtRet ret = cAtOk;
    uint8 part_i, numParts = ThaModulePtpNumParts(mThis(self));

    for (part_i = 0; part_i < numParts; part_i++)
        ret |= PartTimeOfDaySet(mThis(self), part_i, seconds);
    return ret;
    }

static uint64 TimeOfDayGet(AtModulePtp self)
    {
    uint32 regAddr = ThaModulePtpBaseAddress(mThis(self)) + cAf6Reg_upen_tod_cur;
    uint32 longRegVal[cThaLongRegMaxSize];
    uint64 timeOfDay;

    mModuleHwLongRead(self, regAddr, longRegVal, cThaLongRegMaxSize, IpCore(mThis(self)));
    timeOfDay = longRegVal[1] & cBit15_0;
    timeOfDay = (timeOfDay << 32) | longRegVal[0];

    return timeOfDay;
    }

static eAtRet PartPpsRouteTripDelaySet(ThaModulePtp self, uint8 partId, uint32 nanoseconds)
    {
    uint32 regAddr = PartBaseAddress(self, partId) + cAf6Reg_upen_tod_mode;
    uint32 regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_upen_tod_mode_compensate_route_1pps_, nanoseconds);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtModulePtpRet PpsRouteTripDelaySet(AtModulePtp self, uint32 nanoseconds)
    {
    return HelperPartSet(mThis(self), PartPpsRouteTripDelaySet, nanoseconds);
    }

static uint32 PpsRouteTripDelayGet(AtModulePtp self)
    {
    uint32 regAddr = ThaModulePtpBaseAddress(mThis(self)) + cAf6Reg_upen_tod_mode;
    uint32 regVal = mModuleHwRead(self, regAddr);
    return mRegField(regVal, cAf6_upen_tod_mode_compensate_route_1pps_);
    }

static eBool PpsSourceIsSupported(AtModulePtp self, eAtPtpPpsSource source)
    {
    AtUnused(self);
    return ((source == cAtPtpPpsSourceExternal) || (source == cAtPtpPpsSourceInternal)) ? cAtTrue : cAtFalse;
    }

static eAtRet PartPpsSourceSet(ThaModulePtp self, uint8 partId, eAtPtpPpsSource source)
    {
    uint32 baseAddress = PartBaseAddress(self, partId);
    uint32 regAddr;
    uint32 regVal;

    regAddr = baseAddress + cAf6Reg_upen_tod_mode;
    regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_upen_tod_mode_upen_todmode_, (source == cAtPtpPpsSourceExternal) ? 1 : 0);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtModulePtpRet PpsSourceSet(AtModulePtp self, eAtPtpPpsSource source)
    {
    if (!AtModulePtpPpsSourceIsSupported(self, source))
        return cAtErrorModeNotSupport;

    return HelperPartSet(mThis(self), PartPpsSourceSet, source);
    }

static eAtPtpPpsSource PpsSourceGet(AtModulePtp self)
    {
    uint32 baseAddress = ThaModulePtpBaseAddress(mThis(self));
    uint32 regAddr;
    uint32 regVal;

    regAddr = baseAddress + cAf6Reg_upen_tod_mode;
    regVal = mModuleHwRead(self, regAddr);
    return (mRegField(regVal, cAf6_upen_tod_mode_upen_todmode_)) ? cAtPtpPpsSourceExternal : cAtPtpPpsSourceInternal;
    }

static uint32 NumL2PsnGroups(AtModulePtp self)
    {
    AtUnused(self);
    return 2;
    }

static uint32 NumIpV4PsnGroups(AtModulePtp self)
    {
    AtUnused(self);
    return 2;
    }

static uint32 NumIpV6PsnGroups(AtModulePtp self)
    {
    AtUnused(self);
    return 2;
    }

static AtPtpPsnGroup PsnGroupObjectCreate(AtModulePtp self, uint32 groupId, eAtPtpPsnType psnType)
    {
    return ThaPtpPsnGroupNew(self, groupId, psnType);
    }

static eBool CorrectionModeIsSupported(AtModulePtp self, eAtPtpCorrectionMode correctionMode)
    {
    AtUnused(self);
    if (correctionMode == cAtPtpCorrectionModeStandard)
        return cAtTrue;
    return cAtFalse;
    }

static eAtModulePtpRet PartCorrectionModeSet(ThaModulePtp self, uint8 partId, eAtPtpCorrectionMode correctionMode)
    {
    uint32 baseAddr = PartBaseAddress(self, partId);
    uint32 regAddr = baseAddr + cAf6Reg_upen_ptp_dev;
    uint32 regVal;
    uint32 hwMode = (correctionMode == cAtPtpCorrectionModeStandard) ? 0 : 1;

    regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_upen_ptp_dev_device_TCmode_, hwMode);
    mModuleHwWrite(self, regAddr, regVal);

    regAddr = baseAddr + cAf6Reg_upen_ptp40_dev;
    mRegFieldSet(regVal, cAf6_upen_ptp_dev_device_TCmode_, hwMode);
    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eAtModulePtpRet CorrectionModeSet(AtModulePtp self, eAtPtpCorrectionMode correctionMode)
    {
    if (!AtModulePtpCorrectionModeIsSupported(self, correctionMode))
        return cAtErrorModeNotSupport;

    return HelperPartSet(mThis(self), PartCorrectionModeSet, correctionMode);
    }

static eAtPtpCorrectionMode CorrectionModeGet(AtModulePtp self)
    {
    uint32 regAddr = ThaModulePtpBaseAddress(mThis(self)) + cAf6Reg_upen_ptp_dev;
    uint32 regVal = mModuleHwRead(self, regAddr);
    return mRegField(regVal, cAf6_upen_ptp_dev_device_TCmode_) ? cAtPtpCorrectionModeAssist : cAtPtpCorrectionModeStandard;
    }

static uint32 T1T3TimestampCaptureModeSw2Hw(eAtPtpT1T3TimeStampCaptureMode mode)
    {
    if (mode == cAtPtpT1T3TimestampCaptureModeNone)
        return 0;
    if (mode == cAtPtpT1T3TimestampCaptureModeCpu)
        return 2;
    if (mode == cAtPtpT1T3TimestampCaptureModePacket)
        return 3;
    return 0;
    }

static eAtPtpT1T3TimeStampCaptureMode T1T3TimestampCaptureModeHw2Sw(uint32 mode)
    {
    if (mode == 0)
        return cAtPtpT1T3TimestampCaptureModeNone;
    if (mode == 2)
        return cAtPtpT1T3TimestampCaptureModeCpu;
    if (mode == 3)
        return cAtPtpT1T3TimestampCaptureModePacket;
    return cAtPtpT1T3TimestampCaptureModeNone;
    }

static eAtModulePtpRet T1T3CaptureModeSet(AtModulePtp self, eAtPtpT1T3TimeStampCaptureMode mode)
    {
    uint32 address = cAf6Reg_upen_t1t3mode40g_Base + ThaModulePtpBaseAddress(mThis(self));
    uint32 regVal = mModuleHwRead(self, address);
    uint32 hwVal = T1T3TimestampCaptureModeSw2Hw(mode);

    mRegFieldSet(regVal, cAf6_upen_t1t3mode40g_cfg_t1t3mode40g_and_Enable_, hwVal);
    mModuleHwWrite(self, address, regVal);
    return cAtOk;
    }

static eAtPtpT1T3TimeStampCaptureMode T1T3CaptureModeGet(AtModulePtp self)
    {
    uint32 address = cAf6Reg_upen_t1t3mode40g_Base + ThaModulePtpBaseAddress(mThis(self));
    uint32 regVal = mModuleHwRead(self, address);
    uint32 hwVal = mRegField(regVal, cAf6_upen_t1t3mode40g_cfg_t1t3mode40g_and_Enable_);
    eAtPtpT1T3TimeStampCaptureMode mode = T1T3TimestampCaptureModeHw2Sw(hwVal);

    return mode;
    }

static AtPtpPort EgressPortIdToPtpPort(ThaModulePtp self, uint32 egressPortId)
    {
    AtUnused(self);
    AtUnused(egressPortId);
    return NULL;
    }

static void T1T3TimestampContentParser(ThaModulePtp self, uint32 *buffer, tAtPtpT1T3TimestampContent *content)
    {
    content->nanoSeconds = buffer[0];
    content->seconds = buffer[1];
    content->seconds += ((uint64)mRegField(buffer[2], cAf6_upen_cpuque40_egr_tmr_tod_03_) << 32);
    content->sequenceNumber = (uint16)AtUtilLongFieldGet(buffer, 2,
                                                         cAf6_upen_cpuque40_seqid_01_Mask, cAf6_upen_cpuque40_seqid_01_Shift,
                                                         cAf6_upen_cpuque40_seqid_02_Mask, cAf6_upen_cpuque40_seqid_02_Shift);
    content->egressPort = ThaModulePtpEgressPortIdToPtpPort(self, mRegField(buffer[2], cAf6_upen_cpuque40_egr_pid_));
    content->ptpMsgType = mRegField(buffer[3], cAf6_upen_cpuque40_typ_);
    }

static uint32 NumHwT1T3TimestampGet(ThaModulePtp self)
    {
    uint32 regVal = mModuleHwRead(self, cAf6Reg_upen_sta40_Base + ThaModulePtpBaseAddress(self));
    uint32 numTimestamp;

    /* No latched entry indication */
    if (!mRegField(regVal, cAf6_upen_sta40g_upen_sta_not_empty_))
        return 0;

    numTimestamp = mRegField(regVal, cAf6_upen_sta40_upen_sta_length_);

    if (regVal & cAf6_upen_sta40_upen_sta_full_Mask)
        AtModuleLog((AtModule)self, cAtLogLevelCritical,
                    AtSourceLocation, "40G CPU queue is full, numTimestampInHw = %d\r\n", numTimestamp);

    return numTimestamp;
    }

static uint32 T1T3CaptureContentGet(AtModulePtp self, tAtPtpT1T3TimestampContent *timestampInfoBuffer, uint32 numberOfTimestamp)
    {
    uint32 regAddr;
    AtIpCore core = IpCore(mThis(self));
    uint32 numTimestampInHw, timestamp_i;

    /* Check for input parameters */
    if ((timestampInfoBuffer == NULL) || (numberOfTimestamp == 0))
        return 0;

    numTimestampInHw = NumHwT1T3TimestampGet(mThis(self));
    if (numTimestampInHw == 0)
        return 0;

    if (numberOfTimestamp > numTimestampInHw)
        numberOfTimestamp = numTimestampInHw;

    regAddr = cAf6Reg_upen_cpuque40_Base + ThaModulePtpBaseAddress(mThis(self));
    for (timestamp_i = 0; timestamp_i < numberOfTimestamp; timestamp_i++)
        {
        uint32 longRegVal[cThaLongRegMaxSize];
        mModuleHwLongRead(self, regAddr, longRegVal, cThaLongRegMaxSize, core);
        mMethodsGet(mThis(self))->T1T3TimestampContentParser(mThis(self), longRegVal, &timestampInfoBuffer[timestamp_i]);
        }

    return numberOfTimestamp;
    }

static eAtModulePtpRet PartFaceplateTimestampBypassEnable(ThaModulePtp self, uint8 partId, eBool enabled)
    {
    uint32 regAddr = PartBaseAddress(self, partId) + cAf6Reg_upen_ge_ptp_dis_timestamping_Base;
    uint32 regVal = mModuleHwRead(self, regAddr);

    mRegFieldSet(regVal, cAf6_upen_ge_ptp_dis_timestamping_cfg_timstap_dis_face_2_BP_, enabled ? 1 : 0);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtModulePtpRet BackplaneTimestampBypassEnable(ThaModulePtp self, eBool enabled)
    {
    uint32 regAddr = cAf6Reg_upen_bp_ptp_dis_timestamping_Base + ThaModulePtpBaseAddress(self);
    uint32 regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_upen_bp_ptp_dis_timestamping_cfg_timstap_dis_BP_2_face_, enabled ? 1 : 0);
    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eAtModulePtpRet FaceplateTimestampBypassEnable(ThaModulePtp self, eBool enabled)
    {
    return HelperPartSet(self, (PtpPartHandler)PartFaceplateTimestampBypassEnable, enabled);
    }

static eAtModulePtpRet TimestampBypassEnable(AtModulePtp self, eBool enabled)
    {
    eAtRet ret;
    ret  = FaceplateTimestampBypassEnable(mThis(self), enabled);
    ret |= BackplaneTimestampBypassEnable(mThis(self), enabled);
    return ret;
    }

static eBool TimestampBypassIsEnabled(AtModulePtp self)
    {
    uint32 regAddr = cAf6Reg_upen_ge_ptp_dis_timestamping_Base + ThaModulePtpBaseAddress(mThis(self));
    uint32 regVal = mModuleHwRead(self, regAddr);
    uint32 enabled = mRegField(regVal, cAf6_upen_ge_ptp_dis_timestamping_cfg_timstap_dis_face_2_BP_);

    return (eBool) (enabled ? cAtTrue : cAtFalse);
    }

static uint32 AlarmR2C(AtModule self, eBool r2c)
    {
    uint32 regVal, address, event = 0;

    address = cAf6Reg_upen_int40_Base + ThaModulePtpBaseAddress(mThis(self));
    regVal  = mModuleHwRead(self, address);
    if (regVal & cAf6_upen_int40_int_en_Mask)
        {
        event = cAtModulePtpAlarmTypeCpuQueu;
        if (r2c)
            mModuleHwWrite(self, address, cAf6_upen_int40_int_en_Mask);
        }

    return event;
    }

static uint32 AlarmGet(AtModule self)
    {
    uint32 regVal, address, event = 0;

    address = cAf6Reg_upen_sta40_Base + ThaModulePtpBaseAddress(mThis(self));
    regVal  = mModuleHwRead(self, address);
    if (regVal & cAf6_upen_sta40g_upen_sta_not_empty_Mask)
        event = cAtModulePtpAlarmTypeCpuQueu;

    return event;
    }

static uint32 AlarmHistoryGet(AtModule self)
    {
    return AlarmR2C(self, cAtFalse);
    }

static uint32 AlarmHistoryClear(AtModule self)
    {
    return AlarmR2C(self, cAtTrue);
    }

static uint32 SupportedInterruptMasks(AtModule self)
    {
    AtUnused(self);
    return cAtModulePtpAlarmTypeCpuQueu;
    }

static eAtRet InterruptMaskSet(AtModule self, uint32 eventMask, uint32 enableMask)
    {
    uint32 regVal, address;

    address = cAf6Reg_upen_int_en40_Base + ThaModulePtpBaseAddress(mThis(self));
    regVal  = mModuleHwRead(self, address);
    if (eventMask & cAtModulePtpAlarmTypeCpuQueu)
        {
        mRegFieldSet(regVal,
                     cAf6_upen_int_en40_int_en_,
                     (enableMask & cAtModulePtpAlarmTypeCpuQueu) ? 1 : 0);
        mModuleHwWrite(self, address, regVal);
        }

    return cAtOk;
    }

static uint32 InterruptMaskGet(AtModule self)
    {
    uint32 regVal, address;

    address = cAf6Reg_upen_int_en40_Base + ThaModulePtpBaseAddress(mThis(self));
    regVal  = mModuleHwRead(self, address);
    if (regVal & cAf6_upen_int_en40_int_en_Mask)
        return cAtModulePtpAlarmTypeCpuQueu;

    return 0;
    }

static void InterruptHwEnable(AtIpCore ipCore, eBool enable)
    {
    ThaIpCorePtpHwInterruptEnable((ThaIpCore)ipCore, enable);
    }

static eAtRet InterruptEnable(AtModule self, eBool enable)
    {
    uint8 i, numCore;
    AtIpCore ipCore;
    AtDevice device = AtModuleDeviceGet(self);

    numCore = AtDeviceNumIpCoresGet(device);

    for (i = 0; i < numCore; i++)
        {
        ipCore = AtDeviceIpCoreGet(device, i);

        /* If a product supports interrupt pin enable register, just simple
         * enable module interrupt on IPcore. Otherwise, we need to check if
         * IPcore interrupt was enabled before enable module interrupt. */
        if (ThaIpCoreCanEnableHwInterruptPin((ThaIpCore)ipCore) || AtIpCoreInterruptIsEnabled(ipCore))
            InterruptHwEnable(ipCore, enable);

        /* Store to DB */
        ((ThaModulePtp)self)->interruptIsEnabled = enable;
        }

    return cAtOk;
    }

static eBool InterruptIsEnabled(AtModule self)
    {
    return ((ThaModulePtp)self)->interruptIsEnabled;
    }

/* Enable interrupt on one core */
static void InterruptOnIpCoreEnable(AtModule self, eBool enable, AtIpCore ipCore)
    {
    if(InterruptIsEnabled(self))
        InterruptHwEnable(ipCore, enable);
    }

static void InterruptProcess(AtModule self, uint32 glbIntr, AtIpCore ipCore)
    {
    uint32 event = AtModuleAlarmInterruptClear(self);
    AtUnused(glbIntr);
    AtUnused(ipCore);

    if (event)
        AtModuleAllAlarmListenersCall(self, cAtModulePtpAlarmTypeCpuQueu, cAtModulePtpAlarmTypeCpuQueu);
    }

static uint32 BaseAddress(ThaModulePtp self)
    {
    AtUnused(self);
    return 0xD0000;
    }

static ThaPtpClaController ClaControllerCreate(ThaModulePtp self, AtPtpPort port)
    {
    AtUnused(self);

    if (IsBackplanePortId(AtChannelIdGet((AtChannel)port)))
        return ThaPtpBackplaneClaControllerNew(port);

    return ThaPtpClaControllerNew(port);
    }

static eAtRet DefaultSet(AtModulePtp self)
    {
    eAtRet ret = cAtOk;
    uint8 i, numGlobalVlanSupported = AtModulePtpNumExpectedVlan(self);

    ret |= AtModulePtpDeviceTypeSet(self, cAtPtpDeviceTypeTransparent);
    ret |= AtModulePtpPpsSourceSet(self, cAtPtpPpsSourceInternal);
    ret |= AtModulePtpDeviceMacAddressEnable(self, cAtFalse);
    ret |= AtModulePtpTimestampBypassEnable(self, cAtFalse);
    for (i = 0; i < numGlobalVlanSupported; i++)
        ret |= AtModulePtpExpectedVlanSet(self, i, NULL);

    return ret;
    }

static uint32 ActivationRegister(ThaModulePtp self)
    {
    return cAf6Reg_upen_cla_rdy + ThaModulePtpBaseAddress(self);
    }

static eAtRet PartHwEnable(ThaModulePtp self, uint8 partId, eBool enable)
    {
    uint32 regAddr = ActivationRegister(self) + ThaModulePtpPartOffset(self, partId);
    uint32 regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_upen_ptp_rdy_ptp_rdy_, mBoolToBin(enable));
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet HwEnable(AtModule self, eBool enable)
    {
    return HelperPartSet(mThis(self), (PtpPartHandler)PartHwEnable, enable);
    }

static eAtRet Activate(AtModule self)
    {
    return HwEnable(self, cAtTrue);
    }

static eAtRet Deactivate(AtModule self)
    {
    return HwEnable(self, cAtFalse);
    }

static eBool IsActive(AtModule self)
    {
    uint32 regVal = mModuleHwRead(self, ActivationRegister(mThis(self)));
    return (regVal & cAf6_upen_ptp_rdy_ptp_rdy_Mask) ? cAtTrue : cAtFalse;
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    ret |= DefaultSet((AtModulePtp)self);
    ret |= AtModuleActivate(self);

    return ret;
    }

static void PrintBitWithLabelAndColor(const char* title,
                                      uint32 hwValue, uint32 mask,
                                      const char *setString, const char *clearString,
                                      eAtSevLevel setColor, eAtSevLevel clearColor)
    {
    AtPrintc(cSevNormal, "  * %-32s: ", title);
    if (hwValue & mask)
        AtPrintc(setColor, "%-10s", setString);
    else
        AtPrintc(clearColor, "%-10s", clearString);

    AtStdFlush();
    AtPrintc(cSevNormal, "\r\n");
    }

static void PrintErrorBit(const char* title, uint32 hwValue, uint32 mask)
    {
    if (title)
        PrintBitWithLabelAndColor(title, hwValue, mask, "ERROR", "CLEAR", cSevCritical, cSevInfo);
    }

static void PrintInfoBit(const char* title, uint32 hwValue, uint32 mask)
    {
    if (title)
        PrintBitWithLabelAndColor(title, hwValue, mask, "SET", "CLEAR", cSevInfo, cSevNormal);
    }

static void PrintClaIngressInfo(uint32 regVal)
    {
    PrintInfoBit("CLA Input EOP",   regVal, cBit2);
    PrintInfoBit("CLA Input SOP",   regVal, cBit1);
    PrintInfoBit("CLA Input Valid", regVal, cBit0);
    }

static void PrintClaEgressPtpInfo(uint32 regVal)
    {
    PrintInfoBit("CLA Output EOP (PTP packet)",   regVal, cBit3);
    PrintInfoBit("CLA Output SOP (PTP packet)",   regVal, cBit1);
    PrintInfoBit("CLA Output Valid (PTP packet)", regVal, cBit0);
    }

static void PrintClaEgressNormalInfo(uint32 regVal)
    {
    PrintInfoBit("CLA Output EOP (normal packet)",   regVal, cBit2);
    PrintInfoBit("CLA Output SOP (normal packet)",   regVal, cBit1);
    PrintInfoBit("CLA Output Valid (normal packet)", regVal, cBit0);
    }

static void PrintPtpParserIngressInfo(uint32 regVal)
    {
    PrintInfoBit("PTP Parser Input EOP",   regVal, cBit2);
    PrintInfoBit("PTP Parser Input SOP",   regVal, cBit1);
    PrintInfoBit("PTP Parser Input Valid", regVal, cBit0);
    }

static void PrintPtpParserEgressInfo(uint32 regVal)
    {
    PrintErrorBit("PTP Parser Error",       regVal, cBit3);
    PrintInfoBit("PTP Parser Output EOP",   regVal, cBit2);
    PrintInfoBit("PTP Parser Output SOP",   regVal, cBit1);
    PrintInfoBit("PTP Parser Output Valid", regVal, cBit0);
    }

static void PrintPtpOutputToMacInfo(uint32 regVal)
    {
    PrintInfoBit("PTP Parser Output-to-MAC Done",  regVal, cBit3);
    PrintInfoBit("PTP Parser Output-to-MAC EOP",   regVal, cBit2);
    PrintInfoBit("PTP Parser Output-to-MAC SOP",   regVal, cBit1);
    PrintInfoBit("PTP Parser Output-to-MAC Valid", regVal, cBit0);
    }

static void StickyDebug(AtModule self, const char *regName, uint32 regAddr)
    {
    uint32 regVal = mModuleHwRead(self, regAddr);

    AtPrintc(cSevNormal, "* **********************************\r\n");
    AtPrintc(cSevInfo, "* %s (0x%08x == 0x%08x):\r\n", regName, regAddr, regVal);
    AtPrintc(cSevNormal, "\r\n");

    PrintClaIngressInfo(regVal >> 20);
    PrintClaEgressPtpInfo(regVal >> 16);
    PrintClaEgressNormalInfo(regVal >> 12);
    PrintPtpParserIngressInfo(regVal >> 8);
    PrintPtpParserEgressInfo(regVal >> 4);
    PrintPtpOutputToMacInfo(regVal);
    AtPrintc(cSevNormal, "\r\n");

    mModuleHwWrite(self, regAddr, regVal);
    }

static void AllStickiesDebug(AtModule self)
    {
    uint32 baseAddress = ThaModulePtpBaseAddress(mThis(self));
    const uint32 cPtpGeClaSticky = 0x730;
    const uint32 cPtp40GClaSticky = 0x8730;

    StickyDebug(self, "PTP GE CLA Debug Sticky", (baseAddress + cPtpGeClaSticky));
    StickyDebug(self, "PTP 40G CLA Debug Sticky", (baseAddress + cPtp40GClaSticky));
    }

static eAtRet Debug(AtModule self)
    {
    AtPrintc(cSevInfo,   "\r\nPTP debug information:\r\n");
    AtPrintc(cSevNormal, "* **********************************\r\n");
    AtPrintc(cSevNormal, "* PActive       : %s\r\n", AtModuleIsActive(self) ? "Enabled" : "Disabled");
    AtPrintc(cSevNormal, "\r\n");

    AllStickiesDebug(self);

    return cAtOk;
    }

static eBool HasRegister(AtModule self, uint32 localAddress)
    {
    AtUnused(self);
    if ((localAddress >= 0x00D0000) && (localAddress <= 0xDFFFF))
        return cAtTrue;

    return cAtFalse;
    }

static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject)(mThis(self)->backplanePortLongRegisterAccess));
    mThis(self)->backplanePortLongRegisterAccess = NULL;

    /* Call super to fully delete itself */
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaModulePtp object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObject(backplanePortLongRegisterAccess);
    mEncodeUInt(interruptIsEnabled);
    }

static void OverrideAtObject(AtModulePtp self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModulePtp self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(module), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, LongRegisterAccessCreate);
        mMethodOverride(m_AtModuleOverride, LongRegisterAccess);
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, Activate);
        mMethodOverride(m_AtModuleOverride, Deactivate);
        mMethodOverride(m_AtModuleOverride, IsActive);
        mMethodOverride(m_AtModuleOverride, Debug);
        mMethodOverride(m_AtModuleOverride, HasRegister);
        mMethodOverride(m_AtModuleOverride, AlarmGet);
        mMethodOverride(m_AtModuleOverride, AlarmHistoryGet);
        mMethodOverride(m_AtModuleOverride, AlarmHistoryClear);
        mMethodOverride(m_AtModuleOverride, SupportedInterruptMasks);
        mMethodOverride(m_AtModuleOverride, InterruptMaskSet);
        mMethodOverride(m_AtModuleOverride, InterruptMaskGet);
        mMethodOverride(m_AtModuleOverride, InterruptProcess);
        mMethodOverride(m_AtModuleOverride, InterruptEnable);
        mMethodOverride(m_AtModuleOverride, InterruptIsEnabled);
        mMethodOverride(m_AtModuleOverride, InterruptOnIpCoreEnable);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtModulePtp(AtModulePtp self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePtpOverride, mMethodsGet(self), sizeof(m_AtModulePtpOverride));

        /* Setup methods */
        mMethodOverride(m_AtModulePtpOverride, NumPtpPorts);
        mMethodOverride(m_AtModulePtpOverride, PtpPortObjectCreate);
        mMethodOverride(m_AtModulePtpOverride, DeviceTypeSet);
        mMethodOverride(m_AtModulePtpOverride, DeviceTypeGet);
        mMethodOverride(m_AtModulePtpOverride, DeviceMacAddressSet);
        mMethodOverride(m_AtModulePtpOverride, DeviceMacAddressGet);
        mMethodOverride(m_AtModulePtpOverride, TimeOfDaySet);
        mMethodOverride(m_AtModulePtpOverride, TimeOfDayGet);
        mMethodOverride(m_AtModulePtpOverride, PpsRouteTripDelaySet);
        mMethodOverride(m_AtModulePtpOverride, PpsRouteTripDelayGet);
        mMethodOverride(m_AtModulePtpOverride, PpsSourceSet);
        mMethodOverride(m_AtModulePtpOverride, PpsSourceGet);
        mMethodOverride(m_AtModulePtpOverride, NumL2PsnGroups);
        mMethodOverride(m_AtModulePtpOverride, NumIpV4PsnGroups);
        mMethodOverride(m_AtModulePtpOverride, NumIpV6PsnGroups);
        mMethodOverride(m_AtModulePtpOverride, PsnGroupObjectCreate);
        mMethodOverride(m_AtModulePtpOverride, CorrectionModeSet);
        mMethodOverride(m_AtModulePtpOverride, CorrectionModeGet);
        mMethodOverride(m_AtModulePtpOverride, DeviceMacAddressEnable);
        mMethodOverride(m_AtModulePtpOverride, DeviceMacAddressIsEnabled);
        mMethodOverride(m_AtModulePtpOverride, T1T3CaptureModeSet);
        mMethodOverride(m_AtModulePtpOverride, T1T3CaptureModeGet);
        mMethodOverride(m_AtModulePtpOverride, T1T3CaptureContentGet);
        mMethodOverride(m_AtModulePtpOverride, DeviceTypeIsSupported);
        mMethodOverride(m_AtModulePtpOverride, CorrectionModeIsSupported);
        mMethodOverride(m_AtModulePtpOverride, PpsSourceIsSupported);

        mMethodOverride(m_AtModulePtpOverride, NumExpectedVlan);
        mMethodOverride(m_AtModulePtpOverride, ExpectedVlanSet);
        mMethodOverride(m_AtModulePtpOverride, ExpectedVlanGet);

        mMethodOverride(m_AtModulePtpOverride, TimestampBypassEnable);
        mMethodOverride(m_AtModulePtpOverride, TimestampBypassIsEnabled);
        }

    mMethodsSet(self, &m_AtModulePtpOverride);
    }

static void Override(AtModulePtp self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    OverrideAtModulePtp(self);
    }

static AtEthPort PhysicalEthPortGet(ThaModulePtp self, AtPtpPort port)
    {
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModuleEth);
    return AtModuleEthPortGet(ethModule, (uint8)AtChannelIdGet((AtChannel)port));
    }

static eBool PortCanJoinPsnGroup(ThaModulePtp self, AtPtpPort port, AtPtpPsnGroup group)
    {
    AtUnused(self);
    AtUnused(group);

    if (IsBackplanePortId(AtChannelIdGet((AtChannel)port)))
        return cAtFalse;

    return cAtTrue;
    }

static uint32 PortLocalId(ThaModulePtp self, AtPtpPort port)
    {
    uint32 portId = AtChannelIdGet((AtChannel)port);
    AtUnused(self);

    if (IsBackplanePortId(portId))
        return 0;

    return portId - 1U;
    }

static uint32 PartOffset(ThaModulePtp self, uint8 partId)
    {
    if (partId < ThaModulePtpNumParts(self))
        return (uint32)(partId * 0x10000UL);

    return cInvalidUint32;
    }

static uint8 NumParts(ThaModulePtp self)
    {
    AtUnused(self);
    return 1;
    }

static void MethodsInit(ThaModulePtp self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, BaseAddress);
        mMethodOverride(m_methods, PhysicalEthPortGet);
        mMethodOverride(m_methods, PhysicalBackplaneDeviceTypeSet);
        mMethodOverride(m_methods, EgressPortIdToPtpPort);
        mMethodOverride(m_methods, PortCanJoinPsnGroup);
        mMethodOverride(m_methods, PortLocalId);
        mMethodOverride(m_methods, NumParts);
        mMethodOverride(m_methods, BackplaneHoldRegistersGet);
        mMethodOverride(m_methods, T1T3TimestampContentParser);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaModulePtp);
    }

AtModulePtp ThaModulePtpObjectInit(AtModulePtp self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtModulePtpObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(mThis(self));
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePtp ThaModulePtpNew(AtDevice self)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePtp newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    return ThaModulePtpObjectInit(newModule, self);
    }

uint32 ThaModulePtpBaseAddress(ThaModulePtp self)
    {
    if (self)
        return mMethodsGet(self)->BaseAddress(self);
    return cInvalidUint32;
    }

uint32 ThaModulePtpPartOffset(ThaModulePtp self, uint8 part)
    {
    if (self)
        return PartOffset(self, part);
    return 0;
    }

uint8 ThaModulePtpNumParts(ThaModulePtp self)
    {
    if (self)
        return mMethodsGet(self)->NumParts(self);
    return 0;
    }

ThaPtpClaController ThaModulePtpClaControllerCreate(ThaModulePtp self, AtPtpPort port)
    {
    if (self)
        return ClaControllerCreate(self, port);
    return NULL;
    }

AtEthPort ThaModulePtpPhysicalEthPortGet(ThaModulePtp self, AtPtpPort port)
    {
    if (self)
        return mMethodsGet(self)->PhysicalEthPortGet(self, port);
    return NULL;
    }

eAtModulePtpRet ThaModulePtpPhysicalBackplaneDeviceTypeSet(ThaModulePtp self, eAtPtpDeviceType deviceType)
    {
    if (self)
        return mMethodsGet(self)->PhysicalBackplaneDeviceTypeSet(self, deviceType);
    return cAtErrorNullPointer;
    }

AtPtpPort ThaModulePtpEgressPortIdToPtpPort(ThaModulePtp self, uint32 egressPortId)
    {
    if (self)
        return mMethodsGet(self)->EgressPortIdToPtpPort(self, egressPortId);

    return NULL;
    }

eBool ThaModulePtpPortCanJoinPsnGroup(ThaModulePtp self, AtPtpPort port, AtPtpPsnGroup group)
    {
    if (self)
        return mMethodsGet(self)->PortCanJoinPsnGroup(self, port, group);
    return cAtFalse;
    }

uint32 ThaModulePtpPortLocalId(ThaModulePtp self, AtPtpPort port)
    {
    if (self)
        return mMethodsGet(self)->PortLocalId(self, port);
    return cInvalidUint32;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PTP
 * 
 * File        : ThaPtpPort.h
 * 
 * Created Date: Jul 6, 2018
 *
 * Description : Default PTP port
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPTPPORT_H_
#define _THAPTPPORT_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModule.h"
#include "AtPtpPort.h"
#include "controllers/ThaPtpClaController.h"
#include "AtSerdesController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPtpPort *ThaPtpPort;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPtpClaController ThaPtpPortClaControllerGet(ThaPtpPort self);
eAtRet ThaPtpPortDelayAdjustDefaultSet(ThaPtpPort self, eAtSerdesMode serdesMode);
uint32 ThaPtpPortDefaultOffset(ThaPtpPort self);

/* Product concretes */
AtPtpPort ThaPtpPortNew(uint32 channelId, AtModule module);
AtPtpPort ThaPtpBackplanePortNew(uint32 channelId, AtModule module);
AtPtpPort Tha60290051PtpPortNew(uint32 channelId, AtModule module);
AtPtpPort Tha60290051BackplanePtpPortNew(uint32 channelId, AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _THAPTPPORT_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PTP
 * 
 * File        : ThaModulePtp.h
 * 
 * Created Date: Jul 6, 2018
 *
 * Description : Default module PTP
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEPTP_H_
#define _THAMODULEPTP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModulePtp.h"
#include "AtEthPort.h"
#include "controllers/ThaPtpClaController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModulePtp *ThaModulePtp;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 ThaModulePtpBaseAddress(ThaModulePtp self);

/* Parts */
uint32 ThaModulePtpPartOffset(ThaModulePtp self, uint8 part);
uint8 ThaModulePtpNumParts(ThaModulePtp self);

/* Port handling */
uint32 ThaModulePtpPortLocalId(ThaModulePtp self, AtPtpPort port);
eBool ThaModulePtpPortCanJoinPsnGroup(ThaModulePtp self, AtPtpPort port, AtPtpPsnGroup group);
AtPtpPort ThaModulePtpEgressPortIdToPtpPort(ThaModulePtp self, uint32 egressPortId);
AtEthPort ThaModulePtpPhysicalEthPortGet(ThaModulePtp self, AtPtpPort port);

/* CLA controller */
ThaPtpClaController ThaModulePtpClaControllerCreate(ThaModulePtp self, AtPtpPort port);

eAtModulePtpRet ThaModulePtpPhysicalBackplaneDeviceTypeSet(ThaModulePtp self, eAtPtpDeviceType deviceType);

/* Product concretes */
AtModulePtp ThaModulePtpNew(AtDevice self);
AtModulePtp Tha60290022ModulePtpNew(AtDevice self);
AtModulePtp Tha60290051ModulePtpNew(AtDevice self);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEPTP_H_ */


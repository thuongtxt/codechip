/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Clock
 *
 * File        : ThaModuleClock.c
 *
 * Created Date: Aug 27, 2013
 *
 * Description : Clock module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../util/coder/AtCoderUtil.h"
#include "../man/ThaIpCoreInternal.h"
#include "ThaModuleClockInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6Reg_clock_monitoring_output_Base                           0x49

#define cAf6_clock_monitoring_output_cfgrefpid_Mask(pId)               (cBit4_0 << (pId * 8))
#define cAf6_clock_monitoring_output_cfgrefpid_Shift(pId)              (0 + (pId * 8))

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaModuleClock)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8                  m_methodsInit = 0;
static tThaModuleClockMethods m_methods;

/* Override */
static tAtModuleMethods        m_AtModuleOverride;
static tAtModuleClockMethods   m_AtModuleClockOverride;
static tAtObjectMethods        m_AtObjectOverride;

/* Save super implementation */
static const tAtModuleMethods  *m_AtModuleMethods = NULL;
static const tAtObjectMethods  *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaModuleClock);
    }

static AtClockExtractor ExtractorCreate(ThaModuleClock self, uint8 extractorId)
    {
	AtUnused(extractorId);
	AtUnused(self);
    return NULL;
    }

static uint8 ExtractorPart(ThaModuleClock self, uint8 extractorId)
    {
	AtUnused(extractorId);
	AtUnused(self);
    return 0;
    }

static void AllExtractorsDelete(AtList extractors)
    {
    AtListDeleteWithObjectHandler(extractors, AtObjectDelete);
    }

static eAtRet AllExtractorsCreate(ThaModuleClock self)
    {
    AtList extractors;
    uint8 numExtractors = AtModuleClockNumExtractors((AtModuleClock)self);
    uint8 i;

    if (numExtractors == 0)
        return cAtOk;

    if (self->extractors)
        AllExtractorsDelete(self->extractors);

    /* Create list of extractors */
    extractors = AtListCreate(numExtractors);
    if (extractors == NULL)
        return cAtErrorRsrcNoAvail;

    /* Create all extractors */
    for (i = 0; i < numExtractors; i++)
        {
        AtClockExtractor extractor = mMethodsGet(self)->ExtractorCreate(self, i);
        eAtRet ret = cAtOk;

        if (extractor)
            ret = AtListObjectAdd(extractors, (AtObject)extractor);

        if ((extractor == NULL) || (ret != cAtOk))
            {
            AllExtractorsDelete(extractors);
            return cAtErrorRsrcNoAvail;
            }
        }

    /* Cache this */
    self->extractors = extractors;

    return cAtOk;
    }

static eAtRet Setup(AtModule self)
    {
    /* Let super do first */
    eAtRet ret = m_AtModuleMethods->Setup(self);
    if (ret != cAtOk)
        return ret;

    /* Create clock monitor */
    if (mThis(self)->clockMonitor == NULL)
        mThis(self)->clockMonitor = mMethodsGet(mThis(self))->ClockMonitorCreate(mThis(self));

    return AllExtractorsCreate(mThis(self));
    }

static void Delete(AtObject self)
    {
    AllExtractorsDelete(mThis(self)->extractors);
    mThis(self)->extractors = NULL;
    AtObjectDelete((AtObject)mThis(self)->clockMonitor);
    mThis(self)->clockMonitor = NULL;
    m_AtObjectMethods->Delete(self);
    }

static AtClockExtractor ExtractorGet(AtModuleClock self, uint8 extractorId)
    {
    return (AtClockExtractor)AtListObjectGet(mThis(self)->extractors, extractorId);
    }

static uint8 MaxNumExternalClockSources(ThaModuleClock self)
    {
	AtUnused(self);
    return 2;
    }

static uint32 MonitorBaseAddress(ThaModuleClock self)
    {
    AtUnused(self);
    return 0xF00000;
    }

static eBool ClockOutputIdIsValid(ThaModuleClock self, uint8 clockOutputId)
    {
    return (clockOutputId < ThaModuleClockMonitorNumOutputs(self)) ? cAtTrue : cAtFalse;
    }

static eAtRet MonitorOutputSet(ThaModuleClock self, uint8 clockOutputId, eThaClockMonitorOutput refClockSource)
    {
    uint32 regVal, regAddr;
    uint16 hwRefClockSource;

    if (!ClockOutputIdIsValid(self, clockOutputId))
        return cAtErrorInvlParm;

    if (!mMethodsGet(self)->MonitorOutputIsSupported(self, refClockSource))
        return cAtErrorModeNotSupport;

    regAddr = cAf6Reg_clock_monitoring_output_Base + MonitorBaseAddress(self);
    regVal = mModuleHwRead(self, regAddr);
    hwRefClockSource = mMethodsGet(self)->MonitorOutputSw2Hw(self, refClockSource);
    mFieldIns(&regVal,
              cAf6_clock_monitoring_output_cfgrefpid_Mask(clockOutputId),
              cAf6_clock_monitoring_output_cfgrefpid_Shift(clockOutputId),
              hwRefClockSource);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eThaClockMonitorOutput MonitorOutputGet(ThaModuleClock self, uint8 clockOutputId)
    {
    uint32 regVal, regAddr;
    uint16 refClockSource;

    if (!ClockOutputIdIsValid(self, clockOutputId))
        return cThaClockMonitorOutputNone;

    regAddr = cAf6Reg_clock_monitoring_output_Base + MonitorBaseAddress(self);
    regVal = mModuleHwRead(self, regAddr);
    mFieldGet(regVal,
              cAf6_clock_monitoring_output_cfgrefpid_Mask(clockOutputId),
              cAf6_clock_monitoring_output_cfgrefpid_Shift(clockOutputId),
              uint16,
              &refClockSource);
    return mMethodsGet(self)->MonitorOutputHw2Sw(self, refClockSource);
    }

static uint8 MonitorOutputNumGet(ThaModuleClock self)
    {
    AtUnused(self);
    return 0x0;
    }

static uint16 MonitorOutputSw2Hw(ThaModuleClock self, eThaClockMonitorOutput refClockSource)
    {
    AtUnused(self);
    AtUnused(refClockSource);
    return 0xFFFF;
    }

static eThaClockMonitorOutput MonitorOutputHw2Sw(ThaModuleClock self, uint16 refClockSource)
    {
    AtUnused(self);
    AtUnused(refClockSource);
    return cThaClockMonitorOutputNone;
    }

static eBool MonitorOutputIsSupported(ThaModuleClock self, eThaClockMonitorOutput refClockSource)
    {
    AtUnused(self);
    AtUnused(refClockSource);
    return cAtFalse;
    }

static eAtModuleClockRet ClockExtractorInit(ThaModuleClock self, AtClockExtractor extractor)
    {
	AtUnused(extractor);
	AtUnused(self);
    /* Let concrete class determine */
    return cAtOk;
    }

static eAtRet Init(AtModule self)
    {
    uint8 output_i;
    eAtRet ret = cAtOk;

    for (output_i = 0; output_i < AtModuleClockNumExtractors((AtModuleClock)self); output_i++)
        {
        AtClockExtractor extractor = AtModuleClockExtractorGet((AtModuleClock)self, output_i);
        ret |= mMethodsGet(mThis(self))->ClockExtractorInit(mThis(self), extractor);
        }

    if (ret != cAtOk)
        return ret;

    for (output_i = 0; output_i < ThaModuleClockMonitorNumOutputs((ThaModuleClock)self); output_i++)
        ret |= ThaModuleClockMonitorOutputSet((ThaModuleClock)self, output_i, cThaClockMonitorOutputNone);

    return ret;
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static uint32 CorePllCheck(AtModuleClock self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    uint8 core_i;

    for (core_i = 0; core_i < AtDeviceNumIpCoresGet(device); core_i++)
        {
        AtIpCore core = AtDeviceIpCoreGet(device, core_i);
        if (!ThaIpCorePllIsLocked(core))
            return cThaClockStatusCorePllFail;
        }

    return 0;
    }

static uint32 AllClockCheck(AtModuleClock self)
    {
    ThaClockMonitor clockMonitor = ThaModuleClockMonitorGet((ThaModuleClock)self);
    uint32 clockStatus = 0;

    if (clockMonitor == NULL)
        return CorePllCheck(self);

    clockStatus |= ThaClockMonitorSystemClockCheck(clockMonitor);
    clockStatus |= ThaClockMonitorLocalBusClockCheck(clockMonitor);
    clockStatus |= ThaClockMonitorGeClockCheck(clockMonitor);
    clockStatus |= ThaClockMonitorDdrClockCheck(clockMonitor);
    clockStatus |= ThaClockMonitorOcnClockCheck(clockMonitor);

    return clockStatus;
    }

static ThaClockMonitor ClockMonitorCreate(ThaModuleClock self)
    {
	AtUnused(self);
    /* Let sub class do */
    return NULL;
    }

static eBool ShouldEnableCdrInterrupt(ThaModuleClock self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void InterruptHwEnable(AtIpCore ipCore, eBool enable)
    {
    ThaIpCoreClockHwInterruptEnable((ThaIpCore)ipCore, enable);
    }

static AtModule CdrModule(AtModule self)
    {
    return AtDeviceModuleGet(AtModuleDeviceGet(self), cThaModuleCdr);
    }

static eAtRet InterruptEnable(AtModule self, eBool enable)
    {
    uint8 i;
    AtDevice device = AtModuleDeviceGet(self);
    uint8 numCores = AtDeviceNumIpCoresGet(device);

    for (i = 0; i < numCores; i++)
        {
        AtIpCore ipCore = AtDeviceIpCoreGet(device, i);

        /* If a product supports interrupt pin enable register, just simple
         * enable module interrupt on IPcore. Otherwise, we need to check if
         * IPcore interrupt was enabled before enable module interrupt. */
        if (ThaIpCoreCanEnableHwInterruptPin((ThaIpCore)ipCore) || AtIpCoreInterruptIsEnabled(ipCore))
            {
            InterruptHwEnable(ipCore, enable);

            if (mMethodsGet(mThis(self))->ShouldEnableCdrInterrupt(mThis(self)))
                {
                AtModule cdrModule = CdrModule(self);
                if (cdrModule)
                    mMethodsGet(cdrModule)->InterruptOnIpCoreEnable(cdrModule, enable, ipCore);
                }
            }

        mThis(self)->interruptEnabled = enable;
        }

    return cAtOk;
    }

static eBool InterruptIsEnabled(AtModule self)
    {
    return mThis(self)->interruptEnabled;
    }

static void InterruptOnIpCoreEnable(AtModule self, eBool enable, AtIpCore ipCore)
    {
    if (!InterruptIsEnabled(self))
        return;

    InterruptHwEnable(ipCore, enable);

    if (mMethodsGet(mThis(self))->ShouldEnableCdrInterrupt(mThis(self)))
        {
        AtModule cdrModule = CdrModule(self);
        mMethodsGet(cdrModule)->InterruptOnIpCoreEnable(cdrModule, enable, ipCore);
        }
    }

static void InterruptProcess(AtModule self, uint32 glbIntr, AtIpCore ipCore)
    {
    if (mMethodsGet(mThis(self))->ShouldEnableCdrInterrupt(mThis(self)))
        {
        AtModule cdrModule = CdrModule(self);
        mMethodsGet(cdrModule)->InterruptProcess(cdrModule, glbIntr, ipCore);
        }
    }

static eAtRet Debug(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Debug(self);

    if (mThis(self)->clockMonitor)
        ThaClockMonitorDebug(mThis(self)->clockMonitor);

    return ret;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaModuleClock object = (ThaModuleClock)self;
    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeList(extractors);
    mEncodeObject(clockMonitor);
    mEncodeUInt(interruptEnabled);
    }

static uint32 RefIdMask(ThaModuleClock self, AtClockExtractor extractor)
    {
    AtUnused(self);
    AtUnused(extractor);
    return 0;
    }

static uint32 RefIdShift(ThaModuleClock self, AtClockExtractor extractor)
    {
    AtUnused(self);
    AtUnused(extractor);
    return 0;
    }

static uint32 SquelchingHwAddress(ThaModuleClock self, AtClockExtractor extractor)
    {
    AtUnused(self);
    AtUnused(extractor);
    return cBit31_0;
    }

static uint32 SquelchingHwMask(ThaModuleClock self, AtClockExtractor extractor)
    {
    AtUnused(self);
    AtUnused(extractor);
    return 0;
    }

static uint32 SquelchingHwShift(ThaModuleClock self, AtClockExtractor extractor)
    {
    AtUnused(self);
    AtUnused(extractor);
    return 0;
    }

static uint32 OutputCounterHwAddress(ThaModuleClock self, AtClockExtractor extractor)
    {
    AtUnused(self);
    AtUnused(extractor);
    return cBit31_0;
    }

static uint32 OutputCounterHwMask(ThaModuleClock self, AtClockExtractor extractor)
    {
    AtUnused(self);
    AtUnused(extractor);
    return 0;
    }

static uint32 OutputCounterHwShift(ThaModuleClock self, AtClockExtractor extractor)
    {
    AtUnused(self);
    AtUnused(extractor);
    return cBit31_0;
    }

static eBool SquelchingIsSupported(ThaModuleClock self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 SquelchingSwToHwMask(eAtClockExtractorSquelching options)
    {
    uint32 mask = 0;

    if (options == cAtClockExtractorSquelchingNone)
        return cBit2_0;

    if (options & cAtClockExtractorSquelchingLos)
        mask |= cBit0;
    if (options & cAtClockExtractorSquelchingLof)
        mask |= cBit1;
    if (options & cAtClockExtractorSquelchingAis)
        mask |= cBit2;

    return ((~mask) & cBit2_0);
    }

static uint32 SquelchingHwToSwMask(uint32 options)
    {
    uint32 mask = 0;

    options = (~options) & cBit2_0;

    if (options == 0)
        return cAtClockExtractorSquelchingNone;

    if (options & cBit0)
        mask |= cAtClockExtractorSquelchingLos;
    if (options & cBit1)
        mask |= cAtClockExtractorSquelchingLof;
    if (options & cBit2)
        mask |= cAtClockExtractorSquelchingAis;

    return mask;
    }

static eAtModuleClockRet HwSquelchingOptionSet(ThaModuleClock self, AtClockExtractor extractor, eAtClockExtractorSquelching options)
    {
    uint32 regOptions = SquelchingSwToHwMask(options);
    uint32 address = ThaModuleClockSquelchingHwAddress(self, extractor);
    uint32 regMask = ThaModuleClockSquelchingHwMask(self, extractor);
    uint32 regShift = ThaModuleClockSquelchingHwShift(self, extractor);
    uint32 regVal = AtClockExtractorRead(extractor, address);

    mFieldIns(&regVal, regMask, regShift, regOptions);
    AtClockExtractorWrite(extractor, address, regVal);
    return cAtOk;
    }

static uint32 HwSquelchingOptionGet(ThaModuleClock self, AtClockExtractor extractor)
    {
    uint32 address = ThaModuleClockSquelchingHwAddress(self, extractor);
    uint32 regMask = ThaModuleClockSquelchingHwMask(self, extractor);
    uint32 regShift = ThaModuleClockSquelchingHwShift(self, extractor);
    uint32 regVal = AtClockExtractorRead(extractor, address);
    uint32 regOptions;

    mFieldGet(regVal, regMask, regShift, uint32, &regOptions);
    return SquelchingHwToSwMask(regOptions);
    }

static uint32 HwOutputCounterGet(ThaModuleClock self, AtClockExtractor extractor)
    {
    uint32 address = ThaModuleClockOutputCounterHwAddress(self, extractor);
    uint32 regMask = ThaModuleClockOutputCounterHwMask(self, extractor);
    uint32 regShift = ThaModuleClockOutputCounterHwShift(self, extractor);
    uint32 regVal = AtClockExtractorRead(extractor, address);
    uint32 counter;

    mFieldGet(regVal, regMask, regShift, uint32, &counter);
    return counter;
    }

static eAtModuleClockRet SquelchingOptionSet(ThaModuleClock self, AtClockExtractor extractor, eAtClockExtractorSquelching options)
    {
    if (ThaModuleClockSquelchingIsSupported(self))
        return mMethodsGet(self)->HwSquelchingOptionSet(self, extractor, options);

    return cAtErrorModeNotSupport;
    }

static uint32 SquelchingOptionGet(ThaModuleClock self, AtClockExtractor extractor)
    {
    if (ThaModuleClockSquelchingIsSupported(self))
        return mMethodsGet(self)->HwSquelchingOptionGet(self, extractor);

    return cAtClockExtractorSquelchingNone;
    }

static uint32 OutputCounterGet(ThaModuleClock self, AtClockExtractor extractor)
    {
    if (ThaModuleClockSquelchingIsSupported(self))
        return mMethodsGet(self)->HwOutputCounterGet(self, extractor);

    return 0;
    }

static AtClockMonitor EthPortClockMonitorCreate(ThaModuleClock self, AtEthPort port)
    {
    AtUnused(self);
    AtUnused(port);
    return NULL;
    }

static void MethodsInit(ThaModuleClock self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Clock extractor */
        mMethodOverride(m_methods, ExtractorCreate);
        mMethodOverride(m_methods, ExtractorPart);
        mMethodOverride(m_methods, MaxNumExternalClockSources);
        mMethodOverride(m_methods, ClockExtractorInit);
        mMethodOverride(m_methods, MonitorOutputSet);
        mMethodOverride(m_methods, MonitorOutputGet);
        mMethodOverride(m_methods, MonitorOutputNumGet);
        mMethodOverride(m_methods, MonitorOutputSw2Hw);
        mMethodOverride(m_methods, MonitorOutputHw2Sw);
        mMethodOverride(m_methods, MonitorOutputIsSupported);
        mMethodOverride(m_methods, RefIdMask);
        mMethodOverride(m_methods, RefIdShift);
        mMethodOverride(m_methods, SquelchingHwAddress);
        mMethodOverride(m_methods, SquelchingHwMask);
        mMethodOverride(m_methods, SquelchingHwShift);
        mMethodOverride(m_methods, OutputCounterHwAddress);
        mMethodOverride(m_methods, OutputCounterHwMask);
        mMethodOverride(m_methods, OutputCounterHwShift);
        mMethodOverride(m_methods, SquelchingIsSupported);
        mMethodOverride(m_methods, HwSquelchingOptionSet);
        mMethodOverride(m_methods, HwSquelchingOptionGet);
        mMethodOverride(m_methods, HwOutputCounterGet);

        /* Clock checking */
        mMethodOverride(m_methods, ClockMonitorCreate);
        mMethodOverride(m_methods, EthPortClockMonitorCreate);

        /* Interrupt */
        mMethodOverride(m_methods, ShouldEnableCdrInterrupt);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtObject(AtModuleClock self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModuleClock(AtModuleClock self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleClockOverride, mMethodsGet(self), sizeof(m_AtModuleClockOverride));

        mMethodOverride(m_AtModuleClockOverride, ExtractorGet);
        mMethodOverride(m_AtModuleClockOverride, AllClockCheck);
        }

    mMethodsSet(self, &m_AtModuleClockOverride);
    }

static void OverrideAtModule(AtModuleClock self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Setup);
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, Debug);
        mMethodOverride(m_AtModuleOverride, InterruptEnable);
        mMethodOverride(m_AtModuleOverride, InterruptIsEnabled);
        mMethodOverride(m_AtModuleOverride, InterruptOnIpCoreEnable);
        mMethodOverride(m_AtModuleOverride, InterruptProcess);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void Override(AtModuleClock self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    OverrideAtModuleClock(self);
    }

AtModuleClock ThaModuleClockObjectInit(AtModuleClock self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleClockObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

uint8 ThaModuleClockExtractorPart(ThaModuleClock self, uint8 extractorId)
    {
    if (self)
        return mMethodsGet(self)->ExtractorPart(self, extractorId);
    return 0;
    }

uint32 ThaModuleClockPartOffset(ThaModuleClock self, uint8 partId)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    return ThaDeviceModulePartOffset(device, cAtModuleClock, partId);
    }

uint8 ThaModuleClockMaxNumExternalClockSources(ThaModuleClock self)
    {
    if (self)
        return mMethodsGet(self)->MaxNumExternalClockSources(self);
    return 0;
    }

ThaClockMonitor ThaModuleClockMonitorGet(ThaModuleClock self)
    {
    return self->clockMonitor;
    }

eAtRet ThaModuleClockMonitorOutputSet(ThaModuleClock self, uint8 clockOutputId, eThaClockMonitorOutput refClockSource)
    {
    if (self)
        return mMethodsGet(self)->MonitorOutputSet(self, clockOutputId, refClockSource);
    return cAtErrorNullPointer;
    }

eThaClockMonitorOutput ThaModuleClockMonitorOutputGet(ThaModuleClock self, uint8 clockOutputId)
    {
    if (self)
        return mMethodsGet(self)->MonitorOutputGet(self, clockOutputId);
    return cThaClockMonitorOutputNone;
    }

uint8 ThaModuleClockMonitorNumOutputs(ThaModuleClock self)
    {
    if (self)
        return mMethodsGet(self)->MonitorOutputNumGet(self);
    return 0x0;
    }

uint32 ThaModuleClockSquelchingHwAddress(ThaModuleClock self, AtClockExtractor extractor)
    {
    if (self)
        return mMethodsGet(self)->SquelchingHwAddress(self, extractor);
    return cBit31_0;
    }

uint32 ThaModuleClockSquelchingHwMask(ThaModuleClock self, AtClockExtractor extractor)
    {
    if (self)
        return mMethodsGet(self)->SquelchingHwMask(self, extractor);

    return 0;
    }

uint32 ThaModuleClockSquelchingHwShift(ThaModuleClock self, AtClockExtractor extractor)
    {
    if (self)
        return mMethodsGet(self)->SquelchingHwShift(self, extractor);
    return 0;
    }

uint32 ThaModuleClockOutputCounterHwAddress(ThaModuleClock self, AtClockExtractor extractor)
    {
    if (self)
        return mMethodsGet(self)->OutputCounterHwAddress(self, extractor);
    return cBit31_0;
    }

uint32 ThaModuleClockOutputCounterHwMask(ThaModuleClock self, AtClockExtractor extractor)
    {
    if (self)
        return mMethodsGet(self)->OutputCounterHwMask(self, extractor);

    return 0;
    }

uint32 ThaModuleClockOutputCounterHwShift(ThaModuleClock self, AtClockExtractor extractor)
    {
    if (self)
        return mMethodsGet(self)->OutputCounterHwShift(self, extractor);
    return 0;
    }

eBool ThaModuleClockSquelchingIsSupported(ThaModuleClock self)
    {
    if (self)
        return mMethodsGet(self)->SquelchingIsSupported(self);

    return cAtFalse;
    }

eAtModuleClockRet ThaModuleClockSquelchingOptionSet(ThaModuleClock self, AtClockExtractor extractor, eAtClockExtractorSquelching options)
    {
    return SquelchingOptionSet(self, extractor, options);
    }

uint32 ThaModuleClockSquelchingOptionGet(ThaModuleClock self, AtClockExtractor extractor)
    {
    return SquelchingOptionGet(self, extractor);
    }

uint32 ThaModuleClockOutputCounterGet(ThaModuleClock self, AtClockExtractor extractor)
    {
    return OutputCounterGet(self, extractor);
    }

AtClockMonitor ThaModuleClockEthPortClockMonitorCreate(ThaModuleClock self, AtEthPort port)
    {
    if (self)
        return mMethodsGet(self)->EthPortClockMonitorCreate(self, port);
    return NULL;
    }

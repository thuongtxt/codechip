/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Clock
 * 
 * File        : ThaModuleClock.h
 * 
 * Created Date: Aug 27, 2013
 *
 * Description : Clock module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULECLOCK_H_
#define _THAMODULECLOCK_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleClock.h" /* Super class */
#include "ThaClockMonitor.h"
#include "AtClockExtractor.h"
#include "AtClockMonitor.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
#define cThaClockStatusSystemClockFail        cBit0
#define cThaClockStatusCorePllFail            cBit1
#define cThaClockStatusLocalBusClockFail      cBit2
#define cThaClockStatusGeClockFail            cBit3
#define cThaClockStatusOcnClockFail(clockId) (cBit4 << (clockId))
#define cThaClockStatusDdrClockFail(ddrId)   (cBit16 << (ddrId))
#define cThaClockStatusReferenceClockFail(referenceId) (cBit20 << (referenceId))

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleClock * ThaModuleClock;

/*--------------------------- Forward declarations ---------------------------*/
typedef enum eThaClockMonitorOutput
    {
    cThaClockMonitorOutputNone,
    cThaClockMonitorOutputSpareSerdesDiv2,  /**< Monitor clock output of Spare SERDES reference clock source divide 2 */
    cThaClockMonitorOutputOverheadDiv2,     /**< Monitor clock output of Overhead reference clock source divide 2 */
    cThaClockMonitorOutputXfi155_52M_0Div2, /**< Monitor clock output of XFI serdes 155.52M reference clock source#0 divide 2 */
    cThaClockMonitorOutputXfi155_52M_1Div2, /**< Monitor clock output of XFI serdes 155.52M reference clock source#1 divide 2 */
    cThaClockMonitorOutputXfi156_25M_0Div2, /**< Monitor clock output of XFI serdes 156.25M reference clock source#0 divide 2 */
    cThaClockMonitorOutputXfi156_25M_1Div2, /**< Monitor clock output of XFI serdes 156.25M reference clock source#1 divide 2 */
    cThaClockMonitorOutputEth40G_0Div2,     /**< Monitor clock output of ETH 40Ghz reference clock source#0 divide 2 */
    cThaClockMonitorOutputEth40G_1Div2,     /**< Monitor clock output of ETH 40Ghz reference clock source#1 divide 2 */
    cThaClockMonitorOutputBaseX2500Div2,    /**< Monitor clock output of BaseX2500 reference clock source divide 2 */
    cThaClockMonitorOutputQdrDiv2,          /**< Monitor clock output of QDR reference clock source divide 2 */
    cThaClockMonitorOutputDdr_1Div2,        /**< Monitor clock output of DDR reference clock source#1 divide 2 */
    cThaClockMonitorOutputDdr_2Div2,        /**< Monitor clock output of DDR reference clock source#2 divide 2 */
    cThaClockMonitorOutputDdr_3Div2,        /**< Monitor clock output of DDR reference clock source#3 divide 2 */
    cThaClockMonitorOutputDdr_4Div2,        /**< Monitor clock output of DDR reference clock source#4 divide 2 */
    cThaClockMonitorOutputPcieDiv2,         /**< Monitor clock output of PCIE reference clock source divide 2 */
    cThaClockMonitorOutputPrc,              /**< Monitor clock output of PRC reference clock source */
    cThaClockMonitorOutputExt,              /**< Monitor clock output of External reference clock source */
    cThaClockMonitorOutputSystem            /**< Monitor clock output of System PLL 311.04Mhz reference clock source */
    }eThaClockMonitorOutput;

/*--------------------------- Entries ----------------------------------------*/
AtClockMonitor ThaModuleClockEthPortClockMonitorCreate(ThaModuleClock self, AtEthPort port);

/* Concretes */
AtModuleClock ThaStmMlpppModuleClockNew(AtDevice device);
AtModuleClock ThaPdhMlpppModuleClockNew(AtDevice device);

uint8 ThaModuleClockExtractorPart(ThaModuleClock self, uint8 extractorId);
uint32 ThaModuleClockPartOffset(ThaModuleClock self, uint8 partId);
uint8 ThaModuleClockMaxNumExternalClockSources(ThaModuleClock self);
eAtRet ThaModuleClockMonitorOutputSet(ThaModuleClock self, uint8 clockOutputId, eThaClockMonitorOutput refClockSource);
eThaClockMonitorOutput ThaModuleClockMonitorOutputGet(ThaModuleClock self, uint8 clockOutputId);
uint8 ThaModuleClockMonitorNumOutputs(ThaModuleClock self);

ThaClockMonitor ThaModuleClockMonitorGet(ThaModuleClock self);
uint32 ThaModuleClockSquelchingHwAddress(ThaModuleClock self, AtClockExtractor extractor);
uint32 ThaModuleClockSquelchingHwMask(ThaModuleClock self, AtClockExtractor extractor);
uint32 ThaModuleClockSquelchingHwShift(ThaModuleClock self, AtClockExtractor extractor);
uint32 ThaModuleClockOutputCounterHwAddress(ThaModuleClock self, AtClockExtractor extractor);
uint32 ThaModuleClockOutputCounterHwMask(ThaModuleClock self, AtClockExtractor extractor);
uint32 ThaModuleClockOutputCounterHwShift(ThaModuleClock self, AtClockExtractor extractor);
eBool ThaModuleClockSquelchingIsSupported(ThaModuleClock self);
eAtModuleClockRet ThaModuleClockSquelchingOptionSet(ThaModuleClock self, AtClockExtractor extractor, eAtClockExtractorSquelching options);
uint32 ThaModuleClockSquelchingOptionGet(ThaModuleClock self, AtClockExtractor extractor);
uint32 ThaModuleClockOutputCounterGet(ThaModuleClock self, AtClockExtractor extractor);

/* Product concretes */
AtModuleClock Tha60030081ModuleClockNew(AtDevice device);
AtModuleClock Tha60031021ModuleClockNew(AtDevice device);
AtModuleClock Tha60070041ModuleClockNew(AtDevice device);
AtModuleClock Tha60091023ModuleClockNew(AtDevice device);
AtModuleClock Tha60031031EpModuleClockNew(AtDevice device);
AtModuleClock Tha60031031ModuleClockNew(AtDevice device);
AtModuleClock Tha60031032ModuleClockNew(AtDevice device);
AtModuleClock Tha60091132ModuleClockNew(AtDevice device);
AtModuleClock Tha60031131ModuleClockNew(AtDevice device);
AtModuleClock Tha60031033ModuleClockNew(AtDevice device);
AtModuleClock Tha60030101ModuleClockNew(AtDevice device);
AtModuleClock Tha60210011ModuleClockNew(AtDevice device);
AtModuleClock ThaStmPwProductModuleClockNew(AtDevice device);
AtModuleClock ThaPdhPwProductModuleClockNew(AtDevice device);
AtModuleClock Tha60290021ModuleClockNew(AtDevice device);
AtModuleClock Tha60290011ModuleClockNew(AtDevice device);
AtModuleClock Tha60290022ModuleClockNew(AtDevice device);
AtModuleClock Tha60290051ModuleClockNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULECLOCK_H_ */


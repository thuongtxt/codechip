/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Clock
 * 
 * File        : ThaClockMonitor.h
 * 
 * Created Date: Nov 22, 2013
 *
 * Description : Clock monitor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THACLOCKMONITOR_H_
#define _THACLOCKMONITOR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"
#include "AtObject.h" /* Super class */
#include "AtModuleClock.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaClockMonitor * ThaClockMonitor;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 ThaClockMonitorDdrClockCheck(ThaClockMonitor self);
uint32 ThaClockMonitorSystemClockCheck(ThaClockMonitor self);
uint32 ThaClockMonitorOcnClockCheck(ThaClockMonitor self);
uint32 ThaClockMonitorLocalBusClockCheck(ThaClockMonitor self);
uint32 ThaClockMonitorGeClockCheck(ThaClockMonitor self);
uint32 ThaClockMonitorReferenceClock1Check(ThaClockMonitor self);
uint32 ThaClockMonitorReferenceClock2Check(ThaClockMonitor self);

/* For debugging */
void ThaClockMonitorDebug(ThaClockMonitor self);

/* Product concretes */
ThaClockMonitor Tha60031031ClockMonitorNew(AtModuleClock clockModule);
ThaClockMonitor Tha60091023ClockMonitorNew(AtModuleClock clockModule);
ThaClockMonitor Tha60091132ClockMonitorNew(AtModuleClock clockModule);
ThaClockMonitor Tha60030101ClockMonitorNew(AtModuleClock clockModule);
ThaClockMonitor ThaStmPwProductClockMonitorNew(AtModuleClock clockModule);

#ifdef __cplusplus
}
#endif
#endif /* _THACLOCKMONITOR_H_ */


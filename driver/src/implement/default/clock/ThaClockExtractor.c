/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Clock
 *
 * File        : ThaClockExtractor.c
 *
 * Created Date: Aug 27, 2013
 *
 * Description : Clock extractor
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../util/coder/AtCoderUtil.h"
#include "../pdh/ThaPdhDe1.h"
#include "../pdh/ThaPdhDe3.h"
#include "ThaClockExtractorInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaClockExtractor)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint32 m_methodsInit = 0;
static tThaClockExtractorMethods m_methods;

/* Override */
static tAtObjectMethods         m_AtObjectOverride;
static tAtClockExtractorMethods m_AtClockExtractorOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaClockExtractor object = (ThaClockExtractor)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeChannelIdString(de1);
    mEncodeUInt(alwaysExtractFromLiu);
    mEncodeChannelIdString(line);
    mEncodeChannelIdString(serdes);
    mEncodeChannelIdString(de3);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaClockExtractor);
    }

static uint8 LocalId(ThaClockExtractor self)
    {
    uint8 numExtractorsPerPart, numParts;
    AtClockExtractor extractor = (AtClockExtractor)self;
    AtModuleClock clockModule  = AtClockExtractorModuleGet(extractor);
    ThaDevice device           = (ThaDevice)AtModuleDeviceGet((AtModule)clockModule);

    numParts = ThaDeviceNumPartsOfModule(device, cAtModuleClock);
    if (numParts == 0)
        return 0;

    numExtractorsPerPart = AtModuleClockNumExtractors(clockModule) / numParts;
    if (numExtractorsPerPart == 0)
        return 0;

    return AtClockExtractorIdGet(extractor) % numExtractorsPerPart;
    }

static uint32 PartOffset(ThaClockExtractor self)
    {
    ThaModuleClock clockModule = (ThaModuleClock)AtClockExtractorModuleGet((AtClockExtractor)self);
    return ThaModuleClockPartOffset(clockModule, ThaClockExtractorPartId(self));
    }

static eAtRet ExtractSourceReset(ThaClockExtractor self)
    {
    self->line = NULL;
    self->serdes = NULL;

    if (self->de3)
        {
        ThaPdhDe3ClockExtractorSet((ThaPdhDe3)(self->de3), NULL);
        self->de3 = NULL;
        }

    if (self->de1)
        {
        ThaPdhDe1ClockExtractorSet((ThaPdhDe1)(self->de1), NULL);
        self->de1  = NULL;
        }

    self->alwaysExtractFromLiu = cAtFalse;

    return cAtOk;
    }

static eAtModuleClockRet De1AcrDcrTimingModeExtract(ThaClockExtractor self, AtPdhDe1 de1)
    {
	AtUnused(de1);
	AtUnused(self);
    return cAtErrorNotImplemented;
    }

static eAtModuleClockRet De1LoopTimingModeExtract(ThaClockExtractor self, AtPdhDe1 de1)
    {
	AtUnused(de1);
	AtUnused(self);
    return cAtErrorNotImplemented;
    }

static eAtModuleClockRet HwPdhDe1ClockExtract(ThaClockExtractor self, AtPdhDe1 de1)
    {
    eAtTimingMode timingMode = AtChannelTimingModeGet((AtChannel)de1);

    if (timingMode == cAtTimingModeSys)
        return mMethodsGet(mThis(self))->HwSystemClockExtract(mThis(self));

    if (timingMode == cAtTimingModeSdhSys)
        return mMethodsGet(mThis(self))->HwSdhSystemClockExtract(mThis(self));

    if (timingMode == cAtTimingModeSdhLineRef)
        {
        AtSdhLine line = (AtSdhLine)AtChannelTimingSourceGet((AtChannel)de1);
        return mMethodsGet(mThis(self))->HwSdhLineClockExtract(mThis(self), line);
        }

    if ((timingMode == cAtTimingModeSlave) || (timingMode == cAtTimingModeLoop))
        return mMethodsGet(mThis(self))->De1LoopTimingModeExtract(mThis(self), de1);

    if (timingMode == cAtTimingModeExt1Ref)
        return mMethodsGet(mThis(self))->HwExternalClockExtract(mThis(self), 0);
    if (timingMode == cAtTimingModeExt2Ref)
        return mMethodsGet(mThis(self))->HwExternalClockExtract(mThis(self), 1);

    if ((timingMode == cAtTimingModeAcr) || (timingMode == cAtTimingModeDcr))
        return mMethodsGet(mThis(self))->De1AcrDcrTimingModeExtract(mThis(self), de1);

    return cAtErrorModeNotSupport;
    }

static eAtModuleClockRet PdhDe1ClockExtract(AtClockExtractor self, AtPdhDe1 de1)
    {
    eAtRet ret = cAtOk;

    if (!mMethodsGet(mThis(self))->CanExtractPdhDe1Clock(mThis(self), de1))
        return cAtErrorModeNotSupport;

    ret = mMethodsGet(mThis(self))->HwPdhDe1ClockExtract(mThis(self), de1);
    if (ret == cAtOk)
        {
        mMethodsGet(mThis(self))->ExtractSourceReset(mThis(self));
        mThis(self)->de1 = de1;
        ThaPdhDe1ClockExtractorSet((ThaPdhDe1)de1, self);
        }

    return ret;
    }

static eAtModuleClockRet HwPdhDe1LiuClockExtract(ThaClockExtractor self, AtPdhDe1 de1)
    {
	AtUnused(de1);
	AtUnused(self);
    return cAtErrorNotImplemented;
    }

static eAtModuleClockRet PdhDe1LiuClockExtract(AtClockExtractor self, AtPdhDe1 de1)
    {
    eAtRet ret = cAtOk;

    if (!mMethodsGet(mThis(self))->CanExtractPdhDe1LiuClock(mThis(self), de1))
        return cAtErrorModeNotSupport;

    ret = mMethodsGet(mThis(self))->HwPdhDe1LiuClockExtract(mThis(self), de1);
    if (ret == cAtOk)
        {
        mMethodsGet(mThis(self))->ExtractSourceReset(mThis(self));
        mThis(self)->de1 = de1;
        mThis(self)->alwaysExtractFromLiu = cAtTrue;
        ThaPdhDe1ClockExtractorSet((ThaPdhDe1)de1, self);
        }

    return ret;
    }

static eBool PdhDe1LiuClockIsExtracted(AtClockExtractor self)
    {
    if (mThis(self)->de1 != NULL)
        return mThis(self)->alwaysExtractFromLiu;
    return cAtFalse;
    }

static AtPdhDe1 PdhDe1Get(AtClockExtractor self)
    {
    return mThis(self)->de1;
    }

static eBool CanExtractSystemClock(ThaClockExtractor self)
    {
	AtUnused(self);
    return cAtTrue; /* It seems that almost products support this mode */
    }

static eBool CanExtractSdhSystemClock(ThaClockExtractor self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static eBool CanExtractSdhLineClock(ThaClockExtractor self, AtSdhLine line)
    {
	AtUnused(line);
	AtUnused(self);
    return cAtFalse;
    }

static eBool CanExtractExternalClock(ThaClockExtractor self, uint8 externalId)
    {
	AtUnused(externalId);
	AtUnused(self);
    return cAtFalse;
    }

static eBool CanExtractPdhDe1Clock(ThaClockExtractor self, AtPdhDe1 de1)
    {
	AtUnused(de1);
	AtUnused(self);
    return cAtFalse;
    }

static eBool CanExtractSerdesClock(ThaClockExtractor self, AtSerdesController serdes)
    {
    AtUnused(serdes);
    AtUnused(self);
    return cAtFalse;
    }

static eBool CanExtractPdhDe1LiuClock(ThaClockExtractor self, AtPdhDe1 de1)
    {
	AtUnused(de1);
	AtUnused(self);
    return cAtFalse;
    }

static eAtModuleClockRet HwSystemClockExtract(ThaClockExtractor self)
    {
	AtUnused(self);
    return cAtErrorNotImplemented;
    }

static eAtModuleClockRet SystemClockExtract(AtClockExtractor self)
    {
    eAtRet ret = cAtOk;

    if (!mMethodsGet(mThis(self))->CanExtractSystemClock(mThis(self)))
        return cAtErrorModeNotSupport;

    ret = mMethodsGet(mThis(self))->HwSystemClockExtract(mThis(self));
    if (ret == cAtOk)
        ret |= mMethodsGet(mThis(self))->ExtractSourceReset(mThis(self));

    return ret;
    }

static eAtModuleClockRet HwSdhSystemClockExtract(ThaClockExtractor self)
    {
	AtUnused(self);
    return cAtErrorNotImplemented;
    }

static eAtModuleClockRet SdhSystemClockExtract(AtClockExtractor self)
    {
    eAtRet ret = cAtOk;

    if (!mMethodsGet(mThis(self))->CanExtractSdhSystemClock(mThis(self)))
        return cAtErrorModeNotSupport;

    ret = mMethodsGet(mThis(self))->HwSdhSystemClockExtract(mThis(self));
    if (ret == cAtOk)
        ret |= mMethodsGet(mThis(self))->ExtractSourceReset(mThis(self));

    return ret;
    }

static eAtModuleClockRet HwSdhLineClockExtract(ThaClockExtractor self, AtSdhLine line)
    {
	AtUnused(line);
	AtUnused(self);
    return cAtErrorNotImplemented;
    }

static eAtModuleClockRet SdhLineClockExtract(AtClockExtractor self, AtSdhLine line)
    {
    eAtRet ret = cAtOk;

    if (!mMethodsGet(mThis(self))->CanExtractSdhLineClock(mThis(self), line))
        return cAtErrorModeNotSupport;

    ret = mMethodsGet(mThis(self))->HwSdhLineClockExtract(mThis(self), line);
    if (ret == cAtOk)
        {
        ret |= mMethodsGet(mThis(self))->ExtractSourceReset(mThis(self));
        mThis(self)->line = line;
        }

    return ret;
    }

static AtSdhLine SdhLineGet(AtClockExtractor self)
    {
    return mThis(self)->line;
    }

static eAtModuleClockRet HwExternalClockExtract(ThaClockExtractor self, uint8 externalId)
    {
	AtUnused(externalId);
	AtUnused(self);
    return cAtErrorNotImplemented;
    }

static eAtModuleClockRet ExternalClockExtract(AtClockExtractor self, uint8 externalId)
    {
    eAtRet ret = cAtOk;

    if (!mMethodsGet(mThis(self))->CanExtractExternalClock(mThis(self), externalId))
        return cAtErrorModeNotSupport;

    ret = mMethodsGet(mThis(self))->HwExternalClockExtract(mThis(self), externalId);
    if (ret == cAtOk)
        ret |= mMethodsGet(mThis(self))->ExtractSourceReset(mThis(self));

    return ret;
    }

static eBool ExternalIdIsValid(AtClockExtractor self, uint8 externalId)
    {
    ThaModuleClock clockModule = (ThaModuleClock)AtClockExtractorModuleGet(self);
    return (externalId >= ThaModuleClockMaxNumExternalClockSources(clockModule)) ? cAtFalse : cAtTrue;
    }

static eBool CanExtractPdhDe3Clock(ThaClockExtractor self, AtPdhDe3 de3)
    {
    AtUnused(de3);
    AtUnused(self);
    return cAtFalse;
    }

static eBool CanExtractPdhDe3LiuClock(ThaClockExtractor self, AtPdhDe3 de3)
    {
    AtUnused(de3);
    AtUnused(self);
    return cAtFalse;
    }

static eAtModuleClockRet HwPdhDe3ClockExtract(ThaClockExtractor self, AtPdhDe3 de3)
    {
    eAtTimingMode timingMode = AtChannelTimingModeGet((AtChannel)de3);

    if (timingMode == cAtTimingModeSys)
        return mMethodsGet(mThis(self))->HwSystemClockExtract(mThis(self));

    if (timingMode == cAtTimingModeSdhSys)
        return mMethodsGet(mThis(self))->HwSdhSystemClockExtract(mThis(self));

    if (timingMode == cAtTimingModeSdhLineRef)
        {
        AtSdhLine line = (AtSdhLine)AtChannelTimingSourceGet((AtChannel)de3);
        return mMethodsGet(mThis(self))->HwSdhLineClockExtract(mThis(self), line);
        }

    if ((timingMode == cAtTimingModeSlave) || (timingMode == cAtTimingModeLoop))
        return mMethodsGet(mThis(self))->De3LoopTimingModeExtract(mThis(self), de3);

    if (timingMode == cAtTimingModeExt1Ref)
        return mMethodsGet(mThis(self))->HwExternalClockExtract(mThis(self), 0);
    if (timingMode == cAtTimingModeExt2Ref)
        return mMethodsGet(mThis(self))->HwExternalClockExtract(mThis(self), 1);

    if ((timingMode == cAtTimingModeAcr) || (timingMode == cAtTimingModeDcr))
        return mMethodsGet(mThis(self))->De3AcrDcrTimingModeExtract(mThis(self), de3);

    return cAtErrorModeNotSupport;
    }

static eAtModuleClockRet De3AcrDcrTimingModeExtract(ThaClockExtractor self, AtPdhDe3 de3)
    {
    AtUnused(de3);
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static eAtModuleClockRet De3LoopTimingModeExtract(ThaClockExtractor self, AtPdhDe3 de3)
    {
    AtUnused(de3);
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static eAtModuleClockRet HwPdhDe3LiuClockExtract(ThaClockExtractor self, AtPdhDe3 de3)
    {
    AtUnused(de3);
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static eAtModuleClockRet PdhDe3ClockExtract(AtClockExtractor self, AtPdhDe3 de3)
    {
    eAtRet ret = cAtOk;

    if (!mMethodsGet(mThis(self))->CanExtractPdhDe3Clock(mThis(self), de3))
        return cAtErrorModeNotSupport;

    ret = mMethodsGet(mThis(self))->HwPdhDe3ClockExtract(mThis(self), de3);
    if (ret == cAtOk)
        {
        mMethodsGet(mThis(self))->ExtractSourceReset(mThis(self));
        mThis(self)->de3 = de3;
        ThaPdhDe3ClockExtractorSet((ThaPdhDe3)de3, self);
        }

    return ret;
    }

static eAtModuleClockRet PdhDe3LiuClockExtract(AtClockExtractor self, AtPdhDe3 de3)
    {
    eAtRet ret = cAtOk;

    if (!mMethodsGet(mThis(self))->CanExtractPdhDe3LiuClock(mThis(self), de3))
        return cAtErrorModeNotSupport;

    ret = mMethodsGet(mThis(self))->HwPdhDe3LiuClockExtract(mThis(self), de3);
    if (ret == cAtOk)
        {
        mMethodsGet(mThis(self))->ExtractSourceReset(mThis(self));
        mThis(self)->de3 = de3;
        mThis(self)->alwaysExtractFromLiu = cAtTrue;
        ThaPdhDe3ClockExtractorSet((ThaPdhDe3)de3, self);
        }

    return ret;
    }

static eBool PdhDe3LiuClockIsExtracted(AtClockExtractor self)
    {
    if (mThis(self)->de3 != NULL)
        return mThis(self)->alwaysExtractFromLiu;
    return cAtFalse;
    }

static AtPdhDe3 PdhDe3Get(AtClockExtractor self)
    {
    return mThis(self)->de3;
    }

static eAtModuleClockRet HwSerdesClockExtract(ThaClockExtractor self, AtSerdesController serdes)
    {
    AtUnused(self);
    AtUnused(serdes);
    return cAtErrorNotImplemented;
    }

static eAtModuleClockRet SerdesClockExtract(AtClockExtractor self, AtSerdesController serdes)
    {
    eAtRet ret = cAtOk;

    if (!mMethodsGet(mThis(self))->CanExtractSerdesClock(mThis(self), serdes))
        return cAtErrorModeNotSupport;

    ret = mMethodsGet(mThis(self))->HwSerdesClockExtract(mThis(self), serdes);
    if (ret == cAtOk)
        {
        mMethodsGet(mThis(self))->ExtractSourceReset(mThis(self));
        mThis(self)->serdes = serdes;
        }

    return ret;
    }

static AtSerdesController SerdesGet(AtClockExtractor self)
    {
    return mThis(self)->serdes;
    }

static ThaModuleClock ThaModuleClockGet(AtClockExtractor self)
    {
    return (ThaModuleClock)AtClockExtractorModuleGet(self);
    }

static eAtModuleClockRet SquelchingOptionSet(AtClockExtractor self, eAtClockExtractorSquelching options)
    {
    return ThaModuleClockSquelchingOptionSet(ThaModuleClockGet(self), self, options);
    }

static eAtClockExtractorSquelching SquelchingOptionGet(AtClockExtractor self)
    {
    return ThaModuleClockSquelchingOptionGet(ThaModuleClockGet(self), self);
    }

static uint32 OutputCounterGet(AtClockExtractor self)
    {
    return ThaModuleClockOutputCounterGet(ThaModuleClockGet(self), self);
    }

static void Debug(AtClockExtractor self)
    {
    if (AtClockExtractorSquelchingIsSupported(self))
        AtPrintf("Clock extractor #%d, output counter: %d Khz\r\n", AtClockExtractorIdGet(self), AtClockExtractorOutputCounterGet(self));
    }

static eBool SquelchingIsSupported(AtClockExtractor self)
    {
    AtModule module = (AtModule)AtClockExtractorModuleGet(self);

    return ThaModuleClockSquelchingIsSupported((ThaModuleClock)module);
    }

static void MethodsInit(ThaClockExtractor self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, CanExtractSystemClock);
        mMethodOverride(m_methods, CanExtractSdhSystemClock);
        mMethodOverride(m_methods, CanExtractSdhLineClock);
        mMethodOverride(m_methods, CanExtractExternalClock);
        mMethodOverride(m_methods, CanExtractPdhDe1Clock);
        mMethodOverride(m_methods, CanExtractSerdesClock);
        mMethodOverride(m_methods, CanExtractPdhDe1LiuClock);
        mMethodOverride(m_methods, HwSystemClockExtract);
        mMethodOverride(m_methods, HwSdhSystemClockExtract);
        mMethodOverride(m_methods, HwSdhLineClockExtract);
        mMethodOverride(m_methods, HwExternalClockExtract);
        mMethodOverride(m_methods, HwPdhDe1ClockExtract);
        mMethodOverride(m_methods, HwSerdesClockExtract);
        mMethodOverride(m_methods, De1AcrDcrTimingModeExtract);
        mMethodOverride(m_methods, De1LoopTimingModeExtract);
        mMethodOverride(m_methods, HwPdhDe1LiuClockExtract);
        mMethodOverride(m_methods, CanExtractPdhDe3Clock);
        mMethodOverride(m_methods, CanExtractPdhDe3LiuClock);
        mMethodOverride(m_methods, HwPdhDe3ClockExtract);
        mMethodOverride(m_methods, De3AcrDcrTimingModeExtract);
        mMethodOverride(m_methods, De3LoopTimingModeExtract);
        mMethodOverride(m_methods, HwPdhDe3LiuClockExtract);
        mMethodOverride(m_methods, ExtractSourceReset);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtClockExtractor(AtClockExtractor self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtClockExtractorOverride, mMethodsGet(self), sizeof(m_AtClockExtractorOverride));

        mMethodOverride(m_AtClockExtractorOverride, PdhDe1ClockExtract);
        mMethodOverride(m_AtClockExtractorOverride, PdhDe1LiuClockExtract);
        mMethodOverride(m_AtClockExtractorOverride, PdhDe1LiuClockIsExtracted);
        mMethodOverride(m_AtClockExtractorOverride, PdhDe1Get);
        mMethodOverride(m_AtClockExtractorOverride, SystemClockExtract);
        mMethodOverride(m_AtClockExtractorOverride, SdhSystemClockExtract);
        mMethodOverride(m_AtClockExtractorOverride, SdhLineClockExtract);
        mMethodOverride(m_AtClockExtractorOverride, SdhLineGet);
        mMethodOverride(m_AtClockExtractorOverride, ExternalClockExtract);
        mMethodOverride(m_AtClockExtractorOverride, ExternalIdIsValid);
        mMethodOverride(m_AtClockExtractorOverride, PdhDe3ClockExtract);
        mMethodOverride(m_AtClockExtractorOverride, PdhDe3LiuClockExtract);
        mMethodOverride(m_AtClockExtractorOverride, PdhDe3LiuClockIsExtracted);
        mMethodOverride(m_AtClockExtractorOverride, PdhDe3Get);
        mMethodOverride(m_AtClockExtractorOverride, SerdesClockExtract);
        mMethodOverride(m_AtClockExtractorOverride, SerdesGet);
        mMethodOverride(m_AtClockExtractorOverride, SquelchingOptionSet);
        mMethodOverride(m_AtClockExtractorOverride, SquelchingOptionGet);
        mMethodOverride(m_AtClockExtractorOverride, OutputCounterGet);
        mMethodOverride(m_AtClockExtractorOverride, Debug);
        mMethodOverride(m_AtClockExtractorOverride, SquelchingIsSupported);
        }

    mMethodsSet(self, &m_AtClockExtractorOverride);
    }

static void OverrideAtObject(AtClockExtractor self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtClockExtractor self)
    {
    OverrideAtObject(self);
    OverrideAtClockExtractor(self);
    }

AtClockExtractor ThaClockExtractorObjectInit(AtClockExtractor self, AtModuleClock clockModule, uint8 extractorId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtClockExtractorObjectInit(self, clockModule, extractorId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

uint32 ThaClockExtractorDefaultOffset(ThaClockExtractor self)
    {
    return LocalId(self) + PartOffset(self);
    }

uint32 ThaClockExtractorPartOffset(ThaClockExtractor self)
    {
    return PartOffset(self);
    }

uint8 ThaClockExtractorPartId(ThaClockExtractor self)
    {
    ThaModuleClock clockModule = (ThaModuleClock)AtClockExtractorModuleGet((AtClockExtractor)self);
    return ThaModuleClockExtractorPart(clockModule, AtClockExtractorIdGet((AtClockExtractor)self));
    }

uint8 ThaClockExtractorLocalId(ThaClockExtractor self)
    {
    return LocalId(self);
    }

eAtRet ThaClockExtractorSourceReset(AtClockExtractor self)
    {
    if (self)
        return mMethodsGet(mThis(self))->ExtractSourceReset(mThis(self));
    return cAtErrorNullPointer;
    }

eAtModuleClockRet ThaClockExtractorHwSerdesClockExtract(ThaClockExtractor self, AtSerdesController serdes)
    {
    if (self)
        return mMethodsGet(self)->HwSerdesClockExtract(self, serdes);
    return cAtErrorObjectNotExist;
    }

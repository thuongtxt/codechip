/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Clock
 *
 * File        : ThaClockMonitor.c
 *
 * Created Date: Nov 22, 2013
 *
 * Description : Clock monitor
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModuleRam.h"
#include "../../../generic/man/AtModuleInternal.h" /* For module read/write */
#include "../man/ThaDevice.h"
#include "ThaModuleClock.h"
#include "ThaClockMonitorInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cInvalidValue 0xCAFECAFE
#define cStableCheckingTime 50
#define cExpectedDifferent  50

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaClockMonitorMethods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 Read(ThaClockMonitor self, uint32 address)
    {
    return mModuleHwRead(self->clockModule, address);
    }

static uint32 LocalBusClockFrequencyInKhzGet(ThaClockMonitor self)
    {
    uint32 mask    = mMethodsGet(self)->LocalBusClockCheckMask(self);
    uint32 shift   = mMethodsGet(self)->LocalBusClockCheckShift(self);
    uint32 regAddr = mMethodsGet(self)->LocalBusClockCheckRegister(self);
    uint32 regVal  = Read(self, regAddr);
    uint32 frequency;

    mFieldGet(regVal, mask, shift, uint32, &frequency);
    return ThaClockMonitorKhzCalculation(frequency);
    }

static uint32 DdrClockFrequencyInKhzGet(ThaClockMonitor self, uint32 ddrId)
    {
    uint32 mask    = mMethodsGet(self)->DdrClockCheckMask(self, ddrId);
    uint32 shift   = mMethodsGet(self)->DdrClockCheckShift(self, ddrId);
    uint32 regAddr = mMethodsGet(self)->DdrClockCheckRegister(self, ddrId);
    uint32 regVal  = Read(self, regAddr);
    uint32 frequency;

    mFieldGet(regVal, mask, shift, uint32, &frequency);
    return ThaClockMonitorKhzCalculation(frequency);
    }

static uint32 *DdrClockExpectedFrequenciesInKhz(ThaClockMonitor self, uint32 ddrId, uint8 *numFrequencies)
    {
    static uint32 expectedFrequencies[] = {125000};
	AtUnused(ddrId);
	AtUnused(self);
    if (numFrequencies)
        *numFrequencies = mCount(expectedFrequencies);
    return expectedFrequencies;
    }

static uint32 DdrClockCheckRegister(ThaClockMonitor self, uint32 ddrId)
    {
	AtUnused(ddrId);
	AtUnused(self);
    return cInvalidValue;
    }

static uint32 DdrClockCheckMask(ThaClockMonitor self, uint32 ddrId)
    {
	AtUnused(ddrId);
	AtUnused(self);
    /* Sub class must know */
    return 0;
    }

static uint32 DdrClockCheckShift(ThaClockMonitor self, uint32 ddrId)
    {
	AtUnused(ddrId);
	AtUnused(self);
    /* Sub class must know */
    return 0;
    }

static uint32 _DdrClockFrequencyInKhzGet(ThaClockMonitor self, void *param)
    {
    uint32 ddrId = *((uint8*)param);
    return DdrClockFrequencyInKhzGet(self, ddrId);
    }

static uint32 *_DdrClockExpectedFrequenciesInKhz(ThaClockMonitor self, void *param, uint8 *numFrequencies)
    {
    uint32 ddrId = *((uint8*)param);
    return mMethodsGet(self)->DdrClockExpectedFrequenciesInKhz(self, ddrId, numFrequencies);
    }

static eBool ClockDifferentIsInRange(uint32 frequency_1, uint32 frequency_2, uint32 expectedDiff)
    {
    uint32 diff = (frequency_1 > frequency_2) ? (frequency_1 - frequency_2) : (frequency_2 - frequency_1);
    return (diff <= expectedDiff) ? cAtTrue : cAtFalse;
    }

static uint32 *SystemClockExpectedFrequenciesInKhz(ThaClockMonitor self, uint8 *numFrequencies)
    {
    static uint32 expectedFrequencies[] = {19440};
	AtUnused(self);
    if (numFrequencies)
        *numFrequencies = mCount(expectedFrequencies);
    return expectedFrequencies;
    }

static uint32 SystemClockFrequencyInKhzGet(ThaClockMonitor self)
    {
    uint32 mask    = mMethodsGet(self)->SystemClockCheckMask(self);
    uint32 shift   = mMethodsGet(self)->SystemClockCheckShift(self);
    uint32 regAddr = mMethodsGet(self)->SystemClockCheckRegister(self);
    uint32 regVal  = Read(self, regAddr);
    uint32 frequency;

    mFieldGet(regVal, mask, shift, uint32, &frequency);
    return ThaClockMonitorKhzCalculation(frequency);
    }

static uint32 SystemClockCheckRegister(ThaClockMonitor self)
    {
	AtUnused(self);
    /* Sub class must know */
    return cInvalidValue;
    }

static uint32 SystemClockCheckMask(ThaClockMonitor self)
    {
	AtUnused(self);
    /* Sub class must know */
    return 0;
    }

static uint32 SystemClockCheckShift(ThaClockMonitor self)
    {
	AtUnused(self);
    /* Sub class must know */
    return 0;
    }

static uint32 *_SystemClockExpectedFrequenciesInKhz(ThaClockMonitor self, void *param, uint8 *numFrequencies)
    {
	AtUnused(param);
    return mMethodsGet(self)->SystemClockExpectedFrequenciesInKhz(self, numFrequencies);
    }

static uint32 _SystemClockFrequencyInKhzGet(ThaClockMonitor self, void *param)
    {
	AtUnused(param);
    return SystemClockFrequencyInKhzGet(self);
    }

static uint32 *LocalBusClockExpectedFrequenciesInKhz(ThaClockMonitor self, uint8 *numFrequencies)
    {
    static uint32 expectedFrequencies[] = {50000};
	AtUnused(self);
    if (numFrequencies)
        *numFrequencies = mCount(expectedFrequencies);
    return expectedFrequencies;
    }

static uint32 LocalBusClockCheckRegister(ThaClockMonitor self)
    {
	AtUnused(self);
    /* Sub class must know */
    return cInvalidValue;
    }

static uint32 LocalBusClockCheckMask(ThaClockMonitor self)
    {
	AtUnused(self);
    /* Sub class must know */
    return 0;
    }

static uint32 LocalBusClockCheckShift(ThaClockMonitor self)
    {
	AtUnused(self);
    /* Sub class must know */
    return 0;
    }

static uint32 *_LocalBusClockExpectedFrequenciesInKhz(ThaClockMonitor self, void *param, uint8 *numFrequencies)
    {
	AtUnused(param);
    return mMethodsGet(self)->LocalBusClockExpectedFrequenciesInKhz(self, numFrequencies);
    }

static uint32 _LocalBusClockFrequencyInKhzGet(ThaClockMonitor self, void *param)
    {
    AtUnused(param);
    return LocalBusClockFrequencyInKhzGet(self);
    }

static uint32 OcnNumClocks(ThaClockMonitor self)
    {
	AtUnused(self);
    /* Sub class should know */
    return 0;
    }

static uint32 *OcnClockExpectedFrequenciesInKhz(ThaClockMonitor self, uint32 clockId, uint8 *numFrequencies)
    {
    static uint32 expectedFrequencies[] = {155520};
	AtUnused(clockId);
	AtUnused(self);
    if (numFrequencies)
        *numFrequencies = mCount(expectedFrequencies);
    return expectedFrequencies;
    }

static uint32 OcnClockCheckRegister(ThaClockMonitor self, uint32 clockId)
    {
	AtUnused(clockId);
	AtUnused(self);
    /* Sub class must know */
    return cInvalidValue;
    }

static uint32 OcnClockCheckMask(ThaClockMonitor self, uint32 clockId)
    {
	AtUnused(clockId);
	AtUnused(self);
    /* Sub class must know */
    return 0;
    }

static uint32 OcnClockCheckShift(ThaClockMonitor self, uint32 clockId)
    {
	AtUnused(clockId);
	AtUnused(self);
    /* Sub class must know */
    return 0;
    }

static uint32 OcnClockFrequencyInKhzGet(ThaClockMonitor self, uint32 clockId)
    {
    uint32 mask    = mMethodsGet(self)->OcnClockCheckMask(self, clockId);
    uint32 shift   = mMethodsGet(self)->OcnClockCheckShift(self, clockId);
    uint32 regAddr = mMethodsGet(self)->OcnClockCheckRegister(self, clockId);
    uint32 regVal  = Read(self, regAddr);
    uint32 frequency;

    mFieldGet(regVal, mask, shift, uint32, &frequency);
    return ThaClockMonitorKhzCalculation(frequency);
    }

static uint32 _OcnClockFrequencyInKhzGet(ThaClockMonitor self, void *param)
    {
    uint32 clockId = *((uint32*)param);
    return OcnClockFrequencyInKhzGet(self, clockId);
    }

static uint32 *_OcnClockExpectedFrequenciesInKhz(ThaClockMonitor self, void *param, uint8 *numFrequencies)
    {
    uint32 clockId = *((uint32*)param);
    return mMethodsGet(self)->OcnClockExpectedFrequenciesInKhz(self, clockId, numFrequencies);
    }

static uint32 *GeClockExpectedFrequenciesInKhz(ThaClockMonitor self, uint8 *numFrequencies)
    {
    static uint32 expectedFrequencies[] = {125000};
	AtUnused(self);
    if (numFrequencies)
        *numFrequencies = mCount(expectedFrequencies);
    return expectedFrequencies;
    }

static uint32 GeClockCheckRegister(ThaClockMonitor self)
    {
	AtUnused(self);
    /* Sub class must know */
    return cInvalidValue;
    }

static uint32 GeClockCheckMask(ThaClockMonitor self)
    {
	AtUnused(self);
    /* Sub class must know */
    return 0;
    }

static uint32 GeClockCheckShift(ThaClockMonitor self)
    {
	AtUnused(self);
    /* Sub class must know */
    return 0;
    }

static uint32 GeClockMonitorKhzCalculation(ThaClockMonitor self, uint32 frequency)
    {
	AtUnused(self);
    return ThaClockMonitorKhzCalculation(frequency);
    }

static uint32 GeClockFrequencyInKhzGet(ThaClockMonitor self)
    {
    uint32 mask    = mMethodsGet(self)->GeClockCheckMask(self);
    uint32 shift   = mMethodsGet(self)->GeClockCheckShift(self);
    uint32 regAddr = mMethodsGet(self)->GeClockCheckRegister(self);
    uint32 regVal  = Read(self, regAddr);
    uint32 frequency;

    mFieldGet(regVal, mask, shift, uint32, &frequency);
    return mMethodsGet(self)->GeClockMonitorKhzCalculation(self, frequency);
    }

static uint32 _GeClockFrequencyInKhzGet(ThaClockMonitor self, void *param)
    {
	AtUnused(param);
    return GeClockFrequencyInKhzGet(self);
    }

static uint32 *_GeClockExpectedFrequenciesInKhz(ThaClockMonitor self, void *param, uint8 *numFrequencies)
    {
	AtUnused(param);
    return mMethodsGet(self)->GeClockExpectedFrequenciesInKhz(self, numFrequencies);
    }

static AtDevice Device(ThaClockMonitor self)
    {
    return AtModuleDeviceGet((AtModule)(self->clockModule));
    }

static AtModuleRam RamModule(ThaClockMonitor self)
    {
    return (AtModuleRam)AtDeviceModuleGet(Device(self), cAtModuleRam);
    }

static void DdrClockStatusShow(ThaClockMonitor self)
    {
    uint32 ddrId;
    AtModuleRam ramModule = RamModule(self);

    AtPrintc(cSevNormal, "* DDR: \r\n");
    for (ddrId = 0; ddrId < AtModuleRamNumDdrGet(ramModule); ddrId++)
        {
        uint8 numExpectedFrequencies;
        uint32 *expectedFrequencies = mMethodsGet(self)->DdrClockExpectedFrequenciesInKhz(self, ddrId, &numExpectedFrequencies);
        AtPrintc(cSevNormal, "  - %u: ", ddrId + 1);
        ThaClockMonitorClockStatusShow(expectedFrequencies, numExpectedFrequencies, DdrClockFrequencyInKhzGet(self, ddrId));
        AtPrintc(cSevNormal, "\r\n");
        }
    }

static void SystemClockStatusShow(ThaClockMonitor self)
    {
    uint8 numExpectedFrequencies;
    uint32 *expectedFrequencies = mMethodsGet(self)->SystemClockExpectedFrequenciesInKhz(self, &numExpectedFrequencies);

    AtPrintc(cSevNormal, "* System: ");
    ThaClockMonitorClockStatusShow(expectedFrequencies, numExpectedFrequencies, SystemClockFrequencyInKhzGet(self));
    AtPrintc(cSevNormal, "\r\n");
    }

static void LocalBusClockStatusShow(ThaClockMonitor self)
    {
    uint8 numExpectedFrequencies;
    uint32 *expectedFrequencies = mMethodsGet(self)->LocalBusClockExpectedFrequenciesInKhz(self, &numExpectedFrequencies);

    AtPrintc(cSevNormal, "* LocalBus: ");
    ThaClockMonitorClockStatusShow(expectedFrequencies, numExpectedFrequencies, LocalBusClockFrequencyInKhzGet(self));
    AtPrintc(cSevNormal, "\r\n");
    }

static void GeClockStatusShow(ThaClockMonitor self)
    {
    uint8 numExpectedFrequencies;
    uint32 *expectedFrequencies = mMethodsGet(self)->GeClockExpectedFrequenciesInKhz(self, &numExpectedFrequencies);

    AtPrintc(cSevNormal, "* GE: ");
    ThaClockMonitorClockStatusShow(expectedFrequencies, numExpectedFrequencies, GeClockFrequencyInKhzGet(self));
    AtPrintc(cSevNormal, "\r\n");
    }

static void OcnClockStatusShow(ThaClockMonitor self)
    {
    uint32 clockId;

    if (AtDeviceModuleGet(Device(self), cAtModuleSdh) == NULL)
        return;

    AtPrintc(cSevNormal, "* OCN: \r\n");
    for (clockId = 0; clockId < mMethodsGet(self)->OcnNumClocks(self); clockId++)
        {
        uint8 numExpectedFrequencies;
        uint32 *expectedFrequencies = mMethodsGet(self)->OcnClockExpectedFrequenciesInKhz(self, clockId, &numExpectedFrequencies);

        AtPrintc(cSevNormal, "  - %u: ", clockId + 1);
        ThaClockMonitorClockStatusShow(expectedFrequencies, numExpectedFrequencies, OcnClockFrequencyInKhzGet(self, clockId));
        AtPrintc(cSevNormal, "\r\n");
        }
    }

static void Debug(ThaClockMonitor self)
    {
    DdrClockStatusShow(self);
    OcnClockStatusShow(self);
    SystemClockStatusShow(self);
    LocalBusClockStatusShow(self);
    GeClockStatusShow(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaClockMonitor);
    }

static void MethodsInit(ThaClockMonitor self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* DDR clock checking */
        mMethodOverride(m_methods, DdrClockExpectedFrequenciesInKhz);
        mMethodOverride(m_methods, DdrClockCheckRegister);
        mMethodOverride(m_methods, DdrClockCheckMask);
        mMethodOverride(m_methods, DdrClockCheckShift);

        /* System clock checking */
        mMethodOverride(m_methods, SystemClockExpectedFrequenciesInKhz);
        mMethodOverride(m_methods, SystemClockCheckRegister);
        mMethodOverride(m_methods, SystemClockCheckMask);
        mMethodOverride(m_methods, SystemClockCheckShift);

        /* OCN clock checking */
        mMethodOverride(m_methods, OcnNumClocks);
        mMethodOverride(m_methods, OcnClockExpectedFrequenciesInKhz);
        mMethodOverride(m_methods, OcnClockCheckRegister);
        mMethodOverride(m_methods, OcnClockCheckMask);
        mMethodOverride(m_methods, OcnClockCheckShift);

        /* Local bus clock checking */
        mMethodOverride(m_methods, LocalBusClockExpectedFrequenciesInKhz);
        mMethodOverride(m_methods, LocalBusClockCheckRegister);
        mMethodOverride(m_methods, LocalBusClockCheckMask);
        mMethodOverride(m_methods, LocalBusClockCheckShift);

        /* GE clock checking */
        mMethodOverride(m_methods, GeClockExpectedFrequenciesInKhz);
        mMethodOverride(m_methods, GeClockCheckRegister);
        mMethodOverride(m_methods, GeClockCheckMask);
        mMethodOverride(m_methods, GeClockCheckShift);
        mMethodOverride(m_methods, GeClockMonitorKhzCalculation);

        /* Debug */
        mMethodOverride(m_methods, Debug);
        }

    mMethodsSet(self, &m_methods);
    }

ThaClockMonitor ThaClockMonitorObjectInit(ThaClockMonitor self, AtModuleClock clockModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    m_methodsInit = 1;

    /* Private data */
    self->clockModule = clockModule;

    return self;
    }

uint32 ThaClockMonitorDdrClockCheck(ThaClockMonitor self)
    {
    uint8 ddr_i, ddrId;
    uint32 status = 0;

    for (ddr_i = 0; ddr_i < AtModuleRamNumDdrGet(RamModule(self)); ddr_i++)
        {
        ddrId = ddr_i;
        if (ThaClockMonitorClockCheck(self, _DdrClockFrequencyInKhzGet, _DdrClockExpectedFrequenciesInKhz, &ddrId) != cAtOk)
            status |= cThaClockStatusDdrClockFail(ddrId);
        }

    return status;
    }

uint32 ThaClockMonitorSystemClockCheck(ThaClockMonitor self)
    {
    if (ThaClockMonitorClockCheck(self, _SystemClockFrequencyInKhzGet, _SystemClockExpectedFrequenciesInKhz, NULL) == cAtOk)
        return 0;

    return cThaClockStatusSystemClockFail;
    }

uint32 ThaClockMonitorOcnClockCheck(ThaClockMonitor self)
    {
    uint32 clock_i;
    uint32 status = 0;

    if (AtDeviceModuleGet(Device(self), cAtModuleSdh) == NULL)
        return 0;

    for (clock_i = 0; clock_i < mMethodsGet(self)->OcnNumClocks(self); clock_i++)
        {
        uint32 clockId = clock_i;
        eAtRet ret = ThaClockMonitorClockCheck(self, _OcnClockFrequencyInKhzGet, _OcnClockExpectedFrequenciesInKhz, &clockId);
        if (ret != cAtOk)
            status |= cThaClockStatusOcnClockFail(clockId);
        }

    return status;
    }

uint32 ThaClockMonitorLocalBusClockCheck(ThaClockMonitor self)
    {
    if (ThaClockMonitorClockCheck(self, _LocalBusClockFrequencyInKhzGet, _LocalBusClockExpectedFrequenciesInKhz, NULL) == cAtOk)
        return 0;
    return cThaClockStatusLocalBusClockFail;
    }

uint32 ThaClockMonitorGeClockCheck(ThaClockMonitor self)
    {
    if (ThaClockMonitorClockCheck(self, _GeClockFrequencyInKhzGet, _GeClockExpectedFrequenciesInKhz, NULL) == cAtOk)
        return 0;
    return cThaClockStatusGeClockFail;
    }

void ThaClockMonitorDebug(ThaClockMonitor self)
    {
    if (self)
        mMethodsGet(self)->Debug(self);
    }

uint32 ThaClockMonitorKhzCalculation(uint32 hwValue)
    {
    uint32 mhz    = (hwValue & cBit15_8) >> 8;
    uint32 remain = (hwValue & cBit7_0);

    return (mhz * 1000) + (remain * 10);
    }

eAtRet ThaClockMonitorClockCheck(ThaClockMonitor self,
                                 uint32 (*FrequencyInKhzGet)(ThaClockMonitor self, void *param),
                                 uint32 *(*ExpectedFrequenciesInKhz)(ThaClockMonitor self, void *param, uint8 *numFrequencies),
                                 void *param)
    {
    uint32 currentFrequency;
    uint32 time_i;
    uint8 numExpectedFrequencies;
    uint8 frequency_i;
    uint32 *expectedFrequencies = ExpectedFrequenciesInKhz(self, param, &numExpectedFrequencies);
    eBool stable = cAtTrue;

    for (frequency_i = 0; frequency_i < numExpectedFrequencies; frequency_i++)
        {
        /* Assume clock is stable */
        stable = cAtTrue;

        /* The first time must be in range */
        currentFrequency = FrequencyInKhzGet(self, param);
        if (!ClockDifferentIsInRange(currentFrequency, expectedFrequencies[frequency_i], cExpectedDifferent))
            {
            stable = cAtFalse;
            continue;
            }

        /* And it must be stable in that range */
        for (time_i = 0; time_i < cStableCheckingTime; time_i++)
            {
            uint32 newFrequency = FrequencyInKhzGet(self, param);
            uint32 diff = (currentFrequency > newFrequency) ? (currentFrequency - newFrequency) : (newFrequency - currentFrequency);
            if (diff > 1)
                {
                stable = cAtFalse;
                break;
                }
            }

        /* Stable, do not need to check more */
        if (stable)
            return cAtOk;
        }

    return cAtErrorClockUnstable;
    }

void ThaClockMonitorClockStatusShow(uint32 *expectedFrequenciesKhz, uint8 numExpectedFrequencies, uint32 currentFrequencyKhz)
    {
    eBool isInRange = cAtFalse;
    eAtSevLevel color;
    uint8 frequency_i;

    if (numExpectedFrequencies == 0)
        return;

    /* Check if clock is in range */
    for (frequency_i = 0; frequency_i < numExpectedFrequencies; frequency_i++)
        {
        isInRange = ClockDifferentIsInRange(expectedFrequenciesKhz[frequency_i], currentFrequencyKhz, cExpectedDifferent);
        if (isInRange)
            break;
        }

    /* Color to display */
    color = isInRange ? cSevInfo : cSevCritical;

    /* Show expected frequencies */
    if (numExpectedFrequencies == 1)
        AtPrintc(cSevNormal, "Expected: %u (Khz)", expectedFrequenciesKhz[0]);
    else
        {
        AtPrintc(cSevNormal, "Expected: (");
        for (frequency_i = 0; frequency_i < numExpectedFrequencies; frequency_i++)
            AtPrintc(cSevNormal, (frequency_i == 0) ? "%u" : ", %u", expectedFrequenciesKhz[frequency_i]);
        AtPrintc(cSevNormal, ") (Khz)");
        }

    /* Show current frequency */
    AtPrintc(cSevNormal, ", Current: %u (Khz)", currentFrequencyKhz);
    AtPrintc(color, "(%s)", isInRange ? "Good" : "Bad");
    }

uint32 *ThaClockMonitorSystemClockExpectedFrequenciesInKhz(ThaClockMonitor self, uint8 *numFrequencies)
    {
    if (self)
        return mMethodsGet(self)->SystemClockExpectedFrequenciesInKhz(self, numFrequencies);
    return NULL;
    }

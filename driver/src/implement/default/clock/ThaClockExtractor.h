/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Clock
 * 
 * File        : ThaClockExtractor.h
 * 
 * Created Date: Aug 27, 2013
 *
 * Description : Clock extractor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THACLOCKEXTRACTOR_H_
#define _THACLOCKEXTRACTOR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtClockExtractor.h" /* Super class */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaClockExtractor * ThaClockExtractor;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Default concretes */
AtClockExtractor ThaStmMlpppClockExtractorNew(AtModuleClock clockModule, uint8 extractorId);
AtClockExtractor ThaPdhMlpppClockExtractorNew(AtModuleClock clockModule, uint8 extractorId);

uint8 ThaClockExtractorPartId(ThaClockExtractor self);
uint32 ThaClockExtractorDefaultOffset(ThaClockExtractor self);
uint32 ThaClockExtractorPartOffset(ThaClockExtractor self);
uint8 ThaClockExtractorLocalId(ThaClockExtractor self);
eAtRet ThaClockExtractorSourceReset(AtClockExtractor self);

/* Product concretes */
AtClockExtractor Tha60030081ClockExtractorNew(AtModuleClock clockModule, uint8 extractorId);
AtClockExtractor Tha60031021ClockExtractorNew(AtModuleClock clockModule, uint8 extractorId);
AtClockExtractor Tha60031031ClockExtractorNew(AtModuleClock clockModule, uint8 extractorId);
AtClockExtractor Tha60031032ClockExtractorNew(AtModuleClock clockModule, uint8 extractorId);
AtClockExtractor ThaStmPwProductClockExtractorNew(AtModuleClock clockModule, uint8 extractorId);
AtClockExtractor ThaPdhPwProductClockExtractorNew(AtModuleClock clockModule, uint8 extractorId);
AtClockExtractor Tha60210011ClockExtractorNew(AtModuleClock clockModule, uint8 extractorId);
AtClockExtractor Tha60290021ClockExtractorNew(AtModuleClock clockModule, uint8 extractorId);
AtClockExtractor Tha60290022ClockExtractorNew(AtModuleClock clockModule, uint8 extractorId);
AtClockExtractor Tha60290022ClockExtractorV2New(AtModuleClock clockModule, uint8 extractorId);

#ifdef __cplusplus
}
#endif
#endif /* _THACLOCKEXTRACTOR_H_ */

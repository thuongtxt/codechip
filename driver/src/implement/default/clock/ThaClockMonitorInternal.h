/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Clock
 * 
 * File        : ThaClockMonitorInternal.h
 * 
 * Created Date: Nov 22, 2013
 *
 * Description : Clock monitor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THACLOCKMONITORINTERNAL_H_
#define _THACLOCKMONITORINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaClockMonitor.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaClockMonitorMethods
    {
    /* DDR clock checking */
    uint32 *(*DdrClockExpectedFrequenciesInKhz)(ThaClockMonitor self, uint32 ddrId, uint8 *numFrequencies);
    uint32 (*DdrClockCheckRegister)(ThaClockMonitor self, uint32 ddrId);
    uint32 (*DdrClockCheckMask)(ThaClockMonitor self, uint32 ddrId);
    uint32 (*DdrClockCheckShift)(ThaClockMonitor self, uint32 ddrId);

    /* System clock checking */
    uint32 *(*SystemClockExpectedFrequenciesInKhz)(ThaClockMonitor self, uint8 *numFrequencies);
    uint32 (*SystemClockCheckRegister)(ThaClockMonitor self);
    uint32 (*SystemClockCheckMask)(ThaClockMonitor self);
    uint32 (*SystemClockCheckShift)(ThaClockMonitor self);

    /* OCN clock checking */
    uint32 (*OcnNumClocks)(ThaClockMonitor self);
    uint32 *(*OcnClockExpectedFrequenciesInKhz)(ThaClockMonitor self, uint32 clockId, uint8 *numFrequencies);
    uint32 (*OcnClockCheckRegister)(ThaClockMonitor self, uint32 clockId);
    uint32 (*OcnClockCheckMask)(ThaClockMonitor self, uint32 clockId);
    uint32 (*OcnClockCheckShift)(ThaClockMonitor self, uint32 clockId);

    /* GE clock checking */
    uint32 *(*GeClockExpectedFrequenciesInKhz)(ThaClockMonitor self, uint8 *numFrequencies);
    uint32 (*GeClockCheckRegister)(ThaClockMonitor self);
    uint32 (*GeClockCheckMask)(ThaClockMonitor self);
    uint32 (*GeClockCheckShift)(ThaClockMonitor self);
    uint32 (*GeClockMonitorKhzCalculation)(ThaClockMonitor self, uint32 frequency);

    /* Local bus clock checking */
    uint32 *(*LocalBusClockExpectedFrequenciesInKhz)(ThaClockMonitor self, uint8 *numFrequencies);
    uint32 (*LocalBusClockCheckRegister)(ThaClockMonitor self);
    uint32 (*LocalBusClockCheckMask)(ThaClockMonitor self);
    uint32 (*LocalBusClockCheckShift)(ThaClockMonitor self);

    /* Debug */
    void (*Debug)(ThaClockMonitor self);
    }tThaClockMonitorMethods;

typedef struct tThaClockMonitor
    {
    tAtObject super;
    const tThaClockMonitorMethods *methods;

    /* Private data */
    AtModuleClock clockModule;
    }tThaClockMonitor;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaClockMonitor ThaClockMonitorObjectInit(ThaClockMonitor self, AtModuleClock clockModule);

eAtRet ThaClockMonitorClockCheck(ThaClockMonitor self,
                                 uint32 (*FrequencyInKhzGet)(ThaClockMonitor self, void *param),
                                 uint32 *(*ExpectedFrequenciesInKhz)(ThaClockMonitor self, void *param, uint8 *numFrequencies),
                                 void *param);
uint32 ThaClockMonitorKhzCalculation(uint32 hwValue);
void ThaClockMonitorClockStatusShow(uint32 *expectedFrequenciesKhz, uint8 numExpectedFrequencies, uint32 currentFrequencyKhz);
uint32 *ThaClockMonitorSystemClockExpectedFrequenciesInKhz(ThaClockMonitor self, uint8 *numFrequencies);

#ifdef __cplusplus
}
#endif
#endif /* _THACLOCKMONITORINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Clock
 * 
 * File        : ThaModuleClockInternal.h
 * 
 * Created Date: Aug 27, 2013
 *
 * Description : Clock module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULECLOCKINTERNAL_H_
#define _THAMODULECLOCKINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtList.h"
#include "../../../generic/clock/AtModuleClockInternal.h"
#include "../man/ThaDevice.h"
#include "ThaModuleClock.h"
#include "ThaClockExtractor.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleClockMethods
    {
    /* Clock extractors */
    AtClockExtractor (*ExtractorCreate)(ThaModuleClock self, uint8 extractorId);
    uint8 (*ExtractorPart)(ThaModuleClock self, uint8 extractorId);
    uint8 (*MaxNumExternalClockSources)(ThaModuleClock self);
    eAtModuleClockRet (*ClockExtractorInit)(ThaModuleClock self, AtClockExtractor extractor);

    /* Clock checking */
    ThaClockMonitor (*ClockMonitorCreate)(ThaModuleClock self);
    AtClockMonitor (*EthPortClockMonitorCreate)(ThaModuleClock self, AtEthPort port);

    /* Monitoring clock */
    eAtRet (*MonitorOutputSet)(ThaModuleClock self, uint8 clockOutputId, eThaClockMonitorOutput refClockSource);
    eThaClockMonitorOutput (*MonitorOutputGet)(ThaModuleClock self, uint8 clockOutputId);
    uint8 (*MonitorOutputNumGet)(ThaModuleClock self);
    uint16 (*MonitorOutputSw2Hw)(ThaModuleClock self, eThaClockMonitorOutput refClockSource);
    eThaClockMonitorOutput (*MonitorOutputHw2Sw)(ThaModuleClock self, uint16 refClockSource);
    eBool (*MonitorOutputIsSupported)(ThaModuleClock self, eThaClockMonitorOutput refClockSource);

    /* Polymorphism Register, Shift, Mask */
    uint32 (*RefIdMask)(ThaModuleClock self, AtClockExtractor extractor);
    uint32 (*RefIdShift)(ThaModuleClock self, AtClockExtractor extractor);
    uint32 (*SquelchingHwAddress)(ThaModuleClock self, AtClockExtractor extractor);
    uint32 (*SquelchingHwMask)(ThaModuleClock self, AtClockExtractor extractor);
    uint32 (*SquelchingHwShift)(ThaModuleClock self, AtClockExtractor extractor);
    uint32 (*OutputCounterHwAddress)(ThaModuleClock self, AtClockExtractor extractor);
    uint32 (*OutputCounterHwMask)(ThaModuleClock self, AtClockExtractor extractor);
    uint32 (*OutputCounterHwShift)(ThaModuleClock self, AtClockExtractor extractor);
    eBool  (*SquelchingIsSupported)(ThaModuleClock self);

    /* Polymorphism squelching completely */
    uint32 (*HwOutputCounterGet)(ThaModuleClock self, AtClockExtractor extractor);
    eAtModuleClockRet (*HwSquelchingOptionSet)(ThaModuleClock self, AtClockExtractor extractor, eAtClockExtractorSquelching options);
    uint32 (*HwSquelchingOptionGet)(ThaModuleClock self, AtClockExtractor extractor);

    eBool (*ShouldEnableCdrInterrupt)(ThaModuleClock self);
    }tThaModuleClockMethods;

typedef struct tThaModuleClock
    {
    tAtModuleClock super;
    const tThaModuleClockMethods *methods;

    /* Private data */
    AtList extractors;
    ThaClockMonitor clockMonitor;
    eBool interruptEnabled;
    }tThaModuleClock;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleClock ThaModuleClockObjectInit(AtModuleClock self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULECLOCKINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Clock
 * 
 * File        : ThaClockExtractorInternal.h
 * 
 * Created Date: Aug 27, 2013
 *
 * Description : Clock extractor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THACLOCKEXTRACTORINTERNAL_H_
#define _THACLOCKEXTRACTORINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSdhLine.h"
#include "AtPdhDe1.h"
#include "AtPdhDe3.h"
#include "../../../generic/clock/AtClockExtractorInternal.h"
#include "../man/ThaDevice.h"
#include "ThaClockExtractor.h"
#include "ThaModuleClock.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaClockExtractorMethods
    {
    /* Ability */
    eBool (*CanExtractSystemClock)(ThaClockExtractor self);
    eBool (*CanExtractSdhSystemClock)(ThaClockExtractor self);
    eBool (*CanExtractSdhLineClock)(ThaClockExtractor self, AtSdhLine line);
    eBool (*CanExtractExternalClock)(ThaClockExtractor self, uint8 externalId);
    eBool (*CanExtractPdhDe1Clock)(ThaClockExtractor self, AtPdhDe1 de1);
    eBool (*CanExtractPdhDe1LiuClock)(ThaClockExtractor self, AtPdhDe1 de1);
    eBool (*CanExtractSerdesClock)(ThaClockExtractor self, AtSerdesController serdes);

    /* Control HW system clock extraction */
    eAtModuleClockRet (*HwSystemClockExtract)(ThaClockExtractor self);

    /* Control HW SDH clock extraction */
    eAtModuleClockRet (*HwSdhSystemClockExtract)(ThaClockExtractor self);
    eAtModuleClockRet (*HwSdhLineClockExtract)(ThaClockExtractor self, AtSdhLine line);

    /* Control HW external clock extraction */
    eAtModuleClockRet (*HwExternalClockExtract)(ThaClockExtractor self, uint8 externalId);

    /* Control HW PDH clock extraction */
    eAtModuleClockRet (*HwPdhDe1ClockExtract)(ThaClockExtractor self, AtPdhDe1 de1);
    eAtModuleClockRet (*De1AcrDcrTimingModeExtract)(ThaClockExtractor self, AtPdhDe1 de1);
    eAtModuleClockRet (*De1LoopTimingModeExtract)(ThaClockExtractor self, AtPdhDe1 de1);
    eAtModuleClockRet (*HwPdhDe1LiuClockExtract)(ThaClockExtractor self, AtPdhDe1 de1);

    /* ADD for DS3/E3 */
    eBool (*CanExtractPdhDe3Clock)(ThaClockExtractor self, AtPdhDe3 de3);
    eBool (*CanExtractPdhDe3LiuClock)(ThaClockExtractor self, AtPdhDe3 de3);
    eAtModuleClockRet (*HwPdhDe3ClockExtract)(ThaClockExtractor self, AtPdhDe3 de3);
    eAtModuleClockRet (*De3AcrDcrTimingModeExtract)(ThaClockExtractor self, AtPdhDe3 de3);
    eAtModuleClockRet (*De3LoopTimingModeExtract)(ThaClockExtractor self, AtPdhDe3 de3);
    eAtModuleClockRet (*HwPdhDe3LiuClockExtract)(ThaClockExtractor self, AtPdhDe3 de3);

    /* Serdes clock extractor */
    eAtModuleClockRet (*HwSerdesClockExtract)(ThaClockExtractor self, AtSerdesController serdes);

    eAtRet (*ExtractSourceReset)(ThaClockExtractor self);
    }tThaClockExtractorMethods;

typedef struct tThaClockExtractor
    {
    tAtClockExtractor super;
    const tThaClockExtractorMethods *methods;

    /* Private data */
    AtPdhDe1 de1;
    eBool alwaysExtractFromLiu;
    AtSdhLine line;
    AtSerdesController serdes;
    AtPdhDe3 de3;
    }tThaClockExtractor;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtClockExtractor ThaClockExtractorObjectInit(AtClockExtractor self, AtModuleClock clockModule, uint8 extractorId);

eAtModuleClockRet ThaClockExtractorHwSerdesClockExtract(ThaClockExtractor self, AtSerdesController serdes);

#ifdef __cplusplus
}
#endif
#endif /* _THACLOCKEXTRACTORINTERNAL_H_ */


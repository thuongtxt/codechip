/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Diagnostic
 *
 * File        : ThaDeviceQuerier.c
 *
 * Created Date: Jun 17, 2017
 *
 * Description : Device querier
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/diag/querier/man/AtDeviceQuerierInternal.h"
#include "../../man/ThaDeviceInternal.h"
#include "ThaQueriers.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaDeviceQuerier
    {
    tAtDeviceQuerier super;
    }tThaDeviceQuerier;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtQuerierMethods m_AtQuerierOverride;

/* Save super implementation */
static const tAtQuerierMethods *m_AtQuerierMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void ProblemsQuery(AtQuerier self, AtObject object, AtDebugger debugger)
    {
    AtIterator moduleIterator = ThaDeviceModuleIteratorCreate((AtDevice)object);
    AtModule module;

    m_AtQuerierMethods->ProblemsQuery(self, object, debugger);

    while ((module = (AtModule)AtIteratorNext(moduleIterator)) != NULL)
        {
        AtQuerier querier = AtModuleQuerierGet(module);
        if (querier)
            AtQuerierProblemsQuery(querier, (AtObject)module, debugger);
        }

    AtObjectDelete((AtObject)moduleIterator);
    }

static void OverrideAtQuerier(AtQuerier self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtQuerierMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtQuerierOverride, mMethodsGet(self), sizeof(m_AtQuerierOverride));

        mMethodOverride(m_AtQuerierOverride, ProblemsQuery);
        }

    mMethodsSet(self, &m_AtQuerierOverride);
    }

static void Override(AtQuerier self)
    {
    OverrideAtQuerier(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaDeviceQuerier);
    }

static AtQuerier ObjectInit(AtQuerier self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtDeviceQuerierObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtQuerier ThaDeviceSharedQuerier(void)
    {
    static tThaDeviceQuerier querier;
    static AtQuerier pQuerier = NULL;

    if (pQuerier == NULL)
        pQuerier = ObjectInit((AtQuerier)&querier);

    return pQuerier;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Diagnostic
 * 
 * File        : ThaQueriers.h
 * 
 * Created Date: Jun 17, 2017
 *
 * Description : All of queriers
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAQUERIERS_H_
#define _THAQUERIERS_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtQuerier.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtQuerier ThaDeviceSharedQuerier(void);
AtQuerier ThaEthPortSharedQuerier(void);

#ifdef __cplusplus
}
#endif
#endif /* _THAQUERIERS_H_ */


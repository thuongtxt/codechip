/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PWE (internal module)
 * 
 * File        : ThaModulePwe.h
 * 
 * Created Date: Sep 10, 2012
 *
 * Description :
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEPWE_H_
#define _THAMODULEPWE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModule.h"
#include "AtPw.h"
#include "../pw/ThaPwInternal.h"
#include "AtPwGroup.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModulePwe * ThaModulePwe;
typedef struct tThaModulePweV2 * ThaModulePweV2;

typedef struct tThaPweCache
    {
    /* Current cache */
    uint32 free512ByteCache;
    uint32 free256ByteCache;
    uint32 free128ByteCache;
    uint32 free64ByteCache;
    uint32 freeNumHiBlock;
    uint32 freeNumLoBlock;

    /* In-used */
    uint32 used512ByteCache;
    uint32 used256ByteCache;
    uint32 used128ByteCache;
    uint32 used64ByteCache;
    uint32 usedNumHiBlock;
    uint32 usedNumLoBlock;
    }tThaPweCache;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Concrete modules */
AtModule ThaModulePweNew(AtDevice device);
AtModule ThaModulePweV2New(AtDevice device);
AtModule ThaStmPwModulePweV2New(AtDevice device);

uint32 ThaModulePweBaseAddress(ThaModulePwe self);
uint32 ThaModulePwePlaBaseAddress(ThaModulePwe self);
uint32 ThaModulePwePartOffset(ThaModulePwe self, uint8 partId);
eAtRet ThaModulePwePwAddingPrepare(ThaModulePwe self, AtPw pw);
eAtRet ThaModulePwePwAddingStart(ThaModulePwe self, AtPw pw);
eAtRet ThaModulePwePwRemovingPrepare(ThaModulePwe self, AtPw pw);

eAtRet ThaModulePwePwNxDs0Set(ThaModulePwe self, AtPw pw, AtPdhNxDS0 nxDs0);
uint8 ThaModulePweHwPwType(ThaModulePwe self, AtPw pwAdapter, eAtPwType pwType);

eAtRet ThaModulePwePwRemovingStart(ThaModulePwe self, AtPw pw);
eBool ThaModulePwePwRemovingShouldStart(ThaModulePwe self, AtPw pw);
eAtRet ThaModulePwePwRemovingFinish(ThaModulePwe self, AtPw pw);
eAtRet ThaModulePwePwRamStatusClear(ThaModulePwe self, AtPw pw);

eAtRet ThaModulePwePwTxAlarmForce(ThaModulePwe self, AtPw pw, eAtPwAlarmType alarmType);
eAtRet ThaModulePwePwTxAlarmUnForce(ThaModulePwe self, AtPw pw, eAtPwAlarmType alarmType);
uint32 ThaModulePwePwTxForcedAlarmGet(ThaModulePwe self, AtPw pw);

eAtRet ThaModulePwePwEnable(ThaModulePwe self, AtPw pw, eBool enable);
eBool ThaModulePwePwIsEnabled(ThaModulePwe self, AtPw pw);

eAtRet ThaModulePweVcFragmentTypeSet(ThaModulePwe self, AtSdhChannel sdhVc);
eAtRet ThaModulePwePwCepEquip(ThaModulePwe self, AtPw pw, AtChannel subChannel, eBool equip);
eAtRet ThaModulePwePwHeaderCepEbmEnable(ThaModulePwe self, AtPw pw, eBool enable);
eBool ThaModulePwePwCepIsEmpty(ThaModulePwe self, AtPw pw);

eAtRet ThaModulePwePwTohByteEnable(ThaModulePwe self, AtPw pw, const tThaTohByte *tohByte, eBool enable);
eAtRet ThaModulePwePwTohFirstByteEnable(ThaModulePwe self, AtPw pw, const tThaTohByte *tohByte, eBool enable);
eAtRet ThaModulePwePwTohLastByteEnable(ThaModulePwe self, AtPw pw, const tThaTohByte *tohByte, eBool enable);
eAtRet ThaModulePwePwTohEnable(ThaModulePwe self, AtPw pw, eBool enable);
eBool ThaModulePwePwTohIsEnabled(ThaModulePwe self, AtPw pw);
eAtRet ThaModulePweTohPayloadSizeSet(ThaModulePwe self, AtPw pw, uint32 numFramePerPacket);
uint16 ThaModulePweTohPayloadSizeGet(ThaModulePwe self, AtPw pw);
eAtRet ThaModulePweBindLineToPw(ThaModulePwe self, AtSdhLine line, AtPw pw);
tThaTohByte* ThaModulePwePwTohFirstByteGet(ThaModulePwe self, AtPw pw, tThaTohByte *tohByte);
tThaTohByte* ThaModulePwePwTohLastByteGet(ThaModulePwe self, AtPw pw, tThaTohByte *tohByte);
uint8 ThaModulePwePwTohBytesBitmapGet(ThaModulePwe self, AtPw pw, uint32 *bitmaps);
uint16 ThaModulePwePwCesMaxNumFramesInPacket(ThaModulePwe self);
uint16 ThaModulePwePwTohMaxNumFrames(ThaModulePwe self);

eAtRet ThaModulePwePayloadSizeSet(ThaModulePwe self, AtPw pw, uint16 payloadSize);
uint16 ThaModulePwePayloadSizeGet(ThaModulePwe self, AtPw pw);
eAtRet ThaModulePwePwNumDs0TimeslotsSet(ThaModulePwe self, AtPw pw, uint16 numTimeslots);
eAtRet ThaModulePwePwNumFramesInPacketSet(ThaModulePwe self, AtPw pw, uint16 numFramesInPacket);
uint8 ThaModulePwePwNumDs0TimeslotsGet(ThaModulePwe self, AtPw pw);
uint16 ThaModulePweNumFramesInPacketGet(ThaModulePwe self, AtPw pw);

uint32 ThaModulePwePwTxPacketsGet(ThaModulePwe self, AtPw pw, eBool clear);
uint32 ThaModulePwePwTxBytesGet(ThaModulePwe self, AtPw pw, eBool clear);
uint32 ThaModulePwePwTxRbitPacketsGet(ThaModulePwe self, AtPw pw, eBool clear);
uint32 ThaModulePwePwTxMbitPacketsGet(ThaModulePwe self, AtPw pw, eBool clear);
uint32 ThaModulePwePwTxLbitPacketsGet(ThaModulePwe self, AtPw pw, eBool clear);
uint32 ThaModulePwePwTxNbitPacketsGet(ThaModulePwe self, AtPw pw, eBool clear);
uint32 ThaModulePwePwTxPbitPacketsGet(ThaModulePwe self, AtPw pw, eBool clear);

eAtRet ThaModulePwePwTypeSet(ThaModulePwe self, AtPw pw, eAtPwType pwType);
eAtRet ThaModulePwePwRtpEnable(ThaModulePwe self, AtPw pw, eBool enable);
eAtRet ThaModulePwePwCwEnable(ThaModulePwe self, AtPw pw, eBool enable);
eBool ThaModulePwePwCwIsEnabled(ThaModulePwe self, AtPw pw);
eAtRet ThaModulePwePwCwAutoTxLBitEnable(ThaModulePwe self, AtPw pw, eBool enable);
eBool ThaModulePwePwCwAutoTxLBitIsEnabled(ThaModulePwe self, AtPw pw);
eAtRet ThaModulePwePwCwAutoTxRBitEnable(ThaModulePwe self, AtPw pw, eBool enable);
eBool ThaModulePwePwCwAutoTxRBitIsEnabled(ThaModulePwe self, AtPw pw);
eAtRet ThaModulePwePwCwAutoTxMBitEnable(ThaModulePwe self, AtPw pw, eBool enable);
eBool ThaModulePwePwCwAutoTxMBitIsEnabled(ThaModulePwe self, AtPw pw);
eAtRet ThaModulePwePwCwAutoTxNPBitEnable(ThaModulePwe self, AtPw pw, eBool enable);
eBool ThaModulePwePwCwAutoTxNPBitIsEnabled(ThaModulePwe self, AtPw pw);
eBool ThaModulePwePwCwAutoTxNPBitIsConfigurable(ThaModulePwe self);

eAtRet ThaModulePweNumVlansSet(ThaModulePwe self, AtPw pw, uint8 numVlans);
eAtRet ThaModulePweHeaderWrite(ThaModulePwe self, AtPw pw, uint8 *buffer, uint8 headerLenInByte, AtPwPsn psn);
eAtRet ThaModulePweHeaderRead(ThaModulePwe self, AtPw pw, uint8 *buffer, uint8 bufferSize);
eAtRet ThaModulePwePwSuppressEnable(ThaModulePwe self, AtPw pw, eBool enable);
eBool ThaModulePwePwSuppressIsEnabled(ThaModulePwe self, AtPw pw);
eAtRet ThaModulePwePwPsnTypeSet(ThaModulePwe self, AtPw pw, AtPwPsn psn);
void ThaModulePwePwCepFractionalEquippedChannelsGet(ThaModulePwe self, AtPw pw, AtList equippedChannels);
eAtRet ThaModulePwePwJitterAttenuatorEnable(ThaModulePwe self, AtPw pwAdapter, eBool enable);
eAtRet ThaModulePweHdlcPwPayloadTypeSet(ThaModulePwe self, ThaPwHdlcAdapter adapter, eAtPwHdlcPayloadType payloadType);

eAtRet ThaModulePwePwBackupPsnTypeSet(ThaModulePwe self, AtPw pw, AtPwPsn psn);
eAtRet ThaModulePweBackupHeaderWrite(ThaModulePwe self, AtPw pw, uint8 *buffer, uint8 headerLenInByte);
eAtRet ThaModulePweBackupHeaderRead(ThaModulePwe self, AtPw pw, uint8 *buffer, uint8 bufferSize);
uint32 ThaModulePwePwPweDefaultOffset(ThaModulePwe self, AtPw pw);
uint32 ThaModulePwePwPlaDefaultOffset(ThaModulePwe self, AtPw pw);

/* For debugging */
void ThaModulePwePwRegsShow(AtPw pw);
void ThaModulePwePwDebug(AtPw pw);
void ThaModulePweCacheUsageGet(ThaModulePwe self, tThaPweCache *cache);
eBool ThaModulePweCanAccessCacheUsage(ThaModulePwe self);
void ThaModulePweEthFlowRegsShow(AtEthFlow flow);

/* PTCH */
eAtRet ThaModulePwePwPtchInsertionModeSet(ThaModulePwe self, AtPw pw, eAtPtchMode insertMode);
eAtPtchMode ThaModulePwePwPtchInsertionModeGet(ThaModulePwe self, AtPw pw);
eAtRet ThaModulePwePwPtchServiceEnable(ThaModulePwe self, AtPw pw, eBool enable);
eBool ThaModulePwePwPtchServiceIsEnabled(ThaModulePwe self, AtPw pw);
eAtRet ThaModulePweEthFlowPtchInsertionModeSet(ThaModulePwe self, AtEthFlow flow, eAtPtchMode insertMode);
eAtPtchMode ThaModulePweEthFlowPtchInsertionModeGet(ThaModulePwe self, AtEthFlow flow);
eAtRet ThaModulePweEthFlowPtchServiceEnable(ThaModulePwe self, AtEthFlow flow, eBool enable);
eBool ThaModulePweEthFlowPtchServiceIsEnabled(ThaModulePwe self, AtEthFlow flow);

/* PW grouping */
uint32 ThaModulePweV2Pseudowire_Transmit_HSPW_Protection_Control(ThaModulePweV2 self, uint32 groupIdx);
uint32 ThaModulePweV2Pseudowire_Transmit_UPSR_Group_Control(ThaModulePweV2 self, uint32 groupIdx);
uint32 ThaModulePweV2Pseudowire_Transmit_UPSR_and_HSPW_Control(ThaModulePweV2 self, uint32 pwIdx);
uint32 ThaModulePweV2Pseudowire_Transmit_HSPW_Label_Control(ThaModulePweV2 self, uint32 pwIdx);
eAtRet ThaModulePweV2HsPwLabelSet(ThaModulePweV2 self, AtPw pw, uint32 label);
uint32 ThaModulePweV2HsPwLabelGet(ThaModulePweV2 self, AtPw pw);
eAtRet ThaModulePweV2ApsGroupEnable(ThaModulePweV2 self, AtPwGroup pwGroup, eBool enable);
eBool ThaModulePweV2ApsGroupIsEnabled(ThaModulePweV2 self, AtPwGroup pwGroup);
eAtRet ThaModulePweV2HsGroupLabelSetSelect(ThaModulePweV2 self, AtPwGroup pwGroup, eAtPwGroupLabelSet labelSet);
eAtRet ThaModulePweV2HsGroupSelectedLabelSetGet(ThaModulePweV2 self, AtPwGroup pwGroup);
eAtRet ThaModulePweV2ApsGroupPwAdd(ThaModulePweV2 self, AtPwGroup pwGroup, AtPw pwAdapter);
eAtRet ThaModulePweV2ApsGroupPwRemove(ThaModulePweV2 self, AtPwGroup pwGroup, AtPw pwAdapter);
eAtRet ThaModulePweV2HsGroupPwAdd(ThaModulePweV2 self, AtPwGroup pwGroup, AtPw pwAdapter);
eAtRet ThaModulePweV2HsGroupPwRemove(ThaModulePweV2 self, AtPwGroup pwGroup, AtPw pwAdapter);
eAtRet ThaModulePweV2HsGroupEnable(ThaModulePweV2 self, AtPwGroup pwGroup, eBool enable);
eBool ThaModulePweV2HsGroupIsEnabled(ThaModulePweV2 self, AtPwGroup pwGroup);

/* TxPwActiveForce */
eAtRet ThaModulePweV2ApsGroupPwHwAdd(ThaModulePweV2 self, uint32 pwGroupHwId, AtPw pwAdapter);
eAtRet ThaModulePweV2ApsGroupPwIsInHwGroup(ThaModulePweV2 self, uint32 pwGroupHwId, AtPw pwAdapter);
eAtRet ThaModulePweV2ApsGroupHwEnable(ThaModulePweV2 self, uint32 pwGroupHwId, eBool enable);
eBool ThaModulePweV2ApsGroupHwIsEnabled(ThaModulePweV2 self, uint32 pwGroupHwId);
eAtRet ThaModulePweTxPwActiveForce(ThaModulePwe self, AtPw pw, eBool enabled);
eBool ThaModulePweTxPwActiveIsForced(ThaModulePwe self, AtPw pw);

/* Subport VLAN */
eAtRet ThaModulePweSubPortVlanBypass(ThaModulePwe self, eBool enable);
eBool ThaModulePweSubPortVlanIsBypassed(ThaModulePwe self);

/* Utils */
eBool ThaModulePweUtilPwCircuitIsDe1OverStm(AtPw pw);
eBool ThaModulePweUtilPwCircuitIsLiuDe1(AtPw pw);

/* IDLE pattern to insert for TDM to PW direction in case rx TDM alarm los/lof/ais */
eAtRet ThaModulePwePwIdleCodeSet(ThaModulePwe self, ThaPwAdapter pwAdapter, uint8 idleCode);
eAtRet ThaModulePwePwCesopCasIdleCode1Set(ThaModulePwe self, ThaPwAdapter pwAdapter, uint8 abcd1);
uint8  ThaModulePwePwCesopCasIdleCode1Get(ThaModulePwe self, ThaPwAdapter pwAdapter);
eAtRet ThaModulePwePwIdleCodeInsertionEnable(ThaModulePwe self, ThaPwAdapter pwAdapter, eBool autoIdleEnable);

/* Product concretes */
AtModule Tha60030080ModulePweNew(AtDevice device);
AtModule Tha60031021ModulePweNew(AtDevice device);
AtModule Tha60150011ModulePweNew(AtDevice device);
AtModule Tha61031031ModulePweNew(AtDevice device);
AtModule Tha60210011ModulePweNew(AtDevice device);
AtModule Tha60210051ModulePweNew(AtDevice device);
AtModule ThaPdhPwProductModulePweNew(AtDevice device);
AtModule Tha60210021ModulePweNew(AtDevice device);
AtModule Tha60210031ModulePweNew(AtDevice device);
AtModule Tha60210012ModulePweNew(AtDevice device);
AtModule Tha60210061ModulePweNew(AtDevice device);
AtModule Tha60290011ModulePweNew(AtDevice device);
AtModule Tha60290022ModulePweNew(AtDevice device);
AtModule Tha60291011ModulePweNew(AtDevice device);
AtModule Tha60291022ModulePweNew(AtDevice device);
AtModule Tha60290061ModulePweNew(AtDevice device);
AtModule Tha60290081ModulePweNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEPWE_H_ */


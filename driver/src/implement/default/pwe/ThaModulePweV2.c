/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PWE
 *
 * File        : ThaModulePweV2.c
 *
 * Created Date: Sep 10, 2012
 *
 * Description : PWE internal module
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaModulePweInternal.h"
#include "ThaPweRegV2.h"
#include "ThaModulePwe.h"
#include "../pw/ThaModulePwV2Reg.h"
#include "../pw/ThaModulePw.h"
#include "../util/ThaUtil.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaMaxPsnHdrLen   64

#define cMacLen 6

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaModulePweV2)self)
#define mPwHwId(pw_) AtChannelHwIdGet((AtChannel)pw_)
#define mBaseAddress(self) ThaModulePweBaseAddress((ThaModulePwe)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaModulePweV2Methods m_methods;

/* Override */
static tAtModuleMethods     m_AtModuleOverride;
static tThaModulePweMethods m_ThaModulePweOverride;

/* Save super implementations */
static const tThaModulePweMethods *m_ThaModulePweMethods = NULL;
static const tAtModuleMethods     *m_AtModuleMethods     = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
mDefineMaskShift(ThaModulePwe, PWPdhPlaAsmPldTypeCtrl)
mDefineMaskShift(ThaModulePwe, PWPdhPlaAsmPldSizeCtrl)
mDefineRegAdress(ThaModulePweV2, PwTxEthHdrValCtrl)

static eAtRet PwRtpEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    uint32 regAddr, regVal;
    uint8 rtpEnable = enable ? 1 : 0;

    regAddr = cThaRegPwTxEthHdrLengthCtrl + mPwPweOffset(self, pw);
    regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cThaRegPwTxEthPwRtpEn, rtpEnable);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static uint32 PwTxEnCtrl(ThaModulePwe self)
    {
    AtUnused(self);
    return 0x321000;
    }

static eAtRet PwSupressEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    uint32 regAddr, regVal;

    regAddr = mMethodsGet(self)->PWPdhPlaPldSizeCtrl(self) + mPwPlaOffset(self, pw);
    regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cThaPWPdhPlaAsmPldSupress, enable ? 1 : 0);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool PwSupressIsEnabled(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr, regVal;

    regAddr = mMethodsGet(self)->PWPdhPlaPldSizeCtrl(self) + mPwPlaOffset(self, pw);
    regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);

    return (regVal & cThaPWPdhPlaAsmPldSupressMask) ? cAtTrue : cAtFalse;
    }

static eAtRet PwCwEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    uint32 regAddr, regVal;

    if (!enable)
        return cAtErrorModeNotSupport;

    regAddr = cThaRegPwTxEthHdrLengthCtrl + mPwPweOffset(self, pw);
    regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cThaRegPwTxEthHdrLengthCtrlTxEthPwCwType, AtPwIsTdmPw(pw) ? 0 : 1);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    regAddr = mMethodsGet(self)->PwTxEnCtrl(self) + mPwPweOffset(self, pw);
    regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cThaRegPwTxPwCwType, AtPwIsTdmPw(pw) ? 0 : 1);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eAtRet PwCwAutoTxLBitEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    uint32 regAddr, regVal;

    regAddr = mMethodsGet(self)->PwTxEnCtrl(self) + mPwPweOffset(self, pw);
    regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cThaRegPwTxPwAutoLbit, enable ? 0 : 1);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool PwCwAutoTxLBitIsEnabled(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr, regVal;

    regAddr = mMethodsGet(self)->PwTxEnCtrl(self) + mPwPweOffset(self, pw);
    regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    return (regVal & cThaRegPwTxPwAutoLbitMask) ? cAtFalse : cAtTrue;
    }

static eAtRet PwCwAutoTxRBitEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    uint32 regAddr, regVal;

    regAddr = cThaRegPWPdhTxRBitControl + mPwPweOffset(self, pw);
    regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cThaPWPdhTxRBitAuto, enable ? 0 : 1);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool PwCwAutoTxRBitIsEnabled(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr;
    regAddr = cThaRegPWPdhTxRBitControl + mPwPweOffset(self, pw);
    return (mChannelHwRead(pw, regAddr, cThaModulePwe) & cThaPWPdhTxRBitAutoMask) ? cAtFalse : cAtTrue;
    }

static eAtRet PwCwAutoTxMBitEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    uint32 regAddr, regVal;

    regAddr = mMethodsGet(self)->PwTxEnCtrl(self) + mPwPweOffset(self, pw);
    regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cThaRegPwTxPwAutoMbit, enable ? 0 : 1);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool PwCwAutoTxMBitIsEnabled(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr, regVal;

    regAddr = mMethodsGet(self)->PwTxEnCtrl(self) + mPwPweOffset(self, pw);
    regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    return (regVal & cThaRegPwTxPwAutoMbitMask) ? cAtFalse : cAtTrue;
    }

static uint32 PwTxPacketsGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
    return mChannelHwRead(pw, cThaRegPmcPwTxPktCnt(mRo(clear)) + mPwPweOffset(self, pw), cThaModulePwPmc);
    }

static uint32 PwTxBytesGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
    return mChannelHwRead(pw, cThaRegPmcPwTxPldOctCnt(mRo(clear)) + mPwPweOffset(self, pw), cThaModulePwPmc);
    }

static uint32 PwTxLbitPacketsGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
    return mChannelHwRead(pw, cThaRegPmcPwTxLbitPktCnt(mRo(clear)) + mPwPweOffset(self, pw), cThaModulePwPmc);
    }

static uint32 PwTxRbitPacketsGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
    return mChannelHwRead(pw, cThaRegPmcPwTxRbitPktCnt(mRo(clear)) + mPwPweOffset(self, pw), cThaModulePwPmc);
    }

static eBool IsSimulated(ThaModulePwe self)
    {
    return AtDeviceIsSimulated(AtModuleDeviceGet((AtModule)self));
    }

static eAtRet PwEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    uint32 regAddr, regVal;

    regAddr = mMethodsGet(self)->PwTxEnCtrl(self) + mPwPweOffset(self, pw);
    regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cThaRegPwTxEn, enable ? 1 : 0);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool PwIsEnabled(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr, regVal;

    regAddr = mMethodsGet(self)->PwTxEnCtrl(self) + mPwPweOffset(self, pw);
    regVal = mChannelHwRead(pw, regAddr, cThaModulePwe);

    return (regVal & cThaRegPwTxEnMask) ? cAtTrue : cAtFalse;
    }

static eAtRet NumVlansSet(ThaModulePwe self, AtPw pw, uint8 numVlans)
    {
    uint32 regAddr, regVal;

    regAddr = cThaRegPwTxEthHdrLengthCtrl + mPwPweOffset(self, pw);
    regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cThaRegPwTxEthHdrLengthCtrlTxNumVlan, numVlans);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static ThaModulePw PwModule(ThaModulePwe self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return (ThaModulePw)AtDeviceModuleGet(device, cAtModulePw);
    }

static uint32 HeaderOffset(ThaModulePwe self, AtPw pw, uint8 dwordId)
    {
    ThaModulePw pwModule = PwModule(self);
    return (mPwHwId(pw) * 16) + dwordId + ThaModulePwePartOffset(self, ThaModulePwPartOfPw(pwModule, pw));
    }

static eAtRet HeaderWrite(ThaModulePwe self, AtPw pw, uint8 *buffer, uint8 headerLenInByte, AtPwPsn psn)
    {
    uint8 i;
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 txEthHdrValCtrlRegAddress = mMethodsGet(mThis(self))->PwTxEthHdrValCtrl(mThis(self)) ;
    AtUnused(psn);

    /* SMAC is property of PW, so the buffer contains this field. But this product
     * does not use PW SMAC, it internally uses SMAC of Ethernet port for
     * instead. So, SMAC must be stripped before the full header can be written
     * to hardware */
    mMethodsGet(osal)->MemCpy(osal, &(buffer[cMacLen]), buffer, cMacLen);
    buffer = &(buffer[cMacLen]);
    headerLenInByte = (uint8)(headerLenInByte - cMacLen);

    /* Write header into registers */
    for (i = 0; i < (cThaMaxPsnHdrLen / 4); i++)
        {
        uint32 regAddr = txEthHdrValCtrlRegAddress + HeaderOffset(self, pw, i);
        uint32 regVal  = ThaPktUtilPutDataToRegister(buffer, i);
        mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);
        }

    return cAtOk;
    }

static eAtRet HeaderRead(ThaModulePwe self, AtPw pw, uint8 *buffer, uint8 bufferSize)
    {
    uint8 i, byte_i = 0;
    uint32 txEthHdrValCtrlRegAddress = mMethodsGet(mThis(self))->PwTxEthHdrValCtrl(mThis(self)) ;
	AtUnused(bufferSize);

    /* Header returned from hardware does not include SMAC, but in the view of
     * PW, SMAC must be seen. This product use SMAC of Ethernet port. So, put SMAC
     * back to buffer */
    for (i = 0; i < (cThaMaxPsnHdrLen / 4); i++)
        {
        uint32 regAddr, regVal;

        regAddr = (uint32)(txEthHdrValCtrlRegAddress + HeaderOffset(self, pw, i));
        regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);

        buffer[byte_i++] = (uint8)(regVal >> 24);
        buffer[byte_i++] = (uint8)(regVal >> 16);

        /* Put SMAC got from Ethernet port */
        if (byte_i == cMacLen)
            {
            AtOsal osal = AtSharedDriverOsalGet();
            AtEthPort port = AtPwEthPortGet(pw);
            uint8 smac[cMacLen];
            eAtRet ret = AtEthPortSourceMacAddressGet(port, smac);
            if (ret != cAtOk)
                mMethodsGet(osal)->MemInit(osal, smac, 0, sizeof(smac));

            /* Put */
            mMethodsGet(osal)->MemCpy(osal, &(buffer[byte_i]), smac, cMacLen);
            byte_i = (uint8)(byte_i + cMacLen);
            }

        buffer[byte_i++] = (uint8)(regVal >> 8);
        buffer[byte_i++] = (uint8) regVal;
        }

    return cAtOk;
    }

static uint32 PwTxMbitPacketsGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
    if (ThaModulePwMbitCounterIsSupported(PwModule(self)))
        return mChannelHwRead(pw, cThaRegPmcPwTxMbitCnt((clear) ? 0 : 1) + mPwPweOffset(self, pw), cThaModulePwe);

    return 0;
    }

static uint32 PwTxNbitPacketsGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
	AtUnused(clear);
	AtUnused(pw);
	AtUnused(self);
    return 0;
    }

static uint32 PwTxPbitPacketsGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
	AtUnused(clear);
	AtUnused(pw);
	AtUnused(self);
    return 0;
    }

static uint8 HwPsnType(AtPwPsn psn)
    {
    eAtPwPsnType lowerPsnType;
    if (AtPwPsnTypeGet(psn) != cAtPwPsnTypeUdp)
        return 0;

    lowerPsnType = AtPwPsnTypeGet(AtPwPsnLowerPsnGet(psn));
    if (lowerPsnType == cAtPwPsnTypeIPv4)
        return 1;
    if (lowerPsnType == cAtPwPsnTypeIPv6)
        return 2;

    return 0;
    }

static eAtRet PwPsnTypeSet(ThaModulePwe self, AtPw pw, AtPwPsn psn)
    {
    uint32 regAddr, regVal;

    regAddr = cThaRegPwTxEthHdrLengthCtrl + mPwPweOffset(self, pw);
    regVal = mChannelHwRead(pw, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cThaRegPwTxEthHdrLengthCtrlTxEthPwPsnType, HwPsnType(psn));

    /* Number of outer labels in case of MPLS */
    if (AtPwPsnTypeGet(psn) == cAtPwPsnTypeMpls)
        mRegFieldSet(regVal, cThaRegPwTxEthHdrLengthCtrlTxNumMplsOutLb, AtPwMplsPsnNumberOfOuterLabelsGet((AtPwMplsPsn)psn));

    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool PwRemovingIsReady(ThaModulePwe self, AtPw pw)
    {
    const uint32 cTimeoutMs = 50;
    uint32 regAddr = cThaRegPDHPWRemovingProtocol + ThaPwPartOffset(pw, cThaModulePwe);
    uint32 regVal  = 0;
    uint32 elapseTime = 0;
    tAtOsalCurTime startTime, curTime;
    AtOsal osal = AtSharedDriverOsalGet();
	AtUnused(self);

    if (IsSimulated(self))
        return cAtTrue;

    mMethodsGet(osal)->CurTimeGet(osal, &startTime);

    while (elapseTime <= cTimeoutMs)
        {
        regVal = mChannelHwRead(pw, regAddr, cThaModulePwe);
        if (mRegField(regVal, cThaRemovePwState) == cThaRemovePwStateReady)
            return cAtTrue;

        /* Retry */
        mMethodsGet(osal)->CurTimeGet(osal, &curTime);
        elapseTime = mTimeIntervalInMsGet(startTime, curTime);
        }

    AtChannelLog((AtChannel)pw, cAtLogLevelWarning, AtSourceLocation, "Cannot wait before removing\r\n");

    return cAtFalse;
    }

static eAtRet PwRemovingStart(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr = cThaRegPDHPWRemovingProtocol + ThaPwPartOffset(pw, cThaModulePwe);
    uint32 regVal  = 0;

    /* Make request */
    mRegFieldSet(regVal, cThaRemovePwId, mPwHwId(pw));
    mRegFieldSet(regVal, cThaRemovePwState, cThaRemovePwStateStart);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    /* Wait till hardware done */
    return PwRemovingIsReady(self, pw) ? cAtOk : cAtErrorDevBusy;
    }

static eAtRet PwRemovingFinish(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr = cThaRegPDHPWRemovingProtocol + ThaPwPartOffset(pw, cThaModulePwe);
    uint32 regVal  = 0;
	AtUnused(self);

    mRegFieldSet(regVal, cThaRemovePwId, mPwHwId(pw));
    mRegFieldSet(regVal, cThaRemovePwState, cThaRemovePwStateStop);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eAtRet PartDefaultSet(ThaModulePwe self, uint8 partId)
    {
    uint16 pw_i;
    AtModulePw pwModules = (AtModulePw)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModulePw);
    uint32 partOffset = ThaModulePwePartOffset((ThaModulePwe)self, partId);

    for (pw_i = 0; pw_i < ThaModulePwNumPwsPerPart((ThaModulePw)pwModules); pw_i++)
        mModuleHwWrite(self, mMethodsGet(self)->PwTxEnCtrl(self) + partOffset + pw_i, 0);

    /* Insert PAD when control word plus payload length is less than 64 bytes */
    mModuleHwWrite(self, cThaRegPwTransmitEthPadControl + partOffset, 1);

    return cAtOk;
    }

static uint8 NumParts(ThaModulePwe self)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    return ThaDeviceNumPartsOfModule(device, cThaModulePwe);
    }

static eAtRet DefaultSet(ThaModulePwe self)
    {
    uint8 part_i;
    eAtRet ret = cAtOk;

    for (part_i = 0; part_i < NumParts(self); part_i++)
        ret |= PartDefaultSet(self, part_i);

    return ret;
    }

static uint32 PwRamStatusRegister(ThaModulePweV2 self)
    {
	AtUnused(self);
    return 0x321800;
    }

static eAtRet PwRamStatusClear(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr;
    uint32 longReg[cThaLongRegMaxSize];
    uint32 offset = mPwPlaOffset(self, pw);
    AtOsal osal;

    osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, longReg, 0, sizeof(longReg));

    regAddr = mMethodsGet(mThis(self))->PwRamStatusRegister(mThis(self)) + offset;
    mChannelHwLongWrite(pw, regAddr, longReg, cThaLongRegMaxSize, cThaModulePwe);

    regAddr = mMethodsGet(self)->PWPdhPlaEngStat(self) + offset;
    mChannelHwLongWrite(pw, regAddr, longReg, cThaLongRegMaxSize, cThaModulePwe);

    return cAtOk;
    }

static eAtRet PwTxLBitForce(ThaModulePwe self, AtPw pw, eBool force)
    {
    uint32 regAddr = mMethodsGet(self)->PwTxEnCtrl(self) + mPwPweOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);

    mRegFieldSet(regVal, cThaRegPwTxPwLbitForce, mBoolToBin(force));
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool PwTxLBitIsForced(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr = mMethodsGet(self)->PwTxEnCtrl(self) + mPwPweOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    return (regVal & cThaRegPwTxPwLbitForceMask) ? cAtTrue : cAtFalse;
    }

static eAtRet PwTxRBitForce(ThaModulePwe self, AtPw pw, eBool force)
    {
    uint32 regAddr = cThaRegPWPdhTxRBitControl + mPwPweOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);

    mRegFieldSet(regVal, cThaPWPdhTxRBitForce, mBoolToBin(force));
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool PwTxRBitIsForced(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr = cThaRegPWPdhTxRBitControl + mPwPweOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    return (regVal & cThaPWPdhTxRBitForceMask) ? cAtTrue : cAtFalse;
    }

static eAtRet PwTxMBitForce(ThaModulePwe self, AtPw pw, eBool force)
    {
    uint32 regAddr = mMethodsGet(self)->PwTxEnCtrl(self) + mPwPweOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);

    mRegFieldSet(regVal, cThaRegPwTxPwMbitForce, mBoolToBin(force));
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool PwTxMBitIsForced(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr = mMethodsGet(self)->PwTxEnCtrl(self) + mPwPweOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    return (regVal & cThaRegPwTxPwMbitForceMask) ? cAtTrue : cAtFalse;
    }

static eAtRet PwStmPortSet(ThaModulePwe self, AtPw pwAdapter, uint8 lineId)
    {
    uint32 regAddr = cThaRegPwTxEthHdrLengthCtrl + mPwPweOffset(self, pwAdapter);
    uint32 regVal  = mChannelHwRead(pwAdapter, regAddr, cThaModulePwe);

    mRegFieldSet(regVal, cThaRegPwTxEthPwStmLineId, lineId);
    mChannelHwWrite(pwAdapter, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static uint32 BaseAddress(ThaModulePwe self)
    {
    AtUnused(self);
    return 0x300000;
    }

static uint32 Pseudowire_Transmit_HSPW_Protection_Control(ThaModulePweV2 self, uint32 groupIdx)
    {
    return mBaseAddress(self) + 0x008000 + groupIdx;
    }

static uint32 Pseudowire_Transmit_UPSR_Group_Control(ThaModulePweV2 self, uint32 groupIdx)
    {
    return mBaseAddress(self) + 0x007000 + groupIdx;
    }

static uint32 Pseudowire_Transmit_UPSR_and_HSPW_Control(ThaModulePweV2 self, uint32 pwIdx)
    {
    return mBaseAddress(self) + 0x006000 + pwIdx;
    }

static uint32 Pseudowire_Transmit_HSPW_Label_Control(ThaModulePweV2 self, uint32 pwIdx)
    {
    return mBaseAddress(self) + 0x005000 + pwIdx;
    }

static void HspwDefaultSet(AtModule self)
    {
    AtModulePw modulePw = (AtModulePw)AtDeviceModuleGet(AtModuleDeviceGet(self), cAtModulePw);
    uint32 maxHspws = AtModulePwMaxHsGroupsGet(modulePw);
    uint32 maxPwg = AtModulePwMaxApsGroupsGet(modulePw);
    uint32 maxPws = AtModulePwMaxPwsGet(modulePw);
    uint32 i, address;
    
    for (i = 0; i < maxHspws; i++)
        {
        address = ThaModulePweV2Pseudowire_Transmit_HSPW_Protection_Control((ThaModulePweV2)self, i);
        mModuleHwWrite(self, address, 0);
        }
        
    for (i = 0; i < maxPwg; i++)
        {
        address = ThaModulePweV2Pseudowire_Transmit_UPSR_Group_Control((ThaModulePweV2)self, i);
        mModuleHwWrite(self, address, 0);
        }
        
    for (i = 0; i < maxPws; i++)
        {
        address = ThaModulePweV2Pseudowire_Transmit_UPSR_and_HSPW_Control((ThaModulePweV2)self, i);
        mModuleHwWrite(self, address, 0);
        }
    }

static uint32 GroupLabelSetSw2Hw(eAtPwGroupLabelSet labelSet)
    {
    return (labelSet == cAtPwGroupLabelSetPrimary) ? 0 : 1;
    }

static eAtPwGroupLabelSet GroupLabelSetHw2Sw(uint32 labelSet)
    {
    return (labelSet == 0) ? cAtPwGroupLabelSetPrimary : cAtPwGroupLabelSetBackup;
    }

static eAtRet ApsGroupHwEnable(ThaModulePweV2 self, uint32 pwGroupHwId, eBool enable)
    {
    uint32 regAddr = ThaModulePweV2Pseudowire_Transmit_UPSR_Group_Control(self, pwGroupHwId);
    mModuleHwWrite(self, regAddr, mBoolToBin(enable));

    return cAtOk;
    }

static eBool ApsGroupHwIsEnabled(ThaModulePweV2 self, uint32 pwGroupHwId)
    {
    uint32 regAddr = ThaModulePweV2Pseudowire_Transmit_UPSR_Group_Control(self, pwGroupHwId);
    return mBinToBool(mModuleHwRead(self, regAddr));
    }

static eAtRet ApsGroupPwHwAdd(ThaModulePweV2 self, uint32 pwGroupHwId, AtPw pwAdapter)
    {
    uint32 pwAdapterHwId = AtChannelHwIdGet((AtChannel)pwAdapter);
    uint32 regAddr = ThaModulePweV2Pseudowire_Transmit_UPSR_and_HSPW_Control(self, pwAdapterHwId);
    uint32 regVal = mModuleHwRead(self, regAddr);

    mRegFieldSet(regVal, cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwUpsrUseValue_, 1);
    mRegFieldSet(regVal, cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwUpsrGrpValue_, pwGroupHwId);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool ApsGroupPwIsInHwGroup(ThaModulePweV2 self, uint32 pwGroupHwId, AtPw pwAdapter)
    {
    uint32 pwAdapterHwId = AtChannelHwIdGet((AtChannel)pwAdapter);
    uint32 regAddr = ThaModulePweV2Pseudowire_Transmit_UPSR_and_HSPW_Control(self, pwAdapterHwId);
    uint32 regVal = mModuleHwRead(self, regAddr);
    uint32 isInGroup, hwGroupId;

    isInGroup = mRegField(regVal, cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwUpsrUseValue_);
    hwGroupId = mRegField(regVal, cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwUpsrGrpValue_);

    if ((hwGroupId == pwGroupHwId) && (isInGroup == 1))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet ApsGroupPwAdd(ThaModulePweV2 self, AtPwGroup pwGroup, AtPw pwAdapter)
    {
    uint32 pwGroupHwId = AtPwGroupIdGet(pwGroup);
    return mMethodsGet(self)->ApsGroupPwHwAdd(self, pwGroupHwId, pwAdapter);
    }

static eAtRet ApsGroupPwRemove(ThaModulePweV2 self, AtPwGroup pwGroup, AtPw pwAdapter)
    {
    uint32 regAddr = ThaModulePweV2Pseudowire_Transmit_UPSR_and_HSPW_Control(self, AtChannelHwIdGet((AtChannel)pwAdapter));
    uint32 regVal = mModuleHwRead(self, regAddr);

    AtUnused(pwGroup);

    mRegFieldSet(regVal, cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwUpsrUseValue_, 0);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet HsGroupPwAdd(ThaModulePweV2 self, AtPwGroup pwGroup, AtPw pwAdapter)
    {
    uint32 regAddr = ThaModulePweV2Pseudowire_Transmit_UPSR_and_HSPW_Control(self, AtChannelHwIdGet((AtChannel)pwAdapter));
    uint32 regVal = mModuleHwRead(self, regAddr);

    mRegFieldSet(regVal, cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwHspwUseValue_, 1);
    mRegFieldSet(regVal, cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwHspwGrpValue_, AtPwGroupIdGet(pwGroup));
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet HsGroupPwRemove(ThaModulePweV2 self, AtPwGroup pwGroup, AtPw pwAdapter)
    {
    uint32 regAddr = ThaModulePweV2Pseudowire_Transmit_UPSR_and_HSPW_Control(self, AtChannelHwIdGet((AtChannel)pwAdapter));
    uint32 regVal = mModuleHwRead(self, regAddr);

    AtUnused(pwGroup);

    mRegFieldSet(regVal, cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwHspwUseValue_, 0);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool ShouldResetAllPwGroups(ThaModulePweV2 self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet ApsGroupEnable(ThaModulePweV2 self, AtPwGroup pwGroup, eBool enable)
    {
	uint32 pwGroupHwId = AtPwGroupIdGet(pwGroup);
    return mMethodsGet(self)->ApsGroupHwEnable(self, pwGroupHwId, enable);
    }

static eBool ApsGroupIsEnabled(ThaModulePweV2 self, AtPwGroup pwGroup)
    {
    uint32 pwGroupHwid = AtPwGroupIdGet(pwGroup);
    return mMethodsGet(self)->ApsGroupHwIsEnabled(self, pwGroupHwid);
    }

static eAtRet HsGroupLabelSetSelect(ThaModulePweV2 self, AtPwGroup pwGroup, eAtPwGroupLabelSet labelSet)
    {
    uint32 regAddr = ThaModulePweV2Pseudowire_Transmit_HSPW_Protection_Control(self, AtPwGroupIdGet(pwGroup));
    uint32 regVal;

    regVal = GroupLabelSetSw2Hw(labelSet);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet HsGroupSelectedLabelSetGet(ThaModulePweV2 self, AtPwGroup pwGroup)
    {
    uint32 regAddr = ThaModulePweV2Pseudowire_Transmit_HSPW_Protection_Control(self, AtPwGroupIdGet(pwGroup));
    uint32 regVal  = mModuleHwRead(self, regAddr);

    return GroupLabelSetHw2Sw(regVal);
    }

static eAtRet HsGroupEnable(ThaModulePweV2 self, AtPwGroup pwGroup, eBool enable)
    {
    AtUnused(self);
    AtUnused(pwGroup);
    AtUnused(enable);
    return cAtErrorModeNotSupport;
    }

static eBool HsGroupIsEnabled(ThaModulePweV2 self, AtPwGroup pwGroup)
    {
    AtUnused(self);
    AtUnused(pwGroup);
    return cAtFalse;
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Init(self);

    if (mMethodsGet(mThis(self))->ShouldResetAllPwGroups(mThis(self)))
        HspwDefaultSet(self);

    return ret;
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static void PwPweRegsShow(ThaModulePwe self, AtPw pw)
    {
    uint32 offset = mPwPweOffset(self, pw);
    uint32 address = cThaRegPwTxEthHdrLengthCtrl + offset;
    ThaModulePweRegDisplay(pw, "Pseudowire Transmit Ethernet Header Length Control", address);

    address = mMethodsGet(self)->PwTxEnCtrl(self) + offset;
    ThaModulePweRegDisplay(pw, "Pseudowire Transmit Enable Control", address);

    address = cThaRegPWPdhTxRBitControl + offset;
    ThaModulePweRegDisplay(pw, "Thalassa PDHPW TX R-Bit control", address);
    }

static eAtRet TxPwHwActiveForce(ThaModulePwe self, AtPw pwAdapter, eBool enable)
    {
    ThaModulePweV2 pweV2 = (ThaModulePweV2)self;
    uint32 pwGroupId = AtChannelIdGet((AtChannel)pwAdapter);

    /* Add PW to group if it has not been there */
    if (!ThaModulePweV2ApsGroupPwIsInHwGroup(pweV2, pwGroupId, pwAdapter))
        {
        eAtRet ret = ThaModulePweV2ApsGroupPwHwAdd(pweV2, pwGroupId, pwAdapter);
        if (ret != cAtOk)
            return ret;
        }

    /* Use group to control packets transmitted to PSN */
    return ThaModulePweV2ApsGroupHwEnable(pweV2, pwGroupId, enable);
    }

static eBool TxPwHwActiveIsForced(ThaModulePwe self, AtPw pwAdapter)
    {
    ThaModulePweV2 pweV2 = (ThaModulePweV2)self;
    uint32 pwGroupId = AtChannelIdGet((AtChannel)pwAdapter);

    if (ThaModulePweV2ApsGroupPwIsInHwGroup(pweV2, pwGroupId, pwAdapter))
        return ThaModulePweV2ApsGroupHwIsEnabled(pweV2, pwGroupId);

    return cAtFalse;
    }

static eAtRet TxPwActiveForce(ThaModulePwe self, AtPw pw, eBool enable)
    {
    if (mMethodsGet(self)->TxPwActiveHwIsReady(self))
        return TxPwHwActiveForce(self, pw, enable);

    return m_ThaModulePweMethods->TxPwActiveForce(self, pw, enable);
    }

static eBool TxPwActiveIsForced(ThaModulePwe self, AtPw pw)
    {
    if (mMethodsGet(self)->TxPwActiveHwIsReady(self))
        return TxPwHwActiveIsForced(self, pw);
    return m_ThaModulePweMethods->TxPwActiveIsForced(self, pw);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void OverrideThaModulePwe(AtModule self)
    {
    ThaModulePwe pweModule = (ThaModulePwe)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePweMethods = mMethodsGet(pweModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePweOverride, m_ThaModulePweMethods, sizeof(m_ThaModulePweOverride));

        /* Override bit fields */
        mBitFieldOverride(ThaModulePwe, m_ThaModulePweOverride, PWPdhPlaAsmPldTypeCtrl)
        mBitFieldOverride(ThaModulePwe, m_ThaModulePweOverride, PWPdhPlaAsmPldSizeCtrl)

        /* Override methods */
        mMethodOverride(m_ThaModulePweOverride, DefaultSet);
        mMethodOverride(m_ThaModulePweOverride, PwRemovingStart);
        mMethodOverride(m_ThaModulePweOverride, PwRemovingFinish);
        mMethodOverride(m_ThaModulePweOverride, PwRtpEnable);
        mMethodOverride(m_ThaModulePweOverride, PwCwEnable);
        mMethodOverride(m_ThaModulePweOverride, PwCwAutoTxLBitEnable);
        mMethodOverride(m_ThaModulePweOverride, PwCwAutoTxLBitIsEnabled);
        mMethodOverride(m_ThaModulePweOverride, PwCwAutoTxRBitEnable);
        mMethodOverride(m_ThaModulePweOverride, PwCwAutoTxRBitIsEnabled);
        mMethodOverride(m_ThaModulePweOverride, PwCwAutoTxMBitEnable);
        mMethodOverride(m_ThaModulePweOverride, PwCwAutoTxMBitIsEnabled);
        mMethodOverride(m_ThaModulePweOverride, PwTxPacketsGet);
        mMethodOverride(m_ThaModulePweOverride, PwTxBytesGet);
        mMethodOverride(m_ThaModulePweOverride, PwTxLbitPacketsGet);
        mMethodOverride(m_ThaModulePweOverride, PwTxRbitPacketsGet);
        mMethodOverride(m_ThaModulePweOverride, PwTxMbitPacketsGet);
        mMethodOverride(m_ThaModulePweOverride, PwTxNbitPacketsGet);
        mMethodOverride(m_ThaModulePweOverride, PwTxPbitPacketsGet);
        mMethodOverride(m_ThaModulePweOverride, PwSupressEnable);
        mMethodOverride(m_ThaModulePweOverride, PwSupressIsEnabled);
        mMethodOverride(m_ThaModulePweOverride, PwEnable);
        mMethodOverride(m_ThaModulePweOverride, PwIsEnabled);
        mMethodOverride(m_ThaModulePweOverride, NumVlansSet);
        mMethodOverride(m_ThaModulePweOverride, HeaderWrite);
        mMethodOverride(m_ThaModulePweOverride, HeaderRead);
        mMethodOverride(m_ThaModulePweOverride, PwPsnTypeSet);
        mMethodOverride(m_ThaModulePweOverride, PwRamStatusClear);
        mMethodOverride(m_ThaModulePweOverride, PwTxLBitForce);
        mMethodOverride(m_ThaModulePweOverride, PwTxLBitIsForced);
        mMethodOverride(m_ThaModulePweOverride, PwTxRBitForce);
        mMethodOverride(m_ThaModulePweOverride, PwTxRBitIsForced);
        mMethodOverride(m_ThaModulePweOverride, PwTxMBitForce);
        mMethodOverride(m_ThaModulePweOverride, PwTxMBitIsForced);
        mMethodOverride(m_ThaModulePweOverride, PwStmPortSet);
        mMethodOverride(m_ThaModulePweOverride, PwPweRegsShow);
        mMethodOverride(m_ThaModulePweOverride, BaseAddress);
        mMethodOverride(m_ThaModulePweOverride, TxPwActiveForce);
        mMethodOverride(m_ThaModulePweOverride, TxPwActiveIsForced);

        /* Registers */
        mMethodOverride(m_ThaModulePweOverride, PwTxEnCtrl);
        }

    mMethodsSet(pweModule, &m_ThaModulePweOverride);
    }

static void MethodsInit(ThaModulePweV2 self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, PwTxEthHdrValCtrl);
        mMethodOverride(m_methods, PwRamStatusRegister);
        mMethodOverride(m_methods, Pseudowire_Transmit_HSPW_Protection_Control);
        mMethodOverride(m_methods, Pseudowire_Transmit_UPSR_Group_Control);
        mMethodOverride(m_methods, Pseudowire_Transmit_UPSR_and_HSPW_Control);
        mMethodOverride(m_methods, Pseudowire_Transmit_HSPW_Label_Control);
        mMethodOverride(m_methods, ApsGroupPwAdd);
        mMethodOverride(m_methods, ApsGroupPwRemove);
        mMethodOverride(m_methods, HsGroupPwAdd);
        mMethodOverride(m_methods, HsGroupPwRemove);
        mMethodOverride(m_methods, ShouldResetAllPwGroups);
        mMethodOverride(m_methods, ApsGroupEnable);
        mMethodOverride(m_methods, ApsGroupIsEnabled);
        mMethodOverride(m_methods, HsGroupLabelSetSelect);
        mMethodOverride(m_methods, HsGroupSelectedLabelSetGet);
        mMethodOverride(m_methods, HsGroupEnable);
        mMethodOverride(m_methods, HsGroupIsEnabled);
        mMethodOverride(m_methods, ApsGroupPwHwAdd);
        mMethodOverride(m_methods, ApsGroupPwIsInHwGroup);
        mMethodOverride(m_methods, ApsGroupHwEnable);
        mMethodOverride(m_methods, ApsGroupHwIsEnabled);
        }

    mMethodsSet(self, &m_methods);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideThaModulePwe(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaModulePweV2);
    }

AtModule ThaModulePweV2ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModulePweObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtModule ThaModulePweV2New(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ThaModulePweV2ObjectInit(newModule, device);
    }

uint32 ThaModulePweV2Pseudowire_Transmit_HSPW_Protection_Control(ThaModulePweV2 self, uint32 groupIdx)
    {
    return mMethodsGet(self)->Pseudowire_Transmit_HSPW_Protection_Control(self, groupIdx);
    }

uint32 ThaModulePweV2Pseudowire_Transmit_UPSR_Group_Control(ThaModulePweV2 self, uint32 groupIdx)
    {
    return mMethodsGet(self)->Pseudowire_Transmit_UPSR_Group_Control(self, groupIdx);
    }

uint32 ThaModulePweV2Pseudowire_Transmit_UPSR_and_HSPW_Control(ThaModulePweV2 self, uint32 pwIdx)
    {
    return mMethodsGet(self)->Pseudowire_Transmit_UPSR_and_HSPW_Control(self, pwIdx);
    }

uint32 ThaModulePweV2Pseudowire_Transmit_HSPW_Label_Control(ThaModulePweV2 self, uint32 pwIdx)
    {
    return mMethodsGet(self)->Pseudowire_Transmit_HSPW_Label_Control(self, pwIdx);
    }

eAtRet ThaModulePweV2HsPwLabelSet(ThaModulePweV2 self, AtPw pw, uint32 label)
    {
    uint32 regAddr = ThaModulePweV2Pseudowire_Transmit_HSPW_Label_Control(self, AtChannelHwIdGet((AtChannel)pw));
    mChannelHwWrite(pw, regAddr, label, cThaModulePwe);

    return cAtOk;
    }

uint32 ThaModulePweV2HsPwLabelGet(ThaModulePweV2 self, AtPw pw)
    {
    uint32 regAddr = ThaModulePweV2Pseudowire_Transmit_HSPW_Label_Control(self, AtChannelHwIdGet((AtChannel)pw));
    return mChannelHwRead(pw, regAddr, cThaModulePwe);
    }

eAtRet ThaModulePweV2ApsGroupEnable(ThaModulePweV2 self, AtPwGroup pwGroup, eBool enable)
    {
    return mMethodsGet(self)->ApsGroupEnable(self, pwGroup, enable);
    }

eBool ThaModulePweV2ApsGroupIsEnabled(ThaModulePweV2 self, AtPwGroup pwGroup)
    {
    return mMethodsGet(self)->ApsGroupIsEnabled(self, pwGroup);
    }

eAtRet ThaModulePweV2HsGroupEnable(ThaModulePweV2 self, AtPwGroup pwGroup, eBool enable)
    {
    return mMethodsGet(self)->HsGroupEnable(self, pwGroup, enable);
    }

eBool ThaModulePweV2HsGroupIsEnabled(ThaModulePweV2 self, AtPwGroup pwGroup)
    {
    return mMethodsGet(self)->HsGroupIsEnabled(self, pwGroup);
    }

eAtRet ThaModulePweV2HsGroupLabelSetSelect(ThaModulePweV2 self, AtPwGroup pwGroup, eAtPwGroupLabelSet labelSet)
    {
    return mMethodsGet(self)->HsGroupLabelSetSelect(self, pwGroup, labelSet);
    }

eAtRet ThaModulePweV2HsGroupSelectedLabelSetGet(ThaModulePweV2 self, AtPwGroup pwGroup)
    {
    return mMethodsGet(self)->HsGroupSelectedLabelSetGet(self, pwGroup);
    }

eAtRet ThaModulePweV2ApsGroupPwAdd(ThaModulePweV2 self, AtPwGroup pwGroup, AtPw pwAdapter)
    {
    return mMethodsGet(self)->ApsGroupPwAdd(self, pwGroup, pwAdapter);
    }

eAtRet ThaModulePweV2ApsGroupPwRemove(ThaModulePweV2 self, AtPwGroup pwGroup, AtPw pwAdapter)
    {
    return mMethodsGet(self)->ApsGroupPwRemove(self, pwGroup, pwAdapter);
    }

eAtRet ThaModulePweV2HsGroupPwAdd(ThaModulePweV2 self, AtPwGroup pwGroup, AtPw pwAdapter)
    {
    return mMethodsGet(self)->HsGroupPwAdd(self, pwGroup, pwAdapter);
    }

eAtRet ThaModulePweV2HsGroupPwRemove(ThaModulePweV2 self, AtPwGroup pwGroup, AtPw pwAdapter)
    {
    return mMethodsGet(self)->HsGroupPwRemove(self, pwGroup, pwAdapter);
    }

eAtRet ThaModulePweV2ApsGroupPwHwAdd(ThaModulePweV2 self, uint32 pwGroupHwId, AtPw pwAdapter)
    {
    return mMethodsGet(self)->ApsGroupPwHwAdd(self, pwGroupHwId, pwAdapter);
    }

eAtRet ThaModulePweV2ApsGroupPwIsInHwGroup(ThaModulePweV2 self, uint32 pwGroupHwId, AtPw pwAdapter)
    {
    return mMethodsGet(self)->ApsGroupPwIsInHwGroup(self, pwGroupHwId, pwAdapter);
    }

eAtRet ThaModulePweV2ApsGroupHwEnable(ThaModulePweV2 self, uint32 pwGroupHwId, eBool enable)
    {
    return mMethodsGet(self)->ApsGroupHwEnable(self, pwGroupHwId, enable);
    }

eBool ThaModulePweV2ApsGroupHwIsEnabled(ThaModulePweV2 self, uint32 pwGroupHwId)
    {
    return mMethodsGet(self)->ApsGroupHwIsEnabled(self, pwGroupHwId);
    }

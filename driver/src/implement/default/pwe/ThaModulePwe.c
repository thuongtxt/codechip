/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PWE (internal)
 *
 * File        : ThaModulePwe.c
 *
 * Created Date: Sep 10, 2012
 *
 * Description : PWE internal module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../generic/sdh/AtSdhChannelInternal.h"
#include "../../../util/coder/AtCoderUtil.h"
#include "../bmt/ThaModuleBmtInternal.h"
#include "../pw/ThaModulePw.h"
#include "../sdh/ThaSdhVc.h"
#include "../sdh/ThaModuleSdh.h"
#include "../pw/ThaPwToh.h"
#include "../pdh/ThaModulePdh.h"
#include "ThaModulePweInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaPwCwRtpDisable 0
#define cThaPwCwOnly       1
#define cThaPwCwRtp        2
#define cThaPwRtpCw        3
#define cTu1xBitMask(tug2, tu) (cBit0 << ((tug2) * 4 + (tu)))
#define cTu1xBitShift(tug2, tu) ((tug2) * 4 + (tu))
#define cBitMaskEmpty      1

/* PW Adding state */
#define cStatePrepare        1
#define cStateStartAlignment 2

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaModulePwe)self)
#define mPwHwId(pw_) AtChannelHwIdGet((AtChannel)pw_)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaModulePweMethods m_methods;

/* Override */
static tAtModuleMethods m_AtModuleOverride;
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementations */
static const tAtModuleMethods *m_AtModuleMethods = NULL;
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
mDefineMaskShift(ThaModulePwe, PWPdhPlaAsmPldTypeCtrl)
mDefineMaskShift(ThaModulePwe, PWPdhPlaAsmPldSizeCtrl)
mDefineRegAdress(ThaModulePwe, PWPdhPlaPldSizeCtrl)
mDefineRegAdress(ThaModulePwe, PWPdhPlaEngStat)
mDefineRegAdress(ThaModulePwe, PDHPWPlaSignalingInverseLookupControl)

static uint32 PDHPWAddingProtocolToPreventBurst(ThaModulePwe self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return 0x2C0003;
    }

static uint32 PDHPWAddingStateMask(ThaModulePwe self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cThaAddPwStateMask;
    }

static uint32 PDHPWAddingPwMask(ThaModulePwe self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cThaAddPwIdMask;
    }

static ThaModulePw PwModule(ThaModulePwe self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return (ThaModulePw)AtDeviceModuleGet(device, cAtModulePw);
    }

static uint32 PwPweDefaultOffset(ThaModulePwe self, AtPw pw)
    {
    ThaModulePw pwModule = PwModule(self);
    return mPwHwId(pw) + ThaModulePwePartOffset(self, ThaModulePwPartOfPw(pwModule, pw));
    }

static uint32 PwPlaDefaultOffset(ThaModulePwe self, AtPw pw)
    {
    /* Default implement, PLA and PWE have same pw offset */
    return PwPweDefaultOffset(self, pw);
    }

static eAtRet PsnHdrPacketLengthUpdate(AtPw pw)
    {
	AtUnused(pw);
    /* TODO: implement me */
    return cAtOk;
    }

static eAtRet PwRtpEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    eAtRet ret       = cAtOk;
    uint32 address   = cThaRegPWEncPWHdrCtrl + mPwPweOffset(self, pw);
    uint32 regVal    = mChannelHwRead(pw, address, cThaModulePwe);
    eBool rtpEnabled = mMethodsGet(pw)->RtpIsEnabled(pw);

    if (enable)
        {
    	eAtPwPsnType psnType;
    	eBool ipPsn;
    	
    	/* If CW is disabled, do nothing */
		if (!AtPwCwIsEnabled(pw))
			return ret;

        psnType = AtPwPsnTypeGet(AtPwPsnGet(pw));
        ipPsn = (psnType == cAtPwPsnTypeIPv4) || (psnType == cAtPwPsnTypeIPv6);
        mFieldIns(&regVal, cThaPWEncPwHdrCtrlMask, cThaPWEncPwHdrCtrlShift, ipPsn ? cThaPwRtpCw : cThaPwCwRtp);
        }
    else
        mFieldIns(&regVal, cThaPWEncPwHdrCtrlMask, cThaPWEncPwHdrCtrlShift, cThaPwCwOnly);

    mChannelHwWrite(pw, address, regVal, cThaModulePwe);

    /* RTP is changed, need to update header */
    if (rtpEnabled != enable)
        ret |= PsnHdrPacketLengthUpdate(pw);

    return ret;
    }

static eAtRet PwCwEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    uint32 address, regVal;

    if (enable)
        return mMethodsGet(pw)->RtpEnable(pw, mMethodsGet(pw)->RtpIsEnabled(pw));

    /* Disable */
    address = cThaRegPWEncPWHdrCtrl + mPwPweOffset(self, pw);
    regVal  = mChannelHwRead(pw, address, cThaModulePwe);
    mFieldIns(&regVal, cThaPWEncPwHdrCtrlMask, cThaPWEncPwHdrCtrlShift, cThaPwCwRtpDisable);
    mChannelHwWrite(pw, address, regVal, cThaModulePwe);

    return cAtOk;
    }

static eAtRet PwCwAutoTxLBitEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    uint32 address = cThaRegPWEncPWHdrCtrl + mPwPweOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, address, cThaModulePwe);

    /* Disable forcing if enable */
    if (enable)
        mFieldIns(&regVal, cThaPWEncLbitValCtrlMask, cThaPWEncLbitValCtrlShift, 0);

    mFieldIns(&regVal, cThaPWEncLbitEngEnCtrlMask, cThaPWEncLbitEngEnCtrlShift, mBoolToBin(enable));
    mChannelHwWrite(pw, address, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool PwCwAutoTxLBitIsEnabled(ThaModulePwe self, AtPw pw)
    {
    uint32 address = cThaRegPWEncPWHdrCtrl + mPwPweOffset(self, pw);
    uint32 regVal = mChannelHwRead(pw, address, cThaModulePwe);
    return (regVal & cThaPWEncLbitEngEnCtrlMask) ? cAtTrue : cAtFalse;
    }

static eAtRet PwCwAutoTxRBitEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    uint32 address = cThaRegPWEncPWHdrCtrl + mPwPweOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, address, cThaModulePwe);

    /* Disable forcing when enable */
    if (enable)
        mFieldIns(&regVal, cThaPWEncRbitValCtrlMask, cThaPWEncRbitValCtrlShift, 0);

    mFieldIns(&regVal, cThaPWEncRbitEngEnCtrlMask, cThaPWEncRbitEngEnCtrlShift, mBoolToBin(enable));
    mChannelHwWrite(pw, address, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool PwCwAutoTxRBitIsEnabled(ThaModulePwe self, AtPw pw)
    {
    uint32 address = cThaRegPWEncPWHdrCtrl + mPwPweOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, address, cThaModulePwe);

    return (regVal & cThaPWEncRbitEngEnCtrlMask) ? cAtTrue : cAtFalse;
    }

static eAtRet PwCwAutoTxMBitEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
	AtUnused(self);
    if (AtPwTypeGet(pw) != cAtPwTypeCESoP)
        return cAtErrorInvlParm;

    /* Cannot disable with current FPGA */
    if (enable)
        return cAtOk;

    return cAtErrorModeNotSupport;
    }

static eBool PwCwAutoTxMBitIsEnabled(ThaModulePwe self, AtPw pw)
    {
	AtUnused(self);
    if (AtPwTypeGet(pw) == cAtPwTypeCESoP)
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet PwNxDs0Set(ThaModulePwe self, AtPw pw, AtPdhNxDS0 nxDs0)
    {
    ThaModulePdh modulePdh = (ThaModulePdh)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)nxDs0), cAtModulePdh);
    if (ThaModulePdhCasSupported(modulePdh))
        {
        uint32 regAddr = mMethodsGet(self)->PDHPWPlaSignalingInverseLookupControl(self) + mPwPlaOffset(self, pw);
        uint32 longReg[cThaLongRegMaxSize];
        uint32 de1FlatId = ThaPdhDe1FlatId((ThaPdhDe1)AtPdhNxDS0De1Get(nxDs0));

        mChannelHwLongRead(pw, regAddr, longReg, cThaLongRegMaxSize, cThaModulePwe);
        longReg[cThaPDHPWPlaAsmSigInvLkIdCtrlDwIndex] = de1FlatId;
        longReg[cThaPDHPWPlaAsmSigInvLkBitMapCtrlDwIndex] = AtPdhNxDS0BitmapGet(nxDs0);
        mChannelHwLongWrite(pw, regAddr, longReg, cThaLongRegMaxSize, cThaModulePwe);
        }
    return cAtOk;
    }

static eAtRet PwNumDs0TimeslotsSet(ThaModulePwe self, AtPw pw, uint16 numTimeslots)
    {
    uint32 regAddr, regVal;

    regAddr = mMethodsGet(self)->PWPdhPlaPldSizeCtrl(self) + mPwPlaOffset(self, pw);
    regVal = mChannelHwRead(pw, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cThaPWPdhPlaAsmCesNumDs0PldSizeCtrl, numTimeslots);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static uint8 PwNumDs0TimeslotsGet(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr, regVal;

    regAddr = mMethodsGet(self)->PWPdhPlaPldSizeCtrl(self) + mPwPlaOffset(self, pw);
    regVal = mChannelHwRead(pw, regAddr, cThaModulePwe);
    return (uint8)mRegField(regVal, cThaPWPdhPlaAsmCesNumDs0PldSizeCtrl);
    }

static eBool NumFramesInPacketIsInRange(uint16 numFramesInPacket)
    {
    uint32 maxNumFrames;
    maxNumFrames = cThaPWPdhPlaAsmCesNumFrmPldSizeCtrlMask >> cThaPWPdhPlaAsmCesNumFrmPldSizeCtrlShift;
    return (numFramesInPacket <= maxNumFrames) ? cAtTrue : cAtFalse;
    }

static eAtRet PwNumFramesInPacketSet(ThaModulePwe self, AtPw pw, uint16 numFramesInPacket)
    {
    uint32 regAddr, regVal;

    if (!NumFramesInPacketIsInRange(numFramesInPacket))
        return cAtErrorOutOfRangParm;

    regAddr = mMethodsGet(self)->PWPdhPlaPldSizeCtrl(self) + mPwPlaOffset(self, pw);
    regVal = mChannelHwRead(pw, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cThaPWPdhPlaAsmCesNumFrmPldSizeCtrl, numFramesInPacket);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static uint16 PwCesMaxNumFramesInPacket(ThaModulePwe self)
    {
    AtUnused(self);
    return 512;
    }

static eAtRet PwTypeSet(ThaModulePwe self, AtPw pw, eAtPwType pwType)
    {
    uint32 address = mMethodsGet(self)->PWPdhPlaPldSizeCtrl(self) + mPwPlaOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, address, cThaModulePwe);
    uint32 mask = mMethodsGet(self)->PWPdhPlaAsmPldTypeCtrlMask(self);
    uint8 shift = mMethodsGet(self)->PWPdhPlaAsmPldTypeCtrlShift(self);

    mFieldIns(&regVal, mask, shift, ThaModulePweHwPwType(self, pw, pwType));
    mChannelHwWrite(pw, address, regVal, cThaModulePwe);

    return cAtOk;
    }

static uint32 PwTxPacketsGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
    uint32 address = cThaRegPwEncTxPktCnt(clear) + mPwPweOffset(self, pw);
    return mChannelHwRead(pw, address, cThaModulePwe);
    }

static uint32 PwTxBytesGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
    uint32 address = cThaRegPwEncTxByteCnt(clear) + mPwPweOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, address, cThaModulePwe);

    return regVal;
    }

static uint32 PwTxLbitPacketsGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
    uint32 address = cThaRegPwEncLBitPktCnt(clear) + mPwPweOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, address, cThaModulePwe);

    return regVal;
    }

static uint32 PwTxRbitPacketsGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
    uint32 address = cThaRegPwEncRBitPktCnt(clear) + mPwPweOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, address, cThaModulePwe);

    return regVal;
    }

static uint32 PwTxMbitPacketsGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
    /* Same register address for M and N bit */
    uint32 address = cThaRegPwEncTxMBitCnt(clear) + mPwPweOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, address, cThaModulePwe);

    return regVal;
    }

static uint32 PwTxNbitPacketsGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
    /* Same register address for M and N bit */
    uint32 address = cThaRegPwEncTxMBitCnt(clear) + mPwPweOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, address, cThaModulePwe);

    return regVal;
    }

static uint32 PwTxPbitPacketsGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
    uint32 address = cThaRegPwEncTxPBitCnt(clear) + mPwPweOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, address, cThaModulePwe);

    return regVal;
    }

static eAtRet PwSupressEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    uint32 address = cThaRegPWEncPWHdrCtrl + mPwPweOffset(self, pw);
    uint32 value;

    value = mChannelHwRead(pw, address, cThaModulePwe);
    mFieldIns(&value,
              cThaPWEncSuprEnCtrlMask,
              cThaPWEncSuprEnCtrlShift,
              enable ? 1 : 0);
    mChannelHwWrite(pw, address, value, cThaModulePwe);

    return cAtOk;
    }

static eBool PwSupressIsEnabled(ThaModulePwe self, AtPw pw)
    {
    uint32 value;

    value = mChannelHwRead(pw, cThaRegPWEncPWHdrCtrl + mPwPweOffset(self, pw), cThaModulePwe);
    return (value & cThaPWEncSuprEnCtrlMask) ? cAtTrue : cAtFalse;
    }

static eAtRet PayloadSizeSet(ThaModulePwe self, AtPw pw, uint16 payloadSize)
    {
    uint32 address, regVal;
    uint32 mask  = mFieldMask(self, PWPdhPlaAsmPldSizeCtrl);
    uint32 shift = mFieldShift(self, PWPdhPlaAsmPldSizeCtrl);

    address = mMethodsGet(self)->PWPdhPlaPldSizeCtrl(self) + mPwPlaOffset(self, pw);
    regVal = mChannelHwRead(pw, address, cThaModulePwe);
    mFieldIns(&regVal, mask, shift, payloadSize);
    mChannelHwWrite(pw, address, regVal, cThaModulePwe);

	return cAtOk;
	}
	
static uint16 PayloadSizeGet(ThaModulePwe self, AtPw pw)
    {
    uint32 address, regVal;
    uint16 payloadSize;

    address = mMethodsGet(self)->PWPdhPlaPldSizeCtrl(self) + mPwPlaOffset(self, pw);
    regVal = mChannelHwRead(pw, address, cThaModulePwe);
    mFieldGet(regVal, mFieldMask(self, PWPdhPlaAsmPldSizeCtrl), mFieldShift(self, PWPdhPlaAsmPldSizeCtrl), uint16, &payloadSize);

    return payloadSize;
	}

static uint16 NumFramesInPacketGet(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr, regVal;

    regAddr = mMethodsGet(self)->PWPdhPlaPldSizeCtrl(self) + mPwPlaOffset(self, pw);
    regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    return (uint16)mRegField(regVal, cThaPWPdhPlaAsmCesNumFrmPldSizeCtrl);
    }

static ThaModuleBmt BmtModule(ThaModulePwe self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return (ThaModuleBmt)AtDeviceModuleGet(device, cThaModuleBmt);
    }

static eAtRet PwEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    ThaModuleBmt bmtModule = BmtModule(self);

    if (bmtModule)
        return mMethodsGet(bmtModule)->Enable(bmtModule, pw, enable);
    return cAtErrorNullPointer;
    }

static eBool PwIsEnabled(ThaModulePwe self, AtPw pw)
    {
    ThaModuleBmt bmtModule = BmtModule(self);

    if (bmtModule)
        return mMethodsGet(bmtModule)->IsEnabled(bmtModule, pw);

    return cAtFalse;
    }

static eAtRet NumVlansSet(ThaModulePwe self, AtPw pw, uint8 numVlans)
    {
	AtUnused(numVlans);
	AtUnused(pw);
	AtUnused(self);
    return cAtOk;
    }

static eAtRet HeaderWrite(ThaModulePwe self, AtPw pw, uint8 *buffer, uint8 headerLenInByte, AtPwPsn psn)
    {
    ThaModuleBmt moduleBmt = BmtModule(self);
    AtUnused(psn);

    if (moduleBmt)
        return mMethodsGet(moduleBmt)->PwHeaderArraySet(moduleBmt, pw, buffer, headerLenInByte);

    return cAtErrorNullPointer;
    }

static eAtRet RawHeaderWrite(ThaModulePwe self, AtPw pw, uint8 *buffer, uint8 headerLenInByte)
    {
    return mMethodsGet(self)->HeaderWrite(self, pw, buffer, headerLenInByte, NULL);
    }

static eAtRet HeaderRead(ThaModulePwe self, AtPw pw, uint8 *buffer, uint8 bufferSize)
    {
    ThaModuleBmt moduleBmt = BmtModule(self);

    if (moduleBmt)
        return mMethodsGet(moduleBmt)->PwHeaderArrayGet(moduleBmt, pw, buffer, bufferSize);

    return cAtErrorNullPointer;
    }

static eAtRet PartDefaultSet(ThaModulePwe self, uint8 partId)
    {
    uint32 regAddr;
    uint32 pw_i;
    uint32 dataBuffer[5];
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    AtIpCore ipcore = AtDeviceIpCoreGet(device, AtModuleDefaultCoreGet((AtModule)self));
    AtModulePw pwModules = (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);
    uint32 partOffset = ThaModulePwePartOffset((ThaModulePwe)self, partId);

    /* Set first 9 bits for RTP (V=2, P=0, X=0, CC=0, M=0) */
    mModuleHwWrite(self, cThaRegPWEncRtp9BitsHdrCtrl + partOffset, cThaPwRtpFirst9Bits);

    /* Write random value to PLA engine status */
    for (pw_i = 0; pw_i < ThaModulePwNumPwsPerPart((ThaModulePw)pwModules); pw_i++)
        {
        regAddr = mMethodsGet(self)->PWPdhPlaEngStat(self) + pw_i + partOffset;
        dataBuffer[1] = pw_i << 12;
        mModuleHwLongWrite(self, regAddr, dataBuffer, 5, ipcore);
        }

    return cAtOk;
    }

static AtModuleSdh ModuleSdh(ThaModulePwe self)
    {
    return (AtModuleSdh)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModuleSdh);
    }

static uint32 LinePartOffset(ThaModulePwe self, AtSdhLine line)
    {
    return ThaModulePwePartOffset(self, ThaModuleSdhPartOfChannel((AtSdhChannel)line));
    }

static eAtRet TohPhysicalLineEnable(ThaModulePwe self, AtSdhLine line, eBool enable)
    {
    uint32 address = cThaRegThalassaPdhPWPldAssembleTohPhyLineConvCtrl + LinePartOffset(self, line);
    uint32 regVal = mChannelHwRead(line, address, cThaModulePwe);
    uint8 lineHwId = (uint8)AtChannelHwIdGet((AtChannel)line);

    mFieldIns(&regVal, cThaPDHPWPlaAsmTOHLineEnCtrlMask(lineHwId), cThaPDHPWPlaAsmTOHLineEnCtrlShift(lineHwId), mBoolToBin(enable));
    mFieldIns(&regVal, cThaPDHPWPlaAsmTOHLogicLineCtrlMask(lineHwId), cThaPDHPWPlaAsmTOHLogicLineCtrlShift(lineHwId), lineHwId);
    mChannelHwWrite(line, address, regVal, cThaModulePwe);

    return cAtOk;
    }

static eAtRet TohPhysicalLineDefaultSet(ThaModulePwe self)
    {
    eAtRet ret = cAtOk;
    AtModuleSdh moduleSdh = ModuleSdh(self);
    uint8 numLine = AtModuleSdhMaxLinesGet(moduleSdh);
    uint8 line_i;
    AtSdhLine line;

    for (line_i = 0; line_i < numLine; line_i++)
        {
        line = AtModuleSdhLineGet(moduleSdh, line_i);
        ret |= TohPhysicalLineEnable(self, line, cAtTrue);
        }

    return ret;
    }

static eAtRet SubPortVlanBypass(ThaModulePwe self, eBool bypass)
    {
    AtUnused(self);
    return bypass ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool SubPortVlanIsBypassed(ThaModulePwe self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint8 NumParts(ThaModulePwe self)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    return ThaDeviceNumPartsOfModule(device, cThaModulePwe);
    }

static eAtRet DefaultSet(ThaModulePwe self)
    {
    uint8 part_i;
    eAtRet ret = cAtOk;

    for (part_i = 0; part_i < NumParts(self); part_i++)
        ret |= PartDefaultSet(self, part_i);

    ret |= TohPhysicalLineDefaultSet(self);
    return ret;
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return mMethodsGet(mThis(self))->DefaultSet(mThis(self));
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static void Delete(AtObject self)
    {
    /* Delete private data */

    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeNone(maxPwAddingWaitTimeInMs);
    }

static eAtRet PwPsnTypeSet(ThaModulePwe self, AtPw pw, AtPwPsn psn)
    {
	AtUnused(psn);
	AtUnused(pw);
	AtUnused(self);
    return cAtOk;
    }

static void PwDebug(ThaModulePwe self, AtPw pw)
    {
	AtUnused(pw);
	AtUnused(self);
    }

static eBool AddingProtocolIsEnabled(ThaModulePwe self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static eAtRet PwAddingPrepare(ThaModulePwe self, AtPw pw)
    {
    uint32 regVal, regAddr;

    if (!AddingProtocolIsEnabled(self))
        return cAtOk;

    regVal  = 0;
    regAddr = mMethodsGet(self)->PDHPWAddingProtocolToPreventBurst(self, pw) + ThaPwPartOffset(pw, cThaModulePwe);
    mFieldIns(&regVal, mMethodsGet(self)->PDHPWAddingPwMask(self, pw), cThaAddPwIdShift, mPwHwId(pw));
    mFieldIns(&regVal, mMethodsGet(self)->PDHPWAddingStateMask(self, pw), cThaAddPwStateShift, cStatePrepare);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool IsSimulated(ThaModulePwe self)
    {
    return AtDeviceIsSimulated(AtModuleDeviceGet((AtModule)self));
    }

static eAtRet PwAddingWait(ThaModulePwe self, AtPw pw)
    {
    uint32 timeoutMs, elapseMs;
    tAtOsalCurTime startTime, curTime;
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 regAddr = mMethodsGet(self)->PDHPWAddingProtocolToPreventBurst(self, pw) + ThaPwPartOffset(pw, cThaModulePwe);

    if (IsSimulated(self) || AtDeviceWarmRestoreIsStarted(AtModuleDeviceGet((AtModule)self)))
        return cAtOk;

    timeoutMs = 256;
    elapseMs  = 0;
    mMethodsGet(osal)->CurTimeGet(osal, &startTime);
    while (elapseMs < timeoutMs)
        {
        if ((mChannelHwRead(pw, regAddr, cThaModulePwe) & mMethodsGet(self)->PDHPWAddingStateMask(self, pw)) == 0)
            {
            if (elapseMs > mThis(self)->maxPwAddingWaitTimeInMs)
                mThis(self)->maxPwAddingWaitTimeInMs = elapseMs;
            return cAtOk;
            }

        /* Wait */
        mMethodsGet(osal)->CurTimeGet(osal, &curTime);
        elapseMs = mTimeIntervalInMsGet(startTime, curTime);
        }

    AtChannelLog((AtChannel)pw, cAtLogLevelWarning, AtSourceLocation, "PW adding timeout\r\n");
    return cAtErrorDevBusy;
    }

static eAtRet PwAddingStart(ThaModulePwe self, AtPw pw)
    {
    uint32 regVal, regAddr;

    if (AtDeviceWarmRestoreIsStarted(AtModuleDeviceGet((AtModule)self)))
        return cAtOk;

    if (!AddingProtocolIsEnabled(self))
        return cAtOk;

    regVal  = 0;
    regAddr = mMethodsGet(self)->PDHPWAddingProtocolToPreventBurst(self, pw) + ThaPwPartOffset(pw, cThaModulePwe);
    mFieldIns(&regVal, mMethodsGet(self)->PDHPWAddingPwMask(self, pw), cThaAddPwIdShift, mPwHwId(pw));
    mFieldIns(&regVal, mMethodsGet(self)->PDHPWAddingStateMask(self, pw), cThaAddPwStateShift, cStateStartAlignment);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return PwAddingWait(self, pw);
    }

static eAtRet PwRemovingStart(ThaModulePwe self, AtPw pw)
    {
	AtUnused(pw);
	AtUnused(self);
    /* Concrete may do */
    return cAtOk;
    }

static eAtRet PwRemovingFinish(ThaModulePwe self, AtPw pw)
    {
	AtUnused(pw);
	AtUnused(self);
    /* Concrete may do */
    return cAtOk;
    }

static eAtRet PwRamStatusClear(ThaModulePwe self, AtPw pw)
    {
	AtUnused(pw);
	AtUnused(self);
    /* Concrete may do */
    return cAtOk;
    }

static eAtRet PwTxLBitForce(ThaModulePwe self, AtPw pw, eBool force)
    {
    uint32 regAddr = cThaRegPWEncPWHdrCtrl + mPwPweOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);

    mRegFieldSet(regVal, cThaPWEncLbitValCtrl, mBoolToBin(force));
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool PwTxLBitIsForced(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr = cThaRegPWEncPWHdrCtrl + mPwPweOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);

    return (regVal & cThaPWEncLbitValCtrlMask) ? cAtTrue : cAtFalse;
    }

static eAtRet PwTxRBitForce(ThaModulePwe self, AtPw pw, eBool force)
    {
    uint32 regAddr = cThaRegPWEncPWHdrCtrl + mPwPweOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);

    mRegFieldSet(regVal, cThaPWEncRbitValCtrl, mBoolToBin(force));
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool PwTxRBitIsForced(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr = cThaRegPWEncPWHdrCtrl + mPwPweOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);

    return (regVal & cThaPWEncRbitValCtrlMask) ? cAtTrue : cAtFalse;
    }

static eAtRet PwTxMBitForce(ThaModulePwe self, AtPw pw, eBool force)
    {
    uint32 regAddr = cThaRegPWEncPWHdrCtrl + mPwPweOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);

    mRegFieldSet(regVal, cThaPWEncMbitValCtrl, (force) ? 2 : 0);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool PwTxMBitIsForced(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr = cThaRegPWEncPWHdrCtrl + mPwPweOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    return (mRegField(regVal, cThaPWEncMbitValCtrl) == 2) ? cAtTrue : cAtFalse;
    }

static uint32 TxForcibleAlarms(ThaModulePwe self, AtPw pw)
    {
    uint32 forceAbleAlarms = 0;
    AtPwCESoP pwCesop;
	AtUnused(self);

    if (AtPwCwAutoRxLBitIsEnabled(pw))
        forceAbleAlarms |= cAtPwAlarmTypeLBit;

    if (AtPwCwAutoRBitIsEnabled(pw))
        forceAbleAlarms |= cAtPwAlarmTypeRBit;

    if (AtPwTypeGet(pw) != cAtPwTypeCESoP)
        return forceAbleAlarms;

    pwCesop = (AtPwCESoP)ThaPwAdapterPwGet((ThaPwAdapter)pw);
    if (pwCesop)
        {
        if (AtPwCESoPCwAutoTxMBitIsEnabled(pwCesop))
            forceAbleAlarms |= cAtPwAlarmTypeMBit;
        }

    return forceAbleAlarms;
    }

static eBool CanForceTxAlarms(ThaModulePwe self, AtPw pw, eAtPwAlarmType alarmType)
    {
    return ((uint32)alarmType & TxForcibleAlarms(self,pw)) ? cAtTrue : cAtFalse;
    }

static eAtRet PwTxAlarmForceHelper(ThaModulePwe self, AtPw pw, eAtPwAlarmType alarmType, eBool force)
    {
    eAtRet ret = cAtOk;

    if (alarmType & cAtPwAlarmTypeLBit)
        ret |= mMethodsGet(self)->PwTxLBitForce(self, pw, force);

    if (alarmType & cAtPwAlarmTypeRBit)
        ret |= mMethodsGet(self)->PwTxRBitForce(self, pw, force);

    if (alarmType & cAtPwAlarmTypeMBit)
        ret |= mMethodsGet(self)->PwTxMBitForce(self, pw, force);

    return ret;
    }

static eAtRet PwTxAlarmForce(ThaModulePwe self, AtPw pw, eAtPwAlarmType alarmType)
    {
    if (!CanForceTxAlarms(self, pw, alarmType))
        return cAtErrorModeNotSupport;

    return PwTxAlarmForceHelper(self, pw, alarmType, cAtTrue);
    }

static eAtRet PwTxAlarmUnForce(ThaModulePwe self, AtPw pw, eAtPwAlarmType alarmType)
    {
    return PwTxAlarmForceHelper(self, pw, alarmType, cAtFalse);
    }

static uint32 PwTxForcedAlarmGet(ThaModulePwe self, AtPw pw)
    {
    uint32 forcedAlarms = 0;

    if (mMethodsGet(self)->PwTxLBitIsForced(self, pw))
        forcedAlarms |= cAtPwAlarmTypeLBit;

    if (mMethodsGet(self)->PwTxRBitIsForced(self, pw))
        forcedAlarms |= cAtPwAlarmTypeRBit;

    if (mMethodsGet(self)->PwTxMBitIsForced(self, pw))
        forcedAlarms |= cAtPwAlarmTypeMBit;

    return forcedAlarms;
    }

static uint32 SdhChannelOffset(ThaModulePwe self, AtSdhChannel channel)
    {
    uint8 hwSlice, hwSts;
    ThaSdhChannel2HwMasterStsId(channel, cThaModulePwe, &hwSlice, &hwSts);
    return hwSts + ThaModulePwePartOffset(self, ThaModuleSdhPartOfChannel(channel));
    }

static AtSdhChannel Vc3Tug3Get(AtSdhChannel vc3)
    {
    AtSdhChannel tu3 = AtSdhChannelParentChannelGet(vc3);
    return AtSdhChannelParentChannelGet(tu3);
    }

/*
 * If sub-channel belongs to this TUG3, set equip/un-equip for it.
 * If it does not belong to this TUG3, return is-equipped status
 */
static eBool Tug3CepFractionalVc3Equip(ThaModulePwe self, AtSdhChannel tug3, AtChannel subChannel, eBool equip)
    {
    uint32 regAddr;
    uint32 longReg[cThaLongRegMaxSize];
    uint32 offset = SdhChannelOffset(self, tug3);
    AtOsal osal;
    eBool isEquipped;

    osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, longReg, 0, sizeof(longReg));

    regAddr = cThaRegThalassaPdhPWPldAssembleCEPFractionalEBmCtrl + offset;
    mChannelHwLongRead(tug3, regAddr, longReg, cThaLongRegMaxSize, cThaModulePwe);

    isEquipped = mRegField(longReg[1], cThaPdhPWPlaAsmCEPEBmTug3EnCtrl) ? cAtTrue : cAtFalse;

    /* If subChannel does not belong to this TUG3, do nothing and return */
    if (Vc3Tug3Get((AtSdhChannel)subChannel) != tug3)
        return isEquipped;

    mRegFieldSet(longReg[1], cThaPdhPWPlaAsmCEPEBmTug3EnCtrl, (equip) ? 1 : 0);

    /* Let another fields are 0
     * In fractional VC-4 CEP mode all of the 28 EBM bits of 7 VTG in a VC3 are set to zero,
     * it means the TUG-3 within VC-4 carries VC-3. */
    mChannelHwLongWrite(tug3, regAddr, longReg, cThaLongRegMaxSize, cThaModulePwe);

    return equip;
    }

static AtSdhChannel Vc1xTug3Vc3Get(AtSdhChannel vc1x)
    {
    AtSdhChannel tu1x = AtSdhChannelParentChannelGet(vc1x);
    AtSdhChannel tug2 = AtSdhChannelParentChannelGet(tu1x);
    return AtSdhChannelParentChannelGet(tug2);
    }

static uint32 Vc1xEquipSetToReg(uint32 currentMask, uint32* longReg, uint8 tug2, uint8 tu1x, eBool equip)
    {
    if (tug2 <= 4)
        {
        mFieldIns(&longReg[0],
                  cThaPdhPWPlaAsmCEPEBmVtgDatCtrlMask(tug2, tu1x),
                  cThaPdhPWPlaAsmCEPEBmVtgDatCtrlShift(tug2, tu1x),
                  mBoolToBin(equip));
        }

    else if (tug2 == 6)
        {
        mFieldIns(&longReg[1],
                  cThaPdhPWPlaAsmCEPEBmVtg7DatCtrlMask(tu1x),
                  cThaPdhPWPlaAsmCEPEBmVtg7DatCtrlShift(tu1x),
                  mBoolToBin(equip));
        }

    else
        {
        if (tu1x <= 1)
            {
            mFieldIns(&longReg[0],
                      cThaPdhPWPlaAsmCEPEBmVtg6DatCtrlHwTailMask(tu1x),
                      cThaPdhPWPlaAsmCEPEBmVtg6DatCtrlHwTailShift(tu1x),
                      mBoolToBin(equip));
            }
        else
            {
            mFieldIns(&longReg[1],
                      cThaPdhPWPlaAsmCEPEBmVtg6DatCtrlHwHeadMask(tu1x),
                      cThaPdhPWPlaAsmCEPEBmVtg6DatCtrlHwHeadShift(tu1x),
                      mBoolToBin(equip));
            }
        }

    mFieldIns(&currentMask, cTu1xBitMask(tug2, tu1x), cTu1xBitShift(tug2, tu1x), mBoolToBin(equip));
    return currentMask;
    }

static uint32 Vc1xEquipGetFromReg(uint32 currentMask, uint32* longReg, uint8 tug2, uint8 tu1x)
    {
    uint8 bitVal;
    if (tug2 <= 4)
        {
        mFieldGet(longReg[0],
                  cThaPdhPWPlaAsmCEPEBmVtgDatCtrlMask(tug2, tu1x), cThaPdhPWPlaAsmCEPEBmVtgDatCtrlShift(tug2, tu1x),
                  uint8, &bitVal);
        }

    else if (tug2 == 6)
        {
        mFieldGet(longReg[1],
                  cThaPdhPWPlaAsmCEPEBmVtg7DatCtrlMask(tu1x), cThaPdhPWPlaAsmCEPEBmVtg7DatCtrlShift(tu1x),
                  uint8, &bitVal);
        }

    else
        {
        if (tu1x <= 1)
            {
            mFieldGet(longReg[0],
                      cThaPdhPWPlaAsmCEPEBmVtg6DatCtrlHwTailMask(tu1x), cThaPdhPWPlaAsmCEPEBmVtg6DatCtrlHwTailShift(tu1x),
                      uint8, &bitVal);
            }
        else
            {
            mFieldGet(longReg[1],
                      cThaPdhPWPlaAsmCEPEBmVtg6DatCtrlHwHeadMask(tu1x), cThaPdhPWPlaAsmCEPEBmVtg6DatCtrlHwHeadShift(tu1x),
                      uint8, &bitVal);
            }
        }

    mFieldIns(&currentMask, cTu1xBitMask(tug2, tu1x), cTu1xBitShift(tug2, tu1x), bitVal);
    return currentMask;
    }

/* Return cAtTrue when Tug3/VC3 is NOT empty */
static eBool Tug3Vc3Vc1xMaskSet(ThaModulePwe self, AtSdhChannel tug3, AtSdhChannel vc1x, eBool equip)
    {
    uint32 regAddr, vc1xMask = 0;
    uint32 longReg[cThaLongRegMaxSize];
    uint32 offset = SdhChannelOffset(self, tug3);
    uint8 tugId, tuId, tug2, tu1x;
    eBool hasVc = cAtFalse;

    tug2 = AtSdhChannelTug2Get(vc1x);
    tu1x = AtSdhChannelTu1xGet(vc1x);
    regAddr = cThaRegThalassaPdhPWPldAssembleCEPFractionalEBmCtrl + offset;
    mChannelHwLongRead(tug3, regAddr, longReg, cThaLongRegMaxSize, cThaModulePwe);

    for (tugId = 0; tugId < 7; tugId++)
        {
        for (tuId = 0; tuId < 4; tuId++)
            {
            if ((tug2 == tugId) && (tu1x == tuId)) /* This vc1x */
                vc1xMask = Vc1xEquipSetToReg(vc1xMask, longReg, tug2, tu1x, equip);
            else
                vc1xMask = Vc1xEquipGetFromReg(vc1xMask, longReg, tugId, tuId);
            }
        }

    mChannelHwLongWrite(tug3, regAddr, longReg, cThaLongRegMaxSize, cThaModulePwe);

    hasVc = hasVc || (vc1xMask != 0);
    return hasVc;
    }

/* Return cAtTrue when Tug3/VC3 is NOT empty */
static eBool Tug3Vc3IsNotEmpty(ThaModulePwe self, AtSdhChannel tug3)
    {
    uint32 regAddr, vc1xMask = 0;
    uint32 longReg[cThaLongRegMaxSize];
    uint32 offset = SdhChannelOffset(self, tug3);
    uint8 tugId, tuId;
    eBool hasVc = cAtFalse;

    regAddr = cThaRegThalassaPdhPWPldAssembleCEPFractionalEBmCtrl + offset;
    mChannelHwLongRead(tug3, regAddr, longReg, cThaLongRegMaxSize, cThaModulePwe);

    for (tugId = 0; tugId < 7; tugId++)
        {
        for (tuId = 0; tuId < 4; tuId++)
            {
            vc1xMask = Vc1xEquipGetFromReg(vc1xMask, longReg, tugId, tuId);
            }
        }

    hasVc = hasVc || (vc1xMask != 0);
    return hasVc;
    }

/* Return cAtTrue when Tug3/VC3 is NOT empty */
static eBool Tug3Vc3CepFractionalVc1xEquip(ThaModulePwe self, AtSdhChannel tug3vc3, AtChannel vc1x, eBool equip)
    {
    /* If subChannel does not belong to this TUG3VC3, do nothing */
    if (Vc1xTug3Vc3Get((AtSdhChannel)vc1x) != tug3vc3)
        return Tug3Vc3IsNotEmpty(self, tug3vc3);

    return Tug3Vc3Vc1xMaskSet(self, tug3vc3, (AtSdhChannel)vc1x, equip);
    }

/* Return cAtTrue when Tug3 is NOT empty */
static eBool Tug3PwCepFractionalSubChannelEquip(ThaModulePwe self, AtSdhChannel tug3, AtChannel subChannel, eBool equip)
    {
    if (AtSdhChannelMapTypeGet(tug3) == cAtSdhTugMapTypeTug3MapVc3)
        return Tug3CepFractionalVc3Equip(self, tug3, subChannel, equip);

    return Tug3Vc3CepFractionalVc1xEquip(self, tug3, subChannel, equip);
    }

static uint32 VcPartOffset(ThaModulePwe self, AtSdhChannel vc)
    {
    return ThaModulePwePartOffset(self, ThaModuleSdhPartOfChannel(vc));
    }

static eAtRet PwCepEbmEmptySet(ThaModulePwe self, AtSdhChannel sdhVc, eBool isEmpty)
    {
    uint32 offset = VcPartOffset(self, sdhVc);
    uint32 regAddr;
    uint8 sts_i;
    uint32 longReg[cThaLongRegMaxSize];

    for (sts_i = 0; sts_i < AtSdhChannelNumSts(sdhVc); sts_i++)
        {
        uint8 hwSlice, hwSts;
        uint8 stsId = (uint8)(AtSdhChannelSts1Get(sdhVc) + sts_i);

        ThaSdhChannelHwStsGet(sdhVc, cThaModulePwe, stsId, &hwSlice, &hwSts);
        regAddr = cThaRegThalassaPdhPWPldAssembleCEPFractionalEBmCtrl + hwSts + offset;
        mChannelHwLongRead(sdhVc, regAddr, longReg, cThaLongRegMaxSize, cThaModulePwe);
        mRegFieldSet(longReg[1], cThaPdhPWPlaAsmCEPEBmEmptyCtrl, (isEmpty) ? 1 : 0);
        mChannelHwLongWrite(sdhVc, regAddr, longReg, cThaLongRegMaxSize, cThaModulePwe);
        }

    return cAtOk;
    }

static eBool PwCepIsEmpty(ThaModulePwe self, AtPw pw)
    {
    AtSdhChannel sdhVc = (AtSdhChannel)AtPwBoundCircuitGet(pw);
    uint32 offset = VcPartOffset(self, sdhVc);
    uint32 regAddr;
    uint32 longReg[cThaLongRegMaxSize];

    uint8 hwSlice, hwSts;
    uint8 stsId = AtSdhChannelSts1Get(sdhVc);

    ThaSdhChannelHwStsGet(sdhVc, cThaModulePwe, stsId, &hwSlice, &hwSts);
    regAddr = cThaRegThalassaPdhPWPldAssembleCEPFractionalEBmCtrl + hwSts + offset;
    mChannelHwLongRead(sdhVc, regAddr, longReg, cThaLongRegMaxSize, cThaModulePwe);

    return (mRegField(longReg[1], cThaPdhPWPlaAsmCEPEBmEmptyCtrl) == 1) ? cAtTrue : cAtFalse;
    }

static void Tug3CepFractionalEquippedVc3Get(ThaModulePwe self, AtSdhChannel tug3, AtList equippedChannels)
    {
    uint32 regAddr;
    uint32 longReg[cThaLongRegMaxSize];
    uint32 offset = SdhChannelOffset(self, tug3);
    AtOsal osal;
    eBool isEquipped;

    osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, longReg, 0, sizeof(longReg));

    regAddr = cThaRegThalassaPdhPWPldAssembleCEPFractionalEBmCtrl + offset;
    mChannelHwLongRead(tug3, regAddr, longReg, cThaLongRegMaxSize, cThaModulePwe);

    isEquipped = mRegField(longReg[1], cThaPdhPWPlaAsmCEPEBmTug3EnCtrl) ? cAtTrue : cAtFalse;

    if (!isEquipped)
        return;

    AtListObjectAdd(equippedChannels, (AtObject)AtSdhChannelSubChannelGet(tug3, 0));
    }

static eBool Vc1xIsEquipped(uint32* longReg, uint8 tug2, uint8 tu1x)
    {
    if (Vc1xEquipGetFromReg(0, longReg, tug2, tu1x))
        return cAtTrue;
    return cAtFalse;
    }

static void Tug3Vc3CepFractionalEquippedVc1xGet(ThaModulePwe self, AtSdhChannel tug3vc3, AtList equippedChannels)
    {
    uint32 regAddr;
    uint32 longReg[cThaLongRegMaxSize];
    uint32 offset = SdhChannelOffset(self, tug3vc3);
    uint8 tugId, tuId;

    regAddr = cThaRegThalassaPdhPWPldAssembleCEPFractionalEBmCtrl + offset;
    mChannelHwLongRead(tug3vc3, regAddr, longReg, cThaLongRegMaxSize, cThaModulePwe);

    for (tugId = 0; tugId < 7; tugId++)
        {
        AtSdhChannel tug2 = AtSdhChannelSubChannelGet(tug3vc3, tugId);
        for (tuId = 0; tuId < 4; tuId++)
            {
            AtSdhChannel tu1x = AtSdhChannelSubChannelGet(tug2, tuId);
            if (Vc1xIsEquipped(longReg, tugId, tuId))
                AtListObjectAdd(equippedChannels, (AtObject)AtSdhChannelSubChannelGet(tu1x, 0));
            }
        }
    }

static void Tug3PwCepFractionalEquippedChannelGet(ThaModulePwe self, AtSdhChannel tug3, AtList equippedChannels)
    {
    if (AtSdhChannelMapTypeGet(tug3) == cAtSdhTugMapTypeTug3MapVc3)
        Tug3CepFractionalEquippedVc3Get(self, tug3, equippedChannels);

    else
        Tug3Vc3CepFractionalEquippedVc1xGet(self, tug3, equippedChannels);
    }

static void Vc4PwCepFractionalEquippedChannelsGet(ThaModulePwe self, AtSdhChannel sdhVc, AtList equippedChannels)
    {
    uint8 tug3Id;

    for (tug3Id = 0; tug3Id < AtSdhChannelNumberOfSubChannelsGet(sdhVc); tug3Id++)
        {
        AtSdhChannel tug3 = AtSdhChannelSubChannelGet(sdhVc, tug3Id);
        Tug3PwCepFractionalEquippedChannelGet(self, tug3, equippedChannels);
        }
    }

static void PwCepFractionalEquippedChannelsGet(ThaModulePwe self, AtPw pw, AtList equippedChannels)
    {
    AtSdhChannel sdhVc;
    eAtSdhChannelType vcType;

    if (PwCepIsEmpty(self, pw))
        return;

    sdhVc = (AtSdhChannel)AtPwBoundCircuitGet(pw);
    vcType = AtSdhChannelTypeGet(sdhVc);
    if (vcType == cAtSdhChannelTypeVc4)
        Vc4PwCepFractionalEquippedChannelsGet(self, sdhVc, equippedChannels);

    else if (vcType == cAtSdhChannelTypeVc3)
        Tug3Vc3CepFractionalEquippedVc1xGet(self, sdhVc, equippedChannels);
    }

/* Return cAtTrue when VC4 is NOT empty */
static eBool Vc4PwCepFractionalSubChannelEquip(ThaModulePwe self, AtSdhChannel sdhVc, AtChannel subChannel, eBool equip)
    {
    uint8 tug3Id;
    eBool vc4IsNotEmpty = cAtFalse;

    for (tug3Id = 0; tug3Id < AtSdhChannelNumberOfSubChannelsGet(sdhVc); tug3Id++)
        {
        AtSdhChannel tug3 = AtSdhChannelSubChannelGet(sdhVc, tug3Id);
        eBool tug3HasEquippedVc = Tug3PwCepFractionalSubChannelEquip(self, tug3, subChannel, equip);

        vc4IsNotEmpty = vc4IsNotEmpty || tug3HasEquippedVc;
        }

    return vc4IsNotEmpty;
    }

static eAtRet CepFractionalJ1ReturnControl(ThaModulePwe self, AtSdhChannel sdhVc, AtPw pw)
    {
    uint32 address;
    uint32 J1ReturnValue = ThaPwCepFractionalJ1ReturnCalculate(pw);
    uint8 sts_i, hwSlice, hwSts;
    uint32 partOffset = ThaModulePwePartOffset(self, ThaModuleSdhPartOfChannel(sdhVc));
    uint8 startSts = AtSdhChannelSts1Get(sdhVc);

    for (sts_i = 0; sts_i < AtSdhChannelNumSts(sdhVc); sts_i++)
        {
        ThaSdhChannelHwStsGet(sdhVc, cThaModulePwe, (uint8)(sts_i + startSts), &hwSlice, &hwSts);
        address = cThaPdhPWPlaCepFractionalJ1ReturnControl + hwSts + partOffset;
        mChannelHwWrite(sdhVc, address, J1ReturnValue, cThaModulePwe);
        }

    return cAtOk;
    }

static eAtRet NewEBMActivate(ThaModulePwe self, AtSdhChannel sdhVc)
    {
    uint32 address = cThaPdhPwPlaCepFractionalNewEbmEnable + SdhChannelOffset(self, sdhVc);
    static const uint8 cNewEbmActive = 3;

    mChannelHwWrite(sdhVc, address, cNewEbmActive, cThaModulePwe);
    return cAtOk;
    }

static eAtRet PwCepEquip(ThaModulePwe self, AtPw pw, AtChannel subChannel, eBool equip)
    {
    eAtRet ret;
    AtSdhChannel sdhVc = (AtSdhChannel)AtPwBoundCircuitGet(pw);
    eAtSdhChannelType vcType = AtSdhChannelTypeGet(sdhVc);
    eBool hasVc;
    if (vcType == cAtSdhChannelTypeVc4)
        hasVc = Vc4PwCepFractionalSubChannelEquip(self, sdhVc, subChannel, equip);

    else if (vcType == cAtSdhChannelTypeVc3)
        hasVc = Tug3Vc3CepFractionalVc1xEquip(self, sdhVc, subChannel, equip);

    else
        return cAtErrorModeNotSupport;

    ret  = PwCepEbmEmptySet(self, sdhVc, hasVc ? cAtFalse : cAtTrue);
    ret |= CepFractionalJ1ReturnControl(self, sdhVc, (AtPw)pw);
    ret |= NewEBMActivate(self, sdhVc);

    return ret;
    }

static uint8 IsTug3MapVc3(AtSdhChannel tug3)
    {
    if (AtSdhChannelMapTypeGet(tug3) == cAtSdhTugMapTypeTug3MapVc3)
        return 1;

    return 0;
    }

static void EbmControlVtgTypeRegFieldSet(uint32* longReg, uint8 tug2, uint8 vtgType)
    {
    if (tug2 <= 4)
        {
        mFieldIns(&longReg[0],
                  cThaPdhPWPlaAsmCEPEBmVtgTypeCtrlMask(tug2),
                  cThaPdhPWPlaAsmCEPEBmVtgTypeCtrlShift(tug2),
                  vtgType);
        }

    else if (tug2 == 6)
        {
        mFieldIns(&longReg[1],
                  cThaPdhPWPlaAsmCEPEBmVtg7TypeCtrlMask,
                  cThaPdhPWPlaAsmCEPEBmVtg7TypeCtrlShift,
                  vtgType);
        }

    else
        {
        mFieldIns(&longReg[1],
                  cThaPdhPWPlaAsmCEPEBmVtg6TypeCtrlMask,
                  cThaPdhPWPlaAsmCEPEBmVtg6TypeCtrlShift,
                  vtgType);
        }
    }

static void Vc3Tug3EbmControlVtgTypeRegFieldSet(AtSdhChannel tug3, uint32* longReg)
    {
    uint8 tug2_i;
    uint8 vtgType = AtSdhChannelMapTypeGet(AtSdhChannelSubChannelGet(tug3, 0));
    uint8 hwVtgType = (vtgType == cAtSdhTugMapTypeTug2Map3xTu12s) ? 1 : 0;

    for (tug2_i = 0; tug2_i < AtSdhChannelNumberOfSubChannelsGet(tug3); tug2_i++)
        {
        EbmControlVtgTypeRegFieldSet(longReg, tug2_i, hwVtgType);
        }
    }

static eAtRet Tug3PwCepFractionalSet(ThaModulePwe self, AtSdhChannel tug3)
    {
    uint32 regAddr;
    uint32 longReg[cThaLongRegMaxSize];
    uint32 offset = SdhChannelOffset(self, tug3);
    AtOsal osal = AtSharedDriverOsalGet();
    eBool isTug3MapVc3 = IsTug3MapVc3(tug3);

    regAddr = cThaRegThalassaPdhPWPldAssembleCEPFractionalEBmCtrl + offset;
    mMethodsGet(osal)->MemInit(osal, longReg, 0, sizeof(longReg));
    mRegFieldSet(longReg[1], cThaPdhPWPlaAsmCEPEBmEmptyCtrl, cBitMaskEmpty);

    if (!isTug3MapVc3)
        Vc3Tug3EbmControlVtgTypeRegFieldSet(tug3, longReg);

    mChannelHwLongWrite(tug3, regAddr, longReg, cThaLongRegMaxSize, cThaModulePwe);
    return cAtOk;
    }

static eAtRet Vc4FragmentTypeSet(ThaModulePwe self, AtSdhChannel vc4)
    {
    uint8 tug3Id;
    AtSdhChannel tug3;
    eAtRet ret = cAtOk;

    if (AtSdhChannelMapTypeGet(vc4) == cAtSdhVcMapTypeVc4MapC4)
        return cAtOk;

    for (tug3Id = 0; tug3Id < AtSdhChannelNumberOfSubChannelsGet(vc4); tug3Id++)
        {
        tug3 = AtSdhChannelSubChannelGet(vc4, tug3Id);
        ret |= Tug3PwCepFractionalSet(self, tug3);
        }

    return ret;
    }

static eAtRet Vc3FragmenTypeSet(ThaModulePwe self, AtSdhChannel vc3)
    {
    uint32 regAddr;
    uint32 longReg[cThaLongRegMaxSize];
    uint32 offset = SdhChannelOffset(self, vc3);
    AtOsal osal = AtSharedDriverOsalGet();

    regAddr = cThaRegThalassaPdhPWPldAssembleCEPFractionalEBmCtrl + offset;
    mMethodsGet(osal)->MemInit(osal, longReg, 0, sizeof(longReg));

    if (AtSdhChannelMapTypeGet(vc3) == cAtSdhVcMapTypeVc3Map7xTug2s)
        {
        Vc3Tug3EbmControlVtgTypeRegFieldSet(vc3, longReg);
        mRegFieldSet(longReg[1], cThaPdhPWPlaAsmCEPEBmEmptyCtrl, cBitMaskEmpty);
        }

    mChannelHwLongWrite(vc3, regAddr, longReg, cThaLongRegMaxSize, cThaModulePwe);
    return cAtOk;
    }

static eBool Vc3IsMappedE3(AtSdhChannel sdhVc)
    {
    AtPdhChannel pdhChannel = (AtPdhChannel)AtSdhChannelMapChannelGet(sdhVc);
    eAtPdhDe3FrameType de3FrameType = AtPdhChannelFrameTypeGet(pdhChannel);

    if ((de3FrameType == cAtPdhE3Unfrm)   ||
        (de3FrameType == cAtPdhE3Frmg832) ||
        (de3FrameType == cAtPdhE3FrmG751))
        return cAtTrue;

    return cAtFalse;
    }

static uint8 DefaultFractionalJ1Value(AtSdhChannel sdhVc)
    {
    static const uint8 cJ1ReturnDefaultValue = 1;
    static const uint8 cJ1Vc3FractionalE3Value = 567 / 9;
    static const uint8 cJ1Vc3FractionalDs3Value = 729 / 9;

    if (AtSdhChannelTypeGet(sdhVc) != cAtSdhChannelTypeVc3)
        return cJ1ReturnDefaultValue;

    if (AtSdhChannelMapTypeGet(sdhVc) != cAtSdhVcMapTypeVc3MapDe3)
        return cJ1ReturnDefaultValue;

    if (Vc3IsMappedE3(sdhVc))
        return cJ1Vc3FractionalE3Value;

    return cJ1Vc3FractionalDs3Value;
    }

static eAtRet CepFractionalJ1ReturnDefault(ThaModulePwe self, AtSdhChannel sdhVc)
    {
    uint32 address = cThaPdhPWPlaCepFractionalJ1ReturnControl + SdhChannelOffset(self, sdhVc);
    mChannelHwWrite(sdhVc, address, DefaultFractionalJ1Value(sdhVc), cThaModulePwe);

    return cAtOk;
    }

static eAtRet VcFragmentTypeSet(ThaModulePwe self, AtSdhChannel sdhVc)
    {
    eAtSdhChannelType vcType = AtSdhChannelTypeGet(sdhVc);
    eAtRet ret = cAtOk;

    if (vcType == cAtSdhChannelTypeVc4)
        ret = Vc4FragmentTypeSet(self, sdhVc);
    else if (vcType == cAtSdhChannelTypeVc3)
        ret = Vc3FragmenTypeSet(self, sdhVc);
    else
        return cAtErrorModeNotSupport;

    ret |= CepFractionalJ1ReturnDefault(self, sdhVc);
    ret |= NewEBMActivate(self, sdhVc);
    return ret;
    }

static eAtRet PwHeaderCepEbmEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    uint32 address   = cThaRegPWEncPWHdrCtrl + mPwPweOffset(self, pw);
    uint32 regVal    = mChannelHwRead(pw, address, cThaModulePwe);
    AtSdhChannel sdhVc = (AtSdhChannel)AtPwBoundCircuitGet(pw);

    mRegFieldSet(regVal, cThaPWEncCEPEBmEnCtrl, mBoolToBin(enable));
    mChannelHwWrite(pw, address, regVal, cThaModulePwe);

    return NewEBMActivate(self, sdhVc);
    }

#define cThaRegPwTohEnableControl   0x2E0001
#define cThaRegPwTohEnablePwIdMask  cBit10_2
#define cThaRegPwTohEnablePwIdShift 2

#define cThaRegPwTohEnableStateMask  cBit1_0
#define cThaRegPwTohEnableStateShift 0

static eAtRet DisableTohPwIsDone(ThaModulePwe self, AtPw pw)
    {
    const uint32 cTimeoutMs = 1500;
    tAtOsalCurTime startTime, curTime;
    uint32 elapseTime = 0;
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 address, offset, regVal;
    ThaModulePw pwModule = PwModule(self);

    if (IsSimulated(self))
        return cAtOk;

    offset = ThaModulePwePartOffset(self, ThaModulePwPartOfPw(pwModule, pw));
    address = cThaRegPwTohEnableControl + offset;

    mMethodsGet(osal)->CurTimeGet(osal, &startTime);
    while (elapseTime < cTimeoutMs)
        {
        regVal = mChannelHwRead(pw, address, cThaModulePwe);
        if (mRegField(regVal, cThaRegPwTohEnableState) == 1) /* Ready */
            return cAtOk;

        /* Continue waiting */
        mMethodsGet(osal)->CurTimeGet(osal, &curTime);
        elapseTime = mTimeIntervalInMsGet(startTime, curTime);
        }

    AtChannelLog((AtChannel)pw, cAtLogLevelCritical, AtSourceLocation,"Disable Toh pw timeout \r\n");

    return cAtErrorDevFail;
    }

static eAtRet PwTohEnableStart(ThaModulePwe self, AtPw pw, eBool enable)
    {
    ThaModulePw pwModule = PwModule(self);
    uint32 offset = ThaModulePwePartOffset(self, ThaModulePwPartOfPw(pwModule, pw));
    uint32 address = cThaRegPwTohEnableControl + offset;
    uint32 regVal = 0;

    mRegFieldSet(regVal, cThaRegPwTohEnablePwId, AtChannelHwIdGet((AtChannel)pw));
    mRegFieldSet(regVal, cThaRegPwTohEnableState, (enable) ? 1 : 2);

    mChannelHwWrite(pw, address, regVal, cThaModulePwe);
    if (enable)
        return cAtOk;

    /* Incase of disable, need to wait until reach first EOP or timeout */
    return DisableTohPwIsDone(self, pw);
    }

static eAtRet EnableTohPwIsDone(ThaModulePwe self, AtPw pw)
    {
    const uint32 cTimeoutMs = 1500;
    tAtOsalCurTime startTime, curTime;
    uint32 elapseTime = 0;
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 address, offset, regVal;
    ThaModulePw pwModule = PwModule(self);

    if (IsSimulated(self))
        return cAtOk;

    offset = ThaModulePwePartOffset(self, ThaModulePwPartOfPw(pwModule, pw));
    address = cThaRegPwTohEnableControl + offset;

    mMethodsGet(osal)->CurTimeGet(osal, &startTime);
    while (elapseTime < cTimeoutMs)
        {
        regVal = mChannelHwRead(pw, address, cThaModulePwe);
        if (mRegField(regVal, cThaRegPwTohEnableState) == 0) /* Ready */
            return cAtOk;

        /* Continue waiting */
        mMethodsGet(osal)->CurTimeGet(osal, &curTime);
        elapseTime = mTimeIntervalInMsGet(startTime, curTime);
        }

    AtChannelLog((AtChannel)pw, cAtLogLevelCritical, AtSourceLocation, "Enable Toh pw timeout \r\n");
    return cAtErrorDevFail;
    }

static eAtRet PwTohEnableStop(ThaModulePwe self, AtPw pw, eBool enable)
    {
    ThaModulePw pwModule = PwModule(self);
    uint32 offset = ThaModulePwePartOffset(self, ThaModulePwPartOfPw(pwModule, pw));
    uint32 address = cThaRegPwTohEnableControl + offset;
    uint32 regVal = 0;

    mRegFieldSet(regVal, cThaRegPwTohEnablePwId, AtChannelHwIdGet((AtChannel)pw));
    mRegFieldSet(regVal, cThaRegPwTohEnableState, (enable) ? 3 : 0);

    mChannelHwWrite(pw, address, regVal, cThaModulePwe);
    if (!enable)
        return cAtOk;

    /* Incase of enable, need to wait until reach first EOP or timeout */
    return EnableTohPwIsDone(self, pw);
    }

static eAtRet LineTohEnable(ThaModulePwe self, AtSdhLine line, eBool enable)
    {
    uint8 lineHwId = (uint8)AtChannelHwIdGet((AtChannel)line);
    uint32 address = cThaPdhPwPlaTohLineEnableControl + LinePartOffset(self, line);
    uint32 regVal  = mChannelHwRead(line, address, cThaModulePwe);

    mFieldIns(&regVal, cThaPdhPwPlaTohLineEnableMask(lineHwId), cThaPdhPwPlaTohLineEnableShift(lineHwId), mBoolToBin(enable));
    mChannelHwWrite(line, address, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool LineTohIsEnabled(ThaModulePwe self, AtSdhLine line)
    {
    uint8 lineHwId = (uint8)AtChannelHwIdGet((AtChannel)line);
    uint32 address = cThaPdhPwPlaTohLineEnableControl + LinePartOffset(self, line);
    uint32 regVal  = mChannelHwRead(line, address, cThaModulePwe);
    uint8 isEnabled;

    mFieldGet(regVal, cThaPdhPwPlaTohLineEnableMask(lineHwId), cThaPdhPwPlaTohLineEnableShift(lineHwId), uint8, &isEnabled);
    return (isEnabled > 0) ? cAtTrue : cAtFalse;
    }

static eAtRet PwTohEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    eAtRet ret = cAtOk;
    AtSdhLine line = (AtSdhLine)AtPwBoundCircuitGet(pw);
    eBool needProtocol = cAtTrue;
    AtPwToh pwToh = (AtPwToh)ThaPwAdapterPwGet((ThaPwAdapter)pw);

    if (line == NULL)
        return (enable == cAtFalse) ? cAtOk : cAtError;

    /* Do nothing if it is already in expected state */
    if (LineTohIsEnabled(self, line) == enable)
        return cAtOk;

    /* Important: if there is no TOH byte, there will be no running packets,
     * protocol will return fail, so don't need protocol in this case */
    needProtocol = (AtPwTohNumEnabledBytesGet(pwToh) > 0) ? cAtTrue : cAtFalse;

    if (needProtocol)
        ret = PwTohEnableStart(self, pw, enable);

    ret |= LineTohEnable(self, line, enable);

    if (needProtocol)
        ret |= PwTohEnableStop(self, pw, enable);

    return ret;
    }

static eBool PwTohIsEnabled(ThaModulePwe self, AtPw pw)
    {
    uint32 address, regVal;
    uint8 lineHwId, bitVal;
    AtSdhLine line = (AtSdhLine)AtPwBoundCircuitGet(pw);
    if (line == NULL)
        return cAtFalse;

    lineHwId = (uint8)AtChannelHwIdGet((AtChannel)line);
    address  = cThaPdhPwPlaTohLineEnableControl + LinePartOffset(self, line);
    regVal   = mChannelHwRead(line, address, cThaModulePwe);

    mFieldGet(regVal, cThaPdhPwPlaTohLineEnableMask(lineHwId), cThaPdhPwPlaTohLineEnableShift(lineHwId), uint8, &bitVal);

    return (bitVal) ? cAtTrue : cAtFalse;
    }

static uint32 TohMappingRegOffset(ThaModulePwe self, AtSdhLine line, uint8 stsId)
    {
    uint32 partOffset = LinePartOffset(self, line);
    eAtSdhLineRate rate = AtSdhLineRateGet(line);

    if (rate == cAtSdhLineRateStm1)
        return (uint32)((stsId * 4UL) + AtChannelHwIdGet((AtChannel)line) + partOffset);

    return (uint32)((stsId * 2UL) + AtChannelHwIdGet((AtChannel)line) + partOffset);
    }

static void TohMappingReset(ThaModulePwe self, AtSdhLine line)
    {
    uint8 sts_i, numSts = (AtSdhLineRateGet(line) == cAtSdhLineRateStm1) ? 3 : 12;
    uint32 address;
    uint32 longReg[cThaLongRegMaxSize];
    AtOsalMemInit(longReg, 0, sizeof(uint32) * cThaLongRegMaxSize);

    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        address = cThaPdhPwPlaTohMappingControl + TohMappingRegOffset(self, line, sts_i);
        mChannelHwLongWrite(line, address, longReg, cThaLongRegMaxSize, cThaModulePwe);
        }
    }

static eAtRet BindLineToPw(ThaModulePwe self, AtSdhLine line, AtPw pw)
    {
	AtUnused(pw);
    TohMappingReset(self, line);
    return cAtOk;
    }

static eAtRet PwTohFirstByteEnable(ThaModulePwe self, AtPw pwAdapter, const tThaTohByte *tohByte, eBool enable)
    {
    AtSdhLine line = (AtSdhLine)AtPwBoundCircuitGet(pwAdapter);
    uint32 longReg[cThaLongRegMaxSize];
    uint32 address;
    uint8 row = 0;
    uint8 col = 0;

    if (line == NULL)
        return cAtError;

    if (enable)
        ThaTohByteRowColGet(tohByte, &row, &col);

    address = cThaPdhPwPlaTohMappingControl + TohMappingRegOffset(self, line, tohByte->sts1);
    mChannelHwLongRead(pwAdapter, address, longReg, cThaLongRegMaxSize, cThaModulePwe);

    mRegFieldSet(longReg[cThaPdhPWPlaAsmTOHFirstByteEnDw], cThaPdhPWPlaAsmTOHFirstByteEn, mBoolToBin(enable));
    mRegFieldSet(longReg[cThaPdhPWPlaAsmTOHFirstByteColDw], cThaPdhPWPlaAsmTOHFirstByteCol, col);
    mRegFieldSet(longReg[cThaPdhPWPlaAsmTOHFirstByteRowDw], cThaPdhPWPlaAsmTOHFirstByteRow, row);

    mChannelHwLongWrite(pwAdapter, address, longReg, cThaLongRegMaxSize, cThaModulePwe);
    return cAtOk;
    }

static tThaTohByte* PwTohFirstByteGet(ThaModulePwe self, AtPw pwAdapter, tThaTohByte *tohByte)
    {
    AtSdhLine line = (AtSdhLine)AtPwBoundCircuitGet(pwAdapter);
    uint32 longReg[cThaLongRegMaxSize];
    uint32 address;
    uint8 row = 0;
    uint8 col = 0;
    uint8 numSts, sts_i;
    eBool firstFound = cAtFalse;

    if ((line == NULL) || (tohByte == NULL))
        return NULL;

    numSts = (AtSdhLineRateGet(line) == cAtSdhLineRateStm1) ? 3 : 12;
    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        address = cThaPdhPwPlaTohMappingControl + TohMappingRegOffset(self, line, sts_i);
        mChannelHwLongRead(pwAdapter, address, longReg, cThaLongRegMaxSize, cThaModulePwe);

        if (mRegField(longReg[cThaPdhPWPlaAsmTOHFirstByteEnDw], cThaPdhPWPlaAsmTOHFirstByteEn) == 0)
            continue;

        /* Opps, There is more than one first, return NULL to indicate that something wrong */
        if (firstFound)
            return NULL;

        firstFound = cAtTrue;
        col = (uint8)mRegField(longReg[cThaPdhPWPlaAsmTOHFirstByteColDw], cThaPdhPWPlaAsmTOHFirstByteCol);
        row = (uint8)mRegField(longReg[cThaPdhPWPlaAsmTOHFirstByteRowDw], cThaPdhPWPlaAsmTOHFirstByteRow);
        tohByte->sts1 = sts_i;
        tohByte->ohByte = ThaTohByteFromRowColGet(row, col);

        if ((uint32)tohByte->ohByte == cThaInvalidTohByte)
            return NULL;
        }

    return (firstFound) ? tohByte : NULL;
    }

static uint8 PwTohBytesBitmapGet(ThaModulePwe self, AtPw pwAdapter, uint32* bitmaps)
    {
    AtSdhLine line = (AtSdhLine)AtPwBoundCircuitGet(pwAdapter);
    uint32 longReg[cThaLongRegMaxSize];
    uint32 address;
    uint8 numSts, sts_i;

    if (line == NULL)
        return 0;

    numSts = (AtSdhLineRateGet(line) == cAtSdhLineRateStm1) ? 3 : 12;
    if (bitmaps == NULL)
        return numSts;

    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        address = cThaPdhPwPlaTohMappingControl + TohMappingRegOffset(self, line, sts_i);
        mChannelHwLongRead(pwAdapter, address, longReg, cThaLongRegMaxSize, cThaModulePwe);

        bitmaps[sts_i] = mRegField(longReg[cThaPdhPWPlaAsmTOHByteEnCtrlDw], cThaPdhPWPlaAsmTOHByteBitmap);
        }

    return numSts;
    }

static eAtRet PwTohLastByteEnable(ThaModulePwe self, AtPw pwAdapter, const tThaTohByte *tohByte, eBool enable)
    {
    AtSdhLine line = (AtSdhLine)AtPwBoundCircuitGet(pwAdapter);
    uint32 longReg[cThaLongRegMaxSize];
    uint32 address;
    uint8 row = 0;
    uint8 col = 0;

    if (line == NULL)
        return cAtError;

    if (enable)
        ThaTohByteRowColGet(tohByte, &row, &col);

    address = cThaPdhPwPlaTohMappingControl + TohMappingRegOffset(self, line, tohByte->sts1);
    mChannelHwLongRead(pwAdapter, address, longReg, cThaLongRegMaxSize, cThaModulePwe);

    mRegFieldSet(longReg[cThaPdhPWPlaAsmTOHLastByteEnDw], cThaPdhPWPlaAsmTOHLastByteEn, mBoolToBin(enable));
    mRegFieldSet(longReg[cThaPdhPWPlaAsmTOHLastByteColDw], cThaPdhPWPlaAsmTOHLastByteCol, col);
    mRegFieldSet(longReg[cThaPdhPWPlaAsmTOHLastByteRowHeadDw], cThaPdhPWPlaAsmTOHLastByteRowHead, row >> 3);
    mRegFieldSet(longReg[cThaPdhPWPlaAsmTOHLastByteRowTailDw], cThaPdhPWPlaAsmTOHLastByteRowTail, row);

    mChannelHwLongWrite(pwAdapter, address, longReg, cThaLongRegMaxSize, cThaModulePwe);

    return cAtOk;
    }

static tThaTohByte* PwTohLastByteGet(ThaModulePwe self, AtPw pwAdapter, tThaTohByte *tohByte)
    {
    AtSdhLine line = (AtSdhLine)AtPwBoundCircuitGet(pwAdapter);
    uint32 longReg[cThaLongRegMaxSize];
    uint32 address;
    uint8 row = 0;
    uint8 col = 0;
    uint8 numSts, sts_i;
    eBool lastFound = cAtFalse;

    if ((line == NULL) || (tohByte == NULL))
        return NULL;

    numSts = (AtSdhLineRateGet(line) == cAtSdhLineRateStm1) ? 3 : 12;
    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        address = cThaPdhPwPlaTohMappingControl + TohMappingRegOffset(self, line, sts_i);
        mChannelHwLongRead(pwAdapter, address, longReg, cThaLongRegMaxSize, cThaModulePwe);

        if (mRegField(longReg[cThaPdhPWPlaAsmTOHLastByteEnDw], cThaPdhPWPlaAsmTOHLastByteEn) == 0)
            continue;

        /* Opps, There is more than one last byte, return NULL to indicate that something wrong */
        if (lastFound)
            return NULL;

        lastFound = cAtTrue;
        col = (uint8)mRegField(longReg[cThaPdhPWPlaAsmTOHLastByteColDw], cThaPdhPWPlaAsmTOHLastByteCol);
        row = (uint8)mRegField(longReg[cThaPdhPWPlaAsmTOHLastByteRowHeadDw], cThaPdhPWPlaAsmTOHLastByteRowHead);
        row = (uint8)((uint32)(row << 3) | mRegField(longReg[cThaPdhPWPlaAsmTOHLastByteRowTailDw], cThaPdhPWPlaAsmTOHLastByteRowTail));

        tohByte->sts1 = sts_i;
        tohByte->ohByte = ThaTohByteFromRowColGet(row, col);

        if ((uint32)tohByte->ohByte == cThaInvalidTohByte)
            return NULL;
        }

    return (lastFound) ? tohByte : NULL;
    }

static eAtRet PwTohByteEnable(ThaModulePwe self, AtPw pwAdapter, const tThaTohByte *tohByte, eBool enable)
    {
    AtSdhLine line = (AtSdhLine)AtPwBoundCircuitGet(pwAdapter);
    uint32 longReg[cThaLongRegMaxSize];
    uint32 address;
    uint8 row = 0, col = 0;

    if (line == NULL)
        return cAtError;

    ThaTohByteRowColGet(tohByte, &row, &col);

    address = cThaPdhPwPlaTohMappingControl + TohMappingRegOffset(self, line, tohByte->sts1);
    mChannelHwLongRead(pwAdapter, address, longReg, cThaLongRegMaxSize, cThaModulePwe);
    mRegFieldSet(longReg[cThaPdhPWPlaAsmTOHPWIDDw], cThaPdhPWPlaAsmTOHPWID, AtChannelHwIdGet((AtChannel)pwAdapter));

    mFieldIns(&longReg[cThaPdhPWPlaAsmTOHByteEnCtrlDw],
              cThaPdhPWPlaAsmTOHByteEnCtrlMask(row, col),
              cThaPdhPWPlaAsmTOHByteEnCtrlShift(row, col),
              mBoolToBin(enable));

    mChannelHwLongWrite(pwAdapter, address, longReg, cThaLongRegMaxSize, cThaModulePwe);

    return cAtOk;
    }

static eAtRet PwTohPayloadSizeSet(ThaModulePwe self, AtPw pw, uint32 numFramePerPacket)
    {
    AtSdhLine line = (AtSdhLine)AtPwBoundCircuitGet(pw);
    uint32 address = cThaRegThalassaPdhPWPldAssembleTohPldSizeCtrl + AtChannelHwIdGet((AtChannel)line) + LinePartOffset(self, line);

    uint32 regVal = 0;
    mRegFieldSet(regVal, cThaPdhPWPlaAsmTohPldSizeCtrl, numFramePerPacket - 1);
    mChannelHwWrite(pw, address, regVal, cThaModulePwe);

    return cAtOk;
    }

static uint16 PwTohPayloadSizeGet(ThaModulePwe self, AtPw pw)
    {
    AtSdhLine line = (AtSdhLine)AtPwBoundCircuitGet(pw);
    uint32 address = cThaRegThalassaPdhPWPldAssembleTohPldSizeCtrl + AtChannelHwIdGet((AtChannel)line) + LinePartOffset(self, line);

    uint32 regVal = mChannelHwRead(pw, address, cThaModulePwe);
    return (uint16)(mRegField(regVal, cThaPdhPWPlaAsmTohPldSizeCtrl) + 1);
    }

static uint16 PwTohMaxNumFrames(ThaModulePwe self)
    {
	AtUnused(self);
    return 32;
    }

static eAtRet PwStmPortSet(ThaModulePwe self, AtPw pwAdapter, uint8 lineId)
    {
	AtUnused(lineId);
	AtUnused(pwAdapter);
	AtUnused(self);
    /* Let sub class do */
    return cAtOk;
    }

static eAtRet PwCwAutoTxNPBitEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    uint32 regAddr, regVal;

    regAddr = cThaRegPWEncPWHdrCtrl + mPwPweOffset(self, pw);
    regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    mFieldIns(&regVal, cThaPWEncCEPEPAREnCtrlMask, cThaPWEncCEPEPAREnCtrlShift, (enable ? 1 : 0));
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static void PwRegsShow(ThaModulePwe self, AtPw pw)
    {
    AtPw adapter = (AtPw)ThaPwAdapterGet(pw);

    mMethodsGet(self)->PwPlaRegsShow(self, adapter);
    mMethodsGet(self)->PwPweRegsShow(self, adapter);
    }

static void PwPlaRegsShow(ThaModulePwe self, AtPw pw)
    {
    uint32 offset = mPwPlaOffset(self, pw);
    uint32 address = mMethodsGet(self)->PWPdhPlaPldSizeCtrl(self) + offset;
    ThaModulePweRegDisplay(pw, "PWPDH Payload Assemble Payload Size Control", address);

    address = mMethodsGet(self)->PDHPWPlaSignalingInverseLookupControl(self) + offset;
    ThaModulePweRegDisplay(pw, "PDHPW Payload Assemble Signaling Inverse Lookup Control", address);

    address = mMethodsGet(self)->PWPdhPlaEngStat(self) + offset;
    ThaModulePweLongRegDisplay(pw, "PWPDH Payload Assemble Engine Status", address);
    }

static void PwPweRegsShow(ThaModulePwe self, AtPw pw)
    {
    uint32 offset = mPwPweOffset(self, pw);
    uint32 address = cThaRegPWEncPWHdrCtrl + offset;

    ThaModulePweRegDisplay(pw, "PWENC PW Header Control", address);
    }

static eBool PwCwAutoTxNPBitIsEnabled(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr, regVal;

    regAddr = cThaRegPWEncPWHdrCtrl + mPwPweOffset(self, pw);
    regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    return (regVal & cThaPWEncCEPEPAREnCtrlMask) ? cAtTrue : cAtFalse;
    }

static eBool PwCwAutoTxNPBitIsConfigurable(ThaModulePwe self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet PwRemovingPrepare(ThaModulePwe self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cAtOk;
    }

static uint32 CounterOffset(AtPw pw)
    {
    return AtChannelIdGet((AtChannel)pw);
    }

static void ProfileInfoDisplay(ThaModulePwe self)
    {
    AtPrintc(cSevNormal, "* maxPwAddingWaitTime: %u(ms)\r\n", mThis(self)->maxPwAddingWaitTimeInMs);
    }

static const char *TypeString(AtModule self)
    {
    AtUnused(self);
    return "pwe";
    }

static eAtRet PwPtchInsertionModeSet(ThaModulePwe self, AtPw pw, eAtPtchMode insertMode)
    {
    /* Let concrete product do */
    AtUnused(self);
    AtUnused(pw);
    AtUnused(insertMode);
    return cAtErrorModeNotSupport;
    }

static eAtPtchMode PwPtchInsertionModeGet(ThaModulePwe self, AtPw pw)
    {
    /* Let concrete product do */
    AtUnused(self);
    AtUnused(pw);
    return cAtPtchModeUnknown;
    }

static eAtRet EthFlowPtchInsertionModeSet(ThaModulePwe self, AtEthFlow flow, eAtPtchMode insertMode)
    {
    /* Let concrete product do */
    AtUnused(self);
    AtUnused(flow);
    AtUnused(insertMode);
    return cAtErrorModeNotSupport;
    }

static eAtPtchMode EthFlowPtchInsertionModeGet(ThaModulePwe self, AtEthFlow flow)
    {
    /* Let concrete product do */
    AtUnused(self);
    AtUnused(flow);
    return cAtPtchModeUnknown;
    }

static eAtRet PwJitterAttenuatorEnable(ThaModulePwe self, AtPw pwAdapter, eBool enable)
    {
    AtUnused(self);
    AtUnused(pwAdapter);
    AtUnused(enable);
    return cAtOk;
    }

static eAtRet HdlcPwPayloadTypeSet(ThaModulePwe self, ThaPwHdlcAdapter adapter, eAtPwHdlcPayloadType payloadType)
    {
    AtUnused(self);
    AtUnused(adapter);
    AtUnused(payloadType);
    return cAtErrorModeNotSupport;
    }

static eBool PwCwIsEnabled(ThaModulePwe self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cAtFalse;
    }

static uint8 HwPwType(ThaModulePwe self, AtPw pwAdapter, eAtPwType pwType)
    {
    AtUnused(self);

    if (pwType == cAtPwTypeCESoP)
        {
        AtPw pw = ThaPwAdapterPwGet((ThaPwAdapter)pwAdapter);
        if (pw)
            return (AtPwCESoPModeGet((AtPwCESoP)pw) == cAtPwCESoPModeBasic) ? 0x5 : 0x4;
        else
            return 0x4;
        }

    if (pwType == cAtPwTypeSAToP) return 0x0;
    if (pwType == cAtPwTypeCEP)   return 0x6;
    if (pwType == cAtPwTypeATM)   return 0x1;
    if (pwType == cAtPwTypeToh)   return 0x0; /* Same as SAToP */

    return 6;
    }

static eAtRet PwPtchServiceEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(enable);
    return cAtErrorNotImplemented;
    }

static eBool PwPtchServiceIsEnabled(ThaModulePwe self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cAtFalse;
    }

static eAtRet EthFlowPtchServiceEnable(ThaModulePwe self, AtEthFlow flow, eBool enable)
    {
    AtUnused(self);
    AtUnused(flow);
    AtUnused(enable);
    return cAtErrorNotImplemented;
    }

static eBool EthFlowPtchServiceIsEnabled(ThaModulePwe self, AtEthFlow flow)
    {
    AtUnused(self);
    AtUnused(flow);
    return cAtFalse;
    }

static eBool TxPwActiveHwIsReady(ThaModulePwe self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet TxPwActiveForce(ThaModulePwe self, AtPw pw, eBool enable)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(enable);
    return cAtOk;
    }

static eBool  TxPwActiveIsForced(ThaModulePwe self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cAtFalse;
    }

static uint32 BaseAddress(ThaModulePwe self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 PlaBaseAddress(ThaModulePwe self)
    {
    AtUnused(self);
    return 0;
    }

static eBool PwRemovingShouldStart(ThaModulePwe self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return AtModuleInAccessible((AtModule)self) ? cAtFalse : cAtTrue;
    }

static void CacheUsageGet(ThaModulePwe self, tThaPweCache *cache)
    {
    AtUnused(self);
    AtOsalMemInit(cache, 0, sizeof(tThaPweCache));
    }

static eBool CanAccessCacheUsage(ThaModulePwe self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void EthFlowRegsShow(ThaModulePwe self, AtEthFlow flow)
    {
    AtUnused(self);
    AtUnused(flow);
    }

static eAtRet HwResourceCheck(AtModule self)
    {
    ThaModulePwe modulePwe = (ThaModulePwe)self;
    uint32 maxNumBlocks = mMethodsGet(modulePwe)->MaxNumCacheBlocks(modulePwe);
    uint32 numFreeBlocks = mMethodsGet(modulePwe)->NumFreeBlocksHwGet(modulePwe);
    AtDevice device = AtModuleDeviceGet(self);

    if (AtDeviceIsSimulated(device) || AtDeviceInAccessible(device))
        return cAtOk;

    AtModuleResourceDiminishedLog(self, "Blocks", numFreeBlocks, maxNumBlocks);
    return cAtOk;
    }

static uint32 MaxNumCacheBlocks(ThaModulePwe self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 NumFreeBlocksHwGet(ThaModulePwe self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static void DebuggerEntriesFill(AtModule self, AtDebugger debugger)
    {
    ThaModulePwe modulePwe = (ThaModulePwe)self;
    uint32 maxNumBlocks = mMethodsGet(modulePwe)->MaxNumCacheBlocks(modulePwe);
    uint32 numFreeBlocks = mMethodsGet(modulePwe)->NumFreeBlocksHwGet(modulePwe);

    if (!AtDeviceShouldCheckHwResource(AtModuleDeviceGet(self)))
        return;

    m_AtModuleMethods->DebuggerEntriesFill(self, debugger);
    AtDebuggerEntryAdd(debugger, AtDebugEntryInfoResourceNew("Blocks", maxNumBlocks, numFreeBlocks));
    }

static eBool PwIsSAToP_or_CESoP(AtPw pw)
    {
    eAtPwType pwType = AtPwTypeGet(pw);
    if ((pwType == cAtPwTypeSAToP) || (pwType == cAtPwTypeCESoP))
        return cAtTrue;

    return cAtFalse;
    }

static eBool CircuitIsDE1Channel(AtPdhChannel circuit)
    {
    uint8 type = AtPdhChannelTypeGet(circuit);
    if ((type == cAtPdhChannelTypeE1) || (type == cAtPdhChannelTypeDs1))
        return cAtTrue;

    return cAtFalse;
    }

static AtPdhChannel De1CircuitGetIfPossible(AtPw pw)
    {
    AtPdhChannel circuit;

    if (PwIsSAToP_or_CESoP(pw) == cAtFalse)
        return NULL;

    circuit = (AtPdhChannel)AtPwBoundCircuitGet(pw);
    if (circuit == NULL)
        return NULL;

    if (AtPwTypeGet(pw) == cAtPwTypeCESoP)
        return (AtPdhChannel)AtPdhNxDS0De1Get((AtPdhNxDS0)circuit);

    if (CircuitIsDE1Channel(circuit))
        return circuit;

    return NULL;
    }


static eBool PwCircuitIsLiuDe1(AtPw pw)
    {
    AtPdhChannel circuit = De1CircuitGetIfPossible(pw);
    if (circuit == NULL)
        return cAtFalse;

    return AtPdhChannelHasLineLayer(circuit);
    }

static eBool PwCircuitIsDe1OverStm(AtPw pw)
    {
    AtPdhChannel circuit = De1CircuitGetIfPossible(pw);
    if (circuit == NULL)
        return cAtFalse;

    return (AtPdhChannelHasLineLayer(circuit) ? cAtFalse : cAtTrue);
    }

static eAtRet PwIdleCodeSet(ThaModulePwe self, ThaPwAdapter pwAdapter, uint8 payloadIdleCode)
    {
    AtUnused(self);
    AtUnused(pwAdapter);
    AtUnused(payloadIdleCode);
    return cAtOk;
    }

static eAtRet PwCesopCasIdleCode1Set(ThaModulePwe self, ThaPwAdapter pwAdapter, uint8 signallingIdleCode)
    {
    AtUnused(self);
    AtUnused(pwAdapter);
    AtUnused(signallingIdleCode);
    return cAtOk;
    }

static uint8  PwCesopCasIdleCode1Get(ThaModulePwe self, ThaPwAdapter pwAdapter)
    {
    AtUnused(self);
    AtUnused(pwAdapter);
    return 0;
    }

static eAtRet PwIdleCodeInsertionEnable(ThaModulePwe self, ThaPwAdapter pwAdapter, eBool autoIdleEnable)
    {
    AtUnused(self);
    AtUnused(pwAdapter);
    AtUnused(autoIdleEnable);
    return cAtOk;
    }

static void MethodsInit(ThaModulePwe self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Override bit fields */
        mBitFieldOverride(ThaModulePwe, m_methods, PWPdhPlaAsmPldTypeCtrl)
        mBitFieldOverride(ThaModulePwe, m_methods, PWPdhPlaAsmPldSizeCtrl)

        /* Override register addresses */
        mMethodOverride(m_methods, PWPdhPlaPldSizeCtrl);
        mMethodOverride(m_methods, PWPdhPlaEngStat);
        mMethodOverride(m_methods, PDHPWAddingProtocolToPreventBurst);
        mMethodOverride(m_methods, PDHPWAddingPwMask);
        mMethodOverride(m_methods, PDHPWAddingStateMask);
        mMethodOverride(m_methods, PDHPWPlaSignalingInverseLookupControl);
        mMethodOverride(m_methods, BaseAddress);
        mMethodOverride(m_methods, PlaBaseAddress);

        /* Override methods */
        mMethodOverride(m_methods, DefaultSet);
        mMethodOverride(m_methods, CacheUsageGet);
        mMethodOverride(m_methods, CanAccessCacheUsage);
        mMethodOverride(m_methods, EthFlowRegsShow);
        mMethodOverride(m_methods, PwAddingPrepare);
        mMethodOverride(m_methods, PwAddingStart);
        mMethodOverride(m_methods, PwRemovingStart);
        mMethodOverride(m_methods, PwRemovingShouldStart);
        mMethodOverride(m_methods, PwRemovingFinish);
        mMethodOverride(m_methods, PwRemovingPrepare);
        mMethodOverride(m_methods, PwPweDefaultOffset);
        mMethodOverride(m_methods, PwPlaDefaultOffset);
        mMethodOverride(m_methods, PwRtpEnable);
        mMethodOverride(m_methods, PwCwEnable);
        mMethodOverride(m_methods, PwCwAutoTxLBitEnable);
        mMethodOverride(m_methods, PwCwAutoTxLBitIsEnabled);
        mMethodOverride(m_methods, PwCwAutoTxRBitEnable);
        mMethodOverride(m_methods, PwCwAutoTxRBitIsEnabled);
        mMethodOverride(m_methods, PwCwAutoTxMBitEnable);
        mMethodOverride(m_methods, PwCwAutoTxMBitIsEnabled);
        mMethodOverride(m_methods, PwCwAutoTxNPBitEnable);
        mMethodOverride(m_methods, PwTypeSet);
        mMethodOverride(m_methods, HwPwType);
        mMethodOverride(m_methods, PwTxPacketsGet);
        mMethodOverride(m_methods, PwTxBytesGet);
        mMethodOverride(m_methods, PwTxLbitPacketsGet);
        mMethodOverride(m_methods, PwTxRbitPacketsGet);
        mMethodOverride(m_methods, PwTxMbitPacketsGet);
        mMethodOverride(m_methods, PwTxNbitPacketsGet);
        mMethodOverride(m_methods, PwTxPbitPacketsGet);
        mMethodOverride(m_methods, PayloadSizeSet);
        mMethodOverride(m_methods, PayloadSizeGet);
        mMethodOverride(m_methods, PwSupressEnable);
        mMethodOverride(m_methods, PwSupressIsEnabled);
        mMethodOverride(m_methods, NumFramesInPacketGet);
        mMethodOverride(m_methods, PwNxDs0Set);
        mMethodOverride(m_methods, PwNumDs0TimeslotsSet);
        mMethodOverride(m_methods, PwNumDs0TimeslotsGet);
        mMethodOverride(m_methods, PwNumFramesInPacketSet);
        mMethodOverride(m_methods, PwCesMaxNumFramesInPacket);
        mMethodOverride(m_methods, PwEnable);
        mMethodOverride(m_methods, PwIsEnabled);
        mMethodOverride(m_methods, NumVlansSet);
        mMethodOverride(m_methods, HeaderWrite);
        mMethodOverride(m_methods, RawHeaderWrite);
        mMethodOverride(m_methods, HeaderRead);
        mMethodOverride(m_methods, PwPsnTypeSet);
        mMethodOverride(m_methods, PwDebug);
        mMethodOverride(m_methods, PwRamStatusClear);
        mMethodOverride(m_methods, PwTxAlarmForce);
        mMethodOverride(m_methods, PwTxAlarmUnForce);
        mMethodOverride(m_methods, PwTxForcedAlarmGet);
        mMethodOverride(m_methods, PwTxLBitForce);
        mMethodOverride(m_methods, PwTxLBitIsForced);
        mMethodOverride(m_methods, PwTxRBitForce);
        mMethodOverride(m_methods, PwTxRBitIsForced);
        mMethodOverride(m_methods, PwTxMBitForce);
        mMethodOverride(m_methods, PwTxMBitIsForced);
        mMethodOverride(m_methods, VcFragmentTypeSet);
        mMethodOverride(m_methods, PwCepEquip);
        mMethodOverride(m_methods, PwCepFractionalEquippedChannelsGet);
        mMethodOverride(m_methods, PwHeaderCepEbmEnable);
        mMethodOverride(m_methods, PwStmPortSet);
        mMethodOverride(m_methods, PwTohByteEnable);
        mMethodOverride(m_methods, PwTohFirstByteEnable);
        mMethodOverride(m_methods, PwTohLastByteEnable);
        mMethodOverride(m_methods, PwTohEnable);
        mMethodOverride(m_methods, PwTohIsEnabled);
        mMethodOverride(m_methods, PwTohPayloadSizeSet);
        mMethodOverride(m_methods, PwTohPayloadSizeGet);
        mMethodOverride(m_methods, BindLineToPw);
        mMethodOverride(m_methods, PwTohFirstByteGet);
        mMethodOverride(m_methods, PwTohLastByteGet);
        mMethodOverride(m_methods, PwTohBytesBitmapGet);
        mMethodOverride(m_methods, PwTohMaxNumFrames);
        mMethodOverride(m_methods, PwRegsShow);
        mMethodOverride(m_methods, PwPlaRegsShow);
        mMethodOverride(m_methods, PwPweRegsShow);
        mMethodOverride(m_methods, ProfileInfoDisplay);
        mMethodOverride(m_methods, PwCwAutoTxNPBitIsEnabled);
        mMethodOverride(m_methods, PwCwAutoTxNPBitIsConfigurable);
        mMethodOverride(m_methods, CounterOffset);
        mMethodOverride(m_methods, PwPtchInsertionModeSet);
        mMethodOverride(m_methods, PwPtchInsertionModeGet);
        mMethodOverride(m_methods, EthFlowPtchInsertionModeSet);
        mMethodOverride(m_methods, EthFlowPtchInsertionModeGet);
        mMethodOverride(m_methods, PwJitterAttenuatorEnable);
        mMethodOverride(m_methods, HdlcPwPayloadTypeSet);
        mMethodOverride(m_methods, PwCwIsEnabled);
        mMethodOverride(m_methods, PwPtchServiceEnable);
        mMethodOverride(m_methods, PwPtchServiceIsEnabled);
        mMethodOverride(m_methods, EthFlowPtchServiceEnable);
        mMethodOverride(m_methods, EthFlowPtchServiceIsEnabled);
        mMethodOverride(m_methods, TxPwActiveHwIsReady);
        mMethodOverride(m_methods, TxPwActiveForce);
        mMethodOverride(m_methods, TxPwActiveIsForced);
        mMethodOverride(m_methods, SubPortVlanBypass);
        mMethodOverride(m_methods, SubPortVlanIsBypassed);
        mMethodOverride(m_methods, MaxNumCacheBlocks);
        mMethodOverride(m_methods, NumFreeBlocksHwGet);

        mMethodOverride(m_methods, PwIdleCodeSet);
        mMethodOverride(m_methods, PwCesopCasIdleCode1Set);
        mMethodOverride(m_methods, PwCesopCasIdleCode1Get);
        mMethodOverride(m_methods, PwIdleCodeInsertionEnable);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtObject(AtModule self)
    {
    AtObject object = (AtObject)self;

    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static eAtRet Debug(AtModule self)
    {
    m_AtModuleMethods->Debug(self);

    AtPrintc(cSevNormal, "\r\n");
    AtPrintc(cSevInfo, "PWE information\r\n");
    AtPrintc(cSevInfo, "========================================================\r\n");
    mMethodsGet(mThis(self))->ProfileInfoDisplay(mThis(self));

    /* Clear statistic */
    mThis(self)->maxPwAddingWaitTimeInMs = 0;
    return cAtOk;
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, Debug);
        mMethodOverride(m_AtModuleOverride, TypeString);
        mMethodOverride(m_AtModuleOverride, HwResourceCheck);
        mMethodOverride(m_AtModuleOverride, DebuggerEntriesFill);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideAtObject(self);
    }

AtModule ThaModulePweObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaModulePwe));

    /* Super constructor */
    if (AtModuleObjectInit(self, cThaModulePwe, device) == NULL)
        return NULL;

    /* Override */
    Override(self);
    MethodsInit((ThaModulePwe)self);
    m_methodsInit = 1;

    return self;
    }

AtModule ThaModulePweNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, sizeof(tThaModulePwe));
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ThaModulePweObjectInit(newModule, device);
    }

void ThaModulePweEthFlowRegsShow(AtEthFlow flow)
    {
    ThaModulePwe modulePwe = (ThaModulePwe)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)flow), cThaModulePwe);
    AtPrintc(cSevInfo, "* Pwe registers of '%s':\r\n", AtObjectToString((AtObject)flow));
    if (modulePwe)
        mMethodsGet(modulePwe)->EthFlowRegsShow(modulePwe, flow);
    }

eBool ThaModulePwePwCwAutoTxMBitIsEnabled(ThaModulePwe self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwCwAutoTxMBitIsEnabled(self, pw);

    return cAtFalse;
    }

eAtRet ThaModulePwePwCwAutoTxMBitEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->PwCwAutoTxMBitEnable(self, pw, enable);

    return cAtError;
    }

eAtRet ThaModulePwePwAddingPrepare(ThaModulePwe self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwAddingPrepare(self, pw);

    return cAtError;
    }

eAtRet ThaModulePwePwAddingStart(ThaModulePwe self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwAddingStart(self, pw);

    return cAtError;
    }

uint32 ThaModulePwePartOffset(ThaModulePwe self, uint8 partId)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    return ThaDeviceModulePartOffset(device, cThaModulePwe, partId);
    }

eAtRet ThaModulePwePwNxDs0Set(ThaModulePwe self, AtPw pw, AtPdhNxDS0 nxDs0)
    {
    if (self)
        return mMethodsGet(self)->PwNxDs0Set(self, pw, nxDs0);

    return cAtError;
    }

uint8 ThaModulePweHwPwType(ThaModulePwe self, AtPw pwAdapter, eAtPwType pwType)
    {
    if (self)
        return mMethodsGet(self)->HwPwType(self, pwAdapter, pwType);
    return cInvalidUint8;
    }

eAtRet ThaModulePwePwRemovingStart(ThaModulePwe self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwRemovingStart(self, pw);

    return cAtError;
    }

eAtRet ThaModulePwePwRemovingFinish(ThaModulePwe self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwRemovingFinish(self, pw);

    return cAtError;
    }

eAtRet ThaModulePwePwRamStatusClear(ThaModulePwe self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwRamStatusClear(self, pw);

    return cAtError;
    }

eAtRet ThaModulePwePwEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->PwEnable(self, pw, enable);

    return cAtError;
    }

eBool ThaModulePwePwIsEnabled(ThaModulePwe self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwIsEnabled(self, pw);

    return cAtFalse;
    }

eAtRet ThaModulePwePwTxAlarmForce(ThaModulePwe self, AtPw pw, eAtPwAlarmType alarmType)
    {
    if (self)
        return mMethodsGet(self)->PwTxAlarmForce(self, pw, alarmType);

    return cAtError;
    }

eAtRet ThaModulePwePwTxAlarmUnForce(ThaModulePwe self, AtPw pw, eAtPwAlarmType alarmType)
    {
    if (self)
        return mMethodsGet(self)->PwTxAlarmUnForce(self, pw, alarmType);

    return cAtError;
    }

uint32 ThaModulePwePwTxForcedAlarmGet(ThaModulePwe self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwTxForcedAlarmGet(self, pw);

    return 0;
    }

eAtRet ThaModulePweVcFragmentTypeSet(ThaModulePwe self, AtSdhChannel sdhVc)
    {
    if (self)
        return mMethodsGet(self)->VcFragmentTypeSet(self, sdhVc);

    return cAtError;
    }

eAtRet ThaModulePwePwCepEquip(ThaModulePwe self, AtPw pw, AtChannel subChannel, eBool equip)
    {
    if (self)
        return mMethodsGet(self)->PwCepEquip(self, pw, subChannel, equip);

    return cAtError;
    }

eAtRet ThaModulePwePwHeaderCepEbmEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->PwHeaderCepEbmEnable(self, pw, enable);

    return cAtError;
    }

eBool ThaModulePwePwCwAutoTxNPBitIsEnabled(ThaModulePwe self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwCwAutoTxNPBitIsEnabled(self, pw);

    return cAtFalse;
    }

eBool ThaModulePwePwCwAutoTxNPBitIsConfigurable(ThaModulePwe self)
    {
    if (self)
        return mMethodsGet(self)->PwCwAutoTxNPBitIsConfigurable(self);

    return cAtFalse;
    }

eAtRet ThaModulePwePwTohByteEnable(ThaModulePwe self, AtPw pw, const tThaTohByte *tohByte, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->PwTohByteEnable(self, pw, tohByte, enable);
    return cAtError;
    }

eAtRet ThaModulePwePwTohFirstByteEnable(ThaModulePwe self, AtPw pw, const tThaTohByte *tohByte, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->PwTohFirstByteEnable(self, pw, tohByte, enable);
    return cAtError;
    }

tThaTohByte* ThaModulePwePwTohFirstByteGet(ThaModulePwe self, AtPw pw, tThaTohByte *tohByte)
    {
    if (self)
        return mMethodsGet(self)->PwTohFirstByteGet(self, pw, tohByte);
    return NULL;
    }

eAtRet ThaModulePwePwTohLastByteEnable(ThaModulePwe self, AtPw pw, const tThaTohByte *tohByte, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->PwTohLastByteEnable(self, pw, tohByte, enable);
    return cAtError;
    }

tThaTohByte* ThaModulePwePwTohLastByteGet(ThaModulePwe self, AtPw pw, tThaTohByte *tohByte)
    {
    if (self)
        return mMethodsGet(self)->PwTohLastByteGet(self, pw, tohByte);
    return NULL;
    }

eAtRet ThaModulePwePwTohEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->PwTohEnable(self, pw, enable);
    return cAtError;
    }

eAtRet ThaModulePweTohPayloadSizeSet(ThaModulePwe self, AtPw pw, uint32 numFramePerPacket)
    {
    if (self)
        return mMethodsGet(self)->PwTohPayloadSizeSet(self, pw, numFramePerPacket);
    return cAtError;
    }

uint16 ThaModulePweTohPayloadSizeGet(ThaModulePwe self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwTohPayloadSizeGet(self, pw);

    return 0;
    }

eBool ThaModulePwePwTohIsEnabled(ThaModulePwe self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwTohIsEnabled(self, pw);
    return cAtFalse;
    }

eAtRet ThaModulePweBindLineToPw(ThaModulePwe self, AtSdhLine line, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->BindLineToPw(self, line, pw);
    return cAtError;
    }

uint8 ThaModulePwePwTohBytesBitmapGet(ThaModulePwe self, AtPw pw, uint32 *bitmaps)
    {
    if (self)
        return mMethodsGet(self)->PwTohBytesBitmapGet(self, pw, bitmaps);

    return 0;
    }

uint16 ThaModulePwePwCesMaxNumFramesInPacket(ThaModulePwe self)
    {
    if (self)
        return mMethodsGet(self)->PwCesMaxNumFramesInPacket(self);

    return 0;
    }

uint16 ThaModulePwePwTohMaxNumFrames(ThaModulePwe self)
    {
    if (self)
        return mMethodsGet(self)->PwTohMaxNumFrames(self);

    return 0;
    }

uint32 ThaModulePwePwTxMbitPacketsGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->PwTxMbitPacketsGet(self, pw, clear);

    return 0;
    }

eAtRet ThaModulePwePwNumDs0TimeslotsSet(ThaModulePwe self, AtPw pw, uint16 numTimeslots)
    {
    if (self)
        return mMethodsGet(self)->PwNumDs0TimeslotsSet(self, pw, numTimeslots);

    return cAtError;
    }

eAtRet ThaModulePwePwNumFramesInPacketSet(ThaModulePwe self, AtPw pw, uint16 numFramesInPacket)
    {
    if (self)
        return mMethodsGet(self)->PwNumFramesInPacketSet(self, pw, numFramesInPacket);

    return cAtError;
    }

uint8 ThaModulePwePwNumDs0TimeslotsGet(ThaModulePwe self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwNumDs0TimeslotsGet(self, pw);

    return 0;
    }

uint16 ThaModulePweNumFramesInPacketGet(ThaModulePwe self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->NumFramesInPacketGet(self, pw);

    return 0;
    }

eAtRet ThaModulePwePayloadSizeSet(ThaModulePwe self, AtPw pw, uint16 payloadSize)
    {
    if (self)
        return mMethodsGet(self)->PayloadSizeSet(self, pw, payloadSize);

    return cAtError;
    }

uint16 ThaModulePwePayloadSizeGet(ThaModulePwe self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PayloadSizeGet(self, pw);

    return 0;
    }

uint32 ThaModulePwePwTxPacketsGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->PwTxPacketsGet(self, pw, clear);

    return 0;
    }

uint32 ThaModulePwePwTxBytesGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->PwTxBytesGet(self, pw, clear);

    return 0;
    }

uint32 ThaModulePwePwTxLbitPacketsGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->PwTxLbitPacketsGet(self, pw, clear);

    return 0;
    }

uint32 ThaModulePwePwTxRbitPacketsGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->PwTxRbitPacketsGet(self, pw, clear);

    return 0;
    }

uint32 ThaModulePwePwTxNbitPacketsGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->PwTxNbitPacketsGet(self, pw, clear);

    return 0;
    }

uint32 ThaModulePwePwTxPbitPacketsGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->PwTxPbitPacketsGet(self, pw, clear);

    return 0;
    }

eAtRet ThaModulePwePwTypeSet(ThaModulePwe self, AtPw pw, eAtPwType pwType)
    {
    if (self)
        return mMethodsGet(self)->PwTypeSet(self, pw, pwType);

    return cAtError;
    }

eAtRet ThaModulePwePwRtpEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->PwRtpEnable(self, pw, enable);

    return cAtError;
    }

eAtRet ThaModulePwePwCwEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->PwCwEnable(self, pw, enable);

    return cAtError;
    }

eAtRet ThaModulePwePwCwAutoTxLBitEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->PwCwAutoTxLBitEnable(self, pw , enable);

    return cAtError;
    }

eBool ThaModulePwePwCwAutoTxLBitIsEnabled(ThaModulePwe self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwCwAutoTxLBitIsEnabled(self, pw);

    return cAtFalse;
    }

eAtRet ThaModulePwePwCwAutoTxRBitEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->PwCwAutoTxRBitEnable(self, pw, enable);

    return cAtError;
    }

eBool ThaModulePwePwCwAutoTxRBitIsEnabled(ThaModulePwe self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwCwAutoTxRBitIsEnabled(self, pw);

    return cAtFalse;
    }

eAtRet ThaModulePweNumVlansSet(ThaModulePwe self, AtPw pw, uint8 numVlans)
    {
    if (self)
        return mMethodsGet(self)->NumVlansSet(self, pw, numVlans);

    return cAtError;
    }

eAtRet ThaModulePweHeaderWrite(ThaModulePwe self, AtPw pw, uint8 *buffer, uint8 headerLenInByte, AtPwPsn psn)
    {
    if (self)
        return mMethodsGet(self)->HeaderWrite(self, pw, buffer, headerLenInByte, psn);

    return cAtError;
    }

eAtRet ThaModulePweHeaderRead(ThaModulePwe self, AtPw pw, uint8 *buffer, uint8 bufferSize)
    {
    if (self)
        return mMethodsGet(self)->HeaderRead(self, pw, buffer, bufferSize);

    return cAtError;
    }

eAtRet ThaModulePwePwSuppressEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->PwSupressEnable(self, pw, enable);

    return cAtError;
    }

eBool ThaModulePwePwSuppressIsEnabled(ThaModulePwe self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwSupressIsEnabled(self, pw);

    return cAtFalse;
    }

eAtRet ThaModulePwePwPsnTypeSet(ThaModulePwe self, AtPw pw, AtPwPsn psn)
    {
    if (self)
        return mMethodsGet(self)->PwPsnTypeSet(self, pw, psn);

    return cAtError;
    }

eAtRet ThaModulePwePwCwAutoTxNPBitEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->PwCwAutoTxNPBitEnable(self, pw, enable);

    return cAtError;
    }

void ThaModulePwePwCepFractionalEquippedChannelsGet(ThaModulePwe self, AtPw pw, AtList equippedChannels)
    {
    if (self)
        mMethodsGet(self)->PwCepFractionalEquippedChannelsGet(self, pw, equippedChannels);
    }

void ThaModulePwePwRegsShow(AtPw pw)
    {
    ThaModulePwe self = (ThaModulePwe)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)pw), cThaModulePwe);
    AtPrintc(cSevInfo, "* PWE registers of channel '%s':\r\n", AtObjectToString((AtObject)pw));
    if (self)
        mMethodsGet(self)->PwRegsShow(self, pw);

    AtPrintc(cSevInfo, "\r\n");
    }

void ThaModulePweRegDisplay(AtPw pw, const char* regName, uint32 address)
    {
    ThaDeviceRegNameDisplay(regName);
    ThaDeviceChannelRegValueDisplay((AtChannel)pw, address, cThaModulePwe);
    }

void ThaModulePweLongRegDisplay(AtPw pw, const char* regName, uint32 address)
    {
    ThaDeviceRegNameDisplay(regName);
    ThaDeviceChannelRegLongValueDisplay((AtChannel)pw, address, cThaModulePwe);
    }

eAtRet ThaModulePwePwRemovingPrepare(ThaModulePwe self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwRemovingPrepare(self, pw);
    return cAtErrorNullPointer;
    }

void ThaModulePwePwDebug(AtPw pw)
    {
    ThaModulePwe self = (ThaModulePwe)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)pw), cThaModulePwe);
    if (self)
        mMethodsGet(self)->PwDebug(self, pw);
    }

eAtRet ThaModulePwePwPtchInsertionModeSet(ThaModulePwe self, AtPw pw, eAtPtchMode insertMode)
    {
    if(self)
        return mMethodsGet(self)->PwPtchInsertionModeSet(self, pw, insertMode);
    return cAtErrorNullPointer;
    }

eAtPtchMode ThaModulePwePwPtchInsertionModeGet(ThaModulePwe self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwPtchInsertionModeGet(self, pw);
    return cAtPtchModeUnknown;
    }

eAtRet ThaModulePweEthFlowPtchInsertionModeSet(ThaModulePwe self, AtEthFlow flow, eAtPtchMode insertMode)
    {
    if(self)
        return mMethodsGet(self)->EthFlowPtchInsertionModeSet(self, flow, insertMode);
    return cAtErrorNullPointer;
    }

eAtPtchMode ThaModulePweEthFlowPtchInsertionModeGet(ThaModulePwe self, AtEthFlow flow)
    {
    if (self)
        return mMethodsGet(self)->EthFlowPtchInsertionModeGet(self, flow);
    return cAtPtchModeUnknown;
    }

eAtRet ThaModulePwePwJitterAttenuatorEnable(ThaModulePwe self, AtPw pwAdapter, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->PwJitterAttenuatorEnable(self, pwAdapter, enable);

    return cAtError;
    }

eAtRet ThaModulePweHdlcPwPayloadTypeSet(ThaModulePwe self, ThaPwHdlcAdapter adapter, eAtPwHdlcPayloadType payloadType)
    {
    if (self)
        return mMethodsGet(self)->HdlcPwPayloadTypeSet(self, adapter, payloadType);

    return cAtError;
    }

eBool ThaModulePwePwCwIsEnabled(ThaModulePwe self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwCwIsEnabled(self, pw);

    return cAtFalse;
    }

eAtRet ThaModulePwePwPtchServiceEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    if(self)
        return mMethodsGet(self)->PwPtchServiceEnable(self, pw, enable);
    return cAtErrorNullPointer;
    }

eBool ThaModulePwePwPtchServiceIsEnabled(ThaModulePwe self, AtPw pw)
    {
    if(self)
        return mMethodsGet(self)->PwPtchServiceIsEnabled(self, pw);
    return cAtFalse;
    }

eAtRet ThaModulePweEthFlowPtchServiceEnable(ThaModulePwe self, AtEthFlow flow, eBool enable)
    {
    if(self)
        return mMethodsGet(self)->EthFlowPtchServiceEnable(self, flow, enable);
    return cAtErrorNullPointer;
    }

eBool ThaModulePweEthFlowPtchServiceIsEnabled(ThaModulePwe self, AtEthFlow flow)
    {
    if(self)
        return mMethodsGet(self)->EthFlowPtchServiceIsEnabled(self, flow);
    return cAtFalse;
    }

eAtRet ThaModulePweTxPwActiveForce(ThaModulePwe self, AtPw pw, eBool enabled)
    {
    if (self)
        return mMethodsGet(self)->TxPwActiveForce(self, pw, enabled);

    return cAtErrorNullPointer;
    }

eBool ThaModulePweTxPwActiveIsForced(ThaModulePwe self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->TxPwActiveIsForced(self, pw);

    return cAtFalse;
    }

eAtRet ThaModulePweSubPortVlanBypass(ThaModulePwe self, eBool bypass)
    {
    if (self)
        return mMethodsGet(self)->SubPortVlanBypass(self, bypass);

    return cAtErrorNullPointer;
    }

eBool ThaModulePweSubPortVlanIsBypassed(ThaModulePwe self)
    {
    if (self)
        return mMethodsGet(self)->SubPortVlanIsBypassed(self);

    return cAtTrue;
    }

uint32 ThaModulePweBaseAddress(ThaModulePwe self)
    {
    if (self)
        return mMethodsGet(self)->BaseAddress(self);
    return 0;
    }

uint32 ThaModulePwePlaBaseAddress(ThaModulePwe self)
    {
    if (self)
        return mMethodsGet(self)->PlaBaseAddress(self);
    return 0;
    }

eBool ThaModulePwePwRemovingShouldStart(ThaModulePwe self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwRemovingShouldStart(self, pw);
    return cAtFalse;
    }

void ThaModulePweCacheUsageGet(ThaModulePwe self, tThaPweCache *cache)
    {
    if (self)
        mMethodsGet(self)->CacheUsageGet(self, cache);
    }

eBool ThaModulePweCanAccessCacheUsage(ThaModulePwe self)
    {
    if (self)
        return mMethodsGet(self)->CanAccessCacheUsage(self);
    return cAtFalse;
    }

uint32 ThaModulePwePwPweDefaultOffset(ThaModulePwe self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwPweDefaultOffset(self, pw);
    return cInvalidUint32;
    }

uint32 ThaModulePwePwPlaDefaultOffset(ThaModulePwe self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwPlaDefaultOffset(self, pw);
    return cInvalidUint32;
    }

eBool ThaModulePweUtilPwCircuitIsDe1OverStm(AtPw pw)
    {
    if (pw)
        return PwCircuitIsDe1OverStm(pw);

    return cAtFalse;
    }

eBool ThaModulePweUtilPwCircuitIsLiuDe1(AtPw pw)
    {
    return PwCircuitIsLiuDe1(pw);
    }

eAtRet ThaModulePwePwIdleCodeSet(ThaModulePwe self, ThaPwAdapter pwAdapter, uint8 payloadIdleCode)
    {
    if (self)
        return mMethodsGet(self)->PwIdleCodeSet(self, pwAdapter, payloadIdleCode);
    return cAtErrorNullPointer;
    }

eAtRet ThaModulePwePwCesopCasIdleCode1Set(ThaModulePwe self, ThaPwAdapter pwAdapter, uint8 signallingIdleCode)
    {
    if (self)
        return mMethodsGet(self)->PwCesopCasIdleCode1Set(self, pwAdapter, signallingIdleCode);
    return cAtErrorNullPointer;
    }

uint8 ThaModulePwePwCesopCasIdleCode1Get(ThaModulePwe self, ThaPwAdapter pwAdapter)
    {
    if (self)
        return mMethodsGet(self)->PwCesopCasIdleCode1Get(self, pwAdapter);
    return cAtErrorNullPointer;
    }

eAtRet ThaModulePwePwIdleCodeInsertionEnable(ThaModulePwe self, ThaPwAdapter pwAdapter, eBool autoIdleEnable)
    {
    if (self)
        return mMethodsGet(self)->PwIdleCodeInsertionEnable(self, pwAdapter, autoIdleEnable);
    return cAtErrorNullPointer;
    }

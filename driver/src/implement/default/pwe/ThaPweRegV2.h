/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : PWE
 *
 * File        : ThaPweReg.h
 *
 * Created Date: Nov 20, 2012
 *
 * Description : Pseudowire Encapsulation registers
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _THAPWEREGV2_H_
#define _THAPWEREGV2_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaPweReg.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

/*--------------------------- PWE ----------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name: Pseudowire Transmit Enable Control
Reg Addr: 0x321100
Reg Desc: This register configures pseudowire enable for transmit to Ethernet direction
------------------------------------------------------------------------------*/
#define cThaRegPwTxEnCtrl                           0x321000

/*--------------------------------------
BitField Name: TxPwEn
BitField Type: R/W
BitField Desc: Tx PW enable
           - 1: Enable
           - 0: Disable
--------------------------------------*/
#define cThaRegPwTxEnMask                           cBit0
#define cThaRegPwTxEnShift                          0

/*--------------------------------------
BitField Name: TxPwCwType
BitField Type: R/W
BitField Desc: Control word length
               - 0: Control word 4-byte (used for PDH PW or PW ATM N to 1)
               - 1: Control word 3-byte (used for PW ATM 1 to 1)
--------------------------------------*/
#define cThaRegPwTxPwCwTypeMask                     cBit1
#define cThaRegPwTxPwCwTypeShift                    1

#define cThaRegPwTxPwLbitForceMask                  cBit2
#define cThaRegPwTxPwLbitForceShift                 2

#define cThaRegPwTxPwMbitForceMask                  cBit3
#define cThaRegPwTxPwMbitForceShift                 3

#define cThaRegPwTxPwAutoLbitMask                   cBit4
#define cThaRegPwTxPwAutoLbitShift                  4

#define cThaRegPwTxPwAutoMbitMask                   cBit5
#define cThaRegPwTxPwAutoMbitShift                  5

/*--------------------------- PLA ----------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name: PWPDH Payload Assemble Payload Size Control
Reg Addr: 0x2C2000-0x2C20FF
          The address format for these registers is 0x2C2000 + pwid
          Where: pwid (0 � 255) Pseudowire Identification
Reg Desc: These registers are used to configure payload size in each PW
          channel
------------------------------------------------------------------------------*/

/*--------------------------------------
BitField Name: PDHPWPlaAsmPldSupress
--------------------------------------*/
#define cThaPWPdhPlaAsmPldSupressMask           cBit18
#define cThaPWPdhPlaAsmPldSupressShift          18

/*--------------------------------------
BitField Name: PWPDHPlaAsmPldTypeCtrl
BitField Type: R/W
BitField Desc: Payload type in the PW channel
               - 0: SAToP
               - 4: CES with CAS
               - 5: CES without CAS
--------------------------------------*/
#undef cThaPWPdhPlaAsmPldTypeCtrlMask
#undef cThaPWPdhPlaAsmPldTypeCtrlShift
#define cThaPWPdhPlaAsmPldTypeCtrlMask                 cBit17_15
#define cThaPWPdhPlaAsmPldTypeCtrlShift                15

/*--------------------------------------
BitField Name: PWPDHPlaAsmPldSizeCtrl
BitField Type: R/W
BitField Desc: Payload size in the PW channel
               SAToP mode: [11:0] payload size
               AAL1 mode : [5:0] number of DS0 [11:6] number of PDU
               CES mode  : [5:0] number of DS0 [14:6] number of NxDS0
--------------------------------------*/
#undef cThaPWPdhPlaAsmPldSizeCtrlMask
#undef cThaPWPdhPlaAsmPldSizeCtrlShift
#define cThaPWPdhPlaAsmPldSizeCtrlMask                 cBit14_0
#define cThaPWPdhPlaAsmPldSizeCtrlShift                0

#undef cThaPWPdhPlaAsmCesNumDs0PldSizeCtrlMask
#undef cThaPWPdhPlaAsmCesNumDs0PldSizeCtrlShift
#undef cThaPWPdhPlaAsmCesNumFrmPldSizeCtrlMask
#undef cThaPWPdhPlaAsmCesNumFrmPldSizeCtrlShift
#define cThaPWPdhPlaAsmCesNumDs0PldSizeCtrlMask       cBit5_0
#define cThaPWPdhPlaAsmCesNumDs0PldSizeCtrlShift      0
#define cThaPWPdhPlaAsmCesNumFrmPldSizeCtrlMask       cBit14_6
#define cThaPWPdhPlaAsmCesNumFrmPldSizeCtrlShift      6

/*------------------------------------------------------------------------------
Reg Name: Thalassa PDHPW TX R-Bit control
Reg Addr: 0x325000 + PWID
Reg Desc:
------------------------------------------------------------------------------*/
#define cThaRegPWPdhTxRBitControl 0x325000

/* 1: force */
#define cThaPWPdhTxRBitForceMask  cBit1
#define cThaPWPdhTxRBitForceShift 1

/* 1: disable */
#define cThaPWPdhTxRBitAutoMask   cBit0
#define cThaPWPdhTxRBitAutoShift  0

#ifdef __cplusplus
}
#endif
#endif /* _THAPWEREGV2_H_ */
